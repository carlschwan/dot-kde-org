---
title: "Akademy 2017 \u2012 Call for Hosts"
date:    2016-11-30
authors:
  - "sandroandrade"
slug:    akademy-2017-‒-call-hosts
---
<img src="https://akademy.kde.org/sites/akademy.kde.org/files/2016/akademy_website_header_blank_0.png"/>

Akademy, KDE's annual conference, requires a place and team for the year 2017. That's why we are looking for a vibrant, enthusiatic spot in Europe that can host us!

<h2>A bit about Akademy</h2>

Akademy is KDE's annual get-together where our creativity, productivity and love are at their peak. Developers, users, translators, students, artists, writers - pretty much anyone who has been involved with KDE will join Akademy to participate and learn. Contents will range from keynote speeches and a two-day dual track session of talks by the FOSS community, to workshops and Birds of a Feather (BoF) sessions where we plot the future of the project.

Friday is scheduled for the KDE e.V. General Assembly and a pre-Akademy party/welcoming event. Saturday and Sunday covers the keynotes, talks and lightning talks. The remaining four days are used for BoFs, intensive coding sessions and workshops for smaller groups of 10 to 30 people out of which one day is reserved for a Day Trip of the attendees around the local touristic sights.

Hosting Akademy is a great way to contribute to a movement of global collaboration. You get a chance to host one of the largest FOSS community in the world with contributors from over the world and be a witness to a wonderful inter-cultural fusion of attendees in your home town. You'll also get great exposure to Free Software. It is a great opportunity for the local university students, professors, technology enthusiasts and professionals to try their hand at something new.

<h2>What You Need to Do</h2>

Akademy requires a location in Europe, with a nice conference venue, that is easy to reach, preferably close to an international airport.

Organizing Akademy is a demanding and a resource intensive task but you’ll be guided along the entire process by people who’ve been doing this since years. Nevertheless, the local team should be prepared to spare a considerable amount of time for this. 

For detailed information, please see the <a href="https://ev.kde.org/akademy/CallforHosts_2017.pdf" target="_blank">Call for Hosts</a>. Questions and applications should be addressed to the <a href="mailto:kde-ev-board@kde.org">Board of KDE e.V.</a> or the <a href="mailto:/akademy-team@kde.org">Akademy Team</a>. Please indicate your interest in hosting Akademy to the Board of KDE e.V. by December 15th. Full applications will be accepted until 15th January. We look forward to your enthusiasm in being the next host for Akademy 2017!