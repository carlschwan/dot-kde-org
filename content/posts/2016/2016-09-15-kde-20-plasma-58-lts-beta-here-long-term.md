---
title: "KDE at 20: Plasma 5.8 LTS Beta. Here for the Long Term."
date:    2016-09-15
authors:
  - "jriddell"
slug:    kde-20-plasma-58-lts-beta-here-long-term
comments:
  - subject: "Looking good!"
    date: 2016-09-15
    body: "<p>Plasma 5.7.x was already quite amazing... this version really seems even more polished!</p><p>&nbsp;</p><p>Kudos, and thanks!</p>"
    author: "Jan"
  - subject: "Panel auto hide animation is back"
    date: 2016-09-17
    body: "<p>Also thanks to KWin on X11 plasma panel auto hide is back again!</p>"
    author: "Josep"
  - subject: "Awesome!"
    date: 2016-09-23
    body: "<p>Great work, everyone on KDE team does excellent job everyday.</p><p>I am looking forward to this release.</p><p>Btw. I love plasma default wallpapers, it makes really good first impression when I am logged into my new plasma desktop and everything looks excellent!</p>"
    author: "Martin1234"
  - subject: "Monochrome icons"
    date: 2016-09-28
    body: "<p>Can please someone explains to me how can monochrome icons be synonym for the professional look and yadda yadda.</p><p>Because windows has them?</p><p>Plasma 4 had beautiful and visible icons ... not this middle age monochrome crap.</p><p>Well I am just overwriting breeze icons with old Oxygen icons after each upgrade. Some icons like kteatime are without animations but at least they look alive and are visible ... unlike breeze icons that all look the same.</p><p>Beside that ... many thanks for your effort and all the work that you have done so far.</p><p>Cheers for that</p>"
    author: "Damijan"
  - subject: "I agree"
    date: 2016-09-29
    body: "<p>I second that 100%.</p><p>Unfortunately looks like \"trendy, fashion and eyecandy\" overruled \"useable\" some years ago, not just in the KDE world.</p>"
    author: "Vajsravana"
  - subject: "kde-look icons do not work anymore"
    date: 2017-02-21
    body: "<p>I Agree too. I try to \"fix\" the problem by applying some of older icon themes from kde-look but only Faenza seem to be compatible nowadays, and even it beginning to miss icons. All of my favorite icon packs from 10+ years ago are no longer compatible. I cannot of course expect those theme makers to dedicated their entire life to keeping up with KDE updates, nor for KDE to be compatible with decade old formats. Just miss customization community being so active in the past providing all the choices, to save us from modern monochrome flatness.</p>"
    author: "Sasha"
  - subject: "agree too"
    date: 2017-02-21
    body: "<p>I Agree too. I try to \"fix\" the problem by applying some of older icon themes from kde-look but only Faenza seem to be compatible nowadays, and even it beginning to miss icons. All of my favorite icon packs from 10+ years ago are no longer compatible. I cannot of course expect those theme makers to dedicated their entire life to keeping up with KDE updates, nor for KDE to be compatible with decade old formats. Just miss customization community being so active in the past providing all the choices, to save us from modern monochrome flatness.</p>"
    author: "Sasha"
---
<a name="cp-content"></a>
<div style='clear:both;'></div>
    <div id="module">
    </div>


<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<!-- video
<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/A9MtFqkRFwQ?rel=0" frameborder="0" allowfullscreen></iframe>
</figure>
<br clear="all" />
-->

<figure style="float: none">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-wee.png" style="border: 0px" width="600" height="375" alt="KDE Plasma 5.8 LTS" />
</a>
<figcaption>KDE Plasma 5.8 LTS Beta</figcaption>
</figure>

<p>
Thursday, 15 September 2016. Today KDE <a href="http://www.kde.org/announcements/plasma-5.7.95.php">releases a beta of its first Long Term Support edition of its flagship desktop software, Plasma</a>.  This marks the point where the developers and designers are happy to recommend Plasma for the widest possible audience be they enterprise or non-techy home users.  If you tried a KDE desktop previously and have moved away, now is the time to re-assess, Plasma is simple by default, powerful when needed.
</p>

<br clear="all" />
<h2>Plasma's Comprehensive Features</h2>

<p>Take a look at what Plasma offers, a comprehensive selection of features unparalleled in any desktop software.</p>

<h3>Desktop Widgets</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-widgets.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-widgets-wee.png" style="border: 0px" width="350" height="196" alt="Desktop Widgets" />
</a>
<figcaption>Desktop Widgets</figcaption>
</figure>

<p>Cover your desktop in useful widgets to keep you up to date with weather, amused with comics or helping with calculations.</p>

<br clear="all" />
<h3>Get Hot New Stuff</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-hotnewstuff.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-hotnewstuff-wee.png" style="border: 0px" width="350" height="241" alt="Get Hot New Stuff" />
</a>
<figcaption>Get Hot New Stuff</figcaption>
</figure>

<p>Download wallpapers, window style, widgets, desktop effects and dozens of other resources straight to your desktop.  We work with the new <a href="http://store.kde.org">KDE Store</a> to bring you a wide selection of addons for you to install.</p>

<br clear="all" />
<h3>Desktop Search</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-search-launch.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-search-launch-wee.png" style="border: 0px" width="350" height="417" alt="Desktop Search" />
</a>
<figcaption>Desktop Search</figcaption>
</figure>

<p>Plasma will let you easily search your desktop for applications, folders, music, video, files... everything you have.</p>

<br clear="all" />
<h3>Unified Look</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-toolkits.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-toolkits-wee.png" style="border: 0px" width="350" height="196" alt="Unified Look" />
</a>
<figcaption>Unified Look</figcaption>
</figure>

<p>Plasma's default Breeze theme has a unified look across all the common programmer toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, even LibreOffice.</p>

<br clear="all" />
<h3>Phone Integration</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-kdeconnect.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-kdeconnect-wee.png" style="border: 0px" width="350" height="337" alt="Phone Integration" />
</a>
<figcaption>Phone Integration</figcaption>
</figure>
<p>Using KDE Connect you'll be notified on your desktop of text message, can easily transfer files, have your music silenced during calls and even use your phone as a remote control.</p>

<br clear="all" />
<h3>Infinitely Customisable</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-custom.jpg">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-custom-wee.jpg" style="border: 0px" width="350" height="218" alt="Infinitely Customisable" />
</a>
<figcaption>Infinitely Customisable</figcaption>
</figure>
<p>Plasma is simple by default but you can customise it however you like with new widgets, panels, screens and styles.</p>

<br clear="all" />
<h2>New in Plasma 5.8</h2>
<h3>Unified Boot to Shutdown Artwork</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-boot.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-boot-wee.png" style="border: 0px" width="348" height="153" alt="Unified Boot to Shutdown Artwork" />
</a>
<figcaption>Unified Boot to Shutdown Artwork</figcaption>
</figure>

<p>This release brings an all-new login screen design giving you a complete Breeze startup to shutdown experience. The layout has been tidied up and is more suitable for workstations that are part of a domain or company network. While it is much more streamlined, it also allows for greater customizability: for instance, all Plasma wallpaper plugins, such as slideshows and animated wallpapers, can now be used on the lock screen.</p>

<br clear="all" />
<h3>Right-to-Left Language Support</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-reverse.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-reverse-wee.png" style="border: 0px" width="325" height="299" alt="Right-to-Left Language Support" />
</a>
<figcaption>Right-to-Left Language Support</figcaption>
</figure>
<p>Support for Semitic right-to-left written languages, such as Hebrew and Arabic, has been greatly improved. Contents of panels, the desktop, and configuration dialogs are mirrored in this configuration. Plasma’s sidebars, such as widget explorer, window switcher, activity manager, show up on the right side of the screen.</p>

<br clear="all" />
<h3>Improved Applets</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-media-controls.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-media-controls.png" style="border: 0px" width="331" height="370" alt="Context Menu Media Controls" />
</a>
<figcaption>Context Menu Media Controls</figcaption>
</figure>
<p>The virtual desktop switcher (“Pager”) and window list applets have been rewritten, using the new task manager back-end we introduced in Plasma 5.7. This allows them to use the same dataset as the task manager and improves their performance while reducing memory consumption. The virtual desktop switcher also acquired an option to show only the current screen in multi-screen setups and now shares most of its code with the activity switcher applet.</p>

<p>Task manager gained further productivity features in this release. Media controls that were previously available in task manager tooltips only are now accessible in the context menus as well. In addition to bringing windows to the front during a drag and drop operation, dropping files onto task manager entries themselves will now open them in the associated application. Lastly, the popup for grouped windows can now be navigated using the keyboard and text rendering of its labels has been improved.</p>

<br clear="all" />
<h3>Simplified Global Shortcuts</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-global-shortcuts.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-global-shortcuts-wee.png" style="border: 0px" width="350" height="204" alt="Global Shortcuts Setup" />
</a>
<figcaption>Global Shortcuts Setup</figcaption>
</figure>

<p>Global shortcuts configuration has been simplified to focus on the most common task, that is launching applications. Building upon the jump list functionality added in previous releases, global shortcuts can now be configured to jump to specific tasks within an application.</p>

<p>Thanks to our Wayland effort, we can finally offer so-called “modifier-only shortcuts”, enabling you to open the application menu by just pressing the Meta key. Due to popular demand, this feature also got backported to the X11 session.

<br clear="all" />
<h3>Other improvements</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-discover.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-discover-wee.png" style="border: 0px" width="350" height="281" alt="Plasma Discover's new UI" />
</a>
<figcaption>Plasma Discover's new UI</figcaption>
</figure>

<p>This release sees many bugfixes in multi-screen support and, together with Qt 5.6.1, should significantly improve your experience with docking stations and projectors.</p>

<p>KWin, Plasma’s window manager, now allows compositing through llvmpipe, easing the deployment on exotic hardware and embedded devices. Now that there is a standardized and widely-used interface for applications to request turning off compositing, the "Unredirect Fullscreen” option has been removed. It often lead to stability issues and because of that was already disabled for many drivers.</p>

<p>Now that <a href="https://dot.kde.org/2016/08/10/kdes-kirigami-ui-framework-gets-its-first-public-release">Kirigami</a>, our set of versatile cross-platform UI components, has been released, we’re pleased to bring you a revamped version of Plasma Discover based on Kirigami.</p>

<p>We have new default fonts, the Noto font from Google covers all scripts available in the Unicode standard while our new monospace font Hack is perfect for coders and terminal users.</p>

<br clear="all" />
<h3>We’re in Wayland!</h3>
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-wayland.png">
<img src="http://www.kde.org/announcements/plasma-5.8/plasma-5.8-wayland-wee.png" style="border: 0px" width="350" height="196" alt="Plasma on Wayland Now with GTK+ support" />
</a>
<figcaption>Plasma on Wayland Now with GTK+ support</figcaption>
</figure>

<p>Plasma on Wayland has come a long way in the past months. While our long term support promise does not apply to the fast-evolving Wayland stack, we think it is ready to be tested by a broader audience. There will still be minor glitches and missing features, but we are now at a point where we can ask you to give it a try and report bugs. Notable improvements in this release include:</p>
<ul>
<li>Support for xdg-shell, i.e. GTK+ applications are now supported</li>
<li>Much improved touch screen support</li>
<li>Support for touchpad gestures – the infrastructure is there, there aren't any gestures by default yet</li>
<li>The “Sliding Popups” effect is now supported</li>
<li>Clipboard contents are synced between X11 and Wayland applications</li>
</ul>

<p><a href="http://www.kde.org/announcements/plasma-5.7.5-5.7.95-changelog.php">Full Plasma 5.7.95 LTS changelog</a></p>
<p><a href="http://www.kde.org/announcements/plasma-5.7.95.php">Plasma 5.8 announcement</a></p>
<!--break-->

<!-- // Boilerplate again -->

<h2>Live Images</h2>

<p>
The easiest way to try it out is with a live image booted off a USB disk. You can find a list of <a href='https://community.kde.org/Plasma/LiveImages'>Live Images with Plasma 5</a> on the KDE Community Wiki.
</p>

<h2>Package Downloads</h2>

<p>Distributions have created, or are in the process of creating, packages listed on our wiki page.</p>

<ul>
<li>
<a href='https://community.kde.org/Plasma/Packages'>Package download wiki page</a></li>
</ul>

<h2>Source Downloads</h2>

<p>You can install Plasma 5 directly from source. KDE's community wiki has <a href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>. Note that Plasma 5 does not co-install with Plasma 4, you will need to uninstall older versions or install into a separate prefix.
</p>

<ul>
<li>

<a href='http://www.kde.org/info/plasma-5.7.95.php'>Source Info Page</a>
</li>
</ul>

<h2>Feedback</h2>

You can give us feedback and get updates on <a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='http://www.kde.org/announcements/facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a> or <a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='http://www.kde.org/announcements/twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a> or <a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='http://www.kde.org/announcements/googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>.
<p>
Discuss Plasma 5 on the <a href='https://forum.kde.org/viewforum.php?f=289'>KDE Forums Plasma 5 board</a>.</a>
</p>

<p>You can provide feedback direct to the developers via the <a href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel mailing list</a> or report issues via <a href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>.  If you like what the team is doing, please let them know!</p>

<p>Your feedback is greatly appreciated.</p>

