---
title: "KDE PIM Spring Sprint Report in Toulouse "
date:    2016-04-15
authors:
  - "jriddell"
slug:    kde-pim-spring-sprint-report-toulouse
comments:
  - subject: "Just want to say thank you!"
    date: 2016-04-19
    body: "<p>&nbsp;</p><p>We're using KDEPIM (still KDE4) on our KDE (now Plasma) desktops in our office (10 PCs) since 10 years now.</p><p>We're also running a KOLAB instance since 1 year because we wanted to gain groupware funtionality. So far we're pleased with the result. Keep up the good work!</p><p>We're really looking forward to new shiny KF5 based KDEPIM!</p>"
    author: "thomas"
  - subject: "Long live the KDE-PIM indeed!"
    date: 2016-04-19
    body: "Long live the KDE-PIM indeed! And thank you guys for all the great work."
    author: "emilsedgh"
  - subject: "Update with 'social' times."
    date: 2016-04-29
    body: "<p>I think KDE-PIM should update with social times, making fields like Twitter/Facebook/Instagram/Skype etc, more accessible to the default UI. Granted is not open source or anything but alshould be some module that can quickly add such fields. Also there is like a lack of imagination on the KAddressBook that needs some true love.</p><p>KDE for the longest have tried to innovate using their Online Accounts on KDE System Settings, but having this spirit transfered to KAddressBook would be more consistent with the idea of the 'social desktop'.</p><p>Another idea is to get with other messaging platforms besides Email, like <strong>Bitmessage</strong>, or just be a platform to alternative messaging protocols.</p>"
    author: "Jzarecta"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/WorkingPhoto2-big.JPG"><img src="/sites/dot.kde.org/files/WorkingPhoto2-wee.JPG" /></a><br />Hard at work</div> 

Like a routine now, the KDE PIM spring sprint was held in Toulouse again, first week of April at <a href="https://www.ekito.fr/">Ekito</a>'s city center office, many thanks to them.

First of all thank to all the participants: Franck Arrecot, Andre Heinecke, Sandro Knauß, Volker Krause, John Layt, Christian Mollekopf, Laurent Montel, Kevin Ottens, Daniel Vrátil that made of this sprint an awesome moment.

As a general theme you could say we were preparing the future of PIM with the goal of stability: reducing some library dependencies, designing a way to reach a better internal architecture, cleaner codebase and improving communication among each contributor of the suite on the release process.
<!--break-->
Since there are plenty of different applications within PIM Suite we will break down what happened on each subproject and what benefit each thread gave to the main rope:

This sprint was kind of special because the famous John Layt was around, so the opportunity to work on removing the final uses of KDELibs4Support from PIM had to be taken! Most remaining uses of KCalendarSystem and KLocale could be ported, and the use of KTimeZone was largely contained to KCalCore implementation details. John is working on a replacement for the remaining features in KDateTime that are not yet covered by Qt, which would allow the remaining uses of KDELibs4Support code to be removed eventually. KAddressBook was able to work entirely without loading KDELibs4Support at the end of the sprint.

Daniel has been working with our awesome sysadmins on debugging and fixing an issue in the new CI that was causing (not just KDE PIM) unit tests to fail. He also worked on further improving Akonadi performance by redesigning the notification system, which opens doors to a new set of cool features like starting Akonadi agents on-demand and shutting them down when no longer needed.

Laurent did plenty of bug fixing and worked on the QtWebEngine support within PIM, this will later allow us to get rid of the unsupported QtWebkit.

Christian presented to us the state of Kube and Sink, the two new open source Kolab projects, how these would work and what are the issues they are facing. This overview helped us thinking of a possible integration when the project would reach a more mature phase.

Sandro and Christian started to work on a way to represent mails in QML instead of plain HTML. With HTML all interactions must go via HTML too so e.g. start decryption or show more info are links inside a browser window. With rendering to QML we can directly interact with the mail via QML Elements buttons, what is a cleaner way of user-feedback loop.  MimeTreeParser was prepared by Sandro to render to both outputs and mimetreeparser was cleaned up and refactored while at the sprint. Also it was discussed how to get rid of unnecessary dependencies, to untangle MimeTreeParser.

Andre took a dive into our crypto libraries, moving some generic non-GUI code from libkleo into QGpgMe. He also started preparing the QGpgMe library to be moved to GpgMe upstream. It was also discussed how error messages could be more helpful and how the API of QGpgMe could be modified to be more easy to use.

The Zanshin team, Kevin and Franck, added a couple of fixes and prepared a 0.4 release and designed a road map for the next 0.5 release.

One of the great benefits of this sprint was to share some experiences that have been made in Zanshin and that can be extend into the rest of the suite. The architectural work and experimentation done on Zanshin lead us to apply some of its principle to PIM, that is what we called the "zanshinification". We discussed how well the Zanshin onion architecture would fit the KDE PIM codebase and laid down a step by step plan with a couple of critical steps where we had to make sure performance are still good enough.  We think this architectural change would bring more stability, readability within the codebase and might help to attract new contributors. We have to keep in mind how important PIM is as one of the most feature-filled suite of personal information management in free software.

Release cycles and version numbers was another big topic on the sprint. Looking at how to manage versions among different PIM projects in a smoother way, Sandro identified some part of the process that could be improved. The main idea is to make sure everyone is aware of upcoming releases and changes to the dependency set of the software, and to make this clear to packagers as well.

Long live KDE PIM!
<div style="text-align: center; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/GroupPhoto-big.JPG"><img src="/sites/dot.kde.org/files/GroupPhoto-wee2.JPG" /></a><br />Your happy PIM team, Andre, Volker, John, Christian, Sandro (front), Franck (back), Laurent, Daniel</div> 
