---
title: "Akademy 2016 Tuesday BoF Wrapup Video"
date:    2016-09-06
authors:
  - "jriddell"
slug:    akademy-2016-tuesday-bof-wrapup-video
---
<p>In today's BoF wrapup at Akademy find out the future of Plasma music player, the Frameworks LTS and the future of KDE neon.</p>

<div style="text-align: center;"><iframe src="https://www.youtube.com/embed/8CpnjvaX8GU" frameborder="0" width="560" height="315"></iframe></div><!--break-->

