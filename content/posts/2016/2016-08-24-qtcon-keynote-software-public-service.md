---
title: "QtCon Keynote: Software as a Public Service "
date:    2016-08-24
authors:
  - "jriddell"
slug:    qtcon-keynote-software-public-service
---
<p><a href="https://conf.qtcon.org/en/qtcon/public/speakers/1387"><img alt="P1012552_juliareda_portrait" src="https://conf.qtcon.org/system/avatars/1387/large/P1012552_juliareda_portrait.jpg?1471512086" align="right" /></a>QtCon is happy to welcome Julia Reda, the closing keynote speaker.  Member of the European Parliament for the Pirate Party and Vice-Chair of the Greens/European Free Alliance. Reda's legislative focus is on copyright and internet policy issues.</p>

<p>As a member of the European Parliament and together with Max Andersson, Julia Reda initiated the pilot project “Governance and quality of software code – Auditing of free and open source software” in 2014 as a reaction to the so-called “heartbleed” bug in OpenSSL. The idea turned into the pilot-project "Free and Open Source Software Auditing“ (FOSSA) that is aiming at improving the security of those Free Software programs that are in use by the European Commission and the Parliament.</p>

<p>Although the implementation of this project did receive some feedback for improvement, Reda will explain why this project is important and how it takes use one step further towards understanding FLOSS as a public service: <a href="https://conf.qtcon.org/en/qtcon/public/events/520">"If free/libre open source software belongs to the public, the public needs to take responsibility for it."</a></p>

<p>Julia Reda's talk will leave participants at QtCon with an inspiring and forward-looking talk about Free Software, security and public responsibilty.</p>

<p>Happening on: <a href="https://conf.qtcon.org/en/qtcon/public/events/520">Sunday, 2016-09-04, 15:45 - 16:45 CEST, BCC Germany</a></p>

<!--break-->