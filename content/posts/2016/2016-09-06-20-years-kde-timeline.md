---
title: "20 Years of KDE Timeline"
date:    2016-09-06
authors:
  - "jriddell"
slug:    20-years-kde-timeline
comments:
  - subject: "First use and timeline"
    date: 2016-09-07
    body: "<p>My first KDE use ws the second beta release, which I guess was 1997/8. Haven't used it continuously since then, but keep coming back every once in a while and do tend to use it on my desktop.</p><p>Incidentally, starting the timeline with Thompson and Ritchie creating Unix gives entirely the wrong impression: Unix only came about because Bell Labs pulled out of the Multics project (they originally called their cut-down version Unics, making the pun more obvious) for various reasons, mostly financial, and Multics gave us many things which OSes still use and fail to better - the Multics dynamic linking, for example, was far more sophisticated than DLLs or the Unix equivalent.</p>"
    author: "dgrb"
---
<p><img src="https://timeline.kde.org/images/kde20_anos.png" alt="" width="720" height="281" /></p><p>KDE is celebrating 20 years as the original and best free software end-user creating community. The milestones of our project are marked on our <a href="https://timeline.kde.org/">20 Years of KDE timeline</a>. Find out the meetings and releases which defined KDE. Learn about the early and recent KDE gatherings around the world and how we have evolved over the years. What was your first KDE release?</p>