---
title: "Plasma Meets Nextcloud"
date:    2016-12-13
authors:
  - "jospoortvliet"
slug:    plasma-meets-nextcloud
comments:
  - subject: "Hot on the heels of this post"
    date: 2016-12-13
    body: "Hot on the heels of this post, we <a href=\"https://nextcloud.com/blog/nextloud-11-sets-new-standard-for-security-and-scalability/\">released Nextcloud 11.</a>\r\n\r\nThe security features in here are a very good incentive to upgrade and large installations (like the KDE Nextcloud instance!) will benefit from the improvements in scalability and performance.\r\n\r\nAlso: full-text search, improved federation, a preview of integrated video/audio chat and much more. Enjoy!"
    author: "jospoortvliet"
  - subject: "cooperation?"
    date: 2016-12-13
    body: "<p>1) Would it make sense to also have some cooperation with KolabSys?</p><p>2) I was wondering wether it is possible to divert NextCloud from its intended use as some sort of email archive? Does NextCould provide a full-text indexing and search?</p>"
    author: "thomas"
  - subject: "Collaboration with OC and others"
    date: 2016-12-13
    body: "<p>I think the best solution would be to define open standard for integration with Cloud Services. As&nbsp;<span style=\"background-color: rgba(255, 255, 255, 0.498039); color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px;\">federation defines s2s standard it would be perfect to have c2s standard.</span></p><p>The standard could be baased on other standard actually used (WebDav, iCal etc. )</p>"
    author: "Govi"
  - subject: "We introduced full text"
    date: 2016-12-16
    body: "We introduced full text search/indexing with Nextcloud 11, this week. It uses Apache Solr for the actual indexing and search."
    author: "jospoortvliet"
  - subject: "Right, something like the"
    date: 2016-12-16
    body: "Right, something like the <a href=\"https://blog.schiessle.org/2016/12/13/cloud-federation-getting-social/\">Federated Cloud Sharing</a> but for settings or such. That would be quite interesting yes!"
    author: "jospoortvliet"
  - subject: "Cooperation with KolabSys"
    date: 2016-12-21
    body: "KDE is already cooperating with KolabSys in several areas, such as the joint development of Kube, the next-generation PIM client. You are right in that it might make sense to publicly formalize this cooperation."
    author: "thomaspfeiffer"
  - subject: "Great initative!"
    date: 2016-12-22
    body: "<p>Next cloud integration would be very nice! Good luck with the project! I already use my phone to sync it with Next cloud.</p>"
    author: "Akos Szederjei"
  - subject: "collaboration with both Cloudservices, Own and nNext Cloud"
    date: 2017-06-02
    body: "<p>what's going on ?</p><p>we as users need a \"webdav\" and \"carddav\" possibility to sync the Clouds wth Plasma .</p><p>&nbsp;</p><p>best regards</p><p>Blacky</p>"
    author: "blackccrack"
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://dot.kde.org/sites/dot.kde.org/files/grouppic.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/grouppic_wee.jpg" /></a><br /><figcaption>Participants Frank, Thomas, Kai Uwe and Martin</figcaption></figure>
At a meeting back in July in Stuttgart, KDE and Nextcloud developers discussed deeper integration between the respective communities. We'd like to share some of those ideas and, as always, invite anyone interested in participating to help make it happen!

<h2>Deeper Plasma integration</h2>
A feature that has been discussed is synchronization of settings, password and file metadata over cloud servers.

The fork of Nextcloud from ownCloud has brought about new questions regarding this topic: Will the Plasma team have to support two different systems? It would obviously be vastly preferable if both cloud implementations could be supported with one client, which would require collaboration between ownCloud and Nextcloud. The Plasma team has communicated this requirement for both parties. From an implementation point of view, at least Nextcloud features a key-value store which could be used to store client settings.

Another area with room for improvement is syncing tags, ratings and comments. Nextcloud offers these and would be able to sync them.

<h2>Implementation</h2>
The most important part would be to write an Online Accounts module so you can setup Nextcloud to begin with.
It needs to be evaluated how setting up Nextcloud in there could affect KDE-PIM (ie. automatically set up Nextcloud calendar in Akonadi etc or vice-versa, if it's already setup in KDE-PIM migrate that to Online Accounts) and to automatically set up the calendar in Plasma.

What's next is, of course, to get to work. If you're interested in getting involved, let us know!