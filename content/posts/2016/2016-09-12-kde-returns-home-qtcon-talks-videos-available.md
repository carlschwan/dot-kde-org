---
title: "KDE Returns Home, QtCon Talks Videos Available"
date:    2016-09-12
authors:
  - "jriddell"
slug:    kde-returns-home-qtcon-talks-videos-available
comments:
  - subject: "Kodi add-on"
    date: 2016-09-13
    body: "<p>I've created an add-on to be able to view these presentations directly from Kodi. I've created a <a title=\"forum topic\" href=\"https://forum.kde.org/viewtopic.php?f=15&amp;t=136005\">forum topic</a> with more information. I hope this makes it easier for people to view these presentations.</p>"
    author: "Arucard"
---
KDE has finished its fantastic week, celebrating 20 years of hacking and freedom fighting together with Qt, VLC and FSFE in Berlin.  We finished our week with a fun day trip to Pfaueninsel, Berlin's Peacock Island.

Videos from many of the talks are <a href="http://files.kde.org/akademy/2016/">now available to download</a> with the rest being added in the coming weeks.  They are also linked from the <a href="https://conf.qtcon.org/en/qtcon/public/schedule">conference program</a> with slides, where available.

Many thanks to the organisers of the conference.  We have now returned to our homes around the world to implement our plans for the next 20 years of being the best community for end-user software.

<div style="padding: 1ex; margin: 1ex; border: 1px solid grey; width: 515px">
<img src="/sites/dot.kde.org/files/dsc04299.jpg" /><br />
<img src="/sites/dot.kde.org/files/dsc04348.jpg" /><br />Peacock Island</div> 
<!--break-->