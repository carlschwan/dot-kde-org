---
title: "KDE Interview Questions - Riccardo Iaconelli"
date:    2016-02-07
authors:
  - "jriddell"
slug:    kde-interview-questions-riccardo-iaconelli
comments:
  - subject: "An example to all of us"
    date: 2016-02-16
    body: "<p>What a great attiude to have!&nbsp;</p><p>It is that kind of attitude and community-based thinking that first attracted to me to the world of Free Software.</p><p>Thank you for everything you have done Ricarrdo</p>"
    author: "Gonzalo Porcel"
---
<a href="/sites/dot.kde.org/files/19966926396_0c1c95203e_b.jpg" title="IMG_6257"><img src="/sites/dot.kde.org/files/19966926396_0c1c95203e_n.jpg" width="256" height="320" alt="IMG_6257" style="float: right; padding: 2ex"></a>

Following his talk at FOSDEM last weekend, we present an interview with WikiToLearn founder Riccardo Iaconelli by Google Code-in student Stanford.

<b>What do you do for a living?</b>
 
I am a student enrolled in the Master degree of Physics at the University of Milano-Bicocca.
<!--break-->
<b>What do you do for KDE?</b>
 
Currently, I am the maintainer of WikiToLearn, working on all the parts of the project where is needed, but mostly on the promotion/networking side. I deliver talks and presentations, and I am in charge of getting in touch with excellent academic institutions that could partner with us.
In the past... well, I have been doing thousands of things! :-) I have been a core developer of Plasma, writing the first plasmoids, a core developer and a designer of Oxygen (working on the theme, window decoration, cursor theme, icons, wallpapers...) and many more things (from kdelibs to games to PIM). Probably the major work (outside these big projects) I am most proud of the complete UI redesign (and implementation) of Amarok in QML. It was sexy, but unfortunately it was never released, due to a decision of the maintainers.
 
<b>How did you get into computer programming?</b>
 
I was 9 and I wanted to build my website about skating. So I started to learn HTML and JavaScript. And then PHP. And then I decided I was going to be an hacker, when I grew up, so I had to study many more languages. I still have to finish that website...
 
<b>Do you have any advice for people who would like to pursue computer programming as a major?</b>
 
Definitely. Here's my take: learn by doing, and always get great mentors to review your work. Get your hands dirty and teach yourself code. Try, fail, try harder and iterate. And don't stop until you get perfect. Aim for elegance. Learn everything you can from the world and your peers, and never stop doing that. The challenge is with yourself, not with anyone else. How much better than can you be? :-)
 
<b>Who is your role model, and why?</b>
 
This is a tough question :-) There was not a single role model, but I had the luck to find many great mentors within KDE. In general, I try to simply be attentive to other people, trying to find out what I like in what they do, and what I can learn from them. Once I see it, I start to apply those elements in my life, whether it's a personal lifestyle choice, a technical decision or a way to relate to others.
 
<b>What are some ways you motivate yourself?</b>
 
I always remind myself why I am doing what I am doing. I believe in a free world; I believe in the power of decentralization. I believe that the sum of our collective minds and efforts is greater than what any of us alone can achieve. And I want to have fun while doing what I am doing.
 
<b>What are some of your future goals for your involvement with KDE?</b>
 
I have been a KDE developer for half of my life. I want to continue see this amazing community to flourish, and I want to play my part in the first project that I maintain, by adding yet another global success to our portfolio. And I think that with WikiToLearn we have all the right cards in the hand to achieve that. :-)

