---
title: "KDE's Kirigami UI Framework Gets its First Public Release"
date:    2016-08-10
authors:
  - "thomaspfeiffer"
slug:    kdes-kirigami-ui-framework-gets-its-first-public-release
comments:
  - subject: "Hamburger controls"
    date: 2016-08-10
    body: "<p>The framework is certainly interesting for a number of reasons.</p><p>I must question the hamburger/menu button positioning choice though. &nbsp;It may seem like an interesting option for ergonomics on mobile since it places access to the buttons closer to the bottom of the screen, but floating buttons will always cause some problems with layouts because they can obscure content. &nbsp;In this respect, the left side of the screen is significantly worse than the right side (eg - material floating action buttons, which are also less of a problem because they're only displayed contextually) for left-to-right languages, since that's where text is usually aligned. &nbsp;Additionally, since the appbar/menubar is already a staple component of app design, the floating hamburger button is actually just wasting screen real-estate, and on mobile the swipe mechanic is often more convenient anyway (when well implemented). &nbsp;Finally, because the interactive elements in the drawer are aligned top-to-bottom, any ergonomic gains are immediately lost by the need to shift to the top of the screen to interact with the drawer contents.</p><p>I can appreciate a desire for differentiation, but I'm not convinced by this choice.</p>"
    author: "pdf"
---
<div><img style="display:block; margin-left:auto; margin-right:auto; border: 1px solid grey" src="/sites/dot.kde.org/files/kirigami.png" /></div>
Kirigami, KDE’s lightweight user interface framework for mobile and convergent applications, which was first announced in March, is now <a href="https://techbase.kde.org/Kirigami">publicly released</a>! This framework allows Qt developers to easily create applications that run on most major mobile and desktop platforms without modification (though adapted user interfaces for different form-factors are supported and recommended for optimal user experience). It extends the touch-friendly <a href="http://doc.qt.io/qt-5/qtquickcontrols-index.html">Qt Quick Controls</a> with larger application building blocks, following the design philosophy laid out in the <a href="https://community.kde.org/KDE_Visual_Design_Group/KirigamiHIG/">Kirigami Human Interface Guidelines</a>.

KDE has a long tradition of providing user interface components beyond the basics that are offered in Qt itself. With <a href="http://api.kde.org/frameworks-api/apidox-frameworks/frameworks5-apidocs/">KDE Frameworks 5</a>, these have become more easily available for Qt developers who are not part of KDE. Now, with KDE’s focus expanding beyond desktop and laptop computers into the mobile and embedded sector, our QWidgets-based components alone are not sufficient anymore. In order to allow developers to easily create Qt-based applications that run on any major mobile or desktop operating system (including our very own existing Plasma Desktop and upcoming <a href="http://plasma-mobile.org/">Plasma Mobile</a>, of course), we have created a framework that extends Qt Quick Controls: Welcome Kirigami!

Kirigami is not just a set of components, it is also a philosophy that defines precise UI/UX patterns. It allows developers to quickly develop intuitive and consistent apps that provide a great user experience. Some concepts are:

<div><div style="margin-left: auto; margin-right: auto; width: 820px"><img style="width:30%; margin:10px; border: 1px solid grey" src="http://dot.kde.org/sites/dot.kde.org/files/Global_Drawer.png" alt="Global Drawer"/><img style="width:30%; margin:10px; border: 1px solid grey" src="http://dot.kde.org/sites/dot.kde.org/files/Context_Drawer_0.png" alt="Context Drawer" /><img style="width:30%; margin:10px; border: 1px solid grey" src="http://dot.kde.org/sites/dot.kde.org/files/Example_Column.png" alt="Column-based Navigation" /></div></div>

<ul>
    <li> Actions are available in two drawers and additionally through some shortcuts (buttons or swipes)</li>
    <li> Actions and options are distinguished into global ones and contextual ones, put in two different drawers in the opposite vertical sides of the screen</li>
    <li> The app’s content is organized in pages that you can browse through with horizontal swipes.</li>
</ul>

The Kirigami Components for smartphones are optimized to allow easy navigation and interaction with just one hand, making it ideal for using applications casually “on the move”. Kirigami is not only for smartphone applications. It will allow to create convergent applications, which are not simply the exact same user interface scaled to different sizes, but morphing between optimized interfaces based on the input method and screen size, changing as the context changes (e.g. flipping over a convertible, docking a phone). Another important concept is non-invasive pop-ups to undo an action, rather than confirmation dialogs.

<img style="display:block; width:800px; margin-left: auto; margin-right: auto; border: 1px solid grey" src="http://dot.kde.org/sites/dot.kde.org/files/Desktop.png" alt="An example of a desktop UI" />

Why the name Kirigami? <a href="https://en.wikipedia.org/wiki/Kirigami">Kirigami</a> is a Japanese art of papercraft. It is derived from origami, but adds cutting as an additional technique to folding. The reason we chose it as the name for our framework is that different layers or “sheets” in the UI are an important element in its design philosophy. Navigating through screens and the pop up of the drawers are like flicking through sheets of paper.

<div style="float:right; margin-left:10px"><a href="/sites/dot.kde.org/files/Subsurface-mobile.jpg"><img src="/sites/dot.kde.org/files/Subsurface-mobile_small.jpg" /></a></div>
The first real-world application implemented using Kirigami components, <a href="https://subsurface-divelog.org/2016/03/announcing-subsurface-mobile-for-android/">Subsurface-mobile</a>, is available for Android, a version for iOS is currently in the works – sharing most of their code. The Subsurface mobile team (which is lead by VMware’s Chief Open Source Officer Dirk Hohndel and has a certain Linus Torvalds as one of their core contributors) and their group of enthusiastic beta testers have worked closely with the developers and designers behind Kirigami to improve both the framework and the application based on their real-world experiences.

The second Kirigami-based application to be released, <a href="http://peruse.kde.org">the comic book reader Peruse</a>, had its initial release in June, and was made available on both desktop Linux and Windows as well as KDE’s own Plasma Mobile. The application targets both touch based devices and the touchless desktops and laptops that are still very common, and as with Subsurface above, Kirigami has allowed for the majority of the code to be shared between those two versions. When asked about the experience, the main developer of Peruse, Dan Leinir Turthra Jensen, stated that “developing with Kirigami has been really exciting, and the team behind it is very responsive to suggestions and reports of any issues in the components.”

Kirigami currently officially supports Android, Desktop GNU/Linux (both X11 and Wayland), Windows, and the upcoming Plasma Mobile. iOS support is currently in an experimental stage, support for Ubuntu Touch is being worked on.
The plan is to eventually become part of KDE Frameworks 5, but is currently released standalone in KDE Extragear. Since it is aimed to be a Tier 1 framework, it has no other dependencies apart from Qt, and therefore will not increase your application’s size any more than necessary.
Kirigami is relatively easy to port to new platforms. If you'd like to see support for a platform not mentioned here, please get in touch with the Kirigami team and they will be glad to help you get Kirigami to work there.

You can find links to the code, API documentation and contact details on the <a href="https://techbase.kde.org/Kirigami">Kirigami Techbase page</a>.