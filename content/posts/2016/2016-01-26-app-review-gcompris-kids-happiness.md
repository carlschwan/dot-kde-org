---
title: "App Review of GCompris: Kids' Happiness"
date:    2016-01-26
authors:
  - "jriddell"
slug:    app-review-gcompris-kids-happiness
comments:
  - subject: "GCompris rocks"
    date: 2016-01-26
    body: "<p>Thanks a lot for your review. I aso believe GCompris offers more and better than any other children application, well this is not always true but we then consider it a bug.</p><p>GCompris on iPad is close to reality, one version has been uploaded on the Apple store and I just opened a beta test, mail me if you want to test it.</p>"
    author: "Bruno Coudoin"
  - subject: "Q:"
    date: 2016-01-27
    body: "<p>What if my child not speak english</p>"
    author: "Niki"
  - subject: "A package for Kubuntu?"
    date: 2016-01-27
    body: "<p>Great news, but would you be so kind as to package it for Kubuntu?</p>"
    author: "dd"
  - subject: "F-droid"
    date: 2016-01-27
    body: "<p>Is it available in any alternative app-store for Android, or as a .pkg download?</p>"
    author: "Rafael Alisson Schipiura"
  - subject: "A: "
    date: 2016-01-29
    body: "<p>Hi,</p><p>Gcompris is translated in a lot of languages thanks to the KDE translation teams: http://l10n.kde.org/stats/gui/stable-kf5/po/gcompris_qt.po/. You can change it in the configuration.</p><p>If the translation for your language is not done/complete and you feel confident about contributing, you can contact the translator lists for your language to ask them how to contribute.</p><p>Johnny</p>"
    author: "Johnny Jazeix"
  - subject: "I am pretty sure that the"
    date: 2016-01-31
    body: "I am pretty sure that the program itself as well as its material are translated the same way all other programs in KDE are, so if you generally get KDE applications in your language you should be also getting a translated GCompris"
    author: "kevkrammer"
  - subject: "apk download"
    date: 2016-02-02
    body: "<p>Hi,</p><p>unfortunetaly, it is not packaged in F-droid (Qt is not handled if I'm not wrong: https://f-droid.org/forums/topic/gcompris/).</p><p>You can find the latest apk directly at http://gcompris.net/download/beta/GCompris-Android-release-signed-aligned-armeabi-dl-0.52.apk</p>"
    author: "Johnny Jazeix"
  - subject: "ubuntu/debian"
    date: 2016-02-02
    body: "<p>Hi;,</p><p>we are actually in discussion with a debian packager. Once it will be done, it should automatically be in kubuntu. We also have auto extractible packages: http://gcompris.net/download/beta/gcompris-qt-x86-0.50-Linux.sh and http://gcompris.net/download/beta/gcompris-qt-x86_64-0.50-Linux.sh for now.</p><p>Johnny</p>"
    author: "Johnny Jazeix"
---
<p>Our series of articles by Google Code In students continues with a review of educational applications GCompris by Sergey Popov.</p>

<p>If you have children, you know how hard it is to make a child happy and interested in something for a long time. But there is an easy way to do that: show them GCompris. It is a really great game set for children 2-10 years old and they surely will like it. You may ask, if GCompris is really so good, and I would answer you "Yes". And that is not a joke. Here are some proofs of that. But, you know, nothing is ideal, so I will also mention its bad sides (unfortunately, they are present too).</p>

<div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/gcompris-on-android_0.png" width="181" height="290" /><br />GCompris on Android</div> 

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/gcompris-on-pc_0.png" width="516" height="290"  /><br />GCompris on PC</div> 

<br clear="all" />

<p>To start, I will show you some of the advantages:</p><ul>
<li>It is totally opensource: What could be better that a huge, excellent, full of cool features and opensource application? Probably nothing.</li>
<li>Easily customizable: As result of the previous paragraph, it is a really good feature, which most alternatives doesn't have.</li>
<li>A lot of activities: More than 120 of interesting, beautifully drawn and useful games for you child - that is really amazing!</li>
<li>Portable: Going on a trip with a little kid? No problems, since you can use GCompris even on Android!</li>
<li>Useful for children: GCompris activities train children in arithmetics, drawing, logic, using a computer and so on.</li>
</ul>

<p>But there are also still some disadvantages:</p><ul>
<li>No iOS support: That's it, GCompris currently does not support iOS. But that will be fixed very soon!</li>
<li>No administration panel: It existed in the legacy, GTK+ version but in the Qt one it is not implemented yet.</li>
<li>No ability to separate users / groups: As result of the previous paragraph, you cannot set two different activity sets for John and Joey, for example.</li>
</ul>

<p>There are not a lot of alternatives to GCompris. There are Easybits Magic Desktop, Timez Attack and Sebran's ABC. But they are not as cool as GCompris:<p><ul>
<li>Easybits Magic Desktop is not as customizable - you cannot change anything in its code.</li>
<li>Timez Attack teaches children only arithmetic operations and nothing else.</li>
<li>Sebran's ABC's interface is not great and it has much fewer activities.</li>
<li>None of them support Linux, Mac OS X and Android but GCompris does.</li>
</ul>

<p>Recently I added more levels to some activities, fixed a bug, created more bonus characters and updated documentation. And my little brother really likes GCompris - that is his favorite program on my PC :).
So, I think GCompris is <span style="text-decoration:line-through">one of</span> the best educational application for kids. What's your opinion about that?<p>