---
title: "KDE Arrives in Berlin, Elects Thomas Pfeiffer to the e.V. Board"
date:    2016-09-01
authors:
  - "jriddell"
slug:    kde-arrives-berlin-elects-thomas-pfeiffer-ev-board
---
<div style="width: 500px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://www.flickr.com/photos/jriddell/29096911400/"><img width="500" heigh="251" src="/sites/dot.kde.org/files/29096911400_20afa2f754.jpg" /></a><br />KDE meets, chats and hacks</div> 
Today KDE has been arriving in Berlin for Akademy, our annual meeting, which is year is part of the larger QtCon conference.  This year we are teaming up with KDAB to gather together with the wider community of Qt developers for the first time, which is a major opportunity to share experiences between the open source and the commercial worlds.  Also at the gathering are the VLC developers. VLC is one of the most successful open source projects successfully reaching out to users on all platforms and is a project we have long cooperated with.  And the Free Software Foundation Europe will be brining the important political edge to our talks.
<!--break-->
<h2>20 Quotes for 20 Years, What are you looking forward to at this QtCon Akademy?</h2>

At this Akademy we celebrate 20 years of KDE, so ahead of QtCon 2016, we went around asking 20 people what they are looking forward to the most this Akademy.

<p style="padding-right: 100px">"It was my first KDE ev AGM today and  I'm excited for the talk I'm giving on plasma mobile. I'm also hosting and joining multiple bofs; so excited about that!" - <b>Bhushan Shah</b> with KDE since 4 years</p>

<p style="text-align: right; padding-left: 100px">"I'm excited that Kubuntu people are meeting face to face, and working together with Debian and Neon." - <b>Valorie Zimmerman</b>, KDE'ian since 7 years</p>

<p style="padding-right: 100px">"Excited for the hallway track and to know whatever's been going on and catching up with all the stuff, and not just what is presented." - <b>Sune Vuorela</b>, contributing to KDE since 11 years</p>

<p style="text-align: right; padding-left: 100px">"Looking forward to the Keynote by Julia Reda because I think it is very important that people in the European parilament understand digital issues and protect digtal consumer rights. I also look forward to meeting someone from Google and grilling them regarding Google Doc's standards of interoperability." - <b>Jos Van den Oever</b> - been with KDE since 11 years</p>

<p style="padding-right: 100px">"I'm excited about the Akademy awards, especially the most interesting one - the most valuable contribution award."- <b>David Faure</b>, been a member of KDE since 19 years</p>

<p style="text-align: right; padding-left: 100px">"To introduce my KDE friends to Slavic cuisine" - <b>Ivan Čukić</b> been around KDE for 10 years</p>

<p style="padding-right: 100px">"Meeting the contributors. It's been almost a yearly appointment for me now. I like to keep a track of what's going on." - <b>Luciano Montenaro</b> - a KDE member since 15 years.</p>

<p style="text-align: right; padding-left: 100px">"The Conference this year is really huge. Tons of tracks this year and it will be really interesting to see tracks from other communities. Attending the BoFs and to solve things more quickly face to face is also something I'm looking forward to." - working for KDE since 12 years, <b>Luigi Toscano</b></p>

<p style="padding-right: 100px">"Meeting old friends, and hearing interesting talks"  - <b>Ingo Klöcker</b> been with KDE since 17 years</p>

<p style="padding-right: 100px">"Meeting and collaborating with people from lower levels of the stack." - 12 years at KDE, <b>Marco Martin</b></p>

<p style="text-align: right; padding-left: 100px">"I have no idea, surprise me." <b>Boudhayan Gupta</b> - 1.7 years at KDE</p>

<p style="padding-right: 100px">"I want KDE to surprise me and the entire world." - with the KDE community since 9 years <b>José Milian Soto</b></p>
 
<p style="text-align: right; padding-left: 100px">"I'm looking forward to writing KDE dot articles to spread the awesomeness of KDE" been around with KDE since 17 years <b>Jonathan Riddell</b></p>

<p style="padding-right: 100px">"I'm looking forward to collaborate with new people, brainstorm on promo ideas and learn a lot of new things this time!" <b>Devaja</b>, With KDE since 4 years</p>

<p style="text-align: right; padding-left: 100px">"Looking forward to be re-energised and positively charged" - <b>Sebastian Kügler</b> been at KDE for 11 years</p>

<p style="padding-right: 100px">"Honestly, meeting all the community members." <b>Pinak Ahuja</b> with KDE for 2 years</p>

<p style="text-align: right; padding-left: 100px">"Meeting friends, getting to bring us and the other projects and organisations closer together and hearing about all the exciting work people have been doing over the year all around at KDE. So much is happening. You only truly appreciate that at Akademy." <b>Lydia Pintscher</b> with KDE for 10 years</p>

<p style="padding-right: 100px">"Excited about hanging out with my friends since I haven't met them in a long time." <b>Vishesh Handa</b> contributing to KDE for 6 years</p>

<p style="text-align: right; padding-left: 100px">"Looking forward to hearing some nice talks and meeting old friends like usual. And since we are in Germany, having good beer" <b>Jan Grülich</b> working with KDE for 5 years</p>

<p style="padding-right: 100px">"I don't know what to expect from Akademy. That's the beauty of the conference, you never know what's going to happen" <b>Helio</b> who has been working for KDE since 15 years</p>


<h2>KDE e.V. AGM</h2>

Today some of the KDE community met for the annual meeting of KDE e.V., the legal body which holds the finances we rely upon.  Board president Lydia presented the activities of the board over the previous year including the new Advisory Board made up of members of sponsors and interested parties.  They highlighted the 9 sprints and 4 conferences that were organised by members of KDE. We heard reports from the Working Groups for Community, Sys Admins and Finance.  Our treasurer Marta gave a report of finances and we were pleased at having a working cashflow as well as a surplus in the last year. Three candidates put themselves forward for the role of the Board member which became vacant leading to a lively debate about what they wanted to work on and where KDE e.V. was going.  It was a close election which saw VDG member and visionary Thomas Pfeiffer elected to the board.
<div style="width: 800px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/29276303942_306daa82c0_c.jpg"><img width="800" heigh="534" src="/sites/dot.kde.org/files/29276303942_306daa82c0_c.jpg" /></a><br />Your New Board with Sandro, Aleix, Thomas, Lydia, Marta</div> 


