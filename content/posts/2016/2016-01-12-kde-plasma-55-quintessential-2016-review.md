---
title: "KDE Plasma 5.5: The Quintessential 2016 Review"
date:    2016-01-12
authors:
  - "jriddell"
slug:    kde-plasma-55-quintessential-2016-review
---
<div style="float: right; border: 1px solid grey; padding: 1ex; margin: 1ex"><a href="http://www.phoronix.com/scan.php?page=article&item=kde-2016-review&num=1"><img src="/sites/dot.kde.org/files/intro-splash.png" width="200" height="123" /></a></div>

KDE contributor Ken Vermette has written <a href="http://www.phoronix.com/scan.php?page=article&item=kde-2016-review&num=1">The Quintessential 2016 Review of Plasma 5.5</a> which was released last month, a 9 page cover of the good, the bad and the beautiful. 

<blockquote>
Plasma 5.5 marks the beginning of the lifecycle where the vast majority of people will find it capable of serving as their workhorse environment. While at the beginning of the year Plasma 5.2 was exciting but a little wobbly, 5.5 has seen enough iteration to mature and close the significant issues found by the majority of early adopters.

As of 2016 Plasma 5.5 has evolved well beyond where Plasma 4 ended while showing no signs of slowing down in the slightest, and I confidently recommend trying it out.
</blockquote>
<!--break-->
