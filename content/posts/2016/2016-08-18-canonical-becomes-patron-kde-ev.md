---
title: "Canonical Becomes a Patron of KDE e.V."
date:    2016-08-18
authors:
  - "bshah"
slug:    canonical-becomes-patron-kde-ev
comments:
  - subject: "State of relationship between Mir and KDE"
    date: 2016-08-25
    body: "<p>There was a time that KDE development was planning to ignore Mir. &nbsp;Has this changed, or might it, with the this new sponsor?</p>"
    author: "Connie"
---
<div style="float: right; padding: 1ex"><img src="/sites/dot.kde.org/files/canonical.png" alt="Canonical" /></div>
KDE and <a href="http://www.canonical.com/">Canonical</a>'s Ubuntu have collaborated for years. Today we celebrate the extension of this collaboration with the addition of Canonical to the <a href="https://ev.kde.org/supporting-members.php">KDE Patrons family</a>, as part of the corporate membership program.

<!--break-->

As explained by Michael Hall, Ubuntu Community Manager,

"From its very beginning Canonical has been a major investor in the Free Software desktop. We work with PC manufacturers such as Dell, HP and Lenovo to ship the best Free Software to millions of desktop users worldwide. Becoming a corporate patron is the continuation of Canonical's decade-long support for KDE and Kubuntu as important members of the Ubuntu family.
Canonical will be working with the KDE community to keep making the latest KDE technology available to Ubuntu and Kubuntu users, and expanding that into making Snap packages of KDE frameworks and applications that are easily installable by users of any Linux desktop. We will benefit from sharing knowledge, experience and code around Qt and Qt packaging, pushing the advancement of QML and increasing its adoption in Unity and Ubuntu native applications alongside KDE's own work towards convergence."

Aleix Pol Gonzalez, Vice-President of <a href="https://ev.kde.org">KDE e.V.</a> stated,

"We are excited to have Canonical supporting KDE's work. It is important that we make a continuous effort to work together and this is the best way to continue offering a thriving free and open development platform to build upon. We are confident that this collaboration can be very beneficial for the overall GNU/Linux community and ecosystem."

Canonical will join KDE e.V.'s other Patrons The Qt Company, SuSE, Google and Blue Systems to continue to support Free Software and KDE development through the KDE e.V.