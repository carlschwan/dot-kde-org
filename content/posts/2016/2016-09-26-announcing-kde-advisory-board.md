---
title: "Announcing the KDE Advisory Board"
date:    2016-09-26
authors:
  - "thomaspfeiffer"
slug:    announcing-kde-advisory-board
---
<div style="float:right; margin-left:10px"><img src="/sites/dot.kde.org/files/advisory_board.jpg" alt="Illustration of a meeting" /></div>
With KDE having grown to a large and central Free Software community over the last 20 years, our interactions with other organizations have become increasingly important for us. KDE software is available on several platforms, is shipped by numerous distributions large and small, and KDE has become the go-to Free Software community when it comes to Qt. In addition to those who cooperate with KDE on a technical level, organizations which fight for the same vision as ours are our natural allies as well.</p>

<p>To put these alliances on a more formal level, the KDE e.V. hereby introduces the KDE e.V. Advisory Board as a means to offer a space for communication between organizations which are allied with KDE, from both the corporate and the non-profit worlds.</p>

<p>One of the core goals of the Advisory Board is to provide KDE with insights into the needs of the various organizations that surround us. We are very aware that we need the ability to combine our efforts for greater impact and the only way we can do that is by adopting a more diverse view from outside of our organization on topics that are relevant to us. This will allow all of us to benefit from one another's experience.</p>

<p>"KDE's vision of a world in which everyone has control over their digital life and enjoys freedom and privacy cannot be realized by KDE alone. We need strong allies. I am therefore excited that we are formalizing our relationship with a number of these strong allies with the Advisory Board and what that will bring for them, for KDE, our users and Free Software as a whole." says Lydia Pintscher, President of KDE e.V.</p>

<p>We are proud to already announce the first members of the Advisory Board:</p>

<ul>
<li><p>From its very beginning <a href="http://www.canonical.com">Canonical</a> has been a major investor in the Free Software desktop. They work with PC manufacturers such as Dell, HP and Lenovo to ship the best Free Software to millions of desktop users worldwide.</p>

<p>Canonical will be working with the KDE community to keep making the latest KDE technology available to Ubuntu and Kubuntu users, and expanding that into making Snap packages of KDE frameworks and applications that are easily installable by users of any Linux desktop.</p></li>

<li><p><a href="https://www.suse.com/">SUSE</a>, a pioneer in open source software, provides reliable, interoperable Linux, cloud infrastructure and storage solutions that  give enterprises greater control and flexibility. More than 20 years of engineering excellence, exceptional service and an unrivaled partner ecosystem provide the basis for SUSE Linux Enterprise products as well as supporting the openSUSE community which produces the Tumbleweed and Leap community-driven distributions.
It was natural for SUSE &amp; openSUSE, being long standing joint KDE patrons, to join the KDE Advisory Board. This new channel will foster the already good communications between the KDE community and SUSE/openSUSE, which will bring mutual benefits for all. </p></li>

<li><p>Following its motto "Liberating Technology", <a href="http://www.blue-systems.com/">Blue Systems</a> not only produces the two Linux distributions Maui and Netrunner, but also invests directly in several KDE projects, such as Plasma (Desktop and Mobile) or KDE neon. Being part of the Advisory Board further enhances the collaboration between Blue Systems and KDE on all levels</p>.</li>

<li><p>Founded in 1998, the <a href="https://opensource.org">Open Source Initiative</a> protects and promotes open source by providing a foundation for community success. It champions open source in society through education, infrastructure and collaboration. The OSI is a California public benefit corporation, with 501(c)(3) tax-exempt status.</p>

<p>Free and Open Source software projects are developed by diverse and dynamic groups that not only share common goals in the development of technologies, but share common values in the creation of communities. The OSI is joining KDE's advisory board to fulfill their mission of building bridges among different constituencies in the open source community, a critical factor to ensure the ongoing success of both Free and Open Source projects and contributors that support them.</p></li>

<li><p>FOSS Nigeria is a non-profit organization with the aim of educating Nigerians on KDE and other FOSS applications.</p>

<p>The first contact between FOSS Nigeria was In 2009 when Adrian de Groot and Jonathan Riddell had a talk about KDE and Qt projects before more than 300 participants during the first Nigerian international conference on free and open source software, and Frederik Gladhorn joined Adrian de Groot in the following year's conference. </p>

<p>KDE e.V helped in making FOSSng and the KDE Nigeria user group what it is today. Participation of people like Adrian de Groot, Jonathan Riddell and Fredrick Gladhorn plays a vital role for the major breakthrough behind the spread of KDE in Nigeria from 2009 to date.</p>

<p>Every two months the KDE Nigeria user group meets and discusses the future of KDE in Nigeria. The organisers of FOSS Nigeria 2017 are proposing that 55% of the conference papers will focus on KDE and Qt.</p></li>

<li><p>The <a href="http://fsfe.org">Free Software Foundation Europe</a>'s aim is to help people control technology instead of the other way around. KDE is an <a href="https://fsfe.org/associates/associates.en.html">associated organisation of the FSFE</a> since 2006.</p>

<p>Since 2009, KDE and the FSFE share rooms for their offices in Berlin, and regularly exchange ideas how to improve the work for software freedom. "It just felt natural and the right thing to do for the FSFE to join the advisory board after we have been asked if we would be interested.", says Matthias Kirschner, President of the FSFE.</p></li>
<li><p><a href="http://www.april.org/en">April</a> is the main French advocacy association which has been promoting and defending Free Software in France and Europe since 1996.  April brings together several thousand individual members and a few hundred organisations (businesses, nonprofits, local governments, educational institutions).</p>

<p>Through the work of its volunteers and permanent staff, April is able to carry out a number of different campaigns to defend the freedoms of computer users. You can join April, or support it, by making a donation.</p></li>
<li><p>The Document Foundation is the home of LibreOffice, the world's most widely-used free office suite, and the Document Liberation Project, a community of developers focused on providing powerful tools for the conversion of proprietary documents into ODF files. It was created in the belief that the culture born of an independent foundation brings out the best in contributors and will deliver the best software for users.</p>

<p>"We share with KDE the same commitment to Free Software and open standards, and we have both invested a significant amount of time for the development and the growth of the Open Document Format", says Thorsten Behrens, Director at <a href="https://www.documentfoundation.org/">The Document Foundation</a>. "By joining KDE Advisory Board, we want to underline potential synergies between large free software projects, to grow the ecosystem and improve the quality of end user solutions".</p></li>
<li><p>In 2003 the city administration of Munich, one of the largest cities in Germany, evaluated the migration to Open-Source desktop software. Within the <a href="https://en.wikipedia.org/wiki/LiMux">LiMux</a> project from 2005 until 2013 around 18,000 desktop computers were migrated from Microsoft Windows to a Linux-based desktop with KDE and OpenOffice.org. Remaining Windows-based desktop computers for special use cases and applications are equipped as much as possible with Open-Source applications like Firefox and Thunderbird. In the beginning of the project, KDE 3.x was chosen as the desktop environment for the LiMux client to ensure a smooth transition in handling of graphical user interfaces for the users who were used to the, at that time, established Windows 2000 desktop. Current LiMux client versions ship with KDE Plasma 4.x desktop. 

The desktop environment and its applications play a major role in the user experience of the desktop system. Many of the around 44,000 employees of the city administration use KDE applications for their work on a daily basis. Some of them are more and some are less computer-oriented and therefore the preconfiguration of the desktop is rather conservative and geared towards continuity. </p>

<p>The city of Munich has often the need to customize the system-wide standard configuration of the desktop environment and its applications. In order to communicate and discuss such issues and with the KDE community and to find solutions, developers from Munich regularly attend Akademy. Our goal is to make, together with the KDE community, the Plasma Desktop suitable for a large enterprise environment.</p></li>
<li><p>The <a href="http://fsf.org">Free Software Foundation</a> is a 30-year-old nonprofit with a worldwide mission to promote computer user freedom. They defend the rights of all software users. KDE has an important part to play in achieving that mission, because of its excellent work creating a free and user-friendly application environment. KDE's free, powerful, and exceptionally usable software both demonstrates that a future world of everyday computer users using entirely free software is entirely possible, and helps us get there. We're looking forward to closer collaboration and are honored to be part of the advisory board.</p></li>
</ul>

<p>If you would like an organization you're with to be part of the KDE e.V. Advisory Board, you can read more about the program here:
https://ev.kde.org/advisoryboard.php</p>

<p>For more information, don't hesitate to send an e-mail to kde-ev-board@kde.org</p>