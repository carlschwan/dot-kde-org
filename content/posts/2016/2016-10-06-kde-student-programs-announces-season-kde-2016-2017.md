---
title: "KDE Student Programs announces Season of KDE 2016-2017"
date:    2016-10-06
authors:
  - "valoriez"
slug:    kde-student-programs-announces-season-kde-2016-2017
---
KDE Student Programs announces the 2016-2017 Season of KDE for those who want to participate in mentored projects which enhance KDE in some way. Projects from past Seasons of KDE include new application features, the KDE Continuous Integration system, new reporting for developers, as well as a web framework, porting and a plethora of other work.

Successful mentees earn a certificate of completion along with &nbsp;a very cool t-shirt and other goodies. Any person who wants to complete a project is eligible to enter.

Those who want to mentor are asked to add ideas here: https://community.kde.org/SoK/Ideas/2016.

Students are asked to begin discussion about their ideas or those on the various KDE mail lists and IRC channels even before applications open. 

The schedule this year will be: 
<ul>
<li>7 October to 31 October: Student and mentor applications</li>
<li>1 November: Official coding period begins. Students can start work once mentor and student agree on the project and the timeline</li>
<li>28 February : End of coding period</li>
</ul>

To apply as a mentor or student, please visit https://season.kde.org