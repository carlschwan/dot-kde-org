---
title: "KDE at Augsburger Linux-Tag"
date:    2016-04-19
authors:
  - "broulik"
slug:    kde-augsburger-linux-tag
comments:
  - subject: "Perfectly Crafted"
    date: 2016-04-19
    body: "<p>Hello Friends of KDE world, please allow me to congratulate all the team for your great work, I am a plasma user, and every time that a new version is out is definitely better, I am very happy with plasma 5.6 thank you, team, thank you.</p>"
    author: "Konqi"
---
Kai Uwe Broulik reports from open source conference Augsburger Linux-Tag which happened in Bavaria last weekend.

    On Saturday, 16 April I had the honor of representing KDE at the <a href="http://luga.de/Aktionen/LIT-2016">15. Augsburger Linux-Tag</a>, one of the oldest and largest Linux gatherings in southern Germany.
     
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/ausberg_0.jpg"><img class="size-large wp-image-212" src="/sites/dot.kde.org/files/ausberg-wee_0.jpg" alt="Konqi guarding the KDE booth" width="660" height="495" /></a> Konqi guarding the KDE booth</div> 

     
    It was my first time attending such an event and it was a great experience. With me I had a computer running the latest and greatest <a href="http://neon.kde.org/">KDE neon</a> with <a href="https://www.kde.org/announcements/plasma-5.6.0.php">Plasma 5.6</a> – hooked up to a gorgeous 4K monitor. In addition to that I brought a Nexus 5 running <a href="http://plasma-mobile.org/">Plasma Mobile</a> and an infrared remote control for browsing Plasma Media Center.
    <!--break-->
     
    The place was quite busy and due to lots of talks and presentations going on, people stopped by in batches usually. Feedback on the new Breeze look and feel of Plasma 5 was overwhelmingly positive, kudos to the <a href="https://vdesign.kde.org/">VDG</a> and everyone involved in making this software masterpiece. Critics of our “flat design” were happy to hear that we continue to ship Oxygen style and icons, enabling you to visually turn Plasma 5 into Plasma 4 with ease.
          <br clear="all"/>
<div style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://blog.broulik.de/wp-content/uploads/2016/04/IMG_20160416_1439081.jpg"><img class="wp-image-214 size-medium" src="http://blog.broulik.de/wp-content/uploads/2016/04/IMG_20160416_1439081-300x225.jpg" alt="IMG_20160416_1439081" width="300" height="225" /></a> The common architecture between desktop and phone was easily illustrated by common widgets</div> 
     
    Plasma Mobile did its job well, getting people excited about the possibilities to come: having a full-fledged KRunner on a phone, deep KDE Connect integration, a full Linux-stack underneath, and more.
     
    <a href="https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui">Kirigami</a> UI concepts, especially the ability to go “forward” rather than just back, were well-received and made people look forward to our gesture-driven user interface.
     
    Skeptics of Plasma Mobile were relieved to hear that Plasma Mobile – which is running Wayland rather than X – helped our Wayland adoption on the desktop and resources spent there benefit the entire Plasma stack.
          <br clear="all"/>
<div style="width: 225px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://blog.broulik.de/wp-content/uploads/2016/04/IMG_20160416_1702013-e1460975060905.jpg"><img class="size-medium wp-image-216" src="http://blog.broulik.de/wp-content/uploads/2016/04/IMG_20160416_1702013-e1460975060905-225x300.jpg" alt="Three input concepts, easily served by Plasma technology" width="225" height="300" /></a> Three input concepts, touch, keyboard, few-buton remote control; easily served using Plasma technology</div>

    Augsburg visitors ranged from a 5 year old running across the hallway yelling “how cuuuuute” Konqi is, to an elderly man proudly telling his story about how he recently switched from Windows to Linux and enjoys using Plasma and KDE Applications. I then showed him how easily he could get a full screen application launcher he got used to from his old operating system (Right-click Kickoff → “Alternatives” →  “Application Dashboard”).
     
    There were also quite a few users of alternative desktop environments and window managers who were glad to be able to use KDE software, such as KWin, Kate, Dolphin, outside a full Plasma session.
     <br clear="all"/>
<div style="width: 225px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://blog.broulik.de/wp-content/uploads/2016/04/IMG_20160416_1702148-e1460975016890.jpg"><img class="size-medium wp-image-217" src="http://blog.broulik.de/wp-content/uploads/2016/04/IMG_20160416_1702148-e1460975016890-225x300.jpg" alt="Browsing Plasma Media Center using the input device it was designed for" width="225" height="300" /></a> Browsing Plasma Media Center using the input device it was designed for.</div>

    In the afternoon I came across a guy from LiMux, a project by the city of Munich to migrate their software systems to free and open-source software, who are using Plasma for their desktop computers. He illustrated some of the challenges they’re facing with Plasma in such a large enterprise deployment with a sheer number of novice users. We got invited to the <a href="https://wiki.debian.org/BSP/2016/05/de/Munich">LiMux Hackfest</a> in May where we will work together to prepare Plasma 5 for the requirements they have.
     
    In conclusion, I can say the day was a success: we got lots of comments, feedback, and praise, and I am looking forward to attending this and other similar events in the future.
     
    Special thanks to Ingo Blechschmidt of Hochschule Augsburg for inviting us and Jonathan Riddell for sending me his KDE stand-up display!

