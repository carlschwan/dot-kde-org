---
title: "Randa Meetings 2016 Fundraising Campaign"
date:    2016-06-10
authors:
  - "sandroandrade"
slug:    randa-meetings-2016-fundraising-campaign
---
<a href="https://www.kde.org/fundraisers/randameetings2016/" target="_blank"><img src="https://www.kde.org/fundraisers/randameetings2016/images/fundraising2016-ogimage.png" alt="" /></a>

<h2>About this fundraising campaign</h2>

<p><a href="http://www.kde.org" target="_blank"><strong>KDE</strong></a> is one of the biggest free software communities in the world and has been delivering high quality technology and spreading the principles of hacker culture for nearly two decades. KDE brings together users, developers, maintainers, translators and many more contributors from across six continents and over fifty countries, all of them working with the bonds and spirits of a truthful community.</p>

<p>KDE has a vision:</p>

<div class="alert alert-info text-center"><h5>A world in which everyone has control over their digital life and enjoys freedom and privacy</h5></div>

<p><a href="http://randa-meetings.ch/" target="_blank"><strong>Randa Meetings</strong></a> is the largest sprint organized by KDE. Roughly fifty KDE contributors meet yearly at Swiss alps to enjoy seven days of intense in-person work, pushing KDE technologies forward and discussing how to address the next-generation demands for software systems. One of the biggest challenges we've historically had for KDE technology is that we haven't completely reached the platforms where all of our users work. Aligned with our vision and pursuing the delivery of KDE technologies to every user, Randa Meetings 2016 &#8210 which happens from 12th to 19th June &#8210 will be centered on bringing <strong>KDE technology on every device</strong>.</p>

<p>This fundraising campaign aims at supporting the continuity of KDE efforts, over the year, towards the following goal:</p>
                                            <div class='alert alert-info text-center'>
                                                <h5>KDE technology on every device; be it in your pocket, in your bag or perched on your table; with the clear intention to start delivering software that will empower its users regardless of the platform they use</h5>
                                            </div>

<p>Different teams will be working in this direction at Randa Meetings 2016 and later on. Making software work on the platforms is not everything that's needed. We'll also need to have the infrastructure that makes sure this effort will have continuity over time, maintaining the quality our users deserve. This also means investing quite some effort in the KDE DevOps technology such as the CI (Continuous Integration) system for the different platforms and mechanisms to properly distribute binaries.</p>

<h2>How will we do that?</h2>

<p>Randa Meetings 2016 activities and subsequent efforts will be centered around three important topics:</p>
                                    <ol>
                                        <li>Have a defined set of KDE Applications ported to specific platform of interest.</li>
                                        <li>Come up with a reasonably generic plan to distribute on each platform.</li>
                                        <li>Come up with a sustainability plan for keeping those applications and binaries up to date.</li>
                                    </ol>

<p>For a glimpse of what is to come in the upcoming days, here's a list of applications and platforms that will be tackled by Randa Meetings 2016 participants: <a href="https://marble.kde.org/" target="_blank">Marble</a> on Android and Linux; <a href="https://kdenlive.org/" target="_blank">Kdenlive</a> on Windows; <a href="https://edu.kde.org/applications/s/artikulate" target="_blank">Artikulate</a> on Android; <a href="https://www.calligra.org/" target="_blank">Calligra</a> on Android; <a href="https://inqlude.org/groups/kde-frameworks.html" target="_blank">KDE Frameworks</a> on Android and Windows; <a href="https://kate-editor.org/" target="_blank">Kate</a> on OS X and Windows; <a href="https://www.kdevelop.org/" target="_blank">KDevelop</a> on OS X and Windows; <a href="https://cmollekopf.wordpress.com/2016/03/02/so-what-is-kube-and-who-is-sink/" target="_blank">Kube</a> on Windows, OS X, and Android; <a href="https://community.kde.org/KDEConnect" target="_blank">KDE Connect</a> on Windows; and <a href="https://edu.kde.org/labplot/" target="_blank">LabPlot</a> on OS X and Windows.</p>

<p>As for distribution efforts, we will investigate the use of technologies such as <a href="http://appimage.org/" target="_blank">AppImage</a> and <a href="http://flatpak.org/" target="_blank">Flatpak</a> to build distribution-independent bundled applications.</p>

<h2>Help make this vision come true!</h2>

<p><a href="https://www.kde.org/fundraisers/randameetings2016/" target="_blank">Make your donation</a> or simply help spreading the word!</p>

* The Randa Meetings 2016 fundraising campaign's image on top of this story is a derivative of <a href="https://en.wikipedia.org/wiki/Post-PC_era#/media/File:The_iOS_family_pile_%282012%29.jpg" target="_blank">Blake Patterson's work</a>, used under <a href="http://creativecommons.org/licenses/by/2.0" target="_blank">CC BY 2.0</a>. This work is licensed under <a href="http://creativecommons.org/licenses/by/2.0" target="_blank">CC BY 2.0</a> by <a href="http://sandroandrade.org" target="_blank">Sandro Andrade</a>.