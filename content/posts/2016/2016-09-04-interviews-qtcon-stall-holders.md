---
title: "Interviews with QtCon Stall Holders"
date:    2016-09-04
authors:
  - "jriddell"
slug:    interviews-qtcon-stall-holders
---
KDE Dot News sent its roving reporter Devaja round the stalls at QtCon to ask them what they were promoting and of their experience of the conference.

Think-Cell
<iframe width="560" height="315" src="https://www.youtube.com/embed/J9ar2WXuqco" frameborder="0" allowfullscreen></iframe>

openSUSE
<iframe width="560" height="315" src="https://www.youtube.com/embed/u8NoDZVLop4" frameborder="0" allowfullscreen></iframe>

Pelagicore
<iframe width="560" height="315" src="https://www.youtube.com/embed/IXePnN-rghc" frameborder="0" allowfullscreen></iframe>

Ableton
<iframe width="560" height="315" src="https://www.youtube.com/embed/wSC0ubWaTvs" frameborder="0" allowfullscreen></iframe>

Froglogic
<iframe width="560" height="315" src="https://www.youtube.com/embed/PcsGVqdvN0w" frameborder="0" allowfullscreen></iframe>

Blue Systems
<iframe width="560" height="315" src="https://www.youtube.com/embed/Znw35kOSZIg" frameborder="0" allowfullscreen></iframe>

KDAB
<iframe width="560" height="315" src="https://www.youtube.com/embed/z6D3nFM8KP8" frameborder="0" allowfullscreen></iframe>
<!--break-->
