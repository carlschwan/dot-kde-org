---
title: "KDE Proudly Presents Kirigami UI!"
date:    2016-03-30
authors:
  - "thomaspfeiffer"
slug:    kde-proudly-presents-kirigami-ui
comments:
  - subject: "Misspelling"
    date: 2016-03-31
    body: "<p><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.8px;\">There is a misspelling in this sentence:</span></p><blockquote><p><span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.8px;\">They are aimed to be a Tier 1 framework, meaning they have no other dependencies apart <strong>form</strong> Qt, and therefore won't increase your application's size any more than necessary.</span></p></blockquote><p><span style=\"font-size: small;\">Just wanted to give y'all a heads up.</span></p>"
    author: "Michael Rutherford"
  - subject: "On desktop?"
    date: 2016-03-31
    body: "<p>I would really like to see how an UI implemented with Kirigami looks like on the desktop. How it morphs and stuff. A side-by-side screenshot comparison would be nice :)</p>"
    author: "cmakeshift"
  - subject: "Oops, thank you, fixed!"
    date: 2016-04-02
    body: "Oops, thank you, fixed!"
    author: "thomaspfeiffer"
  - subject: "Desktop components are not ready yet"
    date: 2016-04-03
    body: "At this point, only phone-optimized Kirigami Components exist (we focused on phones first), so we still have to create the desktop-optimized components before we can show the transition of convergent applications properly."
    author: "thomaspfeiffer"
  - subject: "Kirigami info?"
    date: 2016-04-07
    body: "<p>Looks great! But where can I find more info about the Kirigami library? I might like to give it a try.&nbsp;</p>"
    author: "PLJ"
  - subject: "Kinetic scrolling"
    date: 2016-04-07
    body: "<p>Does Kirigrami support optimized kinetic scrolling? When I say optimized, I mean smooth, non choppy kinetic scrolling?</p><p>What tools will we need to build this? Will there be a QtCreator like RAD?</p><p>Back to Kinetic scrolling, it is vital that smooth Kinetic is implemented. We also need smooth animations.</p><p>Is there a swipe / flip gesture support? How complex is the implementation?</p><p>&nbsp;</p>"
    author: "developer"
  - subject: "Convergence overview"
    date: 2016-04-08
    body: "<p>I created an overview of device types on the community wiki to keep track of how convergence might work on different devices and hopefully help in figuring out what's needed to make it work well. I was hoping you could help me with some information about which Kirigami components can/should be used on specific parts of each device type. I think it would be a good way of showing how Kirigami fits in with convergence and should provide some guidance on what is still needed.</p><p>I created a forum topic for feedback here: https://forum.kde.org/viewtopic.php?f=285&amp;t=131170</p><p>And the community wiki page is here: https://community.kde.org/Plasma/Convergence_Overview</p>"
    author: "Arucard"
  - subject: "and found some info, "
    date: 2016-04-13
    body: "<p>Just to let you know , more can be found at https://github.com/KDE/kirigami</p>"
    author: "PLJ"
  - subject: "Porting a Show-Case App"
    date: 2016-04-25
    body: "<p>If you are interested in porting a usefull show-case app like a native calling, short messaging, or network manager app to kirigami, there is a</p><h1>Hacking weekend</h1><pre>4 &amp; 5 June 2016 in Munich, Germany</pre><p>http://lists.goldelico.com/pipermail/community/2016-April/001393.html</p><p>http://projects.goldelico.com/p/openphoenux/page/HackingWeekend2016/</p><p>Ideas, experience, and you are very welcome. The idea is to convert an existing qt apps from the device specific qt-moko distribution (running only the \"GTA04 phone\") into generic qt-5 apps that can run on mobile devces as well as on the deskop (and the \"pyra\" https://pyra-handheld.com/boards/pages/pyra/). See the project page if you need support with travel expences.</p>"
    author: "Chris2"
  - subject: "Is there any tutorial for kirigami"
    date: 2016-09-12
    body: "<p>Is there any tutorial as to how use kirigami to code for android?</p>"
    author: "addictioneer"
  - subject: "a11y"
    date: 2017-01-03
    body: "<p>does it provide any accessibility&nbsp; features?</p>"
    author: "chrys"
---
<div style="text-align:center"><img src="/sites/dot.kde.org/files/kirigami.png" /></div>
KDE has a long tradition of providing user interface components beyond the basics that are offered in Qt itself. With <a href="http://api.kde.org/frameworks-api/apidox-frameworks/frameworks5-apidocs/">KDE Frameworks 5</a>, these have become more easily available for Qt developers who are not part of KDE. Now, with KDE's focus expanding beyond desktop and laptop computers into the mobile and embedded sector, our QWidgets-based components alone are not sufficient anymore. In order to allow developers to easily create Qt-based applications that run on any major mobile or desktop operating system (including our very own existing Plasma Desktop and upcoming <a href="http://plasma-mobile.org/">Plasma Mobile</a>, of course), we have created a framework that extends the touch-friendly <a href="http://doc.qt.io/qt-5/qtquickcontrols-index.html">Qt Quick Controls</a>: Welcome Kirigami UI!

Kirigami UI isn't just a set of components, it's also a philosophy: It defines precise UI/UX patterns to allow developers to quickly develop intuitive and consistent apps that provide a great user experience. Some concepts are: Actions are available in two drawers and additionally through some shortcuts (buttons or swipes); actions and options are distinguished into global ones and contextual ones, put in two different drawers in the opposite vertical sides of the screen; app's content is organized in pages that you can browse through with horizontal swipes. The Kirigami Components for smartphones are optimized to allow easy navigation and interaction with just one hand, making it ideal for using applications casually "on the move". Kirigami UI is not only for smartphone applications, however: It will allow to create convergent applications, which are not simply the exact same user interface scaled to different sizes, but morphing between optimized interfaces based on the input method and screen size, changing as the context changes (e.g. flipping over a convertible, docking a phone). Another important concept is non-invasive pop-ups to undo an action, rather than confirmation dialogs.

As Kirigami aims to seamlessly integrate with the basic sets of controls offered by QtQuick, such as QtQuickcontrols, it does not duplicate the effort to provide essential controls such as buttons and text input fields, but provides ready to use, high level controls to implement its design philosophy and UX guidelines.

Why the name Kirigami? <a href="https://en.wikipedia.org/wiki/Kirigami">Kirigami</a> is a Japanese art of papercraft. It is derived from origami, but adds cutting as an additional technique to folding. The reason we chose it as the name for our UI framework is that different layers or "sheets" in the UI are an important element in its design philosophy. Navigating through screens and the pop up of the drawers are like flicking through sheets of paper.

<div style="float:right; margin-left:10px"><a href="/sites/dot.kde.org/files/Subsurface-mobile.jpg"><img src="/sites/dot.kde.org/files/Subsurface-mobile_small.jpg" /></a></div>
The first real-world application implemented using Kirigami Components, <a href="https://subsurface-divelog.org/2016/03/announcing-subsurface-mobile-for-android/">Subsurface-mobile</a>, has recently been released for Android, a version for iOS is currently in the works, and the cool thing is: The two versions share most of their code! The Subsurface mobile team (which is lead by Intel's Chief Linux and Open Source Technologist Dirk Hohndel and has a certain Linus Torvalds as one of their core contributors) and their group of enthusiastic beta testers have worked closely with the developers and designers behind Kirigami to improve both the framework and the application based on their real-world experiences.

The Kirigami Components are planned to become part of KDE Frameworks 5, but will initially be released stand-alone around the end of April. They are aimed to be a Tier 1 framework, meaning they have no other dependencies apart from Qt, and therefore won't increase your application's size any more than necessary.

Long story short: If you're planning to build a mobile or convergent mobile/desktop application using Qt and would like to give your users easy ways to navigate through pages and access functions, be sure to give Kirigami UI a try!