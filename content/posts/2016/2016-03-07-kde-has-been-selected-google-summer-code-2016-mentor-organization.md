---
title: "KDE has been selected as a Google Summer of Code 2016 mentor organization"
date:    2016-03-07
authors:
  - "valoriez"
slug:    kde-has-been-selected-google-summer-code-2016-mentor-organization
comments:
  - subject: "Valorie,"
    date: 2016-03-11
    body: "<p>Valorie,</p><p>&nbsp;</p><p>Please always add the news info from the subject into post content, otherwise I start reading the message below the title line and don't understand what it is about and why you suddenly say \"<span style=\"color: #2e3436; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 20.8px;\">All interested students are encouraged to begin working with the KDE community</span>\".</p>"
    author: "Alexander Potashev"
  - subject: "Do you have a link to a"
    date: 2016-03-11
    body: "<p>Do you have a link to a Google blog announcing accepted orgs for GSoC?</p>"
    author: "Alexander Potashev"
  - subject: "Improving stories"
    date: 2016-03-23
    body: "<p>Thank you, Alexander. I'm new, and that is great feedback. I'll take your advice.</p><p>Valorie</p>"
    author: "valoriez"
  - subject: "GSoC accepted orgs "
    date: 2016-03-23
    body: "<p>Hello Alexander,</p><p>&nbsp;</p><p><a title=\"2016 Google Summer of Code mentor organizations\" href=\"http://google-opensource.blogspot.com/2016/02/2016-google-summer-of-code-mentor.html\">http://google-opensource.blogspot.com/2016/02/2016-google-summer-of-code-mentor.html</a>&nbsp;links to the front page of the new GSoC site,&nbsp;<a title=\"GSoC organizations\" href=\"https://summerofcode.withgoogle.com/organizations/\" target=\"_blank\">https://summerofcode.withgoogle.com/organizations/</a>.</p>"
    author: "valoriez"
---
<img style="float: right;" src="/sites/dot.kde.org/files/GSoC2016Logo.jpg" alt="GSoC 2016 logo" />

<p>All interested students are encouraged to begin working with the KDE community.</p>

<p>The <a href="https://community.kde.org/GSoC">KDE GSOC guide</a> is a good place for students to start before beginning to create their proposals. The KDE community creates software in teams; students should find a team working on software they want to help with, get to know team members, familiarize themselves with the code-base, and start fixing bugs.</p>

<p>Mentors are presenting their <a title="here" href="https://community.kde.org/GSoC/2016/Ideas">ideas for students</a> as an aid to students creating their proposals. Students will use each team's preferred channels, but can ask general questions in #kde-soc on freenode IRC, or <a title="KDE-Soc Telegram group" href="https://telegram.me/joinchat/A-9tjgavn9YJ2myF1aTK4A">KDE-Soc Telegram group</a>.</p>

<p>The KDE Student Programs team looks forward to getting mentors signed up in Google's new SoC webapp, and meeting with prospective students before student applications begin.</p>
