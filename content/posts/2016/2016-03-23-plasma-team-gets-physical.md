---
title: "Plasma Team Gets Physical"
date:    2016-03-23
authors:
  - "sebas"
slug:    plasma-team-gets-physical
---
In March, the yearly meeting of KDE's Plasma team was held in Geneva, kindly hosted by the European Centre for Nuclear Research (<a href="http://home.cern/">CERN</a>). In-person meetings provide unique opportunities to work together face-to-face, at high bandwidth to tackle problems together and plan for the future. As there were some other groups present during this meeting, notably the visual design group and the Wiki cleanup team, there was ample opportunity to think outside of the Plasma box.
<p><img style="border: 0px;" src="/sites/dot.kde.org/files/plasma-cernbanner.png" alt="Getting Physical -- Plasma" /></p>
The Plasma team discussed many topics that are currently being worked on, or need more attention, but also sat down to hack on the code. The result was a nice mix of hands-on activities and dreaming of the future.

<h2>Wayland</h2>

As the Wayland port is nearing completion, the team had a closer look at the current state. Bugs, that have gone previously undiscovered have been identified and fixed, code that lived in branches was reviewed and merged, and holes in the implementation were discussed. Most Plasma developers actually returned home with a fairly functional Plasma/Wayland session, which will accelerate dogfooding and make tracking of problems easier. An ongoing effort is also to spread the work on Wayland across more shoulders.
A Plasma/Wayland session is currently somewhat functional, but not suitable yet for being a daily driver, or even beta testing. The goal is to have a beta-level version ready by summer and fix bugs and regressions from there. The final Plasma/Wayland session should be hardly any different from an X11 session in terms of functionality and features, but run just a little bit smoother and carry less annoying idiosyncracies of a typical X11 session or jerky animations.

<h2>Visuals</h2>
<img style="float: right;" src="/sites/dot.kde.org/files/plasma-cern-meetingroom-wee.jpg" alt="Getting Physical -- Plasma" />
Together with the visual design group, many smaller and bigger issues have been discussed, and fixed. Among the bigger parts was a redesigned settings module for Plasma themes, featuring live previews and a more user-friendly design. This new module has already been merged into Plasma master (which currently is the tree that will eventually become Plasma 5.7 this summer). A lot of work has been put into correcting and completing usage of icons. New icons have been created, applications' icon usage has been reviewed and fixed. The result are lots of small fixes all over the place, which result in a yet more consistent look and feel.

<h2>Window Metadata</h2>

One of the central roles of a desktop is to show and switch between application windows. This means that the "desktop shell" has to be able to communicate with applications (it wants to know its title, icon, set its state, etc.). To further improve the integration between the desktop and the applications, the Plasma developers have designed the Windowmetadata framework, which provides bi-directional communication between applications and the desktop to allow for a richer experience. The results will be better previews, and improved functionality of the task manager. The Window Metadata framework has been designed to be independant of the desktop or window decoration that might, or might not be present, and thereby addresses one of the major concerns with <a href="https://blog.martin-graesslin.com/blog/2010/05/why-you-should-not-use-client-side-window-decorations/">client-side</a> <a href="https://blog.martin-graesslin.com/blog/2010/05/follow-up-on-client-side-decorations/">window decorations</a>. The Window Metadata framework is still in its <a href="https://kver.wordpress.com/2016/03/15/dwd-structured-at-cern/">early design phase</a>, so if you are interested in working on this topic, now is an excellent time to get involved.

<h2>Bits and Pieces</h2>

Among other things that have been the topic of this meeting are
<ul>
<li>Documentation for developers. Plasma 5 has been quite radically redesigned under the hood, that means that some of the documentation now needs a revamp. The docs and Plasma teams sat down and reviewed a preliminary developer guide, and jointly created a list of things to be improved in there, before it can be made available to a wider public.</li>
<li>The weather widget, which has been re-added to the latest Plasma 5.6 release has been reviewed, and a new visual design has been created for it, likely to be realized by the next Plasma release, 5.7, this summer.</li>
<li>Together with the VDG, some of the system settings modules have been reviewed and design improvements have been proposed.</li>
</ul>

The Plasma team would like to thank the <a href="http://ev.kde.org">KDE e.V.</a> for their generous support of our travel costs. If you are using Plasma and would like to support its development, we encourage you to sign up as a <a href="https://relate.kde.org/">Supporting Member</a> or to <a href="https://www.kde.org/community/donations/index.php">donate</a>.
