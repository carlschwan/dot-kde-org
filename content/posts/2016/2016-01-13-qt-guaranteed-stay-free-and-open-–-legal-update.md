---
title: "Qt is Guaranteed to Stay Free and Open \u2013 Legal Update"
date:    2016-01-13
authors:
  - "Olaf Schmidt-Wischh\u00f6fer"
slug:    qt-guaranteed-stay-free-and-open-–-legal-update
comments:
  - subject: "Great!"
    date: 2016-01-13
    body: "<p>It's nice!</p>"
    author: "CSRedRat"
  - subject: "Thank you KDE for having"
    date: 2016-01-13
    body: "Thank you KDE for having started this shift of the Qt world to open source. It changed my life and will change many more to come. And thank you Qt Company and co to agree more and more to this bright future ahead of you, you are a true embodiment that open source does not contradict commercial success, it just need proper legal and managerial tweaking.\r\n\r\nTo conclude, one word: Bravo!"
    author: "Anonymous"
  - subject: "Big effort from multiple parties"
    date: 2016-01-14
    body: "<p>Thanks!</p><p>It was a big joint effort from Lars Knoll and Tuukka Turunen from The Qt Company on one side, Olaf and myself from KDE on the other side while Eirik Chambe-Eng (Trolltech Founder) help in finding solutions to difficult problems.</p><p>Special kudos to our lawyer Till Jaeger who helped to improve the legal texts for clarity and preciseness through careful examination.</p>"
    author: "Martin Konold"
  - subject: "Great work Qt and KDE teams!"
    date: 2016-01-15
    body: "<p>Great work Qt and KDE teams!</p>"
    author: "Anonymous"
  - subject: "great"
    date: 2016-01-17
    body: "<p>Thanks so much, We love u KDE</p>"
    author: "azzamsa"
---
KDE develops Free, Open Source Software. Most of the KDE software uses the Qt library, which is available under compatible free licenses. The continued availability of Qt as Free Software is safeguarded by the <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation</a>, founded in 1998. Recently, the foundation was able to conclude <a href="https://www.kde.org/community/whatiskde/Software_License_Agreement_2015.pdf">an updated agreement</a> with <a href="https://www.qt.io/about-us/">The Qt Company</a> that extends the scope of the protection and allows a license update.

<p><strong>More platforms included</strong></p><p>The old agreement already offered protection for X11 (i.e. Desktop Linux) and for Android. The new agreement now also extends to Microsoft Windows, Microsoft Windows Phone, Apple MacOS and Apple iOS. They must be covered by the Free Software release of Qt as long as they are supported at all by The Qt Company. The protection for Linux is even stronger: Support for X11 and Android cannot be dropped at all. The Foundation also has the right to update to supported successor platforms (e.g. from X11 to Wayland) without additional negotiations with The Qt Company.</p>

<p><strong>More code to be available as Free Software</strong></p><p>With the new agreement, The Qt Company honours the request of the KDE Free Qt Foundation to release the <a href="https://www.qt.io/ad-commercial/">commercial features</a> as Free Software. The Add-ons will be released under the <a href="https://en.wikipedia.org/wiki/GPL">GPL</a> (version 3, and 2 in most cases). This means that developers of GPL applications will have have access to even more Qt modules than in the past.</p>

<p><strong>License update</strong></p><p>Most parts of Qt are currently available under the <a href="https://en.wikipedia.org/wiki/GNU_Lesser_General_Public_License">LGPL</a> license, which can be used both for Free Software and for proprietary software. The new agreement continues with this approach while making some changes in the details:
<ul>
<li>New add-ons for Qt may use the GPL license, which allows use for Free Software only. Developers of proprietary applications can buy an additional Qt license if they wish to use the new functionality. This set-up makes it possible that The Qt Company recovers the cost of developing new add-ons – while releasing them as Free Software inside Qt, rather than as a separate extension product.</li>
<li>The core libraries of Qt (Essentials) and all existing LGPL-licensed Qt add-ons must continue to be available under the LGPL.</li>
<li>Applications included in Qt (e.g. Qt Creator) will be licensed under the GPL (version 2 and 3).</li>
<li>Qt will migrate from LGPL version 2.1 to LGPL version 3 (plus GPL version 2). The newer version is a better license for Free Software (patent clauses, <a href="https://en.wikipedia.org/wiki/Tivoization">Tivoization</a>) while continuing to allow use in proprietary applications.</li>
</ul>
</p>

<p><strong><em>In summary: All parts of Qt are available under the GPL (version 3) or under a compatible license. Most parts are also available under the LGPL and under the version 2 of the GPL.</em></strong></p>

<p><strong>Compatibility with existing licenses</strong></p><p>The terms of the license update have been carefully negotiated to allow full license compatibility with existing users of Qt:</p>
<ul>
<li>Full GPLv2-compatibility is kept for all existing Qt code, for the core part of Qt (Essentials) and for the Qt applications. Completely new add-ons can use GPL version 3 only, which allows the use of Apache-licensed dependencies. This enables new functionality in Qt which has been impossible under the old license terms.</li>
<li>Version 3 of the GPL is supported by all parts of Qt. Future versions of the GPL are supported “if approved by the KDE Free Qt Foundation”.</li>
<li>Authors of proprietary software can continue to use Qt free of charge if they are willing to comply with the terms of the LGPL. The LGPL license is kept for the core part of Qt (Essentials) and for all existing LGPL-licensed add-ons. Companies that do not like the LGPL conditions (e.g. allowing reverse engineering, passing on the license rights to Qt itself, naming the Qt authors and allowing end users to use a different Qt version) can buy an enterprise license of Qt and thereby fund the development of new Qt versions. The conditions are worded slightly differently in version 2.1 and in version 3. The newer version tends to have more legal “teeth”, but the principles are the same. It is legally possible to use both LGPLv2.1-licensed and LGPLv3-licensed libraries in the same proprietary application.</li>
<li>The applications included in Qt come with two GPL exceptions that clarify: 1. generated code is not license-restricted, and 2. bridges to third party applications are still possible.</li>
<li>All compatibility rules also cover cases where functionality is replaced by new modules.</li>
</ul>
<p>The new licensing will be applied with Qt 5.7. The <a href="https://blog.qt.io/blog/2015/12/18/introducing-long-term-support/">long term support release Qt 5.6</a> will stay with the old licensing.</p>

<p><strong>Qt Open Governance</strong></p><p>Since 2011, Qt is developed by an open community under a meritocratic <a href="https://wiki.qt.io/The_Qt_Governance_Model">governance model</a>. KDE developers have become major contributors to Qt. The new agreement between the KDE Free Qt Foundation and The Qt Company takes this change into account. New clauses protect the free license of all publicly developed code – even when it has not yet been included in a Qt release.</p>

<p><strong>Past and future</strong></p><p>The KDE Free Qt Foundation already played an important role when Nokia bought Trolltech, the original company behind Qt, and later sold Qt to Digia, which then founded The Qt Company. The contracts are carefully worded to stay valid in cases of acquisitions, mergers or bankruptcy. The history of the past 17 years has shown how well the legal set-up protects the freedom of Qt – and will continue to protect it in the future.</p>

<p><strong>More information:</strong></p><p><a href="http://blog.qt.io/blog/2016/01/13/new-agreement-with-the-kde-free-qt-foundation">Blog post at The Qt Company</a></p>