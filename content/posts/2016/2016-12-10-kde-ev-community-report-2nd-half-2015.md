---
title: "KDE e.V. Community Report - 2nd Half of 2015"
date:    2016-12-10
authors:
  - "sandroandrade"
slug:    kde-ev-community-report-2nd-half-2015
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/ev_large200.png" /></div>
The KDE e.V. community report for the second half of 2015 is <a href="https://ev.kde.org/reports/ev-2015H2/" target=”_blank”>now available</a>. It presents a survey of all the activities and events carried out, supported, and funded by KDE e.V. in that period, as well as the reporting of major conferences that KDE has been involved in.

<h4>Featured Article – The KDE Incubator</h4>

The KDE Incubator is how KDE currently approaches new projects joining our "umbrella". This program aims to help projects with similar ideals to our existing projects join us with all that that implies. The incubator couples a sponsor from the KDE community with a plan to move/migrate a project into the systems KDE provides as a community, including mailing lists, websites, code repositories, etc. In this featured article, Jeremy Whiting tells us how The KDE Incubator works and provides an overview about the projects that have been incubated in 2015.

<h4>Other Activities</h4>

The report also provides a synopsis of member activities during the second half of 2015. The WikiToLearn Sprint, in Bormio, focused mostly on preparing the new project website. Randa Meetings 2015 was a big success and people from many KDE projects worked on many interesting advances regarding the Randa Meetings 2015's motto: "Bring Touch to KDE". During the Kate/KDevelop Sprint in Berlin, a lot of Kate bugs have been fixed and all remaining things for the KDevelop 5.0 release were put in place. The report also provides an overview of Akademy 2015 and a summary of the KDE participation in the 16th International Free Software Forum (FISL 2015).

<h4>Results</h4>

The report concludes with a summary of activities undertaken by the Sysadmin Working Group, the financial report, and the contributors who joined KDE e.V. during the period. We invite you to read the <a href="https://ev.kde.org/reports/ev-2015H2/" target=”_blank”>entire report</a>!