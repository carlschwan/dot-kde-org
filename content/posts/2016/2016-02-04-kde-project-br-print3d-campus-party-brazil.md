---
title: "KDE Project Br-Print3D at Campus Party Brazil"
date:    2016-02-04
authors:
  - "jriddell"
slug:    kde-project-br-print3d-campus-party-brazil
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/pic05.JPG"><img src="/sites/dot.kde.org/files/wee-pic05.JPG" /></a></div> 

Last week, between January 26 and 31, the ninth Campus Party Brazil (<a href="https://www.facebook.com/campuspartybrasil/videos/10153816183137349/">promo video on Facebook</a>) was held in Sao Paulo. 8000 people inside an arena, with talks, workshops and hackathons, with the main subject being technology.

The team from KDE project Br-Print3D was invited to participate of this event. To show our work on the Free Software stage and on the tables there are scattered all over of this arena.
<br clear="all"><div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/pic12.JPG"><img src="/sites/dot.kde.org/files/wee-pic12.JPG" /></a><br />Br-Print3D written in Qt</div> 

We received sponsorship from Sethi3D, a Brazilian company that makes 3D printers here.  They kindly gave us four 3D printers to use, 3 of them with a 20cm<sup>3</sup> print area and one with 40cm<sup>3</sup>. We made a lot of prints, that you can <a href="https://goo.gl/6DVE7M">see on our album on Facebook</a>.

<br clear="all"><div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/pic13.jpg"><img src="/sites/dot.kde.org/files/wee-pic13.jpg" /></a><br />Sethi BB - 40cm<sup>3</sup> area of printing</div> 

Over the five days almost 500 people stopped on our table to talk about 3D printing and to learn about our project.

At the event we are had more access to different printers and we saw how other firmware variations work. Focusing on Repetier we were for the first time able to test formally Sethi’s printers and we detected some particularities of this firmware which probably can be found on others models.

They were five days of challenges, compliments and reviews about our work. I made a talk about C++ and Qt, and how to use Qt to develop user interfaces. What is funny, that only 7 people attending my my talk, out of over 100, knew about Qt and only one really worked with it. That was the same feeling that I had when I started work with Qt. It’s hard do find people that knows Qt here in Brazil, mainly if you don’t have more information or connection to open sources projects.

<br clear="all"><div style="float: left; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/pic02.jpg"><img src="/sites/dot.kde.org/files/wee-pic02.jpg" /></a><br />Lays running the tutorial for Br-Print3D</div> 

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/pic10.JPG"><img src="/sites/dot.kde.org/files/wee-pic10.JPG" /></a><br />Hogwarts House Crests printed with Br-Print3D</div> 
