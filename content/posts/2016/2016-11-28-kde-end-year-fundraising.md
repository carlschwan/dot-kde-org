---
title: "KDE End of Year Fundraising"
date:    2016-11-28
authors:
  - "canllaith"
slug:    kde-end-year-fundraising
---
<a href=""><img src="https://www.kde.org/fundraisers/yearend2016/postcard01.png" style="float:right; height:286px; width:200px; margin-left:15px" alt="KDE Fundraising"></a>
<p>Have you ever felt that you wanted to give back to the KDE project? As the season of giving draws near there's never been a better time to support KDE and help the project continue to bring free software to millions of lives worldwide.</p>
<p>By participating in the end of year fundraiser, you can help us in our mission. Your donations are used to pay for transport and accomodation for developers to attend sprints as well as to support the server infrastructure required to keep the project running.</p>
<p>Donations over 30&euro; are eligible to recieve a cute postcard designed by KDE artists to the address of the donors choice. For more information, including more details on donation rewards, please visit the <a href="https://www.kde.org/fundraisers/yearend2016/">KDE End of Year 2016 Fundraising</a> page.

<!--break-->