---
title: "KDE Software Store"
date:    2016-09-03
authors:
  - "jriddell"
slug:    kde-software-store
comments:
  - subject: "This is really interesting."
    date: 2016-09-03
    body: "This is really interesting. Thanks a lot for taking control of the situation and improving it."
    author: "emilsedgh"
  - subject: "Fantastic!!!"
    date: 2016-09-03
    body: "<p>This is fantastic news!!!!</p>"
    author: "psifidotos"
  - subject: "..."
    date: 2016-09-03
    body: "<p>The fact that you didn't even mention our works and efforts confirms kde is run by blue systems (and sisters) and that there are first class persons and second class ones in kde.</p><p>Be happy with your store then! All the luck!&nbsp;</p>"
    author: "snizzo"
  - subject: "I'm a bit confused, could you"
    date: 2016-09-03
    body: "I'm a bit confused, could you explain how you contributed to the KDE Store? When I asked you if you wanted to participate, you declined, and didn't take part or contribute to the KDE store or the client side at all.\r\n\r\n"
    author: "sebas"
  - subject: "..."
    date: 2016-09-03
    body: "<p>Because you did a secret meeting to discuss and plan the development of a product that wasn't goign to be opensourced until TODAY. The community wasn't informed and you folks basically acted like a company. In fact, TODAY is the first day I'm seeing the source code. Why would have I wanted to partecipate if development was done behind closed doors? That's not open source development, that's company strategy marketing and I didn't want to work for free for blue systems, simple as that.</p><p>And no, I wasn't referring in our contributions in the KDE store that was announced TODAY. So no one knew anything until was announced TODAY. It is perfectly reasonable that I didn't knew of it. And yes you invited me privately to take part in the development behind the doors, but that is \"work\" in my opinion.&nbsp;<br />I was referring to the fact that we started the development of an opensource server that was also in good shape regarding functionality and was also working with attica and new stuff libraries, but wasn't even mentioned in a line in the history of the kde server side technologies. It gives a reader the impression of \"there was a problem and since no one took care of it we solved it now\" (sorry for the solved part, that's still very questionable).</p><p>It's just my two cents, and I know you're not going to understand again, you're going to be puzzled etcetcetc. but the truth is I don't really care anymore. There are far more healthier communities to stay in so it's not my problem anymore, just wanted to drop by and let people know others were working towards a solution but there are people able to take strong decisions in this community, and that community lately follows blue systems willing (and thus driving) kde.</p><p>Again, good luck with your store!</p><p>p.s. Ah, our framework (GFX), that one that sucks you know, is now used on real reserch projects for predictions of serious illness like breast cancer. Just to say: I really don't care and I won't reply again.</p>"
    author: "snizzo"
  - subject: "What work did you do?\n\u00a0"
    date: 2016-09-03
    body: "<p>What work did you do?</p><p>&nbsp;</p>"
    author: "Anonymous"
  - subject: "Great news!"
    date: 2016-09-03
    body: "<p>This is really good. I've always thought FOSS needed a way to centralize and make it easy for software developers to get donations regularly. Especially with all the quality FOSS out there.&nbsp;</p>"
    author: "Abanh"
  - subject: "KDE Software store vs. Discover"
    date: 2016-09-04
    body: "<p>When will ths replace the <a href=\"https://bugs.kde.org/show_bug.cgi?id=362585#c4\" target=\"_blank\">badly broken Discover</a>?</p>"
    author: "Dan Dascalescu"
  - subject: "Good! But some questions remain"
    date: 2016-09-05
    body: "<p>This is great! Kudos to everyone involved.</p><p>Now I have some questions.</p><p>1. kde-apps.org is no more. Is there any way where we can get the latest and greatest apps for Plasma in source form?</p><p>2. I see that kde-files.org runs on the old framework. Are there any plans to migrate it to the new format?</p><p>Thanks again!</p>"
    author: "Eduardo Sanchez"
  - subject: "this is how it went down..."
    date: 2016-09-05
    body: "<p>\r\nI think you're misrepresenting things, let me correct just a few of your claims:\r\n\r\n<ul>\r\n    <li>The meeting wasn't secret, you, among other related developers were invited, if it were secret, believe me, you'd not have heard about it</li>\r\n    <li>The meeting happened very early in the process, we had taken no decision at that point, the whole idea was to get an idea of options to solve the underlying problem</li>\r\n    <li>When I asked you what your plans with your OCS server code base were, you said you weren't planning to work on it, unless you'd be paid for it (we did consider that, but ... read on...)</li>\r\n    <li>When I reviewed that code, it\r\n<ul>\r\n        <li>was not working, it was incomplete\r\n        <li>was of pretty sad quality (lots of string-indexed hash tables, no autotests, etc.)\r\n        <li>was unmaintained and you weren't planning to work on it\r\n</ul></li>\r\n    <li>When I looked at the git logs, I didn't see a single commit after your GSoC project ended</li>\r\n</ul>\r\n</p>\r\n<p>\r\nThe source code that we decided to use for the service in the end was incubated in KDE (and with that relicensed and open-sourced) only on Tuesday last week, and I think that alone is a great achievement and a good thing for both, KDE and the Free software community in general. I might add that Blue Systems is not making any money with it, in fact it invested a lot in developing and running the service for KDE.\r\n</p>\r\n<p>\r\nA personal note, I think you're being unfair and you're misrepresenting how things went down. We worked together with those people in KDE who would be productive, the current maintainer of opendesktop, the KDE e.V. board, the maintainer of KNewStuff, people working on appstream and flatpak, the guy who mentored your ocs-server project, various application maintainers and the KDE e.V. board itself. We followed a defined incubation process to make the code part of KDE.\r\n</p>\r\n<p>\r\nI can understand that you're disappointed that your project went nowhere, but frankly, that is nothing but your own fault. Your expectations were way off, you were hard to deal with (even when we went out of our own way to have you participate) and the code you wrote was quite bad quality: nothing I'd rely on.\r\n</p>"
    author: "sebas"
  - subject: "discover and the kde store are orthogonal"
    date: 2016-09-05
    body: "It won't, these two things are orthogonal. We'll continue to fix bugs in discover, the store will likely add to the content offered via discover."
    author: "sebas"
  - subject: "pling.it FOSS?"
    date: 2016-09-11
    body: "<p>Does this mean that pling.it is driven by FOSS, available under AGPL? If that were the case it would solve a long-standing requirement I have - which would be totally awesome.</p>"
    author: "Arne Babenhauserheide"
  - subject: "Remplace to discover"
    date: 2016-09-26
    body: "<p>I'm a new user of KDE Neon User and disvorer is a bad software store cause is slow for show apps, updates and repositories... i hope that very soon disvover just remplaced for a new software store...</p>"
    author: "Ramon Marquez"
  - subject: "Out Of The Loop"
    date: 2016-09-29
    body: "<p>As someone that's not been keeping up with software updating... er, software, is there a link or something where I can read up on the status of Apper, Discover etc.?</p><p>I only knew about Discover after it became the default on Kubuntu, and it seemed quite unpolished.</p>"
    author: "madman"
  - subject: "When I was started to use"
    date: 2019-09-08
    body: "<p>When I was started to use Linux - I was pretty impressed by the packages/repositories - everything is secure and so cool compared to the way we work on windows system.</p><p>I don't understand why we try to use something like flatpack, snap, etc.... this is EXACTLY the way it works on Windows and it sucks actually. I can understand that we may use such approach to test the new things that is not available yet in the Ubuntu/Debian/Fedora, but I do think that repositories should stay as the default way to deal with packages.</p>"
    author: "Anonymous"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://store.kde.org"><img src="/sites/dot.kde.org/files/store.kde_.org_.png" width="420" height="330" /></a><br />KDE Store</div> 

At this year's Akademy, KDE announced <a href="https://store.kde.org">The KDE Store</a>. The new store replaces the services provided by openDesktop.org with a Free-as-in-Freedom software sharing platform.

<h2>A Bit of History</h2>

OpenDesktop, <a href="http://karlitschek.de/2016/01/opendesktop-org-15-years-in-review/">founded in 2001</a> was one of the first of its kind, very innovative and perhaps even a bit ahead of its time. OpenDesktop served addons such as themes, wallpapers and other non-compiled assets for applications or the desktop. It never established itself as a platform for distribution of applications, or even binary packages. Nevertheless, openDesktop offered users of KDE software (and other desktops as well) a way to extend their apps, and creators a way to share their work with users.

In recent years, openDesktop hasn't seen much love other than keeping it running, there weren't any new features and no solution for offering binary packages. Compared to modern software stores, it fell short.

<h2>A New Beginning</h2>

In January 2016, Blue Systems acquired hive01 from Frank Karlitschek and restarted work on openDesktop. Since then, content has been cleaned up, the server backend has been replaced by a more modern and scalable solution, and some future plans have been made. Today, KDE announced that the source code for this new service has been released as Free software under the AGPL, fixing a long standing bug in KDE software: reliance on a proprietary web service. The <a href="https://quickgit.kde.org/?p=ocs-webserver.git">source code for this new web service</a> has been incubated into KDE and is now actively developed under the KDE umbrella. The new store allows users to easily donate to the creator, so artists, developers and contributors now have a clear revenue model when they upload their content.

<h2>Sustainability Guaranteed</h2>

KDE e.V. has entered a contract with <a href="https://www.pling.it/">PLING</a>, a sister company of Blue Systems who run the KDE Store service on behalf of KDE. This agreement guarantees KDE the availability of the source code and data, KDE will receive regular data dumps from PLING, so KDE e.V. can, if that situation should arise, take over operation of the KDE Store. The reliance on a third party has also been reduced. The license of the software, the open development process and the availability of the data put KDE (and its users) in a much better place with respect to the sustainability of this service.

<h2>Containerized Apps</h2>

Freeing and migrating the openDesktop services is just the first step. KDE is already looking into also offering applications in containerized form in the software store. This may lead to a much more direct way of distributing software. It will allow users get their software from the developer, thereby cutting out the middle man and reducing the time it takes for an update to reach the users. KDE is currently experimenting with different containerized app technologies, such as Flatpak, Snappy and AppImage. The jury is still out there on which of these will be the most useful formats, as the technologies are very much work in progress, a progress KDE is happy to be at the centre of.

<h2>Upwards and Onwards</h2>

The new KDE store offers users and developers more freedom, it gives creators a revenue model and users a way to thank them, and if offers interesting perspectives on software distribution for the future.
Perhaps most importantly, it puts all those things in the hands of the community.
<!--break-->
