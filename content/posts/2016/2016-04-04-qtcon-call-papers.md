---
title: "QtCon Call for Papers"
date:    2016-04-04
authors:
  - "araceletorres"
slug:    qtcon-call-papers
---
<a href="https://qtcon.org/">QtCon 2016</a> Call for Papers is open. The event will assemble KDE Akademy, VideoLAN Developer Days, Qt Contributors' Summit, FSFE Summit and KDAB Qt training day. We invite contributors to these projects to present their work and insight at QtCon 2016. The conference will take place from 1st to 8th September in Berlin, Germany. The talks will be from 2nd to 4th with KDE continuing with BoFs till the 8th

If you know of someone else who should present, please nominate them. For more details see the proposal guidelines and the <a href="https://qtcon.org/cfp">Call for Papers</a>.

The submission deadline is 15th May, 23:59:59 CEST.

<h3>About QtCon 2016, Berlin, Germany</h3>

QtCon is a congress of hundreds of developers, translators, artists, writers, users, supporters, sponsors and other people interested in collaborating with our FLOSS projects. Contributors from KDE, Qt, FSFE and VideoLAN usually meet annually in their respective events to share their ideas and knowledge, develop new technologies and improve existing ones. They also make plans for the future of their projects and meet people from their communities face to face. This year all these events will gather to celebrate a very special moment in the history of the FLOSS movement: KDE, one of the largest communities of free software in the world is celebrating its 20th anniversary. Additionally, VideoLAN and FSFE also celebrate their 15th anniversary. It will be a historic event with trainings, talks, meetings and coding sessions. Companies building on technology of these projects, and those that are looking for opportunities are welcome.

For more information, please contact the <a href="mailto:team@qtcon.org">QtCon Team</a>