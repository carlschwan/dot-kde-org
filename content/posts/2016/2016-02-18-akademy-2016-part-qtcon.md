---
title: "Akademy 2016 part of QtCon"
date:    2016-02-18
authors:
  - "sealne"
slug:    akademy-2016-part-qtcon
---
2016 is a special year for many FLOSS projects: KDE has its 20th birthday while Free Software Foundation Europe and VideoLAN both have their 15th birthday. All these call for celebrations! This year KDE has come together with Qt, FSFE, VideoLAN and KDAB to bring you <a href="https://qtcon.org">QtCon</a>, where attendees can meet, collaborate and get the latest news of all these projects.

Akademy 2016 will be organised as part of QtCon in Berlin. The event will start on Friday the 2nd of September with 3 shared days of talks followed by 4 days of Bird of a Feather (BoF) and coding sessions. 

The current plan is to have a Call for Papers in March with the Program announced in early June.

<img src="/sites/dot.kde.org/files/QtConInfo_v4_wee.png" />

<h2>About Akademy</h2>
For most of the year, KDE—one of the largest free and open software communities in the world—works online by email, IRC, forums and mailing lists. Akademy provides all KDE contributors the opportunity to meet in person to foster social bonds, work on concrete technology issues, consider new ideas, and reinforce the innovative, dynamic culture of KDE. Akademy brings together artists, designers, developers, translators, users, writers, sponsors and many other types of KDE contributors to celebrate the achievements of the past year and help determine the direction for the next year. Hands-on sessions offer the opportunity for intense work bringing those plans to reality. The KDE Community welcomes companies building on KDE technology, and those who are looking for opportunities.

For more information, please contact The <a href="mailto:akademy-team@kde.org">Akademy Team</a>.