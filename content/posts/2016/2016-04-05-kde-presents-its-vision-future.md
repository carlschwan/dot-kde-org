---
title: "KDE Presents its Vision for the Future"
date:    2016-04-05
authors:
  - "thomaspfeiffer"
slug:    kde-presents-its-vision-future
comments:
  - subject: "a11y"
    date: 2016-04-05
    body: "<p>I also would be prefer to see some improvements in kind of accessibility for visual impaired blind people or people with other disablitys.</p><p>Sadly KDE is mostly inaccessible.</p><p>Keep on your gread work :).</p>"
    author: "simon"
  - subject: "Edukation; Inspektor"
    date: 2016-04-05
    body: "<p>This is an exciting vision!</p><p>Following from <a href=\"http://lu.is/blog/2016/03/23/free-as-in-my-libreplanet-2016-talk/\">Luis Villa's recent post</a> and the point about Freedom, I thought of this:</p><blockquote><p>\u201cSome may feel in control of a <strong><ins>Free</ins></strong> application as long as it obeys their commands, but without the freedom to make changes and share them, they are entirely reliant on the <strong><ins>community</ins></strong>'s benevolence for this apparent 'control'.\u201d</p></blockquote><p>People only have that freedom if they have the skills to actually, really modify their software. We should help to educate people so that they have the coding skills. We should encourage them to participate in our community, so that they feel empowered to use their skills to help others.</p><p>And we must simplify the practical steps to find source code, modify it, build it, and install the modified app locally. Tinkering should be as easy as we can possibly make it. Imagine a Web Inspector, but for all KDE software\u2014imagine if you could edit KDE software as easily as a web page!</p>"
    author: "Greg K Nicholson"
  - subject: "looks familiar"
    date: 2016-04-05
    body: "<p>You just described oss software.</p>"
    author: "Anonymous"
  - subject: "Everyone"
    date: 2016-04-05
    body: "<p>When making the fruits of your work available to all, please don't forget the disabled. &nbsp;It's nice that I can afford it, but I have to be able to use it too! &nbsp;:)</p>"
    author: "Adina"
  - subject: "Security & privacy"
    date: 2016-04-06
    body: "Privacy without security doesn't exist, please add that to your list."
    author: "someoneelse"
  - subject: "From mission to strategy"
    date: 2016-04-06
    body: "<p>I am curious for the mission, but most interesting will be how you translate this into your strategy going forward. I think that KDE will have to choose its battles, so I am looking forward to better understand your priorities (and 3-year plan).</p>"
    author: "Quentin"
  - subject: "Beautiful"
    date: 2016-04-06
    body: "<p>A nice concrete vision! Love it.</p>"
    author: "Martin de Boer"
  - subject: "a11y"
    date: 2016-04-20
    body: "<p>Hi Simon, yes, I think KDE can do better. For now, we're working upstream in Qt to make the tools for a11y better everywhere. You are welcome to join us in IRC, #kde-accessibility, or on the KDE Forum, or the mail list:&nbsp;https://mail.kde.org/mailman/listinfo/kde-accessibility . We need testers as well as coders!</p>"
    author: "valoriez"
  - subject: "a11y"
    date: 2016-04-20
    body: "<p>Hi Adina, we've not forgotten the disable. But I think KDE can do better. For now, we're working upstream in Qt to make the tools for a11y better everywhere.</p><p>&nbsp;</p><p>Please join us in IRC, #kde-accessibility, or on the KDE Forum, or the mail list: https://mail.kde.org/mailman/listinfo/kde-accessibility . We need testers as well as coders!</p>"
    author: "valoriez"
---
<div style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="/sites/dot.kde.org/files/Vision_collage.jpg"><img src="http://dot.kde.org/sites/dot.kde.org/files/Vision_collage_small.jpg" /></a><br />KDE contributors showing their personal visions at Akademy 2015</div> 

KDE is a highly diverse community and every one of our contributors has his or her own motives, such as having fun, developing new skills and meeting nice people. However, a common desire unites all of KDE: to change the world for the better. This shared motivation, although the major driving force behind KDE, has never really been made explicit.

A <a href="https://evolve.kde.org/results.html">survey</a>  conducted among KDE contributors and users last year showed that the respondents were missing exactly that: an explicit vision and strategy for KDE. As a result, we began working on a shared vision at last year's Akademy (KDE's annual contributor conference). After months of collecting ideas and holding open discussion across our community, we have united around a simple statement of what we want to achieve:

<p style="text-align: center"><cite ><strong>A world in which everyone has control over their digital life and enjoys freedom and privacy.</strong></cite></p>
This is our vision.

<h2>Our Vision in Detail</h2>

Each part of the vision has been carefully chosen to convey our intent and scope:

</div><strong>A world:</strong> We are not doing this only for ourselves, our friends and family, our employer or customers, and we recognize no geographical barriers to our work. We want to change no less than the world we live in. 

<strong>Everyone:</strong> The work should not just be for a small group of people. The fruits of our work should be available to all, without being restricted to materially, educationally or socially privileged people.

<strong>Control:</strong> KDE has always aimed to put people in control. We don't want to hand over control to anybody else. Not to some service providers, not to some hardware vendors, not to governments, not even to KDE. KDE wants to put <em>you</em> in the driver's seat.

<strong>Digital life: </strong>We want to allow people to control every aspect of their digital lives: Hardware, software, data, communication, everything. Of course, there is much more to life than the 'digital' part. While we all want freedom and control in the other parts too, influencing that is beyond KDE's scope, so we limit our vision to "digital life".

<strong>Freedom: </strong>We believe that freedom is a prerequisite to true control. Some may feel in control of a proprietary application as long as it obeys their commands, but without the freedom to make changes and share them, they are entirely reliant on the vendor's benevolence for this apparent 'control'.

<strong>Privacy: </strong>In a world where our privacy is increasingly threatened, we wanted to emphasize its importance. Freedom without the right to privacy is no freedom at all.

<h2>Next Steps: From Vision to Mission</h2>

Our vision unites KDE in common purpose. It sets out where we want to get to, but it provides no guidance on how we should get there. After finalizing our vision (the "what"), we have immediately started the process of defining KDE's Mission Statement (the "how"). As with all things KDE, you are invited to contribute. You can easily add your thoughts on our <a href="https://community.kde.org/KDE/Mission/Brainstorming">mission brainstorming</a> wiki page.

If you share our vision you can also support us by helping to fund our work, either via a regular payment as an official <a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5">Supporting Member</a> or via a one-off <a href="https://www.kde.org/community/donations/">donation</a>.