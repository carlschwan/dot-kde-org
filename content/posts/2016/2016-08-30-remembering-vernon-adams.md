---
title: "Remembering Vernon Adams"
date:    2016-08-30
authors:
  - "jriddell"
slug:    remembering-vernon-adams
comments:
  - subject: "Vernon Adams"
    date: 2016-09-11
    body: "<p>Please consider donating to the family of Vernon Adams. &nbsp;I know I will when I get paid. &nbsp;https://www.youcaring.com/vernon-adams-642574</p>"
    author: "Kevin Remisoski"
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="http://static.lwn.net/images/2016/08-vernon-adams-sm.jpg"><img src="http://static.lwn.net/images/2016/08-vernon-adams-sm.jpg" /></a><br />Vernon</div> 

<a href="http://lwn.net/Articles/697980/">LWN reports</a> on the sad death of Vernon Adams, designer of the Oxygen font and author of the invaluable how to use Font Forge guide.

VDG Artist Thomas Pfeiffer <a href="https://plus.google.com/u/0/115138436279578375375/posts/i2rn7TYkaag">writes</a>:
<blockquote>
The name Vernon Adams might not ring any bells for you, but if you have used Plasma in the recent past, you know at least one of his works: The Oxygen font, which was Plasma's default user interface font for a long time.

Vernon did excellent work on the font, and we'd still be using it as our default today if a tragic car accident had not rendered him unable to continue his work (sadly nobody else took it up, either). Sadly, Vernon has now passed away.

Vernon Adams will always be remembered by the Free Software community for his tireless work for freedom in font design, and we hope he will inspire countless font designers to come.﻿
</blockquote>
