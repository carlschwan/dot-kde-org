---
title: "Review KmPlot Application"
date:    2016-01-27
authors:
  - "jriddell"
slug:    review-kmplot-application
---
<p>Our series of articles by Google Code In students continues with this review of graphing applications KmPlot by Andrey Cygankov.</p>
<hr />

<p>Studying maths, I often work with functions and graphs. Graph plotter KmPlot is a great help with this. A list of its features shows it can do enough to solve even the most difficult tasks.</p>

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/kmplot1-resize.png" /><br />Drawing a function and its derivative.</div> 
<!--break-->
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/kmplot2-resize.png" /><br />Finding the area under a graph.</div> 

<p>KmPlot can:</p>
<ul>
<li>Draw various graph types: functions, parametric, polar</li>
<li>Display on a flexibly configurable grid</li>
<li>Draw 1st and 2nd derivative and the integral of a plot function</li>
<li>Export in different formats: BMP, JPG, SVG and XML for futher editing</li>
<li>Use custom constants and parameters</li>
<li>Finding the minimum and maximum point in the function and the y-value of them.</li></ul>

<p>KmPlot has important advantages, importantly it's open source with the opportunity to participate in development.  If you have ideas for development - join the community and improve it! It has an easy and intuitive UI so you can start work with it quickly and easily.</p>

<p>The disadvantage of KmPlot is that it has no support on mobile platforms. This type of application is often necessary when there is no access to computer.</p>

<p>Of course, there are applications with greater functionality (Gnuplot, Gri, Grace and others) but all of them require a much greater level of training to work with them and their interface is not as friendly as in KmPlot.</p>

<p>KmPlot helps me in these problems for my homework. With KmPlot I can be sure my answers are correct.  It helps with research of functions.  KmPlot allows to edit functions as you like, which allows me to understand their properties.</p>

<p>As a student I have KmPlot in the category of "must have" application, and I think it will remain so for a long time. I recommend it to my friends.</p>
