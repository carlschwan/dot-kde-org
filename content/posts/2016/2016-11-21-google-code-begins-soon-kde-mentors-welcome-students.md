---
title: "Google Code-in begins soon; KDE mentors welcome students"
date:    2016-11-21
authors:
  - "valoriez"
slug:    google-code-begins-soon-kde-mentors-welcome-students
---
<p><a href="https://codein.withgoogle.com"><img src="https://developers.google.com/open-source/gci/images/contest-site-screen.png" alt="Google Code-In" style="float:right;width:207px;height:133px;"></a>

<p>The KDE community will once more be participating in Google Code-in, which pairs KDE mentors with students beween the ages of 13 and 18 to work on tasks which both help the KDE community and teach the students how to contribute to free and open source projects. Not only coding, but also documentation and training, outreach and research, quality assurance and user interface tasks will be offered.</p><p>Students who are interested in contributing, see&nbsp;https://codein.withgoogle.com for more information.</p><p>Mentors, please subscribe to the KDE-Soc-Mentor mail list, and write to kde-soc-management@kde.org if you have not yet been invited to the GCi webapp. Now is the time to login and begin adding your tasks. <em>Former Google Summer of Code students are especially welcome to mentor.&nbsp;</em></p><p>&nbsp;</p>

<!--break-->