---
title: "KDE Talk Videos from QtCon"
date:    2016-09-05
authors:
  - "jriddell"
slug:    kde-talk-videos-qtcon
comments:
  - subject: "Videos marked as private"
    date: 2016-09-05
    body: "<p>I just wanted to have a look and found that all videos are private and not publically accessible.</p>"
    author: "Robby"
  - subject: "Videos marked as private"
    date: 2016-09-05
    body: "Sorry folks, fixed now\r\n"
    author: "jriddell"
  - subject: "Slides?"
    date: 2016-09-07
    body: "<p>Are there plans for a central place to look at presentation slides? Would be great :)</p>"
    author: "Simon Schmei\u00dfer"
  - subject: "Thanks"
    date: 2016-09-14
    body: "Thanks for producing these and making them available - some excellent talks there!"
    author: "bluelightning"
---
<p>QtCon talks are over, and today we start the discussion groups and hacking sessions to plan out work on the KDE community's projects over the coming year. If you want to learn what's going on in KDE technologies and community you can spend some time watching over the videos from the <a href="https://www.youtube.com/playlist?list=PLsHpGlwPdtMogitRYzwPz4dWTVsZRGwts">QtCon KDE talks</a>.</p>