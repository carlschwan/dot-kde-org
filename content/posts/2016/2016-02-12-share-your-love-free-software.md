---
title: "Share your love for Free Software"
date:    2016-02-12
authors:
  - "sandroandrade"
slug:    share-your-love-free-software
---
<a href="http://fsfe.org/campaigns/ilovefs/2016/" title="#ilovefs banner"><img src="/sites/dot.kde.org/files/ilovefs-banner-extralarge.png" width="627" height="105" alt="#ilovefs banner" style="padding: 2ex"></a>

Love! Ah, that “serious mental disease” which make us reveal the best (and, sometimes, the worst) of our unconscious fantasies, longings, fears, defenses and internal images. And we love many things, sometimes in unthinkable and wacky ways. Some people love the melancholic sound of the cello, ice-cream, dancing, and reading, while others may even love insects, slowly pulling shoelaces out of sneakers, or that feeling after sneezing. It doesn't matter what, we love anything that makes our neurotransmitters dancing frantically and aimlessly like there is no tomorrow.
<!--break-->

<figure style="float: right; margin: 0px">
<a href="/sites/dot.kde.org/files/ilovefs-gallery-15.jpg" title="KDE loves free software!"><img src="/sites/dot.kde.org/files/ilovefs-gallery-15.jpg" width="400" height="300" alt="KDE loves free software!" style="padding: 2ex"></a>
<figcaption><center>KDE loves Free Software</center></figcaption>
</figure>

Yes, we love Free Software and this readily means that we love technology, people, social equanimity, and the various meanings one may take on for the word “freedom”. We care about it and we all want to bear witness of the growth and consolidation of new projects, and the progress of elder ones into full-fledged solutions driven by healthy and thriving communities. Free Software communities are inherently diverse and put together people with different motivations, expectations, and interests. Some are there to make friends and advance their technical and social skills, while others want to pursue the dream of an open world or even have Free Software as their daily paid job. In spite of such a diversity, one thing unite all of us in this Free Software odyssey: we love what we do.

On February 14th 2016, Free Software Foundation Europe (FSFE) celebrates the Valentine's Day with the seventh edition of the <a href="http://fsfe.org/campaigns/ilovefs/2016/">#ilovefs campaign</a>. It's a moment to show our appreciation and say “thank you” to Free Software contributors and whole communities around the world. Let everyone involved in all Free Software awesomeness know how much you love them: developers, translators, designers, testers, documentation writers, packagers, and promotion people. You can participate in many different ways:

<ul>
<li>Engage in the #ilovefs competition.</li>
<li>Tag with #ilovefs in Twitter or GnuSocial that cool video/picture of yourself or your friends or something completely different showing all your love to Free Software.</li>
<li>Thank your favorite Free Software project or community by sending one of the campaign's postcards.</li>
<li>Create a beautiful "I love Free Software" day by using the hashtag #ilovefs when sharing your pictures, videos, and blog posts. Disseminate that feeling and watch others reverberate all gratitude and acknowledge Free Software deserves.</li>
<li>Just do something else … be creative and lovesick!</li>
</ul>

Happy #ilovefs day to everyone!
