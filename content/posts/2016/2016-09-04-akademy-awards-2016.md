---
title: "Akademy Awards 2016"
date:    2016-09-04
authors:
  - "jriddell"
slug:    akademy-awards-2016
---
<div style="padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://www.flickr.com/photos/104544784@N04/29370954391/sizes/l"><img src="https://c1.staticflickr.com/9/8447/29370954391_82b3672918_b.jpg" alt="" /></a><br />Winners Kenny, Dan, Christoph, Dominik, Aleix</div><p>QtCon talks closed with our annual awards ceremony, the Akademy Awards. Given each year to the most valued and hardest working KDE contributors, they are awarded by the jury from the previous year. This year's winners are:</p><h2>Best Non-Application Contribution</h2><p>Aleix Pol who for many years has worked hard not just on KDE code but also on the community with KDE e.V. as a board member and KDE España.</p><h2>Best Application</h2><p>Dominik Haumann and Christoph Cullmann for their work making Kate and the related parts. We all rely on a quality text editor and KDE has the finest one.</p><h2>Jury Award</h2><p>To Daniel Vrátil and the KDE PIMsters for creating and maintaining the largest suite of communication applications in the world.</p><h2>The Organisers</h2><p>As is traditional, an award was given to the organisers of Akademy, this year represented by Kenny Coyle who has been helping out for nearly a decade running the videos and many other tasks.</p><!--break--><p>&nbsp;</p>