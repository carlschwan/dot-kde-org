---
title: "KDE neon User Edition 5.6 Available now"
date:    2016-06-08
authors:
  - "jriddell"
slug:    kde-neon-user-edition-56-available-now
comments:
  - subject: "Convert Ubuntu/Kubuntu installation to NEON?"
    date: 2016-06-20
    body: "<p>I did not manage to install Neon on my laptop. Yet Kubuntu did install just fine.</p><p>Is there any possibility to convert a fresh Kubuntu / Ubuntu installation into Neon \"afterwards\"?</p><p>&nbsp;</p>"
    author: "thomas"
  - subject: "How is HiDPI support?"
    date: 2016-06-09
    body: "<p>How is HiDPI support?</p>"
    author: "Casey"
  - subject: "It is a wonderful thing!"
    date: 2016-06-09
    body: "<p>It is a wonderful thing! Works really sleek, and from here looks pretty usable. When I'm to move onto Linux, I'll use it.</p><p>Keep it up.</p>"
    author: "ConcernedCitizen"
  - subject: "imropve default fonts"
    date: 2016-06-09
    body: "<p>$ cd /etc/fonts/conf.d/</p><p>$ ln -s ../conf.avail/10-sub-pixel-rgb.conf</p><p>$ ln -s ../conf.avail/30-droid-noto.conf</p><p>$ ln -s ../conf.avail/30-droid-noto-mono.conf</p>"
    author: "jk"
  - subject: "Torrent ?"
    date: 2016-06-10
    body: "<p><span style=\"font-size: medium;\">It would be awesome if you provide a torrent for download.</span></p>"
    author: "Alaa"
  - subject: "KDE Neon 32-bit"
    date: 2016-06-14
    body: "<p>Hello!</p>\r\n<p>Please also have a 32-bit version available for CD-ROM images (to burn/boot, so we can add packages later as needed).&nbsp; Some of us can only use the old PCs.</p>\r\n<p>Thanks!</p>"
    author: "Anonymous"
  - subject: "I Love It! <3"
    date: 2016-07-27
    body: "<p>I've been using openSUSE for a while and it just seems bloated, but KDE Neon has no bloatware, just KDE Programs, I would recommend this to a new Linux user!</p>\r\n<p>&nbsp;</p>\r\n<p>Keep up the good work #KDECommunity</p>"
    author: "Piggyuniform"
  - subject: "KDENLive crashes after recent update and more"
    date: 2016-11-25
    body: "<p>So after the recent update KDENLive now crashes when ever a video clip is added to the project and also it won't keep my sound card preferences it keeps trying to use my HDMI connection as the default sound card. Both of these things don't make it a stable platform. I moved to Neon so I could have the best KDENLive experience however that is turning out not to be the case. I have filled bug reports and will give it a week. If in that time its not fixed, then I will go back to Linuxmint KDE with the older version of KDENLive at least there it was stable and worked every time. Come on guys please please be more careful with the updates.</p>"
    author: "Scott"
---
KDE is thrilled to announce the first at-large version of KDE neon User Edition.

<div style="text-align: center;"><a href="/sites/dot.kde.org/files/installer.png">
<img style="border: thin solid grey; padding: 1ex;" src="/sites/dot.kde.org/files/installer-wee.png" alt="" /></a><br />
KDE neon User Edition 5.6 installer</div>

KDE neon User Edition 5.6 is based on the latest version of Plasma 5.6 and intends to showcase the latest KDE technology on a stable foundation. It  is a continuously updated installable image that can be used not just for exploration and testing but as the main operating system for people enthusiastic about the latest desktop software. 
<!--break-->
It comes with a slim selection of apps, assuming the user's capacity to  install her own applications after installation, to avoid cruft and  meaningless weight to the ISO. The KDE neon team will now start adding all of KDE's applications to the neon archive.
 
Since  the announcement of the project four months ago the team has been  working on rolling out our infrastructure, using current best-practice devops technologies. A continuous integration Jenkins system scans the download servers for new  releases and automatically fires up computers with Docker instances to build packages. We work in the open and as a KDE project any KDE developer has access to  our packaging Git repository and can make fixes, improvements and inspect  our work. 

With the major infrastructure now in place we will start  adding continuous builds of all KDE applications and projects.

Before installation of KDE neon User Edition, we suggest you read the <a href="https://community.kde.org/Neon/UserEdition/5.6">release notes</a>, eye over information about the <a href="https://www.kde.org/announcements/plasma-5.6.4.php">latest release of Plasma</a>, check <a href="https://community.kde.org/Neon/UserEdition/5.6#Minimum_Requirements">minimum requirements</a>, <a href="https://community.kde.org/Neon/UserEdition/5.6#Notable_Issues">notable issues</a> and  follow the <a href="https://community.kde.org/Neon/UserEdition/5.6#How_to_install">install instructions</a>.
 
For developers and advanced testers we have the <a href="https://neon.kde.org/develop">KDE neon Developer Edition</a> available with the latest in-development Plasma straight from Git.

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;  text-align: center;"><a href="/sites/dot.kde.org/files/harald.jpg"><img src="/sites/dot.kde.org/files/harald-wee.jpg" alt="" /></a><br />Hard at work on KDE neon</div>

<p>KDE neon Developer Editions are also available for those wanting to test the latest code straight from Git, either stable or unstable branches.</p><p>"We're really exited to be bringing KDE software directly to users who are enthusiastic about open source and picky about their operating system," said KDE neon developer Jonathan Riddell "although there will always be plenty of options for getting KDE software, by working directly with the KDE teams writing the software and using modern deployment techniques we aim to give the best experience to the fans that they deserve."</p>

