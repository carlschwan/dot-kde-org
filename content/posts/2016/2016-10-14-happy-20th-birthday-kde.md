---
title: "Happy 20th Birthday, KDE"
date:    2016-10-14
authors:
  - "nightrose"
slug:    happy-20th-birthday-kde
comments:
  - subject: "Congratulations and many thanks"
    date: 2016-10-14
    body: "<p>for all these years of fantastic work!...</p><p>KDE has been one of my favorite OSS projects since I use Linux. I started with 3.x version many years ago, and today it's on my work and home desktop.</p><p>Many thanks for all people who are or has been involved with KDE.</p>"
    author: "Hugo OH"
  - subject: "Congrats"
    date: 2016-10-14
    body: "<p>Congratulations to all the community that in many ways make it possible!!</p><p>Keep on hackin' in the free world!!</p><p>&nbsp;</p>"
    author: "victorhck"
  - subject: "Congrats!"
    date: 2016-10-14
    body: "<p>Thank you for all of your work over the years! KDE has been and remains the best OSS desktop and one of the best OSS communities out there today. It has always been a great plesure to run KDE software!</p>"
    author: "Thomas S Hatch"
  - subject: "Congratulations!"
    date: 2016-10-14
    body: "<pre id=\"tw-target-text\" class=\"tw-data-text tw-ta tw-text-small\" style=\"text-align: left; height: 48px;\" dir=\"ltr\"><span lang=\"en\">Congratulate the people who have made this great project! You are awesome! Long live to KDE, long live to Free Software!</span></pre>"
    author: "baltolkien"
  - subject: "Happy Birthday KDE!KDE had"
    date: 2016-10-14
    body: "<p>Happy Birthday KDE!<br /><br />KDE had been my favorite desktop enviroment for more then ten years since I started to learn about Linux with a Conectiva Linux running KDE 3.<br /><br />Congrats!</p>"
    author: "Bruno Tonia"
---
<figure style="width: 252px; float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/konqi-20.png" /></figure> 

20 years ago today Matthias Ettrich sent an <a href="https://groups.google.com/forum/embed/#!topic/comp.os.linux.misc/SDbiV3Iat_s%5B1-25%5D">email that would mark the start of KDE</a> as we know it today - a world-wide community of amazing people creating Free Software for you. In his email he announced the new Kool Desktop Environment and said “Programmers wanted!” <a href="https://timeline.kde.org/">In the 20 years since then so much has happened</a>. We released great software, fought for software freedom and empowered people all over the world to take charge of their digital life. In many ways we have achieved what we set out to do 20 years ago - “a consistant, nice looking free desktop-environment” and more. Millions of people use KDE’s software every single day to do their work, have fun and connect to the most important people in their life. And yet we still have a long way ahead of us. Our job is far from done.

Today Free Software and KDE matters more than ever before. Only through Free Software can people truly break out of the walled gardens technology is so often building these days and stand up to surveillance. But Free Software communities like KDE also matter because they bring the world closer together. Our community is a truly global one. It is one that strives on mentoring people and letting them reach their true potential - be it as a programmer, artist, translator, community organizer, system administrator, tester or any of the hundreds of roles that make a community like KDE work.

No matter if you’ve been around KDE since the very beginning or if you just joined us for a short while: Thank you for being a part of our journey so far. And if you’ve not contributed yet, today is the best day to start.

I am looking forward to many more years in this great community building Free Software for and with you.

Here’s to many more years of KDE! <a href="https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary">Come celebrate with us</a>.
<!--break-->
