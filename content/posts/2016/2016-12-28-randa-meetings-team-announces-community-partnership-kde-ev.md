---
title: "Randa Meetings Team Announces Community Partnership with KDE e.V."
date:    2016-12-28
authors:
  - "valoriez"
slug:    randa-meetings-team-announces-community-partnership-kde-ev
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/mascot_konqi-commu-randa.png" width="400" height="283" /></figure> 

<p>The team behind <a href="http://randa-meetings.ch">The Randa Meetings</a> is pleased to announce a community partnership with the KDE e.V. The Randa Meetings is the largest sprint organized by KDE, where roughly fifty KDE contributors meet yearly in the Swiss Alps to enjoy seven days of intense team work, pushing KDE technologies forward.</p>

<p>For the past years the sprints were organized by a core Swiss team and supported by the KDE community. Organizing sprints was challenging, but the result always justified the efforts. To keep the yearly sprints going and make easier the work for the Swiss core team, the Randa Meetings have now become an official KDE e.V. community partner.</p>

<p>This decision allows the Randa Meeting to simplify their workflows like organizing fundraising and traveling reimbursements. It removes pressure from the organizers and allows them to spend more time on the sprint and their own personal affairs like family or the building of a new house.</p>

<p>The Randa Meetings will continue with a yearly meeting. For 2017, the date is already set to 10 September to 16 September. This year the meetings will be focused on accessibility and personal information management (PIM). We want to make our software accessible for people with visual disabilities and also to make our software accessible from different operating systems, different devices and with different user interfaces, such as graphical with keyboard and mouse, or touch or speech and other senses.</p>

<p>We look forward to greeting you in 2017 and wish you all a good new year!</p>
<!--break-->
