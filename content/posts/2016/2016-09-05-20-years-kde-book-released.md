---
title: "\"20 Years of KDE\" book released!"
date:    2016-09-05
authors:
  - "devaja"
slug:    20-years-kde-book-released
comments:
  - subject: "Congrats to the whole"
    date: 2016-09-05
    body: "Congrats to the whole community for 20 years of going strong.\r\n\r\nThe KDE Timeline is really nice. However, I think it misses some important projects. There is a lot of emphasis on Plasma.\r\n\r\nBut I'm sure KHTML and Konqueror, Amarok, Okular (or KPDF), Kontact, Krita (and Calligra), Dolphin and many more projects deserved a spot in there."
    author: "emilsedgh"
  - subject: "\"There is a lot of emphasis"
    date: 2016-09-05
    body: "<p>\"There is a lot of emphasis on Plasma\"</p><p>Not really a surprise. The designers wants everyone to forget how good KDE was back in the 3.x days.</p>"
    author: "Anonymous"
  - subject: "Quality"
    date: 2016-09-05
    body: "<p>I am a big fan of KDE, but hope you will start focusing more on stability than adding more features. You still cannot beat KDE3 in that regards.</p><p>After each update I expect to get better, but KDE lately is very crashy even in your distro of choice - Arch</p><p>And again all these system services preventing running apps in other DE or as root (akonadi, MySQL etc.) and eating resources. Some times even KMail has hard time launching because of these cr*ps. Good luck launching simple KWrite editor as root or via ssh session.</p><p>&nbsp;</p><p>Basic 2 monitor setup is hit and miss - you never know on which display the app will launch. Taskbar also some times is duplicated on both screens, some times is extended - hard to tell when and why.</p>"
    author: "jet"
  - subject: "Don't forget to get a"
    date: 2016-09-06
    body: "<p>Don't forget to get a different ssl certifcate. The current one at 20years.kde.org is from startcom, whcih is likely to be removed from a lot of systems.</p>"
    author: "Evert"
  - subject: "Never run gui software as root"
    date: 2016-09-06
    body: "<blockquote><p>Good luck launching simple KWrite editor as root</p></blockquote><p>Although I think that this is offtopic to this news I want to point out that you should never ever run gui software as root. I recommend you to watch my talk about desktop security at QtCon, where I talk about running applications as root and what this implies. I also recommended all applications to check for being root and terminate. Please see: https://conf.qtcon.org/en/qtcon/public/events/324</p>"
    author: "Martin Gr\u00e4\u00dflin"
---
<img src="/sites/dot.kde.org/files/20yearsbook-Konqi.png" alt="" /> For all the gearheads around the world, the occasion of KDE's 20th birthday brings with it the traditional yet unconventional slice of our virtual birthday cake - our brand new book called <em>20 Years of KDE: Past, Present and Future</em> scribbled in icing on top. 

With the birth of KDE came about the birth of change, the birth of a little bit of brilliancy, and a presence across five continents that was unimaginable at the time of its conception. It was the start of something significant. And even though we've known KDE for so long, have been responsible for shaping it and making it what it is today and have interacted with it on a personal level since quite some time, there is a lot more about KDE that not all of us know and a whole lot more about it that we are yet to discover. As we stand today with a vision in our pocket trying to learn, decide, imagine and tame the unforseeable future of KDE, we also find strength, satisfaction and validation in our past and in the large part of the never-ending journey that we have already traveled. Our destination terminates not in perfection, but in freedom and power infinite. Power to you, and to KDE. 

The book, in its nostalgic travel through our timeline, gives the reader insights into KDE like never before. The many stories and the many ways in which our contributors have envisioned the path ahead for us, are there for you to read in '20 Years of KDE'. Matthias Ettrich, the founder of KDE, gives you one such very important side and story about the inception of KDE when he says, "Like any other complex project, KDE was created twice. At first as an idea, and secondly as an implementation of the idea in the real world." With people who've been around at times drastically different from now, Richard Moore shares a humbling reflection down the memory lane saying ''If you had said to me at the time that KSnapshot would continue to be developed and released for 18 years I wouldn't have believed you.'' Albert Vaca expands more on what might be in store for KDE in the book saying "Our future software will run on devices we have yet to conceive of and will do things for our users that have yet to even be dreamed of. Yet one thing will remain the same – the creation of software people love – that will inspire the next group of contributors to our community." Andreas Cord-Landwehr rightly marks this milestone in the book when he states, "In a community we are people of various backgrounds, educations and ages, and with twenty years completed, we finished what one can call a generation."

Kevin Ottens sums up the need to go and get your copy at the earliest, "Why am I telling you all this you may ask? Well, I just want you, dear readers, to realize that it has been an awfully long time!" So, Happy Birthday KDE! I hope you like your birthday cake. Find more details about the book at <a href="https://20years.kde.org/book">20years.kde.org/book</a>!