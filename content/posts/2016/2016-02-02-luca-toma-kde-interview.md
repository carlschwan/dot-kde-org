---
title: "Luca Toma KDE Interview"
date:    2016-02-02
authors:
  - "jriddell"
slug:    luca-toma-kde-interview
---
<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/sites/dot.kde.org/files/lucatoma-wee.jpg" width="300" height="300" /><br />Luca Toma</div> 

Google Code In is our annual project to give tasks to school pupils to contribute to KDE projects.  One task this year is to write a Dot article and top Code In student Stanford L has interviewed <a href="http://en.wikitolearn.org/Main_Page">WikiToLearn</a> contributor and Sysadmin Luca Toma. 

<!--break-->
<b>Please tell us a little about yourself</b>
I am a second year physics student studying at the University of Milan Bicocca. I’ve been passionate about computers since the age of 10, especially the part of sysadmin / networking.

<b>What do you do for a living?</b>
Currently, I am a student and I work within an office that deals with management of business systems and website development.

<b>What do you do for KDE?</b>

At this time my contribution in KDE is WikiToLearn, my role is the system administrator. I take care of the maintenance of the infrastructure and server project.

<b>How did you get into computer programming?</b>
I started programming when I was 12 years of age, I was intrigued by a friend working on a GNU/Linux distribution using Bash, just like in the movies. I started with VB6, after which was passed on to C / C ++, in order and PHP.

<b>Do you have any advice for people who would like to pursue computer programming as a major?</b>
My advice is to write the code on what you know to be able to understand what you want and to be able to make the most out of it.

<b>Who is your role model, and why?</b>
I do not have a well-defined role model because I try to take inspiration from the best of all. Einstein created a model for what concerns thinking in their own way and I think it is extremely important to be able to solve problems in the best way.

<b>What are some ways you motivate yourself?</b>
One thing that motivates me is to do my best.
I think if each one of us always did their best in situations, it would be the best for everyone.

<b>Do you have a vision, like where do you want KDE in general to be in 5 years and sysadmin in particular?</b>
I hope that KDE will become a reference point for all those who want to learn computer science.
A community that is able to support projects (e.g. WikiToLearn) while providing all necessary resources, both in terms of computing power, and that of access to the necessary knowledge but also as a community in which to grow.
From the perspective of a sysadmin, to be able to provide the right environment is necessary to continue and pursue their development
