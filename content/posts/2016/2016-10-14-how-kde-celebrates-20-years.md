---
title: "How KDE Celebrates 20 Years"
date:    2016-10-14
authors:
  - "jriddell"
slug:    how-kde-celebrates-20-years
---
KDE is 20 years old, a community working on beautiful software to free the world and spread privacy, all while having a lot of fun which we do it.

In cities across the world there are <a href="https://community.kde.org/Promo/Events/Parties/KDE_20_Anniversary">parties being held this weekend</a> to celebrate.  As we write the KDE Korea party in Seoul is setting up for some talks and drinks.
<img src="/sites/dot.kde.org/files/kde20-korea2.jpg"  width="240" height="320">&nbsp;<img src="/sites/dot.kde.org/files/kde20-korea3.jpg.jpeg" width="320" height="240">

Some 20 year parties have already been held such as at FISL in Brazil last month.

<img src="/sites/dot.kde.org/files/kde-cake2.jpg" width="341" height="256" />

Showing the strength of the KDE development community, our flagship product Plasma released its first Long Term Support edition.  The Linux Action Show, never ones to shy away from critisism, <a href="http://www.jupiterbroadcasting.com/103736/plasma-5-8-shines-bright-las-438/">give it a thorough review</a> and decided it was "light years ahead" and had "more compelling features" than the competition.

<video id="video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="none" width="854" height="506" poster="http://jb7.cdn.scaleengine.net/wp-content/uploads/2016/10/las438-v.jpg" data-setup='{ "plugins" : { "resolutionSelector" : { "default_res" : "SD" } } }' > <source src="http://www.podtrac.com/pts/redirect.webm/201406.jb-dl.cdn.scaleengine.net/las/2016/linuxactionshowep438-432p.webm" type="video/webm" data-res="WEBM" /> <source src="http://www.podtrac.com/pts/redirect.mp4/201406.jb-dl.cdn.scaleengine.net/las/2016/linuxactionshowep438-432p.mp4" type="video/mp4" data-res="SD" /> <source src="http://www.podtrac.com/pts/redirect.mp4/201406.jb-dl.cdn.scaleengine.net/las/2016/linuxactionshowep438.mp4" type="video/mp4" data-res="HD" /></video>

And if being 20 years old makes you feel old you can look back at the latest release, KDE 1.  Helio has brought the classic version back to life.  There are even <a href="http://jriddell.org/2016/10/14/kde-1-neon-lts-20-years-of-supporting-freedom/">Docker images you can install yourself from KDE neon</a>.

<a href="http://www.heliocastro.info/?p=291"><img src="/sites/dot.kde.org/files/desktop2-768x432.png"></a>

If you haven't read them already take a look at the <a href="https://timeline.kde.org/">20 Years of KDE timeline</a> and download or buy the <a href="https://20years.kde.org/book/">20 Years of KDE book</a>.
<img src="https://20years.kde.org/book/assets/img/frontcover.png" width="210" height="297" />

Brazilian IT website <a href="http://vidadesuporte.com.br">Vida de Suporte</a> did a special comic for KDE:
<img src="http://i1.wp.com/www.angrycane.com.br/wp-content/uploads/2016/09/Tirinha_KDE.jpg" />
Support: Hey, look, KDE is 20 years old!
Intern: Oh, damn it, I tougth it was a pokemon.

Let us know how you celebrate this anniversary how what you think KDE can do in the next 20 years to spread freedom, privacy and community.

Comments welcome here or on <a href="https://www.reddit.com/r/linux/comments/57fik3/kde_celebrates_20th_birthday/">Reddit thread</a>.
<!--break-->
