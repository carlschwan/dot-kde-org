---
title: " Krita 2016 Kickstarter: Let's Make Text and Vector Art Awesome"
date:    2016-05-09
authors:
  - "Boudewijn Rempt"
slug:    krita-2016-kickstarter-lets-make-text-and-vector-art-awesome
comments:
  - subject: "Why is KDE-Krita love is asymmetrical?"
    date: 2016-05-10
    body: "<p>I see KDE mentioning Krita in lots of places, but I can't find any single mention of KDE in the Krita website.</p><p>&nbsp;</p><p>Does anyone know why?</p>"
    author: "John Martin"
  - subject: "Mostly just because of"
    date: 2016-05-10
    body: "Mostly just because of iterative website redesign that focused on only presenting the information a user would really need. But you've got a good point, and I've brought it to the attention of the webmaster just now. I even had a hard time finding the vision statement: https://docs.krita.org/KritaFAQ#What_is_Krita.3F, which mentions KDE really explicitly. We're trying to fix this now."
    author: "Boudewijn Rempt"
  - subject: "the same apply for calligra office"
    date: 2016-06-14
    body: "<p>The same apply for http://calligra.org/ , No much credit for calligra or kde in krita.<br />Though i love them all kde, calligra and krita and i wish them to continue to be the best.</p>"
    author: "carl"
---
<div style="float: right; padding: 1ex; margin: 1ex; "><a href="/sites/dot.kde.org/files/texteditor-mock.jpg"><img width="300" src="/sites/dot.kde.org/files/texteditor-mock.jpg"/></a></div>

Today we kick off the third Krita kickstarter! It's beginning to become a tradition. Last year, our Kickstarter backers funded the development of performance improvements and animation support -- and a bunch of other features. Right now, we're still working on finishing up the Krita 3.0 release before we implement the last of the stretch goals you all helped fund last year.

<a href="https://youtu.be/pThKpaDXxj8">Krita Kickstarter 2016 video</a>

But we're looking forward. And 2016/2017 will be the Year of Text and Vector. Krita's text and vector tools were shared with the other Calligra applications, and that's always been a difficult situation. Should they be optimized for office applications or for artistic applications?

With the port to Qt5 and KDE Frameworks 5, the tight connection between Calligra and Krita got broken, and now is the time to focus on making the text and vector tools great for artists. Out with the bibliographical database, in with OpenType based fine typographic control. Out with OpenDocument Graphics as the file format, in with SVG-2.
The tools themselves need to be rewritten, too, to make them more fun to use!

And beyond these two big projects, there are some exciting stretch goals, too! From smaller ones like composition guides to bigger ones like stroking vector paths with Krita's brush engines to the <i>really</i> big one: support for scripting Krita in Python!

<a href="http://www.krita.org/2016kickstarter"><img src="/sites/dot.kde.org/files/support-krita-2016-3.png"></a>

We're also doing something special to celebrate our third kickstarter project: After the kickstarter is funded, we will commission Krita artists from all over the world to create art for us that we will use in various rewards! We're even planning an artbook.

Join in this year's <a href="http://www.krita.org/2016kickstarter">Krita Kickstarter campaign</a> and tell the world about it.