---
title: "KDE e.V. Quarterly Report - 1st Half of 2015"
date:    2016-04-27
authors:
  - "sandroandrade"
slug:    kde-ev-quarterly-report-1st-half-2015
---
<div style="float: right; padding: 1ex; margin: 1ex; "><img src="/sites/dot.kde.org/files/ev_large200.png" /></div>
The KDE e.V. report for the first half of 2015 is <a href="https://ev.kde.org/reports/ev-2015H1/" target=”_blank”>now available</a>. It presents a survey of all the activities and events carried out, supported, and funded by KDE e.V. in that period, as well as the reporting of major events, conferences and mentoring programs that KDE has been involved in.

<h4>Featured Article – conf.kde.in 2015</h4>

The featured article covers a bit of <a href="https://conf.kde.in/" target="_blank">conf.kde.in</a> history and how it brought together nearly 300 students in its 2015’s edition. It presents an overview of conf.kde.in 2015 keynotes, information regarding the many talks about all sort of KDE projects presented at the conference, and details about the lab sessions devoted to game development with Qt and QML. The article concludes with some thoughts on the KDE presence in India and its massive potential for raising new contributors.

<h4>Other Activities</h4>

The report also describes a synopsis of member activities during the first half of 2015. The Plasma Sprint, in Barcelona, focused mostly on lowering contributions barriers, improving power management, and Plasma’s future goals. During the KDE PIM Spring Sprint, in Toulouse, several contributors started the port of Kontact Suite to KDE Frameworks 5 and did a massive cleanup of KMail reported bugs. The 3<sup>rd</sup> edition of <a href="https://br.kde.org/lakademy-2015" target="_blank"> LaKademy – the Latin­-American KDE Summit</a> – took place at Federal University of Bahia (in the city of Salvador) and contributed to establishing a regular and consolidated venue for discussing actions, making contributions of many facets, and supporting newcomers for KDE in Brazil and Latin America.

<h4>Results</h4>

The report concludes with a summary of activities undertaken by the Sysadmin Working Group, and the contributors who joined KDE e.V. during the period. We invite you to read the <a href="https://ev.kde.org/reports/ev-2015H1/" target=”_blank”>entire report</a>!