---
title: "Three Weeks Until QtCon!"
date:    2016-08-12
authors:
  - "unknow"
slug:    three-weeks-until-qtcon
---
<div style="float: right; padding: 1ex"><img src="/sites/dot.kde.org/files/QtCon16_Logo_250.png" alt="QtCon" /></div>
From 1 to 4 September 2016 the communities of KDE, Qt, FSFE, VideoLAN and KDAB join forces in Berlin for <a href="https://qtcon.org/">QtCon</a>. The program consists of a mix of Qt trainings on day 1, unconference sessions, lightning talks and more than 150 in-depths talks on technical and community topics on days 2 to 4. Track topics range from <em>KDE‘s Latest and Greatest</em>, <em>Testing and Continuous Integration</em> and <em>QtQuick</em> to <em>Free Software policies and politics</em>, <em>Community</em> and <em>Beyond code</em>. Check out the <a href="https://conf.qtcon.org/en/qtcon/public/schedule">program</a>.

The main conference days will take place at the <a href="http://www.bcc-berlin.de/en">bcc</a>. The KDE community part of the conference with <a href="https://akademy.kde.org/2016">BoFs and hacking</a> will happen at the Technical University of Berlin from 5 to 8 September.

If you haven't <a href="https://conf.qtcon.org/">registered</a> yet, do it now!

Attendance for QtCon (but not for the Training day) is generally free for community members if you register in advance. However, we're asking for a donation during the registration process that will help to cover parts of the organization costs (venue, catering which includes lunch and so on) not covered by our sponsors. There's a recommended amount, but any contribution will be welcome. Thank you for your support.

Also, please be aware that you will have to pay a full fee of 200 Euro if you register on the day, so make sure to register through the website now.

Looking forward to seeing you in Berlin!
