---
title: "WikiToLearn Reaches 1.0"
date:    2016-11-03
authors:
  - "jriddell"
slug:    wikitolearn-reaches-10
---
<figure style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><a href="https://blogs.wikitolearn.org/2016/11/announcing-wikitolearn-1-0/"><img src="/sites/dot.kde.org/files/wikitolearn-wee.jpeg" width="300" height="301" /></a></figure> 

<a href="https://www.wikitolearn.org">WikiToLearn</a> is KDE's project to create textbooks for university and school students. It provides free, collaborative and accessible text books. Academics worldwide contribute in sharing knowledge by creating high quality content.

One year after founding WikiToLearn, the love for sharing knowledge helped our community to grow stronger. During this year a lot of great things happened, but we also had to face some technical and organizational problems.

The effort and work of our community during the last months helped us to achieve great results developing new features and improving our platform, concentrating on the editing experience. As a result of this, we are very proud to announce the release of <strong>WikiToLearn 1.0</strong>!

<strong>Here are some new features:</strong>

<ul>
<li> <strong>New skin</strong>: we rebuilt the whole skin for a better user experience. Users will now be able to take and review notes, create books and share knowledge more easily.</li>
<li> <strong>Course editor</strong>: this tool allows users to create and manage courses in a guided and very effortless way</li>
<li> <strong>Visual editor</strong>: together with <i>Course editor</i> this tool will help users to contribute easily; editing and reviewing has become more intuitive and it’s not necessary anymore to be a “<i>wikitext expert</i>” to work on our platform</li>
</ul>

Having reached the goal of a more comfortable user interface, to make it more easy for you to contribute, our next objective is to expand the content on all of our pages. Since we already have some content on our Italian and English pages, we would also very much appreciate if you would help us to develop the pages in other languages.

Get started now, visit <a href="https://www.wikitolearn.org">WikiToLearn.org</a> right away and get in touch with our new features!

<center><iframe src="//www.youtube.com/embed/BTS-YKnJYv0?rel=0" width="560" height="315" allowfullscreen="allowfullscreen"></iframe></center>
<!--break-->
