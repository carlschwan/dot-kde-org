---
title: "KDE e.V. Joins Advisory Board of The Document Foundation"
date:    2016-06-16
authors:
  - "nightrose"
slug:    kde-ev-joins-advisory-board-document-foundation
comments:
  - subject: "The Document Foundation announcement"
    date: 2016-06-16
    body: "https://blog.documentfoundation.org/blog/2016/06/16/kde-e-v-joins-advisory-board-of-the-document-foundation/"
    author: "tsdgeos"
---
Today we are delighted to announce that KDE e.V. is joining the <a href="https://www.documentfoundation.org/governance/advisory-board/">advisory board of The Document Foundation</a>, the foundation backing LibreOffice and the Document Liberation Project. The Document Foundation also joins KDE e.V.'s group of advising community partners as an affiliate.

The KDE Community has been creating Free Software since 1996 and shares a lot of values around Free Software and open document formats with The Document Foundation, and brings the experience of running a Free Software organization for almost two decades to their advisory board. Both organizations are working in the OASIS technical committee for the OpenDocument Format. We also collaborate on common aspects of development of office software, such as usability and visual design. The affiliation of KDE e.V. and The Document Foundation on an organizational level will help to move forward with the shared goal of giving end users control of their computing needs through Free Software.

"The KDE community is one of the essential pieces of the FLOSS desktop initiative, and we are most happy to liaise with them now also on an organizational level. Beyond the shared goal of liberating people's software experience, there are also lots of synergies in areas ranging from document filter technologies over to volunteer-driven governance to explore", says Thorsten Behrens, founder and member of the board at The Document Foundation.

Lydia Pintscher, president of KDE e.V., says "KDE e.V. was one of the first Free Software non-profit organizations incorporated according to German law. We are happy to share some of the learnings we have made over the many years running KDE e.V. Free software and open formats are two of the cornerstones which unite us with many other organizations, such as The Document Foundation, working on establishing freedom for users of digital devices everywhere. In order to achieve our vision of a world in which everyone has control over their digital life and enjoys freedom and privacy we need strong partnerships with like-minded organizations like The Document Foundation."