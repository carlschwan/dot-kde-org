---
title: "KDE Plasma 5.7 Beta"
date:    2016-06-17
authors:
  - "jriddell"
slug:    kde-plasma-57-beta
comments:
  - subject: "frameworks 5 - general print dialog without cups options?"
    date: 2016-06-29
    body: "<p>I did the step to upgrade to latest Kubuntu 16.04 LTS... when trying to print with a kf5 based app (e.g. gwenview) I am missing the functionality to adjust cups variables (tab \"extended\" is missing at the properties print dialog), whereas with kde4-based apps (e.g. okular) it is still present. Am I missing something or was this imprtant functionality \"ripped\" of kf5?</p><p>Any hints appreciated...</p>"
    author: "thomas"
  - subject: "Cool!"
    date: 2016-06-17
    body: "<p>Agenda Items in Calendar are back :-) Also the other changes look great. THX</p>"
    author: "Till Eulenspiegel"
  - subject: "Thank you for nice job!!!"
    date: 2016-06-18
    body: "<p>Thank you for nice job!!! Especial thanks for implementing personal events (agenda view) in Calendar widget!!! It is really crucial feature for Plasma and great step forward! I was looking forward for it from Plasma 5 very beginning.</p><p>One small bug (or misfeature) related with it: when Plasma starts, the events are not available in calendar. One has to open 'Digital Clock Settings' dialog, choose 'Calendar' page, uncheck and check again 'Holidays' and 'PIM Events Plugin' and press 'Apply', and only after that events will appear. And so after each Plasma start. Tesdted in openSUSE Leap with Argon repo, Neon unstable and KaOS with kde-next repo. I very hope, that in 5.7 it will be OK.</p>"
    author: "Menak Vishap"
  - subject: "Thanks for performance and wayland support"
    date: 2016-06-18
    body: "<p>Thanks for the performance improvement and wayland support. You can do more</p><p>I hope too less memory usage.</p>"
    author: "BRULE Herman"
  - subject: "Volume control above 100% ?"
    date: 2016-06-18
    body: "<p>Hi,</p><p>whats the reason for feature overriding volume above 100% ? If sound volume is too low, it is better to turn up your amplifier, than turning volume above 100% because of clipping issues. If you also implement volume meter and clipping for each application THEN settting volume above 100% would make sense. Anyway thank you for your work on KDE5!</p>"
    author: "Standa"
  - subject: "calendar modules"
    date: 2016-06-18
    body: "<p>How do you activate PIM Events Plugin? I can only chose/activate \"Holidays\"... Is it a separate package I need to install?</p><p>Thanks!</p><p>(openSUSE Leap 42.1)</p>"
    author: "alfaflo"
  - subject: "calendar modules"
    date: 2016-06-18
    body: "<p>Hello, did you something special to add \"PIM Events Plugin\" calendar module? I can see only \"Holidays\".</p><p>(openSUSE Leap 42.1)</p><p>Kind regards</p><p>Florian</p>"
    author: "alfaflo"
  - subject: "Another awesome release in the pipeline"
    date: 2016-06-18
    body: "<p>Plasma keeps getter better and better. Looking forward to this release!</p>"
    author: "rob@hasselbaum.net"
  - subject: "Accessibility Support"
    date: 2016-06-19
    body: "<p>Great Improve to Plasma!</p><p>Do you think include accessibility features in future releases?</p><p>Some good accessibilities:</p><p>* Text-to-Speech like Orca</p><p>* High Contrast button</p><p>* Key sound when tap</p><p>* Accessibility at login screen</p><p>Thanks!!!</p>"
    author: "KDE User"
  - subject: "Changelog"
    date: 2016-06-19
    body: "<p>Links does not work.</p>"
    author: "kuede"
  - subject: "Fixed"
    date: 2016-06-19
    body: "Fixed"
    author: "tsdgeos"
  - subject: "calendar modules"
    date: 2016-06-20
    body: "<p>I am sorry, I don't know how it happened, but after installing some calendar-related package it appeared in my Leap. Unfortunately, I did not track what I had installed because I needed to install a lot to get everything working. In Argon live media it is also works since June 3. In Neon and KaOS it is still unavailable.</p>"
    author: "Menak Vishap"
  - subject: "Thank you and.."
    date: 2016-06-21
    body: "Thank you guys you are making my everyday life easier and more beautiful. It would be great if you will make fingerprint auth support in kde :( thank you again, you are great!"
    author: "uhu"
  - subject: "Goodbye KDE"
    date: 2016-06-29
    body: "<p>More and more projects are abandong bloated ugly slow crash prone KDE: next in line - QubeOS:</p><p>&nbsp;</p><p><em>So, <a href=\"https://github.com/QubesOS/qubes-issues/issues/2119\" target=\"_blank\">what's wrong</a> with the latest KDE we got in Qubes 3.2? A few things:</em></p><ol><li><p><em>It seems very unstable. I installed and tested on several machines with different GPUs (some very old ones!) and on every one I keep getting plasma/kwin crashes every few hours. This is just ridiculous.</em></p></li><li><p><em>KDE has a reputation for being heavy and slow, but this latest release seems to beat itself in this respect beyond any reasonable expectation. This is clearly visible on some HiDPI systems, where the number of pixels (more specifically: memory pages) to process causes our GUI daemons to draw very slowly. Admittedly there are some bottlenecks in our GUI protocol (specifically that the actual lists of PFNs are copied), but switching from KDE to Xfce4 visibly improved the graphics speed from totally-not-usable to somehow-slow. Apparently KDE (kwin) tries some composition magic that slows down the system. Perhaps there are humans who could understand the KDE's graphics settings dialog-box, but surely this is beyond my mental powers.</em></p></li><li><p><em>The KDE's infamous bloated UI got even more bloated. Specifically the new \"Start Menu\" distracts the user even more from the Qubes isolated domains concept.</em></p></li><li><p><em>While this is a very subjective opinion, the KDE is ugly, and it's getting uglier with each new release. Plus it completely doesn't match the mostly-gnome apps we have in AppVMs by default (and which we have because they are so much lighter than their KDE alternatives)</em></p></li></ol><p>&nbsp;</p><p>P.S. Not so quick fixes will be:</p><ol><li>Reduce the bloat (<a href=\"http://i.imgur.com/7ZuZYyL.png\" target=\"_blank\">Plasma shell</a> with <strong>zero</strong> processes/applets/etc running occupies over <strong>180MB</strong> of RAM; Krunner - <strong>130MB</strong> - WTF?!)</li><li>Fix most crashes instead of working on new shiny features</li><li>Revert back to KDE3/4 visual style instead of this Windows-8-like grayish abomination</li><li>Animations should not block user's interaction - i.e. when I click the start menu I should be able to click any further item immediately instead of waiting for animation to finish</li></ol>"
    author: "birdie"
  - subject: "Qubes - let's just remember"
    date: 2016-06-29
    body: "<p><em>Qubes - let's just remember this name to avoid any deal with this system in future.<br /></em></p>"
    author: "Menak Vishap"
  - subject: "Re: Goodbye KDE"
    date: 2016-07-05
    body: "<p>Although your comment is a week old and I'm not that familiar with Qubes, I'd like to add some points:</p><p>According to the page of Qubes, 3.2 is not a stable final release yet and thus not recommended for daily use.</p><p>You can change the speed of the animations or completly turn them off, as well as compositing, if that leads to performance or productivity improvements for you.</p><p>I don't get why KDE apps should match the style of GNOME apps, however there are Breeze and Oxygen Styles for GTK apps available, if that helps.</p><p>Plasma's primary goal isn't beeing a extreme lightweight DE, but to be visually appealing, feature rich and customizable. And this customizability also allows you to set everything to the Oxygen Style, if you are not a fan of Breeze (which I personally find modern and beautiful :)).<br /><br /><br /></p>"
    author: "somebody"
  - subject: "A late reply, I know, but"
    date: 2016-12-28
    body: "<p>A late reply, I know, but this seems to be a Qt5 issue:</p><ul><li>https://bugreports.qt.io/browse/QTBUG-54464</li><li>https://bugreports.qt.io/browse/QTBUG-3546</li></ul><p>&nbsp;</p>"
    author: "S. Christian Collins"
  - subject: "Accessing calendar events in KDE calendar widget"
    date: 2017-02-07
    body: "<p>I solved this by adding the package \"kdepim-addons\", couldn't have done it without this thread, thanks! Perhaps this will help someone else.</p>"
    author: "Niklas"
---
<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>


<figure style="float: none">
<a href="http://www.kde.org/announcements/plasma-5.7/plasma-5.7.png">
<img src="http://www.kde.org/announcements/plasma-5.7/plasma-5.7-wee.png" style="border: 0px" width="600" height="375" alt="KDE Plasma 5.7" />
</a>
<figcaption>KDE Plasma 5.7</figcaption>
</figure>


<p>
Thursday, 16 June 2016. Today KDE releases a <a href="https://www.kde.org/announcements/plasma-5.6.95.php">beta update to its desktop software, Plasma 5.7</a>.
</p>
<!--break-->
<h3>More Refined Breeze Experience</h3>

<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.7/login.png">
<img src="http://www.kde.org/announcements/plasma-5.7/login-wee.png" style="border: 0px" width="350" height="254" alt="Unified Startup Design" />
</a>
<figcaption>Unified Startup Design</figcaption>
</figure>

<p>This release brings an all-new login screen design completing the Breeze startup experience we trialed in Plasma 5.6. The layout has been tidied up and is more suitable for workstations that are part of a domain or company network. The Air and Oxygen Plasma themes which we still fully support for users that prefer a more three-dimensional design have also been improved.</p>

<br clear="all" />
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.7/icons-invert.png">
<img src="http://www.kde.org/announcements/plasma-5.7/icons-invert.png" style="border: 0px" width="167" height="102" alt="Icons Tint to Match Highlight" />
</a>
<figcaption>Icons Tint to Match Highlight</figcaption>
</figure>
<p>For improved accessibility, Breeze icons within applications are now tinted depending on the color scheme, similarly to how it's done within Plasma. This resolves situations where our default dark icons might show up on dark surfaces. </p>

<br clear="all" />
<h3>Improved Workflows</h3>

<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.7/krunner-jump-actions-wee.png">
<img src="http://www.kde.org/announcements/plasma-5.7/krunner-jump-actions.png" style="border: 0px" width="350" height="336" alt="Jump List Actions in KRunner" />
</a>
<figcaption>Jump List Actions in KRunner</figcaption>
</figure>

<p>In our previous release we added Jump List Actions for quicker access to certain tasks within an application. This has feature has been extended and those actions are also found through KRunner now.</p>

<br clear="all" />
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.7/pim-events-wee.png">
<img src="http://www.kde.org/announcements/plasma-5.7/pim-events-wee.png" style="border: 0px" width="350" height="207" alt="Agenda Items in Calendar" />
</a>
<figcaption>Agenda Items in Calendar</figcaption>
</figure>

<p>Plasma 5.7 marks the return of the agenda view in the calendar, which provides a quick and easily accessible overview of upcoming appointments and holidays.</p>

<br clear="all" />
<figure style="float: right">
<a href="http://www.kde.org/announcements/plasma-5.7/plasma-pa-drag-wee.png">
<img src="http://www.kde.org/announcements/plasma-5.7/plasma-pa-drag.png" style="border: 0px" width="350" height="330" alt="Dragging Application to Audio Device" />
</a>
<figcaption>Dragging Application to Audio Device</figcaption>
</figure>
<p>Many improvements have been added to the Volume Control applet: it gained the ability to control volume on a per-application basis and allows you to move application output between devices using drag and drop. Also implemented is the ability to raise the volume above 100%.</p>

<br clear="all" />
<h3>Better Kiosk Support</h3>

<p>The <a href="https://userbase.kde.org/KDE_System_Administration/Kiosk/Introduction">Kiosk Framework</a> provides means of restricting the customazibility of the workspace, in order to keep users in an enterprise or public environment from performing unwanted actions or modifications. Plasma 5.7 brings many corrections about enforcing such restrictions. Notably, the Application Launcher will become read-only if widgets are locked through Kiosk policies, i.e. favorites are locked in place and applications can no longer be edited. Also, the Run Command restriction will prevent KRunner from even starting in the first place.</p>

<h3>New System Tray and Task Manager</h3>

<p>The System Tray has been rewrittten from scratch to allow for a simpler and more maintaineable codebase. While its user interface has only seen some minor fixes and polishing, many issues caused by the complex nature of the applet housing applets and application icons within have been resolved.</p>

<p>Similarly, the task bar has gained a completely rewamped backend, replacing the old one that has already been around in the early days of our workspace. While the old backend got many features added over the period of time it was used, the new one has a remarkably better performance and could be engineered more cleanly and straight-forward as the requirements were known beforehand. All of this will ensure a greatly increased reliability and it also adds <a href="https://blog.martin-graesslin.com/blog/2016/06/a-task-manager-for-the-plasma-wayland-session/">support for Wayland</a> which was one of the most visible omissions in our Wayland tech previews.</p>

<br clear="all" />
<h3>Huge Steps Towards Wayland</h3>

<figure style="float: right">
<a href="https://c2.staticflickr.com/8/7418/26639077964_445c1761e3_b.jpg">
<img src="https://c2.staticflickr.com/8/7418/26639077964_445c1761e3_b.jpg" style="border: 0px" width="350" alt="Betty the Fuzzpig Tests Plasma Wayland" />
</a>
<figcaption>Betty the Fuzzpig Tests Plasma Wayland</figcaption>
</figure>

<p>This release brings Plasma closer to the new windowing system Wayland. Wayland is the successor of the decades-old X11 windowing system and brings many improvements, especially when it comes to tear-free and flicker-free rendering as well as security. The development of Plasma 5.7 for Wayland focused on quality in the Wayland compositor KWin. Over 5,000 lines of auto tests were added to KWin and another 5,000 lines were added to KWayland which is now released as part of KDE Frameworks 5.</p>

<p>The already implemented workflows got stabilized and are ensured to work correctly, with basic workflows now fully functional. More complex workflows are not yet fully implemented and might not provide the same experience as on X11. To aid debugging a new debug console got added, which can be launched through KRunner using the keyword “KWin” and integrates functionality known from the xprop, xwininfo, xev and xinput tools.</p>

<p>Other improvements include:</p>
<ul>
<li>When no hardware keyboard is connected, a <a href="https://blog.martin-graesslin.com/blog/2016/05/virtual-keyboard-support-in-kwinwayland-5-7/">virtual keyboard</a> is shown instead, bringing a smooth converged experience to tablets and convertibles</li>
<li>The sub-surface protocol is now supported which means that System Settings works correctly and no longer errorneously opens multiple windows.</li>
<li>Mouse settings, such as pointer acceleration, are honored and the touchpad can be enabled/disabled through a global shortcut. Touchpad configuration is still missing.</li>
</ul>

<div style="text-align: center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/_29BYcm8xKk" frameborder="0" allowfullscreen></iframe><br />
Virtual Keyboard Support in Plasma Wayland
</div>
<p><a href="https://www.kde.org/announcements/plasma-5.6.5-5.6.95-changelog.php">
Full Plasma 5.6.95 changelog</a></p>
