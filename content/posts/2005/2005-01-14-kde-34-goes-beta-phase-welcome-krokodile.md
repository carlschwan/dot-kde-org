---
title: "KDE 3.4 goes into Beta Phase: Welcome \"Krokodile\""
date:    2005-01-14
authors:
  - "skulow"
slug:    kde-34-goes-beta-phase-welcome-krokodile
comments:
  - subject: "YAY!"
    date: 2005-01-13
    body: "YAY!\n\n"
    author: "me"
  - subject: "Schni, schna, schnappi..."
    date: 2005-01-13
    body: "LOL ...everyone living in Germany now certainly knows what inspired\nthat name ;-)\nAnd I have an idea for a new default KDE startup sound (or rather:\nsong)!\n\nThe most exciting things for me:\n1) Improved KPDF (This will make it the best free PDF reader available)\n2) Passwordless KWallet (This stupid password thing was _really_ annoying)\n3) Themeable KDM (Will this be in for KDE 3.4? Would be great)\n\n"
    author: "Martin"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "perhaps people from around the world would like to know what song is on Place 1 ! in the German Charts since 2 weeks. Its called Schni Schna Schnappi from Krokodil. Perhaps you can find it somewhere , and keep in Mind : thats Place 1.\n\nok dont judge us by that :>>>"
    author: "ch"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "Is it as good as Wadde Hadde Dudde Da?"
    author: "charles samuels"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "You can see the schnappi video here: (RealVideo works with XINE)\nhttp://artists.universal-music.de/_real/schnappi/v/schnappi_schnappi.ram"
    author: "MaX"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "I am utterly speechless."
    author: "CPT"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "I would agree.\n\nIt's kind of mesmerising.  I've already watched it twice.\n\n\"Da Da Da\" - award winning lyrics from germany, indeed. :)"
    author: "charles samuels"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: "Don't forget about that other \"hit\" that toped the charts all over western Europe last year: o-zone's \"Dragostea din tei\", which has same shitty lyrics (maaa-iaaa-hiii, maaa-iaaa-huuuu).\n\nIs it just me, or people are getting dumber and dumber?"
    author: "Panzerboy"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: "Keep in mind that \"schnappi\" was designed to be a song for children in the famous children TV-show \"Die Sendung mit der Maus\".\n\nSo it's not the song but the taste of the masses which becomes more and more infatile.\n\nregards"
    author: "thefrog"
  - subject: "Something in the water makes people stupid..."
    date: 2005-01-14
    body: "Indeed.\n\nWhat is worser \"Schni, schna, schnappi\" (played in disco,\nfor small kids its ok), DJ Bobo/\u00d6tzi or Scooter?\n"
    author: "Mutator"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: "Still, it is not Banana Phone: http://www.mikeserve.info/raffi-banana_phone.mp3"
    author: "scaba"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-18
    body: "They even have a merchandising site! www.vdmcd.com"
    author: "Billy "
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-23
    body: "...or schnappi.tv (in German)\n\nIt is also very interesting to hear this song if you have had some beers intus!"
    author: "Carnevalier"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-27
    body: "It's hard to believe these songs topped European charts when Schni Schna Schnappy has barely made it to America. Same with Dragosta Din Tei. Here in the US, VERY different songs top our charts."
    author: "Asmodeus"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-10-28
    body: "well the schnappi song waz hhhhhhhhhhuuuuuuuuuuuuuuuuggggggeeeeeeeeee in australia.But now its the crazy frog."
    author: "michelle"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-30
    body: "Hi guys,\n\nI am from Germany. I read this topic in a German forum. I want to express myself times to the song \"Schnappi\".\n\nMany Germans hate the song. I also. And I cannot believe that it is sing by a 8 year old little girl.\n\nAt the beginning it was still funny, but with the time it is used only to the commerce.\nI legend: Away with \"Schnappi\".  Nobody needs this stupid, green crocodile.  \n\nFrom where do you actually know \"Schnappi\"? \n\nMany greetings from Germany\n\nchallo"
    author: "challo"
  - subject: "Schni, schna, schnappi..."
    date: 2005-03-31
    body: "I agree to most of the comments made so far...including the one about the beer. The song is hilarious when you're intoxicated. Besides...a little immaturaty is good for you. Y'know...in small doses.\nJase(New Zealand)(Yes, we have heard this song ALL the way down in New Zealand. And no, New Zealand is NOT a part of Australia, and no we do NOT live in mud houses.)"
    author: "Jase "
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-04-21
    body: "Hey people i live in newzealand and i love the schnappi song ;P its all over da place i hear this song on da radio !! :P ... and i think its cool coz im learning german at skool n yeh :P [ im 13 ] i hope theres more and NEW ZEALAND aint part of australia lol ... and of course we dnt live in mud houses wea on earth u gt dat frm ?"
    author: "Annie"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-05-06
    body: "the schnappi song is now at number 3 on the new zealand charts! it jumped 29 places in a week :) i love the song but after hearing it a number of times in a row it can get on ur nervs"
    author: "hailie"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-05-06
    body: "yeah i hate the schnappi song now .. i think it is stupid .. !! i want all mi old fav so\nngs to jump bk on da charts lol .."
    author: "Annie"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-05-07
    body: "Let me guess, you turned 14 between your two posts.  ;-)))\n\nSCNR. \n\n"
    author: "cm"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-10-09
    body: "It's been out in Australia for a little while, and doesn't seem to be going that great... after the onslaught of that bloody 'crazy frog' thing we do NOT need more stupid animated singing animals...\n\nHowever I don't mind it... I guess it is SLIGHTLY musical :P\n\nIt's catchy and cute- it achieves its purpose. I would not listen to it often, unless I just fancied learning the German lyrics, as my first and only language is English [excluding a little Italian]. However, I think I would prefer \"99 Red Balloons\"!\n\n- grace\n\nP.S No, New Zealand is not a part of Australia, but we're still mates, and it's only natural for people to associate NZ with Australia and vise-versa. HINT: ANZAC.\n\nP.P.S I know where you're coming from about the mud huts. Just because we're isolated, it does not mean we are a third world country (or countries, when including New Zealand)!!! I get it even worse as I live in Perth- I even get it from other Australians!"
    author: "-grace-"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-11-18
    body: "Can't say that Crazy Frog has won me over either.  I do love Schnappi though, it has made me laugh on many a Saturday morning! I would love a translation of the lyrics as my German is a little shaky!\n\nP.S - try living out bush, most people think I've never heard of email, let alone use it!!!"
    author: "alex"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2006-09-04
    body: "i love that song!!! plus schnappi is super cute!! :P but the song gets a bit annoying after a while cos im learning german at school and we listen to it like EVRY LESSON!! "
    author: "anna"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-05-31
    body: "Schnappi has hit Australia.....\n\nCame in through New Zealand and has just started on radio.\n\nGod help us all.\n\nAnd to top it all of I just heard that f@#$#^%g frog song!\n\n"
    author: "simon"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-05-10
    body: "it's an ok song if ur bored and need something-anything to listen to... and I like the Dragostea din tei.  Well not the english version so much..."
    author: "..."
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-08-31
    body: "hey everi1 \n\ni thort that this iz a FAB song, its so cute, i luv it!!!! also i do German at skool so i can undastand a tiny amout of it (im 13) i spos i luv it so much cuz it's in a different language to english lol ( i am from australia and no we DO NOT ride kangaroos to skool lol hoo started that one?)\n"
    author: "la la la la lauren"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-12-12
    body: "Look Germans are cool (although my dad says they are pricks) but the language sounds cool.. so I won't judge germany on some little song... but I like the song.. I know it's not a real song and i don't mosh to it for god sakes but it's cute and sounds cool.. plus I am begining to learn german sooooo... it's ,cool.. but F!@#@#** crazy frog can burn!!! man, that would have to be the lamest thing on the plant right now.\n\nSchnappi is doing ok in Australia, It was pretty big for a little while but it's lost appeal I think.. but I still think it's cool.\n\nIf I had to choose between rap and schnappi.. Scnappi kicks raps ass.. rap can also burn...\n\nI think it's pretty stranger I am writing this in december when this entry was writen in january... \n\n\nconclusion..\nschnappi kicks crazy frogs ass... germany is cool\n\npeace.but burn crazy frog and his censored areas."
    author: "Melissa"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2006-06-03
    body: "Jem, Blarney and Robin, our three dogs, love the Schnappi song.  We recorded it into a dog toy and they fight over it when it is playing.  Then they go into a tug of war."
    author: "Blarney"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2006-12-15
    body: "Hey, I heard this song in German class and though it was cute. From the US too."
    author: "Someone"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2008-06-20
    body: "Schni schna schnappi was not the number one hit because it is a great song everyone is walking around hearing this song on his iPod...\n\nIt is just funny to listen to this song BECAUSE the lyrics are this simple and there is a funny dance to... well don't you have some songs for children that are so ammusing that it get's popular all over the country?\n\n\nWadde hadde dude da - was published by Stefan Raab, famous entertainer since years.\nThis song was intended to be funny, less then it was intended to be a great hit. And it WAS funny, if you know how it came to this song:\n\nStefan told the audience that he was sitting somewhere watching a woman with her dog and the dog had something in his mouth, saying \"wadde hadde dude da\"\nI try to translate..\n\"was hast du da\" -> \"what do you got there\"\n\"wadde hadde dude da\" (look at the additional letters) is just a childish way of pronounce the sentence..\nwhat sounds cute but is a little bit riddiculous ^^\n\nGermans are really funny people so they are addicted to some funny songs ;) that's all :)\n\ngreets from cologne!"
    author: "Timme"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "Thx for the Passwordless KWallet, kde guys! I'll be updating to kde3.4 as soon as it comes out, just because of this single bugfix.\nIt is really annoying to login and then, after the startup sequence, having to insert the same password again for Kopete...\nKudos!"
    author: "blacksheep"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: ">1) Improved KPDF (This will make it the best free PDF reader available)\n\nwhy people need kdpf? I feel really comfortable with kghostview. What can kdpf what kghostview can't? And why always so many programs for one task?\n\n>2) Passwordless KWallet (This stupid password thing was _really_ annoying)\n\nAnd how will the password protected?"
    author: "pinky"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "> What can kdpf what kghostview can't? \n\nLook at the open \"most wanted\" KGhostView features: Continuous page mode, search function, saving of selected text, 2 pages at once, ..."
    author: "Anonymous"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-13
    body: "\"why people need kdpf? I feel really comfortable with kghostview. What can kdpf what kghostview can't? And why always so many programs for one task?\"\n\nIn KDE 3.3 they're pretty similar.  But both of them are dreadfully slow when displaying large PDFs compared to the Adobe Acrobat Reader.  The new KPDF should fix that as well as adding a lot of cool features like continuous page view and text select and links.\n\n\"And how will the password protected?\"\n\nIt will be protected in that you have to be logged in before you can see your passwords.  I'd still keep my kwallet password though.  KWallet stores too much sensitive information to be fast and loose with security."
    author: "Leo Spalteholz"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: "\"It will be protected in that you have to be logged in before you can see your passwords.\"\n\nahh.. only by system user/group settings.  Anyone who can get access to the kwallet file (root user, someone who has local access to your machine, a compromised system, etc.. etc.. etc..) will have full access to your password list.  Maybe that is OK when you are only storing your kopete passwords, but if you are using kwallet to store anything more useful than that.. then God help you.\n\nBobby"
    author: "brockers"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-15
    body: "If the one who wants to read your data is root, all he has to do is install a keyboard sniffer.\n\nYou know, to any half-awake hacker that will delay him about 50 seconds, plus whatever it takes for you to log back in. And some of the data will still be in caches anyway.\n\nIMHO, the password only protects you from someone stealing your data storage. Which is good, but not terribly likely. And you could just encrypt your $HOME and mount/decrypt on login (much better idea). Anyone knows of a reasonably simple way to do it?\n"
    author: "Roberto Alsina"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-16
    body: "Qryptix is supposed to do something like that -- or so the author claimed when we were talking at Linux Bangalore.  Sounds pretty interesting, actually -- it uses PAM as the authentication mechanism for the encryption / decryption.\n\nhttp://sourceforge.net/projects/qryptix"
    author: "Scott Wheeler"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: ">2) Passwordless KWallet (This stupid password thing was _really_ annoying)\n\nPersoanlly, I have to disagree with you on this. While I understand why george did it (to appeas ppl such as yourself), I wish that he had not. But a quick suggestion (new company is blocking bugs.kde.org - how stupid  :( ); Please consider modifing it so that it goes password iff the user has a passworded login.\nNo sense helping the future crackers.\n\nThanx."
    author: "a.c."
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: "How would that provide any protection. Passwordless login only works for people who are at the physical computer I thought. And anyone with physical access to a computer could access anything in a passwordless kwallet."
    author: "Ian Monroe"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-01-14
    body: "Hang about, you wish he hadn't put the feature in?\n\nThis is just a matter of choice... if you want the extra security afforded by using a separate password for KWallet, go ahead and enable it.\n\nMeanwhile I'll keep the password disabled. I already enter a password to login, and I'm the only person who uses my computer. The second password is redundant.\n\nAs for passwordless logins, again, it is pointless for force that behaviour. If somebody has a passwordless login and they use kwallet, they presumably don't worry about other people even touching the computer and so won't want a password on KWallet either."
    author: "Tom"
  - subject: "KWallet Passwordless login"
    date: 2005-01-14
    body: "> The second password is redundant.\n\nThat is simply not true! The password encrypts your Wallet, which is a very important extra security. If someone gains access to your machine he can access all your passwords in plain text. If you have an extra Password in KWallet then even if your box is rooted the attacker still has to either crack the encryption or find a way to crack KWallet.\n\n> As for passwordless logins, again, it is pointless for force that behaviour\n\nNo, not realy. Especially for your usecase it would be more secure to have a passwordless login, and a password for kwallet than vice versa. \n\n/////\n\nI agree with you that entering two passwords at startup is tendious though, but the only real solution to this problem is to integrate pam into kwallet, so that kwallet gets the password you entered at login, and can decrypt the wallet with it. (Provided that both passwords are Identical)\n\n\n\n\n\n\n\n\n\n"
    author: "Fabio"
  - subject: "Re: KWallet Passwordless login"
    date: 2005-01-15
    body: "The KWallet master password *is* redundant.  Let's examine the possible attack scenarios:\n\n1. Rooted\nIf your box is rooted you are lost anyway.  The attacker can impersonate KWallet and capture your password, or he can read the passwords out of KWallet's memory directly as they are decrypted, or even impersonate your real login screen to capture your real password!  There is no protection from an attacker who is already root.\n\n2.  Your account compromised\nIf the attacker has cracked your user account but not gotten root access, he can still impersonate a KWallet dialog, read KWallet's memory, or install a keylogger and capture your password.  Anyway, since Linux has so many privelege escalation holes this will likely turn into scenario 1...\n\n3.  Other account on same computer\nAs I said above, this will likely turn into scenario 1 (rooted) momentarily.  But ignoring that, depending on the priveleges of the compromised account this is equivalent to either scenario 2 or scenario 4 below.\n\n4. Only read access to KWallet file\nThis is the only scenario where the second password could help you:  if an attacker can read your KWallet files but cannot do anything else.  However, we can protect against this attack in a way that is more secure than a password dialog, while being more convenient at the same time.  Instead of using a user-supplied (weak) master password, we can use a master key supplied by a daemon running as root and stored in a file readable only by root.  KWallet starts, asks the daemon for the user key, and then decrypts your passwords with it for the rest of your session, without ever asking you anything.  Since the key is autogenerated it can be very strong and immune to dictionary attacks, unlike a user-supplied password.  The only problem is in this case we need to cooperate with distros to install a KWallet daemon, and we have to do a thorough security audit on the KWallet daemon to make sure it doesn't have any security holes of its own.  It could be made extremely simple to make this an easy process.\n\nSo KWallet's password dialog is redundant and useless in most situations, and in the one situation where it is useful it can be replaced by something that is more secure *and* more convenient at the same time."
    author: "Spy Hunter"
  - subject: "Re: Schni, schna, schnappi..."
    date: 2005-05-15
    body: "http://www.crocodile-schnappi.co.uk\n\nIt's all about Schnappi... Finally in English!"
    author: "Gino"
  - subject: "screenshots, please :)"
    date: 2005-01-13
    body: "Hey guys, where the shots with comments showing us the new great additions?"
    author: "Saulo"
  - subject: "Re: screenshots, please :)"
    date: 2005-01-17
    body: "here there are some\n\nhttp://www.deviantart.com/view/14149449/"
    author: "Saulo"
  - subject: "Re: screenshots, please :)"
    date: 2005-01-17
    body: "How do you switch to another than the first picture on this site?"
    author: "Anonymous"
  - subject: "Re: screenshots, please :)"
    date: 2005-01-17
    body: "I had the same question: just download the .tar.gz file with the other pictures; the download button is in the same screen, above the 3.4 screenshot!"
    author: "Saulo"
  - subject: "Screenshots anyone? :)"
    date: 2005-01-13
    body: "Yay, I'm the first to ask for screenshots :)\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Re: Screenshots anyone? :)"
    date: 2005-01-13
    body: "Sorry, seems like I was late for four minutes ;-)\n"
    author: "Eleknader"
  - subject: "Re: Screenshots anyone? :)"
    date: 2005-01-13
    body: "time, time,...."
    author: "time watching"
  - subject: "Re: Screenshots anyone? :)"
    date: 2005-01-13
    body: "So who is going to be the first to post them?"
    author: "Leverkusen"
  - subject: "Re: Screenshots anyone? :)"
    date: 2005-01-15
    body: "A screenshot of 3.3 running the new and the old kpdf:\n\nhttp://darkshines.net/pub/kpdf-revolution.png\n\nYou can see screenshots of the new kicker eyecandy in aseigo.blogspot.com"
    author: "suy"
  - subject: "Re: Screenshots anyone? :)"
    date: 2005-01-16
    body: "I've installed suse 9.1 rpms, but looks like the new kicker eyecandy is out of KDE 3.41 beta1, is that right?\nIf so, will it go into beta2?"
    author: "Iuri Fiedoruk"
  - subject: "Feature complete?"
    date: 2005-01-13
    body: "Hmmm, looking at the feature plan, I couldn't help but notice that 70% of all features are still not in the green phase. Just go to the border between yellow and green items and look at where your scrollbar handle is. Is the plan out of date, are the remaining red features peanuts, or what is happening? By my comprehension, the beta phase of a piece of software begins when it is roughly feature-complete and needs a diverse installed base in order to uncover the numerous bugs that have invariably crept in. Yet \"Krokodile\" seems to be in mid-development.\n\nAm I misreading something here, guys (and girls)?\n\n--------\nRegarding the \"Schnappi\"-thingie; while I am German (only 20 years old,too), I usually avoid charts music. I am currently listening to this... song... and it is helping me remember why."
    author: "Andreas Steffen"
  - subject: "Re: Feature complete?"
    date: 2005-01-13
    body: "Some developers don't update the list when a feature is completed, other features just won't get done in time and will be moved to the next KDE version.\n\nThe feature plan isn't set in stone, if something doesn't get done it will just have to wait until KDE 4.0"
    author: "Leo Spalteholz"
  - subject: "Re: Feature complete?"
    date: 2005-01-14
    body: "This is the way it usually is.  Unfortunately at the end of the release process it always turns out that a number of planned features are scrapped because the developers couldn't manage to implement them in time.\n\nThis will be the case with a feature that I planned to implement, namely a network game feature for KReversi.  I would like to take this opportunity to apologize to all KDE users for this, but since I moved to a new apartment in december, I simply have had too little time to put into it.\n\nI'm sorry."
    author: "Inge Wallin"
  - subject: "Re: Feature complete?"
    date: 2005-01-14
    body: "No need to be sorry! \nThanks very much for working on KDE and on KReversi, when network support will be implemented I'll try and enjoy it for sure but the game is already very nice.\n\nCongratulation for new apartment and have a nice day ;) "
    author: "Luca"
  - subject: "Re: Feature complete?"
    date: 2005-01-14
    body: "I think KReversi can be released off the usual release schedule of KDE."
    author: "gerd"
  - subject: "Re: Feature complete?"
    date: 2005-01-14
    body: "\"I usually avoid charts music. I am currently listening to this... song... and it is helping me remember why.\"\n\nI personally think the song is charming. I don't think it should be on top of the charts and spinning off half a dozen remix versions, but that the song itself isn't that bad considering the source."
    author: "Brandybuck"
  - subject: "DBUS & IIIMF"
    date: 2005-01-14
    body: "I noticed these were not on the 3.4 build requirements list. I hope that we don't drop the ball on either of these technologies. IIIMF being the most important; cuz, yes, we have DCOP.  The most important piece of DBUS are the hooks in HAL, hotplug, etc. wrt to USB pluggable devices.\n\nAnyone in the know for Qt getting IIIMF support?  There's been a transition period where I've upgraded to the latest and greatest, but now I no longer can input in Chinese in KDE apps. Pretty disappointing; hope we see some improvements here.\n\nI'm extremely happy with what KDE has brought to my desktop. Just hope for additional polish around USB (kdebluetooth does really well here; good model) and IME."
    author: "Jeff Pitman"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "DBUS should be for kde 4.0. I have not idea what IIIMF even is. I would love it if people would define their terms. I have seen some really bad acronyms when doing programming and found up to 10 meanings for a single one and the context does not always make it even close to clear as to which one is meant. The best guess I have is some input framework although why that is needed I don't know."
    author: "Kosh"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "I know it's pretty tough to write IIIMF in google, but it's the \"Internet Intranet Input Method Framework\".\n\nAnd as for why it is needed: because over half the planet needs it to write in their native language. Might be hard for some to comprehend, but some languages require more than just the 101 keys on a keyboard.\n\nLearn Chinese sometime.  It'll put hair on your chest."
    author: "Jeff Pitman"
  - subject: "Or not."
    date: 2005-01-14
    body: "Most of my Chinese mates are _less_ hairy than I (I'm caucasian)."
    author: "Leon Brooks"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "What's wrong with SKIM? It works perfectly for inputting Chinese, Japanese, Korean, whatever you want. Don't tell me there's yet another standard for doing the same again."
    author: "koffice fan"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "USB pluggable devices is already working fine in 3.4! DBUS+HAL sends signal  to qt dbus bindings that media://'s kdedmodule is using.\n\nhttp://webcvs.kde.org/kdebase/kioslave/media/\n"
    author: "performa 300"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "This is great news! I'm glad they're using the same name as the /media directory. This will make it easier for users to make the connection.\n\nStill crossing my fingers for IIIMF, which is very important for wide adoption in Asia countries.  I'm certain Redflag and Asianux put in some pretty good integrations and hacks to get the system working.  But, I don't use those.  So, it'll be nice to get KDE working \"out-of-the-box\" for those of us on English-centric distributions."
    author: "Jeff Pitman"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "Sorry if I'm tedious but those kind of distributions are not \"english centric\" but more correctly \"latin centric\" (yes, ok, for sure english is the best supported language but also all other latin languages are well supported). Don't know the situation for cyrillic and so on, but I can imagine it has to be a pain in the ass for CJK users"
    author: "Davide Ferrari"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-14
    body: "Once again, SCIM/SKIM was developed for exactly this purpose. What's wrong with them?"
    author: "koffice fan"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-15
    body: "SKIM is a frontend to IIIMF.  IIIMF is a backend system that replaces the legacy XIM framework.  Question is does 3.4 have SKIM and is it fully functional or are we waiting for key changes at the Qt level before adoption?  Possibly in KDE 4?"
    author: "Jeff Pitman"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-15
    body: "Sorry, but you are wrong:\nskim is a frontend to SCIM, not IIIMF.\nDetails: http://scim.sf.net\n\nBTW: SCIM is one of the main competitors of IIIMF."
    author: "liucougar"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-01-15
    body: "SKIM is a frontend to SCIM (Simple Common Input Method), which is a replacement for XIM. SCIM is also a FreeDesktop project, so it has the backing of KDE and GNOME.\n \nI use SKIM here and the integration with all KDE applications and all programs which support SCIM (this includes all of GTK/GNOME) is flawless and my girlfriend commented that it's superior to the Windows solutions she was using.\n \nSo I'd say we're way further than patchy solutions for CJK input on the desktop."
    author: "koffice fan"
  - subject: "Re: DBUS & IIIMF"
    date: 2005-06-23
    body: "If your're still talking on this thread - just an observation:\nI've found it a complete impossibility to get Skim fully working so far.\nI've been studying chinese for a couple of years now and it'd be really useful. I think it'd make a massive difference to the uptake of KDE worldwide.\n\nI rate myself as competant with linux, but IMHO (in my humble opinion)its far from working out-of-box.\nAccurate help files would help too, but I guess thats an issue for the scim project."
    author: "Alister"
  - subject: "Error in the Info-page."
    date: 2005-01-14
    body: "The 3.4 info-page (http://www.kde.org/info/3.4beta1.php) still mentions Klassroom, and not Krokodile:\n\n\"Binary packages\n\nSome Linux/UNIX OS vendors have kindly provided binary packages of _Klassroom_ for some versions of their distribution....\""
    author: "Janne"
  - subject: "Fedora Packages?"
    date: 2005-01-14
    body: "Fedora Packages anyone? Could not find anything on http://kde-redhat.sourceforge.net/\n\n"
    author: "Anonymous"
  - subject: "LiveCD"
    date: 2005-01-14
    body: "I can't wait until a OneBase LiveCD is released with this :)"
    author: "Alex"
  - subject: "Re: LiveCD"
    date: 2005-01-14
    body: "Yeah, I hope someone makes a live cd of it, but looking at the onebase website, the forums[1] are pretty much dead,(hasn't been much ativity since october) so I wouldn't count on them :)\n\n[1]<A href=\"http://www.ibiblio.org/onebase/onebaselinux.com/Community/phpBB2/\">One base forums</A>\n\n"
    author: "Sam Weber"
  - subject: "no html links?"
    date: 2005-01-14
    body: "P.S. Why didnt that <A> work there?"
    author: "Sam Weber"
  - subject: "oh, nevermind, found in faq :)"
    date: 2005-01-14
    body: "qoute/\nKnown Bugs\n\nHTML comments are not yet allowed. The year and timezone is often not included in article dates. \n/qoute\n\n-Sam"
    author: "Sam Weber"
  - subject: "Waht i laways hated with KDE"
    date: 2005-01-14
    body: "- no round border selection of desktop icons as with gnome\n\n- ugly underline in Konqueror\n\n- Konqueror stability\n\n- Kontrol Center mess, no good theme engine, unsystematic settings and naming.\n\nI like KDE very much but KDE has to be polished. I currently use KDE 3.1 Usually it's fixed after an upgrade, let's see what will happen when I will switch to 3.4 \n\n"
    author: "Andre"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-14
    body: "> I currently use KDE 3.1\n\nThat's two year old software and not even a bugfix release! And you complain today about it?"
    author: "Anonymous"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-15
    body: "KDE 3.1 was released on January 28, 2003. Oh wait, it's 2005 now, so yes it has been almost 2 years. Time sure does fly.\n\nSo the original poster complained about minor bugs in 2 year old software that has had 12 !!! ( 3.1.1, 3.1.2, 3.1.3, 3.1.4, 3.1.5, 3.2, 3.2.1, 3.2.2, 3.2.3, 3.3, 3.3.1, 3.3.2) updates during those 2 years. Oh well, he's either a troll or a sloth."
    author: "Johan Veenstra"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-14
    body: "Lol\nYes please update to 3.3.\nIt has been a lot polished, cleaned and konqueror is really stable."
    author: "JC"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-14
    body: "> - no round border selection of desktop icons as with gnome\n\nhttp://www.kde-look.org/content/show.php?content=16962\nhopefully for kde 4.0 some of these changes will go into the main kde codebase\n\n> - ugly underline in Konqueror\n\nyou can disable it and have a windows-like 2-clicks behaviour\n\n> - Konqueror stability\n\nupgrade. konqui is rock-solid now\n\n> I like KDE very much but KDE has to be polished.\n\ni agree on this one: konqueror's default toolbar is too crowded"
    author: "Anonymous"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-14
    body: "> http://www.kde-look.org/content/show.php?content=16962\n> hopefully for kde 4.0 some of these changes will go into the main kde codebase\n\nFull Ack!"
    author: "gerd"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-15
    body: "> i agree on this one: konqueror's default toolbar is too crowded\n\nnot as of today's CVS it isn't."
    author: "Aaron J. Seigo"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-15
    body: "you mean the copy/cut/paste and print buttons are gone?\nthat would be a great news!"
    author: "Anonymous"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-17
    body: ">> - ugly underline in Konqueror\n \n> you can disable it and have a windows-like 2-clicks behaviour\n\nWhat?  I have single-click behavior and no underlines.  What version are you using?!?\n\nI'm using 3.3.2 (and have never seen the underline thing since, at least, 3.1 or so...)\n\nM.\n"
    author: "Xanadu"
  - subject: "Re: Waht i laways hated with KDE"
    date: 2005-01-17
    body: "I agree, KControl is a mess as it currently is. Nearly a year ago there was some talk of a new KControl. Does anyone have any updated info/links on this redesign? When will we see it?\n\n"
    author: "Kad"
  - subject: "Re: What I always hated with KDE"
    date: 2005-01-17
    body: "There is kdenonbeta/kcontrol4 if it is what you mean.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "what should get better in KDE"
    date: 2005-01-14
    body: "Desktop Icons: should be placed at that position after logout-login even if I enable \"align to grid\" (I think it's Kicker's fault)\nDesktop handling: if I click into the background image, no icon should be selected. It's now selected with a \"ghosted\" frame. Pls look at Gnome if you don't know what I mean. All in all, the Desktop Icon feeling in Gnome is a bit better.\n\nKonqueror: if I switch tabs, then Konqueror repaints the symbol toolbars (isn't it possible to do that like it is in Opera? Opera is also based on QT.\n\nKmail: smiley-_pictures_ instead of smileys (like in Thunderbird or Opera-Mail or a phpBB or ...)\n\nOk, this should be only some inspirations for the next version ;-)\n"
    author: "anonymous"
  - subject: "Re: what should get better in KDE"
    date: 2005-01-14
    body: "addon to Kmail suggestion: I mean smiley-pictures in _text_only_ mails, not html-based mails (i know that this one is possible)\n\nA text-only mail looks nicer with smiley pictures and maybe some users send with this \"feature\" text-only mails if they see some nice smiley graphics ;-)\n"
    author: "anonymous"
  - subject: "Re: what should get better in KDE"
    date: 2005-01-15
    body: "What you call \"ghosted frame\" is a standard focus recangle. It indicates that you can use the keyboard to select an icon. Everything else would be considered a bug, unless you disallow keyboard input to the desktop (but why would you want that?)\n\nI also don't understand your \"align to grid\" report. You seem to say that icons should not align to the grid even if \"align to grid\" is enabled, which doesn't make sense. Please try to phrase it more clearly what you meant and report it to bugs.kde.org for kdesktop."
    author: "Anonymous"
  - subject: "Stupid problem"
    date: 2005-01-14
    body: "Since I upgraded to KDE 3.4 Alpha 1, and now with the beta, my trashcan on the desktop has been changed to a file (which I have removed) and I cannot find a way to get a trashcan again on the desktop. Of course, I can access it using the trash:/ ioslave but it is not as user friendly.\nAnyone knows how to recreate a trash on the desktop whose icon changes when I delete a file and which I can empty with a right click.\nI know, that's stupid, but since I know the way to handle the trash has changed to allow multiple files of the same name to be deleted without the \"file already exist\" message, I wonder if there is a way out..\n\nRichard"
    author: "Richard Van Den Boom"
  - subject: "Re: Stupid problem"
    date: 2005-01-14
    body: "Hi Richard,\n\nI must say, isn't there some price you can be awarded for accomplishing this?"
    author: "ac"
  - subject: "Re: Stupid problem"
    date: 2005-01-15
    body: "like \"dumbest KDE user ever\"? :-)\nHonestly, I didn't do anything special whe I upgraded to KDE 3.4 alpha, and the trash was no more a trash. :-|\nI guess I just need to recreate a file in $HOME/Desktop or something. Isn't there nobody else to have this problem?\n\nRichard  "
    author: "Richard Van Den Boom"
  - subject: "Re: Stupid problem"
    date: 2005-01-18
    body: "cp $KDEDIR/share/apps/kdesktop/directory.trash ~/Desktop/trash.desktop"
    author: "Anonymous"
  - subject: "OK, a post bellow explained it all"
    date: 2005-01-15
    body: "I'm a dumb ass. :-D\n\nRichard"
    author: "Richard Van Den Boom"
  - subject: "Re: Stupid problem"
    date: 2005-01-14
    body: "Don't know for sure (can't test it right now, my KDE 3.4 is on a defect hard disc), but have you tried to create a link to trash:/ on your desk?"
    author: "SegFault II."
  - subject: "Re: Stupid problem"
    date: 2005-01-15
    body: "It works more or less but doesn't behave like the regular trash (change icon when something is put in it, allow to empty it by rightclicking the icon, etc.).\n\nRichard"
    author: "Richard Van Den Boom"
  - subject: "Re: Stupid problem"
    date: 2005-01-25
    body: "With the current CVS, any link to trash:/ has \"Empty Trash\" automatically.\nThe only thing missing when re-creating the link by hand is the EmptyIcon entry.\n\nNote that deleting the link to the trash isn't as easy anymore either."
    author: "David Faure"
  - subject: "Re: Stupid problem"
    date: 2005-01-17
    body: "You can put a link in your taskbar\nRight-click taskbar - add applet - Trashcan"
    author: "VenimK"
  - subject: "Re: Stupid problem"
    date: 2005-03-27
    body: "I have a related problem (albiet minor).  With the KDE trashcan being updated to use Trash ioslave instead of a plain ol' filesystem directory, my goofy little bash script replacement for 'rm' no longer works as nicely.  That is, my little script is not as integrated because I can't empty the items from the 'centralized' gui icon.  What would help me is a way to 'mv' files directly into the trash ioslave from a script; any suggestions/samples?\n\nThing is, the 'major' feature added here (allowing of dup filenames & location info) was never really an issue for me.  I would always simply choose suggest new name and rename when removing files via the gui.  The little script appends a time-stamp to the file name and moves it so duplicate filenames wasn't a problem there either.  Here's a copy of the script in case you were wondering:\n\n#!/bin/sh\nwhile [ $# -ge 1 ]; do\n    DATE=`date +'%m-%d-%Y@%H:%M:%S'`\n    NEW_NAME=${1%%/}\n    NEW_NAME=${NEW_NAME##*/}\n    mv $1 ~/Desktop/Trash/${DATE}-${NEW_NAME}\n    shift\ndone\n"
    author: "boredhacker"
  - subject: "Re: Stupid problem"
    date: 2005-03-27
    body: "Use kfmclient to delete the files then they'll end up in the trash can.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Stupid problem"
    date: 2005-03-27
    body: "Marvelous!  Now, I can simply change:\n\nmv $1 ~/Desktop/Trash/${DATE}-${NEW_NAME}\n\nto:\n\nkfmclient move \"$1\" trash:/\n\nand... JOY!  I never knew about kfmclient and kept googling for dcop ways of doing it but was coming up short.  In fact, this even relinquishes the need for my script to rename the file.\n\nThanks again."
    author: "boredhacker"
  - subject: "Re: Stupid problem"
    date: 2005-03-28
    body: "Well, simply replacing the 'mv' with kfmclient isn't as nice as I initially thought.  The script ends up taking WAY TOO LONG (because kfmclient is called from within the body of the loop, potentially many times).  As you may have guessed, the goal is to use 'rm' as an alias for this script so I don't 'accidentally' delete my files from a shell prompt.\n\nAnyway, I've settled on changing the script to be:\n\n#!/bin/sh\nDATE=`date +'%m-%d-%Y@%H:%M:%S'` #grab the date and time\nwhile [ $# -ge 1 ]; do\n    FILE_NAME=${1%%/}  # remove trailing backslash from $1 if it exists\n    DIR_NAME=`dirname ${FILE_NAME}`     #what directory is the file in?\n    BASE_NAME=`basename ${FILE_NAME}`   #same as..  BASE_NAME=${FILE_NAME##*/}\n    if [ ! -d ${DIR_NAME}/trashed-${DATE} ]\n        then  # if we don't have one, make a sibling temp directory\n        mkdir ${DIR_NAME}/trashed-${DATE}\n    fi\n    #move the requested files into the temp directory\n    mv ${FILE_NAME} ${DIR_NAME}/trashed-${DATE}/${BASE_NAME}\n    shift\ndone\n#move the temp directory into the KDE trashcan\nkfmclient move ${DIR_NAME}/trashed-${DATE} trash:/\n\n\nWith 'mv' in the body of the loop and kfmclient only called once (at the end, to move a temp directory into the kde trashcan) things zip along quickly.  In addition, placing the temporary folder in the 'BASE_NAME' directory before moving it via kfmclient allows the utilization of the trashcan's new ability to store the original location - not bad!  Now I'm happy again!  Once again, thanks for tellin' me about kfmclient.\n\nNow to test if KDE is running and do things the old way if it's not... any more suggestions, lol?\n\n;-)"
    author: "boredhacker"
  - subject: "Re: Stupid problem"
    date: 2007-11-08
    body: "For posterity I thought I'd post the latest version of the script I use.\n\n\n#!/bin/sh\n\nDATE=`date +'%m-%d-%Y@%H.%M.%S'` #grab the date and time\nWORKINGDIR=`pwd`\nFILE_CNT=0\n\nwhile [ $# -ge 1 ]; do\n    FILE_NAME=${1%%/}  # remove trailing backslash from $1 if it exists\n\n    if [ -e \"$FILE_NAME\" ]  # if the file exists regardless of type\n    then\n        FILE_DIR=`dirname \"${FILE_NAME}\"`  #what directory is the file in?\n        cd \"${FILE_DIR}\" # change to that directory\n        FILE_DIR=`pwd`   # so we can get the absolute path\n\n        #move the requested files into the Attic directory\n        if [ ! -d \"${HOME}/Attic/${DATE}${FILE_DIR}\" ]\n        then\n            mkdir -p \"${HOME}/Attic/${DATE}${FILE_DIR}\"\n        fi\n        cd \"${WORKINGDIR}\"\n        mv \"${FILE_NAME}\" \"${HOME}/Attic/${DATE}${FILE_DIR}\"\n        FILE_CNT=$(($FILE_CNT+1))\n    fi\n\n    shift\ndone\n\necho \"Cleared ${FILE_CNT} file(s).\""
    author: "boredhacker"
  - subject: "Re: Stupid problem"
    date: 2005-04-14
    body: "Hello,\n    I recently upgraded to KDE 3.4, and I am having some problems w/ my trash can.  When I try opening it, or moving files to it, I get this error:\n\"Could not start process Unable to crate io-slave: klauncher said: Unknown protocol 'trash'.\"\n\nI have not seen many people w/ this problem.  I really don't want to reinstall KDE, as I don't want to have to re-apply my settings.\n\nI am running SimplyMEPIS 3.3."
    author: "Mike"
  - subject: "Re: Stupid problem"
    date: 2005-07-11
    body: "I am having the same problem.  Updated to KDE 3.4 and trash no longer works.  Same error message.  I have seen a lot of complaints about this problem but no solutions that actually works.  If you find one I would be glad to know.  I have a trash icon with the usual right click stuff (which doesn't work) and a trash folder where I am putting stuff I cannot delete."
    author: "Ben"
  - subject: "Re: Stupid problem"
    date: 2005-07-24
    body: "got the same problem\nhere is a solution: http://www.linuxquestions.org/questions/showthread.php?threadid=339769"
    author: "sacha"
  - subject: "Looks great!!"
    date: 2005-01-14
    body: "Question: Why were the rounded corners and updated rubber band NOT included in this release?  I could care less about them but with the number of people who mention them (and the popularity of http://www.kde-look.org/content/show.php?content=16962) they really should be part of 3.4.\n\nBobby "
    author: "brockers"
  - subject: "Probably because they require QT patches (n/t)"
    date: 2005-01-14
    body: "(n/t)"
    author: "Jason Keirstead"
  - subject: "Re: Probably because they require QT patches (n/t)"
    date: 2005-01-14
    body: "and qt is not opensource :-)\n\nthats the price for using qt\n\nwith qt4 i somewhere read its possible"
    author: "ch"
  - subject: "Re: Probably because they require QT patches (n/t)"
    date: 2005-01-15
    body: "Qt IS Open Source\nQt IS licensed under the GPL\n\nYou could patch Qt if you wanted, but the KDE Team thinks there are more important things to do."
    author: "Pau Garcia i Quiles"
  - subject: "Re: Probably because they require QT patches (n/t)"
    date: 2005-01-15
    body: "people can patch Qt if they wish (i have in the past). so distributions could ship a patched Qt quite easily.\n\nthe patches are there and need to be merged upstream so that they become part of Qt proper before the behaviour is seen in KDE itself.\n\nthis is no different than asking, for instance, why one needs to use taskbarv2 to get transparent buttons in the taskbar. well, i haven't received a clean patch for it yet and haven't had time to do it. once it's been submitted upstream i'll merge it in. at this point that looks like KDE4.\n\nthis is no different than any other bit of Open Source software really; nothing to do with us thinking there are more important things to do, more like the maintainers of Qt either not liking the current patches or, being in the middle of a huge upgrade as they are, are busy with other things at the moment =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Probably because they require QT patches (n/t)"
    date: 2005-01-16
    body: "I thought most KDE developers worked off a patched Qt tree anyway? A sort of KDE fork, but I guess that's wrong then."
    author: "Jonas"
  - subject: "Re: Probably because they require QT patches (n/t)"
    date: 2005-01-15
    body: ">thats the price for using qt\n\nYes, and the price is free QA for all patches going into the base toolkit.\n"
    author: "Morty"
  - subject: "Re: Probably because they require QT patches (n/t)"
    date: 2005-01-18
    body: "QT is most assuredly opensource and in fact it is GPL.\n\nBobby"
    author: "brockers"
  - subject: "kdelibs won't compile (using konstruct) - libkdnss"
    date: 2005-01-14
    body: "On a G4 Powerbook:\n\nIn file included from libkdnssd_la.all_cpp.cpp:2:\nremoteservice.cpp: In function `void DNSSD::resolve_callback(_DNSServiceRef_t*, DNSServiceFlags, uint32_t, DNSServiceErrorType, const char*, const char*, uint16_t, uint16_t, const char*, void*)':\nremoteservice.cpp:144: error: `TXTRecordGetItemAtIndex' undeclared (first use this function)\nremoteservice.cpp:144: error: (Each undeclared identifier is reported only once for each function it appears in.)\nIn file included from libkdnssd_la.all_cpp.cpp:5:\npublicservice.cpp: In member function `void DNSSD::PublicService::publishAsync()':\npublicservice.cpp:127: error: `TXTRecordRef' undeclared (first use this function)publicservice.cpp:127: error: expected `;' before \"txt\"\npublicservice.cpp:128: error: `txt' undeclared (first use this function)\npublicservice.cpp:128: error: `TXTRecordCreate' undeclared (first use this function)\npublicservice.cpp:132: error: `TXTRecordSetValue' undeclared (first use this function)\npublicservice.cpp:133: error: `TXTRecordDeallocate' undeclared (first use this function)\npublicservice.cpp:140: error: `TXTRecordGetLength' undeclared (first use this function)\npublicservice.cpp:140: error: `TXTRecordGetBytesPtr' undeclared (first use this function)\nmake[5]: *** [libkdnssd_la.all_cpp.lo] Error 1\nmake[5]: Leaving directory `/home/wvl/konstruct/kde/kdelibs/work/kdelibs-3.3.91/dnssd'\n"
    author: "wvl"
  - subject: "kpdf - wow"
    date: 2005-01-14
    body: "I just tried out the new kpdf and all I can say is wow.\nWhat an amazing improvement, really the best pdf viewer I ever used.\nJust wanted to share my excitement and thank all the kde devs and especially the kpdf devs. "
    author: "ralph"
  - subject: "Re: kpdf - wow"
    date: 2005-01-15
    body: "yeah, kpdf is amazingly good in 3.4. i have but one word for it: DAMN! =)"
    author: "Aaron J. Seigo"
  - subject: "Re: kpdf - wow"
    date: 2005-01-19
    body: "Yes. The new kpdf is gorgeous. Can't wait for the presentation 'transitions' to be implemented. Thank you for freeing me from the crappy acrobat reader!"
    author: "Flavio"
  - subject: ":S"
    date: 2005-01-15
    body: "sweeeet finally got it working hahaha :D\n\ni love the new \"Trash\" kicker app... i wonder why they took out the devices kicker app tho.. that was pretty handy :/ anyone know where i can get it again?"
    author: "quaff"
  - subject: "Slackware is Best suited for KDE :)"
    date: 2005-01-15
    body: "Slacware is better for compiling programs, and I always find packages for Slackware quickly than any other distro.\n\nKDE was the reason for which I switched to Slackware. And Slackware is just amazing.\n\nThanks for the Slackware packages. "
    author: "Fast_Rizwaan"
  - subject: "Re: Slackware is Best suited for KDE :)"
    date: 2005-01-15
    body: "you're welcome."
    author: "JC"
  - subject: "SUSE rpms"
    date: 2005-01-16
    body: "I've loaded arts, kdelibs, kdebase - they look great - the kdepim stuff seems to show missing dependencies.  Do these go when you load them all?   Anyone know? "
    author: "gerry"
  - subject: "Re: SUSE rpms"
    date: 2005-01-16
    body: "Compare the offered files to the printed list of missing dependencies for an answer."
    author: "Anonymous"
  - subject: "Problem"
    date: 2005-01-16
    body: "This is odd but using the slackware .tgz packages I get this message when trying to send with kmail:\n\n\"Authorization failed, Authentication support is not compiled into kio_smtp. authentication not supported\"\n\nSo whoever packaged the slackware packages really should fix this. Or I'll just compile it myself. Definitely not like its the first time I've compiled KDE from source."
    author: "Matt"
  - subject: "Re: Problem"
    date: 2005-01-16
    body: "> So whoever packaged the slackware packages really should fix this.\n\nBecause you posted here? Really, lookup the packager in the README and write a mail."
    author: "Anonymous"
  - subject: "Re: Problem"
    date: 2005-01-16
    body: "Just looking at the 3.4 requirements suggests to me that you've not installed the SASL library."
    author: "Johan Veenstra"
  - subject: "Re: Problem"
    date: 2005-01-17
    body: "If you're good enough to compile yourself kde, just try to find the problem and send the fix. It will be even faster."
    author: "JC"
  - subject: "Re: Problem"
    date: 2005-04-18
    body: "So after we install Cyrus SASL plugins, do we have to reinstall kdebase?"
    author: "sleepkreep"
  - subject: "Re: Problem cyrus sasl"
    date: 2005-04-25
    body: "Hi,\n\nI installed the cyrus-sasl package and found that the problem does not lie with the mailer.  The cannot send error message persists.  The kde-base program must be compiled  with kio_smtp. authentication.\n\nAs much as I like kmail, the prospect of downloading the kdebase package, and installing it are daunting.  It just never seems to install where I want it to go, and then you never know what dependencies will be affecting the already installed kde packages.\n\nThanks\n\nDan"
    author: "Daniel Herkes"
  - subject: "Re: Problem cyrus sasl"
    date: 2005-07-06
    body: "Hi,\n\nActually I did recompile with the \"--with-kio_smtp\" option and it fixed the problem."
    author: "Daniel Herkes"
  - subject: "SUSE rpms"
    date: 2005-01-16
    body: "I've loaded arts, kdelibs, kdebase - they look great - the kdepim stuff seems to show missing dependencies.  Do these go when you load them all?   Anyone know? "
    author: "gerry"
  - subject: "Re: SUSE rpms"
    date: 2005-01-16
    body: "oops, sorry"
    author: "gerry"
  - subject: "finding missing libraries"
    date: 2005-01-16
    body: "the new SUSE RPM for kdepim is looking for these:\n\nlibkalarmd.so.0\nlibkmime.so.2\n\nhowever currently, my opt/kde3/lib seems to contain all these \n\nlibkalarmd.la  libkalarmd.so  libkalarmd.so.0  libkalarmd.so.0.0.0\n\nbut not libkmime.so.2\n\ncouldn't find it on google either\n\nany thoughts gratefully received\n\n"
    author: "gerry"
  - subject: "Sweeeeeeeeet"
    date: 2005-01-18
    body: "The new document relations toolbar rocks111!!oneoneone!!11"
    author: "Adrian"
---
A lot of development has happened since <a href="http://dot.kde.org/1102585658/">KDE 3.4 Alpha</a>, so we are now happy to publish <a href="http://www.kde.org/announcements/announce-3.4beta1.php">KDE 3.4 Beta 1</a> code named Krokodile.  For a list of already implemented new features skim over the <a href="http://developer.kde.org/development-versions/kde-3.4-features.html#inprogress">KDE 3.4 Feature Plan</a>. For sources and packages (only Slackware so far, more have been promised to be released in the next few days), please visit the <a href="http://www.kde.org/info/3.4beta1.php">KDE 3.4 Beta 1 Info Page</a> and browse the <a href="http://www.kde.org/info/requirements/3.4.php">KDE 3.4 Requirements List</a>. The <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build toolset has been updated for this release. KDE 3.4 Beta 2 is <a href="http://developer.kde.org/development-versions/kde-3.4-release-plan.html">planned for mid February</a> with a final release due for mid-March.





<!--break-->
<p>The KDE team asks everyone to try this version and give feedback through the <a href="http://bugs.kde.org/">bug tracking system</a>. Please make sure to check out the reworked panel and the KDE service auto discovery features. Note that the kdepim, kdevelop and kdewebdev modules also compile on KDE 3.3 systems.</p>



