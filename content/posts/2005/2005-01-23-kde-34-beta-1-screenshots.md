---
title: "KDE 3.4 Beta 1 Screenshots"
date:    2005-01-23
authors:
  - "aangler"
slug:    kde-34-beta-1-screenshots
comments:
  - subject: "A new control center"
    date: 2005-01-22
    body: "Settings:/ is nice, but it is not new and not especially convenient. Is there any effort underway to replace the myriad tree in KControl center with a search-based system? I've seen screenshots of such a design (kcontrol3) but would like to see them implemented... While we're at it is there any plan to get rid of the horrible Konqueror settings dialog and replace with a tree view?"
    author: "LuckySandal"
  - subject: "Re: A new control center"
    date: 2005-01-22
    body: "*Search* for settings? Using a textbox and a 'search' button? That sounds genuinely insane."
    author: "foo"
  - subject: "Re: A new control center"
    date: 2005-01-22
    body: "yeah, open the control center, and click the middle tab (top left, three tabs: index, SEARCH, help).\n\ntype your search term...\n\nmaybe this could be done better, but well, it works, doesn't it?"
    author: "superstoned"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "Maybe if you read kde-usability you would know that trees and hierarchies are not suitable interfaces. New users don't know where the hell in the tree to find their setting anyway. We could still have a tree, but it's just not going to be usable by homo sapiens. \n\nAnd yes, there is already a search tab, but it is not readily noticable, it should be on the front page. Also the most often used KCMs should be on that front page. We need something like a box that says \"Enter a setting you would like to change or a task you'd like to perform\" and then list tasks corresponding to those words. Not just an alphabetical list like we have now. "
    author: "LuckySandal"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "That might have been handy for me a couple of days ago.\n\nI thought my keyboard was broken because it wasn't pressing any keys.  Then 10 minutes later, I realised it worked when I held the keys down longer.  Switched keyboards and the same thing happened, so I knew it was in software.\n\nIf only I could have gone into KControl and searched for \"slow keyboard\", I might have found the option which turned itself on without hunting through the entire control panel thanks to keyboard settings being under Accessibility instead of Peripherals, where it SHOULD be.\n\nScore minus one to obscure gestures which are turned on by default and make life a living hell when activated accidentally.\n"
    author: "Trejkaz"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "Slow keyboard is accessibility feature, I would always expect it to be locaetd under Accessibility. Maybe the modules should specify more than one group they belong to but that is also unintuitive, unless you somehow make it obvious that these things are the same so that users don't become too confused what the difference might be.\n\nAnyway, if I go to KControl, click on \"Search\" and type in \"Pomal\u00e9 kl\u00e1vesy\" (that is Slow keyboard in Czech), it does return what I need. What else do you need?"
    author: "Lada"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "I dunno.  Part of the problem is that I didn't _know_ there was such a feature, so I wouldn't have even thought I could turn such a thing on.\n\nSuch an option really should be off by default."
    author: "Trejkaz"
  - subject: "Re: A new control center"
    date: 2005-01-24
    body: "Typically that feature is off by default. I know it's never been turned on for me."
    author: "Dolio"
  - subject: "Re: A new control center"
    date: 2005-01-24
    body: "The gestures were definitely on by default in the 3.4 beta.  They may have been on in 3.3 as well but I might just have never been unlucky enough to trigger them by accident."
    author: "Trejkaz"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "Well, Settings:/ looks VERY VERY nice!!! finally.\nWould be nice to see the internal screenshots of every icons.\nAnyone has such screenshots? please!\n\nOf course, you still have KControl as a \"Tweak UI\".\n\nhttp://eminor.antrix.net/gallery/file/kde_3.4_beta1/settings.png\n\nKControl3 is this ?!\n\nhttp://www.avenheim.online.fr/kcontrol3/index.png\nhttp://www.avenheim.online.fr/kcontrol3/appearance.png\nhttp://www.avenheim.online.fr/kcontrol3/search.png\nhttp://www.avenheim.online.fr/kcontrol3/kcm_font.png\n\nSincerely yours,\nFred."
    author: "fprog26"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "> Well, Settings:/ looks VERY VERY nice!!! finally.\n\nFinally? As in \"already in KDE 3.3\"?\n\n> Would be nice to see the internal screenshots of every icons.\n\nWhat? Clicking the icons starts the normal kcontrol modules."
    author: "Anonymous"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "I have never seen those screenshots before.  That is the most awesome\ncontrol center I have ever seen.  Man, when I use WindowsXP I don't ever\nknow where to find a setting.  It's like digging through a random\nmaze of icons, popup windows, tabs, trees, and buttons\n(the \"Classic\" style makes it easier to find stuff).\n\nAnd I have a Computer Science degree from MIT...I can't imagine how\nmy grandma can find anything on the WinXP Control Center.  I don't\nknow, maybe I don't have enough practice at thinking like Microsoft.\n\nKDE is very logical, but a tad convoluted.  Those screenshots do seem\nlike a good design.  Perhaps some tweaking would be needed, but overall,\nvery nice."
    author: "Henry"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "This is very true. Round here we like to bash the KDE Control Center a lot, but if you compare it to the Windows Control Center, it's a breeze to use!\n\nRecently I got to use a Windows computer computer again, the experience with the system settings was just horrifying. I had forgotten how bad it really is.\n"
    author: "Mark Kretschmann"
  - subject: "Re: A new control center"
    date: 2005-01-25
    body: ">This is very true. Round here we like to bash the KDE Control Center a lot, but if you compare it to the Windows Control Center, it's a breeze to use!\n\nIt certainly isn't bad. I work in a Mac (OS X) environment however, and I have to admit that Apple is certainly worth looking at in terms of usability design. Their  system preferences solution is simple - just right for the \"typical\" user.\n\nKDE control panel stumps even me from time to time."
    author: "Marek Wawrzyczny"
  - subject: "Re: A new control center"
    date: 2005-01-23
    body: "Yes, that's KControl3 which can be obtained from kdenonbeta/kcontrol3"
    author: "tpr"
  - subject: "Kcontrol 3 and KDE 3.4?"
    date: 2005-01-23
    body: "Why won't this be in 3.4? It looks really good!"
    author: "Tom"
  - subject: "Re: Kcontrol 3 and KDE 3.4?"
    date: 2005-01-23
    body: "I have no idea."
    author: "tpr"
  - subject: "Re: Kcontrol 3 and KDE 3.4?"
    date: 2005-01-23
    body: "Well then, thank you for the helpful reply :p"
    author: "Tom"
  - subject: "Re: Kcontrol 3 and KDE 3.4?"
    date: 2005-01-23
    body: "Dig through the kde-usability archives and you'll find the answer (basically: too big change for a 3.x release)"
    author: "Davide Ferrari"
  - subject: "Re: Kcontrol 3 and KDE 3.4?"
    date: 2005-01-23
    body: "> Why won't this be in 3.4? It looks really good!\n\nThat was my question that started this thread!"
    author: "LuckySandal"
  - subject: "Re: A new control center"
    date: 2005-02-01
    body: "Do you know how I can grab the CVS source through a firewall that wont normally allow ssh access? I cannot reconfigure the firewall because that's not on my machine.\n\n"
    author: "Kanwar"
  - subject: "Re: A new control center"
    date: 2005-02-02
    body: "Hmm, as far as I know anonymous cvs doesn't use ssh, so what's the problem? Here's a documentation how to use anoncvs, if you didn't know already: http://developer.kde.org/source/anoncvs.html"
    author: "tpr"
  - subject: "Re: A new control center"
    date: 2005-02-02
    body: "Thanks but I have already tried that. Thing is I cannot do this behind my office proxy/firewall. Is there a way to use cvs behind firewall/proxy? I tried cvsgrab that does it but does not work with webcvs.kde.org although www.kde.org is listed as working with it.\n\nCVSGrab: http://cvsgrab.sourceforge.net/\n\n"
    author: "Kanwar"
  - subject: "Re: A new control center"
    date: 2005-02-01
    body: "Actually setttings:/ is still a little cryptic for average users who wonder why the :/ has to be added there. The only commands with colon and other characters most users know are the smileys in chat sessions :-)\n\nBetter would be to show a list of all kio-slaves in the location bar by default, like Explorer if you wish, so that settings:/ can appear as Settings and media:/ probably can be masked as Computer etc ...\n\nAlso see my reply titled \"A user-friendly suggestion\" to the main article."
    author: "Kanwar"
  - subject: "Re: A new control center"
    date: 2005-01-31
    body: "Worst idea ever. Was one reason for me to leave Gnome when 2.0 came out.\nJust because Windows/Gnome do it like that doesn't mean KDE has to follow. \n\nYou enter one subsection - you don't see the other sections anymore.\ndistracting.\nYou open in a new folder - taskbar/desktop will be clogged with unarranged folders.\n\nPlease KDE devs stay away from this crap or at least make it an *option*, not a replacement.\n"
    author: "Dexter Filmore"
  - subject: "xorg composite"
    date: 2005-01-22
    body: "Don't forget, kwin from kde-3.4pre supports xorgs composite extention. So you get real opacity, real menu shadows and this looks really cool :-)\n\nhttp://gentoo-wiki.com/TIP_Xorg_X11_and_Transparency\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: xorg composite"
    date: 2005-01-23
    body: "Wow, that's extremely slow on my machine. AMD Athlon 1800+, Matrox G550, Suse 9.2 with x.org 6.8.2rc2. As soon as I start xcompmgr to get drop shadows, moving windows will become annoyingly slow.\n\nI thought the composite extension used hardware acceleration?"
    author: "Erik Hensema"
  - subject: "Re: xorg composite"
    date: 2005-01-23
    body: "Well, it depends if you have drivers installed that can do hardware acceleration, like nVidia's. Hardware acceleration doesn't just come out of nowhere :)."
    author: "David"
  - subject: "Re: xorg composite"
    date: 2005-01-23
    body: "Just the standaard mga driver in x.org which is AFAIK accelerated."
    author: "Erik Hensema"
  - subject: "Re: xorg composite"
    date: 2005-01-23
    body: "I'm using a G550 on Suse 9.2 as well. Using only the mga driver doesn't give acceleration. You would have to use DRI support, but this does freeze the machine when closing the X session. So you better don't try to activate it. It's sad to see the quality of mga support going down. My next card will be a NVidea or ATI."
    author: "anonymous"
  - subject: "Re: xorg composite"
    date: 2005-01-26
    body: "nvidia and ati dont support linux very good either. I'd say buy the open graphics card (google it if you don't know what it is, or check www.kerneltrap.org)."
    author: "superstoned"
  - subject: "Re: xorg composite"
    date: 2005-01-24
    body: "It is most likely not accelerated for the things that xcompmgr needs for good performance.\n\nxcompmgr does all its drawing through the render extension, so if you want good xcompmgr performance, the drivers must provide render acceleration (accelerating image composition may be enough, if memory serves; we don't do any transforms or anything yet).\n\nThe current X acceleration architecture (XAA) isn't well set up to provide such acceleration, so things are generally slow with it. Off hand, the only cards I'm sure have any acceleration are Radeons. The nv driver doesn't, and I don't know about any Matrox or Intel stuff.\n\nThe nVidia proprietary drivers use their own X server and acceleration architecture, I believe, which is why they do a much better job accelerating xcompmgr (although it's only image composition performance that's good; I tried hacking xcompmgr to do transforms on the windows and it ground my machine to a halt).\n\nIf/When Xorg switches over to Keith Packard's server/drivers, the situation will likely improve. KAA is much better set up to provide render acceleration, which can be seen by the fact that people were running xcompmgr on his experimental xserver without performance problems."
    author: "Dolio"
  - subject: "Re: xorg composite"
    date: 2005-04-27
    body: "I've got some problems with xcomposite in KDE. Firstly, when I start xcompmgr up (with any settings- this happens in every case) my desktop background disappears and turns to black. Starting this from Konsole, my original Konsole window leaves an 'imprint' on the desktop behind it and the text disappears, from Konsole. If I move the window about that original imprint of the window is left on the desktop with full opacity and I can still not see any text in Konsole. Furthermore, any menus seem to loose their background colour and become transparent and gain it again if I move my cursor over the menu. The menu regains background on the whole menu except for directly behind the text. Apart from all these (major) problems, the drop shadows seem to work fine. I'm using the latest ATI drivers and have a Radeon 9600 XT. In my console that I have started X from I see a *lot* of the following:\n\nQPixmap::resize: TODO: resize alpha data\n\nAlso, transset does not work at all.\nI'de really like to get this all working and any help would be appreciated.\nThanks!"
    author: "Michael Findlater"
  - subject: "Re: xorg composite"
    date: 2005-04-27
    body: "Why do you use xcompmgr instead of kompmgr?"
    author: "Anonymous"
  - subject: "Re: xorg composite"
    date: 2005-04-28
    body: "I don't seem to have kompmgr.. :o"
    author: "Michael Findlater"
  - subject: "Re: xorg composite"
    date: 2005-04-28
    body: "It's part of the KDE 3.4 release."
    author: "Anonymous"
  - subject: "Re: xorg composite"
    date: 2005-04-28
    body: "I have KDE 3.4, yet do not have it :S"
    author: "Michael Findlater"
  - subject: "Re: xorg composite"
    date: 2005-04-28
    body: "Actually, neg on that- it's kde 3.3.2, looks like it's time to upgrade."
    author: "Michael Findlater"
  - subject: "Looks Great"
    date: 2005-01-22
    body: "Finally a good image browser in Konqueror. I've been trying to find something like that for ages!\n\nContinuous view and table-of-contents support in a PDF reader! Exclamation point! I like! Yay!\n\nKontact looks great, as usual. But, what's with the weird alignment underneath the headers? Appointments and Anniversaries & Birthdays are centered, To-dos are one tab left of centered, Special Dates are one tab right of centered, and Inbox is left aligned.\n\nAlso, Kontact looks to be getting a little cluttered. It looks like one of those apps where they're adding everything everyone needs. I just hope I can remove the things I don't want. I've been considering using a calendar and e-mail app for a while, it's nice to have a contact's list, and I might even use the to-do list (but I wouldn't bet on it), but I definitely don't need a journal, a note keeper, RSS support, or syncronization support. Can that stuff be turned off?"
    author: "jameth"
  - subject: "Re: Looks Great"
    date: 2005-01-22
    body: ">Can that stuff be turned off?\nSettings->Select Components\nPick the ones you want."
    author: "Morty"
  - subject: "Re: Looks Great"
    date: 2005-01-22
    body: "All this stuff can be turned off, of course.\n\nSee that in the summary screenshot, there are just the default settings (no appointments, no to-do, etc.)."
    author: "suy"
  - subject: "Re: Looks Great"
    date: 2005-01-23
    body: "Can you please vote for this issue? -->\nhttp://bugs.kde.org/show_bug.cgi?id=25820"
    author: "Markus Heller"
  - subject: "Tools for bloggers"
    date: 2005-01-22
    body: "Where is this?\n\nhttp://eminor.antrix.net/gallery/file/kde_3.4_beta1/pingback.png\n\nI'm recompiling kdeaddons to see if it appears, but I don't see in konq-plugins something that seems related to this...\n\nOn a side note: I don't understand why something like kdeaddons exists. Why aren't this plugins merged with it's application? I mean, I understand that they are optional, so a packager will split them. But why the source is in another place?"
    author: "suy"
  - subject: "Re: Tools for bloggers"
    date: 2005-01-22
    body: "I've already found it. It's in kdeaddons, in the rellinks directory."
    author: "suy"
  - subject: "Re: Tools for bloggers"
    date: 2005-01-23
    body: "Rellinks is not a blogger tool. This plugin just give access to all the \"link\" tag in the top of the document throw a new toolbar.\n\nSee http://shift.freezope.org/konq_rellinks/"
    author: "Shift"
  - subject: "konqueror"
    date: 2005-01-22
    body: "I wish the icons on Konqueror get hidden by default? I wonder why anyone would want to \"cut\", \"paste\" or even increase/decrease fonts by default. I hope the Konqueror widow that is shown is not the default one. Firefox does not seem to be less functional by having fewer items on the menu bar. Konqueor as shown has the disabled icons whose purpose I do not understand. This does not help.\n\nCb.."
    author: "kb"
  - subject: "I agree"
    date: 2005-01-22
    body: "Couldn't agree more. If some icons were removed, there would be more real estate enabling the \"location\" and \"google\" bar to be moved to a position just beside the \"stop\" button."
    author: "joe"
  - subject: "Hide automatically unused (toolbar,menu..) items!"
    date: 2005-01-23
    body: "\nOh, so not only we'll not get a less  crawded toolbar in konqueror, but that we're adding one toolbar more on it showed by default hehe :-). Maybe it was added only so that people test it more than they would if it  were not showed by default or something?\n\nAnyway, I really feel that the solution to the \"KDE clutter problem\" is just to be slicker. We already have a fairly good framework for customzing in KDE, dcop can do marvelous things!. \n\nMy proposition is to just hide automatically unused items in toolbars and menus. It's not a perfect solution of course, but it's far better than actual one. You could always click in the bottom item \">>\" and show the rest. If you are curious, you can read more about it in my <a href=\"http://bugs.kde.org/show_bug.cgi?id=81637\">wish/bug report</a>. The rest of the post will only add some ideas to what I mentinoed in that report!\n\nNote that you could apply my idea in some other places where it could be handy. Where? In KDE Control Center, for example! You rate most used settings. Then you can do many handy things with that information:\n - In an hypothetic search bar of kcontrol similar to the one in Juk, Kmail, Amarok... you could search for settings, and the returned items would be showed in order by how much it had been used before.\n - You could also configure the control center to show only the 60% or the xx% of the settings in the tree by default, ordered also by how much they had been used before.\n - You could also use KDE betas and versions prior to final releases to let testers send statics about their use rates, calculate the average and then ship it as default in final releases. You could also create \"profiles\" for that matter: programmers, casual users, multimedia ones, etc, because testers are not usually normal users. Or maybe it's not needed, who knows - only statics will show us :-).\n - You could detect which handy settings are never used (i.e. becuase they could be in a place difficult to locate) so that you can fix that problems\n  \n and.. what not!!\n\nMaybe when I finish my exams I'll try to write a proof of concept so that people understand better my approach. I've used very little C++ and never for toolkits but I'm willing to learn and already develop in C, php, bash, etc.\n\nThanks for your time,\n      Edulix."
    author: "Edulix"
  - subject: "Re: Hide automatically unused (toolbar,menu..) items!"
    date: 2005-01-23
    body: "Or maybe that's the worst thing one could to.\nLet's be serious things like this have been tried before on other platforms - and it just doesn't work well.\n\nThe problem is not just that too many items are visible but that too many items are _there_.\nYour proposal would just hide them temporarily - and worse it would hide other items on everyone's desktop.\nSo in the end you'd have a VERY customized desktop on all of your workers desktops even though they don't really know about it. Sit one at another workstation and he'll be lost. Or give them a new (fresh) set up and see hw they will fare.\n\nThe main problem with that is that icons and context elements or whatever will have different locations - this is bad. There is a reason why unusded context menu items are only greyed out, but still there - it adds a huge amount of time if you have to look for the option you want all the time because it shifts location depending on what you're doing.\n\nThe other issue I have is that this might just be used as an \"excuse\" to add yet more options and items in places where they aren't really needed.\n\nLike so many people (actual developers, not just stupid users like me) have said before what is needed is more thought as to what option goes where and if it is really needed there.\nA feature such as described here would be counter productive."
    author: "Christoph Wiesen"
  - subject: "Re: Hide automatically unused (toolbar,menu..) items!"
    date: 2005-01-24
    body: "Microsoft uses this for menus, and while I'm not going to trot out any sort of usability theories as to why, it pisses me the hell off. It's always the second thing I disable, right after 'hide extensions of known file types'."
    author: "Illissius"
  - subject: "Re: konqueror"
    date: 2005-01-22
    body: "agreed..."
    author: "anon"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "The icons after the \"Download Manager\" icon (blue down arrow, opens KGet), is actually another toolbar (I don't believe on by default), its called Document Relations.\n\nMaybe the icons after 'Stop' on the main toolbar should be split into their own toolbar, which would be easier to disable or something?"
    author: "Corbin"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Konqueror's toolbars are fully adjustable. Just do it.\n\nBut you are right, those cut/copy/paste icons are useless (and those are the first ones I remove).\n\nBtw. it would be much better if KDE would tell new users to use the middle mouse button for pasting instead of showing those redundant cut/copy/paste icons on every applicatin's toolbar. Unix is not windows!"
    author: "Asdex"
  - subject: "Re: konqueror toolbar adjustability..."
    date: 2005-01-23
    body: "Not true.  There are features in there, once removed, can't be put back in a feasible manner.  Some of the special KHTML buttons or some such, they're context sensitive.  You can't shift those around.  Smarter defaults are required.  Configurability is must, just as much as capturing the gist of the application is.  There are many useless buttons considering average tasks done by illiterate users.\n\nScaling up for power users and having smart defaults for the former makes more sense."
    author: "Saem"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "really has nothing to do with the news, but anyway:\nI don't think cut/copy/paste are redundant in anyway.\nWhat your are doing when you \"copy\" a text is different from just marking the text and using the middle mouse. When using \"copy\" you can be sure that the text will be in the 'clipboard' until you copy something else.\nIn KDE there's even a history for that, but that's not the point.\nYou don't lose the text when you \"accidentially\" select some other random text. As far as I know you can't cut with the Unix-style and you can't 'overwrite' a selection with something from your clipboard.\n\nAll in all the basic unix copy function is clumsy at best, but it is handy at times - for very quick copy here, paste there actions. KDE has the easiest but yet most powerful cut/copy/paste implementation aorund - I don't see anything redundant here.\n\n(Despite the toolbar cut/copy/paste buttons ;))"
    author: "Christoph Wiesen"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "It must be nice to be young, and not need glasses. I use the increase/decrease font buttons all the time.\n\nThis hasn't been an issue in firefox, so the solution here may be a more reasonable selection of font sizes. That would alleviate the need for toolbar buttons.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Ohh...So Derek now sounds to be elderly? The Derek Kite I know is relatively young, http://derekkite.com. Is it you?"
    author: "judith"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Heh. He looks much better than I do.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Just use Ctrl+ mouse wheel to ajust fonts. It`s faster & easily (you don`t have to position mouse on the toolbar button)."
    author: "Tip"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "So many people want to remove icons for usability, but when someone comes along who uses those icons, he's told to use a keyboard shortcut or something instead.  Having to remember a bunch of key combos will really do well to attract new users lol.  \n\nWhat new user is going to have a clue that he can make the fonts bigger if he can't quite make them out?  He's going to come to the .dot and read your suggestion about ctrl and mousewheel?  "
    author: "MamiyaOtaru"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Yes, this is a problem. Today`s solution for it is Konqi`s Tips dialog. KDE 3.4 will include new user`s manual (this is the place for such things).\n\nBut I think that better is an 1 hour KDE video (or pseudovideo aka recorded actions?) guide about common ways to work with KDE (Desktops, Kicker configure, KHotKey configure, use of search in the K Control Center,KIO,.desktop files,Konqueror,Alt+F2,Toolbar editor,App menu edit, KDE menu edit,DCOP&KDCOP,...). Just to show how it can be used."
    author: "Tip"
  - subject: "Re: konqueror"
    date: 2005-01-25
    body: "I think a KDE Tour for new users would be a nice idea too. For those who are already familiar, then there should be an option to skip it."
    author: "ninaw"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Are you going to send me a mouse with weel then?"
    author: "Morty"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "Personally, I don`t know about any PC without mouse with mouse with wheel...\nI`m just think that this is not _common_ problem. You can always add buttons if you need it."
    author: "Tip"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "I've got nine pc's about the house, and only one of them has a scroll wheel. The rest are laptops"
    author: "Boudewijn"
  - subject: "Re: konqueror"
    date: 2005-01-24
    body: "Most laptops can scroll using the synaptics driver.\nhttp://qsynaptics.sourceforge.net/\nAFAIR there are also solutions for trackpoint equipped notebooks out there."
    author: "testerus"
  - subject: "Re: konqueror"
    date: 2005-10-02
    body: "i have a dell laptop. i want to make the fonts bigger and smaller. i volunteer at a seniors home, and this feature is really handy.\n\ni got the laptop to do this by accident, but for the life of me cannot replicate.\n\nplease please help.\n\ncordially, phillip"
    author: "phillip vu-tran"
  - subject: "Re: konqueror"
    date: 2005-10-03
    body: "The quickest way to change font size on demand is using Ctrl + Mousewheel Up and Ctrl + Mousewheel Down.  \n\nThis works in Konqueror, KMail, KGhostview, KPDF and firefox.  Those are the ones I know of, there may be others. \n\n"
    author: "cm"
  - subject: "Re: konqueror"
    date: 2005-10-03
    body: "Oops, sorry, I was missing the context of your message.  Without a mouse you can still use the keyboard shortcuts (Ctrl-\"+\" and Ctrl-\"-\"). \n\nBTW:  The first thing I do when I set up my laptop is plugging in my USB mouse.  It's much more comfortable for me that way. \n\n"
    author: "cm"
  - subject: "Re: konqueror"
    date: 2005-10-03
    body: "thanks a mil\n\nthe only way i can get my dell latitude 110L to do this, is to go into internet explorer, click on to 'view' and change it from there\n\ni was hoping for the 'control' +  or -\n\nbut these functions wont work on xphome/i. explorer for me\n\nanyhoo, thank you again for posting a reply.\n\ncordially, phillip"
    author: "phillip vu-tran"
  - subject: "Re: konqueror"
    date: 2005-10-04
    body: "Oh!  Your question was about Windows and Internet Explorer...\n\nI'm afraid you've come to the wrong place then.  This newsboard and this discussion is about KDE, a graphical desktop for Linux and UNIX operating systems. \n\n"
    author: "cm"
  - subject: "Re: konqueror"
    date: 2005-01-25
    body: "Eight laptops!?  Wow.\n\nThe first thing I did when I got mine last year was to plug in an external wheel mouse. :-)\n\n\n\n"
    author: "cm"
  - subject: "Re: konqueror"
    date: 2005-01-25
    body: "Three of them are pretty old clunkers I bought second-hand for my daughters :-). One is a Mac. One is my wife's, one is mine. And I do use a scrollwheel mouse when sitting at a desk, but not when I'm on the train. In any case, depending on something like a scroll wheel for important ui functions is a bad idea. And I think that KDE already uses the scroll wheel to much. You can hardly idly scroll the wheel without moving another desktop, another application or something else into or out of focus :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: konqueror"
    date: 2005-01-25
    body: "I think the zoom in/out should be got rid of too. I normally get very annoyed with website text being too small, so I set my konqueror HTML fonts to 11 for the minimum and medium sizes in the settings. Now I never have to zoom in because they are guaranteed to never be too small. You should try this too. It means you never have to click the toolbar icons or hit the shortcut keys again for zooming in, and if it's a common problem it'll make your surfing that little bit easier."
    author: "Jonathan"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "I'd agree that the cut/copy/paste and print buttons aren't needed on the toolbar by default. These aren'T usually expected to be there by users and the first three are hardly useable because almost everybody does this via the RMB context menu, the keyboard shortcuts or even the edit menu.\n\nMaybe \"print\" needs discussion, but I think most people expect and use it under \"Location\". Though I realize that some folks will be used to having it on the toolbar in KDE by now. Yet in File-Management Mode I can't relly see a reason for it. You can't print a directories content with it so it's use is zero unless you use some kind of embedded viewer.\n\nThe in-/decrease symbols icons are needed in file management view imo, because they are necessary to switch between different view modes - to remove them the four icons (increase icons, decrease icons, list-views, tree-views) would need some kind of a merge, so all the functions are more easily accessible with only one or two buttons. Otherwise i'd say those buttons are needed for file-management - yet they could be removed from the web browser part.\n\nBut honestly, if the following icons are removed the whole Konqueror looks a lot cleaner already and shouldn't remove too much functionality or resort people to using keyboard shortcuts:\n- cut\n- copy\n- paste\n- print\n\nI suppose we're not aiming at having the adress / search bar to the left of the main toolbar like e.g. firefox here, because I think Konqueror is more than that and needs more than just the basic navigational buttons by default."
    author: "Christoph Wiesen"
  - subject: "Re: konqueror"
    date: 2005-01-23
    body: "I raised a feature request to change the increase/decrease font size buttons to one font size button that pops up a slider. Takes one button off the toolbar at least and without losing any functionality either.\n\nhttp://bugs.kde.org/show_bug.cgi?id=96663"
    author: "Dan"
  - subject: "CLUTTER AND USELESS TOOLBARS"
    date: 2005-01-24
    body: "I agree. If there is one thing I hate about KDE besides inconsistent UIs, it is the clutter of pointless options (KDE 3.4 will have a feature to enable or disable shading of active row in listview, that is ridculous)and toolbars with everything under the sun on them.\n\nWe really need something like Gconf for options taht only 0.1% of userbase cares about and taht just confuse the rest."
    author: "Alex"
  - subject: "Re: CLUTTER AND USELESS TOOLBARS"
    date: 2005-01-24
    body: "I disagree with the Gconf thing but that probably does not matter since there is so much more obvious stuff that can be tackled first.\n\nWhy does Kaboodle have a toolbar button for hiding the menu? Is Kaboodle in some sense unique so it needs it and other apps do not? Surely those sorts of obscure settings that advanced users may want (and I'm not understanding why anybody would want this feature) should not be on the toolbar? \n\nWhen we have cleaned up obvious UI design errors that KDE is so full of then we can debate other solutions but for now I see no point."
    author: "Dan"
  - subject: "KDE is a winner"
    date: 2005-01-23
    body: "KDE is a winner! You know why? Because of integration (e.g. the RSS integration, network transparancy) and code reuse (e.g. koffice libs).\nI like Gnome as well, but integration is really, really poor at some points (e.g. vfs) compared to KDE.\nOn the other hand the Gnome UI is very polished thanks to the HIG.\nI always think of migrating to Gnome (just to try it out for a while), but always end up missing\nimportant features that only KDE has.\nMore than three cheers for KDE!\n\nP.S.: does anyone know where to get the journal plugin for kontact?\nThanks a lot!"
    author: "dwt"
  - subject: "Re: KDE is a winner"
    date: 2005-01-23
    body: ">P.S.: does anyone know where to get the journal plugin for kontact?\n\nFrom a KDE 3.4 release near you :)\n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: KDE is a winner"
    date: 2005-01-23
    body: "I agree.  I also consider switching to Gnome each time I see one of those elegant and simple UI's.  I must add that I've always sticked to KDE because of the programs, features and integration...\nIt would be great if KDE could addopt some UI-knowledge of Gnome.\n\nKDE 3.4 looks great."
    author: "Rutger Claes"
  - subject: "New features crowd out bug fixes?"
    date: 2005-01-23
    body: "At some point, KDE will have enough users that the demand for bug fix releases will be strong enough that bugs will be fixed in previous versions and a bug fix releases will be made on a regular basis. Maybe this has happened in the past.\n\nSee \"Is there any chance of KDE 3.3.3?\"\nhttp://lists.kde.org/?l=kde-linux&m=110591224012969&w=2\n\nMy interest in this is that I have a bug in Kmail which prevents me from properly using IMAP, and I rely on IMAP for my most important email. Many IMAP related bugs have been identified in KDE 3.3.X and fixed in the KDE 3.3 branch of CVS. Some of these bug fixes did not make it into KDE 3.3.2 and are still sitting in CVS. For example, \"Bug 88511: message preview panel sometimes does not show message\" http://bugs.kde.org/show_bug.cgi?id=88511\n\nI tried to incorporate the CVS for KDE PIM into my KDE 3.3.2 Level \"a\" setup on SUSE 9.1 Professional AMD64, but compilation failed:\nSee \"How to integrate bug fix from source into RPM-based\"\nhttp://lists.kde.org/?t=110466934400002&r=1&w=2\n\nSo I am currently left with waiting for KDE 3.4, hoping that all the new bugs introduced with all the new features are not worse than any bugs that may have been fixed. I only have one PC at home which is a production machine - I use it for all my PhD work - and right now I don't want to make it unstable by making it rely on beta software, KDE or otherwise.\n\nFor comparison, note that Linux kernel 2.4.28 was released on 2004-11-17 and 2.4.29 was released on 2005-01-19. Even though the current kernel is 2.6.10, bug fix releases for 2.4 still come out on a regular basis. See http://www.kernel.org/pub/linux/kernel/\n\n\n"
    author: "Paul Leopardi"
  - subject: "Re: New features crowd out bug fixes?"
    date: 2005-01-23
    body: "Have you talked to the kde packager of your distro? It's their job to provide updated binaries when security issues or important bugs are fixed in cvs."
    author: "Anonymous"
  - subject: "Re: New features crowd out bug fixes?"
    date: 2005-01-23
    body: "The O.P wants \"older\" bug fixes to be included in current source code.  That isn't the distributors' job, that's a development issue."
    author: "anon"
  - subject: "Re: New features crowd out bug fixes?"
    date: 2005-01-23
    body: "Can't you build kdepim yourself from the latest cvs of KDE_3_3_BRANCH? It is pretty simple. You would need all *devel-RPM SuSE provides you with, cvs and perhaps some other RPM like gcc, autotools. It might even be that you already have all those tools on you machine."
    author: "Carsten"
  - subject: "Konqueror sidebar"
    date: 2005-01-23
    body: "I see that the konq sidebar widget redesign from http://www.kde-look.org/content/show.php?content=17049 isn't present in these shots. Will this get in to 3.4 final?\n\nI've always thought it looks better and is far more intuitive than the present buttons down the side."
    author: "Robert"
  - subject: "Re: Konqueror sidebar"
    date: 2005-01-23
    body: "> Will this get in to 3.4 final?\n\nHow could mockups ever enter source code?"
    author: "Anonymous"
  - subject: "Re: Konqueror sidebar"
    date: 2005-01-23
    body: "http://www.kde-look.org/content/show.php?content=16962"
    author: "Anonymous"
  - subject: "Re: Konqueror sidebar"
    date: 2005-01-23
    body: "I posted the wrong link.\n\nThere is working code is there, as 'anonymous' above points out."
    author: "Robert"
  - subject: "Re: Konqueror sidebar"
    date: 2005-01-23
    body: "IIRC, Andre got an account a fair bit ago and those patches were contributed and accepted.  If I'm not mistaken, he's continuing that work and possibly other stuff within KDE."
    author: "Saem"
  - subject: "Re: Konqueror sidebar"
    date: 2005-01-23
    body: "I sure hope that sidebar redesign never happens.\n\nThe problem with using the toolbox widget for the Konqueror sidebar tabs is that it doesn't scale. In the screenshots of the link you posted there are only four tabs. (See http://www.kde-look.org/content/pre1/16962-1.png) This is probably the pain threshold for the widget. In my current setup, I have eight sidebar buttons. Try imagining eight tabs in this screenshot. A lot of space is wasted."
    author: "teatime"
  - subject: "Re: Konqueror sidebar"
    date: 2005-01-23
    body: "Yes, you have a point.\n\nBut I've always found the existing buttons down the side to be quite confusing and ambiguous. Which drawer is open? Is one of them open under the others? How do I make this one disappear? Of course, I do actually know, but my first impressions were along these lines.\n\nMaybe someone has a better idea."
    author: "Robert"
  - subject: "NO PLEASE NO"
    date: 2005-01-24
    body: "The sidebar he proposes is terrible for usability and real estate.\n\nThere isa lso no compact mode, one in which that you just press icons and it appears and man is it a big hack too. There are just so many problems with it.\n\nLinspire even had this as default in pre release versions for Linspire 5 and after Insiders complained, the patch was quickly removed.\n\nDon't make the same mistake."
    author: "Alex"
  - subject: "More sceenshots"
    date: 2005-01-23
    body: "I posted here screenshots showing this: \nknewtuff that allows you to get new wallpaper from kde-look, kopete and its search function in buddy list and you the ability to easily switch user or start a new session while running another (althought that maybe not new but I'm not sure :))\n\nhttp://www.kde-look.org/content/show.php?content=19923"
    author: "Pat"
  - subject: " KDE 3.4 Beta 1 Screenshots"
    date: 2005-01-23
    body: "Nice screenshots...i also tried to installed and installed\nfor 30 minutes new beta KDE. It works fine but i lost many\nicons from the panel: nemo, klipper, suse hardware tool,\nsuse watcher and amarok. Amarok doesn't works (1.2 beta 3).\nBut at all looks very nice and on mine computer works a little faster.\n\nMitja\n"
    author: "Mitja"
  - subject: "Yast2 modules??"
    date: 2005-01-23
    body: "In the settings screenshot appears a yast2 module.\nIs it because you are running a suse based distro, or does yast2 been added to kde cvs? That would have some implications to the kde hardware support settings, which is not a bad thing per si, but i didn`t find anything in the kde cvs digests talking about yast2 integration.\n\nCan you elaborate on that?\n\nJust curiosity.\n\nBest regards"
    author: "Paulo Dias"
  - subject: "Re: Yast2 modules??"
    date: 2005-01-23
    body: "> does yast2 been added to kde cvs?\n\nNo."
    author: "Anonymous"
  - subject: "Re: Yast2 modules??"
    date: 2005-01-23
    body: "Errrrrrrrrr.... that just means that he has Yast2 installed. Yast2 puts that section in KControl to integrate the Yast2 configuration modules into the KDE Control Center. Its all about integration, man."
    author: "Aaron Krill"
  - subject: "KPDF Icons order"
    date: 2005-01-23
    body: "http://eminor.antrix.net/gallery/file/kde_3.4_beta1/kpdf.png\n\nthe icon order makes no sense to me. why is there first the +, then the -\nand what does the third without desription mean?\n\nI think that both, a selection, and this +/- are not perhaps for zooming\n\nI would prefer a horizontal ruler for zoom.\n\nAnd despite that I do not understand the Icon oder \n\nleft + \nright -\n\nso it is agaisnt the common direction where left is small and right is high numbers."
    author: "aile"
  - subject: "Re: KPDF Icons order"
    date: 2005-01-23
    body: "The third one is \"search\", I agree that using almost the same icon for that as \"zoom\" is a mistake from the icon designers.\n\nAs far as + and - go, my first approach for the order of these buttons was \"- +\" but that felt very unnatural so I reverted it to the current order (they are now like this throughout KDE). I think it has to do with the fact that you tend to use \"zoom in\" much more than \"zoom out\" and that \"zoom out\" is basically undoing the \"zoom in\". As such it makes sense to have \"zoom in\" first.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: KPDF Icons order"
    date: 2005-01-24
    body: "There is a bug for kghostview http://bugs.kde.org/show_bug.cgi?id=74284\nI agree that + -  seems to be wrong for left-to-right languages, but that might be just what I am presonally more used to.\nBetter someone would have to do a real usability study on this."
    author: "testerus"
  - subject: "Clean up the defaults!"
    date: 2005-01-23
    body: "I think KDE is fantastic, but the first thing I do with a new installation is spend 30 minutes changing the defaults so it looks cleaner. I've attached a screenshot of my desktop which has a konqueror window with the screenshot of the current 3.4 default desktop inside it.\n\nHiding the task bar applet handles is a must; having 5 dotted bars with an arrow at the top looks cluttered and horrible. There's not need for the panel hiding button on the right either because most people aren't going to use it. 4 desktops is too many in my option and should either be 2 or 3 so as not to take up so much space. The task bar icons should be set to small in my option as they just waste space that could be used for task buttons. I think the digital clock is quite ugly too and should just use a plain clock. As for konqueror, there should only be a minimum number of toolbar icons on by default like in my screenshot which makes it look similar to firefox.\n\nI like KDE, I just don't like seeing people being put off because it looks so cluttered. I know they can change it themselves, but I'd rather the default was simple so you could make it cluttered if you wanted instead of the other way around."
    author: "Jonathan"
  - subject: "Re: Clean up the defaults!"
    date: 2005-01-23
    body: "You can disable a lot settings with Kiosk.\n\nPerhaps some settings like panel hiding and system applet handle could be disabled by default. If someone wants to change everything, he/she would then need to run kiosktoo and enable stuff there.\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Re: Clean up the defaults!"
    date: 2005-01-24
    body: "Yes, the defaults tend to be very bad. Way too many things are default. And OMG, don't get me started on the toolbars!"
    author: "Alex"
  - subject: "Re: Clean up the defaults!"
    date: 2005-01-25
    body: "Your taskbar looks ugly. There is no separation between eg. your pager and your taskbar. I much prefer having the applet handles set to 'fade', so there's a gap between objects in the kicker but you can't see the handles till you mouseover. There, see how useless that comment was? :-)"
    author: "foo"
  - subject: "Re: Clean up the defaults!"
    date: 2005-01-26
    body: "I think there should be visible applet handles, because most people don't know they can alter the taskbar. its easier to see that with the applet handles...\n\nand if you know about the applets, you probably also know how to turn them off... I don't want KDE to be tuned for ME or users like me, but for newbies. but, if possible, in such a way they can access the power of KDE easy. so they can become power users :D\n\nthat's what I dislike about Gnome, it limits its users' development.\n\nbut I like your konqi setup, although I'd say make the buttons bigger. and increase the size of the kicker. maybe create two kicker panels, like gnome does? its very clean, and most newbies like it."
    author: "superstoned"
  - subject: "Trash icon on desktop"
    date: 2005-01-23
    body: "Not extremely important but why a trash icon on the desktop?\nShouldn't this be under \"My documents\" or be an icon in the system tray?\nI personally belief that there shouldn't be anything on the desktop except a karamba theme.\nBy proposal would be the move the trash icon to the my documents map.\n\nWho decides such kind of things?"
    author: "Jeroen"
  - subject: "Re: Trash icon on desktop"
    date: 2005-01-23
    body: "I totally agree."
    author: "gaspojo"
  - subject: "Re: Trash icon on desktop"
    date: 2005-01-23
    body: "Mostly, the people who code these things decide these things... \n\nBut in this particular case, in the beta I'm running now, I can put the trash icon as an applet in Kicker. And for as long as I can remember, you can move the trash icon to wherever you want. Until I started to use a Mac regularly I didn't have any use for the trash icon, so it resided in .kde for me; there's a nice dialog in the desktop settings where you can move the trash to whereever you want."
    author: "Boudewijn Rempt"
  - subject: "Re: Trash icon on desktop"
    date: 2005-01-24
    body: "But we're talking aboit sane defaults. Probably, now that is possible, the best place where to put it is in the kicker, but to be useful the kicker should be always at medium siza"
    author: "Davide Ferrari"
  - subject: "Re: Trash icon on desktop"
    date: 2005-01-24
    body: "I know you can hide the trash icon or move, that's not the point. I meant just the default settings. Personally I belief the trash icon should be in the My Document map.\n\nGreat that you can move the icon to the kicker but the kicker or the system tray gets easily to crowded."
    author: "Jeroen"
  - subject: "Re: Trash icon on desktop"
    date: 2005-01-24
    body: "What 'My Document map' are you talking about?"
    author: "ac"
  - subject: "Re: Trash icon on desktop"
    date: 2005-01-27
    body: "I guess they mean the default folders in their home drive, ie ~/Documents ~/Pictures ~/Trash ~/Music etc etc"
    author: "another mouse"
  - subject: "Oh, I didn't checked KCalc myself before..."
    date: 2005-01-23
    body: "But I hope the Constants menu will be placed to the left of Help...\n"
    author: "Shulai"
  - subject: "Yeah"
    date: 2005-01-24
    body: "Just like in every other KDE program."
    author: "Alex"
  - subject: "Color Scheme"
    date: 2005-01-24
    body: "As of 13-01-2005 CVS, KDE default color scheme is not the plastik one, but the old keramik, which looks quite ugly with the new plastik default deco. Has it been fixed?"
    author: "Anonymous"
  - subject: "Kate and KWrite"
    date: 2005-01-24
    body: "Now, when we will stop pretending that Kate and KWrite are two separate programs? Is that a task for KDE 4?"
    author: "LuckySandal"
  - subject: "Re: Kate and KWrite"
    date: 2005-01-24
    body: "They ARE two different programs."
    author: "teatime"
  - subject: "Re: Kate and KWrite"
    date: 2005-01-24
    body: "Grrrr!!!! Well, call me ignorant, but it appears that most of their code is shared. At any rate, the only real advantage that Kate has over KWrite is that that it can edit multiple files in the same window. I think that should be a setting rather than a separate application. Sometimes I open up something in KWrite only to find that I need to open another file but can't do it in the same window!!!"
    author: "LuckySandal"
  - subject: "Re: Kate and KWrite"
    date: 2005-01-26
    body: "KDE has a text editor part, and two wrappers around it. The text editor part does all of the editing (of course). KWrite is a wrapper around the part for simple editing (like notepad). Kate is a wrapper around it that adds features to make it useful as a programmers' text editor, like having a built-in console and so on.\n\nThat is the distinction. There's no need for a simple text editor to be able to dock other components all over the place, so KWrite doesn't have it, but that's useful in Kate's application domain. It's like notepad versus textpad."
    author: "Dolio"
  - subject: "Re: Kate and KWrite"
    date: 2005-01-24
    body: "KWrite is not an entirely separate program anyway. It is more a sort of a skin (simple editor) for Kate.\n\nAnd I suppose the editor to remove is KEdit, as soon as Kate can do Bidi.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "What about..."
    date: 2005-01-24
    body: "What about an \"ioslaves:/\" protocol?\n\nIts output? All available ioslaves like audiocd, fish, smb, sftp etc.\n"
    author: "gr8teful"
  - subject: "[OT] Security enhancement for KDE 3.4 or 4.0"
    date: 2005-01-24
    body: "I was reading some article on the web and found this article:\n\nhttp://www.hpl.hp.com/techreports/2004/HPL-2004-221.html\nor\nhttp://www.google.com/search?q=polaris+xp\n\nBasically, it describe a mechanism to restrict privileges and sandbox\nany application by providing \"null priviledge\" \nPLUS whatever GUI dialog box needs to run for any given application.\n\nMaybe we could do something similar in KDE.\n\nSince currently, most GUI application runs with \"user priviledge\",\nwhich means any app can see any files in your $HOME directory \nor mess with it.\n\nI know that currently no one is really exploiting this,\nbut it might be an issue at some point in the future.\n\nI know most people have backups, but it doesn't protect against\nsending your backups to some evil adware company. =P\n\nSincerely,\nFred.\n"
    author: "fprog26"
  - subject: "Re: [OT] Security enhancement for KDE 3.4 or 4.0"
    date: 2005-01-24
    body: "One might be able to set something like this up with the -u option to kdesu, thus running an untrusted app under the \"nobody\" account. Or a special \"sandbox\" user created just for this purpose.\n\nGoing further, one could imagine a graphical frontend to setting up a chrooted environment in which suspect apps could run."
    author: "Dr Stupid"
  - subject: "Re: [OT] Security enhancement for KDE 3.4 or 4.0"
    date: 2005-01-25
    body: "In the context menu of an application icon (on your desktop, K-menu,...) you can select a user (e.g. nobody) to run the program with.\n\nThat's all you need. (This has been available since KDE 3.0 (or KDE 3.1))\n\nUnfortunately it's hidden very well."
    author: "Asdex"
  - subject: "Digital Clock !!! We are in 2005 not in 1970!"
    date: 2005-01-24
    body: "I would ask to people how many of you hate the fact of that KDE brings Digital Clock as the default option. Please, be consistent with the look provided by your Look&Feel artists and change it to the clean (anti-aliasing fonts) clock !!!!\n\nThanks. ;))"
    author: "A simple observator"
  - subject: "Re: Digital Clock !!! We are in 2005 not in 1970!"
    date: 2005-01-24
    body: "Yes, it looks horrible in a clean style as plastik."
    author: "Anonymous"
  - subject: "Re: Digital Clock !!! We are in 2005 not in 1970!"
    date: 2005-01-24
    body: "I also wish that i could display the date in _smaller_ print underneath the time..."
    author: "LuckySandal"
  - subject: "Re: Digital Clock !!! We are in 2005 not in 1970!"
    date: 2005-01-25
    body: "In Configure clock in the Display part set the Date checkbox and change the font and fontsize in the Date box(Alt-h if you like). Wish granted....."
    author: "Morty"
  - subject: "Re: Digital Clock !!! We are in 2005 not in 1970!"
    date: 2005-01-25
    body: "It looks quite nice in my pseudo-MacOS config, but I agree that it's ugly with the defaults."
    author: "Illissius"
  - subject: "Re: Digital Clock !!! We are in 2005 not in 1970!"
    date: 2005-01-25
    body: "I fully agree and one of these should really be merged into cvs:\n\nhttp://www.kde-apps.org/content/show.php?content=18543\nhttp://www.kde-apps.org/content/show.php?content=14423\n\nThe old one sucks big time, it looks simply awful.\nKDE 3.4 should have a good looking analog clock, pleaaaaaase!!!"
    author: "ac"
  - subject: "Something like Synaptic"
    date: 2005-01-26
    body: "I was wondering if there is around some KDE tool similar to Synaptic (http://www.nongnu.org/synaptic/). In negative case, would be interesting a port or something like that because is a very cool tool."
    author: "Something like Synaptic?"
  - subject: "Re: Something like Synaptic"
    date: 2005-01-26
    body: "http://webcvs.kde.org/kdenonbeta/ksynaptics/\n\nhttp://kde-apps.org/content/show.php?content=17286"
    author: "Christian Loose"
  - subject: "Re: Something like Synaptic"
    date: 2005-01-26
    body: "Oops...embarrasing. Should have looked closer.\n\n*hides somewhere far away*"
    author: "Christian Loose"
  - subject: "Re: Something like Synaptic"
    date: 2005-01-26
    body: "<paste>\nDescription:\nKSynaptics is a control center module that enables users to take full advantage of their mobiles' synaptics touch pad.\nIt depends on the XFree's synaptics driver and offers the following features:\n\n*adjustable pressure sensitivity\n*tapping configuration / smart tapping\n*mouse button emulation\n*circular scrolling\n\nCheck it out, if you prefer Qt, there's also an Qt-based version available.\n</paste>\n\nThat has *nothing* to do with what he was asking. He was asking about the apt-get front-end that is installed by default w/Ubuntu, for example. \n\nGood try, but I think kpackage is more like synaptic in this case. However I do prefer synaptic to kpackage. "
    author: "Jeremy"
  - subject: "Re: Something like Synaptic"
    date: 2005-01-26
    body: "Oh!! You _DID_ realize. I thought the embarassment comment was from the original poster. Sorry man, didn't mean to rag on ya!\n\n"
    author: "Jeremy"
  - subject: "Re: Something like Synaptic"
    date: 2005-01-27
    body: "Kapture (http://webcvs.kde.org/kdenonbeta/kdedebian/kapture/README?view=markup)\nis also an APT frontend."
    author: "anonymous"
  - subject: "Re: Something like Synaptic"
    date: 2007-02-16
    body: "Yes, there is.  The Adept package manager is similar to Synaptic, but is written for KDE using qt, rather than gtk.  The home page is at http://web.mornfall.net/adept.html.  \nA screenshot can be found in the Wikipedia article at http://en.wikipedia.org/wiki/Adept_Package_Manager"
    author: "Mark"
  - subject: "Too much looks the same as always."
    date: 2005-01-28
    body: "Why is it so hard to create a usable free desktop for unixlike environments.\nThey always seam to focus on the needs of the sysadmin, and not the users. In real  working life, sysadmins are fewer than ordinary users. So why not make interfaces that take this into account. We need to start to speak their language, and map models artefacs they feel familliar onto the unix stuff we use as sysadmins\n\nE.g Most users will never need to navigate the entire file system tree. They need some place for their own files, some place to rech the files of other people. And here the key word is people. Users should not need to know that John have his files in /home/john. or /home2/john or some other unix related way. They should just be able to get the files John is prepared to share with them in one sinle place, e.g. the folder \"Other Users Files/John\" regardless\nof where they are stored in the filesystem. In the same way the user needs to find their own documents and the applications, and add on hardware in a uniform way.\n\nApple is doing a wonderful job at this in Mac OS-X. Unfortunately they have forgotten the sysadmin, that still needs to see the unix system underneth the useroriented surface.\n\nMost people here would probably call this a dumbed down interface, but guess what, most people are not that smart, and if they are, they probably have better things to do than learning how to adapt to the computer sciens guy or the sysadmin way of thinking."
    author: "Uno Engborg"
  - subject: "Now if only ..."
    date: 2005-01-30
    body: "... gmail would work in Konqueror, I would reduce the bloat on my desktop further by getting rid of Firefox. I know, I know Firefox is the hottest thing around but face it, despite its sleeker code, its very huge on memory. I am sticking to it *only* because I cannot access my gmail account in Konqueror. Otherwise, there is nothing to stop konqueror now!\n"
    author: "Kanwar"
  - subject: "Re: Now if only ..."
    date: 2005-01-31
    body: "Since KDE 3.3.2, Gmail works in Konqueror.\nTake a look at http://osdir.com/Article2722.phtml for visual proof ;)"
    author: "antrix angler"
  - subject: "Re: Now if only ..."
    date: 2005-01-31
    body: "Thanks for the link. However, I have tried 3.3.2 as well as the latest source from CVS and I get an error that konqueror could not load the page. After pressing 'Ok' on the error dialog, KDE crash handler comes up.\n\nMaybe i'm missing something but it would be great if a kde developer would post details of how to get it working.\n\nIn the meanwhile, here's a screenshot of the error dialog (from KDE 3.3.91)"
    author: "Kanwar"
  - subject: "Re: Now if only ..."
    date: 2005-02-01
    body: "I get the same error frequently on gmail, but not always..."
    author: "ac"
  - subject: "Re: Now if only ..."
    date: 2005-02-01
    body: "Ummm dunno, I get the error each time. Oh and an update, I downloaded Mandrake 10.2 beta and the KDE 3.3.2 version in it works perfectly with gmail (Konqueror, I mean). Now how come a bug that's probably fixed in 3.3.2 crops up in 3.3.91 again? Or am I missing something?\n\nThanks for all replies."
    author: "Kanwar"
  - subject: "A user-friendly suggestion"
    date: 2005-02-01
    body: "Can we have joe-blow friendly names for the kio slaves? So that smb:/ or lan:/ could be called, say, \"Network\" or something. My example reeks of Windows but its just because that the most obvious one that comes to mind.\n\nThe idea is that in place of entering smb:/ in the Location bar, it should show the Mnemonic there. I know there's Konqueror profiles but I wonder if the \"technical\" names of kio-slaves could be hidden by default?\n\nIt would go far in making KDE more average-guy/gal friendly than it already is.\n\nSorry if I haven't spelled it out accurately. I hope the smart developers get the idea!\n\n"
    author: "Kanwar"
---
KDE 3.4 Beta 1, christened Krokodile, <a href="http://dot.kde.org/1105642626/">was released</a> not too long ago. For those of you who have not yet taken the plunge, <a href="http://eminor.antrix.net/">Eudpytula Minor</a> <a href="http://eminor.antrix.net/2005/01/19/sneak-peek-kde-34/">has announced</a> some <a href="http://eminor.antrix.net/gallery/folder/kde_3.4_beta1">Krokodile screenshots</a> for your viewing pleasure.




<!--break-->
