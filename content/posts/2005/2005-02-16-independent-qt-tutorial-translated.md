---
title: "The Independent Qt Tutorial Translated"
date:    2005-02-16
authors:
  - "jthelin"
slug:    independent-qt-tutorial-translated
comments:
  - subject: "Good work!"
    date: 2005-02-16
    body: "This really shows off how easy it is to get started. I think many still overrate the difficulty of programming.\n\nWhen I started C++/Qt/KDE programming 2 years ago the few tutorials where immensly helpfull. I also see there is a link to the free \"Thinking in C++\" on that site, good! Maybe some links to the various KDE tutorials may also find their way; Qt programming is powerfull, KDE programming is even more pwoerfull (i think: KHTML, KTextEditor, KConfigXT, XMLGUI, KURL, KIO_slaves, etc, etc, etc.).\n\nHappy coding!\nCies."
    author: "Cies Breijs"
  - subject: "Re: Good work!"
    date: 2005-02-16
    body: "You are right. Seeing the size/complexity ;) of the KDE infrastructure a complete tutorial is much needed. I'd love to have a book/manual :("
    author: "veton"
  - subject: "Re: Good work!"
    date: 2005-02-16
    body: "I agree a book on KDE Programming would be really useful.\n\nBut you don't have to learn C++ to program KDE, although it's useful to be able to understand enough C++ to follow the Qt and KDE docs. There are some QtRuby tutorials on the KDE Developer's corner site, along with this KDE Korundum ruby one:\n\nhttp://developer.kde.org/language-bindings/ruby/kde3tutorial/\n\nAnd you can program in KDE apps in python like this with PyKDE - they're really easy to write too.."
    author: "Richard Dale"
  - subject: "Re: Good work!"
    date: 2005-02-17
    body: "I've always prefered the 'dive in head-first' model of learning a new API, rather then reading a book. With new programming languages a book is certainly nice (unless they're really easy like PHP), but API's are different. Just write a program using some basic examples and the API documentation; it'll end up making some sense. I first used Korundum as Richard Dale suggests. Now I'm doing some work on amaroK. \n\nProbably going back and reading though a tutorial like this wouldn't be a bad idea however..."
    author: "Ian Monroe"
  - subject: "Any good code to learn from?"
    date: 2005-02-16
    body: "Can anyone suggest a good text-book example of a KDE app that would be easy to look at? I guess compact and *clean* is what I am looking for.\n\nI've not really used C/C++ in anger and neither have I programmed a GUI so would be interested in the patterns used and also the coding style.\n\nThanks.\n"
    author: "mrtom"
---
<a href="http://www.digitalfanatics.org/projects/qt_tutorial/">The Independent Qt Tutorial</a> provides an example-driven approach to learning Qt. It tries to bridge the gap between the official Qt documentation and the average programmer. Recently, translation work has made impressive progress and the tutorial is now available in <a href="http://www.digitalfanatics.org/projects/qt_tutorial/fr/index.html">French</a> and <a href="http://www.digitalfanatics.org/projects/qt_tutorial/it/index.html">Italian</a>.



<!--break-->
