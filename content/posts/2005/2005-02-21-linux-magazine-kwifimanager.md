---
title: "Linux Magazine: KWifiManager"
date:    2005-02-21
authors:
  - "binner"
slug:    linux-magazine-kwifimanager
comments:
  - subject: "Nice"
    date: 2005-02-21
    body: "Nice job Chris, very well written!"
    author: "GldnBlls"
  - subject: "Only complaint"
    date: 2005-02-21
    body: "I really like kwifimanager, but the only thing that keeps me from using it is the fact that it insists on creating a log file in my home directory.  It would be really nice if I could turn that off or change the location of the log file or something.\n\nI'm just one of those people who doesn't like to clutter up their home directories."
    author: "Fizz"
  - subject: "Re: Only complaint"
    date: 2005-02-22
    body: ">I'm just one of those people who doesn't like to clutter up their home directories.\n\nWho isn't? :-)\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Only complaint"
    date: 2005-02-22
    body: "Jesus, you're one of those people who don't get any work done and worry about that crap all day, aren't you?\n\nFind a hobby."
    author: "Joe"
  - subject: "Re: Only complaint"
    date: 2005-02-23
    body: "I get plenty of work done (most days), it just bothers me when I see things putting stuff in my home directory when it SHOULD be somewhere else.  and it bothers me more when I can't tell it to put it somewhere else.  That is just poor design."
    author: "Fizz"
  - subject: "Re: Only complaint"
    date: 2005-03-02
    body: "Hi,\n\nplease open a wishlist bug report on bugs.kde.org. I will look into it later...\n\nStefan Winter"
    author: "Stefan Winter"
---
The <a href="http://www.linux-magazine.com/issue/52">March 2005 Issue</a> of <a href="http://www.linux-magazine.com/">Linux Magazine</a> has a story written by our own <a href="http://people.kde.org/chrish.html">Chris Howells</a> about KWifiManager (<a href="http://www.linux-magazine.com/issue/52/KWiFiManager.pdf">PDF format</a>). It introduces KWifiManager, tells you how to find and connect to wireless networks and how to use it for monitoring your wireless connection.



<!--break-->
