---
title: "KDE Kiosk: Battening Down the Hatches"
date:    2005-04-27
authors:
  - "bo'donovan"
slug:    kde-kiosk-battening-down-hatches
comments:
  - subject: "Internet "
    date: 2005-04-27
    body: "Is there other KDE software which supports the needs of internet caf\u00e9s?"
    author: "Andr\u00e9"
  - subject: "Re: Internet "
    date: 2005-04-27
    body: "yes there is kaffetower which is in early stage and openkiosk which is cross platform and really cool.\n\nhttp://www.kde-apps.org/content/show.php?content=23194\n\nhttp://www.kde-apps.org/content/show.php?content=17259"
    author: "Pat"
  - subject: "The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "It comes from what is told to be an advantage of the unix-like systems: their decouplement of the GUI from the \"system\".\n\nI see Kiosk-like to be nothing more than the so cold \"policies\" from the old Windows 95 days.  They are useful to help the user, but are almost nothing to *enforce* a policy over said user.\n\nOnce you have locked down the user's GUI, how long have you gone about *really* locking its environment?  You know that once he reaches the command line kiosk policies amount almost to nothing (thus the comment about trying to avoid him to reach it as a means to \"secure\" his environment).\n\nIf you have managed Windows 95 policies, you should remember they tried to achieve just the same... with no success; they migth avoid you calling the command line interpreter directly but, since Win95 has no proper process-level security it was not too dificult to \"scape\" from an application to the underlying shell and then, patch the registry with a provided reg file, for instance.\n\nSo, unless your environment is really a kiosk-like environment (a touch screen on a public environment, for instance, with no keyboard and just a single app running), how do you expect a user computer to be a useful tool and at the same time avoid each and every access to the shell?  Just as an example: I am writing this from konqueror; now I go to See/See source of the document; it opens it within kwrite; now I edit kwrite preferences so it use kate bindings; reopen it and choose the option to open a command line; done.\n\nI don't say you can't cope with this very spefic way to gain a command line; you'd probably do, what I say it is that you can't hope to control each and every way to do it in any \"useful\" bussiness environment (thus \"complex\").  Are you sure there's no way I can access vim, or rewrite an environment variable, or...?\n\nFor a proper \"policy enforcing\" environment, there must be an intimate couplement between the OS (so it can enforce ACLs, for instance), the shell and the GUI, and there is no unix tool I'm aware of that will give you that; without this couplement all you achieve is hand-taking your user on *some* circumnstances, but certainly not enforcing anything on him."
    author: "devil advocate"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "The primary part of the kiosk system comes from an advantage of the unix-like systems, it's way to implement access restrictions on files. Not decouplement of the GUI from the \"system\", this is only secondary as in scenarios like \"crash the GUI to get cli\". \n\nThis is not comparable to win 95 where every user in reality are administrator. A simple shell access will not let you upgrade your \"policies\", since you still don't have write access to the files defining your kiosk environment letting you change them.\n\nTo get what you call a proper \"policy enforcing\" environment, in addition to kiosk you may change user/group policy's for \"dangerous\" applications or remove them completely from the system. If you want to restrict users to only certain part of the desktop functionality, you also have to restrict their access to similar comandline tools. "
    author: "Morty"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "But still, these restrictions are not enforced by the OS - the applications themselves decide whether to respect them or not. Non-KDE applications would not care about them at all.\n\nIf you could get shell access, you could exit from KDE and start a different desktop environment or window manager. Or even if the admin got rid of everything but KDE, you could still logout, etc. - do some minor damage that a kiosk environment should not let you do.\n\nBasically, a program can't really stop the user from doing something, if it has the same privileges as the user. It should be the job of the OS, or a program running as a different user that has only a limited interface, etc.\n"
    author: "Dima"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "Why wouldn't KDE be running as a user with limited access? Then Kiosk mode lets you 'lock down' the user-specific settings that a user would normally have control over. So they can't add a bunch icons to the desktop, set Konqueror's homepage to pr0n etc. \n\nTo my surprise, a few weeks ago in the the breakroom at work a few KDE Kiosk's appeared replacing the scary looking Windows 9x machines. They're working well. "
    author: "Ian Monroe"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "Stop talking about something you don't understand. If KDE is properly set up, KIOSK can really lock things down so that you have no way to change your environment."
    author: "Me or Somebody Like Me."
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "Why don't you publish such settings so that devil's advocate can try to break out of them?"
    author: "Martin"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "There's no need to publish anything to be able to say that what devil's advocate is saying is completely non sense. how could you compare Windows 95, a single user environment, with a multi user OS as a Unix? you can do everything you want, you can even mount the user home with a noexec attribute, so what's DA's point?\n\nAnd anyway it should be him the one who proves KIOSK weakness with a proof-of-concept."
    author: "Davide Ferrari"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-28
    body: "\"how could you compare Windows 95, a single user environment, with a multi user OS as a Unix?\"\n\nBecause I was NOT comparing Windows 95 to Unix, I was comparing a tested way of doing things (tested on Windows 95) with that very same strategy on KDE.  I said that strategy was a noway in Windows 95 because it couldn't be enforced at the OS level (rigthly so, because Windows 95 by itself hadn't support for proper environment policy enforcement), and I say it is no real way for KDE too because of the same: KDE is decoupled from underlying OS and without that couplement you are going nowhere.\n\n\"you can do everything you want, you can even mount the user home with a noexec attribute, so what's DA's point?\"\n\nYes, I could.  Or I couldn't, KDE's Kiosk will do nothing to impose one way or the other, will do?  Again, for most *productive* environments are you, for instance, ready to cope with the burden of mounting home 'noexec' thus avoiding your users having useful own-developed scripts for automate their day-to-day tasks (or/and mounting 'noexec' too wherever $TEMP or $TMP points to)?  Are you going to allow for a *sane* productive environment under typical UGO perms *and* making the user unable to delete (or modify) their environment-declaring files (like .bashrc)?  Are you ready to *proof* yourself there's no way for the user to declare/overwrite an environment variable like $KDEHOME thus making irrelevant *even* the KDE-related Kiosk framework except for the simplest scenarios?\n\n\"And anyway it should be him the one who proves KIOSK weakness with a proof-of-concept.\"\n\nNot at all: Kiosk framework is *obviously* conceptually-wise not the way to *enforce* policies onto a user, thus I shown both with my simplest example on the first post and those questions in this one (mostly rethoric: I know for sure my answer is \"no\" for them).  The burden of the proof is really in your roof, not mine: offer me an environment I can do my day-to-day work on (which more than probably will include non-kde tools like OpenOffice too) I can't break out from, and you *may* have a case (I say \"may\" since even if I am unable to break out it still will *seem* to be possible for other to success where I failed)."
    author: "devil advocate"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-29
    body: "You are right. In an otherwise default box I can do ctrl-alt-f* to get a shell. Any application that doesn't use the KDE configuration wouldn't know anything about kiosk.\n\nFor that matter I could hit the reset button and boot a cd or floppy.\n\nAnd so? If an administrator wants a firmly locked down box they would have to use many different levels to limit the users, ie. unix permissions, selinux, or whatever fits his needs. And then use kiosk to put some limit on the desktop.\n\nKiosk is a means of controlling (turning on or off) desktop function. It is one level, a very useful one at that. On an otherwise securely set up box, it will permit the administrators to further limit function. Or to put it better, allows the administrator finer control over function to allow work to get done.\n\nEach access control mechanism has it's own focus. I doubt if any one mechanism could cover the whole range of desired function.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-04-27
    body: "After reading Barry O'Donovan's article and about one half of the kiosk readme http://webcvs.kde.org/kdelibs/kdecore/README.kiosk?view=markup, it seems to me that kiosk is currently pretty tight. The only way I can think of bypassing a configured kiosk system is to use a program that can change the values of environment variables or cause kde to employ a comprimised resource eg a configuration file found on a usb stick .\n"
    author: "dryclean_only"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-05-04
    body: "\"The only way I can think of bypassing a configured kiosk system is to use a program that can change the values of environment variables or cause kde to employ a comprimised resource eg a configuration file found on a usb stick\"\n\nThat means almost anything will do."
    author: "devil advocate"
  - subject: "Re: The problem with kiosk-like \"solutions\""
    date: 2005-05-04
    body: "\"That means almost anything will do.\"\n\nYes? Please tell me how you get KDE to employ a configuration from a non-standard location."
    author: "ac"
  - subject: "Implementing"
    date: 2005-04-27
    body: "A few weeks ago I added kiosk support to konversation so that you can restrict downloading/uploading files.  I have no idea what to do now.  How do I add those to a general 'lock down' setting in the kiosk tool?  Do I have to modify the kiosk tool to know about my new settings or what?\n"
    author: "JohnFlux"
  - subject: "Re: Implementing"
    date: 2005-04-27
    body: "Make sure to document these kiosk settings in kdelibs/kdecore/README.kiosk\n\nYou can make them available in kiosktool by adding them to kiosk_data.xml\n\n"
    author: "Waldo Bastian"
---
The KDE Kiosk is a framework that has been built into KDE since version 3. It allows <a href="http://www.kde.org/areas/sysadmin/">administrators</a> to create a secure and controlled environment for their users by customising and locking almost any aspect of the desktop. It is an essential element for deploying KDE in the enterprise environment. <a href="http://www.barryodonovan.com/">Barry O'Donovan</a> has written <a href="http://enterprise.kde.org/articles/kiosk-lp.php">an article</a> (to appear in the June edition of <a href="http://www.lpmagazine.org/">Linux+</a>) which outlines the features of the Kiosk framework and gives some examples for implementing it.





<!--break-->
