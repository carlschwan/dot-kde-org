---
title: "People Behind KDE: Derek Kite"
date:    2005-09-12
authors:
  - "tbaarda"
slug:    people-behind-kde-derek-kite
comments:
  - subject: "Nice"
    date: 2005-09-12
    body: "to know you better (after all we're the same generation ;-) and of course thanks for the digests which keep us all aware of what's going on in other modules. Thanks also for mentionning KDE-Edu :-)\n"
    author: "annma"
  - subject: "Just like I imagined..."
    date: 2005-09-12
    body: "That is, the picture of you, Derek :)  \n\n>When I started developing, I was waiting for the digest to see if I made it >to the Digest. Can you provide any tips to get into the Digest?\n\n>Write descriptive commit logs. This will make maintaining changelogs much >easier. CC to the Digest email, or notify us of what you are working on. If >your style is lots of small commits, every so often explain what you have >done up to then.\n\nGood advice.  Not only does that allow the reader to understand what is added, but also the commit-digest producer :-D I may skip over something if it sounds trivial.  Please, tell us how hard you worked on that feature! \n\n-Sam\n"
    author: "Sam Weber"
  - subject: "Great read."
    date: 2005-09-12
    body: "I didn't know how much I wanted to read an interview by Derek Kite until I went and read it. :)"
    author: "Michael Pyne"
  - subject: "Re: Great read."
    date: 2005-09-13
    body: "I agree :)\nNice interview"
    author: "user"
  - subject: "Are you being paid to work on KDE?"
    date: 2005-09-12
    body: "Best answer to this question, ever.  As always, \"thanks Derek\"!\n"
    author: "LMCBoy"
  - subject: "Last Digest"
    date: 2005-09-13
    body: "Is the digest still published weekly? The last one is missing ...\n"
    author: "Michal"
  - subject: "Re: Last Digest"
    date: 2005-09-14
    body: "See http://digested.blogspot.com/2005/08/hiatus.html\n\nHelp is on the way."
    author: "Bram Schoenmakers"
---
He has fans all over the world. Every week he provides us with news from the latest and greatest straight out of the KDE SVN tree. This time the <a href="http://people.kde.nl/">People Behind KDE</a> presents you <a href="http://people.kde.nl/derek.html">Derek Kite</a>, the man behind the KDE Commit Digest.

<!--break-->
