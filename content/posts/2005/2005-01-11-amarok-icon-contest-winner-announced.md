---
title: "amaroK Icon Contest Winner Announced"
date:    2005-01-11
authors:
  - "jriddell"
slug:    amarok-icon-contest-winner-announced
comments:
  - subject: "Suboptimal choice"
    date: 2005-01-11
    body: "The icon set by David Vignoni looks the best.\nThe \"winner icon set\" looks like a circle with a starting rocket. Nobody would expect an audio player behind that icon."
    author: "Hugh Jazz"
  - subject: "Too bad"
    date: 2005-01-11
    body: "I must say that the new icon looks horrible, especially compared with the general crystal feel of the whole desktop.\nGreat app, ugly icon."
    author: "Anonymous"
  - subject: "No! It's great!"
    date: 2005-01-11
    body: "Glad you like the app! Also I have to say the icon looks awesome. Check out amaroK CVS to see how it feels."
    author: "Max Howell"
  - subject: "Hmm"
    date: 2005-01-11
    body: "Sorry, but I don't like the icon either. Icons should convey the purpose of the application as well as some branding for the user to remember the specific application. A wolf howling in a circle does not make me think \"juke box\" when I look at it. Not to sound stupid, but an actual juke box, a speaker, a musical note etc. would have been much better."
    author: "Jonathan"
  - subject: "Re: Hmm"
    date: 2005-01-11
    body: "The most important thing an icon can do is be distinctive so that people associate the icon with the application. Every mediaplayer in the world uses musical-notes/speakers, we wanted to make use of the fact that amaroK means wolf in the inuit language.\n\nI don't see any reason the icon needs to represent the product anymore. At what point does the user use the icon to determine what the product does? Never. The only icons that must reflect content are mimetype icons.\n\nThis icon was perfect for a few reasons:\n\n1. It scales well to small sizes, which is where it is viewed 95% of the time.\n2. It is not overly complex, thus it is distinctive\n3. It has high contrast\n4. It uses only 2 colours (this is personal preference for me)\n\nThe next best icons were those shown above IMO, but they didn't conform to all our requirements."
    author: "Max Howell"
  - subject: "Re: Hmm"
    date: 2005-01-11
    body: "Right on.  It's not overly important whether an application icon reflects the pupose of the application, it's far more important that it is distincive and easy to recognize at any size.\n\nLook at a couple icons for well known, commercial applications (on windows).  \nWinamp: a diamond shape with a lightning bolt\nMS Word: A W in a box\nPhotoshop: An eye\n\nIt's good to have simple, descriptive icons for the basics like a text editor, but for more complex applications, you're not going to get an icon that represents what the application does anyway.\n"
    author: "Leo Spalteholz"
  - subject: "Re: Hmm"
    date: 2005-01-11
    body: "Oh yeah.  Both amarok and the new icon kick ass!  Thanks."
    author: "Leo Spalteholz"
  - subject: "Re: Hmm"
    date: 2005-01-11
    body: "Also I believe, the howling wolf is a subtle reference to producing sounds. Subtle, but the link to playing audio is definitely there.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Hmm"
    date: 2005-01-12
    body: "The whole point of icons is to offer a visual cue as to what a GUI widget will do when you click it. If you're looking for the juke box application for the first time and see a little juke box picture, you'll be immediately drawn to it because it looks like what you want. I like that my home directory button is a little picture of a home, that my email button is a little picture of an envelope etc. It makes sense. When a newbie is on the computer who doesn't like exploring, it's nice to just say \"click the mail button\" if they can't just find it themselves. Having to suggest they \"click the wolf icon\" to start the juke box is just silly and doesn't help usability. Just because there are other applications in existance that have silly names and silly icons doesn't mean KDE should do the same thing. Having a desktop where every icon made sense would be wonderful.\n\nAnother icon I don't like is the Kopete one. Kopete is a great application but the icon is terrible. It's a shrunken speech bubble with 4 little blobs in the corners (For each protocol it supports. Not a good idea especially when a new protocol comes along.). In the system tray (remember, juk, amarok, kmix etc. all go there), it just looks like a blob of stuff because it's too cluttered and detailed. A much better icon would be a simple speech bubble. I don't want my system tray filled up with random, silly, icons. I want them all to look like what they do.\n\nLook at TV remotes. They have purposeful icons for most things (some things are hard to come up with obvious images for, but they try). It would look stupid and be bad for usability if they started using random images like wolves, foxes, bands etc. for different functions."
    author: "Jonathan"
  - subject: "Re: Hmm"
    date: 2005-01-12
    body: "You're missing one important point here: TV remote control buttons don't compete with each other; computer apps (especially sound related ones) do."
    author: "aquegg"
  - subject: "Re: Hmm"
    date: 2005-01-12
    body: "Applications require identity, actions/mimetypes \"offer a visual cue as to what a GUI widget will do when you click it.\" \n\nYour Kicker example is valid, but Kicker icons are actions. If SuSE/Mandrake wanted to allow XMMS/Kaffeine/amaroK to be launched from the Kicker I'd expect them to use a generic music-player icon. It is no coincidence that the icons for KMail/Konsole/KWord represent the purpose of the application. It is also no coincidence that the icons for KMail/Konsole/KWord are completely indistinguishable from 1000s of other application/action icons out there.\n\nIcons represent things, that's all. You are asking for all KDE applications to forfeit any kind of unique identity. How boring.\n\n"
    author: "Max Howell"
  - subject: "Re: Hmm"
    date: 2005-01-13
    body: "\"You are asking for all KDE applications to forfeit any kind of unique identity. How boring.\"\n\nBut what's wrong with that? I want all my GUI widgets to look the same, I want fonts used consistently, I want the interfaces to be as consistent as possible etc. I don't want applications with wacky splash-screens, silly icons and crazy unique GUIs (e.g. WinAmp/XMMS, Windows Media Player) trying to get my attention. I was all my applications to act in such a similar way that after a small learning curve, I don't need to think about applications but think about what my current task is and get on with it.\n\nAmarok does go in the system tray when you minimise it remember, so its icon should be a functional one in my opinion."
    author: "Jonathan"
  - subject: "Re: Hmm"
    date: 2005-01-14
    body: "I want all the same things as you. Really! I do! But I'm not unrealistic enough to expect all applications to be able to use icons that represent the function they perform.\n\nYou cannot expect a 32x32 icon to express an application with 200-odd features. You _can_ expect a 32x32 icon to express a button that performs a single action, like \"play\". Also you completely undervalue the importance of application identity.\n\n\"I was[sic] all my applications to act in such a similar way that after a small learning curve, I don't need to think about applications but think about what my current task is and get on with it.\"\n\nObviously we all agree with this statement. One of the easiest things for anyone to learn is an icon. Recognition after seeing one for a fraction of a second is instant for everyone I have ever met. However the icon _must_ be distinctive! Thus to give the user a usable experience you must pick a distictive icon! You would actually ruin the user-experience if you used an icon that can not be recognised easily because it is so typical and/or similar to that used by other software. Can you remember the KWord icon off the top of your head? I bet you remember the amaroK one.\n\n\"Amarok does go in the system tray when you minimise it remember, so its icon should be a functional one in my opinion.\"\n\nBut how does it get to the systray? It has to be launched by the user. There is no GUI way in KDE to launch amaroK without seeing its icon first! The only systray icons that should help the user identify their purpose are ones that the user doesn't launch manually. So Klipper only then.\n\nThanks for the discussion."
    author: "Max Howell"
  - subject: "Re: Hmm"
    date: 2005-01-14
    body: ">You cannot expect a 32x32 icon to express an application with 200-odd features.\n\nWhy should an application have 200-odd features? Amarok does not have 200-odd features; it has a large set of focused features. The icon shouldn't be representative of Amarok's ability to switch backends, or provide an OSD--those are odd features. But, the icon should be representative of Amarok's primary focus: music/sound. \n\n>Can you remember the KWord icon off the top of your head? I bet you remember the amaroK one.\n\nThe icon and program name (and description) should cue users to a program's functionality. If KWord and OpenOffice.org Writer icons are not very different because they perform similar tasks; the focus is on allowing the user to quickly find an application to perform a task. Does the program used matter? \n\n>But how does it get to the systray?\nI would argue that, in KDE and GNOME, memorability of the icon does not matter as much as it would in something like OS X. The GUI for launching applications is the menu where names and program descriptions are more visible than icons (because the text takes more space). Here, the association between application and icon is not strongly made. When the application needs to be accessed from the systray (and you know what the application's function is), one would look for an icon representing the application's function. (Is this a problem when multiple sound applications are playing? How often does one open multiple sound applications?) If applications were launched from an OS X-like docker, or from the desktop (alla WinXP), I would argue placing memorability over representation would work because the association between application and icon is stronger. (But, I would argue that initial usability would suffer because one would initially have to figure out what the icons were for.) In KDE, what would happen if one opened an instant messaging application, a calendar/organiser application, and an audio application only to have a teddy bear, a sun, and a bananna appear in the systray? Using the menu to launch the applications (rather than clicking on an icon), the user would have a difficult time deciding whether the teddy bear or bananna should be used to access the calendar.\n\nThe icon must be distinctive while still acting as a cue hinting at what the user will be able to do once clicking on it. There shouldn't be much argument whether or not the Amarok icon is useful; the icon is distinctive (a blue wolf) and useful (a wolf howling--producing a sound).\n"
    author: "dw"
  - subject: "Thanks"
    date: 2005-01-15
    body: "Thanks for the well-thought out reply. Your points make me concede that I have been stressing the identity over function argument too strongly, and like everything in life, a little of each is always required.\n\n> > You cannot expect a 32x32 icon to express an application with\n> > 200-odd features.\n> Why should an application have 200-odd features? Amarok does \n> not have 200-odd features; it has a large set of focused features.\n\nToo true. I think really what I wanted to say is it is a lot harder to represent something with the scope of a complex piece of software, than it is to represent a toolbar button with a single purpose. And thus it is not feasible to expect all software to be clearly representable by an icon that is normally viewed at 32x32 pixels.\n\nHowever I agree with you that it is sensible to try and indicate what the application does with the icon. Hence the Firefox icon beats the Mozilla icon."
    author: "Max Howell"
  - subject: "Re: Hmm"
    date: 2005-01-16
    body: "There is also a little identity to it. An even better icon for Amarok would perhaps be a sound note, but then there's Rosegarden for which the note would be an even better fit (where you actually write music and produce notes)."
    author: "Jonas"
  - subject: "Re: Hmm"
    date: 2005-01-12
    body: "\"At what point does the user use the icon to determine what the product does? Never. \"\n\nYou're dumb.\n\nLet me guess, you never went to usability training...because you are clueless. Although, looking at the haphazard design of Amarok...."
    author: "Joe"
  - subject: "Re: Hmm"
    date: 2005-01-12
    body: "Most people consider choosing to insult someone rather than argue a point, fairly \"dumb\"."
    author: "Max Howell"
  - subject: "Re: Hmm"
    date: 2005-01-13
    body: "checkout some other icons:\nmozilla - a dragon\nfirefox - a fox\nkbear - a bear\nKuickshow - a flower\n"
    author: "ac"
  - subject: "Re: Hmm"
    date: 2005-01-14
    body: "Yeah, those icons aren't very good choices either.  Check out KMail - a picture of a pile of mail.  Kate - an open notebook with a pen.  The icone for the home directory - a picture of a home.  And so on.\n"
    author: "Jim"
  - subject: "Re: Hmm"
    date: 2005-01-15
    body: "The home icon is not an application, it is an action, thus it MUST have an icon that reflects its purpose. You can't have a toolbar button that doesn't give the user a hint as to what happens when s/he pushes it.\n\nHowever, if someone laid out the kword, kate and kmail icons in front of me without me knowing it was those three applications that were in question, I would not be able to tell you what applications were behind those icons. \n\nThis is what I've been trying to make a point about. It doesn't much matter for applications like KMail and Kate since currently they are synonymous with KDE. amaroK, Mozilla, Firefox, etc. all need to be recognised outside of the context of desktop environments. Using an icon that reflects the application's purpose is a secondary consideration for such applications, it is far more important that people recognise the symbol should they see it on the web, in magazines, on posters, on t-shirts, in newspapers or even in their dreams."
    author: "Max Howell"
  - subject: "All great"
    date: 2005-01-11
    body: "Congrats to da-flow, and also to euxneks and David Vignoni. Their icons are all great."
    author: "John Icons Freak"
  - subject: "The original version"
    date: 2005-01-11
    body: "http://kde-look.org/content/show.php?content=18848\n\nthe original version of Blue Wolf looked much better"
    author: "gerd"
  - subject: "Re: The original version"
    date: 2005-01-11
    body: "Yes, i prefer this version too"
    author: "gnumdk"
  - subject: "different icon"
    date: 2005-01-12
    body: "the artist submitted two versions: dark wolf and blue wolf.\n\nYes, the blue wolf icon was changed a bit after the contest for various reasons, but the icon to which the parent linked is not the same entry."
    author: "Dhraakellian"
  - subject: "i like it"
    date: 2005-01-11
    body: "I think it's a great icon.  Classy and mysterious."
    author: "ac"
  - subject: "Re: i like it"
    date: 2005-01-11
    body: "Me too. I also like David's one and from time to time I switch my icon set to Nuvola. I am very pleased to see so many talented artists, this is amazing. I also had a look at the svg wallpapers, fantastic work. \nHaving different icon styles provides choice which is always a good thing.\nBravo to all the artists! "
    author: "annma"
  - subject: "no..."
    date: 2005-01-12
    body: "> The most important thing an icon can do is be distinctive so that people \n> associate the icon with the application. Every mediaplayer in the world uses \n> musical-notes/speakers, we wanted to make use of the fact that amaroK means \n> wolf in the inuit language.\n\nNo, the most important thing is that people are able to figure out what something is based on a name/icon. One of the problems plaguing open source software is that the application creators REFUSE to come up with good names for their app and instead use silly personal names. GIMP? How the hell is your grandma supposed to know that \"GIMP\" means a photo manipulation suite? That's why people that actually SELL their products come up with at least HALFWAY descriptive names for them like \"Photoshop\" and \"Paint.\"\n\nIcons are the same. Icons should be recognizeable to the user as to what they do or at least what company they come from. This winning icon does NONE of that."
    author: "Dan Ostrowski"
  - subject: "Re: no..."
    date: 2005-01-12
    body: "> That's why people that actually SELL their products come up with at least HALFWAY descriptive names for them like \"Photoshop\" and \"Paint.\"\n\nAhh, that's why one of our products is called \"Mareon\" which is a B2B solution for the real estate industry. I'm sure you would've guessed that from just hearing the name.\n\n> No, the most important thing is that people are able to figure out what something is based on a name/icon\n\nYou might want to read the following paper about visual memory and file icons:\nhttp://www.idiom.com/~zilla/Work/visualids.pdf\n\nIcons are a fast visual clue to identify the application I want to start. There is no way that 48x48 pixel can tell you enough about an application that you immediately know what's the purpose of it.\n\nThe best icon is one that is really different than the other icons from applications in the same category.\n"
    author: "Christian Loose"
  - subject: "Re: no..."
    date: 2005-01-12
    body: "WinRAR\nPGP\nOutlook\nExcel\nAccess\nInternet Explorer\nAdobe Acrobat\nAdobe GoLive!\nSteam\nAlcohol 120%\n\nAll those seem to tell me very little about the program, (IE does the most, but to someone new to windows they wouldn't really know what it is), but the rest tells you nothing.\n\nFYI: GIMP stands for GNU Image Manipulation Program"
    author: "Corbin"
  - subject: "Re: no..."
    date: 2005-01-12
    body: "Well to just be pedantic, should people know what a RAR file is, based on that, WinRAR is intuitive, and given that PGP is an abbreviation, Pretty Good Privicy, indicates its something to do with privicy... encryption.. i think that one also is intuitive, but the rest, are, as u said... random"
    author: "Martin"
  - subject: "Re: no..."
    date: 2005-01-12
    body: "\"FYI: GIMP stands for GNU Image Manipulation Program\"\n\nAnd the point of this FYI is what?\nWe know that. It's still called the gimp. It's not called the GNU Image Manipulation Program...."
    author: "Joe"
  - subject: "Re: no..."
    date: 2005-01-12
    body: "One of the problems plaguing Dan Ostrowski is he REFUSES to come up with a good name for himself, instead he uses a silly \"personal name\". DAN? How the hell is grandma supposed to know what kind of person Dan is?"
    author: "Max Howell"
  - subject: "I'm glad I use Nuvola"
    date: 2005-01-12
    body: "Just sayin'. "
    author: "beacon"
  - subject: "Make it an option?"
    date: 2005-01-12
    body: "Blue Wolf is good IMO, but why not let to choose an icon?"
    author: "arael"
  - subject: "Really now.. "
    date: 2005-01-12
    body: "Tool tips would be the best way to decide what a program does.\nHow about Task Based menus?  That would indictate what a program's general purpose is as well( ie. Office, Multimedia->Audio Player, System->Configuration->YaST).\nIcons are a way to look \"pretty\" on the desktop and in menus to fit the current look of your interface. Names are a way to identify uniqueness to an application from a group of applications."
    author: "Ryan"
  - subject: "Kmenu"
    date: 2005-01-13
    body: "OK, this whole icon must reflect the app is pure rubbish. Where do you go searching for your newly installed media player? My guess is KMenu -> Multimedia -> amaroK. Hmm, I wonder what all these apps in Multimedia do, I guess I need five different speaker icons in there so I won't get confused. \n\nI could see if an app is default on the Desktop maybe, but for anything other than that you should have something distinctive. Having several icons that look similar is less usable than having distinctive icons in an orderly menu. For example, Kmix next to another media app in the systray and you have instant confusion. Which of these two speakers do I click to raise my media app? Now let's throw in the Wolf. In an instant I can tell what is Kmix, and what is amaroK.\n\nThe new icon seems to fit the needs of amaroK imho, it looks good, scales well, and most important of all, no more kmix confusion.\n\n"
    author: "Mike Diehl"
  - subject: "Bad choice IMHO"
    date: 2005-01-16
    body: "\"KDE-Look.org's amaroK icon contest received 28 amazing icons for the amaroK developers to choose from.\"\n\n28 amazing icons? Who wrote this one? Give me a break. 3-4 icons are OK, and the rest is crap. And the  \"winner\" icon belongs to the rest. The result has been expected because Amarok's developer(s) think that they besides their excellent hacker skills own artist skills. \n\nAnd this is too bad. I feel that people who participated in the contest have also been abused. Amarok developer(s) create a contest on kde-look.org and then he/they choose an amateurish created icon. \n\nThey proclaimed that submitted icons should have 'crystal' look and then choose one which doesn't have 'crystal' look.\n\nThey proclaimed that submitted icons should have something to do with wolf's eye because Amarok means a wolf eye, and then choose a wolf's head instead.\n\nThe winner icon is not good because:\n\n1. it is too complex ! It is not true that it scales well. The wolf's head is too complex and not too mention unnecessary circle around the head.\n\n2. it doesn't have a good contrast ! Dark blue and white is not a good combination. Especially because everything in KDE is blue.\n\n3. it is meaningless ! Zero usability. AmaroK is a music player ! Doesn't matter what AmaroK means in some language (wolf's eye or wolf's tail), its icon should show something which is connected to music and not to animal planet. \n\n4. it is not consistent ! It simply doesn't have a crystal look.\n\n5. it is plain and boring ! It is not colorful as an application icon should be, and the wolf's head is boring (it looks like any other wolf's head - see excellent Gimp icon for better understanding), as if the author just copied and pasted a wolf's picture, added some gradients and hopla.\n\n-------------------------\n\n"
    author: "wygiwyg"
  - subject: "Re: Bad choice IMHO"
    date: 2005-01-16
    body: "Good day sir! I wonder, could you tell me, where does one go to get constructive criticism? Is there any available in the vicinity of this site?\n\nI apologise for the misleading text on the contest announcement, the points were only meant as suggestions, we are not perfect and thus our announcement was not perfect either. We were just hoping to get a good icon for our project and we were truly astounded by the huge numbers of entries and the high quality of the artists. I'd like to thank again all the artists who spent their time working on the icon :-)\n\nPS. the icon is not complex, does scale well, does have good contrast, is easy to identify (and thus has good usability), and is not boring (in our opinions). Otherwise I accept your points."
    author: "Max Howell"
  - subject: "sorry..."
    date: 2008-12-17
    body: "i'm not going to get into argumentation about wolf icon. i can just tell you that since i've seen it for the very first time it was annoying. i've searched for months to find alternative music player just because of wolf icon. however there is no better music player than amarok so every few minutes when i glance over it a question is coming to my mind: what the f*** is this wolf doing on my desktop. :)\nplease, please, please change it!!!"
    author: "Goran Miskovic"
---
<a href="http://www.kde-look.org">KDE-Look.org</a>'s amaroK icon contest received 28 amazing icons for the <a href="http://amarok.kde.org/">amaroK</a> developers to choose from. The winner is ...

<!--break-->
<p><img src="http://static.kdenews.org/fab/screenies/amarok/cr64-app-amarok.png" align="left" width="64" height="64" /> The winner is <a href="http://kde-look.org/content/show.php?content=18839">Blue Wolf</a> by <a href="http://kde-look.org/usermanager/search.php?username=flow">da-flow</a>.</p>



<p><br /><img
 src="http://static.kdenews.org/binner/amarok-icon-contest/wolf-howl.png" align="right" width="64" height="66" />KDE-Look.org's highest rated amaroK icon is <a href="http://kde-look.org/content/show.php?content=18560">Wolf Howl</a> by <a href="http://kde-look.org/usermanager/search.php?username=euxneks">euxneks</a>.</p>

<p><img src="http://static.kdenews.org/binner/amarok-icon-contest/am-a-rock-star-band.png" align="left" width="64" height="48" />The contest also inspired some great humour and craft from <a href="http://kde-look.org/usermanager/search.php?username=dadeisvenm">dadeisvenm</a> with this newfound talent <a href="http://kde-look.org/content/show.php?content=18831">AM-a-RoK-star - The Band</a>.</p>

<p><img src="http://static.kdenews.org/binner/amarok-icon-contest/nuvola-amarok.png" align="right" width="64" height="64" />The Nuvola icon set creator <a href="http://kde-look.org/usermanager/search.php?username=Dave">David Vignoni</a> was inspired to create his amaroK icon which is the second highest rated icon in the contest. His new creation will be replacing the existing amaroK icon in nuvola.</p>

<p>Meanwhile the <a href="http://www.kde-look.org/news/index.php?id=148">SVG wallpaper contest has closed</a>.  Over 80 entries were received and the judges will announce the winners soon.  The next KDE-Look.org contest will be announced in a few days time.</p>
