---
title: "Cornelius Schumacher on KConfig XT"
date:    2005-07-08
authors:
  - "jriddell"
slug:    cornelius-schumacher-kconfig-xt
comments:
  - subject: "KConfigEditor, erm..."
    date: 2005-07-08
    body: "...so KDE got its Registry Editor/GConf Editor now? Don't know what to say about that..."
    author: "ac"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "Good!\n\nReally!\n\nJust because MS's implementation of a good idea is an absolute mess doesn't mean the original idea is bad.  What you should remember (if I understand correctly) is that nothing has really changed... there's just a gui to access everything from one place rather than opening up every single application's own configuration.  This is useful!\n\nL."
    author: "ltmon"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "or you could look at it that KDE now has an application to generically edit your configuration files in a way that tells you what the settings are for (as opposed to cryptic \"keys\" you have to guess at) and what the possible settings are.\n\nnot every config file editor gui is suddenly bad because a couple of such apps out there aren't great. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "The config isn't one big file/package that is easily broken, taking out the whole system. If I understand correctly the config files are still text, just that they are generated and read by code. One can still use the unix tools to change, automate, distribute, or whatever the configuration data.\n\nAgain if I understand correctly, this whole thing is for the developer to handle configurations. One writes an xml file with the configuration items, defaults and options, then a config file is produced, the code to change, edit, read and parse the config file is provided.\n\nI fail to see how that is similar to Windows registry.\n\nI really have trouble taking someone seriously when they are really suggesting that we all create and edit our configurations by hand, with no options other than sed and awk.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "yep, that's about it =)\n\nbtw, my favourite feature is that it doesn't save default values to the file, making changing system defaults easy. prior to this changing system wide settings was a massive annoyance as users would have the previous defaults written to their local configs which would then override subsequent changes."
    author: "Aaron J. Seigo"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-09
    body: "This is my favorite feature as well (but to be honest I probably don't know all features).\n\nI'm wondering how existing local configfiles are migrated to KConfig XT?\n\nAre values in local configfiles removed if they are equal with the current system defaults? This would make it possible to change system defaults and activate them for users that already have local configfiles from previous versions.\n\nOr is it necessary to remove all local configfiles and let KConfig XT recreate them based on differences from the default.\n\nWhat I'm trying to say is that I would like when upgrading packages that KDE is able change system defaults and activate them for me (for example saner default values have been agreed upon based on usability studies), but removing all local configfiles and start all over seems like a lot of work."
    author: "LB"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "I guess you mean Windows registry and GConf from GNOME, I certaintly think Windows registry sucks but I have a different opinion about GConf, the editor sucks that's true but the structure of XML config files works like a charm, KConfig XT by the other hand needs to warp .INI files to XML to get the job done, there's nothing wrong with it but the ideal should be get rid of the .INI files and use only XML, maybe is not possible right now but hopely in the future.\n\n"
    author: "Alex"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "> KConfig XT by the other hand needs to warp .INI files to XML to get the job done\n\nNo, you got something wrong there. \n\nThe *definition* of the configuration parameters (how they are named, what type they are...) is in XML.  The kconfig_compiler then generates C++ code to access the settings in conventional ini-style plain text config files.  That code is compiled and becomes part of the app you use. There's no conversion from .ini data to XML.\n\n"
    author: "cm"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "Thank you! Thank you thank you thank you.\n\nThis stuff is buzzword compliant enough as it is."
    author: "Jonas"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "KConfigXT is independent of the storage detail, it works via the KConfig classes which encapsulate those details. so there is no \"warping\" of one format to another going on; the two ideas are actually orthogonal. i believe Cornelius even said as much in his interview.\n\nfurthermore, the primary benefit of using XML over \"ini style\" formatting for configuration files is larger files that are slower to parse. ;) but yes, it is completely possible to move directly to XML today if it actually was useful since that detail is abstracted away from the code that uses KConfig.\n\nbut i'd much sooner see a performant network storage back end for KConfig that a buzz word wanking XML back end. network storage (samba 4's new back end, ldap, sql, whatever!) would have real world benefits and be very nice for centralizing management even more than it already is. the big hurdle here is making sure that you can disconnect a computer from the network and have your settings go with you. in other words, a master-slave replication issue."
    author: "Aaron J. Seigo"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-11
    body: "\"but i'd much sooner see a performant network storage back end for KConfig that a buzz word wanking XML back end. network storage (samba 4's new back end, ldap, sql, whatever!)\"\n\nLDAP support is in progress:\n\nhttp://bugs.kde.org/show_bug.cgi?id=101716\n\n(BTW, samba4 will still be using an external LDAP server under certain cirumstances anyway ... and in all cases where KConfig support would be desireable)"
    author: "ranger"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "You're very late with your flame: submitted to kde-apps.org one year ago."
    author: "Anonymous"
  - subject: "Re: KConfigEditor, erm..."
    date: 2005-07-08
    body: "I think people are mixing up 2 things. First, there is KConfigXT, which is a programming API. Instead of using KConfig, you are now using the classes and tools provided by KConfigXT. You write a XML file, which describes all options. This makes handling options much easier, also saving the state of an application before exit is easier now. However, this all is mainly a developer issue.\n\nOn the other hand you have an editor. The editor also uses the information provided in those XML files. You can configure the settings of different applications from one place. This is especially nice for sysadmins. They configure KDE one time and then upload the settings to all workstations. This is a user issue and you are not force to use this GUI anyway. You can always set the options from inside the applications through the config dialog.\n\nHope everything is more clear now,\n\nSebastian"
    author: "Sebastian"
  - subject: "Unasked question"
    date: 2005-07-08
    body: "So, where does the XT in \"KConfig XT\" come from? How did you come up with that name?"
    author: "Thiago Macieira"
  - subject: "Re: Unasked question"
    date: 2005-07-08
    body: "While I'm not Cornelius, I'd guess it means \"eXTended\".\n"
    author: "Mark Kretschmann"
  - subject: "Re: Unasked question"
    date: 2005-07-08
    body: "Xylonite Temerarious (look it up)"
    author: "ac"
  - subject: "xylonite temerarious"
    date: 2005-07-11
    body: "All Google shows me is advertising for bestial pornography.  Separating the terms tells me Xylonite is a trade name for celluloid (a plant-derived plastic film) and temerarious means bold, daring or rash.\n\nSo, wrong answer.\n"
    author: "Jonathan Maddox"
  - subject: "Re: Unasked question"
    date: 2005-07-08
    body: "See http://websvn.kde.org/trunk/KDE/kdelibs/kdecore/DESIGN.kconfig?rev=409203&view=markup\n\n<blockquote>\n   My buzzword picker offered KConfig XT (\"eXtended Technology\") and KConfig NG \n   (\"Next Generation\").  Since the planned changes are ment to be evolutionary \n   rather than revolutionary, KConfig NG was dropped.\n</blockquote>"
    author: "Waldo Bastian"
  - subject: "Poll on KDE-Look"
    date: 2005-07-08
    body: "There is a related poll on KDE-Look (http://www.kde-look.org/poll/index.php?poll=113) that deals with a central repository for configuration data and a lot of people are interested in Elektra.\n\nWhile I see that KConfig XT goes beyond the scope of Elektra, wouldn't it have been a good idea to use Elektra as backend for KConfig XT instead of inventing parts of the wheels for a second time?"
    author: "Yaba"
  - subject: "Re: Poll on KDE-Look"
    date: 2005-07-08
    body: "KConfig XT isn't about reinventing the wheel at all. It still uses the same *rc config files that the kconfig classes use (plain text in INI-style). It's just a more convenient way for the programmer to access the settings...\nWhat's new is that for each application there is an additional xml file that describes what settings can be inside the corresponding *rc file.\n\nAnd the config editor also is only a simple frontend to edit the old rc files, with the additional advantage that it can also get a description of each setting from the xml file (which does *NOT* hold the actual settings, but serves only as a description of what can be in the *rc file, with type, what's this message, description etc.).\n"
    author: "Reinhold"
  - subject: "Re: Poll on KDE-Look"
    date: 2005-07-08
    body: "Additionally the INI-style file backend is just the currently used implementation of the KConfig backend.\n\nI guess there is no technical limitation for creating an Elektra based backend."
    author: "Kevin Krammer"
  - subject: "Re: Poll on KDE-Look"
    date: 2006-05-16
    body: "Sadly there is ...\n\nI just written a patch for KConfig use elektra rather than ini files.\nThe problem is that impose some ugly hack since KConfig rely heavily on the\nfact that configuration are stored individualy in files which is, of course,\nnot the case with elektra.\n\nKConfig will probably have a XML backend one day, but if things doesn't change (KConfig API), it will never had some funny other backend ... :-/"
    author: "seasonfive"
  - subject: "Whene will be ready?"
    date: 2005-07-08
    body: "Nice, but he doesn't say when will be ready just like Thenor, that makes it one of the two biggest vaporware in KDE.\n\n"
    author: "Alex"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "Quote: \"KConfig XT is used all over the place now.\""
    author: "Anonymous"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "It is?\n\nI haven't seen any application or frontend of it?\n\nPlease be more specific, what applications are taking advantage of it and where can I download the FrontEnd?\n\n"
    author: "Alex"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "Why don't you just read the interview instead of pretending you know what you're talking about?"
    author: "ac"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "I did,  it only says to look KOrganizer but nothing else."
    author: "Alex"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "And don't get rude on me, Im just corious.\n"
    author: "Alex"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "the point is, this is a PROGRAMMERS TOOL to make coding applications (esp the config file part) easier. an user can't use this too, he only sees the advantage if the applications get better (more stable and easier to code). and of course, the framework gives the advantage that it makes it possible to write tools like kconfigeditor, or coupling the KDE config file system with Electra."
    author: "superstoned"
  - subject: "Re: Whene will be ready?"
    date: 2005-07-08
    body: "Run\n\nfind . -name '*.kcfg'\n\nover your KDE sources and you will know. It gives 149 hits in my check out.\n\nThe KConfigEditor front end is available from http://extragear.kde.org/apps/kconfigeditor/ since several months already."
    author: "Waldo Bastian"
  - subject: "It works with Ruby too!"
    date: 2005-07-08
    body: "The Ruby Korundum bindings install a tool called 'rbkconfig_compiler'. \nIt takes .kcfg/.kcfgc files and compiles them into ruby code. \nThere is a KConfigXT/Ruby project template in KDevelop 3.2.x, which shows\nhow the combination of .ui and .kcfg files is language independent from KDE 3.4 onwards.."
    author: "Richard Dale"
  - subject: "Question about the .XML file"
    date: 2005-07-08
    body: "If I got it right, the .XML file holds a description of the structure.\nThe developer uses this to generate code.\nThe format of the *rc file is not changed.\nSo i would assume that the .XML File is not necessary on the user machine.\n\nBut where does the kconfigeditor get the informations from (e.g. the whatsthis texts)?\nDo I need to have the .XML files on my machine to use kconfigeditor?\n"
    author: "Peter"
  - subject: "Re: Question about the .XML file"
    date: 2005-07-09
    body: "To take advantage of the additional information in the XML you need to have them installed yes. You probably do, see your $KDEDIR/share/config.kcfg directory."
    author: "Waldo Bastian"
  - subject: "Re: Question about the .XML file"
    date: 2005-07-09
    body: "Thank you, that makes it clear.\nAnd yes, you are right: The mentioned directory holds the .kcfg files.\n\n"
    author: "Peter"
---
KConfig XT is an increasingly commonly used technology in KDE programmes.  KDE Dot News talked to creator Cornelius Schumacher (also recently <a href="http://dot.kde.org/1117363968/">interviewed on KDE PIM</a>) to find out the advantages of KConfig XT over plain old KConfig.  Read on for the full interview -- and if you think your programme might benefit from KConfig XT, <a href="http://developer.kde.org/documentation/tutorials/kconfigxt/kconfigxt.html">Zack's tutorial</a> is a good place to start.






<!--break-->
<div style="border: thin solid grey; padding: 1ex; width: 200px; float: right"><img src="http://static.kdenews.org/jr/cornelius2.jpg" /><br />Cornelius</div>
<p><strong>Most developers will be familar with KConfig which reads and saves settings out to files. What does KConfig XT add to this?</strong></p>
 
<p>While KConfig is a powerful and efficient way to handle configuration settings it doesn't address two things: Type safety and GUI. KConfig is great as backend, but to address these two areas we had to put another layer on top of it. That's what KConfig XT is. The key feature of KConfig XT is that it provides a machine-readable description of the configuration settings, so that we can do all kind of fancy stuff like generating type-safe code to access the settings, associate the settings with a GUI or provide tools like Zack's <A href="http://extragear.kde.org/apps/kconfigeditor/">KConfigEditor</a> with the ability to give the user the needed context for editing configuration files.</p>
 
<p><strong>What do you mean by "type safety" in the context of KConfig XT?</strong></p>
 
<p>To access a setting by KConfig you need to know two things: the string representing the key of the setting and the type of the setting. If you make wrong assumptions about the type, KConfig can do nothing to prevent this. It will happily read a string as a number, if requested by the programmer. Another source of error are the string keys. If you make a typo in the string KConfig once again can do nothing to prevent this. Your program will behave strange with no indication of the error.</p>
 
<p>KConfig XT solves these problems. It takes an XML description of the configuration settings and automatically generates the code necessary to access them. The generated code provides a clean type-safe API and all the error-prone string key and type handling is encapsulated, so that the developer doesn't have to care about this anymore. So if the developer now makes errors when accessing the settings, the compiler is able to tell, if the name of the setting or the type are wrong.</p>
 
<p>As an additional benefit the generated code also takes care of reading and writing the settings, immutable settings, labels and whatsthis texts, singleton access, default values and more.</p>
 
<p><strong>KConfig XT can generate configuration dialogues, how does it achieve this?</strong></p>
 
<p>Right now KConfig XT can't fully automatically generate dialogs. But it provides a very easy way to associate widgets from a dialog with configuration settings. This makes it possible to create a dialog for example with Qt Designer and just by associating the widgets via their names with the settings get all the reading and writing of the setting for free. So you can create a configuration dialog without writing any C++ code.</p>
 
<p>The next step would be to also generate the dialog from the settings description. I'm quite sure that it's possible to do that for at least 90% of the KDE configuration dialogs. Up to now there only is a very small prototype, but I hope I can provide a more complete solution by aKademy this year.</p>
 
<p><strong>Is using KConfig XT any easier for a programmer than directly reading and writing using KConfig?</strong></p>
 
<p>Definitely. My main motivation when starting to write KConfig XT was to get rid of the need to write all that annoying and error-prone code to handle KConfig settings at the string key level, with all the unnecessary duplication of things like default values. I think, this mission was accomplished. Once you get used to KConfig XT you don't want to go back anymore.</p>
 
<p><strong>The KConfig XT tutorial states that it makes the administration of large KDE installations more manageable, how does it achieve this?</strong></p>
 
<p>KConfig XT provides a simple and machine-readable description of the configuration settings. This can be used by management tools to effectively handle configuration files without the danger of making wrong assumptions about their content.</p>
 
<p><strong>What prompted your development of KConfig XT?</strong></p>
 
<p>As I already said, my main motivation was to get rid of the annoyance of having to write the same ugly code for accessing configuration settings again and again. One of the most central principles of software design is the DRY principle: Don't Repeat Yourself. I felt that having to deal with KConfig directly violated this principle in numerous ways. KConfig XT is the solution to this problem.</p>
 
<p><strong>What problems did you come across when developing KConfig XT?</strong></p>
 
<p>The heart of KConfig XT is "kconfig_compiler", the tool which takes the XML description of the settings and generates the C++ code to access them. Writing code that writes code always is, well, interesting. You have to think in a very twisted way, because of the additional indirection. Especially when it comes to things like escaping characters, it's easy to get lost. On the other hand it's fun, because it's challenging and you can feel the power of this technique, because you can have great effects with only very little code.</p>
 
<p>Another challenge was the integration with the build system, because using code generators, integrating the generated code in the build system and getting the dependencies right doesn't work out of the box. Fortunately our build system gurus did a great job and tackled this problem. Now it's dead easy to use KConfig XT: Write the XML file, add it to the sources line of your make file, enjoy the comfort of the generated classes.</p>
 
<p><strong>Has there been much take up of KConfig XT in KDE?</strong></p>
 
<p>KConfig XT is used all over the place now. Not all applications have been ported yet, but when reading the commit messages you will notice "ported to KConfig XT" commit logs again and again.</p>
 
<p><strong>What is the best example of KConfig XT use in KDE?</strong></p>
 
<p>Good question. I don't have a complete overview about where it is used and I actually can't imagine how it could be used in a bad way, as the interface is pretty straight-forward. But I would recommend to look at KOrganizer because that's where it all began.</p>
 
<p><strong>Sebas on IRC wonders if KConfig XT supports locking for Kiosk and about the possibility of a network option for editing settings on a remote machine.</strong></p>
 
<p>I don't think that's in the scope of KConfig XT. Locking is probably more a backend issue and remote editing I would see on the application level. As far as I know KConfigEditor already provides the option of remote editing configuration files.</p>
 
<p><strong>There has been some discussion on freedesktop.org's XDG mailing list about a possible cross-platform configuration system (DConf), how would this affect KConfig XT?</strong></p>
 
<p>This probably wouldn't affect KConfig XT too much, at least not its users. KConfig XT is an abstraction of the actual configuration system, so it would be quite easy to make use of another backend. If the efforts at freedesktop.org come up with a new unified configuration system, KDE is certainly in a good position to make use of it. That's the power of our framework.</p>





