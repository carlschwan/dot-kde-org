---
title: "Kubuntu Preview Released with KDE 3.4"
date:    2005-03-18
authors:
  - "jriddell"
slug:    kubuntu-preview-released-kde-34
comments:
  - subject: "Screenshots"
    date: 2005-03-18
    body: "http://shots.osdir.com/slideshows/slideshow.php?release=286&slide=1"
    author: "Anonymous"
  - subject: "Re: Screenshots"
    date: 2005-03-18
    body: "osdir.com is atm slooooooooooow"
    author: "MaX"
  - subject: "Re: Screenshots"
    date: 2005-03-18
    body: "I have fetched the 58th first screenshots here : http://bobuse.free.fr/kubuntu\n\nNow, shots.osdir.com is down :(\n"
    author: "Nicolas Dumoulin"
  - subject: "Re: Screenshots"
    date: 2005-03-18
    body: "shots.osdir.com lives again"
    author: "Anonymous"
  - subject: "HAL not working"
    date: 2005-03-18
    body: "I have HAL installed and gnome-volume-manager does work and autodetect all my usb devices but when I do media:/ in konqueror it doesn't detect any of my usb device (unless I already started gnome-volume-manager). Do I need to install some dbus patch to qt or something?\n\nPat"
    author: "Pat"
  - subject: "Re: HAL not working"
    date: 2005-03-20
    body: "Perhaps it can help you : http://kde.ground.cz/tiki-index.php?page=DBUS"
    author: "Capit Igloo"
  - subject: "Final death of UserLinux"
    date: 2005-03-18
    body: "Ubuntu used to be a major thread to Bruce Perens business plan of offering an enterprise ready Debian system to the masses.\n\nNow that Kubuntu offers a shiny KDE based user oriented desktop Bruce can finally declare UserLinux dead.\n\nAnyone going to bet when Bruce will admit that he made a mistake?\n\n"
    author: "MortimeR"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "now, why do you want to see something dead?\n\nmaybe userlinux is such a good idea that it got us ubuntu and in turn kubuntu..."
    author: "bangert"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "Actually it's alot simpler than that. UserLinux depended on goodwill and contributions from developers, which died when they excluded at least half of the free software developers.\n\nUbuntu is based on cash and hiring a large number of developers. Goodwill can be bought.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "Seeing that Ubuntu doesn't have more than about 10 paied developers and seeing that Kubuntu is a community effort by people who don't get any money for their work, I fail to see your point.\n\nHowever, I agree that alienating a large part of the free software developers even before the project had really started wasn't the smartest move for UserLinux."
    author: "ralph"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "That's why I'm skeptical of Ubuntu.  So Canonical poured some money into it and made a nice release.  But where is their source of income?  The canonical site gives no indication that they are making money somehow.\n\nSo what happens when the money they do have eventually dries up?  I think I'd rather stick to vanilla Debian.  Not as glitzy but it is guaranteed to stay around.  I'm just wary of depending on some company's charity (although Ubuntu would probably survive without Canonical, it would have to change a lot)."
    author: "Leo Spalteholz"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "> But where is their source of income?\n\nThey offer services based and support for Ubuntu.\n\n> So what happens when the money they do have eventually dries up?\n\nAs most changes are merged into upstream (Debian) over time nothing gets lost."
    author: "Anonymous"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "Shuttleworth is the same fellow who blew US$20million to tag along into space with some cosmonauts. that's one hell of a vacation.\n\nso ... maybe they don't need a source of income anytime soon.\n\ni don't think money issues are going to be the challenge with Ubuntu. it'll be escaping the gravitational pull back to earth that most fad distros succumb to.  it's got a great start and some great adoption going for it; can it maintain that?\n\nfor many years now there's been a regular cycle of popular fad distros that emerge like bright stars. they get a seemingly huge user base (though in reality it's often smaller than one would think given the Internet noise) who swear by it. achieving lift off and settling in with Red Hat, SUSE, Debian, Mandrake, etc... is what most never really get to.\n\nGentoo was the most recent of these prior to Ubuntu's arrival. will either of these be a force in 3 years time? hard to say... regardless of the product quality and engineering $ behind it, what will matter is usage and community. that's what helped Mandrake survive where Caldera didn't, for instance.\n\nso ... Mark's made it into space once ... maybe he can help pull it off again."
    author: "Aaron J. Seigo"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "Good point.  If he's willing to dump $20 mil into Ubuntu, that translates into at least 10 years of work if there's about 10 payed devs (+overhead).\n\nI'd say that's a bit more bang for your buck than a short vacation.  (Although literally, the space vacation had more bang).\n\nWhen Gentoo came out, everyone and their dog was posting about how awesome it was.  Now we see the same with Ubuntu.  Every forum, related or not, has some Ubuntu advocates, and the name gets dropped a lot.  Gentoo excitement eventually died down (of course the community is still strong, but it's certainly not a mainstream distro).  \n\nI think that Ubuntu has a better chance at mainstream than Gentoo though.  They are concentrating on polish, and good integration, which everyone can appreciate.  Optimization on the other hand, is only important to a small subset of users.  Most people want something that works, and don't give a damn about whether they're squeezing every last cycle out of their processors. "
    author: "Leo Spalteholz"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-22
    body: "Gentoo isn't about getting the most speed, thats just a nice side effect. Gentoo is a meta-distrobution and therefore gives you all the control you want. I use gentoo but dont really care much about optimization (although i do do it because its easy).\n\nObviously some people use gentoo because of its optimization (thats what brought me to it, but i dont really care anymore). My favourate part of gentoo is the useflags so i can have packages exactly how i want them (my kde is without arts support, but i didn't have to work to get that, just set -arts in my useflags and emerge kde-meta).\n\nGentoo isn't for everybody and will probably never make it mainstream. \n\nI believe Ubuntu's advantage is that its like mandrake except based on debian instead of redhat (and the kde v gnome thing). In fact, mandrakesoft were considering changing from a redhat based distro to a debian based distro a few years ago, unfortunately they stuck with redhat."
    author: "Matthew Robinson"
  - subject: "Re: Final death of UserLinux"
    date: 2005-03-18
    body: "A basic assumption of UserLinux was that only one desktop (GNOME) shall be offered.\n\nBruce Perens totally missed the boat of freedesktop.org standardization.\n\nThe main point for ISVs is not the gui toolkit of the desktop environment but the interface they can depend on for deploying their solution.\n\n"
    author: "MortimeR"
  - subject: "Kubuntu = \"to humanity\""
    date: 2005-03-18
    body: "I dont know if its a coincidence but 'kubuntu' translates to 'to humanity' in my language (Bemba, central africa).\n\n\"Peleni KDE kubuntu\" would mean \"give KDE to humanity\"\n\nSo its quite appropriate I think."
    author: "genica"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2005-03-18
    body: "I thought \"ubuntu\" without 'k' means that?"
    author: "Anonymous"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2005-03-18
    body: "ubuntu == 'humanity'\nkubuntu == 'to humanity'\nku london == 'to london'\n\nThats just in my language. There are hundreds of these 'Bantu languages' in africa with all sorts of variations around this word."
    author: "genica"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2005-03-18
    body: "It's a relief that the Kubuntu people either did their homework or were just lucky with the meaning of their project name. I'm sure adding \"k\" to the beginning of some words in some languages can produce unfortunate derivations of the original words. ;-)"
    author: "The Badger"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2005-03-18
    body: "Yeah, I was doing some work earlier on a user node table (obviously unt). Decided to change it before the k got added. Just Kidding."
    author: "a.c."
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2005-03-18
    body: "Yes, my favourite is coolo ;-)"
    author: "MortimeR"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2007-05-13
    body: "Do you guys even bother doing any read-ups before posting your opinions on this site? \n\nThe base word Ubuntu was chosen specifically because of its meaning:\n\nUbuntu is an African word meaning 'Humanity to others', or 'I am what I am because of who we all are'. The Ubuntu distribution brings the spirit of Ubuntu to the software world. (http://www.ubuntu.com/products/WhatIsUbuntu)\n\nHere's a heads up, on the Canonical home page, http://www.canonical.com/, you will get this mission statement, \n\n\"Canonical's mission is to realise the potential of free software in the lives of individuals and organisations. In just three years, Canonical has been globally recognised as a leading provider of services to both individual and corporate open source software users.\n\nWe achieve our mission by:\n\n    * delivering Ubuntu, the world's best free software platform\n    * ensuring its availability to everyone\n    * supporting it with high quality professional service and engineering    \n      offerings\n    * facilitating the continued growth and development of the free software   \n      community.\"\n\num.., in case you don't know, Canonical Ltd. is the company that Mark Shuttleworth built with some of the money he got from selling Thawte to Verisign in 1999. Then he set up a foundation with his own funds, briefly what his vision is for the foundation is: \n\n\"Our goal is to invest in projects that offer unique and innovative solutions to educational challenges faced by the developing world. To concentrate our efforts we have identified broad themes (Education, Technology and Content) with specific focus areas that we believe are critical to youth development. We are aware that our strategy of supporting pilot projects is a 'high risk' strategy, as some of the projects we invest in will not succeed; however, we feel that all good ideas are worth trying, at least once. The successful projects that we pilot, those that have a positive measurable impact on the communities they are working with, will, we hope, become accepted and supported by other funders and the provincial departments of Education within South Africa and the world\" \n\nhttp://www.shuttleworthfoundation.org/\n\nFor all the skeptics out there, take the time to read from this site\n\nhttp://en.wikipedia.org/wiki/Mark_Shuttleworth\n\nand then decide what kind of guy Mark is. When he sold his first, self made company Thawte to Verisign, he gave each one of his employees One million dollars as a severance package, from the janitors to the developers. \n\nThis guy makes millions and invests in the future of the continent that he lives and believes in. He gives free software to anyone who wants it and fits the bill for the support and continued development of it. He does not sell it at discounted rates like Bill does. \n\nMark epitomizes the spirit of Ubuntu, that\u0092s the core of the subject. He puts his money where his mouth is, and gives something back to his community, for free. When was the last time that Bill did anything like that, ever? \n\n"
    author: "Macx"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2007-05-13
    body: "> Do you guys even bother doing any read-ups before posting your opinions on \n> this site? \n>\n> The base word Ubuntu was chosen specifically because of its meaning. \n\nDo you even bother reading the posting you are replying to? \nHe was referring to Kubuntu, not Ubuntu. \nHe didn't wonder if or even deny that \"Ubuntu\" was chosen because of that meaning. \n\nBut I'd say the fact that a meaningful term comes out when you apply KDE's habit of prefixing an existing word (Ubuntu in this case) with a \"K\" is a lucky coincidence, nothing more. \n\nOr do you really think that canonical (who originally didn't even plan to release a KDE-based version) chose a word from the Bantu language family so that the Bantu grammar allowed KDE to keep their naming habit?  You got to be kidding. \n\n\nIn any case he's perfectly entitled to asking that question, IMHO. \n\n"
    author: "cm"
  - subject: "Re: Kubuntu = \"to humanity\""
    date: 2007-05-13
    body: "> In any case he's perfectly entitled to asking that question, IMHO.\n\nMore precisely, he didn't ask a question, he just expressed that he didn't know. \n\n"
    author: "cm"
  - subject: "Kubuntu rocks"
    date: 2005-03-18
    body: "Hi !\n\nI'm a new user of Kubuntu - I have it installed since wednesday. You know what? I've just switched from SuSE 9.1 to Kubuntu in two days. Kubuntu is everything I need (the reduced system configuration I need to do can be done easily via debian or command line tools). And It's soo much faster than SuSE! And new KDE is nearly perfect :-).\n\nI'd like to tell you all that there is an ongoing development of a port of Yast to Debian (and you know, Ubuntu=Debian).\n\nSo for you the enthusiasted KDE users out there, if you've tried Ubuntu but feel more at home in KDE, install Kubuntu. And if you already have Ubuntu installed, do simple apt-get install kubuntu-desktop and see what happens ;-).\n\nThanks for your time,\n        Edulix."
    author: "Eduardo Robles Elvira"
  - subject: "Re: Kubuntu rocks"
    date: 2005-03-18
    body: "What happens? Not everything unless you also \"apt-get install kubuntu-default-settings\"."
    author: "Anonymous"
  - subject: "Re: Kubuntu rocks"
    date: 2006-08-08
    body: "I agree absolutely. I just installed Kubuntu on my old PC with a wireless network card, and here i am online already. I have Ubunutu running Gnome on my Laptop and wanted to see KDE in action. I think I might install this and run KDE on the Laptop also.\nUbuntu is the linux distribution without a doubt and like you I have come across from SUSE 10 and Mandrake "
    author: "thewizardalbany"
  - subject: "logo"
    date: 2005-03-18
    body: "I love your logo with the 3 konqui ;)\nlol"
    author: "JC"
  - subject: "Re: logo"
    date: 2005-03-18
    body: "Man, I love it too!\n\nGood luck to all of the people bringing us Kubuntu/Ubuntu!\nThanks so much."
    author: "CB"
  - subject: "Funny"
    date: 2005-03-18
    body: "On the Kubuntu homepage is a graphic of three \"nude\" dragons dancing.  I think in the real Ubuntu this used to be three nude humans, one guy and two gals.  =)"
    author: "ac"
  - subject: "Re: Funny"
    date: 2005-03-18
    body: "Basse said that he perhaps will create a Katie Blender model later."
    author: "Anonymous"
  - subject: "Re: Funny"
    date: 2005-03-18
    body: "> three \"nude\" dragons dancing\n\nwell... each Konqi is wearing (as usual) a neckerchief. which is more than most dragons i've seen wear. heck, Konqi even puts on a stocking cap when he sleeps (but wouldn't you if you slept on the moon?)\n\nso i wouldn't quite say they are nude.\n\none also has to admit, it's a better picture than, say .... three scantily clad KDE hackers dancing about in a circle. ;)\n\ni did like the original Ubuntu pic of the nekid people. so earthy and free! "
    author: "Aaron J. Seigo"
  - subject: "Re: Funny"
    date: 2005-09-09
    body: "ROFL!\n\nAnd yes, the Ubuntu artwork was/is great, just sad that they stopped making them :(\n\nI will never understand why so many people find nudity so offensive...not that I run around naked all the time, mind you :p"
    author: "Joergen Ramskov"
  - subject: "Kubuntu"
    date: 2005-03-18
    body: "Kubuntu is very nice. But the live CD is slow. Knoppix is a faster live-CD but its desktop is more messy."
    author: "Jakob"
  - subject: "Re: Kubuntu"
    date: 2005-03-19
    body: "IMO the (K)Ubuntu Live-CD is a bonus and not the primary target of development."
    author: "Anonymous"
  - subject: "Re: Kubuntu"
    date: 2005-03-19
    body: "As opposed to Knoppix where the Live-CD is the main target and the hard disk installation is the by-product.  AFAIK Knoppix CDs are optimized for efficient reading. Well, maybe Kubuntu could do the same but I don't think that this has a high prio, nor that it should. Just IMHO.\n\n"
    author: "cm"
  - subject: "Re: Kubuntu"
    date: 2006-01-09
    body: "The Live CD is there so you can test the distribution before you decide to switch, it is simply there to give you a flavour of (K)Ubuntu , and if you like it the install CD is there. In my experience, the install CD is very simple to use for beginners, and has some good advanced features for experts.  Everything is pretty much automated for the beginner, but configurable for the more advanced user.  All in all, this is my choice of Linux distribution."
    author: "Tez"
---
The <a href="http://www.kubuntu.org.uk">Kubuntu</a> team is pleased to announce the preview of their first release. Kubuntu is a new distribution using the solid base of Ubuntu and the stunning KDE 3.4 desktop, released yesterday.  This preview release includes both install CDs and bootable Live CDs for three architectures available <a href="http://cdimage.ubuntu.com/kubuntu/releases/hoary/preview/">for download</a> now.


<!--break-->
<p>This preview release includes the latest KDE 3.4 which brings many improvements including the excellent
KPDF reader, advanced groupware support in Kontact Personal
Information Management Suite and Kopete instant messenger plus HAL
support for removable media. We also include the feature packed amaroK
1.2 music and Kaffeine 1.5 video players.</p>

<p>We welcome feedback on this release which you can leave on <a href="https://www.ubuntulinux.org/wiki/KubuntuPreviewComments">this wiki
page</a>.</p>

