---
title: "KToon: 2D Animation Toolkit"
date:    2005-02-19
authors:
  - "ggonz\u00e1lez"
slug:    ktoon-2d-animation-toolkit
comments:
  - subject: "seems like a nice missing piece for linux"
    date: 2005-02-18
    body: "if the flash export is good enough, it could help alot with drawing designers (no pun intended) to the opensource desktop"
    author: "Maarten"
  - subject: "KToon?"
    date: 2005-02-18
    body: "Man.  I wonder why they didn't go with the obvious \"kartoon\""
    author: "standsolid"
  - subject: "Re: KToon?"
    date: 2005-02-19
    body: "Because all KDE applications must take the must unintuitve, tongue-twisting name possible."
    author: "Anonymous"
  - subject: "SVG export"
    date: 2005-02-18
    body: "is ktoon include a svg export ?"
    author: "GML"
  - subject: "Re: SVG export"
    date: 2005-02-19
    body: "no."
    author: "MaX"
  - subject: "Re: SVG export"
    date: 2005-02-19
    body: "no YET... i am the manager of the KToon development team and i want to say that one of the _main_ goals of KToon is to export open formats (as SVG).\n\nWe start dealing with the SWF format because the Ming library [http://ming.sourceforge.net/] exists and it just take some few minutes to make it works with KToon... but our priority is with SVG and open formats.\n\nCurrently we are studing the source code of QT applications like Karbon [http://www.koffice.org/karbon/] to include the SVG compatibility inside our project.\n\nHelp from developers with SVG experience is welcome!\n"
    author: "Gustavo Gonz\u00e1lez"
  - subject: "Problems with the source distribution"
    date: 2005-02-18
    body: "First, this looks very cool and it is great that you are using KDE/Qt technology for this...\n\nBut you need to alter your source distribution by taking out the generated Makefiles and including the top-level ktoon.pro which can be found in your svn repository.  Finally, alter you INSTALL to run 'qmake && make' or it won't work.\n\nOther than that, I can't wait to try this out."
    author: "manyoso"
  - subject: "Re: Problems with the source distribution"
    date: 2005-02-19
    body: "We expect to fix the \"ktoon.pro\" bug as fast as our website provider help us to fix another problem that we have with our internet connection.\n\nBy now, you can download the \"ktoon.pro\" file from:\n\nhttp://svn.berlios.de/viewcvs/*checkout*/ktoon/ktoon/ktoon.pro?rev=1\n\nWe offer excuses by this temporary problem...\n"
    author: "Gustavo Gonz\u00e1lez"
  - subject: "similar to ... ?"
    date: 2005-02-18
    body: "looking at the screenshots i see several widgets that scream \"re-use me to make a non-linear video editor!\" ;) \n\ni'm highly impressed by how the screenshots look. i would download it and give it a whirl, but i'm no artist. i'll have to get some of my artist friends to look at it though ... \n\nsweet stuff... mad props to Toonka Films!"
    author: "Aaron J. Seigo"
  - subject: "Re: similar to ... ?"
    date: 2005-02-19
    body: "To me the screenshots look like a bad mix of GIMP-esque tool windows fetish combined with the worst of Adobe's Windows MDI. Not like a KDE app at all, and I hope that's a subject to change sometime soon."
    author: "ac"
  - subject: "Other contenders"
    date: 2005-02-19
    body: "Two other packages in the same category (but not free...) are:\n\n\tMoho  (http://www.lostmarble.com)\n\t  \tVector oriented, SWF export, bones,...\n\t  \tWindows,Mac & Linux version available\n\t  \t99$ and you get all platforms\n\t  \tslightly limited demo available\n.\n\tPlasticAnimationPaper (http://plasticanimationpaper.dk/)\n\t \t virtual lightbox ( 'traditional' animation package )\n\t \t Windows & Linux\n\t \t Expensive (esp in relation to Moho)\n\t \t Limited demo available"
    author: "UglyMike"
  - subject: "Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "This is an extremely nice addition to the multimedia linux suite. Today, we have:\n\n* Blender3d for 3d work\n* Rosegarden with lilypond (beautiful music production) for music composition\n* Audacity for sound recording and editing\n* Ogg and Theora formats for sound and video\n* Fluendo's Flumotion media server which is GPL for Ogg and Theora streaming\n* Bittorent for sharing large media files \n* Gimp (interface is improving) for pixel based graphics\n* Inkscape (extremely nice for \"fake\" artists like me) for vector based graphics\n* Scribus along with improvements in KPDF for professional desktop publishing\n* Kile for Tex documents (still evolving)\n* Mediawiki (for collaborative multimedia content production - counterintutitive perhaps, but very useful in my company)\n\nAnd now specialized niches like cartoon animatation are being filled in. Is there anything missing? I am admittedly not a professional, but I find most of these applications (with the exception of the gimp and blender3d) quite intuitive to use and feature complete for my needs. There was recently an article about making a multimedia office suite and improving interoperability between the different open source applications. Given how expensive commercial counterparts are, such an office suite (especially if integrated with OpenOffice or KOffice) could draw a significant number of users to the Linux desktop. \n\nOut of curiousity, are there any KOffice developers on the forum? Does anyone know if there are plans in the future to integrate the above suite into KOffice or embed them in the OASIS format? Such integration would put KOffice far ahead of its competitors and radically increase the attractiveness of KDE. It's entirely possible too that the maintainers of each of the above projects would themselves work to support the integration given the benefits that they would reap from the network effects of application integration. \n\nIt might evolve along this path slowly on its own anyway, but a little effort towards interoperability might make both KOffice and Open Office (with 15 integrated applications and one multimedia OASIS format that could embed Ogg, Lilypond, Theora, SVG, Tex, etc.) significantly superior to Microsoft Office (with its five traditional applications). The office application space has been static for so long ... "
    author: "N. Geisinger"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "GIMP's interface is improving?  Do you mean the 2.x vs. 1.x changes, or is someone working on MDI for GIMP now?  Personally, I'm holding out for Krita :D"
    author: "Jel"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "> Is there anything missing?\n\n<broken-record>non-linear video editing</broken-record>"
    author: "Aaron J. Seigo"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "What does the \"non-linear\" part mean, specifically?"
    author: "AC"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "http://en.wikipedia.org/wiki/Non-linear_editing_system"
    author: "Spy Hunter"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "In tape to tape editing, or linear editing, you edit as the first title would lead you to believe. This allows only the simplest of transitions, live, as you are editing. Non linear editing on the other hand involves digitizing the video first and messing around with it on a computer. Thus, you can add transitions, overlays, and audio all after the fact. So a non linear editor for kde would be alot like avid, final cut pro, or premiere."
    author: "ac"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "Cinelerra - http://heroinewarrior.com/cinelerra.php3\nKino - http://kino.schirmacher.de/\nAvidemux - http://fixounet.free.fr/avidemux/"
    author: "Ted"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "> <broken-record>non-linear video editing</broken-record>\n\nKino?\nhttp://kino.schirmacher.de/article/static/2"
    author: "ac"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-20
    body: "> <broken-record>non-linear video editing</broken-record>\n\nmainactor\n"
    author: "anonymous"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "The color pallette looks very nice, maybe make a KPart of it? =)\nhttp://ktoon.toonka.com/images/screenshots/colorpalette.png\n\nAlso, maybe others enhanced widgets/algo could be reused.\n\nLooks very nice tough. =)\n\nThanks to ToonKa, ParqueSoft et cie.\n\nFred."
    author: "fprog26"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "Yeah, that palette is wonderful ! it would be a very good idea to include it in krita for example. And maybe it could be a base to improove the actual palette dialog too :-)."
    author: "Eduardo Robles ELvira"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-19
    body: "\"Out of curiousity, are there any KOffice developers on the forum?\"\n\nI'm a pretty regular reader of the Dot, yes. One has to do something while waiting for KOffice to compile... There are no plans to really integrate any of these applications in KOffice by regular KOffice hackers: they have too much to do anyway. On the other hand, some apps would make a really neat fit, especially Scribus, but that's up to the Scribus hackers who, no doubt, have quite enough to do already.\n\nThe situation with OASIS is a bit more complex. OASIS currently defines file formats for a very limited set of applications. KOffice applications like Kivio and Krita don't use an OASIS file format because such a beast does not exist. I've toyed for a moment with trying to write a spec, but that would keep me from hacking on Krita, and my first concern now is to get the !@#$% thing stable again, and released. \n\nBesides, I don't know enough about graphics apps to know what needs to go into a pixel-image file format. I mean, two months ago, before I started to integrate color management in Krita, I didn't know about .icm profiles..."
    author: "Boudewijn Rempt"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-20
    body: "Thanks for answering my questions! And for working on Krita :) ...\n\nIf - as a multimedia app writer - you've toyed with writing an Open Office spec without prompting, then that is extremely suggestive. By extension, it suggests that other multimedia application developers (e.g., gstreamer, scribus, or the tex crowd) might also be looking at the integration question and the OASIS format even if there is little public talk about it. It makes sense that this would be the case since integrating your application into an office suite would significantly increases its user base.\n\nPerhaps, then, there's no need to organize an integration effort. The invisible hand of open source is already self organizing its own solution with no conscious cooperation from its participants. \n "
    author: "N. Geisinger"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-21
    body: "> Besides, I don't know enough about graphics apps to know what needs to go into a pixel-image file format.\n\nopenexr.com has a good description of their (floating point) pixel format. Maybe some input for you!?"
    author: "Carlo"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2005-02-21
    body: "I have looked at openEXR (and we'll probably support the file format one of these days), but it isn't enough: we need to have at least support for\n\n* layers\n* icm profiles\n* selections (easy, that's just another layer)\n* meta information like color model\n\nKrita's current file format supports all of that, so the pressure to invent something new isn't that strong. On the other hand, Krita's file format is just as ad hoc as .xcf and just as closely tied to the application."
    author: "Boudewijn Rempt"
  - subject: "Re: Question for KOffice or Open Office developers"
    date: 2006-02-04
    body: "Wings3D is a great program also. Real easy to use."
    author: "robteed"
  - subject: "A fine free software contribution."
    date: 2005-02-19
    body: "Thanks very much, this is a great contribution to the free software community.  I look forward to trying this out."
    author: "J.B. Nicholson-Owens"
  - subject: "Documentation?"
    date: 2005-02-19
    body: "Did anyone found documentation? Download area for docs is empty :("
    author: "m."
  - subject: "Re: Documentation?"
    date: 2005-02-19
    body: "Hi, i am sorry about the doc part of the project. We have a little problem with the wiki system, but we expect to fix it soon!\nThere will be a Developers section and a Users section.\n\nThank you for your pacience."
    author: "Gustavo Gonz\u00e1lez"
  - subject: "That's odd"
    date: 2005-02-19
    body: "two days ago I checked in the source code of Kast into kdeplayground multimiedia. It's supposed to do nearly the same for SVG and maybe later also flash.\n\nIt's hard to decide what to do now. On the one hand Kast is still far away from the functionality KToon has to offer (right now only the renderer works good enough to show it)... on the other hand Kast is supposed to be my diploma thesis, therefore I can't just stop, throw away all the work and start contributing to the more advanced project...\n\nI guess I'll have a thorough look at this piece of software today."
    author: "Hans Oischinger"
  - subject: "Re: That's odd"
    date: 2005-02-19
    body: "please keep up the good work on kast, we need a good svg tool like this so that we don't have to depend on that flash proprietary plugin anymore :)"
    author: "Pat"
  - subject: "Re: That's odd"
    date: 2005-02-19
    body: "Even if the Kast project keep growing... i want to say as director of the project that KToon will include the SVG support in future releases. We hope it happen soon as we can."
    author: "Gustavo Gonz\u00e1lez"
  - subject: "Re: That's odd"
    date: 2005-02-19
    body: "Do you have a Homepage or/and Screenshots from Kast?"
    author: "MaX"
  - subject: "Re: That's odd"
    date: 2005-02-19
    body: "Not yet, but there is not much to see right now.\nI'll put something up next month when the project has matured a bit."
    author: "Hans Oischinger"
  - subject: "Re: That's odd"
    date: 2005-02-19
    body: "Matured like in adding the missing files subpath.* in the elements directory for a start? ;)\n\nI just co'ed kast to have a look at it but could not compile it. What did you do to integrate kcanvas, do a symlink in the main dir to it or extend kcanvas Makefile.am's to install all needed headers?\n\nThanks for helping out :)\n"
    author: "Friedrich"
  - subject: "Re: That's odd"
    date: 2005-02-19
    body: "Matured in just that way :)\nPlease grab the source again. the Makefile.am is updated now.\n\nKCanvas headers are not all installed by make install therefore symlinking would be the easiest thing for now."
    author: "Hans Oischinger"
  - subject: "Any translation volunteers?"
    date: 2005-02-20
    body: "I saw that the application was translated from English to Spanish...\n\nAre there any translation volunteers to port this to other languages?\n\nHere's the XML file in question:\nhttp://svn.berlios.de/viewcvs/ktoon/ktoon/src/trans/ktoon_es.ts?rev=2&view=markup\n\nHere's the BerliOS project:\nhttp://developer.berlios.de/projects/ktoon/\n\nContact info:\nhttp://developer.berlios.de/project/memberlist.php?group_id=2960\n\nThe project use SubVersion. \n\nSincerely yours,\n\nFred."
    author: "fprog26"
  - subject: "Re: Any translation volunteers?"
    date: 2005-02-23
    body: "Translation volunteers can find useful this little \"howto\" about it:\n\nhttp://ktoon.toonka.com/documentation/index.php?title=KToon_Internationalization_%28Adding_support_to_foreign_languages%29"
    author: "Gustavo Gonz\u00e1lez"
  - subject: "KTOON Can be used to Create KDE presentation!"
    date: 2005-02-21
    body: "I wish that KToon be used to create dynamic KDE Presentations and Introcution, like Windows XP's flash introduction to XP's features."
    author: "fast"
  - subject: "Re: KTOON Can be used to Create KDE presentation!"
    date: 2005-02-21
    body: "It should be possible if some people of the KOffice team wants to help us to implement that feature. KToon is GPL so everybody is invited :)"
    author: "Gustavo Gonz\u00e1lez"
  - subject: "Error"
    date: 2005-03-06
    body: "I unzipped Ktoon and when I run it I get an error:\nUnable to resolve GL/GLX symbols - please check your GL library installation.\n\nBut if I run glxinfo I get an info:\n\n[root@localhost ktoon]# glxinfo\nname of display: :0.0\ndisplay: :0  screen: 0\ndirect rendering: No\nserver glx vendor string: SGI\nserver glx version string: 1.2\nserver glx extensions:\n    GLX_ARB_multisample, GLX_EXT_visual_info, GLX_EXT_visual_rating,\n    GLX_EXT_import_context, GLX_SGI_make_current_read, GLX_SGIS_multisample\nclient glx vendor string: SGI\nclient glx version string: 1.2\nclient glx extensions:\n    GLX_ARB_get_proc_address, GLX_ARB_multisample, GLX_EXT_import_context,\n    GLX_EXT_visual_info, GLX_EXT_visual_rating, GLX_MESA_allocate_memory,\n    GLX_MESA_swap_control, GLX_MESA_swap_frame_usage, GLX_OML_swap_method,\n    GLX_OML_sync_control, GLX_SGI_make_current_read, GLX_SGI_swap_control,\n    GLX_SGI_video_sync, GLX_SGIS_multisample, GLX_SGIX_fbconfig,\n    GLX_SGIX_visual_select_group\nGLX extensions:\n    GLX_ARB_get_proc_address, GLX_EXT_import_context, GLX_EXT_visual_info,\n    GLX_EXT_visual_rating\nOpenGL vendor string: Mesa project: www.mesa3d.org\nOpenGL renderer string: Mesa GLX Indirect\nOpenGL version string: 1.2 (1.4 Mesa 5.0.2)\nOpenGL extensions:\n    GL_ARB_depth_texture, GL_ARB_imaging, GL_ARB_multitexture,\n    GL_ARB_point_parameters, GL_ARB_shadow, GL_ARB_shadow_ambient,\n    GL_ARB_texture_border_clamp, GL_ARB_texture_cube_map,\n    GL_ARB_texture_env_add, GL_ARB_texture_env_combine,\n    GL_ARB_texture_env_crossbar, GL_ARB_texture_env_dot3,\n    GL_ARB_texture_mirrored_repeat, GL_ARB_transpose_matrix,\n    GL_ARB_window_pos, GL_EXT_abgr, GL_EXT_bgra, GL_EXT_blend_color,\n    GL_EXT_blend_func_separate, GL_EXT_blend_logic_op, GL_EXT_blend_minmax,\n    GL_EXT_blend_subtract, GL_EXT_clip_volume_hint, GL_EXT_copy_texture,\n    GL_EXT_draw_range_elements, GL_EXT_fog_coord, GL_EXT_multi_draw_arrays,\n    GL_EXT_packed_pixels, GL_EXT_polygon_offset, GL_EXT_rescale_normal,\n    GL_EXT_secondary_color, GL_EXT_separate_specular_color,\n    GL_EXT_shadow_funcs, GL_EXT_stencil_two_side, GL_EXT_stencil_wrap,\n    GL_EXT_subtexture, GL_EXT_texture, GL_EXT_texture3D,\n    GL_EXT_texture_edge_clamp, GL_EXT_texture_env_add,\n    GL_EXT_texture_env_combine, GL_EXT_texture_env_dot3,\n    GL_EXT_texture_lod_bias, GL_EXT_texture_object, GL_EXT_texture_rectangle,\n    GL_EXT_vertex_array, GL_APPLE_packed_pixels, GL_ATI_texture_mirror_once,\n    GL_ATI_texture_env_combine3, GL_IBM_texture_mirrored_repeat,\n    GL_MESA_pack_invert, GL_MESA_ycbcr_texture, GL_NV_blend_square,\n    GL_NV_point_sprite, GL_NV_texgen_reflection, GL_NV_texture_rectangle,\n    GL_SGIS_generate_mipmap, GL_SGIS_texture_border_clamp,\n    GL_SGIS_texture_edge_clamp, GL_SGIS_texture_lod, GL_SGIX_depth_texture,\n    GL_SGIX_shadow, GL_SGIX_shadow_ambient\nglu version: 1.3\nglu extensions:\n    GLU_EXT_nurbs_tessellator, GLU_EXT_object_space_tess\n\n   visual  x  bf lv rg d st colorbuffer ax dp st accumbuffer  ms  cav\n id dep cl sp sz l  ci b ro  r  g  b  a bf th cl  r  g  b  a ns b eat\n----------------------------------------------------------------------\n0x22 24 tc  1 24  0 r  .  .  8  8  8  0  0  0  0  0  0  0  0  0 0 None\n0x23 24 tc  1 24  0 r  y  .  8  8  8  0  0  0  0  0  0  0  0  0 0 None\n0x24 24 tc  1 24  0 r  .  .  8  8  8  0  0 16  0  0  0  0  0  0 0 None\n0x25 24 tc  1 24  0 r  y  .  8  8  8  0  0 16  0  0  0  0  0  0 0 None\n0x26 24 tc  1 24  0 r  .  .  8  8  8  0  0 32  0  0  0  0  0  0 0 None\n0x27 24 tc  1 24  0 r  y  .  8  8  8  0  0 32  0  0  0  0  0  0 0 None\n0x28 24 tc  1 24  0 r  .  .  8  8  8  0  0 24  8  0  0  0  0  0 0 None\n0x29 24 tc  1 24  0 r  y  .  8  8  8  0  0 24  8  0  0  0  0  0 0 None\n0x2a 24 tc  1 24  0 r  .  .  8  8  8  0  0  0  0 16 16 16 16  0 0 None\n0x2b 24 tc  1 24  0 r  y  .  8  8  8  0  0  0  0 16 16 16 16  0 0 None\n0x2c 24 tc  1 24  0 r  .  .  8  8  8  0  0 16  0 16 16 16 16  0 0 None\n0x2d 24 tc  1 24  0 r  y  .  8  8  8  0  0 16  0 16 16 16 16  0 0 None\n0x2e 24 tc  1 24  0 r  .  .  8  8  8  0  0 32  0 16 16 16 16  0 0 None\n0x2f 24 tc  1 24  0 r  y  .  8  8  8  0  0 32  0 16 16 16 16  0 0 None\n0x30 24 tc  1 24  0 r  .  .  8  8  8  0  0 24  8 16 16 16 16  0 0 None\n0x31 24 tc  1 24  0 r  y  .  8  8  8  0  0 24  8 16 16 16 16  0 0 None\n\n"
    author: "MaBu"
  - subject: "V 0.8 - I can't export"
    date: 2006-05-23
    body: "I have version 0.8 installed and I can't export.\n\nWhen I try, it asks me to choose the format before export. The problem is that I can't see where to choose a format. (Putting the \"right\" extension on the filename does not work for me.)\n\nHas anyone solved this in V 0.8?\n\nall the best,\n\ndrew"
    author: "drew Roberts"
---
<a href="http://ktoon.toonka.com">KToon</a>, is a new 2D animation toolkit created by <a href="http://www.toonka.com">Toonka Films</a> and now made available as a free GPL'ed option to the 2D animation industry. KToon has been developed using <a href="http://www.opengl.org">OpenGL</a> from the <a href="http://www.trolltech.com/products/qt/">Qt</a> library and follows the same interface style of commercial products such as Macromedia Flash (TM) and ToonBoom (TM). Currently KToon is still in beta and only has 2 modules (Illustration and Animation), but it already allows one to export animations to the SWF format. <a href="http://ktoon.toonka.com/images/screenshots/">Screenshots</a> are available. Developers and testers are welcome!

<!--break-->
