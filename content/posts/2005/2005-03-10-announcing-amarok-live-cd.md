---
title: "Announcing amaroK Live CD"
date:    2005-03-10
authors:
  - "GMeyer"
slug:    announcing-amarok-live-cd
comments:
  - subject: "Tried it and amaroK crashes on my computer"
    date: 2005-03-10
    body: "Gave this a try, but amaroK closes itself whenever I try to open it - JuK without any error messages... \n\nI wonder what's wrong with amaroK? My HW is a Intel P4/2400MHz on a Intel i845 motherboard with integrated audio (ICH4) and a Ati Radeon 9000. JuK works perfectly from the same CD. Some nice tracks there. \n\n++ R"
    author: "Reiska"
  - subject: "Re: Tried it and amaroK crashes on my computer"
    date: 2005-03-10
    body: "Well, it is some problem with xine-engine perhaps.  Juk is using arts, so maybe you could try starting amarok with arts engine.  at console prompt, try 'amarok --engine arts'  or maybe try gstreamer with 'amarok --engine gst'\n\namarok in this LiveCD is enabled to use all three engines, but xine is set as default."
    author: "GMeyer"
  - subject: "Re: Tried it and amaroK crashes on my computer"
    date: 2005-03-11
    body: "try \n\n$ rm -rf .kde/share/apps/amarok*\n$ rm .kde/share/config/amarok*\n\nI had installed amaroK 1.1.x and 1.2 crashed on start. The above procedure fixed it.\n\n"
    author: "Norberto"
  - subject: "Re: Tried it and amaroK crashes on my computer"
    date: 2005-04-26
    body: "Hi\n\nI have the same issue on Slack 10.1 and Amarok will not run (v1.22 and v1\nInitially it complains about gstplugins and gstreamer which I installed.\n(why the packagers don't list or check dependencies when they make dependent apps - I don't know)\nAfter removing the directories from your post - are you reinstalling anything? Or have the paths changed between versions?\n"
    author: "Tim vP"
  - subject: "Solved my problem  Re: amaroK crashes [...]"
    date: 2007-09-29
    body: "Mandriva 2007 Spring.  Got same issue after installing some extra packages (like gpm and bash-completion).  Amarok was crashing my X-server.\nI thought about deleting  ~/.kde/share/apps/amarok*\nBut missed the  ~/.kde/share/config/amarok*\n\n  => problem solved now.  Thanks"
    author: "S\u00e9bastien Lacroix"
  - subject: "The wolf icon"
    date: 2005-03-10
    body: "Nice to see the new icon \"in action\"! It sure looks better than the previous one."
    author: "ac"
  - subject: "Re: The wolf icon"
    date: 2005-03-10
    body: "I'm a little confused.  Are you saying you are seeing the wolf and you like it, or you like that it is gone?"
    author: "GMeyer"
  - subject: "Announcing Kedit Live CD"
    date: 2005-03-10
    body: "You can find the ISO DVD-Image (3GB) here.\n.\n.\n.\n.\n.\nDo we really need a live cd for each little program (like amarok)?"
    author: "Hans"
  - subject: "Re: Announcing Kedit Live CD"
    date: 2005-03-10
    body: "well, for killer apps like amaroK, sure. "
    author: "Ian Monroe"
  - subject: "Re: Announcing Kedit Live CD"
    date: 2005-03-11
    body: "> Do we really need a live cd for each little program (like amarok)?\n\n\nDoes it take something away from you that this LiveCD exists?\nWhy do you care, if it doesn't interest you?\n"
    author: "ac"
  - subject: "Re: Announcing Kedit Live CD"
    date: 2005-03-11
    body: "The cool thing about amaroK Live is that you can remaster it easily with a selection of your own music, and take it to a party. DJ on a CD :)\n"
    author: "Mark Kretschmann"
  - subject: "iTunes"
    date: 2005-03-10
    body: "Why it looks like iTunes? Is it just a skin or it looks by default like iTunes?"
    author: "Anton Velev"
  - subject: "Re: iTunes"
    date: 2005-03-10
    body: "Um, no it doesn't."
    author: "Ian Monroe"
  - subject: "Re: iTunes"
    date: 2005-03-10
    body: "The skin in the screenshots is baghira. Baghira mimics the Apple brushed metal theme."
    author: "Max Howell"
---
The <a href="http://amarok.kde.org/">amaroK</a> development team has teamed up with the Creative Commons sponsored <a href="http://creativecommons.org/wired/">Wired CD</a>, published last year by Wired Magazine, to create a really cool Live CD called <a href="http://amarok.kde.org/wiki/index.php/AmaroK_Live">amaroK Live</a>. It is an interesting extension of the project started by Wired using the Creative Commons licensing technique, which includes many of the principles embodied in free software.  




<!--break-->
<p>The KDE-centric <a href="http://www.pclinuxonline.com/pclos/index.html">PCLinuxOS LiveCD distro</a> was used as a base to create this really cool Live CD.  amaroK Live is not so much a Live CD distro as it is a demonstration of a really cool music player.  It is a stripped down Live CD (only 289MB including the music) with a fully functional amaroK music player bundled with the tracks commissioned last year by Wired Magazine, which are distributed under the Creative Commons Sampling Licenses. It includes - among other major artists - tracks by the Beastie Boys and David Byrne.</p>

<p>The tracks can all be legally shared for non-commercial purposes (some also allow commercial use) and can even be used to make derivative works using sampling techniques.</p>

<p>You can find <a href="http://shots.osdir.com/slideshows/slideshow.php?release=248&slide=1">screenshots</a> of the recently released amaroK 1.2 on OSDir.</p>

