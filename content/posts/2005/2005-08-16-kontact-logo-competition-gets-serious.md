---
title: "Kontact Logo Competition Gets Serious"
date:    2005-08-16
authors:
  - "tzander"
slug:    kontact-logo-competition-gets-serious
comments:
  - subject: "wacom"
    date: 2005-08-16
    body: "Does KDE suppiort tablet PCs?"
    author: "aile"
  - subject: "Re: wacom"
    date: 2005-08-16
    body: "The mentioned paint application \"Krita\" is a KDE application and Krita supports tablets, including its pressure sensitivity, just fine."
    author: "Thomas Zander"
  - subject: "Re: wacom"
    date: 2005-08-16
    body: "Well, there's a difference between a tablet pc and a tablet; but if X11 supports the tablet pc, Qt will support it, and then Krita will be able to use it.\n\nNote for Debian users: Debian's Qt packages do not include tablet support in the default Qt packages. I don't know why, but Jonathan Riddell has fixed this for Kubuntu.\n\nAs an aside: adding tablet support was why I started working on Krita. I had bought a Wacom tablet, and discovered that there was no app under Linux that worked really well with my tablet..."
    author: "Boudewijn Rempt"
  - subject: "Re: wacom"
    date: 2005-08-16
    body: "Microsoft's Tablet PC was no success but the software looked intresting. I wonder whether tablet OCR is easy to be supported."
    author: "Bert"
  - subject: "Re: wacom"
    date: 2005-08-16
    body: "That's a tablet as in mouse replacing input tool for artists, not tablet PC."
    author: "Jim"
  - subject: "Re: tablet PC"
    date: 2005-08-16
    body: "oops; I did not expect you to go off topic from wacom tablets to tablet PCs.\nI actually have a tablet PC and the tablet is fully supported since the touchscreen is just another input driver for X11 and your software just thinks its being controlled by a mouse.  So; not support for KDE is needed, only for X11.\n\nSee also the Jul-2005 edition of the German magazine LinuxUser where this is discussed in."
    author: "Thomas Zander"
  - subject: "Re: tablet PC"
    date: 2005-08-16
    body: "In that case I guess there's no pressure sensitivity, tilt or twirl, right?"
    author: "Boudewijn Rempt"
  - subject: "Re: tablet PC"
    date: 2005-08-16
    body: "There are tablet PCs from Motion Computing with pressure sensitivity, tilt and everything, taylored at artists. I think the tablet they use is made by Wacom (a tablet PC/ Wacom Cintiq combo):\n\nhttp://www.bauhaussoftware.com/products_nomad_LP.php\n"
    author: "Willie Sippel"
---
<a href="http://kontact.org/">Kontact</a> started a <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,352.0">competition</a> for a new logo earlier this month; where the winning logo will become the official logo at the next release.  Now we found a sponsor to back the competition to give a Wacom tablet to the artist that created the winning logo.









<!--break-->
<p>Kontact is the application that can bring order to your life by managing your contact addresses, emails and appointments in a mature and professional manner.  In the upcoming release of Kontact, together with <a href="http://developer.kde.org/development-versions/kde-3.5-release-plan.html">KDE 3.5</a>, there have been various changes we are very excited about, usability reviews have been done and a lot of deployments have resulted in a smoother operation when backed with the groupware backends like <a href="http://kolab.org">Kolab</a>.</p>

<p>The <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> will sponsor the contest for a new logo by providing a prize to be given to the winning entry.  The prize will be a Wacom Graphire A5 tablet, which works great with paint applications like <a href="http://www.koffice.org/krita/">Krita</a>.</p>

<p>The new logo will be used in icons for application and menu entries and documentation.  Next to that, a new website and advertising or t-shirts will use the logo so be prepared to let lots of people see your work!</p>

<p>More info at the <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,352.0">kde-artist website</a>.</p>






