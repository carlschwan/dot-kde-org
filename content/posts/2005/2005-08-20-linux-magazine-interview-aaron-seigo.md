---
title: "Linux Magazine: Interview with Aaron Seigo"
date:    2005-08-20
authors:
  - "binner"
slug:    linux-magazine-interview-aaron-seigo
comments:
  - subject: "Nice"
    date: 2005-08-20
    body: "Even though Aaron don't think so himself, I found it quite interesting.\n\nKeep up the good work and have a nice weekend :)"
    author: "Joergen Ramskov"
  - subject: "nice interview"
    date: 2005-08-20
    body: "this was a really good read"
    author: "Tom"
  - subject: "yes"
    date: 2005-08-20
    body: "good read :D\n\nand thanx for the comforting remark: \"You're still going to be able to put icons on the desktop if that's what you want to do.\"\n\nnot that i'll do it,a s i now don't have any icons on my desktop, but its good to see plasma will EXTEND what we can do, not replace..."
    author: "superstoned"
  - subject: "Re: yes"
    date: 2005-08-20
    body: "What I always wanted to do is:\nPutting icons in containers on the desktop.\nYou can say what you want, but I still find putting icons for apps\nand documents I frequently use directly on the desktop the best\nsolution. In the real world you dont stack away your phone in a\ndrawer. To improve this experience it would be great if you could\n\"draw\" a box on your desktop and put all your icons in it.\nIt's like a KNotes-window which is always on your desktop, below\neverything else. Now, you can move around groups of icons and roll-up\nthose boxes if you dont need them right now.\n"
    author: "Martin"
  - subject: "Re: yes"
    date: 2005-08-20
    body: "well, at least with containers it is easier to use. i don't use icons on my desktop, as i use the 'run command' applet, and some icons on my kicker panels.\n\ni only have some superkaramba themes on the desktop, and i would love to see more and more usefull ones (like one showing my last used documents, bookmarks, a quick-search like beagle, etc etc)."
    author: "superstoned"
  - subject: "Re: yes"
    date: 2005-08-21
    body: "You mean quicksearch like Kat :o)"
    author: "rinse"
  - subject: "Re: yes"
    date: 2005-08-21
    body: "yeah, but beagle exists longer, is better known, and kat just doesnt work here...\n\nyou monkeyboy :D"
    author: "superstoned"
  - subject: "Great read..."
    date: 2005-08-22
    body: "from a great guy working on a great desktop using a great toolkit.\nAnd so cuddly too :-p"
    author: "illogic-al"
  - subject: "Re: Great read..."
    date: 2005-08-23
    body: "agreed! ;-P"
    author: "Tm_T"
  - subject: "Bindings for mono?"
    date: 2005-08-23
    body: "Aaron mentioned that python, ruby and java bindings will be shipped with KDE by default. Lately i've been hearing a lot of the GNOME guys talk about mono bindings (mostly because of novell and beagle) but i haven't heard the KDE guys say anything about mono. As KDE fans, do we care about mono? Does anyone know if you will be able to use mono with kde? Will someone make bindings and will they ship be default?"
    author: "Laszlo Pandy"
  - subject: "Re: Bindings for mono?"
    date: 2005-08-24
    body: "As long as there is noone (interested in) working on Mono bindings there's naturally no chance such bindings could be shipped with KDE. As of now nobody seems to care."
    author: "ac"
  - subject: "Re: Bindings for mono?"
    date: 2005-08-24
    body: "There have been some work done, and at one point was two(three?) different sets of bindings. Mostly for Qt I think. It looks like no one are actively working on this at the moment. The main reason are the lack of interrest, both for the bindings and Mono. There are lot of talk and hype surrounding Mono, but as a language C# is not that exiting. And if you look at Java, which as a language have much the same pros and cons as C#, and you have many more times the possible experienced developers. Still the KDE/Qt Java bindings see very little use. The main reason for this are that both Java and C# don't bring any real big benefits for the developer over C++ with Qt. Having to deal with the added complexity of the bindings for very little gain explains much of this. The reason the Gnome guys are talking about it are that they get much more benefit form it, as opposed to their native solution of C and GTK+. \n\nOn the other hand more high level languages like Ruby and Python makes more sense, and they are clearly much more popular. When using Python and PyQt/PyKDE you get like an average of 40% less code than C++, you have noticeable gain in using it.\n\n"
    author: "Morty"
  - subject: "Re: Bindings for mono?"
    date: 2005-08-25
    body: "\"Mostly for Qt I think. It looks like no one are actively working on this at the moment. The main reason are the lack of interrest, both for the bindings and Mono. There are lot of talk and hype surrounding Mono, but as a language C# is not that exiting.\"\n\nNo it isn't. For KDE, using a really good C++ toolkit in Qt, Mono, C# or any of the surrounding software frameworks simply aren't that interesting. That is testament to KDE's usage of Qt I think.\n\n\"Still the KDE/Qt Java bindings see very little use.\"\n\nTo be honest, I'd rather just see a Java application running on KDE with some integration - like Open Office. I don't see the point of binding everything to KDE and Qt."
    author: "Segedunum"
---
<a href="http://www.linux-mag.com/">Linux Mag</a> has <a href="http://www.linux-mag.com/index.php?option=com_content&task=view&id=2051&Itemid=2107">interviewed KDE developer Aaron Seigo</a> about his <a href="http://dot.kde.org/1123198410/">sponsorship by Trolltech</a>, how he got involved with KDE and upcoming KDE 4 goodies like <a href="http://plasma.kde.org/">Plasma</a> Aaron is working on, usability and more.


<!--break-->

