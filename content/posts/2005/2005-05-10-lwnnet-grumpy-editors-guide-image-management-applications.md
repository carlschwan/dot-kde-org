---
title: "LWN.net: The Grumpy Editor's Guide to Image Management Applications"
date:    2005-05-10
authors:
  - "binner"
slug:    lwnnet-grumpy-editors-guide-image-management-applications
comments:
  - subject: "Cheers for digiKam"
    date: 2005-05-10
    body: "I use digiKam, and I like it. Although it's UI would benefit from some \"lifting\", it's by far the most useful and powerful application of this type for Unix (as I know). The plugins make it sooo useful. It only tend's to move into a direction I personally do not like too much. It is extended by more and more plugins to manipulate the images. I'd prefer if those functionality would be left for gimp/krita. Instead easy of use, and image-handling should be more in the focus. Let's see what versions 7.3 and 8 will bring.\nAnyway - great work of the digiKam development team!\nAnd maybe I should have a look for KimDaBa again...\n"
    author: "Birdy"
  - subject: "Re: Cheers for digiKam"
    date: 2005-05-10
    body: "The way I see it, digikam's focus is plugins that are useful for photographers. I don't think you have to worry about them introducing selection tools, gradient tools, and silly painting tools.\n\nHowever, what you will see are things like lens distortion correction, grain reduction, white balance alteration, advanced b&w conversion, red eye removal etc. Things that are directly relevant to photographers and not for people who want to do massive screwing around with photos a la photoshop/gimp.\n\nAt least that's how I hope digikam will go.\n\nThere are actually several filters in digikam that the gimp doesn't have plugins for yet.\n\nOfftopic, but something I would like to see that has been talked about for a while is a timeline bar."
    author: "Robert"
  - subject: "Re: Cheers for digiKam"
    date: 2005-05-10
    body: "I completely agree:\n- Timeline bar would be useful\n- Digikam is more focus on photography\n \nI would add:\n- Krita is about picturing\n- Between Krita and Digikam there is a huge potential for reuse. And that's good."
    author: "veton"
  - subject: "Re: Cheers for digiKam"
    date: 2005-05-11
    body: "I would vote for advanced selecting tools. Maybe not as part of the image editor but it would be useful for plugins like DRI (dynamic range increase) et.al. To have an internal powerful base with selection channels and alpha channels is a good thing.\nI miss a 16 bit/channel framework and bette raw-file handling\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Article is a shame"
    date: 2005-05-10
    body: "I don't know all the apps, but I know a little of Digikam, as that is what my wife uses (she is not computer literate).\nAnd it is evident from this article, that the author took way too little time reviewing at least Digikam (and I guess most other programs too).\nDigikam does nearly (one exception is erasing in-law) all of the things that the author complains it does not.\nThat's nearly as if all the plugins (kipi-plugins and digikamplugins) where removed when he reviewed digikam.\nFor examples, he complains about rotating images being slow, I don't know, since at home the option for auto-rotating based on EXIF is activated.\nOr the rotation not being lossless (It IS lossless if you have kipi-plugins).\nOr printing capability. I'm pretty sure my wife could not have done by herself the calendar she has printed, with pictures of our daughter. I'm pretty sure it is in the print wizard plugin.\nSeems to me he didn't have any plugin installed, so how can he review Digikam (or kimdaba) correctly ?!"
    author: "Ookaze"
  - subject: "Re: Article is a shame"
    date: 2005-05-10
    body: "I think he just could not find the image printing wizard, because it is in a VERY weird place. I had used DigiKam 0.7.2 for weeks and could not find it either. Later I found it somewhere in File -> Export -> ... I never looked in that spot because I never needed to export anything.."
    author: "Meneer Dik"
  - subject: "Re: Article is a shame"
    date: 2005-05-10
    body: "If Jonathan corbet, who is a very respected member of the community (*), has not found the functionalities such as \"printing\", it outlines some bad interface design in digikam, admitted by digikam users both in the lwn comments and here on the dot.\n\nIt is very sad that his criticism is not taken as a positive feeback and an opportunity for improvements. Did you noticed that his makes compliment to digikam ? Have you noticed too the title of the article series ?\n\n(*) and this is an understatement."
    author: "oliv"
  - subject: "Re: Article is a shame"
    date: 2005-05-11
    body: "Except I think its a reviewers job to know what they are reviewing. Note the functionality it has, but say that finding it is a hassle and bad UI. Instead of just saying it doesn't exist since he apparently couldn't be bothered (perhaps a bit grumpy).\n\nLike was noted at LWN, he really did miss the point of KimDaBa."
    author: "Ian Monroe"
  - subject: "digiKam"
    date: 2005-05-10
    body: "I really like digiKam there are, however, two features that I think it lacks. The first lacking is the ability to search. In my opinion you should be able to search for pictures based on multiple tags, comments and even exif data.\n\nThe second lacking is possible to work around already but, it would be nice if it was easier. That is the ability to restrict editing the images. digiKam makes it almost too easy to alter or damage a picture. I think that it would be better if the changes or editing was done in the database and the original image was preserved. As I said, you can work around the problem by using file permissions. Simply set the file permissions to Read Only.\n\nSearch would be awesome!"
    author: "None"
  - subject: "Re: digiKam"
    date: 2005-05-10
    body: "Search is coming.  0.7.3 is around the corner (though without search), and .8 is in development.  Giles et al on the list are quite friendly, and tend to fix things quite quickly (like the endianness issue with colour stuff like solarize).  I've got the beta running on Kubuntu on amd64 using nothing more than apt-get source and a compile call.  Quite stable, quite fast :)\n\nFor the poster who raised the editing plugins thing - digiKam seems to be keeping the core plugins purely photo related.  It's up to kipi plugins (which are meant to be plugins for every KDE image app) to do the fun things like make a calendar etc.  Don't want that in your digiKam?  Don't enable the plugin."
    author: "Duncan"
  - subject: "Re: digiKam"
    date: 2005-05-11
    body: "Ideally the search would use the kmrml framework, which has awesome potential but I think is underutilized!"
    author: "kundor"
  - subject: "Re: digiKam"
    date: 2005-05-11
    body: ">  In my opinion you should be able to search for pictures based on multiple tags,\n> comments and even exif data.\n\n> ability to restrict editing the images. \n\nIt seems to me that you are looking for KimDaBa[1]. While digikam is a very good application which does well image management and image editing, Kimdaba only does image management but is excellent at doing it. It allowed me to set quickly information about locations/persons/keywords for my 7 000 pictures, and navigating between them is now very pleasant.\n\nIt can search for multiple tags, including incrementive search [2], and for comments, but not for exif data[3] for now. Since it's not an image editor, he never touch original files, you can set your pictures read-only if you want. For example it you rotate a picture, it will store this information in his XML database, and apply it at runtime.\n\n[1] http://ktown.kde.org/kimdaba/  Version 2.1 is only a few days old.\n[2] picture 1 and 3 of the 3 minute tour http://ktown.kde.org/kimdaba/tour/\n[3] The mailing-list is open if you have good comments about this issue.\n\nPS: and if you need an image editor too, I would of course recommand to use our beloved Digikam at the same time."
    author: "jmfayard"
  - subject: "Re: digiKam"
    date: 2005-05-12
    body: "> The second lacking [...] is the ability to restrict editing the images. [...]\n> you can work around the problem by using file permissions. Simply set the file permissions to Read Only.\n\nThat's not a \"workaround\". Everything *else* would be a workaround. Just add a lock action (e.g. with lock icon in the toolbar) that does set/unset the writable file permission. Saving that in a database is IMHO silly and misguided."
    author: "Melchior FRANZ"
  - subject: "Re: digiKam"
    date: 2006-06-16
    body: "digikam is a nice program. However, I had to figure out that it completely *ignores* the file permissions. I set all my image files to read-only, but after editing, it just overwrites the files! Indeed, this should be considered a bug.\n\nWhat would be nice: an undo button for all actions, e.g. for adding keywords to a selection of files or for removing files.\n\nCheers,\nErwin\n"
    author: "Erwin"
  - subject: "Image screensaver"
    date: 2005-05-10
    body: "Unrelated and still somewhat related - which is the best image screensaver available for Linux? Mac supplies a really good one: it shows high-res pictures, with a slow  zoom and pan movement, and after a while fades to another picture. Simple, but really effective and good looking. I have not found anything similar for Linux - there are all kinds of fancy effects but they are more annoying than anything. If none can be found - which is the best image library available to write something like this?"
    author: "Claes"
  - subject: "Re: Image screensaver"
    date: 2005-05-11
    body: "xscreensaver comes with glslideshow. Which is very nice."
    author: "Robert"
  - subject: "Re: Image screensaver"
    date: 2005-05-11
    body: "Exactly what I was looking for. Thanks!"
    author: "Claes"
  - subject: "PrintScreen"
    date: 2005-05-11
    body: "One of the basic graphic tasks in KDE bothers me still. You can't get a screeshot with a simple click on 'Prt Scr' and paste it to an image editing application.\n\nThe current way of using KSnapshot is a bit akward IMHO. It's not a big problem for me, but newbeginners find it difficult to take screenshots and paste them into a document.\n\nYou can bind Prt Scr to KSnapshot, but you still need to save the image and insert the file into document manually. I'd rather not use KSnapshot if all I wanted is a full screen shot. KSnapshot is nice when you want to take a shot of a certain application window.\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "Really? Works for me.\n\nI pressed \"Prt Scr\" and pasted it into KolourPaint...\n\nGlobal shortcuts in the Control Center have \"Desktop Screenshot\" and \"Window Screenshot\" actions, with \"Prt Scr\" with some modifiers assigned as the default shortcuts.\n"
    author: "Dima"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "Ctrl - Prt Scr ->screenshot of desktop\nAlt - Prt Scr ->screenshot of current active window.\n\nAnd as an added bonus you get a thumbnail of the image image in the Klipper menu."
    author: "Morty"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "You should have told me this long time ago! *Really* nice to know."
    author: "Carsten Niehaus"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "I didn't actually know myself before I tried it prior to posting. It's a feature I newer have used myself with KDE, but I have used KSnapshot a few times. Since I take a minimal amount of screenshots I have never bothered to do it another way. I have on the other hand used Ctrl-Prt Scr on windows and guessed that KDE does much the same. I tried it and it worked perfectly. \n\nSometimes when you want a feature or way of doing something in KDE, try it like it's done on other desktops or in the way it's most logic to do. In my experience more cases than not, KDE already has the feature:-)"
    author: "Morty"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "Since when does this work? Or is this distribution-specific?\n"
    author: "ac"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "The feature has been in KDE since at least KDE-3.1.0. There was a problem in Qt that made the feature a bit flakey, but it has worked fine for me for the last two years or so now.\n\n(See http://bugs.kde.org/show_bug.cgi?id=51620)"
    author: "teatime"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "The Ctrl-Prt Sc has worked for a long time, KDE 2 times I'd guess. Just tested on KDE 3.2 and it worked so every release after that. Of course it can become distribution specific, as some distributions are known to do strange things from time to time. Afterall the keybindings are fairly easy to change/mess up, so some distributions may very well have managed. \n\nThe preview in Klipper is a KDE 3.3 thing, I think. Since I have not seriously used 3.3 to any length, I'm not sure."
    author: "Morty"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "My Alt-Prt Scr is reserved for the kernel so I assume that doesn't work. I prefer ksnapshot anyways since I'm usually saving the screenshots online, KDE's network transparency makes this easy."
    author: "Ian Monroe"
  - subject: "Re: PrintScreen"
    date: 2005-05-11
    body: "Just curious - by \"Alt-Prt Scr\" you mean the \"Sys Rq\" feature, right? That requires you to compile kernel in debug mode - does that make the kernel slower or have any other side-effects?\n"
    author: "Dima"
  - subject: "Re: PrintScreen"
    date: 2005-05-12
    body: "It doesn't put the kernel in 'debug mode' - the option to enable sysrq just happens to be under 'kernel hacking' in the kernel config is all. Doesn't have any side effects really. Its nice if your computer crashs a lot (like my previous one did)."
    author: "Ian Monroe"
  - subject: "Re: PrintScreen"
    date: 2005-05-12
    body: "These are configurable shortcuts. Check proper Kcontrol module."
    author: "m."
  - subject: "Re: PrintScreen"
    date: 2005-05-12
    body: "Thanks Morty, I thought that I'm an advanced KDE user, but...\n\nI feel ashamed now - and enlightened :)\n\nI always tried using just simple Prt Scr without modifiers. This is just one of those little rough edges we still have in a very nice desktop environment.\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: PrintScreen"
    date: 2005-05-12
    body: "As others have said, you can use Ctrl-PrintScrn and Alt-PrintScrn, but you should also know that you can press Ctrl-C in ksnapshot to copy the image to the clipboard.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: PrintScreen"
    date: 2006-10-05
    body: "If the print screen button isn't working for you, check that you have a recent version of KDE, and then under control center->Regional & Accessibility->[Global Settings]->General Settings, make sure that Disable KHotKeys daemon is not checked, and double check the settings under Preset Actions.\nI just had a weird thing where this was only working on one of my two systems, and that was the cause"
    author: "DSS"
  - subject: "Re: PrintScreen"
    date: 2007-06-09
    body: "To be honest, I hate this single key screenshot. My PrtScr/SysRq key is right next to Backspace (both being small, normal size keys) and when I type fast, very often I am getting 1-2 snapshots popping up and I have to stop and kill them. Went to KDE hot-keys menu and there it says that it should only work with Alt+PrtScr - not true since I know I don't make this combination... A single key for snapshot with my keyboard is not a good thing, especially when one has my keyboard type. And no, I will not change my keyboard with one that has PrtScr at a safe distance, just for an application."
    author: "Andi V"
  - subject: "Re: PrintScreen"
    date: 2007-11-10
    body: "KDECC > Regional & Accessibility > Input Actions, Preset Actions > PrintScreen, check the box labeled Disable and click the Apply button. Problem solved!"
    author: "Stumpy842"
  - subject: "Slideshow with EXIF rotation possible?"
    date: 2005-05-12
    body: "Hello,\n\ndigikam offers a slideshow feature.\n(This is AFAIK part of the kipi plugins library, and therefore only loosely\nrelated to digikam, I know. But a naive user won't recognize that!)\n\nDoes anyone succeeded displaying images containing EXIF information\nfor rotation correctly? (Is there some settings dialog to activate it?)\nI like to keep the original versions of my images\nand not rotating it with an extra, even \"lossless\" tool...\n\nThe \"build-in\" image editor does not have any problems with that!\nFor the \"aspect ratio crop\" feature, using \"reset values\"\nportrait/landscape is also not chosen according to the EXIF information\nwith my Installation here...\n\n(SuSE 9.2, supplementary rpms for KDE3.4)\n"
    author: "Martin"
---
<a href="http://lwn.net/">LWN.net</a>'s <a href="http://lwn.net/Articles/grumpy-editor/">Grumpy Editor</a> examined <a href="http://lwn.net/Articles/131394/">image management applications</a> including <a href="http://digikam.sourceforge.net/Digikam-SPIP/">digiKam</a> and <a href="http://ktown.kde.org/kimdaba/">KimDaBa</a> after some time ago he already evaluated digiKam for <a href="http://lwn.net/Articles/122826/">accessing his digital camera</a>:  <i>"digiKam is a capable and useful tool with a few remaining shortcomings. Given its pace of development, chances are that those issues will be ironed out in short order."</i>




<!--break-->
