---
title: "Tellico: The Cook's Collection"
date:    2005-05-07
authors:
  - "mgagn\u00e9"
slug:    tellico-cooks-collection
comments:
  - subject: "Great app !"
    date: 2005-05-08
    body: "Thank a lot for this news who made me discover this cool app that I consider to be the book equivalent to amarok in term of ease of use.\n\nThe only downside for me is that the web search doesn't take the cover on amazon like it does in your review.\n\nIs there any mean to use thing like the BaracodaPencil to speed up the process ?"
    author: "JRM"
  - subject: "Re: Great app !"
    date: 2005-05-08
    body: "It used to take the covers from amazon, I used that functionality last time I tried it. It may be a bug or that amazon has done some changes. I looked at the Telico site, but found no ansver there. Don't seem to have any public mailinglist or bugtracker. "
    author: "Morty"
  - subject: "Re: Great app !"
    date: 2005-05-08
    body: "I've found the source of the problem :\nhttp://www.periapsis.org/archives/2005/04/27/tellico_may_not_show_icons.html\n\nI'll have to wait for the next version :)\n"
    author: "JRM"
  - subject: "Re: Great app !"
    date: 2005-05-08
    body: "That bug are annoying, but does not look related to not getting the cover image from Amazon. When selecting a writer this bug shows the Telico icon instead of the covers in books window.\n\nI'll be waiting for the next version too:-)"
    author: "Morty"
  - subject: "file:// urls"
    date: 2005-05-08
    body: "Uhm.. I tried it with the intention of organizing all my PDF books, but it doesn't seem to like URLs referring to my local machine (it works for external http:// and the likes), so I can't really use it for now. Else a great app!"
    author: "uga"
  - subject: "Re: file:// urls"
    date: 2005-05-08
    body: "Forget it. It must have been a problem with my build, since the debian pack seems to work like charm for my PDFs. The message I got while clicking on th url was something like \"service not allowed\" or something like that, but it seems to plain work here.\n\nThanks for your nice work"
    author: "uga"
  - subject: "Re: file:// urls"
    date: 2005-05-08
    body: "Don't know if this was a typo in your posting, and I haven't tried tellico, but the \"file://\" in your subject looks suspicious. The double slash is only for when you are specifying a hostname (as in http://localhost/), which is AFAIK not allowed in the \"file:\" protocol, IIRC not even with \"localhost\". So it should be \"file:/home/foo/\"."
    author: "Melchior FRANZ"
  - subject: "Re: file:// urls"
    date: 2005-05-08
    body: "Ah yes, sorry, I didn't explain it ok.  I had typed it with \"file:/\", without it,  with file:///\" (svn style), double... I tried almost everything I could think of. I didn't try anything like \"localhost\"ing though =)\n\nAnyway, it seems to work fine now, thanks."
    author: "uga"
  - subject: "Great!"
    date: 2005-05-09
    body: "Tellico is the best collection manager I have seen. I use it a lot in combination with Kile to create and organise bibliographies for my articles and reports. \n\nThanks a lot for this great application, Robby."
    author: "Claire"
  - subject: "Re: Great!"
    date: 2005-05-10
    body: "I second that. Tellico is very nice. The CSV importer GUI is very good and should be reused for all KDE applications that need them. The only thing I miss in Tellico is a possibility to sort my bibliography chronologically. The last entry I have done at the top and the first at the bottom.\n\nI think the quality of Tellico makes it a very good candidate for KDE Extragear.\n\nCheers,\nCharles "
    author: "Charles de Miramon"
  - subject: "Cool.."
    date: 2005-05-10
    body: "Could use a template for girlfriends though. ;)"
    author: "Rob"
---
<a href="http://www.periapsis.org/tellico/">Tellico</a> is a KDE application for organising your collections. It provides default templates for books, bibliographies, videos, music, coins, stamps, trading cards, comic books, and wines.  Marcel Gagné has written <a href="http://www.marcelgagne.com/cwl042005.html">an extensive review of Tellico</a>.  "<i>I like to think of it as a very versatile personal library system.</i>" He explains how he uses Tellico to keep track of which books his friends and family have borrowed.


<!--break-->
