---
title: "KDE at FOSDEM 2005 Report"
date:    2005-03-03
authors:
  - "jriddell"
slug:    kde-fosdem-2005-report
comments:
  - subject: "Woooow"
    date: 2005-03-03
    body: "Thanks for this report, it leads us to interesting talks to read and nice pics to look at. Debian Women! Go! ;-) Cool logo by the way. \nAh, and a  note to the reader: as we say in French, not all who wear a skirt are women (loosely translated)"
    author: "annma"
  - subject: "Debian project leader"
    date: 2005-03-03
    body: "Just for correctness' sake...  Matthew Garret is a Debian Project Leader candidate - elections will be held soon, and http://www.nl.debian.org/vote/2005/vote_001 shows 5 other candidates, too.  So speaking of 'future DPL' is a bit premature.\n\ncheers\n"
    author: "cmot"
  - subject: "Re: Debian project leader"
    date: 2005-03-03
    body: "Maybe Jonathan has a krystal ball? :-)"
    author: "Anonymous"
  - subject: "klink"
    date: 2005-03-03
    body: "This sounds really cool, especially the knotes integration. But I have a question... what happens when a file becomes renamed? Will there be a monitor that records all 'rename' actions?\n\nJust curious...\n"
    author: "MM"
  - subject: "Re: klink"
    date: 2005-03-03
    body: "Well, nothing has been implemented yet, these are only ideas. But you might try contacting Scott for this issue."
    author: "Bram Schoenmakers"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "No need for it anymore, thanks though."
    author: "MM"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "> Will there be a monitor that records all 'rename' actions\n\nhopefully not. \n\nthere's no reason the name of the note must be reflected in the name of the file. the file \"name\" could simply be numeric and in a hidden folder, which they already are. so the user shouldn't be going about in there and renaming those files. \n\nit's kind of like worrying what happens when a user renames config files in /etc or `kde-config --localprefix`/share/config ;)\n\nmoreover, there's no real reason why the notes themselves would have to be stored in regular on-disk files at all."
    author: "Aaron J. Seigo"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "Well, I meant it the other way around...\n\nJust to make it clear... e.g., there is a file file://home/mm/unnamed.kwd and it has a note assigned to it. Then the file is renamed/moved to file://home/mm/Documents/todo.kwd\n\nWill the note be linked against a wrong/non-existing document? What about the search feature, will it find the right file, but the wrong name and path?\n\nI'm asking because I believe I remember Apples spotlight seems to have integrated something that solves such problems, but I don't know how this works (and if it works). Maybe they just changed their \"Finder\" so it keeps track of all rename and move actions. Or they changed the libraries.\n\nI'm aware that this is a minor problem during the design phase, just wanted to mention it.\n\nSorry for my english...\n"
    author: "MM"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "ah.. when the _target_ of the note's URI changes.. yes, this is something we need to watch for. there area few ways of doing it that i can think of, but i'm not sure at this point which is the best. certainly an area for research =)\n\nthe big factor will be whether we care if it breaks when using the command line (we probably do) and if so how efficiently we can hook into change notifications from the kernel. this _is_ one of the things that's easier to do when you embed it into the filesystem itself, a luxury we don't have.\n\nbut as long as we can get these notifications working decently, there is only one point in the mesh for any given URI where than URI is actually referenced. so making these changes, once they are known, is trivial to do with exactly zero loss of information within the mesh itself."
    author: "Aaron J. Seigo"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "AFAIK, the only notification DNotify produces is \"directory changed\" --- and do all systems even have change notification? So you probably can't count on kernel help.\n"
    author: "SadEagle"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "> this _is_ one of the things that's easier to do when you embed it into the filesystem itself, a luxury we don't have.\n\nI was about to suggest a Reiser4 plugin, but I'm not sure if plugins are allowed to do anything more than providing attributes (or \"files within a file\").\n\nIf plugins are limited in this way, it though should be possible to save a \"history\" of the file (recent names and paths, date and time of the rename/move action). But this information would be useless for klink, as attributes can only be read if the correct path and name are known, so klink would have to search the whole harddisk and scan the \"history\" attributes of all files...\n\nAnd not everybody uses Reiser...\n"
    author: "MM"
  - subject: "Re: klink"
    date: 2005-03-04
    body: "Oh, and even if Reiser4 plugins are not limited, I wonder how a notification system of a file system could look like... I feel a bit uncomfortable having a file system that depends on dcop (no offense, dcop is great, but on file system level?)\n"
    author: "MM"
  - subject: "Using extra computer resources"
    date: 2005-03-03
    body: "It should be noted that CPU processing power has increased dramatically in the last 9 years but that harddisk access times haven't. KDE performance is limited mostly by the latter and adding background search indexing is going to hurt there because it will make the harddisk access times worse.\n\nEven despite the increase in CPU processing, drawing in e.g. Konsole manages to keep even the fastest CPU's busy because of Qt's poor support for fixed width fonts.\n\nSo if you like challenges: redesign Konsole to work with variable width fonts and still have your \"ls -al\" output line up.\n\nExtra cookies are available for bidi-support.\n"
    author: "Waldo Bastian"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-03
    body: "It should also be noted that:\n\na) if you index the contents of file just after it has been modified, then it is probably still in the system's disk cache.\n\nb) Most desktop hard disks are not kept busy 100% of the time. They are often doing nothing. This wasted time can be used for indexing. The inder will also need stop what it is doing when something more important (like the user) wants to use the hard disk.\n\nThere is no reason for search indexing to hurt.\n\nBefore everyone gets the wrong impression about Scott's work, content indexing *is not* what it is all about. Although it looks like it will play a part, the idea is to record the context and 'links' between things and documents that users create implicitly by just using their desktop and applications.\n\njust IMHO of course.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-03
    body: "> There is no reason for search indexing to hurt.\n\nIs there already a way (or always has been w/o me knowing it :-)) to do disk indexing w/o trashing the disk caches?"
    author: "koos"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-03
    body: "> Is there already a way to do disk indexing w/o trashing the disk caches?\n\nnope,\n\nbut you don't have to constantly thrash the hard disk either. That is only needed when building the _initial_ index.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-04
    body: "and even the initial indexing can probably be mitigated somewhat by being run as a lazy background process."
    author: "Aaron J. Seigo"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-04
    body: "I don't understand what you mean with thrash the hard disk. You said that it should run if hdd is idle and it wouldn't hurt performance. But in the morning, after updatedb has ran, my system is a lot slower (that's what I meant with the disk cache being trashed). Well at least on linux, their is no API that I know of to skip disk caching while indexing (or eg. playing a DVD).\nSo, how do you manage to not hurt performance then?"
    author: "koos"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-04
    body: "It's becoming a requirement of modern kernels to have useful (i.e. not dnotify) mechanisms for notifying user-space of file system changes.  At least on Linux there's a reasonable chance that by the time this stuff is ready for the mainstream that inotify will also be something we can rely on.\n\nWe'll see.  But these really aren't the issues that we're worrying about now, premature optimization being the root of all evil and all that."
    author: "Scott Wheeler"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-03
    body: "Why then can we just hack on the QT implementation of the fonts? This would be more efficient than a picemeal approach!\n"
    author: "Ngigi Waithaka"
  - subject: "Re: Using extra computer resources"
    date: 2005-03-04
    body: "> adding background search indexing is going to hurt there because it will make\n> the harddisk access times worse\n\nit seems that we will likely only have to index files \"relevant\" to desktop search. so... we likely won't be indexing things like the http cache, or system directories, or tmp directories, or ... the number of \"relevant\" file writes and locations is, hopefully, manageable.\n\nby plugging support for such things into the file dialogs, we should be able to catch many such file writes quickly.\n\nthis doesn't mean it's without yet-to-be-solved challenges such as which locations to index by default, or how to pick up changes that were made on the file system but not through the system (changed file notifications, basically).  there's also the factor of how up to date the search needs to be in relationship to the file system. \n\nwhen someone saves 100 files in one go to the filesystem, do they need to be indexed immediately? what are the \"rules\" that govern this?\n\nwhen someone removes 100 files from the filesystem, how quickly do they need to be removed from the index?\n\netc... the file indexer will be an interesting project in its own right. =)"
    author: "Aaron J. Seigo"
  - subject: "thanks jonathan..."
    date: 2005-03-03
    body: "For your tireless and interesting coverage of FOSDEM up to date.  \n\nYou're also a great editor to have on the KDE Dot News team and it's just awesome to see all the work you are doing for KDE.  We're lucky to have you.  :)"
    author: "Navindra Umanee"
  - subject: "and Fabrice"
    date: 2005-03-03
    body: "...who organised the accommodation and the dev-room and a bunch of other stuff too.  Thanks Fab and everyone else involved!\n\n--\nSimon"
    author: "Simon Edwards"
---
<a href="http://www.fosdem.org">FOSDEM</a> is Europe's biggest meeting of Free Software developers and KDE turned out in force at it last weekend. As well as talks in the main track on KDE and KDevelop, the KDE Developers' room hosted a series of other talks. We also ran a stall and still found time for some hacking.
 

<!--break-->
<p>FOSDEM is a spectacular event. In the capital of Europe over two thousand geeks come together for a weekend of talks, hacking and socialising. One contingent of KDE developers came on a road trip from the Netherlands and spent much of Friday evening spiraling Brussels until we came to the pre-FOSDEM party in Grand Place where we were greeted by the welcome sight of as many litre glasses of Hoegaarden as we could drink. The great and the good of Free Software were there including <a href="http://jriddell.org/photos/2005-02-26-fosdem/2005-02-28-fosdem-daf-alan-jonathan.jpg">Alan Cox</a>, possible future Debian Project Leader <a href="http://www.angrydpl.com/">Matthew Garrett</a> and our own founder Matthias Ettrich.</p>
 
<p>On the Saturday we set up our stall selling a fine range of KDE t-shirts and badges sporting the <a href="http://www.kde.org/stuff/clipart.php">new official KDE logo</a>. The first KDE talk was on <a href="http://www.kde.me.uk/index.php?page=fosdem-2005-kdevelop-talk">KDevelop by Harald Fernengel</a> who ran us through the many features of KDE's IDE. We then retreated to our own KDE Developers room for a <a href="http://www.kde.me.uk/index.php?page=fosdem-2005-koffice-talk">talk on KOffice</a> discussing the up-coming 1.4 release and how to make plugins for KOffice using KParts. </p>
 
<p>That evening we went to a centre which trains women to use computers. In return for talking about Free Software and KDE we got internet access and a space to hack for the evening. It was great to see that many of the women were already running GNU/Linux. They mentioned that they would really like Ubuntu if only it came with KDE, which made Jonathan and Andreas smile.</p>
 
<p>Sunday opened with <a href="http://www.kde.me.uk/index.php?page=fosdem-2005-kde-talk">Matthias's talk on KDE</a> showing how much the Unix desktop has changed since 1996. We are, he says, at a very exciting time in desktop development with challenges and opportunities including Qt4, desktop search, D-BUS and more.</p>
 
<p>Later in the day our developer's room was busy for the <a href="http://www.kde.me.uk/index.php?page=fosdem-2005-kdepython-talk">KDE Development Using Python Talk</a>. Programming in Python will slow down your applications by only a second or two but speed up your development time by a factor of 2 or more because the language is so much easier to use than C++.  Issues about KDevelop's less than optimal Python support were raised, then swiftly brought crashing down when at the end of the talk Alexander Dymo announced that during the talk he had added Python support for KDevelop's Qt Designer. This earned him a round of spontaneous applause - obviously Python development is moving fast.</p>
 
<p>Our final talk was an update from Scott Wheeler about his ideas for <a href="http://www.kde.me.uk/index.php?page=fosdem-2005-search-talk">a searchable web of context</a> on the desktop. He has found existing search tools such as Google desktop and Beagle lacking because, unlike Google on the web, the desktop has no links, if we can add links to desktop entities then search will become a lot more powerful.</p>
 
<p>Other talks we went to included Alan Cox explaining how he manages to make a stable branch of Linux releases based on all the fixes Linus doesn't use; Hanna Wallach promoted the Debian Woman project ("not here to provide girlfriends to lonely geeks") which has managed to double the number of women involved with Debian, also RMS presented the Free Software Foundation's award to OpenBSD's Theo de Raadt.</p>
 
<p>If you ever get the opportunity to go to FOSDEM do not turn it down.</p> 
 
<p><a href="http://jriddell.org/photos/2005-02-26-fosdem/">Photos from Jonathan</a>, <a href="http://vizzzion.org/?id=gallery&gcat=Fosdem2005">Sebastian</a> and <a href="http://jriddell.org/photos/2005-02-26-fosdem/raphiel/">Raphiel</a>.</p>

