---
title: "TUX Magazine: iPod for KDE and K3b"
date:    2005-06-17
authors:
  - "jriddell"
slug:    tux-magazine-ipod-kde-and-k3b
comments:
  - subject: "I Love K3B!"
    date: 2005-06-17
    body: "K3b is very useful and much more useful than Nero (on windows). I wonder how could we make a bootable cd in k3b using the boot.img or boot.ima image files? A million thanks to k3b developers!"
    author: "Fast_Rizwaan"
  - subject: "Re: I Love K3B!"
    date: 2005-06-17
    body: "If k3b supports the imageformats of the boot.img or boot.ima image files and those images are bootable the CD's become bootable, just like when you burn a bootable iso image. It all depends on the image, if the image is bootable then the burnt CD becomes bootable. You  have to burn as image, not burn the image file onto the CD. Use Tools->CD->Burn CD image..."
    author: "Morty"
  - subject: "Re: I Love K3B!"
    date: 2005-06-17
    body: "I already burned a useless cd with an image as a file... K3B should pop up a warning if the project file contains just an image file, and you tell it to burn it..."
    author: "Amadeo"
  - subject: "Re: I Love K3B!"
    date: 2005-06-19
    body: "Then add your vote to Bug 64560. I reported this problem long ago."
    author: "Meneer Dik"
  - subject: "Re: I Love K3B!"
    date: 2005-06-17
    body: "Although I _really_ like K3B, calling it more useful than Nero is something I don't agree with. Even basic things, like burning IMG files isn't possible with K3B. Not to mention all other formats (e.g. MDF files). Also it's not possible to burn WMA-files to an audio cd. These I consider basic features which K3B doesn't have."
    author: "LB"
  - subject: "Re: I Love K3B!"
    date: 2005-06-17
    body: "don't use wma, use mp3 instead."
    author: "ggabriel"
  - subject: "Re: I Love K3B!"
    date: 2005-06-18
    body: "don't use wma, don't use mp3, use ogg vorbis (or flac) instead."
    author: "maestro_alubia"
  - subject: "Re: I Love K3B!"
    date: 2005-06-20
    body: "Considering how poorly ogg (or flac) is supported on portable players, I for one can't really use it at all."
    author: "Janne"
  - subject: "Re: I Love K3B!"
    date: 2007-01-31
    body: "this thread is probably long dead, however, if you have a supported player, RockBox firmware replacement for iRiver, iPod and others DOES support both FLAC and Ogg\n\ncheers"
    author: "Matthew"
  - subject: "Re: I Love K3B!"
    date: 2007-11-14
    body: "But then you don't have support for wma, so we are just running in circles."
    author: "Travis Wilson"
  - subject: "WMA is supported!"
    date: 2005-06-17
    body: "K3b version 0.12 supports wma!"
    author: "Fast_Rizwaan"
  - subject: "Re: WMA is supported!"
    date: 2005-06-17
    body: "Cool, thanks for the update, and IMG files?"
    author: "LB"
  - subject: "Re: WMA is supported!"
    date: 2005-06-17
    body: "If you bother to explain what IMG files are, perhaps you may get some answers. Is it some kind of strange undocumented proprietary format, seeing limited use by a singel program on some platform? I for one have never seen or heard of it before."
    author: "Morty"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "I'm sorry but an IMG file is a common and open format (UDF) and all the burn progs I have checked (6) support them.\n\nISO files are based on the iso9660 standard\nIMG files are based on the udf standard\n\nIMG files are related to DVD's as ISO files are related to CD's\n\nIf your Linux kernel supports UDF file system you can mount an IMG file to the loopback, just like ISO's\n\nIMG files are common in places where DVD images are shared, most well known example of course is BitTorrent, 98% of all the DVD that are shared are based on IMG files."
    author: "LB"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "As far as I know cdrecord has supported this for a long time, since K3b uses it I'd guess it should work. Have you tried burning it the same way you would a iso image?"
    author: "Morty"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "The problem is that K3B doesn't allow me to select IMG files, but I could try to rename it to ISO and see what happens."
    author: "LB"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "Not even if you set the filter to all files, as opposed to iso?"
    author: "Morty"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "Oops, sorry of course this is possible, maybe it just works in K3B but the name of the menu and filter fooled me and made me believe it's not possible."
    author: "LB"
  - subject: "Re: WMA is supported!"
    date: 2005-06-20
    body: "As far as i know, .img files are just containers for any kind of disk. An Image can be made of 1,44MB, 2,88MB discs or CDs,DVDs oder even Hard Disks. An .ISO file is also just an .img file with a different extension. .IMG stands for Image by the way. "
    author: "Metatron"
  - subject: "Re: WMA is supported!"
    date: 2006-04-18
    body: "You mentioned that it is possible to mount IMG files via the loopback - I tried it, but it with mount udf and it says wrong fs type - how can I mount a IMG file?\n\nThanks,\n\nRainer"
    author: "Rainer"
  - subject: "Re: WMA is supported!"
    date: 2005-06-17
    body: "You mean like PNG or GIF?"
    author: "ac"
  - subject: "Re: WMA is supported!"
    date: 2005-06-19
    body: "LOL, Just dump the windows applications all together. After all bill gates is rich enough, and screwed up those o/s long enough. Not to mention all of the companies; he has screwed over to become this rich! If anything we should pirate all windows app's and post them for those herds of misinformed people. "
    author: "Kaptain Kwerkie"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "Is IMG just some wierd proprietary perversion of a cd image file by any chance?"
    author: "Robert"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "I have little clue what the grandparent thinks an IMG boot file is, but, in my pre-cd experience, it was the extension most would give to a bootable floppy image. I remember using rawwrite to make boot floppies from files named like boot.img, floppy.img, or rescue.img as recent as 2003. I wonder if this is the same IMG being discussed."
    author: "ac"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "If it's a boot floppy, you can enable floppy emulation for the CD with k3b. Just go to \"Project\"->\"Edit Boot Images\"->\"New...\" and add your boot.img floppy image.\n\nIf it's a raw DVD image containing an ISO or UDF filesystem, go to \"Tools\"->\"DVD\"->\"Burn DVD ISO Image\"."
    author: "Stefan Heimers"
  - subject: "Re: WMA is supported!"
    date: 2005-06-18
    body: "It's indeed a raw DVD image containing an UDF filesystem, if you go to \"Tools\"->\"DVD\"->\"Burn DVD ISO Image\" you can't select IMG files. Will try to rename it to ISO and see what happens. Maybe this is easy to fix and is it only necessary to adjust the filter and rename the menu option."
    author: "LB"
  - subject: "Re: WMA is supported!"
    date: 2005-06-20
    body: ".IMG files are actually supported. just click on burn cd/dvd iso image, choose the image to burn. if you cant see your .IMG file just change the filter to \"All Files\" instead of iso9660 and select your .IMG file."
    author: "gardo"
  - subject: "Re: WMA is supported!"
    date: 2007-07-17
    body: "can somebody help me and explain how to burn wma with k3b?\ngreets fabi"
    author: "fabi"
  - subject: "Re: I Love K3B!"
    date: 2005-07-16
    body: "indeed, it is supported and has been for as long as I can remember, just select the .img file where you'd select the .iso.\nA little trial & error session could've saved you the moaning ;)"
    author: "whacker"
  - subject: "Re: I Love K3B!"
    date: 2005-06-17
    body: "I still have a Win98 partition on my computer mainly because, well, i paid for it, and Nero's drag and drop feature with rewritable CD's is gonna be about the only thing i will miss about windows, as i don't plan on updating to longhorn. But otherwise  K3B does everything Nero does. A pop-up notification if you are trying to burn in the wrong format would be great, tho. \nYou can download Nero for Linux for free if you have a reg number for nero 6, but mine is 5.5, and i don't think it would be worth paying for Nero when K3B already has it covered. And I don't know if Nero for Linux has drag and drop."
    author: "Old Guy"
  - subject: "Re: I Love K3B!"
    date: 2005-06-17
    body: "What are you meaning when talking about drag and drop, the ability to format a CDRW and use it much the same way you would use a floppy? Available from all applications? In that case it's only a implementation of UDF filesystemdriver (sometimes called packet writing) for windows, and that's defiantly not included in Nero for Linux. \n\nOn the other hand the Linux kernel have had such driver for years, sadly it is still considered unstable and experimental and not installed by defalt on most systems. If you are running a fairly recent 2.6 kernel and your CDRW drive supports Mt Rainer it's not to hard to setup, otherwise it's little harder but not impossible. More info: http://lists.suse.com/archive/packet-writing/ "
    author: "Morty"
  - subject: "amarok.kde.org?"
    date: 2005-06-17
    body: "Speaking of amarok, what happened to amarok.kde.org? /dev/null?"
    author: "ac"
  - subject: "Re: amarok.kde.org?"
    date: 2005-06-17
    body: "Rejoice, it's back now :) If it isn't, you'll just have to wait a couple of hours until your DNS cache is updated.\n\nWe're very sorry about the long downtime, but for the most part it wasn't our fault. I'll probably blog about the whole story soon.\n"
    author: "Mark Kretschmann"
  - subject: "Re: amarok.kde.org?"
    date: 2005-06-17
    body: "echo \"216.32.84.227 amarok.kde.org\" >> /etc/hosts"
    author: "jens dieskau"
  - subject: "Re: amarok.kde.org?"
    date: 2005-06-19
    body: "That just gives me the portal of some web space provider."
    author: "furanku"
  - subject: "Re: amarok.kde.org?"
    date: 2005-06-19
    body: "Thats because you went to IP address instead of following the instructions. :)"
    author: "Ian Monroe"
  - subject: "So, why not including this into amarok help?"
    date: 2005-06-20
    body: "In the amarok help I find only one useless sentence \"covering\" the iPod topic. Why is this info given to a closed magazine exclusively, while amarok users don't get a hint.\n\nSad thing, this is also true for many, many hyped features in KDE apps.\n\n"
    author: "bela"
  - subject: "Re: So, why not including this into amarok help?"
    date: 2005-06-21
    body: "I have just found a article about this feature. It says that you just need to mount your ipod to /mnt/ipod and restart amarok. I dont know about the required version and cannot test right now.\n\nArticle (german):\nhttp://www.noreality.ch/linux/howtos/ipod_linux_debian\n\nGreat work of the amarok team. Would be nice to have a windows version of it."
    author: "Hypnos"
  - subject: "Re: k3b bootable cd"
    date: 2006-02-19
    body: "i'm trying to burn a windows xp install cd i have all the files and the boot img.\n\nbut when i burn i get an error: CDBOOT: can not find NTLDR"
    author: "Xorphan"
  - subject: "Just about the strange extensions you find"
    date: 2006-11-25
    body: "/mnt/stuff/pro-dame.mdf on /mnt/cdrom type iso9660 (rw,loop=/dev/loop0)\n\nand the information you find like... here: http://filext.com/detaillist.php?extdetail=MDF\n\nLet's say, there are only 2 kinds of CD/DVD images: ISO and BIN"
    author: "Dejan"
---
In <a href="http://www.tuxmagazine.com/node/1000135">this month's TUX magazine</a> KDE's <a href="http://dot.kde.org/1116000449/">Jes Hall</a> explains how to get your iPod working with <a href="http://amarok.kde.org/">amaroK</a>.  It also includes a comprehensive guide to KDE's CD burning application <a href="http://www.k3b.org/">K3b</a>.  Available in HTML is a <a href="http://www.tuxmagazine.com/node/1000130">review of Kubuntu</a>. TUX is a magazine for new GNU/Linux users and available as free PDF download.  






<!--break-->
