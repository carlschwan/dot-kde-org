---
title: "ReallyLinux.com: Linux Makes Granny Cry "
date:    2005-03-31
authors:
  - "brockers"
slug:    reallylinuxcom-linux-makes-granny-cry
comments:
  - subject: "This is not a troll post..."
    date: 2005-03-30
    body: "... but I really wonder if a typical Granny is able to code HTML ?\n"
    author: "MM"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-30
    body: "The question is rather whether a typical HTML coder is able to become a grandpa ..."
    author: "Flitcraft"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-30
    body: "I think that Granny must be Grace Hopper:\nhttp://www.sdsc.edu/ScienceWomen/hopper.html"
    author: "jaykayess"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "Admiral grace died some years ago."
    author: "a.c."
  - subject: "Re: This is not a troll post..."
    date: 2005-03-30
    body: "Coding HTML is not exactly rocket science (There you got a inaccurate term, rockets are rather simple really. Perhaps quantum physics are a better description) and I don't think there are any reasons why a Granny can't learn. It all boils down to interrest and willingness to learn. Afterall you regularly see Granny's preform activities like crossword puzzles and knitting, like HTML coding those are skills where it's mostly a question of bothering to learn.   "
    author: "Morty"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "Oh, sure rockets are simple. In theory.\n\nThere are a bunch of 8-fingered guys that could explain you the problems with an excessive confidence in theory, though ;-)"
    author: "Roberto Alsina"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-30
    body: "Kate for granny is funny indeed..."
    author: "Amadeo"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "A few years ago I was visiting my aunt, who is a grandmother. She told me she was taking some courses to advance her employment, including something about computers. She went on to explain with obvious awe and fascination how an integer is marked as negative or positive. Her wonder increased as I added some other tidbits to her understanding, maybe surprised that her brothers kids had turned out alright after all.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "Why is the assumption always a.) typical users are the only users that matter and b.) the typical user is a drooling moron?\n\nI mean, if you feel the need to cater to the common user, great, but remember that many common users are able to do things like repair automobile engines, manage their own finances, etc.  When I use, say, GNOME, I get the impression that the \"average user\" is assumed to have Downs Syndrome.  Please don't rally to take KDE down that path."
    author: "regeya"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "> Why is the assumption always\n[...]\n> b.) the typical user is a drooling moron\n\n...because every software developer has met so many of this type?  ;-)))\n\n"
    author: "cm"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: ">When I use, say, GNOME, I get the impression that the \"average user\" is assumed to have Downs Syndrome.\n\n That remark *really* made my day! :-D\n\n>Please don't rally to take KDE down that path.\n\n Hail! HAIL! HAAAIILLL!!! Much rather make the online help so good that anyone who wishes to lern complex tasks actually *can* get the hang of it without a live teacher :-)\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: This is not a troll post..."
    date: 2005-04-01
    body: "## Much rather make the online help so good that anyone who wishes to ##\n## lern complex tasks actually *can* get the hang of it without a live ##\n## teacher :-)  ##\n\nAgree. Just like the new KDE-3.4 kprinter has. Really, really usefull, these \"What's This\" help blurbs....."
    author: "kprinter-fan"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "Hi you all,\n\nSorry for not replying to each post. I'm of course aware that not every user is silly. And I know there are experts of any age out there. But I though think that a significant amount of users with the age of that lady is neither able to code HTML, nor willing to learn it.\n\nThat said, this does not mean, KDE should treat its (\"average\") users as if they are exactly silly. The \"average\" user is not silly, but MAY be \"unexperienced\" with computers (or KDE in special). As someone else said it: For most people, the computer is a tool - and a frightening tool...\n\nPeace!\n"
    author: "MM"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "Dude, you obviously haven't been down to the local Genealogy Society lately, you should see what some of them are getting up to well into their 70's and 80's!  The more active section of the 'Seniors' community have brains just as sharp as us young-uns, and see computers and the internet as just another tool for them to use to achieve their ends.  And theyhave the time to do it too :-)\n\nJohn."
    author: "Odysseus"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "Yes, some of them..."
    author: "MM"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "I teach HTML to grandmothers all the time... OK, not all the time ;-)\n\nSeriously, some of my students really are grandmothers, and they do learn the basics of HTML.\n"
    author: "Eleknader"
  - subject: "Re: This is not a troll post..."
    date: 2005-03-31
    body: "No doubt that there are such exceptions. But they are, well, exceptions..."
    author: "MM"
  - subject: "Bored now..."
    date: 2005-03-30
    body: "Is it April 1st somewhere already?\n"
    author: "Rob"
  - subject: "Re: Bored now..."
    date: 2005-03-30
    body: "was exactly my reaction.so sad to see the Dot having such low standards..."
    author: "anon"
  - subject: "Re: Bored now..."
    date: 2005-03-31
    body: "It's funny, smile! Life is beautiful..."
    author: "Anonymous"
  - subject: "Re: Bored now..."
    date: 2005-03-31
    body: "in the netherlands it's still 30/Mar/2005....\na well, I'll read it again tomorrow then :P"
    author: "Mark Hannessen"
  - subject: "granny's short list"
    date: 2005-03-31
    body: "\nAnyone who spells \"voila\" as \"wallah\" 'aint no granny, no way.\nWe senior citizens \"can\" spell\n\n\n"
    author: "Brian"
  - subject: "Re: granny's short list"
    date: 2005-04-02
    body: "I didn't even make that connection. I was thinking wallah as in Bombaywallah - \"dude\".... wonder if Granny is British? :-) \n"
    author: "Bjorn"
  - subject: "hehe"
    date: 2005-03-31
    body: "Amusing... but is it just me, or do some here in the KDE community severely lack a sense of humor?  Is it inevitable that as the KDE community grows the tone of the community will change like this?"
    author: "ac"
  - subject: "Re: hehe"
    date: 2005-03-31
    body: "oh oh! :: raises hand :: I, for one have a sense of humor :-D\n\nI like the article, although it didn't have much content.  I presume its primary audience likes that stuff as well\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: hehe"
    date: 2005-03-31
    body: "No, I think it's the writers at Really Linux who lack a sense of humour.\n \nBesides, this is supposed to be a news site. There are better outlets for humour than the Dot, such as Planet KDE.\n"
    author: "Rob"
  - subject: "Re: hehe"
    date: 2005-03-31
    body: "Get a life!  Sheesh.   It's good publicity for both KMail and Kate."
    author: "ac"
  - subject: "Re: hehe"
    date: 2005-03-31
    body: "Make up your mind: is it a joke or a serious PR effort? You can't have it both ways. I think it sucks either way: for a joke it is not that funny and a bit awkwardly timed; for a PR effort it has way too many factual errors and weird assumptions where inexperienced desktop users immediately start programming before getting their senior butts on MSN to talk to the kids only to find out it doesn't work that way because they're sitting right next to them in the first place, having installed and set up everything.\n\n(I realise I am ranting. :-) But if this is supposed to be good publicity, I'd hate to see what bad publicity is like.)"
    author: "Rob"
  - subject: "Re: Rob's lost his cool..."
    date: 2005-04-01
    body: "Rob, please do us all a favor, take a moment to step outside, breathe some fresh air and relax a bit.  \n\nUnwind... for your own sake! \n \nIt's okay for differing opinions, it's also okay to chill rather than start flinging at other posters here on the dot, or at other linux website editorial people.  So we understand, you didn't think much of the article or of someone posting a link on the dot... okay.  That's fair enough, but the rest is just note nice... for instance, leave hehe alone!\n\n"
    author: " "
  - subject: "Re: Rob's lost his cool..."
    date: 2005-04-04
    body: "Sorry, I forgot that in the KDE community it's considered chilled behaviour to tell me to \"get a life\" but it's not okay for me to respond to that - even while admitting I'm on a rant.\n\nI'll take back that the editors of the article have no sense of humour, but I maintain that this particular article doesn't truly show it.\n"
    author: "Rob"
  - subject: "Re: hehe"
    date: 2005-03-31
    body: "Everything goes on April the 1st, the only problem is that it's not April 1st yet."
    author: "Jiohan V"
  - subject: "kwrited"
    date: 2005-03-31
    body: "Kate, kwrite, and kwrited are the same thing? Hmmm..."
    author: "anonymous"
  - subject: "Re: kwrited"
    date: 2005-04-21
    body: "No. Kwrite and Kate are built over a common library that makes some features of them similar. Kwrite is simpler than kate and, as expected, needs less memory to run. Kwrite is intended for single file editing. Kate is perfect for multiple files editing.\n\nTake a look at the url:\n\nhttp://wiki.kde.org/tiki-pagehistory.php?page=Performance+Tips&preview=7\n\nThere's an explanation about several KDE services, including kwited."
    author: "Mauricio Kaster"
  - subject: "Who cares what these people use?"
    date: 2005-03-31
    body: "My fave apps are Firefox & K3b. Who cares?\nNow, I become also on top at dot.kde.org?\n"
    author: "anonymous"
  - subject: "not surprising to me "
    date: 2005-03-31
    body: "HTML seems to be something really cool for seniors. Look at kdewebdev (Quanta (html editor)/Kommander...) project leader Eric  Laffon, ain't he a grandpa anyway? :)"
    author: "Pat"
  - subject: "Re: not surprising to me "
    date: 2005-04-01
    body: "Thanks for the senior reference... especially since I just turned 48. I guess I can join AARP at 50 and get senior discounts, but I'm still going to the gym and feeling like a kid. I'm old enough to be a grandpa but I don't see myself as an old person. In fact you should expect people who take care of themselves to start living a lot longer in the 21st century. I plan on getting a lot older and having at least another 50-100 or more years of active life. Scientists predict children born the first part of this century could have lifespans of 10 times what we do with a much slower aging process. Anyway the old ones in my house are my 15 year old dog and two 14 year old cats. They still play too. Only one of my cats is interested in computers though and she snuck her face onto the Kommander splash.\n\nThanks for thinking of me when the subject of old people comes up, but I can't go getting old until I save enough money to hire a nurse to push my wheelchair while I swing my cane at you and rant about young people. ;-)"
    author: "Eric Laffoon"
  - subject: "The nicest features have not even been mentioned!"
    date: 2005-03-31
    body: "Kate supports tabs, saves loaded file states and editing positions across sessions, KDE has a VERY nice kalendar app of which there is NO free alternative. The KDE gv app even remembers the page number, where one has left reading, across sessions.\n\nKDE is the greatest desktop ever! GNOME does not even manage to load KDE applets in time... sometimes they are loaded before the tray app has been loaded and they begin to float around on the screen. The KDE gv app does not reload properly across sessions and so on and on and on. And, btw., GNOME does not have a calendar at all!\n\nBut there are also missing some small things:\n\n1.) The clock should have its' background color configurable even if the control bar is transparent: you cannot see what time it is if the background image has approximately the same color as the clock's digits.\n\n2.) Want a CPU speed applet.\n\n3.) The battery applet should execute a (configurable) shutdown command by itself, ie. not relying on ACPI events.\n\n4.) Want a wireless network manager. There is NetworkManager, but, to be honest, I don't care for automatic network detection. I want to switch between network profiles easily. Nothing more. There is kwifimanager, but that one requires entering the root pwd each time. That's not good. \"root\" should define the various network profiles or allow users to do so. But the user himself should be able to switch them."
    author: "Cinquero"
  - subject: "Re: The nicest features have not even been mentioned!"
    date: 2005-03-31
    body: "kcpuload is a very nice cpu load applet."
    author: "Boudewijn Rempt"
  - subject: "Re: The nicest features have not even been mentioned!"
    date: 2005-04-01
    body: "> 4.) Want a wireless network manager. There is NetworkManager, but, to be honest, I don't care for automatic network detection. I want to switch between network profiles easily. Nothing more. There is kwifimanager, but that one requires entering the root pwd each time. That's not good. \"root\" should define the various network profiles or allow users to do so. But the user himself should be able to switch them.\n \nA Kommander user told me he made a custom network and profile configuration manager in Kommander in a matter of minutes. It was cool, but rather specific to his needs. The point is, the other cool stuff like Quanta's extensive project management and Kommander for application integration and mini apps wasn't in there either. ;-) \n\nSoftware is very personal, and eventually you will need to do something with it to make it just what you personally need. The general applicability of a given piece of application software will eventually reach it's limits. That's why we're developing Kommander with the eventual target to be dead easy for users."
    author: "Eric Laffoon"
---
<a href="http://www.reallylinux.com/">ReallyLinux.com</a>'s resident Granny <a href="http://www.reallylinux.com/docs/gran3.shtml">has posted a short list</a> of her favorite KDE applications. Granny specifically thanks <a href="http://www.kde.org/areas/people/stefan.html">Stefan Taferner</a>, <a href="http://www.matha.rwth-aachen.de/people/kloecker/index.en.html">Ingo Kloecker</a>, and <a href="http://www.kontact.org/shopping/sanders.php">Don Sanders</a> for their outstanding work on <a href="http://pim.kde.org/components/kmail.php">KMail</a>, and also has some words to say on <a href="http://kate.kde.org/">Kate</a>.



<!--break-->
