---
title: "KDE at LinuxTag 2005: Summary and Technologies"
date:    2005-06-29
authors:
  - "jspillner"
slug:    kde-linuxtag-2005-summary-and-technologies
comments:
  - subject: "By the way"
    date: 2005-06-29
    body: "What software does the dot use and is it open source?"
    author: "gerd"
  - subject: "Re: By the way"
    date: 2005-06-29
    body: "Easy to answer because it's written on dot.kde.org: \"KDE Dot News is based on Squishdot which runs on Zope using Python technology.\""
    author: "Anonymous"
  - subject: "Re: By the way"
    date: 2005-06-29
    body: "Okay, squishdot, will have a look.\n\nThere are two wonderful sites, Prolinux and KDE-Dot."
    author: "gerd"
  - subject: "KDEMM"
    date: 2005-06-29
    body: " I'm very glad that you made the design decisions for KDEMM that you did. This really looks like no-nonsense development to me.\n\n Pros:\n It's advanced enough to be actually usefull.\n It's not just the knotify-must-have-hack it initially sounded to be.\n It's lightweight enough to be fast (depending on backend that is).\n It looks much more cross-platform freindly.\n\n Cons:\n Cons?!?... We don't need no stinkin' cons! ;-)\n\nThanks in advance! I just as excited about KDE4 as I was about KDE2.. Which says a whole lot! :-)\n\n~Macavity"
    author: "macavity"
  - subject: "Re: KDEMM"
    date: 2005-06-29
    body: "Cons: It's an abstraction library around... an abstraction library."
    author: "Robert"
  - subject: "Re: KDEMM"
    date: 2005-06-29
    body: "And that is negative because...?\n\nActually for many media frameworks it would make sense to provide a layered API themselves, because the \"low level\" API that media frameworks provide give most developers more freedom and possibilities than they need. Another API layer above that can make usage of the media framework a lot easier without a noticable performance loss.\n\nBut of course that all depends on what you want to develop. And that's why KDEMM doesn't target pro-audio application developers."
    author: "Vir"
  - subject: "Re: KDEMM"
    date: 2005-06-30
    body: "Actually that's what most libraries are.  It's not like most software is designed such that you have exactly one level above the hardware.\n\nLet's look at the drawing portions of Qt:\n\nHardware -> Kernel Driver Collection -> Device Abstractions -> Xlib -> Qt (oversimplified with a few levels missing)\n\nI'm not advocating needless levels of abstraction, but you need to consider who your API is for and create an API that fits your target audience.  We never debated if we'd have a \"KDE abstraction\" for KDE 4 -- the debate was simply if that would be a binding to a specific backend or if it would be a more generic wrapper."
    author: "Scott Wheeler"
  - subject: "German translation"
    date: 2005-06-29
    body: "\nWell, the whole translation framework came to a standstill, no real innovation, no real infrastructure that scales. Recruiting new translators had obviously low priority. So it is very nice to see the Germans acting here to get more people on board."
    author: "martin ben"
  - subject: "KPresenter"
    date: 2005-06-29
    body: "I'm looking at the KDE Multimedia Roadmap (server seems really bogged down right now), and noticed it was made in KPresenter.  I never realized how nice the pages it made were (when exported to HTML)!  Much better than that abomination that PowerPoint outputs!"
    author: "Corbin"
  - subject: "Re: KPresenter"
    date: 2005-06-29
    body: " Uhm.. the main picture is just a jpeg. I don't even think you kan make something that good looking in straight html (that is: without using tons of smaller bitmaps or svgs)\n\n But yes, it struck me to how nice looking it all was :-)\n\n>server seems really bogged down right now\n\nThe point of KDEMM is not to be the all-singing-all-dancing-multimedia library, rather to be simple but effective for everyday small and medium tasks. Stuff that goes beyond this should either use the common libs in the kde-multimedia package, or do it them selves (like amarok). The latter wont be nearly as cumbersome when aRts doesnt lock up the device for apps who wish to talk directly to ALSA/OSS/SunSound (or what its name is)\n\n~Macavity"
    author: "macavity"
  - subject: "Re: KPresenter"
    date: 2005-06-29
    body: "dooh.. wasnt quite awake there X-/"
    author: "macavity"
  - subject: "poor server"
    date: 2005-06-29
    body: "> server seems really bogged down right now\n\nYes, that's an old 200MHz Laptop with 64MB of RAM on a DSL line with 128kBit uplink. I wasn't prepared for that much publicity. :-)\nI'm looking into it and might need to replace that memory hog of apache2 with something lighter. But I actually don't really understand why it is that slow because the load average is around 0.1.\n\nBTW, those slide images were originally PNGs (6MB). I changed that to low compression JPEGs (2MB) and moved them to my Uni's webspace (else my server probably wouldn't even be reachable for you anymore)."
    author: "Vir"
---
As <a href="http://www.linuxtag.org/">LinuxTag 2005</a> is over, many KDE contributors went home to implement ideas
that were discussed during the event. In case you got lost among all of the
announcements, here's an easy-to-click-through list of new features.
Before reading, you might want to have a look at <a href="http://www.kstuff.org/lt2k5/day2-4/">the pictures</a>, and there are
now more added daily to the <a href="http://www.infodrom.org/Debian/events/LinuxTag2005/pictures.html">community photo list</a>.




<!--break-->
<p>
Let's start the list with the announcement of probably the biggest impact,
the <a href="http://meta.wikimedia.org/wiki/KDE_and_Wikipedia">cooperation between
KDE and Wikipedia</a>. From community news sites like <a href="http://www.pro-linux.de/news/2005/8304.html">Pro-Linux</a>
to <a href="http://slashdot.org/articles/05/06/24/149231.shtml">Slashdot</a> and technical mailing lists such as
<a href="http://lists.debian.org/debian-edu/2005/06/msg00346.html">debian-edu</a> discussions sparked and
led to <a href="http://websvn.kde.org/trunk/KDE/kdeedu/kalzium/TODO?rev=428530&r1=427619&r2=428530">first  improvements</a>
in the source code.
</p>

<p>
A related topic is the introduction of web services into KDE. Some development code already exists
for the <a href="http://ghns.berlios.de/dxs/">Desktop Exchange Service (DXS)</a>, a web service transport
to be included into KNewStuff, and Kung, a KDE frontend for dynamic web service navigation. Find out more
at the <a href="http://kstuff.org/webservices/">web services section</a> on <a href="http://kstuff.org/">KStuff.org</a>.
</p>

<p>
During the booth service a lot of new potential contributors presented themselves to the project,
especially in areas where there is a need of such, like the
<a href="http://i18n.kde.org/teams/de/">German translation team</a>. As the demopoints were equipped
with SUSE Linux 9.3 and Kubuntu, and we distributed about 500 Kubuntu CDs, many old and new users of
KDE 3.4 told us of ideas of improvement, some of which are already implemented, while others will be soon,
as is the case with <a href="http://vir.homelinux.org/talks/kdemm/lt2005/">KDE 4 multimedia</a>.
</p>

<p>
The involved KDE contributors would like to say a big thank you to:
</p>
<ul>
<li><a href="http://www.iiyama.de/">Iiyama</a>, who lent us 4 large displays</li>
<li><a href="http://www.transtec.de/">Transtec</a> who helped us organising the hardware</li>
<li><a href="http://www.credativ.de/">credativ</a> who helped out a lot, including transportation</li>
<li><a href="http://www.canonical.com/">Canonical</a> for sponsoring the Kubuntu CDs</li>
<li><a href="http://www.linuxtag.org/">LinuxTag e.V.</a> for making the event possible</li>
<li><a href="http://www.infodrom.org/">joey</a>, the diligent guy who managed all free booths</li>
<li>and finally <a href="http://www.kde-ev.org/">KDE e.V.</a> for backing the organisation.</li>
</ul>



