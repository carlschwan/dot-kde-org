---
title: "Shuttleworth Affirms Commitment to Kubuntu"
date:    2005-11-15
authors:
  - "numanee"
slug:    shuttleworth-affirms-commitment-kubuntu
comments:
  - subject: "Looks good"
    date: 2005-11-15
    body: "The fonts look great.  Why do KDE or Gnome fonts in screenshots always look like ass?  Don't people take time to mess with their hinting?   The theme is better than that disgusting brown that Gnome gets.    Maybe Ubuntu changed it to a different theme recently, I gave up with Ubuntu after realizing I hated the freeze/development cycle.  I'm on Kanotix now which does *everything*, including even native drivers for my rtl8180 wireless, out of the box.  \n\nAll in all, it's a good thing for KDE.  How many developers has he hired to work on it full time?"
    author: "Rick"
  - subject: "Re: Looks good"
    date: 2005-11-15
    body: "just a few, afaik. but at least he will be sponsoring the cd's. maybe he now realizes no matter how much money you spend on gnome, it won't get much better than KDE, thanx to the great community developing KDE :D\n\nanyway, i hope Kubuntu gets things right. it looks great, and i think they will be able to get a great KDE 3.5 togheter for (K)ubuntu 6.04!"
    author: "superstoned"
  - subject: "Re: Looks good"
    date: 2005-11-15
    body: "you can get already KDE 3.5-RC1 packages for breezy badger."
    author: "void"
  - subject: "Re: Looks good"
    date: 2005-12-02
    body: "I agree. I've used KDE since Mandrake first appeared and freed me from Red Hat's religious dedication to the beginnings of Gnome.\n\nI found Debian's package management through Mepis, then discovered Kanotix. I think Kano's additions to Knoppix put him streets ahead of the pack. I tried Ubuntu and Kubuntu recently. It was like stepping back in time and I spent a dreadful week trying to get my system running as well as Kanotix does in two minutes. They may catch up in a couple of years or so, but I think the duplication of effort is a waste of time and confusing to newbies.\n\nI admire what Mark Shuttleworth is doing for the community, but think we'd be better off if he simply gave away free Kanotix disks."
    author: "Queensland Bob"
  - subject: "The ugly underline"
    date: 2005-11-15
    body: "I thought that the ugly line under every filename in Konqueror had been removed in 3.5. It looks like it is still there. Huh."
    author: "Mr Bla"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "Sorry bud! But I can not confirm this. Been using KDE from SVN 3.5 atm. and I don't have these lines under filename as you can see in my Screenshot."
    author: "ac"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "I see, thought the Kubuntu screenshoot was with 3.5. By the way what theme is that you are using?"
    author: "Mr Bla"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "Seems the default one to me (Plastik)."
    author: "ac"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "Default Plastik Theme, Nuvola Icon Theme, Grounation Mouse Theme.\n\nhttp://img498.imageshack.us/my.php?image=snapshot39kg.png"
    author: "ac"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "I always modify the look making the location bar share space with other bars. I cannot imagine just one bar covering the entire area. Take a look.\n\nCb.."
    author: "This time I want to help out..."
  - subject: "Re: The ugly underline"
    date: 2005-11-18
    body: "I cannot imagine using more than one bar or text area. Konq web shortcuts make the extra \"search\" text area redundant. "
    author: "SubAtomic"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "Using KDE 3.5 RC1 Mandrake Cooker\n\nKontrol Centre > Components > File Manager\nRemove Check Mark against Underline Filenames\nApply\n\nThe deed is done!\n\nEach to their own. I have it on by default. Fits nicely with my single-click to open files and folders setting"
    author: "The Dude"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "Thanks for the info.  I had yet to get that far in the Control Center so jumped ahead to take care of it.  I think it looks cleaner, that way.\n"
    author: "Yogich"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "The default version of KDE in Kubuntu 5.10 is 3.4.3 - so any changes in 3.5 will not have made it there yet.\n\nL."
    author: "ltmon"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "I'm fairly sure this is an option since back in KDE2."
    author: "teatime"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "I'm also quite sure that nobody has found out about it :)\n\nNo, just kidding. "
    author: "ac"
  - subject: "Re: The ugly underline"
    date: 2005-11-15
    body: "The underline is actually an option - In Konqueror, go to Settings -> Configure Konqueror -> Appearance. Remove the check from 'underline filenames'"
    author: "David"
  - subject: "Re: The ugly underline"
    date: 2005-11-18
    body: "You can disable it already in 3.4. Check konqueror settings."
    author: "Shulai"
  - subject: "Re: The ugly underline"
    date: 2005-12-03
    body: "The ugly underline can be so easily removed by changing the settings in konqueror:)"
    author: "elijah"
  - subject: "AMD64 packages"
    date: 2005-11-15
    body: "I have installed Kubuntu 5.10 for AMD64.\n\nI would like to update to KDE 3.5 when it has been released , but i saw that the KDE 3.5 RC1 packages for Kubuntu are not available to AMD64 , only for i386 :\nhttp://www.kubuntu.org/announcements/kde-35rc1.php\n\nAnyone knows if KDE 3.5 would have Kubuntu packages for AMD64?\n"
    author: "Marlo"
  - subject: "Re: AMD64 packages"
    date: 2005-11-15
    body: "kde 3.5 is not released yet, so better wait until it is officially available"
    author: "rinse"
  - subject: "Re: AMD64 packages"
    date: 2005-11-15
    body: "Last I heard was that they were still building AMD64 builds for the release candidate."
    author: "Jonathan Jesse"
  - subject: "2 questions about kubuntu"
    date: 2005-11-15
    body: "why kdevelop and digikam are not packaged?\ndoes KDE 3.5 rc1 still have issues with arts?\n\nother than that, thanx for the great work."
    author: "Patcito"
  - subject: "Re: 2 questions about kubuntu"
    date: 2005-11-16
    body: "I believe Ubuntu and Kubuntu are mostly marketed to new GNU/Linux users, and if this is true then it explains the absence of KDevelop."
    author: "NSK"
  - subject: "Re: 2 questions about kubuntu"
    date: 2005-11-18
    body: "I believe you will need the universe repository for kdevelop (package kdevelop3).\n\nRegards\n"
    author: "Walter Plinge"
---
In some good news for KDE, <a href="http://www.markshuttleworth.com/">Mark Shuttleworth</a>, the famous African entrepreneur, announced at the <a href="http://wiki.ubuntu.com/UbuntuBelowZero">Ubuntu Below Zero conference</a> that he is now using the  <a href="http://www.kubuntu.org/">Kubuntu</a> distribution (<a href="http://shots.osdir.com/slideshows/slideshow.php?release=466&slide=8&title=kubuntu+5.10+rc+screenshots">screenshots</a>) on his own desktop machine and <a href="http://www.kubuntu.org/announcements/kde-commitment.php">affirmed his commitment</a> to the KDE-based distribution.  
As per the website, <i>"Kubuntu is a user friendly operating system based on KDE, the K Desktop Environment. With a predictable 6 month release cycle part of the Ubuntu project, Kubuntu is the GNU/Linux distribution for everyone."</i>  Kubuntu has apparently been prominent throughout the conference with a large number of Kubuntu users having shown up. As a first class Ubuntu citizen, CD copies of Kubuntu will be freely available to users. Congratulations to the <a href="https://wiki.ubuntu.com/KubuntuPeople">Kubuntu team</a> for their dedication and hard work in bringing the KDE desktop to <a href="http://www.ubuntu.com/">Ubuntu</a>!













<!--break-->










