---
title: "KDE PIM Developers and OpenUsability on Cooperation"
date:    2005-05-13
authors:
  - "swheeler"
slug:    kde-pim-developers-and-openusability-cooperation
comments:
  - subject: "PIM"
    date: 2005-05-13
    body: "I. What I think is crucial for KDE-PIM is to get some integration with project mangement software. Something like Gantt (GPL) for Windows http://ganttproject.sourceforge.net/ It is still the missing tool.\n\nII. Usability: Get rid of popup dialogues of any kind, do it like firefox with its search option. \n\nIII. do not confuse us with 20 different KDE PIM projects and websites."
    author: "gerd"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "RE: getting rid of popups\n\nHELL NO!  Why on earth would you want to do that?  I _hate_ that aspect of firefox and do not want to see it duplicated."
    author: "Dude"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Popups just annoy, you have to click them away or use some keystroke, it is 100% unnecassary and even worth when you get several popup windows.\n\n\n\n---\n\nbtw: http://freshmeat.net/projects/merlinosx/ is my favourite \n\n"
    author: "Martin"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Embedded popups (which is what Firefox search thingy really is) annoy, you have to use some keystroke to get them away, it is 100% unnecessary and takes up valuable screen realestate."
    author: "Dude"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "with the difference that they do not interfere with your current task, you have to click normal popups away to just continue with what you're doing. That's mainly the annoying part."
    author: "bsander"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Exactly. And it's not only a browser problem. E.g. Kate or Amarok are applications which unnecessarily invoke popup dialogs during startup, when they can't access a file for some reason. The better way is to log uncritical anomalies, indicate them passively and let the user choose to view the log, if he's really missing something."
    author: "Carlo"
  - subject: "Re: PIM"
    date: 2005-05-14
    body: "\"with the difference that they do not interfere with your current task, you have to click normal popups away to just continue with what you're doing. That's mainly the annoying part.\" \n\nIf you are searching for something and a search popup comes up, *searching* is your current task.  The popup does not interfere with it.  Maybe next you'll want the filesave dialog to appear at the bottom of the page instead of a popup, cause a popup would take your focus away from your task... of saving files\n\nAt least the search bar on firefox still lets me use my ctrl-f [type search] enter f3 f3 approach.  and hitting esc even removes it like the search dialog.. (though this creates its own problem.  If the bar focused, esc stops page-loading, and to get rid of the bar you have to move the mouse down to the bottom and press a stupid button.  That's OK I guess, but having the esc key do different things isn't all that cool) I guess it isn't so bad, but then I still don't think it is very much better.\n\nI can't say whether a noob would find it better, but I am used to search being a popup, and when I first used firefox I wondered why pressing search didn't seem to do anything (eventually noticed the bar)\n\nMeh, guess I've made my peace.  I could live with it (though I'd rather not), but there are so many actually pressing needs that could be worked on instead.."
    author: "MamiyaOtaru"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "When you search for several expressions firefox search is a lot of better.\n\nPopu windows \"popup\", bash you, slap you into the face and often take the focus of the window, so you cannot continue."
    author: "Gerd"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "The \"Firefox with its search option\" fails miserably in the \"Dad test\". He's always confused as to why nothing happens when he presses CRTL-f. I do rather like it myself. But usablity doesn't necesarily mean efficient to use, it means easy to use. Which means doing what the users are expecting.\n\n"
    author: "Ian Monroe"
  - subject: "Search in Firefox"
    date: 2005-05-13
    body: "When CTRL+F is pressed a Tooltip should show where to type the search keyword. that will bring attention to the search toolbar. Once a person got to know where to look for Search, it becomes easy onwards."
    author: "fr_dude"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Well, the gui changes visibly. I'd more say, Dad failes because he's trained to use stupid popup dialogs. It is still the dominating approach to force the user to use the software as the software tells him and not the other way around. I am convinced the firefox search option is more usable, since the functionality is just there as long as you need it."
    author: "Carlo"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "\"Usability\" has to take into consideration what people are already \"trained\" to do, no matter how stupid it is. If you separate pure usability from common and historical practise you alienate a large group of people for whom your usability improvement is not, in practise for them, an improvement. Further, you risk doing something really stupid without realising it, because it seems right in your mind, while the much of the rest of the world's mind is in a different place. For example, many infamous gnome interface decisions rammed down its users throats from on high.\n\nThis being said, i generally hate popups as well. I remember wordperfect (or lotus wordpro? i forget now) introduced spell check/thesaurus as panel that opens at the bottom of the document... that was a great interface innovation. No more stupid popup window covering the words, or moving around to avoid covering the words... brilliant and simple. And yet... didn't seem to catch on.\n\n"
    author: "Tim"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "> \"Usability\" has to take into consideration what people are already \"trained\" to do, no matter how stupid it is. \n\nOf course usability has to care how people perform tasks. In this case it is just not caring about that, but caring about how software forces people to do things and their familarity with it. It'd mean more or less to sacrifice a better idea of usabilty for a broken one... And it is really easy to figure out (something you have to do only once) how to use this feature. I bet that someone, who never used a popup dialog in such a context would wonder, if he'd be confronted with one."
    author: "Carlo"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "> I. What I think is crucial for KDE-PIM is to get some integration with project mangement software. Something like Gantt (GPL) for Windows http://ganttproject.sourceforge.net/ It is still the missing tool.\n \nAs soon as kplato http://www.koffice.org/kplato/ is released you get\nwhat you want.\n"
    author: "ac"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "GanttProject is a java software, and so is not dedicated to the Windows platform."
    author: "Christophe"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Thanks, I did not realise that. Just it only on windows until now."
    author: "Gerd"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "KPlato seems to be in very early stage of development. TaskJuggler (http://www.taskjuggler.org) is already usable."
    author: "Christophe"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Taskjuggler is not usable in a professional setting! And ktjview2 is a joke. Sorry. Usable tools for project managment aren't available under Linux! "
    author: "Dr. Joe"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "Indeed, I said \"Usable\" but I meant \"Does not crash\". TaskJuggler is not a complete solution yet, but IMHO it has a great potential."
    author: "Christophe"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "www.dotproject.com is. Unfortunately, it's web based(PHP), you need to run a webserver to run it. And access via Browser. I heard good things about MRProject, I think it's a GTK application..."
    author: "Ivan"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "MrProject is not usable at all. I think it's not even maintained anymore...\nI have completely given up to find a project managment tool for our linux platform. Maybe the dotproject thing would have been a good tool. Dunno.\nAnyway, we have now got MSProject. Unfortunately! But hey, there was NO alternative! "
    author: "Dr. Joe"
  - subject: "Re: PIM"
    date: 2005-05-13
    body: "> MrProject is not usable at all. I think it's not even maintained anymore.\n\nMrProject was renamed to Planner which is actively developed."
    author: "Anonymous"
  - subject: "Re: PIM"
    date: 2005-06-14
    body: "on the search functionality: there ought to be a poll on that: I find the firefox solution an absolute dream: I'd give my right arm to have that integrated into every kde window. It's a great aid to mouse-free working as well, which anybody who's interested in the productivity and ergonomics aspects should be concerned about. \nThe wretched Ctrl-F dialogue box is always where you don't want it, usually over the term you're looking for...\nThe sound features with Ctrl-F in Firefox and also more than just a gas - they communicate something..."
    author: "kepler2"
  - subject: "And while we are at it why not change the name"
    date: 2005-05-13
    body: "Speaking of being useable why not change the name. PIM being an acronymn for three overly used words on computers. \n\nPersonal (as in PC - so 1970's) \nInformation (except for gaming the whole friggen industry is about information)\nManagement (Also says nothing)\n\nCome on - there's gotta be a better name then PIM\n\n"
    author: "rasin"
  - subject: "Re: And while we are at it why not change the name"
    date: 2005-05-13
    body: "How about name it P1M, in the spirit of a11y, i18n and l10n?"
    author: "anon"
  - subject: "Re: And while we are at it why not change the name"
    date: 2005-05-13
    body: "Hi,\n\nwe already had long, long threads on kde-pim@kde.org and kde-core-devel@kde.org about this naming issue\nand came to no solution...\n\nCiao,\nTobias\n"
    author: "Tobias K\u00f6nig"
  - subject: "Re: And while we are at it why not change the name"
    date: 2005-05-13
    body: "I wonder why you didn't propose a better name. Could you probably not come up with a better one? Anyway, there's already a better name for it: Kontact.\n\nNote that pim.kde.org is the website for developers while kontact.kde.org is the website for users."
    author: "Ingo Kl\u00f6cker"
  - subject: "Most horrible KMail usability thing..."
    date: 2005-05-13
    body: "...IMHO:\n\nis the fact that there is no way AFAIK to prevent the columns in\nyour in-box from being clickable. It would be just great if you\nright click them and say \"lock\" once and forever.\nI just never want my in-box to _not_ be sorted date and because\nI like the most recent mails to appear on top (\"reverse sorting\") I\nclick on the columns accidentally quite often when trying to click\nthe latest incoming subject line. I know lots of other people (colleagues, my mum) who complain about this - it's admittedly not only a problem with KMail, though. \n"
    author: "Martin"
  - subject: "Re: Most horrible KMail usability thing..."
    date: 2005-05-13
    body: "It's hardly KMail's problem if you \"click on the columns [headers] accidentally quite often when trying to click the latest incoming subject line\". Anyway, if you have a feature request then go to bugs.kde.org not dot.kde.org. If you already did so, great. If not, then how were we supposed to know about this."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Most horrible KMail usability thing..."
    date: 2005-05-14
    body: "> It's hardly KMail's problem if you \"click on the columns [headers] accidentally quite often when trying to click the latest incoming subject line\".\n\nYes it is.  It places the \"re-sort\" action directly adjacent to the \"select message\" action.  When KMail deliberately puts you in the position of re-sorting your whole inbox if you are a SINGLE PIXEL out of place, then it is indeed KMail's fault.\n\nAnd it happens to me too, and it's quite annoying.  I'd hate to think what it's like for somebody who has difficulty using a mouse (accessibility isn't just about catering to the blind, you know).\n\n> If not, then how were we supposed to know about this.\n\nIt's a common usability error.\n"
    author: "Jim"
  - subject: "Re: Most horrible KMail usability thing..."
    date: 2005-05-14
    body: "Indeed. If it's really a usability error then it's a fairly common one since basically all applications showing sortable information in columns behave like this. I just started Evolution 1.4.6 and Thunderbird 0.5 (both included in SuSE 9.2) and didn't find an intuitive way to lock the sorting column. (Maybe it's still possible to lock the sorting column. I just looked for this option in the context menu of the column headers.)\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "KMail keyboard handling disaster"
    date: 2005-05-13
    body: "Funy to talk about KDE PIM usability when KMail BY DEFAULT does not respect even the basic keyboard navigation habits in the mail listview like:\n\n- UpArrow, DownArrow: does not bring you to previous/next message\n- Home, End: does not bring you to top/bottom of the list\n- Enter: does not open selected message\n- Shift DownArrow/UpArrow: does not add next/previous message to selection\n\nAll other mail programs behave correctly, all other KDE apps behave correctly. Why not KMail?"
    author: "Krystof Zacek"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "KMail's keyboard navigation is focused on efficiency and therefore we ignore which pane has the focus. Of course, this means that you have to learn some other keys especially for KMail, but we think it's definitely worth it. Having that said, we will very likely (we all have very limited time so we can't make any definite promises) give the user the choice to choose between a focus oriented keyboard mode and the current much more efficient keyboard mode. But that won't happen before KDE 4.\n\nYou surely know this, but nevertheless:\nhttp://docs.kde.org/en/3.4/kdepim/kmail/keyboard-shortcuts.html\n\nAnd Enter should work. In fact it does work in standalone KMail, but it does not work in Kontact. There seems to be a problem with the initialization of this action. If you check Settings->Configure Shortcuts then you'll see that the Return key is bound to the Display Message action. And if you bind another key to this action, click OK and then change it back to Return then it suddenly works. As workaround you can add the Display Message action to the toolbar (Settings->Configure Toolbars, choose \"Main Toolbar <kmmainwin>\", look for \"Display Message\", click on the right arrow and then on OK). Since we don't have a nice icon for this action you'll get an \"unknown\" icon, but at least Enter works now in Kontact.\n\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "Efficiency versus usability = exactly this is the problem of KMail. KMail is sacrificing usability to efficiency.\n\nIt would be OK for specialized programs like Blender, Photoshop. But not for KMail which is the standard mailer for KDE.\n\nSince, as I hope, KDE will spread more and more among average users, we should rather regard normal users and do the least surprising things in our programs. Users have already learned keyboard navigation from other programs like Konqueror. Why force them to learn new ones for one single program?\nIn KMail the STANDARD keyboard behaviour  should be DEFAULT. Power users will still be able to reconfigure the program to their needs.\n\nThere is a nice book from Eric Raymond on UNIX programming saying: prefer simplicity to clever hacks, always do the least surprising things, keep it stupid simple.\n\nRegards\nKrystof"
    author: "Krystof Zacek"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "> There is a nice book from Eric Raymond on UNIX programming saying: [...]\n\nOh, you mean the one in which he praises kmail's usability aspects?  :-P"
    author: "Melchior FRANZ"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "KMail replaces the bad usability of MDI for the better usability of displaying in MDI but navigating in SDI, the best of both worlds."
    author: "ac"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "Hehe! KMail rather replaces KDE standards with its own ones :-)"
    author: "Krystof Zacek"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "Nope. Actually KMail set the standard in KDE. KDE's guideline forbid MDI interfaces, but mailing apps works the best with the three panes approach. So KMail, being the first KDE app facing this issue, solved this by making all panes accessible the same way everywhere (the SDI approach) instead letting the user have to tab through them (the MDI approach).\n\nThe only issue I see is that this solution never had been added to the guidelines (so plenty newer KDE apps unfortunately use the MDI approach) and that the GUI visuals in KMail sometimes (still) don't reflect correctly that the mail view always is the primary view in the app (ie. directory keys without modifiers works and not in the other panes)."
    author: "ac"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-14
    body: "What's wrong with MDI?  Are you against tabbed browsing?  Should IDEs open each source file in a new window?  Should we not have multiple sessions in Konsole?  I just don't find it that terrible to keep related tasks grouped in a single window..\n\nWhen I'm editing code, or browsing the web, I don't need a seperate taskbar entry for each file/page.  It's great to have fewer taskbar entries so I can actually find the app and can then easily get to whatever document I want.  "
    author: "MamiyaOtaru"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "Actually, I think kmail may not go **enough** in that direction.\n\nI have never been as happy with a mailer as with pine. Everything was one key away. And I don't mean a key combination, but a *key*."
    author: "Roberto Alsina"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "I think you are confusing Usability and expectation.  They are not necessarily the same thing.  Kmail is usable but Kontact is not.  The reason I say this is that each module behaves differently from the other.  For instance keyboard navigation in Kmail and Knode are different which is irritating.  The new Akregator modules is a nightmare from a usability stance, the articles should be in the top pane like Kmail and Knode and the bottom pane should be done as tabs, and I shouldn't have to open Akregator before it starts doing its thing, it should automatically start working when I open Kontact.  Setting up contacts in Kontact is very difficult as you have to select some resource but it doesn't make sense what you are trying to select.  The Journal and ToDo could also be cleaner.  And using the same storage for Knotes and Notes in Kontact is cool but they are different interfaces so one has Rich Text while the other doesn't.  Oh and the fact that not all types can have some information in the Summary is a little annoying.  The news feeds in the summary come from someplace else other than Akregator which I can't keep in the summary. What would be interesting is something similar to the Lotus Notes active summary feature where I can actually work from the summary itself.  \n\nSo the issue really isn't Kmail, it is the integration of the solution that is the problem.  I actually like Kmail's keyboard navigation as it is today.  I am able to navigate from the keyboard through my messages rather quickly.  This isn't to say it couldn't be better.  One thing I would like is the ability to have the message pane to be used for composing new messages instead of popping up a separate window.  But that is me.   \n\nAnd no I haven't posted all these as wishes/bugs or whatever because right now I am rather pleased with the progress that is being done.  Since to my understanding this is only the 3rd iteration of Kontact I think it has come a long way. But again Usability isn't about using what someone else decided the interface should do, it is about intuitive interfaces, and common function across a single application, and like it or not Kontact is seen as a single application.  As long as the keyboard shortcuts are documented, I don't see any issue with the way they work today. "
    author: "Anthony Moulen"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-14
    body: "Obviously you have never used a mailer that works like you think they should.   I use a Windows based mailer at work (Calypso), that works like you say.   The interface sucks, as I never know if pressing page down will bring me down a page in the message, or to a different message.   It varies depending on which pane is selected.   Delete only works when the subject pane is selected, but in that pane I cannot scroll the message. \n\nIt is consistant.   It also doesn't work they way someone reading more than one email (as in checking email when you first arrive in the morning) needs it to work."
    author: "Hank Miller"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-13
    body: "> KMail's keyboard navigation is focused on efficiency \n\nTo say it bluntly: No, it's just non-standard bullshit. :)"
    author: "Carlo"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-14
    body: "Since you feel so strongly about it, why not suggest a scheme that meets the following criteria:\n\n1. Throw your mouse away.\n2. Single keystrokes that will scroll between messages (left right arrow) and scroll the message (up down arrow)\n3. Keystrokes that will select folders. (ctrl up down arrow)\n4. Keystrokes that will change folders. (ctrl space)\n\nBzzzt. You suggested the tab key, which will add at least one or two keystrokes for common actions, and add the necessity of having some visual focus indication, taking away space from what I need my email client to do: show me emails.\n\nThere is no standard used in email clients. This is one example where the user interface elements just don't work and if applied religiously make the application unusable.\n\nOh, and when you are done, send me your address so I can send my medical bills for sore wrists.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-14
    body: ">There is no standard used in email clients.\n\nThere's no desktop standard for shortcuts. And by standard I mean abstraction of the functionality from the real shortcuts, common to Gnome, KDE,... I don't need (new) legacy shortcut systems for every single application because the developer thinks it's worth it. I want desktop wide profiles, the user can chose between. Even the broken KMail one, if you're fine with it.\n\n\n>Oh, and when you are done, send me your address so I can send my medical bills for sore wrists.\n\nWhen I can send mine to you, because KMail is not usable without the mouse for me!?"
    author: "Carlo"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-14
    body: ">2. Single keystrokes that will scroll between messages (left right arrow)\n\nI'm not using kmail so I don't know if the problem can happen or not, but if you use left and right arrow to select a message, how do you horizontal scrolling inside your email?\n\nI think here the basic problem is that there is a lack of 'standard keys' to say go to next/previous message in the thread, go to next/previous thread, go to next/previous message (ignoring threads).\n\nThis problem is not linked to email only, newsgroup readers have the same problems.."
    author: "renox"
  - subject: "Re: KMail keyboard handling disaster"
    date: 2005-05-14
    body: "I love the KMai approach. It remainds a lot of mutt and pine. It's really easy and intuitive. "
    author: "Petteri"
  - subject: "Sounds great!"
    date: 2005-05-13
    body: "This is great news IMHO. \n\nI would like to thank all the usability experts for helping out making KDE better!\n\nOf course, there would be nothing to help with if it wasn't for all the great KDE hackers ;)"
    author: "Joergen Ramskov"
  - subject: "Accessible then Usable"
    date: 2005-05-14
    body: "KDE lacks \"onscreen keyboard\" which is required for my aunt who can't type clicking with onscreen keyboard will help her get her work done by herself."
    author: "fr_dude"
  - subject: "Re: Accessible then Usable"
    date: 2005-05-14
    body: "Viki is a KDE onscreen keyboard."
    author: "Anonymous"
---
The folks from Newsforge are <a href="http://programming.newsforge.com/programming/05/05/05/1823209.shtml?tid=25&tid=130">covering</a>  some of the recent cooperation between the usability folks from <a href="http://www.openusability.org/">OpenUsability</a> and the <a href="http://pim.kde.org/">KDE PIM</a> developers.  PIM developers Cornelius Schumacher and Till Adam along with Ellen Reitmayr from OpenUsability talked about some of the usability that stemmed from the KDE PIM usability review which started at aKademy last year.  Many of these changes are already visible in KDE 3.4.  They also discuss some of the challenges that had to be overcome to make it possible for the teams to cooperate and some of the things that the future has in store for usability work inside of KDE.


<!--break-->
