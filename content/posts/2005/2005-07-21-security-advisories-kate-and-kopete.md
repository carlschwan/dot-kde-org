---
title: "Security: Advisories for Kate and Kopete"
date:    2005-07-21
authors:
  - "binner"
slug:    security-advisories-kate-and-kopete
comments:
  - subject: "Ok..."
    date: 2005-07-21
    body: "So lets wait for the Service Pack. :)\n\n"
    author: "CJ"
  - subject: "what is gadu-gadu"
    date: 2005-07-21
    body: "and are there really people using it?\n"
    author: "lemmy"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-21
    body: "It's quite popular in eastern Europe, or so I am told."
    author: "Andre Somers"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-25
    body: "I use Kopete a bunch to talk with those on the MSN network, but no, I have not heard of Gadu Gadu. MSN, IRC, Jabber, Yahoo!, and AIM are what I know (yes, Kopete does not do IRC, Konversation does, but it's what I know.)"
    author: "SuSE_User"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-25
    body: "> yes, Kopete does not do IRC\n\nIt does. \n"
    author: "cm"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-21
    body: "http://en.wikipedia.org/wiki/Gadu_Gadu\n\nFew millions of (mostly) teenagers use that proprietary protocol. Service owners are making business by pushing advertisements aimed mostly at kids. Since their server is centralized, it crashes sometimes. In fact nobody knows why this is so popular then; I suppose teenagers are more happy with their displays filled with ads... :)\nHistorical reasons are most obvious answer though..."
    author: "bungy"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "ads?\ntalk to millions of stupid dumb people that use aol's instant messenger that has least features, has ugliest and dumbest interface, and has the most annoying sound out of all messengers. yet there are tons of people that use it?\nperhaps this is telling us something - most people are real morons :)\n\nif you don't agree, go back and look at icq. it was the best messenger and the original one. it had tons of features. it had a kick ass interface. aol bought it and they screwed up the whole thing...now its nothing more than stupid aol.\nonly thing that icq has still that is better than any other messenger out there is the fact that it uses unique numbers instead of nick names. so you can pick whichever name and nick you want and you don't have to remember dumb nick names such si \")($mda9#lad_91291\". \n\nplus icq has badass search features. you can search by country, city, state, zip code, sex!, interests, etc, etc. in aol you can only search by that stupid nickname. so retarded.\n\nand yes, i on purpose did not use any capitalization in this post. i figure that more aol users will understand this better that way. i couldn't bare not to use punctuation though!"
    author: "disapointed user"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "Um, you don't pick your IM protocol, your friends pick it for you.\n\nWhich is why its nice to have clients like Kopete. :)"
    author: "Ian Monroe"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "not always right. I'm partly successful in convincing my friends to switch to jabber. :-)\n\nbut it's surely good to have kopete to chat to the rest I haven't convinced yet..."
    author: "ac"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "> perhaps this is telling us something - most people are real morons :)\n\nI know you're just making a crack here, but since this is a common sentiment, it deserves debunking.\n\nMost people don't care about technology at the level that say, readers of the Dot do.  I'm sure auto mechanics sit around and talk about how stupid most people are when they can't figure out that the X-Blah '05's engine is a piece of crap.  But those people (i.e. most of us) aren't morons, they just don't care.\n\nIt's the same with messaging protocols or operating systems or desktop environments or whatever.  Most people just don't care.  People generally don't ask, \"Do I have access to the source?\" or \"What's the user name storage scheme?\"  It's more like, \"Can I talk to Bob using that?\" or \"Can I open the things my mom sends me?\""
    author: "Scott Wheeler"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "Not to forget: \"PC magazin XY said that the app is cool and it even had the current version on CD\"."
    author: "Christian Loose"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "I use it.\n\nIt has many features that ICQ is praised below: unique numbers as user ID (unfortunately, recently they have started reusing dead numbers, I've been bitten by it (talking to somebody I've thought was my long-unseen friend)), searching by sex, age, location, first name AND the nickname.\n\nTeenagers are fun sometimes, too ;->"
    author: "divide"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "s/below/above/\n\nAlso, keep in mind that only the original client software shows the ads. All the others, including many open source ones (and there are many, multi-ones: Kopete, Miranda and dedicated: Kadu, EKG, Gnu Gadu, freegg; also there is Jabber transport for it, available, IIRC, on jabber.org.pl) don't do it.\n\nSmart masses use the unofficial ones, that goes without saying ;-)"
    author: "divide"
  - subject: "Re: what is gadu-gadu"
    date: 2005-07-22
    body: "Replying myself for the second time ;-)\n\nI forgot Gaim, of course.\n\nAlso cool feature related to gadu-gadu (though perhaps in a convoluted manner) is standard encryption, which is encapsulated in open-source library libsim (IIRC). Although it's hardly official (the original client doesn't offer encryption), most alternative clients have the support built-in; so that one can have secure chats (as in: not cleartext, at least; ensuring all authenticity, integrity, non-replayability and secrecy on an asynchronous medium IM is is not that easy, and libsim certainly has its deficiencies) without the need to mix and match clients and plugins to get them to cooperate."
    author: "divide"
---
Two <a href="http://www.kde.org/info/security/">security advisories</a> have been issued this week. The <a href="http://www.kde.org/info/security/advisory-20050718-1.txt">first affects Kate and KWrite</a> as shipped with KDE 3.2.x up to including 3.4.0: backup files are created during saving with default permissions, even if the original file had more strict permissions set. The <a href="http://www.kde.org/info/security/advisory-20050721-1.txt">second affects Kopete</a> as included in KDE 3.2.3 up to including KDE 3.4.1: the included copy of libgadu, if installed, can lead to integer overflows and remote DoS or arbitrary code execution.

<!--break-->
