---
title: "Linux Desktop Development and KDevelop Developers Conference 2005"
date:    2005-05-11
authors:
  - "adymo"
slug:    linux-desktop-development-and-kdevelop-developers-conference-2005
comments:
  - subject: "you don't need visa to travel to Ukraine"
    date: 2005-05-11
    body: "if you are from EU, you don't need visa to travel to Ukraine. The visa requirement was dropped for Eurovision. The KDevelop site is misleading.\n\nIf you are traveling by air, it may be better and safer to take a bus (just outside of the terminal), rather than \"unofficial\" cabby at the Boryspil airport. Have a nice stay in Kiev."
    author: "Sergey"
  - subject: "Re: you don't need visa to travel to Ukraine"
    date: 2005-05-11
    body: "Its a wiki, so you can fix it. :)"
    author: "Ian Monroe"
  - subject: "It&#180;s Kyiv, not Kiev"
    date: 2005-05-12
    body: "The correct transliteration of Ukraine's capital (according to ISO) is Kyiv. It's time to leave behind the Soviet past."
    author: "Mol"
  - subject: "Re: It&#180;s Kyiv, not Kiev"
    date: 2005-05-15
    body: "Heh, this meeting organize people (from emt.com.ua) \nwho actually hate ukrainian language, they actively \npromote in Ukraine different software .. in russian language.\n\nWhat do you expect?\n\n"
    author: "kde_ua"
  - subject: "Offtopic"
    date: 2005-05-12
    body: "I apologize of this off-topic post, but can someone tell me what's up with this?\n\nhttp://www.konsec.de/en/products/\n\nFrom the webpage:\n\n>---\n\nKONSEC Kolab Server\nKONSEC Kolab Server is a highly scalable, cluster-capable messaging and collaboration server that is based on the outstanding open source project, Kroupware. The KONSEC Kolab Server is delivered as a complete distribution. You, as our customer, will receive the service and support you require.\n\nKONSEC Kontakt\nKONSEC Kontakt is a groupware client for Linux/UNIX which is based on Kmail, Korganizer and Kaddressbook. This personal information manager allows the user functionality which is comparable to Microsoft\u00ae Outlook\u00ae. In addition, users will be able to collaborate with Microsoft\u00ae Outlook\u00ae users. All this using the award-winning KDE environment.\n\n>---\n\nWhat is this? A company listing Kolab and Kontact as one of their products?"
    author: "sorry"
  - subject: "Re: Offtopic"
    date: 2005-05-12
    body: "> What is this? A company listing Kolab and Kontact as one of their products?\n\nYes. And where's exactly the problem?\n\nThey can do this, as soon as they respect the licence (which they probably do) and they don't say they own the copyright (which they do: \"based on Kroupware\")."
    author: "MacBerry"
  - subject: "Re: Offtopic"
    date: 2005-05-12
    body: "The KONSEC Kolab Server looks like a specialized Linux(?) distribution, giving you a dedicated groupware server running Kolab. It also looks like they do a branded version of Kontakt. All this has been done, to some degree by others before. Companies like Suse, RedHat, Linspire etc."
    author: "Morty"
---
The <a href="http://www.kdevelop.org/">KDevelop Team</a> and <a href="http://www.osdn.org.ua/">Open Source Developers Network Ukraine</a> are proud to 
announce the <a href="http://www.kdevelop.org/mediawiki/index.php/Linux_Desktop_Development_and_KDevelop_Developers_Conference_2005">First Linux Desktop Development and KDevelop Developers 
Conference</a> that will be held in Kiev, Ukraine, 1st to 6th of July 2005.

<!--break-->
<p>
<b>The conference program:</b>
<ul>
    <li>talks and presentations by experts in Linux desktop development</li>
    <li>talks and workshops by prominent KDevelop developers covering various 
aspects of development with KDevelop IDE</li>
    <li>joint development for all KDevelop contributors and enthusiasts</li>
</ul>
</p>

<p>The conference language is English. Most important talks and workshops will be also held in Ukrainian.</p>
<p>
<b>Target audience:</b>
<ul>
    <li>all active KDevelop (and KDE) contributors</li>
    <li>developers interested in joining KDevelop IDE and/or platform 
development</li>
    <li>developers interested in Linux and Unix desktop development</li>
    <li>general public interested in development with and for Open Source 
systems</li>
</ul>

<p>
<b>Support:</b><br>
KDevelop Team is looking for sponsors to help us running the conference and 
provide accomodation for speakers and delegates from other countries/continents.
</p>
<p>
<b>Commited sponsors and supporters:</b><br>
<a href="http://www.kde-ev.org/">KDE e.V.</a><br>
<a href="http://www.sourcextreme.com/">SourceXtreme Inc.</a><br>
</p>
<p>
<b>Conference information:</b> agenda, registration, traveling and other possible information is collected at the <a href="http://www.kdevelop.org/mediawiki/index.php/Linux_Desktop_Development_and_KDevelop_Developers_Conference_2005">conference website</a>.
</p>




