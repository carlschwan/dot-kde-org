---
title: "amaroK 1.3 'Airborne' Takes Off"
date:    2005-08-18
authors:
  - "mkretschmann"
slug:    amarok-13-airborne-takes
comments:
  - subject: "Where is the dynamic playlist?"
    date: 2005-08-18
    body: "Is the Quemanager [1] what you call dynamic palylist? What kind of playlist is in the main window?\n\n[1] http://shots.osdir.com/slideshows/410_or/26.png"
    author: "testerus"
  - subject: "Re: Where is the dynamic playlist?"
    date: 2005-08-18
    body: "No, it isn't. Here is a <a href=\"http://amarok.kde.org/index.php?full=1&set_albumName=album03&id=16_G&option=com_gallery&Itemid=60&include=view_photo.php\">\nDynamic Mode screenshot</a>\n\n\n\n"
    author: "dina"
  - subject: "Re: Where is the dynamic playlist?"
    date: 2005-08-18
    body: "Excellent, your URL does nice things to the formatting. Could an editor delete this URL, please?\n"
    author: "Mark Kretschmann"
  - subject: "Re: Where is the dynamic playlist?"
    date: 2005-08-18
    body: "Done."
    author: "Daniel Molkentin"
  - subject: "Like Amaroka lot, but..."
    date: 2005-08-18
    body: "has there EVER been a truly stable Amarok release???\n\nI've been using it for a few months now, at the moment using 1.2.3. in Kubuntu - Amarok crashes quite often, plus it's darn slow when skipping tracks, where XMMS is lightening quick...\n\nI love KDE, but stuff like this is a major bugbear of mine..."
    author: "john"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "actually amaroK is here extra ordinary fast and stable\n\nso well, Kubuntu isn't bad, but it still has a ubuntu core, and ubuntu hates amaroK (or maybe it's just bullshit :/), dunno\n\nfact is that (k)ubuntu quite often makes problems with amaroK"
    author: "apachelogger"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Ubuntu also hates autopackage but this is what we need: http://www.autopackage.org/ui-vision.html"
    author: "Bertram"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Works perfectly on my Gentoo box. Maybe you should try a more mature distro?"
    author: "Anonymous"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Yes, blame the guy's distro, why doncha. Actually, amaroK was unstable as all hell at version 1.2.3 on my (Gentoo) box, as well. 1.3 seems to be doing fantastically well, however. Using the xine backend, for reference."
    author: "Antonio Salazar"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Having tried Kubuntu I can say it's not only AmaroK who has problems in Kubuntu. I've never experienced them while using Gentoo. Yeah, I know it's probably unfair to blame solely the distro, it just might be bad luck, but it was only a suggestion after all ;)"
    author: "Anonymous"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Hello,\n\nI'd also like to say that I have heard of people complain about amarok instability and I think it really comes down to the engine/configuration you use.  \n\nI use amarok on gentoo and it's the best player I have ever used.  It makes winamp look like a slow POS and that is GREAT. I don't think the person was flaming by saying that gentoo does things in such a way that it avoids many problems that your binary based distro does.\n\nJust sharing my opinion (I listen to music constantly all day everyday with a 10K song play list and I don't crash)\n\nPeace\n-mole"
    author: "mole"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-19
    body: "Xine is the engine to use with amaroK. aRts crashes after one song on my distribution- SuSE 9.3, certainly a \"mature\" distribution.  "
    author: "SuSE_User"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-19
    body: "Well, thanks, but I completely disagree.\nthe xine engine won't even COMPILE here...So, I use the gstreamer output. Works fine."
    author: "Joe"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: ">> has there EVER been a truly stable Amarok release???\n\nNot as far as I know. I have also been experiencing frequent crashes ever since 1.2.0 (currently using 1.3.0). I can only hope that the automatic trace e-mails end up solving these issues."
    author: "Elad"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Does Juk (or fill-in-your-soundapp) also crash? arts is not that stable, and may cause these apps to crash."
    author: "Bram Schoenmakers"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "Ditto for GStreamer. Haven't had any crashes since I switched to Xine."
    author: "Illissius"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "That's really the only problem with Kubuntu in regards to stability is that last I checked they used the aRts backend by default. And yes, I did file a bug. :)\n\nNot stability related, but they do confuse the heck out of our users by having musicbrainz enabled without MP3 support. I'd rather they would just remove musicbrainz altogether rather then distribute a shoddy version of it."
    author: "Ian Monroe"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "And if Juk(or fill-in-your-soundapp) don't crash perhaps you are using amaroK with one of the other backends? The Xine backend and gstreamer are not that stable, and may cause amaroK to crash."
    author: "Morty"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "> The Xine backend and gstreamer are not that stable, and may cause amaroK to crash.\n\nNow that's funny. No matter what backend I choose, it's always unstable and may cause amaroK to crash? Maybe the amaroK devs should put a big fat warning on the splash screen about that."
    author: "ac"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-19
    body: "No, if you choose an unstable backend it may cause amaroK to crash. Rather reasonable isn't it. Myself, I have never had any problems with aRts, on all the official versions I have used. Except some occasions with SVN/CVS and alpha or beta versions and long ago with some but not all versions of 2.0.x and 2.1.x. So I can't say why people constantly complain about it. On the other hand, I currently have a setup making the Xine backend crash and burn rather fast(Since I don't use it I don't bother to fix it or upgrade). And the few times I have bothered to try Gstreamer I have been far from impressed about it's stability.  "
    author: "Morty"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-24
    body: "\" No, if you choose an unstable backend it may cause amaroK to crash. Rather reasonable isn't it. \"\n\nWhat's reasonable is them not including a backend IF IT'S UNSTABLE. \n\nDon't be a wanker, wanker."
    author: "Joe"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-26
    body: "Man, I must say, your comments are always so constructive.\n\nKudos."
    author: "Dolio"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "This is a problem with the Ubuntu kernels (which Kubuntu shares). I finally fixed the same problem after finding the answer on Ubuntu forums.\n\nWherever you configure boot options (grub: /boot/grub/menu.lst: # nonaltoptions= ; lilo: /etc/lilo.conf??), add the kernel boot parameter 'noinotify'. The problem is with the interaction between inotify and amaroK.\n\n(amaroK was always quite stable on my Debian box. Now, it is just as stable on my Kubuntu box.)"
    author: "d w"
  - subject: "Re: Like Amarok a lot, but..."
    date: 2005-08-18
    body: "Yeah, but inotify is essential for having nautilus play nicely with the things happening to the filesytem elsewhere - and probably lots of other stuff... turning off inotify is stoopid, amorok should be fixed!!"
    author: "groooovious"
  - subject: "Re: Like Amarok a lot, but..."
    date: 2005-08-18
    body: "nautilus depends on a feature that isn't even part of a vanilla linux kernel release? Are you sure that you don't mix things up with dnotify or fam?"
    author: "ac"
  - subject: "inodify"
    date: 2005-08-18
    body: "Inodify will included in2.6.13 (currently it is a part of the rc's)."
    author: "freqmod"
  - subject: "Re: inodify"
    date: 2005-08-19
    body: "I know what fam does, and I have heard of inotify. What is the difference between the two? Both monitor the filesystem for changes, but what differences do they have?"
    author: "Janne"
  - subject: "Re: inodify"
    date: 2005-08-19
    body: "Well, how about gg:inotify?\n\nThere you would have found the following with a very nice explanation:\nhttp://www-128.ibm.com/developerworks/linux/library/l-inotify.html?ca=dgr-lnxw52Inotify"
    author: "ac"
  - subject: "Re: inodify"
    date: 2005-08-20
    body: "Well, that doesn't answer my question. That article talks about inotify and dnotify. My question was \"what's the difference between fam and inotify?\". nd to continue: what about fam and dnotify? there seems to be three tools for monitorin filesystems: inotify, dnotify and fam. How do those relate to each other and what are their differences?"
    author: "Janne"
  - subject: "Re: inodify"
    date: 2005-08-20
    body: "So you are learn resistant.\n\n\"gg:fam inotify dnotify\" results in:\n\n\"File alteration monitoring techniques under Linux\"\nhttp://www.devchannel.org/devtoolschannel/04/05/13/2146252.shtml\n\nWhich explains fam and dnotify. I guess we now got it, right?"
    author: "ac"
  - subject: "Re: inodify"
    date: 2005-08-20
    body: "Strange, I have googled for it in the past, but I didn't come across any sites that really explained it. Sure, there were sites about fam, about inotify and about dnotify, but no sites that really talked about the differences between them.\n\nAnyway, that site seems like it's something I have been looking for. Thanks!"
    author: "Janne"
  - subject: "Re: Like Amarok a lot, but..."
    date: 2005-08-19
    body: "amaroK doesn't do anything with inotify et all directly. It uses KDE methods for watching directories (which might in turn depend on inotify)."
    author: "Ian Monroe"
  - subject: "Re: Like Amaroka lot, but..."
    date: 2005-08-18
    body: "im using 1.3 in kubuntu with gstreamer and it hasnt crashed yet. amarok was only unstable for me at 1.1 and below. once it hit 1.2.x, it very very rarely crashes."
    author: "drizek"
  - subject: "Yammi-my choice"
    date: 2005-08-18
    body: "The juke box yammi may not be widely known.  I discovered\nit recently and found it to be the only application of \nits type that does the greatest number of tasks in the simplest \npossible gui.  It is also way faster than amarok and \nso simple and intuitive to use.  It is not yet a fully \ndeveloped kde app and lacks such features as systray support,\nvolume slide bar etc.  But it shows the way, how efficiently\na gui can be designed to do lot many tasks in the most simple and \nfastest way.\n\namarok has of late become cluttered, and the organization \nof the music is not yet the most efficient.  But that was what \nI used until I found yammi.  thanks to the developers.\n\nAsk\n"
    author: "Ask"
  - subject: "Re: Yammi-my choice"
    date: 2005-08-18
    body: "http://www.kde-apps.org/content/show.php?content=16399\n\nand this would be a less cluttered UI than amarok's one? are you serious?"
    author: "Davide Ferrari"
  - subject: "Re: Yammi-my choice"
    date: 2005-08-18
    body: "I just downloaded and installed yammi, and I am astounding that anyone could say the ui is less cluttered than amaroK.  Yammi has a few interesting features that amaroK lacks, like removeable media support and preview mode, but less cluttered and more simple and efficient?   What are you smoking?"
    author: "GMeyer"
  - subject: "Re: Yammi-my choice"
    date: 2005-08-18
    body: "For preview try the Intro-play script for Amarok.\n\nCharles\n"
    author: "Charles Philip Chan"
  - subject: "Re: Yammi-my choice"
    date: 2005-08-18
    body: "Hehe, whatever floats your boat. More power to you. ;)\n\nYammi is new to me. However I think that between amaroK and JuK most people's music playing styles are accommodated."
    author: "Ian Monroe"
  - subject: "wikipedia and kde locale"
    date: 2005-08-18
    body: "when searching for a wikipedia page amarok (or KDE future wp-API) should respect the current KDE users locale settings. If a page is e.g. in English and German I want to read the German one and do not want to read at the end:\n\nWikipedia Other Languages:\nDeutsch \nFran\u00e7ais \nL\u00ebtzebuergesch\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: wikipedia and kde locale"
    date: 2005-08-19
    body: "Dunno, I spose a lot of people running a non-English desktop would still prefer to use the English Wikipedia, since it's much more complete. So I'd rather make it so that you can switch the language in the app.\n"
    author: "Mark Kretschmann"
  - subject: "AmaroK never stops suprise me!"
    date: 2005-08-18
    body: "This is the first player that I find it joy to use. The music database is fast (I have ~ 25K songs in my), the wikipedia integration is sweet, the program is stable, fast and good looking. \n\nKudos to all the amaroK developers!"
    author: "Sam the man"
  - subject: "filtering lists"
    date: 2005-08-18
    body: "i hope they made filtering lists faster...\nif you have about 11k objects in your list, filtering freezes amarok for some secs.. very annoying... made me stop using the filter...\n\nand about amarok not being stable.... it only got stable for me, when compiling myself and using xine... gstreamer seems to be rather resource consuming (short freeze in sound before fade on many boxes i know) and arts... well you all know arts :D"
    author: "hannes hauswedell"
  - subject: "Re: filtering lists"
    date: 2005-08-19
    body: "Yea, amaroK isn't intended to have thousands of items in the playlist. This situation will improve with amaroK 2.0."
    author: "Ian Monroe"
  - subject: "Re: filtering lists"
    date: 2005-08-22
    body: "Is a decision regarding the release schedule of Amarok already made? Will there be an Amarok 1.4 or are you guys starting with 2.0? Will that include a Qt4 port?"
    author: "Bram Schoenmakers"
  - subject: "Re: filtering lists"
    date: 2005-08-23
    body: "Seems like i'm living in a different world!\nMy collection exceeds 13K, and the filter is the fastest query i've ever seen since a long time! BTW this is the feature that incourages me to use amarok!\nBy"
    author: "magatz"
  - subject: "Debian packages"
    date: 2005-08-18
    body: "Hello noticed that debian packages can be found here: http://goonie.us.es/~adeodato/temporary-location-of-my-blog/debian/27_amarok_1.3.html\nInstalled and that wikitab is good :)"
    author: "Petteri"
  - subject: "Reverted to 1.2.4"
    date: 2005-08-19
    body: "I reverted to 1.2.4 for the time being, because suddenly amarok started to name each playing track from a m3u file \"Radiostream\" and it did not use the title embedded in the m3u file anymore (when trying to play the m3u's on my own site: www.wilbertberendsen.nl/audio).\n\n1.2.4 does display the titles. I will try to sort out and report a bug if I can find a pattern :-)"
    author: "Wilbert"
  - subject: "Re: Reverted to 1.2.4"
    date: 2005-08-19
    body: "Yeah, I'm looking into this. It's a regression I caused when I fixed the Metadata-History thingie for xine.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Reverted to 1.2.4"
    date: 2005-08-19
    body: "Fixed :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: Reverted to 1.2.4"
    date: 2005-08-19
    body: "Great, you guys rock :-) but you knew eh :-D\n\nHow can I get the fix? Is there a patch or should I check out svn? (IUSE=\"gentoo\" :-) )"
    author: "Wilbert"
  - subject: "Re: Reverted to 1.2.4"
    date: 2005-08-19
    body: "You can use the get-amarok-svn.sh script from kde-apps.org. It fetches, builds, and installs amaroK SVN automatically.\n\nIf you got further questions, come around to #amarok on irc.freenode.net.\n"
    author: "Mark Kretschmann"
  - subject: "Just wish to say this:"
    date: 2005-08-20
    body: "Just want to say Amarok is the better Music Player I ever used - by a large gap.\nOn any OS.\nThank you"
    author: "Mario Lupi"
  - subject: "Stability and speed with large file counts"
    date: 2005-08-25
    body: "I've been using amaroK for over a year, but have found all releases since 1.2.0 to be completely unstable, including 1.3.0. The final straw for me came when amaroK locked my Slackware 10.1 desktop system.\nAfter starting to look at other applications to play my music I decided on a hunch to try amaroK with MySQL instead of SQLite (I had already tried compiling my own SQLite rather than using the built in version, but with no better results.)\nMySQL has transformed amaroK from being unstable and unresponsive to lightning fast and ultra stable. No crashes at all so far. This with amaroK 1.3.0.\nIf you have a very large file count and are having problems I'd recommend trying MySQL over SQLite."
    author: "Bud"
  - subject: "\"Airborne\" - so appropriate"
    date: 2005-08-29
    body: "Does that mean that when I give it a swift kick, it will sail a lot further away?\n\nI hope so."
    author: "Doug Laidlaw"
---
After four months of development, the <a href='http://amarok.kde.org'>amaroK</a> team has finished the new stable release of the popular KDE based audio player. amaroK 'Airborne' introduces the highly anticipated <a href="http://en.wikipedia.org">Wikipedia</a> lookup feature. The cooperation between KDE and the Wikipedia project has already been the topic of an <a href="http://dot.kde.org/1119552379/">earlier article</a>.





<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex">
<a href="http://static.kdenews.org/danimo/amarok_airborne.png">
<img width="150" height="120" src="http://static.kdenews.org/danimo/amarok_airborne_thumb.png" border="0"/><br />'Airborne' lifts you up!</a></div>
<p>
amaroK is one of the first applications to integrate live information from the free encyclopedia, providing useful facts about the music you are currently listening to. Discography of an artist, biography and even photos are just a mouseclick away with the new Wikipedia tab. 
</p>

<p>
Another major new feature is the integrated <a href='http://en.wikipedia.org/wiki/Podcast'>Podcast</a> support. Podcasts are Internet radio shows; amaroK can play them live from the Internet, or download to your harddisk to enjoy them later.
</p>

<p>
The new Dynamic Mode automatically updates your playlist on the fly, adding music based on criteria you choose. You can start with a small playlist, and let amaroK generate a fresh stream of music infinitely. New tracks will be added randomly, from selected playlists, or based on suggestions from <a href='http://last.fm'>last.fm</a>. This allows you to rediscover your music-collection in a completely new way.
</p>

<p>
Everyone loves eye candy, so we made these pretty <a href='http://shots.osdir.com/slideshows/slideshow.php?release=410&slide=1'>screenshots</a>, showing amaroK 1.3 in action. 
<p>
 
<br/>

<strong>New key features in amaroK 1.3:</strong>
<ul>
<li>
Wikipedia artist lookup, a revolutionary feature providing information from the free online encyclopedia. 
</li>
<li>
Redesigned sidebar, with improved look-and-feel. 
</li>
<li>
Automatic download of scripts and themes with KNewStuff. 
</li>
<li>
Helix engine. Using code from the GPL Helix Player, it allows amaroK to play using the codecs of Real Player or Helix Player.
</li>
<li>
New Playlist Browser, powerful and easy to use. 
</li>
<li>
Cue file sheet support. 
</li>
<li>
Dynamic playlist mode. 
</li>
<li>
PostgreSQL database support. 
</li>
<li>
Much extended DCOP scripting interface. 
</li>
<li>
Multiple analyzer visualizations for the playlist window. 
</li>
<li>
Editable Smart Playlists.
</li>
<li>
Podcast support, including live streaming and download capability.
</li>
<li>
Crossfading for the xine audio backend.
</li>
</ul>

<p>
Tarball and binary packages for most distributions can be downloaded at <a href='http://amarok.kde.org/wiki/index.php/Download'>amarok.kde.org</a>
</p>

<br/>

<strong>Release Schedule and Future Plans</strong>
<p>
As during the 1.2.x release cycle, you should expect new 1.3.x releases at the beginning of the week every two weeks. amaroK has a history of being punctual in its releases. There will probably be a few minor releases. These releases will have cleanups and bug fixes thanks to <i>Users Like You</i> writing useful bug reports, as well as minor (meaning, non-refactoring) feature enhancements.
</p>

<p>
After 1.3.x development is over, we will begin working on amaroK 1.4. From here plans become more tentative. One idea is to have amaroK 2.0's release be simultaneous as KDE 4.0, as we are looking forward to taking advantage of the new technology made available by <a href='http://www.trolltech.com/video/qt4dance.html'>Qt 4.0</a> and the potential of KDE 4.0 as soon as possible.
</p>

<p>
Stay tuned for the upcoming amaroK 1.3 Live-CD, soon to be released on <a href='http://amaroklive.com'>amaroklive.com</a>.
</p>






