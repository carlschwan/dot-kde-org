---
title: "KDE and digiKam Receive TUX 2005 Readers' Choice Award"
date:    2005-09-04
authors:
  - "tmacieira"
slug:    kde-and-digikam-receive-tux-2005-readers-choice-award
comments:
  - subject: "Concratulation digiKam"
    date: 2005-09-04
    body: "digiKam is one of KDE power application outside of KDEs base modules and deserves the award! The pace from 0.5 to 0.8pre is really impressive!\n\nBye\n\n   Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "fully agreed! togheter with AmaroK and K3B is digiKam one of the showcases of the excelent applications available for KDE."
    author: "superstoned"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "Agreed.  Digikam is really really good.  I can't wait for 0.8  :)  The export to gallery is fantastic!"
    author: "am"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "Although digikam is really great, I would like to see some improvements in its UI."
    author: "BIrdy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "Cool, tell us what.\nhttp://bugs.kde.org/\n\nDo you know our new UI in 0.8?\nhttp://www.digikam.org/Digikam-SPIP/IMG/jpg/sidebar2.jpg"
    author: "J\u00f6rn"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "What a coincidence, just bevore I read this I also sended you a mail about a interface idee :)"
    author: "leeghoofd"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "And another improvements using SideBar for image properties ! it's planed to 0.8.1. Take a look here :\n\nhttp://digikam3rdparty.free.fr/SideBar/\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "There are just small things which should be debated before, so not well suited for bugs.kde.org. Some things are:\n* No 90\u00b0 roations in the toobars ( 90\u00b0 and 270\u00b0 are bad without icons - I always have to think about which is for which direction)\n* The context menus for the images are to long\n* Are the browsing buttons in the toolbar used by anyone?!?\n* The installed digicam(s) should be acessable by the toolbar (as it was some versions ago)\n* ...\n"
    author: "Birdy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-04
    body: "- the image context menu was cleaned up for 0.8\n- apart from the rotation icons, you know you can change the toolbar yourself?"
    author: "J\u00f6rn Ahrens"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "Nice to hear that for 0.8 there was some cleanup. And yes, I know how to customize a toolbar. But the most important buttons (rotate left, rotate right) are not possible to add :-("
    author: "Birdy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "I really like digiKam, great job! \n\nBut I would like that there are buttons (in the toolbar) to rotate the image because they are really important, I think.\n"
    author: "Domi"
  - subject: "Toolbar not adjustable"
    date: 2005-09-07
    body: "The toolbar not really adjustable because it is not possible to add plugin actions into it. For example you can't add a button for lossless rotate or for colour adjustments.\nThat's bad, really bad."
    author: "Hans"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-07
    body: "\"* Are the browsing buttons in the toolbar used by anyone?!?\"\n\nYes, me ;) And anyone else, i presume, who keeps collections of pictures that need to be swapped through to see them at a larger than the very ample 256x256 size thumbnails :)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "One thing I really miss going from an older version to a newer one (I'm not on my comp atm, might screw up the version numbers) is being able to view an image in full size from the camera.\n\nI think it was 6.x, I could get a window with thumbnails of all the images on a camera, that I could then delete, download or view.  I could doubleclick on a thumbnail and see it in full size.\n\nIn 7.x, a window shows up with thumbnails of all the images on the camera, but I cannot see the full size image without first transferring it over to the hard drive.  If there is a way to view the full image directly from the camera, well it isn't exactly obvious ;)\n\nIt is IMHO a bit of a step back, and why I downgraded to 6.x.    Sometimes It's nice to be able to weed out images before putting them on the HD.  Of course you have to transfer them to view them anyway, but at least you don't have to switch to the destination folder to purge, creating and deleting a file, which is possibly on a fat partition leading to etra fragmentation.  Also nice if you want to look at someone's pics on their camera without putting them on the HD first.\n\nIt's hardly a bad thing to be able to doubleclick on a thumbnail and view the full image straight from a camera is it?  "
    author: "MamiyaOtaru"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "\"which is possibly on a fat partition leading to extra fragmentation.\"\n\nEverything you do on a fat partition wil lead to fragmentation :)\n\n"
    author: "rinse"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "\" It's hardly a bad thing to be able to doubleclick on a thumbnail and view the full image straight from a camera is it? \"\n\nTried this on digikam 0.7.4, and when I single click on an image in the camera interface of digiKam, it opens a new window with the image full screen.\n"
    author: "rinse"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "digiKam is just a cheap copy of other photo managing software. As soon as some people gave good comments about other projects suddenly it also was in digiKam. Instead of working together with others they prefer to go for themselves. DigiKam gets bloated, it lost something. I rather go for another project."
    author: "Dandy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "Then why do you comment here to begin with?"
    author: "ac"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "because it's free to, isn't it?"
    author: "Dandy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "Nope. One thing I learned long ago is that nothing is free.\n\nSpecifically, stating one's opinion **always** has an associated cost."
    author: "Roberto Alsina"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "\nThe guy said 'it's free to', not 'it's free' ;)\n\nHe probably meant freedom, not associated costs.\n\nWhich reminds me that it just cost me time to post this :)\n "
    author: "ac"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "I agree. I personally use FLPhoto. Functional, light interface, just what I want.\nhttp://www.easysw.com/~mike/flphoto/\n\nAnyway, kudos for digikam."
    author: "David Costa"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-06
    body: "Looks a bit like showfoto :)\n"
    author: "rinse"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-05
    body: "> digiKam is just a cheap copy of other photo managing software.\n\n==> digiKam is a photograph management software. You can find similar features in other sofware.\n\n> As soon as some people gave good comments about other projects suddenly it also was in digiKam.\n\n==> It's free to credit digiKam around the world\n\n> Instead of working together with others they prefer to go for themselves. \n\n==> Completly wrong : kipi-plugins is a framework to share plugins between other images managment softwares !!! Before to said any cretic, please take any information about digikam & co.\n\n> DigiKam gets bloated, it lost something.\n\n==> Yes digiKam have lost a stupid kde users ! Have you take a look in manual for example. Can you give me any other open source project with a complete handbook like digiKam ? Have you an idea about any time digiKam team working on ? I'm sure that you not developper!\n\n>  I rather go for another project.\n\n==> Yes, a \"GTK\" project like for example...\n\nGilles Caulier\ndigiKam developper !!!"
    author: "Gilles Caulier"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-06
    body: "It's nice to see how some people can't handle criticism... even if the criticism is as bad as the mine... I did not even give any facts or reasons...\n\n\"You can find similar features in other software.\"\n--> and they were first, so it still is a copy... but you are right, somebody should gather the good stuff and put it in one application...\n\n\"Yes digiKam have lost a stupid kde users\"\n--> so who should that be? Can something loose someone when not having it in the first place? Oh, and if you mean me, you don't even know me and judge me to be stupid... That behavior is somewhat childish... and you are a digiKam dev? rrrright\n\n\"Have you an idea about any time digiKam team working on ?\"\n--> I do, I know how much time it takes to develop applications. digiKam is no exception i believe but it also is nothing special there. I bet even smaller apps took a _lot_ of time to develop. Every application does and I appreciate all that work of those great people spending that much time so that I can enjoy such a great desktop with such great applications.\n\n\"I'm sure that you not developper!\"\n--> right you are _sure_? You must be divine or something... knowing stuff you can't actually know.\n\nYes, a \"GTK\" project like for example...\n--> No, why should I? Qt is great. This sounds very narrow minded... Be it GTK or Qt. It does not matter. It's the idea that counts. But maybe that is not your opinion\n\nI think I was wrong with that \"going for themselves\". The plugins prove the opposite. I'm sorry for that. It's actually great how you put all that stuff under one hood. Now I'm just a little bit disappointed about parts of your comment...\n\nAnyway, best of luck for the project.\n\nGreetings\n"
    author: "Dandy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-09
    body: "Perhaps he couldn't \"handle criticism\" because you just lobbed a bunch of unconstructive insults his way. You called his work bloated and a cheap copy of other applications, yet gave no real examples or ideas for improvement.\n\nYour initial post wasn't criticism, it was just a useless insult. How did you expect him to react?"
    author: "Dolio"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-09
    body: "Absolutly, and i had not time to lost with him (:=)))\n\nRegards \n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-13
    body: "haha, and reacting to such a post in that way... that's even more sad than the original post in the first place. And I myself do even have the curage to say that I was wrong in a point of my posts... he did not... so I still assume he is some holy guy?\n\nBest Regards."
    author: "Dandy"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-06
    body: "Interesting. So what is the original that was so badly copied by digikam?\nI'd love to have something better than digikam for KDE :D"
    author: "uddw"
  - subject: "Re: Concratulation digiKam"
    date: 2005-09-07
    body: "Your so innovative patches are always welcome, thanks!\nOh, where are your patches, proposals, I haven't got anything.\nYeah, you're right, bashing other peoples work is much easier :-)\nThanks for choosing another project."
    author: "J\u00f6rn Ahrens"
  - subject: "They are crazy"
    date: 2005-09-04
    body: "A list of best linux applications without amarok can't be serious."
    author: "Paulo Eduardo Neves"
  - subject: "Re: They are crazy"
    date: 2005-09-04
    body: "Amarok is in the list?"
    author: "leeghoofd"
  - subject: "Re: They are crazy"
    date: 2005-09-06
    body: "Ya, Amarok came in second to XMMS.  This reminds me a little of the \"user polls\" where sh wins for favorite shell.  Users don't know any better and are fairly uneducated about the specific software choice and so pick something they have heard of.   XMMS stinks (even from a simplicity standpoint.)  Amarok is possibly the best audo player on any platform... there is simply no comparision.\n\nBobby"
    author: "brockers"
  - subject: "XMMS winner?"
    date: 2005-09-04
    body: "This is quite absurde...we are currently in 2005 and a so unusable and file-oriented music player still wins prizes?\nReally, I don't understand. \n(amarok rocks!)"
    author: "Davide Ferrari"
  - subject: "Re: XMMS winner?"
    date: 2005-09-04
    body: "Fucking Gnome users, all of them use XMMS and they think that XMMS is a Gnome application... juas juas, it's just Gtk application.\n\nOf course amaroK is modern and better than XMMS, the only thing of XMMS that is better is the engine, faster than amaroK (using arts, xine or gstreamer)."
    author: "I\u00f1aki"
  - subject: "Re: XMMS winner?"
    date: 2005-09-06
    body: "The dang kicker to this is that amaroK can use the XMMS engine.  Like amaroK but want to use the XMMS engine?  No problem....\n\nBobby"
    author: "brockers"
  - subject: "Re: XMMS winner?"
    date: 2005-09-07
    body: "Yes problem, the XMMS engine, as you call it, is not a playback engine - it's used for grabbing and using the XMMS visualisations ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: XMMS winner?"
    date: 2005-09-04
    body: "Maybe it's because it \"Just Works (tm)\" and fits most users needs just perfectly?\nI prefer using amaroK, but XMMS still has it's benefits."
    author: "Barefooted"
  - subject: "Re: XMMS winner?"
    date: 2005-09-05
    body: "Well, mpg123 and ogg123 too \"just work (TM)\", and they are lighter than XMMS, so why people are using it? :P"
    author: "Davide Ferrari"
  - subject: "Re: XMMS winner?"
    date: 2005-09-05
    body: "Maybe it's hard to manage Playlists with [mpg|ogg]123 and they want something visually more appealing than a console? ;-)"
    author: "Barefooted"
  - subject: "Re: XMMS winner?"
    date: 2005-09-05
    body: "\nPrecisely. they want playlist management, but not too good, and they want it more visually appealing than a console, but less than amarok :)\n\nOr maybe they didn't try it out. Why would they? Most people don't use the best software, they just use the best known.\n\nMaybe 'we' should spread the word?"
    author: "ac"
  - subject: "Re: XMMS winner?"
    date: 2005-09-06
    body: ">  Maybe 'we' should spread the word?\n\nDon't worry, I'm already doing it :)"
    author: "Davide Ferrari"
  - subject: "Re: XMMS winner?"
    date: 2005-09-04
    body: "Hey, let 'em suffer. We are already happily enjoying Amarok :-)"
    author: "Anonymous"
  - subject: "Re: XMMS winner?"
    date: 2005-09-05
    body: "I guess it's because the lighter interface. I'm still using noatun, and I might jump for XMMS if noatun wasn't shipped with my distro."
    author: "Francisco Salsicha"
  - subject: "Re: XMMS winner?"
    date: 2005-09-06
    body: "So is the majority of people running Linux on really old hardware? and so why didn't FMWM win the desktop prize?"
    author: "Davide Ferrari"
  - subject: "Re: XMMS winner?"
    date: 2005-09-06
    body: "No, but I don't think people want a low priority backround task, like playing music, steal too much of the total CPU power. "
    author: "Morty"
  - subject: "Re: XMMS winner?"
    date: 2005-09-07
    body: "I was ironic :) to me, it's clear that XMMS is the most voted due to ignorance of amarok existance and may be due to the \"geek factor\", amaroK is too simple and powerful for a true geek :)"
    author: "Davide Ferrari"
  - subject: "AmaroK often crashes"
    date: 2005-09-07
    body: "AmaroK often crashes.\n\nThat's the reason many people stick to xmms which \"just works\"."
    author: "Hans"
  - subject: "Re: AmaroK often crashes"
    date: 2005-09-07
    body: "Do you perhaps use a HyperThreading-enabled CPU and an SMP-kernel? That's the only situation which causes frequent amaroK crashes that I know of, and probably it isn't even amaroK's fault, but a bug in the kernel or in low-level libraries - unfortunately, nobody seems to know for sure.\n\nhttp://bugs.kde.org/show_bug.cgi?id=99199"
    author: "Frank Roscher"
  - subject: "kaffeine as favorite media player!!!"
    date: 2005-09-04
    body: "I love kaffeine, but the only bad thing with kaffeine is that it crashes konqueror, and the bug is not related to xorg, as the kaffeine author assumes.\n\nKaffeine is a very good media player, and it will be *the best* media player if it supports fully embedded media in konqueror.\n\nAfter KDE 3.2, kaffeine crashes konqueror (still on kde 3.5 svn) on rtsp sites :( like www.dishant.com etc.\n"
    author: "fast_rizwaan"
  - subject: "kopete"
    date: 2005-09-04
    body: "Kopete is a real gem for kde, But I find 2 annoying things in kopete,\n\n1. Can't login to googletalk (jabber with my gmail.com) id\n2. Removing an account does not remove the contact list.\n\n1. I can logon to googletalk using gaim, but kopete won't log me on :(\n\n2. add user account \"Joe\" say in yahoo, and remove \"joe\", then later add \"MOE\", fortunately/unfortunately MOE will get all contacts of \"JOE\" :( this is not nice.\n\nApart from those 2 annoyances, I find kopete wonderful!\n\n"
    author: "fast_rizwaan"
  - subject: "Re: kopete"
    date: 2005-09-04
    body: "Try the settings from the screeny, those work perfectly for me."
    author: "ac"
  - subject: "Re: kopete"
    date: 2005-09-04
    body: "Hmm... 'QCA TLS plugin not installed on your system'\n:/\nI'll find it tho."
    author: "Patrick Davies"
  - subject: "Re: kopete"
    date: 2005-09-05
    body: "Install PSI. It's the only app I know providing that QCA-TLS plugin"
    author: "ZeD"
  - subject: "Re: kopete"
    date: 2005-09-05
    body: "kopete can do it, too. You just need to install qca-tls plugin on fc3."
    author: "anonymous"
  - subject: "Re: kopete"
    date: 2005-09-04
    body: "Google Talk works with Kopete fine. The previous reply with the settings screenshot attached holds the secret...\n\nThe secret is that it seems you need the \"SSL\" connection checked... "
    author: "T. Middleton"
  - subject: "Re: kopete"
    date: 2005-09-05
    body: "Kopete does not connect behind firewalls, because you can't tell him to use port 80, just like GAIM can do. I like kopete, I want to use kopete, but this, and not having a toolbar in the chat window to access account functions are an obstacle."
    author: "Ivan da Silva Bras\u00edlico"
  - subject: "Re: kopete"
    date: 2005-09-05
    body: "How is the IRC integration on Kopete going, btw?\n\nIt used to be quite good on the first releases it was shipped with KDE, but sadly it got worse and worse. I can't seem to be able to even connect to an IRC server at my KDE 3.2 version. Hope it improves because I hate to use another application for IRC."
    author: "blacksheep"
  - subject: "Re: kopete"
    date: 2005-09-06
    body: "Hmm, I never experienced any problems with connecting to a irc-server using kopete.\nYour problem must be a configuration error or something"
    author: "rinse"
  - subject: "Re: kopete"
    date: 2005-09-06
    body: "> I can't seem to be able to even connect to an IRC server at my KDE 3.2 version\n\nThe following information helped me to get IRC working again:\n\nhttp://kopete.kde.org/faq.php#id2291930\nhttp://dot.kde.org/1080892495/1081381765/1101217681/1101694059/\n\nHope it helps...\n"
    author: "Christian Loose"
  - subject: "Re: kopete"
    date: 2005-09-06
    body: "Take a look at the Kopete wiki:\n\nhttp://wiki.kde.org/tiki-index.php?page=Google+Talk+support\n\nJust my 0.02 eur. ;)"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Kubuntu"
    date: 2005-09-04
    body: "\"Favorite Distribution: Kubuntu/Ubuntu\"\n\nAnd I most certainly agree. :)"
    author: "Patrick Davies"
  - subject: "Re: Kubuntu"
    date: 2005-09-05
    body: "Can you explain what makes Ubuntu the Favourite Distribution ?"
    author: "Youssef"
  - subject: "Re: Kubuntu"
    date: 2005-09-05
    body: "\nFree cdroms. Lots of hype. Pretends to be beginner-friendly (is not, but how should beginners know?). Commitment to free software (this one for real). Some things just work. Many don't just work, but see lots of hype :)"
    author: "ac"
  - subject: "Re: Kubuntu"
    date: 2005-09-05
    body: "Well it's the first distro I've got installed on my laptop that got the sound, networking (incl. wifi), display (1280 x 800 at last!), ACPI, touchpad, USB etc working first time. It tries hard to present things in a smart and easy to use way - kde looks very clean.\n\nBUT, it's buggy as hell ATM and it's not a beginners distro IMHO - I'd also recommend staying well clear of adding universe to your sources unless you're prepared for regular re-installs.\n\nIt's got promise though - if Ubuntu can work more closely with debian we might just end up with the so far \"mythical\" desktop linux success story that we all want!"
    author: "Rich"
  - subject: "Re: Kubuntu"
    date: 2005-10-30
    body: "After much work in the terminal, building madwifi, etc. I finally got Hoary working just about perfectly on my laptop.  I still had to dig through forums for hours almost every time I installed a new app -- but it was pretty solid all around -- even my Wifi, Sound, Touchpad, and ATI wireless were working fairly well.\n\nThen I read Breezy had much better laptop support, so I did the dist-upgrade...  That was weeks ago, and I'm still not back to the level of functionality I had beforehand.  Mpeg video almost always fails now (kaffiene used to ace everything -- now it's consipcuously missing from the repositories) -- the touchpad is prone to fits of random behavior, and the sound is not as reliable.\n\nI agree, this is as close as I've ever gotten to the \"promised land\" of a desktop linux.  (Other distros either seem to give 70% working hardware OR 70% working software -- Hoary was at about 85% on both scores.  Breezy seems to be prettier and maybe run a bit faster, but is less reliable for both hardware and software.)\n\nWe all want this to happen, I think -- a free, powerful desktop that's not in the pocket of a mega-corp.  As an overall OS, Linux probably already has M$ beaten.  Still, any windoze replacement is going to need strong laptop, peripheral, USB, 3d graphics, sound, midi, compressed video, wifi, etc.  \"All the modern conveniences,\" as they say."
    author: "gat"
  - subject: "Re: Kubuntu"
    date: 2006-10-25
    body: "I'm a complete beginner with Linux and not too tech savy at all, yet I've managed to get Kubuntu working on my creaking old iBook. Loving it.\n\nNow my only question is: can anyone explain to a dummy like me how to get a Risc OS theme working on it?"
    author: "Will"
  - subject: "&#1567;&#1567; ??"
    date: 2005-09-05
    body: "  I think it is strange that KDE is the favorite desktop but the other competing KDE applications are not the winners of their categories. One thing that makes a desktop environment a good one is the consistency between its applications.\n  And that is the reason why I use KDE and along with it : KMail, Konqueror, KDEwebdev,  ... What is KDE without Kopete, Konqueror, KMail, KOffice, amaroK, Kaffeine, Kate ... ? The answer: kdesktop, kicker, kwin."
    author: "Youssef"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-05
    body: "answer: kdesktop, kicker, kwin.\n\nDon't forget kioslaves. Of course they don't work without applications, but they are an important part of KDE that shouldn't be overlooked."
    author: "Paul Eggleton"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-05
    body: "If you don't use any KDE application, ioslaves are useless. Youssef as a point IMHO."
    author: "blacksheep"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-05
    body: "Not if you are using FUSE with that gateway stuff someone mentioned a while ago."
    author: "Roberto Alsina"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-05
    body: "FUSE is KDE independent."
    author: "blacksheep"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-05
    body: "Yes, but with something like kio_fuse non-KDE apps can benefit from the power of KDE's IOSlaves.  \n\nSee http://wiki.kdenews.org/tiki-index.php?page=KIO+Fuse+Gateway\n\nDunno what state that gateway is in, though...\n\n"
    author: "cm"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-05
    body: "I don't think it is strange at all, given I've seen ordinary users with KDE. The desktop may be good, that's not the equivalent to say all the apps are the best. My wife uses Gnumeric continually for example, despite its KDE desktop, and she never *got* the fact that the application is not a KDE app (she does not know what KDE is anyway). She uses MPlayer (with plastik skin) without any second thought. She uses Galeon for web browsing because she prefers it. She does not know the difference between Gnome and KDE apps, she just chooses what suits her best.\n\nEvery user is not fanatic about its desktop. Fortunately, I'm not either, or I would have given her a Gnome desktop like mine."
    author: "Ookaze"
  - subject: "gnome"
    date: 2005-09-05
    body: "the funny thing is that gnome people assimilate every non-qt non-KDE app as an gnome app. but in fact that is wrong.\n\nindeed, it should not matter whether it is written in QT or not. We need a better integration of nonqt/nonKDE apps into the KDe desktop. "
    author: "martin"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-06
    body: "The main reason in some categories KDE applications don't win, are pure lag in user base. Lots of users are conservative and don't change the applications they use easily. Only in cases where the new application are clearly better will we see a sudden shift of the userbase, like what happend with X-CD-Roast and k3b. The way people stick with such antiquities as XMMS are beyond me, and honestly Noatun was the better application 4 years ago. Even if Noatun or Juk managed to dislodged it, but it seems like amaroK has got the hype and momentum to finally do so. In other areas I think it also will shift, as the quality of the KDE applications increase even more and more and more people realize it. I think Krita is applications it will be interesting to watch. Besides it took a KDE application to end the vi-emacs war:-) "
    author: "Morty"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-07
    body: "And what KDE application is that?  I find myself using KDevelop/Kate and sometimes kyzis instead of emacs and command-line vi, but I'm just about the only one around that does.  Of course, nobody uses vi.  It's mostly a battle between emacs and the slickedit windows guys.\n\nNow for me, I stopped using emacs for mail (gnus) when I found that kmail was actually useful and emacs again for aim (tnt) when I decided gaim might be worth the hassle of windows.  I tried kopete, but it was a little frustrating having it crash every third message back then.  It does seem to have stabilized a bit in the past few months, though.  Kudos to the team.\n\nNow, can anyone tell me what it is that evolution is supposed to have over kmail, except maybe organization in the configuration hierarchy?  Kontact really needs to try and contain its schizophrenia..."
    author: "Yrrebnarg"
  - subject: "Re: &#1567;&#1567; ??"
    date: 2005-09-07
    body: "As you already stated, Kate. It has started to really take on the old boys, vi and emacs in popularity. In the last year started to split the top duo in popularity contests. And you should have seen this little gem from the article blurb:\n\"KDE applications Kate and KWrite, which are in fact one application only, won second and third places in the \"Favorite Text Editor\" category, and would have got the first place if their votes were added together.\"\n\nSaying that nobody uses vi is somewhat correct, as I guess they mostly prefer vim. But it's customary to lump them together as one, same as emacs and xemacs. Looking at it that way there are a few somewhat well known nobodies using vi, like that Thorvalds guy. \n\nI remember trying gnus years ago, back in the days when X only was used as a way to have many terminal windows open, but I decided it was not worth it. I went for Pine insted, as it was both userfriendly and powerfull(for my needs).\n\nPerhaps it's time to consider Kopete again then, for me it has been stable for the last two years. Nearly whitout crashes, had a few on svn/cvs versions. But that's the price of bleeding edge tho. The only small problems have been the times when MS and the likes, plays silly buggers and change the protocols. And even then the total time of outage until Kopete got updated are lover than the outages I have had caused by the MSN server being down."
    author: "Morty"
  - subject: "kde-look.org"
    date: 2005-09-05
    body: "Is kde-look.org down?"
    author: "Matt"
  - subject: "Re: kde-look.org"
    date: 2005-09-06
    body: "Not anymore :)"
    author: "rinse"
  - subject: "kde won, let's tell the world!"
    date: 2005-09-09
    body: "Should somebody update http://www.kde.org/awards ?"
    author: "Anonymous"
---
With its <a href="http://www.tuxmagazine.com/node/1000150">Issue #6</a>, <a href="http://www.tuxmagazine.com">TUX Magazine</a> published the winners of its first annual <a href="http://www.tuxmagazine.com/node/1000151">Readers' Choice Awards</a>, placing <a href="http://www.kde.org/">KDE</a> in the first place in the &quot;Favorite Desktop Environment&quot; category and <a href="http://www.digikam.org">digiKam</a> first in the &quot;Favorite Digital Photo Management Tool&quot;.




<!--break-->
<p>In addition, KDE applications were placed second in categories &quot;Favorite Communications Tool&quot; (<a href="http://kopete.kde.org/">Kopete</a>), &quot;Favorite Web Browser&quot; (<a href="http://konqueror.kde.org/">Konqueror</a>), &quot;Favorite Mail Client&quot; (<a href="http://kmail.kde.org/">KMail</a>), &quot;Favorite Productivity Tool&quot; (<a href="http://koffice.org/">KOffice</a>), &quot;Favorite Music Player&quot; (<a href="http://amarok.kde.org/">amaroK</a>) and &quot;Favorite Media Player&quot; (<a href="http://kaffeine.sourceforge.net/">Kaffeine</a>). KDE applications <a href="http://www.kate-editor.org/">Kate</a> and KWrite, which are in fact one application only, won second and third places in the &quot;Favorite Text Editor&quot; category, and would have got the first place if their votes were added together.</p>

<p>The KDE Project and its contributors are very proud of these results, considering they are the users' choices and votes that counted. Congratulations to everyone involved!</p>


