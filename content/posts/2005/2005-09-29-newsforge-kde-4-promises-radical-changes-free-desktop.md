---
title: "Newsforge: KDE 4 Promises Radical Changes to the Free Desktop"
date:    2005-09-29
authors:
  - "jriddell"
slug:    newsforge-kde-4-promises-radical-changes-free-desktop
comments:
  - subject: "I can't wait KDE4!"
    date: 2005-09-29
    body: "Fooooooo!!!"
    author: "LaserRamonHG"
  - subject: "Re: I can't wait KDE4!"
    date: 2005-09-29
    body: "start coding today !"
    author: "chris"
  - subject: "Re: I can't wait KDE4!"
    date: 2005-10-02
    body: "But as it seems, you'll have to wait 1 more year for it...\n\nOr maybe the devs will think about the release schedule and aim a release 6 or 9 month after KDE 3.5...\n\nI hope it gets out of the door before windows vista..."
    author: "ac"
  - subject: "tenor"
    date: 2005-09-29
    body: "tenor is supposed \"collects, colates and stores metadata, full text indexes, linkages and contextual relationships between information on the desktop.\"\n\nwhile this is surely a very very cool and exciting feature, I would like to raise some potential issues that might occur when adding it.\n\npossible issues that come up in my mind that should be addressed include:\n1. nfs friendliness.\n2. limited file indexing.\n\n1. most linux installations distribute home directory's over nfs. while this will not be a safe way of distributing home directory's until nfs4 has been released it has become common practice to distribute home directory's this way, and it probably also is the only available for now. (perhaps openafs or coda would work, don't know) I personally can forsee a great problem with network traffic when all those clients go on indexing on a nfs share. In this case it might be nicer to let the server index the files instead, and have the server monitor for changes as well. this would safe a lot of traffic. and the server would also be able to do it much faster.\n\n2. I personally don't think it is a good idea to go and index the entire harddisk. there are many files the user would simple never search for like files in /etc or /usr\n\nthe first thing that would probably come to mind to only index the users home directory, but this has a disadvantage. I for example keep many files in /mnt/share. this is a readonly nfs share that contains files that are often used by everyone, but that are not allowed to be changed. (templates for example)\n\nhaving every user index both his home and /mnt/share would probably be a waste of discspace. because the loss of discspace would increase with every user that is added to the system. now discspace itself might not be that much of a problem. but backuping large amounts of data surely IS an issue.\n\nthe most clean way of doing it in my opinion would probably be to:\n1. have the server index shares like /mnt/share in a separate file that can be read (but not written) by all users. (in case of a desktop pc the client is also the server) \n2. have a second file with all the home dir metadata stored in the users home directory. this file would in the best case also be written by the server to reduce network traffic overhead.\n\nthe user would then include both files and have full searching ability in all the files that are usefull to him.\n\n(and sorry for my bad english...)"
    author: "Mark Hannessen"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "> 1. most linux installations distribute home directory's over\n> nfs. (...) In this case it might be nicer to let the server\n> index the files instead (...)\n\nAt the time Tenor is shipped, file systems should be able to do that work, so Tenor could just use those features directly, instead of using the software backend.\n\n> 2. I personally don't think it is a good idea to go and index\n> the entire harddisk. there are many files the user would simple\n> never search for like files in /etc or /usr (...)\n\nDistros should just put all that crap into a common directory called System or something. Then, you could just add it to the ignore list."
    author: "blacksheep"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "\"Distros should just put all that crap into a common directory called System or something. Then, you could just add it to the ignore list.\"\n\nNonsense, it's the otherway around. Systems ignore /home and that is where users should put their stuff. And guess what, it already works that way for a long time."
    author: "koos"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "> Distros should just put all that \n> crap into a common directory\n> called System or something.\n> Then, you could just add it to\n> the ignore list.\n\nOMG YEAH LETS BREAK THE STANDARD FILE HIEARCHY because it SUCKS!!!! Lets put all non-user files in a folder called Linux\\system32. Great Idea!!! Will solve all problems."
    author: "Anon the Cow"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "And your point being?"
    author: "blacksheep"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "I'm not sure, but I think his point was that your idea is idiotic."
    author: "anon"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "What if someone has 64 bit arch? Shouldn't it be /Linux/System64 then? ;)"
    author: "pks"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "I hope you meant C:\\Linux\\System64 "
    author: "cm"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "You mean, like C:\\Linux\\System64, C++:\\FreeBSD\\System32, Java:\\AIX\\System64... sounds like hell to me. I'd rather stick to /usr, /opt and /home."
    author: "Andy"
  - subject: "Re: tenor"
    date: 2005-10-01
    body: "No, I was referring to drive letters, but your obfuscation idea is even better. :)  "
    author: "cm"
  - subject: "Re: tenor"
    date: 2005-10-01
    body: "No, \"C:\" refers to the third used drive and its files. It seems you never had the priviledge to use one of ms grandious products... ;-)\nok, i'll give you a free introduction: C: is /dev/sda for us, D: is /dev/sdb for us and so on. But A: and B: is /dev/fda. You have multiple /'s, one for each drive, e.g. you have to know on which drive and which partition (and which host, when using nfs) the files are, only the path gets you nowhere.\nConfused? Well, you got ms business plan: Success by obscurity."
    author: "Lars"
  - subject: "Re: tenor"
    date: 2005-10-02
    body: "That is not correct. /dev/sda is the first initialized device that used the scsi subsystem, sdb is the other and so on. C: is the first primary partition on the master of the first ide channel. D: can be the second primary partition on the first drive or it could be the first partition on the first ide channel slave drive.\n\nThe drive letters C: D: etc are a major pain in the neck because of how it does partition names. The letters are assigned by primary partition first on master then slave across all ide channels and then it is gees by the extended partitions in the same order."
    author: "Kosh"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "> Systems ignore /home and that is where users\n> should put their stuff.\n\nI don't think Tenor should work that way. I've some root folders (e.g. /multimedia) where I keep stuff shared by everyone on the computer.\nOf course, I could just move all that crap into /home, but it'd be quite messy.\n\nI guess it'd make more sense to push distros into presenting an user-friendly directory tree."
    author: "blacksheep"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: ">I've some root folders (e.g. /multimedia) where I keep stuff shared by\n>everyone on the computer. I could just move all that crap into /home,\n\nOr perhaps make a symlink in the home directories? And as a added benefit having ~/multimedia, keep the users in their home directory. Making it less likely they get lost in the \"complex\" filesystem layout(A real problem according to some).  "
    author: "Morty"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "well, this is an idea, but it will cause double indexing.\nsince every user is going to store the metadata of /multimedia\n(unless metadata gathering is done by a system wide daemon or something like that of course)"
    author: "Mark Hannessen"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "> I've some root folders (e.g. /multimedia) where I keep stuff shared by \n> everyone on the computer.\n\nActually, that should be: \"/usr/local/multimedia\"\n\nAnd we are going to have to index \"/usr/local\" except that we need an ignore list.\n\nOTOH, the idea of having each user have a link:\n\n~/Files/User_Multimedia -> /usr/local/multimedia\n\nseems like a good idea.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: tenor"
    date: 2005-10-01
    body: "This seems to be the logical solution to me, and how I've always run my computer. I have /usr/local/music and /usr/local/movies, and have symlinks from my home directory (and the other users who may need them). So when guest users log in, they can access all the multimedia stuff, and I can have /usr/local/ as a separate partition from /home, which mostly stores personal configuration files and stuff."
    author: "KOffice Fan"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "User friendly? It is user friendly. Unless you mean user friendly like windows where you get no physical presentation of how the directory structure really is organised. \n\nHmm ... /home/my_user_account vs C:\\Windows\\Documents and Settings\\my_user_account\n\nNo thanks, I like the directory structure just how it is."
    author: "fred"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "I don't consider the Windows tree user friendly either, so please don't put word on my mouth.\n\nThe MacOSX tree would be an example of what I consider a user friendly tree."
    author: "blacksheep"
  - subject: "Re: tenor"
    date: 2005-10-03
    body: "Wrong:\n\nC:\\Windows\\Documents and Settings\\my_user_account is not correct. it is actually....\n\nC:\\Documents and Settings\\my_user_account Which would be equivilent to C:\\home\\My_user_account. Just a more descriptive name.\n\nNow, you might say that \"Documents and Settings\" is a waste of time to type out. You may be right. But nobody actually types it out while using their windows box. That is why it is more user friendly.\n\n\nNow, if it HAD been under C:\\Windows then THAT would be a damn shame.. to mix the system files with the user files. But it isn't. In fact, a windows sytem has 4 directories:\n\nC:\\Windows (for system files)\nC:\\Program Files (for programs, not os provided system utils)\nC:\\Documents and Settings (equivilent to /home)\nC:\\temp\n\nThis is thousands less complex to an end user when compared to any of the *nix which might have /usr /usr/local, and /opt to store programs they might want to run on the desktop. /etc to store settings for all users (whereas on windows, /docs and settings/all users has it). /bin /tmp /var /lost+found /boot etc etc..\n\nIf you are going to say that /home should segregate the users from the system then you are wrong. because users must be able to navigate through /usr, /usr/local, and /opt just to find a program and depending on the system and program combination depends on where it is installed. Until there is ONE place to find programs for users, the unix root directory is too complex.\n\nmy proposal:\n\n/home\n/usr/program name (for programs)\n/system (to contain /var, /etc, /lost+found, /log, etc etc..)\n/tmp\n/mnt\n\nYou also have to realize that being user friendly doesn't only mean service end users who are too idiotic to use a pc. It is to make it easier for local administrators to install drivers, configure the video card options in xfree, etc etc.. Navigating all the spagetti for standard driver and program management is a paint in the ass with the current model. Nothing is labeled intuitively, and it is spread out all over the root directory. That is NOT user friendly. And it isn't administrator friendly. That is the point..\n\nNow, to break the traditions and change it? I don't think it is possible. But the point is that it isn't off the wall to suggest such a thing. It is very appropriate. And anybody who says it is not is a hypocrite (like you)"
    author: "anon"
  - subject: "Re: tenor"
    date: 2005-10-04
    body: "> paint in the ass\n\nI don't even want to imagine how that would feel.  \n\n\nSCNR. \n\n"
    author: "cm"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "\"At the time Tenor is shipped, file systems should be able to do that work, so Tenor could just use those features directly, instead of using the software backend.\"\n\nI disagree with you on this one. you see, many metadata is stored in things like mp3tags or headers of files. and a filesystem will never be able let you search through such metadata. the only way to do this is by gathering the data and then index it.\n\nwhile the filesystem metadata searching facility is a great thing, I guess it will have to coexist with a indexing engine for a very very long time."
    author: "Mark Hannessen"
  - subject: "Re: tenor"
    date: 2005-10-06
    body: "> Distros should just put all that crap into a common directory called System or \n> something. Then, you could just add it to the ignore list.\n\nThe hell they should.\n\nDistros should keep on using the current file system hiarchy, and not piss off every knowledgeable unix user out there.\n\nDo you actually *want* *nix users to abandon the system?\n\nAre you on crack or something?"
    author: "Anonymous Coward"
  - subject: "Re: tenor"
    date: 2005-10-06
    body: "There is a lot bunch of distros for every taste and I am all for it. I just feel that desktop-oriented distros should make the directory tree more intuitive.\n\nBy the way, there is no \"current file system hiarchy\". In fact, some distros are starting using /media, instead of /mnt and there are lots and lots of little things like distros putting KDE under /usr, others under /usr/local, or even under /opt. \"Current file system hiarchy\" doesn't exist."
    author: "blacksheep"
  - subject: "Re: tenor"
    date: 2005-10-07
    body: "> There is a lot bunch of distros for every taste and I am all for it. I just \n> feel that desktop-oriented distros should make the directory tree more \n> intuitive.\n\nI think the last thing we need is a *more* inconsistent file system hierarchy across distros.  But this would be the result if your suggestion were implemented.  \n\n\n\n> By the way, there is no \"current file system hiarchy\".\n\nSure there is:  http://www.pathname.com/fhs/ . Unfortunately interpretation and  degree of compliance is not the same with all distros. \n\n\n> some distros are starting using /media, instead of /mnt \n\n/media is for removable media, /mnt is for temporarily mounted file systems.  They're both in the standard, as is /opt. \n\n\n"
    author: "cm"
  - subject: "Re: tenor"
    date: 2005-09-29
    body: "I thought tenor was dead. It was a great idea for KDE4 last year, but have died when faced with reality. I was surprised to see it brought up again in KDE4 expectations."
    author: "Carewolf"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "Just as bad as spreading false hope is spreading false despair.\n\nWe will see Tenor, have a look at Kat in order to guess what it will be capable of, and it will not be shiny and all perfect, but an interesting thing with happy and not so happy users, the later stopping to use it.\n\nThat's how it's going to be. :-)\n\nI personally as a KDE apps only user, want to see KDE 4.1 with the first apps picking it up for real.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: tenor"
    date: 2005-09-30
    body: "BTW: What you're discussing here is not really Tenor (or better: the interesting part of Tenor, the Context Linkage Engine), it's about Kat [1] \n(indexing, full text searching, etc.). But anyway, ATM it looks like Kat and Tenor will work together.\n\nI wish all a nice day and weekend.\n\n[1] http://kat.sf.net"
    author: "Mario Fux"
  - subject: "Question about Tenor..."
    date: 2005-09-29
    body: "I've been wondering this for a while now...\n\nIf you're running, say, Opera (or any non-KDE browser), will Tenor still be able to detect and keep track of where you got the image or whatever that you download?\n\nI mean, if I were on Opera, and I downloaded an image, would Tenor be able to see that I got it from whatever site I got it from, or will it only support Konqueror?\n\nYes, I'm using Opera.  :p  But I like only having to open one app for all my mail, RSS, and Browsing needs."
    author: "K3V"
  - subject: "Re: Question about Tenor..."
    date: 2005-09-30
    body: "I suspect in order to do that, Opera would have to be designed to make use of Tenor. I don't think it will automatically be able to pull information from every application.  Does Opera have a plugin architecture of some sort? I suppose it'd be conceivable to write some sort of a plugin that interacts with Tenor. Otherwise, I wouldn't get my hopes up."
    author: "Dolio"
  - subject: "Re: Question about Tenor..."
    date: 2005-09-30
    body: "Well, that would be part the part where RuDI comes in, I guess.\n\nOpera would just subscribe to a RuDI service offering the indexing and KDE, providing the RuDI service, would take care of the rest."
    author: "Arend jr."
  - subject: "Just like Mac OS 10.4"
    date: 2005-10-01
    body: "Sounds like they are copying Apple. They have really come a long way, seriously; I mean that as a compliment. Not the direction I was looking to see - one of the reasons I switched to Linux. My question is how much will this up the system requirements to run KDE? One of the big advantages of Linux and BSDs is lower system requirements as compared to Vista. They may not be so far apart if these features are implemented."
    author: "Benjamin Huot"
  - subject: "Re: Just like Mac OS 10.4"
    date: 2005-10-02
    body: "What gives you the idea that the proposed features would ask higher system requirements?"
    author: "rinse"
  - subject: "Re: Just like Mac OS 10.4"
    date: 2005-10-02
    body: "Because the RAM requirements for OS X doubled when they added those features. Don't more processes take more memory?"
    author: "Benjamin Huot"
  - subject: "Re: Just like Mac OS 10.4"
    date: 2005-10-02
    body: "Not necessarily. If you manage to reduce the memory requirements of other processes, you will end up with some extra memory that you can use for new features..\n\nRinse"
    author: "rinse"
  - subject: "Re: Just like Mac OS 10.4"
    date: 2005-10-02
    body: "Pretty amazing programming to go down in system requirements for the same functions. KDE programmers must be either much better than most other programmers or not having an interest in hardware sales really helps program efficiency."
    author: "Benjamin Huot"
  - subject: "Re: Just like Mac OS 10.4"
    date: 2005-10-02
    body: ">>Pretty amazing programming to go down in system requirements for the same functions.\n\nWhy, that is called optimizing :)\n\n\n>>KDE programmers must be either much better than most other programmers\nOr they have better tools :o)\n\n>> or not having an interest in hardware sales really helps program efficiency.\namong other reasons.\n\nBut you probably know that you can implement 1 function in very many ways, from very slick and small, to very bloated. No-one creates functions with very small footprints the first time, but while optimizing things in a later state, one could end up using less memory footprint for the same function by implementing it in a better way..\n"
    author: "rinse"
  - subject: "Re: Just like Mac OS 10.4"
    date: 2005-10-02
    body: "Sounds great. Looking forward to KDE 4.0 then."
    author: "Benjamin Huot"
  - subject: "Getting better GUI"
    date: 2005-10-01
    body: "Well I don't know what Kde developers are doing, but I hope they are doing something wonderful. I hope that Kicker will be totally rethought like somebody promised in his blog. I guess it was somebody of those plasma developers.\n\nWindows panel look should be changed somehow, apple panel looks little weird. something between or whole new approach(like BeOS) would be awesome. Menu standards from free-desktop.org could be integrated thought I hope that something better will be invented as gnome menus are not going to be anygood in new kde."
    author: "Antti"
  - subject: "Re: Getting better GUI"
    date: 2005-10-01
    body: "It funny how many people don't like the KDE panel just because it is remotely like windows. If it works, then why should we worry that it is to similar to windows?\n\nAnyway, I think KDE will reorganize the panel a little; making it more powerfull, etc.\n\nMark"
    author: "Mark Watson"
  - subject: "Re: Getting better GUI"
    date: 2005-10-02
    body: "Rudi looks like adding yet-another desktop API for Linux. But hey - if we\ncan get everyone behind it, that's exactly what we need. Kudos for the great\nidea.\n\n"
    author: "Anonymous Coward"
  - subject: "I hope RuDi gets in"
    date: 2005-10-01
    body: "You know, we are in 2005, I think it's already time we can build programs that are toolkit independent.\nImagine you writting a app in any language, linking it to RuDi that just make it look and feel like the current desktop.\n\nI've always dreamed about some kind of interface wrapper using XML to describe the interface and the connections between widgets and functions/methods. I know it's not trivial, but I have faith on open source developers skills :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: I hope RuDi gets in"
    date: 2005-10-03
    body: "Actually, i wrote one of those interface wrappers in Java because i got sick of adding listeners and crap to every friggin component and then having to add the bounds checking and so forth, and as you said, it was not trivial. \n\nI think it would be cool to get a toolkit independant language to describe the lyaout and behaviour of widgets/components, so that any toolkit can use it. ( of course this has probably been done. :) )"
    author: "Glen"
  - subject: "Oh ho"
    date: 2005-10-01
    body: "Hope KDe 4 will also support tablet PCs. May be they are not succesful now buzt on the long run, tablet PCs tehcnology will be the future."
    author: "fennes"
  - subject: "My sole wish: Simplify KDE's Progface"
    date: 2005-10-03
    body: "Please, just eliminate all the clutter in Konqueror and other programs. Toolbars are meant for MOST FREQUENTLY USED FUNCTIONS BY GENERAL USERS, note I said GENERAL, not computer whizzes which hack on KDE. Trim those context menus. Elimante all those options which are only used by 1% of people from the visible places, even well made and organized options confuse if in large numbers. Take some hints from GNOME. Get a HIG that everyone can understand and that covers everything, KDE's HIG is almost too old to be useful and certainly too shallow to make KDE more consistent. And please, get KDE some better marketing and fundraising efforts."
    author: "Mike Torrent"
  - subject: "Re: My sole wish: Simplify KDE's Progface"
    date: 2005-10-03
    body: "we don't need KDE4 for this, much of this will already be done in KDE 3.5. KDE4 is about big changes to the underlying framework, not about some small interface changes (not saying those won't happen, just they are not the focus of KDE4)."
    author: "superstoned"
  - subject: "Re: My sole wish: Simplify KDE's Progface"
    date: 2007-06-08
    body: "I strongly disagree.  It's popular myth that the extra options, functions, and configurability of KDE reduces usability.  In a 2003 usability report (Sorry, the page with the article is no longer available.  The dead link can still be found on Wikipedia's page on KDE.) comparing KDE to Windows XP, users were quickly able to identify functions and perform comparative tasks.  Usability was found to be about the same.\n\nI remember the old Galeon web browser in GNOME and its \"cracked up\" interface with a million functions and features.  It was a major attraction to many of my customers and was a reason so many of them chose GNOME.  Developers all agreed that users don't want the mess, and they stripped it down until it was less useful than the Mozilla engine it was based on.  Unfortunately, my customers all agreed that they liked the features and functions, and most of them refused to upgrade from Red Hat 8.0 for years.  Many of them came back to my shop asking for SuSE 10.0, choosing the KDE desktop and Konqueror browser.\n\nEven grandma's and grandpa's are plenty smart enough to identify the buttons and navigate around.  I consider myself quite the computer expert, but even I often find myself simply not using features that have moved inside of submenus.\n\nDon't insult users and developers by saying some of the features are \"used by 1%.\"  \"General users\" includes computer whizzes, grandma and grandpa, Mr. Businessman, Gamer-Kid, and all other groups.  Just as there is no such thing as an average person, there is no such thing as a General User.  In the computer service business, I often see technicians who feel like they need to save computer-illiterate users from themselves.  The truth as I have seen it is that so many of my customers, even those who can't figure out digital cable or satellite receivers, are really quite good at figuring out what they want a computer to do -if- the options are there for them.  People aren't computer illiterate; software is simply refusing to communicate with people.\n\nAnd telling KDE to take some hints from GNOME is also insulting.  KDE and GNOME cooperate and interact quite well, offering hints, code, standards, and media to eachother.  As for KDE's marketing and fundraising, I understand that your comment is almost two years old, but even considering that, KDE's community saturation has been phenomenal, especially considering its young age.  KDE has been well managed.  I don't know what made you think that KDE needed help in that area."
    author: "Benjamin Vander Jagt"
  - subject: "Some great ideas"
    date: 2005-10-09
    body: "On a page on gnome wiki I found some great ideas for the next version of gnome that could be implemented in kde too. The page (http://live.gnome.org/ThreePointZero) is more focused on new functionaly in the desktop than the GUI.\n\nI found this really interesting:\n\n\"Application Structure: Applications in it current form scatter all over the system when being installed. Stuff gets in /bin, /sbin, /usr/share and wherever. When looking at an application as a single object, apps should be in a single file (much like an rpm, except that the files won't be distributed across the system). Applications can indicate what they are capable of in a capabilities.xml or something like that so that the system can index which apps can do what. This will also make it very simple to deactivate a app without deinstalling it. When apps are a single (archive) file, they can very easily be distributed, installed (simply put it in the apps dir using the \"Application Manager\"), deactivated (click on \"deactivate\" in the \"Application Manager\") of removed (you get the idea). Another good point on this is that this makes security much easier, you can simply allow or disallow rights to apps based on their codebase since all bins of an app are in or under 1 dir/archife. Image how easy it would be to limit bandwith to apps, to allow/deny internet access, to allow/deny users to use the app.\"\n\nToday installing or removing an application is a headache. This will simplify things. I think Mac has a similar approach on his desktop."
    author: "Nicolas"
---
<a href="http://tom.acrewoods.net">Tom Chance</a> discusses the <a href="http://software.newsforge.com/software/05/09/19/1616206.shtml?tid=130">developments happening in KDE 4</a> on <a href="http://www.newsforge.com">Newsforge</a>.  He looks at the <a href="http://appeal.kde.org/">Appeal group</a> which brings artists and usability experts together with developers at the earliest stages of development, the context linking platform <a href="http://appeal.kde.org/wiki/Tenor">Tenor</a> and the ideas coming together for the KDE 4 desktop <a href="http://plasma.kde.org/">Plasma</a>.  He also looks at <a href="http://www.kdedevelopers.org/node/1398">RuDI</a>, a library which would help ISVs integrate with KDE.  "<em>All of these innovations, if completed, will make KDE an even better desktop environment. When next year's aKademy rolls around, it will be interesting to see what kind of progress has been made.</em>"


<!--break-->
