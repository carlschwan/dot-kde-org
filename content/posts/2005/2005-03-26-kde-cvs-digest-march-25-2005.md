---
title: "KDE CVS-Digest for March 25, 2005"
date:    2005-03-26
authors:
  - "dkite"
slug:    kde-cvs-digest-march-25-2005
comments:
  - subject: "OT: KWebDesktop?!"
    date: 2005-03-26
    body: "Sorry for being offtopic: I'm looking for an equivalent of the MS ActiveDesktop. I want to have a website as my fullscreen-desktop-background.\n\n*IF* kwebdesktop worked, I could have a perdiodically updated *image* of a webpage on my desktop. But since I might also want to fill out forms on that page, I need a konqueror that runs in the background, without a taskbar entry, without toolbars, window decoration etc.\n\nDoes that exist?"
    author: "me"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2005-03-26
    body: "I know this is possible but not exactly how. \nRight-click on the desktop and choose properties, then background, then \"advanced options\" or something. There can you choose to run a program in the background. You'll have to type in some commands I don't know.\nGood luck! Let me know If you find out how to run Konqueror in the background, find the right commands!"
    author: "jeroen"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2005-03-26
    body: "You would only need to run KHTML, not Konqueror (Konqueror is just a thin wrapper around KHTML and the other KIO-Slaves)"
    author: "Corbin"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2005-03-26
    body: "If he wants to submit forms or click links, he needs the full Konqi, not just KHTML.  KHTML will only render a web page.  It will not follow a link, such as a form submission."
    author: "Mark Taff"
  - subject: "Not True"
    date: 2005-03-28
    body: "This is false - KHTML will follow links / form submissions / etc. All you have to do is connect a few signal/slot combinations and you are ready to rock.\n\nSee kdenonbeta/kafilah for a very thin wrapper around KHTML, that is basically a fully functional browser."
    author: "Jason Keirstead"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2005-03-26
    body: "rightclick on the windowdecoration of a konqi and go to advanced - specific window settings (? translated from dutch). there it is. you'll have to play with it a bit, to make sure not all konqi windows become this way :D"
    author: "superstoned"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2005-03-27
    body: "You could experiment with kstart and use a custom profile without toolbars and menubar, using this as a starting point:\n$ kstart --keepbelow --fullscreen --alldesktops --skiptaskbar --skippager konqueror --profile myplainprofile <url>"
    author: "hugo"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2005-03-30
    body: "Why not just put a fullscreen Konqueror (Ctrl-Shift-F) without toolbars (disable them from Settings then save a view profile) on a seperate desktop such as Desktop 5 ad then switch back and forth using Ctrl-F1, Ctrl-F2, ... Ctrl-F5 and so on?\n\nUNIX has had excellent window and desktop management for decades, no need for something silly like kwebdesktop. Should be possible to even load this setup automatically from a session, although maybe some DCOP scripting is required.\n"
    author: "Rob"
  - subject: "Re: OT: KWebDesktop?!"
    date: 2007-02-24
    body: "sorry no, not silly at all my friend.\n\nit paves the way for mootools powered desktop ui that suit your taste and needs\n\nbest part is that your favourtie desktopUI developer on the fictional GnoDubDesktop.org is most likely from rhajastan....or Ankmorporhk. 1ghz pcs are falling off ftrucks like no tommorrow thanks to vistas new requirements....\n\nthe rampant consumerism of those more financially endowed than I fuel a underground hardware economy...simply because they view their old hardware like the plague and wish no more of the old beast than for it to walk out the door itself.\n\nand usually...more often than not... you see these computer boxes on the side of the road...hard rubbish i think they call it.\n\ni call it cluster birth,"
    author: "airtonix"
  - subject: "question about subversion"
    date: 2005-03-26
    body: "I quote (from http://svnbook.red-bean.com/en/1.1/ch02s03.html):\n\"Figure 2.7, &#8220;The repository&#8221; illustrates a nice way to visualize the repository. Imagine an array of revision numbers, starting at 0, stretching from left to right. Each revision number has a filesystem tree hanging below it, and each tree is a &#8220;snapshot&#8221; of the way the repository looked after a commit.\" (see http://svnbook.red-bean.com/en/1.1/ch02s03.html#svn-ch-2-dia-7)\n\nWhat's the limit for that global revision number N ? 65535, 2^32 ? 2^64 ? With a big project like kde, I think that question is very important. "
    author: "Capit Igloo"
  - subject: "Re: question about subversion"
    date: 2005-03-27
    body: "The revision number seems to be a 32-bit int. See http://svn.haxx.se/users/archive-2004-05/0820.shtml."
    author: "Daniel Mitchell"
  - subject: "Re: question about subversion"
    date: 2005-03-27
    body: "You'd have to commit for a very, very long time to reach the limit and it is even larger on a 64-bit machine.\n\nOn interesting point about Subversion is that it tracks changes across a whole repository, so it is much more important to think about how your repository or repositories are organised. If the changes you commit are not logical to you then you're in a bit of trouble. I hope they've been doing some thinking on that."
    author: "David"
  - subject: "Re: question about subversion"
    date: 2005-03-27
    body: ">\u00abYou'd have to commit for a very, very long time to reach the limit and it is even larger on a 64-bit machine\u00bb\n\nFrom the thread http://svn.haxx.se/users/archive-2004-05/0820.shtml (thanks Daniel Mitchell for that URL), I read : \u00abSo you'd have to commit every second for the next 60 years or so, to use up all 2 billion revisions. On a 64-bit machine, it would take much longer\u00bb.\n\nSo, finally, the only disadvantage I see for svn again cvs is the license: svn is free, but its license is not compatible with the GPL (see http://subversion.tigris.org/servlets/LicenseDetails?licenseID=9 and http://www.gnu.org/licenses/info/Apache.html). Only one disadvantage, but a big one.\n\n>\u00abOn interesting point about Subversion is that it tracks changes across a whole repository, so it is much more important to think about how your repository or repositories are organised. If the changes you commit are not logical to you then you're in a bit of trouble. I hope they've been doing some thinking on that.\u00bb\n\nOn the contrary, with, svn, if you're repository's organisation is not good, you can simply modify it (you can move, rename files and directories without losing the history of those files and directories, and with the history of those moving and renaming), you can't do that with cvs."
    author: "Capit Igloo"
  - subject: "gmail"
    date: 2005-03-27
    body: "speaking of khtml, I can't access gmail on standard mode, I always get:\n\"Oops...the system was unable to perform your operation.\nPlease try again in a few seconds.\" and then konq crashes.\ndoes this happen to other people?"
    author: "Pat"
  - subject: "A few more steps on the road towards perfection..."
    date: 2005-03-27
    body: "Applause and hooray to Michael Brade. Can't await KDE 3.4.1 with this fix. :)"
    author: "Carlo"
  - subject: "Will Kate replace blank lines?"
    date: 2005-03-28
    body: "If I Search and Replace on the REGEXP: ^ *$, and replace with nothing, will it now remove blank lines?  Or, is there something else that is needed to replace blank lines?\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Will Kate replace blank lines?"
    date: 2006-02-11
    body: "Well, it looks like the answer is sadly NO. ; BTW, thanks for the reminder about the \"new line\" regexp char.\n\nthe question remains : with *what* will kate replace it ?\nI'd really like to leave my old & ugly \"cleanup\" perl script where it is, and do it within KATE, that I'm really considering as my day2day editor. This app is juste great. Well, KDE is great. Sorry about that digression.\nI don't natively speak english, so its quite hard for me to try and elaborate on that one, but on question is burning my lips : Why can't the \"Replace with\" field output regexps too ?\n\nThanks for any tips, even late, I checked the \"Notify\" box, so I *will* get you answer\n\npX"
    author: "pX"
---
This week's <a href="http://cvs-digest.org/index.php?newissue=mar252005">KDE CVS-Digest</a>:
dnssd adds invitation support.
<a href="http://www.koffice.org/kchart/">KChart</a> adds PNG export.
KPDF adds annotation support.
Speedups in <a href=" http://www.konqueror.org/features/browser.php">khtml</a>, KPDF, <a href="http://kmail.kde.org">KMail</a>, and Plastik.
Plus, getting ready for <a href="http://subversion.tigris.org">Subversion</a>.
<!--break-->
