---
title: "Third Osnabrueck PIM Meeting"
date:    2005-01-20
authors:
  - "dmolkentin"
slug:    third-osnabrueck-pim-meeting
comments:
  - subject: "Osnabruck"
    date: 2005-01-20
    body: "Osnabruck must be fun although I was told there were pubs without alcohol in that town. "
    author: "Jakob"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "I am living in Osnabr\u00fcck and I know the pubs :) Never seen a pub without beer (especially Erdinger Weizenbier). Pretty nice town, btw."
    author: "Carsten"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "Erdinger? I thought the northern German people prefer Pils ;)\n\nlippel (who was born in Georgsmarienhuette)\n\n"
    author: "lippel"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "Indeed, esp. Becks, but I prefer drinking Weizenbier and this means usually Erdinger."
    author: "Carsten"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "And Erdinger is called Wei\u00dfbier, as it's from Oberbayern and there we don't call it Weizenbier ;-)"
    author: "Philipp"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "Uh - those 'white' beers are horrid"
    author: "anonymous"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "Let me explain, as a native Bavarian beer-drinker: Both descriptions are right. Though, people here in Munich prefer to call it \"Wei\u00dfbier\", and people in other parts of Bavaria call it \"Weizen\". I have not so often heard the description \"Weizenbier\". But \"Wei\u00dfbier\" is more official. \n\nNevertheless I prefer Schneider Weizen. I know Georg Schneider (the boss of the brewery) personally, and I know that they have excellent production standards. Besides, it is very tasty. \n\n.... but there's no use discussing taste. :-))\n\nMarkus"
    author: "Markus Heller"
  - subject: "Re: Osnabruck"
    date: 2005-01-21
    body: "Schneider Weisse indeed rocks, I even prefer it over Erdinger. It is a pitty that in Holland it's hard to get."
    author: "Debian user"
  - subject: "Re: Osnabruck"
    date: 2005-01-21
    body: "Not where I live... The Poort van Kleef in Deventer stocks Erdinger, Schneider and a few other German beers."
    author: "Boudewijn Rempt"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "It depends. I wouldn't drink an Erdinger, but an Augustiner is lucious. I prefer a good Pils, though."
    author: "Carlo"
  - subject: "Re: Osnabruck"
    date: 2005-01-20
    body: "Well, last year we were in this Arabic (?) pub and they didn't serve any alcoholic beverages. So last year there was at least one pub without beer in Osnabr\u00fcck. I guess you just don't go there. I can't blame you. :-)"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Osnabruck"
    date: 2005-01-21
    body: "Excellent, Erdinger is one of my favourite beers -- they sell it in the Asda near where I live (sardly part of Wal Mart :( )"
    author: "Chris Howells"
  - subject: "Re: Osnabruck"
    date: 2005-01-21
    body: "Exactly. Note that there is exactly one pub in Osnabr\u00fcck that make their own beer (Rampendahl, in Hasestrasse IIRC)."
    author: "Arnd"
  - subject: "Evolution to be ported to windows"
    date: 2005-01-20
    body: "at least, that's what the Ximian people inside Novell are going to try. From my point of view, that a bold venture. They'll need to apply massive changes to get it work, and it may even got stuck in the middle. Time will tell, if it's a good idea to already advertise it while they haven't done anything to the code yet.\nI'm very pleased with the ongoings on KDE-PIM though. Kontact matures apparently. I think it would be a wasted effort to (officially) try to port Kontact/kdelibs to windows (well you never know, if kdelibs can be ported, maybe we'll even see apps like Kontact on windows one day)\n\nbtw.: I'd still like to see a solution to ease the attachment of files to appointments like discussed in this bug:\nhttp://bugs.kde.org/show_bug.cgi?id=75047\n\njust my 2 cents\n"
    author: "Thomas"
  - subject: "Re: Evolution to be ported to windows"
    date: 2005-01-20
    body: "As we people from Oche (== Aachen in Germany) say \"Net mulle, werke.\" (\"Don't talk, work.\"):\nhttp://lists.kde.org/?t=110528165700001\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Evolution to be ported to windows"
    date: 2005-01-23
    body: "Waste of time. Kontact/KMail etc. are KDE integrated apps."
    author: "David"
  - subject: "Re: Evolution to be ported to windows"
    date: 2005-01-20
    body: "Evolution... Now that chimes in with a discovery I made yesterday evening. I'm porting the Wet & Sticky paint model to Krita, and I needed a cut & dried rgb to hls color conversion routine. That particular routine wasn't yet in the KOffice painter lib, so I googled for something cut & pastable.\n\nIt turns out that (at least in some version) Evolution contains not just code to draw a bevelled button, but that that code also contains its own particular colourspace conversion routines:\n\nhttp://www.gxsnmp.org/CVS/evolution/notes/e-bevel-button-util.c\n\nI don't like to use the word 'bloat' in combination with code, mostly because that almost invariably exposes one as a complete ignoramus, but, well, I'd be tempted if I were sure that this code is actually inside a compiled Evolution."
    author: "Boudewijn Rempt"
  - subject: "Re: Evolution to be ported to windows"
    date: 2005-01-20
    body: "Great.. so they'll have the same beveled buttons on windows as on linux. Cool stuff. It has to be so much fun to reinvent the wheel over and over again, I guess... What else did they build in? \n\n\"In order to make an apple pie from scratch you first have to create the universe.\" "
    author: "Thomas"
  - subject: "Re: Evolution to be ported to windows"
    date: 2005-01-20
    body: "Jaroslav Staniek has already ported Kexi to Windows, why should it not be possible to port Kontact/kdepim especially because a windows based gpl'ed qt library release 3.3 is also available. \n\nKexi on windows     - http://iidea.pl/~js/qkw/ \nQT/Win Free Edition - http://kde-cygwin.sf.net/qt3-win32/. \n\nRalf \n\n\n"
    author: "Ralf Habacker"
  - subject: "D'oh!"
    date: 2005-01-20
    body: "If I had known that I would have gone to my hometown! (I'm quite sure that this is not correct English but I cannot do it better, sorry...)\n\nBy the way: Osnabr\u00fcck is the town where, according to a study, the happiest people of Germany live in. I can agree to that...\n\nAnd I think, alcohol is no problem there. It's up to you to get it into a casual context ;-)"
    author: "maestro_alubia"
  - subject: "Re: D'oh!"
    date: 2005-01-20
    body: "> If I had known that I would have gone to my hometown!\n\nWhy? This was an invited-only developer meeting."
    author: "Anonymous"
  - subject: "Re: D'oh!"
    date: 2005-01-20
    body: "Yes, you are right, but I do not think that the developers will isolate theirselves totally, so that there would have been the chance to talk to them somewhere (maybe in a pub after the meeting).\n\n"
    author: "maestro_alubia"
  - subject: "KDE4 Timeframe"
    date: 2005-01-20
    body: "From the meeting notes: 1+ years? Oh no!!! That's too long ;)\n"
    author: "Anonymous"
  - subject: "Re: KDE4 Timeframe"
    date: 2005-01-21
    body: "Not really, I expect KDE 4 to take at least 1-1.5 years from the 3.4 release. It's a big change."
    author: "Tom"
  - subject: "Re: KDE4 Timeframe"
    date: 2005-01-21
    body: "Yeah, I know... But wouldn't you want to see a feature complete and stable KDE 4 the day after KDe 3.4 is out? \n\nKDE 4 promises so many important/useful changes, I can't wait to see it happen. Then again: I know I should be patient. Developers: Keep up the good work! It is highly appreciated!!"
    author: "Anonymous"
  - subject: "something about kdepim / Network dep."
    date: 2005-01-20
    body: "Hi.\n\nRight now there is only a Qtopia Konnector available  when using Multisynk or Kitchensync.\nBut there is also a IRmC Konnector , for Handys like Motorla T610 t630 in Kde-bluetooth. The Problem was that it has dependencies on KDEPIM and KDEBluetooth.\nI also think that there is an SyncML Konnector , for Syncing newer devices or handies which already understand Syncml. (or pda's). \nCan you make a plan to include those ? i used the irmc konnector with bluetooth quite a while, to sync my t630 with kadressbook, and its perfectly stable. \n\nThe inclusion of the mostly Bluetooth related sync stuff would benefit many people and would kick off sync usage and device integration.\n\nare there plans to talk with the kde-bluetooth people for collaboration ?\n\nAlso a systray daemon for syncable devices would be nice if they are getting in range (detected).\n\n\nso - any comment on KDEPIM<->KDE-BLUETOOTH ?\n\n"
    author: "xor"
  - subject: "Re: something about kdepim / Network dep."
    date: 2005-03-19
    body: "I did once get it to work under KDE3.2.  Syncing Evolution1 with either IRmC and the LDAP plugins.  However, when I install Multisync and Evolution2, it looks as if it compiles all necessary plugins but won't show any of them under the resources.  Just the default plugins as local, Qtopia etc.  Right now, under KDE3.4, I can't even view my resources.  It just crashes.\n\nShould I compile all necessary apps manually with certain features enabled or disabled?\n\nAny help would be appreciated.\n\nThanks.\n\nVictor,"
    author: "Victor"
---
For the third time, the town of Osnabr&uuml;ck was the place for a lot of <a href="http://pim.kde.org">KDE PIM</a> hackers to hold their annual meeting, kindly hosted by <a href="http://www.intevation.de">Intevation</a>. While the 
<a href="http://pim.kde.org/development/meetings/osnabrueck3/group.php">participants</a> were primarily focused on fixing bugs and getting the various 
PIM applications in shape for the KDE 3.4 release, some discussions also 
targeted post-3.4 development.
<!--break-->
<p>With over 40 bugs fixed, some <a href="http://www.kdedevelopers.org/node/view/809">interesting</a> <a href="http://webcvs.kde.org/kdepim/libkdepim/kpartsdesignerplugin">features</a> implementented, the <a href="http://www.kolab.org">Kolab</a> hackers participating, and the 
<a href="http://www.groupdav.org/">draft for GroupDAV</a> revised with Helge Hess from <a href="http://www.opengroupware.org">OpenGroupware.org</a>, this 
meeting was not only a successful boost of KDE PIM development, but also for 
PIM client-server interoperability in general. </p>

<p>Read the <a href="http://pim.kde.org/development/meetings/osnabrueck3/overview.php">meeting report</a> for details.</p>



