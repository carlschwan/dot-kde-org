---
title: "LugRadio: A KDE Update from Aaron Seigo"
date:    2005-12-20
authors:
  - "msmith"
slug:    lugradio-kde-update-aaron-seigo
comments:
  - subject: "Hail Aaron!"
    date: 2005-12-20
    body: "Aaron you could make KDE 4 as fast as athene and stylish more than athene desktop.\n\nhttp://www.rocklyte.com/athene/\n\nThe best thing about athene - the widgets are very polished. menubar, titlebar, statusbar, toolbars all blend nicely, unlike kde's."
    author: "Larry Woods"
  - subject: "Re: Hail Aaron!"
    date: 2005-12-20
    body: "of course Aaron can do that. He can do anything, for He is a God.\n\nahem.\n\n:D"
    author: "superstoned"
  - subject: "Re: Hail Aaron!"
    date: 2005-12-20
    body: "you must be stoned :p"
    author: "Mark Hannessen"
  - subject: "Re: Hail Aaron!"
    date: 2005-12-20
    body: "bah... I think it looks like like crud. Not to mention that it doesn't support any current-gen video cards. But seriously, Plastik + crystal looks light-years better."
    author: "hanover"
  - subject: "Re: Hail Aaron!"
    date: 2005-12-20
    body: "That's one of the ugliest environments I've seen.\n\nIf you think that looks nice, you could probably get a job in Sun's desktop graphics department."
    author: "Robert"
  - subject: "Nice to know they are human beings"
    date: 2005-12-20
    body: "Aaron? Aaron Seigo? Never heard of that name before. Some kind of KDE contributor wanna be?\n\nBut seriously, more ability of listening to people speak and human beings communicate either via voice or trough both visuals and sounds (like human to human communication (!)) is something I think which could really help KDE.\n\nI mean, we have a thousand KDE posts spread all over the globe and in every corner of cyberspace, but listening to discussions, seeing discussions seeing human beings, why not focus on that more as well? And I don't mean long monologs, but discussions where you hear human beings communicate in stead of shout in text.\n\nPerhaps some more \"humanity\" also enables much more understanding and 'feeling' more connected. Take for example what Microsoft is doing on Channel9, which is a good means of seeing the people. I bet KDE could up that greatly due to more transparency for example. etc. etc. I dunno, I just really appreciate hearing the people who are involved in these projects.\n\nApart from this, interesting talk, thanks."
    author: "ac"
  - subject: "Re: Nice to know they are human beings"
    date: 2005-12-20
    body: "> Aaron Seigo? Never heard of that name before.\n\nYou must be new here. ;)\nhttp://www.kde.nl/people/aaron.html\nHe is currently a full-time KDE programmer sponsored by Trolltech."
    author: "blacksheep"
  - subject: "Re: Nice to know they are human beings"
    date: 2005-12-21
    body: "He was probably joking.\n"
    author: "Reply"
  - subject: "Re: Nice to know they are human beings"
    date: 2005-12-21
    body: "He MUST have been joking.\n\n:-)"
    author: "Debian User"
  - subject: "dependency on Qt"
    date: 2005-12-20
    body: "Is KDE4 going to use the SVG code of QT?  If that is the case it creates more dependency on Qt which is unnecessary in my opinion.  \n\nKDE also has it's own SVG-code and it is being developed intensively for use in the new khtml for KDE4..  So I wonder why use the code of Qt..  Can someone give more explanation?\n\ngreetings\nFilip"
    author: "Filip"
  - subject: "Re: dependency on Qt"
    date: 2005-12-20
    body: "ouch sorry,\nwrong post in the wrong place.  Answers already provided in the thread on Qt 4.1.  If you can, you may remove these posts.\nFilip"
    author: "Filip"
  - subject: "Re: dependency on Qt"
    date: 2005-12-20
    body: "They are complimentary not competing.\n\nQt's SVG implements the \"Tiny SVG\" spec.  It is good for quick renderings (e.g. icons) but doesn't implement DOM or on-the-fly manipulation of the SVG rendering.  KSVG2 is implementing the full specification, which will provide the full capabilities of SVG, but be more suited to other tasks."
    author: "ltmon"
  - subject: "Re: dependency on Qt"
    date: 2005-12-22
    body: "complementary ? \nif ksvg2 implements the full svg spec , then it probably implements the tiny svg spec too, so if you want to manipulate your icons (link adding status text, or animations when hovering-we should use ksvg2 for that task too.\n\nso i think , it probably just a performance and memory thing."
    author: "chri"
  - subject: "Re: dependency on Qt"
    date: 2005-12-22
    body: "you can manipulate and animate images using QtSVG, but it's all pre-render. that's good enough for things like adding status text, hovering, etc.. ksvg2 would be immense overkill."
    author: "Aaron J. Seigo"
  - subject: "Support of the FD.o Icon Theme standard"
    date: 2005-12-22
    body: "This standard is based on KDE's icon methods.  But, I have to take issue with the statement that KDE supports the standard.\n\nWe do not have a set of HiColor icons line GNOME does.\n\nKDE code is directly dependent on the CrystalSVG icon theme.\n\nThe standard specifies exactly how inheritance, resizing, and search order should work and they do NOT work correctly in KDE.\n\nYes, I have reported the bugs: 114527, 106542, 59901, & 53052\n\nA much needed feature is for the icon theme selection to work cross platform.  If you start a KDE application while running the GNOME desktop, the apps now use the icon theme that is selected in the KDE Control Center rather than the icon theme selected with the GNOME configuration tools.  This is something that the FD.o standards need to address -- a standard way for this to be stored so that apps for all platforms can access it."
    author: "James Richard Tyrer"
  - subject: "Bunch of monkeys"
    date: 2005-12-22
    body: "Did anyone else notice how well Aaron came off compared to the hosts?\n\nThe LugRadio guys sound like a bunch of monkeys bickering for food.  I admire Aaron's ability to go on the show and still sound sane.  They seem so obviously anti-kde in all their snide remarks and questions.  In all the talks they have had about kde-related stuff (that I have heard) they raise issues that are just aimed to infuriate the kde crowd rather than actually constructively criticise the project. \n\nI realise that talk-show hosts need to display a bit of flamboyance to keep the show sounding exciting, but these guys just sound pathetic to me. \n\nIt's cool that Aaron is doing such a great job, and I'm especially impressed by his ability to communicate ideas.  Keep up the good work Aaron!\n\nMatt"
    author: "Matt"
  - subject: "Re: Bunch of monkeys"
    date: 2005-12-26
    body: "You are insulting all monkeys!\nThey are like gnomes bickering on a compost heap."
    author: "reihal"
  - subject: "Re: Bunch of monkeys"
    date: 2006-01-02
    body: "So have you listened to more than one show? Pathetic monkeys. What? The guys at lugradio do a fantastic job and their work should be praised. Honestly, have you listened to the pure rubbish that is already out there. \n\nThey do a really good job with such little resources. How can you possibly expect them to do anymore and get everything right.\n\nFor example. They slate gentoo all the time and they say XP is way faster than linux. This is wrong. Especially if you run gentoo. Do you see me getting into a fuss about this. NO. Take the rough with the smooth. Bunch of monkeys bickering for food. Interesting choice of words but you might want to consider thinking about what you type the next time you return to your machine.\n\nPersonally i originally used kde for about six months and found it to be mainly bloat eye candy for kids. Gnome on the other hand is faster, stabler and has less bloat. There is also the licensing issue which is more purer to the cause. I like kde dont get me wrong but the real hardcore linux users dont. The majority use the terminal anyway to do most of their work. Keep an opened mind."
    author: "Marc"
  - subject: "Re: Bunch of monkeys"
    date: 2006-01-03
    body: "I agree with the basis of your comment - I listen to LUGradio all the time and just ignore anything derogatory that they say about KDE/Gentoo/etc. It's still a very entertaining show.\n\nThere's no need to spout rubbish about KDE yourself, though. I consider myself a \"hardcore linux user\" and I much prefer KDE. You speak of terminal use, but for one thing I don't think anyone could ask for a better terminal program than Konsole - it's fast and featureful. At least a couple of versions ago there was absolutely no comparison between Konsole and any other terminal application either speed or functionality-wise."
    author: "Paul Eggleton"
---
<a href="http://www.lugradio.org/">LugRadio</a>, the online radio show of the <a href="http://www.wolveslug.org.uk/">Wolverhampton Linux User Group</a> has an interview in <a href="http://www.lugradio.org/episodes/40">their latest episode</a> with KDE hacker <a href="http://aseigo.blogspot.com/">Aaron Seigo</a>.  The appearance consists of a five-minute update on where the KDE desktop is heading, cool stuff they are working on and KDE's relationship with <a href="http://freedesktop.org">freedesktop.org</a>.  Start listening 30 minutes in for the update.




<!--break-->
