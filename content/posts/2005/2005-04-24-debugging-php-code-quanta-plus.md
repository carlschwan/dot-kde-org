---
title: "Debugging PHP Code with Quanta Plus"
date:    2005-04-24
authors:
  - "amantia"
slug:    debugging-php-code-quanta-plus
comments:
  - subject: "Neat"
    date: 2005-04-25
    body: "Did the rather simple installation and setup, lo and behold, it works!\n\nWith large scripts you may want to increase the server timeout.\n\nVery cool, and just in time to track down a bug in my code.\n\nThanks to everyone.\n\nDerek"
    author: "Derek Kite"
  - subject: "Sweet :-)"
    date: 2005-04-25
    body: "Man the Gubed homepage looks sweet with all those KDE whistles and bells :-)\n\n~Macavity"
    author: "Macavity"
  - subject: "Good Gubed docs"
    date: 2005-04-25
    body: "I've been reading about how the Gubed debugger works on the site. The docs are very good, with clear diagrams about how it all fits together with the web request. \n\nWe we discussing ruby/Rails support in the recent Dot KDE story about Quanta, but I didn't really know what was involved. I can think about what would be needed now to do the same thing in ruby. Maybe the same wire protocol over the socket connection could be used for a ruby debugger, I'm not sure - that wasn't in the docs. "
    author: "Richard Dale"
  - subject: "Re: Good Gubed docs"
    date: 2005-04-25
    body: "Hi Richard,\nin fact our idea was that any scripting debugger should be pluggable. Of course there can always be surprises but we would love to expand our capabilities. In addition to this there is the document type package which manages tag auto completion and structure. If you have an itch to do this just let us know how we can help."
    author: "Eric Laffoon"
  - subject: "no packages == no go"
    date: 2005-04-29
    body: "This is the umptieth tutorial aimed at beginners that suggests installing software from source. When will the world realise that installing from source is very bad from a maintainace point of view? Do you want all those users to reinstall -\u00e0 la windows- every year or so? installing from source does not provide easy upgrades, dependency maintainance and removal. You will have to do it all manually yourself. \nThat itch is the reason package management was developed. Just ask kpackage, synaptic, apt or yum to install the software you want, and you are done. No hassle trying to hunt down the correct libraries or breaking other applications. \nNow what if a program has not been packaged for your distro yet? Well... maybe you are wanting too much. In the case of this program: it is at version 0.2 or something: not really stable software if you can judge from the version number. So it is no wonder really it has not been packaged yet. Their homepage does not even mention packages! So unless you want to be a developer of said package, are prepared to clean up the mess it makes, steer clear of it.\n\nJust my grumpy 2 cents worth.\n(and no, not installiong this stuff, although I use Quanta and php regularly)"
    author: "JelleB"
  - subject: "Re: no packages == no go"
    date: 2005-04-30
    body: "What are you talking about?\n\nIf you would have tried it you would see that you don't need to compile anything. All you need to do is to download the archive, extract it and copy a directory to the right place. There are no dependencies because you don't download a program - it's just a bunch of PHP scripts in a directory that need to be placed in you web root.\n\n\"it is at version 0.2 or something: not really stable software if you can judge from the version number\"\n\nThe low version number does not mean that it's not stable. If you want to give your opinion about something please at least give it a try!"
    author: "Tom"
  - subject: "Re: no packages == no go"
    date: 2005-04-30
    body: "well maybe this one is not chanting the ./configure && make && make install mantra. But it is software, right? Would you say that the are some sorts of software that does not need to be installed with a package manager? \n\nPersonally I think not. As they admid themselves, it is a security risk to put this sofware in a publically accesible webroot. So you need to have an intelligent install script that put is somewhere usable but safe, and changes some seting in the webserver to make it safe. \n\nAs for the version number: it clearly signals \"not yet ready\" to me. "
    author: "JelleB"
  - subject: "Re: no packages == no go"
    date: 2005-05-01
    body: "\"not yet ready\" != \"not stable\"\n\nNormally you don't debug scripts on your production server. It will never be complety safe to do this, no matter if you install it by hand or with \"packet management\" but don't you think it is more safe to do this by hand where you have to think about what you are doing?\n\nNormally you debug on your local developement machine while developing...\n\n"
    author: "Tom"
  - subject: "Re: no packages == no go"
    date: 2005-05-04
    body: "This is for developers to use as a tool to edit and debug PHP code.  Written in PHP.  Provided as PHP code.\n\nIs giving *developers* a tool that is only useful for active editing of code and is provided in the same format as their projects now undesirable?"
    author: "Evan \"JabberWokky\" E."
  - subject: "PHP_Debug"
    date: 2005-07-25
    body: "Hello, i have made a library to debug php code. You can check it here http://www.php-debug.com   See you.  Coil  :) "
    author: "Loic Vernet"
---
Ewald Kicker has written a <a href="http://www.very-clever.com/quanta-gubed-debugging.php">detailed tutorial about debugging a PHP script</a> inside <a href="http://quanta.kdewebdev.org">Quanta Plus</a> using the <a href="http://gubed.sourceforge.net">Gubed</a> debugger. The document is aimed at beginners and describes step-by-step how to set up and use the debugger for your web projects.



<!--break-->
<p>There is <a href="http://www.hoernerfranzracing.de/kde/gubed.html">an older Gubed tutorial</a> written by Werner Joss, now slightly out of date, but was the first detailed document about using Gubed inside Quanta Plus.</p>

<p>Both tutorials show how anyone can contribute to a Free Software project even if you are not a C++ developer.  If you do not have time even for writing documentation or tutorials, you can still use <a href="http://kdewebdev.org/donate.php">Quanta's donation page</a> to assure and support the future of Quanta Plus and <a href="http://kommander.kdewebdev.org">Kommander</a>.


