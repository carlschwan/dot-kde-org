---
title: "KDE CVS-Digest for February 4, 2005"
date:    2005-02-05
authors:
  - "dkite"
slug:    kde-cvs-digest-february-4-2005
comments:
  - subject: "Thank you"
    date: 2005-02-05
    body: "Thanks for the news!\n\nI can't wait till I get my hands on the next beta, preuerrably through a livecd D"
    author: "Matt"
  - subject: "Re: Thank you"
    date: 2005-02-05
    body: "Wasnt beta2 meant to be released yesterday? :("
    author: "Joff"
  - subject: "Re: Thank you"
    date: 2005-02-05
    body: "Tagged, not released. Release maybe next week."
    author: "Anonymous"
  - subject: "lots of great news!"
    date: 2005-02-05
    body: "Lots of great news in the digest! I saw blogs about more new and exciting things added this week such as:\n http://www.kdedevelopers.org/images/e3760da08ad6e1ba4a5862246e867490-837.png\n http://www.icefox.net/gallery/phpquickgallery/?gallery=2005%2FSystem+Preferences/\n http://developer.kde.org/~danimo/screenies/webdavfolder.html\nin which you will see the new about screen that has been added to konqueror, kontact and kmail, the new and very interesting proposal for kcontrol in kde 4.0 (by Benjamin Meyer) and a movie (recorded by Daniel Molkentin) to show how to connect to a WebDAV folder (and the remote:/ protocol is shown too).\nHave fun!"
    author: "alien"
  - subject: "Re: lots of great news!"
    date: 2005-02-05
    body: "Addition:  I just saw autoscrolling in search and mass colored highlighting in kpdf too"
    author: "alien"
  - subject: "Re: lots of great news!"
    date: 2005-02-05
    body: "Yuck, that's one ugly preferences interface. Is this gonna replace kcontrol? I like kcontrol in it's tree view. :("
    author: "unlimited"
  - subject: "Re: lots of great news!"
    date: 2005-02-05
    body: "Come one, this new interface is way better for everybody.\nIt's basically a rip off from macosx, plus the added benefit that\nthe search bar finally has the place it merits, that is proheminent\nand right on the front, since it's the only usable and quick way to find \nthings when the amount of information grows too quickly.\n\nAnd if you want a tree view, add settings:/ in Konqueror's sidebar.\n"
    author: "jmfayard"
  - subject: "Re: lots of great news!"
    date: 2005-02-05
    body: "treeview is very nice "
    author: "ch"
  - subject: "Re: lots of great news!"
    date: 2005-02-05
    body: "If you like this interface more just because the search bar is best located, I find that a non-sense reason to replace the current one. I mean, let's just make the search bar in a better position in the current interface then."
    author: "blacksheep"
  - subject: "Re: lots of great news!"
    date: 2005-02-06
    body: "Take a closer look it IS a tree view, just with a far better layout.  The new GUI has much more potential (the way the search function works out) and it's easier to find things otherwise as well -- you can see the fine details and the big picture at once!  Not to mention the fact that it'd be much easier in this to have a really smart searching function that could be asked questions by novice users.  To top it off, integration of third party modules would seem more orthogonal!"
    author: "Saem"
  - subject: "Re: lots of great news!"
    date: 2005-02-06
    body: "> Take a closer look it IS a tree view, just with a far better layout.\n\nJust because the icons are larger doesn't make it \"better\".\n\nCurrently, I can go through many different settings without much hassle because I CAN SEE THE TREE AND THE SETTINGS AT THE SAME TIME.\n\nAs far as I understand, the new layout will always use the whole window or (even worse) spawn new windows for settings, so that will become impossible.\n\nAlthough I agree that the search function is displayed a lot better now, I don't need any big icons and I would prefer the old treeview a lot more.\n\n\n"
    author: "Roland"
  - subject: "Re: lots of great news!"
    date: 2005-02-06
    body: "You can't please everyone, but this new one will please a lot more people than the current one. It is just lovely to use, BenMeyer did a good job.\n\nAnyway, you'll still be able to use the old one, it's just a frontend to KCMs, like the new one is."
    author: "Max Howell"
  - subject: "Re: lots of great news!"
    date: 2005-02-06
    body: "All they did is make the tree take up the whole window, so that way its much harder to use.  In the new interface 4 groups of modules almost take up the whole screen, and I have TEN groups (which I think is the normal amount), so I see this thing as requiring a scrollbar and being full screen always...  This will be a pain for everyone that thinks OS X's system preferences is horribly designed, and also designed to show you the minimum amount of settings they can get away with.\n\nIf this replaces KControl, it will be very bad for KDE I believe.  KControl may not be perfect, but its better than the mess an OS X style control center would bring."
    author: "Corbin"
  - subject: "Re: lots of great news!"
    date: 2005-02-06
    body: "This new kcontrol does not solve the main problem in KControl : the unused modules, which we cannot make disappear (vaio laptop, laptop batteries, etc.)\n\nGerard"
    author: "gerard"
  - subject: "Re: lots of great news!"
    date: 2005-02-06
    body: "This is an already addressed issue and worked on (by Frans Englich, IIRC). Obviously it will be finished for KDE4 (or at least, let's hope :)"
    author: "Davide Ferrari"
  - subject: "KolorPaint / KSnapshot"
    date: 2005-02-05
    body: "Actually they could integrate together neatly - KSnapshot provides a DCOP interface with just this in mind.\n\nCheers\n\nRich.\n"
    author: "Richard Moore"
  - subject: "feeback for \"what's this\"-help?"
    date: 2005-02-05
    body: "It is great that Kurt Pfeifle took the time to add lots of What's this-help texts. But even if his hope comes true one day and 80% of all widgets have an help text, it will still be a guessing game - \"Let's see.. maybe there is a detailed help text for this widget?\"\n\nThere is just no indication in the ui which tells the user if the long way to the top right corner and back is worth it. Wouldn't it be possible to let the What's-this-button light up if the user moves over a widget which has a help text? Or include a questionmark icon into the regular tooltip as a hint that there is more detailed information?\n\nAlso, if the user wants to explore the what's this-help texts, the interface should help him by highlighting all the widgets with help text when in What's this-mode.\n\nIt would be much less frustrating to get no detailed information if you know that it doesn't exist right away."
    author: "uddw"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-05
    body: "> There is just no indication in the ui which tells the user if the long way to the top right corner and back is worth it.\n\nWill be in KDE 4. Qt 4 snapshots include the necessary base."
    author: "Anonymous"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-05
    body: "Imho, there shouldn't be any indication.  Either the information should be entered properly by the app's creators, or the what's this option shouldn't be supported or even there at all.  Developers shouldn't be able to sit on the fence if they don't provide what's this support.  It should be obvious, because the button is missing."
    author: "Jel"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-06
    body: "and if there are 15 discrete possible targets and only 12 of them have WhatsThis entries?\n\nunfortunately we have to deal with reality (not all developers of all apps will get 100% WhatsThis coverage, or put WhatsThis everywhere they should such as text labels as well as their associated widgets) which leads to these sorts of quandries.\n\nso even when there is a WhatsThis button, the user is still left with the question: what can i click on?"
    author: "Aaron J. Seigo"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-06
    body: "Well, that's the problem.  There SHOULD be 100% coverage.  I mean, it's not a gargantuan task, to describe your interface in a few words while constructing it.  I've done it myself with no trouble.  The only time I end up with less than 100% coverage is when I get lazy, and that shouldn't happen at the released software stage.\n\nYou can try to make up for it all you want, but the fact is, it's simply an inconsistent and frustrating nightmare for users.  Either do a good, usable interface, or hack together something quickly, but don't try to do one and pretend it's the other."
    author: "Jel"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-07
    body: "> There SHOULD be 100% coverage.\n\ni agree. but for whatever reason (mostly HR issues) we don't have 100% coverage. \"all\" we need are dedicated people writing WhatsThis entries to turn our current reality into the utopia you describe. you obviously feel pretty passionate about this. \n\n> I've done it myself with no trouble.\n\nbrillian! as someone who's evidently quite good at this, maybe you can channel some of the previously expressed passion into helping solve the lacking documentation that makes things \"inconsistent and frustrating ... for users\".\n\n> Either do a good, usable interface, or hack together something quickly, but\n> don't try to do one and pretend it's the other.\n\nwell, it's possible that it isn't \"quickly hacked together\" AND things like WhatsThis aren't complete. as a software developer, you should know that's quite often the case. i don't think your statement is particularly fair to those working on this software you are likely using. but, you can certainly help improve the situation so that it becomes a moot point. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-06
    body: "I think there should be feedback when the what's this cursor is active. For example the widget under the cursor could be highlighted if there is a what's this text. An alternative would be to make the cursor the indicator.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-07
    body: "yep. this is possible with Qt4. in fact, the TT guys added this upon request just so we could do such a thing. =))"
    author: "Aaron J. Seigo"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-05
    body: "Whoaaaaah!\n\nThis was a really, really useful addition for me. I took the time (must have been more than an hour) to click myself thru all the new whatsThis help blobs about kdeprinter. What an amazing piece of software the whole of kdeprinter is I only am realizing now (having been a user of kde printing since 3 years!\n\nThe new whatsthis additions let kdeprinter shine even more (even if most of its functions are due to the underlying CUPS system).\n\nI guess there are a lot more KDE applications out there that could well bear with some of this kind of polishment.\n\nNow off to study the tutorial by Aaron Siego that was mentioned above, maybe I will now convert myself into a whatsthis contributor.  Where is an area in need of additions?"
    author: "good job!"
  - subject: "Re: feeback for \"what's this\"-help?"
    date: 2005-02-05
    body: "<AOL>I must agree here!</AOL>\n\nWhat I like most are the \"Additional hints for power users\" at the bottom. I think this is a very efficient way to attract guys like myself to read on, and scare my mom away. (Yes, she is using KDE too, and she reads the whatsThis items of kprinter now, and she frequently lurks on the dot even).\n\nHi, mom ;-)"
    author: "Anonymous Son"
  - subject: "DOM Editing"
    date: 2005-02-05
    body: "This opens many possibilities.\nf.e. kill images/flash (banners) for a site\nThis was one of the few things firefox had but konqueror was lacking.\n\ngo konqi go:)\n\nAdd DOM editing primitives to domtreeviewer.\n\nIt is now possible to\n- add/change/rename/remove attributes\n- insert/delete/move elements\non arbitrary existing documents.\n\nAnother item that can be ticked off on the feature list :-)\n\nCCMAIL: zack@kde.org, kde-kafka@master.kde.org, schlpbch@iam.unibe.ch\nFEATURE"
    author: "chulio martinez"
  - subject: "Re: DOM Editing"
    date: 2005-02-07
    body: "> This opens many possibilities.\n> f.e. kill images/flash (banners) for a site\n> This was one of the few things firefox had but konqueror was lacking.\n\nThis has nothing at all to do with DOM editing. You could already do this very easily in KHTML using the plugin API ( I have a plugin that does a simmilar thing  - changes all links in the page to use the Coral caching proxy ) - just no one has written a plugin to do it.\n\nDOM editing refers to having a WYSIWYG HTML editor.\n\n\n"
    author: "Jason Keirstead"
  - subject: "Qt-Mozilla"
    date: 2005-02-05
    body: "Dead again after a short live...?\nIs there a blog or website or Mozilla-bug that informs about progress?"
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-02-05
    body: "you have to grasp that mozilla code is horrible and hard to work around.  send tokens of food, sex and or money and it may happen faster. even better check out the CVS and go nuts.  nothing gets what you want faster than doing it yourself."
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-02-05
    body: "Is it just me or you're actually replying to yourself? ;D"
    author: "blacksheep"
  - subject: "Re: Qt-Mozilla"
    date: 2005-02-05
    body: "ac = anonymous coward"
    author: "Illissius"
  - subject: "Re: Qt-Mozilla"
    date: 2005-02-06
    body: "I have been checking the component \"Ports: Qt\" under bugzilla.mozilla.org and that seems to be a fine way to check progress.  It appears from this that progress is slow or stalled (very few active developers on the task as far as I can tell).\n\nIt would be a real shame if we lost the change of a Qt port of Mozilla and/or Firefox.  AFAIK it's the only non-Qt application I and a lot of other people I know use, and it currently integrates badly with KDE (file associations, printing, etc).\n\nKeith"
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-02-07
    body: "\nMyself being unable to use that hellish bugzilla advanced search interface, could\nsomeone please post the relevant bugnumbers?"
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-02-07
    body: "http://tinyurl.com/3v85u\n\nLooks like changes are being committed as recently as this month.  They look swamped and I'm sure they'd appreciate help, even if it's just finding duplicates and finding old bugs that have already been resolved (many of the older Qt bugs are from previous attempts to port Mozilla to Qt and may no longer be applicable)\n"
    author: "ac"
  - subject: "I love KTTSD"
    date: 2005-02-05
    body: "It is really nice to see Konqueror and Kwrite applications speak (of course using festival text-to-speech engine). \n\nThank you KTTSD and KDE Accessablility developers for wonderful applications.\n\nKTTSD can read out clipboard, web pages in konqueror, selection (selected text), and notification (information and error dialog messages).\n\n"
    author: "Fast_Rizwaan"
  - subject: "Re: I love KTTSD"
    date: 2005-02-05
    body: "I agree, great work by the KDE accessibility team. I can't wait until kontact, kopete, kword and other apps get fully integrated with kttsd."
    author: "ac"
  - subject: "Re: I love KTTSD"
    date: 2005-02-06
    body: "I think KTTSD should be rename to KITTY (KDE Is Talking To You)!"
    author: "me"
  - subject: "Re: I love KTTSD"
    date: 2005-02-06
    body: "KITTY is brilliant! :P"
    author: "Troy Unrau"
  - subject: "Re: I love KTTSD"
    date: 2005-02-08
    body: "loving it!"
    author: "pieter"
  - subject: "Re: I love KTTSD"
    date: 2005-02-06
    body: "Kate & Kitty :) A good duo indeed!"
    author: "dh"
  - subject: "Re: I love KTTSD"
    date: 2005-02-06
    body: "YES!"
    author: "Sam Weber"
  - subject: "LOL I love that"
    date: 2005-02-07
    body: "I second the name change! :)"
    author: "Matt"
  - subject: "Re: I love KTTSD"
    date: 2005-02-07
    body: "+5!"
    author: "konqui-fan"
  - subject: "media:/ and HAL"
    date: 2005-02-06
    body: "I wish KDE drop this stuff until there some information how to setup HAL and DBUS and go with an own kded-module for KDE 3.4!\n\nlinux's udev is such a nice thing and HAL makes it unnecessary complicated :-/ It prints tons of lines into the logs in verbose mode, no error messages, but it does not work. HAL and DBUS documentation is nealy a vacuum on freedesktop, HAL-CVS does not like DBUS-CVS... This stuff looks overengineered and is crap!\n\nCompare it with the kdebluetooth-plugin waiting in kicker that you plug in a bluetooth-dongle - that's nice!\n\nBye\n\n  Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-06
    body: "media:/ uses HAL only if you have it installed and set up. otherwise it uses more traditional means. so it's not like you won't have media:/ if you don't have HAL. this is good news for those who don't like HAL or who are on an OS that doesn't have it. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "Yo Aaron, Minor Deity of Cool Stuff and Nifty New Kicker Tooltips,\nJust a silly question: will there be something like a right-click menu entry on media:/ items where you can 'Add an action to run when this media is plugged in'? Will there be defaults for whole classes of media (like, for all cameras or USB keys or webcams)? That would be Way Good(tm), and I like things that are Way Good(tm). :)"
    author: "Anonymous Coward"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "An even better idea, I think: offer an API for apps to register themselves as willing to handle this or that device class when the device in question is plugged in. That way, you install, say, Konference, it registers itself as handler for webcams, and it handles your webcam automatically when you hotplug one. (It should probably leave it be if the webcam was already plugged in at boot though.)\n\nHow feasible would that be? Could it be doable in a desktop-interoperable way (ie, based on D-BUS rather than an in-kded solution), perhaps?"
    author: "Anonymous Coward"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "not for 3.4, no. this functionality is slated for the next release, however."
    author: "Aaron J. Seigo"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-08
    body: "No problem, I'll just delay my worshipping you a little, no big deal there. :D"
    author: "Anonymous Coward"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "For me, a Gentoo user, HAL was simple to install and test, and it works wonderfully with the new media:/ ioslave and the \"Storage Media\" panel applet in KDE 3.4 beta1. When I plug in a USB flash drive, an icon instantly appears next to my K Menu on kicker. When I insert a CD-ROM,another icon appears. HAL finally fills a void that has long been a need of mine. "
    author: "Braden MacDonald"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "Did you only have to emerge hal, or did you need to set some USE-flags or done something else to make it work with media:/ ioslave or kicker?"
    author: "LB"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-08
    body: "I simply had HAL and D-BUS installed when I emerged the new KDE. I didn't use any special use flags."
    author: "Braden MacDonald"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "does it work with the desktop? eg does a icon appear on the desktop, too?"
    author: "superstoned"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-08
    body: "I don't normally have icons on my desktop, but I turned them on, and yes, devices appear there dynamically as well."
    author: "Braden MacDonald"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-08
    body: "that's great.  I'm really looking forward to KDE 3.4!!! And even more to KDE 4.0, for sure..."
    author: "superstoned"
  - subject: "Re: media:/ and HAL"
    date: 2005-07-28
    body: "I just switched to KDE 3.4.1 and the \"Storage Media\" applet works well, but I would perfer to have the icons dynamically appear on the desktop. Could you tell me how to turn this on.\n\nthanks"
    author: "Janos"
  - subject: "Re: media:/ and HAL"
    date: 2005-07-28
    body: "I just switched to KDE 3.4.1 and the \"Storage Media\" applet works well, but I would perfer to have the icons dynamically appear on the desktop. Could you tell me how to turn this on.\n\nthanks"
    author: "Janos"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "I've written a bit of a HOWTO on setting up D-BUS, HAL and media:/ after I frustrated myself getting it working for a day or so.\n\nYou can find it on my server:\n\nhttp://mvgrafx.dyn.ca/~vmark/documents/DBUS-HAL-MEDIA-HOWTO.html"
    author: "Vic"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-07
    body: "Thanks Vic! This look very good. So - a last chance for hal ;-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-08
    body: "Thanks this is very good :) It would be even better if it was in the KDE WIKI\n*hint* ;)"
    author: "Max Howell"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-08
    body: "I signed up and added it to the Wiki... I'm new to Wiki's so let me know if I did something wrong."
    author: "Vic"
  - subject: "Re: media:/ and HAL"
    date: 2005-02-10
    body: "Looks great :) Good move putting it in the wiki as now it can be updated by anyone that learns a tip or trick. Now to follow the steps and get my own HAL system working..."
    author: "Max Howell"
  - subject: "KDE4"
    date: 2005-02-06
    body: "what's the plan for KDE 4.0?\n\nStart working on it when KDE 3.4 is out?\nFirst make a KDE 3.5, and then work on 4.0?\nOr start porting/working on kdelibs/kdebase to QT4 while we still have a KDE 3.5 release, with (almost) the same kdebase/libs as KDE 3.4?"
    author: "superstoned"
  - subject: "Re: KDE4"
    date: 2005-02-06
    body: "AFAIK from reading the blogs and whatnot, next is KDE 4."
    author: "Ian Monroe"
  - subject: "Re: KDE4"
    date: 2005-02-07
    body: "Yep, it's 3.4 then 4."
    author: "anon"
  - subject: "Re: KDE4"
    date: 2005-02-07
    body: "ow ok... And I guess it'll take some time? a year? or less? more?"
    author: "superstoned"
  - subject: "Re: KDE4"
    date: 2005-02-07
    body: "Don't expect a KDE 4 release in 2005..."
    author: "Christian Loose"
  - subject: "Re: KDE4"
    date: 2005-02-07
    body: "sad. will it beat longhorn in release date? I'd wish we had a polished KDE 4.0 before longhorn comes out... to show there isn't very much proprietary software has to offer, compared to what the free world can do."
    author: "superstoned"
  - subject: "Re: KDE4"
    date: 2005-02-07
    body: "Just want to mention my point of view...\n\nYou can't compare KDE with Windows.\nBoth are completely different things.\n\nKDE is a desktop environment.\nWindows is an operating system.\n\nEven if you compare KDE with the desktop environment of Windows, then there will always be some things one can do better than the other. And even that is sometimes a personal preference of doing things.\n\n\nAnd from what I read in articles (and don't take these articles for fact), Longhorn will not be released in 2005 either. Betas and Release candidates, yes, but a final product, probably not."
    author: "Tim Beaulen"
  - subject: "Re: KDE4"
    date: 2005-02-07
    body: "I agree. but comparisons of linux/windows are made all the time, and KDE plays a huge role in them... KDE is what the users see, and KDE is what the users use. It hides the internals of linux for them, exposing only what is usefull and understandable, while trying not to stand in their way if they want to explore more (konsole, man:/ kioslave, terminal in konqueror and kate - they make it easier to 'enter the real linux')"
    author: "superstoned"
  - subject: "Re: KDE4"
    date: 2005-02-13
    body: "> You can't compare KDE with Windows.\nYep, it's just too unfair to Windows.\n> Both are completely different things.\nThe Good, and the bad and ugly.\n\n> KDE is a desktop environment.\nThe best ever, that is.\n> Windows is an operating system.\nIt tries both and fails miserably (even if only few know about the full catastrophic extent).\n\nIf this really is a free market economy, the market hopefully will decide for the better alternative this time ;)"
    author: "thatguiser"
  - subject: "Congratulations KDE :)"
    date: 2005-02-06
    body: "KDE - has won Desktop Environment of the Year at LinuxQuestions.org MC Awards - :)\nKonqueror - has won File Manager of the Year.\nQuanta - has won Web Development Editor of the Year\nKdevelop - has won IDE of the Year.\n\nhttp://www.linuxquestions.org/questions/showthread.php?s=&threadid=286716\nhttp://www.linuxquestions.org/questions/forumdisplay.php?s=&forumid=62\n\nCouple of 2nd places etc aswell ;)\n\nThanks for all the excellent work over the years :)\n\n "
    author: "Anonymous"
  - subject: "and with style and gratitude, that is.."
    date: 2005-02-13
    body: "it IS simply the best DE mankind ever gave birth to! Hoooray for all the boobies!  KDE 4.0 will kinda be the abortion of Brutus Longhornus Betamus, muah. Fear the community, darnn Microsuxerz..."
    author: "thatguiser"
---
In <a href="http://cvs-digest.org/index.php?newissue=feb42005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?issue=feb42005">old layout</a>):

<a href="http://digikam.sourceforge.net/">Digikam</a> does black and white tonal conversion. KPDF implements history and <a href=" http://accessibility.kde.org/developer/kttsd/">KTTSD</a> (screen reader) support. <a href="http://kmail.kde.org/">KMail</a> adds graphical emoticons.
<a href="http://kdepim.kde.org/components/knotes.php">KNotes</a> implements read-only support. <a href="http://www.konqueror.org/">Konqueror</a> shows document title and favicon in location bar autocomplete. <a href="http://amarok.kde.org/">amaroK</a> supports the Akode engine.


<!--break-->
