---
title: "Interview with Will Stephenson"
date:    2005-05-21
authors:
  - "skoning"
slug:    interview-will-stephenson
comments:
  - subject: "and Jabber ?"
    date: 2005-05-21
    body: "Thanks very much for your work on Kopete, but it's very important that jabber support must be ameliorate.\nWith proprietary protocols, we aren't sure that Kopete works all the time (for example, with the changes in msn login, the stable kopete version don't work)\nMost Internet protocols are open, why with the IM is different ?\nThe Free Software must be promove open protocols :-)\nOpen Office and Koffice promove Open Document format, why the IM apps don't promove Jabber protocol ?\nI see that kopete uses now libiris (a library from psi developpers), kopete developpers helps psi developpers to maintain this librairy ?\nWhile a client jabber doesn't have same functions that msn messenger, migration on jabber are difficult."
    author: "GML"
  - subject: "Re: and Jabber ?"
    date: 2005-05-21
    body: "Looks like someone wasn't paying much attention during his English class."
    author: "Pat"
  - subject: "Re: and Jabber ?"
    date: 2005-05-21
    body: "I don't use Jabber because\n a) I somehow never figured out how to use it\n b) ICQ always worked for me\n c) all my friends only use ICQ\n\nWhy should I use Jabber? If you give me a good reason I might even use it but as of now I have no reason to use it..."
    author: "Carsten Niehaus"
  - subject: "Re: and Jabber ?"
    date: 2005-05-21
    body: "I think what he meant was that we should use it because it's FLOSS (free libre open source softawre).\nThe problem is that unlike operating system, DEs, mail clients etc... You need your friends to be using the same IM protocol. So using Jabber can be useless if you don't know anybody willing to switch \n\nIn a perfect world, all IM clients would be using the same protocol just like with other internet applications http, smtp,imap etc). Unfortunately it seems like the big ones (MS,AOL,Yahoo!)  never really understood the way internet works or rather they did understand but never wanted it this way :-(\n\nIf it was up to MS we would all have to use Outlook (or some outlook clone using reverse engineered outlook private protocol) to get our mail :)"
    author: "Pat"
  - subject: "Re: and Jabber ?"
    date: 2005-05-21
    body: "I totally agree that all should switch to Jabber as it is free and of course as it could be the RFC which defines a standard just like mails are based on RFC and so on.\nBut you say there is no Jabber->ICQ-Bridge or something so that I would use Jabber but my friend could still see me somehow? That way *I* would get the free thing but don't have any regressions."
    author: "Carsten Niehaus"
  - subject: "Re: and Jabber ?"
    date: 2005-05-21
    body: ">>But you say there is no Jabber->ICQ-Bridge or something so that I would use Jabber but my friend could still see me somehow? That way *I* would get the free thing but don't have any regressions.\n\nYes there are, well sort of. These are called gateaways. The problem is that you still need an ICQ number to use it. here is how you can do it:\n\n1)create a jabber account with kopete, chose a server such as jabber-hispano.org that propose such services\n2)get online\n3)right click on your jabber account icon and then click Services... in the context menu\n4)click on the button \"Query server\"\n5)double click on Transporte ICQ and enter your number and pass\n\nthat should be it\n"
    author: "Pat"
  - subject: "Re: and Jabber ?"
    date: 2005-05-22
    body: "Most servers allow subscriptions to their transports from foreign servers, so you can have your Jabber account for example on jabber.org and use the transport at amessage.de or jabber-hispano.org."
    author: "tigloo"
  - subject: "Re: and Jabber ?"
    date: 2005-05-22
    body: "That's how I use it, I have friends on both ICQ and MSN which I chat with through the gateways on jabbernet.dk. Been using it for quite a while and it's been very stable. \n\nI use Psi instead of Kopete though. So far, it just fits me better than Kopete. I can't really remember why, but I think one of the things were the logging functionality in Psi."
    author: "Joergen Ramskov"
  - subject: "Re: and Jabber ?"
    date: 2005-05-22
    body: "Others did already answer, I will try to give you some other reasons to switch:\n\n- Jabber is already an IETF protocol with official RFCs; look here http://www.jabber.org/protocol/ and here http://www.xmpp.org/\n- since jabber is an open protocol it works just like email [on the server side]: everyone can set up its own servers [http://www.jabber.org/network/], with proprietary protocols all your packets are going through AIM/MSN/Yahoo servers and you really don't know what's happening and what they do with your msgs....\n- you can setup a private/secure network with friends using SSL\n- Apple now supports jabber in his client apps [iChat] and in his server ... more jabber users are coming...\n\nthere are more reasons of course... ;-)\n\nciao\n"
    author: "ste"
  - subject: "Re: and Jabber ?"
    date: 2005-05-21
    body: "There is Jabber support in Kopete. It's not as complete as in Psi, but it should satisfy your everyday needs.\n\nFor those wanting to use other protocols, Jabber integrates all other protocols by providing transports that relay messages between Jabber and for example ICQ or MSN. You don't even need to have an account on the server providing the transport. Unfortunately, Kopete's support of transports is still very basic but we're working on that.\n\nAs for Iris, yes, it is Psi's backend library. We have always shared the backend with Psi and they're very friendly towards Kopete. Iris is a very mature and complete library (Psi is one of the best Jabber clients around) and we benefit a lot from the cooperation."
    author: "tigloo"
  - subject: "Re: and Jabber ?"
    date: 2005-05-22
    body: "I tried building a jabber server with an msn and icq backend.\nand actually got it running after some experimenting.\nbut the contactlist names imported by the transport looked real ugly.\nin my memory it looked something like user%domain@jabberserver.com\nI wasn't able to find out how to get it to display it by nickname.\nthat that is where I largely stopped using jabber.\n\nanother feature I greatly missed is the feature that allows you to see when another person is typing. I somehow seemed to got addicted to that...\n\nwebcam and voice support sure are things that would be really nice to have too, but since no IM under linux supports that at all I am not gonne bitch about that. I don't think that should be routed through the jabber server anyway, but it would be nice to have a jabber client that would support an easy way to get an video/voice link between jabber contacts.\n\nall in all I think that jabber is a very neat solution.\nit just isn't entirely there yet.\n"
    author: "Mark Hannessen"
  - subject: "Re: and Jabber ?"
    date: 2005-05-22
    body: "I don't use the MSN transport but you can always rename the contacts. Most clients (as well as Kopete) automatically suggest an appropriate nickname, so you don't even have to do it yourself.\n\nTyping notifications are supported by Jabber.\n\nVoice and video chat are not implemented in Jabber yet. An attempt has been made but work on the protocol has been suspended for the moment. Eventually Kopete will have a solution for this."
    author: "tigloo"
  - subject: "Interviews"
    date: 2005-05-21
    body: "Many interesting interviews lately. Hope to see more of them soon :)"
    author: "Anonymous"
  - subject: "Re: Interviews"
    date: 2005-05-21
    body: "I can tell you there will be more of them in the near future. All I can say is: be patient. ;)"
    author: "Bram Schoenmakers"
  - subject: "Skype.."
    date: 2005-05-21
    body: "Let me be the first to say that Skype support would be great!!"
    author: "Michael Jahn"
  - subject: "Re: Skype.."
    date: 2005-05-21
    body: "I should point out that a new Kopete contributor, Michal Vaner, has made an excellent start at Skype support since this interview was carried out.  See the Kopete mailing list for more details."
    author: "Will Stephenson"
  - subject: "Big Hello to you Will"
    date: 2005-05-21
    body: "Nice to hear from somebody from Newcastle here in the North of England contributing to KDE. Hope things work out great for you in Nuremburg.\n\nHere's to great things in the future for Kopete, and I'm intrigued by the Skype support!"
    author: "David"
  - subject: "VoIP support would be great"
    date: 2005-05-21
    body: "Support for VoIP protocol (SIP) would be great"
    author: "odborg"
  - subject: "Re: VoIP support would be great"
    date: 2005-05-24
    body: "There is also Twinkle: http://www.twinklephone.com/\nSomeone pointed me to this application. Haven't tried yet, but it shouldn't be too hard to port this from Qt to KDE. This way, maybe even Kontact integration would be possible."
    author: "Bram Schoenmakers"
  - subject: "Re: VoIP support would be great"
    date: 2005-05-25
    body: "This looks really great! Did anyone ask him to join the upcoming PIM meeting as he's apparently Dutch as well?"
    author: "ac"
  - subject: "Releases"
    date: 2005-05-21
    body: "One thing that has been on my mind for a while now is how the fast pace of development that is required of something tracking so many different protocols sits with the fairly well-spaced releases of something like KDE.\n\nAnd with the recent breakage of MSN (which is far more MSN's fault, though why they should let me use their network, I'm not sure, especially as I try hard to remove Windows when I find it :) ), it looks like a bit of a liability.\n\nWhen I got into FLOSS a while back, I often heard \"release early and often\". I understand that for various reasons, KDE only wants to release stable software, and that means releases need to be well-planned. It's amazing they release as often as they do, considering the size of the codebase.\n\nI also understand why Kopete wants to be in the official KDE releases. I would, if I was an application. But we're in a situation where normal users are going to have to patch kopete from the SVN copy, and if I'm not wrong, compile it within 3.4's kdenetwork package. The minimum complexity is likely to involve a patch against that tree, yes? So what was gained by shipping a solid, stable kopete is all a loss when these users try to connect to MSN.\n\nOr switch to Gaim, which I used to use until I found the better status support of kopete indispensable. \n\nOr, could kopete be made to compile outside of a larger source tree, and thus regain independence of release to a certain extent, without sacrificing any of the political and organisational (and QA?) benefits of being in the big KDE releases?\n\nI'd love to see releases made between KDE releases, which would mean periods of breakage were minimized (as I'm sure the fix is done already), and users would benefit from all the other great features going into it every week."
    author: "mishgosh"
  - subject: "Re: Releases"
    date: 2005-05-21
    body: "kopete from svn trunk is pretty stable and you can compile it on KDE trunk,3.4 and even 3.3 so all you need is  svn up everytime something goes wrong with <enter your favorite protocol here> :)\n\nhere are the instructions http://kopete.kde.org/index.php?page=cvs\n\nit's really easy and allows you to test new cool features such as get new stuff and all."
    author: "Pat"
  - subject: "Re: Releases"
    date: 2005-05-22
    body: "Yep, that worked- using the 3.4 branch, not the trunk (which wouldn't compile for me, failing on some v4l-related stuff).\n\nFor anyone else- honestly, it was very easy, and no, I didn't have to download or compile all of kdenetwork. Even on my twin P3 500, it didn't take long.\n\nCheers"
    author: "mishgosh"
  - subject: "Re: Releases"
    date: 2005-05-27
    body: "you need to update your kernel headers, then it'l work"
    author: "Matthew Robinson"
---
Will Stephenson is one of the attendees at the <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">KDE PIM Event</a>. 4 years ago he started with contributing to <a href="http://kopete.kde.org/">Kopete</a>, the instant messaging client for KDE. His recent contributions made it possible to let Kopete communicate with <a href="http://www.kontact.org/">Kontact</a>. In this interview, he will tell you about Kopete and his exciting plans for the upcoming meeting.








<!--break-->
<p style="float: right; border: 1px solid grey; width: 110px; padding: 1ex;"><img src="http://static.kdenews.org/jr/will-stephenson.jpg" width="80" height="101" alt="Will Stephenson" /><br />Will Stephenson of Kopete</p>

<p><strong>Please tell us something about yourself.</strong></p>

<p>My name is Will Stephenson, I'm 30, and from Newcastle upon Tyne, UK. However, I've just moved to Nuremberg, Germany. I've been contributing for KDE for 3-4 years, most of my early stuff was things like icons and animations though. I started out contributing to Kopete, icons, animations, artwork etcetera, then I coded a couple of plugins. Then I went to <a href="http://events.kde.org/info/kastle/">Kastle</a> and got really hooked and coded KIMProxy, which brought me into contact with other KDE applications, mostly KDEPIM. Do you know about KIMProxy?</p>

<p><strong>No.</strong></p>

<p>It's some KDE infrastructure that lets applications use instant messaging
services (presence, starting chats, sending files) from different IM clients.
So Kontact uses it in lots of ways, so does Konqueror, and on the client side,
Kopete and Konversation support it, and now Licq does too. The nice thing is it
isn't bound to any particular application or IM client, so it is easy to use or
to add support for it to other IM apps. Anyway, so I did that, and added support for it to the Kontact apps and Kopete and also work on integrating Kopete with the KDE addressbook. Then last summer I got a contract with SUSE to write Novell GroupWise Messenger support for Kopete, which was pretty scary at first as I had only assisted on existing protocols, I had never written an entire protocol from scratch before (whole new IM protocols don't come along that often, and those that do aren't very important). Anyway we got it done and I got a job out of it. So I work for SUSE now, developing KDEPIM things for them. Which mostly comes down to supporting Novell Groupwise in KDEPIM and Kopete.</p>

<p><strong>So what do you think is Kopete's greatest strength?</strong></p>

<p>The way it works like every other KDE app, and the way it cooperates with other applications, so you can store your data in KAddressbook, or send a mail
using KMail, from Kopete and vice versa. Those apps can use Kopete if they need
to reply to an email using IM. Another real strength Kopete has is the way it
puts people first, not IM systems. So you don't have a list of ICQ contacts, MSN contacts, or see the same person listed 3 times in different IM systems,
instead you see the person, with their picture (from KAddressbook or from
IM) and just click on them to communicate with them. Other things that are nice
about Kopete are the degree of customization available using chatwindow and
emoticon themes, downloadable from kde-look.org using KNewStuff.</p>

<p><strong>What feature would you really like to see included?</strong></p>

<p>I would really like to see voice and video support in Kopete. Several developers (not me) are working on that right now so I'm sure we will make some progress on that.</p>

<p><strong>Will that include Skype, to name a buzzword?</strong></p>

<p>Skype support is one neat thing. I'm going to be working that at the KDEPIM
meeting, however because Skype is a proprietary application, that's just using
Kopete as a frontend to the Skype app. Which is great, opens the way to using
Skype from Kontact using KIMProxy etcetera. But what I was talking about before
is supporting voice/video in the well known IM protocols like MSN, Yahoo and
AIM/ICQ.</p>

<p><strong>What is the largest problem with Kopete development?</strong></p>

<p>Manpower, having enough developers. It's getting better at the moment as we have some new talent coming on board, but we can always use more. We're a really open project and we always welcome contributions. For example look at Eric Cartman - he started coding on Kopete on a Monday and by Wednesday he had added cool code that shows emoticons in people's nicknames in the contact list. It's my experience that any technical difficulty we have gets overcome if we have time to work it.</p>

<p><strong>How about the multitude of protocols? Does that cause troubles?</strong></p>

<p>Yes, it means that although Kopete is one program, it has code equivalent to the backends of several other programs behind it. And it means there are more,
different codebases for developers to follow. Therefore it works best if there
are one or more protocol specialists who each maintain a particular protocol.
At the moment we could really do with more AIM/ICQ and Yahoo developers.</p>

<p><strong>What are you occupied with at the moment?</strong></p>

<p>At the PIM meeting, I'm going to code on Skype support. I also want to sort out the IMapplet from kdenonbeta that I wrote ages ago and never finished, which lets you put contacts in your panel, for important contacts like girl/boyfriends. Another thing I am working on is the KDE4 version of the KIMProxy/KIMIface API. One weakness of KIMIface is that it only allows for one
means of contact with people - fine if you only do text, but for Skype you have
a choice of text or IM, so we need to address that.</p>

<p><strong>Thanks a lot!</strong></p>





