---
title: "Announcing the New KDE-Artists.org Website"
date:    2005-06-05
authors:
  - "jtheobroma"
slug:    announcing-new-kde-artistsorg-website
comments:
  - subject: "Great work Theobroma!"
    date: 2005-06-05
    body: "The new site looks awesome, will artist.kde.org be a redirect to kde-artists.org?"
    author: "Samuel Weber"
  - subject: "Re: Great work Theobroma!"
    date: 2005-06-05
    body: "I second this! please do. There are so much dead leaves in the kde domains and they puzzle people. One reference site for artists.\n\nThat's because the new site is soo goood and really replaces the other. The gfx is cool, the site is so.. artistic!\nIt's great there are the discussion forums, already filled with very interesting topics! <- go read 'em.\n\nanother sidenote: 'guidelines & tutorials' triggers the 'art development' menu, are there other ways to get to it? is G&T a 1st level menu entry?\n\nfinally: *thank you artists*, you're enlighting our community :-)\n"
    author: "jumpy"
  - subject: "Re: Great work Theobroma!"
    date: 2005-06-06
    body: "I third this ! ;)\n(Since I still have no write access to SVN, I can't update the info on current artist.kde.org :( )"
    author: "luci"
  - subject: "cystral and Konqueror icons!"
    date: 2005-06-05
    body: "It is very confusing for New users (my friends) to know where is the \"Home\", \"Reload\" buttons!\n\nBecause it uses Blue and White for all these\n\n1. Go back\n2. Go Forward\n3. Go UP\n4. Go HOME\n5. Reload\n\nOther than seasoned KDE users it is not possible for new users to understand where \"HOME\", and \"Reload\" buttons are :(\n\nwhy not make the Stop button in a BLUE color so that even experienced KDE users can't find it.\n\nJust look at Linspire's clear-e theme, which has nice HOME and Reload icons for konqueror. \n\nEveraldo please release your clear-e konqueror toolbar icons (Back, forward, up, home, reload and stop)! for KDE!!! We need your help!"
    author: "fast_rizwaan"
  - subject: "Re: cystral and Konqueror icons!"
    date: 2005-06-05
    body: "I second this!\nIMHO the \"Reload\" button should be in green color (just like other browser's default color) and the \"Home\" button should be in red color (just like in root folder's color).\nDon't get me wrong, I like the blue but new users will definitly be confused by the all blue looking buttons."
    author: "Hugo Rodrigues"
  - subject: "Linspire Konqueror toolbar icon"
    date: 2005-06-05
    body: "Instead of asking you to look at linspire icons, here is the attachment."
    author: "fast_rizwaan"
  - subject: "lame!"
    date: 2005-06-06
    body: "Wow, your new icons are incredibly lame!  Check out the ones I've attached.  Sooooo much better."
    author: "ac"
  - subject: "Re: lame!"
    date: 2005-06-06
    body: "lol, what are those, kde classic?\n\n"
    author: "Samuel Weber"
  - subject: "Re: lame!"
    date: 2005-06-06
    body: "an older version of crystal. =)"
    author: "ac"
  - subject: "Re: lame!"
    date: 2005-06-06
    body: "Yes it is better.\n\nThere is one issue with the blue arrows.  These same icons are also used for other purposes and then the round recess looks out of place.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "The site is BEAUTIFUL :)"
    date: 2005-06-05
    body: "I love the color and style used there :) Thank you!"
    author: "fast_rizwaan"
  - subject: "wow"
    date: 2005-06-05
    body: "a beautiful site, fits the artists well i think :D"
    author: "superstoned"
  - subject: "Mouse icons/themes"
    date: 2005-06-06
    body: "I feel KDE needs some serious new mouse icons. The X11 default icon theme has not changed in a long time. This mouse theme is not as clear/crisp as KDE is in general."
    author: "charles"
  - subject: "Re: Mouse icons/themes"
    date: 2005-06-06
    body: "There are some nice ones at kde-look.org: http://www.kde-look.org/content/show.php?content=24929\nI'm using Grounation from there, for example: http://www.kde-look.org/content/show.php?content=14484\n\nSo you're suggesting KDE should bundle one (of them?) and install it on first startup?\n\n"
    author: "cm"
  - subject: " "
    date: 2005-06-06
    body: "Maybe a little more empty space between the sections and the menus would be fine. As it is now, it looks too crowded."
    author: "Anonymous"
  - subject: "Main site design, anyone?"
    date: 2005-06-06
    body: "Too bad www.kde.org doesn't share this look... it looks like stoneage site :)"
    author: "Marko Rosic"
  - subject: "Re: Main site design, anyone?"
    date: 2005-06-06
    body: "You're not using the latest Konqueror with shadow support, are you? It only looks stone aged in outdated browsers. ;)"
    author: "ac"
  - subject: "Re: Main site design, anyone?"
    date: 2005-06-07
    body: "the shadow effect was removed from the web version of the CSS afaik (iirc)"
    author: "luci"
  - subject: "Re: Main site design, anyone?"
    date: 2005-06-07
    body: "Why that?"
    author: "ac"
  - subject: "Re: Main site design, anyone?"
    date: 2005-06-09
    body: "Well... as a web designer I just have to say that it is a bad design to make something visible in just one browser. CSS2 and 3 look great on paper but... Anyway project like KDE need a great looking site for marketing reasons. Would you ever buy badly packaged product... or to rephrase, would you rather buy a well designed and packaged product? :)"
    author: "Marko Rosic"
---
<a href="http://www.kde-artists.org">KDE-Artists.org</a> is a new KDE sister website created specifically for artists and coders to use for reference and direction in creating a high quality consistant user interface. It is also the home of <a href="http://kde-artists.org/main/content/blogsection/4/38/">Kollaboration</a>, a new concept created by several people to give dreamers, artists, and coders a place to work together. 




<!--break-->
<p><img src="http://static.kdenews.org/jr/kde-artists.png" width="138" height="128" align="left" />Here are just a few things you will be able to do at KDE-Artists.org: use Kollaboration to see your ideas or concepts come to life, interact with and learn from seasoned developers, learn new "tips and tricks", easily find <a href="http://kde-artists.org/main/content/blogsection/5/37/">guidelines</a> for creating high quality KDE artwork, acquire re-usable tools like color palettes, and icon elements, get involved with the "official development", vote for preferences, help establish standards, get involved to help coordinate the look and feel of each release, and a space where coders can <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/board,2.0">request specific artwork for their applications</a>.</p>



