---
title: "Jes Hall Talks About KDE's Documentation"
date:    2005-05-13
authors:
  - "jriddell"
slug:    jes-hall-talks-about-kdes-documentation
comments:
  - subject: "Thanks"
    date: 2005-05-13
    body: "Great interview, but the interviewer should\nhave asked her if she was free. Especially\nbecouse she looks so good!\n\n:-)"
    author: "Pieter"
  - subject: "free as speech ?"
    date: 2005-05-13
    body: "or free as beer ? ;)"
    author: "Alex"
  - subject: "Re: Thanks"
    date: 2005-05-13
    body: "Is her sex or looks in anyway relevant?\n\nYou guys need ot get more out."
    author: "josel"
  - subject: "Re: Thanks"
    date: 2005-05-13
    body: "Read what you have triggered: http://theobromas.blogspot.com/2005/05/new-resources-urgently-needed.html"
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2005-05-14
    body: "Hello, I think you mistyped the url.. what you are looking for is http://sex.kde.org , where you can have unlimited amounts of free seKx!\n\n(but only according to the terms of the GPL, of course)\n\n---\n\n\nNow, just to give this Kute looKing girl some positive feedback, I'm now going to say I have actually been reading the whole interview without drooling or fapping.\n\nI'm very happy to see there are people out there who actually enjoy writing documentation.\n\nKeep up the good work and thanks a lot for this informative interview!\n"
    author: "konqi"
  - subject: "hey"
    date: 2005-05-14
    body: "First of all, I want to say thanks to Jes for her important contributions and no less a thanks to Jonathan who has been extremely active since he joined the Dot editors.  So thanks a bunch for your valuable contributions, both of you!\n<p>\nAs for this particular thread, Scott <a href=\"http://www.kdedevelopers.org/node/view/1051\">posted the new HIG here</a>.  Worth a read.\n<p>\nI have to say I considered closing this thread when you first started it but was afraid I would have been overreacting unnecessarily.  I'm glad I didn't, but you sure made me reach. Thankfully Janet <a href=\"http://theobromas.blogspot.com/2005/05/new-resources-urgently-needed.html\">responded with humour</a> (posted on Google no less!).\n"
    author: "Navindra Umanee"
  - subject: "Freaking out the women-folk"
    date: 2005-05-16
    body: "This kind of behavior is part of the reason why there aren't as many women in computers.  It's really not appropriate in any case."
    author: "me"
  - subject: "Wiki"
    date: 2005-05-13
    body: "Is there any plans on start using a wiki for parts of the documentation? This would allow more people to help, with less effort. I mean, I'm beggining to contribute with kde-i18n-ptBR project, but getting access to the SVN, installing and learning KBabel is not as easy as accessing a wiki, like wikibooks.org.\n\nIt could even be made more syncronized with the development process. I'm a developer and I'm using XP and a Wiki in my development process, so I and the users write the user stories in the wiki, I implement the things, they, with my help, improve the user stories, I finish it detailing and adding snapshots of the application, and, when the application is released, 90% ot the help is concluded.\n\nSorry for the bad english."
    author: "Ivan"
  - subject: "Re: Wiki"
    date: 2005-05-13
    body: "As written you can send new documentation in either ASCII or Docbook format, no need to be familiar with version control systems. And it's a riddle to me how you want to use KBabel for writing documentation."
    author: "Anonymous"
  - subject: "Re: Wiki"
    date: 2005-05-13
    body: "I'm using KBabel for traduction of the applications(i18n-ptBR).\n\nI think the use of a wiki could be more dynamic in help constucting, because of the just-in-time atualization."
    author: "Ivan"
  - subject: "Re: Wiki"
    date: 2005-05-14
    body: "The K-3D team (a GTK project) is using a wiki for most of their docs, but they've also written a script that produces other document formats from that wiki.  For something like KDE, more work would be required, but the idea itself might be worth looking into."
    author: "Lee"
  - subject: "Re: Wiki"
    date: 2005-05-13
    body: "For translations KBabel are a much better suited tool than wiki, partly because of the type of data a translation consist of. Translations(of UI)also have to be verified on running code, it's not simple text to text translation. \n\nAs for writing and translating documentation the teams accept contributions in pure ASCII form, and that's several levels easier than any wiki I have ever seen. Just write the text and let the documentation and translation teams do the rest, can't get any simpler.\n\nOn the other side, I think both the documentation and translations of KDE could make use of some new infrastructure. But aimed at the end users, rather than developers. A way to easily update both documentation and translations, using some implementation of GetHotNewStuff. Giving users the ability to update docs and translations independent of KDE releases."
    author: "Morty"
  - subject: "Great user support as well"
    date: 2005-05-13
    body: "Just had to say that Jes and Philip are very welcomed contributors on the KDE user mailinglists.\n\nTheir knowlegde about applications, settings, location of switches, etc. are quite often way beyond what other regulars on the lists know, including developers like myself.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Great user support as well"
    date: 2005-05-14
    body: "yeah, that's so nice to see new people doing such useful work. The doc team rocks! Lauri, Phil and Jess know a lot and they share this knowledge with all KDE users through IRC and docs.\nI hope this article will trigger more doc updates from developers! And QWhatsTHis help!\nThanks canllaigh for this lively interview!"
    author: "annma"
  - subject: "XMLMind"
    date: 2005-05-14
    body: "One tool I'd point people at is XMLMind. It's a pretty decent XML editor that lets you write WYSIWIG docbook. I used it to write the tutorial I submitted for the new user manaual. \n\nhttp://www.xmlmind.com/xmleditor/\nhttp://xmelegance.org/customising-window-behaviour/html/customising-window-behaviour.html\n"
    author: "Richard Moore"
  - subject: "KHelpCenter is UGLY!"
    date: 2005-05-14
    body: "run khelpcenter and you can get confused easily, There are simply no ICONS to guide you! seems like khelpcenter imitating windows help center ;)\n\n\nBTW, she is cute! :)\n"
    author: "fr_dude"
  - subject: "Re: KHelpCenter is UGLY!"
    date: 2005-05-14
    body: "You can of course use the Help -> appname Handbook menu entries, as well as typing help:/appname in konqueror to get to help documents if you find the khelpcenter interface isn't as pleasing to you as it could be. There are many ways of getting to docs to suit a wide range of approaches =)"
    author: "canllaith"
  - subject: "Re: KHelpCenter is UGLY!"
    date: 2005-05-14
    body: "What's the significance of your handle?"
    author: "AC"
  - subject: "Re: KHelpCenter is UGLY!"
    date: 2005-05-14
    body: "ehe :) i really would like to know too.."
    author: "wsjunior"
  - subject: "handle"
    date: 2005-05-15
    body: "It's a Welsh word that means (to the best of my limited knowledge) family kindness. The significance is that my ex husband's family is Welsh, and I was at my wits end to find a name that wasn't taken on every flipping site I tried to sign up for ;)."
    author: "canllaith"
  - subject: "read C++ for dummies"
    date: 2006-03-17
    body: "try to become a REAL developer, you are NOT a developer if you write some stupid doc's ...\n\n*sigh*\n\nI suggest reading c++ for dummies ;)\n\nhttp://www.amazon.com/gp/product/076450746X/102-6245694-1234545?v=glance&n=283155"
    author: "TranceDude"
  - subject: "Re: read C++ for dummies"
    date: 2006-03-18
    body: "Crawl back under your rock, troll."
    author: "cm"
  - subject: "windows = for you"
    date: 2006-03-19
    body: "go use windows slut !"
    author: "trancedude"
---
Jes Hall is a new contributor to <a href="http://docs.kde.org/">KDE's documentation team</a>.  In the interview below she talks about how she joined the team, how KDE's documentation is made, how you can help them and how they can help KDE's coders.  She also reveals the 5 finest examples of documentation in KDE.










<!--break-->
<p style="float: right"><img src="http://static.kdenews.org/jr/jes_hall.jpg" alt="jes_hall.jpg" /><br />Canllaith is a regular helper on #kde</p>

<p><b>Please tell us about yourself and your role in KDE.</b></p>
 
<p>I'm a Unix systems and network administrator and technical writer. I'm a new contributer to KDE and my role is primarily in documentation, although I've made some contributions to Kicker &amp; Kopete.</p>
 
<b>How did you become involved with the documentation team?</b>
 
<p>I spent a bit of time hanging out in the #kde IRC channel on Freenode helping users with problems they encountered in their KDE desktop. It seemed after a while a lot of the same questions came up over and over, and <a href="http://users.ox.ac.uk/~chri1708/Phil.html">Phil Rodrigues</a> suggested I should write up some of my advice for <a href="http://docs.kde.org/en/HEAD/kdebase/faq/">the FAQ</a>. Now I'm the FAQ maintainer, as well as having contributed to other KDE docs.</p>
 
<b>How many other people are there on the documentation team?</b>
 
<p>Not enough! There are some developers who do their own documentation, and some people who pop in every now and then with patches - but I think that as far as regular people spreading themselves over KDE documentation in general there are only half a dozen of us.</p>
 
<b>What functions do the team do to get KDE's documentation into its excellent state?</b>
 
<p>The documentation team deals with a lot of different technologies working with docs. We use <a href="http://bugs.kde.org/buglist.cgi?product=docs&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">Bugzilla</a> to keep a list of docs that are incomplete or missing. Documentation is written either as <a href="http://i18n.kde.org/doc/markup/">Docbook</a> or as plain text and then marked up later. We tend to proofread each others work, and then it's commited into SVN. The KDE build system has a framework in place to process and install docs alongiside applications. We use our mailing list and IRC channel to coordinate efforts.</p>
 
<b>Documentation is often seen as an example of an area most hackers would find boring, what keeps the documentation team working on it?</b>
 
<p>We've all felt the frustration of attempting to use a new program and finding it's not easy to use and its documentation is poorly written, or even worse, absent. You can't possibly design an application interface that would be intuitive for all people from all backgrounds, and documentation is what bridges this gap.</p>
 
<p>I've also always found writing enjoyable, and technical writing gives me an opportunity to write about what I'm interested in, even though it's not 'mainstream'.</p>
 
<b>What documentation is available within KDE?</b>
 
<p>KDE has a wide range of user documentation from application handbooks designed to describe an application's function and configuration options in detail to shorter task based documentation. The <a href="http://people.fruitsalad.org/phil/kde/userguide-tng/">new userguide</a> is a general overview of KDE - I'd like to see it published as the KDE definitive guide once complete. There are also more task focused documentation in the FAQ and the various KDE Wiki.</p>
 
<p>There is also a small selection of documentation focused on Docbook and other aspects of writing for KDE up on <a href="http://i18n.kde.org/doc">the KDE i18n site</a> as well as developer and API documentation on <a href="http://developer.kde.org">developer.kde.org</a>.</p>
 
<b>What are the tools used to create KDE's documentation?</b>
 
<p>KDE's documentation is written in a system for writing structured docs in XML called Docbook. Docbook allows you to write once, and then publish in a variety of different formats with no or little modification to your original source files. KDE's documentation is stored in SVN with the rest of its source code, and this is an integral part of working with documentation. Checking files out of SVN, creating patches, applying patches, commiting changes are all part of our regular workflow. The conversion from Docbook source files to the HTML pages that KHelpCenter display is done by meinproc, which is a version of xsltproc customised for KDE's purposes.</p>
 
<p>The most important tool is a great editor with good docbook support. <a href="http://kate.kde.org/">Kate</a> is my favorite editor for writing Docbook, with the XML plugins availible in kdeaddons.</p>
 
<b>The applications handbooks contain a wealth of information but are quite monolithic. Is anything being done to make it more accessible?</b>
 
<p>I believe there is a place for large, detailed monolithic documents for those who do want or need to learn every option about an application in detail. I'd like to see more task focused help that can be retrieved in context though - rather like <a href="http://urbanlizard.com/~aseigo/whatsthis_tutorial/">WhatsThis?</a>, ways of getting to the single help page relevant to an option while you're sitting there at that option, instead of having to open the help center and search for the page.</p>
 
<b>What changes can we expect to the documentation setup in KDE 4?</b>
 
<p>I haven't even thought of 3.4 yet! I have one project I'm researching for 3.4 at the moment, which is a multimedia quickstart guide that would double as marketing material. As far as documentation as a whole goes, I have no clue yet. ;)</p>
 
<b>How does the KDE documentation compare to the competition at Gnome? Are there any moves to standardise documentation formats between KDE and Gnome so only one help reader is needed?</b>
 
<p>As far as I'm aware, KHelpCenter is already capable of displaying Gnome documentation. I don't actually have any Gnome applications installed to test this, using only KDE when on Unix. I know some collaboration has been discussed between the maintainers of KHelpCenter and Yelp.</p>
 
<b>How does a developer get started with creating documentation or getting some written?</b>
 
<p>Write something! We're happy to accept plain text documents and add the relevant markup for you. This goes for any person wanting to create documentation as well as developers. Developers who want to get some documentation created for their application can email us at <a href="http://mail.kde.org/mailman/listinfo/kde-doc-english">kde-doc-english</a> and see if anyone on that list has some free time to pick up the project for them.</p>
 
<b>What can a developer do to improve the documentation in KDE?</b>
 
<p>Write documentation, even if it's incomplete or even just a rough outline. It's far easier for a technical writer to flesh out a paragraph or two or add some markup to plain text than it is to learn every detail of an application they may not have used before in order to create content. Being available to answer questions about the application is also very helpful. Often what's obvious about an option to someone who can recite the source code backwards may not be so obvious to others and needs a bit of explaining. =)</p>
 
<b>How can the documentation be improved?</b>
 
<p>Right now, we're severely understaffed for the job of keeping the documentation up to date. I'd like to see a first stage goal for myself before 3.4 is to help with bringing all the application handbooks up to date and accurate with current KDE versions.</p>
 
<p>I'm also toying with some other approaches to documentation on the side - improving WhatsThis? help, looking into the technologies required to create interactive presentations detailing specific tasks. </p>
 
<b>What are the 5 best examples of documentation in KDE?</b>
 
<p><a href="http://docs.kde.org/en/HEAD/kdebase/faq/">The FAQ</a> is a collection of questions that are regularly asked on mailing lists and in IRC channels, and if you have a niggle about something it should be your first port of call. It's actively maintained and new question and answer sets are being added regularly. </p>
 
<p><a href="http://people.fruitsalad.org/phil/kde/userguide-tng/">The userguide</a> is a book that provides an overview of the entire KDE experience. It's designed to lead you through fundamental desktop concepts like using Kicker and Kwin all the way to KDE for administrators with Kiosk and desktop scripting.</p>
 
<p><a href="http://urbanlizard.com/~aseigo/whatsthis_tutorial/">WhatsThis?</a> context sensitive application help is also a great example of documentation. It's right there to be read exactly when you want it, rather than requiring you to read it beforehand or switch to another help application to search for it.</p>
 
<p>The application handbooks, while monolithic are a great resource to anyone who's ever had to support KDE in the workplace. Not all applications are documented yet, but reading through those that are up to date has helped me answer a wide range of user questions about an application.</p>
 
<p>The <a href="http://i18n.kde.org/doc/markup/index.html">KDE Docbook markup guide</a> is a fantastic reference for anyone interested in KDE documentation, and a great example of well written task based documentation. I have a printed copy of this on my bedside table. :)</p>




