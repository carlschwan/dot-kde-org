---
title: "KDE CVS-Digest for January 21, 2005"
date:    2005-01-23
authors:
  - "dkite"
slug:    kde-cvs-digest-january-21-2005
comments:
  - subject: "offtopic question about kmail"
    date: 2005-01-23
    body: "I've just started using KDE and kmail. My question is: how can I force kmail into a plain-text only mode? Does it have such a thing? I don't ever want to see, and especially not send, HTML email (not even by accident). At the moment I have to select 'use fixed font' and also hide the formatting toolbar. But you can still easily turn formatting back on, and when you do, it still misleadingly shows 'use fixed font' selected. Argh. Please tell me there is a plain-text mode :-)"
    author: "foo"
  - subject: "Re: offtopic question about kmail"
    date: 2005-01-23
    body: "> Please tell me there is a plain-text mode :-)\n\nThere is a plain-text mode :-) Happy now?\n\nIn kmail 1.7.2 that I am using there is the menu item \"Folder\"->\"Prefer HTML to unformatted text\". I have that turned off.\n\nWhen sending there is the menu item \"Options\"->\"Formatting (HTML)\" which is also turned off."
    author: "Erik"
  - subject: "Re: offtopic question about kmail"
    date: 2005-01-23
    body: "i have the exactly opposite question =)\nis it possible to write rich-html emails? for example, i want to insert images in the body of the email, and right now (3.3) i can't do it. All I can do is insert the image as an attachment.\nSome months ago I saw this was on the feature plan, but now I can't find it anymore. Has it been postponed to 4.0?\n"
    author: "Anonymous"
  - subject: "Re: offtopic question about kmail"
    date: 2005-01-24
    body: "\nWith the kmail in kde 3.4  ( currently in its first beta ), you can\ndo rich-text/html messages.\n\n"
    author: "kmail"
  - subject: "Re: offtopic question about kmail"
    date: 2005-01-24
    body: "KMail 1.7 (with KDE 3.3) supports very basic HTML formatting.  You can choose the font, font size, bold, italics, underline, font color, and alignment.  Finally, you can construct lists.  To the best of my knowledge, that is all it supports.  Those features were new with version 1.7.\n\nHopefully, later versions will support full HTML editing including inserting pictures as you have mentioned.\n\n"
    author: "Zippy"
  - subject: "Re: offtopic question about kmail"
    date: 2005-01-27
    body: "Same problem here too!\n\nThere is an annoying header on every html message that I haven't figured out how to hide. --AP"
    author: "Aaron"
  - subject: "Bugs"
    date: 2005-01-23
    body: "As a regular reader of the digest, i am a bit concerned about the number of bugs in KDE. I think it was a good idee to highlight the Top 10 Bug Fixers.\n\nMaybe you could adding an additional statistic:\nThe Top 10 of the \"highes rated\" bug fixes. (highest rated = The bugs with the most votes)\n\nThe would be especially interesting for users and would make the CVS-Digest even better.\n\n"
    author: "Peter"
  - subject: "Re: Bugs"
    date: 2005-01-23
    body: "Really cool idea. I know that not only bugs or features \nwith most votes can be fixed/realized but it would be great if the\nCVS digest could somehow highlight bug fixes / feature implementations\nwith more than a certain number of votes. After all those (IMHO)\nunnecessary layout experiments with the CVS digest this would be\nsth. regular readers of the all-in-one/non-paged digest will profit\nfrom.\n"
    author: "Martin"
  - subject: "Re: Bugs"
    date: 2005-01-23
    body: "Keep in mind, however, that the bugs with the most votes are not necessarily the most critical. In most cases, they're of the \"major annoyance\" variety, while more critical issues exist that require deeper knowledge of the technology and thus have a limited audience among the voter base. A lot of those \"unpopular\" bugs also pertain to major areas of potential growth for KDE, e.g. internationalization."
    author: "Eike Hein"
  - subject: "Re: Bugs"
    date: 2005-01-23
    body: "You are right. Bugs with the most votes may not be the most critical ones.\nBut there is no measurement for that, or for the work required to fix a bug. (Also the number of bugs fixed by a person is not such a value)\n\nIt is more meant as information for the users that a \"popular\" bug has gone.\nIn my opinion a bug with more than 100 votes can't be irrelevant."
    author: "Peter"
  - subject: "Re: Bugs"
    date: 2005-01-23
    body: "Note that 100 votes doesn't have to be more than 5 people. "
    author: "teatime"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "I agree, this is an excellent idea. when Konquerer alone has 1756 bugs, you know that's not good. We need to highlight this problem."
    author: "Tom"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "Yeah, good idea.\n\nKonqueror should really be under 1500 CONFIRMED bugs, that actually used to be a goal. Maybe it will happen for KDE 4 with better tools and toolkits."
    author: "Alex"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "Confirmed? The 1700+ bugs reported for Konqueror includes the UNCONFIRMED bugs. Take that category out of the query and you end up with 732.\n\nSounds like they could use a bit of help sorting out the reports though."
    author: "teatime"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "I would not stress too much the difference between UNCONFIRMED and NEW in KDE Bugs. Sure the original Bugzilla makes a rather big difference between the two but they have teams to sort out bugs, however KDE has not any such team.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "There is a difference. For instance UNCONFIRMED bugs have not been fully evaluated by a developer. They could be DUPLICATE or INVALID, and for Konqueror they are often not yet sorted between KHTML and file-browing."
    author: "Carewolf"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "\"As a regular reader of the digest, i am a bit concerned about the number of bugs in KDE.\"\n\nare you one of those who stare at the numbers in bugs.kde.org and think that KDE is buggy? what would you say if I told you that Mozilla (a single app) has more bugs in their bugzilla than entire KDE does? Does that mean that Mozilla is buggy piece of crap?\n\nNumber of open bugs in bugzilla doesn't really tell that much about the quality of the software. It just tells how often bugs are reported. That might be due to large number of users, large number of bugs, or something else. And besides, KDE is a HUGE project, so there will be bugs. Even if they managed to reduce the number of bugs in bugs.kde.org, it wouldn't necessarily mean that KDE is less buggy. It would just mean that less bugs are reported. And that could mean that the bugs are still there, they are just not reported.\n\nBesides, how many of those bugs are due to user-error? How many are from older versions of KDE? How many are due to corrupted packages, unstable compilation-options, unstable hardware etc. etc.? How many of those bugs are quite frankly wrong? Sometimes bug isn't necessarily a bug but a \"I think this thing here could be done differently IMO\""
    author: "Janne"
  - subject: "Re: Bugs"
    date: 2005-01-24
    body: "\"are you one of those who stare at the numbers in bugs.kde.org and think that KDE is buggy?\"\n\nNot at all. I am (at home) an \"KDE only\" user since 2.01, and I am really happy with it.\nAnd since KDE is a growing project it is somewhat natural that the number of bugs is also growing.\nBut I am afraid that the number of bugs may make the bug tracking system more and more unusable.\nAs you mentioned, there are lot reasons for entries that are no real bugs. But they may crowd bugs.kde.org and are therefore an issue as well.\nTo draw a bit of attention to bugs.kde.org is allways a good idea."
    author: "Peter"
  - subject: "Re: Bugs"
    date: 2005-01-25
    body: "Maybe the issue is bugzilla rather than anything else.\n\nIt is noteworthy, as mentioned above, that bugzilla was written for a project where they had paid help to do the boring administrative stuff such as maintaining a bugs database.\n\nPeople have requested more statistics and information from bugs.kde.org in the Digest. Other than the easily available statistics (which are interesting but meaningless) the only thing I would include is some examples of good interaction between developer and user leading to a bug being fixed. If someone wants to watch the flow into bugs.kde.org and put together such a report, I would publish it.\n\nOtherwise the bugzilla database represents a falsehood: I can report a bug and automagically it will be fixed. That isn't the way free software works.\n\nDerek"
    author: "Derek Kite"
  - subject: ">ask people if they want to quit app via systray"
    date: 2005-01-23
    body: "This \"do you really want to? are you absolutely sure!?\" approach reminds me to Windows.  Please revert. It is really the problem of these people, if they are not able to decide when to click. I mean - you have to open the popup-menu and click again. That's a double opt-in already. Don't annoy everyone else because a minority seems not to be able to control their fingers."
    author: "Carlo"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-23
    body: "Basically I would agree. I do not like unneccesary dialogs all over\nthe place either. BUT: Reading about this bug reminded me that I have very often accidentally closed systray apps. I never gave that issue much\nthought, though. I just said ARRRRGH and restarted the app.\nThe problem here is that you DO NOT have to click again (try it out yourself)\nas you say. You just have to press the\nmouse button over a systray app with the right mouse button, move the \nmouse accidentally a bit up and to the left and release the mouse\nbutton and there goes your app. This is really annoying.\nI guess this functionality is there to make it easy to select other options\nfrom the systray app menu. This is OK IMO but \"Quit\" is unfortunately\nat the very bottom of this menu so it only takes a few cups of coffee\nto be without systray apps after half an hour ;-(\n\n\n"
    author: "Martin"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-23
    body: "I agree that a \"Do you really want to quit?\" popping up everytime can be a very irritating thing.\nMaybe a \"Don't ask me again\" checkbox would be fine?\n"
    author: "Anonymous"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-23
    body: "There's already such checkbox..."
    author: "tpr"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-24
    body: "\"The problem here is that you DO NOT have to click again (try it out yourself) as you say. You just have to press the mouse button over a systray app with the right mouse button, move the mouse accidentally a bit up and to the left and release the mouse button and there goes your app.\"\n\nEvery single Windows systray app I have works in one of two ways:\n\n1) The context menu does not appear until you release the right mouse button\n2) The context menu appears, but the selection bar doesn't appear until you release the right mouse button\n\nBoth of these methods do not suffer from your problem. Maybe one of these would be a good way to go?"
    author: "mmebane"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-26
    body: ">\"The problem here is that you DO NOT have to click again (try it out yourself) as you say. You just have to press the mouse button over a systray app with the right mouse button, move the mouse accidentally a bit up and to the left and release the mouse button and there goes your app.\"\n\nTrue. I never noticed that KDE has the stupid \"feature\" to perform a popup-menu click with left and right mouse button. Would be good, if this would change as well."
    author: "Carlo"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-26
    body: "OK true. Then again you could put the system tray in a panel on the top of the screen (where it should be IMHO). Quit will be in the other end of the pop-up menu then. I agree with the parent. Since linux is so much about choices it will allways be nice to see new ideas."
    author: "Elias Pouloyiannis"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-24
    body: "Aaron says \"this is in response to repeated bug reports that complain it's too easy to quit an app from the systray.\" [1]\n\nI don't think people will complain about things that are easy to do. I think the real complain is that \"it's too easy to ACCIDENTALLY quit an app from the systray.\" [2]\n\nThe solution by Aaron either make [1] harder or make [2] harder, which is not good. The ideal solution should make [2] harder, without affecting [1], e.g. moving up the context menu for systray apps.\n\nPersonally, I have never accidentally closed the klipper or any window from the taskbar, but I have closed KMLDonkey accidentally for numerous times. I think it is due to position of the context menu.\n\n"
    author: "ac"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-24
    body: "Well, I mean the solution by Aaron either make both [1] and [2] harder, or doesn't change the situation except creating annoyance if the user marks the checkbox."
    author: "ac"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-24
    body: "It's too easy to rightclick on an applet, accidently get the quit under the mouse, and activate it by letting go. Fixing this would make the whole situation better. \n\nIt should require a **click** not just moving the mouse over the menu with the button already pressed, and simply let go. Maybe not being able to use the right mouse button to simply select menu entries would be even better, that's what the left mouse button is there for."
    author: "John Usability Freak"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-24
    body: "> Maybe not being able to use the right mouse button to simply select menu entries would be even better\n\nI don't think it is a good idea to take away the feature. Some people prefer click-hold-release than 2 separate clicks."
    author: "ac"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-24
    body: "No, your solution would be even worse, since in most cases what you want is to use one of the 4-7 other options in those menus. With those the press and go are the best solution. And doing this only for the quit option does not make sense, different activation of different entry's in the menu. "
    author: "Morty"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-24
    body: "Could it be possible to just delay activation of menu options on \"release\" event for half second or so?"
    author: "Ted"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-25
    body: "Yes you're all right, my idea was pretty bad. I agree with this though, deactivating it for 1/3 of sec or something like that wouldn't hurt, I think."
    author: "John Usability Freak"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-25
    body: "Activation of menu options already depends on the mouse being moved, so in cases where you got the menu behind the pointer instead next to it it's not activated."
    author: "ac"
  - subject: "Re: >ask people if they want to quit app via systray"
    date: 2005-01-25
    body: "To prevent this problem, the quit button should be the furthest option away from the mouse and the most common actions near the mouse. This is the case with regular menu-bars, so maybe the taskbar should be moved to the top of screen so it is more consistent with how menu-bars act (eg. dropping down, quit at the bottom)? This makes sense to me, but I think I'm too used to the bar being at the bottom to change now without feeling weird. :)"
    author: "Jonathan"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-25
    body: "Yes, what you say is correct. The solution is simply: kicker should be intelligent enough to detect where it lies and so display the menu in the more convenient manner. "
    author: "Davide Ferrari"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-25
    body: "And the \"more convenient manner\" would be what?"
    author: "ac"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-24
    body: "At least disable it if the panel is on top of the sceen please."
    author: "testerus"
  - subject: "Re: >ask people if they want to quit app via systr"
    date: 2005-01-24
    body: "yes, indeed. I don't have this 'problem' because my systray is at the top of the screen..."
    author: "superstoned"
  - subject: "KDE PERFORMANCE"
    date: 2005-01-24
    body: "This is OT, but here is something developers should keep in mind!\n\n\"Disk seeks are one of the most expensive operations you can possibly perform.\nYou might not know this from looking at how many of them we perform, but trust\nme, they are.  They suck.  Consequently, please refrain from the following\nsuboptimal behavior:\n\n\t(a) Placing lots of small files all over the disk.\n\t(b) Opening, stating, and reading lots of files all over the disk\n\t(c) Doing the above on files that are laid out at different times,\n\t    so as to ensure that they are fragmented and cause even more\n\t    seeking.\n\t(d) Doing the above on files that are in different directories,\n\t    so as to ensure that they are in different cylinder groups and\n\t    and cause even more seeking.\n\t(e) Repeatedly doing the above when it only needs to be done once.\n\nWays in which you can optimize your code to be seek-friendly:\n\n\t(a) Consolidate data into a single file.\n\t(b) Keep data together in the same directory.\n\t(c) Cache data so as to not need to reread constantly.\n\t(d) Share data so as not to have to reread it from disk when each\n\t    application loads.\n\t(d) Consider caching all of the data in a single binary file that\n\t    is properly aligned and can be mmaped.\n\nThe trouble with disk seeks are compounded for reads, which is unfortunately\nwhat we are doing.  Remember, reads are generally synchronous while writes\nare asynchronous.  This only compounds the problem, serializing each read, and\ncontributing to program latency.\"\n\nHopefully the next KDE will be faster. Heard Qt 4 would also help!"
    author: "Matt"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-24
    body: "Imho the biggest performance problem LINUX has (not just KDE or Gnome) is the application startup time. start firefox under windows- first time you have to wait a few secs. then again - flop, its there immediately. \n\ntry it under linux - you'll have to wait... at least twice as long as under windoze, and at the second start, much longer. would be great if this could be fixed, as long as this isn't, I can't tell anyone linux is faster. although it is more responsive when running..."
    author: "superstoned"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-24
    body: "I don't know where you quoted this from, but this is complete bullshit and the person who wrote that doesn't know a thing about systems programming.\n\n> Disk seeks are one of the most expensive operations you can possibly perform.\nTrue... but much bigger problem IMO is wasteful use of memory which leads to swapping. Reading config files happens only once - when you start the app; swapping happens all the time.\n\n> (a) Consolidate data into a single file.\nAnd have to deal with file locking for when several apps / libraries try to change this one large file simultaneously. A single large file can also get fragmented you know. Not to mention the loss of convenience: Windows may be user friendly, but it definitely isn't administrator friendly! Finally, unless this file is indexed/binary, you'll loose a lot of time and/or memory searching through it.\n\n> (b) Keep data together in the same directory.\nThis will gain you absolutely nothing, since directories are probably already cached by the time you start your app. Not to mention that having a very large directory can actually slow down the file access cause it makes your search tree shallow.\n\n> (c) Cache data so as to not need to reread constantly.\nAny operating system newer than DOS 4.01 will do that for you!\n\n> (d) Share data so as not to have to reread it from disk when each\n> application loads.\nWhere, in memory? But there is already a very nice shared repository of configuration files in your memory, it's called The Disk Cache. It also has a nice feature of releasing memory when another application needs it.\n\n> (d) Consider caching all of the data in a single binary file that\n> is properly aligned and can be mmaped.\nBut there is already a binary file with a nice and very optimized index tree, that is properly aligned and can be mmaped. It's called The File System!\n\n\nAfter you've just reinvented what the brightest minds just spent 50 years coming up with, you end up with a lovely fast system that can be configured only through a usability nightmare called \"registry editor\". Sorry, but if Linux ever gets that way I'm switching to something else :(\n"
    author: "Ted"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-25
    body: "It was from the official GNOME Developer website."
    author: "Matt"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-25
    body: "That explains much."
    author: "Asdex"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-25
    body: "> I don't know where you quoted this from, but this is complete bullshit and the person who wrote that doesn't know a thing about systems programming.\n\nhttp://developer.gnome.org/doc/guides/optimisation/harmful.html\n\nWritten by Robert Love (author of the book \"Linux Kernel Development\" and Linux kernel hacker)."
    author: "Christian Loose"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-26
    body: "Makes sense. From a kernel perspective many of these suggestions are common sense. From an application point-of-view they are the kernel/file-system responsibilities.\n\nAlso remember there are thousands of different use-cases where we access files, and optimal behavior depends on use. For instance data that needs locking or are changed a lot are much better in small bundles. Where things that are often read together and rarely edited are good in large bundles. In this way KDE is very good. Configuration files are small and isolated, where as libraries are large, and dependent libraries are often installed in large modules."
    author: "Carewolf"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-26
    body: "indeed. and keep in mind robert love wrote this some time ago, and didn't take ReiserFS(4) in account :D"
    author: "superstoned"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-26
    body: "I know. I just wanted to point out that one shouldn't judge someones background and knowledge without knowing where the quote is coming from.\n"
    author: "Christian Loose"
  - subject: "Re: KDE PERFORMANCE"
    date: 2005-01-27
    body: "Hm.. perhaps it's just the opposite? Text should be judged based on its own merits, not the merits of its author? Maybe Robert wasn't feeling well when he wrote it ;)\nAnyway, my words are a bit strong but I stand behind the basic idea that very little can be gained (performance-wise) by switching to a registry-style config and very much can be lost (convenience-wise)."
    author: "Ted"
---
In <a href="http://cvs-digest.org/index.php?issue=jan212005">this week's issue of KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=jan212005">experimental layout</a>):

Ruby <a href="http://developer.kde.org/language-bindings/">kdebindings</a> now support .kcfg files.
<a href="http://www.kdevelop.org/">KDevelop</a> adds source navigation history.
KChart adds print support.
KWin adds translucency support.
A new HighContrast style added for partially sighted users.

<!--break-->
