---
title: "KHTML/Webcore: Know the Facts!"
date:    2005-05-18
authors:
  - "kmcneill"
slug:    khtmlwebcore-know-facts
comments:
  - subject: "How do I know what the CURRENT facts are?"
    date: 2005-05-17
    body: "I saw the blog posting about the invitation for apple to have a conference call, yet there was no follow up on that.\n\nI saw Hyatt's blog inviting people's feedback, yet have yet to see any actionable items from that.\n\nie, what (if anything) is KDE doing now?  What (if anything) is apple doing now?  Has all this attention from mainstream media and slashdot actually changed anything? \n\nIt would be nice if there was a central location that was tracking/updating to follow this issue.  (Mailing lists perhaps?  I don't read those...)\n\nAny clues?"
    author: "foobie"
  - subject: "Re: How do I know what the CURRENT facts are?"
    date: 2005-05-17
    body: "Yes: Patience and Planet."
    author: "ac"
  - subject: "Re: How do I know what the CURRENT facts are?"
    date: 2005-05-17
    body: "The issues are being discussed behind closed doors to avoid anymore bad press and infights among fans.\n\nSo have patience, faith and keep your fingers crossed."
    author: "Allan Sandfeld"
  - subject: "Re: How do I know what the CURRENT facts are?"
    date: 2005-05-17
    body: "Thanks for the info ;)\n\nHopefully the \"summit\" will turn out well for both parties ;)"
    author: "foobie"
  - subject: "Re: How do I know what the CURRENT facts are?"
    date: 2005-05-18
    body: "> So have patience, faith and keep your fingers crossed.\n\nWill do.\n\nLonghorn/IE7 is coming. Hope Apple realizes that. We need to join forces big time soon.."
    author: "Janne Karhunen"
  - subject: "Re: How do I know what the CURRENT facts are?"
    date: 2005-05-17
    body: "Here's the latest I think:\nhttp://www.kdedevelopers.org/node/view/1046\n\nand yea, planetkde.org is the place to watch."
    author: "Ian Monroe"
  - subject: "Re: How do I know what the CURRENT facts are?"
    date: 2005-05-17
    body: "> Has all this attention from mainstream media and slashdot actually changed anything? \n\nLike hurting?"
    author: "Anonymous"
  - subject: "dotted?"
    date: 2005-05-17
    body: "I can't get to the article right now, but the original seems to be here:\n\nhttp://www.kdedevelopers.org/node/view/1049"
    author: "Brane"
  - subject: "My web server (osviews) is being overloaded"
    date: 2005-05-17
    body: "My web server (osviews) is being overloaded because the site's referer logos are bogging the server down.\n\nI know KDE.news is a popular site, but not THAT popular.\n\nThis is bizarre.\n\nSorry the inconveenience everybody.\n\n\n--\nKelly McNeill\nosOpinion Inc.\nwww.osviews.com"
    author: "Kelly McNeill"
  - subject: "And maybe soon"
    date: 2005-05-17
    body: "19. The kdom team starts khtml2."
    author: "ac"
  - subject: "Re: And maybe soon"
    date: 2005-05-17
    body: "And maybe soon?\n\nNo, it's happening already. Look:\n\n  http://websvn.kde.org/trunk/kdenonbeta/ksvg2/"
    author: "anonymous"
  - subject: "Re: And maybe soon"
    date: 2005-05-18
    body: "I think you mean kdom instead of ksvg2? KSVG is just a application of kdom. Rob, Nikolas and Frans are doing a great job, it's amazing how much they work on these modules."
    author: "Bram Schoenmakers"
  - subject: "The problem occurred in 13"
    date: 2005-05-18
    body: "\"13.  A KHTML developer clarifies in his blog that this is not going to happen any time soon \u0096 because of the de facto fork of WebCore from KHTML, which just doesn\u0092t give an easy path to integrate Apple\u0092s WebCore changes back into KHTML. He also does not hide the fact that he is pretty pissed off by clueless user comments which give all the credits to Apple but don\u0092t acknowledge the KHTML developers\u0092 work who mostly work for free in their spare time on KHTML.\"\n\nThis is where the problem occurred.  This summary conveniently leaves out the fact that, although he admitted that Apple were doing everything by the book, the overall tone of the posting was very dismissive of Apple and it was quite apparent what his opinion of Apple was.\n\nSure, he didn't directly attack Apple in specific words, but everybody reading it thought to themselves \"here's a KDE developer who is unhappy with Apple\".  Couple that with a legitimate complaint, and it's not hard to see where the controversy came from.\n"
    author: "Jim"
  - subject: "Re: The problem occurred in 13"
    date: 2005-05-18
    body: "How is this a problem?\n\nThere may have been an issue of user expectation, but that has been resolved.\n\nThe issues behind this date from the time that Apple forked khtml.\n\nSo they got some bad press. Break my heart.\n\nRequests for more openness from Apple have been made for a long time. Now some discussion is taking place. I suppose we have seen how to go about it. Asking politely didn't work.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The problem occurred in 13"
    date: 2005-05-18
    body: "> How is this a problem?\n\n> So they got some bad press. Break my heart.\n\nIf it's no big deal, then why bother writing this article?\n"
    author: "Jim"
  - subject: "Re: The problem occurred in 13"
    date: 2005-05-18
    body: "> If it's no big deal, then why bother writing this article?\n\nThat seems like a good enough reason. People have been making a big deal of it and so Kurt wanted to set the record straight.\n\nThere's a lot of potential reasons for things to happen as they did. Over time a number of things have happened with Webcore and people have asked me when they would get into Quanta. In turn I've asked KHTML developers and it always gets met with a degree of frustration on the part of the developer that works with us. The projected inclusion of features and fixes continued to be further out and finally missed KDE 3.4 and then it got to where we were being told that it may not be possible to include these features at all. Follow up on the details if you want to understand it. If you look into the issues from a developer standpoint it's hard not to be dissappointed. I imagine it's more impacting if you're the one trying to interface with Apple.\n\nWhatever the reason and whoever you want to point blame at the result is the same. I'm not going to second guess Apple's intentions here but the net result has been a loss of a lot of improvements for KDE users because the current process has made it increasingly difficult to sort out. It was exciting that Apple chose KHTML, but it would be better if we could share changes. If you're using KDE and KHTML it's something that affects you. So it's certainly important to let people know what is happening.\n\nRemember there is a difference between complying to the letter and complying to the spirit of an agreement. A fork is compliant with an open source license for instance, but hardly the same as a collaboration. The license is the legal agreement and so people who think everything is fine if a legal compliance is there are missing the point."
    author: "Eric Laffoon"
  - subject: "Re: The problem occurred in 13"
    date: 2005-05-18
    body: "It is also strange that they do not work on khtml as a plattform independend solution, hire khtml programmers, but take khtml and mess it up with proprietary bindings. Apples guerilla marketing department had a lot to do to reverse the accusation back into one againgst of KDE I guess.\n\nIt is no wonder that Safari is more advanced if they take khtml and hire full time developers."
    author: "Gerd"
---
If you've been following the news surrounding Apple's relationship with KDE developers and Apple's implementation of KHTML into Safari, then it's almost inevitable that you've developed your opinion based on false or misleading news or comments surrounding the issue. Kurt Pfeifle has authored <a href="http://www.osviews.com/modules.php?op=modload&name=News&file=article&sid=4400">an 18 point article</a> (<A href="http://www.kdedevelopers.org/node/view/1049">blog version</a>), which clarifies the issue in hopes that the confusion will stop and that people will generate their opinions based on the facts... rather than a whole lot of conjecture as is currently the case.






<!--break-->
