---
title: "KDE 3.5 Beta 2 \"Koalition\" is Waiting For You"
date:    2005-10-18
authors:
  - "skulow"
slug:    kde-35-beta-2-koalition-waiting-you
comments:
  - subject: "KKill"
    date: 2005-10-18
    body: "Pleeeeaaasssee stop with the dodgy K names! They aren't cool, they're just annoying."
    author: "Tim"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "I like them. Note that the release manager is a German and both \"Kanzler\" (Beta 1) and \"Koalition\" are proper German words (and related to current German political events, as a tongue-in-cheek joke)."
    author: "Eike Heinb"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "oKay. It is Open Source, feel free to rename it, forK it or whatever."
    author: "gerd"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "I think you meant to say Krename it. ;)"
    author: "Whoever"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "kwite korrect..."
    author: "fast_rizwaan"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "Although I agree, I'd be willing to settle for a CONSISTENT use of the letter K.  Right now, we have words chosen simply because they contain the letter K (Knights), words with K inserted in front of them (KWord, KSpread), and words misspelled with a K instead of a C, Q, or other consonant (Kaffeine, Konsole, Kuickshow).  And we even have a few with no K's at all (my favorites: Quanta Plus, Scribus).\n\nBut more importantly, it's just a beta!  Who cares what the code name is?!?  I'd be willing to have all kinds of nonsense going on with code names if KDE could come up with a more consistent naming style for its applications.\n\nConstructive criticism of KDE is best done at bugs.kde.org, BTW.  At least you can be \"officially\" ignored."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "Names like KWord and KSpread are bad.\n\nConsonant + Consonant\n\n"
    author: "gerd"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Not intending to defend the nomenclature, but KWord-style names follow a common practice in the Windows and Mac worlds (WinDVD, WinBatch, iTunes, iPhoto).  Assuming you pronounce it \"KayWord\" and not \"Quord\", you should be fine.\n\nHowever, the wisdom of smacking extra letters in front of perfectly good words, common practice or not, is still quite debatable."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-21
    body: "i dont see WinWordPad, WinPaint or WinCalculator here"
    author: "roger"
  - subject: "Re: KKill"
    date: 2005-10-21
    body: "No, but you do see WinDVD, WinImage, WinFax, etc..."
    author: "Paul Eggleton"
  - subject: "Re: KKill"
    date: 2005-10-21
    body: "It seems 95% of all KDE applications seem to have a K, Windows applications dont follow the same convention. KDE programmers are lazy, they cant think of a proper application names, and resort to K-style because everyone else does it, and its always done... think outside the Kbox."
    author: "lynxy"
  - subject: "Re: KKill"
    date: 2005-10-21
    body: "Really, this argument has been done to death. If all you judge KDE by is the naming of its applications, then quite honestly you can have your Windows."
    author: "Paul Eggleton"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "I agree."
    author: "rainier"
  - subject: "Re: KKill"
    date: 2005-11-01
    body: "Thinking up a name starting with a k is no easier than thinking up a name that doesn't start with a k, if anything it's harder."
    author: "mikeyd"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: ">>my favorites: Quanta Plus, Scribus\n\nScribus is not a KDE app."
    author: "Patcito"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "Konsole is German for console"
    author: "Michael Thaler"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "What's Kuickshow German for?"
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "That is not a german word. I guess it means: \"quick show\", being able to look quickly at your pictures."
    author: "Carsten Niehaus"
  - subject: "Re: KKill"
    date: 2005-11-01
    body: "Exactly, but this is inconsistent with KSpread etc."
    author: "mikeyd"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "I like the Kwords :) Apps starting with the letter K are distinguishing them selves from other apps on my system. I think it is nice and intuitive way to name things, keep up the good work Kteam :)"
    author: "Petteri"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "Considering all the work that goes into KDE it's such a shame\nthere are always some trolls who only talk about the KNames.\nI think it's great there is some sort of name brinding by using\nthe K. There are some clever ways to do it, like amaroK or\nKarbon14 and Krita. And some not so clever ones (you know them\nso I wont mention them here). Nevertheless no-one ever complained\nabout Microsoft Word, Microsoft PowerPoint, Microsoft Paint or\nabout iTunes, iPhoto, etc. \nThe K-Names of the releases are really fun and well thought-of\nmaking you think about how the next release could be named.\nFor people not knowing what's currently on the news in Germany\nthis is probably not very funny but - hey, cheer up, you get\na brand-new KDE release anyway which is in no way restricted\nto Germany ;-)\nThe thing which makes the K-thing somewhat offensive to mostly native \nEnglish-speaking people is perhaps that K is an unusual letter to start\na word with in English - so the whole thing is quite foreign looking.\nThese people should consider the fact that nearly anything else (like this\npost) on the Internet and PC is looking \"foreign\" for all of us who dont speak English natively.\n\nBack to the new release: Did anyone see any \"What's new\" information?\nWhat are the big new developments when you are currently using 3.4?\nIs it a good idea to switch or is the whole thing still too unstable?\nAre there any common pitfalls or things I should know if I want to try\nan upgrade?\nThanks to all the KDE developers particpating in this project for\nbringing this great software to all of us! There are just so many\nthings I got really used to over the years. When I sometimes use a colleagues Windows PC at work I am amazed how backwards this whole thing is (press \"Ctrl\"+\"+\" in Konq and try that with Explorer for example). \nThere is not much I can do with programming but I always file bug reports\nif I find any bugs and try to make these reports as valuable as\npossible for the developers and not file any duplicates - so this is my way to say thank-you. Many others will certainly agree.\n\n\n\n\n\n\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "i do totally agree. In all matters. The K-Thing is imho genious ;) And KDE is as well!"
    author: "Stephan"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "I recognize that quite a lot of work goes into KDE, and resent that certain types of constructive criticism are perpetually labelled \"trolls\" and summarily ignored.\n\nYou like it.  I don't.  Sounds more like an honest disagreement than one side being a troll and the other being unassailably correct.\n\nFor what it's worth, your conjecture about what's troublesome (sounds foreign) isn't correct, at least in my case.  \"Amaroq\" would be a fine foreign-sounding name for a media player.  \"Amarok\" would be a close second.  \"AmaroK\" is my least favorite of the three.\n\nI'd complain about iTunes and iPhoto (and WinDVD, not sure Microsoft Powerpoint is a good example) if I used them and cared about them.  Criticism means we love KDE and want it to become better."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Your first sentence is a non-sequitur. Your criticism is not constructive; it is whining, pure and simple. But there's something constructive you can do: create a couple of high-profile KDE applications with names you like."
    author: "Boudewijn Rempt"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "> Your criticism is not constructive; it is whining, pure and simple. But\n> there's something constructive you can do: create a couple of\n> high-profile KDE applications with names you like.\n\nThat's ridiculous. No one is going to create whole new applications just to satisfy their moderate dislike over how existing applications are named. And besides it is constructive. I suggest a solution: different names.\n\nAs I have been labelled a troll (the geek equivalent to \"Well I'm right so there!\"), I feel I should defend myself. I don't actually mind K[Normal Word] apps like KOffice and KMail - they do have advantages:\n\n- Similar naming method to other OS's (Win[Word], i[Word], Be[Word], K[Word])\n- You know it is a KDE app.\n- You know what it does (usually the [Word] is descriptive, eg KPDF, KDevelop)\n\nThe ones I don't like are where C's and Q's are replaced with K. How many commercial products are named like that (that don't make you cringe)? It's one step on the long and annoying road to l337 5p34k."
    author: "Tim"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "I believe the \"write your own\" argument would be better phrased as \"the authors have the right to name the apps they create.  Where does your right come from?\""
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "You mean like the Konqueror developers have the right to create an application with CSS bugs, therefore I have no right to complain about these bugs?\n\nYour logic escapes me.  I see a problem and am suggesting a fix.  People do that all the time.  If I don't have the right to do that, we may as well shut down bugs.kde.org."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "\"Your logic escapes me. I see a problem and am suggesting a fix.\"\n\nRenaming an established app is more than 'fixing a bug'. It is a major change involving the Free Software equivalent of rebranding and remarketing. What if the site where the source is downloaded from is named after the app? Or if the way you retrieve bug reports is via the app name?\n\nAnyway what track record do you have in inventing interesting names? As far as I can see you don't have the technical skills to write an app, and probably couldn't invent a suitable name for it either.\n\nOften developers will seek help to think of a good name before they release their app, and anyone in the community including yourself is welcome to make suggestions. That isn't the same as demanding that existing apps be renamed."
    author: "Richard Dale"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "The costs of fixing a bug are often high.  The costs of fixing this bug, no doubt, are high.  Much like the costs of rewriting webcore.  It's really up to the developer to decide if it's worth it.  High costs do not, however, mean the bug stops being a bug.\n\nRegarding your question about my expertise: You mean, if I can identify a bug but can't fix it, I have no right to report it?  I had no idea that this was KDE policy.  I'll make a note.  Please keep in mind that the Quanta Plus developers seemed to have found a solution for this problem in their application.\n\nAnd lastly--I'm not DEMANDING that anything be renamed.  I am simply pointing out that the current naming scheme is inconsistent at best and silly at worst.  I am SUGGESTING that renaming the apps would solve this problem.\n\nThe only thing I am demanding is the right to make honest suggestions to improve the products I use every day.  You have the right to ignore me, but you'd be wrong to label me a troll."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "> Renaming an established app is more than 'fixing a bug'. It is a major change\n> involving the Free Software equivalent of rebranding and remarketing. What if \n> the site where the source is downloaded from is named after the app? Or if\n> the way you retrieve bug reports is via the app name?\n\nI agree. Renaming existing apps is probably a reasonably amount of hassle (depending on how large/popular it is).\n \n> Anyway what track record do you have in inventing interesting names? As far\n> as I can see you don't have the technical skills to write an app, and\n> probably couldn't invent a suitable name for it either.\n\nDon't go assuming this about people. For all you know he could be in the advertising business! Furthermore, since when did development skills have anything to do with good naming skills?\n \n> Often developers will seek help to think of a good name before they release\n> their app, and anyone in the community including yourself is welcome to make\n> suggestions. That isn't the same as demanding that existing apps be renamed.\n\nWell, maybe sometimes. And sometimes they have competitions which is good. Should be done more.\n\nMaybe dot.kde.org should have a vote on alternative names for (eg) Kuickshow. Then at least we'd know how many people prefer what style. I think small utility apps should be called obvious names btw. I'd suggest \"Image Viewer\", or something."
    author: "Tim"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: ">   I'd suggest \"Image Viewer\", or something.\n\nCompare:\ngoogle for \"Kuickshow bugs\" versus google for \"Image Viewer bugs\" \n\nWhich gives better results?\n\nDistinctive names are good.  I don't particularly care either way about the K naming, but general names are terrible for software."
    author: "Leo"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "It's not my logic; it is the logic of the person you were first replying to.  I never said I agree with it.\n\nYou certainly have a right to complain.  People also have the right to ignore your complaints.  \n\nPersonally, I see no difference between KWord and KSpread versus MS Office and MS Windows and iBook and iPod.  It's just a prefix for branding.  Apple has the i- and Power- prefix, KDE has the K- prefix.\n\nUnless you're saying that Microsoft and Apple are both lousy at marketing and branding, KDE is doing a pretty standard thing."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Would \"AmaroK\"'s analogue in the commercial world be \"PowerPointMICROSOFT\"?  I don't see a lot of that out there."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "*sigh*\n\nOkay, okay, we all bow to your brilliant expose of the clearly inferior names of KDE applications.\n\nNow, I need to get work done, so I'll return to my fatally flawed desktop, thanks."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "I would have settled for a \"I can see how some people might think that, but I don't agree\" but suit yourself.  I wasn't looking for surrender so much as an end to the denial of the validity of my position.\n\nBack to my own fatally flawed desktop too."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "amaroK is named after the song \"Amarok\" by a famous singer, the amaroK-team is a fan of.\n\nThe could have named is Kamarok, but decided to emphasize the last K in the word in stead :)"
    author: "rinse"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "Bugs and names are very dissimilar. Bugs generally affect everyone, and everyone wants them fixed, but names are matters of taste, some people like them and some not, you can't 'fix' a name so that everyone would like it.\n\nI have nothing against suggesting names, but demanding that names of established apps because some people do not like them is silly"
    author: "Arb"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "Calling it silly is being kind."
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "This comparison is transparent sophistry, and your followup is even worse, and damages your credibility in the eyes of reasonable persons.  Your dislike or annoyance about an app name is not a bug."
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "> The ones I don't like are where C's and Q's are replaced with K. \n> How many commercial products are named like that \n> (that don't make you cringe)? \n> It's one step on the long and annoying road to l337 5p34k.\n\nI can see what you mean but don't agree. I find the one great K in KDE apps not only quite funny, but also very useful. Especially in the smaller helper apps, like e.g. Image Viewers they enable me to assert wether the application is useful to me by only looking at the name. Kuickshow is an ideal example, IMVHO.\nOk, KQuickshow would work, too, but I find that somewhat lame (And it can't be pronounced without stutering). \"Image Viewer\" or \"Quickshow\" would not enable me to do that, since I can't see wether it is written for Windows, Gnome, X, Qt only, or whatever. Even worse are names like Gwenview, which misgide you.\n\n"
    author: "Fabio"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "\"I suggest a solution: different names.\"\n\nOh yeah, that's real constructive, like \"I don't like this.  My constructive offering: do it differently.\"\n\n\"The ones I don't like are where C's and Q's are replaced with K.\"\n\nYou complained about Koalition which, as has been pointed out is a German word. So all we have here is your personal opinion, which is worth squat, and your ignorance, which is worth less.  And your dishonesty, since your subject line was \"KKill\".\n\n\"It's one step on the long and annoying road to l337 5p34k.\"\n\nIt's also one step on the road to the heat death of the universe.  See\nhttp://en.wikipedia.org/wiki/Slippery_slope#The_slippery_slope_as_fallacy\n(\"the slippery slope claim requires independent justification to connect the inevitability of B to an occurrence of A. Otherwise the slippery slope scheme merely serves as a device of sophistry\")"
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Don't be silly.\n\nAmarok was named after a song called \"Amarok\".\n\nWhy would calling it \"Amaroq\" be in any way better? Why is randomly substituting \"q\"-s better than randomly substituting \"k\"-s?"
    author: "KOffice fan"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Because \"Amaroq\" is the widely accepted transliteration of the Inuit word for wolf?"
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "So widely accepted that the spelling \"amarok\" beats it 17:1 in Google?  (That's *after* excluding the terms kde and oldfield).  \n\nSo widely accepted that amarok (the wolf) has a wikipedia entry and amaroq doesn't?  \n\n\n"
    author: "cm"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "Mike Oldfield chose a bad transliteration, almost all those other references are derived from that one source.\n\nGoogle (thank goodness) is not the arbiter of transliteration of Inuit words into latin letters.  Consult a linguist next time and you'll get better results.\n\nIncidentally, I did say Amarok was \"a close second\" naming-wise.  I'd greatly prefer \"Amarok\" to \"AmaroK\"."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "- Why don't you cite your linguistic source?   This is in no way to doubt your knowledge but why should I believe an ac?  \n\n- Have you already corrected/added the Wikipedia entry?  Don't forget to give references. \n\n- It's funny how language works.  People use a certain meaning or spelling of a word that may be considered wrong and because of that use it becomes accepted at some point.  One example is the wrong use of the word \"irony\" that is now recognized by the American Heritage Dictionary.  See the section \"Usage controversy\" in the article http://en.wikipedia.org/wiki/Irony .  \n\n- Google may not be an arbiter of original linguistic transliteration of a word but it is an indicator of actual use.  Good luck convincing the world that it is wrong.  Oh, right, that's what you're trying here.  \n\n"
    author: "cm"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "Greenland went through the process of providing dual names to its cities (Danish and Inuit).  Although the Danes liked to spell Inuit words with K's, it turns out the Inuits spelled the city names with Q's.\n\nIn fact, there's not a single Inuit word containing the letter K.  The K, you see, is a reminder of a long past of Danish cultural dominance.\n\nThanks for caring.  Linguistic source?  Any Inuit dictionary, published in the past five years.  No K's.  Anywhere."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "Hey!  Turns out we're both wrong.\n\nIt's Amaruq.\n\nGo figure.\n\nhttp://www.livingdictionary.com/term/viewTerm.jsp?term=49171742339\n\nYou can also get Amaguk (with a K!) in the Nunatsiavut dialect."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "Thanks for that background info. \n\nI'd still say that anything but the spelling amarok is not widely used/accepted in the real world, though (no matter if the other spellings are more correct...) \n\n"
    author: "cm"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "Regarding languages, I define \"the real world\" as the people who speak the language in question natively--which is always a minority of the world, no matter what language you're talking about.  Mistakes by foreigners are frequent, and often the same mistakes are repeated again and again, but they rarely become \"official\"."
    author: "ac"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "But we're not talking about speaking the Inuit language here but about what speakers of English / a well-known musician / the internet audience have made out of a word of Inuit origin.  And for the latter the spelling amarok is much more widely used. \n\nSpelling, pronounciation and meaning of words are subject to change.  For example, think of how Americans pronounce the expression \"deja vu\".  It's not at all like the French pronounciation of \"d\u00e9j\u00e0 vu\".  Note also that d\u00e9j\u00e0 lost its accents in many English texts.   Do the French complain?  \n\n"
    author: "cm"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "Given the way the French regard their language, they probably do. :-)  But that doesn't make ac's sophistry any more valid."
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "No, only you are wrong.  Amarok is the common spelling, and the name of the song.  Inuit spellings are not privileged in this regard."
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "It would be better in the minds of mental defectives."
    author: "Jay"
  - subject: "Re: KKill"
    date: 2008-02-04
    body: "oi mate st5op fucking ya mum"
    author: "hfdjvjvnnv "
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "What about \"Pleeeeaaasssee stop with the dodgy K names! They aren't cool, they're just annoying.\" is constructive?  Generalization of one's personal opinion into some sort of absolute truth is destructive, not constructive.\n\n"
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "I love the knames too !\n\nI can see very easily if an app will be well integrated to my desktop and I remember the name of them more easily :)\n\nKeep on going ! ;)"
    author: "Keep them"
  - subject: "Re: KKill"
    date: 2005-10-18
    body: "What a kunt!"
    author: "Jim"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "The original name for kate was kant.  Several English speakers (including myself) pointed out the phonetic similarity to a very strong obscenity.\n\nI'm pretty sure KDE Advanced Text Editor is a contrived acronym. "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KKill"
    date: 2005-10-26
    body: "One of the greatest philosophers of all time was an obscenity?"
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-27
    body: "No, he meant the pronounciation of the name Kant had a \"phonetic similarity to a very strong obscenity\" (in English).\n\n"
    author: "cm"
  - subject: "Re: KKill"
    date: 2005-10-27
    body: "2 times yes ;)\nWe renamed Kant because of this problematic spelling ;)\nAnd yes, Kate was chosen by me because it\n a) starts with K\n b) is no hacked together name but just a normal girl name\n    (first thought was to use Katie, like the female Konqui, but Katie was    already taken by a sf.net project)\nThe KDE Adv... stuff is just derived, in the old KDE 2.x releases, KWrite was the KDE Adv... thingy, but it just matched the KATE letters by accident so it was taken over."
    author: "Christoph Cullmann"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Hmm, there's also a practical side to it...it's easy to search the internet with a program name that is distinguishable from common (english) words, you generally get better link relevance. "
    author: "lynx"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Good point! Especially from a \"kpromoteing\" point of view."
    author: "Macavity"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "This can be viewed as a naming convention.\nAnd this is good. I find convenient, when I browse news,\nto detect immediatly if a software is a KDE software or not.\nJust as G for GNOME is a good thing: some people look for\napplications for the desktop they use, and do not want to\ncheck readme/download and inspect if this it is not for\ntheir desktop.\nAnd do not tell apps work nicely in other desktop, that's\nstupid: they load a bunch of useless (for already launched apps)\nlibraries, they do not integrate well and are not consitent.\n\nIn one word, KApplication is a good name for KDE applications,\neven if the K replace a valid C/Q in an English word - especially\nif the word can be a valide German word (why use only English\nnames?). At least, the app name is not a single word (Windows,\nWord, Write, etc. is really a stupid naming scheme and brings\ncopyright absurdities btw).\n"
    author: "anon"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: ">In one word, KApplication is a good name for KDE applications, even if the K replace a valid C/Q in an English word - especially if the word can be a valide German word (why use only English names?)\n\nI agree that if substituting with a k makes it a valid German word that is just an added bonus (for those who speak German that is), but any and all names should have English origin, as only some 85-100 milion people worldwide speak German. It annoys me that there is a progame involved in the buildprocess called \"meinprog\" (eventhough I'm Danish, and hence understand basic German).\n\nJust my 0.2&#8364; opinion."
    author: "Macavity"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "> as only some 85-100 milion people worldwide speak German\n\n<nitpicking>\nIt's more than 120 million people worldwide and it is the language with the most native speakers in the EU.\n</nitpicking>"
    author: "Christian Loose"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "If we name apps by popularity, shouldn't they be WinWord and WinConsole?  After all, more people use apps prefixed with Win- than speak German *or* English.  ;)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "According to your logic, all apps should be named in Chinese.\n\nBTW, it's meinproc, not meinprog."
    author: "Henrique"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "> It annoys me that there is a progame involved in the buildprocess \n> called \"meinprog\"\n\nthen why are you using it?\n\nnow that 'gcc' thingy really drives me crazy...\nwhat kind of language is THAT supposed to be!"
    author: "bangert"
  - subject: "GCC (was: Re: KKill)"
    date: 2005-10-20
    body: "cc means \"C Compiler\"\n\nThe g is for GNU.\n\nSo in full: \"GNU C Compiler\"\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: GCC (was: Re: KKill)"
    date: 2005-10-23
    body: "Gcc means GNU Compiler Collection"
    author: "ZeD"
  - subject: "Re: GCC (was: Re: KKill)"
    date: 2005-10-23
    body: "That is the new meaning."
    author: "Nicolas Goutte"
  - subject: "Re: GCC (was: Re: KKill)"
    date: 2005-10-26
    body: "What exactly is your point?  The old meaning was GNU C Compiler."
    author: "Jay"
  - subject: "Re: GCC (was: Re: KKill)"
    date: 2005-10-26
    body: "Oy -- where's the delete button?  Mea maxima culpa for my foolish post above."
    author: "Jay"
  - subject: "Re: KKill"
    date: 2005-10-20
    body: "According to your logic, all apps should be named in Chinese.\n\nBTW, it's meinproc, not meinprog."
    author: "Henrique"
  - subject: "Re: KKill"
    date: 2005-10-19
    body: "Why?  It is spelled correctly, and there was no substitutions of k for c.\n\nAssuming you're German."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KKill"
    date: 2005-11-04
    body: "and Czech too ;)"
    author: "Rezza"
  - subject: "Keep the K"
    date: 2005-10-22
    body: "No, the K is important, it lets you know its a KDE compliant application off the bat.\n\nMuch as the Gnome people should stick with G-apps..\n\nDo we end up with some silly names? Sure, but its useful."
    author: "Ziggy"
  - subject: "YOU are annoying,"
    date: 2005-10-26
    body: "Only an utter fool would complain about a release name."
    author: "Jay"
  - subject: "Splash screen"
    date: 2005-10-18
    body: "I really hope the default splash screen in the final release won't be the one in the screenshots. I mean, it looks like it was done by a 10 y.o. kid... especially the use of the Impact font (how lame!). And the shade of blue clashes with the default background. And the K logo is really out of place. And... please change it!!"
    author: "ac"
  - subject: "Re: Splash screen"
    date: 2005-10-18
    body: "I agree... the double K-logo looks out-of-place.  Maybe some darker colors please?\n\nThanks\nSam"
    author: "Sam Weber"
  - subject: "Re: Splash screen"
    date: 2005-10-18
    body: "Agreed ... the splash is pretty horrible."
    author: "Eike Heinb"
  - subject: "Re: Splash screen"
    date: 2005-10-19
    body: "yup, it's pretty ugly, perhaps time to do a little color-theme updating  for the final release? aren't we all pretty tired of the blue?"
    author: "Anders Storsveen"
  - subject: "Re: Splash screen"
    date: 2005-10-20
    body: "Yep, the color scheme for the splash screen is not too good, looks washed out."
    author: "ac"
  - subject: "Re: Splash screen"
    date: 2005-10-21
    body: "ok, so I see many people agree with me... any plan to change it? any artist working on it?"
    author: "ac"
  - subject: "Oh man!"
    date: 2005-10-18
    body: "I just compiled beta 1 !\n\nBut something is broken (panels).\n\nSecond attempt under way! :)"
    author: "Mark Czubin"
  - subject: "Compilation Requirements"
    date: 2005-10-18
    body: "The compilation requirements page says \"While there have been reports of successful KDE compilations with the so-called gcc-2.96 and gcc-3.4 (cvs), the KDE project at this time recommends the use of gcc-3.3.1 or a version\" known to compile KDE correctly.  \n'gcc-3.4 (cvs)' should have been 'gcc-4.1 (cvs)', and 'gcc-3.3.1' should have been gcc-3.3 or later."
    author: "Anonnymous"
  - subject: "Re: Compilation Requirements"
    date: 2005-10-18
    body: "Hey, no problem at all. Slackware includes gcc-3.3.6 :)\n\nNearly all other must downgrade his cra.. holy stuff ;)\n\n"
    author: "anonymous"
  - subject: "Re: Compilation Requirements"
    date: 2005-10-20
    body: "That's out of date, you compile with GCC 3.3 , GCC 3.4 , GCC 4.0 maybe GCC CVS (for the bleadding edge people)\n"
    author: "Anonnymous Person"
  - subject: "Re: Compilation Requirements"
    date: 2005-10-22
    body: "For what it's worth, GCC 4.0.2 is required for certain things anyway (visibility support) and compiles KDE perfectly.\n\n[alistair] 15:24 [~] gcc -v\nUsing built-in specs.\nTarget: x86_64-unknown-linux-gnu\nConfigured with: ../gcc-4.0.1/configure --prefix=/usr --libexecdir=/usr/lib --enable-shared --enable-threads=posix --enable-__cxa_atexit --enable-clocale=gnu --enable-languages=c,c++ --disable-multilib\nThread model: posix\ngcc version 4.0.1"
    author: "Alistair John Strachan"
  - subject: "progress against feature plan?"
    date: 2005-10-18
    body: "How does beta2 relate to \"KDE 3.5 Feature Plan\", http://developer.kde.org/development-versions/kde-3.5-features.html , which shows roughly 1/3 features still TODO and another 1/3 still in progress?  There's no \"Last Modified\" date on the feature plan page.\n\nIt's amazing the KDE contributors are able to develop 3.4.3, 3.5, and KDE4 in parallel!\n\n"
    author: "S Page"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-18
    body: "Never trust the feature plan.\n\nSome things on there will never be done. Many things that are done never appear there."
    author: "Eike Heinb"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-19
    body: "Correct, take one of the widely known new feature, the addblock filter for Konqueror. It's not on the list as far as I can see.\n\nAnd you forgot to mention the case when the developers have not had time/forgoten to update the status.  "
    author: "Morty"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-20
    body: "then take it offline if it doesnt contain any usefull and updated info !\nits misleading ...."
    author: "alex"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-20
    body: "it's a resource for the KDE developers. It was never meant for the end-users. That's why it is on developer.kde.org.\n\nSo don't look at it if it contains misleading information for you."
    author: "Christian Loose"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-19
    body: "You must be joking.\n\n98% of KDE developers work for 3.5, 2% work for KDE 4.0, and yes 0% work for 3.4 as 3.4 is now deprecated."
    author: "Artem Tashkinov"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-23
    body: "Then who is making the updates to the 3.4 BRANCH on SVN?\n\nSince 3.5.0 still hasn't been released, there should be a final bug fix release for the 3.4 series."
    author: "James Richard Tyrer"
  - subject: "Re: progress against feature plan?"
    date: 2005-10-23
    body: "Nobody anymore.\n\nKDE 3.4.3 was the last release version for KDE 3.4.x (except emergencies).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "downgrade"
    date: 2005-10-19
    body: "If I find that I can't that I can't function in with Beta 2, is there some safe way for me to downgrade back to 3.4?"
    author: "It's me Mario"
  - subject: "Re: downgrade"
    date: 2005-10-19
    body: "Oh, it would probably help if I mention I'm using kubuntu."
    author: "It's me Mario"
  - subject: "Re: downgrade"
    date: 2005-10-19
    body: "de-install all of KDE (deinstalling xorg will do the trick);\nremove beta1/2 lines from /etc/apt/sources.list;\napt-get update;\napt-get install kubuntu-desktop\nthat's it."
    author: "Humberto Massa"
  - subject: "Re: downgrade"
    date: 2005-10-19
    body: "1. if you want to downgrade only one package:\n\napt-cache policy <packagename>\nsudo apt-get install <packagename>=<older-version>\n\nfor example:\nsudo apt-get install kontact=4:3.4.3-0ubuntu3\n\nand you should hold the package.\n\n2. if you want to fully downgrade, increase the priority of the older packages - man apt_preferences.\n"
    author: "pakos"
  - subject: "Re: downgrade"
    date: 2005-10-19
    body: "KDE 3.4 and 3.5 generally install into different directories.  You should, unless Ubuntu crippled something, be able to select 3.4 from the login screen if 3.5 dies horribly.  It even keeps its configuration in a different directory. :-)"
    author: "Trejkaz"
  - subject: "Re: downgrade"
    date: 2005-10-19
    body: "It's Suse behaviour, not kde way to do.\n\nSo, in Debian/Ubuntu, if you install 3.5, it remove 3.4"
    author: "gnumdk"
  - subject: "Re: downgrade"
    date: 2005-10-20
    body: "Not just Suse.  Gentoo separate them as well.\n\nAlso, one could install it to /opt/kde3.5 if one wanted, to keep it separate."
    author: "Trejkaz"
  - subject: "Everthing fine on Suse 10.0"
    date: 2005-10-19
    body: "I installed it just when I saw the new konstruct on kde-apps getting the rpms for my suse 10.0 box. It really rocks and surelly is very stable.\nJust kopete is giving me problems with ICQ connection and empty messages from some ICQ contacts, I hope they fix those problems soon.\nAnyway, KDE 3.5 is shiny and beautifull, but does anybody know if the KDM theme used by Suse 10.0 that shows all users in a windows XP style also works for another distros or is a Novell hack?"
    author: "Iuri Fiedoruk"
  - subject: "Re: Everthing fine on Suse 10.0"
    date: 2005-10-19
    body: "The theme requires http://websvn.kde.org/branches/work/coolos_kdm/"
    author: "Stephan Kulow"
  - subject: "Changelogs"
    date: 2005-10-19
    body: "Jeez, doesn't anybody post changelogs anymore?  They'll go through the trouble of doing a bunch of screenshots, but trying to find a changelog is like pulling teeth."
    author: "Sean"
  - subject: "Re: Changelogs"
    date: 2005-10-19
    body: "Yes, that's what I'm looking for too :("
    author: "Lee"
  - subject: "Re: Changelogs"
    date: 2005-10-19
    body: "there will be a changelog only for the final release"
    author: "ac"
  - subject: "Re: Changelogs"
    date: 2005-10-19
    body: "Hey there must be changelog for 3.5 beta 2 version somewhere. If it's not how to  help in debugging? Developers can't expect from us clicking on all icons and trying all features.\n\nCan somebody post a list of features in beta to debug. \n\nI very like conception of mozilla firefox QA Test. It would be great to see such document for kde beta\n"
    author: "what? changelogs ony for final??"
  - subject: "Re: Changelogs"
    date: 2005-10-20
    body: "> I very like conception of mozilla firefox QA Test. It would be great to see such document for kde beta\n\nPlease go ahead. Your QA test documents will be surely welcomed in the kde project."
    author: "cl"
  - subject: "Re: Changelogs"
    date: 2005-10-20
    body: "The feature plan is supposed to be the changelog until KDE 3.5 (final):\nhttp://developer.kde.org/development-versions/kde-3.5-features.html\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Unstable"
    date: 2005-10-19
    body: "I just updated from KDE 3.5 Beta 1 to KDE 3.5 Beta 2 on SuSE 10.0. I've found a couple of bugs/oddities.\n\nKontact crashes when you try to load the korganizer part after the kaddressbook part.\n\nKonqueror file manager mode lags the entire computer down like crazy. 99% CPU usage. I have to kill the process every time I run konqueror filemanagement mode.\n\nI think it maybe have something to do with me updating from KDE 3.5 Beta 1.. so I'm gonna uninstall all of the KDE packages and install them again, I'll let everyone know if that fixes my problems. :D"
    author: "quaff"
  - subject: "Re: Unstable NOT"
    date: 2005-10-19
    body: "working perfectly here, so the Problem is with your Distribution , go to the Forum /Page/Wiki/Email there an write there !\n\nchris"
    author: "ch"
  - subject: "Re: Unstable NOT"
    date: 2005-10-19
    body: "erm... where do i post? hahaha your post is really confusing.."
    author: "quaff"
  - subject: "Re: Unstable NOT"
    date: 2005-10-19
    body: "I believe he's saying \"Go to whatever method of discussion your distribution uses and post about it there\", implying that the problem is on their end rather than KDE.\n\nHe's probably right, but not necessarily, and it's phrased as a brush off.  You would do better to discuss it in a bug forum for either KDE or your distro than discussing it on a news site, though."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Unstable"
    date: 2005-10-19
    body: "Kill the 'tee' process.\n\nAll is good then"
    author: "VenimK"
  - subject: "Re: Unstable"
    date: 2005-10-23
    body: "I have the same problem with konqueror and I don't have \"tee\" process"
    author: "cassini83"
  - subject: "Re: Unstable"
    date: 2005-10-25
    body: "> Konqueror file manager mode lags the entire computer down like crazy. 99% CPU usage.\n\nThere is an updated kdelibs.rpm available on ftp.kde.org, try if it fixes your problem."
    author: "Anonymous"
  - subject: "Re: Unstable"
    date: 2005-10-25
    body: "That fixed the problem. 10x a loooot!"
    author: "cassini83"
  - subject: "suse patch"
    date: 2005-10-19
    body: "how about the suse patch for the Kmenu which adds a search in kde 3.4.3? i wonder if it'll be in 3.5... as suus tested it aparantly, i guess its kind'a safe?!?!?\n\nsuse 10.0 has it, it'd be a pity if a suse user upgrades 10.0 to the final kde 3.5 and finds a nice feature missing..."
    author: "superstoned"
  - subject: "Re: suse patch"
    date: 2005-10-19
    body: "oh thats not something to worry about, its a different package i think... cause i updated and its still there haha but just warning you.. the suse packages for kde 3.5 beta 2 is buggy.. kontact is kinda buggy, and file management mode for konqueror uses up too much cpu... 99% :S\n\n"
    author: "quaff"
  - subject: "Re: suse patch"
    date: 2005-10-19
    body: "well, i guess the bugs are fixed in kde 3.5 as i don't see them here (KDE SVN, gentoo). and if you're right, i don't have to worry about the menu search :D"
    author: "superstoned"
  - subject: "Re: suse patch"
    date: 2005-10-19
    body: "beta 1 packages worked quite fine, beta 2 is really messed with konqui being extremely slow, so im downgrading back to beta1.."
    author: "eol"
  - subject: "Re: suse patch"
    date: 2005-10-19
    body: "Not a different package, suse menu patch is in kde 3.5 packages..."
    author: "gnumdk"
  - subject: "kolourpaint"
    date: 2005-10-19
    body: "what happened to it?  development seems to have gotten to a standstill."
    author: "ac"
  - subject: "Re: kolourpaint"
    date: 2005-10-19
    body: "It has the features a small bitmap paint application need and no grave bugs, what more development does it need?"
    author: "Morty"
  - subject: "Re: kolourpaint"
    date: 2005-10-21
    body: "It has lots planned, Morty, but it hasn't happened. I guess it's what happens when people let the KDE development cycle decide when the software gets updated. STALL"
    author: "monsy"
  - subject: "Re: kolourpaint"
    date: 2005-10-21
    body: "To repeat one of my earlier posts: the main developer had major problems with SVN. (I have no idea about his current situation.)\n\nI think that this was/is the main reason why no significant development was done; it is not linked to any development cycle.\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: kolourpaint"
    date: 2005-10-19
    body: "take a look at krita...\nMaybe that's what you think of"
    author: "me"
  - subject: "Beta"
    date: 2005-10-19
    body: "tested the atlantik board game, the usability leaves room for improvements. And I was unable to disconnect.\n\nBut: this KDE 3.5 has a very good look and feel, it is different and better than before.\n\nI used KDE 2.2, back then linux was so nice, but in KDE 3.x line kde was not that much inspiring any more. now I get again this positive feeling. This beta is quite stable. \n\nKonqueror is still a bit slow in rendering but this is probably no real issue. The setting menu is still horrible. What are view profiles btw\n\nFor a beta this is stablissimo.\n\nWhy isnt Amarok shipped with KDE 3.5\n\nmenu panel / internet / \nresort the menu. Browser shall come first. Sort by priority is better than bz alphabet.\n\nmz personal opinion is than a menu as we have it with Ms Windows start menu is obsolete. You do not need it. This could be done via an xml/based  browser and a local knowledge base. Everbody can use the www. Kde has kIoslaves. So why not search for an installed app by a google like search option or like html. If it on your computer you get an info page displayed which enables you to launch the app with one click. Further similar links to other applications or the group, a deinstall option, a link to an application related online web forum. If it is not installed a www page with application data is displazed which enables you to download the app by one klick. "
    author: "betatest"
  - subject: "Re: Beta"
    date: 2005-10-20
    body: "amaroK is in the addons b/c of how they choose to release it(i.e. on their own release cycle).\n\nThe menu as you know it, should be changing quite a bit for kde4.  Keep up to date on that by visiting <a href=\"http://www.canllaith.org/blog/\">canallaith's blog</a>."
    author: "p0z3r"
  - subject: "Re: Beta"
    date: 2005-10-20
    body: "View profiles save the state of your current Konqueror:  toolbars, position, URLs, tabs, split windows, etc.  When you load a profile, it changes konqueror to the way you left it -- this allows you to save a useful and complicated setup, with different folders in a split view and exactly the functions you need in the toolbar, and load it when you need it.\nThe view profile you save as Web Browsing comes up when you start Konqi as a browser (and the URL you save in that comes up as your start page, as opposed to your home page.)  The view profile you save as File Management comes up when you start Konqi as a File Manager.  This allows you to have a simple firefox-like toolbar for web browsing, but a full-featured one for file browsing.\n\namaroK has releases separately, and much more often, than KDE.\n\nRe menus:  See programs:/ (which you can use tools->find on) and klik:/\nThese are steps in that direction."
    author: "kundor"
  - subject: "6 Configure menu entries == not good"
    date: 2005-10-20
    body: "I agree with you about the konqueror settings menu, why do you need a separate menu entry to configure spell checking!?"
    author: "Dan Z"
  - subject: "Re: 6 Configure menu entries == not good"
    date: 2005-10-20
    body: "First of this is because you configure the KDE wide spell checking with this. Besides it's plainly more accessible located where it is now, rather than buried in some menu somewhere. When I need to change the spell checking I want it easy accessible, I don't want to hunt for it(If I have to it's more likely I just skip it). Lots of users, like me, regularly switch between two or more languages. "
    author: "Morty"
  - subject: "Build problems, please help!"
    date: 2005-10-20
    body: "I downloaded konstruct and tried to build the beta, but I ran into a number of problems in the build process.  I don't know if it's just me with a uncommon setup (RedHat EL3 WS), and most of the developers are running something like SUSE with all the up-to-date packages.  But I've never had a completely trouble-free build experience.\n\nFirst some issues with konstruct, checksum for kompose is mismatched, I had to manually update the checksum file.  Also a suggestion, Makefiles should be provided for the apps directories so they can be built individually.\n\nkdeaddons-3.4.92/kfile-plugins/cert failed to compile, it couldn't find the kerberos files which on the RedHat EL3 system lives under /usr/kerberos, I had to manually add the -I/usr/keberos/include -L/usr/keberos/lib to the CXXFLAGS, the configure script should've taken care of that.\n\nThere was another build problem, I forgot which package (kdebase?) that required the files libacl.{a,la} and libattr.{a,la} under /lib, when they're normally in /usr/lib,  sounds like an autoconf problem.\n\nmetabar/src/Makefile failed to set KDE_INCLUDES to the KDE include headers, causing compilation to fail.\n\nscribus failed compiling, after a little digging, it's found that my python is too old (2.2), while you need > 2.3 for python support, reran configure using --without-python, seemed to build fine, again, should've been taken care of by the autoconf scripts.\n\nFinally, tellico (the collection manager), sounds like a really neat application that I want to use.  But unfortunately, fails to compile, and this one I have not been able to resolve.\n\nThe error output is below:\n\nIn file included from tellico.all_cpp.cpp:40:\ncalendarhandler.cpp: In static member function `static void\n   Tellico::CalendarHandler::addLoans(Tellico::Vector<Tellico::Data::Loan>,\n   KCal::CalendarResources*)':\ncalendarhandler.cpp:49: no matching function for call to `\n   KCal::CalendarResources::CalendarResources()'\n/usr/local/kde3.5-beta2/include/libkcal/calendarresources.h:67: candidates are:\n   KCal::CalendarResources::CalendarResources(const KCal::CalendarResources&)\n/usr/local/kde3.5-beta2/include/libkcal/calendarresources.h:161:\n    KCal::CalendarResources::CalendarResources(const QString&, const QString& =\n   QString::fromLatin1(const char*, int)(-1))\n...\n\nMaybe I should be reporting these issues on bugs.kde.org, instead of ranting here where no one is likely to read or do anyting about them.  But using bugs.kde.org to report beta releases, especially on build issues, is a pretty frustrating experience, if there's a more appropriate place I'll be glad to post there.  I hope the final release has these things cleaned up.  But even with these issues KDE is still the best desktop anywhere, bar none."
    author: "Dan Z"
  - subject: "Re: Build problems, please help!"
    date: 2005-10-20
    body: "Sometimes with build issues you can try the relevant mailinglists. But check the archives first, as it may already be reported or fixed. \n\nBesides neither Scribus and Tellico has anything to do with the 3.5 beta, consult their respective homepages for the maintainers preferred way of feedback."
    author: "Morty"
  - subject: "Re: Build problems, please help!"
    date: 2005-10-23
    body: "The KCal api changed. Tellico 1.0.3 should compile cleanly with kdepim 3.5."
    author: "Robby Stephenson"
  - subject: "Can't report bugs"
    date: 2005-10-20
    body: "OK, I've downloaded Klax, tested some stuff and I can't wait to report the bugs :)\n\nBut I can't. I forgot my bugs.kde.org password, and I've requested another one some 24 hours ago. And then another one today, suspecting that something had gone wrong with yesterdays request. I got no email until now.\n\nAnybody knows whether there's a problem with bugs.kde.org? Or do I just need to wait some more?\n\n"
    author: "tst"
  - subject: "Re: Can't report bugs"
    date: 2005-10-20
    body: "If the problem cannot be solved by yourself (check email address, check also your spam filters), please ask the Sysadmins: sysadmin@kde.org"
    author: "Nicolas Goutte"
  - subject: "Re: Can't report bugs"
    date: 2005-10-20
    body: "Thanks."
    author: "tst"
  - subject: "taskbar text is unreadable in elegant mode"
    date: 2005-10-20
    body: "kde 3.5beta2 is nice.. i installed it on SuSE 10.0\n\nbut, anybody knows, how to set a diffrent font-color for the taskbar-buttons ?\nthe default color is the same as the button-text-color. but if you use a dark kicker-background, then the text (on the taskbar) is unreadable.\n\nbefore, i used \"mtaskbar\", it has got an option called \"use window background color for text\" and \"show text glow\". is there a possibility to set this options for the new \"elegant or transparent\" taskbar in KDE 3.5 ?\n\nthanks\n"
    author: "anonymous"
  - subject: "Re: taskbar text is unreadable in elegant mode"
    date: 2005-10-21
    body: "i think you have to resort to using another style which allows for this..."
    author: "superstoned"
  - subject: "Re: taskbar text is unreadable in elegant mode"
    date: 2005-10-22
    body: "I think it is a good idea to have an option to choose the default font-color for taskbar buttons. Elegant mode is wonderfull but when the taskbar is transparent and the desktop wallpaper is dark the text is unreadable. There is one solution but I don't like it. From Control Center -> Appearance & Themes -> Colors -> Widget Color -> Window text you make the color to be white for example. It affects on the taskbar font-color but also on the menus (and I don't want  to have white menus).  "
    author: "plamen_t"
  - subject: "Re: taskbar text is unreadable in elegant mode"
    date: 2005-10-22
    body: "yes, that's right, if the menus are white, then i can't read the menus anymore.. it would be perfect, if the taskbar doesn't inherit the font color from the usual button in kde. taskbar should define its own fontcolor. and that should be configurable in the controlcenter/appearence/Colors.\njust like the color setting \"alternative background for lists\".\n\ni think, there should be even some more entries for other widgets. for example a color-setting kmenu, as long as kmenu doesn't support themes.\n"
    author: "anonymous"
  - subject: "Re: taskbar text is unreadable in elegant mode"
    date: 2006-02-07
    body: "I see it has been sometime since the last post. I've been searching for a way to change font color of the taskbar only. I haven't found a way yet. The problem is unreadable text in the taskbar when using dark colors or non-contrasting colors. This seem quite the oversight. Has anyone figured a way to change the colors yet? Currently using KDE 3.5 on FC4."
    author: "Bob"
  - subject: "Re: taskbar text is unreadable in elegant mode"
    date: 2006-12-13
    body: "1. right click on the task bar, select configure panel.\n2. select the task bar icon in the configure KDE-Panel\n3. from the Appearance option, select/ For Transparency / \n4. apply\n\nthe result gives a stroked font, it's not perfect but its the only choice I have found for a dark background."
    author: "Ric"
  - subject: "anti k-naming"
    date: 2005-10-24
    body: "Going by whats happening in most of the posts in here, people are finally getting sick of the k-naming scheme. Hopefully KDE dev's take heed."
    author: "brother"
  - subject: "Re: anti k-naming"
    date: 2005-10-24
    body: "Let's rename ourselves to QDE"
    author: "ac"
  - subject: "Re: anti k-naming"
    date: 2005-10-26
    body: ">>Hopefully KDE dev's take heed.\n\nOr KDE dev's be kursed!\n\n"
    author: "ac"
  - subject: "Most of the posts?"
    date: 2005-10-26
    body: "I think your abacus is broken."
    author: "Jay"
  - subject: "Re: anti k-naming"
    date: 2005-10-27
    body: "I am extraordinarily surprised that the naming of beta-versions is an aim of criticism. On the one hand it does not affect the package's use in any way, on the other hand it is creative and intelligent, with another word: witty.\n\nFinding one word describing what the whole german nation is affected by momentarily, with the restriction to words beginning with 'k' is a challenge requiring some esprit.\n\nMy \u0080.02\n        Ingolf\n\nbtw: NRK altid klassisk (http://nettradio.nrk.no) is just sending Brahms opus 29, \"Schaffe in mir, Gott, ein rein Herz\" (\"Create in me, God, a clean heart\"). Very beautiful."
    author: "Ingolf"
---
The KDE Project is pleased to announce</a> the immediate availability of <a href="http://www.kde.org/announcements/announce-3.5beta2.php">KDE 3.5 Beta 2</a>, dubbed "Koalition".  The <a href="http://www.kde.org/info/3.5beta2.php">3.5 Beta 2 info page</a> lists where to download with packages available for <a href="ftp://xentac.net/tpowa">Archlinux</a>, <a href="http://www.kubuntu.org/announcements/kde-35beta2.php">Kubuntu</a>, <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta2/contrib/Slackware/10.2/README">Slackware</a> and <a href="http://download.kde.org/binarydownload.html?url=/unstable/3.5-beta2/SuSE/README">SUSE</a>.  
The KDE team asks everyone to try the version and give feedback through <a href="http://bugs.kde.org">the bug tracking system</a>.  A <a href="http://developer.kde.org/~binner/klax/devel.html">Klax Live CD</a> is available or you can build it yourself using <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>.  If you want to see what it looks like <a href="http://shots.osdir.com/slideshows/slideshow.php?release=474&amp;slide=3">OSDir has screenshots</a>.

<!--break-->
