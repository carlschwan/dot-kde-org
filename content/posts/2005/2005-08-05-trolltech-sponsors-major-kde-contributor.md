---
title: "Trolltech Sponsors Major KDE Contributor"
date:    2005-08-05
authors:
  - "aseigo"
slug:    trolltech-sponsors-major-kde-contributor
comments:
  - subject: "Congrats."
    date: 2005-08-05
    body: "Good news! Congrats... Always good to see $ flowing into the community.\n\nAnd now, a request of those developing ideas for next gen desktops... Please at least LOOK at http://www.usercreations.com/spring/ and what they have done. Very neat idea."
    author: "Jonathan Dietrich"
  - subject: "Re: Congrats."
    date: 2005-08-07
    body: " And please make a \"revert to totally conservative and boring desktop\" checkbox! :-D\n\n Not to ruin your crative energies, but i absolutely dread when being forced to use anything \"workflow oriented\".. as my workflow usually doesnt match that of the creator :-P So please dont make anything too invasive without making a turn-it-off.\n\n Other then that: HIT ME! :-D~~\n\n/macavity\n\n "
    author: "macavity"
  - subject: "Another idea..."
    date: 2005-08-12
    body: "Everyone should looks at http://www.symphonyos.com/mezzo.html\n\nThe Mezzo desktop looks very atractive... they just aren\u00b4t using the Right Technology for it.\n"
    author: "Laerte"
  - subject: "Congrats to Aaron :-)"
    date: 2005-08-05
    body: "Now the release of KDE4 can just be a matter of minutes ;-)"
    author: "Torsten Rahn"
  - subject: "Congratulations"
    date: 2005-08-05
    body: "Congratulations, Aaron!\n\nI'm really glad that you got this opportunity. And I'm also happy to see Trolltech sponsoring full-time KDE development -- not that comes as a surprise, given their well-known commitment to Open Source.\n\nI look forward to seeing the fruits of this job of yours. :-)"
    author: "Thiago Macieira"
  - subject: "Congratulations!"
    date: 2005-08-05
    body: "Congratulations Aaron!\nThis is really great news for the KDE Community.\n\nKDE4 will rock :D"
    author: "Mauricio Bahamonde"
  - subject: "Congrats Aaron!"
    date: 2005-08-05
    body: "Keep up the good work!"
    author: "mmebane"
  - subject: "Congratulations and thanks!"
    date: 2005-08-05
    body: "I just wanted to say that Aaron J. Seigo is one of the greatest assets KDE-project has, and I'm thrilled by the idea of him doing full-time KDE-developement.\n\nCongratulations Aaron! And thank you Trolltech!"
    author: "Janne"
  - subject: "Re: Congratulations and thanks!"
    date: 2005-08-05
    body: "Exactly!  A project needs people like Aaron that are not only kickass coders, but also do work to promote the great features of the project.\n\nCongratulations Aaron.  And Trolltech, you guys rock."
    author: "Leo S"
  - subject: "Re: Congratulations and thanks!"
    date: 2005-08-05
    body: "> and I'm thrilled by the idea of him doing full-time KDE-developement.\n... and frightened! Look what he did without being paid by TT, now I expect major KDE releases on a monthly basis ;-)\n\nbtw. will Aaron move to Europe now?\n\nAnd of course congrats to Aaron and thanks to TT."
    author: "aac"
  - subject: "Re: Congratulations and thanks!"
    date: 2005-08-05
    body: "> btw. will Aaron move to Europe now?\n\nNo."
    author: "Anonymous"
  - subject: "Re: Congratulations and thanks!"
    date: 2005-08-05
    body: "no, i'm staying here in Canada. there are two reasons for this, one KDE related and one not so KDE related.\n\ni have a son and his mother, whom i currently do not live with (just very near, we're 5 minutes away from each other =), lives here in Canada and is (for whatever reason) not into moving out of the country, not even to europe. it's important for my son, and therefore me, that he have both his parents around him.\n\nthe other reason is that KDE needs more representation in north america. addressing our reach in this very important market and large body of users and developers is one of my short and long term goals. to accomplish that i need to stay local.\n\ni'll still visit europe regularly though. i'm in spain at the end of this month for aKademy, and may be in the netherlands in november."
    author: "Aaron J. Seigo"
  - subject: "Congratulations Aaron!"
    date: 2005-08-05
    body: "And thank you TrollTech!"
    author: "Cartman"
  - subject: "Thanks a lot, and congratulations!!"
    date: 2005-08-05
    body: "Thanks a lot, Trolltech, and congratulations, Aaron!\n\nYou do wonderfull work at KDE. Not only the coding, but reading your blog, and receiving your feedback at the KDE lists, is always a great pleasure.\n\nAnd Trolltech, thanks a lot, really. I hope that your business model will cotinue being a success, because that means that your decisions were correct, and you will be making more great gifts like this to the comunity in the future."
    author: "suy"
  - subject: "(hidden)"
    date: 2005-08-05
    body: "<i>(See HTML source for full thread)</i></td>\n\n<!--\nStatus: Known KDE Dot News Abuser\nTitle: Of course\n\nThis is in Trolltech's interest to do this. Get as many KDE developers under you thumb so that they can spread the propaganda that the Qt license isn't bad news for KDE.\n\nKDE is a proprietary desktop environment.\n-->\n<!--\n\n\n"
    author: "David"
  - subject: "I have a question"
    date: 2005-08-05
    body: "Do we feed the trolls or do we kill them?"
    author: "Passer by"
  - subject: "Re: I have a question"
    date: 2005-08-05
    body: "Third option - ignore."
    author: "David"
  - subject: "Re: I have a question"
    date: 2005-08-05
    body: "You've been trolled by trolltech.  \"Let's see if we. If we pay off as many KDE developers as possible, then that's less of a chance for one of them to question the long-term viability of KDE with a bad QT license\"\n"
    author: "David"
  - subject: "Re: I have a question"
    date: 2005-08-05
    body: "<!--\nOk I'll bite. Just what is wrong with the gpled version of qt?\n\n-->\n</tr>\n<tr><td>\n<table>\n<tr><td>\n\n\n\n\n\n\n"
    author: "stumbles"
  - subject: "Trolltech is on the right path"
    date: 2005-08-05
    body: "Trolltech recently is doing great things for coummnity, and I belive the GPL version of Qt for windows will be a great thing for them, contrary to what some people belive.\nImagine that soon, KDE apps will start running well on windows, kdevelop is a huge sucess, konqueror takes on the firefox wave, kate is used as a replacement for notepad. Companies will look at this, and they will see that all this technology is build under the great libraries of trolltech and seriously consider using it for their cross plataform programs. Trolltech have a great marketing on Linux/Unix with KDE, but was lacking the same on win32, I belive that Qt4 will solve this and their sales will really raise.\n\nGood luck to them, keep doing great software and being nice people!"
    author: "Iuri Fiedoruk"
  - subject: "(hidden)"
    date: 2005-08-05
    body: "<i>(See HTML source for full thread)</i>\n<!--\nStatus: Known KDE Dot News Abuser\nTitle: Re: Trolltech is on the right path\n\nSorry, but KDE will always be a bit player as long as QT is straight GPL.  Wake up to reality\n-->\n<!--\n"
    author: "David"
  - subject: "Re: Trolltech is on the right path"
    date: 2005-08-05
    body: "If you dislike Qt and/or it's license, then don't use it. It really is as simple as that. No need to troll about it here, simply refuse to use it. If someone else wants to use it, that is of no concern to you. So mind your own business."
    author: "Janne"
  - subject: "Re: Trolltech is on the right path"
    date: 2005-08-05
    body: "Yes, indeed.\n\nWhy people keep complaining about Qt being GPL? I love this!\n1. Want to use it without paying for their work? Then make it open-source under GPL terms.\n2. Want to use another license? Then pay them a fee and choose wathever you want.\n\nThis simply makes more people writing GPL stuff, that IMHO is great."
    author: "Iuri Fiedoruk"
  - subject: "Re: Trolltech is on the right path"
    date: 2005-08-05
    body: "<!--\nWhy? because you have to pay for a commercial license? then wake-up yourself. To develop software in windows, most developers use visual studio. And visual studio isn't free. Free as in no cost. Windows itself isn't free of cost also. To develop software in macosx you can use the freely available apple-IDE, but you need to buy macosx (and a mac-computer for that matter) before you can develop software for it. To develop commercial qt-based software you need to aquire a trolltech-license. So what is the big difference that makes kde remain a bit player? i don't see it. \n-->\n</td></tr>\n<table>\n<tr><td>\n\n\n\n\n"
    author: "mOrPhie"
  - subject: "Re: Trolltech is on the right path"
    date: 2005-08-05
    body: "It's disappointing that this news story in particular got thread-hijacked.\n\nLet's be clear on some issues:\n\n1) The undying need for software to be free (agreeing with parent poster): Software as a product category should not be comprehensively free.  If you want to worry about sectors that should be free to improve the quality of living in some vague sense, you may want to first consider pharmaceutical companies, social work, psychiatry, ad absurdum.  Free software and software development tools? About 2,000th on the list of importance.  Focus your efforts on going to Home Depot and petitioning that all tools should be free so that Habitat for Humanity can build houses more cheaply.\n\n2) Trolltech:  They do not have a Redmond-sized campus of employees, twiddling their thumbs and waiting for a new project to join.  The act of sponsoring individuals like Aaron shows both their philanthropic nature as well as their understanding of their symbiotic relationship with OSS.  Trolltech gets it.  Their business model and success is proof.  If people didn't feel their toolset was worthwhile, they'd be a footnote of failed ventures.  They're flourishing, and rewarding KDE every step of the way.\n\n3) Aaron: Good choice by Trolltech, as he is the real deal.  I was beaten to the punch on fed exing him panties, and I don't want him to turn into the Tom Jones of the Troll tech team - so one pair will have to suffice.  I have the utmost confidence that Aaron/Trolltech/KDE users are all going to benefit from this development."
    author: "Wade"
  - subject: "Re: Trolltech is on the right path"
    date: 2005-08-05
    body: "Before the replies roll in - yes, the last line of the third point has double entendre.  Intended."
    author: "Wade"
  - subject: "Kudos Aaron"
    date: 2005-08-05
    body: "Congratulations Aaron. Good show."
    author: "Marcel Lecker"
  - subject: "Re: Kudos Aaron"
    date: 2005-08-05
    body: "Congrats Aaron! This is great news! :)"
    author: "mOrPhie"
  - subject: "Re: Kudos Aaron"
    date: 2005-08-05
    body: "Great choice Trolltech, way to go Aaron!"
    author: "Vlad C."
  - subject: "Congrats!"
    date: 2005-08-06
    body: "My congratulations to Aaron, who I know will be much happier now ;-) and my thanks to Trolltech. This is great news."
    author: "Eric Laffoon"
  - subject: "Thanks Trolls, congrats Aaron!"
    date: 2005-08-06
    body: "This is really great news. Thanks and congrats!"
    author: "Joergen Ramskov"
  - subject: "Contrats Aaron and Trolltech!!!"
    date: 2005-08-06
    body: "I'm happy for YOU and KDE 4!"
    author: "fast_rizwaan"
  - subject: "impressive"
    date: 2005-08-06
    body: "this is wonderful news. it's nice to see talented developers being sponsored."
    author: "macewan"
  - subject: "Great!"
    date: 2005-08-06
    body: "Great Aaron!! Congratulations from an avid KDE-user and promoter :)"
    author: "Wilbert"
  - subject: "Kudos"
    date: 2005-08-06
    body: "Congrats both to Aaron & Trolltech.\n\nAs long Trolltech keeps making business decisions like this (or the GPL'd Qt for Win32), they are heading straight for success. I have got to get some shares soon.\n\nMark"
    author: "Mark Szentes"
  - subject: "Re: Kudos"
    date: 2005-08-07
    body: "I don't think that they are publicely traded."
    author: "Anonymous"
---
<a href="http://www.trolltech.com">Trolltech</a>, makers of the <a  href="http://www.trolltech.com/products/qt/index.html">Qt application development framework</a> used by KDE and the 
<a href="http://www.trolltech.com/products/qtopia/index.html">Qtopia</a> application development platform for embedded Linux devices, is proud to announce their full-time sponsorship of <a href="http://www.kde.org">KDE</a> developer <a  href="http://aseigo.bddf.ca">Aaron Seigo</a>. This arrangement will enable Aaron to devote his full time and attention to KDE software projects such as <a href="http://plasma.kde.org">Plasma</a>, which aims to reshape the desktop, as well as to engage in greater Open Source community participation and support.

<!--break-->
<div style="float: right; border: grey 1px solid; margin: 6px; padding: 6px;"><img src="http://static.kdenews.org/jr/trolltech.png" width="264" height="46"></div><p>"It's amazing to be given the opportunity to concentrate on KDE, especially 
with KDE4 approaching," said Aaron Seigo, "This certainly underlines 
Trolltech's commitment to the Open Source community, and I really couldn't ask for a better group of people to have as a partner for such a project."</p>

<p>Matthias Ettrich, VP of Engineering at Trolltech and KDE's Founder, added, "KDE is more than just a showcase for Trolltech. It brings Linux to the desktop, and thus it is a major day-to-day working environment at Trolltech. Aaron is a long-term and well-known KDE contributor. He has a vision, he does great work, and he can become a figure that leads the KDE desktop experience to a higher level. I'm happy that we can help him work on KDE full-time."</p>

<h3>About Plasma </h3>
<p>Since the introduction of the Mac in 1984, mainstream environments have treated the desktop as a file manager view. The user is allowed to put icons there but it's pretty much a world of its own separate from the rest of the desktop, and is often obscured by windows. </p>

<p>The goal and mandate of Plasma: to take the desktop as we know it and make it relevant again. Breathtaking beauty, workflow driven design and fresh ideas are key ingredients. "It is time that the desktop caught up with modern computing practices and once again made our lives easier and more interesting when sitting in front of the computer," says Seigo.</p>

<h3>About Aaron J. Seigo</h3>
<p>Aaron J. Seigo began developing software for the UNIX platform in 1991 and 
eventually found his way into the Open Source world via Linux. With 
additional experience in business management, event coordination and project 
management, Aaron joined the KDE project in 2001 as a software developer but 
quickly found areas to apply his non-technical skills within the project. 
Today Aaron is a KDE core developer and community leader who works full time 
on KDE and KDE-related endeavors.</p>








