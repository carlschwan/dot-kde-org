---
title: "DistroWatch Weekly: Tips and Tricks with Konqueror and Kate"
date:    2005-08-09
authors:
  - "jpryor"
slug:    distrowatch-weekly-tips-and-tricks-konqueror-and-kate
comments:
  - subject: "TIP: Hide Konqueror sidebar (buttons)"
    date: 2005-08-09
    body: "Could you please tell me how to hide the sidebar buttons, they really look \"awful,\" I prefer Metabar alone and no other sidebar button. I remember something has to be added in konquerorsidebartngrc or something like that. TIA.\n\n"
    author: "fast_rizwaan"
  - subject: "Re: TIP: Hide Konqueror sidebar (buttons)"
    date: 2005-08-09
    body: "You can just rightclick on each undesired sidebar button and choose \"Remove\". If that is what you meant."
    author: "Bram Schoenmakers"
  - subject: "Re: TIP: Hide Konqueror sidebar (buttons)"
    date: 2005-08-12
    body: "yes"
    author: "yes"
  - subject: "Re: TIP: Hide Konqueror sidebar (buttons)"
    date: 2005-08-11
    body: "\u00bfF9?"
    author: "asharrot"
  - subject: "Konqueror as Universal Viewer"
    date: 2005-08-09
    body: "I love konqueror for it's universal document view/preview capability. May it be music, video, text, pictures, or documents (pdf, ps, rtf, odt, etc).\n\nIf we improve konqueror's Music and Video preview support with a *sidebar playlist* then who'd need a dedicated music player like xmms, juk.\n\nKonqueror really rocks!!! thanks konqi and kdelibs developers for wonderful kparts!"
    author: "fast_rizwaan"
  - subject: "Re: Konqueror as Universal Viewer"
    date: 2005-08-09
    body: "amarok provides a sidebar playlist for konqi\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Konqueror as Universal Viewer"
    date: 2005-08-15
    body: "It sounds as though you just use konq, not kde itself?\nIf I am correct on this please explain how you were able to do this\nthanks."
    author: "Rev"
  - subject: "mc"
    date: 2005-08-09
    body: "Being a \"die-hard\" KDE user (since pre-1.0),and in particular \na die-hard konqueror user, I'm still not convinced...\n\nThe ultimate tool for me is still Midnight Commander + Konsole.\nMC does:\n*)remote access via ftp and ssh\n*)opens tar.gz, zip, unpacks RPMs, etc in place\n*)deleted file recovery on ext2\n*)a flexible editor with user-defined syntax-highlightling\n*)easily extensible with any file-viewer you want\n*)excellent integration of panels with commandline \n(I mean CTRL-ENTER and CTRL-X,P and hiding the panels using \"ESC\" to\nshow the output of the commands I executed).\n\nAll in one, with memory-footprint of some 500kB...\n\nPlus my favourite konsole features: add new session, \nsend output to all sessions, switch to previous/next session...\n\nAnd no, customizing Krusader key-bindings is far from approaching it...\nTyping CTRL-ALT-N (new konsole window) and typing \"mc\" is probably\none of the most used key combinations on my PCs... and it's ready in less than half-a-second... And it works great over ssh over a modem line.\n\nI would really appreciate deeper integration of konq/kioslaves with command-line. There's a lot of commandline behind system administration anyway... Simple embedding of konsole and browsers in the same window\nis not enough..."
    author: "piters"
  - subject: "Re: mc"
    date: 2005-08-09
    body: "You can do a lot of KIO slave work from commandline using kioexec and kfmclient\n"
    author: "Kevin Kramme"
  - subject: " KIO slaves on the cmdline"
    date: 2005-08-09
    body: "I could probably google, but this is easier :).  \n\nHow do you use KIO slaves on the cmd line?  For example, I love smb:// because I can't scp to my linux box from windows.  How can I do a cmdline cp smb://server/path/file /path?\n\nI looked at kioexec --help and got nothin'.\n\nthanks"
    author: "TomM"
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-09
    body: "kfmclient copy URL destination\n"
    author: "Kevin Krammer"
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-10
    body: "FWIW, the name kfmclient is depreciated... kioexec is the exact same app, and is preferred.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-10
    body: "Are you sure about that?\n\nHow does\nkfmclient exec http://www.kde.org/\nmap to kioexec?\n\n"
    author: "Kevin Krammer"
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-10
    body: "Heh.  I popped back to post a correction and you had already checked.\n\nYup, they are different.  For some reason (maybe somebody had suggested it at one point), I thought they were identical and merely linked.  I do note that kfmclient is awfully small.\n\nGreat... now I have a couple hours of poking around ahead of me to play with them and read through the source code.  :) "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-10
    body: "A quick way to figure out the cmd line version of a konq copy operation is to drag a remote file from konq to konsole, which pops up a dialog asking among other things if you want to copy the file to the working directory.  If you choose copy, then the appropriate command is executed in that terminal session.  Of course, at this point the file is already there ;-) but it's still helpful for the 'reverse' case, i.e. copying the file back.\n Drag and drop *into* terminal sessions is for me one of those \"KDE just got it right\" sort of things :-)\n\n-chris"
    author: "ChrisK"
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-10
    body: "\u00dcbercool!\n\nSomehow I never thought about dragging to Konsole, I even had problems figuring out how to create script commands from kdcop (again DnD to Konsole)\n\nDrag&Drop functionality is something really hidden. If you don't try it you never find out :(\n\nP.S.: kfmclient copy is one of the examples in my KDE scripting presentation:\nhttp://www.sbox.tugraz.at/home/v/voyager/scriptingkde/"
    author: "Kevin Krammer"
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-10
    body: "Of course you can scp into linux from windows!\n\nhttp://www.chiark.greenend.org.uk/~sgtatham/putty/download.html"
    author: "Roberto Alsina"
  - subject: "Re:  KIO slaves on the cmdline"
    date: 2005-08-11
    body: "Oh, I know its possible, but with the way my network is set up, my windows box can't get to my linux box.  Something wierd about the vpn router the linux box is on."
    author: "TomM"
  - subject: "Re: mc"
    date: 2005-08-09
    body: "Other than deleted file recovery (as opposed to trash, which is the way Konqueror handles it), I see nothing in your bulleted list that Konqueror plus Kate can't do.  \n\nPerhaps you're just used to mc, and are trying to make Konqueror to work like it - a recipe for frustration.  Try to approach a new app with an open mind and figure out how to use it in the manner it was designed. \n\nAs for your last point, there are a slew of commandline tools like kfile and kioexec, plus commandline apps like konsolekalendar as well as the dcop app and DCOP key bindings that allow integration in any manner you wish.  I have...\n\ngrep dcop ~/usr/bin/* |wc -l\n341\n\nOver 300 calls to dcop in my personal tools, almost all of which are bash scripts.  Some are bound to keys, but most are commandline tools.  There are a handful of bash functions like klip ( http://www.linuxgazette.com/node/9047 ) in my .bashrc as well.\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Konqueror is so slow"
    date: 2005-08-09
    body: "I use mandrake/mandriva. No different what version and computer I use, konqueror is so slow, when webpage uses flash animation. FF and flash is ok. I hope, this is mandriva problem.. but how can i fix this system ?"
    author: "Kris"
  - subject: "try another distro"
    date: 2005-08-09
    body: "a very user-friendly like Linspire (http://www.linspire.com) or Xandros!"
    author: "fast_rizwaan"
  - subject: "Re: try another distro"
    date: 2005-08-09
    body: "I dont want install new distro. I want only know why mandriva and konqueror work slowly (pages where is flash) and konqueror take all power from cpu !?!If from other distros KDE & konqueror works fine webpages where is flash, then I think that mandriva team need fix something.\n"
    author: "Kris"
  - subject: "Re: try another distro"
    date: 2005-08-09
    body: "the problem is with flash, the linux plugins for it just suck, afaik. i don't even have them installed, they make it all too slow. hope gplflash, which seems to be alive again, fixes this."
    author: "superstoned"
  - subject: "Re: try another distro"
    date: 2005-08-09
    body: "No. It is a Mandrake problem. I hope when they release Mandriva, a cooperative work of Mandrake and Conectiva and Xandros they can learn how to deliver a proper konqueror. I will say some of the problems with Mandriva, spread the news please:\n\n- The use of cache is disabled (!!!!). I cant figure out why they did this. In my conectiva, it was never like this. Enable it.\n\n- The use of javascript and java are disabled (!!!!). No comments. Easy Distribution ?\n\n- it happened in my Mandriva, cant say if it was only my problem, but the cursor changes when i pass a link, and it doesnt appear a hand, it appears that bar that should appear in line edits. I really believe this is some strange error in my instalation only, but please, verify.\n\n- the use of plugins were disabled (!)\n\n- and at last, who is the artist that works there? it is all terribly ugly. i like carnival in brasil, but not in my computer.\n\nI hope they produce a better product in the future first Mandriva release. this one thats out there is a monster, made in a hurry. it is not representative of Mandrake."
    author: "Henrique Marks"
  - subject: "Re: try another distro"
    date: 2005-08-10
    body: "Am I also the only one who notices that KControl uses a *way old* outdated layout and is *never* updated to get the new KDE improvements?  I still see ugly entries like \"LookNFeel\" in Mandriva."
    author: "Anonymous"
  - subject: "Re: try another distro"
    date: 2005-08-10
    body: "But whay FireFox and flash works oko, but konqueror and flash don't ?"
    author: "Kris"
  - subject: "Re: try another distro"
    date: 2005-08-10
    body: "it might be the combination, i dunno... as i said, i don't use it anymore, it was just problems problems problems. with firefox, too, here."
    author: "superstoned"
  - subject: "Re: Konqueror is so slow"
    date: 2007-08-25
    body: "Hi all,\ni use debian 4.0 with firefox and konqueror on 386mb of RAM. Firefox goes to hell.. it always crashes after 1 or 1.5 hr using my 150mb or more of my RAM so i used konqueror in gnome. I found really shocked when i was not able to get my gmail (might be google's policy). The performance was extra, really much faster than firefox. Now a day's it is not. konqueror is very much slow. firefox loads much faster than konqueror. when watching youtube, firefox streams completely in 15 sec but konqueror displays video in scratch mode. I'm really sick of these. Wish to get solution for this problem."
    author: "ujwal"
---
The current <a href="http://distrowatch.com/weekly.php?issue=20050808">DistroWatch Weekly</a> is guest-edited by Adam Doxtater, of <a href="http://www.madpenguin.org">Mad Penguin</a>. His <a href="http://distrowatch.com/weekly.php?issue=20050808&mode=1#tips">Tips and tricks</a> section takes a look at Konqueror and Kate and lets us know that <em>"Konqueror and Kate make an excellent remote admin team... and a killer casserole"</em>.  He covers Konqueror's split view and the power of kioslaves to edit directly from a remote server.



<!--break-->
