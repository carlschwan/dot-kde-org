---
title: "Scribus Team Releases 1.3.0 \"La Libert\u00e9\""
date:    2005-07-18
authors:
  - "mrdocs"
slug:    scribus-team-releases-130-la-liberté
comments:
  - subject: "Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Nah, not that again. ;)\nBut I'd like to repeat what went unaswered last time: Is the KDE integration effort outlined or coordinated anywhere? It's omitted in this article and barely mentioned in the roadmap for 1.3.x, and I don't know what investigations were actually made and what the results are."
    author: "ac"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "It seems there is a bug in their database :\nhttp://bugs.scribus.net/view.php?id=14\nAnd I think scribus already uses kdeprint when available."
    author: "Cyrille Berger"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "\"I think scribus already uses kdeprint when available.\"\n-----\nHeh... You can *bet* that KDEPrint is available on all my boxen. But Scribus (1.2cvs and 1.3cvs) do not use it -- it calls its own print dialog.\n\nAnother nasty thing, I find particularly annoying: Scribus uses the very simple Qt-only \"FileOpen\" and \"SaveAs\" dialogs. No comfort for me any more, if I use Scribus, no bookmarks, no KIO slaves (http://, ftp://, fish://, smb://, webdav:// or webdavs://) for opening remote documents seamlessly and transparently saving back to that place...\n\nProbably we should just do an extended demo of KDE's file dialogs to the Scribus developers  ;-)   -- once they can feel its value, they will be auto-motivated to take advantage of that cool features. (I do not think that it can be too complicate and too much work to make Scribus aware of \"I am no running inside a KDE session -- great, let's use the KDE print and file dialogs\". Also, the actual call to these dialogs shouldn't be too complicated either. After all, even some GTK applications have been proofed to be able to use them in the past...).\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "Having a Qt-only application of my own, I can say it's a pain in the butt to make a Qt-only program \"aware\" of KDE. You end up with ugly #ifdefs everywhere making your code unreadable, or you end up with adaptor classes for all your dialogs, adding to the \"bloat\"."
    author: "Brandybuck"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Yes, Scibus feels alien on KDE because it lacks all those nice integration efforts. It looks ugly.\n\nThe question is: Will scribus be able to integrate or do we have to fork.\n\nIntegration is a huge productivity boost."
    author: "Gerd"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "I'd guess the work with making Scribus intergrate with KDE does have lower priority than some of the other things the Scribus team are working on. Making it more or less a manpower issue. So rather than forking, start sending them patches and work with the team. New contributors are always welcome in Open Source projects, and I'd guess the Scribus team are no different.\n\nWhen on the issue of dual Qt and KDE interfaces, does someone know some nice examples of how this may be achieved in a nice clean manner? There are a few Qt applications where some KDE intergration should have been nice. I only know of Eric3, but it's developed in Python. And doing the Qt/KDE thing in Python looks much easier to do than C++, and it's done at run-time."
    author: "Morty"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Yes, to an extent, it is a manpower issue. Of course, we would certainly welcome someone with  good KDE foo to work with us.\n\nLet me say also, that we are *very* appreciative of the support we have gotten from KDE community, folks like Kurt Pfieffel and Fabrice Mous to name two, as well as the wider community. It is recognized and appreciated.:)"
    author: "mrdocs"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "If it's a manpower issue why doesn't my question (the above \"Is the KDE integration effort outlined or coordinated anywhere?\") get answered to easen the entry for new people able and willing working on KDE? Should they (we) rather fork it to make a proof of concept? You know, nothing is more offputting then core people answering first that investigations were made (with what results) and later that manpower is the issue, with nothing of that traceable from the outside... =/"
    author: "ac"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Not even enough time/manpower to properly outline a doc which would answer your question..No joke.\n\nThe short answer is some parts of Scribus need major refactoring, which is what 1.3.x is about. When we get those bits done, then integrating KDE is much more feasible."
    author: "mrdocs"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "The effort was admittedly a quite small effort which was surely not deep enough. Scribus is about pagel layout, we have concentrated 1.3.0 on layout features and major cleaning and rework. If (as I see in other posts) there are examples, or if someone is willing to make a simple one, we will look very quickly at this and implement this as soon as possible.\n\nOnce I'm back from holidays, if someone hasn't already looked at whatever is submitted or perhaps into the Rekall sources or the other URL, then I will myself. BTW, if someone is interested, we do need to be able to customise the dialog, but I'm sure the KDE file dialog has this ability too (eg adding compression and other checkboxes). Please feel free to email me directly, or submit patches to bugs.scribus.net preferably as others can look at it then too."
    author: "Craig Bradney"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "are any of the scribus devels going to be at aKademy this year?"
    author: "Aaron J. Seigo"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "Doubtful, to be honest :S"
    author: "mrdocs"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "Pity....\n\nThere is no better opportunity for a whole year to get stuff done, and get it done fast."
    author: "Kurt Pfeifle"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "Would be useful to have more meetings of that kind. Usually it is good to use existing bigger events in order to meet there. KDE devel meeting at 22C3 or whatever"
    author: "Hein"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "Indeed! I had looked forward to meeting Scribus developers and compare notes on color management and other topics, like better ways of calling Krita from Scribus."
    author: "Boudewijn Rempt"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "Likewise.. At least all of us, save one, now live in Euroland. So, it won't be an impossibility.... It is just the timing for aKademy is not good for us :("
    author: "mrdocs"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-20
    body: "Fosdem? 22C3? OOocon05?"
    author: "Aranea"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "[quote]\nWhen on the issue of dual Qt and KDE interfaces, does someone know some nice examples of how this may be achieved in a nice clean manner?\n[/quote]\n\nRekall (www.rekallrevealed.org) builds as a KDE application (which it started life as, contrary to the comment on another similar application's web site:), and as a QT application on Linux, Windows and Mac OS/X.\n\nWhether this is a \"nice\" example I leave others to judge, but the strategy is to keep #ifdef's to an *absolute* minimum, to split functionality between pairs of files (one kde-based and one qt-based), and to use wrapper classes where needed. Before anyone shouts, wrapper classes are a pretty minimal overhead in this case; most of the time its trivial (for example, to get KComboBox in KDE and QComboBox in QT-only). Where its more complicated, say file dialogs, the computer is still a lot faster than the user!"
    author: "mikeTA"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Talking about \"forking\" is absolutely non-sensical.\n\nOf course, forking is technically and legally possible at any time. But it should only be seriously considered if there is a substantial advantage to gain. And it is only an option, if the developers of the original code base are uninterested in the features the suggested \"fork\" wants to push, or for some other reasons are impossible to work with. Talk about a fork only after you have tried -- not once, not twice, not thrice... but at least 10 times -- to cooperate with the original developers!\n\nI know for sure that the Scribus folks are very friendly and cooperative people.\n\nSo where is the first patch you submitted to them? Where is their rejection mail?\n\nCheers,\nKurt\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "You don't need to fork! I got a patch accepted into Scribus yesterday :-). Now the new file dialog has the same tab order as the new file dialog of Qt Desiger or KOffice. See -- it's as easy as that!"
    author: "Boudewijn Rempt"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "We have already made one attempt to build Scribus with native integration. However, it requires more than just linking to some kdelibs. ;)\n\nThere is a lot of under the hood work which needs to be done in string handling and other details. Moreover, we must maintain the ability to build this optionally to support other non-*nix platforms.\n\n\"The question is: Will scribus be able to integrate or do we have to fork?\"\n\nWe will eventually, but understand for a complex application like Scribus it is not trivial. You are welcome to fork, the code is GPL, but understand as of 1.3.x a huge pile of new stuff coming into the devel series. It will be a challenge to keep up. \n\n\"Integration is a huge productivity boost.\"\n\nAgreed. All of us are drooling to have kio support and kde files dialogs, among other benefits.\n\nAnother posted mentioning being only Qt \"It's ugly\".\n\nWell, Scribus will pickup KDE themes and fonts if your Qt is configured correctly. I'd note the screenshots in the docs use a modifed Plastik theme. This has worked since Qt2 days and mentioned in the docs. If Qt does not inherit the style plug-ins from KDE, then is a qtconfig issue or Qt is not packaged to enable this by your distro. I know it works on Suse, RH/Fedora and Gentoo among others. \n"
    author: "mrdocs"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Fork'n'integration for koffice2/kde4? :-D"
    author: "Nobody"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "> All of us are drooling to have kio support\n\nKIO support can be achieved quite easily if you don't need your own QApplication subclass and likely not very hard if you need one.\n\nFor the first case, i.e. not needing a QApplication subclass, I have a working implementation to make Qt use KIO slaves through its own QNetworkProtocol API, which means remote and local access can be handled through QUrlOperator.\n\nIn case you are interested:\nhttp://www.sbox.tugraz.at/home/v/voyager/qds/"
    author: "Kevin Krammer"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-18
    body: "Cool! Please announce it more publically here on dotty or on planekde!"
    author: "ac"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "I blogged about it when I first created it:\nhttp://www.kdedevelopers.org/node/548\n\nHaven't had much time to work on it since than :(\n\nThe main problem beside the KApplication thing is that most Qt-only programs don't even use Qt's own possibilities for remote file access and only operate on local files.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-19
    body: "well, my 2c... I would *love* to have KDE integration but there are some areas with higher priority now. Scribus is DTP application so we're focusing to improve this area now. Future (I'm hoping so!) KDE integration will be something like cherry in the pie.\nbtw #ifdefs everywhere frighten me more than threading in applications ;)))"
    author: "subik"
  - subject: "Scribus and Quark Xpress"
    date: 2005-07-18
    body: "I'd like to present Scribus to some friends, they use Quark Xpress, I want to know what are the differences between Scribus and Quark.\nThanks"
    author: "Althazur"
  - subject: "Re: Scribus and Quark Xpress"
    date: 2005-07-18
    body: "Ready for a book chapter ;)\n\n1) The PDF Export in Scribus is hands down easier to use and generates superb PDF without lots of fuss, nor the need to run it through distiller. There are vastly fewer possibilities to screw it up on Scribus, and with 1.3.0 and the pre-flight tool almost impossible, unless you willing ignore it :)\n\n2) The basic use of frames is similar, except Scribus has more extensive right-click context menus. I *highly* recommend a two-button+wheel mouse, even on OSX.\n\n3) The tool kit and widgets are different, which brings its own set of differences and usage habits. Qt on Mac hides much of this though. \n\n4) At the moment, Scribus very well with Fink and excellent on OSX with the aqua build, but the core of Scribus was developed with Linux/Unix in mind and should be compared only when using it in its native environment. As progress is made with the aqua build this will no longer be an issue, but testing *right now* should keep this in mind.\n\n5) The image handling in Scribus is much nicer IMO, TIFF handling and PDF import are much easier, plus in 1.3.0 PSD is natively supported including some really nice tricks for working with clipping paths in TIFF/PSD. More to come there..\n\nThat said, a real page layout pro with some experience beyond just X-Press, should be able to sit down and grok it pretty quickly. I am thinking of someone who also has used Pagemaker, Ventura, Framemaker or InDesign in some mix.\n\nLastly, support. \n\nWe win, I am sure:) We have a very friendly community both on IRC and mailing lists.. The developers *do* listen. :) When did your friend last directly talk to a Quark developer? ;-)\n\nEvery single bug report or feature request is looked at byt at least one team member usually within 24 hours. Bug reports usually even quicker :) Fixes come as quickly as possible. Easy ones sometimes in 24-48 hours. On top of that, there is commercial support for those needing it.\n"
    author: "mrdocs"
  - subject: "Re: Scribus and Quark Xpress"
    date: 2005-07-18
    body: ">We win, I am sure:) We have a very friendly community both on IRC and mailing lists.. The developers *do* listen. :) When did your friend last directly talk to a Quark developer? ;-)\n\nAnd I trylly belive this is one of the strenghts of open-source. Most developers are just nice people, and we thanks them for their efforts into creating nice tools like Scribus.\nCongratulations for this nice release and your great work."
    author: "Iuri Fiedoruk"
  - subject: "Re: Scribus and Quark Xpress"
    date: 2005-07-18
    body: "What does quark what scribus does not?\n\nis the better question.\n\nThe main lack of Scribus is some frefab content. When I try a new program I want to play around with it. "
    author: "Martin"
  - subject: "Re: Scribus and Quark Xpress"
    date: 2005-07-18
    body: "Pre-fab content? <a href=\"http://www.scribus.org.uk/modules.php?op=modload&name=Downloads&file=index&req=viewdownload&cid=5\">Start here</a>\n\nand <a href=\"http://www.scribus.org.uk/modules.php?op=modload&name=Downloads&file=index&req=getit&lid=87\">here</a>\n<p>\nThat should help :)\n\n\n\n"
    author: "mrdocs"
  - subject: "DTP vs Typesetting"
    date: 2005-07-18
    body: "My impression (after toying) is that Scribus was built for DTP (Quark replacement) but not for typesetting (Tex, FrameMaker replacement). Though  Tex is excellent in certain scenarios there is plenty of room for further Typesetting products. Are there any plans to make Scribus usable for long structured documents, novels, master thesis and the like? \n"
    author: "Roland Wolf"
  - subject: "Re: DTP vs Typesetting"
    date: 2005-07-18
    body: "Since DTP and Typesetting are at the different ends of the scale in the way you work when doing documents, trying to adapt one to the other way of work are rather useless. It's better to use the right tool for the job. If you are doing DTP, use Scribus or Quark(Even for longer documents, but use it like a DTP tool). If you need a Typesetting products use a tool that's made for that kind of work, like Tex/LaTEX or their flashy sister LyX, FrameMaker or KWord in frames mode. It's not a accident that two so different paradigms have evolved for making documents, it's mostly about workpattern and workflow. Trying to blend those two ways to work ends up in some kind of bastard application, not really suited for either forms of work, like MS-Word. \n\nTo sum it all up, if you need a tool for DTP use the most excellent Scribus. Or if you have need for a tool writing your novels or master thesis use the likevise superb LyX. OSS covers both bases with great tools:-)  "
    author: "Morty"
  - subject: "Re: DTP vs Typesetting"
    date: 2005-07-19
    body: "sure, but for usability KDE integration is a must."
    author: "jamie"
  - subject: "Re: DTP vs Typesetting"
    date: 2005-07-19
    body: "Why? Intergration is nice and gives you added benefits like with the fileselector and it's IO-slaves, and the familiarity of the print dialog. And folowing the KDE menu structure are also nice, but don't bring any huge usability benefit either. And both Scribus and LyX are Qt application making them use the same widget style as KDE (if you don't have a really broken install) making the visual appearance similar. \n\nOther than for people having a work pattern making heavy use of IO-slaves, I can't see why KDE integration gives any big usability gain to those applications. Surly not enough to make it a must, but I may be mistaken so pleas enlighten me?"
    author: "Morty"
  - subject: "Re: DTP vs Typesetting"
    date: 2005-12-14
    body: "I have a problem with KDE integration. and it is simply this:\nI dont want to be forced to install a ton of KDE stuff just to run scribus. I actually run and rather like CDE. also for cross platform capabilities Its easier not to depend on anythign in KDE at all. What if i want to run scribus on my IRIX 6.5.11 box? I'm not even sure KDE compiles/runs on that. but im pretty sure Qt does. all i want is a free page layout system that I can run easily as a standalone UNIX app without having to install stuff I will never use. If it is integrated, I would really like it to be optional some how."
    author: "William Schaub"
  - subject: "Re: DTP vs Typesetting"
    date: 2006-06-05
    body: "Isn't that what configure scripts are for? \n\nIt would be very nice, I think, to have full KDE integration there, but it would certainly suck hard if that were done in such a way that it couldn't be compiled without it, yes. "
    author: "Arker"
---
The <a href="http://www.scribus.org.uk/">Scribus Team</a> is <a href="http://www.scribus.org.uk/modules.php?op=modload&name=News&file=article&sid=100">pleased to announce</a> a <i>technology preview</i> of the next generation of Scribus Open Source Desktop Publishing.  The aim for this release is to bring improvements and enhancements for both professionals and beginner users alike. 1.3.0 "La Liberté" improves Scribus' abilities in areas like performance, accessibility and workflow. With this release we also commence support for Scribus running natively on Windows and Mac OS X.  Download Scribus from <a href="http://www.scribus.org.uk/modules.php?op=modload&name=Downloads&file=index&req=viewdownload&cid=18"> our 1.3.0 Downloads page</a> or view some <a href="http://scribus.sourceforge.net/gallery/">screenshots</a> of the various versions.











<!--break-->
<p>While it is very stable and usable, there are a number of under the hood changes which will continue through the 1.3.x series</p>

<ul>
<li> Work is on-going to make Scribus more platform independent and alpha quality builds of Scribus are already working on Win32 and Mac OS X. A native Mac OS X build is available from <a href="http://aqua.scribus.net">aqua.scribus.net</a>
<li>There is a new undo system, which includes an undo palette that has the undo history with details of each action. Undo also has object specific capabilities. The number of undo/redo steps can be set in the preferences.</li>
<li>Revised document and application preferences. Extensive attention has been placed on making preferences easier to use with document and application preferences distinctly separated.</li>
<li>Scribus now has "scratch space" which can be used to store objects partially or fully off the page.</li>
<li>Scribus can now generate Tables of Contents, based on user definable/selectable attributes.</li>
<li>Scribus now has its own built-in pre-flight verifier for printing or exporting to PDF. The pre-flighter is designed to look for issues which might cause problems when printing or exporting PDF.</li>
<li>More professional DTP features like adding ciceros for measurement units and support. Scribus now has true facing page support and page objects such as images can span them.</li>
<li>Scribus now has more extensive support for CMYK images, as well as clipping paths in TIFF files, CMYK-JPEG, as well as new support for native Photoshop PSD files. 16-bit RGB/CMYK TIFF and PSD support will follow soon. ICC color management of for these image formats is also upgraded.</li>
<li>New image effect features including tinting, greyscaling, blurring, coloring and sharpening.</li>
<li>Support for PDF 1.5 features including layers in PDF.</li>
<li>Scribus 1.3.0 uses an intermediary file format based on the Scribus 1.2.x format. The new format to finally replace this intermediary format is in the design and development stages and its implementation will begin soon.</li>
</ul>

<p>For future development versions, we will be announcing competitions for icon sets and splashscreens.</p>

<p>The Scribus Team would also like to thank Anduin.net/Øverby Consulting and n@work GmbH, Hamburg for gracefully sponsoring our Scribus Documentation site, our Bug Tracker and Anonymous CVS server. Also, a note of thanks to The University of Salford's School of Media, Music and Performance for providing continued hosting of www.scribus.net and upgrading the server hardware.
</p>










