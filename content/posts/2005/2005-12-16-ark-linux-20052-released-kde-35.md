---
title: "Ark Linux 2005.2 Released with KDE 3.5"
date:    2005-12-16
authors:
  - "brosenkraenzer"
slug:    ark-linux-20052-released-kde-35
comments:
  - subject: "nice compliment to krita"
    date: 2005-12-16
    body: "seen on Planet GNOME:\nhttp://blogs.gnome.org/view/bolsh/2005/12/15/1"
    author: "ac"
  - subject: "Re: nice compliment to krita"
    date: 2005-12-16
    body: "So he needs a new computer. Anyone who has a free machine for more developer productivity?\n\nI must be made sure that productive people are fully supported."
    author: "Andr\u00e9"
  - subject: "OT: Status updates"
    date: 2005-12-16
    body: "What about the since 2004-running port of Firefoy to Qt, will it ever be completed...? IIRC there were problems for getting cvs-write accounts for some kde devs to the mozilla sources... Is that still an issue? If mozilla does not want us to improve their code maybe this issue should be brought to a greater public...\n\nAnd when will Zack continue working on the x.org EXA acceleration? The beginning of EXA was so promising, but it seems now there is no more work done on it and xorg 7.0 will come out with an experimental EXA , non-working for many chips.\nKDE4.0 will need stuff like EXA..."
    author: "ac"
  - subject: "Re: OT: Status updates"
    date: 2005-12-17
    body: "The Firefox Qt port is in CVS, but doesn't work very reliably yet (we're watching, and switching the Ark Firefox package over to Qt as soon as it stops crashing frequently -- unfortunately at the moment it requires a series of patches to even compile [but we have those]).\n\nEXA is nice where it works - Ark defaults to using EXA on ATI Radeon chips where it's known to work well."
    author: "Bernhard Rosenkraenzer"
  - subject: "Re: OT: Status updates"
    date: 2005-12-17
    body: "The Firefox Qt port is in CVS, but doesn't work very reliably yet (we're watching, and switching the Ark Firefox package over to Qt as soon as it stops crashing frequently -- unfortunately at the moment it requires a series of patches to even compile [but we have those]).\n\n\nwhy are these patches not in cvs? Is it so hard to contribute so mozilla?"
    author: "ac"
  - subject: "Re: OT: Status updates"
    date: 2005-12-17
    body: "> What about the since 2004-running port of Firefoy to Qt, will it ever be completed...?\n\nYes, distributions will ship it next year."
    author: "Anonymous"
  - subject: "Re: OT: Status updates"
    date: 2008-03-26
    body: "Dude, it's been 3 years..."
    author: "scorpion_9"
---
Following the release of KDE 3.5, the <a href="http://www.arklinux.org">Ark Linux</a> team is pleased to announce the release of <a href="http://www.arklinux.org/article.php?story=20051209192557743">Ark Linux 2005.2</a>.  Ark Linux is a Linux distribution designed especially for desktop use. Based on KDE it is easy to use even for people without prior Linux or any computing experience.  As well as the latest KDE 3.5, this release includes OpenOffice.org <a href="http://kde.openoffice.org/">fully integrated</a> with KDE and the <A href="http://www.koffice.org/krita/">Krita</a> image manipulation program.  Ark Linux 2005.2 can be <a href="http://www.arklinux.org/staticpages/index.php?page=downloads">downloaded</a> free of charge.





<!--break-->
