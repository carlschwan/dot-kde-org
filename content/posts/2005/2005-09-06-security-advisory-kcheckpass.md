---
title: "Security: Advisory for kcheckpass"
date:    2005-09-06
authors:
  - "binner"
slug:    security-advisory-kcheckpass
comments:
  - subject: "Good work Ilja!"
    date: 2005-09-06
    body: "I actually know the guy that found the vulnerability. Great that this gets relayed to the kde team. Many eyes make bugs shallow :)"
    author: "Coolvibe"
  - subject: "Re: Good work Ilja!"
    date: 2005-09-06
    body: "from netric?"
    author: "me"
  - subject: "security is as security does"
    date: 2005-09-06
    body: "I think security is as security does"
    author: "kde user"
---
The <a href="mailto:security@kde.org">KDE security team</a> has published a <a href="http://www.kde.org/info/security/advisory-20050905-1.txt">security advisory</a> concerning a local root vulnerability: kcheckpass can, in some configurations, be used to gain root access. All KDE releases starting from KDE 3.2.0 up to including KDE 3.4.2 are affected.

<!--break-->
