---
title: "KDE-Artists.org: Plasma Button Art Contest"
date:    2005-07-15
authors:
  - "aseigo"
slug:    kde-artistsorg-plasma-button-art-contest
comments:
  - subject: "Suggested Eyecandy and Substantive Changes"
    date: 2005-07-15
    body: "Here are some ideas. Eyecandy:\n\n*  Etched Glass Panel - the background would be translucent and fuzzy, similar to the window borders in this screenshot: http://www.gnome-look.org/content/pre1/23232-1.png\n(This etched glass feel is already being worked on for KDE window borders: http://bugs.kde.org/show_bug.cgi?id=108194)\n\n*  MouseOver Effects for _background_ of Buttons - smooth/gradual increase in opacity of the area surrounding the icons when the user moves the mouse over the icon. This would be analogous to the Plastik theme\u0092s way of smoothly changing the background color of buttons in the upper-right corner. (I think the current behavior of changing the brightness of icons upon MouseOver makes them harder to see.)\n\n*  Sliding Menus \u0096 Menus would slide up from underneath the panel, and the panel would cast a shadow onto the bottom of the menus to enforce the overlaying effect. \n\n*  Etched Glass Menus \u0096 for aesthetic consistency, menu background would also be translucent and fuzzy.\n\n*  MouseOver Effects for _background_ of Menu Items \u0096 The background of menu items would also gradually become more opaque when the mouse is over them.\n\n*  Menu items (including text and icons) would be 2-3X larger than currently (see http://www.kde-look.org/content/pre2/26424-2.png for mockup). This will make text easier to read and allow icons to be seen in their full glory! Check out the \u0093Substantive Changes\u0094 section below for an explanation of how these larger menu items can be arranged to fit on the screen.\n\nSubstantive Changes:\n\n*  Split up Kmenu \u0096 Instead of the \u0093Start\u0094-like Kmenu, have multiple menu icons directly accessible from the panel: Internet, Office, Music&Movies, Pictures, Computer (note that I stayed away from using Multimedia, Graphics, and System, since I find those terms to be technical and somewhat geekish).\n\n*  Submenus \u0096 Each menu should have submenus that are stacked on top of one another and visible at the same time. Each submenu would have a header (similar to the \u0093All Applications\u0094 header in current Kmenu) and the items it contains listed below the header. For example, the Internet menu would have these submenus: Web, Email, Chat.\n\n*  Collapsible Submenus \u0096 If a menu gets too large to vertically fit on the screen, the user should be able to collapse one or more submenus. Once a submenu is collapsed, only its header will appear, the items appearing to the right of the header once it is clicked (this is the current behavior for submenus).\n\n*  Description of Items on MouseOver \u0096 The items should normally have only the name of the program (in relatively big font) written in the text area. However, when the user moves the mouse over the item, the text area should be split vertically into two parts: the top part will still contain the name of the application, but the bottom part will contain a short description. This will do away with some yellow information pop-ups."
    author: "Vlad C"
  - subject: "Re: Suggested Eyecandy and Substantive Changes"
    date: 2005-07-15
    body: "http://www.kde-artists.org\n\nI seriously suggest you go to the forum there and start posting.  Some mock-ups would rock."
    author: "Saem"
  - subject: "Re: Suggested Eyecandy and Substantive Changes"
    date: 2005-07-15
    body: "for this specific contest, we're looking just for icon buttons so the first two things you mentioned would be in the ballpark.\n\nfor the other items, there are discussion boards at kde-artists.org where those would be appropriate.\n\nand yes, actual artwork is what is needed. it's easy to dream of ideas but to actually see them in front of you in pixels is the trick ;)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Suggested Eyecandy and Substantive Changes"
    date: 2005-07-16
    body: "I'm creating some screenshots to make my ideas more concrete, but it might take me some time since I'm just starting to learn Gimp ;)"
    author: "vladc"
  - subject: "Re: Suggested Eyecandy and Substantive Changes"
    date: 2005-07-16
    body: "awesome! =)\n\nwe've got a good long road ahead of us, so it's not like if you don't get them done by monday it'll be too late or something ... i look forward to seeing what you come up with."
    author: "Aaron J. Seigo"
  - subject: "Etched Glass KDE-look.org site"
    date: 2005-07-16
    body: "I've started posting screenshots for the Etched Glass style at:\nhttp://kde-look.org/content/show.php?content=26513\n\nI'll add other features (such as layout of expanded menus) in the next few days."
    author: "vladc"
  - subject: "Re: Suggested Eyecandy and Substantive Changes"
    date: 2005-07-15
    body: "About the first idea (etched glass panel) please...no! Longhorn beta is just out and we suddenly start to copy their \"innovations\"? I find that windec horrible and unusable (and I bet they will change it in the final release), so don't give a chance to micro-fans to say: you are only able to copy Windows.\n\nKDE4 is meant to be innovative, as in Plasma guidelines, so..."
    author: "Davide Ferrari"
  - subject: "Re: Suggested Eyecandy and Substantive Changes"
    date: 2005-07-15
    body: "Yeah, etched glass is pretty, but not very usable... the whole point of transparency is that you can see what's behind it, so naturally the first thing Microsoft does is figure out how to negate that =).\nIt is good for making it clear which window is actually on top, but I think there's better ways of achieving that; shadows for one, or applying different, non-obfuscating effects to the transparent areas... (darkening it a bit is one that comes to mind, but I'm far from a photoshop monkey, so there's probably better ones out there)"
    author: "Illissius"
  - subject: "Eye candy"
    date: 2005-07-15
    body: "Eye candy is good, but I'm worried we need to purchase a new expensive video card for the next KDE. Someone ease my mind. Someone tell me that I'll still be able to use KDE 4.0 without having to go backward to a Keramik look. Right now kompmgr is unusable and I'm worried the next KDE will be the same: wonderful for those who bought the approved video card but useless for the rest of us."
    author: "Brandybuck"
  - subject: "Re: Eye candy"
    date: 2005-07-15
    body: "KDE will continue to run on any X server. If you have no state-of-the-art video card you may simply miss some optional eye candy."
    author: "Anonymous"
  - subject: "Re: Eye candy"
    date: 2005-07-15
    body: ">>KDE will continue to run on any X server. If you have no state-of-the-art video >>card you may simply miss some optional eye candy.\n\nHow do you know?\n\nAre you a developer or something?\n\n"
    author: "JB"
  - subject: "Re: Eye candy"
    date: 2005-07-15
    body: "I am though, and participating on the panel-devel list (i.e. KDE 4 kicker/plasma ) you'll see this mention:\nhttp://mail.kde.org/pipermail/panel-devel/2005-June/000234.html"
    author: "Ryan"
  - subject: "Re: Eye candy"
    date: 2005-07-15
    body: "they probably know this because i (and others) have explained this numerous times in various forums on the net.\n\ni'm quite aware and sensitive to the needs of low-end hardware. we have users all over the world with a disparate array of hardware; moreover the desktop can not be allowed to take up much in the way of resources because you need those for your applications.\n\nthis is why i've repeatedly rejected the idea that plasma will require opengl, for instance. it simply isn't an option today.\n\ninstead plasma will employ an adaptive graphics system that will deliver a slightly less eye candied presentation when the system is not as capable (due to hardware, software or configuration) which will still be performant."
    author: "Aaron J. Seigo"
  - subject: "Re: Eye candy"
    date: 2005-07-16
    body: "\"state-of-the-art video card\"\n\nWhat does that mean? I hope it doesn't mean \"proprietary drivers\" or \"nvidia-only\"."
    author: "Brandybuck"
  - subject: "Re: Eye candy"
    date: 2005-07-15
    body: "Any really cool stuff that would bring older cards grinding to a halt, just wouldn't be displayed for you.  From what I understand, we'll be able to detect what Plasma can/can not do and adjust accordingly.\nSo if you have an older card, fear not."
    author: "Ryan"
  - subject: "Not sure about this"
    date: 2005-07-16
    body: "From the contest description:\n\n\"It used to be that icons on your desktop were no different than icons in your file manager\"\n\nand then\n\n\"Desktop Buttons are a COMPLETELY NEW OBJECT that will be used throughout all of Plasma. They will serve to create a level of consistency never seen in a desktop enviroment before.\"\n\nI don't think it's getting any more consistent than haing icons on the desktop behave the same way as in the file manager. After all, on my desktop I have mostly links to applications and documents. Why change that? Or are all these little animations meant to be used in the file manager as well?\n"
    author: "ac"
  - subject: "Re: Not sure about this"
    date: 2005-07-16
    body: "the idea that the desktop is just a file manager view is, imho, a bit broken from a few perspectives. first off, unlike a file manager, when you click on a folder the current view doesn't change: you get a new window in a completely different application! this isn't even the same as a \"spatial\" file manager where each window that opens is at least of the same type. moreover, the icons on the desktop also reference applications and are even dynamic (removable media, etc). also unlike a file manager you only have one view: it's always the same icons. and finally, unlike in a file manager where you generally want your icons all lined up in a list or a grid, you often want icons scattered into groups on your desktop.\n\nupon further consideration it seems that the desktop is actually used much more like a REALLY big desktop panel, not a file manager. in fact, things like superkaramba show that many users want to use it _exactly_ like a big desktop panel with applets and everything. so it seems to make sense that the consistency should be between panels and the desktop.\n\nand this gives the us the first steps towards being able to offer project/task centric layouts dynamically without struggling with the \"but it's a filemanagery view!\" concept, among other things.\n\ni know this is a newish way of thinking about the desktop, but i think it's superior. you'll still get to do have a bunch of file icons and app launches if that's all you really want, of course. but the rest of us will be free to do cool things finally.\n\nthat said, i do believe that some similarities between file icons on the desktop and file icons in the filemanager will remain, but that's another project i'm quietly working on elsewhere =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Not sure about this"
    date: 2005-07-16
    body: "aaron, i JUST can't wait for KDE 4... you've just made me drool ;P\n\ni had a look at KDE-Artists.org, there are some ideas brooding there, i'm sure Microsoft would love to read them :D"
    author: "superstoned"
  - subject: "Mirror"
    date: 2005-07-18
    body: "Could someone please mirror http://plasma.bddf.ca ??\nMaybe there are mirrors out there of which I don't know yet - but that site is just unreachable all the time :("
    author: "Fabian Wolf"
  - subject: "Re: Mirror"
    date: 2005-07-18
    body: "Currently it's apparently offline for good. Next time visit it by appending .nyud.net:8090, then a Coral cache of the site will exist, see coralcdn.org (would be a nifty feature to offer a button for in Konqueror)."
    author: "ac"
  - subject: "Re: Mirror"
    date: 2005-07-18
    body: "well, if people would quit submitting it to slashdot and everywhere else it would be fine =P\n\ni'm moving to a new server that will be able to handle the bandwidth needs. this new host was graciously donated and i'll be announcing it once it's all done."
    author: "Aaron J. Seigo"
  - subject: "Wow!"
    date: 2005-07-18
    body: "I've just came back to the KDE website after a while, and I'm really IMPRESSED of all this progress about KDE 4.0. I hope we'll see a KDE that can beat up ALL the desktops around (possibly MacOS too :P ).\nSo.... guys, KEEP ON THE GOOD WORK!!!\n\nP.S.\nWhere can i find additional information on the Plasma Interface?"
    author: "Daniel"
  - subject: "Re: Wow!"
    date: 2005-07-19
    body: "the plasma project website is at http://plasma.bddf.ca, but we're busy moving it to a new server that has enough bandwidth to withstand the weekly slashdottings. so ... check back sometime tomorrow or the next day =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Wow!"
    date: 2005-07-20
    body: "Thanks, I had a look and CAN'T WAIT for the release! Let's see if i can help any way... :P"
    author: "Daniele Draganti"
  - subject: "Re: Wow!"
    date: 2005-07-20
    body: "Thanks, I had a look and CAN'T WAIT the release! Let's see if i can help any way... :P"
    author: "Daniele Draganti"
---
Break out those digital paint brushes! <a href="http://www.spreadshirt.com/shop.php?sid=2718">Revelinux Gear</a> is <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,240.0">sponsoring a contest this weekend</a> over at <a href="http://kde-artists.org">KDE-Artists.org</a> which challenges the artistically inclined to come up with visual concepts for <a href="http://plasma.bddf.ca">Plasma</a>'s desktop buttons. These themable buttons will be used for all clickable file and action icons on the desktop, applets and panels. Being such a critical part of the user experience, they must be usable <em>and</em> stunningly beautiful. This contest aims to find out what they might look like.
Winners will receive an autographed <a href="http://plasma.bddf.ca">Plasma</a> t-shirt and may also have their artwork appear in KDE 4.0. 

<!--break-->
