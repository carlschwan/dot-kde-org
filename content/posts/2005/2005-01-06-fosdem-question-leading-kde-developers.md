---
title: "FOSDEM: Question Leading KDE Developers"
date:    2005-01-06
authors:
  - "jriddell"
slug:    fosdem-question-leading-kde-developers
comments:
  - subject: "KDE 4"
    date: 2005-01-06
    body: "Please, please please! What's comin' up in KDE 4? Can't wait!"
    author: "secretfire"
  - subject: "Re: KDE 4"
    date: 2005-01-06
    body: "Qt 4 :-)"
    author: "Morty"
  - subject: "Re: KDE 4"
    date: 2005-01-06
    body: "LOL\n\nand gstreamer, d-bus, hall support... better svg I guess, doublebuffered widgets and better xorgsupport. better multi-media and koffice and kwrite will get better fast(er). and more speed, of course (again...)"
    author: "superstoned"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "It's really, really, really gonna rock! :) I'm really looking forward to some UI improvements. Also, IO slaves are becoming better and better, and I'm sure KDE 4 is gonna be even better in this respect. And of course, Qt 4 will be nice. D-Bus and HAL are also much-needed. :) My mouth is watering... \n\n"
    author: "secretfire"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "Hall support? Is that because somebody want to use the kitchen sink outside the kitchen? :-P"
    author: "Shulai"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "AFAIK, it hasn't been decided yet what multimedia-system (gstreamer, MAS etc. etc.) ships with KDE4."
    author: "Janne"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "indeed,\n\ngstreamer might be nice, but I hear people talk about stability problems... the good thing about gstreamer is that has a much lower delay then arts, but remember gstreamer is NOT a sound daemon, the problem is network transparant sound. arts did this, but does gstreamer do this? we could rule that writing some sort of alsa backend in the kernel itself would be the best way to tackle the problem. that way kde devs won't have to care about this problem. but people needing this feature would be left in the cold until someone does. mas does is a sound server, but I was told it currently lacks features (that could be added?)\n\nso I expect there is still going to be a lot of talk about this."
    author: "Mark Hannessen"
  - subject: "Kernel support ? In how many of them :-)"
    date: 2005-01-07
    body: "Hello !\n\nI'm with you on most of your points. I only want to point that the option writting an alsa backend in the kernel itself doesn't seems a good solution: KDE works in many platforms, thus that backend should be written over and over again. It does seem a lot of work to me. \n\nCheers!\n\n"
    author: "Eduardo Robles Elvira"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "Going direct to ALSA doesn't solve problems like video synchronization, codec handling, or in fact any of the interesting problems of a modern multimedia system.\n\nMaking sound come out of the speakers is easy.  It's everything else that requires a real multimedia system (i.e. GStreamer, NMM) to make things work.\n\nJust to put things in perspective -- the size of the GStreamer ALSA output plugin is 3,404 lines of code.  The size of GStreamer and its shipped set of plugins is around 300,000."
    author: "Scott Wheeler"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "mplayer works fantastic.  both video synchronization is great and codec handling is superb."
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2005-01-08
    body: "And there's not a single distribution that will ship it where it can do anything particularly useful because of it being one huge, monolithic binary that can't leave more questionable codecs for later instalation.\n\nIt's also not a framework, it's a tool.  MPlayer is nice for what it does, but being the basis of a desktop multimedia framework isn't it."
    author: "Scott Wheeler"
  - subject: "Re: KDE 4"
    date: 2005-01-08
    body: ">And there's not a single distribution that will ship it where it can do >anything particularly useful because of it being one huge, monolithic binary >that can't leave more questionable codecs for later instalation.\n\nThat's not true.  I have all these codecs as individual DLLs on my system.  I can delete these at any time and mplayer will still work with other unaffected codecs.  Take a look at /etc/mplayer/ for details.\n\n> It's also not a framework, it's a tool. MPlayer is nice for what it does, but being the basis of a desktop multimedia framework isn't it.\n\nAll I need, and I suspect what KDE users need, is a convenient tool to view audio, videos, DVDs, streams.  mplayer with alsa and friends provide this. What is it you need, or that you claim that KDE needs?"
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2005-01-08
    body: "So you want to start mplayer each time you press backspace key in an empty line in Konsole so that the bell sound can be played?"
    author: "Ted"
  - subject: "Re: KDE 4"
    date: 2005-01-08
    body: "Huh?  No.  It works fine already.  Do you want to implement a desktop media framework for that?"
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "I know, but for the sake of simplicity (and its almost certain gstreamer WILL be supported, altough several other multi-media systems will be supported too)."
    author: "superstoned"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "I'd guess none will ship at all, and the multimedia backend (whatever you end up choosing) will be a loose (and foreign, not self-contained) dependency allowing you to go as well completely without any. I think after all one of the reasons for the architectural change away from aRTs is the current hard dependency to have it installed before anything KDE even if you don't care about sound."
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2005-01-07
    body: "Don't forget glib, gobject, atk and maybe even gnome-vfs!  KDE is gonna rock!"
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2005-02-04
    body: "KDE 4 desperately needs an extremely cool designer-kicker!"
    author: "Me"
  - subject: "email address"
    date: 2005-01-06
    body: "Shouldn't the email address be \"fosdem@gmail.com\" (not \".org\")?\n"
    author: "LMCBoy"
  - subject: "Not only Matthias !"
    date: 2005-01-06
    body: "And don't forget there will be other KDE speakers than Matthias Ettrich ! Just have a look at the speakers' list on the FOSDEM site, and you can also send questions regarding non-KDE subject of course !"
    author: "FOSDEM"
---
The biographies of KDE speakers at <a href="http://www.fosdem.org/2005/">FOSDEM 2005</a> are up for <a href="http://www.fosdem.org/index/speakers/speakers_ettrich">Matthias Ettrich</a>, <a href="http://www.fosdem.org/index/speakers/speakers_fernengel">Harald Fernengel</a> and <a href="http://www.fosdem.org/index/speakers/speakers_dymo">Alexander Dymo</a>.  FOSDEM will interview speakers before the event so if you have questions about the future of KDE or KDevelop please send them to <a href="mailto:fosdem@gmail.com">fosdem@gmail.com</a> or add a comment to this story and we will send them on.

<!--break-->
<p>If you are a KDE developer planning on coming to FOSDEM join the <a href="https://mail.kde.org/mailman/listinfo/kde-events-benelux">mailing list</a> and add yourself to the <a href="http://www.kde.me.uk/index.php?page=fosdem-2005">wiki page</a>.</p>




