---
title: "amaroK 1.2 First Player to Integrate  Audioscrobbler"
date:    2005-02-18
authors:
  - "mkretschmann"
slug:    amarok-12-first-player-integrate-audioscrobbler
comments:
  - subject: "Great Application!"
    date: 2005-02-17
    body: "Amarok is a great application. Thanks!\n\n(I signed up for Audioscrobbler and still submit my music, but since the personal radio feature never really worked for me, there is not much use in Audioscrobbler for me personally.)\n"
    author: "Anonymous"
  - subject: "Sharing"
    date: 2005-02-17
    body: "Just to note, this doesn't appear to in anyway let you share songs.\nI thought it did at first, misreading some of the sentences in the summary :)\n\nThis looks really good.  Not sure why to be so proud that it's not a plugin, but each to their own.\n\n"
    author: "JohnFlux"
  - subject: "Bah"
    date: 2005-02-17
    body: "Knew I should have asked Charles to include my audioscrobbler plugin with noatun 2.6 :). In all seriousness though, I think audioscrobbler is a prime example of something which should be a plugin, because for many users it will seem like pointless bloat. Anyone know why AmaroK decided to go with it in the player? Could it be because despite their \"Powerful scripting interface\" they don't have a proper plugin system?"
    author: "mikeyd"
  - subject: "Re: Bah"
    date: 2005-02-17
    body: "'amaroK goes a step further than other media players and allows users to receive music recommendations from the site.' being one reason.\n\nBut yes, if you think everything should be a plugin, by all means use Noatun."
    author: "Ian Monroe"
  - subject: "Re: Bah"
    date: 2005-02-17
    body: "That's certainly doable with a plugin, I'm putting it in mine now. (Well, as soon as I can get the spec from audioscrobbler. Site is pretty slow right now)"
    author: "mikeyd"
  - subject: "Re: Bah"
    date: 2005-02-17
    body: "To me, bloat means that an unused feature has a negative impact on the performance of the application, gets in the user's way or makes the softwarepackage too large to handle.\n\nAFAIK audioscrobler in amarok causes neither of these issues, so it is not bloat :o)\n\nI like the idea of plugins though..\nBut problem with plugins is that they usually get bundled together, so if you want to use the equalizerplugin, you get audioscrobbler and the rest for free with it :)\n"
    author: "ac"
  - subject: "Re: Bah"
    date: 2005-02-20
    body: "If it's not a plugin, then it must be in the main executable and add to the overall size, so it will impact performance by increasing size in memory, unless it has no size, in which case it does not exist."
    author: "jameth"
  - subject: "beta testing"
    date: 2005-02-17
    body: "I like amarok. A lot. I always use it if I can, and its interface is great! The only problem is its stability, it has always crashed or hung far too often. I wonder whether other people have made the same experiences?\n\nOh, and in the collection view, I'd like to be able to select TagFilter->FirstLevel->Filename.\n\nOther than that, its the best music player I've ever used. And the people in #amarok are nice and helpful, too!"
    author: "me"
  - subject: "Re: beta testing"
    date: 2005-02-18
    body: "Blame all problems on artsd ;)\n"
    author: "JohnFlux"
  - subject: "Calling home"
    date: 2005-02-17
    body: "That's what I was missing from MS Windows, apps which connect in the background to the internet (without user interaction).\n\nI disabled audioscrobbler, but ethereal shows that amarok still connects to audioscrobbler.\n\nAmarok has nice features but has usability problems:\n\nFor no reason it uses a different menu structure, a different toolbar position,\ncontrols in the status bar.\nMany actions are only possible when you open the right panel.\nWhy is \"repeat track\" in the Settings menu? \nWith juk or kaffeine I needed 5 sec to find out how I can add a folder, with amarok I needed 15 min. \n"
    author: "ano"
  - subject: "Re: Calling home"
    date: 2005-02-17
    body: "> With juk or kaffeine I needed 5 sec to find out how I can add a folder, with amarok I needed 15 min.\n\nThen please use Juk or Kaffeine."
    author: "ac"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "No! Please give Juk, Kaffeine AND amaroK a usability review. Strange menu-layouts suck (usually)."
    author: "Anders"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "Please spare us the 'usablity review.'\n\nSomething good did come out of it, its a bug that File->Open Media doesn't allow the selection of directories."
    author: "Ian Monroe"
  - subject: "Re: Calling home"
    date: 2005-02-17
    body: "\"I disabled audioscrobbler, but ethereal shows that amarok still connects to audioscrobbler.\"\n\nHave you unchecked \"retrieve similar artist\" in the AudioScrobbler config dialog? If this option is enabled, Amarok will still submit the current song (to get similar artist) but does not submit it to your profile, so the submission is anonymous.\n\n"
    author: "Anonymous"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: ">Have you unchecked \"retrieve similar artist\" in the AudioScrobbler config dialog?\n\nUm, I absolutely hate it, if applications connect to the world submitting data, even though the user wasn't asked before. This should always be disabled by default and an info dialog be shown the first time you enable such a \"feature\".\n\n\nbtw. I'd like to have a --disable-audioscrobbler option, too."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "I was also surprised and annoyed when I found out that amarok chats with someone else on the internet, without having asked me first. I added this line to my /etc/hosts to stop this undesirable practice:\n\n127.0.0.3  www.audioscrobbler.com  audioscrobbler.com\n\n:-)"
    author: "Melchior FRANZ"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "if that's easier than unchecking one setting :-)"
    author: "muesli"
  - subject: "Re: Calling home"
    date: 2005-02-19
    body: "No, it isn't. But I trust it more than amaroK, which already has disappointed me once in this area.  :-P"
    author: "Melchior FRANZ"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "Well how about adding a bug to bugs.kde.org"
    author: "ac"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "Adding a bug is not the problem. The question is: Are all kde developers so unreflected about privacy issues!? Is there no policy thingie you have to read and agree to before you get a cvs account?"
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "please also file a bug report for konqueror. i just noticed: every time you surf to a website, some suspicious dns server gets connected! ;-)\n\nomg, firefox has the bug. :-))\n\n...muesli\n"
    author: "muesli"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "muesli: I can wobble with my ass, too. ;p There is a difference between intentionally requesting some external website data and giving away information \"user A likes to hear X, Y and Z, please suggest some more\" behind user A's back. It's spying - like your browser wouldn't ask you, if you accept a cookie."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "i'm totally aware, that there is a difference. but i think you understood the irony, otherwise your ass wouldnt wobble ;-)\n\nplease also see my other post. we only submit the artist, only the first time you ever played an artist. it's anonymously, it doesn't get stored anywhere. it's not worse than any banner on a website, which you prolly didnt request either. contrary to the banner, this really helps and gives the user useful feedback: suggestions.\n\nif that's spying, omg better watch your neighbours, they might be able to hear what you are listening to right now. unanonymously in this case.\n\nregards,\nmuesli"
    author: "muesli"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: ">unanonymously in this case.\n\nUm, it's not. On the one hand there are static ip's, on the other hand it suffices to know which music people like in some area to make money with it. It's very unfriendly any definitely in a legal grey zone, if not illegal, to (help to) gather data without asking the individuals, if they agree.\n\n>suggestions\n\nAs I said: Disable it by default and popup some information, when users enable it the first time."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-19
    body: "sorry, but: no-one gathers data, here. you can look at the audioscrobbler sql-tables on their site and tell me where your ip and artist gets stored.\n\nand knowing an ip with an artist related doesn't help anyone, get real. what do you expect? spammers to winpopup your ip with \"buy cds\", now? ;-)\n\n...muesli\n"
    author: "muesli"
  - subject: "Re: Calling home"
    date: 2005-02-19
    body: ">sorry, but: no-one gathers data, here.\n\nMaybe, maybe not - I'm pretty sure you know very well that looking at the sql tables just tells me that the information isn't stored there. It wasn't my point to say that they do this. I wanted to show the possibilities.\n\nMy point is that you as a developer don't have any right to decide for the user to reveal his data - even in such a minor case. It is the developers responsibility to care for user data as much as to care not to produce vulnerable code. If this is not a central point in your thinking when developing applications, it is a major problem, imho."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "carlo: i def. see your point. i'm just wondering if it's worth the hassle, and hiding the option, since 99% of our users want this behaviour by default, and prolly wouldnt know about it otherwise.\n\ni started thinking the last days, about how to solve the situation for both of us: the best solution i could come up with, is to add some info to the first-run wizard, telling the user about audioscrobbler's suggestions, their impact and asking them whether to enable or disable them.\n\n...muesli"
    author: "muesli"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: ">i'm just wondering if it's worth the hassle.\n\nI agree that's a hassle, but it's fairly important, since you can't fight against dictation technologies like drm, while \"betraying\" the user yourself.\n\n\n>since 99% of our users want this behaviour by default\n\nI'm still sorrowing for AudioGalaxy, so count me in. :)\n\n\n>i started thinking the last days, about how to solve the situation for both of us: the best solution i could come up with, is to add some info to the first-run wizard, telling the user about audioscrobbler's suggestions, their impact and asking them whether to enable or disable them.\n\nWould be wonderful, if you'd find a way to let the user decide, but making it as simple as possible for him."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "Lalala, so you suggest that konqueror should ask for extra confirmation with every http request it makes since it's revealing data right? And of course turn off referrer by default since you are leaking data here, too. I suppose dns queries should be confirmed too since the dns server may be logging what addresses are you resolving, damn it. Sure, a checkbox in first-run wizard or something would be fine, since it's pretty much optional here and few people know the risk of someone sniffing in their music preferences... But your central point argument is way overblown IMO."
    author: "mornfall"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "Nah, there is a difference between a referrer and someones personal music preference. Nevertheless: A browser should reveal only as much information as necessery, too. The dns queries etc. are technically needed so there's nothing to say about it. You say it's overblown, I say: Better to care now, than to be taken by surprise later."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-19
    body: "*wink* *wink* don't get offended ;-)\n\nHere is an anonymous suggestion: go to the real world more, maybe get some unanonymous meaningful relationships with human beings. I find amusing your paranoia, and delusional \"i-will-be-uber-anonymous\" complex. For one, I like getting  suggestions and so I can expand my cultural and musical palate. This is just a piece of software, is just computers, is just music. Don't worry your soul and your mind won't get compromised by them, but it seems that your mind is already controlled by the inherent flaws of computer systems.\n\nAnd to muesli: Thank you for such a fine piece of software. You guys are dedicated and the average, healthy user will be thankful for the suggestions."
    author: "Manuel Garcia-Duque"
  - subject: "Re: Calling home"
    date: 2005-02-19
    body: ">Don't worry your soul and your mind won't get compromised by them, but it seems that your mind is already controlled by the inherent flaws of computer systems.\n\nThank you very much, but you're wrong. It's not an inherent flaw of computer systems, it's missing education regarding the social aspect of software development. You're resposible for the applications you write."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "Wtf! I am genuinely offended by your point here. I as a developer have absolutely zero responsibility for what you do. I go as far as providing you with full source code and a virtually unlimited use, modify and distribution license and you come accusing and crying here that i am irresponsible for not caring for your privacy enough and/or that i am missing education? Dear sir, please think twice before accusing someone of being uneducated, you could deeply offend the said someone (well, already happened, oops). If you are so paranoid please go and audit the source code of whatever you use. But i'm afraid the http server you will download the source from will log your address so better hang yourself. Anyway, there is something very very wrong with the way you think."
    author: "mornfall"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "Uh, then be offended. ;p Even if you dismiss it, my plea got your attention at least. Too bad you don't see the difference between needed information and personal user data, but maybe it's the point to agree to disagree."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "I think the word \"awareness\" is a better word than \"education\" here. \nAnd privacy is a touchy issue to many people. \n\nI do think it's the responsibility of the developer to make his app secure.  It would be irresponsible to release a piece of code that did an \"rm -rf *\" or sends arbitrary documents from the users disk to the developer's email account when an unsuspecting user launches it.  That's more than \"zero responsibility\" for what happens on the user's computer.\n\nOf course, audioscrobbler seems a corner case to me.  It's *very* probably harmless. Still, I would prefer if amarok implemented it either defaulting to off or, as discussed above, as option in the first time wizard. \n\nIMHO it should be a design principle that no *unexpected* network transfers be done without asking.  DNS lookups and HTTP request *are* expected when browsing a web page.  Contacting a service on the internet when listening to music is not, IMHO. \n\nAnd then there are people with dialup lines who pay for the connection.  I guess they won't be too happy discovering that \"something\" causes costs.  And how should the average user find out that it's a turned on option in his music player?  Not everyone knows ethereal or how to operate it... \n\n"
    author: "cm"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: "Do you suppose average users know anything about http requests or dns? How is assuming someone knows how web browser works different from assuming someone knows how audioscrobbler suggestions work? You are trying to say that all users should be protected from unexpected network traffic. But that is certainly not achievable. So what you are saying is, basically, that there are certain groups that deserve more protection (iow, those that are majority right now). Anyway, my point wasn't at all about the particular problem. My point is, it is users' (and noone else's) responsibility. Read the license again. Noone is guaranteeing you anything here. You take all the risks. Sure if someone makes a program rm -rf your $HOME, well, he may be an asshole. But it is your fscking responsibility that you downloaded and ran the program. You have to read the license anyway, since else you couldn't know you have a right to even run the program legally, so you also should know that there is no damn guarantee attached. If you are paranoid, just don't run it. Or audit the source as i said above. Or pay someone to do it for you. But whatever my program does to your data is _your_ problem. Of course you can be angry with me and stuff, but the responsibility is still yours. Aight? Your argument generalizes to \"it's carmaker's fault there are people driving cars without a clue about them and cause harm\". Maybe you didn't really want that?"
    author: "mornfall"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: "> Do you suppose average users know anything about http \n> requests or dns? How is assuming someone knows how web \n> browser works different from assuming someone knows \n> how audioscrobbler suggestions work? \n\n\nNo, the average user does *not* know how these things work. \nThat's exaclty why it's the responsibility of the developer \nto choose safe and sane defaults. \n\nAn average user expects that his computer connects the internet\nwhen he calls up a web page, but he doesn't expect it to when\nhe's just listening to music.  So the software shouldn't by default \nand without warning.\n\n\n> My point is, it is users' (and noone else's) responsibility. \n> Read the license again. Noone is guaranteeing you anything here. \n[...]\n> But it is your fscking responsibility that you downloaded and ran the program. \n\n\nThis is not about legal boilerplate.  It is about responsible \nsoftware development.  This is a moral category.  I think it's \nup to the developer to release the software in a safe and sane \nstate when he calls a program stable. \n\nHonestly, I'm somewhat surprised if not shocked to find this \nattitude in the KDE community.  I have come to know it as \na very responsible one that cares about the user's safety. \n\n\n> Your argument generalizes to \"it's carmaker's fault there are \n> people driving cars without a clue about them and cause harm\".\n\nWe're not talking about clueless drivers, we're talking about the \nequivalent of cars that do something unexpected every time you \ndrive at more than 100 km/h because that's a feature hidden \nsomewhere in the car's configuration.   \n\nIt may be a feature, it may be even documented, \nbut no carmaker would implement it without warning the driver \nabout it if it was potentially dangerous. \n\n\n"
    author: "cm"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: "You are missing the point, as usually. Right this is not about legal boilerplate. This is about you demanding things from free software developers which is a big no-go. I merely defend my freedom to implement whatever *I* like and however *I* like in software *I* write. I heed not what a random jerk downloading the software says i am doing wrong. Your basic problem here is that you come with *demands*. So stop that if you ever want someone to listen to you. Of course you are free to make suggestions or state wishes. Or report bugs. But I don't care about whatever you have to say as soon as you demand anything from me. You have no damn right to. Not the slightest. You may also notice that i stated that the checkbox-in-startup-wizard is a good idea in my opinion. You may also notice how i didn't go and tell muesli that he must implement it and as soon as he refuses i call him uneducated or whatever. And indeed, this is very shocking nowadays, that developers are not acting as slaves of their users. I suppose we should be all ashamed. Dear Godly User who brings me my daily bread blah blah. It's not as i didn't care about users. I listen to reasonable users every day. I heed what they say because i think it's important. It's more like rude users are demotivating experience for an opensource developer. And when i defend myself you are shocked which i find somewhat ironical. You are just spoiling it for everyone else..."
    author: "mornfall"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: "With all due respect: \n\n\n- What makes you think I'm \"only\" a user.  What makes you think I'm talking about you in particular?  I've made my own (albeit modest) contributions to Open Source software and KDE in particular.  I've just stated what I think are good development principles with respect to network transfers.  I do feel a moral (not legal) responsibility for the users of the apps I touched.  Don't I have the right to discuss what I believe should be a general policy in KDE (I even thought it already was, at least implicitly.). \n\nWould you have told Waldo Bastian or David Faure the same thing if they had expressed their opinion on the subject of responsibility of the developer? No, I don't want to compare myself to them, nor to you(!), on grounds of merit, I wouldn't stand a second.  But I object to being called a \"random jerk\" which you seem to imply.  \n\nYou talk about demotivation.  Right now I feel pretty demotivated by you to do anything more for KDE.\n\n\n> You are missing the point, as usually. \n- Here you're getting personal.  I've only posted twice on this subject up to now.  Do you really think you can judge me from those two postings?  Or are you talking about me in general?  IIRC I've never had any discussion with you in particular so it would probably take a very blatant case of nonsense from my side to stay in your memory like that.  Please point me to it if that's the case. \n\n\n\n- I have called *no one* uneducated.  That's been someone else, and this may be a case of a language problem.  I don't think the person wanted to be offensive.\n\n\n- I have never said the amarok developers, or you, *must* do anything.  I just stated my opinion on good development principles, and why I think they are good. You never even bothered to respond to any of the arguments, you just stated the user was responsible, and no one else.\n\n\n- Have you really just called me rude?  Please point me to where I've been rude to you.  Because I talked about \"attitude\"?\n\n"
    author: "cm"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: ">- I have called *no one* uneducated. That's been someone else, and this may be a case of a language problem. I don't think the person wanted to be offensive.\n\nTo give it the truth, I chose the word with caution. Not to flame, but to have a discussion about privacy considerations in software development, an often underrated point. I'm sure quite a few KDE developers read the dot and I hoped someone would be vexed enough to write such a reply as mornfall did. Thank you mornfall. :)"
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-22
    body: "Well, you are in the role of user in this thread. Every developer is an user, too. I am an user to some apps while i'm a developer to others. Anyhow, if say David came (as much as he never would) and \"ordered\" me to do something in say libapt-front \"his\" way (a project he has pretty nothing to do with), i would pretty much send him to hell (that, and lose much of the respect i have for him). I am pretty damn confident he would never do it, as i said.\n\nAs for you, well, you very much side with Carlo and thus i somewhat confused the two of you. Anyhow, you assert that it is my responsibility what user does with the program, more or less, and there i conclude you want me to do so that however clueless user doesn't get burnt, which is something i never will, unless i get paid for such work (in which case, i would think about it, yes). It is however me myself who decides how much i help the user with avoiding pitfalls. You may have your say in that of course. Keep in mind that the better and more of a contribution you did the more you can say. I maintain that users of a given application should just ask politely, or maybe can afford a bit less politely if they have patches. From my experience, the more aggressively something is demanded, the less important it is. That, and it is very unrewarding to work on anything that was previously demanded by anyone, since you are not going to be thanked upon implementing that whatever. Easy as that. Asking nicely will get you much farther than arguing.\n\nNevertheless, the random jerk or rudeness comments weren't directed at anyone in particular, really. In fact, i wasn't really personal anywhere, and i'm sorry if you took that personally. I tend to speak in general, even that \"missing the point as usual\" wasn't directed at you specifically. Anyway, i was pretty much offended (thank Carlo for that one) and consequently somewhat angry (which i hate to be, since it affects my productivity badly) and thus not very discriminating in my replies. Anyway, you get what you ask for. I am not known to be \"easy\" with strangers or for being politically correct. Yada. Sorry 'bout that. I should go do my work now instead of commenting in this silly thread. I suck."
    author: "mornfall"
  - subject: "Re: Calling home"
    date: 2005-02-22
    body: "> I should go do my work now instead of commenting in this silly thread. \n\nThat's exactly what I promised myself to answer to your next reply \nafter posting my previous comment.  :) \nLet's do something productive.\n\n\n"
    author: "cm"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: ">Sure if someone makes a program rm -rf your $HOME, well, he may be an asshole. But it is your fscking responsibility that you downloaded and ran the program.\n\nIf the doomed user can make it clear in court, that it was done by intention (o.k., won't be easy), the one who wrote it would likely be plead guilty. Even giving something away for free doesn't mean, that you can't be held responsible for your doing. But it's more about moral obligations than law we speak about.\n\nWhat puzzles me is, that you say, you are not able to distinct between private data and data that's needed for a networking task, while everyone else can."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: "If i recall correctly (i probably don't), the only case where warranty disclaimer like the one in GPL is void is when serious injury or death is inflicted due to the program. Anyway that is pretty irrelevant.\n\nYou say that \"everyone else can\" may i ask this \"everyone else\" what exactly is private data and what exactly is \"needed for a networking task\"? This is very unclear to me, since in some places, uploading illegal content is \"needed for a networking task\" and illegal content would probably also qualify as \"private data\" huh. You are somewhat confused (or confusing?) with your \"clear\" and \"commonly known\" ideas. In other words, \"everyone else knows that...\" is a common practice, where you try to persuade someone that your personal opinion is held by general public and try to substitute your missing authority with that notion of many people thinking whatever you think."
    author: "mornfall"
  - subject: "Re: Calling home"
    date: 2005-02-21
    body: ">If i recall correctly (i probably don't), the only case where warranty disclaimer like the one in GPL is void is when serious injury or death is inflicted due to the program. Anyway that is pretty irrelevant.\n\nI wonder how a german court would decide, if you can prove something like `rm -rf /` was added by intention, maybe with the comment \"Don't compile as root, asshat.\". General responsibility exclusion rules are somewhat limited here.\n\n\n>is a common practice, ...\n\nWell, you're the only one in this thread, who says, that you can't distinct between sending user data behind the users back to a third party website, even though the user never agreed and a common protocol usage, when a user explicitly requests website."
    author: "Carlo"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "[different menu structure]\n\nIt's a different app with a different focus, so I think it's pretty much OK to have a different menu structure...\n\n[different toolbar position]\n\nSee above. On a side note, every CD player has the controls below the display, not above the display.\n\n[controls in the status bar]\n\nUse Konqueror? Check the green LED at the left of the status bar, and the checkbox at the right - big surprise! amaroK is not the only app with controls in the status bar!\n\n[Many actions are only possible when you open the right panel.]\n\nSame is true for 99% of all applications. Eg, if you remove the sidebar in Konqueror, you'll also loose quite a few actions...\n"
    author: "Willie Sippel"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "> It's a different app with a different focus [...]\n\nThat's true for every single application in KDE.  I don't see why different rules should apply to amaroK.\n\n> every CD player has the controls below the display [...]\n\nAnd that's how we ended up with user interfaces that look like this:\n\nhttp://www.linux-user.de/ausgabe/2002/09/010-neueprogs/xine-main.png\n\n...which pretty much everyone with an ounce of usability experience knows, well, plain suck.\n\n> Use Konqueror?\n\nThose aren't exactly great in Konqueror.  And there they're not central interface features such as they are in amaroK.\n\nEssentially you've argued three times here that \"other people do stupid things, so they can just be ignored in amaroK too\".  I'd rather see a constructive argument on why those choices are or are not useful on their own.  Like mom always said -- two wrongs don't make a right.  ;-)"
    author: "AC"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "You're mom probably also told you something like \"Don't judge things that you got for free\".\n\nThe programmers like there app.\nThe users love amarok.\nAareon Seigo loves amarok. (http://aseigo.blogspot.com/2005/02/amarok.html)\n\nSo if you don't find it usable, *don't* use it.\n\nThanks!"
    author: "ac"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "I *do* find it usable, but I too think that it could be even better than it is now. Your blantant refusal to accept some critisism \"because it's free\" isn't really in the spirit of how things are done in KDE. Why not keep an open mind on how this program could be even better. Not all suggestions can or should be followed, but simply dismissing them with \"Don't judge things that you get for free\" is far to easy for my taste. \n\n> Aareon Seigo loves amarok. (http://aseigo.blogspot.com/2005/02/amarok.html)\nYeah, but he has a point about the blinking highlight in the playlist...\n\nAndr\u00e9"
    author: "Andre Somers"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "> Your blantant refusal to accept some critisism \"because it's free\" isn't really in the spirit of how things are done in KDE.\n\ncritisism isn't in the spirit of KDE. Offering suggestions how to improve things is.\n\nSaying menu-structure, toolbar, etc are all unusable just because they are different, is just critizing other people work.\n\nSaying \"repeat track\" should be in the \"Settings\" menu because [reason], is helpful and always welcomed."
    author: "ac"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "Amarok is a great app, and I can't live without it. Its a fine piece of work. But it took me quite some time to get used to it, and altough I know how to use it now, I do think thats wrong. I know, if you are used to how amarok works, its no problem. I'm sure that's why the developers don't see anything wrong with it. And the users don't have a problem anymore. but everyone who sees amarok for the first time is confused, and has no idea what to do. And I think a usabillity report could help.\n\nI know, its not the developers fault - I think its normal. this is how all bad usabillity starts - once you (the developer) gets used to it, you don't notice it. Thats why you guys REALLY need a few outsiders, who know what they are talking about, to check the app. and give some FREE ADVICE. Listen to them, and please - you don't have to do what they say, but at least think about it.\n\namarok has become better, usabillity-wise. the menu on top is really an improvement, absolutely. so don't think we don't like your app. but it could become better. isn't that what you want? its also what we, the users want. please. don't ignore us, think about usabillity, let some outsiders look at it, and take their comments into considderation."
    author: "superstoned"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "> amarok has become better, usabillity-wise. the menu on top is really an improvement, absolutely. \n\nYes, that was something that bugged me, too.  That's a very good example where consistency with the rest of the desktop trumps fresh interface design. Every time I wanted to open the menu I first searched automatically in the wrong spot and said \"Argh! This is different in amarok.\"\n\nThanks for changing that. \n\n"
    author: "cm"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "yes, you see? the fine guys @ amarok DO listen to users, so I have good faith they will think about the current design (which, mostly, is very good, by the way, and I don't see a way to improve it - but I'm no usabillity expert...)."
    author: "superstoned"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "Just to make sure.... I'm not an amarok developer."
    author: "ac"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "> Aareon Seigo loves amarok.\n\nyes, yes i do. it makes my MacOS X loving friends drool with envy. it makes my music listening experience a joy. it gives me lyrics and other cool toys. it doesn't kill my mail/browsing box at home (which is a PII-400 ... i see no need to harass my devel machine with my email habits, it's already too busy compiling source code =)\n\nHOWEVER ... the toolbar in amaroK is, IMHO, nonsense. i have no problem with it being down on the bottom, but the order and choice of buttons doesn't make much sense. for instance, play is off to the right in the middle of a bunch of other buttons. and what's the most common thing one does when they go to use a media _player_? play. and why undo/redo is on the toolbar, especially when things like enqueue aren't are a bit mysterious to me.\n\na bug (apparently) in v1.2 causes the sidebar to flip to the context tab when tracks switch automatically. it should only do this when the user switches the track.\n\nclicking on an album cover with no album image in the context view gives you a dialog that says, \"close this box and right click!\". it probably should have a couple of buttons that allow you to take the most common actions of \"select a custom image\" or \"fetch from amazon.us\".\n\nthere are other things in there that could use help, like the playlist tab has some inconsistencies.\n\nbut ... these are all fairly minor things. polish that will come with future releases. most important is that amaroK seems to have most of the large scale interaction issues down. those are the things that are far hard to change than the default toolbar set up.\n\nluv 'n hugs, aseigo."
    author: "Aaron J. Seigo"
  - subject: "Re: Calling home"
    date: 2005-02-20
    body: "hey aaron!\n\ni totally agree, our toolbar settings should be refined. since you're way more experienced in the whole usability area than we are, it'd be cool to get your opinion about the right order of icons down there. a few suggestions you already made, and i guess we will take care of these issues before 1.2.1!\n\nbtw, dont know if you noticed the switch in the config dialog: you can completely disable auto-switching to contextbrowser. but again, you still got a valid point: it should only switch there, if the user changed the song.\n\nthe third issue is an akward one: to be able to legally fetch covers from amazon, we need to stick to their license. which, i'm afraid, says: clicking on an amazon cover should open the amazon product page. now, you can guess already and you'll prolly agree: we need to stick to the right-click menu for all the covers then (if downloaded from amazon or local). the total inconsistency of click-behaviour would be worse than this little dialog, i assume.\n\ngroup-hug ;-)\n...muesli"
    author: "muesli"
  - subject: "Konqui and the \"strange\" controls"
    date: 2005-02-18
    body: "As we are on the topic, WHAT do the checkbox and the green LED do in Konqueror? I never found out."
    author: "Humberto Massa"
  - subject: "Re: Konqui and the \"strange\" controls"
    date: 2005-02-18
    body: "It's for splitted views: The green LED shows the active view. With the checkbox you can \"chain\" views together (and with \"Lock to Current Location\" you can fix one of them while having the others following)."
    author: "Anonymous"
  - subject: "Re: Konqui and the \"strange\" controls"
    date: 2005-02-18
    body: "If you use multiple views in a split konqueror screen, the checkers are used to link the views with each other (e.g. when changing directory in one view, the linked view will follow the change).\nThe green led shows which view is currently active"
    author: "ac"
  - subject: "Re: Calling home"
    date: 2005-02-18
    body: "it's retrieving similar artist. if you dont want: disable it in the config dialog.\n\nfor all of your funny paranoia-freaks:\n\n1. only the artist name gets transmitted to audioscrobbler.\n2. only the first time you play a song of an artist, it retrieves the related artists from audioscrobbler. after that it's cached.\n\n...muesli"
    author: "muesli"
  - subject: "KDE Usability"
    date: 2005-02-18
    body: "I know that this is a contentious issue, but as we move towards KDE 4 the applications issued as part of KDE need to scrutinised.\n\nAmarok is an excellent media player and should become the standard player, with other KDE music players removed.\n\nKaffeine has demonstrated time and again that it is better than the other movie players and should be the only player provided as part of the standard KDE packages.\n\nHow many image viewers, text editors and other applications that do the same task are really needed?"
    author: "Ian Ventura-Whiting"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "\"Amarok is an excellent media player and should become the standard player, with other KDE music players removed.\"\n\nAFAIK, the Amarok-developers were asked that would they like to be part of \"official\" KDE. They declined (their release-schedule was too fast IIRC). KDE needs to have an integrated music-player, and since Amarok-guys are not interested, there needs to be another one.\n\n\"How many image viewers, text editors and other applications that do the same task are really needed?\"\n\nText-editors should be reduced to one. I know, I know... We have \"complex\" text-editor (Kate) and \"simple\" editor (KWrite?). And we have third one (Kedit? I always confuse Kedit and Kwrite) that does BiDi and it will be dropped as soon as Kate can do Bidi. But seriously:\n\n1) I have been hearing about the BiDi-issue for a long time already. How long is it going to take?\n\n2) I don't buy the \"complex\" editor and \"simple\" editor argument. Writing text with either of those editors is just as easy/difficult. Sure, Kate has loads of features, but they don't make it really harder to use. And if they do, maybe KDE could implement Kate-profiles like in Konqueror? one profile for full-featured editor, and another for simple editor.\n\nIf thoe issues were resolved, number of text-editors would drop from 3 to just one. And maybe we could then drop the \"Editors\" entry from Kmenu as well? Of course, Kword would be whole different ball of wax."
    author: "Janne"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "KWrite basically is a Kate profile.  They share the same editing component and KWrite is just a thin wrapper around that.\n\nThe problem is that KWrite is still more or less an advanced text editor.  KEdit I think is more of the \"It's just supposed to edit text, stupid\" editor that I think a lot of users are looking for.\n\nWhere or is we'll find an answer to this is an open question, but I expect that there will be a fair amount of house cleaning for KDE 4."
    author: "Scott Wheeler"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "> The problem is that KWrite is still more or less an advanced text editor. KEdit I think is more of the \"It's just supposed to edit text, stupid\" editor that I think a lot of users are looking for.\n\nYeah, I never understood why people want to remove KEdit and keep KWrite. IMHO KWrite is to advanced for a simple text editor. Please just remove KWrite.\n\nI also never understood why we have a special category for text editors in KMenu. How about moving Kate to \"Development\" and KEdit to \"Utilities\". This way we also hide the fact that we have more than one editor. ;-)\n\n"
    author: "Christian Loose"
  - subject: "Re: KDE Usability"
    date: 2005-02-19
    body: "> Yeah, I never understood why people want to remove KEdit and keep KWrite. IMHO KWrite is to advanced for a simple text editor. Please just remove KWrite.\n\nI feel the same way. I use Kate all the time. I use KEdit all the time. I never touch KWrite. It's too advanced for a simple text editor and too simple for an advanced text editor."
    author: "QV"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "Best suggestion so far, IMHO.\n\nJust, don't remove kwrite, simply remove the .desktop entry so it disappears from the menu (and people will magically stop complaining). The app/code is good to keep around both as a test for simple KTE editor plugin embedding, and as example code for such an app.\n\nKEdit is a simple text editor. Kate is a programming editor, just a debugger plugin short of an IDE. ;)\n\n\n(Btw, the amount of virtual ink spilled on this non-issue is amazing. When will people start complaing about KDE shipping with ~10 style plugins? They even take up far more harddrive space than KEdit ever did! Oh, the horror!)"
    author: "teatime"
  - subject: "Re: KDE Usability"
    date: 2005-02-19
    body: "You don't gain anything by removing KWrite. KWrite is just a flimsy shell around the KatePart KPart, while Kate is a more complicated shell around the same component. You can actually remove the KWrite binary and still be able to start it from miniCli (ALT-F2). \nSome people actually need the features it has. If you think it puts too much buttons on your toolbar, why don't you just remove them? As soon as this BiDi thing is solved, I'd be glad to see KEdit go. KWrite and Kate I both use regulary."
    author: "Andre Somers"
  - subject: "Re: KDE Usability"
    date: 2005-02-19
    body: "> You don't gain anything by removing KWrite. KWrite is just a flimsy shell around the KatePart KPart\n\nAs if Scott doesn't know this. \n\nI think he is talking more about KMenu than removing it from CVS.\n\n> Some people actually need the features it has.\n\nThen why don't you use Kate.\n\n> I'd be glad to see KEdit go. KWrite and Kate I both use regulary.\n\nFor me KEdit starts up much faster than KWrite. And sometimes I just need a stupid plain text editor. That's why I regulary use Kate and KEdit. I don't have any use for KWrite."
    author: "Christian Loose"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: ">I don't buy the \"complex\" editor and \"simple\" editor argument. Writing \n> text with either of those editors is just as easy/difficult. Sure, Kate \n> has loads of features, but they don't make it really harder to use. \n\nThis would get into an interesting discussion with the camp claiming a few extra buttons in a toolbar makes for \"horrible usability\". ;)\n\n\n>And if they do, maybe KDE could implement Kate-profiles like in Konqueror? \n>one profile for full-featured editor, and another for simple editor.\n\nYou mean we should have one meny entry calling \"kate --simple\" and one \"kate --full-featured\"? ;) (I hope the irony here isn't lost..)\n\n"
    author: "teatime"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "\"This would get into an interesting discussion with the camp claiming a few extra buttons in a toolbar makes for \"horrible usability\". ;)\"\n\nI'm actually in that camp *cough*. My rationale is that we shouldn't have more buttons than needed in the toolbar. Of course, we can argue what is \"needed\" and what is not. My line of thought is that we should look at the intended core-purpose of the app, and have minimium amount of buttons that allow the user to accomplish that purpose. But I digress.\n\n\"You mean we should have one meny entry calling \"kate --simple\" and one \"kate --full-featured\"? ;) (I hope the irony here isn't lost..)\"\n\nIrony is not lost :). And that's not what I meant. I meant that we could have one app (Kate) with one menu-entry. Inside that app would be different profiles (like we have in Konqueror). One of the features would be full-featured editor (what Kate is today), and another one would be simple-editor.\n\nOTOH, handling of profiles could be handled smoother than they are handled in Konqueror. Right now I have just one button for Konqueror in my Kicker. Once in Konqueror, I load the appropriate profile. And in order to do that, I have to hunt for the relevant settings in the menus. I tried setting a keyboard-shortcut for the profiles (I only really use filemanagement and web-browsing), but I noticed that it can't be done. But, I have to see what 3.4 has in store in this respect. I heard that improvements have been made."
    author: "Janne"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "> Right now I have just one button for Konqueror in my Kicker. Once in Konqueror, I load the appropriate profile.\n\nKicker -> Add to Panel -> Special Button -> Konqueror Profiles"
    author: "Christian Loose"
  - subject: "Re: KDE Usability"
    date: 2005-02-19
    body: "Well I'll be damned! I never knew about that feature! While that does solve my initial problem, maybe profiles could be handled more elegantly (than they are now) inside Konqueror?"
    author: "Janne"
  - subject: "Re: KDE Usability"
    date: 2005-02-19
    body: "Personally I think that really the only apps that should be part of the KDE release schedule are kdebase and kdelibs. Maybe some other apps would make sense as well. Much of the innovative apps (k3b, amaroK, gwenview, digikam, filelight) are in extragear. There really isn't any reason that amaroK can't be 'the' media player and exist outside of the KDE release cycle, its just a matter of communicating this to the distributions."
    author: "Ian Monroe"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "well, I think there is place for amarok and juk, an advanced and a simple music player.\n\nkaffeine WAS good (until 0.4.2), now they want to be a 'mediaplayer', so they can compete with juk and amarok (?) and play music too. so there is a playlist now (no way to disable it, it seems) and some other stupid features, and the speed is gone. well, I'd say lets just use kmplayer...\n\nabout image viewers, that's a bad one :D\n\ntexteditors, well, kate and kwrite are the same, I'd say make kwrite a bit more lean, and dump kedit, or give kedit code highlighting and dump kwrite..."
    author: "superstoned"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "> texteditors, well, kate and kwrite are the same, I'd say make kwrite a bit more lean, and dump kedit, or give kedit code highlighting and dump kwrite...\n\nPlease no code highlighting in the simple KDE text editor. Just use Kate if you want to have that feature.\n\nThe simple text editor should be as fast as possible. IMHO code highlighting would just slow it down.\n\n"
    author: "Christian Loose"
  - subject: "Re: KDE Usability"
    date: 2005-02-20
    body: "What computer are you using exactly, and what are you doing to the poor thing that you can see code highlighting in process?\n\nI mean, I used to have highlighted code on text editors on a 486, so on anything capable of running KDE, it should be invisible."
    author: "Roberto Alsina"
  - subject: "Re: KDE Usability"
    date: 2005-02-21
    body: "Try to load some 100meg SQL dump just to change one table name... you can have a 4 Ghz CPU but still I hope you don't forget highlighting activated :)))\n\nPlease, if you developers go for one complex editor and one simple editor, keep the simple one, well, as simple as possible! (notepad.exe, got the idea? :))"
    author: "Vajravana"
  - subject: "Re: KDE Usability"
    date: 2005-02-21
    body: "Well, ever tried to load a 100MB SQL dump on notepad.exe? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE Usability"
    date: 2005-02-21
    body: "LOL! :))\nOk, nice one... but I think you understand what I mean... sometimes one just want to change some character/word/phrase here and there. There surely is place in KDE for an editor wich is just a QUICK editor, with no highlightning to run, no plugins to load, no profile to choose and so on. \nJust OPEN -> FIND -> EDIT -> SAVE -> CLOSE (cut/copy/paste as a bonus :)) at the maximum possible speed."
    author: "Vajravana"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "About Kaffeine: I was of your opinion when the first 0.5 serie betas were released, but now with the final 0.5 and a little work of customization I changed my mind. Technologically Kaffeine is perfect, in features is very very good, what is definitely wrong are the default settings. But after removing that absurd \"star page\", after leaving only a toolbar and after taking it out of the system tray, it returns the lovely player we have seen in the past. Givi it a try! (and I'll try to do some bug-suggestion reporting)"
    author: "Davide Ferrari"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "Only problem with kaffeine is that seeking video can't be controled from keyboard..."
    author: "gnumdk"
  - subject: "Re: KDE Usability"
    date: 2005-02-18
    body: "Yes, from what I read, a lot of people are _very_ disappointed with the changed GUI in Kaffeine 0.5."
    author: "Carlo"
  - subject: "Re: KDE Usability"
    date: 2005-02-19
    body: "> Amarok is an excellent media player and should become the standard player, with other KDE music players removed.\n\nUh, no. You'll have my JuK when you pry it from my cold, dead fingers.\n\n> Kaffeine has demonstrated time and again that it is better than the other movie players and should be the only player provided as part of the standard KDE packages.\n\nI love Kaffeine 0.4.x. I really do.\n\nHowever, Kaffeine 0.5 has completely destroyed its usability. If KDE is to include Kaffeine, they should fork it from 0.4.3b and ignore 0.5. Hell, if I were a better coder, I'd fork Kaffeine myself."
    author: "QV"
  - subject: "MySQL??"
    date: 2005-02-18
    body: "> Support for MySQL databases. Now you can keep your Collection on a remote computer\n\nWhat do you mean? Can I have my mp3/ogg files on another computer? Do they get streamed to my work-computer then, or what?"
    author: "Amarokker :)"
  - subject: "Re: MySQL??"
    date: 2005-02-18
    body: "That's possible, but it's not what the text meant. It meant to read: Keeping your Collection database on a remote computer.\n"
    author: "Mark Kretschmann"
  - subject: "amarok vs juk"
    date: 2005-02-19
    body: "I've got a rather large music collection and have tried most of the KDE audio players.  I've found amarok to be the best by quite a margin.\n\nI don't understand how anyone can use Juk when they have a large music archive.  Juk is insanely slow when trying to just use the UI.. everytime I try to select a different task the locks up for about 10 seconds while it looks through the music database (or appears to).\n\namarok seems to have solved this problem quite well... song lookups are extremely quick and accurate.. I really think this should be included in the standard KDE install rather then Juk."
    author: "spudder"
  - subject: "Re: amarok vs juk"
    date: 2005-02-20
    body: "I have never had Juk hang at all on anything, except that it takes a bit to load, but so does Amarok. I don't know what you consider large, but I have 2400 songs so I would expect to see a slowdown if there were one.\n\nI can't imagine what problem you are running into, but it might be specific to you and you need to submit a bug report."
    author: "jameth"
  - subject: "Re: amarok vs juk"
    date: 2005-02-21
    body: "I have a collection of over 3400 songs in juk and I've never noticed any hanging or anything of that nature either.\n\nI still find myself flip-flopping between amarok and juk. Many of the features of amarok are very appealing (tracking which songs I like best and automatic lyric lookup), but I much prefer the juk model of having ones entire collection playing randomly (by song or album). It's nice to be able to push play and hear a random stream of music without having to craft it ahead of time myself. Amarok isn't well set up for this sort of playback (in fact, when I enqueue my entire collection into the playlist, amarok performs noticeably slower than juk on some operations).\n\nI also prefer juk's cover management in some ways. The Amazon covers are of consistent quality, but I have some obscure albums whose covers I was able to find with juk (it just does a google search or something similar, no?), but not with amarok.\n\nAnyhow, they're both great applications that do some things better than the other (for me, at least). I guess my ideal player would be somewhere in the middle, but I don't see that happening. :)"
    author: "Dolio"
  - subject: "Dear god, no."
    date: 2005-02-25
    body: "I have an AthlonXP 2200+ system with 768mb of RAM and amaroK is absurdly slow compiling my collection playlist of 4400+ entries, which it has to do every time I need to add stuff. It keeps me from sticking with it every time I try it. JuK, on the other hand, takes a minute or two only if I'm rebuilding the entire list, and is able to handle additions in a quick and timely manner.\n\nI also much prefer JuK's emphasis on collection first, playlist second. It makes things much easier to manage than amaroK's almost XMMS-like single/playlist first, collection second way of doing things."
    author: "akuten"
  - subject: "Ammendium"
    date: 2005-02-25
    body: "Come to think of it, I don't think JuK even takes a minute."
    author: "akuten"
  - subject: "Re: amarok vs juk"
    date: 2008-06-18
    body: "I don't have that big of a collection and juk is a bit slow, but still usable until I put something in the search, then it takes in excess of 5 minutes to do anything at all. If I cover it with another window and uncover it, it takes between 5 and 10 minutes to redraw, if I push the play button, greater than 5 minutes before it starts, when the current track ends, it takes another 5 minutes or so before it starts the next one, all the time it is using 100% CPU according to top. I have had this problem across three versions of KDE latest is 3.5.9 and across several distros and machines including a compile from scratch, it has the same symptoms on every machine I have ever tried it on."
    author: "Neil Whelchel"
---
With its new 1.2 release, KDE-based media player <a href='http://amarok.kde.org'>amaroK</a> becomes the first player to offer integrated support for <a href='http://www.audioscrobbler.com'>Audioscrobbler</a>. In close cooperation with the Audioscrobbler team amaroK developers have deployed exciting new ways to use the popular Internet service. Read on to learn about Audioscrobbler and new features in amaroK 1.2.







<!--break-->
<p>Audioscrobbler allows users to share music tastes with friends on the Internet, making use of automatically submitted song statistics. amaroK goes a step further than other media players and allows users to receive music recommendations from the site.
</p>
<p>
In contrast to competing players amaroK does not require a plugin to use this functionality. With the recently released amaroK 1.2 Audioscrobbler support comes built-in and easy to set up. Sunday's <a href='http://amarok.kde.org/index.php?option=com_content&task=view&id=46&Itemid=1'>release of amaroK</a> has <a href='http://aseigo.blogspot.com/2005/02/amarok.html'>already created buzz</a> in the KDE community.
</p>

<p>Also new in amaroK 1.2:</p>

<ul>
<li>Support for MySQL databases. Now you can keep your Collection on a remote 
computer.</li>
<li>The playlist has seen vast speed improvements.</li>
<li>10-band graphic Equalizer.</li>
<li>Many usability improvements. We have made amaroK more accessible to new users and more comfortable for power-users.</li>
<li>Automatic song lyrics display. Shows the lyrics to the song you're currently playing.</li>
<li>Support for your iPod with the all new media-browser.</li>
<li>On screen display has been revamped, now with optional translucency.</li>
<li>Theme your ContextBrowser with custom CSS support.</li>
<li>Support for the latest LibVisual library for stunning visualizations.</li>
<li>Great new amaroK icon "Blue Wolf", made by KDE artist Da-Flow.</li>
<li>Better compatibility with GNOME and other non-KDE environments.</li>
<li>Powerful scripting interface, allowing for easy extension of amaroK.</li>

</ul>










