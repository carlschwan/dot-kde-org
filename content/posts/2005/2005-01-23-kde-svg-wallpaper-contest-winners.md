---
title: "KDE SVG Wallpaper Contest Winners"
date:    2005-01-23
authors:
  - "jtheobroma"
slug:    kde-svg-wallpaper-contest-winners
comments:
  - subject: "State of the SVG support?"
    date: 2005-01-23
    body: "I assume that as there has been a competition like this that targets SVG content, that SVG support in KDE is fairly ok now? Or are the wallpapers just supposed to be scaled to the various normal resolutions and used as PNG/JPEG versions? I haven't really checked up how SVG has worked in KDE, but if wallpapers as complex as these can be directly supported it's great news.\n"
    author: "Chakie"
  - subject: "Re: State of the SVG support?"
    date: 2005-01-23
    body: "A lot of the submitted wallpapers render incorrectly in KDE3.4 because of limitations in the KDE SVG Icon engine, but I suppose judges have taken in account the correct rendering of the wallpapers. I also submitted some artworks and had to redo large parts of them to work around the aforementioned limitations."
    author: "raoulduke2"
  - subject: "Native support"
    date: 2005-01-23
    body: "KDE 3.4 will natively support SVG background images. No need to pre-render manually."
    author: "Eike Hein"
  - subject: "Re: Native support"
    date: 2005-01-23
    body: "The author of KDE SVG Gearflowers says:\n\n\"Renders fine with inkscape and sodipodi; renders kinda slowly with svgdisplay from kde3.3; renders *slowly* with librsvg 2.8.1.\"\n\nI think I'll pre-render mine, thank you.  When do I really change desktop resolutions?  I think it's smarter to include copies for various resolutions and render only if an appropriate version is not available.  \n\nBetter to waste some disk space on different size copies than give people a sluggish wallpaper."
    author: "Leo S"
  - subject: "SVG Icon Suport?"
    date: 2005-01-23
    body: "Does this mean that KDE will finally be shipping its icons in svg format as well? SVG would also benifit well for icon coloring, inaddition to only requiring one file for all sizes."
    author: "Anonymous"
  - subject: "Re: SVG Icon Suport?"
    date: 2005-01-23
    body: "All icons sources svg are already shipped in KDE sources."
    author: "annma"
  - subject: "Re: SVG Icon Suport?"
    date: 2005-01-23
    body: "> All icons sources svg are already shipped in KDE sources.\n\nMost of the SVGs are missing because we never got them off Everaldo.\n\nThere will be a new version of Crystal out in the next couple of months and hopefully we will get more SVGs then.  There will be a new SVG rendering engine in KDE 4 which should be fast and reliable enough for everyday use.\n"
    author: "Jonathan Riddell"
  - subject: "Re: SVG Icon Suport?"
    date: 2005-01-23
    body: "Sorry about my mistake. I can safely say then: \"All edu icons have svg sources!\" ;-)\n\n"
    author: "annma"
  - subject: "Keep them coming KDE artists!"
    date: 2005-01-23
    body: "It is great to see such an active artist community. Also I think Janet is doing an excellent job by organizing these contests at KDE-Look.org. Kudos to all KDE artists out there and a special thank you to Janet!!\n\nCiao'\n\nFab"
    author: "fab"
  - subject: "Missing"
    date: 2005-01-23
    body: "Where are MART-IN's http://www.kde-look.org/content/show.php?content=19342 and skullbook's  http://www.kde-look.org/content/show.php?content=19121 ?  IMO they are both better than all of these, and the visitors of KDE-Look agree.  MART-IN's excellent wallpaper is the highest-rated wallpaper on KDE-Look by 2%, and it is rated 6% higher than the contest winners!  I wish we were using KDE-Look rankings instead of a judging panel."
    author: "Spy Hunter"
  - subject: "Re: Missing"
    date: 2005-01-24
    body: "While I do appreciate the winning works, I agree with you. Personally, I prefer those two you have mentioned, and the two that got third place -- to me, they are cleaner, less obtrusive images more suitable for a wallpaper. Particularly MART-IN's."
    author: "Matthew Kay"
  - subject: "Re: Missing"
    date: 2005-01-24
    body: "Yeap. Congrats to the winners, but while I do think them did a good job, the only awarded one I'd dare to use as a wallpaper is 'Geared Globe' by Tschaeck. "
    author: "John Wallpaper Freak"
  - subject: "Re: Missing"
    date: 2005-01-24
    body: "Yes, Skullbook's (http://www.kde-look.org/content/show.php?content=19121) is the best and I'm shocked he didn't win first place. He had an obvious lead for a while on kde-look too.\n\nYou people are right, and I think that the judges were on crack when they picked these. Gear Flowers is by far the worst. We are Gear is just too busy. I like Geared Globe, but I think that MART-IN deserves first and then Skullbook after looking at them again."
    author: "Turd Ferguson"
  - subject: "Re: Missing"
    date: 2005-01-28
    body: "Too easy to manipulate. Can you say \"vote-stuffing\"? How about \"astroturfing\"? Yeah, thought so. :)"
    author: "Anonymous Coward"
  - subject: "yikes!"
    date: 2005-01-24
    body: "Those wallpapers makes me feel I just smoked crack. I mean, who (with a sane mind) would use them on their workstations? Apart from Geared Globe, which is beautiful, relaxing and elegant, they're too distracting. Probably I won't even be able to recognize the trash icon on those desktops."
    author: "A"
  - subject: "Re: yikes!"
    date: 2005-01-24
    body: "yeah, i was a bit surprised at some of the winners too =)\n\nmy favourites didn't even make it into the top four. oh well ... just shows that even being on the judging panel doesn't always make things go your way.. hehe"
    author: "Aaron J. Seigo"
  - subject: "This is absurd"
    date: 2005-01-24
    body: "The top two wallpapers on kde-look don't even place in the top three??? What the hell is going on here!?!?!?!\n\nI demand a recount!"
    author: "Mohammad Chang"
  - subject: "I think I know why"
    date: 2005-01-24
    body: "Those explicitly mentioned KDE 3.4 and that renders them useless for any other release of KDE. They probably wanted something reusable."
    author: "Alex"
  - subject: "Re: I think I know why"
    date: 2005-01-24
    body: "So does Geared Globe, in third place.  Really, if that was the only problem, I'm sure they would have contacted the artists to change it."
    author: "Spy Hunter"
  - subject: "Re: I think I know why"
    date: 2005-01-24
    body: "It is SVG so you can change the \"3.4\" text in the source of the XML file easily:\n\nsed \"s/3.4/4.0/g\" wallpaper.svg > newwallpaper.svg"
    author: "raoulduke2"
  - subject: "Re: I think I know why"
    date: 2005-01-25
    body: "Actually, if you downloaded Skullbook's work, there is no KDE 3.4 text like in the screenshot. Maybe the judges never actually tested the artwork????"
    author: "Mohammad Chang"
  - subject: "Re: I think I know why"
    date: 2005-02-05
    body: "actualy there is the 3.4 in the wallpaper. but there are two versions: one with a path with 3.4 and onw with only text. the text you can easy change in the xml file or with inkscape. but for the svgiconengine text must be convert into a path. so there is the other version.\n\nbut if this was a criterion for the judges it wasn't announced...\nso the tip for the future: announce all the criteria!"
    author: "skullbooks"
  - subject: "Re: This is absurd"
    date: 2005-01-24
    body: " I do quite agree with you that this is in fact a bit fishy.. but I think a recount is a bit too drastic though :-)\n\n I propose: Ask the authors of the two/three wallpapers in question if it would be allright to remove the refference to the KDE version number (they still look great without them), and then include them in 3.4 anyway. This way both the honnor of the judges' desitions and the choice of the userbase can can be preserved.\n\n If you have an oppinion about this, please post a \"Yes\" or \"No\"-like message below to make it easy for the maintainers to get a clear demotratic picture of the users' wishes :-D\n\n~Macavity\n\n--\n True teamwork is when the result is greater then the sum of the individuals."
    author: "Macavity"
  - subject: "Not good"
    date: 2005-01-24
    body: "From the winners list I think only \"Andes Venezolanos\" by Rendergraf and \"We are Gear\" by gg3po should recive a winner place, the other two are just ugly IMHO, and  this is even more true if compared to the missing winner submission by MART-IN. I feel his wallpaper is well done and professional, maybe the most oriented to be a default desktop.\n"
    author: "David Vignoni"
  - subject: "Re: Not good"
    date: 2005-02-14
    body: "To like your many people say the same thing, but all those that they designed wallpaper do it very well. Congratulations to all without exception"
    author: "Rendergraf"
  - subject: "I agree with the criticism"
    date: 2005-01-24
    body: "While normally I don't like this kind of criticism, this time I have to agree...the winner and the even more the second placed are ways TOO distracting. Let try to put any icon (for example, the default trashbin icon we'll have in KDE 3.4) on the 2nd placed wallpaper...you won't simply cannot find it. I really don't understand the method used to choose those papers. The only \"usability safe\" is the third placed, it's nice, slick and cool. Maybe a little too blue-ish, but KDE seems to be blue-ish :)"
    author: "Davide Ferrari"
  - subject: "Re: I agree with the criticism"
    date: 2005-01-24
    body: "Wallpapers are hard to choose. Personally, I prefer the winner and the 2nd placed wallpaper. Because I want some good looking one. I don't like to have any icon on my desktop, and I usually have lots of windows so I don't see the wallpaper when I'm working. I only see it when swithcing to antoher desktop and launching any application. In this case, the wallpaper appears a few seconds before the launch of the app.\n\nObviously, you can't say : \"hey, this one is better and this one is uglier\". It really depends on desktop use, personal taste, etc."
    author: "Romain"
  - subject: "Re: I agree with the criticism"
    date: 2005-01-25
    body: "Personal taste does not count here. It simply a matter of coherence. If you put by *default* some icons on the desktop as KDE *does*, then you have to choose a good wallpaper for this situation, and the second placed is not definitely a kind of wallpaper that let you clearly see the icons on it. If you do not put icons on desktop, well, it's *your personal* taste, but it's plent of people that use desktop to place shortcuts, and being this ecouraged by default KDE desktop settings...as I said, the wallpaper is not coherent.\nAnyway, I suppose the second classified won't never put as default, so it's not so bad :)"
    author: "Davide Ferrari"
  - subject: "What? They didn't pick ME? :-D"
    date: 2005-01-24
    body: "I am surprised that they did not pick my awesome portrait, drawn by Pilaf.  Here's the link, http://kde-look.org/content/show.php?content=19173.  I am, afterall, KDE's mascot and greatest fan.  Pilaf did a great job of translating my overall brilliance and attention-grabbing presence into the svg format.  Although I can appreciate the winners, I'm dissappointed that my clout didn't even get me an honorable mention.\n\nI'm going to dinner with Katie soon, so I'll leave you with those words.\n\nCheers,\n\nKonqui  "
    author: "Konqui"
  - subject: "Re: What? They didn't pick ME? :-D"
    date: 2005-01-24
    body: "Yeap, that one is really great. Not too distracting. We can easily set the background colour to what better fits the rest of our desktop and makes our eyes feel better. And it is one of the very few featuring you, the KDE mascot. It would only be better if it featured Katie instead.\n\n"
    author: "John Usability Freak"
  - subject: "Re: What? They didn't pick ME? :-D"
    date: 2005-01-25
    body: "Hey, thanks for the nice words :)\n\nSorry I didn't ask for your consent, Konqui. I'm glad you liked it.\n\n-Pilaf"
    author: "Pedro Fayolle (aka Pilaf)"
  - subject: "We cant always get what we want."
    date: 2005-01-24
    body: "First off we at KDE-Look decided not to use the popular vote system at the KDE-Look web site  because unfortunately it has some issues.\nhttp://www.kde-look.org/news/index.php?id=147 \n\nSecondly none of this is an exact science. In an attempt to offset this we sought to create a panel of judges that would take many factors into account. As a result we choose some judges from KDE: artist, usability and developers communities. For those of you really that interested in the voting process I am including the scoring sheet from all phases of this contest. A special thanks to all of the judges who took of their own time to offer their opinion.  Anybody interested in becoming a judge for future contests please email me. \n\nThirdly we recognize that there are many incredible wallpapers that did not make the top ten we are not going to let these sit idle. There is much planning and scheming yet to do:). \n\nFourthly Any reference to the judges using performance enhancing drugs such as crack, LSD, Pot, etc... is beyond the scope of our contests. As we do not have a drug policy nor do we plan on implementing one soon. This insures that you too can be a judge someday. hehe\n\nLastly I welcome constructive comments and helpful suggestions concerning how we can improve our contests. I feel that with each contest we hold we receive more and more high quality artwork each time. Thank you for your comments Janet Theobroma.          "
    author: "Janet Theobroma"
  - subject: "Re: We cant always get what we want."
    date: 2005-01-24
    body: "Of course there's no accounting for the taste of the judging panel; I quite understand.  And the quality of the artwork in general is indeed getting higher than ever before.\n\nIt may have issues, but to me it seems like the KDE-look vote system is quite accurate overall.  My suggestion would be to implement some rudimentary measures to prevent blatant ballot-stuffing, and then just go with it for the next contest.  That way everybody can participate in the voting easily."
    author: "Spy Hunter"
  - subject: "Re: We cant always get what we want."
    date: 2005-01-25
    body: "Its not as easy as it seems. This is the answer that I received about the voting system from the KDE-Look admin:\n\n( \"the voting system is a difficult thing. If I store every single vote for every \nsingle user and every content, it will overstrain the database. And I need a \ninterface so that user can change their votes. And to make sure that users \ndon't get multiple user accounts. So this is a bit tricky.\n\nI will work an a better solution in the future.\nVoting on the internet are allways insecure.\" )\n\nI am working on other possible solutions. I may need some help with this in the near future. But for right now it is what it is.  \nThanks Theobroma"
    author: "Janet Theobroma"
  - subject: "Sorry for the offtopic..."
    date: 2005-01-25
    body: "But could someone over at k-l.o do me a favor? I registered an account there a while ago, but accidentally mistyped my email address (.og instead of .org). This is easy to verify by going to the user info section and typing in 'illissius'. Because of this I didn't get my password, as a result of which I can't access my account to change my email address, so that I could change my password, so that I could change my email address, ...\nIf someone could either change it for me, or just delete the account altogether so I can start a new one, I would greatly appreciate it."
    author: "Illissius"
  - subject: "Re: Sorry for the offtopic..."
    date: 2005-01-25
    body: "Why don't you use http://www.kde-look.org/feedback/index.php?"
    author: "Anonymous"
  - subject: "Re: Sorry for the offtopic..."
    date: 2005-01-25
    body: "I did that weeks ago. No response or reaction as of yet."
    author: "Illissius"
  - subject: "Re: We cant always get what we want."
    date: 2005-01-25
    body: "Maybe in futur votes on kde-look should get _one_ place in jury?\n\nIn this way community will get voice but not super-decisive."
    author: "m."
  - subject: "opps!"
    date: 2005-01-26
    body: "I uploaded the wrong file! That file was missing Aaron Seigo's votes. Sorry. Here is the correct file with all the judges votes."
    author: "Janet Theobroma"
  - subject: "KDE WALLPAPER HAS BEGUN TO SUCK LARGE"
    date: 2005-01-25
    body: "Has anyone noticed that YES, KDE wallpaper has become nicer looking, but when you browse the KDE Look & Feel website that the majority of the wallpaper is BLUE or GRAY? What gives??? It's sad that on KDE I'm stuck searching for Windows Wallpaper (from some Windows theme site)because I need something with a different colour (and shocking RED should only be used as a desktop under root to warn you that you are using root - JUST IN CASE)!\n\nIf I'm not using a Windows Wallpaper I'm using a SUSE Linux Wallpaper - amazing - GREEN. \n\nWell, Congrats KDE - You enherited 4 more Blue wallpapers - Oh wait, never mind"
    author: "thesimplefix"
  - subject: "Re: KDE WALLPAPER HAS BEGUN TO SUCK LARGE"
    date: 2005-01-25
    body: "There are so many places to hunt for wallpapers, you can't even count them. My favourite one is deviantart. Just in case...\nbtw. The standard XP-wallpapers are fairly well done, but boring... You'll surely find something better on the net..."
    author: "Thomas"
  - subject: "Re: KDE WALLPAPER HAS BEGUN TO SUCK LARGE"
    date: 2005-01-25
    body: "Good news! 'Geared Globe' and 'We are Gear' are both transparent wallpapers. So you can make these wallpapers ANY color you want by changing the colors in the Background section of the Control Center. I have even included a .png green version of \"Geared Globe\" just for you! "
    author: "Janet Theobroma"
  - subject: "Re: KDE WALLPAPER HAS BEGUN TO SUCK LARGE"
    date: 2008-05-22
    body: "All your base now belong to us."
    author: "Darth Vader"
  - subject: "Re: KDE WALLPAPER HAS BEGUN TO SUCK LARGE"
    date: 2008-05-22
    body: "SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!\nSPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!\nSPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!SPAM!"
    author: "SPAMMER"
  - subject: "Weird way..."
    date: 2005-01-26
    body: "Ugh.\nI'm a little bit shocked. I don't like any of those wallpapers, and I find all of them (meaby beside Geared Globe) both, too messed and semi-proffesional.\nThe choice ignores users voices (where the winners are 19342 and 19121 - both great IMO) and does not correspond ANYHOW with previous way of visual concept that was used for KDE 3.\nAll visual aspects of KDE 3.0, 3.1, 3.2 and 3.3 were more and more smooth, crystal, elegant, fashion, with small but bright icons and silver, gray or blue background, all background ware clean, modern and stylish.\n\nNow, we'll have background full of objects around, very distracting for me, and hardly \"fashion\". :(\nAlso it does not correspond at all with proposal of Control Centre redesign (id 19139).\n\nNo offence to authors of winner backgrounds. They made their work, but IMO it's just not a good decision for KDE.\nBad choice, bad way :("
    author: "gandalf"
  - subject: "Re: Weird way..."
    date: 2005-01-27
    body: "I am shocked too! but for a different reason! Most of these comments offer nothing constructive or helpful! It occurred to me after reading all of these comments as a whole that if different designs had won there would be comments such as Ugh. Nothing new here same old KDE art man when are we going to get something fresh or new piss on this or piss on that...\n\nYou don't have to use the wallpapers you don't like! You can use whatever ones you do like. \n\nThis contest rocked and I think it was wildly successful! 80+ wallpapers in 21 days! SVG Wallpaper support for KDE! Some extremely killer wallpapers! No matter what your taste there is something for everyone! Did I mention it costs you nothing.     HOLY fill in the blank!   \n\nThe second thing that occurred to me is this, all of you should be judges in upcoming kde-look contests you are totally qualified and in some cases over qualified ;) contact theobroma she invited you to do this!\n\nHere's to free speech, free OS and apps and of course free beer!\nCheers\nfiniteman  \n\n       "
    author: "finiteman"
  - subject: "Re: Weird way..."
    date: 2005-01-27
    body: "First, I did not write a suggestion, but a review. Review of the choice. Sorry it was \"not constructive\"...\n\nI gave my suggestions, but it's not important what I do like, or what I do not like. It's not subjective that in winner wallpaper there is a lot of objects around, a lot \"happens\" inside the image, right?\nAnd I wrote that it is a difference to previous line of art for KDE 3. I think that it's also objective to say that such \"full of items\" wallpaper will be more distractive to user who will want to find icons on his desktop than subtle faded, one color/two color background.\n"
    author: "gandalf"
  - subject: "Re: Weird way..."
    date: 2005-01-29
    body: "My comment was not actually specific only to yours. Suggestion or review your comment was just another rehash of negetivity that has prevailed on this news item. Does this make you wrong or right? Neither, I think it is awesome that you offered your comments and equally awesome that I can make mine. I think that a lot of you have strong opinions and that if you are not contestants you would be excellent judges for future contests. No joke I think people like you would raise the bar. Contact Theobroma. \n\nfiniteman"
    author: "finiteman"
---
The <a href="http://dot.kde.org/1103326589/">KDE SVG Wallpaper contest</a> has been finalized. It was not an easy task as there was so much incredible artwork.  The judges sorted through all 86 entries and choose the top four and the winners are...













<!--break-->
<p>Tied for third place is '<a href="http://www.kde-look.org/content/show.php?content=19252">Geared Globe</a>' by <a href="http://www.kde-look.org/usermanager/search.php?username=Tschaeck"> Tschaeck</a> and '<a href="http://www.kde-look.org/content/show.php?content=19085">Andes Venezolanos</a>' by <a href="http://www.kde-look.org/usermanager/search.php?username=Rendergraf">Rendergraf</a>. </p>
<img src="http://static.kdenews.org/binner/svg-wallpaper-contest/3640957_28b0913b66_m.jpg"/>

<img src="http://static.kdenews.org/binner/svg-wallpaper-contest/3640956_c1dda68aec_m.jpg"/>

<p>The second place winner is '<a href="http://www.kde-look.org/content/show.php?content=19524">Gear Flowers</a>' by <a href="http://www.kde-look.org/usermanager/search.php?username=tetromino">Tetromino</a>. Tetromin has won a T-shirt featuring his wallpaper provided by <a href="http://www.cafepress.com/revelinux">Revelinux 
Gear</a>.</p>

<img src="http://static.kdenews.org/binner/svg-wallpaper-contest/3640954_1a948f4809_m.jpg"/>

<p>And the 1st place winner of the KDE SVG Wallpaper Contest is...
'<a href="http://www.kde-look.org/content/show.php?content=19064">We are Gear</a>' by <a href="http://www.kde-look.org/usermanager/search.php?username=gg3po">gg3po</a>.  As the 1st winner gg3po gets to choose between a nVIDIA GeForce FX5700LE Video Card or a 120GB Hard Drive provided by <a href="http://www.corefunction.com/">Corefunction.com</a>.</p>

<img src="http://static.kdenews.org/binner/svg-wallpaper-contest/3640958_bbda1a8852_m.jpg"/><br><br>

<p>Not only will the winning wallpapers be included in KDE 3.4 they will also will be made into T-shirts on sale at <a href="http://www.cafepress.com/revelinux/480348">Revelinux 
Gear</a> with all the proceeds going to support <a href="http://kde-look.org/">kde-look.org</a> and <a href="http://kde-apps.org/">kde-apps.org</a>.</p>

<p>Thank you to all the judges who had the hard job of picking the top four wallpapers.</p>

<p>To all the artists that entered this contest: THANK YOU!! We couldn't believe the amazing artwork that was made in such a short period of time.</p> 

<p>Interested in what the kde-look community is up to this month? Then check out the <a href="http://www.kde-look.org/index.php?xcontentmode=43">latest entries</a> to our <a href="http://www.kde-look.org/news/index.php?id=152">KDE-Look T-Shirt Contest</a>.</p>





