---
title: "Kubuntu 5.04 Released"
date:    2005-04-08
authors:
  - "jriddell"
slug:    kubuntu-504-released
comments:
  - subject: "Kubuntu 5.04 Final Screenshots by OSDir.com"
    date: 2005-04-08
    body: "<a href=\"http://shots.osdir.com/slideshows/slideshow.php?release=306&slide=29&title=kubuntu+5.04+final+screenshots\">http://shots.osdir.com</a>\n\n"
    author: "Chris"
  - subject: "KDE UserLinux aka Kalyxo"
    date: 2005-04-08
    body: "Hi,\n\nare there still people involved in the Kalyxo project ?\nIf so, I think they should join forces and jon the kubuntu project.\nIMHO UserLinux is dead, actually it never was alive.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-08
    body: "Didn't you ask exactly the same (and got answers) in a mailinglist already?"
    author: "ac"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-08
    body: "Yes, but I didn't get answers, at least they didn't arrive in my mailbox. So I thought the lists are dead.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-08
    body: "I got the answers.  Check again."
    author: "ac"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-08
    body: "And instead of sharing the info, you have to be a dick."
    author: "Joe"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-10
    body: "I don't think I have to fulfil the duty of copying a mailinglist archive here. If you know what this is about you can easily look it up yourself at the right place. (If not say so and I'll happily point you to some KDE mailinglist documentation page where you can get all neccessary info and more.)"
    author: "ac"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-11
    body: "..and then you go on being a dick, defending your right to be one. Awesome. You could have used the same amount of keystrokes to actually say something useful."
    author: "thatguiser"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-11
    body: "I know, just like you do..."
    author: "ac"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-09
    body: "I replied to you personnally though... A small OT discussion started, etc.\nThe lists are not dead. It's just that you're not subscribed at the right\nplace, we changed list with the project rename (Kalyxo) since some users where\nconfused and asking things on list that was in fact directed to the debian\ndevelopers. You were not even aware of this...\n\nDon't take this as a personal attack, but please stop to ask people working\non Kalyxo to go working on Kubuntu. That's a different kind of work currently,\nit's sometimes even complementary and some things in Kubuntu comes from our\nwork. But well... you've hardly been involved enough in Kalyxo during the past\nyear to notice this.\n\nI have to admit that we're maybe not communicating enough outside the core\npeople involved. That's probably our biggest mistake, but well... we dislike\nto claim we have something when it's not good enough (following our own\ncriteria).\n\nAs a side note, I'd like to add that (K)ubuntu is mostly a Debian fork (even\nif it's the most respectful Debian fork I know, some of their improvements\ngoing back to Debian). Kalyxo on the other hand is trying to improve the\nsituation directly for people using official Debian and KDE. We're trying to\nimprove both projects and their integration, not to fork one of them.\n\nRegards.\n"
    author: "ervin"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-13
    body: "well, what about kalyxo.org? if my name-server\nisn't totally broken your domain has vanished.\nwhere is your staging archive now located?\n\nthanks for your good work in the past though!\njjm"
    author: "jjm"
  - subject: "Re: KDE UserLinux aka Kalyxo"
    date: 2005-04-13
    body: "See http://mail.kde.org/pipermail/kalyxo-devel/2005-April/000161.html"
    author: "cm"
  - subject: "Kubuntu, Debian and KDE 3.4"
    date: 2005-04-08
    body: "It used to be that Debian was very fast with having new kde releases available in the 'unstable' branch. This time it seems to be taking long for KDE 3.4 to make it into 'unstable'. It would not surprise me if some Debian users are now Kubuntu users in part because of this.\n\nWhat are the benefits of running Kubuntu over a Debian system?"
    author: "ac"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-08
    body: "KDE 3.4 won't be in Debian unstable until sarge is released.\nSee http://wiki.debian.net/?Kde34 for more info. You will also find 3.4 packages there."
    author: "Henning"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-08
    body: "Doesn't seem to include KDE 3.4.0 for X86_64.\n\n\n- Roey"
    author: "Roey Katz"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-08
    body: "That means next two years without new kde... That's why I'm after 2 years of using Debian switched to Kubuntu. Debian sucks with their releases. Most of users use unstable / testing branch or Woody with backports."
    author: "viru"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-10
    body: "Exactly..\n\nDebian is losing interest because they don't bother to put newer (and more stable) packages of popular programs into their releases. It has been 2 years and still there is no updates to KDE. Pathetic. There have already been 5 major releases of KDE in that time period and still not a SINGLE update. They don't even plan on putting KDE 3.4 into the next release. And KDE3.4 is the latest KDE!\n\nWhy would they do that? It doesn't make sence at all. There is no reason to be releasing software with KDEs's several versions older than what is currently out and currently also more stable.\n\nmy 2c"
    author: "ac"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-19
    body: "You don't have the slightest idea of what STABLE is when you're talking about Debian, STABLE contains stuff that hasn't crashed for what is it 2 years???\nKubuntu and KDE3.4 ran for 5 minutes on my system before Konqueror crashed on me."
    author: "M\u00e5rten W"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-08
    body: "Well, I am currently in exactly the situation you describe. I have been running Debian testing/unstable for over a year, and tried Ubuntu Warty once when it first came out. Decided I really _did_ like KDE that much better, and booted back to Debian within the hour. Now that Kubuntu is out, I installed it this morning, and have been enjoying it ever since.\n\nThe Good:\n* It's so similar to Debian, I can copy config files, apt-get, change locales and so forth just like before.\n* It's wicked fast. My Debian seems bloated in comparison, but that might be because of KDE 3.3 or because I let it grow bloated (remember, everyone creates his own Debian)\n* It was very easy to install and includes almost all software I regularly use right on the CD. Exceptions: German localization, KDE development stuff, ethereal, xosview\n\nThe Bad:\n* Not many repositories as of yet. My university (www.rwth-aachen.de) alone has a couple of Debian repos.\n* MPlayer had to be compiled from source (Marillat package had dependency problems)\n\nThe Undecided:\n* It doesn't support as many architectures as Debian does. This is however no serious problem, as Kubuntu isn't really the OS you'd want on that big-iron IBM or that 33 MHz PDA anyway.\n\nJust my EUR 0,02 :)\nAndreas"
    author: "Andreas Steffen"
  - subject: "I'm staying with Debian (unstable)"
    date: 2005-04-09
    body: "These cool new Debian-based distros come and go, but Debian itself remains.\n\nWhen MEPIS/Kubuntu/WhateverLinux runs out of steam, it may be difficult to migrate back to Debian proper."
    author: "Martin"
  - subject: "Re: I'm staying with Debian (unstable)"
    date: 2005-04-10
    body: "\"but Debian itself remains.\"\n\nNot to sound like a troll, but debian remains in the stone age. KDE2.2 is their latest stable release. That is FIVE major KDE versions ago. FIVE.\n\nSince testing and unstable aren't releases anyway, there is no guarantee you can keep track of debian proper while using them without breakage. So what is the downfall of using kubuntu (which is just a stabalized tracking of Sid)? Kubuntu is just the same that you and I do with Debian Unstable in our own homes. Only it is done out in the open with cooperation. This allows people who wish to track Sid on their computers to track Sid more reliably, and with better support, performance, and features. Debian Sid still doesn't have 3.4. Why not? They said that 3.4 will not go into sid until the next version is released. Why not? 3.4 is proven better and more reliable than 3.3. So why would they release an OLDER and BUGGIER version of KDE into an operating system which isn't even released yet?\n\nI'm a big fan of Debian. And I use it. No disrespect for Debian. It's a great distribution. But it won't remain for long if it keeps being outstripped because of their stubbornness to not release reasonably modern desktops.\n"
    author: "ac"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-09
    body: "KDE 3.4 is waiting for next stable release. Add\ndeb http://pkg-kde.alioth.debian.org/kde-3.4.0/ ./\nto your sources.list. This repository is maintained by Debian KDE maintainers."
    author: "Petr Balas"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-10
    body: "Thanks for the tip. Hopefully adding that won't generate any problems when in the future kde 3.4 will come to Sid?"
    author: "Petteri"
  - subject: "Re: Kubuntu, Debian and KDE 3.4"
    date: 2005-04-11
    body: "Shouldn't be a problem.. The packages you are adding are the ones that will eventually go into Sid."
    author: "Leo Spalteholz"
  - subject: "Knoppix"
    date: 2005-04-08
    body: "I would like to see Kubuntu merge with Knoppix. Kubuntu has a clean desktop, Knoppix leads with speed and hardware recognistion.\n\n(I know Knoppix is a LiveCD, but can installed on hard disc as well) \n(I know Kubuntu is a distro but there is a sloooow LIVE-CD as well)\n\n"
    author: "hannes g\u00fctel"
  - subject: "Re: Knoppix"
    date: 2005-04-08
    body: "try slax 5.0 and you know how fast KDE could be... :-)\n"
    author: "anonymous"
  - subject: "Re: Knoppix"
    date: 2005-04-08
    body: "Please, please, get a clue about how hard this would be.\nPost the pie-in-the-sky stuff somewhere else, like on osnews"
    author: "Joe"
  - subject: "Major issues..."
    date: 2005-04-08
    body: "A small writeup covering Kubuntu problems is here:\n\nhttp://linux.slashdot.org/comments.pl?sid=145437&cid=12176562\n\nTo paraphrase a saying, \"Next Year on the Desktop!\"\n\n- Roey  "
    author: "Roey Katz"
  - subject: "Re: Major issues..."
    date: 2005-04-08
    body: "Sounds all Ubuntu general (except Python bindings), not Kubuntu-specific."
    author: "Anonymous"
  - subject: "Re: Major issues..."
    date: 2005-04-08
    body: "Hey, if Kubuntu bills itself as a distribution[1], with liveCD and all, then it's all the same to me... \n\n\n- Roey\n\nreferences:\n1. \"distribution\" in this sense meaning a GNU/Linux distribution like Debian or Fedora, not in the sense of \"KDE distribution\". "
    author: "Roey Katz"
  - subject: "excellent"
    date: 2005-04-09
    body: "I tried it last night (the live version) and got really impressed!\nAfter booting there are few menu-driven questions and in a couple of minutes (yes, my cd-drive is so slow) you have it up and running.\n\nIt looks clean, polished, it's fast and has the best selection of kde apps available around. Amarok startd up immediately and was configured by pressing next->next->next->done :-). Kopete looks gorgeous along with all other kde apps, including k3b, kaffeine, kolourpaint, ..., kpdf (ehm.. I'm a little biased here :-).\n\nEvery chip in my hardware was recognized and set up appropriately, even the sound was crisp and at a good default volume even without KMIXing it :-).\n\nNever had a single crash or problem, so what can I say.. If I wasn't so addicted to compiling sources I'd immediately left gentoo for that. Good work Jonathan!.\n"
    author: "eros"
---
<a href="http://www.kubuntu.org">Kubuntu</a> has <a href="http://www.kubuntu.org/hoary-release.php">announced its first release</a>.  Kubuntu 5.04 includes KDE 3.4 and the finest pick of leading Free Software desktop programmes.  Live and install <a href="http://kubuntu.org/download.php">CD images are available for download</a> for x86, PowerPC and AMD64, please download via Bittorrent if possible.  OSDir carries some <a href="http://shots.osdir.com/slideshows/slideshow.php?release=306&slide=1">screenshots of the release</a>.
<!--break-->
<p>"<em>With Ubuntu we had the opportunity to make the first class distribution that KDE deserves, in Kubuntu we have now done that</em>" said developer Jonathan Riddell.</p>

<p>Ubuntu's founder Mark Shuttleworth commented "<em>KDE is a profoundly important part of the open source world - it's popular with both end users and developers and is one of the key pieces in the battle for the future of the desktop. I'm very excited to support this community effort to get KDE into Ubuntu.</em>"</p>





