---
title: "dataKiosk Adds Advanced Search and Data Entry"
date:    2005-06-16
authors:
  - "manyoso"
slug:    datakiosk-adds-advanced-search-and-data-entry
comments:
  - subject: "spot on"
    date: 2005-06-16
    body: "I think datakiosk is one of the better approaches to building simple fast interfaces to data.  I think the only thing that could make it better would be the ability to interface beyond SQL.  I would love to drag and drop an LDAP editor :)\n\nIdeally Qt4 will give even more power with the new model view classes."
    author: "ian reinhart geiser"
  - subject: "Re: spot on"
    date: 2005-06-16
    body: "I'd like to see embedded ruby, python or kjsembed to do input validations, virtual fields, simple workflow rules and dcop interfacing (e.g. if you had an email column in your database).  Hopefully datakiosk will continue to grow in functionality, but right now it already has a really useful feature set.  Thanks manyoso."
    author: "ltmon"
  - subject: "Re: spot on"
    date: 2005-06-16
    body: "Don't worry it is coming.  I plan on fully integrating a scripting layer.  KJSEmbed will likely be the first engine and the Virtual Fields will then be calculated with JavaScript.  Same goes for workflows and I plan on having a lockdown feature which will blow away MDB/MDE files.\n\nOh, and input masking is already in svn.  I just started with that the day after the 0.7 release.\n\nMaybe by KDE4 Ian will have figured out how to create a generic search plugin for LDAP :)\n\nAnyway, I am glad you like."
    author: "manyoso"
  - subject: "Congrats! it looks Good!"
    date: 2005-06-16
    body: "It is feature rich! Could it also work with xbase? http://www.geocities.com/tablizer/xbasefan.htm\n\n"
    author: "fast_rizwaan"
  - subject: "Re: Congrats! it looks Good!"
    date: 2005-06-16
    body: "It will work with any databast that has a Qt SQL driver plugin.  I'm not aware of any Qt driver for XBase, but Trolltech has docs on how to create new drivers and there are already XBase c++ libs out there to interface with... I think ReKall has such a driver or something."
    author: "manyoso"
  - subject: "Re: Congrats! it looks Good!"
    date: 2005-06-16
    body: "Rekall and Knoda both use xbsql. Horst Knorr made some fixes to it and named it xbsql-hk_classes\u00b9. Would be nice, when you would use this library, if you think about suporting it. Users want shared libs. Need to push the Rekall guys in the same direction, too.\n\n\n[1] http://sourceforge.net/project/showfiles.php?group_id=23940"
    author: "Carlo"
---
The competition for KDE's best database application is heating up with the <a href="http://web.mit.edu/~treat/Public/datakiosk-0.7-announce.txt">recent release</a> of <a href="http://extragear.kde.org/apps/datakiosk/">dataKiosk</a> version 0.7. Virtual fields, custom SQL searches, a full blown data entry form with dynamically created data aware widgets and data reports that can be sorted, grouped and bound to tables/fields/searches are among its new features.  A <a href="http://web.mit.edu/~treat/Public/datakiosk-0.7-changelog.txt">detailed changelog</a> and <a href="http://web.mit.edu/~treat/Public/datakiosk-0.7.tar.gz">release tarball</a> are available.









<!--break-->
<div style="float: right; width: 300px"><a href="http://static.kdenews.org/content/datakiosk/datakiosk-relation-combo.png"><img src="http://static.kdenews.org/content/datakiosk/datakiosk-relation-combo.png" width="300" height="200" /></a><br />dataKiosk editing a table</div>

<h4>What can this version of dataKiosk do?</h4>

<p>dataKiosk can provide a fully featured data entry application tailored to any SQL database in a matter of minutes.</p>

<h4>What does fully featured mean?</h4>

<ol>
<li>dataKiosk uses Trolltech's Qt SQL module which includes drivers for: MySQL, PostgreSQL, Oracle, MS SQL Server,IBM DB2, ODBC, SQLite, Interbase and Sybase.
<ul>
<li>A single project can even include multiple tables from multiple databases.</li>
<li>dataKiosk 0.7 ships with identical sample projects for both MySQL and PostgresSQL.</li>
</ul>
</li>

<li>Integrated Basic, Advanced, and Custom SQL query modes that provide seamless searching capabilities no matter how simple or advanced your query needs.
<ul>
<li>Basic searches are provided automatically via a search bar attached to every datatable.</li>
<li>Advanced searches are available via a query editor allowing you to specify the tables/fields/operators and values you wish to narrow your search.</li>
<li>Custom SQL searches allow you to edit the actual SQL used to generate your search.</li>
<li>Advanced and Custom searches can be saved with the project to be used again and again.</li>
<li>You can even specify parameters for your searches to be prompted from the user. Once the search is invoked a parameter prompt dialog pops up with data aware widgets asking the user for the appropriate parameters.</li>
</ul>
</li>

<li>An optimized data entry form that automatically configures its data aware widgets to the fields in your table including relation editors with text completion.
<ul>
<li>The data entry form can keep track of the state of the current record with a colorbox that surrounds the form indicating whether unsaved modifications have been made to the current record.</li>
<li>The data entry form has navigation buttons and configurable keyboard shortcuts for optimum speed of entry.</li>
<li>The relation combo editor has the ability to constrain itself to other values in the editor form.  For example if I have two relation editors that point to the street and city values of an address, the street relation editor can be constrained to only display those streets from the current city value.</li>
</ul>
</li>

<li>Customizable data tables that automatically include a search bar at the top which converts human language queries into SQL and filters accordingly.    
<ul>
<li>Data tables can be configured to display or exclude fields with customized labels.</li>
<li>The fields order and many other properties are also configurable.</li>
<li>Foreign key fields can be marked as such and configured to display another field via the foreign key relationship.</li>
</ul>
</li>

<li>The ability to relate data tables with master-detail, one-to-one, one-to-many and many-to-many relationships.
<ul>    
<li>Selecting a particular record from the master table will constrain the child table's records and so on.</li>
<li>The navigation buttons and keyboard shortcuts are sensitive to the relationships between fields.  For instance if you are currently in a child datatable that has a one-to-one relationship with its parent and you navigate to the next record, you will actually navigate to the parent's next record.</li>
</ul>
</li>

<li>Integrated data reports that can be configurably bound to any set of tables, fields and searches in your project.
<ul>    
<li>The reports can be associated with a particular saved search or they can be run against the current searches of their respective datatables.</li>
<li>The reports can be configured to sort and group according to the set of fields associated with it.</li>
<li>The reports automatically create a JOIN SQL statement combining the associated search (whether it is an Advanced search or a Custom SQL query) and the set of tables and fields associated with it.  The resultant data set is then used to generate an XML file and fed to <a href="http://www.koffice.org/kugar">Kugar</a>.</li>
<li>The reports can even display virtual fields. (See Below.)</li>
</ul>
</li>

<li>Ability to specify the default sorting and grouping of your tables and reports. See above.</li>
<li>Virtual Fields can be added to any data table (and accordingly to any data report) and configured to calculate a user specified equation.  Every field in every table can be used as a variable in the virtual field along with constant variables like: current date, current time, a constant string or a constant number.</li>

<li>Clipboard manager modeled after the clipboard manager found in MS Excel or Access.  You can use this to copy disparate data into your editor form with tab navigation.</li>

<li>In short, INSERT/SELECT/DELETE and create a user friendly interface to any SQL database with a Qt SQL driver to your heart's content.</li>
</ol>









