---
title: "Security: Advisories for kdelibs and Kommander"
date:    2005-04-22
authors:
  - "binner"
slug:    security-advisories-kdelibs-and-kommander
comments:
  - subject: "kde 3.4.1?"
    date: 2005-04-22
    body: "kde 3.4.1? when?"
    author: "Mathias"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "When it's done :) But here some Screenshots of current CVS."
    author: "Screenshots"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "... and a last one ..."
    author: "Screenshots"
  - subject: "Not related with kde security ..."
    date: 2005-04-22
    body: "What font are you using in your desktop?"
    author: "...."
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-22
    body: "... the default ones shown + AA enabled ..."
    author: "Screenshots"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-22
    body: ":) Where did you get a version of Helvetica that can be anti-aliased?\n\nBitmapped Helvetica still plagues me to this day."
    author: "Dolio"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-22
    body: "magic :)"
    author: "Screenshots"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-22
    body: "Would you explain this \"magic\" for inquiring minds?"
    author: "stumbles"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-23
    body: "He prolly hasn't Helvetica in his fonts paths or has it aliased, KDE tends to go with the nearest choice when a font is not available, I think.\n\nI generally install M$ webfonts and comment out any other fonts paths in my /etc/fonts/fonts.conf. If KDE was set to use Helvetica, it will now use Verdana instead, and so on."
    author: "Pilaf"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-23
    body: "I do have XOrg installed with Bitstream fonts (the stuff that comes with it) and the original fonts from WindowsNT (not the corefonts as found on Sourceforge). These are Helvetica AA fonts that you see on the Screenshots."
    author: "Screenshots"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-23
    body: "Macintoshes come with truetype Helveticas but you can also buy it from Linotype."
    author: "Robert"
  - subject: "Re: Not related with kde security ..."
    date: 2005-04-25
    body: "A simple solution to the Helvetica problem *used* to be to install the URW clones as the Adobe fonts, but with FontConfig, there isn't a file available for FontConfig.  Perhaps KDE could provide this.\n\nYou can install the Helvetica font that comes with the Adobe Acrobat 3.x for Windows (get the AFM files from Adobe FTP), but that Helvetica is an old font that doesn't have a Euro symbol and I don't know if doing this is 100% legal so distros are not going to include it.\n\nSo, the question is: Why do we persist in using \"Helvetica\" and \"Courier\" as the default rather than the FontConfig generic font names: \"sans-serif\" & \"monospace\".  This would appear to be an obvious solution, or are there some systems that don't use FontConfig?\n\nAlternatively, KDE could automatically substitute \"Arial\" for \"Helvetica\", Courier New for \"Courier\", & \"Times New Roman\" for \"Times\".  Perhaps there should be a way to turn this off, but for many users it would solve some problems.  Or, this could be added to the FontConfig configuration files.  Perhaps a KDE GUI to edit your FontConfig: \"/etc/fonts/local.conf\" file would help.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "What windeco are you using? Is it the default Plastik?"
    author: "Anonymous"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "Yes."
    author: "Screenshots"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "Its taking a step backwards IMO. \n\nI liked the old style much better. "
    author: "Anon"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "> I liked the old style much better.\n\nI didn't.  I'm glad Plastik is default now.\nAnd I got the impression that a lot of people agree with me."
    author: "cm"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "You're right, of course.  Plastik has been generally well-received.  And a popular theme makes a decent choice for default, although I'd say a boring inoffensive one is better, but that's for distros to decide.\n\nBut I have to say I agree with the original complainer.  I tried to use Plastik, really I did.  But it's ugly, busy and takes up a lot more screen real estate than necessary.  This is all my subjective opinion, of course, but I've found it's a generally consistent opinion of those who don't like Plastik.  Basically the problem is that it's Windows XP's \"Luna\" theme, mercifully without the abrasive color scheme.\n\nI actually think ThinKeramik is great, and Keramik is almost as good (but a little busy).  It's inobtrusive, nondescript, and out-of-the-way, and functional.  Again, my opinion.\n\nI think it's a divide between those who don't want window decorations to distract from the window contents, and those who...well, want flashy window decorations.  No offense intended.\n\nAs long as KDE still offers plenty of theme choices aside from the default, who cares?  Distros often change the default to their own anyway.\n"
    author: "ac"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "> I tried to use Plastik, really I did. But it's ugly, busy and takes \n> up a lot more screen real estate than necessary. \n\nFunny, that's what I always say about Keramik. \n\n\n> As long as KDE still offers plenty of theme choices aside from the default, \n> who cares? Distros often change the default to their own anyway.\n\nACK.\n\n"
    author: "cm"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "It looks like they are still using that really bad blue colour on the information screens. See http://bugs.kde.org/show_bug.cgi?id=100448 for more information, and please vote on it, it was much better in the early 3.4 betas."
    author: "Ian Ventura-Whiting"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "I like the information screens they look quite cool."
    author: "Screenshots"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "I am still kind-of disappointed in KDE. I hope I am wrong in thinking that Konqueror photo is of the default settings. IMNSHO, first, all those icons after and including the printer icon should be removed, and the location toolbar put or even merged with the apace after the back/forward bar and the KDE animatied logo.\n\nSecond: The fonts look \"thick\" and \"big\". Why? Is the insane behavior of the toolbar now tamed? Sometimes visiting some sites would make those toolbars un-dockable (sp), and Konqueror would on several occasions, forget its toolbar settings. A [useful] bug report was not easy to file since reproducing this behavior was not possible. It happens at random!\n\nSince KDE is still being fine tuned, I'll keep my fingers crossed for now."
    author: "charles"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "> IMNSHO, first, all those icons after and including the printer icon should be\n> removed, and the location toolbar put or even merged with the apace after the\n> back/forward bar and the KDE animatied logo.\n\nI exactly like the icons where they are and the icons after the printer icon should stay where they are and of course the location toolbar is right as well. It's exactly how I want my desktop to look like.\n\n> Second: The fonts look \"thick\" and \"big\". Why?\n\nBecause I have chosen them, they are healthy for my eyes.\n\n> Since KDE is still being fine tuned, I'll keep my fingers crossed for now.\n\nMy Screenshots are in no way representative for the KDE project, these are my Screenshots."
    author: "Screenshots"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "> I am still kind-of disappointed in KDE. I hope I am wrong in thinking that Konqueror photo is of the default settings.\n\nYou're disappointed with something what you don't use?"
    author: "Anonymous"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-23
    body: "It seems unlikely that your problems with Konqueror are random.  They are perhaps unpredictable and difficult to analyze, but that is true about most complicated systems.\n\nYou should fill out a bug report if you think you've found a bug.  Hopefully other people that have the same problem will add to it, and some commonality can be discovered allowing a fix or work around to be developed."
    author: "brian"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "Depends on when the switch to Subversion will happen."
    author: "Anonymous"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "Anyway I think that this updates will be backported to KDE 3.4.0 by distro-makers. At least, Gentoo has already done it, don't know others. (yesterday I got a kdelibs-3.4.0-r2 update)"
    author: "Davide Ferrari"
  - subject: "Re: kde 3.4.1?"
    date: 2005-04-22
    body: "Patches are already available thanks to the KDE developers, the distro-makers has only to apply the patches, rebuild and release. "
    author: "Morty"
  - subject: "KUBUNTU Unusable for Desktop"
    date: 2005-04-22
    body: "Much hype made me try kubuntu 0.5.4, but to my disappointment i found that there are many things lacking in it. I say slackware 10.1 with KDE 3.4 is the best!\n\nKUBUNTU:\n-------\nPROS:\n\n1. tighter integration of KDE with base system.\n2. KDM Theme.\n3. Lipstik style. Good Look and Feel. Lipstick rocks.\n4. few and most useful applications (k3b,amarok, gwenview, openoffice, etc.)\n5. automatic configuration after installation.\n\n\nCONS:\n\n1. configuration applications missing (alsaconf, adsl-setup, xorgsetup)\n2. and many nifty small applications like links, lynx, etc. (when X11 is crashed how will you access internet?)\n3. not fully multimedia ready (libdvdcss not installed DVD Playback not possible)\n4. Too less Default KDE applications installed.\n5. nice applications like karchiver are not part of the KDE centric OS :(\n\n\nLack of Configuration tools and GCC/G++ are too much a trouble. I'll stick with Slackware!"
    author: "Fast_Rizwaan"
  - subject: "Re: KUBUNTU Unusable for Desktop"
    date: 2005-04-22
    body: "Is there a single on-topic comment on the Dot from you?"
    author: "Anonymous"
  - subject: "Re: KUBUNTU Unusable for Desktop"
    date: 2005-04-22
    body: "There doesn't seem to be any on-topic comments here.\n\nOh, yes, security is good.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KUBUNTU Unusable for Desktop"
    date: 2005-04-22
    body: "The \"3.4.1\" thread refers to the story text."
    author: "Anonymous"
  - subject: "Re: KUBUNTU Unusable for Desktop"
    date: 2005-04-23
    body: "Hmmm... your posting here indicates that you're aware of existence of the internet, in its' widest sense - you know, http, ftp... so, missing packages are easily downloaded and installed via - guess what - internet... Distro that comes on a single CD is not to be expected to have all the packages YOU want. Btw, gcc and make are on the CD, along with kernel-headers and such - if you launched e.g. kynaptic to check available packages, you would see that you could install them with a few mouse clicks... Slackware users - rrright... "
    author: "Petar"
---
Two <a href="http://www.kde.org/info/security/">security advisories</a> have been issued by the <a href="mailto:security@kde.org">KDE Security Team</a> which both affect KDE 3.2 up to and including KDE 3.4: <a href="http://www.kde.org/info/security/advisory-20050421-1.txt">kdelibs</a> does not properly perform input validation for image files. <a href="http://www.kde.org/info/security/advisory-20050420-1.txt">Kommander</a> executes without user confirmation data files from possibly untrusted locations. These issues will be fixed in KDE 3.4.1, for older KDE versions <a href="ftp://ftp.kde.org/pub/kde/security_patches">patches are available</a>.
<!--break-->
