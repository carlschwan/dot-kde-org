---
title: "KDE CVS-Digest for March 11, 2005"
date:    2005-03-13
authors:
  - "dkite"
slug:    kde-cvs-digest-march-11-2005
comments:
  - subject: "Wow..."
    date: 2005-03-13
    body: "Wow: \"Sunday 13/Mar/2005, @02:48\". Thank you Derek!"
    author: "MacBerry"
  - subject: "khtml regression tests"
    date: 2005-03-13
    body: "Derek mentions khtml regression tests in this digest. If someone is interested in looking at those tests they can be viewed at http://webcvs.kde.org/khtmltests/regression/tests/ (if you ever wondered to where well made testcases go people should attach to khtml bug reports, there it is)."
    author: "ac"
  - subject: "Question about the Digest"
    date: 2005-03-13
    body: "Are the developments in the Digest to be included in the final KDE 3.4 or 3.3.x or are they fixes to current stable KDEs?"
    author: "charles"
  - subject: "Re: Question about the Digest"
    date: 2005-03-13
    body: "KDE 3.4.0 has been tagged, so no new developments there. You can see some developments in KDE_3_4_BRANCH, those are for 3.4.1 I believe. HEAD will turn into 3.5.\n\nkdeextragear has its own per-app development cycle. So with amaroK we're working on 1.2.\n\n"
    author: "Ian Monroe"
  - subject: "Re: Question about the Digest"
    date: 2005-03-13
    body: "Shouldn't this be 1.3, since 1.2.x has already been released?"
    author: "ac"
  - subject: "Re: Question about the Digest"
    date: 2005-03-13
    body: "We haven't yet branched 1.2.\n"
    author: "Mark Kretschmann"
  - subject: "Problems with the digest"
    date: 2005-03-13
    body: "I don't know if this is just happening to me (Konqueror 3.3.2), but it seems like all the links of the digest point to the all-in-one version. The separated view only works if I add '&clearall' to the Location bar. Examples:\n\n- http://cvs-digest.org/index.php?newissue=mar112005 goes to the all-in-one page, just like http://cvs-digest.org/index.php?newissue=mar112005&all\n\n- http://cvs-digest.org/index.php?newissue=mar112005&clearall goes to the separated view\n\nThe same happens when I click on the links of each category..."
    author: "VTN"
  - subject: "Re: Problems with the digest"
    date: 2005-03-13
    body: "&all sets a preference by a cookie. &clearall clears the cookie. So yes, if you click on all in one page, that will be the view you see.\n\nDerek"
    author: "Derek Kite"
  - subject: "bug or translation ?"
    date: 2005-03-13
    body: ">Anders Lund (alund) committed a change to kdebase/kate/app/kategrepdialog.cpp >in HEAD \n>Now also with a informative message if you enter a invalid search root.\n>Luckily, it can be disabled :o\n>This contains translated strings, and will be released with the next\n>real release, 3.5 or 4.0\n>BUGS: 101246\n>Refer to bug 101246 - Find in files allows remote directories but doesn't\n>work with such\n\nI would prefer to have the bug removed now, even when there is no translation. "
    author: "a"
  - subject: "Re: bug or translation ?"
    date: 2005-03-14
    body: "That is against KDE policy. Translators have a lot of work to do, and not adding new strings in the _BRANCH are part of the KDE project's commitment to ensure that translators can get the work done. \n\nRemember that if a string is not translated, it will show up in English, and that is a very nasty thing to do for some users."
    author: "Brad Hards"
  - subject: "Many commits in KOffice"
    date: 2005-03-13
    body: "Wow!\n\nKOffice was the most active software package (only after i18n and www) measured in number of commits.  Things are heating up towards the release of KOffice 1.4!"
    author: "Inge Wallin"
  - subject: "Re: Many commits in KOffice"
    date: 2005-03-13
    body: "Also thanks for your contributions to kchart :-)  BTW would it be possible to update the screenshots [http://www.koffice.org/kchart/screenshots.php] which look a bit outdated. "
    author: "MK"
  - subject: "Re: Many commits in KOffice"
    date: 2005-03-13
    body: "Hmm.  Good idea.  I will try, but I can't promise anything."
    author: "Inge Wallin"
---
In <a href="http://cvs-digest.org/index.php?newissue=mar112005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=mar112005&all">all in one page</a>):

<a href="http://accessibility.kde.org/developer/kttsd/">KTTSD</a> adds support for Kiswahili, Zulu, and Ibibio <a href="http://www.cstr.ed.ac.uk/projects/festival/">Festival</a> languages.
<a href="http://extragear.kde.org/apps/digikam/">digiKam</a> adds undo/redo operation for its image editor.
<a href="http://www.koffice.org/kchart/">KChart</a> can now flip row and column data.
<a href="http://www.koffice.org/kexi">Kexi</a> scripting can now pass signals, slots and Q_PROPERTY's between C++ and scripting languages.
Periodic table viewer <a href="http://edu.kde.org/kalzium/">Kalzium</a> adds a family view.







<!--break-->
