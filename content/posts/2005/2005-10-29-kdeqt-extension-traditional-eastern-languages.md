---
title: "KDE/Qt Extension for Traditional Eastern Languages"
date:    2005-10-29
authors:
  - "jray"
slug:    kdeqt-extension-traditional-eastern-languages
comments:
  - subject: "o my..."
    date: 2005-10-29
    body: "what shall i say...\n\nit is truly scary to see what kind of features are required to support \"everything\" out there,\n\nand it is always great to see kde doing a good job in that area :D\n"
    author: "Mark Hannessen"
  - subject: "Mongolian letters"
    date: 2005-10-29
    body: "[Each language script has its own features. For example, traditional Mongolian is written vertically from top to bottom in columns advancing from left to right and the letters always change their shapes depending on different conditions. Thus Qt's text engine and widgets have to be extended to satisfy the requirement of an operating system to supporting these minority languages.]\n\nLetters changing according to position in a word is exactly what Arabic script does, so if a way of rendering Arabic letters already exists, adapting it for the left-right Mongolian shouldn't be much of a problem.  When I first saw it I thought it was upside-down until I saw the numbers.\n\nIs the script widely used anyway?  I've seen books on Mongolian which use the Cyrillic script."
    author: "Matt Smith"
  - subject: "Re: Mongolian letters"
    date: 2005-10-29
    body: "i think this script is widely and officially used in Inner Mongolia, China.\nThe Cyrillic script is used in (Outer) Mongolia, which is outside of China.\n\nStill, I find the use of Uigur Mongolian script as shown in the screenshots is really impressive."
    author: "altan"
  - subject: "Can't wait for QT 3.3.6"
    date: 2005-10-29
    body: "Congratulation on continuous success in making life easier for developers and users. \n\nI hope QT JAVA in 4.1 will rock. I wish that existing java application's could be ported to QT using some sort of interpreter or crosscompiler; since Java is a very structured language and Java -> QT would be easy to port using a interpreter (java to QT) IMHO.\n\nGood Luck!"
    author: "Fast_Rizwaan"
  - subject: "Re: Can't wait for QT 3.3.6"
    date: 2005-10-29
    body: "You can use the existing QtJava bindings in the meantime if you want. I regenerated them against the Qt 3.3.5 headers the other day for the KDE 3.5 release."
    author: "Richard Dale"
  - subject: "Re: Can't wait for QT 3.3.6"
    date: 2005-10-30
    body: "Thank you for your work. Lots of people prefer Java to C++, but prefer the Qt APIs to Swing for the same reasons that they prefer Java to C++ (a better way to manage complexity, at least we think so)\n\nIf Qt/KDE becomes a working and relatively well-known Java platform, I suspect this would attract quite a bunch of developers (since Java seems to be the most popular programming language currently, but it's still lacking a good GUI framework) (BTW, I know about SWT, but it's licence is GPL incompatible which means no GPL'd Qt implementation, and it's also a layer of 100% abstraction, which means it wouldn't allow for real KDE development even if it had a KDE implementation)"
    author: "j"
  - subject: "What a second here..."
    date: 2005-10-30
    body: "Is Kate shown working in Microsoft's Windows? cool!"
    author: "Vlad Blanton"
  - subject: "Re: What a second here..."
    date: 2005-10-30
    body: "Sorry to disappoint you, don't think that's windows. It looks like the Redmond windowdecoration, with the Redmond 2000 colorscheme. "
    author: "Morty"
  - subject: "Re: What a second here..."
    date: 2005-10-31
    body: "yes. This program is adopted to one of the large number of themes in KDE. But window styles are not related to the color schemes and themes. All widgets will take on different appearances and behaviours, which can be determined by the script set up by desktop language environment."
    author: "James Ray"
  - subject: "Wow!"
    date: 2005-10-30
    body: "I've always liked the Mongolian script ever since I read a Latin grammar of Mongolian -- with the script running in the right direction. And Tibetan... Pity we don't see any screenshots of that. I actually learned Classical Tibetan ten years or so ago."
    author: "Boudewijn Rempt"
  - subject: "Re: Wow!"
    date: 2005-10-31
    body: "Thank you for your concern. Tibetan is another very interesting script in the world. According to Tibetan character set in ISO/IEC 10646, Tibetan characters must be subjoined one by one to form an overlapped stack, which is very alike to the ideograph, such as Chinese and Hangul. Here is some snapshots about Tibetan and Uighur:  \n\nhttp://159.226.5.82/~ruijw/Snapshots.html."
    author: "James Ray"
  - subject: "Re: Wow!"
    date: 2005-10-31
    body: "Again, wow! I'm really impressed. This looks just like it should. Next step antialiasing?"
    author: "Boudewijn Rempt"
  - subject: "Re: Wow!"
    date: 2005-11-01
    body: "Next step is not antialiasing, which has been done by FreeType. We will focus on the other scripts, such as New TaiLue, Yi, and so on. I'm very glad to hear the advice from you."
    author: "James Ray"
  - subject: "sigh"
    date: 2007-01-09
    body: "As I know, these code had not been merged into Qt or KDE by now. Sigh. And some urls and pictures are broken.\n\nThe homepage of James Rui\nhttp://sonata.iscas.ac.cn/rjw/index.html\"\n"
    author: "Cavendish Qi"
---
A research group in the Institute of Software at the <a href="http://english.cas.cn/Eng2003/page/home.asp">Chinese Academy of Sciences</a> have been working on an operating system to support traditional Eastern languages such as Mongolian, Uighur and Tibetan. We have now extended Qt and KDE 3 to support these langages.  Some <a href="http://159.226.5.82/~ruijw/MongolianSystem.html">screenshots show the scripts in use</a> (<a href="http://kubuntu.org/~jr/dot/MongolianSystem/">mirror</a>). Any comments are welcome.






<!--break-->
<p>This work is against KDE 3.4/Qt 3.3.2. To support these minority scripts, the following things have been done:</p>

<ul>
<li>QPainter text drawing routines have been extended to support top-top-bottom left-top-right and top-to-bottom right-to-left directions. We consider it imperative that QPainter can adapt to the major scripts in the world.</li>
<li>Some script engines have been added.</li>
<li>Widget interfaces have been extended to satisfy the requirements of different scripts. As the screenshots show, all widgets can be rotated for vertical scripts.  We have also made some modifications to better support widget presentation in reverse mode.</li>
</ul>

<p>Each language script has its own features. For example, traditional Mongolian is written vertically from top to bottom in columns advancing from left to right and the letters always change their shapes depending on different conditions. Thus Qt's text engine and widgets have to be extended to satisfy the requirement of an operating system to supporting these minority languages.</p>

<p>We have completed the largest part of this work, but it is not yet finished. Our engineers are still working on Qt and KDE Libs. We have also been working on Konqueror which will be extended to meet some of the extra features from CSS3.</p>





