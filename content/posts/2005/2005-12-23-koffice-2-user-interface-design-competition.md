---
title: "KOffice 2 User Interface Design Competition"
date:    2005-12-23
authors:
  - "iwallin"
slug:    koffice-2-user-interface-design-competition
comments:
  - subject: "Let the clones begin :)"
    date: 2005-12-22
    body: "I naturally expect a lot MS Office 12 clones to be submitted and then discarded because they are not researched enough to ensure the result would actually be better then what we have now :)"
    author: "Thomas Zander"
  - subject: "Re: Let the clones begin :)"
    date: 2005-12-22
    body: "Clownding clones and possibly Melissas doggy lounge."
    author: "Ami Pro 3.1"
  - subject: "February or March?"
    date: 2005-12-22
    body: "Here (http://www.koffice.org/competition/guiKOffice2.php) it says February the 15th, but here (http://www.koffice.org/news.php) March the 15th.\n\nWhat is correct?"
    author: "Dominik"
  - subject: "Re: February or March?"
    date: 2005-12-22
    body: "The deadline is february 15th.  The winner will be announced march 1st.  And there is another error that says 2005 when it should be 2006.\n\nAs soon as our webmaster is around, we'll fix that."
    author: "Inge Wallin"
  - subject: "Re: February or March?"
    date: 2005-12-22
    body: "Corrected. Thanks!"
    author: "Daniel Molkentin"
  - subject: "Re: February or March?"
    date: 2005-12-22
    body: "My mistake, deadline is February 15th 2006!\nSorry about that!"
    author: "annma"
  - subject: "koffice kids"
    date: 2005-12-22
    body: "I'm more interested in koffice kids :))) It is one of the best ideas I heard this year, and I think the edubuntu folks would appreciate it very much. I'm curious: have anyone contacted them about it?"
    author: "molnarcs"
  - subject: "Re: koffice kids"
    date: 2005-12-23
    body: "Isn't Edubuntu an effort to build a distribution especially targeted to schools, with terminal sharing facilities and alike?\nAnd, at least in my country, the goal to teach office applications to kids is really about teaching them the real deal in order to prepare them for the future and the office software needs to be what their parents use (or at least easily installable -- that's way OpenOffice.org is the selected one).\n\nAnyway, I totally agree that a word processor for kids would be nice. Though I think SDL or some other game library would make more sense. Bill Kendrick, the author of TuxPaint, was working on such a project."
    author: "blacksheep"
  - subject: "Re: koffice kids"
    date: 2005-12-23
    body: ">a word processor for kids would be nice.\n\nYou have to think bigger than that, not for kids but a genereal real simple wordprocessor. But not by doing the usuall mistake, by building one with only a limited set of features. Rather take the whole KWord and put a simple UI on top of it, like the 'kids' mockups. Your core is still a full featured open document based wordprocessor, but you will only utilize a small subset of the functionality. Make it customizable and you can make a whole range of versions customized for people with special needs using only part of the functionality, like 'grandma', 'author', 'journalist' or 'script writer'."
    author: "Morty"
  - subject: "Re: koffice kids"
    date: 2005-12-24
    body: "I don't really understand the point. If a kid is old enough to be literate and writing enough to require a computer, they're old enough to use a normal software. My first word processor was WordPerfect 6 in 3rd or 4th grade, I don't recall it being an issue. I wasn't creating tables or printing out address labels, but I knew how to write, save, print and spell check.\n\nI suppose if you want to teach like kindergardens or 1st graders to use a computer word processor, a special UI might make sense.\n\nBut you know, IANAT - I am not a teacher. ;)"
    author: "Ian Monroe"
  - subject: "Nice, but..."
    date: 2005-12-22
    body: "This is all very nice, but as long as the print quality is as terrible as it is now (due to font kerning problems) KOffice isn't even usable in real life. Let us please get the basics right first.\n\nI would love to start using KOffice (and I always try out new versions) but the printing quality is still horrifying."
    author: "Meneer Dik"
  - subject: "Re: Nice, but..."
    date: 2005-12-23
    body: "Well you are mostly talking about KWord and you are right unfortunately. The situation will change once KOffice is ported to Qt4 which offers everything with respect to fonts that we would need. But once this is fixed and once OASIS has become a more common file format KWord will become a really shining word processor application: Fast, featureful, easy to use and developed on a clean decent framework that allows it to be extended easily."
    author: "Torsten Rahn"
  - subject: "Re: Nice, but..."
    date: 2005-12-24
    body: "Actually, as the recent discussion has uncovered, it will take more than just porting the existing code to Qt-4.1.  It appears that while the new \"HighResolution\" option will greatly improve text layout that the code will need to be rewritten to use the new: \"QTextLayout\" class to get true WYSIWYG printing."
    author: "James Richard Tyrer"
  - subject: "Re: Nice, but..."
    date: 2005-12-24
    body: "Why? It doesn't sound all that hard to replace int with double in the existing KWord code, and to replace QFontMetrics with QFontMetricsF. What else would be required that would require porting everything to QTextLayout?\n\nBtw: you always post about fonts and font kerning. Are you contributing yourself to those things in KDE, as a programmer? Would you be the one who will actually do the work to add true WYSIWYG to KOffice?\n"
    author: "anonymous"
  - subject: "Re: Nice, but..."
    date: 2005-12-24
    body: "Yes I believe he has more than once posted and contributed patches.  I first thought he was a troll but over time reading all his posts on the dot and elsewhere I think he truly cares deeply about this issue and is trying to advance the state of KDE.  This guy has gone up in my estimation."
    author: "ac"
  - subject: "Please, embrace document processing!"
    date: 2005-12-23
    body: "My advice, at least regarding KWord, is simple: dump the legacy word processing stuff.  Have a long, hard look at how LyX works, and learn from it.  Instead of having low-level options like Letter vs. A4 pages, bold vs. italic vs. underline, etc., have better, high-level options like \"Book\" vs. \"Formal Letter\", and \"title\" vs. \"chapter\" vs. \"inline quotation\".\n\nMake sure LOTS of templates are included, and neatly categorised, so that people can easily find and begin the document of their choice.  Make it even easier to actually write that document, because inserting elements is as simple as choosing them from the style list or the context menu (technical books would have options like \"quotation\", \"callout\", \"illustration\", etc.).  Make it possible to download and *contribute* templates easily through an automated and  rated version of KHotNewStuff.\n\nFinally, make it really easy to edit these templates, ot to create your own, with a style creation mode, that works like an outliner, and has all the style options like font choices and bold, italics, positioning of elements etc., but  that won't let you put actual content in from that mode, so users clearly see the difference in style vs. content, and can manage their styles easily, and contribute them to others.\n\nPerhaps, also, for simple changes, styles could be easily modified in simple ways without getting into all the style mode stuff.  So, you could choose to increase the overall size of all the fonts, or to increase the paper margins or make your book double-sided, without actually getting into all the gritty details of what elements a technical book has, and which headings are big and whcih are small etc.  This could even be done with template wizards, which would run on startup (for your first book) or later through a menu item.\n\nThis sounds crazy to many, I'm sure.  But, if you've ever tried LyX for document editing, you know that it's really a perfect way to write documents; the only thing missing is that setting up templates is a nightmare, because it's based on LaTeX.  KWord isn't; it has a lot of great style and formatting technology, and I'm not suggesting throwing that away for a second.  But, I think it should take its rightful place as an engine that drives styles, and not as a user-exposed thing, where users have to explicitly say that they're typing in 12pt font now, or that this is italic font, rather than \"this is speech\", or \"this is an address\", or \"I'd like to insert a quotation\".\n\nBy doing these things, you'll also make it possible to do more things.  Obviously, if the word processor understands that something is an address, rather than just an indented block of text in a different font style, then it can do cool and intelligent things for you, like automatically handling address changes from KAddressBook.  Or, if it knows that some quotations are included, it could easily generate a list of thanks to those inspiring writers/speakers  at the end of the book.\n\nIf a word processor is better for writing documents because it knows the difference between lines and paragraphs and images, then just imagine what a document processor could really do, if implemented properly, when it knows you're writing a book, and that this is the title, and that is the first chapter, and this is a quotation, and that's speech, etc.  The possibilities   and new opportunities are endless.  Just think of how it might go down in workplaces too, when secretaries and government document authors realise that they never need to worry about fonts again, and disabled users can have their interface described in terms of chapters and quotations and authors rather than paragraphs and lines and letters.\n\nKWord already has the style mechanisms to do this, so why not use it?  Make a document editing tool, and not a souped-up typewriter.  Document processing is here, but no one has really done it yet.  KWord could be a new killer app that everyone else is just slowly inching towards, if you let it."
    author: "Lee"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-23
    body: "Your post says to me that you prefer LyX and have a few problems with it that you feel kword does better. But your problems would be better solved by fixing those issues with lyx. For people who prefer the whole content oriented thing lyx is there. Don't make kword a copy of lyx. Some people want a program that is very much visual, and oriented towards the actual print layout more than the content, and that's what kword is for.\nMaybe lyx should be adopted by koffice as well. I know I'd prefer it to become a proper kde program with kioslaves..."
    author: "mikeyd"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-23
    body: "Your post says to me that you have never tried LyX. If you want program which is very much oriented towards actual print layout, then you are either talking DTP and then you should use Scribe or some real layout program, or you don't know what you are talking about. The point was not to eliminate WYSIWYG, but to enable us who prefer that logical authoring. And about LyX, those small problems with LyX he is talking about (at least that's my experience and I am heavy user of LyX myself) would mean complete rewrite, throwing out LaTeX and rewriting LyX based on some other printing technique (like FOP-PDF toolchain combined with ODF). I don't think it will ever happen."
    author: "Matej"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-23
    body: "Wow, that's a great idea. Please write a proposal :)\n\nAlthough I also have the fear that there will be many Office12 clones around, and one of them will win :(\n\nMy suggestion: Please put in a modal concept like Vi has _as an option_.\n\nOnce you are used to the vi editior, you really would also like to have the HJKL keys for navigating in a document.\n\nIt should be possible to say:\n- Default key bindings (like it is now)\n- Vi style key bindings\n- Emacs style key bindings\n\nAlthough I know that probably will never happen, let me dream about my proposed favorite WordProcessor."
    author: "Martin Stubenschrott"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-23
    body: "\"Although I also have the fear that there will be many Office12 clones around, and one of them will win :(\"\n\nHighly unlikely. After all, there are at least two dependable people on the judging committee..."
    author: "Boudewijn Rempt"
  - subject: "Great ideas! ... but..."
    date: 2005-12-23
    body: "I endorse everything you said, point-by-point. Such pondered and well-written are even rare. Also, it is a source of ideas on which to base an interface project (hint, hint).\n\nBut you're going too fast! With luck, KOffice might be improved and become the #2 Linux suite, behind Openoffice. This involves a lot of faith, because the competition is most excellent (e.g., Abiword and Softmaker products).\n\nIn that context, I suggest we do the _basics_ very well done. I work with Oo.o on Linux in a Windows corporate environment. Let me say, interoperability is king. I even have to save as \"doc\" to share memos, letters, specifications etc. People there are not very enlightened, odf is out of question. Period.\n\nSo, Koffice _must_ read M$-Office docs as perfectly as possible. Oo.o 2.0 does a terrific job; I expect (and need) no less. I'm all for odf (and use it whenever possible), but in some places there's a Windows culture so ingrained people prefer to pay whatever M$ asks just to keep things as they are, because change is scary to them.\n\nAlternatively, since Oo.o is in the Windows-compatible niche, KOffice might \"go for the younger\" and work for people/companies with no previous IT structure. I don't know whether or how this would work out.\n\n"
    author: "Gr8teful"
  - subject: "Re: Great ideas! ... but..."
    date: 2005-12-23
    body: "> So, Koffice _must_ read M$-Office docs as perfectly as possible. Oo.o 2.0 does a terrific job; I expect (and need) no less\n\nUmm; just because OpenOffice 'supports' (something that resembles supporting but does not really work when you need it the most) MS Office formats Microsoft has been able to postpone rollout of open formats for half a decade now. So let's not play their game anymore. And if the money spent on reverse-engineering MS Office formats would have been spent on writing new software we would have one hell of a Open Office by now.\n"
    author: "Janne Karhunen"
  - subject: "Re: Great ideas! ... but..."
    date: 2005-12-23
    body: ">  spent on reverse-engineering MS Office formats ... we would have one hell of a Open Office by now.\nand.. nearly nobody would use Open Office. You can drop support of common format only if you have >= 50% of the market, else most people won't consider using your product.\n\n>  because OpenOffice 'supports' office formtats ... postpone rollout of open formats for half a decade now.\nIt didn't postpone anything. These things are independent.\n"
    author: "sss"
  - subject: "Re: Great ideas! ... but..."
    date: 2005-12-24
    body: "> and.. nearly nobody would use Open Office\n\nFor starters yes, but via truly common open format true mass adoption could be reality in not too distant future. With current versions i really can't see that happening as it's really inferior product quality/feature wise.\n\nSo, I would have taken the same approach than Linus took with the kernel: no undocumented binary drivers (or even binary ABIs), please.\n\n"
    author: "Janne Karhunen"
  - subject: "Re: Great ideas! ... but..."
    date: 2005-12-25
    body: "It's not this the point. I can do the same comparition with mp3 / ogg. One is copyrighted, one is free (at most). Who is the leader now?"
    author: "Davide Matarese"
  - subject: "MP3 is not \"copyrighted\""
    date: 2006-02-06
    body: "It's patented."
    author: "Humberto Massa"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-23
    body: "You do know that Matthias Ettrich founded both LyX and KDE?\nSo there must be something in what you are proposing."
    author: "reihal"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-23
    body: "YES, my dream ...\n\nThis way a word (sorry ... document) precessor would be a lot easier to use.\n\nI already use styles and as I can see, Kword is already very good handling them. At least it is a lot easier to use styles with Kword rather with OpenOffice.org. And OpenOffice.org is also a lot easier than MS Office (mainly because of the good documentation of OpenOffice.org).\n\nAt work, I must use MS Office 2003 for small reports. I must use chapters and I have never been able to create a style to automatiquely auto increment chapters numbers (I have searched the documentation and never find). I have also tried with OpenOffice.org and founded in the documentation how to do it (but it is difficult for me and I must look at the doc each time I want to create a new document). But with Koffice, it is really a delight. I didn't need to look in the doc for these basic features (even if the koffice doc is truly excellent and is a pleasure to read).\n\nPS. When I was a computer science student, I used LaTeX and I found it a lot easier than any office suite (except koffice). I am really a newbe in office suites.\n"
    author: "tgrauss"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-24
    body: "Most of this is already in KWord, and where its not yet, its mostly because the implementation is not working correctly there. Or, in other words, this setup is what I had in mind when I started working on KWord years ago.\n\nYou can create a template with ease from the current document, for example.   There certainly are good templates missing, but you and everyone can help with that (*wink*).\nStyles already do everything you need, including cool things like the 'next style' feature that makes sure that after typing the headline and pressing enter the next line is of the style for that paragraph.\nWhats incorrectly implemented currently is the 'styles based on other styles' feature.  What this does is that if you change the base style all others will follow.  So change the base style to be 1 point bigger and all follow.  But that does not work currently.\n\nWe will always support people that want to manually set the font size to 12 / bold.  We can't force a workflow on them that takes effort to learn, I'm sure we'd loose too many users that way.\n\nIf you have more specific suggestions, please post them to the mailing list, or enter the contest :)"
    author: "Thomas Zander"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-27
    body: "> We will always support people that want to manually set the font size to\n> 12 / bold. We can't force a workflow on them that takes effort to learn, I'm\n> sure we'd loose too many users that way.\n\nYet if you stick to that in advance, I fear you have already lost the battle. Openoffice also allows style-based and (somewhat) content-based editing, yet all the interface buttons for layout- and markup-related things make it all too easy to not use styles consistently, and mess up your document if you change the style afterwards. That increases the effort needed to learn a new workflow, instead of reducing it.\n\nWriting in a structured latex-like way takes time to get used to, and in my opinion the only way to get people to do so is to almost force them, if not completely.  That does not mean you need to remove all formatting options - latex also allows emphasis and other markup options. It does help to actually name it emphasis and not 'italic', as the two don't need to be the same.\n\nI would love to see competition entries focussing on the ideas discussed in this thread - I think a good start might be to remove as many text markup buttons from directly visible toolbars as possible, and to replace them with style/content related functions. Even if you keep them in the program, removing all the 'bad' functions from plain sight might just do the trick in educating a lot of users.\n\nAs for the msword format vs. open document format discussions - my ideal would be a single (command line?) tool that converts msword documents to opendocument format and vice versa. All compatibility work could go into that tool, freeing openoffice and kword developers from integrating it's functionality into their software.\n\nUnfortunately I'm one of many that will only contribute in replying and not in code, but hey, perhaps it will help to form some ideas. Keep up the good work!\n"
    author: "H. Zelle"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-27
    body: "I agree completely. Perhaps put the manual style tools into some advanced menu or dialogue?\n\nSo long as KWord continues to be Yet Another Wordprocessor I fear it's only advantages will be the KDE architecture and its speed. Otherwise OpenOffice will always look more attractive.\n\nKWord probably allows me to focus on workflow but I never really noticed that. To me it looks like MS Word or OpenOffice Writer, it invites you to start making bits of text 14pt-bold and to manually adjust every bullet point to the right margin and style. The whole UI should scream \"this is how to use me correctly\". It should make writing an article, letter, book or random document as easy as possible and make the user immediately aware of how it will help them. It should force me to say \"make this a list, and let me configure the style of lists if I really need to\", or \"emphasise this\" which means \"use my configured style of 14pt-bold-Dejavu Sans\". Good defaults, a clear mainwindow UI and obvious style configuration utilities would really make KWord stand out from the crowd.\n\nUntil then I'll battle on with LaTeX..."
    author: "Tom Chance"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-27
    body: "Great ideas! So how about this; let there be a \"style\" toolbar that is an expanded version of the style dropdown list. It should have a buttons for emphasis on/off rather than bold, italics etc. buttons. Also the most important style options for the current document type should be represented. Hide the text-level formatting toolbar by default.\n\nThe style toolbar is to be generated from the formatting styles currently defined. So when creating formatting styles in a document template you also choose how they are to be displayed in the formatting tool bar.\n\nIn addition: Mark everything that has been formatted using low-level styles by a wiggly underline in a suitable colour, just like the automatic spell/grammar checkers do. (This should be optional of course.)"
    author: "Martin"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-24
    body: "I would love a fully document-oriented mode. And I do mean that down to the point where you cannot type two spaces in a row.\n\nWhen I wrote a 200-page thesis earlier this year, in LyX, I made some last-minute changes just before sending it to be printed. Even though I knew that those changes would change things throughout the document; page breaks and everything, I didn't bother checking the formatting. (And of course it worked perfectly.) That is the power of document processing.\n\nAs someone else pointed out; LyX cannot be made a generic document processor without huge effort. It would have to be KWord."
    author: "Martin"
  - subject: "Re: Please, embrace document processing!"
    date: 2006-01-03
    body: "Not typing 2 spaces in a row is also a feature that KWord already has for some time. :)\nIts part of the autocorrection."
    author: "Thomas Zander"
  - subject: "Re: Please, embrace document processing!"
    date: 2005-12-26
    body: "I prefer Kile to LyX, but utilize both. What is so terribly difficult about installing Macro Packages? The Dummy's Guide to LaTeX/TeX & their many Apps with sections on Kile, LyX, TeXShop.app would be a huge aide in bridging the gap between these powerful tools.\n\nPeople who use Final Cut Pro don't just point and click. They study workflows, design considerations, so on and so forth.\n\nKWord could leap forward if it were morphed into a Kile-light for its publishing capabilities and a Framemaker-light/Scribus for its DTP capabilities. If they could develop interfaces to enscapsulate the LaTeX Macro Packages and just simply install them and leverage much of their functionality out-of-the-box, then you'd see people taking a stronger look at these tools for the small office consumer.\n\nAnyone wanting to publish novels, journals, etc., should focus on apps like Kile/LyX/Scribus and make sure they continue to grow.\n\nMost people don't write 500 page novels, technical books, but those that do should not see their beautiful tools become less of a focus just so we can get more users who write 5 page papers interested in Linux.\n\nWhat I would prefer happen is that Kword provided Services, ala NeXT/OS-X, that interact with Kile/LyX and Scribus/Inkscape and with these tools create a suite of smaller tools that address publishing needs for all."
    author: "Marc J. Driftmeyer"
  - subject: "Textmaker 2002 interface!"
    date: 2005-12-23
    body: "it is very clean and simple, but very featureful. how many of us have tried the free version of textmaker which can be used freely (free as beer) and redistributed unlimitedly.\n\nTextmaker is an alternative, efficient and powerful word processor for\nLinux and FreeBSD systems. It starts quickly and contains a huge amount\nof features. It is extremely easy to use, compatible to MS Word formats\nand comes along in a modern look-and-feel.\n\nRegistration:\nyou can register (which is optional) easily by clicking \"Register & Ordering\" button when you launch the program.\n\ndownload:\nftp://fr2.rpmfind.net/linux/SuSE-Linux/i386/9.3/suse/i586/textmaker-2005.2.11-3.i586.rpm\n\ninstall and use:\nrpm -ivh textmaker-2005.2.11-3.i586.rpm --nodeps --force\n\nrun:\n/opt/textmaker/tml"
    author: "fast_rizwaan"
  - subject: "Export to PDF planned?"
    date: 2005-12-23
    body: "Since QT 4.1 was recently announced to have a PDF backend, will that make it easier for KOffice to have an \"Export to PDF\" functionality similar to OpenOffice 2.0?"
    author: "Vlad C."
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-23
    body: "You can already export to PDF, it is in the print-dialog. Select the PDF-printer."
    author: "Carsten Niehaus"
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-23
    body: "yeah, but just being able to say 'save as' and then pdf would be better.\n\nand to parent - i don't think so ;-)"
    author: "superstoned"
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-23
    body: "\"Save\" or \"Save as\" assumes that you can \"Open\" the result in the same application.  Users would be disappointed if this assumption doesn't hold true.  On the other hand, \"Export\" doesn't imply the ability to \"Open\"."
    author: "chainik"
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-24
    body: "kword 1.4.2 can open .pdf files, using the file-open dialog. So, Save as PDF is more logical since we can open the pdf file using kword, as you are expecting \"save = open\" and \"export = import\" ;)"
    author: "fast_rizwaan"
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-24
    body: "Yes.  But not before the 2.0 release of KOffice, I fear."
    author: "Thomas Zander"
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-26
    body: "Just wondering, is it not possible to do a quick and dirty version of this by using the existing kprint and some DCOP magic? Just to silence the ones wanting the \"Export to PDF\", who don't know the power of the KDE framework."
    author: "Morty"
  - subject: "Re: Export to PDF planned?"
    date: 2005-12-24
    body: "It increases the possible implementations, but doesn't really make much difference.  KDE can add what OO has: a shortcut that has the exact same effect as printing to a PS file and converting it to a PDF with GhostScript (which is what KPrint does when you print to PDF).\n\nPerhaps a more important issue is the ability to use a PPD when making a PDF.  KPrint has the GhostScript PPD for pdfwriter translated into XML and it uses the XML file rather than being able to access the PPD file directly.\n\nOO can use any PPD file that you want to PRINT to a PDF, but the short cut seems to be able to use only the Adobe Distiller PPD.\n\nBoth programs need for the user to be able to choose the PPD that they want to use for generating PDFs."
    author: "James Richard Tyrer"
  - subject: "usability experts!"
    date: 2005-12-23
    body: "Nice but...\n\n\"The judges will be picked from the KOffice developer\"\n\n\nBe sure to include some usability expert too!"
    author: "Diego Calleja"
  - subject: "Re: usability experts!"
    date: 2005-12-23
    body: "Oh god, please no.  So-called usability expert is just going to kill the creativity of this project."
    author: "ac"
  - subject: "Re: usability experts!"
    date: 2005-12-24
    body: "The usability experts from OpenUsability.org are doing really good work for KDE and other free software projects. Their style is totally different from those \"so-called usability experts\" that you refer to."
    author: "Olaf Jan Schmidt"
  - subject: "Re: usability experts!"
    date: 2006-01-02
    body: "I think the orginal poster was looking for usability experts, not so-called usability experts.\n\nA group of techies will of course almost always develop something they themselves can use, and benefit from using. That is not always the same thing as making a program usable for people outside that gruoup. \n\nReal usability experts have tools that when applied actually makes the application better for its intended users.\n\nIn my experience, techies refusing to see the big picture, is just as often the culprit when creativity is killed.\n\n"
    author: "Uno Engborg"
  - subject: "An interesting future OOo concept for OS X..."
    date: 2005-12-28
    body: "... which could also be applied, maybe, to KOffice 2: http://www.vorbisinfo.org/openoffice .\n\nEspecially the tabs (and also the OS X-like toolbars and sidebars) would be a good thing, IMHO.\n\nOnly an idea, of course..."
    author: "Sven"
  - subject: "Re: An interesting future OOo concept for OS X..."
    date: 2005-12-31
    body: "wow that's great. I really like this interface idea."
    author: "Martin Stubenschrott"
  - subject: "New way of thinking needed."
    date: 2006-01-02
    body: "Even though it is nice to get KWord, KSpread and KPresenter improved, I think we need to rethink the concept of an office suit. Today programs like MS-Office, OpenOffice.org, and KOffice are no more than glorified typewriters and adding machines.\n\nThey are a reminicence of the days when you put a piesce of paper in your typewriter, wrote a document and then stored it in a binder on a shelf or in a file cabinet. The only difference is that now the file cabinet is a digital artefact. A computer should be able to do a lot more than that.\n\nFor one thing, current office suites catches very little of the dynamics and business processes going on in an office. They are very poor at relating piesces of information to each other in a way that makes sense from a business perspective. We lack things like workflow handling, and collaborative tools. \nThey also doesn't describe the work process in business terms. E.g. projects, customers/clients, employees,...\n\nIn short, they are document creation suites, not office suits. What I would like to see are tools that make it simple to tie the various components together. E.g. if you create some type of document that needs to be approved before it is published, it should be possible to create a template for such behavior, so that when the worker selects \"new publishable document\" it should turn up in the managers todo list as soon as the employee signals that he thinks its ready, and if the manager approves it it should be published automagically and if he doesn't approve he should be able to send it back to the employee with a comment on why.\n\nIt should also be possible to let documents and workflows to be part of projects, and it should be possible to create project templates containing certain documents. If a users selects \"create new project\" he would perhaps get a gantt diagram, an empty or partly filled out project description, all depending on how the template project was defined. Such templates should also make it possible to ask for what human resources that should be part of the project and perhaps set up mailinglists and define calendar categories for the project. It should be possible to right click on a part of the gantt diagram to create new documents associated with a certain project part. Another possibility would be to drag documents, meeting and address book references to the gantt diagram to associate them with a project or a certain part of the project.\n\nAnother idea would be to make all of it more or less webbased, using serverside storage for easy backup, and cross platforms components tied together by web technologies like html/xml/xforms and cross platform browser plugins. That way deployment costs could be lowered.\n\n"
    author: "Uno Engborg"
  - subject: "Re: New way of thinking needed."
    date: 2006-01-05
    body: "Yes thats were the trend is going at least with MS Office (MSOffice 12 + sharepoint (with ITIL implementation)) for handling project and documents in groups/collaborations. "
    author: "Hans G."
  - subject: "Been done with Office for years"
    date: 2006-03-06
    body: "I believe starting with the 2000 version of Office, there has been an increasing level of integration via SharePoint and Exchange. I worked at a lawfirm where several people would be working on 200 - 300 plus page documents with graphics pros, Excel people, secretaries, the attorneys on both sides, etc. all collaborating all at the same time. The Word doc would be the master with several people working on the various sections in chapterized form according to their specializations with the graphics, spreadsheets, and merge data all dynamically flowing in from other departments. Sign-offs and edits went through to all the people they needed to through SharePoint and via Exchange for outside council. It all plugged into the document management system, iManage.\n\nWhat I would be looking for is an entire enterprise level document system with per user controls, auditing, document management, compliance devices, access controls, edit routing, plugs into a robust messaging system, tight integration with several tools (think a stable API), etc. Most, if not all, of the editing abilities are there already. Of course, this would have to be Windows native. Sorry, but there are too many apps that are absolutely necessary for my company to switch.\n\nSo I am waiting for KOffice 2."
    author: "Jason"
---
KOffice development is currently going on at a tremendous pace.  Version 1.5, with Open Document as the default file format, will be released in March 2006, and it is time to start collecting ideas for version 2.  KOffice has received a donation of $1000 to be used as the prize in a GUI and Functionality Design competition.  So whip out the RAD tools and your imagination and design the next big thing in office automation!



<!--break-->
<p>An anonymous donor who cares a lot about KOffice has donated a sum of $1000 USD to be used as prize money in a design competition.  The purpose of the competition is to generate new ideas for the user interface of the next generation of KOffice, which is expected to be released around the same time as KDE 4.0.</p>

<p>Submissions to the competition should be GUI mockups accompanied with a written description of the intended workflow with ideas for the design of KOffice 2.  See <a href="http://www.koffice.org/competition/guiKOffice2.php">the KOffice 2 competition page</a> for an example of how this could be done.</p>

<p>In case the judges cannot find an entry that is truly best the prize money may be split between at most 3 entries.  We do not expect this to happen, though.</p>

<p>There is no guarantee that any suggestion from the entries will actually be implemented in KOffice 2, but we will do our best not to waste good ideas. We also reserve the right to use any idea from any entry.</p>

<p>Our donor said "I believe in the potential of KOffice to become a better office suite than all the others in the market.  It would be great for me and society to be able to buy KOffice which is better than Microsoft Office for the total price of 0 dollars."</p>

<p><strong>Small Print:</strong></p>

<p>The judges will be picked from the KOffice developers.  Currently, Inge Wallin and Boudewijn Rempt are appointed, but there might be one more to make the number uneven. Those who enter the competition should be aware that there will be no other compensation paid ever, and that all ideas, graphical elements, workflows, etc, can be used in KOffice 2.0 and published under the GNU General Public License. The final decision of the judges cannot be overruled. Any taxes due will be paid by the winners.</p>


