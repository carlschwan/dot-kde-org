---
title: "Qt Quarterly Articles About Qt 4"
date:    2005-07-12
authors:
  - "binner"
slug:    qt-quarterly-articles-about-qt-4
comments:
  - subject: "can I compile KDE 3.4.1 with qt4?"
    date: 2005-07-13
    body: "is it possible to compile KDE 3.4.1 with qt4, i need the unicode support."
    author: "fast_rizwaan"
  - subject: "Re: can I compile KDE 3.4.1 with qt4?"
    date: 2005-07-13
    body: "No. What Unicode support are you missing in Qt 3.3 exactly?"
    author: "Anonymous"
  - subject: "Re: can I compile KDE 3.4.1 with qt4?"
    date: 2005-07-13
    body: "basically Hindi (devanagari), type \"aassddff aassddff\"(using hindi keyboard layout causes garbage) i.e., \"\u00e0\u00a5\u008b\u00e0\u00a5\u008b\u00e0\u00a5\u0087\u00e0\u00a5\u0087\u00e0\u00a5\u008d\u00e0\u00a5\u008d\u00e0\u00a4\u00bf\u00e0\u00a4\u00bf \u00e0\u00a5\u008b\u00e0\u00a5\u008b\u00e0\u00a5\u0087\u00e0\u00a5\u0087\u00e0\u00a5\u008d\u00e0\u00a5\u008d\u00e0\u00a4\u00bf\u00e0\u00a4\u00bf\" and FRA \u00e0\u00a4\u00ab\u00e0\u00a4\u00bc\u00e0\u00a5\u008d\u00e0\u00a4\u00b0 which should be like \u00e0\u00a4\u00ab\u00e0\u00a5\u008d\u00e0\u00a4\u00b0\u00e0\u00a4\u00bc is not rendered properly in KDE 3.4.1 because of QT 3.3.4 because many words are based on FRA (\u00e0\u00a4\u00ab\u00e0\u00a4\u00bc\u00e0\u00a5\u008d\u00e0\u00a4\u00b0) like AFRICA, FREE, like \"\u00e0\u00a4\u0085\u00e0\u00a4\u00ab\u00e0\u00a5\u008d\u00e0\u00a4\u00b0\u00e0\u00a5\u0080\u00e0\u00a4\u0095\u00e0\u00a4\u00be\", \"\u00e0\u00a4\u00ab\u00e0\u00a5\u008d\u00e0\u00a4\u00b0\u00e0\u00a5\u0080\" (these are APHRICA and PHREE to show you that \u00e0\u00a4\u00ab\u00e0\u00a4\u00bc\u00e0\u00a5\u008d\u00e0\u00a4\u00b0 should be like \u00e0\u00a4\u00ab\u00e0\u00a5\u008d\u00e0\u00a4\u00b0 with a dot). Though Open Office 1.1.4 has no problem with that rendering.\n\nShy do I need that weird \"aassddff aassddff\" like thing? It is because I am creating Hindi Typing lessons for KTouch and because of QT 3.x it is not comfortable :(\n\nthanks though."
    author: "fast_rizwaan"
  - subject: "Re: can I compile KDE 3.4.1 with qt4?"
    date: 2005-07-14
    body: "The \"aassddff\" sequences look correct (at least with the font I'm using here). \n\nIf you have some problems and some combination of chars are not rendered correctly, please send a mail to qt-bugs@trolltech.com, so that we can fix the issue for the next patch level release of Qt.\n\nJust posting issues here gives you the 99% chance they won't be found by the relevant people.\n\nThanks,\nLars\n"
    author: "Lars Knoll"
  - subject: "[OT] QComboBox"
    date: 2005-07-16
    body: "some method descriptions are missing in http://doc.trolltech.com/4.0/qcombobox.html such as clear() removeItem() ..."
    author: "ac"
  - subject: "Re: [OT] QComboBox"
    date: 2005-07-16
    body: "You should better report documentation errors to Qt Bugs. (See http://doc.trolltech.com/4.0/bughowto.html for the email address.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
Qt Quarterly is a paper-based newsletter exclusively available to <a href="http://www.trolltech.com/">Trolltech</a>'s Qt customers. As a courtesy and convenience, a selection of delayed articles are made available online in HTML format. Some of the more interesting articles are &quot;<a href="http://doc.trolltech.com/qq/qq13-apis.html">Designing Qt-Style C++ APIs</a>&quot; by Matthias Ettrich, &quot;<a href="http://doc.trolltech.com/qq/qq12-qt4-iterators.html">Qt 4's New Style of Iterators</a>&quot; and 
&quot;<a href="http://doc.trolltech.com/qq/qq13-styles.html">The QStyle API in Qt 4</a>&quot; both by Jasmin Blanchette.




<!--break-->
