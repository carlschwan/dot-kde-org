---
title: "aKademy 2005: Website and Logo"
date:    2005-04-09
authors:
  - "jriddell"
slug:    akademy-2005-website-and-logo
comments:
  - subject: "Well done"
    date: 2005-04-09
    body: "Perhaps a little blue is missing (as blue is KDE's primary colour) but other than that: cool logo! The spanish flag is nicely integrated, as is the \"sun\" instead of the KDE-gear."
    author: "cniehaus"
---
The website for this year's aKademy launched today at <a href="http://conference2005.kde.org/">conference2005.kde.org</a>.  It features the winning logo <a href="http://www.kde-look.org/news/index.php?id=167">announced on KDE-Look</a>, <a href="http://www.kde-look.org/content/show.php?content=21280">Steve King's Blush Stroke logo</a>.  It gives <a href="http://conference2005.kde.org/overview_program.php">an overview of the program</a> and an appeal for <a href="
http://conference2005.kde.org/sponsors.php">sponsors</a>.








<!--break-->
<p><img src="http://conference2005.kde.org/pictures/logo.png" width="350" height="205" /><br />
<em>aKademy 2005 logo</em></p>

<p>Registration via the website will be open soon.</p>









