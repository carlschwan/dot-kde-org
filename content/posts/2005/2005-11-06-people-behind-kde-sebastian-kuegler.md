---
title: "People Behind KDE: Sebastian Kuegler"
date:    2005-11-06
authors:
  - "tbaarda"
slug:    people-behind-kde-sebastian-kuegler
comments:
  - subject: "guidance"
    date: 2005-11-06
    body: "looks pretty already in some parts. nice job!"
    author: "eol"
  - subject: "Re: guidance"
    date: 2005-11-06
    body: "In general I share the goals of Guidance. However, I have a couple of concerns regarding duplication of work and integration with existing KDE/QT tools: \n \n1) Shouldn't the \"Configuration - Disk & Filesystem Configuration\" module be accessible via remote:/ in Konqueror and be integrated with (or replace) KNetAttach?  \n \nI realize KNetAttach doesn't actually mount anything, but instead sets up network KIO slaves. I was thinking of modifying KNetAttach to mount KIO slaves onto the filesystem using KIO-FUSE http://kde.ground.cz/tiki-index.php?page=KIO+Fuse+Gateway \nso that all apps (including shell utilities) can open/save to network locations set up under KDE. \n \nI think it would also be nice to let users browse the SMB neighborhood and click on the computer they want rather than making them remember the remote computer's name and location of the remote directory. SMB4K ( http://smb4k.berlios.de/ ) has this nice GUI browsing of SMB networks, but unfortunately it's not integrated with either KNetAttach or Guidance. Instead of reinventing the wheel, we should combine the best of these tools. \n \n2) User Administration -- wouldn't it be better to use the YAST2 modules that pertain directly to these tasks? YAST2 is nicely modularized and built on QT, so I think select modules could be pretty painlessly integrated in mainline KDE. Not to mention the fact that YAST2 has been used for some time and is relatively bug-free. \n \n3) Display Configuration \u0097 A very thorny area due to the large variation in hardware. Wouldn't it be better focus work on improving the existing QT-based tool for configuring X-Servers (Sax2) and integrating Sax2 into mainline KDE?"
    author: "Vlad C."
  - subject: "Re: guidance"
    date: 2005-11-07
    body: "I would say: As much config tools integrated as possible. for a user it does not matter whether it is DE or System related."
    author: "gerd"
  - subject: "Re: guidance"
    date: 2005-11-07
    body: "I've taken this to the kubuntu-devel list. Here is a direct list to my reply:\n\nhttp://lists.ubuntu.com/archives/kubuntu-devel/2005-November/000528.html\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: guidance"
    date: 2005-11-10
    body: "Thanks for looking into these suggestions!"
    author: "Vlad C."
  - subject: "OT: anyone noticed.."
    date: 2005-11-07
    body: ".. that traffic on the SUSE mailing lists has almost stalled."
    author: "Anon"
  - subject: "Re: OT: anyone noticed.."
    date: 2005-11-07
    body: "got a link?"
    author: "Sam Weber"
  - subject: "Re: OT: anyone noticed.."
    date: 2005-11-07
    body: "It could be related to (hopefully a rumor) novell dropping KDE as the primary desktop in SuSE.  One of the ideas people mentioned on the #suse IRC channel was for everyone that was unhappy with the decision to show what the SuSE community would be without the support of the KDE user base."
    author: "Corbin"
---
He is a KDE developer, the founder of the Marketing Team for KDE 4 and a member of KDE's Dutch local group. His chinchilla acts as a role model for Wikipedia and he calls himself a "not-so-standard-but-still-very-much-a-geek". Today's <a href="http://people.kde.nl">People Behind KDE</a> interview is with <a href="http://people.kde.nl/sebastian.html">Sebastian K&#252;gler</a>.
<!--break-->
