---
title: "Linux Times: Interview with George Staikos"
date:    2005-01-13
authors:
  - "a"
slug:    linux-times-interview-george-staikos
comments:
  - subject: "KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "\"Another typical developer we get is one who works just on his own application. These people don't contribute to the KDE desktop often\"\n\nas one of those developers, i am bitterly insulted that the thousands of hours i have put into developing my app has not contributed to the KDE desktop at all.\n\nor maybe you're narrow-sighted and wrong.\n\nas you are belittling the developers you are supposed to represent, you should resign.\n"
    author: "4234124"
  - subject: "troll"
    date: 2005-01-13
    body: "."
    author: "c"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "I can not imagine that your interpretation is what George meant. My understanding is that he means actually kdelibs when he refers to \"KDE desktop\".\n"
    author: "MK"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "For me it's not only kdelibs but kdelibs+kdebase. The \"KDE desktop\" consists of the window manager, the (k)desktop, the panel, a file/web browser and a terminal emulator. The \"K Desktop Environment\" aka KDE consists of the \"KDE desktop\" and its applications."
    author: "Anonymous"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "I don't think there's any belittling going on here whatsoever.  The logic can be broken down as such:\n\n- We get many types of developers.\n- One type just works on their own application.\n- This one type seldom contributes to kdelibs.\n\nThat's factual, and hardly saying that the efforts aren't appreciated.  It's just commenting that some developers only work in their own little corner... a credo I've seen espoused with pride by many many KDE developers.  Some of the best apps are created by people like that - they don't contribute to the desktop beyond their app very often, but their contributions are important."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "I'm that kind of developer, I only work on my own\napplication. I felt flattered by Staikos his comment,\nwhile he states that I don't really contribute to\nthe desktop itself, my application does that, and\nthis makes the application important.\nI don't understand your reasoning, everybody knows\nthat the applications are kde's most important\nfeature.\n"
    author: "pieter"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "Either you contribute to the core kde desktop, or you don't.  If you do, then don't \"work just on your own application\".  If you don't contribute to the core kde desktop, then you clearly just work on your own application.  What part of that is so hard to understand, and so insulting?\n\nThere are plenty of people who write applications, and this is really a good thing.  It's a progression of sorts, where in the past KDE was made up almost entirely of people who contributed to the KDE core and their applications were shipped with the KDE tar packages.  Now we have hundreds of developers writing applications for KDE, and many people who contribute to the KDE core have never even heard of some of these people or applications.  It's a strong sign of growth."
    author: "George Staikos"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "I think there is another sort of KDE software project - development tools, in addition to core developers or application developers. For instance, language bindings or KDevelop are obviously neither an application or core library, they are tools for helping to write actual KDE apps."
    author: "Richard Dale"
  - subject: "Re: KDE desktop+experience excludes its apps - Sta"
    date: 2005-01-13
    body: "Translation tools... \n\nHowever the entrance barrier is still there."
    author: "gerd"
  - subject: "Be careful with your words"
    date: 2005-01-13
    body: "I guess \"Core\" made all the difference, didn't it?\n\nNo one would have \"complained\" if you said \"Core\", but you didn't."
    author: "Another dev"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-14
    body: "I guess people are disturb by the implication that KDE applications are not part of the KDE desktop. Applications are part of the KDE desktop. This is evident from the founding post:\nhttp://groups-beta.google.com/group/comp.os.linux.development.apps/msg/cb4b2d67ffc3ffce\n\nThe KDE Desktop consists of (at least) a Panel, Filemanager, mail client, easy texteditor, Terminal, Image viewer, Lots of small other tools, Hypertext Help System, Window Manager, System Tools, Games, Icons, Documentation, Web-Pages / Ftp Server / Aministration(sic), Discussion, and Applications.\n\nDon.\n"
    author: "Don"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-15
    body: "> Either you contribute to the core kde desktop, or you don't. If you do, then\n> don't \"work just on your own application\". If you don't contribute to the\n> core kde desktop, then you clearly just work on your own application. What\n> part of that is so hard to understand, and so insulting?\n\n1. you didn't state \"core\" which resulted in misunderstanding\n\n2. you meant \"core kde desktop\" so now your interview reply is pointless - what can a user do with a \"core kde desktop\" can you send an email or listen to sound with \"core kde\"? as you can see, there is no need to put \"core kde\" on a pedestal. i guess next time i'll try to get my work into \"core kde\" instead of the other modules just so that you recognise it as somehow more important.\n\nremember: modules such as kdewebdev or kdemultimedia DO contribute positively to the kde desktop as whole."
    author: "Yet another dev"
  - subject: "Re: KDE desktop+experience excludes its apps - Staikos"
    date: 2005-01-13
    body: "I'm also a developer who mostly works on his own stuff, I don't feel insulted by George's comment. I think you just read it wrongly."
    author: "Max Howell"
  - subject: "To contribute or not contribute"
    date: 2005-01-17
    body: "Contribution is measured as lines of code or bugs reported. It doesn't mean that you don't contribute to the community in other ways and nobody is telling you how a horrible selfish you are, unable to \"contribute\".\n\nImagine this: you are a developer, and you don't contribute to a project with opinions neither wishes nor comments... how does anybody know that you exists? You are merely a perception, a concept. There has to be developers out there, who doesn't speak and make no noises... but there they are. Is that insulting? NO! there is no way to contribute on every project on earth!!!\n\nIt's just the way things are."
    author: "Tristan Grimaux"
  - subject: "new Concepts ?"
    date: 2005-01-13
    body: "Will there ever be radical new concepts in KDE - or will it be always a system where the ideas are copied from another OS (mostly windows ?) \ni yesterday saw the concept of iWork from apple , and thought why koffice has had to be the same as Word for windows with its quadrozillion buttons. I like the template based idea and the easyness of the gui. They just thought how to make text editing the best they can , no to copy everything to make everyone happy.\n\nIt perhaps is not possible to make radical changes because the direction and the descisions how to implement things are not strong enough. There is no \"Direction\" where kde is headed , and many people will say this is good , but i dont think so.\nWhen Gnome introduced its new Spatial file-things, everyone screamed but i think that are the things a desktop env. needs. New Ways of doing things. Reduce the work you have with the Gui. Optimize the count of Clicks. \n\n?\n\ngreets chr"
    author: "chris"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "well, first, spatial wasn't new - its a very old concept, older than the 'browsing' concept used by konqueror.\n\nsecond, you are right - koffice isn't really doing new stuff. but that's because they first have to catch up to the 'mainstream' office products. you can't innovate if you can't even copy... copying takes less time, so you first copy a PROVEN concept, then enhance it.\n\nI'm sure there are a lot (paid) coders working on iWorks, while just a few are working on Koffice... and as long as there aren't more people working on Koffice, it won't be really innovative.\n\nYou should wonder why Microsoft doesn't innovate, with all its billions of $$$! They don't do much new, while their budgets are much much higher than Apple's.\n\nThere is innovation in Free Software, but only in the 'mature' software, software that is at least on par with their commercial counterparts, AND have sufficient manpower to innovate."
    author: "superstoned"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "The Gnome usability freak Seth Nickell even just wrote about this:\nhttp://www.gnome.org/~seth/blog/allworknoplay"
    author: "MaX"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "> Will there ever be radical new concepts in KDE - \n> or will it be always a system where the ideas are \n> copied from another OS (mostly windows ?\n\nI for one discovered with KDE a radical new way to play with the many \nthousands pictures I got from my digital camera. New Ways of doing things.\nIt reduce the work you have with the GUI. It optimiwed the count of Clicks.\nJust like you said.\nhttp://ktown.kde.org/kimdaba/\nhttp://ktown.kde.org/kimdaba/tour.htm\n\nThis is certainly not an idea copied from another OS, because the software\nthat comes with the digital camera on windows are usually utterly bad,\nand iPhoto from Apple does not quite shine as well as Kimdaba does\n(iPhoto is a too much \"classical\" app, think if it was a music player,\nit would be a clone of WinAmp while Kimdaba would be iTunes or amaroK)\n\nI'm sure you can find more...\n(amaroK, mentionned before, is another good example)"
    author: "jmfayard"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "You mean something similar to my desktop?\n\nSomething I have created entirely with stock KDE utilities and GUI menus (no editing of any config files)?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "How did you get the rounded corners for your panels?"
    author: "Boudewijn Rempt"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "Maybe a background that \"simulates\" the rounded corners?"
    author: "Christian Loose"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "With kde 3.3.2 that doesn't work, because the image is repeated for every element on the kicker; no doubt it's better with a current cvs version."
    author: "Boudewijn Rempt"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "O.K. showing such a nice screenshot and not telling us how you managed to do this is not fair:) So what style, color-scheme, icon scheme etc. are you using and how did you do the rounded corners? I didn't change my desktop for a long time, but this looks really cool:-)))"
    author: "Michael Thaler"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "I think it can be done really easy if he is using 3.3 or newer. Just pop into the theme manager, press create new theme. That's it, then he only have to publish the resulting theme file somewhere. "
    author: "Morty"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "Also, could you please tell us how you managed to get the tab bar on the bottom of konqueror?\n\nThanks!\n"
    author: "Me!"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "http://bugs.kde.org/show_bug.cgi?id=76156"
    author: "Anonymous"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "But the original poster said that the effect was achieved without editing any config files..   The solution to that bug is editing a config file."
    author: "Leo Spalteholz"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "KDE just copies from Windows (and other OS)?!?\nWhere's the original of KIO-slaves?\nVirtual Desktops were copied from Windows?\nFully flexible and customizable keyboard shortcuts and toolsbars were cpoied from???\n...\n"
    author: "Birdy"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "1.see the gui from apps and compate the workflow \n2.io-slave is nothing new - they are just protocol handlers...\n3.virutal desktops is an very very old unix thing \n4.i dont know about the shortcuts , but what is so \"revolutionary\" about it ?"
    author: "ch"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "1.) So what? Some apps look similar to ones existing before. Some work similar to ones existing before. Some applications copied ideas from KDE, and KDE copied ideas from many others - so what? Inventing everything from the beginning? Show a single MS product that wasn't \"copied\". Do you claim MS for not being innovative?\n\n2.) Using all those io-slaves was new! Because they are _really_ integrated in every application. I can access my imap-server, ssh-server, etc. even in any file-open dialog. Show me a GUI that had this feature before.\n\n3.) Yes they are known from Unix. But why is KDE told to be a Windows clone if it has a lot of features from other OS?\n\n4.) The flexibility! You can assign _all_ your shortcuts as you want. I haven't seen this before in any other GUI.\n"
    author: "Birdy"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "to 1. dont compare to windows - never! - perhaps make a workflow diagram and design your software around it. if you copy the workflow from windows you never get a better program just by adding better features!!\n\nto 2. you can enter http:// in many many locations in windows (file open/save dialogs , system run dialog , filemanager ...)\n\nto 3. i never said it should compare to windows or mac . it just should do the job very good. see 1.\n\nto 4. this is nice . but just an example : a friend of mine configured his windowmake with zillions of shortcuts and dropped the menus for that. he likes his system. but if i sit in front of it - i cant open/maximize/run a command or whatever. Dynamically shortcut would be cool , if , if you hold a key , there opens something like \"Auto-completions\" where you can see what commands you can access/are defined.\n"
    author: "ch"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "> you can enter http:// in many many locations \n> in windows (file open/save dialogs , system \n> run dialog , filemanager ...)\n\nKDE already had this almost right from it's start \nwhen something like this didn't exist at all in Windows.\n\n"
    author: "Torsten Rahn"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "And Windows does _only_ support http (and local files). Not ftp, scp, ... Not even https!\n"
    author: "Birdy"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "you forgot windows network AKA \\\\...."
    author: "ch"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "2.  Yes you can use http:// in some windows dialogs.  But KDE is infinitely more useful by supporting all the other protocols like ftp, ssh, sftp, etc etc...\nThis is the single biggest timesaver when I'm programming on KDE.  Remote files act identically to local files and I never have to worry about synchronizing or uploading or whatever.  No other OS/Environment has anything close to this."
    author: "Leo Spalteholz"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "yep, and where is the locate:/ kioslave in windows? and where is the system:/, locations:/, settings:/???? apt:/?"
    author: "superstoned"
  - subject: "Re: new Concepts ?"
    date: 2005-01-14
    body: "Not to forget the uber cool things like svn:/, ipod:/ or nomad:/."
    author: "Christian Loose"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "The too-many-buttons-on-a-toolbar complaint... That's getting so incredibly old; every webforum whiner whines about all those buttons. \n\nBut there are two reasons for the way toolbars are handled in KDE. The first is technical: the Qt toolbar class mimics windows toolbars, nor NeXT-style toolbars (which are now the norm in OS X; toolbars in OS 9 were mostly windows-style toolbars attached to the menubar or floating around). If that's the library you use, that's the kind of toolbars you get. There's nothing more incredibly stupid than not using the standard widgets your library provides you with. (Only yesterday I saw a Windows app that had re-implemented the split bar -- badly.)\n\nThe other reason is users. You will not believe this, but actually, most naive application users love toolbars, the more the better. I see them daily, typing their texts in the little letterbox left them by the default version of Word, picking the \"paste\" button without fail or hesitation from the crown. (Funny: from actual observation, most people seem to use the keyboard for copy, and the button for paste.)\n\nThat's for the toolbars... As for the rest of you two cents of rhetoric; iPage is a Pagemaker clone, nothing more, nothing less. The idea of a spatial file browser is as old as the Mac. Nothing new. And \"direction\" is what you get when you have directors in charge."
    author: "Boudewijn Rempt"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "Boudewijn,\n\nI agree with you. I've never ever had any problems with Konqueror toolbar buttons, and it really makes me wonder why some people have problems with them.\n\nEleknader\n"
    author: "Eleknader"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "I haven't seen iWork, and no doubt it will be interesting. The unfortunate reality of wordprocessing is that to make it useful it has to do many things that wordprocessing users expect. And until the software is able fulfill these expectations, 'new concepts' is usually a marketing term to hide the fact that users will be disappointed in many ways, ie. won't be able to do the things they need to do. Beautiful, elegant, easy to use, but ultimately useless.\n\nA quick example of how this plays out; the first thing I do with any wordprocessor is to set up the header/footer layout that we have. If I can't do it quickly and easily with similar results, I don't want to use it. Guess what wordprocessor didn't pass my simple test. Microsoft Word. KWord was innovative, slick, easy to use, contained new concepts, or whatever buzzword you want compared to MS Word. Ditto Wordperfect (Dos and Windows versions), Abiword, Microsoft Works, and the Lotus wordprocessor whose name escapes me.\n\nIsn't Kword fundamentally different from MSWord? It may have a toolbar with buttons and similar function, but to say it is a copy of MS Word would be quite a stretch. In other words the imaginative thinking may have already happened but the sleek look typical of Apple software caught your eye.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "When talking about KWord as a \"copy\" of MS Word and concepts I' like to mention Frames and Views. MS Word will never support these. Those are both new concepts for a wordprocessor.\n"
    author: "Birdy"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "Concerning the word processing side, you should have a look at this project :\nhttp://www.kde-apps.org/content/show.php?content=12725\n\nIf you really think that your ideas and concepts are really worth being seen in kde (buttons on the opposite side, windows popping up all the time, whatever), you should perhaps code a prototype and show it first."
    author: "ita"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "the article the guy mentioned above from the Gnome Guy describes it.\n\nyou have to make a new app to introduce new concepts. If a Programm is post 1.0 it get too many users which will complain id you change the concept."
    author: "ch"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "Nonsense.\n\nBack to the wordprocessor. It takes a mature application, version 1 at least to work at all. Work in the sense of being able to be even considered in a production environment. Reliable import/export, printing, mail merge, embedding other graphical components, and all the various stuff that wordprocessor users need.\n\nThe rest is interface. Experimenting with different interface ideas is possible when the core engine stuff is working. If it isn't, all you have is a nice facade that no one will (or can) use, and as a consequence, no one can test.\n\nConsider Konqueror web browser. All the playing with the interface is fun and nice, but if it doesn't reliably render the pages you need, it is of limited usefulness. Isn't that what happened with Firefox? A mature and reliable engine with a rewritten front end?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "I definitely agree. The concept I really would like  to see reinvented (but much different) is the K-menu / Start-menu. It is 10 years old now, there must be much much better user interfaces to find applications and settings in the system. The whole web revolution have passed since Windows 95 introduced it to a greater audience. A new interface for starting and browsing applications could be inspired by how you use the internet to accomplish many things. Think of how search for movie information and book tickets, work with your internet bank, order books on Amazon, search on Google. Most people are used to accomplish tasks on the web. Extend this thinking to the desktop!\n\n"
    author: "Claes"
  - subject: "Re: new Concepts ?"
    date: 2005-01-13
    body: "Try typing \"apps://\" (or is it \"applications://\" ... I forget and I'm at work so no KDE to play with) into the Konqueror address bar.  I find this kioslave along with the autocomplete in Konquerors address bar is my favourite to navigate to my applications."
    author: "ltmon"
  - subject: "Re: new Concepts ?"
    date: 2005-01-14
    body: "Even better is to add it to the sidebar, like 'Add New|Folder' with url applications:/ and same for settings:/ You can also assign a nice icon then."
    author: "koos"
  - subject: "that's it?"
    date: 2005-01-13
    body: "not much of an interview"
    author: "kade foster"
  - subject: "Re: that's it?"
    date: 2005-01-13
    body: "And not much of \"KDE Team\" either."
    author: "Anonymous"
  - subject: "There's a link one the top of the page ;-D"
    date: 2005-01-13
    body: "That is all"
    author: "Sam Weber"
---
I recently got a chance <a href="http://www.linuxtimes.net/modules.php?name=News&file=article&sid=644">to interview George Staikos</a>, the official representative for the KDE project in North America. He addresses some questions on the current status of the KDE project, and about the problems they have faced. 










<!--break-->
<p><i>Q: It has been eight years since the start of the KDE Project. At the very  end of the project announcement, Matthias said that he is a "dreamer". Has  his dream become a reality or is KDE still someway off?</i></p>
<p>A: I think it has.  KDE is used by an incredible number of people worldwide. It has matured into a stable platform and even sets the standards in many areas.  It's truly something to be proud of.</p>
Read more over <a href="http://www.linuxtimes.net/modules.php?name=News&file=article&sid=644">at Linux Times</a>.