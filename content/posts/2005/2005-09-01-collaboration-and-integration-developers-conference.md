---
title: "Collaboration and Integration at Developer's Conference"
date:    2005-09-01
authors:
  - "jriddell"
slug:    collaboration-and-integration-developers-conference
comments:
  - subject: "Usability"
    date: 2005-09-01
    body: "The useability study that Novell is producing is excellent and will give a lot of useful feedback. I have just a few points about it:\n\n* Only authenticated users with the relevant permissions should be able to add users. Adding a feature that will allow new users to be created from KDM or GDM is insane.\n\n* People who know about multiple desktops do use them.\n\n* The default configuration of KDE applications should be similar to MAC style interfaces. A small number of toolbar icons with a short text description below.\n\n* A lot of screen space is wasted in some KDE applications. A good example of this is KAddressBook. KAddressBook wastes screen space with the address book resources pane (who uses that?).\n\nGreat work.\n\n"
    author: "Ian Whiting"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> KAddressBook wastes screen space with the address book resources pane (who uses that?).\n\nYou can almost completely hide the resource pane by using the splitter above it.\n"
    author: "Christian Loose"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "You're missing the point, KDE-style ;-) The idea is that the default appearance of the app should meet, in the best way possible, the needs of the average user. Thus hide the resource pane by default (the average user won't need it) and have an option, in the View menu say, to display it. Only a more technical user will want to display it, and a more technical user will know to go to the View menu or settings to find out how to display it. \n\nThis is the thing with KDE. It's okay saying you can customise things, but it's better to default to a stripped down GUI to suit the most commonly used actions (with the option to turn on more features), than to turn on everything and expect an average user to find what they want within such a busy interface; to know that they can turn off things they don't use; and to know how to turn off things they don't use.\n\nIt's important to realise that KDE developers and enthusiasts aren't average computer users. Linux users aren't average computer users even. Average users are taught a very small number of very mechanical things to allow them to use computers for specific tasks. If Linux is to become a dominant desktop, it's going to have to suit users for whom using a computer does not come naturally.\n\nA computer enthusiast will always know how to turn on features, and won't especially mind doing it. But an average computer user won't, and will be quite fearful of venturing into settings menus."
    author: "Bryan Feeney"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> Thus hide the resource pane by default (the average user won't need it) \n\nAnd this opinion comes from which data source?"
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "It comes from common sense. Normal users just enter addresses in to addressbook. I know I do. Normal users are not concerned about \"resources\". I mean, what are those resources it refers to? LDAD directories and the like? How many normal users are running directory-servers in order to store their addresses? Very few. Sure, they might be used in corporate environments, but there the local IT could easily change KAB to display the resources-field. But in everyday use, it's merely a waste of screen-space.\n\nKAB is not the only one to blame here, Korganizer is another one.\n\nHell, even the KDE-docs talk about corporate uses when discussiong that particular feature:\n\nhttp://enterprise.kde.org/articles/korporatedesktop6.php#\n\nwhat about non-corporate-users? What about Joe Sixpack who wants to use KDE at home (yes, they DO exist!)? It seems that KDE expects them as well to run LDAD-servers and the like, since the \"resources\"-panel is given such a high priority in the UI. Why give such a high priority to a feature that is not that widely used in the end? Why not give features that ARE used all the time higher priority? I find it strange that some obscure (but \"powerful\") features are raised on a pedestal, whereas everyday usability and cleanliness of the UI is sacrificed.\n\nI'm just waiting for the \"but somewhere, there might be some user who uses that feature even at their home-machine now and then, therefore we need it there!\"-argument...."
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> what about non-corporate-users?\n\nYou can use the resource system to organize your addresses in different categories e.g. by using several resource files. \n\nThis way you can hide/show group of addresses in the address list very easily by just using the checkboxes in the resource pane."
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Uh-huh. And is that a widely-used feature? I don't think so. Most users simply want to add contacts to their addressbook. And the UI should accomodate that with minimum of fuzz. Yet we have large section of the UI dedicated to a feature that is not\n\na) a necessity\nb) widely used\n\nMost users are simply confused by \"resources\". And the whole feature is not that well explained so even if they might find it useful, they would propably not use it, since it's too confusing."
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "There was a bit of simul-posting going on there. Unlike Janne, I can actually see the use in what you're suggesting (at a very least I'd split mine between \"Work\" and \"Personal\"). \n\nHowever, I'm still not sure it's something every KDE user is going to do, and should therefore not be available on the default. Further, to ease its use, it should possibly be renamed from \"Address Resources\" to \"My Addressbooks\".\n\nI can see things are going to move along nice and smoothly with KDE 4!!"
    author: "Bryan Feeney"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Your renaming suggestion definitely makes sense. The current name probably scares most users away from this otherwise useful feature.\n\nHow about the following idea: Leave the resource panel in but default to a hidden state. IMHO the allows fast access to the panel without disturbing the \"normal\" users."
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Suggestion: Why not make the adress list view a tree view instead, with the address resources as the top nodes. Then you can show/hide different resources by folding/unfolding that branch. Maybe this could be accompanied by a checkbox to switch between flat view/tree view. That way you could sort the list without regard to address resources."
    author: "Mr X"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Tree views are generally considered more complex than other approaches (increases clutter and decreases visible area of the whole data etc.). I guess just improving the resources pane into a more user visible label/resources approach (with the easy ability to add/remove lables from single address entries) with be one of the best solutions."
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "Yeah, as I do..I created a resource called \"personal\", a resource called \"work\" and so on (well, guessing I need a vCard fileformat and that I need to change the filename to avoid data losses). Then I tried to sync with my Palm and I got synced only one resource (the default one). Then I merged everything into one resource and I discovered the labels thing, the default since decades in the Palm world and that's nicely supported by KABC. And now I'm an happy sync'ed man.\n\nSo, resources are useful only to join remote resources like LDAP, Exchange, Kolab ecc ecc and so that pane should be OBVIOUSLY not visible by default."
    author: "Davide Ferrari"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> what about non-corporate-users?\n\nWhat about corporate users?\n\nSee. We obviously have to make a choice which users we want to target. In any case, there's always room for improvement and we are open for suggestion. We have already realized the problems with the address book resource pane and we are going to address it. If the user does only have one resource then the pane will be hidden by default. If the (corporate) user has several address books then the pane will be shown, but we want to make it a bit simpler (basically just the list and probably one button) and move the more complex options (Add, Delete, Edit, ...) to a dialog.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "\"What about corporate users?\"\n\nCorporate users have a dedicated IT-staff whose sole purpose it to make the system suitable for the users. I should know, I'm one of that staff :). Home-users do not have that staff at hand. Sure, many have friends and relatives, but they would rather be doing something else than fixing their computers on their free time (I should know, I'm one of those as well).\n\n\"We obviously have to make a choice which users we want to target.\"\n\nDoes KDE have a clear goal on this? I mean, if KDE plans to target corporate-users, why does KDE ships with games :)?\n\nI do not think that KDE should \"target\" some specific users. OS X doesn't target any specific area, neither does Windows. But they are still usable in both corporate-settings and in homes. Being easy to use for regular Joe does not mean that the system is useless for corporate-users. Believe me, corporate-users are not always uber-users, they value simple interfaces as well.\n\nAnyway, I'm happy to see that the problem has been identified, and steps are being taken to correct it. Thanks :)!"
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: ">What about corporate users?\n\nThere is really no difference between corporate and non-corporate users, with the expection of IT staff. IMO IT staff have enough to do without running around teaching LUSERS how to use kaddressbook and what the resource pane is for. Really where is it that we think joe user works? Hmm, usually in some sort of corporation.\n\nI think the rule of thumb should be KISS Keep It Simple (mr.) Software-engineer. Target the lowest common denominator with the system defaults and leave advanced options there for advanced users and IT staff. Make the easy things easy and the hard things possible.\n\n"
    author: "Tormak"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "\"What about corporate users?\"\n\nWhat about them? Just because you have a stupid resource thingy that most users find useless doesn't mean that doesn't also apply to corporate users as well.\n\n\"We obviously have to make a choice which users we want to target.\"\n\nErrr, no, you just have to implement something that's sensible.\n\nI know many users who I have tried to encourage to use Kontact who've went off and used Thunderbird and Evolution because of some of the silly things Kontact and its components do. No one can fault the solidness of the technology, but most users simply cannot use it.\n\nThat's fine if that's what you want, but if you have any aspirations of getting Kontact used in businesses or in corporate environments like at Novell you can quite simply forget it unless it changes. That's not a threat or anything like that, but that's just the way it is. Stuff like using horizontal arrow keys in a vertical e-mail list...... It may prove to be a usability improvement in some ways, but you have to make it straightforward and logical for users to see and use and see that it is a problem for a lot of people. I have a feeling that won't happen though.\n\n\"If the (corporate) user has several address books then the pane will be shown, but we want to make it a bit simpler (basically just the list and probably one button) and move the more complex options (Add, Delete, Edit, ...) to a dialog.\"\n\nSounds like a step forward in the right direction, although I think you'll find that just about every corporate user does not have more than one address book."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> Errr, no, you just have to implement something that's sensible.\n\nErrr, he doesn't have to implement anything.\n\n> \"...\" just about every corporate user does not have more than one address book.\n\nWhere do you get those interesting facts from?\n\nThe corporation I work for has the following addressbooks:\n\n-> customer contacts\n-> global company addressbook\n-> local subsidiary addressbook\n "
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: ">>> We obviously have to make a choice which users we want to target.\n>> Errr, no, you just have to implement something that's sensible.\n> Errr, he doesn't have to implement anything.\n\nWhile it's true that open source developers do not have to implement anything, playing that particular card right now makes no sense. The original guy (a developer, presumably) said they had to do something. The guy above you was merely suggesting an alternative (though it was a vague one). Saying that the original guy doesn't have to do anything doesn't make any sense. It was the original guy (developer?) who was proposing the necessity in the first place.\n\nOpen source developers largely do what they do without pay, and are therefore not under anyone's control, but that fact should not be used simply to stymie any suggestion you don't agree with."
    author: "Dolio"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "> While it's true that open source developers do not have to implement anything, playing that particular card right now makes no sense\n\nIt does make sense because it is very important how you state your opinion.\n\nThere is a difference between \n\n\"you just have to implement something that's sensible\" \n\nand \n\n\"how about this idea: [..]. Wouldn't it be more sensible?\"\n\nIt always better to be polite when you want things to change. Especially in the Open Source world.\n\n"
    author: "cl"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "No, it was merely a useless jab at the way someone stated something. The first guy said, \"we have to do this.\" A perfectly reasonable counterpoint is, \"no, all you have to do is this,\" which is how anyone reasonable would interpret, \"you just have to implement something that's sensible,\" in context.\n\nIt's good to be polite, but when someone isn't as polite as is theoretically possible, it shouldn't be taken as an excuse to make random, irrelevant jabs at the wording of their argument. That's worse for resolving the situation than any initial impoliteness is."
    author: "Dolio"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "> No, it was merely a useless jab at the way someone stated something.\n\nThat is your interpretation. But it's not mine.\n\n> anyone reasonable would interpret\n\nCool, now you call me unreasonable. Thanks!\n\n> It's good to be polite, but when someone isn't as polite as is theoretically possible, it shouldn't be taken as an excuse to make random, irrelevant jabs at the wording of their argument. That's worse for resolving the situation than any initial impoliteness is.\n\nNow that's an interesting argument. So we should ignore impoliteness when the arguments are correct? No sorry. Not in my free time."
    author: "cl"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "I fail to see any impoliteness in it. Maybe you are just oversensitive? This isn't a diplamatic summit, you know, it's just normal people talking normally."
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"There is a difference between\n\n\"you just have to implement something that's sensible\"\n\nand\n\n\"how about this idea: [..]. Wouldn't it be more sensible?\"\"\n\nOnce you talk about using open source software in businesses and encouraging companies like Novell to use it then no, I'm afraid the rules of the game are different.\n\nBesides, when you suggest something more sensible you get ridiculous comments back that don't reflect real usage or stuff like \"We don't want to be like Windows and Outlook\"."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"Errr, he doesn't have to implement anything.\"\n\nFine. Don't do it then.\n\n\"Where do you get those interesting facts from?\"\n\nFrom being in a corporation.\n\n\"The corporation I work for has the following addressbooks:\"\n\nThey are addressbooks not created by users. They're created and used by default inside the business."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Janne basically beat me to the punch. Seriously, do you expect your mom to alter or modify an address resource once every time she launches the addressbook? Do you add, delete or modify address resources every time you launch the address book? Every fifth time, even?\n\nIt's just not something you'd do very often. There is an argument in KOrganizer for showing calendars from specific profiles, but that could be achieved with a combo-box buddied up to a \"Show events from\" label, located just above the calendar pane (like the search pane in Thunderbird). \n\nOne thing that would be interesting would be to create a GUI-activity profiler, and see which actions (most likely KActions in fact) are triggered and how often in the course of several usage sessions. I imagine that in both the hardcore developer and the basic computer user would end up with broadly similar usage profiles.\n"
    author: "Bryan Feeney"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> Janne basically beat me to the punch. Seriously, do you expect your mom to \n> alter or modify an address resource once every time she launches the \n> addressbook? Do you add, delete or modify address resources every time you\n> launch the address book? Every fifth time, even?\n\nNo, no and no. I completely agree with Bryan that there are certain problems and as I wrote above we are going to address those problems."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "\"No, no and no. I completely agree with Bryan that there are certain problems and as I wrote above we are going to address those problems.\"\n\nThanks Ingo, and it's nice to hear that people are thinking about it."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "Great stuff, it sounds like a sensible solution. Any chance it could be relabelled \"My Addressbooks\" *ducks* ;-)"
    author: "Bryan Feeney"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "Sheesh, even Microsoft is dropping the childish \"My\" prefixes those days.\n\n \n\n\n"
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "They're dropping them because they feel people don't need them any more. They were added in the first place to give a sense of ownership and to make the whole thing feel a bit more welcome. Personally, I think that logic still stands.\n\nFrankly, anyone who's seen the state of the Vista GUI would have to wonder about Microsoft's grasp of usability lately."
    author: "Bryan Feeney"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "I agree 100% with Bryan. It's what I've been preaching for years:\n\n/Sane defaults/ are essential.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "I agree, but i think neither the corporate user nor \"Joe Sixpack\" is downloading the original KDE packages.\n\nThey are using a distribution.\n\nSo the \"sane defaults\" for their targeted audience should be chosen by the distribution vendor, at least because otherwise they are not going to be very successfull."
    author: "Alexander Holy"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> So the \"sane defaults\" for their targeted audience should be chosen\n> by the distribution vendor, at least because otherwise they are\n> not going to be very successfull.\n\nDistribution vendors don't customize kde. Perhaps they should, but they don't. That's just it. (kubuntu does do a bit of this, though)"
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: ">Distribution vendors don't customize kde.\n\nWrong, they do. Some more than others, but nearly all do. With expection of Slackware all the major distributions makes modifications/customizations to KDE. "
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "They do, but I never noticed them significantly improving kde whilst customizing. The point is, the 'evil' has to be cut at its roots."
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "Perhaps you are not looking at it right then. When commercial distributions spends time and money to customize KDE, they do it for reasons. Even if those changes are not significant to you, they perhaps are for the customers of the distribution. And since none seems to bother with the 'evil' as you call it, it's perhaps because it's insignificant and not important at all for their customers. "
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-04
    body: "OR they just don't have (or want to waste) the resources to do it...\n\nIf kde would have more sensible defaults (btw imho they ARE quite sensible, but they could be better) all these linux distributions wouldn't have to spend time to make it easier to use, thus they could concentrate on fixing bugs or making things better. and they wouldn't prefer gnome over KDE because it is 'out of the box' easier to use.\n\nthus it would lead to more resources spend on making KDE better instead of usable. wouldn't that be great?"
    author: "superstoned"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "> So the \"sane defaults\" for their targeted audience should be chosen\n> by the distribution vendor, at least because otherwise they are\n> not going to be very successfull.\n\nDistribution vendors don't customize kde. Perhaps they should, but they don't. That's just it. (kubuntu does do a bit of this, though)"
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"I agree, but i think neither the corporate user nor \"Joe Sixpack\" is downloading the original KDE packages.\n\nThey are using a distribution.\"\n\nI remember when Red Hat \"customized\" KDE. The result was a storm in a teacup, when many KDE-developers felt that they were ruining KDE.\n\nWho knows KDE best? I would say that the KDE-developers do. Then why should KDE-developers rely on people who do NOT know KDE as well as they do to clean up KDE? Why shouldn't the problems in KDE be solved by the people who know it the best, instead of some other people who have no intimate knowledge of KDE?\n\nAnd relying on distributors to do the job (and they do not do the job. Even SUSE's KDE (which is considered the best KDE-distro) is sub-optimal) results in several slightly different KDE's. Instead of having one default KDE that makes sense, we would have several slightly different KDE's. Not only does that increase the burden on the distributors who have to spend time cleaning up KDE, it dilutes KDE's brand. If the \"pass the puck to distros\"-people had their way, people would not use KDE. They would be using SUSE-KDE, Mandriva-KDE, Red Hat-KDE (shudder), Gentoo-KDE etc.\n\nKDE should be as easy as possible to distros and to users. And fact is that expecting the distros to customize KDE increases their workload, making KDE less appealing to distros. And fact is that many distros do a poor job at cleaning up KDE, or they don't do it at all. So the end-result is a KDE that is confusing to the end-users. Of course it could be said that \"It's not KDE's fault! It's the responsibility of the distro to make our product kick ass!\" is a cop-out. Distros would rather be doing something else. I bet the distros would much rather choose a KDE that kicks ass by default, over a KDE that requires serious cleaning up and hand-holding."
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "From your commnet it sounds like you think the distributors live in their own universe and nothing they do gets back into KDE. Since this is clearly not the case, and most of the changes the distributions do to KDE that makes sense finds it way back to KDE. Since the distributions have much better understanding of their customers/users, they are in a much better position to choose suiteable defaults. That's the reason the default in Red Hat differs from the default in Linspire, and why uncustomized KDE differ from both.\n\nAnother possibility you fail to consider is that perhaps KDE does not need the serious cleaning up you are talking about. The fact is, none of the distributions do anything major regarding 'cleaning up' the UI. Even tho this is rather easily done in most cases, by editing simple XML files. Since the distributions actually communicate with their customers and they still don't do those simple changes, it looks more like a minimal- or non-issue and not the serious problem some people try to make it. \n\nAnd regarding your reference to the Red Hat debacle, the problem was that they broke compability with stock KDE(Not only binary, but source too). Making difficulties for 3rd party KDE programs. And most of those changes could have been done in the RC files anyway."
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"From your commnet it sounds like you think the distributors live in their own universe and nothing they do gets back into KDE. Since this is clearly not the case, and most of the changes the distributions do to KDE that makes sense finds it way back to KDE.\"\n\nI haven't yet seen any reduction in the abundant number of apps that ships with KDE. During the KDE3-era, I have seen some changes in the default style, and few dropped icons from toolbars (note: I'm talking about cleaning up here, features are a different matter). Yes, changes take place, but very, very slowly.\n\n\"Since the distributions have much better understanding of their customers/users, they are in a much better position to choose suiteable defaults.\"\n\nAnd many of them don't do it at all, but offer a default KDE instead. Seriously, I  have really hard time following this logic that KDE-project can ship just about any kind desktop, since it's the distro's responsibility to clean it up. What about users of those distros who use more or less the default KDE? It's now the respsonsibility of the user to clean KDE up?\n\nMy suggestion is pretty simple: instead of relying on others to make sure KDE kicks ass, why not make KDE kick ass right at the source? That way ALL users (users of distro-KDE and default-KDE) would benefit from having a kick-ass KDE. And it would mje mostly the same KDE across the board.\n\n\"Another possibility you fail to consider is that perhaps KDE does not need the serious cleaning up you are talking about.\"\n\nIMO, it does. I love KDE and I think it needs cleaning up. Lots and lots of people around the internet have voiced their opinion on how KDE needs cleaning up. Of course, it's always possible to say \"but those people are GNOME-trolls!\" or something, and ignore them. But fact remains that many users feel that KDE is simply too busy, too cluttered and simply too much.\n\nExample of this (I have used this example before): When my wife asked me how to write some text in KDE, I told her to use the Kmenu, to access the \"Editors\"-category. When I mentioned that in kdedevelopers, the response I got was \"Are you crazy? Do you want your wife to suffer?\". I think that shows quite well the state of KDE. It's simply overloaded with stuff, and something as simple as navigating the Kmenu can be seen as \"painful\" by newbies.\n\n\"Even tho this is rather easily done in most cases, by editing simple XML files.\"\n\nIs it? Is it easy to remove, merge and condence the menu's in the menubars? Is it easy to remove the excessive lines and borders from Konqueror and other apps? is it easy to split up the monolithic KDE-packages, and rip out the unneeded/redundant apps? Is it easy to turn Kcontrol in to something that mere mortals could use? Tell me how to do those \"easily\", and I might do it myself.\n\n\"And regarding your reference to the Red Hat debacle, the problem was that they broke compability with stock KDE(Not only binary, but source too). Making difficulties for 3rd party KDE programs.\"\n\nThat was part of the argument, perhaps. But other part of the argument was the \"OMG, they are turning KDE in to Gnome!\"-thingy."
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"I haven't yet seen any reduction in the abundant number of apps that ships with KDE.\"\n\nHave it ever entered your mind that it may not be a big problem at all? Since no one, KDE developers or distributions with paying customers, put any big amount of resources into changing it. Had it been a major issue for the distributions and their customers don't you think they would have allocated resources to fix it a long time ago? Afterall, most of them even have experienced KDE hackers on their payroll.\n\n\n\"Seriously, I have really hard time following this logic that KDE-project can ship just about any kind desktop, since it's the distro's responsibility to clean it up.\"\n\nYour logic is flawed, as the base you have is that KDE is broken and need a massive cleanup. So you are not folowing the logic at all. Had it been a big problem the distributions would have started fixing it and reworking it in a major way years ago. The logic are that since Suse and Linspire base it on their customers need, they use different defaults. And since none of them do radical changes, clearly KDE can't bee too broken. It's not about distro's responsibility to clean it up, but to adopt it to their customers need.\n\n\n\"But fact remains that many users feel that KDE is simply too busy, too cluttered and simply too much.\"\n\nThe fact is that KDE have millions of users and the number keep increasing, and distributions keep selling their KDE based solutions. All pointing to another fact that the so called clutter simply are not a big problem at all. I'm not saying things can't be done better, but it's clearly not a big problem.\n\n\n\"Is it easy to remove, merge and condense the menu's in the menubars?\"\n\nAll that's XML-GUI, so yes it's relatively simple. XML is not that simple, but it's doable. Try look-kde.org, there have been shown several examples of it there.\n\n\n\"Is it easy to remove the excessive lines and borders from Konqueror and other apps?\"\n\nWhat's not RC based of that is style/theme specific, and several distributions have their own styles. Some have even more lines and such, so perhaps they don't see it as a problem\n\n\n\"is it easy to split up the monolithic KDE-packages, and rip out the unneeded/redundant apps?\"\n\nRedundancy is in the eye of the beholder, but yes. And all modern distributions does this. Strangely most of them install both Vim and Emacs by default too, so I'd guess redundant apps a real problem. And if you compile from source it's just as easy, and you save time by not compiling everything.\n\n\n>\"The problem was that they broke compability with stock KDE\"\n\"That was part of the argument, perhaps.\"\n\nThat was the only argument(expect some statements along the lines with: that's ugly/stupid/userunfriendly), everything else was from the usual internet trolls."
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "> Your logic is flawed, as the base you have is that KDE is broken and need a  \n> massive cleanup.\n\nNope. Whether Janne's logic is flawed or not has nothing to do with Janne's 'base'. The argument's assumptions and the logic are different things. Think at it like this:\n\nJanne's base: \"Some things in KDE need improvement.\"\n\nJanne's logic: \"If something in KDE needs improvement, then it is best done by KDE itself.\"\n\nYour logic: \"If distributors don't do it, KDE doesn't need to do it. If something needed fixing, distributors would do it.\"\n\nThe consequence of your logic: \"Whatever needs to be done _in_ KDE, does not need to be done _by_ KDE.\"\n\nNow I know that this is not what you meant to say, but it follows from your logic. Ergo, your logic is flawed, and Janne's is correct. Whether Janne's assumption is also correct is another point (but the claim that \"some things need fixing\" is lax enough to be impossibly false.)\n\n> The fact is that KDE have millions of users and the number keep increasing, \n> and distributions keep selling their KDE based solutions. All pointing to \n> another fact that the so called clutter simply are not a big problem at all.\n\nThis is a non sequitur fallacy. Millions of people are breathing toxical air, and their number keeps increasing. And it's still a big problem. (Oh yes, and millions of users are using windows and its viruses, which still doesn't prove that viruses are not a big problem).\n\n> Redundancy is in the eye of the beholder\n\nRedundancy has mathematical definitions of it's own, it's not in the eye of the beholder, unless you claim that compression is also in the eyes of the beholder.\n\nWhenever one has n things doing something which could be accomplished by a strict subset of m things (m<n), one has redundancy. The fact that (say) noatun is redundant does not lie in the eyes of the beholder. \n"
    author: "mihnea"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "Wrong interpretation of what I said, and taking it out of context at that. Making a generic example out of a specific case does not help either. The validity of the logic is decided by the problem it's applied to. A small change to your interpretation makes it match my logic \"If something badly needs fixing, distributors will allocate resources to fix it. But if the issues are less important, they don't.\"  \n\n\nEven applying the strict mathematical definition of redundancy in this case makes my statement correct, because you also have to include rather complex factors as the users work habits and ways of handling work flow.  \n\n>The fact that (say) noatun is redundant does not lie in the eyes of the beholder.\n\nRather nice example really, since it's not redundant to me. The multimedia package give me tree different media players, while they have some overlapping functionality they have very different uses. Kaboodle are used for one shot, single file playing. Juk manages and plays the resident music collection on my harddrive. While Noatun holds the playlists for internet radio stations and the music video collection. So for me it's not redundant, but it may be for a person only listening to his mp3 collection."
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "> Wrong interpretation of what I said\n\nThat may be the case. If it is the case, I'm sorry.\n\n> Making a generic example out of a specific case does not help either.\n\nIt does, when it comes to logic, and we are not maling an example but a counterexample.\n\n> The validity of the logic is decided by the problem it's applied to.\n\nThis is mostly correct, if by 'logic' we mean 'principles', 'rules of action'. However if we take 'logic' in its proper acception, it's wrong. Logic is a way of extracting conclusions from premises, and it doesn't matter to logic what the premises are. Take this for an example:\n\n'The moon is blue and schweitzer comes form Switzerland, therefore the moon is blue.'\n\nThe above sentence is perfectly correct from a logical point of view, and it doesn't matter that we are applying it to a false premise.\n\nNow you are saying that \n\n> \"If something badly needs fixing, distributors will allocate resources to fix  > it. But if the issues are less important, they don't.\"\n\nIf you take this as a _premise_, you can safely infer that 'something' (in this case KAddressbook's 'reources' pane, does not need fixing (as long as it's true that distributors don't fix it.\n\nBut when Janne was saying that you logic does not hold, this meant something else: it was regarding this sentence : \"If something badly needs fixing, distributors will allocate resources to fix  it\" as your _logic_, not as your premise. And it is indeed your logic, because it's a rule of inference (it has the form 'if ... then ...').\n\nAnd form this rule follows that whatever distributors do not fix, needs not be fixed (or at least not badly). But this conclusion does not apply to reality. X11 systems needed a consistent desktop, but no distributor allocated resources to fix this. Besides, commercial distributors have their businesses to mind, aand they probably wouldn't find it doable to apply the same lot of changes each time they upgrade KDE. This would be inefficient. Think of it as a matter of code factorization through inheritance: where would you want too write one and the same thing: in the super class, or in each of the inheritors?\n\nCommercial distributors might sponsor improvements if they occured inside KDE (at the root), but I wouldn't expect them to make the improvements on their branch just to have something to port and maintain and than port again.\n\nON the other hand I'm not a KDE developer (I don't fancy C++ that much, I prefer safe threads :) ), so it might be that I'm arguing in vain. Anyway, maybe somebody reads this aand finds it relevant.\n\n\n "
    author: "mihnea"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "\"Have it ever entered your mind that it may not be a big problem at all?\"\n\nI remember a recent discussion in kde-devel (or was it core-devel?) where the guy who packages KDE for FreeBSD said that yes, it is a problem. So it seems that there are others besides clueless users who feel this way.\n\nYes, the number of apps is a problem. Users are confused when they are presented by zillion apps. Like it or not, users want that someone makes the decisions for them. If they have to choose from multitude of apps, they get confused. If they have to choose from several apps that do the exact same thing, they are even more confused. And they get confused when they have to find that one app they are looking for among dozens of other apps. I have seen this with my own eyes.\n\n\"And since none of them do radical changes, clearly KDE can't bee too broken.\"\n\nOr maybe they have something better to do?\n\n\"The fact is that KDE have millions of users and the number keep increasing, and distributions keep selling their KDE based solutions.\"\n\nMicrosoft has a lot more users than KDE does. So I guess Windows is simply kick-ass product, since it has so many users? Comapring the user-bases of KDE and Windows, Windows clearly wins. Therefore, Windows is a better product? I guess you want to use the best product possible, so I assume you use Windows?\n\nYou can't simply think that \"we have lots of users. This proves that we don't have to improve our product\". \n\n\"What's not RC based of that is style/theme specific, and several distributions have their own styles. Some have even more lines and such, so perhaps they don't see it as a problem\"\n\nI haven't seen any style that removes those excessive lines and borders. Do you have any specific example in mind?\n\n\"All that's XML-GUI, so yes it's relatively simple. XML is not that simple, but it's doable.\"\n\nSo is it simple or not? Apparently it's simple, yet XML is \"not that simple\". Which way is it?"
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "\"I remember a recent discussion in kde-devel (or was it core-devel?) where the guy who packages KDE for FreeBSD said that yes, it is a problem.\"\n\nOut of the countless different individuals who packages KDE, one of them seem to have some problems splitting the packages(Actually from the discussion it sounded more like the logistics of handling the packages was his problem, not the splitting part. But I may have got the wrong impression.). And I guess it's mostly related to the way the packaging system used by FreeBSD works in any case, as DEB and RPM based system does not have such problems. Debian does not only split the different apps, they split almost everything into app, libs and devel packages. Making both splitting and handling large amount of packages routine.\n\n\"And they get confused when they have to find that one app they are looking for among dozens of other apps. I have seen this with my own eyes.\"\n\nThe complete computer illiterate class of users, I have seen them too. On the other hand they are not only a minuscule minority, they also very fast(in most cases) graduate out of that class. Basing any system on that class of users will in the long run become counterproductive and hurt the real users. It's like defaulting cars to max speed of 40, because some people are incapable of driving faster.\n\n\"Or maybe they have something better to do?\"\n\nSince KDE only have some minor problems compared to other areas of their products, they rather spend their resources on the more important problems. The ones helping them get more customers, and making more money. So I'd guess you are right, they have something better to do.\n\n\"Windows is a better product? I guess you want to use the best product possible, so I assume you use Windows?\"\n\nIn several areas Windows are a better product, that's plain fact. And yes, I will undoubtfully use Windows in cases where it has superior or the only solutions. Other times it's a simple evaluation of the pros and cons, and for the task I do now KDE comes out ahead. \n\n\"You can't simply think that \"we have lots of users. This proves that we don't have to improve our product\".\"\n\nI do not, and stop trying to implicate I do. You can always improve. What I am saying is that you keep maximizing something as a mayor problem, when ALL evidence says it's not. Had it been a major problem the business intrest had allocated resources to fix it, or lost customers. Since neither happens, it clearly are no more than minor issues.\n\n\"I haven't seen any style that removes those excessive lines and borders.\"\n\nNeither have I, so perhaps it's not a issue for the majority of users. Since several exist, but non does anything about it. Does it not speak of the low importance of the issue?\n\n\"So is it simple or not?\"\n\nXML requires some skill, but other than that it's simple. "
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-04
    body: "\"The complete computer illiterate class of users, I have seen them too. On the other hand they are not only a minuscule minority\"\n\nALL computer-users are \"complete computer illiterates\" in the beginning. And if they had to choose between simple and easy to use system, and a complex lumbering beast, they would choose the easy to use system. My wife is a good example. She likes using OS X, because the UI is so clean and uncluttered. Of course you could say that \"well, if she giives KDE a chance, she would learn to love it as well\". But she doesn't want to do that. She can choose between KDE and OS X. And even though I have tried telling her of the good features in KDE and I have tried cealing it up, she still prefers OS X. And at this rate, she will not be using KDE in the future, because the busyness and compexity of the system has turned her off of it. She doesn't want to \"learn to love\" the GUI, she just wants to use it. \n\nKDE does not cater to newbies at all. It caters to power-users. But those power-users were newbies once. And those newbies prefer simple system that doesn't get in their way. So they start using those simpler systems, and there's a good chance that they will stay with those simpler systems inn the future.\n\n\"What I am saying is that you keep maximizing something as a mayor problem, when ALL evidence says it's not.\"\n\nConsidering how much people complain about the busyness and complexiity of KDE, I find your claim that \"ALL evidence says this is not a problem\" rather weird. Every single time KDE makes a release, people all over the net complain how busy and complex it is. Of course lots of people love KDE (myself included). But many times even those KDE-lovers complain that it's too confusing and busy.\n\nI guess you could just label those complainers as \"Gnome trolls\" or something, and ignore them.... But still, trying to claim that \"there is no evidence that this is a problem\" is just plain wrong. All you have to do is to listen to what the users are saying.\n\nI have used OS X. And I have tried Gnome. And yes. of the three, KDE is by far the most cluttered and busiest. And it does not have to be like that.\n\n\"Had it been a major problem the business intrest had allocated resources to fix it, or lost customers.\"\n\nWell, KDE HAS lost customers. I think it's safe to say that Ubuntu and Fedora are the two most widely talked about distros right now. What desktop do they use by default? KDE does have SUSE, but it seems that SUSE is not as widely used as those two.\n\n\"Neither have I, so perhaps it's not a issue for the majority of users. Since several exist, but non does anything about it. Does it not speak of the low importance of the issue?\"\n\nOr maybe it can't be fixed by a simple theme?"
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-09-04
    body: "\"Considering how much people complain about the busyness and complexiity of KDE, I find your claim that \"ALL evidence says this is not a problem\" rather weird.\"\n\nGiven the number of users KDE has, the number of complaints are very very minor. Even take this site, there are only a few people complaining and calling it a major problem compared to the number of visitors. Even if the 4-5 regulars complain loudly, it does not change that fact that for the wast majority it's not a problem. You keep maximixing your pet peeves. \n\n\"I think it's safe to say that Ubuntu and Fedora are the two most widely talked about distros right now. What desktop do they use by default?\"\n\nAnd with all the hype, the number of KDE users still increase more than the competition. Go figure."
    author: "Morty"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "there are lots of things that can be enhanced in KAddressbook, altough it is already a nice application.\n\none of the things that would be nice is the abillity to change name and other info in the preview pane, without having to open the edit dialogue."
    author: "superstoned"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Why would you want that? That makes the editor dialog redundant And how many times do you change a person's name? Not that often I guess."
    author: "Bram Schoenmakers"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "Probably because Mac OSX has it. ;)"
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "We're making KDE, not a clone of Mac OS X. Just because Mac OS X has some feature doesn't mean that KDE should have it too."
    author: "Bram Schoenmakers"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "You missed the small smiley."
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "Yeah I know. In a few minutes I'm gonna pack my stuff and go the residents to have some sleep :)"
    author: "Bram Schoenmakers"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "well, if it works without making it more complex, why not make the editor dialogue redundant??? it is a complex piece, imho. things like name, adress and picture should be much easier to change. at least I tried to change these without opening edit, the first time. i thought it should work...\n\n\nbtw i dunno how mac OS X does it (don't have it) so that has nothing to do with it. but if they allow easier changing of this information, kudos to them. generally, one only changes name, adress, im, picture, birthdate and categories. if you could change these in a easy way, why not allow it?"
    author: "superstoned"
  - subject: "Re: Usability"
    date: 2005-09-01
    body: "\"They found that good icons are nice but words are what really helps people.\"\n\nAnother thing I've known for a while. Make toolbars have text under the icons, where possible, by default. Don't just give them a load of incomprehensible icons. That would help users no end."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "well, it is easy to set up, and it'll be available in all KDE applications. search in the control panel...."
    author: "superstoned"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"well, it is easy to set up, and it'll be available in all KDE applications. search in the control panel....\"\n\nNo, no, no, no, no, you're side-stepping it in the usual KDE fashion. It has to be on by default if it makes sense, and it certainly makes sense. No one should have to go looking in the control panel to turn something on like that."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "Is it so hard to right-click on a toolbar?\nAnd it doesn't make sense everytime. On 1024x768 display and lower it uses a lot of screen space. And once you've learned what an icon means (which you can do also by moving the mouse over it) screen space is much valuable than a text which is redundant for you (as you already know what the icon means)."
    author: "Andras Mantia"
  - subject: "Re: Usability"
    date: 2005-09-02
    body: "\"Is it so hard to right-click on a toolbar?\"\n\nHave you read the transcript of the desktop presentation or seen the video? The point is, you shouldn't need to to it for something like that. Icons by are a good visual aid, but by themselves they simply don't help. Text is what's necessary, and that's what he was talking about.\n\n\"And it doesn't make sense everytime. On 1024x768 display and lower it uses a lot of screen space.\"\n\nThat's not a user's problem. It should scale adequately.\n\n\"And once you've learned what an icon means (which you can do also by moving the mouse over it) screen space is much valuable than a text which is redundant for you\"\n\nNo it isn't. What we're talking about here is the time it takes you to think about what icon it is that you need, even though you might 'know' what it is. You have to be able to use the whole desktop more unconsciously than that. Those few milliseconds, or seconds, that you spend dithering about what icon you need makes all the difference."
    author: "Segedunum"
  - subject: "Re: Usability"
    date: 2005-09-03
    body: "> No one should have to go looking in the control panel to turn something on \n> like that.\n\nFully agree. Besides, the real problem isn't even that one has to go to the control center, the real problem is that users do not know that this one precise setting would help them, so they don't do it. Not only are they non-developers, they also have no knowledge of what would make their desktop more efficient. Users don't (and at this point I'm fairly sure that I mean all user categories) - they don't run usability labs just to find out what would be better for them. \n\nThis means that providing 'sane defaults' is also a way of providing valuable information, and not providing good defaults whilst expecting the users to reconfigure their dektop means a lot more than expecting them to \"right click there etc.\" It means expecting them to also know _what_ to do, not only how to use the kcontrol or the toolbar editor in order to do it.\n"
    author: "mihnea"
  - subject: "Re: Usability"
    date: 2005-09-06
    body: "* Only authenticated users with the relevant permissions should be able to add users. Adding a feature that will allow new users to be created from KDM or GDM is insane.\n\nI disagree.   On my HOME systems I want my guests to be able to create a personal account on my machine from KDM.   At work this feature makes no sense.   At home it would be nice to give them an automatic account.   Right now I have a guest account, but the password is a target of a lot of ssh attacks (second only to root).  (I mitigate this by setting the shell to nologin in /etc/passwd, and having kdm allow passwordless logins, but this isn't a good idea)\n\nSome features only make sense for some people."
    author: "bliGill"
  - subject: "Impressive"
    date: 2005-09-01
    body: "I am reading all the blogs about akademy and this year seems really impressive. Lot of things seem to be going on, and we have a big number of hackers together for more than one week. A lot of things will certainly come out of this.\n\nWho could think 5 or 10 years ago, that free software would evolve to a point where hackers can meet on an international basis for hacking free software, with corporate sponsors ?"
    author: "Philippe Fremy"
  - subject: "Oxygen"
    date: 2005-09-02
    body: "Looking at the linked webpage for Oxygen, I got a bit worried:\n\n\"1 - for hardware like printers, usb sticks, monitors and so on : Realistic design with a frontal high view when possible. \n2- for media things like CDs, floppies, memory card I would like the same perspective of mimetypes, with the object lying on the desktop.\"\n\nIsn't this a plain copy of the ideas of the Mac OSX icon set? :-\\\n"
    author: "Anonymous Koward"
  - subject: "Re: Oxygen"
    date: 2005-09-02
    body: "\"Isn't this a plain copy of the ideas of the Mac OSX icon set? :-\\\"\n\nMaybe OS X got it right? Suppsoe that Windows had icons that resembled things that are related to the task (like, an icon of printer for printing). Would KDE be \"copying Windows\" if KDE had icons that resembled things that are related to the task (like, having an icon of printer for printing)? Would KDE have to do things in different way, even though the end-result would be harder to use, just so KDE wouldn't be \"copying Windows\"?\n\nIf some other OS does something the way it should be done, there's no harm in doing it the same way in KDE."
    author: "Janne"
  - subject: "Re: Oxygen"
    date: 2005-09-04
    body: "I was referring to the more stylistical aspects: \"realistic\" and \"frontal high view\" of hardware devices, \"object lying on the desktop\" for mimetypes ('paper sheet' icons).\n\nRemember when BeOS came up with icons with isometric perspective and many free icon sets (including KDE, in 1.x/2.x era) copied it?\n\nThen it was the translucent mania, with \"bubble\"/\"aqua\" icons everywhere (Crystal), copying the overall looks of Mac OS X.\n\nLooks like it's the same thing, only copying now the style of the Mac's photo-realistic icons (frontal high view of devices, etc.)\n\nThere's nothing inherently wrong about that, but I'd really like to see KDE have an 'original' look and I thought this time would be it. I don't mean to bash the excellent work the KDE people is putting into it (I thank you all) -- this is just meant to be constructive criticism.\n\nSo, if any of the Appeal guys is listening: please don't make icons that look taken straight out of the Mac. Actually, that would be a good test: put some  new-KDE icons in a Mac desktop and vice-versa; if they _don't_ look out-of-place, then we have a problem... Icons are a key component in an environment's visual identity; KDE icons need a look of their own.\n"
    author: "Anonymous Koward"
  - subject: "Re: Oxygen"
    date: 2005-09-02
    body: "I get a bit worried about the apple style 'keep it under wraps and make a big splash in kde4' approach to the Oxygen icon set. The icon examples on the web site look beautiful, but surely one should follow the open source 'release early, release often' mantra in this respect too? If you are going off course with the icon set in terms of useability etc. it would surely be good to know early on in the process. I am sure it will still be a surprise to Joe User when Qt4 comes out as they are unlikely to be following the development process anyway, and those that do care about these things could at least contribute suggests during the process....\n\nJust my 2x. The oxygen icons that you have previewed look lovely so keep up the good work anyway...\n\nTim"
    author: "Tim Sutton"
  - subject: "Re: Oxygen"
    date: 2005-09-02
    body: "Just in case you missed it: there will be alpha and beta releases of KDE 4 as well. There is no need to make the set popular and in widespread use long before the set is finished and system of which Oxygen is supposed to be part of is even publically available outside svn. What you see here is a concerted effort at doing better promotion of KDE as a whole."
    author: "ac"
  - subject: "Re: Oxygen"
    date: 2005-09-02
    body: "Fair enough, point taken. Thanks for the great software all involved!"
    author: "Tim Sutton"
  - subject: "Vnc Recording"
    date: 2005-09-02
    body: "Hi\n\nI have been using vnc2swf recording in the past to make screenie movies of QGIS (e.g. on http://community.qgis.org). The talk mentioned above describes how to add voice overs. It there a link with more details? I know vnc2swf lets you add a soundtrack in mp3, but its difficult to record the voice first and then the video. Did the speaker describe a way to record the voice after the screen video was shot and then add it to the video?\n\nMany thanks\n\nTim Sutton"
    author: "Tim Sutton"
  - subject: "Re: Vnc Recording"
    date: 2005-09-02
    body: "From home page... <http://www.unixuser.org/~euske/vnc2swf/>\n\nQ: How to record audio?\nA: Record your audio separately with an MP3 encoder like lame, and combine it later with edit_vnc2swf. Simultaneous audio/video recording is planned in future."
    author: "Jonathan Dietrich"
---
The aKademy 2005 KDE Developers Conference finished yesterday with a second day of talks to prepare for KDE 4.  Topics of the day included integration with other programming platforms, marketing KDE and accessibility.  In their keynote, David Carson and Deepika Chauhan from Nokia described the challenges involved with porting KHTML to the series 60 platform.  Another highlight was Novells desktop migration study.  After four days of conference talks the KDE developers are now into a 5 day hacking marathon which will feature not only 24 hour non-stop coding but more spontaneous <a href="http://en.wikipedia.org/wiki/BoF">BoF</a> sessions and two days of usability sessions.  




<!--break-->
<div style="border: thin solid grey; padding: 1ex; float: right; width: 300px">
<a href="http://static.kdenews.org/jr/nokia-talk.jpg"><img src="http://static.kdenews.org/jr/nokia-talk-wee.jpg" border="0" height="225" /></a><br />
<a href="http://static.kdenews.org/jr/nokia-talk.jpg">David Carson Outlining the Nokia Browser Architecture</a>
</div>

<p>The day was opened with comedy double act Aaron Seigo and Waldo Bastian on Marketing for Geeks (<a href="http://conference2005.kde.org/slides/marketing-for-geeks--aaron-seigo.kpr">Slides</a>, <a href="http://wiki.kde.org/tiki-index.php?page=Marketing+For+Geeks+Talk">Transcript</a>).  "We suck" Waldo said, "I was talking to my mother the other day and we make all this great software but she doesn't know about it".  The duo went on to give ideas on how to get our software better marketed including grassroot marketing like blogging and the value of cute furry animals.</p>

<p>Continuing the theme of presenting KDE to non-technical users better was a talk from Rainer describing how to make multimedia presentations.  By using VNC recording and voiceovers we can describe our software much better than with plain text.  Look out for more dynamic previews of KDE in future feature guides and documentation.</p>

<p>Meanwhile Mirko B&ouml;hm took a "Walk on the Dark Side" reporting on strengths and weaknesses of Java and .NET.  Richard Dale gave one of the most involving talks on The State of KDE Language Bindings (<a href="http://wiki.kde.org/tiki-index.php?page=Language+Bindings+Talk">Transcript</a>).  He has worked particularly hard on the KDE Ruby bindings, Ruby he says is like going back to his pop-11 roots from 30 years ago because it is so much more interactive than programming in C++.</p>

<p>Nokia's talk was conducted by two engineers from the Series 60 webbrowser team. It covered their experiences of porting KDE's web rendering engine KHTML to the Series 60 mobile phone platform. After a brief history of mobile phone usage and lot of technical details they outlined the challenges of porting KHTML/Webcore to a very constrained operating system. The KHTML developers seized the opportunity to get in touch with the Nokia representatives. They were keen to emphasise that they did not want to fork KHTML. They feel able to contribute back a lot of their changes in the future. </p>

<p>Domas Mituzas, developer for the Wikimedia Foundation and contributor to Wikipedia described details on the worlds largest and free encyclopedia project, its contributors and the technology behind it all. He then described the importance of metadata and proper ways to index data in order to make them available to third parties like KDE, e.g. via WebServices.</p>

<p>The Windows Desktop Migration Study (<a href="http://wiki.kde.org/tiki-index.php?page=Novell+Desktop+Migration+Study+Talk">Transcript</a>) had a few surprising examples of how users interact with KDE.  They gave their testers 5 basic desktop tasks and videoed their actions.  One task that caused a lot of problems was adding a new user, most of their subjects found Switch User in the K-Menu and ended up trying to create a new user through the login manager.</p>  

<p>The KDE OpenOffice Integration talk from Jan Holesovsky (<a href="http://wiki.kde.org/tiki-index.php?page=KDE+OpenOffice+Talk">Transcript</a>) described how they got OpenOffice's Native Widget Framework to use KDE widgets, finding and creating 1500 icons and the integration of OpenOffice with KAddressBook.</p>

<p>The conference closed with a presentation on the new <a href="http://appeal.kde.org/wiki/Oxygen">Oxygen</a> icon theme which looks set to become the new icon theme in KDE 4.  They are keeping the exact look under wraps for now to keep it fresh for KDE 4 but the ideas include a more serious colour palette and well defined concepts for different icon types.</p>

<p>The <a href="http://www.kdedevelopers.org/node/1390">kdelibs and kdebase</a> restructuring BoF came up with a new structure for these core KDE modules in KDE 4.  This will give ISVs more flexibility to use KDE while being easier to port to other platforms and have smaller memory footprint for non-GUI applications.</p>

<p>The coding marathon now goes on until Sunday.  KDE's resident usability experts have <a href="http://www.kdedevelopers.org/node/1388">announced their usability tracks</a> which will take place on Thursday and Friday.</p>






