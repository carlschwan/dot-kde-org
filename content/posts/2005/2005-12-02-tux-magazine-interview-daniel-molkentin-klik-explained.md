---
title: "TUX Magazine: Interview with Daniel Molkentin, Klik Explained"
date:    2005-12-02
authors:
  - "Jim"
slug:    tux-magazine-interview-daniel-molkentin-klik-explained
comments:
  - subject: "what's so special about tux magazine ??"
    date: 2005-12-02
    body: "it is pretty unusual to offer a magazine in the 'net for free, and still ask for an a priori registration. \n\nthis smells fishy.\n\nare them publishers then spamming my mailbox with advertisements praising arbitrary payware in return? are them address collectors harvesting my email address in exchange for a few cent?\n\nabove, the title page of that thingie looks much too polished to be true. i've yet to see a software running on linux that would be able to create such a layout. i could imagine that they use adobe indesign or quark express on windows or mac to produce it. \n\nso what was the point of \"tux\" magazine, again?"
    author: "noobie"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-02
    body: "They could be using Scribus, or many other programs.  I don't believe I've gotten any spam from them (I generally make up an address at my website for each place I register, like tuxmgz@MYDOMAIN.net).\n\nThey apparently offer advertising in their magazine which is probably how they make their money.  I really doubt they get enough subscribers to be able to sell them to spammers."
    author: "Corbin"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-02
    body: "it's done by the same people who do linux mag (or at least i hope i got the right publication.. i'm always mixing up those publications as they all have such similar names).\n\nthey are giving it away for free and keeping the publication costs low specifically to help promote and grow the open source desktop market while having a sustainable publication (ergo why it isn't on paper)\n\nregistration is a pretty low cost and i haven't gotten a single piece of spam due to it myself.\n\nas for open source software that can do layouts that good, one would obviously point to scribus which fits the bill. however, i'd bet that they use more \"traditional\" tools since they are part of a larger and older publishing house.\n\nfor me the point of tux mag is to get nice user-level articles that average computer using people utilizing linux at home or in the SOHO would actually be interested in. that means being gui-centric and not much about writing software or the latest amazing enterprise server stuff.\n\nless SAN, more iPod"
    author: "Aaron J. Seigo"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-02
    body: "Its done by the same people that do Linux Journal & Linux Gazette (it says on the bottom of the site)."
    author: "Corbin"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-02
    body: "Corbin, Aaron J.:\n\nthanks for your replies. they clarified a few bits. even makes me confident to register my email there now. see, how I trust your testimonials?  ;-)"
    author: "noobie"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-04
    body: "It looks like it was made in a Dreamweaver template.  you can run dreamweaver through crossover office, or one wine if you tweak the wine.conf and know which dll's to use.  So it could have still be developed in linux on a windows application.\n\njoe"
    author: "JOe"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-04
    body: "There are logins available at bugmenot (use the wonderful firefox plugin)"
    author: "Vicks"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-07
    body: "you could use just about anything to create that layouy - it's just a case of how easy it would be"
    author: "mabinogi"
  - subject: "...and what's so special about klik?"
    date: 2005-12-02
    body: "... _that_ is the question _I_ ask myself.\n\nAfter all, there is .rpm, .deb, .tar.gz, Autopackage, ZeroInstall, Gobo Linux and some more in the One Big Linux World. How do they differ from klik?\n\nI'm pretty new to Linux (coming from a Windows programming background), and I've been looking at Linux packaging formats since early summer because my boss told me to prepare to lead a new software project next spring that is supposed to also offer value to Linux customers.\n\nI read the klik FAQ linked above, but didn't grok it all. The FAQ avoids the comparison with the other formats.\n\nAdding Linux to your supported platforms if you are an ISV is a pretty non-profitable business, if you ask me!"
    author: "heh"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-02
    body: "From the FAQ:\nIs klik a new alternative package management system? \nNo. klik does neither replace, nor interfere with your system's package manager. \n\nWhat good is klik then for? \nklik is good for letting you test bleeding edge software releases and development versions, or use packages that are not available for your distro (or only in outdated versions). Once you start to use and like klik, you will probably find many more use cases on your own."
    author: "Morty"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-02
    body: "Alrighty then.\n\nBut why can't I test bleeding edge releases and development version with one of the other package formats? \n\nWhy can't there be just _ONE_ package format??"
    author: "heh"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-02
    body: "Let me add a thing or two.\n\nDon't get me wrong: I do not want to impose one package format, one window manager, one distribution onto this movement. I am aware that you love the freedom of choice this gives to you, and the freedom to experiment and research. I must admit that I am even attracted to this, your Freedom, as a person. I find it more and more fascinating.\n\nBut I also try to figure how to make a successful business based on Linux software development. The more I let myself be entangled by the fascinating sites of Linux, the less I think about business. I spend a day or two diving into some technical problem or learning a new lesson about this OS, forgetting the world around me... then some days later I awake with the sobering question in my mind \"And how do we earn our livings with it?\" Happened not only once to me in the last 5-6 months.\n"
    author: "heh"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-02
    body: ">how to make a successful business based on Linux software development.\n\nThat's a big question isn't it! The best way to answer it is to use some of your skills learned as a software developer, start with dividing the problem into manageable parts. If you remove the \"Linux\" from your question, you still have the hardest part of the question left. Linux does not change that bit, and it's no problem delivering proprietary solutions on Linux the same way you do on other platforms. Several companies do so successfully.\n\nBut on Linux you also have the opportunities and competition that is FLOSS. Making it perhaps little harder to get a good business model, but not much. Direct competition with FLOSS will require outstanding products, and it's perhaps not a good idea:-) For some products you may do dual licenses like MySQL or Qt. But the important thing to remember is that most software development is not abut writing one application and selling it, it's about delivering solutions. In that market a FLOSS license are really not an issue, afterall your customer will not be interrested in distributing the software. Most likely the only one interrested in such custom solutions are their competition, actually making it rather unlikely they will distribute. On the other hand FLOSS will give you possibilities to reduce development costs with lots of premade code. "
    author: "Morty"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-03
    body: "Fell for the FUD? No offense, really, but the package format issue is mood IMHO. I hear it all the time, but I don't believe it, because I know it's BS. In case you don't know, there are quite a few commercial applications for Linux (my company is currently developing one as well, using Qt). Anyone not completely stupid won't create distro packages at all. There are distro-independent installers, like Loki installer (used by most Linux games, Crossover Office, and Nvidia drivers AFAIK). Then, there's InstallAnywhere, commercial, Java-based, used mostly for Java applications (eg Omnicore X-Develop). A few companies use hand-written custom installers (Softimage|XSI and ptc Pro/E, Pixel32). Or just use plain tgz archives (Apple, D2 Software, ifx).\n\nPlus, there's only _one_ Shake and _one_ Softimage|XSI package for Linux (this is true for all the apps I mentioned), both should work on pretty much every distro starting at RedHat 7.3 - I've yet to see an incompatible distro. \n\nAnd well, yes, the remaining 'issue' is the desktop environment. But why is that an issue? Ever noticed that even on Windows or OSX, most apps won't integrate at all? That almost every current application uses it's own HIG, design, style, color scheme and custom widget set anyway? Yes, Softimage|XSI uses Mainwin. It never looks or feels 'native', regardless of the desktop environment. But it works, copy and paste works, moving/ maximizing/ minimizing the application works, so hip hip who cares if it uses a different style and color scheme? If Softimage (or Mainwin) wanted to do it, they could at least parse a few config files and set the fonts and colors accordingly, but that wouln't make such a huge difference anyway...\n"
    author: "Willie Sippel"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-04
    body: "There are lots of different package formats because in the Free software world the packaging is the distribution's job. Not the vendor. The distribution authors know how their system  works and how they want their system to work.\n\nThey are the ones that keep linux systems consistant. If everyone went around downloading .exe equivalents from developers' websites, everyone's systems would be a mess and be going wrong all the time.\n\nNow, that can be a problem if you want to distribute proprietary software. Ransomware relies on you keeping it not freely available. So the distributions won't be able to package it for you and hold it on their repositories. Some people make the binaries frely avaliable but use a registration code system. I don't know , I'm not really interested in non-Free software. I suppose the answer is to keep the file layout of the software quite simple, so that installation/making packages is rather trivial.\n\n\"...then some days later I awake with the sobering question in my mind 'And how do we earn our livings with it?'\"\n\nYou get paid to create the software by the person or organisation that needs it."
    author: "Robert"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-05
    body: "I hope that KDE and Gnome will adopt something like Elektra in the future. When we have one flat registry, x-distribution thing will get a lot of easier.\n\nsee\nelektra.sf.net"
    author: "gerd"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-06
    body: "KConfig-XT is more better suited and already commonly used by many KDE applications!"
    author: "Hans"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-02
    body: "Of course you can, but there are a few situations it is not possible or reasons you don't want to do it that way.\n\n-The package are not available for your distribution/packageformat.\n-You or other users want to keep using the stable version of the same program, using the packagemanger will most likely overwrite or replace the old version.\n-Your user account don't have rights to install packages on the system with the packagemanager.\n\nKlick is an addition to the package formats, think of it as a kind of sandbox rather than a plain install method.\n\n_ONE_ package format would not really make a big difference, since in most cases you would have to deal with different libraries and compiler versions. Forcing you to make different packages for the different distributions anyway."
    author: "Morty"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-02
    body: "Klik is a self contained image, so you don't 'install' it or any of it's dependencies.  Klik isn't suitable as a means of distributing things such as libraries (or anything else thats not directly run by the end user).\n\nUsing the system's normal package manager would mean you would have to update 1 or more packages on your system and if they are unstable, you would have to uninstall them and reinstall the old ones (which if it pulled in many new dependencies may not be very fun)."
    author: "Corbin"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-06
    body: "> Why can't there be just _ONE_ package format??\n\nDesktop LSB + elektra --> one plattform in 4-5 yrs."
    author: "gerd"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-07
    body: "\"Why can't there be just _ONE_ package format?\"\n\nThere's this inconvenient thing called \"freedom\". Because of it Fedora is not obligated to use Debian's package manager, or vice versa. There is no dictator telling distros and developers what package manager they must use."
    author: "Brandybuck"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-04
    body: "Klik's main advantages over a \"real\" packaging system are:\n- Installable by non-priveledged user.  Don't need to ask your admin to install \"really crashy, untested application x\" just so you can try it out.\n- Doesn't mess with any other versions.  You can keep your stable digikam installed via the normal package manager, but test out bleeding edge digikam without removing anything else.  That's really hard to do using traditional deb/rpm etc.\n- Easily portable.  Copy a klik package to a usb key and take it to another computer, and it will (usually) run just fine.\n\nOf course, not all of these goals are _quite_ realised yet, but that's the direction that they are heading in.\n\nL."
    author: "ltmon"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-04
    body: "These are all problems that should be addressed in the package manager itself.\n\nFor example, users should be able to install a .deb package in their own dir (/home/user/.local for example, instead of /). As long as the files don't conflict with other packages, two packages of the same name should be installable in this case."
    author: "Reply"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-04
    body: "So, are you going to write that fabulous package manager ??\n\nIn the meantime, I'll be a happy klik user.\n\nAs soon as you have something to test, tell so. Announce here: I'll get in touch with you to be your first beta tester."
    author: "reply-reply"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-05
    body: "It's called autopackage.\nhttp://autopackage.org/\n\n-> Fritz"
    author: "Fritz"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-05
    body: "yeah, works perfect with all those KDE/Qt applications, right?\n"
    author: "cl"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-07
    body: "Actually, it works just as well with KDE/Qt applications as Klik does. The only potential issue is the C++ ABI which has changed a little bit between different GCC versions. In practice I never found it to give any trouble though, and their current development version even allows for packages to contain different binaries for the different ABI's.\n\n@rinse: Autopackage is _not_ a replacement for existing package managers, it just complements them."
    author: "Arend jr."
  - subject: "That's so special about klik! (+autopackage diffs)"
    date: 2005-12-07
    body: "############\n++++ (autopackage) \"works just as well with KDE/Qt applications as Klik does\"\n############\n\nNo it doesn't.\n\nThe reason is: autopackage and klik do different things (with similar goals).\n\nautopackage compiles source code. And tries to make the result as portable as ever possible. Its main religion is *first* to create a cleaner build environment+system, obtain cleaner source code from upstream, work for a cleaner dependency determination... and only *then* they'd put everything for 1 application underneath one directory. autopackage creates new tools to change the world of software.\n\nklik re-packages already compiled binaries (.deb, .rpm, .tgz, and even .package packages). And tries to make the result as portable as currently possible. It works with means and tools that are currently available. Its main religion is \"1 file == 1 application\" (have everything contained in one .cmg file system image). klik uses existing tools to change the world of software.\n\nautopackage works now for only very few applications. It requires the programmers to learn new tricks. autopackage requires sometimes many, sometimes fundamental changes in the source code of a program to make it auto-package-able. And it always requires re-compilation before an autopackage for a given application is created for the first time. Sometimes many man-days of \"non-sexy\" work.... Therefore, its success and popularity amongst coders is not spectacular, though I wish it were. (I really appreciate, applaud and admire the efforts of the autopackage guys -- but I am also a big fan of klik,  if you could not tell yet.)\n\nklik works already now for many, many applications. It requires no changes in the source code, and no re-compilation. Its results are ready in minutes very often. The klik developers have me had surprised and stunned on various occasions already, when a user came into their IRC #channel and asked for a new klik://package and they were able to create a fully working one within 2 minutes (happened with  klik://flock, klik://wesnoth-latest, klik://wengophone and klik://xara).\n\nCall klik an assortment of ugly hacks -- but it is wildly successful in what it does! And the removal of the ugliness is not in the realm of klik itself. This lays with the upstream packagers, with the Linux Standard Base efforts, and with the GCC developers (hell, they should not break ABI with every other minor release!). It also lays with the future success of autopackage!\n\nIf I understand the klik developers in #klik right, they even *wish* autopackage to succeed more than it currently does. Because this would give them much cleaner ingredient files for their klik recipes, which would then be much better portable across different Linux distributions."
    author: "I disagree"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-05
    body: "No, that is another replacement :)\n"
    author: "rinse"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-07
    body: "\"For example, users should be able to install a .deb package in their own dir\"\n\nThey already can. I don't know the syntax for Debian, but every package manager has the ability to install to a different directory hierarchy (/, /usr/local, /opt, /sandbox, ~/MyStuff, ...)."
    author: "Brandybuck"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-07
    body: "++++ \"every package manager has the ability to install to a different\n++++ directory hieraevery package manager has the ability to install \n++++ to a different directory hierarchy (/, /usr/local, /opt, /sandbox,\n++++ ~/MyStuff, ...).rchy (/, /usr/local, /opt, /sandbox, ~/MyStuff, ...).\"\n\n##########\n\nStop telling fairy-tales. Please!\n\n"
    author: "I disagree"
  - subject: "Re: ...and what's so special about klik?"
    date: 2005-12-12
    body: "While, yes, dpkg and rpm both _can_ install files into a new root, making it work right is not for the faint hearted. First, if the package is not relocatible, then you have the problem of needing to create a new package manager db in that fake \"root\", then the fun begins since if this is NOT a relocatible package, you will now have to deal with providing all those wonderful dependancies that that app needs to run.\n\nNow what developers and packagers both need to strive for is making software completely relocatible. If you're furthur in doubt of this, take a look at the Maximum RPM book info about relocatible packages at http://www.rpm.org/max-rpm-snapshot/ch-rpm-reloc.html"
    author: "Gary Greene"
  - subject: "OT: rdf feed?"
    date: 2005-12-05
    body: "I recognized that the rdf feed is not working anymore. What's the reason for this?\n\nliquidat"
    author: "liquidat"
  - subject: "Re: OT: rdf feed?"
    date: 2005-12-05
    body: "The RDF is hosted on www.kde.org which is down currently.  Should return to normal once www is back."
    author: "Navindra Umanee"
  - subject: "Re: OT: rdf feed?"
    date: 2005-12-05
    body: "www.kde.org is up, you must have obsolete dns information."
    author: "Anonymous"
  - subject: "www.kde.org now hosted by GNU???"
    date: 2005-12-05
    body: "> host www.kde.org\nwww.kde.org has address 199.232.41.13\nwww.kde.org mail is handled (pri=1) by ktown.kde.org\n> host 199.232.41.13\n13.41.232.199.IN-ADDR.ARPA is a nickname for rev-c41-13.gnu.org\nrev-c41-13.gnu.org domain name pointer bluemchen.kde.org\n\nI'm all for it!\n"
    author: "interesting"
  - subject: "Re: www.kde.org now hosted by GNU???"
    date: 2005-12-06
    body: "Didn't Troll Tech use to host it?  What happened?"
    author: "KDE User"
  - subject: "Re: www.kde.org now hosted by GNU???"
    date: 2005-12-06
    body: "Trolltech is atm busy moving."
    author: "Anonymous"
  - subject: "Re: www.kde.org now hosted by GNU???"
    date: 2005-12-06
    body: "As far as I know, Trolltech has/had kde.org not www.kde.org\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: what's so special about tux magazine ??"
    date: 2005-12-06
    body: "\n FWIW: The PDF from the latest issue says it was made with Acrobat Distiller 6.0.1 for Macintosh (haven't checked the earlier ones).\n\n Seems to be a pretty good magazine, though.\n\n> \n> by JOe on Saturday 03/Dec/2005, @18:29\n> It looks like it was made in a Dreamweaver template. you can run dreamweaver\n> through crossover office, or one wine if you tweak the wine.conf and know which > dll's to use. So it could have still be developed in linux on a windows \n> application.\n> \n> joe\n"
    author: "Cazo"
  - subject: "klik://firefox"
    date: 2005-12-07
    body: "Hey, I am running a klik version of Firefox 1.5 in Mandriva 2006. It runs just dandy (except for a failure to register some \"chrome\" component, and java does not work). The flash plugin I had to reinstall, but it took 15 seconds, and I did it all as a regular user, and within Firefox. Excellent !"
    author: "MandrakeUser"
  - subject: "klik://thunderbird16-tabbed"
    date: 2005-12-09
    body: "I just found pipitas' blogged about his new klik package:\n\n* klik://thunderbird15-tabbed ....(a patched 1.5 release candidate version)\n* klik://thunderbird16-tabbed ....(a patched 1.6 Alpha version)\n\nWay cool!\n\nAlso thanks to Jess Hall for writing such a useful article, giving really useful insight."
    author: "kliker-noobie"
---
In its current issue, <a href="http://www.tuxmagazine.com/">TUX Magazine</a> interviews KDE core developer <a href="http://www.kdedevelopers.org/blog/15">Daniel Molkentin</a>. Daniel talks about the features in the just-released KDE 3.5 and explains the current plans and ongoing efforts for the next major KDE release, KDE 4.0. In the same issue, KDE's <a href="http://dot.kde.org/1116000449/">Jess Hall</a> explains how <a href="http://klik.atekon.de/wiki/index.php/User's_FAQ">Klik</a> makes trying and using new applications easy.
<!--break-->
