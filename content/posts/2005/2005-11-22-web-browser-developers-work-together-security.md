---
title: "Web Browser Developers Work Together on Security"
date:    2005-11-22
authors:
  - "gstaikos"
slug:    web-browser-developers-work-together-security
comments:
  - subject: "IE"
    date: 2005-11-22
    body: "Little surprised to see IE on board with a meeting like this, but it is good too see.  For better or worse most people still use it.  This way, those who migrate over will see things they are used to (colored address bar) and those who don't migrate will have that added security.\n\nCongrats on bringing all those browser folks together!"
    author: "MamiyaOtaru"
  - subject: "Re: IE"
    date: 2005-11-28
    body: "Actually, judging from the IE Blog on MSDN (http://blogs.msdn.com/ie/), they're getting quite into the whole browser development these days. If they manage to get their browser straight we'll be up to developing by real standards in some 4-5 years when it gets adopted by normal users."
    author: "Arrow"
  - subject: "cool"
    date: 2005-11-22
    body: "nice initiative on the SSL front.\n\nabout the phishing prevention i am not so sure. we should come up with a better solution to the identity verification problem and if that requires better technology _that_ should be supported.\n\nfx. could banks sign our keys and when going to a site a small notification would tell me: this is your bank! if thats missing, then i know its not my bank...\n\nanother problem is the location field: half (if not more) of the people surfing the net, don't have a clue what is going on in there. now how do we make that more simple?\n\nanyway, trying to blacklist or whitelist stuff is really not a smart way to do this. also, this problem is not new and exists in the same way in the real world: credit fraud, fake notes, weird guys ringing your door bell trying to sell stuff etc.\n\nthe reason why phishing (on the internet) is so attractive are:\n - easy to do\n - difficult to track\n - clueless users\n\nAFAICT the proposed solution does not solve any of these problems... the way i see it, it gets even worse, because \"now we can feel safe again\"!\n\n... and i don't feel confident about sending each and every URL i visit to some server for authorization (even if it is only SSL).\n\nin conclusion i would claim that the clueless user is the biggest problem here and i tend to agree that technology should be simple enough for the clueless user also... but he has do to his part as well. so lets educate those guys..."
    author: "bangert"
  - subject: "Re: cool"
    date: 2005-11-22
    body: "http://www.microsoft.com/mscorp/safety/technologies/antiphishing/vision.mspx\n\nThis was in a comment in the msdn blog concerning MS's Anti-Phishing plug-in."
    author: "Stephen Leaf"
  - subject: "Re: cool"
    date: 2005-11-22
    body: "http://www.microsoft.com/mscorp/safety/technologies/antiphishing/vision.mspx\n\nThis was in a comment in the msdn blog concerning MS's Anti-Phishing plug-in."
    author: "Stephen Leaf"
  - subject: "The location field; a vestige from the early days!"
    date: 2005-11-23
    body: "\"another problem is the location field: half (if not more) of the people surfing the net, don't have a clue what is going on in there. now how do we make that more simple?\"\n\nI am glad you mentioned this.  Raw URLs would not have gotten a prominent display in the GUI, had the WWW been developed with the general public in mind.  URLs were just the kind of notation that nuclear phycisists would find user friendly! ;-)\n\nAfter 15 years, the URLs have become established, and the editable address field is something the users expect.  No browser vendor would dare changing it drastically in one go.  But I think drastic change is called for.\n\nA URL consists 4-5 logical items:\n\n1) The scheme/protocol, e.g. http:// (mostly optional, http being default)\n2) The domain name\n3) The port number (optional)\n4) The path/filename\n5) The query strings (optional)\n\nThe protocol could be replaced by an icon.  The domain and the port number could be considered one unit.  The path and the query strings are somewhat dependant on each other; commonly the query strings are an optinal appendage to the path.\n\nDisplaying all these as one continuous, unformatted text string is not a good example of usability."
    author: "Herman Robak"
  - subject: "Re: The location field; a vestige from the early days!"
    date: 2005-11-23
    body: "Actually this sounds like a really good idea.  We could even hide the query completely, so that it is all displayed more clearly.  Then have a button linked to a popup with all the queries editable.  \n\nI am liking this idea the more I think about it."
    author: "Matt"
  - subject: "Re: The location field; a vestige from the early days!"
    date: 2005-11-23
    body: "If the address field was read-only, changing the display to something more readable and structured would be trivial.\n\nHowever, it is editable.  The users expect to be able to paste raw URLs into it, and to edit those URLs freely.  Because of this, any change is likely to upset a lot of users. \n\nIf the address field accepts a full URL with query strings as input, yet hide the query strings when the links are accessed by clicking them, then it breaks the \"principle of least surprise\".  How could one make it clearer to the user what is going on?  Press Tab to switch from raw to formatted URL? (too power-user-ish, IMHO)"
    author: "Herman Robak"
  - subject: "Lets all sing the University of MN fight song now"
    date: 2005-11-23
    body: "Remember Gopher?   It died about 1995, but in the early 90s it was more popular than WWW, and growing faster.   One major limitation is it didn't have a URL.  Every site gave directions in the following form:\n\ngoto the UofMN main gopher (which all clients had programed in)->all the servers in the world->North America->state->Company main page->link->link->text file.    (Gopher did not support graphical pages like the web did).    \n\nURLs may not be perfect, but that was one large advantage of the web - you could give someone a (complex) URL and they would get right to their site.\n\nMaintaining the UofMN main gopher site cost so much money that they started to charge for use of gopher, and killed the whole thing off.  \n\n"
    author: "bluGill"
  - subject: "Re: The location field; a vestige from the early d"
    date: 2005-11-23
    body: "I disagree completly!\n\nI paste about 100 URLs per day and splitting them into 5 fields would be annoying.\n\nHow often do you keep anything fix but the protocol?\n...but the port?\n...but the path-and-file?\n\nSometimes you may only change the query-string, but who does this by hand?\n\nIf it is build as optional feature (splitted URL vs. classical URL), and the user is free to choose: Well, I can agree.\nPeople would learn more easily how to read an URL, and might switch to the more usable feature (classical) later on.\n"
    author: "Stefan Wagner"
  - subject: "Re: The location field; a vestige from the early d"
    date: 2005-11-23
    body: "I am not sure whether _editing_ the URLs in a 5-part form would be preferrable.  But I think it is worth considering.\n\nAs for copying and pasting: there are context menus for page addresses and link URLs.  I think a raw/formatted toggle is called for, to not disrupt the workflow of those who rely on raw, editable URLs in the address field.  Changing it over night with no way back is way too disruptive.\n\nbluGill: Sure, we shall forever be able to pass URLs around in one piece.  Just like we can pass around files of HTML edited in Notepad.  They just won't appear verbatim in modern clients, like HTML is not shown like a bunch of tags in monospaced font to most users.  Old-timers will complain during the transition, because they think the URL widgets are more convoluted than plain raw URLs. \n\nIt's time to expect a richer representation of URLs than we are used to today! For example, a URL widget could contain the anchor text.  Of course, the anchor text is not always helpful, like the WAI-defying \"here\" or \"next\".  But would it  hurt to make such accessibility failures stand out more?"
    author: "Herman Robak"
  - subject: "Re: The location field; a vestige from the early d"
    date: 2005-11-24
    body: "Yeah your right, you do need a way to toggle off and on the \"formatted URL\"  and you also need a quick way to copy a link.  But both of those features can be implemented along side the formatted features.\n\nI guess one of the reasons why I really like this idea is because in many cases the URL ends up having so many different query strings that they become completely unreadable and quite useless at a glance.  Being able to quickly see the site name and path gives an indication of the sites structure.  Which I think can be useful for navigation.\n\nMatt"
    author: "Matt"
  - subject: "query strings being replaced with long paths"
    date: 2005-11-24
    body: "Keep in mind that some CMSses \"cheats\" with their URLs to make them look like static content URLs:\n\nFrom\nhttp://www.some-cms.tld/cms-app.cgi?topic=foo&article=bar\n\nTo\nhttp://www.some-cms.tld/cms-app/foo/bar\n\nThe latter is shorter and cleaner.  And if the CMS is clever enough, it can provide all the proper directives (ETag, Last-Modified, Content-Length...) of a proper static document (brace yourselves for some weird bugs...)\n\nThis can be used to make the URLs more opaque, too.  Instead of a lot of semi-readable parameters, you get one or two really long numbers, or random strings. You don't know how much of the URL is the path to the web application and where the input parameters to the web app start.  Systematic digging for articles or vunlerabilities becomes harder that way.\n\nHence, the path and the query string are not separate entities."
    author: "Herman Robak"
  - subject: "Re: The location field; a vestige from the early d"
    date: 2005-11-26
    body: "I've suggested before and will suggest again that syntax highlighting be added to the address bar. It's not too hard to have some highlighting of key portions of the text (protocol, separator marks, query entries, whatever) and doesn't completely break proper usage of the location bar (pasting paths and hand-typing addresses). Also, it might make it easier for new users to learn what that stuff actually means.\n\nOf course, I suggested that first over a year ago, so I expect nobody is gonna rush to implement it now."
    author: "jameth"
  - subject: "Usability versus phishability"
    date: 2005-11-24
    body: "The original posting was about user interfaces to maintain security.  In such a context, \"usability\" gets a different meaning.  The usual meaning is along the lines of \"whatever makes the user happy, empowered, productive, faster, and is easy to learn\".\n\nUsable security features have another primary goal: Keep the user out of harm's way.  Making sure that the program follows the user's _reflected_ intent.  That often means deliberately slowing down the user, so that there is indeed time to reflect.  It also means telling the user about perils that the user may know little or nothing about, yet avoid crying wolf too often.\n\nIn this context, the address bar has a flawed design.  It is quite evident that it fails to keep the majority of users out of harm's way.  The address bar does not ensure that the browser follows the user's real intent.  All the successful phishing scams serve as proof of that.\n\nThe web is about serious stuff now.  People buy expensive stuff and manage bank accounts with it.  Yet they are totally oblivious of the underlying architecture.  Which is OK!  You don't need to know the building structure of a house to use it safely.  Opening and closing doors is not supposed to have fatal side effects.  Web browsers and web applications need to be the same way, so users can trust their gut feeling without getting burned again and again."
    author: "Herman Robak"
  - subject: "konqueor..."
    date: 2005-11-22
    body: "Konqueror browser web  and  konqueror browser file should be separated. Althought konqueror browser file can use khtml. This is the best. Less dificult, more open velocity, ...\n"
    author: "Ilovekonqueror"
  - subject: "Re: konqueor..."
    date: 2005-11-22
    body: "I totally agree with you. Konqueror is bloated because of its \"file manager\" aspect.\n\nMake an external browser using khtml and focus on it to improve add-ons development, plugins,... and let Konqueror use kpart to see html (like now).\n\nPlease !!! \n\n"
    author: "Shift"
  - subject: "Re: konqueor..."
    date: 2005-11-22
    body: "If you guys actually knew anything, you would be dangerous.\nWhen you become real-life coders and know what you are talking about...well, that's never going to happen, so let's all have a laugh."
    author: "Joe"
  - subject: "Re: konqueor..."
    date: 2005-11-22
    body: "While technically Joe's outburst is more learn\u00e8d than those he was responding to, the fact that there is a *perception* to the contrary is enough to perhaps to consider... "
    author: "T Middleton"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "Of course I know what I am talking about. My name is on one of the Konqueror add-on changelog files. Rellinks is a small add-on but it was sufficient for me to stop working on Konqueror add-ons.\n\nAnd what I can say is that a Konqueror add-on developer need to deal with the \"file manager\" aspect of Konqueror.\n\nAnd I am not a GNOME troll. I use KDE since KDE 1.0. To love something doesn't mean that you mustn't critisice it :)\n\nKonqueror can open PDF, but kpdf is the PDF application.\nKonqueror can open images but kview it the image application.\nI only want KDE to be able to open web-pages (like now) but I prefer an external KDE browser focused on web and flexibility for plugins, addons,... See how much addons Konqueror has and how much firefox has. I hope that one day it will be possible to program addon for Konqueror with kjsembed.\n\n"
    author: "Keep them"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "I don't why my name was changed to \"Keep them\" but the previous message was mine.\n"
    author: "Shift"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "yes!!!!!\nLess profiles konqueror and more two aplication :o)"
    author: "arthur"
  - subject: "Re: konqueor..."
    date: 2005-11-24
    body: "what rellinks really needs is a means to say \"i'm for this type of data or this type of URL/browsing only\" and thereby have a way to filter what plugins are chosen / selected by default.\n\nsplitting konqueror into two pieces along the lines of \"file\" vs \"web\" won't actually solve this. it will address certain specific cases (e.g. rellinks, actually) but leave the problem for many other plugins."
    author: "Aaron J. Seigo"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "Im a developer too.  Linux Philosophy is a program for each necesity and this programa do it work very good.\n\nIm sorry my English. "
    author: "Ilovekonqueror"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "Unix philosophy is to have small tools that do one thing well, and that can be work together to do new things as well.\n\nThink using ls | more in a shell.\n\nKonqueror is just another kind of shell, and the KParts and KioSlaves are the equivalent to the shell commands. I actually see little point in KPDF as a program, actually - what can you do with it that you can't with its KPart embedded in Konqueror?  \n\nYou can actually combine the same commands in a different way, too.\n\nOn the other hand, Akregator uses the khtml part to give a different functionality, and I'm increasingly relying on it to browse the net, to the point that I'd like it to grow some more \"regular\" browser functionality (the Location bar, for example).\n"
    author: "Luciano"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "You are guest, but...here we are speaking split konqueror.\nWhy kpdf if konqueror display pdf with kpart of kpdf?. Because is good.\nThe same is browser and konqueror. why dont have a browser out of konqueror as kpdf or other aplications?. \n\nagain Im sorry my English.\n"
    author: "Ilovekonqueror"
  - subject: "Re: konqueor..."
    date: 2005-11-23
    body: "Are you Gnome trolls perhaps?\n"
    author: "k"
  - subject: "White-listing vs black-listing"
    date: 2005-11-22
    body: "The above improvements sound good. But can I make a further suggestion?\n\nLike many people, I have a few sites that I want complete assurance about, such as  my personal banking sites. I don't want to simply trust a third-part CA to vet them, even if it is capable of providing high-assurance. As well as concerns about the business model for that CA, it still will sign a very large number of web-site certificates. If any of those web sites were compromised or the CA was tricked into signing a certificate, it opens an opportunity for the browser to say \"highly trusted\" when it isn't - and may even be a different web site if DNS could be compromised. And I expect it would take a long time, if possible at all, to persuade all sites to get the signed by one of the \"blessed\" CAs.\n\nI much prefer the model used by the Petnames extension of Firefox (http://www.waterken.com/user/PetnameTool/), which allows me to register the server digital certificate thumbprint, and to give the site a nick-name (\"My bank\"). If the certificate changes in any way, I'll get warned and can do the appropriate checks. Effectively I'm managing my own white-list of a handful of sites, so don't need to trust someone else's whitelist of tens of thousands; or even worse a blacklist of far more.\n\nThis can co-exist with the proposals above; for example by allowing the user to store their trust relationship which then displays (say) a blue address bar. Other sites will go through the green / red / white display. "
    author: "Andrew Yeomans"
  - subject: "Re: White-listing vs black-listing"
    date: 2005-11-22
    body: "Certificates protect against hijacked DNS. It's a two-way authentication."
    author: "kL"
  - subject: "Re: White-listing vs black-listing"
    date: 2005-11-22
    body: "Yes, they do, but this Petnames business can protect against something *else*. Say I set a Petname for amazon.com to be 'Amazon Bookstore'. Now some tricky email directs me to amafon.com, and I'm blind, so I don't see the s/z/f/. The Petname 'Amazon Bookstore' will *not* come up, making it obvious I'm not at Amazon.\n\nNote of course that there will be a nice yellow lock icon anyway, because Amafon.com has bought a security certificate certifying that they are Amafon.com."
    author: "Chase Venters"
  - subject: "Weak Ciphers"
    date: 2005-11-22
    body: "Please, please, please, please, please dont't remove support for weaker ciphers, just have them disabled by default. I often make use of weaker ciphers in my work and if you remove support for them you will force me to use another browser. If you disable them I will then be able to re-enable the support when I need it.\n\nFizz"
    author: "Fizz"
  - subject: "Re: Weak Ciphers"
    date: 2005-11-22
    body: "i'm sorry, fizz, but as you might have read, most (if not all) browsers will remove support for weaker ciphers soon, so you'll have to just use an older version, or upgrade your sites."
    author: "superstoned"
  - subject: "Re: Weak Ciphers"
    date: 2005-11-22
    body: "but why do you do it?\n\nthe reason they are being disabled is because it is a bad idea..."
    author: "bangert"
  - subject: "Re: Weak Ciphers"
    date: 2005-11-23
    body: "I can see that most of the replies to my posting are along a similar line to this. I do however have a very specialised requirement for this feature, and they are not my systems that I will be accessing. I am a security specialist and have been recommending for some time now that all my clients remove support for weaker ciphers from their servers. Occasionally it isn't possible to use stronger ciphers.\n\nWhy not do what MS have said that they will do with IE and simply disable support for the weaker ciphers, but not remove it. Then when there is a requirement for weaker ciphers (for whatever the reason) IE can still be used. If KDE remove support for weaker ciphers it will be a reason not to use KDE. Please just disable support by default.\n\nFizz "
    author: "Fizz"
  - subject: "Re: Weak Ciphers"
    date: 2005-11-28
    body: "I have to speak up in favor of keeping weaker ciphers available but disabled. Otherwise, I'd have to keep an older version (probably a statically linked firefox) kicking around because my ISP is the only broadband provider available, \"only supports Microsoft products\" (blames your client if it complains), and would probably just provide instructions to re-enable weak ciphers when IE 7 rolled around."
    author: "ssokolow"
  - subject: "Re: Weak Ciphers"
    date: 2007-12-10
    body: "Can you please tell me how to disable the weaker ciphers on IE 6.0?  "
    author: "Terry Tran"
  - subject: "Too bad"
    date: 2005-11-22
    body: "The security concern and the cause is more important than those who continue to use terrible security practices. Supporting everything and everyone is only good if everything and everyone is safe and that isn't the case. This is a smart decision."
    author: "Alex"
  - subject: "Re: Weak Ciphers"
    date: 2005-11-22
    body: "You can turn off encryption altogether if it comes to that.\n\nBetween being completely open and providing a false sense of security, I'd go with being completely open. At least (some) users will have a clue not to provide sensitive information on those work websites (even if on an Intranet)."
    author: "Thiago Macieira"
  - subject: "Thank you"
    date: 2005-11-22
    body: "George, thank you for all the effort and work that you are putting in this. It will definitely help the world of browser to move toward more security and without somebody tackling the problem as you did, we could have seen many different and incompatible approaches. You are doing a great job !"
    author: "Philippe Fremy"
  - subject: "Re: Thank you"
    date: 2005-11-22
    body: "Yes, very nice job George. Great to see co-operation from the majority of browser vendors on such an important matter."
    author: "Chris Howells"
  - subject: "Microsofts take"
    date: 2005-11-22
    body: "http://blogs.msdn.com/ie\n"
    author: "Richard Moore"
  - subject: "Re: Microsofts take"
    date: 2005-11-22
    body: "Now I know why they are improving the IE, they found a bunch of Indians doing the job...."
    author: "Boemer"
  - subject: "Careful now"
    date: 2005-11-22
    body: "This is not /.; be nice."
    author: "a.c."
  - subject: "Re: Microsofts take"
    date: 2005-11-22
    body: "Racist asshole."
    author: "Klan Boy"
  - subject: "Re: Microsofts take"
    date: 2005-11-22
    body: "Who is racist? AFAIR Microsoft had indians do the not that appealing job of writing .NET bindings for various Windows libraries. And western (and especially US) software firms experiment with outsourcing tasks that they presume are routine tasks - easily managed from a distance."
    author: "anders"
  - subject: "Re: Microsofts take"
    date: 2005-11-23
    body: "I'm not being racist...\n\nit was just funny to see that it were all indians doing it. Calling it IndianExplorer instead of Internet Explorer.\n\nIt just gave a new meaning to the verb: outsourcing....\n\nMaybe the sentence wasn't really nice, but I'm no englishman..."
    author: "boemer"
  - subject: "Re: Microsofts take"
    date: 2005-12-01
    body: "Zenophobic asshole"
    author: "An Englishman"
  - subject: "Good idea! but..."
    date: 2005-11-22
    body: "please remember that there are color blind people out there. \nConveying information only by means of color is not so good from an accessibility point of view. But maybe using that in addition to distinct icons would be great.\n"
    author: "Luciano"
  - subject: "Re: Good idea! but..."
    date: 2005-11-22
    body: "I don't know, but is the (U.S.) traffic light a world standard?  If so, it might be used as an indicator where the colored light would change in intensity and size for the visually impaired."
    author: "John Gutierrez"
  - subject: "You don't understand"
    date: 2005-11-22
    body: "Red and Green are EXACTLY the same color to me.  [1] I go when the traffic light turns BLUE.   There is a green tint, but blue is a better description of the color.    Modern traffic lights have the blue tint (which most people do not notice) because nearly all color blind people can tell blue from red.   (There are about 100 different forms of color blindness, so I don't want to make a statement that is too strong)\n\nThere is a size and intensity difference to lights, but it is not something I actually notice.  I've known people to stop at a strange light because all they knew was that one of the lights was lit, but had no clue which one.   (When the lights were in a horizontal row, not vertical like the lights where they live)\n\nI guess I have answered the objection though.   When the color red is shown, it should be pure red, with no blue part at all.   When Green is shown it should have a significant blue part, almost blue-green.   If size and intensity can be varied, so much the better.\n\nThis is important to me, because there is a lot of color blindness in my family.  (Enough that my sisters are color blind, which is nearly unheard of)\n\n[1]Actually, I'm not that color blind, I can normally tell red and green apart.   Not always though.  I do however know people who are that color blind that they cannot tell the two apart."
    author: "bluGill"
  - subject: "No, you don't understand"
    date: 2005-12-02
    body: "What he meant was to use the ordering of colors in the US-style traffic light as a supplement to colors - ie, red on top, green on the bottom.  He didn't know if this was a worldwide standard or US-specific, however."
    author: "kundor"
  - subject: "Re: No, you don't understand"
    date: 2005-12-02
    body: "As far as I know, red on top, yellow in the middle, green at the bottom is pretty international.\n\n(The only difference that I know is that Swizterland does not use round lights, due to colour blindness, but a square for red, a triangle for yellow and a disk for green.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Good idea! but..."
    date: 2005-11-23
    body: "I do not know how the traffic lights work in the US.\nIn Italy you cannot get a driving license if you are color blind.\n\nHowever, as the following message shows, the icons are not going to be removed, so this is probably not a big problem."
    author: "Luciano"
  - subject: "Re: Good idea! but..."
    date: 2005-11-23
    body: "About 10% of the population is red-green color-blind to some degree or other.\n\nColor-blindness to yellow, however, is extremely rare. Perhaps that's why FireFox chose yellow to begin with?\n\nOthers on this thread are right, however. Something in addition to the color needs to be unambiguously shown in the UI to indicate what the browser has determined the site's authenticity to be."
    author: "Chuck Somerville"
  - subject: "Re: Good idea! but..."
    date: 2005-11-23
    body: "Might be a good idea to read up on accessibility first:\n\nhttp://www.w3.org/WAI/wcag-curric/chk3-0.htm\n"
    author: "Tom Potts"
  - subject: "Colorblinds..."
    date: 2005-11-23
    body: "As said before, colorblindness differs from person to person.\nI may distinguish red from green sometimes, depending on the brightness, saturation and darkness of colors.\n\nThe idea to use icons is good.\n\nLet the user modify his preferred colors is another idea, you might add.\nThanks."
    author: "Stefan Wagner"
  - subject: "Re: Good idea! but..."
    date: 2005-11-22
    body: "There are icons... read back through the writeup.  It's even specifically addressed in the third to last paragraph: \"The existence of the padlock and extended identity information makes it safe even for those who have difficulty distinguishing colours.\""
    author: "Evan \"JabberWokky\" E."
  - subject: "Next time you get together...."
    date: 2005-11-22
    body: "Next time you get the developers together, can't you agree on a common bookmarks file ~/Bookmarks or something that all Linux browsers would use. It doesn't make any sense for every browser to have their own bookmarks."
    author: "Tony"
  - subject: "Microsoft did not invent this idea (color url bar)"
    date: 2005-11-22
    body: "Firefox has been doing this for over a year now.\n\nNot that it matters much who invented it first, but I am really surprised that you have never seen this before until you saw an IE7 demo.\n\n\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Microsoft did not invent this idea (color url "
    date: 2005-11-23
    body: "I don't know where you get that \"invented it first\" stuff from since it's not in the article. What's in the article however is following impression of Mr. Staikos:\n\n\"I was initially resistant to the idea of using colour to indicate security - especially the colour yellow! However the idea we have discussed have been implemented by Microsoft in their IE7 address bar, when I saw it in action I was sold. I think we should implement Konqueror the same way for KDE4.\"\n\nTo make that clearer, Firefox started with coloring the URL bar yellow when an encrypted connection is used. The approach by the IE developers was to make use of that URL widget coloring to not only display whether the connection is encrypted, but also whether it's signed and whether the URL string may be pishing related."
    author: "ac"
  - subject: "Re: Microsoft did not invent this idea (color url "
    date: 2005-11-23
    body: "I'm not sure if the Microsoft developers were aware of this or not, but I had this very idea (of different colours for different states of encryption) when the proposal was first floated to colour Firefox's url over a year and a half ago.\n\nhttps://bugzilla.mozilla.org/show_bug.cgi?id=244025#c1\n\nStill, if IE implements this idea and the other browsers follow, this will be one example where Microsoft takes the lead for once."
    author: "David P James"
  - subject: "Permanat Location Toolbar?"
    date: 2005-11-22
    body: "On the desktop for my kids I remove the location bar from konqueror because everything that they access is already bookmarked.  This has the added benefit of simplyfing the browser window for them.  Would this change make that impossible - the location toolbar has to be part of the window???"
    author: "AlfredNilknarf"
  - subject: "Re: Permanat Location Toolbar?"
    date: 2005-11-22
    body: "Removing it manually, using the menus, may still be allowed.\n\nHowever, allowing webpages to turn the location bar off using JavaScript may be impeded in the future."
    author: "Thiago Macieira"
  - subject: "Self Signed Certs"
    date: 2005-11-22
    body: "I would like a better handling of self signed certificates.  It is so annoying that when using a perfectly valid and effective self signed certificate to secure internal pages (MRTG / webmail, etc) that we have to deal with the inherent warnings on most browsers. \n\nI'd like to see a color code and location/status bar indicator, that the sie in encrypted, just not verified or something to that effect.  I have had clients insist on wasting money on a certficate for internal only use!  "
    author: "Brandon"
  - subject: "Re: Self Signed Certs"
    date: 2005-11-23
    body: "This may make your life a little easier, but it will also be of tremendous benefit to phishing scammers if all those annoying warnings went away, if they could be casually dismissed permanently for all sites.  As it is (at least in Mozilla & Firefox), you can permanently accept the certificate for that one site."
    author: "Paul"
  - subject: "Re: Self Signed Certs"
    date: 2005-11-23
    body: "You should find some way of installing those certificates in the browsers that need to use them, or for more flexibility run an internal CA (it's not that hard) and install its certificate, or just not bother with SSL. As it is, you're just getting a false sense of security, because your browser can't tell the difference between the certificate you created and another one presented by an attacker who spoofed the DNS response for the web server you're trying to use."
    author: "Ben Hutchings"
  - subject: "Re: Self Signed Certs"
    date: 2006-01-08
    body: "Today you have much better and free alternative, which will be supported by most web browsers: http://cert.startcom.org\nThis will make the problem of self-signed certificates disapear, but as well expensive validated / verified certificates too..."
    author: "Eddy"
  - subject: "Remember the colorblind"
    date: 2005-11-22
    body: "A color coding based on security is all well and good. However, it does 1/20th of the population no good if it is based on Gree, Red, and Yellow. Make sure that those who can't actually distinguish between two disparate colors can actually determine them in your implementation. Writing the color RED in white above a red background works wonders."
    author: "concerned"
  - subject: "Re: Remember the colorblind"
    date: 2005-11-23
    body: "Just what i was thinking!"
    author: "rob"
  - subject: "Re: Remember the colorblind"
    date: 2005-11-23
    body: "to both of you: read. just read the article and the link(s) in it. this has already been discussed, and adressed. apart from the color indication, there will always be some other form of indication for the visually impaired."
    author: "superstoned"
  - subject: "IRI / I18N warnings too"
    date: 2005-11-22
    body: "IRIs and internationalised domain names and URLs are also a problem: particularly where a \"foreign\" codepoint has the same or a similar glyph as a \"native\" one. I'd like to see some kind of visual cue for:\n\n- any characters in a URI that don't belong to my configured locale;\n- any characters in a URI that don't belong to the locale of the displayed document.\n\n"
    author: "jang"
  - subject: "Leading web browsers"
    date: 2005-11-22
    body: "Which \"leading web browsers\" had developers at the meeting?"
    author: "Rib"
  - subject: "Re: Leading web browsers"
    date: 2005-11-23
    body: "It said so in the article: Konqueror, Opera, Mozilla/Firefox and IE developers were present."
    author: "Thiago Macieira"
  - subject: "Re: Leading web browsers"
    date: 2005-11-23
    body: "I can't believe what I read. The community is fighting M$'s monopoly, and the developers of the \"leading\" browsers sat together to help them bugging out the flaws of their software. Thank you very much."
    author: "Hugh"
  - subject: "Re: Leading web browsers"
    date: 2005-11-23
    body: "Or to put it a completely different way: The developers of the leading web browsers were for once not keeping their users hostage in their fight for dominance, but had a meeting to agree on a consistent user interface for some security features."
    author: "Herman Robak"
  - subject: "Re: Leading web browsers"
    date: 2005-11-24
    body: "when it comes to certain things, data formats in particular, cooperation on standards is vital."
    author: "Aaron J. Seigo"
  - subject: "Re: Leading web browsers"
    date: 2005-11-23
    body: "were you people born stupid, or we're you cloned that way?\n\nit's about user experience and unerversal understanding of the way browers tell you something isn't right with the page you are looking at, as it goes (if you bothered to find out) you'd know that ie 7 actually has features like that anyways, the meeting was about a common standard.\n\njust because some people still live in caves, doesn't mean ideas can't be shared with the likes of microsoft, as long as it's not all give."
    author: "streaky"
  - subject: "Kmail also uses green, yellow, red"
    date: 2005-11-23
    body: "Kmail also uses green for verified gpg signatures, yellow for signatures which are unknown (but exists) and red for signatures which are not ok.\n\n\nIn general I don't think that it is a good idea to say to the user: Green, the web site is secure, because that is just wrong as: I have a firewall that says 100 Attacks blocked this day, I am save.\n\nI would keep saying that people should not enter passwords in web forms and live is so great without a credit card, at least in germany ;)\nA company will never ask for such things in a email, it is just that simple, they will send a mail, not a email and is they do, go to the site, login and follow the navigation. Don't klick on link in cool html mails.\n\n\nFelix\n\nP.S. After all, people need to learn companies are wrong if they say, everybody can use computers without thinking/learning. It is hard, but also a driver needs a driver lizense.\n\n"
    author: "Felix"
  - subject: "co-operation"
    date: 2005-11-23
    body: "I'm glad to see the Internet has matures over the last 9 year (which is now long I have been working on the web). Back in '96 it was such an adversarial place with all those competing technologies.\n\nBravo to everyone who took part in the conference and making all our lives in the net easier.\n\nGerard"
    author: "Gerard Earley"
  - subject: "Good start, but only that!"
    date: 2005-11-23
    body: "It's really great that the browser manufacturers have decided to take this one on the chin and recognise that phishing is a browser problem.  It's also really fantastic that you are all now moving to get rid of SSL v2.  As you must realise, this clears the way for proper sharing of TLS over single IP numbers which promises to make available low cost service.  More SSL (v3 or TLS) can dramatically help the anti-phishing tools and this one of the few ways to get more TLS.\n\nHowever, I urge caution in getting into bed with the other browsers.  Browser manufacturers have come late to this party, and already they've done the worst possible thing for innovation:  start a cartel.  The people who really know about phishing weren't at your event, and your post and the posts of the other manufacturers are already evidencing group-think.\n\nIf the innovators had been there, they would have explained the real security thinking behind such innovations as Trustbar and Petname.  Compared to these ideas, and the security and UI thought that went into them, the debate over URL bar colours is just a funny distraction.  Also, the \"strongly-verified cert\" idea won't work, for much the same reasons that phishing broke the browser security model in the first place - it's an institutional and technical implausibility that is akin to asking the UN to organise World Peace.  If it was going to work, we wouldn't have phishing in the first place, because the browser security model already said that it would stop MITMs like phishing (and, the original Netscape alphas even would have!).\n\nhttps://www.financialcryptography.com/\nhttp://wiki.cacert.org/wiki/AntiFraudCoffeeRoom"
    author: "Iang"
  - subject: "Pointless to have high assurance certificates"
    date: 2005-11-24
    body: "Has anyone ever been phished because of a low assurance certificate?\n\nWhy then have high assurance certificates - seems that it is just a way for certificate issuers to make more money.\n\nFurther, there is no evidence that the procedures for high assurance certificates will be any different - only the price.\n\nPhishing is possible because:\n\n1.  Users are bewildered by the multiplicity of names, which makes proving the true name rather pointless.\n\n2.  Website software of major companies frequently has major errors, allowing phishers to make phishing content appear to come from the official web site and appear to be certified by the official certificate supposedly proving the true name.\n\nThe solution to phishing, as I say in my post <http://blog.jim.com/?postid=48>, is for browsers to support SRP-TLS-OpenSSL, where both parties prove knowledge of the shared secret without revealing the shared secret, thereby proving a relationship, not a name.\n"
    author: "James A. Donald"
  - subject: "Names?"
    date: 2005-11-24
    body: "Do the other \"security developers from the leading web browsers\" have names?"
    author: "Soyapi"
  - subject: "Re: Names?"
    date: 2005-11-25
    body: "yes"
    author: "Melchior FRANZ"
  - subject: "TrustBar - a FF extension implementing these ideas"
    date: 2005-11-24
    body: "TrustBar is a FireFox extension that already (and for a while already) implements several of these ideas, and others. In particular, it supports both `petnaming` of a site, i.e. to assign a name (or, with TrustBar, a logo) to a site, and also display `Identified by` and the logo (or name) of the organization and of the CA, like IE 7. You can install it via http://AmirHerzberg.com/TrustBar. \n\nTrustBar is the result of secure usability study by Ahmad Jbara and myself, and has some other mechanisms, including random `exercise training attacks` to help users stay trained to watch for the name/logo of the site. (I must admit that this mechanism is now set for too frequent `exercise attacks`, we will improve this in our next release very soon, but you can also reduce or eliminate this using the user interface of course).\n\nWe are very happy to see some of this research adopted by browsers. We have some more ideas we are investigating, and would love to cooperate with any browser developers to help improve security indicators. TrustBar is an `open source`, public domain project. \n\nBest, Amir Herzberg\n\nAssoc. Prof., Dept. of Computer Science, Bar Ilan University\nhttp://AmirHerzberg.com"
    author: "Amir Herzberg"
  - subject: "Don't use popups, require user to initiate action?"
    date: 2005-11-24
    body: "I've been wondering about the use of popups to accept a (self-signed) certificate and to ask for user authentication.\nWouldn't it be much more secure to refuse the certificate/authentication request by default and show an (error) icon in the status bar?\nThen the user has to initiate action and is far less likely to just 'click yes'.\nI'd prefer this as an advanced user but I think it's also much better for 'simple' users."
    author: "Olaf van der Spek"
  - subject: "Re: Don't use popups, require user to initiate action?"
    date: 2005-11-25
    body: "It would be safer, but less usable.  To many users, this would be a showstopper (\"the site doesn't work!\") and if only some browsers did this, the users would blame the browser.\n\nIf self-signed, expired, or wrong-name certificates were really rare, then your suggested approach could be viable.  But, alas, they are not all that rare.  Only MSIE could start refusing access unilaterally.  If MSIE doesn't work, the webmaster will take the heat.  If one of the other browsers doesn't work, the browser's vendor will take the heat.\n\nSure, sites with broken certificates shouldn't work, in ANY browser.  To make that happen, the browser vendors have to cooperate."
    author: "Herman Robak"
  - subject: "Re: Don't use popups, require user to initiate act"
    date: 2005-11-25
    body: "MSIE already replaced a (download) popup by a yellow bar below the address bar.\nI think that's quite usable and a lot safer than a popup with Yes as default button.\n\nIt would be nice if browsers at least provide this as an option.\n"
    author: "Olaf van der Spek"
  - subject: "Re: Don't use popups, require user to initiate action?"
    date: 2005-11-28
    body: "I don't think this is really good. For me, ssl provides two different things.\n1. It supports encryption of the data transmited\n2. It can give you the information that well known companies are trusting this site.\n\nA self signed cert only supports the first one, because nobody knows if the private ca key is public or not.\n\nBut a self signed cert supports encryption, and often, this is enought. E.g. I don't like to buy a really expensive cert only for the external download server, bug tracker and mail gateway. I like to be sure that nobody can read what I transmit. I cannot be sure that the server is really my server."
    author: "Felix"
  - subject: "Re: Don't use popups, require user to initiate action?"
    date: 2005-11-28
    body: "> I don't think this is really good. For me, ssl provides two different things.\n\nI don't see how this relates to my proposal. SSL/TLS continues to provide these things."
    author: "Olaf van der Spek"
  - subject: "Re: Don't use popups, require user to initiate act"
    date: 2008-08-07
    body: "I think you should use form for security!"
    author: "james"
  - subject: "Re: Don't use popups, require user to initiate act"
    date: 2008-08-07
    body: "In addition, you can post it through the action property."
    author: "james"
  - subject: "SuSe"
    date: 2005-12-19
    body: "Is there anything close to DreamWeaver on Linux for ease of use to build a websites with flash and fireworks."
    author: "Brian Woods"
  - subject: "such a perfect spying tool!"
    date: 2005-12-25
    body: ">One more key item that Microsoft is implementing is their anti-phishing plug-in. I hope that Microsoft will be open with this system and allow us to write our own Konqueror plug-in, allowing our users to contribute to their database and take advantage of it.\n\n---------\n\n...and now Microsoft will have the list of webpages (and perhaps their referers too) each IP (or even each user) is surfing through.\nThat would greately help them in targeting the advertisements of Windows Live.\nBut i heard where do i surf, and what links do i follow was always considered private information and those companies like DoubleClick was punished for such spying ?\n"
    author: "Arioch"
  - subject: "IVR"
    date: 2008-08-07
    body: "http://www.voicent.com/ivr.php?a=8223"
    author: "IVR"
---
Core KDE developer <a href="http://www.staikos.net/">George Staikos</a> recently hosted a meeting of the security developers from the leading web browsers.  The aim was to come up with future plans to combat the security risks posed by phishing, ageing encryption ciphers and inconsistent SSL Certificate practise.  Read on for George's report of the plans that will become part of KDE 4's Konqueror and future versions of other web browsers.


<!--break-->
<p>In the past few years the Internet has seen a rapid growth in phishing attacks.
There have been many attempts to mitigate these types of attack, but they
rarely get at the root of them problem: fundamental flaws in Internet
architecture and browser technology.  Throughout this year I had the fortunate
opportunity to participate in discussions with members of the Internet Explorer,
Mozilla/FireFox, and Opera development teams with the goal of understanding and
addressing some of these issues in a co-operative manner.</p>

<p>Our initial and primary focus is, and continues to be, addressing issues in PKI
as implemented in our web browsers.  This involves finding a way to make the
information presented to the user more meaningful, easier to recognise, easier
to understand, and perhaps most importantly, finding a way to make a distinction
for high-impact sites (banks, payment services, auction sites, etc) while
retaining the accessibility of SSL and identity for smaller organisations.</p>

<p>In Toronto on Thursday November 17, on behalf of KDE and sponsored by my company Staikos Computing Services, I hosted a meeting of some of these developers.  We
shared the work we had done in recent months and discussed our approaches and
strengths and weaknesses.  It was a great experience, and the response seems to
be that we all left feeling confident in our direction moving forward.
There was strong support for the ideas proposed and I think we'll see many of
them released in production browsers in the near future.  I think we were
pleasantly surprised to see elements of our own designs in each other's
software, and it goes to show how powerful our co-operation can be.</p>

<p>The first topic and the easiest to agree upon is the weakening state of current
crypto standards.  With the availability of bot nets and massively distributed
computing, current encryption standards are showing their age.  Prompted by
Opera, we are moving towards the removal of SSLv2 from our browsers.  IE will
disable SSLv2 in version 7 and it has been completely removed in the KDE 4
source tree already.</p>

<p>KDE will furthermore look to remove 40 and 56 bit ciphers, and we will
continually work toward preferring and enforcing stronger ciphers as testing
shows that site compatibility is not adversely affected.  In addition, we will
encourage CAs to move toward 2048-bit or stronger keys for all new roots.</p>

<p>These stronger cryptography rules help to protect users from malicious cracking
attempts.  From a non-technical perspective, we will aim to promote, encourage,
and eventually enforce much stricter procedures for certificate signing
authorities.  Presently all CAs are considered equal in the user agent
interface, irrespective of their credentials and practices.  That is to say,
they all simply get a padlock display when their issued certificate is
validated.  We believe that with a definition of a new "strongly verified"
certificate with a special OID to distinguish it, we can give users a more
prominent indicator of authentic high-profile sites, in contrast to the
phishing sites that are becoming so prevalent today.  This would be implemented
with a significant and prominent user-interface indicator in addition to the
present padlock.  No existing certificates would see changes in the browser.</p>

<p>To explain what this will look like, I need to take a step back and explain the
history of the Konqueror security UI.  It was initially modeled after Netscape
4, displaying a closed golden padlock in the toolbar when an SSL session was
initiated and the certificate verification project passed.  The toolbar is an
awful place for this, but consistency is extremely important, and during the
original development phase of KDE 2.0, this was the only easy way to implement
what we needed.  Eventually we added a mechanism to add icons to the status bar
and made the status bar a permanent fixture in browser windows, preventing
malicious sites from spoofing the browser chrome and making the security icon
more obvious to the user.  In the past year a padlock and yellow highlight were
added to the location bar as an additional indication.  This was primarily
based on FireFox and Opera.</p>

<p>I was initially resistant to the idea of using colour to indicate security -
especially the colour yellow!  However the idea we have discussed have been implemented by Microsoft in their <a href="http://blogs.msdn.com/ie/archive/2005/11/21/495507.aspx">IE7 address bar</a>, when I saw it in action I was sold.  I think we should implement Konqueror the same way for KDE4.  It involves the following steps:</p>

<ol>
<li>The location toolbar becomes a permanent UI fixture along with the status bar</li>
<li>The padlock goes into the location combo-box permanently, is the only place
it appears, and the location bar stays white by default</li>
<li>When verification on a site fails, the location bar is filled in red</li>
<li>When a high-assurance certificate is verified, the location bar is filled in
green, the organisation name is displayed beside the padlock, and it rotates
displaying the name of the CA</li>
</ol>

<p>I am afraid that the missing yellow will confuse our users, but at the same time
I think it was misguided to add the yellow when it was added, and I think this
is the price we must pay.  Hopefully users will be able to adjust quickly, and
KDE4 is the right time to do it.  The existence of the padlock and extended
identity information makes it safe even for those who have difficulty
distinguishing colours.</p>

<p>One more key item that Microsoft is implementing is their <a href="http://blogs.msdn.com/ie/archive/2005/11/17/494040.aspx">anti-phishing
plug-in</a>.  I hope that Microsoft will be open with this system and allow us
to write our own Konqueror plug-in, allowing our users to contribute to their
database and take advantage of it.  I think this is in everyone's best interest.
Microsoft says that they are not evangelising the anti-phishing service to other
clients at this time but they are "working with the community on the issue
through many avenues and groups, such as the Anti-Phishing Working Group and
Digital PhishNet".  They didn't rule out the potential to open up their client
technology in the future.  They suggested that others interested in offering
similar technologies could take their own approaches and work with the same
industry data providers that they use.</p>

<p>I'm very optimistic about the future of co-operation among browser developers
and I hope this recent work signals a new trend of good relations.  Together we
can really create some amazing new technology and make it possible to solve some
of the major problems we face today.</p>

