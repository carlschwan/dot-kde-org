---
title: "KDE Commit Digest for June 10, 2005"
date:    2005-06-11
authors:
  - "dkite"
slug:    kde-commit-digest-june-10-2005
comments:
  - subject: "Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "Where can I find the source code for the digest and bugs.kde.org (in case they're free software). \n\nI mean, they are powerful things that I'd like to use in my own development environment.\n\nAlso, in case that it doesn't exist, if I use bugs.kde.org software I'd like to implement a KDE GPLed client that could make simple queries and bug reports directly from KDE via SOAP or somewhat like, and capable of caching reports in computers with no broadband access to Internet. Maybe it could be called optionally from the exceptions tracker. Like Mozilla Quality Feedback agent does.\n "
    author: "Ivan da Silva Bras\u00edlico"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "bugs: http://websvn.kde.org/trunk/bugs/\ndigest: http://websvn.kde.org/trunk/www/areas/cvs-digest/"
    author: "Christian Loose"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "Well, you might want to look at kbugbuster ;-D"
    author: "Sam Weber"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "Huuum.... seems like suited for developers... You can just read and track posted erros, but not create new reports. No handbook. I'm using KDE 3.4.1.\n\nAny plans for letting the user do reports of crashs automatically? I tried to find kbugbuster in the mailing list or docs or anywhere kde.org, I found an utility named KDebugDialog, that is for developers too. Well, I'll mail the developers, thanks for your help."
    author: "Ivan da Silva Bras\u00edlico"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "The code is in the kde svn repository in trunk/www/areas/cvs-digest/enzyme. The software is called enzyme. It's php scripts with templates and some glue to the repositories. Essentially you change some paths, some titles, point to a svn repository, make a list of revisions, stir and poof. If you run on a cvs repository it is more complicated.\n\nThere are a few kde specifics, such as the nick->full name translations. The statistics are gathered (screen scraping) from KDE sites.\n\nI would be pleased to hear from you. Using the code for a different project would force encapsulation and maybe some configuration setup.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "I'll go for it. \n\nI'm using CVS, I have plans for moving to SVN, but not for the next weeks.\n\nI already downloaded the code, as soon as I can I will study it, and I will do the tests on my repository. PHP... very well suited, since my main projects now are on php.\n\nI'll return you every feedback that I can. Hopefully I can be capable of bettering the code and return it to!\n\nThank you very much.\n\n\n\n"
    author: "Ivan da Silva Bras\u00edlico"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "Cvs requires an indexing scheme to build the atomic commits, and requires a local repository. It is a bit of a pain, but it works.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-11
    body: "bugs.kde.org is a slightly modified Bugzilla"
    author: "Anonymous"
  - subject: "Re: Commit diggest and bugs.kde.org"
    date: 2005-06-12
    body: "No, it's not even \"slightly modified\" but the standard Bugzilla with custom wizard, custom templates and additional report scripts."
    author: "ac"
  - subject: "Where is  DBUS?"
    date: 2005-06-11
    body: ">>DCOP client/server implemented for KDE Win32\n\nNo DBUS? that sucks."
    author: "TIM.."
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-11
    body: "That's about porting KDE3, not KDE4."
    author: "Anonymous"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-11
    body: "And how can they port KDE 3 to Windows if Qt3 is not GPL on it?\n\n"
    author: "TIM.."
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-11
    body: "They did port Qt3 first: http://sourceforge.net/forum/forum.php?forum_id=471148"
    author: "Anonymous"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Because they ported the GPLed Qt from Linux to Windows. I'm quite amazed at this work and that they actually got something quite usable. \nNo reflection on the porting developers, but I am quite happy to see that Qt4 will be GPLed everywhere."
    author: "cph"
  - subject: "GPLed Qt Windows port"
    date: 2005-06-12
    body: "It looks like it so usable it soon will be used in a release of a major application on the windows platform. The LyX developers have been busy, even making a nice windows installer according to reports. They now have LyX running native binarys on all major platforms."
    author: "Morty"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-11
    body: "Well, when Novell starts shipping Gnome apps with the dbus support ripped out, like Beagle, then it's maybe time to rethink the whole \"replace dcop with dbus 'cos it's standard\" idea..."
    author: "Boudewijn Rempt"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-11
    body: "SUSE ships D-BUS according to this:\n\nftp://ftp.suse.com/pub/suse/i386/9.3/suse/src\n\nAnd it looks like Trolltech are willing to adopt it too, with Harald having made Qt bindings and clients like Skype following suite and adopting it."
    author: "Navindra Umanee"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "I think Boudewijn might be being a little sarcastic.  Poking slightly at the GNOME camp and their silly banner waving, while lacking follow through.  I know it's not the general case, but I remember a lot of whoo-ha over dbus, even when it wasn't there implementation wise or stable.  The irony is that KDE seems to be pushing DBUS harder and faster, even though the development movement was often criticized for the slow adoption."
    author: "Saem"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "I know, he mentioned Beagle which recently had a release without D-BUS support for the first time.  It might be yet another war between Novell and Redhat, or it might be something else.\n\nBtw, I find it strange that everyone is saying how great Guadec was, but nobody is saying anything really significant e.g. about the direction of the project.  I was looking forward to some real news and the outcome of some of their freeform sessions[1].  Like, what did they decide on the whole Mono/GCJ/future of the GNOME platform thing?  Or which revision control system are they adopting?\n\nI guess it was mainly a marketing event.  10x10!\n\n[1] http://live.gnome.org/Stuttgart2005_2fFreeformSessions"
    author: "Navindra Umanee"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "It appears to me that the greatest thing the GNOME developers took from GUADEC was a heightened sense of community and enthusiasm for the project.  Most of the posts refer to how good it was to talk/drink with other developers face-to-face.  The 10x10 talk was about a goal, not so much about marketing IMHO.\n\nAlso, I think the GNOME developers were mostly tired of the Java/Mono flamewar, and they don't appear too bothered about keeping CVS for the time being.\n\nRespectfully,\n\nAndrew"
    author: "coulamac"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Honestly, this is spin.  It's great spin, to be sure, but like I wrote, I was hoping for something more informative than just spin.  \n\nI think GNOME has potentially a very exciting future, but the confusion about that future is well... confusing."
    author: "Navindra Umanee"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "D-Bus seems to be a solution in search of a problem. As geiseri so elegantly stated, the largest consumer of IPC resources is something like Kicker figuring out what is running. I have dbus running and it sits taking up memory with a daemon waiting for me to put in a cd, which fires up hotplug scripts, which fires up HAL, and broadcasts on d-bus, and then shows an icon on Kicker, probably through dcop. An engineering marvel. All this works just as well in KDE without DBUS.\n\nAs for pushing, it will come down to someone writing some code.\n\nTrolltech uses D-Bus for accessibility purposes, ie. talking to the rather more mature gnome accessibility apps.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Well, abuse of D-BUS does not mean anything.  DCOP could be abused as well.\n\nIsn't D-BUS just a replacement for DCOP i.e. they both do the same thing except the one isn't as dependent on Qt/X11/ICE as the other.  And as far as I know Harald wrote generalised bindings for D-BUS, not something accessibility-specific.  I'm not as sure as you are that Trolltech isn't planning to use it for other things.  The Skype people seem to think it's the way Trolltech will go.\n\nIf people want to push DCOP over D-BUS, fine.  But you're right, then they have to do the work so that companies like Trolltech and Skype can adopt it...  not just complain when others assume D-BUS is the future.  \n\nJust because some people cannot see a use for DCOP beyond Kicker doesn't mean other people don't.\n"
    author: "Navindra Umanee"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "First of all DBUS is basically what you said. I haven't kept up with all the developments but initially it appeared it may become a C implementation of the same essential thing as DCOP. While it was developed at freedesktop.org it looked like it would be adopted by GNOME as it seemed largely their people were behind it. The critical new benefit was that it could function as a system wide bus and be used by hardware and other things and that it could offer the opportunity to be a joint messaging system for KDE and GNOME. It would be exciting if it were so. Effectively GNOME has no real answer for DCOP, which was developed as an alternative to CORBA. CORBA is powerful in networks but has a much higher overhead and is not easy to program for, so it became somewhat vestigal in GNOME. The potential problem is that the guys writing DBUS didn't necessarily have experience with this type of IPC and so there are performance questions. Ideally KDE developers want to resolve this, but the question remains in my mind if GNOME will commit to it. If not it's much less attractive.\n\nAs for Trolltech, they have made things easier for KDE... The calls are anything but pretty and Trolltech has been working on a nice interface for Qt 4. This still leaves the question of what to do about DCOP... Of course assuming DBUS were to be adopted. KDE already automatically supports a lot of DCOP and it's very useful for interfacing KParts (Something else not currently in the mix with GNOME which is odd as the name is supposed to mean \"GNU Object Model\") as well as scripting with tools like Kommander. The question would be what to do with all this code. Ideally tools would be created to make this painless, or it's just another bus running on the system.\n\nI'm not sold yet but I think the question of whether this would be a universal bus is a good first question. Frankly I can't imagine KDE without a universal IPC like DCOP and I find it more than a little odd that there doesn't seem to be wide consensus among GNOMEs that they need a good IPC and desktop interoperability. Maybe I am just in the dark there."
    author: "Eric Laffoon"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Even if GNOME does right now not adopt DBUS, I think KDE should, for simple reasons:\n\n\n- DBUS is desktop- and X11-independent\n- if it works well within KDE, GNOME will have to follow, as the first system-apps will also start to take advantage of it (e.g. syslogd)\n- If it's good enough, it's better to use something from fdo, instead of boiling it's own soup.\n"
    author: "ac"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Sorry, but this reasoning is nonsense.\n\n# DBUS is desktop- and X11-independent\n\nWith DCOP ported to win32 DCOP isn't X11-dependent anymore. And conceptually there isn't any reason why DCOP couldn't be easily made desktop independent either.\n\n# if it works well within KDE, GNOME will have to follow, as the\n# first system-apps will also start to take advantage of it (e.g. syslogd)\n\nDCOP works well with KDE, so where is GNOME?\n\n# If it's good enough, it's better to use something from fdo, \n# instead of boiling it's own soup.\n\nfdo isn't a standards body. It's just a place where existing successful technology is evaluated to possibly be recommended for wide spread use in the open source world. So far _every_ freedesktop \"standard\" started off as an \"own\" soup until it gained wider adoption. So DCOP and DBUS are on equal levels with regard to this .. it's only that DCOP is much more mature at the present point of time.\n\n \n"
    author: "MaX"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Doesn't DCOP depend on C++ making it unusable for desktops which are trying to stick to a pure C base?"
    author: "mikeyd"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Not a really valid argument against DCOP, as the issue are purely political and religious not technical. There is no reason why DCOP can't be adapted to work with a C environment. As I understand it also has a Qt dependency, but if any serious need arise there are no technical reasons why this cant be changed/removed. It will require some work and skill, but it's doable. The biggest requirement are for the developers to be dedicated in cooporating and work together with KDE developers."
    author: "Morty"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "No. I wish I could be less blunt, but, well, no. Nonsense. You know, the big thing about protocols is that they are protocols: agreements about what certain bytes passed through a transport mean. You can encode that meaning in any language you want. DCOP is an absurd exercise in not-invented-here coding that has no easily discernable technical merit over existing free software solutions. Dbus is a political, not a technical thing, Dbus is not something KDE needs; it is superfluous. And C++ versus plain C is just a blind, a cover, not an argument germane to the issue.\n\nYou can implement dcop in C. You can implement dcop in Python; you could even, conceivably, implement dcop in Visual Basic. Same with dbus. Except that dbus hasn't been tested for a few years in the wild.\n\nDbus is an exercise in fire and motion (look at Joel on software for an explanation) -- throwing something in the air in the hope that others will waste their time on it, instead of working on their own thing. I know that while I haven't wasted time on dbus I have let the dcop interface to Krita wither away because, well, what would be the sense of the effort if dcop is to be end-of-lived anyway? Replacing a working technology with something new, heck, even contemplating doing that, can be extremely destructive. Especially if the agenda for the new thing isn't set by yourself. Especially if the agenda in question is being something that's fought over by a couple of corporate interests.\n"
    author: "Boudewijn"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "the dcop protocol uses QT datatypes like QString, so it ain't very useful as a desktop independent solution"
    author: "ik"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "What do you think it's harder: to change DCOP to use a different datatype from QString, or to develop a good IPC mechanism from ground up? Besides as  Boudewijn said its only about certain bytes passed around, you can encode that meaning in any language you want. Being based on QString only makes it somewhat more complex, not impossible."
    author: "Morty"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-13
    body: "> what would be the sense of the effort if dcop is to be end-of-lived anyway\n\nregardless of what happens to DCOP, implementing IPC interfaces to applications is not a waste of time because: a) KDE3 still has a lot of life left in it and b) if DBUS does supercede DCOP in KDE4, it's not like you're going to have to rewrite all of your interfaces from the ground up or the idea of having such interfaces will be tossed to the wind.\n\nthere are serious issues with DBUS atm, but let's not turn this into a protest of inaction as that's just distractionary."
    author: "Aaron J. Seigo"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "C bindings to DCOP were available ages ago but went unnoticed."
    author: "ac"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "Your simple reasons does not cut it, the two first are nice but not very crucial. Both X11-independent and possibility to communicate with system-apps are nice and it gives lots of possibilities, but in no way important for KDE as a desktop. \n\nAnd fdo are by their own words not a standardization body, more a place to collaborate on different technology and tools who MAY get adopted and become usefull for desktops. As it stands now KDE already have a widely used and working solution for IPC, while the alternative are mostly untested in production environments. When Beagle, an application making heavy use of IPC, decides to drop DBUS and go for their own solution this is something to take note of. According to them the main reasons to drop it was lack of speed and stability, both possible also making it unfit for use in KDE. \n\nNo doubt the core KDE developers keeps themselves somewhat informed of the development of DBUS. But I don't think they have done a real evaluation to see how it actually will preforms as an IPC, used the way KDE needs. Deciding to switch before knowing if the technology/implementation are going to meet the needs of KDE will only lead to problems. It may end in a situation where the KDE developers have to spend lots of time and energy trying to fix DBUS, as opposed to plainly maintaining DCOP. Choosing DBUS for political reasons or since everybody else does (which they apparently no longer does) are not a way KDE should go.\n\nAnyway, I'm confident the core developers will make the decision based on technological benefits and their knowledge of IPC and KDE's need rather than hype. "
    author: "Morty"
  - subject: "Re: Gnome inertia wrt IPC mechanisms"
    date: 2005-06-12
    body: ">>Frankly I can't imagine KDE without a universal IPC like DCOP and I find it more than a little odd that there doesn't seem to be wide consensus among GNOMEs that they need a good IPC and desktop interoperability. Maybe I am just in the dark there.\n\nHonestly Eric, I think that the GNOMEs have fallen into the same trap as Apple have (to bring in a different argument!), to a lesser or greater extent. Apple spent the last few years telling us how their dual G5s were twice as fast as a 3Ghz P4. Now they are intending to use the technologies that they have been bad-mouthing, and finding little enthusiasm from their supporters as a consequence. The rest of us know that P4s aren't half the speed of dual G5s, but that understanding has become deeply embedded into the thought of the customers.\n\nWith GNOMEs, we have known for years that CORBA wasn't cutting it, and that DCOP solved the problem, and really works well, especially for coders. GNOME developers have convinced themselves that because they don't have good IPC mechanisms, that they aren't actually useful or important. The fruits of DCOP use have also been argued against as being unimportant. So how do they move forward implementing a technology that they have been declaring to be insignificant since KDE 2?\n\nAs a application programmer, I find DCOP to be brilliant, and I've done some really cool things with it and especially the Perl bindings; but until they try it, GNOME developers won't understand that, and GNOME users (the OSNews readers) will dismiss it. Same reason that they dont realise how intuitive and good QT is; they can't try it and see."
    author: "Luke Chatburn"
  - subject: "Re: Gnome inertia wrt IPC mechanisms"
    date: 2005-06-13
    body: "So, why doesn't the KDE Project ask the GNOME Project - as a project to project talk - what's their stance on DBUS/COP? Silly as it may sound, an open letter might help, asking whether or not they want to work on interoperability via fd.o and DBUS or not. This might also help with the rumours circulating among GNOMErs (osnews comes to mind) of how KDE is not willing to use the so called standards developed at fd.o. It worth a try."
    author: "molnarcs"
  - subject: "Re: Gnome inertia wrt IPC mechanisms"
    date: 2005-06-13
    body: "I think it has been repeated several times, fdo is no place for standards."
    author: "ac"
  - subject: "Re: Gnome inertia wrt IPC mechanisms"
    date: 2005-06-13
    body: "you know what's really ironic here? \n\nit's these sorts of rumours that turn FD.o from a useful technology sharing commons into a political lever. and using it as political leverage is detrimental to FD.o, as it causes divisions and clouds the technical issues with ones of \"find the hidden agenda\".\n\nif anyone wants to make FD.o less relevant, please turn it into a political gameshow. if they really want to create standardization (which isn't the same thing as standards, btw) then they ought to try and refocus it on technology.\n\nKDE people are very good at picking up technologies, foreign or indigenous. they also tend to be very wary of political shenanigans."
    author: "Aaron J. Seigo"
  - subject: "Re: Gnome inertia wrt IPC mechanisms"
    date: 2005-06-14
    body: "That's the problem: FD.o's function, its purpose is confusing. Until recently, I assumed (and most people still assume I guess) that it is about developing common standards and functionalities. Than I read this thread, and before that, you blog, and I understand the issues. Which leds to the question: what is FD.o? Is is just a site for hosting desktop related software projects? Is it no longer about developing common standards? Was it ever? Is that a problem? And most importantly: who wants standards? I know KDE does, and by all appearances, so does GNOME. So what went wrong, and how can this be solved?\n\nAs I see it, there is no common body, a kind of steering commitee with members from various projects (GNOME, KDE, enlightenment, xorg) who can agree on what to develop jointly, and who simultaneously have an authoritive voice in the participating projects as well. I don't see any other way of developing shared functionalities or standards. \n"
    author: "molnarcs"
  - subject: "Re: Gnome inertia wrt IPC mechanisms"
    date: 2005-06-14
    body: "FD.o has slowly morphed into a \"collaboration zone\" (their words, not mine). standards of the de facto kind may arise out of these collaborations.\n\nthat seems to be the current position. whether this is good, bad or ugly is another matter, and i'm sure we can find people who would argue for each of those positions ;)\n\nyou're right that there has been a lot of ambiguity, and that has been in part due to the relative silence of those most involved and in part due to some people ill-advisedly creating unnecessary politics around FD.o.\n\nand personally, i don't think we need a steering committee to increase interop. what we do need is active collaboration without politics. both users and developers need to help out with that latter bit. "
    author: "Aaron J. Seigo"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "I think the point made about dbus by the Beagle developers was not that dbus was bad, but that it was not right for their particular needs.  One tool does not necessarily meet all needs."
    author: "coulamac"
  - subject: "Re: Where is  DBUS?"
    date: 2005-06-12
    body: "So a system designed (or desired) for general purpose ipc (inter process communication) doesn't meet the needs of application developers?\n\nThis is one of the common usages of ipc. Quick reliable communication between modules in your application. If DBUS doesn't fit that requirement, what does it fit? As I said above, it's a solution in search of a problem.\n\nBeagle found D-Bus unstable software-wise, unstable interface-wise, slow and non-ubiquitous, ie. not available and difficult to setup. Call us when it's fast, everywhere and dead stable. For now they are writing their own.\n\n\"we'd replace our current uses of dbus with a simple socket message passing system\".\n\nI think it's time to rephrase the debate about D-Bus. The issue has been framed as \"the desktop needs an IPC system\". That isn't entirely true since KDE has a functional and heavily used IPC system. The issue is \"Gnome needs a  workable IPC system\". When Gnome has a reasonable IPC system that is used and supported by Gnome developers, maybe KDE can start talking to it. I don't think it's wise to tie KDE's fortunes (it is that important to a working desktop) to the travails of Gnome attempting to impose/cajol/encourage use of D-Bus.\n\nDo we think that the packaging and binary interface issues with D-Bus will go away if we start using it? DCOP works because its simple and always there.\n\nDerek"
    author: "Derek Kite"
  - subject: "Kopete Implements Yahoo Stealth!"
    date: 2005-06-11
    body: "That's a good news, Kopete is improving soooo fast that it fells new every few weeks!\n\nStill, I miss \"login under invisible mode\" or \"invisible login\", and \"Buzzes\", Love to annoy friends! :D\n\nThanks Kopete Developers for your great endeavor, We the users appreciate it!"
    author: "fast_rizwaan"
  - subject: "Re: Kopete Implements Yahoo Stealth!"
    date: 2005-06-11
    body: "There ARE \"Buzzes\" in the repository."
    author: "Mario"
  - subject: "Re: Kopete Implements Yahoo Stealth!"
    date: 2005-06-12
    body: "I just wish the documentation was up to date. I have an itch in terms of a plugin I'd like and am prepared to write (OTR messaging) but there's very little help there. When I was in the same situation with noatun there was a howto on the website and kdevelop ships with project templates for making plugins. Perhaps noatun is atypical in this regard though."
    author: "mikeyd"
  - subject: "Re: Kopete Implements Yahoo Stealth!"
    date: 2005-06-12
    body: "That would be because noatun /is/ plugins."
    author: "Ian Monroe"
  - subject: "Re: Kopete Implements Yahoo Stealth!"
    date: 2005-06-12
    body: "The inline API documentation is pretty much up to date, but most of the knowhow about writing a plugin is, unfortunately, just in the heads of the Kopete team.  But we're not precious about it - just ask on kopete-devel@kde.org or in #kopete and we will help out.  Don't neglect the Testbed plugin as a template.  Michal Vaner, who had never previously been involved with KDE or Qt, wrote the Skype plugin in the last few weeks, so let that encourage you.\n\nI was just asked about OTR support the other day (hi Ken!) so it would be cool if you could step in and write it."
    author: "Will Stephenson"
  - subject: "Re: Kopete Implements Yahoo Stealth!"
    date: 2005-06-13
    body: "I can see Buzz, Stealth feature in Kopete SVN! Thank you Kopete developers! And skype plugin would be really great!"
    author: "fast_rizwaan"
  - subject: "videodvd:/ illegal in some countries?"
    date: 2005-06-11
    body: "The videodvd:/ KIOslave looks cool, but I think it will be illegal in certain countries like Spain. Months ago the spanish government approved a law that forbids having any hardware or software whose purpose is decrypting protected DVD or CDs. And this KIOslave has just this purpose, so I think it will be illegal. Of course that won't stop the people from using it, but they won't be acting according to the law."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: videodvd:/ illegal in some countries?"
    date: 2005-06-11
    body: "> whose purpose is decrypting protected DVD or CDs. And this KIOslave has just this purpose\n\nNo, as long as unprotected DVDs still exist it cannot be the sole purpose."
    author: "Anonymous"
  - subject: "My DVD and I can't copy it???"
    date: 2005-06-11
    body: "Fuck the LAW which forbids individual freedom! It's your own DVD why can't you decrypt it? You paid the money, you own it! \n\nWe must not heed to the Anti-People laws which curbs our freedom and right to use what belong to us for the sake of Big Multinational companies!\n\nThe Government is for the people and not vice-versa!"
    author: "fr_dude"
  - subject: "Re: My DVD and I can't copy it???"
    date: 2005-06-12
    body: "Where is the government for the ppl? Not in place that I can think of. Look at China and USA. Both government are very much against the ppl."
    author: "a.c."
  - subject: "Re: videodvd:/ illegal in some countries?"
    date: 2005-06-12
    body: " The funny thing about laws of that kind is that they are most likely void.. You have an ***inalienable*** right to \"protect you property\", which means that you can make a copy of your newly bought DVD, and put the original in your burn-proof foot-thick safe. By only using the copy (which will in time get scratched, and all gooed from you children's candy bars), the only thing you have done is to \"take appropriate messures to ensure product lifespan\"... So ofcourse videodvd:/ is not a piece of \"software whose purpose is decrypting protected DVD or CDs\".. It is a backup plugin!\n\n However if there was a dvdripper:/ which allowed DVD->DivX drag'n'drop with automatic filesize of 699.99MB i think we might have a problem with credibility ;-)\n\nHave a nice day!\n\n~Macavity"
    author: "macavity"
  - subject: "Re: videodvd:/ illegal in some countries?"
    date: 2005-06-12
    body: "The ioslave will not be illegal since it does not contain any decryption code and if libdvdcss is not installed it simply reads from the filesystem as if the dvd was mounted.\nIt is the same thing as with all applications that link dynamically to libdvdcss (for example xine or the dvd plugin, libdvdread).\nAt least I think the first sentence is correct..."
    author: "Sebastian Trueg"
  - subject: "Re: videodvd:/ illegal in some countries?"
    date: 2005-06-13
    body: "How can you view a encrypted dvd with anykind of software/hardware/standalond dvd-player if the law forbids you to decrypt it?"
    author: "Rinse"
  - subject: "Re: videodvd:/ illegal in some countries?"
    date: 2005-06-13
    body: "some countries always suck. But there are so many countries , so let us just ignore the one or two countries that are against their people ..."
    author: "ch"
  - subject: "Re: videodvd:/ illegal in some countries?"
    date: 2005-06-13
    body: "I don't believe that: \nin Spain we have the right to make a copy of a CD/DVD even if we don't own the original one, as long as we don't make any money from it.\n\nOn the other hand, we have to pay a special tax for every blank CD/DVD (even if you use it to burn your holiday photos), which it's then handed over the artists.\n \nV\u00edctor, can you link any page with supports your statement?"
    author: "Quique"
  - subject: "Thanks Derek"
    date: 2005-06-11
    body: "as always. And enjoy your week off."
    author: "burki"
  - subject: "kdereview?"
    date: 2005-06-12
    body: "/trunk/kdereview/ commits aren't listed?"
    author: "Ryan"
  - subject: "You just scratched my itch!"
    date: 2005-06-13
    body: ">FEATURE: RMB entry for locking icons where they are, so that they can't be\n>moved.\n>FEATURE: 92789\n\nYESSSS!!\n\nThanks a bunch David :-D\n\n~Macavity\n\n"
    author: "Macavity"
---
In <a href="http://commit-digest.org/?issue=jun102005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=jun102005&all">all in one page</a>):

DCOP client/server implemented for KDE Win32.
New videodvd:/ KIO slave does on the fly decryption from DVD.
<a href="http://kopete.kde.org/">Kopete</a> implements Yahoo! stealth feature.
<a href="http://webkit.opendarwin.org/">Opening of WebCore</a> development yields fruit: DOMParser, and CSS fixes.

<!--break-->
