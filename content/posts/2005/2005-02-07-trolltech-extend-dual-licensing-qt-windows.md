---
title: "Trolltech to Extend Dual Licensing to Qt for Windows"
date:    2005-02-07
authors:
  - "wbastian"
slug:    trolltech-extend-dual-licensing-qt-windows
comments:
  - subject: "ooohhh"
    date: 2005-02-07
    body: "maybe this will get the OOo kids off the VCL crackrock?"
    author: "Ian Reinhart Geiser "
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "must have been a big shock to you, geiseri, no? The shock-of-your-life probably....\n\nHehe..."
    author: "Qt friend"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "Shock yes, biggest, no... you have never gone drinking with Alex Kellett."
    author: "Ian Reinhart Geiser "
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "Come on drinking with Alex is not that bad ... "
    author: "Mathieu Chouinard"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "C'mon, Alex Kellett's drinking habits are dwarfing against these news.\n\nOr are you suggesting you still haven't fully grokked the news?\n\n;-)"
    author: "Qt friend"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "I thought the most important and fascinating part of geiseri's comment was getting OO.o folks to drop that crap called VCL and go QT for UI layer?! Now it's much easier and very much possible. Gotta go and post this news in OO.o forum somewhere.."
    author: "DoesntMatter"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "I might think it were possible to get the Openoffice folks to ditch VCL and go with Qt, but they seem kinda wed to the LGPL.  As opposed to the GPL....\n"
    author: "raindog"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "On the other hand the currently embarrassing state of affairs on Mac OS X for OO.o would suddely look much brighter if they do ditch VCL for Qt."
    author: "ac"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "I think the problem is that StarOffice (Sun's non-free version) counts on being able to use LGPL'd code.\n\nWhile OpenOffice would be able to switch to GPL'd QT, StarOffice wouldn't.  And Sun's still funding much of the development.\n\nI guess if a QT flavor of VCL were built, then OpenOffice could be released for all platforms and StarOffice could still be built for the current platforms, but Sun wouldn't be too happy with that.\n\nAt the risk of being accused of trolling, this *is* the crux of the GPL vs. LGPL controversy over QT.  I guess Sun could afford to ante up for QT licenses, but then again, OpenOffice is GPL code.  Essentially, does this mean that dual-licensed GPL'd code can't be built against the GPL'd version of QT?  Does anybody else see a bit of a conflict there?  Trolltech certainly sees the value of dual-licensing, but thay've made their toolkit unusable by other dual-licensed projects.\n\nThis is probably not intentional, but it's an unfortunate side-effect.  I wonder if there could be an exception carved out in the QPL to allow dual-licensed GPL code to link against QT for free.  Certainly an OpenOffice that's fully portable between Windows, Mac and Linux would be a great bit of publicity for Trolltech."
    author: "Rob"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "Why do you see a problem with dual-licenced projects builing on Qt?\n\nA licencee of the Qt commerical licence can put their code under any licence they want, as long as the licences are either compatible with GPL or the proprietary Qt licence.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: ooohhh"
    date: 2005-02-08
    body: "And quite aside from the freedom of licence granted by the commercial licence, which wuld be sufficient in and of itself, it ought to be understood that the Trolltech folks have often made it very clear that they consider licensing fairly negotiable. They give away free commercial windows licenses for educational institutions, charities, etc. on a regular basis. Their key priority in their GPL dealings has always been to benefit open source and assist in the creation of valuable tools and programs to help society in general.\n\nSo even if the license weren't sufficient, they wouldn't ever sue Sun over StarOffice/OOo. Just one phone call to the Trolls, and they'd sort it."
    author: "Luke Chatburn"
  - subject: "Re: ooohhh"
    date: 2005-02-08
    body: "No...  The Qt problem with dual licenses is the dual license, in particular, one that permits\nproprietary (as opposed to commercial) use, without requiring that proprietary user to purchase a license from\nTrolltech.  If that weren't an issue, they'd LGPL Qt instead of GPLing it.  They chose the GPL in part because it\nprevents proprietaryf use, so anyone wishing to base their proprietary stuff on it has to buy the commercial license.\n\nThey wouldn't have an issue with the GPL side of the dual license, but allowing Sun commercial use would mean Sun would\nneed to pay in some way, either buying the commercial license, or making other arrangements in some sort of one-off\nlicense agreeable to both parties.\n\nAnything LGPL licensed can be linked against GPL licensed stuff (and the reverse), but the problem Trolltech would have\nwould be the more liberal terms of the LGPL in regard to Qt -- it couldn't relicense it for OOo or anyone else under\nLGPL or it would effectively be allowing that license everywhere.  Therefore, the situation on the ground would be that\nproprietary reusers of OOo code, if it linked to Qt, would still require the commercial/proprietary Qt license. LGPL\nreusers would be free to use the Qt code, but would have to keep it separate and avoid co-mingling of the code, so as to\npermit Trolltech to retain its rights to require proprietary users get a commercial/proprietary license.\n\nThus, it could legally be done, but the OOo folks might not wish to do it, because that would require what they may view\nas artificial constraints on reuse terms as opposed to the LGPL, freezing out Sun and other proprietary users who would\nthen have to make their own arrangements with Trolltech for Qt, a tradeoff Sun in particular may be unwilling to make,\nas may be certain other OOo contributors as well, because it would effectively require some licensing negotiation where\nOOo currently doesn't require it, in the case of proprietary users of the otherwise LGPLed OOo code.\n\nIf that makes any sense...\n\nDuncan"
    author: "Duncan"
  - subject: "Re: ooohhh"
    date: 2005-02-08
    body: "I am afraid I don't get the problem.\n\nRob claimed that it would be a problem to build dual licenced code on Qt, but as I said, as long as you have the proprietary licence of Qt you can licence your code anyway you like.\n\nLets say I write some extension library and dual licence it under GPL and some licence allowing gratis use in proprietary software, for example CPL.\n\nDevelopers can then choose under which licence they want to use my library, in case of GPL they can even use the GPL licenced Qt, in case of the other they will have to get themselves a proprietary Qt licence.\n\nNeither case restricts my options for licencing my code, in fact I could just LGPL my library, making it available for both parties end even require proprietary users to make changes available again."
    author: "Kevin Krammer"
  - subject: "Re: ooohhh"
    date: 2005-02-07
    body: "Hey,\n\nOOo will have KOffice on Windows as a concurrent now. With common import filter and common native OASIS file format, KOffice will be much nicer in many cases.\n\nI am sure I shall recommend it over OOo.\n\nKay"
    author: "Debian User"
  - subject: "Re: ooohhh"
    date: 2005-02-08
    body: "Have to port the everything required by KOffice to Qt4, first.  And it has to be ported to run on Windows, also. :-)\n\nPersonally though, I'm hoping someone ports Konqueror, or at least makes some KHTML browser for Windows.\n"
    author: "Trejkaz"
  - subject: "Very nice"
    date: 2005-02-07
    body: "Very great news.\n\nI hope it won't be abused so Trolltech will change their mind again."
    author: "Tim Beaulen"
  - subject: "Re: Very nice"
    date: 2005-02-07
    body: "Isnt it hard to change their mind on this one?\n\nonce both versions are dual-lic'd, and trolltech -at one point- stops lic'ing the next Win vesion under GPL. Then it is easy to merge the differences of the last and the next GPL (unix) version with the last, still GPL, version on Windoze.\n\n1. i hope you, dear reader, anderstand the above (if yes: well done!)\n2. i hope i, am not talking shit here\n\n_cies."
    author: "cies"
  - subject: "Wow"
    date: 2005-02-07
    body: "I really think this is something that can potentially change the world.\n"
    author: "ac"
  - subject: "Link to GPL Qt/Windows Faq"
    date: 2005-02-07
    body: "http://www.trolltech.com/developer/faqs/duallicense.html\n\nPretty cool stuff!  We'll have to wait and see what kde applications get ported to windows, with the KDElibs/win32 port already available.   \n\n-Sam"
    author: "Samuel Stephen Weber"
  - subject: "Is a kde-shell possible?"
    date: 2005-02-07
    body: "I wonder, how much work has to be done to get a kde.exe shell, so that all those windows guys can ditch their explorer.exe and have a Kicker, Kontact, Konquerer and all those nifty programs?\n\nI guess that the porting to Qt/Win32 is already a bit underway, because the Qt/Mac guys are also trying to get rid of all these X11 dependencies. A possible Qt/Win32 port should profit by that.\n\nPlease don't bring that religous flames up, that someone shouldn't port FOSS to Win32, because that would mean to support a proprietary operating system. I'd really appreciate a KDE-Win32-Shell, because on day 1 you introduce kontact in your company, on day 2, you substitute the explorer with konquerer and on day 3 the shell with kicker. If you can get your staff used to kword as well, they won't even notice the difference, once the underlaying system is not windows anymore. (be it bsd, linux or some other free os).\n\nI hope, that some companies will see it like this and will support and maybe even sponser the KDE-Win32 port."
    author: "Anonymous Coward"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Just say NO to FOSS on Win32.\n\nMake them suffer, because that's the only way they will be willing to switch."
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "On the contrary.\n\nMaking the applications available on both Linux and Windows will eventually put more people on Linux.\n\nWhy?\nBecause one of the biggest complaints now is... the program I use, or something very similar in look and feel, is not available in Linux.\n\nMy opinion though.\nMight still take years and years."
    author: "Tim Beaulen"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "On the contrary.\n\nWhy should I switch when all the games or that business app that I need + my favorite FOSS/KDE apps are available on Windows?\n\nWhy should I ask my business app provider to port to Linux?"
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Why you should switch?\n\nSimple...\nSuppose that your favourite games and programs are available on Linux and Windows.\n\nWhat would you choose? Windows or Linux?\nI would choose Linux, as it would save me a little bit of money.\n\nI'm talking about the cost of buying linux or windows, not about the cost of support."
    author: "Tim Beaulen"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "> Suppose that your favourite games and programs are available on Linux and Windows.\n\nThat just isn't the status quo:\n\n1. Most PCs come with a default Windows installation that means most people have Windows already available (if they want to or not).\n2. Most games run excusively on Windows.\n3. Most business apps run excusively on Windows.\n4. Most people I have met don't care about the operating system. They want good apps.\n\nSo \n\n- they already have Windows (actually most people have to buy Linux..think SUSE)\n- they have the games\n- they have the business apps\n- now they also get our good desktop apps\n\nWhy switch?"
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Hmmmm, yeah, indeed...\nPoint 1 is very frustrating.\n\nBut point 4 is a big downside for Windows itself too.\nWindows is it's own enemy.\nThe pc comes standard installed with windows 2K for example... why switch to xp?\n\nbut yeah, you're right that most people are very conservative.\n\nOn the other hand, I believe that exposing more and more users who never even heard about kde or gnome or kmail or evolution etc... will eventually make those people also read and think about linux. And in the end, maybe a small fraction of those people will swith from windows to linux. Maybe also the other way around."
    author: "Tim Beaulen"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "You're right point #4 isn't a strong point for Windows.\n\n> On the other hand, I believe that exposing more and more users who never even heard about kde [...]\n\nIMHO Live CDs are much more powerful here. A few months ago I gave our Windows admin a Knoppix CD. He was amazed because most stuff was automatically setup (IP address, SMB, graphic card, etc). \n\nNow he's really interested in Linux and he installed it at home. I don't think that installing Firefox or OpenOffice on Windows would have had the same effect."
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "This is bad news folks!  \n\nI don't care what the KDEonWin32 ppl say, this is a bad deal.  I have 3 people who are using liveCD's and testing Linux/KDE based solely on the application quality and integration of KDE.  I am willing to bet all of the money I have that given the choice they will stay on Windows!  They don't want to switch... most of their games/programs/friends are on Windows.  Lets face it, to most people Windows if free, as fair as their concerned (stole it or got it with the PC.)  There is NO argument I have seen that can convince me that KDE on Win32 is a good idea.  Some applications (like Office and Browsers) makes sense on both platforms; but KDE is a selling point for FOSS software.  Something unique.  Something better than ANYTHING on their platform.  Something to bring ppl to Linux... and now that selling point is going to disappear.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "I also want to note this:\n\nThere is a difference between an OS developer and an application developer.\n\nAn OS developer wants to get as many users using his/her OS.\n\nAn application developer wants to get as many users using his app.\nIf this can be OS independent, that's even better as it means potentially a lot more users."
    author: "Tim Beaulen"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Thats really the point isn't it.  KDE on Win32 is probably good for KDE (at least in the short run), however it is really bad for Linux.  Without Linux we would not have had KDE.  If this kills Linux on the desktop, KDE will also be hurt in the long run.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-09
    body: "You are missing the point. KDE on Win32 couldn't POSSIBLY hurt linux. All it can do is increase usage of KDE apps. Since KDE apps will natively run on Linux  and be PORTED to win32, how can you say that this would kill Linux?\n\nIf ANYTHING it would get more people using KDE apps for their primary use, and make the OS platform become even LESS relevant to the end user. This would instantly mean that it would no longer be a death certificate for a PC manufacturer to ship Linux on new systems now that all their customers will be using NATIVE linux apps... Why would they ship WINXP on a system that comes with K3B, KMail, iCalendar connectivity (to replace Outlook/Exchange) and FireFox/Konw (to replace IE).. Why would a MFG WASTE 100-150$ per unit shipped to buy the OEM copy of winXP when they could ship thousands of copies of linux for $0 and save MILLIONS? What is the incentive of PC MFG's to ship winXP if all their customers are using FOSS software primarilly?\n\nYour point makes no sence because it doesn't take these factors into account. If these ported QT apps became as popular as you fear they would, then there would be NO incentive to purchase a more expensive PC that shipped with WinXP."
    author: "mp3phish"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-09
    body: ">>Why would they ship WINXP on a system that comes with K3B, KMail, iCalendar connectivity (to replace Outlook/Exchange) and FireFox/Konw (to replace IE).. Why would a MFG WASTE 100-150$ per unit shipped to buy the OEM copy of winXP when they could ship thousands of copies of linux for $0 and save MILLIONS? What is the incentive of PC MFG's to ship winXP if all their customers are using FOSS software primarilly?\n\nBecause the OEM's don't pay 1000150 per copy, because if the OEM's ship ONE copy of Linux they DO pay 100-150, because they know that Linux will not run Halo 2, because they know that their costomers are using a mix of FOSS software and tnon-FOSS  software adn there is only one platform that does that!  All this does is expand the product offering of Windows and DOES NOT increase what we have on our platform.\n\n>>If these ported QT apps became as popular as you fear they would, then there would be NO incentive to purchase a more expensive PC that shipped with WinXP.\n\nYour statements make no sence because you fail to take into the fact that OEMs (Dell, HP, and even until recently IBM) charge MORE for PC's without an OS because that is what their OEM licence for Windows deads that they do.\n\nI am not speaking from a black hole or simply guessing that this COULD be the problem.  This same kind of problem killed OS/2 for OEMs, and Linux is LOOSING users to Mac OSX because of this VERY SAME REASON.  \n\nbrockers"
    author: "brockers"
  - subject: "in a world without walls and fences, who the f* .."
    date: 2005-02-08
    body: "...needs windows and gates?\n> ...\n> Why switch?\nbecause Windows sux badass?\nI mean, I've used it to the limit from 1995 on, and it is the most worthless piece of crap EVER assembled to run on computers. It simply does not work - it is a giant, badly implemented pile of design-flaws composed with bugs and non-sense, laminated with an eye-contamininating skin of worst-colored unusable illogical user interface. Screw that - fully converted to Gentoo Linux in 3Q2004 (took some sweat and bubblegum, but hey it flies, too) - and mighty proud. Guess what - it has no limits. You decide, it follows the order. Only hardware's the limit..\nIt never even came to my mind to boot my XP partition - when I tried once some time ago - it instantly BSODed on startup...\nwhy bother with a shitpile that costs money if you can have a cake (and eat it, too) for free?\nM$ is going down, and I love it. All the grief, all the pain... it's all coming back on them pretty soon. Hurray.\nHail Eris. !Live! the discord. !Feel! the power of the chaos. muah!!!111!1!"
    author: "thatguiser"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-09
    body: "Re point #1: Read comments above to notice that if FOSS applications are ported to windows, people will be used to them and slowly come off the high that is Microsoft (perfect example: OOo, FireFox, Thunderbird, iCalendar/Sunbird [replaces exchange]) Now that people are switching to OOo, FF, Tbird like a flock of wild birds (yes thousands per day) it breaks them from the stronghold that is MS office, Outlook/Exchange, and IE. Save this thought for later discussion below.\n\n#2: good point. But you forget that most OpenGL games are tri-platform (and several companies do it on purpose so they can keep the Mac marketshare which is growing) Not to mention that _almost_ all DX games work under Transgaming when paired with an nVidia card under linux. This includes HL2, Zoo Tycoon, The Simms, etc etc... Anything that is on the top sellers list.\n\n#3: You have a point here but that is where my point #1 comes in. The business apps (outlook/exchange, IE, MS Office) are slowly being switched over by significant numbers of people to FOSS alternatives which have been ported and stable on windows. \n\n#4: Your point 4 actually helps the point I'm trying to make. Imagine KMail, Konq, K3B, and other popular FOSS QT aps get ported to windows. All these apps have PAY versions in windows (Nero/Roxio for CD-R, MS Office for office apps, Exchange server for outlook+calendar integration) These software packages do NOT come with windows and have a VERY significant cost to the end user. CD-R Software for windows is 100 USD, MS Office is 400 USD, Exchange server charges THOUSANDS of dollars per year for licensing.... Every single one of these benefits can be gotten _FOR_FREE_ on FOSS servers and applications with the SAME or better functionality. And more reliable.\n\nNow I will make my point #5: Imagine that these programs slowly do replace MS apps (MS office, IE, NERO/Roxio, Exchange server) with the proliferation of FOSS apps on windows because of Free QT on win32. Imageine 2 years from now (assuming these apps have been ported) where now there is no reason to order a computer with WinXP on it because you don't use any windows specific applications anymore. Why would a manufacturer ship winXP on a new machine if it is targeted at a user who is only using K3B, FireFox/Konq, Thunderbird/Sunbird/iCalendar, OOo, etc? The facts are that MOST people could move away from all applications which require windows if FOSS QT apps were ported over. And the facts are that if this were to happen, there would be NO incentive to ship MS Windows on a new system when the manufacturer can simply ship a license free OS at significantly reduced cost.\n"
    author: "mp3phish"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-09
    body: "Re #2: I know about the OpenGL games. I play them on my Linux system, like i.e. Neverwinter Nights. But those games are still rare compared to DX games.\n\nAbout Transgaming, I'm not sure because I haven't tried it. That's because I do have a Windows system. So why should I fight with non-MS Windows implementation when I have the real thing (and most people do have Windows). \n\nTransgaming or WINE will always be behind Microsoft. For example what will happen with Longhorn when there are many APIs changes? I'm confident that they will have a hard time catching up.\n\nRe #3: Just to clarify my point. I wasn't talking about outlook, MS Office, etc when I talked about business apps. For me, business apps are ERP software, real estate management, call center solutions, archiving software, route optimizer...\n\nThat's missing on Linux in big numbers. Especially with the support that businesses expect.\n"
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2008-01-16
    body: "1. If the key app's that people use are available on linux then there will be more demand for Linux. More demand for Linux will equate to more support from  vendors.\n\n2. I don't care about games. Neither do the people in my department.\n\n3. In our department the tech support folk [myself included] play a key role in deciding who runs what OS. We create our own disk images for school computers, and we inform ppl about what OS / applications best suits their needs. \n\nWe would much rather support a product that is multi-platform. This is much better than having to support 3 completely different app's for WinXP, OSX, and *nix. That just means 10 times the headache [yeah you do the math.. definitely more than 3 times harder].\n\nI do care about security and viruses.. as does your average Jo running WinXP  in our department.\n\n5. MS Word, Excel, and MS Outlook are a crucial part of my work. Why should I switch to Linux if I'm not familiar with it's applications? Too much to learn all at once. \n\nIf the applications I need to get my work done can run on Linux then why shouldn't I switch to Linux? After all I don't care much for my OS [WinXP]. I know Linux is a rock stable OS, and I know MS Windows is flakey.. but I need to get my work done! I can't stuff around for a week doing my work inefficiently just because I am unfamiliar with the applications.\n\n\n"
    author: "z"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "\"Why should I switch when all the games or that business app that I need + my favorite FOSS/KDE apps are available on Windows?\"\n\nor: \"Why would I continue to use Windows, if all the apps I currently use on Windows were available on Linux as well?\".\n\nSeriously, I can think of several reason to switch to Linux:\n\n- Better security\n- No viruses\n- More choice\n- Free as in speech and in beer"
    author: "Janne"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: ">>\"Why would I continue to use Windows, if all the apps I currently use on Windows were available on Linux as well?\" \n\nBecause this will never be the case.  Do you honestly think that MS will port Office, Outlook, Halo 2, or anything to Linux?  Seriously?  Not a chance.  There will always be some applications that people want that are only available on Windows.  The only way to make F/OSS more appealing is to have MORE applications that they want on F/OSS platforms; and only on those platforms.\n\n>> Better security\n\nJoe Sixpack does not care!\n\n>> No viruses\n\nJoe Sixpack does not care!\n\n>> More choice\n\nNot if they can use OSS software AND Windows software on Windows!\n\n>> Free as in speech and in beer\n\nRepeat after me... Joe Sixpack does not care!  The vast majority of computer users see computers as a tool; not a vehicle to express their first amendment rights.\n\nThere are some application exceptions (i.e. their are some applications that SHOULD be ported, i.e. office applications) but otherwise this is a BAD BAD BAD THING.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "\"Because this will never be the case. Do you honestly think that MS will port Office, Outlook, Halo 2, or anything to Linux? Seriously?\"\n\nNo. Did I make that claim? I said that the kick-ass software that the user uses in Windows could also be available on Linux. And why couldn't that software be open-source software? Seriously?\n\n\"There will always be some applications that people want that are only available on Windows.\"\n\nAnd there will be applications that will only be available on Linux/*BSD. So what's your point?\n\n\"Joe Sixpack does not care!\"\n\nHe will care if the resident tech-support guy (read: the computer-literate relative) tells him to move to Linux. And if someone mentions to him \"hey, why do you waste $200 on Windows, when you can get same functionality for free?\"\n\n\"Not if they can use OSS software AND Windows software on Windows!\"\n\nAnd who is to say that the OS-software can't be so good that the user will choose it instead of \"Windows-software\"? IE is \"Windows-software\", does that mean it's good?\n\n\"Repeat after me... Joe Sixpack does not care!\"\n\nNot all of them, but some will. And those who do care, are the ones who the Joe Sixpack turns to when he wants advicee on computers.\n\n\"There are some application exceptions (i.e. their are some applications that SHOULD be ported, i.e. office applications) but otherwise this is a BAD BAD BAD THING.\"\n\nNo it isn't. First you get the users hooked on OS-software. After they have gotten used to them on Windows, moving them to Linux is trivial. Do you really expect the users to switch from Windows to Linux, and expect them to replace all their apps as well? That's not going to happen. You know it, and I know it. But if you can get them to switch the apps to OS-equivalents, changing the underlying OS is more than doable. But if we will try to replace their apps AND the OS in the same time, it will be very, very difficult.\n\nAnd besides: what makes office-apps an exception to the rule?"
    author: "Janne"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: ">> And there will be applications that will only be available on Linux/*BSD. So what's your point?\n\nMy point is that alot of that software, that kick ass software Linux/BSD only software, will now be available on Windows... even more software that is available on Windows with nothing coming back towards us.\n\n>> He will care if the resident tech-support guy (read: the computer-literate relative) tells him to move to Linux. And if someone mentions to him \"hey, why do you waste $200 on Windows, when you can get same functionality for free?\"\n\nUsers don't switch because of price (if they did then they would have already.)  Users switch because the software they want runs on their particular computer.  Users don't care about Linux... they care about their applications.  Show them an application they cannot live without (that only runs on Linux) and they will switch.  That is how the half-dozen people I have gotten to switch have switched.  They sure as hell didn't do it to save money on an OS they pirated anyway.\n\n>> And who is to say that the OS-software can't be so good that the user will choose it instead of \"Windows-software\"? IE is \"Windows-software\", does that mean it's good?\n\nExactly... if OS-software becomes so good the user chooses to switch, they will.  BUT WAIT.. that same OS-software is already able to run on your CURRENT OS.  So why switch?  Because the KDE software is sooo much better than anything one Windows?  Wait.. they will be able to run the so-much-better KDE software on Windows... nevermind.  They may (and in many cases will) choose the better OS-software but they will have no reason to do so on the FOSS operating system.  This announcement may be good for KDE (in the short run) but it is very bad for Linux.\n\n>> Not all of them, but some will. And those who do care, are the ones who the Joe Sixpack turns to when he wants advicee on computers.\n\nMy tech department cannot force its own employees to use a given OS because the users SCREAM when the OS we tell them to use will not run their copy of (pick a random Windows application.)\n\nRepeat after me... users DON'T CARE about OSes, they care about applications.  All we have done is improved the software stack of WINDOWS.  We have don't almost nothing to help Linux.\n\nYes, some will try linux (as some already do) but what we have really done is decrease the incentive to try Linux... not given them any reason to pick Linux's software stack over Windows software stack.\n\n>> First you get the users hooked on OS-software. After they have gotten used to them on Windows, moving them to Linux is trivial. Do you really expect the users to switch from Windows to Linux, and expect them to replace all their apps as well? That's not going to happen. You know it, and I know it.\n\nOk, we get them hooked on KDE on Windows.. then we move them to Linux.  But why? I could switch because Linux is more stable but I will not be able to run my other applications.  \n\n>>  But if you can get them to switch the apps to OS-equivalents, changing the underlying OS is more than doable.\n\nIf what you say where true than Mac OSX should have been a windfall for Linux.  Run all of unix software stack AND get MS Office, Photoshop, and Dreamweaver; find out you like the Unix stuff better... switch to Linux.  But that is NOT what has happened. Instead every survey I found says that more Linux users are switching from Linux to Mac OSX than Mac users are switching to Linux.  Mac OSX hurt us.  Its a fact!  Why? because they can run their Linux apps AND their OSX apps and they don't give a damn about price or freedom.\n\nIn the end all this will do is give Windows more great applications.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "<i>If what you say where true than Mac OSX should have been a windfall for Linux. Run all of unix software stack AND get MS Office, Photoshop, and Dreamweaver; find out you like the Unix stuff better... switch to Linux. But that is NOT what has happened. Instead every survey I found says that more Linux users are switching from Linux to Mac OSX than Mac users are switching to Linux. Mac OSX hurt us. Its a fact! Why? because they can run their Linux apps AND their OSX apps and they don't give a damn about price or freedom.</i>\n<p>If your numbers are right, you got the point but... everything in a web forum has a 'but'</p>\n<p>Joe Sixpack doesn't care, even he doesn't think. He will use what people use. He will be the last one to switch. He is the last checkpoint.</p>\n<p>FLOSS and distros must concentrate, and they do, in targets that can be acomplished, these are corporate desktops. A easy migration path is *must*. And this means trying to replace, first, horizontal apps, as browsers and office suites. Then the OS. Joe Sixpack will use what his admin tell him. And this cares, thinks and there are many who want to do it, but can not.</p>\n<p>These massive migrations also happen in a school and a faculty. And means money for distros. Money to survive. Money to the kernel, to the mail servers and the desktop.</p>\n<p>There should be both type of apps. Some (horizontal ones) to help migration. And others unique to add value to FLOSS OSes. Remember: embrace and destroy!</p>\n"
    author: "mcosta"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Ooops I did not see the 'plain text' field. There was a 'preview' button before?"
    author: "mcosta"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "\"Users don't switch because of price (if they did then they would have already.) Users switch because the software they want runs on their particular computer.\"\n\nyou get it. Yet you don't. I have been saying all along that with Free software on Windows, we can lure the user in to Linux. If they start to use that free software on Windows, making them switch to Linux is trivial. Like you said: \"users switch because the software they want runs on their particular computer.\". And if they notice that the software they use is also available on an OS that offer several advantages over Windows, why wouldn't they switch? I mean, the apps are already there.\n\n\"that same OS-software is already able to run on your CURRENT OS. So why switch?\"\n\nBecause:\n\n- Linux is free (both in price and in speech. And people DO care about the price!)\n- Linux has no viruses\n- Linux has better security\n- Linux has more choice\n\nSeriously, do I have to take out the good 'ol clue-by-four? This is not rocket-science! Right now, the advantage Windows has over Linux are the apps. If we get people hooked on Free software on Windows, that advantage will disappear, since people can switch to Linux (which is otherwise superior to Windows) and keep on using their apps! While Windows might have an advantage in absolute number of apps, all that matter is the apps that people actually use. We already offer a kick-ass browser (Firefox). OpenOffice is getting better all the time. Those two are major apps that people will want to use. But how could they find out about them, if it didn't run on their OS (Windows)? Now they can use them on Windows, and some day they will think that \"hey, why do I have to suffer this virus-ridden Windows, when I can use Linux, and keep on using my apps like nothing had happened?\"\n\n\"If what you say where true than Mac OSX should have been a windfall for Linux.\"\n\nNo, since Mac OS requires specific hardware from specifig vendor. And the hardware or it's price-point might not be suitable for the user.\n\n\"Mac OSX hurt us. Its a fact!\"\n\nStating that it's a fact does not make it a fact. Care to show any real studies on this? And every study I have seen suggested that Linux is growing faster on the desktop than Mac is.\n\n\"In the end all this will do is give Windows more great applications.\"\n\nFunny, I thought we were giving the USERS more great applications...."
    author: "Janne"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "- Linux does not have DirectX --> almost no games\n- Linux does not have the amount of applications business needs like real estate management, small/mid-size ERP software, etc\n\nIt's not all about browser, ms exchange and ms office."
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "\"- Linux does not have DirectX --> almost no games\"\n\nLinux might not have DirectX, but it does have SDL, OpenGL and the like.\n\n\"- Linux does not have the amount of applications business needs like real estate management, small/mid-size ERP software, etc\"\n\nThen we need to offer people the missing software, free or otherwise. That is what I have been saying all along. And how many Joe Sixpacks need real-estate management software, ERP and the like?\n\nP.S. Some of my comment might have been a bit harsh in my previous comment, and I apologize for that."
    author: "Janne"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "Hi Janne\n\n------------------\nI AM a WinXP user.\n------------------\n\nIf KDE ported their apps to Windows, I would use them on WinXP without switching to Linux. It really is that simple for me.\n\nThe reason why I haven't switched is the speed of the interface. I know this has been covered to death, but the problem remains.\n\nKDE + apps offer all the funtionality I need from a desktop already, so that is a non-issue.\n\nI even bought Linux Format (LXF58), which had Gentoo on the cover disk. I followed the instructions and compiled both Gentoo + KDE from scratch. My conclusion: Win2K still felt significantly faster to use on a daily basis than Gentoo + KDE on my P2 400MHz. \n\nI now have a Celeron M 1500MHz laptop running WinXP. I will try Gentoo + KDE 3.4 when it is released.\n\nLinux + KDE slows me down. MS Word loads as fast as it can draw all the widgets on FIRST load on my 2.4GHz machine at the office, which seems to take just under 1 second. It is instantaneous on second and subsequent loads. Anything longer than that is unacceptable to me. I use Firefox and Thunderbird (as alternatives to Explorer and Express), but these two apps still take too long to load. Anything more than 1 second is unacceptable for an application that is potentially opened and closed many times an hour. I don't keep apps open if I am not using them at that moment. Why? Because I can open and close them so quickly.\n\nThe day KOffice, Konqueror, amaroK etc are ported to WinXP is the day I stop reading the Dot and following the progress of KDE. Why? There would no longer be a need to see what is happening on the other side of the fence.\n\nKind regards\nH."
    author: "WinXP User"
  - subject: "Re: Is a kde-shell possible?"
    date: 2006-04-17
    body: "umm, if you want to use linux apps, in windows -- ??? not sure why not dual boot -- I would just download one of those partial distrobutions, you will have to do some searching (www.linux.org.com) you can search through distro there, any ways, you can get distros that are not entireally a real operating system, but they run in windows for that sort of then,not sure how efficient on system resources though."
    author: "Abic Shadar"
  - subject: "Re: Is a kde-shell possible?"
    date: 2006-04-17
    body: "http://www.linux.org i mean"
    author: "Abic Shadar"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-09
    body: "\"ecause this will never be the case. Do you honestly think that MS will port Office, Outlook, Halo 2, or anything to Linux? Seriously? Not a chance. There will always be some applications that people want that are only available on Windows. The only way to make F/OSS more appealing is to have MORE applications that they want on F/OSS platforms; and only on those platforms.\"\n\nYou miss the point entirely. Nobody is claiming MS will port MS Office, Outlook, Halo2, or any of that BS garbage. Halo2 is a FLOP(this should be in another discussion but I will report it here: Halo2 sold more games on opening day than any other game in history. It has since sold fewer games than any other top selling game in history). Back to my main point. These programs will NO LONGER BE REQUIRED if FOSS apps were available on windows. Just like people are ditching IE for FireFox. Just like people are ditching Outlook/Exchange for Thunderbird/Sunbird/iCalendar. Just like (some) people are ditching MS Office for OOo. Only locked in businesses need these things. And only high end PC gamers are buying systems that play Halo2. Did you know that Dell's top selling home PC is the Dimension 2400? That computer doesn't even HAVE A 3D Card and WILL NOT PLAY 3D video games. The principles discussed above will account for MOST of the people in the world. Your argument only accounts for a MINORITY of the people in the long run.\n\nFor your argument to make sence would imply that if people ported QT apps to Win32, that nobody would switch to them from the windows alternatives. If this was the case, then NO HARM DONE. But if people DO SWITCH to them, then there is NO LONGER A NEED for Outlook/Exchange, MS Office, MSVC++, Publisher, NERO/Roxio Burning software, ETC ETC.\n\nSo your argument makes its point against itself. It requires the success of KDE apps on Win32 for it to cause harm. But that success would automatically prove you wrong. "
    author: "mp3phish"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-10
    body: "> Joe Sixpack does not care!\n\nThe preferences of home-users are irrelevant; what matters is that  organizations (businesses, governments, schools, etc.) do care about license costs, security, etc., and if they switch, the home-users will eventually follow. Remember, in the 1980s, there were the so-called \"home computers\" (Commodore 64, Sinclair ZX80, Apple II, etc.), whereas the IBM PC was designed for business users; the IBM PC beat the others because people wanted to use the same platform at home that they use at work."
    author: "rqosa"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "> Making the applications available on both Linux and Windows \n> will eventually put more people on Linux.\n>\n> Why?\n> Because one of the biggest complaints now is... the program \n> I use, or something very similar in look and feel, is not available in Linux.\n\n\nSince years now, a hell lot of FOSS is available on macosX fixing for free (money and efforts) every gap and lacks the platform had. (For example, everybody uses VideoLan Client and/or mplayer. Having only QuickTime would have been a pain in the ass). If your theory is good, you would have seen a lot of people working as secretary in your office switching to Linux. \n\nHave you ?\n\n\nNote that I don't care/even welcome the macos port, because Apple will ever remain a niche market, and because there is 40 times less concurrency there than for Win32. The later means that a lot of FOSS app will have a realistic chance there, so it's still a win for \"us\" if you switch from windows to macosX. (If you switch from Linux to macosX, it's a net loss though). \n\nWin32 is a whole other story."
    author: "jmfayard"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: " Since years now, a hell lot of FOSS is available on macosX fixing for free money and efforts) every gap and lacks the platform had. (For example, everybody uses VideoLan Client and/or mplayer. Having only QuickTime would have been a pain in the ass). If your theory is good, you would have seen a lot of people working as secretary in your office switching to Linux. \n \nHave you ?\n\n-------------\nIn the office I work?\nNo, unfortunatly no.\n\nIn other offices.\nYes, I've seen a lot of offices switch from Windows to something else, including linux.\n\n\n\n\n\nNOTE THOUGH:\nI did never mention FOSS software.\nI was talking about software in general.\nAs in... for example, Dreamweaver or Photoshop for linux too.\n\nIt would make lots of people think more about linux, and probably even switch.\n\nIf it really would be that way.\nI have absolutely no idea... I don't have a magic cristal ball :)\nIt's just my opinion.\n"
    author: "Tim Beaulen"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "But the point jmfayard made is spot on.  The next movement in terms of MacOSX and Linux is TOWARD OSX.  The general rule of thumb is that more people are moving TO OSX from Linux than the other way around.  Why?  Because, \"I can have all the power of a unix system with a nice Mac polish and Microsoft Office natively supported.\"  OSX running Linux apps have HURT Linux, PERIOD!\n\nThis is bad news for F/OSS!\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "Linux as a OS has no advantages over MacOS but the freedom and the price.\nMacOS X is UNIX, it's safe, it does not have viruses, it does not have spyware, so, why to choose Linux when you alredy paid for MacOS X when you bought your computer?.\nWindows is a completely different story. I know a lot of people (Joe users) that are worried about virueses and spyware and run antivirus and firewall software consuming their resources and making the system a lot more unstable. I even have one of them running Ubuntu in his laptop (I know it's GNOME, but gnome-volume-manager is a big advantage to GNOME until the media:/ kioslave is released with kde 3.4). If they run kde in their windows machine and use only free software (konqueror/firefox, kmail/thunderbird, openoffice/koffice) then I could say, look at this! In that OS you can have all of this and there's no spyware nor viruses!\nI think porting kde to windows could make the switch to linux easier."
    author: "Jose"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "I agree. If linux is to move into a dominant position, businesses and governments must be able to have the applications they current use run in a linux environment. Consider the area of emergency services (Police, Fire and EMS). There are no commercial quality Linux apps available for dispatching, call taking, incident reporting, etc. \n\nWhy would a government make a switch to Linux, when it would then have to support two operating systems? One for office apps, another for service operations, it is a nightmare situation. Then there is the problem of using the data generated on one system and importing it to the other system.\n\nThe more applications that are cross platform, the better.\n\nThen why would they switch from Linux to Windows? Several reasons. My agency was force to switch to XP from NT4 due to lack of support from MS. NT4 did the job, but it was no longer supported. An agency with 38 stations, a training academy, etc, that is a lot of money. Now if I could do a gradual change to Linux, cause all of the apps were cross platform, the situation changes. I don't have an enormous outlay of cash because MS decides it is time to stop support. I can budget expenses based on my timetable, not MS. And lets not forget the ever popular MS software audits, that cost the agency time and money. \n\nI agree, the cross platform support is critical to making Linux dominant.\n\nJust my two-cents worth"
    author: "Bob Austin"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Making them suffer != making them switch.\n\nSometimes I have to use Windows in developing. I use Linux as much as I can, but our company sells Windows software. If I could have all of my favorite KDE apps in Windows, I'd be much happier. Making me suffer, in this case, makes no sense."
    author: "Bad_Bob"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Why does your company sell Windows software?\n\nMy guess:\n\na. Your customers use Windows\nb. Your customers don't have enough problems with the Windows platform to favor a switch to Linux.\n\nWill the availability of KDE applications on Windows improve the situation?\n\nIMHO no. It will actually make it worse because it solves some problems that Windows user have (see Firefox) and it ports the great features like kioslaves to the windows platform."
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Sometimes you use Windows in developing... You try to use Linux as much as your can but your company sells Windows software.  If you could have your favorite KDE apps in Windows, you would be happier.  Being able to use those KDE apps in Windows is gonna make you more likely to use Linux... NO?  Of course not!  \n\nIf you need a KDE app and it only runs on Linux... you start up Linux.  If you now have it on Windows and you are using it on Windows then why bother starting up Linux.  KDE on Win32 HURTS Linux... period! Its a simple fact.  Some people (like yourself) will continue to use Linux/KDE, but for the vast majority of people all this means is that their Windows machines will have more/better software for them.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "So you're saying that choice is a bad thing? That we shouldn't give Windows users F/OSS at all? \n\nWhat do you think about Firefox, OpenOffice, etc.? Did they hurt Linux?"
    author: "Bad_Bob"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "> What do you think about Firefox, OpenOffice, etc.? Did they hurt Linux?\n\nActually yes, because Windows user now have a viable alternative to the \"bug contaminated\", old IE. One less reason to switch to Windows."
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Being on multiple platforms helped Firefox and Open-Office... but they did little to help Linux.  Open-office is one of the only possible exceptions that I can think of, because it opened a file format to use on any platform.  But the reverse would not be true because KDE's file formats are already open. \n\nFirefox is an extension of Mozilla which is an extention of the old Netscape.  Netscape was arguable ported to Linux and not the other way around.  And yes, porting Netscape to Linux HURT Windows.  Now that Linux/KDE apps will be ported to Windows they will most assuredly hurt Linux.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "Swings and roundabouts. There are many ways to look at this.\n\nAs you have said, Joe user generally doesn't care about the OS they use, but they do care about applications and more precisely, getting the task in hand done. In the home environment, this probably isn't going to change much. But in a company, a sys admin who is aware of potential, valid, professional benefits of switching to Linux may rejoice at this news.\n\nIf Joe user at work can be gradually migrated to using F/OSS QT/KDE apps on Windows which are also available on Linux and still get the job done, then the OS can be eventually switched over to Linux too with minimal disruption and retraining cost."
    author: "Colin"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "My parrents use their computer for reading e-mail, paying bills via the web and write documents (Word for now). I installed Firefox and Thunderbird to minimize the risk of viruses and everything was OK. Then later I thought why not even install linux and KDE. Now my favorite browser is Konqueror and my favorite mail reader is KMail, so naturally that was what i planed to install, but they where already used to Firefox and Thunderbird and wanted the same applications under Linux. \nIf KMail and Konqueror would have been available under Windows they would have been accustomed to them in stead of Firefox and Thunderbird. This is a WIN for KDE and Linux. Exposure to FOSS makes people AWARE of the alternatives and start to understand that the Computer doesn't have to equal Windows.\n\n K\u00e5re"
    author: "K\u00e5re"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "No.\n\nWhy do e.g. mozilla, eclipse and OOo big momentum ?\nBecause they run on both windows and linux. That's just not the case qith Qt/KDE apps (since there's no free Qt version available).\nHaving Qt GPL on windows might gain us (KDE) a lot of mindshare and developers.\n\nQt GPL on windows is just great ! I never thought this would happen. Trolls, you rule !\n\nNow how about QtCore LGPL on Linux/Mac/UNIX ?\nIMO this would be easier step than the release of Qt under the GPL on windows.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "> Why do e.g. mozilla, eclipse and OOo big momentum ?\n\nNoone questions that Firefox and the rest are successful on the Windows and Linux platform at the moment.\n\nWhat I question is that this success will move people away from Windows."
    author: "ac"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Don't know you, but I'm not \"fighting\" for a world without Windows, I'm \"fighting\" for a world where one could choose in TOTAL freedom the programs and operating system to use, without being frustrated by closed standards or things like this. And as the world is going, the standards are dictated by the applications, and be the applications spreading. For example, the succes of Firefox will be probably make the web a more standard world. And that's good."
    author: "Davide Ferrari"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Like it or not, the success of software GPL'ed software on the desktop was a total failure until their was a GPL'ed OS.  If Linux dies on the desktop because of KDE's success, we will loose our freedom.  When KDE ends up on Windows and developers discover the power of KDE do you honestly think they will care if their new KDE apps are able to run, work, or even compile on Linux?  This same thing already happens on firefox, where there are tons of plug-ins that work on Windows but no other OSes.  Programs that use Windows closed API's, closed standards because they are the de-facto standard. So what happens when KDE on Windows is actually better than KDE on Linux?  \n\nThose who are not willing to fight for their freedom will eventually loose it.  And an FOSS desktop OS is a huge part of that equation.  Porting KDE to Windows only contributes to the destruction of our freedom.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "In my eyes the best argument in this thread. All those windoze-devs who never bothered to have a look at linux/kde theirself. Eventually they will adopt kde-apps they like, enhance them even - but as windozw-kde-apps, using not only the KDE/Qt-API but native windoze-calls as well (why shouldn't they, they know 'em best, they used it for years). Worst of all, we all are lazy, so, why bother porting back to linux, it works for me (them) *sigh*\n\nin short: kde/win32 is evil (on the long run)\n\n    Daniel"
    author: "Daniel Franke"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Well, first of all KDE is not a simple application, KDE is a fundamental part of a modern operating system as it could be a GUI. I think that whole KDE will never be ported to win. Look, GTK has been GPL'ed and even LGPL'd from the very beginnings, do we have a Gnome over Windows? No, we have the possibility to run a bunch of GTK+ apps on win32, and that's good cause the users are normally addicted to applications, not operating systems or desktop environments. Moreover, as KDE devolopers stated, Windows is not a 100% POSIX system and KDE needs a POSIX system to work.\nAnd about incompatible changes, you may get surprised but *there* are open source application natively thought and built on win32 and not even ported to Linux or other OSes...so what? The problem here is simple: if a developer wants to create a portable and opensource application, he will do. If he doesn't, he won't.\n\n"
    author: "Davide Ferrari"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "> What I question is that this success will move people away from Windows\n\nI know at least 10 people, including me, that switched to Linux after using OpenOffice and Mozilla for a while.\n\nSo...\n\"Get the facts...\" ;-)\n"
    author: "ste"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "And I know a dozen people who have choosen NOT to use Linux because they can run OpenOffice and Mozilla without leaving their precious Halo 2.\n\nI know the facts, and this announcement scares me.\n\nbrockers"
    author: "brockers"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: ">Now how about QtCore LGPL on Linux/Mac/UNIX ?\n\ni think that would be a bad idea, two reasons:\n1. Trottech would lose the whole GNU/BSD/Mac/Unix market\n2. it wouldn't be an advantage for us, it would be only an advantage for people who want to deny us our freedom. But if they deny people freedom, they can at least pay some money to save the development of our and their basis."
    author: "pinky"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-07
    body: "Just QtCore, not QtGUI.\nQtCore provides: threading, unicode, file access, event loop, containers\n\nYou can get the same, also portable:\nthreading: pthreads (http://sources.redhat.com/pthreads-win32/announcement.html) or a custom wrapper\ncontainers: STL\nfile access: fopen() and friends, file streams from STL\nunicode: isn't there a wide character class in STL ?\nevent loop: not too hard to write a custom one\nSo I think not too many customers buy Qt just to use these features. If they just need these features they can find low-cost solutions.\n\nBut there simply isn't any alternative to QtGUI, so I think the paying customers pay for the portable GUI and this wouldn't change.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "ohnoes! Trolltech won't be able to make money under the LGPL. You care about a commerial company that much?"
    author: "ricky"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: ">You care about a commerial company that much?\n\nI care about commercial companies if they develop Free Software, if they develop proprietary sftware i don't care about them that much.\n\nBut what would happen if Trolltech would lose their business? Than the KDE Team would have to care about KDE and the Toolkit.\nAnd i don't know if we would find enough people in the community wo would be able and interested to develop Qt on all platforms."
    author: "pinky"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "The original author didn't want this to turn into a big political debate, but it has, so I'll chip in as well.\n\nMy point is short and simple; it's that there is no way in FLOSS you can control development, so don't bother. If some guy wants to port KDE to windows, then let him, if not then there's not much to say.\n\nIt's not our place to try and force everyone on this planet to switch our favorite OS. \n\nWe're just writing cool software. Lets let the developers decide what they want to write and let the users decide what they want to do with the fruits of our labour. Isn't that what Open Source is all about anyway: choice?\n"
    author: "Philip Scott"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "The original author didn't want this to turn into a big political debate, but it has, so I'll chip in as well.\n\nMy point is short and simple; it's that there is no way in FLOSS you can control development, so don't bother. If some guy wants to port KDE to windows, then let him, if not then not.\n\nIt's not our place to try and switch everyone on this planet to our favourite OS.\n\nWe're just writing cool software. Lets let the users decide what they want to do with it.\n\n"
    author: "Philip Scott"
  - subject: "Re: Is a kde-shell possible?"
    date: 2005-02-08
    body: "> Just say NO to FOSS on Win32.\nI have to agree with this. Even if the KDE was ported on Win32, I certain Microsoft will try to stop it through breaking API and/or creating FUD about KDE.\n\n>Make them suffer, because that's the only way they will be willing to switch.\nYep. Let all the windows users suffer then unfold their blindfold and show them GNU/Linux and *BSD. This actually works, I have converted some Windows Users to GNU/Linux with KDE.\n\n\n\n"
    author: "Anonymous Specutator"
  - subject: "You are all missing a big point!"
    date: 2005-02-08
    body: "GPLED Qt on Windows->GPLED software on Windows->GPLED software on Linux and Unix and Windows due to Qt's cross platform nature and open source->mindshare and growing acceptance of Qt and OSS-> more skilled developers usinc OSS technology-> some will contribute to KDE and other projects-Trolltech becomes richer as a result of more interest in their product->Qt becomes better.\n\nTherefore, this is a very goood thing and at the very least it won't make Linux a less attractive platform."
    author: "Matt"
  - subject: "Does this mean...."
    date: 2005-02-07
    body: "Does this mean it will be available under the QPL and the GPL just like the Linux version? I would really hope I could still choose the QPL, but heck, I'll take the GPL at the very least!"
    author: "Questioner"
  - subject: "Re: Does this mean...."
    date: 2005-02-07
    body: "This is my concern as well. Trolltech is saying in their FAQ that they are considering the triple license as well. But in the meantime, I really don't think it makes a difference. The FSF has said consistantly that linking to a library with *multiple* implementations is not derivation. This is because you are now coding to an API instead of a specific library. This is what allows non-GPL-but-still-FOSS software to be ported to Qt/Mac. Now that the commercial-only barrier of Qt/Win is gone, it's available to any software with an Open Source license."
    author: "Brandybuck"
  - subject: "Notes"
    date: 2005-02-07
    body: "Ah, and let's don't forget what TT says:\n- \"We want to encourage our users to use and port to Qt 4. We have no plans of releasing Qt 3 on Windows under the GPL license.\"\n- \"The C++ compilers from Microsoft, Intel and Borland are not supported by the tools in the GPL version.\" \n\n(http://www.trolltech.com/developer/faqs/duallicense.html)\n\nKDE is not ported to Qt4 yet. I dont know if end users (not testers/players) may want (or even can) see KDE4-stable this year. Anyway, qt3/win32 project (http://kde-cygwin.sourceforge.net/qt3-win32/) is still reasonable and important.\n\n--\nregards / pozdrawiam,\n Jaroslaw Staniek / OpenOffice Polska / Kexi Team\n KDElibs/Windows: http://wiki.kde.org/tiki-index.php?page=KDElibs+for+win32\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: Notes"
    date: 2005-02-07
    body: "<i>\"Anyway, qt3/win32 project is still reasonable and important.\"</i>\n\nNo, it really isn't."
    author: "manyoso"
  - subject: "Re: Notes"
    date: 2005-02-07
    body: "\n> No, it really isn't.\n\nWow. Could you please elaborate?"
    author: "MacBerry"
  - subject: "Re: Notes"
    date: 2005-02-07
    body: "Why spend work on risking splitting the developer comunity in two parts, one following a then either unsupported or forked platform?"
    author: "ac"
  - subject: "Re: Notes"
    date: 2005-02-07
    body: "This is about Qt3, not Qt4.\n\nQt3/Free/Win32 is still needed for any Qt/Free software built on the Qt3 API.\n\nSome software might never get ported to the Qt 4 API"
    author: "Kevin Krammer"
  - subject: "Re: Notes"
    date: 2005-02-08
    body: "This is exactly what I meant, keeping one part of the developer community in the then stoneage of Qt3 while everyone else moved forward to Qt4. And someone is seriously still putting work into creating such a situation? I don't see what good about that (except that of course everyone is free to do whatever he wants)."
    author: "ac"
  - subject: "Right"
    date: 2005-02-08
    body: "qt3/win32 has been rendered irrelevant with this announcement. qt3/win32 will only hold back Qt 4 GPL (official) and it will only split development communities. It would have been useful had Trolltech not made thin breaking announcement.\n\nBut now, I think it is pointless to work on it isntead of KDE or more improtant things. We don't need it anymore! Sorry, it's the truth!"
    author: "Matt"
  - subject: "Re: Right"
    date: 2005-02-08
    body: "Looks like your reasoning is based on your own needs. Do you think that KDE corporate users will switch to newer (stable or unstable) KDE versions everytime as home \"hobbyists\" often do? You can say \"Shut up, I don't care!\". And that's good FOR YOU. But lots of users DO care. Companies like SUSE or Lycoris rely on stable releases because, want to provide support for a particular desktop version: the support what is usually longer than, say, 6 months. \n\nA user who purchased eg. SUSE commercial support last year won't be happy to give up and switch to, say, developemnt version on Debian or Slackware because someting so irrelevant for him like newer development libraries are \"so cool\". These are cool for me (and for you, IF you're developer, I don't know), but for users it's cool if they can get their daily work done. It's cool enough that they have chosen Linux as desktop, ... do you want to force them to switch back to win32 by telling them \"Guys you're not at the bleeding edge!\"?\n\nSo better say \"it's my opinion\" and not \"it's the truth\", because it's false for many developers out there. The time for Qt4 and KDE4 will come, but transition needs to be performed smoothly. That's the advantage of GPLed Qt3/win32.\n"
    author: "Jarslaw Staniek"
  - subject: "great!"
    date: 2005-02-07
    body: "A great day for Trolltech and the whole Free Software Community.\nNot long ago some people told me on this place, that Trolltech would never do that because they would lose to much customers.\nIt seems like Trolltech has much more customers who develope software for their customers than everyone thought and they don't earn all their money by in-house development.\n\nI think this will give Qt and Trolltech an great boom on windows.\nNow everyone can develop Free Software for all platform and also young people can learn programing with Qt and they will bring their favorite toolkit to business when they became grown-up.\n\nSimply great!"
    author: "pinky"
  - subject: "Re: great!"
    date: 2005-02-08
    body: "TrollTech was forced to make this move; I am sure they did not want to.  However, if they waited any longer, the QT port being done by the Cygwin guys would have been finished, and there would be GPL QT/Windows anyway; except that it would not be TrollTech's version of QT/Windows.  \n\nTrollTech made a smart move here by realizing that this would be bad for QT.  The two versions of QT/Windows would have different bugs and incompatibilities, and TrollTech would have no control over the Cygwin QT code.  Now everyone can use the same QT/Windows.  \n\nUnfortunately, TrollTech may lose a lot of sales to companies doing in-house projects.  We can only hope that this move causes a surge of interest in QT, even from companies doing commercial software development.  Otherwise TrollTech could be toast."
    author: "Spy Hunter"
  - subject: "Re: great!"
    date: 2005-02-08
    body: "I don't believe that in-house projects will choose the GPL version. This would mean explaining your PHB that you never ever could give away / sell the software without also giving the source.\n\nDou you think any sane (ha!) PHB would go for that? QT Licenses are cheap in the commercial setting, they're really only a fraction of the total development costs.\n"
    author: "Bausi"
  - subject: "Re: great!"
    date: 2005-02-08
    body: "There's another reason why in-house teams will want the commerical version: the commerical database drivers that ship with it. \n\nMost businesses choose big expensive databases, even if they go with free development tools, because they're terrified a F/OSS database would corrupt their data over time. For them, Qt Commercial is the only viable version."
    author: "Bryan Feeney"
  - subject: "Re: great!"
    date: 2005-02-09
    body: "Not true, a GPL in-house project can be relicensed and then compiled with the QT commercial edition and sold, as long as the company holds the copyright on the code (which they always do).  You're never locked into the GPL if you hold the copyright."
    author: "Spy Hunter"
  - subject: "Congratulations TrollTech..."
    date: 2005-02-07
    body: "I want to congratulate TrollTech for this smart move, this will allow not only to have more Qt based applications on windows it will make the Linux market share grow.\n\nThis move will bring benefits to all, OpenSource Community, Linux, Trolltech, everyone.\n\n"
    author: "U"
  - subject: "SUPPORING KDE ON WINDOWS?"
    date: 2005-02-07
    body: "Why would we support KDE on windoze, i think a lot of support is so called 'community support'.\n\nThen you will see this a lot: do you run on windows? Yes? G'bye!\n\nOr am i the only one fearing/hoping/expecting that windows users will be 2nd class users in the community?\n\n\n\n\n"
    author: "cies"
  - subject: "Re: SUPPORING KDE ON WINDOWS?"
    date: 2005-02-07
    body: "i don't see, that the whoe kde will be ported to windows.\nI think it will be 'only' some key applications.\nSo i don't think many people will come from windows into the kde-community. If they like programing they may come in the Qt newsgroups or forums, but there already windows and macOS people and there is no problem."
    author: "pinky"
  - subject: "Re: SUPPORING KDE ON WINDOWS?"
    date: 2005-02-07
    body: "I don't know how well you know the KDE architecture. What needs heavy engineering to be ported is Libs. The applications depend on Libs, not on the underlying system except for things like kernel maintenance apps that are not even useful in Windows.\n\nIf done well, the applications that already compile for Linux/BSD/etc. should compile without changes with the Windows kdelibs headers. Maintenance will be done just like it's always done."
    author: "Cloaked Penguin"
  - subject: "Re: SUPPORING KDE ON WINDOWS?"
    date: 2005-02-08
    body: "The scope of KDE on UNIX isn't just in the libs.\n\nFor example, in kdebase we have kicker and kwin (as examples) which rely-on/exist-due-to X, as well as kdm, kdesktop, etc.\n\nPrograms dock into kicker for system tray, etc. but would have to be reworked to dock into explorer's systray.  Or kicker would have to be rewritten.\n\nKcontrol modules would have to be rewritten or dropped due to redundancy with the windows control panel.  Screensavers would likely be entirely dropped.  Session management would go away, since it uses feature of X, or reimplemented to be compatible with windows.\n\nFile dialogs, file property dialogs, etc. would all need to be rewritten or modified (this is kdelibs, I know)... Even tooltips in konq, which display the unix file permissions would have to be redone.\n\nSupport for Konqueror plugins for flash and related would have to rewritten to use the windows equivalents.\n\nArts would have to play to DirectSound - kmix would be redundant.  \n\n--\n\nWhat I would look forward to are KHTML based windows browsers (not konq port) which will help the greater cause of website standards and interoperability.  If a bunch of people are using KHTML, more website are likely to work in KHTML. (and gecko for that matter).\n\nAlso, I look towards the possibility of Qt+OOo becoming a staple (might be a wet dream though).\n\n--\n\nthat bored? read my boring blog at http://tblog.ath.cx/troy"
    author: "Troy Unrau"
  - subject: "that this mean..."
    date: 2005-02-07
    body: "that we'll get w32 qtdesigner gpl too? and is there a gpl qt IDE (other than kdevelop soon :)) that will be release with it?"
    author: "Pat"
  - subject: "Finally free QT on win32!"
    date: 2005-02-07
    body: "And please stop the complaints about FOSS on win32. If you don't like it, just don't work for it. But don't pretend people have to argue with you before porting their app to win32."
    author: "Flavio"
  - subject: "Re: Finally free QT on win32!"
    date: 2005-02-08
    body: "But there are certain demented individuals that believe they can tell people what to work on - especially volunteer projects.  "
    author: "Plisken"
  - subject: "why did trolltech do this?"
    date: 2005-02-07
    body: "For years Matthias and the trolls have been telling us they have no plans whatsoever to release Qt/Win under the GPL.  That the last time they made a free Qt/Win available their sales dropped dramatically.  That it would be a cold day in hell before they released Qt/Win GPL.\n\nNow all of a sudden they decide to release Qt/Win under the GPL?  Why?  What's the new reason?\n\nNot that I'm complaining...  I'm very happy. :)"
    author: "ac"
  - subject: "Re: why did trolltech do this?"
    date: 2005-02-07
    body: "It's in the FAQ \n\n\n> Why have you not done this before now?\n\t\t\n\nWe have been exploring ways to offer the Open Source community the opportunity to use the Windows version of Qt for some time. Until now, we have not done so because we had concerns about the viability of the Open Source model on Windows.\n\nThere are several reasons why we do it now:\n\t* Understanding of the Open Source development model and both the benefits and obligations of using dual licensed software have been on the rise in the Windows community.\n\t* We want to make our licensing and messaging simpler and more consistent\n\t* We want to give the Windows Open Source development community the opportunity to use what we believe is the best application framework of its kind."
    author: "jmfayard"
  - subject: "Re: why did trolltech do this?"
    date: 2005-02-07
    body: "Exactly, and the more F/OSS developers out there that have experience of developing with QT (GPL) on Windows the more developers that may suggest to their management that they use the commercial QT for their software.\n\nPersonally, I develop apps for internal process improvements and am restricted to VB and VC++ with MFC/ATL simply due to the environment we run and current dev licenses we have. I would love to be able to ditch the shoddy MFC/ATL framework and VB in favour of C++ with the much more sane QT framework, even using more customisable IDE's such as Kdevelop and all at no cost (my software doesn't get distributed externally or sold)."
    author: "Colin"
  - subject: "Why in the world would you do this?"
    date: 2005-02-08
    body: "I like QT, but there are much more productive environments to develop in. Python, .NET/Mono, etc. Developing apps in C++ is just insane these days. I'm not suggesting KDE should be rewritten in C#, but there's just no reason to use C/C++ for most apps these days. "
    author: "Some guy"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-08
    body: "Python, Ruby, yes.\nAnd there is PyQt und RubyQt, but until now they didn't exist under windows.\nHopefully this will change now :-)\n\nAlex\nwho currently uses RubyTk for portability reasons :-/"
    author: "Alex"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-08
    body: "Well, PyQt does exist under Windows:\n\nhttp://www.riverbankcomputing.co.uk/pyqt/faq.php\n\nIt's just not licensed under the GPL on that platform."
    author: "Anonymous Custard"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-08
    body: "The FAQ may be out of date. But I'm pretty sure that you can use GPL PyQt on Windows. The reason why there is tradionally no GPL PyQt on windows is simply because there was been no GPL Qt on windows for it to work with.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-08
    body: "And the GPL version works fine with the GPL Qt-X11 port, so yes you have GPL PyQt on windows already."
    author: "Morty"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-09
    body: "\"with the GPL Qt-X11 port\"\n\nAnd adds another useless layer on Windows. The GPLed QT-X11 port for Windows is out of date as well. Luckily, for QT4 this is solved which is awesome and those people who did the porting aren't doing that useless double-work anymore (who wants to run X11 native on Windows unless compatibility reasons which are forced upon ones throat?). Kudos to Trolltech for this move!"
    author: "Anonymous"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-08
    body: "To be honest, I find QT to take roughly the same time to program medium apps (complex point of sale + subscription + stock tracking + web site sync, my current project) as C#. Both are surprisingly quick to use and provide usable output within a short space of time.\n\nHere's my list:\n\nFastest: \nC++/QT <-> C#\n\nVB6\nJava 1.4\nC++/GTK+\n\nWith Java 1.5, I expect the implicit casting to make life easier and probably pop it up abive VB6, but ultimately, the gap between VB6 and the two top runners is huge for me, in terms of coding time. I doubt it can bridge that gap, but at least it'll get half way there.\n\nWhether to choose QT or C# is a hard question that depends on the platform, required execution speed and what you want to do.\n\nFor example, writing an Image Processor, I'd use QT. C#'s VB legacy thinking makes image manipulation tricky. It can be done, but you have to jump through multiple class hoops first. In QT, just load into your QImage, get the pointer to the start of the image memory, read out each 32bits and shift&mask for each channel. QT's still better for serious and non-GUI stuff, and both are very similar for generalised GUI applications."
    author: "Luke Chatburn"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-02-08
    body: "What about Python and Ruby, they are both much faster to develop in than both Java and C#? When you combine the Qt/KDE apis with Qt Designer support and a modern IDE with one of these language you have a very nice RAD environment. For example, Eric the Python IDE or the next release of KDevelop with the QtRuby/Korundum bindings.\n\nC# is a systems programming language for implementing cross-language components, it isn't a RAD language. Java is also a systems programming language descended from Algol, but with features removed to make it simpler for 'everyday' programmers. I personally found the learning curve for C# pretty steep, and was surprised how many more features it has than java. The .NET class libraries are enormous and very complex to master.\n\nWhen you talk about manipulating a QImage using shift and mask, that is systems programming. I doubt very much that most VB programmers would even know what a shift and a mask are. And that sort of knowledge shouldn't be needed to write a custom business app, only if you want to write the new PhotoShop or something.\n"
    author: "Richard Dale"
  - subject: "Re: Why in the world would you do this?"
    date: 2005-10-14
    body: "I like Python, but its only faster than Java / C# for small projects - and even then not by much. The moment you get into even the medium scale, debugging becomes a big time-sink, and the more 'serious' languages win out. Static typing changes from a chore to a powerful aid.\n\nThe key thing is the IDEs. Python kicks Java's arse for development time - if you're using a text-editor to write your Java. But Java has Eclipse, which is a simply magical IDE that leverages all of Java's reflection abilities to the full. \nIt completes names, writes out class outlines, method stubs and other Java verboseness for you (and with a natural and easy-to-use interface). It detects simple bugs (and 'lint' - unused or suspect code), and suggests corrections. It has a wonderful full-featured debugger that lets you inspect and edit running code. Python debugging is pretty good, but this is way better. It knows the API (with full documentation) for all the classes (Python's doc strings are nice, but I still have to go to the manual lots. JavaDoc _is_ the manual). More sophisticated features allow you to restructure your class design whenever you feel like it (e.g. change a method's name, signature or what class its in - and have all your code still work).\n\nIn short, I recommend it.\n\nAnd C# has Visual Studio, which is almost as powerful as Eclipse though rather expensive and not as user friendly.\n\nNB: I'd say that Java 1.5 has made the difference between Java and C# minimal. You could almost convert back and forth with reg-exs. Also, there's a Java compiler that targets Mono, so you can even mix Java and C#.\n\nSo I'd say use Python for page-long scripts that you don't plan to extend too far, and Java for anything else. \n - Dan\n"
    author: "Dan"
  - subject: "Re: why did trolltech do this?"
    date: 2005-02-08
    body: "Repost from Slashdot http://it.slashdot.org/comments.pl?sid=138577&cid=11597641\n\n Re:Kindows???? (Score:5, Interesting)\nby Bulln-Bulln (659072) on Monday February 07, @11:58AM (#11597641)\nQuote from KDE-Cygwin:\n\nPosted By: habacker\nDate: 2005-01-27 14:21\nSummary: source and binary snapshots of QT/Win Free Edition available\n\nThe QT/Win Free Edition is not far away from to be a full working release.\n\nMaybe this is why Trolltech made this announcement? Trolltech propably had its reasons not to release the Windows version under GPL, but with this fork their reasons may be undermined. So maybe the guys at Trolltech thought \"better done right (by us), than done buggy (by others) and give us bad reputation\".\n\nOf course this is just speculation and the close time gap between the KDE-Cygwin announcement and the Trolltech announcement could be just a coincidence."
    author: "Anonymous Coward"
  - subject: "maybe this is a presage of the end of the world?"
    date: 2005-02-07
    body: "maybe tomorrow will start to rain frogs?\nmaybe bill gates will release windows GPL'd?\nmaybe sco will become a non-profit organization?\n\nbut, by the way, this news make me a very happy child :-)"
    author: "mart"
  - subject: "Re: maybe this is a presage of the end of the worl"
    date: 2005-02-07
    body: "Accounting gymnastics aside, SCO have pretty much been approaching non-profit status for some time now, at least according to Yahoo! Finance's data."
    author: "The Badger"
  - subject: "Qt as Free as GTK?"
    date: 2005-02-07
    body: "Does this mean that the GPL'd Qt is now as Free as GTK+?  Or are there still other platforms that Qt is not Free under, like embedded?"
    author: "Jel"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "QT it is not as free GTK. QT is licensed under the GPL, GTK is under the LGPL."
    author: "smoerk"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "According to the FSF, the GPL is more \"free\" than the LGPL, and is their preferred license for libraries.\n\nhttp://www.fsf.org/licenses/why-not-lgpl.html\n\nNot that I really care.  They're both fine licenses as far as I'm concerned.  But for people who do care, that's where the FSF stands on the matter.  Quite clearly, they prefer Qt's way of licensing things."
    author: "ac"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "You're incorrect.  The page you're referring to doesn't say that the GPL is more \"free\" than the LGPL.  What's relevant to this discussion is the following paragraph:\n\n\"Using the ordinary GPL is not advantageous for every library. There are reasons that can make it better to use the Library GPL in certain cases. The most common case is when a free library's features are readily available for proprietary software through other alternative libraries. In that case, the library cannot give free software any particular advantage, so it is better to use the Library GPL for that library.\"\n\nAs for UI toolkits, there's a plethora of \"alternative libraries\" and the LGPL is therefore the preferrable license.  The FSF aren't the GPL-zealots they're sometimes made out to be, and they even backed up Xiph on the license change of the Ogg/Vorbis implementation from LGPL to BSD-style in order to \"boost\" the codec."
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "Freedom? So having not the freedom to link to a library (GPL) is more free than having the freedom to link to a library LGPL) -- both when not having the LGPL. I say no, unless you want all software being licensed under the GPL (there goes license diversity!).\n\n-1 troll because of (pick one)\nA) KDE zealot (misquoted disinformation)\nB) RMS propaganda (GPL zealotism)\n\nI picked A!"
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "That's the freedom of the potential abuser you are talking about. If you want to talk about keeping the code and everything which makes use of that code free then GPL is indeed more free than LGPL. Many developers have the latter in mind when wanting to ensure that their contributions stay free."
    author: "ac"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "> So having not the freedom to link to a library (GPL) is more free than having the freedom to link to a library LGPL(?)\n\nWhose freedom? For users GPL promotes a greater freedom because it insures that whatever is distributed will remain free for them. For users you would think that would be important. For companies an argument can be made for linking to LGPL, but it's not a very strong one. \n\n1) No more than a few percent of the total cost of developing a software application is the cost of licensing a toolkit. All one has to do is have the neurons to add up the growth of Trolltech to see that they have a lot of customers who understand this basic math... even if those people who have nothing left to harp about but how Trolltech refuses to give away it's core business for corporate welfare don't.\n\n2) Both KDE and GNOME have LGPL'd core libraries. Show me the piles of commercial shrink wrap software being distributed based on them? Show me the Linux users waiting in droves outside their computer store to shell out for these imaginary programs? I see a little of Qt based stuff and a some cross platform other toolkit programs. In fact aside from the proprietary model losing viability outside of contract work and niche markets like we all talk about as the success of OSS there just isn't a substantial market potential compared to Windows, unless you go cross platform.\n\n3) Given that you can dual license as well as build a service business, as many in the Linux community have, it's a silly argument to whine about not having corporate welfare. I mean Red Hat sells GPL software with a per seat license, they get a good price and they do it with a service business. \n\n> A) KDE zealot (misquoted disinformation)\n\nA GNOME troll I presume? Whatever happened to the good old days when Trolltech was attacked for not having a GPL'd Qt? The one consistent factor between the \"it should be GPL\" and \"It shouldn't be GPL\" attacks are the attacks. If Trolltech were to tell their customers not to give them any more money because they weren't charging, then go out of business, then quit maintaining Qt... then would the critics be happy? Probably not, because then Qt would go to a BSD license, somebody would maintain it and they would complain it should be GPL. After someone decides they hate someone or something then everything else is just lame rationalization, especially when it doesn't hold up to the light of cold hard reason.\n\nWhy would somebody take the \"Qt GPL sucks\" position? Either they don't understand reality or they want to prevail with their ventures and wish Qt would go away. Here's a thought... Don't use Qt and then don't complain about it."
    author: "Eric Laffoon"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "Qt is freeer than GTK+ and has been for years, claiming it isn't is pure FUD."
    author: "mikeyd"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "please explain..."
    author: "smoerk"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "http://www.fsf.org/licenses/why-not-lgpl.html\n\nHope that helps.  FYI--I don't necessarily agree with the FSF in a lot of cases, but this serves to explain why THEY (and presumably, the person you're asking) think the GPL is \"more free\" than the LGPL."
    author: "ac"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "Only if you have a demented definition of what the word free means - as in the Stallman definition."
    author: "Plisken"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: ">Only if you have a demented definition of what the word free means - as in the Stallman definition.\n\nDo you would say this is a demented definition:\n- The freedom to run the program, for any purpose\n- The freedom to study how the program works, and adapt it to your needs\n- The freedom to redistribute copies so you can help your neighbor\n- The freedom to improve the program, and release your improvements to the public\n\n???\n\nI think this is a really clear definition and it would be hard to argue that this definition don't define freedom for computer programs."
    author: "pinky"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "Users don't have rights.  Only developers do.  Stallman doesn't care about programmer rights.  LGPL is more free than the GPL for anybody that is rational."
    author: "Plisken"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "> Users don't have rights.\n\nThat's exactly the point why LGPL (Lesser GPL) is less free than the GPL, according to Stallman. Because users loose rights, simply because any program that wants to use that library will have to be free.\nThat's also why Stallman finds GPL more free than BSD.\n\nIn terms of the programmer, yes, they loose freedom."
    author: "blacksheep"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "\"LGPL is more free than the GPL for anybody that is rational.\"\n\nUmm, so anyone who disagrees with you is 'irrational'? \n\nThere are all sorts of problems with the LGPL, such as defining what 'linking against' means. For instance, if I have a proprietary java .jar file that I use in my java app, can it be said that the app 'links against the .jar file'? Maybe it does, maybe it doesn't. If you want to write a commercial app, you should get a commercial license, the LGPL isn't suitable with these vague terms and potential legal problems."
    author: "Richard Dale"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: ">Users don't have rights. Only developers do. Stallman doesn't care about programmer rights.\n\nStallman cares about progremmers right and users right!\nThis is essential in every free society. You are free to do what you want, as long as you don't cut of the rights of other people. Therefor you don't have the right to take the car from your neighbor if you like it or kill a person if you don't like there attitudes. Your right to do what you want ends at that point were the right of others and the whole society begans.\nThat's the basis of a free society and also of copyleft licenses for software. "
    author: "pinky"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "> Users don't have rights. Only developers do.\n\nReally?\n\nThen let's shoot all users for shouting that the developers are not listening to them!"
    author: "ac"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "Well, free software is not a human right, I think we can agree to that.\n\nThen, what rights do users have? I mean, I buy a DVD and I don{ t even have the right to watch it on the UK because of zones. And I paid for it! What right am I supposed to have over software I have paid nothing for?\n\nWhere do those rights come from?"
    author: "ac"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "He started the whole free software movement. Anyone else who is devoted to free software is following his movement, and following him. So I think his definition of what the movement is about should be the right one."
    author: "mikeyd"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "Sorry, but that\u00b4s just propaganda. Free software has existed for much much longer than the GNU project. Hell, go read their manifesto, they say they are taking lots of free software written by others."
    author: "AC"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-08
    body: "You could not be more wrong even if you tried.  GTK+ is released under the LGPL, and the LGPL gives you more freedom than the GPL, just as a BSD-style license is more free than the LGPL (and public domain is more free than BSD)."
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "BSD gives you more freedom to do whatever you want with the code and use it in a project licensed however you want.  However that also means it lets you prevent others from seeing what you do with it.  You can do more with it, but you can also make sure others do less with the results.  You are given the right to restrict others' rights.\n\nGPL forces you to give the same rights to others that you got yourself.  There's less you can do, but it ensures others retain their rights.  GPL is imho only bad for people who want a handout: the ability to take code and give nothing back, and I don't feel sorry that this isn't possible with GPL.  MY rights are preserved, that is freedom."
    author: "MamiyaOtaru"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "True, all of that applies to the BSD-license, but that has very little to do with either the LGPL or the GPL.  Your second paragraph applies equally much to the LGPL as it does to the GPL.\n\nIf I develop a library under the LGPL and proprietary software links against it: good!  That's the reason I chose the LGPL.  MY rights are preserved, and that is freedom!"
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "But the end users have more freedom under the GPL, which is what Free Software is all about. See the link above to FSF position."
    author: "mikeyd"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "No, end users have the same amount of freedom under both the GPL and LGPL since the license only covers distribution.  For developers, the LGPL is more free.\n\nAs for the question of which license is better for free software as a whole?  It all depends on the situation, and the FSF clearly states that in the document to which you are referring.  The FSF doesn't take a stand on which license is more free."
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "No, because the end users of something LGPL may end up using a non-free program. Wheras the end user for a GPL program will always get a GPL one."
    author: "mikeyd"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "Your argument would be valid if the library in question was GPL-only, but since Qt is dual-licensed you might end up running non-free software anyway."
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-10
    body: "In which case, the person using a proprietary QT based application still supports free software as a portion of the money they spent on the software goes to Trolltech, who in turn releases their work under the GPL.  With a proprietary GTK app, none of the funds that the buyer has spent goes back to Owen & company to support their work.  "
    author: "Joe Kowalski"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "Embedded is allready available under GPL. Only, I believe GTK is LGPL, not GPL. That makes it possible to create closed source commercial applications with it without paying anyone. With Qt, you'd have to pay Trolltech (and get a great toolkit in return that saves you a the money spend on a licence easily in developers time...)"
    author: "Andre Somers"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-07
    body: "Qt has been more free than GTK+ for almost 5 years now.  Its only the ignorance of the Gnome community (or plain old FUD) that says otherwise.\n\nbockers"
    author: "brockers"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "Actually, it's simple logic (or \"rational thinking\") that says otherwise.  The LGPL is a more free license than the GPL."
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "It is more free the same way anarchy is more free than democracy. People are more free to screw <i>you</i>.\n\nNo, fewer rules doesn't mean more personal freedom."
    author: "Carewolf"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-09
    body: "That has to be the worst analogy in history.... but alright, turnabout is fair play:\nThe LGPL is more free than the GPL the same way democracy is more free than a dictatorship."
    author: "Anonymous"
  - subject: "Re: Qt as Free as GTK?"
    date: 2005-02-10
    body: "How so?\n- Proprietary means no one may take your stuff.\n- LGPL means anyone can take your stuff.\n- GPL means anyone can take your stuff, if you in return are allowed to take their stuff.\n\nGPL therefore has more rules, but the extra rule is used to guaranty a greater freedom. Therefore the comparison to a modern free-society that has more rules than an anarchy but uses those to guaranty greater freedom for the individuals."
    author: "Carewolf"
  - subject: "Thank you"
    date: 2005-02-07
    body: "\nThank you Trolltech!\n\nThis will give your toolkit total dominance (we will - once available - make it default for all internal projects) and well deserved. \n\nAnd of course, Free Software is going to be able to gain large user populations over. I think KDE on Windows is going to be much better as a native application. \n\nI personally will love to recommend e.g. KimDaba and Kate to Windows users. :-)\n\nYours,\nKay"
    author: "Debian User"
  - subject: "Great!!"
    date: 2005-02-08
    body: "Now this is great news! I hope in time this proves to be a good move for Qt!\nOne question. Would it be possible now for WxWindows to use Qt? Or does it need LGPL?"
    author: "John Qt Freak"
  - subject: "Binary Libraries???"
    date: 2005-02-08
    body: "According to http://www.trolltech.com/developer/faqs/duallicense.html#q18 , \"Commercial customers will have access to a package containing a prebuilt Qt library.\" Does this mean that there won't be a standard Qt dll for Windows????!!!! This seems like it would be a major problem - Windows is a monolithic platform, we can't have all these different people having their own \"builds\" of the Qt library!"
    author: "LuckySandal"
  - subject: "To the whiners"
    date: 2005-02-08
    body: "And who said that the KDE project's goal was Linux advocation?"
    author: "Plisken"
  - subject: "Re: To the whiners"
    date: 2005-02-08
    body: "Heh. You should see what happens when someone commits something for win32 that breaks or even causes problems for the *nix workings.\n\nThere are a couple of developers who keep KDE building on the BSD unixes, there is one developer who maintains the OSX port of KDE. Will anyone actually start porting the KDE apps? KDE as an environment doesn't make any sense since Windows comes with it's own environment. \n\nIt all comes down to someone willing to do the work. I know that KDE will be around for Linux in the years to come. I would deploy it without any fear of it disappearing. But windows ports? Talk to me in two or three years. The same goes for Evolution. \n\nHas any project been successful in attracting win32 developers?\n\nThis is a good move for Trolltech, and will spread their developer mindshare even further.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: To the whiners"
    date: 2005-02-08
    body: "Why type if you won't answer the question?  Or maybe you got very confused and thought you were posting to another thread."
    author: "Plisken"
  - subject: "Re: To the whiners"
    date: 2005-02-08
    body: "There is one developer working on win32 stuff.\n\nThere are more than 200 active contributors.\n\nThe KDE desktop on Windows is a non starter.\n\nKDE is primarily a desktop environment.\n\nI would say that KDE advocates for *nix. (Linux, the BSD's, and some proprietary unixes)\n\nLet me repeat. KDE desktop has no possibilities, no future, no place on Windows. There is already a well entrenched desktop that is probably very difficult if not impossible to replace.\n\nSome KDE applications may find a place on windows, but time will tell. And that will be up to individuals who are moved to do the work to maintain them.\n\nAs I said, this move is good for Trolltech. I suspect it will have very little effect on KDE. If I see two or three dozen windows developers join up and start contributing, then I will be proved wrong.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: To the whiners"
    date: 2005-02-08
    body: "Well said!"
    author: "Matt"
  - subject: "Re: To the whiners"
    date: 2005-02-08
    body: "In reality, the KDE desktop is a non-starter on Linux as long as it relies on the Qt toolkit.  Too late now."
    author: "Plisken"
  - subject: "Re: To the whiners"
    date: 2005-02-08
    body: "That's trolling and BS."
    author: "Matt"
  - subject: "Fantastic. Now what about PyQt?"
    date: 2005-02-08
    body: "If I could develop PyQt apps cross platform under the GPL, that would be awesome. "
    author: "Jon Scobie"
  - subject: "Re: Fantastic. Now what about PyQt?"
    date: 2005-02-08
    body: "I've seen a report on the PyKDE mailing list that someone got the recent WIN32 port of Qt3 (GPL), and PyQt compilied and working on Windows. So it is possible. What is now needed is a neatly compiled and packaged up version Qt3 and PyQt that people can easily install and use. Volunteers? anyone?\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "They fear..."
    date: 2005-02-08
    body: "... .NET on Windows and must do a desperate move!\n\n"
    author: "performa 300"
  - subject: "Re: They fear..."
    date: 2005-02-09
    body: "Ahh, here is the reason for the discussed business decision. Thank you!\nHehe :-)"
    author: "Anonymous"
  - subject: "I don't care about migrating others..."
    date: 2005-02-09
    body: "I didn't read all the comments, but most seem to be about using KDE to get people to migrate... nevermind that most people in the Windows world don't know what KDE is or could even name a KDE app.\n\nI've already migrated though to Linux. And I'm happy using KDE. However, it would be a great boost to KDE developement if it had a larger userbase. Something like amaroK would catch on pretty well in Windows, I imagine. And if the userbase swelled to double, or triple the size it could only mean more developers. Which is what I care about: a better user experience for me. \n\nPeople who know what Linux is about generally like the philosophy of FOSS. That's what made me jump over. What's driving people to Linux is definately not, \"Oh! I hear the new versions of konqueror and koffice are so uber, time to make the switch!\" They already have parallel software: IE/MSoffice, or Firefox/OOo. But, get konq and company running under Windows and you just might attract more developers. As a side effect, you might even improve the user-friendly image of Linux... but that shouldn't be the main aim."
    author: "thomas"
---
<a href="http://www.trolltech.com/">Trolltech</a>, maker of the <a href="http://www.trolltech.com/products/qt/">Qt toolkit</a> which forms the basis for KDE,
<a href="http://www.trolltech.com/newsroom/announcements/00000192.html">
announced today</a> that the Qt version for Microsoft Windows will be available under the GPL in addition to its current commercial license offerings for that platform. This change will take place with the release of Qt 4.  The Qt version for Linux has been available under a similar dual licensing scheme for several years already. The availability of a GPL'ed Qt for Microsoft Windows will make it much easier to distribute KDE applications that run on the Microsoft Windows platform.


<!--break-->
