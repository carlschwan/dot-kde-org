---
title: "KOffice 1.4 Beta 1 Released"
date:    2005-04-30
authors:
  - "tzander"
slug:    koffice-14-beta-1-released
comments:
  - subject: "Congratulations!"
    date: 2005-04-30
    body: "Congratulations to all KOffice developers! And also thank you very much for your great works, I really love this office suite."
    author: "fyanardi"
  - subject: "Re: Congratulations!"
    date: 2005-04-30
    body: "Me too! :-)\nI really appreciate your work. Thanks, guys!"
    author: "Dario Massarin"
  - subject: "Re: Congratulations!"
    date: 2005-05-01
    body: "You misspelled Kongratulations :-D"
    author: "Corbin"
  - subject: "Re: Congratulations!"
    date: 2005-05-02
    body: "... and You misspelled Your name, Korbin ;-]"
    author: "taki"
  - subject: "Well done"
    date: 2005-04-30
    body: "IMHO, koffice is more important that openoffice, and I look forward to seeing 1.4 with hightend anticipation."
    author: "Colin JN Breame"
  - subject: "Re: Well done"
    date: 2005-04-30
    body: "Both projects are wonderful. Great efforts."
    author: "Andr\u00e9"
  - subject: "Re: Well done"
    date: 2005-05-01
    body: "http://www.kde-docs.org\n\nGood document templates are perhaps even more important and the entrance barrier is VERY low."
    author: "gerd"
  - subject: "Kubuntu packages"
    date: 2005-04-30
    body: "Kubuntu packages are available at using this deb source.\n\ndeb ftp://download.kde.org/pub/kde/unstable/koffice-1.4-beta1/kubuntu ./\n"
    author: "Jonathan Riddell"
  - subject: "Re: Kubuntu packages"
    date: 2005-04-30
    body: "Wow - nice one Jonathan. I'll head on over there."
    author: "David"
  - subject: "Thanks"
    date: 2005-04-30
    body: "Many, many, many, many thanks to everyone involved in KOffice: David, Inge, Boudewijn, Jaroslaw, Laurent, Ariya, Adrian, Thorsten, Nicolas, Luk\u00e1\u009a, Brad, Melchior, Sebastian, Michael, Raphael, Dag, Tomas, Casper, John... and everyone I forgot :)\nYou guys rock!\nKOffice is a little wonderful gem, too bad it doesn't get the hype it deserves... but I'm sure that the future looks bright. Go KOffice!"
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2005-04-30
    body: "And me!  Always forgetting me!\n\nOh wait, I haven't done anything yet.\n\nMaybe next time\n"
    author: "JohnFlux"
  - subject: "Fedora packages"
    date: 2005-04-30
    body: "Are there any Fedora packages?"
    author: "Kennet"
  - subject: "Re: Fedora packages"
    date: 2005-05-03
    body: "The kde-redhat project (http://kde-redhat.sf.net) has koffice-1.3.91 packages (in the unstable repo)."
    author: "Rex Dieter"
  - subject: "Re: Fedora packages"
    date: 2005-05-03
    body: "Thanks!"
    author: "Kennet"
  - subject: "OASIS"
    date: 2005-04-30
    body: "I have here an OASIS OOo-ott-file (template file) that I can't open in kword-cvs. Should this work?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: OASIS"
    date: 2005-05-01
    body: "Good point.\nThis makes it work:\n\n[Ed: see attachment]\n\n(i.e. adding application/vnd.oasis.opendocument.text-template to both X-KDE-ExtraNativeMimeTypes (comma-separated) and MimeType (semicolon-separated)). \nIt works as in: kword opens the .ott file.\nBeing able to really use an installed ott template when doing \"New\" is another matter though.\n\n\n\n\n"
    author: "David Faure"
  - subject: "Re: OASIS"
    date: 2005-05-01
    body: "works :-)\nDoes not look exactly like in OOo but the content is there.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: OASIS"
    date: 2005-05-02
    body: "If you can share the file, I'd be happy to look into why it looks different from OOo."
    author: "David Faure"
  - subject: "Re: OASIS"
    date: 2005-05-02
    body: "Please look in your INBOX :-)\nBTW, I don't know, if OOo generates valid files. It was only a problem which appears here. \n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Killer Release"
    date: 2005-05-01
    body: "Allright, I might be wrong. But I have a strong feeling\nthat this will be the a cornerstone release for KOffice. \n\nWhy ? Because of the switch to Oasis. Now KOffice will be\nable to use the OpenOffice filters to import/export M$ documents\nI would suppose. And this is it. OO.org is nice and everything,\nbut the user interface is much nicer in KOffice. \n\nThe great advantage of OO has been that \"it just works.\" Meaning:\nsomeone sends you a M$ document, and most likely you OO suite will\nopen and display it almost as intended by the monopoly. This is\nimportant in the real world. \n\nI still recommend OO to almost everyone. But this may change soon.\n:-)\n\nKudos for the great work, and thank you so much!"
    author: "MandrakeUser"
  - subject: "Re: Killer Release"
    date: 2005-05-01
    body: "As I understand it, the OO filters are unfortunately highly intertwined with the rest of OO, which is part of the reason why there aren't any OO command-line utilities to convert among file formats.  Maybe the situation has improved with the 00.org 2.0 architecture, but otherwise I wouldn't expect to see any filter sharing with KOffice..."
    author: "Ellis Whitehead"
  - subject: "Re: Killer Release"
    date: 2005-05-02
    body: "    I haven't used koffice in a while but I hope that the fixes in the filters for OO.org 2.0 are incorporated (at a code level rather than just using the filters as are) because the filters in OO.org 2.0 are such a great improvement. At long last my CV opens correctly in OO.org 2.0 which has never been the case before as it is heavily formatted, Infact I haven't been able to find a .doc that doesn't open properly yet! \n    \n    One question though how come if RTF is an open standard then why doesn't it open consistently in all word processors? (Assuming that Microsoft's interpretation is correct as they control the format, at least that's the impression I get from http://en.wikipedia.org/wiki/RTF )"
    author: "behavedave"
  - subject: "Re: Killer Release"
    date: 2005-05-03
    body: "> I hope that the fixes in the filters for OO.org 2.0 are incorporated (at a code level\n\nI don't think that this will ever happen."
    author: "Anonymous"
  - subject: "Re: Killer Release"
    date: 2005-05-03
    body: "It is my understanding that RTF is a poorly documented format, despite being open, and that this is the reason different word processors interpret it differently.\n\nThis is probably one of the reasons why the format hasn't caught on as a standard document format."
    author: "Andreas Lundin"
  - subject: "Re: Killer Release"
    date: 2005-05-05
    body: "The other problem is that many RTF import filters are quite picky sometimes (leading up to crashes of the application).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Killer Release"
    date: 2005-05-03
    body: "\"At long last my CV opens correctly in OO.org 2.0 which has never been the case before as it is heavily formatted\"\n\nCrazy idea:\nMake it work natively in OO and then export it to Doc?\n\nThat's what I did and it works beautifully..."
    author: "Joe"
  - subject: "Re: Killer Release"
    date: 2005-05-04
    body: "No matter how intertwined the filters may be, it should at least be possible to instruct oo.org from the command line to start and run a macro that imports a document, saves it in the OASIS format and then terminates the app, no? In the worst case, you might have to use a virtual X server like Xvfb to prevent the GUI from appearing.\n\nThis conversion would take a while and load the whole oo.org memory hog, but this does not mean it could be included as an alternate filter in KOffice. The only thing the user would have to do is wait!"
    author: "Martin"
  - subject: "Re: Killer Release"
    date: 2005-05-04
    body: "Good idea, you create such a thing, please. And please report progress on this to OO.o, not here, since OO.o is not related to KDE whatsoever. Thanks!"
    author: "ac"
  - subject: "Re: Killer Release"
    date: 2005-05-04
    body: "He's talking about using OO.o as a filter FOR KOFFICE, which is a real issue since OO.o's MS-importing is so much better, so obviously it's extremely related to KDE.\n\nWhy the heck would OO.o care?\n\nI'm not saying that such a filter would be the best solution -- someone is eventually going to have to go through OO.o's code and make sense of it all and rip out parts that should be in shared libraries -- but it's a good suggestion.  No need to be a jerk."
    author: "kundor"
  - subject: "Re: Killer Release"
    date: 2005-05-04
    body: "Why the heck would KDE care?\n\nSeriously, as soon as you a familar with KWord's code (there are too few who are, as you know it needs more developers) it's far easier for you to extend KWord and its filters instead making yourself familar with OO.o (which is a far bigger beast) and working on that instead. Also OO.o will never be part of KDE or be advertised as such so every solution \"for KDE\" you offer through OO.o is pretty much useless for anyone running just KOffice and not OO.o.\n\nBut why even bother? OpenDocument is coming anyway now."
    author: "ac"
  - subject: "Re: Killer Release"
    date: 2005-05-05
    body: "1. Disk space is free.\n2. KDE does not hold itself above trying to interoperate with other software.\n3. Reimplementing all of OO.o:s import (and export) facilities in KOffice is not easier than writing a short macro.\n4. Why don't you take kundor's friendly advice, ac?"
    author: "Martin"
  - subject: "Re: Killer Release"
    date: 2005-05-05
    body: "1. You mean yours is free?\n2. So by that you mean effort should be spend on software outside KDE now for that reason?\n3. You imply that such a macro would be already possible. If so why not offer just it? Also reimplementing all of OO.o's import facilities is excatly what I'm NOT talking about, just simply someone spending sufficient time working at improving the existing filters is needed. Looking at the OO.o's filter code for that purpose is currently just a waste of time since the code is too OO.o specific. Just take a look yourself if you don't believe me.\n4. Because it's unrealitic, and you shouldn't raise false hope here."
    author: "ac"
  - subject: "Re: Killer Release"
    date: 2005-05-05
    body: "I think he was referring to the \"No need to be a jerk.\" part. \n\n> 4. Because it's unrealitic, and you shouldn't raise false hope here.\n\nThen again, it could be that you're fully aware of that and just being realistic. ;-)\n"
    author: "cm"
  - subject: "Re: Killer Release"
    date: 2005-05-05
    body: "> But why even bother? OpenDocument is coming anyway now.\n\nBy the way, do you have any idea what you are talking about? I thought not."
    author: "Martin"
  - subject: "Thank you very much! but:"
    date: 2005-05-01
    body: "Is it more stable now? Even some beginner (from me) one side KWord document brings KWord to nirvana.\n\nI always like KOffice from it's startup times, from it's layout and the right functions for me, but it really feels unstable.\n\nMy system: Slackware 10.1, KDE 3.4, Koffice 1.4b1\n\nKDE is stable as a rock, and even on older versions (Slack 9.0, 9.1, 10.0) KOffice was always unstable. This is sad because I like this program a lot!\n\n"
    author: "anonymous"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-01
    body: "> Is it more stable now?\n\nPlease try it and report any issues you find to bugs.kde.org."
    author: "Anonymous"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-01
    body: "> and report any issues you find to bugs.kde.org\n\nIf I draw a frame, type some text into it (with different sizes, fonts, bolds...) and then KWord crashes when I move/resize this frame, something other must be wrong than *I* write into bugs.kde.org... sorry, this should every developer see by yourself.\n\nMy other apps are *a lot* more *stable* (in compare to KOffice, these apps are bugfree)\n"
    author: "anonymous"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-01
    body: "dot.kde.org is not bugs.kde.org"
    author: "Anonymous"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-01
    body: "> dot.kde.org is not bugs.kde.org\n\nsure, but I discuss about KOffice and don't mind what you want to teach me. If you can't accept critics then Internet isn't your medium. So please stop.\n\nAnd developers doesn't need bug reports when KOffice stop working around a 1/4 full page with beginner work (move frames,...), they should see it independently if the developers use his programs for 5 minutes...\n"
    author: "anonymous"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-02
    body: "Indeed. If the developers experienced this bug it would have been fixed. Obviously they haven't.\n\nWelcome to the sometimes incomprehensible world of software development.\n\nPlease report the problem and your configuration. Someone may be able to figure it out.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-03
    body: "And Engrish, obviously, you can't write, so please stop.\nRead words from page when pick up Engrish book."
    author: "Joe"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-03
    body: "That's really uncalled for and uncool.  So not everyone speaks the Queen's English as well as you do, so what?  Maybe you don't speak the GP's language so well either, but I'm sure he won't deride you for it.\n"
    author: "LMCBoy"
  - subject: "Re: Thank you very much! but:"
    date: 2005-05-01
    body: "I tried to create a new frame, write in some random text, set some of it to bold, italic underline, change font size and font type and resize the frame/move it around and nothing happened.\n\nPerhaps you could supply some more specific instructions on how you provoked the crash? perhaps you could place a sample document (ie. one with a frame in so that if you resized it the kword would crash) on the web?\n\nHope the problem you described gets fixed."
    author: "Anonymous"
  - subject: "Koffice is Stable in Slackware 10.1 and KDE 3.4"
    date: 2005-05-01
    body: "Here i have Slackware 10.1 with KDE 3.4 and koffice 1.4b1, i don't have any crashes. Except for the first time when Kexi and krita crashed (perhaps due to no configuration file).\n\nAnd 1 more crash I had with kword is with when i tried to save my file in OASIS file format (perhaps juk was responsible too as it was playing songs and we know what arts is). Next time I ran only Kword and NO CRASH AT ALL.\n"
    author: "Fast_Rizwaan"
  - subject: "Problems to start kspread"
    date: 2005-05-01
    body: "I've never had problems starting kspread before. I have a recent cvs (5 days or so)\n\n$ kspread\nkio (KSycoca): Trying to open ksycoca from /var/tmp/kdecache-perra/ksycoca\nkoffice (lib kofficecore): kspreadpart.desktop found.\nkio (KTrader): query for KOfficePart : returning 1 offers\nkdecore (KLibLoader): WARNING: KLibrary: /opt/kde/lib/kde3/libkspreadpart.so: undefined symbol: init_libkspreadpart\nkdecore (KLibLoader): WARNING: The library libkspreadpart does not offer an init_libkspreadpart function.\nkoffice (lib kofficecore): WARNING: The library libkspreadpart does not offer an init_libkspreadpart function.\nkdecore (KLibLoader): The KLibLoader contains the library libkspreadpart (0x812b700)\n"
    author: "kaviar spreader"
  - subject: "Re: Problems to start kspread"
    date: 2005-05-01
    body: "> I have a recent cvs\nThat's the problem\n\nhttp://bugs.kde.org/\nThat's the solution."
    author: "ac"
  - subject: "Re: Problems to start kspread"
    date: 2005-05-03
    body: "actually, problems in CVS/SVN code should be reported to the appropriate mailing list, rather than b.k.o.\n"
    author: "LMCBoy"
  - subject: "Re: Problems to start kspread"
    date: 2005-05-03
    body: "Is this official KDE policy, as I typically run cvs, and report bugs I can reproduce to bugs.kde.org?\n\n"
    author: "James L"
  - subject: "Re: Problems to start kspread"
    date: 2005-05-03
    body: "No, LMCBoy is wrong - what he says only applies to likely temporary compilation errors."
    author: "Anonymous"
  - subject: "KOffice 2"
    date: 2005-05-01
    body: "When will Koffice 2 come out and what will the major change be? Full OASIS support?"
    author: "Hayer"
  - subject: "Re: KOffice 2"
    date: 2005-05-01
    body: "That will take a while.\n\nMost likely it will be based on Qt4 and KDE4, which means that there's probably a lot of work ahead.\n\nFor some more details, check out this post on the dot:\nhttp://dot.kde.org/1061919133/"
    author: "Tim Beaulen"
  - subject: "Re: KOffice 2"
    date: 2005-05-01
    body: "Is there a feature list for 1.4, and a feature plan for 4.0 somewhere?\n\nWould be good to see for a move from OO/SO."
    author: "Hayer"
  - subject: "Re: KOffice 2"
    date: 2005-05-01
    body: "http://developer.kde.org/development-versions/koffice-features.html\n\nBut there's no \"official\" feature plan for the next releases of KDE or KOffice.\n\nBasically it is:\n- continue to work on things not finished for the current feature plans\n- fix bugs and implement relevant wishes from bugs.kde.org\n(- and whatever the developers feel like doing next)"
    author: "Tim Beaulen"
  - subject: "Re: KOffice 2"
    date: 2005-05-05
    body: "There are not any real feature plan but there are some long term goals.\n\nSee the links (especially the one of the Kastle conference) in the foreword of: http://developer.kde.org/development-versions/koffice-features.html\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice 2"
    date: 2005-05-01
    body: "Actually, I'm aiming at full OASIS support in KOffice-1.4 already, at least for KWord. We're not far from it already."
    author: "David Faure"
  - subject: "Re: KOffice 2"
    date: 2005-05-01
    body: "Does this mean that kword has the exact same features as openoffice writer? I've never understand how can you interoperate with openoffice writer files if some Writer features are not in kword yet? please explain,\n\nPat\n\nPS: i love koffice and Kongratulation! :)"
    author: "Pat"
  - subject: "Re: KOffice 2"
    date: 2005-05-01
    body: "KWord certainly doesn't have all the features that openoffice writer has.\nSorry for my possibly misleading sentence then: I meant loading and saving 100% of KWord's features using OASIS, so that OASIS can really be used as a native file format for KWord (although it won't be the default one in 1.4, to be on the safe side; if enough people test it I can maybe make it a hidden option though).\n\nWhen you open a file produced by kword in ooo, or the other way round, even when using OASIS, you have to be aware that features supported by only one of the apps will obviously not work in the other, there's no way around that; so I obviously recommend sticking to the common set of features (i.e. using kword to write the document :).\n"
    author: "David Faure"
  - subject: "OpenOffice.org"
    date: 2005-05-01
    body: "The reason I did not use KOffice so far is the horrible table support.\nIt's so buggy that it's almost unusable. Perhaps this has improved\nin KOffice 1.4 (havent tried out so far). Nevertheless KOffice is\nan important project. I'm happy when I can finally discard the bloated\nOpenOffice. The start-up time, even in 2.0beta, of OpenOffice is\ntotally unacceptable. My machine has enough memory and a decent CPU.\nIt takes ages even for the splash screen to appear anyway. There is\nno hope that this will change in the near future because of all that\nstupid Java stuff in there. Probably a tribute to its Sun origin.\nFor now, I will have to remember never accidetally closing OpenOffice\nonce it's started - otherwise I have some free to time to fetch\na cup of cofee when I want to open a document. Kudos to KOffice what\nthey've achieved so far! If you dont need tables: It's the best\nfree Office suite out there.\n\n"
    author: "Martin"
  - subject: "Re: OpenOffice.org"
    date: 2005-05-02
    body: "Table support in KWord:\n\nYou can embed KSpread into KWord, so that's probably why table support in KWord is not so \"feature-rich\", it's not necessary to reimplement too many features in every component. That's the advantage of a really integrated office suite."
    author: "Raphael"
  - subject: "Re: OpenOffice.org"
    date: 2005-05-04
    body: "So why isn't embedding KSpread made obvious and intuitive and replace ANY Kword table support with Kspread-embedding?"
    author: "kundor"
  - subject: "Re: OpenOffice.org"
    date: 2005-05-04
    body: "The obvious answer is: how do you create formatted text in a cell, then? By embedding kword in it? ;-)"
    author: "Roberto Alsina"
  - subject: "Great Work!!! Thank you KOffice Developers!!!"
    date: 2005-05-02
    body: "OASIS is a big advantage for KOFFICE Acceptance in LINUX... Abiword 1.4, OO 2.0, KOFFICE 1.4 can now Share Documents and that's real GOOD for KOFFICE users.\n\nNow a request for Improvement of KOffice:\n\n1. Copy All Good Features from Open Office: Slowly and Consistently will hep KOffice Mature. Features like:\n\n   a. Drop cap\n   b. Autocomplete (inline, words more than 5 characters)\n   c. etc.\n\nI think that these features are going to be implemented automatically as to support the OASIS document format. \n\nI Strongly believe that KOFFICE will be the OFFICE SUITE of the Linux Desktop!!! Thank you for your great effort, we users appreciate it!"
    author: "Fast_Rizwaan"
  - subject: "Re: Great Work!!! Thank you KOffice Developers!!!"
    date: 2005-05-02
    body: "Thanks for the nice words.\nAutocompletion was already in KOffice-1.3, and autocompletion with a tooltip (as in OOo) is in 1.4-beta1. Is that what you meant by \"inline\"?"
    author: "David Faure"
  - subject: "Autocomplete is there"
    date: 2005-05-02
    body: "i enabled the autocomplete and the words >5 characters are appeneded to the list but won't work as expected. there is no tooltip suggesting the completeion of word!\n\ni'll report it at bugs.kde.org"
    author: "Fast_Rizwaan"
  - subject: "Nice"
    date: 2005-05-02
    body: "Didn't know that that KDE has its own officesuite before this announcement. Installed it and tried (I'm running debian sid so not the 1.4beta). Kword is much better than Openffice writer much liter too, I like it a lot. Only problem I encountered was KPresenter did not open some of my PowerPoint slides.\n\nPs. if someone has gotten the finnish spell software soikko working with Kword i wouldn't mind to know."
    author: "Petteri"
  - subject: "Re: Nice"
    date: 2005-05-02
    body: "I agree with you about KPresenter. I have to admit that PowerPoint is the only good reason for me to keep Windows around. I didn't try KPresenter in 1.4 so I can't comment on it. But I don't think the OOImpress is good enough in OO 1.1.4.\nIf I could dictate the development of KOffice I would say: Join your forces on KPresenter and .ppt-import/export. Reason: The rest is already good enough for me :-)"
    author: "anaon"
  - subject: "Re: Nice"
    date: 2005-05-03
    body: "\"I have to admit that PowerPoint is the only good reason for me to keep Windows around.\"\n\nYou obviously don't do any real work.\nPowerpoint has been banned at some companies because it wastes so much time."
    author: "Joe"
  - subject: "Re: Nice"
    date: 2005-05-03
    body: "Could you explain why Joe?"
    author: "E.SCHRODER"
  - subject: "Koffice works "
    date: 2005-05-02
    body: "Can't really thank enough the Koffice developers (tried OO, bought and tried SO7, but somehow never took to that).\nThere are a few bugs (maybe some day I'll gather up the courage to submit a bug report), but for my needs it is *very* usable as it is already.\nSo once more  - *thank* you; if it stays being fun, keep up the good work, and if happy users mean something to you, here's one ..."
    author: "Ned"
  - subject: "Grrrr ;-)"
    date: 2005-05-02
    body: "No SuSE packages.\nI badly want to try out Krita.\nIt must be a great application.\nBut I never get the chance.\nThose stand alone packages they released just\nnever worked - no matter what I tried - and\nI tried A LOT.\nNow I thought: Cool, KOffice 1.4 with Krita\nis released. But now: No packages.\nWell, one day I will finally get to try out\nKrita and if it's in an old people's home ;-)\n\n"
    author: "Martin"
  - subject: "Re: Grrrr ;-)"
    date: 2005-05-02
    body: "They are there.  Look again.  I went to the oregonstate mirror and rest assured, the SuSE packages for KOffice 1.4 beta were right there."
    author: "Dude"
  - subject: "Re: Grrrr ;-)"
    date: 2005-05-02
    body: "Doh! You are right.\nThey are in unstable, of course.\nThe reason why I did not find them:\n1) www.kde.org Homepage\n2) Click on KOffice 1.4beta Released under \"Latest Announcements\"\n3) Click on \"Binary packages\" under Download\n4) Click on Contributed binaries can be found here\nMy mistake now: I did not notice that it says \"1.3.5\".\nWhy does it say so? I clicked on KOffice 1.4beta before.\nGee, that web site organization can be really confusing sometimes...\nThanks for your help!\n\n"
    author: "Martin"
  - subject: "Still needs work"
    date: 2005-05-02
    body: "So I installed binaries from vendor RPMs.\n\nI went to confirm the version.\n\nWhen KWord eventually opens, it displays a few menu headings, one of\nwhich is Help.\n\nBut one cannot access anything in that menu because some damn silly popup\nwindow about opening documents appears and blocks interaction with Kword\nproper.  If the popup dialog is canceled, KWord crashes (disappears). This\nis known as user-hostile GUI design.  But it gets worse.  If some option is\nselected from the popup dialog to get KWord to actually stay open so that the\nversion can be checked, it still takes ages for KWord to become responsive,\nand when it does, the Help menu heading that was clicked on has moved, and\ninstead some other menu is displayed.\n\nWhen the Help menu is finally accessed, Help->About KWord says that the\nversion is 1.3 beta 1, rather than 1.4 beta 1 (and the binary is dated\nApril 26 2005 and replaced 1.3.5).\n\nAttempting to open (or import) a one-page, simple Microsoft Word document\npops up an Error window with only an \"OK\" button (no it's not OK!).  Now there\nis a minor improvement at this point; KWord 1.3.5 would crash when clicking on\nthat button, sending the user back to the KDE start menu to re-launch (from\nseveral levels down in the menu with no reference at all to KOffice). Kword\n1.<whatever-it-really-is> beta 1 doesn't crash at that point, nor does it do\nanything useful.\n\nBy contrast,OpenOffice.org Writer doesn't force silly dialog boxes on users,\ndoesn't crash when opened dialog boxes are canceled, doesn't shuffle menu\nheadings all over the place, displays meaningful and accurate information in\nthe \"About\" dialog, and reliably opens Microsoft Word documents.\n\nAlthough I'd love to have an alternative to OpenOffice, KOffice doesn't yet\nseem to be ready for that role.\n "
    author: "Anonymous Coward"
  - subject: "USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-02
    body: "SuSE and Mandrake though they claim better support for KDE are nothing but annoyance. Their packages are patched so heavily that you can't compile new applications without having those patches.\n\nSlackware is just Plain Good Distro. Performance wise and Source code compilation wise, and configurability it is the best!\n\nOther Distros are good like Mepis, Kubuntu, but they are not full-fledged like slackware.\n\nGet most of the Slackware packages at: \nhttp://www.linuxpackages.net\n\nGet Slackware support at:\nhttp://www.linuxquestions.org/questions/forumdisplay.php?forumid=14\n\nGet Supermount (good feature in mandrake and linspire) in Slackware:\nhttp://www.linuxquestions.org/questions/showthread.php?s=&threadid=318091\n\nI don't have any grudge for other distros like mandrake, suse, but Slackware is just great on performance! try it cause you'll always get the Latest KDE packages!!!"
    author: "A Happy Slackware User!"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-02
    body: "> Their packages are patched so heavily that you can't compile new applications\n> without having those patches.\n\ni compile new apps without modification and with 100% success[1] all the time on SUSE using their KDE binaries. so i'm not sure this is an accurate characterization.\n\n[1] given that the app sources don't have actual errors in them, which i sometimes find when i pull someone's beta code to play with, so no surprises there ;) "
    author: "Aaron J. Seigo"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-02
    body: "Wow, a Slackware zealot, I thought they were extinct =)\n\n"
    author: "Gentoo"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-02
    body: "hehehe... the irony of that comment coming from someone using the name \"Gentoo\" is delicious."
    author: "Aaron J. Seigo"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-03
    body: "Not all Gentoo users are zealots."
    author: "Paul Eggleton"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-03
    body: "Give them a few days..."
    author: "Joe"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2005-05-03
    body: ">Wow, a Slackware zealot, I thought they were extinct\n\n To call myself a zealot may be a bit over the top, as I don't bother fighting over anything.. But I am by far not extinct...\n\n Why you ask? Because we are quite a few who like the \"plain-vanilla-do-it-your-self-department-from-hell\", without any \"neat\" \"enhancements\", without any \"automagical\" this or \"userfriendly\" that (perhaps that's also why you don't hear us all that much.. we generally RTFM or send patches instead of arguing about what would be better).\n\n Besides his disregarding tone/misinformed accusations, i agree very much with the above poster.. Slackware provides a very nice KDE experience. The only thing that matches it (in my world) is LFS (and i don't have THAT much time ;-)...\n\n Cheers!\n ~Macavity"
    author: "Macavity"
  - subject: "Re: USE SLACKWARE for Better KDE experience!!!"
    date: 2007-02-26
    body: "Seriously. \nKDE runs faster on slack than anything.\nAlso, KDE's features tend to be dependent on little besides KDE itself, which brings about a lot of happiness to slack users, as most slackware packages are made to be less interdependent than on other distributions.\nSide note: KDE as is, is not my *favorite* desktop, but i'm looking forward to seeing 4.0! \n\nNow that's going to be awesome."
    author: "Mr. Pwnage"
  - subject: "Congratulations"
    date: 2005-05-02
    body: "Koffice is light in resources, and has the potential to be as multiplatform as openoffice.\n\nIt is known to have a been cleanly programmed (what should I know, and could be a fast, light and modular alternative to the 2 big ones.\n\nThe market needs competition, and openoffice (the other suite in the duopoly)is too slow and bloated, but Koffice has still too far to go to be an alternative for in a production environment.\n\nso:\n- use oasis format (check)\n- port to windows/mac (expand user/programmer base, as Kpim did)\n- use openoffice filters (should be feasable with the common oasis format)\n- keep it light: soon nor openoffice nor Msoffice will work on a 600 Mhz computer.\n- support Koffice, submit bugs, program if you can.\n\nKDE does not need a 54th program to sort music files, not a 69th sucking game that crashes my computer, it needs a good office package: Kontact and Koffice and 1 good music player."
    author: "Geert"
  - subject: "the only reason is mail merge"
    date: 2005-05-02
    body: "the only reason I mainly use OOo and not koffice is that mail merge in OOo allows as data source a flat text file, and koffice not. \n\nAs soon as koffice can do that, my OOo is only needed for opening occasionally arriving files from the legacy MS office world.\n\nChecking 1.4 now, full of hope :-) \n\nkoffice is more fun to work with, it is the future!"
    author: "Matt"
  - subject: "Re: the only reason is mail merge"
    date: 2005-05-03
    body: "You can read the data in a kspread document first; which allows better feedback if something is not entirely right.  Saves you from printing 50 pages only to find one wrong :)\n\nSo; its one extra step, but one that in the end gives you much more control and basically saves you loads and loads of work if the textfile was not 100% (which happens to more people then you might think!)\n\nHope you like it; Cheers!\n\nps; a mailmerge tutorial: http://www.virtualsky.net/daves/2005-06.htm\nand a translated version for people from Holland: http://www.kde.nl/doc/kword-mailmerge/"
    author: "Thomas Zander"
  - subject: "Re: the only reason is mail merge"
    date: 2005-05-04
    body: "Thanks, Thomas!\n\nyes, going through kspread is a possibility. However in my case it is just an extra step, because the data in the text file is produced by an application, and always formatted correctly. And then there is always a rush to get it printed... so a manual extra step is definitely not what I want here.\n\nMay be writing a small app to convert it to a koffice accepted format might be a way - if i find the time. But then again, there are probably also others with the same request, I guess (and I hope) sooner or later koffice will read flat text files a  datasource for mailmerge. \n\nRegards,\nMatt"
    author: "Matt"
  - subject: "Re: the only reason is mail merge"
    date: 2005-05-06
    body: "> \"the data in the text file is produced by an application, and always formatted correctly.\"\n\nFamous last words :)\n\n> \"I guess (and I hope) sooner or later koffice will read flat text files a datasource for mailmerge.\"\n\nMy hope is more along the lines of kword not going to code a parser that is already found in kspread.  It would be silly to duplicate work.\nSo a better solution would be to automate the parsing of that file for you using the kspread libraries and filters.\n\nYou would not have some spare time to hack on this, right?"
    author: "Thomas Zander"
  - subject: "Re: the only reason is mail merge"
    date: 2005-05-06
    body: ">> \"the data in the text file is produced by an application, \n>> and always formatted correctly.\"\n \n> Famous last words :)\n\nWell, after 0 errors in the last 2 years with many thousands of records processed, I dare to use \"always\" :)\n \n>> \"I guess (and I hope) sooner or later koffice will read flat text files as datasource for mailmerge.\"\n \n> My hope is more along the lines of kword not going to code a parser \n> that is already found in kspread. It would be silly to duplicate work.\n> So a better solution would be to automate the parsing of that file \n> for you using the kspread libraries and filters.\n\nYou are absolutely correct with this, yes, it is the better way to go. I did do a test, importing a text file in kspread. Easy and straightforward :)\n\nThen all what is missing is to allow a kspread file as a datasource for mail merge, I could live with that. I guess I could automate that step with a macro.\n\nThis would allow using all import filters of kspread easily.\n \n>You would not have some spare time to hack on this, right?\n\nUnfortunately not :(\n\nLack of time did get me already to drop C++ and replace it with python and python-kde to get my helper apps programmed more quickly - with much success :)\n\nRegards,\nMatt"
    author: "Matt"
  - subject: "krita"
    date: 2005-05-03
    body: "Here's my little krita screenshot: <a href=\"http://members.aon.at/mfranz/krita.jpg\">krita.jpg</a> [180 kB] (It's not using fancy crystal icons, but oldschool \"kde classic\". I hope this doesn't scare anyone away. :-)\n"
    author: "Melchior FRANZ"
  - subject: "Re: krita"
    date: 2005-05-04
    body: "Krita is very welcome as a competitor GIMP - Ok, I know, Krita wants to be more a painter application but I only want to use it for photo retouching.\n \nThe basics like CMS support, multi-channel, multi-layer and a fundament for 16bit/channel looks very strong, but krita (here CVS HEAD) is still not solid rock an crashs often. One thing I miss: A better zoom-view control like a slider and some fixed percent menu entries. Another problem is that some code seems to have a GUI but no backend code like \"duplicate\" or \"variations\". But I will follow this development with great enjoyment.\n\nI hope that krita gets more stable towards the final release.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: krita"
    date: 2005-05-16
    body: "I should hope so, too. I've been on a two-week holiday and am now waiting for the 10.000-odd emails to be filtered by KMail so I can get back into the swing, but even in the weeks following the beta packaging we already fixed many crashes."
    author: "Boudewijn Rempt"
---
The KDE Project today announced the release of KOffice 1.4beta1, the first preview release for KOffice 1.4, scheduled for release this June. <a href="http://www.koffice.org/">KOffice</a> is an integrated office suite with more components than any other suite in existence.  This release specifically introduces the database management application <a href="http://www.koffice.org/kexi/">Kexi</a>, the image editor <a href="http://www.koffice.org/krita/">Krita</a> and upgrades the major KOffice components to use the <a href="http://en.wikipedia.org/wiki/OpenDocument">OASIS Open Document</a> format, an industry standard allowing better interoperability across applications and platforms. A <a href="http://ktown.kde.org/~binner/klax/koffice.html">KOffice Live-CD</a> has been created and several distributions provide <a href="http://download.kde.org/download.php?url=unstable/koffice-1.4-beta1/">binary packages</a>. Read the <a href="http://www.koffice.org/announcements/announce-1.4-beta1.php">full announcement</a> and <a href="http://www.koffice.org/announcements/changelog-1.4beta1.php">the changelog</a> for more details.


<!--break-->
