---
title: "KDE CVS-Digest for December 31, 2004"
date:    2005-01-01
authors:
  - "dkite"
slug:    kde-cvs-digest-december-31-2004
comments:
  - subject: "Happy new year everybody ;-)"
    date: 2005-01-01
    body: "...and especially to Derek. Can't thank you enough for compiling the digest \nfor us every week. My saturday mornings would be quite less fun without them.\n"
    author: "Martin"
  - subject: "Re: Happy new year everybody ;-)"
    date: 2005-01-02
    body: "Yes, thank you Derek and everybody involved with KDE!"
    author: "Mikhail Capone"
  - subject: "Thank you and happy new year:)"
    date: 2005-01-01
    body: "2005 will be a great year for KDE."
    author: "Alex"
  - subject: "Media KIOslave"
    date: 2005-01-01
    body: "this media kioslave really sounds like it is something cool.\nbut I can't find any real information about it.\n\nany plans for some sort of udev/hal frontend to manage hardware in 2005?\nor is this supposed to be done at a lower level...\n"
    author: "Mark Hannessen"
  - subject: "Re: Media KIOslave"
    date: 2005-01-01
    body: "As far as I once read. The task of providing a front end is the goal of Freedesktop.org's HAL project."
    author: "Nolridor"
  - subject: "Re: Media KIOslave"
    date: 2005-01-01
    body: "There is already a HAL backend for this kioslave. All you need to enable it is to install hal >= 0.4 and dbus-qt bindings from cvs, and then compile kdebase. Feel free to test it !\n\nJ\u00e9r\u00f4me"
    author: "J\u00e9r\u00f4me Lodewyck"
  - subject: "Re: Media KIOslave"
    date: 2005-01-03
    body: "for me the media io slave should provide access for :\n\ncamera , usb stick , mp3 player (ipod->amarok) , and handy (with kitchensync).\n\nempty cdrs and dvdrs.\n\nchris"
    author: "chris"
  - subject: "You forgot Pol^W..."
    date: 2005-01-01
    body: "Enrico Ros work on Konqueror optimizations (unnecessary redraws)\n\nHappy New Year and thanks for CVS-Digest :)"
    author: "m."
  - subject: "Re: You forgot Pol^W..."
    date: 2005-01-03
    body: "YES, very great work, it enhanced the experience of using konqueror a lot!"
    author: "ac"
  - subject: "Thanks DEREK"
    date: 2005-01-01
    body: "I would like to thank you for your freat effort cause depite the fact it's a holiday you make our saturday morning more fun with your digest so 10^1000000000 thanks"
    author: "Ismail Belhachmi"
  - subject: "kdom?"
    date: 2005-01-01
    body: "Happy new year and thanks a gazillion for all the cvs-digests past year!\n\nCan someone explain (in \"normal\" people language) what kdom is? Thanks!"
    author: "LB"
  - subject: "Re: kdom?"
    date: 2005-01-01
    body: "http://www.w3.org/DOM/\n\nDocument Object Model. Structured documents (html, svg) are parsed and then are represented as objects which can be manipulated however you want. khtml has a similar implementation.\n\nhttp://webcvs.kde.org/kdenonbeta/kdom/DESIGN?rev=1.4&view=markup\n\nIn laymans terms, its a way to get really confused really quickly.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: kdom?"
    date: 2005-01-01
    body: ">In laymans terms, its a way to get really confused really quickly.\nlol; you are right, I still have no clue what can be done with it, but thanks for the links."
    author: "LB"
  - subject: "bmeyer"
    date: 2005-01-01
    body: "Since he didn't get a mention. I'd like to thank Benjamin Meyer, he always seems to be at hand to help out with those silly mistakes I've made with autotools or cvs. Mr Binner and Mr Faure are also my heros, be it through helpful emails or helpful commits :)\n\nAnd since this is a \"thanks-chum\" type post, I'd like to thank everyone! Working on KDE is the best thing I've ever done. I just wish I had started earlier - I had strange illusions about being a scientist.."
    author: "Max Howell"
  - subject: "Re: bmeyer"
    date: 2005-01-01
    body: "I need to thank you Max for useful tips and psn,argonel,strm,binner and many people for the help all over the year :-)\n"
    author: "cartman"
  - subject: "Re: bmeyer"
    date: 2005-01-01
    body: "\"me too\"!  I feel very fortunate to be a part of the KDE project.  Looking forward to all we accomplish in 2005!"
    author: "LMCBoy"
  - subject: "Thanks KDE :)"
    date: 2005-01-02
    body: "Wishing everyone involved with KDE a happy new year - you've produced some excellent software in 2004 and no doubt more improvements to come in 2005 - in particular like the performance improvements with Konqueror as a file-manager - very fast/snappy now.\n\nLooking forward to any brief preview articles on 3.4 or 4 over the next 6 months or so - with all the hard/involved coding work you're all doing, a couple of \"brief\", \"light\", \"eye-candy/features\" articles would be an excellent complement to the difficult dev. work you do - anyways,\n\nThanks everyone - the work's appreciated."
    author: "anon"
  - subject: "[OT] LinuxQuestions.org awards"
    date: 2005-01-02
    body: "vote for KDE!\n\nhttp://www.linuxquestions.org/questions/forumdisplay.php?s=&forumid=62"
    author: "anon"
  - subject: "Some additional Commit statistics..."
    date: 2005-01-03
    body: "...missing from the year's overview (I counted manually, which means, there could   be small errors in the numbers):\n\nThere have been 634 people committing to KDE.\n\n  5       people with >= 3600         commits, meaning about 10 per day or more;\n 27  (22) people with >= 1000 (<3600) commits, still about 3 per day or more;\n230 (203) people with >=  100 (<1000) commits, more than 2 per week;\n475 (245) people with >=   10 (<100)  commits, about one per month;\n595 (120) people with >=    2 (<10)   commits and\n 39       people with one commit.\n\nOf course the are \"some\" more people contributing to KDE who don't have CVS access, increasing the commit counts of those responsible for the modules."
    author: "Harald Henkel"
---
In <a href="http://cvs-digest.org/index.php?issue=dec312004">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=dec312004">experimental layout</a>): 
<a href="http://www.koffice.org/krita/">Krita</a> (<a href="http://www.koffice.org/krita/screenshots.php">screenshots</a>) implements pasting, layers and autogradient. The
media kioslave now handles cameras.
<a href="http://edu.kde.org/kgeography/">KGeography</a> adds more maps.
Special year end retrospective of the KDE development effort.



<!--break-->
