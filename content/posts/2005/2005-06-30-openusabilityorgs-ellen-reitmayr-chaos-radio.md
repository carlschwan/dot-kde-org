---
title: "OpenUsability.org's Ellen Reitmayr on Chaos Radio"
date:    2005-06-30
authors:
  - "swheeler"
slug:    openusabilityorgs-ellen-reitmayr-chaos-radio
comments:
  - subject: "Tabbed Interfaces!"
    date: 2005-06-30
    body: "I like the \"apollon\" tabs (buttons on the left, though it should be on right hand side for right-handed people ) interface much better for multimedia players.\n\n\n"
    author: "fast_rizwaan"
  - subject: "sceptical"
    date: 2005-07-02
    body: "Usability experts didnt do too much good to that other desktop envirronment and I see a lot of good UIs that are NOT following guidelines and that are loved by their users, like apollon, cited above, or amarok."
    author: "llort"
  - subject: "Re: sceptical"
    date: 2005-07-02
    body: "Engineers didn't do so well with the Titanic.  We should probably start having plumbers design cruise ships."
    author: "Scott Wheeler"
  - subject: "Re: sceptical"
    date: 2005-07-02
    body: "Usability is not about removing functionality, there must be some misunderstanding there.\n\nAnd in what parts does amaroK not follow KDE style guide?"
    author: "Anonymous"
  - subject: "Re: sceptical"
    date: 2005-07-02
    body: "I think we have quite a different situation here. The usability people are working closely with the developers. It definitely isn't a 'solution from on high'.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Taking Gnome's HIG ideas"
    date: 2005-07-02
    body: "maybe OpenUsability should take some notes on Gnome's HIG, it has some good ideas on how Usability should be."
    author: "Salsa King"
  - subject: "Re: Taking Gnome's HIG ideas"
    date: 2005-07-02
    body: "Which? And GNOME didn't invent usability or human interface guidelines."
    author: "Anonymous"
  - subject: "Installation"
    date: 2005-07-03
    body: "The main usability problem on the linux desktop is installation and distribution fragmentation. Autopackage seems to adress some of these problems. At least it initiates a discussion process that will hopefully result in a desktop LSB."
    author: "peter"
  - subject: "Usability"
    date: 2005-07-05
    body: "I think these are steps in the right direction, and more should be taken.\n\nI was on the kde usability list for a couple years, and we spent a lot of time arguing about what was simpler or more meaningful, without clearly defining who exactly was confused about what.  The rancorous debate about shortcut keys attached to the article about the KDE PIM Usability review strikes me as more of the same.\n\nAlmost everyone agrees that KDE's usability efforts are still rather limited.\n\nI believe what we have always lacked is a testing-driven usability strategy.  A lot of usability talk centers around what's simpler for novice users, but almost everyone who uses KDE, and certainly everyone participating in the debates is a computer hobbyist - including me, which makes us poor candidates for novice-style testing.    What we need to understand is what tasks users (which users?) want do with KDE, how they are trying to learn and apply the skills they need to do these tasks, and what the hangups are.  And the only way we can find this out is by testing."
    author: "Eric E"
---
For the German speakers in the KDE community, last night Ellen Reitmayr, of <a href="http://www.openusability.org/">OpenUsability.org</a>, who was involved in the recently covered <a href="http://dot.kde.org/1115936744/">KDE PIM Usability Review</a> was a guest for the most recent episode of the <a href="http://www.ccc.de/">Chaos Computer Club</a>'s <a href="http://chaosradio.ccc.de/">Chaos Radio</a> for which the topic was user interfaces.  Roughly 110 minutes in KDE developer Daniel Molkentin also called.  Ellen was a guest for the entire duration of the somewhat, well, <i>chaotic</i> three hour program.




<!--break-->
<p><a href="http://de.wikipedia.org/wiki/Benutzer:Tim_Pritlove/Chaosradio/103">Topics covered</a> include the history of user interfaces, <a href="http://www.kdedevelopers.org/node/view/1184">multiple, single and tabbed document interfaces</a>, input methods, benefits and problems of configurability and an amusing section on terrible dialog texts &mdash; as well as a mish-mash of other topics.</p>

<p>The 66 MB Ogg Vorbis file should be available via the CCC's <a href="ftp://ftp.ccc.de/chaosradio/cr103/">FTP server</a> later this week, but is already available over <a href="http://bittorrent.ccc.de:2342/file?info_hash=5cec1c3c0ec23c4ee0e5011014ae1b936207c177">Bit Torrent</a>.</p>


