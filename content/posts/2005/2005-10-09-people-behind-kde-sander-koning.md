---
title: "People Behind KDE: Sander Koning"
date:    2005-10-09
authors:
  - "jriddell"
slug:    people-behind-kde-sander-koning
comments:
  - subject: "KOffice: What The ...?"
    date: 2005-10-09
    body: "About usability: \"Well, KOffice could use some work, but that's not really a KDE application.\"\n\nThis would be interesting to hear more about.  Of course KOffice needs more work, but why on earth don't you think this it is a KDE application?"
    author: "Inge Wallin"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-09
    body: "Didn't you know? Applications that use Qt, and the KDE framework are not longer declared as KDE apps. We call them now 'Qt-and-KDE-framwork-using-but-not-KDE-apps-being'-applications. ;)\n\nBut seriously: Why did he say this? And of course KOffice needs some work, but the most important thing would be to get ODD working (correctly)."
    author: "Patrick Trettenbrein"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-09
    body: "> But seriously: Why did he say this?\n\nMaybe the KDE-NL l10n team forgot to work on KOffice and now they are looking for an excuse ;)\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-09
    body: "Not really, Koffice is fully translated into Dutch, and besides kdeedu, has the highest percentage of translated documentation "
    author: "rinse"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-09
    body: "unfortunately we've arrived with two uses of the term \"KDE application\": one where \"KDE\" means \"the KDE project ships it with each KDE release\" and one where \"KDE\" means \"uses KDE libraries and technology\". in retrospect, it appears we can only have one meaning and avoid confusion and that the latter is the more important of the two. just MHO."
    author: "Aaron J. Seigo"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-09
    body: "And the former was the one I meant.\nAs a side note, I don't consider KOffice to be _an_ application. (How many people will take this as a wrong remark?)"
    author: "Sander Koning"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-09
    body: "And KOffice is in some kind of middle land: It is shipped from the KDE project, but with its own release schedule. In my mind, the first criterium is what matters when you decide wether it is a KDE program or not."
    author: "Inge Wallin"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-10
    body: "I agree with Aaron that the second is more important.\n\nEspecially since this is the common one: applications are considered Qt applications if the use Qt, not if they are distributed by Trolltech.\n\n"
    author: "Kevin Kramme"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-24
    body: "Qt isn't a project that has set out to create a whole lot of applications. KDE, on the other hand, is all about creating an entire desktop worth of applications. The fact that the applications all use kdelibs is merely a side effect."
    author: "Pingveno"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-11
    body: "Why not call it a \"K-application\"  for the meaing \"uses KDE libraries and technology\". A \"KDE-aplication\" (meanig \"the KDE project ships it with each KDE release\") would be a subset of this, as would for instance the term \"Koffice-appilcation\". \n\n "
    author: "Swoosh"
  - subject: "Re: KOffice: What The ...?"
    date: 2005-10-11
    body: "Calling it a \"K-application\" would be rather natural since using a \"K\" in the application name is a signum for the KDE-project (love it or don't). \n\n"
    author: "Swoosh"
  - subject: "RMS or Linus........"
    date: 2005-10-09
    body: " I think its a bit of an odd question to ask, out of thin air, \"Richard Stallman or Linus Torvalds?\"..\n\n Richard deals with an entirely different field than Linus. I happen to like them both. I like Richard's views on software ethics, and I like Linus' view on how to build, and manage the development of, a kernel. So in short I think the question is just about as valid as \"Square Dance or Orange Juice?\" :-P\n\n~Macavity"
    author: "macavity"
  - subject: "Re: RMS or Linus........"
    date: 2005-10-09
    body: "Linus has a software ethic as well, I think thats what the question is really asking about."
    author: "Ian Monroe"
  - subject: "Re: RMS or Linus........"
    date: 2005-10-10
    body: "Well, it's a more evocative way of asking 'Is free software important for ethical reasons or for pragmatic reasons?'\n\nOf course, the question is impossible to ask in that way, because first you'd have to ask 'Is this stuff called free software or open source software?' which is basically the same question as well."
    author: "kundor"
  - subject: "Re: RMS or Linus........"
    date: 2005-10-11
    body: "orange juice."
    author: "Vlad Blanton"
  - subject: "Re: RMS or Linus........"
    date: 2005-10-11
    body: "You're wrong.\n\nSquare dance."
    author: "Jameth"
---
Known in <a href="http://www.kde.nl">KDE.nl</a> circles as the documentation coordinator for the Dutch localisation project, this man is also a demon player at ultimate frisbee and a reader of Hercule Poirot.  He might describe himself as a "nerd trying to have a fairly social life" but he is cute.  Tonight's <a href="http://people.kde.nl/">People Behind KDE</a> interview is with <a href="http://people.kde.nl/sander.html">Sander Koning</a>.
<!--break-->
