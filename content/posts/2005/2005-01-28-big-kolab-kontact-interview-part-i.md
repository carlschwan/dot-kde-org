---
title: "The Big Kolab Kontact Interview - Part I"
date:    2005-01-28
authors:
  - "fmous"
slug:    big-kolab-kontact-interview-part-i
comments:
  - subject: "It's not all vanilla"
    date: 2005-01-28
    body: "Although free and well known software applications are used, some of them\nneed to be patched before they can be used for kolab.  Components that need\npatches are: apache, cyrus imapd and amavisd-new.  This makes it almost\nimpossible for distributors to integrate kolab.  How is the kolab project dealing with this?  For details see: http://wiki.kolab.org/index.php/Kolab-major-app-patches\n\nIs there any intention to use apache2 instead of apache?\n\nThe 'LDAP Account Manager' http://lam.sourceforge.net/\nseems a nice application for kolab.  Any change that it will be used in kolab?\n"
    author: "groupy"
  - subject: "Re: It's not all vanilla"
    date: 2005-02-01
    body: "> Is there any intention to use apache2 instead of apache?\n\nKolab 2 does not make any heavy use of apache specific features so going for Apache 2 in the future is no big issue.\n\nWe depend on Apache 1.3 for Kolab 2 mainly because it is stable and supported while there are still known issues with Apache 2 and PHP."
    author: "Martin Konold"
  - subject: "questions"
    date: 2005-01-28
    body: "I'm really happy that there are finally some groupware solutions appearing for unix. Thank you very much for this interesting interview!\n\nStill, I have some questions:\n\n- There once was an idea to set up a kolab-server for the kde-community. One could integrate bugs, mailinglists, resources, todos, schedule kde-meetings and all that stuff. Also, one could probably stress-test kolab quite well with this setup. Why didn't this ever happen? Because of the costs?\n\n- I understand that putting all data into a single database might be a little too much. But since I've never understood (and even grown to hate) LDAP, I am wondering why all the configuration-data that now lives in LDAP wasn't put into a database, like mysql?! What are the disadvantages?\n\n- When I tried kolab at version1, it took me a couple of days to find out that the data stored with the kde kolab client is the same data that is accessed with the webinterface, but the outlook/toltec connector uses its own set of data and is NOT synchronized with the webinterface or the kde client. Thus, kolab1 was useless for me. I understand this has changed with kolab2?\n\nIn the second interview, I would really appreciate to hear about the synchronization plans. IMO, Syncing is one of KDEs really confusing areas. There are so incredibly many pieces of code meant to synchronize something (opensync, wince-sync, kitchensync, kpilot, multisync, kde-resources, kandy and all their friends) without a plan on how to bring it all together. Whats going to happen?"
    author: "me"
  - subject: "Re: questions"
    date: 2005-01-28
    body: "> I am wondering why all the configuration-data that now lives in LDAP wasn't put into a database, like mysql?\n\nI guess it depends on your LDAP configuration, there are SQL backends, arent't there?\n"
    author: "Kevin Krammer"
  - subject: "Re: questions"
    date: 2005-01-28
    body: "No, this hasn't changed with Kolab2. There is still no web interface which can access the same data like Kontact or Outlook which is probably mostly due to the concept which requires that any client keeps an offline IMAP4 cache of the complete data and per user.\nSo writing a web client is basically the same like writing a new groupware server from scratch _plus_ all the Kolab syncing stuff on top."
    author: "me"
  - subject: "Re: questions"
    date: 2005-01-30
    body: "This is nonsense. There is a webclient under development. Basic functionality is avaliable, but it will take time to complete the features. The webclient is able to access the same data as the other clients. Everything else would be against the concept to have one server accessibla with different clients."
    author: "Andreas"
  - subject: "Re: questions"
    date: 2005-01-31
    body: "The question was whether Kolab2 _has_ a usable webclient (one which works for more than 50 appointments in a folder), and it doesn't (if its in development for _Kolab3_, all the better!).\nAnd apparently the development has stalled (a webclient needs to implement offline IMAP to be useful with Kolab and that is probably out of question for something like Horde).\n\nAnyway, tell us if there is a webclient for Kolab which actually works in practice. The concept seems to be strictly focused on fat clients."
    author: "me"
  - subject: "Re: questions"
    date: 2005-01-31
    body: "> a webclient needs to implement offline IMAP to be useful with Kolab\n\nWhy would that be needed?\nThe webserver is part of the Kolab service it has direct (over loopback interface) access to the other services such as the IMAP server.\n"
    author: "Kevin Krammer"
  - subject: "Re: questions"
    date: 2005-02-02
    body: "Because its part of the Kolab concept for clients, read it? Besides the IMAP4 protocol doesn't allow for range queries (like gimme all events from this monday to friday) as required for a web calendaring client, so live access is a no-go."
    author: "me"
  - subject: "Re: questions"
    date: 2005-02-02
    body: "> Because its part of the Kolab concept for clients, read it?\n\nNo, that's why I was asking.\nMight still be possible without, usually clients are on a remote machine where reducing network operations matters.\n\n> Besides the IMAP4 protocol doesn't allow for range queries\n\nWell, unless I am complete mistaken on what offline IMAP is, it is still IMAP, only using a local cache. All the query logic has still to be implemented on top of that.\n\nSo my guess is that caching greatly improves performance but is not necessarily required (technically)."
    author: "Kevin Krammer"
  - subject: "Re: questions"
    date: 2005-01-29
    body: ">lives in LDAP wasn't put into a database, like mysql?! What are the disadvantages?\n\nNothing lives in LDAP, because LDAP has no storage. It is strictly an access protocol.  LDAP allows you to change out the storage mech. . If you like you can use DBM, DBM, or flat-file, without changing the client or the protocol. If this goes down the DB path, then it is locked in. No scaling. No flexability. Think MS solution.\n\n\nWhile I have not seen kolab2, kolab1 used imap to handle the sync. Good idea. "
    author: "a.c."
  - subject: "Re: questions"
    date: 2005-02-01
    body: "> - There once was an idea to set up a kolab-server for the kde-community.\n> One could integrate bugs, mailinglists, resources, todos, schedule \n> kde-meetings and all that stuff. Also, one could probably stress-test \n> kolab quite well with this setup. \n> Why didn't this ever happen? \n\nDue to lack of resources (mainly a decent server with a good connectivity).\n\nAs soon as server plus bandwidth is available I am more than happy to provide a maintained Kolab Server to the community!\n\n> - I understand that putting all data into a single database might be a little\n> too much. \n\nWhat kind of data are you talking about?\n\n> But since I've never understood (and even grown to hate) LDAP, \n> I am wondering why all the configuration-data that now lives in LDAP \n> wasn't put into a database, like mysql?! \n\nThe biggest advantage of LDAP is that it is a network access protocol while MySQL is a product with proprietary protocol network clients. Another plus for LDAP in the Kolab design is that it features a simple, efficient and reliable replication protocol.\n\nUsing LDAP as the access protocol says nothing about the storage technology. \n\nActually most LDAP servers use some kind of (relational) database in the backend.\n\n> What are the disadvantages?\n\nRelational databases are incompatible to each other and many clients including Kontact, Mozilla, Microsoft Outlook and Lotus Notes know how to retrieve information like addressbook data via LDAP but they are not capable to talk to a MySQL db.\n\n> accessed with the webinterface, but the outlook/toltec connector uses its\n> own set of data and is NOT synchronized with the webinterface or the kde\n> client. Thus, kolab1 was useless for me. I understand this has changed \n> with kolab2?\n\nYes, with Kolab 2 all Kolab 2 clients (PHP, KDE, Windows) use the same new Kolab 2 storage format. Basically this new format is a set of iCalendar attributes encoded in valid XML including an XML Schema (Relax-NG).\n\nThe new storage format was intentionally created in order to allow for the usage with shared folders. Multiple different clients can have simultaneous read and write access to the same folder and its objects.\n \n> In the second interview, I would really appreciate to hear about the\n> synchronization plans. IMO, Syncing is one of KDEs really confusing areas.\n\nI agree that synching is currently confusing. For the Proko2 project we limited ourself initially for syncing with the Palm Pilot for the KDE platform. With MS Windows/Outlook any existing OL-based synching solution keeps working so plenty of choices are available. We currently have no plans for synching support via the webclient.\n"
    author: "Martin Konold"
  - subject: "celsius with kweather"
    date: 2005-01-28
    body: "hey i use US locals cause I want kde in American English but I want my kweather to display temperatures in celsius. I didn't see anything about it in kcontrol accessibility and stuff, only how to change to metric system. Anyone?\nthanx in advance\n\nPat"
    author: "Pat"
  - subject: "Re: celsius with kweather"
    date: 2005-01-28
    body: "Looks like you can't choose Celsius without 'metric'. The code that decides whether to use C or F does it based on KLocale::Metric (kdetoys/kweather/metar_parser.cpp:437). Doesn't make much sense, but then again, measuring in body parts lengths doesn't make sense to me, either. At least, the U.S. are in good company: Liberia and Burma use imperial units, too. :-)"
    author: "Melchior FRANZ"
  - subject: "Re: celsius with kweather"
    date: 2005-01-29
    body: "Well, I think that I would prefer to have american with metric standard.\n(I am American who spent too much time in a lab).\nI wonder, if it is possible to simply create a setting for it (I do not know the Locale code)."
    author: "a.c."
  - subject: "Re: celsius with kweather"
    date: 2005-01-29
    body: "> I would prefer to have american with metric standard.\n\nUS + metric is no problem. It's US + imperial + Celsius that doesn't work now."
    author: "Melchior FRANZ"
  - subject: "Re: celsius with kweather"
    date: 2007-06-04
    body: "How is kweather set to US (F)?\n\nGreat program...as is KDE and LINUX!!!\n\nThank YOU"
    author: "Ken Fine"
  - subject: "A better website for Kolab"
    date: 2005-01-28
    body: "Thanks Fab for this very nice interview.\n\nKolab looks like a great project but the weak point is marketing. Intevation should make some effort before the release of Kolab2 to revamp the Kolab website, have a much clearer message to what Kolab is about, scrap all the stuff about Kroupware, Proko2 that are more internal cooking that something you wan't to communicate on.\nI don't know which is the best between OpenGroupware and Kolab but OpenGroupware has clearly much better marketing.\n\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: A better website for Kolab"
    date: 2005-01-28
    body: "Actually the Kolab server and OpenGroupware.org are complementary. Since Kolab server is just a Cyrus server / Postfix MTA plus an LDAP setup, OpenGroupware can use it as a regular backend. There are setup guides for that on the documentation website of OpenGroupware.\n\nThen of course is the question why one would want Kolab server at all since all those daemons are standard on any current distribution anyway, including polished configuration interfaces (like YaST for example). The requirement to turn of the system daemons maintained by your favorite distributor in favor of packages patched by \"someone\" seems rather outdated?"
    author: "Urks"
  - subject: "Re: A better website for Kolab"
    date: 2005-01-30
    body: "Please read the concepts behind the groupware solutions before posting. Both solutions target the same areas, but the technical concepts are different. It is correct that the integration of Kolab in a universal services machine (ftp, http, mail, cvs, whatever) is difficult, but Kolab is made for larger installations where you'll have dedicated Kolab server anyway. There you won't worry about other deamons."
    author: "Andreas"
  - subject: "Re: A better website for Kolab"
    date: 2005-01-31
    body: "I've read about that, too. From the paper it looks like Kolab is targetting fat-client installations (web interface near to impossible due to offline IMAP4 requirement) with 100.000 and above (overkill for installations <1000?). On the other side the only reference which uses Kolab seems to be the German BSI which sponsors the whole project. BSI might be some hundreds of users at best and certainly doesn't count as a \"larger installation\".\n\nAre there actually any _large_ Kolab installations? I suppose it would be really great if some references (besides BSI) would be posted in the followup article! So far it sounds like a university research project and I guess people wanting to switch from Exchange are curious about real world examples!\n\nPS: for a large installation I would rather ensure that the mentioned services are maintained by one of the bigger Linux companies which have sufficient resources to ensure proper QA, like in SLES 9 or RedHat AS (or Debian if you want to go for free)."
    author: "me"
  - subject: "Re: A better website for Kolab"
    date: 2005-02-01
    body: ">BSI might be some hundreds of users at best and certainly doesn't count as a \"larger installation\".\n\nThe german authorities do have \"a few\" hundred thounsand employees..."
    author: "Carlo"
  - subject: "Re: A better website for Kolab"
    date: 2005-02-02
    body: "The German \"authorities\" do not use Kolab, only BSI is known to actually use it (as mentioned, some deployment examples would be cool for the next article). The current Linux migration guide for German authorities list a lot of groupware servers.\n\nBTW: Besides there are no really \"big\" German authorities given that this is a federal republic with a lot of smaller ministries (usually some hundreds of users, only sometimes a few thousand). Quite different to for example France where you have huge ministries (with more than 100.000 employees in a single location)."
    author: "me"
  - subject: "Re: A better website for Kolab"
    date: 2005-02-01
    body: "> Actually the Kolab server and OpenGroupware.org are complementary.\n\nI know that this information can be found on the OpenGroupware website though it is incorrect. Kolab 2 is a substitute for traditional database driven groupware solutions.\n\n> Then of course is the question why one would want Kolab server at all since \n> all those daemons are standard on any current distribution anyway\n\nKolab has many advantages compared to OpenGroupware. \n\nJust consider scalability, security, offline support and support for MS Outlook clients for example!"
    author: "Martin Konold"
  - subject: "Evolution is better for enterprices..."
    date: 2005-01-28
    body: "I still believe Evolution is a way better solution for an enterprice, it has the Exchange pulggins and a Windows version is on the way and the next version will be plugginable.\n\nKontac is more oriented to the home users.\n\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-28
    body: "In KDE 3.4 Kontact features Exchange support as well. Furthermore a port of Kontact to Windows should be much easier and faster to do than porting Evolution to Windows for various reasons (Actually some parts of Kontact have already been ported to Windows).\n"
    author: "MaX"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-28
    body: "\"Furthermore a port of Kontact to Windows should be much easier and faster\"\n\nSounds good in theory, but we all know this will never be done.\n\n"
    author: "U"
  - subject: "Right you are"
    date: 2005-01-29
    body: "Since most of KDE already runs on windows. Like this.\nhttp://kde-cygwin.sourceforge.net/kde3/screenshots.php?img=kmail.png\n\nAnd the goal is to do it even better.\nhttp://kde-cygwin.sourceforge.net/qt3-win32/features.php\n"
    author: "Morty"
  - subject: "Re: Right you are"
    date: 2005-01-29
    body: "Using Cygwin, you must be joking! I don't think that KDE apps are easier to port than gtk+ apps, eg GIMP works just fine on Windows.\nIf I remember right, there are also issues with Qt on Windows, doesn't this require licensing costs?"
    author: "me"
  - subject: "Re: Right you are"
    date: 2005-01-29
    body: "The first link shows it works NOW, with Cygwin. As are more than you can say about the alternatives. Except Athera who actually uses the a modified KOrganiser as callendar part:-) \n\nAs for porting to windows, you have to compare KDE to Gnome and Qt to gtk+. Then let the numbers speak, Qt applications clearly outnumbers gtk+ ones.\n\nHad you actually tried the links, the second link would have shown you a working port of the GPL Qt to windows. Already used to port applications, not simple ones at that.  "
    author: "Morty"
  - subject: "Re: Right you are"
    date: 2005-01-31
    body: "I think \"me\"'s point is that it is as easy to port a GTK or Qt application with Cygwin.\nThe real issue is when making a native binary port without using some sort of virtual machine environment.\nCompiling a native binary of Kontact in Windows would require Novell to buy Qt licenses and that's probably why they have followed the path of using Evolution, even if it is harder. The issue is not technical, it's practical, that's \"U\"'s and other's points.\n\nAnyway, personally, I couldn't care less about free software being ported to Windows."
    author: "blacksheep"
  - subject: "Re: Right you are"
    date: 2005-01-31
    body: "<i>Compiling a native binary of Kontact in Windows would require Novell to buy Qt licenses</i>\n\nThat would be the fastes way, another option would be the route they are taking with GTK+, porting Qt/Free to Windows, or more accurate, finishing the ongoing portin effort.\n\nAfter all the current cygwin stage is only considered an intermediate step towards a fully native Win32 port of Qt/Free.\n"
    author: "Kevin Krammer"
  - subject: "Re: Right you are"
    date: 2005-02-01
    body: "The work in porting to Cygwin is not in the GUI part in any case, so the toolkit does not really matter. The fact is that the Qt port is already functional.\n\nIf the problem are for Novell to buy a few Qt licenses for their developers, you are right the issue is not technical. But it's also far from practical, it's  political. In this case tho, I'll guess this is more like some kind of  backroom program than a Novell strategy, since Novell already have a successful Groupware solution on windows."
    author: "Morty"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: "> Sounds good in theory, but we all know this will never be done.\n\nThe PIM developers seem to think different:\nhttp://dot.kde.org/1106179379/1106206796/1106211548/\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: ">> Waste of time. Kontact/KMail etc. are KDE integrated apps.\n\nWhat does that supose to mean?\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: "No idea, I was referring to Ingo's posting.\n"
    author: "Kevin Krammer"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: "I do not understand why Novell does not buy Trolltech and make Qt LGPL for Linux. \n\nI think this would solve many problems because as far as I read is Qt technical much mature and better crossplattform than GTK+.\n\nSo instead of tinkering with GTK+ for other platforms they could save time and could concentrate their efforts on developing Kontact and port it with less effort to Windows.\n\nAnd maybe this would also attract more developers developing applications in C++ for Linux instead of using C with GTK+.\n\nOK - maybe I'm a bit offtopic now."
    author: "Felix"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: "> I do not understand why Novell does not buy Trolltech\n\nMaybe because Trolltech has currently no interest in being bought?\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: "Evolution is as buggy as hell, and has got more so over the past three or four years. Porting the thing to Windows is rather pointless and will create an absolute ton of new work and bugs. I don't think they realise how much work it will be. It also raises serious questions as to how Novell's desktop migration to Linux is going."
    author: "David"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-29
    body: "\" Evolution is as buggy as hell\"\n\nThen you should check your distro or pc, my Evo installation is rock solid and is fast.\n\nKontact by the other hand keeps crashing on me, I dont really care cause I don't find it mature enough to using at work anyway.\n\n\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-30
    body: "Your comment shows that you haven't understood at all what Kolab and Kontact are about. And if Evolution is really that cool, it should be easy to provide support for the Kolab server beside of the Exchange suppport."
    author: "ac"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "I don't know, they are already to busy with the Novell Groupware.\n\nAnd I know what Kolab is and what is pretending to do, but honestly, I dont think it will be successful, why? simple, it is QT based, that mean, if you want to implement, lets say, a pluging for it in your own enterprice you have to pay to Troll Tech for the developers tools, and that's at least $1,500, and no, not every enterprices like to opensource they technique, and even so, according to QT licence you must pay if you are getting benefits of QT in your enterprice, that's why I think it won't be adapted, not for enterprices and surely not by countries.\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "First, it's written \"Qt\". Then, assuming you talk about the client side who forces you to write your plugin with Qt?"
    author: "Anonymous"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "That's why is not a viable solution, if I have to use a third party tool for writing plugins for it, I don't see the point of using it at all, if I use the Qt tool kit, then it is at least $1,500, those are two big showstoppers for Kolab.\n\n\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "I guess you are mixing Kolab and Kontact here. Kontact is Qt based, but as far as I know there is no Qt requriement when developing a Kolab client.\n\n> not every enterprices like to opensource they technique\n\nThey don't have to distribute their code as long as they use is purely internally\n\n> and even so, according to QT licence you must pay if you are getting benefits of QT in your enterprice\n\nNo, you are mixing that with the Qt/non-commerical licence for Windows.\nQt/Free as available on Unix/Linux and OS X is at least licenced under GPL and GPL does not allow such a restriction."
    author: "Kevin Krammer"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: ">> No, you are mixing that with the Qt/non-commerical licence for Windows.\n\nExactly, and since 90% of the world pc users use Windows, they are atached to this license, that's the big problem.\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "I fail to see the problem.\n\nKolab runs on Unix/Linux systems, the communication with the server does not require Qt, so there is no need to use Qt on Windows if you want to access the Kolab server.\n\nVery likely the most used Kolab client under Windows is currently Outlook with one of the connector plugins.\n"
    author: "Kevin Krammer"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: ">>> Very likely the most used Kolab client under Windows is currently Outlook with one of the connector plugins.\n\nThat's the answer I needed, thank you.\n\n"
    author: "U"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "> Very likely the most used Kolab client under\n> Windows is currently Outlook with one of the\n> connector plugins.\n\n You've just reached his point. :D"
    author: "blacksheep"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-01-31
    body: "How so?\n\nHis point is that Kolab can't be successful because he thought it would put a Qt dependency on its usage, which it does not as I explain and what he acknowledge.\n\nSo this means we have reached the point where we both agree that Kolab is going to be successful because it can be used by a number of different clients on different systems, even allowing the use of already deployed clients and just changing the PIM server.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Evolution is better for enterprices..."
    date: 2005-02-01
    body: "if you want to implement, lets say, a pluging for it in your own enterprice you have to pay to Troll Tech for the developers tools, and that's at least $1,500\n\nThey won't have to pay. If you use the plugin only in your company, you don't have to realease the code. Read the GPL."
    author: "Carlo"
  - subject: "Kolab name has Mormon references"
    date: 2005-01-28
    body: "A somewhat unknown part of the Mormon religion is that the universe is a modeled after what we know as a solar system... in essence the entire universe is rotating within a fixed orbit. In the same way that the Sun is the center of our solar system, the universe is rotating around its own massive object. Mormonism theology suggests this object is an ULTRA massive planet that is so large is would cause the entire universe to rotate around it. The name of this massive planet is Colab.\n\ninitially I would have thought of the name as something coincidental, but the Colab logo causes me to think otherwise.\n\nAre there Mormon influences within the Colab project?"
    author: "Kelly McNeill"
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-28
    body: "kolab was founded mostly by germans in germany, and I don't (think) there are very many mormans in germany, so I doubt it. Interesting information though."
    author: "anon"
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-28
    body: "\"Are there Mormon influences within the Colab project?\"\n\nThe project's name is Kolab, not Colab"
    author: "ac"
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-29
    body: "The client name is Kontact, not Contact."
    author: "anonymous"
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-29
    body: "I might have misspelled the name. I just remember someone telling me... never seeing it spelled.\n\nBack in college, I had to Mormon missionaries that lived across from me at apartment that told me the info.\n\nI should have typed Kolab to make my text relevant."
    author: "Kelly McNeill"
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-28
    body: "I'm sure the name probably comes from \"collaboration\" seeing as it's a groupware solution. I'm no definitive source though, ask the developers :p"
    author: "Kamil Kisiel"
  - subject: "Depends...."
    date: 2005-01-29
    body: "If you are mormon, then yes.\nIf you are not, then no.\nIssue solved.\n:)\n\nBut most of this was done in germany, which is heavy luthern, no?"
    author: "a.c."
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-29
    body: "I created the current version of the Kolab logo (modelled after a previous existing one for the kolab project). AFAIK neither the original author nor me (pretty sure about the latter ;-) are Mormons. And the name is just a short form for collaboration.\nStill it's a nice story (although as a physicist I'd like to point out that you don't really need the hypothesis of a massive central body just to make the remaining universe rotate around a common center ;-) .\n\nBut of course everyone needs Kolab! :-)"
    author: "Torsten Rahn"
  - subject: "Re: Kolab name has Mormon references"
    date: 2005-01-29
    body: "On another note, I would like to say that the logo is totally awesome! It looks extremely professional and fresh.\n\nGreat work, Torsten!\n"
    author: "Mark Kretschmann"
  - subject: "Re: Kolab name has Mormon references"
    date: 2007-02-14
    body: "The exact Mormon name is 'Kolab'. Not Colab.  You can see this referenced by their mountain and canyons in Southern Utah -- Kolab Canyon. Google it.\n\nBtw, mormons suck.\n\n"
    author: "NotaMoe"
  - subject: "Re: Kolab name has Mormon references"
    date: 2007-02-15
    body: "Although I am now a follower of Christ, I am a former Mormon who attained the Melchezidek priesthood, therefore I can speak from personal experience.  I can tell you that Mormons believe and teach that Jesus originally came from the planet Kolab, which is somewhere in the universe.  They may deny this, but they also believe that Jesus and Lucifer were brothers created by God the Father.  They teach that God the Father told Jesus and Lucifer to come up with a plan for the salvation of mankind, that God the Father preferred Jesus' plan of sacrificing Himself over Lucifers plan to force mankind to obey God.  That, supposedly, was the beginning of Lucifer's jealous rebellion against God.  They also believe that those who are worthy will become gods themselves and create their own worlds just as Jesus did.  Their real focus is not on Jesus, but on Joseph Smith as a prophet of God.  In reality, because they believe that Jesus was created, they deny His deity as part of the Triune God, which, by definition qualifies Mormonism as a cult.  Mormonism is truly one of the more cleverly devised deceptions of Satan.  I know a great deal about Mormonism and am willing to answer any questions about it.     "
    author: "Jim Avila"
  - subject: "Re: Kolab name has Mormon references"
    date: 2008-12-10
    body: "I feel bad for you. You are not correct and missed the mark many times in your text."
    author: "H"
  - subject: "JESUS is coming!"
    date: 2007-11-18
    body: "Are you ready?"
    author: "A. Christian"
  - subject: "Cyrus does *NOT* use Maildir"
    date: 2005-01-29
    body: "Either the Kolab server is not using Cyrus-IMAP, or the interviewer and developers are not talking about the same thing.\n\nCyrus-IMAP uses several different database formats for message storage.  It does *NOT* use Maildir for message storage.\n\nCourier-IMAP uses the Maildir format, but it does not scale into the thousands of users, or the tens of thousands of messages per folder.  Trust me, I've tried.\n\nI have 500 MB of archived mail, with several folders with 15,000+ messages in them.  Trying to get this to work on a dual-AthlonMP 2200+ system with 2 GB of RAM was not a lot of fun using Courier-IMAP and Maildir.  It would take upwards of 30 seconds to load a message index, and it could take even longer to do operations on multiple messages.\n\nI installed Cyrus-IMAP on this same server, copied my messages over from one setup to the other (using Kontact no less), and haven't had any problems since.  Folder indexing and access is in the sub 5 second range on my largest folders (just under 20,000 messages to date), and doing operations on multiple messages takes no time at all.\n\nSo, are the Kolab poeple using Courier-IMAP and Maildir, or are they using Cyrus-IMAP and database storage for messages????"
    author: "Phoenix"
  - subject: "Re: Cyrus does *NOT* use Maildir"
    date: 2005-01-29
    body: "I believe that the interviewer means that Cyrus IMAP stores messages as files in the filesystem -- just like MailDir.\n\nBut you are right that Cyrus does not use MailDir. It uses it's own storage format which is basically this: One file per message, filename is the IMAP UID of the message. In addition to that, metadata/indexes/etc are stored in small binary databases (skiplists, berkeley dbs etc...). The important issue here is that the message contents is not put one big blob somewhere."
    author: "Steffen Hansen"
  - subject: "Re: Cyrus does *NOT* use Maildir"
    date: 2005-01-29
    body: "Aha.  That makes sense."
    author: "Phoenix"
  - subject: "Re: Cyrus does *NOT* use Maildir"
    date: 2005-02-01
    body: "FYI: My company has all email stored on a Pentium III - 550 MHz, 256 MB RAM Kolab 2 server using an encrypted filesystem on a venerable 3Ware based RAID-1.\n\nMy personal email alone is 4773 MB spread across 580.000 messages.\n\nSofar the performance of Kolab2 or Kontact is not an issue at all!"
    author: "Martin Konold"
  - subject: "What is Kolab?"
    date: 2005-01-29
    body: "Reading the article it is really hard to understand what Kolab actually is. It basically comes down to\n- Kolab server, which is just Cyrus and some other components already available on every\n   regular Linux distribution\n- Kolab Client, which is just Kontact and available for a lot of servers, including Exchange,\n   Groupwise or OpenGroupware\nWhat do I need Kolab for?"
    author: "Member"
  - subject: "Re: What is Kolab?"
    date: 2005-01-29
    body: "One can get the kernel source from kernel.org, the sources of the GNU system from gnu.org, the KDE sources from kde.org, yet most Linux users use one of the available distributions.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: What is Kolab?"
    date: 2005-02-01
    body: "Kolab offers a lot of extra features including multi-location support and server side freebusy generation and server side resource management (rooms, car fleet....)"
    author: "Martin Konold"
  - subject: "Standards for open source groupware"
    date: 2005-01-29
    body: "Actually, the emerging standard looks like it's going to be GroupDAV (http://www.groupdav.org), an HTTP-based protocol designed for open source groupware clients to talk to open source groupware servers.  On the client side, KOrganizer and Evolution are both going to support it (very soon), and there is talk about eventually getting Thunderbird/Sunbird speaking GroupDAV as well.  On the server side, both the Citadel and OGo servers will offer GroupDAV support in upcoming versions.  I would strongly suggest that the Kolab developers get on this bandwagon as well -- as I've said elsewhere before, I really think the Kolab2 protocol is the wrong direction to go in.\n\nAnyone looking for a good open source groupware system, please do check out Citadel [ http://www.citadel.org ] -- it is a very complete, flexible system and it is VERY easy to install.\n"
    author: "Art Cancro"
  - subject: "It's possible to convert between iCal and TNEF"
    date: 2005-01-29
    body: "Bynari (www.bynari.net) has the solution.  Their Web based groupware and Outlook connectors also use both TNEF and iCal natively."
    author: "WartyWarthog"
  - subject: "Re: It's possible to convert between iCal and TNEF"
    date: 2005-01-30
    body: "Using the Binary connector to access a Kolab 1 server didn't convice me. The Toltec connector did by far a better job."
    author: "ac"
  - subject: "Re: It's possible to convert between iCal and TNEF"
    date: 2005-02-01
    body: "I can second this experience very much. \n\nFor Kolab1 we initially relied upon the Bynari connector. In the meantime I am very happy that alternatives did develop.\n\nThe Toltec connector is a very nice product. Unfortunately as of today the Toltec connector for Kolab 2 is not publicly available. (I expect it in the next weeks though)."
    author: "Martin Konold"
  - subject: "\"Meta-package\" for FreeBSD?"
    date: 2005-01-30
    body: "I like to run server software on freebsd for linux, os/x and win clients ...\n\nCan I do\n\ncd /usr/ports/net/kollab ; make install \n\nto get it running?  Kollab (the server) seems to have no LDE specific components?\n\n"
    author: "FreeBSD Server Guy"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-01-30
    body: "\nThis would truly rock!!  I think openpkg is basically RPM ... there is an \"rpmtopkg\" script out there somewhere the rest would be getting all the bits to build on FreeBSD using the ports MAKEFILE infrastructure.\n\nhttp://lists.freebsd.org/pipermail/freebsd-ports/2005-January/019928.html\n\nMost of the pieces already build on FreeBSD?  e.g. OpenLDAP postfix CyrusIMAP Apache ProFTPd SASL etc. let's wrap it up!!  \n\nThe ports MAKEFILE stuff will be easy to port to OSXServer, NetBSD, OpenBSD, and DragonFly ... :-)"
    author: "More-OSX_KDE_Interpollination"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-01-30
    body: "Check out http://wiki.kolab.org/index.php/Kolab2_linux_distribution_integration\nfor an exmaple to see what needs to be done to port kolab..."
    author: "Richard"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-02-01
    body: "Actually FreeBSD (no support for jail currently) is a common and supported platform for Kolab 2. \n\nAll you need to do to get the Kolab 2 server running on FreeBSD is:\n\n- get all packages from http://max.kde.org:8080/mirrors/ftp.kolab.org/server/beta/kolab-server-2.0-beta-2/sources/\n\n- become user root\n\n- run sh -x ./obmtool kolab\n\n- run /kolab/etc/kolab/kolab_bootstrap -b\n\nThe rest is configured via the webinterface https://yourserver/admin/"
    author: "Martin Konold"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-02-01
    body: "Sorry forgot to mention that before login into the web admin gui the Kolab server must be started of course:\n\n/kolab/bin/openpkg rc all start"
    author: "Martin Konold"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-02-01
    body: "If OpenPKG would be fine for everyone... Is the kolab wiki patch list up to date?"
    author: "Carlo"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-02-01
    body: "Yes, it is uptodate and we are in the process of providing the patches upstream"
    author: "Martin Konold"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-10-13
    body: "did this ever go anywhere?\n\nI am very interested in using Kolab, but the openpkg method of double installing everything that probably already installed on our FreeBSD server is not very attractive.\n\nDid anyone make progress in creating a FreeBSD port for the Kolab server using patches on the standard port packages or cyrus, postfix etc...?\n\n"
    author: "Oliver Schonrock"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2005-12-17
    body: "The double install in unavoidable when install Kolab. But, there is an upside to how they install things:\n\nAll (for the most part) of Kolab's necessary programs are contained in one directory: /kolab. The programs there are statically linked to one another and will not interact with programs out side of its directory. (at least this is how I understand that it functions).\n\nSo, I guess the one of the things to consider when installing Kolab is whether or not dedicating a whole server to its functionality balances against your needs. Otherwise, I find it absolutely worth it."
    author: "Bill Totman"
  - subject: "Re: \"Meta-package\" for FreeBSD?"
    date: 2006-10-25
    body: "Kolab server can not be installed the way you explain it in /Kolab because it doesn't respect the layout of the FreeBSD's filesystems. man 7 hier\n\n*BSD are usualy cleaned system: they don't install double system/server just because the author of a software think that it is a good idea to pack a lot of other project available on the net with his/her project.\n\nThe only good reason I think to install a copy of something already available in the ports tree and outside the usuale $PATH is if your software (kolab) is running with specialy patched version of imap and smtp server (or something else) that is not available in the ports tree."
    author: "Serge Gagnon"
  - subject: "Aethera"
    date: 2005-02-01
    body: "Hi,\nI was expecting to see at least few words related to Aethera (http://www.thekompany.com/projects/aethera/) because IMHO it helps both Kolab and Kontact projects (see my comments below).\nKolab has only *one* client which is available *now* for all the most important platforms (Linux, Mac OSX and Win) and this is Aethera. It looks they are not very interested in promoting it.\nMore, Aethera has support for shared folders for Kolab *and* Citadel, it's the only client which can do it for both groupware servers now.\nAethera is open source and it's using a changed version of KOrganizer. I am sure that my effort to port KOrganizer to Mac and Win will help KDE PIM to get Kontact ported to those platform too (especially the current effort to synchronise the sources with latest KOrganizer). I am sure that both Aethera and Kontact have something to get looking each other for bad and good points.\nThanks,\nEug\n"
    author: "Eugen Constantinescu"
  - subject: "Re: Aethera"
    date: 2005-02-01
    body: "Aethera looks real nice, and have actually been on my \"recommend for windows users\" list for some time:-) So keep up the good work.\n\nAs for promoting, have you tried to get it included on TheOpenCD, http://theopencd.sunsite.dk/. As of 2.0 they only have a mailclient (Thunderbird) and not a real PIM application.  "
    author: "Morty"
  - subject: "Re: Aethera"
    date: 2005-02-04
    body: "> have you tried to get it included on TheOpenCD, http://theopencd.sunsite.dk/\n\nI think they won't allow a program that uses the QT library in the OpenCD, since the windows version is not libre."
    author: "Damjan"
  - subject: "Re: Aethera"
    date: 2005-02-01
    body: ">  It looks they are not very interested in promoting it.\n\nWho is \"they\" referring to.\nAethera's Kolab support is mentioned on both Aethera's and Kolab's website.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Aethera"
    date: 2005-02-01
    body: "Yes, 'they' means the Kolab project team.\nThe Aethera description from Kolab website is poor (nothing about OSX).\nThere is an old comment that Win version has problems which looks like 'please don't try' etc.\nThere are more comments but it's not the place to talk about.\n"
    author: "Eugen Constantinescu"
  - subject: "Re: Aethera"
    date: 2005-02-01
    body: "Please provide a patch to the website with uptodate information about Aethera.\nActually I am very happy about Aethera as the mere existance of many Kolab clients proves that the Kolab concept is pragmatic.\n\nLastly I have to admit that the Kolab website is far from optimal. Help is definetly appreciated. "
    author: "Martin Konold"
  - subject: "other Connector for Outlook"
    date: 2005-02-02
    body: "There is a german Company named \"Konsec\" (www.konsec.com) that has a Product similar to the Toltec Connector. I downloaded a Public beta and it seemed to work fine. Has anyone tried it in combination with the KDE Kontact?"
    author: "Thomas Wagner"
  - subject: "help"
    date: 2005-04-11
    body: "Is there any posibility to get an kolab2 setup from the beginning till the end?\nBecouse i have bin trying to do that for two days and I still havent done anything."
    author: "somainis"
  - subject: "Re: help"
    date: 2005-04-11
    body: "I'm afraid posting a comment to a two month old news article won't get you much attention from people who have the knowledge to answer your question (I don't, sorry).  I would try the kolab homepage http://www.kolab.org/.  There are links to documentation, to a support forum and to the mailing lists. \n\n"
    author: "cm"
---
KDE Dot News recently spoke with some prominent people from the <a href="http://www.kontact.org/">Kontact</a> and <a href="http://www.kolab.org/">Kolab</a> projects. We talked about how both projects got started and how they have evolved. Enjoy the first part of this two-part interview.


<!--break-->
<!-- About Kolab -->

<p align="center"><IMG src="http://www.kolab.org/images/kolab_logo.small.png" alt="Kolab logo" border="0"></p>

<p><strong>First of all can you explain who you are and what is your role in 
the Kolab/Kontact project?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: My name is Tobias Koenig, I have been doing KDE development for 5 years and since 3
years ago I've been the maintainer of <A href="http://www.kaddressbook.org/">KAddressBook</A>.</p>
    
<p><strong><em>Cornelius Schumacher</em></strong>: My name is Cornelius Schumacher. I started my KDE career by contributing 
to KOrganizer and I have maintained it now for something like five years. At <A href="http://conference2004.kde.org/">aKademy</A> I passed on KOrganizer maintainership to Reinhold Kainhofer. I also do various other things for KDE, one of them is trying to take care of kdepim generally.</p>
    
<p><strong><em>Steffen Hansen</em></strong>: My name is Steffen Hansen, I work for <A href="http://www.klaralvdalens-datakonsult.se/">Klar&#228;vdalens Datakonsult AB</A> and
have been on the KDE project more or less since the beginning. I am
the lead developer of the <A href="http://www.kolab.org/">Kolab server</A>.</p>
    
<p><strong><em>Bernhard Reiter</em></strong>:  I am a managing director of Intevation GmbH, which is a Free Software company. For Intevation I coordinated the Kroupware and Proko2 contracts.</p>



<p><strong>What are Kolab and Kontact, and how do they relate to each other?</strong></p>

 
<p><strong><em>Steffen Hansen</em></strong>: Kolab is a Free software groupware solution. The components are the
Kolab server and Kontact, which is the KDE Kolab client. There is also a Kolab web client in the works.</p>

<p><strong><em>Tobias Koenig</em></strong>: Kontact is part of KDE and integrates the main KDE PIM applications
(KMail, KOrganizer, KAddressBook, KNotes) into one GUI. So it has the appearance of MS Outlook/Evolution but 
the advantage that all components can still run as standalone applications. Kontact supports access to various groupware servers and one of them is the Kolab server.</p>

<p><strong><em>Bernhard Reiter</em></strong>: Kolab consists of several components:</p>
<ul>
<li>Kolab Solution Architecture</li> <!-- as pointed out by Martin Konold -->
<li>Kolab Server implementation</li>
<li>Kolab Client implementations (especially the KDE Kolab Client)</li>
</ul>        

<p>So I would say the current Kontact can act as KDE Kolab Client.</p>

<p><strong>How did the Kolab project start?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: Martin Konold, a KDE developer, gave a talk on LinuxTag 2002 
in Karlsruhe, which described a groupware solution built upon Open Source Software that can be used as
replacement for Outlook/Exchange.</p>

<p><strong><em>Bernhard Reiter</em></strong>: I have to comment on Tobias here. The Kolab Server 
was not designed to replace MS Exchange. We addressed this question in our <A href="http://kroupware.kolab.org/faq/faq.html#General3">Kroupware FAQ</A>:</p>

<p><em>"So no, it's not a goal to build a replacement for Exchange
or Outlook. On the software side we have assembled a solution that
solves some typical asynchronous groupware tasks like group
calendaring. Our usage of standards like disconnected IMAP and the
iCalender format is a new, innovative approach."</em></p>




<!--Do you work for any of these companies? -->


<p><strong>What exactly does a Kolab server do? Because sharing folders on 
 IMAP is not a new concept.</strong></p>


<p><strong><em>Steffen Hansen</em></strong>: Correct! The server uses well-known and available free software
components like OpenLDAP, Postfix, Apache and Cyrus IMAP server plus some
custom code to glue it all together.</p>


<p>The features you get from the Kolab server in addition to just using the
individual components (mail, web, imap,...) are:</p>

<p>a) Central configuration. The tweakable configuration parameters, user
database and so on are stored in one central place: The LDAP
database. Centralised administration of the Kolab server is done
through a web-based interface.</p>

<p>b) Multilocation support. Because of a) it is possible to set up
multiple Kolab servers that share configuration and user database. It
works by the use of LDAP replication. I see this as a very nice
feature for both supporting multiple weakly connected locations and
for being able to scale up to multiple servers to support huge
numbers of users.</p>

<p>c) Groupware specific functionality. The Kolab server contains code
that does automatic appointment handling for group and resource
accounts on the server. It also creates and manages freebusy lists for
all accounts.</p>



<p><strong>I heard about Kroupware and Proko2. What's with the names? How do these 
 projects relate to each other?</strong></p>


<p><strong><em>Tobias Koenig</em></strong>: The first project was called 'Kroupware', 
it consisted of Kolab1 and a KDE client, which was mainly a modified KOrganizer with embedded KMail.</p>

<p><strong><em>Steffen Hansen</em></strong>: 'Proko2' (which stands for "Project for Kolab2") 
had a main focus of enhancing usability of the client and adding features to the server to allow
groups of people to collaborate, do server based virus scanning, the 
multilocation feature and so on. </p>

<p><strong><em>Bernhard Reiter</em></strong>: The names refer to two contracts to deliver a groupware solution.
        Our companies did this with Free Software and started Kolab.
        Kolab is a Free Software project and thus has potentially 
        different players and goals than what the contracts might say, 
        so we needed to come up with names to decouple this 
        and being able to openly talk about it.
        For example we did not want to overpower the 
        naming of the client done by the KDE community.</p>

<p><strong>How many companies are involved with the Kolab project? I see the names 
Klar&#228;vdalens Datakonsult AB, Erfrakon PartG and Intevation GmbH passing by. 
Can you eleborate a bit on your cooperation?</strong></p>

<p><strong><em>Steffen Hansen</em></strong>: These three companies combined their efforts
to create a crossplatform groupware solution. But we need to mention a few others as well:</p>
 
<p>Radley Network Technologies is a South African company that make a 
plugin called <A href="http://www.toltec.co.za/">Toltec Connector</A> for Microsoft Outlook. It allows Outlook 
to use IMAP for mail and groupware data. Radley's has put a big effort 
in adding the Kolab-specific features to the Toltec Connector to allow 
it to interoperate with the Kolab server and other Kolab clients. Among 
other things, this includes support for reading and writing the XML 
format that Kolab clients use for internal storage of groupware data. 
Communication between clients is done with iCal/vCard.</p>

<p>A fifth company also deserves mention here: <A href="http://www.codefusion.co.za/">CodeFusion</A>. Like Radley's, 
they are also based in South Africa. CodeFusion offers professional 
Kolab support, they have helped out with implementing some 
server-features and they also work on the web client.</p> 



<p><strong>Microsoft Outlook uses the legacy TNEF format which is not capable of working properly with iCal. How did you guys find a solution for that?</strong></p>



<p><strong><em>Steffen Hansen</em></strong>: Well, let's first make a distinction between the format used for
communication and the format used for storage. Both Kolab1 and 2 do indeed use standard
iCal for inter-client communication (ie. sending invitations
etc.). Kolab1 also used iCal as the storage format to store your
calendar etc. on the imap server. Unfortunately is has not been
possible to create a solution that allows Kontact and Microsoft
Outlook to share the same storage based on iCal. iCal is too flexible
in some respects and not flexible enough in others. Therefore Kolab2
uses an easy to read/write and well documented XML format for the
internal storage. </p>

<p>Creating our own format has been frowned upon by
some, but I really think that it has been a big help for us to have
our own format that we could tailor to our specific needs to be able
to make both the KDE client and Outlook 'happy'. For the end user, the
visible result of using this new format is that he/she can switch back
and forth between Outlook, Kontact and the web client without having
to import and export data between them.</p>

    
<p><strong>Citadel/UX, a BBS system <A href="http://www.citadel.org/kolab.php">claims to be Kolab1 compatible</A> 
these days. Does this mean we can use Kolab compatible clients like Kontact with Citadel? </strong></p>

<p><strong><em>Bernhard Reiter</em></strong>: As far as I remember they are not fully compatible with Kolab1 because 
free/busy handling, directory server architecure and lack of legacy
support make it slightly different. Apart from that you probably
can use it with Kolab1 clients.</p>

<p><strong><em>Cornelius Schumacher</em></strong>: Citadel/UX is quite interesting because it has a 
long history and is a mature server system. There once was a <A href="http://www.shadowcom.net/Software/infusion/history.html">KDE client</A> for it which used KOrganizer as the calendar component. I would love to see some more interaction between the Kontact and the Citadel/UX developers.</p>

<p>One problem we have with groupware servers is that there are no really 
accepted standards for them. There are several drafts for standards and 
quasi standards set by implementations, but there is no generally 
accepted standard for access to groupware servers. It would be great if 
we could work into the direction of something like a common way to 
access groupware servers. We did make some very small steps but there is 
still a long way to go. I expect that this will become more important in 
the future as the current situation isn't really beneficial for 
anyone.</p>


           
<p><strong>Kolab stores mail folders using the Cyrus "maildir" format. Citadel stores 
everything in Berkeley DB. It seems you are using IMAP to access files on 
the server, which uses the filesystem and returns you that file. Tell 
me.. why didn't you use a database engine for this like Citadel/UX does?</strong></p>

<p><strong><em>Steffen Hansen</em></strong>: We strongly believe that you need a storage mechanism that fits the problem to
be able to scale up. Mail messages are variable sized independent
pieces of data stored in a tree-like folder structure. This looks a
lot like a filesystem to me. For an example, look at the <A href="http://graphs.andrew.cmu.edu/cgi-bin/hammer/mail-stats.pl">statistics for
Carnegie Mellon</A> (the home of cyrus imapd):</p>


<p>I seriously doubt that you can scale that high with a database-based solution.</p>

<p>Using mail messages for storing calendar information might seem odd at
first, but it is actually quite nifty. Having one calendar entry per
mail makes it easy to merge data when you have manipulated your
calendar from several places. Also, conflict detection/resolution can
be done nicely with this approach.</p> 

<p><strong>A lot of people complain about the installation procedure of Kolab. Has 
the situation improved? What made it such a pain to install anyway? Was 
it because of the <A href="http://www.openpkg.org/">OpenPKG</A> format? </strong></p>

<p><strong><em>Tobias Koenig</em></strong>: For Kolab1 the installation procedure was really a pain
but this has been improved in newer versions. Kolab2 Server is installable by simply executing one shell script. The reason was not the OpenPGK format, it's used for Kolab2 as well and has a lot 
of advantages. I guess the main reason was the limited time frame and
the developers didn't put much resources into implementing an installer but tried to get a stable
Kolab version finished.</p>

<p><strong><em>Steffen Hansen</em></strong>: Now the server installation has been streamlined a lot. After
fetching the install script, all you need to do is this:</p>


<pre>./obmtool kolab
 [wait while it builds and installs]
 
/kolab/etc/kolab/kolab_bootstrap -b
 [follow on screen instructions for initial setup]

/kolab/bin/openpkg rc all start
 [now the Kolab server is running. You can go to the 
  webgui and create users and so on]</pre>




<p><strong>Will Kolab in the near future be able to integrate more with an enterprise 
authentication system for single-sign-on? (for example OpenLDAP, Samba with LDAP support and maybe 
if you have Kerberos/Heimdal set up as well).</strong></p> 

<p><strong><em>Steffen Hansen</em></strong>: I can not make any promises, but this is something that has been on
our own wishlist for some time.</p>


<p><strong>Will Kolab have a mailman interface one day? Any plans for that?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: It's not planned yet. Nevertheless it's possible from the technical point of view, so <A href="https://mail.kde.org/mailman/listinfo/kde-pim">volunteers</A> are already welcome :)</p>


      
<p><strong>People on the MS Windows platform can use the Kolab server through the use
     of the Toltec connector. What would be a compelling reason to use Kolab 
      instead of let's say MS Exchange?</strong></p>
      


<p><strong><em>Steffen Hansen</em></strong>: Using a Kolab server instead of MS Exchange buys you better scalability,
higher reliability and a soft migration path away from proprietary software
on the desktop (because you can gradually introduce Kontact and it
will share data with Outlook).</p>

<p><strong><em>Tobias Koenig</em></strong>: Kolab is fast! Kolab2 will have the same functionality as 
Exchange, and it's Free Software. </p>


<p><strong>Another very interesting topic is the cooperation with the Free Software 
     project KDE. How did that work out? How were decisions being made with regard to 
     implementations?</strong></p>
    
<p><strong><em>Tobias Koenig</em></strong>: The cooperation went quite well, most of the staff from the 
company group come from the Open Source community, so you can talk and work with them like with 
every other Open Source developer. There were also three <A href="http://pim.kde.org/development/meetings.php">meetings</A>, organized by Intevation GmbH, 
where KDE-PIM developers and people from Klar&#228;vdalens Datakonsult AB attend to discuss the 
further steps for integrating Kolab support into the official KDE release. Some evidence of the 
success of these meetings is that KDE 3.3 was released with full Kolab2 support.</p>
    
<p><strong><em>Cornelius Schumacher</em></strong>: I'm speaking from the KDE perspective and have to say that it was an 
interesting challenge. I really appreciate that the Kolab project 
explicitely includes integration of the client code into KDE 
development. In practice there have been some problems because the 
Kolab people actually didn't have enough time to do this integration in 
a smooth way, but in the end, I think, we can be happy that the Kolab 
client has finally arrived in KDE.</p>

<p>It's a general problem of how to handle integration of third-party code 
into KDE as there are a couple of different interests which have to be 
balanced. Commercial contributors usually produce a lot of code which 
might be hard to properly review for Free Software maintainers doing 
this job in their spare time and which is developed with a different 
goal in mind from the code coming from inside of the project. The most 
important point is to talk with each other as early and as much as 
possible. There have been a couple of personal meetings between Kolab 
and KDE people and they have shown to be extremely valuable.</p> 

