---
title: "JLP's KDE 3.5 Previews (Part 2)"
date:    2005-08-13
authors:
  - "jrepinc"
slug:    jlps-kde-35-previews-part-2
comments:
  - subject: "Can't wait"
    date: 2005-08-13
    body: "It's all looking great.  Thank you"
    author: "Gerry"
  - subject: "KDE's look"
    date: 2005-08-13
    body: "kde seriously need a refresh for its default look. That shade of grey and blue is really boring, and Plastik is starting to show its age. Maybe a little touch here and there could make kde's look fresh again? Is there anything planned about that?"
    author: "ac"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "Lipstik?\n\nIt's like Plastik, with small but excellent improvements in certain critical points."
    author: "mhn"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "Calling anything theme related critical is a bit of a laugh ;)"
    author: "Anon"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "\"Kritical\".. \n\nNow there is a theme name, now we just needs the theme"
    author: "Allan Sandfeld"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "Linspire Clear."
    author: "Vik"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "try to use different color schemes and plastik will rock. using a lighter gray does the job. don't trust me, just try it :-)"
    author: "jumpy"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "I know, I know, I was talking about defaults :)"
    author: "ac"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "What it's really lame nowadays is the tray clock style.\nPlease change that to a smaller and more normal font, please.\nI can't believe we'll have to wait until KDE4 to have a decent one.\nIt's so simple to do!\n"
    author: "ggabriel"
  - subject: "Re: KDE's look"
    date: 2005-08-13
    body: "\"It's so simple to do!\"\n\nSo do it, ggabriel!"
    author: "Zippy"
  - subject: "Re: KDE's look"
    date: 2005-08-14
    body: "Right click -> Configure Clock -> Font\n\nProblem solved."
    author: "Trejkaz"
  - subject: "Re: KDE's look"
    date: 2005-08-14
    body: "Or even better: Replace the kde clock by styleclock (http://www.kde-apps.org/content/show.php?content=14423 ). It's themable and much more powerful and less cluttered."
    author: "Hans"
  - subject: "Re: KDE's look"
    date: 2005-08-14
    body: "This is already the plan.  Style Clock will be the default clock in kde4.  In fact the old clock isn't even in HEAD any longer. ;)"
    author: "p0z3r"
  - subject: "Why not use blending in Plastik?"
    date: 2005-08-13
    body: "Everything looks great. I'm just wondering, can't Smoothblend be integrated into Plastik (ie. keep everything the same in Plastik while smoothly blending the window background color into the titlebar)?\n\nThe reason I don't currently use Smoothblend is because I find the right-hand buttons a little too bulky."
    author: "Vlad C."
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "\"right-hand buttons a little too bulky\"?\nThe buttons can be resized in the control center along with everything else in the decoration.  Maybe I misunderstand you, but I'd love some feedback on it.\ncheers,\n-Ryan"
    author: "p0z3r"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "Smooth Blend could be nice.. But it's just waaay to bulky by default now.. The height could use a little trimming down ;)"
    author: "Bart Verwilst"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "So what should we base the defaults after?  1024x768, 800x600, 640x480 ?\nI personally use 1280x1024 which is maybe why it seems big for you.  Currently the default height is 30 pixels with 26 pixel height/width for the buttons.\nWhat do you use to make it look less bulky, and what resolution are you using?\n\n-Ryan"
    author: "p0z3r"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "I'm using 1152x864 and to me something like height of 19-21 pixels and 16-17 pixel height/width for the buttons looks best. The default are huge and not nice at all.\n\nGetting good defaults may become rather hard with all those strange resolutions out there. Is it possible to perhaps calculate the default/initial, from the height of the titlebar font or something. As that's what most of the other decorations use to calculate titlebar height."
    author: "Morty"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-14
    body: "I'm using 1024*768.. What i'm currently using is 20pixels height, with 15pix. buttons and 3pix frame. Looks just great :d"
    author: "Bart Verwilst"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-15
    body: "I use 1024x768 on my desktop (it can run 1280x1024 but hertz is to low for my test). 1280x800 on my laptop (a few new widescreen laptop, and this resolution is it's max)\n"
    author: "Mark Hannessen"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "then I have some feedback (sorry to interupt :D) - i'd love to see you try something to make the difference between the on and off state of the 'always on top' button more clear. the 'on all desktops' is way cool!"
    author: "superstoned"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "I still haven't found a solution to this yet, but it's been on my list of things to do before feature freeze on the 25th."
    author: "p0z3r"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-13
    body: "thanx. hope you can do it. you might remove the top bar on the button, just make a big triangle pointing upwards?"
    author: "superstoned"
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-14
    body: "I like Smoothblend's ability to customize title height, frame width, and button size \u0096 all options that Plastik doesn't have.\n\nBy bulky I was referring to the style/font in which the button symbols (? _ O X) are drawn. I tend to think the Plastik symbols are cleaner-looking, but that varies with personal taste.\n\nI've also noticed some other small drawbacks to Smoothblend:\n1) The area surrounding the app. icon on the left side doesn't change color on mouseover (in Plastik it does change color, but I don't think the change is noticeable enough even there)\n2) When pressing on a right-side button, the highlighting disappears and the button looks like any of the other unpressed buttons (in Plastik, the colors of the pressed button are different from the colors of the unpressed buttons)\n3) The X button doesn't acquire a red tinge when pressed (I think the red tinge has a usability value because it warns that the window will be permanently closed)\n4) The frame width can be changed, but it remains the color of the window background, making it impossible to distinguish where the window ends and where the border starts. I don't know if it's worth it, but you could even create a smooth gradient between the window color and the border color.\n5) I think for nomenclature consistency, you should rename \u0093Frame Width\u0094 to \u0093Border Size\u0094. \n6) The frame width seems to also affect the top of the title bar, when it should only affect the right, left, and bottom borders.\n7) As for defaults, I think a Title Height of 21 px. and a Button Size of 17 pixels would be good.\n\nWith a couple of fixes, I think Smoothblend would make a really good default theme in future releases!"
    author: "Vlad C."
  - subject: "Re: Why not use blending in Plastik?"
    date: 2005-08-14
    body: "Thanks for all the good points.\n1) I've thought about highlighting on hovering on the app icon, but instead I went with a slight movement when clicking on the app icon. The way plastik does it is it introduces a new box area behind it that wasn't there before, and then highlights that new box.  I just didn't like that effect too much personally.  It is worth investigating though from a usability perspective.\n2) Plastik does a negative for depressed buttons.  I wasn't quite sure what I wanted to do to indicate the button was clicked without copying plastik's feel.  I'm open to suggestions on this.\n3) I think I may change the close button to highlight with a red tint to some degree.  I didn't do it before b/c I didn't want to copy plastik.\n4) The frame width not only effects the borders(left,right,bottom) but it is also the clearance above and below the buttons/title.  Maybe they should be separated from each other an add a new config option?  Regarding the left,bottom,right not using the blending, I did that for more of a minimal feel.  I could try it in the way you suggest, but it would take away from what I was intending.\n5) Yes you're right.\n6) Hrm, probably a good idea to separate the top from the left,bottom,right.\n7) That seems to be what people are suggesting.  I'll change it to be smaller by default before the next 3.5 beta release.\n\nAs far as it becoming the default, I wouldn't want to start a debate on that one.  giessl (http://www.kde-look.org/usermanager/search.php?username=ceebx) has done a great job with plastik and is doing some work with KDecoration to make creating styles and decorations easier for developers. There's a reason it's the current default. ;)\n"
    author: "p0z3r"
  - subject: "SuperKaramba"
    date: 2005-08-13
    body: "The download/install problem was actually fixed the day before this review came out.  The fix is in SVN currently and will of course be in the next release.\nWe're also working on getting the uninstall and theme listing a little cleaner along with improving our documentation.\n-Ryan"
    author: "p0z3r"
  - subject: "ksysguard"
    date: 2005-08-14
    body: "That screenshot of ksysguard is out of date - I just changed it :)\n\n(Also it says \"powerfull\" instead of powerful)"
    author: "JohnFlux"
---
It looks like the <a href="http://jrepin.blogspot.com/2005/07/jlps-kde-35-previews-part-1.html" target="_blank">first part</a> of my KDE 3.5 previews was extremely popular. Much more than I could ever anticipated. I even got Slashdotted. Anyway, here is the <a href="http://jrepin.blogspot.com/2005/08/jlps-kde-35-previews-part-2.html" target="_blank">second part</a> of the look into the KDE's near future. Enjoy the tour!


<!--break-->
