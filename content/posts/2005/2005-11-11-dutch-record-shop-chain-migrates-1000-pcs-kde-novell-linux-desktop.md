---
title: "Dutch Record Shop Chain Migrates 1000 PCs to KDE on Novell Linux Desktop"
date:    2005-11-11
authors:
  - "rinse"
slug:    dutch-record-shop-chain-migrates-1000-pcs-kde-novell-linux-desktop
comments:
  - subject: "KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "http://www.kdedevelopers.org/node/1608"
    author: "Patcito"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "Too little, too late. Face is already lost."
    author: "Anonymous Coward"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "yeap."
    author: "eol"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "Novell: next time, better listen to the big crowd of customers \"outside\" than to the little crowd of ex-ximian people \"inside\" (no matter how loud they are)"
    author: "Thomas"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "Well, it wasn't just the ex-Ximian people: what about the \"quick buck\" shareholders that set this whole thing in motion? Anyway, see you in another ten years when you regain that market relevance, Novell!"
    author: "The Badger"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "http://www.heise.de/english/newsticker/news/66068\n\nHeise news in English. Supported equally my ass. 'Equal' on this context means Gnome is given 100x the money. "
    author: "Anonymous Coward"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "Too little, too late. Customers already learned that you can't trust these folks."
    author: "Anonymous Coward"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "The, or rather one, good thing about open source is that you don't have to trus them. I will continue to use SuSE since I find it to work best. But the day they are not the best anymore, e.g. by not supporting KDE, I simply switch to the next best and continue to use all my apps. There is no way SuSE can lock me in, especially with YaST open sourced."
    author: "AC"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-12
    body: "> The, or rather one, good thing about open source is that you don't have to trus them\n\nDepends on the size of the project. Some bigger projects (namely Telcos etc) are pretty well locked in due to migration costs. Linux is not Linux is not Linux."
    author: "Anonymous Coward"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "Went from being the center piece to a \"supported option\". I am waiting for a good distro to come along that does KDE. I am hopeful that Tex will do something interesting with pclinuxos."
    author: "a.c."
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "KDE will be the center piece and default on OpenSUSE.  Nothing much has changed really since OpenSUSE is what has replaced the old SUSE Desktop."
    author: "KDE User"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "Ok, sounds like you are with Novell. So what has changed? IOW, a company like Novell tends to do 2 steps forward, and 1 step back (of course for them, it was 2 steps back, and 1 forward). So, what was lost?"
    author: "a.c."
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "It's not clear what has changed and it's obvious that things will keep changing. \n\nThat's the thing that arouses the most anger and/or contempt, who wants to depend on the caprices of a dying giant?"
    author: "ac"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "I agree, we should not be satisfied until Novell understands that KDE should have its full commitment support.  Only their customers can make this happen by sending them a message."
    author: "KDE User"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE Linux"
    date: 2005-11-11
    body: "I think you are blinded by the talk about OpenSUSE and not relizing what it actually are, as i guess Novell hope people will in this case. Irrespective of how good OpenSUSE are it does not really matter, as it's not commercially supported by SUSE. The paying customers of SUSE are whats important, and their default. Not some comunity part, partly sponsored and developed by SUSE/Novell. In the same way that Fedora are largely irrelevant for RedHat's customers."
    author: "Morty"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE L"
    date: 2005-11-11
    body: "But does it really have to stay that way? What would stop someone to take OpenSUSE and offer the support and services for that distribution the same way SuSE used to do it? Hubert Mantel, are you looking for a new challenge?\n"
    author: "ac"
  - subject: "Re: KDE to Stay on Novell Linux Desktop and SUSE L"
    date: 2005-11-12
    body: "That's quite a different thing, and another discussion entirely. But I can't say I would be displeased if someone picks up OpenSUSE and does a Mandrake.  "
    author: "Morty"
  - subject: "Good news"
    date: 2005-11-11
    body: "I am dutch and I didn't know this. This is major news. This is very good major news. I think this opens up doors, since free record shop is one of the biggest suppliers of media in the netherlands. Good choice frs!"
    author: "mOrPhie"
  - subject: "Re: Good news"
    date: 2005-11-11
    body: "Yeah, I imagine all the Dutch people going 'Wow, what's this?' 'It's the Novell Desktop System'. \"Wow, let's buy that'\n\nI suppose the article proves that part of the NLD customers want KDE, but what good, if NLD doesn't give it to them? (Yes, they say it'll be there. But will it be good? Most all distros provide KDE packages, but how many make a decent job out of it?)"
    author: "ac"
  - subject: "Similar news from KDE based Linspire"
    date: 2005-11-11
    body: "Look here:  http://www.linspire.com/lindows_news_pressreleases.php\n"
    author: "reihal"
  - subject: "Re: Similar news from KDE based Linspire"
    date: 2005-11-11
    body: "Not really that similar, actually its even cooler. Linux getting retail floor space."
    author: "Ian Monroe"
  - subject: "Novell PR"
    date: 2005-11-11
    body: "Looks like Novell is trying to clear up the greatest PR disaster ever:\nhttp://www.novell.com/prblogs/index.php?title=kde_and_gnome&more=1&c=1&tb=1&pb=1\n\nHowever, they're not doing too well though. Quote:\n\"This change has no implication for current Novell customers.\"\n\nSure thing. People learn new desktops every day. Right?\n"
    author: "Anonymous Coward"
  - subject: "Re: Novell PR"
    date: 2005-11-11
    body: "is moving to gnome really *that* bad geez "
    author: "roger"
  - subject: "Re: Novell PR"
    date: 2005-11-11
    body: "Have you used GNOME?"
    author: "Alex"
  - subject: "Re: Novell PR"
    date: 2005-11-12
    body: "> Have you used GNOME?\n\nThe question is, did any GNOME users actually use GNOME or do they still wade from version to version with the increasing expectation that it's getting better one day ?\n\nI know a lot of GNOME users (not in terms of using it but in terms of prefering it) who still hope that it gets better one day and who still haven't lost the faith that the developers start wearing the clue hat."
    author: "ac"
  - subject: "Re: Novell PR"
    date: 2005-11-12
    body: "the common user doesnt care about which DE they use, all they care about is opening their email and viewing webpages...and other misc applications. So whether its Konq or Firefox or Epiphany, does it really matter?"
    author: "roger"
  - subject: "Re: Novell PR"
    date: 2005-11-12
    body: "> the common user doesnt care about which DE they use, all\n> they care about is opening their email and viewing webpages.\n\nSince the day when I first started using a computer I was interested in what I use, what the technical benefits are and so on. So, yes I do care what I use otherwise I wouldn't be commenting here.\n\nBut concerning your sentences, if a user don't really care which DE they use, then they probably use Windows anyways, they have all the tools, all the professional apps, tons of first class games and so on. The audience you are refering to usually isn't found on Linux or on Open Source in particular.\n\nAbout all the users who don't care, how comes there is such an enthusiast mega complaint going to be thrown infront of Novells front door ?\n\nSorry bud, but your reply is quite pointless and tiresome, since it's been repeated over and over for years and it has been nullified for the same timeframe too."
    author: "ac"
  - subject: "Re: Novell PR"
    date: 2005-11-13
    body: "<i>Since the day when I first started using a computer I was interested in what I use, what the technical benefits are and so on. So, yes I do care what I use otherwise I wouldn't be commenting here.</i>\n\nNovell doesn't sell software to idiots like you and could care less what you think.\n\n<i>But concerning your sentences, if a user don't really care which DE they use, then they probably use Windows anyways, they have all the tools, all the professional apps, tons of first class games and so on. The audience you are refering to usually isn't found on Linux or on Open Source in particular.</i>\n\nThe businesses that Novell targets don't give a damn about KDE or Gnome or open source any other kind of idiotic fanboy drivel.  They want apps.\n\n<i>About all the users who don't care, how comes there is such an enthusiast mega complaint going to be thrown infront of Novells front door ?</i>\n\nThe only mega complaining is by moron fanboys who are irrelevant to Novell's business.\n\n<i>Sorry bud, but your reply is quite pointless and tiresome, since it's been repeated over and over for years and it has been nullified for the same timeframe too.</i>\n\nYou are irrelevant as well as the other morons that are complaining.\n\n\n"
    author: "Rick"
  - subject: "Re: Novell PR"
    date: 2005-11-13
    body: "> The businesses that Novell targets don't give a damn about KDE or Gnome or open source any other kind of idiotic fanboy drivel. They want apps.\n\nOur company presents one of the (big) businesses Novell targets. Yes, we do give a damn about KDE and GNOME. We want a platform for which is efficient to build applications for, and currently only KDE/QT/C++ gives us that. Or to be more precise, it might have given in not too distant future. However, now with GTK we're back to eighties - and this is not acceptable.\n\nSo go buy Apples stock. Free desktop is quite unlikely to make it any time soon."
    author: "Anonymous Coward"
  - subject: "Re: Novell PR"
    date: 2005-11-13
    body: "> Novell doesn't sell software to idiots like you and could care less what you \n> think.\n\nNovell doesn't sell software at all.\nThat's why its profits dropped to near zero.\nAnd that's why they've lost 20% of SuSE's market share.\n\nAnd people don't trust whomever is calling them 'idiots'. Yes, that means you.\n\n> The businesses that Novell targets don't give a damn about KDE or Gnome or \n> open source\n\nYou mean to say that Novell is targeting businesses that don't give a damn about what it's selling. You are right on this point. Really. Novell has no idea how to attract interest or even preserve trust.\n\n> any other kind of idiotic fanboy drivel\n\nSince you're calling KDE admirers 'idiotic fanboys', what precisely tells you that a gnome fan insulting people on the dot does not look and smell like an idiot? (Yes, I know, you're no fanboy at all. You're only trying to educate the idiots. Spare us that. We're too stupid and we can't reach your level of wisdom or even understand what you mean to say.)\n\n>  They want apps.\n\n'Oh please target companies, look, we have this thing called Gnome, it's not the best out there, but please buy it because you don't care about it anyway, you care about apps'\n\nHahahahaha\n\nI like you, you're silly.\n\nIf nobody cares about DEs, then why switch DEs?\n\n> The only mega complaining is by moron fanboys who are irrelevant to Novell's \n> business.\n\nYeah, all those moron customers, employees and contributors which are obviously irrelevant. To Novell, that is.\n\n> You are irrelevant as well as the other morons that are complaining.\n\nSo why are _you_ complaining? Those 'irrelevant morons' complain because some well-intended guys who know what people don't care about are trying to ruin something that they do care about. But you - you're complaing that we don't want to allow that your beloved warlords drive the rest of the guys out. Now who's reasonable, and who's the instigator?\n\nBtw, if you think that I replied to you because you said something worth the trouble, you're wrong. I replied because I like stupid things on Sundays. However be aware that you shouldn't push this, because I'll probably not be in the same mood tomorrow (monday, work to do, you know :) )\n"
    author: "sd"
  - subject: "Re: Novell PR"
    date: 2005-11-13
    body: "> The businesses that Novell targets don't give a damn about\n> KDE or Gnome or open source any other kind of idiotic fanboy\n> drivel. They want apps.\n\n\nReally?\n\nhttp://lists.kde.org/?l=kde-promo&m=113190863900075&w=2"
    author: "ac"
  - subject: "Re: Novell PR"
    date: 2005-11-13
    body: "I love how Novell just proved all the GNOME fanboys wrong.  They have now realized that KDE must be supported 100%."
    author: "KDE User"
  - subject: "Re: Novell PR"
    date: 2005-11-12
    body: "> the common user doesnt care about which DE they use.\n\nOh, and assuming your sentences is right then may I ask why Novell didn't chose the technological better one as their primary Desktop (KDE in this case) the whole world speaks about how mature KDE is and how much ahead it is (even ahead of WindowsXP in many areas). I can also continue arguing that the common user don't care about license and bullshit politics either - but I bet you don't have ears for that."
    author: "ac"
  - subject: "Re: Novell PR"
    date: 2005-11-12
    body: "\"the common user doesnt care about which DE they use\"\n\nThey do when the desktop environment isn't up to snuff and they go back to Windows ;-)."
    author: "Segedunum"
  - subject: "DEs are all about standardisation"
    date: 2005-11-13
    body: "If you are using KDE, yes. Because KDE's DE is much more unified and harmonised than the random set of apps that make up GNOME. Things like toolbars, and other complex GUI widgets are highly non-standard under GNOME; compare that with things like the KDE Find and Replace dialogs to see what a real DE looks like."
    author: "Shaheed"
  - subject: "Re: Novell PR"
    date: 2005-11-11
    body: "Yes, it really is.\n\nGNOME doesn't work the same way that KDE does, and I do not like the way it works. I have horrible trouble trying to do any decent file management in GNOME, and find most of their basic tools awkward.\n\nI want to be able to easily sort files between my FTP storage at work, my main storage at home, and my back-up list. In Konqueror, I just hit ctrl+shift+L and ctrl+shift+T and I have one view with three panes. About ten total keystrokes after that, and each one is an open folder and I can easily pass things between them. Last time I tried GNOME, I didn't get my panes. That might seem like a corner case, but I do that about every two days because it's easiest that way. Maybe GNOME can do this now, but I couldn't when I was still considering it as an option.\n\nI try to do some easy text-file editing under GNOME, and I end up wishing I had Kate still. Why? Because basically everything I do is in text files and it's wonderful to have fifteen open in an easy-to-sift-through sidebar all in one screen. Again, the pane splitting thing is wonderful, allowing me to easily work on one file while referencing another, or to look at one portion of a file while looking at another. Since Kate is open on my computer twenty-four hours a day (no, really, the last time I closed it was about three weeks about, and that didn't last long) and I average about ten files open (I currently have 22 open, seven of which I've need to use in the last two hours), it's kinda essential that they have a replacement. Again, maybe GNOME has one. Last time I tried it, the only option I really had for a text editor was Gedit, and it didn't give me panes and a list of opened files.\n\nThe other apps that I use constantly would in some ways be easy to replace, as with Amarok and Kopete, but switching over to Rythymbox and Gaim would still require some time and hassle, making actually switching over desktops irritating.\n\nFurther, I'd have to reset all my keybindings so that pressing the media key would give me a run command and the go key would switch virtual-desktops, and that's always a lot of hassle.\n\nThe only advantage switching to GNOME would give is that a few apps that are in GTK would fit in a little better (Inkscape, Gimp, and Gnumeric being the only apps that KDE has no superior alternative for that I've ever had cause to use). However, those are all less commonly used and work easily along with my current setup. By contrast, using a different file manager with a desktop is definitely more trouble."
    author: "jameth"
  - subject: "Re: Novell PR"
    date: 2005-11-12
    body: "Yes it is, and on their server products rather pointless. They're going to have to replace YaST, develop new graphical tools..... Years of effort for nothing.\n\nMeanwhile, you've got a lot of Ximian people at Novell trying to be King Canute as most of their employees and customers choose KDE as their desktop."
    author: "Segedunum"
  - subject: "Hmmm..."
    date: 2005-11-11
    body: "Perhaps the idiots in Novell's management division should take notice!\n\nI'm aware that they won't be completely phasing out KDE... yet, only deprecating it. However, the trend is clear. In fact, it started a long time ago when they did not have a default desktop. Once they make GNOME a default, KDE will take a backseat and it will surely be removed if Novell's plan goes well and their customers do not rebel. But I for one will REBEL."
    author: "Alex"
  - subject: "Re: Hmmm..."
    date: 2005-11-12
    body: "\"Once they make GNOME a default, KDE will take a backseat and it will surely be removed if Novell's plan goes well and their customers do not rebel.\"\n\nWell, then any distributor that does ship KDE as their default will have a competitive advantage. It will be lovely to see Novell trying to sell their NLD while everyone goes out and buys Suse Linux and Open Suse.\n\nSwitching to Gnome and GTK on their server stuff makes zero sense, simply because it is years of effort. Would you want to re-write YaST and all the graphical stuff for GTK. What would Novell do to cut down the time? Port Anaconda?"
    author: "Segedunum"
  - subject: "Re: Hmmm..."
    date: 2005-11-15
    body: "\"hmmmm\" indeed\n\nI have been using SUSE for 6 years. And Yast has been working in Gnome on SUSE all that time. \n\nYou should know that even KDE aplications like Konqueror and K3b work in Gnome, just as well as Gnome applications like Pan and Evolution work in KDE. And I am happy for that, because Pan is a much more advanced newsreader compared to Knode, just as well as K3b is a much more advanced CD/DVD-burner compared to the Gnome CD/DVD-creator. So I am glad I can use these apllications at the same time without switching desktops."
    author: "Guido"
  - subject: "Re: Hmmm..."
    date: 2005-11-13
    body: "Does Jan Holesovsky btw still work on KDE integration for the Openoffice Project?\n\nI fully agree. KDE has to care more about companies and build up alliances to defend its lead role in the Linux desktop market."
    author: "manes"
  - subject: "Re: Hmmm..."
    date: 2005-11-13
    body: "Yes, and 64 bit version."
    author: "Anonymous"
  - subject: "NLD with KDE??"
    date: 2005-11-12
    body: "Wait a moment, wait-a-moment, waitamoment please!\n\nThis article contradicts itself. It says NLD was used by the shop. But I know for sure that NLD ships with Gnome only. And all NLD screenshots you'll ever find online on a Novell web page will only show off Gnome proofing just that!\n\nC'mon! Why should this non-techie customer have defied the official Novell recommendation, and installed an non-supported desktop there. It just makes no sense.\n\nIsn't the KDE community inventing stories here?"
    author: "FaithlessThomas"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-12
    body: "Sorry, but you're wrong. The current NLD ships with both desktops and supports them equally. In addition, THERE IS NO DEFAULT DESKTOP for it; users select which they want at install."
    author: "Max"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-12
    body: "Wow, it seems all the FUD Ximian spreaded on NLD was succesful..."
    author: "ac"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-12
    body: "Feel the sarcasm - this was meant as a joke ;-)."
    author: "Segedunum"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-12
    body: ">Wait a moment, wait-a-moment, waitamoment please!\n\nOkay :)\n\n \n>It says NLD was used by the shop.\nYep\n\n> But I know for sure that NLD ships with Gnome only. \nHmm, did you ever bother to run NLD? It ships with both KDE and Gnome.\nYou could have known that, even if you never used NLD, because Novell just announced that they will continue to ship NLD with both Desktop Environments.. \"continue\" means that it already ships with KDE and that they will do that in the future as well.\nSpeaking about screenshots, look at these at osdir:\nhttp://shots.osdir.com/slideshows/slideshow.php?release=194&slide=15\n\n> C'mon! Why should this non-techie customer have defied the official Novell recommendation, and installed an non-supported desktop there. It just makes no sense.\nWell, probably because KDE was a better choise :)\nBesides, since when is an automation supplier (Noa automatisering in this case) non-techie?? Or did you ever hear about ICT departments that are non techie? And yes, there still are companies in this world that actually listen to their ICT-department when making choises :)\n \n> Isn't the KDE community inventing stories here?\nWell, you can check that easily:\n1 - the article is featured by a magazine, contact them or subscribe yourself to read the original article\n2 - there are 400 shops in Nothern Europe that run the system, visit one and look behind the sales counter what desktop is on the screen of the point-of-sale system.\n"
    author: "rinse"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-12
    body: "Thankyou, Rinse,\n\nfor your patient and friendly reply. It clarifies a _lot_ for me. \n\nSorry for my apparent doubts about your sincerity. However, believe me please, it was more of a surprise that made me write my posting than it was a real suspicion of you having made propaganda without substance.\n\nThe OSdir screenshots of NLD with KDE are really nice. I never saw Novell itself promote KDE with NLD. They don't have KDE screenshots of NLD on the Novell website. They seem only to be interested to sell Gnome with NLD. I wonder how many potential customers they have turned away by showcasing only one, and not both of their major desktops?\n\nI'm starting to understand the gripes of the KDE community now, being treated in such an unfair manner. I'll have a closer look at all the PR I hear from each of the two camps from now, and not blindly trust everything I hear from the official spokesmen.\n\nI am a Gnome user ever since I started to use Linux, 3 years ago. Every time I looked at KDE, it always behaved \"different\" to me as a person now being used to Gnome (but so did Gnome, when I came over from Windows).\n\nHumans are animals that like to stick to what they have learned, and don't like to relearn everything every other month (or even every other year). This is the reason why Windows is still so powerful -- people started with Windows, and they'll stick to it, unless there is a *very* strong reason to switch.\n\nI recently saw someone showing off a few really cool features on his KDE desktop (webdavs:// URLs used to open files from the FileOpen dialog, fish:// URL to directly save a HTML file to a webserver, and the beautiful kdeprinter, t oname a few). Maybe I will give KDE another chance in the near future.\n\nHowever, let me first congratulate you to your success with KDE in the record shop chain! I do not doubt that what you write is true.\n\n\n"
    author: "FaithlessThomas"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-16
    body: "when you open the url sftp://user@host in Nautilus under Gnome you get the same result as\nthe url fish://user@host in Konqueror under KDE (in fact fish is graphical sftp) :)\n\nThey're just both great desktops, really.\n\n"
    author: "Guido"
  - subject: "Re: NLD with KDE??"
    date: 2005-11-16
    body: "In fact you can use that type of url everywhere in Linux. Like saving an odt-file form OOo to sftp://user@host to save the odt-file directly to a server whixh alows  ssh acces (which is the default for most Linux-computers)"
    author: "Guido"
  - subject: "Gnome lost Novell 1000-seat migration. Keet it up!"
    date: 2005-11-12
    body: "I am currently working with the Spanish Ministry of Interior for a potential desktop migration to Linux. Our choice WAS Suse and KDE. We had done the trials, we had tested our configuration. Now we feel that we can no longer trust Novell to stick by us and offer 5 years of support.\n\nKDE was heavily tested and has better, faster applications, smaller memory footprint and was far more intuitive for our soon to be ex-Windows users. Unfortunately, we will have to redo a lot of the testing with whichever distribution we end up choosing. We plan to talk to Mandriva and Canonical.\n\nNovell seems unable to pick the superior technology and by failing to do so sends its customers the signal that they do not actually understand their own technology. Have they actually asked its customers what they prefer? Which technologies they are using to build their in-house apps? We were able to develop an in-house security cam recording application using Qt in less than 4 months and it works both in windows and Linux/KDE.\n\nIt is really hard to understand why a company would act against its own best interest. This leads me to believe that Novell's leadership doesn't understand its own technologies and has been blindsided by a bunch of people whose selfish agendas will run the company into the ground. \n\nFurthermore, it makes me think that the Gnome folks at novell do not have Novell's best interest at heart. If they did, they would admit that currently KDE is a far better platform. Eventually, the market will speak.\n\nThe day I believe that Novell has regained common sense will be the day that I trust my reputation to them again.  "
    author: "Anon"
  - subject: "Re: Gnome lost Novell 1000-seat migration. Keet it up!"
    date: 2005-11-12
    body: "I think, you should send your above statement to your local *as* *well* *as* to the US Novell management.\n\nThese people need to wake up. They need to be reminded that their 180 degree turn can't just be one that buys them time before trying to move again into the old direction. It must be one that needs to be sustained."
    author: "ac"
  - subject: "Re: Gnome lost Novell 1000-seat migration. Keet it up!"
    date: 2005-11-12
    body: ">> \"Furthermore, it makes me think that the Gnome folks at novell do not have Novell's best interest at heart. If they did, they would admit that currently KDE is a far better platform.\"\n\nWell, I always thought Miguel de Icasa was a Microsoft mole.\nThere's a famous picture of him and Nat Friedman dancing with a Microsoft manager in a hotelroom.\n\nI would explain a lot.\n"
    author: "reihal"
  - subject: "Re: Gnome lost Novell 1000-seat migration. Keet it up!"
    date: 2005-11-12
    body: "\"Have they actually asked its customers what they prefer?\"\n\nThey should ask their own employees with their own rollout :-). The vast majority of employees at Novell who haven't had any previous experience with desktop Linux, Gnome or KDE choose KDE. I've seen it at trade fairs and events. That's the point in the whole default changing. To get people to use Gnome they have to make it the default on unwitting people and installers. It's rather like a Microsoft strategy - get it pre-installed first!\n\n\"Eventually, the market will speak.\"\n\nIndeed it will, and has in many ways. These fanboys fail to realise that without *at the very least* the right technology, desktop Linux will simply not happen at all."
    author: "Segedunum"
  - subject: "And their sound-clips ...?"
    date: 2005-11-12
    body: "This is certainly good news.  If only the Free REcord Shop could now be persuaded to offer their sound-clips in something other than .wma .... :-)"
    author: "donnek"
  - subject: "Re: And their sound-clips ...?"
    date: 2005-11-12
    body: "LOL, yeah that would be nice. However, you can play .wma on Linux if you so desire."
    author: "Alex"
  - subject: "Re: And their sound-clips ...?"
    date: 2005-11-12
    body: "as far as i know you can only play wma legally on linspire\n\n"
    author: "j"
  - subject: "Well... actually..."
    date: 2005-11-14
    body: "> At first glance, a shop is not a place where you would expect to find KDE in the  > workplace. \n\nFunny, I was picking up a new pair of glasses at Pearl (a dutch optical shop), and I was amazed to see a terminal with a KDE desktop behind the counter. \n\n-- \n\n"
    author: "hobbezak"
  - subject: "Re: Well... actually..."
    date: 2005-11-14
    body: "He, that's really cool :)\nperhaps we can create a new succes story around this :o)\n\nRinse"
    author: "rinse"
---
At first glance, a shop is not a place where you would expect to
find KDE in the workplace. Yet the Dutch <a href="http://www.freerecordshop.com/">Free Record Shop</a> is
deploying it on a large scale as the operating system for
their point of sale systems. According to the supplier <a href="http://www.novell.com">Novell</a>,
it is one of the application areas where simple and restricted functionality is
required, leading to a breakthrough for GNU/Linux on the PC.  An article from
<a href="http://www.automatiseringsgids.nl">Automatiseringsgids</a> magazine is translated below.









<!--break-->
<div style="margin-left: 1em;">
<p>The 'Free Record Shop' is a chain of shops in the Benelux countries
along with a sister organisation <a href="http://www.vanleest.nl">Van Leest</a>. The original Dutch company also has shops in <a href="http://www.freerecordshop.no">Norway</a> and <a href="http://www.freerecordshop.fi">Finland</a> with about 400 shops in total selling CDs, DVDs and computer games. ICT Manager Ton Arrachart describes their core activity as 'distribution of content in the area of home entertainment'. In the past every division of the company had its own ICT department, but now they
have all been merged into the main office in Capelle aan den IJssel. The central ICT department works as a shared services centre for all divisions.</p>

<p> "We didn't focus on cutting ICT costs, on the contrary, the
headcount has grown", according to Arrachart. His department now
has 14 employees in house, and 5 in the field. The strategy is
aimed at doing only the essential ICT business in house and to
outsource the rest.</p>

<p>The Free Record Shop has been using PC cash registers since the
80's, when the founder Hans Breukhoven deployed software for shop
automation from <a href="http://www.anoa.nl/html/mainorg.html">Anoa Automatisering</a> of Tilburg, running <a href="http://www.sco.com/">SCO UNIX</a> on Intel CPUs. Around 2002/2003 they decided to move to GNU/Linux,
SCO's new licensing policy being one of the reasons cited. At
first they tested Red Hat Linux but found it did not meet their
requirements. Arrachart says "We wanted to provide a supported
service, a managed point-of-sale environment. Red Hat only
provided support from the Open Source community, which wouldn't
have been usable in the context of a service level agreement. Red
Hat acknowledged this issue and suggested using their Linux
Enterprise Edition, but we had some cost concerns about that
approach."</p>

<p>Anoa then offered SuSE Linux as alternative, and Cappelle
assigned him to port the UNIX application to Linux.  For
financial reasons, the Finnish division had already decided to
migrate from UNIX to Linux. Because of that, the first shops in
Finland were already running Red Hat Linux, but in the future
they would move to Novell too.  The whole setup was aimed at
simplicity and effectiveness. "The cash register only has one
task: billing and showing stock", Breukhoven says, "an
efficient environment was wanted. We did not object to a
graphical interface, but it needed to be stripped down as far as
possible and Windows was not an option..". He continues, "..as
then you would be subject to Mr Gate's licensing policies, and it
would have also meant more investment in hardware."</p>

<p>The new cash register PCs run a stripped down version of the
Novell Linux Desktop. According to Arrachart, although all
essential components are present, a great deal of software has
been stripped out of the KDE Desktop GUI.  The applications
remaining include a PDF viewer and X Server, with KDE's Kiosk
mode and associated admin tool being used to lock down the
configuration.  Access to the central applications is provided
via a web browser.  At first, the shop employees
will only get the base packages so email using a separate
application won't be possible, although it can still be done via
a web portal.</p>

<p>Each shop has about 2 or 3 PCs; one is the head console and can
be used by the shop manager to access the central server based
applications. The other PCs are used as cash registers.</p>

<p>At the moment, almost all shops in The Netherlands and Belgium
already use the KDE Desktop. After that phase is complete, the
migration team will go to Norway and Finland to migrate the PCs
used by the Free Record Shop and Bravo chains. "It's a fun
project" says Arrachart, "We can show that you can save costs
with ICT, while at the same time allowing greater possibilities
in the way the shops are organised."  All the cash registers in
the Netherlands Free Record Shops are geared towards simplicity
and effectiveness. The Head of ICT concludes "so Windows is not
a viable option."</p>

</div>

<p>The original article can be found <a href="http://www.automatiseringsgids.nl/news/default.asp?artId=18691">at automatiseringsgids</a>, membership subscription required.</p>






