---
title: "KDE at Paris Solutions Linux 2005"
date:    2005-01-27
authors:
  - "cmiramon"
slug:    kde-paris-solutions-linux-2005
comments:
  - subject: "Have a nice expo!"
    date: 2005-01-28
    body: "I wish I was there to see all the KDE-francophone team members. I wish you all success in demonstrating KDE. Well done to the fr translation team and to all the i18n teams that make it possible to see KDE so widely spread.\nAnd if you have a little time, go to the Lea booth and salute my friend Jiel whom I met in Montreal last year!\nI'm waiting for some pics, do you plan to invite another celebrity for tea? (reference to last year Stallman's tea party)."
    author: "annma"
  - subject: "Konqui needs fresh blood!"
    date: 2005-01-28
    body: "<p>The French KDE Team needs your help. Check out the <a href=\"http://lists.kde.org/?l=kde-promo&m=110683868726545&w=2\">kde-promo mailinglist</a> .</p>\n\n<p>Anyway I hope KDE-France is having a succesful event and show us the pics :) </p>\n\n<p>Ciao' </p>\n\n<p>Fab</p>"
    author: "Fabrice"
  - subject: "Well Done"
    date: 2005-01-28
    body: "Must say i love KDE! Doing a great job!!\n\nPlease keep it up!\n\nLook forward to the pics :D\n\nClair"
    author: "Clair Ruins"
---
<a href="http://www.solutionslinux.fr/fr/index.php">Solutions Linux</a> is the annual Parisian Linux and Open Source exhibition. The event runs in the Cnit exhibition center in La Défense from February 1 to February 3. The KDE Team will have a booth in the <a href="http://www.solutionslinux.fr/fr/exposer_associations.php">Pavillon des associations</a>.
Come to meet developers, translators and other KDE fellows and watch the upcoming KDE 3.4 demonstrations.

<!--break-->
