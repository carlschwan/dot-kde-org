---
title: "Success Story: KDE for Science"
date:    2005-03-04
authors:
  - "mrenner"
slug:    success-story-kde-science
comments:
  - subject: "Interesting...some question though!"
    date: 2005-03-03
    body: "This is very interesting. I first heard of this kind of setup with HP's Mandrake OEM offering some time ago. Is there a way to convince the [kernel] or distro packagers to include this patch, so that an appropriate setup can be selected during installation?\n\nSchools in environments where dollars are scarce would find this very useful. It even sounds better than LTSP.\n\nHow many keyboards/mice can the kernel take? \n\nWhat about the length of the keyboard/mice cables? I have never been able to find a mouse with a sufficiently long cable."
    author: "charles"
  - subject: "Re: Interesting...some question though!"
    date: 2005-03-03
    body: "I believe the entire console part of the kernel is to be redesigned, so the ruby patch will not be accepted, but similar functionality will be in the kernel in the future (post 2.6). \n\nThis setup is also useful at home if you have several family members, or it could be useful in a lot of offices since most officecomputers is mostly used for office apps + email, etc. I guess one could even do 3-4 clients per computer in an office environment. \n\nI dont know what the limit is on number of keyboards/mices, etc, but since those are USB devices and I bet the kernel can handle well above 10 USB devices, it should be sufficient for at least a 4 console setup, or more. \n\nSince the computer does not have 10 USB ports (normally) you can use USB-hubs - and that will make the effective cable length longer. (for example, you can use 1 USB hub for each console)"
    author: "Magnus"
  - subject: "Re: Interesting...some question though!"
    date: 2005-03-04
    body: "If you're interested in the nuts and bolts of the new console implementaion take a look at Jon Smirl's \"OLS and console rearchitecture\" post to the LKML (http://tinyurl.com/3vcs9).  The plan is to move as much of the code as possible to userspace.  Unfortunatly as far as I can tell nothing has been put to code yet."
    author: "ac"
  - subject: "I am using this at home"
    date: 2005-03-03
    body: "I am using this at home since december 2003. \n\nMy computer is an Athlon XP 2000+ with 512 MB RAM. I have one geforce 4 AGP and one nvidia TNT 2 pci graphic card. By using the backstreet ruby patch I have 2 X consoles on the same computer. Currently I have only one sound card, but it is possible to use different sound cards for each X-server for the complete two-desktops-in-one-computer feeling. \n\nI am using default debian with gdm as login and kde as desktop environment and this was really easy to set up, just a litle configuring in gdm to start 2 gdm clients using different X config files (one for the AGP card with one set of keyb+mouse, and one for the PCI card with the other set of keyb+mouse). Anyone can do this at home, and its quite fun. Apart from the kernel beeing patched with the ruby patch, nothing else on the system is patched. \n\nThe coolest part? You can play some 3D games, such as quake II on both consoles 3d accelerated with low ping. :) Not all games work in this setup due to bad network code (for example, two doom legacy clients cannot be run on the same computer beacuse both clients wants to use the same network port). \n\nAnyways, I am really looking forward for a dual PCI express motherboard so I can use this multu-console setup with 2 fast graphic cards - since at the moment the TNT 2 is a bit slow. \n"
    author: "Magnus"
  - subject: "2x PS/2 keyboards?"
    date: 2005-03-03
    body: "...\n\"Keyboards:      2x PS/2 with US layout,\"...\n\nthis one thing is new to me. Can you attach a keyboard to a PS/2 mouse connector? Most time I undeliberately interchange the keyboard and mouse plugs, both of them cease to work.\n\nOtherwise, I wonder how you managed to clean the users' sessions after they left the surf station. I hope they did not see the leftovers from previously logged-in visitors in the browser history, just to name an example.\n\nSalut\n\nMark"
    author: "kmsz"
  - subject: "Re: 2x PS/2 keyboards?"
    date: 2005-03-03
    body: "Yes, with the ruby patch. "
    author: "Magnus"
  - subject: "Re: 2x PS/2 keyboards?"
    date: 2005-03-07
    body: "Both PS/2 ports are simple low-speed serial lines.\nWith the Linux kernel 2.6.x, it doesn't matter\nwhich device is plugged into which port.\nOf course, you have to convince your BIOS to accept them.\nE.g. an old ASUS P2B-D (dual P2/PIII) board did not\ncare about the mouse port, both ports were usable\nwith either keyboards or mice. My one year old\nMSI K8T Neo Athlon64 motherboard stops at boot\nbecause it detects the keyboard on the mouse port.\nIt's just an annoyance that I have to press F1\nto continue booting with it.\n\nIt's the ruby patch that separates the keyboard inputs\nto different consoles, the plain 2.6.x kernel still\nunifies them, regardless of whether they are USB or PS/2."
    author: "Zolt\u00e1n B\u00f6sz\u00f6rm\u00e9nyi"
  - subject: "If you don't need the extra devices..."
    date: 2005-03-04
    body: "Linux users can log into their local account, hold down CTRL+F2, supply the credentials of another local user, type startx -- :1. To switch back to the first user, hold down CTRL+F7. To switch back to the second user, hold down CTRL+F8. If you need a third user, hold down CTRL+F3, type startx -- :2. This users session is now at CTRL+F9.\n\n"
    author: "Anonymous"
  - subject: "Re: If you don't need the extra devices..."
    date: 2005-03-04
    body: "KDE 3.4 will automate doing this for you (and it's CTRL-ALT, not CTRL).\n\nMakes switching users nice and easy :)"
    author: "Simon Farnsworth"
  - subject: "Re: If you don't need the extra devices..."
    date: 2005-03-08
    body: "This is not about switching users. They have a setup where 3 persons can use one computer at the same time - and that isn't possible with your method"
    author: "Peter Wiersig"
  - subject: "udev for mouse devices"
    date: 2005-03-04
    body: "Nice to see such multi-head setups. It's about time. Concerning the problem with the mouse association. This can be solved via udev configuration which allows to give every mouse a unique name. So you can even reboot happily..."
    author: "Seiden Tiger"
  - subject: "Re: udev for mouse devices"
    date: 2005-03-05
    body: "Only that udev is not used everywhere as much as it should. I guess that a year from now, when all big distros use udev and 2.6 hotplugging will work much better. The tech is there, the migration has still to be done in most cases."
    author: "anon"
  - subject: "Totally GNU should not copy and fake too much"
    date: 2005-03-10
    body: "Think about why KDE can't replace Microsoft's X-Windows. GNU should focus on provide an open architecture for all software, if Microsoft's Windows don't provide the standard for others such as KDE to compete with theirs, it's illegal. GNU needs lawyer much more than developers."
    author: "Allen"
  - subject: "Re: Totally GNU should not copy and fake too much"
    date: 2005-03-10
    body: "Who is that GNU guy and how is he in involved in KDE and what did he copy and fake?"
    author: "ac"
---
How do 250 scientists spend their coffee break at a scientific conference? They use KDE!  There was a scientific conference, the <a href="http://www.twk.tuebingen.mpg.de/twk05">Tübinger Perception
Conference 2005 (TWK)</a> at the end of February. Many scientists from all over the world joined to take part in the symposia and the poster sessions. Because many scientists are addicted to their e-mail, the organizers installed an Internet Corner with 3 KDE Terminals.



<!--break-->
<p>
This setup was a little bit special, because one PC served 3 GeForce graphic adapters with
2 TFTs and 1 old SGI monitor, 3 keyboards and 3 mice attached. Under normal circumstances, you can only attach
1 monitor, 1 keyboard and one mouse to a computer. If you attach more than these, all keyboards and mice are
combined into one!
</p>
<p>
But a modern PC is idle most of the time, especially for internet tasks, and the processor load is not as important
as the network speed. So the idea was to setup one PC with several graphic adapters serving several terminals
with KDE as the GUI.
</p>
<p>
To run a multi-localuser X system is not a big problem. The so-called <a href="http://www.ltn.lv/~aivils">
backstreet ruby</a> provides this functionality as a simple kernel patch for Linux 2.4 and 2.6.  As soon as the system is running, a <i>sysctr</i> command breaks up the input
device binding. To speed up the whole system, we also applied the
<a href="http://www.zip.com.au/~akpm/linux/schedlat.html">low latency patch</a>, but didn't measure its effect. 
</p>
<p>
The 3 X servers in our setup were started automatically by a script at boot time. No login was
necessary, KDE starts up without any user interaction but with unprivileged user rights. The KDE version was 3.3.1.
This is not the most current version but, as this setup was done in 5 hours, there was little time left to update program
versions from the existing state. Anyhow: 3.3.1 is fancy enough for the scientists to read online tickers and ssh to their university account to start Pine!
</p>
<p>
The biggest challenge for us was to find out which keyboard and mouse were associated with which monitor. While this was a static
mapping, the mouse mapping changed with every system startup. This problem was not understood, but finding out
which of the 3 mice belonged to which monitor wasn't so difficult. Because the system was very stable, this was only necessary three times in total.
</p>
<p>
The participants loved the setup and many of them were very surprised to see one PC with 3 independent monitors.
Some of them had a little hesitation using the printer, although printing is very simple with <a href="http://printing.kde.org/">KDEPrint</a>.
<p>
Nobody reported problems with the desktop, although many of them use XP for their daily desktop work. The most
useful applications like <a href="http://www.mozilla.org/">Mozilla</a> and <a href="http://konsole.kde.org/">Konsole</a> (because Pine is still used by
many students), devices like USB-stick and CDROM, were represented by  desktop links. 
<p>
Nobody reported performance problems, although the machine wasn't very fancy:
<pre>
Processor:      P IV with 2.4 GHz
Memory:         1 GB
HDD:            80 GB
Graphics:       2x GeForce2 MX/MX 400, 1x  nVidia Quadro FX 500
NVidia-Version: 0.5336
Keyboards:      2x PS/2 with US layout, 1x USB with DE layout
Mice:           3x USB wheel mouse
Screens:        1x LCD 1024x768, 1x TFT 1600x1200, 1x CRT 1024x768
Sound:          Intel
Network:        10 MBits
Distribution:   Debian testing/unstable
kernel:         2.4.25 with backstreet ruby and low latency patches
</pre>
</p>
<p>
We want to improve our Conference Internet Corner next year with more terminals, more sockets for
USB-sticks, digicams and maybe a CD burner. And, of course, with a more current KDE version. Whether we should
use KDE's <A href="http://www.kde.org/areas/sysadmin/">Kiosk Mode</a> is not yet decided; we will test the kiosk mode first to see if it fits our needs.
</p>
<p>
With Linux, the backstreet ruby patch and KDE and we got a fine, sensational and cheap solution for accessing mails and other online resources at a conference.
</p>

