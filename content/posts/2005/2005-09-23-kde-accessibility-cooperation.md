---
title: "KDE Accessibility Cooperation"
date:    2005-09-23
authors:
  - "oschmidt"
slug:    kde-accessibility-cooperation
comments:
  - subject: "Heya"
    date: 2005-09-23
    body: "With respect to Nokey, I greatly appreciated the chance to develop it as part of the Google Summer of Code.  Thank you to Gary Cramblitt and Olaf Jan Schmidt for helping me out with this!\nAt the moment, Nokey may not be suitable for real use yet, because of problems with transparency and accessibility interfaces in Qt3.  But I'm really excited about continuing work in this area.  If you look at alternative onscreen text generation interfaces available today, they are mostly boring and uninspired (Dasher being the notable exception).  This is not a problem unique to *nix platforms, even for Windows there are tons of on-screen keyboards, none of which represent an ideal solution.\nI've used a lot of different interfaces with an eye tracker, and have found that the key criteria for an effective interface are:\n1.  Simplicity - less than 10 areas of interest on the screen\n2.  Inobtrusivity (is that a word?) - the interface shouldn't obscure the user's work.\n\nReproducing a qwerty keyboard on the screen is a terrible solution, and yet there are tons of applications doing exactly that.  Why would qwerty, which isn't even optimal for an able-bodied typist, be even close to optimal for a user with an alternative input device?  The only advantage that gives you is familiarity, and even that is only valid if your users have typed on a regular keyboard before.  The whole concept is silly.  \n\nI think the Nokey interface isn't quite there yet, but it will be much better by the time I've ported it to Qt4, and when AT-SPI is more widespread.\n\nRight now I'm working for UVATT (http://web.uvic.ca/uvatt/).  Our vision right now is to develop an interface that is not only for text input, but for complete computer control.  That is, that a disabled person using an eye tracker (or head tracker or Intellikeys device) could have complete control of the computer as soon as it is turned on.  Too much assistive software doesn't go that last step   and think about how a disabled user would start the software, or navigate their system.  That's what we hope to accomplish.  We target Windows, but we are evaluating Qt at the moment to replace MFC.  In that case we leave the option of porting to other platforms open.  \n\nWhew.  Longest comment ever.  Anyway, I am very excited about this stuff. :)"
    author: "Leo S"
  - subject: "Re: Heya"
    date: 2005-09-23
    body: ">  Inobtrusivity (is that a word?) -\n\"Unobtrusive\" is the work you are looking for :)"
    author: "cph"
  - subject: "Re: Heya"
    date: 2005-09-23
    body: "> > Inobtrusivity (is that a word?) -\n> \"Unobtrusive\" is the work you are looking for :)\n\nWell, to maintain parallelism, \"inobtrusivity\" or \"unobtrusivity\" are both quite good invented words. For a less awkward word, my top recommendations would be \"innocuousness\" or \"inconspicuosness\". Of course, changing \"simplicity\" to \"simple\" and going with \"unobtrusive\" is possibly the best option open."
    author: "jameth"
  - subject: "Re: Heya"
    date: 2005-09-24
    body: "Unobtrusiveness."
    author: "Robert"
  - subject: "Onscreen Keyboard!!!"
    date: 2005-09-23
    body: "> Reproducing a qwerty keyboard on the screen is a terrible solution\n\nYou're right Leo, it is very difficult for non-typist to find \"Alphabets\" on the *onscreen keyboard*, causing more problem than solving the problem of typing with keyboard.\n\nmy opinion: why not arrange things in Columns like this: (this can be improved considerably); logically arranging for faster typing using mouse\n\n-------------------------------------------------------------\nNumbers and   : [1] [2] [3] [4] [5] [6] [7] [8] [9] [0] [,] [.] [!] [?] | [+] [-] [*] [/] [=]\npunctuations\n-------------------------------------------------------------\nAlphabets  : [a] [b] [c] [d] [e] [f] [g] [h] [i] [j] [k] [l] [m]| [spacebar on the right side]\nLower case : [n] [o] [p] [q] [r] [s] [t] [u] [v] [w] [x] [y] [z]| [for easy]\n----------------------------------------------------------------------------| [access]\nAlphabets  : [A] [B] [C] [D] [E] [F] [G] [H] [I] [J] [K] [L] [M]| \nUppper case: [N] [O] [P] [Q] [R] [S] [T] [U] [V] [W] [X] [Y] [Z]| \n----------------------------------------------------------------|\nSymbols    : [`] [~] [!] [@] [#] [$] [%] [^] [&] [*] [(] [)] [_]| \n             [+] [|] [[] []] [;] [:] ['] [\"] [/]\n----------------------------------------------------------------|\n\nI'm missing function keys, HOME, INSERT, PAGEUP, etc., keys, you can fit it nicely. Also other languages can also be put like English keys, or as people have learnt the alphabet sequence.\n\nThe onscreen keyboard is meant for user's convenience, easy to work with one mouse button only. (because one of the user's hands could be injured or not present). thanks."
    author: "fast_rizwaan"
  - subject: "Re: Onscreen Keyboard!!!"
    date: 2005-09-24
    body: "The idea of the default keyboard layout is to cluster important letters in a center region. Just an alphabetical order is a bad idea. Just conisider, i.e. in the German language the letters ERNSTL are contained in most words. I want to have them close to each other avoiding long distance mouse moves. "
    author: "Sebastian"
  - subject: "Re: Onscreen Keyboard!!!"
    date: 2006-09-20
    body: "I use WinXP on-screen keyboard and I think its very good - it looks almost like standard qwerty keyboard and works fine for me. The only thing i miss is a possibilty to change layout the way i need (i.e. to make it shorter by getting rid of numeric section). I think it would be hard to get used to unusual layout. qwerty is the best."
    author: "Soul"
  - subject: "Onscreen keys Using Right Click to shift the Case"
    date: 2005-09-23
    body: "The Screen space is precious, so to save screen space we can use Left mouse button to enter these keys (small/lower case)\n\nAlphabets  : [a] [b] [c] [d] [e] [f] [g] [h] [i] [j] [k] [l] [m]| [Enter]\nLower case : [n] [o] [p] [q] [r] [s] [t] [u] [v] [w] [x] [y] [z]| [Shift]\n\nand using \"Right Mouse Button\" or Right-Click to \"Enter Capital/Upper case\"\n\nAlphabets  : [A] [B] [C] [D] [E] [F] [G] [H] [I] [J] [K] [L] [M]| \nUppper case: [N] [O] [P] [Q] [R] [S] [T] [U] [V] [W] [X] [Y] [Z]| \n\nthis will save much screen space, and the onscreen keyboard can be used as a small bar (toolbar or something similar.)"
    author: "fast_rizwaan"
  - subject: "Screen magnifier"
    date: 2005-09-23
    body: "\"Development of a new X.Org based screen magnifier\"\n\nThere will still be an vanilla-X11-based screen magnifier available as well, right? Or at least have this magnifier support vanilla-X11 but with reduced eyecandy? Not every KDE platform has X.org by default, and not every user has the authorization to change it. It seems to me more productive to keep the old magnifier than to lock people out from this functionality."
    author: "Brandybuck"
  - subject: "Re: Screen magnifier"
    date: 2005-09-24
    body: "<blockquote>There will still be an vanilla-X11-based screen magnifier available as well, right?</blockquote>\n\nYes, of course.\n\n<blockquote>Or at least have this magnifier support vanilla-X11 but with reduced eyecandy?</blockquote>\n\nThis is not about eyecandy. The X.Org Composite extention is needed to implement some important features for partially sighted people which are simply impossible using vanilla-X11.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
  - subject: "Re: Screen magnifier"
    date: 2005-09-24
    body: "> partially sighted people\n\n\"partially sighted\"? This is not a corporate website. Let's call things what they are. \"partially sighted\" sounds as if you have trouble taking aim at them.\n"
    author: "PC Squad"
  - subject: "Re: Screen magnifier"
    date: 2005-09-25
    body: "What exactly is the term you'd like to use?\n\nAs it is, your message seems very offensive to me, but I might be just misunderstanding you."
    author: "Henrique Pinto"
  - subject: "Keyboard Accessibility Audio Feedback"
    date: 2005-09-26
    body: "I realize many people in the 'nix world view keyboards differently than people using Windows and Macs, but one KDE accessibility tool I have yet to find that has existed in Windows for 15 years is a simple audio beep when toggling Caps Lock, Num Lock, and Scroll Lock. Some people debate the usability of those keys, but many people still find them useful.\n\nI think GNOME has this tool and I wonder why KDE does not. Additionally, the Insert Key needs the same toggle beep. That key often is used to toggle between Insert and Overwrite mode. Yes, I realize that with 'nix users Shift-Insert is still popular for pasting text, and the audio beep should sound only when the Insert key is toggled without a modifier key.\n\nSlowly the past couple of years I have been migrating to GNU/Linux and I especially enjoy KDE, but the lack of this audio feedback often frustrates me. Seems if the code already exists in GNOME that adapting that code to KDE should be straightforward for the heady KDE developers. I hope. I wish. With each release I keep waiting for this accessibility tool. Will version 3.5 finally be the one or will I continue to wait?"
    author: "D.A."
  - subject: "Re: Keyboard Accessibility Audio Feedback"
    date: 2005-09-26
    body: "I think your chances of getting that wish fulfilled would increase if you headed over to http://bugs.kde.org/ and entered it as wishlist entry.\n\nJust register and report it as bug with severity \"wishlist\" (or vote for the already existing entry, if any). \n\n"
    author: "cm"
  - subject: "Re: Keyboard Accessibility Audio Feedback"
    date: 2005-09-26
    body: "The feature is already part of kdeaccessibility 3.4. It shows the modifier state in panel, and it can be configured to beep when the state changes. Or it can open a passive popup and read out a message via text-to-speech.\n\nJust add the Keyboard status applet to the panel. Then you can enable the beeps or passive popups in KControl (Sound & Multimedia / System notifications). Text-to-speech can then be configured under Regional & Accessibility / Speech Synthesis.\n\nOlaf\n"
    author: "Olaf Jan Schmidt"
---
Recent weeks have seen a lot of cooperative activity between the <a href="http://accessibility.kde.org">KDE Accessibility Team</a> and various other Free Software accessibility teams. The <a href="http://accessibility.freestandards.org/">Free Standards Group Accessibility Workgroup</a>, KDE Accessibility and <a href="http://developer.gnome.org/projects/gap/">GNOME Accessibility</a> teams have now released a <a href="http://accessibility.freestandards.org/a11yweb/forms/soi.php">joint statement</a> describing some of this cooperation.  <i>"We believe users who are persons with disabilities should be empowered to choose technologies from any and all environments which provide accessibility just as other desktop users today routinely use a mix of technologies from different desktop environments. Our goal is seamless interoperability."</i>



<!--break-->
<p>In addition to participation in the accessibility workgroup of the Free Standards Group, KDE Accessibility also closely cooperates with a number of other accessibility teams. During the KDE Conference, for example, we worked together with the <a href="http://www.brailcom.org/">Brailcom</a> to make KDE Accessibility interoperate well with applications for the console.</p>

<p>Other areas of activity and cooperation include:</p>
<ul>
<li>Participation in the Google Summer of Code to support Leo Spalteholz in developing a new application for eye trackers (<a href="http://developer.kde.org/summerofcode/nokey.html">project NoKey</a>)</li>
<li>Development of a new X.Org based screen magnifier</li>
<li>Foundation of the Freedesktop.org Accessibility Initiative</li>
<li>Foundation of the Unix Accessibility Forum during last years KDE Conference, and cooperation with the German accessibility user group Linaccess to organise it again during LinuxTag this year</li>
<li>Cooperation with <a href="http://www.openusability.org">OpenUsability.org</a> to develop accessibility guidelines for KDE developers</li>
</ul>

<p>All of this will contribute to making Unix in general and KDE in particular the software of choice for disabled users.</p>


