---
title: "Interview with KHTML Developers Ivor Hewitt and Allan Sandfeld Jensen"
date:    2005-06-15
authors:
  - "Anonymous"
slug:    interview-khtml-developers-ivor-hewitt-and-allan-sandfeld-jensen
comments:
  - subject: "Other Developers?"
    date: 2005-06-15
    body: "First off, I'd just like to thank all of the current and former members of the KHTML team.  Thank you for a great browser!  Believe me, your work is _really_ appreciated by all of us!\n\nI would have liked it if Germain Garand was also interviewed.  Of all the KHTML devs, he seems to be the one that receives the least publicity.  There wouldn't happen to be an interview with Germain in the works, would there?  ;-)"
    author: "KHTML Fan"
  - subject: "Re: Other Developers?"
    date: 2005-06-15
    body: "Indeed it barely scratches the surface, and a more in-depth intro would have been good, along with making it clearer that Allan did all the hard work! :)\nAlthough as it says he is really just approaching this from the point of view of the over-publicised Acid2 test rather than KHTML development as a whole."
    author: "Ivor Hewitt"
  - subject: "Re: Other Developers?"
    date: 2005-06-16
    body: "I'm the author of the interview.  I was mainly interested in seeing how Apple's interaction with KHTML affected KDE development (the whole notion of a company forking open source interested me).  Hopefully that came out a little in the interview.  (It does seem that the noise that was made around Apple's fork of KHTML will benefit both Apple and KDE in the future....)\n\nI'm willing (and planning) to do more interviews in the future.  Currently planning to do one about sound on linux.  If anyone has any good ideas or suggestions feel free to post here or on my blog.\n\nmatt"
    author: "foobie"
  - subject: "Re: Other Developers?"
    date: 2005-06-15
    body: "Actually, I enjoy remaining silent.\nBut thank you for the kind thought toward the whole team."
    author: "Germain Garand"
  - subject: "Now, does this work...?"
    date: 2005-06-16
    body: "Anyone fancy a version of UNIX written in JS? :-)\n\nhttp://www.masswerk.at/jsuix/"
    author: "Richard Moore"
  - subject: "Re: Now, does this work...?"
    date: 2005-06-16
    body: "Cool stuff.\n\nDoesn't work for me in Konqi from 3.4.1, though.  \n\nBut in the svn version it works (except the keyboard, I have to use the on-screen one). \n\n"
    author: "cm"
---
<a href="http://panela.blog-city.com/">Matt Harrison's blog</a> features a <a href="http://panela.blog-city.com/interview_with_kde_developer_ivor_hewitt.htm">rather interesting interview</a> with two <a href="http://khtml.info/">KHTML</a> developers, Ivor Hewitt (of AdBlocK fame) and Allan Sandfeld Jensen (of ACID2 fame). They talk about how they got involved in KDE, the projects they've worked on and the situation between KDE and Apple.




<!--break-->
