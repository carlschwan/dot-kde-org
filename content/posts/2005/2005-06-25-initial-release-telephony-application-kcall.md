---
title: "Initial Release of Telephony Application KCall"
date:    2005-06-25
authors:
  - "ebrucherseifer"
slug:    initial-release-telephony-application-kcall
comments:
  - subject: "integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "I've heard, that the Kopete team is planning to support Skype in future. If KPhone integrates with Skype, Kopete and Kontact, KDE will be the perfect PIM solution for everybody. :)"
    author: "Patrick Trettenbrein"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "Skype is garbage for 2 major reasons (not to mention lesser reasons):\n- It is not Free\n- It does not use standard VoIP protocols-- so it does not interoperate with any other VoIP programs.\n\nOf course, most Kopete users use it for non-standard protocols, so I wouldn't find it *overly* surprising..."
    author: "Luke-Jr"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "Skype is not garbage for the following reason:\n\n- It seems to be the only damn thing that Just Works on both Windows and Linux, behind firewalls, etc."
    author: "KDE User"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "It doesn't matter how easy something is to use if it doesn't work properly (read: interoperability) or is immorally licensed (read: not Free)"
    author: "Luke-Jr"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "Luke, I'll forgive your troll (read: propaganda), given your young age (read: junior)."
    author: "KDE User"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-26
    body: " Luke didn't troll.. \n\n What ever you like it or not, lots and lots of people in this community focus on the ethics/politics of Free Software. That you personally belong strictly in the camp which is only in it for the technical benefits does not give you the right to label other people's oppinion as \"propaganda\"... Let alone to be condecending and to deminish a viewpoint as something assiciated with only with the young. \n \n FYI I used to be in the \"Linux is Open Source\" camp, but have gradually slided over to the \"GNU/Linux is Free Software\" camp. This transition slowly happened when I started to give heck about the by-gone-flame-wars and then actually listen to what \"the other camp\" really has to say. It might, in this context, be worth pointing out that I've been in the OSS/FS communities since Redhat 5.1... and too that I am pushing 30 years.\n\n I [and alot of others from the FSF camp] respect that not everyone is interested in \"Free as in freedom\". I would never attempt to hinder or ague against that a given project supports an external non-free program, eventhough I would never use it myself. But it *does* start to piss me off that it the \"Don't Care Camp\" is more and more often lashing out at \"Free Camp\" with scornfull, ridiculing and outright childish remarks.\n\n If you dont care about Free Software at all... then why do you make an issue out of people mentioning it? Use what you want to use.. And let others do the same [even if they chose somewhat differently from you].\n\n Have a nice an coorporating day :-D\n\n~Macavity"
    author: "macavity"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "Doesn't work properly because it isn't interoperable?  Well, linux doesn't work with windows binaries...so maybe linux doesn't work properly?\n\nWhen you think of interoperability, you have to think interoperable with WHAT?  The quality of skype is much much greater than most SIP phones out there.  I have a cable connection and have tried both SIP and skype.  I settled on skype because when calling PSTN lines, Skype has MUCH greater voice quality.\n\nIMMORALly licensed?  Oh please.  Who gives a flying monkey what the license is.  If it works but the GPL'd software doesn't...then USE it.  This is the real world.  Many of us are not ready to give up functionality over issues of license.  Right Linus? \n"
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "The end of the Linus' story doesn't support your argumentation very well."
    author: "Phase II"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "Sure it does.  The problem was that someone wanted to reverse engineer the software and take advantage of the hard work the company did instead of using their software that was being GIVEN to them.  Any company in their right mind would pull the software if that happened.\n\n"
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "You couldn't be more wrong. A lot of companies are selling the server application or the tool to create some files while they give away the client application (NX from NoMachine) or the viewer (Adobe Acrobat) for free. Moreover those companies have no problem with somebody creating an alternative client or viewer."
    author: "Ingo Kl\u00f6cker"
  - subject: "Reverse Engineering"
    date: 2005-06-30
    body: "Sigh, so little information..\n\nTridge talked at LCA this year (an excellent conference, BTW) about the \"reverse engineering\" and he said there were basically two camps:\n\n1) That he had somehow reverse engineered a binary and broken a license;\n\n2) That Tridge is god and magically knows the internals of any protocol he cares to think about.\n\nBoth camps are wrong.\n\nTo sum it up, Tridge put up a bitkeeper URI, which looks like:\n\n   bk://host.name:5000/blah\n\nHe then posed the audience some quick questions.\n\nQ: So, what would you do to connect to the server ?\n\nA:  telnet host.name 5000\n\nQ: Then what ?\n\nA: Type \"help\"\n\n\"Reverse engineering\" complete...\n\nFor more info see the Register article at http://www.theregister.co.uk/2005/04/21/tridgell_bitkeeper_howto/ which includes a screenshot of what you get if you do it.."
    author: "Chris Samuel"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "The quality of Skype is not better than other softphones. They even use the same codec (iLBC) so it's physically impossible. Why do you so desperately seek to defend one proprietary protocol? SIP has many technical advantages compared to Skype, if you'd actually care, but I suspect you don't."
    author: "Fredrik"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "It's not physically impossible.  Skype has much better quality....even on a cable connection.\n\nI don't care what technical advantages SIP has, if I make a phone call, I want the best quality possible.  That's all I care about.  \n\n"
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "Hahaha, yeah.  It takes a special kind of zealot to declare something \"physically impossible\".  :-)"
    author: "KDE User"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "Yes, I think he needs to stick idealism to the side and actually call a ptsn line with skypeout.  He would be amazed at the quality.  THEN let him tell me it's physically impossible."
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "Better yet, use sipphone.com to call someone.  Then, call them right back with skypeout.  Then, ask THEM if they noticed a difference.\n\nI've had everyone I've called with skype or has called me remark how much better it sounds now."
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-29
    body: "Interoperability obviously refers to using standard (or at least documented) protocols.\n\nMany people are not ready to give up our rights and freedom just for mere ease of configuration."
    author: "Luke-Jr"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "That doesn't even take into account that kphone is only a partial implementation of the SIP protocol, so it DOESN'T work with sipphone.com.\n\nI don't want to hunt for ways to make buggy software work.  I want software that works.  Hence, I use skype and can call PSTN lines with no problem.....and have PSTN lines call me."
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "What part of the SIP protocol are you talking about? I am not aware of any. \n\nI tried to find out the sip data in my sipphone.com account I registered ages ago, but cannot find it. Can you help? \n\nI tested with sipgate.de and didn't have notice any problems."
    author: "Eva Brucherseifer"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "SIP common call scenarios document is 500 pages long. No single client ever\nwritten supports all scenarios. That's one of the fundamental problems with\nSIP, it's just not implementable. It's heavily overdesigned by big telcos with\nsome serious amount of cash.\n"
    author: "Janne Karhunen"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-26
    body: "Well, this is a start:\n\nhttp://sourceforge.net/mailarchive/forum.php?thread_id=7546152&forum_id=45235\n\nand the sipphone.com crew said they'd be willing to help with the other:\n\nhttp://forum.sipphone.com/viewtopic.php?t=1003"
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "Then use IAX if you think SIP is too complicated. It survives most forms of NAT, it is pretty straightforward, and is submitted to the IETF for consideration. Just don't use some properitary magic that we can't ever change in the future and tied to one specific company."
    author: "Fredrik"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-27
    body: "Who said I thought it was too complicated?  I never said anything about it being too complicated.\n\nI said it doesn't have the quality of skype and kphone is buggy.\n\nThat has nothing to do with the complexity of the protocol.\n\nI use mine to dial ptsn lines and have ptsn lines call me.  I have no other use for it.  So, Skype works fine."
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-06-29
    body: "Skype uses the iLBC codec. SIP and IAX2 both support iLBC, so there's no basis for your claim of quality difference.\n\nIf you only do POTS calls, then none of Skype's \"benefits\" even apply to you."
    author: "Luke-Jr"
  - subject: "Re: integration with skype would be nice in future"
    date: 2005-07-08
    body: "I'm getting the impression that you know nothing at all about skype, you just hate it because it's not \"free\".  First, as far as it using the ilbc codec, that is true, however you might want to check out: http://www.skype.com/products/explained.html and their network administrator's guide, also available from skype.com.  \n\nAs far as skype's benefits not applying to me because I do POTS calls, from my computer to the POTS network, I am dependent upon skype.  So, I don't understand how you can say skype's benefits do not apply to me. \n\n"
    author: "Michael Staggs"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-26
    body: "Quote: \"It doesn't matter how easy something is to use if it doesn't work properly (read: interoperability) or is immorally licensed (read: not Free)\"\n\nDear Luke, I understand you very well since I also do suffer of this kind of idealism. However, we live in a world that IS, and it does as it does - and not really as we would like it to do.\nSo: Yes, it does matter if it just works and it actually doesn't matter to most people if it is or isn't free. (Not to speak about the fact, that for them it is \"free\" enough (read: as beer). They want just something that works, not something that \"is morally so nice\" but doesn't work. That's the reality, let's face it. You won't change the reality just by denying it..."
    author: "wanthalf"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "Thats's also my opinion. Skype is the only voip-software that just works. And that very easy to install on *all* systems (inclusive Windows, Linux, MacOS...).\n\nAnd note: Not every piece of software must be free, but of course everybody should prefer free software! :)"
    author: "Patrick Trettenbrein"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-27
    body: "Others would say MSN Messenger \"just works\". Why are you plaing around with that Linux stuff?"
    author: "Johan"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-27
    body: "Every Instand Messenger I know just works, but not every VoIP software I know just works... That's the difference! :)"
    author: "Patrick Trettenbrein"
  - subject: "QNext also works between windows and linux!"
    date: 2005-06-26
    body: "Skype is not the GOD of VOIP, QNEXT is also a great app. but again not free.\n\n"
    author: "fast_rizwaan"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-27
    body: "You administratively prohibit protocols with a firewall because you DON'T want them used -- not so your users can find new protocols to circumvent the filters.\n\nEspecially not when it is a monopolist such as Skype backing a proprietary format."
    author: "Fredrik"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "I agree on the second reason, because this implies that achieving (1) ourselves would require heavy reverse engineering.\n\nSame situation as in the IM realm, but this time there are open standards available from the very beginning, instead of being developed when the IM protocol situation grew nasty.\n\nBut of course everybody nca decide for themselves if they want to turn them in again and be kept like addicts or hostages later on.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: integration with skype would be nice in future :)"
    date: 2005-06-25
    body: "Kopete is going to integrate with skype's dbus interface. So you still need the proprietory software to phone with skype. afaik there is no free solution available. With the opentapi framework we will be able use the same mechanism for kcall and use the gui to control the calls. \nbtw, we also want to integrate with kopete in the long term. But of course time is limited and it will take its time ;-)\n\nbtw: this is kcall, not kphone. kphone is only the sip implementation, which might be replaced later on.\n"
    author: "Eva Brucherseifer"
  - subject: "compatible with GnomeMeeting?"
    date: 2005-06-25
    body: "Would be cool if it's compatible with GnomeMeeting.\n\nIs SIP only free in Europe?  I notice the URL of the site is sipgate.de."
    author: "KDE User"
  - subject: "Re: compatible with GnomeMeeting?"
    date: 2005-06-25
    body: "GnomeMeeting uses the H.323 protocol for VoIP.\nSIP is a a more common VoIP standard. FreeWorldDialup is one (of a number of) free SIP registration servers.\n"
    author: "Luke-Jr"
  - subject: "Re: compatible with GnomeMeeting?"
    date: 2005-06-25
    body: "Please have a look at the opentapi web page. The sip implementation will only be one way to communicate. Of course a H.323 backend is possible too. "
    author: "Eva Brucherseifer"
  - subject: "Re: compatible with GnomeMeeting?"
    date: 2005-06-25
    body: "I was just basing my comments on the description. KCall is still compiling for me."
    author: "Luke-Jr"
  - subject: "Re: compatible with GnomeMeeting?"
    date: 2005-06-26
    body: "Does SIP enable video conferencing the same way as H.323 does? Will that be supported by KCall?"
    author: "ac"
  - subject: "Re: compatible with GnomeMeeting?"
    date: 2005-06-26
    body: "SIP (=session initiation protocol) is only for signalling phone calls. The media stream can be audio and/or video. Actually kphone already supports this through the tool \"vic\", but we didn't include this in the first step in order to keep things easy."
    author: "Eva Brucherseifer"
  - subject: "Softphone possibilities"
    date: 2005-06-25
    body: "Hi Eva -- looks like promising work your doing.\n\nSoftphone development could really improve the rediculous artificial urgency that's accompanied by a ringing a telephone.  If I'm talking with a work collegue *in person*, for example, and then someone else suddenly ran in making alarm-like noises, I'd really be annoyed if it were not actually urgent. *grin*\n\nAnyhow, what I really hope for is someday having an Instant Messaging paradigm (without the online status part), so that if you happen to be busy when the other person makes a connection request, you can continue as normal and easily establish the connection in your \"connection queue\" 15 minutes later.  The caller should be able to indicate if it's urgent or not, too, since you wouldn't want to miss the rare urgent call either...\n\nWell, just a few thoughts I wanted to get off my chest! ;)  I look forward to using your work, and also find the kontakt integration plans quite good."
    author: "Ellis Whitehead"
  - subject: "Re: Softphone possibilities"
    date: 2005-06-25
    body: "The keyword here is \"presence service\". IM protocols have this already, technically it is possible to add this to phone applications as well and to let them act upon presence information. Its rather a matter of standardizing the mechanism. "
    author: "Eva Brucherseifer"
  - subject: "Thank you for the name"
    date: 2005-06-25
    body: "I'd like to take a moment to thank the developers for going with KCall for a name, and not something silly like \"Kall\". I cringe when I see idiotic names such as \"Kallery\" (to be part of KDE 3.5) or \"aKregator\". I know this isn't a very productive thing to post about, but silly naming traditions really is a thorn in the side of an otherwise top-class desktop environment."
    author: "Haakon Nilsen"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "well, i'd rather have Kall and not Kcall - i prefer the names where the K is 'embedded\", like kontact, kopete, kaffeine - over names like kget and kmail."
    author: "superstoned"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "I hope that some traditions will be forgotten. I am very happy with the naming of some new projects like Plasma, Tenor and <secret>Oxygen</secret>."
    author: "Fab"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "That's exactly what I mean -- real words for real names, not conjured up twisted permutations of some letters. Look to GNOME for good names."
    author: "Haakon Nilsen"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "well, as Fab pointed out, i don't we have some fine examples within KDE as well. we're not terminally challenged when it comes to naming, though it seems people love to bark about it."
    author: "Aaron J. Seigo"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "> Look to GNOME for good names.\n\nLike gconf, gedit, gthumb..."
    author: "Anonymous"
  - subject: "Re: Thank you for the name"
    date: 2005-06-27
    body: "Evolution and Nautilus are pretty nice names. But notice they came from outside Gnome, by actual commercial entities... which is kind of interesting to ponder... seems we'll be stuck with crap K names as long as hackers are allowed to name their own stuff. <-; Maybe we need an OpenDecentSoftwareNames group, similar to OpenUsability effort.... submit your project and get a nice name.\n\nActually \"Gnome\" itself is not a bad name either. Rather better than \"KDE\".  Alas. Though at least our \"gear\" is marginally better than the ridiculous old \"footprint\".\n"
    author: "T. Middleton"
  - subject: "Re: Thank you for the name"
    date: 2005-06-27
    body: "Why is Evolution a good name? KMail makes it obvious what the application is for, but Evolution does not do this at all. So I really prefer KMail to evolution. And what is good about names like Epiphany? I guess most non-English speaking people have to look up what Epiphany actually means... at least I had to and I lived in the US for a year. The same goes for evince, which is also not a really common word. kpdf at least makes it clear that the thing shows pdfs. And what is better about Gnome then KDE? Gnome does not indicate it is a desktop environment at all, so it is not better then KDE. \n\nI really don't see any reason for replacing names like KMail or Kontact which at least makes it clear to everyone what the app is for by names like Evolution and Epiphany, which don't do that.\n\n"
    author: "Michael Thaler"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "> Look to GNOME for good names.\n\nWhat a load of crap!  This is just GNOME propaganda, some of their names are far worse than the KDE ones."
    author: "KDE User"
  - subject: "Re: Thank you for the name"
    date: 2005-06-26
    body: "I don't like the k* namimg system, neither the k*, neither the \"c\" or \"g\" replaced by \"k\".\n\nSomehow it remembers me the times when people and apps were migrating from\ndos to windows, and often a \"win\" was prepended or added somewhere to applications names to show they are the latest and hottest...\n\ne.g. word -> winword\n\nAnd, Quanta makes a nice exception of the k-scheme...\n"
    author: "ac"
  - subject: "KCall + kopete = KDE p2p voip"
    date: 2005-06-26
    body: "kphone though existing for long, I could not figure out how to use it. We definitely need \"Easy to understand\" and \"Not for Nerds!\" documentation for VOIP in KDE and GNU/Linux!\n\nOpenWengo works nicely but under Windows, Linux version doesn't compile. \n\n\nWhat I wish is a Skype like Just Works! (TM) for KDE's voip solution like KCall and/or kphone or any other voip/p2p voip. \n\nAn integration of KCall into Kopete would be the greatest thing. Getting IP Address from the 'ifconfig' and providing it to kopete's voip thing will make it more like \"Kopete p2p voice over ip\" even though a user use msn, aol, yahoo, or whatever is supported by kopete. \n\nkopete can get the system ip and send it to kopete's kcall plugin for establishing of p2p voip communication. That would be a coolest thing. no logins, no sip blah blah etc. it will just work!\n\ncorrect me if i'm wrong.\n\n\n\n\n"
    author: "fast_rizwaan"
  - subject: "Re: KCall + kopete = KDE p2p voip"
    date: 2008-01-09
    body: "You are not wrong.  I wish I could have a voip like skype for KDE.  I wish the kde team would work on something like that."
    author: "Josh"
  - subject: "Re: KCall + kopete = KDE p2p voip"
    date: 2008-01-31
    body: "\nNobody is working on that because skype doeas a very good job. \n\nAnd they use QT for the Linux Version.\n\nSo what's the problem with skype?\n\n"
    author: "Jaime R. Garza C."
  - subject: "Re: KCall + kopete = KDE p2p voip"
    date: 2008-02-18
    body: "\nskype is not free software. \n\n"
    author: "u.n. finished "
  - subject: "Re: KCall + kopete = KDE p2p voip"
    date: 2008-04-01
    body: "Yep, Skype use Qt but it's a proprietary software and it also is forbidden in many universities and societies because of security matter.\n\nIn fact, KCall and Kopete should be able to interract in a way to have a good gtalk equivalent on kde.\n\nIf someone need a good free software allowing using Xmpp tchat and voip, Gizmo is very good. But its interface is in GTK+.\n\nUsing GTK-QT-Theme, the difference between gtk apps and qt apps won't be so visible and Gizmo will look like a Qt app. :)"
    author: "Sebastien F."
  - subject: "Re: KCall + kopete = KDE p2p voip"
    date: 2008-07-09
    body: "> Nobody is working on that because skype doeas a very good job. \n> \n> And they use QT for the Linux Version.\n> \n> So what's the problem with skype?\n\nBecause Skype is closed-source, and also uses a closed protocol, and so on.\n\nAnd some people (like me) need a program that is able to talk to standard open-protocol VoIP systems (in other words, a SIP phone).\n\nI don't know about KCall, but Twinkle works for me. Unfortunately, the interface is not that good. Being able to integrate something like Twinkle inside Kopete would be great."
    author: "Denilson F. de S\u00e1"
  - subject: "Naming"
    date: 2005-06-26
    body: "Why KCall and not Kall??"
    author: "Andre"
  - subject: "Wine"
    date: 2005-06-26
    body: "openTAPi, btw. didn't the Wine project implement the MS-TAPI?\n\nhttp://msdn.microsoft.com/library/default.asp?url=/library/en-us/wcetapi/html/cmconPhoneDeviceFunctions.asp"
    author: "gerd"
  - subject: "HELP!!!!"
    date: 2005-06-26
    body: "Unfortunately, there is no help! I have registered with http://www.freeworlddialup.com/. But what do i do with that FWD number 671998?\n\ncould you please guide me how to use FWD account with kcall?"
    author: "fast_rizwaan"
  - subject: "Re: HELP!!!!"
    date: 2005-06-26
    body: "I guess http://www.freeworlddialup.com/support/configuration_guide is your friend. KCall has a settings dialog. \n\nYour settings should then be something like\nSIP URI: 671998@freeworlddialup.com\nUsername: 671998\nPassword: *****\n\nDon't forget to adapt the STUN server."
    author: "Eva Brucherseifer"
  - subject: "Re: HELP!!!!"
    date: 2005-06-26
    body: "http://www.freeworlddialup.com/support/configuration_guide/configure_your_fwd_certified_phone/kphone_linux/stun\n\nthanks kphone 4.1.1 works (dialed 613 echo service) but not kcall :(. \n\nI wish that kphone and kcall should allow new-freeworld dialup account creation and saving the username and password for trouble free voip :)"
    author: "fast_rizwaan"
  - subject: "Using phone lines"
    date: 2005-06-27
    body: "Hello,\n\nWill it be able to use phone lines from isdn (like ant-phone) or voice-modem devices?\n"
    author: "ant-phone user"
  - subject: "integration with the world"
    date: 2005-06-27
    body: "i believe this is an important thing, however i do believe this should happen in the form of plugins for kopete allowing SIP/Iax integration with kopete, aswell as an skype plugin for kopete\n\nthis way, the kde world would be able to connect to anything availiable, since i believe nearly all voip is iax/sip for the normal stuff, and for the p2p thing skype is dominating"
    author: "Kasper Sandberg"
---
The <a href="http://www.basyskom.de/index.pl/kcallpeople">KCall team</a> is happy to announce its <a href="http://www.basyskom.de/index.pl/kcalldownload">first public beta release</a>, which was finished during <a href="http://www.linuxtag.org/">LinuxTag 2005</a>. The <a href="http://kcall.basyskom.de/">telephony application KCall</a> integrates with <a href="http://www.kontact.org/">Kontact</a> and makes use of KDE's central addressbook (<a href="http://www.basyskom.de/index.pl/kcallscreenshots">screenshots</a>). This enables KCall to access phone numbers stored in the <a href="http://kontact.org/groupwareservers.php">groupware servers</a> supported by Kontact.
The currently supported telephony backend is <a href="http://www.wirlab.net/kphone/">kphone</a>'s SIP implementation, a VoIP softphone. Other backends <a href="http://www.basyskom.de/index.pl/kcallroadmap">will be provided</a> through the planned <a href="http://www.opentapi.org/">OpenTAPI</a> framework.








<!--break-->
<p>Major features of this release are:
<ul>
<li>VoIP calls via SIP</li>
<li>user notification through an applet, the main application is not necessary for making phone calls</li>
<li>access to KDE's addressbook. Through this phone numbers stored in a groupware server are also available.</li>
</ul>
</p>








