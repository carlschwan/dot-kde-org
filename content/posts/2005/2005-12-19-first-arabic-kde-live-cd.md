---
title: "First Arabic KDE Live CD"
date:    2005-12-19
authors:
  - "reply-reply"
slug:    first-arabic-kde-live-cd
comments:
  - subject: "Software"
    date: 2005-12-20
    body: "Looks really pretty! Does anyone know what the terminal is from 06.png? Or what looks like some sort of right click menu from 09.png?"
    author: "AC"
  - subject: "Re: Software"
    date: 2005-12-20
    body: "terminal is yakuake. it's really quite nice as it folds up when you don't need it. great for the occasional command here and there.\n\nhttp://www.kde-apps.org/content/show.php?content=29153"
    author: "Aaron J. Seigo"
  - subject: "Re: Software"
    date: 2005-12-20
    body: "06.png is YaKuake (http://kde-apps.org/content/show.php?content=29153 ); it's quite excellent."
    author: "Antonio Salazar"
  - subject: "Re: Software"
    date: 2005-12-20
    body: "This menu on 09.png is Kommando. You can find it on kde-apps. Nice program is Kompose, which represents Apple's Expose."
    author: "Software"
  - subject: "Damn nice looking ..."
    date: 2005-12-20
    body: "... but what about picture 0.3 - Is it an alternate kcontrol view ????"
    author: "Matthias Fenner"
  - subject: "Re: Damn nice looking ..."
    date: 2005-12-20
    body: "Yes kind of, I think is the correct reply. I don't remember the name, but I think they use it in Kubuntu. Try looking at the their site."
    author: "Morty"
  - subject: "Re: Damn nice looking ..."
    date: 2005-12-21
    body: "System Settings it's called."
    author: "Patrick Davies"
  - subject: "Re: Damn nice looking ..."
    date: 2005-12-22
    body: "this system settings is with normal kde : i forgot where to enable it:\n\ntry : settings:// in konqueror or system://"
    author: "chri"
  - subject: "Other software"
    date: 2005-12-21
    body: " Does anyone know where to find aUDF (in 01.png), or something like it, for other distributions (Fedora 4)? I've been doing it via konsole using\n\n http://www.raoul.shacknet.nu/2005/11/10/packet-writing-on-cdrw-and-dvdrw-media/\n\n as a guide, but would like the point-and-click version to try out.\n\n"
    author: "Cazo"
  - subject: "Wow"
    date: 2006-01-09
    body: "It looks very nice"
    author: "arido"
  - subject: "Arabbix is the first Arabic Linux system"
    date: 2007-07-11
    body: "On Live-CD\nPlease check its website for more info:\nhttp://arabbix.org/"
    author: "arabic linux user"
  - subject: "Re: Arabbix is the first Arabic Linux system"
    date: 2007-07-11
    body: "I forgot: It's Gnome-based..not yet KDE"
    author: "arabic linux user"
---
<a href="http://www.arabian.arabicos.com/">Arabian Linux</a> also known as <em>arl</em> is a bootable CD with a compilation of GNU/Linux software, full support for Arabic and English languages and automatic hardware detection. It is the first Arabic live-distro and uses KDE as the GUI.  Arabian is the first to have the Arabic language enabled in consoles, pre-compiled ready-for-use softmodem drivers and the first to have its own control panel system in both Arabic and English languages. Arabian Linux is intended for beginners, whether using Arabic language or not, to get an idea see the <a href="http://www.arabian.arabicos.com/rc1/">rc1 screenshots</a>.







<!--break-->
