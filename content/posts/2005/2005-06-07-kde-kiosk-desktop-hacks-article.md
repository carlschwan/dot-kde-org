---
title: "KDE Kiosk Desktop Hacks Article"
date:    2005-06-07
authors:
  - "canllaith"
slug:    kde-kiosk-desktop-hacks-article
comments:
  - subject: "Article could be a litte more thorough."
    date: 2005-06-07
    body: "The article is a good beginning but should be a little more extensive.\n\nKiosk is one of the key differentiating technologies that KDE offers over other environments and it is an admin's dream. I think I and many others would benefit from an expanded version of this article. I only wish I had the knowledge to write it."
    author: "Anthony Black"
---
Administrators and power users will be pleased to note that the text of the <a href="http://jriddell.org/programs/kiosk-article.html">Locking down KDE with Kiosk Mode</a> article as featured in newly published book <a href="http://www.oreilly.com/catalog/linuxdeskhks">Linux Desktop Hacks</a> is available for reading online.  Written by our very own Dot editor and KDE hacker Jonathan Riddell, this article takes you through basic Kiosk configuration using the <a href="http://extragear.kde.org/apps/kiosktool/">Kiosk Admin Tool</a> to lock down KDE. A must read for anyone from internet cafe administrators to those who have a KDE machine for children to use.



<!--break-->
