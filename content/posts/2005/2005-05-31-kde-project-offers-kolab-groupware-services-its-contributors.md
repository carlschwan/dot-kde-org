---
title: "KDE Project Offers Kolab Groupware Services to its Contributors"
date:    2005-05-31
authors:
  - "fmous"
slug:    kde-project-offers-kolab-groupware-services-its-contributors
comments:
  - subject: "The real question"
    date: 2005-05-31
    body: "I wonder if this will pass the Jamie Zawinski test?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: The real question"
    date: 2005-05-31
    body: "I know that this Jamie guy is one of the \"I-know-better-than-anybody-else\"-folks. \n\nBut I don't know what the Jamie Zawinski test is. Care to enlighten me, Derek?"
    author: "anonymous"
  - subject: "Re: The real question"
    date: 2005-06-01
    body: "gg:groupware laid. \n\nHis blog is/was the first item.\n\nDerek"
    author: "Derek Kite"
  - subject: "Good news"
    date: 2005-05-31
    body: "That's great.\n\nIts a pity that installing Kolab is so difficutl/strange. I've been desiring to try it since I started to actively use kaddressbook and korganizer, but my email accounts don't support sieve, so I'm still using local files and POP accounts.\n\nI hope in the future we can see tarballs of Kolab, and more easy to install/package, instead of that strange openpkg and RPMs."
    author: "suy"
  - subject: "Re: Good news"
    date: 2005-05-31
    body: "I agree on the point about source tarballs, minus the rpm stuff. Unless I am mistaken (it does happen :) ), doesn't the source tarballs have to be created to make the RPMS?\n\nAs it is, it tends to either leave the source distros out in the cold or jump through a bunch of hoops just to extract the source from the rpms."
    author: "stumbles"
  - subject: "Re: Good news"
    date: 2005-05-31
    body: "I don't understand. I've found the Kolab setup very clean and easy: one script to launch, a few questions to answer. What strange with that?"
    author: "andrianarivony"
  - subject: "Re: Good news"
    date: 2005-06-01
    body: "i guess it's somewhat strange to have two packaging systems side-by-side, one deb and rpm. and for most (like me) rpm isn't part of their daily work, so they have to learn that, too. it's easy, no big difference, just an other db structure, but it's a psychological barrier.\nbut Kolab install doesn't require any knowledge about rpm, since the scripts take care of everything."
    author: "Andy"
  - subject: "Re: Good news"
    date: 2005-06-01
    body: "There exist more than two systems package systems: \"gg:site:distrowatch.com package management -rpm -deb -src\""
    author: "Anonymous"
  - subject: "Re: Good news"
    date: 2005-06-01
    body: "See KDE for example. You can compile the sources, and install them wherever you like. I have KDE installed both from the standard packages of my distribution, and from source, under ~/kde/local (so they don't collide one with the other).\n\nKDE is very easy to package (at least, applications). If you take a look at \n\nhttp://www.fruitsalad.org/kolab/server/beta/kolab-server-2.0-rc-2/ix86-debian3.0/\n\nYou'll see RPMs... for Debian!\n\nI want just a tarball with some source, and some user defined prefix for installation. The README says that the installation is going to /kolab, instead of following the FHS. Also, have in mind that you need patched versions of some applications. This is pretty ugly for packaging purposes. Why I need to download again apache?"
    author: "suy"
  - subject: "Re: Good news"
    date: 2005-06-02
    body: "just for completeness:\n\nThe prefix is arbitrary in Kolab 2. So you may for example set it to /usr/local/kolab or /opt/kolab if you happen to prefer these paths. "
    author: "Martin Konold"
  - subject: "Webclient?"
    date: 2005-05-31
    body: "I wonder, does this implementation include the experimental webclient (based on Horde)? I want to migrate from Exchange 2000, but would prefer an easier installation method, and a working webclient."
    author: "Philip Cain"
  - subject: "Re: Webclient?"
    date: 2005-05-31
    body: "I think you can consider that the webclient is no longer in an experimental status.\n\nThe Kolab webpage has to be refreshed cause I use it in a production environment for one month (with a Kolab server of course) ... :)\n\nM"
    author: "Mike"
  - subject: "Re: Webclient?"
    date: 2005-05-31
    body: "That's good news. Now if we could just get an easy installer. Heck, I don't care if was ncurses on the CLI, just something more than editing conf files and reading man pages. (Of course, it may well have improved since the last time I tried it - downloading now!)"
    author: "Philip Cain"
  - subject: "Re: Webclient?"
    date: 2005-05-31
    body: "if you follow the instructions to a T, you can get away with running two scripts (and waiting quite a few hours, depending on the speed of your machine) to have a working kolab2 server.\n\ni made the initial mistake of using a non-FQDN for the server which led to me blowing away the install and starting again, though a /kolab/etc/kolab/kolab_bootstrap would've also done the trick it turns out (as i found out later on when i was screwing around with the configurations)"
    author: "Aaron J. Seigo"
  - subject: "Not all contributers know more of PIM than mutt"
    date: 2005-05-31
    body: "> KDE contributors who are interested in having such an account can send an\n> email to kdemail signup\n \nUhm, this means mail to signup@kdemail.net and with what subject, content ..? What bot or person sits on the other end and how does the promised work?"
    author: "WakkoWillie"
  - subject: "Re: Not all contributers know more of PIM than mutt"
    date: 2005-05-31
    body: "There's a person at the other end. Mainly Matt Douhan."
    author: "Anonymous"
  - subject: "When will a Kolab2 interview appear?"
    date: 2005-05-31
    body: "Hey, after all these awesome KDEPim interviews -- wouldn't it be in order to publish some interviews with Kolab2 developers, administrators and users too?\n\nI want, I want, I want...."
    author: "anonymous"
  - subject: "Re: When will a Kolab2 interview appear?"
    date: 2005-05-31
    body: "Already again? Just had it four months ago: http://dot.kde.org/1106909457/"
    author: "Anonymous"
  - subject: "Re: When will a Kolab2 interview appear?"
    date: 2005-06-01
    body: "> Already again?\n----\nYes. A different one.\n\n> Just had it four months ago: http://dot.kde.org/1106909457/\n----\nMissed that one. Thanks for the link.\n\nDidn't we have multiple KDEPim interviews? Each of which covered anther person involved with the project, and another perspective?\n\nSame should happen for Kolab2. Now that it will soon be released. After all, it is not hype -- it is a very well working solution, and it scales to many thousand of users. Unlike Hula.  Hula currently is not more than vaporware. However, I read more about Hula on various news sites than on Kolab2. That must change.\n"
    author: "anonymous"
  - subject: "Re: When will a Kolab2 interview appear?"
    date: 2005-06-01
    body: "There will likely/hopefully be some press about the Kolab2 release but that's up to the Kolab people (and you?) and not the KDE developers."
    author: "Anonymous"
---
At the <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">Dutch KDE-PIM meeting</a> in <a href="http://www.wallsteijn.nl">Annahoeve</a> last weekend it was announced that the KDE project will offer groupware services to all KDE contributors using the Free Software groupware server <a href="http://www.kolab.org/">Kolab2</a>. This means that every KDE project or contributor can get a Kolab2 account for sharing tasks, appointments, contacts and email. Every project can manage their own groupware services and decide with which users they want to share these resources. The Kolab2 server will run under the <a href="http://www.kdemail.net">kdemail.net</a> domain and will be administered by the KDE project.







<!--break-->
<div style="float:right; width: 210px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/kolab.png" width="210" height="60" alt="Kolab Logo" /></div>
<p>Kolab2 is a very robust groupware solution, capable of handling large scale installations. It is tightly integrated with <a href="http://www.kontact.org/">Kontact</a>, the PIM suite from KDE. Currently Kolab2 offers an excellent Free Software solution for providing groupware services to the GNU/Linux and Unix desktop and even to the Windows desktop through the use of the <a href="http://www.toltec.co.za/">Toltec plugin</a>.</p>

<p>KDE contributors who are interested in having such an account can send an email to <a href="http://www.kdemail.net">kdemail signup</a>. The bandwith and hardware is donated by <a href="http://www.fruitsalad.org/">Fruitsalad</a>, a community that provides hosting and other services to the KDE and Kolab community.</p>


