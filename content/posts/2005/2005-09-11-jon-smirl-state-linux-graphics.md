---
title: "Jon Smirl on The State of Linux Graphics"
date:    2005-09-11
authors:
  - "zbraniecki"
slug:    jon-smirl-state-linux-graphics
comments:
  - subject: "rather late article post"
    date: 2005-09-11
    body: "This article was on slashdot rather a Long time ago (more than a week back). http://linux.slashdot.org/article.pl?sid=05/08/31/1340241&tid=104&tid=152&tid=106\n\nI thought this was something new. Hopefully there are new thoughts about this."
    author: "ac"
  - subject: "Re: rather late article post"
    date: 2005-09-11
    body: "> This article was on slashdot rather a Long time ago\n\nWhy care when it was on Slashdot? Did anyone read Slashdot during Akademy?"
    author: "Anonymous"
  - subject: "Mac OS X"
    date: 2005-09-12
    body: "How is it done on Mac OS X? They do not need an X-Server??"
    author: "Aranea"
  - subject: "Open GL on *NIX"
    date: 2005-09-14
    body: "A relevant question is what do other *NIX (AIX, Solaris, HP/UX, etc.) use for graphics hardware?  I presume that some are using 3Dlabs hardware or their own proprietary hardware.\n\nThis Graphics hardware issue seems to be a serious problem for the future of *NIX.  Perhaps we could ask where it all went wrong?  SGI started out with a hardware implementation of the OpenGL pixel pipeline which IIRC was two full size boards and cost like $3K (ouch!).  If things had progressed correctly, it should now be a half board and cost like $300 and run a lot faster.  But this didn't happen.\n\nI think that we all know what went wrong -- in a word 'GAMES'.\n\nBut where do we go from here.  It appears obvious that the poor quality proprietary OpenGL adapter drivers for ATI and NVida are not going to be the answer.  3Dlabs is still making boards, but the Oxygen line (which was OpenGL) has been dropped (development stopped some time ago).  This appears to be a large mistake since their switch from firmware/hardware to software has resulted on a lot of driver problems with the WildcatVP series.\n\nNote to the TI graphics chip department: perhaps they had a good idea, at least for *NIX.\n\nI see only one solution, there is clearly a market for a *NIX/OpenGL graphics board which works with OpenGL.  There is nothing to hide here since the API is known and the driver should be as trivial as possible.  Where is the competitive advantage -- in the hardware of course.  Run the SGI pixel pipeline faster and you have a better board.\n\nOTOH, would we be better off with a board like Apple used to sell that was just a frame buffer and a general purpose RISC processor.  On *NIX, such a board could also have a processor to run the X server (doesn't need to be as powerfull as the main CPU)."
    author: "James Richard Tyrer"
---
The dust has yet to settle after the XFree86-&gt;X.org migration and the future of the defacto Linux windowing system  is still unclear.
Jon Smirl has written an <a href="http://dri.freedesktop.org/~jonsmirl/graphics.html">extremely interesting article</a> describing the current problems of the X.org server and overviews related projects.











<!--break-->
