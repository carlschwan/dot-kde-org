---
title: "Application of the Month: KSpread"
date:    2005-03-07
authors:
  - "jriddell"
slug:    application-month-kspread
comments:
  - subject: "Still really needs..."
    date: 2005-03-07
    body: "better lookup support. Or, at least based on the last time I looked, lookup support at all. But it has promise."
    author: "mikeyd"
  - subject: "Re: Still really needs..."
    date: 2005-03-07
    body: "Er, what exactly do you mean when you say \"lookup support\"?"
    author: "Inge Wallin"
  - subject: "Re: Still really needs..."
    date: 2005-03-07
    body: "Lookup tables, so I can do VLOOKUP or HLOOKUP like in Excel or Gnumeric. If I need to be able to sum multiple attributes of the same list, e.g. (since I can't think of a better example right now) the price and weight of an order, the obvious way to do this is with a lookup table, and I have a few Excel spreadsheets that use them heavily. I also need the SUMIF function, but I imagine that will be implemented soon since it's probably a trivial hack to COUNTIF which is already implemented. But lookups are a bit more complex."
    author: "mikeyd"
  - subject: "Re: Still really needs..."
    date: 2005-03-08
    body: "Why, SUMIF already exists in latest CVS. ;)\n"
    author: "Tomas"
  - subject: "Bugs in KOffice?"
    date: 2005-03-07
    body: "How good is KOffice when it comes to bugs?\n\nFeaturewise, KOffice seems to be pretty Ok. But how much of the bugs have been eliminated?"
    author: "Kdeuser"
  - subject: "Re: Bugs in KOffice?"
    date: 2005-03-07
    body: "well, I dunno how many bugs they did squash lately, but for me it became quite stable over time. it used to crash now and then, but that seems to be alot less common..."
    author: "superstoned"
  - subject: "Re: Bugs in KOffice?"
    date: 2005-04-04
    body: "> How good is KOffice when it comes to bugs?\n\nPretty good.  We have some of the best bugs around."
    author: "JohnFlux"
  - subject: "What about KChart?"
    date: 2005-03-07
    body: "Hey!  He could have mentioned KChart that goes nicely along with KSpread.  That would have been nice to show in the article as well. "
    author: "Inge Wallin"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "Are you sure it would've been a good idea to mention KChart? Have you ever used it? I tried to do some simple things, like creating a chart from columns, which is IMPOSSIBLE! I had to reformat my data to be in rows in order to create a chart. Then I thought I could print my charts... Ha! The chart appeared somewhere in the middle of my sheet, covered up some data, and was the size of a thumbnail. In short, KChart can be made to look nice in an example but is otherwise unusable! In case you wonder, there are bug reports about the stuff I mentioned but KChart is more or less abandonware, I don't think anybody is working on it and it's just over my head to fix these problems. I had to switch to Openoffice Calc eventually.\n"
    author: "ac"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "I think that if you go into bugzilla and look up KChart you will find a completely different story.  I have been fixing KChart for the last couple of weeks, and it is getting really usable. It is most certainly *not* abandonware. The bug count is down to 7 bugs now and shrinking steadily. Things are looking good for the upcoming release of KOffice 1.4.\n\nBtw, it's funny that you mention the rows vs. columns issue.  I commited the final fix for that just 10 minutes ago.  :-)\n\nBut if you only look at the 1.3 series, I can understand how you feel."
    author: "Inge Wallin"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "Just want to say: thanks for your work!  =)"
    author: "ca"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "I didn't realize that somebody picked up KChart, I tried it last year. Many thanks for your work and I'm looking forward to KOffice 1.4! I'll definitely give KChart another shot then (OOo is just so slooowww). Please also make sure that charts can be printed properly! That was the thing which finally drove me to OOo.\n\nkk.\n"
    author: "ac"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "Will do.  \n\nIn fact, I wasn't aware of the problem until you mentioned it here.  However, I suspect that it is a problem with KSpread, not KChart.  KChart just prints into the area that it receives.  I will try to make sure it works, wherever the problem lies."
    author: "Inge Wallin"
  - subject: "Re: What about KChart?"
    date: 2005-03-09
    body: "Thanks for your (and all the others) dedication to improve our favorite office suite. Keep it up, it's everybody's gain."
    author: "Hugo Rodrigues"
  - subject: "Strange"
    date: 2005-03-08
    body: "> Btw, it's funny that you mention the rows vs. columns issue. I commited the\n> final fix for that just 10 minutes ago. :-)\n\n?? Koffice 1.4 has been nearly unusable until 10 minutes before?\n\nProbably the version number is a off-by-one-bug, too, maybe it should read 0.4 ;-)\n\nBut hey, even the chartengine of Openoffice does suck very badly, so Koffice shlould be quite comparable (oh, sorry, \"komparable\") to Openoffice.\n"
    author: "Xedsa"
  - subject: "Re: Strange"
    date: 2005-03-08
    body: "Well, KOffice 1.4 is not out yet.  The current official release is 1.3.5, and I think the version number of the code in CVS is 1.3-post."
    author: "Inge Wallin"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "Hey, feel free to post a KChart article/review/showcase on the dot!  Or maybe you could try to get someone to do it for you."
    author: "Navindra Umanee"
  - subject: "Re: What about KChart?"
    date: 2005-03-08
    body: "Maybe I will.  But I think a better time would be after the release of KOffice 1.4.  No time now."
    author: "Inge Wallin"
  - subject: "Screenshots"
    date: 2005-03-07
    body: "Application screenshots should be taken with English screen language to be understandable for a bigger audience."
    author: "Anonymous"
  - subject: "Re: Screenshots"
    date: 2005-03-08
    body: "Oops, the screenshots for the English version are in German :o)\n"
    author: "ac"
  - subject: "Fanatismo cultural"
    date: 2005-03-08
    body: "\"Usted los amercians que cogen es toda la cogida iguales,,,\" (-:"
    author: "An\u00f3nimo"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: "Was sagst du da? (-:\n"
    author: "Anonymer Nutzer"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: "Azt se tudom milyen nyelven mondta... =)"
    author: "Illissius"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: "L\u00c3\u00a4gg av nu, va.  Skriv s\u00c3\u00a5 man begriper!"
    author: "Inge Wallin"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: "Daar versta ik dus helemaal niets van!"
    author: "Andre Somers"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: "C'est bon, vous pouvez arr\u00eater l\u00e0, on a compris."
    author: "jmfayard"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: "\u00c9gua da putaria, macho..."
    author: "Taupter"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-08
    body: " jIH ghaj ghobe' prala vaD KDE."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-09
    body: "Kakvi su ovo &#269;udni jezici ovdje?"
    author: "KOffice Fan"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-09
    body: "Wszystkiego najlepszego z okazji nowego roku..."
    author: "MacBerry"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-09
    body: "Es pot saber qu\u00e9 collons esteu diguent? \u00d2stia!\n\nFrom Carcaixent. spain"
    author: "rotovator"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-09
    body: "Quem \u00e9 o filho da puta que come\u00e7ou a merda desta discuss\u00e3o?"
    author: "portuguese"
  - subject: "Re: Fanatismo cultural"
    date: 2005-03-15
    body: "Duh, pada ngomong apa ya? Aku gak ngerti."
    author: "Indonesian"
  - subject: "Good to See KOffice Still Progressing"
    date: 2005-03-08
    body: "Although I can't use KOffice for much of what I do due to compatibility reasons with other places I use computers, I love to see it progressing so well. When OpenOffice was released, it seemed likely that the competing open-source office suites would just die away, which would have been a huge shame. OpenOffice is only a clone of MS-Office and doesn't seem to have much hope of growing beyond that. Also, OpenOffice is wildly slow, especially on load times. KOffice has been built right from the ground-up and is staying snappy and easy to use as it adds features, rather than bloating as other office suites have.\n\nOnly a year ago, KSpread was, as far as I was concerned, completely unusable due to bugs, crashiness, and featurelessness. Now, it's approaching the point where I can just open it up and get working on whatever I need to. Thanks for all the great work!"
    author: "jameth"
  - subject: "KOffice 1.4"
    date: 2005-03-08
    body: "Koffice 1.4 appears promising, at least until OOo 2.0 :p Seriously though, will KOffice 1.4 be in KDE 3.4? Sure has a nice ring to it :)"
    author: "Al"
  - subject: "I guess not..."
    date: 2005-03-08
    body: "http://developer.kde.org/development-versions/koffice-1.4-release-plan.html"
    author: "Al"
  - subject: "Re: KOffice 1.4"
    date: 2005-03-08
    body: "From what I hear, they upped the Java in OOo 2.0\n\nI'm looking at a switch to KOffice..."
    author: "Joe Lorem"
  - subject: "maintainer ?"
    date: 2005-03-08
    body: "Reading the interview, it is clarified (that otherwise is obvious regarding the bug history for last two years) that L.Montel is not a maintainer of Kspread. In the interview, there is no words about Kspread. He is neither focusing nor slightly interested in Kspread. \nYes I know, he is engaged with other parts of Koffice and with Linux in general. I appreciate his other works. But let say clearly: Koffice hasn't had a true maintainer for the last few years. \nUnfortunately, Kspread in the stage more or less, how it was born. It is a great promise but nothing more. It is full of primitive bugs and of simple feature lacks. If one gives a try for Kspread, will leave it very soon because it is not suitable for daily usage. \nTo make it clear: I like Kspread. And use (but dislike) OOo in my work. (That is reading data columns; creating charts after simple manipulation on them).\n\nRecently Inge Wallin has taken the charge of Kspread (as it is visible in the bug statistics). It is highly wellcomed that there, finally, is someone who fixes (sometimes 3 years old) bugs. I would have a warning to the developer: the big flow of bug reports will raise after the point the Kspread quality reach the usability level and a lot of people begin to use it.\nI am interestingly waiting if one person's effort is enough to improve significantly such a complex program as Kspread.\nMy full respect to Inge, who is the current maintainer."
    author: "devians"
  - subject: "Re: maintainer ?"
    date: 2005-03-08
    body: "Regarding the interview, I think you are a bit unfair to Laurel.  He answers the questions from the interviewer, and they are not all of them focused on KSpread.  To say that he is not interested in KSpread is of course ridiculous.  I can only assume that you used stronger words here than you actually intended.\n\nWhen you say that I have taken charge of KSpread, I assume that you mean KChart.  I have not done much job on KSpread. I actually tried to start with fixing KSpread bugs, but the code base was simply too big to get an easy grasp on, so I had to start out with something smaller, i.e. KChart.  I will go back to KSpread, though, once KChart is in a workable shape.\n\n(At this point, I think I will make a wish: Please create a simple document that describes the classes in KSpread, and how they interact.  I don't mean proper class documenation; just something that very superficially describes what each of the main classes is supposed to do. I did something similar for Kreversi in kdegames when I tried to figure that program out, and it helped me tremendously when working on the program.)\n\nBack to the main topic. There is one thing that I fully agree with you on: The tendency of many developers to focus on new features instead of fixing bugs.  For me, a powerful program with many bugs is worse than a smaller program that works well.  I hate to see a program with lots and lots of small, unnecessary bugs.  I try to attack the simplest one first, and then continue from there.  That is how I started out with KChart, and that is how I will tackle KSpread.\n\nTo be fair to the KSpread people, there _are_ a lot of bugs that look simple on the surface, but that actually are pretty difficult to fix.  There is currently a big rewrite of the internals going on to make KSpread faster, use less memory and be more scalable.  Such a big rewrite of the internals make the surface (the visible parts) suffer, as it were, but in the long run it is necessary.  But still, I think that there are a number of irritating bugs that should simply be fixed before any attempts are done to do other things."
    author: "Inge Wallin"
  - subject: "Re: maintainer ?"
    date: 2005-03-08
    body: "In addition the interview did not mention that Laurent has done most of the work with moving the whole KOffice to the OASIS format. "
    author: "Morty"
  - subject: "Re: maintainer ?"
    date: 2005-03-08
    body: "Thanks for your reply.\n >you used stronger words here than you actually intended.<\nYou may be right. Perhaps the introduction exagerated me, being misleading. My intention was not to blame Laurel. It is possible that he was forced to be a maintainer, in the lack of other person who could effort more time to Kspread in the last few years.\nInstead, I wanted to illuminate the painful state of Kspread.\n\nTurning to more enjoyable points: the question if fixing a bug is simple or not, depend on the fact, how familiar you are with the code. Sometimes a really simple change takes you several days or weeks, if first you have to 'learn' the code and find the proper location of interaction. I used the word simply in the sense that 'looks simple'. There are obviously hard bugs whose fixing involves the 'treating' of several parts of the code.\n\nI was glad to read, that some rewriting is in progress in the background."
    author: "devians"
  - subject: "Openoffice"
    date: 2005-03-08
    body: "OpenOffice 2.0 series is a giant leap.\n\nKSpread: When will it adopt the OO-fileformat as default?\n\n\n---\n\nFrom a user perspective I think what really matters for users are nice document templates for everyday business, not special features. \n\nI would like to see a kde-template community emerge just as kde-look and kde-apps. What users makes happy are birthday greeting cards printing solutions. What users make stick to a program are useful documents you cannot live without. "
    author: "gerd"
  - subject: "Re: Openoffice"
    date: 2005-03-08
    body: "KSpread: When will it adopt the OO-fileformat as default?\n\nIMHO: Import/Export should be enough, otherwise you may be forced to include same features as OO, at the same time they are included there, On the other hand, if you want to include a feature, you would require OO to include it... this can be crazy."
    author: "p"
  - subject: "Re: Openoffice"
    date: 2005-03-08
    body: "That's been decided a long time ago.  It's just a question of when the move will be completed.\n\nIt's not the (old) OOo file format but the one standardised at OASIS.  Both OpenOffice and KOffice developers have worked together defining it.  I don't think either project wants to make proprietary extensions to the format.  But still, the point you bring up is an interesting one.  I wonder how this common format will work out with respect to unimplemented features in either of the office suites. \n\n"
    author: "cm"
  - subject: "Re: Openoffice"
    date: 2005-03-08
    body: "> Import/Export should be enough\n\nTake example of a person X, who don't know much about computers but uses Linux (with KDE and KOffice as default). He creates a documents (in default file format) and sends it to his friend who dont know Linux. Since KOffice does not work with windows/MacOS, his friend will never be able to view the document.\n\nOn the other hand, <A href=\"http://www.oasis-open.org/home/index.php\">OASIS</A> is a more widely accepted standard. I think, KOffice should use OASIS file format as default."
    author: "nilesh"
  - subject: "Re: Openoffice"
    date: 2005-03-08
    body: "Ahh! the famous made up usercase of the imbecile users, always a winner. But let us now tie this to the real world please. The windows/MacOS user who gets documents she/he cannot open will send a reply to our Linux using friend informing him of the fact(In the case she/he are actually interrested in what the person sent in the first place). If our friend need to get the information to his friend, he will most likely try to find out a way to do so. He will then discover either how to print as pdf or how to save as a format his friend can access, in the process learning something and then becoming a user not usable in this kinds of examples.\n\nOn the other hand, OASIS will not do any difference, since most likely the friend on windows/MacOS does not have anything installed to deal with that format either. With MS-office still being the \"standard\" office application for windows/MacOS users, and MS-office does not do OASIS. Being \"a more widely accepted standard\" does not make MS-office handle it any better than KOffice documents."
    author: "Morty"
  - subject: "Re: Openoffice"
    date: 2005-03-08
    body: "I recently had to mail a jobbing printer an image of a cross for a funeral card; I mailed it as .jpg and .eps. The printer mailed me back that they couldn't open it and could I just resend it as a Word document? I swallowed all my pride (in view of the melancholy occasion) and created a Word document with the image inline. In KWord. They could read it... Other Windows users have sent me panicky mails that there was something binary in my mails that they couldn't read and was it a virus? It was my digital signature...\n\nSo, you're right, even the most clueless of Windows users will mail you back telling you they cannot open your attachment. And in the end, the only they'll know how to open is a Word document."
    author: "Boudewijn Rempt"
  - subject: "Filter for MSOffice"
    date: 2005-03-09
    body: "MS does not do OASIS, well what about an import filter for MSOffice? As far as I knoe MS-programs are extensible"
    author: "Jakob"
  - subject: "Re: Filter for MSOffice"
    date: 2005-03-09
    body: "MS stated that they want to support OASIS. So I hope we'll see OASIS filter built into the next version of MS Office."
    author: "Birdy"
  - subject: "One feature missing for me.."
    date: 2005-03-08
    body: "There is only one thing that is keeping me from using Kspread right now.  I don't even know what the feature is called, but it works in both Gnumeric and OpenOffice.org.  \n\nMy parts supplier sends me price lists in xls format, and for a few parts there will be a cell that has a little arrow in the corner.  If I hold the mouse over this arrow, a little balloon help thingy will open up giving me some extra information.  On my parts lists it is usually useful stuff like promotions (Buy 5 get 1 free, free Targus case with laptop).  With KSpread I miss this information,  so I am stuck with Gnumeric's great functionality and truly HIDEOUS file dialogs.\n\nMark"
    author: "Mark"
  - subject: "Re: One feature missing for me.."
    date: 2005-03-08
    body: "There is support for comments in KSpread, is that what you mean? You can add a comment to a cell and then you get a small red triangle in the upper right corner. If you place your mouse over the cell the comment is shown like a tooltip.\n\nIt might be possible though that comments from Excel sheets are not imported, so that's maybe why you don't see them."
    author: "Raphael Langerhorst"
  - subject: "Re: One feature missing for me.."
    date: 2005-03-08
    body: "Yeah I had a further look.\n\nComments are the feature that I need.  They work fine natively, but importing from Excel seems to be missing.\n\nMark"
    author: "Mark"
  - subject: "KOffice is a basketcase"
    date: 2005-03-09
    body: "it is full of bugs in basic functionality, has poor ms import/export, has an underdocumented/unmaintainable codebase. it won't be used by the broader community. the only way forward is to kde-erize openoffice (already underway) and optimize openoffice. look at how many years koffice has had to develop. where is it? where will it be? it is time to let it go.\n\nsince some fanatics are going to accuse me of being a troll, i might as well leave something you can flame me over and so that you can claim that i am mad:\n\nDo you have the time\nTo listen to me whine\nAbout nothing and everything all at once\nI am one of those\nmelodramtic fools\n"
    author: "anonymous coward"
  - subject: "Re: KOffice is a basketcase"
    date: 2005-03-09
    body: "> it is full of bugs in basic functionality\nYes, it has bugs. But since version 1.2 it's usable for me. Crashes didn't happen since 1.3 for me.\n\n> has poor ms import/export\nBut 1.4 will handle the (in future) much more important OASIS format perfectly.\n\n> unmaintainable codebase\nIf the codebase were unmaintainable, it wouldn't be possible to build such a big project with so few developers. So I'd say the codebase is _very_ good and one of KOffice's streangths.\n\n> it won't be used by the broader community\nThanks to OASIS this will be no big problem in the future\n\n> optimize openoffice\nIf you manage to double performance of OOo, KOffice would still be more than twice as fast...\n\n> look at how many years koffice has had to develop. where is it?\nLook how few developer worked on it. Where is it?\n\n> where will it be?\nLooking at krita, kexi, the progress of the \"old\" parts and looking at OASIS - I see a shiny future for KOffice.\n"
    author: "Birdy"
  - subject: "Re: KOffice is a basketcase"
    date: 2005-03-10
    body: "\"look at how many years koffice has had to develop.\"\n\nLook at how long OpenOffice has had to develop.\n\nRemember, OpenOffice was StarOffice beforehand. I couldn't find how old it is very easily, aside from the fact that it was a mature product and was releasing a free Linux binary in 1996, so a good bit over ten years old. And, with all that time, OpenOffice is bloated, slow, fairly reliable, and feature-complete to about four-years behind MS-Office.\n\nNow, how old is KOffice? Five years? Six? KOffice is extremely young for an Office Suite, and it has covered huge amounts of ground. It already does more things than most any office suite out there, has good integration through almost all of itself, and is lightning fast. It has been maturing steadily, and there is no reason to doubt it will continue doing so."
    author: "jameth"
  - subject: "Re: KOffice is a basketcase"
    date: 2005-03-10
    body: "> I couldn't find how old it is very easily, aside from the fact that it was a mature product and was releasing a free Linux binary in 1996, so a good bit over ten years old.\n\nThe company was Star Division was founded 1985. The first project was the word processing program Star Writer, which had its first release in 1986.\n\nStar Office 1.0 was released 1992/1993 and it contained StarWriter compact, StarBase 1.0, StarDraw 1.0."
    author: "Christian Loose"
  - subject: "Re: KOffice is a basketcase"
    date: 2005-03-10
    body: "Even though koffice still has many bugs, it is still a promising office suite. I admit that openoffice is a very good office suite and has so many features that koffice doesn't have, (I also use openoffice to edit complex documents, and I also like to see the progress of OpenOffice.org 2.0), but koffice also has features that openoffice doesn't have, for example KParts. With koffice you can easily preview the content of a document using konqueror, and even you also can preview some simple M$ documents using konqueror. Thanks to KParts technology!\n\nSo if someone don't like koffice, just simply don't use it, don't hurt the koffice developers by saying let it go. They have worked very hard to for this project. I'm sure there are many people still love this lightweight office suite. We have freedom to choose our own office suite."
    author: "KAnonymous"
---
March's Application of the Month covers <a href="http://www.koffice.org/kspread">KSpread</a>, the spreadsheet program from <a href="http://www.koffice.org">KOffice</a>.  Markus Grob introduces us to using KSpread and we have an interview with its maintainer Laurent Montel.  Read it now in 
<a href="http://www.kde.nl/apps/kspread/">Dutch</a>, <a href="http://www.kde.org.uk/apps/kspread/">English</a>, <a href="http://www.kde-france.org/article.php3?id_article=133">French</a>, and <a href="http://www.kde.de/appmonth/2005/kspread/index-script.php">German</a>.


<!--break-->
