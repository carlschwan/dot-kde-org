---
title: "KDE Contributors Worldwide"
date:    2005-03-26
authors:
  - "rendres"
slug:    kde-contributors-worldwide
comments:
  - subject: "maps with nicks"
    date: 2005-03-26
    body: "To be honest with you I like the map at http://developer.postgresql.org more - it is interactive, whereas in Central Europe there are so many folks it is illegible...\n\nI wish I could be there too... But unfortunately I am just a user.\n\nCheers\n\nZoltan"
    author: "Zoltan Bartko"
  - subject: "Re: maps with nicks"
    date: 2005-03-26
    body: "\nThanks for the link, did not know this one. \n\nI am working on a image map for worldwide.kde.org. \n\nYou are right that it will be unusable in Europe and thats the main reason it is not there so far. Searching for a nice solution there.\n\n"
    author: "Rainer Endres"
  - subject: "Who is my neighbour?"
    date: 2005-03-26
    body: "As a West European it's difficult to see who lives near my place. Zooming would be perfect ;)"
    author: "Bram Schoenmakers"
  - subject: "Re: Who is my neighbour?"
    date: 2005-03-26
    body: "Tried http://worldwide.kde.org/pics/centraleurozoom.jpeg?"
    author: "Anonymous"
  - subject: "How many developers"
    date: 2005-03-26
    body: "How many developers are there? \nCan you give a number on the website?\n "
    author: "jeroen"
  - subject: "Re: How many developers"
    date: 2005-03-26
    body: "There's 1052 people listed in kde-common/accounts"
    author: "Ian Monroe"
  - subject: "Re: How many developers"
    date: 2005-03-26
    body: "\nSince there are a) not only developers on this map but all kind of contributors I can only give you this. and b) not all developers are on this map. Will think about giving the number on the site.\n\n\n "
    author: "Rainer Endres"
  - subject: "latin Europe, where are you?"
    date: 2005-03-26
    body: "France, Spain, Portugal, Italy... \nthat's an enormous territory and a stupendous lot of people, yet there are seemingly only a handful of KDE contributors there.\n\nThat speaks volume about the language barrier ;(\n"
    author: "."
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-26
    body: "Wel... One would assume the language barrier to be just as great for the Germans and Dutchpeople... And there's no stint of them on the map."
    author: "Boudewijn"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-26
    body: "No I can assure you it's not. German, Dutch and English are based on the same vernacular roots.\nThat mean it is much much harder for latin languages speakers to grasp the basics of day-to-day language, as it's where most germanic roots are used.\n\nOnly scientific/sustained terms tend to be latin based, and even then that's not the case for computer science.\n"
    author: "."
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-26
    body: "Oh, but English is more than half French anyway..."
    author: "Boudewijn"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-26
    body: "whoever* told* you* that* was* very* wrong*\n\n(*): Old High German root :-)"
    author: "."
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-27
    body: "it's true. half the enlish words stems from france. they reigned england for quite a time. And some words came back to france... do you know the beefsteak? le biefteck (or however these french write that word, correct me please, any french natives?) - it came from the french word for meat, I can't write that either - something like bieuf? anyway, the french rulers asked the english peasants for meat - they thouht it meant meat from a cow - and it became beef. which went later back to france as le biefteck or whatever...\n\nsorry for all the spellingmistakes, french lessons are 8 years ago, and I learned this in english class 6 years ago."
    author: "superstoned"
  - subject: "Re: latin Europe, where are you?"
    date: 2007-01-24
    body: "I can assure you that over half of the rootages of English words in the English dictionary come from Latin around 25% via Old French and 1066, and around a further 25% through fashions in science.  It is true that most of the gramatical structures and 28% of the most used words are germanic. However, it is quite unlike Dutch and German in the size of it vocabulary.  And there are plenty of latinate words in everyday use.  To use the previous post as an example the words 'assure' 'based' 'vernacular' 'latin' 'language' 'scientific' 'sustained' 'term' 'tend'.\n\nhttp://www.askoxford.com/asktheexperts/faq/aboutenglish/proportion?view=uk\n\nThe oxford dictionary will back me up"
    author: "mmm"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-27
    body: "Actually, I believe the english language has its roots in western germanic tribes that migrated to the English islands I don't know how many thousands of years ago."
    author: "Plisken"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-27
    body: "Well, actually, no. Wright, whose monumental series on the history of the English language ought to be on every shelf, dates the first monuments of Old English to circa 700 AD. Before the advent of the speakers of Old English arrived, the British islands were the domain of speakers of Celtic, Pictish and Latin.\n\nOr course, Old English had much more Germanic roots than modern English, but we all know what happened in 1066... Hastings, William the Conqueror and all that. French became the language of court, state and law. \n\nNobody does so now except jokingly, as I was, but for years debate in linguistic circles has raged about whether we could or should classify English as a creole because of the large component of French words and constructions; and then around Pope people started to consciously add as much Latinate words and constructions as they could.\n\nResearch is still ongoing: http://www.anglo-norman.net/articles/missinglink.xml is a nice article. Wikipedia has a little information, too: http://en.wikipedia.org/wiki/Latin_influence_in_English, but I guess they underestimate the foreign component in English.\n\nBy the way, and to bring this back to KDE, the very first application I made using the KDE libraries was Kura, an application to morophologically analyze texts and build interlinearas and dictionaries. It's still going strong under new maintainership at: http://www.ats.lmu.de/kura/index.php."
    author: "Boudewijn Rempt"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-27
    body: "Hmmm honestly I wouldn't expect that to be the case. At least in Portugal you have 4 years of compulsory English classes (5th-9th grade), and as an optional language for 3 more years (10th-12th grades). So most people in the sci fielad have at least 7 years of practice. Most sci/tech grad courses will also end up requiring you to deal with English bibliography sooner or latter. In my experience most programmers in here have no problem whatsoever communicating in English. \n\nOne thing that would help KDE a lot would be some Qt/KDE lobbying directed to sci and tech University Departments. I know that experienced KDE developers want to be... developing... but if it was possible to have something like 1 week crash courses in some universities, maybe associated with other KDE events, it would be great. That is a great way to recruit, not only new young student programmers, but eventually teachers that will use it later in their classes. \n\n"
    author: "John Languages Freak"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-27
    body: "I am Portuguese -- dunno if you're portuguese or not. From my experience, I find it amazing how computer science students are so afraid of the English books and always go for Portuguese alternatives that most of the case are far worse in quality.\n\nIn my college, I'm very lucky that only Linux is installed in machines, so people have no choice but to use it. Even in the few computers where Windows is installed on ppl don't use it because they're more used to Linux and the Windows setup sucks badly. Anyway, this is an exception. Most colleges use only Windows or offer an optional out-dated Linux distro for boot. As a matter of fact, some classes (and even profissional courses) require some Microsoft software -- there is a very reputative college where students learn Microsoft Office in the first year to Microsoft SQL and so forth (FEUP).\n\nIn Portugal, students are used to Windows and I think that's the main point why there aren't many developers from here. Even if students have to use Linux in the college computers, most of them do not even have it installed in their home computer and only a very few use it as their main OS. There isn't also that spirit of free software as in other countries -- I have never heard the couple words \"free software\" in any generic classes about Linux nor in any other for that matter. I've sent a few patches to a in-house program that is used to teach classes and none of them was accepted or there was much interest in getting help on the development of the program.\n\nThat said, Netherlands rule! Really, best country of Europe. I would love to study there. :)"
    author: "blacksheep"
  - subject: "Re: latin Europe, where are you?"
    date: 2005-03-28
    body: "As an italian currently living in Spain that has got some \"nordic\" friends, all I can say is that we need time to \"advance\" with mentality. I know it's plenty of LUGs here, and that it's plenty of skilled people helping KDE and other OS projects, but the IT masses are still way behind the trend in northern Europe.\n\nMmmh...I can't easily explain myself with a comment on the dot, the topis is really a very long one, but maybe you can get the idea.\n"
    author: "Davide Ferrari"
  - subject: "Big thanks"
    date: 2005-03-26
    body: "This is very cool.\n\nExplains why I don't run into KDE users every day.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Big thanks"
    date: 2005-03-26
    body: "Um, no it doesn't."
    author: "Ian Monroe"
  - subject: "Re: Big thanks"
    date: 2005-03-26
    body: "It only explains why you are not running into KDE contributors every day ;) And let me say this, this does not happen here, too :) \n\nI run into users every day though.\n\n"
    author: "Rainer Endres"
  - subject: "Re: Big thanks"
    date: 2005-03-27
    body: "I've never run into anyone who uses KDE. A few linux server types, and some POS. The only linux desktop user I know uses Enlightenment.\n\nAround here people show their technical prowess by bragging about how many spy and ad wares their free scanning software found.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Big thanks"
    date: 2005-03-29
    body: "Bragging about how many spy and ad wares their free scanning software found? That's quite a sick state of affairs..."
    author: "ac"
  - subject: "This is getting ridiculous!"
    date: 2005-03-26
    body: "Why are KDE's marketing efforts so pathetic?\n\nAfter all this time since the KDE 3.4 release, there still is no user friendly feature guide for KDE 3.4. Please don't point me to developer.kde.org, users will get confused and become uninterested in the vast number of technical jumbo on there. KDE needs a nice walk through its new features with screenshots, real world examples and down to Earth language that anyone can understand. Checkout GNOME 2.10's \"What's New\" section for an example: http://gnome.org/start/2.10/notes/rnwhatsnew.html\n\nFurthermore, KDE needs to do this as soon as KDE 3.4 is released, not a month later. Marketing and release efforts should be coordinated to work with the greatest effect. Right now the KDE websites are a mess and often contain information about old KDE releases. For example, after all this time, KDE still has not updated the screenshot's section. In addition, the widely touted flash guide of KDE 3.4 features is still nowhere to be found, and after all this time, its potential use is diminishing. That's just terrible.\n\nIf KDE intends to coninue to be a successful project, it will need to market itself better. It will need to entice users with new features for each release in an easy to follow manner. It will need to organize promotions. It will need to actively organize fun raisers. It will need to coordinate marketing and development. Etc. As any successful company will tell you, marketing is as much as 50% of the equation. You may have he best technology, but if a user's perception does not reflect that, you won't get anywhere. I love KDE, but the lack of marketing, the pathetic websites updated once a year for its projects, the lack of decent fund raisers, etc. are worrying me. At this rate KDE will have as many developers as it does users."
    author: "Matt"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "I am working on the feature guide.\n\nWait till you see it before you complain. \n\nHow about you helping instead of complaining?\n\nI always wonder about the things possible if more people would actually go out and promote KDE instead of telling others what they should do.\n\nWell, one can dream.\n"
    author: "Rainer Endres"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "Tell us how to help...I'd be an interested candidate."
    author: "nope"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "> Tell us how to help...I'd be an interested candidate.\n\nI think the KDE 3.4 feature guide is almost done, but if you'd like to help, drop an email to kde-quality@kde.org explaining what you'd like to do, and they can help you to get started.\n\nIf you're particularly interested in the Feature Guide and similar documentation, you might like to drop by #kde-docs on irc.freenode.net, where we can discuss how you can help.\n\n"
    author: "Philip Rodrigues"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "Please contact kde-quality [1]. Really there is enough to do even for non-coders. \n\nCiao' \n\nFab\n\n\n[1] https://mail.kde.org/mailman/listinfo/kde-quality"
    author: "Fabrice"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-29
    body: "http://kde.org/support/"
    author: "ac"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "First reason to complain would be because it was not published when KDE 3.4 was released I think."
    author: "Anonymous"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "We are all voluntarees.\nNobody is paid to  do this work and we are all doing it in our free time.\nWe are not a company.\nSure KDE prefers to put more energy behind the coding rather than the PR.\nThis may be wrong for some people but i prefer it this way.\nI fear the days when its the other way around.\n\n\n\n"
    author: "anon"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "\"We are all voluntarees.\"\nMostly\n\n\"Nobody is paid to do this work and we are all doing it in our free time.\"\nNot true.  Many KDE developers are sponsored by various companies to do KDE work.\n\n\"We are not a company\"\nWell, there is KDE e.V. at http://www.kde.org/areas/kde-ev/ plus all of the companies that are sponsoring individual hackers, etc.  Generally speaking, KDE e.V. makes the important decisions related to how to spend donations (bandwidth, etc.)\n\nI take small offence to speaking on behalf of KDE as 'We' whilst posting anonymously.  Especially when some of what you say is misleading and could be interpreted by users as truths spoken. "
    author: "Troy Unrau"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "Addendum:\n\nafter physos on IRC let me know that currently, we are down to one sponsored KDE developer at this time, I'd like to ammend \"Many KDE developers are sponsored by various companies to do KDE work.\" to read \"Several KDE developers have been sponsored in the past, or work(ed) for a KDE friendly company.\"\n\nThere are a handful working for Novell or Trolltech mostly, that are also KDE contributors, plus paid work on occasion that makes its way back into KDE, such as Everaldo's icons (which were originally made for SuSE)."
    author: "Troy Unrau"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "First sign of insanity is to reply to oneself.\n\nMessage from physos:\n    troy: uh, the first crystal set was made for Conectiva/Mandrake :}\nMessage to #kde-freebsd:\n    physos: didn't Suse pay for them on contract though?\nMessage from physos:\n    troy: Not in the begining. crystal was for Conectiva and became KDE default at one point, thats when United Linux an thus SUSE money got in the crystal game, since they wanted their own apps to adjust to the look."
    author: "Troy Unrau"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "Next time I will anwser here instead of tracking you on IRC. ;)"
    author: "Rainer Endres"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "> after physos on IRC let me know that currently, we are down to one sponsored KDE developer at this time...\n\nWhy not say who this is so that we can point out how erroneous it is? I believe David Faure still is sponsored for part of his time and I'm aware of a developer sponsored for KDE development on OO.org. I believe there are others and then I sponsor Andras Mantia full time on Quanta and Michal Rudolf part time on Kommander. What I'm aware of is that typically we have a dozen or so donations a month and are down to about one or two a month to help me with sponsorship.\n\nIn addition to this we have several sponsoring individuals and KDE's annual events have several companies who participate in sponsoring. None of this counts people working at KDE friendly companies who work on KDE in their free time. When you add it all together it is a significantly helpful amount that adds up to a tiny fraction of what is possible. We are trying to figure out how to afford to travel to place we've been invited where funding is an issue.\n\nEven where development is sponsored it is done on a fraction the cost of traditional development and stretched to the limit of potential. However anyone pointing a finger at developers and saying \"you guys\" is totally missing the concept of community development. Without the effort and sacrifice of a fraction of a percent of users who are also developers there would be nothing to use. Not all contributors are developers either, but all developers and contributors are users. \n\nSo here is 5 years of FLOSS project leadership speaking. If 1% of users were contributors there would be no deficiencies. Users who are only using and beating up the less than 1% of users who are contributing for not contributing enough are exhibiting a severe failure to understand how to improve their user experience. The vast majority of users are capable of making some small form of contribution but operate on the assumption that they are incapable because they were not born programming complex algorithms or some other silly thing that is more a matter of their not considering the possibility. Like the parable of the tourtise and the hare, desire almost always overcomes native ability.\n\nMaking a contribution of some sort is very rewarding... until you're spending all your free time and people are asking you why you can't do more. As someone who was not prepared to become the one person between whether a very popular application grew or died I know that free software is meaningless unless those freedoms are exercised. When you see something not as you like ask yourself \"If I don't make this happen who will?\" In my case I was that one person for Quanta in 2001. Webmaker was last built for KDE 2 and Quanta very nearly ended there too. It's easy to assume someone else will do something, but a lot of times those assumptions prove disappointingly wrong.\n\nKDE is not a closed community. You can be a part. Just pick a program you care about and ask how you can help. You want to know who makes KDE better? It's people who care. If you care enough to be bothered by something that is not getting done then you at the threshold of caring enough to do something. Complaining will frustrate you and doing will fulfill you. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "Very well said, Eric!"
    author: "Jason Harris"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "eric, you really did a great job, and I, as 'just a user', than you for that. But I'd like to point out, many on these fora (maybe not the user you replied on) help in other places. from answering questions on fora to developing a distribution. building rpm's to writing how-to's. so I think even if 1% of the users contributed, not all of these would help KDE, if they use it.\n\nBut I actually agree on most you said, just pointing this out :D"
    author: "superstoned"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "Why were there more sponsored KDE developers in the past? Are community-driven distros killing the ones developed by companies?"
    author: "blacksheep"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-28
    body: "Cut the bs, KDE devs, can do more in free time than developers working on different DE, why? half of the job is already done because they don't have to worrie to much about APIS, just take all the Qt framework and work with it, other DE does't have that priviledge so stop complaining about \"Free time\".\n\n"
    author: "U"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-29
    body: "Sure, we all know that KDE is just a blown up Qt. I bet you even prefer running solely Qt as window manager, desktop environment and file/internet browser for that reason. *rolls eyes"
    author: "ac"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-29
    body: "When you just use an API and you dont have to worrie about creating it or for maintenance then half or your job is already done, and ain't the KDE devs slogan that you are productive with Qt/C++ and do the work in less time and with less effort? and now they whine about the lack of time?\n\nOther DE who write it from the scrash from creating APIS and maintenance in a language like C doesn't seem to whine, so I say again, cut the BS."
    author: "U"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-30
    body: "KDE doesn't offer any API on its own? People can't lack time when a development framework per se is effective? You are calling BS something which is barely publically mentioned while some other projects go openly begging for donations for being able to keep running its foundation bureaucracy? And what other DE wrote its API from scratch in a language like C? Enlightenment, true, I'm looking forward to finally seeing their long awaited DR17 while KDE keeps getting featurefull releases about every half a year but still also hoarding plenty of ideas which aren't realized due to developers lacking required time.\n\nYou are such a cute troll."
    author: "ac"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-30
    body: "To much words w/o meaning.\n\nYou didn't answer my question, so is that your way to distract the subject and skip it?\n\nAnd the KDE APIS you mention are nothing but a fork of Qt, so let that \"KDE owns me\" atttitude and get the facs.\n\nso if even with Qt/C++ KDE devs are lacking of time that makes me think:\n\n1.- Is just a lame excuse of something they forgot to do.\n\n2.- Despite the good tools they use like Qt They are not good enought to get the work done.\n\n\nI won't even anser to this stupit topic anymore, and less to a blind person like you. \n\nperiod.\n"
    author: "U"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "Since it seems you're very good at writing, why don't you volunteer? ;-)\nhttp://quality.kde.org/"
    author: "Anonymous"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-26
    body: "Just remember, KDE developers are just users who took the initiative, and got involved in order to improve KDE. So, take the initiative - get involved with the kde-promo team, or the docs team, and help to improve the things you think are lacking."
    author: "Philip Rodrigues"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "You know what, I actually will try to help.\n\nBut that's not the point.  It is simply that KDE should synchronize marketing efforts with releases. For example, when there is an announcement for KDE 3.4, that's when the feature guide and screenshots should be available. I know it is a volunteer project, but I still think this can work. And don't forget that being free does not gurantee users, even FOSS still needs good marketing.\n\nI also can't say that KDE needs money enough times. You guys are not gettign the donation $$$ that you have the potential to receive. This is not just the fault of users, but also KDE's resistance to promoting fundraisers or \"begging\". GNOME does that for a while now, in fact 50% of the space on their website was once dedicated to fundraising for a few months. You may remember the \"Hey kids! The Gnome foundation needs your support.\" graphic. This is not shameful and it will bring in more $$$."
    author: "Matt"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-27
    body: "Hey Matt, \n\nPlease join kde-promo [1] as you seem to have a lot of good ideas. To be honest ideas is something we are never short of as we need people to realize these cool ideas. Please join and help. \n\nCiao' \n\nFab\n\n[1] https://mail.kde.org/mailman/listinfo/kde-promo\n\n"
    author: "Fabrice"
  - subject: "Re: This is getting ridiculous!"
    date: 2005-03-29
    body: "I do find the in the face begging for donations by GNOME shameful, and I find it insulting that you are thinking that such a bland thing would be a good idea for KDE to do the same. I'd rather like to see more efforts punt into guiding potential contributer of 'whatever they are willing to contribute' through http://kde.org/support/ and the Quality team."
    author: "ac"
  - subject: "how about svg?"
    date: 2005-03-27
    body: "don't you think a svg map would be better suited than plain jpeg for that kinda stuff?\nwe could zoom easily without clicking on another jpeg all the time. Plus it would be great to show the power of ksvg. There are plenty of project on the net providing svg map:\nhttp://www.google.com/search?q=world+svg+map&ie=UTF-8&oe=UTF-8\n\nI found that one in particular: \nhttp://jibbering.com/2002/8/foaf-people-map.svg\n\nthat one would kick ass :)\n\nPat"
    author: "Pat"
  - subject: "jabber map could be cool too :)"
    date: 2005-03-27
    body: "http://ralphm.net/world?language=en hehe"
    author: "Pat"
  - subject: "Re: jabber map could be cool too :)"
    date: 2005-12-03
    body: "http://jobble.uaznia.net/map\nnew way to show yourself :)\nregistration: add bot livedelu//jobble.uaznia.net (replace // with @) to your roster, choose a language and follow the instructions :) enjoy :)"
    author: "koniczynek"
  - subject: "my contribution."
    date: 2005-03-28
    body: "I'd like to contribute libgaim to kopete."
    author: "ricky"
  - subject: "Re: my contribution."
    date: 2005-03-28
    body: "Maybe you would have more luck advertising your likes and your dislikes on your blog?  I'm not sure people will see your comments and feelings here."
    author: "ac"
  - subject: "Re: my contribution."
    date: 2005-03-28
    body: "Interesting that you want to contribute that. But instead, how 'bout contributing to the code base that's already in kopete? We've already replaced the backend support code for the aim/icq protocol several times. The current code for aim and icq has great potential, but simply needs more people working on it who know things about how the oscar protocol works for it to gain critical mass in terms of features. I'm sure the kopete team would welcome any contribution you could provide in terms of coding ability. They're really short on coders right now, but I don't think using libgaim is the answer."
    author: "Matt Rogers"
  - subject: "Central European/German phenomenon"
    date: 2005-03-29
    body: "So, this map clearly shows that KDE is a German/central European phenomenon.\nThanks! Enough proof :-)"
    author: "blubb"
  - subject: "Re: Central European/German phenomenon"
    date: 2005-03-29
    body: "\nAs is any other Open Source Project.\n\n\nFor example:\nhttp://www.debian.org/devel/developers.loc\n\nThe distribution is more or less the same.\n\n"
    author: "Rainer Endres"
  - subject: "Re: Central European/German phenomenon"
    date: 2005-03-29
    body: "I was trying to point out that this is one of the reasons why KDE is more popular in central Europe than let's say in the states."
    author: "blubb"
  - subject: "Re: Central European/German phenomenon"
    date: 2005-03-30
    body: "> I was trying to point out that this is one of the reasons why KDE is more popular in central Europe than let's say in the states.\n\nI don't think this is the reason, because some projects are less popular in Europe even though most developers come from Europe: \n\nhttp://live.gnome.org/GnomeWorldWide"
    author: "falonaj"
---
The <a href="http://worldwide.kde.org">worldwide.kde.org</a> <a href="http://worldwide.kde.org/pics/smallmap.jpeg">contributors map</a> has hot fresh updates. The contributor map on worldwide.kde.org shows developers, translators, doc writers, artists, packagers and other contributors of KDE in all the world. If you are a contributor to the KDE Project, <a href="http://worldwide.kde.org/form.php">submit your coordinates</a>.


<!--break-->
<p>Beside the map with night effect on the main page, it features:
<ul>
 <li>4 more map sizes</li>
 <li>zoom on Europe so you can see more names</li>
 <li>zoom on Central Europe so you can see all names</li>
 <li>maps with nicks</li>
 <li>and <a href="http://worldwide.kde.org/webimages/">web images</a></li>
</ul>
</p>

<p>You want to show your dedication to KDE on your webpage? Why not use the new webimages. They are available in several sizes, and feature a little KDE logo so they are a nice way to link to kde.org on your page.
</p>
<p><a href="http://worldwide.kde.org/webimages/"><img src="http://worldwide.kde.org/pics/webimages/fullmap/120x60_tinymap.jpeg" /></a>
<a href="http://worldwide.kde.org/webimages/"><img src="http://worldwide.kde.org/pics/webimages/fullmap/150x75_tinymap.jpeg" /></a>
<a href="http://worldwide.kde.org/webimages/"><img src="http://worldwide.kde.org/pics/webimages/fullmap/180x90_tinymap.jpeg" /></a>
</p>


