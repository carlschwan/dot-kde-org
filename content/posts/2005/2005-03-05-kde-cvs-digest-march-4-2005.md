---
title: "KDE CVS-Digest for March 4, 2005"
date:    2005-03-05
authors:
  - "dkite"
slug:    kde-cvs-digest-march-4-2005
comments:
  - subject: "Introducing bugs..."
    date: 2005-03-05
    body: "From the digest: \"Notice all the bugs that were backported.\"\n\nWhy the hell did they do this?"
    author: "ac"
  - subject: "Re: Introducing bugs..."
    date: 2005-03-05
    body: "Probably for compatibility reasons.  You don't want KDE behaving differently because a bug was inadvertently fixed.  Such major changes require a new major version number."
    author: "Anonymous"
  - subject: ":-)"
    date: 2005-03-06
    body: ":-)"
    author: "Xedsa"
  - subject: "Re: Introducing bugs..."
    date: 2005-03-05
    body: "It should read bug*fixes*, not  bugs.\n\n"
    author: "anon"
  - subject: "Re: Introducing bugs..."
    date: 2005-03-05
    body: "Probably for testing purposes."
    author: "ca"
  - subject: "Re: Introducing bugs..."
    date: 2005-03-05
    body: "No, it's because of the sun spots and the American national debt. And global warming, of course."
    author: "anon"
  - subject: "Re: Introducing bugs..."
    date: 2005-03-06
    body: "Your sarcasm is uncalled for.  The man asked a legitimate question."
    author: "ca"
  - subject: "Re: Introducing bugs..."
    date: 2005-03-06
    body: "What legitimate question?  Especially, what question that hasn't been cleared up yet?\n\nHe stumbled over a little inaccuracy in the digest as I've already explained above.  If you had read and understood my comment you wouldn't have tried to find a rationale for something that didn't happen. \n\nI can't believe we're having this discussion...\n\n"
    author: "anon"
  - subject: "Stress tested RC1 ..."
    date: 2005-03-05
    body: "...the only major bugs still left were:\n\n1) More often than not the new tooltips remained partly on screen\nafter closing and they were always painted with a black background\nso I finally had to disable them. No problem though since they\nwerent really important\n2) Logging in on SSH servers from the remote protocol resulted\nin KWallet showing completely wrong user credentials I had to\ncorrect everytime.\n\nOtherwise: Thanks for the most stable release ever AFAICT!\n\nKPDF with disabled DRM by default??\nNot that I wouldnt support to leave out this stupid stuff \nand this would certainly make KPDF a killer-app for PDF viewing BUT:\nI wonder how will Adobes legal team will react on this?\nThey already sued elcomsoft. They lost AFAIK but who wants to\ngo through a legal battle with them.\nAnyway, if it comes back in there's still \"Advanced PDF Password Recovery\" ;-)\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: Stress tested RC1 ..."
    date: 2005-03-05
    body: "The statements in the PDF spec about \"copyrighted\" data structures are pure legal BS.  Adobe cannot sue you under copyright law for ignoring their DRM, no matter what their lawyers want you to think.\n\nIt is possible that Adobe has patents on ways of implementing the PDF spec, which is a completely different story.  If KPDF infringes any Adobe patents, then they could sue.  But in this case KPDF is already violating the patents, so removing the DRM wouldn't put KPDF in a worse legal position.  Also, the patents probably only apply in the US (at least for now), so many KPDF developers and users would not be affected anyway.  \n\nAlso, it is possible that the DMCA could make it illegal to use KPDF to remove the DRM from PDFs.  However, I don't think it would make KPDF itself illegal (since KPDF is very useful apart from this feature), and I don't think the KPDF developers could conceivably be charged with any crime under the DMCA.\n\nFinally, if it did go to court, I'll bet the EFF would be happy to represent the KPDF developers."
    author: "Spy Hunter"
  - subject: "Re: Stress tested RC1 ..."
    date: 2005-03-05
    body: "> More often than not the new tooltips remained partly on screen\n> after closing and they were always painted with a black background\n> so I finally had to disable them.\n\nusing a buggy composite manager?\n\nthere is one known bug left in 3.4.0 for the new mouse overs. it has to do with the \"show desktop\" functionality. it's fixed in 3.4.1 though."
    author: "Aaron J. Seigo"
  - subject: "Re: Stress tested RC1 ..."
    date: 2005-03-05
    body: "Thanks for answering.\nNo composite manager - just plain SuSE 9.2.\nDont know why the tooltips are black and sometimes remain \non screen but it's not a big problem. I disabled them and do \nnot really miss them ;-)\n"
    author: "Martin"
  - subject: "Re: Stress tested RC1 ..."
    date: 2005-03-06
    body: "do they remain on screen when using the show/hide desktop button?\nand would it be possible to get a screenshot of the black ones?\neven if you don't use it, i can use this information to make it better for those who do =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Stress tested RC1 ..."
    date: 2005-03-06
    body: "There is also an X11 priority issue.\n\nIn some cases, the popup windows have a lower priority than the panel that they pop up from.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Stress tested RC1 ..."
    date: 2005-03-05
    body: "KPDF still has DRM enabled by default, the thing is that you can disable it now via a GUI option instead of a ./configure option"
    author: "Albert Astals Cid"
  - subject: "Amarok icon change?"
    date: 2005-03-05
    body: "From the amarok changelog for 1.2.1:\n\nCHG: Replaced \"Blue Wolf\" icon with Nenad Grujicic's amaroK 1.1\n          icon, due to legal issues.\n\nAnyone know what legal issues there were?  They replaced the new icon from the kde look contest with the old one?"
    author: "Leo"
  - subject: "Re: Amarok icon change?"
    date: 2005-03-05
    body: "Seems the creator of the icon has sold the copyright on it before putting it up to the contest. Bad idea."
    author: "Andy"
  - subject: "Re: Amarok icon change?"
    date: 2005-03-05
    body: "That's not true, nobody sold anything.\n\nThe icon was inspired by some clipart that was found on the web and this clipart happened to be very similar to the logo of a certain company. As the copyright holder, that company had not given permission for the distribution of the clipart in the first place and considered it a violation of its copyrights and trademark. Since the amaroK icon and the clipart were rather similar as well, the company held the same position wrt the amaroK icon.\n\nThe lesson that can be learned from this is that you shouldn't _EVER_ contribute artwork to a free software project that isn't totally original and completely made by you. It can bring the project in real problems and result in embarrassing situations for everyone involved, you may also be left with a substantial bill from your laywer.\n"
    author: "Waldo Bastian"
  - subject: "Re: Amarok icon change?"
    date: 2005-03-05
    body: "Then they really should use the second place from the contest, I thought it was better than the winner anyway:-)"
    author: "Morty"
  - subject: "Re: Amarok icon change?"
    date: 2005-03-05
    body: "It's a pity. The wolf looked great. The current one looks like nothing."
    author: "Carlo"
  - subject: "Re: Amarok icon change?"
    date: 2005-03-06
    body: "1.3 should have a new icon. The current icon scales and looks professional enough to be used until then."
    author: "Ian Monroe"
  - subject: "Re: Amarok icon change?"
    date: 2005-03-06
    body: "its a bloody shame, i was quite fond of the new icon. not that it matters, amarok will continue to be an excellent media player regardless of what icon they use."
    author: "Chris Cobb"
  - subject: "Lots of KHTML development"
    date: 2005-03-05
    body: "Didn't knew that KHTML development is still this strong. I counted seven developers that contributed to khtml in some way in this cvs-digest (btw thanks Derek!). Thanks to\n\nKoos Vriezen\nDirk Mueller\nAllan Sandfeld Jensen\nHarri Porten\nGermain Garand\nLeo Savernik\nDavid Faure\n\nfor their improvements to the best html rendering engine :-)\n"
    author: "MK"
  - subject: "Re: Lots of KHTML development"
    date: 2005-03-05
    body: "In case you can, could you inform an ignorant KDE user why this site http://www.toronto.ca/ttc/schedules/index.htm renders poorly/badly in KDE's latest Konqueror release (3.4rc1).  When one selects a route and clicks \"GO\" there should be stations/stops on the left, which when clicked, times show on the right. There is no problem with it in Firefox. Should this be filed as a bug to the developers? Changing the browser identification does not help either.\n\nThanx."
    author: "charles"
  - subject: "Re: Lots of KHTML development"
    date: 2005-03-05
    body: "File a bug to the Toronto transit people.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Lots of KHTML development"
    date: 2005-03-05
    body: "Two words: BLAME CANADA\n\n-- \nOh my God they killed Kenny! :D"
    author: "Kryczek"
  - subject: "Re: Lots of KHTML development"
    date: 2005-03-07
    body: "I am using 3.3.2, and everything works except when you click on the station, the times show on the right in a really small frame.  About 100 pixels in height.  However, I can \"grab\" the bottom of that frame with my mouse--it's a little tricky because the frame border is invisible.  Once you grab the frame border, you can drag it down to the bottom of your browser and now everything looks normal.  It seems either Konqueror isn't respecting their frame sizes correctly or the damn Canadian's have a bug in their frame's definitions."
    author: "Henry"
  - subject: "Re: Lots of KHTML development"
    date: 2005-04-12
    body: "Works for me konqueror 3.4.\nNo problems at all.\n"
    author: "Matthew Churcher"
  - subject: "Digikam Plugins"
    date: 2005-03-05
    body: "Is there any plan to put all the cool plugins into a shared library so that every KDE application (Krita comes to my mind, but there may be others too) can make use of them? That would be really great."
    author: "Anonymous"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-05
    body: "i believe that's what KIPI* is.\n\n\n* KDE Image Plugin Interface"
    author: "Aaron J. Seigo"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-06
    body: "No no no :\n\nKipi multi-images traitements like batch processes using by photo management programs like digiKam, kimdaba, showimg, gwenview.\n\nDigikamImagePlugins: tools to transform/correct/improve a single photograph at the same time (like in gimp for example). It's used only in digiKam image editor !\n\nThere no plan to export this tools from digiKam project. There is too depencies with digiKam core and this is a non sence (for me) to put these parts in a shared lib (I won't destroy digiKam project !!!).\n\nA nice day\n\nGilles Caulier\n\n"
    author: "Gilles Caulier"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-07
    body: "if other projects could use the digikam plugins (in digikam only used to improve photo's, isnt it?) they would only become better (tested, and they might send patches and write new plugins). digikam's power is not in its plugins (I still use the gimp or kolourpaint, or krita) but in its great interface, search-and-meta-data capabillities. I'd prefer to see you working on these (example: please, let me be able to use the name of the file as meta-data if there is no metadata yet) instead of the plugins. I'd say let digiKam work with krita/kolourpaint/gwenview/etc to create a great pictureviewer with a plugin structure to enhance pictures, and concentrate on digikam itself..."
    author: "superstoned"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-07
    body: "Well, there are some kind of plugins/filters that are really useful to have at hand while using digikam cause they are strictly related to photos management. If I should have to open a particular image app only to sharp a too blurred photo I'm viewing in digikam it would be really uncomfortable."
    author: "Davide Ferrari"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-09
    body: "well, he really shouldn't remove the photomanagement tools, as they are cool. b ut would it not be possible to use eg kolourpaint embedded as a kpart? or gwennview? and as far as these aren't good enough, I'm sure their authors would accept some patches ;-)"
    author: "superstoned"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-09
    body: "Sorry for losing track of this thread -- I'm been down with influenza at the moment. I thought that Kolourpaint also used the Digikam plugins to a certain extent? I must admit that I get confused by all this stuff myself... "
    author: "Boudewijn Rempt"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-05
    body: "In theory, yes, and I've been looking at it quite closely, as has Cyrille. There are a few problems, though. Many kipi plugins are more oriented to batch edits, which doesn't fit Krita. Then, the digikam image plugins seem to work on QImages only, which would mean that Krita would have to convert every layer to a QImage, filter it, and convert it back to its internal representation. It would have been easier to feed pixels to the filter pixel by pixel, but of course, some types of filter don't work on single pixels. \n\nIn any case, I think a Krita kipi/digikam ueber-plugin that loads and makes available all relevant plugins would be easy to do and will probably happen some day. It just means that if you use those filters on you cmyk image of 16-bit rgba image you're going to lose some precision."
    author: "Boudewijn Rempt"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-05
    body: "have you had a look at ksquirrel.sf.net and its squirrel-libs?\n\nI had the impression that kipi is more batch-filters, print-dialogs and other \"features\", while ksquirrel-libs is just plain image-codecs."
    author: "me"
  - subject: "Re: Digikam Plugins"
    date: 2005-03-06
    body: "... and another image viewer using openGL... We don't need to use opengl to pay with photographs...\n\ndigiKam include now an imagefilter in a core dedicaced to improve/transform/correct photographs.\n\nA nice day.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "amaroK"
    date: 2005-03-05
    body: "AmaroK does not like the gstreamer engine. It crashes everytime I select it for use. What I do not like about aRts is the enormous delay between the equalizer display and the music out of the speakers. The display seems to be doing its thing at random! XMMS does not show this. I heard gstreamer would solve this problem."
    author: "charles"
  - subject: "Re: amaroK"
    date: 2005-03-05
    body: "Read the documenation of amaroK, it explains why the analyser is not in sync when using aRts"
    author: "ac"
  - subject: "Re: amaroK"
    date: 2005-03-05
    body: "GStreamer works here, in fact it's the only one that does =). That's the advantage of multiple backends, you can never tell which one will be borked on any particular machine...\nTry Xine."
    author: "Illissius"
  - subject: "Re: amaroK"
    date: 2005-03-05
    body: "I can recommend xine. It works perfectly here.\nArts is always skipping and sometimes outputs garbled sound\nand GStreamer does not work at all - even on the command line.\nI found out on the web that this is a known issue and already fixed\nin CVS but I didnt really want to go through all the hassles of\nrecompiling GStreamer.\nThe multiple backend thing is really cool because now I still\ncan listen to my music collection comfortably without using \ncommand-line tools.\n\n"
    author: "Martin"
  - subject: "Re: amaroK"
    date: 2005-03-05
    body: "Sticked with xine, too. gstreamer, akode and arts are buggy OR are too cpu intensive."
    author: "Carlo"
  - subject: "Re: amaroK"
    date: 2005-03-06
    body: "Indeed, arts is very cpu hungry, gstreamer is very laggy (I hit play/pause/stop and it doesn't actually play/pause/stop until 0.5 to 1 second later).\n\nXine is the only one with acceptable performance..  Xmms with OSS is still much more responsive, but I can deal with the short lag of Xine in trade for all the awesome amaroK features."
    author: "Leo S"
  - subject: "Re: amaroK"
    date: 2005-03-06
    body: "If you like Xine, you'll love mplayer.  mplayer plays pretty much *any* video or audio codec you can throw at it and it's much less hassle than even on Windows once you install the right packages. =)"
    author: "ca"
  - subject: "Re: amaroK"
    date: 2005-03-06
    body: "MPlayer isn't a back-end for amaroK AFAIK..."
    author: "Corbin"
  - subject: "Re: amaroK"
    date: 2005-03-06
    body: "They're talking about xine-lib as a backend to amaroK, not xine the video player."
    author: "Ian Monroe"
  - subject: "Re: amaroK"
    date: 2005-03-06
    body: "> mplayer plays pretty much *any* video or audio codec you can throw at it \n\nNowadays, xine does pretty well in that department, too.  Kaffeine (using the xine-libs) just works for me in most cases. \n\n\n"
    author: "cm"
  - subject: "Re: amaroK"
    date: 2005-03-06
    body: "In response to the laggy gstreamer thing, consider voting for this bug: http://bugs.kde.org/show_bug.cgi?id=96680"
    author: "Ari"
  - subject: "Old layout was much better"
    date: 2005-03-05
    body: "The old layout contained a table with the icons which allowed fast access and a very good overview.\n\nWhy was it removed?\n"
    author: "Roland"
  - subject: "Re: Old layout was much better"
    date: 2005-03-06
    body: "you're beating a dead horse"
    author: "ac"
  - subject: "Key question"
    date: 2005-03-06
    body: "Will SUSE 9.3 Pro have KDE 3.4?\n\nPlease let it be yes!"
    author: "Al"
  - subject: "Re: Key question"
    date: 2005-03-06
    body: "Hard to tell, but Novell seems to put fair amount of money into Gnome..\n\nhttp://slashdot.org/articles/05/03/06/1436213.shtml?tid=131&tid=223"
    author: "anon"
  - subject: "Re: Key question"
    date: 2005-03-06
    body: "700 dollars. Right, KDE has no chance to survive. I guess it's time to switch over.  :-P\n\n"
    author: "cm"
  - subject: "Re: Key question"
    date: 2005-03-07
    body: ":)\n\nTry again by counting people working with G."
    author: "anon"
  - subject: "Re: Key question"
    date: 2005-03-07
    body: "Yes. And also first time a current GNOME version (2.10)."
    author: "Anonymous"
  - subject: "Transparency on Kicker?"
    date: 2005-03-08
    body: "A little cosmetic , but does anyone know if 3.4 will have transparancy support built-in or will it be another hack job. Seems weird that Kicker has trans-support but Taskbar doesnt without hacking it....Anyone?\n"
    author: "Helfrez Gama"
---
In this <a href="http://cvs-digest.org/?newissue=mar42005">week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/?newissue=mar42005&all">all-in-one version</a>):

Beginnings of Subversion support in <a href="http://www.kde.org/apps/cervisia/">Cervisia</a>.
Cleanup of initial application sizing.
<a href="http://www.kdevelop.org">KDevelop</a> adds <a href="http://www.trolltech.com/products/qt/designer/">Qt Designer</a> support for Python.
IDN issues fixed in Konqueror.
<a href="http://digikam.sourceforge.net/">Digikam</a> adds more plugins: Insert Text, Channel Mixer, Infrared, Blur, Distortion, and a new ratio crop tool.
<a href="http://kmail.kde.org">KMail</a> adds an account setup wizard.



<!--break-->
