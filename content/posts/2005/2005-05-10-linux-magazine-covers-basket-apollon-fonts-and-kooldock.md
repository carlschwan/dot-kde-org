---
title: "Linux Magazine Covers BasKet, Apollon, Fonts and KoolDock"
date:    2005-05-10
authors:
  - "jriddell"
slug:    linux-magazine-covers-basket-apollon-fonts-and-kooldock
comments:
  - subject: "Klipper replace"
    date: 2005-05-10
    body: "Btw, when kde will replace Klipper with BasKet? ;)\n\nregards\nFrancisco"
    author: "Francisco"
  - subject: "Re: Klipper replace"
    date: 2005-05-10
    body: "It looks a bit too over-engineered to me to be included into main KDE. But some ideas should really go into klipper. Or even better into a streamlined replacement for kicker.."
    author: "uddw"
  - subject: "Re: Klipper replace"
    date: 2005-05-10
    body: "If BasKet like remembered the last X amount of selections and the last Y amount of copys you made, then it would be a great replacement for Klipper (which half the time doesn't notice I copy something)."
    author: "Corbin"
  - subject: "Re: Klipper replace"
    date: 2005-05-10
    body: "Are you sure you have configured it to do so?\nConfigure Klipper->Synchronize contents of clipboard and the selection\n"
    author: "Morty"
  - subject: "Basket"
    date: 2005-05-10
    body: "Whoa! I didn't know about Basket before! It seems EXACTLY like the app I have been looking for! Now, if only Gentoo got ebuils for the latest version..."
    author: "Janne"
  - subject: "Re: Basket"
    date: 2005-05-10
    body: "Notice that I'm searching BasKet packagers for Gentoo, Mandriva, Fedora...\nFor all distributions that are not packaged yet in the download page in fact."
    author: "Sebien"
  - subject: "Font Catalog"
    date: 2005-05-10
    body: "As \"Alphabet Soup\" article said, it would be cool if KDE could do a \"Font Catalog\" out of the box.\nI remember my mother some years ago who printed the list of all installed fonts on her Windows box... by hand with Publisher!\nShe needed and installed a lot of fonts to make cards/menus/... special events.\nIt would have helped she."
    author: "Sebien"
  - subject: "Install fonts in: \"~/.fonts :-("
    date: 2005-05-12
    body: "Unfortunately, the article fails to mention the pitfalls of installing fonts in your \"$HOME/.fonts\" directory (or any subdirectory of $HOME).\n\nAnd the article fails to explain how to install them in \"$KDEDIR/share/fonts\"."
    author: "James Richard Tyrer"
  - subject: "Handy BasKet"
    date: 2005-05-13
    body: "Just installed BasKet and it seems very handy!\n\n- Swaroop\nwww.swaroopch.info"
    author: "Swaroop C H"
---
<a href="http://www.linux-magazine.com">Linux Magazine</a> has made available a number of KDE related articles for download.  <a href="http://www.linux-magazine.com/issue/55/Installing_Fonts_in_KDE.pdf">Alphabet Soup</a> covers installing fonts in KDE.  <a href="http://www.linux-magazine.com/issue/55/Apollon.pdf">Olympian Exchange</a> explores the file sharing application <a href="http://apollon.sourceforge.net/">Apollon</a>.  <a href="http://www.linux-magazine.com/issue/54/KDE_Basket.pdf">A Virtual Basket</a> looks at <a href="http://basket.kde.org/">BasKet</a> the ultimate scrap book for KDE.  Finally <a href="http://www.linux-magazine.com/issue/54/KoolDock_KXDocker.pdf">On the Docks</a> looks at two replacements for the panel, <a href="http://ktown.kde.cl/kooldock/">KoolDock</a> and <a href="http://www.xiaprojects.com/www/prodotti/kxdocker/main.php">KXDocker</a>.

<!--break-->
