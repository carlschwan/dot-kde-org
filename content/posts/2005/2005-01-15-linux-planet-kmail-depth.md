---
title: "Linux Planet: KMail In Depth"
date:    2005-01-15
authors:
  - "jriddell"
slug:    linux-planet-kmail-depth
comments:
  - subject: "Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-14
    body: "... like Thunderbird or Operamail did?\n\nIs there a patch or is this an option in near future? I think, it made your/my day instead of boring text smileys. Pls look at Kopete!\n\nIf you made these smileys in Kmail (and maybe Knode?) themeable, then I don't want to think about what we'll see on kde-look.org soon ;-)\n\nThank's for your answers!\n\nPS: it's not a security hole, textbased mails will be text based. It's only the rendering. And for the \"serious\" users here, there should be an option to disable this goodie of course...\n"
    author: "anonymous"
  - subject: "Re: Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-14
    body: "i'd say file a wish on bugs.kde.org. or ask some coder you know to try to make a patch."
    author: "superstoned"
  - subject: "Re: Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-14
    body: "<a href=\"http://bugs.kde.org/show_bug.cgi?id=83388\">bug 83388</a> is what you wanna vote for!"
    author: "ac"
  - subject: "Re: Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-15
    body: "Thank you very much too for your informations. That's what I want to see in Kmail. It's not a feature, it's only a very nice goodie!\n"
    author: "anonymous"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-15
    body: "are you kidding me?  I can't believe so many others are supporting this idea... Graphical smileys are _not_ a good thing.  If you're going to add the feature, please disable it by default!\n\nIf KDE is used in business settings, such eye-rollers can't do much for KDE's reputation..."
    author: "Logan"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-15
    body: "How wrong you are.  Microsoft Outlook, the gold standard for PIM clients, used at businesses worldwide, automatically replaces any smiley you type with the stupid ugly smiley-face from the Wingdings font.  KMail's smileys (and I've heard that they have already been added for 3.4) are going to look much better than THAT for sure."
    author: "Spy Hunter"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-15
    body: "a) How's it going to be represented in the email?  Is it going to remain as :) (colon + close parenthesis), or is it going to be converted to a standard smiley character (U+263A, &#9786;)?\n\nb)  If the latter, what character encoding is going to be the default (US-ASCII won't cut it)?\n\nc) What compatibility issues arise from switching character encodings?\n\nd) If you actually mean to type a colon and a close parenthesis, how do you get it to not do it?\n"
    author: "Jim"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-15
    body: "Only Outlook does that weird conversion thing, and I believe it simply changes the font to wingdings and types a letter \"J\", as opposed to using the real unicode smiley character.\n\nIf it works in KMail the way it does in Thunderbird, it is something that happens only when you view other people's emails, not during composing (unless you explicitly select a smiley from the smiley button in the toolbar, but it still sends only the text version).  So it won't convert any characters to anything else or use any special encodings.  However, it will mess you up if you are trying to read an email that legitimately has the character sequence :) not representing a smiley.  Luckily these emails are few and far between, and there will of course be an option to turn it off.  Furthermore if you copy and paste the smiley into a text editor it will hopefully come out as :), so copy and paste of code snippets should still work even if they have smileys in them.  I can't guarantee that's how it works in KMail, but that's how it *should* work."
    author: "Spy Hunter"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-17
    body: "\"However, it will mess you up if you are trying to read an email that legitimately has the character sequence :) not representing a smiley. Luckily these emails are few and far between\"\n\nI agree, but how about \"(?)\"? I have a tendency to write this (when I just wrote something that I'm not entirely sure of), and the annoying \"ASL?\"-figure is drawn. Sure I can turn it off, but that won't help the recipient much. Also, when discussing C++, a really common thing is writing for example std::string, where \":s\" will be rendered as a smily with an \"s\"-mouth.\n\nThese examples are from kopete, but I guess it's true for more IMs. I like the smilies, but they sure can be annoying when you didn't really want one there."
    author: "sacrebleu"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-17
    body: "Wow, kopete must has a pretty comprehensive set of smiley replacements.  I guess that is to be expected in an IM client.  Hopefully KMail does not go quite that far in its smiley support."
    author: "Spy Hunter"
  - subject: "Re: Kmail 2 render smiley pictures in text based m"
    date: 2005-01-26
    body: "Oh, please.  Since when is Microsoft Outlook the yardstick by which software should be measured?  Or are you one of those people whose car gets 40 rods to the hogshead, and that's the way you like it?"
    author: "Paul Johnson"
  - subject: "Re: Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-14
    body: "In the \"In Progress\" section of the KDE 3.4 Feature Plan [1], there's a listing under Kmail that says \"Smileys\" which may or may not be what you're wanting. \n\nFWIW, an Anonymous Coward over at Slashdot mentioned that having emoticons in Kmail is one of the new features.\n\n[1] <http://developer.kde.org/development-versions/kde-3.4-features.html#inprogress>"
    author: "Jefffrey M. Smith"
  - subject: "Re: Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-15
    body: "Thank you very much for your information, hopefully this is what I want!\n"
    author: "anonymous"
  - subject: "Re: Kmail 2 render smiley pictures in text based mails"
    date: 2005-01-14
    body: "At least to me, a LaTeX plugin like the one in Kopete would be much more handy :) Also, ability to load images on demand would be nice."
    author: "John Icons Freak"
  - subject: "KMail is great"
    date: 2005-01-14
    body: "Not really adding anything usefull here, but KMail is great. KMail is great, again, KMail is great. And so is KDE, and so is KDE3.4, it's all great.\n\nI mean, KMail is great. Can't mention it enough. Hey, KMail is great."
    author: "Leverkusen"
  - subject: "Re: KMail is great"
    date: 2005-01-14
    body: "Meh.  I use it (switched from Mozilla mail) and I'm still not sure if it was a good trade or not.\n\nAdvantages: deals with file associations correctly (this is why I switched)\nDisadvantages: No Cut/Copy/Paste application or context menus in composition windows (hotkeys only?  What is this, EMACS?), no \"format=flowed\" support makes you choose between all of your mail looking bad on other people's machines (linebreaks everywhere!) OR having your mail composition scroll indefinitely off the right side of the window, and the window for choosing address book entries for new mails is so large the buttons disappear off the bottom of the screen (workaround: hit enter for OK)!\n\nIt's rough.  Still.  Yes, all of the above bugs are being worked on.  The Cut/Copy/paste thing will even be fixed in 3.4.  But \"polished\" it is not.\n\nThen again, it opens my attachments in the correct applications, and even uses nice icons.  So that's better than Mozilla.\n\nWait...and when you click a URL in KMail, it for some reason reads the whole page in Konqui (silently) and THEN launches your preferred browser!  No, this is not me forgetting to put %U on the end of the file association, it really does this.  Because it's silent (there's no Konqui window) you can tell if you make Konqui prompt you for cookies and then go to a URL with cookies.  Loading the same page twice is pretty silly.\n\nThere are bugs filed on all of these.  I'm just venting.  I WANT to agree that KMail is great.  Unfortunately, I just find it to be adequate.\n"
    author: "ac"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: "\"No Cut/Copy/Paste application or context menus in composition windows (hotkeys only? What is this, EMACS?)\"\n\nNot so for me (KDE 3.3.1).  Cut/Copy/Paste are in the usual places (toolbar, edit menu, context menu) in the new mail composition/edit menu.  Do you mean something else?\n\n\"no \"format=flowed\" support\"\n\nWould be nice to have I guess, but I don't see that it's really a problem.  Mail that you send does not look bad on other people's computers.  Lines will be a consistant length.  Lots of email programs do this.  I've never heard anyone complain.\n\n\" the window for choosing address book entries for new mails is so large the buttons disappear off the bottom of the screen\"\n\nOnce again, I have no idea what you're talking about.  On my machine the window is very small. Would even fit on a 640x480 screen with room to spare.\n\n\"and when you click a URL in KMail, it for some reason reads the whole page in Konqui (silently) and THEN launches your preferred browser!\"\n\nIt doesn't load the whole page in Konqueror, as evidenced by the fact that if you click on a link, it will pop up konqueror  (on my machine) and then finish loading the page.  It may check if the website can be connected to before launching your preferred browser.  This may be silly but its not a big deal.\n\n\n\n\n\n"
    author: "Leo S"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: "I believe that KMail is checking what type of file is being linked to, so it can choose whether to launch your web browser or some other program (for example a video player).  It doesn't just automatically send any http:// links to your web browser, and it doesn't assume that the file extension is correct.  Personally I think that this is a misfeature because it makes the common case (link to an html page) twice as slow.\n\nSome form of email text flowing is really important.  I just can't stand looking at 10 layers of word-wrapped >>>s any more!  What year is this, 1985?  I think the real fix for this is HTML composition (which I heard is possible now in kmail?).  I don't understand why so many peole hate HTML mail.  IMHO every mail should be HTML, even unformatted ones, just so you get the nice word wrapping and the ability to indent sensibly instead of using >>>.  I don't care about the images and fonts and colors; in fact I think KMail should have a feature to throw those all away when rendering HTML messages, and it should be enabled by default.  Wacky colored fonts and background images in email should die a horrible death, but we shouldn't deny ourselves nice word wrapping and indenting just because some people have bad taste.  It can be fixed with technology! :-)"
    author: "Spy Hunter"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: "> I don't understand why so many peole hate HTML mail.\n\n- because 99% of all html-mail I get is SPAM, and ~95% of all SPAM I get is html-mail\n- because a lot of clients set the font in html-mails to some ridiculously small size making it unreadable with greater resolutions \n- because html-mail (as commonly send by for instance outlook) is often (at least) 30 times as big, as plain-text-mail (and that's without using background images, background music, and similar idiocy)\n- because with html-mail, I'm now at the mercy of people who like sending their mail in a light blue font on a slightly darker blue background, and using hidious font.\n- because using html brings up security concerns (as you can embed all kinds of weird stuff in it)"
    author: "cobaco"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: "1. So get a spam filter, this isn't HTML's fault and it's no reason to ignore the benefits HTML mail can give.\n2. That's why KMail should automatically fix up these types of problems in incoming mail.  It would be easy to remove backgrounds and fix low-contrast or tiny text.\n3. Even large HTML mails are still small enough that they've never caused me any problems.  Outlook's HTML is generated by Word and of course bad, but good HTML isn't much larger than plain text.\n4. See 2\n5.  No embedding is possible in decent email clients (including kmail of course), they forbid it.\n\nIf you use a decent email client, HTML mail is nothing to fear.  The important thing is the simple formatting that HTML allows, like text flowing and justification, lists, tables, and indented quotes.  The the other crap can and should be stripped out by your mail client."
    author: "Spy Hunter"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: "> 1. So get a spam filter, \n Why bother when I can suffize whith just filtering html-mail\n\n>this isn't HTML's fault and it's no reason to ignore the benefits HTML mail can give.\n\nwhat benifits would that be exactly?\nhtml-mail gives the sender more controk about how to display his mails, but IME those that actually use the extra possibilities (most legitimate html-mail does not), use it a way I _really_ don't want\n\n> 2. That's why KMail should automatically fix up these types of problems in\n> incoming mail. It would be easy to remove backgrounds and fix low-contrast or\n> tiny text.\n\nyep, but then why use html-mail, as doing so would get rid of the html stuff (from legitimate html-mail) I've ever got (everything else can be done in plain text)\n\n> . Even large HTML mails are still small enough that they've never caused me\n> any problems. Outlook's HTML is generated by Word and of course bad, \nmy mail archive currently takes up 1.2 GB, times 30 that would take up more then half of my laptops harddisk (leaving to little space for the rest)\n\n> but good HTML isn't much larger than plain text.\ntrue, but I've yet to see such mail (and that's assuming people don't start adding backgrounds and such), the html produced by almost all e-mail clients sucks big time\n\n> 4. See 2\nyep but that kind ogf thing is the only thing I've seen people actually using it for\n\n> 5. No embedding is possible in decent email clients (including kmail of\n> course), they forbid it.\nideally yes, by:\n1. I's one more avenue for attack, one that's not necesary \n2. Most non-geeks, unfortunately, don't use a decent e-mail client\n \n> The important thing is the simple formatting that HTML allows, like text\n> flowing and justification, lists, tables, and indented quotes. \ntables are the only thing a good text editor can't do easily in that list, and I have yet to see an html-email using tables for something other than layout. \n"
    author: "cobaco"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: ">my mail archive currently takes up 1.2 GB, times 30 that would take up more >then half of my laptops harddisk (leaving to little space for the rest)\n\nWhy not archive it? I'm sure a *giggle* \"power user\" *giggle* such as yourself could easily write a script to bzcat it when needed.\n\nYou are what's wrong with linux today: too much conservatorism. You don't want HTML mail? So delete it. Nobody's forcing you to keep your mail. By the same logic, we should all turn our web sites to space formatted .txt files because it would obviously take way less space. It never ceases to amaise me how hardcore adepts of an operating system can be so opposed to new technologies. Anything that makes your work easier is \"bloat\", something a \"real\" user wouldn't look at. Well guess what: you are not a real user! You are just a network administrator or a developer at best. "
    author: "Adrian"
  - subject: "Re: KMail is great"
    date: 2005-01-15
    body: "> You are what's wrong with linux today: too much conservatorism. You don't\n> want HTML mail? \nWhy on earth would I want html-mail? I have yet to find a single real benefit to it. And I know of several (readibility, space, security) real disadvantages. \n\nYou seem to think html-mail is the best thing ever, so enlighten me exactly what makes it so?\n\n> So delete it. Nobody's forcing you to keep your mail. \nI do for html-mail (unless in the extreeeemly rare case it contains something potentially interesting)\n\n> By the same logic, we should all turn our web sites to space formatted .txt\n> files because it would obviously take way less space. \nWeb sites and mail are 2 different media with different requirements, and goals. I suppose you think we should al start to IRC in html-code to? How about SMS, let's start html-coding them also.\n\nNote: I won't oppose anybody using html-mail, when it's sensible and necessary, but I have yet to come across a _single_ instance of that. Html-mail I used to get from relatives and friends (before I educated them), was mostly less then 12 lines of simple text without any formatting at all.\n\n> It never ceases to\n> amaise me how hardcore adepts of an operating system can be so opposed to new\n> technologies. \nWhen those new technologies don't offer advantages, only disadvantages, off course I'm opposed. \n\n\n> Anything that makes your work easier is \"bloat\", something a\n> \"real\" user wouldn't look at. \ner, sorry? what on earth are you talking about?\n\n> Well guess what: you are not a real user! You\n> are just a network administrator or a developer at best.\n\noogh! I just love the condescending tone of that! (note: sarcasme in preceeding sentence) So in your world a network admin, or developer is somehow worth less then someone else? \n\nNewsflash:\nBeing an network admin, developer, power user, ordinary user, total newby, or whatever other category you think up in no way implies anything abouth worth. It might tell you something about gathered knowledge, experience, and typical use, but even then you'll make mistakes by assuming.\n\nNewsflash 2: \nDon't even bother with the ad hominem attacks, I've been online to long to bother getting mad about them (besides they only weaken your position)"
    author: "cobaco"
  - subject: "Re: KMail is great"
    date: 2005-01-16
    body: "\n>Why on earth would I want html-mail? I have yet to find a single real benefit\n>to it. And I know of several (readibility, space, security) real\n>disadvantages. \nI don't actually care what *you* want. It's about what other people want. I don't personally write a lot of html mail, but I don't bitch about other people using it like some baby who's favorite toy is been shaded by that cool new kid's car.\n \n>You seem to think html-mail is the best thing ever, so enlighten me exactly\n>what makes it so?\nI never said that. And read above.\n \n>Web sites and mail are 2 different media with different requirements, and\n>goals. I suppose you think we should al start to IRC in html-code to? How\n>about SMS, let's start html-coding them also.\nAgain: read first <p> :)\n \n>Note: I won't oppose anybody using html-mail, when it's sensible and\n>necessary\n<snip>\nGood for you, but who say's when it is sensible and necessary? For me it's\nsensible that my gf is going to buy me drinks when we go out. For some abhorant\nreason, for her it is not!\n \n>When those new technologies don't offer advantages, only disadvantages, off\n>course I'm opposed. \nWhen those new technologies don't offer advantages, only disadvantages, they\nwon't be used by anyone. If they are used by at least one person, they offer\nsomething usefull. To become academical, most new technologies don't offer\nanything usefull at the time of their introduction (not that html mail was just\nintroduced so don't get into that; note \"academical\").\n \n \n>> Anything that makes your work easier is \"bloat\", something a\n>> \"real\" user wouldn't look at. \n>er, sorry? what on earth are you talking about?\ner, ranting :)\n \n>> Well guess what: you are not a real user! You\n>> are just a network administrator or a developer at best.\n>oogh! I just love the condescending tone of that! (note: sarcasme in\n>preceeding sentence) So in your world a network admin, or developer is somehow\n>worth less then someone else? \nIt's not about worth, it's about numbers (newsflash no1); \"real\" users far\noutnumber the number of educated computer users. I am sorry you view\nthat as an attack on your own person(newsflash no2). It's a dissagreement with\nyour point of view.\n "
    author: "Adrian"
  - subject: "Re: KMail is great"
    date: 2005-01-16
    body: "A spam filter would be a million times more effective and accurate than simply filtering HTML mail.  I get plenty of text spam and legitimate HTML in my mailbox.\n\nYou say that plain text can do everything HTML can do, but you are living in a dream world.  Non-HTML email CANNOT do text flowing and justification, or indented quotes, or bulleted lists, in a reasonable way that survives forwarding or mangling by SMTP servers.  These are the benefits HTML provides.\n\nHTML can do many very useful things that plain text simply cannot do.  That is why it is necessary in a modern email client.  It can also do many things that should not be done in email.  That is why modern email clients should (and do) only support a subset of HTML's full capabilities.  When you use a good mail client HTML mail is good, *even if* everybody else uses bad email clients.  That's why KMail should support HTML mail."
    author: "Spy Hunter"
  - subject: "Re: KMail is great"
    date: 2005-01-16
    body: "We use standard KDE methods for opening a URL you click on so it behaves just like any other KDE application. If you think that's wrong then file a bug report against kdelibs."
    author: "Ingo Kl\u00f6cker"
  - subject: "HTML-Mails"
    date: 2005-01-17
    body: "I'd really prefer to have a better ascii-editor in Kmail instead of HTML.\n\nA lot of layout (like indenting, itemization, enumerated lists, centered text) can be done in ascii without compromising the security of the reader or setting off false alarms because of using a technique normally used by spammers.\n\nWould be great to have some buttons for these purposes in the normal plain text mode. And it would really set kmail apart from the other MUAs.\n\nHELLO! KMAIL DEVELOPERS! PLAIN TEXT LAYOUTING!!!\n\nJ\u00c3\u00b6rg\n\n"
    author: "J\u00c3\u00b6rg Lippmann"
  - subject: "Re: KMail is great"
    date: 2005-03-04
    body: "HTML Email offers a lot of benefits if used appropriately. \n\nAs a business owner, when replying to potential customers it is very useful for sending a professionally layed out document, that provides them with all of the necessary information in a very readable way. Email is not just about mailing lists but also about business communications.\n\nAn important part of that communication is branding. One thing we like to do with our mail is to include the business logo in the signature of every email. Now my current \"Windoze\" email client PEGASUS (a very security minded app) supports not only HTML email but also embedded images, including there use in signatures.\n\nIn Linux land (where I am progressively moving) the only Email app that seems to support that feature is Evolution. When weighing up the differences between Kmail and Evolution, I much prefere the PIM elements of Kontact to the ones in Evolution, but the very limited HTML supported by Kmail is pushing me in the direction of choosing to use Evolution.\n\nIn now way do I suggest the use of remote hosted images. This kind of content should always we blocked (let a browser view that content if wanted), but the embedding of appropriate images/logos within the text of an email is a very proper use of HTML email.\n\nA look at the user functionality of Pegasus would show that yes this can be done securely, even on a \"Windose\" platform. So how about Kmail implementing embedded images in email (incoming & outgoing)?\n\n\nKerry.\n"
    author: "Kerry"
  - subject: "Re: KMail is great"
    date: 2005-03-04
    body: "It's on the TODO-list, AFAIK.\nYes, it's one the feature I miss most too."
    author: "Anonymous"
  - subject: "Re: KMail is great"
    date: 2005-01-16
    body: "Bug#59101 (no copy/paste, etc).  Resolved 2005/1/9 in CVS.  Can't wait to finally see what apparently you've been seeing.\n\nBug#41926 (format=flowed).  Lines are only all the same length if the window size on the receiving computer is wider than your line length.  If it's narrower, they get automatic line breaks AND your line breaks too.  Looks like crap.\n\nMaybe I have more address book entries than you.  Can't find this bug number--may it hasn't been reported after all.\n\nBug#60787 (Konqui downloads file twice).  Actually it DOES download the whole file twice--otherwise why would Konqui prompt for every cookie on the page before launching Firefox?!?  It may not do this if your preferred browser IS Konqueror.\n\nThese are real bugs.  I won't be a happy KMail user until they're fixed.\n"
    author: "ac"
  - subject: "Re: KMail is great"
    date: 2005-01-16
    body: "It doesn't use Konqueror to download the whole file, it just uses the HTTP IOSlave to request the very beginning of the file from the server to determine the file type so it knows what application to launch.  The cookie-handler is a separate service provided by KDE through the HTTP IOSlave; it doesn't have anything to do specifically with Konqueror.\n\nI do agree that this behavior is annoying, but the alternatives all seem bad too.  KMail could always launch your web browser for HTTP links, but then if you clicked a PDF file link (for example) you would end up with an empty browser window in addition to your PDF reader.  It could get the file type from the file extension alone but then it would need to know about every funky extension used on the web (.aspx, .jsp, etc).\n\nThe real solution is for KMail to hand off the partially downloaded file to the application that will use it, so the download continues instead of being restarted.  But this really is impractical, especially for applications that don't use KDE's IOSlaves.  I suppose the practical solution is to always launch your web browser, and just ignore the empty browser windows that pop up when viewing PDFs or videos or such.  Or perhaps the popping up of browser windows could be suppressed somehow?"
    author: "Spy Hunter"
  - subject: "Re: KMail is great"
    date: 2005-01-16
    body: "\"Bug#59101 (no copy/paste, etc). Resolved 2005/1/9 in CVS.\"\n\nOh. You mean when viewing messages, I thought you meant when composing them.  \nIt's a problem for sure, but shouldn't be critical for you.  CTRL-C and CTRL-V work just fine in 3.3.1.\n\n\"Bug#41926 (format=flowed). Lines are only all the same length if the window size on the receiving computer is wider than your line length. If it's narrower, they get automatic line breaks AND your line breaks too. Looks like crap.\"\n\nWhat is your line length set to?  It should be about 72 or so.  Of course if you set it to longer than most people's screen size it will look like crap.\n\n\"Bug#60787 (Konqui downloads file twice). Actually it DOES download the whole file twice\"\n\nActually it doesn't.  But Spy Hunter already explained that.  I think you're making these small issues out to be a much bigger deal than they really are.  I would classify them more as minor annoyances, not critical bugs"
    author: "Leo S"
  - subject: "Re: KMail is great"
    date: 2005-01-17
    body: "No, I meant viewing AND composing.  Yes, Ctrl+C and Ctrl+V works great, but if there are no menu options in the compose screen in KDE 3.4, I'll file another bug.  I'd certainly hate it if they fixed the problem in only one spot.\n\nI set mine for 72, which is smaller than most people's windows at a standard font size.  But not everyone.  Is that their problem?  I'd say no--one of my friends has vision problems and sets the fonts large so they can read my mail.  This works great for everyone else's mail--but mail from me looks bad because it comes from KMail.\n\nI appreciate that exactly ONE of the several problems I highlighted was explained as less severe than I had thought.  (on what planet is not being able to see the OK/Cancel buttons not critical?!?)  Let's hope the rest are fixed.\n"
    author: "ac"
  - subject: "Re: KMail is great"
    date: 2005-01-17
    body: "There are people out there on slow connections and they work sometimes on text MUAs. Why should KMail insist on using HTML?\n\nI mean, writing a message, a letter or something like that means dealing with WRITING. You don't draw a picture to invite your colleagues to a meeting. You write a short text, do you?\n\nKMail by default uses text messages. This is simple and enough. This is secure and efficient.\n\nOTOH you can use KMail to read and to compose HTML messages. Just use it.\n\nI remember times when watches started to get LCDs. Soon there were watches with calculator functionality. It was a demonstration of what could be achived using that state of technology. Most young people dreamed of such a watch. Do you want to know what that watches really were? They were crap. The keys were so small, you could use them with a pen only. And soon people started to realize, that they usually don't need that calculator functionality because you usually use a watch as a watch.\nI (personally) see HTML mails as such an overkill. Yes, a lot of people use it. Given that HTML composition is on by default in most of the \"business\" MUAs, we'll get more and more HTML mails. But this is hopefully only a phase. How many watch models with calculator functionality can be found today in a shop or catalog?"
    author: "Andreas"
  - subject: "Re: KMail is great"
    date: 2005-01-17
    body: "HTML provides a lot of features that are not useful for mail (and these should not even be supported by KMail), but unlike calculator watches, HTML also provides many features that *are* useful.  It is the only way to send a mail message that wraps properly, has any text formatting at all (center, italics, etc), and survives quoting or forwarding gracefully.  \n\nI consider these features essential in a modern email client, and even terminal users can benefit from them.  Links is a fine HTML viewer; any modern email client has no excuse for not supporting HTML.  \n\nProper HTML need not be much larger than the text alone (and it could be smaller if you try fancy ASCII formatting in the text version).  Not providing the good features of HTML mail by default may turn quite a few people off from using KMail."
    author: "Spy Hunter"
  - subject: "Re: KMail is great"
    date: 2007-11-06
    body: "It's 2007 and it's still hard to impossible to link to websites when composing email in kmail.  I'm not saying that html should be the default mode, but at least it should be an option for those that desire it.  "
    author: "Brice"
  - subject: "Attachments"
    date: 2005-01-14
    body: "Any way to view email attachments like jpg, txt and whatnot rendered within the main kmail window as opposed to launching an outside app? I know, security, security. I mean just having the option available, or I completely missed it!\n\nIt's a minor little annoyance in an overall great app! Kontact rox!\n\n\n"
    author: "Jeremy"
  - subject: "Re: Attachments"
    date: 2005-01-14
    body: "Is\n\nView > Attachments > Inline\n\nwhat you're looking for?\n\n(Using Kmail 1.7.2)"
    author: "Jeffrey M. Smith"
  - subject: "Re: Attachments"
    date: 2005-01-15
    body: "Thank you!!! I missed it. \n\nThe KDE Community rocks. "
    author: "Jeremy"
  - subject: "Too bad it is tied down by X11"
    date: 2005-01-14
    body: "the only major issue with any KDE apps is they required X11 to function, KMail would be great it if ran natively in one of the GUI OS's that buisness people used. ( W32 or OSX )"
    author: "james brown"
  - subject: "I am glad this is the case"
    date: 2005-01-15
    body: "Those OSes have their own email applications. It is a misktake to invest development time and resources into a platform where the platform owners have greater control than you over defaults and over the functioning and hooks to the platform.\n\nTell me how well is Eudora doing on Win32? How about Opera Mail or even Thunderbird?\n\nThe latter might be doing a slightly bit better based on the free price of admission. Most people are clueless and just use whatever is on the computer. \n\nIt would do KDE no good to spend considerable time porting to these other operating systems. What we need is more Linux, Free/Open/Net BSD users. That's a goal worth striving for.\n\nI have no interest in seeing free software apps on proprietary apps, other than as gateway apps, such as OpenOffice.org\n\n"
    author: "Gonzalo"
  - subject: "GNOMEDESKTOP.ORG"
    date: 2005-01-15
    body: "\"LinuxQuestions.org has opened voting for their 2004 Members Choice Awards, so be sure to head on over and vote for your favorite applications. GNOME and various other GNOME/GTK related projects are in the running so be sure to make your vote count!\"\n\nWhy don't we blatantly advertise KDE and drive users to vote. I'm sure it wouldn't hurt. "
    author: "Vote"
  - subject: "Re: GNOMEDESKTOP.ORG"
    date: 2005-01-15
    body: "take a close look at the other articles on the Dot :o)"
    author: "ac"
---
<a href="http://www.linuxplanet.com/">Linux Planet</a> features "<a href="http://www.linuxplanet.com/linuxplanet/tutorials/5707/1/">KMail in Depth</a>" describing KDE's e-mail application as having 'masses of features and no malware'.  The article describes converting to <a href="http://kmail.kde.org/">KMail</a>, encrypting &amp; signing e-mail and configuring multiple accounts.






<!--break-->
