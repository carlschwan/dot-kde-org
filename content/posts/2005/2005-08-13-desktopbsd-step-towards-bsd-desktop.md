---
title: "DesktopBSD: A Step Towards BSD on the Desktop"
date:    2005-08-13
authors:
  - "brockers"
slug:    desktopbsd-step-towards-bsd-desktop
comments:
  - subject: "Looks pretty good"
    date: 2005-08-13
    body: "They seem to have a good set of system tools that integrate well with KDE/Qt.  I don't know though, it'd be hard for me to leave the familiar Linux kernel... but good to keep an eye on.\n\nThey certainly picked the right desktop :=)"
    author: "anon"
  - subject: "Re: Looks pretty good"
    date: 2005-08-13
    body: "It's great to see desktop initiatives recognizing how nicely integrated and mature KDE components are. DesktopBSD (along with Linspire, Xandros, Slackware, ...) is yet another project that has chosen to rely solely on KDE for its desktop needs, and in the process has shown that it is quite practical to build a customized system using KDE.\n\nThe problem I have with BSDs in general is that they don't use a copyleft (ie. forced-sharing) license. This means that unscrupulous players like M$ can appropriate (aka leech and steal) anything they like and sell it back as proprietary, closed-source, and non-free. Such actions will ensure that proprietary software will always have an edge over free software. This in turn will reduce the incentive for people to switch to free software and consequently reduce the support that free software projects like KDE receive from the community.\n\nBy focusing on seamless integration with Linux (which is licensed under the most robust and widely-used copyleft license, the GPL), KDE will be part of a free desktop system that can not only stand its ground in the face of industry giants like M$ and Apple, but stand on the same level."
    author: "Vlad C."
  - subject: "Re: Looks pretty good"
    date: 2005-08-15
    body: "How come everytime a BSD system is mentioned positively, someone always has to come along and piss all over it? It's almost like some people are so insecure with their own choices that they must denigrate those of other people."
    author: "Brandybuck"
  - subject: "Re: Looks pretty good"
    date: 2005-08-15
    body: "Yeah the GPL is way too restrictive IMHO.\nBSDL: they focus on developing good software and making it available for anyone to use. No strings attached. The more people use it (home users, enterprise users), the better.\nGPL: they focus on developing software that is free (free as in freedom) by restricting what you can do with it. And yes, it's called \"free\" software.\n\nThose are some of their goals but they don't apply in some cases.\n"
    author: "anonymous"
  - subject: "Availability of DesktopBSD tools ?"
    date: 2005-08-13
    body: "DesktopBSD looks great ! I was wondering where does the network configuration tool come from ? Is it part of the specific DesktopBSD tools ? It looks really nice and I'd be glad to give this tool a try on Linux..."
    author: "thibs"
  - subject: "Impressive"
    date: 2005-08-13
    body: "Looks extremely impressive to me. If the management tools work as well they look in the screenshots it should be incredible :)"
    author: "Chris Howells"
  - subject: "KDE is irresistible"
    date: 2005-08-13
    body: "Now I feel like trying BSD Unix ;)"
    author: "fast_rizwaan"
  - subject: "Other BSDs preferable."
    date: 2005-08-13
    body: "Dammit.  I'd like to try a modern BSD distro (tried FreeBSD years ago) but I'm on PPC these days, and FreeBSD *still* hasn't gotten around to that.  I'm more interested in NetBSD's portability and OpenBSD's security anyway.\n\nHopefully someday we'll see the other BSDs with decent installers etc. too :)"
    author: "Lee"
  - subject: "Re: Other BSDs preferable."
    date: 2005-08-14
    body: "> Hopefully someday we'll see the other BSDs with decent installers etc. too :)\n\nYears ago I used FreeBSD (circa 3.x) and its installer was able to install over a PPP connection. I'd like to see a Linux distro with an installer that can do that, but haven't yet.\n\nBTW, it ought to be possible to use debian-installer with the BSD kernels eventually:\nhttp://www.debian.org/ports/netbsd/\nhttp://www.debian.org/ports/kfreebsd-gnu/\n(However, these don't yet have debian-installer, but instead rely on some other installation method.)"
    author: "rqosa"
  - subject: "Re: Other BSDs preferable."
    date: 2005-08-16
    body: "My very first install of linux was over a ppp connection, installing an M68K port to an Atari Falcon.\nThe installer ran under MiNTOS, a multi-tasking version of Atari's OS.\n"
    author: "LinuxHack"
---
<a href="http://desktopbsd.sourceforge.net/">DesktopBSD</a> is a custom FreeBSD-based desktop focussed on stability, usability and simplicity.  In developing an easy to use desktop operating system, DesktopBSD has chosen to use KDE. Screenshots <a href="http://www.bsddesktop.de/gallery/thumbnails.php?album=1">showing the desktop tools in action</a> are available.



<!--break-->
<p>
Their <a href="http://desktopbsd.sourceforge.net/sub/faq.html">website FAQ</a> further explains the choice of KDE:
<p>
<i>"KDE is easy to work with and has many useful features and well-integrated components such as the PIM package (Personal Information Manager). Additionally, KDE is probably easier for people that used Windows before, but this is rather a nice side-effect than a main reason for this choice."</i>
<p>
<i>"Of course, there are other great desktop environments out there that are in some ways superior to KDE, but we decided to use only KDE so we can have better support for that one and have better integration of the DesktopBSD Tools with that environment."</i>



