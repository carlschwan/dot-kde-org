---
title: "KOffice 1.4.2: Improved OpenDocument Support and Updated Karbon"
date:    2005-10-11
authors:
  - "iwallin"
slug:    koffice-142-improved-opendocument-support-and-updated-karbon
comments:
  - subject: "First Post!"
    date: 2005-10-11
    body: "Wow, nice to see many improvements and bugfixes in KOffice, and especially sooo many in Karbon14. Thanks to all KOffice developers for the most complete office suite on earth!"
    author: "fyanardi"
  - subject: "Second Post? (Re: First Post!)"
    date: 2005-10-11
    body: "Is the OpenDocument format native in KOffice?"
    author: "Ernest"
  - subject: "Re: Second Post? (Re: First Post!)"
    date: 2005-10-11
    body: "Just as native as the old native format, that's to say, it's implemented in the applications themselves, not as a filter from and to a native representation. But it's not yet the default format: that will happen with the 1.5 release, which is scheduled for the end of January."
    author: "Boudewijn"
  - subject: "Rich text copy and paste"
    date: 2005-10-11
    body: "Does KOffice supports rich text copy and paste ? This is the main reason I use Openoffice.org "
    author: "Althazur"
  - subject: "Re: Rich text copy and paste"
    date: 2005-10-11
    body: "Yes it does.\n\nIn the klik bundle, I'm including a few documents, describing some things you can do with it (like start up in different languages, and start up the different applications individually, instead of just via koshell (the Everything-Integrating Office Suite Environment). \n\nAll these documents I've written with alpha versions of the bundle itself.\n\nAfter I had readied different 1-page documents, I decided to put it all in 1 multi page document. I did so by copying and pasting the formatted content from the first into the second, onto empty pages. It worked flawlessly."
    author: "Kurt Pfeifle"
  - subject: "Re: Rich text copy and paste"
    date: 2005-10-12
    body: "I mean from an html web page ( konqueror or mozilla ... ) to kword."
    author: "Althazur"
  - subject: "Re: Rich text copy and paste"
    date: 2005-10-12
    body: "I've tested between OOo en KOffice, and there it doesn't work. I'm not sure why not, actually, but there's probably something complicated with clipboard standards and so on involved; it's something that should definitely be looked into."
    author: "Boudewijn Rempt"
  - subject: "Klik package"
    date: 2005-10-11
    body: "Please! Could the klik package be made \"self contained\" so that it doesnt depend on debian libs ? Something like a  self-contained directory ? (with very basic external dependencies). I have been having fun kliking around in Mandriva, but only some self contained packages (are they called bundles? ), like Firefox, etc. really work fine (which is expected anyways)\n\nMany thanks ..."
    author: "MandrakeUser"
  - subject: "Re: Klik package"
    date: 2005-10-11
    body: "Cyrille Berger tested the experimental Klik package Kurt is working on on Mandriva today and reported that it worked."
    author: "Boudewijn Rempt"
  - subject: "Re: Klik package"
    date: 2005-10-11
    body: "Thanks a lot Boudewijn!\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Klik package"
    date: 2005-10-11
    body: "### 'Could the klik package be made \"self contained\"' \n----\nYes. Of course. The \"self-contained\" is exactly the base idea of klik.\n\n\n### 'so that it doesnt depend on debian libs'\n----\nWhy?\n\nDebian libs work great. ;-P\n\nEven on other distros...\n\nThe most important thing for getting the klik package work on as many platforms as possible, is to avoid \"bleeding edge\" compilers and library dependencies, and avoid dependency to libstdc++6. Debian Sarge is just about perfect for that. You better make sure that your distro (Mandriva) ships with Kernels that support loopmounting cramfs images (this has been the major hurdle for Mandriva users utilizing klik).\n\nWe can't include *everything* without making the self-contained AppDir bundle reproduce and mirror a complete Knoppix system.\n\nWe do include a few \"known\" things, and tweaks that take care of various naming conventions in between distros for the same library.\n\nHaving said that thing about Debian -- in this case I used SUSE-compiled binaries (because I couldnt find any Debian packages of KOffice yet).\n\nI'm currently working on a fix for a problem with the included docs and translations (they are not used or shown). Unfortunately, I didnt find many alpha testers for it (but thanks to you two, CyrilleB and Dennis_p!). Looks like everyone enjoys the finished free cake, but no-one is prepared to help the baker do some work....\n\nOnce it is ready, I'll announce a download URL, and a bit later a klik://koffice link will also work.\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Klik package"
    date: 2005-10-12
    body: "Thank you Kurt, you rock!\n\nThanks for all the info and clarifications. I'll give the klik a shot when it is done. Unfortunately, these days I have very little time to contribute to free software. I'll contribute again when I get a break ... from life!\n\nAs per cramfs. I am running Mandriva 2006.0 and it seems to be working.\nRight now I am runnning the klik for kmplot, and here is my mtab:\n\n#####\n~> more /etc/mtab\n/dev/sda1 / ext3 rw 0 0\nnone /proc proc rw 0 0\nnone /sys sysfs rw 0 0\nnone /proc/bus/usb usbfs rw,devmode=0664,devgid=43 0 0\n/dev/hdc1 /disk2 ext3 rw 0 0\n/dev/sda6 /home ext3 rw 0 0\nnone /tmp tmpfs rw 0 0\n/tmp/app/1/image /tmp/app/1 cramfs ro,nosuid,nodev,loop=/dev/loop0,user=myUserName 0 0\n#####\n\nMy kernel: Linux 2.6.12-12mdk #1 Fri Sep 9 18:15:22 CEST 2005\n\nCheers!"
    author: "MandrakeUser"
  - subject: "OOO Filters"
    date: 2005-10-12
    body: "Now that KOffice is using the OASIS format natively, would it be possible to reuse the OpenOffice excellent import/export filters from the MonopoliSt office ? Last time I asked, it was impossible, because the OO code was very convoluted and monolitic. Have they refactored stuff for OO 2.0 ? It would be beneficial for everyone, including them. If their filters could run as stand-alone processes, people could write scripts to transform all the documents in a large installation of MS office to OASIS, etc. \n\nAnd for KOffice, many people who are not using it now could start using it immediately if this functionality (better interoperability with MS office documents) was available. \n\nAny thoughts on this ?"
    author: "MandrakeUser"
  - subject: "Re: OOO Filters"
    date: 2005-10-12
    body: "I'm sorry to have to say that the OOo filter code is still the same convoluted unintelligible mess that appears to directly poke stuff into the internal OOo datastructures that is has always been -- so, no, unless you want the type of solution where KOffice uses OOo automation to first convert an MS document to an OpenDocument document and then load that. But that would, of course, be horribly slow and hackish.\n\nWe need better filters, but that's one thing that's nearly impossible to achieve in ones copious spare time; you really have a couple of full-time people for that."
    author: "Boudewijn Rempt"
  - subject: "Re: OOO Filters"
    date: 2005-10-12
    body: "That's sad to hear. Hopefully they will consider cleaning up and refactoring for 3.0. There is a general consensus that both their code and their build system needs a major rewrite if they are to succeed in the long term. Just building OO is , by itself, a challenge. Not to mention porting, or trying to hack code into it. \n\nThe alternative would be to try to coordinate in FreeDesktop.org , or maybe within  oasis, to write (maybe roll up sleeves and try to extract it from the OO mess) stand-alone filters (from/to different formats to/from oasis). Oh well, wishfull thinking. If people from KDE, Gnome, freedesktop.org, etc. pull together maybe this is attainable. I wish I had some time to help coding ..."
    author: "MandrakeUser"
  - subject: "CommandLine OOO Filters"
    date: 2005-10-12
    body: "yes, a command-line OOo filters would be the solution.\n\nIt could allow non-ms office utilities to directly convert a .doc to .odt or (od?) so that other applications could use it.\n\nI wish oo developers should consider a \"Document viewer\" and/or \"Document Converter\" commandline, so that other applications like abiword, kword, etc., could use ms filters to get a readable .odt or .odp or .ods file.\n\n"
    author: "Fast_Rizwaan"
  - subject: "Re: CommandLine OOO Filters"
    date: 2005-10-12
    body: "hmm, but nodody at oo.o works on that tool... hmmm"
    author: "gerd"
  - subject: "Re: CommandLine OOO Filters"
    date: 2005-10-13
    body: "<mode=conspiracy>\nKind of makes you wonder.. is it World Domination (TM) they are after? If people have to load up the entire OO to convert documents, then why would they need \"crummy little koffice\"? ;-)\n</mode>\n\nBut speaking of this.. the a2ps (anything-to-postscript) is a killer app! If we eventually got a \"a2od\" product, this would make life a whole heck of a lot better for everyone (except M$ ;-). Being able to convert even dvi, pdf, wpd and the more exotic formats to OpenDocument too with a right-click maneuver on both GNU/Linux and win32 could speed up the transition quite a bit... or do you all think I'm barking up the wrong tree? :P\n\n"
    author: "Macavity"
  - subject: "Re: OOO Filters"
    date: 2005-11-26
    body: "Try this one:\nhttp://visioo-writer.tuxfamily.org/EN/"
    author: "marcodefreitas"
  - subject: "Xdeltas, please"
    date: 2005-10-13
    body: "I can't find an xdelta patch from 1.4.1 to 1.4.2... Who knows where I can get this thing?"
    author: "Artem Tashkinov"
  - subject: "word import...."
    date: 2005-10-13
    body: "hi\n\nwhen koffice will use the import word package from openoffice?\n\nopenoffice is really nice to open, edit word file... but it's not true for koffice...\n\nthanks"
    author: "Marc Collin"
  - subject: "Re: word import...."
    date: 2005-10-13
    body: "You're kidding, right?"
    author: "cl"
  - subject: "Re: word import...."
    date: 2005-10-13
    body: "This has been discussed only a few posts further up.  \n\nShort version:  Never.  It's not possible in a sensible way.\n\n"
    author: "cm"
  - subject: "Any distributions including it yet?"
    date: 2005-10-13
    body: "Any distributions including it yet?  Live CD distributions?  "
    author: "Anonymous "
  - subject: "Re: Any distributions including it yet?"
    date: 2005-10-14
    body: "No, as the release notes mention, there are no live cd's available (at the very moment I'm writing this)"
    author: "Bram Schoenmakers"
  - subject: "Re: Any distributions including it yet?"
    date: 2005-10-24
    body: "The Klax Live-CD with KDE 3.5 Beta 2 has it."
    author: "Anonymous"
---
The <a href="http://www.koffice.org/">KOffice</a> team today <a href="http://www.koffice.org/announcements/announce-1.4.2.php">announced the second maintenance release</a> of the 1.4 series. Among various bugfixes and translation improvements, the KOffice 1.4.2 release further improves support for the OASIS OpenDocument file format and interoperability with <a href="http://www.openoffice.org/">OpenOffice.org</a>. Also, <a href="http://www.koffice.org/karbon/">Karbon</a>, the vector drawing application has been much improved. More changes are listed in the <a href="http://www.koffice.org/announcements/changelog-1.4.2.php">changelog</a>. <a href="http://download.kde.org/binarydownload.html?url=stable/koffice-1.4.2/">Binary packages</a> for Kubuntu and SUSE have been contributed and a Klik package is being prepared right now for those who want to test this release without installing it first (testers are welcome).
<!--break-->
