---
title: "KDE at German Events, October 2005"
date:    2005-10-15
authors:
  - "fkossebau"
slug:    kde-german-events-october-2005
comments:
  - subject: "Showing your face"
    date: 2005-10-15
    body: "It's great to hear that KDE shows it's face on so many places. I was a bit worried about KDE Germany, because I didn't hear much of them. Despite the incredible amount of people using/developing KDE in Germany.\nBut this proves you guys are doing great work on the promotional part."
    author: "Bram Schoenmakers"
  - subject: "Re: Showing your face"
    date: 2005-10-15
    body: "Yes, especially with the LinuxTag, the biggest linux-fair in europe I thing and the CeBIT, the biggest IT-fair in the world. Both always have KDE-developers attending :)"
    author: "Carsten Niehaus"
  - subject: "KDE and Systems: 24-28 October 2005, Wanted: staff"
    date: 2005-10-17
    body: "Systems will happen next week and we are in need for some people \nstaffing the booth. So if you can help us for 1-2 days please send me an email."
    author: "Torsten Rahn"
---
October in Germany is filled with a lot of local Free Software events and KDE 
is present at them. Join us first at <a href="http://www.berlinux.de">Berlinux 2005</a> on Fri October 21 and Sat 22.  Then we are off to Dresden for <a 
href="http://linux-info-tag.de/">Linux-Info-Tag Dresden 2005</a> on Sat October 29.  Read on for how we'll be helping the users to explore the full range of wonders in KDE.




<!--break-->
<p>
Berlinux takes place at Berlin's Technical University.  Ellen 
Reitmayr, Oswald Buddenhagen, Stephan Binner and some others will run a booth 
and demonstrate the latest of KDE.
</p>

<p>
At Linux-Info-Tag Dresden, which takes place at Dresden's Technical University 
(see a pattern?), the <a href="http://linux-info-tag.de/72">KDE stand</a> will 
be run by Carsten Niehaus and Tobias K&ouml;nig with sporadic help from Josef 
Spillner, Konrad Rosenbaum and Friedrich Kossebau.  Our stall will be next to 
<a href="http://www.skolelinux.org/">Skolelinux</a> and will
emphasise <a href="http://edu.kde.org/">KDE-Edu</a> and <a 
href="http://pim.kde.org/">KDE-PIM</a>.
An additional <a href="http://linux-info-tag.de/62/detail/43">talk by Tobias K&ouml;nig</a> (in German) will give information about the current state and  future plans of KDE. 
</p>

<p>
Other events which currently have no KDE presence yet planned are <a 
href="http://olix.bytemine.net">Oldenburger Linux-Informationstag 
(Olix)</a> and the <a href="http://www.lug-ld.de/infotag/">Linux-Infotag in 
Landau/Pfalz</a>, both at Saturday, October 15th. Let <a href="https://mail.kde.org/mailman/listinfo/kde-promo">kde-promo</a> know if you can give KDE some face there too.
</p>



