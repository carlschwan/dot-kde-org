---
title: "Kolab 2 Groupware Released"
date:    2005-06-21
authors:
  - "dmolkentin"
slug:    kolab-2-groupware-released
comments:
  - subject: "Great"
    date: 2005-06-21
    body: "Good job guys. I'm going to try it for sure."
    author: "JC"
  - subject: "iCal, Apple Address Book, Thunderbird, Sunbird"
    date: 2005-06-21
    body: "any chance we can connect to it under Windows and MacOSX ? \nI especially need OSX,\n\nremote addressbook and calendar is what we have been expecting for a long long time.\n\nif it's kontact/KDE only, there is not much point to this.\n\n"
    author: "Office Representative"
  - subject: "Re: iCal, Apple Address Book, Thunderbird, Sunbird"
    date: 2005-06-21
    body: "It is definitely not KDE only. If you care to visit the website, you will see that there is support for MS Outlook, a web interface in the works, a firebird plugin being actively developed, and more.\n\nBesides, it is designed to be a Exchange killer, so it deas with all MS Outlook quirks. I believe it is the first open source / open standards server to do so."
    author: "Amadeo"
  - subject: "Read carefully !!"
    date: 2005-06-21
    body: "As the news says:\n\n\"Windows users can use the Toltec plugin to turn Outlook in a fully-fledged Kolab client. A web-based interface to mail, calendar and contacts based on the Horde framework is in beta-stage and will be released later.\"\n\nSo yes, it works in windows too :)"
    author: "Maeztro"
  - subject: "Re: Read carefully !!"
    date: 2005-06-22
    body: "Just to add more about open source (and free) Kolab clients.\nAethera has the Kolab2 support in progress, it should have a new release in the next few weeks.\nAethera will support both Kolab1 (or Citadel... or just a simple IMAP server) and Kolab2.\nAethera is running on Linux, Windows and Mac OSX.\nThe Windows version has even a better performance (speed, process size) than Linux version.\nIf you want to use a Kolab server, you will find easily a client application for it using your favorite platform.\n"
    author: "Eugen Constantinescu"
  - subject: "Re: Read carefully !!"
    date: 2005-06-23
    body: "> The Windows version has even a better performance (speed, process size) than Linux version.\n\nWhy is that?"
    author: "Anonymous"
  - subject: "Re: Read carefully !!"
    date: 2005-06-24
    body: "There is also the KONSEC Konnektor (http://www.konsec.com) which is not an Outlook plugin like Toltec but a Kolab 2 compatible MAPI Storage Provider. \n\nThis results in better integration with other MAPI services like ActiveSync and full support for shared folders. "
    author: "KO"
  - subject: "Re: iCal, Apple Address Book, Thunderbird, Sunbird"
    date: 2005-06-22
    body: "With Qt4/windows (announced) beeing GPL licenced for windows, (right now Qt4 is not yet released and only the Qt4/X11 lib is available under GPL), we will quite likely see more KDE apps running natively on Windows.\n\nWether this is a good or a bad thing for KDE... i dont know.\n\n\n"
    author: "cies breijs"
  - subject: "openpkg"
    date: 2005-06-21
    body: "\nThey have _got_ begin providing a regular tarball source package, a.s.a.p., for this thing to ever get installed in a larger base.  The openpkg might be nice and useful and convenient for some, but for other's it is absolutely horrible.\n\nAn open cvs/subversion repository for the codebase would also be extremely helpfull - as would a repository/package specifically comprising each of the patches/diffs needed to apply to the source distributions of the other open source components they use as a base for kolab; which would allow Kolab to be _integrated_ into _existing_ servers, rather than basically usurp a large collection of ubiquitous services ( postfix, openldap, cyrus, etc ) all to itself.\n\nIt's really frustrating that I can't simply apply some patches to my existing installed software, and then just configure them for use with Kolab; that I'm instead forced to use a foreign package tool, which monolithicly installs it's own versions of existing software/components.\n\n"
    author: "Name"
  - subject: "Re: openpkg"
    date: 2005-06-21
    body: "\nLooks like they do at least have a cvs repository, so I appologize for that mistake.\n\n:pserver:anonymous@intevation.de:/home/kroupware/jail/kolabrepository"
    author: "Name"
  - subject: "Re: openpkg"
    date: 2005-06-21
    body: "Kolab is not designed to be integrated into existing solutions. So, using its own, separate root viy OpenPKG was the best thing they could do, although you have to learn a bit to use rpm but it's not that different and if you don't install additional packages everything is fully automated.\nIf you don't wanna set up a new machine for Kolab there's an interesting explanation on how to use UML to install Kolab without messing up your config."
    author: "Anonymous"
  - subject: "Re: openpkg"
    date: 2005-06-21
    body: "\nLooks like I wasn't the only one who saw the good sense in the possibility/option of decoupling Kolab from  openpkg:\n\nhttp://wiki.kolab.org/index.php/Integrating_Kolab2_Server_in_GNU/Linux_distributions#What_has_been_achieved\n\n\nLooks like there's already some good progress underway.  The first/primary objective is that they're changing some hardcoded stuff to be more portable in the Kolab source, then they're providing a distro-specific conf file.\n\nCheck out this thread:\n\nhttp://intevation.de/pipermail/kolab-devel/2005-June/003867.html\n\n\nHelp get Kolab portable and decoupled from openpkg for your distro of choice!\n\n( there's already work being done for gentoo! )\n"
    author: "Name"
  - subject: "Re: openpkg"
    date: 2005-06-22
    body: ">Help get Kolab portable and decoupled from openpkg for your distro of choice!\n\n Yes please... Eventhoug Kolab was not originally intended to integrate with the system i think that is an essential long-term goal. Free Software is about freedom.. freedom to patch your own system packages and have Kolab use them if you desire ;-)\n\n I know that this is a lot of extra work, and that it will make auto-configuration harder.. but its worth the efford on the long run.\n\n~macavity "
    author: "Macavity"
  - subject: "Re: openpkg"
    date: 2005-06-22
    body: "The openpkg stuff really annoyed me at first but after having actually installed and used kolab 2 since beta3 or so in production (and installing random other versions for testing) I can say that it actually works out pretty well and is super-easy to set up.  Dl the files, run 3 commands, answer a few questions and you have a fully working system.  Then you can tgz up /kolab and have a fully contained install for backup or copying to other machines.  Just be careful about entering proper hostnames and such on install, if you put in bogus info I find it easier to just reinstall than track down all the needed config changes."
    author: "Anonymous"
  - subject: "LiveCD?"
    date: 2005-06-22
    body: "I really would like to try Kolab 2 with Kontact and Outlook. But as LB stands for Lazy Bastard I don't want to install it (initially). Anyone knows if/when a LiveCD exists/will be created?"
    author: "LB"
  - subject: "Re: LiveCD?"
    date: 2005-06-24
    body: "There is a live CD you can order from erfrakon.\n\nSee http://www.erfrakon.de/seiten/aktuelles/index.html?article=24"
    author: "foo"
  - subject: "Re: LiveCD?"
    date: 2005-06-27
    body: "Why order? Anywhere available for download?"
    author: "Anonymous"
  - subject: "Re: LiveCD?"
    date: 2005-06-27
    body: "Intevation from the \nhttp://www.Kolab-Konsortium.de handed out some Kolab Live-CDs at Linuxtag 2005.\n\n"
    author: "Bernhard Reiter"
  - subject: "Kolab 2 Live CD"
    date: 2005-06-30
    body: "It's now available for download on http://www.konsec.com/en/downloads/"
    author: "Anonymous"
  - subject: "Re: Kolab 2 Live CD"
    date: 2005-09-26
    body: "I have downloaded this cd.  The md5sums checked out.  I tried it on 4 different pcs of various ages on two different lans.  All 4 failed with an error that went into a loop. The error message was in German, but was about dhcp.  Several of these computer have happily fired up in Knoppix (upon which this is based) previously so I am not sure where the problem is.\n\nHas it worked for anyone else?"
    author: "Ken Walker"
  - subject: "Re: Kolab 2 Live CD"
    date: 2005-09-26
    body: "### All 4 failed with an error that went into a loop.\n----\n\nI downloaded the CD too (about two months ago). It worked like a charm. Very impressive.\n\nHowever, I made sure to read all the relevant documentations. And what made me postpone my first booting of it was this bit of information from their web page (paraphrasing, don't want to search for it now):\n\n ++ \"a Kolab server requires a fully qualified hostname to work, \n ++ and a DNS server that is capable of doing reverse DNS lookups.\"\n\nSo this meant that the Kolab2 Live CD will not boot unless it gets an FQHN assigned by its DHCP server, as well as the DNS server doing reverse lookups. The Kolab2 Live CD documentation says the same. \n\nOne way to set it up is to use a Linksys WLAN access box on which you put on a new OS image (google for it) with the capabilities required.\n\nThen it is very easy. Until then, let your booting CD loop on. Once the DHCP server that assigns your Kolab2 CD box an FQHN is appearing in your net, the boot sequence magically picks up and completes.\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
---
After two years of hard work, the <a href="http://www.kolab.org">Kolab Groupware Project</a> has <a href="http://www.kolab.org/news/pr-20050620.html">released Kolab 2</a>. It allows for sharing of personal folders, calendars and contacts and comes with improved spam and anti-virus capabilities. With Kolab 2 installation is now an easy two-step process, and the admin web interface has many improvements.  KDE <a href="http://dot.kde.org/1117544382/">recently started supplying</a> Kolab for its contributors. <a href="http://kontact.kde.org">KDE Kontact</a> is the project's official groupware client on GNU/Linux and Unix derivates. 






<!--break-->
<div style="float:right; width: 210px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/kolab.png" width="210" height="60" alt="Kolab Logo" /></div>
<p>If you do not have KDE 3.4.1 installed and still want to try Kolab, the Kolab folks have provided a <a href="http://www.kolab.org/download.html#kolab2kdeclient">special version</a> of Kontact that will work with KDE 3.2 and higher. </p>

<p>The unique concept of storing everything in MIME structures on the Cyrus IMAP server allows for on-the-fly-backup. As the client performs most of the groupware tasks, Kolab is extremely scaleable. It is built entirely on <a href="http://www.kolab.org/about-kolab-server.html">Free Software</a>.</p>

<p>Windows users can use the <a href="http://www.toltec.co.za">Toltec plugin</a> to turn Outlook in a fully-fledged Kolab client. A web-based interface to mail, calendar and contacts based on the Horde framework is in beta-stage and will be released later.</p>

<p>Help is provided via <a href="http://www.kolab.org/mailman/listinfo/kolab-users">mailing lists</a>, the <a href="http://eforum.de/">forum</a> and a <a href="http://wiki.kolab.org">wiki</a>. You can also get <a href="http://www.kolab-konsortium.de/en/index.html">professional</a> help for all kinds of consultancy, training or development. Kudos to the Kolab guys!</p>

<div style="width: 300px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/kolab2-kontact.png" width="300" height="196" alt="Kolab Logo" /><br /><a href="http://www.kolab.org/screenshots.html">Kontact Connected to Kolab 2</a></div>




