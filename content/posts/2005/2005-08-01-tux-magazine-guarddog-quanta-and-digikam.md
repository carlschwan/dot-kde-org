---
title: "TUX Magazine: Guarddog, Quanta and digiKam"
date:    2005-08-01
authors:
  - "jriddell"
slug:    tux-magazine-guarddog-quanta-and-digikam
comments:
  - subject: "Guarddog is insecure by default!"
    date: 2005-08-02
    body: "Let your computer be an ICQ _client_ and the supposedly \"secure by default\" firewall opens all _server_ ports >= 1024!!!"
    author: "c"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-02
    body: "then don't use ICQ. Seriously, the ICQ protocol is grossly insecure and just doesn't work with firewalls. ICQ wants almost every port open so that it can do direct connections for chat.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-02
    body: "Perhaps it could check using netstat which server ports >1024 are already in use and exclude them from the rule?\n\nOk, it's harder, and it is not perfect, but it is slightly better.\n\nAnother idea: it could ask the user what ICQ app (say kopete) he is using and then do something like \"-m owner --cmdowner kopete\" on the iptables rule.\n\nOr, you could have ICQ-kopete ICQ-whatever rules.\n\nOf course, that only works on the OUTPUT chain, so the connection may still take place, but the box shouldn't send much stuff over that connection that isn't ICQ (or gadu-gadu/MSN/whatever kopete can handle), I suppose."
    author: "Roberto Alsina"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-03
    body: "then do 1 or more of the following:\n\n1. warn that the icq protocol is not secure\n2. use ralsina's proposal\n3. don't advertise guarddog as secure by default"
    author: "c"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-03
    body: "> 1. warn that the icq protocol is not secure\n\nI'll have a good think about how best to communicate that to the user.\n\n(Suggestions are welcome. I'm gravitating towards any extra 'risk' column with a small rating symbol. Instead of little gold stars there will be scary little skull-and-crossbones symbols. :-) Seriously, that is exactly what I am thinking now.)\n\n> 2. use ralsina's proposal\n\nThat is not likely to happen soon. It is, well, radically different than the current approach.\n\n> 3. don't advertise guarddog as secure by default\n\nICQ is not on by default.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-04
    body: "> > 1. warn that the icq protocol is not secure\n \n> I'll have a good think about how best to communicate that to the user.\n \n> (Suggestions are welcome. I'm gravitating towards any extra 'risk' column\n> with a small rating symbol. Instead of little gold stars there will be scary\n> little skull-and-crossbones symbols. :-) Seriously, that is exactly what I am\n> thinking now.)\n\nIn addition to that, how about a GRC-style set of coloured boxes that show what ports Guarddog has opened up.  Additionally doubly warn of server ports that are opened up.\n\n> > 3. don't advertise guarddog as secure by default\n \n> ICQ is not on by default.\n\nOh please.  The real issue is this:\n\nNo user can expect that allowing an ICQ client turns their computer into an open server!\n\n"
    author: "dsaf"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-04
    body: "To be honest I don't think that warning the user is really going to help much. People don't read warnings. The best idea I can think of right now is to choose a range of ports and open only those for ICQ, and tell everyone to configure their ICQ client. That looks like the safest way to me.\n\n(Email me, or take this to the mailing list. Lets not take over the Dot. ;) )\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Guarddog is insecure by default!"
    date: 2005-08-02
    body: "use jabber's icq transport (its supported at least by psi)"
    author: "Nick"
  - subject: "Guarddog is good but it can be the best"
    date: 2005-08-02
    body: "for average joe user Guarddog is an enigma! Anyone who knows these:\n\n1. TCP/IP\n2. Port addresses \n3. Client/Server applications\n\ncan only have a good firewall in place with Guarddog. That's definitely for some System Administrators but not for average joe user, who *uses* the computer but doesn't administer.\n\n\nHere What I think would help Guarddog become \"User-friendly\" not \"administrator-only\" application:\n\n1. Observing which applications are being used (learning mode) and allowing incoming and outgoing (if necessary) traffic.\n2. KDE applications should have firewall integration or Guarddog should monitor the Network applications like \"kopete\" or \"kmail\", and ask the user \"whether s/he wants to allow internet connection.\n\n3. likewise kpf (personal webserver) etc., should also take permission from Guarddog or Guarddog should restrict/allow an incoming connection.\n\nThis could only happen if Guarddog becomes a PART of KDE instead of just *an application*.\n\nif Guarddog implements few small things then Guarddog becomes the *friend* of average joe KDE user ;) else it will remain a friend of administrator which users in general can't make use of it.\n\nAnyways, I love Guarddog, it is much better than using iptables rules setting by hand. And I really wanna thank Guarddog Authors/Developers! Thanks guys you made my KDE/Linux system secure :)"
    author: "fast_rizwaan"
  - subject: "On the other Hand"
    date: 2005-08-05
    body: "Actually, I thought Guarddog was the easiest firewall software I've found in a long time (ever?). I tried several different frontends to generating iptables stuff and I couldnt figure any of it out.  None.  Then I found guarddog and was instantly impressed.  Perhaps I know a bit more about IP, servers and clients than the average person, but anything that was \"learning\" like you suggested would be a pain.  (I've tried ZoneAlarm on Windows and the constant popups about \"Internet Explorer is trying to access the internet in the following way:...\" just got bloody annoying.\n\nOne minor (ok major) problem which might not be guarddog related:  I'm on Gentoo and I had their scripts save the IPtables state and reload it when I rebooted.  This worked fine when I was behind a router, but now that I'm directly connected to the cable modem, it seems that the saved IPtables state blocks all internet access (while im sure thats very secure its not exactly what i want ;p).  I checked that the IP address doesn't change but I cant imagine what other problem would be."
    author: "james"
---
The <a href="http://www.tuxmagazine.com/node/1000144">August edition</a> of the free to subscribe <a href="http://www.tuxmagazine.com/">TUX Magazine</a> covers a number of KDE applications: <a href="http://www.simonzone.com/software/guarddog/">Guarddog</a> "<em>lets
you have total control over your personal firewall without having to invest years in the study of firewalls and security</em>" while <a href="http://www.digikam.org/">digiKam</a> is "<em>a perfect all-in-one solution for importing, editing and managing photo albums from all my digital cameras over the last few years</em>".  There is also an extensive introduction to KDE's webpage editor <a href="http://quanta.kdewebdev.org/">Quanta Plus</a> and you can vote for your favourite KDE applications in their first <a href="http://www.tuxmagazine.com/TUX_Readers_Choice">readers' choice awards</a>.

<!--break-->
