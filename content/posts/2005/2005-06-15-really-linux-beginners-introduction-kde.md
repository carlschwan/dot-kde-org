---
title: "Really Linux: Beginner's Introduction to KDE"
date:    2005-06-15
authors:
  - "brockers"
slug:    really-linux-beginners-introduction-kde
comments:
  - subject: "\"Windows to Linux\" series!"
    date: 2005-06-14
    body: "Hi KDE developers!\n\nDo you really care for Windows users to switch over to KDE+Unix? I feel there is a sublime hatred towards Windows in KDE!\n\nKDE itself doesn't allow any user to make it Windows like! The things which makes a Windows user comfortable on the Desktop is:\n\n1. Big Start button (now thanks for having it in SVN, and kbfx is good)\n2. KMenu: it can't be made like windows 2000 start menu or windows xp start menu!\n3. My computer (which thankfully you've implemented as System icon on the desktop!)\n\nI'm not saying that those windows menu and start button are technically superior or great but \"Millions\" of Windows users are used to that layout of start menu and rectangular button. \n\nDo you allow KDE users to have their (atleast at the configuration (xml file) level) kmenu to be made like windows' start menu? NO! You don't want KDE users to have the liberty to customize their beloved KDE like Windows! You are denying them the right to have it like that! Please give them the choice to modify (without programming) KDE like Windows or any other OS, as the user deem likeable!\n\nGive us the freedom!\n"
    author: "fast_rizwaan"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-14
    body: "The KDE programmers are not making windows. they are making a free, easy to use, powerfull operating system. and as the winXP menu is not easy nor powerfull, it has no place in KDE."
    author: "superstoned"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-14
    body: "Your right, KDE programmers are not making Windows! \"They are Making a free, easy to use, powerful _operating_system_?\" well, I rephrase that to \"Desktop Environment!\"\n\nNo Doubt, KDE is powerful, stable, attractive, and free! but for \"EX-Windows\" users and their families who were using windows, is not \"easy to use\"! because of these things:\n\n1. kmenu button\n2. kmenu menu layout!\n\nMillions of users are tired of Windows which has Viruses and Trojans and which is highly insecure OS! They are stuck into windows snare, they don't have time and energy to again learn a new Desktop environment! \n\nThe only thing they have problem with a New Desktop Environment like KDE or Gnome or XFCE is that They are totally baffled by the un-familiarity!\n\nDo you know how helpless you would feel in front of a Windows box when you have never used Windows earlier!\n\nLikewise for EX-Windows users, they _feel_ helpless and dumb in front of our great KDE! \n\nFor a person who has never used Windows or KDE, he/she would find KDE more userfriendly and easy!\n\nDon't you feel sympathy for (ex)windows users who are troubled by their insecure windows desktop! be kind, help them to get rid of windows' suffering!\n\nYou can help relieve the pain, anguish and suffering! But will you help those miserable souls!? Please be kind to them, Let them get rid of Windows!\n\n"
    author: "fast_rizwaan"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "There you go:  http://www.xpde.com/\n\nWindows XP interface clone for Linux."
    author: "Leo S"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: ">There you go: http://www.xpde.com/\n>Windows XP interface clone for Linux.\n\nBut unfortunately, XPDE is in COMA (dead like). KDE is technically superior and  fully functional. whereas XPDE needs to grow a lot! even if they start now it will take them years to come close to KDE! \n\nWhereas KDE can be allowed to be like Windows just by allowing fully customizable kmenu!"
    author: "fast_rizwaan"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-17
    body: "For Usability discussion can join the www.kde-artists.org forums, there's lot's of talk there on these kind of things for the next kde 4.0.\n\ndan"
    author: "dan"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "'\"EX-Windows\" users and their families who were using windows, is not \"easy to use\"! because of these things:\n \n 1. kmenu button\n 2. kmenu menu layout!'\n\nYou're off your rocker. Windows users and their families are using Windows because it came preinstalled on their computer, and they're content enough not to care to change. Whether or not KDE's kmenu looks like the XP one is largely irrelevant, because most people aren't looking at switching in the first place. If anything, making it look exactly the same would probably discourage switching at least as much as encourage it, since 'hey, it's the same thing; why switch?'\n\n'The only thing they have problem with a New Desktop Environment like KDE or Gnome or XFCE is that They are totally baffled by the un-familiarity!'\n\nBoy, that's probably why there's a significant number of people switching to Macs because their totally unfamiliar interface is so easy for people to use.\n\n'Don't you feel sympathy for (ex)windows users who are troubled by their insecure windows desktop! be kind, help them to get rid of windows' suffering!'\n\nSo, your opinion of 'being kind' is to keep everyone using inferior solutions, rather than coming up with something better that will be easier for everyone to use in the long run?\n\nYour arguments could probably do without the Adopt-a-Third-World-Child-esque appeals to emotion, as well. These people aren't imbeciles. They aren't terrified as soon as they see a different looking start menu. If KDE comes up with something genuinely better, these people are perfectly capable of learning to use it, and eventually will if they're pining for change as much as you say. Most people will assimilate KDE enough to use it in a few days, tops. Those who can't probably have consistent problems using Windows as well.\n\n'Please be kind to them, Let them get rid of Windows!'\n\nCloning Windows is not a particularly good way of getting rid of Windows. Not all of its problems are behind the scenes. KDE should learn from its good points, but it should also try to improve upon its deficiencies. Copying everything in the name of seducing some mythical Windows switcher is not a recipe for success, nor is it even particularly likely to work.\n\nAt the point when 'millions' of people are considering a switch to KDE, the Kmenu not looking like Windows XP will not be an issue."
    author: "Dolio"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "fully agreed. and i'd like to add:\nwindows users where able to get used to the XP menu layout (which is different from the windows 2000 layout). can't they get used to the KDE menu layout (which is very similair to windows 2000)???"
    author: "superstoned"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "Yeah, exactly.\n\nPeople got used to working with graphical interfaces when moving from DOS to Windows, and then they got used to the start menu when moving from Windows 3.1 to Windows 95. And then they got used to the new look of the start menu when moving from Windows 2000 to Windows XP.\n\nThe notion that an open source desktop needs to clone UI elements from Windows to succeed is silly. Either people see enough of a benefit to switch, or they don't, and whether or not KDE has a rectangular button that says 'Launch', or whether the KMenu contains an entry labeled 'Recent Documents' rather than 'Documents' is insignificant in that calculation.\n\nThe only conceivable use I can see for all this is if you're trying to trick people into thinking that they're actually running Windows, in which case you'll just anger them more when something they try fails, and you're forced to tell them that you swiped out their Windows for (from their perspective) a crappy work-alike (and, let's face it, it's also an insult to their intelligence). Don't do that, you're not winning any points for KDE or other open source desktops."
    author: "Dolio"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-14
    body: "We failed. We miserably failed. Our one and only promise was freedom - and instead we keep oppressing those who long for freedom with square start buttons! Now it turned out that we're not better than the rest. I feel so sad and empty :'-["
    author: "uddw"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-14
    body: "> KDE itself doesn't allow any user to make it Windows like! The things which makes a Windows user comfortable on the Desktop is:\n\nWell, Windows does not allow any user to make it KDE like. KDE, however, allows to a great extent to be customized to look and feel like windows.\n\n> 1. Big Start button (now thanks for having it in SVN, and kbfx is good)\n\nAs far as I can see, KDE's start button by default is far bigger than Windows'. And as you said, in SVN it already is labeled \"Applications\".\n\n> 2. KMenu: it can't be made like windows 2000 start menu or windows xp start menu!\n\nDo you mean KMenu cant be made as chaotic as the Windows start menu is? At least, in KMenu applications are categorized and sorted by name. It takes lots of manual efforts to get it that way on windows.\n\n> 3. My computer (which thankfully you've implemented as System icon on the desktop!)\n\n\"My Computer\" is a stupid concept with roots going back to the operating system CP/M, which was designed 30 years ago for hobby computers of that time. Windows (yes, XP, too) is based on a CP/M clone (also called MSDOS). Having a list of your hardware as a starting point of your daily work means that you must know a lot about your computer's innards, which is unacceptable for most users. There is a reason why it is different on Unix/Linux systems.\n\n> I'm not saying that those windows menu and start button are technically superior or great but \"Millions\" of Windows users are used to that layout of start menu and rectangular button. \n\nYeah, and just because billions of flies eat shit, would you do that, too?\n\n> Do you allow KDE users to have their (atleast at the configuration (xml file) level) kmenu to be made like windows' start menu? NO! You don't want KDE users to have the liberty to customize their beloved KDE like Windows! You are denying them the right to have it like that! Please give them the choice to modify (without programming) KDE like Windows or any other OS, as the user deem likeable!\n \nInstead of complaining and demanding, why don't you get your hands dirty and change the KDE code to implement your wishes? That's what free software is all about."
    author: "no name"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: ">Yeah, and just because billions of flies eat shit, would you do that, too?\n\nlol, you don't seem to respect individual's freedom! If I were a fly and want to eat shit, then no fly (nobody) has the right to tell me to \"not to eat shit\", as I am a free fly (person), I have the right to eat shit or nectar! That's my freedom to choose and do what I like! And I respect your freedom of choice and action to not to eat shit but nectar!\n\nAnd if you were a fly, won't you want to have the freedom to eat nectar, instead of shit which most windows files (users) are forced to eat! and they are unfamiliar with KDE flower (which has the nectar of freedom and stability), we need to help them come to KDE flower by allowing them to make KDE flower as KDE shit! because they know only shit! they never saw nectar! To let them introduce to KDE nectar, we the KDE flower flies help them by showing nectar as shit, so that they feel comfortable eating nectar as shit!\n\nThat's a big shitty thing ;) Free and Let others be free to choose and do what they wish! \n"
    author: "fast_rizwaan"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "Aside from that your fly-shit-explanation doesn't make any sense to me, your right when saying that giving freedom to people is a good thing. BUT, in your first post you used an imperative language and complained about \"missing features\", which are actually implemented or suck. And your argument is: \"I know that feature sucks, but windows users are used to it, so you better implement it!\"\n\nAnd, I think you're the one who doesn't respect the individual's freedom. For example the freedom of the KDE developers not to copy the most crappy features of Windows.\n\nAs I said before, if you really want these features, I guess you'd have to implement them yourself. Because I don't see any KDE developer volunteering for that job."
    author: "no name"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "> And your argument is: \"I know that feature sucks, but windows users are used to it, so you better implement it!\"\n\nPlease let me rephrase that:\n\n\"I know that feature sucks, but windows users are used to it, so *could you please allow it to be get implemented easily without users to code for it!\"\n\nlike freetype's ByteCode interpreter (which is patented) and disabled by default, any user can 'Enable' the code to get the patented thing work for better TTF display!\n\nAnd Start Menu and Start button is not patented! You must have seen StarOffice 5.1 and 5.2!\n\nAnd I Love KDE developers and respect their work and opinions! I'm just requesting on behalf of desperate (unable to program windows users including me) to have them an easier way to be comfortable with KDE!\n"
    author: "fast_rizwaan"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "\"I know that feature sucks, but windows users are used to it, so *could you please allow it to be get implemented easily without users to code for it!\"\n\nPlease file a bug report and when enough 'desperate' people 'on your behalf' support it, it will be added.\n\nIn case you think reporting bugs is a wrong progress for users to go trough, then file a bug report about that process."
    author: "ac"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: ">In case you think reporting bugs is a wrong progress for users to go trough, then file a bug report about that process.\n\nNo way! I've filed hundreds of bug reports and wishlists in last 7 years of my KDE love affair!"
    author: "fast_rizwaan"
  - subject: "Flies and shit?"
    date: 2005-06-15
    body: "...wow   ..flies and shit huh?\n\nDude, sit down, relax...take a chair. I think the detail in your \"shit, nectar and flowers\" talk is kind of scary.\n\nLook, nobody is taking any freedom away from you as you can always offer updates yourself or via people who support you. There could actually be allot of support for what you ask, fine, in that case it's likely to be added anyway and I asume there to be sanity in the choices being made then. But some people will disagree. Bottom line is, developers, who mostly voluntary work on this, can add this or not. All somebody did is gave his _oppionion_ to you and you turned into a rather scary detailed essay about \"him not respecting your freedom\". But if absolutely no one supports you, you are always FREE to update it yourself. So you ALWAYS have the freedom to acheave that freedom you are talking about.\n\nYou cannot demand your choice, but you can get support for it or even work on it yourself. *That's* the freedom we are talking about here. Flies and shit are just that.\n\n"
    author: "ac"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "very true ac, 3 things required to make windows user comfortable with KDE! and as I said it\n\n1. Rectagular start button (which can be enabled in KDE 3.5/4.x)\n2. My Computer (We already have System in KDE 3.4)\n3. Kmenu layout (here please, allow it be changed completely without programming or with least code like un-commenting!)\n\nwe already have 2 things which makes life for ex-windows user easier, but kmenu I wish to be configurable! I read somewhere something like \"kmenu is planned not to be fully customizable!\""
    author: "fast_rizwaan"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "Best thing you can do is to file a bug report about this. I am sure if you use sensible words (stay away from flies and shit) you could perhaps get a discussion going about this, you then could receive insight or offer this yourself in return.\n\nPersonally I think it's more constructive to offer smart original solutions, that's what I like about this freedom and where KDE is going. Also in relation to Plasma (http://plasma.bddf.ca/), that's a prime example of creativity leading to inspiration.\n\nYou should bookmark this http://bugs.kde.org/ . I trust that if you use good enough arguments, people will use them. If you fail to convince people, you have to be open to see the other side of the arguement, this can be enlighting sometimes and perhaps inspire your own creativity actually."
    author: "ac"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "sure, I'll do that! but please have a look at this KLaunch2 button i've created for kbfx! to help Ex-Windows users!\n\nhttp://tinyurl.com/btf2b\n\nand my windows frinds just love it!\n"
    author: "fast_rizwaan"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "> but please have a look at this KLaunch2 button i've created for kbfx!\n\nNow I understand, all this trolling because this kbfx is a product of yours."
    author: "Anonymous"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "What are your problem with the Kmenu lauout, you want the prorams placed in a more random fashion, like windows? \n\nNo need to do any programing to change the layout and placement of the programs in the menu, you simply use the KDE Menu Editor(as you could have done for the last 5 years or more). Alt-F2 kmenuedit, or from the big button labled 'Edit K Menu' in the menus section of Configure KDE panel.\n\nYou may stop trolling now."
    author: "Morty"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: ">What are your problem with the Kmenu lauout, you want the prorams placed in a more random fashion, like windows? \n\nOne cannot rename/order (place above or below) these things:\n\n1. Recent Documents (can't be renamed as \"Documents\")\n2. Bookmarks        (can't be renamed as \"Favorites\")\n3. Run Command      (can't be renamed as \"Run...\")\n4. switch user      (can't be disabled)\n5. lock screen      (can't be disabled)\n6. logout...        (can't be renamed as \"Shutdown...\")\n\nand one cannot have 22x22 size for main menu and 16x16 for submenus! change in size will affect all menus :(\n\n\n>You may stop trolling now.\n\nYes, I do it, right now! ;)\n"
    author: "fast_rizwaan"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "For reordering those entries I think you are right, it does not look possible.\n\nFor your points 1,2,3 and 6 you you can change the name manually in the desktopfiles found in $KDEDIR/share/apps/kicker/menuext. But  honestly if any user have problems with the difference of those, the fundamental problem the user have are so big the KDE project can do nothing;-) \n\nOn points 4 and 5 I think it's possible to disable those with the kiosk framework.\n\nThe different sized icons don't look easily achieved, but that's a XP thing anyway. Since KDE don't come with a Luna themed style either, making any close resemblance to XP impossible."
    author: "Morty"
  - subject: "Re: Flies and shit?"
    date: 2005-06-15
    body: "As already pointed out the strings can be renamed. Even if they were inside a source file, because they are translatable and no one says you can't have a translation for \"Windows English\".\n\nAs for disabling the actions: I would really be suprised if those where the only two action that cannot be removed through KIOSK settings.\n\n"
    author: "Kevin Kramme"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-17
    body: "> you don't seem to respect individual's freedom!\n\nFreedom isn't the ability to make KDE developers work on the features _you_ want above the features other people want.\n\nYou are free to modify the source.  You are free to pay somebody else to modify the source.  You are free to file bug reports.  You are free to ask people to vote for the bugs you want fixed.\n\nYou are NOT free to make KDE developers work on your pet peeve, and it is NOT an infringement on your freedom if they choose to work on other things instead.\n"
    author: "Jim"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "Why don't you use XPDE\n\nhttp://www.xpde.org\n\nKDe shall never become like Windows."
    author: "Gerd"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "xpde.com[mercial] seems to be correct"
    author: "Anonymous"
  - subject: "Re: \"Windows to Linux\" series!"
    date: 2005-06-15
    body: "Let's see. With a simple graphic, I can easily make a start button and drop it in. I can also make KDE display a global top-menu for all applications if I wanted to make it look like a Mac. \n\nPlease don't come in here trolling like that, you are not doing anyone any good. "
    author: "Erich"
  - subject: "About the article"
    date: 2005-06-15
    body: "I must say that this is probably the worst introductory article to anything I've ever read. It says that it is for \"non-techie\" users, but it actually requires a lot of knowledge to understand it. In a KDE introduction, especially in such a short article, I would not expect any command line calls. At a very least, it should be explained what a Konsole is, if the word is used.\n\nThe concept of virtual desktops is used before it is explained (and the explanation is bad). The statement that 4 virtual desktops use a lot of memory is just plain wrong, and the recommendation of having more than 64 MB of RAM is useless, because you can't even do a graphical SuSE install with less memory.\n\nI don't think that any relevant (for new users) subjects are discussed in the control center section. In a beginner's article, I wouldn't even mention kcontrol, as this is for advanced users only. What about the desktop itself? Why isn't there more about the panel? How about the KMenu? The kcontrol part is twice as long as the konqueror part, which gives me the impression the kcontrol is used twice as much as konqueror, right?\n\nThis article probably gives new users more questions than answers. And the last paragraph is a joke, right?\n"
    author: "no name"
  - subject: "Re: About the article"
    date: 2005-06-15
    body: "How do you change Konqueror views?\n\n konqueror --profile filemanagement\n konqueror --profile webbrowsing\n konqueror --profile filepreview\n konqueror --profile midnightcommander\n konqueror --profiles     (shows the available profiles) \n konqueror --help     (offers help on using the command) \n\nThis will scare off many people! and windows users never used something called profiles (except hardware profiles), for a web browser and filemanager!\n\nI wonder why can't we have a 'kpresenter' introduction to KDE! that would be more attractive and eyecandy and easy to learn about KDE using presentation!"
    author: "fast_rizwaan"
  - subject: "Re: About the article"
    date: 2005-06-15
    body: "So I take it that this means you are volunteering to create such a presentation?\n\nVery cool, thank you in advance!"
    author: "Kevin Krammer"
  - subject: "What is this all about"
    date: 2005-06-15
    body: "A clone of the windows desktop is in my humble opinion senseless. I haven't one and millions of other people also don't have one. I wonder if they want. And what about GNOME? Should they also addopt it? No, I see no reason for this. Imagine you would have the same discussion with a Mac OS user (definitely superior to Windows). They wouldn't even understand what you want.\nBut KDE has at least the advantage to look like a Tiger :) without any share and \"free\" ware. Who wants the ugly Windows style? How wants the confusing PX start menu. I don't want to and I'm glad that I don't have to. Ever tried to find a program as a novice on a other Windows machine. I wish you good luck, good nerves and a sense of humour. Belive me, in this case it is easier to adopt than to make the same mistake again ;). Work long time with KDE and than go back to Windows. You will immediately miss the KDE "
    author: "joer04"
  - subject: "Re: What is this all about"
    date: 2005-06-15
    body: "Amen."
    author: "no name"
  - subject: "Re: What is this all about"
    date: 2005-06-15
    body: ">Work long time with KDE and than go back to Windows. You will immediately miss the KDE \n \nThat's what flies and shit and nectar and kde flower thing was all about! KDE is soooo userfriendly that windows can only dream of. \n\nI really can't live without KDE on my computer! it's 99% Slackware+KDE and 1% windows for games (Age of Empires!)\n\n> A clone of the windows desktop is in my humble opinion senseless. I haven't one and millions of other people also don't have one. I wonder if they want. And what about GNOME? Should they also addopt it? No, I see no reason for this. Imagine you would have the same discussion with a Mac OS user (definitely superior to Windows). They wouldn't even understand what you want.\n\nExactly for you and me Windows like desktop is just UGLY and crippled! I feel sympathy for windows users who are so much in trouble (viruses, trojans, security leaks, cracking, denial of service attack) by using windows desktop! \n\nBy allow them to have KDE exactly (without the copyright stuff) like windows they will realize that UNIX+KDE is the way to go ahead, and finally they'll switch over to KDE's default better desktop environment!\n\nThe increase in KDE+UNIX userbase will really help KDE+Unix! how? Device Drivers will be release for Unix/linux/freebsd, New applications and new people join the developers! Fresh blood will flow towards free Linux, Unix and KDE!\n\nIt may look bad, but the exodus from windows to Linux/Unix will bring more improvement and Unix acceptability as a great Desktop Platform!\n"
    author: "fast_rizwaan"
  - subject: "Re: What is this all about"
    date: 2005-06-15
    body: "> KDE is soooo userfriendly that windows can only dream of. \n\nSo what you're proposing is to make KDE more under-UNfriendly?"
    author: "Erich"
  - subject: "Re: What is this all about"
    date: 2005-06-16
    body: "fast_rizwaan, could you just think about what you write before you hit the \"Add\" button? You'd prevent lots of misunderstandings."
    author: "no name"
  - subject: "Re: What is this all about"
    date: 2005-06-18
    body: "Don't really think so. I'm a Linux user for like one year and a half now, but before that I've used all flavors of Windows (3.x, 9x) for over six years. Right now my machine has Kanotix64/KDE 3.3 which I use 95% of the time and Windows 98 SE which I use for games only. I began using KDE with version 3.1.4. The way it is, KDE is not so drastically different from Windows... and I actually got LOST in Windows XP's Start menu (being a long-date Windows user) last time I had to touch it on another person's machine! \nWith KDE it was pretty straightforward, even at the time I switched, and even though it was running over a totally different OS. And, I proved to myself here it IS straightforward for a beginner - I'm teaching my father (he's from a time when not only Windows, but home computers themselves did not even exist) some Internet basics, and I made the switch with him. I began with Windows 98, then created an user account and switched him over to Linux. The result - he loved Linux, since KDE offers great usability (even better than Windows) and a better experience overall (since Linux doesn't present us with that ugly blue screen and reboots the way Windows does). I configured the look and feel for him to look like Windows (like mine) and the OS change became almost transparent. In short - KDE doesn't need to be an exact clone of Windows to be the great desktop environment for Linux it is now. Even for beginners (my father) and people switching from Windows (my case a year and a half ago)."
    author: "nitrousoxide82"
---
<a href="http://www.reallylinux.com">Really Linux</a> has published a feature titled <a href="http://www.reallylinux.com/docs/kdeintro.shtml">Beginner's Introduction to the KDE Desktop</a> by Andrea Cordingly.  Topics covered by the article include Konqueror profiles and the KDE control center.  The article is part of their "Windows to Linux" series.



<!--break-->
