---
title: "Benjamin Meyer on Type Managers"
date:    2005-11-16
authors:
  - "jriddell"
slug:    benjamin-meyer-type-managers
comments:
  - subject: "No links?"
    date: 2005-11-16
    body: "Did you forget to include some links or is this a quicky on it's own?"
    author: "Bram Schoenmakers"
  - subject: "Re: No links?"
    date: 2005-11-16
    body: "Added\n"
    author: "Jonathan Riddell"
  - subject: "What does that mean?"
    date: 2005-11-16
    body: "amaroK and digiKam integrated in the Desktop? Aren't they today? What has become of the old Unix philosophy \"Do just one thing but do that right\"? And what if I prefer Jukbox to amaroK or even F-Spot to digikam? Does integration not need establishing good interfaces first? From what I read in this abstract (is there a link to Benjamins article?) I can't tell what he means nor if it's a good idea, leading to a user friendly Desktop or a Microsoft like unflexible feature monster which tries to do everything and does nothing right."
    author: "furanku"
  - subject: "Ouch!"
    date: 2005-11-16
    body: "Thanks for adding the link, now that I've read the article and also the reactions on slashdot, I think that would have been a good opportunity for the new KDE marketing group to intervened.\n\nTo me that all sounds like trying to establish a new buzz word by mixing obvious things with personal preferences. Take a look at the GUI of an existing good \"type manager\" and the mockup in the article and you get a feeling of what this article misses. That's the essential and the hard part: Writing something that is user friendly, has some cool new features, ... and that's where this article isn't helpfull at all. "
    author: "furanku"
  - subject: "Re: What does that mean?"
    date: 2005-11-17
    body: ">What has become of the old Unix philosophy \"Do just one thing but do that\n>right\"?\n\nThat isn't dead.  Things like amarok and digikam actually use smaller applications to achieve all their various tasks.  Those smaller applications adhear to the unix philosophy, they do one thing and do it right.  Really most KDE apps are frontends to a bunch of smaller apps.  This follows the philosophy just fine, it just brings all those apps that do one thing together in one place.  There's nothing wrong with that."
    author: "Carl"
  - subject: "Re: What does that mean?"
    date: 2005-11-17
    body: "A frontend is also a small application that does one thing right: the user interface\n\nCreate a very good UI and leave the lowlevel stuff for the CLI programms that do their own part right."
    author: "CAPSLOCK2000"
  - subject: "Re: What does that mean?"
    date: 2005-11-18
    body: "So CAPSLOCK200, are you saying that applications like amarok and digikam that do several tasks around one type of media shouldn't exist?  Is it better to have several applications and have the user use all of them to achieve what they want rather than one?  I'm not attacking you, I really am curious on your view."
    author: "Carl"
  - subject: "Re: What does that mean?"
    date: 2005-11-18
    body: "As far as I can tell, that is the exact opposite of what he is saying.  The point was supporting the paradigm of \"one-task-well\" programs by having multiple such sub-programs brought together in a UI such as amaroK, which is another \"one-task-well\" program, that task being user interface.\n\nI am quite curious how you could have gotten such an opposed interpretation from his post."
    author: "kundor"
  - subject: "Re: What does that mean?"
    date: 2005-11-23
    body: "You have to realize that many gui programs are just that, a gui. The \"real\" work is done by other programms, that are driven from the gui.\nFor example amarok is using an sql server for it's storage. It didn't create it's own storage module, but used an off-the-shelve component.\n\nThis way the application writer can focus on what he is good in, namely writing gui's. Somebody who is good in writing databases did the storage component of the program.\n\nI don't use Amarok or Digikam, so I don't know if they follow this system. Maybe they are \"fat\" applications, that do everything themselves.\nThere is nothing inherently bad in that, but it is conflicting with the time honoured Unix adagium: \"Do one thing and do it good\".\nExperience learns that every program has bugs, and the larger the program, the more bugs. By keeping the individual programs small it's easier to find and prevent bugs.\nAnother benefit of using small components is that you can replace a module that is broken or not to your liking.\nTake for example text editors. Everyone in ICT has a favorite text editor (Vim!), and nobody seems to be able to agree on 1 editor to use for everything. But that's not a problem, you can edit any file with any editor.\nAnd if you set the EDITOR environment variable other programs will use your favorite editor when an editor is needed. This would not be possible in a fully integrated system.\n\n"
    author: "CAPSLOCK2000"
  - subject: "Re: What does that mean?"
    date: 2005-11-23
    body: "Replying to myself after reading the thread again, it's obvious you do understand the Unix philosphy. Never mind my previous post.\n\nAs I'm posting anyways I'd like to point out that I recently came to the conclusion that Konqueror and Type Managers should not opose each other as a primary interface. They should integrate.\n\nKonqueror should provide as much of a general Type Manager as possible, eg a search & filter system, and load plugins specific to the file/object to be handled. Throw in Kat or some other desktop search system to locate the objects (not just files) and we have a very nice and extensible Type Manager framework."
    author: "CAPSLOCK2000"
  - subject: "type manager?  wtf."
    date: 2005-11-16
    body: "does this article mean anything at all?  "
    author: "KDE User"
  - subject: "The future?"
    date: 2005-11-16
    body: "If Type Managers are the wave of the future, please give me the past. While I like Amarok as a player, and the ability to organize my songs into multiple playlists, I find the concept of Type Manager confusing. Can't you just give me a simple a freaking file manager? For people who dump every song they've ever touched into a single folder, the type manager is the only way to find something. But for people like me who organize their folders, the type manager is an impediment. Why do I feel like a dangerous radical bucking convention everytime I create a subdirectory? Maybe I should just dump every file I own onto \"My Documents, just like my manager at work who can never find anything.\n\nI guess I'm just too old fashioned. I guess it's time for someone to push me out on the ice in a sled so I can freeze to death and no longer bother the young kids in the igloo. Until someone gets around to doing that, though, I hope they put a \"Old Fart File Manager\" button in their new fangled desktop."
    author: "Brandybuck"
  - subject: "Re: The future?"
    date: 2005-11-16
    body: "Gawd I can be such a drama queen at times!"
    author: "Brandybuck"
  - subject: "Re: The future?"
    date: 2005-11-17
    body: "Well, you might be acting the drama queen but IMO you have some valid points."
    author: "stumbles"
  - subject: "Re: The future?"
    date: 2005-11-17
    body: "I'm just hoping that the sled wasn't one of those valid points..."
    author: "Brandybuck"
  - subject: "Re: The future?"
    date: 2005-11-17
    body: "Hee, no. Not quite enough snow for the sled.\n\nLike you these new \"pair of dimes\" are in my view a lot like the new toy my daughter wants. It looks pretty, nifty, etc and once she gets it. Plays with it for a little while and off it goes in the toy box along with all the other stuff she doesn't play with.\n\nNow I'm not trying to start a flame fest here. But there have been many technologies proclaimed to be the greatest thing since sliced bread.\n\nJust leave me the option of using the stone age methods."
    author: "stumbles"
  - subject: "Re: The future?"
    date: 2005-11-18
    body: "\"But there have been many technologies proclaimed to be the greatest thing since sliced bread. Just leave me the option of using the stone age methods.\"\n\nI'm just trying to get back to the days when everything didn't have to be a distributed component!"
    author: "Brandybuck"
  - subject: "Re: The future?"
    date: 2005-11-17
    body: "Well, as the number of files that you personally mangage goes through the roof, you probably will eventually want something to make the process more efficient.  You probably already use a type manager as I seriously doubt that you have manually setup and manage the files that comprise all the software on your system.  You use a package manager that automates that process and presents you with a simple interface for adding, removing and updating pieces of software without having you manage each and every file that comprise the software on your system.  "
    author: "Joe Kowalski"
  - subject: "Re: The future?"
    date: 2005-11-17
    body: "Yes the number of music files, videoclips and movies increase rapidly but it is only a limited number of types that increase rapidly. For these types of files a metadata type organization is OK, but when you want to group many different types of files this metadata stuff is not effective any more. A directory structure gives the lazy (I think most of us are) a simple and quick method to group files of various types. Example: I have a project that contains text files, make files, shell scripts, source files, header files, and various other exotic files. With the metadata model I would have to add a tag to every file. Now if I want to copy some of these files and add them to another project I would have to make a copy and re-tag the files. It could be possible to use the tags as we use directories today, but I think this method would be a LOTT more confusing for the inexperienced user.\n\nWhat could be good is an indexer (like locate) that would be integrated in the filesystem and that would be updated as the filesystem changes. Indexing files containing specific data would be a waist of time/speed and diskspace. You don't always know in advance what data you are going to search for. \n\nSo what we need is a smart search mechanism not a new file browser.\n"
    author: "K\u00e5re S\u00e4rs"
  - subject: "How to get rid off dubs"
    date: 2005-11-16
    body: "My question is how to get rid off dublicate programs in KDE?\n\nI mean two image viewers, I mean x music players, 3 editors (or 2.5).\n\nWhat iTunes explained to us is that what matters is not the functions but the combination. Type manager is a good phrase. But this means that the functions have already to be there and you can recombine them. Firefox is another example. Or Kontact.\n\nMultimedia and Usability."
    author: "gerd"
  - subject: "Re: How to get rid off dubs"
    date: 2005-11-16
    body: "\"My question is how to get rid off dublicate programs in KDE?\"\n\nJust don't install both?\n\nToo trivial?\n\nAutomatic removal of duplicates could anger some users"
    author: "Kevin Krammer"
  - subject: "Re: How to get rid off dubs"
    date: 2005-11-16
    body: "There could be a program cleanup wizard, though. You run it (manually, so as not to annoy people) and it goes through your system checking for programs with very similar functionality. (Do you really want juk and amarok?) Then it provides a list of \"duplicates\" and you can choose either to get rid of a dupe or to keep both. There would have to be a series of plugins for different package management schemes (rpm, deb and ebuild); these would then work out what package(s) the duplicates were in, and uninstall them. There would be a problem, I suppose, for distributions that have huge bloated packages like \"kdemultimedia\" instead of separating things out nicely as they should, but that would just become a matter of educating the distributions to do things differently."
    author: "Adrian Baugh"
  - subject: "Re: How to get rid off dubs"
    date: 2005-11-17
    body: "Even better would be to not necessarily uninstall the duplicate applications but remove all references to them.  For instance, who cares if I have Kwrite installed if none of my mimetypes know about it and it isn't in my KMenu?  The program could search filetype associations and detect which filetypes have multiple applications associated with it.  After the user selects which apps they don't want, the apps are de-associated with the various filetypes and removed from the KMenu.  Besides so many distributions have apps installed that the user never even knows about.  I know I have found many through the various years."
    author: "Carl"
  - subject: "Type Managers and KDE"
    date: 2005-11-16
    body: "I wrote up a blog entry talking about what *I* think Type Managers really means for KDE here: <a href=\"http://www.kdedevelopers.org/node/1624\">http://www.kdedevelopers.org/node/1624</a>\n\nPretty much we have some work to do.\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: Type Managers and KDE"
    date: 2005-11-17
    body: "After so much of bottlethrows, I hope you would be seriously considering reviewing your own blog article or in the future, would try to avoid dot posting of a simple blog :-P\n"
    author: "vkhaitan"
  - subject: "Re: Type Managers and KDE"
    date: 2005-11-17
    body: "Hmmmm.... one can only wonder what vkhaitan really meant by that post.  I do review my articles, in fact I had a number of people review it."
    author: "Benjamin Meyer"
  - subject: "Re: Type Managers and KDE"
    date: 2005-11-17
    body: "hmm, you are right. A number of people here reviewed your article and thrown bottle over it :P . I agree, it has been reviewed by many people :)\n(just kidding man)\nOn a serious note, I think, You should be more clear about your vision in the article. I could not find enough exciting information into it. May be because, partly , I could not understand it.\nI can only understand one fact, people should never bother about where their file goes."
    author: "vkhaitan"
  - subject: "OT: KDE3.5rc1 packages for debian sid available"
    date: 2005-11-16
    body: "\nhttp://lists.debian.org/debian-kde/2005/11/msg00060.html"
    author: "ac"
  - subject: "Don't forget KimDaBa"
    date: 2005-11-16
    body: "KimDaBa has been an absolutely brilliant 'Type Manager' for images and in that funcitonality pre-dates digiKam by a few years.  \n\nIt deserves a mention, I think.\n\n\n"
    author: "Dan"
  - subject: "better name"
    date: 2005-11-16
    body: "How about calling them Kype Kanagers?  That name sounds *much* better and less confusing."
    author: "helpful"
  - subject: "Re: better name"
    date: 2005-11-16
    body: "KYawn.\n"
    author: "cm"
  - subject: "Re: better name"
    date: 2005-11-17
    body: "It's really confusing, in the other world, the world outside KDE, a type manager is used to manage Type. As in Fonts. I'll be a lot of people will think of that first, at least I did. I thought KDE had finally gotten a Type ie Font manager.\nConfusing....."
    author: "Tink"
  - subject: "Re: better name"
    date: 2005-11-17
    body: "Me to. At fisrt I thought it was some cool font manager for KDE 4 that lets me manage fonts."
    author: "Mark"
  - subject: "Re: better name"
    date: 2005-11-17
    body: "Same here. However wrong we may have been, a Type Manager of the original and more naturally-named fashion is a Type Manager of this new-fangled and rather-confusingly-named fashion that we really could use."
    author: "jameth"
  - subject: "Extend Konq?"
    date: 2005-11-16
    body: "Is there anyway that Konq can be extended to be a type manager? maybe thru kioslaves?\nThen each individual app such as juk could use an embeded konq window for the types that juk says it wants to manage and then konq would also know more about the files when in general file manager mode.\nWe could also create profiles for a specific set of types.\nOne of the types mentioned in the article is email. Could kontact/kmail use an embeded konq for email?\nWhat about bookmark management?\nWhat is used for the file dialogs when saving and opening files in programs like koffice? This could get the benefit and become a type manager for koffice file types.\nAre my ideas useful or am I just not understanding the concept?"
    author: "Zot"
  - subject: "Re: Extend Konq?"
    date: 2005-11-16
    body: "There are circumstances in which you need a filemanager or something similar: a businessman would want to associate files belonging to a particular project, rather than all OpenDocument files, for example.\nHowever, I do believe there is a lot to be said for the idea of \"type management\" (if not for the name) and I think that now, while KDE4 is still up in the air, is the time to get it right. In my opinion KDE is going down the right track with kparts allowing for embedding of media viewers in konqueror and kio-slaves allowing for konq to cope with much more than local filesystems. However, to be brutal about it most of the kparts are a bit lacking; they are mostly little more than embedded viewers. All the context of the rest of the directory (think project files) disappears when, for example, I look at a particular PDF using the kpdf kpart. Also, while many kparts do a great job as viewers, very few include much worthwhile editing functionality.\n\nSidebars: do we need the amarok sidebar as readily available when dealing with a project full of image files as when dealing with a project full of mp3s? Sidebars should put themselves forward on a more intelligent basis, when they can offer functionality relevant to the content being worked with.\n\nIn KDE4 I believe we need to rethink the philosophy behind kparts so that, for each file the user may click on, konqueror offers functionality relevant to that file via a kpart viewer/manipulator and a sidebar or menus with the relevant editing and metadata handling functionality, but still (by default, anyway) has a strip making available other files in the same directory: the user has grouped them together for a contextual reason, so why should konqueror take that grouping away just because one particular file is foregrounded?\n"
    author: "Adrian Baugh"
  - subject: "Re: Extend Konq?"
    date: 2005-11-17
    body: "I agree. on one hand, i think typemanagers are a great concept. now this article has made clear what they are, and what they are good for, developers of type-managers can re-think what they are doing, and do it better. for example, amarok should evolve in a much more complete music manager. better integration of burning, ripping, downloading etcetera.\n\non the other hand, imho konqi should be able to do these things too (with help from the type managers). so the KIO slaves system might need some refinement."
    author: "superstoned"
  - subject: "Re: Extend Konq?"
    date: 2005-11-17
    body: "Yes, that's the way to go in my opinion. We need Konq. to handle those \"type manager\" programs in an easy ui.. if it's KIO slaves, KParts I don't know. We need  multimedia to be easy and consistent in KDE. Does anybody know that the plans are for multimedia in KDE4?"
    author: "David"
  - subject: "Re: Extend Konq?"
    date: 2005-11-17
    body: "All components of a TV/video typemanager application are there. "
    author: "gerd"
  - subject: "Type Manager = confusing!"
    date: 2005-11-17
    body: "Really it means fonts for many a people. a \"Data Type Manager\" or \"Data Type Content Manager\" or \"Content Type Manager\" or something more appropriate and unambiguous would be nice.\n\ngood article though!"
    author: "fast_rizwaan"
  - subject: "Re: Type Manager = confusing!"
    date: 2005-11-17
    body: "reminds me of ye old Amiga Datatypes ;-)"
    author: "nn"
  - subject: "koffice as well...."
    date: 2005-11-18
    body: "maybe software like kword could move from a \"standard word processor\" to a \"document type manager\"\n\n"
    author: "Mathieu Jobin"
  - subject: "Re: koffice as well...."
    date: 2005-11-19
    body: "> maybe software like kword could move from a \"standard word processor\" to a \"document type manager\"\n\n\nA document manager is a good idea, but I don't think it should be in KWord. Rather, I think it should be in Kontact. Most documents have importance in relation to people and tasks, and Kontact is already designed to track those things. Thus, if it were extended to organizing documents that relate to contacts, I could look for PHI-220 and get all the papers I've done for that class so far, and I could look for NaNoWriMo and get all the chapters I've finished so far this month. Depending on the design paradigm, it might even be able to work emails and IM logs and the like into those lists, which I think would be really cool.\n\nFurther, if properly integrated with the proposed search system for KDE 4 it could be an excellent example of how to leverage that search system properly. One of the main problems of search systems is having difficulty structuring queries. Since Kontact would make it extremely easy for a user, even a user who isn't very computer savvy, to look for documents relating to a specific person or a specific task. For example, I could look up documents relating to Steph and I'd get all the stories we've been working on so far in one category, our emails in another category, our IM logs in yet another category, and a couple dozen pictures in yet another category. Because Kontact creates a pre-understood context by which the searches are conducted, a user can easily find things relating to the tasks and people they are currently dealing with.\n\nI think the idea of a document manager in Kontact has the potential to be a killer app; not in the sense of, \"Man, I gotta get that app,\" but in the sense of, \"After having used this for a while, I feel bereft without it.\""
    author: "jameth"
  - subject: "Re: koffice as well...."
    date: 2005-11-19
    body: "agreed."
    author: "wannabe using ....."
---
KDE developer Benjamin Meyer <a href="http://www.icefox.net/articles/typemanager.php">explains the concept of a Type Manager</a> as a new form of specialist file manager application.  "<em>In the past few years many of us have been introduced to a new type of application, the Type Manager. There are many Type Managers out there such as digiKam and amaroK that are gaining market share and a rabid fan base of users . Type Managers seem to have that magic combinations of features that makes users love them. I have been taking a closer look at the Type Manager, what makes them so useful, what they really provide for the user and came to some surprising results.</em>" He concludes that Type Managers are part of the future of the desktop.






<!--break-->
