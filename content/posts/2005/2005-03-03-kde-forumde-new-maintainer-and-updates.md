---
title: "KDE-Forum.de New Maintainer and Updates"
date:    2005-03-03
authors:
  - "zapalotta"
slug:    kde-forumde-new-maintainer-and-updates
comments:
  - subject: "I for one ..."
    date: 2005-03-03
    body: "... welcome our new Forum Overlord!\n\nTed Baker\ninfo@trendyhosting.com"
    author: "Ted Baker"
  - subject: "New ideas?"
    date: 2005-03-03
    body: ">> \u00a7igma announced some new ideas for the forum ...\n\nDid he elaborate on those ideas? His introductory post on the forum is somewhat vague on the subject."
    author: "Flitcraft"
---
Due to the unfortunate fact that Dirk Doerflinger, the old maintainer of the <a href="http://www.kde-forum.de/">German KDE Forum</a> has way too little time for the forum at the moment, we are pleased and grateful to announce that §igma from <a href="http://www.linux-web.de/">www.linux-web.de</a> has graciously adopted it. We moved the forum to his server a few days ago and are expecting the domain to be fully transferred; there may still be some annoyances with referring mails until the transfer is complete. This will soon be (or already is) history.


<!--break-->
<p>
§igma announced some new ideas for the forum and it really looks like this will blow new life into the community.
We apologize for the inconvenience of forum breakage and really hope that many people will continue using the forum and help each others just like they did the years before.</p>


