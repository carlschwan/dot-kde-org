---
title: "People Behind KDE: Janet Theobroma"
date:    2005-09-26
authors:
  - "jriddell"
slug:    people-behind-kde-janet-theobroma
comments:
  - subject: "Kool Kids!"
    date: 2005-09-26
    body: "Hey, Janet, nice to have you in KDE. I loved the answer about getting bored in a train ;-) Very nice family, you show it's possible to manage it all. You're bringing new ideas and new concepts to KDE and that's awesome, keep up the good work!\n/annma "
    author: "annma"
  - subject: " "
    date: 2005-09-26
    body: "I bet you've seen The Chocolate Factory, Janet :)"
    author: "Anonymous"
  - subject: "Good times"
    date: 2005-09-26
    body: "Glad to learn more about Janet.\n\nKDE is lucky to have her involved, that's for certain."
    author: "Wade Olson"
  - subject: "Re: Good times"
    date: 2005-09-26
    body: "As a Spanish speaker, I must say something about her signature  ;-P  It should be \"Las cosas claras y el chocolate espeso\", without the \"R\" in \"espReso\", just \"espeso\". Maybe it was just a misspelling in the article, but just in case...\n\nKeep on the good work!"
    author: "Bardok"
  - subject: "Re: Good times"
    date: 2005-09-26
    body: "Thank you. Yet another successful Kollaboration. ;) \n\nYes the K in Kollaboration is on purpose hehe. ;) ;)\n\n"
    author: "theobroma"
  - subject: "Re: Good times"
    date: 2005-09-30
    body: "<i>Yes the K in Kollaboration is on purpose hehe. ;) ;)</i>\n\nArgh! Am I the only one who hates this excessive use of the work K?\n\nIt's like a more mild form of l337. It isn't cool!\n\nThere have been some sane names: appeal, plasma, quanta (thank god it's not 'kuanta'), umbrello and so on, but most of the K names are like horribly contrived acronyms. Examples:\n\naKode\namoraK\nKontact\nKuickshow\n\nSome of them are less offensive:\n\nKile\nKDevelop\nJuK\n\nPlease! Application name choosers! Think of the children!"
    author: "Tim"
  - subject: "Re: Good times"
    date: 2005-10-01
    body: "amoraK? Don't you mean anoraK? And where could I get it?"
    author: "ac"
  - subject: "Re: Good times"
    date: 2005-10-02
    body: "he must mean amaroK, only the best media player for linux out there :)\n\ngrab it at http://amarok.kde.org and you won't be disappointed."
    author: "leo"
  - subject: "Re: Good times"
    date: 2005-10-03
    body: "Kollaboration is a koncept kreated by kinda klever people klaiming to have fun while kooroperating with others. Keep in mind that Kollaborating is kool and helps kut down on the kalamity the applikation devel faces konstantly. It should also be futher knoted and taken into konsideration that the K in Kollaboration was chosen mostly for fun and the humor faktor. K? ;) "
    author: "lando"
  - subject: "Good reading as usual"
    date: 2005-09-26
    body: "It's always great fun to read these interviews.\nThis time it was even more interesting than usual, since it gave some new insights into what it can be like to be an KDE developer. I feel a lot of respect for Janet Theobroma! (Everyone else in KDE as well, but a mother of five - that really is impressive :-)\n\nG\u00f6ran Jartin\nVarberg, Sweden"
    author: "G\u00f6ran Jartin"
  - subject: "Re: Good reading as usual"
    date: 2005-09-27
    body: "yeah, i agree. she seems to be cool... and i enjoy her work on KDE."
    author: "superstoned"
  - subject: "I am surprised..."
    date: 2005-09-27
    body: "... noone noticed this\n\nhttp://www.hort.purdue.edu/newcrop/duke_energy/Theobroma_cacao.html\n\nOr everyone did and I am just too dense? ;-)"
    author: "Roberto Alsina"
---
She is the woman behind the artwork contests on <a href="http://www.kde-look.org">KDE-Look</a> and the <a href="http://www.kde-artists.org">KDE-Artists</a> site where artists and coders can "kollaborate".  She also has more children than any other KDE contributor we know of and seems to have a liking for chocolate.  Tonight's star of <a href="http://www.kde.nl/people">The People Behind KDE</a> is geek girl <a href="http://www.kde.nl/people/janet.html">Janet Theobroma</a>.


<!--break-->
