---
title: "Kexi 0.9 Stable Released"
date:    2005-06-04
authors:
  - "jstaniek"
slug:    kexi-09-stable-released
comments:
  - subject: "shorthorn??"
    date: 2005-06-03
    body: "Haha, very funny."
    author: "ac"
  - subject: "How do the two fit in?"
    date: 2005-06-03
    body: "Kexi and Knoda, that is. Are they both in competition for the exact same place and one of them is going to have to die? Or is there a difference that makes having both useful?"
    author: "mikeyd"
  - subject: "Re: How do the two fit in?"
    date: 2005-06-04
    body: "> Kexi and Knoda, that is. Are they both in competition for the exact same place and one of them is going to have to die?\n\nWhat is the MS inspired perception that only one open source application should exist for any given use? For those who don't yet get why it is so important to have diversity choice and competition is a good thing look at how innovation has been thriving in other industries and how many years it has taken MS to produce a tabbed browser. If a project should make a critical decision you don't like or fail to be viable in some area, having others to choose from is good. Projects and users benefit from competition.\n\nAs for how the programs stack up, why not try using both and decide? Another factor is if you don't see a need for a data management tool you can get other people's opinions, but if you have a need your opinion matters most. ;-)\n\nBTW it's possible to use them both with the same databases and other access methods like PHP. So choosing doesn't need to have the expense of importing and exporting data and if you already have databases you can try integrating them."
    author: "Eric Laffoon"
  - subject: "Re: How do the two fit in?"
    date: 2005-06-04
    body: "But it sounds as if kde and especially koffice are already very short staffed. In a time when you don't have enough developers, doesn't it make sense to come together to build one good app first, then offer choice?"
    author: "anonymous"
  - subject: "Re: How do the two fit in?"
    date: 2005-06-04
    body: "So lets have a centrally controlled system, where someone somewhere decides how everyone uses their free time. Or their money as in the case of Kexi.\n\nA short while ago we had very few desktop database solutions. Now we have a choice.\n\nIn other words, it's working.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: How do the two fit in?"
    date: 2005-06-04
    body: "it's just that knoda and kexi don't do the exact same thing.\nKexi is a real Access replacement with advanced and easy form designing and stuff.\nKnoda is more of a KDE version of phpmyadmin that works with many DBs, it also offers an \"API lib\" that allows other kde apps to connect and manage those DBs easily (eg kommander). There's also a form designer nut it's not as complete as kexi's (although I might me wrong here cause it's been a long time since I last checked out knoda).\nHope that clears up things a little."
    author: "Pat"
  - subject: "Re: How do the two fit in?"
    date: 2005-06-04
    body: "Competition is a good thing(R)!\nI go a bit further than Eric, I will give you the example within KDE.\n\n1. K3B is one of the best CD/DVD burning tools in OS. But it wasn't the first. We had KWinOnCD and we had cdbakeoven. Both existed before K3B and AFAIR KWinOnCD was the one we shipped with KDE. Even that the people from K3B started their own project in favour of enhancing the other ones, they generated the best now.\n\n2. Amarok is one of the best mediaplayers. But it wasn't the first one. There was Noatun before and even this one wasn't the first (and I think we had another one before).\n\nSo the competition here showed how good programs can be and the best is now our favorite!\n\nOn the other hand, both are developed differently. Horst Knorr prefers his own style (more independent), while the Kexi guis prefer to be part of the big community. Both styles have their merits, I wouldn't say there is a golden way.\n\nSo let the persons do what they prefer and at the end the users decide what to use.\n\nPhilipp"
    author: "Philipp"
  - subject: "kubuntu packages"
    date: 2005-06-04
    body: "Are there (k)ubuntu packages available?\n\nThanks\n\nTim"
    author: "timlinux"
---
The <a href="http://www.kexi-project.org/">Kexi</a> Team <a href="http://www.kexi-project.org/wiki/wikiview/index.php?0.9Announcement">have announced</a> the availability of Kexi 0.9, codenamed "Shorthorn", the newest release of the integrated environment for managing data. Changes include official support for database forms, many improvements in handling server databases. For user convenience, Tabular Data View's behaviour is now very similar to Form Data View's behaviour. Data and project migration from existing data sources (SQLite, MySQL, PostgreSQL) is supported, and import of Microsoft Access database files (MDB) is available as optional plugin. The project delivered tens of overall improvements and hundreds of bug fixes. Read the <a href="http://www.kexi-project.org/wiki/wikiview/index.php?DetailedChanges0.9">full changelog</a> and enjoy <a href="http://www.kexi-project.org/screenshots.html">screenshots</a>.








<!--break-->
