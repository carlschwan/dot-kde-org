---
title: "Dutch KDEPIM Meeting Kicks Off "
date:    2005-05-28
authors:
  - "fmous"
slug:    dutch-kdepim-meeting-kicks
---
Today you will find 20 hackers on the lawn amongst the flowers and the peacocks, enjoying the Dutch sun. <a href="http://www.wallsteijn.nl/">Annahoeve</a> plays host to the fourth KDEPIM meeting which is situated in Achtmaal, Netherlands. You can read the press release below.




<!--break-->
<p><strong><a href="http://www.kde.nl/agenda/2005-kdepim-meeting/press_release_en.php">Press release from kde.nl</a>.</strong></p>

<p>Today a group of KDE programmers have gathered in the little village
of Achtmaal, Netherlands in the <a href="http://www.wallsteijn.nl/">Annahoeve</a> to work on the PIM applications in KDE over
the next few days. This is quite a special meeting for the developers
who normally only meet by email or on IRC.</p>

<p><a href="http://www.kde.org">KDE</a> is the leading Linux desktop, with over 600 applications,
translated into 88 languages and created by some 1100 developers,
translators and artists.  The <a href="http://pim.kde.org">KDE PIM project</a> works on <a href="http://www.kontact.org">Kontact</a>, an
application suite for KDE that helps you to organize your e-mail,
addresses, todo's, appointments and other personal information. It
also includes programmes to keep your mobile phone and PDA in-sync
with your desktop data.</p>

<p>The expectations of the meeting are high. Cornelius Schumacher, one of
the programmers who created Kontact, announced that he would like to
discuss the roadmap to KDE 4. Such discussions are far more efficient
when all involved are located in the same room. Will Stephenson,
programmer of instant messenging client Kopete, announced he will be
working on a <a href="http://www.skype.com/">Skype</a> frontend in <a href="http://kopete.kde.org/index.php">Kopete</a>. Better syncing of mobile
devices, more instant-messaging integration and input from usability
experts is also on the agenda.</p>

<p>What really is stunning is the amount of support from commercial
companies to make this meeting possible. Not only was <a href="http://www.trolltech.com">Trolltech</a> very
generous, but also the <a href="http://www.nlnet.nl/">NLNet foundation</a>, the <a href="http://www.nluug.nl">Dutch Unix Users Group
NLUUG</a> and several smaller Dutch companies like <a href="http://www.mensys.nl">Mensys</a>, <a href="http://www.i2rs.nl">i2rs</a> and <a href="http://www.kovoks.nl">Kovoks</a>.</p>

<p>All work and no play makes KDE PIM programmers dull boys and girls, so
the weekend will also have its share of "play". There will be a
sponsor session, a chance to meet developers from related projects and
a social event.  The NLUUG and Holland Open conferences which fall
before and after the meeting will receive their share of attention as
well.</p>

<p>The <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">meeting</a> ends on sunday evening when the KDE PIM programmers will
disperse across western Europe again, while anticipating the next
KDE PIM meeting.</p>

