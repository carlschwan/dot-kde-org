---
title: "Quickies: Trolltech, KHTML, Kontact, VoIP and KOffice"
date:    2005-05-27
authors:
  - "jriddell"
slug:    quickies-trolltech-khtml-kontact-voip-and-koffice
comments:
  - subject: "Line breaks PLEASE!"
    date: 2005-05-27
    body: "Could someone re-edit this newsitem with some markup please? Could all those <ul><li>\"...\"</li></ul> items be put to a list (&lt;ul&gt;)? I'd edit it here but only plain text is allowed :(\n\n"
    author: "Anonymous Coward"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "Yea, I was thinking the same thing.\n\nThe info is *GREAT* to have available, as is the announcement in and of itself, but it's a VERY brutal read on a web page...\n\n"
    author: "Xanadu"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "I've added line breaks in and posted it on my website. Please do not post there but instead post here! I only posted it on my site to make it more readable.\nhttp://smileaf.org/news/1117166133/"
    author: "Stephen Leaf"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "I checked your website's layout version: it is not much better there, the linebreaks dont make a huge difference.\n\nThe major difference could be by\n\n * really\n * formatting\n * the news\n * into listed (<li>....</li>)\n * items\n\nNothing else."
    author: "anonymous"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "there you are. it is alot better you are right."
    author: "Stephen Leaf"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "Its still a bit cramped.  Could you please please please add a little bit of spacing between each line?  An extra <br> might do the trick.  I'd edit it myself but I can't seem to get proper access to your site.  Also, could you increase the font size?"
    author: "ac"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "OH COME ON! It wasn't that bad! Stop complaining please.\n\nOh wait, were you being sarcastic?"
    author: "Phil"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "Well, I think the fonts on his site are small thats why it looks cramped and hard to read.  Again, I would fix it myself but I cant.  All I can do is complain and hope it gets fixed promptly. =)"
    author: "ac"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "Ah, the Powerpoint generation. ;-)"
    author: "Kevin Pfeiffer"
  - subject: "Re: Line breaks PLEASE!"
    date: 2005-05-27
    body: "Well, I'm not a member of the powerpoint generation, but if you replace the \"***\" by \"+++\", add a \"stop\" here and there, you'll get the right impression from which time this kind of formating is coming from.\n\nBut as long as my computer still has 329 teletypes in it's /dev/ directory, that's OK to me. And as we all know from sci-fi movies: The better the computer the slower the character output rate. So, could we please add some strange noise when each characters appears on the dot? ;)"
    author: "furanku"
  - subject: "OpenWengo!!!"
    date: 2005-05-27
    body: "works like charm in Windows 2000 professional, but doesn't compile on Slackware 10.1 with qt 3.3.3, has anybody got success compiling Wengo on Linux? It shows error at Sound.cpp compilation!\n\nsee also:\nhttp://developer.berlios.de/forum/forum.php?forum_id=9436\n\nHey KDE guys you can fix the issue easily, since it requires some small change in QT Sound constructor, here's the error message:\n\n-------------------------------------------\n++ -c -pipe -Wall -W -g  -DQT_SHARED -DQT_NO_DEBUG -DQT_THREAD_SUPPORT -I/usr/lib                                  /qt/mkspecs/default -I. -I../util -I/usr/lib/qt/include -Itmp/ui/ -Itmp/moc/ -o tm                                  p/obj/Sound.o unix/Sound.cpp\nunix/Sound.cpp: In constructor `Sound::SoundPrivate::SoundPrivate(const std::string&)':\nunix/Sound.cpp:32: error: no matching function for call to `QSound::QSound(const std::basic_string<char, std::char_traits<char>, std::allocator<char> >&)'\n/usr/lib/qt/include/qsound.h:48: note: candidates are: QSound::QSound(const QSound&)\n/usr/lib/qt/include/qsound.h:54: note:                 QSound::QSound(const QString&, QObject*, const char*)\nunix/Sound.cpp: At global scope:\nunix/Sound.cpp:62: error: prototype for `void Sound::play(const std::string&)' does not match any in class `Sound'\nunix/Sound.cpp:58: error: candidates are: void Sound::play()\n./Sound.h:69: error:                 static void Sound::play(const std::string&, const std::string&)\nunix/Sound.cpp: In member function `void Sound::play(const std::string&)':\nunix/Sound.cpp:63: error: no matching function for call to `QSound::play(const std::basic_string<char, std::char_traits<char>, std::allocator<char> >&)'\n/usr/lib/qt/include/qsound.h:52: note: candidates are: static void QSound::play(const QString&)\n/usr/lib/qt/include/qsound.h:78: note:                 void QSound::play()\nunix/Sound.cpp:64:2: warning: no newline at end of file\nmake[1]: *** [tmp/obj/Sound.o] Error 1\nmake[1]: Leaving directory `/root/trunk/sound'\nmake: *** [sub-sound] Error 2\n-------------------------------------------"
    author: "Fast_Rizwaan"
  - subject: "Re: OpenWengo!!!"
    date: 2005-05-27
    body: "a) bugs.kde.org is the appropriate place for reporting bugs and not braindead in a news item.\nb) QT bugs are forwarded to trolltech and not on an kde news item."
    author: "Wengo"
  - subject: "Re: OpenWengo!!!"
    date: 2005-05-27
    body: "OpenWengo is not developed in kde.org repository, so bugs.kde.org is the wrong place."
    author: "Anonymous"
  - subject: "Re: OpenWengo!!!"
    date: 2005-05-27
    body: "I asked to the wengo.fr forum and this is what the dev answered:\n\n\"2 months ago I had fixed all the compilation  problems in trunk for the linux version. It compiled and worked.Today I work on another branch (which compiles under linux and Windows, and does not use more qmake/make but scons http://www.scons.org ). \n \n Given that I did not work on trunk during this time, the modifications of the other developers made that the code does not compile any more under linux (win32 is for the moment the principal target of wengo). \n \n In little time (ie a few days) my changes will be merged back in  trunk, you will thus have a softphone usable under linux. Then we will try to make  trunk compiles all the time and everywhere. \n \n While waiting, the compilation problems for Thread.cpp/h was fixed in trunk (or in v1.1 branch I don't remember). As regards audiodevice it is not corrected in the trunk.\"\n\nSo that mean we'll have to wait a couple of days before he merges everything into trunk.\nHe also told me that they're about to release a English website so that non-french people can buy credits too :)"
    author: "Pat"
  - subject: "KDE for win98 machines?"
    date: 2005-05-27
    body: "mepis-lite low-resources version of their distribution\n-------------------------------------------------------\n\nGreat but, KDE needs to be low-resource friendly and responsive.\n\nI have observed that recompiling the kernel without the OSS sound support improves kernel responsiveness and also KDE applications loads a lot faster. It took 3-7 secs to load konqueror ($HOME), but after the kernel recompile, it takes around 1-3 seconds.\n\nI wonder is the kernel only thing which improves KDE performance, especially the loading of applications which are in SECONDS not in milliseconds.\n\nHow can we make KDE applications LOAD in MILLI-SECONDS instead of several seconds?\n\nKonqueror works better with \"instance preloading\", I believe preloading options for frequently used applications like:\n\n1. konsole\n2. konqueror file manager\n3. konqueror web browser\n4. custom applications (kdict, kplayer, kmplayer, kaffeine, kmix, etc.)\n\nwill greatly improve KDE responsiveness. Can't we have preloading for other applications apart from konqueror?"
    author: "linux_user"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "Are you aware that preloading occupies much memory which isn't available in large quantities on low-resources computers? Very contradicting."
    author: "Anonymous"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "I have Celeron 1.7 GHz, with 256MB RAM! still it feels very slow compared windows 2000 pro and xp. I guess preloading of applications might help KDE perform better (get responsive).\n\n"
    author: "linux_user"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "Again, preloading with 256MB RAM would likely make your machine start swapping which would make it *slower*."
    author: "Anonymous"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "> How can we make KDE applications LOAD in MILLI-SECONDS instead of several seconds?\n\nBuy a faster CPU and more RAM\n"
    author: "ac"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "well, very comparable software from the windows platform very often starts much faster on the same hardware, so there is obviously something wrong with kde/linux performance here.\n\nsome of this is due to GCC not being very fast, and it also has to do with certain optimizations simply not being present on linux systems (eg automatically alinging data on the harddisk to optimize startup times). also, windows gives a huge priority boost to apps starting and/or running in the foreground. this kills multitasking performance, but most windows users don't run many apps (probably since the windows interface does not make it easy, and because performance suffers...).\n\nanyway, the comments are legitimately - linux/kde IS slower than windows 98. yes, it has more features, and is more stable. but things could be improved. GCC 4.1 will help, and QT4 too. I think KDE 4.x, combined with GCC 4.x and linux 2.6.12 and up will be a serious contender to windows 98 in terms of speed (and kick Lonhorn's ass in terms of features)."
    author: "superstoned"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "Win9x is not a fair comparison to linux, because it doesn't have memory/access/.. protection a multi user OS should have. If you want to compare linux with something from Redmond, take at least a NT-based OS.\nAbout Qt4, we'll have to see of course. I remember the promises about Qt2 and Qt3, and they didn't really speed things up (probably it did, but new features ...).\nHowever at home I've compiled Qt/KDE with gcc-4.0 and there is certainly a noticeable speed up. As we all know for some time, the symbol resolving of the dynamic loader is the main culprit slowing down KDE based apps. Use of prelink (pre resolve symbols) and gcc-4 (which significant reduces the exported symbols) helps a lot.\nEven so, at my work I have a dual boot laptop with w2k and debian/testing (kde-3.3) and I find linux much better to work with. Yes simple apps, like kcalc, take longer to load. But after a days work, linux hasn't changed while with w2k even opening the 'Start' menu can take seconds.\nAlso linux GUI's benefits a lot from multi CPU systems, because of all inter-process communications it does (X/arts/kwin/kded/..). So the future, with multi-core, looks bright.\nAfter sarge, when KDE-3.4 comes in testing and everything compiled with gcc4, things like startup will improve a lot. IIRC, this will happen by the end of this month (yeah :-))"
    author: "Mult-kde-user"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "> After sarge, when KDE-3.4 comes in testing and everything compiled with gcc4,\n> things like startup will improve a lot. IIRC, this will happen by the end of \n> this month (yeah :-))\n\n\nCan anybody confirm this...?"
    author: "ac"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-28
    body: "Seems to be delayed again :-(, 6 june is new target.\nhttp://lists.debian.org/debian-devel-announce/2005/05/msg00020.html\nhttp://lists.debian.org/debian-release/2005/04/msg00146.html"
    author: "Mult-kde-user"
  - subject: "Re: KDE for win98 machines?"
    date: 2005-05-27
    body: "No need to buy a new PC. But you\nwill need to write and patch KDE\nto make it leaner. I'm sure that\npatches like that will be welcome.\n\nYou can't force a programmer not\nto implement something. You can\nhelp him implement it in a leaner,\ncleaner and faster way."
    author: "naomen"
  - subject: "about kdesvn-build (OT)"
    date: 2005-05-27
    body: "I'm trying to install kdesvn on my system instead of ~kde\n\nwhat do you mean by :\n # If you would like install KDE to the system (DO NOT INSTALL *over* a prior\n # installation!), then you'll probably need to use sudo to install everything.\n # make-install-prefix sudo\n how can I make it install in /usr ? and will I need to recompile everything?\n\nthx in advance\n\nPat\n\nPS: i'm asking it here cause there doesn't seem to be any mailing list for it"
    author: "Pat"
  - subject: "Re: about kdesvn-build (OT)"
    date: 2005-05-27
    body: "It means that if you have KDE binary packages installed, don't install over them, use a different path, like usr/local or /opt.\n\nOr, in other words, don't mix binary packages and and source installs, as you will create a mess when you try to update / remove software using your distribution tools.\n\nIf you wish to make a mess anyway, install to /usr, but at least remove the KDE binary packages first."
    author: "Amadeo"
  - subject: "Re: about kdesvn-build (OT)"
    date: 2005-05-27
    body: "I know it's bad to mix with another installation but I just wanted to know how I can install it to usr/local or /opt or /usr. I just remove the # from make-install-prefix? where do i need to specify the path?\n\nthanx in advance"
    author: "Pat"
  - subject: "Re: about kdesvn-build (OT)"
    date: 2005-05-27
    body: "I believe that you require the help of ./configure --prefix=/opt/kdesvn/\nOn my Suse box, I have kde 3.3, kde 3.4 and kde cvs (I've not gotten svn yet).\nEach one is in it's own /opt/kde<version>/ directory.\nThen you can edit your .bashrc to specify where your KDEHOME is, along with the new libraries.  Viola!  Multiversion install on one pc.\nRead here for other details:\nhttp://developer.kde.org/build/build2ver.html\n\ncheers,\n-Ryan"
    author: "Ryan"
  - subject: "Re: about kdesvn-build (OT)"
    date: 2005-05-27
    body: "No. If you really wish to install to /usr, you have to add:\n\nmake-install-prefix sudo\n\n*and*\n\nkdedir /usr"
    author: "Amadeo"
  - subject: "Possibly an unpopular post"
    date: 2005-05-28
    body: "Looking at these screenshots and thinking about it, what these other products (MS Outlook and Evolution) are and what Kontact is not is a cohesive application. Kontact is an example of the power of KDE, but also it shows where that power can be a weakness.  Sure you can combine extremely powerful KDE apps under one roof, but ultimately they are still individual apps.  There's no architectural integrity, and it shows.  This is not an attempt to belittle Kontact (I personally don't use it), but just honesty. I use all the apps in Kontact, I just generally just don't want them all at once.\n\nSo just as a suggestion, ignore the urge to combine these apps into one frankeinstein app, focus on making it very easy to use these apps together.  That is give them an invisible framework.  For example the ability to right click on a email and turn it into a note would be cool, turn a journal entry into an email simply by right clicking on it. Think of MS Office, Word and Excel work very well together, but I don't need them both up to get that integration.  That's what people want, small apps (tools) that I can master and combine with other tools to really get do amazing things.\n\nO.k. I'll hop off the soapbox.  :)"
    author: "brian"
  - subject: "Re: Possibly an unpopular post"
    date: 2005-05-29
    body: "You can actually accuse Kontact of being everything *but* not having architectural integrity. What you miss is integrity in the GUI, and that's obviously historical and being worked on."
    author: "ac"
  - subject: "Re: Possibly an unpopular post"
    date: 2005-05-30
    body: "What I meant by architectural integrity is being designed by one mind or a few similar thinking minds.  (See \"The Mythical Man-Month\" by Frederick P. Brooks.  Everyone should check it out, it's about managing large software projects. Although it feels dated at times, and isn't really about open source specifically I think it may be really worthy of your time.)"
    author: "brian"
  - subject: "Re: Possibly an unpopular post"
    date: 2005-05-29
    body: "The problem I have with Kontact is that Korganizer is buggy.  I've reported problems a long time ago yet see few commits in CVS/SVN.  I've seen a lot of work on Kmail, but nothing to fix problems like lack of meeting notification for meetings stored on the Exchange server.  Or frequent hangs of Korganizer wen changing the view or moving to a new day.  I've reported these bugs many months ago, but sadly it looks like Korganizer is dead.\n"
    author: "Aaron Williams"
  - subject: "Re: Possibly an unpopular post"
    date: 2005-05-31
    body: "During the KDE PIM meeting last weekend, a lot of new development was planned for KOrganizer. A merge with Ko-PI will happen. Features which are considered as useful will be implemented in KOrganizer.\n\nThere is only one reason for the slow development of KOrganizer: lack of developers. Reinhold and me (the most active developers last year) are currently quite occupied with other things. So some extra helping hands are always welcome."
    author: "Bram Schoenmakers"
  - subject: "Re: Possibly an unpopular post"
    date: 2005-05-30
    body: "nice example. try to drop an email on the \"todo\" button, or on the Korganizer button. a task or an agenda item (multiple mails for an meeting) is created. and I guess dropping a journal entry on kmail will open a mail with it, dunno for sure - don't use journals.\n\nso, what you say is already there. as another poster said, the interfaces might not look the same. but there is a integration. a lot :D\n\njust like Koffice, btw, which is as coherent as MS office."
    author: "superstoned"
---
Trolltech <a href="http://www.trolltech.com/newsroom/announcements/00000206.html">announced a successful round of funding</a> and <a href="http://www.trolltech.com/developer/affiliations.html">the end of their "relationship" with SCO and Canopy</a>. Read more on <a href="http://scottcollins.net/blog/2005/05/sco-long-and-thanks-for-all-fish.html">Scott Colin's blog</a>. *** LWN.net has <a href="http://lwn.net/Articles/136538/">an article on KHTML and Apple</a> featuring KDE e.V. board member Harri Porten. *** Open Source Versus did a screenshot comparison of <a href="http://opensourceversus.com/modules.php?op=modload&name=News&file=article&sid=199">KDE's Kontact against two of its rivals</a>. *** We've been notified of a couple of promising looking Qt/KDE based Internet telephony programmes, <a href="http://developer.berlios.de/projects/openwengo/">OpenWengo</a> and <a href="http://www.twinklephone.com/">Twinkle phone</a>. *** MEPIS has announced a <a href="http://www.mepis.org/node/6634">low-resources version of their distribution</a> using KOffice and if you haven't tried the recent KOffice 1.4 beta there are now <a href="http://download.kde.org/unstable/koffice-1.4-beta1/">packages available</a> for Kubuntu, Mandriva, SuSE and Slackware plus a special <a href="http://download.kde.org/unstable/koffice-1.4-beta1/contrib/x86-livecd">KOffice edition of Klax Live CD</a>. *** <a href="http://www.kde-look.org/content/show.php?content=24015">KDE Brilliant Buttons</a> are available for almost every KDE application, add to your blog or website to show your allegiances.
<!--break-->
