---
title: "Next Generation KDE Technologies Ported to WebCore"
date:    2005-07-11
authors:
  - "kpfeifle"
slug:    next-generation-kde-technologies-ported-webcore
comments:
  - subject: "Good programing"
    date: 2005-07-10
    body: "Sure this is hard, but also I'm sure it's easyer dealing with the long time KDE hackers vision have. The first time I saw the Qt/KDE API I fall in love with the flexible and simple interface it has. When I saw the code I said myself \"These guys know what they are doing, and they are doing it seeing the coming years\". But it would be sad if it's the only one supporting it, no webmaster mass will put svg until IE, or at least mozilla/firefox, can handle it. Someone knows if there is some plan for these browsers about svg or mathml?\n\nBTW I'll say I think these things are related to OO programing, slow at start, but when the framework is done, the speed increases geometrically. I'm seeing something like this with KOffice, many time building the bricks, without seeing the house is not the best frame for hackers to work on it. But now seems there are very solid walls, attracting good bricklayers, draughtsmans and architects :)"
    author: "mcosta"
  - subject: "Re: Good programing"
    date: 2005-07-11
    body: "AFAIK Firefox already supports SVG experimental. You'll just have to activate it in the about:config."
    author: "Patrick Trettenbrein"
  - subject: "chicken and egg"
    date: 2005-07-12
    body: "Mozilla has had support for MathML for years (5?) and some basic support for SVG also for many years (3?), although far from complete. But since no one is using these standards, they are not enabled in default builds. This reduces bloat due to unused features. They did the same for MNG. It used to be included, and they removed it.\nhttp://www.mozilla.org/projects/mathml/\nhttp://www.mozilla.org/projects/svg/"
    author: "oliv"
  - subject: "Mozilla svg is underway"
    date: 2005-07-11
    body: "the nightly build have had svg enabled for quite some time now and firefox 1.1 will ship, with svg switched on by default, soon.\n\nkudos to the KDE Hackers, i've taken a look at the sources in svn repository and was impressed ! Can't wait to see KDE4 going Alpha.\n\nbest regards\n\nMaxxCorp"
    author: "MaxxCorp"
  - subject: "Wondering..."
    date: 2005-07-11
    body: "Are Nicolas Zimmermann and Rob Buis employed by someone to do this work?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Wondering..."
    date: 2005-07-11
    body: "Hi Derek,\n\nUnfortunately not :} Its all (rare) spare time programming.\nI would love to hack on it fulltime, but there you go...\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Wondering..."
    date: 2005-07-11
    body: "Not yet :-)\n\nRob and me would be interessted to work \nfull-time on our babies, given the fact\nwe have so much more stuff \"in the pipeline\".\n\nThink about XPath, XSLT, MathML, (XQuery/XForms)\nand most important: Compound Document Format.\n(for instance inline SVG in xhtml etc..)\n\nWe'll keep on hacking, just like the last four\nyears, though bein' hired would surely push it even more.\n\nSo if anyone feels like helping KDE's future,\nyou know our mail adresses :-)"
    author: "Nikolas Zimmermann"
  - subject: "Re: Wondering..."
    date: 2005-07-11
    body: "\nYupp, couldn't agree more :)\n\nWhile a large part of XPath 2.0 is currently implemented, the complete support of XPath/XSL-T 2.0 in KHTML would arrive more predictably, if it was economically stabilized.\n\n\nCheers,\nFrans\n"
    author: "Frans Englich"
  - subject: "Re: Wondering..."
    date: 2005-07-12
    body: "Woldnt it be cheaper for Apple to use Rob and Nikolas for the job???? Does anyone knows somebody from HR in Apple?\n\n"
    author: "Mario"
  - subject: "Go Apple!"
    date: 2005-07-11
    body: "Go Apple!  It's nice to see the technology being pushed forward."
    author: "James Katt"
  - subject: "GREAT!"
    date: 2005-07-11
    body: "Anyone know if a QT4 backend is planned?"
    author: "am"
  - subject: "Re: GREAT!"
    date: 2005-07-11
    body: "Hi,\n\nYou must mean for linux, since currently the OS X backend\nuses the native toolkits and that will probably be most efficient.\nYes, a qt4 backend is likely to be created.\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: GREAT!"
    date: 2005-07-11
    body: "\"You must mean for linux, since currently the OS X backend\nuses the native toolkits and that will probably be most efficient.\"\n\nNot really.  I would want a backend that will work on any platform that supports QT 4.  :)  I am not interested in adding platform specific code to x-platform QT app.\n\nKeep up the good work."
    author: "am"
---
After successfully using <a href="http://khtml.info">KHTML</a> and <a href="http://en.wikipedia.org/wiki/KJS">KJS</a> as cornerstone technologies to build their much praised <a href="http://en.wikipedia.org/wiki/Safari_(web_browser)">Safari web browser</a>, Apple engineers have now made the first steps to adopt the next generation of KDE's web technology into their WebCore rendering engine.  <a href="http://developer.apple.com/darwin/projects/webcore/">Apple</a> developer Eric Seidel was proud to announce the <a href="http://www.opendarwin.org/pipermail/webkit-dev/2005-July/000273.html">introduction of experimental SVG support into WebCore</a>: <em>"Over the last few months I ported KDE's new <a href="http://www.w3.org/DOM/">DOM</a> architecture 'KDOM' as well as their Scaleable Vector Graphics (<a href="http://www.w3.org/Graphics/SVG/">SVG</a>) implementation 'KSVG2' and render tree library 'KCanvas' to WebCore.</em>"














<!--break-->
<p>There is no <a href="http://www.w3.org/Graphics/SVG/">SVG</a> support in Safari itself yet, but the chances of KDE 4's SVG technology being used by Safari and Mac OS X have been greatly increased by this move. There is now a special <a href="http://webkit.opendarwin.org/projects/svg">section devoted to SVG</a> on the <a href="http://webkit.opendarwin.org/">WebCore</a> site.</p>

<p>KDOM and KSVG2 are slated to be moved into the core of KDE 4.  With Apple including the technology into WebCore, this means that several Safari engineers will be now be working full-time with KDOM/KSVG2/KCanvas. Eric Seidel's KSVG2 contributions can already be found directly committed to the KDE Subversion repository. Given that Apple has recently improved the accessibility of WebCore development to KDE hackers by moving it to an <a href="http://tinyurl.com/bspue">open bug tracking</a> system and a <a href="http://webkit.opendarwin.org/building/checkout.html">publicly viewable CVS</a> server, this is good news for KHTML hackers.</p>

<p>KDE core developers Nicolas Zimmermann ("WildFox") and Rob Buis ("rwlbuis") have been <a href="http://svg.kde.org/introduction.php">working</a> rather silently, but hard on a new <a href="http://websvn.kde.org/trunk/kdenonbeta/kdom/">DOM implementation</a> for nearly 2 years. KDOM is intended to be much more extendable than the current DOM technology used by Konqueror (extending to <a href="http://en.wikipedia.org/wiki/MathML">MathML</a> comes to mind), and will make it easy to build in any future <a href="http://www.w3.org/">W3C</a> standard. A sketched out design document for interested developers can be found <a href="http://websvn.kde.org/trunk/kdenonbeta/kdom/docs/WHYKDOM.txt?&view=markup">in KDOM's repository</a>.</p>

<p>It looks like KDE 4 is already well on track to establish itself as the leading implementation and development platform for current and future web technologies.</p>









