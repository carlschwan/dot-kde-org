---
title: "Quickies: QtRuby, amaroK, Create, Ging"
date:    2005-11-05
authors:
  - "jriddell"
slug:    quickies-qtruby-amarok-create-ging
comments:
  - subject: "OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Is this true?\nhttp://www.eweek.com/article2/0,1895,1882118,00.asp\n\nNovell is making one large strategic change. The GNOME interface is going to become the default interface on both the SLES (SuSE Linux Enterprise Server) and Novell Linux Desktop line. KDE libraries will be supplied on both, but the bulk of Novell's interface moving forward will be on GNOME."
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "I expected this from day one. American nationalism."
    author: "reihal"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "I can assure you, most Americans have no idea what GNOME is. Or KDE for that matter.\n\nThis is more likely one of those \"business\" decisions where they decided that they can write closed source GTK applications without having to pay anything. Whether that business decision or not is good is another matter. We can always tell them, \"We were going to buy your suse product, but now that the default desktop is GNOME we think we'll try another distribution.\"\n\nP.S. Plenty of us love KDE. It wouldn't win in the Linux Journal polls otherwise."
    author: "J.M."
  - subject: "Complain on OpenSuse mailinglist!"
    date: 2005-11-05
    body: "We should get on the OpenSuse mailinglist and complain about Novell's abandonment of KDE. Tell them it's a bad business decision to drop a technically superior, well-integrated framework that has a greater, more energetic open-source developer and user community!\n\nArchives: http://lists.opensuse.org/archive/opensuse/ \nSubscribe: http://www.opensuse.org/Communicate"
    author: "Vlad C."
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-05
    body: "Yes. A fire Novell managers petition. "
    author: "Andre"
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-06
    body: "No. A sell SUSE to some sane and predictable people and you can keep playing the managers petition :)"
    author: "dott"
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-06
    body: "Yes, complain on the mailing list. I'm sure lots of angry, poorly spelt flames from the KDE faithful will really change their minds.\n\nHere's a clue for some of the more crazed inhabitants of this board: this change was going to happen from the day that Novell bought SUSE. There was no way in hell that Novell was going to support two desktop codebases. It would be insanity and a software engineering nightmare. Novell wanted SUSE for its Linux expertise (actually they wanted Red Hat, but they couldn't afford it), but SUSE had a legacy of KDE users... and they were not part of Novell's plans. KDE was quietly made a legacy product, while the main development went on with GNOME... then Novell's financials hit the skids. Now they can't afford to keep up the pretense of supporting KDE just to avoid the bad publicity that comes from a bunch of angry zealots running around calling them every name under the sun. So... boom... KDE gets officially turfed out.\n\nThat's it. Business decision. Convincing your zealot hordes to run off screeching onto the mailing list that \"Novel is eevil and gnome sux\" (and don't kid yourself, I've read the contents of dot.kde.org before... that's it's level) will do you no good. Try complaining to Trolltech instead. If you can get them to change their licensing, KDE might be able to try to merge with GNOME in future... and maybe survive (obviously this would require KDE to do a lot of clean up, work to higher coding standards, and pay more attention to security... as well as reduce some bloat... but nothing is impossible).\n"
    author: "Darg"
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-06
    body: "\"Try complaining to Trolltech instead. If you can get them to change their licensing...\"\n\nQT is licensed GPL. The GPL is the best Free Software license because it contains strong copyleft provisions that require derivative works to also be Free and Open Source. This is in the interest of users (like me) so I have absolutely no problems with QT or anything else licensed under the GPL. \n \nWhy exactly do you care about proprietary, closed source, non-free software anyway? So you can lock users in your product, prevent them from making modifications, restrict their rights, control what they can and can't do with their computers? Proprietary software is immoral, and if that's what you're developing, then you SHOULD be penalized by having to pay licensing fees."
    author: "Vlad C."
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-06
    body: "* Why exactly do you care about proprietary, closed source, non-free software anyway?\n\nI don't, but I'm not a zealot. I do realise that choice in license in important... KDE doesn't give you that choice, unless you pay TrollTech's tax. Even Richard Stallman realizes that for non-innovative things (such as widget toolkits like GTK and QT -- and this isn't a criticizm, a widget toolkit isn't \"innovative\" as such), the GPL is a liability, and LGPL exists to fill that gap.\n\n*Proprietary software is immoral, and if that's what you're developing, then you SHOULD be penalized by having to pay licensing fees.\n\nReally? Does that include things like GLIBC? And if you are so fired up about the GPL, why are the KDE libraries LGPL, rather than GPL? If the KDE libraries were GPL you wouldn't be able to write closed-source KDE apps at all... so don't get all high-and-mighty, zealot. Your beloved desktop's licensing is deliberately set up to ensure that in order to drop the GPL restrictions across the entire KDE code base and write closed-source apps, you pay a fee to TrollTech... what a bunch of filthy hypocrites you are. When the KDE project stands up for the principles that fools like you mouth off about, and relicenses its libraries under the GPL, and stops acting as a loss-leader for TrollTech and its commercial QT fee... *then* you can lecture others on the evils of \"proprietary software\".\n"
    author: "Darg"
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-07
    body: "What's wrong with Qt, what's wrong with Mysql?\n\nNothing: They both are GPL licensed. And there are two companies behind these two products that base a bussiness on FREE SOFTWARE. They took a HIGH risk freeing their code and that they did it and that they succeded should be highly acknowleged. So if you call a bussiness a tax you dislike making money: Remember: Free software is not about free beer.\n\nWell you claim that KDE has some internal code problems. Hm well KDE 4 is going to improve things as everytime with a major version numer change but Gnome is everytime doing exactly the same (they apparently try to overcome the library hell at the moment with project Ridley), so what's your point beside lame rant?\n\nWith respect to technology: I guess you never used KIO-Slaves (yes there exist Gnome-VFS, coded after KIO but it is not that universal usable). Ever used fish://<user@server> ? Or did you ever enter the following line into Konqueror: man:<program name> or even shorter #<programm name>. This also works for Info-pages: info:<foobar>.\n\nAnd Firefox (which is no Gnome project) now also has such nice Konqueor shortcuts like wp:<Article>, gg:<search word>, leo:<word I need to get translated>. By the way I like Firefox and to mention a small but brilliant thing: Its yellow background for https was really a good thing and the Konqueor folks liked it and adopted it in no time.\n\nHm and if I look at DBUS. This technology is coded after DCOP (there is nothing wrong with that and I hope that DBUS is better than DCOP and that it get's finally used by all Gnome apps).\n\nK3b is also not the worst CD-burning app in the world...\n\nNo question: Gimp is the best linux pixel graphics app and Inkscape is the same for vector graphics and I longtime used XMMS until amaroK replaced it. \n\nSo please forget your Desktop war. KDE always tried to be an integrative Desktop with integrating but not pocketing GTK/Gnome apps. KDE's agenda was always: Take the best free software technology regardless who wrote it and try to integrate it nicly into the desktop. Sadly at Gnome there were attempts to pocket Mozilla and OpenOffice and I hope this failure was something from the past and that Gnome will start doing the same as KDE and focus on an integrative Desktop as well. Freedesktop.org seems to be a good collaboration zone for such a task.\n\nAnd I'm sure companies selling proprietary products on Linux would like it that way. Let the companies decide if they want to use GTK or Qt for their product. Apparently there is a market for both.\n\nAnd wouldn't it be nice if they knew that their app integrates in all major Linux Desktops? This is needed for bussiness and not arbitary focus on one true and only Desktop as this focus will fail - there will always be an proprietary app using \"the wrong\" Toolkit/Desktop and thus a company behind which now looses interest in that distro. So for Novel it is much more wiser not to focus at one Desktop in order to maximize profit but to focus on making both desktops more integrative.\n\nHave fun."
    author: "Arnomane"
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-07
    body: "\"I do realise that choice in license in important\"\n\nYou do have a choice.\n\n\"Even Richard Stallman realizes that for non-innovative things (such as widget toolkits like GTK and QT -- and this isn't a criticizm, a widget toolkit isn't \"innovative\" as such), the GPL is a liability, and LGPL exists to fill that gap.\"\n\nhttp://www.gnu.org/licenses/why-not-lgpl.html\n\nThat essay by RMS contains (among others) this quote:\n\n\"Which license is best for a given library is a matter of strategy, and it depends on the details of the situation. At present, most GNU libraries are covered by the Library GPL, and that means we are using only one of these two strategies, neglecting the other. _So we are now seeking more libraries to release under the ordinary GPL._\"\n\nRMS seems to disagree with you.\n\n\"Your beloved desktop's licensing is deliberately set up to ensure that in order to drop the GPL restrictions across the entire KDE code base and write closed-source apps, you pay a fee to TrollTech\"\n\nI really see no problems with that. If anything, current scheme makes it more appealing to publish GPL'ed software, since you don't have to pay. Other alternative is to fund the developement of Qt (which benefits KDE and other projects, through improved Qt) and buy a license. But unfortunately you can't take advantage of other people's work and not give anything in return.\n\n\"what a bunch of filthy hypocrites you are\"\n\nNo, YOU are the hypocrite. You demand that you must be free to take the work of others, use it for free, and not give anything back to others. You want to earn money from Qt, but you want to deny Trolltech that same right. In my book, that reeks of hypocricy."
    author: "Janne"
  - subject: "Re: Complain on OpenSuse mailinglist!"
    date: 2005-11-07
    body: "\"Here's a clue for some of the more crazed inhabitants of this board\"\n\n\"Convincing your zealot hordes to run off screeching onto the mailing list that \"Novel is eevil and gnome sux\" (and don't kid yourself, I've read the contents of dot.kde.org before... that's it's level)\"\n\n\"obviously this would require KDE to do a lot of clean up, work to higher coding standards, and pay more attention to security... as well as reduce some bloat... but nothing is impossible\"\n\nWith all due respect: you sir are a troll and an a-hole. Above quotes prove it. If you are not a troll, then you are a mere ignorant moron who has no idea what he's talking about. take your pick."
    author: "Janne"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Just for the record, GNOME was started by a Mexican, not someone from the US."
    author: "Scott Wheeler"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "I couldn't care less.\n\nWhen Mandrake first appeared, I moved from Red Hat to Mandrake because they offered a better desktop (KDE). I still find Gnome clunky (like mose experienced Linux users). After reading all the SUSE hype, I wasted 4 days last week downloading 5 CDs of SUSE and all I could get was a command prompt without no sound card or network card configuration. It took 10 minutes to re-format my root hard drive and re-install my favourite Kanotix with the usual symbolic links from my home directory to the data files on my 2nd hard drive. It's light years ahead of SUSE. With Debian package management and KDE, the choice is a no-brainer.\n\nLots of other Linux companies have made even sillier choices."
    author: "Bob Buick"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Sounds true too me.\n\nA long expected move I guess - I never expected this big american company to realize the potential \"small\" SUSE and it's long standing KDE expertise could give them. The local GNOME advocates inside Novell cried loudest as usal and won the internal struggle (if at all there was any).\n\nI don't think when someone states that Novell is throwing away it's desktop products is largely overreacting - GNOME on the Enterprise desktop always seemed like a strange decesion - just being as 'simple' as possible doesn't usually cut it here.\n\nIf true - KDE lost it's last serious Enterprise distribution. That's it folks, we're now 'the alternative'."
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "It is sad. Gnome will have to move from C to C++, Java, Mono or something else in the coming years (I suggest sooner rather than later) and this will mean a very long, difficult transition period. \nKDE seems more ready for the future and Suse/Novell already had everything they needed with KDE."
    author: "p"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Has anyone noticed that they are laying off workers on Mono and Evolution while making GNOME the default desktop - a rather contradictory move, does anyone think?\n\nAnd the biggest problem facing SUSE at the moment is its font rendering problem.  Serif fonts look like crap in SUSE 10.  They are the worst I've seen on any Linux distro, ever.  I brought this up on OSNews and people referred me to the Freetype patents page.\n\nWhat's the patent situation in Europe, does anyone know?  Would it be possible to launch a fork of OpenSUSE in Europe which uses the bits Americans can't?  Or perhaps people could do what Packman does with the MP3's, and produce a free \"encumbered\" Freetype RPM."
    author: "Indigo Jo"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Well they just did it... I was thinking about trying suse again after a few years, but when they stick to GNOME, like debian, well then bye bye...\n\nEnough good distributions that use KDE as a standard, and not as an extra foreign citizen..."
    author: "Boemer"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "\" Enough good distributions that use KDE as a standard, and not as an extra foreign citizen...\"\n\nI'm not so sure. Actually I was pretty pleased with SUSE 10.0 and actually considered switching to this, but those plans got smashed this morning (why switch distributions for just one release). But now? There are a few 'nice and easy' distributions like Xandros (main apps aren't KDE) and Linspire (well...), but what else?\nTo me only three other distiburions come to mind: MEPIS, Kubuntu and Mandriva.\n\nMEPIS is just Debian (sometimes unstable, sometimes stable) - used it for a while, but not beeing a distribution of it's own had it's weaknesses.\n\nKubuntu is starting to alienate from a true KDE distribution - important settings and stuff are altered from what a trained KDE user might want or expect.\n\nMandriva - had bad experience with some of their previous releases - when they still were Mandrake and hat a non-year versioning scheme. Is this a good distribution now for someone expecting good and current KDE packages and stuff?"
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "While reading my own post two things sprang to my eye:\n\n1. MEPIS is Debian with some altering packages, no 'distribution' of it's own\n2. Kubuntu is the KDE version of Ubuntu a VERY popular GNOME distribution, so no matter how well Kubuntu does it will always be in the shadow of mighty Ubuntu.\n\nIf Mandriva decides to dump KDE tomorrow and go with the big guys - or better yet get's brought by yet another big american company (SUN anyone?) KDE will would really be gone from any kind of large surface distribution that might actually be considered inside a company (enterprise or not).\n\nPlease, someone tell me I'm really seeing this wrong - there's got to be something I miss here :-/"
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "There is another nice Desktop distro that focus on KDE: Ark-Linux\n\nActually I did never try it (as I was happy with my Suse) but it looks quite nice and is an active project.\n"
    author: "Arnomane"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "> Actually I was pretty pleased with SUSE 10.0 and actually considered switching to this, but those plans got smashed this morning (why switch distributions for just one release). But now?\n\nI don't understand. Why not use and support SUSE Linux which has and will contain KDE? Why make your decision dependent on what the business products default to?"
    author: "Anonymous"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "I'l try to explain with a quote from the article:\n\"The entire KDE graphical interface and product family will continue to be supported and delivered on OpenSuSE\"\n\nSo, NLD and SLES will be using GNOME and only the OSS version of SuSE will \"continue to support and deliver\" KDE. To me this means that there will be no more work done inside Novell to have a well integrated KDE environment 'on top' of SUSE, like it used to be.\nThe fact that KDE will be there just doesn't cut it imo. That's the same reason I don't use Fedora (ok, among others). "
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Who said that there will be only the OSS version of SUSE Linux in the future?"
    author: "Anonymous"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "The Novell representative did. But that's propably merely a misunderstanding -> OSS version in my context above means the 'community' release, which to some degree or another is the retail SUSE Linux package as well - oficially tailored at 'early adopters and enthusiasts'."
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "\"The Novell representative did.\"\n\nThe Novell representative is an ex-Ximian fanboy trying to make the cuts that have have happened on Mono, Hula and Gnome look better. These people have a long history of this. We'll need to wait some months to see what has happened if anything. Remember Ximian Desktop and Suse when Suse first got taken over?"
    author: "Segedunum"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "But isn't it correct that these Ximian guys are now in lead position at Novell's Linux business?\n\nAnyway the loss here isn't Novell per s\u00e9, but the last important commercial vendor that relied on KDE. IF there is another soon then there won't be much harm done. If not KDE will lose importance and exposure. IMO the main issue is as simple as that.\n\nThe reasoning behind Novell's move is another question though - like some already said it was predictable and I'm sure Qt licensing is part of the deal."
    author: "Christoph"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "\"But isn't it correct that these Ximian guys are now in lead position at Novell's Linux business?\"\n\nNo, but they like to think they are ;-).\n\n\"Anyway the loss here isn't Novell per s\u00e9, but the last important commercial vendor that relied on KDE.\"\n\nApart from the fact that this is unconfirmed (nothing has happened, no products have changed yet), made when the company is making huge job losses and when you simply can't make statements like that.\n\n\"like some already said it was predictable and I'm sure Qt licensing is part of the deal.\"\n\nAdd up the amount of money Novell spend on salaries and resources for people to hack on low-level stuff like GTK, Mono etc. and give it away fo free. Shock horror! Hacking on this stuff is not free for Novell! Now add up the cost of some Qt licenses (or nothing if they're writing GPL software) to do development on top of that same type of low-level development stuff to produce the same types of applications and functionality. The days of Novell pumping money into projects which they give away for free to people so they can develop everything for nothing are long gone, and at some stage the penny will drop.\n\nNot tell me what is cheaper at a time when Novell has to cut costs ;-). Money talks, Novell need cost effectiveness and KDE and Qt are as cost effective as it gets."
    author: "Segedunum"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Well, FIRE Novell managers. Leave Desktop strategy to SuSE."
    author: "Andre"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "<BLOCKQUOTE>... there will be no more work done inside Novell to have a well integrated KDE environment 'on top' of SUSE, like it used to be.\n\nThe fact that KDE will be there just doesn't cut it imo.</BLOCKQUOTE>\n\n\nYet at the same time you pan Kubuntu for doing just that. Make up your mind man. Besides, a seasoned KDE user needs to make his changes once and then it works like you expect.\n\nI have been using Kubuntu for a while now and love it!"
    author: "Jonathan Dietrich"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "But Kubuntu just moved up to be a fully supported citizen next to Ubuntu, as you can read on http://www.kubuntu.org/announcements/kde-commitment.php"
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Interesting news, thanks for pointing this out.\n\n\nJonathan, the reason I was a bit harsh with Kubuntu (telling it's alienating KDE users or whatever) is somewhat mixed in with personal disappointment of some sort. I actually fell in love with Kubuntu when hoary hit the streets (debian-like and great KDE commitment... yay) and was a bit annoyed by some of the changes done throughout breezy - simple changes only affecting me personally for 5 minutes until I change them.\nYet this kind of stuff means a lot to me since you will always end up working on 'standard installs', be it at the buddies PC you just installed kubuntu to or in the office - so the standard install is where a distribution has to shine and what people will know the distro for.\n\nAdd to that, that I just installed SUSE 10 (yeah, did it anyway) because of overall slow performance I have in kubuntu (harddrive related, never able to track it down) and we get some bad mood and a bad comment, no hard feelings ;)\n\nThis leads way to far here - let me just say that I'm really looking forward to the next Kubuntu releases and hope it will end up beeing the nice KDE-centric distro everybody wants."
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "I'm still not sure about Kubuntu.\n\nTake a look at this:\n\nhttps://launchpad.net/distros/ubuntu/+specs\n\nYou will see that there are tasks in Ubuntu that are deemed more important than the whole Kubuntu roadmap altogether\n\nIf these (probably Ximian originated) rumours prove to be true, then here is really no good KDE distro left. Which means a nice field for somebody wanting to start something new."
    author: "a"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "I think the team working on Kubuntu alone is still very small compared to those working on the Ubuntu core and the GNOME part. The more impressive is what was done with Kubuntu so far. And with this decision I'm sure Kubuntu will be allowed to expand."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "I think so as well. The small Kubuntu team has proven what it is capable of with the last two releases. They're definitely going their own way and would not halt to wait for Ubuntu when they had the ressources to surpass it.\n\nYet maybe they just don't have these ressources. All in all Kubuntu is a community distribution in the end. Afaik the main developer (and poster of the original news above btw) Jonathan Riddell is now employed by Canonical (at least for a year) to work full-time on Kubuntu. The announcement linked above shows some more good faith from Mr. Shuttleworth in Kubuntu, but still the ressources are way smaller and maybe not even as organized as those of Ubuntu...\n\nThis is my main gripe - it isn't true that there are no good KDE distributions left now - there are a few - but what happened now in my eyes is that KDE is not even a proper option for a company (enterprise or not) anymore since there are no 'big' commercial vendors offering KDE as their first choice.\n\nWell, I'll just try and live with that talking about the \"Linux Desktop\" will mean GNOME first in the future. Truth is I lost any enthusiasm advocating \"Linux\"... bah."
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "> Is this a good distribution now for someone expecting good and current \n> KDE packages and stuff?\n\nYeah, try PCLOS at http://www.pclinuxonline.com/pclos/index.html. Based on Mandriva, but a long way forked now, designed as a desktop that just works out of the box with Multimedia/Java/Flash/Everything.  New versions of KDE and apps within days of release, unlike Mandriva.  Kernel patched and tuned for reponsive desktop. Good sized repository.  Very active developement by Texstar and team.  New live-cd version due in the next few days.  Try it.\n\nJohn."
    author: "odysseus"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "He, now OpenSuSe will have be a KDE only dist.\n\nKDELinux 1.0\n"
    author: "reihal"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "\"KDE lost it's last serious Enterprise distribution.\"\n\nThe last time I checked, Mandriva Linux was using KDE as its default. \n\nI am now using KDE 3.4.2 on OpenSuSE 10.0 and I love it. If Novell indeed switches to Gnome, I am going back to Mandriva or will give Kubuntu a try. Is Novell going to tail chase Red Hat from now on?"
    author: "Jerzy Ostrowski"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "* The last time I checked, Mandriva Linux was using KDE as its default.\n\nPerhaps you misunderstood. He said \"serious Enterprise distribution.\""
    author: "Darg"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "lamest trolling I heard in a while"
    author: "ano"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "You're right, of course, why should anyone question the claim that Mandriva, the all-but bankrupt French distro that barely anyone uses (let alone corps), isn't a \"big enterprise distro\". How foolish of me. I humbly apologise."
    author: "Darg"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Well, it actually sounds lame, but the \"serious\" was really meant this way.\n\nI've never (ever) come across any Mandriva support in an area I've been working at that is more or less enterprise related.\nIf you have \"linux support\" with hardware or software concerning SAN, Content Management or e-commerce (can't comment on something else since these are the topics I've worked with) this is mostly Red Hat and SUSE. Sometimes there's Debian support.\n\nMaybe this is different with local companies in france, but that's my experience."
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "Mandriva is actually doing very well. It has a clear desktop and server strategy, competitive pricing and stable products.\n\nWhat else can anyone ask for? You have shown through this whole thread that you are nothing but a troll."
    author: "Gonzalo"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "GNOME? why?  Kde is best...Novell...arrgg!"
    author: "Joel Zerpa"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Okay, sounds true. So - what do we (KDE users) install and advocate now?"
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "SUSE Linux. And don't forget to tell your Novell sales man that you're using it instead of next Novell Linux Desktop because it includes KDE. If customers give this feedback Novell management may reconsider the single desktop strategy very quickly."
    author: "Anonymous"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Umm; 95% of their existing customers are using KDE and prefer it. Not listening\nto that customer base is relatively bad mistake. And just because some high\nlevel manager has emotional ties with the G. Nice."
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Kubuntu.  Or Debian.  I don't know where one of the other posters got the idea that Debian is GNOME-centric.  It develops a few utilities primarily for GNOME, but supports KDE *fully*, and often more quickly than GNOME.\n"
    author: "Lee"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "* Okay, sounds true. So - what do we (KDE users) install and advocate now?\n\nIf you want a long-term future, I'd suggest installing and advocating GNOME."
    author: "Darg"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "> If you want a long-term future, I'd suggest installing and advocating GNOME.\n\nWhy? I'd rather go back to windows."
    author: "G\u00f6ran"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "You know what? Me too, and that's what makes me so upset about this whole thing :("
    author: "Christoph"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "Indeed, so much for the whole \"Qt is *more* free than GNOME because it uses the GPL\"... where's your freedom now, Windows-boy?\n\nYou know... I don't just mouth the word \"freedom\". Given a choice between Windows and KDE, I'd choose KDE... but then I'm not a crazed bubbled-headed zealot who has lost all sense of proportion."
    author: "Darg"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "Pretty aggressive tone isn't it?\n\nMaybe you should try differentiating people for what they are - persons, not trying to group all and everything together. It's not \"Darg against the KDE-zealots\" here, who in your mind obviously all say the same.\n\nFor the record. I (that's me, the person) never ever said Qt would be more free than anything else because it's GPL. Honestly I think KDE would be better of with a less strictly licensed Qt - but I know and accept the opinion of those opposing this for various reasons.\n\n\nMy personal choice to use KDE is based on the first class software it is - it always served me well. If KDE fails for whatever reason, I would first go to Windows which is a platform I know rather well even though I don't like working with it much. Yet people have to do their work, right? KDE makes this pretty easy for me - almost a pleasure. Windows allows me to do things in different, more indirect ways, but in the end you get there. With GNOME I miss various features I have in KDE easily and via some thrid party app in windows.\nActually GNOME get's in my way too often due to beeing over-simplified. Things are missing and you have to go hunting to get them back.\n\nSo, why would I even consider using GNOME? Just because it's free (beer or freedom, whatever) ? No. Thanks."
    author: "Christoph"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "Who the fuck are you, lamer? This whole thread is full of your bullshit. Fuck off."
    author: "Darg's an idiot"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "* Who the fuck are you, lamer? This whole thread is full of your bullshit. Fuck off.\n\nOooh... temper temper little boy. I realise that this is a difficult moment for you KDE hotheads, but you must see the truth here. KDE is now, at best, a second division desktop with an extremely limited future. You aren't going to take over the world... your desktop is destined to be the plaything of the few zealots who can be bothered to install it.\n\nIf you want someone to blame, then blame the fools who took the initial decision to based KDE on the closed-source Qt (read that very carefully before replying. Qt *was* closed-source when KDE started... and even now suffers from licensing problems being a library using the full GPL). It set the course for the entire fiasco that's followed... KDE developers have wasted hundreds of man-years writing code for a desktop that's going nowhere, and all because you didn't pay attention to licensing at the beginning. It's tragic really, but also strangely hilarious.\n\nIf nothing else, KDE's pitiful downfall should serve as an important lesson for other projects. DON'T DO WHAT THESE HALFWITS DID. MAKE SURE YOU LISTEN TO THE LICENSING EXPERTS.\n\nIt would also be nice if all the little KDE attack squad goons who have thrown so much abuse around over the years would publicly apologise to Richard Stallman, Bruce Perens, Red Hat, Sun Microsystems... who they you have repeatedly abused, slandered and lied about for pointing out the obvious definiencies in the arrangement between KDE and its puppeteer TrollTech. \n"
    author: "Darg"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "...on the closed-source Qt (read that very carefully before replying. Qt *was* closed-source when KDE started..\n\nAnd if nothing else labels you as a clueless moron, that statement did. Qt was never closed-source. Don't matter how many times or carefully you read it, it's plain fact. Those earlier versions are still obtainable so you can download and compile them yourself, no closed source there. The earlier Qt was on the other hand licensed under a license not compatible with the GPL, but that in no way makes it closed source. But that is clearly to hard for your small brain to comprehend."
    author: "Morty"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "You are wrong... do some reading you bloody fool. The original Qt *WAS CLOSED SOURCE* and KDE was in direct and blatant violation of the GPL by using it. The fact that the code for early versions was retroactively released (and can now be downloaded) does not change that -- you cannot airbrush history so easily. The later versions were released under an Open Source license that was not GPL compatible... and then finally, dual-licensed under the GPL to solve the incompatibility problem... but still leaving the \"must pay Trolltech\" problem.\n\nYou'd think a dump like this, full of brazen swaggering advocates, would know the basics... but obviously not."
    author: "Darg"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "As if something delivered with sources can be closed source. As if developers can violate their own code."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Don't forget to also quote: \"The entire KDE graphical interface and product family will continue to be supported and delivered on OpenSuSE\". The OpenSUSE project creates SUSE Linux which is the base for the business products above so the KDE packages will work also on SLES and NLD."
    author: "Anonymous"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "True, but like somebody said KDE is now a foreign citizen on SUSE. Why would anyone go through the hassle of trying to get their KDE packages together on SUSE when (if) there's something that comes with KDE default?"
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "opensuse is (and i guess will be) KDE based. so Gnome is still foreign there... but the corporate editions include gnome as default. sounds like not really efficient from a novell point of view, but hey, the community prefers KDE, the company's gnome..."
    author: "superstoned"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "and i wonder if KDE 4 will change their attitude... i don't really understand why the choose gnome, as it doesn't really have any advantages (except maybe that its slightly more usable, but this is offset by the fact most buisiness users are used to windows, and KDE is more windows-user-friendly) and KDE has a way better framework to build applications upon. it might be licensing, but then again, i still considder Qt an advantage. guess its just gnome trolls that convinced them. the most imporant reason gnome is used at all is they make lots of noize, while knowing KDE kicks their ass on most parts of the desktop.\n\nwell, IF kde 4 will be so much faster and better as i expect it to be, the difference between the archaic gnome and the much more advanced Mac OS X, Windows Longhorn and KDE 4(which may very well be the best of these 3) will be staggering..."
    author: "superstoned"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "> opensuse is (and i guess will be) KDE based\n\nNot really. Enterprise editions show the way."
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "IMHO the only sane way SUSE users can have a vote on this is to vote with their\nwallets and feet. Step away from SUSE until the issue has been resolved."
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Agreed.\nThe idea that KDE is still around on SUSE and could be used by anyone who likes it more would not help in showing Novell that customers want KDE in any way."
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "This is a sad day and a big step backwards for the linux desktop - officially spoken. GNOME as desktop is nowhere close to where KDE is these days. It's no secred and we all know that GNOME is basicly nothing else than a huge mess, architectual wise as well as usability wise. Stuffed full with different languages GNOME matures into an unmaintainable big mess without any future scalability. GNOME - since the 2.x transition kept stuck in stagnation and people keept patching around in it in various areas but there is still no real progress. People came up with different other alternative solutions and programminglanguages in hope to improve softwaredevelopment. Unfortunately GNOME is now stuck and relies on many developmentlanguages and they all run in the background in case you run a ruby, python, java, mono GNOME application. Not just that, but also the majority of GNOME applications are nothing more than halfbacked slammed together crashing tools that - in no ways - could stand competition software as found on Windows or OSX (with just a handful applications as exception). And yes as the other readers and commenters noted. I think the move to GNOME was long seen before and of course pushed by the GNOME zealots who cried loud. I think we keep hearing corporates continue to cry that \"Linux is not ready for the Desktop\" no wonder if they get GNOME as default installed. A corporate Desktop where the corporates cry out \"It's not ready\" - yet to hear for the upcoming 10 years. GNOME doesn't work, trivial simple tasks can't be acomplished. Printing doesn't work reliable, viewing documents doesn't work reliable, evolution and other programs permanently crashes or trash important data, no productive tools to get any work done in the science area or the computer science area. The entire Desktop feels like slammed together in a hurry and as repeat again, not even trivial tasks can be done reliable enough. I do feel sorry for the entire Open Source world."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "\"Unfortunately GNOME is now stuck and relies on many developmentlanguages and they all run in the background in case you run a ruby, python, java, mono GNOME application.\"\n\nOh, come on, drop the language supremacy FUD! Whilst it makes sense to keep dependencies moderately sane (and to avoid certain redistribution issues with things like Java), running Python programs (for example) just entails having slightly more libraries on the system, and they don't all \"run in the background in case you run [such an application]\". In fact, given the shower of odd libraries stated as dependencies for various C/C++ programs, I'd argue that by choosing something like Python (or even Java) you often get a more sane dependency foundation because the application developers just used the standard library for most of the functionality rather than using libarbitraryrandomlib234, last touched for maintenance in 1997, because they haven't learned something better.\n\nGiven the continued segfaulting of applications in the otherwise excellent Kubuntu distribution, I'd argue that more applications need to be done in Python, not fewer."
    author: "The Badger"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "The continous segfaulting on Kubuntu is Kubuntu specific, I hava KDE here on SUSE 10 and it's quite excellent\n\nI'm terribly sorry to hear about Novell perverting SUSE, but then, it was already obvious that they know squat about SUSE and what to do with it.\n\nUnfortunately, this leaves me/others with no good KDE distro at all."
    author: "a"
  - subject: "KDE should find a BIG way into College Curricula"
    date: 2005-11-12
    body: "As I recall, Microsoft prospered in part due to the flow of undergraduates trained on MS Visual Studio and familar with the Windows API who influenced technologies used in developing systems.  As a college professor, I've encouraged students to develop KDE applications given KDE's architecture, OO foundation and development tools, but could not imagine supporting GNOME development as strongly.  That a diminishment in the importance of KDE potentially lessens the pool of students trained in Linux technologies (if others think as I have) raises the loss for Linux and Open Source as well.  I'd suggest the KDE community find some way to make a BIG push into college curricula, perhaps through developing sound academic teaching materials and tools, or finding more ways to collaborate with faculty.  I think there's a window of opportunity now... Java's suitability is being questioned more often among professors as a teaching language.  Swing is unpopular due to its complexity.  CS departments need to become more attractive and more responsive to real-world needs in order to succeed- languages, software engineering, etc."
    author: "MAFAnon"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "As a purchaser/user since 6.1 I'll wait and see whether SUSE stop supporting KDE on the ground.  They've always updated the RPMs and added stuff. IMHO, quicker than when SUSE was independent, (they RPM WINE quickly too). When that stops, well I'll just have to:\na) agree with the foregoing\nb) learn how to compile the bl...y stuff myself\nc) donate directly to KDE.  \n\nAt the moment it seems that Novell are still supporting KDE developers, OpenSUSE has put out and SUPER looks interesting (I'm trying to work out if I'm skilled enough to get on with that one).  Let's just watch for a while. \n\nEven though they've taken a business decision to support GNOME, it's hardly up there with supporting the BSOD, lock-in, forced upgrades, spyware, anti-spyware. They've decided this is how they get their bread on their tables. And if we get Linux or the other *nixes on the corporate desktop then all our lives get easier anyway. \n\n\n\n\n\n\n\n\n"
    author: "gerry"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "> At the moment it seems that Novell are still supporting KDE developers,\n> OpenSUSE has put out and SUPER looks interesting (I'm trying to work out if\n> I'm skilled enough to get on with that one). Let's just watch for a while. \n\nHey, Andreas J\u00e4ger already confirmed the move. This time it's for real. Time\nto unsubscribe from the lists, clear the bugs and install something different.\nSad, sad day for OSS. I, too, had bought SUSE for 7 years now.."
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Looks like a battle of the SuSE employees then, Marcus Meissner denied it."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "And Jeremy Allison say yes and no (while being unspecific about what to refer to)."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "AFAIK Marcus didn't really deny it, he was just a tad more vague with he's comments. Like politician, really."
    author: "Anonymous Coward"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Hm, looks like those who say the fears are \"unfounded\" keep indirectly referring to OpenSUSE, knowingly omitting that apparently Novell doesn't plan to ship any more boxed customer Linux versions with KDE. So you may be right. This would really be a hard blow for all the distribution tradition SuSE stands for up to now."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "The problem seems to be: Does Novell know its customers? Who runs this company?"
    author: "Andre"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "Let's face it: SuSE is a KDE desktop and customers will not accept Gnome.\n\nMaybe ximian's fud machine is able to use novell as its plattform but certainly business shall follow a market/demand driven approach."
    author: "Gerd"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "> Let's face it: SuSE is a KDE desktop and customers will not accept Gnome.\n \nAgreed, I just don't get it. Since when has it been a good business practice to kick your own customers in the nuts? Or have these folks lost a last bit of their sanity?"
    author: "Anonymous"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "I don't know if this is true or not, but I already had a bad feeling about the future of SUSE when Richard Seibt, the former Suse president left Novell and when they announced OpenSUSE. I guess by now Novell's Linux strategy is pretty much controlled by former Ximian people.\n\nI am a long time Debian User, but I downloaded and installed Suse10 last week and I have to say it is a pretty nice distro. Some people once started Yast4Debian. Did this project proceed? Kubuntu with Yast would probably make a very nice desktop distribution."
    author: "Michael Thaler"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "No, it's not. The comments in that article were made by an ex-Ximian Marketing guy, and he was never actually quoted as saying they were moving in that direction. He allowed the author to imply it. It's a well worked tactic employed by these people in the past, most around the time of the Suse acquisition."
    author: "Segedunum"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "This thread scared me, but I sure hope you're right.\n\nAfter all, the Ximian people have already tried to play this kind of cheap tricks in the past, and maybe now they think that thay might influence a slightly modified management by launching rumours.\n\nWe all know this: \"Yes, Your Majesty. You already said so.\" :)"
    author: "a"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "There's an article around the time of the Suse acquisition exactly like this one that said Ximian Desktop would be tightly stitched with Suse. Where is Ximian Desktop now?"
    author: "Segedunum"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "In a way it's the default desktop for SLES and NLD in the future.\n\nThe product \"Ximian Desktop\" was merely a customized GNOME desktop. When Ximian got acquired by Novell there was no point in having Ximian Desktop AND GNOME, so basically all the sensible changes from Ximian would have flowed into the default GNOME desktop of Novell.\n\nThe easy answer of course is: Ximian Desktop now IS SuSE*....\n\n*misspelled intentionally to remind of the good old days"
    author: "Christoph Wiesen"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "\"In a way it's the default desktop for SLES and NLD in the future.\"\n\nROTFL. No it isn't. It certainly isn't for SLES, and it isn't even for the NLD.\n\n\"The product \"Ximian Desktop\" was merely a customized GNOME desktop. When Ximian got acquired by Novell there was no point in having Ximian Desktop AND GNOME, so basically all the sensible changes from Ximian would have flowed into the default GNOME desktop of Novell.\"\n\nYou're dodging the issue. There's no real Novell build of Gnome as there was with Ximian Desktop."
    author: "Segedunum"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "He is not dodging any issue. NLD is exactly what Ximian Desktop was before, only that it now comes bundled with an operating system. It is and always was a polished distribution of GNOME with a few enhancements."
    author: "Daniel"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "\"NLD is exactly what Ximian Desktop was before, only that it now comes bundled with an operating system. It is and always was a polished distribution of GNOME with a few enhancements.\"\n\nThe NLD is not a Gnome distribution, although it is one of two desktops available to you in it."
    author: "Segedunum"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "GNOME distribution as in \"a distribution of the GNOME desktop software\" not as in \"a Linux distribution centered on GNOME\". Their Ximian stuff was simply merged into NLD (alongside the entire SuSE OS with KDE), and that's exactly what they had planned."
    author: "Daniel Borgmann"
  - subject: "KDE is too \"colorful\" for entreprise?"
    date: 2005-11-05
    body: "I won't defend too strongly this opinion but could the fact that KDE doesn't look like an entreprise desktop be detrimental? \n\nCOLORFUL\nI tried a while ago to make my KDE desktop less colorful and lo, it was a real pain in the ass (one point is i couln't find a fairly decent, complete, tone neutral icon set).  Don't get me wrong I love color but it might be hurtful for the eyes of the normal management person. Point me to screenshots if i'm wrong.  If you look at the gnome desktop, it look much more subdued, more neutral wich might a good thing if you want your people to work. After all you *as a manager* want your people to work, not to look in awe at the art on their screen (I know I do). \n\nMake a comparison:\nhttp://wiki.kde.org/tiki-index.php?page=Colors\nhttp://developer.gnome.org/projects/gup/hig/2.0/design.html#Palette\n\nSIMPLICITY\nFor entreprise simple, less gizmo is better. A personnal experience is the less i can touch at the settings of my desktop, the more i am productive. (Yes kiosk is a good thing and I haven't tried it). It might be a nice addition to have *profiles*. Think about demoing to your boss gnome and kde. Wich desktop environnement would take more work to make it acceptable? \n\nThis whole set of thinking was based \n\n1) on the comment my friend made on the fact that IBM laptops look ugly (ie: black). It could be suggested that people equals \"ugliness\" with reliability (it's doesn't look attrative so it's surely reliable). \n\n2) The other one is that simple is better *sometimes* depending on your goals. Users want beautiful so kde is beautiful. Entreprise wants simplicity, not too many options that you can lose your time on, (how much putting your taskbar at a x pixels height improve your productivity really?) professionnaly looking (yes boring). \n\n3) The last is a personnal experience. I *want* to be in control of every aspect of my desktop. Entreprise doesn't want that.\n\nI think despite my preferences that gnome has answered the needs of the entreprise better on those aspects (look, simplicity) that kde has. It stands to reason that it can be fixed. KDE4 will be great of course but it might useful to integrate a way of toning down (a really complete theme and kiosk profile) the KDE desktop. That is if we want KDE to please entreprises.\n\nPS: As for the political aspects, as a SuSE user, I am sorry to see their active support for KDE go (will it change something for KDE developper wise?)\n\n\n\n"
    author: "Vincent"
  - subject: "Re: KDE is too \"colorful\" for entreprise?"
    date: 2005-11-05
    body: "Yes, \"business\" want boring colour schemes...\n\nWe will have crosstheming soon.\n\nThe default colour theme can be changed anytime and will get changed."
    author: "Andre"
  - subject: "Re: KDE is too \"colorful\" for entreprise?"
    date: 2005-11-06
    body: "What a load of absolute fucking shite."
    author: "Segedunum"
  - subject: "Re: KDE is too \"colorful\" for entreprise?"
    date: 2005-11-06
    body: "It is perfectly doable to give KDE a corporate/boring face and to lock it down. This is considerably less expensive than investing in other technologies. So no, this is not the explanation.\n\nThe fact that Trolltech is not for sale might be an explanation, though. As a matter of fact, I keep thinking, how can you create SuSE and sell it to strangers? Not that I'm at all against business, but how can you sell your business? This seams mean."
    author: "dott"
  - subject: "Re: KDE is too \"colorful\" for entreprise?"
    date: 2005-11-07
    body: "Sure, too colorful is not \"business like\".\n\nBecause if you look at Windows XP and Vista, all you see is brown, right?  Like in Gnome.\n\nMaking a fugly desktop will not bring success in business.\n"
    author: "Silviu Marin-Caea"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-05
    body: "What makes me wonder is why a bunch of loosers keep flooding the SUSE forums with \"KDE's non-GPL licencing model' stuff. Also nobody is correcting them.\n\nI always suspected that the SUSE userbase is infested with ignorant people, by where are the rest of the people?"
    author: "dott"
  - subject: "YaST going Gnome?"
    date: 2005-11-05
    body: "I just installed OpenSuSE 10 and found that the button order was swapped around \"Gnome style\" on a few YaST screens (mistake?  sign of things to come?).  The fact that YaST looked \"native\" on my desktop was one of the joys of SuSE.  The fact that SuSE will keep KDE as an option is pretty much \"too little, too late\" if they screw up YaST.\n\nAny suggestions for new distros?"
    author: "ac"
  - subject: "Re: YaST going Gnome?"
    date: 2005-11-05
    body: "You can switch to Mandriva (formerly Mandrake) Linux."
    author: "Anonymous Coward"
  - subject: "Re: YaST going Gnome?"
    date: 2005-11-05
    body: "Mandriva Control Center is GTK, and it definitely has the buttons in the wrong order, so no, the guy cannot switch to Mandriva.\n\nAs for Yast, I think it has some strange buttons placings because it's a wizzard style gui, so 'Next' is supposed to be on the right."
    author: "dott"
  - subject: "Re: YaST going Gnome?"
    date: 2005-11-06
    body: "No, I could have sworn it was a \"Cancel/OK\" abomination in YaST.\n\nI'll double-check to make sure it wasn't my imagination, but admin tools that don't look alien on your desktop are a big plus in my book.  All suggestions welcome--sorry to hear about Mandriva.\n"
    author: "ac"
  - subject: "List of possible alternative distros:"
    date: 2005-11-07
    body: "1. Kubuntu (Aka - Your first dive into linux)\npro: Debian-based, yet all bells and you know. Free, so you loose nothing if you make a mistake.\ncon: seems to move in the \"separate but equal\" to Gnome status.(Used to be in \"foster child\" category)\n\n2. Mandriva (Aka - RedHat generation is welcome here)\npro: KDE is da thing. Long history. Lots of own content. (Crystal Icon theme was initially theirs)\ncon: Too much glue on the system level. It's (at least was) heavily customized on system level. Some generic stuff doesn't compile ocasionally.\nThere is a review of it on osnews.com\n\n3. Slackware (Aka - Why Gentoo when you have Slackware?)\npro: Everything is stock. Everything compiles on this thing. > as soon as KDE comes out, chances are there are Slackware binaries already on KDE ftp. It is often more stable/faster than a comparable Suse version.\ncon: Lots of rough edges, like: laptop unfriendliness, lack of Hal, BSD style init.\n\nI personally use Slackware. It feels exactly right for KDE spirit. It is extendable and tweakable to extreme. Package management is available, yet dependancy checking absence is a big releif as opposed to continuous rpm hell."
    author: "Daniel"
  - subject: "Re: List of possible alternative distros:"
    date: 2005-11-07
    body: "\" 3. Slackware (Aka - Why Gentoo when you have Slackware?)\n pro: Everything is stock. Everything compiles on this thing. > as soon as KDE comes out, chances are there are Slackware binaries already on KDE ftp. It is often more stable/faster than a comparable Suse version.\n con: Lots of rough edges, like: laptop unfriendliness, lack of Hal, BSD style init.\"\n\nBSD style init is a big plus. It's not mess like SysV."
    author: "accc"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "To be frank, this type of move just made loath Novell to the extreme. Heck, even many parts of the 'OSS community' are really rotten to the core.."
    author: "Foo Bar"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "OK, I'm trying to understand what's going on.\n\nIf everything means what it seems to mean, then the following hold:\n\n1. Novell dropped one KDE developer (which is sad) and many Ximian developers (which is sad from a social POV, but on the other hand it makes me rejoice at the thought of the world being bothered by one less silly diversion). Gnome Planet says that former Ximian employees are being offered new jobs at VMWare (?!).\n\n2. Evolution is dead. The team has been disbandled.\n\n3. It is not at all clear whether Hula has been dropped or not.\n\n4. Cuts have been made in Mono, but it seems to be still alive. That would be politically sad to hear, because Mono is the silliest diversion of them all (a patent encumbered technically inept clone of a clone of java which always claims to be the future of the 'linux desktop'- which is how ximian megalomaniacs call gnome)\n\n5. Gnome is the default on NLD, but there's nothing new in that. On the other hand, it's not clear whether there will still be an NLD left.\n\n6. It seems that Gnome will be the default on SLES, but nothing has been actually said about this. A journalist (S. Vaughan-Nichols) thinks so, but that doesn't really matter\n\n7. Up to now nothing really to worry about (except perhaps point 6). The real problem seems to be that\n\n- the real SUSE (not NLD, which is just a ximianesque exercise in futility) might get reduced to a 'community edition'. Don't read 'community effort', which is good. Read 'crippled, beta quality crap, just as fedora or mandriva community edition, which the community losers test, debug and improve so that we (the smart ones at novell) can get richer'. We could no longer recommend SUSE to non-geek friends, and there would be nothing left to point them to\n\n- if the previous holds, then Novell will wisely invest it's money in something more profitable than KDE support, which will be left to volunteers. Of course, wise and profitable are to be read with a slight touch of 'mamma, you bought me this toy but you never taught me how to play with it so i broke it'\n\n8. If 7 holds, then there are two ways out:\n\n- (the compromise) Kubuntu\n\n- the good way out. A new distro to finally provide something good. Who might be interested in doing that? I don't know. Trolltech doesn't seem to, and I'd only trust a company with something if there's a profit to be made (of course, there's a profit to be made with SuSE too, but one needs to know how)\n\nThe bottomline: just wait for more news :) And never trust Ximian propaganda.\n"
    author: "dott"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "After scratching my head I came up with the right solution :)\n\nDon't petition Novell, because they have every right in the world to ignore you and they will. Also don't quit openSUSE, because it's good.\n\nBut if you're German, petition your government to expropriate Novell in the public interest and turn SuSE Linux into a foundation. Even on a tight budget, this is perfectly doable, and the foundation would more than certainly be eligible for European funding (not that I have something against American people using and supporting it, quite the contrary holds)."
    author: "ac"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "Ouch!"
    author: "cm"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-06
    body: "Novell's move is sad but as I read announcements not very bad for KDE.\n\nAs I read announcements looks like Novell is withdrawing from Linux on\ndesktop. Bad news for Munich and Linux on desktop generally but for well\nmanaged project like KDE not tragic. You say NLD. Did you hear about\nlarge adoptions of it in real life? I predict it will follow Sun's idea\nin 2 years (JDS - silent death). It will make one another hyped project\ndead with GNOME as default. Maybe then someone will realize that most of\ncommercially viable desktop oriented Linux distributions are using KDE\nas default (Xandros, Linspire, Mandriva).\n"
    author: "m."
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "Erm, Munich is not affected, Novell had lost that bidding: \nhttp://news.com.com/Debian+wins+Munich+Linux+deal/2100-7344_3-5689003.html\n\n"
    author: "cm"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "Well, that means they went to Gnome, too... Debian is a Gnome-centric distro."
    author: "AnonyMouse"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "Debian is not centric-anything. There are quite a few GTK2 (not even GNOME) applications for administration (synaptic beeing the most important), but that's due to many Debian developers *using* GNOME. Still you don't have any preferrence at all in a freshly installed Debian system.\n\nAs far as I can tell Munich will be using KDE, just like Vienna (Wienux*) and the BSI* distribution do - two different systems for public and governmental use.\n\nSo, choosing Debian doesn't automatically give you GNOME in any way. In german-speaking parts of europe you'd probably need a pretty strong attraction towards GNOME first.\n\n\n*Links:\nhttp://www.wien.gv.at/ma14/wienux.html\nhttp://www.bsi.de/produkte/erposs3/"
    author: "Christoph"
  - subject: "Re: OT: SUSE going Gnome"
    date: 2005-11-07
    body: "nope, they run KDE. (munchen, that is. and Debian is DE-agnostic, altough the KDE release in it is quite vanilla. maybe the kubuntu changes find their way back into debian?!?)"
    author: "superstoned"
  - subject: "Debian!"
    date: 2005-11-07
    body: "I'd go much farther. To anyone who is afraid that there will be a lack of good distros for KDE:\n\nDebian is a good KDE distro *because* they release a vanilla, non-mangled, KDE."
    author: "ac"
  - subject: "Re: Debian!"
    date: 2005-11-07
    body: "ACK, I've been using that very version for more than a year now.  No major complaints so far.  Even Sarge got released at some point along the road.  ;-)\n\n\n"
    author: "cm"
  - subject: "Re: Debian!"
    date: 2005-11-07
    body: "well, i prefer suse's enhanced KDE release, but i guess that's also depending on taste... (yeah, ive used vanilla kde, and still use it (gentoo) but i really love what suse did to KDE)."
    author: "superstoned"
  - subject: "I think Gnome people will be very sorry soon."
    date: 2005-11-07
    body: "I think Gnome people will be very sorry if they win this fight. Because, then, they will loose the war. \"Ugh?!\" you say; read on...\n\nKDE is KDE not because we are uneducated about \"the better ways\", but because we fundamentally don't give a damn about \"the better ways.\" There are so many of us; there is NO single better way possible. KDE is an elastic building, you can shape it any way you want, and build on top effortlessly.\n\nYou can kill the toolkit by pigeonholing it, but you can't keep the massess from wanting a platform that is as elastic and free-willing as itself. If KDE will be shunned into a hole because of Qt's lisencing liabilities we may move over to GTK territory. As I said before, when our forefathers chose Qt, \"they didn't give a damn\"(c) about lisencing. All they cared about is having a good base to build on. If Qt really becomes a liability for us when it comes to swindling money from the major distros, we will move over to your lawn.\n\nWe will tramp your flowers, step on your bare fingers with our heavy boots and spit on your spacial browser from a konquering height. We will forcefully industrialize you, and make GTK dance C++. \n\nThe final piece will be the sweetest - the gang of feudal war-lords (you know how you are) will find themselves without your ideologically-brainwashed goons. \n\nIf you sink our island, we will move over to yours and, then, there will be no Shire no more....\n\nAt that point Miguel de Icaza woke up and realized that it was only a nighmare... or was it?! Tam tam Tam Paaaaam!"
    author: "Daniel"
  - subject: "Re: I think Gnome people will be very sorry soon."
    date: 2005-11-07
    body: "Actually, everyone would be happy if you do that. No nightmare at all. :) Most of the free software community is already mixing and matching and not bothering with this \"us vs. them\" attitude. I do not think that it will ever happen though, because it would be a mighty bunch of work for no obvious technical advantage."
    author: "Daniel Borgmann"
  - subject: "Re: I think Gnome people will be very sorry soon."
    date: 2005-11-07
    body: "na, i think he is right. gnome is quite closed, you don't seem to get into it easilly. so it is likely, IF kde would have to be abandoned over-night (very very unlikely, but hey, for the sake of argument) they'll fork gtk, rebuild it into qt-something and adapt KDE to run on it... will kill gnome, i'm sure, if they succeed."
    author: "superstoned"
  - subject: "Re: I think Gnome people will be very sorry soon."
    date: 2005-11-07
    body: "That's not necessary because none of thbis is true ;-)."
    author: "Segedunum"
  - subject: "Re: I think Gnome people will be very sorry soon."
    date: 2005-11-07
    body: "If you have the skills, you get in easily. But more importantly, I was talking about individual software projects mixing and matching. If the KDE project would decide to fork Gtk for example and if this fork would be successful, then nobody would say \"ooh that comes from the evil KDE project, let's not touch it\". It would be a good thing! And if it would be too different to mix and match, then it would still be a good thing because it would provide choice. Just like XFCE is a choice for people for whom GNOME is too heavy and nobody is bitter about that (any such feelings are usually made up from the outside). Some \"GNOME developers\" are actually running XFCE theirself. Or mixing XFCE with Nautilus. Or GNOME with XFWM. Whatever works for them.\n\nBut then again it won't happen, so this is unfortunately just theoretical talk. ;)"
    author: "Daniel Borgmann"
  - subject: "\"amaroK started their fundraising drive.\""
    date: 2005-11-05
    body: "On their website it states that the fundraising is over since 4 days and they hvae no reached their target. I think they made a mistake by not making more \"advertising\" for it like the late announcement in this news. I was not aware the'y were looking for donations until today."
    author: "Morreale Jean Roc"
  - subject: "Re: \"amaroK started their fundraising drive.\""
    date: 2005-11-05
    body: "Yes, that was bad timing. Anyway, we are going to extend the fund drive for another two weeks or so, until we have reached the goal.\n\nSo your contributions are still welcome :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: \"amaroK started their fundraising drive.\""
    date: 2005-11-05
    body: "Already done and I'm forcing others to do so ;)"
    author: "Morreale Jean Roc"
  - subject: "Re: \"amaroK started their fundraising drive.\""
    date: 2005-11-06
    body: "can you tell who is getting how much money for what ?"
    author: "ch"
  - subject: "Re: \"amaroK started their fundraising drive.\""
    date: 2005-11-06
    body: "I'm citing from the amaroK website:\n\n\"These costs include server hosting and bandwidth, development hardware and also developer meetings. So we are starting a fundraising campaign: our aim is to collect 5,000 US $ by 1st November 2005\"\n"
    author: "dott"
  - subject: "Re: \"amaroK started their fundraising drive.\""
    date: 2005-11-06
    body: "yes sure but $5k , does sound like some random number like : hmmmm wie need xxxx money so just make some more like the next big number.....\n\n"
    author: "ch"
  - subject: "Re: \"amaroK started their fundraising drive.\""
    date: 2005-11-09
    body: "Well, it is for website hosting and bandwidth, some development hardware, some travel expenses to bring some of the developers together for a coding session and project meeting, etc. and it is actually far less than the real expenses because anybody doing any of the traveling will also will be dipping into their own pockets, not to mention many of us expend significant personal funds to pay for development related stuff, like hardware, bandwidth, etc.\n\nSo yes, it is kind of a round number because it is all we felt we could really ask the community to contribute even though we'd really like twice that.  So don't worry, we won't be buying too much beer with your money."
    author: "oggb4mp3"
  - subject: "Novell petition"
    date: 2005-11-05
    body: "Everybody was sceptical when Novell took over SuSE. It was expected that Novell would ruin this wonderful company and it turns out to be well-founded when you look at these stupid business strategies. I say.\n\n1. SuSE knows its market. SuSe has customers. SuSE customers want KDE. Good companies leaves the decision to their customers.\n\n2. Novell solutions are just a rebranded SuSE. Novell has no experience in Linux and xiamian has no market.\n\n3. Ximian is a desktop FUD machine. Leave desktop strategy to those in your company who know their customers and actually sell products.\n\n4. \"Linux Desktop\" means KDE in Europe. Ubuntu might be a rare exeption. Userlinux was vapourware. RedHat does not target the desktop.\n\n5. It's not about babies. It is about knowledge of the market and your corporate best pratice. Why did you buy a traditional KDE desktop distribution? In order to ruin its market? \n\n6. Cut jobs in the US but do not cut jobs in Nuremberg. Fire those managers responsible for this decision which gives a very bad impression about Novell and sents a message of distrust to the market."
    author: "Andre"
  - subject: "Re: Novell petition"
    date: 2005-11-08
    body: "How delightful. :)\n\nOh yes, Novell has /ruined/ SuSE. /No one/ in their right mind would /ever/ use a distribution where KDE is optional, right?\n\n1) Note that these are the Enterprise versions of SuSE, the Enterprise Server and their Novell edition, not SuSE itself. SuSE, and openSuSE, continue to support KDE as a primary desktop.\n\n2) Novell solutions are based off of SuSE, yes, and I'm sure they contain some minor improvements. However, to say that Novell has no experience in the market is absurd, and to say Ximian (and by extension GNOME) has no market is ludicrous.\n\n3) Heh.\n\n4) Probably one of the reasons they're not removing KDE from their desktop distribution. ;)\n\n5) To gain a profitable company with which they can base their Enterprise Linux products?\n\n6) heh."
    author: "senori"
  - subject: "but the good news is"
    date: 2005-11-05
    body: "Ubuntu Conference Affirms Commitment to Kubuntu and KDE\n\nThe Ubuntu Below Zero conference is in full momentum this week and Kubuntu has been prominent throughout. In his opening remarks at the start of the conference Ubuntu founder Mark Shuttleworth announced that he was now using Kubuntu on his desktop machine and said he wanted Kubuntu to move to a first class distribution within the Ubuntu community. The large number of Kubuntu users at the conference was evidence as the need for this. Free CDs for Kubuntu through shipit should be available for the next release if the planned Live CD Installer removes the need for a separate install CD.\n\nhttp://www.kubuntu.org/announcements/kde-commitment.php"
    author: "Pietro"
  - subject: "Re: but the good news is"
    date: 2005-11-05
    body: "Indeed. And if all the stuff around Novell/SuSE appears to be true KDE supporters at OpenSUSE better move their efforts to Kubuntu since that's getting shipments starting with the next release."
    author: "ac"
  - subject: "Re: but the good news is"
    date: 2005-11-05
    body: "Already downloading DVD iso. Should be installed by tomorrow morning :)"
    author: "Anonymous Coward"
  - subject: "Re: but the good news is"
    date: 2005-11-05
    body: "Good move. Strange to say I move from Kubuntu breezy to OpenSUSE this morning. Ok I installed it side-by-side like I always do, but still...\n\nI can say though that there's not much thats actually easier to do on SUSE than on Kubuntu - some trimmed down YaST (minus everything that's already in system-settings) for hardware configuraiton would do kubuntu some good, but otherwise I think it can handle any SUSE refugees already.\n\nFYI my Kubuntu -> SUSE move was merely based on performance issues with Kubuntu. Maybe I'll try a fresh breezy install - possibly my issues are just a leftover from hoary (upgraded to breezy)."
    author: "Christoph Wiesen"
  - subject: "Re: but the good news is"
    date: 2005-11-05
    body: "> but otherwise I think it can handle any SUSE refugees already.\n\nI'm a corporate user, so i'm probably a bit difficult as I require encrypted\npartitions, cisco VPN connections, MS Exchange interworking, ACPI etc. Hope it really works.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: but the good news is"
    date: 2005-11-06
    body: "* encrypted partitions: use encrypted ext3. That's part of _any_ distribution out there. (don't know about encrypted reiserfs though)\n\n* cisco VPN: http://www.kde-apps.org/content/show.php?content=12570\n\n* MS Exchange interworking: There's nothing in SUSE concerning that that wouln't be in Debian/Kubuntu as well.\n\n* ACPI: usually works well with kubuntu\n\n"
    author: "enterprise needs"
  - subject: "Re: but the good news is"
    date: 2005-11-05
    body: "> I can say though that there's not much thats actually easier to do on SUSE > > than on Kubuntu\n\nTo give you a hint: try to write a J2EE app with Eclipse deploying on Tomcat, using various jars (mostly from Apache, with one exception), in a sane (working) Java environment while getting securtiy upgrades for part of these. Install the JDK sources from repositories and have them automatically added to your Eclipse configuration. Install Apache Httpd and some PHP apps and watch them working from the first try. Etc Etc Etc\n\nDo all of these with Ubuntu/Kubuntu. You can, of course, but you need to take the distro's job on your shoulders. Kubuntu won't give you that.\n\nOf course, for a hobby it's OK, albeit unstable.\n\nThis is why people need SUSE."
    author: "a"
  - subject: "Re: but the good news is"
    date: 2005-11-06
    body: "I don't develop Java apps so might not be able to follow all the stuff you suggested, but it doesn't sound very unlikely to work on Kubuntu. Security updates, working environment, apache /w php (what is there works - have yet to find something that's really broken on a stable release).\n\nWhat is an issue here of course is the community / OSS thing. basically we would only get the \"openSUSE OSS\" version of SUSE with Kubuntu, there's no business around it and that's what will break KDE in the end for these uses.\n\nYou want VPN, SAN, supported backup solutions? Of course you can! Pick Novell's and Red Hat's GNOME desktops...\n\nI said it once, I'll say it again: bah!\n\n\n--------------\n\nTotally unreleated to this post, but some more opinions about the topic in general:\n\nhttp://suslikcentral.blogspot.com/2005/11/kde-i-hope-we-go-down-kicking-and.html\n\nBut this discussion has been there every now and again - lot's of (imporatant) people disagree completely with the Qt thing and so it's probably no good going through all this again."
    author: "Christoph Wiesen"
  - subject: "Re: but the good news is"
    date: 2005-11-08
    body: "There are a lot of small companies who do business with Debian. Those can center around Kubuntu as well."
    author: "enterprise needs"
  - subject: "proposal"
    date: 2005-11-05
    body: "Any chance people would vote with their wallets instantly?\n\nClear your bugz entries, leave mailing lists, pack out. This could probably be the only way to send clear signal to SUSE management.\n"
    author: "Anonymous Coward"
  - subject: "Re: proposal"
    date: 2005-11-05
    body: "Clear your bugz entries, leave mailing lists, pack out.\n\nAnd then fork it, make it better and incompatible with Suse just to piss them off! :)"
    author: "Patcito"
  - subject: "Re: proposal"
    date: 2005-11-05
    body: "Mail back the box, and tell them that if you wanted a GNOME desktop, you will go with Redhat or Sun."
    author: "a.c."
  - subject: "More OT: Skolelinux"
    date: 2005-11-05
    body: "This just shows the importens of (independant)distros like skolelinux (link to english site http://www.skolelinux.org/portal/index_html ) Distroes like this is all about technikal merrit and not about PR_person_let_us_marketing_our_ass_off_decision. If we are able to penetrate the education system whit this distro, just think what the generation growing up will choose when they start working after ended education  ;)"
    author: "Jo \u00d8iongen"
  - subject: "QtRuby vs PyQt"
    date: 2005-11-05
    body: "What are the pros and cons of either of these excellent toolkits for rapid GUI development?"
    author: "Alex"
  - subject: "Re: QtRuby vs PyQt"
    date: 2005-11-06
    body: "From a technical standpoint, both are very very similar.  Basically it's a choice of which language you prefer.\n\nAs for what you can do with QtRuby, I've put up some small hacks in my blog for things you can do with them:\n\nhttp://www.tarkblog.org/qtruby"
    author: "Caleb Tennis"
  - subject: "eweek likens Novell to UserLinux"
    date: 2005-11-06
    body: "# \"For example, in 2003, open-source leader Bruce Perens' attempt \n# to make GNOME the default desktop for the Debian-based UserLinux \n# was fought by KDE supporters.\"\n-----\n\nAnd we all know what happened to KDE back then: it got killed. It got butchered. It got sliced. It killed itself. It committed suicide. It died the death of bloat. It starved from being not accepted by enterprises.\n\nOh wait... Where is UserLinux nowadays?\n\nWhere will Novell be in 2 years from now?"
    author: "ac"
  - subject: "Re: eweek likens Novell to UserLinux"
    date: 2005-11-06
    body: "The problem is: KDE will not get killed. But SuSE.\n\n\nUsual scheme:\n\n1. adopt Gnome\n2. ???\n3. abandon Desktop Linux strrategy"
    author: "Bert"
  - subject: "Proof for desktop decision pro-Gnome is profitable"
    date: 2005-11-06
    body: "Comparative study of various vendors' business decisions:\n\nRedhat:\n1. Adopt Gnome as the only Linux desktop\n2. Loose lots of money\n3. Become disillusioned in desktop on Linux\n4. ???\n5. Abondon Linux desktop strategy\n\nSun:\n1. Adopt Gnome as the only Linux desktop\n2. Loose lots of money\n3. Become disillusioned in desktop on Linux\n4. ???\n5. Abondon Linux desktop strategy\n\nHP:\n1. Adopt Gnome as the only Unix desktop\n2. Loose lots of money\n3. Become disillusioned in desktop on Unix\n4. ???\n5. Abondon Unix desktop strategy\n\nEazel:\n1. Adopt Gnome as the only Linux desktop\n2. Loose lots of money\n3. Become disillusioned in desktop on Linux\n4. ???\n5. Abondon Linux desktop strategy\n\nUserLinux:\n1. Adopt Gnome as the only Linux desktop\n2. Loose lots of money\n3. Become disillusioned in desktop on Linux\n4. ???\n5. Abondon Linux desktop strategy\n\nAnyone else seeing a common pattern here?\n\nProbably Mark Shuttleworth does. He's a smart guy. He embraced KDE and encouraged Kubuntu to take off. He now uses KDE/Kubuntu on his personal desktop \n[ http://www.kubuntu.org/announcements/kde-commitment.php ].\nI'm sure this man already thinks how he can take advantage of the stupidity displayed by Novell management in chopping KDE and betting their careers on Gnome.\n\nThe Ubuntu suite of offerings is starting to become really sweet -- rock-solid servers based on Debian, Gnome/Ubuntu as well as KDE/Kubuntu offerings for desktops, workstations and thin clients -- and it is going to be ready to enter the enterprise market very soon too. (And in my  definintion of \"enterprise\", it is not only the top 500, but most important the Small and Medium Businesses (SMB), where Linux stands the best chances to be adopted by the owners, as well as supported by small IT professional service providers.)"
    author: "ex-suse"
  - subject: "Re: Proof for desktop decision pro-Gnome is profit"
    date: 2005-11-06
    body: "I don't know if you realized, but MandrakeSoft and SUSE also lost lots of money. SUSE was bought by another company, and I don't have to begin talking about the financial problems with Mandrake.\n\nAnother thing, SUSE Linux and OpenSUSE will always support both KDE and GNOME equally, as you see in version 10. What's changing is Novell products for corporations.\n\nAnd since you mention Sun, I can tell you they gave new life to their workstations since making GNOME the default. "
    author: "Reply"
  - subject: "Re: Proof for desktop decision pro-Gnome is profit"
    date: 2005-11-06
    body: "<i>\"And since you mention Sun, I can tell you they gave new life to their workstations since making GNOME the default.\"</i>\n\nWell duh! Sun could have chosen ANYTHING besides CDE and given new life to their workstations!"
    author: "Brandybuck"
  - subject: "Re: Proof for desktop decision pro-Gnome is profit"
    date: 2005-11-06
    body: "> and I don't have to begin talking about the financial problems with Mandrake.\n\nActually it was the e-learning division that made Mandrake lost money, not the Linux desktop.\n\n> And since you mention Sun, I can tell you they gave new life to their\n> workstations since making GNOME the default.\n\nYet they seems to have changeed idea: http://osnews.com/story.php?news_id=12501\nGnome is a \"throw away\" desktop now..."
    author: "ac"
  - subject: "Re: Proof for desktop decision pro-Gnome is profit"
    date: 2005-11-07
    body: "\"I don't know if you realized, but MandrakeSoft and SUSE also lost lots of money. SUSE was bought by another company, and I don't have to begin talking about the financial problems with Mandrake.\"\n\nActually Mandriva is profitable now.  The distro always ways, it was the ill-conceived e-learning stuff that lost money.  Heck, they've even started buying out other distros, not the normal sign of a financially troubled organisation.\n\nJohn."
    author: "odysseus"
  - subject: "Re: Proof for desktop decision pro-Gnome is profitable"
    date: 2005-11-06
    body: "Agreed. Common pattern there is not listening to your customers."
    author: "Anonymous Coward"
  - subject: "KDE for all Desktops -- Gnome for all Servers!"
    date: 2005-11-07
    body: "To take advantage of the situation now, KDE should concentrate on that one message, and hammer it home.\n\nI'm even happy with leaving the server market to Gnome, and the GUI admin terminals serving as a window into the servers' bowels.\n\nBut the field of KDE is the desktop. The workstations. The thin clients. The kiosk-ed computers. The classrooms. The home users. The SOHO, SMB & SME offices."
    author: "ex-suse"
  - subject: "KDE should target SOHO, SMB and SME market"
    date: 2005-11-07
    body: "Over the boring weekend I came across a most excellent market analysis and study conducted by John H. Terpstra, member of the Samba Team.\n\nIt is published by planetlinux.com. A four part series with extremely valuable facts, figures and arguments:\n\n --> \"The Yin and Yang of Open Source Commerce, Parts 1-4\" \n\n* Part 1: http://linuxplanet.com/linuxplanet/reviews/6062/1/\n* Part 2: http://linuxplanet.com/linuxplanet/reviews/6064/1/\n* Part 3: http://linuxplanet.com/linuxplanet/reviews/6065/1/\n* Part 4: http://linuxplanet.com/linuxplanet/reviews/6066/1/\n\nJohn does not only comment on the past blunders of Novell (while writing this series he was not aware of the current decision process inside Novell), he also has invaluable advice for anyone who tries to establish Linux desktops in commercial environments.\n\nAccording to John, the best bet for any pushers of Linux for the desktop is the market of the SOHOs (Small Office/Home Office), SMBs (Small and Medium sized Businesses -- up to 25 employees) and SMEs (Small and Medium sized Enterprises -- up to 500 employees)."
    author: "ex-suse"
  - subject: "Re: KDE should target SOHO, SMB and SME market"
    date: 2005-11-07
    body: "The point about the SOHO, SMB market is very agreeable, until you actually dive into it. Once you are there the buiseness owners only care about \"compatibility with major business software\". This means, they dont give a rats ass about Free. What they value is the ability to run QuickBooks, FedEx shipping software, junk-mailing plugins for Outlook and other 3rd party soft. Small businesses buy computers to run functional, off the shelf applications, not share files and browse all day. \n\nUntil linux has actual QuickBooks and the family, SOHO Desktop is an unclimbable castle for FOSS. Talking about going in circles."
    author: "Daniel \"Suslik\" D."
  - subject: "Re: KDE should target SOHO, SMB and SME market"
    date: 2005-11-08
    body: "I agree. Linux makes sense for SMBs only if they have cheap access to unix expertise, in which case reduced maintenance costs (spyware, AV etc) can marginally outweigh the initial productivity loss introduced by fewer available off-the-shelf applications. Same for SOHO, maybe less so for skilled IT consultancies with in-house expertise.\n\nWhen a linux setup really shines is in medium-to-big organizations, where maintainance and business needs operate on a bigger scale. It's much more pleasant to administer 500 unix/linux boxes than 500 quirky Windows 2000/XP/2003/everything-goes... But still, unix expertise shortage is an issue."
    author: "Giacomo"
---
There is an <a href="http://www.toolshed.com/blog/articles/2005/10/07/interview-with-qtruby-author-caleb-tennis">audio interview with Caleb Tennis</a> author of the new book <a href="http://www.pragmaticprogrammer.com/titles/ctrubyqt">Rapid GUI Development with QtRuby<a>.  *** <a href="http://amarok.kde.org">amaroK</a> started their fundraising drive.  *** <a href="http://software.newsforge.com/article.pl?sid=05/10/11/2055236">Newsforge reports</a> on <a href="http://create.freedesktop.org">Create @ Freedesktop</a> the new project to bring together graphics projects including <a href="http://www.scribus.org.uk">Scribus</a> and <a href="http://www.koffice.org/krita">Krita</a>, meanwhile <a href="http://www.openclipart.org">Open Clip Art Library</a> 0.18 was released. *** <a href="http://glibc-bsd.alioth.debian.org/ging/">Ging</a> released 0.1.0 of their <a href="http://www.debian.org/ports/kfreebsd-gnu">Debian GNU/kFreeBSD</a> Live CD based on KDE.



<!--break-->
