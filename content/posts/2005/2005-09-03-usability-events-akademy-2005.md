---
title: "Usability Events at aKademy 2005"
date:    2005-09-03
authors:
  - "cpaul"
slug:    usability-events-akademy-2005
comments:
  - subject: "Did they discuss this...?"
    date: 2005-09-03
    body: "I wonder whether any discussion was made on tool-bars within KDE applications. I would like them to be as flexible as possible. I mean, they should be able to be merged and moved with ease. What is happening now is that too much space is being wasted by tool-bars which do not cover the entire width of the application window and merging an existing tool-bar with another is in most cases impossible. In cases where merging might be possible, one finds that the application sometimes \"forgets\" its menu settings! They should also try to limit the default number of tool-bars. Konqueror has so many of these tool bars, some with just 1 item! Imagine! "
    author: "charles"
  - subject: "Usability in Kmail"
    date: 2005-09-03
    body: "Talking about usability in kmail, how about solving bug 97925?\nIs it logical every time you exit kmail it ask you if you want to save your password in the wallet, once for each account, if you want them saved in the config file? Shouldn't be enough to ask just one time, or better, put an option in the configuration dialog?\nPlease, it is a one liner!"
    author: "John Doe"
  - subject: "KDE Toolbars"
    date: 2005-09-03
    body: "I scanned Akademy 05 all week and saw no mention of KDE toolbars, so I guess their will be no new developments there, how sad. I hoped QT4 would solve this problem!\n\nI think the toolbar for FireFox is really, really great, beats out IE5+, hands down! Thats the kind of flexibility I like to see for Konqui and all its relatives\n\nAnd I dont think collapsible toolbars (eg MS Office toolbars when the icons are too many) is right Usability."
    author: "athleston"
  - subject: "Re: KDE Toolbars"
    date: 2005-09-05
    body: "KDE Toolbars are a HIG issue and we are at the very early stage of defining their look and feel.  Discussion about the toolbars can be found on the kde-usability-devel mailing list."
    author: "Celeste Paul"
  - subject: "UIsability?"
    date: 2005-09-03
    body: "I do not have usability problems with KDE but there is a lot of usability problems with other parts of the system. Integration within KDE is nice but KDE is fine. The problem is how it interacts with the rest of the system. \n\nUsability improvements would mean to me:\n\n- integration of emulators config in Kontrolcenter, such as Wine config\n\n- unified theme bridge for KDE and Gnome\n\n- same colour scheme for gnome and kde\n\n- better integration of non KDE qt apps like Scribus\n\n- easier download of the *illegal* DVD codecs\n\n- easier translation management\n\n- grammar check for word processor\n\n- more example files for applications to start with. this motivates a lot. KDE-files looks pretty empty."
    author: "martin"
  - subject: "Re: Usability?"
    date: 2005-09-04
    body: "- integration of emulators config in Kontrolcenter, such as Wine config\nYou essentially want everything needing configuration have their own kcm-module:-) Suse's Yast has several, and I think you can find a few more on apps-kde.org. Further more the python bindings makes it possible to write kcm's in python lovering the difficulty creating additional modules. As for Wine they have now started to \"eat their own dogfood\" by using winelib in winecfg.\n \n- unified theme bridge for KDE and Gnome\nThe GTK-QT Theme Engine does this to some degree, it lets you use Qt styles in GTK.\n \n- same colour scheme for gnome and kde\nHave existed since KDE 1, in the color kcm select \"Apply colors to non-KDE applications\".\n \n- better integration of non KDE qt apps like Scribus\nSounds like it was discussed at aKademy, and different solutions for this is considered for KDE4. Unfortunately this is this is not easily done in C++. Sadly it is not as simple as in Python, try Eric3 with and without the PyKDE bindings present. Nice.\n \n- easier download of the *illegal* DVD codecs\nIn the realm of national laws, lawyers and the approach to this by the different distributions. Turbolinux has a licensed player, making it legal everywhere. \n\n- easier translation management\nNot sure what you are referring to, as you now have the ability to select translations from a list of available ones. Can it get any simpler? Only thing I can think of are improved support for people using more than one language when writing. Things like the ability to define different languages to contacts and automatic switching of the spellchecker according to the language you are writing.  \n \n- grammar check for word processor\nTypical nice to have feature, and a difficult problem to solve. Adding internationalization to the mix makes it incredible difficult.\n\n- more example files for applications to start with. this motivates a lot. KDE-files looks pretty empty.\nNot quite sure what you mean, templates perhaps? If so it's a nice job for non-programmers and people looking to get involved with KDE, so why not get started and make some. Most applications maintainers will happily accept them."
    author: "Morty"
  - subject: "KDE toolbars clumsy"
    date: 2005-09-03
    body: "The problem I see with toolbars, say for example, konqueror's toolbar, is one cannot add \"location\" toobar items to Main toolbar and too bookmark toolbar.\n\nThe toolbar must be flexible, it should allow any item from any other toolbar from the same (or for more configurability from other application too).\n\nThe \"action\" item in the main toolbar must be made individual so that those actions can be added to other toolbars.\n\nToolbars and menubars must provide more flexibility. especially \"making menubar like toolbar\" make life easy for user and developer (so that hide menubar code is not required to code into individual application)\n"
    author: "fast_rizwaan"
  - subject: "Re: KDE toolbars clumsy"
    date: 2005-09-04
    body: "Actually you can add location toolbar items in the main toolbar. Just remember that there can only be one location text box in all your toolbars so remove it from the location bar first :P\n\nBesides that there are many many items that you can add to just one class of toolbars though (location not among them)\n\nThe thing is that in the current kde all the toolbar implementations are somewhat crappy. As many other users have allready suggested the firefox approach is very cool and it's what i'd like to see in the future\n\nApart from all these I hope that the new kde HIG will enforce a cleaner menu & toolbar style..."
    author: "Elias Pouloyiannis"
---
Usability has grown over the year since the last aKademy. During the Coding Marathon portion of the conference, the KDE-Usability group gave several presentations and tutorials so developers can learn more about usability, and get live usability support while they hacked away. It was a great success and there were a lot of great bug fixes completed during the weekend. The following is a summary of the presentations and tutorials from the conference.







<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; width: 310px;">
<a href="http://static.kdenews.org/jr/usability.jpg"><img src="http://static.kdenews.org/jr/usability-wee.jpg" alt="photo" width="300" height="225" border="0" /></a><br />
<a href="http://static.kdenews.org/jr/usability.jpg">Usability experts</a> Ellen and Jan (sitting) with core developers Dirk and Kulow
</div>

<p><b>Report on KDE Usability</b><p>

<p>There was a brief report on the major usability work which has been done over the past year. There have been several reports and tests done for various applications as well as bug fixes. Major work has been done on Kontact, KMail, KPDF, and many other applications.</p>

<p><b>BoF Human Interface Guidelines</b></p>

<p>The usability section of the HIG has made slow progress over the past year. After speaking with developers, we found that they would like more technical code examples in the document, as well as more explanations about concepts. There are also grey areas as to how we would like the interface to look and react to the user and we need more community feedback. We are currently coordinating with some developers to collect code examples and technical information. </p>

<p><b>Exercise Usability Methods: Personas </b><br />by Tina Trillitzsch and Florian Graessle</p>

<p>Tina and Florian presented a tutorial on how to build personas using KDissert. Personas are a good way to get to know who your users are, and to determin who is your number 1 user you should be designing for.</p>

<p><b>Live Usability Test: KDissert</b><br />by Ellen Reitmayr </p>

<p>El demonstrated a live usability test with KDissert, showing developers one of the methods we use to discover and address usability issues. It was a very good test which found several issues with the behaviour of KDissert and allowed the developers to discuss solutions to the problems.</p>

<p><b>Usability Research: Alternatives to the KMenu</b><br /> by Celeste Paul </p>

<p>Celeste collected and presented research information related to some of the issues with the KMenu. She gave developers background information in recent research in information management, information retrieval and menu design. Additionally, she applied this research to the KMenu, explaining the good and bad things about it. There were also some suggestions for reworking the KMenu and replacement applications based on her findings.</p>

<p><b>Exercise Usability Methods: Paper Prototyping</b><br /> by Tina Trillitzsch and Florian Graessle</p>

<p>Tina and Florian presented a hands-on demonstration with developers on how paper prototyping can save interface development time and discover usability issues early on in development. Several developers participated in the exercise and performed on-the-fly paper prototyping tests with each other.</p>






