---
title: "Electrical Engineering Comes to KDE with KTechLab"
date:    2005-01-10
authors:
  - "jriddell"
slug:    electrical-engineering-comes-kde-ktechlab
comments:
  - subject: "KTechLab"
    date: 2005-01-10
    body: "Cool, engineering apps coming to linux/kde/free software :-)\n\nBut, can you please give it another name, instead of K<something> ?\nMaybe TekkLab ?\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: KTechLab"
    date: 2005-01-10
    body: "I always prefer something in an obscure language."
    author: "Ian Monroe"
  - subject: "Re: KTechLab"
    date: 2005-01-11
    body: "You beat me to it.  I was going to suggest TekLab...isn't\nthere already something simmilar called LabTech?  They\nmight not be too happy about it.\n\nKTechLab is fine. I\nwould not change it.  It could be something worse,\nlike CirkuitLab, PikLab, or ElektrikLab.\n\n\n"
    author: "Henry"
  - subject: "Re: KTechLab"
    date: 2005-01-11
    body: "You are thinking of LabVIEW, which is a frontend GUI to read data commonly used to interface with hardware."
    author: "Turd Ferguson"
  - subject: "Cool app"
    date: 2005-01-10
    body: "I think it's the best engineering app.\nAt the moment it support only PIC16F84 but i think they'll include\nother PIC series. \nVersion 0.1 works fine and this is a good stuff.\n\nBye, \nvalter"
    author: "valter"
  - subject: "Re: Cool app"
    date: 2005-01-10
    body: "Atmel AVR support would be cool too."
    author: "Aaargh!"
  - subject: "Re: Cool app"
    date: 2005-01-11
    body: "I'm not that familiar with the Atmel AVR. Are there decent software simulators the AVR range of microprocessors under linux?\n\nI've attempted to design ktechlab with easy extendibility to support other microprocessors. So it would be a matter of interfacing with a simulator if one exists; the rest is easy :-)\n"
    author: "David Saxton"
  - subject: "Re: Cool app"
    date: 2005-01-11
    body: "/me wonders if its worth digging out his old 68HC11 simulator he did in school... does anyone use that gem still? ;)  "
    author: "Ian Reinhart Geiser"
  - subject: "Re: Cool app"
    date: 2005-01-11
    body: "I have an old simulator here too, and a full EVBU with an HC11E9 sitting in my\ndressing table drawer. IE, yes dig it out. :)"
    author: "taj"
  - subject: "Re: 68HC11"
    date: 2005-01-11
    body: "Its one of the uP's that I am going to learn in one of my courses this year. I'd use a simulator for it built into KTechLab if there was one."
    author: "John Hughes"
  - subject: "Re: Cool app"
    date: 2005-01-11
    body: "Well, some google searching has revealed Simulavr: http://www.nongnu.org/simulavr/\n\nNot sure how good it is though.\n\nGCC supports AVR as a backend. http://gcc.gnu.org/backends.html \n\nSome interesting relevant info linked from there: http://gcc.gnu.org/ml/gcc/2003-10/msg00027.html\n\nKeep up the great work! KTechLab is already looking very good."
    author: "Paul Eggleton"
  - subject: "Re: Cool app"
    date: 2005-01-11
    body: "You can also use sdcc (sdcc.sf.net) to compile, debug and sometimes simulate avr and other microcontrollers. The advantage of sdcc over gcc is that the sdcc compiler was designed for small chips while gcc is sometimes difficult to optimise for small chips. I don't remember that it supports overlay for example. "
    author: "Philippe Fremy"
  - subject: "Re: Cool app"
    date: 2005-01-11
    body: "For simulation of PICs, the next version of ktechlab will contain a *lot* more support, hopefully matching all PICs that are capable of being simulated by gpsim. 0.2 will be released after gpsim-0.22 is released, which I'm told should be soon.\n"
    author: "David Saxton"
  - subject: "Great news!"
    date: 2005-01-10
    body: "I have been trying to find something like this for a while.  Proper simulators for certain non-free OSes are very expensive.  Will try it as soon as I get home."
    author: "Robert Knight"
  - subject: "Re: Great news!"
    date: 2005-01-10
    body: "And proper simulators for certain free OSes are usually even more expensive. Lots of vendors of EDA tools have versions with various limitations they sell for relative low prices or free for non commercial use. Sadly it is most common for the windows versions to be the gratis one. The Linux versions usually mirrors the vendors most expensive windows offerings. On the positive side, I think this tells us that the \"real professionals\" use Linux:-)"
    author: "Morty"
  - subject: "very good!"
    date: 2005-01-10
    body: "Now, next semester, I can use this instead of win-apps for my electonic circuit classes on my computer science course!\nThanks a lot."
    author: "Iuri Fiedoruk"
  - subject: "cool program"
    date: 2005-01-10
    body: "and a really nice logo!\n\nIt looks like this will help a lot of students!"
    author: "standsolid"
  - subject: "The review is denied access"
    date: 2005-01-11
    body: "I wish to read the review, but when I click in \"This review\" the following message is showed:\n\nYou have been denied access to this site.\n\nPlease contact the administrator for assistance.\n\nThadeu"
    author: "Thadeu"
  - subject: "Eng. "
    date: 2005-01-11
    body: "How does this relates to kpicdev ?\n"
    author: "Amilcar"
  - subject: "Shouldn't it be 'Electronic Engineering Comes ...'"
    date: 2005-01-11
    body: ">> While only at version 0.1 it already contains a lot of functionality for developing and simulating <b>electronic</b> circuits\n\nSounds more like an electronic engineering tool than an electrical engineering tool to me."
    author: "pedant"
  - subject: "Avr"
    date: 2005-01-12
    body: "For more info on Avr simulation check out www.avrfreaks.net, my favourite AVR site.Haven't been there in ages though out of embedded for a while.\nAvr-Gcc works pretty ok though.\nCheers,\nTisham."
    author: "Avr->Simulavr,gdb"
  - subject: "FANTASTIC"
    date: 2005-01-12
    body: "Hey, guys, this program is FANTASTIC!! Have you tried it?\n\nWhat I would like to know is how the nonlinear components are modeled. For example which is the transfer function of a diode? Is it modeled as a linear function or can be something more complex (something like pspice does)?\n\nIt's incredible how many things this program can do. Thanks again!!\n\nCiao,\n\tDario"
    author: "Dario Massarin"
  - subject: "typo"
    date: 2005-01-12
    body: "a typo that can be seen on the 4th screenshot (http://ktechlab.fadedminds.com/screenshots/microbasic_editor.png): \"sucessful\"\n"
    author: "ac"
  - subject: "Re: typo"
    date: 2005-01-12
    body: "Thanks; fixed now."
    author: "David Saxton"
  - subject: "And what about the 8051 family?"
    date: 2005-01-12
    body: "Currently I'm working with the MCS-51 MCUs because they're very popular. They have been around for ages and it looks like they're going to last.\n\nIt's feasible support them?"
    author: "David"
  - subject: "Maybe it could be built on top of KDevelop platfom"
    date: 2005-01-12
    body: "Hi,\n\nMaybe you could write your program on top of KDevelop platform:\nhttp://www.kdevelop.org:8080/HEAD/doc/platform/html/\n\n"
    author: "Amilcar"
  - subject: "Re: Maybe it could be built on top of KDevelop platfom"
    date: 2005-01-12
    body: "That looks quite interesting; I wasn't aware of the KDevelop Platform before.\n\nSo my understanding is that the KDevelop Platform provides a codebase for IDE features such as project management, plugins, language support, etc?\n"
    author: "David Saxton"
  - subject: "avoid duplicate work"
    date: 2005-01-12
    body: "There is a gtk/glib toolsuite at http://www.geda.seul.org/ called gEDA that is starting to approach widespread usability.  They are still migrating their GUIs to gtk2, though.  I suggest some kind of interface sharing or at least dialogue in order to prevent duplication of work, or orthogonal directions of work from stalling GPL'ed EDA toolsuites from taking off, which I think is long overdue.\n\nFor instance, a common symbol library might be one way to cut down on maintenance work, while still allowing a KDE schematic editor and a gtk schematic editor to compete, coexist, and contribute to each other.\n\nGreg"
    author: "greg"
  - subject: "3-tier model maybe?"
    date: 2005-01-13
    body: "It would be better to use a 3-tier model for gEDA,\nmainly if the logic could be kept seperate from the GUI.\n\nBut hey, anyone can feel free to port it to KDE.\n\nFred."
    author: "fprog26"
---
The first version of <a href="http://ktechlab.fadedminds.com/">KTechLab</a> was released a couple of weeks before the new year. While only at version 0.1 it already contains a lot of functionality for developing and simulating electronic circuits. Currently KTechLab can create circuit diagrams for electronics and flow diagrams for PIC chips (a family of programmable chips). It can even compile and run your flow diagrams in a circuit.  <a href="http://www.kde.me.uk/index.php?page=ktechlab-review">This review</a> contains an introduction and a look at the features to be expected in future versions.



<!--break-->
