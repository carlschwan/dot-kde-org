---
title: "KDE Commit Digest for September 2, 2005"
date:    2005-09-03
authors:
  - "skite"
slug:    kde-commit-digest-september-2-2005
comments:
  - subject: "finally"
    date: 2005-09-05
    body: "\"Konqueror defaults to \"smart\" popup blocking.\"\n\nIt's about time.  I do enjoy KDE's configurability, and finding the setting for blocking popups was pretty obvious for me (having worked with a lot of javascript), but it was a bit much to expect a new user to figure it out.  Thanks for this sensible default."
    author: "MamiyaOtaru"
  - subject: "Re: finally"
    date: 2005-09-05
    body: "> Thanks for this sensible default.\n\nIt was not just about changing the default but implementing that you can let show suppressed windows first."
    author: "Anonymous"
  - subject: "Wallpaper cleanup"
    date: 2005-09-05
    body: "Great! I was \"shocked\" when I saw the artwork included in KDE the first time. Such old-stylish things I've never seen in new releases of Windows, MacOS or Gnome.\n\nAnd because the 3.5 release will be the last in the 3.x series, it probably had to stand against the new Windows Vista. With this old artwork it would not make a good impression."
    author: "Domi"
  - subject: "Re: Wallpaper cleanup"
    date: 2005-09-05
    body: "It is also important to unify at other places.\n\nIt is simply bad to call a style \"light, 2nd revision\".\n\nthe naming has to be unified. "
    author: "Bert"
  - subject: "Re: Wallpaper cleanup"
    date: 2005-09-06
    body: "I certainly hope that these old styles are not removed, because I actually like some of them.\n\nI do not mean to offend anyone, but for me they have been far more timeless than the other ones. Need I remind anyone of the frenzies over Baghira, Liquid, Keramik, Plastik, and now Clearlooks over the past four years? These UI styles are fine, but like everything else they become clich\u00e9 and loose their value. One must ask critically where these in vogue styles will be two or three years from now. \n\nWhat does it for me is that some of the older UI styles are often less flamboyant than these newer ones that mimic candy.\n\nWhat I would hope to see developed is a clean looking, unique, professional, elegant, and timeless UI style. It is impossible to know exactly what this would be, but it is what I would like."
    author: "Matt T. Proud"
  - subject: "Other KDE 3.5 Changes"
    date: 2005-09-06
    body: "Hi,\n\nI have graphically documented some other KDE changes in this <A href=\"http://process-of-elimination.net/index.php?title=KDE_3.5\">KDE 3.5 article</A> that includes among other things usability improvements and a screenshot of Konqueror passing the Acid 2 test."
    author: "Matt T. Proud"
---
Some highlights from <a href="http://commit-digest.org/?issue=sep22005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=sep22005&all">all in one page</a>):



<a href="http://konqueror.kde.org/">Konqueror</a> defaults to "smart" popup blocking.  <a href="http://edu.kde.org/ktouch/">KTouch</a> adds a Russian keyboard and training file.  New nxfish ioslave which allows sharing between local NX Client and the remote NX server without requiring Samba.  Wallpapers and background tiles were "cleaned up" for <a href="http://developer.kde.org/development-versions/kde-3.5-release-plan.html">the 3.5 release</a>.



<!--break-->
