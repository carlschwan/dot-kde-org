---
title: "KDE Commit Digest for May 13, 2005"
date:    2005-05-15
authors:
  - "dkite"
slug:    kde-commit-digest-may-13-2005
comments:
  - subject: "Adblock"
    date: 2005-05-15
    body: "> Konqueror supports AdBlock.\n\nIvor Hewitt, you're my personal hero!"
    author: "Anonymous"
  - subject: "Ah but..."
    date: 2005-05-15
    body: "Derek's my hero. :)"
    author: "Ivor Hewitt"
  - subject: "Congratulations Scripty!"
    date: 2005-05-15
    body: "Our new little fellow, Script Kiddy, just entered the top 10 in his first active week. Even better, (s)he's our #1 committer among us ;)"
    author: "Bram Schoenmakers"
  - subject: "Re: Congratulations Scripty!"
    date: 2005-05-15
    body: "Fixed. There is one more issue where the module isn't assigned for some reason.\n\nDerek"
    author: "Derek Kite"
  - subject: "nice"
    date: 2005-05-15
    body: "nice work again, derek. keep 'em coming..."
    author: "superstoned"
  - subject: "AdBlock support"
    date: 2005-05-15
    body: "Does it support the usage of AdBlock filter lists, such as the excellent Filterset.G?"
    author: "mmebane"
  - subject: "Re: AdBlock support"
    date: 2005-05-15
    body: "[1]Qoute: vor Hewitt\n\n\"The current version of AdBlocK will support simple (wildcard style) filter sets exported from Firefox AdBlock. \n I'm making the changes at the moment to make it support Filterset.G.\"\n\n[1]http://bugs.kde.org/show_bug.cgi?id=15848#c103 "
    author: "Sam Weber"
  - subject: "Broken links?"
    date: 2005-05-15
    body: "Is it just me, or do those \"View Source Code(X Files)\" links take you to blank pages?"
    author: "AC"
  - subject: "Re: Broken links?"
    date: 2005-05-15
    body: "Yup, the same happens for me. Nevertheless it is great to have a CVS digest ahem.. Commit Digest again :-) Thanks Derek!"
    author: "MK"
  - subject: "Re: Broken links?"
    date: 2005-05-15
    body: "Check the dept line... late-and-broken =)"
    author: "ac"
  - subject: "Re: Broken links?"
    date: 2005-05-15
    body: "Fixed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Broken links?"
    date: 2005-05-16
    body: "Thanks. :-)"
    author: "AC"
  - subject: "Old style gone?"
    date: 2005-05-15
    body: "I used to be able to change the \"newissue\" in the url to \"issue\" and get the old formatting. It seems you've switched entirely to the new formatting. Is there any way of getting the old format back?"
    author: "mmebane"
  - subject: "Broken link"
    date: 2005-05-15
    body: "The link for 'Internationalization Status' is broken!\nRight URL: http://i18n.kde.org/stats/gui/trunk/toplist.php\n\nThanks Derek for your work.\n"
    author: "Georg Prager"
  - subject: "optimizations? "
    date: 2005-05-15
    body: "Hi,\n\nthank you Derek for your weekly work on the digest. I noticed that the commit type \"optimize\" doesn't exist any longer. Is this intentional or is this feature not ported yet? "
    author: "Michael Jahn"
  - subject: "Re: optimizations? "
    date: 2005-05-15
    body: "There were no noteworthy optimizations this week. (or none that I noticed)\n\nDerek"
    author: "Derek Kite"
  - subject: "KDE 3.4.1"
    date: 2005-05-15
    body: "KDE 3.4.1 is coming soon -:) !\n\n"
    author: "Anonymous Specutator"
  - subject: "Re: KDE 3.4.1"
    date: 2005-05-15
    body: "Rather late actually - one month delayed caused by Subversion transition delay and problems. On the other side it will have x.x.2 quality. :-)"
    author: "Anonymous"
  - subject: "SVN"
    date: 2005-05-15
    body: "Hey,\n  In case anyone is interested, to get konqueror to 'view the svn repositories' a new feature to .desktop was added.  Basically I added a new field which is basically a dcop call.  The dcop call returns the list of actions to display.  I think that this is really powerful for other apps and would allow the action menu to correct show unmount/mount drive, depending on whether it is mounted or not, and things like this.\n\n  I'm wondering whether it is possible to make this async.  So the menu can appear, then in the background have the dcop calls made, action list decided, and _then_ added to the list in realtime, while the user is watching it.  This would allow for more complex scripts without blocking the gui.\nIf anyone know how to do this, please email me.\n\nJohn (Flux) Tapsell"
    author: "JohnFlux"
  - subject: "Re: SVN"
    date: 2005-05-15
    body: "The svn feature for Konqueror, are update of kdebase adequate.  Or are other packages involved, like kdesdk or something?"
    author: "Morty"
  - subject: "Re: SVN"
    date: 2005-05-15
    body: "yes, you need latest kdesdk/kioslave/svn too\nyou need subversion headers/libs to be able to compile it ;)\n\nCheers,\nMik"
    author: "Mickael Marchand"
  - subject: "Re: SVN"
    date: 2005-05-16
    body: "That was actually what I suspected, but I hoped for something simpler. \n\nParticularly since a few days ago when I ran configure, I saw the message of some svn tools who was excluded from the build. Thinking they would come in handy, I decided to install the relevant headers/libs as you mention. Since then, the build has failed :-) I have to examine that one closer, one of these days."
    author: "Morty"
  - subject: "Re: SVN"
    date: 2005-05-16
    body: "Isn't this a huge security problem?  As I understand what you've written, right-clicking a .desktop file (that you perhaps downloaded from somewhere) would execute an arbitary dcop command to get the list of actions, but the dcop call could do anything.  Previously left/double clicking on a .desktop file to launch it would be unsafe if you got it from an untrusted source, but now right-clicking is too.  If I right-clicked an untrusted .desktop file to view it's contents, for example, I'm open to attack just by doing that.\n\nIf I'm correct, this would make .desktop files the most dangerous of any files you can download, for any operating system I've heard of -- they can actually execute arbitrary commands without even being opened, by someone attempting just to inpsect them.  Please, please revert if this is the case."
    author: "Luke Plant"
  - subject: "Re: SVN"
    date: 2005-05-16
    body: ".desktop files are not the only think to worry about, KNewStuff is a big open security hole, but no one seems to care.\n\n"
    author: "Jimmy "
  - subject: "Re:security "
    date: 2005-05-17
    body: ">KNewStuff is a big open security hole\nThat is pure and utter nonsens, altough you have the possibility to create a security hole with KNewStuff. One major part of the security in KNewStuff are the responsibility of the application developer, and it boils down to \"do not allow executable material from a untrusted source\". Basically any application who enable downloading of executable material, have to use a trusted repository. This is not a security problem, since it's controlled by the application developers(And you already trust them enough to install their app). \n\nNon executable content, like background pictures are not a added security risk. Afterall if you have an exploit for the image library, it will also affect other parts of KDE too, like Konqueror. Giving several attackvectors, most more efficient than KNewStuff.\n\nSo please elaborate for all who don't care, how this security hole will work."
    author: "Morty"
  - subject: "Re: SVN"
    date: 2005-05-16
    body: "I've now looked at the whole file, and it doesn't seem to be the security problem I had envisaged.  When you right click on a file, various .desktop files are read to find the list of actions available, including ones in the servicemenu folders and the file itself if it is a .desktop file.  However, the functionality described has only been added to the ones that were found in the servicemenu folders, so the dcop commands  in the hypothetical file you downloaded from the net won't be executed.\n\nIt still makes me rather nervous though...\n"
    author: "Luke Plant"
  - subject: "Re: SVN"
    date: 2005-05-17
    body: "Have a think about it, and if you see any security problems let me know, or even anything that you can think of to make it more secure."
    author: "JohnFlux"
  - subject: "FWIW on Dates"
    date: 2005-05-16
    body: "22th and 23th should be 22nd and 23rd."
    author: "Marc Driftmeyer"
  - subject: "Two KOffice categories"
    date: 2005-05-16
    body: "While there is significant activity in KOffice development right now because of the upcoming release, I think that we could still manage to handle it even with just one category.  :-)"
    author: "Inge Wallin"
---
In this week's <a href="http://cvs-digest.org/?issue=may132005">KDE Commit Digest</a> (<a href="http://cvs-digest.org/?issue=may132005&all">all in one page</a>): 

<a href="http://digikam.sourceforge.net/Digikam-SPIP/">digiKam</a> adds an image refocus plugin.
<a href="http://www.djvuzone.org/">DjVu</a> support added to KViewShell.
<a href="http://www.konqueror.org">Konqueror</a> file manager can view Subversion repositories. Konqueror supports <A href="http://lists.kde.org/?l=kfm-devel&m=111454967004664&w=2">AdBlock</a>.
<a href="http://amarok.kde.org/">amaroK</a> gains <a href="http://dot.kde.org/1114696139/">Zeroconf</a> support for Shoutcast, and begins initial work on multi-engine service discovery.
K-Menu now has a search field.





<!--break-->
