---
title: "Quickies: RSS, HURD, Edu, Icons and Marriage"
date:    2005-07-17
authors:
  - "jriddell"
slug:    quickies-rss-hurd-edu-icons-and-marriage
comments:
  - subject: "Oxygen"
    date: 2005-07-16
    body: "Isn't a bit too early to show Oxigen at Akademy? KDE4 seems at least 1.2 year from now, and by the time it will be out, Oxygen will be already looking \"old\"."
    author: "ac"
  - subject: "Re: Oxygen"
    date: 2005-07-16
    body: "> by the time it will be out, Oxygen will be already looking \"old\"\n\nHint: simply don't look at it now then. ;-)"
    author: "Anonymous"
  - subject: "Re: Oxygen"
    date: 2005-07-16
    body: "That's impossible, I'm too curious :)\nAnd not only I will look at Oxygen, I will also install it on my 3.4 as soon as someone on kde-look makes an iconset more or less \"inspired\" by Oxygen ;)\nSeriously, that's exactly why I'm saying that it is too early. Microsoft will show Aero just in the very last betas of Longhorn, not now: a new product needs a fresh look."
    author: "ac"
  - subject: "Re: Oxygen"
    date: 2005-07-18
    body: "How does Aero look like?"
    author: "Martin"
  - subject: "Re: Oxygen"
    date: 2005-07-16
    body: "1.2 years? A long time, sounds like 3.6 will be released before..."
    author: "Gerd"
  - subject: "Re: Oxygen"
    date: 2005-07-16
    body: "KDE 3.5 will be really the last release of 3.x series."
    author: "Anonymous"
  - subject: "Re: Oxygen"
    date: 2005-07-21
    body: "Wasnt that what they said about 3.4 as well?\nAnyway I'm pretty sure it'll be the last 3.x, but why make such a fuss about Oxygen? Isn't it just an icon set?"
    author: "Elias Pouloyiannis"
  - subject: "Re: Oxygen"
    date: 2005-07-16
    body: "The release will took long, but you're free to use and feel the greatness of alpha-1-2-3-x, beta-1-2-x, rc-1-2-3-x and all the other releases that will come out till the final release. You'll start having fun soon, and me too :-)"
    author: "jumpy"
  - subject: "Re: Oxygen"
    date: 2005-07-16
    body: "I hope it will be complete. As an example: I like so much Crystal Clear, it's really awensome! But it took me many days to start feeling it good, since Icons for konqueror/kmail are the 'old' CrystalSVG ones and didn't feel good placed aside the folder masterpiece icons.\nSo a theme, to stole your heart, must at least have the same style (gradients?, perspective?,...?) for every visible icon when your desktop starts.\nTo David: I'm really feeling in the mood of getting addicted to your theme :-)"
    author: "jumpy"
  - subject: "Hurd"
    date: 2005-07-17
    body: "That hurd port looks really promising :) But I can't find info how the project is improving or whats it status is. Is those debian devolopers the main developers of hurd, or are they just porting programs to it?"
    author: "Petteri"
  - subject: "Re: Hurd"
    date: 2005-07-19
    body: "http://www.gnu.org/software/hurd/hurd.html"
    author: "John Schneiderman"
  - subject: "CrystalClear Icon theme"
    date: 2005-07-21
    body: "The new CrystalClear icons are nice (if you like artsy stuff) but there is a problem.  There was a long discussion on the kde-artists list about the fact that there are no SVG files.\n\nThe general thought was that it should be policy that we shouldn't accept any totally new icons without SVG files.  But, the question wasn't resolved.\n\nI do wish that Everaldo would provide at least AI files that can be opened in Acrobat Reader for all of his new icons."
    author: "James Richard Tyrer"
---
LWN did <a href="http://lwn.net/Articles/138227/">a survey of RSS aggregators</a> featuring <a href="http://akregator.sourceforge.net/">aKregator</a>. *** Those clever Debian developers got <a href="http://lists.debian.org/debian-hurd/2005/06/msg00056.html">KDE running on HURD</a>. *** KDE Edu has a list of <a href="http://edu.kde.org/development/3.5improvements.php">improvements in 3.5</a> and the first <a href="http://edu.kde.org/development/port2kde4.php">porting to KDE 4 docs</a>. *** Customise Kicker within an inch of its life with <a href="http://wiki.kde.org/Kicker+Hacks">the Kicker Hacks</a> wiki page. *** As <a href="http://linux.slashdot.org/article.pl?sid=05/07/11/038245&tid=109&tid=106">reported on Slashdot</a>, <a href="http://www.eweek.com/article2/0,1895,1835516,00.asp">KDE was demoed at a Microsoft roadshow</a>. *** Everaldo released his <a href="http://www.kde-look.org/content/show.php?content=25668">Crystal Clear</a> icon set while David Vignoni <a href="http://davidvignoni.blogspot.com/2005/07/oxygen.html">announced Oxygen</a>. *** And finally, congratulations to the aforementioned David for <a href="http://davidvignoni.blogspot.com/2005/06/suse-contract.html">getting hired by SUSE to work on KDE</a> and to release dude Stephan Kulow for <a href="http://www.kdedevelopers.org/node/1240">getting married</a>.






<!--break-->
