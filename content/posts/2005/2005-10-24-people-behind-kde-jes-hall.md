---
title: "People Behind KDE: Jes Hall"
date:    2005-10-24
authors:
  - "tbaarda"
slug:    people-behind-kde-jes-hall
comments:
  - subject: "Diary application"
    date: 2005-10-24
    body: "\"What do you think is still badly missing in KDE?\nI'd love to see some journal/diary software.\"\n\nI'm missing that too. I have plans to start my own project regarding diaries, blogs, notes, whatever. As soon as I have more time."
    author: "Bram Schoenmakers"
  - subject: "Re: Diary application"
    date: 2005-10-24
    body: "\"I'd love to see some journal/diary software.\"\n \n>I'm missing that too.\n\nI'm not sure what features and such you need/want in such an application, but have you tried the Journal pluggin in Kontact? "
    author: "Morty"
  - subject: "Re: Diary application"
    date: 2005-10-30
    body: "the one biggest missing part in the journal plugin is the option to protect/encrypt entries. a journal is very personal. that's the biggest reason i don't use the journal plugin..."
    author: "ash"
  - subject: "Re: Diary application"
    date: 2005-10-24
    body: "Why start an own project? \n\nI just hope that journal feature in Kontact will be improved...\n\nWhy not contribute to this? :-\\"
    author: "fish"
  - subject: "Re: Diary application"
    date: 2005-10-24
    body: "Why start my own project?\n\n1. The plans I have are overkill for KOrganizer.\n\n2. I took maintainership of KDiary a couple of months ago, a diary application as Kontact plugin. To be found on kde-apps.org. But for the plans I have in mind, I cannot go on with KDiary. It would need a complete rewrite because the sourcecode is quite a mess.\n\n3. I like to start from scratch for once. I learn a lot from that.\n\n4. I can do what I want with it. No need to conform to other's existing API/policy."
    author: "Bram Schoenmakers"
  - subject: "There are some apps"
    date: 2005-10-24
    body: "What about KTagebuch (\"Tagebuch\" means diary in German):\nhttp://www.kde-apps.org/content/show.php?content=9973\n\nor KBlogger:\nhttp://www.kde-apps.org/content/show.php?content=29552"
    author: "Mario Fux"
  - subject: "Thank you!!"
    date: 2005-10-24
    body: "Hello,\n\nwell, I'm no dev, but I use KDE daily and love it. Jes, I just wanted to say thank you for your \"This Month in SVN\" - I am reading it regularly and really enjoy it. It's so much appreciated! So, thanks! Superb work! ;)\n\nbtw, \"She prefers Linus Torvalds over Richard Stallman because he is cuter and is a \"dreamer who sees beauty in the symmetry of mutually recursive functions\" is misleading, no? one could mean she says this about Linus but the question actually is \"How would you describe yourself?\" - okay okay, maybe it's just my English...haha\n\nGreetings from Switzerland and thank you tons!\n\nA happy KDE user! ;)"
    author: "fish"
  - subject: "Re: Thank you!!"
    date: 2005-10-24
    body: "Yes, you're right, I should have seen it before I submitted... Thanks for your Aufmerksamkeit :)"
    author: "Tijmen"
  - subject: "Re: Thank you!!"
    date: 2005-10-24
    body: "sry, double posts...silly me! maybe someone may delete this? @_@"
    author: "fish"
  - subject: "the \"Linus Torvalds over Richard Stallman\" thing.."
    date: 2005-10-25
    body: " I think \nit is causing unnecessary friction within the community to put a delicate matter on edge like that.\n Personally I think that making a choice between them is like choosing between apples and rollerskates, as they deal in two highly highly fields of endeavor.\n If you absolutely *have* to create personal fronts like that, I would consider \"Matthias Ettrich vs Linus Torvalds\" or \"Richard Stallman vs Eric S. Raymond\" would be more appropriate (or less inappropriate actually). And for those of you who don't know this, the former two are software-project maintainers/leaders, and the later two are advocates of a certain line of politics....\n Think of it.. would the question \"Matthias over Eric\" have made any sense?\n\n\n~Macavity\n--\n\"Open Source is *all* about upstream!\" --Mark Shuttleworth"
    author: "Macavity"
  - subject: "Re: the \"Linus Torvalds over Richard Stallman\" thing.."
    date: 2005-10-25
    body: "s/highly highly/highly different/\n\n*sigh* :-)"
    author: "Macavity"
  - subject: "Re: the \"Linus Torvalds over Richard Stallman\" thing.."
    date: 2005-10-27
    body: "Maybe this is the point of the question ? \nPerhaps it is meant as \"Do you prefer this theoretical and idealistic side of open source or the more pragmatic one ?\"\n\nThat's how I would understand it ....\n\npossebaer"
    author: "possebaer"
---
Calling herself a KDE hacker chick, she is known as the woman behind <a href="http://www.canllaith.org/articles.html">This Month in SVN</a> and as the maintainer of the <a href="http://docs.kde.org/development/en/kdebase/faq/">KDE FAQ</a>. She prefers Linus Torvalds over Richard Stallman because he is cuter and is a "dreamer who sees beauty in the symmetry of mutually recursive functions". Today's <a href="http://people.kde.nl/">People Behind KDE</a> interview is with <a href="http://people.kde.nl/jes.html">Jes Hall</a>.

<!--break-->
