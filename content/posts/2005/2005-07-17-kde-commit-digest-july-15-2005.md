---
title: "KDE Commit Digest for July 15, 2005"
date:    2005-07-17
authors:
  - "dkite"
slug:    kde-commit-digest-july-15-2005
comments:
  - subject: "Digest feature"
    date: 2005-07-17
    body: "Derek, could you please re-add a feature from the old digest layout? I'm talking about the table near the top that cross-referenced app categories with commit types.\n\nOther than that, keep up the great work. ;)"
    author: "mmebane"
  - subject: "Top Ten Statistics for the Week"
    date: 2005-07-17
    body: "This section appears empty."
    author: "Anonymous"
  - subject: "Kopete AIM Buddy Icon Support"
    date: 2005-07-17
    body: "Based upon some e-mail that I received from bug #<a href=\"http://bugs.kde.org/show_bug.cgi?id=54156\">54156</a>, I believe that Kopete now has support for AIM buddy icons.\n\nIf I could allocate some time, I would compile the latest trunk code to offer a screenshot, but I do not believe that I will be able to do so."
    author: "Matt T. Proud"
  - subject: "as always..."
    date: 2005-07-17
    body: "thanks for the digest :-))"
    author: "jumpy"
  - subject: "thanks a lot"
    date: 2005-07-18
    body: "Derek,\n\nyour Commit Digest is one of the things that makes me press \"Reload\" button more times in konqueror. It's nice to have you around here doing that good work.\n\nThanks a lot a continue with your commit digest  :)"
    author: "LuisMi Garc\u00eda"
  - subject: "Composite extension?"
    date: 2005-07-18
    body: "Today I tried to enable the composite extension, and it seems cool, but far too\nslow, and unstable... :-(\n\nMove the windows and enjoy the shadows, and see the whole kde crash,\n\nThis is with xorg from debian sid.\nI have an nividia GeForce FX 5200 and use their latest driver.\n\nWhat are other people's experiences?\nDoes anybody run xorg cvs with the improvements of Zack and Lars?\n\n\np.s.\nIn case you get an error on X startup saying that you can not enable glx and\ncomposite in the same time, add this to the \"Device\" section of your\nxorg.conf:\n\nOption          \"AllowGLXWithComposite\" \"1\"\n"
    author: "ac"
  - subject: "Re: Composite extension?"
    date: 2005-07-19
    body: "Composite is still declared experimental - for a reason."
    author: "Anonymous"
---
In this week's <a href="http://commit-digest.org/index.php?issue=jul152005">KDE Commit Digest</a> (<a href="http://commit-digest.org/index.php?issue=jul152005&all">all in one page</a>):

<a href="http://knode.sourceforge.net/">KNode</a> adds SMTP authentication.
<a href="http://developer.kde.org/~wheeler/juk.html">JuK</a> implements drag and drop of covers.
Adding support for Sky Commander controller in <a href="http://edu.kde.org/kstars/">KStars</a>.
Adding a real ACL editing GUI in KFile.
<a href="http://amarok.kde.org/">amaroK</a> adds a configuration dialog for the Helix engine.
<a href="http://kopete.kde.org/">Kopete</a> supports setting your own personal message in the MSN plugin.






<!--break-->
