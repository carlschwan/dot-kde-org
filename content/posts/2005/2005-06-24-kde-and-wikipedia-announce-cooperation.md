---
title: "KDE and Wikipedia Announce Cooperation"
date:    2005-06-24
authors:
  - "skrohlas"
slug:    kde-and-wikipedia-announce-cooperation
comments:
  - subject: "great!"
    date: 2005-06-23
    body: "This will be really great for the \"AstroInfo\" articles in KStars.  These are short, hyperlinked articles about astronomical concepts.  They are currently in the KStars handbook, but I've been thinking for a while I'd like to break them into a separate reader and make it more tightly integrated with the rest of the program.\n\nSome of the AstroInfo articles have already migrated into Wikipedia.  \n\nSeems like a good opportunity for two great projects (KDE and Wikimedia) to help each other out.\n"
    author: "LMCBoy"
  - subject: "Re: great!"
    date: 2005-06-24
    body: "Hi Jason :)\n\nOf course KStars is one of the most obvious applications where Wikipedia-Integration would make sense. Actually it was even one of the first ones mentioned when we started to think of a stronger KDE-Wikipedia cooperation. For KDE-Edu in general it's certainly a huge opportunity because we could get some translated high-quality part of the \"documentation\" integrated that way.\n\nNow we just need some generic mechanism which makes i18ned offline-reading easily possible with \"preshrinked\"/optional contents for the KDE-EDU packages.\nDanimo is working on this already.     "
    author: "Torsten Rahn"
  - subject: "Re: great!"
    date: 2005-06-24
    body: "yes, but are there free offline package of Wikipedia available? Last time I talked to danimo, that was the issue. Is it solved?"
    author: "annma"
  - subject: "Re: great!"
    date: 2005-06-24
    body: "If Wikipedia is available on CD/DVD (which it is) then I assume that offline packages should be possible (if only as cache)."
    author: "ac"
  - subject: "Re: great!"
    date: 2005-06-24
    body: "Not yet -- at least not in a suitable form for distributors and KDE.\nIdeally we would have a tool which would make it possible for packagers to specify the desired category(/ies), certain core-keywords, the language(s) and the limit for the amount of data. The tool would then create an offline package which would be tailored down to the packagers / distributors needs.\n\nOf course creating such a tool is certainly much less work for KDE than to recreate glossars for each educational application and translate and update them on our own ...   "
    author: "Torsten Rahn"
  - subject: "Re: great!"
    date: 2005-06-24
    body: "i can only agree - i hope the kstars developer(s) can join here!"
    author: "superstoned"
  - subject: "Re: great!"
    date: 2005-06-24
    body: "The Kalzium-devs already joined :)"
    author: "Carsten Niehaus"
  - subject: "Re: great!"
    date: 2005-06-25
    body: "that's wonderfull news! cool to see all this cooperation! that gives one confidence the path chosen is right..."
    author: "superstoned"
  - subject: "APIv"
    date: 2005-06-24
    body: "If you wish to follow this in the future there's an open bug for external APIs here: vhttp://bugzilla.wikimedia.org/show_bug.cgi?id=208"
    author: "\u00c6var Arnfj\u00f6r\u00f0 Bjarmason"
  - subject: "Idea for the kicker watch"
    date: 2005-06-24
    body: "A great feature would be a point in kontextmenu of the kicker watch that is called \"Show this day in history\" (or something simmilar). So that konqueror is started and shows the wikipedia entry for the current date in history.\n\nExample: It's the 24.06.2005 (I'm sorry for writing the date this way, but I'm Austrian and don't really know the American or English way :)) Klick on the menu entry and konqueror shows up with the URL: http://de.wikipedia.org/wiki/24._Juni\n\nPatrick"
    author: "Patrick Trettenbrein"
  - subject: "Re: Idea for the kicker watch"
    date: 2005-06-24
    body: "> (I'm sorry for writing the date this way, but I'm Austrian\n> and don't really know the American or English way :))\n\nYeah, I'm Austrian too, and already faced this a little bit. I think the most important non-German date formats are the international one (YYYY-MM-DD, like in SQL for example) and some English or American one (don't ask me which, but it's MM/DD/YY). I'd use the international one if I need to write dates.\n\nAnd for your idea - not bad, I like it :)"
    author: "Jakob Petsovits"
  - subject: "Re: Idea for the kicker watch"
    date: 2005-06-24
    body: "I believe American is MM/DD/YYYY and English (also Australian) is DD/MM/YYYY.\n\nL."
    author: "ltmon"
  - subject: "Re: Idea for the kicker watch"
    date: 2005-06-24
    body: "I thought the German was DD/MM/YYYY too, as a German friend told me how he was unable to buy beer in the USA as they expected his passport to have his date in the MM/DD/YYYY format.\n\nHe ended up taking a driving test to get some American ID."
    author: "richard"
  - subject: "Re: Idea for the kicker watch"
    date: 2005-06-24
    body: "The whole of Europe is DD/MM/YYYY. I think everywhere except the US is."
    author: "mikeyd"
  - subject: "[OT] Dates (was: Re: Idea for the kicker watch)"
    date: 2005-06-24
    body: "To be picky: not exactly!\n\nFor example, Germany uses dots as separator: DD.MM.YYYY\n\nAnd the European Norm is in the international one: YYYY-MM-DD\n(It is even the national norm now in many (old) E.U. countries, even if not used commonly.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Idea for the kicker watch"
    date: 2005-06-25
    body: "All of northern Europe uses (YY)YY-MM-DD."
    author: "Jonas"
  - subject: "Re: Idea for the kicker watch"
    date: 2005-06-24
    body: "Would be nice for the Kontact summary page also."
    author: "debian user"
  - subject: "Why use SOAP?"
    date: 2005-06-24
    body: "\"en:SOAP/en:WSDL is probably the best choice since it can deliver semantics (as opposed to en:XML-RPC) and is an accepted industry standard also used by e.g. google\"\n\nPlease consider REST style web services, which, IMHO, are much easier to use and more powerful. Read this article for quick introduction to REST: http://www.xfront.com/REST-Web-Services.html\n\nFor more info, there is a wiki: http://rest.blueoxen.net/"
    author: "Flavio"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "REST??  IMHO AJAX is teh cool & more appropriate.\n\nhttp://www.sagal.com/ajax/"
    author: "other ac"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "I think you've got the wrong idea.\n\nAJAX is at it's heart a presentation technology, not a web service technology.  An AJAX application may connect to a web service to retrieve data, but is not the enabling technology for the web service itself.\n\nThe main web service technologies are:\n- SOAP (venerable, complicated, powerful, standard)\n- XML-RPC (much simpler, but missing some power)\n- REST (believed by many to be the best of both worlds)\n\nL."
    author: "ltmon"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "Unfortunately for you, I'm afraid I can't express my disdain about these silly acronyms you so favor any better than the link I provided in the previous post."
    author: "other ac"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "If I understood correctly, REST isn't really a technology, it's more like a design approach to web APIs. While SOAP tries to solve everything inside SOAP itself, REST means you rely on existing mechanisms (URLs, HTTP) as much as possible.\n\nI wouldn't be surprised if KDE will choose REST eventually, just like DCOP was developed on top of existing mechanisms (ICE) when CORBA turned out to be too complicated.\n"
    author: "Maarten ter Huurne"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "> If I understood correctly, REST isn't really a technology\n\nAbsolutely correct.  Rest is what you do when tired you are.  Soap is what you use when bath you take."
    author: "KDE User"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "Yoda Talk is what you do, when you watch too much Starwars."
    author: "fp"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "Here is some information on REST: http://www.xml.com/pub/a/2004/12/01/restful-web.html\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "AJAX is not a technology *at all* - it is a buzzword co-opted by Adaptive path to describe a technique for asynchronous data retrieval by using the XMLHttpRequest pbject, that they like to think they invented.\n\nToo bad web developers (such as myself) had been doing this for years earlier.\n\nAJAX is not a protocol - the protocol is HTTP. It is not a data format - the data format is XML and/or JSON. It is just a buzzword.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Why use SOAP?"
    date: 2005-06-24
    body: "I agree completely.  SOAP is hideous compared with REST (which is actually just a buzzword; REST simply means \"use HTTP properly\").\n\n\"SOAP is probably the best choice\" sounds like somebody who isn't very familiar with web services thought that the only options were SOAP and XML-RPC, and took five minutes reading FAQs to decide that he preferred SOAP.  It does not sound like an informed, considered opinion and hopefully it will change.\n\n"
    author: "Jim"
  - subject: "agreed - REST has much more potential"
    date: 2005-06-24
    body: "Making all of the GFDL text corpus processable via REST would be a very very very good thing indeed - please see this slashdot.org comment: http://slashdot.org/comments.pl?sid=153801&cid=12901846"
    author: "A. Random Troll"
  - subject: "Translation"
    date: 2005-06-24
    body: "Hope the translation  issue also gets adressed. KDE's translation infrastructure is getting outdated.\n\n"
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "Outdated? What's your issue with it?"
    author: "ac"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "The translation project does not scale anymore. There is a huge translation lag, contributing is not as easy as it could be. And the translation infrastructure almost came to a standstill.\n\nJust look at\nhttp://i18n.kde.org/stats/gui/stable/index.php\nor even worse\nhttp://i18n.kde.org/stats/doc/stable/index.php"
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "Contributing is still as easy as it was 3 years ago, and a lot easier then it was 5 years ago. (5 years ago, you had to use an editor, like kwrite or nedit, nowadays you can use special po-editors, webbased interfaces, translation memories, etc. etc...)\n\nThe difference between the years is that the amount of strings that need to be translated is a lot larger.\nStatistics don't tell the whole story about the status of a translation team. Some teams decide not to translate all available software in cvs, but to maintain only thos apps that make sense to them. Statisticly those teams came to a standstil, while in reality they are continuing improving their current translations..\n\nAlso, looking in 'stable' does not give you the correct picture. For example, the Dutch team is translating the docs in 'trunk', not in 'stable'. And now that extragear can use branches, third party applications will probably appear in the 'stable' stats of i18n.kde.org. Not every team is translating third party apps, so the stats will go down...\n\nBesides, whatever infrastructure we will use, translating documentation and applications will always mean that you have to translate the English strings, and that is a very time consuming task...\nWhat the translation teams need is people who are willing to spent some of their precious time translating the gui and docs to their language..\n\nRinse"
    author: "Rinse"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "What the wiki way showed us was: lower entrance barriers and the project will lift up. Wikipedia reached what the closed open contribution project Nupedia failed to reach. Translation is an easy task but it is not made easy. \n\n- you have to contact the project and get involved\n\n- you have to know what po and pot files are\n\n- you have to get the right files.\n\n- you need knowledge of KDE internals, SVN\n\nWebbased tools for translations are of course a great contribution but currently third party.\n\nThere are today much more KDE users than 3 years ago. The translation infrastructure does not scale. It does not attract contributors. Community plattforms such as KDe-look or KDE-apps were also a great example what can be achieved whwn you lower entrance barriers."
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "\"The translation infrastructure does not scale.\"\n\nWhile everything else you wrote is fine and dandy (and mostly bogus in KDE's case unless you are the first translator of your mother language) what's your proof for the above statement?"
    author: "ac"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "It does not scale means: A lot of new apps are added but the \"low involvement\" task translation does not adapt in a fast way. Even major KDE languages such as German are not 100% done in the stable branch. Note that there are many KDe users in Germany. \n\nI know that some review is needed. But contributing is currently not an average joe task. Translations have still a serious lack. While programming and development in general becomes easiert the translation infrastructure was frozen in say 2002, regarded as good enough. i don't think it is.\n\n"
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "Looking at the German stats, Except for kio_svn.po and some strings in spy.po their translation of the official KDE-modules is completed.\nkio_svn.po was added after the release of kde 3.4.1 (afaik), so it's no wonder that one is untranslated. The german team has until july 17 to translate it :)\n\nLooking at the stats of the german koffice, You will notice that krita, kplato and kivio are unfinished.\nBoth krita and kivio contain a lot of strings that need more than average knowledge of the subject to translate.\nIn those cases, the fact that they are po-files is not the barrier, but being familiar with the typ of application is.\n\nI don't see how a wiki or other webbased solution can solve that.\nOne of the major issues that a wiki does not solve either is the quality of the translation in order of consistency with other parts of KDE. One can translate the hundreds of different colors in krita, or the hundreds of different stencils kivio by sucking them out of you thumb, but that would end up with a shamefull translation rather then a usefull translation.\n\nLooking back at the Dutch translation of Krita, most of the color strings are left untranslated. But you won't see that in the stats, because we copied/pasted the English phrase to the translation. If the German team also decides not to translate the named colors, but does not copy them to the translation fields, then their stats would put them behind the Dutch team, while in real life they are not. (like i said earlier, stats don't tell the whole story)\n"
    author: "Rinse"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "Forget about the stats. It is about the fact that in the current translation model translation is not the easy taks it is supposed to be. \n\nAnd documentation is really worse than GUI. The team does not scale. The question is: how long will it last to translate 100%, is the change and new string expansion faster than what a translation team contributes.\n\nWe don't want a translation lag.\n\nI understand pretty well that translation is not that easy, that it requires high skills.\n\nIt is more a recruitment issue. Open models are always superiour."
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-26
    body: "> It is about the fact that in the current translation model translation is not the easy taks it is supposed to be. \n\nBut why isn't it?\nYou don't come up with any reason of why the current model fails...\n\n>I understand pretty well that translation is not that easy, that it requires high skills.\n \n>It is more a recruitment issue\nYou won't reach more users with high skills by lowering the barier to join a team.\nIt is the opposite, to ensure that you get volunteers that are capable of doing the job right, you need to have some kind of tresshold.\n\n"
    author: "Rinse"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "<i>What the wiki way showed us was: lower entrance barriers and the project will lift up. </i>\nLooking at wikipedia.org, most Dutch translations are incomplete or outdated.\nWith wiki, it is true that the initial translation is easier, because of low entrance etc, but keeping the files up to date compared to their English equivalents is much more a pain than keeping po-files up to date.\n\n<i>you have to contact the project and get involved</i>\nNot necessary, but in general a good idea.\n\n<i>you have to know what po and pot files are</i>\nYou only need to know that your working with po-files, just like you need to know that you are working with doc-files when writing a letter in MS Word..\n\n \n<i>you have to get the right files.</i>\nwhich are available from i18n.kde.org, just click on the right file to download it.\nOr use a webbased system like pootle to organise the translation project, you can't translate the wrong file with pootle :)\n\n \n<i>you need knowledge of KDE internals, SVN</i>\nIt's alway nice to know what you are translating, so knowledge of kde is indeed necessary.\nBut SVN knowledge?\nWhy?\n\n\n<i> \n Webbased tools for translations are of course a great contribution but currently third party.\n</i>\nNot true, there are several GPL-based versions, like pootle and another one of which i forgot the name of (the nameless one is part of the kde project)\nAlso, Wiki is third party..\n\n<i>\n There are today much more KDE users than 3 years ago. The translation infrastructure does not scale. It does not attract contributors. \n</i>\nThat is not true.\nLooking at the Dutch project, while a few years ago translating was allmost a one man job, now about 10 people are translating the desktop, and especially the documentation has never been this far translated in the past (currently 70%)\n\n<i>\nCommunity platforms such as KDE-look or KDE-APS were also a great example what can be achieved when you lower entrance barriers.</i>\nYou can't compare kde-look and kde-apps with translating software.\n\nThings like wiki sound nice, but it is not in any way sufficient for translating software or docbookbased helpfiles.\n\nFirst, a wiki does not have a translation memory. Without a translating memory, you need far more translators to do the job.\n\nFor example, the documentation of digikam contains about 28458 words (tags not included). Translating that amount of words would take several months. But since most of the chapters of the digikam documentation are using the same layout, about 60% of the docs can be automaticly translated with a tool called kbabel. Besides errorprone copy&paste, I can't think of a way with wiki that a translator can automaticly translate files based on earlier work from other files.\n\nAnother benefit of using po-files for documentation translation is that you don't have to deal with most of the tags and elements found in docbook sources of the kde help files.  And the tags that are left over can be filled in with a single keystroke in kbabel.\nThat is also something I don't expect to do using a wiki-based translation.\n\nThird, small changes in the original docbook-files are easy to find without the use of a diff-application.\nJust checkout the sections in the po-files that are marked as 'fuzzy'\nNew and changed English strings are automaticly integrated in the po-files, and the translator only has to startup a po-editor like kbabel, or surf on the net to pootle, and move from fuzzy/untranslated to fuzzy/untranslated with just a single mouseclick or keystroke.\n\nAnd last but not least, most translation projects resides in countries that don't have the luxury of flat-fee broad band internet. Using a wiki as translation tool (or any other kind of webbased solution) would mean that they need to use a dialup-connection with the internet while translating, which would make translating quite an expensive job to do...\n"
    author: "Rinse"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "\"With wiki, it is true that the initial translation is easier, because of low entrance etc, but keeping the files up to date compared to their English equivalents is much more a pain than keeping po-files up to date.\"\n\nThis is a problem of diffs. English as a primary documentation source, when the english version is changed, the translation has to adapt.\n\nWikipedia articles are usually not translations, so this is different. More articles about the same topic.\n\nWith KDE documentation it is 99% direct translation with perhaps other wording. So a change in the english original triggers a possible change need in the derived document. \n\n> Things like wiki sound nice, but it is not in any way sufficient for translating software or docbookbased helpfiles.\n\nIt is not about using a wiki. It is about doing it the wiki way. Linux Professional Institute used a succesful system to get translations of articles on their website.\nt7e http://www.lpi.org/en/projects.html\n"
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-26
    body: ">Wikipedia articles are usually not translations, so this is different. More articles about the same topic.\n \nSo you agree that a 'wiki' way of translating documents is not the option.\n\n>It is not about using a wiki. It is about doing it the wiki way. Linux Professional Institute used a succesful system to get translations of articles on their website.\n\nBut this is not about websites or other 'instant publishing' mechanismen, we are talking about the translations applications and documents that have a release schedule, deadline, etc.\nIf your translation is finished, one day after the freeze, your translation is not included and won't be until the next release.\nNo wiki-kind of translation mechanism can avoid that.\n "
    author: "Rinse"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "wrong stats link. This is the important one:\nhttp://i18n.kde.org/stats/gui/stable/essential.php\n\nDoesn't look too bad to me."
    author: "cl"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "> The translation project does not scale anymore.\n\nin what ways? can you offer some examples?\n\n> There is a huge translation lag,\n\ndefine \"huge\" and is this due to available person hours, # of strings needing to be translated, of translation patches being triaged? you're being a bit vauge here =)\n\n> contributing is not as easy as it could be.\n\nwhat would you suggest for improvements?\n\n> And the translation infrastructure almost came to a standstill.\n\nwhat infrastructure was this, and when did it \"almost come to a standstill\" exactly?"
    author: "Aaron J. Seigo"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "I mean:\n\nDo it the wiki way. Imagine I had twenty minutes left and want to translate a portion of a KDE doc. I do not want to subscribe to mailing list. I do not want to learn SVN or get to know po or pot files orr tools like kbabel."
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-24
    body: "Translation of your ideas on translation:  You don't want to do a good job properly and in the proper application context.\n\nKDE needs serious and dedicated translators not people with low standards."
    author: "KDE User"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "The wikipedia way means recruiting high involvement people by low entrance barriers. The truth is that a core of 1000 supporters max writes most of the articles. it is alwways the 80-20 rule. But how do you get these people involved?\n\nTranslation is said to be a job for non-programmers but fact is it currently  requires some skills, there are unecessary barriers."
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "Translating applications are NOT simply about changing strings to a new language, it is not that easy. And you also have to do testing and debugging of your translation, which imply actually running it with the translation. Spend some time reading the translation mailinglist and you would see.\n\nBut if you really want to use those twenty minutes of yours helping with translation of KDE, it's easier than with any wiki out there. You translate the documentation, it's the most neglected or lowest priority part of the translations too. As for the easy part, you only have to supply the text. The language maintainers will gladly help with markup, and integrating it into the correct place in svn. Since its' only text you can even write it directly in your e-mail client if you like, and send it to the appropriate maintainer.\n"
    author: "Morty"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "At a wiki you can edit it on the fly. html editing is also easys but the wiki way means, you see an error and just correct it."
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "Hi can edit on the fly, that is true, but only when you are writing documentations.\nNot when you create translations of documentations.\nAn initial translation in a wiki is quite easy, but keeping it up to date according to changes in the English version is not.\nFor example, if the English author changes 6 words in a document containing 25.000 words, in order to reflect GUI-changes in the next release of KDE, how is the translator supposed to find those changes in his translation?\n\nIf you look at wikipedia.org you will notice that most localised articles are not translations of the  original English versions, most of the time they are rewrites that contain some, but not all, of the information that is available in the English version, most of the times complemented with information that is of interest to the  audience of that language.\n\nAnd again, most translated documents on wikipedia.org are outdated or contain less information then their English counterpart.\nIf wiki is so great for translating, why can't wikipedia (with a much larger audience then localised kde-versions) keep up the pace???"
    author: "rinse"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "I don't mean document editing via a wiki which might be a good proposal.\n\nI mean the principle of wiki: low entrance barrier for participation.\n\n> And again, most translated documents on wikipedia.org are outdated or contain less information then their English counterpart.\n\nGerman articles are usually independend from the English ones, not translations of their English counterparts.\n\n> An initial translation in a wiki is quite easy, but keeping it up to date according to changes in the English version is not.\n\nDiffs are your friend.\n"
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-26
    body: ">I mean the principle of wiki: low entrance barrier for participation.\n\nNot suitable for translating software\nYou need some barrier, in order to maintain quality.\n\n>Diffs are your friend.\nNo, they are not :)\nNot when it comes to translations..\nusing diffs would mean that you need 3 files, the old English file, the new English file, and the translation.\nNot very usefull...\n"
    author: "Rinse"
  - subject: "Re: Translation"
    date: 2005-06-27
    body: ">>I mean the principle of wiki: low entrance barrier for participation.\n\n>Not suitable for translating software\n>You need some barrier, in order to maintain quality.\n\nEqually unsuitable for an encyclopeadia then, since you need to maintain quality there too."
    author: "david"
  - subject: "Re: Translation"
    date: 2005-06-27
    body: "Well, you should at least keep in mind that a wiki encyclopeadia could contain errros.\nBut the difference between an wiki encyclopeadia and is that the lather uses instant publishing. You read the article, you notice an error, you change it, and it gets published as soon as you hit the 'publish' button (or similar)\n\nThis is not the case with software documentation, if you find an error and correct it, the improved docs won't be available until the next version of the application, which could take 6 months or even longer (depending on the release cycle the application uses...)\n\nSo in short, while a wiki encyclopeadia with a large audience would quite fast gain in quality, it would take a lot longer before software documentation would reach the same quality level if it was translated in the same way. \n\nAlso, users who don't upgrade everytime a new version of the application comes out, will be using the documentation with errors for a very long time..\n\n"
    author: "rinse"
  - subject: "Re: Translation"
    date: 2005-06-27
    body: "That's indeed commonly claimed to be a weakness of wikis."
    author: "ac"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "Well, if you don't want to spent time looking at the specs that the translation team of your language uses, the chance that your translation is actually useable is very small.\n20 minutes of your translation on the fly would mean 1 hour of my time trying to correct your errors in consistency, etc.\nIf that is what a new infrastructure should bring, then no thanks.\nRather have a small dedicated team that produces translations of an acceptable quality, but a bit lower quantity, then a large translation team without any structure that gives high quantity and nice stats, but no quality at all..\n\nRinse"
    author: "rinse"
  - subject: "Re: Translation"
    date: 2005-06-25
    body: "Same problem for wikipedia. Your small team cannot keep up with 3rd party applications. In wikipedia we have the quality offensive.\n\nyou can also lock parts as you can lock pages.\n\nyou can enable comments as in the wiki discussion page\n\n\n> 20 minutes of your translation on the fly would mean 1 hour of my time trying to correct your errors in consistency, etc.\n\nDon't think so. the usual process would be not to adopt the patch and say \"see this spec, improve and resubmit\".\n\nIt is all about getting people involved.   "
    author: "Andre"
  - subject: "Re: Translation"
    date: 2005-06-26
    body: ">the usual process would be not to adopt the patch and say \"see this spec, improve and resubmit\".\n\nYes, and the new, improved and resubmitted version comes in one day after the release of the software resulting in an unfinished translation shipped with the application.\nNot much different from the current situation :o)\n\nWiki is great for what it is meant for: instant publishing of articles.\nWith software, this is different, because the translation are not instantly published, but packaged with a certain release.\nSo if the translation is not 100% at release time, the application will be partially translated until the next release..\n"
    author: "Rinse"
  - subject: "Local Wikipedias"
    date: 2005-06-24
    body: "I think it would be better if it automatically redirected you to the local Wikipedia, if available, else it should fall back to the English WP.\n\n(P.S. It would be cool to integrate KWorldClock with WP, just click on a country/city/area et voila...)"
    author: "Bram Schoenmakers"
  - subject: "Re: Local Wikipedias"
    date: 2005-06-24
    body: "Actually it should behave just like specified  in your KDE settings:\nIf KDE specifies a fallback language in the Regional/Language settings then of course it should be shown (as far as it is available in your online/offline ressources). So if you have specified french as your primary language in KDE and you chose german and english as a second and third then it should also behave similar with respect to wikipedia ressources of course."
    author: "Torsten Rahn"
  - subject: "Re: Local Wikipedias"
    date: 2005-06-24
    body: "Wiki language setting should not be tight with Regional/Language setting of the entire KDE. Wiki would need a separate setting.\n"
    author: "Milan Svoboda"
  - subject: "Re: Local Wikipedias"
    date: 2005-06-25
    body: "Sorry but this is unnecessary featurecreep and nonsense."
    author: "Torsten Rahn"
  - subject: "Re: Local Wikipedias"
    date: 2005-06-25
    body: "Is there a way to add languages to this dialog in the controle centre without installing complete i18n packs?\n\nFor example, I'd understand a french wikipedia entry (and would like to read some in french) but I wouldn't ever use KDE in french or read some application's docs in french.  So normally no reason to install kde-i18n-fr.  But IIUC I would not be able to add french to the regional settings language list then, would I?\n\n"
    author: "cm"
  - subject: "Re: Local Wikipedias"
    date: 2005-06-25
    body: "Another thing that's tied to that list:  Content negotiation for the WWW.  Sometimes (if the quality of the german translation of a site is just too bad) I would like to change the precedence of the languages and put english in the first place but AFAIK that would require me to switch the language of the complete desktop... \n\nIs there a hidden setting to tweak this?  Will the Wikipedia interface have one?\n\n"
    author: "cm"
  - subject: "Re: Local Wikipedias"
    date: 2005-06-24
    body: "This is where an API would come in very handy. Currently its difficult to know what languages are a given topic is in without first choosing a language and downloading its whole page."
    author: "Ian Monroe"
  - subject: "Mediawiki specific?"
    date: 2005-06-24
    body: "Hopefully this will be more broader than just Wikipedia, mayby mediawiki specific? There are so many other good wikis :) For example kdevelop could use http://gpwiki.org/ for help pages when doing game programming etc.\n\nThis project sound really exiting can't wait for the new amarok to arrive to Debian."
    author: "Petteri"
  - subject: "Re: Mediawiki specific?"
    date: 2005-06-24
    body: "Yes, whatever will be done will be a MediaWiki plugin and not special Wikipedia service."
    author: "\u00c6var Arnfj\u00f6r\u00f0 Bjarmason"
  - subject: "this is cool"
    date: 2005-06-24
    body: "I got nothing really useful to say, other than this is really cool.  I think this could add a lot of power and neat features to many applications, and the KDE desktop in general."
    author: "Bojan"
  - subject: "Wikipedia in amaroK sounds nice, but..."
    date: 2005-06-24
    body: "...when do we get a Wikilyrics? A Google search shows one with this name, but the site appears to be dead. Wikipedia already has lots of info on albums, it would be nice if it could have lyrics, too."
    author: "mmebane"
  - subject: "Re: Wikipedia in amaroK sounds nice, but..."
    date: 2005-06-24
    body: "For copyright reasons, I don't think you could ever have that."
    author: "KDE User"
  - subject: "Re: Wikipedia in amaroK sounds nice, but..."
    date: 2005-06-24
    body: "Amarok already uses http://lyrc.com.ar/ to get lyrics."
    author: "Jim"
  - subject: "generic api?"
    date: 2005-06-24
    body: "Would this api be specific to KDE? or can any application use the webservice (like a gtk app, windows app, osx app, etc). It seems to me that this is just a webservice and I am confused as to why kde is involved."
    author: "Derek Slovin"
  - subject: "Re: generic api?"
    date: 2005-06-24
    body: "> Would this api be specific to KDE?\n\nThe Wikipedia one? No. The one within kdelibs? Sure.\n\n> I am confused as to why kde is involved.\n\nKDE is the one to initiate this and the first one to use it. \n\nRemember, KDE - the desktop with momentum. ;-)"
    author: "Anonymous"
  - subject: "Re: generic api?"
    date: 2005-06-24
    body: "Yes, others will be able to use the mediawiki API. Its just that a KDE app, amaroK, is already showing how Wikipedia can be used in applications.\n\nIt appears a KDE API is also being planned, to make it easy for any KDE app to insert Mediawiki content (basically a wrapper around the webservice, perhaps with offline features). That would be specific to KDE."
    author: "Ian Monroe"
  - subject: "Wikihelp?"
    date: 2005-06-24
    body: "Could the help system use this?\nObviously, there would need to be a mechanism for storing a local minimal version during install, but then this API could be used to keep the help up to date.\nThis would also allow the help by way of wiki to include more tips and tricks and such."
    author: "Zot"
  - subject: "Re: Wikihelp?"
    date: 2005-06-24
    body: "Sounds like a great idea.\nI think that using a wiki to write help/docs would speed this process up a lot. Adding help would also be easier (no need for a svn account and command line tools)"
    author: "Sepp"
---
Today Jimmy Wales, chairman of the <a href="http://www.wikimedia.org/">Wikimedia Foundation</a>, announced the beginning of a <a href="http://meta.wikimedia.org/wiki/KDE_and_Wikipedia">cooperation between Wikimedia and the KDE project</a> at <a href="http://www.linuxtag.org">LinuxTag</a> in Karlsruhe, Germany. As the first applications, like the media player <a href="http://amarok.kde.org">amaroK</a>, start to integrate Wikipedia content the idea is to create a webservice API to access the information from Wikimedia projects such as <a href="http://www.wikipedia.org">Wikipedia</a> or <a href="http://www.wiktionary.org">Wiktionary</a>. There are also plans for a KDE API.









<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex"><img width="180" height="150" src="http://static.kdenews.org/jr/amarok-wikipaedia.png" /><br />Wikipedia in amaroK</div>
<p>The API would allow KDE applications to easily embed Wikimedia content, data could even be fetched from a local database depending on your online/offline status. First progress can be seen in <a href="http://de.wikipedia.org/wiki/Benutzer:Danimo/Knowledge">Knowledge</a>, a Qt 4 based offline reader for Wikipedia.</p>

<p>Jimmy was also searching for people who want to help with the design of this API, so if you want a good API for your application join the efforts!</p>








