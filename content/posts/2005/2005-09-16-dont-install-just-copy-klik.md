---
title: "Don't Install, Just Copy with klik"
date:    2005-09-16
authors:
  - "kpfeifle"
slug:    dont-install-just-copy-klik
comments:
  - subject: "Wow..."
    date: 2005-09-16
    body: "Wow. It looks like a really neat idea. It could use a little polishing, for example having multiple sites to look for the download for an app. That way if one is down there wont be any problems with downloading as it will just use another one. Great job.\n\nTake that, linspire....\n\nMark"
    author: "Mark Watson"
  - subject: "Re: Wow..."
    date: 2005-09-16
    body: "Perhaps instead of multiple sites, now is the time to move to an easy torrent set-up for doing this kind of klik installer. have a script (or a set of scripts) that builds the klik, the torrent, and an rss. Likewise, it should also handle the update issue. Perhaps, when you load the klik on your system, an rss url is included that can be loaded into a local update app to track the what and where. That way, if an update does occur your system finds out about it."
    author: "a.c."
  - subject: "Re: Wow..."
    date: 2005-09-17
    body: "Do you _really_ want to run software from random torrent peers?"
    author: "paranoid user"
  - subject: "Re: Wow..."
    date: 2005-09-17
    body: "This shouldn't matter, as long as the source of the .torrent file is trustworthy. In theory at least. But IIRC bittorrent uses md4 (not md5 or sha1) checksums, so maybe it's really not as tamper-proof as it could be."
    author: "uddw"
  - subject: "Re: Wow..."
    date: 2005-09-16
    body: "why are you addressing Linspire there? \n\nFab"
    author: "Fab"
  - subject: "Re: Wow..."
    date: 2005-09-17
    body: "yeah, there is no reason why it won't work in linspire... on the other hand, linspire has one-click, too - so i guess that is what he wanted to say :D"
    author: "superstoned"
  - subject: "That is \u00fcber-coolishness!"
    date: 2005-09-16
    body: "Booaaaaaaaaah!\n\nThis is brilliant!\n\nReading the article first, I thought it was an April Fool's joke gone late. But then I tried it with the klik://ooo2 link -- and boy!, was I suprised. (Really nice splash screen made by the Novell artists, BTW.) This thingie runs wonderfully. I'm still wondering how they did it (not really understanding Kurt's explanations about what makes klik tick...)."
    author: "ac"
  - subject: "Libs?"
    date: 2005-09-16
    body: "How would this work with libs? are they compiled into the cmg-file so there wont be any dependencies?"
    author: "Adam Tulinius"
  - subject: "Re: Libs?"
    date: 2005-09-16
    body: "Yes, the cmg will include required libraries.  What is required in the cmg is determined by assuming all target systems will have a certain base set of packages.  Anything required beyond those base packages (or which requires a newer version then can safely be assumed to exist) is included within the cmg making it quite self contained.   The klik server is aware of the full installed package set for many of it's supported distributions and can provide quite small (but less portable) packages for them using \"serverside-apt\".  For unknown distributions the server defaults to assuming that the target system will have a \"lowest common denominator\" of packages provided by the supported distributions (including debian sarge), e.g. the cmg will only assume the target system has the packages installed which are in all supported distros, and only at the lowest version in any supported distro."
    author: "Niall Walsh"
  - subject: "Re: Libs?"
    date: 2005-09-16
    body: "To complement it, you can find more information on klik's architecture here:\n\nhttp://klik.atekon.de/docs/"
    author: "Christian Loose"
  - subject: "Easy enough from my Grandma!"
    date: 2005-09-16
    body: "1 yr. ago I taught my 71 aged Grandma how to use Kanotix + email/KMail + webbrowsing/Konqueror. She really enjoyed it.\n\n3 months ago she phoned my and complained: Why hadn't I told her about that easy one-click installation method of new software?!\n\nI didnt get what she wanted to explain to me. She got angry and hang up, when I told her this was impossible...\n\nLast month we went to visit her. She showed me how to do it. You can imagine that my jaws dropped 10+ inches. I must have displayed the most stupid face I ever made in my life.\n\nSo, yes -- klik rulez! And it was introduced to my not by this nice article, but by my beloved Grandma... "
    author: "Gary Silverman"
  - subject: "Re: Easy enough from my Grandma!"
    date: 2005-09-16
    body: "excellent points\n\nIn fact, the aoutopackage folks, for this very reason of package consistency and maintainability, only recommend autopackage for certain, almost self contained packages (think firefox, open-office, games)\n\ncheers!\n"
    author: "MandrakeUser"
  - subject: "Re: Easy enough from my Grandma!"
    date: 2005-09-16
    body: "sorry, I meant to post this in liquidat's thread below"
    author: "MandrakeUser"
  - subject: "Re: Easy enough from my Grandma!"
    date: 2005-09-16
    body: "Does your Grandma read the Dot to? "
    author: "Mark Watson"
  - subject: "Re: Easy enough from my Grandma!"
    date: 2005-09-17
    body: "guess so :D"
    author: "superstoned"
  - subject: "Cool"
    date: 2005-09-16
    body: "Would it be difficult to get this working on slackware ? \n\nThis could really be interesting, instant beta testing. No need to explain people how to get the svn version of KTorrent, just klik and try it.\n\nBtw, You have the knoppix and kanotix torrents mixed up."
    author: "Joris Guisson"
  - subject: "Re: Cool"
    date: 2005-09-16
    body: "### \"difficult to get this working on slackware ?\"\n----\n\nNo. Not at all.\n\nAre you volunteering?\n\n\n### \"knoppix and kanotix torrents mixed up.\"\n---\n\nRats! (I'll try to see if a dot editor can fix that. Thanks for the hint.)"
    author: "Kurt Pfeifle"
  - subject: "Re: Cool"
    date: 2005-09-16
    body: "\"knoppix and kanotix torrents mixed up\" are fixed now."
    author: "ac"
  - subject: "Re: Cool"
    date: 2005-09-16
    body: "To busy with KTorrent and I start to work next week, so not enough time."
    author: "Joris Guisson"
  - subject: "Re: Cool"
    date: 2005-09-17
    body: "Have you already tried klik://ktorrent ? ;-)"
    author: "probono"
  - subject: "Re: Cool"
    date: 2005-09-17
    body: "on my slackware it works pretty well:\nit must be payd attention only on two little things:\n-check that you have a kernel with loopback filesystem and cramfs compiled in\n-it helps but not necessary copying /sbin/pidof to /bin/pidof"
    author: "mart"
  - subject: "Autopackage?"
    date: 2005-09-16
    body: "Sounds nice, but after reading the faq, http://klik.atekon.de/docs/?page=FAQ, there are still some questions open:\n\n- Is there an update system integrated? If I can install >20 packages I am not able to look after updates, especially if I am a normal user!\n- What is, if the installed version replaces an old, system wide installation? Which version will be used?\n- Where is the difference to the autopackage system? What is better, was is worse?\n\n...\n\nliquidat"
    author: "liquidat"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "### \"Is there an update system integrated?\"\n----\n\nNo. You'll have to look yourself for updates.\n\nHowever, some of the latest packages include version numbers in their names. So it would be easy to \"install\" (it is just copy, really) the \"krita-1.4.88.cmg\" side by side with \"krita-1.4.0.cmg\" and \"krita-2005.17.09.cmg\" and even run all 3 in parallel.\n\n### \"If I can install >20 packages I am not able to look after updates, especially if I am a normal user!\"\n---- \n\nMay be true.\n\nHowever, my own idea about klik is to use it primarily as a very efficient tool to help experienced users to testdrive development versions of KDE packages without them needing to compile themselvs, and without the distro packages needing to provide weekly snapshot RPMs and .debs (which they don't do, anyways).\n\nLet's see how the idea is accepted. If it gets enough support, someone may even think of hacking the Makefiles so that there is a \"make cmg\" target available for everyone who compiles KDE apps on his own host.\n\n\n### \" What is, if the installed version replaces an old, system wide installation?\"\n----\n\nA klik .cmg file never replaces an installed old, system wide software. It is there as an alternative.\n\nYou can run the system wide installed app side by side with the klik version. (The only conflict you may need to watch out for is that both may write into the same $HOME/.yourapp and $KDEHOME/somewhere files and directories with conflicting content...\n\n\n### \"Which version will be used?\"\n----\n\nklik may have put its apps into your K menu. But even there it does not replace the installed app's entry. To the system, it looks like a separate app. "
    author: "Kurt Pfeifle"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Great article and great tool, thx. I'm kliking ooo2 ;-).\n\nBut I think the problem with applications writing to the same config files could become a real problem for some persons.\n\nAnyway, great tool, but how do I uninstall it?"
    author: "Mario Fux"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "### \"great tool, but how do I uninstall it?\"\n----\n\nWhy should you ever want to uninstall it in the first place?  ;-P\n\nSeriously: to get rid of everything klik installed (including possible K menu entries), running these commands should accomplish it:\n\nrm $HOME/{.klik,.zAppRun} \\ \n$HOME/Desktop/*.cmg \\\n{$KDEHOME,$HOME/.kde}/share/services/klik.protocol \\ \n{$KDEHOME,$HOME/.kde}/share/applnk/klik/{klik.desktop,.directory} \\\n{$KDEHOME,$HOME/.kde}/share/mimelnk/all/cmg.desktop \\\n{$KDEHOME,$HOME/.kde}/share/applnk/.hidden/AppRun.desktop \\\n\nrm -rf /tmp/{app,klik}\n\n\nLast, edit /etc/fstab to remove the /tmp/app/{1,2,3,4,5,6,7} mountpoints.\n\n\nCheers,\nKurt\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "> ### \"Is there an update system integrated?\"\n> ----\n> \n> No. You'll have to look yourself for updates.\n\nWould it be possible to integrate this system with the GetHotNewStuff system? If I remember correctly, that is set up to allow for a central server that shows versions of files and everything so that the end user can see, in a list, what is already installed on their system, what is installed on their system and could be updated, and what is not installed on their system at all.\n\nOf course, I haven't particularly used GHNS of Klik, so I wouldn't have any idea what I'm talking about, but I much prefer the idea of Klik if it makes not only installing, but also updating as easy as clicking a button. Also, I'm always a fan of reusing technologies, so..."
    author: "jameth"
  - subject: "Re: Autopackage?"
    date: 2005-09-17
    body: "I think it's a good idea, and i've used both :D\n\nyeah, i think it should work. maybe this is the solution to the problem of c++ applets/plasmoids... and to the problem of installing kde applications. I think, and some others do so to, kde itself should be smaller, and the additional applications should be installable easilly. if possible with an KDE interface."
    author: "superstoned"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "The update system is to klik/install the latest version of any application you want.\n\nYou run the program from the cmg normally by clicking on it, typing the command name at the command line will use the normal system installed version.   Your system is uneffected, only the application you have installed, when run from the cmg, will see any alterations.\n\nAutopackage is completely different imho.  Autopackage is still a normal system installation tool, klik is not.   Klik is a complimentary technology to normal system installation tools/processes, which uses them to take care of the things they do best (dependencies) but provides an alternative way to use the information and software they hold. "
    author: "Niall Walsh"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Correct me if I'm wrong, but klik seems to be more like Apple's Fat Binaries. A great solution for a specific problem (as in distributing testing / nightly versions of software in this case). It takes care of dependencies by putting all libs, but an agreed upon base set, in the package and you just stick it wherever you want and run it. \n\nThe aims of Autopackage are different/broader as it seeks to be a solution for distributing end user stable versions, without duplication of Libs and ultimately integrating it with the native distro package system be that tar.gz, deb, rpm or whatever else. It also a non-root user install locally and then informs the root user so he/she can install it globally if he/she thinks its pertinent. Actually those are just some highlights that I find important for me :) The Autopackage aproach is way more complicated, both to implement and to get working right, as there are, due to it's complexity, more points of failure. BUT... it solves a greater set of software distribution problems than klik.\n\nMy evaluation: Klik seems great for the job it was created (running testing/nightly builds on a tester/developer machine) THANKS GUYS. To install software on a non-testing machine, where things need to be installed globally, not duplicate lots of libs, allow easy updates & integrate well with the distro, there's Autopackage. \n\nI have followed the Autopackage mailing list for many monts now and it is really exciting to see how they are solving the problems that crop up. Before I started reading the mailing list I had a vague idea that Linux Distros worked very differently and where using different incompatible versions of basic libs, but now that I see the guys at Autopackage solving so many things to be able to achieve their goal, (making relocatable binaries, dealing with different File structures and config systems, packaging systems etc) I see their efforts in a totally different light. It's a difficult task, but they have done very well so far.\n\nCongrats on y'alls achievements with klik. It will make my complusive nightly testing and cutting edge feature admiring much easier ;)\n\nCheers, \n\nAndreas\nMontevideo, Uruguay"
    author: "Andreas Hennig"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Yes, it is more like an \"Apple Fat Binary\" then an autopackage.\n\nI was unanaware (or had forgotten) that autopackage was usable by normal users to install software without administrative priveleges.   I guess that puts klik and autopackage into \"slightly\" more competition which is never a bad thing :-)   That being said, they address different needs, as you have pointed out, and have a fundamental difference, klik bundles dependencies and allows the \"installation\" of an application to have zero impact on your system (ok it might add a menu entry and maybe an icon to your system).\n\nAs I now understand it in user mode with autopackage the installation of one newer package could pull in updates which adversely imapct other software you have running.   The flip-side is that autopackage can pull in underlying library updates which can see improvements in other applications.   This is not trying to say klik is better then autopackage, just that they address different issues so comparisons can only really be made carefully.   One updates a software system, the other simply makes programs available to run on a system.  Horses for courses.\n\nAs I understand it if autopackage was successfully generally adopted by packagers , klik would have few if any problems using autopackages as the sources for it's cmg files."
    author: "Niall Walsh"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "I think you have explained it well, Autopackage is still in the process of solving certain issues (although it's very usefull already). And I agree with you on you last statement, I don't have a clue if the developers have thought about that, They might. I think it would be cool if it was possible for developers to easily create both types of packages simultaneously. That would be great. I think having both types of packages created and available for a software would be very profitable. \n\nCheers, \n\nAndreas\n\n"
    author: "Andreas Hennig"
  - subject: "Re: Autopackage?"
    date: 2005-09-19
    body: "\"ok it might add a menu entry and maybe an icon to your system\"\n\nWhile I do not mind the addition of an icon or configuration files to my system, I find the menu entry a bit intrusive. While one can safely ignore the addition of an icon or configuration file, menu items are a different story. They are more difficult to remove (for end-users), particularly since klik doesn't have \"uninstall\" options. If the purpose is to avoid a \"true\" installation, then it shouldn't have a visible effect (when it isn't running)... Any behavior that is somewhat permanant is a \"bug\", IMHO.\n\nHence, I have a suggestion. Would it be possible to either:\nA) Create a \"subsystem\" within KDE that automatically removes any \"cruft\" that this would add to the system (over time)? Eg. When I delete the klick package, it could automatically remove any .desktop items that point to it/etc.\nB) Instead of adding menu entries, could it be integrated via plasmoids? (KDE 4) It would be great to have a plasmoid that stores your last N klick downloads. For KDE 3.5 this could be implemented by either putting icons on the dekstop, or creating a kicker applet).\n\nI don't really know if either of the above solutions are implementable. However, if one of them (or something similar) would be implemented, this system would definitely be a nice complement to autopackage."
    author: "Zachary Jensen"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "I agree with almost all you have said, but I wouldn't limit the scope of klik as much as you did. I think it's also interesting for end-users.\n\nJust a few use-cases, where I think that klik would be useful:\n\na) You want to try-out a new version of an application without removing the current installation.\nb) You want to try-out a new application but you want to make sure that you can remove it cleanly without disturbing the rest of the system.\nc) You are on a multi-user machine and really want to use this one application that the sysadmin doesn't care about. :)\n\nBesides the amount of duplicate libraries really depends on the definition of the base libraries. When you see Qt and kdelibs as part of the base system than the duplication is IMHO negligible."
    author: "Christian Loose"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "How about that application where nobody has bothered yet to make a package for?\nOr just because it's so damn easy to click on a link and start using the app?"
    author: "Quintesse"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Of course! My list wasn't meant to be exhaustive. :)"
    author: "Christian Loose"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "You're totaly right... my comment was not so much about what klik can do...  but rather about how Autopackage complements Klik or vise-versa. \n\nScenario 1. I am the Admin\n\nI can even think of a situation where I would use klik to test, check out the feature set  or just generally check out a new tool I am thinking of deploying, and then (assuming both klik and autopackages are available... would be nice) once I want to deploy it for the rest of the users, I'd just get an autopackage and install globally, which would give me a few other advantages (as mentioned in earlier post). Or get the rpm, deb or whatever the native package format is.\n\nScenrio 2. I am just Joe User (or Andi user in my case :)\n\nI want to run a tool my Admin does not have available, and probably won't provide... I get the Klik package and I am done, or I can get the Autopacke which also works, BUT:  More than likely though, with me being one of those guys who likes to carry everything with Him (yes, I got a notebook... but the battery is dead, the powersupply is dying, I live in South America and got married recently - i.e. I can't afford a new one), I'd just put the Klik package on my USB stick and carry the tool with me... ;) Can't do that with Autopackage...\n\nSo, really In my earlier post I was just trying to clarify some more the usage of Autopackage (which is what I know more about) and not as much to explore the multiple uses of Klik.\n\nI think there is some overlap in possible usage cases, but I still think that they are sufficiently different that a developer might want to publish binaries of all builds (nightly, alpha, beta, rc and final) with Klik and then maybe only publish final builds or maybe beta, rc and finals with Autopackage. I believe it's definitely not an either - or type scenario, but rather a matter of using the right package type for the right job and/or target audience.\n\nCheers."
    author: "Andreas Hennig"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "<blockquote>Can't do that with Autopackage...</blockquote>\nYou can create sealed installers with Autopackage, as well as stick the Autopackage support code on a USB drive, and then you have a stand-alone Autopackage. :)\n\nOr maybe I completely missed what you meant. :)\n\n-- \nTaj"
    author: "Taj Morton"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Taj, \n\nthanks for correcting me... that's exactly what I meant and I was wrong. That's why I sent a message to the Autopackage mailing list asking everyone to come over and join in the discussion, so the developers themselves would comment on Autopackage and correct any missrepresentation on my part. \n\nGreat to see you guys are joining in! You are the ones qualified to talk about Autopackage!! I just try and spread the word, though my understanding might not be complete.\n\nThanks!!\n\nAndreas"
    author: "Andreas Hennig"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Taj, \n\nthanks for correcting me... that's exactly what I meant and I was wrong. That's why I sent a message to the Autopackage mailing list asking everyone to come over and join in the discussion, so the developers themselves would comment on Autopackage and correct any missrepresentation on my part. \n\nGreat to see you guys are joining in! You are the ones qualified to talk about Autopackage!! I just try and spread the word, though my understanding might not be complete.\n\nThanks!!\n\nAndreas"
    author: "Andreas Hennig"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Taj, \n\nthanks for correcting me... that's exactly what I meant and I was wrong. That's why I sent a message to the Autopackage mailing list asking everyone to come over and join in the discussion, so the developers themselves would comment on Autopackage and correct any missrepresentation on my part. \n\nGreat to see you guys are joining in! You are the ones qualified to talk about Autopackage!! I just try and spread the word, though my understanding might not be complete.\n\nThanks!!\n\nAndreas"
    author: "Andreas Hennig"
  - subject: "Re: Autopackage?"
    date: 2005-09-16
    body: "Autopackage can do c). As for a and b, parallel-installs are slated for 1.1, but they're not implemented yet.\n\nPatches welcome!\n\n-- \nTaj"
    author: "Taj Morton"
  - subject: "Re: Autopackage?"
    date: 2005-09-17
    body: "It would be nice to see the packaging tool create a torrent file, and an RSS file. Then when adding a klik, it can be registered in a DB with the RSS url. When needed, it simply torrents it down. In fact, the tool could be set-up to do autodownload (nice for nightly downloads) with the use of torrents (but the file could be palced in a different directory until the user has had time to approve the install). That way, most of the downloads occur in the first night (lots of systems to work with)."
    author: "a.c."
  - subject: "Debian"
    date: 2005-09-16
    body: "Debian not on supported distributions lists? Sound kind of strange since all those distributions debian derivatives? Am I missing something here or what?"
    author: "Petteri"
  - subject: "Re: Debian"
    date: 2005-09-16
    body: "### \"(....) works for Live CD distributions (Knoppix, Kanotix) as well as for Debian, Linspire, Ubuntu, Kubuntu and openSUSE/SUSE Linux 10.0.\"\n----\n\nThis looks to me like I listed Debian amongst the supported distributions... ;-P\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Debian"
    date: 2005-09-16
    body: "Ok. Sorry missed that one while looking at this page (which lists supported distributions):\nhttp://klik.atekon.de/docs/?page=How%20to%20use\n\nBut thanks for sorting the misunderstanding :)"
    author: "Petteri"
  - subject: "Re: Debian support, but a sarge/etch .deb?"
    date: 2005-09-17
    body: "There is support, but I like to maintain my debian system\nby apt-get'ing (now aptitude'ing) packages when possible.\nThis way they're tracked via dpkg --get-selections, and I\ncan minimize items I need to track should I need to perform\na bare-metal reinstall.\n\nI heard a 'slh' on #kanotix (irc.freenode.net) has .debs, but\nit would be nice to pick them up from klik or backports.org\n(or the like).\n"
    author: "Krishna Sethuraman"
  - subject: "Unbelievable! there's even a enlightenment package"
    date: 2005-09-16
    body: "I can't believe my eyes!\n\nJust out of curiousity, I installed the klik client (very nice background info in Kurt's article, I really appriciated to read it and follow it through), and visited the mentioned openSUSE repository in .NZ to see what it had to offer.\n\nEnlightenment poked my interest.\n\nI run klik://enlightenment, and the popping up dialog informed me that it had discovered a SUSE 10.0 system, and it wanted to download 18 MByte of software. I confirmed, and 3 minutes later I had a georgious E17 desktop starting inside an Xnest window. All fully automatically! No installation, no configuration! I can't believe it!"
    author: "e17 fan"
  - subject: "Re: Unbelievable! there's even a enlightenment package"
    date: 2005-09-16
    body: "### \"(...) a georgious E17 desktop starting inside an Xnest window.\"\n----\n\n;-)\n\nYes, I was myself pretty impressed with the enlightenment package probono created a few days ago within 2-3 hours after it was requested in the #klik IRC channel.\n\nIt used indeed Xnest at first. \n\nI suggested to use \"nxagent\" instead of Xnest (because it can be resized, and doesnt have a fixed window size, like Xnest), but unfortunately the current version has nxagent from 1.4.0 included (which does *not* support resize).\n\nSo I hacked my local .cmg myself. It was pretty easy to de-compress the .cmg into a directory tree, take out the old version of nxagent (including the old nx libs) and put in the 1.5.0 version (including the nx libs).\n\nSo my local enlightenment.cmg now starts up in a window that I can resize at will. And coolest thing: it even obeys the [CTRL]+[ALT]+[F] shortcut to toggle the window into fullscreen mode and back!\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Unbelievable! there's even a enlightenment package"
    date: 2005-09-16
    body: "Thats freaking awesome...  and maybe a little creepy!"
    author: "Corbin"
  - subject: "Re: Unbelievable! there's even a enlightenment package"
    date: 2005-09-17
    body: "yeah, this is frightening... the MS ppl won't like this. now they took most of plasma's ideas into their 'gadgets' but this will be hard for them to do :D\n\nthey still don't have a centralized way of packagemanagement (and especially upgrading)..."
    author: "superstoned"
  - subject: "Pb with online klik store link"
    date: 2005-09-16
    body: "\nThere's a problem with the online klik store link, atekon has been replaced by artekon  ;)\n\nHope a dot editor sees that."
    author: "Anonymous"
  - subject: "Re: Pb with online klik store link"
    date: 2005-09-16
    body: "Fixed\n"
    author: "ac"
  - subject: "Re: Pb with online klik store link"
    date: 2005-09-22
    body: "And it's down to, perhaps due to too many hits ..."
    author: "dan"
  - subject: "Magic!"
    date: 2005-09-16
    body: "\nYes, your friends from OpenUsability.org really appreciate that service! We usability people sometimes have problems installing the latest developers version of an application, so we welcome smart workarounds like klik.\n\nI've tried klik for Skype and I was amazed how easy it is to install a fully functional application - and especially how easy it is to deinstall it later: You simply delete the skype icon from your desktop. It's magic!\n\nFor single applications, klik is a real alternative to the NX technology as keyboard shortcuts, clipboard etc. work as usual. For evaluating the whole desktop, however, NX is superiour (plus klik is not yet able to deliver the whole DE, I think).\n\nFrom an OpenUsability point of view, it would be cool to have klik for non-KDE applications, too :)\n\nThanks guys and go on developing such cool tools!\n\n/el\n\n"
    author: "Ellen Reitmayr"
  - subject: "Re: Magic!"
    date: 2005-09-16
    body: "\n\naahh! \n\nI didn't fully read the article (as I knew the technology before) \n\n-> there ARE many kliks for non-KDE apps\n\nwonderful :)"
    author: "Ellen Reitmayr"
  - subject: "Great must be made a standard"
    date: 2005-09-16
    body: "This is an awesome thing that for me will help a lot porting commercial apps without bothering of package building (or hating rpm-only distribution made by some vendor) but to have success this must be supported by other DEs like Gnome and xfce (and also enlightenment) ROX already supports AppDirs, so if AppDirs (or compressed AppDirs) become standard they could help a lot distributing software under linux (or BSD) especially commercial ones (which most of the times are staically linked so there are no dependencies problems)\nGreat work!"
    author: "ra1n"
  - subject: "mimetypes"
    date: 2005-09-16
    body: "Wow, this looks great. What I missed in the description is if this also works for kpart plugins, ie. does it register mimetypes/servicetypes?\n"
    author: "koos"
  - subject: "Re: mimetypes"
    date: 2005-09-16
    body: "### \"kpart plugins\" ?\n----\n\nwaitaminutewaitaminutewaitaminute!\n\nYou can't have all at once. But development certainly is not yet finished! New developers are welcome. Come by and visit us in #klik on Freenode.net."
    author: "Kurt Pfeifle"
  - subject: "Re: mimetypes"
    date: 2005-09-16
    body: "klik can set them up for the application contained in the cmg, but it does not touch the world outside the cmg (this is by design)."
    author: "probono"
  - subject: "Re: mimetypes"
    date: 2005-09-16
    body: "If it works for KDE applications it is very likely expanding KDEDIRS as the KDE application rely on KStandardDirs to find their own resources.\n\nIf an application installs a MIME type into \"its own\" KDEDIR equivalent it should be visible to the rest of KDE as well "
    author: "Kevin Krammer"
  - subject: "Clean"
    date: 2005-09-16
    body: "It would be great also if instead of putting the application .config files into my homedir, they would be put into their own little space, maybe in the installation file itself. I really don't like having tons of .something into my homedir for every app I try."
    author: "Anonymous"
  - subject: "Gentoo"
    date: 2005-09-16
    body: "Right now, it seems like gentoo isn't supported. Is there any change for gentoo to get supported?"
    author: "mOrPhie"
  - subject: "Re: Gentoo"
    date: 2005-09-16
    body: "yep i am using gentoo too"
    author: "ch"
  - subject: "Re: Gentoo"
    date: 2005-09-16
    body: "I'm running Gentoo and the example app worked perfectly for me.  I also tried smb4k though that didn't work... :-/"
    author: "Corbin"
  - subject: "Re: Gentoo"
    date: 2005-09-16
    body: "I get \"/tmp/apps/1/wrapper\" no such file or directory on gentoo, but on my debian-PC it works great. :)"
    author: "mOrPhie"
  - subject: "Re: Gentoo"
    date: 2005-09-16
    body: "Please open konsole and run manually:\n~/.zAppRun ~/Desktop/xxxxxx.cmg\n\nThen watch for error messages, that might give you a hint. Please join #klik if you need further support ;-)\n"
    author: "probono"
  - subject: "Re: Gentoo"
    date: 2005-09-17
    body: "Try another one.  I get that error with freedoom and some others, but like frozen bubbles works perfectly along with bzflag and some others.  Right now its about 50% of them work flawlessly for me."
    author: "Corbin"
  - subject: "Re: Gentoo"
    date: 2005-09-19
    body: "You need cramfs installed as a kernel module (and cloop to?)  "
    author: "Sam Weber"
  - subject: "Re: Gentoo"
    date: 2005-09-19
    body: "yes, it's part of most distros"
    author: "probono"
  - subject: "Re: Gentoo"
    date: 2005-09-16
    body: "It work for me, but you have to had cramfs installed."
    author: "n:ag"
  - subject: "Excellent"
    date: 2005-09-16
    body: "This kind of thing is exactly what Linux needs.  Every application should be installed this way."
    author: "Spy Hunter"
  - subject: "Re: Excellent"
    date: 2005-09-17
    body: "nice idea, but this is just not the scope of Klik. it would be inefficient if every application would have a certain amount of dependencies (amount getting larger, the lower you get into the 'chain' of applications - if you say only base apps are installed, every 'klik' would need lots of basic libraries, and be very large on disk and memory). the native packagemanagement software or autopackage should cover this.\n\nas said before, the scope of this is limited, but cool :D"
    author: "superstoned"
  - subject: "Re: Excellent"
    date: 2005-09-17
    body: "Personally, I don't care about a reasonable amount of inefficiency.  My computer is more powerful than I really need; it's time to put that power to work doing *something* useful.  I would accept double the memory consumption and disk space usage in all my applications if it would enable something as cool and useful as this.  If a little inefficiency is the price to pay for the complete elimination of all packaging and installation problems across all distros, I say go for it!  Of course the old way of packaging will always still be available for those who can't stand a single wasted resource."
    author: "Spy Hunter"
  - subject: "Re: Excellent"
    date: 2005-09-17
    body: "I personally would definitely _not_ want every application installed this way. This would lead to a system with dozens of different versions of libraries stored in dozens of different application directories, wasting space, computer resources with unoptimized builds and unnecessarily large waste of disk space (I don't care how cheap the disks are, I wouldn't want to waste my money even if I were a billionaire).\n\nI could go on listing situations where a system built fully based on klik would not be desireable. The point is, of course there are situations where klik can simply RULE, it's nice, I love it, really. Still, I wouldn't use it to install anything on my machines, I'd use it for quick-testing alpha/beta software then quick removal, but nothing more.\n\nThe situation is different of course when you want to provide a very easy and quick setup of applications for 6packjoes, for whom some waste of bandwidth, space and efficiency is not a big pay for having a simple installation method.\n\nCutting this short, klik is great, but one should not loose the focus and apply a seemingly nice solution to all problems, even for some which don't even exist yet."
    author: "Levente"
  - subject: "Management"
    date: 2005-09-16
    body: "It would be nice if there was some way you could manage what things you have install (maybe though klik:/config or something?).  Also I think 'klik:/' should take you to the klik page with all the downloads."
    author: "Corbin"
  - subject: "MIME type of recipes?"
    date: 2005-09-16
    body: "I am afraid I am not up to date with klik but when I discovered it about a year ago it was bound to the URI scheme and did't yet offer a MIME type for the recipes.\n\nThe drawback of the situation back than was that the server, or more generic the source, of the package couldn't be transported along with the recipe as the part of the klik URI where the hostname would usually be is occupied by the package name.\n\nBeing bound to the URI had also the drawback of being only accessible to browser that can be configured for additional protocols.\n\nIts definitely a nice thing to have this easy to remember URIs additionally, though"
    author: "Kevin Krammer"
  - subject: "Does klik://plasma work?"
    date: 2005-09-16
    body: "Does it work if I want to install Plasma with a klik://plasma link? (Cannot test now myself, sitting in front of a WinXP box visiting my brother...)\n\nAnd if there is not yet a klik package for Plasma, will Aaron \"Jack-Of-All-Trades\" Seigo provide one?\n\nWill there be a CMG package of Zack Rusin's EXA drivers/X extension so I can do a simple klik://exa ?\n\nWill klik become an integrated part of KDE 4?\n\nAre there plans to make klik:// a full KDE KIO slave (coded in C++ instead of Shell) and with more configuration options?"
    author: "plasmoide"
  - subject: "Re: Does klik://plasma work?"
    date: 2005-09-16
    body: "As Plasma still is in it's infancy I don't think there are anything really interesting to install yet, unless you are one of its developers:-)\n\nAnd from what I read of Klik I don't think it's intentional use are those fundamental system parts/libraries like Plasma and even more EXA. It's more targeted for use with higher level stuff, like applications. But maybe it  perhaps could be used for Plasma extentions, what I think they call plasmoids.\n "
    author: "Morty"
  - subject: "Re: Does klik://plasma work?"
    date: 2005-09-17
    body: "most plasmoids are scripts, based on java, python or ruby, thus easilly installable. but for the c++ plasmoids, autopackage or klik would be nice."
    author: "superstoned"
  - subject: "Well Done Kurt"
    date: 2005-09-16
    body: "I know you speak from experience with the packaging mess that NoMachine has for different versions of Linux. This is a perennial problem that *does* need to be solved."
    author: "Segedunum"
  - subject: "Re: Well Done Kurt"
    date: 2005-09-16
    body: "klik://nxclient"
    author: "probono"
  - subject: "DCC"
    date: 2005-09-16
    body: "I already knew about klik, and I am really excited about this way to get (not so) simple packages with one click :)\n\nI have one question: How does this relate to the freshly announced DCC?*\n\nI read Knoppix, Linspire, Xandros and many others have joined the DCC, so it should be easier to simply target one ...\"common core\" indeed. Is this correct?\n\nIf this is the case, well I'm simply amazed, think about what klick could become for the entire OSS community in the next months...\n\n... i want klik URL's on kde-apps.org! :)\n\nKudos\n\n* That's Debian Common Core, but don't tell the debian dev's ;)"
    author: "Coolio"
  - subject: "Re: DCC"
    date: 2005-09-16
    body: "DCC should indeed help. I don't know whether they will specify a set of \"base packages\" that will be installed on every DCC-compliant system, but that would help even more."
    author: "probono"
  - subject: "Re: DCC"
    date: 2005-09-16
    body: "It's more like a common set of libraries taken from sarge + 2 or 3 packages to make it LSB compliant.\n\nThe \"core\" is already downloadable and \"weighs\" about 350MB:\nhttp://archive.progeny.com/progeny/linux/iso-i386/\n\nannouncement:\nhttp://lists.dccalliance.org/pipermail/dcc-devel/2005-September/000116.html"
    author: "Coolio"
  - subject: "Re: DCC"
    date: 2005-09-16
    body: "This is a start, but where is KDE?"
    author: "probono"
  - subject: "Re: DCC"
    date: 2005-09-16
    body: "heh, i guess somwhere in the repositories...\n\nI don't know if the common core is or will be extended to KDE or other non-system librarier, but this is their very first step, and it's a step in the right direction IMHO."
    author: "Coolio"
  - subject: "QA possibilities"
    date: 2005-09-16
    body: "My big attraction to this is that with a single click, i could run last night's konsole (e.g.,) build every morning, and if there's a problem, I can file the bug *and move back to the known working version of konsole* immediately.  This would close the loop on the qa process, tighten it to a single day, and make it nonintrusive to getting my work done.\n\nThis lets me 'slipstream' QA into my normal work routine -- and when I run into a serious bug, I can resume with only a minor interruption to file a bug and restart my working version of the app.\n\nI currently run MacOSX Firefox nightlies this way.  It's valuable knowing that the bugs I file are timely and relevant to the current state of development."
    author: "Krishna Sethuraman"
  - subject: "Great article, great klik app!"
    date: 2005-09-16
    body: "I am blown away by ease of use of klik://scribus-latest\n\nIt give me Scribus-1.3.1cvs up and running on  Kubuntu within 2 minute, with one click only! After I close Scribus again (I successfull printd Scribus tutorial found on kde-files.org), I am ask for feedback, but stupid I cancel the dialog, without filling it. So here is my feedback to klik inventors:\n\nThis is so revolutionary for everyone: developer, user, developer-helpers (beta-tester, translator, documentor, artist).\n\nDidnt know about klik, but I am Knoppix user since 2 years. Great stuff!\n\nI look around the klik website to see if they publish their feedback somehow. Discoverd it: they feed it even *live* into this page (50 latest comment):\n\n. -->   http://klik.atekon.de/comments.php\n\nIt is amazing t watch the various comments coming in now. Dunno if it was this article. Seems like a *lot* of people try it klik now.\n\nThank you very much for klik."
    author: "Beneditto"
  - subject: "played with it a little"
    date: 2005-09-16
    body: "while I'm entusiast about this, I think that cmg images and AppDirs should be natively supported by kde, klik it's great, but I don't want to depend on it, I want to download a .cmg image mount it and drop my app (AppDir) where I want, like in MacOS.\nAgain dependencies are a problem, but it could be simple to create a way to install needed libs with AppDirs (ROX already does it)"
    author: "ra1n"
  - subject: "Re: played with it a little"
    date: 2005-09-16
    body: "klik also does include the required libs in the cmg. In fact, a cmg is nothing but a ROX AppDir inside a cramfs image."
    author: "probono"
  - subject: "Brilliant!"
    date: 2005-09-17
    body: "What a fantastic implementation of a very interesting idea! Works well here on my Gentoo system.\n\nIt might be nice if later on the disconnect between a systems' own package manager and klik could be bridged over, so the user could choose at some point to properly install the application."
    author: "Paul Eggleton"
  - subject: "Re: Brilliant!"
    date: 2005-09-17
    body: "If you run a Debian based system you could just install the .deb that the cmg image is generated from (and any others that it depends on that you don't already have that is)."
    author: "Corbin"
  - subject: "The point about klik:  .cmg vs. .deb installations"
    date: 2005-09-19
    body: "\"If you run a Debian based system you could just install the .deb \nthat the cmg image is generated from (and any others that it depends \non that you don't already have that is).\"\n------\nYou didn't get the point of klik then....\n\n*Of course* you could install the .deb. But what would it do? It would change your base system. The .deb would install its files into your /usr, /bin, /sbin,... whatever directories, additionally drawing in possibly many dependencies (installing more), and on the way replace the previous version of that application(s). You couldn't revert easily to the previous state of your system.\n\nThe .cmgs are self-contained. They don't touch your /usr & /bin & /sbin... They do not replace your system's applications. Never. The cmg AppDir files are designed to be run side-by-side with your existing apps (which are under the reign of some package manager). And they are designed to also get rid of them, easily: just delete one file. Your package manager will not be affected in any way."
    author: "Kurt Pfeifle"
  - subject: "Re: The point about klik:  .cmg vs. .deb installations"
    date: 2005-09-20
    body: "No thats not what I meant.  I was replying to:\n\n\" It might be nice if later on the disconnect between a systems' own package manager and klik could be bridged over, so the user could choose at some point to properly install the application.\"\n\nI didn't meant for it to sound like I was saying 'so what?' to klik, I think klik is awesome (I have an icon on my desktop that launches the klik .cmg image of Frozen Bubble).  I run Gentoo so I find being able to just 'klik' and run a program (like a little games like Frozen Bubble) very good to see if I want to actually install the application later (since compiling takes a long time).  The primary advantage of installing it using the distribution's package system would be that it would be automatically updated whenever you update the system. *hint hint: feature request for klik*"
    author: "Corbin"
  - subject: "Interesting statistics"
    date: 2005-09-17
    body: "hi,\n\njust to say klik is an extremely interesting technology -- just what i have wanted already a long time ago!\n\nthe klik website has an interesting statistic showing how popular each of the applications offered are (download numbers), and how successfull the users where in installation.\n\nit is here:\n\nhttp://klik.atekon.de/popular.php\n\nAurelio"
    author: "Aurelio"
  - subject: "FUSE is coming, no more root password !"
    date: 2005-09-17
    body: "I don't really like the idea of modifiying /etc/fstab, so I'm looking forward to see an implementation working with FUSE (actually merged in what will become the 2.6.14 linux kernel).\n\nThat way no root access will be needed, and every app will be mountable under ~/.kde or ~/pub etc...\n\nAnyway, klik is a very useful piece of software, congrats !"
    author: "Arnaud"
  - subject: "Sounds nice, but its just a Zero Install rip off"
    date: 2005-09-17
    body: "There's this old one: \"Anyone who doesn't know Unix is doomed to re-invent it, badly.\"\n\nSame can be said for Zero Install. I've see quite a few idea on that path (AppDirs, fat binaries, auto package, auto installers) and none can hold a candle to Zero Install (http://zero-install.sourceforge.net).\n\nIt basically offers everything that \"klik\" offers above and also includes:\n* Bundling libraries (so you don't have to worry about supported distros)\n* Managing library versions (avoids DLL hell)\n* Auto-updates software versions as required\n* cross desktop support as no special \"handlers\" are required\n* Downloads are cached across the system (multiple users don't need to download the same program multiple times)\n\nIt also offers some interesting security features. I do suggest that people start to do some research before duplicating efforts."
    author: "Guss"
  - subject: "Re: Sounds nice, but its just a Zero Install rip off"
    date: 2005-09-17
    body: "klik follows the \"1 application = 1 file\" paradigm, 0install does not. "
    author: "probono"
  - subject: "Re: Sounds nice, but its just a Zero Install rip off"
    date: 2005-09-25
    body: "Like a lot of technologists, you completely missed the point. \n\nThe issue isn't how it does its magic but how it presents it to the user. The idea of using the file manager/web browser paradigm that the user is familiar with to also (easily) manage software installations is the main issue here, not the technological nitty-gritty details of how you do it.\n\nKlik also cheats a bit by hacking on support for its \"one file = one application\" paradigm into Konqueror. The fact that its very easy to do not withstanding. How does it intergrate for users who run ROX on their K desktop, or even - god forbid - nautilus ? Currently it doesn't and as I read it there aren't any plans on making it so or even providing a framework that would work for new and unlisted file managers. This is the major down side of Klik - it does not fix the fact that the OS itself does not support internally the neat idea described above, instead it bolts on support on a per file manager implementation.\n\n0install has one better - its integrated into the kernel. You don't need to support each individual file manager, you can use whatever you are most comfortable with and get away with it. Heck, you can even use the command line if you so prefer. \n\n0install of course supports the file management based software install/uninstall, it just have its own way of doing it (which IMHO is better).  As a fringe benefit to the nitty-gritty details of how 0install implements the paradigm (vs. how Klik does it), that with 0install you can install and run a software on your computer just by clicking a special link on a web page (its not that special, it simply a link to a local 0install powered file path that doesn't exist yet) - with all the required warning dialogs of course."
    author: "Oded Arbel"
  - subject: "Re: Sounds nice, but its just a Zero Install rip o"
    date: 2005-09-25
    body: "With the important limitation that the 0install kernel hack is just for one particular kernel, afaik there aren't port to kernels of other OS (and undoable for OS which have a closed source kernel)."
    author: "ac"
  - subject: "Re: Sounds nice, but its just a Zero Install rip off"
    date: 2005-09-17
    body: "There's one huge difference between zero install and klik.  Klik is dead simple to install.  Zero install needs a kernel module and a bunch of other garbage, klik is one command.  I've always wanted to try zero install but the install is too much hassle.  Klik rocks!"
    author: "leo spalteholz"
  - subject: "Re: Sounds nice, but its just a Zero Install rip o"
    date: 2005-09-19
    body: "If you don't like 0install you can try Injector (http://0install.net/injector.html) from the same author as 0install.\n\nBye\nPiero\n"
    author: "Piero"
  - subject: "Re: Sounds nice, but its just a Zero Install rip o"
    date: 2005-09-19
    body: "i like the zero install concept tooo !\ni hope it will be integrated with FUSE , if it goes in 2.6.14\nand i hope too , that one system will be selected , not that we have 10 new , zero install systems ....\n\ni would like to have one where i could install the complete kde 4 svn , to start developing right away....\n\nlike klik:/kde4svn , oder 0install klick on kde4svn.--\nklick on kdevelop , new project -> open directory where you want to kontribute , and start coding ....\n\nch"
    author: "ch"
  - subject: "Re: Sounds nice, but its just a Zero Install rip o"
    date: 2005-09-19
    body: "Heh, why stop there??!!\n\nWhat you really need is something along the lines of:\n\n'klik:/code_cool_new_kde4_feature_and_call_it_my_own_and_make_patch_and_email_to_kde_core_devel_and_collect_fame_and_glory' \n\nNow, _THAT'D_ be a really useful app...!"
    author: "manyoso"
  - subject: "Re: Sounds nice, but its just a Zero Install rip o"
    date: 2005-09-20
    body: "The old version of Zero Install needed a kernel module and a daemon process loaded at startup. The new version requires neither; it's pure Python and you don't even need to be root to install it (unlike Klik). Tarballs and packages are here:\n\nhttp://0install.net/injector.html\n\nPlease give it a go if you found the previous version too difficult, as we've made it a lot easier now."
    author: "Thomas Leonard"
  - subject: "Re: Sounds nice, but its just a Zero Install rip o"
    date: 2005-09-20
    body: "Ah.. Thank's for the heads up!  I will definitely try it out now..  As so often, one tries some software, forms an opinion, and then neglects to try the newer versions.  "
    author: "Leo"
  - subject: "Sure, that's great ..."
    date: 2005-09-22
    body: "Now if only more kde stuff was available under it :-)  I tried it,\nit looks to work very smoothly."
    author: "Krishna Sethuraman"
  - subject: "P2P"
    date: 2005-09-17
    body: "If I got it right, when clicking on a lick link, you will download the apps, and it will be saved in your desktop and you will be able to run it without installing, right?\n\nIf that's the case, it's a good idea to integrate a p2p network on it, such as bittorrent, so that once it's popular, the servers aren't down because of too much people downloading, or you start getting problems of connection. It would be nive to kind of force people using p2p in this case.\n"
    author: "Iuri Fiedoruk"
  - subject: "Why mount image files?"
    date: 2005-09-17
    body: "The idea is good. But I don't get why you need to mount an image to run an app, it could work just as well with a normal directory, say a MyApp.app directory which would be automatically recognized as an application, and no overhead of mounting an image file."
    author: "Me"
  - subject: "Re: Why mount image files?"
    date: 2005-09-17
    body: "Having AppDir support in KDE would be nice, actually I have asked for it quite some time ago on http://bugs.kde.org/show_bug.cgi?id=81772\n\nHaving said this, cmg still has some advantages over AppDirs:\n\n* You can simply e-mail a file, but not a folder.\n* A cmg file is compressed, thus saving ~50% disk space.\n\nBtw, a cmg file is nothing but an AppDir packaged in a cramfs file.\nYou can easily unpack it with /sbin/cramfsck -x which gives you a ROX-compliant AppDir."
    author: "probono"
  - subject: "autopackage"
    date: 2005-09-17
    body: "when you look at the autopackage vision, the \"vision\" is almost the same, the implementation was bullied by several persons. Klik makes only sense when it is available and working on all major distributions.\n\nhttp://autopackage.org/ui-vision.html"
    author: "Aranea"
  - subject: "klik://KDE 3.5 svn "
    date: 2005-09-17
    body: "friend Kurt Pfeifle, you have really made some good point. klik is the answer to  the Linux Package Hell!!! Linux Standard Base must include klik for package management. (we need to improve klik for Global and local (user) software installation).\n\nAlso if klik could do installation from source like in BSD then it would be the standard package management tool for GNU/Linux OS! Thank you.\n\nI'm tired of running kdesvn-build to get one or more errors to beta test kde 3.5.\n\nit takes around 2 days to compile the whole KDE 3.5 svn, and most of the time kdenetwork, kdelibs, kdebase, or extragear stuff get errors.\n\nNow, with klik life of kde users like me who would like to test and enjoy latest kde 3.5 (or kde 4.x) would become easy.\n\nPlease consider klik://kde_3.5_latest, so that it becomes easier.\n\nnon-kde packages get installed in slackware nicely.\n\nktorrent shows libqt-mt.so.3 is not found. (but i have my custom compiled qt-copy with kde 3.5svn)."
    author: "fast_rizwaan"
  - subject: "Re: klik://KDE 3.5 svn "
    date: 2005-09-17
    body: "libqt-mt.so.3 error should be fixed."
    author: "probono"
  - subject: "ktorrent 1.1rc1 works with slackware 10.1"
    date: 2005-09-18
    body: "with kde 3.4.1 and qt 3.3.4 of klax, ktorrent works file with klik. I love klik :)"
    author: "fast_rizwaan"
  - subject: "Can you imagine..."
    date: 2005-09-17
    body: "Can you imagine that this is what I'm doing on windows since, what?, 10 years. The installation of application under Linux is probably the only thing that refrains me from using it. I really wish success for that king of approch."
    author: "me"
  - subject: "Re: Can you imagine..."
    date: 2005-09-17
    body: "You must have a secret version of Windows... ;-) Mine puts stuff everywhere in random locations and a monster file called the \"registry\" - everything gets intermingled so that you can't take an installed application and move it over to another computer.\n\nWitk klik, every application is one single file that you can easily move, mail, or delete. "
    author: "probono"
  - subject: "Re: Can you imagine..."
    date: 2005-09-17
    body: "But this sounds like the autopackage vision. So I hope klik is not KDE-only"
    author: "gerd"
  - subject: "Our Windows is much dumber! [was: Can you imagine]"
    date: 2005-09-17
    body: "### \"Can you imagine that this is what I'm doing on windows\"\n----\n\nHonestly, I can't.\n\nHave you got a URL/link? Where can I get and test this Wonder Windows? I want to buy a license (rather, let my boss buy one), even if it is double as expensive to the ones MS sold us so far.\n\n*Our* Windows doesn't follow the simple \"1 file == 1 application\" principle that klik implements. Our Windows cannot remove any application by removing just 1 file. Our Windows does not allow us to copy 1 file to a different Windows computer and then have the application run there. Our Windows does not allow us to mail the application (remember, 1 file!) to a friend so he can run it. Our Windows does ask for money for most applications we want to install on top of it. Our Windows does not give us the freedom to modify most of the applications that are installed on it. Our Windows has an \"installer\" that writes many, many files to multiple places when installing an application, and a fat \"Registry\" with many secret keys that keeps growing with each installation as well as with each de-installation.\n\nOur Windows is too dumb for klik.\n\nOur Windows does not have anything that even comes close to klik."
    author: "Kurt Pfeifle"
  - subject: "Re: Our Windows is much dumber! [was: Can you imag"
    date: 2005-09-17
    body: "Okay, \n\nbut could you update us on registry issues. It was considered to use Elektra as a KDE4 registry. The common consensus was: not a registry is bad per se, but win reguistry is a pain.\n\nWhat's the difference between klik and the autopackage vision?"
    author: "gerd"
  - subject: "Re: Our Windows is much dumber! [was: Can you imag"
    date: 2005-09-19
    body: "autopackage does an install. klik does not."
    author: "superstoned"
  - subject: "Re: Our Windows is much dumber!"
    date: 2005-10-03
    body: "> Our Windows does not allow us to copy 1 file to a different Windows computer\n> and then have the application run there. Our Windows does not allow us to \n> mail the application (remember, 1 file!) to a friend so he can run it.\n\nActually it does! That file is called a \"worm\" and it is exactly why klik will never be installed in my Linux workstations.\nReally, people, are we still trying to let end-users execute applications from e-mail messages or floppies/flashcards coming by untrusted PCs???? No lessons taken from YEARS of viruses, worms and trojans? Very, very sad...\n"
    author: "Vajsravana"
  - subject: "Re: Can you imagine..."
    date: 2005-09-17
    body: "By me\n\nDo not take my post too \"seriously\". When I am saying on windows it's easy to install an application, I mean how to get the application, and how to install it.\n\nWhat the installer is doing behind you back and what it is necessarely to be able to run the fresh installed app is one another thing. This too often brings problems. I agree with you.\n\nHaving said this, it is possible to write good applications on win. I'm using Python and wxPython for coding, I create exe with py2exe and eventually, but not necessarely, a setup.exe with Inno Setup. Once installed, all the files are in a directory (the app + Python + wxPython lib). No entries in the registry. The application can be removed just by deleting the dir containing the app. If the app has been installed with a tool like InnoSetup, the app can be removed with the windows add/remove win utility.\n\nMy (GPLed) applications are available in two forms, as Python scripts and as exe. Believe me or not, the exe's are more downloaded than the script versions. \n\nI'm still convinced, what an end user wishes is something that it easy to install. They do not care if the libs are statically linked (as explained above) or dynamically linked.\n\nBTW, wxPython is the kind of app, I *never* succeed to install on any Linux distro. Of course, it is doable. But this is simply too much for me. On windows,\nI install it with wxPython-setup.exe :-).\n\nYou see, I really wish a brilliant success for you klik installer. I think this is the way to go.\n\nJust a question at the end, what do you think about the \"pbi way \" of the PCBSD project?\n\nRegards\n"
    author: "me"
  - subject: "Re: Can you imagine..."
    date: 2005-09-28
    body: "\"BTW, wxPython is the kind of app, I *never* succeed to install on any Linux distro. Of course, it is doable. But this is simply too much for me. On windows,\nI install it with wxPython-setup.exe :-).\"\n\nHave you tried Gentoo? \"emerge wxpython\" does the trick.\n\n/ceel"
    author: "ceel"
  - subject: "Re: Can you imagine..."
    date: 2005-10-23
    body: ">Once installed, all the files are in a directory (the app + Python + wxPython >lib). No entries in the registry. The application can be removed just by deleting >the dir containing the app. If the app has been installed with a tool like >InnoSetup, the app can be removed with the windows add/remove win utility.\n\n...can you do the same things with MS Office? I'm wotking with kliked ooo2...."
    author: "Minkiux"
  - subject: "Just one thing"
    date: 2005-09-17
    body: "Thank you... *sigh* I think I'm going to cry..."
    author: "Starfish"
  - subject: "amd64, etc. support?"
    date: 2005-09-17
    body: "Are the repositories x86 specific?  If I'm on an amd64 system, can I reasonably except many of the links to work? I'm assuming that a 64bit cmg file would work fine once compiled into the cmg, however I cannot tell what architecture the cmg file contains at first glance..."
    author: "Troy Unrau"
  - subject: "Re: amd64, etc. support?"
    date: 2005-09-17
    body: "Currently it's all x86, but there is no limitation in the architecture that would prevent us from doing the same for 64bit as well. One could even think of \"universal binary\" cmg files that would run natively on both architectures, just like Apple does now."
    author: "probono"
  - subject: "Re: amd64, etc. support?"
    date: 2005-09-17
    body: "I didn't even think about that.. I'm running Suse 10 RC1 for amd64 and so far all the klik packages work awesome.  So at least Suse10 has no issues running the i386 klik cmgs on an amd64 OS."
    author: "leo spalteholz"
  - subject: "not quite"
    date: 2005-09-18
    body: "Unlike Mac OSX, you have to use specific protocol klik://\nwhich checks for libs on your computer and creates files.\n\nI might be wrong, though.\n\nSince I cant go on internet in Linux it is useless for me.\nBut if someone finds this great, then well... great.\n\n"
    author: "a"
  - subject: "Re: not quite"
    date: 2005-09-18
    body: "the klik:// protocol is only for when you do not already have a .cmg file -- anyone can post a cmg file on their website and it can process over http, etc."
    author: "Backwards"
  - subject: "Re: not quite"
    date: 2005-09-18
    body: "And how can you get cmg file?"
    author: "a"
  - subject: "Re: not quite"
    date: 2005-09-20
    body: "USB, cdrom, automatically from repository, download by hand, you name it..."
    author: "superstoned"
  - subject: "Re: not quite"
    date: 2005-09-19
    body: "If Linux is \"useless\" for you, what are you doing here? And if you can't get on the net, how can you post here?"
    author: "Janne"
  - subject: "Re: not quite"
    date: 2005-09-20
    body: ">If Linux is \"useless\" for you, what are you doing here?\n\n\"...Since I cant go on internet in Linux _it_ is useless for me.\"\nI didnt' say Linux is useless for me.\n\n>And if you can't get on the net, how can you post here?\nNot all people connect on the internet from Linux.\n\nIf I can download package from windows, please someone \npost link to the web page. For example Firefox package on klik site.\n\n(When it gets back.)\n"
    author: "a"
  - subject: "Re: not quite"
    date: 2005-09-20
    body: "http://opensuse.linux.co.nz/klik/10.0/firefox_1.6a1.cmg"
    author: "probono"
  - subject: "Re: not quite"
    date: 2005-09-21
    body: "Thanks."
    author: "a"
  - subject: "Please support Slackware!!!"
    date: 2005-09-18
    body: "If you don't support Slackware and other popular distros like fedora core, Mandriva, then you are doing great unjustice to them.\n\nYou have added support for OpenSUSE, why? Is openSuSe a debian based distro? I guess not. \n\nbtw, many applications work with slackware, so you can (and you must) support non-debian distro alike.\n\nwhen installing some software, klik can get libraries from slackware ftp mirrors. \n\nAlso how do I make a .cmg file for say slackware (i would compile with ./configure --enable-static) so that it would work on other systems too?"
    author: "fast_rizwaan"
  - subject: "Re: Please support Slackware!!!"
    date: 2005-09-18
    body: "### If you don't support Slackware and other popular distros like \n### fedora core, Mandriva, then you are doing great unjustice to them.\n.......\nDude -- this is Free Software. This doesn't mean it grows on trees, just hanging there for you to harvest. Creating Free Software also involves hard work before it can be shipped to your convenience. And have you ever thought that the developers can only start with support for those systems they run themselves? Have you ever thought, that a student developer may not own dozens of boxen, running dozen of distros, in dozen of sub-versions, and has the time to maintain them all?\n\nDude -- this is Free Software. This means *also* that the developers are free to do what *they* like. -- Who are you to play the guy trying to boss them around? Who are you to be so impolite and demanding things from developers who are working on this in their own time, on their own money??\n\nDude -- this is Free Software. This means *also* that *you* are free to add what is missing for *your* needs. Or pay someone to add it for you, if you lack the skills yourself. And if you can't do it, can't pay for it, be at least polite. This very often helps, and you *may* still get it in the end...\n\nDude -- you're an obnoxious, impolite egoist polluting this forum. Go away.\n\n----------------\n### You have added support for OpenSUSE, why? Is openSuSe a debian \n### based distro? I guess not. \n\nSo?\n\nDude -- if you were a bit intelligent, you'd have discovered that openSUSE support was added only recently. You would see this as a sign of klik development now going into its next stage, starting to support non .deb based distros. You would be thankful for that, and celebrate it. You would probably apply a bit of politeness to your question about when one could expect support for even more non .deb based distros.\n\n-----------------\n### so you can (and you must) support non-debian distro alike.\n\nDude -- the developers \"must\" nothing. Not even when an impolite non-Gentleman calling himself fast_rizwaan starts demanding things.\n"
    author: "ac"
  - subject: "Re: Please support Slackware!!!"
    date: 2005-09-18
    body: "Calm down.. He was just asking.  \nBut yes, it's probably a manpower issue, not an idealogical one."
    author: "leo spalteholz"
  - subject: "Re: Please support Slackware!!!"
    date: 2005-09-19
    body: "funny, that you got annoyed by my opinion.\n\nklik is a nice concept (and a reality now), and it MUST be available to all GNU/Linux distros, klik can bring ease of use to all GNU/Linux users! that's why it is a MUST!!!\n\nI can help create .cmg but there's no howto for that!\n\n>Dude -- you're an obnoxious, impolite egoist polluting this forum. Go away.\n\nYou (ac) are welcoming, polite, humble, cleaning this forum . welcome to KDE ;)\n\nand you know opposite attracts ;)\n"
    author: "fast_rizwaan"
  - subject: "Re: Please support Slackware!!!"
    date: 2005-09-18
    body: "We'll try our best... please come to #klik on irc.freenode.net if you would like to help (testing)."
    author: "probono"
  - subject: "equals to Java WebStart"
    date: 2005-09-18
    body: "The klik seems very similar to WebStart, insn\u00b4t it ?"
    author: "Felipe Ga\u00facho"
  - subject: "hopefully klik-apps are run in a secure sandbox?"
    date: 2005-09-19
    body: "Really - what we dont need is another Windows-Thingy.\n\nAll those thoughtless users that install anything they can do with a single click is the real problem of Windows itself.\n\nBe i paranoid? I mount all my rw-partitions with noexec (and would even disallow source script.sh but dont know how for myself at my own box).\n\nReason: all the buffer overruns discovered in the last months.\nIt is just a matter of time that a virus is distributed that erases your home-partion, with a single klick...\n\nThats why i say: those apps should be run in a sandbox with read-only-rights at least."
    author: "gcf"
  - subject: "Re: hopefully klik-apps are run in a secure sandbox?"
    date: 2005-09-19
    body: "klik operates under the rights of the local user, unlike rpm and apt which require root. klik does nothing that wouldn't be possible without it - every time you install software from the web you run the same risk. And remember, klik:// doesn't run stuff from random places, an attacker cannot set up his own klik:// links. That being said, I am open for improvement suggestions."
    author: "probono"
  - subject: "Re: hopefully klik-apps are run in a secure sandbox?"
    date: 2005-09-19
    body: "> klik operates under the rights of the local user, \n\n\nWhich is already enough to destroy valuable data or to create an email worm that works by tricking the user into executing it.  There are many examples of this in the Windows world.  And the more non-geek users the Linux desktop gets the easier this will be. \n\n\n\n> klik does nothing that wouldn't be possible without it - every time you \n> install software from the web you run the same risk.\n\n\nI agree.  But klik makes it easier to shoot oneself in the foot. :)\nThat's the downside of being user-friendly. \n\n\n\n> That being said, I am open for improvement suggestions.\n\nI think (embedded?) gpg signatures for the images and a configurable list of trusted keys would be a good idea.  That way for example a translator would be able to define \"I trust klik images from <author of the app I'm about to translate>\" but no one else's.\n\nKlik and HotNewStuff (amarok uses this feature for downloading of Python scripts already, for example) are places where it's appropriate to be extra paranoid, IMHO.  \n\nI agree that the fact that the download location for this stuff is restricted already helps a lot.  But signing of the packages would add an indicator that packages have been tampered with, should the download server ever be compromised (which happened to the Debian project at least once), or if the images are distributed by Email or other means.  Of course this would only help if the images are signed offline by the author and in their final state. This could be a problem when the images are created on-the-fly, I guess. \n\n\n"
    author: "cm"
  - subject: "Re: hopefully klik-apps are run in a secure sandbo"
    date: 2005-10-23
    body: ">> klik does nothing that wouldn't be possible without it - every time you\n>> install software from the web you run the same risk.\n\n\n> I agree. But klik makes it easier to shoot oneself in the foot. :)\n> That's the downside of being user-friendly. \n\n...so we use complicate installation/run method, so we got reduced security risk..."
    author: "Minkiux"
  - subject: "Re: hopefully klik-apps are run in a secure sandbox?"
    date: 2005-12-08
    body: "it would be really good to be able to run a klik program as nobody"
    author: "JohnFlux"
  - subject: "Debian Stable? Dependendies"
    date: 2005-09-19
    body: "Is there a way to make klik work on Debian stable (sarge)? This would be The Thing for stable on the desktop; just klik-install bleeding edge desktop stuff, and remove it if it breaks things, while the core system remains rock solid. But if it depends on new stuff in ubuntu etc, then it won't work...\n\nAnyway, how does klik handle the dependency issue? Does every app include its libraries? Or is there a predefined set of versions which klikified apps are compiled against?"
    author: "Michael Ramendik"
  - subject: "Re: Debian Stable? Dependendies"
    date: 2005-09-19
    body: "The problem with debian is that you can never know what is in the \"base system\". That is why klik officially supports only debian-derived distributions, where the set of packages that come with the \"base system\" is known.\n\nBut of course, there is nothing in klik that prevents it from being used on sarge. You just have to make sure that you have a reasonable set of base system libraries installed (easy with apt-get)."
    author: "probono"
  - subject: "WTF?"
    date: 2005-09-20
    body: "<i>The problem with debian is that you can never know what is in the \"base system\".</i>\n???\nAll the packages with priority = base?\nCould you please elaborate on this? O was I punk'd?"
    author: "Humberto Massa"
  - subject: "Downloading on another machine?"
    date: 2005-09-19
    body: "Is there a way to convert a \"klik://\" into an ordinary URL, so I can download it on another machine (with a better or cheaper network connection)? That machine may even run Windows, but it has wget."
    author: "Michael Ramendik"
  - subject: "Re: Downloading on another machine?"
    date: 2005-09-19
    body: "No, since most cmg files are dynamically created on-the-fly on the client using existing ftp archives. (One could probably use klik on Windows for creating cmg files...)\n\nBut once you have a cmg, you can ftp/mail/... it. "
    author: "probono"
  - subject: "Can not get to http://klik.atekon.de"
    date: 2005-09-19
    body: "The site does not work for me."
    author: "Chris Parker"
  - subject: "Re: Can not get to http://klik.atekon.de"
    date: 2005-09-19
    body: "Indeed, it worked just fine for me yesterday. I had a good look around and tried it out on OpenSuse10 RC1. Today it is not available at all :("
    author: "Rhakios"
  - subject: "Re: Can not get to http://klik.atekon.de"
    date: 2005-09-19
    body: "There is a DNS issue of our provider. Please visit #klik for a workaround in the meantime."
    author: "probono"
  - subject: "Re: Can not get to http://klik.atekon.de"
    date: 2005-09-20
    body: "klik.atekon.de has IP 134.169.172.48\nI remember earlier today name resolution didnt work. But now it does again."
    author: "ac"
  - subject: "what about virus"
    date: 2005-09-19
    body: "What about virus, worms etc and destruction of ~/  and so on? \n\nCan this can be a new super-tool to make Linux as virus infested as Windows is now? \n\nPlease tell me I am wrong (and why)! "
    author: "worm"
  - subject: "Re: what about virus"
    date: 2005-09-19
    body: "klik does not run stuff from random places."
    author: "probono"
  - subject: "Re: what about virus"
    date: 2005-09-20
    body: "Other then installing software on Windows by clicking random links in Internet Explorer,  Klik behaves like other package systems like YaST, APT, click-n-run, urpmi and yum: you tell it what you want to run, and it downloads and installs the necessary packages for you from predefined centralized  servers, not from random locations."
    author: "rinse"
  - subject: "Re: what about virus"
    date: 2005-10-23
    body: "you are wrong"
    author: "Minkiux"
  - subject: "Re: what about virus"
    date: 2005-12-04
    body: "i can see where you're coming from. imagine that you've already installed 20 packages using klik, all of which are in your home-directory somewhere, and then you install a package with a nasty script in it wiping the the home-directory clean. that would be a very bad-hair-day. \n\nthere must be a simple way around this. maybe one shouldn't give the klik script the write to delete anything without the user OKing it. maybe you should be warned, if the klik package isn't kosher. \n\nwhatever, it does make things a bit more complex. \n\n"
    author: "hold_breal"
  - subject: "http://klik.atekon.de/ OFFLINE"
    date: 2005-09-21
    body: "The website is offline, any alternative?"
    author: "Neto Cury"
  - subject: "Re: http://klik.atekon.de/ OFFLINE"
    date: 2005-09-21
    body: "http://134.169.172.48"
    author: "/me"
  - subject: "Re: http://klik.atekon.de/ OFFLINE"
    date: 2005-09-23
    body: "They are having a dns problem.\n\nGoto http://134.169.172.48/ in the meantime."
    author: "ft"
  - subject: "DOMAIN DOES NOT RESOVE : KLIK DOES NOT WORK"
    date: 2005-09-21
    body: "the komplete KLIK thing does not work for me because the DOMAIN does not resolve. this is very very sag because everybody who tries this klik thing will stop to use it because it wont work... :-( :-(\n:-(\n\n\n:-("
    author: "chris"
  - subject: "Re: DOMAIN DOES NOT RESOVE : KLIK DOES NOT WORK"
    date: 2005-09-21
    body: "Oh, come on. Click the flatforty link and only two entries below your comment is the answer to your problem."
    author: "ac"
  - subject: "Re: DOMAIN DOES NOT RESOVE : KLIK DOES NOT WORK"
    date: 2005-09-21
    body: "no it does not work !!! perhaps you try first and post then ?\n\nthe domain name ist encoded in some of the scripts (most of them) so i only get errors when using klik because it wants to get things from klik.atekon.....\n\ni just wantet to say it suck that the dns goes off when you announce it on kdenews with 150 comments !....\n\nchris"
    author: "ch"
  - subject: "Re: DOMAIN DOES NOT RESOVE : KLIK DOES NOT WORK"
    date: 2005-09-22
    body: "It's a real pitty that there is no single mirror."
    author: "testerus"
  - subject: "Solution to dns problem found at #klik (freenode)"
    date: 2005-09-22
    body: "Here's the solution\n\nAdd '134.169.172.48  klik.atekon.de' to your /etc/hosts file.\n\nRemember to remove it once the dns problem is solved.\n"
    author: "dan"
  - subject: "Re: Solution to dns problem found at #klik (freeno"
    date: 2005-09-23
    body: "Unfortunately /etc/hosts is irrelevant if you are to use a transparent proxy, right?"
    author: "testerus"
  - subject: "Re: Solution to dns problem found at #klik (freeno"
    date: 2005-09-23
    body: "### \"/etc/hosts is irrelevant if you are to use a transparent proxy\"\n----\n\nYou can circumvent proxy problems by using \"transconnect\". I've to do it to get past my proxy too. \n\ntransconnect is a small library, tconn.so, that can be used with the \"LD_PRELOAD\" mechanism to intercept all CONNECT calls of libc programs and replace these with an explicit proxy connection, including proxy authentication where this required. tconn.so reads from a tconn.conf configuration file in each user's home the exact details how it should behave. Make sure you do \"unset http_proxy\" at the same time you use tconn.so pre-loaded. \n\nSee http://transconnect.sf.net/ for details.\n"
    author: "Kurt Pfeifle"
  - subject: "Nice try but hardly new"
    date: 2005-09-22
    body: "This looks mildly interesting but is hardly something to get too excited about. The same functionality (and then some) has been available in the Tcl/Tk community literally for *years* -- check out \u00bbstarkits\u00ab on the Tcl/Tk Wiki at http://wiki.tcl.tk . A starkit is a Tcl/Tk program, including any required extensions, in a single file, and can be platform-independent (i.e., the same, identical starkit runs on Linux, Windows and the Mac), store its own configuration settings, and self-update from the Internet.\n"
    author: "Anselm Lingnau"
  - subject: "Re: Nice try but hardly new"
    date: 2005-09-22
    body: "And how does that work with KDE apps? GTK apps? Plain old X apps?"
    author: "probono"
  - subject: "Re: Nice try but hardly new"
    date: 2005-09-22
    body: "Well, the article you commented on was about \"the potential uses of this technology [klik] for helping the non-coding contributors to KDE\". \n\nThese starkits don't help a bit regarding development, translating, usability testing, functional testing of KDE or creating artwork for KDE.  Klik is one possible solution for some common problems in the KDE development community.  AFAICS Tcl/Tk and starkits are completely unrelated.  \n\nStarkits may have nice features but they solve a different problem. \n\n"
    author: "cm"
  - subject: "Re: Nice try but hardly new"
    date: 2005-09-23
    body: "How does starkits relate to C++ programs like KDE apps?"
    author: "ac"
---
<a href="http://klik.atekon.de/">Klik</a> is a system which creates self-contained packages of programmes installable over the web with a single click. In the article below <a href="http://www.kdedevelopers.org/blog/418">Kurt Pfeifle</a> discusses the potential uses of this technology for helping the non-coding contributors to KDE.  He also looks at how the system works and the obvious security issues involved.  










<!--break-->
<p>
<ul>
<li>Can you imagine you just copy one single file onto your system, wherever you possess "rw" privileges (no need for being root) and this file represents a complex, runnable application?</li>
<li>Can you imagine that with a simple click on a website you could "copy-install" and run such an application?</li>
<li>Can you imagine that this simple "installation" did not affect in any way the stability of your base system?</li>
<li>Can you imagine that this file could even run from a USB pen drive? And you just stick it to a different computer to run the application there?</li>
<li>Can you imagine that you could run the latest official Krita release side by side with Boudewijn's code from last night, without the two versions interfering with each other?</li>
<li>Can you imagine you revert to your old system again by just deleting one file?</li>
</ul>

And why would this be relevant or useful?

<ul>
<li>It would provide a way to KDE developers for making binary packages of bleeding-edge code directly available to be run by usability experts for early feedback...</li>
<li>It would let KDE developers give out their bleeding-edge packages to beta-testers well before Release Day... </li>
<li>It would let translators see (in their actual, lively GUI context) the strings they are working on... </li>
<li>It would enable art designers to actually *run* a program they are contributing icons and other artwork for... </li>
<li>It would give a full preview of what will go out to our users at Release Day, to all the different groups of our non-technical KDE contributors (who are not typically compiling KDE every night from the latest checked-out sources)...</li> 
<li>It could happen at any time, completely independent of releases, practically 5 minutes after the code was written and compiled, and long before there are official packages created by everyone's favorite distro...</li>
</ul>
</p>

<p>One can dream, no?</p>

<p>At <a href="http://conference2005.kde.org/">aKademy</A> I discussed this question with various people. By now everyone in the KDE community is aware of <em>one</em> of my proposed solutions (or that's what I like to think): use a <a href="http://www.nomachine.com/">NX</a> or <a href="http://freenx.berlios.de/">FreeNX</a> driven KDE Application Server, and provide remote access to the programs in question.</p>

<p>However you are wrong if you imagine that my only occupation is NX. I have <em>another</em>, complementing proposal about how to tackle this same problem. One that is an especially nice fit in cases where testers and users do not have network connections.</p>

<p>One that works for Live CD distributions (Knoppix, Kanotix) as well as for Debian, Linspire, Ubuntu, Kubuntu and <a href="http://www.openSUSE.org/">openSUSE</a>/SUSE Linux 10.0.  (No, it's really got nothing to do with NX. Nor with FreeNX. Other than the fact that Fabian, the main developer of FreeNX, has also been a contributor to this klik thingie...) </p>

<p>Take note! It's not a dream. It is reality. It is reality for Linux. It is reality for KDE. It is called <a href="http://klik.atekon.de/"><em>klik</em></a>.</p>

<p>It works on most Debian systems, and on Knoppix (<A href="http://www.solidz.com/torrents/knoppix/KNOPPIX_V3.9-2005-05-27-EN.torrent">torrent</A>) and Kanotix (<A href="http://debian.tu-bs.de/mirror/kanotix/KANOTIX-2005-03/KANOTIX-2005-03-LITE.iso.torrent">torrent</A>) Live CDs.<a href="http://www.openSUSE.org/">openSUSE/SUSE-10.0</A> is has recently been added to the list too. Here's how you can test it:

<ul>
<li>Install the klik client: press <tt>[Alt]+[F2]</tt> and paste:</i><br> <tt> wget klik.atekon.de/client/install -O - | sh</tt> (this step is not required on Kanotix -- it has the klik client already installed).</li>
<li>Follow the instructions that pop up on your screen.</li>
<li>Start Konqueror (it may have opened automatically), and click on one of the links offered in the <a href="http://klik.atekon.de/">online klik</a> store.</li>
</ul>

<p>You could also just type <a href="klik://xvier">klik://xvier</a> into the Konqueror address field. I actually do recommend to you to start with xvier: it is a simple game that fits into a less than 400 kByte download, so you can have a quick success with klik and see what potential it has...</p>

<p>Neat, isn't it?</p>

<p>klik has been developed by Simon Peter (a.k.a. "probono" in IRC), with the support of Niall Walsh ("bfree"), J&ouml;rg Schirottke ("Kano") and FreeNX's Fabian Franz ("fabianx"). You can meet them in the IRC channel #klik on Freenode.</p>

<p>If you are bit security concerned, you may want to know what klik does to your system. Here's the pitch:</p>
<ul>
<li>Its .cmg files are self-contained AppDirs (applications directories), compressed into a cramfs or zisofs file system.</li>
<li>To run the contained app, klik mounts the bundle file underneath <em>/tmp/app/1/</em> and runs it from there; if mounted, the bundle looks like it is a subdirectory expanded into the real directory structure of the host.</li>
</ul>

<p>It's very much similar to how applications on Mac OS X works.... </p>

<p>If you are even more cautious, or paranoid, you surely want to investigate more closely and see how klik operates on your system. Follow these steps to find out more details:</p>

<ul>
<li><tt>wget klik.atekon.de/client/install</tt> (this downloads the install file without executing it).</li>
<li><tt>less install</tt> (this lets you look at the installer code: fear not -- it's pure shell).</li>
<li><tt>less $HOME/.klik</tt> (this lets you look at the "commandline client+klik protocol handler" code, of course only after running the klik client install).</li>
<li><tt>less $HOME/.zAppRun</tt> (this lets you look at the commandline starter for klik-ified AppDir bundles, also executed if you just click on one of the .cmg files).</li>
<li><tt>less {$KDEHOME,$HOME/.kde}/share/services/klik.protocol</tt> (the secret behind the klik://my_cool_app links, part 1).</li>
<li><tt>less {$KDEHOME,$HOME/.kde}/share/applnk/klik/klik.desktop</tt> (the secret behind the klik://my_cool_app links, part 2).</li>
<li><tt>less {$KDEHOME,$HOME/.kde}/share/applnk/klik/.directory</tt> (why there is now a klik icon and a klik entry in the K Menu).</li>
<li><tt>less {$KDEHOME,$HOME/.kde}/share/mimelnk/all/cmg.desktop</tt> (why klik is now responsible for handling clicks on files that happen to have a .cmg suffix, part 1).</li>
<li><tt>less {$KDEHOME,$HOME/.kde}/share/applnk/.hidden/AppRun.desktop</tt> (why klik is now responsible for handling clicks on files that happen to have a .cmg suffix, part 1).</li>
<li><tt>less /etc/fstab</tt> (why klik can now find mountpoints in the file system to mount the .cmg AppDirs on execution).</li>
<li><tt>ls -lR /tmp/app/{7,6,5,4,3,2,1}</tt> (list the directories underneath the mountpoints while one of the .cmg AppDirs is executed).</li>
</ul>

<p>klik's smartness is all contained in a few shell scripts and typical KDE config files, as you can easily see...
</p>

<p>For most of the 4000+ packages available from the klik warehouse, the "download" consists of a "recipe". The recipe tells the klik client where to fetch the binaries from (in most cases .deb packages from the official Debian repositories), how to unpack them, and how to re-package and compress them into the final .cmg image. So the klik client does most of the work and builds its own .cmg file in most cases.</p>

<p>If you want to look at one such recipe, here is the <a href="http://klik.atekon.de/scribus.recipe.example">klik recipe for Scribus</a>.</p>

<p>There are other packages which have been built on the server (and hand-tuned and tested) so that they work with non-Debian distros too ("serverside apt"). In this case the klik://some-app link will fetch the ready-made .cmg from the URL the klik server names in his recipe.  The special "<a href="http://opensuse.linux.co.nz/klik/10.0/"><em>klik apps for SUSE 10.0</em></a>" repository is filling up by the day. Warning: currently this will only work for <a href="http://www.opensuse.org/">openSUSE/SUSE Linux 10.0</a>, not for other Distros! </p>

<p>You may be interested in trying the following links, if you have more bandwidth (note: they'll not work for you unless you install the klik client). They work on openSUSE/SUSE-Linux 10 RC1 very well, and also support Knoppix, Kanotix, Debian, Linspire and Kubuntu (other distros are untested):

<ul>
<li><A href="klik://apollon">klik://apollon</A>  (downloads 2.2 MByte)</li>
<li><A href="klik://firefox">klik://firefox</A> (downloads 10.0 MByte)</li>
<li><A href="klik://skype">klik://skype</A> (downloads 10.2 MByte)</li>
<li><A href="klik://frozen-bubble">klik://frozen-bubble</A> (downloads 14.0 MByte)</li>
<li><A href="klik://ooo2">klik://ooo2</A>  (downloads 114 MByte) - <a href="http://www.kdedevelopers.org/blog/1192">Martijn</a> can use this to read .odt files easily!</li>
</ul>
I found the <a href="http://opensuse.linux.co.nz/klik/10.0/ooo2_1.9.125-novell.cmg">ooo2 bundle</a> (representing the OpenOffice.org 2 beta release, build 125 by Novell) on par (or even better) in speed and responsiveness as the "real" RPM package that I installed from the <A href="http://www.opensuse.org/Download#Development_Build">RC1 iso images for SUSE Linux 10.0</a>.</p>

<p>If you are a type of person who likes to startup apps from the commandline, use the klik commandline client like this: </p>

<ul>
<li><tt> $HOME/.klik klik://ktorrent </tt> (installs the cool <a href="http://www.kde-apps.org/content/show.php?content=26353">KTorrent BitTorrent</a> client).</li>
</ul>

<p>This will prepare the .cmg AppDir bundle <em>and</em> run it once ready. Once .cmg file is on your system, the commandline to run it (without a repeated download) is this:</p>

<ul>
<li><tt> $HOME/.zAppRun /path/to/app123.cmg </tt> (runs the application named <em>app123</em>).</li>
</ul>

<p>I have already browsed the web with Alpha version 1.6a1 of Firefox to see if the hype around it is justified.  To do so, I tried the old-fashioned manual installation of the klik-ified Firefox:</p>

<ul>
<li><tt>mkdir $HOME/klik-downloads</tt></li>
<li><tt>cd $HOME/klik-downloads</tt></li>
<li><tt>wget http://opensuse.linux.co.nz/klik/10.0/firefox_1.6a1.cmg</tt></li>
<li><tt>$HOME/.zAppRun firefox_1.6a1.cmg</tt></li>
</ul>


<p>I am pretty sure, that at least some of our beloved KDE-PIM hackers will like this new way to take a quick look at their valued competition, without needing to install it:</p>

<ul>
<li><A href="klik://thunderbird">klik://thunderbird</A> -- the Beta 1 of upcoming Thunderbird 1.5, the Mozilla mail client (<A href="http://opensuse.linux.co.nz/klik/10.0/thunderbird_1.5b1.cmg">klik bundle</A>, direct link)</li>
<li><A href="klik://sunbird">klik://sunbird</A> -- Development version of Mozilla calendar application (<A href="http://opensuse.linux.co.nz/klik/10.0/sunbird_0.2+.cmg">klik bundle</A>, direct link )</li>
</ul>

<p>Now, developers, what do you think of a tool that lets you easily create binary snapshots of your own development versions in the form of those nifty <em>"Don't Install, Just Copy!"</em>- .cmg files? -- Which of you will be the first five to step forward and receive the service of getting their SVN weekly builds packaged as .cmg AppDir bundles, for the next 3 months?</p>

<p>I know <a href="http://www.valdyas.org/fading/index.cgi/hacking/krita/">Boudewijn</a> has been struggling in the past to provide Krita snapshots to a dozen beta testers and non-technical contributors, and to help them compile the code, or to support them installing binaries he had built.</p>

<p>I also know that <em>I</em> definitely would love to get quick access to <a href="http://kpdf.kde.org/">kpdf</a>, <a href="http://www.koffice.org/kword/">KWord</a>, <a href="http://amarok.kde.org/">amaroK</a>, <a href="http://quanta.kdewebdev.org/">Quanta</a> and <a href="http://kommander.kdewebdev.org/">Kommander</a> snapshots which I can run on my stable system with the reassuring feeling that the most that can go wrong is that the test app doesnt run at all, and all I had to do is just delete it again, to have my system reverted to its original state.</p>

<p>What about our friends from <a href="http://www.openusability.org/">OpenUsability.org</a>? Would they appreciate such a service too? Have they ever looked at the added value <a href="klik://">klik://</a> can offer to our common development workflow?</p>


