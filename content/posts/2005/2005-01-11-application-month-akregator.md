---
title: "Application of the Month: Akregator"
date:    2005-01-11
authors:
  - "ateam"
slug:    application-month-akregator
comments:
  - subject: "The usable dialogue"
    date: 2005-01-11
    body: "Is the screenshot embedded in the dialogue a standard feature that is shared across all apps that dock in the system tray, or is it just an innovation in Akregator?\n\nIf the latter, it should definitely be rolled into kdelibs as a standard function. It's great :o)"
    author: "Tom"
  - subject: "Re: The usable dialogue"
    date: 2005-01-11
    body: "It's a dialog by S\u00e9bastien Lao\u00fbt that's used in a few apps like akregator and amarok currently. It isn't in kdelibs yet, but there was some dicussion about that on kde-core-devel a week ago. \n\nhttp://lists.kde.org/?t=110494998600002&r=1&w=2"
    author: "anon"
  - subject: "Re: The usable dialogue"
    date: 2005-01-11
    body: "This is of course a great usability enhancement (and it was improved since aKregator included the dialog, see the thread anon linked above).\n\nBut there still is a big problem:\nWhen a windows is behind the systray icon the dialog take a screenshot of the window because the icon is hidden. Then there seem is a circle circling... nothing special: it's a usability problem.\nNo solution has been found for the moment.\n\nOf course this is very rare (don't happens with default KDE config)...\nSince two programs (aKregator and Konversation) include it, should it be in KDElibs even if broken?"
    author: "Sebien"
  - subject: "Re: The usable dialogue"
    date: 2005-01-11
    body: "Well, if it were in kdelibs already there'd be also more peers potentially able to solve this issue. And since this issue really is only a corner case not possible to encounter by clueless users using a default KDE setting I wouldn't think that this should prevent it from being in kdelibs already."
    author: "ac"
  - subject: "I prefer eventwatcher"
    date: 2005-01-11
    body: "I prefer eventwatcher because I like this look and feel."
    author: "GML"
  - subject: "Re: I prefer eventwatcher"
    date: 2005-01-11
    body: "Hey, looks cool, didn't know eventwatcher yet. Seems very useful for collecting events and information from different sources.\nI think which app is best suited, knewsticker, akregator, eventwatcher etc., depends on the \"usage pattern\" and the number of feeds you observe. "
    author: "lippel"
  - subject: "Re: I prefer eventwatcher"
    date: 2005-01-16
    body: "Thank you!\n\nI have followed the Akregator beta versions, but I have this contant feeling it grows in the wrong direction. It doesn't use the KMail keys when reading, and it doesn't follow Konquerors behaviour when opening tabs (especially open in background is missing).\n\nEventwatcher doesn't do this either but looks more promising!"
    author: "Jonas"
  - subject: "Re: I prefer eventwatcher"
    date: 2005-01-21
    body: "Umh, you've been able to use KMail keys (since 15.10.2004/b7) and ability to open tabs in background (since 30.6.2004/b4) for a long time now... Check out the configuration dialog!\n\nAnd about eventwatcher, I think Sandro has concentrated to KDE themeing stuff nowadays."
    author: "tpr"
  - subject: "Good but..."
    date: 2005-01-12
    body: "I've lost content several times now due to the fact that aKregator doesn't use KHTML \"normally\" in the tabs.  I've had to train myself never to open links in aKregator itself.\n\nIf you open a link, try to post a reply, the content goes away and the post is never made.  That, combined with several other \"not quite working as a web browser\" oddities makes me wish that it were a Konqueror sidebar rather than full app.\n\nIt's driven me nuts with Wiki updates.  I'll spend a good bit of time composing a review in a field, then hit \"Save\", and it pops back, no changes made, no way to retrieve the content I just typed.  Annoying as hell."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Good but..."
    date: 2005-01-12
    body: "I fully agree.\n\nI love aKregator, but sometimes it doesn't work as Konqueror.\nInstead of mimic Konqueror for each new versions of Konqueror, a best way would be to have aKregator a Konqueror sidebar.\n\nBut I also understand that it would be quite more difficult since what should happens if I click the aKregator systray icon? What Konquror window should be raised? And should it open a new tab for each opened feed?\n\nPerhapse it could be possible to have a \"special Konqueror window\" that is used only by aKregator and shown/hidden by it...\n\nIn any way, it's the better aggregator for now.\nA big thanks to developers.\n\nI was using eventwatcher before, but it becomes quickly hard to manage lot of feeds. And aKregator have a lot more features..."
    author: "Sebien"
  - subject: "the hassle?"
    date: 2005-01-12
    body: "\"of this nifty application which allows you to browse through thousands of internet feeds without the hassle of using a web browser.\"\n\nsuch a stab at firefox.. tut tut"
    author: "salsa king"
  - subject: "Re: the hassle?"
    date: 2005-01-12
    body: "at firefox? why not at web browsers in general? feed aggregators do tend to be more usable than web browsers by their very nature, since the world wild web is extremely unstructured compared to feeds. "
    author: "anon"
---
This month we present the fabulous <a href="http://akregator.sourceforge.net/">Akregator</a> in our series "Application of the Month". As usual we have an interview with the author and a description of this nifty application which allows you to browse through thousands of internet feeds without the hassle of using a web browser. If you want to help us creating this monthly series or you want to translate it to your own language please <a href="https://mail.kde.org/mailman/listinfo/kde-appmonth">join the mailing list</a>. Enjoy App of the Month in <a href="http://www.kde.nl/apps/akregator/">Dutch</a>, <a href="http://www.kde.org.uk/apps/akregator/">English</a>, <a href="http://www.kde-france.org/article.php3?id_article=126">French</a>, and <a href="http://www.kde.de/appmonth/2005/akregator/">German</a>!


<!--break-->
