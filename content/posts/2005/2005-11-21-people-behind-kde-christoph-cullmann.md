---
title: "People Behind KDE: Christoph Cullmann"
date:    2005-11-21
authors:
  - "jriddell"
slug:    people-behind-kde-christoph-cullmann
comments:
  - subject: "Which text editor do you use? Why?"
    date: 2005-11-21
    body: "We'd be shocked if the reply was \"Emacs\" :-)\n\nGreat interview, Christoph. Now, can you share with us your secret of super-fast-porting-Kate-to-KDE4? It'll sure help in kdelibs."
    author: "Thiago Macieira"
  - subject: "Re: Which text editor do you use? Why?"
    date: 2005-11-21
    body: "I would just love to have emacs embedded in kdevelop. I guess this should be possible after kdevelop has been ported to Qt4.\n\n"
    author: "Swoosh"
  - subject: "Re: Which text editor do you use? Why?"
    date: 2005-11-21
    body: "After kdevelop has been ported to emacs you mean ? :p"
    author: "Narishma"
  - subject: "Re: Which text editor do you use? Why?"
    date: 2005-11-21
    body: "Well, I guess one can argue how serious my comment was. I just got a bit inspired by this:\n\nhttp://blogs.qtdeveloper.net/archives/2005/08/10/playing-around-with-xembed/\n\nUnfortunately the screenshot showing the stuff in action seems to be gone...\n"
    author: "Swoosh"
  - subject: "Re: Which text editor do you use? Why?"
    date: 2005-11-22
    body: "I just hope that some day Kate will have decent Emacs profile which will include emacs-like identation and most of the magical key bindings."
    author: "petteri"
  - subject: "Kate"
    date: 2005-11-21
    body: "Kate is just a nice piece of software. Thanks!"
    author: "Ian Monroe"
  - subject: "Just nice"
    date: 2005-11-21
    body: "Lovely interview. Christoph, in case you read this: thanks a bundle for Kate. It is my favourite text editor. I even install the KDE libraries when I work under a GNOME environment just so I can use it. ^_^\n\nI'll remember to say hi next time I re-visit Germany again. Ich bin von Mainz.\n\nCheers and kudos."
    author: "Pascal Klein"
  - subject: "K8 is gr8"
    date: 2005-11-21
    body: "Kate is great (rhyme intended). Keep up the great work!"
    author: "Roy Schestowitz"
  - subject: "Re: K8 is gr8 - A well known fact!"
    date: 2005-11-21
    body: "The Bouncing Souls already knew this before - and that was back in 1997!\n\nhttp://www.letssingit.com/?/bouncing-souls-k8-is-great-qkc1cm9.html"
    author: "Phase II"
  - subject: "Agree on KHTML"
    date: 2005-11-21
    body: "Completely agree - KHTML is really underrated. Only KJS could be faster."
    author: "m."
  - subject: "Re: Agree on KHTML"
    date: 2005-11-21
    body: "khtml rocks the house\n\nI agree about kjs, see here:\nhttp://bugs.kde.org/show_bug.cgi?id=113921\n"
    author: "ac"
  - subject: "Re: Agree on KHTML"
    date: 2005-11-22
    body: "There are slow pages for almost all browsers. http://blog.naver.com/applezhome makes my Konqueror unusable."
    author: "testerus"
  - subject: "OT: the dot == Ark linux"
    date: 2005-11-21
    body: "Did anyone else experience yesterday that the dot == ark linux?\n\nI typed in dot.kde.org and and the page at http://www.arklinux.org/ showed up. I was like, whoa :)\n\nGood to see it back.\n\n-Sam "
    author: "Sam Weber"
  - subject: "Re: OT: the dot == Ark linux"
    date: 2005-11-21
    body: "Yeah, KDE Dot seemed to be down for a while, and then just before it seemed to be working again I got the same thing as you! It was rather confusing :S"
    author: "Jack H"
  - subject: "Re: OT: the dot == Ark linux"
    date: 2005-11-21
    body: "The dot is hosted by Ark Linux (again).\nThrow in a downtime, a configuration error and someone with his fingers glued onto the refresh button and you get this..! ;-)"
    author: "Phase II"
  - subject: "As a writer, thanks"
    date: 2005-11-22
    body: "I am a writer by trade and passion, and Kate is the best thing out there. When on a Mac, I can abide using TextEdit, so long as I switch it to plain-text mode, and even then it's only fine because the place I end up using Macs at has enormous monitors and I can have three windows open side-by-side.\n\nOn Windows, I have no options. Wordpad in text-only mode is acceptable, but has problems, and has no support for multiple files (and most Windows machines don't have the same cinema display monitors as the Macs). Notepad is a joke, and Word trys to fix my spelling!\n\nAll GNOME gives me is Gedit, which is only a couple steps ahead of Notepad, and if I try something like Abiword it has the lesser issues of MS-Word: thinking it should have page-borders and formatting and options.\n\nIn Kate, I can easily keep open all ten-to-fifteen things I'm working on 24-hours-a-day seven-days-a-week and it's always responsive and easy to use. It's just the flat-out best text editor around (well, vim gives some competition, but only for some specific tasks).\n\nAnd again,\nThanks for Kate, it defines my workflow."
    author: "jameth"
  - subject: "Re: As a writer, thanks"
    date: 2005-11-22
    body: "Also, I just added my vote and comments to bug 65740, the only issue I have with Kate. That is: please add an option to get a word-count for the selected text. Right now, I need to use a console and wc. Hopefully that can get added to improve an already great application."
    author: "jameth"
  - subject: "Excellent "
    date: 2005-11-23
    body: "Wish you all the best"
    author: "Amr Youssef"
---
This man maintains KDE's text editor <a href="http://kate.kde.org">Kate</a> and the associated KTextEditor interface.  He also keeps three cats and disappears from his girlfriend for a week each year in the name of KDE.  The star of tonight's <a href="http://people.kde.nl/">People Behind KDE</a> interview is <a href="http://people.kde.nl/christoph.html">Christoph Cullmann</a>.

<!--break-->
