---
title: "Application of the Month: KPDF"
date:    2005-05-09
authors:
  - "ateam"
slug:    application-month-kpdf
comments:
  - subject: "editing"
    date: 2005-05-08
    body: "\"What is missing is the possibility to edit a document, but this is very hard to implement. However I'm sure they are working on this\"\n\nAre they?"
    author: "Pat"
  - subject: "Re: editing"
    date: 2005-05-08
    body: "I highly doubt it.  PDF like PS, DVI and some others is an output format, it's not ment to be edited."
    author: "ac"
  - subject: "Re: editing"
    date: 2005-05-08
    body: "Not that I know of, however they are working on providing better text selection (sometimes text selection in PDFs doesn't work like you would expect).  Also, I believe KWord has a PDF import filter; not sure how well it works."
    author: "Spy Hunter"
  - subject: "Re: editing"
    date: 2005-05-09
    body: "Maybe they could provide a pop-up to replace simply the \"selected text\",\nthat would be a start.\n\nBut first they should make selection text a bit better. =)"
    author: "fprog26"
  - subject: "Re: editing"
    date: 2005-05-08
    body: "I think that would be outside the scope of KPDF. Maybe improving the KWord import filter would be a better idea.\nAnyway, I really do hope they add to KPDF Postscript support, so it can be THE universal viewer for KDE, and drop KGhostview which right now is a bit redundant."
    author: "Anonymous"
  - subject: "Re: editing"
    date: 2005-05-08
    body: "There is currently work going on to give kpdf, kdvi and kghostview (?) a common framework so that they share all relevant code."
    author: "Robert"
  - subject: "Scribus"
    date: 2005-05-09
    body: "Tried Scribus?"
    author: "KDE User"
  - subject: "Re: Scribus"
    date: 2005-06-01
    body: "It' s great, but if you want to open quarkXPress-data, you better try out viva-designer..."
    author: "Volker"
  - subject: "Re: editing"
    date: 2005-05-09
    body: "I don't know, but if they aren't, I think it's the perfect troll with which to end that interview.  :-)"
    author: "anon"
  - subject: "Re: editing"
    date: 2005-05-11
    body: "Depends what you mean by \"editing.\"\n\nI'm currently working on a (highly proprietary corporate internal) application that mangles PDFs in various ways.  It's not technically difficult to modify the PDF document structure (a complete document structure parser which can read a PDF file, mangle its internal structure, and write a new PDF file is two weeks' effort and about 1300 lines of Perl).  It is a little difficult to do this in open-source land while also respecting the egregious licensing terms of the PDF spec documents (most of which IMHO are probably unenforceable, but Adobe tries to enforce them anyway).  \n\nMost of the PDF document structure is above the page level.  To PDF, the pages you can actually see are just leaf nodes in a big multiple overlapping tree structure.  Most of the structure deals with page indexes, bookmarks, hyperlinks, annotations, fonts, images, and structural details to support incremental revision by appending to the file and efficient document loading over slow/high-latency links (but not both at the same time).  Adobe has a number of patents on those topics.\n\nI expect the first useful editing features to be rearranging pages, changing the bookmarks list, importing pages from other documents, adding annotations and hyperlinks, and limited graphics or text manipulation such as adding a page number, headers/footers, border, logo or watermark to pages--all easy features to implement on top of a working PDF parser/generator."
    author: "Syg"
  - subject: "koffice"
    date: 2005-05-08
    body: "Heya,\n\nI believe the text-part of koffice can import pdf's. I don't know how good the support is ...\n\nKpdf is a nice application. I used it lately a bit. I hope that they can make the rendering go faster and that they test it on papers that contain mathematical expressions. For papers that contain mathematical expressions, I (sometimes) switch to xpdf since it renders the formulas well. Just searching for papers on scholar.google.com or some other source that contain formulas may be a start ...\n\nfor the rest, nice work!!! I like the menu where you can choose between selecting character-data or image-data, ...\n\nMichel"
    author: "Michel Brabants"
  - subject: "Forms"
    date: 2005-05-09
    body: "When can i used and fill pdf forms? With this feature, kpdf will be THE opensource pdf reader!\n\nregards and keep on the good work!"
    author: "Francisco"
  - subject: "No rotate?"
    date: 2005-05-09
    body: "I always end up using acroread, since it lets me rotate the documents so that I can read them full screen on my tablet convertable acer travelmate. I would have guessed rotation to be a standard feature for such a program, but kpdf seems to lack it."
    author: "Kolbj\u00f8rn Barmen"
  - subject: "Re: No rotate?"
    date: 2005-05-09
    body: "Have you tried krandrtray?"
    author: "anon"
  - subject: "Re: No rotate?"
    date: 2005-05-09
    body: "Uhm, I just wanted to rotate the documents, not mess up my desktop. :)\n\nSince there is no way to snapshot and restore icon locations on KDE, using xrandr is something I'd avoid. Also,  it seems that the i810 xorg driver doesnt support rotation, only resize."
    author: "Kolbj\u00f8rn Barmen"
  - subject: "Re: No rotate?"
    date: 2005-05-09
    body: "Looks like the devs are aware of this wish.\n\nhttp://bugs.kde.org/show_bug.cgi?id=99352"
    author: "Jeffrey M. Smith"
  - subject: "Re: No rotate?"
    date: 2008-02-20
    body: "Three years later, the developers have not provided any rotation feature.  Thanks for nothing, guys; acroread is still the only reader that's worth anything."
    author: "Liam"
  - subject: "Re: No rotate?"
    date: 2008-09-07
    body: "Seconded. Today I had to read a document that is rotated 90 degrees. I just realized that neither kpdf nor evince provides rotation, so I had to install acroread. But to give credits where it's due, for regular PDFs I do use kpdf on a daily basis."
    author: "David Garamond"
  - subject: "Re: No rotate?"
    date: 2008-09-13
    body: "Actually, evince _does_ provide rotation. It is under the \"Edit\" menu, for some peculiar reason.\nIMHO this non-feature in KPDF is one of the biggest mysteries in the history of software."
    author: "raziv"
  - subject: "Re: No rotate?"
    date: 2008-09-13
    body: "Of course, we're in 2008 now and shouldn't be talking about kdpf anymore, but okular, which actually _does_ provide rotation. And then... If using a convertible tablet is the reason one wants to rotate the display of a pdf, why not use xrandr to rotate the whole display?"
    author: "Boudewijn Rempt"
  - subject: "Searching"
    date: 2005-05-09
    body: "Hi,\n\nJust wanna say thanks a lot for adding search ability to KPDF. It seems to work sooo good. I have really missed this feature!\n\n   P"
    author: "perraw"
  - subject: "Re: Searching"
    date: 2005-05-09
    body: "I agree, the search is amazing."
    author: "superstoned"
  - subject: "KPDF is really amazing."
    date: 2005-05-09
    body: "I'm using it and just can say that it has all the capabilities I ever needed from a PDF reader and never found in Linux. Thanks for the guys that made it."
    author: "Ivan"
  - subject: "Some questions.."
    date: 2005-05-09
    body: "Hi,\nFirst of all let me thank you for kpdf - it has really improved a lot with the latest release!\n\nNow 2 questions I would like to have seen in the interview:\n1. How is the collaboration with the poppler guys?\n2. Do you plan on switching to poppler anytime soon?\n\nTIA,\nMicha"
    author: "Michael Jahn"
  - subject: "Re: Some questions.."
    date: 2005-05-09
    body: "1. How is the collaboration with the poppler guys?\n - Mostly good\n\n2. Do you plan on switching to poppler anytime soon?\n - Depending of your definition of soon\n \n"
    author: "Albert Astals Cid"
  - subject: "Re: Some questions.."
    date: 2005-05-09
    body: "Thank you for the answers.\n\nActually the question should have been posed without the \"anytime soon\"-part. I was just curious whether you intend to do this at all without any relation to _when_ this will happen. "
    author: "Michael Jahn"
  - subject: "Damn good app!"
    date: 2005-05-10
    body: "Great going!\nOnce they add form fill support, its bye bye to Acroread.\n"
    author: "ashay"
  - subject: "Basic observation"
    date: 2005-05-10
    body: "Presentation View page counter increase is counter intuitive to web page and application scrolling.\n\nThe Logitech mouse scroller increases page count going counterclockwise (right-hand rule) (page up) instead of the commonly used (clockwise) (page down) scrolling of the wheel."
    author: "Marc Driftmeyer"
  - subject: "pity"
    date: 2005-05-10
    body: "Pity this gem isn't included in Mandriva LE download edition.  Neither is KOffice.  :("
    author: "anon"
  - subject: "Re: pity"
    date: 2005-05-10
    body: "Mandriva advertizes to have KPDF of KDE 3.4 backported, and KOffice is also included - Kivio iirc even on one of the two first CDs."
    author: "Anonymous"
  - subject: "Re: pity"
    date: 2005-05-10
    body: "I know they do.   It's not on the CDs I downloaded, trust me.  I looked carefully and urpmi does not find them either."
    author: "anon"
  - subject: "Re: pity"
    date: 2005-05-10
    body: "You should use http://easyurpmi.zarb.org and setup \"main\" and \"contrib\" repositories. Then urpmi should find both."
    author: "Mand* Rules"
  - subject: "ugly fonts?"
    date: 2005-05-10
    body: "While I like the overall feeling of kpdf I usually switch back to acroread for longer texts. Why? The fonts are a lot nicer and better to read. A closer look with xmag shows that compared to acroread the fonts are bold. Btw xpdf produces exactly the same results as kpdf.\nAny chance that the fonts will be rendered more like acroread in future versions or is there a reason why its different?"
    author: "michael"
  - subject: "Re: ugly fonts?"
    date: 2005-05-12
    body: "Would you please try this PDF and see if you have font problems with it:\n\nhttp://home.earthlink.net/~tyrerj/files/PS-12/PS-12.pdf\n\nPeople have reported similar problems, but I can't reproduce them.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Annotations"
    date: 2005-05-12
    body: "I have no real need for editing of PDFs, but the ability to add annotations - underlines, marginal notes, highlighting - would be extremely useful.  They could be added to the PDF itself, or stored as a separate \"overlay\" in whatever format. \n\nI have looked for this feature in various applications, and the only programs that support it are scribus-like applications, which try to load the whole document at once, rendering them useless for documents that consist of (say) four hundred pages scanned at 600 DPI.  (Of course, DjVu is a good format for this, but none of the programs supporting it permit annotations either.)"
    author: "Andrew"
---
It might be late but that is because April's application of the month covers one of the finest additions to KDE 3.4: <a href="http://kpdf.kde.org/">KPDF</a>.  The application overview takes us through the powerful features in KPDF: thumbnails, contents, scrolling, zooming and searching.  We also have an interview with one of the creators of KPDF, Albert Astals Cid.  He tells us how KPDF got started, about Free Software use in Spain and where we should visit on our way to M&aacute;laga.  Enjoy App of the Month in <a href="http://www.kde.nl/apps/kpdf/">Dutch</a>, <a href="http://kde.org.uk/apps/kpdf/">English</a>, <a href="http://www.kde-france.org/article.php3?id_article=136">French</a> or <a href="http://www.kde.de/appmonth/2005/kpdf/index-script.php">German</a>.




<!--break-->
