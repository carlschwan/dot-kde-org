---
title: "Gambas 1.0 Released"
date:    2005-01-10
authors:
  - "jriddell"
slug:    gambas-10-released
comments:
  - subject: "Kbasic"
    date: 2005-01-10
    body: "And what about Kbasic and Hbasic?\n\nhttp://www.kbasic.org\n\nhttp://hbasic.sf.net\n\n"
    author: "Lampe"
  - subject: "Re: Kbasic"
    date: 2005-01-10
    body: "What about them??"
    author: "Christian Loose"
  - subject: "Re: Kbasic"
    date: 2005-01-10
    body: "KBasic has gone commercial I think. HBasic is underexposed (don't know why). Anyone has more knowledge about these projects?\n\nCiao' \n\nFab"
    author: "fab"
  - subject: "Re: Kbasic"
    date: 2005-01-11
    body: "> HBasic is underexposed (don't know why). Anyone has more knowledge about these projects?\n \nhm, because it's name doesn't start with a either a K or a G ? :-O\n"
    author: "cobaco"
  - subject: "Re: Kbasic"
    date: 2005-01-18
    body: "KBasic has gone proprietary. I assume he has thrown out all the GPL code and started from scratch, because the Qt proprietary license prohibits you from starting on the GPL version and porting to the proprietary one: \n\n========================================================\n    http://www.trolltech.com/developer/faqs/license_gpl.html#q15\n\n    Q:  Can we use the Open Source Edition while developing our non-opensource application and then purchase commercial licenses when we start to sell it?\n\n    A:   No. Our commercial license agreements only apply to software that was developed with Qt under the commercial license agreement. They do not apply to code that was developed with the Qt Open Source Edition prior to the agreement. Any software developed with Qt without a commercial license agreement must be released as Open Source software.\n========================================================\n\nAnyway, his development seems to be focused on Windows now, as his initial demo releases were Windows-only (he's put out a Linux one now, without source.)  Nonetheless, the price isn't bad (EU$25) and his goals are ambitious (100% code compatibility with VB.)  If he ever actually releases the product it'll be interesting.\n\nHBasic seems to have stagnated since about last April.  From what I understand after perusing their mailing list archives, the author seems a little more reluctant to accept code from outside sources than the Gambas author, so that could explain how Gambas picked up momentum as HBasic was losing it.  The current version isn't very stable, but it does some things (like native code compilation) that will be a long time coming for Gambas.\n\nThere are other RAD BASIC environments, but they are either antiques (XBasic) or orphaned (Kylix, Phoenix BASIC.)  Gambas seems to be the only one with any life at present, but of course I'm biased since my company hosts the Gambas wiki.\n"
    author: "raindog"
  - subject: "Re: Kbasic"
    date: 2005-01-19
    body: "Wow, you mentioned gambas at the very end.  I was all about to flame KBasic and XBasic ;)\n\nGambas may still be interpreted but all of the newer basics really are.  If HBasic is truly compiled then it likely includes a bunch of bloat from including some sort of basic binaries (like the runtimes for VB).  If it doesn't . . . then I am surprised.  I still believe that HBasic has to have some runtime libraries so it would still be required on a computer for it to run.  Thats what will stump basic on Linux.  No adoptions for it unless there truly was a \"killer app\" written in one of the basics.\n\n"
    author: "Barry Boyce"
  - subject: "Re: Kbasic"
    date: 2005-01-10
    body: "It seems useless to have a FAQ for you people.  The answer is right there:\n\nhttp://www.binara.com/gambas-wiki/bin/view/Gambas/WebSiteFAQ"
    author: "ac"
---
The first stable version of <a href="http://gambas.sourceforge.net/">Gambas has been released</a>.  Gambas is a Visual Basic like development environment (<a href="http://gambas.sourceforge.net/screenshots.html">screenshots</a>) where the programmes are cross-platform single executable files that can be deployed in a manner reminiscent of Java. The platform is in fact powerful enough that the IDE itself is written in Gambas.  For a brief introduction try this <a href="http://www.osnews.com/story.php?news_id=8849">OSNews article regarding Gambas</a> or the <a href="http://www.binara.com/gambas-wiki/bin/view/Gambas/WebHome">Gambas Wiki</a>. Gambas currently integrates with the KDE desktop environment by supporting Qt at the graphical component level as well as enabling DCOP interaction at the desktop level.

<!--break-->
<p>
Also note that the author Benoit Minisini <a href="http://www.fosdem.org/index/speakers/speakers_minisini">will be speaking at FOSDEM</a> and will accordingly be interviewed.  If you have questions for him, please feel free to send them to <a href="mailto:fosdem@gmail.com">fosdem@gmail.com</a>.</p>
