---
title: "Code Skipper Qt Community Resource Site Launches"
date:    2005-06-07
authors:
  - "aatamas"
slug:    code-skipper-qt-community-resource-site-launches
comments:
  - subject: "Articles should teach better style"
    date: 2005-06-07
    body: "Unfortunately, at least the \"3D Programming\" article teaches a questionable programming style. It leaves the destructor of Cube absent, even though there is memory to free. And in Cubes constructor we have the problematic multiple invocation of \"new\", without any provision for the case that one of these operations fails. Those two issues deserve, at the minimum, a small statement like \"This is only example code, in a real program we would have to deal with ressource management.\", or something like that. Why the class does only store pointers to the Point Objects, and does not contain them in itself, is not explained too.\n\nOn another account, the site looks really nice!"
    author: "El Pseudonymo"
---
<a href="http://www.codeskipper.com">The Code Skipper</a>, a new free Qt community resource site has been founded to provide our community of developers with a place to meet. This is a site where tutorials and articles that can be found on a range of Qt related subjects including a <a href="http://www.codeskipper.com/article/17/">Programming with Qt tutorial</a>, <a href="http://www.codeskipper.com/article/23/">Building a Universal SQL Client</a> and <a href="http://www.codeskipper.com/article/21/">3D Programming</a>. The Code Skipper also contains a lot of code that can be easily integrated into your applications. Learn Qt
tricks from there and share your own ideas.



<!--break-->
