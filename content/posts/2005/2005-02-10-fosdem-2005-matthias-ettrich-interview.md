---
title: "FOSDEM 2005: Matthias Ettrich Interview"
date:    2005-02-10
authors:
  - "jriddell"
slug:    fosdem-2005-matthias-ettrich-interview
comments:
  - subject: "Elections"
    date: 2005-02-10
    body: "I'm not really agreeing with the idea of electing some sort of directing figure. This will bring unnecessary social tension, IMHO. (Yet I'm even reluctant to air my opinion on this too loud, as I'm way far away from internal social status of the project these days).\n\nThanks a lot for the great interview. Thanks, Mathias, for sharing again your always interesting ideas."
    author: "Inorog"
  - subject: "Re: Elections"
    date: 2005-02-10
    body: "It seems that the directing figure would be necessary (in his opinion) when contentious issues arise. His example is usability.\n\nI suppose things would happen quicker, and possibly be more satisfying to some people. Interestingly with other contentious issues such as D-BUS the core developers came to a concensus decision when necessary, and all is well because the people deciding have the confidence of most everyone. Not many would question Waldo or coolo, or dharald abilities and judgement in these matters. And much of their authority comes from paying attention to the issues and questions of others.\n\nWith usability there is a growing confidence in the usability group due to their good taste and way of working with the developers. This is a skill whose application will have a major effect on the project, and like Missouri, developers said \"show me\". They have, and now have acquired a certain authority, as much as anyone can have authority in KDE. If there was a vote for 'Usability Expert for KDE With Great and Eternal Powers' there may actually be candidates who could be anointed. A year or two ago this may not have been the case. (If this sounds like a rather long winded kudos to Aaron, it is :)\n\nThe best line of the whole interview:\n\nKDE is not a centralized body, but a vibrant community of hundreds of talented individuals. That means there's hardly anything that wasn't thought about at some point.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Elections"
    date: 2005-02-10
    body: "I think for outsiders it is very hard to understand how KDE is structured and who is 'leading'. One possible solution would be to give to the board of directors of KDE E.V. (Kalle, Eva, Mirko, Harri) the title of 'KDE leaders', i.e. official main points of contact for the KDE project. "
    author: "Charles de Miramon"
  - subject: "Re: Elections"
    date: 2005-02-11
    body: "Maybe KDE should rename itself from a \"project\" to a \"movement\" since the former traditionally invoices the association that some person absolutely needs to lead it otherwise the project is doomed to lead into a mess. The term movement, when thought of it as a movement around an idea, on the other hand more accurately describes what KDE really is, and there can as well be no leader as there can be many leaders."
    author: "ac"
  - subject: "Usability"
    date: 2005-02-10
    body: ">And last but not least, my top 3 favorite focus areas for KDE version 4: usability, usability, and usability.\n\n This is what I want to hear! And I do hope that a lot of the developers thinks the same, and have a responsible oppinion about the upcomming KDE-HIG!\n\n If KDE can reach the same superb level of usability as it has in technology it will no doubt be the only *real* choice for enterprise-vendors :-D\n\n Keep hacking on \"my baby\"... You are all doing a *wonderfull* job!\n\n~Macavity\n\n--\n\"And remember kids! If it aint broke... Hit it again!!!\""
    author: "Macavity"
  - subject: "Re: Usability"
    date: 2005-02-10
    body: "i hoped that one out of 3 would be marketing, since marketing works. see the other desktop, thay have very good marketing and they feel very corporate.\n\nthe reason why KDE is not just as well marketed is, IMO, also in what Matthias sais about the 'anarchial structure' of KDE.\n\nKDE rocks, yet we should make sure that we are also seen like a rock!\n"
    author: "cies"
  - subject: "Re: Usability"
    date: 2005-02-11
    body: "I agree. The best desktop may not be the one who wins, but the desktop with the better marketing. Let's take a look at Windows and Mac OS X. The best desktop/OS is Mac OS X, but it has a ridiculous success in the market compared to the other. KDE could also loose positions stolen by Gnome if we don't pay more attention to the marketing. And I wouldn't really like it."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Usability"
    date: 2005-02-10
    body: "ACK. Usability is important.\nStrangely, I found that more often than not good programmers\nare horrible user interfaces designers. In a strange way,\nthey come up with ideas that are so alien to anyone who is\nnot programming. Probably they go by whats easy to program not\nwhats easy to understand.\nMy sister is the ultimate usability tester because she is a novice\nuser but highly motivated to use the computer for more and more\nthings. Recently she wanted to copy a file to a different folder\nin Konqueror by drag and drop. Interestingly she asked my why\nafter selecting copy file from the menu the selection in the\ntree was not on the current folder but on the folder she copied\nthe file too even though the file pane was still showing the contents\nof the original folder. I must agree this can be very confusing\nfor a novice user. Also highly problematic are ICQ apps. I know no\nsingle app (neither Windows nor Linux) which is really user-friendly.\nAsking to select a server and ICQ network from a list on startup\nwithout any explanation is just too much. And i.e. Konversation is\nriddled with strange buttons labeled C,T,U,A or sth. like that the\nlast time I checked. Really strange that sth. like that EVER gets\ninto software.\n\n"
    author: "ac"
  - subject: "Re: Usability"
    date: 2005-02-11
    body: "Just as long as \"Useability\" isn't confused with removing functionality.\n\nUncluttering toolbars, providing sensible defaults, and providing more consistency are all good things, but \"Remove the ability to change anything via the GUI except what we decide is not too scary, and make the process of changing stuff outside that select set a hundred times _more_ scary\" like Gnome did is not a good thing.\n\nHowever, the KDE developers have shown fairly reasonable restraint along those lines in the past, so I have faith that they'll make the right choices."
    author: "mabinogi"
  - subject: "Re: Usability"
    date: 2005-02-11
    body: "\"Just as long as \"Useability\" isn't confused with removing functionality.\"\n\nWhy does everyone think that improving usability involves removing functionality? Is it because that's what the GNOME-folks did? Well, KDE is not GNOME, why should KDE-folks do it like GNOME-folks did it?\n\nIt _might_ involve removing some REDUNTANT functionality (having the same exact thing in numerous different places etc. etc.). But overall, the functionality would not be reduced one bit. It would just be made more accessible and usable.\n\n\"Uncluttering toolbars, providing sensible defaults, and providing more consistency are all good things, but \"Remove the ability to change anything via the GUI except what we decide is not too scary, and make the process of changing stuff outside that select set a hundred times _more_ scary\" like Gnome did is not a good thing.\"\n\nAnd, AFAIK, no-one is suggesting anything of the sort. So why did you get your panties in a bunch?"
    author: "Janne"
  - subject: "Re: Usability"
    date: 2005-02-11
    body: "Selective memory.\n\nRemember the rather vigorous debate about context menus in Konqueror where necessary function was proposed to be removed. Function that was there for a good reason.\n\nThis stuff isn't happening anymore largely due to the efforts to build a trust relationship between the usability people and developers.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Usability"
    date: 2005-02-11
    body: "ACK!\n\n However I do think that a [Setings]->[Configure Menus...] (which works totally like the [Configure Toolbars...]) should be added to the std. framework. This would have the following bennefits:\n\n a) Very rarely used/powergeek oriented menuitems can be removed (with \"fairly reasonable restraint\").\n\n b) It would allow creative programmers to get their (slighty weird?) functionality into the program, but not on the std. menus.\n\n c) It would allow sysadmins and IT-teachers to create and distribute situation-spicific menutrees without getting to grips with the .ui format. (This would be a true asset to education of the younger students, as the teacher could enable entries one by one in the order they are taught to use them.)\n\n But yes.. scraping everything down to nill has only one effect: the system becomes equally in-usable to both Real Hackers and Real Users...\n\n~Macavity\n\n--\n\"...Use the source, Mook\""
    author: "Macavity"
  - subject: "Re: Usability"
    date: 2005-02-12
    body: "Thank god usablity is something on people's minds today. This is the single biggest weakness of KDE! I really hope this is the prime focus of KDE 4."
    author: "Matt"
  - subject: "Usability"
    date: 2005-02-10
    body: "Full ack!\n\nEsp. Konqueror: I do not understand why I find the fullscreen mode switch in the Settings-Menu and not in the view menu. "
    author: "gerd"
  - subject: "Re: Usability"
    date: 2005-02-10
    body: "this is being addressed in the new HIG, yes. and it isn't just Konqi, that menu item appears (according to our standards, anyways =) in the same place in every KDE app "
    author: "Aaron J. Seigo"
  - subject: "Re: Usability"
    date: 2005-02-11
    body: "Yup, and we need a standard action that handles it. In fact I wrote one for kwintv that could be used...\n\nRich."
    author: "Richard Moore"
  - subject: "Toolbars"
    date: 2005-02-11
    body: "Everybody knows that the Konqueror (3.4 Beta2) toolbar is quite unusable for webbrowsing. Unfortunately there are kde-developers who think every webbrowser should have copy/cut/paste/print icons in its toolbar, so that the location bar does not fit anymore into the main-toolbar. For me it's ok, as I know how to adjust KDE-toolbars.\n\nUnfortunately it's very difficult to share \"good toolbar layouts\" at kde-look.org  (at least I have not seen a \"clean-toolbar.tgz\").\n\nMaybe it would be a step in the right direction to separate the toolbar-configuration from the menu-configuration so that it would be possible to post ~/.kde/share/apps/konqueror/toolbar.conf to www.kde-look.org."
    author: "Asdex"
  - subject: "Just load the simple browser profile!!!"
    date: 2005-02-11
    body: "That's what its there for.  Its looks just like firefox!!\n\n-Sam"
    author: "Samuel Stephen Weber"
  - subject: "Re: Just load the simple browser profile!!!"
    date: 2005-02-11
    body: "Not here.\n\nWhen I select \"Settings/Load view profile/simple browser\", two toolbars appear: A toolbar with Cut/Copy/paste icons, a printer icon, a search icon, the font enlargement icons and the kget icon. When not using Kde 3.4 beta 2 but KDE 3.3.2, additionally a redundant security icon appears. Below that toolbar another one appears which holds the location field and a redundant search field.\n\nI'm using suse-provided RPM's, so maybe it's Suse's fault.\n\nI have attached a screenshot"
    author: "Asdex"
  - subject: "Re: Toolbars"
    date: 2005-02-11
    body: "Quite unsuitable? Quite unsuitable? My dear Asdex, really! You are too pusillanimous, indeed you are! My goodness -- everyone's browsing experience is absolutely ruined, absolutely ruined, I assure you, by those infernal buttons above the web page.\n\n It completely and utterly beyond me how anyone could ever have thought they were browsing the web when those ugly ulcers were inflicting themselves upon their brwowser experience. Horrible! Horrible! \n\nHarumph. \n\nRip them out. No quarter, absolutely no quarter for those insurrectionist toolbar buttons that don't know how to play the game for everyone. Off with their heads! Printing! The very idea itself. Copying! Bah, humbug! Pasting! Pfuiey. Cutting! Need I say more? Nobody could ever use Konqueror as it stands with those buttons."
    author: "Boudewijn"
  - subject: "Re: Toolbars"
    date: 2005-02-12
    body: "ahahahhahhahha.. thank you Mr. Rempt. brilliant."
    author: "Aaron J. Seigo"
---
<a href="http://www.fosdem.org/index/interviews/interviews_ettrich">Matthias Ettrich has been interviewed</a> ahead of his talk at FOSDEM.  The KDE founder talks about the relationship between the KDE and GNOME communities, the future of LyX and of course Qt 4 &amp; KDE 4.



<!--break-->
