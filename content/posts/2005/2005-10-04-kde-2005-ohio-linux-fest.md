---
title: "KDE at the 2005 Ohio Linux Fest"
date:    2005-10-04
authors:
  - "aseigo"
slug:    kde-2005-ohio-linux-fest
comments:
  - subject: "Kiosk"
    date: 2005-10-04
    body: "I don't know who is maintaining the Kiosk code for KDE, but I had a few people asking me about how they could lock down terminals.  I pointed them to kiosk of course, but maybe we should push it more or make it more well known?"
    author: "p0z3r"
  - subject: "Re: Kiosk"
    date: 2005-10-04
    body: "yes, we certainly need to promo kiosk more as it's extremely important to people doing larger roll outs. i've actually been talking with novell and they have started the process of getting till adams and myself involved with the engineering process for the next version of their zenworks product to support kiosk, which is excellent news of course.\n\nfor kde4, i'd actually like to see kiosk renamed to something that is more easily and obviously connected to desktop configuration management, definition and lockdown.\n\nit's a terrific tool (which isn't to say it couldn't be improved and extended to be even better), we just need to tell (more of) the world about it!"
    author: "Aaron J. Seigo"
  - subject: "Re: Kiosk"
    date: 2005-10-04
    body: "How about Lock Down?  ;)\n\nSeriously, if it were integrated into the Control Center or otherwise placed so people tripped across it, parents could also use it for their kids.\n\nFor administrators, there's room for a list of existing features in the press release for each KDE release -- the capabilities of kiosk should be listed.  I don't mean the \"Highlights at a glance\" section; that lists the new features.  I mean an \"About KDE\" type section.\n\nDidn't there used to be two press releases with each release?  A \"New Release\" and \"About KDE\" one?  I don't seem to see but one for the 3.4 release."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kiosk"
    date: 2005-10-04
    body: "how about \"konfine\", \"konstrict\" or \"restriKt(or)\"?\n\nCould someone please start a poll whether people like the K in kde-names?"
    author: "ben"
  - subject: "Re: K in name (was Kiosk)"
    date: 2005-10-04
    body: "I don't need a poll to figure out people do not like unimaginative names starting with a K. Just read slashdot to drive that point very painfully home.\n\nOh, the painful part might be because its slashdot, but you still get the picture :)"
    author: "Thomas Zander"
  - subject: "Re: K in name (was Kiosk)"
    date: 2005-10-04
    body: "Yeah, I always go to slashdot for all my advice. </sarcasm>"
    author: "madpenguin8"
  - subject: "Re: Kiosk"
    date: 2005-10-04
    body: "\"i've actually been talking with novell and they have started the process of getting till adams and myself involved with the engineering process for the next version of their zenworks product to support kiosk, which is excellent news of course.\"\n\nThat's quite interesting. For Novell's desktop Linux roll-out I would assume something like that would be pretty essential for Zenworks to actually be useful. Also, tell Novell and others about Klik. Perhaps talk to them about supporting Klik (well done Kurt) in Zenworks (Klik installation of Zenworks?) as well and looking at what further work needs to be done. I believe that software installation really does need to be coupled with something like Klik or Autopackage, and I cannot for the life of me understand why the vast majority of distros aren't interested. Yes, they want people to make specific packages for their distro, but it isn't helping anyone or themselves.\n\n\"for kde4, i'd actually like to see kiosk renamed to something that is more easily and obviously connected to desktop configuration management, definition and lockdown.\"\n\nGroup Policy?! *Ducks* Don't know, but something that identifies it as a way to manage KDE desktops.\n\n\"it's a terrific tool (which isn't to say it couldn't be improved and extended to be even better), we just need to tell (more of) the world about it!\"\n\nIt certainly is. However, what it needs is some good graphical tools that are integrated properly into KDE - something integrated properly in the new KDE 4 Control Centre perhaps with management tools in one place. I use Group Policy and AD with Windows all the time unfortunately, and many people harp on about how great they all are, but usability-wise for an administrator the management tools are a real pain. There's a lot of different graphical tools spread in different places and they're obviously all produced by different people.\n\nAlso, it's really clear how a networked infrastructure like Kiosk is so related to things like LDAP, distributed authentication etc. and all the other things, unfortunately, people are using Active Directory for with Windows today. If desktops like KDE can be supported by that back-end server infrastructure (which open source software has proven itself to be good at) then the outlook for open source desktop will be much brighter. Get rid of Exchange for Kolab and replace AD with LDAP and a KDE Kiosk framework - eliminate the infrastructure that keeps people locked in. Use that excellent open source, server-based infrastructure, build the management infrastructure around it with KDE and you've got everything you need to support an open source desktop. Rinse, repeat, profit, world domination!"
    author: "Segedunum"
  - subject: "Re: Kiosk"
    date: 2005-10-04
    body: "Best comment I've read for a long time!"
    author: "David"
  - subject: "Re: Kiosk"
    date: 2008-11-15
    body: "see my article for necessary theirs evolution kiosk admin tools for kde 4.\n\nhttp://mandriva.education.free.fr/?p=35\n\n\n\nNous venons de voir toutes les possibilit\u00e9s de Kiosk Admin Tools, maintenant je vais vous parler de ce qu\u0092il manque \u00e0 ce logiciel pour, d\u0092une part \u00e9galer les logiciels sous windows et d\u0092autre part prendre en charge les sp\u00e9cificit\u00e9s de l\u0092environnement linux.\n\n   1.\n\n      Les disques amovibles\n\nAujourd\u0092hui l\u0092utilisation de cl\u00e9 usb ou de disque dur externe est monnaie courante. Il faut donc pouvoir l\u0092autoriser ou pas suivant le contexte et l\u0092utilisation. (p\u00e9riode d\u0092examen par exemple\u0085)\n\n   2.\n\n      Les imprimantes\n\nDans les structures actuelles ont utilisent beaucoup les imprimantes r\u00e9seaux. Pouvoir fixer l\u0092imprimante a utiliser par d\u00e9faut, le quota d\u0092impression, couleur ou noir et masquer les imprimantes des autres b\u00e2timents me para\u00eet indispensable.\n\n   3.\n\n      Les lecteurs r\u00e9seaux\n\nDe plus en plus on utilise des architectures r\u00e9partis sur plusieurs machines. Par exemple beaucoup de CDI utilisent SLCD. Il faudrait pouvoir forcer le montage d\u0092un lecteur sur une lettre pr\u00e9cise dans un profil donn\u00e9.\n\n   4.\n\n      Les options 3D et les widgets (KDE4)\n\nL\u0092apparition de compiz fusion, des widgets dans opera etc\u0085 sont autans de gadgets peut utile \u00e0 la transmission du savoir, parfois m\u00eame d\u00e9routant pour l\u0092enseignant qu\u0092il est n\u00e9cessaire de pouvoir contr\u00f4ler finement.\n\n   5.\n\n      Les r\u00e9glages sons\n\nLes postes achet\u00e9 \u00e0 ugap le plus souvent sont des nec powermate qui poss\u00e8de un \u00e9cran avec des haut parleurs. C\u0092est tr\u00e8s pratique pour certain cours mais un calvaire pour d\u0092autre. La possibilit\u00e9 de pouvoir les verrouiller sur certain profil devient n\u00e9cessaire\u0085\n\n   6.\n\n      Les r\u00e8gles de navigation (contr\u00f4le parental)\n\nLe plus souvent les \u00e9tablissement sont maintenant \u00e9quip\u00e9s de pare-feu avec squid permettant de filtrer l\u0092internet. Il n\u0092en va de m\u00eame pour les \u00e9coles ! Et m\u00eame si l\u0092on regarde scribeNG l\u0092apparition d\u0092une surcharge de filtrage est apparu sur une demande des administrateurs. Mandriva 2008.1 en est \u00e9quip\u00e9, reste \u00e0 le g\u00e9r\u00e9 globalement et pas poste par poste.\n\n   7.\n\n      L\u0092interaction avec le pare-feu Nufw de INL\n\nLe fait que les utilisateurs changent r\u00e9guli\u00e8rement de poste impose de g\u00e9rer les interdictions par rapport \u00e0 un identifiant et pas une adresse IP. C\u0092est la probl\u00e9matique \u00e0 laquelle Nufw r\u00e9pond.\n\n   8.\n\n      Les r\u00e9seaux sans fil (wifi et bluetooth)\n\nDe plus en plus d\u0092\u00e9tablissements s\u0092\u00e9quipe du wifi ainsi que de p\u00e9riph\u00e9rique communiquant par le biais du bluetooth. Il n\u0092est pas possible de laisser la configuration aux utilisateurs. Il faut donc une configuration pr\u00e9-d\u00e9finie et fig\u00e9.\n\n   9.\n\n      Cacher l\u0092acc\u00e8s \u00e0 certains r\u00e9pertoires\n\nM\u00eame si l\u0092acc\u00e8s aux r\u00e9pertoires est bien verrouill\u00e9 sous linux, le fait dans un avoir une grande liste peut perturber voir donner une impression de complexit\u00e9 pour l\u0092utilisateur. Il faut donc pouvoir cacher des r\u00e9pertoires tel que : etc, root, usr, opt etc\u0085\n\n  10.\n\n      Forcer des configurations syst\u00e8mes (maj, snapshots, synchronisation, sauvegarde etc\u0085)\n\nLes utilisateurs ne sont pas sens\u00e9s devoir accomplir des t\u00e2ches tel que la mise \u00e0 jour de l\u0092OS ou proc\u00e9der \u00e0 la synchronisation d\u0092un r\u00e9pertoire local vers le serveur. Il est donc important qu\u0092un syst\u00e8me de s\u00e9curisation des postes soit en mesure de fixer la r\u00e8gle en la mati\u00e8re et forcer son respect.\n\nIl est donc assez claire que dans ce domaine pr\u00e9pond\u00e9rant, linux est incapable de rivaliser avec windows. Pourtant des d\u00e9veloppements avanc\u00e9s existent sous linux. Une solution d\u0092avenir a \u00e9t\u00e9 d\u00e9velopp\u00e9 par novatice pour sa solution edutice. En quelques mots, cette solution permet de fixer un r\u00e9pertoire (le home) qui reste tel que l\u0092utilisateur la laiss\u00e9 \u00e0 la fin de sa session. Pour le reste le profil enregistr\u00e9 par l\u0092administrateur est remis en place \u00e0 chaque connexion (couleur, fonds d\u0092\u00e9cran, position des ic\u00f4nes sur le bureau etc\u0085).\n\nL\u0092avenir dans consistera \u00e0 faire \u00e9voluer Kiosk admin tool sur KDE4 en y ajoutant en autres les 10 points cit\u00e9s ci dessus et le coupl\u00e9 \u00e0 la solution utilis\u00e9e dans edutice.\n\nPourquoi ?\n\nParce que ce qui est imparfait est le fait qu\u0092aujourd\u0092hui on attribut un profil aux diff\u00e9rents groupes de poste (r\u00e9f\u00e9renc\u00e9 dans esu par \u00ab machines \u00bb et par \u00ab sous-parc \u00bb dans iaca).\n\nQuelle est la solution ?\n\nAvoir au moment de l\u0092authentification, en plus des champs \u00ab identifiant, mot de passe et domaine \u00bb un champs \u00ab profil \u00bb. Dans ce champs le professeur demandera aux \u00e9l\u00e8ves de choisir le plus adapt\u00e9 \u00e0 son cours (math\u00e9matiques, langues, documentations etc\u0085) ce qui aura pour effet d\u0092ouvrir une session avec un profil tel qu\u0092il a \u00e9t\u00e9 enregistr\u00e9 par le coordinateur mati\u00e8re ou l\u0092enseignant et d\u0092y ajouter une surcharge de Kiosk admin tools d\u00e9finie par l\u0092administrateur en concertation avec les diff\u00e9rents protagonistes."
    author: "jeff"
  - subject: "What a Great Phrase"
    date: 2005-10-04
    body: "\"The enthusiasm and excitement was palpable behind the table and those that visited fed off that energy.\"\n\nI'll have to remember that one."
    author: "Segedunum"
  - subject: "Gnome Office beats KOffice"
    date: 2005-10-04
    body: "http://www.abiword.com/release-notes/2.4.0.phtml\n\nYesterday Abiword 2.4 was released. It even supports one-the fly grammar check. I do not think KOffice Writer will beat Abiword or OpenOffice.\n"
    author: "Bernd"
  - subject: "\"Gnome Office\"?"
    date: 2005-10-04
    body: "Since when has there been a \"Gnome Office\"?  I thought there was just a bunch of office applications that used GTK that had nothing to do with each other?\n\nDid Abiword make their own grammar checking engine, or did they use a separate library for it?  If it is a third party library then KWord could easy add support for it.  Also does it work at all well, or is the engine so simple that if the sentence isn't in an incredibly simple structure it will be considered incorrect?\n\nIMHO Grammar check is an important feature (since my grammar sucks), but most checkers are even stupider than me.  Also I don't think 1 feature makes Abiword better than KWord."
    author: "Corbin"
  - subject: "Grammar checker"
    date: 2005-10-04
    body: "Not only are most grammar checkers stupid, they are usually not usable or extendable to other languages. How good is the i10n of this checker, is it possible to make usable for other languages? "
    author: "Morty"
  - subject: "Re: Gnome Office beats KOffice"
    date: 2005-10-04
    body: "Especially that there is no app like \"KOffice Writer\" in this universe. Looks like you're living in parallel one :P\n"
    author: "punisher"
  - subject: "Re: Gnome Office beats KOffice"
    date: 2005-10-04
    body: "Yes, and according to that page it also just now added support for sometihng all KOffice apps have had for years (the aility to embed other application's charts and data).\n\nBoth KOffice and GNOME-Office have their strong and weak points. KWords import and export for MS Office files is extremely lacking. And they also are lacking a grammar checker. BUt they are way ahead in the ability to embed other file formats, like SVG and spreadsheets and charts, into documents. They are also ahead when it comes to layout and presentation capabilities, mainly due to their framemaker-like approach to documents rather than the MS Word-like approach of abiword.\n\nIn conclusion, both have a way to go. And both are still playing catch-up with OpenOffice. But both are getting better all the time, and competition is always a good thing, especially with all collaberating on things like OASIS where appropriate.\n\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Gnome Office beats KOffice"
    date: 2005-10-04
    body: "> ... especially with all collaberating on things like OASIS\n\nExcept that AbiWord will not switch to that format, unfortunately:\n\nhttp://www.abisource.com/twiki/bin/view/Abiword/FaqOASISSupport"
    author: "ac"
  - subject: "Re: Gnome Office beats KOffice"
    date: 2005-10-04
    body: "Since it's a Gnome app it would probably require a total rewrite."
    author: "bla"
  - subject: "Re: Gnome Office beats KOffice"
    date: 2005-10-04
    body: "ROTFL"
    author: "Anonymous"
  - subject: "Actually ... "
    date: 2005-10-05
    body: "... I'm a National Weather Service Information Technology Officer out of the Louisville, Kentucky office.  I was just telling Aaron that the NWS will be using KDE as the default desktop very soon ... no exceptions.  I'm more of a GNOME guy, but after Aaron's presentation I decided that KDE may have a lot more to offer than I had previously realized.  Anyway ... I'm looking forward to the switch over and would really appreciate a copy of your presentation."
    author: "Tony Freeman"
  - subject: "Re: Actually ... "
    date: 2005-10-05
    body: "oops! it was your noaa email that confused me =)\n\nstill, great news about the NWS. i'll have audio and perhaps even video soon though and i'll be posting that when i get it. having just the slides wouldn't be of much use as they were, except for 2 of the slides, just icons =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Actually ... "
    date: 2005-10-05
    body: "Aaron,\n\nI would love to have a copy of your presentation as well. We are a total MS shop on the cusp of transistion to Linux (SuSe). I have been showing the Board of Directors the possibilities and doing cost comparisons for them. Looking at OpenMFG and SugarCRM as replacement for our current ERP and CRM apps. Talking to Sun rep for site license for Star Office 8. While we can not shake Windows 100% since some of the propritary apps we use currently do not have a cross over; but we will seriously drop one heck of a hammer on the server and desktop usage. "
    author: "Rick Montgomery"
---
With over 700 attendees and a <a href="http://www.ohiolinux.org/speakers.html">speaking track</a> packed full of goodies, the one-day <a href="http://www.ohiolinux.org">Ohio Linux Fest</a> held this past Saturday in the city of Columbus was an unqualified success.  KDE had a booth at the event which was kept exceedingly busy the entire day. Most of the people who visited the booth mentioned that they use KDE and several asked that we pass on their appreciation to the entire KDE team for all the work that they have put into it over the years.
<!--break-->
<p><div style="border: thin solid grey; float: right; margin: 6px; padding: 6px; width: 300px;">
<a href="http://aseigo.bddf.ca/dms/1/243_ohio_booth.jpg"><img src="http://static.kdenews.org/jr/ohio-booth.jpg" width="300" height="259" /></a><br />
KDE booth staff from left to right: Ryan Nickel, Jaison Lee and Steve Miller
</div>
The booth itself was manned by <a href="http://netdragon.sf.net">SuperKaramba</a> hacker Ryan Nickel, KJots hacker Jaison Lee, KDE user extraordinaire Steve Miller and Aaron Seigo. Laptops showcased both KDE 3.4 and the upcoming 3.5 for the unending stream of booth visitors who kept the KDE team on their toes throughout the day. The enthusiasm and excitement was palpable behind the table and those that visited fed off that energy.</p>

<p>KDE highlights of the show included Aaron's talk "KDE: Every Day Use and Hidden Gems" which packed the conference room with upwards of 350 people, a guest spot featuring <a href="http://www.kubuntu.org">Kubuntu</a> in <a href="http://www.whiprush.org/2005/10/ohio_linuxfest_.html">Jorge Castro's talk on Ubuntu</a> and the numerous fans and fans-to-be that came by the KDE booth with questions and comments galore.</p>

<p>SuperKaramba, the Kiosk desktop management framework, KStars, amaroK and  many of the usual suspects such as Konqueror's amazing array of capabilities were hits with booth visitors. Among the visitors was a system administrator with the <a href="http://www.noaa.gov">NOAA</a> who is overseeing the roll out of KDE desktops in their Columbus office, a class from DeVry who were researching KDE as well as a pair of people from the local university and library who maintain KDE systems for the staffers there.</p>

<p>To top it off, the after-party was a blast and everyone seemed to have a great time at the DJ'd event. You can read more about the LinuxFest at <a href="http://aseigo.blogspot.com/2005/10/ohio-linux-fest.html">Aaron's</a> and <a href="http://www.p0z3r.org/2005/10/kdegnomepicturesbeer.html">Ryan's</a> blogs as well as on the <a href="http://www.ohiolinux.org">Ohio LinuxFest website</a> itself. We look forward to next year's Ohio LinuxFest and talking with even more people at the KDE booth in 2006!</p>






