---
title: "Linux and KDE Provide Rugged Desktops in Uganda, Africa"
date:    2005-06-18
authors:
  - "bkendrick"
slug:    linux-and-kde-provide-rugged-desktops-uganda-africa
comments:
  - subject: "also include the OpenOffice.org!"
    date: 2005-06-18
    body: "I wish it should be \"also include the *koffice* productivity suite\". Perhaps with QT 4, Koffice will become a good competitor for OpenOffice.org, isn't it?"
    author: "Fast_Rizwaan"
  - subject: "Re: also include the OpenOffice.org!"
    date: 2005-06-18
    body: "well, at least that's what i'm hoping for :D"
    author: "superstoned"
  - subject: "Re: also include the OpenOffice.org!"
    date: 2005-06-18
    body: "> I wish it should be \"also include the *koffice* productivity suite\".\n\nJust my thoughts ;)\n\n> Perhaps with QT 4, Koffice will become a good competitor for OpenOffice.org, isn't it?\n\nKOffice is currently getting a lot of momentum, it is already a competitor in many aspects. KOffice offers better KDE integration and has many more components than OpenOffice.org has - just think of Krita, Kexi, Kivio (!!) and the upcoming KPlato ... or Kugar and Karbon14. OpenOffice.org is really just *better* in some aspects like stability and features for, say, text processing and spreadsheets (or presentations). On the other hand I find KOffice more usable with these components already. There are also a number of improvements in the current 1.4 release (watch the dot).\n\nBy the way, KOffice has the better basis - KDE. OOo certainly needs many developers to work on their (custom) basis. This is also what brings KOffice further technology wise in the long run.\n\nBut that shouldn't be any message of a KOffice against OOo kind. OOo has some good aspects as well, like good MS Office compatibility or being available on Windows nativly (not that I need either of these features...). Since OOo takes care of _this_ job, KOffice does not need to do it, which allows KOffice to become a very lightweight and user friendly office suite. By the way... KSpreads new Excel import filter is extremely good, even better than OOo Calc in my experience."
    author: "Raphael Langerhorst"
  - subject: "KOffice \"4\""
    date: 2005-06-19
    body: "Karbon seems a bit abandonned, inkscape [www.inkscape.org] is my choise now for vector drawings. And also the gimp [www.gimp.org] is still my choise for pixel drawing. But...\n\nWhen i look at the progress Krita [http://www.koffice.org/krita/screenshots.php] is making, and what Qt's new painting backend \"arthur\" [http://doc.trolltech.com/4.0/qt4-arthur.html] can do [http://qt4.digitalfanatics.org/articles/rc1.html], i feel the API's/technologies/frameworks are once again great for KDE.\n\nAnd since KOffice adopts the OASIS (OpenOffice.org) standard as the native format, OOo's 'great import filters' will probably soon work as part of KOffice.\n\nThe KDE future only get brighter.\n\nCies Breijs"
    author: "cies breijs"
  - subject: "Re: KOffice \"4\""
    date: 2005-06-19
    body: "> And since KOffice adopts the OASIS (OpenOffice.org) standard as the native \n> format, OOo's 'great import filters' will probably soon work as part of KOffice.\n \nI wouldn't count on it.  The OOo import filters convert a file (e.g. a .doc) into an  OOo-specific internal representation.  That doesn't help KOffice.  \n\nOf course you could use OOo to import the file, export it to the OASIS file format, and then start KOffice and import the OASIS file.  But that wouldn't be the same as having the filters \"work as part of KOffice\".\n\n"
    author: "cm"
  - subject: "Re: KOffice \"4\""
    date: 2005-06-20
    body: "Pierre Stirnweiss has been active fixing issues with Karbon recently, but, well, Karbon activity is much dependent on the final release of either kcanvas or Qt 4. I don't which one the Karbon people will choose (although, Rob Buis is the maintainer of both Karbon can kcanvas). In the meantime, I hope to spend at least a day of the upcoming Krita hackathon with Rob deciding how to best integrate Karbon and Krita gui-wise. \n\nThere have been previous attempts, see the kopainter lib, but those never panned out because neither application had advanced enough to know what kind of things colors or patterns needed to support.\n\nAbout the filters: sorry, that won't work. The OpenOffice interals are horrible and it's impossible to separate the filter code from the rest of OpenOffice. Sharing a fileformat doesn't work. What one could do, but it would be awfully slow, is making OpenOffice a file conversion server using its automation interface from a KOffice filter. KOffice calls the filter, OO converts the file to OASIS and the filter reads it back in. But, well, it would be unworkable."
    author: "Boudewijn Rempt"
  - subject: "KDE narrows the Digital Divide :)"
    date: 2005-06-18
    body: "Thanks to Linux and KDE, the Digital divide of poor and rich is getting narrower. And for Africa and Asia, GNU/Linux + KDE is the way to go! Thanks guys, it really feels good to learn that our KDE is helping the deserving!"
    author: "Fast_Rizwaan"
  - subject: "Re: KDE narrows the Digital Divide :)"
    date: 2005-06-19
    body: " Indeed it does... indeed it does :-)\n\n It's when i read articles like this one that Richard Stallman's argumentation that \"Free Software == Making the world a better place\" and \"Open Source == A great development technique\" kind of makes sense to me ;-)\n\n Too me the former is much more important.. I'm just glad that corporate management took the bait on the latter, so the needing and have-less can benefit from it too :-D\n\n~Macavity"
    author: "Macavity"
  - subject: "great"
    date: 2005-06-18
    body: "I'm very glad about such projects. I think Open Source in general has much potential to help poorer countries by providing low cost and good quality software. Thanks everyone at Inveneo and ActionAid!"
    author: "Raphael Langerhorst"
  - subject: "KDE's really just the desktop they use..."
    date: 2005-06-18
    body: "As I've seen on one of the screenshots, they also don't use KDE programms for internet use. The Mozilla suite replaced KMail and other KDE components. :("
    author: "Patrick Trettenbrein"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-18
    body: "Honestly, I think Firefox is still superior to KHTML/Konqueror for a common web usage. It's something similar to the situation between OO.org and Koffice. The KDE alternatives are smaller and nicer in many ways, but they are not good enough (yet!) in many cases. I like Kmail a lot, though.\n\nYeah, forgive my not-so-good english."
    author: "Anonymous"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-18
    body: "Firefox are definitely not superior to KHTML/Konqueror, perhaps slightly better but not superior. The majority of sites who break/render incorrectly in Konqueror are caused by sites own broken browser identification, simply changing the browser id fix the problem. Those sites remaining which does not work with changing of the browser id, does nearly always not work in Firefox either. Making the difference between the two rather minor. The KDE intergration more than makes up for the minor issues.\n\nAs for KMail, I'd dare say it's better than Thunderbird and the intergration KDE gives increases the value of KMail even more."
    author: "Morty"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-19
    body: "With Konqi, how many combinations of browser identifications must one try on some sites before giving up and going back to FF which Just Works (TM)? And then add in the possible crypto options and you find your self in the much too common situation of having to use multiple browsers with KDE or drifting to FF.\n\nHandling \"Browser Identification\" issues and crypto settings is something a (gasp!) computer should handle, not the user.   \n\nUgly renderings I can handle. Not being able to use a web site in this day and age is just not right.  Things are improving, but not fast enough when compared to FF.  "
    author: "sc"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-19
    body: "> And then add in the possible crypto options ...\n\nI don't know what you're talking about here. \n\n\n> Handling \"Browser Identification\" issues [...] is something a (gasp!) computer should handle, not the user.\n\nIMHO this is utter nonsense.  How should a computer know that a site does not work as expected and try a different browser identification? \n\n\n"
    author: "cm"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-19
    body: "Nonsens, you seldom have to try more than one or two. If you in addition use some intelligence when choosing. I usually do one variant of each  IE-Opera-Safari-Mozilla(usually works on of the first two), or perhaps Netscape 4 if the site is old. And on the few sites when it's needed you can let the setting stick and never have to change it for those sites again. And your comment about the crypto settings are even more nonsens, use the default. It's the same as you have in Firefox."
    author: "Morty"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-20
    body: "Have you tried a banking site with FF which requires IE only? Does FF automagically change its useragent to access that site? No.\n\nStick the the points that have relevance and do not spread FUD about any browser which you personally may not prefer. Konqueror, in fact, can render some of those \"buggy\" sites as well where FF fails completely. One such example is my one my previous employer's employee portal. It had some wierd javascipt popup menus which FF consistently failed to see (even upon changing the useragent to IE). Konqueror, on the other hand (yes, I had a linux desktop) could see those menus without any change of useragent. So, IMO Konqueror is far superior than FF in this case!\n"
    author: "Kanwar"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-18
    body: "I'd have to disagree. I don't use Firefox all that much though I have played around with it just to see what all the hub bub is about. It's nice and all but I wouldn't say it is superior to Konqueror. The latter I use enough to say it is my exlcusive browser and have found very few problems with it. There are some Firefox plugins that are nifty that I think konqueror could use like ad blocking for example. But fiddling with CSS can help minimize those issues.\n\nOver all I'd say konqueror is more than good enough."
    author: "stumbles"
  - subject: "Re: KDE's really just the desktop they use..."
    date: 2005-06-18
    body: "And Konqueror has got ad blocking now, or when 3.5 is released at least. I only hope for a nicer looking image to replace the blocked ad, hopefully something scalable like a svg. "
    author: "Morty"
  - subject: "Awesome!"
    date: 2005-06-18
    body: "Make no mistake, if you contribute to the KDE project you are helping to change the world. I look forward to the online party when we get our first KDE commits from Uganda or people living in similar African villages. Eric Raymond is utterly wrong about how Free Software needs to become more business friendly, and rebrand itself as 'Open Source'. The game is to overturn capitalism as an engine of innovation, and move to a new model based on intellectual capital (freely available in African villages)."
    author: "Richard Dale"
  - subject: "Re: Awesome!"
    date: 2005-06-18
    body: "> I look forward to the online party when we get our first KDE commits from Uganda or people living in similar African villages.\n\nAre you serious!? These people hardly will use the public internet stations for KDE development but will be more interested in researching crops prices, selling crops, organizing transports, telephoning neighbour villages etc."
    author: "Anonymous"
  - subject: "Re: Awesome!"
    date: 2005-06-18
    body: "I think he means it! Contributing is not that hard,\nthe people there just as smart as here, they have\na will to do something back, etc... and they will\nhave special wishes, luckely when you have the source,\nyou can solve your own wishes.\n\nSo yes, it will happen that a geek from uganda will\nfix things we have messed up. It might not start with\ncode, but translations, documentations, artworks are\njust as nobel contributions.\n"
    author: "naomen"
  - subject: "Re: Awesome!"
    date: 2005-06-18
    body: "\"Are you serious!?\"\n\nAbsolutely. You talk about how adults will use the new infrastructure to help with their farming businesses, and indeed that is how internet use in Uganda will probably start. Their children will have no such restraints, they will do whatever interests them. They are all online, and can do anything that anyone else can do online; ie learn stuff and collaborate with the rest of the World using peer review (ie 'the Scientific Method') to invent stuff, as opposed to the money based capitalistic approach which worked well in the last century."
    author: "Richard Dale"
  - subject: "Re: Awesome!"
    date: 2005-06-21
    body: "Perhaps not in the rural places where these guys set up the solar-powered Linux terminals, but there are:\n\n  LinuxChix user group in Africa: http://www.africalinuxchix.org/\n  East African Centre for Open Source Software: http://www.eacoss.org/\n  Uganda Linux User Group: http://linux.or.ug/  (site seems to be down)\n\nSo there are _already_ Linux geeks there! :)\n\n-bill!\n"
    author: "Bill Kendrick"
  - subject: "Re: Awesome!"
    date: 2005-06-21
    body: "\"So there are _already_ Linux geeks there! :)\"\n\nYes, and also there was the article about schools in Namibia recently on the dot news. So maybe we don't need a party to celebrate because it's already happening.\n\nWhat I find exciting is this is describing a rural village with no electrical power, going straight to being online and in two way contact with the rest of the world via the internet and telephone. "
    author: "Richard Dale"
  - subject: "Re: Awesome!"
    date: 2005-06-19
    body: "Second that!\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Awesome!"
    date: 2005-06-20
    body: "\"The game is to overturn capitalism as an engine of innovation\"\n\nNot surprising that it comes from a guy who says that Fidel Castro is his hero.\n\nIs Hitler your hero too Richard?\n\nWho can work with this guy except other marxists?"
    author: "Dave"
  - subject: "Re: Awesome!"
    date: 2005-06-20
    body: "How is this related at all?"
    author: "ac"
  - subject: "Re: Awesome!"
    date: 2005-06-20
    body: "I'm not a marxist, and have never been a marxist thankyou very much. Fidel Castro was not a marxist when he was trying to overthrow Batista in the 1950s, which is what I was discussing in my blog about him. I find your reference to me being a Hitler fan offensive. \n\nFor a drug company to develop a new drug, or for Intel to develop a new fab it involves huge amounts of money and that is a capitalist enterprise. Even so those efforts can be greatly helped by community collaboration, and for instance IBM are opening up the development of their new cell processor to the Free Software/Open Source community. But for many other developments money has become much less important than it was, and intellectual capital is what counts. Did Einstein need large amounts of money to become the greatest scientist of the 20th century? No not at all. He could have been living in a Ugandan village"
    author: "Richard Dale"
  - subject: "KLettres in Luganda"
    date: 2005-06-19
    body: "Recently I got the sounds for Luganda support in KLettres, thanks to John Magoye and Cormac Lynch. \nIt's great news to see such efforts, my family was involved with ActionAid when we were in the UK and I liked their way of giving people responsability (Giving People Choices was one of their motto). I also liked the concrete actions I took, writing to governments and such. That was not us giving some money to feel good, no. That was us fighting for a better world, here and in other countries. "
    author: "annma"
---
<a href="http://www.inveneo.org/">Inveneo</a>, a San Francisco non-profit, together with <a href="http://www.actionaid.org.uk/">ActionAid</a>, an international agency whose aim is to fight poverty worldwide, have <a href="http://www.inveneo.org/?q=uganda">installed their first rugged Linux desktop systems</a> in western <a href="http://en.wikipedia.org/wiki/Uganda">Uganda</a>. The systems <a href="http://www.inveneo.org/img/uganda/large/DSC_1434.jpg">run Linux and KDE desktops</a>, and also include the OpenOffice.org productivity suite.

<!--break-->
<p>Mark Summer writes "<i>the system is up and running since this past Wednesday (June 8th). We have installed 5 units, 4 of which are in villages with no access to power. The system provides Internet access and phone capabilities to the users. Phone calls among the connected villages are free of charge, with the ability to place and receive calls to the Ugandan phone network. The systems are linked using 802.11 WiFi links.</i>"</p>

