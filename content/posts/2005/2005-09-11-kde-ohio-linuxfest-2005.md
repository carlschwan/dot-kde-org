---
title: "KDE at the Ohio LinuxFest 2005"
date:    2005-09-11
authors:
  - "mmeffie"
slug:    kde-ohio-linuxfest-2005
comments:
  - subject: "KDE booth"
    date: 2005-09-11
    body: "I'll also be there helping with the KDE booth.\nBe sure to come and say hi.\n\n-Ryan"
    author: "p0z3r"
  - subject: "LinuxFest"
    date: 2005-09-12
    body: "This isn't 100% on topic, but I was wondering if there are any such events in Southern California. I'd love to be able to meet and mingle with fellow KDE peeps in my own area. Maybe even help with a booth."
    author: "Chris"
  - subject: "Re: LinuxFest"
    date: 2005-09-13
    body: "Yes, SCALE is in Feb 06, http://www.socallinuxexpo.com/\nFor those in the NorthWest, there is LinuxFest Northwest, http://www.linuxnorthwest.org/"
    author: "Michael Meffie"
  - subject: "Re: LinuxFest"
    date: 2005-09-13
    body: "How about in the NYC area? Thanks"
    author: "Jeff K"
  - subject: "Re: LinuxFest"
    date: 2005-09-13
    body: "I don't know... maybe the Ohio LinuxFest 2005 :)"
    author: "Michael Meffie"
  - subject: "Re: LinuxFest"
    date: 2005-09-14
    body: "OSDW will be in San Diego in October 12-15.  That's in California. ;)"
    author: "Ryan"
---
KDE will be present at the <a href="http://www.ohiolinux.org/">Ohio LinuxFest 2005</a>. Aaron Seigo, KDE developer, will be <a href="http://www.ohiolinux.org/speakers.html#aarons">talking</a> at the conference and KDE will have an <a href="http://www.ohiolinux.org/exhibitors.html">exhibit</a> in the .org section of the expo area. The <a href="http://www.ohiolinux.org/about.html">Ohio LinuxFest 2005</a> is a free conference and event for free software enthusiasts in Columbus, Ohio on October 1, 2005. See <a href="http://www.ohiolinux.org/schedule.html">the schedule</a> and <a href="http://www.ohiolinux.org/register.html">registration page</a> for more information.





<!--break-->
