---
title: "The Case for Konqueror"
date:    2005-10-25
authors:
  - "btackat"
slug:    case-konqueror
comments:
  - subject: "Don't we just love KDE?"
    date: 2005-10-25
    body: "I read all that rubbish wherein people were moaning about everything beginning with K and then I read this - demonstrating why its all amazing /fanboy "
    author: "gerry"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-25
    body: "Yeah!  That K argument was totally ridiculous!  They were all \"we're KDE users too\" but you could totally tell they were really just Windoze users wanting to make everything like Windoze.  And we were calling them on it and saying \"you can keep your Windoze\" and they were all like \"We don't even USE Windows\" LOL yeah right like you could go very long using nothing but KDE.\n\nAnd they were all like \"all I'm doing is pointing out that these things are inconsistent in KDE and might be better if we settled on one standard convention, feel free to disagree\" and we all banded together and said \"yeah we're free to disagree, but YOU AREN'T hahahaha looser!!!!!\"\n\nWe have to keep fighting the hard fight against dissent, cause that leads to bad stuff.\n\n/satire"
    author: "ca"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-25
    body: "Apple:  iSoftware\nWindows:  WinSomething, Something32\nGTK & GNU:  GSomething\nKDE: Something-with-a-K"
    author: "Corbin"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-25
    body: "You missed the initial discussion, but right now KDE has 4 divergent naming conventions:\n\n- Words chosen simply because they contain the letter K (Knights)\n- Words with K inserted in front of them (KWord, KSpread)\n- Words misspelled with a K instead of a C, Q, or other consonant (Kaffeine, Kuickshow)\n- Words with no K's at all (Quanta Plus)\n\nSummary of argument: some people think the inconsistency is a problem, others don't.  Many (but not all) of those who felt that the inconsistency wasn't a problem felt that this made pointing out the inconsistency a bad idea."
    author: "ac"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-25
    body: "Let's not forget amaroK =)"
    author: "[Knuckles]"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "and digiKam :)"
    author: "rinse"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-25
    body: "I saw the previous discussion on this (and notice I said \"Something-with-a-K\" and not KSomething).\n\nIf the K naming scheme is going to be abandoned, to what extent are apps going to be renamed?  Just things with a K in front?  Misspellings to have a K instead of a C or Q?  Any word that has a K in it?  What about 'konsole' which is the correct german spelling for the word?\n\nI personally prefer the K names, and I really hate the idea of changing apps to generic names like changing Kuickshow to Image Viewer or KWrite into Write.  If that would happen all the applications would seem to lose their identity, as if they weren't ANYTHING special, so they don't have a special name.\n\nIf you (or anyone that feels the way you do) can come up with replacement names (or naming scheme) for all the 'poorly' named applications I'm sure the KDE community will listen and decide if it would be a good idea to switch the names.  Most of the KDE contributors would probably feel that the time before KDE4 is released is the best time to change any names that need to be changed."
    author: "Corbin"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "\n\"If the K naming scheme is going to be abandoned, to what extent are apps going to be renamed? \"\n\nI personally don't expect that any application will be renamed.\n"
    author: "rinse"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "Agreed, I doubt that any application will be renamed.  However, I'm really impressed with how much better this discussion is going than the last one.\n\nThis time around, we seem to have agreement that the naming system is pretty haphazard.  We also seem to have agreement that it's possible this haphazard naming convention could be seen by some as a bad thing, even if it isn't seen as a bad thing by most.  And we also have agreement that, in the end, KDE will remain inconsistent in this regard.  That's progress!\n\nSo allow me to address some unanswered points brought up in this discussion so far:\n\nYes, MacOS and Windows have branding-related naming conventions too--the \"i\" convention for MacOS is used more frequently and more consistently than any convention for Windows.  Those conventions may annoy Windows and Mac users, and Windows and Mac users may or may not complain about them (although probably not as much, because neither are used anywhere near as frequently as KDE's naming conventions).  I certainly feel users are free to complain about it if it bothers them, but because the vendors are closed-source, I doubt they will be terribly responsive, even if the negative sentiment is overwhelming.  You will not find me complaining about software I do not use frequently, however.  So it's just KDE for me.\n\nSecondly, there's not much point in proposing new names if not enough people see a problem with the existing names to warrant the effort.  i.e. if people see changing names as, at best, no loss and no gain, then there's no way on Earth anyone would want to go through the trouble.  People must really strongly dislike the existing names to warrant the change.  So, if everyone felt like me on the matter, we would all be sitting around floating ideas back and forth and it would be very open and democratic.  If it's just me and the two or three other people who agreed with me in the last discussion, well, it's not going to happen so why even get to that point?\n\nAs far as \"which convention to pick?\" that's in the same boat.  My vote is for no extra effort in trying to put K's into names at all--which would leave words that happened to contain K's (German, English, dubiously transliterated Inuit, or whatever) unchanged, except maybe some corrected capitalization.  But if I'm the only one voting, there's not much of a point, is there?\n\nThis is a collaborative effort, and although I still feel it's incumbent on any of us to bring up ideas or problems as we find them, I also feel that attempting to make changes without any sort of consensus that the changes actually need to be made is foolish.  You will not find, as one poster suggested, a \"KDE: Consistent Edition\" on kde-apps.org, at least not contributed by me.  If nobody wants it, why go through the effort?  That's why discussions like this are valuable--to gauge user interest.  Currently my gauge on the matter says \"you could probably count interested parties on one hand\"\n\nDiversity of opinion is a good thing.  Freedom to discuss these opinions openly is also good.  Neither of these qualifies as, or necessarily leads to forcing a minority opinion on others--and I've never attempted to do that, despite the occasional accusation.\n\nAnd, to counter one quote which could, in context, be considered ad hominem with another:\n\n\"If everyone is thinking the same thing, someone is not thinking\" --Gen. Douglas MacArthur"
    author: "ac"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "\"A foolish consistency is the hobgoblin of little minds\" --Emerson\n\nKDE is a large, complex, diverse project.  I think the \"inconsistent\" app names reflect this nicely.  \n\nIf KDE was a corporation, and we had a Director of Marketing, maybe this discussion would make some small sliver of sense.  But since we aren't, and we don't, it's all pretty absurd and trivial.  I mean, really, who cares?  If it really bothers you, you can go into the desktop files and modify the app names to whatever you like: problem solved.  You could even promote the set of modified desktop files as \"KDE: Consistent, Conservative Edition\" on kde-apps.org.\n\nregards,\nJason, who won't be changing the name of KStars anytime soon...\n"
    author: "LMCBoy"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "Can you really do this? Surely most of the names are hard-coded into the apps. I'd be extremely surprised if this were not the case.\n\nAnyway, two or three people discussing this doesn't give any idea about what the general population things. We need a vote/survey.\n\nAlso worth pointing out is that hardly any windows apps start with 'Win' these days (only WinAmp comes to mind, and that isn't even active anymore). More apple apps start with 'i', but even then there are many many counter-examples:\n\nPhotoshop,\nMS Office,\nPremier,\nSafari,\n...\n\nI searched for 'list of mac apps' on google and went to the first result - it lists:\n\nVoodoo Pad\nSkype\nFlickr Export\nChronosync\nTransmit\nNewsfire\nCocoalicious\nOmniweb\nCamino\nskEdit\n\nSimilarly none of the apps on the front page of http://www.winappslist.com/ start with Win.\n\nAnyway, I don't expect people to start renaming apps, and some make sense (eg KPDF, KDevelop etc), but be more creative/sane in your naming (well done plasma)!"
    author: "Tim"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-27
    body: "Think about this, though:  Out of that list of Mac apps, how many of them tell you much about what the app does?"
    author: "regeya"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-27
    body: "I agree, simple apps (ie boring ones) should tell you what they do-\n\nKDevelop,\niTunes,\nKPDF,\nCalculator,\nNotepad,\netc.\n\nBut that has nothing to do with the crazy letter naming. CF:\n\naKode\namoraK\nKopete\nKRDC\netc\n\nDo any of these tell you what they do? Nope. That's a separate issue and one that doesn't bother me that much."
    author: "Tim"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-27
    body: "KRDC stands for KDE Remote Desktop Connection, if someone knows what RDC is they probably will realize what KRDC is.  aKode is a library for aRts (so end users won't need to know what it is).  Kopete is based on the Chilean word Copete.\n\nWould you consider Kontact to be a good name.  Its much better than 'Outlook' (image viewer???), 'Thunderbird' (awesome game???), and 'Evolution' (um???) since it gives you an idea what it is.  Some of the K names are pretty descriptive.\n\nOk yeah... this is a pointless post..."
    author: "Corbin"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-28
    body: ":: Out of that list of Mac apps, how many of them tell you much about what the app does?\n\n Voodoo Pad\n\n It allows a Voodoo priest or priestess to type up memos to the underworld.\n\n Skype\n\n A app for planning flights of letter writing aircraft.\n\n Flickr Export\n\n Handles iCal extraction of information about seizures.  Very handy for the diagnosing doctor.\n\n Chronosync\n\n Makes sure you have an up to date list of all Squaresoft titles.\n\n Transmit\n\n This really just a renamed SuperKermit with a fancy Aqua interface.\n\n Newsfire\n\n Allows you to cause news reporters on CNN, the Beeb and FOX News to spontaneously burst into flame when you're watching them in KWinTV.\n\n Cocoalicious\n\n This is a family website, so I can't explain what this does, only point out that anybody who owns a copy of this should be deeply ashamed of themselves.\n\n Omniweb\n\n This gives access to back issues of Omni magazine.\n\n Camino\n\n Turns your mac into a half computer-half pickup truck.\n\n skEdit\n\n Allows you to make changes to the Slovak Republic.\n\n\nHonestly, I know what one does and can semi guess at two (Skype is a VoIP app, Voodoo Pad is likely some kind of text editor and Flikr Export probably sends images to Flikr), but I really don't know what the rest are.  skEdit might be another text editor."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Don't we just love KDE?"
    date: 2005-11-04
    body: "I like the naming convention- it's kute.\nother Xploits are the use/misuse of other letters and terms- particularly E,I,X \nin many platforms - and platform specific prefixes win/mac, which seems like it extends hamburger marketing - Mac-Shake, mac-Fries, big mac.  \n\nIf anyone doesn't like the name- grab the source code, rename some files and recompile it - Or just rename the sym-links.\n\n~S~"
    author: "strongheart"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "I think the diversity and unpredictability of names in KDE is sheer beauty, lets not stiffle it with some kind of convention. I for one certainly don't want a boring naming pattern like Windoze has."
    author: "rainier"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "The convention's already there, I'm afraid (roughly described as \"put a K in the name somewhere\").  There are three totally different ways to implement this convention, and a fourth option to disregard the convention entirely.\n\nSo if you're in favor of disregarding this convention, I can certainly agree with that.  All I'm suggesting is that if we HAVE to have a convention (which I don't necessarily think is a good idea BTW), we should do it consistently.\n\nIf the names of KDE apps were truly unpredictable, I'd be delighted.  Unfortunately, of all of the capitalized consonants in any application name (of which there is usually only one or two), I can almost always guess one correctly, without even knowing what the app does."
    author: "ac"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "I think we'd be better off if all the time and energy that people put into arguing about the names of apps went into improving them -- in particular, all that typing could be directed toward improved documentation."
    author: "Jay"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "I would certainly argue that renaming them IS improving them, but certainly it's a lower priority than documentation improvements.  And both are lower priority than functionality improvements.\n\nHowever, you cannot defer documentation and naming bugfixes indefinitely until all functionality bugs are fixed--because those bugs are endless.  Some bugs are also easier to fix early in an applications life than later, such as the application's name.\n\nAnd while I really appreciate the sentiment (proper documentation is hard unrewarding work), it's more than just typing.  If you think the applications don't need to be renamed, just say so.  There's no need to invent harm that this discussion is doing to KDE."
    author: "ac"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-26
    body: "Yes I know you would argue.  And I didn't invent a harm, I merely pointed out that there could be more good.\n\nWe now return you to your regular stream of sophistry.\n"
    author: "Jay"
  - subject: "Re: Don't we just love KDE?"
    date: 2005-10-27
    body: "When called on a straw man, ad hominem.  That was hardly a surprise."
    author: "ac"
  - subject: "I want another lawyer"
    date: 2005-10-25
    body: "If this is the case made for the adoption of Konqueror, then I want another lawyer.  I don't think he would win in \"Browser Court\".  I was expecting to see an explanation of Konq's excellent architecture and it's use of KIO slaves - but that was way down the list, even after the \"I don't have any choice but the command line\" [paraphrased] shining compliment.\n\nThe article focuses on Konq's superior security, of which I was certainly curious - I had never read an article about Konq's security before - and it turned out to be that Konq is not IE - I knew that already.\n\nHe also says that he would only use Konq as a file manager and not a browser, because Gecko is better than KHTML - with friends like these who needs enemies?\n\nKonqueror is an excellent browser (univeral viewing framework, really) for many reasons - few of which are stated here - none of which are stated well.  This article does the KDE and Konqueror cause no good at all."
    author: "Tim"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "This is a boring reply but I've read the article and had the precise same feeling,"
    author: "ac"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "Well, let's forgive the dot for throwing some chintz out every once in a while."
    author: "Segedunum"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "Hello,\n\nhe not only says, it's more secure for not being IE, but he also pointed out that Konqueror is \"Open Source\" and therefore patched faster.\n\nBeing the only alternative to command line, assuming that he tried others, is a good compliment, because it says, it is usable as a file manager. And it really is in my view too.\n\nAnd the ability to also use Gecko instead of KHTML is NOT a strength of Konqueror architecture in your view? In my view it is. And there being 2 choices for HTML rendering, Konqueror has a tradition of allowing the user to select which one. \n\nHis comparison of KHTML and Gecko appears to be the common sense BTW.\n\nI personally will remain with KHTML anyway, it has the cleaner font handling (I always feel like Konqueror rendition looks more pretty) and faster loading.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "Just my thinking, I didn't really like Gecko under Linux, because the fonts look so big (don't know how to change them for the better, everything I did made it look more terrible). KHTML on the other hand, rendered the fonts very well, but alas it has a lot of other problems that I couldn't solve yet in HTML. But it is getting better, maybe one day when it also supports RichText Editing, it will become my standard browser, under linux....\n\nOn the other hand why not make a browser function like Firefox but based on KDE and KHTML. (maybe be able to switch from one rendering to an other). I like Firefox a little better, because of the keyboard-layout and middle-mouse-clicks to open links in tabs. (I'm also using it under windows, so I don't want to learn an other keyboard-shortcut yet...)"
    author: "Boemer"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "<I>On the other hand why not make a browser function like Firefox but based on KDE and KHTML. (maybe be able to switch from one rendering to an other).</I><P>Konqueror is getting gecko integration, at which point it will be able to switch from one to another.<P><I>I like Firefox a little better, because of the keyboard-layout</I><P>You can edit your shortcuts, you know<P><I> and middle-mouse-clicks to open links in tabs. (I'm also using it under windows, so I don't want to learn an other keyboard-shortcut yet...)</I><P>That's a preference"
    author: "mikeyd"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "> and middle-mouse-clicks to open links in tabs. (I'm also using it under \n> windows, so I don't want to learn an other keyboard-shortcut yet...)\n\nmiddle mouse clicks open links in tabs in konqueror too\n\neither middle click on a link or even, if a url occurs in a page but it's not a link, select it and middle click"
    author: "ac"
  - subject: "Re: I want another lawyer"
    date: 2005-10-25
    body: "From the article:\n---\nWhere do you feel Konqueror is strongest -- as a browser or file manager -- and why? \n\nBrickner: Personally, I like it best as a file manager, simply because I wouldn't have a good alternative (other than the command line) to turn to if I didn't have Konqueror. For a Web browser, I could always use Firefox.\n---\nHe's not saying \"he would only use Konq as a file manager and not a browser,\" he's saying Konq is a better file manager than browser, and if Konq wasn't a  browser, he could use FF.  He does indeed say Gecko is better than KHTML further down the article."
    author: "Ed"
  - subject: "Re: I want another lawyer"
    date: 2005-10-26
    body: "Yup.  Tim needs a better english parser more than he needs a better browser lawyer.  And what was that bit above about being \"very surprised\" if apps didn't have their name hardwired in?  Especially in this era of i18n, no competent programmer hardwires text.  Even my throwaway command line apps get their names from the invocation. "
    author: "Jay"
  - subject: "Re: I want another lawyer"
    date: 2005-10-27
    body: "That's a different Tim"
    author: "Tim"
  - subject: "Re: I want another lawyer"
    date: 2005-10-27
    body: "Personally, I've used Firefox, and I've used Konqueror.  And in all seriousness, I wouldn't specifically pick one over the other; they both work really well for surfing the web.  As long as you aren't using IE, it's all good in my book."
    author: "Dark Phoenix"
  - subject: "Re: I want another lawyer"
    date: 2005-10-30
    body: "lol. I'm sorry the interview wasn't to your liking. I wasn't too fond of the interview questions myself, as they focused almost exclusively on security which is not really what I wanted to talk about. But all I could do was answer the questions put to me. The questions defined how the interview turned out, not my answers. If you want to see my opinion on Konqueror as a file manager (from a basic user perspective) read the sample chapter linked to in the article. Another chapter in the book is devoted exclusively to Konqi as a file manager.\n\nYour conclusions about my use of Konqueror are incorrect. The intent of my statements was that I'm more dependent upon Konqueror as a file manager than as a web browser. My next best alternative file manager is the command line, which I'm comfortable with, but which I don't always want to use. However, the next best alternative web browser is Firefox and that is pretty darn good so I don't mind using that.\n\nJust for the record--I really enjoy KDE. If KDE didn't exist I might not be a desktop Linux user, or if I was one I would be less happy. I prefer KDE to any other environment out there, and this includes the OS X interface. GNOME, xfce, and other DEs and WMs are nice and all, and I think it is great that there is so much diversity and choice for Linux and that so many users like the alternatives, but for me KDE is the best choice.\n\nHere is something many of you may find interesting: at O'Reilly all of our production users (these are the people who copy-edit, layout, and quality check our books, as well as prepare the final files that go to the printer) use KDE as their main desktop or as a remote desktop from which they access our layout program FrameMaker. KMail, Konqueror, and Konsole are the main programs used."
    author: "David Brickner"
  - subject: "Page Rendering"
    date: 2005-10-25
    body: "I absolutely love Konqui as a file manager and web browser and life browser with all the protocols (KIOSlaves +/- a small typo) it supports. Stuff like klik:// , fish://, ... it is so powerfull\n\nMy non-techie significant other, OTOH, does not use any of these (except file browsing). After a while, she settled for Konqui for file browsing and Firefox for the web. Why ? More pages render/work/whatever in Firefox than Konqui. The gap is closing, but it is still true. \n\nI still love it, but I really think that embedding (optionally) gecko will be a great benefit. \n\nThe other thing she like better in Firefox is a simpler interface. Please don't flame me for this. Many people do. Actually I do. But I'd rather use konqui for many other reasons\n\nCheers"
    author: "MandrakeUser"
  - subject: "Re: Page Rendering"
    date: 2005-10-25
    body: "There is nothing to stop you from making the interface to Konqueror just as simple as that of firefox -- and then Save View Profile Webbrowsing.\n\nYes, it is a \"complex\" task to set up the simpler interface, but that is a one-time task suitable for the local admin/guru.  IE, you ;-)"
    author: "kundor"
  - subject: "Re: Page Rendering"
    date: 2005-10-25
    body: "yep :-)\n\nWell, and some distro's like Ubuntu are doing it for their users, I think it is a good idea. \n\nA little tougher (without touching the code) would be to simplify the configuration menu. It is a bit overwhelming and it could be improved usability-wise. But then again, even Firefox is configured by this humble servant for everyone else :-)\n\nIn the end, most people I know have someone around \"who knows\" and configure stuff  for them (especially in windows, mac users really choose it and know their way around). All we need is make our (free) software easy enough so that anyone with a bit of interest can configure stuff for them and the people they help !\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Page Rendering"
    date: 2005-10-25
    body: "Actually, its already done for you! Just switch the profile to simplebrowser. If you want this to be the default, save over webbrowser.\n\nTo start konqueror in the simple browser mode, run \n\"konqueror --profile simplebrowser\" (I sure hope I'm right, can't test it right now think).\n\n-sam"
    author: "Sam Weber"
  - subject: "Re: Page Rendering"
    date: 2005-10-26
    body: "Yep, that worked!\n\nThat's pretty simple all right!  Even the menus -- the Go and Window menus are gone which is an improvement even over Firefox (which still has the never-used Go menu) and the Settings menu is much reduced.\n\nNow, making that a default for webbrowsing might be a good idea.  \"Advanced\" users who want the extra functionality will be more comfortable switching to \"Complex Browser\" as default than untechnical users will be switching to Simple Browser."
    author: "kundor"
  - subject: "Re: Page Rendering"
    date: 2005-10-26
    body: "Good points, thank you both !"
    author: "MandrakeUser"
  - subject: "Google Maps don't work with Konqi"
    date: 2005-10-25
    body: "I wonder whether its konqi or the google maps which is responsible for \"unsupported browser\" dialog."
    author: "fast_rizwaan"
  - subject: "Re: Google Maps don't work with Konqi"
    date: 2005-10-25
    body: "It's fixed.\nCheck out \"This month in SVN\" from June 2005\nhttp://www.canllaith.org/svn-features/22-06-05.html"
    author: "jmfayard"
  - subject: "Re: Google Maps don't work with Konqi"
    date: 2005-10-26
    body: "Google of course ;)\n\nGoogle Map works if you spoof as Firefox or Safari. The only thing that doesn't work seems to be sightseeing urls that doesn't jump to the right position or the various special uses of google maps (like crime-maps) which just shows empty screens."
    author: "Allan Sandfeld"
  - subject: "Is it just me?"
    date: 2005-10-26
    body: "Doesn't konqi seem to handle ASP's better than FF? I find very few sites that konqueror can't handle well with just a little setting. "
    author: "ryan"
  - subject: "Unknown feature in konqueror?"
    date: 2005-10-26
    body: "The only thing I like more in Firefox is the \"Begin finding when you begin typing\"-feature. In Konqueror you need to put a / in front of the search.\n\nI know Konqueror has a lot of hidden gems (activated in a conf.file). Does anyone knows if this feature can be enabled in Konqueror? If not, does anyone knows if this is a planned feature?\n\nThanks!"
    author: "AC"
  - subject: "Re: Unknown feature in konqueror?"
    date: 2005-10-26
    body: "My least favorite Konq feature relative to FF is that ctrl-W doesn't close a single tab window -- that's inconsistent and poor UI.  And the Konq/KDE password management (kdewallet) sucks big time.  Other than that and the pages that it can't render and javascripts it can't handle, I'd be happy with it."
    author: "Jay"
  - subject: "Re: Unknown feature in konqueror?"
    date: 2005-10-26
    body: "If I remember correctly, this behavior is intentional. From what I remember, many people have voted in the bugzilla database to change this behavior, but the developers refuse to budge."
    author: "Anonymous Joe"
  - subject: "Re: Unknown feature in konqueror?"
    date: 2005-10-26
    body: "I'm sure it's intentional; that doesn't stop it from being bad.\n"
    author: "Jay"
  - subject: "Konqui rendering"
    date: 2005-10-26
    body: "Konqui rendering is good but slow. Why does it take so long."
    author: "gerd"
  - subject: "Re: Konqui rendering"
    date: 2005-10-26
    body: "Because it's design and the set of algorithms it uses aren't optimal.  (If there were a simple answer, it wouldn't still be slow.)"
    author: "Jay"
  - subject: "Re: Konqui rendering"
    date: 2005-10-27
    body: "Funny.  I've read other posts complaining that konqi's rendering is blazingly fast but not always good.  \n\n"
    author: "cm"
  - subject: "Re: Konqui rendering"
    date: 2005-10-27
    body: "Konqueror is the fastest free browser in GNU/Linux. Only opera is faster in some tasks. The unique konqueror slow work is javascript.\n\nhttp://www.howtocreate.co.uk/browserSpeed.html"
    author: "mcosta"
  - subject: "Re: Konqui rendering"
    date: 2005-10-27
    body: "He's right. I kept hearing people say Konqueror is fast, but always thought it was really really slow. So eventually I tried turning javascript off. It does indeed make a large difference to the speed and general responsiveness (eg hovering over links)."
    author: "Tim"
  - subject: "speed konqui 3.2 faster than konqui 3.5"
    date: 2005-10-27
    body: "http://www.howtocreate.co.uk/browserSpeed.html#linspeed\n\nKonqeror 3.2 (Gnome)\t13.90\t2.85\t0.80\t1.54\t107\t2.44\t41\nKonqeror 3.2 (KDE)\t3.02\t0.55\t0.80\t1.52\t111\t2.34\t60\nKonqeror 3.5 (Gnome)\t14.98\t5.70\t0.91\t2.87\t75\t2.00\t49\nKonqeror 3.5 (KDE)\t10.84\t1.23\t0.72\t2.97\t77\t2.11\t48"
    author: "gerd"
  - subject: "Re: speed konqui 3.2 faster than konqui 3.5"
    date: 2005-10-28
    body: "... or not. Stop trolling for a while and go check the figures for real.\nOnly application start up and table rendering are deemed as slower.\nCss rendering, script speed, multiple images and history tests are all faster.\n\nAs for application startup, this is so dependant on compile time options, fontconfig bugs, plugins configuration, prelinking, gcc with visibility or not... bah. \nI don't see what could have changed for worse since 3.2 in that respect (whereas in the sense of *faster* startup time, we have now the fvisibility enhancements).\n"
    author: "sp"
  - subject: "Re: speed konqui 3.2 faster than konqui 3.5"
    date: 2005-10-30
    body: "So when has KDE 3.5 been released after all? Yeah, right: currently it's still at beta2 stage. And the Author doesn't even mention which beta-version or snapshot he is using and how he got the binaries. Depending on that his results can only get better (Debug-mode? Compile-options? Optimizations?). So if you do a reality check and realize that many of the numbers surpass Konqueror 3.2 already you might come to the conclusion that Konqueror 3.5 will be a magnificent browser! "
    author: "Torsten Rahn"
  - subject: "k is also for kernel"
    date: 2005-10-26
    body: "khelper\nksoftirqd\nkacpid\nkblockd\nksawpd0\nkseriod\nkjournald\nkhubd"
    author: "anonymous user"
  - subject: "Konquerophile."
    date: 2005-10-27
    body: "Thank you for promoting Konqueror.  It is the most pleasing browser that I have used.  A different (and beautiful) UI may be configured each day.~~~"
    author: "Icyfeet"
  - subject: "I'm the only one"
    date: 2005-10-28
    body: "I guess I'm the only that doesn't like Konq for browsing or file management. I much prefer the Mozilla rendering engine."
    author: "Mike"
  - subject: "Konq freezes while rendering"
    date: 2005-10-28
    body: "I'll take the liberty of doing so many others as soon as a Konq story pops up - whining about my Konq pet peeve. Feel free to ignore me like I do myself with similar posts. ;) \n\nI switched to Firefox after having used Konq for a couple of years, because I became gradually more annoyed that Konq freezes while loading a webpage. Middle-clicking on a link to bring it up in a tab in the background is very convenient, but only if the page I'm viewing doesn't freeze waiting for the new page to be rendered.\n\nDoes anyone else have this problem? I'm using Gentoo.\n\n\n"
    author: "ac"
  - subject: "Re: Konq freezes while rendering"
    date: 2005-10-28
    body: "that's \"doing _like_ so many others\" of course"
    author: "ac"
  - subject: "Re: Konq freezes while rendering"
    date: 2005-10-28
    body: "Nope, works just fine here. Well it actually happens sometimes with links to single jpg's, guess it has to do with what displays it. "
    author: "Morty"
  - subject: "Re: Konq freezes while rendering"
    date: 2005-10-28
    body: "No, it works fine for me.\n\nI use Konqueror everyday, with 10-40 tabs, 99% of my browsing activities, and for me it is quite stable. I only use firefox for my online banking website - but in Windows of course I use firefox, at least until KHTML is ported to Win32. I like Konq because it is very fast, integrated with my KDE desktop (you can middle click link a pdf file to open it in new tab, very useful for me ;)\n\nBtw, maybe we need to create Konq's fans club or something like spreadfirefox? Anyone interested? :D"
    author: "fyanardi"
  - subject: "Not Secure enough for online Banking."
    date: 2005-10-28
    body: "If Konqueror is so secure, then why are banks like NAT WEST not prepared to allow it to access their Online Banking Systems. Firefox Yes Konqueror No."
    author: "Gerard"
  - subject: "Re: Not Secure enough for online Banking."
    date: 2005-10-28
    body: "How does that have anything to do with security?  Does NAT WEST not want their site to be able to exploit the web browser accessing the site?\n\nI believe the reason NAT WEST doesn't support Konqueror is because Konqueror's market share is FAR smaller than Firefox's.\n\nIf NAT WEST supports IE then it OBVIOUSLY has nothing to do with security."
    author: "Corbin"
---
<a href="http://searchopensource.techtarget.com/">SearchOpenSource.com</a> has a short <a href="http://searchopensource.techtarget.com/originalContent/0,289142,sid39_gci1135769,00.html">interview with David Brickner</a>, author of <a href="http://www.oreilly.com/catalog/tdlinux/">Test Driving Linux: From Windows to Linux in 60 Seconds</a>, about <a href="http://www.konqueror.org">Konqueror</a>.  Brickner, who writes extensively about Konqueror in his new book, says malware, spyware and viruses have virtually no chance of penetrating machines through the open source browser and file manager.  He talks about the security, multi-protocol support and comfort level that Konqueror provides for those coming over from Windows.  




<!--break-->
