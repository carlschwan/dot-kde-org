---
title: "KDETalk: A KDE Jabber Server"
date:    2005-10-07
authors:
  - "ogoffart"
slug:    kdetalk-kde-jabber-server
comments:
  - subject: "confusing article title"
    date: 2005-10-07
    body: "I thought it was a KDE implementation of a Jabber server.\n\nIt isn't, if it confuses you too!"
    author: "a. p. non"
  - subject: "Re: confusing article title"
    date: 2005-10-07
    body: "Yep, me too. But a KDE Jabber server wouldn't make much sense, would it?"
    author: "ac"
  - subject: "Re: confusing article title"
    date: 2005-10-08
    body: "I don't know about that.  One of the common complaints about Jabber is that only a few servers have any kind of user friendly UI on them."
    author: "Trejkaz"
  - subject: "Re: confusing article title"
    date: 2005-10-08
    body: "So you create a KDE interface to a server that runs at a console/daemon level. You don't need the whole server to run in KDE just to have a user friendly UI. That would be silly."
    author: "Yama"
  - subject: "Re: confusing article title"
    date: 2005-10-08
    body: "Would it be so silly to have a server which conveniently exposes a DCOP interface for administration?\n\nMind you, we don't really have _any_ servers with a good remote interface yet... except perhaps ejabberd's console.\n"
    author: "Trejkaz"
  - subject: "Re: confusing article title"
    date: 2005-10-10
    body: "Well, if KDE's libraries were useful (especially Kopete's, perhaps) in developing it without re-inventing the wheel, then it might make sense.  Also, if it was to be run on machines that already run KDE, then it would make sense to re-use those libraries.  As others mentioned, there is also the user interface issue.  Other OSes, like AtheOS and BeOS and Windows and OS X and AmigaOS have all used GUIs for background programs.  It doesn't have to be a bad thing."
    author: "Lee"
  - subject: "Re: confusing article title"
    date: 2005-10-07
    body: "Indeed, a better title would be \"A Jabber server for KDE\"."
    author: "Bram Schoenmakers"
  - subject: "Re: confusing article title"
    date: 2005-10-07
    body: "That would be even MORE confusing than 'KDETalk: A KDE Jabber Server'.\n\nI think 'KDETalk: A Public KDE Jabber Server' (added in 'Public') would make more sense."
    author: "Corbin"
  - subject: "[OT]"
    date: 2005-10-08
    body: "\"A KDE Jabber Server\"\n\nWell that's exactly the kind of ambiguity that happens all the time in english (or chinese) due to its relative lack of grammar compared to more structured language like german, french or esperanto (and others probably, they are just the ones I know).\n\n\"Time flies like an arrow\" (Noam Chomsky)\n (Oups, which word is the verb ? The 1st, the 2nd, the 3rd ?)\n\n\"English professor\"\n Is that a professor that teach english or a professor born in England ?\n\nUN troll between Israel and the muslims : a resolution says, Israel has to go out \u00ab from occupied territories \u00bb\n From ALL THE occupied territories ? From SOME occupied territories ?\n\n\"International Auxiliary Language Association\"\n What does that mean ? An association that want to promote an international and auxiliary language ? An international association that wants to promote an auxiliary language ?\n\nAnother crazy example resulting in deaths : http://www.benjamins.com/jbp/series/LPLP/27-3/art/0002a.pdf\n....\n\nWhen you begin to learn a language, a soft grammatic seems to be a good idea, but hey, this difficulty is not totally useless. In more structured language, the grammatic avoid those kind of ambiguity ; in english you have to rely on the context. If you don't know the context, you are screwed like our friend << a.p. >>\n\n\n\nSorry for going out of topic."
    author: "jmfayard"
  - subject: "Re: [OT]"
    date: 2005-10-09
    body: "Stop trolling.  All of these examples you have provded are worthless.  All of them can be reworded in such a way that makes them much more precise and clear.  Any ambiguity is the fault of the speaker or writer, not the language."
    author: "MrGrim"
  - subject: "Re: [OT]"
    date: 2005-10-09
    body: "You are most definitely NOT a .NET developer.\n\nI feel very much comforted by this revelation. =)"
    author: "James"
  - subject: "Re: [OT]"
    date: 2005-10-09
    body: "I don't think at all he was trolling (and I don't understand why you feel attacked.)\n\nHave you read the PDF about the communication problems between pilots and towers?  It shows that these ambiguities are a problem in the real world.  \n\nThe point is not that something cannot be reworded in a non-ambiguous way. It's  that common and straightforward ways of expressing it are ambiguous.  \n\n"
    author: "cm"
  - subject: "Re: [OT]"
    date: 2005-10-22
    body: "It shows that no pilot education should miss exams for proper English...\n\nAny pilot flying in International airspace should be able to speak English properly, if not there's something wrong with the company.\n\n(Now, to make this funny, I excuse any errors or ambiguities, I'm a Dane ;) )\n\nDennis\n"
    author: "Dennis Kr\u00f8ger"
  - subject: "Re: [OT]"
    date: 2005-10-22
    body: "> Any pilot flying in International airspace should be able to speak English properly,\n\nOf course.  But that doesn't change the fact that the ambiguities are there in the English sentences actually spoken (by native and non-native speakers). \n\nOTOH, no one's suggesting a change of the language used in international air travel.\n\n"
    author: "cm"
  - subject: "encouraging KDE user to use it"
    date: 2005-10-07
    body: "I think the best way to get people to use it would be to pop up  in the left corner of the screen a bubble that would say \"Would you like to create a KDE talk account to keep in touch with your friend?\". Then if the user click on it, a wizard will help the user create an account and propose him/her a username that would match his current linux username.\n\nOr better, when the user starts KDE for the first time, that wizard could be added automatically next to the select your \"KDE style wizard\" and stuff. If the user skip it, he will get the aforementioned bubble when he start kopete for the first time :)\n\nWhat do you guys think?"
    author: "Patcito"
  - subject: "Re: encouraging KDE user to use it"
    date: 2005-10-07
    body: "> I think the best way to get people to use it would be to pop up in the left\n> corner of the screen a bubble that would say \"Would you like to create a KDE\n> talk account to keep in touch with your friend?\n\nYou assume that the friend uses Jabber =)\n\n> Or better, when the user starts KDE for the first time, that wizard could be\n> added automatically next to the select your \"KDE style wizard\" and stuff. \n\nI don't think it is a good idea to clutter the first KDE start with more wizards. The first time I start a desktop I surely don't want to be interrupted and create an IM account for a protocol I might not know.\nI don't want every single KDE feature advertised to me in such a situation. If at all, the \"advertisement\" should happen in context, that is when I am about to create an IM account.\n\n> If the user skip it, he will get the aforementioned bubble when he start\n> kopete for the first time :)\n\nI would it make the default for Jabber accounts, but not more. And if the user has an IM account, he will surely want to use that and not create a new one on KDE Talk. An account created by accident but not used because the user does not know anybody using Jabber is of very limited use ;-)"
    author: "lippel"
  - subject: "Re: encouraging KDE user to use it"
    date: 2005-10-07
    body: "\"An account created by accident but not used because the user does not know anybody using Jabber is of very limited use ;-)\"\n\nThis is how Microsoft killed ICQ, by bugging user to create an MSN account until they do so even though they already had an ICQ account in some case and it worked! Because eventually everybody ended up with an MSN account even though they never used it. So in the end people ended up using it because that's what everybody had.\n\nIf we can get every KDE user to get a jabber account even by accident, that would be great, because if MSN turns down access to their network to kopete as they do every year, they will remember of their jabber account and use it. In fact they don't even need to know it's jabber, it's just KDE Talk, the KDE messenger ;)"
    author: "Patcito"
  - subject: "Re: encouraging KDE user to use it"
    date: 2005-10-07
    body: "i like you idea, and i like wizards @ startup.\n\nbetter than do it by hand."
    author: "chris"
  - subject: "Re: encouraging KDE user to use it"
    date: 2005-10-07
    body: "By the way this was created by Gof, the great MSN maintainer which is getting tired of MSN. We should all push for the free Jabber Protocol. Great Job Gof!"
    author: "Patcito"
  - subject: "dito"
    date: 2005-10-07
    body: "That's what I thought when i say the feed update..... Cool a KDE jabber server... Well no!\n"
    author: "karwin"
  - subject: "so... chat or VOIP?"
    date: 2005-10-07
    body: "KDE has a jabber-compatable application, Kopete.  So does this follow GoogleTalk, where its a VOIP program (also based on Jabber and possibly compatable!?)?  No one seems to mention that in the comments, but why have another KDE chat program?"
    author: "lefty.crupps"
  - subject: "Re: so... chat or VOIP?"
    date: 2005-10-07
    body: "It's a *server*, not a client."
    author: "mikeyd"
  - subject: "HELP"
    date: 2006-10-29
    body: "I NEED HELP!\n\ni am runing SuSe on a p 3 and Kde will not start\n\ninsted of starting Kde it starts in sum other mode \n\ni thindk its called Kmarck or somthing like that \n\nican only run aone consle "
    author: "josh"
  - subject: "Why would I want to use this one?"
    date: 2005-10-07
    body: "Hi,\n\nTwo questions:\nHow does this fit into the general concept of the KDE project? I thought KDE's goal was to provide software, not services? (Well, apart from the services related to distributing, supporting and advertising said software.)\n\nWhy would I want to use this Jabber server out of the many available? Does it have any outstanding features I can't find elsewhere? The one thing that has plagued most Jabber servers I have been using up to now was a lack of reliability (especially jabber.org...). How do I know that this one will be better? (I could use it for some time to find out, but by then it's too late for an informed decision anyway.)"
    author: "Guido Winkelmann"
  - subject: "Re: Why would I want to use this one?"
    date: 2005-10-07
    body: "\" I thought KDE's goal was to provide software, not services?\"\n\nIf that frightens you then you'd better get yourself ready cause I heard KDE is about to release a whole bunch of other services, namely: KDEMail, KDEEarth and KDEOffice all available for free on the net and of course coded in AJAX. \nKDE board members said that KDE's main goal is to offer the best desktop experience and those services are totally in line with their goal :)"
    author: "Patcito"
  - subject: "Re: Why would I want to use this one?"
    date: 2005-10-09
    body: "Watch out, Google and Microsoft!"
    author: "Guido Winkelmann"
  - subject: "Re: Why would I want to use this one?"
    date: 2005-10-08
    body: "Go for a server with WAY less than a thousand users. \nIf you want more information on that download the video from the talk about jabber from the whatthehack conference. That guy even advocates running your own Jabber server.\n\nThat said, I have been using jabber.ccc.de and it has served me insanely well since I switched from jabber.org a year or so ago."
    author: "jhg"
  - subject: "Re: Why would I want to use this one?"
    date: 2005-10-08
    body: "> since I switched from jabber.org a year or so ago.\n\nWhat does switching over mean?   Does your jabber ID change when you do that?  Is that a new account? "
    author: "cm"
  - subject: "Re: Why would I want to use this one?"
    date: 2005-10-08
    body: "Yes, and you have to tell all your contacts to remove your old address and add you at the new one (or you can add them and have them authorize you). Switching Jabber servers is such a pain; I wish the protocol had some kind of redirection support."
    author: "Haakon Nilsen"
  - subject: "Re: Why would I want to use this one?"
    date: 2005-10-09
    body: "AOL (me too).\n\nI really wish Jabber servers had some of the features commonly found in email servers, like redirections (even crossing server boundaries, with or without revealing the redirected-to address to your contacts) or aliases with regex-support.\n\nConsider that it is no accident that Jabber-ids look and work like email addresses. It was (is) supposed to be a step in the direction of true unified messaging."
    author: "Guido Winkelmann"
  - subject: "Totally OT"
    date: 2005-10-07
    body: "But can I believe that still nobody else has voted for this wish...? :\nhttps://bugs.kde.org/show_bug.cgi?id=112555\n\nTry Firefox 1.5 and ur gonna like it..."
    author: "Yves"
  - subject: "Re: Totally OT"
    date: 2005-10-07
    body: "Wow. You're right -- that was totally offtopic."
    author: "Greg"
  - subject: "Re: Totally OT"
    date: 2005-10-07
    body: "Please dot admin, delete the parent post. This is the kind of post that messes up the bug voting system and makes it irevelant. This is also unfair for other bugs more important than this one :("
    author: "Patcito"
  - subject: "Re: Totally OT"
    date: 2005-10-08
    body: "I don't think it makes any mess. It's just about making aware, and I guess\nnobody would have a problem with konqueror having this feature, or would you?\n\nIn contrary, everybody would benefit of it.\n\nThe point of posting it here (in this calm thread) is to make people aware of\nthat (in the browser world) widely adopted feature of back/forward cache."
    author: "Yves"
  - subject: "Re: Totally OT"
    date: 2005-10-08
    body: "I guess you must be new in here. Nobody likes this kind of post. Just wait for the flood :)"
    author: "Patcito"
  - subject: "Re: Totally OT"
    date: 2005-10-08
    body: "> I guess nobody would have a problem with konqueror having this feature\n\nI could probably post links to thousands of bugs, and nobody would have a problem with getting them fixed.\n\n> to make people aware of that (in the browser world) widely adopted feature of back/forward cache.\n\nWhat makes you think that Konqueror doesn't use a cache?\n"
    author: "anon"
  - subject: "Re: Totally OT"
    date: 2005-10-09
    body: "Konqueror caches the document (html, css, images, etc), sure, but the feature request is about caching the rendered structure of the document as well to speed up the process of redisplaying the page.\n\nHere is more information about how it is done in Firefox:\nhttp://weblogs.mozillazine.org/chase/archives/008085.html"
    author: "Yves"
  - subject: "OT: konqueror vs opera (cache)"
    date: 2005-10-08
    body: "Opera 8.x does efficient caching, whereas konqueror doesn't do \"Session\" or \"Time-out\" based caching.\n\nIf we choose \"Use cache if possible\" then no matter how many times you exit/run konqueror the same site will get open with the \"Stale data\". This is not elegant. konqueror should see the good feature of opera of session based caching. as soon as the session expires the cache is cleared.\n\nand when opera is restarted then the page is updated (cache is flushed). konqueror doesn't do \"flushing\" of cache on exit of session (per page). This is what makes Konqueror's cache system almost useless.\n\nI would suggest:\n\n1. Session based caching: (as the page is closed the cache is deleted)\n2. time-out (30 seconds -2 minutes): after the time-out the cache is flushed-out.\n\nor we can have both 1+2 for better caching in konqueror.\n\n\n\n"
    author: "fast_rizwaan"
  - subject: "Re: OT: konqueror vs opera (cache)"
    date: 2005-10-08
    body: "you could this with a bit of dcop scripting to monitor the cache folder and delete appropriately. You barely need to know how to program"
    author: "Patcito"
  - subject: "Other features"
    date: 2005-10-08
    body: "Nice, next step could be to integrate the Jabber server better in the KDE project. Some ideas:\n* support for notifying via Jabber here\n* support for notifying via Jabber in the bug tracker\n* MUCkl (a web front-end for Multi-User Chat (MUC) rooms, accessible without the need to create a permanent Jabber ID)\n* news notifications via Jabber (new KDE release etc)\n* Publish-Subscribe integration in KDE\n* Jabber kio\n* a JabberFriends service similar to http://web.amessage.info/friends/\n* support for Blocking Communication (Privacy Lists) in Kopete\n* better overall Jabber support in Kopete\n* SVN notifications via Jabber\n* replacing the multi-user chat support in KDE games with Jabber MUC\n* ..."
    author: "Sander"
  - subject: "Re: Other features"
    date: 2005-10-08
    body: "\"* better overall Jabber support in Kopete\"\n\nFrom what I can tell Jabber support in Kopete is just as bad as support for AIM (meaning you can send messages and have buddies)."
    author: "Corbin"
  - subject: "Who's online"
    date: 2005-10-09
    body: "The server should have a page where those people who want to display their id can put it up. Otherwise whats the use of a Jabber server for KDE. This is a great service for bringing the community together, something like IRC. But when you don't know who is online then it gets boring.\n"
    author: "Nikhil"
  - subject: "Are they any chat rooms at kdetalk.net?"
    date: 2005-10-11
    body: "no chat room?"
    author: "fast_rizwaan"
---
For KDE users and contributors who aren't <a href="http://www.jabber.org/">Jabber</a> addicts yet, a new public Jabber server is available at <a href="http://kdetalk.net/">kdetalk.net</a>.
For those which don't know Jabber yet, it's an open Instant Messaging protocol. It has some advantage such as being decentralized, more secure, extensible, and last but not least in our free software world, open and developed by an open community.










<!--break-->
<p>The KDETalk server is public, anyone may register. If you are using KDE, you can register with <a href="http://kopete.kde.org/">Kopete</a> following <a href="http://kdetalk.net/howtoregister.php">these instructions</a>.</p>

<p>KDE developers may even, instead of registering username@kdetalk.net, ask sysadmin kde org for a @kde.org JID which matches with their KDE email or SVN account.</p>




