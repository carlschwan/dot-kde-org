---
title: "KDE CVS-Digest for March 18, 2005"
date:    2005-03-19
authors:
  - "dkite"
slug:    kde-cvs-digest-march-18-2005
comments:
  - subject: "I'm a sucker for optimization..."
    date: 2005-03-19
    body: ">> Replace old-dead xrgbrender stuff with stuff derived from agg's x11\n>> platform support -> makes zooming & panning _A LOT_ faster! I'd have never\n>> ever expected this xrgbrender stuff caused most off the slow downs. really\n>> impressive\n\nAnyone know more about this?  Zooming and panning in what?"
    author: "Leo S"
  - subject: "Re: I'm a sucker for optimization..."
    date: 2005-03-19
    body: "ksvg2"
    author: "Cartman"
  - subject: "KDE settings issues."
    date: 2005-03-19
    body: "Any plans to have this one solved ? Preferabely for KDE 3.4.1 and later ?\n\nhttp://lists.kde.org/?t=110982996300001&r=1&w=2"
    author: "Ali Akcaagac"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "well, KDE 3.4 fixes some of these for me. and I think the problems you have with the default not being default is your distribution, that chooses a different default (more usable, for example). when you click the default button, you revert to the KDE default, not the distribution default (again, I guess this is the case)."
    author: "superstoned"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "Yes, clicking 'default' indeed reverts to the defaults defined in the apps' binaries. Most distributions tweak KDE settings on the configuration level, not on the apps' default level (and that's good this way since the configuration files rely on a stable set of defaults since default values aren't saved, a behavior which is important for the Kiosk framework etc.)."
    author: "ac"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "No your guessing is wrong since this is no Distribution issue. I regulary compile KDE from source to check how much it has matured since my last compile. I for this reason also create a new empty homedir to test the stuff. So you can be sure that:\n\na) the homedir is empty\nb) that KDE is newly compiled\n\nYou and everyone else can try doing this on their own.\n\na) compile KDE from source and install it\nb) create a new empty homedir (for testing)\nc) start KControlCenter and then go through all the stuff and press 'default'.\n\nSo what happens now is that the values which the binaries start with are not the defaults as initially initialized through first startup. This is a bit complex to explain and my main intention is to reach the developers here because they know what I am talking about. A few developers already noticed this behavior as well."
    author: "Ali Akcaagac"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "KConfigXT takes care of getting this right automatically. The \"only\" thing that needs to be done is convert all control modules to use KConfigXT."
    author: "Waldo Bastian"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "I thought that most stuff within KDE has been converted to KConfigXT already. If not, how are the plans to achieve this goal in a reasonable time ?"
    author: "Ali Akcaagac"
  - subject: "Re: KDE settings issues."
    date: 2005-03-20
    body: "by people doing the work. the more people that pitch in, the faster it will occur. (well, at least as long as the number of people doing this is less than the number of discreet areas that need to be fixed)\n\nkicker just got KConfigXT in 3.4, but there wasn't time to switch the configuration dialog over to it. the control panel will hopefully use KConfigXT in the next release (though there are some wrinkles to work out)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE settings issues."
    date: 2005-03-20
    body: "What is the difficulty level of such a conversion?   \n\nWould it qualify as a \"Junior Job\" that's suitable for getting started with KDE development?  \n\nIs there someone who has an overview over the areas and the degree of completion of the conversion? \n\n"
    author: "anon"
  - subject: "Re: KDE settings issues."
    date: 2005-03-21
    body: "> What is the difficulty level of such a conversion? \n\nfor most control panels, it's very easy.\n\nfrom some, it's a bit trickier. kicker's is one of those, unfortunately. not horribly difficult, but not completely straightforward. these are the exception, however.\n\n> Would it qualify as a \"Junior Job\" that's suitable\n> for getting started with KDE development?\n\nmost certainly. kicker's two main KConfigXT files were done up by a first time contributor who is very new to C++. she worked off of some basic instructions on how to grep through the source for settings and what they meant and then how to make analogous KConfigXT entries in XML. i ended up doing the integration into the source code itself, but it was a great help to me that Jes went through and caught all the settings and did the XML in the first place. i think it was also a good learning experience for her.\n\nthe taskbar KConfigXT was done by a new developer as well. he's a good C++ devloper, but new to KDE. besides fixing several kicker bugs and implementing some long standing feature requests, Stefan also converted the taskbar to use KConfigXT. \n\nso whether you are a C++ newbie or someone who knows the language but wants to get familiar with KDE, it's a great low-impact way to get started. and it really helps KDE in a big way, so it also has great returns.\n\n> Is there someone who has an overview over the areas and the degree of\n> completion of the conversion?\n\ni believe there is a script in kdenonbeta somewhere that will create such a list. i believe Benjamin Meyer wrote it. i don't remember the name off hand.\n\nif you wish to get started on this, an email to kde-devel@kde.org or kde-quality@kde.org saying you'd like to start working on this stuff is a great way to say, \"Hey, I'd like to help. Help me help KDE!\" =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE settings issues."
    date: 2005-03-21
    body: "> i believe there is a script in kdenonbeta somewhere that will create such a list. \n> i believe Benjamin Meyer wrote it. \n\nDo you mean the scripts under kdenonbeta/kdetestscripts/ that are used to generate http://www.icefox.net/kde/tests/report.html ? \n\nThat page says that a check for apps that don't use KConfigXT has not been implemented yet (implicitely, it's listed as TODO). \n\n"
    author: "anon"
  - subject: "Re: KDE settings issues."
    date: 2005-03-21
    body: "hum.. perhaps it was a script by Frans E. then... i distinctly remember seeing a list posted somewhere of control panels that didn't use KConfigXT.. \n\ntoo much information, not enough klink. =P"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "Any plans on learning to use the bugtracking system, not core-devel or the Dot?  Preferably before 3.4.1?  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "This has been a problem with KDE since almost day 1.  Face it, it's never going to be fixed."
    author: "ac"
  - subject: "Re: KDE settings issues."
    date: 2005-03-19
    body: "Already in the bugtracker since a couple of months."
    author: "Ali Akcaagac"
  - subject: "Re: KDE settings issues."
    date: 2005-03-20
    body: "Hmm. I've searched bugs.kde.org and I can't for the life of me find a report about you not being able to use the bug tracking system properly. Assuming this report does exist, why haven't you closed it yet? Or should I vote on it so it gets your attention?"
    author: "Anonymous"
  - subject: "Re: KDE settings issues."
    date: 2005-03-21
    body: "Look, we have a comic here."
    author: "ac"
  - subject: "DataKiosk"
    date: 2005-03-19
    body: "DataKiosk looks great. Very intuitive way to work with databases!\n"
    author: "Anonymous"
  - subject: "Recording with the multimedia framework?"
    date: 2005-03-19
    body: "I don't see recording of audio or video mentioned in the requirements document for the media framework. Certainly there won't be too many applications using it, but it would be pretty essential for voice/video chats in kopete or a SIP-Client or simpler stuff like audio notes and similar.\nHas this been left out intentionally or did simply nobody care about it so far?"
    author: "uddw"
  - subject: "Re: Recording with the multimedia framework?"
    date: 2005-03-20
    body: "Um, the document is not finished. What gave you that idea?\n\nI was surprised that a full multimedia abstraction framework is being planned. \n\nCan't wait for KDE 4.0!\n\nIan"
    author: "Ian Monroe"
  - subject: "Re: Recording with the multimedia framework?"
    date: 2005-03-20
    body: "I found the thread on the kde-mm list about it:\nhttp://lists.kde.org/?l=kde-multimedia&m=111118326207636&w=2"
    author: "Ian Monroe"
  - subject: "Konqueror"
    date: 2005-03-19
    body: "Please make Konqueror's toolbars fully adjustable. On some sites, the size and location of my toolbars gets out of hand. Draging them back into position will stop working...that's to say, they will not merge/append to another, using my real estate. This has been filed at bugs.kde.org already.\n\nI have also attached a feature of Konqueror that I'd like to see disabled. Thanx."
    author: "charles"
  - subject: "Re: Konqueror"
    date: 2005-03-19
    body: "the toolbars ARE fully adjustable. but how can a site change the size and location of your toolbars??? anyway, with a richt-mouse-click you can adjust size and the icons in there to your liking.\n\nthe feature you want to see disabled - I'm sorry, but what do you expect? if the tab's don't fit, then what? you want to make them still smaller, until you can't even click them? no, I think this is an excelent solution: make them smaller until a certain minimum size (which, by the way, is configurable, afaik, with a tool called kconfigeditor or something like that) and then create these buttons to move through the tab's."
    author: "superstoned"
  - subject: "Re: Konqueror"
    date: 2005-03-19
    body: "In Kcontrol, one cannot keep on reducing the width of the window...its movement comes to a \"halt\" once some size is reached. It appears that all window items are made to remain reasonably visible in such a case. Which is OK. Just try reducing the size of the window I posted from the right to see what I mean.\n\nOn the feature depicted in the graphic, yes keep all tabs visible at all times or make the feature easily configurable. To me, it's ugly."
    author: "charles"
  - subject: "Re: Konqueror"
    date: 2005-03-19
    body: "so you think the size should be at least so big you can see al tabs. well, altough I agree that's reasonable, and looks better than the current behaviour, the way it works now permits a user to make it smaller (but still usable) so its usefull on small (800x600 or 640x480) screens, I think."
    author: "superstoned"
  - subject: "Re: Konqueror"
    date: 2005-03-19
    body: "Nope. Toolbars in Konqueror are not fully adjustable. Sometimes merging might not work or it might work. This has been talked about in the past. I for example always customize Konqueror to appear as attached, but sometimes, the toolbars get detached from one anther creating a long empty space. Getting them back to where they were becomes a matter of \"chance\".\n\nAgain, I realized that once <merge> is removed from \"current actions\" in configuring toolbars, getting it back is almost impossible!"
    author: "nope"
  - subject: "Re: Konqueror"
    date: 2005-03-19
    body: "well, I agree on this one - the customizing doesn't work very well, sometimes. it can behave very strange. I hope someone can have a look at this code when porting it to QT4/KDE4..."
    author: "superstoned"
  - subject: "Re: Konqueror"
    date: 2005-03-19
    body: "I customized konqi this way, and it works very well (see attachment)."
    author: "superstoned"
  - subject: "Re: Konqueror"
    date: 2005-03-20
    body: "Perhaps it would be better to make it impossible to remove <merge>??\n\nThere are still two things that are not configurable:\n\nActionList viewmode_toolbar\nActionList fullscreen\n\nThese are not configurable as toolbars.\n\nAlso some users still report some problems placing two toolbars on the same line -- especially with the Location ToolBar.\n\nI also find that the Go button is always placed at the end or bottom.  Perhaps we need two of them: Go & Go[end].\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Konqueror"
    date: 2006-01-10
    body: "This must the the messiest piece of code in whole KDE. I have never seen anything as non-intuitve as the konqueror toolbar customization process. Not only is there no documentation to help the user, the process itself is not discoverable by regular users. I tested it with four developer type colleagues; none of them was able to reliably produce the setup (that I had finally achieved after quite some time) in a reasonable amount of time.\n\nAdditionally, the madness varies between versions. Whereas in 3.4.3 I was not able to put the adress field on the main toolbar (and ended up putting the forward, backward, reload and stop buttons on the adress bar instead), I could not get rid of the totally useless (for me, as I use my defined search shortcuts) search field in 3.5.\n\nReally, people. I know some of you (and even me, eventually) are able to figure this out, but no user can and/or will be willing to invest the needed amount of time to do so.\n\nHave a look at the ease with which you can customize the Firefox toolbar layout. THAT is intuitive, easy and fast.\n\nPlease, have mercy!\n"
    author: "The konquered"
  - subject: "Re: Konqueror"
    date: 2006-01-10
    body: "Did you look at the code or what makes you state that?\n\nKonqueror is very dynamical and actually makes very good use of code, the problem is rather that dynamical backends and frameworks that Konqueror is doesn't automatically translate into a good user experience. The dialog for toolbar customization, which is reused in all KDE apps, is one of the pieces not really capable of handling Konqueror's use of dynamic toolbars.\n\nBesides we're posting in a very old thread..."
    author: "ac"
  - subject: "securty plugin"
    date: 2005-03-19
    body: "\"KDevelop adds a security problem detector plugin.\"\n\nWhere to find in the report?\n\n"
    author: "aranea"
  - subject: "Re: securty plugin"
    date: 2005-03-19
    body: "Crtl-F, type \"security\" and press enter four times. This will bring up the following: \n\nSecurity - Development Tools\nAlexander Dymo (dymo) committed a change to kdevelop/parts/security in HEAD \n March 16, 2005 at 10:26:36 PM\nI'm now in Warsaw, Poland and I'm participating in Open Source Security\nconference which makes me think about automated search for security\nproblems a bit more than I initially intended.\nThe result is the plugin I'm committing. This is a plugin which\nsearches for security problems (expressed in regexp patterns) and\nreports them into a \"Security problems\" view.\nPS: I know the problems could go to \"Problem Reporter\" but currently it\nisn't possible. We need to change KDevLanguageSupport interface to\nreturn problem reporter probably.\n\n\n"
    author: "Anonymous"
  - subject: "Re: securty plugin"
    date: 2005-03-19
    body: "http://cvs-digest.org/index.php?newissue=mar182005&module=kdevelop bottom"
    author: "Anonymous"
---
In this week's <a href="http://cvs-digest.org/?newissue=mar182005">KDE CVS-Digest:</a>

<a href="http://www.kdevelop.org/">KDevelop</a> adds a security problem detector plugin.
<a href="http://extragear.kde.org/apps/digikam/">digiKam</a> adds a white balance plugin.
<a href="http://www.konqueror.org/features/browser.php">KHTML</a> implements JavaScript window.atob/btoa.
<a href="http://www.kde.org/apps/cervisia/">Cervisia</a> implements folding/unfolding selected folder.
Request for comments on the new KDE multimedia framework.


<!--break-->
