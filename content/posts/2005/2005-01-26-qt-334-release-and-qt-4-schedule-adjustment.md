---
title: "Qt 3.3.4 Release and Qt 4 Schedule Adjustment"
date:    2005-01-26
authors:
  - "binner"
slug:    qt-334-release-and-qt-4-schedule-adjustment
comments:
  - subject: "solving most hated bug"
    date: 2005-01-26
    body: "And the best one:\n\nQIconView\n        Make it possible to move selected icons short distances\n        (i.e. to a position where there is a selected icon).\n\nThis finally solves #35203\n"
    author: "Martin Koller"
  - subject: "Re: solving most hated bug"
    date: 2005-01-26
    body: "To nitpick, this is only the third most hated bug."
    author: "Anonymous"
  - subject: "Re: solving most hated bug"
    date: 2005-01-26
    body: "Great!!!!\nBTW: It might not have been the most hated bug on bugs.kde.org\nbut it was definitely my very personal \"most hated bug\" as it made\nthe organization of ones desktop icons basically impossible.\n"
    author: "Martin"
  - subject: "Re: solving most hated bug"
    date: 2005-01-26
    body: "It'll help half the problem, but it probably won't stop the icons rearranging themselves every time I log back into KDE. :-/"
    author: "Trejkaz"
  - subject: "Re: solving most hated bug"
    date: 2005-01-26
    body: "You shouldn't experience this anymore with KDE 3.4 I think."
    author: "Anonymous"
  - subject: "Re: solving most hated bug"
    date: 2005-01-27
    body: "I hope that's the case, because it still happens on the beta.\n\nI've largely dismissed this as an \"out of my control\" issue which I've come to accept.\n"
    author: "Trejkaz"
  - subject: "Re: solving most hated bug"
    date: 2005-01-27
    body: "It should be ok in 3.4, see bug report #91575.\n\n"
    author: "Andreas Schallenberg"
  - subject: "Re: solving most hated bug"
    date: 2005-01-27
    body: "Just deactivate snap to grid, doesn't look as neat but at least the icons will stop rearranging."
    author: "Debian user"
  - subject: "Re: solving most hated bug"
    date: 2005-01-27
    body: "Unfortunately this doesn't always help. AFAIK its a bug\nduring login where collisions are checked and \"corrected\"\nbefore the icon texts are wrapped. For details see the report."
    author: "Andreas Schallenberg"
  - subject: "Re: solving most hated bug"
    date: 2005-01-26
    body: "Then why is the report still open? \nYour friendly bugzilla nagbot\n"
    author: "Sad Eagle"
  - subject: "Re: solving most hated bug"
    date: 2005-01-26
    body: "qt-copy has not been updated yet?"
    author: "Anonymous"
  - subject: "Mouse buttons"
    date: 2005-01-26
    body: "It is a QT update we are waiting for to get the option to configure extra mice buttons in KDE, right? Anyone seen anything of it? Imwheel must die."
    author: "allanon"
  - subject: "Re: Mouse buttons"
    date: 2005-01-26
    body: "wheels work fine 'out-of-the-box' w/o imwheel. suse 9.x and fedora 3 tested."
    author: "anononymus cowbay"
  - subject: "Re: Mouse buttons"
    date: 2005-01-26
    body: "with the old qt btw..."
    author: "anononymus cowbay"
  - subject: "Re: Mouse buttons"
    date: 2005-01-26
    body: "Wheels aren't the problem. The problem is mouses with extra buttons on the side. Qt/KDE only handles 5 buttons on a mouse right now, and that includes up and down on the wheel, so you can't make back/forward or other buttons without having imwheel or some other program that converts the mouse clicks to X key combinations (on my system, it converts one to Alt-left and the other to Alt-right for back and forward while web browsing).\n\nBeing able to set shortcuts for these buttons in KDE directly would be much better."
    author: "Dolio"
  - subject: "Re: Mouse buttons"
    date: 2005-01-26
    body: "xmodmap is your friend, no nead for extra programs ;)\n\n"
    author: "ac"
  - subject: "Re: Mouse buttons"
    date: 2005-01-26
    body: "His point is that KDE doesn't seem to have a friendy GUI for configuring extra mouse buttons, e.g. binding actions and key combinations to them, and doing so on a per-window/application basis even like imwheel or Windows allow you to do. It's a usability issue. \n\nHow would you go about to do this in KControl?"
    author: "Sho"
  - subject: "Re: Mouse buttons"
    date: 2005-01-27
    body: "Simple, map the mouse-events with X11-keys, and then configure those keys in khotkeys in kcontrol."
    author: "ac"
  - subject: "Re: Mouse buttons"
    date: 2005-01-27
    body: "What's the xmodmap command to remap a pointer button into a keycode? It's not in the man page."
    author: "Dolio"
  - subject: "Re: Mouse buttons"
    date: 2005-01-27
    body: "To answer my own question...\n\nYou can't do anything with xmodmap to remap a pointer button to a keycode. The only alternative I found was to use xbindkeys to execute something like \"xvkbd -xsendevent -text '\\[XF86Back]'\" on a click.\n\nAt this point, one has to ask how this is any better than using imwheel.\n\nEven if a solution were possible with plain xmodmap, it still requires hacking around with text configuration files. KDE has a gui for all sorts of keyboard remapping options, I don't see why mapping extra mouse buttons should be any different (once it's possible). Mapping mouse buttons to keycodes is not a good solution.\n\nOne last question: the KHotKeys allows gestures to be activated by buttons 2 - 9. How is this possible while general mapping of 7 - 9 button mouses isn't?"
    author: "Dolio"
  - subject: "Re: Mouse buttons"
    date: 2005-01-26
    body: "I couldn't get xmodmap to do 'hold windows key, press other key, release windows key'; it would always press one, release, press other, release. I don't know whether this is because of my own incompetence or a limitation in xmodmap, but either way, imwheel does work."
    author: "Illissius"
  - subject: "I just wish Qt included a garbage collector for C+"
    date: 2005-01-27
    body: "C++ is a terrible pain in the ass compared to Java, Python, C# and D for many reasons, but by far the most frustrating thing is the absence of a garbage collector. \n\nTrolltech should create one to make C++ compete with the new, better and easier to use languages now in the spotlight."
    author: "Matt"
  - subject: "Re: I just wish Qt included a garbage collector for C+"
    date: 2005-01-27
    body: "If you want a garbage collector for C++ then use one, there are a number around eg. http://www.hpl.hp.com/personal/Hans_Boehm/gc/ that said, if you keep track of your object lifetimes decently this is really a non-issue. Garbage collectors are useful, but for well designed code they're hardly a big issue.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "And I take it Qt will work well with the garbage collector. Is that correct? \n\nAnyway, even if the code is well designed, it's still an annoying and unecessary complication to manage all the low level memory stuff and I think a Garbage Collector should be default."
    author: "Matt"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "Then you are dumb.\n\nGarbage collectors BY DEFAULT would completely unhinge the expected performance of manually managed memory, dipshit."
    author: "Joe"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "Someone should moderate this guy off this site. His posts are repeatedly rude."
    author: "Max Howell"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "Sorry, but you are at the same time stupid and rude.\n\n* Garbage collection does simplify things. Qt\u00b4s parent-deletes-child does too, but often when you are using multiple libraries from multiple vendors, figuring out the object-lifetime policies of each is quite a chore.\n\n* Garbage collection has a almost negligible impact on performance. Unless you are writing a really time-sensitive piece of code like, say, a driver where you have a required maximum time for a code path (and trust me, most drivers don\u00b4t do this either), you simply won\u00b4t notice.\n\nThe second issue is even more true if your garbage collector is not simply a reference counter, but something like mark and sweep, and specially so if you can do delayed garbage collection. I think Boehm\u00b4s gc does that. Obviously, if you delay deletion until you are outside the time-sensitive sections, the performance may even be better than if you manually delete the objects (not usually, but it *might*).\n\nManual garbage collection is simply another kind of premature optimization.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "No garbage collection has a negligible algorithmic cost. The actual performance cost is often quite bad depending on the used method. The need to make regular sweeps of all memory means that the common practice of swapping unused parts from memory to disk works very poorly with garbage collected programs. Using garbage collection in something as KDE would be a disaster."
    author: "Carewolf"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "But consider the managed code scenario, where you've frequently executing chains of destructor methods to delete things. Not only is there the overhead of calling an often virtual method, but there's the fact that free() is usually quite an expensive call.\n\nGarbage collection frees you from this; it doesn't entirely undo the performance hit it caused by the GC checks, but it does mitigates it. Also garbase collection prevents memory leaks. Good GCs can offer good performance for desktop applications. For libraries, I'm not so sure.\n\n--\nI should point out that another solution to destructor/free() problem is to create and manage your own memory pools. Instead of freeing an object you just return it to the pool, and reclaim it when you want it again."
    author: "Bryan Feeney"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "When GC release the memory, it must call destructor/free()\nor at least free it back to the pool.\nSo, your point is really pointless. =P\n\nThe main problem with GC like it can be seen in Java \nis sluggish performance.\n\nIt works nicely in \"stupid\" scenario, but as soon as \nyou are working with large memory hungry application, GC collapse.\n\nI think that Qt like VCL are using Parent to Child deletion ownership is the right thing to do. \nIt works great, it's efficient and it makes sense.\nThe parent knows when it's deleted; therefore, it can safely delete its children. \nMoreover, it's a nice design pattern that works well. \nIf you are dealing with other kinds of weird pointer stuff then they exist stuff like QGuardedPtr and similar.\n\nThe major problem with GC is that you cannot plan ahead of time deletion \nand force deletion when its actually needed. \nUnless for trivial cases, GC algorithms cannot know what you want to do \nand implement a good algorithm for every corner case. \nAlso, GC especially in Java, don't know that a huge sub-pool or circular dependancy subtree can be safely deleted.\n\nFor instance, let the following \"simple case\" scenario:\n\nParent to child\nA->B->C->D->E\n\nplus the following internal pointers:\nE->C, D->B, E->B, D->C\n\nI want to delete B and everything down the road (C,D,E).\nMost GC won't be able to safely delete B,C,D,E completely, \nbecause of circular dependancies.\n\nHint, this won't work:\nB = null;\nSystem.gc();\n\nBut this works:\ndelete B; \n// Assuming that the child destructor don't delete twice circular pointer.\n\nIf you think that's too \"simple\" imagine a lenghty \ncircular linklist of 16KB buffers, with pointers all over the place.\nOR\nImagine few 1024x768x16 Image Z-buffer in Java and wonder \nwhy 512MB of RAM is insufficient to run the application \nand that it runs out of memory after few minutes...\neven though you do:\nbuffer = null;\nSystem.gc();\n\nOf course, you can say that's not the proper way to do it,\nyou should implement your own memory pool saving algo in Java...\nbut the above \"should work\" if your GC is sooo bright.\n\nSeriously, I think that having parent/child ownership is the way to go.\n\nBTW, if you think malloc/free are inefficient you can always override them\nwith #define's and implement some custom memory pool mechanism.\n\nHave a nice day!\n\nSincerely,\nFred."
    author: "fprog26"
  - subject: "Hello."
    date: 2005-01-27
    body: "QUOTE BEGINS:\nWhen GC release the memory, it must call destructor/free()\nor at least free it back to the pool.\nQUOTE ENDS\n\nA regular free() needs to perform a lot of work each time free() is called, whereas a GC only needs to perform a lot of work once per GC cycle.\n\nIn total they need to perform the same number of effective free()'s, but the GC has more information and can make smarter decisions about how to free things and about memory layout after the free()'s, etc.\n\nNow, I'm not saying you shouldn't be able to give the GC hints, but allowing force delete and the like means that you lose a lot of the built-in safety of the language."
    author: "Xyzzy"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "You are wrong.  All modern garbage collectors are not fooled by circular references.  There are well-known algorithms to solve that problem; they work quite well.  You say you cannot \"force\" deletion when it is \"actually needed\".  When is it actually needed?  When there are no more references to the object; except for circular ones.  Which is exactly when GCs delete it!\n\nYour little example with the Z-buffer is wrong.  If you really removed all references to the Z-buffer, then Java would have deleted it, without you needing to call \"System.gc()\" (which is a sign of desparation and bad programming).  Since you didn't remove all references to the Z-buffer, Java kept it.  Would you rather do it in C++, where your program would crash instead after trying to use the Z-buffer that you deleted?  Perhaps Java should provide a tool to figure out what is still referencing an object, to make debugging this situation easier.  But GC is still better, because it is safer.  In C++ this situation is a security hole and a crash bug; in Java it merely makes things slow."
    author: "Spy Hunter"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "<quote>\nThe main problem with GC like it can be seen in Java\nis sluggish performance.\n</quote>\n\nYou are wrong in this point, Java has a sluggish performance because:\n- The framework used for the UI is terribly slow, and suffers from some bad design desicions.\n- The VM has a lot of cool optimizations taken from Self (http://research.sun.com/self/), but I think that is a big ball of mud. The VM is bloated with a lot of things (like security management, modifications to support generics, an ugly support for metaprogramming, etc, etc), so the memory management is very complicated in Java, hence is slow compared with other OO languages like Smalltalk.\n\nThe problem with GC in C++ is that objects doesn't have a common memory format, so making a GC in C++ is complicated and error prone.\nTake for example Smalltalk (don\u00b4t look at Java or .Net for examples, those languages are really legacy, the only think that makes them \"modern\" or cool is marketing). In Smalltalk the objects in memory are really simple, so the VM is very fast, and can do complicated things (like \"hot\" code replacement or metaprogramming) very simple.\n\nC++/Qt fans are gonna to hate me, but if you want a nice programming enviroment dont't use C++... \n\nI see QT like a low level framework for UI, I know that QT has a lot of things (like strings, collections, etc), but even them C++ is still a pain in the ass.\n\nIs better to use some other language (like Smalltalk for example ;-) ) and let the \"dirty\" things (like drivers, or OS related functions) for C/C++ or assembler, there isn't any noticiable speed loose with this change.\n\nFor those who think that: \"high\" level language = slow and bloated processes, them look at this http://www.smalltalk.org/smalltalk/TheEarlyHistoryOfSmalltalk_Introduction.html\nThey made a system with graphics, GC, a VM that worked on a... Apple Lisa.\n\nRegards,\nDiego.\nPD1: Sorry for my english, is pretty bad :)\nPD2: If you dind't notice I'm a Smalltalk fan... so as a KDE fan, I'm wishing  some integration from one of the best smalltalks (VisualWorks, http://www.cincomsmalltalk.com/) with KDE.\n"
    author: "Diego"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "With generational GC, this concern is mitigated.  The entire memory pool is swept very rarely if at all, allowing disk swapping to work as intended."
    author: "Spy Hunter"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "I was just doing some quick googling on the subject, and it appears that some of the most advanced GC algorithms that also compact memory may actually improve performance over manual management. Compacting memory helps keep the working set in the processor's cache, reducing nasty cache misses.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "I don't think any GC in wide use performs compacting aggressively enough to have a significant benefit in this area.  I imagine that in the general case measuring all memory accesses, figuring out how to compact things, and actually compacting them in memory (requiring copying each moved object and updating all references to that object, for good compaction) would outweigh the performance gain.   Perhaps a novel research GC might do it, but I don't think any general-purpose ones do.\n\nHowever, GCs can improve performance when using large numbers of small objects, since they do memory allocation/deallocation in large batches instead of every time an object is made/destroyed.  Also, if GC replaces a complex manual memory management scheme such as reference counting, it can perform better (of course in addition to massively reducing code complexity, which has many many benefits)."
    author: "Spy Hunter"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "\"I don't think any GC in wide use performs compacting aggressively enough to have a significant benefit in this area.\"\n\nWell this guy is claiming exactly that using C# and Microsoft's runtime:\n\nhttp://blogs.msdn.com/ricom/archive/2005/01/05/347361.aspx\nhttp://blogs.msdn.com/ricom/archive/2005/01/07/348691.aspx\n\n--\nSimon\n"
    author: "Simon Edwards"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-27
    body: "The garbage collector in Python has been benchmarked. It adds 4% overhead. I would hardly call that \"quite bad\".\n\nhttp://www.arctrix.com/nas/python/gc/\n\n\"Using garbage collection in something as KDE would be a disaster.\"\n\nThe user probably wouldn't even notice it was there.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "But I can assure you that the developer would."
    author: "Matt"
  - subject: "Less than 3% overhead"
    date: 2005-01-28
    body: "I also read that Python 2.4's garbage collector produces less than 3% overhead."
    author: "Matt"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "And on the other hand it gives the developers 10%-20% productivity gain, so they don't care anyway."
    author: "Morty"
  - subject: "Re: I just wish Qt included a garbage collector fo"
    date: 2005-01-28
    body: "Well, I write Qt programs on a gc environment, and I don\u00b4t.\n\nOh, sure, it may be more noticeable if Qt itself was gc\u00b4d but really, 4%? 3%? That\u00b4s noise. All gentoo users get more than that slowdown by compiling on the background anyway, and they love it ;-)"
    author: "Roberto Alsina"
  - subject: "Language bindings"
    date: 2005-01-27
    body: "Does Trolltech have any plans on officially supporting bindings to languages other then C++?"
    author: "anon"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "Unfortunately, I don't think so. It would be great to have up to date Python bindings though :) "
    author: "Matt"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "The Python bindings(Qt) are probably one of the most up to date 3rd party bindings out there, for any project. The bindings are always released shortly after Qt, and because of their autogeneratedness the small delay is nearly all test related. But Phil Thompson usually don't release the bindings for Qt betas, a policy I for one agrees with. Why bother with beta bindings to a beta toolkit."
    author: "Morty"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "So you're saying Python Qt supports Python 2.4 and Qt 3.3.4?"
    author: "Matt"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "PyQt supports Qt versions 1.43 to 3.3.3 and Python versions 1.5 to 2.4. PyQt will normally work with newer versions of Qt and Python without change. If changes are required then these are normally added to snapshots within a few days."
    author: "foo"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "On the aKademy i heard they where concidering Java and C#/mono as possible languages to officially support bindings for.\n\nWhen trolltech decides, i go and master that language. Java never was really cross -desktop (platform) on the GUI, it just look like shit on every platform. Mono/GTK# is becomeing a good alternative, yet it also doesnt use native widgets on each platform/desktop.\n\nI'm still a bit on hold regarding managed code."
    author: "ac"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "Did they say anything about Python?"
    author: "Matt"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "\"On the aKademy i heard they where concidering Java and C#/mono as possible languages to officially support bindings for.\"\n\nThey did talk about experiments about how to integrate Qt with .NET, with callbacks to allow C++ virtual methods to be overriden in C# and so on. As far as I know they aren't developing or supporting the development of language bindings for the complete Qt api at present. \n\nIt's very difficult to sell a java GUI toolkit like QtJava,  if the competition is entrenched and give away their stuff for free as they do with Swing or SWT.\n\n\"When trolltech decides, i go and master that language.\"\n\nI think dynamic languages like python, perl and ruby work particularly well with Qt and all have very good bindings - PyQt, PerlQt and QtRuby. So I wouldn't wait for trolltech to tell you what to do - if you master the Qt api in one language you can easily switch to another because they are all pretty similar.\n"
    author: "Richard Dale"
  - subject: "Re: Language bindings"
    date: 2005-01-27
    body: "\"I think dynamic languages like python, perl and ruby work particularly well with Qt and all have very good bindings - PyQt, PerlQt and QtRuby. So I wouldn't wait for trolltech to tell you what to do - if you master the Qt api in one language you can easily switch to another because they are all pretty similar.\"\n\nmastering is i big word, but i use the Qt api in C++. Yet i wait for officialy supported bindings before decide to go and master an other language than C++.\n\n"
    author: "ac"
  - subject: "Re: Language bindings"
    date: 2005-01-28
    body: "Maybe, but the documentation for Pyqt is not so great, that's why I want Trolltech to support it."
    author: "Matt"
  - subject: "Re: Language bindings"
    date: 2005-01-28
    body: "PyQt is the only Qt binding that's got a book written about it... Okay, so I didn't have time to keep the text updated, what with Krita and writing a novel and so on, and the publisher failed to get it available through Amazon which meant that it might as well not have been published at all, but I still have got a few copies for people who cannot find it in the bookshops."
    author: "Boudewijn Rempt"
  - subject: "Re: Language bindings"
    date: 2005-01-29
    body: "I do not agree with your statement on the documentation.\n\nFor one thing, you can just take the C++ documentation and work with it. The few cases that do not map exactly the C++ are documented in one html page on Phil's PyQt site and that has proven to be very sufficient to me.\n\nThe other thing is that the support of Phil is great and very reactive (I wish TheKompany could have the same level) so I doubt that you will run into any problem. And there are a lot _less_ problems when programming in python than in C++.\n\nAnd since Trolltech has license agreement that allows TheKompany to sell PyQt, you can consider that like an officiel support, no ?"
    author: "Philippe Fremy"
  - subject: "KDE 4 ??"
    date: 2005-01-27
    body: " How is this going to affect the schedule of kde 4.0?? is there going to be one more release in kde 3.x series..."
    author: "kp"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "I don't think so. Two reasons: Qt4.b1 was not too bad and since its release a lot things already improved in the snapshots. Second reason: It was not even clear if there should be a KDE 3.4.\nIf the core-devels decide to do a 3.5-release it will take 4 to 6 month. 3.4 will be released in March, so 3.5 won't be out before August 2005. At that point of time Qt 4.0.0 should already be out."
    author: "anon"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "So when will we start porting to Qt-4?? We don't wanna wait until qt-4 final is out do we??\n\n"
    author: "performa 300"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "If there will be no 3.5 (what I think) porting will start pretty much as soon as kde 3.4 is out the door."
    author: "anon"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "Assuming there will be no 3.5, how long is KDE 4 estimated to take? A year?"
    author: "Matt"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-29
    body: "No, we wanna help/influence qt-4 final."
    author: "Anonymous"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "don't expect it before December 2005."
    author: "jambo"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "Warning, personal opinion only: After the release of 3.3 we start with KDE 4.0. That will be taking a while and will be rather end of Q4 than end of Q2, if not later. The Qt betas should be good enough to start the porting to Qt4 (are they?) KDE 3.3 will be the last release in the KDE 3.x series, apart from the occasional KDE 3.3.x bugfix releases."
    author: "Waldo Bastian"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "You mean 3.4, right?"
    author: "Matt"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-28
    body: "Indeed :-\\"
    author: "Waldo Bastian"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-27
    body: "s/3.3/3.4/g :-)"
    author: "Anonymous"
  - subject: "Re: KDE 4 ??"
    date: 2005-01-28
    body: "Amen!\n\nIf we wait until Qt4 is out we probably won't see kde-4 until first half of 2006 at the best.\n\n"
    author: "performa 300"
---
<a href="http://www.trolltech.com/">Trolltech</a> has announced the <a href="http://www.trolltech.com/newsroom/announcements/00000191.html">release of Qt 3.3.4</a> which is a maintenance release including <a href="http://www.trolltech.com/developer/changes/changes-3.3.4.html">many bug fixes and optimizations</a>. The final Qt 4.0 release is now <a href="http://lists.trolltech.com/qt4-preview-feedback/2005-01/thread00625-0.html">anticipated for late second quarter</a> of 2005 with two
interim beta versions to be released in March and May.

<!--break-->
