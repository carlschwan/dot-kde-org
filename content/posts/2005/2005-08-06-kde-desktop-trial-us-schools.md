---
title: "KDE Desktop Trial in US Schools"
date:    2005-08-06
authors:
  - "jriddell"
slug:    kde-desktop-trial-us-schools
comments:
  - subject: "Challenging"
    date: 2005-08-06
    body: "Some of the bravado on the rate of success is common place for any OS.  I hope the feedback the kids give, including the faculty, are invaluable in shaping KDE 4.0 and future incarnation."
    author: "Marc Driftmeyer"
  - subject: "Re: Challenging - problems to anticipate"
    date: 2005-08-06
    body: "I see trouble if students try to install one of the Linspire based software packages onto other Linux based distros. After all, they are both \"Linux\" in a way, right? The proper thing to do is to also educate users that they are using just one of the more than 100 Linux based distros, and that a software packaged for Linspire might not necessarily work with another distro. An analogy could be made of internal combustion engines using the same gasoline but whose parts cannot be guaranteed to work in all gasoline engines."
    author: "cb"
  - subject: "Re: Challenging - problems to anticipate"
    date: 2005-08-06
    body: "Don't you fret yourself over those kids. The hood is not bolted on a Linux distribution like on proprietory operating systems. After all it was kids that put the Oldsmobile v8 in the Mercury, 6 Stromburgs on a flathead Ford, nitro in the tank, nitrus in the bottle. The kids will do just fine, the adults having to keep up with them will be the problem.\n"
    author: "Dick Colclasure"
  - subject: "Re: Challenging - problems to anticipate"
    date: 2005-08-06
    body: "Well, other distro's ship Linspire-software as wel.\nLike SUSE 9.3, which includes Linspire applications like LPhone and NVU\nSo I don't forsee much trouble.\nBesides, when installing software for Windows, you need to be ware that you pick the right version: win 95/98/98Se/me/nt/2000/xp/vista etc..\n"
    author: "rinse"
  - subject: "Re: Challenging - problems to anticipate"
    date: 2005-08-06
    body: "Afaik Linspire is still Debian based which should theoretically make it compatible with other Debian and Debian based Linux distributions."
    author: "ac"
  - subject: "Re: Challenging - problems to anticipate"
    date: 2005-08-08
    body: "Yes as mixing packages from etch and sid wich are still debian. It's very funny to mess up the dependecies. Other example: sarge packages won't work in Ubuntu Hoary because Hoary has an older libc and if you ever try to install the libc from sarge you'll also mess up the whole system.\n\nInstalling debs from other debian based distros is like installing rpms from other distros."
    author: "Daniel Est\u00e9vez"
  - subject: "\"KDE based operating system\""
    date: 2005-08-06
    body: "\"KDE based operating system\"? Wow.\n\nI mean, I love KDE and all (no, really), but I wouldn't claim my OS is based on it. In my case, it's based on the Linux kernel and some GNU libraries and tools, and is running KDE.\n\nWhere can I get this KDE-based OS?"
    author: "Anno v. Heimburg"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-06
    body: "You're getting too technical.  \n\nIt's like saying a \"Windows-based OS\" instead of, more properly, a DOS-based OS.  DOS may be at the base of it all and is used for bootstrapping, but what really matters are the higher-level APIs and services exposed by Windows.  Same thing goes for Linux.  All the Linux stuff is abstracted away in the higher-level KDE API and services. Linux doesn't even have to be there, it could be BSD or Solaris.\n\nHence it makes sense to speak of a KDE-based OS or at least Operating Environment."
    author: "Anonymous"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-07
    body: "And why would you put the GNU tools in the OS? they are programs in userland, not part of the OS."
    author: "Davide Ferrari"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-08
    body: "http://www.gnu.org/gnu/linux-and-gnu.html"
    author: "Mark"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-09
    body: "Of course they are part of the OS. The OS won't work without them."
    author: "Yama"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-09
    body: "by that logic, the computer is part of the OS. The OS won't work without it....\nAnd for many people, an OS without KDE would be as useless as an OS without KDE tools"
    author: "Vegeta"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-09
    body: "The computer is hardware, not software. The operating system runs on top of the hardware.\n\nA system can run just fine without KDE. Does running GNOME mean you're running a different OS? Of course not."
    author: "Yama"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-09
    body: "If I substitute GNU utilities with different ones, do you think I am running a different OS?\n\nYour logic is that things that are necessary for an OS to function are part of the OS, and that is a fallacy.\n\nGNU/Linux is a political and marketing term invented by RMS to get credit for something he didn't write.\n"
    author: "Vegeta"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-09
    body: "> If I substitute GNU utilities with different ones, do you think I am running a different OS?\n\nYes, it the same kernel but a different os."
    author: "cl"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-10
    body: "How is an _operating_ system supposed to _operate_ with just a kernel? Not even MS-DOS (which is a very simple OS) works like this. Don't bring politics into this. It's just plain common sense."
    author: "Yama"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-10
    body: "You question is nonsense.  I could ask you: \n\nHow is an _operating_ system supposed to _operate_ without applications? Not even MS-DOS (which is a very simple OS) works like this.  Don't bring politics into this.  It's just plain common sense."
    author: "Anonymous"
  - subject: "Re: \"KDE based operating system\""
    date: 2005-08-10
    body: "\"How is an _operating_ system supposed to _operate_ without applications?\"\n\nIt runs just fine. Do you even know what an operating system is? Here's a hint:\n\n  http://en.wikipedia.org/wiki/Operating_system\n\n\"Don't bring politics into this.\"\n\nI never did. In fact, I said this _wasn't_ about politics (which you should know since you blatently ripped off my own words).\n\nIf you are going to troll, at least make some sense."
    author: "Yama"
  - subject: "This is huge"
    date: 2005-08-07
    body: "A major deployment (even if its only a trial) in a US school system is huge!\n\nPeople expect European govnts to do \"crazy socialist\" things like trial OSS, but something like this in the US is exciting. I really hope the trial is successful, and leads to a real deployment in the future, like GNOME in those Spanish schools. \n\nWith a good sysadmin, a linux based OS *can* be as good as a windows based one, with lots of cost savings to boot.\n\nHeres hoping!\n\nI think its too bad RH or Novell have never been as aggressive in getting linux in schools."
    author: "koldpete"
  - subject: "(Ihave used linus since Win 3.0 Admin yet?)"
    date: 2005-08-08
    body: "Riddell,\nThis is refreshing!Finally some news We want to hear!\nKeep up the good work,man good story!\nKeep us posted          (hey i made a pun)\nI think Linux would teach kids more about computers \nin a single semester than all of the \nMicrosoft Certifications available today(combined)\nalso,any other scholastic endeavors subject matter as well!\nLike Algebra,Quadratic equations,Astronomy,all sorts of desktops are \navailable to them,they could contain absolutely nothing on\nthe hard drive,all bootable cds,and the PC's could be recycled,with\nno real damage after time!just replace the cdrom drives!"
    author: "kevin k"
---
<a href="http://www.linspire.com/">Linspire</a> has announced that their KDE based operating system is being <a href="http://www.linspire.com/lindows_news_pressreleases.php">trialed for use by schools in Indiana state</a>.  The Indiana Access Program will provide every classroom with computers for all their pupils. There are already several thousand Linspire machines running KDE in use in dozens of classrooms across the state to explore the benefits of one-to-one classroom computing.  Introducing KDE to high school students in this manner will equip these young people with the skills necessary to be comfortable and familiar with the Open Source platform. Many of these students will likely go on to use KDE after school and perhaps even join the KDE project as contributors, helping to spread KDE and GNU/Linux on the desktop even further in North America.


<!--break-->

