---
title: "Quanta Starts Transition to KDevelop Framework"
date:    2005-07-26
authors:
  - "jherden"
slug:    quanta-starts-transition-kdevelop-framework
comments:
  - subject: "slightly off-topic, but..."
    date: 2005-07-26
    body: "does anybody know a good repository for up-to-date Quanta .deb packages? I noticed yesterday that kdewebdev in Alioth is still at 3.4.0, while I'm almost sure that Quanta got some updates after that."
    author: "Giacomo"
  - subject: "Re: slightly off-topic, but..."
    date: 2005-07-26
    body: "The work on KDE packages is currently on hold due to the C++ ABI transition taking place.\n\nhttp://lists.debian.org/debian-qt-kde/2005/07/msg00172.html"
    author: "Kevin Krammer"
  - subject: "\"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "Please excuse a troll to leave a comment while passing by...\nI'm watching the Quanta+ project remotely for quite some time, without having the time and resources to actually use and test it, so please apologize for commenting without actually participating...\n\nPlease do *not* call the preview version \"Kuanta\", this is a butt ugly name. (I know, i know, this is due to tighter integration with a KDE core application but still.)\n\nSuggestions? How's about \"KWebDevelop\" or \"KHTMLEdit\""
    author: "Robert"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "Hello,\n\nhold on, this is a preview name only. With the next release it will already be a thing of the past. Or do you still know the release name of KDE 3.3 ?\n\nOther than that, great work. I suspect Eric has put a lot of effort into making this happen. Quanta and KDevelop were indeed seeing a lot of duplication. Now that the later is mature and can actually be used as a foundation for Quanta, there is chances for better web focus.\n\nFrom KDevelop I have to expect more though. In fields unrelated to Web, C++ and Ada, the language integration is not up to speed yet. Are there plans to leverage on gcc 4.0 and its parser to better understand the languages?\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "<i>\"Are there plans to leverage on gcc 4.0 and its parser to better understand the languages?\"</i>\n\nNo.\n\nAnd before you protest, yes, I know gcc 4.0 has the best c++ parser around.  Yes, I know it _seems_ like it'd be a no-brainer to 'leverage' this for things like code completion and refactoring.  Unfortunately, that is not how these things work and it is a naive assumption.  A code completion parser requires the ability to parse incorrect code or half completed statements and not choke and throw an error.\n\nThat said, the new kdevelop _will_ have a brand new parser that will hopefully go a long way towards improving things.  Cheers."
    author: "manyoso"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "Hello,\n\nI do not reserve the right to protest. I have only asked and in no way intended to offend anybody involved.\n\nI do reserve the right to be naive, despite being a \"developer\" what ever that means. :p\n\nThat said, I don't think that code completion and gcc 4.x are probably worth to marry. The gcc parser understands so many languages in near perfect ways. It gets some performance tuning and stuff that a separate parser engine would not have.\n\nEven though it may be hard, maybe it's worthwhile? Do you know if somebody has actually tried it? I might be willing to try it myself...\n\nYours,\nKay\n\n\n\n\n"
    author: "Debian User"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "You know it's rather easy to try too, and you will see it's not good. Take some half-finished code with errors and watch the gcc 4.x parser barf on it. Then try visualize how that can help you with code completion when writing the said code. "
    author: "Morty"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "It is ok if you are not a developer, but just understand that what seems obvious to you is actually quite naive in actuality.  Now, if I sounded hostile it is because there was a very long thread recently on kdevelop-devel that had a lot of _developers_ who were postulating the very same thing.  All of them had never written or worked on a code-completion in kdevelop before.\n\nAnd once again, I know you think it would 'probably worth to marry', but you would be WRONG.  This is a naive belief that roughly goes:\n\n\"Hmm, we need a great C++ parser for code completion\"\n\"Well, g++ is probably the most complete we could use\"\n\"BINGO!  That's a great idea, we should use g++!\"\n\nBut, once again, this is WRONG!\n\nThe parser might be the best at parsing and compiling compilable code, but it sucks as a code completion parser.  A code completion parser must be able to parse _incomplete_ uncompilable code.  And it can't just produce an error and stop.  Think about it...  \n\nIn short, the g++ parser would have to be so heavily modified that it would lose the advantage of sharing code with the gnu folks.  It would fork and diverge.  Not only that, but the actual g++ parser would be a pain to retarget and modify -- and yes, I've looked and have actually hacked on both g++ and kdevelop's code completion and parser."
    author: "manyoso"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "I have always thought that the best way to improve parsing of incorrect code would be to give the parser some memory of recent changes.  For example, if the project started out as completely valid code and then you modified a method definition (without changing the code that callst the method), it would be hard to parse the resulting invalid code, but if the parser remembered that the old method used to exist it could make many simplifying assumptions as it parsed the new invalid code and give more helpful error messages too (\"method foo has been changed to bar\" instead of \"unknown identifier 'foo'\" or the like). \n\nAre there any plans or ideas out there for this kind of parser?"
    author: "Spy Hunter"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-27
    body: "I don't know of any plans to do a parser like you describe. Parsers are tedious and involved code that takes a lot of time and debugging. As far as providing better error messages I think your example shows how much more complex it really is. For a relevent changed method message you would need persistant memory of changes for both files which would be like incorporating a revision system into a parser. It's an interesting idea, but parsers have to process so much data optimization is a key factor and making them revision aware would likely be a nightmare and slow. A more ideal system would be to offer the option to find and change the relevent method calls in the project or put them in an edit queue. My preference in adding automation is that it actively seeks to reduce errors.\n\nBTW I am not aware of everything going on with the KDevelop parser, just what we've been doing with Quanta so far. That's the perspective I'm looking at. It will be exciting to review using their parser in places or possible integration and modular parsing concepts. We will have inherent differences as well as similarities."
    author: "Eric Laffoon"
  - subject: "Telepathic parsers possible?"
    date: 2005-07-27
    body: "'..but if the parser remembered that the old method used to exist it could make many simplifying assumptions'\n\nYou really talking about code refactoring here, where the IDE has parsed the source and formed an 'Abstract Syntax Tree' and symbol table of what the names used in the program code mean. Then because it knows about the structure of the code, and not just the correctness of the surface syntax, you could specify refactoring rules to transform the AST. For instance, you could add some text before or after a node, or you could move a node to another part of tree. Moving a method to another class, or renaming a method and fixing up all references to it in the code are examples of refactoring. The new parser generator and language parsers for KDevelop 4.0 will be able to use a common refactoring engine, which will work with ruby just as well as C++.\n\nIf a syntax checker was giving you errors based on what the code looked like 5 or 10 minutes ago it would just be really annoying, and feel more like the editor had only half saved your work. There's no way it would be possible to 'read you mind' to find out what you were intending to do. Hence, the need to work at a higher level than typing in lines of text, like using a refactoring engine."
    author: "Richard Dale"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "The following recent threads probably interest you:\n\nNew parser branch (Was: Dumping the source DOM?)\nhttp://lists.kde.org/?l=kdevelop-devel&m=112013934809419&w=2\n\nprobably the best C++ parser...\nhttp://lists.kde.org/?l=kdevelop-devel&m=112023016703775&w=2\n\n"
    author: "cl"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "> Other than that, great work. I suspect Eric has put a lot of effort into making this happen. Quanta and KDevelop were indeed seeing a lot of duplication. Now that the later is mature and can actually be used as a foundation for Quanta, there is chances for better web focus.\n\nActually Quanta 0.9x was basically a stripped version of KDevelop with an HTML preview and little else. When Andras came on and KDevelop 2x had a new more modular architecture I asked his opinion on adopting the framework but it did not appear practical with our resources. During aKademy last year Jens brought this idea up and it had pretty much fallen into the dust bin as nobody else was thinking about it at the time. Now we have more resources and much of the duplication of effort is more obvious. I think Jens was a little surprised how easily I agreed and we set up a meeting with KDevelop developers. What was really exciting is how eager they were to work with us. That totally changed the complexion of things and turned it into a \"why didn't we do this sooner?\" question.\n\nAs to who deserves credit for the effort to making this happen, I would say that credit goes to Jens Herden who was so dedicated to the cause from the start and who has put so much time and effort into it. We're very fortunate to have Jens on our team and I'd like to personally and publicly thank him for his initiative, skill and caring hard work on our project."
    author: "Eric Laffoon"
  - subject: "KDevelop language integration not up to speed"
    date: 2005-07-27
    body: "\"In fields unrelated to Web, C++ and Ada, the language integration is not up to speed yet.\"\n\nNot so. Ruby support in KDevelop 3.2 was improved quite a lot with a ruby debugger, Qt Designer integration, KDE project templates and an improved class parser. It is certainly 'up to speed', and at least as good as in other ruby IDEs.\n\n"
    author: "Richard Dale"
  - subject: "Re: KDevelop language integration not up to speed"
    date: 2005-07-27
    body: "Speaking of the Ruby application template: wouldn't it be better to remove the C++ starter application?\n\nHaving it in the template requires the developer to install the Ruby devel package for its headers."
    author: "Kevin Krammer"
  - subject: "Re: KDevelop language integration not up to speed"
    date: 2005-07-27
    body: "Yes, that is a problem. The reason it starts the ruby code via a C++ stub is that it would be exactly the same whether it was a KPart project or a kicker applet. For those other project types there is no choice but to start via C++, as they have to be libraries dynamically loaded from a running KDE app. \n\nThe current automake based build system for KDE a way too complicated for ruby anyway. I think the best solution would be to have another application template with exactly the same ruby code, but using a simpler build/install system and no C++ top level stub."
    author: "Richard Dale"
  - subject: "Re: KDevelop language integration not up to speed"
    date: 2005-07-27
    body: "Ah, I see. I suspected that there was a reason for it, but I was neverthelesse quite surprised when I tried the template and got a compiler error about a missing header :)\n\nI agree that having another ruby-only template would be a good idea.\n\nBtw, if one has two Ruby KParts/Kicker applets, will the two calls to ruby_init be problematic or does the Ruby interpreter lib handle this grazefully?"
    author: "Kevin Krammer"
  - subject: "Re: KDevelop language integration not up to speed"
    date: 2005-07-27
    body: "\"neverthelesse quite surprised when I tried the template and got a compiler error about a missing header\"\n\nYes, in fact you have to run the usual 'make -f Makefile.cvs' and 'configure' commands via the GUI to install. So you get incomprehensible configure stuff for about 5 minutes. \n\nNote that you've got the KDE problem where you can't actually debug you program until it's installed because it won't be able to find xmlgui menu items and so on. Or if you have a KPart project, it won't run until the KPart is installed. I really hope that's fixed in some way for KDE 4 for C++ and other languages like Ruby. Maybe there should be 'share/apps' directories under the KDevelop src dir with symbolic links from the subdirectories into the src dir, and the src dir added to $KDEDIRS. But I don't want to have a ruby specific work round, it needs to be sorted properly.\n\n\"Btw, if one has two Ruby KParts/Kicker applets, will the two calls to ruby_init be problematic\"\n\nIt works fine if you call it twice - it just doesn't do anything the second time."
    author: "Richard Dale"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-26
    body: "It is just a codename. Nothing serious, Quanta will remain Quanta, for sure!"
    author: "Andras Mantia"
  - subject: "Re: \"Kuanta\" ...are you kidding or what?"
    date: 2005-07-27
    body: "Yes, it was too late to name it Kuato (from Total Recall) for version 3. ;-)"
    author: "Eric Laffoon"
  - subject: "Error in the article?"
    date: 2005-07-26
    body: "I may be wrong, but shouldn't it read \"But we are confident to release a first preview version called \"Kuanta\" shortly after KDE 3.5 is released.\" instead of \"...after Quanta 3.5 is released\"?"
    author: "heiner"
  - subject: "Re: Error in the article?"
    date: 2005-07-26
    body: "KDE 3.5 will ship Quanta 3.5 so it's equivalent."
    author: "Anonymous"
  - subject: "Re: Error in the article?"
    date: 2005-07-26
    body: "I asked Jens to write this because I wanted to hint that there will be a Quanta 3.5 released. But yes, Quanta 3.5 will be part of KDE 3.5, so the two statements are equivalent."
    author: "Andras Mantia"
  - subject: "The obvious question of course..."
    date: 2005-07-26
    body: "Where are the screenshots? ;-)"
    author: "Henning"
  - subject: "Re: The obvious question of course..."
    date: 2005-07-26
    body: "Good idea. :-) I will post them soon.Jens left Romania today and we were very busy yesterday with other things as well (in Quanta) aside of the dot article, so we forgot to create screenshots."
    author: "Andras Mantia"
  - subject: "Screenshots"
    date: 2005-07-26
    body: "Some screenshots are available at\n http://kdewebdev.org/~amantia/kuanta/kuanta-shots.htm\n\nDon't blame the colors/layout/style, see the content.\n\nAndras"
    author: "Andras Mantia"
---
After five weeks intensive work in Romania <a href="http://quanta.kdewebdev.org/">Quanta</a>'s main developer Andras Mantia together with Jens Herden finished the first major step of Quanta's port to the <a href="http://www.kdevelop.org/">KDevelop</a> framework. Being able to get this result in a relative short time was only possible because of the excellent framework provided by the KDevelop project and the support of <a href="http://www.kde-ev.org/">KDE e.V.</a>.




<!--break-->
<p>Only with this support Jens Herden was able to travel from Cambodia, his current living place, to Romania. Being in Romania made it also possible to travel to Kiev to attend the KDevelop conference and meet the developers personally.</p>

<p>The goal of this new development is to divide up Quanta's code into several plugins for the KDevelop framework. Quanta will remain a separate application and keep its name.</p>

Some benefits of this are:
<ul>
<li>
Code sharing between both projects to avoid double efforts for the same functions.
</li>
<li>
New and old plugins can be used from both applications.
</li>
<li>
Quanta's code base will be much easier to understand and maintain.
</li>
<li>The modular design allows the user to create his/her customised Quanta by disabling unwanted plugins or enabling plugins from other sources.
</li>
</ul>
<p>
The new, plugin based Quanta will be released together with KDE 4.0. But we are confident to release a first preview version called "Kuanta" shortly after Quanta 3.5 is released. </p>
<p>
The source code is available on the SVN server under
<a href="http://websvn.kde.org/branches/work/kdevquanta/">/branches/work/kdevquanta/</a>.
</p>
<p>    
This version is already usable and does provide the basic features of Quanta.
<br>
More information can be found in the <a href="http://websvn.kde.org/branches/work/kdevquanta/README">README</a> and <a href="http://websvn.kde.org/branches/work/kdevquanta/DESIGN">DESIGN</a> files. 
</p>



