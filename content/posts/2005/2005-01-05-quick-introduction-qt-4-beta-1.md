---
title: "Quick Introduction to Qt 4 Beta 1"
date:    2005-01-05
authors:
  - "jthelin"
slug:    quick-introduction-qt-4-beta-1
comments:
  - subject: "I wish they'd gone the other route with QtDesigner"
    date: 2005-01-05
    body: "I have to say, I for one wished development would have continued along the lines of QtDesigner from Qt3 with the following features:\n\n1) Adding the ability to show, in a popup, the available members of a non-visual class. e.g. when writing\n\nMyClass a;\nint b = a.\n \na popup would appear after the dot with all the available members for \"a\"\n\n2) Adding build, compile, and debug buttons to the IDE, harnessing the knowledge that went into QMake to get it working. However I accept that there are some compilers where it may not be possible to do this.\n\n3) Full project management, represented graphically as in KDevelop with docked window with three tabs for file-system, class and GUI members.\n\nNone of this is very far removed from what QtDesigner 3 does now. Such a product could then be shipped on Windows and MacOSX with the GNU compiler as a fully integrated development environment with no additional costs. It would lure in a lot of small companies. An additional sweetener would be to enhance this down the line to support Java and QtJava bindings.\n\nHowever I'm not privy to everything that's going on in Trolltech, and I know Designer is still in the early beta stages, so I'll wait and see.\n"
    author: "Bryan Feeney"
  - subject: "Re: I wish they'd gone the other route with QtDesigner"
    date: 2005-01-05
    body: "Regarding item #2, I've done a part of that work. Check out http://www.digitalfanatics.org/e8johan/projects/deside/index.html .\n\nOne of the problems with this is that it requires a certain build system (make/nmake). The other problem is that the .ui-files are more or less bound to a certain Qt release. A better, but more complex, solution would be to add a non-Qt-specific layer, to make it possible to generate a generic XML-UI-description file..."
    author: "Johan Thelin"
  - subject: "Re: I wish they'd gone the other route with QtDesigner"
    date: 2005-01-06
    body: "Funny you should mention the \"build\" issue.  I just sent a patch to the qt4 mailing list to add the capability into qmake.  It is quite simple and I hope Trolltech implements it or something like it."
    author: "Justin Karneges"
  - subject: "Re: I wish they'd gone the other route with QtDesigner"
    date: 2005-01-06
    body: "#1 - the main feature i miss from Delphi"
    author: "nick"
  - subject: "QT Designer."
    date: 2005-01-07
    body: "was it really needed to separate qt designer into multiple windows.\nhttp://qt4.digitalfanatics.org/images/i/2.png\n\nI know many people comment bad about the gimp because of this...\nwouldn't this be just the same?\n\nit would be very nice if this mode was optional and the developer could choose.\nI for one, don't like it.\n"
    author: "Mark Hannessen"
  - subject: "Re: QT Designer."
    date: 2005-01-08
    body: "Personally, I think that it is a thing of the future. If just the window managers could be taught to raise all windows of one application instance, this layout is great for multiple screen scenarios."
    author: "Johan Thelin"
---
The author of the <a href="http://www.digitalfanatics.org/projects/qt_tutorial">Independent Qt Tutorial</a> has published <a href="http://qt4.digitalfanatics.org/">an example-driven article</a> about <a href="http://doc.trolltech.com/4.0">Qt 4 Beta 1</a>. The text touches the new Designer and some of the code-level changes that will affect application developers.



<!--break-->
