---
title: "OSDir.com:  KDE 3.4 Beta 2 Screenshots"
date:    2005-02-14
authors:
  - "binner"
slug:    osdircom-kde-34-beta-2-screenshots
comments:
  - subject: "KDE<->GNOME Screenshot Comparison"
    date: 2005-02-14
    body: "http://www.fireflybsd.com/screenshots/ - GNOME is now discussing why their desktop looks boring in comparison to KDE."
    author: "Anonymous"
  - subject: "Gnome steps over the line"
    date: 2005-02-14
    body: "Haven't they really stepped over the line, asking money from children?\n\nhttp://www.gnome.org/"
    author: "KDE User"
  - subject: "Re: Gnome steps over the line"
    date: 2005-02-14
    body: "I thought I'd seen it all......"
    author: "David"
  - subject: "Re: Gnome steps over the line"
    date: 2005-02-15
    body: "but the look is still ugly (altough KDE.org isn't that beatifull)"
    author: "superstoned"
  - subject: "Re: Gnome steps over the line"
    date: 2005-02-16
    body: "That's just a joke, man.  They're not asking kids for money."
    author: "Panther"
  - subject: "Re: Gnome steps over the line"
    date: 2005-02-22
    body: "They aren't asking kids for money. Not many kids even know what GNOME is."
    author: "uman"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-14
    body: "I laughed when I looked at the second picture in each category (I was only looking at the one on the left at first and was a little confused).  GNOME looks like OS 9 (i.e. BORIN-*falls asleep* ).\n\nI think someone should make a gallery of screenshots showing KDE looking like as many other different desktop as possibly, and also another category thats completely unique KDE desktops."
    author: "Corbin"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-14
    body: "Rather harsh ... Beauty is in the eye of the beholder and to each its own ...\n\nOne could also call the Gnome desktop professionally looking, which I personally think it does, more so than the KDE one. \n\nWith kind regards, Pieter (KDE user)."
    author: "Pieter Swinkels"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "I am fed up with this professional and clean looking nonsense!\nProfessional means by definition: engaged in by professionals; as, a professional race; -- opposed to amateur.\n\nIf someone is a professional s/he can probably use any tools available \nto conduct her/his trade but would definitely know and prefer to use most\nsuitable and powerful tools. I believe this holds for almost every profession. \n\nKDE provides a state of the art desktop environment which is very\npowerful and its design is improving in every iteration. Just konqueror\nis a sufficient proof for this. For example, as a grad student I use\nKontact to keep my addressbook, read mails, RSS feeds, news etc.\nKile to write my papers; Konqueror to view web, transfer organize \ndownload files,k3b to backup, kdict, amarok, inline spellcheck and countless other things.\nMeanwhile with plastik and kde-look.org, the desktop is a beautiful\nenvironment and is a pleasure to look at...\n\nIn addition, the ok and cancel buttons are where they are supposed\nto be since this has been the convention since I have been interacting\nwith GUI computers (Windows, CDE, linux). Damn the Gnome HIG stupid\nnonsense which stands out like a sore thumb. If these guys don't know what \na convention means than they should be fired!\n\nBecause of all these reasons, I call KDE professional!\n\nend of rant!"
    author: "Noname"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "Although this discussion does not belong here, I want to clarify one thing. We're talking esthetics here, I never said KDE was not built or could not be used by professionals. I think it's a great piece of work and it's my desktop environment of choice.\n\nBut with regards to the available themes, I prefer minimalistic (for the sake of a better term) themes such as the SimpleGnome theme or (what I think is) the Mist theme, see http://www.gnome.org/~davyd/gnome-2-10/ for an example. The themes at KDE-look.org are all a bit more \"elaborate\": bigger widgets using more screen real estate, more colors. By the way, I am currently using ThinKeramik, which is a quite elaborate theme :)\n\n\n\n\n"
    author: "Pieter Swinkels"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-16
    body: "Sorry, I was a bit carried away! It might be a result of having seen stupid\ncomments on KDE in Slashdot for a long time...\n\nAfter your comment, I look at my desktop again and have realized KDE \neverything but minimalistic(unless you tweak it). Therefore, it does not\naddress the needs of people who just want a minimalistic interface by \ndefault. OTOH, there are really good DE's to fill this niche, like XFCE \netc. That is a totally different approach and KDE does not try to compete \nwith those AFAIK...\nI personally like a bit of clutter and color!\n\n\n"
    author: "Noname"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "Would be interesting to have such comparison with Plastik against GNOME's default theme. Should be even better looking for KDE. :-)"
    author: "Anonymous"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "To emphasize this a quote from the GNOME desktop list: \"Keramic (?) looks like Fisher-Price silliness, but Plastik is the best theme on Linux IMO.\" (http://mail.gnome.org/archives/desktop-devel-list/2005-February/msg00208.html)"
    author: "Anonymous"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "Boring is good!\n\nThe DE should not impose on the user. Too much brigt colors makes it hard to convey important messages. If everything is bright red, green, blue,...\nHow do you make an important message stands out, in such environment?\n\nWhat function has the KDE logo at the left side of the K-menu? What function have large KDE logos on the desktop, other than making it harder to see icons on the desktop.\n\nIn this respect the new KDE is a bit of a disappointment.\nApart from still being too bright, in coloring not much else seam to have been done to improve usablility. Just the usual rerrangement of kcontrol. Even though the rearangement this time was to the better, it would probably been more worth while to fix things that users use more often. Good candidates for major overhauls would be konqueror, kontact or the kicker.\n\nSpeaking of the kicker why does it have to look like something from microsoft. The clock (still with ugly digital watch font) occupy one of the places of the screen that is easiest to reach with the mouse. Why such a waste of such a good position. Why not place the pager applet here instead. And why is the pager applet divided into two rows? According to Fitts law things at the screen edges is easier to reach as the present a larger target. If more panelspace is needed why not have a panel on the top of the screen as well?\n\nAll in all KDE needs to have simpler defaults. Why do konqurer need 15 buttons in the toolbar while firefox only have 7. To be fair, konqi would need some more to doulbe as file browser, but do it really need more than twice as many.\nAnd why does the default order of common buttons like left, right and up arrow have to differ from the default order on other commonly used browsers?\n\nToday the only compeling reason to use KDE over Gnome is working kio-slaves. They make it feel like you are sitting in front of the internet, and not just a computer. But that advantage will not last forever. Soner or later the Gnome people will get gnome-vfs to work, and that day KDE will have to be able to offer something else, or it will diminish in use.\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "Well, I love KDE, but I have to agree with you on quite some points... konqi is a mess, they should really ditch some icons. altough it got a bit better in filebrowsing mode. and I also agree on the two panels, I use two, and it works better for sure... I'd say just copy the gnome's panel, enhance it a bit, and use it. it just plain works.\n\nbut I love the colors. much better than gnome, imho."
    author: "superstoned"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "Why don't you guys change the KDE defaults to the way you want it. Package the config files and put it somewhere on the web. \n\nYou can even create a Konqueror profile with a different toolbar. So what's stopping you?"
    author: "ac"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-16
    body: "This is a good idea, only it have one flaw. The people that needs the simplified defaults most are the ones that are the least likely to be able to install or find such settings. Advanced users on the other hand, will always be able to expand or change their settings.\n\n"
    author: "Uno Engborg"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-16
    body: "You can take away my kde-features when you pry my bluetooth keyboard away from my cold dead hands :)\n\nBut seriously - I get windows users walking by my desk (and the other devs) and ask why their desktops cant be as beautiful as mine......they do love the icons.\n\nAnd lets face it - I could use windows to do a lot of my job, but I've got a lot of work to do, and cant be bothered fighting with my computer to get it done.  So I justy want to configure it the way I like it and go.  \n\nBy the way - I love the universal sidebar, I have a control panel (ie settings:/, in one \"folder\") My /media/cdrom for my dvd drive, my home in another folder, services in another one, and we use a web-based calendar interface - so of course I have a BIIIGGG pullout folder for that - ie autohide calendar ;) \n\nyes a weird obscure feature that some may hate, and I use _all_ the time....why - I like my desktop clean - sure I mess it up with files and crap I am working with, but I hate \"home\" icons \"my computer\" and shite...:)\n\nSo good on you KDE devs - love the work - am a huge fan - you guys rock :)"
    author: "Luke"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-15
    body: "I am really sick of all this complaining.  It seems that GNOME has launched a whole movement of complainers.  People with invented and artificial complaints that just bitch on and on about the same thing without real justification.\n\nIt seems there's nothing more annoying in the KDE world than usability and usability experts.\n\nThank you GNOME."
    author: "ac"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-16
    body: "Well, what kind of justification do you need. Most of the usual complaints you hear about KDE usability is right on spot if you compare to state of the art academic research in this field. If we can't trust people who spend years of their lifes studying such things and get university degrees on subjects like this, who should we trust. It has very little to do with Gnome, other than that Gnome in many cases can be used as a source of inspiration and a testbed for other ideas. \n\nIt would be foolish of both Gnome and KDE projects, not to take advantage of experiences from that other big DE project. Gnome is very far behind technically, KDE is far behind on usability and have very advanced technology. Just imagine what great DE we would get if the benefits of both projects somehow was combined.\n\n\n\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-17
    body: "Full Ack!!\n"
    author: "ac"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2005-02-17
    body: "If you want KDE to look like f*cking Gnome, WHY DON'T YOU JUST USE Gnome and spare us your rant! KDE looks different and this is a GOOD THING! People have a choice.\n"
    author: "ac"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2006-03-28
    body: "I think KDE is trying to look like, and copy the features of Microsoft Windows. Don't you agree? (tried KDE for the first time in two years yesterday. Found yet another similarity; KDE Web Desktop. Hooo...Did'nt Microsoft have something similar named \"Active Desktop\"?) One must be a complete fool not to notice this fact. But hey, IT IS A GOOD THING as KDE may attract more Windows users to the*nix world than for an example Gnome and Xfce.\n\nHowever, more advanced users may not wan't their desktop to behave or look like Windows, BeOS, CDE or anything else. They wan't EVOLUTION. INNOVATION. IMPROVEMENT. I think that is what Gnome stands for. They are putting together a Desktop Environment that does'nt look or behave like anything else before it, they do it very well, and I admire them for that.\n\n"
    author: "fd0"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2007-07-31
    body: "Evolution, eh? GNOME looks more like degradation... :D"
    author: "xawari"
  - subject: "Re: KDE<->GNOME Screenshot Comparison"
    date: 2006-02-05
    body: "Hi, If I may, as A GNOME user also speak here? I would agree that the default desktops that are shown here do look fresher in KDE. We all know that you can change Themes and make both Desktops similar. I think it should not be the look that distincts both desktops. I am using http://tango-project.org/ right now. What I find better in GNOME is on the first two pictures that you do not have to go to a second level in drop down menu for selecting a launcher. I have often found it complicated in KDE to find what I was looking for because of a very deep menu structure. GNOME was more similar in GNOME 1.4. What is wrong with the screenshots is that they show the directory for Desktop Preferences in GNOME on parallel to the KDE control center. I think most GNOME users do not use these, because GNOME has switched to a a new menu \"System\". I personally am more interested in the best of both desktops than in the ahte speach that is used so often. For instance I rather would use the KDE printing system and use K3b often. I would not want to miss KDE although I prefer GNOME. I think we should not be forced to decide the one or the other. We may want choice in \"best look\"  ,\"best printing system\", \"best suited environment\",... That highly depends on the personal use. Also take a look at my blog article: http://vinci.wordpress.com/2006/01/18/kde-an-gnome-like-brothers-and-sisters/"
    author: "Thilo Pfennig"
  - subject: "Where are the Slackware 10.x packages?"
    date: 2005-02-14
    body: "Have been waiting for the 3.4 beta2 packages."
    author: "Fast_Rizwaan"
  - subject: "Re: Where are the Slackware 10.x packages?"
    date: 2005-02-14
    body: "I didn't provide Slackware packages this time because my computer still in boxes after my move.\n"
    author: "JC"
  - subject: "Re: Where are the Slackware 10.x packages?"
    date: 2005-02-15
    body: "see http://dot.kde.org/1108288130/1108327746/1108458961/"
    author: "binner"
  - subject: "nothing new here, move on..."
    date: 2005-02-14
    body: "I looked at all those screenshots, and almost all of them showed features that havent changed since 3.3.2 (or at least look the same)..."
    author: "Mathias"
  - subject: "Re: nothing new here, move on..."
    date: 2005-02-14
    body: "shots.osdir.com usually mostly shows desktop menus and configuration dialogs and seldom running applications with data. In short, they show nothing which takes time to arrange.\n\nWith the Live-CD anyone can do better screenshots, so how about you? :-)"
    author: "Anonymous"
  - subject: "Re: nothing new here, move on..."
    date: 2005-02-15
    body: "i would, but i have no space where i could put them up.\nbesides, i'm too busy bitching about other peoples screenshots ;)\n"
    author: "Mathias"
  - subject: "Re: nothing new here, move on..."
    date: 2005-02-14
    body: "Kontact looks different, as does the Control Center (but I'm not sure that was shot - I only skimmed them).\n\nAnd yes, the bulk of the gallery seems to be \"this is KDE\" rather than \"here are the changes\"."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: nothing new here, move on..."
    date: 2005-02-14
    body: "And the K-Symbol is new in KDE-CVS ;-)"
    author: "Thorsten Schnebeck"
  - subject: "The Launch button of Linspire"
    date: 2005-02-14
    body: "review of linspire 5.0 (which has kde 3.3.2)\nhttp://www.osnews.com/story.php?news_id=9707\n\nand the 'Launch' button which is better looking...\nhttp://www.osnews.com/img/linspire1.jpg"
    author: "fast_rizwaan"
  - subject: "Re: The Launch button of Linspire"
    date: 2005-02-14
    body: "http://www.osnews.com/img/linspire1.jpg\n\nMaybe you can post a link to a 32x32px image, too? ;-)\n"
    author: "Xedsa"
  - subject: "Re: The Launch button of Linspire"
    date: 2005-02-15
    body: "http://www.osnews.com/img/1.png"
    author: "Anonymous"
  - subject: "More KDE 3.4 Beta 2 Screenshots"
    date: 2005-02-14
    body: "http://www.tuxmachines.org/gallery/view_album.php?set_albumName=album01&page=1 - not everything showing default setup."
    author: "Anonymous"
  - subject: "Looks quite nice..."
    date: 2005-02-15
    body: "Having looked at all the screenshots, it's quite polished...\nand it starts to look great and more and more usable.\n\nSo what's left... really? \nSome minor tweaking and fine tuning? =P\n\nCan't wait to see KDE 4... =)\n\nAs for Linspire, I don't understand why they wrote:\n\n<L>  Launch...  Instead of  <L> aunch.\n\nI think that \"Launch\" is more proper than \"Start\",\nusability wise, a bigger \"Launch\" button is easier to click,\nsince it's often more used then other icons.\n\nFred.\n\n"
    author: "fprog26"
  - subject: "Re: Looks quite nice..."
    date: 2005-02-15
    body: "> [..} usability wise, a bigger \"Launch\" button is easier to click, since it's often more used then other icons.\n\nACK!\nI just tinkered a little with it, and found that a button that is 1.5x1 of its curent size makes it clearly distinguishable, but without messing up the consistancy of the bar (IMHO the linspire button is estethically too invasive).\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Looks quite nice..."
    date: 2005-02-15
    body: "I'd love to see something like this in kde... and also the menu's as gnome has them, with text."
    author: "superstoned"
  - subject: "The icon placement probem"
    date: 2005-02-15
    body: "I understand that the icon placement problem has been solved in KDE 3.4.\n\nAnyone who can confirm?"
    author: "Kde User"
  - subject: "Re: The icon placement probem"
    date: 2005-02-15
    body: "well, I remember a CVS commit, but I don't really know when/where... you might check the bugreport\nhttp://bugs.kde.org/show_bug.cgi?id=79932 and\nhttp://bugs.kde.org/show_bug.cgi?id=94126\n\nor of course try the Klax livecd :D"
    author: "superstoned"
  - subject: "kde info center"
    date: 2005-02-19
    body: "This screenshot is quite disconcerning:\nhttp://shots.osdir.com/slideshows/slideshow.php?release=241&slide=37\n\nEver heard of not using the word you are defining in a definition? Things that may seem simple/straight forward to you, may not be to your mother. For instance \"SCSI - SCSI Information\", hmmm oooh that's what the SCSI tab does. How about something like \"SCSI - Connected peripherals information\".\n\nGet the drift?"
    author: "Flash3r"
---
<a href="http://osdir.com/">OSDir.com</a> has <a href="http://osdir.com/Article4141.phtml">published screenshots</a> of recently released <a href="http://dot.kde.org/1107971557/">KDE 3.4 Beta 2</a> utilizing the <a href="http://dot.kde.org/1108288130/">&quotKlax&quot; Live-CD</a>. Jump over to their <a href="http://shots.osdir.com/slideshows/slideshow.php?release=241&slide=2">gallery with 70+ screenshots</a>.

<!--break-->
