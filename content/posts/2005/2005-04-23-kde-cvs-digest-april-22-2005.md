---
title: "KDE CVS-Digest for April 22, 2005"
date:    2005-04-23
authors:
  - "dkite"
slug:    kde-cvs-digest-april-22-2005
comments:
  - subject: "SVN Transition/Migration Status"
    date: 2005-04-23
    body: "I know that the KDE project has been in the process of migrating its source code over to Subversion over the past two week or so, but I have heard very little about this throughout the process.\n\nWhat is the status of the migration? Is it done, or when should one expect it to be done? I am not trying to be impatient by asking this here. If the transition is still in process, is development still taking place in the CVS tree or is it in the SVN one?\n\nAre there any up-to-date documents describing how to get KDE's source code via SVN, aside from some out-of-date wiki article?"
    author: "To win the game you must kill me, John Romero"
  - subject: "Re: SVN Transition/Migration Status"
    date: 2005-04-23
    body: "CVS is used unless the whole repository is moved to Subversion. Some people work hard on making the transition a reality, but this seems to be not so easy due to the size and complexity of the KDE CVS repository. Once they are ready the repository is moved and everybody will use SVN. \nThe current SVN repository available on the KDE servers is just for testing, it will be replaced completely once the real move is done."
    author: "Andras Mantia"
  - subject: "webcam"
    date: 2005-04-23
    body: "Woohoo, Kopete soon to MSN webcam support!  That's my last reason I ever have to switch to windows."
    author: "JohnFlux"
  - subject: "Re: webcam"
    date: 2005-04-23
    body: "Webcam issues were also discussed at the recent Wine Weekly news"
    author: "Gerd"
  - subject: "Re: webcam"
    date: 2005-04-23
    body: "yeah, but running msn 6.2 under wine with webcam and the needed patches to support it is BUGGY AS HELL. and even when it will run stable it is still not as nicely integrated in kde tools like kontact/kadressbooks/etc. not to mention that run msn 6.2 under linux is not really the spirit of how the open source world should be."
    author: "Mark Hannessen"
  - subject: "(OT) amaroK..."
    date: 2005-04-23
    body: "informed me it has to delete some of my cover images cause they are older than 90 days due to Amazons licence policy. What about suggesting an agreement: amaroK can use their cover pics and insert an \"order here...\" RMB menu item?\n\nBye\n\n  Thorsten\n\n\n\n\n  "
    author: "Thorsten Schnebeck"
  - subject: "ALLOFMP3"
    date: 2005-04-23
    body: "www.allofmp3.com  - get your cover images there."
    author: "Hans"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-23
    body: "How to circumvent this?\nAs amaroK cannot permanently store the covers due to the Amazon licence, how could I take stuff into my own hands?\nWould it be possible to backup /.kde/share/apps/amarok/albumcovers and copy it over later, but will amaroK recognize the files? Btw how does this work at the moment, the link between cover and album?\nOr perhaps 'touch' some files a bit with an appropriate date?\n\nOnly solution I found so far is a script which copies the covers from the cache to the album folder, though I don't really want to clutter all the folders.\n\nhttp://www.kde-apps.org/content/show.php?content=22517"
    author: "Phase II"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-24
    body: "Well, copying to the folders is nice because then other players can find the album covers too.  And almost every player can be configured to find the album art in the music directory.\nAnd if you have it assign the art to the folder icon, it pretties it up in Konqueror too ;-)\n\nI have heard that touching the files every once in a while works too if you just want to cron that."
    author: "kundor"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-23
    body: "You basically have to do that anyway (clicking on the image requires linking to Amazon).  If you care, the agreement is <a href=\"http://www.amazon.com/gp/browse.html/ref=sc_fe_c_1_13553801_4/002-8424435-8332060?%5Fencoding=UTF8&node=3440661&no=13553801&me=A36L942TSJ2AJA\">here</a>.\n"
    author: "Scott Wheeler"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-23
    body: "The agreement we have with Amazon already requires that clicking the cover opens up amazon.com at that CD. Which we do (in most places). They still require us to delete all content downloaded from them every 90 days unfortunately.\n\nSeems a little bizarre to me; I have 100MB of content downloaded from them!"
    author: "Max Howell"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-23
    body: "amaroK 1.3 will probably offer to refresh the images, instead of deleting them.\n\nThis is OK with Amazon's license and was already planned for 1.2, but we ran out of time to implement this feature.\n"
    author: "Mark Kretschmann"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-24
    body: "Well, the simplest solution would be to use google instead of amazon. JuK does that and it works great so far. This would also allow using covers from one central directory instead of being forced to download covers for juk and amarok separately. \n\nSee also: http://bugs.kde.org/show_bug.cgi?id=99951\n\nBTW: thanks for amarok and juk :-)"
    author: "MK"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "Simple for who? Certainly not for us developers."
    author: "Max Howell"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "I recently implemented a GIS feature in KStars.  Have to say, it was pretty simple...\n"
    author: "Jason Harris"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "Yes, but it wouldn't be simpler because we have to rip out bug-fixed, proven and mature code and replace it with a new solution for the sake of not writing 5 lines of code to refresh the cache. ;)"
    author: "Max Howell"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "As I wrote the code for sending queries to google and fetching the images already exists in JuK. Compared with the refresh image option to circumvent the crazy amazon license this does not seem to be much harder. And as a further sidenote: Michael Pyne wrote that JuK does not use the amazone license because he is afraid that this is a violation of the GPL.\n\nhttp://grammarian.homelinux.net/~mpyne/weblog/2004/Nov/08\n\nI have not checked this in detail, but if this is true, then this is asking for a lot more trouble than the \"simple\" google solution. And to offer some help: I will finish my phd in exactly 6 weeks and after that I have about two weeks of free time and would be willing to help to make the google image fetcher a library and incorporate it in amarok (and by that replace the amazon solution). \n"
    author: "MK"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "BTW: There's a ruby project called musicextras that offers album cover and artist image download, among other things: \nhttp://freshmeat.net/projects/musicextras/\nhttp://kapheine.hypa.net/musicextras/\n\n"
    author: "cm"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "I wrote the code for Juk which connects to Google.  It was actually very simple - but has a down-side...it can break if Google changes the format of their results page.\n\nSince there are quite a few applications out there that depend on them not changing their format, it's not likely that they will - but it's a possibility.\n\nThe reason for using Google was that it allowed a lot more flexibility than Amazon (you can select different sizes, etc.) and it won't run into the problem with licensing - I actually emailed amazon.com, asking them some questions about their license, and it didn't seem too favorable.\n\nOne idea I was playing around with was the creation of a KIOSlave for google images...that would allow other applications to be able to pull them in... \n\nIt was too hard for me to try to code though... :)"
    author: "Nathan Toone"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "- amazon offers xml search without parsing the site: no breakage likely.\n- amazon offers various sizes as well\n- amazon really returns valid search results, whereas google could return all kinds of crap\n- covers are copyrighted... just because you can find them with google doesn't mean it's legal to store them in any way - contrary to amazon's licensing.\n\n.....\n\nmuesli"
    author: "muesli"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "Yes, the API of amazon is safer, but as Nathan pointed out it is unlikely that the google changes the output format in the near future (and I never had problems so far). The results are from google are not always perfect, but I have the same problem with amazon as well.\n\nAbout the various sizes: I never ever got an image that would be large enough to print it as a cover. Amazon offers 240x240 and 100x100 but surely not 640x640 (in contrast to the google image search). The copyright argument sounds a bit strange to me: of course all amazon data is copyrighted as well. Furthermore, amarok has a lyrics fetcher which also downloads copyrighted material, so I don't see what is so special about a google cover fetcher.\n\nOn the downside we have:\n* locked into one company \n* license terms, such as:\n- \"You acknowledge that we (and our corporate affiliates, such as Alexa) may crawl or otherwise monitor your Application\"\n- \"1) link each of the Amazon Properties that is displayed on or within your Application to (a) a product detail page of the Amazon Website, or [...]\". BTW I doubt that amarok is in compliance with this requirement because not _each_ amazon property is linked.\n* the license furthermore forces to update all data at least every 90 days\n* it does not allow sharing the data with other applications\n"
    author: "MK"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-25
    body: "no offense there, BUT: i don't even want the user to print these covers. i want them to show up in amarok and that's all. printing sounds like your burning your CDs, but i'm sure that's just for backup reasons ;-)\n\nthe copyright issue is a totally valid issue and for the same reason amarok most likely won't have an integrated lyrics fetcher for 1.3. if so it will prolly become a plugin/script.\n\nbtw, we talked to the amazon guys and they seem fine with what we do with their covers.\n\n\"what's so special about a google cover fetcher?\" - nothing and that's exactly the problem. in all cases i \"monitored\" it sucked compared with amazon's results. again, no offense there: it's just what i and 99% of our users noticed.\n\n...muesli"
    author: "muesli"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-26
    body: "Click on nothing, then click on the icon."
    author: "Roberto Alsina"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-26
    body: "sorry? don't understand what you're saying... :-)\n\n...muesli"
    author: "muesli"
  - subject: "Re: (OT) amaroK..."
    date: 2005-04-27
    body: "I replied in the wrong thread :-)\n\nIt was meant for the eternal \"how do I select a single item in single-click mode?\" thread."
    author: "Roberto Alsina"
  - subject: "Re: (OT) amaroK..."
    date: 2007-01-12
    body: "> \"what's so special about a google cover fetcher?\" - nothing\n\nThat's true ONLY if you're US (or few others major courtries) based. \nAmazon doesn't have covers from 100 other countries (Europe, Asia etc.)\nwhile google can find them. "
    author: "mg"
  - subject: "Re: (OT) amaroK..."
    date: 2007-06-29
    body: "That's right: I'm really annoyed only albums in Spanish, English and German are covered: that's about 10% or less of my entire collection. Google, on the other hand, has most artists, also of minor countries/languages!"
    author: "rRat\u00f3n"
  - subject: "Bluetooth IRMC Konnector"
    date: 2005-04-23
    body: "Thanks for the IRMC Konnector ! i am looking forward to Sync my Motorola t630 with my kdeadressebook and to put my Tasks in Kontact !! nice work Simone !\n:D\n\n"
    author: "ch"
  - subject: "Thanks Derek, because"
    date: 2005-04-23
    body: "I just wanted to say I really appreciate that the all in one page link is back again. Well, cheers for KDE guys listening to requests of stupid users going on their nerves!"
    author: "Jakob Petsovits"
  - subject: "KDE-docs"
    date: 2005-04-23
    body: "http://www.kde-docs.org/\n\nThe missing part of the puzzle. Just great, this is what we needed."
    author: "Gerd"
  - subject: "Re: KDE-docs"
    date: 2005-04-23
    body: "Wow!"
    author: "Andr\u00e9"
  - subject: "Re: KDE-docs"
    date: 2005-04-23
    body: "Mmm. At first glance I thought they used the KDE-Look/Apps-Engine\nfor documentation. I thought hey what a GREAT idea - never thought\nof sth. like that before. But it's only about documents like \nKOffice templates etc. Not that it isnt useful but the idea of being\nable to post and vote on tutorials, HOWTOs, manuals for KDE-Apps would\nbe a great addition to a Wiki. Guess not everyone likes the fact\nthat s/he is using precious time to enter text into a Wiki system\nwhich is in constant fluctuation. I'm not saying that a Wiki is \na bad idea - but a kde-manuals.org site would certainly be a\ngreat addition.\nNevertheless the kde-docs site shows that the KDE-Look/Apps-Engine is\nreally powerful. I like it more than any other contribution site\non the web. Especially cool is that it isnt getting in your way\nwhen you want to vote, comment, up-/download things, etc.\n\n\n"
    author: "Martin"
  - subject: "Re: KDE-docs"
    date: 2005-04-23
    body: "Is the engine open sourced?"
    author: "Andr\u00e9"
  - subject: "Re: KDE-docs"
    date: 2005-04-27
    body: "No."
    author: "Anonymous"
  - subject: "Re: KDE-docs"
    date: 2005-04-23
    body: "The section for KDE's death penalty game is a little bizarre, though. I like that! Really!  :-}"
    author: "Foo"
  - subject: "Kopete icons"
    date: 2005-04-24
    body: "http://cvs-digest.org/?diff&file=kdenetwork/kopete/icons/cr48-action-metacontact_online.png,v&version=1.2\n\nOMG what happened??\n"
    author: "Anonymous"
  - subject: "Re: Kopete icons"
    date: 2005-04-24
    body: "Much, much nicer than the (imho) annoying smily faces. Well done."
    author: "Eike Hein"
  - subject: "Re: Kopete icons"
    date: 2005-04-24
    body: "Good icon, but doesn't go well with Yahoo users. Yahoo Messenger uses Smileys :) :| to show online and offile. Hope this is themeable."
    author: "Fast_Rizwaan"
  - subject: "Re: Kopete icons"
    date: 2005-04-25
    body: "I love them!"
    author: "Mark Hannessen"
  - subject: "Re: Kopete icons"
    date: 2005-04-25
    body: "I prefer them myself. The smiley faces were nice, but they conflicted with emoticons. And these new ones are smart :)"
    author: "Max Howell"
  - subject: "is the porting to Qt4 started already?"
    date: 2005-04-24
    body: "I've done some testing of Qt4 beta2 and results are really awesome. Can't wait for results of porting of KDE to show up."
    author: "ph"
  - subject: "Re: is the porting to Qt4 started already?"
    date: 2005-04-24
    body: "No. Not before the switch to Subversion."
    author: "Anonymous"
  - subject: "Bug 103891:  $0.02"
    date: 2005-04-26
    body: "Finding and using the URW clones *is* part of the optimum behavior for Helvetica an Courier, but it is not all of it.  The 14 base post script fonts need to have special treatment, to better emulate the rendering in Adobe Acrobat Reader\n\nFirst a question: Does KPDF use TrueType fonts?\n\nIAC (presuming that it does or will do TrueType), for Helvetica, KPDF should first search for a scalable Arial, then a scalable Helvetica.  Only if it doesn't find one of them should it use the URW clone.  For Courier, it should search forBug 103891:  a scalable Courier.  It should find IBM Courier and/or BitStream Courier since they come with X, but if for some reason it doesn't it can try Courier New and only if it doesn't fine one of these should it use the URW clone.  For Times, it should first search for a scalable Times New Roman, then a scalable Times.  Only if it doesn't find one of them should it use the URW clones.\n\nSimilar action should be taken for Symbol and Zapf Dingbats.\n\nSince this might is some circumstances cause problems for a user for some obscure reason like missing glyphs in some of the user's installed fonts, these search lists should be in a kpdf.rc file.  I don't think that they need a GUI configuration dialog.\n\n-- \nJRT"
    author: "James Richard Tyrer"
---
In <a href="http://cvs-digest.org/index.php?newissue=apr222005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/?newissue=apr222005&all">all in one page</a>):  <a href="http://accessibility.kde.org/developer/kttsd/index.php">KTTS</a> can use new Hungarian mbrola voice.
<a href="http://www.koffice.org/kexi">Kexi</a> adds a new script editor and classes in <a href="http://www.python.org/">Python</a> bindings.
<a href="http://kopete.kde.org">Kopete</a> sees start of MSN webcam support.
Continued progress in Kicker, <a href="http://www.konqueror.org/features/browser.php">khtml</a>, <a href="http://kwifimanager.sourceforge.net/">KWifiManager</a> and many other programmes.



<!--break-->
