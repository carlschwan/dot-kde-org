---
title: "K3b 0.12 New Features Guide"
date:    2005-07-28
authors:
  - "strueg"
slug:    k3b-012-new-features-guide
comments:
  - subject: "Simple single-sessions?"
    date: 2005-07-28
    body: "\"The Data project has not been improved as much as the audio project but there are some small things that are worth mentioning. \nThe Automatic Multisession Handling\"\n\nAha. Well, I don't need multisessions. I need to handle singlesessions cleanly. But that isn't possible!\n\nK3b stores file lists file for file instead of those directories that have been added manually to the data project! That's nonsense.\n\nSecond, if I want to backup my home dir, that isn't possible because of two reasons: I cannot easily add hidden files and it takes hours to process 180.000 files or so...\n\nThird and last, the file comparison after writing the disc does not work with umlauts etc.\n\nJust a few hints from someone who does not only backup two files on a DVD at a time :-)))"
    author: "Mark"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-28
    body: "And why don't you think clean session handling is possible? you still have the non-auto modes, don't you?\n\nAnd what about these file lists. I can't follow you.\n\nof course you can easily add hidden files. where is the problem\nand 180000 files are a lot. What do you expect? K3b has to look at every single file. that takes some time.\n\nfor a backup with k3b creating a tar archive is way better than burning the files directly.\n\nAnd I know the thing with the umlauts."
    author: "Sebastian Trueg"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-28
    body: "There's a thing with Umlauts? No probs here (suse 9.3), even kanji and arabic works. rr of course, i have no use for juliet."
    author: "Andy"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-28
    body: "It only happens if you are running an utf-8 environment and it only makes verification fail. But for the verification issue I know a fix. Well, not really a fix but a different verification approach which I will implement soon."
    author: "Sebastian Trueg"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-28
    body: "To be honest, for a regular backup, it's much better to learn how to use the various command-line utilities for cd burning, and then script your way through. \n\nStick to the CLI when you need non-interactive, repetitive jobs; that's what computers do best :)"
    author: "Giacomo"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-28
    body: "there are also several graphical backup programs..."
    author: "superstoned"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-31
    body: "I think Mark means this:\n\nIf I add a directory to a data cd project k3b looks itself into this directory and adds every single file to the project. It stores every information about the added directory, escpecially the content, at the time I choose the directory. That's really nonsense.\n\nk3b should instead take the user much more verbatim: add this directory should mean: add this directory. Exclude that file or directory should mean: exclude this file. Or directory.\n\nSo the next time I want to make a backup of my data I just have to load the k3b project and burn my new backup disc. Then I don't miss any files added since I created the project. Or missing files don't prevent me from successfully burn my new disc.\n\nI remember that using another burning application it does so, but I don't know which application it was. Maybe 'arson'?\n\nTo look in every single directory makes only sense if I want to compute the needed disc space. I see no single reason to save the information about directory contents into the project. Only disadvantages.\n\nAnsonsten liebe Gr\u00fc\u00dfe, k3b ist eine feine Sache und w\u00e4re der Br\u00fcller ohne diesen schweren und nicht nachvollziehbaren Schnitzer.\n\nTim\n"
    author: "Tim Gollnik"
  - subject: "Re: Simple single-sessions?"
    date: 2005-07-31
    body: "This is not nonsense at all! What makes you think K3b's data project is a backup project??? It is not!\nIt will NEVER act the way you say, becasue it makes no sense for a data project.\nI have plans for a backup project which act this way but it won't be fast since users want size information for that, too.\nYou cannot have both: speed and full size information.\nA data project is filled with files and folders which and won't change. Most users would be completely confused if the data project would act as you propose."
    author: "Sebastian Trueg"
  - subject: "Re: Simple single-sessions?"
    date: 2005-08-02
    body: "When backuping your home dir, tar archives play much better than adding the files directly to the CD. However, sometimes you want to burn a bunch of files and have online access to them. For example, a clippart, your mp3 collection. And, of course, burning to a DVD, what opens up the possibility of up to 4.4 GB (DL media is yet too expensive) of data and thousands of small files.\n\nSo, I do agree with Mark about having a option to store directories instead of files in the project. That would make burning my DVDs a much smoother experience. May be an option, in the cd or dvd project, to automatically recursivelly add files within a directory when a directory is added? The default behaviour would be enabled (mimicking the current behaviour), so that only users that really needs to create high volume projects, like my MP3 archive or Mark's backup, would need to disable it (using a checkbox in the project property, for example)."
    author: "Marco Aur\u00e9lio"
  - subject: "version 1.0?"
    date: 2005-07-28
    body: "Are there any plans to stop adding features, fix up remaining bugs and to release something stamped with 1.0 on it?\n\nK3B is a great piece of software. I'm just wondering what the plans are here...\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: version 1.0?"
    date: 2005-07-28
    body: "well, that's not so easy. k3b still has some issues that prevents it from being 1.0.\nI have plans for a version 1.0 but i regularly postpone them..."
    author: "Sebastian Trueg"
  - subject: "Re: version 1.0?"
    date: 2005-07-28
    body: "What are the \"issues\" that prevent it from being 1.0?  As far as I can tell it is easily the best CD burning application out there.\n\nBobby"
    author: "brockers"
  - subject: "k3b is GREAT"
    date: 2005-07-28
    body: "keep up the good work... k3b is great!"
    author: "Andre"
  - subject: "Re: k3b is GREAT"
    date: 2005-07-28
    body: "Have to 2nd that motion!"
    author: "am"
  - subject: "Re: k3b is GREAT"
    date: 2005-07-28
    body: "Yeah... usually all that developers get for their hard work are complaints.\nSo thank you very much!!! "
    author: "Anonymous"
  - subject: "Re: k3b is GREAT"
    date: 2005-07-28
    body: "Ah, that feels good. Thanks a lot. :)"
    author: "Sebastian Trueg"
  - subject: "Re: k3b is GREAT"
    date: 2005-07-29
    body: "Yup, it's one of those apps that I bring up once in a while (I burn stuff quite rarely) and never have any problems with. It's always very simple to get what I want done and I never have to sit and wonder how something is done. Easily one of the best Linux apps.\n"
    author: "Chakie"
  - subject: "Re: k3b is GREAT"
    date: 2005-07-29
    body: "It really rocks, looks great, it does all I need, in a nice way!\n\nA pity it's so hard to find women with the same qualities... ;-)"
    author: "ac"
  - subject: "Re: k3b is GREAT"
    date: 2005-07-29
    body: "oh, yes, it's a willing piece of software AND it's easy to handle :-)) Thanks for writing it! "
    author: "MH"
  - subject: "DVD structure problems??"
    date: 2005-07-28
    body: "I have a weird problem, not sure what it is.\nI have some movies I burned on DVD5 some months back. (I have no way to know the version of k3b I used to burn them with)\nI can put them in any home DVD player (Apex) and watch them normally.\nRecently I took the same ISO image and burned a second copy of them for my son.\nI always test after a burn.  The movies play fine on my PC, but I put them in my home DVD player and they will NOT play, the player says \"NO DISC\"..\nI put the disc in an old RCA (1998) DVD player and it played fine.\nI took it next door and it played fine in my neighbors machine.\n\nSo, I took a disc at random and confirmed that it does play in the Apex.\nI used K3b to copy the DVD to a new disc.  The copy will NOT play, the original will.\nI used k3b to burn the original.  And I am burning from the same batch of blanks.\nHere's the odd thing though, if I burn the movie to a DVD-RW it will play fine in the Apex, it's only DVD-R's that will no longer play in the Apex\n\nConsidering that I have confirmed that discs I burned a few months ago play just fine and discs that I burn now will not play, I can only assume that k3b is not doing something the right way..  I've never had any problems with the Apex, it's about 18 months old and has always played anything I stuck in it.  It still does, as long as it's stuff I burned few months ago..\n\nI am using Suse 9.3 Pro, kernel \"2.6.11.4-20a-default\", Sony DRU-540a, \nk3b -version says\nQt: 3.3.4\nKDE: 3.4.1 Level \"a\"\nK3b: 0.12.2\n\n\nThanks!"
    author: "Dave"
  - subject: "Re: DVD structure problems??"
    date: 2005-07-28
    body: "Some of the more recent DVD players have issues with DVD-Rs.  The newer, expensive ones tend to be really picky about the disks (for me they play part way then the image starts getting corrupted and sometimes crashes the player)."
    author: "Corbin"
  - subject: "Little annoyance"
    date: 2005-07-28
    body: "The only problem with the last k3b, was that I and my wife searched for several minutes where the big BURN button was.\nFinally we gave up, as it was obvious it was not there anymore.\nI see it labelled \"burn\" in the screenshots here, but IIRC it was not labelled on my wife's desktop.\nWell, we finally found it, but it was really annoying.\n\nOokaze"
    author: "Ookaze"
  - subject: "Re: Little annoyance"
    date: 2005-07-28
    body: "every toolbar in KDE can have textlabels, if you want them... rmb on the toolbar and you'll see :D\n\nand this is an universal KDE tip: something annoys you or you think it could be better? try a right mouse click on it. often you'll see config options to solve your problem!"
    author: "superstoned"
  - subject: "Re: Little annoyance"
    date: 2005-07-28
    body: "And what do I see? Did you actually try it?"
    author: "testerus"
  - subject: "Re: Little annoyance"
    date: 2005-07-28
    body: "http://img190.imageshack.us/my.php?image=k3b6tt.jpg"
    author: "superstoned"
  - subject: "Re: Little annoyance"
    date: 2005-07-28
    body: "not this one. it's not a standard kde toolbar but something I hacked together since the toolbar did not fit my needs. But I changed the burn button to always include the text as before."
    author: "Sebastian Trueg"
  - subject: "Re: Little annoyance"
    date: 2005-07-29
    body: "That is really a good thing.\nWhat annoyed me really, was that in old versions, the burn button was well separated from others, while now, it is not remarkable among the other buttons. Putting text beside it is enough to make it remarkable again.\nThe only thing that will remain to fix is the french translation, which now say \"burn\" instead of the french word \"graver\".\nAnyway I can do it myself, provided the .pot is up to date.\nMy wife does not understand english, that's why translations are very important.\nAnyway, many thanks for K3B.\nThat's the first burning program that my wife can use and that does not scare her.\nShe is very confortable with it (sometimes she asks me questions, like for the burn button episode or translations lacking), it's amazing. She could not even use Nero !"
    author: "Ookaze"
  - subject: "k3b and Mandrake"
    date: 2005-07-28
    body: "slightly off-topic.  \nmultisession CD burning with k3b has been a problem\nwith mandrake 10.1 community onwards.  in 10.1\nit was a bad cdrecord binary.  I reverted to old\nmdk 10.0 cdrecord and the problem was gone. In \nmandriva LE again multisession mas buggy this \ntime their k3b binary was the culprit.  I \nreverted to k3b of mdk 10.1 and again it was working\n\nAsk"
    author: "Ask"
  - subject: "Can't add file/folders in k3b project"
    date: 2005-07-29
    body: "Why is that k3b is only for the drag and drop people? The file-browser in k3b should show 'Add to project' or 'Add to compilation' in the Right-Click menu.\n\nIt's inconvenient to drag and drop, It would be cool to have a \"RMB menu\" to add files/folder in the file browser in k3b. thanks sebastian for your creation - k3b :)."
    author: "fast_rizwaan"
  - subject: "Re: Can't add file/folders in k3b project"
    date: 2005-07-29
    body: "there was a possibility in 3b but since kde 3.anything it is broken since kdelibs changed the way the ontext menu in the file browser is created. Now I cannot easily extend it. :(\nSo far I do not know a good solution..."
    author: "Sebastian Trueg"
  - subject: "mkisofs != growisofs?"
    date: 2005-11-30
    body: "I'm running Debian Linux 2.6.8, K3B 0.12.2, growisofs 5.21, mkisofs 2.1. When\ncreating data DVDs in K3B, I started setting the option in K3B to verify\nwritten data. This is failing. I checked the file on the DVD against the original file on the hard disk with md5sum and the hashes are different.\n\nTo see where the difference arises, I created an ISO image of the file\nusing mkisofs (-J -R), on the command line - and still using K3B, burned that *image* to a DVD. On that DVD, the MD5 hash was the same as the original. (Of course,growisofs is used to burn the ISO image, but the mkisofs portion is\nbypassed?).\n\nIs this a known issue? Is there some compatibility parameter to pass into growisofs?   "
    author: "Gort"
---
Among users of the <a href="http://k3b.org">K3b CD and DVD burning application</a> there has been quite some confusion with the new audio project so I wrote a little overview of the new features and changes in version 0.12. Check out the <a href="http://www.k3b.org/support/k3b-0.12-new-features">K3b 0.12 New Feature Guide</a> to get an idea of how much more there is under the hood in the new version.

<!--break-->
