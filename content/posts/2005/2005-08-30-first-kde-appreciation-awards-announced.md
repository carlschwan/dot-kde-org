---
title: "First KDE Appreciation Awards Announced"
date:    2005-08-30
authors:
  - "dmolkentin"
slug:    first-kde-appreciation-awards-announced
comments:
  - subject: "What have you guys been doing to Aaron??!!"
    date: 2005-08-30
    body: "What have you guys been doing to Aaron??!!\n\nHe looks like a dear caught in headlights!"
    author: "manyoso"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-30
    body: "LOL! I swear I did not read your post, but only the article.\nAnd while looking at the photo I thought _exactly_ the same. \nWhat the heck are they putting in the water down in Spain??\nI was about to read the article to its end and then posting sth. about this\nbut you beat me to it!\nAnd BTW: A dear caught in headlights is one of the cutest typo's I've\nseen in a while. Isnt he both in a way? ;-)\n"
    author: "Martin"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-30
    body: "LOL!!!! I believe the bird that came out the camera scared him to death... "
    author: "kmare"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-30
    body: "With his recent introduction into KDE E.V. and Trolltech maybe he is just being fully assimilated."
    author: "Wade"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-31
    body: "Its the lutefisk.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-31
    body: "He has been member of KDE e.V. for a while already."
    author: "Anonymous"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-31
    body: "He just got inducted into the board two days ago."
    author: "kundor"
  - subject: "Re: What have you guys been doing to Aaron??!!"
    date: 2005-08-31
    body: "LOL! Sure it's due to the saturnday night party and/or the girls from Malaga. ;)"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Can I have the oKular source please!"
    date: 2005-08-30
    body: "oKular project was supposed to be finished by 25 august, I hope it is finished by now, I can't find any link to download oKular. Is it compilable in KDE 3.5svn?"
    author: "fast_rizwaan"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-30
    body: "Since a long time ago\n\nsvn co svn://anonsvn.kde.org/home/kde/trunk/playground/graphics/oKular\n\nKeep in mind you need ghostscript compiled as a library, not many, if any distros ship it, so you could need to download gs and make so, make soinstall\n\nAlso the check for chmlib is still not present so install it.\n\nBTW Piotr (Google Summer of Coder) will be grateful to hear from your experiences with his code."
    author: "Albert Astals Cid"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-31
    body: "Good morning\n\nMay I ask you if oKular is supposed to be a replacement (in the long turn) for Kpdf (kghostview and kviewshell)?\n\nDon't unterstand me wrong. I've nothing against kpdf, it's excellent but I heard that oKular is based on kpdf and it would really make sence to have one single document view program.\n\nthx a lot\nMario"
    author: "Mario Fux"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-31
    body: "Yes it is"
    author: "Albert Astals Cid"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-31
    body: "I think he got it, at least after the second answer :-)"
    author: "ac"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-31
    body: "Yes, I do ;-).\n\nBut I think the problem was because of temporary down time of the dot."
    author: "Mario Fux"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-31
    body: "I had some problems with the browser telling me it could not process the answer :-D\nSorry"
    author: "Albert Astals Cid"
  - subject: "Re: Can I have the oKular source please!"
    date: 2005-08-31
    body: "Will be intresting to see whether a OO.org plugin will get developed too."
    author: "martin"
  - subject: "amarok?"
    date: 2005-08-31
    body: "if I had been the jury, amarok would have made it. Its really come from a crash-prone piece of pushbuttons to a great audioplayer! thanks for that! (kpdf is cool, too :)"
    author: "me"
  - subject: "Re: amarok?"
    date: 2005-08-31
    body: "If I had been the jury, made exception for kpdf (for obvious reasons) I'd have prized amarok too, since I use it many hours a day and it keeps surprising me.\n\nOther than that I think there are many apps who can share the prize, such as kolourpaint or krita on gfx, konqueror for the stability it reached in 3.x series, kate for having become a perfect lovely editor, kicker for getting better every other day, and other external apps such as kmymoney, kat, the many apps I use it everyday and.. you name it.\n\nFinally I think that a prize for \"the best kde technology of the year\" sould be given. ksvg2 has lots of efforts behind it, ghns has been adopted in many places as long as khtml (proven good by many 3rd party work) and khtml_part (getting integrated for whatever reason in nearly every mayor app). Next year choosing among those will be easier since we'll found them in kdelibs/frameworks or kdelibs-frameworks or wathever the clever kde4 guys will call it.\n\nIf I'd been in the jury next year, I'd assign the apps prize to the one(s) that most improved in 2005/2k6 and there are many apps ready to be taken, broken, rebuilt and brought to the 21th century. (hey, wait a moment: next year I'll be in the jury! ;D) Anyone ready?\n"
    author: "Enrico Ros"
  - subject: "Re: amarok?"
    date: 2005-09-02
    body: "Is Kolourpaint still beeing developed? Development seems to be stalled."
    author: "martin"
  - subject: "Re: amarok?"
    date: 2006-09-16
    body: "Don't forget that Amarok and kpdf could get a common future:\n\nPodcasts also support PDF documents..."
    author: "furangu"
  - subject: "MS Info Path"
    date: 2005-08-31
    body: "MS Info Path, what open source program does compete with it?\n\nIt's a pretty kool tool, a form assistent, but it is only part of expensive business versions of Office 2003. "
    author: "Matze"
  - subject: "Re: MS Info Path"
    date: 2005-09-01
    body: "You mean MS' try of an Adobe Acrobat killer? Do you seriously expect developers her to buy a license based on such hype to check out if they \"missed something to copy\"?\n\nIf not please try to reword your post describing what features/application you see missing based on what use cases, that all without mentioning exiting proprietary software one \"have to look at\", and you will get an answer what of your suggested pieces are existing already and what would be further needed to fulfil your suggestion as a whole."
    author: "ac"
---
<a href="http://conference2005.kde.org/">This year's aKademy</a> saw a whole new innovation: The KDE Appreciation Awards, also known as the "aKademy Awards". Their purpose is to recognize outstanding contribution to the KDE community. The awards are for best application, best contribution to KDE and the Jury's Choice Award.  The jury consisted of the well-known KDE hackers Aaron Seigo, Brad Hards, David Faure and Matthias Ettrich. If you want to know who the winners are, read on! 












<!--break-->
<div style="float: right; padding: 1ex; border: solid thin grey; width: 300px">
<a href="http://static.kdenews.org/danimo/akademyawards05.jpg"><img width="300" height="225" src="http://static.kdenews.org/danimo/akademyawards05-wee.jpg" /></a><br />
<a href="http://static.kdenews.org/danimo/akademyawards05.png">Aaron Presents the Award to Albert</a>
</div>
<p>The award for the best application or application improvement went to Albert Astals Cid and Enrico Ros for their amazing work <a href="http://kpdf.kde.org">KPDF</a>. In the last year KPDF has gone from a poor comparison to KGhostScript to the best Free Software PDF reader available.  As part of the <a href="http://poppler.freedesktop.org/">Poppler</a> PDF library and the new <a href="http://developer.kde.org/summerofcode/okular.html">oKular</a> universal document viewer they are set to remain the leader.</p>

<p>The prize for the best contribution was awarded to <a href="http://www.kde.nl/people/lauri.html">Lauri Watts</a>, the KDE documentation coordinator, for her outstanding and long-running work which has resulted in KDE having the largest collection of DocBook in the world.</p>

<p>Finally, the "Jury's Choice Award" went to release coordinator <a href="http://people.kde.org/stephan.html">Stephan Kulow</a> and KDM-Maintainer Oswald Buddenhagen for the effort they have put into the <a href="http://dot.kde.org/1115285369/">Subversion migration</a>. KDE development has been faster and easier thanks to the long hours they put into the conversion.</p>

<p>The winners will receive a framed picture of Konqi signed by all attending KDE developers.</p>

<p>Next year, the jury panel will consist of this year's winners. At aKademy 2006 it will be up to them to select the most outstanding work from within the KDE community. Credits go to our very own <a href="http://kde-artists.org/main/content/view/42/29/">David Vignoni</a> for designing the awards.</p>











