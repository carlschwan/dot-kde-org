---
title: "Qt 4.1 Now Available"
date:    2005-12-20
authors:
  - "jriddell"
slug:    qt-41-now-available
comments:
  - subject: "Hope KDE 4 will require Qt 4.1"
    date: 2005-12-20
    body: "Given the SVG and PDF capabilities of Qt 4.1 and all other improvements I hope that KDE 4 will require Qt 4.1 as minimum version. Will that happen?"
    author: "Shyru"
  - subject: "Re: Hope KDE 4 will require Qt 4.1"
    date: 2005-12-20
    body: "most likely, yes.\n\n:D"
    author: "superstoned"
  - subject: "Re: Hope KDE 4 will require Qt 4.1"
    date: 2005-12-21
    body: "Yes, KDE4 will require 4.1, parts of trunk already do :)\n\n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: Hope KDE 4 will require Qt 4.1"
    date: 2005-12-22
    body: "I hope so because 4.0 is buggy as hell!  I hope they plan for 4.1.1 because even 4.1 isn't ready.\n"
    author: "AC"
  - subject: "Re: Hope KDE 4 will require Qt 4.1"
    date: 2005-12-22
    body: "the difference between a troll and a user is that the user actually gives details when he says that something is buggy, crappy or whatever.\ntry again."
    author: "ac"
  - subject: "KSVG"
    date: 2005-12-20
    body: "http://svg.kde.org/\nDid ksvg had any influence on the svg implementation of Qt? Do we now have two different svg renderers? Ksvg supports javascript, afaik. Which one will be used? SVG wallpapers and svg icons are rendered with ksvg atm.\n"
    author: "Thomas"
  - subject: "Re: KSVG"
    date: 2005-12-20
    body: "They suplement each other. qsvg only implements the tiny svg specification. So there is no duplication of efforts. ksvg tries to implement full svg support."
    author: "Michael"
  - subject: "Re: KSVG"
    date: 2005-12-20
    body: "Furthermore, KSvg supports DOM which the Qt-svn does not (not needed for tinysvg-usecases)"
    author: "Carsten Niehaus"
  - subject: "Re: KSVG"
    date: 2005-12-20
    body: "ksvg provides manipulation of svg @ runtime with DOM , qt svg does not"
    author: "ch"
  - subject: "Re: KSVG"
    date: 2005-12-20
    body: "So if I get it right, KDE4 will use QtSVG for things like icons rendering and it will use KSVG for rendering svg embeded in webpages and more complex tasks like that. By the way is KSVG2 able to render that kind of stuff http://www.croczilla.com/svg/samples/svgtetris ?"
    author: "Patcito"
  - subject: "Re: KSVG"
    date: 2005-12-21
    body: "http://www.croczilla.com/svg/samples/svgtetris\n\nclick on svg , konqui goes boom.\n\nand no, i did not report it to bugs.kde"
    author: "eol"
  - subject: "Re: KSVG"
    date: 2005-12-22
    body: "that's because you're not using the yet to be released KSVG2. I was asking to people in the know."
    author: "Patcito"
  - subject: "Re: KSVG"
    date: 2005-12-21
    body: "If QT does not support the DOM interface then they can't be considered 1.2 compliant.  (Since 1.2, is in last call it seems no one can be 1.2 compliant right now, but I degress...  :) )  SVGT does have a microdom that can be used for dynamic updates and scripting.  Any word on when this will be done?\n\nSo there will be overlap if QT wants to be 1.2 compliant."
    author: "AngryMike"
  - subject: "Xara Xtreme (http://www.xaraxtreme.org)"
    date: 2005-12-20
    body: "I've got a feeling that once Xara Xtreme (http://www.xaraxtreme.org/) is released under the GPL, it will be way faster (http://www.xaraxtreme.org/about/#performance) than any existing free software SVG implementation such as Inkscape, QT SVG, KSVG, Cairo, etc. I would suggest QT or KDE devs working on SVG to try to incorporate as much of Xara Xtreme's code as possible, because they're likely to be the best in the field.\n\nHere's the original announcement giving the reasons why they are releasing their  product as open source:\nhttp://www.xaraxtreme.org/news/11-10-05.html"
    author: "Vlad C."
  - subject: "Re: Xara Xtreme (http://www.xaraxtreme.org)"
    date: 2005-12-20
    body: "> it will be way faster\n\nIs that really what is most important?\n\nIMHO, it is more important that SVGs be rendered correctly.  With that as the goal, then Batik (Apache) would be the best choice.  Yes, I know it is Java, but isn't GCJ 4.1 supposed to be able to compile Java classes to be used with C++?\n\nIAC, what we could really use is Gaussian Blur filters -- icons look stupid without drop shadows."
    author: "James Richard Tyrer"
  - subject: "Great!"
    date: 2005-12-20
    body: "I've started using qt4 this year, and I belive it's the best open-source toolkit out there, I can't even imagine why some people still use things as wxwindows and gtk.. well maybe because they don't wnat to create GPL apps, shame on them."
    author: "Iuri Fiedoruk"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "because wxWidgets (as it's called now) is truly cross-platform, making use of true native widgets, as opposed to just painting fake ones that look like the real ones.  If your app is supposed to run on other platforms than linux, then you'll see a big difference in performance."
    author: "Ben"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "we're talking about qt4, so that's bollocks.\n\ncheers,\nmuesli"
    author: "muesli"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "I am a big QT fan, but I think you hit on something here,as far as QT4 needing native widget support. Correct me if I am wrong but doesn't QT4 now do all of its own rendering.  If so isn't this at least responsible for part of the slowness of it. Java did this with Swing which made it very slow and even more unusable.\n\nIS there a choise in rendering engines for QT4 - native widgets or QT4 ?  \n\nAnd if QT offers its own rendering engine why not use openGL and take advantage of the GPU found in almost every mother board. This would allow for really beautiful shading and lighting with virtually no rendering penalities.    \n\n"
    author: "2rpi"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "It's Qt not QT! QT is a dirty codec used by a company that likes to calls it self an apple. Instead please use http://www.theora.org :)"
    author: "Patcito"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "<I>Java did this with Swing which made it very slow and even more unusable.</I><P>It made Java slow and unusable because Java is interpreted. (Yeah, I know, JIT, fast as native performance, you have benchmarks, yada yada. Bollocks.) C++ is natively compiled, so there's no reason at all for the speed of Qt rendering to be any different from native - provided your C++ compiler is up to scratch, that is."
    author: "mikeyd"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "if you created java program who's slow is your fault, bad programmer\n\nbad programmer often do long action in an event... that block the gui, \nthe solution is thread and the gui will be responsive\n\nany good programmer know that"
    author: "Marc Collin"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "if your program slow is now, in future faster will it be.  computer faster get all the time, any bad programmer will know."
    author: "ac"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "that could almost be a poem ;)"
    author: "Leo"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "I think it's not a poem... it's Master Yoda ;-)"
    author: "Bardok"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "Every java program I have used has been horribly slow. Every one. The gui isn't blocked, it's still there if you obscure and reveal it, it's just taking ages to respond to anything because it's flipping slow."
    author: "mikeyd"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "In my experience, even \"hello world\" in Java takes a few seconds to start up, even when compiled to native code with GCJ."
    author: "rqosa"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "> Correct me if I am wrong but doesn't QT4 now do all of its own rendering?\n\nQt has always done its own rendering.\n\n> If so isn't this at least responsible for part of the slowness of it?\n\nHuh?  Qt is hardly slow at drawing.  Perhaps you're using a poorly programmed theme?\n\n> Is there a choise in rendering engines for QT4 - native widgets or QT4\n\nAs muesli just pointed out, Qt uses native rendering on every platform.  On Linux, Qt itself is \"native\" :)"
    author: "Michael Pyne"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "Actually, from what I've heard, the native-looking Qt themes defer to the native implementation where possible, and only render widgets themselves when necessary due to their configuration. This was necessary to get a decent look and feel for Mac OS X (added in 3.2) and in Windows XP.\n\nQt is certainly not slow, and in fact, neither is Swing (it's faster than SWT on a lot of benchmarks). The problem with Java apps is the high memory usage and slow start-up time caused by the JVM loading. Assuming it's coded correctly (a big assumption) Java applications are very responsive. Try the ThinkFree Office demo, of the DbVisualizer demo, for examples of how good Java can be.\n\nQt also supports rendering using OpenGL as of version 4: this is the \"Arthur\" rendering engine. If offers all you want, people just need to write a theme to take advantage of it."
    author: "Bryan Feeney"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "Just tried ThinkFree office.  It's very snappy indeed.  So the question is, is every other Java developer retarded?  If it is possible to make snappy user interfaces, what's up with every other high-profile java app on the planet?  Eclipse, Azureus, both laggy on my system.  So are they programmed by idiots or what's the difference here?"
    author: "Leo"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "Eclipse is laggy because it has so much to do. I use Delphi 2005 at work, and if I didn't have 1GB of memory it would crumble. As for Azeurus, I've never actually tried it, so I don't know, but I've tried RSSOwl (a Java/SWT app) and it was perfectly responsive.\n\nAs for the skills of Java developers, Sun never explained in its documentation the necessity of multi-threading when developing Java GUIs. The SwingWorker class (which made it easier to multi-thread the response to GUI events) was never bundled with the JDK, even though it's been around since 1.1. So the answer is, Sun just never advertised the fact that it was necessary. It has started to do that now, though; there's a lot of momentum building behind Java on the Desktop again, and there are lots of little things like Foxtrot, SwixML and SwingML to make desktop development easier (not to mention the excellent GUI designer in Netbeans 5, the beta of which is available now).\n\nThe fact is Java still requires huge amounts of RAM: if you can manage that, Java will run beautifully. Sun realises this, however, and Java 6 should improve startup time and memory-use significantly."
    author: "Bryan Feeney"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "I feel that Qt4 is not faster as Qt3. If you turn on the fancy stuff, Qt4 is A LOT SLOWER than any other toolkit.\n\nbrought to you as an user feeling, not as a programmer with benchmarks...\n"
    author: "anonymous"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: ">>brought to you as an user feeling\n\nSince when do you users compile (and have feelings *evil grin*)? Are you saying the developers are lying? Because if some developers say it is faster and you say it is not, their lying promotion would be my simple conclusion. You honestly believe they are lying to you?"
    author: "ac"
  - subject: "Re: Great!"
    date: 2005-12-21
    body: "> Since when do you users compile\n\ncompiled it for myself (not that hard) and tried the demos. I think the developer lying if they say it's faster. It's maybe faster to compile Qt4 apps. But that's useless for the \"Suse\" customers...\n\nA faster GUI response from Qt4 based apps is A LOT more important than compile times.\n\nDon't ever hope that KDE4 runs faster on my 500Mhz Laptop than KDE 3.4.x/3.5.x\n"
    author: "anonymous"
  - subject: "Re: Great!"
    date: 2005-12-22
    body: "what aspects of \"GUI response\" are you talking about exactly?\nare you talking about painting N widgets of various types? or model/view based interfaces? or alpha blending and transparency (and if so, client side or hardware accelerated)? or ...?"
    author: "Aaron J. Seigo"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "Qt on Windows and Mac acts as a wrapper around the native APIs. Shame on you!"
    author: "Flavio"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "Not using the native controls, and rendering/managing your own controls that looks like the original one is not a sin. \nOn windows, even microsoft does this because the native widget toolkit is crap, as this blog entry by Raymond Chen explains:\nhttp://blogs.msdn.com/oldnewthing/archive/2005/02/11/371042.aspx"
    author: "MORB"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "Qt supports Windows and MacOSX native widgets when running on those operating systems. In X11, it draws their own widgets because there is no real native toolkit. Only version 1 would try to fake Windows theme (that theme is still available today), but those days are over.\n\nQt is truly cross-platform and performs very well (KDE is kinda slow to startup and stuff, but that has to do with all the technology behind it). Keep the FUD for yourself please."
    author: "blacksheep"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "> KDE is kinda slow to startup\n\nNot anymore :p\nhttp://osnews.com/comment.php?news_id=13039"
    author: "ac"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "Not in my experience.. And I use Qt mostly on Windows at the moment and can't see any difference to native apps (slightly longer start up times for trivial programs).  Dunno about Mac."
    author: "Leo"
  - subject: "Re: Great!"
    date: 2005-12-20
    body: "How true is that :) I was always annoyed by the different menus(they have some offests) and Qt never used cleartype for font rendering so all the Qt apps on windows look alien."
    author: "a"
  - subject: "how about qcanvas?"
    date: 2005-12-20
    body: "I thought it was going to be released with 4.1 :( I suppose it'll come out with 4.2 right guys?, :)"
    author: "Patcito"
  - subject: "Re: how about qcanvas?"
    date: 2005-12-20
    body: "QCanvas ported to Qt4-Style API will be available as a QtSolution.\nQCanvas as in Qt3 is in Qt3Support and is somewhat buggy and slow.\nSomething new and equivalent to QCanvas will be available in Qt 4.2"
    author: "Shyru"
  - subject: "Re: how about qcanvas?"
    date: 2005-12-20
    body: ">>QCanvas ported to Qt4-Style API will be available as a QtSolution\n\nwill it be open source?\n\n>>Something new and equivalent to QCanvas will be available in Qt 4.2\n\nsounds cool :)"
    author: "Patcito"
  - subject: "Re: how about qcanvas?"
    date: 2005-12-20
    body: "http://doc.trolltech.com/4.1/porting4.html#qcanvas wasn't updated. :P"
    author: "Ian Monroe"
  - subject: "Re: how about qcanvas?"
    date: 2005-12-20
    body: "From the release to commercial customers\n\n*********************\nQtCanvas Now Available\n----------------------\nThe Qt 3 Canvas class has been ported to Qt 4.x, and does not depend\non any Qt3Support classes.  The renamed QtCanvas is ideally suited for\ncustomers who wish to port their QCanvas-based applications from Qt 3\nto Qt 4 without relying on Qt3Support.  QtCanvas is now available to\nall Qt Desktop license holders.\n*********************\n\nI wonder if they'll opensource that one"
    author: "Anonymous Coward"
  - subject: "Re: how about qcanvas?"
    date: 2005-12-20
    body: "It is open source already then. But its confusing: http://doc.trolltech.com/4.1/qtcanvas.html appears to have it named Q3Canvas."
    author: "Ian Monroe"
  - subject: "Re: how about qcanvas?"
    date: 2005-12-22
    body: "What Trolltech mean by \"commercial\" is that it is for paying customors only, eg. not open source."
    author: "M_abs"
  - subject: "Native SSH Support"
    date: 2005-12-20
    body: "Since FTP and HTTP are supported natively within QT, I'd love to see a native SSH client available.\n\nTrolltech if you're listening, please implement it!\n\n:D\n\n"
    author: "Steve"
  - subject: "Re: Native SSH Support"
    date: 2005-12-22
    body: "I would also like an built-in opensource SSL Socket, currently it is only in \"Solutions\" which is for their commercial customers only."
    author: "M_abs"
  - subject: "Re: Native SSH Support"
    date: 2005-12-28
    body: "Thanks for the heads up on this page!\n\nNow I'm even more interested in buying a commerical license.\n\nStephen"
    author: "Steve"
  - subject: "Cairo Support"
    date: 2005-12-20
    body: "What is up with Cairo support in qt4?\nWasn't ther a rumour it will happen in qt 4.1?\nI would just love to see everything rendered in Cairo/Glitz."
    author: "Russel-Athletic"
  - subject: "Re: Cairo Support"
    date: 2005-12-20
    body: "What is the advantage? Isn't arthur faster?"
    author: "amadeo"
  - subject: "Re: Cairo Support"
    date: 2005-12-20
    body: "> Wasn't ther a rumour it will happen in qt 4.1?\n\nAFAIK it was said that you could use Cairo as backend for Arthur. That's all. I don't think that TrollTech will provide such a plugin."
    author: "ac"
  - subject: "Re: Cairo Support"
    date: 2005-12-21
    body: "Well, a Cairo backend could certainly be implemented: it's 'just' a matter of subclassing QPaintEngine. Wouldn't it be a step back in terms of speed and functionalities though?"
    author: "Anonymous Coward"
  - subject: "Re: Cairo Support"
    date: 2005-12-21
    body: "that would make any sense. cairo and the qt paint engine do the same thing, so using cairo as a backend only would make it slow, nothing more.\n\ndidn't qt4 have a (buggy?) opengl backend for rendering in the betas? was that removed for the final release? than it would be a good idea to write a glitz backend for arthur."
    author: "SiberianHotDog"
  - subject: "Re: Cairo Support"
    date: 2005-12-22
    body: "Qt has Arthur, it works today and there is simply no reason to use Cairo. Despite much hype Cairo and Glitz will take some time before they are usable. As needs arise Arthur might use Cairo as a backend at some point, but that really depends on whether there's any point at all."
    author: "Segedunum"
  - subject: "Re: Cairo Support"
    date: 2005-12-22
    body: "> As needs arise Arthur might use Cairo as a backend at some point\n\nBetter arthur should use glitz as backend... no?"
    author: "ac"
  - subject: "Re: Cairo Support"
    date: 2005-12-22
    body: "Why? Is there something magical in the name \"glitz\" that it demands it be used? What is wrong with wrong with Arthur?"
    author: "Brandybuck"
  - subject: "comparison"
    date: 2005-12-20
    body: "i would like to see some performance/features comparison between qt4.1 / cairo-glitz / amanith OpenGL based renderers. Anyone know if XaraExtreme has accelerated or software renderer ?"
    author: "Hotbird"
  - subject: "Re: comparison"
    date: 2006-01-15
    body: "Amanith rendering layer is the best in term of features and speed (and it's 100% crossplatform).\nThe incoming version will be the first library that will support in hw all 24 Porter-Duff compositing modes, 15 of them will be available also on hw that hasn't got pixel shaders.\n"
    author: "Matteo"
  - subject: "performance of Qt4.1"
    date: 2005-12-20
    body: "In a kde mailinglist I read a comment from a kde dev saying that Qt4.0 is painting\n*much* slower than Qt3... :-(\n\nHas this situation improved with Qt 4.1?\nIs it on pair, slower or faster than 3.3.5?\n\nAnd what about the backingstore technology...?\nDoes that mean konqueror's tabwidget (and kde apps in general) will not flicker anymore when opening a new tab?\n\nMany questions... I know, but I guess it's of a broad interest ;-)"
    author: "ac"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-20
    body: "> In a kde mailinglist I read a comment from a kde dev saying that Qt4.0 \n> is painting *much* slower than Qt3... :-(\n\nThis is wrong. Most operations are faster in Qt 4, some - like anti-aliased drawing - are even infinitely faster. Just run a Qt 4 program and compare its performance with a counterpart using Qt 3, and judge for yourself.\n \n> And what about the backingstore technology...?\n\nIt's in 4.1.\n\n> Does that mean konqueror's tabwidget (and kde apps in general) will not \n> flicker anymore when opening a new tab?\n\nNo, this has nothing to do with backingstore. Switching tabs is flicker free since Qt 4.0, thanks to an improved recursive setUpdatesEnabled() that implicitely sets the system background to None (so that your X-Server doesn't fill the newly created widgets prior to Qt being able to draw something).\n\nHopefully 4.2 will abandon native widgets (read: X Windows) completely, then even resizing and moving widgets around will be completely solid.\n"
    author: "anonymous"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-21
    body: ">> In a kde mailinglist I read a comment from a kde dev saying that Qt4.0\n>> is painting *much* slower than Qt3... :-(\n\n> This is wrong. Most operations are faster in Qt 4, some \n\nI just refered to this message:\n\nhttp://lists.kde.org/?l=kde-devel&m=113210386019716&w=2\n\nThe rest sounds good though, even for a user! ;-)"
    author: "ac"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-22
    body: "\"Hopefully 4.2 will abandon native widgets (read: X Windows) completely, then even resizing and moving widgets around will be completely solid.\"\n\nHmmm. That's pretty interesting."
    author: "Segedunum"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-22
    body: "> This is wrong. Most operations are faster in Qt 4, some - like anti-aliased \n> drawing - are even infinitely faster. Just run a Qt 4 program and compare its \n> performance with a counterpart using Qt 3, and judge for yourself.\n\nOkay, I just tried. It's true, Qt 4(.1) is a LOT slower than Qt3 on my hardware. System specs: AMD Athlon 1.2GHz, NVidia Geforce FX 5200.\nSoftware specs: NVidia 8174 drivers, xorg 6.8.2, Fedora Core 4. \n\nThe Arthur demo's in particular make my system crawl, but even resizing normal windows lags considerably. Hell, Qt4 makes GTK+ feel fast. :/"
    author: "aac"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-22
    body: "> Okay, I just tried. It's true, Qt 4(.1) is a LOT slower than Qt3 on my hardware.\n\n:-( IIRC the contrary was supposed to be the case.\n\nCan anybody give a deeper look and explanations into this?"
    author: "ac"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-22
    body: "you are talking about something completely different than the person you are replying to.\n\nyou're talking about things like scrolling text being alphablended with a translucent overlay that is performing transformations on the text. \n\nthey were talking about painting things to screen, and in particular widgets.\n\nvery different things. the former is always expensive and slow. the trick is to move it to graphics hardware that has nothing better to do than throw cycles at such things and is optimized for those processes, and then it's nice and fast.\n\nyou can blame the state of X.org for the current annoyances. we're only now starting to get proper drivers for a few cards, powerful (and stable) X extensions and the client-side software that uses them to make the more complex tricks like antialiasing and compositing feel fast."
    author: "Aaron J. Seigo"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-23
    body: "> they were talking about painting things to screen, and in particular\n> widgets.\n\nI don't think so.\nThe last time I tried Qt 4 was something like Qt 4.0 beta.\nYesterday I compiled Qt 4.1.\nDemo apps start really fast :-) And painting has become much faster than in the pre-4 days :-)\n\nBut still, it's significantly slower than Qt 3.\n\nExample: start Qt 4 designer in \"Docked Window\" user interface mode. Create a new \"mainwindow\". Resize this mainwindow inside designer. Awfully slow. It seems to block X refreshing for all other apps while it is being resized. Xosview \"freezes\" while resizing this mainwindow inside designer.\nNow resize the designer mainwindow. On both my boxes, AMD XP 2000+ and Intel P3/450 there is a visible delay. If I release the mouse button, then for a short moment the window background color is visible, and after maybe 0.1 .. 0.5 s the designer window is repainted.\nThis is *really* slow and it flickers.\nAnd I really consider my box fast enough to repaint a window with a lot of widgets fast enough (XP 2000+, 512 MB, Geforce).\n\nAlex\n\nP.S. the arthur \"effect\" demos run slow and take 100% CPU. This is not what I'm talking about above. (while I still that it should be possible to do these things much faster)\n"
    author: "aleXXX"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-23
    body: "I noticed the same thing with Qt blocking 'X' occasionally. For example, scrolling or resizing a window will freeze all other applications for a while.\n\nBut this is probably an X problem, not Qt."
    author: "aac"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-24
    body: "This is not painting speed, this is the cost of indirect painting (painting to an offscreen buffer and then blitting the result on the screen). Some X-Servers are awfully slow doing this. Just blitting a pixmap on the screen without any painting whatsoever will give you a frame rate that windows users or game programmers only laugh at. X currently sucks in that respect. \n\nYour CPU speed doesn't mean much here, it's your graphics card, and how it is supported by the X-Server. Or rather how it's not supported. Maybe Qt should come  with a little test program that blits two pixmaps on the screen, in fastest possible speed using render composite. That would show you the maximum theoretical framerate that a toolkit can achieve on your setup if its drawing speed was infinite. The result would likely be shocking to you.\n\nComparing Qt4's designer with Qt3 is also missleading wrt \"painting\" speed, because Qt4 makes quite a bit of use of alpha effects (like the tinting effect when container widgets are highlighted), and child widgets are composed. Plus there was really suboptimal code in designer that painted the little dots. In Qt 4.0.0 most of the time was spent drawing those dots, don't remember to what degree that was fixed in the final 4.1.0 packages.\n\nPlease don't spread rumours about \"painting being slow\" when you basically mean \"resizing a form in designer exposes some background\". That background is painted by the X-Server when an X Window is reszied, before the very same X-Server finally blits the ready-made contents on it. Qt's painting is fast, the problem is to get the painted pixels on the actual screen. And that's out of the control of the GUI toolkit.\n\nThe same applies to the demos. \n\nOne big step forward that we can do is ditching the concept of native widgets (read: one X Window per QWidget) altogether. This would allow us to do one blit only when a window is resized, as opposed to many blits. This would also fix the problem of exposing some background when resizing child widgets.\n\nFYI: Qt3 painted directly on the screen, like one had to do in the eighties and early ninetees. The costs of doing that are: no child widget composition (required for modern and cool styles), and flicker. Nobody wants that anymore in 2005. Until the X-Servers finally catch up with modern hardware, we will have to live with slower resizing of windows. Good news: users never do that anyway, at most they hit the maximize button.\n"
    author: "Matthias Ettrich"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-28
    body: "Hi Matthias,\n\nI recompiled Qt 4.1 with -no-xrender, and now it behaves noticeable better.\nX isn't \"blocked\" anymore, I can resize the mainwindow in designer as I'm used to.\n\n> Please don't spread rumours about \"painting being slow\" when you basically \n> mean \"resizing a form in designer exposes some background\". That background\n> is painted by the X-Server when an X Window is reszied, before the very same\n> X-Server finally blits the ready-made contents on it. Qt's painting is fast,\n> the problem is to get the painted pixels on the actual screen. \n\nWell, I'm not sure I agree completely here.\nI mean, X is one of the main targets of Qt, maybe *the* main target. So when designing a toolkit for X, the characteristics of X should be taken into account. \nAs a user I don't care why something feels slow, it doesn't matter whether Qt draws blazingly fast and X is too slow or whether Qt is slow and X is fast enough. In the end I only notice that there is something slow.\n\n> And that's out of the control of the GUI toolkit.\n\nWell, not completely. The toolkit can't control the speed of X, but it can use X in different ways. It can use X in a way so that the end result is slow, and it can use X in a way optimized for X, so that the (eventual) performance problems of X are not that much visible.\n\nHow is the performance actually with X over network ?\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: performance of Qt4.1"
    date: 2006-01-25
    body: "I was activated RENDER extension in XOrg with\n\nSection \"Extensions\"\n    Option \"RENDER\" \"Enable\"\nEndSection\n\nAnd also added \n    Option   \"RenderAccel\"   \"1\"\nto my nvidia board options\n\nAfter that QT 4.1 and it's Designer almost flying!"
    author: "Mike"
  - subject: "Re: performance of Qt4.1"
    date: 2005-12-26
    body: "> And what about the backingstore technology...?\n\nI compiled Qt 4.1 yesterday, and all I can say is that flicker is greatly reduced in this release. For comparing, I fired up the qt-assistant from\nQt 3.3.5 and from Qt 4.1. Then I created some tabs, each holding documentation of different classes. Now, by switching from tab to tab you can see no more\nflicker with the 4.1 version, whereas with the 3.3 you can.\n\nGood job trolls!"
    author: "ac"
  - subject: "WYSIWYG: Is this hopeless"
    date: 2005-12-20
    body: "Having skimmed through the new documentation:\n\nhttp://doc.trolltech.com/4.1/qfontmetrics.html\nhttp://doc.trolltech.com/4.1/qprinter.htm\nhttp://doc.trolltech.com/4.1/qpaintdevice.htm\n\nI find two interesting points:\n\nQList<int> QPrinter::supportedResolutions () const\n\nFor X11 where all printing is directly to postscript, this function will always return a one item list containing only the postscript resolution, i.e., 72 (72 dpi ... ).\n\nint QFontMetrics::width ( QChar ch ) const\n\nReturns the logical width of character ch in pixels.\n\nI have to wonder, is it possible that the Trolls just don't get it?  If you compute the font metrics at 72 DPI and then print it at 600 DPI, no wonder it looks BAD.\n\nOn Windows, the printer resolution list will contain numbers like 300, 600 that it gets from the printer driver.  In that context, the resolution of PostScript is INFINITY.\n\n[Another small problem is that not all printers have square resolution.]\n\nOTOH, if we are just using the DPI as logical units it would work with 72 DPI except for the type of \"QFontMetrics::width\".  \"int\" will not work, it needs to be \"float\" for X11.  But, that won't work cross platform -- or will it?  How does Redmond work?  \n\nThe place to start appears to be with: \"enum QPrinter::PrinterMode\".  This is where we should have the three modes:\n\nScreenResolution\nPrinterResolution\nPostScript\n\nBut, this has already been hacked so what appears to be needed is that: \"HighResolution\" should always refer to the resolution of the printer (we need this for Windows compatability and to make PDFs), and PostScript should be added as \"3\".  The problem is that using this will break every function which returns the size of any font metric in pixels.  \n\nI don't have any good answers -- I can only note that they appear to have used the DOS/Windows paradigm of knowing the printer resolution and painted themselves into a corner.  An interim solution would be for X11 to also use the Windows paradigm of using the printer resolution for WYSIWYG layout.  But, this would require changes in KPrint so that it would use a PPD file for all printing.  This would not be an ideal solution since the printing would still be a little off, but it would be a great improvement -- at 1200 DPI, you probably wouldn't be able to tell the difference."
    author: "James Richard Tyrer"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-21
    body: "James,\n\n> I have to wonder, is it possible that the Trolls just don't get it? \n\nThey do get it. If you take a closer look at Qt 4 you'll discover that\n\na) there is also a QFontMetricsF class that gives you (as a developer) floating point precision for simple self-made text layouting.\n\nb) /all/ the internal text layouting in Qt is using floating point precision.\nActually internally 26.6 fixed point is used, just like Freetype, but that's good enough and in the API of QFontMetricsF, QTextLayout and in the higher level Scribe classes you (as a developer) see qreal.\n\nc) when laying out text you can disable the screen hinting and use linear scaled design font metrics for a nice preview on the screen that matches\n(line/page break wise) exactly what you also get on a high resolution printer.\n\nThe last point actually still needs tuning for nicer screen output, but it does not fall into your \"Trolls don't get it\" category."
    author: "Anonymous"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-21
    body: "By WYSIWYG, I mean true WYSIWYG.  Not Windows style which is just font metrics hinted at the printer resolution.\n\nYes, there is also a QFontMetricsF class (should have read more but ... ), but using that in place of the \"int\" typed class will require large changes in code.  After thinking about this, it would appear that you could use the \"float\" type for all layout in KDE and this would solve part of the problem.  It still has cross platform issues since X11 code could be used on Windows but Windows code might not work on X11 if it used the \"int\" class.\n\nYes, according to the documentation:\n\nhttp://freetype.sourceforge.net/freetype2/docs/tutorial/step2.html\n\nFreetype expresses the size of the glyphs in 64ths of a pixel  -- that is 26,6  fixed point binary.  But, that is for _hinted_ font metrics.\n\nHowever, I have not been able to find where in Qt that you would choose unhinted font metrics.  If you choose \"QPrinter::HighResolution\" you have not selected unhinted; it appears that on X11 that you have selected 600 DPI with hinting at that resolution -- at least it appears to me that \"QPrinter::PrinterMode\" would be what you would use to choose unhinted, and there is no PostScript option.\n\nTrue WYSIWYG requires \"metrics in design font units\", and without further information, I don't see how you can do that.  And if you are just doing 600 DPI then the \"float\" class is not needed.  Even if you were using 7200 DPI, integer arithmetic would be faster than float unless you had to use if for scaled font design units.\n\nPerhaps there is a Qt tutorial somewhere that might explain this in more detail."
    author: "James Richard Tyrer"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-21
    body: "-- snip\n>Yes, there is also a QFontMetricsF class (should have read more but ... ), but using that in place of the \"int\" typed class will require large changes in code. After thinking about this, it would appear that you could use the \"float\" type for all layout in KDE and this would solve part of the problem. It still has cross platform issues since X11 code could be used on Windows but Windows code might not work on X11 if it used the \"int\" class.\n-- snip\n\nThere is no portability issue using floating point precision instead of integer precision for text layouting. Not if you also do the glyph positioning in the toolkit, so the same precision is used across all platforms, which is what Qt does.\n\n-- snip\n>Freetype expresses the size of the glyphs in 64ths of a pixel -- that is 26,6 fixed point binary. But, that is for _hinted_ font metrics.\n-- snip\n\nRight, it uses 16.16 for linear(Hori|Vert)Advance, which Qt reduces to 26.6. But that should be by far precise enough for decent glyph positioning on your 600 dpi printer.\n\n-- snip\n>However, I have not been able to find where in Qt that you would choose unhinted font metrics.\n-- snip\n\nPeople doing their own high-level text layouting (such as for example KWord might want to do when ported to Qt 4) should use QTextLayout. With QTextLayout it is possible to enable the use of design font metrics by setting the 'useDesignMetrics' property in the associated QTextOption object. Everyone else is free to use the high-level Scribe classes and enable the use of font design metrics there using QTextDocument's useDesignMetrics property."
    author: "Anonymous"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-21
    body: "-- snip\n> Right, it uses 16.16 for linear(Hori|Vert)Advance, which Qt reduces to 26.6. But that should be by far precise enough for decent glyph positioning on your 600 dpi printer.\n-- snip \n\nMaybe that part should be clarified. I meant it should be good enough for doing text layout in low screen resolution (~96 DPI) but with subpixel positioning for decent output on a higher resolution device then. With a factor 64 of added precision you get an effective resolution of ~6000 DPI.\n"
    author: "Anonymous"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-22
    body: "> There is no portability issue\n\nThe portability issue does exist between *NIX and Windows.  If a large OO Writer document is moved from Windows to *NIX and the compatibility mode in OO isn't used, the document will become noticeably shorter because hinting isn't used to compute the line lengths in *NIX.  I think that the same thing might happen with Qt based wordprocessors, but only if the QFontMetricsF class functions returned non-integer values for unhinted glyphs.\n\n> Right, it uses 16.16 for linear(Hori|Vert)Advance, which Qt reduces to 26.6.\n\nActually, IIUC, FreeType2 supplies these more precise figures but it doesn't actually use them to render the _hinted_ glyphs.  They are supplied so that the application can more precisely compute the layout.  This could be used to support a mode where the screen image was hinted but the printed metrics were correctly based on unhinted glyph dimension.  WordPerfect can do something like this and it isn't really what I would call a feature.  Real WYSIWYG is much better.\n\n> But that should be by far precise enough for decent glyph positioning on your \n> 600 dpi printer.\n\nPerhaps although most office level lasers now start at 1200 DPI and inkjets are 1440 DPI.  But, this isn't WYSIWYG, it is 600 DPI hinted fonts.  As I said elsewhere, perhaps 7200 DPI hinted fonts would be sufficient in all cases, but I don't see 600 DPI as sufficient.\n\nSo, I take issue with the choice of 600 DPI as the ONLY value for \"HighResolution\" with X11.  This is OK as a default but it should be settable.\n\n> People doing their own high-level text layouting (such as for example KWord \n> might want to do when ported to Qt 4) should use QTextLayout.\n\nSo, this is what I am taking issue with.  You shouldn't have to do this (use completely different code) just to get true WYSIWYG.  This should be possible using the \"QFontMetricsF class\" and setting the \"PrinterMode\" to \"PostScript\" (i.e. unhinted fonts)."
    author: "James Richard Tyrer"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-22
    body: "-- snip\n> > But that should be by far precise enough for decent glyph positioning on your \n> > 600 dpi printer.\n \n> Perhaps although most office level lasers now start at 1200 DPI and inkjets are 1440 DPI. But, this isn't WYSIWYG, it is 600 DPI hinted fonts. As I said elsewhere, perhaps 7200 DPI hinted fonts would be sufficient in all cases, but I don't see 600 DPI as sufficient.\n\n-- snip\n\nActually QPrinter::HighResolution gives you 1200 DPI.\n\n-- snip\n> So, this is what I am taking issue with. You shouldn't have to do this (use completely different code) just to get true WYSIWYG. This should be possible using the \"QFontMetricsF class\" and setting the \"PrinterMode\" to \"PostScript\" (i.e. unhinted fonts).\n-- snip\n\nWe are almost in agreement James :). We agree that it should be transparent to the programmer. However I argue that it is easier to default to screen dimensions when printing, because that's exactly what you also see when you debug/develop on the screen. For printing a higher resolution should of course be used, transparently to the programmer by simply scaling the QPainter. And for WYSIWYG the text should be layouted and positioned only /once/, in screen dimension but with a precision high enough to give nice output on a higher resolution printer.\n\nDefault to a 7200 DPI resolution for everything would be an insane waste of resources and it is harder to understand for developers because it significantly differs from what you see on the screen (ever debugged with xmag? :). Plus 99% of the time you do want your text hinted to low-resolution screen output, because 99% of the time text is displayed solely for screen output: Text in menus, text in web pages, text in tooltips, text in labels, etc. That is the common case and that is why text is hinted by default. Those few applications that need WYSIWYG can easily set the one or other property on an object :)\n\nAnyway, I have to run now to catch my plane to my hometown for christmas vacation. Merry christmas everyone on the dot and in the KDE community :)"
    author: "Anonymous"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-22
    body: "> Actually QPrinter::HighResolution gives you 1200 DPI.\n\nThat is good, but I did read 600 DPI in the dox.\n\nI note that as I have said before that you need to be able to choose between: \"Screen\", \"DPI\", and \"FontMetrics\" resolution depending on what you are doing.\n\nObviously \"Screen\" is best for viewing on the screen.  To emulate Windows or to make a PDF, you would normally use \"DPI\" but for true WYSIWYG (e.g. something to be sent out to be printed) you need to be able to choose \"FontMetrics\".\n\nIt would be a great feature if you could choose these three modes and not have to change any code to do it.  This would require using \"float\" for the layout and would (therefore) be a little slower -- but most machines have many times as much power as they need for wordprocessing.\n\nI don't see how 7200 DPI for layout would be a problem.  It shouldn't add overhead since it could all still be done with 32 bit integers and integer arithmetic.\n\nAnd Merry Christmas to everyone.  I got the Flu for Christmas, hope everyone else is doing better."
    author: "James Richard Tyrer"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-21
    body: "I have to admit that I don't understand a word you said. But does this mean that the printing problems in KWord will not be fixed even when based on this version of Qt?"
    author: "ac"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-21
    body: "No, what it does mean is that so far in reading the documentation, I have not yet figured out how to do true WYSIWYG using font design units transformed into actual floating point dimensions (rather than DPI) for glyph dimensions.\n\nHowever, if we could work in 7200 DPI resolution, it would be so close to correct that nobody would know the difference and that appears to require only  small changes in Qt to do.  7200 DPI is a good value to simulate WYSIWYG because it is 5*1440 and 6*1200 so it would work for both common printer resolution moduli."
    author: "James Richard Tyrer"
  - subject: "Re: WYSIWYG: Is this hopeless"
    date: 2005-12-22
    body: "Hello James, \n\nplease take a look at this: \nhttp://blogs.qtdeveloper.net/archives/2005/12/22/printing-and-wysiwyg/\n\n"
    author: "cm"
  - subject: "Now what about KDE 4 itself?..."
    date: 2005-12-21
    body: "Just curious if there are any estimates as to how soon KDE4 will hit the \"usable alpha\" stage at this point.\nJust wondering how soon I, as a compulsive KDE-svn recompiler, can switch to the new KDE4 branch and expect to be able to reasonably use the resulting KDE system productively with \"not too many\" problems.  Recompiling updates to KDE 3.5 just doesn't have the same thrill it used to...\n\n(Is it possible and feasible to have QT4.x and QT3.x coexisting on the same system at the same time?)"
    author: "SMC"
  - subject: "Re: Now what about KDE 4 itself?..."
    date: 2005-12-21
    body: "Yes, it's perfectly possible to have both Qt3 and Qt4. I use KDE 3.5 and develop a Qt4 app. Kubuntu at least has ready packages for Qt4 that work nicely.\n"
    author: "Chakie"
  - subject: "Re: Now what about KDE 4 itself?..."
    date: 2005-12-21
    body: "Same happens in Suse 10.0"
    author: "Iuri Fiedoruk"
  - subject: "3d"
    date: 2005-12-22
    body: "does there are plans to extende arthur to do 3d graphic too?\nI have heard about the upcomming windows-api which will offer a all in one solution (2d/3d) through WPF. I think it would be a real improvement if the Qt api would also offer 2d/3d functionality in a integrative qt-way.\n\nAre there any plans for the future?"
    author: "pinky"
  - subject: "Kubuntu Breezy packages?"
    date: 2005-12-22
    body: "I hope we will see some Kubuntu Breezy packages for this soon.\n\nSean"
    author: "Sean Kelley"
  - subject: "runner up prizes?"
    date: 2005-12-23
    body: "I know that there will be one big prize but what about people who come in like second or third, i feel it is unfair for them to waste their time without having some sort of prize. This would only have to be small in conisderation such as a certificate or whatever... that is signed"
    author: "Luke Parry"
  - subject: "Missing features in Qt4 Designer"
    date: 2005-12-30
    body: "Where is the C++ editor inside Qt Designer?\nWhere is Project Settings Window?\nWhere is the Signal Handlers Tab?\nI like to have the Properties Tab and  Signal Handlers Tab in the same panel.\n\nI think new concepts like Edit Signals/Slots Mode and choose Designer User Interface Mode (Docked Window and Multiple Top-Level Windows) are good.\n\nI think the new Qt4 Designer is too simple , I like Qt3 Designer features.\n"
    author: "Manuel Fdez"
  - subject: "Re: Missing features in Qt4 Designer"
    date: 2006-01-01
    body: "Did you read the http://doc.trolltech.com/4.1/qt4-designer.html reasoning?"
    author: "Anonymous"
---
<a href="http://www.trolltech.com">Trolltech</a> has <a href="http://www.trolltech.com/newsroom/announcements/00000229.html">released Qt 4.1</a>.  The first feature release since Qt 4.0 includes new features which will make it into KDE 4 such as integrated support for rendering scalable vector graphics (SVG) drawings and animations, a PDF backend to the Qt printing system and a lightweight unit testing framework.  Qt Designer has been updated, OpenGL support has been improved and SOCKS5 support has been added.  Their <a href="http://www.trolltech.com/developer/changes/changes-4.1.0.html">4.1.0 changes file</a> has the full details. Get your copy from the <a href="http://www.trolltech.com/download/qt/x11.html">X11 download page</a> or from qt-copy in KDE's SVN.


<!--break-->
