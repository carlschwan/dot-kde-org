---
title: "Oxygen Icons Website Launched"
date:    2005-11-16
authors:
  - "dvignoni"
slug:    oxygen-icons-website-launched
comments:
  - subject: "Wow, professional"
    date: 2005-11-16
    body: "I've taken a look and think these are a good balance of elegance, usability and beauty.  In other words, as soon as it's done, this will be the one I use.\n\nErik\n"
    author: "Erik Hill"
  - subject: "Re: Wow, professional"
    date: 2005-11-16
    body: "Agreed.  Superb.\n\nThis thread is probably going to be a big love-fest, and deservedly so.\n\nI couldn't be more pleased with their work.  Plenty of people have talent, but it's obvious that they have really taken a rigorous and methodical approach, and their preparation is paying off.\n\nSome earlier previews were a tad pixelated on some LCD resolutions, but even that's been refined."
    author: "Wade Olson"
  - subject: "Re: Wow, professional"
    date: 2005-11-16
    body: "I'll give that a boring \"Me too!\"\n\nAbsolutely stunning!"
    author: "Thomas Zander"
  - subject: "Wow"
    date: 2005-11-16
    body: "Those are the most beautiful icons I've ever seen."
    author: "JB"
  - subject: "Absolutely Beautiful"
    date: 2005-11-16
    body: "Those icons rock.  The colors are excellent, and the imagery is sophisticated and professional, but still fun.  I can't wait until KDE 4!"
    author: "Jack Curry"
  - subject: "Nice icon theme, but..."
    date: 2005-11-16
    body: "I was really hoping to get more details about the other cool things about Oxygen that they have hinted at in the past.\n\nFor example, dynamic lighting effects on all icons causing a consistent \"light source\" to be present on the desktop.  Also how are animated icons going to be used?\n\nThat said I really do like the consistent, thought-out and methodical approach taken, rather than the normal \"Here's a whole lot of pretty pictures\" icon design.\n\nL."
    author: "ltmon"
  - subject: "Re: Nice icon theme, but..."
    date: 2005-11-16
    body: "I agree on that.\n\nAfter also plasma and appeal promised a \"whole new desktop experience\" I'ld expect something more exciting then just \"new pretty pictures\". It would be nice to see some of the mentioned effects like the animation and dynamic lightning effects in work, because these are to me the really new features.\n\nFor that reason I don't like the static shadow under the \"actions\" icon, because when lightning is used as a visual aid to tell the user what is going on, the shadows should of course follow that scheme. One could for example give the user the impression of having an icon lifted above the desktop for a drag'n'drop action and a folder opening to signal that it's selected as a possible drop target.\n\nWell, maybe I'm a bit to impatient, but the oxygen, appeal and plasma announcments set my expectations pretty high!"
    author: "furanku"
  - subject: "Re: Nice icon theme, but..."
    date: 2005-11-16
    body: "Well that part is still under development and we dont have anything to show just yet on that department, in some time i espect to have somthing to show, this is one of the parts in oxygen were we are going to need the most feedback couse its new, and we dont want to over do it. "
    author: "pinheiro"
  - subject: "Re: Nice icon theme, but..."
    date: 2005-11-16
    body: "OK!\n\nThanks for the answer, I'm looking foreward to see the first results :)\n"
    author: "furanku"
  - subject: "WOW"
    date: 2005-11-16
    body: "What else could I say... I'm impressed. Now I understand why KDE didn't want to use the crappy Tango icons ;)\n128x128 is also a good choice for the default icon view. I'm not really sure that the one-color-only mimetype icons are a good idea (they look boring right now) but we'll see."
    author: "ac"
  - subject: "Re: WOW"
    date: 2005-11-16
    body: "The previews look amazing! However, I wonder if it is smart to \n\"keep Oxygen fresh for KDE 4, that will be released sometime in the middle of 2006.\" \n \nIn the meanwhile, potential contributors might be drawn to improving Tango not because its icons are better, but simply because Tango draft icons are already available in SVG format.\n\nIt might also be a good idea to include a TODO list on the Oxygen website saying which icons still need to be done. This would help channel efforts into the areas that need most work."
    author: "Vlad C."
  - subject: "Re: WOW"
    date: 2005-11-16
    body: "I don't like the Tango icons, they look cartoonish, unaesthetical. Probably the worst icons made for GNOME so far. Still nuvola beats the hell out of Tango icons. But then it's my personal taste. I also don't like to see Tango influence this project since I believe that the KDE people are doing better in any ways."
    author: "ac"
  - subject: "Re: WOW"
    date: 2005-11-16
    body: "I think Tango will be great at least for creating standard icon naming conventions.  It would be great if a great GNOME icon theme could be used on KDE and a great KDE icon theme could be used on GNOME.  Both sides have some really talented graphic designers."
    author: "Christopher Parker"
  - subject: "Re: WOW"
    date: 2005-11-19
    body: "Afaik the icon naming convention already existed before Tango."
    author: "Anonymous"
  - subject: "Icon design"
    date: 2005-11-16
    body: "PLEASE keep the icons facing at the SAME direction! It is VERY important for consistency and unified look. For example, check the speaker here:\nhttp://www.oxygen-icons.org/wp-content/themes/oxy/images/preview/9.png\nIt should not be looking up."
    author: "UsabilityDude"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "\"It is VERY important for consistency and unified look.\"\n\nWhy is that so important? I don't think they look worse if they face multiple directions. Currently (in my kicker) icons look at various directions, and it doesn't bother me in any way, and it looks good!\n\n\nI'm asking this because I heard this before, but I never heared an explanation - or is it just that it looks better if they face the same direction? (I can accept that, I can even accept that most people would find it more pleasent that way - even though I personally don't - if the claim is backed up by solid research)."
    author: "molnarcs"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: ">I'm asking this because I heard this before, but I never heared an explanation \n\nSome have a sense of aesthetics, some don't.\n\n\n> or is it just that it looks better if they face the same direction?\n\nIt's worse. A set doesn't look consistent, when the imaginary view point axis isn't the same for all icons."
    author: "Carlo"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "I think it's just about aesthetics, nothing more.\n\n> A set doesn't look consistent, when the imaginary\n> view point axis isn't the same for all icons.\n\nWhat is a \"point axis\"?\n\nI think you want to talk about the the \"vanishing point\", which is the point where parallels in some perspectivic point of views intersect. This has nothing to do with the orientation of the depicted object, and is also for a two dimensional desktop irrelevant, because then you would need to change the view of the icon depending on the position on the screen (which would be easier to implement if you would go to a real three dimensional Desktop).\n\nSo maybe you mean that all icons should have the same perspectivic point of view, but the actually are consistent in that. The speaker is in the same perspectivic view as the printer next to it, the object itself is just rotated. I think it's legitim to do that. A speaker for example has it's \"chakteristic view\" of the cone, chassis and the coils if seen in a \"semiprofile\". If you look at it in a frontview you basically see concentic circles, which are not that easy to identify as a speaker. Similar observations lead to other (rotational) views on other objects, but the perspective stays the same.\n"
    author: "furanku"
  - subject: "Re: Icon design"
    date: 2005-11-17
    body: ">What is a \"point axis\"?\n\nSomething similar to a line surface ? but with 1 dimension less... fractal stuff, perhaps?"
    author: "oliv"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "I saw this in the past few weeks. I think this new hobbyhorse is actually a (psycho)linguistic phenomenon. Icons facing in the same direction convey a sense of order which is one of the connotiation of consisteny (it is not its meaning, and I won't say it is its primary connotation, just one of them). \n\nFrom a usability point of view it doesn't make too much sense, nor does it from an aesthetic point of view. Take a look at my kicker in the kde screenshot>\nftp://hatvani.unideb.hu/pub/personal/screenshots/mykde343/kdedesktop_3_3.4.3_kcontrol_and_media.png\n\nThe reason that the icons are inconsistent there is not primarily because they face different directions (take a look at the middle), but because they are from different icon sets (no icon set is complete enough unfortunately). There are a number of aspects that make an icon theme more consistent, all of which are more important than the direction they face (for instance, the color palette). In fact, if we allow the \"direction\" mantra to override other aspects, it may even hurt usability. Icons should be easy to distinguish from a distance, and what's more, if there are two applications that serve more or less the same function (you see kword, openoffice writer and scribus in my screenshot), the difference in the direction helps to distinguish (not only on first sight, but it makes easier to remember as well).\n\nBasically the parent's post is a good example for taking direction too seriously. We see three icons: printer, speaker, CD. The printer and CD face us, the speaker doesn't ... and actually couldn't. Imagine the same speaker from the front :)) So basically parent is asking for redrawing the icon completely, because it doesn't face us. This \"must face the same direction\" is a little bit overrated, don't you think?"
    author: "molnarcs"
  - subject: "Re: Icon design"
    date: 2005-11-17
    body: "'From a usability point of view it doesn't make too much sense, nor does it from an aesthetic point of view.'\n\nYou are talking about about aestetics and usability? First, you have to learn something about colors (your desktop, which you are so proud of, is a proof that you are colorblind). Combination of shity green and shity blue color on your desktop is a great example how one should not use the colors.\n\nAnd rule number one (which you don't understand):\n\nAestetics = usability\n\n'There are a number of aspects that make an icon theme more consistent, all of which are more important than the direction they face (for instance, the color palette). In fact, if we allow the \"direction\" mantra to override other aspects, it may even hurt usability.'\n\nNo shit. Dude, you don't understand anything. Direction is as important as any other aspect. Icons must not be a disturbing element on a desktop, and they are disturbing element if they are not facing the same direction."
    author: "Lala"
  - subject: "Re: Icon design"
    date: 2005-11-17
    body: ">Aestetics = usability\n\nPlease check in a dictionary. And then read some documentation about usability. And then think. The conclusion will spring. A sparkle of understanding in your mind. A delicious feeling."
    author: "oliv"
  - subject: "Re: Icon design"
    date: 2005-11-17
    body: "\"your desktop, which you are so proud of,\"\n\nOh, please - I'm not proud of it, in fact, I linked to it as an example for color mismatch. I prefer green as a background, and I have yet to find an icon theme that is both complete and integrates well to my taste in backgrounds.\n\n\"Aesthetics=usability\"\n\nYeah, sure. I just checked the highest rated icon sets on kde-look: nuvoeXT, crystalclear, nuvola, in this order. These are the icon sets users found most pleasing, and yet - behold the direction of the icons: ftp://hatvani.unideb.hu/pub/personal/screenshots/icons/\n\nI think this pretty much settles the matter: good usability doesn't have to be explained to the users (with brain-pattern-recognition babble). I'm all for cooperation and whatnot, and my intention is not to incite a flamewar, but this entire matter reminds me of usability folks in the \"other\" project having to explain why one or other feature (spatial browsing by default comes to mind is supposed to be better), disregarding every complaint from the userbase... yeah, sure, you usability pundits don't have to listen to your users (this is a hypothetical \"you\" here, adressed to usabilityDude type of pundits). If they don't agree with your views on usability, it simply because they have poor aesthetic taste,  and idiots in general, right?"
    author: "molnarcs"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "Hi usabilityDude;\n\nI'm not new to usability and the only thing I know about is people facing left or right as well as moving things going left or right.\nThe things you point out are new to me, please tell us how you came to the above conclusion.\n\nCheers!"
    author: "Thomas Zander"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "I will only say one thing: BeOS.\nIt had ALL its icons facing at the same direction. And this made a HUGE impression to its users, making them think that the system is actually easier to use, because the brain did not have to adjust itself to recognize shapes looking at different directions.\n\nUsability for large sets of icons DICTATES that for consistency and for brain's ease-of-pattern-recognition, ALL its icons MUST face at the same direction.\n\nThe new Gnome effort is also doing the exact same thing. I hope the KDE dudes also listen and do it too."
    author: "UsabilityDude"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "In my above reply I tried to remember where I saw icons that were aesthetically unpleasent and they looked like an army or something - and BANG! it was the BeOS screenshots I saw (I just didn't remember it).\n\n\"Usability for large sets of icons DICTATES that for consistency and for brain's ease-of-pattern-recognition, ALL its icons MUST face at the same direction.\"\n\nI think this is a hoax - or at least it doesn't make any sense (but sounds good and it looks like as if it was something serious - it isn't). Capitalizing the keywords don't help it either :) Lets see:\n - \"for consistency:\" this almosts amounts to tautolgy ... consistency with what? with each other? Because there are a lot of factors that are more important to make an icon set consistent: the color palette, the \"theme\" (in the sense of kids-icons, realistic-icons, whatnot), etc.\n\n- \"and for brain's ease-of-pattern-recognition,\" Again, what do you mean by that? Becaue it sounds cool and all, but there is no such thing as the brain's easy-of-pattern-recognition. Yes, the brain recognizes patterns (which appears to be acquired with and a function of language) - even where there are none I might add :) - but that doesn't necessarily help usability at all. It is much more important what the icons actually represent. The user should know at first glance what will be the result of clicking a particular icon, and whether all icons face the same direction or not doesn't help a bit in this.\n\n- \" because the brain did not have to adjust itself to recognize shapes looking at different directions.\" You largely underestimate the average brain. Your entire argument is a classic case of thinking of human beings as computer programs in the sense that you reduce the user to a set of functions - the brain's easy-of-pattern-recognition function - without taking into consideration just how complex the entire issue is. For instance, one might argue that \"a slight variation in the direction icons look keeps the brain unconsciously alert\" - I just couldn't make that argument with a straight face :))\n\nAre you sure you didn't make that post with tongue in cheek (because then, I'm a victim of your humour :)))\n \n"
    author: "molnarcs"
  - subject: "Re: Icon design"
    date: 2005-11-16
    body: "You are obviously an idiot. Icons of a given set must be consistent with each other. They do look better, they do look more welcoming, and they don't look like a FREAKING MESS."
    author: "UsabilityDude"
  - subject: "Re: Icon design"
    date: 2005-11-17
    body: "You're probably going to have to do better than resorting to name-calling to get people to take your point seriously.\n\nTry answering his actual points. Calling him an idiot is just a signal that you don't have actual data to back up your point of view."
    author: "Dolio"
  - subject: "Re: Icon design"
    date: 2005-11-17
    body: "\"You are obviously an idiot.\" Yeah, obviously.\n\nYou answered none of the points I raised, and your reaction is quite ironic in the light of your nickname and the fact that you're preaching usability.\n\nI will try to describe my points more simply (I'm not an english speaker, so it might be difficult to understand me sometimes). So: whether an icon theme is consistent or not depends on many things, of which the direction they face is the least important. I refer you to my screenshot that I posted before. If you check out the 4 icons in the middle, odd one is obviously the kword icon - not because it leans in another direction, but because it has an entirely different color palette. \n\nWhat you wrote as an explanation seems to be no more than good sounding but empty terms (brain-pattern-recognition...) and I eagerly awaiting your real answer to the points I raised concerning your techno babble. \n\nMy whole point is that icon direction is totally overrated. Yes, I can imagine certain circumstances when it is beneficial, or simply looks better (like mimetypes/folder icons) - but that doesn't mean that they are actually more usable or user friendly. In very simple terms, a variation in direction might help the users remember the distinctions between say scribus, openoffice or kword which have similar functionality (thus they'll have similar icons: see my example, there is pen and paper on each of them) but are completely different beasts in reality. \n\nYour BeOS example fails on the point that, again, you overrate one aspect of the UI (all BeOS icons lean in the same direction) so much that you attribute the - admittedly legendary - usability of BeOS to this single factor. This is the least important factor in my opinion, and what's more, I haven't seen any solid evidence or theory behind your reasoning except the (silly) BeOS example and the fact that GNOME is doing it. Fine, and tango actually is not bad, but what makes it good is that it is less dull than the current GNOME icon theme - see the comparison on the project's page. And we know pretty well that the current icon theme was also the result of wild usability theories, which now seem to be discarded in favor of a new one (and I might add that what makes tango nice is that it is not very dissimilar to oxygen's less vibrant themes). \n\nIn short: if you come up with such a wild theory, and you keep shouting DO THIS BECOUSE USABILITY DICTATES IT, you better come up with some reasoning beside GNOME is doing it and BeOS ruled once. And don't call those who disagree with you idiots. Thanks."
    author: "molnarcs"
  - subject: "Crystal Icons"
    date: 2005-11-16
    body: "The Crystal Icons are not bad at all, either.  On the contrary.  Remember those before Crystal?  They looked kind of brown (Gnomish) :-)  Crystal is a great appearance for KDE.\n\nEveraldo remains one of KDE's finest artists, too.\n"
    author: "Silviu Marin-Caea"
  - subject: "Re: Crystal Icons"
    date: 2005-11-16
    body: "My problem with the Crystal Icons is that they don't look really good at 32x32 (a bit blurry) and 16x16 (a bit hard to distinguish from each other). \nIf you have a small monitor or just want to use a lot of icons on desktop, toolbars and Konqueror they are not the best choice. For example I have a desktop resolution of 2560x1024 (Xinerama) and still prefer 32x32 for the desktop and 16x16 on quickstart bars.\n\nIn 64x64 or 128x128 they look really great, but who uses such big icons? A good example is the wallet icon. It looks fantastic - but not on smaller icon sizes.\n\nOxygen seems to go into the same direction. In this respect I like the Tango concept a bit more (yet it has some other disadvantages e.g. some icons like the speaker are very hard to recognize at smaller sizes)."
    author: "Ascay"
  - subject: "Re: Crystal Icons"
    date: 2005-11-16
    body: "And yet Oxygen is being made to use 128x128 optimally.  Toolbar icons are being designed for 32x32 at least, but who knows how they'll look in 16 or 22, and mimetypes designed for 128 may just look terrible in detailed list view..\n\nI would hope they address it, but it doesn't sound like they are paying any more attention to making sure small icons are usable than Everaldo did.  SVG just isn't hot at 16px\n\nWe'll see I guess, there's just an awful lot of acclaim being lobbed at what amounts mostly at this point to words."
    author: "MamiyaOtaru"
  - subject: "Re: Crystal Icons"
    date: 2005-11-16
    body: "actually - i really hope icons are going to be 'bigger' in the future...\n\nand i really hope that screens will start to become 'bigger' as well. i want higher resolution in terms of ppi so that nice icons don't fill that much...\n\ngive me 19\" screen with 4000x3000 pixels and i put 128x128 in my toolbar right away...\n\nbut, thanks to the limits in windows, hardware vendors will not give us that... hoping for vista"
    author: "bangert"
  - subject: "Re: Crystal Icons"
    date: 2005-11-17
    body: "Some types of screens can not become larger. For example, I'm currently typing on a laptop with a 14.1\" screen. If the screen got much bigger, it would loose its portability. Since a significant amount of people have a laptop or a small monitor, KDE will always have to keep these people in mind."
    author: "Pingveno"
  - subject: "Re: Crystal Icons"
    date: 2005-11-17
    body: "Yes, but physical screen size has no direct correlation to size of icons without factoring in resolution.\n\nWhich is to say - your future 14.1\" screen with a 4000x3000 resolution will need large icons (around 128px) to have them be the same size as today's medium icons (around 32px).  Physical screen size will likely maintain a range of sizes to suit the consumer's taste, but resolution tends to always increase.  Thus large icon sizes satisfy increasing resolutions on any physically sized screen from your small laptop to large widescreen displays."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Crystal Icons"
    date: 2005-11-16
    body: "I can't say I've ever seen anyone complain about Crystal.  Well, not until I read Oxygen's site complaining about childishness etc.  Of course they are going to push their theme, but to me it looks like an end run around Everaldo, who's put in a ton of time making an awesome icon theme for KDE.  \n\nI obviously wasn't around for the conference that birthed Oxygen, but I just haven't seen the great outcry for a new theme that a project of such magnitude would seem to imply.  \n\nhttp://www.kde-look.org/poll/index.php?poll=96&PHPSESSID=bdc23c30b2d8950bf964a6e4945879ee\nNuvola certainly wasn't far behind, but it was.  Crystal had the lead.  Granted, FLOSS is about choice, and plenty of people like Nuvola and will like Oxygen (while others can continue using Crystal), I just don't see why it is being treated as a shoe-in to be the new default, especially when we really haven't seen any of it.  All may become clear as time passes ;)\n\n/me adopts a wait and see attitude"
    author: "MamiyaOtaru"
  - subject: "Re: Crystal Icons"
    date: 2005-11-16
    body: "A new theme was needed anyway to take advantage of the new upcoming KDE 4 features. Regarding Crystal it is a nice theme and people like it, but there were many problems with small icons and the original author haven't never done a work of theme mantainence and it took a lot of time before the sources were made available to the community, and anyway as Illustrator files. Oxygen will be released as SVG, so all the sources will be available and the Oxygen team is commited to mantain the theme when released. We know SVG isn't good for small icons, for this reason we will made special version for these small sizes."
    author: "David Vignoni"
  - subject: "Yeah... these icons rock!"
    date: 2005-11-16
    body: "Great work! "
    author: "Thomas"
  - subject: "Icon design software"
    date: 2005-11-16
    body: "I am sure that KDE 4 will rock. And more so with the new fresh icon theme.\n\nWhat kind of tools/software is used for designing this kind of stuff. I doubt if GIMP can do that all this. Are there any good linux based softwares?"
    author: "Nilesh Bansal"
  - subject: "Re: Icon design software"
    date: 2005-11-16
    body: "I saw for Linux a program that is more similar in apparence to Photoshop :\nhttp://www.kanzelsberger.com/pixel/?page_id=12\n\nBut the program isn't be OpenSource or Free , you have to pay to use more than trial time ... The program isn't be so expensive."
    author: "Yuko"
  - subject: "Re: Icon design software"
    date: 2005-11-16
    body: "I think that icons are madi with some SVG editor like, Inkscape or Sodipodi. They are both GPL and free. Maybe they are made with Corel."
    author: "MaBu"
  - subject: "Re: Icon design software"
    date: 2005-11-16
    body: "I use mainly inkscape, but i also use krita, karbon14, and all of the great desktop kde is.  "
    author: "pinheiro"
  - subject: "Re: Icon design software"
    date: 2005-11-16
    body: "Besides the applications that are already mentioned, I heard that XaraExtreme will become available for linux under an open source license..."
    author: "rinse"
  - subject: "Re: Icon design software"
    date: 2005-11-17
    body: "I use Adobe Illustrator. The I fix the SVG files with Inkscape. I use Machintosh to work and have KDE installed also, but I try the results on another Linux machine."
    author: "David Vignoni"
  - subject: "Quite nice ..."
    date: 2005-11-16
    body: "... and less blue, which I consider a grand thing."
    author: "St. Kevin"
  - subject: "Licence question"
    date: 2005-11-16
    body: "I'm not sure I understand the licence of Oxygen icons right. It's non-commercial so are commercial (closed source) software vendors able to distribute these icons without opening the source of the programs?\n\nI was wondering this just because it would be great to see some closed source offerings complementing the great KDE user experience.\n\nAlso, will these icons be cross-desktop like Tango icons?\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: Licence question"
    date: 2005-11-16
    body: "Hrm, after RTFA I found that the icons will be released under GPL, LGPL or under a icon-specific similar licence.\n\nSorry for not RTFA before asking ;)\n"
    author: "Eleknader"
  - subject: "Re: Licence question"
    date: 2005-11-16
    body: ">\n> Also, will these icons be cross-desktop like Tango icons?\n>\n\nIf a cross-desktop look was being sought after then there's no reason the better icons couldn't be folded into Tango (the help, navigation and reload buttons in particular are gorgeous and miles better than the Tango ones). For once there's absolutely no difference between the stated goals of the \"rival\" initiatives, and it's obvious that Oxygen is following the same visual guidelines sgarrity came up with for Firefox (and thus Tango).\n\nI'm pretty disappointed with the need to \"preview\" an unfinished icon set for the sake of keeping up appearances, especially when combined with a promise to relicense later. It's yet more duplicated effort.\n\n - Chris\n"
    author: "Chris C"
  - subject: "Re: Licence question"
    date: 2005-11-16
    body: "About cross-desktop,yes i hope so!\nbut we will need to do 2 version, couse ours will be done im full svg and probaly will have specila features that only kde at near future time will be able to render, and a normal multi size png icon set for all other desketops.   "
    author: "pinheiro"
  - subject: "Oxygen needs to be cross-platform!"
    date: 2005-11-16
    body: "Please try hard to remain compatible to 'the other desktop'. Not that I use gnome, but there's a lot of duplicated work or icon themes that never reach the other desktop and it's just a lack of communication.\n\nTango did maybe not involve the KDE developers enough in their preparation phase, but their specification is still adaptable. You three are the ones who will decide on icon names, usage, guidelines and everything, and I ask you to actively participate in the evolution of the Tango icon naming specification, so that it's really usable by KDE too.\n\nI'm glad that you're not against cross-desktop usage, but just hoping isn't enough to succeed here. Get on the Tango-artists mailing list at fd.o and push the changes in that you think are necessary. For the best of both of the big desktops."
    author: "Jakob Petsovits"
  - subject: "Re: Oxygen needs to be cross-platform!"
    date: 2005-11-18
    body: "> Please try hard to remain compatible to 'the other desktop'.\n\nWe will try hard to remain compatible when it comes to standards that actually make sense: like the icon naming specification, a common icon metaphor etc. \n\nBut like all other projects (Enlightenment, XFCE, Gnome, etc.) KDE will stick to its own style, its own color palette and its own visual identity.\n\nBest Regards,\nTorsten Rahn"
    author: "Torsten Rahn"
  - subject: "Re: Oxygen needs to be cross-platform!"
    date: 2005-11-18
    body: "> We will try hard to remain compatible when it comes to standards that\n> actually make sense: like the icon naming specification, a common icon metaphor etc.\n\nExcellent, it's good to hear that.\n\n> But like all other projects (Enlightenment, XFCE, Gnome, etc.) KDE will\n> stick to its own style, its own color palette and its own visual identity.\n\nI fully support this approach. May the schwartz be with you! :)\n"
    author: "Jakob Petsovits"
  - subject: "US flag"
    date: 2005-11-16
    body: "... and please use a different icon for internationalisation (locale.png) than Everaldo. I think the US flag is no longer a symbol for that."
    author: "AC"
  - subject: "Re: US flag"
    date: 2005-11-16
    body: "The Esperanto-flag (Green background with a white star) would be nice as symbol for internationalisation!"
    author: "atrink"
  - subject: "Re: US flag"
    date: 2005-11-16
    body: "How about the UN flag ?"
    author: "Eduardo Alvarenga"
  - subject: "Re: US flag"
    date: 2005-11-17
    body: "I like both ideas. UN flag is also used in Mac OS X."
    author: "David Vignoni"
  - subject: "Re: US flag"
    date: 2005-11-17
    body: "UN flag would raise some hackles in certain portions of the United States of America ;)  \n\nHow about the Olympic flag?  It's probably copyrighted or something, and honestly not quite ideal: too much association with sports.  Still, I like the look better than the UN flag.  Just sayin."
    author: "MamiyaOtaru"
  - subject: "Re: US flag"
    date: 2005-11-17
    body: "do klingons have a flag ?"
    author: "oliv"
  - subject: "Re: US flag"
    date: 2005-11-17
    body: "Yes, but it is oddly shaped to human eyes (a hexagon with very long sides on the upper right and upper left that results in a cut off teardrop shape).  Similar to medieval war banners rather than the common rectangle flags that most governments have adopted.\n\nHow about a KDE flag?  Blue with a white K on it?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: US flag"
    date: 2005-11-18
    body: "\"UN flag would raise some hackles in certain portions of the United States of America ;)\"\n\nBut they would not have a clue about internationalization anyway, so why care?\n"
    author: "Morty"
  - subject: "Re: US flag"
    date: 2005-11-17
    body: "Why not use the gay flag then. It has all the flag colors ;)"
    author: "Carewolf"
  - subject: "what happened with Everaldo?"
    date: 2005-11-16
    body: "Hey guys, what happened with Everaldo and Crystal icons? Aren't his icons not good enough anymore for you or he does not commit anymore?\n\nHis icons were nice, and most importantly the license was a very permittive one without attaching any strings and royalities (not sure if it was BSD or LGPL). Ever since I have seen many apps using this icons, and even sites (especially control panels).\n\nHere is posted the license polocy of Oxygen icons:\nhttp://www.oxygen-icons.org/?page_id=4\n\nIs Oxygen icon set planned to be released under license in the same spirit as Crystal was?"
    author: "Anton Velev"
  - subject: "Re: what happened with Everaldo?"
    date: 2005-11-16
    body: "In the site it's clearly stated that the icons (SVG) will be released under a GNU License. LGPL or an equivalente license for graphics or icons."
    author: "David Vignoni"
  - subject: "Re: what happened with Everaldo?"
    date: 2005-11-16
    body: "Still many apps and sites used Crystall icons. May be they were happy enough with PNGs.\n\nMy question is will this be possible with Oxygene?"
    author: "Anton Velev"
  - subject: "Re: what happened with Everaldo?"
    date: 2005-11-16
    body: "the hole set will be lgpl or somthing that works in the same way, png's, svg's, documentation, you name it.\nBTW and talking about site's any kde realated site is more than welkome to contact any of us in oxygen, we hope to help to create artwork, layouts, icons, logos, for all of kde not just the icons, or the desktop. Remeber Oxygen is not just an icon set."
    author: "pinheiro"
  - subject: "Re: what happened with Everaldo?"
    date: 2005-11-16
    body: "We are not doing an icon theme for websites but for KDE, so what webmaster will do is not very important for us. What we know is we will all the icons needed to KDE, this include apps (I mean the applications shipped with KDE)."
    author: "David Vignoni"
  - subject: "thanks"
    date: 2005-11-16
    body: "Re: pinheiro and David Vignoni\n\nThanks guys! So this icons (and themes) are really going to be avail to everyone, being it hobbyist contributor, commercial vendor or webmaster, good.\n\nAbout the predcessor (Crystall), you may have not, but I have really seen this icons used not only in apps but also in website control panels (I think vdeck and plesk used exactly them). With your release their next versions will still be consistent with the UNIX desktop. You bet how important is millions of website owners to the user to familiarize with your icons, and you only can guess how easy and familiar will be to them when one day they see a running KDE! :)"
    author: "Anton Velev"
  - subject: "wow"
    date: 2005-11-16
    body: "this will very likely be the first time ever that i will use the standard icon theme! thumbs up! ;)"
    author: "fish"
  - subject: "Underwhelming"
    date: 2005-11-16
    body: "Here, to the contrary, this would mean once again NOT using the default.  This reminds me of the pre-crystal icons, altho they were better than the Gnome ones of the time.  I'll stick with Crystal, thank-you very much.\n\nDuncan"
    author: "Duncan"
  - subject: "Re: Underwhelming"
    date: 2005-11-18
    body: "> This reminds me of the pre-crystal icons, \n> altho they were better than the Gnome ones of the time.\n\nGiven that this is one of the most negative views and given that Oxygen is still in a very early stage (like crystal was back in that time) I guess that the Oxygen team is meant to take this as a complement ... ;-) "
    author: "Torsten Rahn"
  - subject: "Hmm."
    date: 2005-11-16
    body: "Not bad at all, but I'm not seeing the 'oh wow!' that everyone else is. The folder icons look great, but the rest are a bit, well, plain. Maybe they're too flat? Don't know, and perhaps a screenshot with them would show them off better, but for now I like Nuvola, and probably Crystal better, though."
    author: "Illissius"
  - subject: "Fantastic!"
    date: 2005-11-16
    body: "Excellent Job David & Team!"
    author: "Mark Mahle"
  - subject: "Please move to analog measures!"
    date: 2005-11-16
    body: "Hi, \n\nmy hopes for the future of OSS are refreshed once again seeing this piece of work :)\n\nOne bigger nitpick: \nWith computers we already use analog measures for fonts, audio, time, etc. Please do so for icons, too. By using SVG instead of rastered pixmaps we can finally follow the route of free scaleable fonts (PS, TT) which is the way to go*. Please, please, take this into account. This would really rock.** ***\n\n*There is even close to no difference between chinese characters and symbols from a certain POV ;)\n**And widgets should follow the very same route. Let's control the window border size in mm, not pixel numbers.\n***Once this is done there should be support for the perceived sizes. Looking at your pda, your monitor and a beamer projection results in different physical \"display\" sizes. But your perception might be the same as your distance to the display is different, too.\n\nThanks\nFriedrich"
    author: "Friedrich"
  - subject: "Re: Please move to analog measures!"
    date: 2005-11-16
    body: "Yes we will, that is the general idea, to make a fully svg theme. only one icon for all sizes. But also to vastly extend the svg features so that icons can starto interact with the uzer, in sum Oxigen is more than just an icon theme, itsa a second generation icon theme."
    author: "pinheiro"
  - subject: "Re: Please move to analog measures!"
    date: 2005-11-16
    body: "imho the interaction idea rules :D\n\nalso the fact they will/might be dynamic, very cool. maybe you can make mouse-over animations?!?"
    author: "superstoned"
  - subject: "Re: Please move to analog measures!"
    date: 2005-11-16
    body: "Still you speak in pixel sizes, like \"The default size we would like for icon view is 128\u00d7128.\" or \"All of the action icons are made in the 32\u00d732 svg size and scaled to other sizes.\" (see http://www.oxygen-icons.org/?page_id=2)\n\n128x128 pixels is different on a 19'' monitor with 1200x1024 from an 15'' with 1400x1024. You would choose the size of the icon based on the physical sizes, that is cm.\n\nI think I understand that to gain less blur if rendered to the pixel grid in smaller sizes (-> less pixels) it is taken care to layout the shapes for this, so pixel numbers are still important. But they should not be the one we use to define the size finally rendered on the display. \n\nDoes SVG allow some kind of hinting? So details (like embedded symbols or shape lines) could be adjusted to match the pixel grid?\n\nFriedrich"
    author: "Friedrich"
  - subject: "Re: Please move to analog measures!"
    date: 2005-11-17
    body: ":: Does SVG allow some kind of hinting? So details (like embedded symbols or shape lines) could be adjusted to match the pixel grid?\n\nYes... thus the \"32x32 svg size\".  They can be rescaled to any size with all the nice look of a scaled vector image."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Please move to analog measures!"
    date: 2005-11-17
    body: "One even bigger nitpick:\n\n\"Analog measurements?\" What the hell are you talking about?\n\nShow me how any of the stuff you just described is \"analog\" in any way. It's all represented in binary at its lowest level."
    author: "erg"
  - subject: "Looks good!"
    date: 2005-11-16
    body: "Keep up the great work!\n\nThanks!"
    author: "Joergen Ramskov"
  - subject: "Continuing Nuvola?"
    date: 2005-11-16
    body: "While these icons in the previews are attractive.  I wonder what will happen to Nuvola?  Will David continue to maintain them or will they be superceded by the Oxygen icons?  The folder and mimetype icons in the Oxygen preview doesn't look as sharp as Nuvola ones.  Maybe a blending of the two?\n\n"
    author: "AC"
  - subject: "Oh My God"
    date: 2005-11-16
    body: "They are the best icons I've ever seen. The perfect mix of colour, eye candy and easiness on the eyes. They're certainly not boring, but they're usable."
    author: "Segedunum"
  - subject: "Interoperability"
    date: 2005-11-16
    body: "Great work! as always \nOnly one thing, I saw people call the tango project crappy (joking I suppose) don't hear them, the tango project it's a good thing, and must be supported, just like oxygen it's needed in order to have complete themes for both DEs there, instead of duplicate icon themes in order to have a coherent desktop.\nWe could have finally (at the moment) two great Icon themes, that work out of the box in both envrionments"
    author: "ra1n"
  - subject: "Everaldo's icons"
    date: 2005-11-16
    body: "I still prefer everaldo's icons ( http://www.everaldo.com/ )... oxygens don't bring nothing new ... well, i'ts my opinion anyway :)\n\n"
    author: "Daniel Semblano"
  - subject: "Re: Everaldo's icons"
    date: 2005-11-16
    body: "Same here, I don't see why everbody is loving oxygen.\nNothing against it, but crystal seems much better to me.\nOr this post have only atracted people who dislike crystal, or it's time for me to change the glasses ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Everaldo's icons"
    date: 2005-11-17
    body: "Well Oxygen is nice and elegant and beautiful, for this reason they are loving it :)\nAnyway, this doesn't mean they didn't find Crystal nice and elegant and beautiful as well, in fact this post isn't titled: \"Do you like Oxygen more than Crystal?\"."
    author: "David Vignoni"
  - subject: "Re: Everaldo's icons"
    date: 2005-11-17
    body: "> Same here, I don't see why everbody is loving oxygen.\n\nProbably because they are not so eye-distracting and \"childish\" as crystal (and XP) are?"
    author: "Davide Ferrari"
  - subject: "No more floppy disks!"
    date: 2005-11-16
    body: "I saw a picture of a 3.5\" floppy disk grouped with their \"action\" icons. A set of action icons designed in 2005 shouldn't use a picture of a floppy disk to indicate  \"save\". That makes as much sense as using a picture of a reel to reel tape or a punch card. There's a whole generation of computer users coming up who've never seen a floppy disk. We need a new metaphor for 'save'. I'm too old to come up with a good one though. We really should ask kids under 15 to 'draw a picture of what it means to save a file.'"
    author: "Scott"
  - subject: "Re: No more floppy disks!"
    date: 2005-11-16
    body: "Well, we still save to harddisk, so perhaps a harddisk would be a symbol that is not too far away from the floppy symbol, so comprehensible by \"older\" users and also not counter-intuitive for \"younger\" users.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: No more floppy disks!"
    date: 2005-11-17
    body: "This is what Tango does right now, a harddisk with an arrow on it:\nhttp://tango-project.org/static/cvs/tango-icon-theme/scalable/actions/document-save.png\nIt's an experiment, but I think it works fairly well."
    author: "Daniel Borgmann"
  - subject: "Re: No more floppy disks!"
    date: 2005-11-17
    body: "How about an icon of a file cabinet, e.g. http://images.google.com/images?&q=file+cabinet\n\nIt would complement the icon of a folder commonly used for Open.\n\nI agree, that more suggestions would be helpful (especially from people not biased by previous computer use.)"
    author: "jeremiah"
  - subject: "Re: No more floppy disks!"
    date: 2006-11-12
    body: "Reminds me windows 3.1"
    author: "Kollum"
  - subject: "Re: No more floppy disks!"
    date: 2005-11-17
    body: "Random fun fact: The Xbox version of GTA: San Andreas uses a floppy as a save symbol."
    author: "Mikal"
  - subject: "Re: No more floppy disks!"
    date: 2005-11-19
    body: "or even better: get rid of the save action... seriously!\nbut then, what's the icon for 'name document'?"
    author: "bangert"
  - subject: "I don't know"
    date: 2005-11-16
    body: "Maybe I should see the whole picture, but I'm not convinced yet. In my opinion the icons bring less new than expected. Don't get me wrong, I thing those nice icons, but they're not as innovative as I hoped for. :)"
    author: "mOrPhie"
  - subject: "Thanks"
    date: 2005-11-17
    body: "Since I'm only replying to people who say that Tango/Crystal/Whatever is better, I would like also to thanks all the people for the nice messages and interest, this was not really expected.\n"
    author: "David Vignoni"
  - subject: "Come on!  ;-)"
    date: 2005-11-17
    body: "Don't make me laugh. Did you really expect no comment about something related to KDE 4? When you raise four fingers in our KDE world, you always discover a pirhana at each tip.  :-D\n\nBy the way: Very nice work! I can hardly wait to have the whole set. (I'm tired of the Crystal icons.)\n\nAnd, shouldn't the Oxygen icon theme remain \"secret\" until KDE 4's release? Now, these icons won't look so fresh and new in six months...\n"
    author: "Remi Villatel"
  - subject: "Re: Come on!  ;-)"
    date: 2005-11-17
    body: "No comment and more than 80 comments, 3,5GB traffic in 36 hours no :)\nAlso because it's just a preview, but that's good, it's incentivating.\n\nAbout secretiveness, remember this is a preview, it may change a little or even a lot, folders for example are will sure been changed to scale down better, browser action may change as well. And in the end we ar just showing less than 5% of a complete theme."
    author: "David Vignoni"
  - subject: "Um ..."
    date: 2005-11-17
    body: "Eh.\n\nThey're okay. I mean, they're nice. I don't understand some of the raging boners that some guys here have for these icons, e.g., \"they're the most beautiful visual expressions of icon-oriented symbology ever have I laid my eyes upon, O Beautiful Icons, Blessed Art Thou ...\", etc.\n\nI think that's a good example of how KDE has a devoted fan base that, at times, can be one toe over the Line of Rational Thought, but hey, I get it, when you're passionate, you're passionate.\n\nOverall, the icons are all right. Good job."
    author: "Greg McClure"
  - subject: "Nice Work!"
    date: 2005-11-17
    body: "These look very nice.\n\nSo, the first question is: are the actual icons available somewhere?\n\nThe Oxygen theme appears to look more contemporary without the usability issues which I have complained about in the Crystal styles.  So, they have done a good engineering job as well as good artwork.  Since they look so good, perhaps they should be installed as HiColor which would solve various issues including the FD standard compliance.\n\nI do wonder about the MIME type icon though.  I think that the folded upper right corner is standard across many desktops and would be preferred.  also, the shadows on the MIME type icons seem wrong for the standard light source over the viewers left shoulder.\n\nI also wonder if some of them, while the look very nice in the large size, have too much detail and are too photo realistic to render well in 16x16 and 22x22.\n\n22x22 suggests another issue: can we manage to switch to 24x24 (or just add it) in KDE-4?  Other desktops use 24x24 and it creates real problems when optimizing an icon from the 128x128 px SVG."
    author: "James Richard Tyrer"
  - subject: "Re: Nice Work!"
    date: 2005-11-18
    body: "> I do wonder about the MIME type icon though. I think that the folded upper right corner is standard across many desktops and would be preferred.\n\n\n\nI also had this question. It looks as if the folding of a lower corner as opposed to an upper corner was done merely to be contrary, without an eye to why the upper corner is usually folded.\n\nIt seems that, by having the upper corner folded, the bottom is wider and flatter and appears to be the base of the icon. Thus, when in an icon-view in a file browser (the main place where mime-type icons are used), the text naming the file appears to be placed at the base of the icon. Also, as noted, the upper-corner-fold maintains cross-desktop consistency.\n\nOf course, this may have already been addressed inside of the group working on these icons, but I wonder as to their reasoning."
    author: "jameth"
  - subject: "Great work!"
    date: 2005-11-18
    body: "This is very impressive work you guys have done with Oxygen. Hats off to Everaldos Crystal and Clear-E as well as it brought KDE's look and feel for KDE 3 to new levels and was certainly the most successful icon theme in the open source world since it started.\n\nBut KDE 4 needs to have a new look and feel which still has its roots in Crystal and is innovative enough to bring in fresh new air and breaks with some of Crystals \"trends\". I think that Oxygen delivers this exactly.\nOf course there are still lots of rough edges and as someone who has seen the Oxygen presentation at Malaga I can assure that this is just a sneak preview with many more details for technical and usability improvements yet to come.\nRemember that we are still at a very early stage.\n\nFor all the people who worry about openness and license I can assure you the best as well: To initiate a project you always need a small team of people who have a vision and who try to build the base and direction. It was like that for Crystal (which originated in Conectiva's offices). And it was like that for Tango (which was kept secret by NDL's for almost a year inside Novell until it was published). So be patient and give your constructive input to our three artist heros. Oxygen will be opened more and more rapidly just as it worked with all previous iconthemes.\nConcerning license and trademarks KDE e.V. will take care that Oxygen will be licensed under a license which is in the spirit of previous iconsets used by default in KDE.\n\nCheers,\n\nTorsten Rahn"
    author: "Torsten Rahn"
  - subject: "Re: Great work!"
    date: 2005-11-19
    body: "Torsten, I also would like to add that oxygen icons will be realeased with sources (in the form of SVG files) from the beginning, so sure you can say this is an open source project."
    author: "David Vignoni"
  - subject: "Great work, - comment on actions and colours"
    date: 2005-11-26
    body: "Please, be kind with the actions.  I hate having a rainbow up there.  Please consider redesigning the icon if the only method of identification is the colour.  I believe I read somewhere that someone claimed each icon should be identifiable as a greyscale.  Please keep in mind the amount of colours you use in the actions.\n\nThanks!"
    author: "Anonymous Coward"
---
David Vignoni, Kenneth Wimer and Nuno Pinheiro, 3 of KDE's finest artists, are very proud to present the <a href="http://www.oxygen-icons.org">Oxygen website</a>, explaining what Oxygen is and the direction it is going in.  Oxygen is the new icon theme being created for KDE4.  Everything started in March 2005 when a bunch of KDE contributors met in Berlin to form the <a href="http://appeal.kde.org">Appeal Project</a> with the goal to promote KDE related projects and to push the open source desktop to another level. Oxygen aims to bring a modern, cool and very usable and consistent icon theme, in SVG format.  In addition to high quality design, Oxygen also promotes technological innovation and increased usability by proposing new file interaction methods, animated effects and the intelligent use of SVGs.








<!--break-->
