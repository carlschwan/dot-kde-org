---
title: "KDE 3.5 Alpha 1 Finally on FTP"
date:    2005-08-11
authors:
  - "skulow"
slug:    kde-35-alpha-1-finally-ftp
comments:
  - subject: "Wots..."
    date: 2005-08-10
    body: "...noo? ;-)\n"
    author: "Martin"
  - subject: "PC-BSD and DesktopBSD"
    date: 2005-08-10
    body: "I was wondering why good news such a very easy-to-use OSes based on FreeBSD such as PC-BSD and DesktopBSD, which they ship with only one DE: KDE, aren't news here in www.kde.org\nBTW, I think FreeBSD + KDE is a great combo!!! Keep up the good work guys!!!"
    author: "anonymous"
  - subject: "Re: PC-BSD and DesktopBSD"
    date: 2005-08-11
    body: "Um, there was an article on them a while ago."
    author: "Ian Monroe"
  - subject: "Re: PC-BSD and DesktopBSD"
    date: 2005-08-11
    body: "I meant here at dot.kde.org not /."
    author: "anonymous"
  - subject: "Re: PC-BSD and DesktopBSD"
    date: 2005-08-11
    body: "So did Ian, I guess: http://dot.kde.org/1114514258/"
    author: "cm"
  - subject: "Re: PC-BSD and DesktopBSD"
    date: 2005-08-11
    body: "thanks!"
    author: "anonymous"
  - subject: "Re: PC-BSD and DesktopBSD"
    date: 2005-08-11
    body: "But, isn't BSD dead?\n\nYours, Kay"
    author: "Debian Troll"
  - subject: "Re: PC-BSD and DesktopBSD"
    date: 2005-08-11
    body: "Trolls, I love trolls!"
    author: "anonymous"
  - subject: "KDE on DesktopBSD"
    date: 2005-08-10
    body: "\"DesktopBSD is the latest easy to install BSD aimed squarely at the desktop. Installation screen shots. From their site: 'DesktopBSD aims at being a stable and powerful operating system for desktop users. DesktopBSD combines the stability of FreeBSD, the usability and functionality of KDE and the simplicity of specially developed software to provide a system that's easy to use and install.' DesktopBSD joins the ranks of PC-BSD and FreeSBIE.\" - <a href=\"http://bsd.slashdot.org/article.pl?sid=05/08/10/0123222&tid=190&tid=7\">/.</a>.\n\n<a href=http://shots.osdir.com/slideshows/slideshow.php?release=403&slide=17>screenshots</a>"
    author: "chris"
  - subject: "KDE 3.5 Alpha 1 Screenshots"
    date: 2005-08-10
    body: "http://shots.osdir.com/slideshows/slideshow.php?release=407&slide=3"
    author: "chris"
  - subject: "NICE JOB"
    date: 2005-08-11
    body: "well done, KDE developers :D\n\ni've been running SVN for some time, and i really like what i see - KDE 3.5 seems to be progressing nicely. not only feature-wise, but also in stabillity - i rarely encounter problems!"
    author: "superstoned"
  - subject: "Re: NICE JOB"
    date: 2005-08-11
    body: "Hi. I know that there is documentation already on how to obtain KDE code via SVN, but I have to admit that I am still a bit confused with the process. Can you help me by telling me exactly what part of the tree you are fetching? Are you using arts or akode? None of this has been very well documented."
    author: "NeedsHelp"
  - subject: "Re: NICE JOB"
    date: 2005-08-11
    body: "hi!\n\ni use gentoo, so i use the Gentoo SVN ebuilds:\nhttp://forums.gentoo.org/viewtopic-t-331588-postdays-0-postorder-asc-start-0.html\n\nif you don't have gentoo, i can recommend kdesvn-build:\nhttp://kde-apps.org/content/show.php?content=23840\n\nin both cases, arts is build. i use arts, indeed. works fine for me, and its the default KDE multimedia framework... KDE4 might bring something better, tough."
    author: "superstoned"
  - subject: "Re: NICE JOB"
    date: 2005-08-11
    body: "I appreciate you providing this link here. I had not seen it prior to today even though I have seen many other KDE and SVN documents. How possible is it to use these utilities with the trunk or KDE 4 code?"
    author: "Confused"
  - subject: "Re: NICE JOB"
    date: 2005-08-11
    body: "i'm sure it is possible, i think you just have to read the documentation or check the (wiki)website..."
    author: "superstoned"
  - subject: "Re: NICE JOB"
    date: 2005-08-11
    body: "http://quality.kde.org/develop/cvsguide/buildstep.php\n\neven after reading that, you have trouble? They have given which brance to fetch and how.\narts is needed to build even kde-3.5 . that is also written there and I saw it too when building."
    author: "Vinay Khaitan"
  - subject: "kubuntu debs"
    date: 2005-08-11
    body: "Are there any kubuntu debs for those of us who like to live on the bleeding edge?"
    author: "Tim Sutton"
  - subject: "Re: kubuntu debs"
    date: 2005-08-11
    body: "Yeah, would be awesome, alpha1 deb's for kubuntu with full debugging enabled! Would make it easy to help out :d"
    author: "Bart Verwilst"
  - subject: "Re: kubuntu debs"
    date: 2005-08-11
    body: "And if they were installable in parallel to the stable version it would be even better.\n\n"
    author: "cm"
  - subject: "bug no. 1"
    date: 2005-08-11
    body: "bugs.kde.org just died...\nwhere do I submit it :P"
    author: "Mark Hannessen"
  - subject: "Re: bug no. 1"
    date: 2005-08-11
    body: "Well, it's online again. So please bug us!!! ;)"
    author: "Bram Schoenmakers"
  - subject: "Gecko integration"
    date: 2005-08-11
    body: "I can't wait for the first 3.5 RC to come out (it's the point i usually start having courage to compile the beast and do some testing).\n\nI have two questions though:\n\n1 - Is konki gecko integration coming out in 3.5 or will it be delayed until 4.0 comes out? Or was it again alpha-vapor-developer-lost-interest-ware?\n\n2 - I know superkaramba will morph into plasma for 4.0, but the integration will be almost complete in 3.5? example, can i use the kde framework (like the kde proxy) with current karamba themes (liquidweather comes to my mind).\n\nAnyway, if both questions are negatives, it will still be a very good release, at least it will let me have something to hang on in the painfull wait for kde 4.0... ahh so many months till...\n"
    author: "Paulo Dias"
  - subject: "Re: Gecko integration"
    date: 2005-08-11
    body: "i'm afraid the answers to both questions will be negative. superkaramba will just be included in KDE 3.5, not much more integrated as it is now (latest rc-version supports KGHNS). not sure about gecko, but it think you can forget it..."
    author: "superstoned"
  - subject: "Re: Gecko integration"
    date: 2005-08-11
    body: "Yeah, i was suspecting just that, i'm using superkaramba 0.37-rc2 which indeed has support for KGHNS for installing and deinstalling, i guess i'll have to wait for 4.0 :)\n\nGecko on the other hand has been a false promise since the first announcement in the first akademy... it worked as a showcase, but the main developer lost interest, then was asked to finish the work, and that was the last i heard from gecko integration in konki.\n\nThanks for your responde."
    author: "Paulo Dias"
  - subject: "Re: Gecko integration"
    date: 2005-08-12
    body: "You should use for svn, rather than sit around waiting for the RC. Overall I think you'd find current svn more stable than most previous KDE RC's. And you even get the benefit of, in some cases, instant feedback/bugfixes for the problems you report:-)"
    author: "Morty"
  - subject: "Re: Gecko integration"
    date: 2005-08-12
    body: "> Is konki gecko integration coming out in 3.5 or will it be delayed until 4.0 comes out? Or was it again alpha-vapor-developer-lost-interest-ware?\n\nFirefox/Qt is being worked on (Mozilla Bugzilla #297788) which will likely also bring Gecko integration into Konqueror. But don't expect it to ship as part of a KDE release when it's ready."
    author: "Anonymous"
  - subject: "Re: Gecko integration"
    date: 2005-08-12
    body: "nice to hear that, i don't need it to be part of a kde release, since i know my way around cvs/svn and i always compile my own kde :)\nI'll be watching for the mozilla bugzilla then. Thanks for the feedback :)"
    author: "Paulo Dias"
  - subject: "KDE 3.5Alpha1"
    date: 2005-08-11
    body: "Hi,\n\nI'm posting this from the klax live cd :-)\n\nFirst a question: Is it compiled with gcc4 and -fvisibility-hidden?\n\nMy first impression (I might be wrong) is that, optically and functionally\nnot many changes are visible. Maybe it's under the hood...\n\nNote some personal observations (fully aware this is alpha, just FYI):\n\n- \"Add Applet to panel\" shows many applets twice\n  Same for Configure KDE panel->Menus->Optional Menus\n- KDE apps still flicker more than gnome apps :-(\n  Switch tabs and have konqueror scrollbars flicker\n  Open new tab and have complete tabbar flicker\n  ...hopefully Qt4 will help\n- The fonts look great\n- The plastik window decoration looks refreshened, due to the white symbols\n- konqueror is a great browser\n- RMB on kicker->lock panel is great, but too restricted. it should only lock\n  the panel, and not it's config possibility. hmm...\n- the applet handles should go to /dev/null\n  Applets should be moveable with ALT-Mouse. That would render the handles obsolete\n- konqueror adblock is cool, but konq is getting featuritis...\n  even on fullscreen (1280x960) I have to scroll in the settings dialog's left iconbar\n\nCongrats to the KDE devs, I can't wait for 3.5 and I personally hope it will be in debian sid as soon as its released :-)\n\n\n\n\n"
    author: "ac"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-11
    body: "... and I forgot, thanks to you Stephan for making klax ! :-)"
    author: "ac"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-11
    body: "add.:\n\nurls that never load in konqueror:\n\nhttp://www.spiegel.de/\nhttp://www.sueddeutsche.de/\nhttp://focus.msn.de/\nhttp://www.wort.lu/"
    author: "ac"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-11
    body: "confirmed :( khtml rendering problem."
    author: "fast_rizwaan"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-12
    body: "All loaded and displayed nicely(no obvious missrendering) using svn 20050806.\n\nPersonally I think the poor release dude had really bad luck when tagging the alpha, and got a a snapshot of svn in the worst shape it has been for weeks. In the last two months I have had nearly no issues at all with svn, some annoyances but nothing bad. Sometimes I have encountered build problems, but it's expected with . The worst problem have been couple of apps crashing on exit(Konqueror and Kicker), and that's really not a big problem:-)"
    author: "Morty"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-12
    body: "> First a question: Is it compiled with gcc4 and -fvisibility-hidden?\n\nNo, Slackware is still gcc 3.3 based.\n\n> - \"Add Applet to panel\" shows many applets twice\n\nYou can also see the entries in khelpcenter being listed twice. Afaik it's considered a bug within unionFS (which allows the modules feature and you to virtually write on running LiveCD like installing more software). Newer Knoppix releases with a stable KDE show it too. :-("
    author: "Anonymous"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-12
    body: "Actually, that's a problem with normal KDE (3.4.x and ealier) on a normal install: if I change an application's properties, the new .desktop file gets stored locally under ~/.kde and I get to see two identically named entries in the K-menu.  That's stupid and confusing; I don't know which is which.  Duplicates should not be allowed (use name as key), and locally defined entries should take preference over the system ones.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: KDE 3.5Alpha1"
    date: 2005-08-12
    body: "- KDE apps still flicker more than gnome apps :-(\n Switch tabs and have konqueror scrollbars flicker\n Open new tab and have complete tabbar flicker\n ...hopefully Qt4 will help\n-------------------\n\nthe switch-tab-flicker is fixed already. the flickering of KDE-apps will most likely be fixed with Qt4, yes. esp the hardware acceleration will help, i think...\n"
    author: "superstoned"
  - subject: "Now just if klax would move to X11R7 with exa"
    date: 2005-08-13
    body: "As klax seem to be on the cutting-edge, where's xorg-server-0.99.1+?\n\nWould be really nice to be able to test those exa and other eye-candy extensions (and would run fine with r300+, heh) with such unstable distro, and not to be forced to either wait 3+ months for some stable distribution release or by trying to compile the whole thing from scratch...\n"
    author: "Nobody"
  - subject: "Re: Now just if klax would move to X11R7 with exa"
    date: 2005-08-13
    body: "Klax base is Slackware which is not cutting edge, see gcc comment."
    author: "Anonymous"
  - subject: "PROBLEM!! with Klax alpha live cd"
    date: 2005-08-21
    body: "Hi i downloaded Klax 3.5 alpha live cd to try out KDE 3.5\nfrom:\n\nwget http://ktown.kde.org/~binner/klax/klax-kde-3.5-alpha.iso\n\nthen i check the *.md5\n\n\nwget http://ktown.kde.org/~binner/klax/klax-kde-3.5-alpha.iso.md5\n\n[real@genio Klax]$ md5sum klax-kde-3.5-alpha.iso\n9ee62eab17cf78da06ad278e3bd426d5  klax-kde-3.5-alpha.iso\n\n[real@genio Klax]$ cat klax-kde-3.5-alpha.iso.md5\n9ee62eab17cf78da06ad278e3bd426d5  klax-kde-3.5-alpha.iso\n\n\nthen i burned the cd, when i boot the cd i keep on getting KERNEL PANIC, then y try all the options boot (F1) and keep getting the same KERNEL PANIC...\n\nMore details of my problem here:\n\nhttp://www.powers.cl/real/index.php?subaction=showfull&id=1124640396&archive=&start_from=&ucat=9&\n\n\nDid someone got a similar problem? or is just a fuck*ng bug?\n\nGO KDE GO!!! =)"
    author: "REAL"
  - subject: "Re: PROBLEM!! with Klax alpha live cd"
    date: 2005-08-21
    body: "Did you verify the burned CD? Tried to rewrite/burn another?"
    author: "Anonymous"
---
To begin the <a href="http://developer.kde.org/development-versions/kde-3.5-release-plan.html">KDE 3.5 release cycle</a>, I <a href="http://download.kde.org/download.php?url=unstable/3.5-alpha1/src">uploaded KDE 3.5 Alpha 1</a> to the FTP servers. We're facing some trouble that is typical for an Alpha release, but it also brings some nice KDE 3.5 features to your desktop. The easiest way to try it is the <a href="http://ktown.kde.org/~binner/klax/devel.html">KDE 3.5 Alpha Live CD</a> or compile it yourself with the <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> build script.



<!--break-->
<p>
But compile it only if you have the place and the time to help with debugging it (pass --enable-debug to "configure"). Please report bugs to <a href="http://bugs.kde.org/">bugs.kde.org</a> and feel welcome to join the <a href="http://quality.kde.org/">KDE Quality team</a> to improve its quality in general. And don't forget: it might be the last KDE 3 Alpha release you have the chance to try - don't let the chance pass by!</p>




