---
title: "Cooking with Linux: Forgotten Security"
date:    2005-03-01
authors:
  - "mgagn\u00e9"
slug:    cooking-linux-forgotten-security
---
<a href="http://www.marcelgagne.com/cwl012005.html">This article</a>, part of the Cooking with Linux series, covers several security related topics starting with steganography (take any message, encode it inside another message or a graphic image). The article then discusses password safes (so you don't have to remember a thousand passwords), including a detailed tutorial on KDE's own KWalletManager.
<!--break-->
