---
title: "KDE's Switch to Subversion Complete"
date:    2005-05-05
authors:
  - "jriddell"
slug:    kdes-switch-subversion-complete
comments:
  - subject: "tags/ and branches/ incomplete"
    date: 2005-05-05
    body: "Those are missing some files (it's being worked on to re-add them) so better don't use Subversion now for anything but trunk/."
    author: "Anonymous"
  - subject: "branches/ now fixed"
    date: 2005-05-11
    body: "branches/KDE/3.4/ works again, tags/ is still broken."
    author: "Anonymous"
  - subject: "tags/ now also fixed"
    date: 2005-05-12
    body: "http://lists.kde.org/?l=kde-core-devel&m=111588685516801&w=2"
    author: "Anonymous"
  - subject: "i18n?"
    date: 2005-05-05
    body: "If see in trunk/KDE/kde-i18n structures like\ntrunk/KDE/kde-i18n/de/messages/kdeextragear-3/\nBut in svn kdeextragear-3 is not anymore, digikam stable is e.g. in\ntrunk/extragear/graphics/\nWill i18n be redesigned?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: i18n?"
    date: 2005-05-05
    body: "There will be several stuff (eg kdenonbeta, www) moved once everything is running fine."
    author: "Anonymous"
  - subject: "websvn"
    date: 2005-05-05
    body: "Clicking View and Diff to previous doesn't seem to work at websvn.kde.org."
    author: "Ryan"
  - subject: "Re: websvn"
    date: 2005-05-05
    body: "It was said in the kde-cvs-announce mail which announced websvn.kde.org that this doesn't work yet and will be worked on."
    author: "Anonymous"
  - subject: "Re: websvn"
    date: 2005-05-05
    body: "I should have read my email more closely. ;)"
    author: "Ryan"
  - subject: "Re: websvn"
    date: 2005-05-06
    body: "This seems to be working now."
    author: "Anonymous"
  - subject: "Graphical front end to svn?"
    date: 2005-05-05
    body: "Does this mean that there will be a graphical KDE front end to SVN, perhaps as a plugin to Konqueror? I'm thinking something similar to the TortoiseSVN plugin to Windows Explorer. =)"
    author: "Kenneth"
  - subject: "Re: Graphical front end to svn?"
    date: 2005-05-05
    body: "You must mean like <a href=\"http://www.kde.org/apps/cervisia/\">Cervisia</a>\n"
    author: "AnonymousCoward"
  - subject: "Re: Graphical front end to svn?"
    date: 2005-05-05
    body: "It's not quite the same thing. Cervisia is fine, but tortoise got very clear differences if you ever used it. Cervisia's kpart for example won't work on normal non-cvs folders (it won't display your files),  the gui in cervisia doesn't match that of the normal file browser (it's a reimplementation, rather than an extension), etc... Tortoise is more integrated on the file browser than cervisia is, which is more like a separate application."
    author: "uga"
  - subject: "Re: Graphical front end to svn?"
    date: 2005-05-05
    body: "Of course I got nothing against Cervisia. It's pretty nice application. Just pointing out they're different things"
    author: "uga"
  - subject: "Re: Graphical front end to svn?"
    date: 2005-05-05
    body: "The most logical place to look for something like this is of course kde-apps.org. There you would have found KSvn (http://kde-apps.org/content/show.php?content=23411)."
    author: "ac"
  - subject: "Re: Graphical front end to svn?"
    date: 2005-05-05
    body: "A pretty good one is eSvn, a Qt-Frontend."
    author: "anaon"
  - subject: "Anonymous SVN"
    date: 2005-05-05
    body: "Does anyone think that these instructions:\n\nhttp://developer.kde.org/source/anonsvn.html\n\ncould use a little more work?\n"
    author: "James Richard Tyrer"
  - subject: "Re: Anonymous SVN"
    date: 2005-05-05
    body: "Where's your diff? And don't forget the other places on developer.kde.org."
    author: "Anonymous"
  - subject: "Re: Anonymous SVN"
    date: 2005-05-05
    body: "Looks like a bad attempt to replace the letters cvs with svn:-) Does it actually work with those instructions, replacing the remaining cvs references?\n\nIs the Anonymous SVN operational at all yet?"
    author: "Morty"
  - subject: "Re: Anonymous SVN"
    date: 2005-05-05
    body: "Yes the second commnd (using \"svn\") is running now:\n\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/KDE/kdelibs \n\nand appears to be working.\n\nNote that aRts is a different place, so the command is:\n\nsvn checkout svn://anonsvn.kde.org/home/kde/trunk/KDE/arts\n\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Anonymous SVN"
    date: 2005-05-05
    body: "Great, going to try it out in a few days. Going to use some time doing some spring cleaning on my HD first, besides I don't mind waiting for the traffic on svn server to decrease some. "
    author: "Morty"
  - subject: "Now, everything and time is now for KDE4"
    date: 2005-05-05
    body: ";-)\n"
    author: "anonymous"
  - subject: "Re: Now, everything and time is now for KDE4"
    date: 2005-05-06
    body: "How about KDE 3.4.1 and a Qt4 release which doesn't change its API anymore first?"
    author: "Anonymous"
  - subject: "Re: Now, everything and time is now for KDE4"
    date: 2005-05-06
    body: "As the API of Qt changes from Qt3 to Qt4, the first KDE for Qt4 will have a changed API, so or so. So probably it is better to do the full way and make all necessary changes for KDE 4.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Now, everything and time is now for KDE4"
    date: 2005-05-06
    body: "You didn't understand: you don't want to port KDE to a moving target. Qt 4 Beta 2 was said to be the end of (incompatible) API changes but it seems that Trolltech is still making changes (http://www.kdedevelopers.org/node/view/1005)."
    author: "Anonymous"
  - subject: "38 hours on what system"
    date: 2005-05-05
    body: "The conversion lasted for 38 hours, but what system performed the job?\nHow GHz and GB does the conversion system have?"
    author: "Richard"
  - subject: "Re: 38 hours on what system"
    date: 2005-05-06
    body: "I've seen a 2TB CVS archive converted to sv and it took almost 20 hour using a dual usparc2 and 8gb ram, so i guess 38h is pretty good, considered kde consists of many small commits."
    author: "Andy"
  - subject: "Re: 38 hours on what system"
    date: 2005-05-06
    body: "The admins aren't at liberty to say what the specs were, but here's some food for thought:\n\nFirst they said it would take a whole week to convert. Then they got this new machine that could do the job in 12 hours (and actually took 38h).\n\nOn one post to kde-core-devel, Coolo posted an image of an icecream network chart that included this machine. It was represented by a big purple blob, much larger than anything else in the chart.\n\nAnd finally, I am positively sure it isn't anything x86. The thing had more RAM than x86 can possibly address (1TB, if I remember correctly).\n\nBy the way, our archive is now 15GB in Subversion."
    author: "Thiago Macieira"
  - subject: "version of subversion"
    date: 2005-05-06
    body: "and what is the version of svn - 1.2 or 1.1?"
    author: "Nick"
  - subject: "Re: version of subversion"
    date: 2005-05-07
    body: "Subversion 1.2 is not even released!"
    author: "Anonymous"
  - subject: "SVN TRUNK buildscripts"
    date: 2005-05-06
    body: "I've put up my very personal KDE SVN trunk buildscripts (changed from CVS). In case someone is interested feel free to grab and test them that's how I use to build my kde here:\n\nhttp://www.akcaagac.com/tools/files/shell/getkde.sh\nhttp://www.akcaagac.com/tools/files/shell/kdemake.sh"
    author: "oGALAXYo"
  - subject: "Gentoo Ebuilds"
    date: 2005-05-06
    body: "Gentoo ebuilds available at: http://csua.berkeley.edu/~mtanev/kde-svn.html"
    author: "Mario"
  - subject: "Document the Conversion?"
    date: 2005-05-07
    body: "Would the architects of the conversion mind writing a short article on how they went about getting ready for such a conversion, and possibly tell us about some of the gotchas that came up?  Your experiences could be of significant value to other large (or small) projects thinking about making the move to subversion.\n\nIf such a document already exists, could someone point me in the proper direction?\n\nThanks."
    author: "brian"
  - subject: "Great! :)"
    date: 2005-05-08
    body: "This is good news. I'm much more fluent with SVN than with CVS, and I've always been a fan of the alternate version control system :)"
    author: "David House"
---
The conversion of KDE's source repository from CVS to Subversion is now complete.  All KDE developers with CVS accounts now have Subversion accounts.  To find out how to use your new Subversion account read the <a href="http://developer.kde.org/documentation/tutorials/subversion/">Using Subversion with KDE tutorial</a>.  To checkout anonymously use svn://anonsvn.kde.org/home/kde/ as the base of your Subversion repository URL.  You can browse the repository through the web at <a href="http://websvn.kde.org/">http://websvn.kde.org/</a>.














<!--break-->
<p>This is the largest ever change from CVS to Subversion.  The conversion script ran for a total of 38 hours from start to completion.  Congratulation to Stephan Kulow, Oswald Buddenhagen and the other system administrators for the successful change.</p>

<p>KDE's family of websites are now managed and updated from the Subversion archive.  The CVS archive itself still exists in read-only mode.  Every developer now needs to do a fresh checkout of their KDE sources.  While the server is still operating under heavy load you may wish to start with <a href="http://ktown.kde.org/~coolo/svn/">these pre-checked-out archives of trunk/HEAD</a> rather than checking out directly from Subversion.</p>

<p>Subversion offers many advantages over CVS while remaining similar enough to use that it should be easy for existing users to learn.  Changes are now made with a single revision number per-commit rather than per-file.  It also offers the ability to move files &amp; directories and makes it easier to work with branches.</p>

<p><a href="http://people.kde.org/stephan.html">Stephan's People Behind KDE interview</a> includes some history of KDE's CVS.  The first ever CVS commit was the import of kdelibs by Stephan on 13 April 1997 (Subversion revision number 2).  The <a href="http://lists.kde.org/?l=kde-cvs&m=111504952916672&w=2">last active commit to CVS</a> was an update to the maps on <a href="http://worldwide.kde.org">KDE Worldwide</a> (Subversion revision number 409201).  The <a href="http://lists.kde.org/?l=kde-cvs&m=111519335623926&w=2">first of many commits to Subversion</a> was to the kde-build script.</p>






