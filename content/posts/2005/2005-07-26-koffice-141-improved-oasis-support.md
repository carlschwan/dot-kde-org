---
title: "KOffice 1.4.1 with Improved OASIS Support"
date:    2005-07-26
authors:
  - "binner"
slug:    koffice-141-improved-oasis-support
comments:
  - subject: "Icons"
    date: 2005-07-26
    body: "Is KDE going to use the OO icons for the Oasis MIME types?\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Icons"
    date: 2005-07-26
    body: "Probably not. Even the old OOo mime types had more KOffice-oriented icons, (modulo bug #95123).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Icons"
    date: 2005-07-27
    body: "Then somebody needs to decide if we are going to use the current KOffice MIME type icons or if we need to request new ones.  The current situation is as stated in that bug.  The desktop files have names of non-existant icons.  The KOffice MIME icons I made are associated with the KOffice apps so we really need new ones that are associated with the Oasis file types.\n\nWhen somebody determines this, please post to kde-artists list, or discuss it there."
    author: "James Richard Tyrer"
  - subject: "thx for koffice"
    date: 2005-07-26
    body: "thx for your work in koffice !"
    author: "chris"
  - subject: "Hopefully OpenDocument support works better now."
    date: 2005-07-26
    body: "The last test of OpenDocument interoperability done at\nhttp://www.heise.de/newsticker/foren/go.shtml?read=1&msg_id=8411045&forum_id=81694 failed misserably.\nhttp://www.students.uni-mainz.de/pwuertz/odt/ contains the test document and the screenshots."
    author: "testerus"
  - subject: "Re: Hopefully OpenDocument support works better now."
    date: 2005-07-26
    body: "Yeah I saw similar stuff when inporting GNUMERIC datasets with KSpread. The tables looked wrong, colors not set correctly, borders missing and and and. But then I believe that it's just a matter of time and manpower once these issues are worked out."
    author: "ac"
  - subject: "Re: Hopefully OpenDocument support works better now."
    date: 2005-07-26
    body: "Not necessarily KOffice is always to blame. For example OOo released a Beta which wrote non-final OpenDocument standard documents."
    author: "Anonymous"
  - subject: "Re: Hopefully OpenDocument support works better no"
    date: 2005-07-26
    body: "Thats why it is called BETA."
    author: "testerus"
  - subject: "Re: Hopefully OpenDocument support works better no"
    date: 2005-07-26
    body: "Which unfortunately doesn't prevent distributions such as SUSE or Fedora to ship it as default office application."
    author: "Anonymous"
  - subject: "Re: Hopefully OpenDocument support works better no"
    date: 2005-07-31
    body: "Fedora again is Red Hat Beta.\nAnd IIRC Suse is Novell Beta."
    author: "Ritesh Raj Sarraf"
  - subject: "Re: Hopefully OpenDocument support works better no"
    date: 2005-08-01
    body: "SUSE is a full retail product."
    author: "Anonymous"
  - subject: "Where is the KLAX package?"
    date: 2005-07-26
    body: "Thanks binner for the release info. But I need the koffice-1.4.1-i486-klax.tgz package. could you please provide the link, since it can be used in Slackware 10.1 easily, else I've to wait a week or so to get it for slackware ;) TIA."
    author: "fast_rizwaan"
  - subject: "Here is the  KLAX package for Slackware 10.1?"
    date: 2005-07-26
    body: "Download Koffice 1.4.1 for slackware 10.1:\n\nhttp://ktown.kde.org/~binner/klax/10.1/koffice-1.4.1-i486-klax.tgz"
    author: "fast_rizwaan"
  - subject: "[OT] KDE 3.4.2"
    date: 2005-07-26
    body: "Wednesday July 27th, 2005: Targeted 3.4.2 Release date, right brothers?"
    author: "fast_rizwaan"
  - subject: "Re: [OT] KDE 3.4.2"
    date: 2005-07-26
    body: "Targeted, right. Actual, may differ."
    author: "Anonymous"
  - subject: "SuSE 9.2 packages, anyone?"
    date: 2005-07-26
    body: "Just checked the mirrors and looked for packages for ix86 / SuSE 9.2, they only list packages for 9.3\nDid any kind soul try to install the 9.3 packages on a 9.2 box with success?"
    author: "Robert"
  - subject: "KOffice is beginning to rock!"
    date: 2005-07-26
    body: "I just woke up to a new and excellent Koffice release. KDE is beginning to provide every single app that I need for work.\n\nKoffice is beginning to rock pretty hard. Krita is a joy to work with. It may not have everything the GIMP has, but the discoverability of its features and its workflow is so much better.\n\nKword is finally beginning to be really stable and Kpresenter is incredible too. (I am not as impressed with Kspread right now, sorry). \n\nI just mention these apps because they are the ones I use the most. If KOffice ever gets perfect document exchange with OpenOffice, and I feel like we are making tremendous progress, it will be able to replace OpenOffice at our non-profit for 90 percent of the people.\n\nKeep the good stuff coming and a big thanks to all of the KDE devs.\n\nHere in the trenches, I can tell you that my users love KDE 3.4 to the point that they bring their Windows computers from home because they want to have the nice KDE environment that they have a work.\n\nWe may not have the one killer app, but we have a killer desktop environment. I can only wait for KDE 4.0 and Plasmma and Tenor.\n\nThanks again."
    author: "Gonzalo"
  - subject: "Re: KOffice is beginning to rock!"
    date: 2005-07-26
    body: "Has kword improved table support ? last time I checked, tables \nwere horrible to work with.\n\n  so also was pdf support.  The pdf files generated from koffice \napplications do not have good quality, bad looking print outs etc.\n\nPeople would love to use koffice for its lightness and integration.\nyet coming to functionality it is not as good sometimes\n\nAsk"
    author: "Ask"
  - subject: "Re: KOffice is beginning to rock!"
    date: 2005-07-26
    body: "My pdfs look just fine. The table support is there and functional and there are some basic templates.\n"
    author: "Gonzalo"
  - subject: "Re: KOffice is beginning to rock!"
    date: 2005-08-01
    body: "There is always the possibility to embed KSpread into KWord, so there is no real reason to reimplement all table functionality in KWord.\n\nIMHO basic table support in KWord is enough. Complex tables should be done with KSpread embedding."
    author: "Raphael Langerhorst"
  - subject: "Congratulations!"
    date: 2005-07-26
    body: "Congratulations to all the KOffice developers. \nThanks for your work!"
    author: "Dario Massarin"
  - subject: "Ghosting of pictures"
    date: 2005-07-28
    body: "One thing I just noticed is that whenever you move a picture frame around, there is ghosting on kword's screen and it only goes away when you minimize and maximize the application.\n\nIs there a way to avoid this ghosting?"
    author: "Gonzalo"
  - subject: "Re: Ghosting of pictures"
    date: 2005-07-29
    body: "Please submit a bug report (if it hasn't been reported earlier). That way all KOffice devs will see it.\nWith some luck, the fix will be in the next release then (only if it's easy-to-fix ;))"
    author: "Bram Schoenmakers"
  - subject: "oasis default"
    date: 2005-07-29
    body: "Is koffice going to make oasis the default saving format? Or is their going to be worked on the own format as well?"
    author: "Terracotta"
  - subject: "Re: oasis default"
    date: 2005-07-29
    body: "Yes, with next version/KOffice 2.0."
    author: "Anonymous"
---
The <a href="http://www.koffice.org/">KOffice</a> team today <a href="http://www.koffice.org/announcements/announce-1.4.1.php">announced the  first bugfix release</a> of the 1.4 series. Among various bugfixes and translation improvements, the KOffice 1.4.1 release further improves support for the <a href="http://en.wikipedia.org/wiki/OpenDocument">OASIS OpenDocument</a> file format. More changes are listed in the <a href="http://www.koffice.org/announcements/changelog-1.4.1.php">changelog</a>. <a href="http://www.koffice.org/releases/1.4.1-release.php">Binary packages</a> for Kubuntu, Mandriva and SUSE have been contributed and the <a href="http://ktown.kde.org/~binner/klax/koffice.html">KOffice Live-CD</a> has been updated.







<!--break-->
