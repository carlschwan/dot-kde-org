---
title: "KDE 3.5 Release Candidate 1"
date:    2005-11-12
authors:
  - "binner"
slug:    kde-35-release-candidate-1
comments:
  - subject: "I'd like to help out"
    date: 2005-11-12
    body: "This time I want to help out but I'd like to know whether I can get back to my good old KDE 3.4.3 when things do not work out with the KDE 3.5 rc1. I have 1 machine and a single hard disk. I am running Kubuntu Breezy and have done lots of customizations done. If this is not possible, what can I do to mitigate the mess I might find myself in, in case I take the plunge?"
    author: "This time I want to help out..."
  - subject: "Re: I'd like to help out"
    date: 2005-11-12
    body: "I did install beta2 and it was too buggy for me so I downgraded  to 3.4.x. This is how I did it:\n\napt-get remove libarts1c2 kubuntu-desktop\n\nremoved beta2 lines from sources.list\n\napt-get install kubuntu-desktop\n\nand it worked"
    author: "Patcito"
  - subject: "Re: I'd like to help out"
    date: 2005-11-12
    body: "There are two ways to be safe:\n\n1) Backup your important data;\n\n2) Stick to official Kubuntu packages.\n"
    author: "Reply"
  - subject: "Re: I'd like to help out"
    date: 2005-11-12
    body: "If you want to be sure you have to use a virtual machine where you set up a second Linux but with KDE 3.5, or you have to install KDE 3.5 only local, which is possible when you use konstruct or kdesvn-build.\n\nI personally had nice experiences with konstruct:\nhttp://liquidat.blogspot.com/2005/08/kde-35-alpha-1.html\n\nBut after that I tried kdesvn-build, and that was even better:\nhttp://liquidat.blogspot.com/2005/08/another-kde-building-tool.html\n\nRegards,\n\nliquidat"
    author: "liquidat"
  - subject: "Re: I'd like to help out"
    date: 2005-11-12
    body: "With kubuntu you can use the package debootstrap to install KDE3.5 and keeping KDE3.4.3"
    author: "Cyrille Berger"
  - subject: "Re: I'd like to help out"
    date: 2005-11-12
    body: "I just use kontruct, but let it install to another diretory, eg /opt/kde3.5rc1.\nFor my testing I use a fresh account, so the customizations and data of my 'normal' account don't get messed up.\n\nIf all runs well and I decide to use the new version day-to-day, I rename .kde in my 'normal' users home, then log in the new kde-version and copy back the data (bookmarks, etc.).\n\nAlfred"
    author: "Alfred"
  - subject: "Live ISO"
    date: 2005-11-12
    body: "There will be the Live Klax ISO to test it?\n\nIf yes, is there a (possibly simple) way to boot an ISO from HD, without burning it to CD/DVD?\n\nThanks!"
    author: "Alessandro"
  - subject: "Re: Live ISO"
    date: 2005-11-12
    body: "to my knowledge there is currently no such way.\n\nif its about the cost/waste than a cd-rw help out nice for the live iso testing.\n\n_c."
    author: "cies breijs"
  - subject: "Re: Live ISO"
    date: 2005-11-16
    body: "The trouble is that my notebook can only read CD/DVDs, but not write them :-("
    author: "Alessandro"
  - subject: "Re: Live ISO"
    date: 2005-11-12
    body: "Vmplayer can do that, if in a non-free way.\n\nThere are guide around on how to change the browser-thing to any OS you want, including live cds (I have a knoppix vm image on my disk atm :P)\n"
    author: "Narg"
  - subject: "Re: Live ISO"
    date: 2005-11-16
    body: "Thanks, but I need a copy of vmware to create the vm to launch with vmplayer, is it right?"
    author: "Alessandro"
  - subject: "Re: Live ISO"
    date: 2005-11-12
    body: "Check out QEMU. Homepage: http://fabrice.bellard.free.fr/qemu/\nYour distro probably packages it. Start the cdrom with:\n\nqemu -cdrom cd_image_file.iso -boot d\n\nman qemu for more information :)"
    author: "Anonymous Coward"
  - subject: "Re: Live ISO"
    date: 2005-11-16
    body: "Thanks, I'll try this!\nIt seems promising..."
    author: "Alessandro"
  - subject: "Re: Live ISO"
    date: 2005-11-13
    body: "It seems the Live CD is now available."
    author: "Anonymous"
  - subject: "Re: Live ISO"
    date: 2005-11-16
    body: "Yes, you are right.\nThanks!"
    author: "Alessandro"
  - subject: "Re: Live ISO"
    date: 2005-11-16
    body: "What I'm looking for, is a way to boot from an ISO like you boot from a partition with GRUB, Lilo, Smart Boot Manager or others.\nSomething like LOADLIN could be nice too.\n\nAdvantages are:\n- you don't have to burn the CD/DVD\n- you can use it without a CD/DVD burner and even without a CD/DVD reader!\n- reading from HD is much faster than reading from CD or DVD.\n\nThanks for every answer."
    author: "Alessandro"
  - subject: "Not Ready."
    date: 2005-11-12
    body: "As long as slave forwarding doesnt work.\n\nTry opening a .tar.bz2 in media:/whatever. It doesnt work.\n\nOr is it planned for KDE4 only?"
    author: "PT"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "Gosh, you're correct, this is really an issue since IOSlaves are going to be the default to navigate through your file system in KDE 3.5 (think of home:/). There is definitely the problem of appending one IOS to another, and the new default will lead to lots of complains, I bet."
    author: "Davide Ferrari"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "this still doesn't work? i thought it got fixed... at least you can now burn cd's from media:/ directly with k3b..."
    author: "superstoned"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "Not exactly the same, this is about using ioslaves in media:/. If you have Ark installed it will handle the compressed file seamlessly, but kio_tar etc will not. \n\nPersonally I think the concept of the whole system:/ and media:/ thing are broken, the k3b issues demonstrated it nicely. Inconsistencies keep popping up all over the place requiring constant fiddling with the implementation. And even if you neglect the inconsistency issues, the end result still does not improve usability very much. Which was the whole purpose of the thing."
    author: "Morty"
  - subject: "Re: Not Ready."
    date: 2005-11-14
    body: "Well, IMO the idea of abstracting from the regular Unix filesystem is a great one, don't get me wrong, but while Kevin put a lot of effort into it. it's something that has should be postponed for KDE4...it's quite a radical change and as you said the process it's leaving quite a few inconsistencies (being the KIOs append issue one of the most noticeble).\nIMO now in RC1 time is even too late to step back, it will lead to even worst problems IMO, so let's hope in the 3.5.x series for some bugfixes and definitely to KDE4 for a major revamp."
    author: "Davide Ferrari"
  - subject: "Re: Not Ready."
    date: 2005-11-15
    body: "Yeah, the implementation needs a lot of work.  In its current state, it is a huge step backwards from standard paths.  For example, take this common task (I just ran into this yesterday)\n\nIn kubuntu, plug in a Digital camera, and the media://camera pops up in konqueror.  So far so good.  Now I want to send a picture to a contact on MSN through Kopete.  So logically I go Send file - navigate to the media://camera directory, and choose my picture.  But then kopete says that I cannot choose remote files to send.  Huh?  Remote files?  It's on my camera!  Now I know enough to copy the pics to the hard drive first, but a normal user would be stumped at this point.  I really don't understand why we can't let the distro mount the camera under /media/camera or whatever and open that dir in konqueror when a camera is plugged in.  Would make everything much more consistant."
    author: "Leo"
  - subject: "Re: Not Ready."
    date: 2005-11-15
    body: "Another example;\n\nIf I put a movie-dvd in the player, it's mounted and shown as media:/hdc, in the media:/-view it's impossible to view the dvd-content (files) in Konqueror, I need to manually go to the mount-path in Konqueror, to be honest this is quite annoying."
    author: "LB"
  - subject: "Re: Not Ready."
    date: 2005-11-16
    body: "Leo:\n\"it is a huge step backwards from standard paths.\"\n\n\"I really don't understand why we can't let the distro mount the camera under /media/camera or whatever and open that dir in konqueror when a camera is plugged in. Would make everything much more consistant.\"\n\nLB:\n\"in the media:/-view it's impossible to view the dvd-content (files) in Konqueror. to be honest this is quite annoying.\"\n\nI can't say anything other than I agree with Leo and LB, the only thing actually achieved by trying to abstract the regular Unix paths with this are making everything less consistent. And having to do lots of work hunting all the different kind of use cases and make them work. And rather than creating better usability, it only creates a inconsistent mess. Introducing another naming scheme for the users to learn. And it's at best only marginally better usability wise, and even useless for legacy applications. IMHO the whole idea should have undergone a real usability review, with lots of why's asked."
    author: "Morty"
  - subject: "Re: Not Ready."
    date: 2005-11-28
    body: "This is because kopete is detecting the media:// as a remote protocol just like http:// in a web browser. And therefore its upto kopete to modify its code to allow this as a local protocol. The other option would be to take you back to the standard path, but it's more about time to get the new system running consistently with other apps."
    author: "Ewan Marshall"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "Is there already a bug report for this? If yes, can you give the number. (Thank you!)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "http://bugs.kde.org/show_bug.cgi?id=73821"
    author: "Michael Jahn"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "Bug #73821 is more general and can probably not be implemented in KDE3.\n\nBut media: often handles local files and so they should be given to a tar: (or zip:) KIO slave.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Not Ready."
    date: 2005-11-15
    body: "I reported it in august:\n\nhttp://bugs.kde.org/show_bug.cgi?id=110951\n(or bug:110951 for the ALT+F2 lovers out there).\n\nAnd its not even confirmed yet.\nWas I too vague in the bug description?"
    author: "PT"
  - subject: "Re: Not Ready."
    date: 2005-11-12
    body: "huh.  Zero problems with that here.  I can go to media:/sdb1 or whatever and open all the zip, tar.gz or tar.bz2 files I want.  (this in KDE 3.4.2).  Is there some more specific case you can give me that should fail?"
    author: "MamiyaOtaru"
  - subject: "Re: Not Ready."
    date: 2005-11-14
    body: "Maybe you should try using the release that is supposed to be broken, ie KDE 3.5, instead of a completely different version?"
    author: "kundor"
  - subject: "kdelibs3-arts rpm not available for SUSE 9.3"
    date: 2005-11-12
    body: "can't find it anywhere (though it's available for Suse 10.0) and kdelibs3-devel-3.5.0-2 is dependent on it.\n\nAny ideas? Anyone else experiencing same?"
    author: "Bill"
  - subject: "Re: kdelibs3-arts rpm not available for SUSE 9.3"
    date: 2005-11-12
    body: "You can ignore this wrong dependency (--nodeps)."
    author: "Anonymous"
  - subject: "KDE 3.5 beta is rock solid"
    date: 2005-11-12
    body: "I've been using KDE 3.5beta1 and beta2 as soon as they were out and to me they are just as rock solid as KDE 3.4.2 the last 3.4 I've been using. I think 3.5rc1 must be even more polished, and must have some other niceties worth upgrading."
    author: "Me"
  - subject: "Re: KDE 3.5 beta is rock solid"
    date: 2005-11-12
    body: "KDE 3.5 went into feature-freeze before Beta 2 so RC 1 will hardly have new niceties."
    author: "Anonymous"
  - subject: "Re: KDE 3.5 beta is rock solid"
    date: 2005-11-12
    body: "but it's sill possible. The new style for Kopete was commited because it is like data file, not a new feature in c++.  "
    author: "Johann Ollivier Lapeyre"
  - subject: "Re: KDE 3.5 beta is rock solid"
    date: 2005-11-13
    body: "Intrestingly the Ikons of left to the Kopete window show the problems of the current Icon scheme. they depict 3 dimensional objects with different angles."
    author: "Andre"
  - subject: "klik:/ packages?"
    date: 2005-11-12
    body: "If any klik:/ packages would be made available for testting that would probably make it very easy for a lot of people to give it a shot while not affecting their current desktop."
    author: "bsander"
  - subject: "Suse 10.0"
    date: 2005-11-12
    body: "Is there a standard way to install kde 3.5 RC1 on Suse ? I'd prefer to add a source to Yast and then update rather than playing directly with rpm. Thanks."
    author: "Med"
  - subject: "Re: Suse 10.0"
    date: 2005-11-12
    body: "I checked the ftp-site, but did not find any yast-specific directories/files on it.\nSo you can't use the ftp directory as yast source.\nBut you can try to do the following:\n\n1 - download all files to a local directory, then right click on that directory and select [Actions->add directory as yast source] (or similar option)\n\n2 - download all files to a local directory, open a konsole, go to that directory and type \"kdesu \"/sbin/yast2 -i *.rpm\""
    author: "rinse"
  - subject: "Re: Suse 10.0"
    date: 2005-11-12
    body: "Thank you very much, option 1 worked perfectly. However now the graphical login screen doesn't work anymore. I guess it has something to do with kdm. Any idea ? Thanks."
    author: "Med"
  - subject: "Re: Suse 10.0"
    date: 2005-11-12
    body: "> Is there a standard way to install kde 3.5 RC1 on Suse ?\n\nTry adding their KDE yast-sources from the supplementary folder.\n\nhttp://ftp.tu-chemnitz.de/pub/linux/suse/ftp.suse.com/suse/i386/supplementary/KDE/update_for_9.3/yast-source/\n\nThis source should be updated with new KDE packages, don't forget to \"refresh\" the source to see the updated packages. Now open the \"Yast software installation\", make sure you see all packages in the \"Installation overview\" (by enabling the \"Keep\" option). Right click on the package list, choose \"Upgrade when new version available\".\n\nIt's somewhat a hack, but it works if you want to upgrade packages :-p"
    author: "Diederik van der Boor"
  - subject: "konstruct + svn up"
    date: 2005-11-12
    body: "Hi,\n\nI used konstruct last week to build and test KDE 3.5b2.\n\nAmong other things, I was obliged to update akode to \"akode-2.0b3.tar.gz\", (instead of beta 2), to add directly forward declarations into some tarballs downloaded by konstruct (because of gcc 4.0.2 delivered into my mandriva), and so on ...\n\nnow \"svn up\" refuses to update my \"konstruct\" directory. I do not want to rebuild everything, but only tarball that have been updated between beta2 and RC1.\n\nDo you have a tip to handle this simply (other than having to restore everything manually) ?\n\nThanks!"
    author: "Tmor"
  - subject: "debian"
    date: 2005-11-12
    body: "i'm just dreaming of downloading 3.5.0 from official debian unstable repository after no more than a weak since the release...\n\nhope that there wont be any more abi transitions or release freezes..."
    author: "Nick"
  - subject: "Re: debian"
    date: 2005-11-12
    body: "I second that..."
    author: "Stackbit"
  - subject: "Re: debian"
    date: 2005-11-12
    body: "I third that... :)\n"
    author: "petteri"
  - subject: "Re: debian"
    date: 2005-11-13
    body: "as do i !! especially since debian and kde are supposed to have such a big cooperation agreement... "
    author: "tellico"
  - subject: "Re: debian"
    date: 2005-11-12
    body: "so do I, I could really not wait and compiled all with konstruct today.\nWorked well, will now find a way to integrate my compiled build into sid,\nand the day 3.5 has been uploaded, I'm gonna simply reinstall (over) it"
    author: "ac"
  - subject: "cool... great..."
    date: 2005-11-12
    body: "cool... great...\n\ni'm looking forward to really seeing KDE 3.5!"
    author: "barosl"
  - subject: "Error compiling kdeutils"
    date: 2005-11-12
    body: "Hello,\nI just tried to compile KDE 3.5 rc1 from source but I get an error with kdeutils:\nmake[3]: Entering directory `/multimedia2/src/kdeutils-3.5.0/superkaramba/src'\nif g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I/usr/local/kde/include -I/usr/lib/qt/include -I/usr/X11R6/include  -I/usr/local/include -I/usr/local/include/xmms -I/usr/include/gtk-1.2 -I/usr/include/glib-1.2 -I/usr/lib/glib/include -I/usr/X11R6/include -I/usr/include/python2.4  -DQT_THREAD_SUPPORT  -D_REENTRANT  -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -O2 -Wformat-security -Wmissing-format-attribute -Wno-non-virtual-dtor -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT meter_python.o -MD -MP -MF \".deps/meter_python.Tpo\" -c -o meter_python.o meter_python.cpp; \\\nthen mv -f \".deps/meter_python.Tpo\" \".deps/meter_python.Po\"; else rm -f \".deps/meter_python.Tpo\"; exit 1; fi\nmeter_python.cpp: Dans function \u00ab PyObject* QString2PyString(QString) \u00bb:\nmeter_python.cpp:129: error: cannot convert `Py_UNICODE*' to `const wchar_t*' \n   for argument `1' to `PyObject* PyUnicodeUCS2_FromWideChar(const wchar_t*, \n   int)'\nmake[3]: *** [meter_python.o] Erreur 1\nmake[3]: Leaving directory `/multimedia2/src/kdeutils-3.5.0/superkaramba/src'\nmake[2]: *** [all-recursive] Erreur 1\nmake[2]: Leaving directory `/multimedia2/src/kdeutils-3.5.0/superkaramba'\nmake[1]: *** [all-recursive] Erreur 1\nmake[1]: Leaving directory `/multimedia2/src/kdeutils-3.5.0'\nmake: *** [all] Erreur 2\nregis@REGIS:/multimedia2/src/kdeutils-3.5.0$ \n\nI've searched in kde bug tracking system and found nothing, maybe I've done something wrong, or maybe should I add a bug report, but I want to be sure, can someone tell me what to do? :)"
    author: "regis"
  - subject: "Re: Error compiling kdeutils"
    date: 2005-11-13
    body: "Hey I have the same problem."
    author: "stumbles"
  - subject: "Wait for your distro to provide binary packages"
    date: 2005-11-13
    body: "... and stop reporting compiling issues here."
    author: "ac"
  - subject: "Re: Wait for your distro to provide binary packages"
    date: 2005-11-13
    body: "Well ya know that'd be *great* if I was *using* a binary distro.... which *I ain't*."
    author: "stumbles"
  - subject: "Re: Wait for your distro to provide binary packages"
    date: 2005-11-13
    body: "I've compiled a lot of things myslef for my slackware so maybe if I install a package for v10.2 or 10.1 it will not work either..."
    author: "regis"
  - subject: "Re: Error compiling kdeutils"
    date: 2005-11-13
    body: "It's already fixed in SVN.\nhttp://www.p0z3r.org/2005/11/pyunicodesize.html"
    author: "Morty"
  - subject: "Re: Error compiling kdeutils"
    date: 2005-11-13
    body: "Good deal. Guess I was *to quick* to submit a bug report."
    author: "stumbles"
  - subject: "Re: Error compiling kdeutils"
    date: 2005-11-13
    body: "Thank you :)"
    author: "regis"
  - subject: "Re: Error compiling kdeutils"
    date: 2005-11-13
    body: "I'm actually kind of surprised that the RC1 picked up this code instead of the final fix which all happened on the same day.  \nRegardless, it should be fixed in SVN and shouldn't show its ugly head in RC2.\n"
    author: "p0z3r"
  - subject: "qt-3.3.5"
    date: 2005-11-13
    body: "hi, sorry for being ignorant here, but is the KDE 3.5-rc1 and indeed KDE 3.5 itself compatible with qt-3.3.5? I recall reading a compile issue with KDE 3.5-beta1, and am wondering whether it's been fixed."
    author: "wynn"
  - subject: "Re: qt-3.3.5"
    date: 2005-11-13
    body: "Yes. Was already in Beta 2."
    author: "Anonymous"
  - subject: "Exchange support in 3.5?"
    date: 2005-11-13
    body: "Hi, everyone!\n\nKDE 3.5 RC1 is definitely great news.\n\nI do have a question though. Does anyone knows what happened to MS Exchange support in Kontact? It was on KDE 3.5 features plan about a month ago and than it strangely dissappeared from there. I am pretty sure there are a lot of people like me out there, who require not only KOrganizer Exchange support(which is great, of course) but KMail Exchange support as well. Having full email support, will bring KDE one huge step closer to corporate desktop world, which currently dominated by Gnome, largely because of Evolution and its' support for MS Exchange.\nMe, I am forced to keep Evolution and everything it requires (a LOT of Gnome-specific libraries!), due to lack of that Exchange support in Kontact.\n\nP.S. Yes I know about the IMAP option, and no, the IT Manager will not open it just so I can use Kontact with it. Frankly, I wouldn't do that either, if I was him. That only adds more headache to the busy work schedule. :)\n\nThanks in advance for any useful info! "
    author: "mitya"
  - subject: "Re: Exchange support in 3.5?"
    date: 2005-11-13
    body: "> KOrganizer Exchange support(which is great, of course)\n\nEven that doesn't seem to work for me. Some shonk with SSL, i presume."
    author: "Anonymous Coward"
  - subject: "Re: Exchange support in 3.5?"
    date: 2005-11-13
    body: "Exchange support was buggy and didn't work, so it was removed."
    author: "Thiago Macieira"
  - subject: "Re: Exchange support in 3.5?"
    date: 2005-11-15
    body: "Thanks for the reply!\n\nAny plans for adding it in the future, hopefully before kde 4?\n\nPerhaps there is a patch I can use to try it out on my system? That will be great to see it with my own eyes.\n\nThank you!"
    author: "mitya"
  - subject: "Re: Exchange support in 3.5?"
    date: 2006-01-09
    body: "Bummer... Even evolution doesn't meet my needs.  I'm still trying to find a decent linux email client that supports MULTIPLE exchange accounts.  IMAP isn't an option for me.  Hopefully KMail can do this when they finally get around to implementing exchange.  I can always dream........."
    author: "mmHg"
  - subject: "IMAP IO Slave"
    date: 2005-11-13
    body: "I cant't use IMAP(S) with Kontact on my laptop after suspend (to RAM or to disk). After restarting Kontact it works again. This problem is not new in 3.5, I have it since I have a laptop (since 3.4.2). Can someone point me to a bug-report? I tried to find one but i'm not famillar with bugzilla..."
    author: "Michi"
  - subject: "First impression"
    date: 2005-11-13
    body: "I had some high expectations for KDE 3.5, but after browsing the Klax live-cd I'm a bit disappointed. My first impression: it's quite unpolished. That's a pity.\n\nI'm talking about things like:\n* Options in KDevelop are grayed-out when they shouldn't;\n* The \"New\" dialog in KWord doesn't give a reasonably default. I opened \"Page layout\" by accident, couldn't type text (appearently you need a textframe for that). It took me a while before I found the \"A4 page\" option.\n* The \"Document settings\" of KWord tab has a messy layout,\n* The default \"Insert\" toolbar in KWord is sitting in the way, and isn't exactly a must-have for text writing.\n* Not being able to see the selection in Krita for empty images.\n* The properties Tab occupies all space in Kexi.\n* The current layout of KGeography (but the idea of the application is really cool though!)\n\nI know these are just minor problems, and maybe I shouldn't nag about it. But these small things give a bad first impression. I get the idea everyone has put a lot of work in KDE, but the final touch is missing.. :-(\n\nSome things are really cool, like:\n* Adblock support in konqueror\n* SDI support in KOffice\n* having Kexi/Krita in KOffice :-)\n* all Kicker improvements\n* better grouping of menu items.\n* the GUI of blinken.. awesome!\n\nI also find KWin/Kicker quite appealing with the latest Plastik version. :-)"
    author: "Diederik van der Boor"
  - subject: "Re: First impression"
    date: 2005-11-13
    body: "It seems most your points are about KOffice - which is not part of KDE 3.5 despite being on the CD. The good news is that KOffice will start still this year a new release round for a release early next year and you have the possibility to influence its development when you split up and feed your points to the KOffice developers."
    author: "Anonymous"
  - subject: "Re: First impression"
    date: 2005-11-13
    body: "KOffice is not part of \"KDE 3.5\" as such.  That is, KOffice doesn't try to sync its releases with KDE 3.5.  The next big release of KOffice is 1.5 and that will probably happen around March next year.\n\nHaving said that, we really need feedback from KOffice users, so if you could send an email to the koffice-devel mailing list that would be very handy.\n\n"
    author: "Robert Knight"
  - subject: "Re: First impression"
    date: 2005-11-14
    body: "Reading http://www.canllaith.org/svn-features/svn-koffice.html\n...\n\n\"In KSpread, Robert Knight has recently implemented formula editing with colour syntax highlighting.\"\n\nWow, seems like the final touch will be there shortly."
    author: "AV"
  - subject: "changelog?"
    date: 2005-11-23
    body: "am i blind or is it simply impossible to find a changelog anywhere on kde.org?"
    author: "hannes hauswedell"
  - subject: "Re: changelog?"
    date: 2005-11-23
    body: "One could simply look at the svn history..."
    author: "ac"
---
KDE 3.5 is <a href="http://developer.kde.org/development-versions/kde-3.5-release-plan.html">almost finished</a>, so we have prepared a <a href="http://lists.kde.org/?l=kde-announce&m=113175000630488&w=2">first release candidate</a>.  We want to have it tested as much as possible, so please give it a show. You can download the <a href="http://download.kde.org/unstable/3.5-rc1/src">sources from download.kde.org</a>. To compile them you can use the <a href="http://developer.kde.org/build/konstruct/">Konstruct build script </a>. In the short time frame since tarball creation only <a href="http://download.kde.org/unstable/3.5-rc1">binary packages</a> for SUSE Linux got finished, <a href="http://www.kubuntu.org/announcements/kde-35rc1.php">Kubuntu are uploading theirs</a>. Thanks for your help in <a href="http://bugs.kde.org/">reporting bugs</a> and giving feedback so far. <B>Update:</b> There is now also a <a href="http://ktown.kde.org/~binner/klax/devel.html">Klax Live-CD for this release</a> available.

<!--break-->
