---
title: "KDE CVS-Digest for January 7, 2005"
date:    2005-01-08
authors:
  - "dkite"
slug:    kde-cvs-digest-january-7-2005
comments:
  - subject: "First Post!"
    date: 2005-01-08
    body: "Thanx, Derek!"
    author: "M"
  - subject: "Because it's only on the end of a cable modem..."
    date: 2005-01-08
    body: "cvs-digest.org is timing out for me (funny, always seems to do that when a new issue is posted on the dot), so for those of you who might also be having problems reading this week's issue, please to be pasting this coral cache link in your location bar:\n\nhttp://cvs-digest.org.nyud.net:8090/index.php?issue=jan72005\n"
    author: "Spanner"
  - subject: "Unified framework?"
    date: 2005-01-08
    body: "Is there a common framework so that plugins will get included in other applications such as Kolourpaint ecc. as well?"
    author: "Bert"
  - subject: "Re: Unified framework?"
    date: 2005-01-08
    body: "There is this awesome start libkipi.  Its a nightmare to actualy use in a host application and it only works on URLS.  The plugings are not really integrated at all but there are quite a few of them, and they are cool too. In about 3 days I wrote a small utility that allows you to apply these transformations in Konqueror.\n\nLuckily these guys don't have an API in kdelibs yet, so they can work out the kinks.  When it works its VERY cool.\n\nIf you want raw images, we have kdefx, and that supplies most image magick operations for QImages and QPixmaps."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Unified framework?"
    date: 2005-01-09
    body: "What about a 16bit/channel framework in kde. Is something like this possible with Qt4? This is more and more a must-have for modern dslr.\n\nBye Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "KWin adds dynamic keybindings."
    date: 2005-01-08
    body: "thx!"
    author: "pouet"
  - subject: "Is this normal?"
    date: 2005-01-09
    body: "I'm looking at the KDE 3.4 feature list http://developer.kde.org/development-versions/kde-3.4-features.html and I notice that hardly 1/3 of the proposed features are listed as completed. \n\nIs this normal for KDE with less than 2 months to go?"
    author: "Alex"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "Pretty much.  I've noticed that the list sometimes does not get updated until after the final freeze and code goes to packaging. "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "Quite normal, it's a clear case of developers too busy coding and not updating the list. If you compare a recent CVS checkout against the list, you will find lots to move to status green. On the other hand a few will sadly stay red and be moved to the feature plan for the next release. \n\nSince updating and maintaining the feature plan requires some work from the developers and usually ends up lagging. Could this perhaps been done in a more userfriendly(developer) way. Some kind of dynamic setup where adding new items to the list and making status change TODO->In Progress->Finished easier. Ticking of some kind of checkbooks are less timeconsuming and easier than patching a *ml  file in CVS. "
    author: "Morty"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "It is quite simple to update. The list is generated from an xml file, \n\ndeveloper.kde.org/development-versions/kde-features.xml\n\nAll the developer has to do is change the status or target.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "As you say it's quite simple to update, but to me it looks like it involves some work to actually do it. Which is supported by the fact that the feature plan are very seldom updated, or at least it imply it's outside the developers normal workhabit/mode. I would guess making changes to a big XML file, in a CVS module the developer don't regularly use makes it, not hard but tedious. And then it very easy gets put on the list of tings to do \"later\", hence ending up as last minute updates before release.\n\nKeeping the feature plan up to date are not very important, it's more a \"nice thing\". As such, I think the only way to keep it up to date are making the process so simple and require so little work it makes the developers feel silly not updating it:-) Some tool of some kind, a plug-in for Kontact's todo list?"
    author: "Morty"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "Perhaps integrating it with CVS comments, as the bug database is?"
    author: "Illissius"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "actually, an easy solution to this would be to put everything into bko... all that is needed is another state BEING_WORKED_ON.\n\nthen the developers would open a bug for every feature they have planned for the next release and the cvs<->feature plan integration comes free.\n\nthe development plan would be reduced to a number of bko bug numbers (as the backend)... \n\nactually, this would improve user<->developer interaction, as users can comment on a feature...\n\nred <-> state NEW\nyellow <-> state BEING_WORKED_ON\ngreen <-> state RESOLVED\n\nperhaps for KDE4"
    author: "bangert"
  - subject: "Re: Is this normal?"
    date: 2005-01-10
    body: "Kontact integration sounds very good!"
    author: "gerd"
  - subject: "Re: Is this normal?"
    date: 2005-01-10
    body: "yes ,\n\nautomatically sync the tasks in kontact with the webpage.\n\nif you set the task on 70% done , it should show it on the webpage.:-)\n\n"
    author: "chris"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "I think in many cases people are too optimistic about what they can do. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Is this normal?"
    date: 2005-01-09
    body: "Hmm, or there are other issues. It is also always a question of entrance barriers. to lower them, to get people involved is very important. Web-based translation tools for example could simplify contribution a lot."
    author: "aile"
  - subject: "It was about time!"
    date: 2005-01-09
    body: "Linux always lacks something like ACDSee for a long time. Gwenview is a good start, even that I don't like some parts of it, this is a program with great potential for becoming the standard image viewr on KDE, but it was lacking support for animated images for a long time, so I had to use a browser (konqueror or mozilla/firefox) to see them. \nLooks like I can finally see all my images in just one program, GREAT!"
    author: "Iuri Fiedoruk"
  - subject: "Re: It was about time!"
    date: 2005-01-09
    body: "> Linux always lacks something like ACDSee for a long time\n\nReally? I've found almost too many ACDSee-clones for linux already.\n\nHave you really looked?"
    author: "KDE User"
  - subject: "Re: It was about time!"
    date: 2005-01-09
    body: "Instead of giving such a redudant comment, you could actually be usefull and post a list of such programs..."
    author: "blacksheep"
  - subject: "Re: It was about time!"
    date: 2005-01-10
    body: "Yes, I've tested a lot of them, and actually never found a real good one until now (Yes, already compiled and instaled the newest version of gwenview, it shines)."
    author: "Iuri Fiedoruk"
  - subject: "Re: It was about time!"
    date: 2005-01-09
    body: "The best graphic viewer/resizer/manipulator is Irfanview:\nhttp://www.irfanview.com/\n\nwin32 only unfortunately, but runs good enough in wine:-("
    author: "ac"
  - subject: "Re: It was about time!"
    date: 2005-01-09
    body: "Unfortunately it doesn't show FPX files under wine. I mean under wine you can view one file, after which it doesn't show any other FPX or it crashes. \n ImageMagick also crashes on FPX files and I couldn't find any other application that even states about itself that reads FPX.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: It was about time!"
    date: 2005-01-10
    body: "What format is FPX?\n\nI have an ImageMagick based QImageIO plugin, so any KDE application can read and write any format supported by ImageMagick.  The only thing stopping us from adding almost 30 image formats to KDE is the lack of regexps to identify the images.\n\nIf anyone has enough skill or time feel free to contact me.  Otherwise, I'll just wait for KDE 4 or so..."
    author: "Ian Reinhart Geiser"
  - subject: "Re: It was about time!"
    date: 2005-01-10
    body: "It's a format used in some (all?) Kodak digital cameras. There is a library available for Unix, called libfpx (I have libfpx-1.2.0.9). The license as I see is GPL incompatible, more like the old BSD (IIRC) with the advertising clause.\n\n> I have an ImageMagick based QImageIO plugin, so any KDE application can read\n> and write any format supported by ImageMagick. The only thing stopping us\n> from adding almost 30 image formats to KDE is the lack of regexps to identify\n> the images.\nThis sounds interesting. Where do you need the regexps? For the mime magic? As otherwise you don't have to tell for ImageMagick the file type, it will recognize without problems. Also theoretically you could see if there is a native QImageIO for a format and if it is use that one, otherwise use the ImageMagick based.\n \n"
    author: "Andras Mantia"
  - subject: "Re: It was about time!"
    date: 2005-01-09
    body: "I prefer showimg"
    author: "Pat"
  - subject: "Re: It was about time!"
    date: 2005-01-09
    body: "seconded. gwenview is pretty nice, however.\nirfanview's rescale/sample algorithms are unbeatable :)\n(aside: I wonder why it isn't made open source, as whoever makes it doesn't make any money off of it asides from donations as it is...)"
    author: "Illissius"
  - subject: "Re: It was about time!"
    date: 2005-01-10
    body: "Unbeatable in which way? The new \"fast\" MMX-based scaling is pretty fast, and the \"best\" scaling IMHO gives very good quality (in fact I remember somebody asked for it to be turned also into a standalone utility because the person couldn't find anything with so good results - apparently hadn't tried ImageMagick, which is where it's from).\n"
    author: "Lubos Lunak"
  - subject: "Re: It was about time!"
    date: 2005-01-10
    body: "The best image viewer for Linux (any OS, really) would be XnView. It's not OSS, but freeware, and uses a butt-ugly Motif GUI, but it opens almost anything and has lots of great tools:\n\nhttp://www.xnview.com"
    author: "Willie Sippel"
  - subject: "subjects are really useless here"
    date: 2005-01-09
    body: "now, those anti-vignetting and lensdistortion filters would be really cool if one could use a few samples to profile the lens being used (one sample for a short zoom-range for zoom-lenses). Then the filter could read the used focal length from the exif-data and apply the most appropriate profile."
    author: "me"
  - subject: "Re: subjects are really useless here"
    date: 2005-01-09
    body: "There is a linux command line version of PTlens using the same database like the windows version. An integration into digikam would be quite cool :-)\n(Remember myself: Write a whishlist bugreport)\n\nhttp://epaperpress.com/ptlens/\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: subjects are really useless here"
    date: 2005-01-09
    body: "Oh, another program which needs more PR:\n\nWhen using a tool like ptlens automatically you need extract the lenstype from the exif data. Libexif and kde wrapper libkexif is very limited compared to the great exiftool. Exiftool is first coice and first class when it comes to decode secret exif vendor tags (like: what lens was used when shooting an image)\n\nExiftool ist till version 4.10 the one and only free software with a starting support in converting metadata between different file formats. I hope it is possible to transfer e.g. Canon CRW metadata into JPG soon, without using THM files. For a program like digikam metadata transfer would be a unique feature.\nThis is not like jhead which can only copy a exif datablock from one file to another. Exiftool decodes and encodes every single metatag.\n\nTime for a new libkexif? :o) \n\nBye\n\n  Thorsten\n\nhttp://www.sno.phy.queensu.ca/~phil/exiftool/\n "
    author: "Thorsten Schnebeck"
---
In <a href="http://cvs-digest.org/index.php?issue=jan72005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=jan72005">experimental layout</a>): <a href="http://gwenview.sourceforge.net/">Gwenview</a> adds support for animated pictures.
<a href="http://digikam.sourceforge.net/">digiKam</a> adds more image editing plugins: Sheartool, anti-vignetting, lensdistortion.
KWin adds dynamic keybindings.
PwManager adds Smartcard interface.



<!--break-->
