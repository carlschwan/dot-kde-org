---
title: "DesktopLinux.com: KDE Increases Its Dominance"
date:    2005-05-07
authors:
  - "Anonymous"
slug:    desktoplinuxcom-kde-increases-its-dominance
comments:
  - subject: "Congrats!"
    date: 2005-05-07
    body: "Thanks to all people involved in KDE development!  You all must be doing something right ;-D\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "Well, yes.  They are, but GNOME is also doing something wrong, imho.  KDE  has that slick integrated environment feel, which is the whole point of a 'desktop environment' rather than just picking a window manager, picking a terminal emulator, running a few background apps.  Konqueror is so fast to pull up and search with (via web shortcuts) that it's probably the most productive environment I've ever used.  By contrast, although GNOME was originally my favorite and I'd still prefer it in Free Software grounds, GNOME seems to have fundamental bits missing: no automatic SDI/MDI switching from a common windowing layer, difficult database/UI integration, and a general lack of useful widgets/high-level programming abstractions.  I think if they'd used C++ and embraced a more object-oriented philosophy from the beginning, they'd be further ahead now.  I think losing Anjuta 2 was a big nail in the coffin: they really needed a better development environment, and it never happened.\n\nOne thing I will say for GNOME is that they really nailed the accessibility and (especially) the international text handling via pango.  I keep having troubles with missing international glyphs in KDE, whereas it all \"just works\" in GNOME.  That alone is a big thing, and is almost enough to tempt me back.  Only *almost*, though.\n\nThankfully, all these things are coming to KDE soon: better glyph handling (I think), better accessibility (thanks to GNOME for that) and better input methods (based on GNOME too)."
    author: "Lee"
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "Wow! That must be the most uninformed comment I've read in a long while. Where do I begin?\n\n<i>no automatic SDI/MDI switching from a common windowing layer,</i>\n\nHuh?\n\n<i>difficult database/UI integration</i>\n\nWhat? Ever heard of GNOME-DB?   http://www.gnome-db.org/\n\n<i>and a general lack of useful widgets/high-level programming abstractions.</i>\n\nWTF? What is GTK+ for then? If C is too cryptic for you, what about all the higher-level bindings for GTK+ including C++? And since C++ is retarded to begin with, what about the Python, Perl and Ruby bindings? Those aren't high-level enough for you? And if you have a phobia for dynamic languages, what about Mono, or the Java bindings?\n\n<i>I think if they'd used C++ and embraced a more object-oriented philosophy from the beginning, they'd be further ahead now.</i>\n\nAnd who said they aren't using object-oriented philosophies? What is Glib, Gobjects, GTK+ etc all about? Procedural philosophies?\n\n<i>I think losing Anjuta 2 was a big nail in the coffin: they really needed a better development environment, and it never happened.</i>\n\nMy God! When was Anjuta 2 lost?  http://anjuta.sourceforge.net/\n\n\nI mean, if you do not understand the GNOME technologies, don't you think it is disingenuous to speak authoritatively about them? The GNOME platform is as object oriented as it gets. Also, applications written for the platform do integrate well with it. Metacity and the gnome terminal integrate excellently well with GNOME. I don't know what the hell you are babbling about.\n\nAnd, for christ sake, a language doesn't automatically determine whether a framework is object oriented or not. Functional programming, object oriented programming, aspect oriented programming, design by contract, etc are just paradigms. Their concepts can be implemented in assembly or even C! Yes, that's very shocking, isn't it? But it's true. Let me let you in on another secret, most frameworks written in so called object oriented languages actually suck! Why? Because OO gets ugly and complex real quick. Almost nobody knows how to write utopian OO. Well, only very few people know how to do it.\n\nWell designed interfaces, abstractions and concepts is what makes great frameworks, not programming languages.\n\nSheesh\n"
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "In fact, that was just a reactionary comment.  The GIMP guys have been complaining about the lack of SDI/MDI facilities, so I'm not wrong there.  GNOME-DB is in it's infancy compared to KDE's database-aware widgets, so get *your* facts straight there.  Likewise, with the high-level widgets.  *You* are the one that is implying someone said they don't use object-oriented philosophies.  *I* spoke of *more* object-oriented philosophies.  Anjuta 2 was lost when they declared it wasn't happening, and they would just go back to improving Anjuta 1.  Anjuta 2 is not the same as the second version of anjuta 1, it was a completely different project.  Again, get *your* facts straight.  Anyway, I've no interest in continuing this conversation with someone who just reacted that way to straightforward comments, so take my comments or leave them and make your own."
    author: "Lee"
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "Don't be silly. libgda and other components in GNOME-DB have been around since GNOME 1. How could you label GNOME-DB infant? Last time I checked, GNOME-DB supports a hell of a lot more databases than Qt does. Has GNOME-DB failed you in any of your projects? If so, how? Do you have any real world experience to back up your careless statements? \n\nSecondly, you are the one who said GNOME was not object-oriented and did not provide higher-level constructs. Your erroneous statements were clearly debunked. \n\nThirdly, Anjuta never died, don't pretend as if you have been following the project. Look, just admit you do not know much about GNOME and its technologies. Don't start spreading misconceptions about shit you know almost nothing about.\n\nFinally, GTK+ does support SDI and MDI widget implementations. How the hell do you think Epiphany, gEdit, Xchat and company support tabs? You need to be specific about what SDI/MDI implementation that GTK+ lacks. And frankly, I'll be shocked if you come up with one. \n\nNo offense, but your statements reek of hearsay, speculation, FUD and unfounded assumptions."
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "> How could you label GNOME-DB infant?\n\nBecause last I looked, its tools were missing basic features, like creating databases.  That wasn't yesterday, but it was recent enough, and I certainly expected more progress from gdb1.  Mergeant is a primitive table-based sql editor, whereas KDE's database tools can easily do RAD-based generation of forms etc. from directly within the IDE.  Thus, gnome-db is, by comparison, in its infancy.  Nothing to argue about here.\n\n> Secondly, you are the one who said GNOME was not object-oriented and\n> did not provide higher-level constructs. Your erroneous statements were\n> clearly debunked.\n\nNo.  You misread my comment the first time, and I corrected you.  You are still missing the simple qualifications in my statement.  I suggest you read that part again until you get it right, then learn what debunking an argument actually involves.\n\n> Thirdly, Anjuta never died,\n\nWell, if it didn't die, it certainly went into deep freeze for a while.  I distinctly remember reading that it had been dropped for various reasons, in favour of focusing limited resources on improving anjuta 1.  But maybe this explains why I heard that.  http://gnomedesktop.org/node/2126\n\n> Don't start spreading misconceptions about shit you know almost\n> nothing about.\n\nPlease try to keep this civil and honest if you wish to continue the discussion.  Frankly, explaining this to you is already pretty boring for me without that.\n\n> Finally, GTK+ does support SDI and MDI widget implementations.\n\nOf course it does.  I never said it didn't.  Again, you have overreacted and have misread what I said.\n\n> You need to be specific about what SDI/MDI implementation that GTK+\n> lacks. And frankly, I'll be shocked if you come up with one.\n\nI don't need to do anything, but in the interests of educating you a little, I pulled the first two links off a two-second google search:\n\nhttp://lists.ximian.com/archives/public/gtk-sharp-list/2002-November/000606.html\nhttp://www.forum4designers.com/archive57-2005-3-201815.html\n\nBeyond that, you'll have to do your own research on such a well-known issue, since again, I have little interest.\n\n> No offense, but your statements reek of hearsay, speculation,\n> FUD and unfounded assumptions.\n\nActually, you've been quite offensive throughout this conversation.  But again, if you read my comments and stop being reactionary, you'll see that I gave GNOME credit where credit was due, and there really is nothing to argue about here.\n\nAnyway.  I've got better things to do than argue with you.  Goodnight."
    author: "Lee"
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "Wrong again! GNOME-DB can create databases and has been able to do that since its inception. I do not see anything KDE's database tool can do, that GNOME-DB can't. GNOME-DB supports more database backends, last I checked.\n\nGTK supports both SDI and MDI interfaces, and has done so forever. You failed to provide any convincing evidence to the contrary. Reading comments on blogs, mailinglists, emails and forums does not qualify as facts or credible sources. Try reference documentations, code and even statements from maintainers of the project, next time.\n\nYes, you really should find something better to do than argue."
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "Well I find your argument as unconvincing as you find mine, so I guess we'll have to agree to disagree."
    author: "Lee"
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "I found your actual *sources* to back up your arguments quite convincing.  The guy who claimed his vitriol is better than your actual sources is obviously either a loon or a troll.  So I wouldn't be worried."
    author: "anon"
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "Yeah, his source were very convincing. You know, I get all my facts from forums and mailinglists these days. And yeah, GTK+ does not SDI/MDI widgets. My sources from a forum told me that. And I believe it. And yeah, GNOME-DB can't create datatbases, I got that from kde.news, and you better believe that. \n\nPeople like you make the \"InterWeb\" a sad ignorant space. Anybody who remotely dispells falsehood is labeled a troll. Go ahead keep supporting and spewing cow dung because some clueless person on IRC said so and that qualifies as reliable sources."
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "People like you would believe GNOME development happens behind closed doors at Fedora cathedral ivory towers.  Wake up and smell the coffee.  It happens on this, the InterWeb, as you call it.  Visit planet.gnome.org sometime, check the mailing lists.  Inform yourself.  You might be surprised.  On the other hand, I would be very surprised if you even *used* GNOME -- you're probably just here to give them a bad name."
    author: "anon"
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "If I didn't use GNOME, how could I have dispelled all of Lee's myths? Do you think it was divine inspiration?"
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "Excuse me, but your so-called dispellation of myths has already been amply refuted by the revelation of your lies and defamation as regards the GNOME project. No need to invoke either your God or your Devil, only that you face your own ignorance."
    author: "anon"
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "If you will be so kind to point out my lies and defamation of the GNOME project. Go ahead, I dare you."
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "> Go ahead, I dare you.\n\nWhat are you 5 years old?  I don't do dares.  Either play by yourself or go find someone else to play with."
    author: "anon"
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "Ahh, you have a point on Anjuta 2 at least, since they've resurrected the project.  But they *did* abandon it, as I said, and it's still not ready.  I stand by my point that GNOME has probably lost a lot of development progress due to that gap."
    author: "Lee"
  - subject: "Re: Congrats!"
    date: 2005-05-10
    body: "First of all, you don't need Anjuta to develop GNOME applications or any applications for that matter. Most Unix developers can't stand Integrated Development Environments, they'd rather use Emacs or VIM. Frankly, I don't blame them, apart from Eclipse and IntelliJ, every other IDE I have used sucks! \n\nSecondly, I don't know where you get this rumors that Anjuta died. I had followed the project for the longest time, and although there were periods of low activity, I don't ever remember the project dying, or resurecting. \n\nI also don't understand what development progress GNOME has lost as a result of Anjuta's supposed misfortune. The development process of GNOME and Anjuta are very orthogonal. GNOME has many problems, but I'm afraid you listed none of them."
    author: "."
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "> better accessibility (thanks to GNOME for that) and better input methods (based on GNOME too)\n\nI don't know why you thank GNOME: the credits all have to go to Trolltech and the Immodule project which provides an input method subsystem already for Qt3."
    author: "Anonymous"
  - subject: "Re: Congrats!"
    date: 2005-05-11
    body: "Don't worry too much -- I wasn't crediting GNOME for the code :)\n\nI read just recently that the input method system in Qt4 would be more like GTK's, and I had seen the current skim-like system, which seems quite different, so I guess I jumped to a conclusion there that they had decided to go with a different method which GNOME(/MS Windows?) led the way on.  I'm still not sure if qt-immodule was based on GNOME or not.  Apologies if that was incorrect, though."
    author: "Lee"
  - subject: "impressive"
    date: 2005-05-07
    body: "61% ?! Wow. What would happen without RHs dominance on corporate landscape..\n\nWhich leads me to thinking 'lies, damn lies and statistics'. KDE is by far the most popular, but 61% without RHs backing sounds bit too heavy."
    author: "Anonymous Coward"
  - subject: "Re: impressive"
    date: 2005-05-07
    body: "Red Hat has committed ritual suicide on the desktop with the whole Fedora mess.  My entire University ditched Red Hat for example, in favour of believe-it-or-not Gentoo with a KDE default.  Prior to that all the desktops were Red Hat BlueCurve, set to KDE by default."
    author: "anon"
  - subject: "Re: impressive"
    date: 2005-05-07
    body: "As impressive piece of work as SUSE 9.3 is, i would have to conclude that they must be heading to something similar than RH with Fedora. They have a plenty of really neat stuff in, especially KDE wise, but it's just not polished well enough (too many annoying bugs). Have to hope that this matures within the next few months.."
    author: "Anonymous Coward"
  - subject: "Re: impressive"
    date: 2005-05-07
    body: "First you have to realize that RH are not as important as you think. Perhaps they have a dominance on corporate landscape, but only in the US. And they see stiff competition from Suse/Novel and others.\n\nRHs backing aside, a high percentage of RH desktop users are also KDE users. In addition to RH not primarily being a desktop distribution the effect from RH not backing KDE are not that big. "
    author: "Morty"
  - subject: "Re: impressive"
    date: 2005-05-10
    body: "Its not as surprising as you might think.  KDE does amazingly well (in terms of usage) on Redhat desktops in spite of Redhats best efforts.  Until the whole fedora mess, I ran Redhat; as did almost all the Linux users I personally know... \n\n...they also ALL ran KDE.\n\nBobby"
    author: "brockers"
  - subject: "Re: impressive"
    date: 2005-05-11
    body: "> ...they also ALL ran KDE.\n\nI hope not using the ugly and still buggy Bluecurve style."
    author: "Anonymous"
  - subject: "Thanks for KDE"
    date: 2005-05-07
    body: "KDE just works and I am getting my work seriously done. Thanks to the people involved you rock."
    author: "Bonobo"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-07
    body: "I found bug every time I use KDE seriously and can't complete a task without report a bug to bugzilla... Seriously, why there is so difficult to have a usable environment that don't flash every time you open a window or a tab...\n"
    author: "Yan Morin"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-07
    body: "http://it.geocities.com/roccopapaleo/faq/italiano/asciidontfeed.html"
    author: "Anonymous"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-08
    body: "Hmm, 15 bugs reported - 14 closed, one unconfirmed and against an old version.\n\nhttp://bugs.kde.org/simple_search.cgi?id=Yan+Morin\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Math"
    date: 2005-05-08
    body: "So grandparent has used KDE seriously 15 times?"
    author: "Martin"
  - subject: "PS"
    date: 2005-05-08
    body: "Sorry -- couldn't resist to do the math. Thanks for reporting bugs!"
    author: "Martin"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-08
    body: "Strange and none of his reported bugs should stop one from getting serious work done. A bunch of compiling errors reported but a normal user shouldn't compile things anyways, he/she should use a ready made distribution, install the stuff and start getting his daily work done. A normal user don't know about memory leaks either. So there was no real reason for him replying that he has to report bugs for every task he did because none of his tasks are real life scenarios where people wouldn't get their work done. It would have been different if he reported a wish that Umbrellos starts supporting Rational Rose UML savefiles but that's another thing. I even doubt he knows that that is :)"
    author: "Bonobo"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-08
    body: "Take a closer look at the hits that produces.  Most of them are from different \"Morins\".  bugzilla doesn't use an AND relation with the two search terms. \n\nI only found Yan Morin in two or three of them. "
    author: "cm"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-08
    body: "Maybe the bug is somewhere else?\n\nSorry, bad joke."
    author: "KDE User"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-08
    body: "I also find bugs. But I find bugs in M$ software, too. I don't know who to inform about M$ bugs. But speaking about KDE I'm glad to have bugzilla. And I have experienced several times that my issues were resolved in less than 24 hours. \n\nDear Yan, let's go on and find bugs. And report them. The next release will be rewarding enough... Unfortunately software has a lifecycle, and bugs are simply a reality in any development process, no matter which software company or hacker group. But it also takes the users who inform the developers that something is getting on their nerves. M$ does not have such an attentive and communicative group of supporters. \n\nRegards,\nMarkus"
    author: "Markus Heller"
  - subject: "Re: Thanks for KDE"
    date: 2005-05-09
    body: "Hear hear!  Of course, we've still got a long way to go, but in my opinion, Linux (specifically Debian) plus KDE has already far surpassed Windows in terms of stability and usability.  I don't get _stressed out_ or _pissed off_ using my Linux box at home like I do on WinXP at work.\n"
    author: "Bill Kendrick"
  - subject: "Invalidating bug reports"
    date: 2005-05-08
    body: "I just want to comment that poo-pooing other peoples bug reports is not  very helpful to anyone. Even if they sound silly or slanderous to you or seem to carry a hidden agenda -- you know we dont always say how we mean.\n\nProfessionally I find that by listening to my users carefully -- especially the foolish ones, I often find great insights into how my application is not doing what I expected or they expected... and that too is a bug. \n\nAnd one more thing; KDE is FREE as in FREEDOM of EXPRESSION"
    author: "athleston"
  - subject: "Re: Invalidating bug reports"
    date: 2005-05-08
    body: "Yep.  Your confusion of the meaning of free in this context is a very common one.  This site goes a long way towards clarifying the confusion:\n\nhttp://lazaridis.com/core/project/gnu_gpl.html"
    author: "anon"
  - subject: "Re: Invalidating bug reports"
    date: 2005-05-09
    body: "Your comment links back to your own web which if anything tries to confuse the meaning of freedom by inventing its own terms.\n\nI think the definitive meaning of free software comes from GNU: http://www.gnu.org/philosophy/free-sw.html\n\nIMHO writing software is a form of expression to a programmer analagous to a painter creating art. \n\nThe comments/bugs submitted in KDE's forums similarly are personal expressions of their owners and as such all are equally valid. "
    author: "athleston"
  - subject: "Re: Invalidating bug reports"
    date: 2005-05-09
    body: "Excuse me but this is not my site.  It is a page on Lazaridis&co to clear up the free software confusion caused by GNU."
    author: "anon"
  - subject: "Re: Invalidating bug reports"
    date: 2005-05-09
    body: "While I don't agree with the sites newly invented terms, one must realize that it was the FSF who started the confusion by using the word \"free\" in the first place.  There are several other English synonyms that more closely fit with the Free Software idea. Among them are \"unrestricted\", \"unencumbered\", and <cough>\"open\"</cough>.\n\nDoes \"libre\" help matters any? Not much. Since the right to copy, modify and distribute traditional copyrighted works is a salable item, \"gratis\" is indeed an appropriate meaning. When one receives Free Software, once is receiving rights normally kept exclusive to the owner. Gratis. For free. As in beer.\n\n</rant>"
    author: "Brandybuck"
  - subject: "Re: Invalidating bug reports"
    date: 2005-05-10
    body: "I agree with most of what you've said here.  Please don't overload the term \"Free\" any more than it already is.  IMHO, Free(dom) is important enough to be a great selling point on its own merits :)"
    author: "Lee"
  - subject: "Left RedHat for KDE!"
    date: 2005-05-08
    body: "As soon as Redhat created Bluecurve and hiding the beauty of KDE,  I started to search for a better distro which did not blemish the beauty of KDE by putting their own icons and styles. And I found KDE in Slackware good, because KDE packages for Slackware are always readily available ;) and Redhat packages would not come out for weeks of each KDE release!\n\nIt is good to hear that KDE is 61%, soon it will be 90% ;) it (slackware+kde) performs well on a Celeron 500Mhz + 196 mb sdram! Definitely KDE 3.4 is the fastest of the previous versions!"
    author: "Fast_Rizwaan"
  - subject: "Re: Left RedHat for KDE!"
    date: 2005-05-08
    body: "I'm just in the same boat as you, slackware on a\n400Mhz system with 320 Mb ram. Running -current I\ngot the latest kde (3.4) and I enjoy it every day!\n\nTo get extra speedy starts of your applications,\nbe sure to let prelink run on your system once in\na while. And to make small applications, you can\nuse ruby/kdevelop, that works perfect, even on this\n(on paper) slow system.\n\nNext to the big group of kde hackers, I want to\nspecial thank Schot Wheeler (Juk), Jesper Pedersen\n(KimDaBa), Richard Dale (Ruby bindings) and Peter\nHedlund (KVocTrain). You created my favorite\napplications, running kde rocks!\n\nPieter"
    author: "Pieter"
  - subject: "Re: Left RedHat for KDE!"
    date: 2005-05-09
    body: "Well, I intended two months ago to buy an upgrade kit, just for fun, and finally I didn't. My K7 500 + Slack + homebuilt KDE 3.4 definitively satisfies me, and I'm still longing for my computer to breakdown. This sh*_\"'$ simply doesn't want to retire, I'm waiting that since more than 2 years now!\n\nMaybe KDE 3.4 (and maybe future stabilized gcc 4 too btw) should be declared \"Unamerican\", for preventing consumers to buy new hardware... Long life to XP & longhorn!\n"
    author: "christian"
  - subject: "Re: Left RedHat for KDE!"
    date: 2005-05-08
    body: "I could not agree any more :-)\n\nSlackware + KDE == Vanilla Evil UNIX With a Freindly Face(TM)\n\n:-D\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Left RedHat for KDE!"
    date: 2005-05-09
    body: "bluecurve hiding the beauty of KDE? its *just* a theme."
    author: "ricky"
  - subject: "Re: Left RedHat for KDE!"
    date: 2005-05-09
    body: "I lot of folks use \"Bluecurve\" to refer not only to the theme, but to the whole collection of usability problems RedHat put into KDE.  AFAIK they can all be corrected, but it's more time-consuming than just changing themes.\n\nExamples: installing the wrong display manager by default with KDE (breaks shutdown/reboot function), mouse cursor themes are strangely broken (default cursor okay, certain \"events\" are not), admin utilities use RTL button order, etc.\n\nYes, everything can be fixed.  But the whole point of the distro is to have this all working out-of-the-box, with no changes left for the user except maybe a simple theme change."
    author: "ac"
  - subject: "Confusing..."
    date: 2005-05-08
    body: "If you look at the details, you'll find that the survey also says that more than half of the KDE users don't use Konqueror as thier main web browser or KMail as mail client. In other words: The majority of the KDE users prefer Firefox/Mozilla to Konqueror and Evolution/Thunderbird/Mozilla to KMail.\n\nI doubt in the correctness of these numbers, but if that would be true it would almost degrading KDE's results to KWin being the most used window manager..."
    author: "furanku"
  - subject: "Re: Confusing..."
    date: 2005-05-08
    body: "Its a matter of how you look at it :)\n\nAssuming that all Konqueror uses and KMail uses are among the KDE users, there are plenty of Firefox and Thunderbird users who are using them on KDE, despite these applications being sometimes mistaken for applications developed for other desktop environments."
    author: "Kevin Krammer"
  - subject: "Re: Confusing..."
    date: 2005-05-09
    body: "Since when is Mozilla a XFCE application .. now that you speak about other desktops .."
    author: "ac"
  - subject: "Re: Confusing..."
    date: 2005-05-09
    body: "I think what Kevin is alluding to is the fact that in the past some people have \"claimed\" Mozilla (the predecessor of both Firefox and Thunderbird) and OpenOffice for GNOME while they haven't been integrated in any way at the time, let alone been native apps (which they probably won't ever be).  \n\n"
    author: "cm"
  - subject: "Re: Confusing..."
    date: 2005-05-10
    body: "Exactly.\n\nThat's why I wrote \"mistaken for\" :)\n"
    author: "Kevin Krammer"
  - subject: "Re: Confusing..."
    date: 2005-05-10
    body: "As a KDE/Firefox user, my opinion is that the numbers are correct.  Konqueror is a great file browser, but it's just not as good as Firefox at web browsing.  I'm sure I'm not the only one who has made this observation.\n\nA lot of Linux users tend to also be web snobs of some sort or another.  As of today, Gecko just does a better job than KHTML.  KHTML is a great second choice and it's getting better every day, but it's still not there (yes, bugs are filed and being worked on, I'm not just uselessly complaining on the dot...).  Opera has some interesting advanced CSS support, but it lacks VERY simple things like DOM createContextualFragment support.  So Opera is #3 in my book.  IE (using Wine) brings up the rear at #4.  Again, all of this only applies to web snobs--if you think IE is fine except for all the security issues, then KHTML is likely more than adequate for your needs.\n\nI don't mean this in a condescending way (I probably sound like it anyway) but I do different stuff with a web browser than most people.  Not better stuff, just different stuff.  And some software is better suited for that different stuff than others.  Right now that means I choose Firefox.  Maybe sometime soon it won't.\n\nI recently switched to KMail from Mozilla mail, and I definitely see some shortcomings.  Because HTML mail tends to be much simpler and easier for KHTML to understand, it does just as well as Mozilla/Thunderbird regarding HTML mail.  But there's plenty of little things, like replies starting inside the quoted message instead of a few lines above (why?  is everyone just used to hitting enter a few times to create spacing?), lack of format=flowed support, and simple things like making new messages appear in *bold*.  (Actually I think this last one is recently fixed, and in all fairness I haven't checked it out since).  I still choose KMail over Thunderbird because I like my attachments to open in the correct application.  (KMail *sigh* occasionally doesn't even get this one right 100% of the time, but I think that'll be fixed soon).\n\nAnyway, I do not intend to \"degrade\" KDE with my choices.  KDE is the only desktop worth my consideration.  Period.  I would sooner use bash than Gnome.  I see a bright future for KDE applications (and Qt applications in general, for that matter).  They are maturing and, once they meet my needs, I will switch over to them in a heartbeat.\n\nAlso, I hardly use \"Office\" software at all.  Because of this, KOffice is fine for my needs although \"heavy\" word processing & spreadsheet users say OpenOffice is better.  They're probably right, but for my needs, KOffice is just as good--and I believe that the KDE integration and clean design will make my choice a good one in the long term.\n\nMany distros default to KDE+Firefox+OpenOffice (SuSE is one).  It's not surprising to me that this is a common combination in the wild.  Not so much because people just \"go with the defaults\" (that's part of it too I'm sure) but because there must be reasons those distros made those the defaults to begin with."
    author: "ac"
  - subject: "Re: Confusing..."
    date: 2005-05-11
    body: "> I recently switched to KMail from Mozilla mail [...]\n\nWell, I'm using KDE as my main desktop but I have a Win XP system in parallel running Firefox and Thunderbird. At a first glance TBird is a nice email application. But when you work with it you realize pretty soon the limitations. I was absolutely surprised to see TBird suppressing ICS attachments when viewing a message. You can see the data in the source view, but you can't handle the attachment in any way. Think about Kontact and you'll know what can be possible with OSS.\n\nMaybe this work pattern is uncommon for most of the TBird users. Nevertheless I believe that the rumours about TBird are to high compared to it's functionality. OTOH KMail seems to be unknown even to a lot of people running KDE despite it's functionality.\n"
    author: "andreas"
  - subject: "Re: Confusing..."
    date: 2005-05-13
    body: "For me it's the other way around.\n\nWhile KDE is wonderfully polished in most respects and just plain rocks, my penchant for conserving RAM and CPU cycles means that I still feel drawn to FVWM with it's blistering speed and unsurpassed configurability.\n\nThe main reasons I stay with KDE are Konqueror and KMail.\nSomething about how they look and feel just suits my tastes perfectly.\nI also love Konqueror's highlighting-as-you-type spell checking in web forms.\n\nI know they both can run happily under FVWM, but as they, together with Emacs, are the apps I use most of the time, causing the KDE libs to be loaded anyway, I figure I might as well use an environment where they fit in.\n\nAnyway, having the option to choose from top notch apps like Konqi and Firefox is great.\n\nJust one question about something you wrote:\n\n> But there's plenty of little things, like replies starting inside the quoted message instead of a few lines above\n\nAre you referring to top quoting as a positive thing?!?!?\nHeathen scum! ;-) (he, he, just kidding... well sort of anyway :-)"
    author: "Kim"
  - subject: "Re: Confusing..."
    date: 2005-05-13
    body: ">Are you referring to top quoting as a positive thing?!?!?\n\nAhem. That should of course be top posting.\nSome things even a spell checker won't catch."
    author: "Kim"
  - subject: "Re: Confusing..."
    date: 2005-05-25
    body: "I don't think so.... Suse & Mandrake are two of the top distro's I am sure that these and other distro's put KDE in as the default desktop... but firfox as the default web browser...... Firefox seems to work with more sites (Konqueror works with all the sites I use regularly thankfully) and has a lot of very cool extensions for it.\n\nKDE happens to be the desktop that works best for me. There are some other excellent applications that would make me concider KDE as best Desktop environment not just Window manager....\n\nAmarok is by far the best media player I have used... K3B is an excellent CD Burning program... Kate is my choice as programming Editor. Digikam is a really nice photo manager. "
    author: "Danni Coy"
  - subject: "Congratulation.."
    date: 2005-05-09
    body: "To all on this great project."
    author: "Huber Daniel"
---
In the <a href="http://www.desktoplinux.com/articles/AT2127420238.html">2004 Desktop Linux Market survey</a> (concluded in January 2005), 3841 <a href="http://www.desktoplinux.com/">DesktopLinux.com</a> readers weighed in with their choices for amongst others Linux distributions, email clients and web browsers. Particularly interesting are the result of the windowing environments poll: in just a year, KDE grew from 44% to 61%. Congratulations to the KDE team for such amazing results.

<!--break-->
