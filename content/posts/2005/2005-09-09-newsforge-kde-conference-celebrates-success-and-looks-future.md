---
title: "NewsForge: KDE Conference Celebrates Success and Looks to Future"
date:    2005-09-09
authors:
  - "binner"
slug:    newsforge-kde-conference-celebrates-success-and-looks-future
---
<a href="http://hardware.newsforge.com/">NewsForge</a> <a href="http://hardware.newsforge.com/article.pl?sid=05/09/07/0326219">summaries the activities</a> at the recent <a href="http://conference2005.kde.org/">KDE Developers and Users Conference 2005 "aKademy"</a>. Thanks again to <a href="http://conference2005.kde.org/sponsors.php">our conference sponsors</a> who made it  possible, spearheaded by premium sponsor <a href="http://www.trolltech.com/">Trolltech</a> and followed by the gold sponsors <a href="http://www.suse.com/">Novell/SUSE</a>, <a href="http://www.hp.com/">Hewlett-Packard</a>, <a href="http://www.visitacostadelsol.com/">Costa de Sol</a> and <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/ceydt/indexPadre.asp">Junta de Andalucia</a>.




<!--break-->
