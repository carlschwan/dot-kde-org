---
title: "Qt 4 Resource Center: The Look of Qt 4 RC 1"
date:    2005-06-15
authors:
  - "Anonymous"
slug:    qt-4-resource-center-look-qt-4-rc-1
comments:
  - subject: "Real world advantages?"
    date: 2005-06-15
    body: "I realize this is way too early to go around asking about this... but are there already any examples of applications using new features of QT4 where there are clear benefits showing? Or are there apps and developers who already have ideas of how to use new features, translated to improved functionality?"
    author: "ac"
  - subject: "Re: Real world advantages?"
    date: 2005-06-17
    body: "For example http://oisch.blogspot.com/2005/06/combination-of-two.html"
    author: "Anonymous"
  - subject: "AA ?"
    date: 2005-06-15
    body: "Too bad the text in the screenshots is not antialiased."
    author: "thom"
  - subject: "Re: AA ?"
    date: 2005-06-15
    body: "Why?  The fonts look fine the way they are.  Very crisp."
    author: "ac"
  - subject: "Re: AA ?"
    date: 2005-06-15
    body: "hum, just a matter of taste.\nhttp://qt4.digitalfanatics.org/articles/images/rc1/qtrc-14.png\nI agree that the small fonts in the ui look nicer but I don't like the bold ones in the titles. "
    author: "thom"
  - subject: "Re: AA ?"
    date: 2005-06-15
    body: "Ok, wow that looks very bad."
    author: "ac"
  - subject: "byte_code_interpreter"
    date: 2005-06-15
    body: "need to enable byte_code_interpreter in freetype for good looking non- antialiased text there!"
    author: "fast_rizwaan"
  - subject: "menu-spacing?"
    date: 2005-06-15
    body: "It seems that they haven't thouched the spacing of the menu's in the menubar. The current system looks a bit cramped. The menus are too close together."
    author: "Janne"
  - subject: "Re: menu-spacing?"
    date: 2005-06-15
    body: "The menu space distance is style specific."
    author: "Anonymous"
  - subject: "Re: menu-spacing?"
    date: 2005-06-15
    body: "Did you actually look at the screenshots? The space next to \"File\" is almost as wide as the word \"File\"."
    author: "Anonymous"
  - subject: "Re: menu-spacing?"
    date: 2005-06-15
    body: "Did I look at the screenshots? Yes I did. And comparing it to my KDE-desktop, it does seem that the distance between menus has been increased a bit. Although it could be a bit bigger still, it's certainly a step in the right direction :)"
    author: "Janne"
  - subject: "Glyph spacing ???"
    date: 2005-06-17
    body: "At the risk of being redundant: I am interested in one major feature.\n\nCan I print un-hinted glyphs using the metrics in the font file?\n\nCan I display an AAed approximation of this on the screen?\n\nOthers might also want to know:\n\nIs it possible to print glyphs hinted at the printer resolution?\n\n[needed for Windows emulation {which OO has}]\n\nIs it possible to display an AAed approximation of this on the screen?\n\nIf Qt isn't going to provide this, we are going to have to figure out how to do it ourselves.  Currently, KWord is crippled due to the lack of these capabilities.\n\n-- \nJRT "
    author: "James Richard Tyrer"
---
The <a href="http://qt4.digitalfanatics.org/">Qt 4 Resource Center</a> site has taken a <a href="http://qt4.digitalfanatics.org/articles/rc1.html">look at the examples and demos</a> supplied with <a href="http://dot.kde.org/1118411746/">the recent Qt 4 release candidate</a>. The article is illustrated with many screenshots.

<!--break-->
