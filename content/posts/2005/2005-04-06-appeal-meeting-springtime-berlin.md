---
title: "APPEAL Meeting: Springtime in Berlin"
date:    2005-04-06
authors:
  - "ateam"
slug:    appeal-meeting-springtime-berlin
comments:
  - subject: "Berlin"
    date: 2005-04-06
    body: "Berlin rocks. I was at WOS 3 and quite amazed about Berlin. "
    author: "kubu"
  - subject: "Re: Berlin"
    date: 2005-04-06
    body: "i agree. i really didn't get to see much of Berlin as we were pretty busy. i arrived in Berlin just in time to boot over to the hotel, check in, have a shower, change clothes then take the train to the meeting location. i was 5 minutes early for that meeting, after traveling for ~14 hours. and that was pretty much how it went the entire time. i did manage to get out on Saturday evening and met a number of really nice people, but next time i'm in Berlin i would like to get out during the day and visit some of the museums and art galleries.\n\nBerlin also struck me as quite unique compared to the other cities and towns i visited last time i was in Germany... \n\nfirst we take Manhattan? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Berlin"
    date: 2005-04-06
    body: "Enjoyed your interview on http://thelinuxbox.org/  Very nice to hear about all that is coming up and being developed for KDE.\n\nAll of the hard work is appreciated.\n\n"
    author: "David"
  - subject: "nice work!"
    date: 2005-04-06
    body: "Sounds fantastic.  It's really great that these companies are sponsoring KDE and taking usability so seriously.\n\nThere is just one problem.  Please explain why you capitalize APPEAL?  Is that supposed to mean something, or are you just shouting?"
    author: "dc"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "> Please explain why you capitalize APPEAL?\n\ni didn't want to capitalize it, but a couple of the others thought it would be a good idea. maybe they'll change their mind ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "Or at least come up with a proper meaning for the APPEAL acronym.  Then you can have an excuse to capitalize it. ;)"
    author: "dc"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "'APPEAL: Please, Enrich, Alleviate, Love' ?\n\nHows that?"
    author: "Corbin"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "Bad."
    author: "anon"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "Hm... I forgot a 'P'!"
    author: "Corbin"
  - subject: "Re: nice work!"
    date: 2005-04-07
    body: "That would be 'pretty please' ;-)"
    author: "Jasper"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: ">i didn't want to capitalize it\nNot a big surprise for anyone who have seen your writing:-)"
    author: "Morty"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "That was mean. =D"
    author: "ac"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "...and true to the letter!  ;-P"
    author: "ac"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "i'll be the first to say that Morty was spot on =P"
    author: "Aaron J. Seigo"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "> It's really great that these companies are sponsoring KDE\n\nYeah, this and SUSE 9.3 will talk all \"Novell is all about GNOME\" trolls a lesson."
    author: "Anonymous"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "What about SUSE 9.3?  Good news for KDE?"
    author: "anon"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "Several Gnome people were saying that SUSE 9.3 would get rid of KDE (or at least make it an optional install) and make Gnome the standard desktop.  This was somewhat based on Ximian's presence within Novell.\n\n9.3 is still KDE based, and several KDE developers still work at SUSE/Novell."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "I don't think there was any doubt about 9.3 being KDE-based.  What they said was that GNOME support would be improved.  Novell is increasing GNOME commitment with bounties, etc.  I'd like to see them do the same for KDE.\n\nMaybe they already are, but it's hard to tell.  All the Novell noise comes from Ximian people, not the SUSE group and predominantly about GNOME.  That too, is bad for KDE in a way."
    author: "anon"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "> What they said was that GNOME support would be improved.\n\nThey did by bumping the GNOME version by 0.3.9 and now ship a current GNOME version.\n\n> Novell is increasing GNOME commitment with bounties\n\nDon't think so. AFAIK their Ximian department once assigned an amount for bounties (was it 20000$?) and then listed tasks for some x000$ only. Later for some other x000$. I think there is still money left for bounties from that initial amount. Maybe they are running out of ideas or the bounties were to controversial?\n\nThe SUSE way btw seems to be to hire developers or offer internships.\n\n> I'd like to see them do the same for KDE.\n \nIt's very controversial if bounties can have an overall positive impact on a project.\n\n> All the Novell noise comes from Ximian people\n\nLike praising Beagle shipping in SUSE 9.3 which is said to be found too buggy shortly before release so that it had to be disabled by default? :-) And the Ximian people have to make some noise: if the NLD doesn't sell better they soon have a big problem."
    author: "Anonymous"
  - subject: "Re: nice work!"
    date: 2005-04-08
    body: "The problem with Ximian is not \nGnome vs. KDE\nthe problem is fair play. FUD from Ximian shows the lack of personality.\n\nI think SUSE shall tell Novell to ask them to shut up and get Ximian under their control."
    author: "hannes g\u00fctel"
  - subject: "Re: nice work!"
    date: 2005-04-06
    body: "When you talk about \"people\" (as in \"KDE people\" or \"Gnome people\" or even \"Computer people\"), you include a wide group.\n\nWhen I used the term \"Gnome people\", I was referring to some people I know in real life, a couple people on random IRC channels and a couple on LiveJournal.  I wasn't referring to anybody actually involved with Gnome development or Ximian.  I've also seen plenty of people saying that SUSE is dead because Novell will kill it, either through inept handling or through over-commercializing it[1].\n\nOf course, there are \"KDE people\" who will tell you equally outrageous claims about KDE, I'm sure.\n\n[1] On an interesting side note, I've seen several people cite the \"death of SUSE\" as a reason to have or plan to move to Kubuntu.  No idea if it means anything at all, but it certainly seems that Kubuntu is the new darling distro."
    author: "Evan \"JabberWokky\" E."
  - subject: "No specifics?"
    date: 2005-04-06
    body: "I was hoping that some of the ideas thought up would be mentioned, or a link to a page with some :-(.  Looks like I'll have to wait for KDE 3.5/4.0.  Great work with KDE 3.4, I love all the new features and the speed increase, though KDE 3.4 on my laptop seems a bit less stable than 3.3, though I only saw the stability issues after upgrading from fc2 to fc3 (while still using the same binaries from the previous version), so I'm suspicioning that it may not be a KDE issues (no stability problems w/ Gentoo on my desktop).  I'm really excited for KDE & QT 4.0, and hopefully before KDE 4.0 is released I will have contributed some code :-)."
    author: "Corbin"
  - subject: "Re: No specifics?"
    date: 2005-04-06
    body: "> I was hoping that some of the ideas thought up would be mentioned, or a link\n> to a page with some \n\nyou wont' have to wait for 3.5/4.0... the last paragraph says, \"A web site, descriptions of the current APPEAL projects and outreach to the broader KDE, Open Source and business communities is underway.\" it's coming ... it just takes a bit. i've been back for about a week now and have been working feverishly will many others to summarize and organize what we've done, what we're doing and where we're going ...\n\ni did an interview with The Linux Box Show that will be released in the next day or two that also goes into some more specifics.\n\nso ... patience .. it's coming. it's just a LOT of work and we want to do it right =)"
    author: "Aaron J. Seigo"
  - subject: "Linux Box is up"
    date: 2005-04-06
    body: "The Linux Box is up:\nhttp://thelinuxbox.org/index.php?subaction=showfull&id=1112764056&archive=&start_from=&ucat=22&\n\nGood to see some support from Novell."
    author: "Ian Monroe"
  - subject: "Re: Linux Box is up"
    date: 2005-04-06
    body: "A Brief history of the K Desktop Environment @ 4:58\nAn Interview With Aaron Seigo about The Appeal Project @ 10:24"
    author: "Anonymous"
  - subject: "A trouble with startup ...."
    date: 2005-04-06
    body: "Sometimes in the start KDE (I use kdm) the font size change from 12 to 9 ...\nis a bug? my X configuration? (Archlinux, kde 3.4)"
    author: "......"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-06
    body: "hard to say without more information, but this forum is probably only marginally better than standing in your bedroom closet and asking the question as far as results will go.\n\ni'd recommend contacting the Arch linux team or asking on the KDE users email list or even coming by #kde on irc.freenode.net and asking there =)"
    author: "Aaron J. Seigo"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-06
    body: "I could also recommend the gentoo channel on freenode and the gentoo forums. They're always helpful even if you use a different distribution.\n\nOne potential problem could be some old config file in ~/.kde - if the problem is tied to one user that's likely the case. If KDE has one problem it's that sometimes whacky settings are in whacky files for whacky reasons (especially the file associations and the konquerorrc are problematic in this regard =)"
    author: "noone"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-06
    body: "I've experienced the same on a SuSE 8.2 system.  Logging out and back in always made the font size return to normal (without me changing any config).  I never found the reason for this strange behaviour, so I went with the workaround ... \n\nI think it has gone away at some point because it hasn't happen in quite a while. \n"
    author: "cm"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-07
    body: "I may have a solution for this. If the size change isn't reflected in your configuration (i.e. that kcontrol actually doesn't show a difference in font size), this may be because X is unable to properly detect your screen.\n\nTry running the following command and compare the results from the two behaviours:\nxdpyinfo |grep -e resol -e dimen\n\nIf they differ X some times does not properly detect your screen. There is settings for doing this manually in xorg.conf/xf86config  but i dont remeber the exact settings, i think it was NoDCC or NoEdid or something - try google or the xorg manual. \n\nAlternatively recycling power on myscreen and reseting X often helped for me when i had this problem (new gfcard and screen so thats why i can't be more specific)\n"
    author: "odborg"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-08
    body: "I have the exact same issue.  I'm pretty sure it's because the X server is choosing different dpi on my screen sometimes, so it's not a KDE issue.  I've never bothered to look into it because I like the smaller size, when it shows up, but also the larger size when it shows up.  I'm easy to please, I guess."
    author: "kundor"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-08
    body: "> I'm pretty sure it's because the X server is choosing different dpi on my screen sometimes, so it's not a KDE issue.\n\nI also think it's because of the dpi used.  I recently played around with the dpi setting, and I saw that this can lead to really grotesque layout of the whole dekstop if the fonts are too large, or to unreadable fonts if they're too small for the resolution.  Changing between 75 dpi and 100 dpi has a huge impact. \n\nAny idea *why* this can happen sporadically, without changing anything in the config?  It only ever happened to me on one of my Linux systems... \n\nIf it's some auto-detection thing that *sometimes* goes wrong then maybe it would help to set the used dpi with the -dpi option of the X server so the auto-detection is not needed at all?  I don't think all distros specify this option in the display manager config.  \n\n"
    author: "cm"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-08
    body: "when i upgraded from a creative geforce2 to a albatron geforce fx 5900xt on my main desktop this problem went away. I now use the creative in another pc with another screen, and haven't seen the problem there. I suspect that the creative and the screen i then used on my main desktop (sony 420gs) had problems working together wrt dcc."
    author: "odborg"
  - subject: "Re: A trouble with startup ...."
    date: 2005-04-09
    body: "Ironic -- I got the problem on a geforce FX 5900.\n\nPerhaps it's something to do with the nvidia drivers."
    author: "kundor"
  - subject: "Real problem"
    date: 2005-04-07
    body: "\"If you look at media players, more and more they're moving away from reflecting the file system and more towards using the meta data contained in the media files to build playlists and show you your actual music files. There's a growing disconnect in media players between what's on the file system and what you're looking at.\"\n\nYes, a real problem and no advantage.\n\nLook what happened on Windows! When you install picture viewers and image editing programs each of them creates its own home directory with its own database of images. Same for music players. Same for music exchange. Under windows it is a non-standardized mess. Windows MediaPlayer and say RealAudioPlayer are by no means similar in organising the content. \n\nKDE applications will also mess up that. So what is really needed is one single comprehensive standard for default directories and metadata for music files and so on. "
    author: "Andre"
  - subject: "Re: Real problem"
    date: 2005-04-07
    body: "The big problem with image viewers on Windows is that they don't deal with file moves very well. When you're coming up with the KDE Way to track metadata and meta-collection, please consider using inodes, so it can cope with all of this."
    author: "Joe Lorem"
  - subject: "Software Installation - autopackage"
    date: 2005-04-20
    body: "Are there any plans to embrace autopackage with KDE4?  What I mean is, is there a way to embed autopackage support into KDE?  Perhaps through KControl.  Perhaps a konqueror plugin to recognize .desktop file embeded in the web page (this idea is from autopackage.org).  Add drag and drop support for that desktop file to permanently install the package or to uninstall it?  I know autopackage isn't ready, but if KDE embraces it as it is, even with its current state of development, more developers will use it.  The only problems we're seeing right now is issues with gcc 3.2 - 3.4 and QT/kdelibs .  Hopefully this will be fixed with QT4.  I realize this will probably reuquire a custom front-end to package, but like I said, if you build it they will come.  And besides, autopackage promises backward compatibility so it wouldn't be that big of deal to go ahead and support it with its current state.  This would also be great for updating KDE without having to use distro RPMs.  We could just have a system updater."
    author: "Carl"
  - subject: "Re: Software Installation - autopackage"
    date: 2005-04-20
    body: "> The only problems we're seeing right now is issues with gcc 3.2 - 3.4 and QT/kdelibs . Hopefully this will be fixed with QT4.\n\nThat's no issue between gcc and Qt/kdelibs but an issue between different C++ ABI versions in gcc. This means that Qt4 will definitely not fix the problem of autopackage and C++.\n\nThe fix will be a stable C++ ABI and all distros shipping at least this gcc version. I don't know if gcc 4.0 will offer it, but even if it does it will take a long time (years) until all distros switched to gcc4."
    author: "ac"
---
A group of KDE contributors converged on Berlin, Germany for five days at the end of March to collaborate on a variety of KDE related projects. The scope was broad and included artwork, human interface guidelines, Tenor (a contextual linkage engine), alternatives to traditional file managers and groupware applications. Graciously sponsored by <a href="http://www.novell.com">Novell</a> with additional support from <a href="http://www.basyskom.de/">basysKom</a>, <a href="http://www.credativ.co.uk/">credativ</a> and <a href="http://www.relevantive.com">Relevantive</a>, this group of individuals came from as far away as Canada and from as near by as 15 minutes across town.
<!--break-->
<p><img title="Appeal Dynamics" alt="Appeal Dynamics" src="http://aseigo.bddf.ca/dms/1/163_appeal1.jpeg" width="234" height="175" border=0 style="float:left; padding: 2px; margin: 3px; border: 1px solid #3D4040;">During the intense day-long meetings attendees broke out into small focus groups, occasionally coming together for presentations and brainstorming sessions. The creation of eye catching, usable interfaces was a common thread in these sessions, which often extended well into the evening hours.</p>    

<p>In attendance were artists Frank Karlitschek, Torsten Rahn, David Vignoni, and Ken Wimer who are working on a wide array of artwork for KDE. Joining them were usability experts Jan M&uuml;hlig and Ellen Reitmayr who explored how to refine the most commonly used interfaces in KDE, such as Konqueror and the KDE PIM applications. A collection of KDE hackers, including Stephan Binner, Eva Brucherseifer, Daniel Molkentin, Hans Oischinger, Kurt Pfeifle, Cornelius Schumacher, Aaron Seigo and Scott Wheeler helped bring technical expertise to the meeting.</p>

<p><img title="Appeal People" alt="APPEAL People" src="http://aseigo.bddf.ca//dms/1/164_appeal2.jpeg" width="190" height="175" border=0 style="float: right; padding: 2px; margin: 3px; border: 1px solid #3D4040;">This first APPEAL meeting provided a hot-house for focused, interdisciplinary KDE development. Looking forward, the group aims to grow organically in scope as others with a similar drive for realizing visual beauty, interface clarity and technical creativity in KDE come together. Additional APPEAL meetings are already being planned.</p>

<p>A web site, descriptions of the current APPEAL projects and outreach to the broader KDE, Open Source and business communities is underway. Until then, enjoy <a href="http://aseigo.bddf.ca/albums/appeal2k5">some pictures taken during the event</a>.</p>
<img title="APPEAL" alt="APPEAL" align="center" src="http://aseigo.bddf.ca/dms/1/162_appeal.png"width="537"height="140" border=0 ></div>


