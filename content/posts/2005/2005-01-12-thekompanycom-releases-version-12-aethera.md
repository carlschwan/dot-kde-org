---
title: "theKompany.com Releases Version 1.2 of Aethera"
date:    2005-01-12
authors:
  - "jriddell"
slug:    thekompanycom-releases-version-12-aethera
comments:
  - subject: "Free commercial software?"
    date: 2005-01-12
    body: "and commercial GPL software?\n\nThat's an oxymoron.\n\nHow can you sell GPLed software, since the 1st person you sell it to can distribute it freely!\n\nAm I missing something?\n\n"
    author: "RMS"
  - subject: "Re: Free commercial software?"
    date: 2005-01-12
    body: "Aethera is commercial Free Software available at no cost under the GNU GPL <b>with some proprietry plugins available to add extra features</b>."
    author: "blacksheep"
  - subject: "Re: Free commercial software?"
    date: 2005-01-12
    body: "It's been said before, but yes, you're missing something.\n\nThere are companies that sell commercial GPL software and manage to post consistent profits.  The value of third-party redistributed code drops precipitously when that third party can't support the software as well as the original vendor.  Also keep in mind that only someone who has purchased your product can create and distribute derivative works--if you don't sell your software to people with software development interests, the risk is low.\n\nThe former is why RedHat (only one example) continues to do well in spite of many RedHat-derived distros.\n\nSo I think it's pretty much settled that you CAN make money selling GPL software.  The only question is how much money can you make before you erode your advantage over your potential sublicensees.  And that's what you go to business school to learn.\n"
    author: "ac"
  - subject: "Re: Free commercial software?"
    date: 2005-01-13
    body: "It's funny the initials you would use to post that message.  One of the first things the real RMS was doing in the early days of the GPL was selling copies of Emacs."
    author: "David Walser"
  - subject: "Yes!"
    date: 2005-01-12
    body: "You miss something ;-)"
    author: "verstehichnich"
  - subject: "A few years ago, this would've been news ..."
    date: 2005-01-12
    body: "But with Kontact, evolution etc. now very mature and doing more than aethera, who cares?\nAethera was in there before all the other PIMs we now have but I think the product was dead in the water quite a while back."
    author: "Jon Scobie"
  - subject: "Re: A few years ago, this would've been news ..."
    date: 2005-01-12
    body: "Which of those run on Windows?"
    author: "Anonymous"
  - subject: "Re: A few years ago, this would've been news ..."
    date: 2005-01-12
    body: "OK, this is my third attempt at responding to your question and I've contradicted myself so many times that I now see one feasible reason why you would want to use Aethera. If your company was heavily cross platform (mixed windows/linux desktops say) and wanted to use a groupware server that could be used in both environments, then maybe I see your point. However, I see this as quite a small market but I stand to be corrected on this.\nMost businesses plum for one or the other. My point was that if you are an MS shop, you'll be using Outlook. If a linux one, why buy Aethera?\nIf you are migrating your desktops, so many options ..."
    author: "Jon Scobie"
  - subject: "Re: A few years ago, this would've been news ..."
    date: 2005-01-12
    body: "No one is selling Aethera, it is free.  We are selling the optional plug ins such as instant messaging and whiteboarding and Voice over IP, these are all multiple platform and there isn't anything else going that, not to mention the Citadel and Kolab support."
    author: "Shawn Gordon"
  - subject: "Re: A few years ago, this would've been news ..."
    date: 2005-01-12
    body: "With Windows XP, who cares for MacOS X?\nWith OpenOffice, who cares for KOffice?\nWith KDE, who cares for GNOME?\nWith Intel, who cares for AMD?\nWith KTM, who cares for Honda?\n...\nMaybe it's good to have a choice?!?\n"
    author: "Birdy"
  - subject: "\"It supports a number of groupware servers\", hu?"
    date: 2005-01-12
    body: "The posting claims that it supports \"a number of groupware servers\", the announcement just mentions support for two, Kolab and Citadel:\n\n  http://www.thekompany.com/press_media/full.php3?Id=278\n\nDoes it support Kolab 2 or just Kolab 1?\n\nWhat other servers does it support, OpenGroupware or phpGroupware?"
    author: "Urks"
  - subject: "Re: \"It supports a number of groupware servers\", hu?"
    date: 2005-01-13
    body: "Kolab1 only\nCitadel\nMore Aethera has support for Shared Folders for both above servers.\n\nNo OpenGroupware and phpGroupware support yet.\nSo, you are right there is support for only 2 groupware servers now.\n"
    author: "Eug"
  - subject: "This is good news"
    date: 2005-01-13
    body: "What's with all this \"if you are using windows you are using Outlook\" stuff anyway? Outlook is horrible (from a security standpoint), and we need all the alternatives we can get."
    author: "mr98ai"
---
<a href="http://www.thekompany.com/">theKompany.com</a> has announced the 1.2 release of <a href="http://www.thekompany.com/projects/aethera/">Aethera</a>, a personal information management suite for GNU/Linux, Windows and MacOS X.  Aethera is commercial Free Software available at no cost under the GNU GPL with some <a href="http://www.thekompany.com/products/aethera/">proprietry plugins available to add extra features</a>.  Calendaring support is provided by the popular <a href="http://korganizer.kde.org/">KOrganizer</a> application from KDE.  It supports a number of groupware servers including KDE sister project <a href="http://kolab.org/">Kolab</a>.






<!--break-->
