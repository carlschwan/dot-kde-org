---
title: "FOSDEM 2005: Scribus in the Commerical DTP World"
date:    2005-02-11
authors:
  - "jriddell"
slug:    fosdem-2005-scribus-commerical-dtp-world
comments:
  - subject: "PDF import the only really missing feature"
    date: 2005-02-11
    body: "A big Thanks to all Scribus developers! I really love this app for all\nits great features. But what I'm still missing is a PDF import function\nbecause on Linux/Unix there seems to be absolutely no way to edit a PDF\nfile :-/\n\nIs this planned already for some future version? If not, is it just lack\nof developers or knowledge? Franz told me a couple of months ago that a\nPDF parser is missing and he does not know how to write one. I know how\nto do things like that since I had to write 3 compilers already, so I\ncould offer my help in this area.\n\nCheers,\n  Michael"
    author: "Michael Brade"
  - subject: "Re: PDF import the only really missing feature"
    date: 2005-02-11
    body: "You can import PDF-files into KWord (if that's of any help :-) )\n\nRegards, \nB\u00f8rre"
    author: "B\u00f8rre Gaup"
  - subject: "Re: PDF import the only really missing feature"
    date: 2005-02-11
    body: "A new parser (library) for PDF files would be incredibly cool if both Scribus and KWord can make use of it as import filters as well as KPDF for offering further standard compliant PDF parsing beyond what XPDF supports!"
    author: "ac"
  - subject: "Re: PDF import the only really missing feature"
    date: 2005-02-11
    body: "Full ACK. And PDF import seems to be nearly there\n(I'm not a programmer though). I recently tried to\nimport a PDF and found that it's putting all pages on\ntop of each other. If only it would import page-by-page\nor at least allow to select one page this would be a\nmajor breakthrough for all of us who so far need\nto fight with making the full Acrobat version  (not the Reader) \nwork under Linux in order to analyse colors, font-sizes, line-heights,\netc. This would even be sth. which would be really be cool in KPDF.\nI'm working at an advertising agency and we have to analyse PDF files\neven more often than to edit them."
    author: "Martin"
  - subject: "PDF Import"
    date: 2005-02-13
    body: "It would be pretty cool.\n\nUnfortunately, truly accurate PDF import is very hard. Users will want something where the round trip of import -> save -> export produces nearly identical, if not identical, output. Given the extreme variety of things that can be done in PDF, that's a big ask.\n\nThat's not to say it can't be done ... just that getting one that can accurately round trip content may be a bit step past just getting one to import text and graphics ;-)\n\nImporting editable text is also another very tricky thing.\n\nPersonally, I'd like to see the ability to embed PDF unchanged in EPS and PDF output (Scribus currently rasterises it) before something like this, but ... well, different folks different priorities I guess."
    author: "Craig Ringer"
  - subject: "./configure --enable-kde && make && make install"
    date: 2005-02-11
    body: " As one of the kde-diehards, as you so rightfully put it, I am really really loking forward to native kde support. I am *quite* pedantic about look and feel, so it would just be \u00fcber-cool to have the std. kde file and print dialog in Scribus!\n\n Keep up the good work... 'cus it *is* bloody good!\n\n~Macavity\n\n--\nThose who can: Code!\nThose who can't: Praise those who can!"
    author: "Macavity"
  - subject: "Acrobat Reader 7"
    date: 2005-02-11
    body: "I am quite sure that Acrobat Reader 7 will appear at least one week before April 1st, because that's the deadline for the Dutch tax returns. All Dutch enterprises now have to do their tax return electronically, in pdf forms format, and I just read that Adobe has promised the Dutch taxation authority that version 7 will be ready in time."
    author: "Boudewijn Rempt"
  - subject: "Credit Michael Goffioul for KDEPrint, not me!"
    date: 2005-02-11
    body: "It is nice to read something flattering about one self, especially if it is voiced by a member of the phantastic Scribus Team. But just to set the record straight:\n\nIt is not me, who deserves any credit about KDEPrint and kprinter. Michael Goffioul has done practically all of this phantastic work. (I was just the one who wrote the documentation, and who had some feature requests that got implemented by Michael.)\n\nThe NX printing support was done by the developers from NoMachine.com. \n\nAnd we all base a lot of our printing on the work of Mike Sweet from CUPS.org and www.easysw.com fame, when it comes to Linux, Unix or Mac OS X printing. \n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Credit Michael Goffioul for KDEPrint, not me!"
    date: 2005-02-13
    body: "s/phantastic/fantastic FYI"
    author: "ac"
  - subject: "Maybe, Sort of"
    date: 2005-02-12
    body: "I would like to love Scribus, but some fundamentals are keeping it a hobby toy.\n\nThe quasi-stateless sort-of XML file format drags down the performance of the rendering engine. I've never understand why it is useful to restate the color, size shade, scale, style, font name, character style, stroke, etc. etc. just because an 'A' gets kerned with a 'W'.\n\n<ITEXT CAB=\"0\" CCOLOR=\"Black\" CSIZE=\"12\" CH=\"A\" CSHADE2=\"100\" CSCALE=\"100\" CSHADE=\"100\" CSTYLE=\"128\" CFONT=\"Bitstream Vera Sans Mono Roman\" CEXTRA=\"0\" CSTROKE=\"Black\" />\n\n<ITEXT CAB=\"0\" CCOLOR=\"Black\" CSIZE=\"12\" CH=\"W\" CSHADE2=\"100\" CSCALE=\"100\" CSHADE=\"100\" CSTYLE=\"128\" CFONT=\"Bitstream Vera Sans Mono Roman\" CEXTRA=\"0\" CSTROKE=\"Black\" />\n\nTry doing a short 100-page book with that kind of baggage! After about 10 pages, the computer is crawling. If you were driving a mechanical typesetting machine with those commands, you would wear it out. Change the font to 12 pt. Check first to see if you already have 12 pt. If so, then go to the next command. Scale the text 100%. Check first to see if the text is already scaled to 100%. If so, go to next command. This is fundamentally flawed. This is ugly. Books are out.\n\nHow about advertising? Most ads are a single page, Scribus works great for picking up pieces of art and type with your mouse and putting them exactly where you want them, but advertising demands lots of attention to manual kerning. Scribus embraces manual kerning, but you must use points to kern your text. Make an ad, kern the text by hand, send it to the art director. The art director (or client) says they want the headline larger. In the real world of setting type, you make your headline larger, the kerning also automatically scales. But in the Scribus world, you must re-kern everything you scaled. PostScript fonts are created with a unit system, 1000 units per em. In ancient days of photomechanical typesetting, there were 52 units per em. The unit system is an old standard. It works because it makes sense when you have a single font which is scaled mathematically. Kerning by points is stupid. \n\nAnd a half a pica is six points, not .5 pica.\n\nThere, I feel better now. I'm just an old fart, but I did spend years working for Linotype AG, and am a master typographer (if I was still a member of the ITU). I know what I'm talking about and realize that I'm coming across as an asshole.\n\nThe CYMK tiff handling works perfectly for me, and I think the handling of svg graphics is great. Don't wast time writing an import filter for pdf files. You'll never be able to edit them well. Look at how awful it is to edit imported pdfs in Illustrator. Please forget new gimmicky features. Try and figure out a way to fix your file (internally and externally) format. Stateful is beautiful when it is a stream of beautiful glyphs. Fix the basics. \n\n\n"
    author: "Allen"
  - subject: "Re: Maybe, Sort of"
    date: 2005-02-12
    body: "If you had bothered to perhaps read a little more of our plans you might have realised a new file format is due within our 1.3 development cycle. Most of the things you say will be gone in 1.3, if they even still exist in 1.3."
    author: "Craig Bradney"
  - subject: "Re: Maybe, Sort of"
    date: 2005-02-13
    body: "> If you had bothered to perhaps read a little more of our plans\n\n\"(CB) The other attraction ... is the overall friendliness of the community, whether on the mailing list or on IRC.\"\n\nI guess this doesn't include the KDE Dot.\n\nAlso if you live in AU then why do you have a business in Luxemborg?"
    author: "ac"
  - subject: "Don't rant, help!"
    date: 2005-02-12
    body: "Allen,\n\ncan you please submit 5 to 10 well-written bug reports about your findings? That is way better than ranting. Or go to #scribus on IRC (irc.freenode.net) and talk directly to the developers.\n\nThe Scribus bugs database is here: http://bugs.scribus.org/\n\nSo if you really are a long-year typographic pro -- why dont you offer your help directly to the Scribus team?\n\nI am sure, if your arguments make sense (I can't tell, I am not a DTP pro), then the nice people from the Scribus Team are the first ones to listen to and take advice from a pro, and not just dismiss it.\n\nBelittling Scribus as a \"hobby toy\" may be regarded as a bit of offensive, though. Because Scribus *is* already being used to produce professional publications...."
    author: "konqui-fan"
  - subject: "Re: Maybe, Sort of"
    date: 2005-02-12
    body: "Adding to my previous comment. What you say is not entirely correct. \n\n-The XML you quote results from a) hyphenation (needs fixing), b) overly explicit XML. Both of these will be fixed in 1.3. Fixing our file is fairly easy, so dont make a big fuss over it.\n-The slowness you see with large amounts of text has almost nothing to do with a) XML b) the above formatting.\n-Why shouldnt we write a PDF importer? Its not at all gimmicky, especially given the number of requests we have for it. Perhaps being in the industry so long means you are one of the many PDF haters out there.\n\nAnyway, happy to see your bug reports, patches and fixes based on your DTP Pro knowledge on http://bugs.scribus.net."
    author: "Craig Bradney"
  - subject: "XML format"
    date: 2005-02-13
    body: "There are many other reasons to improve the file format, too. As Craig Bradney has noted, that's on the plans already - I understand the development team are aware of the issues and future needs in that area. Scribus doesn't work with the XML as you edit the document, however - it's interpreted on load and generated anew on save - so that's not behind the performance issues you mention.\n\nThe performance issues you've observed with large bodes of text are actually mostly to do with the current redraw and text reflow code. I understand that work is progressing on this, too, though like everything else it depends on time, manpower, and the personal priorities of those doing the work. I'm sure help would be appreciated ;-)\n\nPersonally I think it would be useful if you could file a feature request on http://bugs.scribus.net/ (user account required, but easy to set up) regarding the kerning issue. Assuming there's nothing along those lines up there already, that is - I'll admit I haven't had a look. It seems something worth keeping track of, as I tend to agree with you that one generally wants the kerning to be relative to the font size rather than a fixed point-size adjustment. Complaining about it on the dot won't solve the problem ... but filing a bug will at least make sure it's not forgotten.\n\nI tend to agree on the matter of PDF personally - because it would be _extremely_ hard to get right. Even Acrobat can't do much to edit a PDF without an additional plug-in like PitStop ... and it's by the people who literally wrote the book on PDF. That said, Scribus is an open source project - so if someone wants to write a PDF importer, who's to stop them?\n\nAh well, I think that's enough of my blather for now. As always, all this is just my personal opinion as a regular user of the app and (allegedly) a DTP support pro."
    author: "Craig Ringer"
  - subject: "KOffice & Scribus"
    date: 2005-02-12
    body: "I've been using Scribus a bit lately and am very impressed. I think it would be really cool if it could be worked into the KOffice suite of tools. I'm not saying that the stand-alone version should go, or even be secondary, but something more along the lines of the enable=KDE configure flag.\n\nIf this were done right, I might finally be able to get the working environment I've always wanted, which is to have a light and reliable word processor to allow me to lay down some text, followed by a seamless switchover to a layout program that can work on the exact same file. That division of tasks can be just wonderful for someone who really wants to work at a professional level.\n\n(On a side note, if Scribus could get QuarkXpress and InDesign file support, that would be a godsend for me. I am still at college and am often stuck working on one of the school computers and need to use one of those programs if I want to do layout. I usually end up just using a text editor and trying to ignore the layout until I get home and can merge it into my main project, but that is very far from optimal.)\n\nOverall, thanks for a wonderful tool!"
    author: "jameth"
  - subject: "Quark and ID file import"
    date: 2005-02-13
    body: "Support for importing Quark and InDesign files is a common request. Unfortunately, I sincerely doubt it'll ever happen for a couple of reasons.\n\nThe first is getting it right. If you think OpenOffice has trouble with MS word, imagine something _massively_ more complex, with a format that changes dramatically version to version, whose users won't tolerate a 0.01mm misalignment. Then think about doing it twice. OpenOffice has been trying to get MS Word import right for years - and with a lot more manpower than Scribus has behind it too. This should give you some idea of the magnitude of this task. \n\nIt doesn't help that, in my view at least, inaccurate import can be worse than no import at all.\n\nThink of it this way. Adobe InDesign can import Quark 4 ... but it doesn't get it right all the time, and doesn't support all of the format. Adobe is a large company that spent a huge amount of resources on that. The ability to import Q4 was one of the big things folks needed to transition from Q4 to InDesign ... and even then Adobe couldn't get it to work all the time. It's hard.\n\nThe other issue is legal/risk. Adobe and Quark aren't what you'd call friendly, chummy folks who're easy to get along with. They might also be a bit unhappy about file format support, even if it was perfectly legal reverse engineering. Personally, I don't like the idea of being sued by Quark - even if there's no reasonable leagal basis, how often does that actually _matter_? Bankrupt is bankrupt.\n\nPersonally, I think it'd be cool to support slurping text and images from Quark 6's XML export format (it's made for interoperation after all, right?) to build the skeleton of a Scribus document. Anybody who wants to look into this need only write a plug-in - Scribus plug-ins become fully functional parts of the application, so you can do whatever you need to.\n\nThe format it'd be really cool to be able to import - at least the text, graphics, and rough layout - would be Quark 4. Alas, as anybody who's worked with Quark for long will know, Quark 4 can't always import that reliably :-P . It's an undocumented (as far as I know, anyway) binary bundle of fun, and in my opinion probably a lost cause from an import perspective.\n\nMy comments here are, of course, just my personal opinion."
    author: "Craig Ringer"
  - subject: "Re: Quark and ID file import"
    date: 2005-02-18
    body: "I'm pretty sure that Quark has said publicly that V7 will have a full XML representation of documents as a save option. So there's a fighting chance there.\n\nAnd InDesign CS and later have an \"XML binary dump\" format called INX which would be a start for importing the basics, though it really has a lot of binary aspects to it.\n\nFinally, note that InDesign CS and later have a full cross-platform Javascript scripting implementation (they call it ExtendScript, but it's just the Netscape Javascript engine at heart), and it's quite powerful and very good.\n"
    author: "Chris Ryland"
  - subject: "Re: Quark and ID file import"
    date: 2005-02-25
    body: "Craig,\n\nA most interesting perspective. I can tell you from experience that you are certainly correct. It is hard, very hard to do.\n\nWe have had some experience in the file conversion business ourselves. Two successful commercial products in fact, although they go in a different direction... PageMager to Quark and InDesign to Quark.\n\nThe tricky part, as you correctly pointed out, is that these DTP formats are proprietary and thus generally unintelligible. We have over 10 years experience with this sort of thing and have learned the file format layouts of most of the popular DTP applications including Quark.\n\nOur original purpose (and that for which we are best known) is to preflight these files. That is to say, inspect them for design elements that would cause a print output problem for a commercial printer.\n\nSince we can \"see into\" these file formats, we have now turned this patented technology towards extracting the content into XML. Our goal is not just the content but the layout, page geometry, font use, text and image box positions and size, etc. In short, XML tagged with all the formatting information one would require to reconstitute the original document in another application. So, Quark to XML to InDesign for instance.\n\nYou mentioned \"...slurping text and images from Quark 6...\" which is essentially, what we can do today (for Quark V3.1 to 6.x). In addition, we have developed a plugin for InDesignCS that can read the resulting XML and reconstruct the document.\n\nOf course this is still a work in progress and, as you mentioned, there are many factors (and customer expectations) to be considered.  I can certainly understand the context of your comment \"...inaccurate import can be worse than no import at all...\" but cannot completely agree with it. Our considerable experience with our other conversion products has taught us that some reasonable degree of accuracy is far better than doing the document over from scratch.\n\nNevertherless, your point is well taken and we are determined to overcome the barriers.\n\nOur conversion philosophy goes beyond simply converting X2Y. There are hundreds of hungry XML enabled applications out there and thousands of potential customers with vast libraries of legacy DTP content they would like to feed into their variable data publishing, content syndicating, content repurposing, et.al. engines and we aim to supply the XML conversion technology to emancipate that proprietary content.\n\nDid I mention that this is not an XTension? We can inspect and convert these files as a stand-alone application.\n\nAnyway, it is a rather significant challange but there is hope. If you would like to \"...slurp text and images from Quark...\" and import them into InDesignCS, drop me a note and I'll provide a \u00dfeta of our application, code-named XMLazarus.\n\nRegards,\n\nBob Claborne\nDirector Business Development\nMarkzware"
    author: "Bob Claborne"
  - subject: "Re: Quark and ID file import"
    date: 2006-02-23
    body: "Bob\n\nYour XMLazarus looks like it would have a lot of untapped market potential.\n\nExtracting the text and graphics is exactly what we have to do to be able to recycle our OWN stuff - sometimes I print it then OCR to avoid having to get in the queue for DTP time!\n\nIf you could put us down for a beta we'll give you some feedback\n\n\nSteve"
    author: "Steve Marshall"
  - subject: "Re: KOffice & Scribus"
    date: 2005-07-01
    body: "You could probably write an XSL filter. XSL allows you to transform one XML into another. Try to get more information about it, it's all on the web, and it's probably able to do what you need.\n\nXSL is kind of a transform definition to pass from XML to other formats.\n\nThere are free tools you can use to actually apply the XSL to an XML file to get the desired output.\n\nSo this procedure can act as the bridge you want.\n\nOf course, one needs to know the source and target file formats in order to construct this XLS transformation definition file. But it should not be difficult to get startet if you simply look at some sample XML files of the two programs you want to bridge."
    author: "pj"
  - subject: "Installing Scribus 1.2.1 on Win32 is available now"
    date: 2005-03-14
    body: "As noted on the mailing list recently:\n\n\"Finally, we have an easy to follow recipe for common\nmortals, that simply works. \n\nAll steps for installing binaries, described at http://wiki.scribus.net/index.php/Installing_Scribus_on_Win32\nworked without the slightest problems :)\""
    author: "Hap0"
---
In the second in our series of interviews with speakers in the <a href="http://www.fosdem.org/2005/index/dev_room_kde">FOSDEM KDE developers room</a> Scribus developers Craig Bradney and Peter Linnell talk about the state of desktop publishing on Unix and its acceptance in the commercial DTP World.









<!--break-->
<p><strong>Please introduce yourself and your role in Scribus</strong></p>

<p>(Craig Bradney) I have a consulting company based in Luxembourg. My principal areas of expertise are enterprise network, development and project management. My role is kind of all hands, daily project management, running the bugs tracker, working on code refactoring, testing and managing most of the project's infrastructure. I started actively participating soon after Scribus 1.1.0 came out. </p>

<p>(Peter Linnell) I am an independent IT consultant principally for DTP and publishing companies with expertise in cross-platform networks and business processes. My role is first writing the docs and testing, especially bugs and PDF/PS output from Scribus, along with web-mastering <a href="http://www.scribus.org.uk">www.scribus.org.uk</a>, our portal. I've done this since the early days of Scribus, before there was an organized team, just Franz Schmid coding by himself.</p>

<p><strong>Scribus recently released version 1.2.1, what is new in this version?</strong></p>

<p>(CB) The major news in 1.2.1 is a new OpenOffice.org Writer and Draw importer. Underneath this, there is a new API which makes writing import filters much easier. This is thanks to Riku Leino, our newest team member. The other major news is commercial support for Scribus. That means a) we're confident it is reliable enough and robust enough for commercial applications, b) we have the experience and know how to provide the kind of support needed by institutions or companies large and small.</p>

<p><strong>You are now offering commercial support for Scribus, what sort of services do you provide?</strong></p>

<p>(PL) Both my and Craig's companies will work together to provide whatever services a company or institution needs to properly implement Scribus as a print publishing platform. That can mean Scribus as the main platform or along side existing publishing apps. We also have at our disposal other specialist consultants who are world class experts in areas like, for example, enterprise printing. Other team members will be able to write customised parts if requested and feasible. Note, by inference, we will be supporting KDE desktop in a commercial setting :-)</p>


<p><strong>Scribus uses pure Qt, have you ever considered making Scribus also use KDE?</strong></p>

<p>(CB) We are planning this as a compile time option. No definite date, but we see KDE integration for those who wish to have it as a big plus. Stay tuned. :-) This is something often requested by the KDE diehards out there, and fair enough, KDE is a great platform which most of us use too. The issue comes when you consider the cross platform implementation of Scribus into Mac OSX via Fink (and natively in future) and possibly Windows. Scribus runs on various flavours of *NIX, such as HP-UX, IRIX etc and there's no guarantee of KDE there either. A compile time option for use of native KDE file dialogues and KIO slaves, etc seems to be the best option.</p>


<p><strong>How does Scribus compare to the proprietary competition?</strong></p>

<p>(CB) We do not think of Scribus really as competition for others, more providing the best DTP experience for the Linux/Unix world. Our focus is not to make a feature by feature clone. It is interesting to see some of the mailing list threads by long time users of other DTP apps using Scribus for the first time. They are quick to point out 'Well, Scribus does X and I am used to Y. Why?' Often the reply is 'Well, Scribus does Y, because it is the $*nix, $desktop, Scribus way of doing things.'  This an interesting form of comparison as one can see where Scribus shines. </p>

<p>(PL) That said, I use or have used most every professional DTP app. Scribus strengths:</p>
<ol>
<li>It is open source and all the goodness that brings with it. Not just the price, but the ability to interact directly with developers, the community support, frequent releases with new features and the transparency of the process. </li>
<li>I think users of other commercial DTP apps unaware of Scribus would be floored by the level of support Scribus users get - for free. Our commercial support will be even better and more comprehensive.</li>
<li>It runs on more platforms.  I can't run pro DTP apps on what I consider a superior operating system. </li>
<li>I can say with a straight face its PDF export quality is commercial grade. It is, in my opinion, the best in OSS.  It matches up and/or is superior to many pro DTP apps. </li>
<li>It is less resource intensive. You can run Scribus easily on a PIII. </li>
<li>Its file format is very robust - almost corruption proof - a <strong>big</strong> problem for many DTP apps and especially where networks are concerned. I've been paid very well to beef up networks and customise the clients to make them more reliable for certain DTP apps. </li>
<li>Cross-platform Python scripting. This is a sleeper as Python allows you to extend Scribus with the tons of Python modules available.  Cross platform scripting in pro DTP apps is almost non-existent.  </li>
<li>We have better looking developers :-)</li>
</ol>


<p><strong>How many people are working on Scribus and where do you find such talented developers?</strong></p>

<p>(CB) I think one the secrets has been each one of the devels has very complementary strengths. The other attraction for members is the overall friendliness of the community, whether on the mailing list or on IRC. We have made a serious effort to make Scribus approachable to those either new to DTP or Linux. It makes contributing to Scribus a real joy. The other interesting bit is we are probably slightly older overall than those in the typical OSS project. Our experience in DTP, corporate IT and teaching probably help keep us focused and well grounded on practical issues. </p>

<p>(PL) The main Scribus Team is six officially.  It really did not become an official team until we dragged Craig along. We would also be remiss in not acknowledging some important contributions like Steve Calcott's font sampler script complete with GUI. That showcases how powerful the Python scripter can be. Another would be Alex Moskalenko who took over maintaining the Debian packaging. He made our Debian issues just disappear. Craig Ringer has contributed a macro system for Scribus and helped with the scripter.  There are many many others who have helped in code, translating and in serious real world testing. Honestly, the list is getting to the point where we have to start saying "thanks to all those who we have forgotten to mention". </p>


<p><strong>Have you looked at how the changes in Qt 4 will affect Scribus?</strong></p>

<p>(CB) Yes. Already parts of Scribus are being rewritten to accommodate the changes. Much of the menu code for example has been completely rewritten in CVS to accommodate this.</p>


<p><strong>Scribus is not as user friendly as a word processor, for example you can not apply formatting to individual words.  Have you considered including such word processing features?</strong></p>

<p>(CB) So called, character based styling is coming in the next development version. Please note though that Scribus is well served by the use of well thought out styles prepared in advance - a well understood process used by almost any serious publishing application. You <em>can</em> apply formatting to individual words perfectly well, it can be a bit cumbersome at the moment given the mixed advantages of paragraph styles and per character selection. Also, Scribus has a built in Story Editor which is designed to make the text import/styling efficient. By the same token, you could also say most word processors are not user friendly to DTP users... </p>


<p><strong>What have been the experiences of people using Scribus for real world publishing?</strong></p>

<p>(PL) The largest challenge is some printer's equipment/work-flow do not support the more advanced features of PDF 1.4. Really.  In one case, I got urgent messages from a publisher saying the printer had serious problems with the Scribus PDFs.  The printer had one of the world's most advanced pre-press systems with PDF/X-3 support (Scribus was the very first to support PDF/X-3.) The printer's software feeding it was ancient - MacOS9 and Acrobat 4. I sent them special pre-flight PDF certification reports for the same files. That quickly convinced them the Scribus files were not the problem.  </p>

<p>(CB) Overall, despite most printer's unfamiliarity with Scribus, its worked well. We have books published and newspapers using Scribus. I have some nice examples from a classical music group - CD covers, tickets, posters etc, the end result is stunning. We have had no real show-stoppers and no missed deadlines. I edit a car club magazine here in Luxembourg with Scribus and have it commercially printed in Australia with no real issues. </p>

<p>Also, two of our translators are folks who are in commercial pre-press. They have been really helpful to other printers and our users on the mailing list.</p>


<p><strong>Scribus currently recommends Acrobat as a PDF viewer.  Have you looked at the impressive kpdf in KDE 3.4?</strong></p>

<p>(PL) Yes. It is <em>very</em> impressive to see the improvements. Thanks to Kurt Pfeifle, well known for his work with NX and KDE printing, I was testing the CVS HEAD version of kpdf before the 3.4 alpha was released. I recently added it to my Toolbox section of the docs.  Still, we have to recommend Acrobat Reader to be specific, as it is still the most reliable viewer for Scribus users. Adobe Reader 7 should emerge sometime soon from its' public beta. If it is comparable to the released Windows or Mac versions, this will be a major gain for Linux desktop users. </p>

<p>The <a href="http://docs.scribus.net/index.php?lang=en&sm=dtptoolbox&page=toolbox">DTP Toolbox section of the Scribus docs</a> outlines my preferred recommendations (and reasons why) for certain graphics apps. The main idea was to highlight those which are especially helpful alongside Scribus. </p>

<p>(CB) We do get a few people in IRC getting viewing "issues" with some PDFs from Scribus, but until the free PDF viewers can catch up to some of the features in Acrobat Reader we just cannot recommend much else. Some people simply refuse to install the "non-free" Acrobat Reader as a matter of principal. It seems, especially with KPDF in KDE 3.4, we might be approaching this time where we can really suggest an alternative to Acrobat Reader. The devs of KPDF and others shouldn't stand still though :), we have PDF 1.5 and 1.6 on the target. In fact, we could have had PDF 1.5 support ages ago, although we did not implement it as no viewer on Linux could view it at all.</p>


<p><strong>What is the status of Scribus on Windows and what have been the problems with porting Scribus to Windows?</strong></p>

<p>(CB) The KDE-Cygwin Team has come up with a set of patches to enable Scribus 1.2 to compile and run under Cygwin. We really appreciate their interest and it is indicative of their project's progress that a full featured application can run with the KDE-Cygwin project. They have solved many of the porting issues and improved performance dramatically. As for a pure Windows port, the greatest constraint is developer time and the fact that we are fully involved in working on the next 1.3.x devel series. </p>

<p>We do not want anything to distract from taking Scribus to the next level in features, usability, stability and performance.  We will announce the 1.3 Roadmap leading to Scribus 1.4. We are still discussing this amongst ourselves and, as always, carefully considering the valuable input we get from all sides.</p>


<p><strong>What is your view of the future of DTP on Linux and Unix?</strong> </p>

<p>(CB) Well our aim is not to replace wholesale other applications, but to provide the best DTP experience for Linux users - to provide the same kinds of tools users have had on other platforms and to use the inherent strengths of OSS and Linux/Unix to Scribus' advantage. For many many people, businesses and institutions it's a compelling solution with low maintenance and high reliability. Also, more and more design people are active on the development side. For example, two of the main Inkscape coders are also graphics pros. This helps to bridge the gap between developers and users' wants and features.  </p>

<p>(PL) The technical side of DTP is coming along nicely. GPL Ghostscript 8.15 which was recently released is a major leap in capabilities. We hope all distros will upgrade as soon as possible. Many other applications are understanding the need for and are starting to implement colour management. Many other things which were painful on Linux like font management are now solved. KFontInstaller since KDE 3.3 is fabulous. For end users its easier than even proprietary font managers. What could be easier than: Select font > right click > Install font? The overall quality of end user graphics programs is improving steadily.  </p>

<p>Right now, I would say the biggest weakness from an FOSS point of view is there are few good high quality fonts. It is one of those areas which requires tremendous amounts of QA to make them reliable in the commercial print world. This is highlighted throughout our documentation. </p>

<p>Lastly, there is much goodwill amongst the various teams and projects involved in graphics. Witness our cooperation with Inkscape which uses a different toolkit and sometimes different style of doing things. We do share and learn from each other. When I first worked with Linux, the concept of DTP on Linux seemed very distant. </p>








