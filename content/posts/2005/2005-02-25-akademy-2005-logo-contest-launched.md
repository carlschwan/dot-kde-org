---
title: "aKademy 2005 Logo Contest Launched"
date:    2005-02-25
authors:
  - "jriddell"
slug:    akademy-2005-logo-contest-launched
comments:
  - subject: "requirements"
    date: 2005-02-25
    body: "* Be 300 dpi.\n\nCan a svg-file have a resolution? What does 300dpi help you if you don't specify a physical size?\n\n* Must include a transparent wallpaper based on your logo that is a 1024 x 768\n\nMaybe its just my bad mood this morning, but I think you're setting the barrier for creating logos quite high, considering people do this for fun.\n\nAnyway, good luck!"
    author: "me"
  - subject: "Re: requirements"
    date: 2005-02-25
    body: "I've gotta agree with that. Why require a wallpaper? If one logo is clearly better, a wallpaper can be made based off of it fairly swiftly. Now, if there were a lot of time to work on the logo, that would be a different matter, but the deadline is barely a week away."
    author: "jameth"
  - subject: "Re: requirements"
    date: 2005-03-01
    body: "An image that needs to be printed on posters, flyers, t-shirts etc. need a high resolution to look good. Images on 72 dpi screen resolution won't work, it will look good on screen but not in print.\n\nJust because people do something for fun doesn't mean that they can't deliver high quality work. I have seen some excellent artwork in KDE, and AFAIK that is subject to requirements also.\n\n--\nTink\nFrance"
    author: "Tink"
  - subject: "do we know"
    date: 2005-02-25
    body: "Do we know any of the details of the conference yet (outside of its location obviously)?"
    author: "Ian Monroe"
  - subject: "please send us any additional information (please)"
    date: 2005-02-25
    body: "Greetings, added a link to page at http://www.svgx.org\nIf anyone has any additional information, work etc.\nplease let us know, michael at svgfoundation.org\nThank You\nMichael\n"
    author: "Michael Bolger"
  - subject: "submiting???"
    date: 2005-03-04
    body: "What email address do we send our final design too??? Thanks."
    author: "Terry"
  - subject: "Re: submiting???"
    date: 2005-03-04
    body: "http://www.kde-look.org/content/add.php"
    author: "Anonymous"
---
The KDE project is looking for a great new logo for our biggest event of the year: The KDE Developers and Users Conference 2005, also known as aKademy 2005. This logo will be seen everywhere including websites, on t-shirts and in magazines.  <a href="http://www.kde-look.org">kde-look</a> is hosting <a href="http://www.kde-look.org/news/index.php?id=161">the contest to find the new aKademy logo</a>.

<!--break-->
<h2>Guidelines</h2>

<p>All logo entries must</p>
<ul>
<li>Be in the SVG format.</li>
<li>Be 300 dpi.</li>
<li>Must <strong>not</strong> include any gradients.</li>
<li>Must be original.</li>
<li>Must include a transparent wallpaper based on your logo that is a 1024 x 768 PNG.</li>
<li>Must at least be GPL'd but the KDE Artists clarified LGPL or a less restrictive license may be a better fit for some artists.</li>
<li>Must be submitted in the Artwork Contests section of kde-look.org. Please include the words "aKademy 2005" in the name field of your submission.</li>
</ul>

<h2>Please Note</h2>

<p>The images must not violate any other artists copyright. If you use some artwork that you did not create (e.g. the KDE gear) then you must list the artist and the license or your entry will be disqualified.</p>

<h2>Prize</h2>

<p>The first place winner will receive the privilege of seeing their logo being used for the most publicised KDE event of the year <em>and</em> a t-shirt featuring their logo (provided by the aKademy 2005 committee).</p>

<h2>Judges</h2>

<p>Kenneth Wimer, Tink Bastian, Antonio Larrosa and Skullbooks</p>

<h2>Deadline</h2>

<p>March 7th 2005 (10 days).</p>

<h2>Logo Ideas</h2>

<p>The aKademy is taking place this year in <a href="http://en.wikipedia.org/wiki/Málaga">Málaga, Spain</a>. A reference to this in the logo image would be appreciated although it is not necessary.</p>

<p>The name of the conference in Spanish is "Congreso de Desarrolladores y Usuarios de KDE : aKademy 2005"</p>

<h2>Colour Palette Ideas</h2>

<p>The colours of the <a href="http://www.1uptravel.com/flag/flags/es.html">Spanish</a>, <a href="http://www.1uptravel.com/flag/flags/es-ma-ma.html">Málaga</a>, or <a href="http://www.1uptravel.com/flag/flags/es-an.html">Andalusian flags</a>. Málaga is located in the Spanish Autonomous Community, Andalusia.</p>

<p>The new <a href="http://wiki.kde.org/tiki-index.php?page=Colors">KDE color palette</a>.

<p>Remember the last day to submit your logo is March 7th 2005 so don't wait start working on your images now.</p>

<p>Finally the <a href="http://www.kde-look.org/news/index.php?id=155">digiKam Contest</a> is still running for your super-impose template ideas.</p>

