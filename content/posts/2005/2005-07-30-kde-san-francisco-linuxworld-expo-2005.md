---
title: "KDE at San Francisco LinuxWorld Expo 2005"
date:    2005-07-30
authors:
  - "csamuels"
slug:    kde-san-francisco-linuxworld-expo-2005
comments:
  - subject: "Wanted to register..."
    date: 2005-07-30
    body: "but LinuxWorld Expo signup page doesn't support Konqueror...\n"
    author: "Allen of California"
  - subject: "Re: Wanted to register..."
    date: 2005-07-30
    body: "Appears to work fine if you change the user agent. :/"
    author: "Ian Monroe"
  - subject: "Isn't KDEPrint part of the KDE booth?"
    date: 2005-07-30
    body: "According to Kurt Pfeifle's blog (http://www.kdedevelopers.org/node/1271), he will be at LWE also.\n\nWhy is he not mentioned by Charles? Doesn't KDE coordinate their activities well enough? Isn't KDEPrint part of the KDE booth too?"
    author: "ac"
  - subject: "Re: Isn't KDEPrint part of the KDE booth?"
    date: 2005-07-30
    body: "Kurt will definitely be present, but he will live in the Linux Printing booth (also in the .org pavillion).  You're right that I should have mentioned him though."
    author: "charles samuels"
---
The K Desktop Environment project will again be present with a booth at the <a href="http://www.linuxworldexpo.com/live/12/events/12SFO05A">LinuxWorld Expo</a> being held in San Francisco August 9 through 11.  We'll be at booth #2038, upstairs, in the new Moscone building!  We'll be demonstrating not only KDE 3.4, but also the upcoming KDE 3.5 and even maybe bits and pieces of what will become the revolutionary turning of the Gear KDE 4.0.  We'd be pleased to welcome you.


<!--break-->
