---
title: "Qt 3.3.5 Released and \"A Qt Blog\""
date:    2005-09-11
authors:
  - "binner"
slug:    qt-335-released-and-qt-blog
comments:
  - subject: "Thanks Trolltech"
    date: 2005-09-12
    body: "Nice to have steady updates of Qt3! Looking forward to meeting it in debian :-)"
    author: "ac"
  - subject: "Symbol visibility support"
    date: 2005-09-12
    body: "Does anybody know if this version includes the patch for gcc symbol visibility?\nThe changelog says nothing about it.\nSee http://bugs.kde.org/show_bug.cgi?id=109386"
    author: "ac"
  - subject: "Re: Symbol visibility support"
    date: 2005-09-13
    body: "It doesn't, but the old patch works just fine on 3.3.5. If you're compiling with gcc 4.x though, bear in mind that Qt now \"officially supports\" this compiler, which breaks the binary buildkey versus 3.3.4 (previously it would have been called \"g++-g++ 4.0.1 <-V spiel>\", now it's called \"g++-4\".\n\nAs a result I had to rebuild kdelibs (3.5-alpha1) to get things to play nice.\n"
    author: "Alistair John Strachan"
  - subject: "error: cannot stat libqt-mt.la"
    date: 2005-09-13
    body: "I downloaded and compiled qt-3.3.5 but it is showing error with \"gmake install\", the error is \"Cannot stat /tmp/qt/lib/libqt-mt.la\", the kdebase 3.5svn package needs libqt-mt.la to compile, i compiled qt with -shared, so what's wrong? do i need to compile qt with \"-static\"? thanks."
    author: "fast_rizwaan"
  - subject: "Re: error: cannot stat libqt-mt.la"
    date: 2005-09-13
    body: "I don't think '-shared' is needed. I think '-thread' is needed for multi-threading (mt)"
    author: "Johan Veenstra"
  - subject: "is this ok for libqt-mt.la?"
    date: 2005-09-14
    body: "configure -system-zlib -qt-gif -system-libjpeg -system-libpng -plugin-imgfmt-mng -thread -no-exceptions -release -dlopen-opengl -plugin-sql-mysql -plugin-sql-sqlite -I/usr/include/mysql --prefix=/usr/lib/qt -shared -static"
    author: "fast_rizwaan"
  - subject: "PS font names"
    date: 2005-09-14
    body: "It says:\n<<\n- QPSPrinter\n        Generate PS font names correctly.\n>>\n\nThey fixed the bug (I hope).\n\nIAC, don't try my patch for 3.3.3 on it.\n\nI haven't tested it yet, but I hope that it works."
    author: "James Richard Tyrer"
---
Trolltech <a href="http://www.trolltech.com/newsroom/announcements/00000219.html">has released Qt 3.3.5</a>, a new maintenance release to the Qt 3.3 series which incorporates <a href="http://www.trolltech.com/developer/changes/changes-3.3.5.html">over 200 bug fixes</a> as well as support for new platforms. In related news, a new website &quot;<a href="http://www.blogistan.co.uk/qt/">A Qt Blog</a>&quot; has been started whose <a href="http://www.blogistan.co.uk/qt/2005/09/introduction_to_this_blog.php">goal is to summarize happenings</a> in the Qt and KDE worlds.



<!--break-->
