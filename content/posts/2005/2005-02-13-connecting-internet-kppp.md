---
title: "Connecting to the Internet with KPPP"
date:    2005-02-13
authors:
  - "mgagn\u00e9"
slug:    connecting-internet-kppp
comments:
  - subject: "Flaws"
    date: 2005-02-13
    body: "KPPP is currently somewhat flawed in design-- it requires that it manages the connection itself (you can't use your system's init-scripts to connect/disconnect) and it add differentiation between protocols (do your grandparents care whether their internet is using PPP, Ethernet, or SLIP? I kinda doubt it)."
    author: "Luke-Jr"
  - subject: "Re: Flaws"
    date: 2005-02-16
    body: "If my grand parents use the phone line, they sure do :) ... everyone does (except if it's a leased line or <flawed reasoning>they are obscenly rich and for some reason they use ppp</flawed reasoning>)\n\nThe reason why kppp differentiates between protocols is kppp is not an universal network connection tool. It works only over dial-up. If you are on ethernet, you use your init-scripts to start-up the network connection and probably don't stop it until shutdown/reboot. If you use a modem, you connect to the Internet when there is a need for that, and disconnect as soon as that needs end. "
    author: "Adrian"
  - subject: "adsl"
    date: 2005-02-13
    body: "could be useful with adsl too"
    author: "max liccardo"
  - subject: "SuSE 9.1 problems"
    date: 2005-02-13
    body: "I've been using KPPP for years on SuSE 8.2, and I really like it. It does exactly what it sets out to do. However, on a recent install of SuSE 9.1 I noticed that a dial-up connection always hangs up the first time I try to connect after a reboot. All further attempts to connect work flawlessly. I'm not sure if that's a KPPP problem or something else (wvdial, some permissions, etc.), can anyone help?\n"
    author: "AC"
  - subject: "Re:Connecting to the Internet with kppp"
    date: 2005-02-16
    body: "I like kppp and it is my perfered application to connect with my dial-up modem.  However, the process described by Mr.Gagne to configure kppp leaves out a major step.  That is how to let a normal user (not root) to dial out.  The procedure requires additional configuration steps.  This is a neglected aspect of the kppp documentation at least up to kde 3.2."
    author: "Gus Degreef"
---
Most ISPs provide dial-up access through the Point-to-Point Protocol, or PPP. The KDE program that gets you connected to the Internet with a modem is called <a href="http://developer.kde.org/~kppp/">KPPP</a>. On a standard KDE setup, you'll find it under Kicker's big K by choosing the Internet menu, then clicking Internet Dialer. For those interested, <a href="http://www.marcelgagne.com/mtlbd_kppp.html">an overview of KPPP</a> including screenshots is available.

<!--break-->
