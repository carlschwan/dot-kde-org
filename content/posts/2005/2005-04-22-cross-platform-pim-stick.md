---
title: "Cross Platform PIM on a Stick"
date:    2005-04-22
authors:
  - "dmolkentin"
slug:    cross-platform-pim-stick
comments:
  - subject: "Impressive"
    date: 2005-04-21
    body: "Now I *have* to take a look at microKDE. I was hacking some wrapper classes to code PyQt/PyKDE apps (apps that use KDE only if available). But if this has enough coverage (and is sufficiently KDE-like), it is reduced to a wrapping job and coding to the MicroKDE API. "
    author: "Roberto Alsina"
  - subject: "Bloody amazing!"
    date: 2005-04-21
    body: "I've been waiting for this for so long.  Now those of us stuck in Windows can use non-lock-in, cross-platform calendar and addressbook programs.  \n\nBeing able to move away from Outlook (OL) is fantastic.\n\nCongratulations.\nZ"
    author: "Zsu"
  - subject: "Re: Bloody amazing!"
    date: 2005-04-21
    body: "What I always find fascinating as a native German speaker about\nAmerican English is the use of abbreviations. English is already\nmuch shorter than any other European language. Nevertheless\nthe abundance of seemingly unnecessary abbreviations is, well,\nbloody amazing ;-)\nSometimes I'm reading a TIME article - say about medicine or science -\nand there is a strange new technical abbreviation. No explanation\nwhatsoever is given. 4 pages later on the term is used written\nout (no abbreviation this time). Good training for your memory.\nIf find sth. like this quite often. I've decided that the average\nAmerican reader must probably be used to this.\nBut your example is much cooler. Just create an abbreviation \nout of the blue without ever using it again. Just great (JG)!\n"
    author: "Martin"
  - subject: "Re: Bloody amazing!"
    date: 2005-04-22
    body: "Maybe you can add it:\n\nhttp://www.abkuerzungen.de/pc/html/start.php?language="
    author: "Debian user"
  - subject: "http://korganizer.kde.org/"
    date: 2005-04-21
    body: "Seems to be rather outdated."
    author: "Anonymous"
  - subject: "Amazing!!"
    date: 2005-04-21
    body: "Thank you! Great stuff."
    author: "Michael Jahn"
  - subject: "4.0"
    date: 2005-04-22
    body: "Where's the feature plan for KDE 4 ?"
    author: "Hayer"
  - subject: "Re: 4.0"
    date: 2005-04-22
    body: "this one?\nhttp://developer.kde.org/development-versions/kde-4.0-features.html"
    author: "Mark Hannessen"
  - subject: "Re: 4.0"
    date: 2005-04-24
    body: "It's too far away to already tell them, there will be KDE 3.5 and maybe even a KDE 3.6 in parallel to its development."
    author: "Anonymous"
  - subject: "iSync plugin?"
    date: 2005-04-22
    body: "This would be really sweet for an iSync plugin for OS X..  If I can get a 3G PDAphone at some point that runs Linux and Qt, that would be tits..."
    author: "Anonymous"
  - subject: "multiplatform KDE-Lite ?"
    date: 2005-04-22
    body: "So, would it be feasible to extend micro-KDE to produce a set of qt wrappers \nto the kdelibs (even if limited) so that people could run a \"lite\" version of \nKDE ? Would this run in windows ?  I am not sure if there is a minimal gpl-qt\nthat runs in windows/mac\n\nThis would be a boost to qt development. Some people prefer gtk because they\ncan release their software in windows. I know, I fully understand TT's policy.\nBut  I still think the question is relevant ..."
    author: "MandrakeUser"
  - subject: "Am I the only one..."
    date: 2005-04-22
    body: "with these strange experiences? After several years close to the KDE project, I have noticed a pattern that repeats ever and ever again:\n\na) I have a problem that could be solved by a KDE/Qt application\nb) I think about it a lot\nc) I accept that I have no time to implement it\nd) I forget about it\n\nand then, usually 4 months later:\n\ne) someone publishes exactly that kind of application\n\nAnd of course, it is a lot better than the one I imagined!\n\nWell, to summarize: thanks a lot for this tool!\n"
    author: "Matthias H\u00f6lzer-Kl\u00fcpfel"
  - subject: "Great, but unfortunatly no support for synCE."
    date: 2005-04-22
    body: "I've been kind of scared when I bought my ipaq. And yes, the reboots came (every day at least once) and of cource, no syncing. I've seen a friend sync with evolution, but I've not seen anyone sync using kde. Ive looked at kitchensync, but the last version dates from november last year. I know, I should do it myself instead of moaning, but... \n\nAnyway, I check everytime i see an artical like this, but unfortunately....\nShould have bought that palm, and take the car with navigation. Now I cannot go to opie without losing tomtom.\n"
    author: "Daniel Lintjens"
  - subject: "Re: Great, but unfortunatly no support for synCE."
    date: 2005-04-24
    body: "I've installed Linux on my iPaq, and it is stable and nice... Opie is a great environment, and PyQt makes it really easy to create apps.\n\nBut there just aren't that many apps, and none are really mature like DateBK5 or even ShadowPlan on the Palm.  It's pretty, nifty, and I use my Palm instead.\n\nAh, well..."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Great, but unfortunatly no support for synCE."
    date: 2005-05-31
    body: "please send me an e-mail, i would like to know more...."
    author: "mirjam"
  - subject: "May be more than cool. But deserves a cooler name!"
    date: 2005-04-23
    body: "If that thingie really works (will try it next week, on Windows), that is a really cool gem.\n\nAnd as such, it would be very much deserving a really cool name too. \"KDE-PIM/PI\" just sucks. \"pimpi\" or \"Pimpy\" may be better, but not too much so.\n\nBut why don't we just (with the consent of the KDE-PIM/PI developers, of course), run a new contest with the help of kde-apps/kde-look webmasters? \"Find a cool name for a cool app!\"... \n\nTo make your braincells heat up, I'll leak some of my own submissions here: they would start with \"Power Runt\" and \"Mity Might\" and \"Mighty Mite\". "
    author: "Kurt Pfeifle"
  - subject: "Pimp my PDA ;-)"
    date: 2005-04-25
    body: "would be quite a cool, mindshare grabbing name ..."
    author: "egghat"
  - subject: "When Cross platform KOFFICE on a stick"
    date: 2005-04-27
    body: "A lightweight crossplatform Koffice, with oasis files, could be even more of a success."
    author: "Geert"
---
Available for memory sticks on Windows or Linux, the new release <a href="http://www.pi-sync.net/">KDE-PIM/Platform independent</a> lets you carry around your favourite KDE applications and your personal data in the palm of your hand. This device independent software can import your data directly from Outlook and sync it with KDE-PIM running on other computers.

Based on the great work of the <a href="http://pim.kde.org">KDE-PIM</a> developers, KDE-PIM/Pi is available for Windows, Linux and the Zaurus PDA and includes platform independent versions of <a href="http://pim.kde.org/components/kaddressbook.php">KAddressbook</a> and <a href="http://korganizer.kde.org/">KOrganizer</a> (<a href="http://www.pi-sync.net/html/ko_pi_screenshots.html">Screenshots</a>).







<!--break-->
<p><a href="http://www.pi-sync.net/html/ko_pi.html">KOrganizer/Pi</a> (KO/Pi) is based on the embedded version 1.08 beta of KOrganizer from Cornelius Schumacher. Lutz Rogowski has added many features to suite the needs of other platforms and mobile devices. <a href="http://www.pi-sync.net/html/ka_pi.html">KAddressbook/Pi</a> (KA/Pi) is a port of KAddressbook to the Zaurus and has been started by Ulf Schenk. KDE-PIM/Pi replaces the KDE libraries with a Qt-only wrapper library called "micro KDE". A precompiled "all-in-one" memory stick edition for Windows and Linux and other packages can be downloaded from the <a href="http://sourceforge.net/project/showfiles.php?group_id=104103&package_id=112604">SourceForge project page</a>. More info is available at the <a href="http://www.pi-sync.net/html/documentation.html">project homepage</a>.
</p>






