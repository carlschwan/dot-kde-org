---
title: "Interview with KDE-PIM Hacker Daniel Molkentin"
date:    2005-05-28
authors:
  - "aleeuwen"
slug:    interview-kde-pim-hacker-daniel-molkentin
---
Daniel Molkentin, from the beautiful city of K&ouml;nigswinter in Germany, is one of the maintainers of Kontact and has also been involved in many other parts of KDE. He is one of the developers visiting the NL-PIM meeting this weekend. This meeting, sponsored by <a href="http://www.nlnet.nl/">NLnet</a>, <a href="http://www.trolltech.com/">Trolltech</a>, <a href="http://www.nluug.nl/">NLUUG</a> and <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/sponsors_en.php">many more companies</a>, will allow Daniel and the other KDE-PIM developers to get together to further improve the KDE-PIM applications and framework. In this interview we talk about Daniel's involvement in Kontact development, the NL-PIM meeting and future plans for KDE-PIM.












<!--break-->
<div style="float:right; width: 250px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/danimo.jpg" width="250" height="333" alt="danimo" /><br />Daniel Molkentin is the danimo</div>
<p><b>How did you get into KDE development, and what is your role in the development of Kontact?</b></p>

<p>I got involved with KDE development by submitting some patches to the JavaScript-related parts of KHTML back in the Autumn of 2000. I had met the KDE crowd at LinuxTag earlier that year. It was really amazing. Later I got involved with KControl and became its maintainer. I was also involved in the Aegypten II and Kolab II projects.</p>

<p>I am one of Kontact's maintainers, along with Don Sanders and Cornelius Schumacher. I mostly take care of the Kontact framework itself, the visible parts if you will. Other than that, I am the author or several fixes, features and hacks throughout KDE-PIM.</p>

<p><b>So one could say you work on the integration of Kontact's different parts?</b></p>

<p>No, everyone is working hard to ensure optimal interoperability and cooperation among the KDE-PIM applications. The parts I work on usually melts in with the applications, so you can often hardly tell them apart. A good description would probably be "everything that is Kontact but that you can't see when starting the individual applications".

<p><b>What are the kind of problems or difficulties you encountered when developing the framework?</b></p>

<p>Although the framework rarely poses problems, there were some hard nuts to solve, and there probably still are. I took over from the original maintainer, Matthias Hölzer-Klüpfel, who modeled it after what later became KDevelop 3, completely plugin and KPart based. Cornelius imported it into CVS and soon joined me on working out stuff that was needed. The design of the user interface was not always as simple as it may seem.</p>

<p>For instance, we had several approaches on the sidebar. One was very similar to the sidebars of Microsoft Outlook 2003 and Evolution. It had buttons for every entry that is now an icon and a big area that included the folder tree or some options for filtering the contents of the current view (<a href="http://developer.kde.org/~danimo/screenies/kontact_adresses.png">screenshot</a>). It wasn't a very satisfying solution, for several reasons: it required a dirty hack, and many users didn't really like it. That doesn't mean we won't ever have it again, but I think we need a different approach right now.</p>

<p>We also had to find a way to substitute drag and drop among the different embedded applications, or the joint configuration dialog. Another problem was the lack of a mail application that could be used in Kontact. Don Sanders later came to help us here: he turned KMail into a KPart that could easily be plugged into Kontact. So it's everything, from usability problems to technical problems. It's really challenging.</p>

<p><b>What are you working on right now?</b></p>

<p>Currently I am working with several other people on the Qt4 port of kdelibs. We have been very successful so far, and we hope to be able to start a port of KDE-PIM at aKademy 2005. The motivation is that with Qt4, we have a GPL'ed toolkit for both Windows and Mac OS X as alternative platforms. Also, the sooner things are ported over, the more time we can spend on improving the design and user experience of KDE-PIM 4 - but at the same time, we want to provide some features in a KDE 3.5 already.</p>

<p><b>Do you think that Qt4 will also make some usability issues easier to fix (or, put differently, do you think it's possible to create a better user experience with Qt4 than with Qt3)?</b></p>

<p>Yes. Not just because Qt4 will bring significant improvements with regard to accessibility and gives the developer a whole new amazing set of tools, but also because with KDE4 we can restructure some of our own libs that we always cared to keep from changes. Since Qt4 breaks binary and even source compatibility, we should use the opportunity for some cleanups that allow for a better design. The result of all of this is of course to improve the user experience, because coding on libraries is, of course, never the end in itself.</p>

<p>As for concrete changes in the user interface, I couldn't name anything concrete, but the configuration needs a lot of love as do other parts that could be a lot simpler.</p>

<p><b>What do you expect from the NL-PIM meeting?</b></p>

<p>Obviously there will be improvements in the usability department. I'd also like to discuss how we want to continue with KDE PIM development after KDE 3.5 and what should still be done until then, since KDE 3 will be en-vogue for at least a year or two, even though we already see early stages of KDE4 development.</p>

<p>During the meeting, I will try to get into my list on bugs.kde.org and fix issues, implement wishes and of course fix usability issues for KDE 3.5. That said, I am also happy to have Jan and Ellen from OpenUsabiliy around, they proved to be really nice and helpful people in the past. It's really refreshing to work with them.</p>

<p><b>What can developers learn from them?</b></p>

<p>Developers tend to write software in the way they think is best and start to defend their solution as if it's the holy grail. Immunised by self-proclaimed usability experts they tend to ignore other people's ideas and suggestions. For those developers, witnessing a real usability test and seeing the actual problems users have with the software is truly eye-opening. Knowing that suggestions are based on experience and actual tests also helps a lot. The <a href="http://www.openusability.org/">OpenUsability</a> folks managed to convince developers by practical examples rather than starting long threads on a mailing list. It's a two-sided learning process for developers and usability experts to work together and it's not always easy, but in the end, it's really worth it.</p>

<p><b>Do you have any ideas about KDE-PIM's future? What direction would you like to see it go?</b></p>

<p>I would like it to become successful on other platforms too. Windows and Mac OS X both lack a good free groupware client. I also think Kontact should be easily extensible to support the workflow of a specific user or company. No need to mention that improved usability is extremely important here.</p>

<p><b>Anything else you'd like to tell the readers?</b></p>

<p>Nothing else to say but to point out how awesome the whole KDE-PIM community is. it's a joy to work with the other developers and contributors. Make sure you become part of the gang :=)</p>

<div style="width: 500px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/kdepim.jpg" width="500" height="234" alt="KDE PIM Hackers" /><br />Some of the KDE PIM hackers at the meeting today</div>









