---
title: "Quickies: Qt4 Tutorials, KDE 3.5 Alpha Review, Kontact Introduction"
date:    2005-09-14
authors:
  - "jriddell"
slug:    quickies-qt4-tutorials-kde-35-alpha-review-kontact-introduction
comments:
  - subject: "[OT] cp: cannot stat libqt-mt.la "
    date: 2005-09-14
    body: "excuse my offtopic post, but I really donno what's wrong with qt-3.3.5, make compiles fine but the 'make install' give error 'libqt-mt.la' is not found.\n\nSince last 3 days I have been trying to build kdebase 3.5svn, but it complains of libqt-mt.la is missing. please help. Google and trolltech (qt-interest) has no info regarding this. Have you got qt3.3.5 successfully compiled?\n\nMessage: (make install as root)\n--------------\ncp -f \"~/kde_3.5_build/qt/lib/libqt-mt.la\" \"/usr/lib/qt/lib/libqt-mt.la\"\ncp: cannot stat `~/kde_3.5_build/qt/lib/libqt-mt.la': No such file or directory\nmake[2]: [install_target] Error 1 (ignored)\n--------------\n\nconfigured with:\n----------------\n./configure -system-zlib -qt-gif -system-libjpeg -system-libpng -plugin-imgfmt-mng -thread -no-exceptions -release -dlopen-opengl -plugin-sql-mysql -plugin-sql-sqlite -I/usr/include/mysql --prefix=/usr/lib/qt -shared \n----------------\n\nthanks."
    author: "fast_rizwaan"
  - subject: "found the solution to libqt-mt.la "
    date: 2005-09-14
    body: "I was compiling QT 3.3.5 in a different folder (not in the source folder) that's why everything got compiled fine but libqt-mt.la was not created in the *build* folder.\n\nJust now I compiled (make) in qt 3.3.5's SOURCE folder i.e., ~/qt-x11-free-3.3.5/ and viola libqt-mt.la is created.\n\n"
    author: "fast_rizwaan"
  - subject: "Media Manager and Yellow tooltip :("
    date: 2005-09-14
    body: "nice review liquidat, and you are right that *yellow tooltip* are not good looking. Really the new tooltips are nice.\n\nhey aaron, it's time to see the system tray... lol.\n\n\nAlso the Media Manager is very good/useful for 2 reasons (at least):\n\n1. For newbies\n2. When Urgent (no time to run \"mount -t vfat /dev/sda1 /mnt/usbdrive\", whereas the dialog provides fast access.\n\nShouldn't we have somehting like \"shortcut\" say \"CTRL+ALT+M\" to RE-Detect the devices, so that advanced users don't have to see the annoying dialog when a new device is inserted (like cdrom, usb disk, camera etc.)\n\nwhat we really hate is full-automation by the OS/Desktop, So, if we have 2 modes for media detect manager:\n\n1. Newbie mode: prompt dialog on device insertion\n2. Expert mode: do not prompt dialog but a \"passive tooltip\" (or configurably no tooltip if that is also annoying) on device insertion\n\nbut pressing Ctrl+Alt+M will get the popup. in this way both can be happy :), just my 2 paise!\n\nthanks :)\n"
    author: "fast_rizwaan"
  - subject: "Re: Media Manager and Yellow tooltip :("
    date: 2005-09-14
    body: "I remember Aaron blogging/commenting/emailing somewhere or other that the system tray was impossible to update to the new tooltip style without a complete rewrite.\n\nPity :(\n\nL."
    author: "ltmon"
  - subject: "Re: Media Manager and Yellow tooltip :("
    date: 2005-09-14
    body: "I posted a bug report and had a short talk with Aaron about it:\n\nhttp://bugs.kde.org/show_bug.cgi?id=111854\n\nSo we must wait until KDE 4 :-/"
    author: "liquidat"
  - subject: "Re: Media Manager and Yellow tooltip :("
    date: 2005-09-14
    body: "One thing I want to add: when you like to have a look at new screenshots and mockups, than http://liquidat.blogspot.com/2005/08/kde-40-bunt-und-bewegt-und-zum.html could be interesting for you:\nIt is a short overview of the KDE 4 mockups in kde-artists.org. The review is in german, but that is not so important because there are just mockups and you can't really comment on them in a blog.\nThere are some pictures, some animated gifs, two flash movies and at the bottom a link to a interactive flash movie ;-)\n\nBut, as I said, these are all just mockups, and nothing of it is official!"
    author: "liquidat"
  - subject: "Re: Media Manager and Yellow tooltip :("
    date: 2005-09-14
    body: "I really like the taskbar in this one http://img94.imageshack.us/img94/5760/mockuptranstext5ec.jpg\n\nit is different from what we're accustomed to (win/mac) yet very familiar to work with.\nwith a heavily improved top menubar it could be a killer combo."
    author: "Anonymous"
  - subject: "Re: Media Manager and Yellow tooltip :("
    date: 2005-09-14
    body: "I like it too! its a new fresh concept that i would see in the next KDE :)"
    author: "biquillo"
  - subject: "Re: Media Manager and Yellow tooltip :("
    date: 2005-09-25
    body: "A little bit late, but:\nIf you really like these \"mockup reviews\" you should have a look at the new one:\nhttp://liquidat.blogspot.com/2005/09/kde-40-mockup-review-part-ii.html\n\nRegards,\n\nRoland"
    author: "liquidat"
  - subject: "Krusaders"
    date: 2005-09-14
    body: "Does Krusaders depend on KDELibs? If so, what's the deal? Isn't Konqueror enough? \nI understand if it's Qt only, but my guess it's not. "
    author: "user"
  - subject: "Re: Krusaders"
    date: 2005-09-14
    body: "Try it and see.  It's Norton Commander/Midnight Commander/DirWork for KDE -- a different model from a desktop/folder oriented file manager, and often much more useful."
    author: "Lee"
  - subject: "Re: Krusaders"
    date: 2005-09-14
    body: "No Konqi isn't enough, krusader is just like windows explorer vs total commander on windows. Both have there uses. I can use Krusader almost without mouse. And because of that a lot faster than konqi..."
    author: "Boemer"
  - subject: "Re: Krusaders"
    date: 2005-09-14
    body: "The design is compleatly different from konqueror: While konqi provides equal panels (which can be shown one aside the other but apart from browsing they're independent) Krusader always has one source- and one target panel. Every operation (well, nearly) works from the source to the target.\nIn addition Krusader provides many advanced features like directory synchronizing, a Total-Commander compatible file-splitter, very flexible useractions (http://krusader.org/handbook/useractions.html), etc..."
    author: "jbaehr"
  - subject: "Re: Krusaders"
    date: 2005-09-14
    body: "Different people like different things. Konqueror is a great filemanager, but some people might like something different. And if someone decides to write another filemanager for KDE, who are we to say \"you can't do that! use Konqueror instead!\"?\n\nSame logic: some people like KDE, while others like GNOME. Both do the same thing,  but in a different way. And different people like different things."
    author: "Janne"
  - subject: "Re: Krusaders"
    date: 2005-09-14
    body: "I agree, if you like Orthodox File Managers (twin-panel filemanagers) like Midnight Commander or Total Commander, etc. it can't hurt to give Krusader a try, you will probably like it.\nAnd other users prefer to use one panel filemanagers, the choice is up to you.\n\nKrusader is deeply integrated with KDE. This might be the only real drawback for users that use GNOME, XFce, or other desktop environments, but in the apt-get world even that is not a problem, since all dependencies get resolved instantly. \n"
    author: "Frank Schoolmeesters"
  - subject: "KDE Runtime Observer"
    date: 2005-09-14
    body: "For the Google Summer of Code I did a program called KRO, that works as Prashanth Udupa's Signal Spyer Class. It also includes a containment tree (A QCombo contains a QListBox and a QLabel, a KonqMainWindow about 25 diferent childs...), a signal observer and some search functionalities. All this inside a GUI in which you can slide over time and watch past events and how they flowed.\n\nJust now it works with Qt3, but I plan to support Qt4 ASAP.\n\nThe code is at http://websvn.kde.org/trunk/playground/utils/kro/ and tarballs, debs and more info at http://www.monasteriomono.org/programs/kro/\n"
    author: "David Moreno"
  - subject: "Re: KDE Runtime Observer"
    date: 2005-09-14
    body: "sounds interesting, could be usefull for debugging i guess..."
    author: "aaaaaa"
  - subject: "Kaffeine backends"
    date: 2005-09-14
    body: "Hi all\n\nI've been browsing  kaffeine's webpage a bit, but couldn't find the answer. Does it provide a hook to use mplayer (besides xine and now gstreamer) as a backend ? Also: has it become the defacto media player ? \n\nHere is my issue, I am using kmplayer in Mandriva, but I have to download it from the contributed packages, which is annoying. Especially when there is a new KDE release and I can't upgrade with Mandriva's packages because of kmplayer. I'd love to use the default media player and avoid these conflicts ...\n\nCheers!\n"
    author: "MandrakeUser"
  - subject: "Re: Kaffeine backends"
    date: 2005-09-14
    body: "no, it doesn't support mplayer as backend. but you can have it working using kmplayer in kaffeine."
    author: "fast_rizwaan"
  - subject: "Re: Kaffeine backends"
    date: 2005-09-14
    body: "How does that work ? THere is a config option to embed kmplayer in Kaffeine ? Is it using Kparts ? Interesting\n\nthanks!"
    author: "MandrakeUser"
  - subject: "Re: Kaffeine backends"
    date: 2005-09-14
    body: "i copy that,\n\na more/less default media player in KDE would be nice. maybe some of the current mediaplayer people can join force for kde4?\n\nthere is another question, although i think it is a bit offtopic here:\nmedia player then to be sightly unstable (konqueror with a flash animation and a video stream is in my suse suppelemtary kde3.4 always a good crash), and i think that is due to the back-ends an not the kde shell over it. isnt it possible to couple the front-end and the back-end more loosly so the back-end can safely crash, what the front-end notices, what the frontend reports to the user, from which the front-end recovers (be restarting the back-end and skipping to the right moment)...\n\ncan someone maybe answer these:\n- is such 'loose coupling' possible?    , or\n- am i talking total nonsense here?\n\ni already posted a similar question in this thread about RuDI (some framework to do loose coupling, but appearently not for GUI embedded plugins):\nhttp://www.kdedevelopers.org/node/1398\n\nthanks for reading,\ncies breijs."
    author: "cies breijs"
  - subject: "Re: Kaffeine backends"
    date: 2005-09-15
    body: "Not really nonsense, only that there is such a solution for quite some time and is called KMPlayer. I guess that SuSE simply recommends firefox as browser, like the other major distos do, otherwise I can't explain why they ship a video plugin that is known to crash konqueror. Also KMPlayer can easily configured to use Xine as default instead of MPlayer."
    author: "koos"
  - subject: "Re: Kaffeine backends"
    date: 2005-09-15
    body: "Sorry, one addition though. I'm talking about the plugin (the kpart) not the video player as stand-alone application, as it's perfectly possible to have KMPlayer for embedding only and kaffeine for 'normal' video."
    author: "koos"
  - subject: "What you see is what you get printing - Maybe not?"
    date: 2005-09-16
    body: "I do not know whether wysiwyg printing is important to anybody, but kde and qt certainly have problems with it. After being in existence for about nine years it seems a bit too bad that this great application suite does not have great printing. Moreover, it seems only fair for you to expect good wysiwyg printing after being around for so long. I tracked down the problem to two bugs in bugzilla. If the printing quality matters to the community, I suggest you comment and vote on these bugs because no progress has been made on them in a long time.\n\nhttp://bugs.kde.org/show_bug.cgi?id=59367\nhttps://bugs.kde.org/show_bug.cgi?id=85259"
    author: "Anonymous Joe"
---
<a href="http://qt4.digitalfanatics.org/">The Qt 4 Resource Centre</a> has tutorials for <a href="http://qt4.digitalfanatics.org/articles/zoomer.html">A Zoomable Picture Viewer</a> and <a href="http://qt4.digitalfanatics.org/articles/signalspy.html">Spying on Signals</a>. *** This <a href="http://liquidat.blogspot.com/2005/08/kde-35-alpha-1.html">KDE 3.5 Alpha review</a> shows us some new features coming soon. *** Alternative KDE file manager <a href="http://www.krusader.org/">Krusader</a> found themselves new web hosting. *** Linux.com <a href="http://www.linux.com/article.pl?sid=05/08/25/1447236">introduces us to Kontact</a>. *** Real-time 3D strategy game <a href="http://boson.eu.org/">Boson</a> made a new release with <a href="http://boson.eu.org/screenshots.php">extra smooth graphics</a> and multiplayer support. *** Video player <a href="http://kaffeine.sourceforge.net/">Kaffeine</a> added a GStreamer part in their recent 0.7 release. *** Finally congratulations to <a href="http://www.kdedevelopers.org/node/1402">Stephan Binner</a> who had to leave aKademy early to start a job packaging KDE for SuSE.



<!--break-->
