---
title: "This Month in SVN for October 2005: KOffice Development"
date:    2005-10-29
authors:
  - "jriddell"
slug:    month-svn-october-2005-koffice-development
comments:
  - subject: "This is fantastic!"
    date: 2005-10-29
    body: "I really like the progress KOffice has made. Krita is going to be my favorite :) \nI don't think I'll need gimp anytime soon now..."
    author: "David"
  - subject: "Thanks!"
    date: 2005-10-29
    body: "Big thanks to all the koffice developers for their work! Happy coding!!"
    author: "Dario Massarin"
  - subject: "A little note in howto improve Krita"
    date: 2005-10-29
    body: "Krita's progress is incredible amazing. I recall having used it with the KDE 3.0 times and I was nowhere good enough for anything. But nowadays it looks like a fully replaced application to me. I bet that together with the release of KDE 4, Krita will become as powerful as Photoshop and quite a look a like too. Easier for people getting their work going.\n\nThough I wish that with KDE 4 there would be some sort of KDE core library for doing graphic stuff with functions that can be shared by other applications as well. Like image viewer, like basic paint functions in spreadsheets or docwriters and for Krita itself. I say this because I really like to not depend on ImageMagick."
    author: "Me"
  - subject: "Re: A little note in howto improve Krita"
    date: 2005-10-29
    body: "> Krita will become as powerful as Photoshop and quite a look a like too.\n\nAccording to the developers they aim for a Painter-alike application, not Photoshop, which is lightyears ahead to Gimp. And Krita doesn't compare to Gimp feature-wise yet. Also it's even slower than Gimp, which is a lot slower than Photoshop. Regarding usability Krita is better than it's gnomish cousin, though.\n\nI'd say nice ideas, on track, a lot to do, but definitely not an alternative for proffesional software yet."
    author: "Carlo"
  - subject: "Re: A little note in howto improve Krita"
    date: 2005-10-29
    body: "Yes, that's probably true, but I think it won't take too long before it's a professional feature wise-wise alternative."
    author: "David"
  - subject: "Photoshop"
    date: 2005-10-30
    body: "I was not aware that photoshop was ahead of the GIMP. Could you please briefly explain what we are missing in the GIMP?\n\nI'm just being ignorant here -- the only non-free software I have used seriously in years is Matlab -- and I think I was not alone in believing that Photoshop ~ the GIMP.\n\nThanks!"
    author: "Martin"
  - subject: "Re: Photoshop"
    date: 2005-10-30
    body: "Photoshop is lightyears ahead of the gimp, particularly for the printing industry. For a start, the gimp lacks CMYK. I'm given to understand they now have a plugin but it's hackish and not very good."
    author: "canllaith"
  - subject: "Re: Photoshop"
    date: 2005-10-30
    body: "Fortunately, we have CMYK -- in fact, Krita has CMYK, Grayscale and RGB in 8 and 16 bit per channel, and we have RGB in 16 and 32 bit float. The fact that Krita can separate a 16 bit RGB image into 16 bit grayscale images for C, M, Y and K channels alone is a very professional feature.\n\nAnd I see that Adrian Page just has committed optional OpenGL support for Krita, which should speed things up for some people :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: Photoshop"
    date: 2005-11-03
    body: ">Adrian Page just has committed optional OpenGL support for Krita\n\n*NOW* we are talking :-)"
    author: "Macavity"
  - subject: "Re: Photoshop"
    date: 2005-11-02
    body: "agreed.  trying to *work* in cmyk space is atrocious in GIMP, as is working in LAB.  People who say \"oh but it has CMYK\" are deluding themselves.  Unfortuantely, if you want to blame anyone for GIMP not having good alternate colorspace support, you need to blame Adobe, not the GIMP crew.  It's Adobe's anti-competitive patents that prevent GIMP from offering CMYK support.\n\nHaving said all that, I love the GIMP, and it's rare that I absolutely have to work in CMYK and/or LAB colorspace on my Linux box at home.  At work, I have Photoshop."
    author: "regeya"
  - subject: "Re: Photoshop"
    date: 2005-10-30
    body: "> Could you please briefly explain what we are missing in the GIMP?\n\nSure: The selection tools are very basic, Gimp sucks with files >= a few dozen MB since the filters and especially Python and Script-Fu are damn slow - even weird results and crashes I could notice. Overall Photoshop is _a_lot_ faster even in a VMware session, I guess Adobe has just far more experience with in memory image data handling and algorithms. The user interface is still a bad joke - even though it has been improved with 2.2.x. E.g. there are still needlessly a thousand dialogs popping up (this hampers your workflow and isn't the right place for additional information) for text handling, sheering,... I noticed that people have huge problems getting gimp-print working and it didn't worked for me either.\n\nSo if you're making only a bit web graphics and your time isn't worth the buck, Gimp is a really nice application, but beyond that: NO!"
    author: "Carlo"
  - subject: "Re: Photoshop"
    date: 2005-10-31
    body: "Yeah, because most people who install photoshop are professional photographers and  absolutely all of them pay $700 for it.\n\nGive me a break. Gimp is more than good enough for 95% of us and your bullshit insults the Gimp developers and our intelligence."
    author: "Miguel"
  - subject: "Re: Photoshop"
    date: 2005-10-31
    body: "GIMP is more than good enough for the average non-artist who needs to do something, and good enough for most hobbyist artists who want to do something.  Professionsal artists need more power, and should pay the $700.\n\nThe GIMP is great, but building expectations beyond what it is does nothing helpfor for free software or the GIMP.   \n\nWe are much better off in the long run selling GIMP/KRITA as a intersting tool that is worth installing because it is free, and might be good enough.   Undersell it, and let the application sell itself to those who won't hit the limits."
    author: "bluGill"
  - subject: "Re: Photoshop"
    date: 2005-10-31
    body: "Such rants do not change the fact that the GIMP lacks critical features required for the printing industry and many other facets of professional work. It's a lovely program and I use it at home for digital photos and a little web design. Even though I prefer to use KDE/Linux for all tasks, I must face facts though that if a program has not implemented features one's entire business depends apon then it's simply not suited for that purpose. \n\nThis is not an insult to the GIMP - it's just not designed to be that professional photoshop-killer application. Neither is using KOffice to generate PDF's a replacement for professional desktop publishing tools. Is this an insult to the intelligence of KOffice developers? I have a high personal regard and respect for them. I'm sure they'd agree with me that there are professional products that for very certain and specific use cases are actually more suited to the task at hand than KOffice is."
    author: "canllaith"
  - subject: "Re: Photoshop"
    date: 2005-11-07
    body: "personally for me the principle issue is\nthe incredible lack of attention to detail.\nif its going to be useless, can't it at\nleast be pretty about it? ;)"
    author: "lypie"
  - subject: "Re: Photoshop"
    date: 2006-10-19
    body: "I do not think that there is a lack of intelligence on developing such real great tools like the gimp or krita, but it must be allowed to talk about other products, even if they are closed source and even if they are better!\n\nCheers Wolfgang"
    author: "Wolfgang"
  - subject: "Ongoing font-kerning problems"
    date: 2005-10-29
    body: "I hope that somebody can finally solve the ongoing font kerning problems in KOffice. The character spacing in printing previews and real prints is so broken that it looks plain ugly. I have seen this problem on many up-to-date Linux configurations, while others do not experience the problem.\n\nAllthough I like KOffice a lot, it is not usable for me."
    author: "Meneer Dik"
  - subject: "Re: Ongoing font-kerning problems"
    date: 2005-10-29
    body: "I hate to be redundant but ... \n\nIIUC, this problem is caused by Qt, therefore there are two possible fixes:\n\nQt-4 might be able to fix this but I no longer see anything about WYSIWYG on the trolltec web page.\n\nA new painter and PostScript driver (perhaps borrowed from Scribus or OO).\n\nSpecifically, the Qt-3 painter uses fonts hinted to the screen resolution and then tries to print with that spacing (which it doesn't appear to get right either).  To be clear, Qt-3 will not do WYSIWYG -- it can not display using either the UN-hinted font metrics or the font metrics hinted at the _printer_ resolution.  Both of these (in addition to the screen resolution) are needed for a properly functioning office suite.  In addition, to use the printer resolution, KPrint is going to need support for PPD files (it needs it anyhow)."
    author: "James Richard Tyrer"
  - subject: "Re: Ongoing font-kerning problems"
    date: 2005-10-31
    body: "Then why is Scribus not having any font problems? It uses QT3 as well."
    author: "Meneer Dik"
  - subject: "Re: Ongoing font-kerning problems"
    date: 2005-10-31
    body: "Because Scribus does NOT use the Qt PostScript driver.\n\nThe Scribus authors recognized the problem and wrote their own PS driver:\n\nscribus-<version>/scribus/pslib.cpp"
    author: "James Richard Tyrer"
  - subject: "Re: Ongoing font-kerning problems"
    date: 2005-10-30
    body: "I don't know what use is there of an office document if it \ncannot be printed properly.  None of the Koffice components print\ndocuments any good.  The pdfs created from koffice components \nare ugly when printed.  I think the developers should get the \nbasics such as printing  right before proceeding any further"
    author: "Ask"
  - subject: "Re: Ongoing font-kerning problems"
    date: 2005-10-31
    body: "I guess it actually depends on the kind of fonts you are using. With msttcorefonts package I was getting problems similar to what you guys describe. But I was able to get quite decent printing from KWord using Nimbus fonts, as well as other non-MS and non-TTF fonts. It took me a few hours (not to say days) to finally figure out a way to unscrew the debian defoma pile of doggie doo-doo, but in the end it is working pretty well, on screen, on print preview, and on paper."
    author: "kiriuja"
  - subject: "Re: Ongoing font-kerning problems"
    date: 2005-10-31
    body: "Again, this is a Qt issue.  When TrueType fonts are embedded in a PS file (to make a PDF, first a PS file is created and then converted to a PDF by the GhostScript PDFWriter) they are converted to Type42 fonts.  In OO, the Sun converter is used to convert the TrueType font to a Type42 outline font, but Qt converts them to a type42 bit mapped font.\n\nThe solution to this problem is to configure Qt (run \"qtconfig\" and in the \"Printer\" tab, uncheck \"Enable Font embedding\") to NOT embed  the fonts.  Then GhostScript will do the embedding and it also embeds as Type42 outline fonts.  Note that for this to work that GhostScript must find the font files -- your system must be correctly setup.\n\nSo, there are two places where the code to fix this could be borrowed, OO and GhostScript.\n"
    author: "James Richard Tyrer"
  - subject: "Full Compatibility among Office Suites"
    date: 2005-10-29
    body: "I'm a big enthusiast of the whole OpenDocument stuff, but sometime ago I've read two articles on Newsforge that made me wonder that we are far from having FULL compatibility among Office suites. The articles are:\n\nhttp://software.newsforge.com/software/05/09/09/1640253.shtml?tid=93\n\nand \n\nhttp://software.newsforge.com/article.pl?sid=05/09/09/192250\n\nI don't know if there's a solution being developed yet, does anybody know if there are any plans to solve these problems inside OASIS? Maybe standardizing on OpenFormula ( http://sourceforge.net/projects/openformula ) for formulas and Python for macros (do not even know if it possible) could help with the full compatibility among Office suites without the need to reinvent the wheel..."
    author: "V.T.N."
  - subject: "Re: Full Compatibility among Office Suites"
    date: 2005-10-29
    body: "The macro language is a serious problem.\n\nHowever, with the formula issue, why don't we just adopt whatever is used in Gnumeric, it is open source and a mature standard."
    author: "James Richard Tyrer"
  - subject: "Re: Full Compatibility among Office Suites"
    date: 2005-10-29
    body: "I seems the formula problem is already being tackled: \n\nhttp://lists.kde.org/?l=koffice-devel&m=112945605421834&w=2\n\n"
    author: "cm"
  - subject: "Re: Full Compatibility among Office Suites"
    date: 2005-10-30
    body: "It looks like OpenFormula is an external effort, but it would be better if it where handled inside OASIS, which is the official organization behind OpenDocument."
    author: "V.T.N."
  - subject: "Re: Full Compatibility among Office Suites"
    date: 2005-10-30
    body: "I sure hope that the results of this effort will enter the OASIS OpenDocument spec.  I don't know any more than what is said in the mailing list thread and on the sourceforge page, though. "
    author: "cm"
  - subject: "Re: Full Compatibility among Office Suites"
    date: 2005-10-30
    body: "I have to agree, Python would be an ideal language for Spreadsheet and Office macros."
    author: "Vlad C."
  - subject: "Re: Full Compatibility among Office Suites"
    date: 2005-10-30
    body: "Yes Yes Yes Yes Yes Yes Yes Yes Yes Yes Yes!\n\nPython is (almost) the perfect programming language, and absolutely the first\nchoice among interpreteded languages.\n"
    author: "Yves"
  - subject: "lol"
    date: 2005-10-29
    body: "\"Krita is one step closer to world domination with support for NetBSD and many crash fixes.\"\n\n*lol*"
    author: "fennes"
  - subject: "What happened to the Commit Digest?"
    date: 2005-10-30
    body: "Derek Kite: please come back!"
    author: "ac"
  - subject: "Re: What happened to the Commit Digest?"
    date: 2005-11-07
    body: "i prefer this its got pretty pictures!"
    author: "lypie"
  - subject: "Great new addition to Krita"
    date: 2005-10-30
    body: "Just a day after this article was published, Adrian Page included a new feature in Krita: OpenGL-support.\n\nFrom the kde-commits mailing list:\n\n===\nSVN commit 475547 by page:\n\nAdd support for displaying images using OpenGL, taking advantage of hardware acceleration if available. This also opens up the world of shaders, for GPU accelerated rendering algorithms and general purpose computation, the first of which should be real-time adjustment of exposure for high dynamic range images.\n\nOpenGL can be enabled/disabled in the settings dialog and it defaults to off for now.\n===\n\nGreat news. Thanks for that!"
    author: "Bram Schoenmakers"
---
<a href="http://www.canllaith.org/svn-features/svn-koffice.html">This Month in SVN for October</a> looks at <a href="http://www.koffice.org">KOffice</a> development. "<em>While much of the rest of KDE is in feature freeze preparing for the imminent release of KDE 3.5, KOffice developers are starting to work hard for their 1.5 release, scheduled for between KDE 3.5 and KDE 4. This release will be able to be used with KDE 3x and Qt 3x, and will have a great deal of improvements over the current stable version.</em>"  Topics covered include accessibility improvements, <a href="http://www.koffice.org/krita">Krita</a> one step closer to world domination and how you can help out.


<!--break-->
