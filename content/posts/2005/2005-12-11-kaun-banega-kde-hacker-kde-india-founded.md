---
title: "Kaun Banega KDE Hacker: KDE India Founded"
date:    2005-12-11
authors:
  - "dmolkentin"
slug:    kaun-banega-kde-hacker-kde-india-founded
comments:
  - subject: "Congrats!"
    date: 2005-12-10
    body: "This is good news for the entirety of KDE users and developers. I hope that the KDE India people will have some fun and I wish them the best luck."
    author: "EP"
  - subject: "Congrats"
    date: 2005-12-10
    body: "Excellent! This is the kind of reorganizing we need."
    author: "Inorog"
  - subject: "Great"
    date: 2005-12-11
    body: "Always nice to see KDE represented with new regional groups.\n\nJust in case it's not obvious from the article, Sirtaj is a long time KDE developer and press contact so the new group has plenty of experience to get it going (see his bio on http://people.kde.org/taj.html)"
    author: "Chris Howells"
  - subject: "Congratulations!"
    date: 2005-12-11
    body: "Congratulations and best wishes for KDE - India."
    author: "Sagara Wijetunga"
  - subject: "Congrats, KDE India!"
    date: 2005-12-11
    body: "I can't wait until the day when I launch the first \"Made in India\" KDE app on my desktop.\n"
    author: "Atul Chitnis"
  - subject: "Wonderful!"
    date: 2005-12-11
    body: "This initiative will be good for the country and good for the world."
    author: "marlow4"
  - subject: "What does \"Kaun Banega\" mean?"
    date: 2005-12-11
    body: "I am sure the title makes sense to everyone who speaks Hindi. But for those of us who don't: can someone explain what that phrase means, please?"
    author: "Kurt Pfeifle"
  - subject: "Re: What does \"Kaun Banega\" mean?"
    date: 2005-12-12
    body: "It's the indianized name of Till Adam."
    author: "Jeve Stobs"
  - subject: "Re: What does \"Kaun Banega\" mean?"
    date: 2005-12-12
    body: "Its a take from \"Kaun Banega Crorepati\" which is a very popular Indian version of \"Who wants to be a Millionaire\". Basically, it means \"Who wants to be a KDE Hacker\"."
    author: "Nadeem Hasan"
  - subject: "Re: What does \"Kaun Banega\" mean?"
    date: 2007-07-11
    body: "Hai\n\nFriend I am a proxy analyst who sells proxy for needy people,\n\nI have fresh alive proxy list in all category on reliable cost, I am ready to provide the proxies as you want,\n\nI am also ready to provide fresh alive proxy in daily basis\n\nHere you not need to pay for a dead proxy\n\nMy email address is: dominicubaonline@yahoo.com\n\nYahoo IM : dominicubaonline\n\nPls contact me for proxies\n\nThank you\n\nRegards \n\nbinil\n"
    author: "Dr dominic wooden"
  - subject: "The title means ..."
    date: 2005-12-11
    body: "... \"Who wants to be a KDE hacker?\" in English."
    author: "Arun"
  - subject: "Re: The title means ..."
    date: 2005-12-11
    body: "Thanks."
    author: "Anonymous"
  - subject: "Re: The title means ..."
    date: 2005-12-11
    body: " Exactly!. But this title though looks so simple and obvious but it has a little history that most Indians will surely notice.\n\n Well folks we have a very famous TV quiz game show called - \"Kaun Banega Crorepati\" - based on the original \"Who wants to become a Millionaire?\". The title of the story is pun on that.\n\n After the BOF session at the http://foss.in/2005 conference, the same title was scribbled on the board. You will notice it in the photo if you look closely . It was then suggested by Sirtaj to the dot editors to use the same as a title for the story. The rest as they say is history :)"
    author: "Pradeepto Bhattacharya"
  - subject: "Re: The title means ..."
    date: 2005-12-11
    body: "...and the fact that \"Kaun\" starts with a 'K' also helped, I'm sure :)"
    author: "ac"
  - subject: "Re: The title means ..."
    date: 2005-12-11
    body: "Well you said that not me :P. Heh I was waiting for someone to say that, as they say it was a matter of when and not if!"
    author: "Pradeepto Bhattacharya"
  - subject: "Kudos to KDE-India!"
    date: 2005-12-11
    body: "Kudos to KDE-India Team! I wish more and more people will join it and make it better! Personally, I wish to have KDE in Gujarati Language! And Me and team will start it soon.."
    author: "Kartik Mistry"
  - subject: "Nice"
    date: 2005-12-12
    body: "Great to hear that KDE is getting another strong support, and this time from India. Hopefully this will accelerate the development and spread KDE even more, as well as hope to see new ideas being implemented.\n\nKudos to KDE India team."
    author: "E@zyVG"
  - subject: "Congrats"
    date: 2005-12-12
    body: "Congrats to kde teams."
    author: "Deepak Singh"
  - subject: "Best wishes to KDE india team"
    date: 2005-12-18
    body: "Hope you set the benchmarks for quality KDE development for the world to follow.\ngreetings from bangalore\n\nhttp://www.shantanu.co.nr\n"
    author: "Shantanu Bhadoria"
  - subject: "Bridging the Package Devide"
    date: 2005-12-19
    body: "This was long awaited for us KDE fans in India. Hopefully in.kde tries to bring KDE closer to people in India.\n\nLocalisation efforts are there and I am not commenting on it and it is going smooth somewhere and rough other; but what really matters is that India is a place where Linux lives mostly in student-PCs and there the packages don't reach as we developers find them. Mostly they get RH/Fed/PCQL stuff which do great compromises with KDE which is quite unacceptable (in my view). Now with SuSE going with G* with the next issue the situation will worsen as the majority of the users would be deprived of a good CD/Dvd based distro. In this scenario here in India, in.kde can do good.\n\nAll good suggestions are welcome for that matter."
    author: "Manik Chand"
---
A group of enthusiastic KDE users and developers met last week at the
<a href="http://www.foss.in">FOSS.IN</a> conference in Bangalore, one of the largest Free and Open Source Software meetings in the world, to combine their efforts in various regions of the country under a common banner and build a central platform for all things KDE in India. Along with spreading KDE awareness in India, especially in colleges and
with local businesses, <a href="http://in.kde.org">KDE.in</a> has a few more practical goals. KDE.in
will provide Indian KDE developers and users with a community hub to
coordinate with and support each other.





<!--break-->
<p><div style="float: right; padding: 1ex; border: solid thin grey; width: 250px">
<a href="http://static.kdenews.org/danimo/KDE-in-founding-members.png"><img width="250" height="187" src="http://static.kdenews.org/danimo/KDE-in-founding-members_small.png" /></a><br />
<a href="http://static.kdenews.org/danimo/KDE-in-founding-members.png">The founding members of KDE.in</a>
</div>
Besides driving the ongoing
internationalization and localization efforts, a major aim is to foster
the creation and adaptation of KDE applications to needs specific to India
(the Patpedi cooperative banking systems, for example).</p>

<p><i>"We hope it will encourage more contribution to KDE and FOSS in general
from the large pool of technical, artistic and communication talent
present in India."</i> said Sirtaj Singh Kang, one of the founders of KDE India.</p>

<p>Anyone interested is encouraged to join the mailing list at
<a href="http://mail.kde.org/mailman/listinfo/kde-india">kde-india@kde.org</a> and join the discussions. The website at in.kde.org
will be built up over the coming weeks.</p>




