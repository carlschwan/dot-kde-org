---
title: "KDE to Demo at Montr\u00e9al's Copyright and You Day"
date:    2005-06-23
authors:
  - "amahfouf"
slug:    kde-demo-montréals-copyright-and-you-day
comments:
  - subject: "Possibly Can Help"
    date: 2005-06-23
    body: "There is a POSSIBILITY I can be in the Montreal area that day.. although I don't speak French.. \n\nPlease get in touch with me if you are interested in help manning the booth.  Can't guarantee anything at this point, but I will be travelling to Ottawa for sure around that time, and if it works out, could drive on to Montreal."
    author: "Ian Scott"
  - subject: "Re: Possibly Can Help"
    date: 2005-06-23
    body: "\"please contact the kde-promo list if you can help.\""
    author: "A non"
  - subject: "Re: Possibly Can Help"
    date: 2005-06-23
    body: "Anyone else lives in or near Montreal :)\n\n+1"
    author: "fp"
  - subject: "Re: Possibly Can Help"
    date: 2005-06-23
    body: "Yup. ME!!! Wheee!!!"
    author: "cowardus anonymous"
  - subject: "Re: Possibly Can Help"
    date: 2005-06-23
    body: "yes, you'll be welcome. It does not matter if you don't speak French, most people understand English. And we'll be complementary as my spoken English is quite awful!\nMy family postponed my birthday party to allow me to join this event so a big thanks to my husband and kids!"
    author: "annma"
  - subject: "Re: Possibly Can Help"
    date: 2005-06-23
    body: "Don't worry about the language issue, most people here speak and understand english no problem. Too bad I can't be there, I would have offered my help. "
    author: "Steven Hebert"
  - subject: "Wish I could be there."
    date: 2005-06-23
    body: " Oh man.. I wish I could be there! I always enjoy Stallman's talks, and since KDE is both the first Free Software i startet taking interest in, and still is my most dearly beloved project, it would have been so nice to be there [*Sob-Sob*] :-)\n\n But speaking of Copyright/Free Software/KDE, I would like to point out that I find the QPL/GPL -> LGPL situation with KDE more ideal for Free Software then the vanilla LGPL situation found in other popular DEs. Reasoning as follows:\n\nPlain LGPL: ClosedCorp(TM) can pretty much give heck in our community.\n\nPlain GPL : ClosedCorp(TM) can go hang, or play everything according to our rules, which they wont. So instead they try to out-lobby us and what-not.\n\nQPL/GPL -> LGPL: Weee! ClosedCorp(TM) can choose a mixed situation as they see fit. They can go all Free Software, and they can go all Non Free too... But in any case that involves Non Free they pay runtime license to Trolltec, who in terms again uses [some of] that money on the X server, the KDE, on Qt itself.. \n\n *What a treat*! The latter situation is almost like having a Free Software Tax! Of course Trolltec is not \"bound\" by the wishes/demands of the community, but they have always played nice and fair with us, so we don't develop a reason to threatten them with a mulitplatform LGPL Qt-Clone ;-)\n\n Have a Nice and Free Day(TM)\n\n~macavity\n\n--\n\"Open Source is just a sugar-word that makes the medicine go down on Wall Street and in the big Corps.. At the end of the day it is still, and will always be, plain Free Software as we cherish it!\"\n                                                    -- Anders Juel Jensen"
    author: "macavity"
  - subject: "Re: Wish I could be there."
    date: 2005-06-24
    body: "No dice for Stallman ever coming to the KDE side.  No matter how much he is for pure GPL (which of course is the ultimate downfall of KDE), he is such a megalomaniac that anything that has GNU in its name GNOME will always have Stallman on its side."
    author: "Dave"
  - subject: "Re: Wish I could be there."
    date: 2005-07-05
    body: "IIRC, there are NO runtime licenses for QT, only developer licences... Am I wrong?"
    author: "Renaud"
  - subject: "Re: Wish I could be there."
    date: 2005-07-05
    body: "This is correct for the desktop products.\n\nThe embedded product has licences per unit if I remember correctsly, but that is common practice in that business sector."
    author: "Kevin Krammer"
  - subject: "partly translated"
    date: 2005-06-23
    body: "Richard Stallman will give his speech in english, but the panel will be bilingual, and most of the kiosks will be hosted by bilingual people. Also, there's a translation of the site slowly being updated:\n\nhttp://copyright2005.koumbit.org/index.en.html\n"
    author: "Robin Millette"
  - subject: "Re: partly translated"
    date: 2005-06-23
    body: " If you (or anyone else reading this) is going, and has acces to a video-camara, then please, please, please record his speach (and a lot from the kiosk too!) and make an/several ISO MPEG4 ;-) files. If nessecary you can contact me here, and get my e-mail (I have 4GB mail storage), or my physical address to send me a VHS if you dont have video-in gear. I will go bug my university for serverspace for it (they mirror mostly anything anyway).\n\n~Macavity"
    author: "Macavity"
  - subject: "FILMING, was Re: partly translated"
    date: 2005-07-03
    body: "hey, just a note for those who are interested in getting footage from this event -- indymedia volunteers will be there to shoot film, and everything will be available shortly via http://quebec.indymedia.org\n\nwe will also try to provide translation / subtitles as necessary (and as time allows)."
    author: "simms [quebec.indymedia.org]"
  - subject: "Great"
    date: 2005-06-24
    body: "You can't get a more rational spokesman for open source than RMS."
    author: "Dave"
  - subject: "Re: Great"
    date: 2005-06-28
    body: "RMS is no \"open source\" spokesman. Free Software Foundation, repeat after me ;)\n"
    author: "Robin Millette"
  - subject: "How about a better (closer) location?"
    date: 2005-06-24
    body: "I'd like to see one of these Linux-fests somewhere closer to Columbia, MO, where I live. St. Louis or Kansas City would also work. I have family in Atlanta- even there would be fine. It stinks that most of these events are hundreds or thousands of miles away from anywhere I have any excuse to go."
    author: "SuSE_User"
  - subject: "Re: How about a better (closer) location?"
    date: 2005-06-24
    body: "Just organize one yourself (like the two most recent KDE events were organized), I'm sure there are local people willing to join, and if you can organize funding get some 'higher rank' speakers as well."
    author: "ac"
  - subject: "I'll try to be there"
    date: 2005-06-27
    body: "I'm currently in Montreal (for the summer) but... I'm going to Toronto next weekend (3 day vacation)! I wish I heard this before :( Oh well, I'll try to be back around 18h on sunday (I guess it would be too late to help unfortunately...), so mabye see you there!"
    author: "Pierre-\u00c9tienne Messier"
---
KDE will be present at the <a href="http://copyright2005.koumbit.org/">Droit D'auteur et Vous</a> event in Montr&eacute;al (Qu&eacute;bec) on July 3rd.  The event is to promote Free Software and Richard Stallman will be giving a talk on copyright.  There is an expo for LUGs and other organisations where KDE has booked a stall to show off the latest KDE &amp; KOffice and to hand out limited edition <a href="http://www.kubuntu.org/">Kubuntu</a> CDs. I will need people to man the stall and to print posters and flyers, please contact <a href="https://mail.kde.org/mailman/listinfo/kde-promo">the kde-promo</a> list if you can help.






<!--break-->
