---
title: "Kubuntu Breezy Preview Released"
date:    2005-09-10
authors:
  - "jriddell"
slug:    kubuntu-breezy-preview-released
comments:
  - subject: "administrator mode bug"
    date: 2005-09-10
    body: "Have they fixed the problem w/ launching admin mode in Control Center?  I'm really kind of shocked that that one's stayed in for so long."
    author: "jaykayess"
  - subject: "Re: administrator mode bug"
    date: 2005-09-10
    body: "The only way I got it working was by starting it from the Konsole with admin mode.\n\nOr the easiest way just enable the admin account with the same pwd, so I wouldn't notice the problem anymore...\n\nIt was also a problem when using Synaptic...\n\nI do not like that sudo option, to much problems..."
    author: "Boemer"
  - subject: "Re: administrator mode bug"
    date: 2005-09-10
    body: "it works here now."
    author: "superstoned"
  - subject: "Re: administrator mode bug"
    date: 2005-09-10
    body: "It worked for me for a while, and it was said that the bug came from \"upstream\", presumably meaning that it was actually a KDE bug. A few updates later and administrator mode is as non-functional as it was before. It's a good job I know my way around the Debian configuration files..."
    author: "The Badger"
  - subject: "Re: administrator mode bug"
    date: 2005-10-12
    body: "While in KDE Control Module for printing, I click on Admin Mode, enter root password and appears as if I entered the wrong password for root, but it is correct.  I'm using kubuntu dist with universal in the repository for updates.\n\n~Darren"
    author: "Darren"
  - subject: "Re: administrator mode bug"
    date: 2005-11-10
    body: "Same problem here.. I'm on Debian Etch/Sid."
    author: "kellemes"
  - subject: "Re: administrator mode bug"
    date: 2006-10-12
    body: "I have the same problem.\nMy distro is Debian GNU/Linux \"Sid\"."
    author: "Strider"
  - subject: "..screenshots"
    date: 2005-09-10
    body: "anyone have screenshots of:\n\n* System Settings - a user friendly replacement for KControl \n* A simplified Konqueror profile\n\n?"
    author: "me"
  - subject: "Re: ..screenshots"
    date: 2005-09-10
    body: "http://shots.osdir.com/slideshows/slideshow.php?release=433&slide=11\nhttp://shots.osdir.com/slideshows/slideshow.php?release=433&slide=20"
    author: "Klindworth"
  - subject: "Re: ..screenshots"
    date: 2005-09-10
    body: "The settings are very... inspired by OSX.  Ah, well.\n\nI like the simplified browser - I have my Konqueror set up almost the same although I do have the viewmode toolbar in there as well and omitted reload, as I just hit F5.  I also have a View Profile button at the far right so I can jump between workspaces.\n\nI'm using Kubuntu Hoary (the latest stable version), and it has been stable and bug free other than some issues with amarok crashing.  I'm using KDE 3.4.2, and I'm quite happy with it.  It's the first thing to move me away from SUSE."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: ..screenshots"
    date: 2005-09-20
    body: "Does anyone know if \"System Settings\" is a ubuntu-only development, or is it part of KDE 3.5? \n\nThanks!"
    author: "norberto"
  - subject: "Re: ..screenshots"
    date: 2005-09-21
    body: "It's developed in KDE's playground/base svn repository."
    author: "Anonymous"
  - subject: "Re: ..screenshots"
    date: 2005-09-21
    body: "Thanks :)"
    author: "Norberto"
  - subject: "Kubuntu Live CD"
    date: 2005-09-10
    body: "Does anyone actually get to download the real Kubuntu Live CD? the IS Kubuntu site provide is just an Ubuntu install CD! :( (both normal and BT one)"
    author: "Davide Ferrari"
  - subject: "Re: Kubuntu Live CD"
    date: 2005-09-11
    body: "http://releases.ubuntu.com/kubuntu/breezy/"
    author: "carsten"
  - subject: "Re: Kubuntu Live CD"
    date: 2006-09-02
    body: "YES! ive been looking for that forever, thanks! is this user friendly? i am kinda new to the linux scene."
    author: "Narcotica"
  - subject: "Re: Kubuntu Live CD"
    date: 2008-07-07
    body: "Yes, this is based off of Ubuntu which is desinged to be easy for everyone to use."
    author: "Nerd"
  - subject: "too bad"
    date: 2005-09-10
    body: "it generally works well and it is quite fast, so the kubuntu dev's did a great job. not as pollished as ubuntu with gnome (tried that just before kubuntu) but nice.\n\nmy biggest irritation was usb-disk support. after a install, i attached my usb mp3player, and just got a window with the error \"cannot read from /media/sda1\" or something like that. they really should fix that one..."
    author: "superstoned"
  - subject: "Re: too bad"
    date: 2005-09-11
    body: "This is a known bug and i think it will not be in the stable release."
    author: "Volker"
  - subject: "Re: too bad"
    date: 2005-09-17
    body: "Yes, that was fixed last night.\n"
    author: "Jonathan Riddell"
  - subject: "Re: too bad"
    date: 2005-10-11
    body: "That was fixed only in i386 version, not the amd64 one.\n\nfrom kubuntuforums:\n\nRather strange, I had a bug with usb disks and kubuntu prereleases on i386 arch that on recent installation and updates was fixed.\n\nBut on a recent kubuntu 5.10 (today) for amd64 I saw the same bug, already fixed in i386 ubuntu over a week ago.\n\nThe bug was when I connect a usb pen (tried severals) or an external IDE drive and konqueror shows up saying \"media:/dev/sdc1 does not exists\" but the right device is pmounted on /media/usbdisk and work like a charm. Only the kde part (media:/ and desktop icon that does not shows up) seems to be broken. Also dmesg are fines and like the i386 ones.\n\nI know how to manually make it work via fstab, but I lose some multi-usb flexibility that pmount permits me on i386. I Really don't know why it should be related on amd64 if no there is no lag between the archs.\n\nbtw in debian sid (not particulary related) both in i386 and amd64 media:/ work with usb..\n\nIn the same kubuntu 5.10 pre amd64 that I'm talking about, media:/ works for both floppy and cdroms/dvds, at last it seems only to be a usb related bug."
    author: "Me"
  - subject: "Debian friendliness"
    date: 2005-09-10
    body: "How debian friendly is it? Lately I'm having a hard time mixing hoary and woody packages.\n\nDoes it have gcc4? Libc6? Cant really see much detail on the announcement....but goodie goodie goodie, more kubuntu can only be a good thing!\n\nTim\n"
    author: "Tim Sutton"
  - subject: "Re: Debian friendliness"
    date: 2005-09-10
    body: "I assume -- since AFAIK Kubuntu is Ubuntu with KDE packages -- that it has the same base packages as the new Ubuntu preview. At the bottom of the list of updates in the Ubuntu announcement linked to from the Kubuntu announcement:\n \"Under the hood\"\n\n  * GCC 4.0.1\n  * glibc 2.3.5\n  * New early userspace infrastructure based on initramfs-tools\n  * More modular X.org packaging"
    author: "Graeme"
  - subject: "at least they include gnome stuff"
    date: 2005-09-10
    body: "I am very happy that kubuntu5.10 has libgnome and gtk+, I know you can download but, for a far little country without internet access. this is not granted at all.\n\n  and many software like firefox and adobe need that stuff.\n\n  keep the good job."
    author: "mimoune"
  - subject: "Necessary?"
    date: 2005-09-10
    body: "Is it necessary to make a release with 3.4.2 while kde 3.5 is going to be released?\nIt comes with the same xorg 6.8.2, kernel 2.6.12 and qt 3.3.4...\n\nWouldn't be better to have a version that brought new things? I've been using kubuntu hoary since it was released and I feel trapped withoy backports and so, if a release comes like this, nobody is going to use the standard instalation, I don't think that it makes a lot of sense.\n\nBye!"
    author: "apol"
  - subject: "Re: Necessary?"
    date: 2005-09-10
    body: "I think it's mainly because GNOME 2.12 was released yesterday and the Ubuntu devs release a beta CD too thus the Kubuntu had to release one too."
    author: "Patrick Davies"
  - subject: "Re: Necessary?"
    date: 2005-09-10
    body: "Also: KDE 3.5 is not quite around the corner.\n\nFrom the Release Schedule:\nhttp://developer.kde.org/development-versions/kde-3.5-release-plan.html\n\n\nSeptember 13th, 2005: Beta1 is prepared\n\n Beta 1 is prepared and released after some initial testing. The incoming bugs will be reviewed for their severity. \n\nAfter that?\n\n We don't know yet how things develop and the Akademy event might change something unforseen. So I would prefer to leave it open if we have a second beta or release right after beta1. I would guess, we don't need a second one for KDE 3.5. So the targeted release day would be towards end of october (4 weeks of giving beta1 testing, one or two weeks for RCs - release)"
    author: "MandrakeUser"
  - subject: "Re: Necessary?"
    date: 2005-09-19
    body: "I've heard October 13th cited several times.  No \"official\" word on the website, apparently, and I don't follow the mailing lists or however Ubuntu/Kubuntu communicates.\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Necessary?"
    date: 2005-09-19
    body: "And as soon as I post that, I find it.\n\nhttps://wiki.ubuntu.com/BreezyReleaseSchedule"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Necessary?"
    date: 2005-09-12
    body: "I'd agree if it were KDE 4.0 and if \"around the corner\" were a bit closer.  As it is, KDE in Kubuntu is easily upgradable and I'm sure that 3.5 will be available for Breezy as soon as it comes out.\n\n3.5 will probably not be sufficiently different to have completely different base requirements and should install on Breezy with just a few upgrades of dependencies."
    author: "Evan \"JabberWokky\" E."
  - subject: "LILO (K)ubuntu fix in Breezy?"
    date: 2005-09-13
    body: "I had problem using lilo in hoary, dual booting with XP, but I managed to do this manually (I am using Lilo debian splash). Is this fix already in breezy already? Any screenshot in (K)ubuntu Lilo splash? I am just wondering why there is no entry in kde-look.org? Try to do search \"ubuntu\" lilo splash in kde-look.org to see it your self? Hence, the enquiry about.\n\nThanks, NS "
    author: "NS"
  - subject: "I Need Help With The Admin Settings Tab in K Menu"
    date: 2008-04-09
    body: "I recently installed linux Kubuntu and cant seem to figure out the admin account that is written into the program. it worked fine until i had to reinstall because i forgot my password. After i reinstalled Kubuntu i could not find the administrator Settings tab under the K Menu. Please help me if you can. "
    author: "George Bingham"
  - subject: "Re: I Need Help With The Admin Settings Tab in K M"
    date: 2008-04-09
    body: "You are unlikely to get help posting in the comments section of a 2-and-a-half year old article ;) (I only spotted you by a fluke).\n\nThe Ubuntu Forums (http://ubuntuforums.org/forumdisplay.php?f=73) are an excellent source of information, so I'd suggest searching/ posting there.\n\nGood luck!"
    author: "Anon"
---
The preview release of Kubuntu 5.10 (Breezy) has been released.  The <a href="http://www.kubuntu.org/breezy-preview.php">release announcement</a> lists the new features and where to download.  Kubuntu Breezy includes the latest KDE 3.4.2.  Although this is a beta release it should be quite safe for everyday use, so give it a shot and <a href="https://wiki.ubuntu.com/KubuntuBreezyPreviewComments">let us know how you get on</a>.


<!--break-->
