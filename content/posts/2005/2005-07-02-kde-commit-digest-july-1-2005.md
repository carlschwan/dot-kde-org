---
title: "KDE Commit Digest for July 1, 2005"
date:    2005-07-02
authors:
  - "dkite"
slug:    kde-commit-digest-july-1-2005
comments:
  - subject: "kdereview?"
    date: 2005-07-02
    body: "Still no listings about kdereview.  I see kdenonbeta and playground, but no kdereview.  I'm pretty sure it was listed in the CVS Digest before things moved over to Commit Digest.\n"
    author: "Ryan"
  - subject: "Re: kdereview?"
    date: 2005-07-05
    body: "Well, about kdereview I can tell you that KNetworkConf improved it's Wifi support, now you can configure the WEP key and essid of a wireless interface and we're finishing network profiles support."
    author: "Maeztro"
  - subject: "system:/ "
    date: 2005-07-02
    body: "Regarding \"home:/\" JRT commented:\n\nOne thing which the user doesn't need is: \"Home\".  It should be replaced with: \"Documents\" although I think that it could use a better name (I call mine \"USR Home\").  There is nothing in the $HOME directory that a user needs to access except when doing administration tasks. \n\n"
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Home is the user's directory for _everything_ not just files which are for system administration. nor is \"Documents\" a good name. I for one house very little documents in my home directory, and quite frankly that name would give it the idea that documents is the only thing it can hold."
    author: "Stephen Leaf"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "I think he maybe means there should be a \"Documents\" subdirectorey in $HOME and this should be linked in system:/"
    author: "panzi"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "\"Home is the user's directory for _everything_ not just files which are for system administration\"\n\nRight. The problem is, home is generally treated like My Documents on Windows, when it is more like Documents and Settings\\User\\ on Windows.\n\nI think sticking a home icon on the desktop is a bad idea for most users. As other people have mentioned, this should be different."
    author: "mmebane"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Why? Where would you suggest should be treated like My Documents instead? Home is the place for everything belonging to the user."
    author: "mikeyd"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Why do we need something like that? We are NOT Windows! We don't have to emulate them!"
    author: "Brandybuck"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Well unless you have some kind of magic users need somewhere to actually, you know, save their documents."
    author: "mikeyd"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Maybe have a ~/data/ or something?"
    author: "mmebane"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Actually that's exactly the path I point my KDE \"document\" path to.\nSo it is the one showing up in file dialogs.\n"
    author: "Kevin Krammer"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "Possible, but why? Preferences are all in dot files, so there isn't (to me) any obvious need for a separate directory for \"proper files\", it's just another five keys to type."
    author: "mikeyd"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "The whole thing sounds to me more like someone are trying to solve a made up problem. Lots of work and discussion of something that's not a real problem, based on some strange idea of it being a \"usability problem\". The Unix way with users home directory and hidden files/folders already makes this a non issue. If the home directory contains visible files and folders not explicit put there by the user, the it's install or distribution is broken. Creating a workaround for this in KDE are rather pointless. \n\nSince KDE and Konqueror by defalt don't show hidden files or directories the only thing visible for a new user in $HOME, in a sane installation, are Desktop and tmp. Even newbies does not get confused by that. Anything else are creations of the users, as it should be. It is rather pointless to dream up a usability problem for this to solve."
    author: "Morty"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "Unfortunately, preferences are not always in dot files and dot folders and there are other files and folders in my $HOME including some that I found it useful to add.  But, if you don't have an obvious need to set your: \"Documents path\" to something other than $HOME, why does that mean that you should say that the same MUST apply to everyone else?"
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "Those programs that stores their settings in your $HOME and not using dot files or folders are broken, file a bugreport and get it fixed. It's a bug afterall. If you add files yourself which does not belong in $HOME, why not add them somewhere else? If the files annoy or confuse you, put them somewhere else then.\n\nThe thing you are after are to hide files from users, and the way to do this already exist. All KDE programs does this by default, until you explicit tells them to show the hidden files. \n\n"
    author: "Morty"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "Exactly what is you point?  Are you suggesting that we no longer have the option of setting the: \"Documents path\" to a directory other than $HOME?  If that is your point, or implicit in your argument, you certainly have not made a convincing argument.\n\nI find it more convenient to have the directories holding my documents, graphics, music, photos, etc. in a directory separate from $HOME.  The reason is that there are already over a hundred files or folders in $HOME.  Having made this decision, I also find it convenient to have other administration stuff in $HOME.  \n\nYou suggest that I should be forced to have the documents in $HOME and put my additional administration files somewhere else.  Why is this better than making a subdirectory for my user files?\n\nBut, be sure that you understand that what I suggest is permissive.  If users still want to set their \"Documents path\" to $HOME, they can do so.  With the \"home:/\" I/O slave, this is effectively REQUIRED since it is quite inconvenient to do so.  What I am suggesting is that we have, instead, a \"documents:/\" I/O slave that points to the directory specified in the \"Documents path\" which can be $HOME or a subdirectory of it (wherever the user wants)."
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "What my point is, for normal users the current setup with $HOME are already working and the right way to do it. It also have the big advantage it also works with non KDE applications.\n\nThat some advanced users, like you MAY want to have a different solution for some reason, are not the main point. By making Konqueror and filechoosers correctly follow what you call \"Documents path\", or the users defined homedirectory setting. And I think it's much smarter to fix this to work as it was supposed to work, than creating a complex I/O slave to do the same job.\n\nWhat I am against is creating some new piece of infrastructure, who really don't bring anything new or usefull for the waste majority of users. Rather than using and fixing the simple and elegant solution already in place, for those few with a different need. All in all, for the waste majority of users it neither usefull, or solves some kind of usability problem. "
    author: "Morty"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "> By making Konqueror and filechoosers correctly follow what you call \n> \"Documents path\", or the users defined homedirectory setting.\n\nHELLO! 'what *I* call the documents path'??  Do you use KDE??  In the Control Center go to: \"System Administration -> Paths\".  See that third line: \"Documents path\".  This is what *KDE* calls the default working directory: \"Documents\".\n\nYes, the _current_ default for this setting is $HOME.  But, as I said somewhere, it is a design error to design something based on the (incorrect) assumption that the default will allways be used.  Doing it wrong might be simple but it is certainly not elegant.\n\nJust because the default for \"Documents\" is $HOME does not mean that it is the same as $HOME.  Presuming that it is breaks the current design of KDE.\n\nThe way it is supposed to work is that the default working path is determined by this \"Documents path\" setting.  When you set this, it is set in: \"kdeglobals\".  So, for it to work correctly, this directory needs to be referred to as \"Documents\"; it should not be referred to as Home just because the current default is that both reffer to the same location.\n\nI do not consider the way I have my system set up to be for advanced users.  I think that it is easier to use and therefore suitable for all users.  I have a directory that is a subdirectory of $HOME and have the \"Documents path\" set to the path to that directory.  All of my user files are stored in subdirectories of this directory.  I have a link on the DeskTop to this directory which replaces the \"Home\" link.  So, when I want to open a file, I don't have to worry about all of the junk in $HOME, I just go directly to the files directory.  Isn't this why we have the tree directory structure -- or should we just throw everything in the same directory?"
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "Since this works flawlessly for me with $HOME, I haven't bothered to verify the exact wording :-) For all that I care it could have been called \"My Stuff\", which actually are a much more precise name than \"Documents\". But i digress, the name is not the important part.\n\nAnd if you actually read what I wrote, you see we agree on the concept of going directly to the files directory. Be it $HOME or $HOME/USR-Home, since the path setting are there it should work correctly for all KDE apps. Even for those with special/advanced needs not using $HOME. \n\nBut I see no reason to force a new layer of semantics on top of the filesystem(IO-slave). Or forcing a default move away from $HOME, because of some broken applications storing junk there. Anyway, I can't remember last time I saw one doing it. But such an application should have the bug fixed.\n\nAnd by moving away from $HOME by default you create a new set of usability problems, for users not running purely KDE applications. A user with knowledge to change the default directory, will most likely have knowledge to handle the effect of this on non KDE programs. A less skilled user will not, and should not have to face the issue."
    author: "Morty"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "You still don't get it.\n\nIf you have your \"Documents\" directory set to $HOME and you are using \"home:/\" to reference them you have no problem (except for the label).\n\nBut, if you change \"Documents\" to something else (e.g. $HOME/Files), then \"home:/\" still points to $HOME and you have a problem.\n\nOTOH, if you reference the \"Documents\" directory with \"documents:/\" which points to whatever you set the \"Documents path\" to, then it will still work fine if you use the default of $HOME.  And (most important), if you change \"Documents\" to something else (e.g. $Home/Files) then it still works -- it still points to the default working directory specified with \"Documents path\".\n\nUsing \"home:/\" to reference documents is a design error.\n\nYes, currently, the working path only gets set to \"Documents\" for KDE applications.  For non-KDE applications it is still $HOME.  This is a bug in KLauncher that needs to be fixed.  It is perhaps another example of a design error caused by assuming that everyone would use the default.  "
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-04
    body: "Ok, now I see what you are really getting at, and where you missed the point. The \"Documents\" setting should act as default directory when opening anything, like Konqueror as filemanager, any file chooser or the home button when in filemanger mode. Nothing more. The location bar should follow the actual path, like it is today. The creation of another abstraction layer for the filesystem, are the design error. \n\nUsing a IO-slave like home:/, referring to a arbitrary location is where the mistake is. You already have one abstraction in the form of ~, making another specific to KDE will only cause usability problems with regards to non KDE applications. Will KLauncher change it in the filechooser of a GTK\napplication too? As long as we still live with hierarchical filesystem, it will only cause real usability problems. The users not finding their files in $HOME when using non KDE applications. Since KDE  does not follow the standard and define $HOME as something else, like $HOME/Files or something."
    author: "Morty"
  - subject: "Re: system:/ "
    date: 2005-07-04
    body: "> Will KLauncher change it in the filechooser of a GTK application too? \n\nIt already can do this -- change the current directory of a non-KDE application.  To do this, you must set the: \"path=\" paramater in the 'desktop' file.  You can do this with the GUI, with the \"Work Path\" setting either in the Menu Editor or the Application tab of the Properties dialog.\n\nPerhaps the user should have a choice of whether or not to apply the KDE default Work Path to non-KDE applications but I don't see how it is going to cause a usability problem if what users are doing is using non-KDE apps with the KDE desktop.  I would suggest that it be turned on as the default if it is configurable.\n\nI expect to find my GIMP files in: \"$HOME/USR-Home/Graphics\" and that is where The GIMP stores them as a default.\n\n$HOME will not be changed.  We could define an additional environment variable USR_HOME (or whatever we want to call it) if we need to.  But what will be changed is the addition of a \"documents:/\" I/O slave that will be a subset of: \"system:/\" that will point to the base directory for storing the user's files."
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "I don't say it must apply to everyone else. However, it's a perfectly good default documents path, and as such there's nothing wrong and a lot right with having an icon for it on the desktop by default."
    author: "mikeyd"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "I am talking about the: \"system:/\" I/O slave NOT the file system.  The \"home\" entry which references $HOME directories should be removed and replaced with an entry which references the directories pointed to by the \"Documents path\" settings.  The \"kfm_home\" icon is OK (something else would be better [that icon isn't always a house in other themes]) but it should be called something other than \"Home Folder\".  Since the default for the \"Documents path\" is currently $HOME, this does NOT _require_ that any change be made in how the user's home directory is set up.  What it does is _permit_ it to be set up differently.  I have my \"Documents path\" set to something else so I immediately noticed that I couldn't go directly there using: \"System\".\n\nWe should never assume that the default will always be what is used.  It is a design error to do so.  This is not the only place where this applies and this is not the only place that the mistake of assuming that the default will always be used is being made."
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-07
    body: "Since it sounds like there's an option for KDE to set a \"Documents path\" (I'm sadly in front of WinXP at work right now, and can't look at this), this totally makes sense.\n\nFor example, if I do 99% of my work on some shared drive that's mounted somewhere outside of my $HOME directory, having a quick link via \"system:/\" to that directory is much easier than navigating there every single time, especially if KDE apps default to load/save files in that location.\n"
    author: "Bill Kendrick"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "As KDE already has a configurable path for documents (kde-config --userpath document), maybe home:/ should point to that directory.\n\nThe default is $HOME if I remember correctly, but a distributor or admin could change that if they felt $HOME would confuse their users.\n\nIn either case it could be necessary for the KDE launcher to change the working directory to whatever home:/ points to prior to launching a non-KDE application.\n\nCheers,\n_"
    author: "Kevin Krammer"
  - subject: "Re: system:/ "
    date: 2005-07-02
    body: "<em>There is nothing in the $HOME directory that a user needs to access except when doing administration tasks.</em>\n\nHuh? Not everything in my home directory is a document, unless you count \"file\" as synonymous with \"document\". I'll have scripts in there and other executables. I have source code and other stuff.\n\n\"Document\" tells the user that it contains only documents, which is incorrect. On the other hand, \"Home\" implies that it contains everything the user owns, which is correct. \"Home\" isn't more difficult to understand. The concept is simple. There is no usability issue here."
    author: "Brandybuck"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "> Huh? Not everything in my home directory is a document, unless you count \n> \"file\" as synonymous with \"document\". I'll have scripts in there and other \n> executables. I have source code and other stuff.\n\nI don't keep any documents in my $HOME directory.  That is, I don't keep any user data files in $HOME, I keep them all in subdirectories of $HOME/USR-Home.  The reason for this is that there is a lot of other junk in my $HOME directory.\n\nSince you appear to agree with my argument, perhaps you misunderstood.\n\n> \"Document\" tells the user that it contains only documents, which is \n> incorrect. On the other hand, \"Home\" implies that it contains everything the \n> user owns, which is correct. \"Home\" isn't more difficult to understand.\n\nThis isn't about a different name for a directory, it is about a different directory -- a directory that might be called \"Documents\".  Since we already call the path to the default Working Directory: \"Documents\" the I/O slave should probably be called \"documents:/\""
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "Au contraire, there is nothing in the home directory under most default installations, except dot files, which (take note JRT) ARE INVISIBLE TO THE USER. I create my own directory hierarchy under /home/luke ... i have `/home/luke/documents,' `/home/luke/music'... notice how `music' is not a subcategory of `documents,' at least not in my mind and in the minds of most users, but Bill Gate's mind it is, and that is why nobody actually uses the Windows filesystem hierarchy. I believe this has already been discussed and rejected on kde-usability. I think that \"home\" is a better metaphore for personal files anyway.\n\nI also disagree with home:/ being a list of other users' home dirs. I think it should be a redirection to your home directory. `home:/music'; would better than `/home/luke/music' (supposedly), but `home:/luke/music' is worse. If you want to share files with other users in your group (very few systems are multiuser these days, and even fewer share files between users), why not have it be `user:/john' or something?"
    author: "LuckySandal"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "> notice how `music' is not a subcategory of `documents,' \n\nYes, I noticed.  I noticed it long before you posted.\n\nIMO, that is why the \"Documents\" directory needs to be called something else.  In the Control Center, it should be called what it is \"Working Path\"; users can name it whatever they want (perhaps \"Files\" is a good default).  I have: \"/home/jrt/USR-Home/Documents\", \"/home/jrt/USR-Home/Music\" and my Documents Path is: /home/jrt/USR-Home.  Now the question is: why is your system better than what I suggested?  Even if all of the files in $HOME were dot-files, would your system be better?  Wouldn't it still be better to have a separate folder for your default working directory.  I have over a HUNDRED  configuration files and folders in my $HOME directory and, unfortunately, they are not all dot files and dot folders.  And these dot files and folders are only invisible to the user when: \"View -> Show Hidden Files\" is off."
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "> IMO, that is why the \"Documents\" directory needs to be called something else.\n\nSo you are basically arguing that because \"Documents\" is basically a save location for any type of user files (not necessarily \"documents\" in the proper sense) then it should be renamed. In that case I might actually agree, and I would just set mine to my home directory. However, I currently have my \"Documents\" path set to (gasp) /home/luke/documents because normally anything that I would not consider a \"document\" already has a path which its corresponding program is configured to use. But I'm not sure what the creators of the \"Documents\" path really intended.\n\nAs for these programs that place non-hidden configuration files in you home directory, I would like to know what they are and if they are configurable. If they are things like legacy console programs that average users are not going to run anyway, I don't see why it is an issue (for instance, you, being an advanced user, were able to solve this problem in your own way).\n\n "
    author: "LuckySandal"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "> But I'm not sure what the creators of the \"Documents\" path really intended.\n\nThe \"Documents path\" is the default \"Working Path\" for KDE applications -- that is what it is so that must be what was intended.  Why it is called \"Documents path\" in the Control Center: \"System Administration -> Paths\" something I don't understand.  Why isn't it called: \"Default Working path\" since that is what it is? \n\nAlso note that just because most of the configuration stuff is hidden doesn't mean that it doesn't get in the way.  Not everything hides the so-called hidden files."
    author: "James Richard Tyrer"
  - subject: "Re: system:/ "
    date: 2005-07-03
    body: "> Also note that just because most of the configuration stuff is hidden \n> doesn't mean that it doesn't get in the way. Not everything hides the \n> so-called hidden files.\n\nYeah until recently the KDE folder selection dialog didn't hide them.... "
    author: "LuckySandal"
  - subject: "amaroK & streams"
    date: 2005-07-02
    body: "> amaroK adds support for media:/ urls.\n\nDoes this mean that amaroK will now be able to play streaming media? The las time I checked, I was being asked to do some kind of upgrade (I've forgotten which)!\n\nWhen will the play/pause button be \"fixed?\""
    author: "charles"
  - subject: "Re: amaroK & streams"
    date: 2005-07-02
    body: "Amarok always has played streaming media for me? I can perfectly listen to MP3-streams on the Internet.\n\nTo see what media:/ does, just type it in your Konqueror URI bar, it will show you a list of all devices on your system (hard drive partitions, usb mass storage devices,...). It is comparable to My Computer on Windows, but it just works for unmounted partitions too."
    author: "Anonymous Coward"
  - subject: "Re: amaroK & streams"
    date: 2005-07-02
    body: "THE PLAY/PAUSE BUTTON EXISTS!!!!!\n\nAll you have to do is change your toolbar settings."
    author: "Seb Ruiz"
  - subject: "Re: amaroK & streams"
    date: 2005-07-02
    body: "Yes ... but it does work with the system tray icon!"
    author: "Veton"
  - subject: "Re: amaroK & streams"
    date: 2005-07-03
    body: "Just middle click on the tray icon."
    author: "Ian Monroe"
  - subject: "Re: amaroK & streams"
    date: 2005-07-03
    body: "But the play/pause button wants to have little buttonettes with the stop button. Please don't fix it. :("
    author: "Ian Monroe"
  - subject: "media:/ kioslave?"
    date: 2005-07-02
    body: "Isn't media:/ a kioslave ? Shouldn't Amarok support this through KDE libs?"
    author: "anonymous"
  - subject: "Re: media:/ kioslave?"
    date: 2005-07-02
    body: "But engines aren't kde applications, and xine doesn't support kioslaves, so it's necessary convert media:/ urls to file:/ urls"
    author: "Sergio"
  - subject: "Re: media:/ kioslave?"
    date: 2005-07-04
    body: "ah offcourse :)\nThanks for the answer"
    author: "anonymous"
  - subject: "The Evil Plan!!!"
    date: 2005-07-02
    body: "I with you... :)"
    author: "Fast_Rizwaan"
  - subject: "fd.o to the rescue"
    date: 2005-07-02
    body: "hhm, guess it'd be easy enough to talk to the gnome folks and come up with a solution which works for both desktops..."
    author: "bangert"
  - subject: "New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: "Chinchilla - a linear movie editor :) shouldn't it be name \"KMovieEdit\" or \"kme\" or \"KVideoEdit\"?\n\nMy Wishlist:\n\n1. KRichEdit       - A Richtext editor.\n2. KSoundEdit      - A sound editor.\n3. KSoundRecord    - A sound recorder.\n4. KVideoRecord    - A video recorder.\n5. KVideoEdit      - A linear movie editor. (chinchilla :()\n6. KUserSettings   - A KDE settings (bookmark, app. settings, fonts, etc.) save/restore application.\n7. KScreenKeyboard - A Screen Keyboard for users to type unicode text.\n\n\nI really confused by \"weird\" names for applications. I would better appreciate symbolic names which makes sense.\n\nKVideoEdit more describes about the application than chinchilla. \n\nln -sf chinchilla kvideoedit.\nln -sf noatun     kdemediaplayer\nln -sf gwenview   kviewimage\nln -sf kopete     kmessenger\nln -sf guarddog   kfirewall\nln -sf kdehelpcenter khelp\nln -sf kolourpaint kpaint\nln -sf k3b        knero\nln -sf k3b        kcdburn\nln -sf k3b        kdvdburn\nln -sf amarok     ksongs\nln -sf apollon    kp2p\n\njust my thoughts for KDesktopUsers ;)..."
    author: "Fast_Rizwaan"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: "As a first measure, I suggest:\n\nln -sf Fast_Rizwaan KDEBlogUser\n"
    author: "Mark Kretschmann"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: "If you enable descriptions in the K-menu, you get those nice (imo bland) descriptions.  If you all of a sudden start changing names, then people would get more confused."
    author: "Sam Weber"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: ">Mark Kretschmann\n>ln -sf Fast_Rizwaan KDEBlogUser\n\nhappy? ;)\n"
    author: "KDEBlogUser"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: "The dot is not your personal blog site?"
    author: "Anonymous"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: "The problem of trivial names is that there is a huge probability that they are already trademarks. KDE's trademark problems were all on trivial names. (That is why Krita is named Krita and not anything with \"paint\".)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-02
    body: "Never do this please, you can have descriptive names in the menus, but why should there only be one kdemediapalyer? And if another program becomes the best kde solution and is included in KDE by default, should we change its name to kdemediaplayer?"
    author: "Martin Stubenschrott"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-03
    body: "KDE, as you should well realize, is not the first time non descriptive names have been used for objects in every day life.\nEven your suggestion of Nero would only be descriptive to perhaps learned individuals who know that Nero actually did burn Rome (Rome in Italy) and see the pun. That is hardly helpful to the (possibly less than) average user.\n\nThis naming scheme also isn't limited to KDE in the computing world, you have Firefox, Konqueror, Opera to name some web browsers. You have Excel, Outlook, Quicktime etc... Despite this I will agree that some applications may follow your desires already, Openoffice Writer, Math, Draw, Calc etc... and you already have many k* applications such as KGet, Konversation, KView, KPDF, KColorChooser, but as has been mentioned what do you name another colour chooser which is maybe better, KColorChooser2 ? How confusing would that be ?\n\nIn any case do you drive your F*RedneckTransportingTruck or Ford F150 ? I think calling it an F150 is in everyone's best interest. Even the extremely good marketing people at a Japanese company such as... Toyota... have the Avalon/Aventis. Although Avalon was an Island visited by King Arthur it hardly describes a 4 wheeled moving vehicle. And as far as I know Aventis doesn't mean anything although it may resemble some words such as the verb advento(to approach), Aventinus(hills near Rome) or even ventus(wind).\n\nSo really the naming scheme we have had for quite some time, naming things with numbers, after people, animals, plants, objects, invented words, etc... has worked well and will continue to.\n\nAfter all from the position of a developer, if you are too lazy to read a description, you don't belong using my software. Natural selection may retake its course."
    author: "Anonymous"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-04
    body: "You're trolling, arent' you?"
    author: "Davide Ferrari"
  - subject: "Re: New KDE 4 Applications... and their naming!"
    date: 2005-07-04
    body: "Now just strip the 'k's from the names and you end up with something reasonable ;-)"
    author: "Stefan Heimers"
  - subject: "Desktop Users!!!"
    date: 2005-07-02
    body: "and newbies they just know these words: \"song(s)\", \"video\", \"mail\", \"letter\", \"picture\", \"cartoon\", \"document\", \"internet\", and more human names.\n\nYesterday, I saw my one of my friends wishing to hear a song on My KDE 3.4.1 desktop. I had my konsole opened. just went out for a min. And here's what he typed:\n\n1. songs - he wants to listen to songs\n2. video - he wants to view some videos\n3. picture - to see some family photos\n4. internet - to browse internet.\n\nBut unfortunately, KDE is not HUMAN Desktop Environment. KDE/Unix can't understand humans. \n\nI thought why not have a script+kommander application which will show a Wizard/Dialog with options like when he typed \"songs\" and pressed enter:\n\n-------------------------------------------------\nHi there, it seems that you want to listen songs\n\nWhat would you like to do (click on it or press \nrespective number)\n\n1. Play a song... // again a dialog / application (amarok?)appears\n2. Search for a song...\n3. Record a Song... (from radio, tv, microphone, etc.)\n4. Edit a song...\n\n5. Good Bye\n\n---------------------------------------------------\nHere Amarok is a good application which can find, filter a song. If such a dialog appears, even a computer illiterate HUMAN could use KDE.\n\na bash script + kommander script will work wonder for newbies. just my 2 cents for Human users."
    author: "fast_rizwaan"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "that sounds like waht the developers are trying to do with plasma and ALI you should check it out it sounds like they know that problem and are trying to solve it."
    author: "sv"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "url?"
    author: "gerd"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "Plasma is more a concept to integrate the three permant planes (desktop, panel, on-screen display layer) into one unified solution.\n\nhttp://plasma.bddf.ca/\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "What is bddf.ca?  The company behind Plasma?"
    author: "Anonymous"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "simply the hosting company, i guess. plasma is (check the website) fully developed by the KDE community."
    author: "superstoned"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "Normal non-Unix endusers don't type commands into shells.\n\nThey use GUI lauch methods, for example application menus or clicking on files in the file manager.\n\nIf you think shells should understand those concepts as well, go complain to the shell developers.\n"
    author: "Kevin Krammer"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "that's right, normal desktop users don't use konsole much. A desktop shortcut or Plasma links would be good."
    author: "KDEBlogUser"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "Konsole is just a way of running a shell which is certainly not part of kde. Why didn't he just click the menu button in the bottom left, look through and figure out he wanted multimedia, and then click (again after some looking through) Music Player (JuK), ignoring the part in brackets? That's what people I've introduced to kde seem to do. KDE is human-friendly, it's just the unix shell that isn't. Newbies won't be using the shell. I have a suspicion you're a troll."
    author: "mikeyd"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: ">Why didn't he just click the menu button in the bottom left, look through and figure out he wanted multimedia, and then click (again after some looking through) Music Player (JuK), ignoring the part in brackets?\n\ncool down dude, my friend never saw KDE at the first place. And he is not a nerd. did you read, \"I had my konsole opened. just went out for a min.\"?\n\nbtw the 'K' blue icon does not mean anything to a new person. Only you and me know that it is the \"Menu\" button. Do you think that is human friendly? Try getting someone who've never used KDE+GNU/Linux (and non windows user) and see his reaction in front of your KDE. \n\nPerhaps an animated 'K' button with *sparkle* animation every few seconds could attract a newbie's attention leading to kmenu and then to other tasks."
    author: "KDEBlogUser"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-02
    body: "I read it, but I wouldn't go typing in a random program to try and do something, and I can't imagine anyone else would either. If it had been kword rather than konsole would you be making the same complaint?\nThe K icon is in exactly the same place as the start menu on windows, and there's a tooltip that says \"click here to start applications\". Perhaps some text on the button would be a bit friendlier, but it would take up a lot more space. The bottom left is a fairly obvious first place to start, it's the first of the row of buttons, and the buttons there are clearly part of the DE rather than one application. I think it's a pretty obvious first button to click."
    author: "mikeyd"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-03
    body: "1. songs - he wants to listen to songs\n-> he could also have typed this:\nmusic (wants to listen to music)\ntrack (wants to play a track)\nmp3 (wants to play mp3-files)\n\n 2. video - he wants to view some videos\n-> he could also have typed this:\nfilm (wants to see a film)\ncartoon (wants to see a cartoon)\nbambi (wants to see Disney's Bambi)\n\n 3. picture - to see some family photos\n-> he could also have typed this:\nphoto\nimage\nphotos\nimages\netc. etc..\n\n 4. internet - to browse internet.\n-> he could also have typed this:\nie\nwebsite\nwww.kde.org\ngoogle\nsurfing\n\nAnyway, it is crazy to just type random commands if you want to do something\nBetter is to add icons to your desktop with the text [picture] [internet] [songs] [films]\n\nA simple way to do that is using a file hierarchie:\nmy documents -> \n- my movies\n- my music\n- my photo's\n\netc.\n\nThat's how my mom's pc works, if she wants to listen to some music, she clicks on the button 'music', and amaroK starts :)\n"
    author: "rinse"
  - subject: "Re: Desktop Users!!!"
    date: 2005-07-05
    body: "This isn't going to work in Konsole without a major AI application.\n\nHowever, I point out that on my system if you click that house in the upper left corner of the screen, that a browser window opens that has icons for:\n\n    Music\n    Video\n    Photos\n\nAnd you already have a browser window open.\n\nIMHO, this is the way to do it.\n\n-- \nJRT\n\n\n\n"
    author: "James Richard Tyrer"
  - subject: "can anybody see full page at"
    date: 2005-07-02
    body: "kommander documentation:\nhttp://kommander.kdewebdev.org/docs/kmdr-basics.html"
    author: "KDEBlogUser"
---
Some highlights from <a href="http://commit-digest.org/?issue=jul12005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=jul12005&all">all in one page</a>):

<a href="http://kopete.kde.org/">Kopete</a> supports MSN http protocol.
<a href="http://amarok.kde.org/">amaroK</a> adds support for media:/ urls.
Speedups in <a href="http://www.koffice.org/krita/">Krita</a> and <a href="http://akregator.sourceforge.net/">aKregator</a>.
Work continues on <a href="http://quanta.kdewebdev.org/">Quanta</a> plugin for <a href="http://www.kdevelop.org/">KDevelop</a>.
<!--break-->
