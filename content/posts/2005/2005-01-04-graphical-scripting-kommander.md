---
title: "Graphical Scripting with Kommander"
date:    2005-01-04
authors:
  - "jriddell"
slug:    graphical-scripting-kommander
comments:
  - subject: "icon contest?"
    date: 2005-01-03
    body: "The article says:\n\"Finally, Kommander needs an icon, please send suggestions to kde-artists @kde.org\"\n\nwhy not create an icon contest on kde-look.org ?"
    author: "Pat"
  - subject: "Re: icon contest?"
    date: 2005-01-03
    body: "Pat .. I'm pretty sure that Jonathan is aware of those contests. But indeed a icon contest @ KDE-Look.org would be cool. Thanks to Janet and others at KDE-Look who are working on those contests btw!\n\nCiao'\n\n\nFab"
    author: "fab"
  - subject: "Re: icon contest? Divine Idea. "
    date: 2005-01-04
    body: ".. If gods play .. ... they remain on the peaks ... and - as far I am involved in the\nrhearsal what applications of other OS's run already since years( i.e. tested ) on \nKDE-overlays, I will not devote much time on things like icons. But some things\nare very important : In SourceForge-groups are too many members to particiate!\n... gods are beyond any list ... .. .   "
    author: "llllll"
  - subject: "Re: icon contest?"
    date: 2005-01-04
    body: "This March KDE-Look is planning on having the 'World Famous First Annual KDE Crystalish Icon Marathon With Prizes Awarded Challenge' (aka WFFAKCIMWPAC:). Please note: there has been some discussion about shortening the name. \n\nBasicly we are going to work all month with the artists at KDE-Look on completing of as many icons as we can. The Kommander icon will be on the list as I have discussed with Jonathan last month.\n\nTo all the other application developers out there, feel free to email me if you want to be included on the WFFAKCIMWPAC list.\n\nJanet Theobroma\n"
    author: "Janet Theobroma"
  - subject: "Re: icon contest?"
    date: 2005-01-05
    body: "WFFAKCIMWPAC:\n\nhahahahahahahaha"
    author: "Turd Ferguson"
  - subject: "How to select scripting language?"
    date: 2005-01-04
    body: "A simple question: How do I use a language other than bash with Kommander?"
    author: "Martin"
  - subject: "Re: How to select scripting language?"
    date: 2005-01-04
    body: "RTFA.\n\n\nFuture of Kommander\n\nThanks to some hard work and generous sponsorship Kommander is under heavy development. There are a lot of features planned for the near future. Most important of these is an improved parser which will allow for local variables, nesting logic and integration with scripting languages other than bash."
    author: "MacBerry"
  - subject: "Re: How to select scripting language?"
    date: 2005-01-04
    body: "OK -- it's just that every article I have read about Kommander (such as this one: http://applications.linux.com/applications/04/12/17/2033227.shtml) implies that it is script language agnostic (without telling you how to actually set the language). Since all Kommander seems to do is to substitute @ expressions and then call bash with the resulting text, why should it not be to begin with?\n\nOf course, one could always do like in the article; write to a temporary file and then execute that with any interpreter."
    author: "Martin"
  - subject: "Re: How to select scripting language?"
    date: 2005-01-04
    body: "Explained by Eric Laffon at:\n\nhttp://dot.kde.org/1087424515/1087645428/1087651634/\n\n<Rip mode>\nActually this is in the docs... Do this:\n @execBegin(python)\n import webbrowser\n webbrowser.open('@urlField')\n @execEnd\n \n Alternately you could use...\n @execBegin\n #!/usr/bin/python\n import webbrowser\n webbrowser.open('@urlField')\n @execEnd\n \n</Rip mode>\n\nHave fun with such a great piece of code Kommander is."
    author: "Ivan Lloro"
  - subject: "Re: How to select scripting language?"
    date: 2005-01-04
    body: "Cool! I thought it should be in there. I did read the help file, but in my language it was mostly empty..."
    author: "Martin"
  - subject: "1st graphics scripting software?"
    date: 2005-01-04
    body: "Just wondering, is Kommander the first software that aims to graphical scripting?\nIf yes, this is a major thing in computer desktop, right?"
    author: "blacksheep"
  - subject: "Keep it simple!"
    date: 2005-01-05
    body: "http://www.kde-apps.org/content/show.php?content=19436"
    author: "Joe"
---
<a href="http://www.kde-apps.org/content/show.php?content=12865">Kommander</a> is a powerful but easy to learn development environment. <a href="http://www.kde.me.uk/index.php?page=kommander-tutorial">"Graphical Scripting with Kommander"</a> takes us through the creation of a graphical interface for <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>, a tool for downloading, configuring and installing KDE from source packages.  The article also lists some of the exciting developments coming to Kommander in the near future. This is the second of three separate Kommander tutorials, the first of which <A href="http://dot.kde.org/1103960583/">can be found here</a>. 






<!--break-->
