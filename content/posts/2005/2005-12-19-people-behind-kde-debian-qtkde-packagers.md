---
title: "People Behind KDE: Debian Qt/KDE Packagers"
date:    2005-12-19
authors:
  - "jriddell"
slug:    people-behind-kde-debian-qtkde-packagers
comments:
  - subject: "So..."
    date: 2005-12-18
    body: "Pierre Habouzit and Josh Metzler are twins, or why do they have the same face? =D\n\nNice read. =)"
    author: "ac"
  - subject: "Re: So..."
    date: 2005-12-18
    body: "lol!"
    author: "KDE User"
  - subject: "Re: So..."
    date: 2005-12-19
    body: "fixed"
    author: "Jonathan Riddell"
  - subject: "Nice :)"
    date: 2005-12-18
    body: "Really nice interview and suprising one. As a Debian user I can't thank enough Debians QT/KDE team for their efforts. Good job. It's always nice to run dist-upgrade next day after amaroK released and find out that Debian has it already :)\n\nIf somebody had not noticed the KDE 3.5 packages are in http://pkg-kde.alioth.debian.org/kde-3.5.0 \n\nps. RMS > Linus ;)"
    author: "petteri"
  - subject: "Re: Nice :)"
    date: 2005-12-20
    body: "Already in experimental ;-)"
    author: "Michael"
  - subject: "very nice reading"
    date: 2005-12-19
    body: "it's always fun to read about kde devs's life. you get the chance to know some great people... thanks for all your work -- kde is kde because it's made of people like you!"
    author: "ac"
  - subject: "Favorite T shirt is nice and all..."
    date: 2005-12-19
    body: "But I'd like to see these interviews deal with more specific issues relative the developers in question as opposed to this generic template stuff.\nFor example I'd like to find out if the Debian KDE packages are used by submitter Jonathan Riddell in Ubuntu or what the status is of the apt:/ kio slave or adding Adept to Debian.\nThese interviews are nice, but with a little work they could be much more informative.\n"
    author: "Sander"
  - subject: "Re: Favorite T shirt is nice and all..."
    date: 2005-12-19
    body: "Yes, KUbuntu KDE packages are based on the ones created by the Debian Qt/KDE team. Jonathan Riddell adds some KUbuntu artwork and other modifications after that.\n\nI'm going to upload Adept really soon now (as soon as some debtags thing gets fixed). When KDE 3.5 enters Sid, Adept will enter too.\n\nBest regards"
    author: "Isaac Clerencia"
  - subject: "Re: Favorite T shirt is nice and all..."
    date: 2005-12-19
    body: "Interviews are meant to get to know the people behind KDE, not the technical issue that thrives them. All too much people hide behind their code, it has always been fun to get to know the more day to day stuff of the people involved in KDE. \n\nIt's also meant to lower the threshold for new developers/contributors, I always hoped that new people would discover that people participating in the project are just like you and me, so it's nice when they tell more about how they got started participating within KDE and what the challenges were they came across.\n\nInterviews are suppose to be nice, funny and on the light side, there are already more than enough technical reviews and presentations about technical issues. Interviews are suppose to be entertaining and are not limited to coders but will cover all aspects of participating within KDE.\n\nI personally enjoy every one of them and I think they're doing a great job!\n\n--\nTink"
    author: "Tink"
  - subject: "Re: Favorite T shirt is nice and all..."
    date: 2005-12-19
    body: "> Interviews are meant to get to know the people behind KDE\n\nI agree completely. The technical stuff is everywhere, and usually \nvery easy to find. \n\n/Goran J"
    author: "G\u00f6ran Jartin"
  - subject: "some weird dependencies"
    date: 2005-12-19
    body: "I'm using the KDE 3.5 Debian packages from Alioth and wonder why kdebase-dev depends on konqueror-nsplugins, ksysguard and ksysguardd ?!?!?\n\nI don't want to have some daemons running that I'm not using, and I can't see why I need to install Flash support (to distract my eyes) to Konqueror when I want to compile my own apps against KDE.\n\nBTW: because those packages are on Alioth and not (yet) in Sid I cannot file a Debian bug :-(\n"
    author: "Holger Schurig"
  - subject: "Re: some weird dependencies"
    date: 2005-12-19
    body: "To compile your own KDE applications you don't need kdebase-dev but kdelibs4-dev.\n\nYou need kdebase-dev if you want to program Kate plugins, Kicker plugins, KSysguard plugins, ... and that's the reason why kdebase-dev installs kicker, ksysguard, ... Of course, we could split the package but it takes only 73.9Kb so it's not worth independent packages.\n\nFYI, the packages have already been uploaded to experimental.\n\nBest regards"
    author: "Isaac Clerencia"
  - subject: "good job guys"
    date: 2005-12-19
    body: "I'm glad to see the team packaging is still working out great for Debian. Hopefully it will alleviate the burn out that the previous maintainers felt."
    author: "Chris Cheney"
  - subject: "What would the packagers like in KDE 4?"
    date: 2005-12-20
    body: "\"What would the packagers like in KDE 4?\"...is it just me or was this question neither asked or answered? Anyway I'm always starved for kde4 news so look forward to hearing more articles on the matter."
    author: "Tim Sutton"
---
A special treat on tonight's <a href="http://people.kde.nl">People Behind KDE</a> as we bring you the <a href="http://pkg-kde.alioth.debian.org/docs/">Debian Qt KDE Packagers</a>.  A whole seven interviews in one!  How are those packages made and kept up to date?  What would the packagers like in KDE 4?  What customisations do Debian's finest make to their own desktops?  And do they prefer RMS or Linus?  Find out on the <a href="http://people.kde.nl/debian.html">Debian Qt/KDE People Behind KDE interview</a>, the answers may not be what you think.



<!--break-->
