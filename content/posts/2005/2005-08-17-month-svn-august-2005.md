---
title: "This Month in SVN for August 2005"
date:    2005-08-17
authors:
  - "canllaith"
slug:    month-svn-august-2005
comments:
  - subject: "suhweet"
    date: 2005-08-17
    body: "That last screenshot, of the media action thingy.. Could somebody send me that color scheme? :d Thanks ;)"
    author: "Bart Verwilst"
  - subject: "Re: suhweet"
    date: 2005-08-17
    body: "Sure =) It's made by a guy called Alethes in #kde\n\nhttp://www.canllaith.org/foo/purple.kcsrc"
    author: "canllaith"
  - subject: "Re: suhweet"
    date: 2005-08-17
    body: "Hm, this doesn't seem to be exactly the same... :p Anyways, it's becoming a habit that a color scheme looks nice on a screenshot, but once you have it on your own system, it doesn't look so appealing anymore :("
    author: "Bart Verwilst"
  - subject: "Re: suhweet"
    date: 2005-08-17
    body: "> Anyways, it's becoming a habit that a color scheme looks nice on a \n> screenshot, but once you have it on your own system, it doesn't look so\n> appealing anymore :(\n\nSounds like the grass on the other side of the fence..."
    author: "Anno v. Heimburg"
  - subject: "ACID 2"
    date: 2005-08-17
    body: "The news that Konqueror now passes the ACID 2 test deserves a way bigger announcement than just being mentioned in this article. This kind of news would do well in more media and could do a lot in terms of exposure. It would also underline the improved collaboration that is happening now with Apple, an issue that has been a source for many articles and opinions in the recent past. \nHowever, when I looked on the Konqueror website, there is no mention at all of reaching this milestone. "
    author: "Andre Somers"
  - subject: "Re: ACID 2"
    date: 2005-08-17
    body: "ugh! wait till there's a stable release with it in, or you'll just wind people up."
    author: "c"
  - subject: "Re: ACID 2"
    date: 2005-08-17
    body: "Unlike all other webbrowsers you can't \"just downlaod\" konqueror and install it. You have to install the SVN version of kdelibs-3.5 (+ dependencies) and the SVN version of kdebase-3.5.\n\nUnlike Mozilla/Firefox there are no nightly tar balls of konqueror which users could install on their system. So the news that the SVN version of Konqueror supports ACID2 has no direct benefit for users."
    author: "Hans"
  - subject: "Re: ACID 2"
    date: 2005-08-17
    body: "Neither did it for Safari, but they still managed to get quite a bit of attention. I did notice though that I missed part of the buzz, as it seems I was on holiday during the first announcement in june. "
    author: "Andre Somers"
  - subject: "Re: ACID 2"
    date: 2005-08-17
    body: "Konqueror could still end up being the first browser that's released in a final version, that actually passes ACID 2 :)\n\nFirefox doesn't and AFAIK, the upcomming 1.5 release wont either and Opera hasn't released a version that passes it either.\n\nAre there any other browsers that passes it?"
    author: "Joergen Ramskov"
  - subject: "Re: ACID 2"
    date: 2005-08-17
    body: "The iCab browser does.  See http://www.webstandards.org/buzz/archive/2005_06.html\n\nNot sure if that version is already released though. \n\n"
    author: "cm"
  - subject: "Re: ACID 2"
    date: 2005-08-18
    body: "From the website: \"The development of iCab is not fully finished yet. But the first beta release of the final version is already available.\""
    author: "Joergen Ramskov"
  - subject: "Re: ACID 2"
    date: 2005-08-18
    body: "We need a kind of autopackage. I know that some do not like the current implementation but it is a must to get packages of applications run on every single distribution."
    author: "aile"
  - subject: "Re: ACID 2"
    date: 2005-08-17
    body: "There have already been quite a few announcements of the konqueror acid2 pass actually... and in all it's quite a minor achievement when put in context with the amount of work that has gone into KDE3.5 as a whole."
    author: "Ivor Hewitt"
  - subject: "Re: ACID 2"
    date: 2005-08-18
    body: "> when I looked on the Konqueror website, there is no mention at all of reaching this milestone.\n\nBut http://khtml.info has."
    author: "Anonymous"
  - subject: "Re: ACID 2"
    date: 2005-08-18
    body: "Well, Acid2 *was* working, it seems to be somewhat broken now (with a recent KHTML build)."
    author: "Bram Schoenmakers"
  - subject: "Re: ACID 2"
    date: 2005-08-18
    body: "Yeah, I was wondering about that. I'm pretty sure I have a recent svn checkout from the 3.5 branch, and it doesn't seem to pass acid2. In place of the eyes I get an embedded frame with scrollbars and an unreachable host message. Is that a khtml problem or something wrong with the acid2 page?\n\nNot that I think it's a big deal, but it'd be somewhat embarrassing to announce that it passes and release a version that doesn't pass."
    author: "Dolio"
  - subject: "Re: ACID 2"
    date: 2005-08-18
    body: "I don't assume that the Acid2 is changed in the meantime, just to screw our 'compatible' browser up ;)\n\nNo, the problem must be in KHTML. I hope it gets fixed before the release, because, as you said, it's a bit embarassing to say that Konqueror is Acid2 compliant now."
    author: "Bram Schoenmakers"
  - subject: "Re: ACID 2"
    date: 2005-08-19
    body: "How recent is this checkout? I'm looking at it here and it's just fine."
    author: "canllaith"
  - subject: "Re: ACID 2"
    date: 2005-08-19
    body: "Checkout at the 18 does show the described error. An earlier on the 14 also fails, but with a rectangular stripe insted of the eyes(the 18 has this only in place of the left eye)."
    author: "Morty"
  - subject: "Re: ACID 2"
    date: 2005-08-19
    body: "===\nbram@s040391 ~ $ ls -l /usr/kde/head/lib/libkhtml.so.4.2.0\n-rwxr-xr-x  1 bram users 45585969 Aug 15 21:07 /usr/kde/head/lib/libkhtml.so.4.2.0\n===\n\nSo, its a couple of days old."
    author: "Bram Schoenmakers"
  - subject: "Re: ACID 2"
    date: 2005-08-22
    body: "The problem isn't to do with any changes in the khtml codebase, but that the example.com domain that's supposed to return a 404 error is not responding.\n\nIf you add \"www.example.com\" to your hosts file and point it anywhere that returnds a 404 page for the request \"www.example.com/404\", then the face should render correctly.\n\nNow the question is whether the fallback should work in the same manner for an unresolvable domain as it does for a 404 error. I expect it should, but just to clarify again the problem isn't a code change in khtml, but a change in the test environment.\n\nCheers,"
    author: "Ivor Hewitt"
  - subject: "the last screenshot"
    date: 2005-08-17
    body: "I noticed in the last screenshot that is says \"Always do that for this type of media\". Wouldn't \"Always do this for this type of media\" sound better? Alhough some might complain about the \"this... this\".\n\nP.S. Thanks for your work on informing of the changes in KDE. I find your articles to be very easy to read, and they give me very good idea what is going on with KDE-developement :)."
    author: "Janne"
  - subject: "Re: the last screenshot"
    date: 2005-08-17
    body: "Also the media type is \"unmounted cdrom\".  I really hope that we can one day have the user unaware if the cdrom is mounted or not. :/"
    author: "JohnFlux"
  - subject: "Re: the last screenshot"
    date: 2005-08-19
    body: "I'm a user, and I happen to want to know whether it's mounted or not"
    author: "mabinogi"
  - subject: "(un)mounting"
    date: 2005-08-30
    body: "yeah, i'd like to be intuitive for user, and fully working! it happens (with dbus and hal) often that i'm not able to eject nor unmount the cd. killing some processes in bash solves it, but average user must be disgusted by this. in win**ws they don't have problems like this, they need only to press eject button on cdrom"
    author: "pholie"
  - subject: "Re: the last screenshot"
    date: 2005-08-17
    body: "I agree - this little string has irritated me since I first saw it :)\nI have managed to get it changed in SVN, thanks to ervin.\n\nDanny"
    author: "dannya"
  - subject: "Re: the last screenshot"
    date: 2005-08-18
    body: "Could you not change it to just read the selected action where there is now the first \"this\". So for the example in the screenshot you would get:\n\n\"For this type of medium always open in a new window.\"\n\nor if you selected the other option\n\n\"For this type of medium don't do anything.\"\n\nI think that would be more explicit about the action that gets performed [*]. I think it would look more natural (if ever there was such a thing for a computer interface). I don't know whether that is easily done though, but who knows. I'll also take this opportunity to say that I love KDE, I use it when I'm on linux and really like the fact that KDE's interface gets a rethink / some polish. Keep up the good work!\n\n[*] If KDE could find out what type of medium is getting inserted it would be even cooler if the message read: \"When a CD gets inserted always show its contents in a new window.\" Or something to that effect."
    author: "thijs"
  - subject: "Re: the last screenshot"
    date: 2005-08-18
    body: "No, this isn't possible.\n\nYou can't connect sentences like that. It works in English, but you can be sure there will be at least one language where the compounded sentence doesn't make sense or is syntactically incorrect with the action name.\n\nThe other option would be to have the translators translate every possible sentence. There would be one leftover: the customised actions, created by the users and by the installed programs."
    author: "Thiago Macieira"
  - subject: "Re: the last screenshot"
    date: 2005-08-18
    body: "I was afraid of something like that. Would translating all these actions be a heck of lot more work? Probably, interesting problem though. Thanks for the reply :)"
    author: "thijs"
  - subject: "Re: the last screenshot"
    date: 2005-08-18
    body: "Wouldn't it only be a matter of translating \"Only perform action X when a medium of type '%1' is inserted\" along with every possible action? (There can't be that many actions that can be used in this dialog, or am I mistaken?)"
    author: "roiem"
  - subject: "Re: the last screenshot"
    date: 2005-08-18
    body: "If you have for example 10 actions and 10 types, you have already 100 strings to translate!\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Media notification"
    date: 2005-08-17
    body: "A really good edition, Jess, thanks :)\n\nCan anyone explain how configurable the media notification thingummy is, though? I'm sure a lot of people would be pleased if there was some form of notification less obtrusive than the WinXP-style dialogue shown in \"This month in SVN\". I'd favour system tray icon with bubbles that allow you to perform the actions described by the dialogue... it gives you all the functionality but doesn't get in your way (so long as is quietly fades away)."
    author: "Tom"
  - subject: "Re: Media notification"
    date: 2005-08-17
    body: "Yeah, that irks me as well. I ended up just going into kcontrol and manually setting 'do nothing' for every single medium type. Hopefully won't be any more added later on...\n(still get an icon shown on the desktop)"
    author: "Illissius"
  - subject: "Re: Media notification"
    date: 2005-08-17
    body: "You can turn off that icon from the Control Center.\nSimply right click on the desktop, choose Configure Desktop, go to the Behaviour section, and in the Device Icons tab, remove the check above the list (or modify for which devices you want an icon displayed)."
    author: "Andre Somers"
  - subject: "Re: Media notification"
    date: 2005-08-17
    body: "You misunderstand :)\nIcon on the desktop is a good thing."
    author: "Illissius"
  - subject: "Re: Media notification"
    date: 2005-08-17
    body: "You're right, if there is something I hate about windows, it's that useless dialog that popups when an DVD is layed in, or a external-harddisk is connected. Is one of the reasons I disabled the autoplay option under windows.\n\nIf KDE is doing the same shit, than I hope there will be an option to disable it."
    author: "Egon"
  - subject: "Re: Media notification"
    date: 2005-08-17
    body: "We're dealing with linux here. If there ain't no way of disabling/enabling things, the stuff wouldn't belong here, right ? And, on top of that, this is kde we're talking about :] :thumbsup:"
    author: "Levente"
  - subject: "Re: Media notification"
    date: 2005-08-18
    body: "Egon: \"You're right, if there is something I hate about windows, it's that useless dialog that popups when an DVD is layed in, or a external-harddisk is connected.\"\n\nUnfortunately, Windows users think different, and they don't find that dialog \"useless\", as you say. My friend said that he doesn't put DVD's in his diskdrive just for nothing, he wants to watch them. The dialog offers him that opportunity. He finds such a dialog handy. And probably millions of people think the same.\n\nPersonally, I totally agree with you. I'm not among those \"millions of people\". :)"
    author: "Bram Schoenmakers"
  - subject: "Re: Media notification"
    date: 2005-08-17
    body: "Amen to that. A (highly configurable) bubble would be much less obtrusive, following the principles of APPEAL. I certainly won't be using the ugly huge XP-like dialog..."
    author: "LuckySandal"
  - subject: "Re: Media notification"
    date: 2005-08-18
    body: "You mean similar to this one:\nhttp://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,315.0\n\nI really hope that one gets integrated (may need some more polish, but I like it). :-)"
    author: "Willie Sippel"
  - subject: "Re: Media notification"
    date: 2005-08-18
    body: "I guess how could people get used to these system tray \"bubbles\", they are only a windows placebo for notifications. We have applets in kicker, they are more comfortable, usable and whatever and still people complains about an icon in the system tray that pops up a baloon. I don't understand why...\n\n"
    author: "Davide Ferrari"
  - subject: "Tnx again"
    date: 2005-08-17
    body: "Tnx again Jess! You did a really good job. :)\n\nSystem Kioslave looks very promising. It is something I always missed in non-windows desktops. And finally, the icons-rearranging-bug is fixed. That asks for a party. ;)"
    author: "mOrPhie"
  - subject: "Let's have a party!"
    date: 2005-08-17
    body: "\"One of the most exciting things to happen in desktop & panel development this month is the fixing of a long standing and notorious bug. Bug 91575, 'Icons get rearranged on login' is the subject of one of the most frequently asked questions on mailing lists and IRC channels. This bug was caused by lack of proper communication between Kicker & Kdesktop and was recently fixed in a concerted effort between Aaron Seigo and Martin Koller. For everyone who has had their desktop icons rearranged on logging into KDE or on hiding and showing panels, this is finally fixed for 3.5!\"\n\n:) :-) :)\n\nThank you!\n\nBTW: adding Applets also looks sweet: http://www.canllaith.org/svn-features/images/test.gif\n\n"
    author: "Alex"
  - subject: "media manager component "
    date: 2005-08-18
    body: "I won't work with supermounted, that's a bug I presume. if I remove supermount in /etc/fstab (old cdrom mounting) then Media Detection works.\n\nI love supermount and I want media manager support that too. Has anyone got Media Detection work with CDROM mounted with supermount?"
    author: "fast_rizwaan"
  - subject: "Re: media manager component "
    date: 2005-08-18
    body: "Supermount is crap. Pass to a project Utopia (udev+hal+dbus) solution if you want automatic (well, with KDE currently is semi-automatic) mounting of removable medias. Or use your distribution default."
    author: "Davide Ferrari"
  - subject: "Supermount is good!"
    date: 2005-08-18
    body: "for you it may be crap, but I love supermount, it does what it is supposed to do - seamless CDROM access. Never had a problem.\n\nI really hate to unmount, cause konqueror locks cdrom!\n\nI have udev+hal+dbus installed and I am not satisfied with them! I still need to unmount my cdrom by Right-Clicking and really annoying thing is to find out where a konsole or konqueror is locked with /mnt/cdrom!\n\ncould you suggest a way to automatic mounting and especially *unmounting* like supermount with udev+hal+dbus or something. so that I could use my computer, not fight with it to get my work done.\n\n"
    author: "Fast_Rizwaan"
  - subject: "Re: Supermount is good!"
    date: 2005-08-19
    body: "supermount is superior in what it does best - esp for cdrom's its usefull. but it simply would never end up in the kernel, as the kernel developers don't want a kernel-space tool for something userspace can do (hal and dbus can just be ru - installed and upgraded, while supermount has to be in the kernel). so it is better to improve dbus and hal, as these are kind'of long term solutions, instead of spending time on supermount...."
    author: "superstoned"
  - subject: "Re: media manager component "
    date: 2005-08-18
    body: "You will have to abandon supermount like I have. I think Con Kolivas supported it, but now nobody no longer supports it since the 2.6.11 kernel. And if he can't, I can tell you not a lot of people will be able to. Worse thing being that everyone thinks it is crap.\nWell, project Utopia works pretty well, except that udev changes often, and now does not support devfs anymore, so I will have to make some changes to my system again to go back to the old Linux way (even though I used devfs for years)."
    author: "Ookaze"
  - subject: "Re: media manager component "
    date: 2005-08-22
    body: "I recently dumped supermount and switched to ivman (with udev and hal).  Does exactly what supermount did.  Mounts CDs automatically when I put them in, and I can press the eject button whenever I want.  The only apparent difference is that the drives now work with KDE's media:/ and that's hardly a negative :)"
    author: "MamiyaOtaru"
  - subject: "Supermount replacement"
    date: 2006-04-05
    body: "instead of using supermount we can work with autofs/automount. today i've got autofs working in slackware.\n\nyou'd need to create \"mkdir -p /mnt/.auto/cdrom /mnt/.auto/dvd\"\n\nand copy the auto.* to /etc as \"cp auto* /etc\"\n\nand copy the \"cp rc.autofs /etc/rc.d/rc.autofs\"\n\nalso see:\nhttp://gentoo-wiki.com/HOWTO_Auto_mount_filesystems_(AUTOFS)#Config_files\nhttp://www.linuxquestions.org/questions/showthread.php?postid=1149234\n\nnow who needs hal+dbus for cdroms or floppies?"
    author: "fast_rizwaan"
  - subject: "mozilla kpart plugin"
    date: 2005-08-18
    body: "Back in June there was a story about a firefox plugin http://dot.kde.org/1119705193/ that would allow firefox to embed kparts.  I looked through websvn last night but could not find it.... Anyone know where I might find it...\n\n\nThanks\n        Ruairi"
    author: "Ruairi Hickey"
  - subject: "Re: mozilla kpart plugin"
    date: 2005-08-18
    body: "playground/utils/dragonegg"
    author: "Anonymous"
  - subject: "KViewShell"
    date: 2005-08-18
    body: "Why\n\nKViewShell\nKPdf\nKghostview\n\n? \n\nWhy not one single tool?\n\nOne problem is that you have to get rid of too much similar tools.\n\nKedit\nKate \nkwrite\n\nwas a longterm example. This is bad usability. "
    author: "gerd"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "Most applications use katepart internally (kate, kdevelop, quanta and so on). So the code is shared. In KDE 4 there will only be one editor around.\nThe other apps share or will share code in the near future. There is even a SOC-project for this."
    author: "anon"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "Hmmm, but both KViewShell and oKular (the Google SoC project) seem to be thriving.  Are you sure there will only be one unified document viewer?  I think it would be a good thing but I'm not convinced it will happen..."
    author: "cm"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "IIRC, Kedit is only kept because katepart has some problems with BiDi support. Kedit will be removed once katepart handles BiDi to the same degree. Kwrite and Kate are pretty much the same, both use katepart, Kate only adds some advanced tools and functions for coders and the like, while Kwrite uses a basic interface that should be less distracting for the user. I think this is, in fact, good usability, and even I use both flavors (Kwrite for single files, Kate for C++ stuff). A single application with a first-start dialog to set the default behavior (Kate or Kwrite) might be OK, if there was also a command line switch to set the behavior (to use Kwrite as the default, and add a menu item to start it in Kate mode)."
    author: "Willie Sippel"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "The problem is: bidi is only needed for bidi languages. so i understand when hebrew kde users get it installed. but I do not need it.\n\nthe story that kedit will get removed is old. too old."
    author: "gerd"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "If you don't need bidi and KEdit, why do you install it then? Nearly all modern distributions install it as a separate package, so you don't have to have it installed if you don't want to. And if you install from source it's even less excuses, why waste CPU cycles on compiling it if you don't want it installed.\n\nAnd the story never gets too old as it is simple truth. When katepart supports bidi, KEdit will be removed. Since it does not do that presently KEdit stays."
    author: "Morty"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "The problem is that nobody is working on getting BiDi-support, so KEdit will stay forever ..."
    author: "Dominik Huber"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "BiDi implementation is only delayed for KDE 4 because it's much easier to implement with Qt 4."
    author: "Anonymous"
  - subject: "Re: KViewShell"
    date: 2005-08-19
    body: "The experience with KWord has shown that using Qt3's Bidi code for making something else is very difficult, i.e. you have to re-implemt everything. (KOffice has chosen the way to duplicate Qt's code.)\n\nQt4 was modified from this experience, to allow such uses.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KViewShell"
    date: 2005-08-21
    body: "Great, thank you!\n\nHave a nice day too :-)"
    author: "Dominik Huber"
  - subject: "Re: KViewShell"
    date: 2005-08-19
    body: "If you don't need it, don't install it. Very simple! No-one forces you to install every extragear and toy and utils package ;)"
    author: "canllaith"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "KEdit is the only Bidi-aware editor of KDE and will be kept as long as Kate is not Bidi-aware.\n\nAs for KWrite, it is not more than a simple skin for Kate.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "kghostview will probably go away as soon as something else can handle postscript files. And then there's okular which aims to unify kviewshell and kpdf:\n\nhttp://developer.kde.org/summerofcode/okular.html\n\nAnd, as other people have said, kedit is around for bidi text, so it will be gone when the kate part can do that. Kate and kwrite are mostly the same code, although the kate interface is half way between a text editor and an IDE, so how anyone could think it's exactly the same as kwrite is beyond me.\n\nIn other words, pretty much everything you mentioned is being worked on. I'm sure the KDE developers would appreciate it if you wanted to chip in to make it happen faster."
    author: "Dolio"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "It was said kedit will go away years ago. Can we expect kedit to get dropped at KDE 4? At least I do not need biDi "
    author: "Bertram"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "If Kate(katepart) supports bidi KEdit will go away in KDE 4, if not it will stay. But if you don't need bidi you are actually not forced to install KEdit, so I can't say why it should bother you anyway."
    author: "Morty"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "Bertram: \"At least I do not need biDi\"\n\nThat's not an argument for dropping KEdit. But yes, I expect KEdit to be dropped at KDE 4."
    author: "Bram Schoenmakers"
  - subject: "Re: KViewShell"
    date: 2005-08-19
    body: "I could be mistaken, but I believe that kedit is the editor widget used in kmail's composer. If so this is probably another reason it's survived the whole 3 series."
    author: "canllaith"
  - subject: "Re: KViewShell"
    date: 2005-08-19
    body: "Don't mix KEdit, the application, with KEdit, the class."
    author: "Anonymous"
  - subject: "Re: KViewShell"
    date: 2005-08-18
    body: "> And then there's okular which aims to unify kviewshell and kpdf:\n> http://developer.kde.org/summerofcode/okular.html\n\nAh, I guess I got that wrong some posts further up then...\n\n"
    author: "cm"
  - subject: "Spanish translation in KDE-Hispano"
    date: 2005-08-20
    body: "Finally, we have our own translation of \"This month in SVN\" series, you can found every month in KDE-Hispano. \n\nThis month translation is now avaliable http://www.kdehispano.org/svn-agosto-2005"
    author: "melenas"
  - subject: "Re: Spanish translation in KDE-Hispano"
    date: 2005-08-22
    body: "It's really great to hear that another team has picked up the translation of these excellent articles. Keep up the good work!."
    author: "Bram Schoenmakers"
---
More KDE development news in the <a href="http://www.canllaith.org/svn-features/17-08-05.html">August edition of This Month in SVN</a>. This issue packs in twice as much content as the previous one, with new features covered in <a href="http://konqueror.org/">Konqueror</a>, Kicker, KDesktop, <a href="http://amarok.kde.org/">amaroK</a>, <a href="http://konversation.kde.org/">Konversation</a> and more: <i>"This month has seen some drastic changes in SVN, with KDE4 development moved to trunk and KDE 3.5 gearing up for a stable release sometime after this year's KDE conference."</i>


<!--break-->
