---
title: "KOffice 1.4 Released"
date:    2005-06-22
authors:
  - "rlangerhorst"
slug:    koffice-14-released
comments:
  - subject: "Kubuntu Packages and Live CD"
    date: 2005-06-21
    body: "Kubuntu's packages are on download.kde.org, we also have a Live CD with KOffice 1.4 (and KDE 3.4.1).\n\nhttp://kubuntu.org/hoary-koffice-14.php\n"
    author: "Jonathan Riddell"
  - subject: "Thank you!"
    date: 2005-06-21
    body: "Donwloading now....\n\nGreat job everybody! go KDE! ;)"
    author: "Anonymous"
  - subject: "OpenDocument"
    date: 2005-06-21
    body: "Are these now the default file formats for KOffice? How much of the standard is supported? If I remember correctly, there need to be two independ implementations of the OASIS standard before it becomes real. OOo provides ones and KOffice the other. So, is the standard in effect now?"
    author: "Anonymous"
  - subject: "Re: OpenDocument"
    date: 2005-06-21
    body: "> Are these now the default file formats for KOffice?\n\nNo, and they also don't cover all applications contained within KOffice.\n\n> So, is the standard in effect now?\n\nOpenOffice.org 2.0 is not yet released."
    author: "Anonymous"
  - subject: "So.."
    date: 2005-06-21
    body: "How's Krita? Is it any good?"
    author: "bsander"
  - subject: "Re: So.."
    date: 2005-06-21
    body: "I saw a video of it somewhere, I was very impressed!  I don't remember where they released the video but it was cool."
    author: "ac"
  - subject: "Re: So.."
    date: 2005-06-22
    body: "I made a couple of videos for krita. Two were made with the old preview release (probably the one you saw), but I made a new one for the current 1.4 release:\nhttp://www.bartcoppens.be/krita-rc1.avi"
    author: "Bart Coppens"
  - subject: "Re: So.."
    date: 2005-06-21
    body: "It's excellent of course! The proverbial bee's knees, the cat's nip, the ewe's lamb, when bought in the shop it would cost you a fortune, but we can offer you a download for the incredible price, and it's cuttin' me own throat, guv', the incredible price of, listen carefully and don't faint, zero gold dubloons. Get yer fresh software here, get yer fresh software here! Fresh and free! Free and fresh!\n\nEr... Sorry, got a little carried away there. Won't happen again! Promise.\n\nKrita is a young application (it may have been started six or so years ago, but that doesn't mean it's been in continuous development for all that time) and you will certainly find it lacking in features compared to Photoshop. But I think we've got KPaint licked, here, and there are compelling reasons to use it over and above XPaint. And since svn was branched, we've started adding new stuff again, so it can only get better."
    author: "Boudewijn Rempt"
  - subject: "Re: So.."
    date: 2005-06-21
    body: "Kolourpaint was the first usuable program. Krita now is usable too and I expect it now to grow very fast."
    author: "Gerd"
  - subject: "Re: So.."
    date: 2005-06-22
    body: "Hello Boudewijn,\n\nGreat work on Krita! Looks like you guys did an AMAZING job with it. At this rate, we will at last have a professional-grade painting program on Linux in but a few releases!\n\nI had a small question though -- couldn't find anything about it on the site: Does Krita support Kipi plugins? Will it? SHOULD it?\n\nThanks again for you excellent work!"
    author: "A.C."
  - subject: "Re: So.."
    date: 2005-06-22
    body: "No, we don't support Kipi, at the moment at least. Kipi didn't seem terribily relevant as the plugins are mostly concerned with organizing images and take 8-bit rgb QImages as their choice of image format. I'd be very much interesting in integration with Digikam -- e.g., using Digikam as the organizer and Krita as the image editor with full sharing of plugins. But since Gilles and Renchi are developing their own image editor (working on 16bits, too, I have heard) that seems unlikely. And while I have started working on a Krita plugin that loads all digikam plugins, I'm afraid that that would be a dead end -- again, because Krita supports a potentially infinite variety of image formats, and the digikam plugins presuppose 8-bit rgba."
    author: "Boudewijn"
  - subject: "Re: So.."
    date: 2005-06-22
    body: "Okay, I suspected something like this. Anyway, thank you for the clear answer, and thank you again for all the good work!"
    author: "A.C."
  - subject: "It's cool!"
    date: 2005-06-21
    body: "Thanks to Gentoo, instantly providing the new KOffice packages, I just tried out Krita. Resum\u00e9: while still suffering from minor usability problems, it's capable and cool enough to make me (as occasional user, no graphics pro) drop the Gimp.\n\nPros, at the first glance:\n- Finally an advanced bitmap editor fitting in only one window\n- Consistent and comfortable use of brushes everywhere\n- Good color selection, I think that should be ported to the main KDE dialog\n- The docking tool windows work perfectly\n- It just feels right, don't know\n\nIssues that I stumbled over:\n- Didn't find the command to zoom back to 1:1, neither in the View menu nor in the General/Tools dock, where an appropriate tab is also missing (you know, containing various zoom choices and stuff).\n- Selections are not quite as intuitive as in Paintshop Pro, but seem to have their strenghs too. How about deselecting with the RMB?\n- I'd feel better if new text would be created as selection, not as new layer.\n- The Rotate Layer submenu contains direct rotation entries (90\u00b0/180\u00b0, which I like) whereas the Image menu just contains the Rotate Image dialog. I'd propose using the submenu approach also in the Image menu and renaming the Rotate Image/Layer dialog to Custom Rotate.\n- Why can the crop rectangle only be dragged at the grip points, but not from inside of the existing rectangle?\n- Shapes can't be filled with gradients, can they?\n- The Color Manager's current color display is too small.\n\nThat sounds like a lot of problems, but I think they are not too serious. Well, the first one, maybe. But apart from that, it seems I'll be quite happy with Krita, because the devs did many things right.\n\nSo, great work Boudewijn & Co.!"
    author: "Jakob Petsovits"
  - subject: "Re: It's cool!"
    date: 2005-06-21
    body: "Okay, good points. Let's take them one by one:\n\n* Zooming. I just didn't manage to finish the bird's eyeview panel in time, I'm afraid, and I never thought about adding a menu option to go back to 1:1 zoom.\n* Deselecting with rmb: that's a good idea... Could you add that to bugzilla? That way it's less likely to be forgotten by us.\n* Text: I won't do much work on text layers for now. I'm playing with the idea to have the other KOffice components do that work, by inserting their objects as layers.\n* Rotate: see deselect...\n* Crop rectangle: Because we didn't manage to code that in time...\n* That's probably an oversight -- I cannot run Krita at this moment because an injudicious hack is giving me trouble and I need to recompile, but if there's no gradient option in the shape tool option widget combobox, then it should be added... Workaround: use the shape selection tools to select the right shape and fill that with the gradient.\n* I'm not sure about the last point, I've never had a problem with seeing what the current color would be because I look at the selection widget, not the color button in the control frame.\n\nAnyway, I've added some points to the TODO, but if you want to track progress, please take the time to add them to bugzilla, too."
    author: "Boudewijn Rempt"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: "Koffice components as layers?  That would be insane.  So I could have a bitmap, with a vector layer from Karbon14, and a text layer from Kword?  Now that would be a killer feature.\n\nWhile I am dreaming, it would be even cooler if there were fx layers I could apply to these layers so that I could have a something like a drop shadow or transformation dynamically applied to my Kword layer as I updated it."
    author: "the orz"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: "It may be a dream, but not an impossible one. I fact I don't think it would be too long before we start working on it. The whole layer thing: adjustment layers, grouping of layers, and layers from other KOffice applications is all on our todo. Currently though, all of us developers are deep into other features, but with the rate we are progressing right now...who knows.\n\nBut if you like krita just a little bit today - you just wait - you ain't seen nothing yet. LOTS of cool things and behind the scene improvements are being done or are planned."
    author: "Casper Boemann"
  - subject: "Re: It's cool!"
    date: 2005-06-21
    body: "> Finally an advanced bitmap editor fitting in only one window\n\ni consider this a big fat MINUS. i love to have all image tools and dialogs open on the left monitor and the image full-screen without border on the right. you just cant do that with a everything-stuffed-in-one-window approach. so gimp is still the way to go for me, although krita looks smth more ... kawai"
    author: "Jens Wiedemar"
  - subject: "Re: It's cool!"
    date: 2005-06-21
    body: "You can detach all toolbars and dockers from the main window and move them to your second screen; however, I have this nasty suspicion that I didn't take care to ensure that if you open a second view window, you won't get all the dockers and toolbars again... But as long as you work with one image in one view, it'll work exactly like want."
    author: "Boudewijn Rempt"
  - subject: "Re: It's cool!"
    date: 2005-06-21
    body: "Remember, this is KDE. Of course you have the option, just drag the toolwindows where you want them. You better give Krita a try:-)"
    author: "Morty"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: "I don't like the one-window solution either. The tool panels take a lot of screen space away.\n\nDetaching the tool panels isn't a solution either. Having a lot of seperate tool windows floating around is a nightmare. I spend too much time dragging them out of my way.\n\nI would like to have one single master toolwindow, one that you can add as few/many tools to as you like, in the form of tabs for instance. The seperate tool windows should stay as well, because some users seem to like them and they allow a highly customizable single-window layout. Adding a master tool-container window would make Krita attractive to GIMP-UI fans as well."
    author: "Meneer Dik"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: ">I would like to have one single master toolwindow, one that you can add as few/many tools to as you like, in the form of tabs for instance.\n\nI seccond that :-) Except i think it should be more like the kind of \"tabs\" you see in Konq or Amarok out on the left. Having just *one* narrow and tall window at the left, with one-click acces groups of tools, layers, colour chooser, plugins/script extentions, coffee machine etc, would realy make my day. It saves screen real estate, and would work in both \"crammed\" and \"exploded\" mode :-)\n\nI'll go work on making a mock-up right away!\n\n~macavity"
    author: "Macavity"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: "I'd like to see your mockup but I have to warn you... I have started coding a system of dockers where the user can drag & drop tabs between dockers, and where there are two kinds of tabs: the toolbox kind (as in Qt Designer, hope that class doesn't get axed for Qt 4) and the tab kind. So, if you want all the palettes in one big panel, you just drag all of 'em to a toolbox pannel, and bob's your uncle\n\nCuriously enough, the all palettes in one big docker was the approach of Krita when I started hacking the semi-abandoned codebase, and I found it didn't work for me. We've had a lot of grief with developing the dockers. Qt's QDockWindow's have some nasty bugs, Kivio's sliding dockers have other bugs and a home-grown solution has the disadvantage that maintaining it takes precious time from real development. I notice that Bibble Pro (a really great image editor) uses Qt and has the same problems with their dockers as Krita has.\n\nAnyway, about four floating palettes seems to be what Photoshop, Paintshop Pro, Corel Painter, Acrylic and Procreate Painter have standardized on, so I decided to stop fighting Qt, and just go with the flow."
    author: "Boudewijn"
  - subject: "Re: It's cool!"
    date: 2005-06-23
    body: ">Anyway, about four floating palettes seems to be what Photoshop, Paintshop Pro, Corel Painter, Acrylic and Procreate Painter have standardized on, so I decided to stop fighting Qt, and just go with the flow.\n\n Yes.. I've been surfing screenshots of that too. So far it looks just like that, except I keep all four in one window, with konq-sidebar-tabs (\"dockers?\") on the left.\n\n What I'm doing right now, is that making some dummy-work and then try and imagine (clicking on the drawings of the UI) what I would have to do to get the same job done using my approach. However it became clear quite fast that this approach translates to [a lot] more mouse clicks in exchange for the extra screen space. I'm no coder myself [yet], so I don't know if Qt/KDE allows a window in focus to \"send\" the keyboard shortcuts it detects to another window.. but if this is the case it would be truely KDE-like-kick-arse to just bind the \"side-tabs\" to [1], [2], [3] & [4] (except in text-layer-mode), to allow for maximum-screen-space-dual-hand-krita-hyper-productivity-operation ;-)\n\n>but I have to warn you... I have started coding a system of dockers where the user can drag & drop tabs between dockers, and where there are two kinds of tabs: the toolbox kind (as in Qt Designer, hope that class doesn't get axed for Qt 4) and the tab kind.\n\n Again.. I'm no hacker but this doesnt sound as if it contradicts with my idea (is it \"generic\", \"selfcontained\" and \"re-usable\"?). In fact it sounds like this kind of work is axactly what is needed to make it possible to extend and re-configure my idea to fit everyone. It would be truely awesome if Krita is to be the first 100% custamizable IMP.. as every one seem to have their own take on how theese programs should look. If it too was all D'N'D, with 3-click profile management.. well.. that would be kind of devastating to any competitior :-) (If this was on side-tab 5 it could be done in 2 clicks)\n\n I will post on looky sometime next week.\n\n Happy Midsummer everyone :-)\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: It's cool!"
    date: 2005-06-23
    body: "WOW! Amarok-SVN has *exactly* the kind of side-tabs im thinking of! In fact the entire left side of Amarok is pretty much like what I had in mind.. that will make the production of my mock-ups much easyer :-)\n\n~Macavity"
    author: "macavity"
  - subject: "Re: It's cool!"
    date: 2005-06-23
    body: "Don't forget to mail me -- I might miss it otherwise."
    author: "Boudewijn Rempt"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: ">- Finally an advanced bitmap editor fitting in only one window\n\nI do not understand why this can be listed as a \"Pro\". It is just a personal preference of \"look\", like the colour of the window titlebar.\n\nCould you elaborate on what it brings (apart from the look) compared to recent gimp ?\n\nJust as a reminder for people who haven't used gimp since 1.x, recent Gimp can make use of \"transient\" windows (it is an option in the preferences). Thus you have different windows on the screen, but you can minimise them all by minimising the image window. The whole point on \"one window to rule them all\" is thus void.\n\n(and decent taskbars such as KDE one can group all gimp windows in one too)"
    author: "oliv"
  - subject: "Re: It's cool!"
    date: 2005-06-22
    body: "> I do not understand why this can be listed as a \"Pro\".\n> It is just a personal preference of \"look\", like the\n> colour of the window titlebar.\n\n1. I know there are many people who prefer a single window for a single document. And unlike the Gimp, Krita lets you choose how you want it, so how could choice be not listed as \"Pro\" (especially if it defaults to something that I personally like)?\n\n> (and decent taskbars such as KDE one can group all gimp windows in one too)\n\n2. The taskbar handling is still not as comfortable as the \"real thing\". It requires at least one additional click, and when the list of grouped windows appears, I still have to grasp which of them I want. If there are few other windows around, chances are that the window buttons are not grouped and clutter the taskbar. And I can't stand it switching to another desktop just to keep my taskbar clean.\n\nOf course it's possible, learnable, has its good points and whatever, but I instinctively feel better with a single window, that alone should be enough to favour it :-)"
    author: "Jakob Petsovits"
  - subject: "s/ImageMagic//g"
    date: 2005-06-21
    body: "Please if possible remove ImageMagick dependency from Krita."
    author: "ac"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-21
    body: "Why? Is there something unclean about ImageMagick? Of course, if you are content with loading and saving nothing but native files, you can use Krita without ImageMagick already -- but I lack the resources to create import/export filters for all the formats that ImageMagick supports, and Krita needs more than kimgio can deliver."
    author: "Boudewijn Rempt"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-21
    body: "Agree.  I don't see the point of removing the dependency on ImageMagick.  ImageMagick is ubiquitous and does just about every image format conversion you can think of."
    author: "other ac"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-22
    body: "I think the proper solution would be to have a image framework for KDE or something like that."
    author: "ac"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-22
    body: "Why reinvent the wheel?\nImageMagick works. Just do it the unix-way instead of reinventing the wheel all the time."
    author: "Asdex"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-22
    body: "> Just do it the unix-way instead of reinventing the wheel all the time.\n\nOr may this old-skool quote:\n\n\"Those who do not understand Unix are condemned to reinvent it, poorly.\" -- Henry Spencer, University of Toronto\n\n\n\nCies Breijs"
    author: "cies breijs"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-22
    body: "I think both quotes are quite silly. If we argue why re-inventing the wheel then by now we all would still be using CDE. There is a problem we need to solve properly and this means a sane graphic layer library doing these kind of work. Right now we have different KDE applications using different versions of graphic libraries for doing exactly the same task. Some use libimlib, some use stuff from imagemagic, some other things use some other libraries and and and. We could get rid of a lot of dependency on 3rd party libraries and tools if there was a proper graphic library or something. All KDE apps that require such a library can use the one provided by KDE and not from any random 3rd party application. It's easier for bugtracking and easier to maintain too."
    author: "ac"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-22
    body: "You know what I think KDE needs to standardize for all of us?\n\nDatatypes.  Small generic libraries dealing with specific media formats.  Datatype-aware programs become forward compatible; compile a library for a new image type, and all programs dealing with images know how to use that file.  Amiga OS 3.x did it, OS/2 Warp did it... what are we waiting for?  I've been wanting datatypes for audio, 3D objects, image formats and more for a while!  They show these MIME associations that Windows 95 somehow lowered our expectations to for the crude, inflexible constructs that they are.\n\nAnd if a datatype has to call ImageMagick, big deal.  It'd be a minimal chunk of code for the purists to revise later."
    author: "D. Rollings"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-22
    body: "At least as standardized wrappers (in cases of ImageMagick and Xinelib etc.) this would be really a nice idea, possibly this could be done for KDE4. Did you suggest that at some central place already?"
    author: "ac"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-27
    body: "I have as of now.  It looks as though the kfile and kimgio libraries certainly fill in the gap for images with a plugin architecture; at this point, I have a general interest in seeing whether this approach extends to other formats."
    author: "D. Rollings"
  - subject: "Re: s/ImageMagic//g"
    date: 2005-06-28
    body: "between gdkpixbuf and gstreamer gnome has effectively done this for images and video but it was largely incidental and unfortunately not part of some greater plan.  no reason why kde couldn't do one better.  "
    author: "Anonymous Gnome"
  - subject: "Nice to see the Mandriva package."
    date: 2005-06-21
    body: "I have to admit that I have been wanting to upgrade to kde 3.4.1, but no luck so far. Just out of curiousity, how is Suse looking these days?"
    author: "a.c."
  - subject: "Re: Nice to see the Mandriva package."
    date: 2005-06-22
    body: "Free version looks like desktop agnostic, highly advanced alpha version of\nsomething great yet to come."
    author: "Anonymous"
  - subject: "Re: Nice to see the Mandriva package."
    date: 2005-06-22
    body: "A KOffice 1.4 rpm for SUSE 9.3 is on ftp.kde.org."
    author: "Anonymous"
  - subject: "Re: Nice to see the Mandriva package."
    date: 2005-06-22
    body: "Hopefully, the merger with conectiva will bring up to date packages for \nmandriva in the future. Conectiva has been offering the latest KDE rpms\nwith each KDE release. \n\nThe alternative I am considering is to potentially switck to kubuntu.\nI tried the live CD and it is very, very nice. But I am still sticking\nwith Mandriva, at least for a while. It just works for me. The only complain\nbeing that they decided to be always a few months behind. Of course, the \ngood thing this brings is more stability. \n\nI always thought they should keep the free (beer) version up to date,\nand release the commercial version later, after massive testing. Oh well.\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Nice to see the Mandriva package."
    date: 2005-06-22
    body: "> I always thought they should keep the free (beer) version up to date,\n> and release the commercial version later, after massive testing. Oh well.\n \nYeah, I am looking at switching just due to that. It used to be that Mandrake did a great job of staying up on KDE (a bit flakey though), but now they are 6 months to a year out of date.\n\nI do like your idea.\n"
    author: "a.c."
  - subject: "Krita still sucks..."
    date: 2005-06-21
    body: "lots of usability issues plus you need a resolution bigger than the mostly standar 1024*768.\n\n"
    author: "TIM.."
  - subject: "Re: Krita still sucks..."
    date: 2005-06-21
    body: "Go back to your GIMP already."
    author: "ac"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-21
    body: "At least I an use it even with a resolution of 800*600"
    author: "TIM.."
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "What about 320*200?  ...didn't think so.  Gimp sucks!"
    author: "other ac"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Then why are you still complaining?? Bored??"
    author: "ac"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Well, well... That was very, er... helpful.\n\nHowever, maybe I can help you... If you add the line \"dockerStyle=0\" to the General section in your .kde/share/config/kritarc, you will find that this version of Krita uses the same sliding dockers as Kivio. Place them against any border you want, resize or wriggle them a little (that's needed, it's the bug that forced me not to include the option in the gui), and they disappear only to re-appear on mouse-over. That way, you don't need to buy a bigger screen. \n\nOr you could just close the dock windows you don't need -- or roll them up using the conveniently placed button."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Go here: http://openusability.org/projects/krita/"
    author: "ac"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "True that KRITA is not usable (workable) at 800x600 resolution. Hey guys try krandr and switch to 800x600 resolution and try working with krita! I'll bet that you won't find it a good experience.\n\nDue to my monitor which can't have high refresh rate like 85 Hz at 1024x768, which is only available at 800x600 resolution and my monitor can have 60Hz at 1024x768 which causes much eye-strain and sore-eyes. I am stuck at 800x600 resolution.\n\nYou guys have to consider 800x600 resolution for all kde applications, because 800x600 *is the supported KDE resolution*."
    author: "fast_rizwaan"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "As I said, if you close the dockers, hide them push them down the bottom of the screen, roll them up or use the kivio-style dockers you'll have as much space available as possible for any application. You can disable the rulers, remove most of the toolbars -- that only leaves the window decorations and the tools toolbars.\n\nBut in the end, 800x600 is never going to be good enough to work with images, whether you use the Gimp, Kolourpaint or XPaint. Most _images_ are bigger than that."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "I use the Kivio-style dockers myself and they make Krita quite usable on my 1024x768 screen. However, if I may request that their positions be remembered, instead of having to rearrange them from the top left corner each time... ;)"
    author: "Illissius"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Working on that... The first beginning of that code is already in svn."
    author: "Boudewijn"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "thanks"
    author: "Illissius"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Useless, because Tool Dialogs always stay on top, and on your way, this is terrible, I don't want to deal with tool dialogs and roll them up every time for a litle more space, the UI its a mess and need to be more intuitive. to me Krita is still on alpha."
    author: "TIM.."
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "You're still here?"
    author: "other ac"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Please be nice ... \n\nFab"
    author: "Fab"
  - subject: "Re: Krita still sucks..."
    date: 2008-03-29
    body: "Unfortunately, he/she is correct.  Another example of the kind of software that is a road paved with good intentions... and never hath hell so much fury.  Grossly over complicates the simplest things.\n\nEx. an eraser is a concept I thought I was familiar with.  Used 'em like crazy, no doubt (i.e. yes, I make plenty of mistakes--BUT I ERASE THEM!).  But thanks to krita, it's like meeting the eraser all over again.  Click on it, touch your picture, and presto!, the whole thing turns purple. Huh?\n\nThis is why Microsoft, bad as it is, still makes sense to 80% of the world's computing population.  And they PAY for it."
    author: "ExKritaUser"
  - subject: "Re: Krita still sucks..."
    date: 2008-03-30
    body: "Er...\n\nI think you accidentally clicked, two or three years after the posting you replied to, on the \"selection eraser\" tool. Which erases bits of the current selection. If you want to erase bit of your painting, choose the eraser from the combobox in the topmost toolbar. (And yes, that's similar to the way a famous Windows application works.)"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "So you basically want to have your cake and eat it? Have all the necessary options (layers, tools, colors...) in view all the time, and all of your image, and no separate windows, and all that at 800x600? And the Gimp gives you this? I must admit I am slightly puzzled, because when I work with the Gimp I don't have enough pixels for everything and my image on my 1600x1200 screen... But no doubt you can suggest a solution.\n"
    author: "Boudewijn"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Im complaining by the fact the Main Windows (you know, the one containing the image) is covered by the tool windows in that resolution, you cannot send to back  and you cannot roll them up while they are floating, so it covers your work space and only 40% of the image is usefull, that's the problem, maybe you should try it and really try to work in such situations and you will se how unusuable Krita becomes.\n\n\n\n"
    author: "TIM.."
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Ah, in that case, if even the kiviostyle dockers don't satisfy you (and you _have_ tried that, haven't you, you aren't just complaining about something you haven't worked with?), then I'm sorry. I won't make a style of tool windows that can disappear behind the image window. So Krita will never be usable for you."
    author: "Boudewijn Rempt"
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "I couldn't spec less.\n\n"
    author: "TIM.."
  - subject: "Re: Krita still sucks..."
    date: 2005-06-22
    body: "Let's get it working and functional before we start tweaking it to death. All the usability in the world won't make a non functioning application work. Krita has had a long and rocky history (I even worked on it at one point), so I'm extremely happy that it's finally released.\n\nJust be glad they didn't copy the GIMP interface :-)"
    author: "Brandybuck"
  - subject: "Unable to compile with gcc4"
    date: 2005-06-22
    body: "the codes has some problem when built with gcc-4.0.1-rc2, \nneed do some forward declaration. \n\none is kexilabel, and the other is AiParser to AIParase. "
    author: "younker"
  - subject: "Re: Unable to compile with gcc4"
    date: 2005-06-22
    body: "gcc 4 is not officially supported by KDE. you've to wait for gcc 4.x release which compiles KDE code flawlessly. strangely though Fedora core 4 has KDE 3.4.x with gcc 4 ;)"
    author: "fast_rizwaan"
  - subject: "Re: Unable to compile with gcc4"
    date: 2005-06-22
    body: "Well, we all remember them shipping \"gcc\" 2.96..."
    author: "Anior"
  - subject: "Re: Unable to compile with gcc4"
    date: 2005-06-22
    body: "most likely they have compiled kde 3.4.x with GCC 3.4.x! gcc 3.4.x and 4.0 are binary compattible, afaik. and fedora is thus a mixture of gcc 3.4-apps and gcc 4.0-apps."
    author: "superstoned"
  - subject: "Krita is _awesome_"
    date: 2005-06-22
    body: "Krita actually has the potential to become better than the venerable photoshop, and that is the first time for a free software image manipulation program. Gimp usability still remains poor after _years_ of criticism and development, and it is not even well integrated into gnome. Krita integrates into KDE perfectly and now gets all the power from the underlying KIO etc. subsystems and is also very usable. Veeery good work!\n\nNow I'm going to try out Kexi... :-)"
    author: "stephan"
  - subject: "One more feature to add, IMHO"
    date: 2005-06-23
    body: "After educating myself on KOffice a bit (being locked in Windows world for now makes it hard to test) and having a tryst with my friend's Adobe Creative Suite 2 (bought recently for a web design studio) I'd say there is one thing KOffice would benefit from, and I guess it wouldn't be too hard to implement:\n\nA honest summary view.\n\nI fell for Adobe Bridge, honest. IMHO, that's one of the things that Adobe did right (overbloat in PS isn't one of them, personally I prefer 7.0, heck, even 5.5 to CS anyday) and it would be one more killer thing that would make KOffice stand out. What would it look like? I'd say cloning Kontact summary first would be a good start, only this time it would show things like recent documents, recent projects, maybe would also be tied to a file browser via Konq KPart AND would be the first thing user would see after starting KOffice in its entirety. Add to it the possibility of customizing the summary panels (like views in 3D Studio, for instance:\n ______________________\n|        |             |\n|________|             |\n|        |             |\n|________|             |\n|        |             |\n|________|_____________|\n\nor\n ______________________\n|           |          |\n|           |          |\n|___________|__________|\n|           |          |\n|           |          |\n|___________|__________|\n\n(sorry for craptastic ASCII art, and it's mangled anyway - just add spaces), where each panel would show, for instance, recent files, recent projects, todos, file browser etc., et. al. Just like Kontact, most important things at a glance.\n\nSo, what would you people think?"
    author: "aurelius_barzano"
  - subject: "Re: One more feature to add, IMHO"
    date: 2005-06-23
    body: "A kind of web-portal like thing for KOffice? Could be quite a good idea to expand koshell to do this kind of thing."
    author: "Boudewijn Rempt"
  - subject: "Re: One more feature to add, IMHO"
    date: 2005-06-23
    body: "The whole idea is to give the user a way to remind oneself what he was supposed to do at a glance. Imagine this scenario: a user, annoyed due to the fact that it's Monday, weekend is over, comes back to work. Starting KOffice he sees the summary view with all recent files, todos, recently visited sites, projects running, all data necessary to continue working. Without leaving the Office shell, he may launch all necessary programs, heck, also programs outside the Office, such as web browser or communications system (by means of customizable toolbar or maybe a set of quasi-hyperlinks). In essence, KOffice remembers, what he was working on and adjust the Summary to reflect it, thus making those comebacks easier. Also, it would benefit people who have files they return to - a separate field, where user would put \"favorite files\" to have them just a click away would be also an interesting feature.\n\nAs for expanding koshell this way, frankly, I am no software engineer, so maybe I should not throw any ideas around, lest I show my ignorance, but I'd say koshell is, at least from what I have seen, more like a common framework, a container for component applications. I'd keep it that way, as, well, a sandbox of sorts, while building Summary module as an independent application, maybe not available without koshell, but still not merely a set of features for it. \n\nTreating all structured apps like that would be a good idea. Heck, structuring similar apps might be a good idea. That would turn loosely connected programs into systems. A common shell for office programs, another for multimedia, another for the web, all applications available outside the shells, independent, but still ready to be combined into larger entities. For one thing, it would shorten start menus rather drastically. Each shell can be extendable with additional programs designed to fit, each program is able to use plugins, where necessary. And the user would just place, like, two or three shortcuts on his desktop and have everything, every bit of data within his grasp.\n\nJust add a file indexing system like GDS or Copernic Desktop Search, and watch chaos turn into order.\n\nBy the way, since my knowledge of K programs is rather mediocre, is it possible to open multiple documents (spreadsheets, text files, images etc.) in tabs? That, if not implemented, would be a great idea. Or allowing split windows a'la Konqueror to make quick comparisons. I'd say apart from limited functionality in Word, there is nothing like that in commercial software. Though i might be wrong.\n\nAnyway, I have a few ideas I'd like to contribute. I'm no programmer, have no idea how to code (might change that in the future), but when it comes to usability and similar stuff, maybe I could do something. Blast, I have a few ideas that need some good-looking mockups. Who knows, a free weekend..."
    author: "aurelius_barzano"
  - subject: "Re: One more feature to add, IMHO"
    date: 2005-06-23
    body: "KOffice includes the KOffice Workspace (also known as koshell) which does most of the things you just described. It has got a Kontact-like sidebar with all the KOffice apps, tabs for switching between documents, and a standard File menu including recent files. So, don't long for it, it already there :-)\n\nI'm not sure if customized views would be such a great idea, though."
    author: "Jakob Petsovits"
---
The <a href="http://www.koffice.org/">KOffice</a> <a href="http://www.koffice.org/people.php">team</a> is pleased to announce the next version of the <a href="http://www.koffice.org/info/">lightweight, integrated and complete office suite</a>. With exciting highlights like two new components - <a href="http://www.koffice.org/krita/">Krita</a> and <a href="http://www.koffice.org/kexi/">Kexi</a> - and support for the <a href="http://en.wikipedia.org/wiki/OpenDocument">OASIS OpenDocument file format</a>, the KOffice 1.4 release is a large step forward. Even a <a href="http://ktown.kde.org/~binner/klax/koffice.html">Live-CD</a> featuring the latest release is available so you can try before actually installing anything. You can also take a look at <a href="http://shots.osdir.com/slideshows/slideshow.php?release=353&slide=1">screenshots at OSDir</a> to get a first glance. <a href="http://www.koffice.org/announcements/announce-1.4.php">Read the full announcement</a> and the <a href="http://www.koffice.org/announcements/changelog-1.4.php">changelog</a> for further details!







<!--break-->
