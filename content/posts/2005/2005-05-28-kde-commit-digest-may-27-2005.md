---
title: "KDE Commit Digest for May 27, 2005"
date:    2005-05-28
authors:
  - "dkite"
slug:    kde-commit-digest-may-27-2005
comments:
  - subject: "XHTML improvements?"
    date: 2005-05-28
    body: "I don't understand.  Konqueror 3.4 doesn't support XHTML:\n\nhttp://bugs.kde.org/show_bug.cgi?id=52665\n\nHas XHTML support been added to HEAD recently?  The commit digest says:\n\n\"Merge/port/implement XHTML namespace selectors, and a few other XHTML\nimprovements.\"\n\nThis sounds like it's adding features in related things like CSS that are intended to work with XHTML, but under the assumption that XHTML support is already there.  At the moment, Konqueror treats XHTML as HTML, which violates the XML 1.0 and XHTML specifications.\n\nXHTML support is a big task, it's not just a case of piping XHTML documents through an HTML parser.\n"
    author: "Jim Dabell"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "well, who cares about specifications if it renders correctly and 70% of all pages do not comply.\n\nxhtml = html more strict."
    author: "gerd"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "> who cares about specifications if it renders correctly\n\nXHTML has differences in CSS and the DOM.  \"Displaying like HTML\" is not rendering correctly in many cases, even once you leave out the mandatory error handling.\n\n\"Who cares about specifications?\" is a very short-sighted attitude to take anyway.  Every major browser release going back to Netscape 1.2 has broken things for people who thought that they don't have a problem because it works in most browsers.\n\n> 70% of all pages do not comply.\n\nHuh?  You are either talking about pages served as text/html, in which case 70% non-compliance is way too low and is completely irrelevent to XHTML anyway, or you are talking about pages served as one of the XML media types, in which case 100% of pages *do* comply, because if they didn't, they'd break in all other XHTML browsers.\n\nI don't think you understand.  Konqueror is the ONLY browser I know of that fails this part of the XML specification.  Even Internet Explorer gets that bit right!\n"
    author: "Jim Dabell"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: ">who cares about specifications if it renders correctly and 70% of all pages do not comply?\n\naccording to that guy from Novel, people that don't take shower care :)\n"
    author: "Pat"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "Bug #52665 is just a report of lots of small issues many of them that had nothing in particular to do with XHTML. From a cursory glance it looks like all the problems reported in the bug have been fixed.\n\nAnd no, Konqueror doesn't treat XHTML as HTML and never has, except possibly when the mime-type detection have failed."
    author: "Allan Sandfeld"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "> Bug #52665 is just a report of lots of small issues many of them that had nothing in particular to do with XHTML.\n\nAllan, I'm the reporter of that bug, there's no need to explain it to me.  And practically every comment was directly related to XHTML.\n\n> From a cursory glance it looks like all the problems reported in the bug have been fixed.\n\nYou obviously didn't click on the testcase; it still fails.\n\n> Konqueror doesn't treat XHTML as HTML and never has, except possibly when the mime-type detection have failed.\n\nNot failing on parsing errors is one example of Konqueror treating XHTML as HTML.\n\nDifferences between CSS in XHTML and HTML is another example of Konqueror treating XHTML as HTML (hint: try styling tbody elements when you don't have any tbody tags).\n\nDifferences between the content models of the <script> and <style> element types is another example of Konqueror treating XHTML as HTML (try commenting the contents out).\n\nThose three examples are all failing for me right now in Konqueror 3.4.0, where they work just fine in Firefox and Opera, and have done for as long as they've supported XHTML IIRC.\n"
    author: "Jim Dabell"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "No I tried all the test cases, and saw no remaining issues. The first one fails in a parse error, the second adopts the right charset, and the last runs the script in both cases.\n\nThe only problem right now is that Konqueror doesn't advetise its XHTML support in the HTTP header.\n\nNotice that the commit as mentioned fixed many other small bugs, some of which helped a lot more test cases than adding namespaces, they were just small one-liner bug-fixes."
    author: "Allan Sandfeld"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "> No I tried all the test cases, and saw no remaining issues. The first one fails in a parse error\n\nI'm not seeing that in 3.4.0, which is why I was asking if it had changed.\n\n> Notice that the commit as mentioned fixed many other small bugs\n\nI would have liked to have figured that out myself, but the server isn't responding for me so I can't read what they are :).\n\n> The only problem right now is that Konqueror doesn't advetise its XHTML support in the HTTP header.\n\nI'll start adding testcases for the problems I see with 3.4.0.\n"
    author: "Jim Dabell"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "But please add more XHTML test cases which you think we should handle. It has been low priority, but I am fixing them now in order to learn the XML side of KHTML."
    author: "Allan Sandfeld"
  - subject: "Re: XHTML improvements?"
    date: 2005-05-28
    body: "Sorry, but KHTML *did* treat XHTML as HTML, at least in the past (for examples replacing /> by > to be again HTML compatible, not respecting of <?xml encoding=\"\"?> and so on...) \n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "bittorrent in Kget!!!!"
    date: 2005-05-28
    body: "hey Derek, it looks like you forgot to mention that one in the new :)\n\n\"Now it is possible to delete a transfer using the popup menu :-)\nWow! With this in place and all the minor fixes to the bittorrent\nplugin I did today, kget is pretty fuctional again (but there is still a\nlot of work to do..). The most exciting stuff is the Felix's bittorrent \nplugin that, after a little bit of testing, seems to be ok!!\n\nGreat work, Felix!!\n\nNow I can be happy all next week :-)\""
    author: "Pat"
  - subject: "Re: bittorrent in Kget!!!!"
    date: 2005-05-29
    body: "Wooo!  KGet supporting BitTorrent would be very nice (no need to load up the java VM and Azureus, or even have them installed)!  Will KGet's support be pretty much like how the official client's GUI would look (or use to a year or so ago, pretty much like just a download window in IE w/o much additional information)?  Or will it have more information like Azureus?"
    author: "Corbin"
  - subject: "Re: bittorrent in Kget!!!!"
    date: 2005-05-29
    body: "First of all I would like to remember everyone that this feauture (and obviously others) will ship with kget in kde4.\n\nMy plan is to provide a detailed widget showing the most important informations about the torrent. I don't think it will ever reach the level of Azureus in terms of provided info, since this would lead to a bloated gui, but the useful ones will be there.\n"
    author: "Dario Massarin"
  - subject: "Re: bittorrent in Kget!!!!"
    date: 2005-05-30
    body: "How about downloading from multiple urls simultaniously like DAP (download accelerator) or D4X (Downloader for X) \n\nAm looking at the code to try understand how they implemented it in D4X but i've quickly realizing how tricky it maybe to implement with KIO.\n\nAlso ftp search wouldn't a bad idea...\n\nregards,\n\nSam"
    author: "iceman"
  - subject: "Re: bittorrent in Kget!!!!"
    date: 2005-05-31
    body: "What do you mean with FTP search?"
    author: "ac"
  - subject: "Re: bittorrent in Kget!!!!"
    date: 2005-07-04
    body: "I meant for normal download (not torrent) Kget could have an option FTP search feature that searches for mirrors etc thus having multiple source for the same download.  This can speed up downloads by Kget detecting and avoiding busy mirros."
    author: "SammyICE"
  - subject: "Also another missed mention.."
    date: 2005-05-28
    body: "\"Initial upload of SuperKaramba to kdereview.\"\n\nJust thought it was news worthy. ;)\ncheers,\nRyan"
    author: "Ryan"
  - subject: "KDE 3.4.1 release?"
    date: 2005-05-29
    body: "I am eagerly waiting for KDE 3.4.1, it was supposed to be released on 25th may, doesn't it?"
    author: "linux_user"
  - subject: "Re: KDE 3.4.1 release?"
    date: 2005-05-29
    body: "The schedule was to tag on 23th May\nhttp://developer.kde.org/development-versions/kde-3.4-release-plan.html\n\nSo it cannot be released in just two days!\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KDE 3.4.1 release?"
    date: 2005-05-29
    body: "http://www.kde.org/info/3.4.1.php says May 31st, 2005"
    author: "Anonymous"
  - subject: "Re: KDE 3.4.1 release?"
    date: 2005-05-31
    body: "holly shit, that's today!!!\n\n\n\n:)))))))))))))))))))))))))))"
    author: "dan"
  - subject: "(SVN revision|bug) numbers"
    date: 2005-05-29
    body: "Hi Derek,\n\nMaybe you're already aware of it, but SVN revision numbers are marked as bugnumbers in the digest. A nice candidate of a dirty hack in the script :)"
    author: "Bram Schoenmakers"
---
In <a href="http://commit-digest.org/?issue=may272005">this week's KDE Commit Digest</a> (<a href="http://commit-digest.org/?issue=may272005&all">all in one page</a>):

<a href="http://edu.kde.org/kalzium/">Kalzium</a> adds gradients and crystal structure data.
<a href="http://www.koffice.org/">KOffice</a> supports loading of embedded objects from <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">OASIS</a> format.
<a href="http://www.konqueror.org/features/browser.php">khtml</a> improves XHTML handling.
<a href="http://kopete.kde.org/">Kopete</a> adds full text search of history, styles, receiving files and buzzing in Yahoo, and work continues on video device support.
KDE 4 work continues with some applications able to run.



<!--break-->
