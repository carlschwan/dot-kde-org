---
title: "A Whole New World with The G System"
date:    2005-05-20
authors:
  - "rlangerhorst"
slug:    whole-new-world-g-system
comments:
  - subject: "planned server downtime during this afternoon"
    date: 2005-05-20
    body: "Please note that there is planned maintenance work on my internet connection this (20th) afternoon at about 4 p.m. (CET). This means: G System website will be available (not my internet connection). My blog and the universe@raphael.g-system.at master server will NOT be available.\n\nI think it will only take about 2 hours, so everything should be fine and accessible later on.\n\nRegards,\nRaphael"
    author: "Raphael Langerhorst"
  - subject: "Re: planned server downtime during this afternoon"
    date: 2005-05-21
    body: "The server is available again, please note that it has a high latency, thus connecting with the guniverseclient might not always work.\n\nWe're working on providing a guniverse server on a faster internet connection. News about this will be posted on the http://www.g-system.at website."
    author: "Raphael Langerhorst"
  - subject: "You are now in The Matrix! :)"
    date: 2005-05-20
    body: "seems like the movie 'matrix' is becoming real ;)"
    author: "Fast_Rizwaan"
  - subject: "Re: You are now in The Matrix! :)"
    date: 2005-05-20
    body: "There is more truth to this than you might know ;)\n\nReflecting reality is the ultimate goal of the G System.\n\nAnd we know that this is not that easy and restrictions will always apply. Still, we've already spent many a thought on design considerations to make it possible to reach this goal as best as we can.\n\nIt is also important to note that we consider reality not just as a accumulation of mathematical equations and/or physical laws. But we want to go beyond, to embrace all aspects of life including emotional and mental levels (think of society as a whole, or your own inner life), or even spiritual aspects (to use this word carefully...)."
    author: "Raphael Langerhorst"
  - subject: "Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "So can this program be used to proof once for all the ammount of holes in the evolutionist theory that shows the creationist theory to be much more believable?"
    author: "David Costa"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "The answer is, in short, yes and no.\n\nThe project will certainly help in dealing with this question (it is after all intended to fill such gaps of today's knowledge).\n\nI personally(!!) think that there is truth and gaps in both of them - I don't know all about both theories though."
    author: "Raphael Langerhorst"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "Just a note that there are other forums where this \"great debate\" can rage rather than here. However, unlike evolution which is a genuine scientific theory regardless of whether one buys into it or not, creationism and its pseudo-scientific offshoots are merely beliefs. As to whether the software concerned can validate beliefs is arguably also too philosophical for this particular forum. ;-)"
    author: "The Badger"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "> However, unlike evolution which is a genuine scientific theory \n> regardless of whether one buys into it or not\n\nSo you're implying that the evolution hypothesis is falsifiable (which is a prerequisite for a \"genuine\" scientific theory).  How can you falsify an explanation for events that happened in the past, with no surviving observers? \n\nEvolution is plausible to me, but can it be called a theory? \n\n"
    author: "cm"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "Oh, come on, you are getting silly. Evolution is clearly falsifiable. Here are a few ridiculous examples:\n\nSuppose you found a dog's fossil in a cretaceous stratum. Or suppose you found a bird with an insect-like 4-wing arrangement. Or that DNA analysis showed gorillas are closely related to dung beetles.\n"
    author: "Roberto Alsina"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "So what?  All these examples would prove is that a certain accepted development tree may have been wrong.  It says nothing about whether the mechanisms of evolution have led to the development of the species in the past (which I said I find *plausible*). Evolution does not *predict* that birds with insect-like 4-wing arrangement don't (or didn't) exist thus finding one would not falsify it. \n\n"
    author: "cm"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "Well, what exactly is the statement you want to know is falsifiable?\n\nThe examples I gave would falsify the explanations evolution gave for the development of several lifeforms, making it less believable as a general explanation.\n\nIf you want to falsify evolution itself, it's pretty easy, too.\n\n1. Get a big bottle\n\n2. Get fruitflies\n\n3. Breed them for a long time\n\n4. Verify that there have been no mutations, or very few.\n\nIf you do this for a long enough time, that would mean the rate of mutation is too low to account for anything important, and evolution can't be the explanation for anything.\n\nYou see, evolution defined as random mutations selected by the environment makes a prediction: if you breed something that has a short lifespan, it will mutate fast. And that can be proven false. Thus it is falsifiable.\n\nIf that doesn't satisfy you, tell me exactly what you mean by \"evolution\".\n\n"
    author: "Roberto Alsina"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-24
    body: "I actually did that at school. We counted 2 out of 30 flies with too short wings. They didn't die though as they didn't need to flie to get foot in that bottle."
    author: "Stephan Kulow"
  - subject: "Theories do not need to predict"
    date: 2005-05-23
    body: "at least not specific developments ... unless they are being tested in a controlled experimental setting.\n\nEvolutionary theory has predicted adaptation and observed it in action in specific settings such as the Galapagos Finches (which have been under intense observation for decades) repeatedly.  So even the growing proportion of Americans who believe the earth is only 6000 years old and that global warming is not happening, doesn't have to believe in dinosaurs or stretch their minds into frightening new territory in order to see evolution in action.\n\nIn laboratory settings it is fairly easy to demonstration adaptation but this isn't much use in demonstrating the robustness of the theory. If you have accessed this page from a Texas highschool where teaching evolution has been prohibited please exit this site immediately. \n\n"
    author: "ooga"
  - subject: "Re: Theories do not need to predict"
    date: 2005-05-27
    body: "Actually, that'd be Kansas.  I'm in Texas but went to school when EVERYBODY knew evolution was false.  My momma said god made us.  The devil put dinosaur bones there to make us not believe."
    author: "Henry"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "> So you're implying that the evolution hypothesis is falsifiable (which is a > \n> prerequisite for a \"genuine\" scientific theory). How can you falsify an \n> explanation for events that happened in the past, with no surviving observers?\n\nEvolution is a description of a process, it's not an account of history. You can simply falsify evolution by proving the process doesn't work as described in the theory. "
    author: "Erik Hensema"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "> Evolution is a description of a process, \n> it's not an account of history. \n\nAnd from your other post: \n> Note that evolution deals with the development of life, not the origin of life.\n\nI think that's at the core of many misunderstandings, this one included.  It's a semantic problem.   But even Charles Darvin talked about the origin (\"Origin of Species\") so this has been there from the beginning (no pun intended).  And there are many people who claim that the evolution theory allows you to answer the question of the origin of life (but that part is \"just\" a belief, just like the creationist idea.  Not that having a belief were something bad in itself).  \n\nThe same situation occurs with star formation calculation.  You can have very nice models, and you can even still watch the formation at work in space.  But all that nice scientific work cannot prove our sun's origin.  It can make it *extremely* plausible though, just like evolution. \n\n\n> You can simply falsify evolution by proving \n> the process doesn't work as described in the theory.\n\nHow?  (I'm not saying you can't.) \nWhat predictions does it make?\n"
    author: "cm"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: ">> You can simply falsify evolution by proving\n>> the process doesn't work as described in the theory.\n\n> How? (I'm not saying you can't.)\n> What predictions does it make?\n\nMaybe 'simply' was a poor choice of wording ;-)"
    author: "Erik Hensema"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2007-12-23
    body: "I get tired of this argument there is no one who is alive today to say whether there were evolutions in the past . Any mutation would produce a in between species . Lets say a frog is in between becoming a bird the feathers would not help but hinder the animal from surviving . there is a case in point of frogs infested with a parasite that makes them have three legs etc  well these are the ones consumed by hungry predators first .. the fact that they would be consumed by the stronger more developed species proves that natural selection theory everyone is so adament about. your flies dear sir would have been consumed by ants outside the bottle.if there is a instant change then why do we not find this evidence in the fossil records. Cause it just does not happen thats why .I mean come on if it happened then it would be happening know Species develop into different versions of the same species (how many finches are there )they are still finches not finch/dogs/fish. Ihave another question that has always bugged me if we came from apes why are they still apes .I mean to say that dinasaurs became birds then where are the dinasaurs. There are to many variables to convince me that GOD is not on his throne amused  by how smart we all think we are . You cant go back in time to offer proof and i cant die and telephone you to describe God so we will always be at this standstill."
    author: "CHRIS J "
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "> You can simply falsify evolution by proving the process doesn't work as described in the theory.\n\nAlthough the process clearly does work.\n\nSee for example: http://www.informatics.sussex.ac.uk/users/adrianth/ascot/paper/paper.html\n\nAnd what is selective breeding if not evolution with a different selection criteria? Ie, pigs are selected for good meat etc, rather than survival in the wild."
    author: "Tim"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-23
    body: "What, specifically, is not falsifiable? To which theories of evolution are you referring, and to which details of those theories are you referring? (E.g., which specific mechanisms and or products of certain theories of evolution not falsifiable?) \n\nA point is that specific details within the topic of evolution are argued and modified."
    author: "ac"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2006-08-16
    body: "What specifically is not falsifiable? \n\nPut simply, in modern evolution theory:\n\n1. Animals' bodies are built by their genes.\n2. Animals inherit their genes from their parents, and pass them on to their children.\n3. On rare occasions, genes also change, or mutate.\n\nThose are fairly concrete, and taken as true; the following two aspects are educated theories based on the aforementioned ones:\n\n4. If an animal has a gene that gives its body an advantage, that animal is more likely to survive and have    children--children which will also have that gene.\n5. Over time, genetic changes will accumulate in a population until their bodies are so different, they are a new species."
    author: "Dy"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2007-01-21
    body: "The ansrew to your aruguement is in the bible. God made men in his own image. Men were not created randomly or spontanously. Life comes only from existing life that disproves the big bang and soup theory. This is the truth"
    author: "yo"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2007-10-22
    body: "the creationist theory is both believable and also ludacrist  at the same time im not implying that god isn't real simply saying that do you actually think we were planted on earth to grow? the evolutionist theory is unbelievable stupid how do we evolve from cells to scaled fishes to another species of monkeys to ape then humans genaticaly speaking how in hell does your body go from an inviorment of water to trees i doubt with high hopes that i was an ape the evolutionist theory was simply suggested because someone had no idea were we came from we simply came from god Darrow scopes tried to force his idiot thoughts upon the slow people who thought that monkey  and humans are related by DNA  not likely.!!!!!!!!!\n\nIn the early years of anthropology, the prevailing view was that culture generally develops (or evolves) in a uniform and progressive manner.Dose not  state that we can change forms simply any animal to another in the matter if the evolutionist theory is plauseable then what will we look like 300 years from now because of global warming will we be dragons since we are evolving                (and i quote due to the theory of evolution)\n  "
    author: "danyelle"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2008-04-06
    body: "Firstly, dragons? Sarcasm is the recourse of a weak mind. Secondly, due to current changes and inter-racial breeding humans will have near perfect and uniform skin pigmentation, and we're likeley to be much... Bigger than we are now.                     "
    author: "Nero, the true beast"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2008-04-06
    body: "Another thing. we have effectively had our hands in the evolution of plants, viruses and bacterium for centuries now. Think about it."
    author: "Nero, the true beast"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2009-01-15
    body: "Let's not get confused about what is what here.  I am a Christian.\n\nThe :law: of evolution is indisputable, but the :theory: is disputable.\n\nIf you believe that not all species are the same in DNA, and that an offspring produced through sexual reproduction is not exactly the same as its parents, then you believe in the :law:, but not necessarily the theory.  The theory is simply an attempt to explain the law.  Believing in the law does not mean you don't believe in creationism.\n\nCreationism is not a theory, it is a belief.  A theory must be falsifiable.  Any objection to creationism is usually explained as \"because God said so,\" making it unfalsifiable.  If you believe that evolutionary theory is not falsifiable, then you do not believe that it is a scientific theory, and is therefore not a viable explanation to the law of evolution.  The fact is that the theory of evolution is falsifiable; if you found a full human skeleton in an area believed to be from before they evolved, that would falsify the current theory.\n\nIf you found a better explanation for the law of evolution, it would replace the current one.  If it involved creation, so be it.\n\nSo, when you try to discredit evolution, specify which one you are talking about:  the law or the theory.  The theory is honestly debatable.  The law honestly isn't."
    author: "Ian"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "Creationism is not a theory, but a belief. Evolutionism is a theory, thus they cannot be compared (cf. Popper). Do not loose time with beliefs, they just limit your creativity."
    author: "oliv"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "too bad you'd also have to believe in the evolution theory ;)\n\nI'd think it'd take alot _less_ faith to believe that we were created than to think that we are a freak of nature as evolutionists would have you believe.\nif you want a good resource to creationism from a scientifical stand point I've just finished watching all of the DVDs from http://drdino.com Found them to be very interesting."
    author: "Stephen Leaf"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "While your assertions on \"faith\" in science are flawed, let's just run with it for a second.\n\nIt takes a lot less \"faith\" to believe classical model of particle behavior than a quantum mechanical model.  But most would agree that with the latter we've gotten closer to something that approximates reality.\n\nAnd see, that's the real sticking point.  I don't \"believe\" in evolution, it just sounds pretty plausible to me.  It needn't be perfect, in the same way that Newtonian mechanics were in fact \"wrong\" in a sense -- it just needs to be something that seems to fit with the observed information and moves towards understanding.  It also lends a bit of credibility if it doesn't just happen, though a set of a priori assumptions, to confirma belief that we started out with.\n\n"
    author: "But..."
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "As others have pointed out, science is faithless in all senses of the word.  People \"believe\" evolution only because it is the only credible scientific theory.  The second something more credible comes by, scientists will abandon evolution like rats from a sinking ship.\n\nThe reason they haven't?  No credible scientific alternative theory at all, let alone a MORE credible theory.\n\nThat said faith and science are as incompatible as fish and cauliflower.  The fact that they are totally unrelated doesn't mean they're mutually exclusive.  You can \"believe\" both and not have any conflicts.\n\n\"I believe...\", when used regarding matters of faith, is just a polite way of saying \"I know...\"\n\n\"I believe...\", when used regarding matters of science, is just a concise way of saying \"I'll never know, but the available evidence and scientific research indicates...\"\n\nIt's just two ways of looking at the world.  Science assumes we start out knowing nothing, and that we are only more and more closely approximating truth without actually ever knowing it.  Faith assumes you can jump straight to the truth by reading a book.\n"
    author: "ac"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-23
    body: "> As others have pointed out, science is faithless in all senses of the word\n\nWell, maybe in an ideal world. But as people doing science are, well, people,\nwhat you often get, is a mixture of prejudices, beliefs and scientific facts.\nAnd I am not saying, that this is necessarily bad. What is bad, is that the\nscientist is frequently ignorant of this.\n\n> The second something more credible comes by, scientists will abandon\n> evolution like rats from a sinking ship.\n\nHistorical evidence regarding the acceptance of new scientific theories\ndoesn't seem to support your claim. A good book to read is\nTh. Kuhn: Structure of Scientific Revolutions.\n\nP.S. I think this discussion is a bit off-topic here ..."
    author: "Jonathan Verner"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "You got it exactly backwards. The basis of science is that theories **can** be wrong. The basis for belief is that you believe they are right."
    author: "Roberto Alsina"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "Right.  Though this has been debated for, oh, say since about 1859, but I'm sure we're just one quick hack away from finishing this debate.  Why didn't we think of this earlier?  Anyway, thanks for hijacking this thread and turning it into a religious platform."
    author: "Scott Wheeler"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-20
    body: "Yes it can, but only because crazies are great at completely misinterpreting data to justify their own twisted ideas."
    author: "Robert"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "There are to main streams in creationism. Both start off with an intelligent creator who creates life on earth.\n\nWhat happens next is the really interesing part. With a given population of life on earth, how does life develop? Does the development follow the rules of the evolution theory or are there only minor variations in life while no new species at all develop?\n\nThe first version of creationism doesn't conflict with evolution. And evolution doesn't conflict with creationism either. Note that evolution deals with the development of life, not the origin of life.\n\nThe second version of creationism is the version proposed by religious fanatics. Luckily this version can easily be proven wrong, and it indeed has been proven wrong on many occasions. This fundamentalist form of creationism therefore is just a belief.\n\nHowever, the G System is a designed world, just like the creationists world. So to answer your question: yes, the G System can be used by fanatics to misinterpret data in order to support their beliefs.\n\nThe G System will have laws, just like the real world. One of these real life world laws is the law of evolution. We can see it happening around us, we know it's there. We can try to describe this law in a theory, which we did: the theory of evolution. \n\nThe G System can also implement a law of evolution. Or it can implement more fundamental laws which imply evolution (which of course also holds true in the real world). Then somebody not knowledgable of the design laws of the G System can start observing the simulated world and try to formulate theories on these observations. It's quite unlikely anybody will be able to unravel the G System simply by observing it for a short period of time. So it's quite likely the theories on the inner workings of the system will be wrong. The observer may formulate a theory on evolution in the G System, which may or may not be the exact law of evolution as coded into the G System.\n\nAnd of course the theory of evolution in the real world is unlikely to be the same as the law of evolution. But that doensn't change the fact there *is* a law of evolution.\n\nSo in answer to your question: the G Sytem is unlikely to prove anything in the real world, but it may provide an insight in the development of religious beliefs and scientific theories."
    author: "Erik Hensema"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "This is a good summary about the issue of theory/religion/science/belief!\n\nThank you!"
    author: "Raphael Langerhorst"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2007-09-27
    body: "All right i see how thos could be but what if niether Creationism or Evolutism exist?  What if the earth has alwasys been?  Creationism is the belief that an over powerful \"god\" exists and has always existed; while on the other hand Evolutionists believe that there was on cell that JUST happened to be there and split and multiplied and over time happened to form what is today. \nThe reason i contacted you  is becaue i am doig a research report on if the Earth had always been; like creationism view kind of but o over ruling \"god\".\n Please reply to dcparkml@yahoo.com please.\n\n Thanks \n     Miles"
    author: "Miles"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-05-21
    body: "Computer simulations have already been run on parts of evolutionary theory, and have shown them to be plausible - http://www.simonyi.ox.ac.uk/dawkins/WorldOfDawkins-archive/Dawkins/Work/Articles/1995-06-16peepers.shtml is just one example. \n\nIf you look around, you'll find simulations on everything from the male/female ratio to Zahavi's handicap principle. \n\nMay I suggest that a full simulation of evolution is possibly more than is needed to test out one of your \"holes\"... why not simulate this specific part? "
    author: "joe lorem"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-09-07
    body: "i am very surprise that until now people still can't understand & accept that this whole universe keep evolving and changing. Lack of knowledge is one thing , why can't they open up their mind and observe the changing of species and geographical factors which we often encounters like one presented by the national geographic society ? By BELIEVING that this world is created less than 9000 years or 9 millions still doesn't justify and convince world communities on such topics . i for one will never teach my children that this world is created, because i find this is very dangerous as small child may not be able to  differenttiate between fantasy & fact .So please a word of advise to the Creationist out there if you havent got much to prove on your subject please keep them to yourself or your own inner circle of followers. be more more responsible as the law of the world state that 'You are free to believe or do what you like as long as you don't disturb others ' applies.\n\nI can bet with the creationist sooner or later your follower will decrease and in the end can't survive by late 21st century. Look at the number of attendance in the church of england keep dropping every year will only show that the minister, father , priest or pastor can't  answer simple question on the existent of life from their own christian fellows.\n\n  \n\n\n\n\n\n\n\n"
    author: "Thomas Wong P L"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2005-09-09
    body: "> be more more responsible as the law of the world state that \n> 'You are free to believe or do what you like as long \n> as you don't disturb others ' applies.\n\n\nNow that's funny.  What you've said above tells me you don't respect that rule yourself.  So please keep your advice to yourself and stop this thread. \n\n(Note that I'm not a creationist.) \n\n"
    author: "cm"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2006-11-14
    body: "i have been raised in a Christian home my whole life, and i KNOW that each and every one of us was created for a reason, and that everyone has a purpose in life. To say that we evolved from bacteria or what ever is just silly! There is actually evidence to suggest that the stories of the Bible are true! Like for the story of Moses parting the Red sea, in one of the tombs in Egypt there are paintings on the wall that depict a sea being parted by a man, while he is being chased by some of the pharoahs men,how do you explain that? is it all just coincidental that there just happened to be a painting of the red sea being parted? or not? well, what ever you decide to believe is up to you, but hopefully i have given you some food for thought."
    author: "_qw_"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2008-10-27
    body: "did you ever think that picture was a metaphor for something else?  Don't believe everything you read or see.  Things are sometimes not what they appear."
    author: "Mel"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2006-11-14
    body: "i have been raised in a Christian home my whole life, and i KNOW that each and every one of us was created for a reason, and that everyone has a purpose in life. To say that we evolved from bacteria or what ever is just silly! There is actually evidence to suggest that the stories of the Bible are true! Like for the story of Moses parting the Red sea, in one of the tombs in Egypt there are paintings on the wall that depict a sea being parted by a man, while he is being chased by some of the pharoahs men,how do you explain that? is it all just coincidental that there just happened to be a painting of the red sea being parted? or not? well, what ever you decide to believe is up to you, but hopefully i have given you some food for thought."
    author: "_qw_"
  - subject: "Re: Creationist vs Evolutionist theory"
    date: 2006-03-09
    body: "we cannot just belive in the evolutionist theory cause, how can we be created if no one created us?"
    author: "Gerard Filio"
  - subject: "Oh my god..."
    date: 2005-05-20
    body: "God doesn't like me:\n\ngod: symbol lookup error: god: undefined symbol: _ZN3GWE19GWorldEngineFactoryC1EP7QObjectPKc\n\nLet's visit some mailing lists..."
    author: "Bram Schoenmakers"
  - subject: "Re: Oh my god..."
    date: 2005-05-20
    body: "If such things happen it is likely that the libraries are not found, you should have the path where you installed the G System (the lib subdir) in /etc/ld.so.conf or set the environment variable LD_LIBRARY_PATH to the G System lib path.\n\nExample. G System installed in /home/joe/local, then the libs will be installed in /home/joe/local/lib, so you either add /home/joe/local/lib to /etc/ld.so.conf and run ldconfig or export LD_LIBRARY_PATH=/home/joe/local/lib and run god (or any other G System binary) from that shell. Option one is of course recommended, as it permanently solves this issue.\n\nIt is planned that the installer checks if the libraries are accessible, but this is not yet implemented. Meanwhile you have to make sure the libs are found yourself.\n\nGood luck!"
    author: "Raphael Langerhorst"
  - subject: "Re: Oh my god..."
    date: 2005-05-20
    body: "When running ldconfig I get this:\n\nldconfig: /usr/kde/3.3/lib/libGWE.so.0 is not a symbolic link\nldconfig: /usr/kde/3.3/lib/libGCS.so.0 is not a symbolic link\nldconfig: /usr/kde/3.3/lib/libGCE.so.0 is not a symbolic link\nldconfig: /usr/kde/3.3/lib/libGBE.so.0 is not a symbolic link\n\nBut when I run\n\nLD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/kde/3.3/lib god\n\nit's fine. However, the additional pad appears in my /etc/ld.so.conf (because it's just my KDE dir)."
    author: "Bram Schoenmakers"
  - subject: "Shouldn't it be named..."
    date: 2005-05-20
    body: "the K System instead ? :p"
    author: "Narishma"
  - subject: "Re: Shouldn't it be named..."
    date: 2005-05-20
    body: "Psst, Navindra, pssssst!\n\n(This is a conspiracy, don't you know? It's aim is to make all G-Lovers believe it is a G-Program. So they'll all join a stampede to install it. Then the auto-dependency resolving algorithms of the Gent-uh, the Ooboontoo, the RoodHoot and the other distros will, behind their backs, auto-install the Q-Toolkit and the K-Libraries. [OK, some losses we'll have to take -- there are broken distros where this will not work, but well....]. After a bit of using it, the G-Lovers will start to love G-System and will gradually move over to become K-Lover. -- So don't make the guardians of the G-Realm aware of The Grand Plan before it worked out by plastering your careless comments to the K-Dot!)"
    author: "anonymous"
  - subject: "Re: Shouldn't it be named..."
    date: 2005-05-21
    body: "ROFL"
    author: "cm"
  - subject: "G 0.5, or 0.5 G?"
    date: 2005-05-20
    body: "Hmm...  Rather than the traditional version string after the package name, in this case, the obvious thing is to put it before!  Thus...\n\n0.5 G\n\nOf course, things /really/ accelerate beyond 1.0 G...\n\n<g>"
    author: "Duncan"
  - subject: "Re: G 0.5, or 0.5 G?"
    date: 2005-05-20
    body: "This thread reminds me of an old saying:\n\n\"The best way to accelerate any computer running Windows is at 9,81 m/s2\"\n\nOh well, off topic I'm afraid.\n\n;-)\n\nGerard\n\n"
    author: "Gerard"
  - subject: "G-system vs. G-spot"
    date: 2005-05-20
    body: "There sure is allot of hocus pocus talk about this. I remember when this was first announced on the dot. Back then there was talk that this would one day simulate the entire universe or something of that nature by the main developer. All humans and interactions etc. etc. Sure. But it was a GL app showing spheres rotating around other spheres.\n\nIs the app now anything beyond spheres rotating around spheres basically? Or is this more about those who project all kinds of things into it see all kinds of stuff in it, like a painting?\n\nI dunno about the G-system universe, I prefer the G-spot universe myself."
    author: "ac"
  - subject: "The installer"
    date: 2005-05-20
    body: "It was a nice surprise to get a graphical installer (even if it did ask for confirmation a bit too many times). Any plans to make this a generic addition to kde software? Having both the configure-make-make install scripts and a graphical installer script in all kde software would be cool.\n\n"
    author: "Apollo Creed"
  - subject: "Demo bufix available"
    date: 2005-05-21
    body: "Hello,\n\nthe demo in the 0.5.0 release has a bug that prevents normal operation, a patch is now available.\n\nLook at http://www.g-system.at/index.php for the details.\n\nRegards,\nRaphael"
    author: "Raphael Langerhorst"
  - subject: "Frameworks and data types"
    date: 2005-05-25
    body: "Keeping in line with the tradition of innovative programming frameworks the G project has defined their own datatypes for everything. One of the results is that whenever there is text processing involved, the developer is strongly encouraged to use GStrings."
    author: "Anonymous Joe"
  - subject: "Re: Frameworks and data types"
    date: 2005-05-25
    body: "(Are you sure you know what you're talking about?)\n\nThe G System does not introduce any new data types for something that already exists in the framework stack it uses, I hope you don't mean the GElement and GObject or GEnergy classes (or whatever). Such things simply don't exist anywhere - after all this is what actually makes up the system.\n\nIf you refer to the QString classes(??), this is the Qt framework. We just use what's there.\n\nRegards,\nRaphael"
    author: "Raphael Langerhorst"
  - subject: "Re: Frameworks and data types"
    date: 2005-05-27
    body: "Just to be clear about the issue, I have absolutely no idea what I am talking about. My post was utterly and entirely meant for humorous purposes. I just couldn't resist the temptation to use the obvious QString pun.\n\nSorry about the confusion. I blame Slashdot for everything."
    author: "Anonymous Joe"
  - subject: "Re: Frameworks and data types"
    date: 2005-05-28
    body: "ah, I see. I guess I took it too seriously ;)"
    author: "Raphael Langerhorst"
  - subject: "Re: Frameworks and data types"
    date: 2005-05-26
    body: ">the developer is strongly encouraged to use GStrings.\n\nLOL!!! I'd love (not) to see a bunch of hackers, sitting on a stack of empty pizza boxes, programming like fanatics... in nothing but g-strings ;-D\n\n~Macavity"
    author: "macavity"
---
The <a href="http://www.g-system.at">G System</a> is a free and open source simulation framework and virtual reality, using Qt and KDE.  The recent <a href="http://www.g-system.at/news_detail.php?newsid=37&forum=4&thread=69">0.5 release</a> adds multi-user capability, an important milestone in the history of this project. Using the G System many users can now join in the same virtual universe.


<!--break-->
<p>The G System framework provides advanced network distributed operations,  allowing for huge simulations.  The virtual reality created with the G System, called G Universe, focuses on evolution.  This creates a virtual, multi-user, evolving universe with both adventurous and scientific aspects.</p>

<p>This combination of interesting scientific research and a virtual online reality is what makes the G System so exciting. Users are thrown into a world that is enjoyable as well as interesting from the aspect of life research, resulting in an evolving universe where the user is plays an important part. Just as important, the current client is a Jabber IM client!</p>

<p>The evolution and life research aspect is an important one as it tries to fill a gap in todays knowledge base. The process is not just about implementing all the physics and maths equations, but about forming a complete world, where the aspects of life itself are implemented. This goal implies many philosophical considerations which are at least as important as actually coding the application.</p>

<p>It also opens the project to a group of people not usually concerned with software development, those interested in philosophy. People who study philosophy are invited to join the project, as well as coders, documentation writers, artists and translators. Financial contribution is also gratefully accepted since it allows us to spend more time on this project and to pay server hosting bills.</p>

<p>Apart from such contributions the most important contributors are the users themselves. Please do not hesitate to give us constructive feedback.</p>

<h3>The First Universe</h3>

<p>With the recent 0.5 release of the G System we have started our first virtual universe. Now it needs to be filled with users. If you want to give it a try, simply download the package and install the G System.  This release features a graphical installer so everyone should be able to manage. If you encounter any problems do not hesitate to ask us on our mailing lists. If you encounter no problems you are still welcome to join the mailing lists as well of course.</p>

<p>To disspell a common mis-conception, currently you can only travel around and talk to each other in this virtual universe, not much else can happen yet. However, the framework is quite advanced and will allow for additional functionality.</p>

<p>As this 0.5 release is still a development release users are highly advised to subscribe to the users mailing list to get notifications about important changes, server downtimes and other information.</p>

<p>Team Coordinator Raphael has also set up his own blog which discusses design decisions and general development of the G System, among other things. You are welcome to add feedback, discuss further development and philosophical considerations through comments.</p>

<h3>Links<h3>

<ul>
<li><a href="http://www.g-system.at">Homepage and Download</a></li>

<li><a href="http://www.g-system.at/page_doc_api.php">Documentation</a></li>

<li><a href="http://www.g-system.at/page_doc_maillist.php">Mailing Lists</a></li>

<li><a href="http://www.g-system.at/page_doc_server.php">G Universe server status</a></li>

<li><a href="http://www.kde-apps.org/content/show.php?content=14721">KDE-APPS entry</a></li>

<li><a href="http://raphael.g-system.at/blog">Raphael's blog</a></li>
</ul>