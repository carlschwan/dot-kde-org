---
title: "KDE.ie on Software Patents"
date:    2005-03-19
authors:
  - "bo'donovan"
slug:    kdeie-software-patents
comments:
  - subject: "Very good"
    date: 2005-03-19
    body: "Europe lacks a community portal for such a campaign. Your open lobbying is a very good individual effort and I would like to see others to join in. The Mplayer site is currently closed for protests. "
    author: "aranea"
  - subject: "Re: Very good"
    date: 2005-03-19
    body: "http://www.ffii.org/ ?"
    author: "Rene Rebe"
  - subject: "Re: Very bad"
    date: 2005-03-19
    body: "What would it change even if every opensource site would be closed for a couple of weeks? Nothing!?"
    author: "skeptic"
  - subject: "Re: Very bad"
    date: 2005-03-19
    body: "If it would be Open Source friendly Slashdot, there would be immediate international uproars. ;-)"
    author: "Anonymous"
  - subject: "Re: Very bad"
    date: 2005-03-19
    body: "Indeed.  I think grandparent is suggesting that the protesters attempt to take down proprietary websites like Microsoft.com or Google.com and cause much more inconvenience to the public that way."
    author: "ac"
  - subject: "kde.xx"
    date: 2005-03-19
    body: "Is there a list of country-specific KDE sites?"
    author: "AC"
  - subject: "Re: kde.xx"
    date: 2005-03-19
    body: "http://www.kde.org/international/"
    author: "Barry O'Donovan"
  - subject: "How to act. "
    date: 2005-03-21
    body: "Thanks Barry,\n\nI do salute this dedication.\n\nThe problem here is the process involved at the European Parliament, which is relying on MEPs votes only.\n\nIt doesn't sound very democratic but it's the way it is. It's the reason why there are thousands of lobbying offices around the parliament, acting on behalf on big companies (including American ones). These people are highly paid to get results and are actively lobbying MEPs, on a daily basis.\n\nPatents is only one item of their portofolio. I saw a TV magazine on the Erika oil disaster in Spain. Nowadays, oil tankers business is auction-based: the cheaper the winner. That's why there are still so many unsafe tankers sailing around the world and oil disasters are around the corner. So far, these lobbies have succeeded to block any law related to safer (but more expensive to build) boats.\n\nBe assured that for patents they are as good.\n\nSince MEPS are the key, the way to go, is to directly contact them. To do so, a  site (which one I don't know. Maybe: www.ffii.org) should provide a list of MEPs per country and how to contact them (Email, Phones). Each of us who feel responsible should either speak/write to them or send a \"white paper\" similar as the one made by the Irish community (translated in your own language).\n\nAs explained by them, the goal is to get them to be there the D.day and to vote \"NO\".\n"
    author: "Philozen"
  - subject: "Re: How to act. "
    date: 2005-03-21
    body: "Hi Philozen and thank you for your reply.\n\nI just want to clarify one or two points.\n\n\"Since MEPS are the key, the way to go, is to directly contact them.\"\n\nIt's not clear from your post whether that's directed at me or at others planning a similar action. For the record, we did directly contact our MEPs (Ireland and Northern Ireland). The briefing document is a direct result from queries from MEPs after our targeted e-mail campaign. More information can be found in replies to a journalists questions at:\n    http://www.kde.ie/patents/statements/eweek.com.php\n\nYou'll find a list of MEPs by country and political grouping at:\n    http://wwwdb.europarl.eu.int/ep6/owa/p_meps2.repartition?ilg=EN&iorig=home\n\nIf you are going to mail them, try and use their \"local\" address; i.e. an address in their constituency which should be found through the above page.\n\nPersonally, I wouldn't refer to our document as a \"white paper\" but rather a \"briefing document\" as it was titled; \"white paper\" has different connotations. \n\n\"As explained by them, the goal is to get them to be there the D.day and to vote \"NO\".\"\n\nNo it's not. As our conclusion clearly states:\n> The authors of this document and their respective organisations \n> listed below would implore all MEPs to ensure that only a directive \n> which unambiguously ensures that software ideas and business methods \n> are not patentable in the EU is passed into law; one which allows \n> for patenting of algorithms and business methods is unacceptable.\n\nWe are not necessarily against the directive; just one that allows the patenting of software and business methods. Our preference would be for the directive to pass with amendments ensuring the above. Our second preference would be for the directive to be defeated.\n"
    author: "Barry O'Donovan"
  - subject: "Re: How to act. "
    date: 2005-03-24
    body: "Barry, sorry for the late reply and for not being clear. English isn't my native language.\n\nMy post is directed to anybody whising to help. As told, I really appreciate what's you're doing.\n\nThe issue here is that, as an individual, I feel it's very difficult to influence the vote and I feel frustrated. It's a vote of huge impact and I'm powerless.\n\nAfter many thoughts, I found out that there's only one way out to express myself. To contact directly the MEPs of my country, in a similar way of yours. Thanks to your post I know how to do it now.\n\nConcerning my conclusion, I missed your 2 preferences. Sorry.\n\nAs for myself, I'd prefer the second one since it's a tough technical subject, difficult to explain to non-specialists, like most MEPs (hence your briefing document). To go for amendments will complicate the job, in my opinion.\n\n\n"
    author: "Philozen"
---
<a href="http://www.kde.ie/">KDE Ireland</a>, the committees of the Irish Linux Users' Group and the 
Irish Free Software Organisation, on behalf of their members, <a href="http://www.kde.ie/patents/">have sent</a> <a href="http://www.kde.ie/patents/briefing/">a 
briefing document</a> to all Irish MEPs to explain the issues surrounding Software Patentability &amp; EU Directive COD/2002/0047 as we see them and to impress upon the MEPs the importance of voting against this directive in its current form. 



<!--break-->
<p>
Please feel free to <a href="mailto:barry.odonovan@kdemail.net">contact me</a> for any more information or for pointers on how to <em>correctly</em> mount a similar campaign in your own E.U. country.

