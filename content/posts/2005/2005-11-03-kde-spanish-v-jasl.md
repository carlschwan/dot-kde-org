---
title: "KDE in the Spanish V JASL "
date:    2005-11-03
authors:
  - "pmaqueda"
slug:    kde-spanish-v-jasl
comments:
  - subject: "Just what I need! :-)"
    date: 2005-11-03
    body: "\"BulmaG\u00e9s is an Open Source accounting and invoicing application designed for the Spanish (i.e. from Spain) accounting system that pretends to solve all needings for small & medium companies. designed for Linux OS\""
    author: "ac"
  - subject: "Re: Just what I need! :-)"
    date: 2005-11-03
    body: "And IAS based accounting? Is it also supported?"
    author: "gerd"
  - subject: "Re: Just what I need! :-)"
    date: 2005-11-03
    body: "Yeah, I love the \"pretends\" part as well. I am sure that it underwent babblefish with a  little bit of clean up. Hopefully, it is there, and works."
    author: "a.c."
  - subject: "Re: Just what I need! :-)"
    date: 2005-11-04
    body: "The pretends part should be \"intends\".\n\nIt's a common mistake of Spanish speakers to confuse \"pretender\" with \"to pretend\".\n"
    author: "Gonzalo"
  - subject: "Re: Just what I need! :-)"
    date: 2005-11-04
    body: "Well, I bet Pedro is sure embarassado!"
    author: "JSinger"
---
KDE will be present in the next <a href="http://jornadas.adala.org">Jornadas Andaluzas de Software Libre</a> (Andalusian Free Software Conference), an annual event sponsored this year by the <a href="http://www.juntadeandalucia.es">Junta de Andalucía</A> (Andalusian goverment), the <a href="http://www.uco.es">University of Cordoba</a> and many others. This event takes place in Córdoba, Spain this coming weekend (4th, 5th and 6th November) with a strong KDE presence.  KDE related talks include Kiosk mode and KDE-Guadalinex. 




<!--break-->
<p>The free conference program features the next talks:</p>

<ul>
<li><b><a href="http://www.linuxjournal.com/article/7718">KDE Kiosk Mode</a></b> by <i>Pedro Jurado Maqueda</i>, an introduction to this framework to customize and lock down some features of KDE</li>

<li><b><a href="http://sourceforge.net/projects/bulmages">Bulmages</a> Accounting Software for KDE</b> by <i>René Mérou and Tomeu Borrás</i> Bulmages is an Open Source accounting and invoicing application designed for the Spanish  accounting system that hopes to solve all the needs for small &amp; medium companies. It is based on the Qt and KDE libraries.</li>

<li><b><a href="http://freenx.berlios.de">FreeNX</A>, the next remote desktop revolution</b> by <i>Pedro Jurado Maqueda</i>, a brief introduction to NX technology and its applications.</li>

<li><b><a href="http://www.guadalinex.org">KDE-Guadalinex</a> KDE for Guadalinex</b> by <i>Pedro Jurado Maqueda:</i> Guadalinex is the tailored distro made by the Andalusian Goverment for schools and home users.  The idea is, taking advantage for the next release of Guadalinex based in Ubuntu, to use slighted modified <a href="http://www.kubuntu.org">Kubuntu</a> packages to make create a KDE-Guadalinex.</li>
</ul>

<p>There also many more interesting talks during the conference, please consult the <a href="http://adala.org/encuentros/jasl5/programa.html">schedule</a> (only in Spanish) for more information.</p>




