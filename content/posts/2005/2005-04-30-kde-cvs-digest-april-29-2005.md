---
title: "KDE CVS-Digest for April 29, 2005"
date:    2005-04-30
authors:
  - "dkite"
slug:    kde-cvs-digest-april-29-2005
comments:
  - subject: "First post"
    date: 2005-04-30
    body: ":-)"
    author: "Metamatics"
  - subject: "f"
    date: 2005-04-30
    body: "Always fun to read.\n\nFun to see kexi and amarok use knewstuff, although we might have to be careful about security since anyone can upload scripts.\n\n"
    author: "JohnFlux"
  - subject: "Re: f"
    date: 2005-04-30
    body: "Kexi has scripts disabled before 1.0 version, so don't worry. And we're planning to use a sandbox system for better security.\n"
    author: "Jaros\u00b3aw Staniek (Kexi Team)"
  - subject: "imap filters"
    date: 2005-04-30
    body: "Good to see that the filters in kmail can now be applied on imap folders too.\n"
    author: "Richard"
  - subject: "Re: imap filters"
    date: 2005-05-01
    body: "Only two revisions behind schedule.  3.3, then 3.4 and now most likely what, 3.4.1?"
    author: "Marc Driftmeyer"
  - subject: "Re: imap filters"
    date: 2005-05-01
    body: "I was thinking 3.5\n\nKay"
    author: "Debian User"
  - subject: "Re: imap filters"
    date: 2005-05-01
    body: "Its no problem to apply filter to dIMAP folders - but stuff like filter incoming mails does not work if these mails are server side filtered by e.g. sieve. I hope the patch helps to solve this bug and will be backported to 3_4_BRANCH :-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: imap filters"
    date: 2005-05-01
    body: "Which bug? Why should messages which have already been moved to the correct folder by the mail server be filtered again? That sounds like a pretty stupid idea, especially if you think of two differently configured email clients which ping-pong some messages between different folders.\n\nFor your information: Don's patch only filters new messages in the IMAP account's INBOX, just as with dIMAP.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: imap filters"
    date: 2005-05-02
    body: "Because, if a person wants filtering done on their imap mail, they very likely DONT have server side filtering to begin with.  The only imapd that properly supports sieve is cyrus, which, in my experience is not all that common...."
    author: "Joe Kowalski"
  - subject: "Re: imap filters"
    date: 2005-05-02
    body: "You misunderstood me. If a person doesn't use server side filtering then all his new mail will be in his IMAP account's INBOX and will thus be filtered. That's exactly what you want."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: imap filters"
    date: 2005-05-02
    body: "I use it for header rewriting cause this is easy with kmails filter engine.\nBut I can not apply this rule to new incomming mails into this folder. So I have to select new mails by hand and apply filter manually.\nIIRC these rewrited headers are not synced back to the server.\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "A small bug report"
    date: 2005-05-01
    body: "There are two sections: \"Development Tools\" and \"Development tools\", of which the latter feels unnecessary.  :-)"
    author: "Inge Wallin"
  - subject: "Re: A small bug report"
    date: 2005-05-01
    body: "...and same for Kde-Base and Kde-base."
    author: "Inge Wallin"
  - subject: "Re: A small bug report"
    date: 2005-05-01
    body: "Ugh. Something about doing this at 1:30 am...\n\nDerek"
    author: "Derek Kite"
  - subject: "KDE 4"
    date: 2005-05-01
    body: "Has anyone started porting to Qt 4 or is that going to wait until Qt4 is final?"
    author: "ac"
  - subject: "Re: KDE 4"
    date: 2005-05-01
    body: "The KDE 4 branch is not open yet, as far as I know. "
    author: "Ian Monroe"
  - subject: "Re: KDE 4"
    date: 2005-05-01
    body: "Some information about Qt4 here:\nhttp://www.kdedevelopers.org/node/view/1005"
    author: "ac"
  - subject: "kopete to support msn webcam"
    date: 2005-05-01
    body: "This feature (which will hopefully be available in KDE 3.5) will greatly help new users coming from windows feeling more comfortable!"
    author: "ac"
  - subject: "Re: kopete to support msn webcam"
    date: 2005-05-02
    body: "Olivier Goffart is coding the protocol part, and I started to send patches for the video capture support. I hope we'll have a working, useful code sooner than expected, at least for Linux (V4L and V4L2 APIs). Some help from BSD people will be very welcome."
    author: "Cl\u00e1udio Pinheiro"
  - subject: "KDevelop KDE templates tested"
    date: 2005-05-01
    body: "While I am in the top 10 committers, no work from me is mentionned in the digest (I guess my logs should be longer) so there is a summary of what I did last week. I tested all KDE C++ KDevelop templates and made a doc about them. \nhttp://women.kde.org/articles/tutorials/kdevelop_templates/index.php\nIn the process, I had fixed the ones that were broken and I updated the screenshots in the project wizard.\nI also worked more on the new KHangMan design and used valgrind on KLettres -> fixed a memory leak.\n\n"
    author: "annma"
  - subject: "Re: KDevelop KDE templates tested"
    date: 2005-05-02
    body: "Hey, thanks annma\nthe tutorial was quite useful for someone new to kde programming like me\nhow about adding a link to it on developer.kde.org?\n\nAnd.. a little OT: i think here\nhttp://developer.kde.org/documentation/misc/whatisscripty.php\nsomeone forgot a </h2> (at the end of the page)  ;)"
    author: "Anonymous"
  - subject: "Re: KDevelop KDE templates tested"
    date: 2005-05-02
    body: "Just took a quick look at the template doc. Very nice work. Thanks! :)"
    author: "teatime"
  - subject: "Konqueror still doesn't emit JS onclick on Enter"
    date: 2005-05-02
    body: "\nCould you PLEASE take some action regarding this bug. It's so silly and I think really many users suffer because of this:\n\nhttp://bugs.kde.org/show_bug.cgi?id=91652"
    author: "ph"
  - subject: "Re: Konqueror still doesn't emit JS onclick on Ent"
    date: 2005-05-05
    body: "\"...and I think really many users suffer because of this\"\n\nStandard phrase that everyone's using to promote his/her pet bug. Yawn. Posting this in an unrelated article gives bonus points, though."
    author: "tom"
---
In <a href="http://cvs-digest.org/index.php?newissue=apr292005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=apr292005&all">all in one page</a>):
<a href="http://amarok.kde.org/">amaroK</a> and <a href="http://www.koffice.org/kexi/">Kexi</a> support KNewStuff for database examples, context themes and amaroK scripts.
amaroK adds support for Helix multimedia backend.
<a href="http://kmail.kde.org/">KMail</a> filters now can be applied to messages from IMAP accounts.
KWifiManager implements switch network from GUI feature.
<!--break-->
