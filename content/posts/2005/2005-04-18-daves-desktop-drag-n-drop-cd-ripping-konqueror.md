---
title: "Dave's Desktop: Drag 'n' Drop CD Ripping with Konqueror"
date:    2005-04-18
authors:
  - "David"
slug:    daves-desktop-drag-n-drop-cd-ripping-konqueror
comments:
  - subject: "Best feature ever"
    date: 2005-04-18
    body: "Ah, great to see some more exposure for what I have always thought to be the best KDE feature ever, or at least the best example of KIO's power.\n\nShame the article doesn't mention the KIO iPod slave:\n\nhttp://kpod.sourceforge.net/ipodslave/\n\n(and also a shame that KIO slave doesn't do photos yet)"
    author: "Rob"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "My apologies for not mentioning the iPod slave.  However, I don't own an iPod and never really knew about this feature.\n\nI can only write about things I know about and have learned how to do myself.\n\nBut, thanks for taking the time to read and for the suggestion.  Perhaps in a future How-To.... now all I need is someone to send me an iPod to try it out on! ;o)"
    author: "David"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "Ah, since you mentioned the iPod within the first ten words I figured you had one!"
    author: "Rob"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "I see a lot of them.... just can't afford the expense.  The kids needed shoes ;o)"
    author: "David"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "Who needs shoes anyway? ;-)"
    author: "Barefooted"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "Who needs an Ipod? "
    author: "Andr\u00e9"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "Perhaps someone that needs a good music player."
    author: "blacksheep"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "mp3 players are cheap"
    author: "Andr\u00e9"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "If I did buy a high storage media player, I'd most likely go with an iRiver.  They play OGGs, which is what most of my music collection is now-a-days."
    author: "David"
  - subject: "Re: Best feature ever"
    date: 2005-04-18
    body: "Or an IAudio"
    author: "@nonymouse"
  - subject: "Re: Best feature ever"
    date: 2005-04-19
    body: "The new iAudio X5 seems to be the iPod Photo done right! (Support for ogg, USB Mass Storage-compliant syncing, movies, and USB Host photo syncing for the camera.) I'll buy one next week. Can Amarok (or any other playlist management app) auto-sync my favourites the same way as iTunes can?"
    author: "Jonas"
  - subject: "Re: Best feature ever"
    date: 2005-04-23
    body: "I -think- so, but as I don't have an iPod/etc. either I don't know for sure.  There is a 'media device' tab on amarok so it is quite likely true."
    author: "Quinn"
  - subject: "Re: Best feature ever"
    date: 2005-04-19
    body: "It's not the best player around but it's proper bling bling and rather cheap starting at \u00a369. Thinking of getting the new 60GB Photo model myself, although it's rather useless to get a Photo model until kio_ipod supports photos."
    author: "Rob"
  - subject: "shameless plug :-)"
    date: 2005-04-18
    body: "This procedure is now documented as part of the official KDE User Guide.\nRead here: http://docs.kde.org/en/3.4/kdebase/userguide/audio-cd.html"
    author: "deepak"
  - subject: "Re: shameless plug :-)"
    date: 2005-04-18
    body: "That's great!  The more exposure of these wonderful KDE functions, the better."
    author: "David"
  - subject: "Re: shameless plug :-)"
    date: 2005-04-18
    body: "Also feel free to submit a promo/dot article on new KDE stuff documented..."
    author: "Navindra Umanee"
  - subject: "Damaged Tracks"
    date: 2005-04-18
    body: "In my ripping experiences on three different systems, ripping directly to MP3/OGG does not work. It leads to lots of reading errors from the drive. I can only get perfect rips when I rip to uncompressed audio and compress it after ripping is finished... Maybe these systems are too slow to do ripping and encoding at the same time. All three systems are Athlon 800-1000 MHz."
    author: "Meneer Dik"
  - subject: "Re: Damaged Tracks"
    date: 2005-04-18
    body: "I also get reading errors, on an athlon64 3400, 64bit gentoo. Don't know why :S"
    author: "efegea"
  - subject: "Re: Damaged Tracks"
    date: 2005-04-18
    body: "In 4.1 the ripping error messages will be surpressed as it isn't much to worry about.  But for those of you who like them there will be an option to enable it :)\n\n-Benjamin Meyer\naudiocd maintainer"
    author: "Benjamin Meyer"
  - subject: "Re: Damaged Tracks"
    date: 2005-04-19
    body: "Try using kaudiocreator instead then."
    author: "Ian"
  - subject: "It's neat"
    date: 2005-04-18
    body: "This feature is so neat! A couple of days ago I bought a brand new Yepp portable player (which supports ogg!!), inserted it into an USB port, inserted an audio CD in the DVD drive, opened up two Konqui windows with 2 clicks on their respective icons in the kicker (thanks Media applet!), and d'n'd the \"ogg\" folder from the CD to the portable player. 30 mins later, it was done. Everything tagged well, everything ordered well! \n\nThe only problem with the audiocd:/ slave is that is missing the possibility to configure the album folder name for ogg/mp3/flac subfolder. There was an opened bug on BKO but is marked as closed while it isn't. I added a comment to it cause I was looking for the same exact options and I didn't dare to open a new BR but no one replied...let's hope that with some noise here I will get attention :)\n\nthe BR is\nhttp://bugs.kde.org/show_bug.cgi?id=40272"
    author: "Davide Ferrari"
  - subject: "When to use"
    date: 2005-04-18
    body: "As with all tools there are cases when the audiocd:/ slave are not the best tool for the job. It really shines if you want to do quick and simple pick and choose of songs from CD's or as you described d'n'd to USB devices. But if you want to rip one or more whole CD's the audiocd:/ slave has it's weakness, mostly in case of speed. For those big jobs KAudioCreator are a better suited tool."
    author: "Morty"
  - subject: "Re: When to use"
    date: 2005-04-18
    body: "Well, but id doesn't cost anything add support to virtual folders simply modifing the pathname as I described I tried to do in the BR.\nAnd anyway if the KIO method has some flaws, I think the best solution would be fix them, not create (or encourage) alternative KDE solutions, cause with the audiocd:/ KDE is miles ahead all others competitors, usability wise.\n\n(if you didn't get it, I really do love this feat. :)"
    author: "Davide Ferrari"
  - subject: "Re: When to use"
    date: 2005-04-18
    body: "KAudioCreator uses kio_audiocd internally for ripping to WAV before it encodes it using one of the configured encoders."
    author: "floyd"
  - subject: "Still slow and buggy"
    date: 2005-04-18
    body: "I like the idea. The first time I saw this I thought it was the way it has to be done. Is like all people want it. If you want to copy the songs of a cd, then copy and paste it in the explorer window.\nBut in my personal use I've found it's very slow (in general, kio_slaves are slow) compared to other windows 'comercial' solutions. I think is lame related but I've seen how slow the kamera kioslave is so maybe it's konqueror's fault.\nAlso, when using varianble bitrates with mp3 I get wrong times. For example, a song of 3 min is recognized by amarok and other players (even in windows) like a song of 23 min. Another time, maybe lame related. Maybe it woulb be great to have the option of choosing the mp3 encoder in the configuration."
    author: "Jose"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-18
    body: "> Also, when using varianble bitrates with mp3 I get wrong times. For example,\n> a song of 3 min is recognized by amarok and other players (even in windows)\n> like a song of 23 min. Another time, maybe lame related. Maybe it woulb be\n> great to have the option of choosing the mp3 encoder in the configuration.\n\nThis probably means that the encoder does not write the XING header. This header contains the tracklength, and is crucial for precise length determination with VBR-encoded mp3.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-18
    body: "That's why I say that encoder should be chosen in the audiocd configuration.\nMaybe lame is buggy or audiocd does not tell it to write that header or something like that."
    author: "Jose"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-18
    body: "The kioslave interface unfortunately can not seek, so there is no way to go back and write a precise header later. This means the encoded files are really only suitable for streaming and not for storing."
    author: "Carewolf"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-18
    body: "> The kioslave interface unfortunately can not seek\n\nYes, and this is a major shortcoming of KIO. Makes it quite unsuitable for multimedia uses.\n"
    author: "Mark Kretschmann"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-19
    body: "> This probably means that the encoder does not write the XING header.\n\nkcontrol-> \"Sound & multimedia\"-> \"Audiocds\"-> \"MP3 Encoder\"-> \"Write Xing VBR tag\"\n\nCharles\n"
    author: "Charles Philip Chan"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-18
    body: "Yup there is a bug open for the variable bit rate problem.  Unfortunettly it isn't an easy fix so I don't think I will be able to fix it for 4.1\n\n"
    author: "Benjamin Meyer"
  - subject: "Re: Still slow and buggy"
    date: 2005-04-18
    body: "Is it a lame bug or a konqueror bug?"
    author: "Jose"
  - subject: "same thing for DVD"
    date: 2005-04-18
    body: "Why not do the same thing for DVD?\n\n1)You put a DVD video\n2)In Konqueror, you have several folder:\n  - DVD5\n  - DVD9\n  - Divx/Xvid\n  - ...\n3)drag and drop the divx, the DVD is encoded and you obtain a .avi file\nor 4) drag and drop the DVD5, the MPEG2's DVD is re-encoded to fit to a DVD5 like the Windows software DvdShrink.\n\nIs not the idea good?"
    author: "Ollivier Lapeyre Johann"
  - subject: "Re: same thing for DVD"
    date: 2005-04-18
    body: "This would only be useful in places where deCSS (or libdvdread) is legal to use.  It would likely be stripped out of most mainstream distributions."
    author: "Joe Kowalski"
  - subject: "Re: same thing for DVD"
    date: 2005-04-18
    body: "legal stuff this should not hold back development\nof great technologies!\n\nGo konqi, go!"
    author: "Pieter"
  - subject: "Re: same thing for DVD"
    date: 2005-04-18
    body: "I've always wondered how the licensing for DVD playing software works.  Would it be possible to buy KDE a license so they could distribute a feature like this?  "
    author: "Leo Spalteholz"
  - subject: "Re: same thing for DVD"
    date: 2005-04-18
    body: "only if we would make kde closed source and let every \"user\" pay a fee for it's usage. and more other ugly things."
    author: "Mark Hannessen"
  - subject: "Re: same thing for DVD"
    date: 2005-04-19
    body: "Sounds reasonable...   No?  ;)"
    author: "Leo Spalteholz"
  - subject: "Re: same thing for DVD"
    date: 2005-04-19
    body: "I find it amusing that you are so bold with other people's possible legal liability issues.\n\n"
    author: "Roberto Alsina"
  - subject: "Legal DVD copying uses abound."
    date: 2005-04-19
    body: "I appreciate observing what legal issues lie in wait for users around the world, but I also appreciate that not all users have the same limitations imposed on them (not every country observes software patents, for instance).  Free software hackers should not buttress controlling multinational publishers by withholding copying programs like the poster described.  Even the USA has the Betamax case which echoes the sentiment I'm trying to convey.\n\nAs a more constructive criticism, may I recommend making it super-easy to create Ogg Theora+Ogg Vorbis copies of movies ripped from DVD?  I'm sure there are plenty of users with home movies they'd like to send to their free software-running family members.  All the free software movie players support Ogg Theora+Ogg Vorbis movies, as far as I know.\n\nSince there are perfectly legitimate uses for copying DVDs, it would be perfectly fine to give this functionality to everyone and (just like with audio CDs) let them choose to obey the law by not copying DVDs they don't have a license to copy.\n\nWe can trust that users know when to police themselves and observe copyright law."
    author: "J.B. Nicholson-Owens"
  - subject: "Re: Legal DVD copying uses abound."
    date: 2005-04-19
    body: "Sure. It's simply not my decision to make, or yours, since it's not our liability.\n\nUnless you are willing to be the one that writes and distributes such code, thus becoming the responsible party, it's still being brave with other people's risk."
    author: "Roberto Alsina"
  - subject: "Re: same thing for DVD"
    date: 2005-04-18
    body: "Or for people with non-encrypted DVD collections\n(Most B-Movies, like kung fu dvds or gore flicks are not region-encoded and not encrypted...)"
    author: "ac"
  - subject: "Re: same thing for DVD"
    date: 2005-04-19
    body: "Or just people's personal DVD's."
    author: "Ian"
  - subject: "Re: same thing for DVD"
    date: 2005-04-20
    body: "Xine uses libdvdcss if it's available and it's perfectly legal. \nKonqueror could make the same. If libdvdcss is installed then you can copy encrypted dvd, if you don't have it, then you can only rip unencrypted dvd.\nThat way, konqueror would not be illegal. It's the user that install libdvdcss that has the problem of decide."
    author: "Jose"
  - subject: "Audio CD Ripping"
    date: 2005-04-18
    body: "just wondering.\n\nrecording seems to be very slow.\nI have actually never seen it rip fast.\nthis gives me the idea that it is ripping the \"anolog\" way.\nif this is so.. wouldn't it be possible to digitally rip the audiocd?\n\nthat would probably provide a much better experience."
    author: "Mark Hannessen"
  - subject: "Re: Audio CD Ripping"
    date: 2005-04-19
    body: "audiocd:/ uses cdparanoia which does it the digiatal way + it adds a *lot* of checking, since most cd-rom drives has buggy cdda hardware. It is those \"paranoia checks\" which leads to a good deal of re-reading. Technically you can set cdparanoia up to *never *ever* go for anything less then perfect, which in terms again will wear your drive out if you attempt to rip a badly scratched cd.... been there, done  that :-/\n\n~Macavity"
    author: "Macavity"
  - subject: "Flash KDE 3.4 Feature Guide and Screenshots"
    date: 2005-04-19
    body: "LOL\n\nSee what I mean about KDE's marketing.\n\nThis is getting pathetic and ridiculous. More than a month after the erlease of KDE 3.4, the promised Flash feature guide is nowhere to be found and the screenshot  section shows dated 3.3 shots.\n\nWay to go on selling this release to the Linux community!"
    author: "Al"
  - subject: "Re: Flash KDE 3.4 Feature Guide and Screenshots"
    date: 2005-04-19
    body: ">Way to go on selling this release to the Linux community!\n\n\"selling\"? \"pathetic\"? What part of \"free\" and \"volunteers\" you don't get?"
    author: "Amadeo"
  - subject: "Re: Flash KDE 3.4 Feature Guide and Screenshots"
    date: 2005-04-19
    body: "Apparently Al is missing the part where you volunteer yourself."
    author: "Ian Monroe"
  - subject: "Re: Flash KDE 3.4 Feature Guide and Screenshots"
    date: 2005-04-19
    body: "LMAO!\n\n That always seems to be the case with the loud-mouths ;-)\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Flash KDE 3.4 Feature Guide and Screenshots"
    date: 2005-04-19
    body: "Drag'n'drop CD-ripping is by far no new feature of KDE 3.4."
    author: "Anonymous"
  - subject: "Re: Flash KDE 3.4 Feature Guide and Screenshots"
    date: 2005-04-19
    body: "I would gladly pay if KDE could deliver on the marketing front.\n\nBut it seems my $20/year donations are not going to help much unless more people do the same."
    author: "Al"
  - subject: "Kdevelop article on Newsforge"
    date: 2005-04-19
    body: "Sorry, didn't know where to send this, so I'll post here:\n\nhttp://programming.newsforge.com/programming/05/04/12/1357202.shtml?tid=48&tid=140\n\nInteresting comparison between KDevelop and Visual Studio."
    author: "Darkelve"
  - subject: "Re: Kdevelop article on Newsforge"
    date: 2005-04-20
    body: "there is a 'post article' on this page (and all other dot page, you can use contribute too).\n\nas you might know, type just in konqi (when focussed on the webpage): / post article"
    author: "superstoned"
  - subject: "Re: Kdevelop article on Newsforge"
    date: 2005-04-20
    body: "Pretending you're serious about what you just said: \nOr better even, type 'post  (single quote, post)\n\nThat searches only for links.  \n\n"
    author: "cm"
  - subject: "Re: Kdevelop article on Newsforge"
    date: 2005-04-20
    body: "hey, that's cool! I did not know that... thanx!"
    author: "superstoned"
  - subject: "mp3-option gone in KDE 3.4?"
    date: 2005-04-28
    body: "i have been using this feature of KDE with great pleasure sinds many months. but now, with KDE 3.4, i cannot get an mp3 folder in konqueror anymore. though i installed lame on both suse 9.3 and kubuntu, i do not get an mp3-tab in the settings of KDE. does anyone know the reason?"
    author: "bjorn"
  - subject: "can you help me "
    date: 2005-12-17
    body: "i have got a drag n drop cd + dvd i am try to copy so file of one disc to another but it is not work of some resaon it comes up with the files were not added .(code:011-02-1048) F:\\MOVIES\\VCNTSC.PSS can you help ???? thanks tom "
    author: "tom"
  - subject: "Re: can you help me "
    date: 2006-09-25
    body: "I've got the same problem and finally got it solved. Just right click on Data, then clear layout, and make sure on \"option\" (under the \"Data CD Option\"), File system is JOLIET and CD-ROM Mode is Mode2. Good luck. Amy"
    author: "Amy"
---
Ripping tracks from a CD into OGG or MP3 is one of the most popular things we do with our home PCs these days.  This How-To shows you how you can do this task quickly and easier than ever before, with the help of KDE and <a href="http://www.konqueror.org/">Konqueror</a>. Read the complete article at <a href="http://www.virtualsky.net/daves/2005-08.htm">Dave's Desktop - Ep. 21</a>.



<!--break-->
