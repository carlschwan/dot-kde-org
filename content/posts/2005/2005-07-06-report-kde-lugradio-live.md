---
title: "Report: KDE at LugRadio Live"
date:    2005-07-06
authors:
  - "blamb"
slug:    report-kde-lugradio-live
comments:
  - subject: "Print to PDF"
    date: 2005-07-05
    body: "That feature is great.  I use it all the time to print the \"order complete\" receipt when I purchase stuff online.  \n\nKeep up the great work!"
    author: "am"
  - subject: "Re: Print to PDF"
    date: 2005-07-05
    body: "Print to PDF is absurd, it must be \"Save as PDF\" in Print dialog and in the SAVE AS dialog, no new user could know that s/he can create a pdf file by PRINTING. It's like all Great hidden features of KDE :) No offence though."
    author: "fast_rizwaan"
  - subject: "Re: Print to PDF"
    date: 2005-07-05
    body: "\"Save as PDF\" would be wrong as it implies that you can recover your original data (a.k.a. loading).  But that's not the case with PDF (the KWord PDF input filter doesn't count here). \n\nIf anything it qualifies as \"lossy export\". "
    author: "cm"
  - subject: "Re: Print to PDF"
    date: 2005-07-06
    body: "Well, then it must be \"Export to PDF\". No one expects an export filter to load the exported data except if there is an appropriate importer."
    author: "Jakob Petsovits"
  - subject: "Re: Print to PDF"
    date: 2005-07-06
    body: "Couldn't agree more! What about \"Save/Export to PDF?\""
    author: "cb"
  - subject: "Re: Print to PDF"
    date: 2005-07-06
    body: "Why not \"Save/Print/Export to PDF\" then? scnr =P"
    author: "ac"
  - subject: "Why C++ is not cool?"
    date: 2005-07-06
    body: "I've been reading Jonathan's slides and I'm curious as why is C++ listed as a not-so-cool thing? I thought the use of C++ and QT have proved one of KDE's biggest strengths."
    author: "Anonymous"
  - subject: "Re: Why C++ is not cool?"
    date: 2005-07-06
    body: "I didn't attend Jonathan's talk but I would think that his point was about KDE being too much concentrated on C++ currently and there should be more RAD usage of JavaScript, Python, Ruby, ...\n"
    author: "Anonymous"
  - subject: "Re: Why C++ is not cool?"
    date: 2005-07-06
    body: "Outside perception perhaps? (Note that nearly everyone who thinks C++ is cool must be using or developing in Qt/KDE already. People from outside lack that kind of insider knowledge.)"
    author: "ac"
  - subject: "Re: Why C++ is not cool?"
    date: 2005-07-06
    body: "Well, compared to pure C, everything is cool. \n\nIn a perfect world, every application developer could choose the language he likes and would find perfect bindings for his language. Unfortunately we don't live in such a world ;-)\nWith qtruby and korundum there are very good bindings for programming KDE applications in Ruby though. Let's hope the bindings situation will be improved some day (maybe KDE4?).\n"
    author: "lippel"
  - subject: "Re: Why C++ is not cool?"
    date: 2005-07-06
    body: "The bindings situation is already very good; Python, Ruby, Java bindings -- what more would you want?"
    author: "Boudewijn"
  - subject: "Re: Why C++ is not cool?"
    date: 2005-07-08
    body: "I, for one, would like to see Ada95 bindings. Ada is being used not only in defense; it is also a good teaching language that has many of the best features of Pascal, and we have a good GCC compiler for it. However, should you decide to develop desktop programs in it, you're forced to use GTK++/GNOME, since KDE bindings are nil."
    author: "Eduardo Sanchez"
  - subject: "Re: Why C++ is not cool?"
    date: 2005-07-08
    body: "Ada! That's... Exotic. The closest I've ever come to Ada was PL-SQL, which\nis Ada-derived."
    author: "Boudewijn Rempt"
  - subject: "Win32 applications?"
    date: 2005-07-06
    body: "In a point about KDE4 from the slides, it says:\nWin32 Applications\n\nDoes anyone know what that means?  KDE will integrate more with Wine or what?"
    author: "Leo S"
  - subject: "Re: Win32 applications?"
    date: 2005-07-06
    body: "I think it's a reference to the possibility that KDE apps will be ported to windows with the release of qt4 as GPL on windows."
    author: "ltmon"
  - subject: " lead developer"
    date: 2005-07-06
    body: "hmm? \"lead developer\""
    author: "Aranea"
  - subject: "Re:  lead developer"
    date: 2005-07-08
    body: "Yes. Who else?"
    author: "Anonymous"
  - subject: "LUG Radio and Their Gnome Love In"
    date: 2005-07-12
    body: "The guys on LUG Radio are always saying how terrible KDE is (love the show - but they can be totally mindless at times).\n\nYes KDE has it's problems (Konq usability - 2 years in and I still find it tricky to configure...) but all in all it whoops Gnomes ass on so many levels.\n\nThen again I haven't used Gnome that much, so it may not be as bad as I imagine.\n\nJonathan is a brave guy to face the LUG radio crew :)"
    author: "john"
  - subject: "Re: LUG Radio and Their Gnome Love In"
    date: 2005-07-13
    body: "What do you want to configure in Konq?"
    author: "ac"
  - subject: "Re: LUG Radio and Their Gnome Love In"
    date: 2005-07-14
    body: "Nothing in particular - but in general I always dread having to do any configuration in Konq - maybe it's just a psychological thing..."
    author: "john"
  - subject: "Re: LUG Radio and Their Gnome Love In"
    date: 2005-07-14
    body: "Maybe KDE should offer a global configure switch, which allows real time hiding and unhiding all of the applications' configure menu entries and toolbar buttons, thus making the everyday non-configure mode less screaming 'configure me now! since you can', heh..."
    author: "ac"
  - subject: "Re: LUG Radio and Their Gnome Love In"
    date: 2005-07-18
    body: "I want to configure the window size (it should remember it, as all other apps do), and I want it to display the sidebar by default\n\nthis means \"save profile\", which is itself not terribly clear. a regular user would probably never discover that (I mean a regular user who doesn't try all the buttons and post to forums/ search forums instead of just dropping it - i.e. one who isn't a big konq fa, as I am :) ) "
    author: "ac"
---
<a href="http://www.lugradio.org/">LugRadio</a> is a fortnightly podcast covering everything Open Source and last weekend they held the first community run Linux exhibition in England. Jonathan Riddell, lead developer of <a href="http://www.kubuntu.org/">Kubuntu</a>, and Ben Lamb were there to demo the newly released <a href="http://www.koffice.org/">KOffice</a> 1.4, Kubuntu and the upcoming features of KDE 3.5. Jonathan <a href="http://jriddell.org/programs/kubuntu-lugradio-talk-06-2005/">gave a talk</a> about Kubuntu development and KDE 4.

<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; width: 300px"><img src="http://static.kdenews.org/jr/2005-06-25-lugradio-live-jonathan.jpg" with="300" height="225" /><br />The KDE talk was <a href="http://www.david-web.co.uk/blog/?p=115">"one of the most interesting talks of the day"</a></div>

<p>The day was a surprising success with over 250 people, 4 times as many as expected.  Due to some last minute timetable changes the KDE talk was promoted to the main stage.  Jonathan's talk focused on Kubuntu, an easy to install GNU/Linux distribution based around the KDE desktop. It is an ideal way to get the latest version of KDE and several hundred CDs were eagerly picked up by visitors.</p>
 
<p>Ben commented "Visitors were really impressed by how polished KDE applications are. <a href="http://www.koffice.org/krita/">Krita</a>, the new KDE image manipulation program, was very well received and <a href="http://www.koffice.org/kexi/">Kexi</a> also generated a lot of interest."</p>
 
<p>Mark Shuttleworth, founder of Ubuntu, stopped by the KDE stall. We were hoping to get a quote from him but he was speechless after seeing how easy it is to create PDFs from any KDE application and how <a href="http://kpdf.kde.org/">KPDF</a> seemlessly integrates with Konqueror.</p>
 
<p>We were also joined by Rob Taylor who is writing updated GStreamer bindings for KDE and we discussed the relative merits of KOffice against <a href="http://kde.openoffice.org/">KDE integrated OpenOffice.org</a> with <a href="http://www.credativ.co.uk/">Credativ UK</a> founder and Kubuntu developer Chris Halls.  Danny Allan came by to show us his monochrome icon theme, now part of KDE Accessibility and a new recruit was made for Kubuntu when Martin Meredith was persuaded to become the k3b package maintainer, famous as the program required by even the most strict of Gnome users.</p>

<p><a href="http://jriddell.org/programs/kubuntu-lugradio-talk-06-2005/">Slides from Jonathan's talk</a> are available and videos of the days events should be online soon.</p>
