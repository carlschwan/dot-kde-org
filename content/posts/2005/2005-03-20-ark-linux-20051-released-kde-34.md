---
title: "Ark Linux 2005.1 Released with KDE 3.4"
date:    2005-03-20
authors:
  - "ggreene"
slug:    ark-linux-20051-released-kde-34
comments:
  - subject: "really nice"
    date: 2005-03-20
    body: "Looks really, really good. Any problems with SUSE (probably arising from former ximian camp), and i'm going for ark instantly. \n\nKDE is probably the only truly free OSS desktop environment at the moment. It's relatively trivial to set it up with any Linux/BSD system, and you get a decent desktop experience with it out of the box. GNOME, as it is today, requires tons of experienced developer time to polish it up to be good enough for daily use. Shame, really.\n"
    author: "Anonymous Coward"
  - subject: "Re: really nice"
    date: 2005-03-20
    body: "\"KDE is probably the only truly free OSS desktop environment at the moment.\"\n\nEh? Are you living in a cave somewhere? What about GNOME?"
    author: "Mike"
  - subject: "Re: really nice"
    date: 2005-03-20
    body: "You have to sacrifice usability and compatibility in order to use GNOME. :-p"
    author: "Aaron Krill"
  - subject: "Re: really nice"
    date: 2005-03-21
    body: "> Eh? Are you living in a cave somewhere? What about GNOME?\n\nFinish reading the last sentence, thank you."
    author: "Anonymous Coward"
  - subject: "Re: really nice"
    date: 2005-03-21
    body: "Gnome is of course \"truly open source\". But I would agree that KDE is the only choice :-) It just has all the productivity tools that a professional web developer needs - and in my profession, no other DE offers the same level of network integration from the get go.\n"
    author: "m0ns00n"
  - subject: "Re: really nice"
    date: 2005-03-21
    body: "KDE is free Open Source Software, whereas GNOME is only Free Software according to RMS of GNU fame (the G in GNOME).  Please note the subtle distinction.\n"
    author: "ac"
  - subject: "Is it just me..."
    date: 2005-03-20
    body: "Or is using KDE 3.4 a bad idea at this point. I'm running it now, and it seems to me that 3.4 Beta 2 was MUCH more stable than this...\n\nKopete keeps freezing. Konqueror keeps freezing. Memory leaks are everywhere. its a massacre!"
    author: "Aaron Krill"
  - subject: "Re: Is it just me..."
    date: 2005-03-20
    body: "100% you have some buggy install or underlying problem."
    author: "Davide Ferrari"
  - subject: "Re: Is it just me..."
    date: 2005-03-20
    body: "Must just be your installation, but it seems rock solid here."
    author: "Gary Greene"
  - subject: "Re: Is it just me..."
    date: 2005-03-22
    body: "Strange. But these kind of comments seem to appear for every release of KDE. And every time they were wrong, at least on my PC.\n\nI thought all PC:s were fairly compatible. What equipment do these guys use?"
    author: "KDE User"
  - subject: "i'm a developer"
    date: 2005-03-20
    body: "I'm a developer and I'm currently using Mandrake.  I would like to consider Ark Linux for my future distro.  Is it suitable, or only recommended for newbies looking for ease of use?  \n\nI currently use LVM and ReiserFS and have certain requirements for what my partitions look like, etc, etc.\n\nAll in all Ark Linux looks stunning from the screenshots, I just want to figure out if a non-neophyte will be able to tolerate it.  Thanks and good work!"
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-20
    body: "At present the installer doesn't allow for that level of partitioning. I've been designing a rethought out installer, however I can't take care of this until this summer when College lets out. We love to see more developers help us on this distro though."
    author: "Gary Greene"
  - subject: "RE: Problems with Installer Design"
    date: 2005-03-21
    body: "Reminds me of the old phrase:\n  \"Make a product that even an idiot can use, and only an idiot will want to.\"\nWithout an advanced option to install I can not use ARK, even though it looks the best of any distro for my needs."
    author: "jimwelch"
  - subject: "RE: Problems with Installer Design"
    date: 2005-03-21
    body: "I'm working on a new installer, sinc that seems to be the one complaint that I hear over and over. However, I would request that other developers help with this as well, since I'm a college undergrad and cannot devote all my time to working on it. If you're insterested in helping with the installer, email me."
    author: "Gary Greene"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "I'd go for Kubuntu. debian is perfect for developers. need an obscure library? almost sure apt-get has it."
    author: "superstoned"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "Yeah but Debian hasn't had a stable release in years and requires too much fiddling.  A developer does not necessarily want to be a systems administrator in his spare time.  This is why the Ark Linux claim of being easy to use while being technically sane is interesting.  \n\nI don't know about Kubuntu."
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "well, Kubuntu won't be much harder (if at all) to use as ark linux, has a very stable core (debian) but is very up-to-date, and offers all benefits debian has to offer."
    author: "superstoned"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "How can it offer a stable but up-to-date core of Debian when that doesn't even exist?"
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "<rant>\nPlease learn what 'stable', 'testing', and 'unstable' mean within the Debian jargon set before you start to make claims about not having a stable release. I have been running Debian 'testing' for some time now and it is VERY stable and is only a LITTLE laggy when it comes to getting new or upgraded software. The only time I have had any problems with it, is when I got greedy and decided to install a package from 'unstable' and it happened to cause a problem. Now, I use 'stable' for my server box, because I don't care when a new version of KDE comes out for it. It just needs to run mysql, apache and php. It gets the security patches and it keeps running strong. No worries, because I am not running anything new or not tried and tested. It is STABLE as in UNCHANGING. That is the idea of Debian 'stable', an unchanging platform on which you can rely. 'Unstable' is the first major distribution where things go. Then after meeting some <A href=\"http://www.debian.org/devel/testing\">criteria</A> it graduates to 'testing'. Then when a new 'stable' is getting near, the doors to 'testing' are shut and things are nailed down. Then the new 'stable' becomes the solid 'testing' then 'testing' opens up for changes again and the process begins again.\n</rant>\n\nDebian rules!\nApt RULES!"
    author: "Jonathan Dietrich"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "So, after all your pointless ranting, what's the summary?  \n\nDebian hasn't had a up-to-date 'stable' release in many years, which is my point. \n\n'Unstable' is unstable and can break things (see http://www.debian.org/doc/manuals/reference/ch-system.en.html#s-unstable).  People think it's fun to apt things every day and end up with a broken system.  That's fine but it's not for me anymore.\n\nAll I want is stable and up-to-date which Ark Linux gives me."
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "use 'testing', not 'unstable', not 'stable'"
    author: "Jonathan Dietrich"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "I see.  So it seems 'testing' vs 'stable' in Debian is comparable to Fedora vs RedHat.  Thanks for clearing that up.  When was the latest release of 'testing'?"
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "'testing' is a rolling distribution that is not released in a traditional fashion. As individual pieces are found to work correctly in the 'unstable' distribution, and after they have met the required criteria, then these individual packages are released into 'testing'. So every weekend when I do my apt-get update, apt-get upgrade, I get the newest available packages from 'testing'\n\nThe only Debian distribution that has real releases is 'stable'."
    author: "Jonathan Dietrich"
  - subject: "Re: i'm a developer"
    date: 2005-03-22
    body: "My head hurts.  :D"
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-24
    body: "It seem that you know even less about RedHat than you do about debian."
    author: "maestro"
  - subject: "Re: i'm a developer"
    date: 2005-03-24
    body: "What the heck makes you think that something produced by one undergrad is going to be \"stable and up-to-date\"?  Sheesh."
    author: "maestro"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "And also, I think Ark Linux has APT anyway."
    author: "ac"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "the so-called 'unstable' version of debian is more stable than ark-linux's stable release can hope to be. No linux system can even dream of being half as stable as debian-stable is.\n\nif you're used to suse, mandrake and the other major distributions, debian-unstable or debian-testing will be at least as stable as the official versions these distributions have to offer.\n\nThat's why Kubuntu is much better for a developer."
    author: "superstoned"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "\"debian-unstable or debian-testing will be at least as stable as the official versions these distributions have to offer.\"\n\nI would agree for the most part, but it is unfortunately comments like these that can lead to problems. \n\n'testing' is the way to go, unless you must always have the bleeding edge and are willing to deal with the bleeding that comes when 'unstable' actually is UNSTABLE with broken packages and the like, (though it doesn't tend to stay that way for long). \n\n'testing' is often not far behind 'unstable' and is MUCH more friendly. I have had NO issues since I have stuck with 'testing'.\n\n"
    author: "Jonathan Dietrich"
  - subject: "Re: i'm a developer"
    date: 2005-03-21
    body: "An important thing to note is that security updates for testing are NOT managed by the security team.  testing may also have release critical bugs present."
    author: "Anonymous"
  - subject: "Debian"
    date: 2005-03-24
    body: "When it comes to using Debian for a desktop that is connected to the Internet, only 'unstable' will do.\n\nIf you were to use 'stable', the backports that you would be tempted to use would render your system less stable than 'unstable'. \n\nIf you were to use 'testing', you would really have to monitor the security web site / mailing list and be prepared to take your system off line when something serious comes along.\n\nYou will also find that in addition to the large official repository, 'unstable' has the largest number of third-party packages available (if you use those, you will probably have some trouble eventually though).\n\nAnd it is not in any way unstable, at least by desktop standards. Just use it. Yes, it will take a little bit of knowledge.\n"
    author: "Martin"
  - subject: "Re: i'm a developer"
    date: 2005-03-24
    body: "What kind of developer judges a distro by screenshots?  And if you do go by screenshots, the one that says \"Multiple logical devices can be assoziated with a single piece of hardware\" doesn't offer much confidence in quality control."
    author: "maestro"
  - subject: "turkix"
    date: 2005-03-20
    body: "\"The goal of Ark Linux is to build the easiest to use GNU/Linux distribution while keeping it technically sane\"\n\nI don't want to flame a new discussion here but if you look for free, KDE based and easiest to use GNU/Linux distribution, you should also consider Turkix <http://www.turkix.org>. But as for the \"technically sane\" argument, I have nothing to say.. Just look at these screenshots;\n\nhttp://www.turkix.org/modules.php?name=Content&pa=showpage&pid=15&page=2\nhttp://www.turkix.org/modules.php?name=Content&pa=showpage&pid=36"
    author: "piano"
  - subject: "Re: turkix"
    date: 2005-03-22
    body: "Seems to be all in Turkish. That's probably no option for most people. Turkix is probably a localized Turkish Linux distro that also happens to have nice looking screenshots.\n"
    author: "Chakie"
  - subject: "Re: turkix"
    date: 2005-03-22
    body: "< probably a localized ... Linux distro\n\nIsn't Linux wonderful? No more monopolies."
    author: "KDE User"
  - subject: "Re: turkix"
    date: 2005-03-24
    body: "I'm fascinated by how someone can look at a collection of screenshots, some in English and some in Turkish, and write \"Seems to all be in Turkish.\"  Take a look again at\nhttp://www.turkix.org/modules.php?name=Content&pa=showpage&pid=36\n\nand then consider getting your glasses (or your brain) replaced.\n"
    author: "maestro"
  - subject: "Why should I choose ArkLinux"
    date: 2005-03-21
    body: "I strongly believe in supoprting small businesses and giving startups a chance in this world of total corparatization and globalization. When using Linux I use Suse (now Novell, hence corparatized), and I would like to know what advantages there are to using a startup like ArkLinux instead.\n\nSpecifically, what kind of program packages can be used with Ark (debian or RedHat I presume?)? If, for example, Ark is redhat-based, then does it keep in sync with them for program installation purposes? Does Ark make system maintainace as easy as YAST? \n\nWhat scares me about diving into a new distribution is worries about package installation. I do not use Linux extensively these days:-( but really want to; installing/removing programs remains a big issue for me. I want to jut be able to download e.g., Xine and have it work without fighting dependencies etc. Suse will probably be easier to deal with in this regard, but I want to remain open to other possibilities. I just don't have the time to spend weeks fighting to configure my distribution.\n\nThe Ark Website does not seem to answer these questions. Can someone in the know provide some information to these and related questions?\n\nIdris"
    author: "Idris Samawi Hamid"
  - subject: "Worry no more"
    date: 2005-03-22
    body: "http://autopackage.org/"
    author: "Matt"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-22
    body: "apt-get / synaptic works with Ark and AFAIK is installed by default."
    author: "Kanwar"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-22
    body: "Ark comes only with Kynaptic by default, and you better \"apt-get install synaptic\" immediately."
    author: "Anonymous"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-22
    body: "*sigh* yet another pointless K naming scheme"
    author: "ricky"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-22
    body: "What is your KDE app called then?"
    author: "ac"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "how about synaptic-kde?"
    author: "ricky"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "Too long and pointlessly verbose, IMHO.  How about synaptiK or even Kynaptic?  Both of these make the intention obvious and are kuite kute."
    author: "ac"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "I know Gnome does the 'G' naming scheme to a lesser extent, why does nearly every program need the 'K' somewhere inserted. can't you be orginal with orginal names without having a similar ktitle?"
    author: "ricky"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "Who the frack cares.  Grow up..."
    author: "ac"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "don't do it if you don't want to see yourself compiling alot of the packages you want. ark doesn't have such a huge repository of software debian or fedora have. and its rpm based, so you'll run into dependencie troubles for sure, if you install non-offical packages (eg segfaults and the like).\n\nif you want a huge database of easy-to-install software, go for (K)ubuntu or debian itself.\n\nark is nice, but I rather see people working on an ubuntu-like project (keep in touch with your roots, contribute back to mommy) instead of reall forks or fully do-it-yourself distro's. thats quite an duplication of efforts. Ark has some cool things, but if they would implement these on top of debian, they'll have a lot less work just keeping their software up-to-date and testing it, so they can work on their installer and configuration software.\n\nthe first gentoo'ers should have done the same, imho. extend apt with the features they wanted, instead of doing all this work, while only a fraction of it is original and good (emerge and some tools surrounding it). THEY should have created apt-source..."
    author: "superstoned"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "FUD and lies.  I'm a developer using Mandrake which is RPM based with rpmfind.net and I have no such troubles you are talking about.  Debian on the other hand has much larger troubles:\nhttp://lists.userlinux.com/pipermail/discuss/2005-February/007223.html\n\nWith Debian you'll likely find yourself in a never-ending cycle of updates, no security and constant breakages.  Forget trying to develop on this mess and deploying it to other distributions that are actually USED by people."
    author: "ac"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "> With Debian you'll likely find yourself in a never-ending cycle of updates, no security and constant breakages.\n\nThat's completely untrue as far as Debian stable is concerned.  The exact opposite is true.  The link you posted talks about the development version.  But I guess you knew that."
    author: "cm"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "Yes, stable is so out of date for development it's not even worth talking about here.  Sorry. You Just Can't Have It Both Ways!  Why don't Debian folks understand this?"
    author: "ac"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "Please stop trolling.  There are still lots of people who are quite happily running a server based on Debian stable, and doing development for that platform.  \n\n"
    author: "cm"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-23
    body: "You're the one who's off-topic for the thread, so don't accuse me of trolling.\n\nIf OP wants to run latest version of Xine what is he going to do exactly?  So please stop trolling with your unstable debian for this and stable debian for that excuse when people want BOTH AT THE SAME TIME."
    author: "ac"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-24
    body: "I run several Debian computers and have NEVER had a problem with the testing distribution.  I am very often MORE up to date than those running Mandrake, Suse, or Red Hat.  If I want the very newest version of Xine and don't want to wait the couple of months it usually takes (for a package like Xine) to move into testing I get it from unstable.  Otherwise I do simple one command upgrades periodically.\n\nBack to the original purpose of the post, Re: Why should I choose ArkLinux.  I was interested in something that is VERY easy to use so that I can set it up on other people's computers.  I do a lot of repair work, and very often would like to say, \"The easiest way to fix your problem with [spyware, adware, crashes, etc.] would be to install [an easy to use version of Linux.]  Frequently I work with old grandma's and such people that really don't work well with change.  I remember one house where I changed the order of the icons on the desktop to put them into logical groups and the person told me to put them back because she couldn't find her programs.  So I need something VERY easy to use.  I'm not sure if ArkLinux is it yet, but I'm interested."
    author: "Soren"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-24
    body: "Not only don't you know anything about debian, but you don't know what trolling consists of (while practicing it nonetheless)."
    author: "maestro"
  - subject: "Re: Why should I choose ArkLinux"
    date: 2005-03-24
    body: "I've been using mandrake for years, and debian, too. I know them both very well, and its no FUD. damn, I'm even moderator on a mandrakeclub forum... Mandrake isn't as bad as SuSE in this regard (I'm trying suse 9.2, but there seem to be different packages for suse 9.1; suse9.1 with KDE 3.3; and now there have to be packages for suse with kde 3.4!!! what a waste of time to build all these packages! the mess for suse 9.2 isn't that big, yet, but it'll be...).\n\na lot rpm's work just fine, altough you often have to search for quite some time. but there are quite some hard-to-find packages, esp if you want them up-to-date. and also you often have to overwrite files and force nodependencies, which is as bad as it sounds - stability problems will emerge, sooner or later. mandrake and suse require a fresh install, now and then, if you really fiddle a lot with them (if you don't, nothing wrong, but I guess a developer sometimes wants to try new packages, and installs stuff that isn't on his installation cd's - and that spells trouble).\n\nDebian's advantage is that amost everything you want is simply in its official package pool, so it won't break anything. unstable is generaly more stable than a fiddled-with mandrake install (eg after some upgrades, lets say from 9.0 to 10.1, mandrake becomes hard to mantain) and testing is as a rock, for me. for a home system, I don't care too much about security, just apt-get upgrade now and then.\n\nThe best choice is imho (k)ubuntu, for it is as up-to-date as mandrake, and you can use all these debian packages. if you use a official version, its stable, and you can run bleeding-edge if you want."
    author: "superstoned"
---
Following the trend of distributions scheduling themselves around KDE releases, <a href="http://www.arklinux.org/">Ark Linux</a> has released their first stable version, <a href="http://www.arklinux.org/article.php?story=20050319113904421">Ark Linux 2005.1</a>.  The goal of Ark Linux is to build the <a href="http://www.arklinux.org/staticpages/index.php?page=screenshots">easiest to use</a> GNU/Linux distribution while keeping it technically sane.

<!--break-->
<p>Ark Linux 2005.1 is built around the latest desktop technologies, including KDE 3.4, OpenOffice.org 1.1.4 (a preview of 2.0 is also available on the Ark Extra Software CD), glibc 2.3.4, X.Org 6.8.2 and Linux 2.6.11.</p>

<p>The base install CD of Ark Linux contains everything the average desktop user will need - other tools such as compilers and development programmes,
additional games and support for additional languages are available on the 
extra CD images "Ark Development Suite", "Ark Extra Software", "Ark Server 
Software" and "Ark Extra Languages", and of course in our large online 
package repository, easily accessed through the Kynaptic GUI. Experienced 
users can use the "apt-get" tool to install software from the repository on 
the command line.</p>

<p>Ark Linux 2005.1 can be <a href="http://www.arklinux.org/staticpages/index.php?page=downloads">downloaded from arklinux.org</a> using ftp, 
http, BitTorrent or EDonkey.</p>
