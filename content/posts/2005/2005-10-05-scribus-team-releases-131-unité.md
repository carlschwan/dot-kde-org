---
title: "Scribus Team Releases 1.3.1 Unit\u00e9"
date:    2005-10-05
authors:
  - "mrdocs"
slug:    scribus-team-releases-131-unité
comments:
  - subject: "klik://scribus-latest "
    date: 2005-10-05
    body: "Just for curiosity I tried \n\n. . klik://scribus-latest\n\nexpecting it didnt work -- but boy, was I taken by surprise! I beautifully installed a Scribus-1.3.1cvs (build dated 29th of September) onto my SUSE-9.3 system.\n\nI really, really, really wonder, wonder, wonder how the nice klik guys make this thing work. They seem to make wonders happen"
    author: "ac"
  - subject: "Re: klik://scribus-latest "
    date: 2005-10-05
    body: "I just did a klik://scribus-latest and I got 1.3.2cvs dated Oct. 3\nvery cool."
    author: "Jonathan Dietrich"
  - subject: "Re: klik://scribus-latest "
    date: 2005-10-05
    body: "\"I really, really, really wonder, wonder, wonder how the nice klik guys make this thing work. They seem to make wonders happen\"\n\nWhen debs already exists for an application, making a klik package for it is a peace of cake. And Scribus has debs for debian and kubuntu so..."
    author: "Joe"
  - subject: "Re: klik://scribus-latest "
    date: 2005-10-05
    body: "You sound like an old-fart wisacre to me. ...\"piece of cake\"... Tss, tss!\n\nIf it indeed *IS* so easy as you pretend, why didnt *you* invent klik? And why didnt you do so 10 years ago already?"
    author: "ac"
  - subject: "Re: klik://scribus-latest "
    date: 2005-10-06
    body: "I think he was talking about how hard it is to make a klik *PACKAGE*, not klik itself."
    author: "Corbin"
  - subject: "Re: klik://scribus-latest "
    date: 2005-10-06
    body: "\"If it indeed *IS* so easy as you pretend, why didnt *you* invent klik? And why didnt you do so 10 years ago already?\"\n\nOk let me rephrase it for you:\n\"When debs already exists for an application, klik (which is a great app that we all love) will easily be able to use it.\"\n\nThis is what the developper KLIK page says about it:\n\"The easiest way to get your application in klik is to get it into debian - then it will appear in klik automatically. \"\n\nThanx for the fart thing insult by the way, you're very mature."
    author: "Joe"
  - subject: "sounds great"
    date: 2005-10-05
    body: "Scribus is a great gt app. It is a pity that there are only few example files to play around with. Again a wonderful showcase for klik technology."
    author: "Marten"
  - subject: "KWord and Scribus"
    date: 2005-10-05
    body: "Isn't KWord similar to Scribus, more like a Desktop Publishing software? But I find Scribus 1.3 *easy to work with*!\n\nCould we include Scribus in KOffice???"
    author: "Fast_Rizwaan"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "It's a little similar, in that both applications allow you to use frames. The difference is that in KWord, you edit your text in place, while in Scribus you have to import the text or use the story editor. And while I love Scribus, and think it would be useful to be able to embed KOffice parts right in a Scribus document (the other way around would be weird...), I don't think that replacing KWord with Scribus will gain us much fans. Besides, from a purely technical point of view, it would take quite a bit of effort."
    author: "Boudewijn Rempt"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "well, both apps use frames. But there are differences in the goal of each app. Kword is determined mainly for text writting as in \"office\" suite. Scribus is designed for DTP work - it handles things which are unnecessary for common office users. I cannot imagine that one app replace the other one."
    author: "petr vanek"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "I think that all KDE application has to learn how to create a very good PDF from Scriubus, for KDE 4 could be very very cool and fine. An exporting PDF framework. I tryed to insert a 4 Mpixel photo into KWord and create a PDF that losts the hi quality photo definition and I got a bad printing PDF. I made the same thing with Scribus and I don't lost the photo definition and I got a well done printing with a coulor printer. I like KWord and I use it very often, but a PDF framework into kdelibs/kdebase could be a very good thing for KDE future."
    author: "Giovanni Venturi"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "Yes, integrating Scribus PDF capabilities into KDe would be a good thing. I'm not sure how easy it would be. I have created just one little patch for Scribus startup dialog, and taken a look at their use of cms; I haven't looked at the pdf code yet."
    author: "Boudewijn Rempt"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "Why don't you submit a bugreport about this issue."
    author: "Meneer Dik"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "Unfortunately Qt does not allow to modify the generated Postscript. That even leads that EPS files are not transfered to the Postscript stream.\n\nAs far as I know, Scribus has chosen to re-implement everything of the PostScript generation.\n\nAs for putting it in kdelibs, it needs to be LGPL. That is one reason why the work was never started in KOffice (as the goal was clearly kdelibs). (The other reason being also the need of people having knowledge and time, and especially knowing how to handle Asian fonts in PostScript.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-05
    body: "Correct, Scribus implements its own PDF and EPS/PS exporters quite independent of Qt."
    author: "mrdocs"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-06
    body: "The problem is that KDE uses the Qt PostScript driver.\n\nThe Scribus developers recognized that the Qt PostScript driver doesn't print correctly so they wrote their own.\n\nThere is also the Sun PostScript driver which was donated to OpenOffice.\n\nClearly, if the Qt PostScript driver doesn't print KWord documents (and other KOffice documents) correctly that something needs to be done.  Using one or the other of these two PostScript drivers appears to be a possible solution to the problem."
    author: "James Richard Tyrer"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-08
    body: "Bogus. If KWord chooses to scale the imagine before it's printed using a QPainter on a QPrinter, of course you loose quality.\n\nThis is a kword bug, not a Qt bug.\n\nAlso not that a driver buys you nothing if it's not integrated in your painting abstraction. \n\nThe Scribus guys needed features not provided by the Qt3's painter. Qt4's painter does everything they need, so hopefully they'll port to use Arthur soon. With Qt4 there's no point in using libart or cairo anymore with a Qt application."
    author: "anonymous"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-09
    body: "OK anonymous, please tell us how (with Qt-3) to paint with a font using the UNHINTED font metrics and then print with the unhinted font metrics.  I am sure that we would all like to know how to do it.\n\nI would also like to know how to do it with Qt-4.\n\nNote that IIUC, scaling the image will not do it because that would change the size of the glyphs as well as the space between them so the problem would remain the same."
    author: "James Richard Tyrer"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-09
    body: "Giovanni talked about printing a 4 Mpixel *photo* and loosing quality, not about some minor issue with font hinting."
    author: "anonymous"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-09
    body: "No, he talked about the problems with KDE apps:\n\n> I think that all KDE application has to learn how to create a very good PDF \n> from Scriubus,\n\nAs I said, the cause of this is the use of the Qt PS driver.\n\nWhat I said was about \"font metrics\" not about \"hinting\".  This is the usual subtle difference between a a noun and the adjective modifying it.  E.G. if I mention green cheese, I am taking about \"cheese\", not \"green\".\n\nNow, the specific problem that you refer to is probably related to the fact that you can't set the resolution with the Qt-3 PostScript driver.  The result is that a high resolution photo is down sampled."
    author: "James Richard Tyrer"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-16
    body: "> The result is that a high resolution photo is down sampled.\n\nAnd this is where I believe you are wrong:\n\nEven in Qt3, the painter has a drawImage function that takes a rectangle and an image. If you print with this, *all* image data ends up in the PS file, it's scaled by the postscript interpreter when you print it.\n\nSome programs seem to call QImage::scale() first, and by doing so sample the image down to 72 DPI, then they call drawImage() with a sampled down image, and then they blame Qt's postscript driver.\n\nA driver can only print the data you put in, if you give it a 72 dpi image, it will print a 72 dpi image. If you give it a 300dpi image, it will scale it properly to the degree the printer supports it. Postscript, as I'm sure you know, is meant to be resolution independent."
    author: "anonymous"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-16
    body: "You are partially correct.\n\nThe problem starts where the Qt PS print driver (like other printer drivers) does not produce a resoution independent PS file.\n\nThe PS driver uses a scale:\n\n    QPaintDeviceMetrics m( printer );\n    scale = 72. / ((float) m.logicalDpiY());\n\ncomputed from a DPI passed from the Painter which is normal for PostScript drivers -- not a good thing, just common.\n\nKDE's libs don't appear to currently pass anything other than 72 DPI for the logical DPI of a page unless your are printing to PDF.  IIUC, this is because higher resolution doesn't work (exactly where it dosen't work is another question)\n\nIMHO, it would be better if there was a way to include images in the PS file at their original resolution and let the print system (GhostScript in most cases) resample the image to fit the printer, but this isn't the way that Qt appears to work.  \n\nIt appears that the current version of KPrinter/KWord will do this, but only if you print directly to a PDF.  But, I can't get it to work for a PDF. :-(  So, I tried it printing to a PS file (no resolution specified) and I don't see any loss of resolution using Qt-3.3.5.  Was this fixed?"
    author: "James Richard Tyrer"
  - subject: "Re: KWord and Scribus"
    date: 2005-10-16
    body: "I posted my test here:\n\nhttp://home.earthlink.net/~tyrerj/kde/PS-test.png\n\nThis shows the original 640x480 image magnified to 11X in The GIMP\n\nThis image was included in a KWord document at 320x240 pixels which is shows in GSView magnified somewhat.  This was magnified with X-Magnifier on the left.\n\nThese appear to correspond pixel for pixel so there is no loss of resolution -- no down sampling.\n\nThis was with KWord 1.4.2, KDE-3.4.3 & Qt-3.3.5.  If this has been fixed -- it appears that it has been fixed -- I don't know exactly where."
    author: "James Richard Tyrer"
  - subject: "klik for Mandriva ?"
    date: 2005-10-05
    body: "Guys, has anyone tried klik in Mandriva ? Is it feasible to use it ?\n\nthx!"
    author: "MandrakeUser"
  - subject: "Re: klik for Mandriva ?"
    date: 2005-10-05
    body: "Try it and find out!  It works in Gentoo pretty well, so I figure that Mandriva should work as well."
    author: "Corbin"
  - subject: "Re: klik for Mandriva ?"
    date: 2005-10-05
    body: "yeah, I'll try tonight (can't use linux at work unfortunately) -- thanks!"
    author: "MandrakeUser"
  - subject: "Re: klik for Mandriva ?"
    date: 2005-10-07
    body: "I tried klik and it worked. Pretty cool. Many packages do not work in Mandriva, especially (not surprisingly) the ones based on debian. But generic packages are totally fine. They just work slower than the native Mandriva packages, but hey, with one click you install a generic package, this is pretty sweet. \n\nI do share the sentiment that this does not replace distro packaging of software, but is is a very nice way of installing self-contained software, or  bleeding edge packages for testing. Excellent work from the klik team.\n\nOh, I haven't tried the scribus klik yet  ...\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: klik for Mandriva ?"
    date: 2005-10-11
    body: "I asked Olle Dierks to try the scribus klik. He told me it works but with some minor errors. "
    author: "Sam Balenti IV"
  - subject: "cairo?"
    date: 2005-10-06
    body: "I wonder why they start using cairo now. Is Qt4/Arthur lacking any important features?"
    author: "uddw"
  - subject: "Re: cairo?"
    date: 2005-10-07
    body: "Well right now they are using Qt3.  They may figure it would be better to use cairo now instead of waiting for the application to be ported to Qt4 to get the improvements.  Also if Scribus uses KDElibs the developers would have to wait about 1 year before they could port it to Qt4 (for the KDElibs to be ported)."
    author: "Corbin"
  - subject: "Re: cairo?"
    date: 2005-10-07
    body: "Scribus doesn't use the kde libs, so nothing would stop them from going over to Qt4, except that that is a pretty big undertaking."
    author: "Boudewijn Rempt"
---
The Scribus Team is pleased to announce the release of <a href="http://www.scribus.org.uk/modules.php?op=modload&amp;name=News&amp;file=article&amp;sid=106">Scribus 1.3.1</a>, <em>Unité</em>. The 1.3.1 release is the second development version towards a new stable 1.4.  This release brings new features never before available in any open source application including enhanced spot colour printing, intelligent colour wheel, in-line graphics, variable page sizes and ICC colour support.  There has also been work towards Cairo support and a native Windows version and an initial MacOS X native build is available.












<!--break-->
<ul>
<li>Scribus now natively supports spot colour printing for true DeviceN colour space support, natively rendering spot colours, with no maximum limit to the number of spot colours. Scribus uses enhanced spot and separation previews for previewing all separations when a new version of Ghostscript is available. The new spot colour support can also convert spot colours into composite CMYK in both print and PDF export. DeviceN support works with both EPS and PDF export.</li>
<li>This release also adds support for displaying colours and making colour choices with an intelligent colour wheel for colour schemes and to help individuals with colour viewing impairments. This is a first, we believe, for any page layout application. </li>
<li>Scribus text frames now support in-line graphics, both images along with scaled vector drawings.</li>
<li>Additional code, patches and fixes for Win32 and Mac OSX compatibility. Substantial progress has been made for both platforms and an initial 1.3.1 release of Scribus for MacOS X is available. A Win32 native release is hoped to be available for one of the next 1.3.x releases.</li>
<li>Scribus now has a Calendar Creation Wizard python plug-in with support for several languages.</li>
<li>New experimental, optional build time support for using the Cairo graphics library as a rendering engine. Currently, there are some minor performance issues, however, most notably gradient display is improved.</li>
<li>Many improvements in page and document layout. Scribus now supports variable page sizes and orientations within a single document, as well as when printing or exporting PDF.</li>
<li>Implemented a new startup dialogue with more options for creating, or opening recent and existing documents.</li>
<li>Continued improvements in ICC colour support including searching system directories for ICC profiles. Scribus 1.3.1+ now supports the proposed OpenICC specification for colour management profiles.</li></ul>

<p>The Scribus Team would also like to thank <a href="http://anduin.net">Anduin.net/Øverby Consulting</a> and <a href="http://www.work.de/">n@work
GmbH, Hamburg</a> for gracefully sponsoring our <a href="http://docs.scribus.net">Scribus Documentation</a> site, <a href="http://bugs.scribus.net">our Bug Tracker</a> and Anonymous CVS server. Also, a note of thanks to <a href="http://www.smmp.salford.ac.uk/">University of Salford - School of Media, Music and Performance</a> for providing continued hosting of our website including upgrading the server hardware.</p>

<h3>Getting Scribus 1.3.1</h3>

<p>You can:</p>
<ul>
<li>Download source tarballs and document templates and RPMs from our <a href="http://sourceforge.net/project/showfiles.php?group_id=125235">Sourceforge Download page</a> or <a href="http://www.scribus.org.uk/modules.php?op=modload&amp;name=Downloads&amp;file=index&amp;req=viewdownload&amp;cid=20">scribus.org.uk downloads page</a></li>
<li>For Debian and Kubuntu repositories, visit <a href="http://docs.scribus.net/index.php?lang=en&amp;page=install-dpkg">Scribus Debian Repository</a>.</li>
</ul>
	
<p>The Scribus Team would also like to thank the many end users, testers and contributors to making this our finest release to date. </p>












