---
title: "Quickies: KDE Tips, KDE Laptop, Atlantik Server, KDE-Look T-Shirt"
date:    2005-02-09
authors:
  - "jriddell"
slug:    quickies-kde-tips-kde-laptop-atlantik-server-kde-look-t-shirt
comments:
  - subject: "OT - maps.google.com"
    date: 2005-02-09
    body: "Yes, this is off-topic, but It *sorta fits in with the idea of quickies (neat developments worth noting)  Check it out at maps.google.com.  Right now it is US only (hopefully that will change) but unfortunately konqueror in 3.3.2 does not support it. (It seems to use java-script extensively)  Can anyone say if it works in the 3.4 beta?  Hopefully this will become supported, since it puts mapquest to shame :)\n\n(Yes I know, i should probably report it to bugs.kde.org)\n\n-Sam  "
    author: "Samuel Stephen Weber"
  - subject: "Re: OT - maps.google.com"
    date: 2005-02-09
    body: "CVS HEAD does not support maps.google.com\n"
    author: "Luke-Jr"
  - subject: "Re: OT - maps.google.com"
    date: 2005-02-09
    body: "Works for me with post-3.3.2 . Every now and then Konqueror displays a \"kill blocking script\" dialog, but zooming and moving around works just fine here."
    author: "Michael Jahn"
  - subject: "Re: OT - maps.google.com"
    date: 2005-02-09
    body: "Spoof as Safari and click open anyway. It works just fine, or as fine as in Safari.\n\nMore interestingly it somehow sees through our Mozilla spoofs."
    author: "Carewolf"
  - subject: "maps.gmail.com"
    date: 2005-02-10
    body: "From what I understand, it needs XMLHttpRequest and XSLTProcessor which I guess khtml doesn't have just yet. http://jgwebber.blogspot.com/2005/02/mapping-google.html"
    author: "John"
---
<a href="http://www.newsforge.com/">Newsforge</a> is featuring an article on advanced usage of KWin, Hot Keys, Konqueror and GTK+ using Qt themes in "<a href="http://software.newsforge.com/article.pl?sid=05/01/20/1822225">KDE tips and tricks</a>". *** <a href="http://www.linare.com/">Linare</a> contacted us about their new low cost <a href="http://www.linare.com/linare-linux-notebook.php">Linare Notebook</a> which comes with KDE pre-installed. *** The <a href="http://kde-apps.org/content/show.php?content=10019">Atlantik</a> server, which has been down for several weeks, has now returned for all you Monopoly players.  *** The <a href="http://kde-look.org/">KDE-Look.org</a> <a href="http://www.kde-look.org/news/index.php?id=154">t-shirt contest is closed</a>, an open poll to decide the winner will be available in a few days.  

<!--break-->
