---
title: "Open KHTML Info Page Launched"
date:    2005-06-04
authors:
  - "hporten"
slug:    open-khtml-info-page-launched
comments:
  - subject: "konqui&acid2"
    date: 2005-06-04
    body: "I guess this kind-of belongs here...\nhttp://www.kdedevelopers.org/node/view/1129"
    author: "heiner"
  - subject: "Re: konqui&acid2"
    date: 2005-06-04
    body: "that's silly, you think it's worth a minor comment that's not going to be seen by anyone?  why don't you post a real article to theDot instead. =)"
    author: "ac"
  - subject: "So?"
    date: 2005-06-04
    body: "To be fair, I don't think it deserves anything more. Compared to many other of the minor and major changes continuously made to khtml, making it a better browser, it's not that significant. Lots of these other improvements actually make the day to day usage of Konqueror a much better experience than the changes made for Acid2. As they fix problems people encounter in the real world on actual web pages. The only reason this deserves anything more, would purely be for PR purposes. Anyway, kudos to the khtml developers for this and all the other contributions to khtml and Konqueror."
    author: "Morty"
  - subject: "Re: konqui&acid2"
    date: 2005-06-05
    body: "Where can I download a new Konqueror that includes all the recent khtml and Konqueror improvements?\n\nNowhere? Do I really have to wait for KDE 3.5 and update the whole desktop just to get an Konqueror that supports ad-blocking and improved CSS?\nMaybe it's time to switch to Firefox - so that I don't have to update my whole desktop for a new webbrowser."
    author: "Hans"
  - subject: "Re: konqui&acid2"
    date: 2005-06-05
    body: "If I recall correctly, you can have any 3.x version of Konqueror, with any 3.x version of KDE (kdelibs, indeed), because they are binary compatible (of course, assuming they both are compiled with the same compiler, or with binary compatible versions of the compiler).\n\nYou can also install at the same time Konqueror from \"trunk\" in your system, without breaking your distribution's installation. Just use a different prefix, and export some variables (or use unnoficial packages). Is well documented in developer.kde.org, so I'm not going to explain it here. ;-)"
    author: "suy"
  - subject: "Re: konqui&acid2"
    date: 2005-06-05
    body: "Have you ever tried to compile Konqueror 3.4.1 with kdelibs 3.1.5? No? Of course not, because then you would have know that this does not work.\n\nBinary compatibility means that it would theoretically be possible to use Konqueror 3.1 together with kdelibs 3.4.1.\n\n\n> You can also install at the same time Konqueror from \"trunk\" in your system,\n> without breaking your distribution's installation. Just use a different\n> prefix, and export some variables (or use unnoficial packages).\n\nOh right, that's really simple and intuitive. I always compile a new kernel, too, when downloading a new firefox version..."
    author: "Hans"
  - subject: "Re: konqui&acid2"
    date: 2005-06-05
    body: "> Have you ever tried to compile Konqueror 3.4.1 with kdelibs 3.1.5? No? Of course not, because then you would have know that this does not work\n\nThat's correct, but how about installing kdelibs 3.5 and Konqueror/KHTML next to your KDE 3.1 desktop?"
    author: "ac"
  - subject: "Re: konqui&acid2"
    date: 2005-06-05
    body: "It depends on the distro you are using. With Gentoo you can update only khtml and/or konqueror without updating everything else."
    author: "LB"
  - subject: "ACid2 tests passed"
    date: 2005-06-05
    body: "I guess this is the real news:\nhttp://www.kdedevelopers.org/node/view/1129\n\nDespite from that I must confess that khtml is ready for me. Mozilla 1.0 was not. Konqueror is the best browser I know when it comes to rendering but what I would like to see is improvement on the interface. No khtml issue though."
    author: "Gerd"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-05
    body: "I agree, KHTML is becoming really great. And if only GMail worked with it I could uninstall Firefox immediately :("
    author: "Anonymous"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-05
    body: "I'm in the same boat.  Why is it that GMail still doesn't work in Konqueror?"
    author: "ac"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-05
    body: "I use GMail on Konqueror every day. I use the \"Basic HTML\" view of GMail, which I actually like more than the full view. It is less distracting (no \"RSS strip\" which sometimes shows news, other times ads) and, most importantly, right-click to \"Open in new Tab\" works in all links (as opposed to the JavaScript-ridden Standard View). It's unfortunate that some settings panels work only on the Standard View, but I rarely access them.\n\nI'm all for better support of AJAX-based pages on Konqueror, but GMail is no reason for me to use Firefox. Google Maps, on the other hand...\n\n"
    author: "Hisham Muhammad"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-06
    body: "Yeah, me too.\n\nI like 'striped down' GMail more than full featured GMail. More less clutered to me."
    author: "Adi Wibowo"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-05
    body: "It works fine for me (kde 3.4).  Of course, google feeds it a simpler interface than it does to IE, but for me that is a good thing ;)\n\nI can send and receive mail and attachments with it, so afaiac it works with Konqueror."
    author: "MamiyaOtaru"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-05
    body: "Probably the same old reason. Folks at google have used redhat, thus they pretty much dislike anything that has 'k' in it."
    author: "Anonymous"
  - subject: "Re: ACid2 tests passed"
    date: 2005-06-06
    body: "lol ... not at all ... it probably has a lot more to do with issues of market share and khtml capabilitites. the fact that they actually feed konqueror a special (trimmed down) page shows that they care, and i happen to know that there are desktops running KDE at Google."
    author: "Aaron  Seigo"
---
In an effort to open up their development process the developers of the <a href="http://www.konqueror.org/">Konqueror</a> components KHTML, KJS and <a href="http://svg.kde.org">KSVG</a> have launched the open Web portal <a href="http://khtml.info/">KHTML.info</a>.

By providing a central contact point and source of information in
form of an open Wiki the developers want to promote their work and
embrace users and developers from both Open Source as well as
commercial environments.




<!--break-->
<p>KDE's implementations of standard technologies turned out to be not
only useful for KDE's file manager and Internet browser Konqueror but have since found
their way into other parts of KDE and other Free Software projects as
well as commercial products like <a
href="http://www.apple.com/">Apple's</a> Web browser <a
href="http://www.infoanarchy.org/wiki/index.php/Safari">Safari</a>.</p>

<p>With KDE 4 the trend of seeing KDE software being extended and used
on other platforms will continue to rise. Only if developers with
different backgrounds cooperate by exchanging their knowledge and
enhancements in form of features, optimizations and quality assurance
we'll be able to master the technical challenges that lie ahead.</p>



