---
title: "FOSDEM 2005: Python Bindings Interview"
date:    2005-02-19
authors:
  - "jriddell"
slug:    fosdem-2005-python-bindings-interview
comments:
  - subject: "KDE and C++"
    date: 2005-02-19
    body: "i understand your love for python, yet i dont share your opinion on doing KDE core apps in python. You 'love' python (can i say that?), someone else loves ruby, or java, or mono, or javascript, etc.\n\npeople tend to uses programming languages as additional/subsitude religions.\n\ni somehow like the C++ simplicity within KDE.\n\n\nYou say developing in python is faster and more effective, i can believe this. But what is the performance (mem/cpu/responiveness) tradeoff?\n\n\n> After all much of the computer industry has always moved\n> to higher level languages where possible. Look at the\n> use of Java in business, or even, dare I say, Visual\n> Basic. There is probably more high level code being\n> written these days than C and C++.\n\n'They use it' is no argument for doing it too. I feel Java often sucks for GUI apps (for webpages it is nice though). I think we should think for our selves.\n\n\nplease dont get me wrong, bindings are an enormeous contribution to the qt/kdelibs but as far i my limited knowledge goes i'd say: lets keep it simple, lets stick to C++ (for the core stuff)\n\nregards,\ncies breijs.\n\n"
    author: "cies"
  - subject: "Re: KDE and C++"
    date: 2005-02-19
    body: "You must also take into consideration the security tradeoff.  Python has a big advantage here.  It is much easier to write subtle security holes in C++ than in python.  It is very important to make sure that it is hard to introduce security holes in your code, especially on an open-source project such as KDE, where it cannot be guaranteed that every contributor knows the tricks to writing secure C++ code.\n\nI have lately become more concerned about the security of the KDE core libraries.    As KDE's user base grows, I fear we will start to see exploits specifically targeting KDE.  This is still off in the future for now, but it *is* coming sooner or later.\n\nWe have already seen quite a few KDE security advisories, but we have been lucky enough so far that not a single one has been exploited, as far as I know.  KDE is simply not popular enough to attract virus writers yet.\n\nBut the holes that have been found are only the tip of the iceberg.  In such a large project as KDE, written entirely in C++, it is practically certain that there will be many security holes.  I can easily see malicious HTML crashing Konqueror, KMail, and Kontact and exploiting those crashes to run code.  Every KDE application that can load files downloaded from the Internet is a possible vector for viruses: KPDF, amaroK, KMPlayer, KOffice apps, etc etc.\n\nEven Linux's security model will probably not help:  there are far too many Linux kernel exploits out there.  Becoming root on a typical Linux system is not hard, after gaining access under a normal user account.\n\nIf more of KDE were written in Python or other safe languages, I would feel a lot better about its security."
    author: "Spy Hunter"
  - subject: "Re: KDE and C++"
    date: 2005-02-22
    body: "I'm not sure I really believe you. Can you come up with an example to illustrate your point? Already C++ with the KDE classes has a lot of protection from overflows and the likes, and performance is (waaay) on C++s side."
    author: "Johan"
  - subject: "Re: KDE and C++"
    date: 2005-02-19
    body: ">I feel Java often sucks for GUI apps\nNice of you to bring up this example and doing so actually vocalize why PyKDE/PyQt are a real winner. In PyKDE/PyQt programs all GUI heavy lifting are done in C++ KDE/Qt, the thin wrapper to Python generates next to none performance drop. Except for some special computer intensive cases, like image manipulation programs, the speed of Python are a non issue when using PyKDE/PyQt.  "
    author: "Morty"
  - subject: "Re: KDE and C++"
    date: 2005-02-19
    body: "Image manipulation apps, in casu Krita, are the only reason I ever stopped using PyQt and PyKDE for everything... And even there, if there hadn't been a complete application done in C++ already, I'd never have done the GUI in C++, even with KXMLGUI. The core image data lib and the canvas in C++, all neatly wrapped up in Python. Forces a decent separation of core and gui, too.\n\nAs for C++, just having to work with both header and implementation files is such a waste of my time. Not having a \"class\" object makes dynamic applications much more work. I could go on... And in the end, you gain nothing, not even an appreciable difference in speed."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE and C++"
    date: 2005-02-22
    body: "projects like psycho and pypy throw the need for any\nc++ out of the window. i hope you can shortly get back\nto programming in a language that can be loved :)\n\ni'd like to get qtruby pluggable enough to let me run a\nsingle program over a collection of c++ headers, and \nhave an introspectable api accessible from within ruby.\n\ni hope some day that pyqt will follow the smoke route :)\nit makes everything a whole load easier :)\n\nAlex"
    author: "lypie"
  - subject: "Re: KDE and C++"
    date: 2005-02-19
    body: "> 'They use it' is no argument for doing it too. \n\nTrue, by itself \"they use it\" is not a real argument.\n\nIf I may expand a bit on what I said. There is a reason why business gravitates towards higher level languages and tools. It gets the job done faster and cheaper. They are just more effective. Hardware gets cheaper, programmers don't. (ok, let's ignore India for a moment here. ;-) ) Now, in OSS land money is often called time. I've only got a limited amount of time that I can spend on OSS projects. I want to make that time count.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Python is the best!"
    date: 2005-02-19
    body: "It's so easy compared to anything else, I can't mait until the bindings are updated for Qt 3.4.4 and Python 2.4."
    author: "Al"
  - subject: "Re: Python is the best!"
    date: 2005-02-19
    body: "What's there to wait for? The existing release of PyQt supports Qt 3.3.4 and Python 2.4 without problems."
    author: "Phil Thompson"
  - subject: "Re: Python is the best!"
    date: 2005-02-20
    body: "Does this mean that I can just install KDEBindings and write KDE apps in Python, including making DCOP calls, instead of building SIP, PyKDE and PyQt every time I upgrade KDE? "
    author: "Sean Brown"
  - subject: "Re: Python is the best!"
    date: 2005-02-20
    body: "What Phil pointed out to the previous poster was that PyQt already was available for the versions he talked about.\n\nYour question on the other hand is if you can use the kdebindings module rather than standalone SIP/PyQt/PyKDE. Since the python support in kdebindings are SIP/PyQt/PyKDE adapted for KDE cvs it depends, on how the module are built if using a binary(if the provider of the bin include the SIP/PyQt/PyKDE part). From source you are essentially building the same thing. If you are only going to use DCOP from Python without GUI, you should probably use dcoppython(also in bindings)."
    author: "Morty"
  - subject: "How about a set of truly Pythonic bindings?"
    date: 2005-02-19
    body: "I've recently finished my first pykde app recently (neat little english<->hebrew translating dictionary fetching data morfix.co.il), and it seemed to me that (in a few places at least) there are a lot of places where the C++ calls look awkward in Python.  \n\nWhat is the demand for a set of Python bindings to KDE which take advantage of Python's built-in features like \n   - iterators/generators \n   - dictionaries \n   - exceptions \n   - decorators\n   - ability to return a list holding multiple values \nand the like? \n\nHere's a hypotethical example, fetching a web page:\n\n- using python's standard library:\n\n  params = urllib.urlencode({'q': word.encode('cp1255')})\n  headers = {\"Content-type\": \"application/x-www-form-urlencoded\"}\n  conn = httplib.HTTPConnection(\"milon.morfix.co.il\")\n  conn.request(\"POST\", \"/default.asp\", params, headers)\n  response = conn.getresponse()\n  headers['Cookie'] = response.getheader('Set-Cookie')\n  conn.close()\n\n- using KDE's KIO::storedGet():\n\n  url = KURL( QString(\"milon.morfix.co.il\") )\n  stj =  kio.KIO.storedGet( url, 0, 0 )\n  stj.addMetaData(\"Content-type\", \"application/x-www-form-urlencoded\")\n  stj.addMetaData( QString(\"Cookie\"), stj.queryMetaData( QString(\"Set-Cook\nie\")) )\n  self.connect( stj, SIGNAL(\"result(KIO::Job*)\"), self.slotGotDataResponse\n\nTo me, a more Pythonic approach could look like this:\n\n  stj = kio.storedGet( \"http://milon.morfix.co.il\", 0, 0 )\n  stj['Content-type'] = 'application/x-www-form-urlencoded'\n  stj['Cookie'] = stj['Set-Cookie']\n  self.connect( stj, stj.result, self.slotGotDataResponse )\n\nNow, I'm still learning pykde (and more fundamentally, KDE), so I'm probably losing sight of some details here.  But I think Python programmers could be more efficient when they don't have to compromise their programming style to accommodate the limitations of another language altogether.[1]\nAnyway, I'm on Freenode's #debian-kde, #kde-devel and #python.\n\n\n- Roey \n\n1. note:  I don't dislike C++!  I just like Python more.[2] :-D  \n\n2. (no, really, I don't have anything against C++! some of my best friends are C++ coders).\n\n\n\n\n"
    author: "Roey Katz"
  - subject: "Forgot a couple of things"
    date: 2005-02-19
    body: "Python has unit testing and native string processing capabilities.\n\n\n- Roey"
    author: "Roey Katz"
  - subject: "Re: How about a set of truly Pythonic bindings?"
    date: 2005-02-19
    body: "When writing bindings, you have to compromise.  The more different the Python bindings become, the harder it is to translate code back and forth, and the harder it is for C++ programmers to move to the Python API.  The C++ documentation becomes less relevant; in the extreme case writing all new documentation for Python would be necessary, which would be a huge undertaking considering how extensive the C++ documentation is (you can't autogenerate documentation like you can an API, unfortunately...).\n\nWriting a complete, *truly* Pythonic QT binding would require almost as much effort as designing QT's C++ API in the first place, and in the end the result might not look much like QT any more."
    author: "Spy Hunter"
  - subject: "Re: How about a set of truly Pythonic bindings?"
    date: 2005-02-19
    body: "Right, that issue crossed my mind.  But then, why not have two parallel bindings, just like Apple does with cocoa and carbon?\n\nOf course, it would still be a huge undertaking as you say.\nAnd speaking of adding new documentation... well, I think that the autogenerated documentation is not sufficient for someone who is approaching KDE programming from the Python side.  If you're programming KDE in Python, why do you have to learn C++'s prototypes (to use with SIGNAL() and SLOT()).  Ultimately I end up having to go back to the C++ documentation to figure out the specifications for a particular API.\n\nRoey"
    author: "Roey Katz"
  - subject: "Re: How about a set of truly Pythonic bindings?"
    date: 2005-02-22
    body: "I agree with this.\n\nI'm in the middle of writing my first PyKDE program and I love it.  I'm happy just with the fact that KDE methods that return a QDict actually return a Python dict or that methods that return a QStringList actually return a Python list of Python strings.  To me, that is pretty \"Pythonic\".\n\nOn the downside, I keep forgetting that methods that return QStrings don't automatically turn them into Python strings for me.\n\nBack to topic, I completely agree with whoever said that most applications don't require C/C++.  These high level languages make our lives as programmers so much easier at the cost of a very small penalty."
    author: "Christopher J. Bottaro"
  - subject: "A licencing question"
    date: 2005-02-19
    body: "Now that Qt is (maybe) going to be GPL for versi\u00f3n 4, how does this change/affect/favours Python Bindings?\n\n\n"
    author: "Mario"
  - subject: "Re: A licencing question"
    date: 2005-02-19
    body: "It just means that there will be a GPL version of PyQt for Windows."
    author: "Phil Thompson"
  - subject: "Re: A licencing question"
    date: 2005-02-19
    body: "Qt is GPL in version 3..."
    author: "AC"
---
<a href="http://www.simonzone.com/">Simon Edwards</a> will be talking about <em>KDE application development using Python</em> in the <a href="http://www.fosdem.org/2005/index/dev_room_kde">FOSDEM KDE Developer's Room</a>.  In the interview below he talks about the advantages of Python, how it compares to other languages and whether KDE should be rewritten in Python.



<!--break-->
<p><strong>Please introduce yourself and your role in KDE</strong></p>

<p>I started using KDE as a user and application developer when KDE 1.0 came out.
Since then I've developed a Linux firewall utility called Guarddog, and
beginning with KDE 3.3 I've been maintaining a copy of the Qt/KDE Python
bindings in KDE CVS kdebindings module. I've also got a KDE/Python project or
two on the go and am involved with KDE-NL.</p>

<p><strong>How does Python compare to C++ or other high level languages for KDE development?</strong></p>

<p>I think it's an excellent language for developing KDE applications. In my
opinion it is a better language choice than C++ for most applications.
Python is a mature language with the features you need to be able to develop
fast and effectively. It has the usual features that you expect to see in a
high level language, classes/OOP, exceptions, garbage collection, and so on
and so on. But the feature I like the most is that it works the way you
expect it to. There are very few hidden catches, and the language is small
and simple enough that it will fit in your head. All together this makes
development a lot faster and a lot more enjoyable.</p>

<p><strong>Why do you think that so few KDE applications have been written with Python, given the maturity of the bindings?</strong></p>

<p>Getting a working set of bindings compiled and installed has traditionally
meant downloading and building 3 different pieces of software and making sure
that you've picked a set of versions that are mutually compatible and
compatible with your version of KDE/Qt. This has acted as a deterrent to
anyone wanting to write and distribute Python software that users can easily
install and use. And since there are few KDE Python programs, distributors
have had little incentive to package and distribute the bindings. Catch-22.</p>

<p>Now, that is KDE applications, Qt based applications are a different story.
Phil Thompson tells me that hundreds of commercial licenses have been sold for
the Qt part of the bindings. The next commercial Qt application you see could
very well have Python under the hood.</p>

<p><strong>Do you hope that one day Python may be the native language for KDE or Qt?</strong></p>

<p>I'd certainly like to see the day when higher level languages are regulary
used for KDE development.  Interest in languages other than C++ certainly
seems to be increasing inside the KDE project. So I don't think we will have
to wait long before that day comes. After all much of the computer industry has
always moved to higher level languages where possible. Look at the use of Java
in business, or even, dare I say, Visual Basic. There is probably more high
level code being written these days than C and C++.</p>

<p><strong>Would it be realistic to write the whole of KDE in Python?</strong></p>

<p>Well, you've got the technical problem of how would other languages interface
with KDE objects written in Python, and the fact that some parts of KDE are
speed critical, think GUI drawing and rendering. However, I think there are
large tracts of KDE that could be written using Python. The KDE Control
Centre is a good example. There is really no reason why the whole thing
couldn't be done using Python.</p>

<p><strong>How do you see the Python bindings fitting in with other technologies like Korundum, KJSEmbed and Kommander?</strong></p>

<p>I see Python as an alternative to C++ for application development. Kommander
is directed more towards the rapid development of GUI front ends to shell
programs. KJSEmbed strikes me as being better suited for scripting and macros
inside a host program, than for application development. I have my doubts
about the suitability of Javascript for large programs. Mind you, the Mozilla
people have been doing this for quite some time now. Anyway, there are lots
of places that can use different programming tools. There is room for
everyone.</p>

<p>Lastly, some thanks need to go out to Phil Thompson and Jim Bublitz. The
people who did the hard work of creating the Python bindings in the first
place. Thanks guys!</p>


