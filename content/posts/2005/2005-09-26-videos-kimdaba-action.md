---
title: "Videos of KimDaBa in Action"
date:    2005-09-26
authors:
  - "jpedersen"
slug:    videos-kimdaba-action
comments:
  - subject: "klik://kimdaba"
    date: 2005-09-25
    body: "klik://kimdaba if anyone wants to try. \n\nIf you want to see what other users had to say about it:\nhttp://klik.atekon.de/details.php?section=graphics&package=kimdaba"
    author: "probono"
  - subject: "Re: klik://kimdaba"
    date: 2005-09-25
    body: "Cool!\nThe only problem is that this is the rather old 2.0 version. The current version is 2.1"
    author: "Jesper Pedersen"
  - subject: "mmh... with what..."
    date: 2005-09-25
    body: "... did you make flash output?\n\nthx"
    author: "Djento"
  - subject: "Re: mmh... with what..."
    date: 2005-09-25
    body: "well forget :)\n\nkimdaba would be really kickass if it'd support \"xcf\" file format.\n\ncheers"
    author: "Djento"
  - subject: "Good software"
    date: 2005-09-25
    body: "I'd love to have a merge of Kimdaba and Digikam. Do you think it make sense?"
    author: "veton"
  - subject: "Re: Good software"
    date: 2005-09-26
    body: "http://www.kde-apps.org/content/show.php?content=16061"
    author: "gerd"
  - subject: "Wink is another option."
    date: 2005-09-25
    body: "Another app that does well with the creation of flash based application tutorials is the free (as in beer only) app from Debugmode called Wink.  It allows you to record tutorials in a frame based way, taking screenshots of what ever you want, and then edit the series of shots, reposition the cursor, and add in text box pointer explaining what you are doing. The app in its current version does not currently support adding in voice overs, but it generates nice flash, html, or pdf style presenations of your tutorials.  Find it here: www.debugmode.com."
    author: "Joe Kowalski"
  - subject: "Re: Wink is another option."
    date: 2005-09-25
    body: "We just need something that captures its audio from the sound card. An amaroK presentation would be pretty lame without it. :)"
    author: "Ian Monroe"
  - subject: "klik://wink"
    date: 2005-09-26
    body: "klik://wink for those who prefer to klik :)"
    author: "probono"
  - subject: "2006?"
    date: 2005-09-25
    body: "Wow... You allready wend to the 2006 Akadamy event? Most people are still recovering from the 2005 one :D"
    author: "Andre Somers"
  - subject: "Re: 2006?"
    date: 2005-09-26
    body: "Sure, I got this time machine, and it is really fun."
    author: "Jesper Pedersen"
  - subject: "Re: 2006?"
    date: 2005-09-26
    body: "Are they almost finished with KDE 4.0?"
    author: "Ian Monroe"
  - subject: "Re: 2006?"
    date: 2005-09-27
    body: "no, so they sent a killer robot back in time to protect a young man named rudy and his slightly tomboyish yet fetchingly yummy mother from an untimely demise so he can grow up and join the kde project during the kde4 development cycle to ensure its completion. only rudy can lead us into a brave new future where we are not ruled by heartless robots!"
    author: "Aaron J. Seigo"
  - subject: "What I miss in kimdaba"
    date: 2005-09-26
    body: "Here a little bit user feedback:\n\nI like the application and the idea behind it - its a very nice application :-)\nBut it is not perfect, so here some wishes to make it perfect for me:\n\n- addressbook: it would be nice to connect the people keywords to the contacts in the addressbook\n- hidden thumbnail directory: I do not like it that the thumbnail directory is a standard directory - hide it! If you do not so maybe other foto management software is affected, and technically it is not so beautiful to have such \"technical\" directories visible (and there is the possibility that unexperienced users delete this directory)\n- digikam: it would be nice to be able to exchange the tags between digikam and kimdaba (import in both directions!) - that would save a lot (!) or work for me because I have a full tagged photo database with digikam\n\nSo far from me, hope that helps or shows you what some users (or at least me) think about kimdaba.\n\nRoland"
    author: "liquidat"
  - subject: "Re: What I miss in kimdaba"
    date: 2005-09-29
    body: "I did actually stop to tag in Kimdaba when I found Digikam also a nice thing, just with worse tag support, but more promise on usability.\n\nI don't want to spend much time on tags that I will loose when I switch the tagging app.\n\nSo please, digikam, kimdaba, is that XML you use some standard scheme?\n\nYours, Kay"
    author: "Debian User"
  - subject: "Audacity"
    date: 2005-09-26
    body: "So I found the trick with audacity it to make sure your system has not created any sounds for around 30 seconds before starting it. Artsd will automatically relinquish teh dsp after that short delay, and then when you start and app such as audacity that wants native access to the dsp, they will get it. I think you can configure this delay in the settings -> multimedia ->sound system module.\n\nHope that helps, it works for me....\n\nRegards\n\nTim"
    author: "Tim Sutton"
  - subject: "Suggestion of improvement about this news"
    date: 2005-09-26
    body: "You read the whole thing, and in the end even though you've learned how the videos were made, you still have no clue what KimDaBa is about.\n\n(yes, I've heard about kimdaba, read announcements about it, but still couldn't remember what was the purpose of this application. It's only by reading a reply on how it would be cool to merge it with digikam that I remembered it was some kind of image manager)."
    author: "Guillaume Laurent"
  - subject: "Re: Suggestion of improvement about this news"
    date: 2005-09-26
    body: "Yes you are right. It doesn\u0092t say that it is a Picture manager. I can live with this instance since KDE and this news site is done mainly by voluntary work. \nThis News site is not a Mainstream Publishing Site. It\u0092s a special news site for people which are usually deep into KDE. \nCNN wouldn\u0092t explain that Texas is in the USA every time they bring news about the Hurricane either. \n\nHermann"
    author: "Hermann"
  - subject: "Re: Suggestion of improvement about this news"
    date: 2005-09-26
    body: "I think I qualify as being \"deep into KDE\" (and people who are really that deep don't need this site to know about KDE related news, they get it from more direct sources), and yet I don't always remember what an application is for just from its name. May be I'm showing my age... :-)"
    author: "Guillaume Laurent"
  - subject: "How to import photos?"
    date: 2005-09-26
    body: "I was using digikam and just wanted to switch to kimdaba, but I couldn't figure it out, how to import photos from the harddisk.\n"
    author: "hiasll"
  - subject: "Re: How to import photos?"
    date: 2005-09-26
    body: "I just had to set up KimDaBa to use the same base directory as Digikam.\nWhenever you add pictures to that directory, they will be found by KimDaBa,\neither at startup or via Menu maintenance -> Rescan for images.\nYou have also a KIPI import plugin to import photos from your camera.\n\nCheers."
    author: "jmfayard"
  - subject: "Re: How to import photos?"
    date: 2005-09-26
    body: "Thank you very much! I think there should be a button somewhere to import the pictures. It would enhance the usability a lot!\n"
    author: "hiasll"
  - subject: "No plugin found for \"Shockwave Flash Media\""
    date: 2005-09-26
    body: "If there had been a way to play swf files on my platform..."
    author: "Anonymous"
  - subject: "Re: No plugin found for \"Shockwave Flash Media\""
    date: 2005-10-03
    body: "klik://flash \n\nwill do the trick"
    author: "Birne"
  - subject: "exif?"
    date: 2005-09-26
    body: "Does it store all of the meta data in the image using exif <http://en.wikipedia.org/wiki/Exif>  as well as a central database? If not, I would very much like it if it did."
    author: "Jonathan Dietrich"
  - subject: "Re: exif?"
    date: 2005-09-26
    body: "By design KimDaBa doesn't touch your image files at all. It only stores it in its XML file. Fell free to create a plugin that would convert from the database to EXIF :-)"
    author: "Jesper Pedersen"
  - subject: "An UI bug shown in presentation"
    date: 2005-09-27
    body: "Try to find a UI bug in second presentation (http://ktown.kde.org/kimdaba/videos/tour2.html).\n\nTip: look on input fields when annotating images!"
    author: "MZM"
  - subject: "Re: An UI bug shown in presentation"
    date: 2005-09-27
    body: "I take it you are talking about the multiple blinking cursors. Indeed that is a UI bug, but I can't see how I can get my code to do so in the first place. I've searched for this bug for years, so if you have any idea where to look, I'd be happy to hear."
    author: "Jesper Pedersen"
  - subject: "Flash-less tutorial"
    date: 2005-09-28
    body: "Is there anyway to watch this video tutorial without using anything to do with flash ?\n\nI run a pure 64bit system and far too lame to set up some 32bit crap, or change to a Gentoo multilib system, or generally be bothered with anything to do with flash.\n\nIs it possible to get the raw screenshots in Jpeg format perhaps along with an ogg audio file ?"
    author: "markc"
  - subject: "Re: Flash-less tutorial"
    date: 2005-09-30
    body: "Give the cvs version of GPLflash2 a try.  I'm not sure if it will work for this, but it might be worth a try."
    author: "Joe Kowalski"
---
For those of you who do not understand how to use <a href="http://ktown.kde.org/kimdaba">KimDaBa</a>, there is now no reason not to use it.  KimDaBa is the first KDE application to offer small flash videos with voice-overs describing how to use it.  See the tutorials at <a href="http://ktown.kde.org/kimdaba/videos/">KimDaBa's video page</a> or read on below for Jesper's description of how and why to make video tutorials of applications.









<!--break-->
<p>There is a sentence I hear from my users rather often: <em>"This is exactly the software I need, I just wish I had found it a long time ago"</em>.</p>

<p>Now if KimDaBa is that good, why isn't everybody in the whole world using
it? One reason might be its name (and I am considering renaming it to
something boring but more obvious, suggestions very welcome).  Another reason might be that it is different to many other photo managing applications in some ways. This is not a bug, but simply because it approaches the problem differently, which is its strength.</p>

<p>I've tried remedying this problem by offering a demo setup the first time
people start KimDaBa, and now I'm ready with a new approach:
<a href="http://ktown.kde.org/kimdaba/videos/">flash videos including voice-overs</a> that shows KimDaBa in use.</p>

<p>This has actually been on my wish list for years, but I never found any
appropriate tool that would allow me to both record my screen action and
add voice so I could explain what users are seeing.</p>

<p>Before I tell you how I did it, let me send a strong thank you in the
direction of Rainer Endres who held a presentation on the topic at Akadamy
2006.</p>

<p>The tools to use are <a
href="http://www.unixuser.org/~euske/vnc2swf/">vnc2swf</a> plus any audio
editing tool, I ended up using the <em>ahem</em> Gnome application called <a href="http://www.metadecks.org/software/sweep/">Sweep</a>. I tried for hours to get <a href="http://audacity.sourceforge.net/tt">audacity</a> working as I was told that was the best, but without success.</p>

<p>My work flow was the following:</p>
<ol>
<li>First write down the voice-over in a text file - ensure to speak it
out loud to yourself, what sounds good on paper might not sounds good when
speaking.</li>
<li>Next record the audio part (That was by far the hardest part, it took me approximately an hour to record a 1 minute voice-over).</li>
<li>Following that you must record the video part. For that I created a new user, logged into the user's account, and executed: <tt>vncserver -geometry
640x480</tt>. That starts a VNC session for the user that is no larger than
640x480, which seems to be the highest to go if it should all be visible at
playback time on a 1024x768 display.</li>
<li> Time to log in to the newly started server, for that I used a command
similar to <tt>krdc -h localhost:1</tt></li>
<li>Now you should be capable of seeing the new user's session in a vnc
window.</li>
<li>If KDE doesn't start automatically in the VNC session, simply run
<tt>export DISPLAY=:1</tt> followed by <tt>startkde</tt> from your new user's shell.</li>
<li>Now it is time to run a command similar to <tt>vnc2swf.py -o video.vnc localhost 5901</tt>, this will bring up a dialogue where you can start and stop recording. While recording, move your mouse around as dictated in the voice-over.</li>
<li>The tricky part was that vnc2swf grabbed my audio device, so to play
back the voice-over I had to convert it to mp3, and play it on my mp3 player.</li>
<li>The final step is to combine the video and audio, which is done with
a command similar to: <tt>edit.py -c -o video.swf -a audio.mp3 video.vnc</tt>, that gives you a video.html file which will start video.vnc.</li>
</ol>

<p>Now you know how to do it, no more excuses, get started recording
videos for your favourite application. Well only valid excuse is that you
now want to try out <a href="http://ktown.kde.org/kimdaba/">KimDaBa</a> now
you know how to use it.</p>







