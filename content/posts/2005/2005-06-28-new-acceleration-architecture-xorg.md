---
title: "New Acceleration Architecture for X.org"
date:    2005-06-28
authors:
  - "binner"
slug:    new-acceleration-architecture-xorg
comments:
  - subject: "All eyecandy progress is good"
    date: 2005-06-28
    body: "Outstanding.  The instant-gratification part of me is happy this is being done on two stages (KAA then Xgl) as opposed to having to wait for Xgl.  Any news about the Unichrome chipset's support?  I could perhaps lend a hand."
    author: "Erik"
  - subject: "Xgl"
    date: 2005-06-28
    body: "What kind of timetable is Xgl running on? Anyone know when we can expect it to come out?\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "> Anyone know when we can expect it to come out?\n\nWhen it's done. ;)"
    author: "cl"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "and even if Xgl is out we still depend on the proprietary/binary-only drivers that come with the graphic cards...\nif those drivers continue to be unoptimised and of poor quality the real power of our modern graphic monsters will remain unused.\n\nanyway, what zack is doing will probably help pushing X' desktop graphics to an acceptable level.\n\nthanks zack!\n\n_cies.\n"
    author: "cies breijs"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "Well, apart from those of us getting reasonable performance out of the free drivers in Xorg :)"
    author: "Tom"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "i dont know, but, is there any free (as in Freedom) driver for the recent 3D graph cards?"
    author: "cies breijs"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "No, not really. Next time don't throw away your video card just because some pimply kid makes fun of your \"obsolete\" system :-)"
    author: "Brandybuck"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "Hey- that statement isn't exactly fair to those of us who (inexplicably or accidentally, of course!) destroy our hardware. ;)\n\nAs far as the nVidia cards go, the (proprietary) linux drivers have performed very well. I have yet to have an issue with the speed of my rendering, and I've been using their driver for a few years now. \n\nIt supports all of the features available on windows, and it even has its own configuration program. :) nVidia seems quite devoted to the cause.\n\nNote: I have a desktop card (GeForce), not a workstation card (Quatro (?))."
    author: "Zak Jensen"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "You can still buy the (DRI supported) Radeon 9200s. They're really cheap, needs no fans, and some models comes with DVI so it's a perfect product right now. But they probably won't be available in another 6 months, since they will stop producing the chipsets."
    author: "Jonas"
  - subject: "Re: Xgl"
    date: 2005-06-28
    body: "Bought an 9250 some days ago. Works fine with the OS drivers, has DVI+VGA+TV, 128MB and passive cooling. It also work with AGP 2x-8x (3.3V, 1.5V and 0.8V signal level), which I needed for my really old motherboard (GA-7ixe4, SDR-RAM, Duron 700). Nice and cheap card.\n\nIf you need more power, there is the 9550, which is supported by the r200 driver, and for newer cards (r(v)3xx and r4xx series) support is on its way (http://r300.sf.net/), Quake3 already working."
    author: "Lurchi"
  - subject: "Re: Xgl"
    date: 2005-06-29
    body: "Haha, I love the comment about the no fans.  They should hype that as a feature in fact.\n\nI hate the fans.  So noisy and annoying."
    author: "KDE User"
  - subject: "So"
    date: 2005-06-28
    body: "So it's a new 2D acceleration layer which doesn't use OpenGL, how is it different from Evas ? \nIs it specific to Qt and does it need any modifications beside video drivers ?"
    author: "thom"
  - subject: "Re: So"
    date: 2005-06-28
    body: "Its not dependent on any toolkit, not like Evas which is part of a toolkit. It's in the Xserver and not specific to Qt. It does not sound like it needs any modifications besides the video driver, it's a replacement for xaa."
    author: "Morty"
  - subject: "Re: So"
    date: 2005-06-28
    body: "well, it is part of X11, and evas is not. i guess evas is specific for enlightenment...\n\nit has nothing to do with QT, afaik, all it has to do is replace XAA (and be faster)."
    author: "superstoned"
  - subject: "Re: So"
    date: 2005-06-28
    body: "evas is a generic graphics library, it doesn't depend on Enlightenment, much like imlib which was also created by E crew."
    author: "Anonymous"
  - subject: "Evas is a canvas"
    date: 2005-06-28
    body: "Because evas is a fast canvas library. This is an in-driver architecture to let drivers accelerate primitive drawing operations they couldn't before, and do existing ones much more simply.\n\nEvas (and Cairo) provide a simple and optimised way to do things like say \"draw an antialiased line at 50 pt width on a 1m canvas scaled to fit my 400x400 px window\" without the app author descending into sobbing fits. XAA and friends help the graphics card driver do the underlying \"draw this antialiased 5pt line from (x1,y1)px to (x2,y2)px on :0.0\" *really* *fast*.\n\nEvas, Cairo, etc can't reach into the graphics driver level and use hardware specific graphics tricks. XAA and friends aren't a drawing API, they're an acceleration framework. The two really have basically nothing in common, except in that the former can use tha latter."
    author: "Craig Ringer"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-28
    body: "nice post.   to all others doing a \"what about debian\" in a slack topic, leap."
    author: "jenga"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-28
    body: "well, i think it is not bad to ask about technology that seems to be related. not only because it can be informative (i didn't exactly know the diferences as the parent did), but also point to duplication off efforts - which CAN be a bad thing."
    author: "superstoned"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-28
    body: "Everything of E17 is duplication, otherwise it would be a concerted effort together with X, GNOME and KDE."
    author: "ac"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-29
    body: "since the things in E17 came first they are being duplicated. Try getting your facts straight before troll... wait nm. Trolls don't get there facts straight."
    author: "illogic-al"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-29
    body: "Thanks for your answer,\nSo will Qt use Cairo or something similar (is it arthur) on top of EXA ?"
    author: "thom"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-29
    body: "Qt4 has Arthur: http://doc.trolltech.com/4.0/qt4-arthur.html"
    author: "Anonymous"
  - subject: "Re: Evas is a canvas"
    date: 2005-06-29
    body: "Qt Arthur will use XRender that is accelatered by the graphics drivers use of EXA."
    author: "ac"
  - subject: "Thanks Zack!"
    date: 2005-06-28
    body: "Thanks for improving X and Desktop experience. Thanks in advance :)"
    author: "fast_rizwaan"
  - subject: "Re: Thanks Zack!"
    date: 2005-06-28
    body: "Yeah, Thanks Zack! This guy is really a talanted hacker!\nTrolltech: pay him good!"
    author: "blacksheep"
  - subject: "Sweet!"
    date: 2005-06-28
    body: "Plasma here we come!"
    author: "Ryan"
  - subject: "Wunderkind?"
    date: 2005-06-28
    body: "\"Zack volunteered to port all unmaintained graphic card drivers as soon as he gets the respective hardware in his hands\"\n\nWhere did Zack suddenly get the whole knowledge to promise and do something like that? I didn't know him doing any hardware driver development before..."
    author: "ac"
  - subject: "Re: Wunderkind?"
    date: 2005-06-28
    body: "Do you think that he knew something about Mozilla code before hacking on it at aKademy? :-)"
    author: "Anonymous"
  - subject: "Re: Wunderkind?"
    date: 2005-06-28
    body: "When he says \"port\", he really means switch some function calls around so it uses the Exa framework instead of XAA. Only the most basic primitive will actually be accelerated in hardware (and even then, only if the driver already accelerated them before).\n\nPorting a driver to Exa appears to be really simple, due to clever design by Zack, so porting a driver is mostly a formality. Still, you obviously need to have the hardware and do testing on it (which is usually the far more time-consuming part in driver development if it's to be done right).\n"
    author: "Nicolai Haehnle"
  - subject: "Re: Wunderkind?"
    date: 2005-06-28
    body: "It's not needed because of the way he implemented Exa, from the anoncement \"Note that you don't have to be a driver developer to switch any of those drivers.\""
    author: "Morty"
  - subject: "Re: Wunderkind?"
    date: 2005-06-28
    body: "But it's Zack himself who made that possible so he has to already have quite a knowledge about the whole graphic card driver architecture which again involves plenty of hardware details."
    author: "ac"
  - subject: "Re: Wunderkind?"
    date: 2005-06-28
    body: "Zack is not really human. He's the last member of an advanced \u00fcberprogramming alien race, kind of like Superman but with a text editor instead of laser eyes.\n\nHis only weakness? Battlestar Galactica."
    author: "clee"
  - subject: "Re: Wunderkind?"
    date: 2005-06-29
    body: "+5 funny"
    author: "superstoned"
  - subject: "So..."
    date: 2005-06-28
    body: "... let the Slashdot begin..."
    author: "Leon Mergen"
  - subject: "subpixel antialiasing"
    date: 2005-06-28
    body: "Will there be any progress with subpixel antialiasing? What does Eye Candy help when the fonts are still ugly displayed (yes, I switched subpixel hinting on in KDE)\n\nBest wishes\nmm"
    author: "mm"
  - subject: "Re: subpixel antialiasing"
    date: 2005-06-28
    body: "qt4 has all the features you need , help coding / porting today !!!"
    author: "asdasd"
  - subject: "Re: subpixel antialiasing"
    date: 2005-06-29
    body: "> Will there be any progress with subpixel antialiasing?\n\nNo.\nSubpixel hinting is ready to use. Maybe you should select another hinting modus or activate the bytecode interpreter.\n\n> What does Eye Candy help when the fonts are still ugly displayed\n\n??\nsubpixel hinting + hinting=full gives very clear fonts.\nsubpixel hinting + bytecode interpreter gives *sometimes* even better fonts.\n"
    author: "Hans"
  - subject: "Re: subpixel antialiasing"
    date: 2005-06-29
    body: "This has absolutely NOTHING to do with what's mentioned in this article.\n\nTry enableing the Freetype Bytcode Interpreter to improve your fonts."
    author: "zxcvzxvcv"
  - subject: "KAA and KDrive"
    date: 2005-06-28
    body: "Anyone find it funny that these K-initial technologies are in no way related to KDE (in the sense that the K in their name is not from KDE)"
    author: "Renato Sousa"
  - subject: "Re: KAA and KDrive"
    date: 2005-06-28
    body: "Nope, it's just like with the Linux kernel which uses k all the time as well. (Maybe I'm just unfunny.)"
    author: "ac"
  - subject: "Re: KAA and KDrive"
    date: 2005-06-28
    body: "Which brings up a good point, we should port the kernel to Qt :)"
    author: "Jake"
  - subject: "Re: KAA and KDrive"
    date: 2005-06-28
    body: "Dude. Just imagine the power of QT signals in the kernel, and you can see the potential.\n\n*ducks*"
    author: "Zak Jensen"
  - subject: "Re: KAA and KDrive"
    date: 2005-06-29
    body: "\"make xconfig\" is using qt..."
    author: "Hans"
  - subject: "Re: KAA and KDrive"
    date: 2005-06-28
    body: "That's the reason why it had to be replaced/improved ;-)..."
    author: "Anonymous"
  - subject: "With Nvidia"
    date: 2005-06-28
    body: "I use the binary 2d and 3d hardware accelerated driver from Nvidia.\nWill I still see a speed/flicker improvement in X?"
    author: "ac"
  - subject: "Re: With Nvidia"
    date: 2005-06-28
    body: "Nvidia signaled to support Exa in their driver."
    author: "Anonymous"
  - subject: "Is it stable?"
    date: 2005-06-28
    body: "Current COMPOSITE feature crashes a lot of applications. How about this one?"
    author: "wangxiaohu"
  - subject: "Re: Is it stable?"
    date: 2005-06-29
    body: "Here here!\n\nStability of the composite extension is erm... non existant? :)\n\nI've gone through two graphic cards (granted, both nVidia (GeForceMX460, GeForce2GTS), and two completely different hardware platforms (Athlon 1.3, and Athlon64bit3200+), both give lockups consistantly after using xorg with the composite extension enabled after about 1Sec-1Min of use..\n\nI want my pretty desktop damnit :P"
    author: "Jack"
  - subject: "Re: Is it stable?"
    date: 2005-06-29
    body: "the kde composition support is much more stable for me. maybe it helps...\n\n(so dont use xcompositemgr but enable shadows and transparancy in kwin)"
    author: "superstoned"
  - subject: "Re: Is it stable?"
    date: 2005-10-03
    body: "works good for me! nvidia geforce 6800GT with the kde configuration tool in control center.\n\nI don't get any crashes but sometimes windows forget to stop being transparent after I move them or change the size. Oh, and some games flicker a bit."
    author: "Vlad Blanton"
  - subject: "My"
    date: 2005-06-29
    body: "I dont think X.org will be fine"
    author: "aNDY"
  - subject: "Re: My"
    date: 2005-06-29
    body: "Pardon? What do you mean?"
    author: "Anonymous"
  - subject: "re"
    date: 2005-06-29
    body: "Oh My God"
    author: "aNDY"
  - subject: "Wow. Good news !!"
    date: 2005-06-29
    body: "AWESOME. I bet kde withh be the first to take advantage of the features. Then Enlightenment then Xfce and gn0m3 :p"
    author: "Jesus Franco"
  - subject: "Re: Wow. Good news !!"
    date: 2005-07-11
    body: "Well GTK 2.7 already has support for cairo, and cairo can use glitz. Glitz matches the XRender specification, so will take full advantage of all this sort of stuff.\nAs far as I know kde is estimated to have cairo support in KDE 4..\n\nAt least that's the info I've pieced together ;) so don't take my word for it"
    author: "someone300"
  - subject: "Re: Wow. Good news !!"
    date: 2005-07-11
    body: "Gtk+ 2.7 is a development version. So better wait until it's released and the first GNOME version depending on it and tested against Gtk+ 2.8 is released."
    author: "Anonymous"
  - subject: "Re: Wow. Good news !!"
    date: 2005-07-11
    body: "KDE doesn't need cairo because Qt4 offers the painting framework Arthur (http://doc.trolltech.com/4.0/qt4-arthur.html) that can also use OpenGL. \n\nSomebody will probably create an cairo backend for Arthur but it will be only one of the choice."
    author: "cl"
  - subject: "blow to xgl"
    date: 2005-08-12
    body: "mission accomplished!\nhttp://lists.freedesktop.org/archives/xorg/2005-August/009168.html\n\nGood thing someone is working on this now that xgl has had a bit of a setback.  Of course, exa seems to be part of the reason for the setback.  boo hoo."
    author: "MamiyaOtaru"
---
At the recent <a href="http://wiki.x.org/wiki/LinuxTagMeeting2005">European X.Org Developers Meeting</a> KDE developer and <a href="http://www.trolltech.com/">Trolltech</a> employee <a href="http://www.kdedevelopers.org/blog/14">Zack Rusin</a> presented a <a href="http://lists.freedesktop.org/archives/xorg/2005-June/008356.html">new acceleration architecture</a> named Exa (eyecandy X architecture) for <a href="http://x.org/">X.org</a>. Being based on KAA (<a href="http://www.usenix.org/events/usenix04/tech/freenix/full_papers/anholt/anholt_html/">KDrive</a> acceleration architecture) it's designed to be an alternative to the currently used XAA (XFree86 acceleration architecture) with better acceleration of XRender which is used by composite managers for desktop eyecandy effects. The next X.org release which is expected to contain Exa is <a href="http://lists.freedesktop.org/pipermail/xorg/2005-June/008325.html">planned to be released in September</a>.



<!--break-->
<p>Making existing graphic card drivers using XAA take advantage of Exa is said to be easy and Zack volunteered to, besides bringing Exa into X.org, to port all unmaintained graphic card drivers as soon as he gets the respective hardware in his hands. Exa is meant to improve the desktop experience and bring users some much needed eye-candy while <a href="http://www.freedesktop.org/wiki/Software_2fXgl">Xgl, an X-Server</a> layered on top of OpenGL is being worked on.

