---
title: "Application of the Month: KBear"
date:    2005-02-14
authors:
  - "ateam"
slug:    application-month-kbear
comments:
  - subject: "Why?"
    date: 2005-02-14
    body: "This is a lame app for me, as Konqui+bookmarks+kwallet is more than enough to deal with FTP sites, not to mention kio is the real strong here, so FTP access could be done transparently from applications.\nThere is a plus in this app I'm missing (besides the fact it is specially tought for FTP transfers?)\nBesides that, I'm not sure if Word-like MDI fits well in KDE, maybe a SDI or tabbed MDI should be a better choice..."
    author: "Shulai"
  - subject: "Re: Why?"
    date: 2005-02-14
    body: "I also love Konqueror with kio-slaves, but on my Suse 9.1 with Kde3.2, when I ftp my website it shows me the content of the directory as a webpage instead of as in a filemanager view. On my old Suse 7.3 with Kde3.1 I have no such a problem.\nI couldn't figure out how to change it and for this reason I have to use Kbear.\n\nAny help?\n\nThanks\nKlaus"
    author: "Klaus"
  - subject: "Re: Why?"
    date: 2005-02-14
    body: "In the View menu, is \"Use index.html\" checked?  Does unchecking that option help?"
    author: "Spencer"
  - subject: "Re: Why?"
    date: 2005-02-14
    body: "\"Use index.html\" is greyed out. However I don't think that is the problem.\nThe fact is that my Konqueror produces an html file that shows the content of the directory. This presents no problem if you want to download files, but it's useless if you want to upload them.\n\nThanks for the prompt reply anyway.\nAny other hint is welcome, because I'm a big fan of kio-slaves and I want them to work the way I like.\n\nKlaus"
    author: "Klaus"
  - subject: "Re: Why?"
    date: 2005-02-14
    body: "so the adress is ftp://blabla, and you don't see the iconvieuw tools.\n\nhmmm. strange. I have this if I browse a ftp site with http (duh) but never seen it with ftp. try if you can change it with - view -> viewmode (? me dutch, its the top choice) and check if you can choose another vieuw."
    author: "superstoned"
  - subject: "Re: Why?"
    date: 2005-02-14
    body: "Does your FTP connection (in Konqi) go through a proxy?  If so, you will always see the result as an HTML-formatted page, as that is what the proxy returns.  It can be fixed by changing your proxy settings - you could tell it not to use a proxy for FTP at all.\n\nIt seems as if nobody ever came up with a proper way of supporting FTP connections through a proxy - it's just an http hack!\n\n-- Steve"
    author: "Steve"
  - subject: "Yes, it was the proxy!!!"
    date: 2005-02-15
    body: "Thanks Steve,\nyou saved my day and I learned something new.\n\nWithout any special reason I had chosen to use a proxy for ftp....\nI removed that and it works perfectly fine.\n\nKlaus\n"
    author: "Klaus"
  - subject: "Re: Why? and why not?"
    date: 2005-02-14
    body: "If the developer wants to mantain it...and the users to use it...who cares?\n\nDue to this article I've spent a bit more time with it and I think it is quite good. But the following is what I'd had writen for the 2 times I had tried before:\n\n------\nIt's a real pain in the eyes.It represents the worst in terms of usability I've seen in KDE (a part from a poll that come out recently asking for experiencies on every version since the beginings -> not direcly related to KDE :P )\n\n- Awful splash screen.\n- Really, really unfriendly wizard. A 2nd stopper.\n- Sorry I Kan't bear it...directly close the app...Wow! from which of the two tray icons you want to quit?\n\nReally...I use KDE for its user friendliness and I find more friendlier ncftp when I need it.\n------\n\nCleaning up this is a top 10 app that I will use from now on."
    author: "Kapde"
  - subject: "Amazing someone could make this"
    date: 2005-02-14
    body: "on their own ....  Way to go!!\n\nThis really shows what is possible with KDE framework."
    author: "More-OSX_KDE_Interpollination"
  - subject: "GFTP!"
    date: 2005-02-14
    body: "Well, after trying KIOSlaves, KBear and KFTPGrabber,\nI finally tried GFTP. I had major problems with all KDE\nFTP apps. Some servers showed directories as files,\nothers used a wrong encoding, and so on. Now, I'm\nfinally using GFTP and everything works as indended until\nthe problems with all those various FTP servers out there\nare resolved.\n"
    author: "Martin"
  - subject: "Re: GFTP!"
    date: 2005-02-14
    body: "Just remember that one problem with FTP servers traditionally has been that Windows clients don't handshake. This means if there is a problem during a transfer there is no verification. This error has shown up in the past with KIO FTP and people have reported that gFTP worked fine. I don't know if it does the handshaking, but one way not to be bothered by users asking why servers don't work right is to perpetuate the problem and not use it. However if you are using FTP for web hosting or anywhere that is password protected then you are sending your passwords in clear text. It is incredibly easy to sniff these. You can download the software and check it out easily. Hosting companies I used in the past recommended that I change my password weekly for security. Right. That won't be a problem.\n\nThe obvious conclusion is that FTP is okay as an alternative to HTTP for download, but in general it just sucks! I strongly recommend using KIO fish (ssh/scp) for secure file management as it works with most hosting solutions. Unless of course you are just nostalgic. You can then do something very cool if you like. You can use ssh-keygen and generate a passwordless key. Upload your remote key and then you can access the host directly and instantly in Konqueror without being prompted for a password. Right click on the statusbar of Konqueror and split the window and you can drag and drop files between remote and local locations easily.\n\nWhat is really cool about KDE is the transparent access of KIO, one of it's best kept secrets. It makes sense. In a connected world why do you need different programs just to deal with files in different locations or that are accessed with different protocols? Imagine needing a special file manager for NFS. KBear and gFTP seem to me to be pure nostalgia. If there are problems with KDE's FTP KIO slave it seems logical instead of writing another FTP client to work on KIO FTP performance and then create a client that uses KIO. Then your client could use any available protocol. If you like KBear, and I haven't looked for a long time but it seemed like a decent program, what do you do if you decide to use fish?\n\nIn the end, almost all problems with FTP can be traced back to administrators that don't know what they're doing. Ironically almost all of these people are being paid money for these services and it is customers who are paying money to them that are choosing alternate solutions that end up perpetuating the problem. The logical question is... if all these companies with hosting problems started losing all their clients to hosting companies that did things right how long do you think these problems would exist? The conclusion is that you probably shouldn't think you can start a hosting company and pick up a flood of business doing it right, because people are a lot happier to risk their data and security.\n\nTo paraphrase Winston Churchill, for bad FTP to rule informed users must do nothing. When your browser doesn't work because of broken Javascript or IE extentions do you contact the webmaster and ask them to support it or dual boot?\n\nBTW nothing personal against KBear. I'm sure it's a good application for what it does, but it makes as much sense to me as building a KDE application without using DCOP or making plugins without KParts. KDE is full of a lot of very good architectural ideas that are at first foreign to people coming from an operating system designed for two 360K floppies. Windows is a perpetual kludge on top of the original QDOS (Quick and Dirty Operating System). People naturally try to duplicate all the bad things they did there when they move to Linux because that is all they know. They don't understand why they should not run as root and why HTML mail is not good... and thanks to very poor promotion on the part of KDE they have no clue that KIO even exists. The number one request for Quanta over the years has been an FTP manager and in 99% of those cases they didn't know that Konqueror can do FTP, let alone SFTP and fish, or that any properly set up KDE application inherits this network transparency. Why not make KIO the application of the month some time? It's certainly one of the coolest things in KDE! Network transparency for all applications is a lot cooler than finding a program for FTP, another for SFTP, another for SSH, another for SMB, etc... and then having all applications tied to the local net."
    author: "Eric Laffoon"
  - subject: "Re: GFTP!"
    date: 2005-02-14
    body: "I was asked to come up with a replacement to ftp for our servers - something preferably like a windows ftp client, and hopefully with other features that could show the difference between the local and remote server dir's\n\nOnly one app will do what we needed : krusader  - part of the kdedev packages - combined with setting up key exchanges makes a fully secure sftp/ssh ftp like program with some of the best tools and plugins (and even book marks) - they love it :)\n\nhttp://krusader.sourceforge.net/"
    author: "Luke"
  - subject: "Re: GFTP!"
    date: 2005-02-14
    body: "krusader is cool, for sure. but I'm sure you didn't really look deep enough in Konqueror. split screen, anyone? you can have almost all features of krusader, believe me. Not all, of course. not all. but mostly enough, I think."
    author: "superstoned"
  - subject: "Re: GFTP!"
    date: 2005-02-16
    body: "I love konq - dont get me wrong....but krusader is really shanzzy and keeps the ex-windows lovers a nice interface they think is an ftp client (correct me if I am wrong, but krusader is using all the kio slaves to do everything).  But the major killer feature is the many ways it can do a diff on opposing file systems.  But not just a time stamp check - about 3 types of comparisons.\n\nPlus it has bookmarks (like konq bookmarks) - it really is a pretty amazing app :)"
    author: "Luke"
  - subject: "Re: GFTP!"
    date: 2005-02-14
    body: "I totally subscribe to your comment, Erik!\nI was totally amazed when I figure it out that I could just deal with files/dirs over my webpage's ftp interface the same way I deal with files/dirs in my hard drive. And I like it a lot how you can select a file, then \"Open with...\" and, in the end, it asks if I want to upload the changes, even if it is not a kde application that i've opened with... It's just brilliant."
    author: "blacksheep"
  - subject: "Re: GFTP!"
    date: 2005-02-14
    body: "how come krusader never got to be app of the month?\n"
    author: "anonymous coward"
  - subject: "Toolbar Icons...."
    date: 2005-02-14
    body: "Krusader toolbar icons looks much more \"pretty\" than \nthose in the KBear screenshots.\n\nAre there any screenshots of KBear with \"pretty icons\" ?\n\ni.e. The theme used for KBear screenshots is not really appealing...\n\nI don't know what \"theme\" is that though.\n\nFred."
    author: "fprog26"
  - subject: "Re: GFTP! - Towards KDE 4.0 desktop and community"
    date: 2005-02-14
    body: "KBear is a cute little application, especially if you don't know Konqueror/KIO slaves better. I have been using KDE for over 7 years now, and tried a lot of these small apps to see what extras they offer, or, unfortunately, because I just did not know better. And that, I think, is the key problem that we have to deal with. \n\nRecently, I have been helping a couple of my friends discovering parts of KDE and KDE applications, thanks to initiatives such as http://kde-apps.org, the AotM team, developer blogs and a lot of reading all over the internet. Yet, the fact remains, that they did not find the information fast enough on their own. I discussed this with KDE users and some contributors, and the nearer we get to KDE 4.0, the more articles, blogs and remarks appear. Why do a lot of computer and even KDE users not know about KDE's cool features and framework, and have a hard time finding out? \n\nDepending on the level of computer/*nux experience of the user/possible contributor or developer, I think new users/contributors have to do deal with the following KDE (related) issues. (Please complete this with your own findings, for I think pinning down the main issues can help finding/optimising solutions):\n- Users do not really understand how their computer/OS works. This may seem trivial, but basic knowledge about the components of your computer, multi-user/security issues and the link of these to your OS and KDE are crucial. (Go quality team and KDE User Guide, it already has a lot of info!)\n- It still takes a lot of effort/training/guidance to see the benefits that KDE offers for an average/\"normal\"/former Windows users. People are trained with Windows tools and know them, their names, or at least where to look/ask for them.\n- People do not like being forced to read piles of documentation and are often hesitant to approach communities/forums (effort, attitude, etc.). Attractive lay-out/presentation, slideshows/presentations and interactive tutorials, however, can make it a lot easier to understand and less unpleasant.\n- Documentation and valuable information are often non-existent, hard to find (not clickable or too many clicks), fragmented or presented in an unattractive or amateuristic way. I know how much effort KDE developers/contributors put into this, and my heart just bleeds each time I see it, but this is how things  still are.\n- KDE as a product/initiative is not presented as professional, innovating, appealing and well-organised as it could/should be. Please do not be offended/flame me, but if I had not seen KDE years ago, I probably would not have given it a second thought after seeing http://www.kde.org or http://dot.kde.org in this day and age. Compare KDE.org with that of other succesful and good looking open source and commercial/proprietary websites, and be honest to yourself.\n\nWhat do we already have?\n- A great (huge and fantastic), sophisticated and complex product/project\n- A huge, motivated team of devoted developers, contributors and enthusiastic users (feedback), each with their own capabilities and opinions, but \n- all striving for a better KDE desktop.\n- Awareness of the fact that we need to create more resources -- read: (power) users that evolve into contributors/developers)-- to maintain and improve KDE\n- A huge amount of (hidden/scattered) information and ideas in KDE handbooks, on (international) websites/forums and in peoples minds.\n\nHere are some of the solutions/means of coming to solution - I tried to emphasize how newcomers should be able to associate parts of KDE, with conventional concepts:\n- Striving for a more structured and integrated KDE community to reduce the complexity. By this I do not mean that we should strive to force KDE into some kind of traditional hierarchical business/project model, but I do think that we can optimise a lot of efforts that are being made by also integrating our community so it will be more transparent for newcomers and new contributors/developers.\n- Broadening the project management to include updating KDE http://www.kde.org and international/community webpages, more synergy. Offer a framework for Picture yourself a KDE project consisting of teams for product development (coding, usibility, documentation), promotion/public relations (website, conferences, etc.), user assistance by redirecting(FAQs, forums, Wiki IRC), feedback and quality management. Hmmm, sounds familiar, doesn't it? It looks as if KDE already has everything, but that it is not being used/presented optimally.\n- Updating KDE.org so it doesn't just state, but also reflects the \"KDE conquest plan\", our motivation, vision, strategy, technology, usibility, and community that set us apart from commercial products and other competitors. KDE.org should represent the integration and technological excellence that KDE stands for. This is the place everybody that does not know anything about KDE / KDE development visits to get inspired, informed, and to find what he/she is looking for that concerns KDE news, usage, communities etc. I know that maintaining such a huge website is hard work and that you have to put up with a lot of *, but I also see a lot of potential that is not being used: \"This page was created using KDE's Quanta Plus and designed by KDE Artists...\".\n- Optimally using people's skills, such as with the KDE-Look contests. Let developers/promotors and artists work together more closely and get more organised, and most of all: make it explicit, so newcomers know where to look.\n- Making it extremely easy, almost inevitable, to get involved with KDE to get more users and contributors. Offer people the possibility to \"sign up for KDE\" via KDE.org and the KDE desktop immediately, so they get those chunks of valuable information on a \"Crystal platter\": offer the possibility to sign up for a group of KDE communities/forums/mailinglists instead of only referring. Treat them the way you would have liked to be treated when you were a newbie or had question. Have them tell their friends and relatives about it! \n- Give the main KDE.org website the standard KDE look and feel (e.g. using Crystal icons), and make it visually appealing.\n- Offer people to search \"KDE\", by an integrated search from the website/desktop: their desktop, documentation, themes, applications, forums, wikis, presentations, etc. Make them aware of the fact that they CAN conquer their desktop AND the web, just by using (and contributing) to KDE.\n- Structure (international) website integration/synchronisation with KDE.org. Make it easy to create/update their website by translating \"modules\" of KDE.org, and also have them contribute to the main site. This could be like the documentation/application translation, but then for the website.\n- Make it easy for users and companies to donate/provide sponsoring (equipment, money, etc.), and let them and others see what happens with these donations. Again, KDE.org has to be appealing enough to make it almost inevitable for people/sponsors to contribute in every way they can and have them feel involved.\n\nWell, I hope nobody got not offended, and that, although you may not agree with me (on everything), you think about my considerations and ideas as positive feedback of a KDE user and contributor. I have seen a lot of great initiatives in all the areas I mentioned, and just want to help improve KDE as a whole. I would love to see a fully integrated KDE 4.0 desktop and community, with all the above or similar improvements, because I, like so many others around the globe, believe that KDE is a great project with an even greater potential."
    author: "Claire"
  - subject: "Re: GFTP! - Towards KDE 4.0 desktop and community"
    date: 2005-02-14
    body: "Hear hear!\n\nJust a comment on the topic of tips and tricks for KDE.  One of the problems with offering tips for KDE is that so many things are configurable.  It's hard to say \"Hit CTRL-ESC to do this\" if that shortcut can be changed by the user.\n\nI looked at the recent article on the dot about KDE tricks, only about 25% of the tricks they mentioned actually worked as described on my system becuase I had changed some setting over the years that altered the behaviour they were referring to.\n\nI suppose that is the tradeoff with configurability.  It's hard to tell anyone else how to do things on KDE because they may have a completely different setup."
    author: "Leo Spalteholz"
  - subject: "Re: GFTP! - Towards KDE 4.0 desktop and community"
    date: 2005-02-14
    body: "If that article is the one I think, the problem is that the article sucks.\nIt was written by a guy with just a bit more knowledge about KDE that a complete newcomer.\n"
    author: "Shulai"
  - subject: "file list not updated after transfer?"
    date: 2005-02-14
    body: "I remember that when I used kbear, the server file list was not updated when I transferred a file to it, I had to manually do a refresh each time. Is this a known problem?\n\nI hope development will restart soon, there's certainly still room for a good stand-alone ftp client."
    author: "Anonymous Coward"
  - subject: "Re: file list not updated after transfer?"
    date: 2005-02-14
    body: "Yes!! that is another problem, \nI think that kbear is ever in beta stage, good idea but..\n"
    author: "Vento"
  - subject: "Re: file list not updated after transfer?"
    date: 2005-02-14
    body: "http://kde-apps.org/content/show.php?content=11464\n\nThat's why i use kasablanca"
    author: "gnumdk"
  - subject: "kbear, humm.. :-("
    date: 2005-02-14
    body: "I think that is a usefull application but not work well!!\n-manager of the windows is a caos\n-sometime the application crash unexpectly way, *I think* that it does not manage well unexpected events of the ftp actions/response or connection break or ... , thus I use command shell to transfer file!\n\nI'm using suse 9.2 kde 3.3.0 I hope that my release version of kbear is an old one\n"
    author: "Vento"
  - subject: "KDE HIG? not here."
    date: 2005-02-16
    body: "Just looking at the HIG of this application and its a total mess. I like how KDE users will argue \"we have HIG too\", but this KDE program is another example of not following even basic HIG.\n\nLook at the server connect menu its an absolute mess, and theres far too many buttons in the main menu, KBear needs lots of HIG love. "
    author: "ricky"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-16
    body: "Well, whether the developer follows the KDE HIG or not, is up to him."
    author: "ac"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-16
    body: "And since it's not in KDE's CVS the usual team cannot easily change it.\n"
    author: "Anonymous"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-16
    body: "yeah, but the difference here is that Gnome related applications (not yet officially included) will still maintain HIG. Kbear is a native KDE application, and doesnt follow any HIG, it wouldnt surprise if the developer didnt even know a KDE-HIG existed. \nBasically what I'm saying you should be able to use a KDE application, without breaking HIG and a uniform desktop. Just because its not mananged officially by kde.org shouldnt be an excuse for laziness by developers of KDE applications."
    author: "ricky"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-16
    body: "> Basically what I'm saying you should be able to use a KDE application, without\n> breaking HIG and a uniform desktop. \n\nok, wrong wording there, but, When Developer A, wants to make Kapplication, it should be bound to the HIG regardless. Thats my point. No ifs or buts. Make a uniform desktop, instead of spaghetti of Applications with there own sense of 'HIG.\nQT Applications on KDE is different, same with GTK+ Applications Gnome, I'm pointing out native Applications here.\n"
    author: "ricky"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-16
    body: "KDE HIG compliancy will continue like as is, that is an app is only then truely a KDE app when the KDE versions of features are used instead Qt or re-invented custom ones are used. And as soon as KDE features are used they are ensured to follow KDE's HIG."
    author: "ac"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-16
    body: "> it wouldnt surprise if the developer didnt even know a KDE-HIG existed.\n\nStop moaning. Mail them, file a bug report."
    author: "Anonymous"
  - subject: "Re: KDE HIG? not here."
    date: 2005-02-17
    body: "What is this HIG stuff?  Is it like DCOP?\n\n\nAndy"
    author: "Andy"
  - subject: "kbear saved my bacon"
    date: 2005-11-29
    body: "I put an ftp server on my mom's computer before I left the state knowing I could use it to retreive my music, anime, etc.\n\nBut once I got out of the local isp range, I couldnt get back in, something about passive, and the windows firewall, and what, i don't know.\n\nKonq _barely_ could connect, but timed out constantly.\n\nI tried from windows, from linux, from other ip ranges, nothing would work\n\nTonight I tried Kbear, and altho it looks kinda funny, its the only ftp client that works. I have no problems like before.\n\nYay Kbear, now I have all my jap tv series back! GTO!"
    author: "todd yarling"
---
This month in our series "Application of the Month" we show you the alternative FTP client, <a href="http://kbear.sourceforge.net/">KBear</a>. As usual we have an interview with the author and a description of this powerful but easy to use program. Enjoy Application of the Month in <a href="http://www.kde.nl/apps/kbear/">Dutch</a>, <a href="http://www.kde.org.uk/apps/kbear/">English</a>, <a href="http://www.kde-france.org/article.php3?id_article=129">French</a>, and <a href="http://www.kde.de/appmonth/2005/kbear/index-script.php">German</a>!

<!--break-->
