---
title: "Dutch Agency Equips 200 Users With NX-based KDE Desktops"
date:    2005-06-01
authors:
  - "fmous"
slug:    dutch-agency-equips-200-users-nx-based-kde-desktops
comments:
  - subject: "NX technology"
    date: 2005-06-01
    body: "Where can I find more info about the NX technology? Somehow I could not find it on the NoMachine website, nor on Google.\n\nIs KDE going to include this NX stuff sometime soon?"
    author: "anonymous"
  - subject: "Re: NX technology"
    date: 2005-06-01
    body: "A few months ago I tried out FreeNX at work on a Gentoo server, and it worked very nicely.  Try googling for \"freenx\" for more information.\n\nNX doesn't need to be \"included\" in KDE, although work has already been done to integrate the client with KDE.\n"
    author: "Ellis Whitehead"
  - subject: "Numbers?"
    date: 2005-06-01
    body: "The article doesn't mention what hardware and bandwidth the server (servers?) have.  Their local networks probably have a pretty large amount of bandwidth, but what type of connection did they have between Arnhem and Amsterdam?  Was it an OC-192 connection, or was it a 56k modem connection?  Was the line shared with other things (like a web, ftp, e-mail, etc servers), or was it only being used for NX?\n\nI also have to say: YAY FOR KDE!  Hopefully that is just the start of many switches."
    author: "Corbin"
  - subject: "Re: Numbers?"
    date: 2005-06-01
    body: "One thing to keep in mind with NX is that it is incredibly efficient, more so than anything else available. You can get a reasonable performance with dial up and any bandwidth above that is very good. I would guess they have some kind of broadband or better connection to the internet which is all you need. As far as hardware goes you can run roughly one client per MHz of processor power on the server. So a 3 GHz regular box could run 30 clients with no noticable slowdown. At least this is the data I've been given. Also as you may have interpreted this does not require dedicated connections as it's just another protocol over the network. "
    author: "Eric Laffoon"
  - subject: "Re: Numbers?"
    date: 2005-06-01
    body: ">As far as hardware goes you can run roughly one client per MHz of processor >power on the server. So a 3 GHz regular box could run 30 clients with no >noticable slowdown. \n\nInteresting calculation...\nWell, it's close to midnight. Perhaps I'm just too tired and cant\nfigure it out right now. I'll try again tomorrow ;-)\n"
    author: "Martin"
  - subject: "Re: Numbers?"
    date: 2005-06-02
    body: "> As far as hardware goes you can run roughly one client per MHz of processor power on the server. So a 3 GHz regular box could run 30 clients with no noticable slowdown.\n\nNot to pick nits, but that does not compute.  Did you mean that it costs about 100 MHz of CPU per client (3 GHz/30 = 100 MHz), or that a 3 GHz machine can host 3000 clients (3 GHz / 1 MHz = 3000 clients) ?\n\nI guess it must be the former...?"
    author: "LMCBoy"
  - subject: "Re: Numbers?"
    date: 2005-06-03
    body: "Normal ADSL and SDSL lines are used over a private network of the Telco KPN.\nBandwith varies between 128Kb and 2Mb (Server (Amsterdam) side). Overbooking varies from 1:10 to 1:1 (mainly the larger sites). Lines are not shared for anything else. The user gets internet access thrue another 2Mb SDSL line in Amsterdam which is used for Amsterdam office use also. The sessions are running in Amsterdam so for the user there is nog difference.\n\nDoes this answer your questions? :-)\n\nKind regards,\nMichel"
    author: "MvanHorssen"
  - subject: "The Future"
    date: 2005-06-02
    body: "\"The Future\n There are already signs that other regions want to setup their ICT in a similar way and want us to deliver the services for it. Also we probably need at least one more NX Server for SVMG to serve all workstations. So we should consider load balancing. We already have an eye on a project that could help us.\"\n\nDoes somebody know what the project is? I'm looking for the same thing."
    author: "LB"
  - subject: "no KDE for work"
    date: 2005-06-02
    body: "i'm not very fond of KDE for computers used for daily business operations. we tried once and employees started to explore KDE and all the settings and small programs it comes with (no kiosk used, since i consider it to be to intrusive) instead of doing their work. Gnome is much better for this, it resembles windoof way more as it has no additional features, is simple and easy and hasn't a ton of settings to explore. it covers only the basic functions and nothing more. so it's perfect for the job.\nsince we switched to Gnome work is done again, since theres nothing to diuscover employees lose their interest and get back to what they are paid for."
    author: "Andy"
  - subject: "Re: no KDE for work"
    date: 2005-06-02
    body: "Hi, thanks for your comment.\nHowever, for the future, please direct all your trolling to gnomedesktop.org.\n\nThank you,\nThe Management."
    author: "Anonymous"
  - subject: "Re: no KDE for work"
    date: 2005-06-02
    body: "Hmm, if i read his post, i get the feeling, its rather negative of gnome. (like windows, no extra feature, no special effects, nothing new...).\nSo it is no trollpost but dark? humor."
    author: "Anonymous"
  - subject: "Re: no KDE for work"
    date: 2005-06-02
    body: "> and employees started to explore KDE and all the settings and small programs\n> it comes with (no kiosk used, since i consider it to be to intrusive)\n\nI have some hints:\n\n- Don't install these \"small programs\" that are not useful for daily business.\n  (If you can't, you are using the wrong distribution...)\n- Try kiosk, it should enable you to create what you need. No less, no more\n- You can pre-setup KDE to look and act more like windows as the standart KDE \n  setup if you think it would be helpful"
    author: "ac"
  - subject: "Re: no KDE for work"
    date: 2005-06-03
    body: "Try another approach: LET them play with it for a few days.  Let them see what it can do, and configure things how they like them.  When they're done, if they're capable individuals, you might just find that they've optimised things so they can work most efficiently.  KDE continually surprises me, in terms of how fast I can work when I get to know it and configure the settings to suit me."
    author: "Lee"
  - subject: "OT: Details about x.org improvements by KDE devs"
    date: 2005-06-02
    body: "Here:\nhttp://osnews.com/comment.php?news_id=10311\n\nAnd the details are in the x.org mailinglist:\nhttp://lists.freedesktop.org/archives/xorg/2005-April/007446.html\n\nI could not find out if the patches have already been committed, but I think so..."
    author: "ac"
---
Last weekend at <a href="http://www.wallsteijn.nl/">Annahoeve</a> in Achtmaal the <a href="http://www.vluchtelingenwerk.nl/nl/english/">Dutch Refugee Council</a> of Midden Gelderland announced that they succesfully completed a pilot where they migrated 100 machines and 200 users using NX technology and the KDE desktop. Jasper van der Marel held a presentation during the Dutch KDE-PIM meeting where he explained how the current setup was done.






<!--break-->
<p>The goal was reduce costs and to be less dependant on closed source software. At first the Dutch Refugee Council opted for a <a href="http://www.ltsp.org/">LTSP solution</a> but because this was missing important compression features they choose the NX technology from <a href="http://www.nomachine.com/">NoMachine</a>. Some of the applications used are OpenOffice.org, <a href="http://www.kontact.org/">Kontact</a> and <a href="http://extragear.kde.org/apps/kiosktool/">Kiosktool</a>. </p>

<p>The project started in 2003 when they did some tests on their local network and in the first half 2004 they tested connections between Arnhem and Amsterdam, two cities 100km's apart. At a certain point the sysadmins of the Dutch Refugee Council of Midden Gelderland had to decide on a desktop. The decision fell in favour of KDE chiefly because of the availability of the <a href="http://www.kde.org/areas/sysadmin/">Kiosk framework</a>.</p>

<p>Because of this successful migration at the Dutch Refugee Council of Midden Gelderland other regions are now also planning a similar migration for their desktops. You can read more about this migration on <a href="http://www.opensourcenieuws.nl/index.php/content/view/2425/57/">Open Source Nieuws Nederland</a> (Dutch). A <a href="http://www.kde.nl/agenda/2005-kdepim-meeting/artikel-vluchtelingenwerk_en.php">quick translation into English</a> can be found at the Dutch KDE website thanks to Rinse de Vries.</p>




