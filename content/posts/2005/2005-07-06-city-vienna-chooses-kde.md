---
title: "City of Vienna Chooses KDE "
date:    2005-07-06
authors:
  - "mkonold"
slug:    city-vienna-chooses-kde
comments:
  - subject: "nice :)"
    date: 2005-07-05
    body: "It's nice to see, that Austria (my homecountry) is moving into the right direction. :)"
    author: "Patrick Trettenbrein"
  - subject: "?"
    date: 2005-07-06
    body: "Where are all those comments that I just read an hour ago?\n\n"
    author: "TIM.."
  - subject: "Re: ?"
    date: 2005-07-06
    body: "The thread was geared by a troll that made false allegations towards Qt. When he started to get rude in the end of the thread I took the entire thread down.\n\n[Thread Ended, Please Move On]\n\n\n"
    author: "Daniel Molkentin"
  - subject: "Not surprising"
    date: 2005-07-06
    body: "Lots of European cities and governmental agencies seem to be switching to Linux these days. Most use KDE rather than Gnome as the DE, so I'd imagine WEINUX would be no different. Adoption seems to be forthcoming in the US, but a little slower. Linux in the US has the aura of geekdom and difficulty for the average Joe. Some is true, but especially since good DEs like KDE present a logical, familiar, and easy-to-use GUI, a lot of the old \"everything has to be done in the terminal\" can be gone if you want it to.\n\nWHY they are switching from Windows to Linux is the real question for us here. Not wanting to pay royalties to a foreign country (or at all?) Stability? To simply make a statement? What do you think? I'm asking a polite question..."
    author: "SuSE_User"
  - subject: "Re: Not surprising"
    date: 2005-07-06
    body: "I think all those reasons you make + freedom to change and control the code.  Norway, for instance, has put its foot down on closed formats.  I think that makes a lot of sense not just for Norway but for everyone.  Should a company really be using a proprietary format to store company information or secrets in?  \n\nWho knows what's going on with the proprietary format, it could be storing more information than you know and on the other hand one day you might find yourself unable to access your data in a useful manner.  What if Microsoft finds cause to revoke your software license and you have 100% of the company's information assets encoded in a Microsoft format?  You're screwed!"
    author: "Anonymous"
  - subject: "Re: Not surprising"
    date: 2005-07-06
    body: "I didn't immediately think of the revocation of licenses = unable to retrieve files. \n\nThat WILL happen with the \"Next Generation Secure Computing Base\" aka Palladium if it and the Fritz chips become part of the \"trusted computing\" project being tossed around by Microsoft, Intel, the **AA's, and others. If they pull your authentication, you cannot and nobody else can access your files made with the programs whose licenses were revoked. It could have good uses, but I am leery of somebody else tampering around with my box. If I did something wrong, then sue me like the law says."
    author: "SuSE_User"
  - subject: "Re: Not surprising"
    date: 2005-07-06
    body: "Hmm, don't get me wrong, but with DEs I see KDE is more for geeks and Gnome for the \"ordinary employees\". Most poor souls come from m$ and they want a simple and less feature-rich desktop. KDE is way to powerful for most of them to handle (I've seen a secretary going crazy looking for the menubar with osx-like style, she just didn't understand it and she's normally clever and not stupid; I guess peoples habits get damaged using m$ crap). Maybe after some month of training a switch can be considered.\nyes, i've started with Gnome 1.1 myself some years ago and switched to KDE 2.0 when is was released."
    author: "Andy"
  - subject: "Re: Not surprising"
    date: 2005-07-07
    body: "In a workplace, if being \"too powerful\" is a problem, that's when the administrator steps in and disables/locks out functions that are not desired. KDE provides the means to do this with fully customisable toolbars/menus and the Kiosk framework."
    author: "Paul Eggleton"
  - subject: "Re: Not surprising"
    date: 2005-07-07
    body: ">  Most poor souls come from m$ \n\nWell, forum trolls keep asserting that KDE was a Windows clone and that's why they don't use it.  If you took that face value that would mean that KDE was actually the better DE for \"ordinary employees\" switching from Windows. \n\nAnd no, KDE is not just a clone. \n\n\n> and they want a simple and less feature-rich desktop. \n\nIf that were true (*) it could be done by not installing so much software (a much underestimated concept! ;-)  and by using the kiosk framework.  I doubt the Vienna staff will get to see the full array of educational, development and multimedia software, and all the options.  \n\n\n(*): I doubt especially the \"less feature-rich\" part, many of KDE's cool features (e.g. mini-CLI via Alt-F2, IO slaves, mouse gestures for the whole desktop and its apps, ...) are hidden and thus don't get in the way of someone who does not use them.\n"
    author: "cm"
  - subject: "Re: Not surprising"
    date: 2005-07-07
    body: "I always find it strange that people keeps claiming things like that Gnome are more for the \"ordinary employees\". I guess it's some kind of PR spin from the Gnome camp that have caught on, and people keeps on pushing it. It's pretty stange, since close to all the Linux distributions aimed at the newbies and the ones consider most user friendly and easy to install, all are KDE based. When the companies who try to make a living selling easy to use Linux distributions all chose to use KDE, why do people still insist to claim that Gnome are better suited? \n"
    author: "Morty"
  - subject: "Re: Not surprising"
    date: 2005-07-07
    body: "I'm not from Gnome, i haven't used it since KDE 2.0. Only I everyday experience people whose systems were switched having an easier time getting used to Gnome than KDE.\nFor myself, I wouldn't want to work without fish:, man: or webdav: and all those little helpers like Klipper or SuperKaramba. And don't forget about Kontact, although its useful with Gnome too (since Evolution is, well..., somewhat... ugly *duckandcover*)."
    author: "Andy"
  - subject: "Re: Not surprising"
    date: 2005-07-09
    body: "What describes user/case like that is called \"Learned Helplessness\".\n.. i think\n \n"
    author: "something"
  - subject: "Re: Not surprising"
    date: 2005-07-06
    body: "> WHY they are switching from Windows to Linux is the real question\n\nAny sane buyer should avoid monopolists as much as possible.\n\nIn a healthy buy-situation you dictade your demands. In the monopolist situation, you are ignored."
    author: "Dev"
  - subject: "Re: Not surprising"
    date: 2005-07-06
    body: "Don't forget virus attacks, ad-ware etc.\n\nBut don't underestimate sentiment. Europe, due to its long history and dense population, has a tradition of taking care of each other (whereas the US has one of pioneers, american dream). In such a tradition doesn't fit extremely wealthy people nor ghetto's. If MS was more a technological company, rather than a financial one, this wouldn't be a big issue as its now IMO."
    author: "koos"
  - subject: "Re: Not surprising"
    date: 2005-07-07
    body: "> Lots of European cities and governmental agencies seem to be switching to Linux these days. Most use KDE rather than Gnome as the DE\n\nThis Qt Licensing crap is getting quite nasty. Have a look at:\nhttp://www.ofb.biz/modules.php?name=News&file=article&sid=364&mode=&order=0&thold=0\n\nSo many half-truths there that i lost count."
    author: "Anonymous Coward"
  - subject: "Re: Not surprising"
    date: 2005-07-07
    body: "No surprise really. It was some years ago Timothy R. Butler turned that site into a laughable Qt flamebait."
    author: "ac"
  - subject: "City of Vienna Chooses KDE"
    date: 2005-07-06
    body: "... of course they do!"
    author: "ac"
  - subject: "The only sensible option,"
    date: 2005-07-06
    body: "of course is KDE. \n\nCongratulations KDE :)"
    author: "Anonymous"
  - subject: "Kool Desktop Environment?"
    date: 2005-07-06
    body: "I thought history had been rewritten for K in KDE not to mean \"Kool\" ages ago already, why is it mentioned in TFA? =P"
    author: "ac"
  - subject: "Kiosk"
    date: 2005-07-06
    body: "I wonder how much Kiosk played a role in this...(if at all?)"
    author: "am"
  - subject: "Re: Kiosk"
    date: 2005-07-06
    body: " I'm almost 100% sure that this has been up for debate amongst the desicion-makers of this project.\n\n Kiosk fills a niche in this area and it is easy to use yet powerfull. I would not deploy a desktop scenario without having finer-grain control then ordinary the user:group mechanism.\n\n~Macavity"
    author: "macavity"
  - subject: "Ending of thread"
    date: 2005-07-06
    body: "While I do not mind the killing of a thread, I would appreciate it if a single message is posted saying that a thread was killed and why. Sad that somebody had to enquire about the thread."
    author: "a.c."
  - subject: "Re: Ending of thread"
    date: 2005-07-06
    body: "Yeah I'm always the last to complain about such things, but just deleting threads with no trace always makes me feel uneasy.  Is this now going offtopic enough so that this thread will also be deleted?\n"
    author: "JohnFlux"
  - subject: "Re: Ending of thread"
    date: 2005-07-07
    body: "I'd like to second that. \nwhile it is certainly the decision of the maintainers of this cite, to close or kill threads for rude trolling, at least a note would be in order."
    author: "El Pseudonymo"
  - subject: "Re: Ending of thread"
    date: 2005-07-07
    body: "We editors don't generally delete non-abusive threads as a rule, so this was basically a mistake.  We are discussing it internally.\n\nDanimo did leave a message where the original thread was, but TIM.. found it necessary to followup anyway and ask about it.  Since Danimo followed up with a more complete explanation to TIM.., I removed his original note since it was basically a dupe and unnecessary.\n\nCarry on, then.\n"
    author: "Navindra Umanee"
  - subject: "QT License Price"
    date: 2005-07-06
    body: "I think using KDE/QT will help because QT works better on other platforms like Windows than GTK.\n\nI think savy windows developers would choose QT over MFC because it would make migrating their apps to other operating systems much easier. But the price seems designed to make QT unpopular on Windows.\n\nThe only, but serious, drawback to QT is the idiotic pricing for commercial developers.  Idiotic because if they reduced their license fee from $4,000+ to a more reasonable <$149, QT development on Windows (for shareware developers and small software startups) will take off.  I don't think Microsoft would be making billions today if they were charging $4,000 for Windows 10 years ago.\n\n\n"
    author: "AnonymousCoward"
  - subject: "Re: QT License Price"
    date: 2005-07-06
    body: "it's only a drawback for bedroom programmers ... and you must admit that the quality of 80% of windows shareware is crappy at best. \n\nAnd you must know that 10 years ago MS was charging a lot for the Professional Developement tools and ...\n"
    author: "Mathieu Chouinard"
  - subject: "Re: QT License Price"
    date: 2005-07-07
    body: "I am an enterprise developer, and one of those people in a small developer shop many people bang on about. I am going to want to invest in my development tools, end of story. Yes, I'll use free and open source software around as much as I can to give myself the flexibility (and competitive advantage) I need and to ensure I get locked in as little as possible, but I do not expect to develop for nothing.\n\nIf people are complaining about shareware developers looking at development tools that they consider to be too expensive, then basically, their software is crap and isn't going to make much. Anyone who has seen 90%+ of shareware can see that, and why the first thing that many people do when they get shareware is to find a crack."
    author: "David"
  - subject: "Re: QT License Price"
    date: 2005-07-07
    body: "Qt is even an option if you _not_ consider to port the software on other platforms - ever programmed MFC?\n$4000 is not too much, and I guess you could use the GPLed edition and sell your software anyways.\nJust keep in mind that you have to release your sources under GPL License."
    author: "katakombi"
  - subject: "Re: QT License Price"
    date: 2005-07-07
    body: "Interesting we just bought Delphi 8 Enterprise for 1900 Euros without support or cross-plattform availability like Qt offers. \n\nTalk about idiotic pricing.\n"
    author: "ac"
  - subject: "Re: QT License Price"
    date: 2005-07-07
    body: "Delph 8 pretty much sucked. you should have purchased Delphi 9 which is much more stable/faster."
    author: "None"
  - subject: "Summer Of Code"
    date: 2005-07-07
    body: "does anyone have any news on how the different SoC\nprojects are doing - i guess it could be good PR to\nhave some halfway reports or small interviews with \ndifferent participants...\n"
    author: "bangert"
  - subject: "Re: Summer Of Code"
    date: 2005-07-08
    body: "Halfway would be end of July."
    author: "Anonymous"
  - subject: "Lovely to See"
    date: 2005-07-07
    body: "Nice to see something tangible that people are doing on the desktop front. It will be good to see a follow up of details as well so that we know it isn't another China. This is what I've been wanting to see. It's interesting that they're not using Suse, NLD, Mandrake or others, but if this sort of expansion continues (big if) then there will be opportunities for everyone I thin.\n\nGlad to see the Qt rubbish taken down as well, obviously initiated by events elsewhere. A lot of people may not like it, but KDE is undeniably a very nice desktop with good quality features that a lot of people (not involved in this sort of stuff) just want to use. I'm 100% certain that a large portion of that does come from the quality of the Qt toolkit. Kudos to the hard work of the developers and artists etc. of KDE, and most of all, the wonderful attitude KDE's people have that seems to come out in the desktop itself. Rest assured, users do notice it, and goes to show that even the little things matter."
    author: "David"
  - subject: "Way TO GO WIEN"
    date: 2005-07-08
    body: "The City of Vienna has decerning taste and Linux for the city is GREAT\n\nGO TUX GO!!!\n"
    author: "Franz Sturm"
---
As <a href="http://www.magwien.gv.at/vtx/vtx-rk-xlink?SEITE=020050705010">mentioned on the official webpage</a> of the 
<a href="http://en.wikipedia.org/wiki/Vienna">City of Vienna</a>: a customized version of Debian with KDE, dubbed "Wienux" was chosen as the official alternative to MS Windows for the 18,000 PCs of the city. It is up to the individual workers to choose if they prefer a KDE Desktop or a Microsoft based system. The officials expect that about 4,800 machines can run KDE in the short term. Additionally, <a href="http://www.k3b.org">K3b</a> is the official burning application of choice for "Wienux".

<!--break-->
