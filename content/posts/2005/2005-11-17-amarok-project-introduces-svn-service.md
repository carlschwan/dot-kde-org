---
title: "amaroK Project Introduces SVN Service"
date:    2005-11-17
authors:
  - "tteam"
slug:    amarok-project-introduces-svn-service
comments:
  - subject: "Does own SVN server make sense?"
    date: 2005-11-17
    body: "I don't think so."
    author: "Anonymous"
  - subject: "Re: Does own SVN server make sense?"
    date: 2005-11-17
    body: "It makes sense because many authors of small tools simply don't have a KDE SVN account, for various reasons. However, programming without a good revision control system really sucks. It's just so much less productive. So that's why we offer this service, to help these programmers that don't use KDE SVN.\n\nAlso there are some programs which are not directly KDE specific, but useful for KDE in some way. Like SVN::talk, a cool new tool which we also host. Usually you'd have to put stuff like that in SourceForge CVS, which is a major pain. Now you can use amaroK SVN. \n"
    author: "Mark Kretschmann"
  - subject: "Re: Does own SVN server make sense?"
    date: 2005-11-17
    body: "> It makes sense because many authors of small tools simply don't have a KDE SVN account\n\nNor do they have an Amarok SVN account. And if they get an Amarok SVN account they cannot contribute to other KDE projects like with a KDE SVN account. You split the KDE community!\n\nDo you really think people would have spend you money if you would have said that your intention is to duplicate the freely offered infrastructure of the KDE project?"
    author: "Anonymous"
  - subject: "Re: Does own SVN server make sense?"
    date: 2005-11-17
    body: "no, it's not splitting the kde community.\n\nwe're trying to offer a service for projects, which _don't_ belong on the kde subversion server, since they simply don't really belong to the kde community: stuff like amarok scripts and themes for example. i don't think the kde-admins would be happy if all script-authors would now apply for a kdesvn login. but also third party applications: just because you use kdelibs doesn't automatically mean you're part of the kde community.\n\ncheers,\nmuesli"
    author: "muesli"
  - subject: "Re: Does own SVN server make sense?"
    date: 2005-11-19
    body: "i think this makes lots of sense for things like add-on scripts. you are completely correct that having hundreds of people who maintain a 30 lines niche script is something that probably belongs in a more \"user community\" repository. this is something we don't have right now and there's a good opportunity here IMO\n\nfor third party apps, however, as long as they are legal, not pornographic, etc... there's no reason they couldn't be hosted as part of kde extra gear. and in fact that may be preferred =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Does own SVN server make sense?"
    date: 2005-11-17
    body: "I too, as other posters, do not understand the decision.\n\nGetting a KDE SVN account is not that difficult and as for tools that are not directly linked to KDE, as far as I know, there are some in KDE SVN too.\n\nI really hope that at least you think about using SVK instead of SVN, to have a chance to merge everything again into KDE SVN with history.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "nice"
    date: 2005-11-17
    body: "certainly a nice way to support the more actively involved section of the user base. kudos to pwsp and the amaroK project.\n\nas for hosting of 3rd party apps, KDE also offers hosting of KDE apps in our SVN. we have the Exta Gear (http://extragear.kde.org/) project which hosts some 35 different projects (including amaroK, k3b, digikam, datakiosk, kiosktool, kst, fractiq, filelight and many others). this gets you not just svn, but also access to bugzilla, listing on the extragear website and greater exposure to the KDE community.\n\ncombined with pwsp's gracious web hosting, it's pretty easy to get up and running with a serious project without submitting one's self to sourceforge.net or similar systems."
    author: "Aaron J. Seigo"
  - subject: "Misconceptions"
    date: 2005-11-17
    body: "To clear up a few misconceptions\n\n* svn is hosted on pwsp.net, the server as well as the domain\n* amaroK developers used money from the fundraising to help paleo pay for hosting, as it is one of the more frequented sites on pwsp.net\n* svn was offered as a thank you to people who donated, it did not cost us money to implement, and Paleo was willing, at least to some extent because of the money that was used to pay for hosting."
    author: "Dan"
  - subject: "What a great idea!"
    date: 2005-11-20
    body: "This could be a place for all the little things on kde-apps that really will never make it to KDE as is, to get under version control in a trusted repository so they can be extended in a much more efficient way, maybe someday making it into kde itself.\n\nGreat idea, thanks guys."
    author: "Anonsvn"
  - subject: "Why SVN?"
    date: 2005-11-27
    body: "Why SVN?  Why not something else.  Why not arch or git?"
    author: "blah"
---
Are you an amaroK script developer or are you developing a KDE application that should not be in KDE's Subversion for various reasons? We have the solution.  The amaroK project is proud to announce the amaroK Subversion server, a service for amaroK script developers, launched as a thank you gesture to all the supporters who donated to the project during its fundraiser. We hope this will encourage the awesome amaroK community in their extremely valuable amaroK script writing.







<!--break-->
<div style="border: thin solid grey; padding: 1ex; margin: 1ex; width: 375px; float: right">
<img src="http://static.kdenews.org/jr/amarok-logo.png" width="375" height="120" />
</div>

<p>The Subversion server is intended for amaroK scripts and extras that for some reason should not be or are not in the KDE Subversion repository. It is totally independent from the KDE Subversion server and maintained by developers of the amaroK media player.</p>

<p>To get yourself an account, send an e-mail to svnadmin<span>@</span>pwsp.net. You can then access the server at <a href="https://svn.pwsp.net">svn.pwsp.net</a>. Remember that <a href="http://www.pwsp.net">PWSP</a> can give you free hosting if you are leading a KDE-related project.</p>

<p>In other amaroK news, we are now featured on <a href="http://www.last.fm">Last.fm</a>. After supporting the Last.fm (formerly known as Audioscrobbler) service for years, amaroK made it into the <a href="http://www.last.fm/postsignup.php?showplatform=Linux">official list of supported</a> players on Last.fm.</p>

<p>While we have the opportunity, we would also like to bring some attention to <a href="http://amarok.kde.org/amarokwiki/index.php/Promotion">roKymotion</a>, amaroK's promotion team, which currently is in need of more volunteers. roKymotion work is not hard, and if you feel like you want to help amaroK, but do not know how to write code, this is for you. The promotion team handles many interesting public relations tasks for the amaroK project, like writing magazine articles, building contacts with related projects and organising events.</p>

