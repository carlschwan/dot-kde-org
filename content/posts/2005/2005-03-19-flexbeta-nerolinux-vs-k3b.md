---
title: "Flexbeta: NeroLinux vs K3b"
date:    2005-03-19
authors:
  - "numanee"
slug:    flexbeta-nerolinux-vs-k3b
comments:
  - subject: "one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "sure, k3b needs 3rd party tools like cdrecord, dvd+rw-tools, cdrdao, vcdimager and of course kde-libs.\n\nbut it would be nice to compile with qt-libs only instead with full kde-libs.\n\ndon't get me wrong, but my second machine lives better with Xfce and gtk/qt programs only. every kde and gnome program makes this machine feels slower.\n\nin some special cases (recovery & tool cds), the needs for qt-libs only is better than full kde-libs too\n\ni don't know why k3b needs kde-libs so much. don't need multimedia-preview or other kde realated things. drag and drop should be possible with qt-only too.\n\nbtw, i tested nero of course. i can bind special commandline tools to nero so i can burn *.mpc (musepak) files as an audio-cd. but no utf-8 support hurts :-(\n"
    author: "anonymous"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "KDE is one of the reasons K3b is on top of Nero.  You can't have your cake and eat it too."
    author: "ac"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "indeed. quite often people complain about KDElibs. but it would be a huge duplication of efford, and don't forget the bloat, if all these apps would work with QT only."
    author: "superstoned"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "Hmm, if I look at opera, then I don't think it can't be done. for the kde-freaks everything looks the same from the theme side.\n\ni know moneyplex, mainactor, opera, lyx (this is a good example for a small & fast qt-only program and i don't need more features or 'integration' especially with kde-libs) and every program by it's own is a very good qt-only example and that every program integrates very well into kde but doesn't need kde.\n\nsorry, i favour my little slackware and i have kde only installed because of k3b on my second machine. these are to many megabytes related for cd burning program with a gui.\n"
    author: "anonymous"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "it isn't about looks. looks are nice, but not particularly relevant in this case. file management, mimetype handling, toolbar configuration, general configuration, standard keyboard shortcuts and so much more help make K3B what it is.\n\nwithout desktop environments that build on top of the bits and pieces of the wider world of UNIX, not only would we not have coherent systems we wouldn't have nearly as many applications as we have now. KDE's libraries on top of Qt, make it very simple to write complex applications like K3B relative to not having KDE's libraries. trust me, i write Qt only software as part of my work and i often find myself reinventing bits of KDE. and often poorly because the budgets just don't accommodate creating something as nice as KDE's libraries all over again.\n\nyou may not care about coherency (heck, i got along happily for years with blackbox, NS4 and a bunch of aterms, so it's not a foreign concept to me =), but you and i probably both care about the number of available applications.\n\nif K3B wasn't around you'd probably still be using xcdroast. the fact that you don't use xcdroast is telling.\n\n> these are to many megabytes related for cd burning program with a gui.\n\nthat's an easy one to solve! use more KDE apps so that \"megabyte\" investment is spread out more ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "It's all psychological.  Gnome users will bitterly complain about having to pull in KDE libs just to use K3B.  I can sympathize with the argument, I am loathe to install gnome applications on my box for the same reasons (also, I can't think of a Gnome app that I'd need or want).\nHowever, we will both turn around and happily install the 110MB Adobe Reader because it's just not clear exactly what libraries is uses, it lives happily on its own.\nSo what we need, and this is strictly tongue-in-cheek, is some sort of dynamic package renaming service.  If you're running gnome and try to install K3B, it will rename the kde libs to \"libcdburning\".  Then users will think that it is a required cd burning library, and will quite happily install it, no matter the size. :)\n"
    author: "Leo Spalteholz"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-19
    body: "rofl ... i think you're on to something!"
    author: "Aaron J. Seigo"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-19
    body: "Heh, a cunning plan indeed :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-20
    body: "Wooooo-hua-hahahahahahahaha!"
    author: "konqui-fan"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-29
    body: "Not exactly.  Have you ever started a KDE app like K3b on a Gnome desktop?  It takes quite a while.  The same is true when starting a Gnome app on a KDE desktop (although Gnome apps seem to load faster on KDE than KDE apps on Gnome)."
    author: "Chad Kitching"
  - subject: "'psychology' = stupidity"
    date: 2006-04-16
    body: "People who spend their time moaning about \"bloat\" and slow load times have low IQs .  It's as simple as that."
    author: "truth commission"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "Its my experience that the people who complain that xxx application stinks because it a) is linked to kde and not just Qt; or b) its linked to Qt and not gtk; have a problem that has nothing to do with Qt or kde.  Did anyone read the comments from the article?  The biggest complaint for K3b was that it wasn't available on Windows.  lol, like they don't have enough CD burning software to choose from...  If *nix had a couple dozen applications that were of the quality of K3b we would have lots more people switching... just to use OUR apps.  The reality of the matter is that KDE is making that situation a reality faster than an other software \"grouping\" in the free software world.  Why? Because the integration, flexibility, power, and ability to share functionality between applications means that we get great apps like K3b, Amarok, Kopete, and Kontact... faster than would be possible for any of these applications individually.\n\nK3b went from basically not existing to being possibly the best CD burning software on ANY platform in a matter of a couple years.  KDE (whither you like it or not) made that possible. And its time we start making sure that the rest of the free software world realizes it.\n\nBobby"
    author: "brockers"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "Just a few reasons to link against KDE: \n MP3/Ogg decoding\n KIO-Slaves (burning files from network share or CD-Ripping)\n Possibility to use KDE infrastructure for plugins etc.\n Possibly enable KPart integration of K3b into Konqueror\n Controlling K3b from amaroK (via DCOP)\n ...\n\n "
    author: "Anonymous"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-19
    body: "Zeroconf support to discover cd-writers maybe? :)"
    author: "Cartman"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-19
    body: "That would be HAL support (I guess). Zeroconf is for discovering network services/configuring network devices.\n"
    author: "Anonymous"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-20
    body: "> MP3/Ogg decoding\n\nand *.mpc (musepak)? sorry, this is dependencie is a joke\n\n> KIO-Slaves (burning files from network share or CD-Ripping)\n\nthis is not the way to burn a cd successfully. I fetch all data first on local harddisc. and my network mounts lays in fstab\n\n> Possibility to use KDE infrastructure for plugins\n\nwhich ones? musepack decoder ;-)\n\n> Possibly enable KPart integration of K3b into Konqueror\n\nthis is my only kde program\n\n> Controlling K3b from amaroK (via DCOP)\n\ni use xmms, doesn't need amarok or anything bigger. my well organzied harddrive with folders is my \"database\"\n"
    author: "anonymous"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-20
    body: "As someone mentioned, you can compile K3b statically and obtain a 110M binary like you desire. "
    author: "ac"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-20
    body: "> and obtain a 110M binary like you desire.\n\ngood joke\n\nThen I'm really happy that XFCE exists instead of using fat slow KDE. and small NeroLinux, because not everyone needs KDElibs/stuff/sh*\nOr amarock (be happy with xmms)\n\n"
    author: "anonymous"
  - subject: "welcome to frankendesktop"
    date: 2005-03-20
    body: "lol, kthxbye!"
    author: "ac"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-20
    body: "I am sure your well-organized hard drive can tell you easily what songs you prefer and thus give you something like Amarok's \"play my favorites\" or \"play what I haven't heard\" functions. "
    author: "Roberto Alsina"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-04-01
    body: "dunno about them, but i know i can ... i see the artist and i know which ones i prefer and/or haven't heard ... i've got a collection of around 25,000, so it's not like i have 2 full albums or anything ... i use MythMusic for my player (on my HTPC), and while i do like amarok, i like the clean, very fast interface of xmms ... i've been using that interface since winamp < 1.0, so i've gotten used to it and very fast with it ... not saying it's technically/ergonimcally/etc better, i just am better with it\n\n(and yes, i use the same kind of file/directory-based \"database\" for my mp3z)"
    author: "gLaNDix"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-18
    body: "compile k3b-static . This is free software you can but, you know?\nhihihi!"
    author: "nonius"
  - subject: "Re: one \"bad\" thing of k3b"
    date: 2005-03-20
    body: "> This is free software you can but, you know?\n\n./configure --prefix=/shut/your/mouth -static ?\n"
    author: "anonymous"
  - subject: "one \"good\" thing about k3b"
    date: 2005-03-18
    body: "It uses the power of KDE - and with the intuitive design, inetegration and powerful features it is the best burning application ever. That's it.\n\nA big thanks to the k3b-Team especially Sebastian Trueg."
    author: "anonymous"
  - subject: "ISVs"
    date: 2005-03-18
    body: "yes, it's super cool that K3B womps NeroLinux. shows the quality that results from using KDE's libraries, Qt and the open source development model.\n<p>\nhowever, we now have a bit of an odd situation here. the ISVs are starting to venture onto the Open Source desktop with their closed source software, something we've been hoping would start happening for a few years now.\n<p>\nbut if the ISVs don't see an ROI in the form of user base adoption, why should they bother? if all they get is negative press, why not slink back to WinLand and their MacDaddy where the glossy fan-mags will be happy to give them 4 and 5 star reviews?\n<p>\ni think this is solvable, but i'm not sure how it will happen. anyways, i blogged about (surprise! =) and if you care to read yet more rambling on the topic ;) visit:  <A href=\"http://aseigo.blogspot.com/2005/03/isvs-approach.html\">http://aseigo.blogspot.com/2005/03/isvs-approach.html</a>\n<p>\notherwise... woohoo! way to go K3B (and all the console apps that K3B uses) hackers! -=)\n"
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-18
    body: "I experienced the third rate ( being generous here ) software that people ported to OS/2. Buggy, unsupported, dead-end products. With fanfare these things were announced to an eager market, then sales dropped like lead once word got out that they were absolute trash. The conclusion was that no one could make money doing OS/2 software.\n\nI have purchased software for linux that fits my particular needs. I expect it to work with the packaging arrangement that I have, I expect updates when the core libraries or kernel changes. And for some they have shown they want my business. Others obviously don't care about my money, so they don't get any.\n\nLet me count the ways: closed source, proprietary, doesn't fit or work with the popular desktops, doesn't work with the common and well designed software distribution systems that we use, closed source, using obsolete libraries, closed source. Is it lack of knowledge, or just an off the cuff decision to 'cover the linux angle'? Maybe good strategy but bad code.\n\nEnlighten me. Why should I care? Why shouldn't I be insulted with their third rate offerings? Do I look like I just climbed off a hay wagon or something?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: ISVs"
    date: 2005-03-18
    body: "> Why shouldn't I be insulted with their third rate offerings?\n\ni think it would be a bit silly to get insulted when someone pays attention to you just because they don't get it right at first. in fact, that's a great way to chase them off.\n\nwhat if companies had actually managed to write good ports for OS/2 instead of those crappy ports you mention? perhaps they would've sold a few copies. and that would've grown the market. it would've created a market for OS/2 developers. it would've encouraged more people to use OS/2. the ISVs would then have sold more copies and the cycle begins again.\n\nIMO it is in everyone's best interest for ISV's to find success in providing application on the open source desktop. this shouldn't happen at the expense of the terrific open source apps, but it doesn't need to either. there's enough room for everyone."
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-18
    body: "That's the bind isn't it.  We want to encourage ISVs, but we also don't want to say \"yes, this shitty copy of your windows version is just fine, I'll buy it\".\n\nSo what to do?  I'm just waiting for some company to get it right.  Realplayer 10 is nice, Skype is nice.  Those are the only two examples that I can think of.  I've supported Skype though (by buying their VOIP service), so I'm hoping for more good examples like that."
    author: "Leo Spalteholz"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "I think it boils down to making the ISV's realize even if it's new territory for them, there already are lots of high quality applications for the open source desktop. If they want to succeed in this new market, they need to offer competitive products. Those who do will mostly not fall flat on their face with third rate offerings, as they usually understand the concept of competition. \n\nI believe we will see both in the time to come, and in most cases the difference  being in who have done their homework. Whitout doubt some are going to rely on brand recognition alone giving them parts of the market. I think those who do will have to learn, and upgrade their offerings or they will fade away. "
    author: "Morty"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "I agree that it would be good to have successful ISV's and quality software.\n\nThese people are capable of producing fine software.\n\nOne point that can't be ignored: if Nero for windows is good and Nero for Linux is not so good, who gets the black eye? If Acrobat reader on windows is quick, nice to work with and bug free, but slow, bloated and clumsy on Linux, will new users blame Adobe or say that Linux sucks?\n\nThe free software community has built developer resources that permit small underfunded groups to produce quality software. A company that wants to do something on the cheap and doesn't take advantage of the available resources is doomed to failure. The strictures that proprietary software companies operate under don't fit, and for them to produce excellent software for the free platforms requires at least as much investment as the Windows platform, maybe even more. Only those companies who need Linux to survive or advance will put the necessary resources into their projects, and probably will see success.\n\nThe rest deserve our scorn otherwise they will define the platform. That is why it is an insult.\n\nI've read the reaction in KDE when someone commits flawed code. It's quick and sharp, and things get fixed quickly or removed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "> who gets the black eye?\n\nas far as we're concerned, the people currently using the platform? Nero and Adobe. for those investigating the platform? Linux. unfair, but then nobody said life was fair ;)\n\n> A company that wants to do something on the cheap\n> and doesn't take advantage of the available resources\n> is doomed to failure\n\nhonestly, i don't think it's _just_ the lack of investment. the investment level is obviously commensurate with their current confidence in recouping the investment, but i think it goes much deeper than that. as you said, the community creates great applications with moderate investment, so they should be able to as well. i think a lot of it is simply not understanding how to develop effectively for the Open Source desktop platform.\n\n> Only those companies who need Linux to survive or\n> advance will put the necessary resources into their projects\n\ni don't think that's a necessity though. it currently is because of our lack of effective ISV relations.\n\n> The rest deserve our scorn otherwise they will define the platform\n\nfirst, all software on the platform \"defines\" it. the good and the bad. this is why Apple was so amazingly controlling when it came to software stamped as \"Macintosh software\": they wanted to have a platform where all the software was conformant so the platform was clearly defined. Microsoft took the opposite approach and just went for volume of development.\n\nanyways... there's a difference between saying, \"wow, your software sucks\" (which we've all done here =) and saying \"screw off, we don't want you here\". the most effective statement might be, \"wow. your software sucks. thanks for trying though, why don't you let us help you understand how to really develop on this platform?\"\n\n> I've read the reaction in KDE when someone commits flawed code.\n\nwe don't do the same for the apps on kde-look.org, though. 3rd party devel is different than core devel.\n\n(and to be honest, we're not even fastidious enough with core devel yet, IMHO)"
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-18
    body: "Maybe there's not much money to make with commodity/basic/everyday software, since a lot of software exists free: cd-burners, audio/videoplayers, webbrowsers, email, compiler, debugger, ...\nBut I guess a lot of people would pay for games, for special purpose apps (engineering, CRM'n stuff, high-end audio/video processing, ...) and some \"killer\" desktop apps: photoshop, MS Office.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: ISVs"
    date: 2005-03-18
    body: "indeed, the commodity software market is becoming, um... commoditized ;) but yeah, there's a lot of room for both high end and niche software. \n\nthen there are things like Adobe's Acrobat Reader. it would be really nice to see them put enough of an investment into it that it keeps up with the version on Windows and MacOS. Adobe isn't looking to make money off of the reader, so sales aren't an issue. it's user base. if Acrobat is shameful on Linux then few will use it; if it's really good then many people likely will. this has many secondary effects too: people who are on the Windows platform will see the parity in quality and feel more comfortable with Linux (the more quality Windows -> Linux apps we can get, the better!); other ISVs will see that it's possible to write good software in an economic fashion for the Open Source desktops and be more likely to take a swing at it themselves;...\n\nbut if the apps suck, Microsoft and Apple will continue to point at them as say, \"see? grotesque! there be dragons here, ISVs! stay in familiar territory!\"\n\nwe're competing for developers. and the best way to compete for developers is to reward them, or at the very least make it easier for them to reap rewards on their own."
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "You know, there are several fatal flaws with most commercial Linux apps (especially ports): \n\na) They usually don't just work (take Fireburner or Softimage|XSI for example) on all common distros.\nb) They are butt-ugly (NeroLinux uses GTK1.2! Maya and Houdini still use Motif! Amazon uses TCL/TK! - no antialiased fonts, no working clipboard, no themes, no integration, nothing...).\nc) They are badly ported and/or dog slow (Corel Draw/ Photopaint, Compupic).\n\nThis is about competetition. and the Windows-centric companies don't seem to understand that Linux is a rough territory, competition-wise. Not only is it hard to compete with open source apps in the value-for-money segment, but most of the ported applications simply lack quality, too. They don't do their homework. Why didn't Ahead choose to use Qt for Nero (better integration, more eyecandy)? And, much more important, why didn't they focus on features Linux applications (free or not) are lacking? I would have placed my order in no time if NeroLinux supported DVD Video authoring - the Windows version has basic support, the Linux port has none at all! And you have companies like DAZ3D, creators of DAZ|Studio, a Poser-like application, written using Qt3+QSA+3Delight, for Windows and OSX compatibility - but no, they have no plans to 'port' it to Linux (even if all libs are already ported, and a Linux version of DAZ|Studio would most likely only mean a simple recompile)...\n\nI just thought about it and came up with applications/ features Linux is lacking (IMHO):\n\n1.) DVD authoring. All the tools are there, but there's no usable frontend. At least _very_ basic support in K3b would be greatly appreciated (no fancy stuff, not even menues and such - just a plain video DVD with chapters and a single audio track, created from a Xvid file or something).\n2.) DVD ripping and transcoding, something like DVD shrink - but at least there is one project heading in the right direction...\n3.) A noob-friendly music app, something like Fruityloops - there, I'll try to help (as a member of the Hydrogen project - and I'm sure we're getting there!).\n\nAnd that's it. Everything else is there...\n"
    author: "Willie Sippel"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "Aparently Nerolinux uses a modified Gnometoaster."
    author: "Me"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "Can you back that up with facts?  \n\n"
    author: "cm"
  - subject: "Re: ISVs"
    date: 2005-03-20
    body: "http://club.cdfreaks.com/showthread.php?s=2679bb9cfe4c81b04545b8172de03004&t=130931"
    author: "Me"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "Quote:\n\"1.) DVD authoring. All the tools are there, but there's no usable frontend. At least _very_ basic support in K3b would be greatly appreciated (no fancy stuff, not even menues and such - just a plain video DVD with chapters and a single audio track, created from a Xvid file or something).\n 2.) DVD ripping and transcoding, something like DVD shrink - but at least there is one project heading in the right direction...\"\n\nYou might be interested in the apps from http://pingwing.xs4all.nl/view.php/page/KommanderApps\n"
    author: "Andras Mantia"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "Thank you very much, weren't aware of these!\n\nBut it's not exactly what I meant - KDE is all about integration (ie, it makes integration very easy), that's why I mentioned basic DVD encoding from within K3b... It's about noobs! \n\nI'm currently using commandline apps + Varsha for DVD authoring, and LDVD for ripping - but I'll check the Kommander scripts out, of course!\n"
    author: "Willie Sippel"
  - subject: "Re: ISVs"
    date: 2005-03-20
    body: "lxdvdrip is a script front end to a wad of these tools that allows ripping\nand burning DVD's, requanting, etc.\n\ndvdstyler is a gui 'drag and drop' front end to creating multi-level menu\nDVD videos.\n\nNeither are perfect, but they arent alone; these are just two off the top of\nmy head."
    author: "Set"
  - subject: "Re: ISVs"
    date: 2005-03-20
    body: "I meant LDVD for DVD ripping, it's not yet perfect, but I believe it's getting there (usability-wise)...\n\nAnyway, for authoring, dvdstyler is _exactly_ not what I meant (as is Varsha, Q-DVD-Author, KMediaFactory, and the above-mentioned Kommander script). I want (and many others _need_) the ability to load a video file (regardless of codec, size, aspect) in K3b, add it to a video DVD project, and K3b asks you if you want 4:3 or 16:9 (suggesting an aspect depending on the source material), NTSC or PAL, mono, stereo or 5.1 - nothing more (no menues or slideshow, only one audio track, so subtitles - such stuff could be added later). Like that:\n\n- in K3b, click New->DVD Video\n- drag-and-drop a 2.35:1, NTSC, stereo, Xvid encoded file in the project\n- hit 'Burn'\n- a dialog will pop up, suggesting 16:9 (because that's more close to 2.35:1), NTSC, 128kbit stereo audio and a video bitrate suitable to fill the DVD\n- change NTSC to PAL (K3b re-determines the maximum bitrate and sets it accordingly)\n- hit 'OK', go for a cup of coffee, and enjoy your shiny new video DVD after you return\n"
    author: "Willie Sippel"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "This is an example of a shrink wrap 'horizontal market' app, and something that ISV's don't normally write. That's the sort of thing that Microsoft writes and sells, and I wouldn't call them an 'ISV'. I can't see the point of mass market closed source items like Nero on Linux. If K3b is a load better, it probably just means the Nero business model is out of date.\n\nInstead ISV's normally write 'vertical market' software such as a 'cocoa beans trading system' or a 'pet cemetary management system', and they would customise the software for their individual users. In my opinion that's the sort of KDE commercial software development that we should be encouraging. \n\nI notice that Gnome is now called a 'Development platform' as well as a desktop. I don't think we do enough to emphasise th power of the KDE development framework for writing custom vertical market apps."
    author: "Richard Dale"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "> I can't see the point of mass market closed\n> source items like Nero on Linux\n\nthis is almost besides the point =) i agree that making a CD burner for Linux at this point is ... questionable at best, but they did. so: why did they make a crappy one? *that* is the question.\n\n> Instead ISV's normally write 'vertical market'\n\nlike Photoshop and Pagemaker? ;)\n\nbetween the purely horizontal and purely vertical markets there is a large world of software. but even if we look at purely vertical market apps using these as early indicators, i'm a little concerned that that world of software is not going to be ported over very well unless the ISVs learn the platform.\n\nmy whole point of my blog was that we aren't helping in this regard. it's not the easiest platform to figure out for ISVs. if they could figure it out, we'd have a kick ass Nero (even if it didn't sell) and a kick ass Acrobat Reader (even if the KDE fans stuck with KPDF because it is that much nicer =), because creating kick ass software on this platform is not harder than creating the sub par stuff we're seeing. \n\nthat is my concern here. not asking whether we need NeroLinux.\n\n> I notice that Gnome is now called a 'Development platform'\n> as well as a desktop\n\nyou can call it an airplane flight deck but unless you get people landing and taking off it's just a cute raft in the middle of the ocean.\n\ni don't think this is a case of marketing. if it were, Nero and Acrobat Reader would rock. i mean, they chose Gtk+ tools probably because of marketing.\n\nthis is an education and 3rd party develop relationship issue."
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-20
    body: "> Instead ISV's normally write 'vertical market'\n \n \"like Photoshop and Pagemaker? ;)\"\n\nAdobe aren't an ISV, they're too big. According to your definition every company which sells software is an ISV. So is IBM an ISV?\n\nISVs are small companies who depend on a large software company such as Adobe or Microsoft to fill small niches in vertical markets. A vendor selling a Pagemaker plugin for tweaking the PDF output would be an example of an ISV - they depend on the 'eco system' created by a larger company to market their products.\n\n> I notice that Gnome is now called a 'Development platform'\n > as well as a desktop\n \n\"you can call it an airplane flight deck but unless you get people landing and taking off it's just a cute raft in the middle of the ocean.\"\n\nWell, Sun aren't selling the Java Desktop as a development platform, and they don't mention Gnome at all in their branding. You're supposed to use Java/Swing, and they don't recommend using the GTK+/Gnome apis for custom application development. To me that an example of how KDE shouldn't be marketed, because KDE's 'killer feature' is the application framework.\n\n\"i don't think this is a case of marketing. if it were, Nero and Acrobat Reader would rock. i mean, they chose Gtk+ tools probably because of marketing.\"\n\nI'm not sure what you mean here, it sounds self contradictory."
    author: "Richard Dale"
  - subject: "Re: ISVs"
    date: 2005-03-20
    body: "> Adobe aren't an ISV, they're too big\n\ni don't believe the \"independent\" in \"ISV\" is the same as the \"independent\" in \"indirock\" or \"independent film maker\". it doesn't refer to size, but independence from the platform / operating system makers (Microsoft, Apple, IBM, Sun, SGI, etc...)\n\nhttp://searchsmb.techtarget.com/sDefinition/0,,sid44_gci214047,00.html\n\nso yes, Adobe is an ISV for Windows, Apple, etc... there are, of course, ISVs who write software that works with Adobe's offerings as well.\n\n> > i don't think this is a case of marketing. if it were,\n> > Nero and Acrobat Reader would rock. i mean, they chose\n> > Gtk+ tools probably because of marketing.\"\n>\n> I'm not sure what you mean here, it sounds self contradictory.\n\nyou said GNOME calls the core a \"development platform\" and that we should market KDE as one as well. i completely agree. but it takes much more than marketing. in fact, i don't know if marketing is even the most important thing here. the ISVs need direction; they need a body of information and human resources to help direct their efforts. in fact, this was why i first starting talking about an online \"KDE University\" ...\n\nso while marketing may let people know that you claim to have a development platform, successfully getting lots of ISVs to your platform using it and finding success themselves relies on something much more. something we currently lack in the Open Source desktop. this seems to be part of our UNIX heritage, to be honest. but something we can and should fix."
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-20
    body: "\"it doesn't refer to size, but independence from the platform / operating system makers (Microsoft, Apple, IBM, Sun, SGI, etc...)..\"\n\nYes, I had a look on google and I think I'm wrong. Maybe I was thinking of 'VARs', Value Added Resellers.. And I agree with the rest of everything you say above - well put. \n\nI think 'creating a KDE development eco system' is the best way for describing what needs to be done. That could involve marketing, books, training, the KDE University and so on."
    author: "Richard Dale"
  - subject: "Re: ISVs"
    date: 2005-03-21
    body: "\"you said GNOME calls the core a \"development platform\" and that we should market KDE as one as well. i completely agree. but it takes much more than marketing.\"\n\nQuite right. The technology has actually got to work, otherwise if ISVs buy into in a big way (they haven't done yet) then they will run a mile and not come back if it isn't up to scratch.\n\n\"in fact, i don't know if marketing is even the most important thing here. the ISVs need direction; they need a body of information and human resources to help direct their efforts. in fact, this was why i first starting talking about an online \"KDE University\" ...\"\n\nGood idea. Unfortunately, ISVs cannot handle too much information. If you give them six different ways of doing things which should only realistically have one or two options, such as handling sound etc., then they're just going to turn and run. They're also inevitably going to ask about packaging up software to install for people where they don't have to make different packages on three or four distributions and have to test them.\n\nUnfortunately, you can go around the houses on this but you always come back to the same conclusions."
    author: "David"
  - subject: "Re: ISVs"
    date: 2005-03-21
    body: "> Unfortunately, ISVs cannot handle too much information\n\nof course. it needs to be straight forward, succinct, searchable and problem-solving centric (e.g. \"How do I do X?\").\n\n> going to ask about packaging up software\n\nthis is an are that KDE probably can't help too much in, for various probably obvious reasons, but we should still do our part where we can. which is in creating a clear body of information on how to write software that runs on Open Source desktops effectively."
    author: "Aaron J. Seigo"
  - subject: "Re: ISVs"
    date: 2005-03-21
    body: "\"of course. it needs to be straight forward, succinct, searchable and problem-solving centric (e.g. \"How do I do X?\").\"\n\nI seriously think someone should contact Trolltech about this and get them to make KDE (through KDevelop) a fully supported development platform. This would be extremely mutually beneficial for everyone I think, and give ISVs the level of confidence that they need. If this were to take off Trolltech would gain hugely, and there would also be much opportunity for re-investment in KDE.\n\nAt the moment, even though KDE uses Qt,Trolltech doesn't seem to do much to support it in terms of the services and products they offer. KDE depends on a company's product that is of such a quality that it is exactly what ISVs are looking for. I don't think anyone need be shy about that.\n\n\"this is an are that KDE probably can't help too much in\"\n\nNo, and I don't think KDE itself should do too much on this. KDE is a loose technological project (for example, as Windows is internally at Microsoft, however integrated it may look to the outsider). Unfortunately, it is going to continue to be a stone in our shoe for quite a while as ISVs can't really depend on the software that will be shipped as part of the distribution or OS that their software gets installed on. Hence, you get an all-in-one 100MB install with everything on, including mustard. Although Nero's and Adobe's Acrobat ports to Linux of their software are poor, I just can't criticise them for it because I don't think it is all their fault really.\n\nAnyway, look at it this way - when the first ISV thinks that it is commercially viable to purchase a Qt license KDE will have arrived."
    author: "David"
  - subject: "Re: ISVs"
    date: 2005-03-21
    body: "Missed the bit at the bottom - sorry.\n\n\"something we currently lack in the Open Source desktop. this seems to be part of our UNIX heritage, to be honest. but something we can and should fix.\"\n\nHow?"
    author: "David"
  - subject: "Re: ISVs"
    date: 2005-03-21
    body: "\"Well, Sun aren't selling the Java Desktop as a development platform, and they don't mention Gnome at all in their branding. You're supposed to use Java/Swing, and they don't recommend using the GTK+/Gnome apis for custom application development.\"\n\nReally, that's what they're recommending you do? Wow, JDS is really dead then. The first rule of desktop development, as practised by Microsoft (unfortunately, they do some things right), is to have development tools and methods that are in harmony and integrated with the base OS and desktop system. If the above is true then Sun just aren't going to learn, are they?\n\n\"To me that an example of how KDE shouldn't be marketed, because KDE's 'killer feature' is the application framework.\"\n\nGod no."
    author: "David"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: ">however, we now have a bit of an odd situation here. the ISVs are starting to >venture onto the Open Source desktop with their closed source software, >something we've been hoping would start happening for a few years now\n\nIts easy: \n\n-Nero was too late, there are now better alternatives\n-Adobe acoread will lose agains xpdf\n\nand so on. \nSome ISV do not need to exist on Linux.\n\n\n\n\n"
    author: "dummyuser"
  - subject: "Re: ISVs"
    date: 2005-03-19
    body: "but if the ISVs don't see an ROI in the form of user base adoption, why should they bother? if all they get is negative press, why not slink back to WinLand and their MacDaddy where the glossy fan-mags will be happy to give them 4 and 5 star reviews?\n\nIf AHEAD is short sighted and arrogant enough to think they can release subpar software and have people praise and pay them for it then they are going to have a problem. I downloaded and installed this abortion of a cd burning app. after fooling with it, I went and Paypal'd Sebastion and Chris some money. They deserve it!!\nAll Hail K3B! \n\nAhead Software, actually TRY next time.......\n\n"
    author: "LD"
  - subject: "self-burning livecd"
    date: 2005-03-18
    body: "This is great, but I am still waiting for the self-burning K3b LiveCD."
    author: "ac"
  - subject: "Re: self-burning livecd"
    date: 2005-03-19
    body: "lol, good one!"
    author: "Anonymous"
  - subject: "Re: self-burning livecd"
    date: 2005-03-19
    body: ":-D"
    author: "Sebastian Trueg"
  - subject: "LOVE k3b but... few annoyances"
    date: 2005-03-19
    body: "1. can't create Bootable CDROMs with .IMA files?\n2. Advance settings like \"[ ] allow multiple dot in file names\" get unchecked always :(\n\n3. Mode2 cdrom (unable to copy multi track cdroms) though it should automatically try clone copy option.\n"
    author: "Fast_Rizwaan"
---
<a href="http://www.flexbeta.net/">Flexbeta</a> is running <a href="http://www.flexbeta.net/main/articles.php?action=show&showarticle=86">a side-by-side comparison</a> (<a href="http://www.flexbeta.net/main/printarticle.php?id=86">all-in-one page</a>) of CD/DVD burners <a href="http://ww2.nero.com/us/NeroLINUX.html">NeroLinux</a> and <a href="http://www.k3b.org/">K3b</a>. K3b gains plenty of praise for its integrated user interface, look and feel, functionality, free license, and quite simply comes out on top.  A good read if you need to be reminded of the goodness that is K3b.

<!--break-->
