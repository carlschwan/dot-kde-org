---
title: "People Behind KDE: New Series Starts With Antonio Larrosa"
date:    2005-07-11
authors:
  - "jriddell"
slug:    people-behind-kde-new-series-starts-antonio-larrosa
comments:
  - subject: "Konqi"
    date: 2005-07-11
    body: "May I say that the new konqi logo (http://www.kde.nl/people/index.html)is just GREAT?\nIs there an online petition for \"more konqi in kde\"?! :)"
    author: "Anonymous"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "AFAIK not jet, but you should start! one :)"
    author: "Patrick Trettenbrein"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "I dare say the Kubuntu guys have heard your plea. =)"
    author: "KDE User"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "well thank you :) \n\nonline petition has been mostly working so, that you contact me in IRC or via email, and tell me what you need, and I try to find time to make it :)\n\n.b\n"
    author: "basse"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "I don't mean to be rude, but I couldn't disagree more strongly.\n\nWhile the new logo is no worse then the old, the problem of putting cartoon characters into computer programs is that they irritate more than they help.  (because they technically provide no utility at all, it's a low threshold for irritation)\n\nWhy on earth do people LIKE Clippy, Bob the XP Search Dog, and Konqui?  I realize it's a matter of taste, but what surprises me is that these are the DEFAULTS and not part of a \"happy fun cartoon interface add-on\" pack, as I feel it should be.\n\nOkay, that wasn't polite, but the feeling behind it is intense and genuine.  Any takers?  What would be WRONG with making Konqui part of a \"Cartoon Interface\" add-on?\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "where is Konqi annoying you in KDE? what programs?"
    author: "anma"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "KDE, for one.  I guess the official program name is ksmserver.  (I think that's the KDE Session Manager).  Try to log out, and \"Hello Kids It's Cartoon Dragon Time!\"\n\nIt's the first thing I replace every time I install or upgrade KDE.  I'm honestly surprised more distros don't replace it with their own logos by default in their packages.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "so the only program where Konqi is offending is the logout screen. I am curious: what do you replace it with?\n\n"
    author: "anma"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "Actually, you're right--ksmserver is the only thing that springs to mind, but I've been bugged by Konqui elsewhere in the past (and perhaps this has already been rectified).  I occasionally get bugged by the happy penguin/confused penguin/cool penguin in K3B, but this isn't really the forum for that.\n\nThe point is that I see this cartoon character every day, sometimes multiple times a day.  And...it brings out the Clippy flashbacks.\n\nI actually replace the Konqui PNG with nothing--I just delete the file and the look improves dramatically.  However, I had messed with logos in the past and had tried KDE's \"gears\" logo, Fedora's hat logo, and even SuSE's lizard logo.\n\nAnd SuSE's logo is not simply replacing one cartoon with another, I'm afraid.  Konqui is 3d-rendered, has very clear facial expressions, and is directing its cartoonishness directly at the user through its gestures and expressions.  SuSE's lizard, while admittedly weird, doesn't even fit the bill for 1950's Mickey Mouse cartoons.  It's a shape--a perfect profile of an abstracted lizard-shape, devoid of color variation or expression.  It's not animals I'm objecting to, it's cartoons--there IS a difference.  A dancing paperclip with eyeballs, for example, is \"cartoonish\", while KDE's \"gears\" logo is not.  Hope that helps clarify where I'm coming from.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "My thoughts exactly. I've earned laughter from two different people while logging out already. \"Unprofessional\" is what they call it.\n\nI don't like it either and am unable to understand how konqui can still survive in a desktop that wants to appear clean and professional.\n\nPlease, make it an option, a theme, anything that can be switched off (by default)!"
    author: "me"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "Yeah, company mascots are so rare and \"unprofessional\", why should KDE use one.\n\nhttp://en.wikipedia.org/wiki/List_of_American_advertising_characters\n"
    author: "cl"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "Why rise above mediocrity, eh?  KDE's in good company.\n\nListen--I'm not saying they can't have a mascot.  I'm saying that IF they insist on having a CARTOON mascot, they limit that CARTOON to places where it's not as irritating.  Advertising?  Fine!  Web site logo?  Fine!  Guy in a big stuffed dragon costume at conventions?  Fine!  But not in my software, please.\n\nWhen's the last time you saw the cartoon Joe Camel printed ON THE CIGARETTE?  Do you think that certain people might think Camels weren't as cool if such a thing happened?\n\nI'm not trying to troll (although, yes, I may be succeeding anyway).  I just want it to be easier to get a nice-looking desktop, which is what we all want.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "s/professional/boring"
    author: "Anonymous"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "s/boring/inoffensive/g\n\nYou only replaced the first occurance of the word \"professional\" ;-)\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-11
    body: "> It's the first thing I replace every time I install or upgrade KDE.\n\nSee, the power of open source. Be happy!!!\n\n> I'm honestly surprised more distros don't replace it with their own logos\n\nReplacing a dragon with a chameleon is so much better.</sarcasm>"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "Yes, yes.  Open source woohoo.  On Windows I can turn off Bob the search dog with a checkbox.  In KDE I can turn off Konqui the Wonder Dragon by going to /opt/kde3/share/apps/ksmserver, getting root permissions, and deleting/replacing the right PNG file, unless the distro put it in another location.  Which method was more user-friendly again?\n\nReplacing a cartoon with a non-cartoon was the goal, not replacing an animal with a non-animal.  See my above (longer) post for why SuSE's admittedly weird logo isn't a cartoon.  And other non-animal things (most notably paperclips) can easily be made into cartoons too.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "it's called \"having fun\" and to keep it in perspective it's just a log out dialog. what exactly does that picture rob you of? your ability to pretend life is constantly, dramatically and almost comically serious?\n\nIMHO, when we take ourselves and our surroundings too seriously we rob ourselves of happiness. if more people learned to smile and to accept that having a laugh every once in a while was an acceptable and even *good* thing, think about how much nicer this world would be to live in?\n\nwe're all human beings with thoughts and feelings, and while we profess to encourage thought we tend to also actively avoid opportunities of expression.\n\nnow i'm all for a professional KDE environment, but i wonder if \"sterile\" is really the line to be aiming for.\n\nthat's how i see it anyways =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "i agree. i love to see more of konqi, instead of less. professional? boring! \n\n\"put the fun back into computing. Use Linux.\"\n\nso - i vote for more konqi in KDE."
    author: "superstoned"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "I agree that people should have the option of a \"wacky\" interface for when they don't want to take life so seriously.  Konqui is great for that.\n\nBut there does not appear to be an option for those who do not want zany cartoons at logout.\n\nIt is not the philosophy behind the cartoons I'm arguing with--it's the flat denial that people could want anything else.\n\nGenerally speaking, \"sterile\" is indeed professional.  Many business software interfaces are \"sterile\" by default with a \"playful\" option (if, indeed, there are options at all).  But frankly I would settle for \"playful\" by default with a \"professional\" option.\n\nI'm not trying to eradicate Konqui--I'm trying to make sure he's carefully labelled as a \"silly\" thing so that he does not accidentally show up where he's not wanted.  And he's not wanted on my desktop.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "unprofessionnal, cartoonish, taking life seriously, silly thing, I don't like it, ... \nweeeeew!!! you must be so agreeable to live with!!! to work with!!!\nI see the constant frown on your face, the \"seriousness\", is that what you call life? \nI know \"serious\" businesses with KDE as their desktop and people (scientists, managers,...) are as productive as in other enterprizes. Konqi does not diminish their capabilities!!! Maybe it'll even save them a heart attack in the long run as they'll ease stress a bit when they see it waving them Good Night! But there's no \"serious\" studies about that.\n\n\n\n"
    author: "annma"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "Thank you for denying that my aesthetic preferences (and, incidentally, those of the entire commercial software industry) have any validity whatsoever.\n\nThis is the sort of thing that gets some open source projects a reputation for inflexibility.  Let me show what this looks like to outside observers:\n\nUser: I want this software to have the option to behave more like all other software\nCommunity: Your request is crazy, and you are a boring person too.  We won't change the way our software works.\nUser: How about an OPTION to work the way all other software works?\nCommunity: No, you are a crazy person.  And boring too.  Everyone else does it this way because they are mentally ill.  We are the sane ones.  Stop bothering us.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "> I want this software to have the option \n\nput a picture in `kde-config --localprefix`/share/apps/ksmserver/pics/shutdownkonq.png and you're done. you don't need root privs as you stated earlier, and i don't think this minor detail deserves yet another inane option in kcontrol.\n\n> incidentally, those of the entire commercial software industry\n\nseeing as kde is part of the commercial software industry, and that a number of other commerical software packages, even closed source ones, aren't so stuffy either, this statement is demonstrably false.\n\nyou may be comparing it to stuffy microsoft office or whatnot, but i'm not convinced those are the aesthetics we should aim for.\n\n> gets some open source projects a reputation for inflexibility\n\nit's also what makes some developers in open source projects cringe a little when a users approach, and why most companies don't even let users talk to their development teams."
    author: "Aaron J. Seigo"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "My dear anon, please stop being so silly and lighten up a little... Sure, people could want something else, nobody denies that -- but it's a zany request coming from an unidentifiable wacko, or at least, that's how it appears to me. It's not professional to hide behind anonimity when arguing, is it? It's not business-like to concentrate on minor details like a logout screen (although Parkinson would have it differently). And it's a sign of an healthy, creative business if there's some playfullness involved."
    author: "Boudewijn Rempt"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "I think this is just a sign of KDE's success.  The community is larger and more diverse than anyone ever could have imagined.  More people are coming from other camps, disllusioned, while KDE grows and grows and gets more popular.  \n\nIt's somewhat inevitable that we will pick up some whack jobs on the way (not saying anyone here is a whack job), but at the same time it's a sign that KDE is doing SOMETHING RIGHT.  At this time I agree it would be foolish for KDE to change direction and meekly follow, say, the GNOME Professional Guidelines (GPG). =)"
    author: "KDE User"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "> not saying anyone here is a whack job\n\nhey, i am too!\n\n=P"
    author: "Aaron J. Seigo"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "So how's that srcdir!=builddir coming along? :-)"
    author: "Navindra Umanee"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "hehehe... pretty well actually =P\n\nshocked the hell out of binner i think ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "I stated my preference (and fully acknowledged all along that others may have differing preferences), and was attacked for daring to have my preference.  Which side of this argument is unprofessional?\n\nStill I hope something useful comes of this.  Anonymity is not a crutch, it's just how I post to online message boards.  In the unlikely event there is no bug filed on this subject, I will CC you explicitly on the bug report and then you can know exactly who I am and call me unprofessional by e-mail, which would be very professional indeed of you.\n\nThe problem itself is small: there's a silly cartoon dragon I hate seeing all the time.  I manually delete this image file every time I install or upgrade KDE.  I'd like there to be a \"professional appearance\" option so that this is an easier process.\n\nThe discussion is so long and drawn out because others seem hung up on the point of \"You can't possibly mean that you DISLIKE Konqui!  Maybe you need to be happier!\"  This is why there are several posts from me all stating: \"No, really, I hate Konqui.\"  It's not that I think it's worth posting that many times--it's that the validity of my preferences is being questioned, and I am responding that no, I actually dare to dissent.\n\nI'm not a Gnome user or a recent Windows convert.  Before KDE existed, my desktop was FVWM2 (ick).  I've used KDE since the beginning.  I've hated Konqui since the beginning.  I've just never been personally insulted for disliking him before.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "> because others seem hung up on the point\n\nno, i'm hung up on the point that it's not a big deal and if it really bothers you, you can change it locally (per user even). because of that, i'd prefer it not become another microoption that we then get to hum and haw over how to jam it in with the other 18 billion options in the gui we'd end up with if we went that way."
    author: "Aaron J. Seigo"
  - subject: "Re: Konqi"
    date: 2005-07-13
    body: "Thanks for thinking this was a private conversation between me and you, but your relatively topical and helpful responses are not really representative of the responses I'm talking about.\n\nI appreciate that you want to cut down on \"options everywhere\".  That's a valid response.  I thank you for the home directory per-user solution, which is a helpful response (perhaps even helpful enough for me, wish you had mentioned it earlier).\n\nResponses which are neither useful nor helpful are \"you are boring\", \"you need to lighten up\", or \"you are a w[h]acko\".  Do you see where I might get the idea people were not so interested in discussion so much as name-calling?\n\nFrankly even though it's one picture, it's an unneccessary liability for KDE.  I substitute \"Clippy\" for \"Konqui\" in these arguments I'm hearing and it makes my skin crawl.  Some would call it a miniscule detail.  I would call it a low-risk, high-payoff bugfix.\n\nNo big deal.  These forums aren't where these things are decided.  The wishlist is filed, the proper procedures are being taken, etc, etc.  People can vote/ignore to their hearts' delights.  For those who don't like the idea of \"options everywhere\" (which, I agree, is a totally valid argument), there's always the option of getting rid of Konqui altogether, which doesn't add any code or complexity at all.\n"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2005-07-14
    body: "> I substitute \"Clippy\" for \"Konqui\" in these arguments \n> I'm hearing and it makes my skin crawl.\n\nThat's not really a fair comparison. Konqui appears as a static image in a few places in KDE, whereas clippy has an effect on the user's interaction with the application, which is (for me at least) why it's so annoying.\n\nOf course, if you don't like the Konqui image(s), that's just a matter of taste. I for one think it's great :-).\n\nPhil"
    author: "Philip Rodrigues"
  - subject: "Re: Konqi"
    date: 2005-07-15
    body: "You're right.  Clippy actually interrupts your work and impedes your ability to do things.  A more apt Konqui comparison would be Bob the XP Search Dog, who does not actively prevent you from doing things.\n\nMy concern wasn't for annoyingness, though, it was professionalism.  The distinction between animated and static cartoons is degrees of annoyingness, but they are both equally unprofessional.  Splitting hairs, maybe.\n\nI agree liking or disliking Konqui is totally a matter of taste, which is why I don't think it would be a good idea to get rid of him--only to have a better option for getting rid of him if that's what you want.\n\nThanks for reading ;-)"
    author: "ac"
  - subject: "Re: Konqi"
    date: 2006-01-24
    body: "I hate Konqui as well, with a passion. Here are my reasons:\n\n1. There is nothing wrong with a mascot, but it shouldn't appear anywhere in the product. Even the BSD daemon, among the best mascots to ever exist, is not actually seen on the screen.\n\n2. Konqui is poorly designed. Anybody who studied design will tell you this. Nothing about him is done right.\n\n3. Konqui is 3D rendered. This may have been impressive in the 90's, but today such things are just ugly. At least make a decent SVG vector representation of the dragon!\n\n4. Konqui is childish. Makes KDE, and Linux itself (considering that KDE is the most commonly used desktop nowdays) look unprofessional. So does that stupid tradition of naming everything with a \"K\". This makes people believe that Windows is somehow more professional and refined than Linux."
    author: "Ilya"
  - subject: "Re: Konqi"
    date: 2006-01-24
    body: "ok i am with you with your comments 1,2,3 and 4\n\nbut the thing about naming everything with k is called \"branding\". You cants say its bad. Look @ apple i-everything.\n\nthe names make no sense either. iMac iPod iBlubb iblah "
    author: "chris"
  - subject: "Re: Konqi"
    date: 2005-07-12
    body: "Yeah, he looks really cute, although the eyes makes me think he may be abusing some drugs that aren't good for small dragons. :) personally I think Konqi is a nice mascot and like when he shows up in various KDE apps.\n"
    author: "Chakie"
  - subject: "RE: maintenance of the icon loader classes"
    date: 2005-07-12
    body: "Is it the icon loader classes that decide (incorrectly in some cases) which style icon to display?\n\nI am trying to figure out why this (icon theme):\n\nhttp://home.earthlink.net/~tyrerj/kde/testnew.tar.bz2\n\ndisplays CrystalSVG icons when there are KDE Classic ones available.\n\n\n\n"
    author: "James Richard Tyrer"
---
The <a href="http://www.kde.nl/people/">People Behind KDE</a> interviews are back with a new series.  First in the hot seat is <a href="http://conference2005.kde.org/">aKademy</a> organiser <a href="http://www.kde.nl/people/antonio.html">Antonio Larrosa</a>.  For the new series the original interviewer <a href="http://www.kde.nl/people/tink.html">Tink</a> has handed over management to <a href="http://www.kde.nl/">KDE-NL</a>.  The People Behind KDE interviews take a look at the human side of KDE development by asking the important questions to our team of coders, artists, translators and everyone else who helps KDE.  


<!--break-->
