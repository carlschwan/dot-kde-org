---
title: "KDE-Look Announces the Winner of T-Shirt Contest"
date:    2005-02-19
authors:
  - "jtheobroma"
slug:    kde-look-announces-winner-t-shirt-contest
comments:
  - subject: "What about outside of the US"
    date: 2005-02-19
    body: "Am I right that if I want to have one of those shirts sent to germany I have to pay quite a lot shipping? Plus: I need a creditcard or use \"Discover\" which is not explained (I never read about it anywhere...).\n\nPlease correct me if I am wrong. I would love to pay < 20&#8364; for one of those shirts but everything above 20&#8364; is to much for me. And I don't own a creditcard."
    author: "anon"
  - subject: "Re: What about outside of the US"
    date: 2005-02-19
    body: "The &#8364; was the eurosymbol here. Konq-bug or Zope-Bug?\n\nSo: I am willing to pay up to 20 euro, not more."
    author: "anon"
  - subject: "Re: What about outside of the US"
    date: 2005-02-19
    body: "Dot.kde.org bug.  Specifically, not allowing HTML (tags or entities) in posts any more.  Which is due to a squishdot security hole in how it filters the input HTML, which allows posting javascript in your comments.  Which the admins don't apparently feel like ever fixing."
    author: "Spy Hunter"
  - subject: "Re: What about outside of the US"
    date: 2005-02-20
    body: "Discover is a credit card type dealy, which like the Diner's Club International Card, doesn't own a lot of mind-share."
    author: "Troy Unrau"
---
The community has spoken and the winner of the First Annual KDE-Look T-Shirt Contest with <a href="http://www.kde-forum.org/thread.php?threadid=9119&sid=">24 out of 81 votes</a> is <a href="http://www.kde-look.org/usermanager/search.php?username=meNGele">Nenad Grujicic</a> with his entry <a href="http://www.kde-look.org/content/show.php?content=19834">Green</a>. Nenad has won the first official KDE-Look T-shirt and an External 7 in 1 Memory Card Reader.



<!--break-->
<p>Congratulations goes out to him and to all of you who entered in this contest.</p>

<p>Our new KDE-Look T-Shirt is on sale at <a href="http://www.cafepress.com/revelinux">Revelinux Gear</a> with all the proceeds going to help out with the costs of running KDE-Look.</p>

<p><img src="http://revelinux.com/files/18098713_F_store.jpg" width="150" height="150" align="left"  />When we posted the vote to KDE-Forum.org they thought that they were experiencing a denial of service attack because so many people viewed the forum at the same time! They were a bit relieved and excited when they found out it was a KDE related quest.</p>
 
<p>Just a reminder that the <a href="http://www.kde-look.org/news/index.php?id=155">digiKam Superimposed Template Contest</a> is in full swing. We are "looking" forward to seeing the <a href="http://www.kde-look.org/index.php?xcontentmode=42">artwork</a> that this contest brings out.</p>

<p>For those of you interested in helping out on future contests please feel 
free to join <a 
href="http://mail.kde.org/mailman/listinfo/kde-contests">KDE-contests mailing 
list</a>.</p>
 
<p>Be on the "look out" for many more great contests and challenges coming your 
way soon.</p>

<p>KDE-Look... we overwhelm the world with beauty! </p>




