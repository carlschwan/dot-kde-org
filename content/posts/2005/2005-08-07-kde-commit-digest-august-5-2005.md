---
title: "KDE Commit Digest for August 5, 2005"
date:    2005-08-07
authors:
  - "dkite"
slug:    kde-commit-digest-august-5-2005
comments:
  - subject: "CSS 3 multiple background images"
    date: 2005-08-06
    body: "From one of the check-ins, it looks like Konqueror now has CSS 3 multiple background images courtesy of Safari.  Read more here:\n\nhttp://webkit.opendarwin.org/blog/?p=15\n\nA pretty cool test page here:\n\nhttp://decaffeinated.org/archives/2005/08/03/background-image\n"
    author: "Jim"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "I just tried that out on latest Safari.  It really is pretty cool.  Good work for getting it into Konq :)\n\nL."
    author: "ltmon"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "can you post a screenshot of it? im still running kde 3.4."
    author: "drizek"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "Just install the latest Konqueror...\n\nIt should not be to hard to install an additional webbrowser or am I wrong?...\n\nUh oh it's the Konqueror - then forget it to \"just install\" an newer version."
    author: "*nix"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "Shouldn't you go back to windows?\n\nUh, oh, and the 'new konqueror' is pre-alpha software. Should be obvious how to 'just install it', no?"
    author: "mhn"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "I assume that posting a screenshot is quicker than make everybody 'just' build a new Konqueror (and don't mention a new kdelibs)."
    author: "Bram Schoenmakers"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "I have the latest Konqueror.\n\nThis code went in a few days ago. If someone wants to try it out they need to install a development version which is in constant flux. There are tools to do that such as kdesvn-build.\n\nEveryone will be able to run this in a short while when 3.5 is released.\n\nDerek\n\n"
    author: "Derek Kite"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "Here's screenshot of the window at two different sizes. When you resize it and make it smaller the outer pictures slides under the central one, and they move away when making the window bigger. All wery smoothly, with only a little bit of flicker on fast resizing."
    author: "Morty"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "thanks. thats pretty cool."
    author: "drizek"
  - subject: "Re: CSS 3 multiple background images"
    date: 2005-08-07
    body: "Another example of multiple backgrounds: \n\nhttp://amonre.org/pub/multiplebackgrounds/"
    author: "Denis Defreyne"
  - subject: "aRts?"
    date: 2005-08-06
    body: "I thought anything aRts was on its death bed. Personally, I found aRts way too slow with Amarok. Others found bigger problems and in fact, there was talk of replacing it with something better...now, in front of my eyes, I read something that sounds important that is going to be based on aRts? What's going on?"
    author: "cb"
  - subject: "Re: aRts?"
    date: 2005-08-06
    body: "What speaks against aRts as one possible backend? You don't have to use it..."
    author: "Anonymous"
  - subject: "Re: aRts?"
    date: 2005-08-06
    body: "I've had a pretty good experience with arts, and use it as my engine in amarok, rather than gstreamer or xine.\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: aRts?"
    date: 2005-08-06
    body: "The commit is in kdemm, so it is not that Arts has become maintained again. As far as I understand, it seems to be more opening a path for a migration away from Arts.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: aRts?"
    date: 2005-08-06
    body: "The commit log gives some insight:\n\"The first (somewhat) working KDEMM backend based on aRts. I used aRts for a few reasons:\n1. It's the framework I know best and I can rather easily write a backend for\n2. The features and problems are known to me, so I know what is a KDEMM problem and what is a backend problem.\n3. We need it anyway for the poor people who need it for compatibility in KDE4.\"\n\nSounds perfectly sensible to me."
    author: "Antonio Salazar"
  - subject: "Re: aRts?"
    date: 2005-08-07
    body: "I read that akode was to be a replacement sound engine somewhere. Is there any truth in this; and if this is correct, what necessitated the change?"
    author: "Matt T. Proud"
  - subject: "Re: aRts?"
    date: 2005-08-07
    body: "I think that I read that KDEMM will be an API abstracting away the sound engine (video also?) and that any one of a number of backends could be used.  E.g.  Use aRts, gstreamer, Xine, whatever.  New backend plugins are meant to be easy to write, so if one particular backend becomes absolutely the best, or if a new one appears it won't be hard to migrate.\n\nL."
    author: "ltmon"
  - subject: "Re:"
    date: 2005-08-08
    body: "I suppose this will also help connecting KDE to the native 'soundengines' of other platforms?\n\n_c."
    author: "cies breijs"
  - subject: "Bug in Diff View"
    date: 2005-08-06
    body: "Thank you Derek for the great work with the commit digest.\n\nI have noticed a minor bug in the diff view with regards to images: http://tinyurl.com/abuyu for example shows only the new version of images, not the old version on the left hand side."
    author: "Dominic"
  - subject: "KOffice screenshots?"
    date: 2005-08-07
    body: "Just wondering, why almost all KOffice components' screenshots haven't been updated for long time? Some of them (e.g: KPresenter) even still show very old screenshots. Krita seems to update their screenshots regularly. I just think that screenshots can help to attract more people to try KOffice and give good impressions."
    author: "fyanardi"
  - subject: "Re: KOffice screenshots?"
    date: 2005-08-07
    body: "Well, go ahead, make some cool screenshots of koffice and submit them to the koffice maintainers ;o)\n\nRinse"
    author: "rinse"
  - subject: "Re: KOffice screenshots?"
    date: 2005-08-07
    body: "There was already more than a year ago (if not two) that there was a call for volunteers. A part a few screenshots of KWord (1.3.x at that time) I had not received anything.\n\nSo many pictures are still outdated, even more outdated than they were at that time.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Kontact scripting: correct me if I'm wrong"
    date: 2005-08-08
    body: "Embedding a single language (in this case javascript) as standard scripting language inside kontact sounds a bit strange to me. \nBefore you know it everyone will implement their pet scripting language inside the core KDE components, duplicating work and bloating the code.\n\nWouldn't it be more efficient to enhance the DCOP hooks in Kontact and make use of standard DCOP scripting tools (Kommander, maybe some language bindings?).\n\nDon't get me wrong, I'm all for GTD (http://en.wikipedia.org/wiki/Gtd) enhancements to Kontact so I wish the author success."
    author: "MikeR"
  - subject: "wengo beta for linux finally!!"
    date: 2005-08-08
    body: "http://www.wengofiles.teaser-hosting.com/wengophone/beta/wengophone_0.950-i386.deb hey all this is wengo kubuntu\u00b4s deb  and this is fedora core4 rpm:\nhttp://www.wengofiles.teaser-hosting.com/wengophone/beta/Wengophone-0.950-1.i386.rpm\n\nWengo is a kde based SIP phone that allow free call around the world on real phones and on any other SIP phone.\n\nScreenshots:\nhttp://dev.openwengo.com/trac/openwengo/trac.cgi/wiki/WengoPhoneSnapshotsLinux\n\nmore info can be find here:\n\nhttp://dev.openwengo.com/"
    author: "arequipa"
---
Some highlights from <a href="http://commit-digest.org/?issue=aug52005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=aug52005&all">all in one page</a>):

<a href="http://www.koffice.org/kspread/">KSpread</a> improves range functions.
<a href="http://kmobiletools.berlios.de/">KMobileTools</a> adds addressbook import and export to VCard and KAddressBook.
<a href="http://uml.sourceforge.net/index.php">Umbrello</a> adds Tcl code generator.
<a href="http://kmail.kde.org/">KMail</a> now has full text indexing.
<a href="http://kontact.org/">Kontact</a> scripting (a <a href="http://developer.kde.org/summerofcode/">Summer of Code</a> project).
And the first (somewhat) working KDEMM backend based on <a href="http://arts-project.org/">aRts</a>.


<!--break-->
