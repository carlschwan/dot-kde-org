---
title: "KDE Joins Google's Summer of Code"
date:    2005-06-03
authors:
  - "jriddell"
slug:    kde-joins-googles-summer-code
comments:
  - subject: "more ideas"
    date: 2005-06-03
    body: "Great to read these innovative ideas in the list. Especially the very open suggestion are appealing and I'm sure there are quite a few students capable of making great contributions.\n\nHere are some more ideas:\n- KDE widget/component using GtkMathView (http://helm.cs.unibo.it/mml-widget/) so that KDE can display MathML\n- GreaseMonkey for khtml (greasemonkey is so cool i have trouble sticking to khtml)"
    author: "jos"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "One more idea to integrate google and KDE :\nwrite a kipi[1] plugin to integrate digikam, kimdaba and gwenview with blogger like Google's Picassa does on Windows.\n\n[1] : http://extragear.kde.org/apps/kipi/"
    author: "jmfayard"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "Another idea is to port the QT voip application twinkle\nhttp://www.twinklephone.com to kde and have it integrated into kontact.\n\nThe application itself had a quite a good review on a Barry's blog, see:\nhttp://www.barryodonovan.com/blog/?postid=11\n"
    author: "Richard"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "hmm, you know that KDE is based on Qt and not GTK? so your idea should be: a KDE widget/component so that KDE can display MathMl :-)\n\nNice idea :-) (-> KFormula?)"
    author: "annma"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "Well, in theory, you should be able to use any widget set with the code. I've played around with the code and found it not be be very clean, but that was a year a go. Judging from the screenshots, the current status is pretty ok. There's even a thesis on the code, which can be downloaded from the site.\n"
    author: "jos"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "improvements to khtml so that Google officially supports it (e.g. http://maps.google.com/ )"
    author: "LB"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "Sorry, too late. Works in HEAD already :)"
    author: "Daniel Molkentin"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "what about gmail then ;)"
    author: "Anonymous"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "or blogger =)"
    author: "ac"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "Huh? Works here.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: more ideas"
    date: 2005-06-03
    body: "http://bram85.blogspot.com/2005/06/skonquerorfirefox.html"
    author: "ac"
  - subject: "Re: more ideas"
    date: 2005-06-04
    body: "Cool, now the \"Google supports khtml\"-part and we are in business."
    author: "LB"
  - subject: "Even more ideas"
    date: 2005-06-03
    body: "Disclaimer: Each idea should probably be discussed with whoever was/is involved with the projects they pertain to - they might not be good ideas after all. \n\n\n\n\n2. Revive KSnuffle\n\nhttp://www.quaking.demon.co.uk/ksnuffle.html\n\nSource at \n\nhttp://sunsite.mff.cuni.cz/MIRRORS/ftp.suse.com/pub/suse/i386/8.1/suse/src/ksnuffle-2.2-806.src.rpm\n\n\n+++++++++++++++++++++++++++++++++++++++++++++++\n\n\n3. Projects on KOffice open document format usage.\n\n\n+++++++++++++++++++++++++++++++++++++++++++++++\n\n\n4. QT backend for GPLFlash\n\nhttp://sourceforge.net/mailarchive/forum.php?thread_id=6948914&forum_id=43270\n\n\n+++++++++++++++++++++++++++++++++++++++++++++++\n\n\n5. QT backend for Robert O' Callahans Firefox-on-Cairo\n\nhttp://weblogs.mozillazine.org/roc/\n\nZack Rusins earlier work on Firefox for KDE\n\nhttp://www.kdedevelopers.org/node/view/976?PHPSESSID=012e7fc315a9856dfbdad28ffbc0be19\n\nhttp://webcvs.kde.org/kdenonbeta/kmozilla/\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n6. Some KDE frontend goodness for GNU Octave\n\nhttp://www.octave.org/octave-lists/archive/octave-maintainers.2004/msg00766.html \n\n\n+++++++++++++++++++++++++++++++\n\n\n7. Or some other computer algebra package like \n\nhttp://xcas.sourceforge.net/en/GIAC/\n\nor\n\nhttp://www.sai.msu.su/sal/A/1/index.shtml\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n8. Enhance Kat with Inotify support such that it automatically indexes new files (eg. new Kopete chat logs, new documents in some \"documents\" directory, maybe new mails)\n\nhttp://www.kde-apps.org/content/show.php?content=22135\n\nhttp://www.ibm.com/developerworks/linux/library/l-inotify.html\n\n\n++++++++++++++++++++++++++++++++++++\n\n\n9. Experiment with CLucene for Kat\n\nhttp://sourceforge.net/projects/clucene/\n\nsee also\n\nhttp://www.kde-apps.org/content/show.php?content=23874\n"
    author: "anders"
  - subject: "Re: Even more ideas"
    date: 2005-06-03
    body: "What do you mean by \"3. Projects on KOffice open document format usage\" ?\n\nDo you mean tools that can interact with KOffice (and with OOo if possible).\n\nHAND\n \n \n"
    author: "Nicolas Goutte"
  - subject: "Re: Even more ideas"
    date: 2005-06-03
    body: "Well, I don't know how close the switch to the Open Document formats is in KOffice, but maybe some of you KOffice guys could specify some projects that could speed or improve the switch.\n\nAnd yes, a project that involves porting the OOo .doc and .xls import/export filters to KOffice would be wonderful.\n\nThis was basically just a KDE-users wishlist.\n\nThanks for your wonderful work.\n\nAnders\n\n"
    author: "Anders"
  - subject: "Re: Even more ideas"
    date: 2005-06-03
    body: "The OOo filter still depend heavily on OOo, so having a common file format does not change much.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Even more ideas"
    date: 2005-06-03
    body: "A KDE frontend for XCas would be awesome."
    author: "Spy Hunter"
  - subject: "Re: Even more ideas"
    date: 2005-06-03
    body: "as for 6.)\nthere is already koctave - http://athlone.ath.cx/~matti/kde/koctave/\n- unfortunately, seems to be unmaintained, currently."
    author: "hoernerfranz"
  - subject: "Re: Even more ideas"
    date: 2005-08-08
    body: "8. Done :-)\n\ncheck it on http://kat.sf.net.\n\nP.S. Kat has been moved to KDE SVN Playground."
    author: "Roberto Cappuccio"
  - subject: "Some comments"
    date: 2005-06-03
    body: "Awesome ideas presented..  If I can beat my laptop into running Linux after all I might put together a proposal for one of them.\n\nUnfortunately there is no direct link to the ideas site from the google code page..  People might have trouble getting there from just the KDE.org site."
    author: "leo spalteholz"
  - subject: "Midas"
    date: 2005-06-03
    body: "Support for the Midas Spec: http://www.mozilla.org/editor/midas-spec.html\n\nI think that's (part of?) what would be required to make online HTML editing tools like http://tinymce.moxiecode.com/ or http://www.fckeditor.net/ work in Konqueror. \n\n"
    author: "cm"
  - subject: "next year"
    date: 2005-06-03
    body: "i hope such offer will happen again next year cause i'll be 18 only in august :)"
    author: "Nick"
  - subject: ":-("
    date: 2005-06-03
    body: "I've been wanting to get involved with coding in KDE for a long time (well since I first started using Linux (with the exception of the time I used Mandrake for a while).  Unfortunately I still haven't gotten familiar enough with any projects to work on any of them (I probably should just pick one app and start looking at the code and figure out the way it works).  I hope that next year I will be able to participate in the Summer of Code."
    author: "Corbin"
  - subject: "Re: :-("
    date: 2005-06-03
    body: "Would KFloppy interest you?\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: :-("
    date: 2005-06-03
    body: "One thing you could do is approach a sympathetic maintainer who hasn't got enough time to implement everything he's envisioned, but he's a pretty quick typist and can hammer out helpful emails pretty fast -- for instance, and there's no need to point at me specifically, no need at all, you could try to implement parasite loading in the Krita brush handling... I don't doubt there's someone easily reachable who can do a bit of mentoring on an informal basis.\n\nSeriously though, I have a couple of proposals for the summer of code in my drafts folder but I don't dare commit to mentoring someone well enough that they can gain the promised fortune. But on a more informal level, there's plenty of patience and sententiousness to be had from me..."
    author: "Boudewijn"
  - subject: "Please fix the heading of the referred KDE page."
    date: 2005-06-03
    body: "\nCurrently it reads: \"Join developing KDE via Google Code of Summer\".\n\nGreat initiative, I hope many of the suggestions get implemented."
    author: "Gonzalo"
  - subject: "Will all suggested proposals be accepted?"
    date: 2005-06-04
    body: "I don't mean to be a troll, but you have a few proposal suggestions that I don't think to really fit on Google's Summer of Code. Google hasn't been very specific, but they do sound like they only want software programmers, or coders to be more exactly.\n\nWhat I'm saying is that I'm not sure that Google will accept suggestions like \"making mockups for usability\". For instance, for that particulary one, you might have to add making prototypes and such, in order to make it suitable for the proposal.\n\nAnyway, I'm really glad you joined in and KDE does have IMHO the most attractive suggestions. ;)\nBy the way, you should add the (ideas) link to the Summer of Code website, like the other projects: http://code.google.com/summerofcode.html"
    author: "blacksheep"
---
KDE has joined Google's <a href="http://code.google.com/summerofcode.html">Summer of Code</a> programme.  If you are a student looking to get into KDE development this is the perfect opportunity.  We have <a href="http://developer.kde.org/joining/googlecodeofsummer.html">a list of rules and suggested projects</a>.  The deadline is soon, June 14th, for a completed proposal and you will probably need a week of communication first to ensure a good proposal for Google, so move quickly.

<!--break-->
<p>Unfortunately due to a mis-communication KDE was not among the initial list of projects involved in the Summer of Code scheme.  Congratulations to the fast moving KDE community for fixing the situation quickly.</p>

<p>If you would like to join please e-mail bounties<span>@</span>kde.org using the format listed at the bottom of <a href="http://developer.kde.org/joining/googlecodeofsummer.html">our suggested ideas page</a> to let us help you working out your application for final submission to Google.  The projects listed on that page are ideas only.  There are some more on this <a href="http://wiki.kde.org/tiki-index.php?page=Google+Bounties">Google Bounties wiki page</a>, but you are welcome to come up with your own ideas.  All projects need an established KDE contributor as a mentor.</p>

<p>We look forward to welcoming the Summer of Code students into the KDE developer community.</p>

