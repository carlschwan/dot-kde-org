---
title: "Qt 4.0 Release Candidate Available"
date:    2005-06-10
authors:
  - "binner"
slug:    qt-40-release-candidate-available
comments:
  - subject: "YUV support"
    date: 2005-06-10
    body: "Wasn't there supposed to be support for the YUV format in QImages?\n"
    author: "Jens Tinz"
  - subject: "Hindi 'Halant' character Bug fixed???"
    date: 2005-06-10
    body: "All KDE Applications crash when the 'Halant' character is entered many times. Has this been fixed in QT4?"
    author: "fast_rizwaan"
  - subject: "Re: Hindi 'Halant' character Bug fixed???"
    date: 2005-06-11
    body: "I'm just curious: what's special about that character?"
    author: "Jel"
  - subject: "Re: Hindi 'Halant' character Bug fixed???"
    date: 2005-06-11
    body: "In Indian languages, a letter consists of a consonant plus a vowel (\"a\" by default.)  But you can also get compound consonants.  For example, in my surname, if you just put a \"va\" and \"yaa\" together, you would get the wrong sound.  Instead in unicode you would express it as va + halant + ya + aa.\n\nNormally you don't see the halant.  Indian typography has different visual shapes for most compound characters."
    author: "Jaldhar H. Vyas"
  - subject: "Re: Hindi 'Halant' character Bug fixed???"
    date: 2005-06-11
    body: "Do you have a bug number or other reference for this?  Because I can't say I've noticed it."
    author: "Jaldhar H. Vyas"
  - subject: "Licensing"
    date: 2005-06-10
    body: "Hi,\n\nare licensing/pricing informations already available ? Would be cool if Qt/Console would cost significantly less than Qt/Desktop Light.\nWill the runtime licensing for Qt/Embedded remain ? It's quite costly, roughly in the same range as Win CE runtime licenses, so that the cost advantage of using Linux for embedded stuff is gone :-/\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Licensing"
    date: 2005-06-10
    body: "A toolkit given to us the FOSS movement, on a news site about an OSS desktop environment, and you complaining about licensing costs.\nWhy don't you just write free software (or else put your questions somewhere else).\nThanks"
    author: "$$/\\$$"
  - subject: "Re: Licensing"
    date: 2005-06-10
    body: "I don't think he is complaining (yet). And it is a fair question."
    author: "Amadeo"
  - subject: "Re: Licensing"
    date: 2005-06-11
    body: "Google would have told you that I am writing free software for about 8 years now, and you are probably using parts of it.\nQt rocks, so I'd like to use it also at work. The prices for the developer licenses are ok, and probably Qt/Console will cost less than Qt/Desktop.\nBut the argument \"Win CE is more expensive than Linux on embedded devices\"  becomes basically void if the runtime licenses of Qt (GUI toolkit) cost more or less the same as for Win CE (OS + GUI toolkit).\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Licensing"
    date: 2005-06-11
    body: "So do I (a KDE contributer and living from writing software). However I still think this question belongs on a Qt forum, because it discredits KDE's Qt base. KDE is about free software (but can happily run apps based on other toolkits too).\nWhat annoys me with questions like this, is that something is given to us. And instead of saying thank you, lets see how we can use this, you say I want more, cheaper etc. I don't think asking this will encourage trolltech to do more for us. Obviously they have their reasons, making a living for themselves as well. So either pay for it or use a free LGPL alternative. Either way, it's off topic for KDE.\nBtw. it's also possible to write GPL software for a living. My software is always a special product, only one use, and often the client wants the source code anyways. So by giving the client a choice for either cheaper GPL code (because of less time to implement) or more expensive proprietary code, makes it for me a non-issue. Besides, I like to be multi-functional anyways.\nSo IMO, accept what you're given and, if it's not enough, put your energy in extending/improving toolkits that do give you the freedom for writing closed source ones."
    author: "$$/\\$$"
  - subject: "Re: Licensing"
    date: 2005-06-11
    body: "Quite a few people here program for a living and would like to use Qt for that work.  There's no problem with his question; one of the advantages of dual licensing is that it can be used in either a FOSS environment or a commercial environment.  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Licensing"
    date: 2005-06-11
    body: "> are licensing/pricing informations already available ?\n\nNo."
    author: "Anonymous"
  - subject: "just tried it"
    date: 2005-06-11
    body: "wow! it rocks.. better than any previous snapshot.\nqtdemo is great. designer is very usable (there is even some interesting stuff in plugins.. ;-)\ngo trolls, go!\n"
    author: "jumpy"
  - subject: "Can't get it to use opengl"
    date: 2005-06-11
    body: "I use the latest Nvidia driver and I remember that opengl was enabled\nwhen I was compiling beta2. At that point of time I had also an older\nnvidia driver.\nI saw no configure option to turn on opengl support, does anybody have a hint?"
    author: "ac"
  - subject: "Re: Can't get it to use opengl"
    date: 2005-06-28
    body: "apt-get install xlibmesa-glu-dev\n\nthat was the missing package"
    author: "ac"
  - subject: "Font database - and new fonts..."
    date: 2005-06-13
    body: "Haven't had time to compile Qt4 just yet - but can somebody answer this simple(ish) question?\n\nIs it possible to get Qt4 to recognise newly installed fonts whilst an app is running? i.e. run an app, copy a font to ~/.fonts (or fonts:/ in konqueror), can the Qt app now see the new font? \n\nLooking at the source code for Qt3 it appears as if Qt creates a static database of the systems fonts each time an app is started, and there is no way to ask Qt to re-build this database. I assume the same applies to Qt4? (Xft, which Qt uses, can  be told to refresh its list of fonts - so Qt has no excuse...)"
    author: "Craig"
  - subject: "win binaries?"
    date: 2005-06-13
    body: "Argh, and still no GPL'ed win binaries?\nI can't see the point to keep 'em back, since the official release will include a GPL'ed version.\nI need these win version _now_..."
    author: "katakombi"
  - subject: "WYSIWYG"
    date: 2005-06-13
    body: "I still don't see how it is going to do WYSIWYG?\n\nHowever, PaintDeviceMetric seems to be missing from the documentation."
    author: "James Richard Tyrer"
  - subject: "slow?"
    date: 2005-06-14
    body: "I'm surprised noone has mentioned that yet, but\nto me qt4 feels slower, sluggish, definitly\nslower than qt3 / kde. the arthur stuff (bin/qtdemo)\nis creeping (but I had to disable RenderAccel due\nto stability issues - however, that might not\nchange anything anyway), but other things as well:\nresizing the columns in the qtdesigner properties\npane lags behind the mouse cursor, that doesn't\nhappen with qt3.\nMind you, this is an old machine (900 mhz cpu), but\nit'd be a pity if all of kde got slower by that amount\nnext year.\nor am I doing something wrong?\n\nJens"
    author: "Jens"
  - subject: "Re: slow?"
    date: 2005-06-15
    body: "Yes, you disabled RenderAccel."
    author: "ac"
  - subject: "Re: slow?"
    date: 2005-06-15
    body: "yup disabling renderaccel drags it all down. otherwise it looks tasty"
    author: "Dr_Bollokovski"
---
<a href="http://www.trolltech.com/">Trolltech</a> has <a href="http://www.trolltech.com/newsroom/announcements/00000207.html">announced the Qt 4 release candidate</a>, the final preview of next generation Qt. The final release of Qt 4.0 is scheduled for late June until which Trolltech will focus on addressing critical stability issues only. This release introduces several new features and capabilities including completely refreshed and re-engineered Qt demos and examples. Commercial offers were restructured into Qt Console for non-GUI development, Qt Desktop Light replacing the Professional and Qt Desktop comparable to Enterprise editions. Download it from <a href="ftp://ftp.trolltech.com/qt/source/">ftp.trolltech.com</a> (<a href="http://www.trolltech.com/download/betas.html">mirrors</a>) or read the online <a href="http://doc.trolltech.com/4.0/">Qt Reference Documentation</a> for <a href="http://doc.trolltech.com/4.0/qt4-intro.html">What's New in Qt 4</a>.


<!--break-->
