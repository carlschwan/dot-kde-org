---
title: "Malaga Named as Location for 2005 KDE Conference"
date:    2005-02-02
authors:
  - "jriddell"
slug:    malaga-named-location-2005-kde-conference
comments:
  - subject: "Great!"
    date: 2005-02-02
    body: "Great! I live in Valencia (Spain). I was thinking about traveling to Germany if the conference was there, but now I have it only 600 km away. :)"
    author: "V\u00c3\u00adctor Fern\u00c3\u00a1ndez"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "Well you are kind of traveling to Germany, since Malaga is pretty much a German province."
    author: "Ian Monroe"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "Yes, right, seems is following the Mallorca</a> experience :)\nhttp://www.infomallorca.net\n\nGreetings from Barcelona!\nhttp://www.bcn.es\n\n--\nJosep Payo\nTijuana Time - Josep Payo Weblog\nhttp://blog.solucio.com/"
    author: "Josep Payo"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "Where in? Your name sounds familiar... From Gandia, perhaps?"
    author: "jesuscliment"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "No, I'm from La Pobla de Vallbona (25km away from Valencia). I'm in PoLinux and I'm the one behind Plastikfox. Your name is also familiar to me, from Barrapunto I think."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "Victor, I will go with you, when buy the tickets? xD"
    author: "th1nk3r"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "\u00a1Hombre, th1nk3r! \u00bfC\u00f3mo va todo? Supongo que lo comentaremos por la lista por si se quiere apuntar m\u00e1s gente. Tenemos localizado un albergue que est\u00e1 de puta madre, as\u00ed que podr\u00edamos ir en manada, jejeje."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Great!"
    date: 2005-02-02
    body: "Sure! I live in Sevilla, I think that I will take a look to the event :)"
    author: "NabLa"
  - subject: "off-topic : atlantik is back..."
    date: 2005-02-02
    body: "well, to make a story short... As you guys may know already, Atlantik server has been down for a while. but looks like they are back on track.\n\nI'm currently playing using 0.7.5 (3.4 cvs) against somebody using 0.7.1 (3.3)\n\nwhich means it probably works for most of us.\n\ncome on folks, lets play atlantik .....\n\n"
    author: "Mathieu Jobin"
  - subject: "Time / date"
    date: 2005-02-02
    body: "Has the date been set? 13 megametres is a long way to come, and it takes a while to organise if you don't want to pay a fortune."
    author: "Brad Hards"
  - subject: "Re: Time / date"
    date: 2005-02-02
    body: "Likely August to September"
    author: "Anonymous"
  - subject: "Re: Time / date"
    date: 2005-02-02
    body: "I guessed late summer, but I was hoping for a bit higher resolution (like, perhaps which week, or a couple of options, rather than a couple of months:-).\n\nEven knowing when the schedule will be released would help... "
    author: "Brad Hards"
  - subject: "Re: Time / date"
    date: 2005-02-02
    body: "There are not so many weeks spanning from August to September."
    author: "Anonymous"
  - subject: "Re: Time / date"
    date: 2005-02-02
    body: "9"
    author: "anonymous"
  - subject: "Re: Time / date"
    date: 2005-02-02
    body: "Language confusion aside it's only one week."
    author: "Anonymous"
  - subject: "I will be there!"
    date: 2005-02-02
    body: "See you in Malaga!"
    author: "Ignacio Monge"
  - subject: "Antonio, you rock!"
    date: 2005-02-02
    body: "Great!\n\nIn the akademy, Antonio told us, the spanish guys who came there, that Malaga was a candidate for the next conference. I am really happy with this decision, because the amount of spanish people that likes KDE, is big, but we don't make many meetings, and sometimes we don't seem very active.\n\nI live a bit far from Malaga, but in the time I'm at the conference, I will try to do my best in helping. I had a great time at Ludwigsburg, and I hope we'll have a lot of fun in Malaga.\n\nEnjoy!"
    author: "suy"
  - subject: "I'll be there!"
    date: 2005-02-02
    body: "If the conference is held in late September, I'll be there. I expect to be back in Granada by then and it should be a short drive.\n\nBTW, KDE keeps on getting better and better. You guys rock!\n\nTo the rest of the KDE developers. We, Spaniards, are going to teach you how to party. :)"
    author: "Gonzalo"
  - subject: "(hopefully) I'll be there!"
    date: 2005-02-02
    body: "Wow, it's a lot of time I didn't go to Spain, there are many lowcost flights from italy. But, when it will happen?\n\nDave\n"
    author: "David Vignoni"
  - subject: "Malaga Named as Location for 2005 KDE Conference"
    date: 2005-02-02
    body: "Hope it won't rain in Malaga during the conference :-)\n\nTorsten"
    author: "Torsten Rahn"
  - subject: "Re: Malaga Named as Location for 2005 KDE Conference"
    date: 2005-02-02
    body: "Here, in Seville, didn't rain since november, and it was only for a few days, before that...since spring, I think.\n\n\nLol..my \"engrish\" scaries me :-S"
    author: "TeXTer"
  - subject: "Re: Malaga Named as Location for 2005 KDE Conferen"
    date: 2005-02-03
    body: "To clarify for non-spanish people, Seville is close to M\u00e1laga (around 100km away if I don't remember bad). And in M\u00e1laga it doesn't rain usually too."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Malaga Named as Location for 2005 KDE Conferen"
    date: 2005-02-10
    body: "225Km away from Sevilla, and 100Km away from Granada. Hearonly rain from winter."
    author: "fernando"
  - subject: "Great news"
    date: 2005-02-02
    body: "That's great news! Many, many people in Spain love KDE :-)\n\nThanks to all KDE developers and contributors for their hard work!"
    author: "Anonymous"
  - subject: "RE:"
    date: 2005-02-02
    body: "Is there anybody we can reffer to for helping us getting visas for Spain ?? (We are from Serbia)\n"
    author: "I_LOVE_KDE"
  - subject: "RE:"
    date: 2005-02-02
    body: "Visa spam already starts? You will get no invitation if you're not known as KDE contributor."
    author: "Anonymous"
  - subject: "Good idea"
    date: 2005-02-03
    body: "I\u00b4m from Malaga and very well because I hope help in organization with Linux-Malaga :)\n\nCongratulations to Antonio Larrosa."
    author: "cinefilo"
  - subject: "KDE/FreeBSD"
    date: 2005-02-11
    body: "Anyone coming from the KDE/FreeBSD team?"
    author: "anonymous"
  - subject: "Andalucia"
    date: 2005-02-11
    body: "For those who have never been in Andalucia:\n\n1) The sun is shining almost everyday in the summer, so be prepared for that!\n2) Most people in Andalucia is monolingual. They speak another kind of Spanish (they eat consonants among other things). Sometimes is like are they talking in Spanish or what?\n3) The people there is kind of funny and love to party all night-long.\n4) They've got bullfights and flamenco (kind of folk music).\n5) Some people think that Spain is Andalucia. Wrong! Every region is different. It's much like other parts of Europe (full of contrasts).\n6) I don't have anything against Malaga but there are much more beautiful cities in Andalucia (and cleaner too) like Granada; there you can visit the Alhambra (a famous Arabian monument), the mountains (Sierra Nevada), ... and many other places. Did you know that Andalucia is bigger than Switzerland?\n7) Enjoy your stay! :)\n"
    author: "Anonymous"
  - subject: "Re: Andalucia"
    date: 2005-02-21
    body: "Is Granada really cleaner then Malaga? At least the air in Granada was very dirty when I was there, apparently they have the highest per-capita Vespa/Moped usage in Spain. Haven't been to Malaga.\n\nI agree, Granada is a good place to visit though. I was there for a couple of weeks taking Spanish classes, but still didn't get to do everything I wanted."
    author: "Ian Monroe"
---
After an evaluation process of several possible locations, <a href="http://en.wikipedia.org/wiki/MÃÂ¡laga">Malaga</a> in southern <a href="http://en.wikipedia.org/wiki/Spain">Spain</a> has been chosen as the location of the 2005 KDE conference by the <a href="http://www.kde.org/areas/kde-ev/members.php">KDE e.V. membership</a> in a recent vote. The conference will be held by <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> in
cooperation with different sponsors.



<!--break-->
<p>Antonio Larosa Jimenez has been appointed the Assembler, the project
manager for the conference. It is his rightful duty and privilege to decide
on a conference name. Antonio will have a budget available provided by KDE
e.V. for his expenses during the first phase of the meeting preparations.</p>

<p>All KDE contributers are encouraged to actively support Antonio. The conference is the main KDE event of the year.</p>




