---
title: "App of the Month: KDissert"
date:    2005-10-15
authors:
  - "jriddell"
slug:    app-month-kdissert
comments:
  - subject: "klik://kdissert"
    date: 2005-10-15
    body: "klik://kdissert works beautifully on my SUSE-10.0 box!"
    author: "ac"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "I'm using SuSE 10.0 and when I type \"klik://kdisser\", Konqueror says \"Protocol not supported: klik\". Wassup?"
    author: "blacksheep"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "I'll try a wild guess, have you klik installed? "
    author: "Morty"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "Take a look at http://www.opensuse.org/SUPER_KLIK"
    author: "rinse"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "FIRST, it is \"klik://kdissert\" (and not \"klik://kdisser\")\n\nSECOND, you need to have the klik client installed (less than 20 kBytes, less than 20 seconds of effort)  (*)\n\n------------------\n\nTo install the klik client:\n\n* Hit \"[alt]+[f2]\"\n* Type \"wget klik.atekon.de/client/install -O -|sh\"\n* Hit \"Enter/OK\"\n* Follow instructions on screen\n\nDone."
    author: "Kurt Pfeifle"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "That install script is not exactly foolprof, or in other words it fails for me. The first error I get is for not having Xdialog, easily solved by making link to kdialog. Anyway automatic selection of kdialog/gdialog(?)/Xdialog could be an idea.\n\nThe second error I got was in the post install part, the folowing error: \nsh: line 512: firefox: command not found\nSame thing here really, perhaps looking for Konqueror too, and some distros use mozilla-firefox rather than simply firefox. And some may prefer the Mozilla suite rather than standalone firefox."
    author: "Morty"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "It would help debug this if you told the exact distro version you tried klik on....\n\n(And yes, \"automatic selection of kdialog/gdialog(?)/Xdialog\" is as good an idea that it even is already implemented. It just happens, that in your (exotic?) case it didnt work out as intended...)"
    author: "Kurt Pfeifle"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "I had not really expected klick to work on my system anyway, it's not so much exotic as a case of mixing old and new. But with statements like this \"less than 20 kBytes, less than 20 seconds of effort\", there really are no reasons for not trying:-)\n\nI'm running a old Mandrake 10.1, with KDE svn in /opt. Used ftp install so I have only installed the things I need, looks like Xdialog never has been needed:-). Perhaps having kdialog in /opt was the reason it was not found, anyway I'm not sure spending time debuging it are worth it:-). Except for the issues With XDialog and firefox, it did install. \n\n"
    author: "Morty"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "I had the same problem. I had to install Xdialog, since it wasn't already on my system (Debian etch, upgraded a few times from, oh... potato, originally? :^) )"
    author: "Bill Kendrick"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "\nIf kdialog is in your $PATH, this would help too:\n\n\"DIALOG=kdialog wget klik.atekon.de/client/install -O -|sh\",\n\nor\n\n\"DIALOG=/opt/bin/kdialog; export DIALOG; wget klik.atekon.de/client/install -O -|sh\"\n\nor similar\n"
    author: "Kurt Pfeifle"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "Works nice on suse 9.3 as well :)\nAnd fast, before I knew it, kdissert popped up on my desktop"
    author: "rinse"
  - subject: "Re: klik://kdissert"
    date: 2005-10-16
    body: "When I go to that URL, a window appears telling me it's going to download an RPM from http://ftp4.gwgd.de/ but then nothing happens.  After trying a few times, I discovered I had some suspicious looking files in my home directory, \"index.php?mid=23403\", \"index.php?mid=23403.1\", etc.  Looking inside, it looks like some page from a music download website called \"chimp3.com\".  WTF???"
    author: "Bill Kendrick"
  - subject: "Re: klik://kdissert"
    date: 2005-10-17
    body: "This recipes uses as its input file an RPM from http://ftp.gwdg.de/pub/linux/misc/suser-guru/rpm/packages/Office/kdissert/ to create the klik .cmg from (most others use a set of .deb files).\n\nUnfortunately, minutes after createing and testing the recipe, the version was bumped by Guru, the RPM packager. He removed the 1.0.4 an made a 1.0.5 available. So the recipe's link wasn't valid any more. The index.php file you received was an error in the recipe: there is a line that is usually commented out (and which we use for debugging only) which may point to various other non-klik related sites (such as the chimp3 one). Sorry for the inconvenience.\n\n(You can easily verify/control what the .klik script does, by running it from the commandline like this: \n\nuser@host:~>  sh -x $HOME/.klik kdissert\n\nI hope this helps.)"
    author: "Kurt Pfeifle"
  - subject: "Re: klik://kdissert - no spellchecking"
    date: 2005-10-22
    body: "It works beautifully on my SuSE 10.0 box. There is one minor flaw. Spellchecking does not work. Spellchecking is done, but not saved. Anyone with suggestions?\n\nWolfgang"
    author: "Wolfgang"
  - subject: "Amazied"
    date: 2005-10-15
    body: "I am amazed by the breadth and quality of available applications for KDE. It's a good sign when you start to see real diversity in the applications, not only image viewers and mediaplayers.\n\nAs for mindmapping I remember looking at it when I was a student, but back then the old pen and paper version was really no good with my workpattern. KDissert looks much more practical, and to looks like it concentrate more on aiding the workflow than doing mindmaps for the sake of doing mindmaps. Looks flexible and userfriendly, I definitely have to try it out. \n\nOne question tho, is it possible to make hierarchical maps with it? Having  boxes representing submaps and a easy way to navigate up and down in the structure. Since I find it usefull to work on problems that way and it looks like more complex maps soon would clutter the workspace.\n\nSince I know makefies and automake are evil, everything I hear about BKSys fills me with joy. I'd love to see a HOWTO or step by step guide on how to convert a automake project to use BKSys(perhaps aimed at those of us with lesser skill in the black art of buildsystems:-). \n"
    author: "Morty"
  - subject: "Benefits?"
    date: 2005-10-17
    body: "Perhaps it is merely a reflection of my nay-saying personality, but I would like someone to explain to me the benefits this visual tool over the form of a purely textual outline.  \n\nI cannot see any added benefits and an outline can be developed with many readily available tools.  Even pictorial elements could be added to an outline, if needed."
    author: "Ross Baker"
  - subject: "Re: Benefits?"
    date: 2005-10-17
    body: "Mind maps benefits are described all over the web, see for example http://members.optusnet.com.au/~charles57/Creative/Mindmap/Radiant.html"
    author: "anonymous"
  - subject: "Re: Benefits?"
    date: 2005-10-17
    body: "A purely textual outline will always impose a greater degree of structure, in the way the outline is created and developed. This tool allows using a much more unstructured approach. It will give more flexibility in the initial or brainstorming phase. And you can export it to a textual outline, at the point when switching to that mode of working is more appropriate. \n\nBut I guess it mostly boils down to your personal workpattern and style if you will get any benefit out of it or not. Some people are more visually oriented than others, perhaps making this approach better suited for them. "
    author: "Morty"
---
<a href="http://freehackers.org/~tnagy/kdissert/">KDissert</a> is KDE's mindmapping tool.  App of the Month interviews KDissert's author Thomas Nagy about why he started it, the relationship to <a href="http://dot.kde.org/1126452494/">BKSys</a> and his plans for the future.  There is also an overview to help you get started using this exciting application and for the first time we have a <a href="http://kde.org.uk/apps/kdissert/flash.html">Flash Demonstration</a> of KDissert in action.  Enjoy app of the month in <a href="http://www.kde.nl/apps/kdissert/">Dutch</a>, <a href="http://www.kde.org.uk/apps/kdissert">English</a>, <a href="http://lionel.tricon.free.fr/PERSO/aotm/indexfr_kdissert.html">French</a>, <a href="http://www.kde.de/appmonth/2005/kdissert/index-script.php">German</a> and <a href="http://www.kde.org.tr/index.php?option=com_content&amp;task=view&amp;id=73&amp;Itemid=1">Turkish</a>.




<!--break-->
