---
title: "Free Software Marketing"
date:    2005-03-05
authors:
  - "jriddell"
slug:    free-software-marketing
comments:
  - subject: "download?"
    date: 2005-03-05
    body: "I have read the interview with Bernhard Reiter.\nHe has also given a speech at aKademy, is this speech somewhere online?\nI can remember that there was an opportunity to download speeches from the akademy, but i can't find it anymore."
    author: "pinky"
  - subject: "Re: download?"
    date: 2005-03-06
    body: "Yes, on the wiki which is down.\n\nI have a copy here\nhttp://jriddell.org/programs/akademy/user-writeups-9-free-software\n"
    author: "brockers"
  - subject: "Re: download?"
    date: 2005-03-07
    body: "His slides are linked at http://conference2004.kde.org/sched-userconf.php"
    author: "Anonymous"
  - subject: "Guerrilla Marketing and Interview"
    date: 2005-03-06
    body: "Thanks for the links. These are very interesting articles. I also liked\n\"Every Engineer's Checklist for Justifying Free Software\" from the first issue ( http://www.freesoftwaremagazine.com/free_issues/issue_01/engineers_checklist/ )."
    author: "Claire"
  - subject: "time for spreadKDE.com?"
    date: 2005-03-07
    body: "why not follow spreadfirefox.com good example and create a spreadKDE.com site based on civicspace? We need a place to discuss, create marketing materials, installation tutorials and call KDE users to arms. What do you think?"
    author: "Flavio"
  - subject: "Re: time for spreadKDE.com?"
    date: 2005-03-07
    body: "The place to discuss this already exists, the kde-promo mailing list."
    author: "Anonymous"
---
<a href="http://www.freesoftwaremagazine.com/free_issues/issue_02/">Issue 2 of the Free Software Magazine</a> contains two articles by KDE promo member Tom Chance.  In <a href="http://www.freesoftwaremagazine.com/free_issues/issue_02/guerrilla-marketing/">Guerrilla Marketing</a> he discusses how KDE's promotion efforts compare to those of other Free Software projects while his <a href="http://www.freesoftwaremagazine.com/free_issues/issue_02/akademy_interview/">interview with Bernhard Reiter</a>, conducted at aKademy last year, discusses marketing in government and software freedom on the desktop.


<!--break-->
