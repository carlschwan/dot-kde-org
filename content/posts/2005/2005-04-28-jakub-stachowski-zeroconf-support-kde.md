---
title: "Jakub Stachowski: Zeroconf Support in KDE"
date:    2005-04-28
authors:
  - "jriddell"
slug:    jakub-stachowski-zeroconf-support-kde
comments:
  - subject: "Thanks"
    date: 2005-04-28
    body: "Thanks for this interview, learned a lot about ZeroConf and the great work done for it in KDE."
    author: "Anonymous"
  - subject: "OT: acid test.."
    date: 2005-04-28
    body: "These patches anywhere near konqui yet?\n\nhttp://weblogs.mozillazine.org/hyatt/"
    author: "Anonymous"
  - subject: "Re: OT: acid test.."
    date: 2005-04-28
    body: "http://www.kdedevelopers.org/node/view/1001"
    author: "Anonymous"
  - subject: "Re: OT: acid test.."
    date: 2005-04-28
    body: "Thanks Zack for clearing this up. That makes me remember not to buy any Apple products now, or in the future."
    author: "Anonymous"
  - subject: "Re: OT: acid test.."
    date: 2005-05-06
    body: "Why? Because they forked the KHTML code?\n\nWhat would you have them do, slow development of WebKit to suit the KHTML developers?"
    author: "Anonymous"
  - subject: "Re: OT: acid test.."
    date: 2005-05-06
    body: "> What would you have them do, slow development of WebKit to suit the KHTML developers?\n\nNo, to the contrary.  Speed up the development of KHTML for both sides by combining development workforces and by maintaining a common system-independent version in a common public CVS. The system-dependent part (KWQ and whatever else they may have added) could have been maintained wherever Apple wanted it to reside. \n\n"
    author: "cm"
  - subject: "This is a little disturbing"
    date: 2005-04-28
    body: "Seems like we just talked about this a little while ago (\nhttp://dot.kde.org/1097096753/1097113373/ ).\n\nI know that Apple is complying with the letter of the law on this one... but perhaps we can bring some attention to this and maybe it will make apple play fair.\n\nOne thing we can do is make some noise, it's probably too late now though (too much forking???).  \n\nI'll try to <A href=\"http://panela.blog-city.com\">blog</A> about this tonight.\n\n\n"
    author: "foobie"
  - subject: "Re: OT: acid test.."
    date: 2005-04-29
    body: "Boo-f******-hoo.\n\nI use Debian Linux with KDE 3.4, daily and have been using KDE the first day I installed Debian 4 years ago. I also use GNUstep which Debian does a fair job at making it interoperate with KDE.\n\nBeing that no one gives too shits to the wind about GNUstep or implementing native support for Objective-C and ObjC++ within KDE (Openstep is right there and you do have GNUstep to work with) it amazes me that KDE wants Apple to implement their version of KHTML to KDE's desires.  Afterall, GCC 4.1 is supposed to have ObjC++ support.\n\nAs was written on that blog: KHTML was dying and the high profile Apple gives it with code blobs back to KDE only reminds me how dense people can be--there is no such thing as a free meal.\n\nIf KDE can't get enough coders together to duplicate the code blobs updates for KHTML so be it--its their failed responsibility to keep the project viable.  Why doesn't Trolltech step up and produce a KHTML web browser engine for KDE? Or better yet, coordinate with Apple to help smooth the situation over?\n\n"
    author: "Marc Driftmeyer"
  - subject: "Re: OT: acid test.."
    date: 2005-04-29
    body: "Erm... KHTML was dying? You really could have fooled me. I was using it in Konqui on the desktop, as was 50% or higher of the Linux user market and the KDE on BSD folks and many more. KHTML has been ported to numerous platforms, and indeed, provides web browsing to a number of other small OS projects.\n\nWith the current rate of growth in Linux and BSD use, it is entirely possible that more people have Konqui installed than Safari (or indeed OSX as a whole), or they will have within the next year (statistics on Linux use being vague as they are...).\n\nIf Apple writes ugly code that can't be merged without building extensive support harnesses, then maybe it is faster to reimplement that code. That's what Zack is saying, and no doubt, that is probably the intention of Apple, to be quite frank. Manpower resources of any project would have problems fixing this mess; it's not a reflection on the KHTML team strength.\n\nBottom line: The KHTML developers made nice software, LGPL'd it, Apple benefited (they got that free meal that you talked about) and decided not to play nicely with the developers of the nice modern rendering engine that they got *for free*.\n\nThey're a standard selfish company that is chasing profit and not goodwill. That's life and that's okay; but there is no need to blame the KHTML developers for Apple's failures."
    author: "Luke Chatburn"
  - subject: "Re: OT: acid test.."
    date: 2005-04-29
    body: "> Why doesn't Trolltech step up and produce a KHTML web browser engine\n\nAn html engine? Because they work next door to Opera and know from cafeteria talk how hard work that is? :-)"
    author: "Anonymous"
  - subject: "Re: OT: acid test.."
    date: 2005-04-29
    body: "\"Being that no one gives too shits to the wind about GNUstep or implementing native support for Objective-C and ObjC++ within KDE (Openstep is right there and you do have GNUstep to work with) it amazes me that KDE wants Apple to implement their version of KHTML to KDE's desires. Afterall, GCC 4.1 is supposed to have ObjC++ support.\"\n\nWell, I care about Objective-C - all my work on bindings was funded by NeXTSTEP/Objective-C contracting. \n\nI spent 6 months working on Qt/KDE bindings for C and Objective-C, but nobody much was interested, and I never quite got the KDE bindings to link properly, although the Qt-only one worked fine. I think it makes more sense to use GNUstep if you like Objective-C, and if you don't like C++ you can program KDE in ruby or python.\n\nUntil ObjC++ is part of gcc, it isn't really feasible. I learned that if you have to wrap a C++ api in C, and then once again in Objective-C, the whole thing is just too enormous. Once we can go straight to Objective-C via ObjC++ it will be a different matter. And use the Smoke library via -doesNotRecognizeSelector:, so there would be a lot less glue code with just one entry point from Objective-C to C++. You divert the entire api into a single -doesNotRecognizeSelector call, and do all the possible types of Objective-C <-> C++ marshalling in one place."
    author: "Richard Dale"
  - subject: "Re: OT: acid test.."
    date: 2005-04-29
    body: "To suggest that khtml is or was dying is a troll. It isn't and wasn't.\n\nA little history here. Apple is doing the minimum it can to fulfil it's license operations. That is their privilege and right.\n\nApple announced Safari with much fanfare after working on it for around a year. They based their work on an earlier version of khtml, did their changes, integrating the improvements from the khtml team over that time. All in secret. Then they released the software, and as per lgpl, released the source code. Already at that point the code bases had diverged, some due to the removal of any Qt stuff, and over that year considerable progress had been made on khtml without Apple's contribution.\n\nMany improvements were merged into khtml and that work continues. The merging consists of taking a tarball, figuring out the differences from the previous one, matching that to the changelog, factoring out the Apple specific api stuff, and then changing khtml. Repeated requests have been made to have access to the source management tree so that at least patches could be extracted for specific issues. But no.\n\nApple can do what it wants. Fine. They have essentially taken on the development and maintenance of a browser and html engine. Any of the economics of FOSS don't apply anymore here. They are on their own. Any goodwill that may have come from this decision is gone. The license responsabilities are an expense without any of the benefits.\n\nKhtml development continues and will continue with or without Apple. Maybe they figured that eveyone using KDE would drop it for the vastly better MAC os, all the while kissing their feet with admiration and devotion, and continue to improve khtml for Apple's benefit. It hasn't happened that way. When it is no longer in Apple's interest to spend the money to maintain Safari, they will discontinue development. Apple is in a situation where the squeeze is coming from both sides. Microsoft is again taking interest in the browser. The free desktops are grabbing the Microsoft castoffs, or at least a percentage of them. The standard zero sum game of the software business is at play. I ran across an interesting comment the other day. Two three years ago at the Linux conferences most people were using Apple products. That is no longer the case due to the improvements in Linux and the desktops.\n\nWhat Apple does is up to them. I really don't care for them or their products, and even less so seeing how they work with the free software community. I don't need to buy their stuff, and they have given me a reason why I shouldn't. The odd thing is that they would have benefitted from a little openness. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: OT: acid test.."
    date: 2005-04-29
    body: "You are damn right!"
    author: "veton"
  - subject: "multicast? why multicast?"
    date: 2005-04-28
    body: "Everybody know multicast doesn't scale.  It can be \"pretty\" on LANs with, say, up to a dozen boxes, but on LANs with tens, even hundreds of boxes is a maintenance nigthmare.  Indeed, anything that pushes directly from the \"clients\" is a maintenance nigthmare sooner or later.   All you win when your LAN is short is lost when it grows up."
    author: "devil advocate"
  - subject: "Re: multicast? why multicast?"
    date: 2005-04-28
    body: "mDNS daemons are required to aggresively cache everything they get to avoid unnecessary traffic - so it should scale better than unicast. And what exactly are the maintenance problems do you expect?\n "
    author: "Jakub Stachowski"
  - subject: "Re: multicast? why multicast?"
    date: 2005-04-29
    body: "How about security concerns: Think about a very quick answer from a malicous host, which binds all the services. Neat for phishing or man in the middle attacks."
    author: "anon"
  - subject: "Re: multicast? why multicast?"
    date: 2005-04-29
    body: "Give examples please.  Your groundless criticism is not constructive.  This is not NetBIOS; Zeroconf is a real standard made by real networking experts and standardized by the IETF.\n\nZeroconf is the direction that networking in general needs to move.  Networking should be as easy as plugging in a cable (or just using wireless).  There's no reason it needs to be a maintenence nightmare when scaled up; a smooth transition is possible.  If anything is going to provide that, it's Zeroconf and the IETF."
    author: "Spy Hunter"
  - subject: "Licensing"
    date: 2005-04-28
    body: "Licensing..\n\nI know this isn't slashdot and people are a bit more mature, but just incase anyone wants to attack debian for being pedantic about not liking the apple license I want to point why debian etc doesn't like it.\n\nThe license basically says that if you sue apple for anything then your rights to any of its software is revoked.\n\nThis would mean that any company that sues apple over anything (and remember apple is mostly a hardware company) then that company would have have problems using both gnome and kde.\n\nJohnFlux\n\np.s. I think Apple is being a real dick about this.  Just to indicate my bias.\np.p.s.  I helped jacubs add dns-sd to kbattleship so send some of the women and fame to me too."
    author: "JohnFlux"
  - subject: "Re: Licensing"
    date: 2005-04-28
    body: "If I understood correctly this only applies to the licence of the daemon, so the client lib which KDE uses is considered free by Debian as well.\n\nAnd having support built in but not having the daemon running wouldn't result in any harm, only the service will remain useless.\n\nBut I guess nobody will bother to write a compatible replacement daemon in case this new implementations works good enough."
    author: "Kevin Krammer"
  - subject: "Re: Licensing"
    date: 2005-04-29
    body: "Let's see. Wibble on over to the link. Opps don't have an account. Let's see, register. Hmmm, they want email, name and address and to top it off they want my birthday? Now I realize I could put in false information but that is not my nature.\n\nDisappointing it is Apples version would be chosen over howl. Instead of working with the howl developers to over come some \"deficiencies\"."
    author: "stumbles"
  - subject: "Re: Licensing"
    date: 2005-04-29
    body: "At first, KDE's Zeroconf code was based on Howl but got rewritten. Howl internally uses forked old version of Apple's code, so 'overcoming deficiencies' would be just recreating work that is already done in mDNSResponder. What for? If you have problem with registration then fine, download from mirror (check Gentoo ebuild for addresses)."
    author: "Jakub Stachowski"
  - subject: "Re: Licensing"
    date: 2005-04-29
    body: "Well thanks for the info, I did not know that about howl."
    author: "stumbles"
  - subject: "We need more of that"
    date: 2005-04-28
    body: "Thanks for the great interview. I think, Zeroconf is a technology from which we can benefit a lot. It simply feels right: Don't let the user put in tons of configuration data, but make use of the technological power we have.\n"
    author: "Cornelius Schumacher"
  - subject: "Universal service discovery framework?"
    date: 2005-04-29
    body: "Would it be possible to implement some kind of 'universal service discovery framework' which needs to be implemented in apps only once, starting with Zeroconf, but can then be transparently extended with other such protocols like SLP, uPNP and whatever other service discovery protocols proprietary application developers come up (like what Skype uses)?"
    author: "ac"
  - subject: "Re: Universal service discovery framework?"
    date: 2005-04-29
    body: "This is exactly my plan for future. However, I still don't know how to deal with differences in service types between SLP and DNS-SD. SLP uses abstract-type:protocol (where abstract-type is application specific, for example 'drivers' and protocol is high-level protocol like http, ftp, etc.).\nDNS-SD uses [_subtype]._type._protocol where protocol can be UDP or TCP, type is application-specific protocol like rfb, http, ftp, etc. Subtype adds extra info (for example _anon._ftp._tcp). So what I need is kind of abstract naming that can be translated into DNS-SD and SLP (and possibly others) forms."
    author: "Jakub Stachowski"
  - subject: "Re: Universal service discovery framework?"
    date: 2005-04-29
    body: "That's cool. Hope you come up with a good solution for this problem."
    author: "ac"
  - subject: "Another Implementation"
    date: 2005-04-29
    body: "Searching with Google I found another GPL implementation\ndeveloped using the .net framework at forge.novell.com \n\nhttp://forge.novell.com/modules/xfmod/project/?mdnsresponder"
    author: "johng"
  - subject: "kde-redhat includes Zeroconf support"
    date: 2005-04-29
    body: "*shameless plug*\nThe RedHat/Fedora-Core KDE builds available from\nhttp://kde-redhat.sourceforge.net/\ninclude ZeroConf support."
    author: "Rex Dieter"
  - subject: "Thanks to Jakub Stachowski"
    date: 2005-04-30
    body: "Thanks Jakub for working on this."
    author: "Mattias"
  - subject: "Fedora Core 3 includes the mDNSResponder service"
    date: 2005-04-30
    body: "I've switched to gentoo now, but I believe this was enabled at runlevel 3-5 by default?  Given Fedora is gnome centric, does anyone know if gnome supports multicast dns as well?"
    author: "Crimson"
  - subject: "How about jabber?"
    date: 2005-05-01
    body: "If you want to implement this via an Instant messenger you should probably look at the jabber protocol. It's a free protocol in xml that can be easly extended for such purpose. (www.jabber.org)"
    author: "Le Thanh Duong Olivier"
  - subject: "Re: How about jabber?"
    date: 2005-05-03
    body: "I know about Jabber and probably due to its extensible design it will be easy to implement service discovery on top of it. But it does not rule out other protocols - as long as they provide some kind of out-of-band channel it is fine."
    author: "Jakub Stachowski"
  - subject: "Polska g\u00f3r\u00b1"
    date: 2005-05-09
    body: "Polskaaaaaaa bia\u00b3o-czerwoniiiii"
    author: "DUMNY POLAK"
---
Last month's release of KDE 3.4 was the first to feature Zeroconf's service discovery.  Relevant applications can advertise their services, such as shared folders or networked games, which can then be browsed with the zeroconf:/ ioslave.  KDE Dot News spoke to the man behind KDE's Zeroconf support Jakub Stachowski to find out more on this exciting technology.  Read on for the interview and if you have more ideas for KDE technology interviews please add them to <a href="http://www.kde.me.uk/index.php?page=kde-technology-interviews">this wiki page</a>.







<!--break-->
<p style="float: right"><img src="http://static.kdenews.org/jr/jakub-stachowski.jpg" width="96" height="100" /><br />Jakub Stachowski</p>

<p><strong>Please introduce yourself and how you help KDE.</strong></p>

 <p>I'm Jakub Stachowski, a fourth year Electronics and Telecommunications student from Czempi&#324; in Poland. My road with KDE is a typical one - I have used it for several years, then started writing small applications for private use and finally decided to join development. My main field of work in KDE is Zeroconf related: maintaining and improving library and support in various applications. I also fix bugs and add small features to various KDE applications when I have some free time and feel like hacking.</p>
 
<p><strong>How did Zeroconf within KDE start and how did you get involved?</strong></p>
 
<p>It started about a year ago with a wiki page giving some information about Zeroconf and which KDE applications could benefit from it. I found this concept promising so I returned to that page quite often to see if anything was being done yet. Time passed and it remained in concept stage. I decided to take it on myself because I had some free time and I wanted to help improving KDE. This project seemed like a good way to do it. The openness of the KDE developers community was a very pleasant surprise - no questions asked, just 'here is your cvs account, have fun'. I got lots of help too, like pointing out errors in design, fixing bugs and generally good advice.</p>

<p><strong>What do all the different Zeroconf names mean? There's Zeroconf, dnssd, SLP, Rendezvous and Bonjour.</strong></p>
 
<p>Zeroconf is a name used by IETF for several techniques that should allow you to setup and use a simple network with no need for any manual configuration. One of its parts, DNS-SD, is the service discovery protocol based on standard DNS. Rendezvous is just Apple's name for Zeroconf, this has recently been changed to Bonjour after a trademark dispute. It is trademarked so you will not find it anywhere in KDE code or documentation. SLP is another service discovery protocol used primarily by Novell. It has nothing to do with DNS-SD or Zeroconf.</p>
 
 
<p><strong>What can Zeroconf offer for the end user?</strong></p>
 
<p>The goal of Zeroconf is making things simpler by removing as much network configuration as possible. KDE uses one of Zeroconf's parts - service discovery. How do users benefit from that? Lets say you have several particularly good songs and you want your friend (using the same local network) to listen to them. Without a service discovery protocol, after starting the server, you have to tell them the IP address and port. With Zeroconf they don't need to worry about that because your songs will just show up in Konqueror and Amarok's file browser. Another similar case is network gaming on a LAN - it is much simpler to select a game from the list than finding and entering a server's address and port number.</p>
 
 
<p><strong>What is the status of Zeroconf support in KDE 3.4?</strong></p>
 
<p>It is a new feature in 3.4 so support is not extensive, but there are some applications that use it - for example games like lskat, kbattleship and kwin4 (a connect four game, not related to our window manager). The 'Public File Server' kicker applet now announces its presence on the network so it can be found by, for example, zeroconf:/ ioslave. This is a kind of universal browser for all published services on the network. Apart from http it also supports ftp, nfs, ssh, vnc and more. One caveat: it will not show you every ftp server on the LAN, but only those broadcasted using Zeroconf. For now I think only MacOSX announces most of its services by default. Another example is remote desktop - krfb and krdc added support for DNS-SD (in addition to existing SLP) for discovering shared desktops. There are also external applications supporting Zeroconf such as <a href="http://www.kde-apps.org/content/show.php?content=19830">Mateedit</a>, a multi-user text editor that allows concurrent editing of one file by several people at once. It uses Zeroconf to announce and locate editing sessions. There is also a daap:/ protocol handler in kdeplayground-multimedia for browsing iTunes shares. I hope more of them will come eventually.</p>
 
 
<p><strong>How does the support compare to other platforms with Zeroconf such as MacOS or Gnome?</strong></p>
 
<p>Apple began integrating Zeroconf into their system much earlier than us. They are also original authors of DNS-SD and multicast DNS specifications. Support in MacOSX is very comprehensive - almost every network application includes it. Because it is part of the platform lots of 3rd party developers started to use it to make their applications easier to use. Gnome also started before KDE - about half a year earlier in their 2.8 release. It was not surprising that in the beginning support was very sketchy - basically only Nautilus being able to discover WebDAV and FTP servers. More unexpected is that it has not progressed too much since initial implementation - support was added to one game (Nibbles) and GnomeMeeting. Apparently GNOME developers want to finish a more Free replacement for Howl daemon called Avahi before making any serious effort to further enhance Zeroconf support. So KDE is somewhere in the middle - already further than GNOME but there is still work to do before it will be on the same level as MacOSX in terms of Zeroconf support. </p>
 
<p><strong>Do you think Windows will ever support the standard?</strong></p>
 
<p>To some extent, yes. There are already several implementation for Windows, including 'official' one made by Apple. It is used for example by iTunes. But I don't see many Windows applications depending on it - DNS-SD support is available only as 3rd party add-ons and developer can never be sure if it will be available. Fact that Microsoft pushes its own service discovery protocol (uPNP) also does not help.</p>
 
 
<p><strong>What future plans to you have to expand the support?</strong></p>
 
<p>The current design is centered around DNS-SD, but there are also other protocols like SLP or even good old port scanning (used by our lan:/ ioslave). The plan is to have multiple backend modules and combine information gathered using all protocols to provide a more complete list of services to the user. This will also will help solving second big problem: it is hard to use DNS-SD over internet, because current way ('wide area') requires a DNS server configured to accept dynamic updates - which is not common. The user has to configure DNS server name, domain, hostname and key (used for authentication with DNS server) - it can hardly be called zero-configuration. Instead I want to link service discovery with instant messaging. It will work in a similar way to DNS-SD on local network, but with IM connection acting as a communication medium. When publishing a service the user will select recipients from a contact list. Service description will be sent using an out-of-band channel of IM protocol if one exists. If not, then probably encoded text messages and some kind of filter will be needed. Then it will be received, processed and added to discovered services list for applications to use. </p>
 
<p><strong>Technically speaking how does Zeroconf work it's magic and what does a developer have to do to implement it?</strong></p>
 
<p>Zeroconf uses old good DNS to perform new tricks. Instead of a centralised server every host on local network runs its own mini-server that communicates with others using multicast. In order to resolve a name a question is sent to all servers on the network. First one that knows answer responds. Major advantage is that no configuration is necessary - just connect the computer and it instantly becomes part of multicast DNS system. Service discovery also uses DNS. Every service is identified by domain name (for example "My files._ftp._tcp.local.") with an SRV record pointing to hostname and port number. It can also have TXT record with additional attributes. But for developers it is more important to know how to use these services in their of applications. For publishing: create a PublicService object giving its name, type and port, then publish it. That is all, exactly two lines of code. Searching for services is almost as simple - create a ServiceBrowser, connect a signal that will be emitted when something is found and run the browser. If you want to have your services shown in Konqueror (or file dialog) a very simple text file with service description is needed. Several examples can be found in KDEDIR/share/apps/zeroconf.</p>
 
 
<p><strong>What are the required server components for ZeroConf to work, and how difficult are they to install and configure?</strong></p>
 
<p>The sole requirement is Apple's mDNSResponder package. To prevent frequent confusion: it is Apple's mDNSresponder, do not use the Debian package with the name because it actually contains Howl.  KDE is the first big project to use Apple's mDNSResponder outside MacOS X so it have not been packaged by most distributions yet and installation from sources will be necessary in many cases. It is really easy: 'make os=linux && make os=linux install' and you are done. Remember that mDNSResponder has to be installed before compilation of kdelibs. If you have problems, look at <a href="http://wiki.kde.org/tiki-index.php?page=Zeroconf+in+KDE">the KDE Zeroconf wiki page</a> for solutions to several of the most frequent issues. You can also use a <href="http://developer.kde.org/build/konstruct/">Konstruct</a> which automatically compiles and installs KDE with all dependencies.</p>
 
<p><strong>KDE uses the mDNSresponder library from Apple, what are the licencing issues involved with this and what's the advantage over the (mostly GPL) libhowl?</strong></p>
 
<p>mDNSResponder consists of two main components - daemon doing all hard job (licensed under Apple Public Source Licence 2) and client library (BSD licence). APSL is considered free software license by FSF but not, for example, by Debian and Red Hat. It is also not GPL compatible and because in older releases also client library was also covered by APSL2, KDE and most open source project could not use it. Recently it was changed to BSD and now it is exactly the same as Howl's license. Apple's implementation has several technical advantages over Howl: better support for IPv6, cleaner and easier to use API and support for 'wide-area' mode using normal DNS server to work with big networks or Internet. There is also a third Zeroconf implementation called Avahi, that has license acceptable even to Debian, but it is too immature to use it currently.</p>
 
<p><strong>Do any distributions come with KDE Zeroconf support already?</strong></p>
 
<p>Yes, newest versions of several distributions started supporting it. First was as usual Gentoo - you need to add 'zeroconf' to USE flags in order to enable it. Also users of recently released SuSE 9.3 and ArkLinux 2005.1 can enjoy working Zeroconf out of the box.</p>






