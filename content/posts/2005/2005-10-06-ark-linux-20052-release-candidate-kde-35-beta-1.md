---
title: "Ark Linux 2005.2 Release Candidate with KDE 3.5 Beta 1"
date:    2005-10-06
authors:
  - "brosenkraenzer"
slug:    ark-linux-20052-release-candidate-kde-35-beta-1
comments:
  - subject: "Tried it, here the good and bad:"
    date: 2005-10-09
    body: "First the less good stuff:\n\n* Somehow the installer would not allow me to continue without indicating a mountpoint for all of my 8 partitions\n\n* The installer would also not allow 2 swaps (complaining about 2 identical mountpoints) It would also not let me remove one of the swap mountpoints. I had to fire up the built-in qtparted and format my second swap as something else before I could continue\n\n* It nuked my existing grub in the mbr, and replaced it by a non-working one\n\n* I chose en_UK as language in the installer but don't have had that choice for kde in kpersonalizer.\n\n* Somehow kpersonalizer was half\n\n* Networking did not work, my chip is non-exotic (8139too module), launching that network-config tool eventually made the interface up and fetched an ip from my dhcp server\n\n* I never was asked for setting the root password, nor did I get an explanation why it would be unnecessary\n\n* The installer failed to set up my screen correctly (19\" CRT), I got 1024x768 at 75Hz\n\n* The ampersand (at) key does not work, even though I set up my keyboard as usual (fr_CH), and it is correct im xorg.conf, and keyboard layouts are disabled in kde\n\n\nThen the good stuff:\n\n* I really seem to like KDE 3.5 and can't wait for it ! :-)\n"
    author: "Arklinux Testpilot"
  - subject: "Re: Tried it, here the good and bad:"
    date: 2005-10-09
    body: "Hi,  \nsome explanations:  \n  \n> Somehow the installer would not allow me to continue without\n> indicating a mountpoint for all of my 8 partitions\n  \nIt wasn't really designed for systems with hundreds of partitions ;) But I've  \nfixed this in SVN --> will be fixed in the next release.  \n  \n> The installer would also not allow 2 swaps (complaining about 2\n> identical mountpoints) It would also not let me remove one of the swap\n> mountpoints. I had to fire up the built-in qtparted and format my second\n> swap as something else before I could continue\n  \nSame as for the first item  \n  \n> It nuked my existing grub in the mbr, and replaced it by a\n> non-working one\n  \nAny idea why it didn't work for you? It's clear that we overwrite the mbr,  \nbut so far we didn't get any reports of our grub not working.  \nMaybe the grub.conf generator somehow got confused by the many  \npartitions; do you have a copy of the grub.conf it generated?  \n   \n> I chose en_UK as language in the installer but don't have had that\n> choice for kde in kpersonalizer.\n\nStrange, works here...  \n\n> Somehow kpersonalizer was half\n   \nAre you sure you didn't click \"Finish\" in between? It's definitely complete on  \nall boxes I've tried.  \n  \n> Networking did not work, my chip is non-exotic (8139too module),\n> launching that network-config tool eventually made the interface up and\n> fetched an ip from my dhcp server\n  \nThe DHCP client for unconfigured network cards times out after 1 second  \nin order to keep bootup fast for non-networked boxes and boxes on  \nnetworks without DHCP servers.  \nApparently your DHCP server is too slow.  \nWe're thinking about increasing this timeout to 2 seconds because we've  \nhad several reports of this.  \n   \n> I never was asked for setting the root password, nor did I get an\n> explanation why it would be unnecessary\n  \nThat's a feature. Please see the FAQ entry at http://www.arklinux.org/faqman/index.php?op=view&t=6\n  \n> The installer failed to set up my screen correctly (19\" CRT), I got\n> 1024x768 at 75Hz\n\nYes, currently it sets up every screen for 1024x768 because too many displays send completely wrong DDC/EDID information. We're still looking for a reliable way to tell if the EDID information is valid.\n\n   \n> The ampersand (at) key does not work, even though I set up my keyboard\n> as usual (fr_CH), and it is correct in xorg.conf, and keyboard layouts are\n> disabled in kde\n\nThat sounds like a bug in the xorg keytables then... Where should it be? I only have de_CH, de_DE and us keyboards to test with...\n"
    author: "bero"
  - subject: "Re: Tried it, here the good and bad:"
    date: 2005-10-09
    body: ">> I chose en_UK as language in the installer but don't have had that\n>> choice for kde in kpersonalizer.\n \n> Strange, works here... \n\n\n> * Somehow kpersonalizer was half\n\nForget about that 1 ... ;-)\n\nIn kpersonalizer, I eventually chose en_US, en_UK was definately not available. I installed only from the 1st cd, maybe that's a reason...\n\n\nFor grub, the generated grub.conf looks like this:\n\n<pre>\ndefault=0\ntimeout=10\nsplashimage=(hd0,0)/boot/grub/splash.xpm.gz\ntitle Ark Linux H2O (2005.2-rc3) rc3\n        root (hd0,0)\n# Adding ,ypan or ,ywrap after mtrr in the line below will speed up\n# text mode quite a bit -- unfortunately it doesn't work at all on some\n# old hardware.\n        kernel /boot/vmlinuz-2.6.14-0.rc2.1ark ro root=/dev/hda6 vga=791 video=vesa:pmipal,mtrr splash=silent  quiet\n        initrd /boot/initrd-2.6.14-0.rc2.1ark.img\ngrub.conf (END)\n</pre>\n\n\nThe problem is (hd0,0), it should have been (hd0,5) in my case.\nThe kernel command line has been generated correctly.\n\nIt would be nice if it would be possible to write grub into the first sector\nof the root partition... :-)\n"
    author: "Arklinux Testpilot"
  - subject: "Re: Tried it, here the good and bad:"
    date: 2005-10-09
    body: "> In kpersonalizer, I eventually chose en_US, en_UK was definately not\n> available. I installed only from the 1st cd, maybe that's a reason...\n\nRight... Now that you mention it we removed the kde-i18n-British package from the main CD for diskspace reasons (hardly as important as the other kde-i18n packages because anyone who can speak en_GB can speak en_US and vice versa ;) ).\n\n> The problem is (hd0,0), it should have been (hd0,5) in my case.\n\nHmm, and (hd0,0) is a / partition of another Linux distro, right?\nThe installer figures out the location of kernel images etc. by running\n\nROOT=`echo \"find /boot/grub/stage1\" |/sbin/grub --batch |/bin/grep -A1 'find /boot' |/usr/bin/tail -n1 |/bin/sed -e \"s,^ *,,\"`\n\nWhich will, of course, find any partition with /boot/grub/stage1\nWe're doing it this way because using a different way, it's hard to figure out the right ID on mixed IDE/SATA/SCSI systems.\nGuess looking for /boot/kernel-* (with * being our kernel version) instead of just /boot would work better -- changing that in the next version.\n\nWe'll also add the option to install grub into the partition rather than the MBR (though only in expert mode). 2005.2 is the first version with the expert mode -- that's why this simply hasn't mattered before.\n"
    author: "bero"
  - subject: "Re: Tried it, here the good and bad:"
    date: 2005-10-10
    body: ">> In kpersonalizer, I eventually chose en_US, en_UK was definately not\n>> available. I installed only from the 1st cd, maybe that's a reason...\n\n>Right... Now that you mention it we removed the kde-i18n-British package from the >main CD for diskspace reasons (hardly as important as the other kde-i18n packages >because anyone who can speak en_GB can speak en_US and vice versa ;) ).\n\nGranted :-)\n\n> The problem is (hd0,0), it should have been (hd0,5) in my case.\n\nHmm, and (hd0,0) is a / partition of another Linux distro, right?\n\nyes, it is.\n\nThanks for your investigation, I bet the final Arklinux will rock!\n\nregards"
    author: "Arklinux Testpilot"
  - subject: "Re: Tried it, here the good and bad:"
    date: 2005-10-09
    body: "If by \"en_UK\" you mean British English, then better try to find \"en_GB\", which is the right code of the language.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
A release candidate of <a href="http://www.arklinux.org/article.php?story=20051006095431564">Ark Linux</a> 2005.2 has been released, featuring <a href="http://dot.kde.org/1127343716/">KDE 3.5 Beta 1</a>.  <a href="http://www.arklinux.org">Ark Linux</a> is a very KDE centric desktop Linux distribution, aimed at making Linux easily usable to everyone while remaining technically sane.  Aside from the move to KDE 3.5, this release features even more KDE integration including OpenOffice.org KAddressBook integration, and the usual round of speedups and bugfixes.




<!--break-->
<p>The Ark Linux team are planning to release 2005.2 final at the same time as KDE 3.5 is released.</p>

<p>We are also looking for some additional developers to help finishing some related projects - if you are interested, please email us or join #arklinux on irc.freenode.net</p>




