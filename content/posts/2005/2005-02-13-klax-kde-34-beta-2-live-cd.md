---
title: "\"Klax\" KDE 3.4 Beta 2 Live-CD"
date:    2005-02-13
authors:
  - "binner"
slug:    klax-kde-34-beta-2-live-cd
comments:
  - subject: "Size?"
    date: 2005-02-13
    body: "Ordinary Slax has all the important bits of kde and is only 185mb. What takes up so much space?"
    author: "mikeyd"
  - subject: "Re: Size?"
    date: 2005-02-13
    body: "The \"non-important\" bits of KDE."
    author: "binner"
  - subject: "Re: Size?"
    date: 2005-02-13
    body: "The \"extra\" bits of KDE"
    author: "kdt"
  - subject: "Re: Size?"
    date: 2005-02-16
    body: "The -g Option?"
    author: "Stefan Heimers"
  - subject: "Cool!"
    date: 2005-02-13
    body: "Thanks for the livecd, it was much needed!\n\nAfter playing with it for some time, I saw that some issues havn't yet fixed in beta2:\n\n- default system colors (i.e., erasing ~/.kde*) are not the Plastik one, they're a hybrid of old Keramik and Plastik which look quite awful!\n- kmail icons!! i find hard to distinguish quickly between receive, reply and forward icons.. maybe colorizing differently the little circles in them would help?\n- the default file open dialog has a very odd default size\n- authentication using urls like http://username:password@www.site.com doesn't work when username is an email address\n- ark: when i select two files in konqueror, and then right click on one of them and choose 'compress'->'gzippeed tar archive' I expect I'll get a single .tar.gz with both of the files, maybe with a fake default name. Works as expected if I choose 'add to archive'."
    author: "Anonymous"
  - subject: "Re: Cool!"
    date: 2005-02-13
    body: "> authentication using urls like http://username:password@www.site.com doesn't work when \n> username is an email address\n\nI don't think verbatim '@' signs are at all legal in the username part of a URL.\nHave you tried to url-encode the '@'?  For example:  \n\nhttp://username%40host.com:password@www.site.com\n\n"
    author: "cm"
  - subject: "Re: Cool!"
    date: 2005-02-13
    body: "> - kmail icons!! i find hard to distinguish quickly between receive, reply and forward icons.. maybe colorizing differently the little circles in them would help?\n\nWell, I found this bug (*) quite strange for KDE (since it's implementation was refused, it might have been missunderstood when it was reported).\nIt addresses the possibility of adding a new view-style for the toolbars combining \"text on the right\" and \"icons only\" styles into \"selective text on the right\". This would definitely increase visibility of \"New\" or \"Send/Receive\" buttons (in Kmail) & other buttons in konqueror for example.\n\n* http://bugs.kde.org/show_bug.cgi?id=75970"
    author: "Mircea Bardac"
  - subject: "TNX"
    date: 2005-02-13
    body: "thanks!\n\n:-x\n\n_cies.\n"
    author: "cies"
  - subject: "Just downloaded"
    date: 2005-02-13
    body: "TY for the work.\n\nI just used a USA mirror and got very fast DL speed.\n\nNote: This live CD seems to work just fine in VMWare, to easily boot and compare features to other distros side by side.\n\n"
    author: "frank dux"
  - subject: "thanx, but..."
    date: 2005-02-13
    body: "... some tgz for native Slackware 10.1 are welcome :-)\n\nI use your Slackware packages all the times, so KDE is now really choppy and slow if I try it on a Live-CD;)\n\nBtw, Kmail new feature \"Display Smiley\" doesn't work with Live-CD. Is this only because it's a Live-CD or is this an error??\n"
    author: "anonymous"
  - subject: "Re: thanx, but..."
    date: 2005-02-13
    body: "> ... some tgz for native Slackware 10.1 are welcome :-)\n\nI heard from their packager that he intends to create them this weekend.\n\n> I use your Slackware packages all the times\n\nI don't produce the contrib/Slackware packages. \n\n> Is this only because it's a Live-CD or is this an error??\n\nError. Such errors are not arising because it's a Live-CD."
    author: "binner"
  - subject: "Re: thanx, but..."
    date: 2005-02-13
    body: "> I heard from their packager that he intends to create them this weekend.\n\nOh, thanks for the info!\n\n> Error. Such errors are not arising because it's a Live-CD.\n\nOk, thanks again. I mainly tried this Live-CD because of this \"feature\" but don't spend time to look at bugzilla now\n"
    author: "anonymous"
  - subject: "Re: thanx, but..."
    date: 2005-02-14
    body: "That's true. I told Stephan that I will provide Slackware 10.0 packages. But I couldn't this week-end. I moved and my computer still packed. So wait for the next release.\nSorry"
    author: "JC"
  - subject: ":( I wonder if I can use binaries from Klax?"
    date: 2005-02-14
    body: "Sorry to hear that... I tried fruitlessly to compile it over the weekend myself, only to to have multiple compile errors (pth, gpg, etc.) And that's on \"slapt-get --dist-upgrade\"ed Slack 10.\n\nI was looking forward to someone else posting the binaries so I can cram the bugs in before the 3.4 comes out.\n\nI wander if I can use binaries from Klax? It's simply copying the /opt/kde right?\n\nAlso. Is basKet on the Klax? That is pretty much the only app I miss here on 3.4b1 (also doesn't compile for me)"
    author: "Daniel"
  - subject: "Re: :( I wonder if I can use binaries from Klax?"
    date: 2005-02-14
    body: "This sounds like that your system is rather borked."
    author: "Anonymous"
  - subject: "Re: thanx, but..."
    date: 2005-02-14
    body: "> Kmail new feature \"Display Smiley\" doesn't work with Live-CD\n\nI just saw it the first time working with CVS HEAD today."
    author: "Anonymous"
  - subject: "Re: thanx, but..."
    date: 2005-02-15
    body: "> ... some tgz for native Slackware 10.1 are welcome :-)\n\nOk, seems Jean-Christophe will not create KDE 3.4 Beta 2 packages. I uploaded my less than half-official packages as used on Klax to http://ktown.kde.org/~binner/klax/10.1/ - the main packages should be fine, the dependencies packages are all missing descriptions/installation scripts and install to /usr."
    author: "binner"
  - subject: "Klax SCREENSHOTS"
    date: 2005-02-14
    body: "http://shots.osdir.com/slideshows/slideshow.php?release=241&slide=3"
    author: "chris"
  - subject: "Very Cool!"
    date: 2005-02-14
    body: "Very cool stuff coming into KDE.  The aesthetic changes are enough to upgrade!  It was my first time experience with X.org, and those shadows and transulcenly really are nice.  Now im waiting for the full release so i can use zeroconf, and other \"under the hood\" improvements. \n\nA note: Opening a profile in konqueror such as \"simple browser\" doesnt work...however the command 'konqueror --profile simplebrowser' does...very strange... bugs.kde.org here i come!\n\nAnother *off-topic note : A screenshot of the new linspire (5.0) showed up on slashdot, and with 5.0 comes the new everaldo-styled theme! \n\nhttp://adn.bmdhacks.com/desktopsummit/images/lindows.jpg\n\nYou can't make them all out, but it seems really nice\n\n-Sam"
    author: "Samuel Stephen Weber"
  - subject: "it's FAST!!!"
    date: 2005-02-14
    body: "wow, how it's fast!!! congratulations one more time KDE team!\n\nand it's still beta!\n\nI cant wait for the final release; this is what I like in KDE (most opensource), the next version is always lighther and fast than previous one, unlike that other OS :P.\n\nSaulo"
    author: "saulo"
  - subject: "patched Qt 3.3.4"
    date: 2005-02-15
    body: "How is Qt patched? what for?\n\nalso, is this based on the Linux-live 5.x scripts (kernel-2.6.10 + unionfs) or the old 4.2.x scripts? "
    author: "Damjan"
  - subject: "Re: patched Qt 3.3.4"
    date: 2005-02-15
    body: "> How is Qt patched? what for?\n\nIt has the patches of qt-copy/patches applied for the best user experience.\n\n> is this based on the Linux-live 5.x scripts (kernel-2.6.10 + unionfs) or the old 4.2.x scripts\n\nIt has Linux 2.4.29, so Linux Live Scripts 4.2.4. Tomas Matejicek wrote me afterwards btw that \"KDE doesn't work with unionfs and 2.6 kernel\"."
    author: "binner"
  - subject: "good work!"
    date: 2005-02-15
    body: "really really cool work! KDE 3.4 seems to be in good shape. hmmm, in filebrowsing mode you don't have all these buttons for the view, its limited to three. good work.\n\nAnd I love Kontact's new features, and the taskbar... really good... But I had some problems with the clock on the taskbar. I set it to analogue, and turn on anti-aliasing - whoops, messes up the whole clock!\n\nBut I love the speed and system:/ and and and all other features and refinements.\n\nthumbs up for ALL KDE developers!"
    author: "superstoned"
  - subject: "Keyboard does not work. -  Eye candy is great !"
    date: 2005-02-19
    body: "The CD booted fine, however once I got it into KDE, my keyboard stoped responding. Strangely it worked fine at the conole.\n\nWhat I saw looked great, but obvoulsly, I was not able to try out much without using a keyboard.\n\nI am using a microsoft Internet keyboard (USB) with a similar MS mouse. Shuttle SN45G system, nVidia nforce 2 chipset, Radeon descrete Graphics card.\n\nAny ideas?"
    author: "David Pottage"
  - subject: "Re: Keyboard does not work. -  Eye candy is great "
    date: 2005-03-10
    body: "keyboard: that is SLAX/KLAX problem, it thinks it's a mouse :)\nBoot the LiveCD with \"klax mousedev=/dev/mouse\" boot option,\nit should work then.\n\nNew version of SLAX (and maybe KLAX if someone will update it) won't have this problem.\n\nRead more: http://slax.linux-live.org/todo.php"
    author: "Tomas Matejicek"
---
&quot;Klax&quot; is an i486 GNU/Linux Live-CD based on Slackware 10.1 with a patched Qt 3.3.4 and a complete KDE 3.4 Beta 2. Additionally it also contains KOffice 1.3.5 and k3b 0.11.20. All this within an ISO image of only 373MB. It can be used to give you an impression of the upcoming KDE 3.4, to verify if bugs still exist or discover new ones and to make screenshots. For more information and the download site visit the <a href="http://ktown.kde.org/~binner/klax/">&quot;Klax&quot; Homepage</a>.




<!--break-->
