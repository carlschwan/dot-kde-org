---
title: "People Behind KDE: Kevin Ottens"
date:    2005-08-26
authors:
  - "Fab"
slug:    people-behind-kde-kevin-ottens
comments:
  - subject: "Thanks Kevin for media:/ ioslave !!!"
    date: 2005-08-26
    body: "It is the media:/ and system:/ ioslave which makes kde more user friendly. Thank you for such a great contribution to KDE. \n\nI really like media:/ system:/ ioslaves, but most applications can't open files (file open dialog) and from konqueror file. I believe you'll improve media:/ ioslave in coming kde releases. Please try to make it transparent to the applications by changing media:/cdrom to /mnt/cdrom (mount point) automatically when accessed so that non-kde applications could also work easily with media:/ ioslaves.\n\n\n"
    author: "fast_rizwaan"
  - subject: "Re: Thanks Kevin for media:/ ioslave !!!"
    date: 2005-08-26
    body: "I fully second that. The nice framework is one thing, the compatibility with external world (non-kde applications) is another - as important as the first one!"
    author: "MacBerry"
  - subject: "Re: Thanks Kevin for media:/ ioslave !!!"
    date: 2005-08-28
    body: "It's still sort of an issue with one KDE application - Konsole (or, more properly, bash or zsh or any shell in existance as far as I know).\nIf I'm browsing around on say, fish://blackslaveryparty.org and tap F4, I can't access my ssh session"
    author: "Dongz"
  - subject: "Re: Thanks Kevin for media:/ ioslave !!!"
    date: 2005-08-28
    body: "That feature would indeed be very helpful.  It is not implemented but there is a corresponding wishlist entry that you can vote for:  http://bugs.kde.org/show_bug.cgi?id=62978 \n\n"
    author: "cm"
  - subject: "hal replacement for Unix???"
    date: 2005-08-26
    body: "as you've rightly said, hal is linux only solution, much like alsa for sound. But is there anything which will work across all Unix platforms? this of course does not mean to remove the hal+dbus support from kde.\n\nI tried desktopBSD but i missed media:/cdrom thing there. but on my slackware 10.1 with kernel 2.6.11.7 + Hal 4 + dbus + pmount it feels real simple and easy to use cdrom or usb storage disks.\n\nSo, how are we going to make Unix media:/ ioslave compatible???"
    author: "fast_rizwaan"
  - subject: "Re: hal replacement for Unix???"
    date: 2005-08-26
    body: "Isn't that the job of the Desktop BSD project?"
    author: "Bertram"
  - subject: "Re: hal replacement for Unix???"
    date: 2005-08-26
    body: "media:/ works just fine on FreeBSD."
    author: "Lauri Watts"
---
Known for his work on some of the new ioslaves in KDE.  A man who thinks auto-shaving would be nice usability improvement of himself. We present you the latest People Behind KDE interview with ... <A href="http://people.kde.nl/kevin.html">K&#233;vin Ottens</A>. 


<!--break-->
