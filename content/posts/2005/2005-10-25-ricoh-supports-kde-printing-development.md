---
title: "Ricoh Supports KDE Printing Development"
date:    2005-10-25
authors:
  - "Inorog"
slug:    ricoh-supports-kde-printing-development
comments:
  - subject: "Awesome!"
    date: 2005-10-25
    body: "This is great. Printing support in linux can be a bit... troublesome. Each step makes an important difference. Lets hope that Ricoh continues with its support and perhaps send another printer of a different class :)"
    author: "Vlad Blanton"
  - subject: "Thanks to all!"
    date: 2005-10-25
    body: "Printing on KDE has been painless for me so far.  My thanks to all, and I will continue to use and test it - I'm no developer so this is something I can do. ;)"
    author: "Shaman"
  - subject: "Cool"
    date: 2005-10-25
    body: "Cool! I'll keep this in mind when I buy my next printer - not just because I want to say \"thanks\" to Ricoh, but also because I want to be sure that I get a printer where I'm not limited to windows when I want to make full use of it.\n"
    author: "Shura"
  - subject: "Printer drivers for Linux"
    date: 2005-10-25
    body: "Is there a top or low-end printer manufacturer in the world offer printer drivers for Linux for their medium to top-end printers? \n\nPeople want to switch to Linux because Linux can be made immune to viruses and all kind of malware including adware and popups, and the open source software is day by day getting better, now even better than some proprietary software. Software such as KDE, OpenOffice2, Firefox, Thunderbird, Gimp, Inkscape, Scribus, etc. beginning shine and get everybody's attention. \n\nBut when we try to push Linux into corporates with all these benefits, we find hard, because question they ask is are there printer drivers for Linux for our printers? They want printer drivers from the printer manufacturer, not from any third party who do not even have access to the printer specs.\n\nIf printer drivers can be made for Mac which use CUPS, why not for Linux which use the very same CUPS? Both Linux and Mac OS X are Unix.\n\nThis was the case for display drivers some time back. No drivers from the display card manufacturer. But what happened, Nvidia released a driver suit for all of its chipsets, from low-end to very top-end for Linux without any discrimination. The driver suit is as quality as ones for Windows or Mac. So, what's the situation now, Nvidia is the market leader for Linux. \n\nThe printer manufacturer who pioneer printer drivers for Linux will definitely achieve market leadership in Linux as Nvidia achieved. The Linux community will gratefully promote your printer, as currently we do for Nvidia. \n"
    author: "Sagara Wijetunga"
  - subject: "Re: Printer drivers for Linux"
    date: 2005-10-25
    body: "You can look at http://www.linuxprinting.org/\n\nSome PPD files are directly from manufacturers.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Printer drivers for Linux"
    date: 2005-10-27
    body: "Some printer manufacturers do offer support for their printers on Linux.  However, these are not CUPS drivers but rather GhostScript devices or GhostScript plugins.  These can be used directly if you use LPR (which is not currently maintained).  I presume that with the proper modified PPD file that you can use these with CUPS (with Foo-Matic).\n\nThe problem is that there are some very non-standard printers which are totally dependant on a Windows driver.  These are usually inexpensive printers which IMHO are no bargain since there is no guarantee that you will even be able to use them on other versions of Windows."
    author: "James Richard Tyrer"
  - subject: "Maybe someone should contact HP"
    date: 2005-10-25
    body: "Their printers \"Just Work\"(TM) on linux - I'm impressed to no end - and I really think we could advertise this a bit.\nIf you are installing Linux for someone, it is so much easier to make a good impression if they have a HP printer."
    author: "anders"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-25
    body: "i had problems with the hp officejet series with scanning and faxing - printing was ok though"
    author: "chris"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-26
    body: "did you use hplip, and follow the instructions about how to setup the scanner and printer in hplip?\nMost common problem with psc configurations is that the printer uses the usb-port directly, in stead of via hlip. \nThis causes the usb-port to be unavailable for the scannersoftware"
    author: "rinse"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-26
    body: "i did not know there is a special driver for hp office jets \nperhaps these things could auto-install ???\n\n"
    author: "ch"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-26
    body: "Depends on your distribution.\nWith suse 9.3, my experience is that installing a psc correctly is just a few mouseclicks, when plugged in, suse notices both printer and scanner, and allows you to install both with YaST. \nOne should install the scanner first, and in some occasions the scanner should be selected manually in YaST\nwith suse 9.1, installing the scanner had to be done manually by configuring the driver hpoj (hpoj is another HP-driver for hp all-in-one devices).\nThe benefit of hplip above hpoj is that hplip has a grafical toolbox that can manage most modern HP-printers in one (Qt) interface\n(cleaning nozzels, outlining cartrigdes, status info, etc. etc..)\n\nboth hplip and hpoj are very well documented.\nSo installing is in most cases pretty easy"
    author: "rinse"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-25
    body: "Not all of their printers work well - take the HP LaserJet 1020, I haven't been able to get it to work :( Of course, it's one of those \"windows printers\", so it's hardly surprising..."
    author: "Joergen Ramskov"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-26
    body: "From http://www.linuxprinting.org/\n# July 17, 2005: Added HP LaserJet 1020. \n\nhttp://www.linuxprinting.org/show_printer.cgi?recnum=HP-LaserJet_1020"
    author: "AC"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-26
    body: "Yeah, I know. I have tried getting that to work more than once without luck :(\n\nI also emailed HP and got a reply saying that they didn't support that printer in Linux :("
    author: "Joergen Ramskov"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-11-29
    body: "I have fedora core 3 and I cannot get the damn hp 1020 printer to work either. I tried instructions at http://support.ideainformatica.com/hplj1020/\nand downloaded the driver but after printing (blurry on US Letter) once the printer started blinking and fedora claimed that the USB port was busy. \nEven after rebooting I haven't been able to install this printer.\nI am willing to upgrate to fedora core 4 just so that I can use this printer however I am not sure if it is supported in fedora core 4 either.\ni even tried the hp software that is sourceforge and that did not work either.\nif someone knows a solution to this, please let us know.\nDespite all that I love about linux, I hate the tremendous time waste that goes into taking care of these trivial issues. "
    author: "Les ander"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-11-29
    body: "I got it working recently on my Kubuntu install :)\n\nI just followed this guide: \nhttp://ubuntuforums.org/showpost.php?p=483421&postcount=4\n\nIt's for Ubuntu, but I think there is a resonable chance you should be able to get it to work on fedora too :)"
    author: "Joergen Ramskov"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-12-28
    body: "On the topic of Blurry printing on US Letter, here is a workaround:\n\nhttp://forums.gentoo.org/viewtopic-t-378173.html"
    author: "Jordan"
  - subject: "Re: Maybe someone should contact HP"
    date: 2005-10-25
    body: "Kurt Pfeifle wrote about HP's printing support in his Blog a while back (about half a year ago):\n\n--> \"HP Uses Qt For Its FOSS Printing Software\"  ( http://www.kdedevelopers.org/node/957 )\n--> \"More on HP's support for FOSS...\" ( http://www.kdedevelopers.org/node/960 )\n"
    author: "ac"
  - subject: "1 of 2"
    date: 2005-10-25
    body: "There's 2 reasons I switched from Gnome to KDE way back in the v2 days, 1 was the file dialogs, the other was kprinter.  Both were so far ahead of the competition and ubiquitous across the entire desktop, I couldn't see why anyone would choose to use anything else.  Still believe it too, but they're not the only reasons I stay.  \n\nSo add my thanks to Michael, and Ricoh now gets added to the list of recommended printers alongside HP and Epson.  Now if only Canon would take note...\n\nJohn."
    author: "Odysseus"
  - subject: "Re: 1 of 2"
    date: 2005-10-25
    body: "I'd also like to give a plug to Minolta.  Their 2430DL is a non-postscript network color laser printer that has a quality open source driver for it."
    author: "Joe Kowalski"
  - subject: "Re: 1 of 2"
    date: 2005-10-26
    body: "If you search something like kprinter for Gnome, have a look at gtklp. I tried both kprinter and gtklp and I have to say that gtklp offers for me more options than kprinter - especially with adjusting pages on a multi page print."
    author: "Felix"
  - subject: "KPrinter is great, but..."
    date: 2005-10-25
    body: "Yeah, KPrinter is very powerful, but IMO it's UI is really scary, showing way too much CUPS options that Joe Average is never going to use. Look at the printing management, every context menu show exactly ALL the option that are already in the tool bar (and menu bar, they are all the same)...option that obviously aren't \"in the context\". Or answers about banners in the add printer wizard...how many people use this function to justify it's presence by default in a wizard? An tons of other little glitches that IMO ruins the best graphical frontend for CUPS on *nix."
    author: "Davide Ferrari"
  - subject: "Re: KPrinter is great, but..."
    date: 2005-10-25
    body: "mmmh, sorry for the missing \"s\" here and there :P"
    author: "Davide Ferrari"
  - subject: "Thanks Ricoh!"
    date: 2005-10-25
    body: "Given the fact that all my home and work computers run Linux with KDE, being able to print from them easily is a must. If the cooperation between Ricoh, LinuxPrinting.org, and KDE Print yields good support for Ricoh printers, the next printers I'll buy will probably be made by Ricoh."
    author: "Vlad C."
  - subject: "CUPS 1.2"
    date: 2005-10-26
    body: "Is there a guide to these new features of CUPS 1.2 anywhere?"
    author: "Mitchell Mebane"
  - subject: "Re: CUPS 1.2"
    date: 2005-10-26
    body: "http://www.cups.org/roadmap.php will outline many things."
    author: "Matt T. Proud"
  - subject: "Re: CUPS 1.2"
    date: 2005-10-26
    body: "Yeah, I saw that one. I was hoping for something more like \"This Month in SVN\".\n\nOh, well. Thanks."
    author: "Mitchell Mebane"
  - subject: "Ricoh's driver"
    date: 2005-10-26
    body: "Speaking of Ricoh's driver it'd be great if they released a driver for their SD card reader included in Dell Latitude X1's notebooks... so far it is totally unsupported under linux :o("
    author: "Arthur B."
---
Printer manufacturer <a href="http://www.ricoh-usa.com">Ricoh USA</a>, listening to the energetic advocating of their Linux engineer, has decided to provide Cristian Tibirna of the <a href="http://printing.kde.org">KDE printing development team</a> with a professional <a href="http://www.ricoh-usa.com/products/product_features.asp?pCategoryId=25&pSubCategoryId=21&pProductId=262&pCatName=Printers&pSubCatName=Color+%2F+Black+%26+White&pProductName=CL4000DN&tsn=Ricoh-USA">RICOH CL4000DN colour laser printer</a>.  Thanks to this support the KDE printing development team will be able to do better tests of the new features in <a href="http://www.cups.org">CUPS</a> 1.2 and extend the degree of support in KDE Print for professional printing features which currently lack support by Free Software.





<!--break-->
<p>Ricoh's Linux engineer and driver developer George Liu said "<em>What we want to do is support Linux printing, and KDE Print is the most successful printing environment.</em>"  George has also initiated and advocated the contribution of a number of PPD drivers to <a href="http://www.linuxprinting.org">LinuxPrinting.org</a>.</p>

<p>It is a duty of honour to underline that the vast majority of the KDE Print 
work was done by <a href="http://people.kde.org/michaelg.html">Michael Goffioul</a>, the creator of the full suite of KDE Print 
technologies. Michael passed maintainership to me a few months ago, yet he is 
still actively helping with my understanding of the code and with debugging. Our unreserved thanks go to George and Ricoh for their generosity. Many thanks to Michael for the tremendous work he did in past years.</p>




