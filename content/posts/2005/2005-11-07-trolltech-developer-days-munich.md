---
title: "Trolltech Developer Days in Munich"
date:    2005-11-07
authors:
  - "mthaler"
slug:    trolltech-developer-days-munich
comments:
  - subject: "Thanks"
    date: 2005-11-07
    body: "Thanks for all the information for those who couldn't attend!\nIt's great to hear that trolltech grows at such a good rate. The future with Qt 4.1 and KDE 4 looks really great.\n\nCU Dom"
    author: "Dominik Seichter"
  - subject: "At Last, Some Actual News"
    date: 2005-11-07
    body: "And that context search mock-up looked quite interesting."
    author: "Segedunum"
  - subject: "Re: At Last, Some Actual News"
    date: 2005-11-07
    body: "Interesting read. I still hope for some continuity between KDE3 and KDE4 - so that trained people still get the less intutive but in the end faster interfacing stuff like doubleclick instead of mouse-over and so on.\n\nThough design decisions will be made I guess. We will see."
    author: "Chris"
  - subject: "Very interesting mock-up given Aaron's latest..."
    date: 2005-11-07
    body: "Aaron just today on his blog, \"... file managers are an anachronism.\"\n\nI've heard rumors that some TT devs are brainstorming possible file manager replacements and with this mock-up and Aaron's quote, I think whatever comes out will be quite interesting.\n\nI like the idea of working _around_ the endless 'single click' VS 'double click' debate.   If I had to guess, I think that is the true anachronism.  We're limiting our UI choices based on old technology.  Qt4 is going to offer us sooooo much more :)"
    author: "manyoso"
  - subject: "Re: Very interesting mock-up given Aaron's latest..."
    date: 2005-11-07
    body: "Although I somewhat agree with the idea that file-managers are an anachronism, I'm not all for a total revamp.\n\nOn the one hand, I think calling file-managers an anachronism is kinda like saying manual screwdrivers are an anachronism, just because most of the time it's easier to use an electric screwdriver. However, trying to use an electric screwdriver in some situations will result in badly stripped screws and a lot of unnecessary hassle.\n\nLikewise with file-managers. I'd love to have the revved up electric-screwdriver version of Konqueror, where I have to do one-tenth of the work and get all the results. At the same time, I definitely want to be able to use the same old I-do-all-the-work-and-have-far-better-control version of the file-manager available.\n\nI hope that in the redesign of the system, this is kept in mind, and by the mockup it seems that it most likely was. I hope that the \"Ask A Question\" bar is done like the \"Location\" bar currently is: If I just type in a lot of words, it'll do the search I need, but if I put in \"~/personal/writing/\" it'll take me straight to the directory I told it to.\n\nI'm also somewhat concerned by the incredibly cool idea of mouse-over-menus. On the one hand, it's a very elegant solution. On the other hand, it results in a significant interface difference between Icon/Tile views and List/Detail/Tree views, or Icon view at a very small size, as those more reduced views do not have space for a mouse-over menu. I couldn't abide having my icons as large as they are in the example (I have too much stuff in most directories to sacrifice that much screen realestate), so there could be issues with my using this system as it is displayed.\n\nOf course, there are top-notch developers working on this, so I'm sure they've considered these issues."
    author: "jameth"
  - subject: "Qt/Coco"
    date: 2005-11-07
    body: "I like the sound of Qt/Coco development.  X11 is a dinosaur in need of replacement BAD.  Windows kicks X11 100 times all around because X11 is a bloated protocol.  Whoever developed X11 should deserve F's in all their computer science and math classes.  Oh yah, X11 was from MIT.  Shows you how stupid MIT people are.\n\nIn any event, I have a few questions for Qt/Coco:\n1)  Will it replace X11 altogether?  (I think it's about time it should!)\n2)  What kind of license will Qt/Coco be under?  (I'm working on a prototype of a faster than desktop protocol, so if Qt/Coco is under a commercial license, then I will further my development.  The more competition the better. )\n3)  Compare to RDP 5.2, is it faster?  If it's not, then it shouldn't be out in the market.\n4)  Compare to Citrix, is it faster?  Again, if it's not, it shouldn't be out in the market.\n5)  Minor stuffs, will it do application specific display?  Will it have multi users connecting to the same screen for presentation purposes?  Will it have ACL to allow 4 users control access and 5 users view access on the SAME desktop?  Will it allow multi session per instance of running Qt/Coco, meaning I have KDE Linux running on a PC, and I can have 2,000 users access it each having their own desktop?\n\nThanks!"
    author: "Yo"
  - subject: "Re: Qt/Coco"
    date: 2005-11-07
    body: "Cool. You seem to have ABSOLUTELY NO IDEA of what X11 is supposed to be. X11 is a network transparent windowing system, for use in high bandwidth networks. This means that wherever you are, you have access to your GUI *on a central server*. X11 is not supposed to be fast. Its supposed to be usable over the network. This means that it is not as fast as it could be, but usable on thin clients which were common a few decades ago, but recent optimizations make it also usable on new thin clients as well as on local hardware.\n\nYeah I know, there is (Free)NX. However, this doesn't qualify in this discussion, because it uses a *lot* of compression/decompression overhead, using a lot of CPU cycles, which can be saved in high bandwidth networks.\n\nYou think all X11 designers deserve F's? Design somthing better for this same purpose and then come back. Troll."
    author: "hoirkman"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: ">> You seem to have ABSOLUTELY NO IDEA \n>> of what X11 is supposed to be.\n---------------------------------------\nCareful! Let's first see if you have much more of an idea about the real problems of X11.\n\n\n>> X11 is a network transparent windowing \n---------------------------------------\nCorrect. Bonus point for you.\n\n\n>> system, for use in high bandwidth networks.\n---------------------------------------\nNot correct. I can show you a reproducible environment, where you get basically all the bandwidth you like (it may be for example the loopback interface), and X11 is still slow over this bandwidth.\n\nThe reason? The reason is the possible latency between two nodes which can not defeated by any amount of bandwidth you may throw at it. That latency is putting the break on many modern applications displaying their GUI over the network onto a remote screen.\n\nThe consequence? The application logic running on one box needs to talk to its own display screen (which is drawing its dialogs, etc.) and the user's keyboard and mouse (which give it commands to follow). This talking is an exchange of messages, many of which require direct responses for the program flow to go on. A message <--> response pair is a \"roundtrip\". A roundtrip is taking time, the time of the link latency to complete.\n\nLink latency adds up. Mozilla/Firefox needs to go through several thousand roundtrips before even its first complete dialog window appears completely drawn on screen. You do not feel toooo much of this with latencies below 1 millisecond (as in a LAN, or on a loopback interface, or on a Unix domain socket). But over the internet you have to pay the toll. Going over a GSM modem, Firefox' thousands of roundtrips for the first window take more than 7 minutes to finish.\n\nThe guilty? Yes, *some* would say it is the fault of X11 being designed as it is, \"provoking\" too many roundtrips. I'd oppose them. X11 is a very efficient protocol in and of itself. It has minimal overhead. It is more what the programmers of the applications, or more exactly, the programmers of the low level toolkits (Gtk and Qt alike) are doing to the X11 protocol and abusing its atomic messaging capabilities for lots of stuff that shouldn't be done at all, and avoided through more intelligence.\n\nNX is ample proof of this. Yes, NX compresses too. But compression alone is not the only reason. (You can try \"ssh -C -X\" over a real internet link, and over a DSL or faster connection, to see how much or how little it helps...).\n\nGoing back to the same GSM modem connection: NX is able to filter out nearly all of those several thousand roundtrips (less than a dozen remain), and strip down the time for drawing the Firefox window from > 7 minutes to about 20 seconds.\n\n\n>> X11 is not supposed to be fast\n---------------------------------------\nFor me, it is. Since X11 can't fullfil that requirement any more, given the talkative nature of modern GUIs and of the toolkits they're building on, I am happy to have NX which adds that required feature back for me.\n\n\n>> Its supposed to be usable over the network.\n---------------------------------------\nExactly. But for me, it is supposed to be usable even over dialup modem links. Even over 250 milliseconds latency links from a hotel room in San Francisco/USA to a Frankfurt/Germany based KDE desktop server.\n\n\n>> (Free)NX (...) uses a *lot* of compression/decompression \n>> overhead, using a lot of CPU cycles\n---------------------------------------\nActually, it uses a lot less of CPU than you would think it does. \n\nThe special NX compression scheme for X11 traffic uses roughly 10% of the CPU cycles as compared to what a plain and generic ZLIB based compression would use for the same traffic. At the same time, it is about 10x more efficient by using a cache that saves when having to transfer identical content twice, and resorts to differential transfers for similar content.\n\n\n>> You think all X11 designers deserve F's?\n---------------------------------------\nNo, *I* don't. I think they deserve A's. (Some people may give them not exactly these grades for some of their *implementations* -- but that is a completely different matter, and I am not competent to be a good judge here.]  ;-)  \n"
    author: "Kurt Pfeifle"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "And if you don't believe Mr. Pfeifle just check out E17 which is one of the very few projects natively making good use of the X Window System."
    author: "ac"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "You think NX is fast?  Ha!  NX is shiet slow compare to RDP 5.2.  Go do several test cases yourself.  I DID, and one reason why my company chose RDP over NX.  We tried to give NX the benefit of the doubt, but at the end RDP 5.2 kicks NX serious ass all around.  This include 25 frames per second ramdon polygons display, Power PCB board design at resolution 1600x1200-16-bit color, etc.  All of this was compared to NX.  It is without a doubt to us RDP 5.2 was a clear winner.   NX has a lot of mouse/keystroke latency and the graphic display is not immediately when we click on a certain area or type on a certain section.  The latency in response has us thinking at times that the system has froze.\n\nIn summary, NX does a lot of compression and optimizatin for X11 but the response from a user's experience is inadequate.  \n\nHaving said that I welcome Qt/Coco and looking forward to reading its technical spec.  Judging by what I'm designing, I'm assuming for them to achieve such great performance, they'll use UDP and parity to not do \"round trip\".  It's a one-way message passing.\n\nAs far as I'm concern X11 is a very poorly design protocol.  What is this about \"network transparent windowing\" ... and to think u know what you're talking about Citrix/RDP 5.2 can do \"network transparent windowsing\" at 50 times the speed.  Remember, Microsoft licensed Citrix for their RDP technology.  \n\nCheers!\n\n"
    author: "Zero"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "Obviously, you do not understand what a network transparent protocol is. Citrix/RDP/VNC only carry remote desktop sessions. I concede that this is what normally you just want to do, but the usefulness of those protocols are implicitly limited to just this kind of application. You can't, for example, open *just one* remote application which I do constantly. Opening a whole remote session in this case only serves one purpose: cluttering your local Desktop.\n\nOTOH, X11 is a great protocol. Kurt already explained why you experience some shortcomings. If you want to know more, I point you to the great series of articles about FreeNX he wrote for Linux Journal.\n\nLast, I won't discuss about speed. I haven't done those tests myself so I will have to trust yours. In any case, use whatever fill better your needs but, please, next time do *informed and constructive* criticism or leave the rest of the world alone.\n\nThanks."
    author: "Me"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "\"You can't, for example, open *just one* remote application which I do constantly.\"  \n\nWrong!\n\nThe two are orthogonals.  Protocol is INDEPENDENT from how the system handles to deliver each application.  I am able to run just Word or just Excel WITHOUT having the WHOLE windows session from RDP.  Ever heard of Windows 2003 Terminal Service?  You can setup Windows 2003 policy so that when a user logs in, he/she only runs ONE application and have that one application be self-contained as a remote application.  In addition, you can login, and run a \"remote application\". :)  \n\nI don't see your point about \"just one remote application\".\n\nMy criticisms justified!  \n\nCheers!\n\n"
    author: "Zero"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "Hmmm. I hope you have a fast and totally reliable network to run Terminal Services. I also hope you have fairly deep pockets as well."
    author: "Segedunum"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "\"You think NX is fast? Ha! NX is shiet slow compare to RDP 5.2\"\n\nRDP?! Oh, hell yer! Not."
    author: "Segedunum"
  - subject: "Re: Qt/Coco"
    date: 2005-11-09
    body: ">>> You think all X11 designers deserve F's?\n>---------------------------------------\n>No, *I* don't. I think they deserve A's.\n\nI disagree: NeWS was less latency sensitive than X, so does X11 designers really deserve their A?\n\nI think it's a bit strange that using X11 is so difficult that the two major toolkits (GTK and Qt) do not manage to use efficiently X11's protocol and that NX must be used to reduce roundtrips..\n\nBTW what is the link betweeb Qt/Coco and NX:\n- None both do the same thing but Qt/Coco is a reinvention?\n- Qt/Coco implement NX inside Qt?\n- Something else?\n\n\n"
    author: "renox"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "I did't find information about Coco but I implemented solution similar to this.\nNo X, no vnc, just Qt. And this solution is extreamly fast.\nYou can just write a server code similar to QT and get client-server application with one Client application.\nhttp://sourceforge.net/projects/glan\n\n"
    author: "Oleg Shalnev"
  - subject: "Re: Qt/Coco"
    date: 2005-11-08
    body: "Hi, can you give me some documentation on how to work this?  My email is zero@nothingnew.biz.\n\nThanks!"
    author: "Zero"
  - subject: "Re: Qt/Coco"
    date: 2005-11-09
    body: "http://glan.sf.net\n\nGlan is a toolkit for developing network GUI applications. It consists of a remote Qt toolkit, a network (client-server) wrapper for Qt classes, and a Qt-based Application Server development platform for creating GUI oriented client-server applications. \nIt allows the developer to just write code using the Qt programming style for the server side and forget about the client side.\n\nFor example. Here is some of SERVER SIDE code\n___\n TransportPacket Packet;\n\n   GFont font;\n   font.setBold(true);\n\n\n   InterfaceLayout=new GGridLayout(this);\n\n   // Create Panel\n   GPalette palette;\n   palette.setColor(GPalette::Background, GColor(5, 100, 154));\n   palette.setColor(GPalette::Foreground, GColor(255,255,255));\n\n   PanelLayout=new GHBoxLayout();\n   PanelLayout->setSpacing(0);\n   PanelLayout->setMargin(0);\n   LogoLabel=new GLabel(this);\n   LogoLabel->setPalette(palette);\n   LogoLabel->setPixmap(GPixmap(QString::fromLocal8Bit(\"./images/KalpaLogo30.png\")));\n   PanelLayout->addWidget(LogoLabel);\n\n   DateLabel=new GLabel(this);\n   DateLabel->setPalette(palette);\n   DateLabel->setText(QDate::currentDate().toString(\"dd.MM.yyyy\"));\n   DateLabel->setFont(font);\n\n   PanelLayout->addWidget(DateLabel);\n\n   InterfaceLayout->addLayout(PanelLayout, 0,0);\n\n\n   // Create Organize Block\n   OrganizerSummaryLayout=new GHBoxLayout();\n   OrganizerSummaryLayout->setSpacing(0);\n\n   CalendarLayout=new GVBoxLayout();\n   CalendarLayout->setSpacing(0);\n_____\nAs You can see it's similar to Qt. \nAfter compiling you have a server. You must communicate with this server via special client. This solution is no X11 like solution, but a way of creating client-server application without difficulties. Of course Client is available for using on all QT platforms.\nIf Qt/Coco is similar to solution like this it will be very interesting to communicate with Coco programmers."
    author: "Oleg Shalnev"
  - subject: "Re: Qt/Coco"
    date: 2007-05-24
    body: "Could you please give me some complete example of server program for Glan. I want to run and see..Thanks in advance!"
    author: "ikbahrian"
  - subject: "Uhhh..."
    date: 2005-11-07
    body: "Interesting choice of search term.  I assume this was intended as a way to lighten the mood during a dense speech?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "but how about that?"
    date: 2005-11-07
    body: "\"KDE will also make use of new X.org features to allow e.g. transparent menus\"\nThat's cool.\nBut the big question is : is it going to use XGL (or whatever is it called now)? and will we get those cool effects stuff Zack Rusin demonstrated at aKademy !? :)\nhttp://vizzzion.org/stuff/xgl_wanking.avi\n\nI don't know about you but these cool effects with Tenor and Plasma ( I love the SVG KJS tools!) are my most-can't-wait-for-it-wanted features  for KDE4.\n\nAnyway, mockup looks amazing and Qt/Coco sounds great.\n\nI'm sure kubuntu with all these features will be the best desktop distro EVER :) (hear me Novel?) and NO, Trolltech is not gonna get bankrupt because Novel didn't picked KDE duh."
    author: "Patcito"
  - subject: "Re: but how about that?"
    date: 2005-11-07
    body: "I dont think there will be anything like XGL ready in the end-of-2006/beginning-of-2007 time frame. XGl or a successor is unfortunately more long-term.\nZack Rusin has written EXA, which is based on plain old 2-D X but makes hardware alpha blending available, which is the one thing missing for what Aaron Seigo is planning."
    author: "MW"
  - subject: "Re: but how about that?"
    date: 2005-11-07
    body: "Following the xorg mailinglist seems to reveal that EXA is far from being\nready, and does not work well with all drivers yet, not at all with some.\n"
    author: "ac"
  - subject: "Re: but how about that?"
    date: 2005-11-07
    body: "I think this new fetures are Composite and Damage extensions.  Which can be used in KDE now,(Shadows, fading, truly transparent windows) but not that much. "
    author: "MaBu"
  - subject: "Re: but how about that?"
    date: 2005-11-08
    body: "\"...will we get those cool effects stuff Zack Rusin demonstrated at aKademy !?\"\n\nYawn. When it works on a non-proprietary video driver, give me a call. In the meantime I'll keep my freedom (and my system maintainability, stability, etc), and be content to live without the eyecandy."
    author: "Brandybuck"
  - subject: "Gosh I hate that Qtopia marketing..."
    date: 2005-11-07
    body: "I really hate how everything Trolltech is doing is acclaimed here\n\nThis is KDE, it is about Free Software development. Neither Qt/Coco nor Qtopia are Free Software and specially the Qtopia vs .NET thing is simply FUD.\n\nPaying TT customers will truely get the source code, Free Software developers not, .NET (Windows Mobile 5.0) has Sandboxing now, Qtopia will have it some time...\nSeriously Windows Mobile 5 is the best Cell Phone Platform out there, you have source code, it is easy to customize and it allows to sandbox applications. None is generally true with Qtopia 2.\nSo at the same time some one praises Qtopia, Windows Mobile should be praised as well. They are equally Non Free...\n\n... gosh ... I hate that propaganda...\n"
    author: "ich"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-07
    body: "My god, chill out dude :)  This is a KDE site and our primary toolkit is Qt, made by Trolltech.  If you like Windows Mobile, good for you!  Now, can you please run around to all the .NET sites praising Windows Mobile and demand that they equally praise Qtopia?\n\nWe'll see you when you get done.  I'd say you have a few years worth of work.\n\nThanks!"
    author: "manyoso"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-07
    body: "Well we the KDE Project use the Free Software and great component called Qt. It is created and maintained by Trolltech. \nI fail to see the point why we should turn the dot into a Trolltech Proprietary Product advertising channel just because we use a Free Software Product Trolltech created.\n\nAnd once they utilize us to spread their marketing FUD it has to stop. It is bad journalism to simply copy marketing stancas from Trolltech."
    author: "ich"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-07
    body: "and, apart from what the ppl above me already said, what do you mean with the OSS developers don't get the source code? they do! in the form of GPL code, i can't see how it can be more free (in terms of the Free Software Foundation-free)...\n\nand this is not the fact with windows mobile. or IS it available under the FREE(TM) GPL license? no, its not, is it? well, then, stop spreading FUD."
    author: "superstoned"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-07
    body: "Well for Qtopia you certainly do not get the latest source code. E.G. Qtopia 2.1.2 PDA had been released in June to paying customers. Using rsync Qtopia you get version 1.7.\nNot to mention that Qtopia Phone is nowhere Free Software.\n\nSo Qtopia is not like Qt - that we all love. And it is okay for Trolltech to release Qtopia the way they want, though we should not simply generate buzz for Qtopia as it is not Free Software but a Proprietary Product."
    author: "ich"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-07
    body: "sorry, i didn't know that. well, i hope this changes (the chance TT releases it under the GPL are at least 1000x the chance m$ does so)..."
    author: "superstoned"
  - subject: "BS!"
    date: 2005-11-07
    body: "From:\nhttp://www.trolltech.com/download/qtopia/index.html\n\nftp://ftp.trolltech.com/qtopia/source/qtopia-free-source-2.1.1.tar.bz2\n\nftp://ftp.trolltech.com/qt/source/qt-embedded-2.3.10-free.tar.gz"
    author: "Patcito"
  - subject: "Re: BS!"
    date: 2005-11-07
    body: "Well Qt2.3.11 and Qtopia 2.1.2 are current. See http://doc.trolltech.com/qtopia2.1/html/release-index.html for when Qtopia2.1.2 was released to paying customers. And now imagine what would happen if Qt4.1 will only be released to paying customers and  Qt4.2 will be the next GPL release...\n\nThat doesn't make Trolltech bad, or Qt4.0.x but it is weird... "
    author: "ich"
  - subject: "Re: BS!"
    date: 2005-11-07
    body: "Qtopia 2.1.2 is a bug fix release and so is Qt2.3.11. Bugfix releases have always been available earlier for paying customers. This was the case for Qt4.0.1, people with a commercial license got it earlier than us and you would know this if you were following the mailing list :)\n\nSo this is a non issue."
    author: "Patcito"
  - subject: "Re: BS!"
    date: 2005-11-08
    body: "well did paying customers get Qt4.0.1 five month+ earlier? If that is the case I really did not follow the mailinglists..."
    author: "ich"
  - subject: "Re: BS!"
    date: 2005-11-08
    body: "as far as I can recall, it wasn\u00b4t longer than 15 days."
    author: "Patcito"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-08
    body: "Wrong. Qtopia is free software. I do not see where to download Windows Mobile 5 source code, so I can change it in anyway I see fit, then release this myself on a device, if I so choose. If you have such a url please, do share it."
    author: "Lorn Potter"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-08
    body: "Errr, Qtopia was released as free software. It hasn't always been that way, but it was definitely done fairly recently."
    author: "Segedunum"
  - subject: "Re: Gosh I hate that Qtopia marketing..."
    date: 2005-11-09
    body: "Actually, Qtopia _has_ always been free software. Except for the early development phase. At version 1.4, Qtopia (than named QPE) was available at sourceforge. That was 4 years ago.\n\ncheers! :)"
    author: "Lorn Potter"
  - subject: "Why Mouse Over Interface is bad"
    date: 2005-11-07
    body: "2007 with WiMax, WLAN, Bluetooth,WUSB integrated on your SoC in your mobile device you cary everywhere running a Free System like Maemo/GNOME (www.maemo.org). You want to communicate with your NX Rootserver from your University, Company, Home. Your handheld device has a touchscreen....\n\nWell you find yourself unable to use KDE from your handheld: Well you have a touchscreen, KDE will be unable to be used with a mouse pointer device and not with a touchscreen...\n\nNow not only KDE is not cross compilable, runnable on your handheld but you can not even connect to it in the future...\n\nConclusion: KDE fights a monopoly on the desktop, instead of creating a mobile strategy now to prevent M$, Symbian to get a new monopoly. In contrast KDE blocks itself out completely from a market where it could shine... At least GNOME took the money from Nokia to make it ready for a Consumer Electronic device (n770)\n"
    author: "ich"
  - subject: "Re: Why Mouse Over Interface is bad"
    date: 2005-11-07
    body: "You never used a touchscreen, did you? Anyway, a touchscreen you can use by letting your finger mimic the mouse pointer. I do not see a fundamental problem here (other than the one every touchscreen shows: precision of of movements if thick fingers on small devices are involved.\n\nI wholeheartedly agree with you that KDE needs to come up with a \"mobile strategy\". \n\nIt completely sucks that there was not one KDE developer found inside this whole productive community which produces lots of wonderful stuff, who could find interest to fix kNX. Not even for money (Kurt once said to me there has even been money offered, and none was interested -- how disappointing!). \n\nkNX truly sucks. KDE is wasting years and years letting this wonderful technology rot in the dustbin. Looks like all of their coders lack any vision about the future of mobility and the technology that makes it happen. They are all busy with their own little kingdoms where they are the maintainer kings, and don't look further than their noses...\n\nMaybe, maybe -- one day somebody steps forward. Given the wonderful architectural groundwork that is there in KDE, it could be a matter of only weeks until we could have a rocking KDE NX client running.\n"
    author: "nx-er"
  - subject: "Re: Why Mouse Over Interface is bad"
    date: 2005-11-07
    body: "To the Touchscreen: all current GUIs are click orientated and a single click vs. mouse over is hard to distunguish. So mouse over is not becoming reality on touch screen devices.\n\nYes KDE's NX Integration is well disappointing, that synchronizing and integration of mobile gadgets is completely missing in KDE is... That we integrate Palm better than Free Software (Maemo, GPE, Opie) is from a Free Software point of view...\n\nNokia invested two years (n man years of money) to make core GNOME technology usable on handhelds. But luckily KDE moves on forward as well by splitting technology up into minor pieces...\n\nLet us hope KDE does not obsolete itself by only focusing on i386 based desktops as of today... KDE4 is not released now, it will be released in the future, it should be ready for future computing... let us hope at least some others share this believe."
    author: "ich"
  - subject: "Re: Why Mouse Over Interface is bad"
    date: 2005-11-07
    body: "i think kde is doing good on the embeded platform, see what they're up to:\nhttp://opie.handhelds.org/cgi-bin/moin.cgi/OpieRoadmap\n\nespecially these project:\nhttp://opie.handhelds.org/cgi-bin/moin.cgi/DocumentsTab\nhttp://opie.handhelds.org/cgi-bin/moin.cgi/OpenTasks\n\n    *\n\n      Qt/Embedded + X11 compatibility\n    *\n\n      Qt4\n    *\n\n      Revamped DocumentsTab\n    *\n\n      CrossRef project + PIM\n    *\n"
    author: "Pat"
  - subject: "thanx"
    date: 2005-11-07
    body: "Michael Thaler: great article! love it ;-)"
    author: "superstoned"
  - subject: "mouse-over interface problems"
    date: 2005-11-07
    body: "As drawn, I don't see how the translucent mouse-over menu is better than a conventional context menu on right mouse button click.\n\nPresumably, \"click to select\" becomes hover > Actions > Select this icon.  And once you've chosen that you've moved your mouse far from the icon, so direct manipulation gets lost.\n\nAfter selecting the icon, does the mouse-over menu not appear so that you can perform drag-and-drop?  Or do you choose hover > Actions > Move this icon?\n\nAlso, the mouse-over menu would seem to interfere with press-and-hold to initiate drag-and-drop.\n\nIt seems hard to adapt this to a touch screen, or a keyboard or direction-pad UI.\n\nHowever, it's good to see interesting new ideas!  We've learned to live with the confusion of a hybrid click to select/click to activate UI, but it's not easy: when I see a new UI element I \"probe it\" with a mixture of wipe-through selection, press-and-hold, and right-mouse button context menu."
    author: "S Page"
  - subject: "Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "Trolltech would best help the spread of Qt by reducing the price of commercial Qt licence by a factor of 10 at least. The current price (cca 3000E per developer!) is really a show stopper for not only small ISV's - look how Novell has chosen Gnome/Gtk as default desktop/toolkit. The main reason is money, off-course.\nReducing the price would IMO lead to a boom of Qt usage, which would more than compensate for the lower price.\n\nTrolltech, are you listening?"
    author: "Krystof"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "afaik, you can choose what functions of Qt you need, so you can have it a lot cheaper. and if you can't afford it (as startup) you can contact them, and they'll give it for a lower price, or free for 1 year or something. and anyway, as developer time costs a lot more than this e3000, you'll even save money by using and paying for Qt - as it is a much more efficient toolkit compared to GTK (the well-known total-costs-of-ownership are imho lower with Qt).\n\nThe main reason Novell choose to go for gnome is just the trolling of some gnome ppl in Novell. just plain politics. when did a big organisation choose something because it was The Right Thing(TM)? forget it, they don't.\n\nanyway, novell still throws more money in KDE/Qt as in Gnome/GTK. they laid off a bunch of Gnome devs and ONE KDE dev. suse is worth much more than Ximian, and suse is profitable - Ximian never is/was/will. there will come a time they see money spend on Gnome is money thrown away. just as sun HP and IBM have seen."
    author: "superstoned"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "You are maybe right about Novell. However, this situation would probably never happen if Qt was licenced under the same licence as Gtk.\n\nIf Qt was really free (as in BSD or at least LGPL) or at least significantly cheaper then Gtk would hardly ever be used in commercial apps and distros."
    author: "Krystof"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "maybe, but then Trolltech wouldn't be able to employ 120 ppl on Qt and KDE..."
    author: "superstoned"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "\"If Qt was really free (as in BSD or at least LGPL) or at least significantly cheaper then Gtk would hardly ever be used in commercial apps and distros.\"\n\nThere's more to it than mere price. Sure they could use GTK. And they would then sacrifice documentation, training, tech-support, portability etc. etc. Yes, Qt costs money. But maybe you get a lot more with Qt than you get with GTK+?\n\nBMW costs more than Lada does, does that mean that BMW is overpriced?"
    author: "Janne"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "I'm a huge BSD license advocate. But claiming the GPL is not \"really free\" is disingenuous.\n\nThis argument against Qt is a smokescreen. No matter what Trolltech does, the GNU/GTK/GNOME community finds some new argument to pollute the mindspace. When Qt was proprietary they said it should be free. When Qt was QPL they said it wasn't GPL compatible. Now that it's GPL they say it's not free enough. The lesson has been learned: the naysayers don't want a solution, they want to complain. Even if Qt were placed into the public domain, with absolutely zero restrictions, people will STILL bitch about it. All you need to do is to look at any Slashdot topic on KDE or Qt and see the silly arguments waiting in the wings.\n\n\nI know several small proprietary firms who have decided on Qt for their development toolkit. I talked with a developer of one a couple of weeks ago, and he said that Qt pays for itself by speeding development. He said that even though .NET is a vast improvement over MFC, coding in Qt is still more efficient."
    author: "Brandybuck"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "Nobody is bitching here. Face the facts: with Gtk you can develop commercial apps for free, with Qt you cannot. You get it?\n\nNobody can suspect the people here to be Gtk fans. And yes, we will be comming to this problem as long as it will persist. We do it because we like Qt and therefore we are bothered because we see the commercial licencing cost as an important disadvantage of Qt versus Gtk.\n\nI agree that Trolltech needs the money to pay the bills and keep Qt on the top. But does the price really need to be that high?\n"
    author: "Krystof"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "The price is not high at all."
    author: "ac"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-09
    body: "\"Face the facts: with Gtk you can develop commercial apps for free, with Qt you cannot.\"\n\nAnd still you see nearly no commercial apps developed with GTK+, in that arena it's vastly outnumbered by Qt. This should tell you that people developing commercial applications are willing to pay for the advantages they get by using Qt, and for the support they don't get with the develop for free alternative.\n\nBesides you should really talk to some commercial Qt customers about how they rate the value for money when buying Qt. And stop listening to the theories about those hypothetical commercial basement developers."
    author: "Morty"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-09
    body: "\"with Gtk you can develop commercial apps for free, with Qt you cannot\"\n\nPerhaps you meant \"proprietary\" instead of \"commercial\"? But in either case, I see far more commercial and proprietary software written in Qt than in GTK+."
    author: "Brandybuck"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-08
    body: "\"The current price (cca 3000E per developer!) is....\"\n\n...about one months salary for a programmer. Is it REALLY unreasonable to invest some money on a quality set of tools? And in corporate-environment, the cost of Qt is peanuts. Yes it is. It's only a fraction of the cost of the employees salary, and the salary is only a fraction of the total-costs the employee causes to the employer. So cost of Qt is a fraction of a fraction :). And besides, there are some tools that can costs over 10.000e!\n\n\"look how Novell has chosen Gnome/Gtk as default desktop/toolkit. The main reason is money, off-course.\"\n\nPlease provide me with a link where Novell said that \"we chose GTK/GNOME because commercial Qt is just too darn expensive\". If you have no link, then you are merely making assumptions."
    author: "Janne"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2005-11-11
    body: "What I meant to say was that if qt was really free (BSD/LGPL) or at least very cheap from the begining, Novell would hardly ever had bought into Ximian and got infected with Miguel Icaza and Co :-)\n\nI thing the same applies to RedHat and Sun. Choosing default toolkit is a strategic decision for years and choosing toolkit which is controlled by a private company was risk for them. People run to Linux to escape the vendor lock-in. Why do the same mistale again?\n\nYes, I know that the full GPL developers don't have to bother about it. But private companies see this differently. What if Trolltech goes mad and increases the prize 10 times? As a commercial developer you're doomed. With Gtk you avoid this risk regardless to how this toolkit sucks."
    author: "Krystof"
  - subject: "Re: Reduce the price of commercial Qt !"
    date: 2006-01-22
    body: "If Trolltech will increase the price 10 times, then Trolltech will be the doomed one. It will lose most of its clients.\nBut lets say, that it will not lose most of its clients and Trolltech will really profit from such move. Then Qt must be worth 10 times more than it costs now. Wow!"
    author: "Er"
  - subject: "Is there going to be a a Qt/WinCE?"
    date: 2005-11-08
    body: "Is there going to be a a Qt/WinCE?\n"
    author: "Pedro Alves"
  - subject: "Java Bindings"
    date: 2005-11-08
    body: "Official Java Bindings would be nice. But an SWT implementation, or better a solution of the licensing issues (GPL vs EPL) regarding SWT, and maybe a good AWT implementation would be much more useful. At least for KDE/Linux users. I know, it is unfortunately a licensing problem and for Trolltech it is difficult to solve, because they want sell their commercial licenses to Java developers too. But for Java developers it makes much more sense to use SWT instead the Qt API in Java directly."
    author: "Bernd Lachner"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "Did you ever use/program SWT? \n\nDid you ever try to create a cross platform GUI with SWT which does not look alien on all platforms?\n\nIf you ever did you would know that Qt has a lot more to offer to Java developers than SWT. \n\nQt/Java is easier to program (much nicer API) and provides a native look and feel on all supported platforms. \n\nActually Qt/Java is solving the major problem of Java for GUI applications. In addition there is a reason why not even eclipse is using SWT.\n\nIt is funny to note though that initially Java was hyped for GUI applets in the early nineties but won the hearts of the developers for server applications."
    author: "Martin Konold"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "> Did you ever use/program SWT?\nYes. I used some. You know Eclipse? Azureus? And yes I programmed SWT applications and at the moment I work on an Eclipse RCP based application.\n\n> Did you ever try to create a cross platform GUI with SWT which does not look \n> alien on all platforms?\nIn my opinion, SWT integrates well in the platform, because it uses the platform widget set where ever possible.\n\n> If you ever did you would know that Qt has a lot more to offer to Java \n> developers than SWT.\nSure. Qt can offer more. But that is not the point. SWT applications can still run without Qt, on Windows, Linux Motif / GTK, MacOS, .... and that ist the advantage. But a Qt based SWT would nice for some reasons:\n\n- SWT and Eclipse RCP Applications would better integrate in a KDE environment.\n- And, maybe more important, eSWT for Qt embedded would be possible. And this make it possible to write Java eSWT applications that can  run for example on Pocket PC, Symbian, Qtopia and maybe Gtk based devices like the Nokia 770. eSWT is already available for Pocket PC and Symbian. For a Gtk based device, eSWT is likely only a matter of time, because SWT for GTK already exists. Ok, eSWT is afaik available in a commercial version from IBM for Qtopia too, but who wants to buy a license to develop or use open source programs?\n\n> Qt/Java is easier to program (much nicer API) and provides a native look and \n> feel on all supported platforms.\nYes, but SWT and eSWT supports more platforms. Nevertheless an official Qt Java Binding from Trolltech would be nice, but in my opinion a SWT version based on Qt would be more useful.\n\n> Actually Qt/Java is solving the major problem of Java for GUI applications. In\n> addition there is a reason why not even eclipse is using SWT.\nEclipse don't use SWT? This is new for me. And I think you should verify this. ;-) SWT is the Toolkit from Eclipse.\n"
    author: "Bernd Lachner"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "> Eclipse don't use SWT? This is new for me.\n\nYes, sorry this was an factual error of me."
    author: "Martin Konold"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "> Did you ever try to create a cross platform GUI with SWT \n> which does not look alien on all platforms?\n\nEclipse is created with SWT and it only looks alien on linux :) (much is solved by gtk-qt, though)\n\nYour problem is that you don't distinguish SWT from Swing. But then, why are you disinforming people?\n\n> In addition there is a reason why not even eclipse is using SWT.\n \nThis is certainly entertaining to read, because Eclipse is precisely _the_ SWT thing.\n\nAnd what do you mean by 'not even Eclipse'? That's like saying that a bad solution has got to be an Eclipse solution :) But Eclipse is the KDE of development tools, it's powerful, flexible and moderately easy to use.\n\n> Qt/Java is easier to program \n\nThere is no Qt/Java as of yet.\n\n> (much nicer API)\n\nYes. That is right, the Qt API is what makes Qt great. I'm a Java developer and I'd choose Qt anytime, if the API would outweigh C++'s aversion towards threads etc.\n\n> and provides a native look and feel on all supported platforms. \n\nSWT also does that, unless you're on Linux (yes, I hate that file dialog that Eclipse pops up :) . as a matter of fact, the GTK file dialog vastly beats the stupidity of reversing the buttons and 'spatializing' the file manager)\n\n> Actually Qt/Java is solving the major problem of Java for GUI applications.\n\nThere is no Qt/Java today so it isn't solving anything now.\n\n> It is funny to note though that initially Java was hyped for GUI applets in > the early nineties but won the hearts of the developers for server \n> applications.\n\nYes. That's beacause the language is great, but Swing sucks. SWT is better, but Qt/Java will probably be even better."
    author: "ac"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "> if the API would outweigh\n\nReplying to myself I apologize for the bad grammar."
    author: "ac"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "Actually, there _is_ a working Qt/Java right now. In kdebindings, and has been there for ages."
    author: "Boudewijn"
  - subject: "SWT-GTK-Qt okay, but SWT-QT not?"
    date: 2005-11-08
    body: "huh? can some licence docter help me, please?\n\ni learned that:\nSWT-Qt is not possilbe because of (GPL for Qt, CPL for SWT) licence issues, SWT-GTK has no problems because GTK is LGPL.\n\nnow i just read that that using gtk-qt (a gtk lib that uses qt to draw widgets) along with Eclipse it is possible to have a nicer looking Eclipse.\n\nso can i conclude that:\nwith a bit of LGPL 'glue' code it is allowed hook SWT up with Qt?\n\n\nan other question:\ncan i use a propietary GTK app with, a GPL licenced, gtk-qt lib?\nthat again would mean that:\nwith a bit of LGPL 'glue' code it is allowed hook my propietary code up with, a GPL licenced, Qt?!\n\n\ni dont get it. please someone, enlighten me.\n\n_cies.\n"
    author: "cies breijs"
  - subject: "Re: SWT-GTK-Qt okay, but SWT-QT not?"
    date: 2005-11-08
    body: "Nice questions. Unfortunately they beat me.\n\nEclipse comes with SUSE 10, which ships gtk-qt enabled by default. Of course, this turns Eclipse into a friendlier thing.\n\n> with a bit of LGPL 'glue' code it is allowed hook my propietary code up \n> with, a GPL licenced, Qt?!\n\nNope. However, Eclipse is not proprietary code.\n\nThe FSF says:\n\n\"The Common Public License is incompatible with the GPL because it has various specific requirements that are not in the GPL.\n For example, it requires certain patent licenses be given that the GPL does not require. (We don't think those patent license requirements are inherently a bad idea, but nonetheless they are incompatible with the GNU GPL.)\"\n\n\n \n"
    author: "ac"
  - subject: "Re: SWT-GTK-Qt okay, but SWT-QT not?"
    date: 2005-11-09
    body: "GPL compability are not really important when it comes to Qt, as it's dual licensed GPL and QPL. The QPL are rather easygoing license, it has two simple requirements. The code is under a open source license and the code are freely available. So the Eclipse's CPL are no problem as long as the code are available. "
    author: "Morty"
  - subject: "Re: SWT-GTK-Qt okay, but SWT-QT not?"
    date: 2007-08-08
    body: "That makes it sound like there is no real reason for there not to be an SWT-QT.\n\nSo, where is it?"
    author: "oatkinson"
  - subject: "Re: SWT-GTK-Qt okay, but SWT-QT not?"
    date: 2007-08-09
    body: "Someone create the myth that it wouldn't be possible, a lot of people believed it and the remaining ones either have already enough to do elsewhere or do not want to invest time into something which is obviously worked against intentionally."
    author: "Kevin Krammer"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "\"maybe a good AWT implementation would be much more useful\"\n\nDon't Trolltech already sell a Qt version of AWT?\n\n\"But an SWT implementation, or better a solution of the licensing issues (GPL vs EPL)\"\n\nJava developments tend to have huge budgets with large numbers of programmers. I don't think toolkit license costs are much of an issue. The flexibility of being able to choose between a Free or Commercial license is a big plus point for Qt and not a negative at all.\n\n\"But for Java developers it makes much more sense to use SWT instead the Qt API in Java directly.\"\n\nPlease can you add some sort of argument to support your assertion. Do you think there is something wonderful about subclassing event listeners as opposed to connecting signals and slots. In what way do you think that clunky way of doing things is a good idea?"
    author: "Richard Dale"
  - subject: "Re: Java Bindings"
    date: 2005-11-08
    body: "> \"maybe a good AWT implementation would be much more useful\"\n> Don't Trolltech already sell a Qt version of AWT?\nAt least for Qtopia/Qt Embedded a AWT version exists. And AFAIK there is also some work done on a free AWT implementation based on Qt. So this \"problem\" might be solved over time.\n\n> \"But an SWT implementation, or better a solution of the licensing issues (GPL \n> vs EPL)\"\n> Java developments tend to have huge budgets with large numbers of programmers.\n> I don't think toolkit license costs are much of an issue. The flexibility of \n> being able to choose between a Free or Commercial license is a big plus point \n> for Qt and not a negative at all.\nYes, I also think the license costs for a commercial project wouldn't be the problem. But, there is no Qt based SWT available because AFAIK the licenses EPL and GPL are not compatible. I also think, that the choice between a free or commercial license for Qt isn't bad. But for SWT and the EPL this AFAIK prevents a Qt based SWT implementation.\n\n> \"But for Java developers it makes much more sense to use SWT instead the Qt \n> API in Java directly.\"\n> \n> Please can you add some sort of argument to support your assertion. Do you \n> think there is something wonderful about subclassing event listeners as \n> opposed to connecting signals and slots. In what way do you think that clunky \n> way of doing things is a good idea?\nI don't think that a Qt Java binding would be bad. But I think a Qt based SWT implementation would be more useful. Please see my arguments in my reply to the other post."
    author: "Bernd Lachner"
  - subject: "Re: Java Bindings"
    date: 2005-11-09
    body: "SWT is unfortunately a no-go because the IBM legal team decided to use a licence that is neither compatible with GPL nor QPL.\n\nWhich is quite hard, but nevertheless manageable.\n\nAs for AWT, the GNU Classpath project is implementing peers based on Qt4\n"
    author: "Kevin Krammer"
  - subject: "Re: Java Bindings"
    date: 2005-11-09
    body: "Well, it is compatible with the QPL, as long as you include the source code."
    author: "ac"
  - subject: "Transparent Menus"
    date: 2005-11-08
    body: "I might have misunderstud, but I already have transparent menus in my KDE 3.4.3\n\nhttp://images.rusail.com/desktop.png\nhttp://images.rusail.com/gjennomsiktig_meny.png"
    author: "Sindre"
  - subject: "Re: Transparent Menus"
    date: 2005-11-08
    body: "If whats under the menu changes the 'transparency' on the menu doesn't update.  Right now the method used is sort of a hack (though it works rather well)."
    author: "Corbin"
  - subject: "mouse-over based interface"
    date: 2005-11-08
    body: "I am not convinced of the mouse-over interface, as from the mockup I don't see how the default operation (open, run, ...) can be found (and triggered) _fast_. Maybe if the menu would only be the half with of the icon, so clicking the icon that is not under the menu triggers the default operation. And if the mouse \"enters\" the icon from the left, the menu is display on the right side and vice-versa.\n\nAnother idea would be to imitate the behaviour of KDE toolbars: if there are additional operations, open a menu if the thing is clicked for a \"long\" time.\n\nSorry for my english.\n"
    author: "MM"
  - subject: "Re: mouse-over based interface"
    date: 2005-11-10
    body: "Agreed that mouse-over doesn't seem to be interesting, disagreeing with using some kind of menu with the left click: what's wrong with right clicking??\n\nWith a mouseover, there is usually a delay between the mouse pointing to an icon and the apparition of the menu: if there is no or a small delay  then it annoys the users when it moves the mouse and it makes an unwanted menu appearing, if there is a large one then it annoys the user because it takes too long to appear..\n\nI agree that double-clicking icons is not good, but my question is still what's wrong with right clicking?\nA single left click to activate the default action, a right click to select another action, KISS, it works!\n"
    author: "renox"
  - subject: "Re: mouse-over based interface"
    date: 2005-11-10
    body: "I don't think that there is anything wrong with right clicking, or another \"special\" action (Apple key), as long as you KNOW you can make a right click (or that special action) to 'do what you want' with the object you are looking at.\n\nOkay, most users know about it, but an unknown percentage of a >6 billion population may have never used a computer. \n\nIf a good solution is found, that on the one hand does not confuse the user nor drives more professional users crazy and on the other hand gives any human in the world who is able to see (and read) an idea what he or she can do, it would be 'one small step' forward, away from a 'silly computer', towards a useful machine for everybody.\n\nRight-clicking of course works. ENIAC did work as well but I though prefer my notebook (at least in every-day life).\n"
    author: "MM"
---
The second annual <a href="http://www.trolltech.com/campaign/devday.html">Trolltech Developer Days</a> took place last month in San Jose, USA and on last week in Munich, Germany.  They featured keynotes and presentations from Eirik Chambe-Eng, Trolltech's co-founder and President; Matthias Ettrich, founder of KDE and Trolltech's Vice President Software Development; Aaron Seigo, KDE Core Contributor and many others.  Read on for a summary of some of the keynotes and presentations given in Munich.

<!--break-->
<p><strong>Eirik Chambe-Eng</strong></p>

<p>The first keynote by Eirik Chambe-Eng, Trolltech's co-founder and president, gave an overview of the company's current situation and future plans:</p>

<p>Trolltech continues to grow at an impressive rate, they increased their staff from 80 to 140 employees in the last twelve months. A second round of fundraising, which brought $6.7 million of disposable money to the company, was recently completed. Trolltech has opened an office in China. And of course the long awaited Qt 4 was released. In the coming twelve months Trolltech will become a professional service organisation. They will develop new products that complement and expand the usage of Qt. A continued focus on making Qt easier to use, faster, leaner and better will be kept and it is expected that <a href="http://www.trolltech.com/products/qtopia/">Qtopia</a> will explode in the phone market.</p>

<p>One of the new products complementing and expanding the usage of Qt will be a thin client architecture called Qt/Coco, named after designer Coco Chanel who said one can never be too rich or too thin.  It is targeted at enterprise users and it will allow server side Qt apps to be deployed on thin, universal clients in an organisation, with the goal to dramatically reduce administration costs. Currently, Qt/Coco is in the prototype stage and it already shows amazing performance (ten times faster than X11). Trolltech is also developing Java-bindings for Qt called Qt/Java, which will allow Java developers to use the Qt API. A prototype will be available in the first quarter of 2006.</p>

<p>Qtopia is another success story for Trolltech. The company currently has 85 customers using Qtopia, from which 30 are mobile phone builders. Linux is the disruptive force in the handset industry and Trolltech's goal is that 100 million handsets are sold using Qtopia by 2008. Qtopia 4, which is currently under development, will be based on Qt 4. Some of its highlights will be a native sandbox for safe execution of programs, an integrated SQL database on every device and advanced graphics due to the new Arthur painting subsystem in Qt4. Some of the advantages of Qtopia over .Net are: customisability of the user interface, lower memory consumption and the availability of the source code.</p>

<p><strong>Matthias Ettrich</strong></p>

<p>The second keynote by Matthias Ettrich, founder of KDE and Trolltech's Vice President of Software Development gave an overview of Qt 4:</p>

<p>The design goals of Qt 4 were to enhance the capabilities, to increase the flexibility and to lay a solid foundation for the future. Mostly Qt 4 turned out as an immediate success. The clean Qt 4 code base allows Trolltech to turn quickly and there are successful and fast ports from Qt 3. Qt 4 has also been quickly adopted in the open source world. However, some users are dissatisfied with the speed of certain painting operations and the qt3to4 porting tool and the Qt3Support library partly failed to deliver: some ported applications compiled, but looked very odd when running. Qt 4.1 brings many small bug fixes, based on customer feedback. Arthur, the painting subsystem of Qt 4, is optimised, including accelerations using OpenGL. It also included a highly improved and extended Qt3Support library and an enhanced Qt Designer with an action editor. Qt 4.1 also includes a new painting backend for Arthur: a SVG backend that renders SVGs either using the native rendering backend (e.g. Render on X11, core graphics on MacOSX) or OpenGL.</p>

<p>For the future, Trolltech is porting Qt 4 to Windows Vista and it is improving the embedded and the MacOSX version of Qt 4. Also the Qt 3 to Qt 4 migration experience will be further improved. New features according to customer needs will be integrated. Some examples could be: scripting in dialogues, printing (currently there is no cross platform preview function available), desktop integration (systray, mimetypes, help system), high-level mainwindow integration in native platforms (e.g. always use SDI on MacOSX), cross desktop IPC, improvements to the help system (currently there is no infrastructure for a context sensitive help system) and a component model.</p>

<p><strong>Aaron Seigo</strong></p>

<p>After lunch, Aaron Seigo talked about Qt and the Future of the KDE interface.</p>

<p>Aaron first summarised some facts about KDE 3.  It is based on monolithic libraries, has an attractive interface which is limited by the current desktop imaging techniques, has lots of features and lots of applications. KDE 4's library layout will follow Qt 4's more modular approach. The libraries will be split into modular components and frameworks and the naming and API consistency will be improved.</p>

<p>Next Aaron discussed the merits of the single click interface. Single clicks are good because they are easier to learn and more predictable then double clicks. In addition they are quicker. The downsides are that it is not straight-forward how the user can select something. And single click actions still have to be learnt. An alternative approach that shares the advantages of the single click interface, but does not have the disadvantages, is a mouse-over based interface. The basic idea is that a (transparent) menu opens if the mouse is over an icon. This context menu presents options like select and activate that can be performed with an object. The image below shows a mockup of such a mouse-over based interface.</p>

<p><a href="http://kubuntu.org/~jr/dot/qt-developer-day-finder-ideas.png"><img  border="0" src="http://kubuntu.org/~jr/dot/qt-developer-day-finder-ideas-wee.png" width="600" height="259" /></a><br /><a href="http://kubuntu.org/~jr/dot/qt-developer-day-finder-ideas.png">Mockup of a mouse-over based interface</a></p>

<p>Subsequently, Aaron discussed how desktop usage has changed over the years and that the desktop itself has not follow this change.  In 1984 users were typically running one application and only managed a few files. Thus it made sense to put those files on the desktop. Today most users run several applications at once and have thousands of files on their hard disks. Thus putting files on the desktop is not useful anymore. To accommodate this Konqueror will be reworked for KDE 4. It will offer a simple and direct interface to browse the web and a content manager to manage files, context and data. KDE will also make use of new X.org features to allow e.g. transparent menus as shown in the image above. Control centre and the workspace utilities will be reworked and, of course, kicker will be replaced by Plasma. Plasma aims at making the desktop useful again. It will use organic shapes to make the user experience more natural, e.g. widgets will have round corners. Widgets will be extendable, stackable and tearable. It will also introduce universal widgets which can be put everywhere on the desktop. There are numerous applications for such universal widgets: hotplug actions, messaging notifications and events. Such widgets will be easy to create using Javascript and SVG and it will be possible to use alpha blending to blend them with the desktop. Plasma will include a single click installation for such widgets. Finally, panels will be replaced by widgets. Plasma also aims to be a workflow enabler. It will allow the desktop to float and recede. Workflow is supported by following context, multiple custom layouts and networking widgets.</p>

<p>KDE will also feature an improved KDevelop and Kontact. The MDI mode and the toolbar configuration will be reworked and enhanced. KDE will also feature a new default icon theme called Oxygen.</p>

<p><em>I want to thank Matthias Ettrich who made it possible for me to attend the Trolltech developer days in Munich</em></p>
