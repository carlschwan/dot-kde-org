---
title: "Google Summer of Code: Results In"
date:    2005-06-26
authors:
  - "tmacieira"
slug:    google-summer-code-results
comments:
  - subject: "Uhm..."
    date: 2005-06-25
    body: "And while KDE got 24 bountz proposals accepted, GAIM (a single app) got 15 of those. Makes me wonder if KDE shouldn't split up into its major apps next time such a bounty project is initiated by a company. Other than that: Good Stuff!"
    author: "ac"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "Well considering Gaim is one of the most widely used open source apps, its not surprising.  It's also worth a note that Gnome only had 12 places awarded, compared to our 24."
    author: "Seb Ruiz"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "I think it perhaps has more to do with the number of proposals in this case, as Gaim had rather a lot of proposals. \n\nCould be interresting to see the breakdown of proposed vs approved, for the different projects."
    author: "Morty"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "I guess the reason is that GAIM is also running and very popular under MS Windows."
    author: "Anonymous"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "GAIM is not a Gnome-application"
    author: "ac"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "That wasn't my point, it's just that it's curious that a single app can get that much support when there are projects covering more program and use case areas. Actually I probably shouldn't have compared GAIM's 15 with KDE's 24 but with GNOME's 12 instead since this is even more mindboggling."
    author: "ac"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "I don't know if the numbers given here are reliable: http://developers.slashdot.org/comments.pl?sid=153909&cid=12911743\nThey're posted anonymously and don't mention the source. \n\nBut if they're true then Gaim has clearly gotten that many accepted applications by the sheer number of proposals:  859(!) by gaim alone as opposed to 421 by the whole of KDE.  That borders on SPAM but seems to have worked. \n\n"
    author: "cm"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "It will be interesting to see what bounties were accepted for the other projects when they are published. Google sorted out tasks which were proposed on developer.kde.org, for which applicants existed and which/who got favoring votes from KDE side. No complaint, after all it's Google's money."
    author: "Anonymous"
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "Actually, it makes sense. It is no secret that Google is gearing up to take on MS and Yahoo. In order to do so, they will need to adapt/create technology. Google will probably move gaim to more p2p so as to allow nice scaling. In addition, it will probably have video embedded in the app, but for all major platforms. Then they will be able to take on AIM, Yahoo, and MSN in one clean sweep.\n\n At the same time, they will probably introduce their own version of a browser. One that is tied closer to their server (maps, mail, etc). \n\nI would not be surprised if they do not come out with a form of Linux around Dec. that is designed to compete with MS windows directly (Linux, KDE, with their branded form of apps). It would be interesting to see a major company such as Google take on MS. since no other major one  seems willing to do so (think yahoo, aol, etc)."
    author: "a.c."
  - subject: "Re: Uhm..."
    date: 2005-06-26
    body: "Just bad that their already existing branded apps so far are exclusive to Windows. That doesn't really support your theory."
    author: "ac"
  - subject: "Re: Uhm..."
    date: 2005-06-27
    body: "Google creating their own browser?  Creating their own OS?  Are you nuts?  Dude, they are just a friggin search engine.  These technologies already exist and are used to access Google, not the other way around.  There is no reason for Google reinvent the wheel, especially when they are already \"the highway\"."
    author: "ShockedAndAppalled"
  - subject: "Re: Uhm..."
    date: 2005-06-27
    body: "However they may take existing applications (e.g. Firefox, Gaim, NVu, Thunderbird, Desktop Search) and create their own branded versions of those applications with some extensions."
    author: "Bryan Feeney"
  - subject: "Re: Uhm..."
    date: 2005-06-27
    body: "<a href=\"http://www.whois.net/whois.cgi2?d=gbrowser.com\">http://www.whois.net/whois.cgi2?d=gbrowser.com</a>\n\nGoogle's up to a lot more than you think."
    author: "Ryan"
  - subject: "Re: Uhm..."
    date: 2005-06-27
    body: "(oops, forgot to press preview -- I thought <A> tags were allowed)"
    author: "Ryan"
  - subject: "Re: Uhm..."
    date: 2005-06-27
    body: "The first lesson that you should have learned from MS's control of the industry is to not allow others to control your future. That is what happened to Word Perfect, Lotus, Netscape, Dr. Dos, Novell, etc.. All fo these companies allowed MS to control their future. Even now, AOL and Intuit are heading towards their demise (with Oracle joining them unless they move faster off of MS).\n\nGoogle has bought a photo and map company giving them ready made apps that will shortly be portable. In addition, Google is picking up Firfox coders and having them go silent. When a company goes silent, they are trying to rig and spring a trap. MS and Yahoo know they are up against Google (and so far, losing) who has superior engineering. Mark my words, Google will be introducing a network client, whic will include a browser, IM, blogging, e-mail, streaming video, etc. prior to Dec. \n\nI suspect that they will also introduce some sort of CD/DVD linux distro. All of these will be google branded and most if not all apps will take advantage of their capabilities.\n\nNuts? Nope. But neither is Google."
    author: "a.c."
  - subject: "Just in from /."
    date: 2005-06-27
    body: "http://slashdot.org/article.pl?sid=05/06/27/1126237&tid=217&tid=129&tid=1\n\nThe race is not only started, it is already underway, probably as much as 1/8 of the track."
    author: "a.c."
  - subject: "hahah gnome got only 12 proposal"
    date: 2005-06-26
    body: "the gnome team will take over the world on the next google summer code :D\n"
    author: "Lamar"
  - subject: "Re: hahah gnome got only 12 proposal"
    date: 2005-06-26
    body: "That's true, but if you add to it the projects aproved for Ubuntu plus the projects aproved to GIMP, Inkscape, RedHat and others application (not GNOME applications but some way or another it get the beneffits) you will see that it has a strong precense.\n"
    author: "Ramsees"
  - subject: "Re: hahah gnome got only 12 proposal"
    date: 2005-06-27
    body: "You forgot mono.\nNext gnome or the one after will be in C#, according to MDI."
    author: "llort"
  - subject: "Re: hahah gnome got only 12 proposal"
    date: 2005-06-26
    body: "And dont forget the Project Looking Glass supposed to the be next generation Graphics engine for GNOME.\n\n"
    author: "Ramsees"
  - subject: "Re: hahah gnome got only 12 proposal"
    date: 2005-06-26
    body: "What about Project Looking Glass is specific for GNOME?"
    author: "ac"
  - subject: "Re: hahah gnome got only 12 proposal"
    date: 2005-06-27
    body: "Java?"
    author: "llort"
  - subject: "KDE/GNOME"
    date: 2005-06-26
    body: "C'mon kids, dont be so narrowminded!\n\nPlease dont forget its all part of our beloveth Free Software Movement!\n\n> Ubuntu plus the projects aproved to GIMP, Inkscape, RedHat\n\nlet me show some proof:\nUbuntu is the base for Kubuntu\nGIMP has been used for many KDE jobs\nInkscape just ROCKS, we need Inkscape to be 100% compatible with 'our' svg icons (can we actually speek of 'ours' and 'theirs' in the FreeSoftwareMovement?)\nRedHat, well ehhh... who cares. (i dont see them in the list btw)\n\nSo PLEASE dont for get that, how hot the flame warz may get, KDE and GNOME are bot on the same side of the line!\n\nI dare to say: \"We need eachother!\"\n\nThat's it,\nCies Breijs.\n\n\n"
    author: "Cies Breijs "
  - subject: "Re: KDE/GNOME"
    date: 2005-06-26
    body: "My sentements exactly :-D\n\n~Macavity"
    author: "macavity"
  - subject: "Re: KDE/GNOME"
    date: 2005-06-27
    body: ">(i dont see them in the list btw)\ntry to look for fedora."
    author: "llort"
  - subject: "Re: KDE/GNOME"
    date: 2005-06-27
    body: "for what i know:  Fedora != RedHat\nbut i get the idea..."
    author: "Cies Breijs"
  - subject: "Inkscape & KDE icons"
    date: 2005-07-01
    body: "When you become aware of a difference between Inkscape's rendering and KDE rendering [does that mean ksvg in konqueror, or does it mean Qt's SVG support?], then you may wish to either file a bug report against inkscape if inkscape has incorrect rendering (http://www.inkscape.org/report_bugs.php); or submit a feature request (http://sourceforge.net/tracker/?group_id=93438&atid=604309) if KDE has incorrect rendering but it seems desirable for Inkscape to avoid emitting SVG that triggers that bug -- after chatting with the people behind KDE's renderer, of course, to make sure that they're aware of the bug, and to find out whether the bug is due to be fixed soon anyway.\n\nSee http://www.inkscape.org/discussion.php for how to chat to inkscape devs via Jabber or IRC, e.g. if you aren't sure which of the above categories the difference falls in :-) .\n\nThe upcoming 0.42 version of Inkscape (http://www.inkscape.org/download.php) includes an icon preview function that shows the librsvg rendering of the selected object.  It may be interesting to have the corresponding KDE preview somehow, even if implemented differently to avoid having inkscape require Qt/KDE.  Even if both Inkscape and the KDE renderer conform to the spec, it can be useful in icon work to see the different renderer behaviour (anti-aliasing, hinting etc.)."
    author: "Peter Moulder"
  - subject: "Happy and scared"
    date: 2005-06-26
    body: "Well... good news is that my proposal was accepted.\n\nNow I just wish I knew how on earth to do it. :)  My proposal was using real time code analysis with symbolic math and range calculus.\n\nAlmost makes me wish I knew what symbolic math was :)\n\n"
    author: "John (Flux) Tapsell"
  - subject: "Re: Happy and scared"
    date: 2005-06-27
    body: "You submitted your proposal without a good idea how to do it?\nThat's... erm... rather brave... on the brink of deception, I'd say.\n\nShould be interesting nevertheless. Just do a little blogging about it :)"
    author: "Jakob Petsovits"
  - subject: "ASF?"
    date: 2005-06-26
    body: "What the *peep* is ASF?"
    author: "LB"
  - subject: "Re: ASF?"
    date: 2005-06-26
    body: "Apache Software Foundation"
    author: "Anonymous"
  - subject: "Re: ASF?"
    date: 2005-06-26
    body: "Thanks!"
    author: "LB"
  - subject: "Re: ASF?"
    date: 2005-06-26
    body: "The Apache Software Foundation?\nhttp://www.apache.org/"
    author: "Sepp"
  - subject: "Thanks to Google!"
    date: 2005-06-26
    body: "for their kind gesture and help to improve open source! "
    author: "fast_rizwaan"
---
After reviewing the list of submissions, Google has released this morning the final list of <a href="http://code.google.com/summerofcode.html">Summer of Code</a> proposals they have accepted. Out of 8000+ entries, <a href="http://groups-beta.google.com/group/summer-discuss/browse_frm/thread/8b5b85e38fc8efbc/62024b5ec92784f7">410</a> were selected and KDE proponents were awarded 24 out of those which equals $120,000 of support for KDE technology. The KDE Project congratulates all those who got accepted and calls upon those who didn't to join the project anyways. KDE will certainly benefit from this experience, and wishes all the participants good luck. Stay tuned for more information later, including the accepted project list.

<!--break-->
