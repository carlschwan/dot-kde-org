---
title: "LinuxDevCenter: MusE MIDI Sequencing for Linux"
date:    2005-05-23
authors:
  - "rjonsson"
slug:    linuxdevcenter-muse-midi-sequencing-linux
comments:
  - subject: "Also look at rosegarden"
    date: 2005-05-23
    body: "Rosegarden (www.rosegardenmusic.com) is another qt-based midi program. IME it is a bit more stable, and the notation editor is much nicer, however muse has much better jack integration. But it's definately worth trying both."
    author: "mikeyd"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-24
    body: "Okay then these -- altough they dont use qt, nor KDE -- are deffinitly worth a peek:\nhttp://bloodshed.net/wired/?sid=5\nhttp://ardour.org/\n[ funny the screenshots DO show kde desktops :) ]\n\nMore info on sound apps for linux, read this article:\nhttp://www.osnews.com/story.php?news_id=1511\n\nOr get over-informed by this website:\nhttp://linux-sound.org/\n\nPersonally, by judging from the screenies i think wired [http://bloodshed.net/wired/], is woking on something like Ableton Live, which currently gets more and more mindshare from the live performing musicians.\n\nbye\n_cies.\n"
    author: "cies breijs"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-24
    body: "Well, I can't call rosegarden stable. In the last (quite a few ) tries I was quite dissapointed with rosegardens' stability. They added jack support, and since then I couldn't manage to get it working for more than a few minutes without freezing my machine completely. (while other similar software like muse didn't). The only real way I could figure out was disabling sound completely (not only jack). But then it would be rather useless, wouldn't it?\n\nI just wish MuSE had a decent score editor too...\n "
    author: "uga"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-25
    body: "It has: http://mscore.sourceforge.net/"
    author: "Wilbert"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-25
    body: "MusE had a score editor but it was removed since the author, Werner Schweer, found it to be a technical deadend. MScore is a redevelopment of a score editor for MusE. It does read MusE score files (or so it is planned anyway), it is still in it's infancy but already quite usable.\nIf technically possible it will make a return in a future version of MusE.\n"
    author: "Robert Jonsson"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-25
    body: "Wow, thanks!"
    author: "uga"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-25
    body: "If it's really problematic, recompile without jack support. It's still very useful as a midi program without jack."
    author: "mikeyd"
  - subject: "Re: Also look at rosegarden"
    date: 2005-05-24
    body: "What I miss under Linux is a tool such as Band-in-a-BOx, a software which made it from the ATARI and covers what musicians really need. It has no clean design but all the features other solutions lack. It is a wasted effort to catch up with cubase and so on. Bazaars are bad for building cathedrals. At least when everybody knows how the ready cathedral shall look like."
    author: "Gerd"
  - subject: "Congrats!"
    date: 2005-05-24
    body: "It is nice to finally could say: \"The state of Linux audio synthesis _was_ in need\". Between MusE, Rosegarden, Audacity and Hydrogen, those so inclined could finally get down to the essence, instead of continually titubating between half-finished, feature-incomplete, not-quite-always-compiling tradition of things in this domain.\n\nCongrats, MusE team!\n\nI believe there's an important need inside KDE to coopt people like the MusE team (we already have a staunch supporter and contributor in Rosegarden's Guillaume Laurent). \n\nConverging to a better choice of multimedia infrastructure and producing a set of native KDE apps that would cover the bases and fill the (few) remaining gaps could be the major tasks for a to-be-enhanced multimedia task force (see http://www.kde.org/areas/multimedia/)."
    author: "Inorog"
  - subject: "Re: Congrats!"
    date: 2005-05-26
    body: "> It is nice to finally could say: \"The state of Linux audio synthesis _was_ in need\".\n\nHmm... Rosegarden is *very very* slow. MusE requires a kernel patch. (Wtf? Similar programs on windows/mac don't patch their kernels.)\n\nAnd more to the point, I've never got MIDI to even output sound. Don't know what is wrong with it, or where to start troubleshooting. Even the linux midi howto doesn't give any help.\n\nI'd say it needs some work. Looks like it is getting closer to usable though. The new MusE interface is definitely better than the old one."
    author: "Tim"
  - subject: "Re: Congrats!"
    date: 2005-05-27
    body: "There must be some misunderstanding. MusE does not require any kernel patch, though there are patches that enhance the realtime performance, lately the kernel has taken huge leaps in performance improvement, since 2.6.10 you get along quite well with a standard kernel.\n\nAs for MusE needing some work, yes of course, we are far from finished. But it is already stable enough to be able to comfortably create music (depending somewhat on your style of work).\n\nAbout your midi problems, no idea, the midi-howto is very old I think. I recommend trying the linux-audio-user mailinglist available here:\nhttp://music.columbia.edu/mailman/listinfo/linux-audio-user\n"
    author: "Robert Jonsson"
  - subject: "Re: Congrats!"
    date: 2005-05-27
    body: "<i>MusE does not require any kernel patch</i>\n\nOh. I was mislead by your wiki!\n\nhttp://www.muse-sequencer.org/wiki/index.php/Installation#Software\n\nIncludes the realtime module in software requirements...\n\n"
    author: "Tim"
  - subject: "Re: Congrats!"
    date: 2005-05-29
    body: "I pretty agree with the parent comment. From my point of view, it is much more simpler to install and run a webserver than a simple music app like Muse (yeah, simple. Very simple. Reason is complex -but 1000000 times simpler to have running on win-). Have tried during hours to make the midi works, but it still f... Muse is interesting ; but if I could use it it would be much more interesting ! Sure, there are how-to, but at minimum a level of professionnal sys-admin is required to follow them. So there are much more improvements to do in this direction than having always more features. If you write apps for musicians and no musician is able to install it, then you lost. I don't know any musician that use Linux Apps ; however most of them know computers very good, but this is simply a nightmare to have an application working. Musicians are not Geeks, they don't want to patch their kernels and start module and install packages, they want to make music with the computer. Understand that and you will write the killer app.\n"
    author: "Stephane Rodet"
  - subject: "[OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "I wonder what will be the default SOUND System for KDE 4.x, arts seems to be dropped! \n\nHow about ALSA support, which is fast."
    author: "fr_dude"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "KDE has a policy of being system-neutral. ALSA is Linux-specific. If KDE depended directly on ALSA, it wouldn't work on BSD, for instance. That's where aRts came in: to provide sound abstraction. KDE plugs in to aRts which plugs in to OSS (or whatever your system provides).\n\nThere is an ongoing debate about what sound system to use for KDE4, it seems. There was some hubbub about it at the last Konference. From memory, the idea is that aRts is deprecated: the sole developper that built it is gone, and it's difficult (actually, it bordered closer to \"impossible\", I think) to maintain. So they're either going to either rebuild it from scratch, or pickup Gstreamer. Scott Wheeler, author  of JuK among other things, made some KDE bindings for Gstreamer.\n\nPersonnally, I think it'd be cool if KDE picked up Gstreamer. It's a very powerful multimedia framework with lots of activity around it, it has already been picked up by GNOME, and it remains desktop-neutral!\n\nUnfortunately, because it's a framework, and not directly an audio sink (which ALSA and OSS are), the latency problems you had with arts are there to stay... As the KDE team will probably add an extra layer to it (to manage events for instance?), it's not going to get better :("
    author: "other_fr_dude"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "> So they're either going to either rebuild it from scratch, or pickup Gstreamer.\n\nNMM (http://www.networkmultimedia.org/) was mentioned as possible choice, too, and from what I've heard the demonstration last year made quite an impression.  It's written in C++. \n\nOTOH having a common multimedia framework with GNOME would simplify things...\n\nBut many of the pros and cons of either solution have already been discussed on the mailing lists.  See the archive on http://lists.kde.org/ . "
    author: "cm"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "> KDE has a policy of being system-neutral\n\nwe also need codecs, among other media gadgets. ALSA doesn't provide those even if it were x-platform.\n\n> they're either going to either rebuild it from scratch\n\nthis has a snowball's chance in hell of happening. it would make pretty much zero sense to repeat the same mistakes over again (trying to incubate a sound system within KDE) when viable solutions already exist (something that wasn't true with aRts came around).\n\n> or pickup Gstreamer.\n\nthis is highly more plausible, though not the only option being considered at this time.\n\n> As the KDE team will probably add an extra layer to it\n\nseeing as where the layer will be and what it will be used for, i highly doubt this will matter"
    author: "Aaron J. Seigo"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "Just a small note here -- Tim Jansen actually did almost all of the work on the bindings.  I just came after him and generated the source packages; JuK was also the only thing using them at the time.\n\nThere's a lot that could be talked about here relating to media frameworks, but it's not worth repeating everything that's been said on the mailing list and elsewhere.\n\nSee here http://dot.kde.org/1101293677/ for a discussion of some of this stuff.\n\nThat said, it should be noted that things like ALSA *aren't* media frameworks and don't compete with things like aRts, GStreamer, NMM, etc.  The latter are all high level frameworks to simplify dealing with the lower level more complicated issues like, well, dealing with output drivers directly."
    author: "Scott Wheeler"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-06-02
    body: "A question: Is Jack an option? For me, it seems to handle all the needed applications and OSs?\n\n  http://jackit.sourceforge.net/\n\nJostein\n"
    author: "Jostein Chr. Andersen"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "Alsa is a Linux-only solution.\n"
    author: "Luciano"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "> How about ALSA support, which is fast.\n\nThe \"L\" in ALSA means it's Linux-only.  KDE runs on a lot more system than just Linux. \n\nHere's a bit about sound in KDE 4:  http://cvs-digest.org/index.php?issue=sep242004\n\n"
    author: "cm"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-24
    body: "Thanks \"cm\" and \"Other_fr_dude\" for your replies, I am eagerly waiting for KDE 4 with great sound compatiblity with other non-kde apps and awesome performance."
    author: "fr_dude"
  - subject: "Re: [OT] KDE 4 drops arts???"
    date: 2005-05-26
    body: "I hope whatever they choose has support for Solaris.  I wrote the Solaris ARTS driver a number of years ago and it has continued to work well.\n\nIt is especially useful since Solaris does not have anything like ALSA and in general has fairly limited audio support.\n\n-Aaron"
    author: "Aaron Williams"
---
<a href="http://muse-sequencer.org/">MusE</a> is an extensive Open Source MIDI and audio sequencer for Linux, based on Qt. The <a href="http://www.linuxdevcenter.com/pub/a/linux/2005/05/12/muse.html">developers have been interviewed by O'Reilly</a>. For those interested in some background information about MusE, MIDI and audio development on Linux, be sure to check it out. 
A <a href="http://muse-sequencer.org/index.php?action=fullnews&id=90">progress report</a> for the coming 0.7.2 and 1.0 releases has also been posted to the MusE website.




<!--break-->
