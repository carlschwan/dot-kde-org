---
title: "Hai Ti Comic Teaches KDE"
date:    2005-04-26
authors:
  - "uthiem"
slug:    hai-ti-comic-teaches-kde
comments:
  - subject: "supersonic"
    date: 2005-04-26
    body: "All IT professionals should be given supersonic jets."
    author: "Ian Monroe"
  - subject: "Re: supersonic"
    date: 2005-04-26
    body: "Actually, I don't know whether it is supersonic. What the point of a supersonic plane if you are travelling something like 1,000km? It is, on the other hand, VTOL - and that is much more important in a country like Namibia where you have to go remote places.\n\nAnyway, what do you think about the comic, folks? And think about it, the online edition is just that. The real thing, printed and all, has been distributed widely on Tuesday (still today for me).\n \nI watched the access log of that web server. Most people from all over the world actually read the whole 20 pages. So they didn't just pop in and out again. Tell us what you think!"
    author: "Uwe Thiem"
  - subject: "Re: supersonic"
    date: 2005-04-26
    body: "i thought it rocked =)\n\nit had a positive message, was educational and promoted Free Software. i can't wait for patches/artwork/translations/ideas to start flowing from Namibia and other African nations as the students exposed to open source as teenagers become members of the global community of Open Source developers ...\n\nitems like this story make me happy .. and hopeful. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: supersonic"
    date: 2005-04-26
    body: "So we did accomplish something at least. ;-)\n\nLemme lift this secret: Aune and Peli do exist. They are real people. I actually know them. Young ladies unafraid of school principals, technical issues or travelling into the bush.\n\nThese movers are that: Movers. They take up a task and do it. Most of you folks cannot really appreciate what that means. Out there amoung snakes, scorpions and other vermins, they put up solar panels, antennas for wireless connectivity and do whatever is necessary to hook up another school. They don't have that fancy plane - but they do their job.\n\nWhat more can any of us claim?"
    author: "Uwe Thiem"
  - subject: "Re: supersonic"
    date: 2005-04-26
    body: "I think it's great, the comic and the initiative."
    author: "Boudewijn Rempt"
  - subject: "Re: supersonic"
    date: 2005-04-26
    body: "I'd love to read the whole comic as pdf, but\nthe pdf's loaded a bit to slow, to read it\nconfortable...\nWould it be possible to upload the whole comic\nin one big pdf file? I can imagine the real\nthing is much nicer, as a belgian and big fan\nof comics!\n\nRegards!"
    author: "Pieter"
  - subject: "Re: supersonic"
    date: 2005-04-27
    body: "I'll talk to SchoolNet's web mistress."
    author: "Uwe Thiem"
  - subject: "Re: supersonic"
    date: 2005-04-27
    body: "That would be very kind!\n\nRegards,\nPieter"
    author: "Pieter"
  - subject: "Re: supersonic"
    date: 2005-04-26
    body: "Do they have more than one jet? If not, having it parked at that one school for two days meant they would be unable to respond to emergencies elsewhere during that time. So shouldn't they have brought a pilot to fly the plane back while they were working?"
    author: "Martin"
  - subject: "Providing people the means to educate themselves"
    date: 2005-04-27
    body: "Unfortunately, education often is a privilege. Knowledge is power, the power to help yourself and others, and Open Source projects are empowering more and more people to tackle their everyday problems and create new opportunities in life. This is one of the main reasons that I love Free Software/Open Source and KDE. SchoolNet people, you rock! Apart from life and love, education is the most valuable gift you can give/receive, IMHO.\n\nPS: Hai Ti is a cool comic. It also reminds me of a friend of mine, who worked in Haiti on a similar education program and saw how well it works. As a result, he still talks about it a lot and now is totally into Linux and KDE.\n\nThanks everyone "
    author: "Claire"
  - subject: "Creative Commons"
    date: 2005-04-27
    body: "Did you folks actually realise that the whole comic is under Creative Commons? Everybody is entitled to use, modify and adapt it for their fown work. It is, so to say, a gift frofm SchoolNet Namibia to the rest of the world. Kinda reverse development aid. ;-)"
    author: "Uwe Thiem"
  - subject: "Re: Creative Commons"
    date: 2005-04-27
    body: "Cool. Thank you SchoolNet Namibia! :-)\n\nTorsten"
    author: "Torsten Rahn"
---
Today's copy of Namibian newspaper <a href="http://www.namibian.com.na">The Namibian</a> comes with a computer education comic featuring KDE.  Named Hai Ti ("Listen up!" in the Oshiwambo language), <a href="http://www.schoolnet.na/haiti">the comic</a> features the super-hero like <a href="http://www.schoolnet.na">SchoolNet project</a> showing student and teachers <a href="http://www.schoolnet.na/haiti/page17.html">their KDE desktop</a>.









<!--break-->
<p>SchoolNet is a Namibian organisation whose aim is to bring computers and the
Internet to all schools in the country. So far it has
installed computer labs, consisting of 5 to 10 KDE based
workstations, in some 340 schools, mostly in remote areas.
It uses refurbished computers as thin clients powered by
the South African Linux distribution <a href="http://www.direq.org">OpenLab</a>
 which focuses on education and
defaults to KDE as the desktop. Snapshots of <a href="http://www.gutenberg.org">Project Gutenberg</a> and <a href="http://www.wikipedia.org">Wikipedia</a> as well as other educational
content are accessible locally at the schools.</p>

<p>The Hai Ti comic features the daily life and struggle of young
technicians at SchoolNet Namibia as well as teachers and
students at the schools they serve. While the story of their lives
develops it explains how to launch an application, use a
spreadsheet, search the Internet and use the
locally available KDE educational software.</p>








