---
title: "New Engine for KDE Online Documentation"
date:    2005-06-27
authors:
  - "rendres"
slug:    new-engine-kde-online-documentation
comments:
  - subject: "nice"
    date: 2005-06-27
    body: "I like the design very much and the colors.  It's refreshing to see a non-monochrome KDE site for a change.  The navigation is very nice too.\n\nThere are some more details at  http://physos.org/2005/06/27/docskdeorg-01/ that weren't posted in this article for some reason."
    author: "KDE User"
  - subject: "Same API as Wikipedia "
    date: 2005-06-27
    body: "Why don't provide same service/API of the KDE-Wikipedia project, in order to  provide online user ?\n"
    author: "GeoVah"
  - subject: "Re: Same API as Wikipedia "
    date: 2005-06-27
    body: "Arf, online help not online user"
    author: "GeoVah"
  - subject: "Re: Same API as Wikipedia "
    date: 2005-06-27
    body: " The amaroK hackers are experimenting with this already, so I guess its just a matter of time before they have accumulated enough experience on this to make an easy kdelibs addition of \"generic wikipedia api\" :-)\n\n On the other hand it is vital that the primary on-machine documentation doesnt get lower importance over the user-made wikipedia.. not everyone has internet, and wikipedia is not subject to the same kind of controll.\n\n~Macavity"
    author: "macavity"
  - subject: "Re: Same API as Wikipedia "
    date: 2005-07-01
    body: "The Offline Version could be some kind of a statical Snapshot from Wikipedia OnLine Version"
    author: "pjotr"
  - subject: "Navigation is painful"
    date: 2005-06-27
    body: "The layout of http://docs.kde.org is awesome, it efficiently uses the space and allows more information without the need to scroll.\n\nBut, as soon as you click any documentation, you are watching badly arranged documentation, space is not efficiently used, one has to scoll a lot. A frame on the right side with important links inside the document will definitely help. "
    author: "fast_rizwaan"
  - subject: "Re: Navigation is painful"
    date: 2005-06-27
    body: "Why don't we make KDE application documentation frame-based. so that a user doesn't have to scroll to the bottom to click up,home,next. \n\nAnd really annoying is \"the delay to access content\", \n\nto access content a user has to :\n\n1. scroll down and click HOME link\n2. in the HOME again scroll to access the required section.\n\nInstead, a left frame-pane which shows all the Contents clickin on it will be much easier and fast.\n\nbut we've been used to bad navigation system :("
    author: "fast_rizwaan"
  - subject: "Re: Navigation is painful"
    date: 2005-06-27
    body: "The problem here is probably DocBook.  All DocBook documentation I've seen has horrible navigation.\n\nThe worst problem by far of DocBook is you have a page with one or two lines of text then you have to click next to get another one or two lines ad infinitum.  It is extremely aggravating and there is no such thing as an all-in-one page link."
    author: "KDE User"
  - subject: "Re: Navigation is painful"
    date: 2005-06-27
    body: "Well DocBook itself is probably innocent here.\n\nThe problem that you describe is more how KDE cuts the text in sections. That is probably useful but indeed not always, especially, as you tell, if the section has very few text (for a reason or another.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Navigation is painful"
    date: 2005-06-27
    body: "you can use the document relations toolbar. It makes navigation a breeze :-)"
    author: "trollichon"
  - subject: "Re: Navigation is painful"
    date: 2005-06-28
    body: "The primary target for the documentation stylesheets is KHelpcenter, which already provides it's own navigation - duplicating that inside the pages would be a huge waste of space.  It's obviously far (far) easier to maintain one set of layout stylesheets, so the ones that generate docs.k.o are very similar to the default KHC ones, with just a few changes.  \n\nIf you have a better layout, (and especially if you know how to generate that in the XSLT transformation), feel free to send me or Rainer a patch.\n\nOn splitting and where we break for a new page, that's on sections, not on chapters.  DocBook being infinitely flexible doesn't really care, but very very few people have ever complained about it, and several have said they prefer it over having longer pages to scroll through. In any case, how much text is in a section is down to the author of the section - write more content, and we would have longer pages.  It would take a trivial change to generate longer pages, but since there's little demand, and the cost in resources would be enormous (It would bump the time taken to regenerate the pages from 'hours' up into the area 'days') I don't think it's currently a reasonable trade-off.\n\nI don't believe the online version of the docs will any time soon be the primary source for all the documentation requirements, simply because there are still far too many people around the world on low bandwidth and/or high cost connections for it to be considered.  What is a possibility is using GHNS or similar to provide the option to update the local copy, but it's not there yet.  \n\nAs for frames - that was a joke, right?"
    author: "Lauri Watts"
  - subject: "Re: Navigation is painful"
    date: 2005-06-28
    body: "khelpcenter is better than no navigation. But the \"tree\" view looks ugly. a flat frame (no trees) would be good looking as in http://docs.kde.org."
    author: "fast_rizwaan"
  - subject: "PDF Generation"
    date: 2005-06-27
    body: "> especially PDF generation\n\nPDF generation is really something quite some users are looking forward to, because of the possibility to print them.\n\nI guess a printed form of the user guide would sell quite well on Linux events\n"
    author: "Kevin Krammer"
  - subject: "Re: PDF Generation"
    date: 2005-06-27
    body: "Indeed!\nI know it is possible to generate it myself from the docbooks, but I never had any time to look into that.\nBut if docs.kde.org can provide them online, I don't need to bother :)\n\nRinse"
    author: "rinse"
  - subject: "Re: PDF Generation"
    date: 2005-06-27
    body: "If the PDF gen is done carefully, it will be killer stuff, expecially for 1) application tutorials, 2) user guides and 3) application manuals.\n\nI think that having something such as \"The DigiKam 1.3 manual\" in 19 pages with digikam logo on the first page and subtle art on the top and bottom margin of each page will make documentation really useful, since people will start printing manuals of the apps they like.\nAlso the fixed per-page layout (with decorated margins, page numbers, etc) of a PDF is something that can't be done in HTML. (or looks bad)\n\nMore notes:\n1) if an app has 20 sentences of help, the manual must be 2 pages long, not 8 (title, gpl, index, blank, chapter, content1, content2, end).\n2) should be cool to have documentation generated in different styles (some like black over blank 60 letters latek-styled paragraphs, some like them more colorful and with kde logo on each page)\n\nDocs.Kde.Org is COOL!! (and what's better: is automatic, like knewstuff :-))\nThank you!\n"
    author: "jumpy"
  - subject: "Re: PDF Generation"
    date: 2005-06-28
    body: "I'm about to restart work on PDF generation of the KDE docs. I did quite a bit of testing a couple of months ago, but I have some time now to get going again. So, expect at least *some* progress before the end of the summer.\n\nHowever, I'm no expert in XML, LaTeX or PDF (since we'll probably be using DocBook -> LaTeX -> PDF), so if you have any experience with those, please consider helping me out :-).\n\njumpy: I notice you have some ideas - we'd love to have you on board. Drop me a line at \"phil at kde dot org\", and I'm sure I can find ways for you to help out as much or as little as you'd like. The same goes to anyone else, of course."
    author: "Philip Rodrigues"
  - subject: "Eek!  Too many words!"
    date: 2005-06-28
    body: "<p>\nFirst off, there's lots of good info in there, so hats off to everyone involved with the unsexy job of documentation.\n<p>\nHowever, the front page is a disaster.  Let me just give a shotgun summary of my first impression as someone who has never seen the site before:\n\n<ul><li>I can't figure out who the audience is and there's no hints as to who each section is geared toward.  Is it users?  Developers?  Both?  I've looked at it for about 10 minutes and I still don't know the answer.\n<li>What is the relationship between the box at the top and the topics below?  What is the relationship with the box on the left?  There's no navigational hints to tell me.  I had to stare at the page for a few minutes to figure it out.  Someone not already familiar with KDE's apps won't get it.\n<li>Overall there's just too many words that seemingly don't relate.  It's impossible to quickly scan over the list and pick out highlights.  For instance, \"FAQ\" capitalized will quickly draw attention compared to \"faq\".  \n<li>Why is kcontrol listed on the front page?  What's it's significance and what do the words underneath it have to do with kcontrol?\n<li>For me, The User, you have completely useless information on the front page that I don't give a rat's ass about.  Why is kdebugdialog listed on the front page?  Sure, that info should be there if I search for it or attached to whatever part of KDE it's associated with, but not on the front page.\n<li>There is nothing on the page that draws my attention and says \"Start here if you're looking for information.\"  \n</ul>\n<p>I really think you need a front page front-ending this to help with navigation.  Sort of, \"If you know nothing about KDE, start here\".  Of course you don't explicitly say that.  Instead maybe you some how create a text box that has the top 3 - 5 KDE docs: a FAQ?  a User Guide?  Package list? \n<p>\nAs I mentioned, you have a boatload of great documentation in there.  Don't trample it with this interface.\n<p>\nBrian Vincent<br>\nWine Team\n"
    author: "Brian Vincent"
  - subject: "Re: Eek!  Too many words!"
    date: 2005-06-28
    body: "mmmh\n\nI do not have the time to anwser this properly this morning.\n\nWill answer in longer way later the day. \n\n\"Instead maybe you some how create a text box that has the top 3 - 5 KDE docs: a FAQ? a User Guide? Package list?\"\n\nThis, is exactly the content of the front page. UserGuide \nand FAQ highlighted on the top-right, package list on the left. \n\nIf that is the solution to my totaly failed interface, I really wonder. \n\nI agree, there is room for improvements, a lot, but I had to start somewhere. \n\nAs I said, I will try to address your comment in a more thorough way later today. \n\n              Rainer\n"
    author: "Rainer Endres"
  - subject: "Re: Eek!  Too many words!"
    date: 2005-06-28
    body: "I have to wonder if it's the first time for you to notice how big KDE actually is. I personally see not much room to improve the layout since there is no way you can decrease the size of modules without splitting them, and splitting them just for docs is kind of senseless."
    author: "ac"
  - subject: "Re: Eek!  Too many words!"
    date: 2005-06-28
    body: "I've used KDE since '97 when it was still alpha, but I'd hardly consider myself even a 'power user'.  Mostly I use X as a holder for konsole windows and a browser (by the way, konsole is a great terminal emulator.. by far the best on Linux.)  \n\nAnyway, when I first look at the docs page I don't notice anything that stands out and attracts my attention.  The green table background on the left does, but I don't understand the organization of those packages (it makes me think too much).  On the right the text box containing the \"User Guide\" and \"FAQ\" didn't catch my attention because I thought it was simply the title of the search boxes below.  I agree that those areas are useful.  What really seems to detract from everything is the stuff in the middle of the page.  When I look at it I just see a bunch of words that look like kgibberish.  I don't really understand the importance of those middle tables or why they should be on the front page.  Nor do I understand the relationship between all those components.\n\nAnyway, I tried to think of what would help and I think it's just a series of arrows and some page layout changes.\n\n - people read left to right (at least in my country and probably yours), including when they read web pages.  We're accustomed to important navigation being on the left because of that, or may be at the top.  If you really think the package list is the most important, then leave it on the left.  I'd argue that your user guide and FAQ are more important and should be listed there in a color that draws my attention.\n\n - some how move the package selection, but keep the same functionality.  Maybe change the green to blue so it catches my attention, but not as the first thing.  Then use arrows or something to tell me how the selected package is related to a file list (or is that a feature list?  I can't tell) in the middle.  Likewise, use some kind of arrow to tell me how the files relate the tables below.\n\n - finally, add some kind of paragraph and welcome message at the top of the page in the center to tell me what I'm looking at.  I'm a Dumb User who doesn't understand kgibberish or how kfoo relates to kbar.\n\n\n"
    author: "Brian"
---
The KDE online documentation site <a href="http://docs.kde.org/">docs.kde.org</a> has gotten a new back- and frontend addressing experiences 
made over the last few years. It now allows for faster and easier navigation through 
the languages and branches. The <a href="http://docs.kde.org/development/en/kdebase/userguide/index.html">User Guide</a> and <a href="http://docs.kde.org/development/en/kdebase/faq/index.html">the FAQ</a> are 
featured more prominently and in the chosen language if already
translated. The excellent search functionality has gotten a 
better place on the front page. Many things have happened under the hood, which allow for easier 
maintenance and additions over the next months, especially PDF generation.
<!--break-->
