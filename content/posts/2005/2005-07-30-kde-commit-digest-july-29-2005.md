---
title: "KDE Commit Digest for July 29, 2005"
date:    2005-07-30
authors:
  - "dkite"
slug:    kde-commit-digest-july-29-2005
comments:
  - subject: "What decides..."
    date: 2005-07-30
    body: "...whether a commit is listed? It seems most all of them that have BUG:, CCBUG:, FEATURE:, and the like are in, but there's also several without any of them. And then there's a whole lot more that likewise don't have them and /aren't/ listed."
    author: "Illissius"
  - subject: "Re: What decides..."
    date: 2005-07-30
    body: "I believe pointing out something to Derek helps as well, IIRC."
    author: "Ian Monroe"
  - subject: "Re: What decides..."
    date: 2005-07-30
    body: "The grey matter between my ears.\n\nThere are somewhere around 2000 commits a week. Most are small changes, tidying up layout or code, minor fixes for correctness, building, spelling, etc. I select commits when the developer marks things as a feature or bug fix. Some commits are obviously important, implementing a feature or fixing a bug.\n\nIt helps when the log is verbose. If I've missed something you consider important, send me an email. You can do it automatically with CCMAIL:. I don't avoid anything to be obtuse, but sometimes things get missed, especially if I'm not familiar with a project.\n\nDerek\n\n "
    author: "Derek Kite"
  - subject: "Re: What decides..."
    date: 2005-07-30
    body: "Hmm, okay. This wasn't really a \"zomg, my commit isn't included!!1\", but more of a \"that's odd, why is one there, and not the other?\" type of question. (I'd always assumed, apparently incorrectly, that the digest is generated automatically, with /all/ commits).\nDidn't realize you read all of them manually. Must be a lot of work ^_^"
    author: "Illissius"
  - subject: "Re: What decides..."
    date: 2005-07-30
    body: "Even if it is automated, the digest cannot be another kde-commits mailing list. So somewhere there must be a moderator (be it a computer, a human or a mix of both.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Teaser"
    date: 2005-07-30
    body: "Have a glimpse of Kopete: http://process-of-elimination.net"
    author: "Matt T. Proud"
  - subject: "Re: Teaser"
    date: 2005-07-30
    body: "buddy icon support, isn't that already old? :D"
    author: "superstoned"
  - subject: "Re: Teaser"
    date: 2005-07-30
    body: "Not in the contact list, no :d\nI'm happy to see this changing, since it will lighten up the contact list greatly IMHO :d Now i only hope Jabber will have avatar support too :d"
    author: "Bart Verwilst"
  - subject: "Re: Teaser"
    date: 2005-07-30
    body: "well, it IS nice kopete automatically tries to get all these pictures, but they disapear when the person is offline, and if you merge kopete's contacts with the adressbook, you see a grey area when someone is offline... :("
    author: "superstoned"
  - subject: "Re: Teaser"
    date: 2005-07-30
    body: "Hey, it's not out yet ;) They might implement it before 3.5 is out ;) Maybe ask on  IRC's #kopete or mailinglist? I'm sure this will be implemented eventually.."
    author: "Bart Verwilst"
  - subject: "Re: Teaser"
    date: 2005-07-30
    body: "what do you mean old? if you have been able to see buddy icons for a long time, where does you go to see them? has there been some way of making them visible during a conversation? what about for aim?"
    author: "Confused"
  - subject: "Re: Teaser"
    date: 2005-07-30
    body: "well, i don't remember when it came, and i used to run trunk (SVN) for a long time so i guess i've been seeing it longer than most other users, but i think it is at the very least in kde 3.4, if not 3.3!"
    author: "superstoned"
  - subject: "Re: Teaser"
    date: 2005-08-03
    body: "User icons are already in kopete for quiet a while. You can change the userlist setting with the preferences to show user pics."
    author: "Gerwin"
  - subject: "KGet"
    date: 2005-07-30
    body: "Very nice to see active development in KGet."
    author: "m."
  - subject: "Re: KGet"
    date: 2005-07-30
    body: "There is the whole time development in make_kget_cool branch, dunno if Commit Digest covered it."
    author: "Anonymous"
  - subject: "Re: KGet"
    date: 2005-07-30
    body: "How about BitTorrent support in KGet? In for 3.5?"
    author: "Eike Hein"
  - subject: "Re: KGet"
    date: 2005-07-30
    body: "In extragers you can find a very decenttorrent cliente, ktorrent.\nI'm using a compiled versin from svn and I think it rocks,"
    author: "Iuri Fiedoruk"
  - subject: "Re: KGet"
    date: 2005-07-30
    body: "agreed :D"
    author: "superstoned"
  - subject: "Re: KGet"
    date: 2005-08-01
    body: "Nevertheless, integration with KGet would be cool ;)"
    author: "Bram Schoenmakers"
  - subject: "Re: KGet"
    date: 2005-08-01
    body: "Yeah, plus it could support aMule (www.amule.org) core, so we could have a all in one download/p2p solution.\n\nBut I belive it will only happen later, so Kontact as an example, first the apps, then the integration."
    author: "Iuri Fiedoruk"
  - subject: "Issues with media:/ kioslave"
    date: 2005-07-30
    body: "Working with KDE 3.4.1 on Debian Sid, I noticed some weird behaviours when working with files on the disk via media:/.\nThe view didn't get updated when a new file was added, I had to refresh manually. Also when right-clicking to see the size of the folder it didn't compute it automatically, I had to click on compute (or how it is called) to see the size (showing the size is the same as with smb://, but with smb:// it is understandable since it generates load on a remote machine).\nAlso I had trouble unpacking archives with Ark read from media:/ and written to  media:/, as it was twice as slow as before with KDE 3.3.\nIt seemed as if it unpacked everything to /tmp/ or a kde/temp directory first (also on another hard-disk), and wrote from that to the final location.\nI had major trouble with that when I opened a .tar.gz file with my home dir as it took ages to show the files in Ark and seemingly write everything to a temp dir first.\nWith KDE 3.3's file:/ or when switching from the media:/ protocol to just /mnt/hdax/dir it works the right way, also there is no /tmp/ dir in between when unpacking.\nBugs? Deliberate behaviour?"
    author: "Phase II"
  - subject: "Re: Issues with media:/ kioslave"
    date: 2005-07-30
    body: "afaik, this has to do with KIOslaves in kioslaves in kioslaves or something like that. media:/ isn't as direct as file:/, it might indeed first make a full copy in /tmp. same prob with video, as i recall. but - they are working on it, i think it'll be fixed in 3.5 - if not fully, then wait for kde 4 :d"
    author: "superstoned"
  - subject: "Re: Issues with media:/ kioslave"
    date: 2005-08-01
    body: "Some of this is already fixed in trunk but couldn't\nbe backported to 3.4.x.\n\nFurther testing on trunk version and report on\nhttp://bugs.kde.org (currently offline) would be\nuseful of course."
    author: "ervin"
  - subject: "Please add Win key support"
    date: 2005-07-30
    body: "Please aaron, add support for Win key for kmenu. only 3 days to go for features. I would really appreciate your contribution and nice gesture. thanks in advance."
    author: "khaled dude"
  - subject: "Re: Please add Win key support"
    date: 2005-07-30
    body: "Not going to happen.\n\nThat feature was removed because it was buggy.\n\nThe K menu can be opened with Alt+F1, just like it always could."
    author: "Thiago Macieira"
  - subject: "Re: Please add Win key support"
    date: 2005-07-31
    body: "The lineakd, we have a bunch of KDE plugins. KMENU is one of them."
    author: "Tormak"
  - subject: "(hidden) "
    date: 2005-07-30
    body: "<i>(See HTML source for title and body text) </i>\n<!--\nStatus: KDE Dot News Abuser\nTitle: World of Wonders\n\nI wonder why people are still using KDE now that we have GNOME. KDE is written by people who couldn't get hold of the existence of GNOME which came up one year earlier than KDE. KDE sucks because QT sucks due it's propritary and limiting license.\n-->\n\n"
    author: "youknowmewell"
  - subject: "Re: World of Wonders"
    date: 2005-07-30
    body: "Why are pro-GNOME trolls still coming to this site?\n\n> KDE is written by people who couldn't get hold of the existence of GNOME which came up one year earlier than KDE.\n\nIf you want to be taken serious check your facts first! It was the other way around."
    author: "Anonymous"
  - subject: "Re: World of Wonders"
    date: 2005-07-30
    body: "> Why are pro-GNOME trolls still coming to this site?\n\nThey feel that the existence of GNOME is severely endangered by KDE especially considering the very fast progress of the latter (in large part due to Qt) compared to the crippled pace of the former (blame GTK here)."
    author: "KDE User"
  - subject: "Re: World of Wonders"
    date: 2005-08-01
    body: "I do not thing the person was pro-Gnome. I mean, this troll was way too obvious. It must just be somenone who wanted to see if he could gather some reactions and have fun trolling. I mean, I do not think Gnome advocates are so bad at trolling. Honestly."
    author: "oliv"
  - subject: "Re: World of Wonders"
    date: 2005-07-30
    body: "Yeah! Enjoy! http://tinyurl.com/89c9s"
    author: "ac"
  - subject: "Re: World of Wonders"
    date: 2005-07-30
    body: "LOL\n\nbut seriously, wat he means (and i think is he is right) is that gnome is obsolete. Gnome was created because KDE was based on Qt, which had a proprietary license. now Qt is GPL (how can something be more free than the license the Free Software Foundation recommends, and is written by the God of freedom, RMS, who wanted to start gnome in the first place) there is no place for Gnome anymore. KDE is technically far ahead, maybe gnome has a small but dimmishing edge on usabillity - but that's not enough. its a waste of time and money. dump it..."
    author: "superstoned"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "It's a major waste of time and money all right. But i have to admit that i\ndon't like hefty platform licensing fees either. Linux is getting terribly\nexpensive platform as is these days anyway.\n"
    author: "Anonymous Coward"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "well, imho its not that bad. TT is known for being nice to ppl that can't pay it easilly, and with Qt 4 they split up Qt so you can choose to license only what you really need - which can be much cheaper than the current all-or-nothing package.\n\nso, for in-house development and for GPL-like development, linux+Qt is simply free. for commercial sale, you can choose what Qt components you need and license them, and if you're a startup, you can talk to Qt about some other form of payment."
    author: "superstoned"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "Yep, but with Linux you kinda end up with various licensing/support contracts,\nand that is _not_ good. Say you build up a high-end development server:\nreiserfs4 support from Namesys, driver support for that nicey QLogic FC adapter from QL, packaging&security updates from SUSE, software updates and usage support from 3rd party local vendor Z (SUSE only supplies you security fixes, no software updates), UI-tools from TT and god knows what else. \n"
    author: "Anonymous Coward"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "well, reiser4 is only needed if you want to sell it and not give the source with it. and i guess suse can also give you software updates. but you might be right, it can be alot - but how is that with say Microsoft? YOU opt for software updates, YOU opt for a special filesystem etc..."
    author: "superstoned"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "> how is that with say Microsoft? YOU opt for software updates, YOU opt for a special filesystem etc...\n\nEntire set is relatively well integrated (say, in windows you generally have just one http stack, whereas in general Linux you have a dozen half-finished stacks), tested and 'productified' by MS and 3rd party hw/sw vendors. This \nhelps you a lot: you tend to require updates far less often.\n"
    author: "Anonymous Coward"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "So let's go back to the one supplier world. One platform, and we all wait patiently for them to get interested in fixing problems.\n\nA few years ago there was the 'I Love You' email virus. It was a Microsoft issue. The real problem wasn't the bug, but the fact that there wasn't any viable alternative to use. So everyone waited for Microsoft to fix the problems all the while hoping the next wave wouldn't be as bad. Note the huge difference between this situation and web servers where there are viable alternatives.\n\nThat is no longer the case. There are alternative solutions available. If one has a serious issue, users can albeit with some expense change. This forces everyone to be serious about providing decent products.\n\nThere is no magic. We (the company I work for) purposely maintain active accounts and relationships with multiple suppliers. If one does something stupid, we have the choice of not using them. That option is worth the extra costs of maintaining the various accounts.\n\nTo suggest that Microsoft's http stack is more stable and needing updates less often is disingenuous. People who haven't used Microsoft's http stack for the last 4 years or so have essentially been unaffected by the security problems. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "\n> So let's go back to the one supplier world. One platform, and we all wait\n> patiently for them to get interested in fixing problems.\n\nThat's not quite what i was suggesting :/. Linux is getting *incredibly* bloated due to lack of integration. We seem to live in a NIH (Not Invented Here) world. Now, this is _bad_. Moreover, huge sack of code implies more bugs. And it's not just the applications, i've never had so many problems with Linux kernel before. Okay, architecture is way better than it ever was, but a) complexity of the code has increased dramatically, and b) kernel is changing so fast that it's just impossible to keep up with it. I feel bad for people that try to turn this thing into a product. Good luck fellas, you need it.\n\nWhat to do about this? Have no idea. Possibly we should start focusing *big time* on the integration and bug hunt now. And by us, i mean *everyone*.\n\n\n> That is no longer the case. There are alternative solutions available. If \n> one has a serious issue, users can albeit with some expense change. This\n> forces everyone to be serious about providing decent products.\n\nYou're talking about the techies now. Corporations and 'regular people' will not change anything. And you very well know the reasons why. Good that techies\nhave the options though. Besides, even some of us techies tend to get overly \naddicted to our usage patterns and we won't change either. And if you actually\nend up developing technology Z that competes with X, almost no reasoning can \never convince you that X is actually a better thing.\n\nBut you're right about the benefits of the competition though.\n\n\n> There is no magic. We (the company I work for) purposely maintain active\n> accounts and relationships with multiple suppliers. If one does something\n> stupid, we have the choice of not using them. That option is worth the extra\n> costs of maintaining the various accounts\n\nI sure wish this was the case for us. Having worked for two _large_\ncorporations, this is never the case. It's the integration and the focus that\nthrives us..\n\n\n> To suggest that Microsoft's http stack is more stable and needing updates\n> less often is disingenuous. People who haven't used Microsoft's http stack\n> for the last 4 years or so have essentially been unaffected by the security \n> problems. \n\nSure. On the other hand, it's far easier to make one stack work right than a\ndozen of them.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: World of Wonders"
    date: 2005-08-01
    body: "> That's not quite what i was suggesting :/. Linux is getting *incredibly* bloated due to lack of integration.\n\nHow ? You can't come up with any example actually.\n\n> We seem to live in a NIH (Not Invented Here) world.\n\n\"We\" as in \"MS astroturfers\" ? I agree with you.\n\n> i've never had so many problems with Linux kernel before. Okay, architecture is way better than it ever was, but a) complexity of the code has increased dramatically, and b) kernel is changing so fast that it's just impossible to keep up with it.\n\nAt least try not contradicting yourself. So you are an expert enough to affirm that the kernel code complexity has increased dramatically. I can't do that. And then you say that it's impossible to keep up with it. I can.\nWhat I don't understand, is how is it possible that you manage to do a thing (parsing kernel code) that takes one or several order of magnitude more than another (installing a kernel) that you can't manage to do.\nWhat's even more amazing, is that you feel a gun on your temple and a voice urging you to keep up with the kernel.\nContrast with me : nobody urges me to keep up with the kernel, but it's so easy I install it eveytime one major one (2.6.X) is out. And amazingly enough, I can still boot an older one, in case the new one does not work. Could you believe that ? (it will look obvious to true Linux users, but I have to state the obvious for astroturfers like you to understand, and even then I doubt you believe this).\n\n> I feel bad for people that try to turn this thing into a product.\n\nLinux is actually a product but you did not notice. I feel really bad for you, if that's all you can come with.\n\n> What to do about this? Have no idea. Possibly we should start focusing *big time* on the integration and bug hunt now. And by us, i mean *everyone*.\n\nNo, you mean astroturfers. Because we (not you, we, the FOSS community) are already doing that. But you couldn't know, you are not part of us.\n\n> Corporations and 'regular people' will not change anything. And you very well know the reasons why.\n\nActually they are changing things. That's why you can't come up with any reasons, all you can do is affirm that others know what you are talking about, as obviously you don't. I can tell you we will not think in your place. Try to come up with an argument at least, you are supposed to be the astroturfer.\n\n> Sure. On the other hand, it's far easier to make one stack work right than a\ndozen of them.\n\nI still don't understand this \"http stack\" thing. It is nonsense to me.\nI don't understand the argument either. Be it server, protocol or client, Linux has several better implementations in all of those, when compared to MS. So what was your point ?"
    author: "Ookaze"
  - subject: "Re: World of Wonders"
    date: 2005-08-01
    body: "\n> > That's not quite what i was suggesting :/. Linux is getting *incredibly*\n> > bloated due to lack of integration.\n> \n> How ? You can't come up with any example actually.\n\n?\n\nLike i said - start from counting http stack implementations. Then figure out just how many common components each Linux desktop could share, but they don't. \n\n \n > At least try not contradicting yourself. So you are an expert enough to\n > affirm that the kernel code complexity has increased dramatically. I can't\n > do that. And then you say that it's impossible to keep up with it. I can.\n\nYou mean you're able to install it? Or to figure out how things work out in the kernel? \n\n\n > What I don't understand, is how is it possible that you manage to do a thing\n > (parsing kernel code) that takes one or several order of magnitude more than\n > another (installing a kernel) that you can't manage to do.\n\nUmm, how did you come up with this? Installing a kernel is peanuts, when it works. Figuring out what breaks up with it is ( occasionally ) whole another story.\n\n\n > What's even more amazing, is that you feel a gun on your temple and a voice\n > urging you to keep up with the kernel.\n\nYep, i do. It's my manager ;). I make my living off it.\n\n\n> > I feel bad for people that try to turn this thing into a product.\n> \n> Linux is actually a product but you did not notice. I feel really bad for\n> you, if that's all you can come with.\n\nMan, you're a one big ugly troll ;). You entirely missed my point. I was just trying to say that amount of regressions is steadily getting worse. Don't believe me? Call RH or SUSE and ask them.\n"
    author: "Anonymous Coward"
  - subject: "Re: World of Wonders"
    date: 2005-08-01
    body: "I figured as much, but this last post really showed you have no idea what you are talking about.\n\n\"Mutiple implementations of http stacks\", are you joking, have you heard that phrase before and thought it would be cool to throw it out there?\n\nMy god, my distro has KDE, Gnome, XFCE, fluxbox, enlightment, I need to install them all! Why do they all implement the same stuff? \n\n"
    author: "A smarter troll"
  - subject: "Re: World of Wonders"
    date: 2005-08-02
    body: "Yes, but i didn't bring that up because classic argument against that is that you don't have to have all window managers & desktops installed. However, you do *always* end up having dozens of similar components lying around your system due to lack of integration in the system. You wouldn't have a working system otherwise."
    author: "Anonymous Coward"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "I believe linus torvalds once compared the situation to that of political parties in a democracy: sure, it would be much easier if all the parties you disagree with would just disappear..\n\nI don't like gnome either. the technology behind it sucks, and I hate it when I can't easily configure the apps I use to death. but since there are people who actually like gnome, why not respect their freedom of choice? "
    author: "mark dufour"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "well, i just read http://www.gnome.org/~davyd/gnome-2-12/ and its a nice article. they seem to be progressing nice. a few things KDE doesn't have, a few things KDE already has - but overall, looks nice. still, i wonder, what if all this money (check bounty.gnome.org or gnome.org/bounties dunno the link and too lazy to look it up) and time was spend on KDE..."
    author: "superstoned"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "Then happens something like this: http://developer.kde.org/summerofcode/"
    author: "Anonymous"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "I kind of liked the preview when dragging text feature, it's nice. Does KDE have something similar, with preview when drag and drop? Lots of times when when thinking about ways to do stuff or features I want, I find it's already implemented in KDE and only needs activating/configuring."
    author: "Morty"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "i dunno if its here, but i'd wait for KDE 4 anyway, as i guess it'll be better to implement it then... guess it'll be easier with Qt 4."
    author: "superstoned"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: ">  GNOME which came up one year earlier than KDE\n\njust to correct this for honest, regular visitors who may not know the history of things: KDE was started 8 months before GNOME and had a stable release out quite some time before GNOME had a stable (e.g. not their 1.0) release."
    author: "Aaron J. Seigo"
  - subject: "Re: World of Wonders"
    date: 2005-08-01
    body: "Also, if you have a look back at the mailing list postings at the time KDE was conceived, various people from the FSF and who would become Gnome developers, were keen to desperately (and I mean desperately) get involved with KDE - for whatever reason. Maybe they felt they had missed the boat.\n\nOf course, the 'licensing issues' gave them a nice reason to start their own pet project in a classic dose of NIH syndrome. I'm quite glad they did, because it meant that KDE could keep away from the political bollocks that's been never ending over the years. If there weren't 'licensing issues' in their eyes then there would have been some other reason they couldn't use and work on KDE or they would have been desperately keen to hijack the project for their own ends."
    author: "David"
  - subject: "Re: World of Wonders"
    date: 2005-07-31
    body: "Looks like there is still work to do to make AI robots really look like human beings on the net. :-)"
    author: "Richard Van Den Boom"
  - subject: "Re: World of Wonders"
    date: 2005-08-01
    body: "Ok i waited for deb and doesn't work for a work of the human body."
    author: "amaroK"
---
Some highlights from <a href="http://commit-digest.org/?issue=jul292005">this  week's KDE Commit-Digest</a>
(<a href="http://commit-digest.org/?issue=jul292005&all">all in one page</a>):

<a href="http://digikam.sourceforge.net/">DigiKam</a> adds an image editor plugin to remove Hot Pixels' on photographs.
<a href=" http://www.koffice.org/krita/">Krita</a> adds an OpenEXR import filter and adds support for working with high dynamic range images such as 32-bit floating point RGBA colourspace.
<a href="http://www.koffice.org/kspread/">KSpread</a> gets a new function manager and repository (a <a href="http://developer.kde.org/summerofcode/">Google SoC</a> project).
Allow setting the wallpaper via DnD, even when icons on desktop are
disabled.
Media kioslave implements the autostart of application after mount.
<a href="http://kmail.kde.org/">KMail</a> now has Online/Offline status.
<a href="http://amarok.kde.org/">amaroK</a> adds podcast support within the playlist browser.


<!--break-->
