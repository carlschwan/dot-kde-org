---
title: "Nokia Bases Mobile Web Browser on KDE Technology"
date:    2005-11-02
authors:
  - "tchance"
slug:    nokia-bases-mobile-web-browser-kde-technology
comments:
  - subject: "CSS"
    date: 2005-11-02
    body: "I hope they will improve the CSS rendering part and give it back to KDE :)"
    author: "JC"
  - subject: "Re: CSS"
    date: 2005-11-02
    body: "But you now the newest KDE Version will support some of these css improvements?\n\nCheck http://liquidat.blogspot.com/2005/08/kde-35-alpha-1.html , the first given picture speaks for itself.\n\nliquidat"
    author: "liquidat"
  - subject: "Re: CSS"
    date: 2005-11-03
    body: "I think he doesn't now, he laters instead."
    author: "Jeve Stobs"
  - subject: "Is it then KHTML or WebCore?"
    date: 2005-11-02
    body: "this: \"The browser uses the KHTML and KJS rendering engines\"\npress.nokia.com: \"the new Web browser for S60 is based on the WebCore and JavaScriptCore components of Apple's Safari Web Kit\"\n\nThis is confusing. I know that WebCore is derivative of KHTML, I just want to know what exactly they use (possibly as base for their own fork)."
    author: "Boris"
  - subject: "Re: Is it then KHTML or WebCore?"
    date: 2005-11-02
    body: "as they're probably not using qt (but gtk --yak!) - i assume they chose webcore cause the qt deps are already stripped from it"
    author: "Nick"
  - subject: "Re: Is it then KHTML or WebCore?"
    date: 2005-11-02
    body: "This is a phone using the Symbian operating system, so it probably doesn't use either GTK or Qt."
    author: "rqosa"
  - subject: "Re: Is it then KHTML or WebCore?"
    date: 2005-11-21
    body: "From Nokias page:\n\"The WebCore and JavaScriptCore elements are from Apple Computer, which uses them in its popular Safari browser, and are covered by the LGPL open source license. These components in turn are based on KHTML and KJS from KDE's Konqueror open source project.\"\n\nFor more information, see:\nhttp://opensource.nokia.com/projects/S60browser/index.html\n\n-- \nPauli\n"
    author: "Pauli"
  - subject: "Not really good!"
    date: 2005-11-02
    body: "My less then a year old/young Nokia 6630 will not be supported.\n\n1st, Nokia wanted/wants(?) Patents. Thenm they use OpenSource. And 3rd, Nokia doesn't take care and support their customers.\n\nThis is today the day Nokia died...\n"
    author: "anonymous"
  - subject: "Re: Not really good!"
    date: 2005-11-03
    body: "dramatic much?"
    author: "MamiyaOtaru"
  - subject: "what this means.."
    date: 2005-11-03
    body: "Is that whithin few years khtml can take on MSIE on market share.\n"
    author: "Anonymous"
  - subject: "Re: what this means.."
    date: 2005-11-04
    body: "This will only happen if there comes an windows version of Konq. \nAnd then it will first take away firefox share, and noly then nibble on the base of IE. Years, if not more, I fear."
    author: "Ber"
---
Nokia has <a href="http://press.nokia.com/PR/200511/1019239_5.html">unveiled their new web browser</a> for its mobile phones, based upon 
the KDE Project's open source technology. The browser uses the <a href="http://www.konqueror.org/features/browser.php">KHTML and 
KJS rendering engines</a>, also used by Apple in their <a href="http://developer.apple.com/darwin/projects/webcore">Safari browser</a>, to provide a fast and powerful web browser. The move follows <a href="http://dot.kde.org/1129483687">a presentation</a> given by Nokia engineers to the annual KDE conference, aKademy.





<!--break-->
<p>George Staikos, the representative for the KDE Project in North America, said "The KDE Project is excited that Nokia will bring KDE's award-winning open source technology to mobile devices through the S60 platform. It is a testament to the value of the Free Software community's work, and will stimulate further innovation in KHTML and mobile applications. We look forward to helping this development through further collaboration with Nokia."</p>




