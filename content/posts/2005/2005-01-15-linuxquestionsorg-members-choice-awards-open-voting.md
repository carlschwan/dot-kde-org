---
title: "LinuxQuestions.org Members Choice Awards Open for Voting"
date:    2005-01-15
authors:
  - "jriddell"
slug:    linuxquestionsorg-members-choice-awards-open-voting
comments:
  - subject: "Hmmm....."
    date: 2005-01-14
    body: "KPlayer is not even on the list. Pretty pathetic. Oh well, voted for MPlayer instead."
    author: "KPlayer user"
  - subject: "Re: Hmmm....."
    date: 2005-01-15
    body: "Why pathetic? Kplayer is just a frontend, like KMplayer and GMplayer."
    author: "John Icons Freak"
  - subject: "Re: Hmmm....."
    date: 2005-01-15
    body: "Right. Just like KDevelop is a frontend for gcc and gdb, K3b is a frontend for cdrecord, KMail is a frontend for POP3, and so on.\n\nJust like those programs, KPlayer is so much more than just an MPlayer frontend. In fact the MPlayer interface is maybe like 10% of it or even less. The other 90% on top of that is what makes it the best media player around: the great GUI, ease of use, and tons of options."
    author: "anonymous"
  - subject: "Re: Hmmm....."
    date: 2005-01-16
    body: "Yes, really.  The most brilliantly designed gui for \na media player.  Every option is easily accessible, yet \nnot congesting the interface.  The only additions required\nare a few more short-cut buttons for playing vcd etc.(it\nis understandable to write mplayer vcd;//1, but to do the \nsame in gui is not good) and better playlist to organize\nmp3's vedios etc.\n\nthanks for the good work"
    author: "Ask"
  - subject: "no polls"
    date: 2005-01-14
    body: "How do you vote ?\nI can't see any polls"
    author: "JC"
  - subject: "Re: no polls"
    date: 2005-01-14
    body: "You must register first."
    author: "Fredrik Edemar"
  - subject: "Re: no polls"
    date: 2005-01-14
    body: "ahaha. I know that.\nI've done my registration for a few years now."
    author: "JC"
  - subject: "Just voted ;-D"
    date: 2005-01-14
    body: "Basically I voted for all KDE stuff, since that's all I use.  Thanks KDE developers for all the tools nessesary for a full desktop\n\n-Sam"
    author: "Sam Weber"
  - subject: "Winner last year"
    date: 2005-01-15
    body: "FYI Quanta won it's category last year, but given various polls and where KDE typically rates it appears the reader base is skewed to less than one might expect for KDE applications. Two years ago Bluefish edged out Quanta and last year Quanta won, but not decisively. I don't know how you would get accurate user counts but I was surprised how close it was. Maybe a lot of our users have better things to do than online polls or have less questions about Linux because KDE works so well. ;-) Whatever the case I have a nice little certificate hanging by my desk for the 2003 \"Web Development Editor of the Year\". I have to admit, I'm very proud of it... and I think it's lonely. ;-)\n\nBest of luck to all KDE applications this year. Please vote, because it really does mean something to developers to get recognized by users. In fact that may be central to why a lot of us do what we do."
    author: "Eric Laffoon"
  - subject: "Bluefish went 1.0"
    date: 2005-01-15
    body: "With Bluefish's 1.0 release (which generated publicity and gave the project more momontum) and their professional website, I think Quanta will have an even more difficult time winning.\n\nBluefish clearly has better marketing and that adds up to a lot when it comes to popularity. Remember BetaMAX?"
    author: "Vote"
  - subject: "Re: Bluefish went 1.0"
    date: 2005-01-15
    body: "What momentum and marketing are you talking about? Whit a quick search, I could not find anything on any major Linux site about the 1.0 release. Pclinuxonline had a story, but that site is not a major news site. And freshmeat of course, but everything under the sun gets announced there. Other than that, I found it mentioned in a Release Digest on linuxtoday. Leaving only Gnome sites, so I would say it don't generate much publicity.\n\nActually I think Eric is right in pointing out that the reader base on LinuxQuestions.org is skewed, in all other polls I have seen Quanta wins with huge margins.\n\nI will admit their website looks better than Quanta's, but this is being worked on. The new design for KDE Web Dev site looks very slick. "
    author: "Morty"
  - subject: "Sorry for konqueror"
    date: 2005-01-15
    body: "I am sorry for the konqueror guys. I use it. I love it. You make a great work. But firefox is the browser of the year. No way..."
    author: "peroxid"
---
<a href="http://www.linuxquestions.org/">LinuxQuestions.org</a> has opened voting for their <a href="http://www.linuxquestions.org/questions/forumdisplay.php?s=&forumid=62">2004 Members Choice Awards</a>, make sure you vote for your favourite applications. Various KDE and related applications are in the nominations including Kopete, amaroK, KDevelop, Konqueror and of course KDE itself.


<!--break-->
