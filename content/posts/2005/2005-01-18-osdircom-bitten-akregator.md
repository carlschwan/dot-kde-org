---
title: "OSDir.com: Bitten By the aKregator"
date:    2005-01-18
authors:
  - "jriddell"
slug:    osdircom-bitten-akregator
comments:
  - subject: "woooow"
    date: 2005-01-18
    body: "Kontact promises to be again a new killer app with such improvements!"
    author: "Davide Ferrari"
  - subject: "Re: woooow"
    date: 2005-01-18
    body: "kontact ? wrong article ?"
    author: "undo"
  - subject: "Re: woooow"
    date: 2005-01-18
    body: "Akregator can be embedded as a kpart inside kontact."
    author: "ac"
  - subject: "Re: woooow"
    date: 2005-01-18
    body: "akregator is part of kdepim (and kontact)"
    author: "anon"
  - subject: "Lacking Konq-features"
    date: 2005-01-18
    body: "It works nicely, but I quite frequently wish for Konqueror-type features (Address bar, etc)."
    author: "Luke-Jr"
  - subject: "Re: Lacking Konq-features"
    date: 2005-01-19
    body: "And to obey Konq setting such as minimum font size, etc. I find I often have to open article externally if i want to read more than a few lines. It's a great program though. Really such a simple and straight forward interface... one wonders why it took KDE so long to get a *useful* RSS user interface. Good to see Akregator is getting a lot of attention these days (and was sucked into pim)... things can only get better."
    author: "Tim Middleton"
  - subject: "Re: Lacking Konq-features"
    date: 2005-01-20
    body: "> And to obey Konq setting such as minimum font size, etc\n\nalready in cvs"
    author: "anon"
  - subject: "Re: Lacking Konq-features"
    date: 2005-01-20
    body: "http://bugs.kde.org/show_bug.cgi?id=84835"
    author: "anon"
  - subject: "Remove RSS-code from newsticker?"
    date: 2005-01-19
    body: "Since aKregator is stable now, maybe it is a good idea to remove all RSS-related code from newsticker and let newsticker be what it is; a ticker.\n\naKregator could push news-items via DCOP to the newsticker. Moreover these news-items can even be removed after they have been read in aKregator. Again via DCOP. This interface can even allow other applications to post news-items to the newsticker. I can think of Kmail for new messages, Knode for new articles or Korganizer for events and tasks.\n\n- Robert"
    author: "Robert Schouwenburg"
  - subject: "Re: Remove RSS-code from newsticker?"
    date: 2005-01-19
    body: "\"aKregator could push news-items via DCOP to the newsticker. Moreover these news-items can even be removed after they have been read in aKregator. Again via DCOP. This interface can even allow other applications to post news-items to the newsticker. I can think of Kmail for new messages, Knode for new articles or Korganizer for events and tasks.\"\n\nhmmmm.... I find your ideas intriguing, and I wish to subscribe to your newsletter...\n\n:)"
    author: "Janne"
  - subject: "question.."
    date: 2005-01-20
    body: "Does browsing in the aKregator also obey your java, cookie etc. settings?"
    author: "ac"
  - subject: "aKregator and feeds."
    date: 2005-01-23
    body: "aKregator will not work with the following feed: http://forums.suselinuxsupport.de/rssfeed/activeposts.xml\n\nSage and other readers work fine and the feed is validated.  I attempted to post a message to their mailing list and got a bounce message about my gmail.com account not being a valid domain...not that impressed.  Went to using sage with firefox."
    author: "famewolf"
---
In <a href="http://osdir.com/slash3563.html">"Bitten By the aKregator"</a> George Staikos reviews KDE's new RSS feed reader <a href="http://akregator.sourceforge.net/">aKregator</a>: "<em>The power of KDE strikes again! aKregator is little more than a repackaging of KHTML and KDE's RSS parser in a more convenient form for reading news, and that's what makes it so nice.</em>"



<!--break-->
