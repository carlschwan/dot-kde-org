---
title: "KDE to Preview KDE 3.4 at Chemnitzer Linux-Tage"
date:    2005-03-04
authors:
  - "dmolkentin"
slug:    kde-preview-kde-34-chemnitzer-linux-tage
comments:
  - subject: "KDE 3.4rc1 rocks on Slackware 10.1 (klax packages)"
    date: 2005-03-04
    body: "I downloaded and installed klax's packages for slackware 10.1 from \n\nhttp://ktown.kde.org/~binner/klax/10.1/\n\nI am so happy to see KDE 3.4 will be much better than 3.3. Even my settings were not messed up due to the upgrade. and 3.3 packages works well with 3.4. \n\nThanks Klax people and KDE of course."
    author: "Fast_Rizwaan"
  - subject: "FreeNX and thin clients"
    date: 2005-03-04
    body: "First of all: Good luck in Chemnitz!!!!\n\nI do understand that FreeNX is not about thin clients. Still, a TC setup could benefit greatly from FreeNX I think. Apart from TCs being far to expensive compared to entrance-level PCs (yes, I know about LTSP, I am talking about \"real\" TCs), there is one technical issue with them and X: The sheer amount of (unnecessary) X traffic. NX or FreeNX could help ease that issue in large settings.\n\nHas anybody done it? Any links, docs, HOWTOs,...?\n\nCheers\nUwe\n"
    author: "Uwe Thiem"
  - subject: "Re: FreeNX and thin clients"
    date: 2005-03-05
    body: "As far as I understand, FreeNX is about thin client also, why not?\nHave a look at nomachine site for a wide spectrum of clients for NX server, or have a look at:\nhttp://pxes.sourceforge.net/\nthat can be used to setup a TC that connects with FreeNX, ltsp, VNC, etc.\nBest regards\n"
    author: "Marco Menardi"
---
The KDE tradeshow team will be previewing KDE 3.4 at this year's <a href="http://chemnitzer.linux-tage.de/2005/info/">Chemnitzer Linux-Tage</a>, coming up this weekend. Based on the recent <a href="http://dot.kde.org/1109582845/">Release Candidate 1</a>, the team will present the features of the new version at the KDE booth, which is <a href="http://chemnitzer.linux-tage.de/2005/live/plan.html#EG">prominently located</a> in the front of the exhibition area. 
Talks include Kurt Pfeifle elaborating on KDE <a href="http://chemnitzer.linux-tage.de/2005/vortraege/detail.html?idx=145">Tips and Tricks</a>  and Martin Loschwitz (of Debian fame) giving an insight on <a href="http://chemnitzer.linux-tage.de/2005/vortraege/detail.html?idx=33">Kalyxo, the Debian KDE efforts</a>. Kurt Pfeifle's <a href="http://chemnitzer.linux-tage.de/2005/tutorials/detail.html?idx=147">FreeNX workshop</a> is already sold out. See you in Chemnitz! <b>Update:</b> All talks will be available as <a href="http://chemnitzer.linux-tage.de/2005/info/news/streamsonline.html">live streams</a>.







<!--break-->
