---
title: "People Behind KDE: Thiago Macieira"
date:    2005-07-25
authors:
  - "jriddell"
slug:    people-behind-kde-thiago-macieira
comments:
  - subject: "Silly bug reports"
    date: 2005-07-25
    body: "I don't think Thiago should worry about silly bug reports, our first ones ALWAYS are horrible.  I think my first one invovlved not having shutdown on kdm (or prompting for the root password before you shut down) because someone sitting at the box could have a boot floppy and root your machine on reboot. \n\nMy excuse now is that I was 16 at the time :-P"
    author: "Hoyt Pittman"
  - subject: "Re: Silly bug reports"
    date: 2005-07-25
    body: "Now you get me to tell you your bug reports are incomplete. :-)"
    author: "Thiago Macieira"
  - subject: "Re: Silly bug reports"
    date: 2006-02-06
    body: "I'm a silly person, i want to know what is bugging you I loved this. i hope you contact me soon\n\nIza\n"
    author: "eu"
  - subject: "Thiago rocks"
    date: 2005-07-25
    body: "Thiago & Matt Rogers (mattr) answered all my dumb questions when I ported Konversation to KNetwork. You guys rock :-)\n\n"
    author: "cartman"
  - subject: "Lil' bug report..."
    date: 2005-07-25
    body: "Nice interview1 but toward the end the question starting with \"You're stuck on a train for 6 hours...\" isn't bold like the others are which may be confusing for some readers. Cheers."
    author: "ac"
  - subject: "Answer"
    date: 2005-07-25
    body: "...Starfleet ship names"
    author: "Mister-Know-It-All"
  - subject: "Re: Answer"
    date: 2005-07-25
    body: "Not quite. It's more specific than that."
    author: "Thiago Macieira"
  - subject: "Re: Answer"
    date: 2005-07-25
    body: "Star Trek ship *classes*"
    author: "Craig Poland"
  - subject: "Re: Answer"
    date: 2005-07-25
    body: "I'd say ship classes involved in the defense fleet against the Borg attack in \"First Contact\" :)"
    author: "Kevin Krammer"
  - subject: "Re: Answer"
    date: 2005-07-25
    body: "You're too far now. Craig had it right: starship classes.\n\nSo, what should I name the next computer?"
    author: "Thiago Macieira"
  - subject: "question"
    date: 2005-07-25
    body: "> You still do what.. like Keramik or have eyesight problems?\n\nOh man, that gave me a good laugh. =)"
    author: "KDE User"
  - subject: "Only \"trunk\""
    date: 2005-07-26
    body: "IMHO this is a mistake which many developers make.\n\nI keep a current copy of the release branch which I try to update every day.\n\nI also keep a copy of trunk but I haven't been able to get it to build for at leaset a week.\n\nAnd, yes I do have eyesight problems. (Don't like Keramik"
    author: "James Richard Tyrer"
  - subject: "Re: Only \"trunk\""
    date: 2005-07-26
    body: "Thanks to Kurt Pfeifle, I have access to a SuSE machine with KDE 3.4.1, via NX.\n\nThat way, I can test hairy bug reports against the stable branch."
    author: "Thiago Macieira"
  - subject: "cheers"
    date: 2005-07-26
    body: "Appart from being a very talented developer he is a nice and friendly person. He takes lot of time teaching how stuff works to other people and I have learned lot of interesting stuff thanks to him. Very nice interview.\nI can't wait to meet you in person some day!\n\n"
    author: "Duncan Mac-Vicar"
  - subject: "unable to use * aterisk key in konsole"
    date: 2006-11-14
    body: "Hi\ni have seen some other people had the same problem as me\nbut I could not understand the solution. I am referring\nto bugs 111034 and 95977 in http://lists.kde.org/?l=konsole-devel&m=112467223230923&w=2\n\nI cannot type the * key in my konsole and instead I \nshift to the next session.\nI understand I need to paste the output of \"xev\" when \nI type the * key, here it is \n\n\nKeyRelease event, serial 20, synthetic NO, window 0x1e00001,\n    root 0x57, subw 0x0, time 87222980, (453,303), root:(453,347),\n    state 0x1, keycode 36 (keysym 0x2a, asterisk), same_screen YES,\n    XLookupString gives 1 bytes:  \"*\"\n\ncan you please help me ???\n\nthank you very much!!\n\n giulia \n"
    author: "giulia manca"
---
The second in the new series of <a href="http://www.kde.nl/people/">People Behind KDE</a> brings us <a href="http://www.kde.nl/people/thiago.html">Thiago Macieira</a>.  Thiago is a Brazilian who spends his time reading the kde-bugs-dist mailing list.  Somehow he also finds time to look after the networking code in kdelibs and his dog Kayla.  He also tells us why he has two clocks and includes a Unix story in Old English.
<!--break-->
