---
title: "amaroK 1.3 Reviews"
date:    2005-12-25
authors:
  - "tteam"
slug:    amarok-13-reviews
comments:
  - subject: "yeah"
    date: 2005-12-25
    body: "i read it. well, it seemed obvious to me the author did the best he could not to like (the kde app) amarok, but the competition sucked so hard he had no choice ;-)"
    author: "superstoned"
  - subject: "Re: yeah"
    date: 2005-12-25
    body: "Years ago everyone hyped the hell out of Rhythmbox to be the best show that GNOME had to offer and then internal disagreement made the development came to an halt. Rhythmbox was nowhere usable, it was and still is just a simple window infront of a playing engine where you can click 'play' and 'stop'. No real feedback to the user was given, what's playing, duration and so on. Not to mention that Rhythmbox was by far to immature to be anything worth while.\n\nThen from one day to another everyone came up with his own Rhythmbox like player in GNOME (as it is a common thing that as soon someone starts something, everyone else starts to clone the thing or do the same). Now there is Banshee, Muine, Rhythmbox, Net-Rhythmbox and even that thing from Immendio and all the other players that exists.\n\nGNOME ends up in having half a dozen half finished media players.\n\nThen one day the amaroK people have shown up, totally focusing and concentrating on creating a good media player for KDE (imo the best one existing so far) and it has passed all the crap that GNOME initially wrote.\n\nAnd still people do mention RB and Banshee (MONO) to compare with things like amaroK. Imo even XMMS is far ahead of RB in many areas."
    author: "aa"
  - subject: "Re: yeah"
    date: 2005-12-25
    body: "not to forget there is another KDE player for those that prefer a very clean interface: Juk. a shame it wasn't tested, as gnome users often like it more than amarok."
    author: "superstoned"
  - subject: "Re: yeah"
    date: 2005-12-25
    body: "Hey, it's not just Gnome users that favor Juk, I love Juk because it does one thing very well, play music.  You start Juk up and within seconds you understand the interface, there was almost zero learning curve for me.  I never feel comfortable using Amarok.  Regardless, congrats to the Amarok team."
    author: "bozy"
  - subject: "Re: yeah"
    date: 2005-12-25
    body: "And amaroK devs like JuK since sometimes we get wish requests that basically translate to \"make amaroK like JuK\", to which we get to respond \"well, use JuK\". :)\n\nYea, it is odd that JuK wasn't reviewed."
    author: "Ian Monroe"
  - subject: "Re: yeah"
    date: 2005-12-28
    body: "Yes, and look like asses when you do it. \n\nA few SIMPLE options would work wonders in making it POSSIBLE to look/work more like Juk, but Amarok devs love to be adorable and have their little competition..."
    author: "Joe"
  - subject: "Re: yeah"
    date: 2005-12-28
    body: "can you be a bit more specific? (i'm no amarok dev, just wondering)"
    author: "superstoned"
  - subject: "Re: yeah"
    date: 2005-12-28
    body: "One possible option that comes to mind is a \"Embed JuK KPart\" checkbox in amaroK."
    author: "ac"
  - subject: "Re: yeah"
    date: 2005-12-29
    body: "At what's the advantage of embedding the JuK KPart in amaroK over just using JuK from the beginning???"
    author: "ac"
  - subject: "Re: yeah"
    date: 2005-12-25
    body: "Other way around for me ;) Never got the hang of how Juk works, so I tried amaroK and never looked back :) And that is despite the relatively common crashes I experience... :("
    author: "Jo \u00d8iongen"
  - subject: "Re: yeah"
    date: 2005-12-26
    body: "There's code of mine in JuK (or at least, used to be), and I prefer Amarok!"
    author: "GldnBlls"
  - subject: "Re: yeah"
    date: 2005-12-26
    body: "don' forget noatun??\n\nWhat annoys me the most is that I play song a in player a and then start a song b which is then opened in player b just because it is a different format and player b is the default player, but player a does not stop to play song a but. So I have to listen to a song a and b mix."
    author: "manes"
  - subject: "Re: yeah"
    date: 2005-12-26
    body: "That should be easily fixed by changing the affiliation of the media formats to your favorite player :)\n\nOTOH, it is confusing with all the different media engines (gstreamer, arts, xine, etc.). I hope one of them will end up becomming truly great, stable and standard in KDE and elsewhere. "
    author: "Joergen Ramskov"
  - subject: "Re: yeah"
    date: 2006-01-05
    body: "Unless I've done something seriously wrong to retard its functionality (and I haven't altered it at all) NoAtun might be better forgotten.  Every time it opened (it was the default player for most video formats in my distro) there were issues that caused me to kill it immediately:\n  \n1)The video was jumping like a kid who's just realized the powdered sugar on his doughnut is cocaine. \n\n2)The audio wasn't even vaguely synced with the video.\n\n3)Somewhere in the combination of bad video and bad audio it lagged my system so that closing the windows (at least 4 of them, there's another thing!) took much longer than it should have.\n\n(Once its' libraries are updated from their crippled SuSE defaults) I'll take Xine any day. "
    author: "Chester"
  - subject: "Re: yeah"
    date: 2005-12-25
    body: "Quod Libet(http://www.sacredchao.net/quodlibet) kicks Banshee, Muine and Rhythmbox, but Amarok IS f**king perfect. :)"
    author: "Heraze"
  - subject: "Not so fast!"
    date: 2005-12-25
    body: "While AmaroK might be almost flawless to some folks, I find that those that decide to include GStreamer as the default engine with its limitations, do AmaroK nothing good. You might ask; why? Because GStreamer does not play streaming media on all systems I have tried, and it's also slow. (A bug report was filed long ago).\n\nWhen one attempts to play any streaming media with GStreamer as the backend, AmaroK buffers up to 100% and then re-buffers on and on and on. It never stops! To get around this, I have always downloaded and installed Xine from source, then compiled and installed Amarok from source too. This way, Amarok recognizes the xine engine and enables me to play streaming media like radio stations.\n\nWhy won't they include xine too? I always ask myself."
    author: "cb"
  - subject: "Re: Not so fast!"
    date: 2005-12-25
    body: "yeah, i also can't explain why the amarok devs fell in love with gstreamer, it gave me nothing but problems. hell, on systems where xine isn't installed i use the arts backend!! at least it works..."
    author: "ac"
  - subject: "Re: Not so fast!"
    date: 2005-12-25
    body: "The default backend since 1.3 (I think) has been Xine, ever since it gained a stable crossfading capability. (Incidentally, afterwards the GStreamer engine was rewritten without the crossfading capability, as it was causing too many stability issues.)\nMany distros (Kubuntu e.g., don't know about the rest) set GStreamer as the default, however.\n\n(I don't think there's that big of a difference between the two, myself -- Xine has crossfading, very slightly lower CPU usage, and skips a bit less under high load, but otherwise they both work pretty solidly these days, in my experience.)"
    author: "Illissius"
  - subject: "Re: Not so fast!"
    date: 2005-12-25
    body: "Not only streaming media, when I installed amaroK (don't know if it was 1.2.x or already 1.3.x) it was obviously defaulting to gstreamer. I realized that soon when I played certain mp3 files (nothing special about them) and it sounded as if gstreamer was eating them, while munching and burping. Occured on several different mp3s. I didn't want to test them all through, so I just changed to Xine which I knew was working previously and it had no problems with these files. So I'm glad there's stuff out there which is actually reliable."
    author: "Phase II"
  - subject: "Re: Not so fast!"
    date: 2005-12-26
    body: "GStreamer is constantly crashing here. It's instable, not reliable, broken."
    author: "x"
  - subject: "Re: Not so fast!"
    date: 2005-12-26
    body: "Read Illusius message above : your (and my) distro is broken. They patch amarok to use gstreamer by default."
    author: "yoho"
  - subject: "Re: Not so fast!"
    date: 2005-12-26
    body: "no, it's not my distro that is broken. it's gstreamer that is broken. it always has been and always stay so."
    author: "xx"
  - subject: "Re: Not so fast!"
    date: 2005-12-26
    body: "Isn't that a bit harsh? A new release of gstreamer has just arrived and according to what I've read, it should be a pretty massive improvement over the previous version.\n\nNo matter what, saying that it will always be broken is quite lame IMHO."
    author: "Joergen Ramskov"
  - subject: "Re: Not so fast!"
    date: 2005-12-26
    body: "i think that after more than 6 years of development, one could lose its faith, don't you think?"
    author: "ac"
  - subject: "Re: Not so fast!"
    date: 2005-12-27
    body: "Not really, specially since you never tried 0.10 and you probably never worked on free software for 6 years."
    author: "Reply"
  - subject: "Re: Not so fast!"
    date: 2005-12-27
    body: "Gstreamer is buggy, unstable, and a solution looking for a problem.\n\nKDE has always done well by choosing the best tools for the job. In this case, Xine comprehensively beats Gstreamer in terms of robustness, speed and effectiveness. That is why Xine should be the default media backend for KDE4.\n\nIf the GNOME'rs have the same sense, they too will default to Xine... hell, it seems like most of their users are, if not the developers :)"
    author: "anon"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "blabla \"should be the default media backend for KDE4.\"\n\nThere won't be a thing such as a \"default\" media backend for KDE 4 regardless of how long people keep rambling about this stuff (there was a lesson to be learned about the current aRTs fiasco after all)."
    author: "ac"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "Obviously you have no clue what \"default\" means. \n\nSo, your solution is to have...no default? A blank screen? A user saying \"WTF?\""
    author: "Joe"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "Exactly, KDE 4 will be compilable without any media backend as well (something which with KDE 3.x isn't possible due to the hard dependency on aRTs in kdelibs even if you don't intend to actually use it at all).\n\nGet your clue digging somewhere else, please."
    author: "ac"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "Eh, what are you ranting on about?\n\nSince when does \"default media backend\" conflict with \"compilable without any media backend as well\"?\n\n--disable-media-engine\n\n--enable-media-engine (defaults to Xine)\n\nIdeally, the engine would be abstracted out, so that users can switch at runtime easily, like AmaroK. Thus if Xine was suddenly surpassed by something else, KDE4 would not be hampered at all.\n\nBut at the moment, Xine is by far the best and should be preferred over Gstreamer, NMM or anything else."
    author: "anon"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "amarok in kde 4 will not just default to, but most probably ONLY support KDEMM. And kdeMM in turn will support every backend you want. but the choice will be out of amarok's hands, and most probably global for KDE."
    author: "superstoned"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "You can configure each module of KDE3 with --without-arts and you do not get the depedency anymore.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Not so fast!"
    date: 2005-12-26
    body: "If your distro patches amaroK to use by default a (suposedly, I haven't used it yet) broken sound engine, then your distro is broken too. Bug them to use xine engine by default, so it doesn't give amaroK a bad name."
    author: "Henrique"
  - subject: "Re: Not so fast!"
    date: 2005-12-28
    body: "Bad experience with gstreamer on FreeBSD as well - and they don't patch it to be the default. Lack of crossfading is one thing, but it is also unstable. Xine is OK, but I experience occasional crashes (hardly reproducible, for they occur randomly). So far, the most stable backend for me was arts, and it supports crossfading (and that's all I need). "
    author: "molnarcs"
  - subject: "Re: yeah"
    date: 2006-01-07
    body: "I like Kaffeine personally.  Easy to use and works as an all around media player.  AmaroK doesn't work half the time."
    author: "Brad"
  - subject: "Re: yeah"
    date: 2005-12-26
    body: "That is hardly surprising considering it is \"the grumpy editors guide\" ;)\n\nLwn.net is IMHO one of the best Linux/open source magazine that exists. Week after week Corbet produces new articles of very high quality and I highly recommend becomming a subscriber if you want to follow what's going in the kernel land, his coverage is quite impressive.\n\nHappy new year :)"
    author: "Joergen Ramskov"
  - subject: "Re: yeah"
    date: 2005-12-29
    body: "> the competition sucked so hard he had no choice ;-)\n\nWrong. The reason he had no choice is that amaroK just kicks ass.\n\nIt's as simple as that. :-)))\n\nI'm already looking forward to amaroK 1.4. :-)\n\nGreetings,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "amaroks"
    date: 2005-12-25
    body: "There are only few areas where iTunes or Kaffeine are better. \n\nLike: \n* Adding new files to the lib via drag and drop\n* burn your playlist like in iTunes\n* podcast abo\n* format conversion to .ogg\n* usability of freedb edit window\n* mpeg 4 audio is always opened with Kaffeine\n* visualisation\n* mp3 player which is mounted as a memory stick is not recognized.\n\n\nAmarok is wonderful, it just needs to catch up.\n\n"
    author: "Bert"
  - subject: "Re: amaroks"
    date: 2005-12-25
    body: "> * Adding new files to the lib via drag and drop\namaroK adds them automatically if the file is in the search path, and I personally don't see a reason why one would have a file in the database which can't be in a search path as well :|\n\n> * burn your playlist like in iTunes\nI don't use iTunes, but it probably doesn't export the playlist for burning to the awesome K3b burning interface with integrated ultra-fantastic media support ;-)\n\n> * podcast abo\nhm, can you describe where iTunes got and advantage there? Could be right, since podcast are meant for iPods->apple->iTunes at their nascency\n\n> * format conversion to .ogg\nfor that we got scripts\n\n> * usability of freedb edit window\nwtf is a \"freedb edit window\"? since when does amaroK use freedb at all? maybe mixed something up?\n\n> * mpeg 4 audio is always opened with Kaffeine\nas long as the file is spec compatible it gets opened with amaroK\n\n> * visualisation\nI totally agree with you\n\n> * mp3 player which is mounted as a memory stick is not recognized.\nwe're working on\n\nBut well, as we see one needs 2 apps to get enough advantages for a post ;-) (don't take this too serious)"
    author: "Harald Sitter"
  - subject: "Re: amaroks"
    date: 2005-12-25
    body: "Just for the record, iTunes co-opted podcasting, they didn't come up with it. :)"
    author: "Ian Monroe"
  - subject: "Re: amaroks"
    date: 2005-12-25
    body: "oh, sry\n\nI wasn't interested until amaroK got a feature for :-P"
    author: "Harald Sitter"
  - subject: "Re: amaroks"
    date: 2005-12-25
    body: "Wth is podcast 'abo'?"
    author: "Ian Monroe"
  - subject: "Re: amaroks"
    date: 2005-12-26
    body: "RSS feed of Mp3, ogg or even pdf/video files.\n\nSounds simple but is soooo great. \n\nThere are plenty really good podcasting services. For instance the chaosradio pod of CCC. Or language courses."
    author: "Malte"
  - subject: "Re: amaroks"
    date: 2005-12-27
    body: "> Or language courses.\n\nSounds interesting.  Could you please provide URLs if you know of any good ones? \n\n"
    author: "cm"
  - subject: "Re: amaroks"
    date: 2005-12-27
    body: "Amarok does of course support podcast RSS feeds (that's kinda the whole point of podcast support). You can subscribe to feeds and let Amarok automatically check for updates.\n\nBest podcast show ever (Amarok team approved):\n\nhttp://audiocrush.us/\n"
    author: "Mark Kretschmann"
  - subject: "CD import"
    date: 2005-12-26
    body: "CD import and burning in iTunes is perfect from a usability level. What I miss the most in Amarok is easy CD import.\n\nAmarok shall better be included as the default player into KDE and also integrate video functions. \n\niTunes is top of the class but Amarok comes close. both have some little usability glitches as any player."
    author: "Karsten"
  - subject: "Re: CD import"
    date: 2005-12-28
    body: "Hi Karsten,\n\nyou might want to check out kaudiocreator which does importing of CDs pretty nicely.  In KDE we have different tools for different tasks so those tools can specialize more.  Maybe Amarok should provide an entry in the menus for ripping the cd if kaudiocreator is available, though...\n\nCheers"
    author: "Thomas Zander"
  - subject: "Re: CD import"
    date: 2005-12-28
    body: "What I don't understand is why kaudiocreator even exists, when you can just browser your audio cd and \"copy\" the ogg or mp3 (or even flac) directory from it via drag and drop. \n\nIronically, I learned of this feature from an OS X user's review of kde (3.3.x I think). This should be in kandalf's tips, or another very prominent place, for you can hardly think of any approach to ripping that would be more user-friendly. Non-technical users have no concepts of various formats (audio, wav, pcm, ogg, whatever), they just want to \"copy\" the mp3 off their audio cd, and that is exactly what konqi supports :)"
    author: "molnarcs"
  - subject: "Re: CD import"
    date: 2005-12-28
    body: "Totally agree ... although if some media player would be able to catalogize and file OGG files into appropriate directories as well, it would be totally awesome (I could do the last one with JuK, but if doesn't rip CD and I use amaroK anyway)."
    author: "Matej"
  - subject: "Re: CD import"
    date: 2005-12-28
    body: ">What I don't understand is why kaudiocreator even exists,\n\nSimple actually, if you try to rip 3-4 CDs in a row you'll understand. You get a much better workflow, plus it's easier to edit the cddb data if needed."
    author: "Morty"
  - subject: "Re: CD import"
    date: 2005-12-30
    body: "Though this feature is nice, it's not really my way of ripping, since with kaudiocreator you just put in the cd, if the cddb demand was succesfull it starts ripping automatically to a file (if you enable this option). The proces is hence more automated and easier to use. Another negativ point for audiocd:/ is that if you have two dvd-drives, and you put in two copy protected cd's you only get to rip one at a time, where with kaudiocdreator you can copy them both.\n\nI don't think it wouldn't be useless to make a pluggin that does the same things that Kaudiocreator does, then you have one app that takes care of all the music. Since they do include some ways to move media files from one place to another on your pc or to an external device it's not that illogic to make the other way around possible as well.\n"
    author: "terracotta"
  - subject: "Re: CD import"
    date: 2006-01-03
    body: "Being a(nother) front-end to cdparanoia, oggenc and so on it's IMO always in the Unix-way. So, integrated ripping in amarok is IMO a needed feature (plus a better support for audio CDs)"
    author: "Davide Ferrari"
  - subject: "amaroK rocks"
    date: 2005-12-26
    body: "I really love amaroK. Hell, I even bought a sweater and donated during the fund drive. I'm really looking forward to 1.4. What I particulary like about amaroK, is crossfading and gapless playback, my/postgresql backend, cover/lyrics/tag fetching, the potential value of dynamic mode (don't really like the current implementation) and its iPod support, of course. Hats off for the amaroK devs!\n\nI also use xine, since arts is unmaintained and comes with overhead (have to have artsdsp running), and gstreamer IS unstable. It could be great, though, given some (actually, a lot of) time."
    author: "Luk van den Borne"
  - subject: "KDE 4"
    date: 2005-12-26
    body: "I hope Amarok will be the *default* player of KDE4 and we will not get 20 other different players but all functionality will get integrated into Amarok.\n\nI.e. when I insert a CD, I want Amarok to play it. KSCD is ugly and I hate it. The freedb is hell and the editor thereof is usability crap."
    author: "Malte"
  - subject: "Re: KDE 4"
    date: 2005-12-26
    body: "I've heard rumours about KDE4 not being distributed in huge packages anymore. This means that you could actually decide yourself what crap will or will not be installed. Or is it too early to say anything useful about it? It might just have been wishful thinking :)\n\nBut I do not think that all functionality should be consolidated into one app. Because there are always different ways of doing things, fortunately. It would be a huge loss of variety if that happens (I can tell you that it won't).\n\nFor instance, what if someone needs a simple app that just plays music, like juk. In that case, amaroK would be overkill (sqlite, postgres, gstreamer, xine, last.fm, lyrics wtf!?). Simple apps do have a merit (contrary to what Linus wants to make us believe :) ), just not for everyone."
    author: "Luk van den Borne"
  - subject: "Re: KDE 4"
    date: 2006-01-01
    body: "On certain distros like gentoo, this already exists.\nOnly want basic KDE desktop and kopete: emerge kdebase-meta kopete"
    author: "m_abs"
  - subject: "Re: KDE 4"
    date: 2006-01-02
    body: "Debian (and Kubuntu) already provides everything from KDE in separate packages."
    author: "Junx"
  - subject: "Re: KDE 4"
    date: 2005-12-29
    body: "i think amarok is overkill for playing a cd , good it can be done , but a really simple gui with kscd is ok and way better for starters..."
    author: "chri"
  - subject: "Re: KDE 4"
    date: 2006-01-09
    body: "I guess I forgot to mention that amaroK couldn't be part of the official KDE packages as the main developer(s) don't want to adhere to the KDE release schedule.  I'm sure that distros like Kubuntu could go ahead and make it the default audio player, but I doubt it'd ever be the KDE official player."
    author: "Junx"
  - subject: "Re: KDE 4"
    date: 2006-01-09
    body: "Isn't this what KDE Extra Gear is for?  It's pointless to make amaroK go by the slowwww KDE release schedule.  Extra Gear is there so that applications can have their own release cycle.\n\nThis doesn't mean KDE can't choose the latest stable amaroK and make it the default.\n\nIt's the best of both worlds."
    author: "KDE User"
  - subject: "my review"
    date: 2005-12-27
    body: "My review:\nSuperb idea, great DB-functionality, very handy lyrics and artis info finding but still somewhat instable (crashes from time to time) and lousy audiocd support (only per audiocd://Wav kio-slave!!)."
    author: "panzi"
  - subject: "DBUS in KDE ?"
    date: 2005-12-27
    body: "According to this link:\n\nhttp://oskuro.net/blog/freesoftware/gnome-2.12-unstable-2005-12-26-21-17\n\nKDE is using DBUS in the next 3.x series ?"
    author: "ac"
  - subject: "Re: DBUS in KDE ?"
    date: 2005-12-27
    body: "That article makes no sense at all... it's hardly coherent and doesn't explain anything at all. I honestly question how you could deduce that the next KDE 3.x is switching to DBUS from that poorly written article."
    author: "KDE User"
  - subject: "Re: DBUS in KDE ?"
    date: 2005-12-27
    body: "That artice is not about KDE. It is a report by a Debian developer about the transition status of some packages. For the little bit that KDE 3 uses dbus, in debian it is in the process of being recompiled against DBUS .60. So the article could not be called poorly written as it is very specific, with a very narrow target market, and it also has nothing to say about KDE and possible DBUS usage."
    author: "AC"
  - subject: "Re: DBUS in KDE ?"
    date: 2005-12-27
    body: "KDE has used DBUS for some time(from about 3.4) in a very limited fashion, and it has been optional not a requirement. Or more precisely KDE can make use of HAL, by using the DBUS/HAL bindings. \n\nIt's used for things like media detection and handling, CDROMS and USB storages etc. For systems whitout HAL/DBUS KDE supply scripts giving more or less the same functionality and user experience. \n\nThis desktopsession<->system usage is quite different from the usage of DCOP, which is desktopsession<->desktopsession. Had not DBUS been regarded as a possible replacement for DCOP in KDE4, I suppose someone would have done a DCOP/HAL binding removing the need for both DCOP and DBUS.\n\n"
    author: "Morty"
  - subject: "Re: DBUS in KDE ?"
    date: 2005-12-29
    body: "There is no \"next 3.x series\". KDE trunk is already 4.0."
    author: "Ian Monroe"
  - subject: "How can I make gstreamer work with amaroK?"
    date: 2005-12-27
    body: "I've heard so many great things about gstreamer, so I'd prefer to use it too.\n\nI'm feeling pretty lame to have to use Xine. Xine never crashed to me, and it plays my music quite nicely (including radio streams and podcasts) though with amaroK.\n\nGstreamer also never crashed for me, though. I can not confirm at all the various statements from above badmouthing gstreamer's stability. But the problem is, gstreamer also never was able to issue even the faintest farting tone from my stereo speakers.\n\nI'm dying to use gstreamer by now. Can't someone in the know post an \"amaroK+gstreamer HOWTO\"?\n\nPlease, please, please... I also want to be part of the audio listening elite using gstreamer!\n"
    author: "ac"
  - subject: "Re: How can I make gstreamer work with amaroK?"
    date: 2005-12-27
    body: "Have you tried configuring the OSS sink instead of the ALSA sink?  Unfortunately ALSA is a bit buggy so you might want to try OSS with GStreamer.  You might also try compiling the latest CVS version.  GStreamer will still skip but at least it will play a little.  I would however advise you to stick to Xine for a better experience.  There is one bug with gstreamer that needs to be fixed so that it properly supports the same hardware/sound drivers as Xine but otherwise it is great!  I can't wait to see this in KDE4. =)"
    author: "helpful"
  - subject: "Re: How can I make gstreamer work with amaroK?"
    date: 2005-12-29
    body: "Dude, use what works not what has more buzzword value.\n\nGstreamer is a powerful framework that is just now reaching 1.0. Xine does what it does quite well - play media files. And thats all amaroK currently needs in a multimedia framework (which is to say, it doesn't need a \"framework\" at all).\n\nSo for a while now Xine has had a high priority then Gstreamer (meaning if you have xine and gstreamer both installed, it defaults to xine). That very well might change in the future though."
    author: "Ian Monroe"
  - subject: "ac"
    date: 2005-12-28
    body: "> I've heard so many great things about gstreamer.\n\nI don't!"
    author: "ac"
  - subject: "Re: ac"
    date: 2005-12-29
    body: "i did,too (very good design) thats what most important"
    author: "chri"
  - subject: "The nr.1 OS player"
    date: 2005-12-29
    body: "amaroK is great. It really is top of the line amongst players on Linux. But I would really like to see some improvemants on playlist handling. iTunes is so wonderful on this that it feels like a downgrade going to amaroK. Same thing with CD ripping & burning. On everything else amaroK 'pwns da haows' :)  Wikipedia, lyrics and cd cover search built in. You have to install several extra programs to get this functionallity in iTunes.\n\nOhh yeah and vizualisation iTunes is also better at. The amaroK team should just find a bunch of artist, pump the full with acid, stuff them in a room with osme really cool music and see what comes out of it  :)\n\nI the amaroK team is successful in removing all the Unix code and thus making it possible to port to windows. amaroK would really be the open source grand champion agains the evil iTunes empire *insert Star Wars Empire music*."
    author: "Brona"
  - subject: "JuK"
    date: 2005-12-29
    body: "JuK is a decent manager.  It has been mostly eclipsed by amaroK but still deserves a mention as something more lightweight for KDE."
    author: "AC"
  - subject: "Re: JuK"
    date: 2005-12-29
    body: "i agree. while i don't use juk anymore, it IS a great audioplayer. clean, simple, fast, and great looking."
    author: "superstoned"
---
It seems to be review time for <a href="http://amarok.kde.org/">amaroK</a>.  The amaroK team is thrilled to see amaroK way ahead of the pack in a recent <a href="http://lwn.net/Articles/160704/">Grumpy Editor review on LWN.net</a>, in which the author describes the best and worst features of four popular Free Software audio players. The review points out some of the weaker features of amaroK and the developers have written about <a href="http://amarok.kde.org/content/view/67/66/">how they are addressing some of the issues mentioned</a>.  amaroK has also had an audio review on the <a href="http://www.thepodcastnetwork.com/linuxuser/2005/12/12/the-gnulinux-user-show-27/">GNU/Linux User Show No. 27</a>. The author describes most of amaroK's features and shows once again that the slogan "rediscover your music" fits perfectly. You download the review at their website, or by <a href="http://www.newlinuxuser.com/howto-use-amarok-to-listen-to-podcasts/">adding</a> the <a href="http://www.thepodcastnetwork.com/linuxuser/feed/">podcast</a> to amaroK.




<!--break-->
