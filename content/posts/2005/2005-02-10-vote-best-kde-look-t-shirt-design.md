---
title: "Vote for the Best KDE-Look T-Shirt Design!"
date:    2005-02-10
authors:
  - "Theobroma"
slug:    vote-best-kde-look-t-shirt-design
comments:
  - subject: "why pay for kde-look ?"
    date: 2005-02-10
    body: "i dont understand the purpose of paying for kdelook ... kde has so many sites but for kde-look the community has to pay money. perhaps kde-look should get a better sponsor or but the big files not on kde-look itself. \n\nwhat about using sourceforge or something other ?\n\n\n\n(btw. i understand that traffic costs money)"
    author: "chris"
  - subject: "Re: why pay for kde-look ?"
    date: 2005-02-10
    body: "You don't understand but you understand? It costs money. Period. If you can't choke up the money yourself, what are you complaining about?\n\nThey won't make much money if any on the sale of t-shirts anyway.  It's just good for KDE promotion."
    author: "ca"
  - subject: "Re: why pay for kde-look ?"
    date: 2005-02-11
    body: "we dont pay for kde-look, we pay for the t-shirt ! ;)\nkde need more and more promotional items.\n\ncar and water bottle stickers please\n\ndoes kde-look give any money to the kde project out of the t-shirt ?\n\n"
    author: "somekool"
  - subject: "Any hope for non-us?"
    date: 2005-02-10
    body: "All the good hackish t-shirts I ever find seem to be US sites. Any chance there will be UK sellers for this one? And generally, does anyone know any good UK sites for geeky t-shirts? Syswear is good but the selection is a bit limited."
    author: "mikeyd"
  - subject: "Re: Any hope for non-us?"
    date: 2005-02-10
    body: "dito. Is there a german page without credit-card-only payment?"
    author: "anon"
  - subject: "We are working on it:)"
    date: 2005-02-10
    body: "I have been working on setting up a European shop for some time now. Hopefully it will be up in the next few weeks."
    author: "Janet Theobroma"
  - subject: "KDE t-shirts in general?"
    date: 2005-02-10
    body: "I have been looking (although not too hard) for general KDE shirts. Any sites that have some nice shirts, preferably a site that gives something back to KDE from the sale? Normal Thinkgeek-shirts are just too common nowadays, they are no fun anymore, I need something new. :)\n"
    author: "Chakie"
  - subject: "Re: KDE t-shirts in general?"
    date: 2005-02-11
    body: "Fabrice of kde-nl used to have kde t shirts in stock.\nDunno of he still has some, but you can always ask him :)"
    author: "ac"
---
We have opened a poll at kde-forum.org for the whole KDE community to <a href="http://www.kde-forum.org/thread.php?sid=&postid=36252">vote for the new KDE-Look t-shirt</a>. The winning shirt will be on sale at <a href="http://www.cafepress.com/revelinux">Revelinux Gear</a> with all the proceeds going to help out with the costs of running KDE-Look.  Let's show our appreciation for all the great eyecandy we get at kde-look. 





<!--break-->
<p>The first place winner will receive the new KDE-Look t-shirt featuring their artwork AND a 7-in-1 External/Internal USB Memory Card Reader!</p>

<p>You will need to sign up with kde-forum with a valid email address to vote.</p>

<p>Voting is open to everyone until February 16th.</p>





