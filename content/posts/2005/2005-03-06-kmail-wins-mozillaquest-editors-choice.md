---
title: "KMail Wins MozillaQuest Editor's Choice"
date:    2005-03-06
authors:
  - "brockers"
slug:    kmail-wins-mozillaquest-editors-choice
comments:
  - subject: "*yawn*"
    date: 2005-03-06
    body: "ho hum, its only 'mozillaquest, hardly authoritative."
    author: "ricky"
  - subject: "Re: *yawn*"
    date: 2005-03-06
    body: "Indeed, but its a decent review/interview regardless."
    author: "Ian Monroe"
  - subject: "Re: *yawn*"
    date: 2005-03-06
    body: "Arrrgh. Not again! I wonder why articles from MozillaQuest still\nmake it on the dot which I consider otherwise a serious source\nof journalism. I wont even bother to click the link.\nFacts:\n- MozillaQuest is run by a single person\n- This person probably had an argument with some of the Mozilla folks\n- Therefore (or because he is completely nuts, probably both) he is\nwriting articles *for years* goofing on Mozilla with ludicrous arguments\nsuch as \"Bug numbers constantly rising\" and stuff like that. We certainly\nwouldnt like KMail or KDE general to be measured by such standards.\nThe only reason KMail is preferred is because it is not Thunderbird.\n- Sometimes he tries to disguise his trolling with rare spots of \nenlightened journalism but those stretches quickly disapperar behind\nthe mist of the Highlands of the Trolls (TM).\n\nPreventing such tabloid articles to appear on the dot in the future\nis not censorship but quality assurance.\n"
    author: "Martin"
  - subject: "The only reason KMail is"
    date: 2005-03-06
    body: "> The only reason KMail is preferred is because it is not Thunderbird.\n\n?? Have you ever used a recent Kmail? Then you would know that Thunderbird is no match for Kmail"
    author: "Xedsa"
  - subject: "Re: The only reason KMail is"
    date: 2005-03-06
    body: "I'd assume what the original poster meant was something along the lines of \"No matter what KMail was capable of, it would still be prefered to Thunderbird just because it is NOT Thunderbird\". That, at least, was how I read it. "
    author: "shinsaku"
  - subject: "Re: The only reason KMail is"
    date: 2005-03-06
    body: "Exactly that. I'm a big fan of KMail and it is my default mail app.\nThat should say enough. Thanks to parent for clarification."
    author: "Martin"
  - subject: "Re: *yawn*"
    date: 2005-03-06
    body: "Who cares?"
    author: "David"
  - subject: "Relax, dude"
    date: 2005-03-06
    body: "MozillaQuest's latest article on Mozilla seems to be from April 2004 and is titled \"How to Create a Simple Web Page with Mozilla Composer 1.7 and Netscape 7 Composer.  Part I. Getting Started.\".  Hardly inflammatory."
    author: "em"
  - subject: "Re: Relax, dude"
    date: 2005-03-06
    body: "Dig a little deeper.  MQ is just a huge troll.  There's a reason so many people feel this way."
    author: "ac"
  - subject: "Re: Relax, dude"
    date: 2005-03-06
    body: "I read the KMail article and it seems fine.  No idea what your problem is, unless you're the one who's a Mozilla troll and/or just bitter Thundersuck wasn't chosen."
    author: "ca"
  - subject: "Re: Relax, dude"
    date: 2005-03-07
    body: "If there was an article on microsoft.com just stating the plain\nfacts where Outlook is more advanced than any other e-mail-client\nout there would you consider this a source of unbiased information?\nRather not I suppose. The point is: You can find things that work\nbetter than in other with ANY e-mail-client. Only from a neutral \nPOV you can do a convincing comparision. I might tell you that\nsending your mail via telnet on port 25 is superior because it is\nextremly fast (no unnecessary GUI overhead) and you get to read all\nserver responses unaltered and therefore communication problems with\nthe mail server can be detected much easier.\nAn SUV shootout done by a certain automobile manufacturer,\na report about the state of unemployment from the ruling administration,\nwould you believe such things? Surely not.\nOf course, there is always a personal aspect to journalism. This is\nnot to be confused with plain bias, however. Good example with the above\nunemployment example:\nA journalist might or might not have a personal history of unemployment.\nTherefore he will write more or less favorable about an unemployment rate\nof - say 8%. That's perfectly OK.\nThe administration, however, will *always* write an optimistic\nreport - even when the rate is at 30%. Therefore, the whole report is\nin fact useless. (BTW: I'm constantly annoyed about these reports\nfor this very reason - They just burn our money)\nKMail is a very good e-mail-client. Thunderbird is, too.\nBut to make the term \"Editor's [sic!] choice\" and with it\nthe whole article meaningful it is necessary that the source \nis somehow _trustworthy_. Otherwise Mozillazine could select \nThunderbird as Editors' choice just the other day.\n"
    author: "Martin"
  - subject: "Re: Relax, dude"
    date: 2005-03-07
    body: "I think we recognize trolling here perfectly well.\n\nBias has never precluded anybody from opinion.\n\nKay"
    author: "Debian User"
  - subject: "Re: Relax, dude"
    date: 2005-03-07
    body: "Past MQ trolling includes:\n\n* Always referring to Netscape as AOL/Netscape or similar, when that isn't their name\n* Claiming Mozilla is getting buggier because the bug count is increasing - note the bug count on the front page.\n\nLook at \n\nhttp://www.mozillaquest.com/Mozilla_News_03/Mozilla_1-4_released_Story01.html \nhttp://www.mozillaquest.com/Mozilla_News_02/Mozilla_1-0-0_preview-bugs_Story01.html#bugs_and_quality\n\nThese kind of plainly inaccurate stories is why Mangelo cannot be trusted. If he hates Mozilla so much, why did he call the site MozillaQuest? Because he is a troll.\n\nIf you still think I'm a bitter Mozilla fanboy, compare the second story to this one:\n\nhttp://dot.kde.org/1109112982/#1109115434\n\nSo, despite nearly everyone else disagrees with him on this, why does it feature in so many of his stories?\n\nBecause the first time he wrote a critical story was the first time one of his stories got any real attention.\n\nMangelo writes stories that will get him attention, not for accuracy. Even if Thunderbird was a clear winner, it wouldn't have won *his* test, because neither MozillaZine or MozillaNews would have reported on it.\n\nNote the other inconsistencies - the massive pointless cross-linking of articles, use in many cases of \"our\", when there's one of him, his EFnet Mozilla channels with multiple clones of himself, and it points to attention-seeking\n\ni.e. TROLLING\n\nTime to ignore him until he goes away."
    author: "Joe Lorem"
  - subject: "nice page design!"
    date: 2005-03-06
    body: "whoa, I haven't seen such an ugly page in a long time!"
    author: "me"
  - subject: "Re: nice page design!"
    date: 2005-03-06
    body: "LOL. I did not visit that page in a long time but if it still looks\nlike it used to it's clearly on my top list of ugly web design.\nSometimes I wonder how people are able to create such an ugly\ndesign *by accident*. One must do that intentionally, I suppose...\n"
    author: "Martin"
  - subject: "Yup, it's like a time portal"
    date: 2005-03-07
    body: "to the web of around 7 or 8 years ago."
    author: "Mr. Fancypants"
  - subject: "Can KMail compose mails with a background image?"
    date: 2005-03-06
    body: "Can KMail compose mails with a background image?\n\nI know most of you will jump and say that I should not do this for security reasons, but you know, I will like to be given the option to make my own mistakes in KMail too.  ;)\n\nthx!\n"
    author: "gabriel"
  - subject: "Re: Can KMail compose mails with a background imag"
    date: 2005-03-06
    body: "\"...I will like to...\"\n\n\"I *would* like\", should be\n"
    author: "gabriel"
  - subject: "Re: Can KMail compose mails with a background image?"
    date: 2005-03-07
    body: "No, it can't. But, you know, there's bugs.kde.org. ;-)"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Can KMail compose mails with a background image?"
    date: 2005-03-07
    body: "Ingo, do you think that for KDE 3.5 KMail will feature real html editing? I mean, things like inserting images (not as attachments) and so on.. this is a feature I do really miss :("
    author: "Anonymous"
  - subject: "Re: Can KMail compose mails with a background image?"
    date: 2005-03-07
    body: "I'm not aware of anybody working on this. So, I guess the answer is no."
    author: "Ingo Kl\u00f6cker"
  - subject: "PS.."
    date: 2005-03-06
    body: "...just noticed a fun fact:\nIt's Editor's choice - not Editors' choice.\nGuess that says all ;-)\n"
    author: "Martin"
  - subject: "Re: PS.."
    date: 2007-03-16
    body: "Depends ... if they have more than one editor, and these editors have made a choice, that'd be editors' choice"
    author: "K"
  - subject: "KMail did not match Thunderbird"
    date: 2005-03-06
    body: "Everything depends on someone needs. My needs can be different from needs of people which use Kmail over Thunderbird. \n\nWhy I like KMail:\n - its generally faster vs. thunderbird;\n - it is 'native' KDE thing, so I do not need to install something else over \n   KDE;\n - it can be integrated to other KDE applications like 'kontact';\n\nWhy I do not (can not) use Kmail in my everyday work:\n\n - it does not support color marking of Emails in filters;\n - I can not sort by thread;\n - Support for anti-spam is not yet working on acceptable level. I generally \n   support author's  idea about having external filters, but current \n   implementation is not usable for me.\n     Some points:\n      - its damn slow to mark email for spam; \n      - generally I like email which I mark for spam automatically disappear\n        from mail folder, being \"moved\" - not deleted or filed, but just moved\n        to another folder. Not yet possible.\n - I can not copy/paste HTML. I _can_ of course, but what I get in result is not\n   HTML. I need to be able to copy/paste part of web page or table from Open \n   Office, gnumeric, etc.\n\nOf course for someone, what I say is not important. But for me, getting hundrets emails per day sometimes (70% is spam) it is very important to be able to sort it well (with colors/status/thread), compose when needed in HTML (rarely used, but when I need it in 1 Email out of 100 it would be too time consuming do HTML ourside and attach). \n\nSo, generally I like KDE very much, and generally, giving a choice I will choose KDE enabled product, of course if it is usable on level which is required for work.\n\nI am using PINE or Thunderbird...\n\nAlex"
    author: "inspired by article and tried Kmail again"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-06
    body: "I can't speak for the spam filters. (I use IMAP with sieve/spamassasin on the server. What self respecting sysadmin doesn't do spam filtering on the server? ;) )\n\n\nAs for this...\n\"I can not sort by thread;\"\n\nBut you can sort by thread.\n\nSelect Folder --> Folder --> (Thread Messages || Thread messages also by Subject) "
    author: "am"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-06
    body: "> I can't speak for the spam filters. (I use IMAP with sieve/spamassasin on the \n> server. What self respecting sysadmin doesn't do spam filtering on the server? \n> ;) )\n\nYes, I have server side Spam-assassin installed. Yes, it does filter big amount of spam. But, still, it is not effective enough to filter all of spam. So self-learning filters are best to filter out the rest. And enabling learning server side is not the best approach even for my case when I have full control on server.\n\n> As for this...\n> \"I can not sort by thread;\"\n> But you can sort by thread.\n> Select Folder --> Folder --> (Thread Messages || Thread messages also by \n> Subject)\n\nYes, I tried that before, and just tried it again. Right, it selects some threads, but... Not all of them. At least I did not get same result as I get from thunderbird. It does threading using \"In-Reply-To:\", in my case \"Threat also by subject\" does not differ. \n\n\n"
    author: "inspired by article and tried Kmail again"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "KMail threads by \"In-reply-to\", by \"References\" and, optionally, by \"Subject\". In that order. There are no other headers I know of (maybe apart from some proprietary Microsoft crap) which can be used for threading. If you know more than I do, then please send a message to kmail-devel@kde.org.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "Quoth the Poster: \"So self-learning filters are best to filter out the rest.\"\n\nSpamassassin has had Bayesian classification for ages."
    author: "Mr. Fancypants"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "Maybe the OP is running Debian stable."
    author: "Trejkaz"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "\"And enabling learning server side is not the best approach even for my case when I have full control on server.\"\n\nToo bad.  On my server everyone one has a spam folder and each user gets bayesian filtering for thier box based on that spam folder.  :)  All hail IMAP.  :)"
    author: "am"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-04-11
    body: "Dear Admin Gurus,\n\nSo has anyone written an IMAPD agent that can learn from\nthe bayesian filters thunderbird accumulates on the client\nside? I guess both the server-side agent and Thunderbird\ncan dump the suspected spam in the same folder.\n\nI am quite happy with the results of my client-side\nThunderbird but would love to postpone the daily client-side\nfiltering when I am on the road.\n\nThanks!\nSanjay."
    author: "Sanjay"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-06
    body: "I have a question about your problems with the integration of bogofilter and/or spamassassin: What is difficult with them? I set them up within, I think, 5 minutes by reading the howto I found on KMail's homepage. It works extremly good, I am getting about 5 \"unsure\" mails in my \"unsure\" folder after one week training per week (having several hundert ham-mails per, I guess it is about 50:5:600 (spam/unsure/ham)). Using the new setup-filter it is really easy.\n\nI used spamassassin for about 1 year but lost my settings for it; that is why I switched to bogofilter. Bogofilter works really good, is pretty fast (coded in c, not perl like SA)."
    author: "anon"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "> It works extremly good, I am getting about 5 \"unsure\" mails in my \"unsure\" \n> folder after one week training per week (having several hundert ham-mails per, I\n> guess it is about 50:5:600 (spam/unsure/ham)). Using the new setup-filter it is\n> really easy.\n\nHm, I think I was not so patient as you... I wanted \"quickly\" check it. What I did is standard action - tried to \"learn fast\" by giving complete \"Junk\" folder to KMail after configuring it to use bogofilter. Surprise! It _almost_ killed the server by load average... So. I tried for a few days marking it manually and then tired of seeing so big amount spam messages anyway. \n\nAny hints how to learn fast? I have both 'saved messages' which could be used to learn as 'not spam' and 'Junk' folder with 1 month history of junk messages. Does it work for you? May be I am doing something wrong...\n"
    author: "inspired by article and tried Kmail again"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "I tested bogofilter with I think 1000 spam-mails and about 1000 ham-mails and trained it with it. It took quite some time but did not crash. It is a few month ago so I can't remember the exact time, perhaps it took about half an hour on a PIII 1200 with 512mb RAM on Gentoo."
    author: "anon"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "Oh, I forgot to mention: I did the initial learning (the ~2000 mails) not from inside KMail but from the command line. Perhaps it is a bug in your KMail, perhaps not. Did you try the latest KMail (as of KDE 3.3.2)?"
    author: "anon"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-06
    body: "Wow, of all your points for not using KMail one was actually valid and two somewhat true, the other reasons was false. KMail's filters are powerfull, but to me it don't look like you can mark with color. Manually marking mails as spam are somewhat slow, but with some training the filter get most spam anyway so it's not a big problem. You can't copy/past HTML but you can compose HTML mails, and since you rarely write such males the no copy part can't be a big problem.\n\nThe other point you rise have existed for a while already, sorting of spam to a separate folder has been available since KDE3.3. Sorting by tread has been available since forever, I'd guess like KDE2 or something."
    author: "Morty"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: "> - its damn slow to mark email for spam; \n\n\"goddamned slow\" is a better word to describe it...\n\n> - generally I like email which I mark for spam automatically disappear\n> from mail folder, being \"moved\" - not deleted or filed, but just moved\n> to another folder. Not yet possible.\n\nSomehow it works for me but I honestly can't tell you how I set it up. IIRC\nthe spam filter creates some special filter rules which u should examine..."
    author: "ac"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-07
    body: ">Somehow it works for me but I honestly can't tell you how I set it up.\nJust select the folder you want the spam moved to when running the spam wizard. Simple as that. "
    author: "Morty"
  - subject: "Thunderbird does not match Kmail"
    date: 2005-03-10
    body: "I have tried lots of different mail clients to see if any of them can match kmail.  There seems to be two facilities which kill it for me are\n\na) The automatic deletion of mail from a folder after it gets old.  I filter all my mailing lists into separate folders and use that facility to delete old read mail from these mailing lists.\n\nb) The way the space key or keyboard plus keys work.  That is, they work down the message (or not in the case of kbd+) then go to next message in folder, then to next folder (missing out trash). This is *so* convenient of skimming large amounts of mail from high volume mailing lists.\n\nAs for filtering spam, my mail servers spamassassin filter is first line of defence, and then I filter everything else my mailing list of know e-mail address.  What is left in my inbox is then generally spam or the occassional message from someone I don't know. It really takes more than a few moments to pick out any of the latter and delete the rest.  Because of point b) above, my standard read technique will be to read all messages using either one of the two keys mentioned."
    author: "Alan Chandler"
  - subject: "Re: Thunderbird does not match Kmail"
    date: 2005-03-11
    body: "> b) The way the space key or keyboard plus keys work. That is, they work down the message (or not in the case of kbd+) then go to next message in folder, then to next folder (missing out trash).\n\nThe space key of course works the same in thunderbird. If i remember correctly it even worked that way in Netscape 4.x."
    author: "Helm"
  - subject: "Re: Thunderbird does not match Kmail"
    date: 2005-03-12
    body: "Yes, most of the other e-mail packages partially match some of the functionality I need.  I have yet to find another one that matches all.\n\nI can't remember the exact detail with Thunderbird, although I remember this being the closest, but note that I want both the space bar and kbd+ key functionalities - (the latter is when you need to scan a large number of posts rapidly).\n\nAnd of course I still need the folders purged of old messages automatically."
    author: "Alan Chandler"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-03-29
    body: "I also still prefer thunderbird for mail since Kmail does not let me to change and assign special folders on my imap account. I am mobile as much as I can be and cannot stand to have a message sent stored localy on any computer. I'd switch to kmail immediately if I could change sent, trash and junk mail forders to folders on my imap account.  Also a very good sieve editor, gui wizard would be great.  \n\nI personally find Mac OS X's iMail very similar to Kmail. It really felt like kmail after a couple of minutes, but iMail supports special folder assigning. Mailbox -> Use this mailbox for -> Sent (for instance) That would be great for me to see on Kmail. "
    author: "AnonymousCoward"
  - subject: "Re: KMail did not match Thunderbird"
    date: 2005-10-19
    body: "IMHO, you can define the sent, trash and drafts-folder to be wherever you like.\n\ncheers"
    author: "Doggy"
  - subject: "You can sort by thread..."
    date: 2005-03-06
    body: "I can't speak for the spam filters.  (I use IMAP with sieve/spamassasin on the server.  What self respecting sysadmin doesn't do spam filtering on the server?  ;) )\n\n\nAs for this...\n\"I can not sort by thread;\"\n\nBut you can sort by thread.\n\nSelect Folder --> Folder --> (Thread Messages || Thread messages also by Subject) \n\n\n\n\n"
    author: "am"
  - subject: "can't connect"
    date: 2005-03-07
    body: "With KMAIL, I cannot connect to my ISP's mail server! Even after using KMAIL's feature that chooses what the server supports. With Evolution/thunderbird on the *SAME* machine, there is no problem. Why is this the case?"
    author: "judith"
  - subject: "Re: can't connect"
    date: 2005-03-07
    body: "If you need help with KMail then contact kdepim-users@kde.org."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: can't connect"
    date: 2005-03-07
    body: "Ask Charles.\n\nHe deliberately installed an alterted version of KMail to annoy you."
    author: "ac"
  - subject: "can't connect"
    date: 2005-03-07
    body: "With KMAIL, I cannot connect to my ISP's mail server! Even after using KMAIL's feature that chooses what the server supports. With Evolution/thunderbird on the *SAME* machine, there is no problem. Why is this the case?"
    author: "charles"
  - subject: "Re: can't connect"
    date: 2005-03-07
    body: "No idea.. maybe Judith knows in the meantime?"
    author: "uddw"
  - subject: "Sad, or interesting?"
    date: 2005-03-07
    body: "It is now obvious that we have astroturfing going on. It makes me wonder\n1) why go after KDE this way? is KDE really making more inroads than is realized?\n2) how much of this is going on?"
    author: "a.c."
  - subject: "Re: Sad, or interesting?"
    date: 2005-03-07
    body: "If somebody was having trouble sending messages in Program X, then it is useful to know Progam Y works; it eliminates many factors.\n\nKDE and Mozilla are not enemies. KDE uses Bugzilla. Just because Gnome uses more bits of Mozilla than KDE does, it still doesn't make it the enemy because Gnome isn't the enemy either.\n\nAstroturf? Hardly. Are you seriously suggesting this was somebody close to the Mozilla project attempting to promote Thunderbird? For one, your message is the first suggestion ever I've heard that this sort of thing is going on. Certainly, at Slashdot, Mozilla devs post under their own names.\n\nIt looks more like the typical \"Person who can't get something to work and posts an angry message about it\". Not ideal, but then nobody's perfect, and certainly not a cause to fight back."
    author: "Joe Lorem"
  - subject: "Re: Sad, or interesting?"
    date: 2005-03-07
    body: "*blinks*\n\nMake that posts *two* angry messages. Again not unusual, plenty people on IRC think their question is more visible if they repeat it a few times..."
    author: "Joe Lorem"
  - subject: "KMail doesn't cache IMAP emails as Thunderbird"
    date: 2005-03-07
    body: "KMail doesn't cache IMAP emails as Thunderbird.\nThis is important for dialup 56k users (if you choose a mail, then another and finally the first it is re-downloaded...)"
    author: "Trap"
  - subject: "KMail does cache IMAP emails like Thunderbird"
    date: 2005-03-07
    body: "Just use the 'Disconnected IMAP' account type."
    author: "Bille"
  - subject: "Re: KMail does cache IMAP emails like Thunderbird"
    date: 2005-03-08
    body: "But disconnected IMAP has \"wired\" behaviour:\n*) When clicking on a folder, it does not update it's contents (as is done by every other client, even kmail's \"online IMAP\")\n*) It's not selectable which folder (not) to cache\n*) Checking for new mails is much slower than with \"online IMAP\"\n"
    author: "Birdy"
  - subject: "Missing features, lots of bugs"
    date: 2005-03-08
    body: "Features:\n. No X-Face support -- my personal peeve.\n. No true HTML-authoring in composing -- not even pictures (although that'd be nice), how about tables, links?\n. No client-side certificate authentication.\n\nBugs:\n. IMAP remains unstable despite moving from kio_imap to kio_newimap;\n. Occasionally, the messages pane or the message pane remain blank;\n. Obnoxious (and useless) verbosity, that sometimes causes the ~/.xsession-errors to fill up /home.\n\nTo be fair, there are other nice features, which make life tolerable, but I'd rather see the bugs fixed *first*.\n"
    author: "Mikhail T."
  - subject: "Re: Missing features, lots of bugs"
    date: 2005-03-08
    body: "There is X-Face support in KMail in KDE 3.4."
    author: "Paul Eggleton"
  - subject: "Re: Missing features, lots of bugs"
    date: 2005-03-08
    body: "> Obnoxious (and useless) verbosity, that sometimes causes the ~/.xsession-errors to fill up /home.\n\n--disable-debug or kdebugdialog"
    author: "Anonymous"
  - subject: "Re: Missing features, lots of bugs -> Which Emaile"
    date: 2005-03-24
    body: "Are you talking about kmail or thunderbird??"
    author: "Anonymous"
---
<a href="http://www.mozillaquest.com">MozillaQuest Magazine</a> has an <a href="http://www.mozillaquest.com/Linux05/KMail_KDE_Linux_01_Story01.html">overview  of KMail</a>.  Their final evaluation of KMail awards it their "MozillaQuest Magazine Editor's Choice Award".  The article includes the top five features from KMail author Ingo Kl&ouml;cker, an explanation of some of KMail's standout features and some nice screen shots.


<!--break-->
