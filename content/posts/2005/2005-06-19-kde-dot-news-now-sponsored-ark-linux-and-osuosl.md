---
title: "KDE Dot News: Now Sponsored by Ark Linux and OSUOSL"
date:    2005-06-19
authors:
  - "numanee"
slug:    kde-dot-news-now-sponsored-ark-linux-and-osuosl
comments:
  - subject: "hope its fast now"
    date: 2005-06-18
    body: "kdenews.org was (is) the only site from the kde project which does not load fast. I hope its better now. lets see .. . ... . ..   ..    ."
    author: "chc"
  - subject: "Re: hope its fast now"
    date: 2005-06-19
    body: "Yep!  It's fast."
    author: "ac"
  - subject: "Re: hope its fast now"
    date: 2005-06-19
    body: "Finally a fast loading Dot News! Thanks to all!"
    author: "Anonymous"
  - subject: "New Dot"
    date: 2005-06-19
    body: "A while back I remember reading a blog about the dot rewritten in ruby (or was it python?) Is there work on the front?  Or is that necessary now?\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: New Dot"
    date: 2005-06-19
    body: "If you look near the bottom of the front page they write that this site uses squishdot for zope, which is written in python.  Zope is very cool, but is memory hungry."
    author: "J.M."
  - subject: "Re: New Dot"
    date: 2005-06-19
    body: "You didn't have a Zope error on the new server yet? Lucky guy..."
    author: "Anonymous"
  - subject: "Re: New Dot"
    date: 2005-06-19
    body: "I'm not promising anything for before the year's end, because that's what my schedule is looking like, but I intend to go ahead with the Ruby plan.\n\nWe have several problems right now, and to be fair Zope isn't always the culprit.  I've been fixing our installation here and there (e.g. the new improved on-demand flatforty which uses neither a ZCatalog nor the article list) and things have improved somewhat, but we still leak memory and eventually the site slows to a crawl and has to be restarted.  \n\nProbably if I remove the (nearly useless) search features the state of things will improve somewhat, but I don't have a good replacement.  Also our current version of Apache doesn't seem to have a caching module available.  \n\nI'll continue to try improving the Zope setup in the interim, but still expect to replace the whole thing eventually."
    author: "Navindra Umanee"
  - subject: "Re: New Dot"
    date: 2005-06-20
    body: "Instead of removing the simple search field you should simply make it call 'gg:<search phrase> site:dot.kde.org'."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: New Dot"
    date: 2005-06-20
    body: "Exactly, that's what I'd suggest as well (and removing the 'encoding' drop down list for addPostingForm)."
    author: "ac"
  - subject: "Re: New Dot"
    date: 2005-06-20
    body: "Yes I am aware of this and this was my plan all along.  \n\nThe problem is searching for authors or categories or titles or even controlling the date range.  I'm more worried about the first two, since the latter two barely work."
    author: "Navindra Umanee"
  - subject: "Thanks everyone!"
    date: 2005-06-20
    body: "Thank you all for you contribution to make KDE a better place :)"
    author: "Joergen Ramskov"
---
I am pleased to announce that KDE Dot News has gained new hosting sponsors.  We are now hosted on the <a href="http://www.arklinux.org/">Ark Linux</a> server through the <a href="http://osuosl.org/">OSU Open Source Lab</a> network, having successfully completed the transfer little more than a week ago. As some of you may know, we have a long history of having been hosted and co-hosted with Ark Linux, so it is great to be back with our old friends.    

<!--break-->
<p>
We are truly grateful to <a href="http://ndlr.net/txt/">Pierre-Emmanuel Muller</a> and <a href="http://www.levillage.org/">LeVillage</a> for having hosted and supported KDE Dot News for the longest time possible -- three entire years with no strings attached.  We are indeed lucky to have such friends.
</p>
<p>
On a related note, <a href="mailto:d@vidsolbach.NOSPAM.de">David Solbach</a> and <A href="mailto:paleo@pwsp.NOSPAM.net">Olivier Bédard</a> have both made generous offers to host any KDE project in need -- feel free to contact them for details.
</p>




