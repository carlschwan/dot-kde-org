---
title: "KDE 4: Understanding the Buzz"
date:    2005-08-11
authors:
  - "jriddell"
slug:    kde-4-understanding-buzz
comments:
  - subject: "A HUGE job indeed."
    date: 2005-08-11
    body: "Also, people need to realize that even the status of KDE4 since the article was written is still in huge flux.  The kdelibs API is going to be changing and when that happens apps will be broken until they've been updated to reflect the new API.\n\nThis is a tremendous and complicated effort.  It'll take time and patience on the part of library developers, application developers, third party developers, documenters, translaters, artists, and of course... our users.  Stick with us though, we have every reason to believe it is going to be marvelous when it is done. :)"
    author: "manyoso"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-11
    body: "Yes indeed, it is a HUGE job.\n\nI can't help but wonder if this is the wisest course of action.  That is, a professional software developer would advise against the plan.  We are doing several things at once and this is not wise.  If things work out correctly, we will probably have a great product, but if they don't, we could wind up with a large mess on our hands.\n\nA more conservative road map would be to just do the port and release it as 4.0 and THEN start on radical changes for 4.1.\n\nIAC, with the current plan, I expect to see delays so we should plan on a 3.6 release."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-11
    body: "Not really. It's called unstable development, and it happens in every single project, commercial or otherwise. You get to something stable through iterative development and improvements."
    author: "Segedunum"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-11
    body: "And the history of commercial development contains examples of new versions that went a bridge too far resulting in release delays."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "> resulting in release delays\n\nSince when are Open Source projects on a fixed release schedule? A delay in the KDE 4.0 release, so what?"
    author: "cl"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "That would depend on whether it was a week, a month, or a year.\n\nWouldn't it?"
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "No."
    author: "Chris Howells"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "Indeed, no. Its ready when its done... This is Free Software. There are no PHB's and marketing goals, only quality software..."
    author: "r_a_trip"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-17
    body: "Well, GNOME is, as a matter of fact. I'm not saying that is a positive thing, just saying it is."
    author: "Fruity Frog"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "\"And the history of commercial development contains examples of new versions that went a bridge too far resulting in release delays.\"\n\nIs this supposed to tell us something new? Sometimes you just have to accept that something is complex and work with it."
    author: "Segedunum"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-11
    body: "The aim of the large versions numbers (v3, v4) is to maintain binary and API compatibility through-out so that app developers can be guaranteed their exsting code once ported will continue working throughout the series (i.e. code written to v3.0 still runs unchanged on v3.4).  To have a v4 and v4.1 with radically different API's is counter to this aim.  What you propose is what in effect is happening, but just without the public release in-between.  First a port to QT4, then change the API as required, then release v4.0.\n\nJohn."
    author: "odysseus"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "> That is, a professional software developer would advise against the plan\n\nAnd we are all just hobby software developers, right?  I can assure you that there are more professional software developers working on KDE than you might think.\n\n> A more conservative road map would be to just do the port and release it as 4.0 and THEN start on radical changes for 4.1.\n\na) And what should our user do with this great KDE 4.0? Doesn't make sense to release stuff that is just a port to a new library version.\n\nb) We aren't doing several things at once. The plan is to first port KDE to Qt4 and then make the big API changes. Nothing else is happening in SVN. We just don't release the port to the public (see a).\n\n> IAC, with the current plan, I expect to see delays so we should plan on a 3.6 release.\n\nWe don't have the man power for another KDE3 release. After KDE 3.5, everybody will be working on KDE4."
    author: "Christian Loose"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "> And we are all just hobby software developers, right?\n\nActually I don't know what people consider themselves to be.  I don't hear anybody else saying that they studied EE/CS in college, perhaps because the culture here denigrates such an education.\n\n> a) And what should our user do with this great KDE 4.0? Doesn't make sense to \n> release stuff that is just a port to a new library version.\n\nI notice that when people want to disagree with you that they seem to skip over the adjectives in the sentences. :-)  I referred to *radical* changes.  Not doing radical changes doesn't mean not doing any changes.\n\nWhat the user would do with such a release would be to use it.  There is supposed to be many improvements in Qt 4.0 and IIUC there are planed improvements to the libraries.  This should be enough unless you advocate \"featuritis\" as the important thing in software development.  Adding new features should not be the driver in KDE development now that it is mature software.\n\nPerhaps all will be well.  But, I keep reading about completely redoing this, completely redesigning that.  I start to wonder how this is going to be done with the \"limited manpower\"."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "I would suppose that most of KDE's professional developers have studied IT or something near, in universities or \"high schools\".\n\nAnd if EE is \"eletrical engineer\", then there is at least Eva who is one:\nhttp://www.kde.org/areas/people/eva.html\n\nhttp://people.kde.org/dirk.html tells me that Dirk Mueller has studied IT.\n\nAnd probably there are more people having studied anything scientific.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "See also:\nhttp://www.kde.org/people/gallery.php\nthat confirms what I have thought and written.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "Hmm, that page needs a little updating..."
    author: "Paul Eggleton"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "While I don't disagree with you when you say that the current projected KDE 4.0 may be a little more than we can chew off, I do know that unless you aim high, you must fall short of the requirements..\n\nWhile I haven't got a CS MA (I'm a linguist, comparative linguistics of the Sino-Tibetan languages of Nepal -- eastern Nepal -- to be precise), I've got about a decade of experience in the software industry. Aim high, damn the certifications and forget about cmm, that's the only way to produce something worthwile. Ours is a craft, not a profession.\n\nAnd make sure your daily build can be released as-is."
    author: "Boudewijn"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "> KDE 4.0 may be a little more than we can chew off,\n\nThat is really my only point. that we should be careful that we don't:\n\n'bite off more than we can chew'.\n\nThe history of software projects is littered with the remains of projects that did that.  Yes, you are correct that we should aim high, but we need to be realistic regarding what we can accomplish for the first release of KDE-4."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "I bet most changes which will be actually done will be rather evolutional and consequential for the developers who work on the stuff. It's just that the necessity of BC for the whole 3.x series helt those bigger evolutional changes back until the 4.0 release which allows breaking BC once again. So the huge amount of changes make the effort look \"radical\", but isn't if you actually look at the details. So calm down and stop worrying. =)"
    author: "ac"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "If CS means \"Computing Science\" I studied 4 years at the university and before \n4 years in school and 2 more years before as hobby. I'm sure (and know) that there are many others who have similar education in the KDE project.\n The fact that I don't have a master degree and a doctorate is that I find it useless if you just want to be a developer. ;-) I better learn by experience. I'm sure that sometimes those who learned to code by themselves are better than some who studied at universities.\n"
    author: "Andras Mantia"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "I'm glad to hear that there are professionally trained people working on the project.  Perhaps we can form a group to do what ever.\n\nThe question of knowing how to code verses knowing software engineering is an old one that I won't start on here since it is way off topic except to say that much has been written about it."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "This is not really workable. If we release 4.0, then the BC compatibility requirement comes in force which will then stop us from doing any \"radical\" changes. The model currently followed has been used before and has worked very well for us. Think about KDE2 -> KDE3 effort. KDE3 has been hugely successful so far."
    author: "Nadeem Hasan"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "I don't see why the API changes can't be made without completely redsigning the desktop and the panel.  And the dozen or so other radical redesigns which I read about.  Yes, the library API changes need to be made along with the port to Qt-4.  But, every week, I read of more changes.\n\nWe need to remember that Murphy was an optimist and try to keep the project from growing too large to handle."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "IMHO you should leave the decisions to those who do the work."
    author: "cl"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "A complete insult to the large numbers of highly skilled professional software devlopers that _are_ working on KDE and have done since it inception.\n\nBut I should have known, James Richard Tyrer is the expert softare developer that can save KDE from impending doom."
    author: "Chris Howells"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "The history I refer to is about work done by highly skilled professional software developers.  The Lotus 123 release debacle comes to mind.  They simply let the project grow beyond what they could accomplish and there was delay after delay and when they finally released it, it was unstable and buggy.  Then there was the project to completely rewrite Windows for the Windows 2000 release.  It was quietly abandoned because they found that despite having a PhD professional software developer running it that it was simply more than could be done. \n\nI am not a professional software developer and I do not claim to be one.  I am an electronic engineer.  What I said was that a professional software developer would give you certain advice.  I am not that person.  The advice, however, is valid.  The advice I would give as an engineer is to always remember Murphy's law."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "> I am not a professional software developer and I do not claim to be one.\n\nWell, I am a professional software developer working for a company that creates real estate management software (http://www.aareon.de). And I tell you, the development plan the KDE developers have for KDE4 is fine.\n\nPleased now?\n"
    author: "Christian Loose"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "It isn't a question of being pleased now.\n\nI will be pleased when the release is successfully completed.  Till then, I will continue to be concerned that what is starting to sound like a total redesign of everything is simply too much to do for a single release.\n\nActually, I would be much less concerned if some of the developers shared my concerns instead of exhibiting what appears to be over confidence."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "> Pleased now?\nYou remember me of a teacher I once had, who thought he was always right just because he had studied psychology.\nProject leaders know how, or at least they think they do, to distrust software developer time estimations or at least multiply it with five or so.\n\nBtw. is there a plan for this transition, saying which job is done by (or under supervision of) who?"
    author: "koos"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-12
    body: "> You remember me of a teacher I once had, who thought he was always right just because he had studied psychology.\n\nThen you misunderstood me. \n\nI was replying to James point that professional software developers would advise the KDE developers to do the port differently. And I answered him that he is wrong because:\n\na) I'm a KDE developer \nb) I'm a professional software developer \nc) I would do the port exactly like it's done now.\n\nI don't remember saying I was always right or that I studied anything.\n\n> Btw. is there a plan for this transition, saying which job is done by (or under supervision of) who?\n\nDon't think so."
    author: "Christian Loose"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "No that's what I meant to say. Since reasoning is a process of the mind, how can a psychologist be wrong?\n\n> Don't think so\nI find this a bit frustrating. I have time to work on KDE, but I really have no idea whom to contact or what areas to work on. Patches/questions sent to devel ml seems to either stay unanswered or interpreted as a personal insult of the author of that particular piece of code.\nFrom the other side, I've seen someone porting code to Qt4 that I more or less maintain and completely breaking it. Never had any mail asking about this.\nThis is complete chaos IMHO, some list of the maintain->library should be available I think, no?"
    author: "koos"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "\"Patches/questions sent to devel ml seems to either stay unanswered\"\n\nPossible if the original authors/people familiar in the code aren't involved in KDE atm. And most developers don't read kde-devel due to the high traffic volume.\n\n\"or interpreted as a personal insult of the author of that particular piece of code.\"\n\nSorry, but you are completely wrong if you think that this is the opinion of authors on the whole. I don't remember ever experiencing it. Personally I am delighted that someone is using my program and then gone even further to try and fix something that they didn't like."
    author: "Chris Howells"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "\"From the other side, I've seen someone porting code to Qt4 that I more or less maintain and completely breaking it\"\n\nIs this in KDE? Well, maybe that's because the initial aim of the porting was to get it compiling against Qt 4, not a completely fully working port to Qt 4 -- if we had that we could release KDE 4 now.\n\n\"Never had any mail asking about this.\"\n\nSo did you mail them to express your concerns?\n\n\"some list of the maintain->library should be available I think, no?\"\n\nThere are a few lists of maintainers of various classes in kdelibs.\n"
    author: "Chris Howells"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "For maintainers, much of kdelibs has MAINTAINERS files, and if that fails, there is always (c) notices. And it's probably best to use kde-core-devel and not kde-devel for library patches\n"
    author: "SadEagle"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "I have to report the same experience with patches.\n\nI have posted them to bug reports, I have posted them to kde-core-devel, etc.\n\nOccasionally, people take it as an insult, but mostly I am ignored.\n\nSo, now I am posting stuff to KDE Apps/Look: KDE Improvements.  But, that wouldn't be appropriate for everone.\n\nIf there are any other frustrated would be contributors that think they could put up with the old engineer, please contact me by private mail and I will see if we can start a project. \n\ntyrerj@acm.org"
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "> I have to report the same experience with patches.\n\nPlease start to understand: it's about you. First making yourself unpopular with your comments and then expecting to be received cordially doesn't work."
    author: "Anonymous"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "Interesting theory:\n\nBetter the bug goes unfixed than have someone that you don't like fix it.\n\nAnd I thought that this was supposed to be a meritocracy. :-)"
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "You have a SVN account for one and half month now - and did you make one single commit since then? No! You just keep bitching..."
    author: "Anonymous"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "I deliberately waited till the 3.5 branch was split.\n\nThen I have had a lot of problems with SVN.  I thought that I had it working OK and was going to do a build that would be up to date and I got mysterious error messages:\n\n     svn: Delta source ended unexpectedly\n\nI posted this to kde-devel: 08/10/05 21:30.  Nobody had any cure.  A Goggle search turned it up as a bug.  So, three modules were dead.  Other problems continue.  I keep getting the wrong \"admin\" module.  Am I just unlucky -- I never had any trouble with CVS.\n\nSo, I have a build started again and I hope that it goes OK.  If so, I will be able to do some work.\n\nHowever, you should not presume that just because I have not committed anything that I haven't done anything.  I have icons and patches ready to commit.  Some of the CrystalSVG ones were posted to KDE-Look."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "Try to clean your local working copy with:\n\nsvn cleanup\n\n(I do not know if it will work or not but when you get problems under SVN, it is a good try.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-16
    body: "> Please start to understand: it's about you\n\nI second that;  its impossible to have a discussion since the good old engineer always has to be right and never gives in one inch.  Thats not what a discussion is suppost to be about.\nIts faster to fix the problem yourself then to get into a discussion with JRT, is what I always say."
    author: "Foobar"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "Ah, so you now acknowledge that there are lots of highly professional software devlopers working on KDE. I guess that makes your idea of allowing the plan to be examined by professional software developers moot since they would presumably have already shouted on core-devel if there were problems.\n\nK thanx."
    author: "Chris Howells"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-13
    body: "I never said that there were or weren't professional software developers working on KDE.  It has NOTHING to do with my original posting.  You appear to have, basically, misunderstood my original posting (perhaps with intent?).\n\nI still have the same opinion.  That a professional software developer would caution the KDE project against trying to make too many changes for the KDE-4 release.\n\nNote that the meaning of 'professional' generally indicates a person that is paid for their services.  And that is how I meant it."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "\"That a professional software developer would caution the KDE project against trying to make too many changes for the KDE-4 release.\"\n\nNone of the \"professional software developers\" working on KDE so far have, so maybe you should STFU.\n\n\"Note that the meaning of 'professional' generally indicates a person that is paid for their services\"\n\nI'm glad we're in agreement over something."
    author: "ac"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "Look AC, I  am  tired of this game.\n\nI stated my opinion.  I have explicitly said that it is my opinion.\n\nIs it your position that I am not entitled to have my personal opinion?\n\nIs it your position that I am not entitled to express my personal opinion?\n\nThis is FREE SOFTWARE as in FREE SPEECH!  But, you appear to be against free speech.  Note that, in case you don't understand, that free speech means that other people are going to express opinions and ideas that you don't agree with.\n\nUnfortunately we see a lot of comments like yours both here and on KDE lists.  You have nothing constructive to add.  Your only statements are that what someone else said is wrong.  You don't state why they are wrong.  You don't offer a constructive rebuttal.  Your remarks reduce down to a childish: 'No, your wrong'.  What does that accomplish.\n\nNobody has to accept my opinion, but I am not going to change it just because some AC says that I am wrong.  Especially when they take what I say out of context. \n\nNow, you are still not attaching the same meaning that I am to \"professional software developers\".  But, I am not saying that there aren't any working on the KDE project.  I wouldn't say that because it is a negative hypothesis."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "> Look AC, I am tired of this game.\n\nEveryone is. \n\n \n> I stated my opinion. I have explicitly said that it is my opinion.\n> Is it your position that I am not entitled to have my personal opinion?\n> Is it your position that I am not entitled to express my personal opinion?\n\nThe point is that no one really discussed your opinion because you managed once more to make a remark that was understood as offensive, in the very same posting.   \n\nJust how are the KDE devs to interpret what you said?  \n\n\"a professional software developer would advise against the plan\"\n\n1. It could mean \"one professional software developer would advise against the plan\".  Now that's far-fetched and certainly not what you meant, or is it? \n\n2. It could mean \"Any professional software developer would advise against the plan.\" \n\n2a. It's a well-known fact that there are many professional software developers working on KDE using the definition of \"professional\" that has been established in this thread (some of whom have even spoken up in this thread and they said they didn't advise against the plan).  So with this interpretation what you said must be false, this is basic logic.  This was dead obvious right from the start to the people who replied to you so they discarded this interpretation. \n\n2b. Or what you said was to imply that the people working on KDE are no professional software developers (as no one of them has advised against the plan).  That's how your posting was understood as it's the only possible interpretation that is not obviously wrong.  No wonder people feel attacked and took offense. \n\n\nYou stated later in the thread that 2b was not what you meant either.  But then I've run out of possible (not-obviously-wrong) interpretations.  And it seems I'm not the only one.  \n\n\nI'd advise against using such broad statements (\"professional software developer would...\", \"engineers would...\") in the future.  You don't even speak for all engineers there are, let alone all professional software developers.  Just skip this stuff and talk about the subject. \n\n"
    author: "cm"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "> But then I've run out of possible (not-obviously-wrong) interpretations. And \n> it seems I'm not the only one.\n\nThere is a term for people that interpert general and broad statements as applying to them (especial in a negative way): 'paranoid'.  You, and others, misinterpreted what I said as some sort of attack on somebody.  The statements made by various persons that in NO way addressed the question of whether this project -- which someone else characterized as a \"HUGE job\" -- is too much, seem to say more about the persons who made them than they do about me.\n\nNo, I didn't mean 1, 2, or 3.  I was talking about a hypothetical person (this is a common rhetorical or literay device -- a subjunctive construct) and I meant someone that develops software commercially as their profession (the obvious definition).  It should also be clear from context that I was talking about someone who was OUTSIDE of the project.  Now, we may have a few \"professional software developers\" people working on the KDE project, but they are not working on KDE as their profession -- they are doing it as their hobby (and that includes me since I am a RETIRED engineer) and they certainly are not outside the project.  \n\nSo lets just say that considering the (unbelievable) over sensitivity of some people that I should have said: 'an outside \"commercial software developer\" might advise against taking on such a \"HUGE job\" for a single release'.  I am not saying that ALL such developers would say that, only that some would.  If I had meant 'any' or 'all', I would have said that.  However, I do agree with this hypothetical person.  That is my point, and I wonder why nobody addressed the real point. \n\nI plead guilty to not speaking precisely, but that is all.  Why did I say it that way?  Because I did not want to claim to be a \"professional software developer\".  I did not intend any insult and the fact that others took offense is very questionable.  It seem to be a game to me.  One which I would prefer not to play anymore.  When many people find the need to take everything personally, it makes rational discussion of any issue very difficult."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "> You, and others, misinterpreted what I said as some sort of attack on somebody. \n\nIn fact I didn't.  I was pretty sure you did not *want* to attack anyone personally. I just tried to explain why people *did* take offense by pointing out the different possible interpretations.  Obviously your own was not included, but that should make you think. \n\n\n\n> Now, we may have a few \"professional software developers\" people working on \n> the KDE project, but they are not working on KDE as their profession -- they \n> are doing it as their hobby\n\nWrong, not all of them.  There are several people I'd call \"professional software developers\" paid by Trolltech, SuSE, Eric Laffon and others to work on KDE, and some even full time. \n\n\n\n> If I had meant 'any' or 'all', I would have said that. \n\nMaybe my command of the English language fails me here, but to me a statement like \"a sw dev would do this and that\" is as general as \"a sheep produces whool\" or \"rain is wet\".  Leaving little or no alternatives. \n\nWhat you said was understood as \"If you were professional software developers you would see that this plan cannot work\".\n\nI know that's not what you wanted to say, at least not consciously. \n\n\n\n> However, I do agree with this hypothetical person.  \n\nWhy do you construct that hypothetical person at all?  It does not give your point any more authority than you already have.  I think most people here know about your engineering background by now.  So why not let your opinion stand on its own instead of resorting to hypothetical sw devs or engineers.  Otherwise people *will* point out that there are sw devs and engineers who disagree with you and your hypothetical expert.\n\n\n\n> That is my point, and I wonder why nobody addressed the real point. \n \nThat's what I was trying to explain.  \n\nPeople may or may not be *over*-sensitive but it's not the first time this happens to you (and to few others as consistently I might add).  I think you'd have a better chance to get your actual points discussed if you took that sensitivity into account.  \n\n"
    author: "cm"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "> Look AC, I am tired of this game.\n\nAnd I'm tired of some stupid incompetent cantankerous old wanker who does not understand the free software culture, does not understand the open source culture, does not understand what drives KDE developers, does not understand how free software is written and released generally attempting to stick his oar in and making a mess of things.\n\nIf you can't understand how you've offended  some people then there is nothing more to say, you are a completely lost cause.\n\nFree speech allows you to say what you want. It does not force others to listen to what you are saying. Since I don't want to listen to the same old bollocks that you've been spouting on kde-devel for ages I'm kindly requesting that you go and find another project to troll."
    author: "ac"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "Admin, this post crossed a line I don't like to see crossed on the dot or anywhere in KDE.  \n\n"
    author: "cm"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-14
    body: "Free speech is a mutual understanding where everyone is allowed their say without being shouted down by people making personal attacks.\n\nAnd it goes without saying that what I really don't undersand is people like you (AC).  Perhaps it is wrong to allow people to post that don't provide an e-mail address.\n\nToo bad that I was shouted down and, therefore didn't have the opportunity to discuss what I thought was an important issue.  Now, this has gotten way off topic and way out of hand so I will not discuss it any further.  I believe that I have already apologized to anyone that I inadvertently offended so there is really nothing more to say unless someone wants to actually discuss the issue raised in manyoso's posting."
    author: "James Richard Tyrer"
  - subject: "Re: A HUGE job indeed."
    date: 2005-08-16
    body: "\"And I'm tired of some stupid incompetent cantankerous old wanker who does not understand the free software culture, does not understand the open source culture, does not understand what drives KDE developers\"\n\nNo wonder you fucking losers are laughed at when you start spewing drivel like \"free software culture\", \"does not understand the open source culture\".   You fucking retards don't speak for anybody except for your demented selfs.  Do yourself a favor and get professional help."
    author: "Dave"
  - subject: "Thank you, jhall"
    date: 2005-08-11
    body: "I'ld like to say \"Thank you\" to Jhall for her great articles on kde. A part of the buzz that recently (and finally) surrounds KDE is due to her. The KDE PR machinery seems to finally have rumbled to a start."
    author: "Anno v. Heimburg"
  - subject: "One thing KDE 3.5 needs in terms of PR"
    date: 2005-08-11
    body: "A tour like this:\n\nhttp://www.gnome.org/~davyd/gnome-2-12/\n\nI am sure I saw an even cooler toor for GNOME 2.10 but I cannot find it in google. Physos was working on something for 3.4 I think but I never that that either..."
    author: "Carsten Niehaus"
  - subject: "Re: One thing KDE 3.5 needs in terms of PR"
    date: 2005-08-12
    body: "http://www.gnome.org/~davyd/gnome-2-10/\nhttp://www.gnome.org/~davyd/gnome-2-8/"
    author: "Josh"
  - subject: "Re: One thing KDE 3.5 needs in terms of PR"
    date: 2005-08-12
    body: "Ok, that is a pretty easy URL-scheme :-) But I really didn't find them in google ;-)"
    author: "Carsten Niehaus"
  - subject: "Re: One thing KDE 3.5 needs in terms of PR"
    date: 2005-08-13
    body: "this is why Jess is doing \"this month in svn\". she's spreading out the work of putting together such a tour for 3.5 by doing a bit each month rather than trying to cram for it the week before the release."
    author: "Aaron J. Seigo"
  - subject: "SkinStyle"
    date: 2005-08-11
    body: "I found that really cool project, it would be great if KDE4 would integrate it, it\u00b4s Qt based and GPL. \n\nhttp://dev.openwengo.com/trac/openwengo/trac.cgi/wiki/SkinStyle"
    author: "arequipa"
  - subject: "Re: SkinStyle"
    date: 2005-08-11
    body: "HELP!!! HELP!!!\n*runs away*"
    author: "ac"
  - subject: "Re: SkinStyle"
    date: 2005-08-12
    body: "Well... I dontt like it, I think programs could have a \"kool\" interface while still using KDE/Qt as base (take a look at k3b to see what I mean).\nBut.. for some users and companies.. I'm sure this is kind of a good thing, so yes... (arght, my soul is in pain)... yes... it is a good idea to support skinds."
    author: "Iuri Fiedoruk"
  - subject: "Re: SkinStyle"
    date: 2005-08-12
    body: "> I don't like it\n\nDon't you think it can be good for multimedia players like noatun for instance?\nYou don't even have to change 1 single line of code: you just select the right QStyle plugin, you create a .ini file and .png files et voila.\n\nYou can re-organize entirely your layouts, change widgets position and size, map all widgets with .png files just by editing a .ini file."
    author: "tanguy_k"
  - subject: "Re: SkinStyle"
    date: 2005-08-12
    body: "As I said before, I don't like it, BUT, I think it's a great idea.\n\nSo, now you're probally think: what the hell is he saying, this guy is nuts! Well... I don't like this kind of bloated interface, I prefer more simple, take amarok and k3b as an example, interface that dosen't scape very much from the overall look of programs, but that is for my taste! And I know most people like those bloated interface, and it would be very good for us to keep people satisfacted... so, I vote for this even that I don't want to use it ;)\n\nKind of, well, I know 3 or 4 people that would love it, so go ahead and make them happy as I am already happy.\n\nSorry if I was confuse."
    author: "Iuri Fiedoruk"
  - subject: "Re: SkinStyle"
    date: 2005-08-12
    body: "YOU ARE FIRED"
    author: "FIRED"
  - subject: "Re: SkinStyle"
    date: 2005-08-12
    body: "Pretty cool, in my opinion!"
    author: "Knut"
  - subject: "Re: SkinStyle"
    date: 2005-08-12
    body: "that sure is one way to boost the system requirements..."
    author: "mjb"
  - subject: "kdom"
    date: 2005-08-11
    body: "also there's a lot happening on kdom.  As far as I understood kdom is a new layer that will serve as a base for khtml2, ksvg, mathml?, ..  Apple already started adopting it for Webcore!\nAnd once kdom and khtml2 are a bit more finished I suppose lots of merges will happen from Webcore back to kde now that webcore cvs history is available. \n\nAlso there is being worked on a abstraction layer for multimedia playback, it is called kdemm.  Once it's finished you'll be able to choose between different backends (like gstreamer for instance)"
    author: "Filip"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "Let me get this strait. The idea of the multimedia layer is that we get a layer on top of the existing backends? That would mean that an application is talking to kdemm, which is talking to Arts, which is talking to OSS, which is talking to the driver, which is talking to the hardware. Right? Damn... that's 4 layers to cross before the sound goes from the application to the actual hardware. Wouldn't it be better to sometimes just make a *choice* for a framework? Maybe, may be not\n\nAnyway, I am completely ignorant about how this stuff actually works, so I probably should just shut up."
    author: "Andre Somers"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "> Anyway, I am completely ignorant about how this stuff actually works, so I probably should just shut up.\n\nIndeed."
    author: "ac"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "Chances are, aRts will be gone for KDE4 and a completely new multimedia framework will be put in place."
    author: "canllaith"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "> The idea of the multimedia layer is that we get a layer on top of the existing backends? \n\nThe idea isn't to have another layer in order to have another layer but to avoid another case of \"We're stuck with an unmaintained or no-longer-optimal multimedia framework for the whole release cycle for binary compatibility reasons, i.e. for years to come.\".  \n\nBut I guess the kdemm concept will have to prove its viability anyway before it's released. \n\n"
    author: "cm"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "The idea of KDEMM is also to let the user have a choice. I for one am using xine as a backend for amarok, the only sound-app I am using. Systemsound is disabled and so on. But a friend of mine is doing streaming across networks or something like that and will need MAS for that (I hope MAS was the network-thing... not 100% sure). Well, why should I be forced to use a heavyweight soundserver with network-stuff and so on?\nFurthermore KDEMM will make it easy for the application developer: The application will always call something like play->( soundfile ), no matter which backend the user is using... \nIn a perfect world situation KDE will ask the user once if he needs networkstuff or not and then choose the backend for him. Joe User doesn't know what gstreamer or xine or ... is so that layer should be hidden from him.\nLast not least you never know what happens with gstreamer in 1 year from now. They broke binary compatibility ever 6 to 8 month in the past but KDE4 will need BC for about 3 to 5 years. Even if the gstreamer folks would release version 1.0 tomorrow, who would guarantee BC for 4 years? No one. With KDEMM you can work around this problem as the KDE-apps would use and link against KDEMM and not directly against gstreamer. Of course the very same is true for MAS, xine and so on.\nThe final point is that it might be that the day after the release of KDE 4.0 a super-cool light-weight all purpose soundsystem pops up and we all want to use it... Without KDEMM you couldn't use it but with it it is just another backend. Sure, you would have to wait until KDE provides the \"bindings\" for it, but that will happen pretty soon."
    author: "Carsten Niehaus"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "Wrapping one api on top of another has serious disadvantages too. Either the wrapping api completely exposes the underlying api, but perhaps with different semantics, and then it will be hard to port it to other underlying apis. Or it can expose a reasonable subset. In this case you will not get full access to functionality in the underlying API. \n\nWhen the underlying API is stable, well thought out, and offers advanced features, my belief is that the best you can do is expose it and let application developers take advantage of it directly. Wrapping for portability comes with a price.\n\n"
    author: "claes"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "The \"reasonable subset\" is the idea and the way to go. For more special stuff than eg playing audio (notification, music) the single applications have to support the backend directly themselves."
    author: "Anonymous"
  - subject: "Re: kdom"
    date: 2005-08-13
    body: "well, multimedia is an interesting topic because 98% of applications[1] only need a specific and very limited subset of functionality: play, pause, stop, record, etc... what is important to these applications is that it is easy to do this programmatically, that it's widely portable and that latency is low enough that the user doesn't notice the time between action and reaction. \n\nthese needs are very different than one sees in an actual media creation app, of course, where you want perfect latency handling (\"no perceptible delay\" isn't good enough anymore) and access to a wide and complex array of media functionality.\n\nunfortunately for regular app developers, the best media systems are designed by people very familiar with media and so they tend to end up being complex beasts to deal with unless you too are a media developer.\n\nby providing a thin layer we can get the ease of use necessary and the portability between media systems. by keeping the layer thin, it's also possible to keep it performant. and all media systems support the basics needed here (play, pause, etc)\n\nby recommending a given media engine, we can also give something for serious content creation app developers to target.\n\nso i think the path the kdemm developers are taking makes a TON of sense. given the above explanation, would you agree?\n\n[1] 64% of statistics are invented on the spot by winged mammals"
    author: "Aaron J. Seigo"
  - subject: "Re: kdom"
    date: 2005-08-13
    body: "> by providing a thin layer we can get the ease of use necessary and the portability between media systems. by keeping the layer thin, it's also possible to keep it performant. and all media systems support the basics needed here (play, pause, etc)\n\nAnd the multimedia engine layer in amaroK proves that this is doable."
    author: "cl"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "> [..]\"We're stuck with an unmaintained or no-longer-optimal multimedia framework\"[..]\n\nNot only that. It's also this other mm framework offers something (for example multimedia over network) that _I_ need but I can't use it because KDE choosed something else.\n\n> But I guess the kdemm concept will have to prove its viability anyway before it's released\n\nYes."
    author: "cl"
  - subject: "Re: kdom"
    date: 2005-08-12
    body: "A proper and sensible abstraction for KDEMM is the right way to go. Would you rather do something like play->(soundfile), or link directly and heavily into an MM framework like GStreamer? At the moment, using GStreamer means strapping yourself to it, using the C API and creating a lot of god-awful, unmaintainable code. You would end up in a worse situation than there is with arts at the moment.\n\nThere are certainly disadvantages to using wrappers and layers, but a rule about layers is that every one should add value. This one certainly does."
    author: "Segedunum"
  - subject: "What about tenor?"
    date: 2005-08-12
    body: "What about tenor?  The pervasive search stuff sounds like it should be really cool but I haven't heard about it for some time - any details forthcoming, code, etc (yet)?"
    author: "Mark Williamson"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "> What about tenor?\n\nI'm also very interested. Especially since aseigo doesn't seem to work on it anymore because of Plasma.\n\nI would also like to know if you would be willing to work with the kat (http://kat.sf.net) developers. They seem to be pretty far with their stuff and maybe it's a good basis for Tenor."
    author: "cl"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "There's been a lot of discussion on the klink mailing list in the past couple days. It includes an answer to your question about kat. The mailing list seems to be mirrored here:\n\nhttp://blog.gmane.org/gmane.comp.kde.devel.klink"
    author: "Dolio"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "Wow, didn't even know that this list existed. Thanks alot for the link!!"
    author: "cl"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "just wondering what on earth this \"plasma\" is.\nI read there site and to me it reads something like:\n\n<INSERT random promotion textline>\n<something about superkaramba>\n<hype hype hype>\n<kde rocks>\n\nbut anyone out there any idea what plasma \"is\"?\nis it a totally new gui (like the windows vista hardware accelerated UI), or a team effort to add functions and optimize the current UI, or is it some sort of usability project with an addiction for eye candy? the \"extender\" function surely seems to hint in that direction. http://plasma.bddf.ca/cms/1069\n\nI'd love to say that it is great and promising...\nbut I just have some trouble giving it a place somewhere in my mind. :P"
    author: "Mark Hannessen"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "It's a project that tries to unify the desktop and panels into one coherent unit.\n\nFor instance, superkaramba themes appear to sit on the desktop, but they're really just windows below everything else. Kdesktop itself doesn't know anything about the themes, so if I try to do something like, say, mark superkaramba themes as desktop windows (so that windows don't snap to them), the desktop (with icons turned on) will paint over superkaramba.\n\nSimilarly, stuff on the panel is largely separate from the desktop. Plasma aims to have them more integrated. An extender may pop up, and you may be able to drag it off onto the desktop. Kicker applets and karamba-like themes would be one-and-the-same. Stuff like that.\n\nBasically, today KDE has a whole bunch of things that seem like they're all related (panel, desktop, superkaramba), but they're actually all separate applications that don't integrate as well as they could. Plasma is an effort to bring them together as they should be, in addition to taking them in new and interesting directions, for both usability and eye candy.\n\nAt least, that's my take. I do agree that aseigo tends to include a lot of hype when he talks, no offense intended. I think he talks to regular people too much. :)"
    author: "Dolio"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "that's a pretty good summary of the framework that will be plasma indeed. the only thing missing from it is what we aim to build on top of it, but that's stuff that can wait for now ... we're concentrating on getting to that framework in the first place at the moment, namely bringing all those separate pieces you mentioned together and making the look beautiful and work coherently.\n\n> I think he talks to regular people too much\n\nmy therapist keeps telling me this, too. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: What about tenor?"
    date: 2005-08-12
    body: "yes, i've been busy with plasma and a few other kde related tasks. but tenor hasn't been shelved by any means. we've recently been working on bringing kat and its team together with scott, ervin, myself, etc... it seems to be time again for tenor =)\n\noddly, i just blogged about it this morning, and then i come here and see this thread ... woah =)"
    author: "Aaron J. Seigo"
  - subject: "Font Kerning"
    date: 2005-08-12
    body: "As I understand, QT4 should be able to display TT fonts as well as Windows, however looking at the screenshots the fonts still look as ugly as ever.\n\nAnyone have any further info?\n\nThanks..."
    author: "Gogs"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "Just because you saw a screenshot with antialiasing turned off doesn't mean Qt/KDE can't render anti-aliased fonts.\n\nI use Windows TTF fonts under KDE 3.4 and they are very well rendered, crisp and smoothly anti-aliased."
    author: "anonymous"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "Well looking at the screenshot in question, it appears that anti-aliasing is turned on, but that the kerning didn't look right.\n\nI too use KDE 3.4 with anti-aliasing, and I still feel that the fonts don't look as nice as they do in Windows...."
    author: "Gogs"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "Do you understand about the Apple patent issue?\n\nYou need to build FreeType2 from source to have the fonts look as good as Windows.  Perhaps we could talk to Apple about giving us a license.\n\nI don't know about kerning.  We were told that we would have correct font metrics for printing with Qt-4 but now I can't find anything about it.  I have to presume that these font metrics issues are related.  That without correct font metrics that we can't have correct kerning."
    author: "James Richard Tyrer"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "\"as well as\" is a funny thing when talking about antialiasing.  There's no \"correct\" way to do it -- you have to make several decisions on how you want to antialias.\n\nI believe what you want is \"antialiased the same as\", which is plausible.  I personally don't like the antialiasing in OSX, so I hope it's not like that. I've never sat in front of an XP or Win2k machine, so I can't comment on their antialiasing.  I like KDE/Qt/X's antialiasing."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "> as well as\" is a funny thing when talking about antialiasing. \n\nWho is talking about anti-aliasing? Original poster talked about Font Kerning.\n\nTake a look at the strings \"unsermake\" or \"Desktop\" in this screenshot:\nhttp://www.canllaith.org/svn-features/images/kde4-02.png\n\nIMHO the letter \"s\" is too far to the right. \"Desktop\" almost looks like \n\"De sktop\". \n\n"
    author: "cl"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "Heh.  You're right... I failed to read the original comment closely enough. Sorry about that; I'm on a final coding rush for a project here, and I'm skimming news... I should have paid more attention before replying.\n\nKerning does look off in the screenshot.  "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "Guys, you did also notice the strange, misrendered blocks of yellow as well yes? The fact that desktop wallpaper failed to render? This is completely pre alpha software that doesn't even build without a lot of effort. Judging what KDE4 fonts will be from this is just silly =)"
    author: "canllaith"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "> This is completely pre alpha software that doesn't even build without a lot of effort. Judging what KDE4 fonts will be from this is just silly =)\n\nOf course. I know we are discussing pre-alpha code but AFAIK (and please correct me if I'm wrong) KDE isn't much involved in the font layout or rendering. \n\nSo I would expect to see the improvements of Qt4 in font rendering. Even in pre-alpha KDE screenshots.\n\n"
    author: "cl"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: ">the strange, misrendered blocks of yellow \nOh! I just thought you used some strange, non standard color scheme:-) "
    author: "Morty"
  - subject: "Re: Font Kerning"
    date: 2005-08-13
    body: "Heh nope, that's what KDE default (plastik) looks like at the moment. Strange isn't it!"
    author: "canllaith"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "The important question is to why it looks so. Is it because the software used to display the font is not good enough, or is it simply the font who don't have the desired quality. Getting the hinting right in fonts are hard work, and one of the reasons good fonts are really expensive. The Bitstream fonts are not too bad, but the hinting are not all that great and sometimes they kind of break down like in the Screenshot. Compared to some commercial alternatives they still have some potential for improvement.  "
    author: "Morty"
  - subject: "Re: Font Kerning"
    date: 2005-08-12
    body: "> Is it because the software used to display the font is not good enough, or is it simply the font who don't have the desired quality.\n\nGood point! \n\nIt would be great if the article writer would create a screenshot of the same directory in KDE 3.5. This could give us at least a hint if it is a problem in Qt4 or if Qt3 shows the same behavior."
    author: "cl"
  - subject: "SVN digest"
    date: 2005-08-12
    body: "I assume this article is not part of the \"This month in SVN\" series. Am I right?"
    author: "Bram Schoenmakers"
  - subject: "Re: SVN digest"
    date: 2005-08-12
    body: "Nope, 'This Month in SVN' is arriving for August before I go on holiday next week =). This short article is to explain a few things about Qt4 as a percursor to starting to monitor KDE4 development in the SVN series in months to come."
    author: "canllaith"
  - subject: "Do we want to go off the QT 4 cliff"
    date: 2005-08-13
    body: "QT4 on Linux is at best in beta stage, the performace is so slow as to not even be usable. \n\nUnless you link to the qt3 libraries in qt 4 virtually every line of QT in KDE will have to be rewritten.  \n\nMight be best to wait, perhaps even fork QT3 to a KQT and do something over time that doesn't break everything"
    author: "minty"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-13
    body: "> the performace is so slow as to not even be usable\n\nperformance is perfectly acceptable here and i'm not even on a particularly powerful laptop.\n\n> Unless you link to the qt3 libraries in qt 4 virtually every line of QT in\n> KDE will have to be rewritten\n\nhaving ported kde code to qt4, i can say that this is hyperbole\n\n> perhaps even fork QT3 to a KQT and do something over time that doesn't break\n> everything\n\nthis would make 0 sense. there's no need to fork qt3 if we wished to stick with it (we'd just keep using the qt-copy in the 3.5 branch), but that would prevent us from taking advantage of the benefits of qt4 of which there are more than enough to make that a desirable goal.\n\nrealize that kde4 is probably ~18 months away so there will be at least a qt 4.1 release before then if not a 4.2."
    author: "Aaron J. Seigo"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-14
    body: "> realize that kde4 is probably ~18 months away \n\nGood to see a more realistic estimate.  \n\nBut, isn't more than a year a long time to go without anythings but bug fix releases?"
    author: "James Richard Tyrer"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-15
    body: "If you want we can delay KDE 3.5 release nine months after freezing. Would this please you more?"
    author: "Anonymous"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-15
    body: "Very funny. :-D\n\nWhile I don't see that we need to be in a hurry to release 3.5 -- waiting perhaps an other month for the feature freeze wouldn't hurt.  My point is that we should *consider* that we might want to do a 3.6 release -- especially if 4.0 is delayed."
    author: "James Richard Tyrer"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-15
    body: "Rather a higher 3.5.x will have a relaxed feature/message freeze."
    author: "Anonymous"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-15
    body: "> My point is that we should *consider* that we might want to do a 3.6 release -- especially if 4.0 is delayed.\n\nA KDE 3.6 release would delay KDE 4.0 even further. Again, we *don't* have the man power for another KDE 3 release. After KDE 3.5, everybody will concentrate on KDE 4.\n"
    author: "cl"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-15
    body: "> we *don't* have the man power ...\n\nThere are many that would tell you that this statement is questionable.  More man power (and woman power :-)) is available than core developers think.\n\n> After KDE 3.5, everybody will concentrate on KDE 4.\n\nPerhaps we can put new people on the bug fix releases for 3.5.x then.\n\nAfter they get all the bugs fixed, they will have to do something while we wait for 4.0. :^)"
    author: "James Richard Tyrer"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-14
    body: "18 months? shit, that's still a long time... KDE 3.5 september, than still more than a year? sucks... at least i hope it won't be delayed much more!"
    author: "superstoned"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-14
    body: "Wow, I would say such a long time period might even be suicide for the KDE project.  I have bad memories of the KDE 1 to 2 switch.  I don't think doing that a second time is a good thing.\n\nKDE 4 should just be a port of KDE 3 to Qt 4 with some small cleanups.  If things like Plasma are going to make it take 18 months, then that should be left for KDE 4.1 or KDE 4.2."
    author: "KDE User"
  - subject: "Re: Do we want to go off the QT 4 cliff"
    date: 2005-08-14
    body: "well, i feel i must agree... altough i'd love to get my hands on a KDE4 with full tenor integration, reworked conqueror, totally different desktop (plasma) and full hardware acceleration, if this would take 18 months, i'd rather have a KDE4 with just some of these :D"
    author: "superstoned"
  - subject: "Why stay with  Trolltech"
    date: 2005-08-15
    body: "I am confussed by your statement that you have ported KDE to QT4 and yet you say it is going to take 18 months. And 18 months is only an estimate.  QT 4.0 missed its window by almost a year and it is still a beta product.  Whose to say how long it will take to port all of KDE over.\n\nThe API did change a lot, even the most basic way to create a widget changed.  As it no longer takes an argument for a name.  Openoffice and Mozilla were smart not to build upon QT not because QT is a bad product but Trolltech does not understand its customers and their need for stability.\n\nIt makes sense for KDE to fork QT now as Trolltech has really changed the game so much.  A KQT can be evolved in a reasonable way over time so that not every pice of code has to be rewritten. The only things KDE looses are Windows and Mac compatability that QT offers, but does KDE really care about this.\n\nEventually KDE will fork a version of QT, as I doubt that Trolltech will be in business as we know it today. I mean here they are in their hey day and they still can't make money, or so I surmize by their need of second round funding.\n\n\n \n"
    author: "minty"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-15
    body: "> The API did change a lot, even the most basic way to create a widget changed. As it no longer takes an argument for a name.\n\nThe API did change but if you think that is \"the most basic way to create a widget\" I think you are overstating.\n\nw = new QWidget( parent, \"MyWidget\" );\n\nHas become:\n\nw = new QWidget( parent );\nw->setName( \"MyWidget\" );\n\nIt's not that fundamental a change really is it?  It could be done with a sed script.  It seems like a perfectly acceptable change to me.  Why?  Because the code is more readable.  Putting settings in a constructor is never nice.\n\nIt sounds to me like your complaint is that Trolltech broke the API between qt3 and qt4.  Well of course they did - it would have been Qt3.6 (or whatever) if the API were the same.  The Qt3 API was stable for a good long time; as I'm sure the Qt4 API will be."
    author: "Andy Parkins"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-15
    body: "> I am confussed by your statement that you have ported KDE to QT4\n\nHe wrote \"KDE code\", not \"KDE\" as in complete desktop. Learn to read!\n\n> and yet you say it is going to take 18 months.\n\nBecause KDE 4 is more than just porting to Qt 4.\n\n> QT 4.0 missed its window by almost a year and it is still a beta product.\n\nIIRC it was more half a year and it's released already.\n\n> Trolltech does not understand its customers and their need for stability.\n\nYou're trolling, you're not even Qt spelling correctly. How long was Qt 3 line the current and how many years will Qt 3 be continued to be maintained? There is not even set an end of maintenance for Qt 3 yet.\n\n> It makes sense for KDE to fork QT now as Trolltech has really changed the game so much.\n\nChanged game? Hu? Made Qt/Win GPL maybe?\n\n> Eventually KDE will fork a version of QT\n\nNo.\n\n> I mean here they are in their hey day and they still can't make money, or so I surmize by their need of second round funding.\n\nYou don't have a clue about how such stuff works..."
    author: "Anonymous"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-15
    body: "thanks for saving me the typing =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-15
    body: "The fact that the creation of QWidget changed and no longer supports a name argument is just an example of how much the code did change in rather maininless ways.  Setting colors, setting window captions all had cosmetic changes , which as someone stated can be modified with a sed script.  Well perhaps it can, I eman lok at the tool QT provices qt3to4.   The last time I ran this script which was 4 months ago, over 50 percent of QT3 example applications failed to be converted.  So I am not a big believer in Script toools that will magically bring your code up to date.\n\nAll I am saying is that Trolltech needs to think of end users in the commercial space. Who unlike academic and government labs do not have time nor money to contantly keep changing their codes to reflect Trolltech fashion of the day.\n\nAlthough I do not work for Trolltech like some here, I am biased, as I convinced this company I work for to purchase QT. We have some real business concerns about using QT.  Why because QT3 is being put out to pasture, and QT 4 from our codingin experiences is still a beta product.  \n\nLets just see how long it takes for KDE, KOFFICE and all the KDE apps to be migrated to QT4.  And I hope that when it doees happen QT4 is not made obsolete with QT5.  "
    author: "minty"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-15
    body: "Please spell check your posts in the future, thanks."
    author: "ac"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-16
    body: "Sometimes making an improvement requires breaking backward compatibility.\n\nIf you don't like that, then perhaps you should consider getting out of the computer industry (or almost any industry, for that matter).\n\nWould you care to rant about how your ISA SCSI controller won't fit in a PCI-Express slot? How about how nobody makes parts for '85 Caprice Classics anymore?"
    author: "Dolio"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-15
    body: "trolltech is a nice company that writes a superiour toolkit, I love the way they deal with things in general and qt4 is nothing more then a needed evolution of there product. and what they did makes perfect sense."
    author: "Mark Hannessen"
  - subject: "Re: Why stay with  Trolltech"
    date: 2005-08-18
    body: ">> Trolltech does not understand its customers and their need for stability.\n\n>You're trolling, you're not even Qt spelling correctly.\n\nNo doubt.  And if you check >mintys< e-mail address, aol user.  Traditional evidence of a clueless internet user, probably singed up via Windows ME.\n\nAs far as stability, I've been impressed with the ease of porting code between different versions of Qt.  It is quite clear Trolltech pays much attention to their customers, especially when compared with tools like Microsoft VB (end of the road for their users, and they are not happy), and the numerous, annoying times my Java code has broken with new releases of the JDK.  Ask anyone about trials and tribulations with XML.\n\n-Mike"
    author: "Michael O'Keefe"
  - subject: "Endogenous buzz"
    date: 2005-08-13
    body: "This buzz explanation is itself generating lots of buzz (finally, as someone said). Keep it up! :) What good would be a great DE without people fining out about it?"
    author: "mhn"
  - subject: "listviews in qt 4"
    date: 2005-08-13
    body: "i really hope they did something to improve speed of listviews in qt4... \nsearching, filtering oder otherwise working on listviews is soooo slow!\nhow come the qtable widget is advertised with being able to scale a square milllion cells without problems, but filtering a listview with >10k items freezes the app for a second?"
    author: "hannes hauswedell"
---
With all the excitement surrounding KDE 4 development at the moment people are starting to ask why they have not seen any updates on what KDE 4 will look like.  <a href="http://www.canllaith.org/svn-features/kde4.html">KDE 4 - Understanding the Buzz</a> answers these increasingly common questions by explaining the current status of KDE 4 development and why the exciting work so far is only visible to developers.  "<em>Before any new features can be added to KDE and projects like Plasma can get underway, the porting of KDE to Qt 4 has to be completed.</em>"










<!--break-->
