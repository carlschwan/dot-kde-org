---
title: "KDE 3.5 Released"
date:    2005-11-29
authors:
  - "tchance"
slug:    kde-35-released
comments:
  - subject: "Great release"
    date: 2005-11-29
    body: "One big improvement for me is Kopete :)\n\nThanks KDE Team"
    author: "JC"
  - subject: "Re: Great release"
    date: 2005-11-29
    body: "I agree, the new release of Kopete seems great.  I think its safe to finally say 'bye bye gaim' ;-)."
    author: "Corbin"
  - subject: "Re: Great release"
    date: 2005-11-29
    body: "Do http proxies now work?"
    author: "Anonymous Coward"
  - subject: "Re: Great release"
    date: 2005-11-29
    body: "bof... we can't sent file with the icq plugin in kopete... that a very big lack"
    author: "collinm"
  - subject: "Re: Great release"
    date: 2005-11-29
    body: "The biggest in kopete's case, sadly."
    author: "morten"
  - subject: "Re: Great release"
    date: 2005-11-29
    body: "Don't forget the inexistence of visible/invisible lists.\nI have to create a new msn for home, then added people I like to see me online, and just removed the old msn from home (just for work now).\nVoice in msn is sadly adscent too.\nKopete is still behind sim-icq in my opnion, but sim just was discontinued :("
    author: "Iuri Fiedoruk"
  - subject: "Re: Great release"
    date: 2005-11-29
    body: "i hope will we soon be able to send file with icq plugin in kopete...\n\na lot of people wait this feature"
    author: "collinm"
  - subject: "Re: Great release"
    date: 2005-11-30
    body: "Well, Gaim has it now, can't Kopete use that? I know it's built differently, but the idea is probably the same.."
    author: "Hobbit HK"
  - subject: "Re: Great release"
    date: 2005-12-02
    body: "sim-icq, also a GPL software can do that also.\nBut you know, the truth is that most open source projects never trade source or help each other :-P"
    author: "Iuri Fiedoruk"
  - subject: "Re: Great release"
    date: 2005-12-02
    body: "Code usually isn't easily transplantable from one project to another. Assuming the file transfer protocol is documented somewhere ( likely since gaim and sim/icq can both work it out ), someone will still need to code support around kopete's internal structure. "
    author: "Greg"
  - subject: "Re: Great release"
    date: 2005-12-13
    body: "not the biggest.\n\nI could count all occurences when i wanted to send someone a file via any kind of IM on the fingers of my left hand, and still have enough left to play a quint on the piano (not that i can play the piano at all but you get my point).\n\nbut what I really lack in kopete a way to block people from spamming me.\n\nas it is now, any unknown icq user can send me spam, without even being on my contact list (because that would need my auth and joe j spammer doesnt get my auth. no way in hell. never ever.), and whats even worse, the kopete guys chose to ignore the issues for reasons of \"we wont implement ignore for protocols that don't do it natively\". Hell, they dont even recognize (or maybe they ignore) the duplicates of the bug report asking for that feature..."
    author: "Mathias"
  - subject: "Re: Great release"
    date: 2005-12-13
    body: "http://bugs.kde.org/show_bug.cgi?id=57234 and http://bugs.kde.org/show_bug.cgi?id=63839 if you want to see what i mean."
    author: "Mathias"
  - subject: "Food for the Marketing Workgroup"
    date: 2005-11-29
    body: "Nice work, everybody. It will be very interesting to see if the new marketing workgroup will be able to make this release an even bigger splash than 3.4 was.  \n\nGo MWG!!  Make headlines!  Capture users!"
    author: "Inge Wallin"
  - subject: "Re: Food for the Marketing Workgroup"
    date: 2005-11-29
    body: "Capturing new users will be easier than ever, thanks to the combination of 3.5's features and Martijn Klingen's new tranquilizer darts.\n"
    author: "Wade Olson"
  - subject: "kudos"
    date: 2005-11-29
    body: "the way konqui reports about new media (cd / dvd /usb) is great. \nmany big-time<br>\n<pre>TTTTT&nbsp;H&nbsp;H&nbsp;&nbsp;&nbsp;A&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp;N&nbsp;XX&nbsp;XX\n&nbsp;&nbsp;TT&nbsp;&nbsp;HHH&nbsp;&nbsp;AAA&nbsp;&nbsp;NN&nbsp;N&nbsp;&nbsp;&nbsp;X\n&nbsp;&nbsp;TT&nbsp;&nbsp;H&nbsp;H&nbsp;A&nbsp;&nbsp;&nbsp;A&nbsp;N&nbsp;NN&nbsp;XX&nbsp;XX.&nbsp;\n</pre>\n\n\n"
    author: "eol"
  - subject: "Couple of years?"
    date: 2005-11-29
    body: "Do you really think that with no significant upgrades for two years the KDE project could stay alive without a loss in mindshare, if not users? Do you think that KDE 3.5 will serve users' needs for the next two long years without no major upgrades (apart bugfixes release)?\nI hope to see one or two \"interim\" stable release before the drastic changes with KDE 4. Maybe call it KDE 3.9."
    author: "ac"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "Don't overinterprete everything said there. So far there isn't even a release schedule available yet. And I guess that I'm not the only one who thinks that a two years period would put the whole project at risk. Especially among long-time KDE contributors a release aimed at the end of next year seems to be favoured. So don't worry. :-) "
    author: "Torsten Rahn"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "3.5 will serve many in the next couple of years. Keep in mind that a major distro like Debian is still using KDE 3.3.x.\nKDE 4.0 will be released in about a year. There is no exact roadmap but something like Q3 or Q4 2006 sound likely. I guess that is a \"significant upgrade\" :-)"
    author: "Carsten Niehaus"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "Indeed, that's what is meant here.\n\nKDE 4 will be released in late 2006 or maybe even 2007 (we don't know yet). This means KDE 3.5 will survive for at least another 18 months while people upgrade. Also, existing production environments may decide to withhold updating for some more time, given that KDE 4 will have radical changes."
    author: "Thiago Macieira"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "The couple of years figure are not a set date for KDE4, it also includes realistic migration time of distributions and applications before it's common among users. Personally I'm expecting the first alphas and betas 6-8 months from now.\n\nBesides you are forgetting one very important thing when it comes to users need. The interresting part are not the core desktop system really, it's nice and all, but what's important are the applications. The way to continue increase the mindshare are to keep up the stream of high quality applications. \n\nSince it's to early for most application developers to start porting to KDE4, this will happen. For instance a new and improved KOffice version is set to be released in a few months. And other high quality applications keep releasing at a regular pace, like amaroK, digiKam, K3b, Codeine, Kaffeine, Krusader, KTechlab, kdissert, Kile, Tellico, KMyMoney, TaskJuggler, KNemo and the list goes on."
    author: "Morty"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "  > Since it's to early for most application developers to start porting to KDE4, this will happen. For instance a new and improved KOffice version is set to be released in a few months.\n\nTo avoid misinterpretation; the above is about KOffice version 1.5.  This version will be available for KDE3 users and will now use any kde4 ported stuff."
    author: "Thomas Zander"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "now = not\n\nAvoiding misinterpretations you are. =P"
    author: "ac"
  - subject: "Re: Couple of years?"
    date: 2005-11-29
    body: "I think that if KDE-4.0 is delayed past the end of 2006 that KDE-3.6 should be released.  Perhaps it will be necessary to have a 3.6 release for technical reasons in any case.\n\nE.G.: I was told that I can't fix some of the bugs in KView because that will require changes in the documentation which can NOT be changed except for a new minor release (i.e. 3.6.0).\n\nOther than that, I don't see that there are any major new features that are needed for the KDE-3 branch -- that we should concentrate of finding the bugs, fixing the bugs and otherwise polishing the product.\n\nI also think that there should be a final release of the KDE-3.4 branch since some work was done on it after the 3.4.3 release and some people continue to use the older branches for stability reasons."
    author: "James Richard Tyrer"
  - subject: "Re: Couple of years?"
    date: 2005-11-30
    body: "At some point you have to shift the focus of the development to where people are concentrating on a specific release.  Bug fixes and so on can go into old releases -- that's why we have bugfix releases -- but it becomes too tedious to have feature development in two quickly diverging branches (for the things that have already started porting towards KDE 4 this is already the case)."
    author: "Scott Wheeler"
  - subject: "The Applications will see Updates!"
    date: 2005-11-29
    body: "Konqueror 3.5 works (Ok, more or less, but there is Firefox, too)\nKicker works\nKdesktop works\nKIO-slaves, Dcop work\nKGetHotNewStuff works\n\nSo, why do you want a new KDE release?\n\nI'm not looking forward to KDE 3.6 but I'm looking forward to the next releases of\n\nAmarok, Digikam, Krita, Firefox, Openoffice, Gimp, Kopete, Speedcrunsh, K3b, Hugin, Gwenview,...\n\nOK, one or two independent Konqueror (+khtml) releases would be nice :-)\n"
    author: "Hans"
  - subject: "Re: Couple of years?"
    date: 2005-11-30
    body: "I hope KDE4 is the next release, that way the developers can focus on developing it instead of working on other releases in between.\n\nKDE4 will rock!\n\nWe just have to be patient.\n"
    author: "Mark"
  - subject: "Re: Couple of years?"
    date: 2005-12-04
    body: "That's exactly what KDE 3.5 was.\n\nWith only one branch being worked on, everyone will be able to concentrate on progressing to KDE4 and getting their applications ready.\n\nWe could of course go through 3.6, 3.7, 3.8, 3.9... 'just to keep the stable users happy', but each one of those will set back KDE4 another 9 months."
    author: "Robert"
  - subject: "Happy Birthday to Me!"
    date: 2005-11-29
    body: "Yay!\n\nI forget --and haven't looked it up-- is this the update that finally fixes the \"why won't those damn desktop icons stay put!\" bug?"
    author: "Praxxus"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "There were many bugfixes and yes, one was a famous desktopicon-bug. But if you don't describe 'your' in a little more detail I am not 100% sure."
    author: "Carsten Niehaus"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "If you are talking about the bug where hiding and re-showing the panel caused all the desktop icons to move upwards a little bit, then yes, it's been fixed :)"
    author: "Jim"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "My icons still move around randomly if kicker is on the left. Try dragging kicker around, especially from the bottom to the left and back. Or is my system messed up somehow? "
    author: "uddw"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "The icons move when I drag kicker to another side of the screen, but merely hiding and re-showing it doesn't move the icons, even if it's on the left."
    author: "Jim"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "If I drag my taskbar from the bottom where it normally is to the left side of the screen.  It will not 'unhide' unless I move my mouse to the bottom of the screen.  I have the 'unhide' set to bottom edge but shouldn't this follow the edge the panel is on?\n"
    author: "Mike Perik"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-30
    body: "What would be the point of setting it to the bottom edge then? Remember computers aren't psychic and are only capable of doing what we tell them to do. \nNow if there were a \"Show panel when mouse gets to side of the screen where the panel is hidden\" option you should expect this. There, of course, is not."
    author: "illogic-al"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "moving the icons when the panels change configuration is purposeful. it's simply meant to help prevent them from jiggling around when panels simply hide and show."
    author: "Aaron J. Seigo"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-12-07
    body: "Icons shouldn't move when you hide and unhide a panel.\n\nHOWEVER, an  autohide panel should NOT have space left for it to unhide.  It should cover icons when it unhides.  Otherwise, what is the point of having an autohide panel.\n\nYou  appear to have introduced a serious bug by having the icon positions affected by an autohide panel.  With the initial icon arrangement on the desktop, this isn't much of a problem but with multiple autohide panels and a lot of icons, it has made my desktop unusable."
    author: "James Richard Tyrer"
  - subject: "Re: Happy Birthday to Me!"
    date: 2005-11-29
    body: "I don't think it has anything to do with Kicker or the panel.  I don't autohide kicker, nor do I really move it around any.  My icons just never stay where I put them.  Right now my Trash is at the top of my screen, near the left corner.  The last place *I* put it was near the bottom right corner.\n\nAt one point I had seen something about \"icons now stay where you put them\" in one of those DevLog web updates.  Hopefully that has been fixed.\n\nI will find out soon enough . . . . *emerge emerge emerge*"
    author: "Praxxus"
  - subject: "Congrats!"
    date: 2005-11-29
    body: "Congratulations to the complete KDE team for their good work and hard efforts to get this done. KDE is and will be the best desktop out there. Keep up the good work!"
    author: "Dennie"
  - subject: "Re: Congrats!"
    date: 2005-12-05
    body: "Yes, congrats and keep up the good work!"
    author: "MikeGR"
  - subject: "Arch Packages"
    date: 2005-11-29
    body: "The Arch packages haven't been moved out of testing yet, so if you follow the link you will only see 3.4.3 still.  But I've been running 3.5 on Arch testing for two weeks and it is sweet."
    author: "oggb4mp3"
  - subject: "Re: Arch Packages"
    date: 2005-11-29
    body: "They're available now."
    author: "Darin"
  - subject: "Re: Arch Packages"
    date: 2005-11-29
    body: "If you're talking about \"Arch Linux\", you should clarify that.  It took me quite a while to realise that you weren't talking about something to do with  Tom Lord's Arch."
    author: "Lee"
  - subject: "Kopete compatibility with Gtalk/Jabber?"
    date: 2005-11-29
    body: "I love Kopete, but I had to switch back to GAIM in order to chat with friends who are on Google's Gtalk (or whatever they call it). I would love to switch back to Kopete. Can I do so with 3.5?"
    author: "Wolfger"
  - subject: "Re: Kopete compatibility with Gtalk/Jabber?"
    date: 2005-11-29
    body: "Read here:\nhttp://wiki.kde.org/tiki-index.php?page=Google+Talk+support"
    author: "ac"
  - subject: "Certificate of Authority invalid"
    date: 2006-10-14
    body: "I got a certificate of authority invalid when trying the steps listed on the above page."
    author: "Anthony Ettinger"
  - subject: "fix bugs before adding new features"
    date: 2005-11-29
    body: "I would prefer, that developers concentrate on fixing bugs than adding new features.\n\nThe same goes for Firefox :-(\n\nI would love a stable system in favour of a system that has all the bells and whistles. I can't count how many times konqueror has crashed when broswing or working locally as a filemanager. \n\nDon't follow microsoft in that way.\n\nBTW: I will continue to use KDE 3.3.2 und will perhaps upgrade to release 3.4. I don't think that the new features justify to install KDE 3.5."
    author: "richie"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-29
    body: "Maybe if you upgrade to 3.5, you'll find the bugs have gone away?"
    author: "Charles Samuels"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-30
    body: "I am quite conservative regarding updating my system. A new KDE version will certainly introduce new problems.\n\nShall others fight with new bugs. I can wait until the dust settles.\n\nThat might sound cynical, but I am not that person, which must have the latest\nthings around. If it works more or less why should I change something?\n\nRegards,\n        richie\n\np.s. Happy with my MTB GT \"Tequesta\" built 1989 :-)"
    author: "richie"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-30
    body: "Well, there's a certain aspect to \"known bugs are better than unknown bugs\" (even if there are fewer).\n\nHowever, KDE 3.5 is very stable, (even not counting that it's a x.y.0 release)!  This is likely due to how it's mostly a \"polish up\" release as development was shifting to KDE 4.0.\n\ncs"
    author: "Charles Samuels"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-29
    body: "Richie, that's the power of freedom. You can stick to an older\nversion, update part of your system, etc. Hell, you can choose\na distro with focus on stability (rather than a bleeding edge).\nThere is a universe of possibilities. \n\nKDE as a project, though, seems to be doing pretty well. \n3.5 will add new features, and 3.5.x will fix bugs in the 3.5 codebase\n\n4.0 will introduce fundamental (architectural, etc. changes). \nAnd the 4.1, 4.2, etc release series will introduce incremental changes\n\nSeems pretty solid software engineering. \n\nIn fact, let me go a bit OT and say that the linux kernel, OTOH,\nseems to be a different story. Rather fundamental changes are \nbeing introduced in the 2.6 series periodically (for each 2.6.x).\nThe changes are flowing to frequently and there is little time\nto stabilize. This does not seem a good process, at least to me. "
    author: "MandrakeUser"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-30
    body: "I can't argue with you. What you said here is true. For me a stable system is more important, than to have the latest stuff around. \n\nLast time I had upgraded from KDE 3.2 to KDE 3.3.2 I had big problems\nwith upgrading my userprofile. I managed to solve it myself, but it was a major hassle. Things like that should be adressed, so that a upgrade makes less headaches.\n\nI can wait. And if I am feeling bored, I will try out the new release. As I noticed by running a live CD KDE 3.5 feels faster than older releases.\nThat is a good sign :-)"
    author: "richie"
  - subject: "Speaking of bugs...."
    date: 2005-11-29
    body: "If you look at the very last picture in the Visual guide, showing the new-media notifier, you'll see that the \"OK\" button is shorter than the buttons on either side of it. Qt's layout helps maintain the text baseline but, if it's still present in the actual release, it's a silly little mistake to have made.\n\nThe screenshot is http://www.kde.org/announcements/visual_guide_images-3.5/device-popup.png"
    author: "Bryan Feeney"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "The option to make command buttons all the same width is something that Qt has never been able to do.  I think some themes try to do it, not sure how well they succeed.\n\nI fully agree with you; its ugly and since Windows has been doing the equal-width thing for a couple or releases already Qt (or KDE) really should follow."
    author: "Thomas Zander"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "You can enforce this through the style by defining a certain minimum width for command buttons. We do that for Windows style and it covers most cases. The case in the screenshot would be covered well. \n\nAnother problem is really plastik's, and you see it in the screenshot: the default button appears smaller in all four directions. That's space taken away for the default button indicator. Qt's own Motif and Windows styles do it better: they take the same space away from all autoDefault buttons, so all buttons appear to have the same hight."
    author: "Matthias Ettrich"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "> Another problem is really plastik's, and you see it in\n> the screenshot: the default button appears smaller in all\n> four directions. That's space taken away for the default\n> button indicator.\n\nThe default button indicator should have a nice colour and not the background colour!!"
    author: "Hans"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "I wasn't talking about width, I was talking about *height*. Matthias seems to have explained what's wrong though (it's Plastik's fault ;-) ).\n\nTo be honest, I don't really mind variable width; so long as buttons don't get too narrow I don't see any issue. "
    author: "Bryan Feeney"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "He was talking about height, and unless there's a very good usability reason for it it looks like arse. How the hell has no one seen that?"
    author: "Segedunum"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "The big problem here of course is not the button -- switching to another style will help there -- but the wording. It should have said \"An audio CD has been inserted.\" All the verbosity about mediums (new or otherwise) and what do you want to do? is superfluous. I'd also use \"this\" instead of \"that\" in the \"Always...\" message. And I'd left-align the \"configure\" button to create some distance between the ok and cancel button, and reword the cancel button to \"Do nothing for me, and remove the silly icon from my desktop, too, please, and be right snappy about it, cocky\". Or perhaps just \"Close\""
    author: "Boudewijn"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "Errrr, you're avoiding the point. The OK button is smaller in height to all the other buttons around it. It looks like absolute crap unless there is a goo usability reason for it. Seriously - no one picked up on this?"
    author: "Segedunum"
  - subject: "dupe"
    date: 2005-11-29
    body: "Please scroll up a little bit and you'll find that someone going by the name of \"Segedunum\" already posted the exact same thing you did. Only he used the word \"arse\" rather than \"crap\".\n\nHave a very nice day!"
    author: "ac"
  - subject: "Double Dupe"
    date: 2005-11-29
    body: "\"Please scroll up a little bit and you'll find that someone going by the name of \"Segedunum\" already posted the exact same thing you did. Only he used the word \"arse\" rather than \"crap\".\"\n\nGee. If you scroll up a little bit further you'll also see another comment that doesn't answer the question either! It seems as though some people may be a little embarrased they didn't notice this.\n\nHave a nice day!"
    author: "Segedunum"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "The button is a function of the Plastik style; if you use another widget style, then you won't have this problem. And even with Plastik it may look bad, but it isn't half as bad as the wording of the dialog. Upon consideration, I think the cancel button can go, too, since the cancel action is already present in the option list. "
    author: "Boudewijn"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-29
    body: "\"The button is a function of the Plastik style; if you use another widget style, then you won't have this problem.\"\n\nSigh. Plastik is the default style of KDE........"
    author: "Segedunum"
  - subject: "Re: Plastik!"
    date: 2005-11-29
    body: "My KDE shows a blue line around the \"OK\" button (which is very good!).\nI did not do any special. Style: plastik, Colour scheme:plastik.\n\nMaybe it's a bug con the computer where the screenshot has been taken."
    author: "Hans"
  - subject: "Re: Plastik!"
    date: 2005-11-29
    body: "\"My KDE shows a blue line around the \"OK\" button (which is very good!).\nI did not do any special. Style: plastik, Colour scheme:plastik.\n\nMaybe it's a bug con the computer where the screenshot has been taken.\"\n\nIs this KDE 3.5? I sincerely hope so. I haven't used an RC of 3.5 nor have I checked out SVN for quite a while so I don't know.\n\nIf it is just something wrong with the set up of the machine that the screenshots were taken on can somebody just say so?"
    author: "Segedunum"
  - subject: "Re: Plastik!"
    date: 2005-11-30
    body: "Ok, I did investigate a bit:\n\nThis bug can only be seen in some applications, for example the \"what do you want to do\" dialog or the \"file already exists\" dialog of Konqueror.\nThe bug can not be seen on the buttons of the kontrol center.\n\nHow to reproduce:\nOpen konqueror in file manager mode.\nKlick on a file, hold mouse down, press <ctrl> and drop it anywhere in the same directory. Konqueror asks: \"File Already Exists<br> This action would overwrite...\"\nThe \"suggest new name\" and \"Continue\" buttons have the same size, the preselected \"Cancel\" button shows the reduced size. Press <tab> two times and the \"Cancel\" button becomes normal again.\n\nIn Kcontrol or in this web form buttons stay always the same size regardless if they become activated (<tab>) or not.\n\nIf you select another style (for example \".net\") this bug disappeares."
    author: "Hans"
  - subject: "Re: Plastik!"
    date: 2005-11-30
    body: "Hmmm. Interesting, thanks."
    author: "Segedunum"
  - subject: "damn right..."
    date: 2005-11-30
    body: "imho your suggestions would really improve this little dialog. Please fight for it, make a report or *heyho* would one of the devs please obey to his recoms they make the pain go away...."
    author: "zero08"
  - subject: "Re: Speaking of bugs...."
    date: 2005-11-30
    body: "Checking my own computer (also 3.5) the problem is that the actual height of the buttons is exactly the same, but the visual size is indeed smaller for the default button.  This means that there are some very light lines around the default button that take space away from the button itself.\nThe obvious solution would be to make the button larger. Not sure how well that would work, though.\n\nI guess I have gotten used to it and never noticed it :("
    author: "Thomas Zander"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-29
    body: "It's up to the individual developers whether they fix bugs or add new features. No one is there to force them to do one or the other. It's called freedom.\n\nThat said, bugs are still getting fixed. No competent developer ignores reproducible bugs in release software. They do fix a lot of bugs. Some they can't fix, because it's outside of their area of expertise. Others may be minor and not worth the risk to fix in the 3.x codebase (every bug fix has the potential to introduce new bugs). Still others are currently being worked on, and will be merged in for 3.5.1. Then there are those few bugs that are design/architecture related, that must be fixed in the 4.x codebase.\n\n3.5 is a feature release, and the focus is on new features. KDE could keep going on and on and on with 3.4.4, 3.4.5, 3.4.6, 3.4.7, and so on. But without new features users lose their excitement and developers get bored. This is especially true with a high visibility project like KDE. Thus every so often there is a feature release."
    author: "Brandybuck"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-07
    body: "> No competent developer ignores reproducible bugs in release software. \n\nThen should I believe that the developers that ignore (or  worse summarily close) my bug reports are not competent? :-)\n\n> KDE could keep going on and on and on with 3.4.4, 3.4.5, 3.4.6, 3.4.7, \n> and so on.\n\nAnd if we did that we would eventually have the most stable and bug free software on the market.  Some users would prefer that to new features that don't work and the serious regressions that seem to come with them.\n\nKDE 3.5 totally breaks my desktop due to a serious regression, and several serious issues which I have reported have not been fixed."
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-07
    body: "> KDE 3.5 totally breaks my desktop due to a serious regression, \n> and several serious issues which I have reported have not been fixed.\n\nWhich regression/issues are those? Could you quote bug numbers?"
    author: "Paul Eggleton"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-07
    body: "The serious regression which makes my desktop unusable is: Bug 116562\n\nSome unresolved serious issues, 53052, 59901, 06542, 108510, 107087"
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-08
    body: "Personally I would class these bugs as \"annoying\", not \"serious\". I certainly think \"unusable\" would be going too far."
    author: "Paul Eggleton"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-08
    body: "Please look at the screen shots that I attached to bug 116562.\n\nMy previous desktop setup is now unusable."
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-08
    body: "From the bug report it looks like the size of panels is not correctly handled when creating a no-icon placement zone. Rather than getting it correctly from panel/icon size ratio, it looks like it uses a \"better safe than sorry\" approach. A bug creating some annoyance for some users, but IMHO it's preferable to the bug of having panels overlapping icons when unhidden. \n\n>My previous desktop setup is now unusable.\n\nWhich I guess is very annoying for you, but not serious. Afterall you can simply reorder your icons.\n\nAnd I agree with Paul's classification of those other bugs as annoying too. Not that they should not get fixed, but they are far from serious. Besides it's grate you help by folowing up your own bugs, it's far to many stale reports in there. Hope you close those that do not longer apply, too:-)"
    author: "Morty"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-09
    body: "I have posted two more screen shots to bug 116562.\n\nSpecifically, please look at:\n\nhttp://bugs.kde.org/attachment.cgi?id=13831&action=view\n\nI have added yellow cross hatching to show the very limited area of the desktop where I can place icons.  So, although this bug doesn't even matter with the default setup with 3 icons and one panel, it totally screws up my rather elaborate setup."
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-09
    body: "Does not look to nice, I agree. But looking at the other screenshot:\nhttp://bugs.kde.org/attachment.cgi?id=13832&action=view \n\nThe top and botton part look correct, and the same for the right side(if that clock of yours is some kind of panel). But the thing in the middle of the screen looks strange, and most likely whats breaking your setup. Without knowing the exact code I guess the no-icon placement code most likely suppose you have the panels at the screen edge. Not having the possibility to have icons on both sides of it. Other than that the distance to the icons look correct. I think you have to move that one to reclaim your desktop space."
    author: "Morty"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-09
    body: "The thing in the middle is a panel which is supposed to be upper left.  The problem is mostly caused by the external taskbar which is lower left.  I also note that the small panel that holds only my clock has: \"Allow other windows to cover the panel\" checked so it shouldn't influence the position of the desktop icons either.  I did the unhidden screen shot to show that the icon positioning is being set by the panels which are hidden just the same as if they were not hidden which is not correct.  Auto hide panels should not influence the position of the desktop icons."
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-10
    body: ">The thing in the middle is a panel \n>which is supposed to be upper left.\n>The problem is mostly caused by the\n>external taskbar which is lower left.\n\nThis is your real bug then.\n\n>the icon positioning is being set by\n>the panels which are hidden just the\n>same as if they were not hidden which\n>is not correct.\n\nNo, it's actually the correct behavior. If not, changing settings to/from hide/no hide of panels would lead to automatic rearranging of desktop icons which is not wanted. And more importantly panels should never cover desktop icons regardless of settings, anything else is a major bug. \n "
    author: "Morty"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-10
    body: "> This is your real bug then.\n\nThat is what I said, space is being reserved for the hidden External Taskbar and that is a bug.\n\n> No, it's actually the correct behavior.\n\nThis is not the way it worked in 3.4.x or, IIRC, any previous 3.x.y version.\n\nTo elaborate on this, in 3.4.x, two hidden panels could be on the same side of the screen as long as there was some way to determine which one would unhide when you did what.  In my case I have the External Taskbar in the left bottom and a Panel in the left top.  Since neither one is full \"length\" (actually height) there was never a problem.  Now in 3.5.0, a full height space is reserved for both of them (full height space is being reserved for the External Taskbar) with the result that the left top Panel has spaced reserved near the center of the screen where it can never go unless both the External Taskbar and the Panel were unhidden at the same time which is usually impossible.\n\n> If not, changing settings to/from hide/no hide of panels would lead to \n> automatic rearranging of desktop icons which is not wanted.\n\nThis behavior not being wanted, might be the case in some instances, but as you can see this is not always the case and does not work with my DeskTop setup.  Therefore, as I said, if this \"feature\" is what is wanted, it is going to have to be a configurable option."
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-29
    body: "> I would prefer, that developers concentrate on fixing bugs than adding new features.\n\nGood news for you, all next releases for foreseeable feature will be bugfix releases.\n\n> I can't count how many times konqueror has crashed when broswing or working locally as a filemanager. \n\nAnd you use the current version (as of yesterday) and reported the bugs?\n\n> I will continue to use KDE 3.3.2 und will perhaps upgrade to release 3.4.\n\nI don't think that anyone will fix anymore bugs in KDE 3.4.x."
    author: "Anonymous"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-29
    body: "> I don't think that anyone will fix anymore bugs in KDE 3.4.x.\n\nOf course, there aren't anymore left =)"
    author: "ac"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-29
    body: "Unfortunately, this is not how KDE works.  If you use OSS, you *are* an unpaid software tester.  While you should resent this with commercial software, you should simply accept that this is the way it works with OSS.\n\nTherefore, you should upgrade to 3.4.3 since bug fixes for the older branches stopped with the release of a new minor branch.  Actually, IMHO, there should only be one final bug fix release for the older minor branch.  E.G. now that KDE-3.5.0 has been released, there should be a KDE-3.4.4 release which incorporates the bug fixes as of the date of the 3.5.0 release, but with OSS, that is all the support that can be expected for the old version.\n\nYes, it is probably true that 3.5.0 will have more bugs than 3.4.3.  Which is why 3.4.4 should be released and why those that value stability more than features should upgrade to it while waiting for 3.5.1.\n\nYou do make a good point though.  We should be careful that stability does not take a back seat to new features.  If we are not careful about this, adding new features will introduce new bugs and if new features are added faster than bugs are fixed, the releases might become less stable.  Naturally, we should strive to have the new releases always be more stable than the previous ones and it is an important thing to keep in mind."
    author: "James Richard Tyrer"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-30
    body: "> If you use OSS, you *are* an unpaid software tester\n\nThis is true for every kind of software because you can only test so much before a release. The advantage of OSS is actually that the release phase is open and everybody can help to find bugs before the release. This is not possible with closed-source software.\n\n"
    author: "Christian Loose"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-11-30
    body: "> Unfortunately, this is not how KDE works. If you use OSS, you *are* an unpaid\n> software tester. While you should resent this with commercial software, you\n> should simply accept that this is the way it works with OSS.\n\nI have no claims, because I didn't pay anything. As I enjoy reporting bugs and helping to improve software, being an unpaid software tester is also no problem.\n\nWith my comment I only wanted address a common problem today. Often before a problem gets fixed new problems are intoduced. If I had paid for a piece of software, I would get pretty angry.\n\nI am grateful, that I have the choice to wait. \n\n> You do make a good point though. We should be careful that stability does \n> not take a back seat to new features. If we are not careful about this,\n> adding new features will introduce new bugs and if new features are added\n> faster than bugs are fixed, the releases might become less stable. \n\nThis is a thing which is important for me. The second one is: Keep things small and simple. \n\nI can't count how many friends had their \"state-of-the-art\" laptops or mobile phones repaired under warranty several times. \n\nAs I don't follow that direction, I have less problems. "
    author: "richie"
  - subject: "Re: fix bugs before adding new features"
    date: 2005-12-04
    body: "\"I can't count how many times konqueror has crashed when broswing or working locally as a filemanager.\"\n\nHave you reported all these bugs? Have you followed them all the way through to fixes?"
    author: "Robert"
  - subject: "Mandriva ?"
    date: 2005-11-29
    body: "Anyone know is Mandriva is gonna package 3.5 ? (Conectiva used to, and now they  merged ... I really hope so)\n\nThanks in advance. Cheers!"
    author: "MandrakeUser"
  - subject: "Re: Mandriva ?"
    date: 2005-11-29
    body: "Already in Cooker."
    author: "Nobody"
  - subject: "Re: Mandriva ?"
    date: 2005-11-29
    body: "Thanks. Although, Cooker packages many times require updating other packages\nto untested versions. It would be nice if they compiled 3.5 for the current\nstable Mandriva releases and post it in the net (as Conectiva used to do,\nor SUSE, Kubuntu and others do)\n\nCheers\n\n"
    author: "MandrakeUser"
  - subject: "Re: Mandriva ?"
    date: 2005-12-01
    body: "Allright, I tried at home. A KDE upgrade to cooker would\nimply upgrading the X server too ... too much risk. I'm not\ndoing it. \n\nI guess I will soon be signing as KubuntuUser_exMandriva\n;-)"
    author: "MandrakeUser"
  - subject: "Re: Mandriva ?"
    date: 2005-12-03
    body: "Well, considering that in mdv2006 Xorg is a cvs release, it is probably not that bad to upgrade it.\n\nYou may also recompile the src.rpm for your actual configuration."
    author: "Renaud MICHEL"
  - subject: "Re: Mandriva ?"
    date: 2005-11-29
    body: "cooker is not stable, it is an unstable development version, not made for users. In other words, you do not answer the question."
    author: "oliv"
  - subject: "Re: Mandriva ?"
    date: 2005-12-02
    body: "Go to forum.mandrivaclub.com on \"General Software\", search for Thac&Z\u00e9 packages (Mandriva Enhanced). I'm using them."
    author: "ivansb"
  - subject: "KDE 3.5 is fast"
    date: 2005-11-29
    body: "...really fast.\n\nI just upgrade from 3.4.3 to 3.5 on my laptop and most of the big apps load faster. I love it.\n"
    author: "JC"
  - subject: "Re: KDE 3.5 is fast"
    date: 2005-11-29
    body: "If this one day goes into gcc/glibc/ld, it will be even faster:\nhttp://sourceware.org/ml/binutils/2005-11/msg00179.html\n\nLet's pray for the gcc developers to accept this.\n\n\nThis fontconfig optimization sounds very effective as well:\nhttp://mail.gnome.org/archives/performance-list/2005-November/msg00094.html\n"
    author: "ac"
  - subject: "Re: KDE 3.5 is fast"
    date: 2005-11-29
    body: "\"Let's pray for the gcc developers to accept this.\"\n\nIt doesn't look as if they're going to, and you could also do it through pre-linking if you wanted to. It would break some stuff though.......\n\nFrom the luminary that is Lubos Lunak it probably won't make much difference to KDE anyway."
    author: "Segedunum"
  - subject: "Re: KDE 3.5 is fast"
    date: 2005-12-01
    body: "What makes you think that the suggested improvement by Michael Meeks equals to your \"pre-linking\" approach?"
    author: "ac"
  - subject: "Re: KDE 3.5 is fast"
    date: 2005-12-01
    body: "\"What makes you think that the suggested improvement by Michael Meeks equals to your \"pre-linking\" approach?\"\n\nApparently, you can actually achieve exactly the same thing that Michael has achieved for dlopened libraries with a 'hacked' prelink. The only reason why prelink doesn't currently work for dlopen is because it would break some 'rules' about how ELF libs are supposed to work, as does Michael's -Bdirect method according to Lubos and having done a bit of reading around. These rules are not too significant really, but if you ignore them you can actually make a hacked prelink work for dlopen which renders his -Bdirect method a little bit redundant.\n\nIt's a pity, because there looks as if there are some gains to be had, even if not a huge amount for KDE over normal prelinking, but because they break some stuff around ELF libs then I guess nothing will get committed - and there seems to be a complete lack of interest. It's a pity Michael's posting got such short shrift. It would have been interesting to see a discussion."
    author: "Segedunum"
  - subject: "Konqueror passes Acid2 - sort of"
    date: 2005-11-29
    body: "So far, 3.5 seems to be faster and apps seem to load faster. My only disappointment is with Konqueror and the Acid2 Test. It almost successfully passes the test, everything is fine when you run the test, the smiley face shows fine, but then if you scroll, it breaks a tiny bit. I have 3.5 installed on Ubuntu Breezy.\n\nGot pics posted here:\n\nhttp://www.ianchristie.info/\n\nThe first is after scrolling, the second is when you just run the test and don't scroll.\n\nStill excellent work. I think it might have just stollen my desktop from XFCE."
    author: "Ian Christie"
  - subject: "Re: Konqueror passes Acid2 - sort of"
    date: 2005-11-29
    body: "I seem to recall that the test was supposed to go wonky when you scroll."
    author: "Brandybuck"
  - subject: "Re: Konqueror passes Acid2 - sort of"
    date: 2005-11-29
    body: "K, didn't see anything on their site about it going wonky when scrolling.\n\nAnyway, KDE 3.5 and the new Konqueror are great."
    author: "Ian Christie"
  - subject: "Re: Konqueror passes Acid2 - sort of"
    date: 2005-11-29
    body: "\"This row is the scalp of the face and it tests fixed positioning, minimum and maximum heights, and minimum and maximum widths. In the markup, the row is represented by a p element which is fixed to the window rather than the scrollable canvas. If the Acid2 page is scrolled, the scalp will stay fixed in place, becoming unstuck from the rest of the face, which will scroll.\"\n\nhttp://www.webstandards.org/act/acid2/guide.html"
    author: "Zapp!"
  - subject: "Re: Konqueror passes Acid2 - sort of"
    date: 2005-11-29
    body: "Indeed.\n\"If the Acid2 page is scrolled, the scalp will stay fixed in place, becoming unstuck from the rest of the face, which will scroll.\"\nhttp://www.webstandards.org/act/acid2/guide.html"
    author: "ac"
  - subject: "Re: Konqueror passes Acid2 - sort of"
    date: 2005-11-29
    body: "What about when being zoomed? Is it supposed to break then?"
    author: "Andrei Slavoiu"
  - subject: "Try to zoom! Haha, I will stay with Firefox!"
    date: 2005-11-29
    body: "Try to zoom one or two times in (or out).\n\nNot only does the face scroll away, additionally it breaks."
    author: "Hans"
  - subject: "Firefox?"
    date: 2005-11-30
    body: "Firefox does not support it AT ALL."
    author: "Alex"
  - subject: "Re: Firefox?"
    date: 2005-11-30
    body: "The konqueror developers say, Konqueror passes the acid test. It does not because the layout breaks if you don't use the default font size but zoom a bit in or out.\n\nAnd yes, there are other pages which breake, too, if you use the font-zoom-feature.\n\nMost of the time I don't watch the acid-face, so it does not matter for me that firefox does not pass the acid test. but firefox passes the \"real world\" test, which is much more important for me!"
    author: "Hans"
  - subject: "Re: Firefox?"
    date: 2005-11-30
    body: "Does the acid test specify that it must tolerate zooming the text font?\n\nKonqueror passes the real world test as well.  Proper spell checking, fast loading, and real file dialogs instead of that abomination that Firefox calls a file picker in Linux."
    author: "Leo"
  - subject: "Re: Firefox?"
    date: 2005-11-30
    body: "> fast loading,\n\nBack/Forward performance is much slower than firefox and much much much slower than firefox 1.5. :-(\n\nBut Konqueror's Ftp-implementation (=file manager mode) is 1000% better than Firefox's FTP-Implementation!"
    author: "Hans"
  - subject: "Re: Firefox?"
    date: 2005-12-01
    body: "Back/Forward performance in Konqueror is slower since it doesn't cache the processed results but only the downloaded data. Firefox keeps all the results from processing and page rendering in memory, this uses more memory but saves redundant processing time. It's a tradeoff really."
    author: "ac"
  - subject: "Konqueror behaves correctly"
    date: 2005-11-30
    body: "First of all: when you zoom in and out, you should also click again on the link \"Take the Acid2 test\" so the page will be scrolled to the exact position (Konqueror will stay on the same pixels from the page top when you zoom in or out, hence you notice the face that's scrolling away -- you should go back to the top of the page and click on the link again). When you do this, you'll notice that only two things are \"wrong\":\n\nThe first thing in Acid2 that doesn't allow for zooming is the eyes: those are images, and the images don't scale. The other heights and widths are calculated based on the current font size (they are defined in \"em\"), so the other parts will scale. Hence the too small eyes and orange background behind the eyes -- you wouldn't see this orange background when the eyes were scaled well).\n\nThe other part of the face that doesn't seem to behave well is the second line on the top of the face, that isn't in the correct position. Again, that's because the measures for this line to be in it's exact position (and also it's height and width) are specified in pixels, instead of \"em\". So when you zoom in or out or when you use wrong font sizes (by forcing a minimum font size larger than the one that's used in Acid2 for example), it won't scale.\n\n(If you know css, you can check this yourself at http://www.webstandards.org/act/acid2/guide.html )\n\nIn other words: Konqueror behaves exactly how it should when zooming in or out. It's the way Acid2 works that will break the face. No browser will not break the face when zooming, Acid2 isn't designed for it.\n\n"
    author: "Selene"
  - subject: "Re: Konqueror behaves correctly"
    date: 2005-12-02
    body: "Images CAN be scaled! It's a bug in Konqueror that they don't scale, and maybe now it's time to fix this bug. The only browser that I saw does proper page zooming is Opera. Opera scales the entire page, including images and even flash movies. But we will have to wait until Opera passes Acid2 to see if it is able to zoom it corectly."
    author: "Andrei Slavoiu"
  - subject: "Re: Konqueror behaves correctly"
    date: 2005-12-02
    body: "> Images CAN be scaled!\n\nAnd? That still doesn't mean that acid2 shouldn't break when zooming."
    author: "cl"
  - subject: "Re: Konqueror behaves correctly"
    date: 2005-12-02
    body: "When Konqueror \"zooms\" in or out, it will only adapt the font size. It also says so in the \"view\" menu: \"enlarge/shrink font\". That's not really zooming, and I don't think Konqueror can do that. It will not scale the images, unlike Opera. I have no idea how it should be done, if someone can find it on the W3C website...\n\nAnd indeed, images can be scaled, but then you have to say so explicitely in the css or xhtml code (and you can even make it zoom in and out in Konqueror by using em sizes). But there are no heights or widths defined for the eye images in Acid2, and thus it's default dimensions will be used."
    author: "Selene"
  - subject: "Re: Konqueror passes Acid2 - sort of"
    date: 2005-11-30
    body: "It breaks for real if you set a minimum font size of 12 (probably other sizes too, but that's mine).  Fickle thing, that acid2."
    author: "MamiyaOtaru"
  - subject: "Thanks everyone!"
    date: 2005-11-29
    body: "I've just upgraded my Kubuntu install from the latest RC to the final release and it is a nice upgrade from earlier releases. \n\nThanks a lot to everyone in the KDE community for creating such a great desktop environment!\n\nGo get a nice beer or two and then get that KDE4 release finished :p"
    author: "Joergen Ramskov"
  - subject: "Changes since RC2..."
    date: 2005-11-29
    body: "md5sums are ONLY different on:\nkdelibs\nkdebase\nkdemultimedia\nkdeutils\nkdebindings\n"
    author: "Joe"
  - subject: "Re: Changes since RC2..."
    date: 2005-11-29
    body: "You are missing kdepim, kdesdk and kdewebdev changes in your list. kdebindings didn't change."
    author: "Anonymous"
  - subject: "Google Maps"
    date: 2005-11-29
    body: "I was really hoping that Konqueror 3.5 would address the problems with Gmail and Google Maps, but they still don't work. :/ That said I find that I can still use Konqueror as my main browser when in KDE and still not suffer.\nOther than that, it looks like another refinement on the solid foundation of 3.x, I think I can live with that for a while. :)\n\n"
    author: "Sander"
  - subject: "Re: Google Maps"
    date: 2005-11-29
    body: "Actually it is upon google to fix it.\nGo into konqueror's settings and add user agents for maps.google.com/local.google.com as a Safari agent.\nOh, and don't forget to add a user agent for blogger.com as Firefox.\n\nThen they work just fine.\n\nEmail google to tell them to fix thier silliness on excluding konqueror from acceptable browsers, even though Safari is based on khtml. \n\n*sigh*\n\n-Ryan"
    author: "Ryan"
  - subject: "Does not work"
    date: 2005-11-30
    body: "> Email google to tell them to fix thier silliness on excluding konqueror from\n> acceptable browsers\n\nDoes not work because you only receive an automatic answer.\n\nMaybe it's time to publicly announce that Konqueror will switch default search engine to MSN if google continues to lock out konqueror from their web-applications."
    author: "Hans"
  - subject: "Re: Google Maps"
    date: 2005-12-01
    body: "Google Maps still doesn't work for me, even with the user agent changed.  The navigation controls show up, and a gray background, but I never get a map.\n\nDagnabbit."
    author: "Praxxus"
  - subject: "Re: Google Maps"
    date: 2005-12-02
    body: "Yes, I've tried it with changing the user agent in each build, but it never seems to completely work. I've been able to get the background to show up sometimes by moving the map, but never consistently.\nAlso Gmail, if you turn on the advanced features as it appears in FireFox and IE, doesn't work either. :/\n\n\n"
    author: "Sander"
  - subject: "Re: Google Maps"
    date: 2005-11-29
    body: "You only have to change your user agent, and they'll work both Gmail and Google Maps.\nAfter you've done that, please email Google asking to add Konqueror to their supported browser list.\n\nhttp://linux.slashdot.org/comments.pl?sid=169644&cid=14137443"
    author: "ac"
  - subject: "Regression on yahoo! mail?"
    date: 2005-11-29
    body: "Combobox to reply and forward mail doesn't work. I tried chenging my user agent to firefox, safari and ie but it still didn't work. I was forced to use firefox to reply to a yahoo mail :("
    author: "Patcito"
  - subject: "Re: Regression on yahoo! mail?"
    date: 2005-11-30
    body: "Yep.\n\nUnfortunately Yahoo decided to break whatever was left of standards compliance.\n\nAFAIK there is a fix for this waiting to be released with KDE 3.5.1. The real fix should be done by yahoo, of course. It's their bug, but they're too big to care."
    author: "mihnea"
  - subject: "Congratulations!!!"
    date: 2005-11-30
    body: "I'm sure this will be a great, stable, fast and easy to use release. I can't wait until SLAX packages it. :)"
    author: "Alex"
  - subject: "Flash & Konqueror"
    date: 2005-11-30
    body: "Hi\n\nKonqueror and flash webpages works  3.5 well ? Or make high cpu load ?"
    author: "Flasher"
  - subject: "Re: Flash & Konqueror"
    date: 2005-11-30
    body: "I have the problems with high cpu load with FireFox 1.5 when visiting http://www.n-tv.de/ it nearly halts and takes minutes until I can press something and have something else load. Recent FireFox and most recent Flashplayer."
    author: "ac"
  - subject: "Re: Flash & Konqueror"
    date: 2005-11-30
    body: "The flashplayer is not part of KDE. It's the same old crap from Macromedia. It is probably the worst written program ever made for linux. You can expect the same slow and sluggish browsing when you visit sites with flash."
    author: "ac"
  - subject: "Re: Flash & Konqueror"
    date: 2005-11-30
    body: "No way,, flash and FF 1.0.x is oko. But if I use konqueror then cpu load up to 3 (one processor machine) And konqueror uses same plugin that FF. But FF and Flash is oko..load about 0.3 and 0.6\nThis is only problem whay I cant use one part of KDE :( Very booring... "
    author: "Flasher"
  - subject: "What happened to Kate?"
    date: 2005-11-30
    body: "Ok, I know I'm in the minority group who still misses the Tabbed Pages mode in Kate, selectable from the now extinct User Interface/Environment setting which still is in KDevelop. But why remove Projects? Are the Kate developers bent on changing everything about how we work with Kate in every release?\nKate will soon be useless..\n\nAs for KDevelop - you still cannot save the User Interface setup. All toolviews, from valgrind to konsole come up each time the app is launched. \n\nThis is amateur work! Please, fix bugs before adding features.. :-("
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "sounds bad, tough i don't use kate, i understand the problem. hope they fix it for kde 3.5.1 (another six weeks away). you should report this as a bug, and, if you can, include a fix (patch)."
    author: "superstoned"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "And konqueror is now crashing when manipulating the css attribute; display:\nI think there is room for another RC, but ah well."
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "A few questions to you:\nDid you test the last RC?\nWas this bug present in the last RC? \nIf so, did you report it then?\nHad there been one more RC, would you have tested it and reported bugs?\n\n"
    author: "Morty"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "A single answer: I have never seen a 3.x.0 KDE version that I wouldn't call beta quality, which indicates a problem with the release cycle."
    author: "Carlo"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "A single answer: the problem is that you're not helping during the release cycle."
    author: "cl"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "For 3.5 I had no time to do it, but in general you're wrong."
    author: "Carlo"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "bugs get fixed when people report them. if not enough people try the beta's and the rc's, what should the kde devs do?"
    author: "ac"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "This isn't quite true. Have e.g. a look at http://bugs.kde.org/show_bug.cgi?id=89516 I don't know if it's fixed in 3.5 final, but given this reproducible crash is known for ages, it's pretty poor sample. A lot of bugs get fixed, a lot of bugs don't. Or take http://bugs.kde.org/show_bug.cgi?id=56319 and http://bugs.kde.org/show_bug.cgi?id=116876 as example for bugs related to one of the most crucial applications KDE has to offer.\n\nMy point also isn't primarly the amount of bugs, but the short time frame for testing the rc's. Giving users a month of testing for each release candidate would yield far better results - if the devs care (free [broken] open source and time restriction arguments aside)."
    author: "Carlo"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "\"This is amateur work!\"\n\nyeah, we should demand our money back! Oh, wait...\n\n\"Please, fix bugs before adding features\"\n\nyou do realize that if the devels did that, there would never be any new features? There will never be bug-free software. Well, maybe if we had 1000 developers working on current KDE for 10 years, it might be _almost_ 100% bug-free, but there would still be bugs in there. Is that something you want?\n\nI heard that Mozilla (the browser, and nothing but the browser) has something like 50.000 open bugs...."
    author: "Janne"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "Hey! You don't seem to understand that thousands use KDE and rely on it to do their work. Do not give me that \"free software must be allowed to crap out\" shit. One of the arguments of free software is the quality of code - that you don't have to rush to keep deadlines.\n\nHere, in the real world, Kate just became unusable and unreliable for office work. The real world. In your sugar cotton world, things might be however you like."
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "\"Hey! You don't seem to understand that thousands use KDE and rely on it to do their work. Do not give me that \"free software must be allowed to crap out\" shit.\"\n\nSo, KDE-team gives you kick-ass software for free, and here you are making demands. That's a very nice way to show your gratitude, isn't it? \"I use the software that you gave me for free, and I DEMAND that you fix this problem! NOW!\". If KDE does not deliver, use something else, or fix it yourself. Most likely you haven't done jack-shit to help the developers in any shape or form, so why exactly do you think that the developers owe you anything? If I give you a car for free, does that mean that I'm required to give you free gasoline for the year as well? When it comes to software, many people seem to think just that.\n\nSeriously, I find it REALLY surprising that so meny people think that they are entitled to everything. So you use KDE, does that mean that KDE-developers should satisfy your whims? If anything, you owe to the developers (after all, they have given you software that you rely on to do your work, and they gave it to you for free!), not the other way around! And still, lots of people think that by merely using a piece of software (that they got for free) means that they are entitled to make demands... What a strange world we are living in....\n\n\"One of the arguments of free software is the quality of code - that you don't have to rush to keep deadlines.\"\n\nOne of the argoments of free software is that you can change the code to suit your needs. Don't like it? Fine, you have the code. Go right ahead and change it.\n\n\"Here, in the real world, Kate just became unusable and unreliable for office work\"\n\nYou are not forced to use Kate, go right ahead and use something else.\n\nAs to your comments about Mozilla. Yes, it works. So does KDE. so there's no need to solely fix bugs, instead of adding features."
    author: "Janne"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "\"So, KDE-team gives you kick-ass software for free, and here you are making demands. That's a very nice way to show your gratitude, isn't it? \"I use the software that you gave me for free, and I DEMAND that you fix this problem! NOW!\". If KDE does not deliver, use something else, or fix it yourself. Most likely you haven't done jack-shit to help the developers in any shape or form, so why exactly do you think that the developers owe you anything? If I give you a car for free, does that mean that I'm required to give you free gasoline for the year as well? When it comes to software, many people seem to think just that.\"\n \nPlease, don't assume anything. How I show my gratitude is besides my point, not everyone has to prove anything to have any relevance. I develop open source software as well. Nobody owes me anything. But if I'd have a mature application (not talking about KDE as a whole here), I'd make sure I didn't release something that shone badly on myself. The Open Source movement has something to prove, and that is that it's worth while. Your gibberish suggests the opposite. Now that just doesn't make any sense.\n\n \n\"One of the argoments of free software is that you can change the code to suit your needs. Don't like it? Fine, you have the code. Go right ahead and change it.\"\n\nThat's one of the arguments, but again - please make sense. Do you expect kde to be for developers only? Make sense! Secondly, do you really think that developers could just jump in on any given project and change the code to fit his or her need? Then you don't know much about software development.\n \n\"You are not forced to use Kate, go right ahead and use something else.\"\n\nLike what? Quanta can't handle big documents. KDevelop is overkill for PHP sources and has a horrible layout which must be reconfigured each time the application starts, as it doesn't save the state.\n \n\"As to your comments about Mozilla. Yes, it works. So does KDE. so there's no need to solely fix bugs, instead of adding features.\"\n\nYes, KDE works, but many KDE apps are bugridden, and nothing you've said has justified that. And you still don't get the point. Nobody said that they should solely fix bugs, but some bugs should be considered _blockers_!"
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "> > \"You are not forced to use Kate, go right ahead and use something else.\"\n> >\n> Like what?\n\nEmacs :)"
    author: "petteri"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "Hehe, no let's all go back to using VI!\n\nSeriously, this new KDE version is a step forwards in many ways, but some of the apps that has had problems in the past still has problems. \n\nKate, Quanta, KDevelop, Konqueror..\n\nWell, probably others, but as I do web work every day I've gotten accustomed to these bugs. And yes, I've posted them to the bug list. Some a long time ago :-)\n\nThe biggest step forward for KDE 3.5 is desktop polish. \n\nI'll stop whining and get to posting bug reports. Just got meself a new account."
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "\"But if I'd have a mature application (not talking about KDE as a whole here), I'd make sure I didn't release something that shone badly on myself.\"\n\nWhat makes you think KDE3.5 \"shines badly\" on KDE? Maybe they made a design-decision that you simply disagree with.\n\n\"Like what?\"\n\nThe previous version of Kate perhaps?\n\n\"Yes, KDE works, but many KDE apps are bugridden, and nothing you've said has justified that.\"\n\nWell you can\n\na) Use something else\nb) help fix those bugs\n\nIf KDE is so bad, why are you using it? Because it's better than the alternatives? I guess KDE ain't that bad in the end, huh? And if you haven't helped the developers in fixing those bugs, then I fail to see what grounds you have to complain.\n\n\"And you still don't get the point. Nobody said that they should solely fix bugs, but some bugs should be considered _blockers_!\"\n\nYeah, changes that you disagree with are blockers. It's a shame really that the KDE-developers don't ask for your opinion on every change they make."
    author: "Janne"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "I ment:\n\n  Kate, Quanta, KDevelop, Konqueror - in the time for the 3.5 release.\n\nThay are parts that only gets released with new versions of KDE and are therefore bound by the KDE release cycle. I did not mean KDE as a whole, as you quoted above."
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "\"Like what? Quanta can't handle big documents. \"\n\nExample? It can handle, but in some cases might be slow. Examples are welcome. Still, you can configure Quanta to simply not parse the document: DTD->Change DTD->Empty DTEP valid for all files.\n Set this for the project/global dtd and be happy. Of course, Quanta will be degraded, but you will get project manager and some other extra things.\n"
    author: "Andras Mantia"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "So slow it's unusable.."
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "Sorry, these are just words. Without a report with example file, it is useless.\nYou don't even mention what kind of files you edit... \nAnd I have suggested what to do if you don't need advanced Quanta functionality, just the katepart text editor with project management. Believe me, it will not be slow in that case..."
    author: "Andras Mantia"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "I think what he's talking about is trying to keep the software a bit stable and steady. That does not mean you can't add new features or improve it. Seriously, what more can people do to Kate?\n\n\"Seriously, I find it REALLY surprising that so meny people think that they are entitled to everything. So you use KDE, does that mean that KDE-developers should satisfy your whims? If anything, you owe to the developers\"\n\nWell, put a warning on it then that says \"This software may change at any time in the future and if you don't like it, tough. I know we like to promote KDE and open source software in businesses and get people to use it, and we're happy to take the credit for it but if something isn't quite right then tough luck - you still owe us.\"\n\nIt's not going to work really, is it?"
    author: "Segedunum"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "\"Well, put a warning on it then that says \"This software may change at any time in the future and if you don't like it, tough. I know we like to promote KDE and open source software in businesses and get people to use it, and we're happy to take the credit for it but if something isn't quite right then tough luck - you still owe us.\"\n\nIt's not going to work really, is it?\"\n\nThat's not actually that different to way things work in real-life. All companies that make proprietary software have EULA's that basically say \"if this software screws up your computer, you are on your own\". Free software routinely ships with warnings that say \"This software is shipped as is, with no warranties\".\n\nOf course users can and should report bugs. Of course they can suggest improvements. Of course they can post patches. But to make DEMANDS that things must be changed is completely out of line. Or complaining that \"this software sucks!\" Like I said, the software is free. They are not forced to use it. I really fail to see what grounds they have to make demands to the developers.\n\nAnd the only thing the users owe to the developers is a simple \"thank you\". Bug reports, donations, patches and all are great way to show gratitude as well, but simple \"thank you\" would also do wonders. But still, even that \"thanks\" is not required, it's completely voluntary.\n\nAnd in this case it's even worse. Apparently the original poster is using KDE for his work. In other words, KDE-developers give him software for free, and he uses that free software to earn money. And he thinks that he has grounds to make demands? He has already earned money through the use of the software he got for free, yet he thinks he's entitled to even more? And that entitlement comes from the fact that he merely uses the software (that he's not forced to use, and which he paid no money for). What is this world coming to? Seriously? I'm quite astounded by the selfishness! \"Hey KDE-devels! I'm earning thousands of dollars thanks to your free software! Now, I demand that you fix these annoyances I ran in to!\". It just boggles the mind.\n\nOriginal poster complained that \"This is amateur work!\". That's not very constructive criticism. If it's so bad, he can\n\na) use something else\nb) file bug-reports\nc) fix it\nd) make something better altogether\n\nPosting \"your software sucks!\" is not among those options. It achieves nothing."
    author: "Janne"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "\"That's not actually that different to way things work in real-life. All companies that make proprietary software have EULA's that basically say \"if this software screws up your computer, you are on your own\".\"\n\nWell, not really. It's the focus that matters. There are no guarantees with proprietary software but in order to stay ahead and to keep the money coming in they have to at some point consider why some people are unhappy."
    author: "Segedunum"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "\"Well, not really. It's the focus that matters. There are no guarantees with proprietary software but in order to stay ahead and to keep the money coming in they have to at some point consider why some people are unhappy.\"\n\nAnd that's why Microsoft is teetering on bankrupcy..."
    author: "Janne"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "\"And that's why Microsoft is teetering on bankrupcy...\"\n\nThey still have to do it though."
    author: "Segedunum"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "\"So, KDE-team gives you kick-ass software for free, and here you are making demands.\"\n\nPlease, spare us the sissy fit. The guy was making a very pertinent suggestion, and you only answer was throwing a tantrum. \nMaybe you know he's right?"
    author: "Carlos Cesar"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: ">Kate just became unusable and unreliable for office work.\n\nDid you test any of the beta and RC releases to verify that Kate still was usable and reliable for office work?\n\nHave you reported the bugs now?"
    author: "Morty"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "> One of the arguments of free software is the quality of code - that you don't have to rush to keep deadlines.\n\nWasn't it rather \"release early, release often\"?"
    author: "Anonymous"
  - subject: "Re: What happened to Kate?"
    date: 2005-11-30
    body: "By the way, Mozilla works."
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "btw... Kate works\n\nDid anybody here tried the new Kate? It's absolutely useable, despite what has been written here."
    author: "Thomas"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-01
    body: "How do I open my .kateprojects? Can you show me? "
    author: "m0ns00n"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-02
    body: "1) rm -rf /\n2) Install Windows and re-type from memory =) . --Guaranteed-- Notepad / Wordpad won't change at all in the future...\n\nNote: DO NOT run step one to see what it does, as it will erase your entire filesystem."
    author: "James Smith"
  - subject: "Re: What happened to Kate?"
    date: 2005-12-11
    body: "I'd actually like to know about a few features that seem to be missing from the new version of kate.  Before I upgraded to KDE 3.5, kate had the option to replace tabs with spaces on save... I can no longer do this.  Also, I am unable to force newly opened documents to open under the current kate instance.  Perhaps I'm blind and haven't been able to find the options (Which could very well be true), but through an hour or so of searching, I've found nothing.  This is probably the wrong place to ask, but at the same time it seems well on topic.  Why did these features disappear?"
    author: "m4f0x"
  - subject: "Cairo and glitz dependencies in kde3.5"
    date: 2005-11-30
    body: "If libpoppler are going to have dependencies on cairo and glitz should we look for other options ? "
    author: "Dragen"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-11-30
    body: "Good question I think.\n\nBut for 3.5 you really don't need libpoppler anyway. It's only used for some metadata extractions and such in the pdf file-ioslave or something like that. KPdf renders pdf's nicely whitout it."
    author: "Morty"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-11-30
    body: "> If libpoppler are going to have dependencies on cairo and glitz should we\n> look for other options ?\n\nno, there is noting bad about cairo nor glitz"
    author: "ac"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "There might be nothing wrong with them, except that they are not needed in KDE.  If libpoppler insist on including libraries that duplicates qtlibraries (arthur) we should look at it and see if it should be forked, or amended in an even better way for KDE4.  \n\nThat reminds me, if we are going to get rid of arts can we finally get rid of glib as well :)."
    author: "Dragen"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "Doesn't D-BUS use glib?"
    author: "Leo"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "Nope, it doesn't."
    author: "ac"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "Gstreamer uses glib.\n\n"
    author: "Reply"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "So? There are no plans to make Gstreamer a requirement for KDE. Optional backend yes, but not required. Besides looking at the status know, Gstreamer are not even capable of replacing aRts. "
    author: "Morty"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "Maybe that's why it's so crashy :)\nSeriously, even the Gnomers think Gstreamer is crap (just read Gnomedesktop.org from time to time), that sould tell something...\nUnfortunately, there is no real replacement, so we'll have to use something we know it's not-very-good (to be kind) just because there's nothing else better... I hope NMM can be a viable solution for KDE4, even though I doubt it."
    author: "ac"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "The plan is to make KDE4 independent of multimedia backend, making it possible to use aRts, NMM, MAS, Gstreamer or whatever else the developers feel like adding. To me the best alternatives looks like NMM or aRts, quite frankly. Since Gstreamer still are no real replacement. The Gnomers have fiddled around with Gstreamer for 5 years or so, and still not been able to make it an unified solution for their sound needs. Not exactly giving confidence.\n\nPersonally I think NMM looks like the best alternative, the technology simply looks amazing. And judging from a quick glance at the amoraK code, the NMM plugin looks seriously much simpler compared to the Gstreamer one. Perhaps anyone having studied the code closer knows different, but that was my impression.\n\nIf you are interested in trying NMM, they have a live CD: NMM-Oppix\nhttp://www.networkmultimedia.org/Status/NMM-Oppix/\n\nAnd you have KMediaNet, a set of applications to help use Network Mutltimedia Middleware (NMM) in KDE. http://www.kde-apps.org/content/show.php?content=30100"
    author: "Morty"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "\"The plan is to make KDE4 independent of multimedia backend, making it possible to use aRts, NMM, MAS, Gstreamer or whatever else the developers feel like adding.\"\n\nIt's not really the independence from the back end, although tht comes as a side-effect. Ordinarily I'm not a fan of abstraction layers, but in KDEMM I think it's essential. It will provide a much, much better programming interface for developers so you won't bind yourself to a difficult to maintain and moving C (or other) API. You can simple do something like object -> play(parameters) in a familiar way which is what people will want."
    author: "Segedunum"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "IMO we should move qt-copy to _use_ glib and cairo and strip arthur from it. Since qtk+ moves to cairo as well, that will save some memory when using a gtk+ app on KDE.\nBesides cairo is developed by good people and sooner or later will be part of X anyhow. I really wonder what the real reason is that some KDE developers are so allergic to libs used by other DE's. It's about the API that a toolkit offers to their developers not how the toolkit itself solve it. And the latter should be shared as much as possible w/ other tk, efficiently wise and not further dividing us developers in two camps"
    author: "koos"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "IMO we should rewrite KDE in GTK. That will save some memory when using a gtk+ app on kde.\nBesides gtk is developed by good people and sooner or later will be part of X anyhow. I really wonder what the real reason is that some KDE developers are so allergic to libs used by other DE's. It's about the API that a toolkit offers to their developers not how the toolkit itself solve it. And the latter should be shared as much as possible w/ other tk, efficiently wise and not further dividing us developers in two camps"
    author: "i_must_be_drunk"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "For the K classes, yes why not. Remains the Q classes that are also part of the KDE SDK"
    author: "koos"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "But don't get me wrong, idea was to keep the API. Rewritting in GTK is thus not an option. For (text) drawing classes in KDE, it might make sense though."
    author: "koos"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "indeed, you _are_ drunk :D"
    author: "drank_too_much_tea"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "> It's about the API that a toolkit offers to their developers not how the toolkit itself solve it.\n\nExactly! And as most people would agree, the trolls are the best when it comes to creating easy to use APIs.\n\n> I really wonder what the real reason is that some KDE developers are so allergic to libs used by other DE's\n\nIMHO easy to answer. It's C. It really hurts programming against a C API when you are used to Qt."
    author: "Christian Loose"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-02
    body: "Kopete has an indirect dependency on glib too."
    author: "a"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-02
    body: "Of what part of Kopete are you thinking here?"
    author: "ac"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-02
    body: "The webcam support, it test for it when configuring. It's supposed to be no glib no webcam, but it's broken and tries to build webcam support anyway. There are actually no way to stop it from trying to build webcam support:-)\n "
    author: "Morty"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-01
    body: "I guess that will depend on how portable cairo and glitz are. If they can't be offered natively on all platforms KDE 4 will be supporting porting it to Qt's Arthur may well be the better solution."
    author: "ac"
  - subject: "Re: Cairo and glitz dependencies in kde3.5"
    date: 2005-12-04
    body: "There is already a preliminary Arthur backend in poppler cvs."
    author: "Robert"
  - subject: "Konqueror and Google"
    date: 2005-12-01
    body: "This is very frustrating;\n\nKonqueror/khtml renders the Google-sites fine, but due to some incomplete check for useragent Konqueror users get a stripped html-site.\n\nChanging useragent isn't trivial for a lot of users (they even don't know what it is) so the \"out of the box experience\" on Google-sites is lame.\n\nThere are so many Konqueror-users and quite a lot of them have send Google a request to add Konqueror to the useragent-check, but they even get no response or a bot autoreplying on the mail.\n\nWhat the *beep* can we do to make Google listen?!? The Konqueror-developers have contact with Safari-developers. Maybe they have some sort of entrance in the almighty Google-organization."
    author: "AC"
  - subject: "Re: Konqueror and Google"
    date: 2005-12-01
    body: "Hopefully Google actually checks those emails and doesn't just send them to /dev/null.  So keep sending them mail about it, and keep your UA on konqueror!"
    author: "Corbin"
  - subject: "No, Konqueror does not work with Google!"
    date: 2005-12-02
    body: "> Konqueror/khtml renders the Google-sites fine, but due to\n> some incomplete check for useragent Konqueror users get a\n> stripped html-site.\n\nKonqueror 3.5 does NOT render google-maps and gmail correctly. Changing the user-agent does not help very much.\n\nSee this posting:\nhttp://dot.kde.org/1133270759/1133300616/1133302228/1133457470/1133504061/\n \n"
    author: "Hans"
  - subject: "Re: No, Konqueror does not work with Google!"
    date: 2005-12-02
    body: "That does not mean Konq is wrong, google is sending various versions of the website to the different clients.  So you will not always get correct html.\n\nThis really is something google must help with."
    author: "Thomas Zander"
  - subject: "Re: No, Konqueror does not work with Google!"
    date: 2006-01-30
    body: "works fine for me, both gmail and google maps work fine with useragent set to firefox, displays perfectly with none of those problems mentioned"
    author: "eric"
  - subject: "Re: No, Konqueror does not work with Google!"
    date: 2007-05-01
    body: "There is an update to Konquer to solve this problem?"
    author: "Edison"
  - subject: "Konqueror  Crash On NFL Site"
    date: 2005-12-01
    body: "Konqueror crashes when I access the nfl.com site.  Also, if I go to a site that streams live radio (www.xtrasportsradio.com and www.hot92jamz.comm, the stream works, but once I try to surf to another page Konqueror will crash.  I use PCLinuxOS, but I mentioned this on their site and someone tried it using Kubuntu and it crashed as well.  So it's not a distro problem.  Actually, I think this bug creeped in with the 3.4.3 release but I'm not sure.  I need to make a bug report about it, so what is the best way to capture some helpful info about the problem to pass on to the developers?  This is a pretty annoying issue since I'm a big sports guy.  :)  Other than that though, things are looking great!"
    author: "cirehawk"
  - subject: "Re: Konqueror  Crash On NFL Site"
    date: 2005-12-01
    body: "Might be the audio player is crashing. I've tested your links with konqueror with an installed kmplayer plugin and there are no crashes.\nWhat plugin are you using for audio?"
    author: "koos"
  - subject: "Re: Konqueror  Crash On NFL Site"
    date: 2005-12-01
    body: "Thanks Koos.  I believe I'm using mozplugger.  PCLinuxOS has all the multimedia pre-configured (which is great), so I can't say for sure.  But I am fairly certain mozplugger is the audio plugin."
    author: "Cirehawk"
  - subject: "Re: Konqueror  Crash On NFL Site"
    date: 2005-12-02
    body: "I guess you should report this by PCLinuxOS, they give konqueror a bad press with that plugin."
    author: "koos"
  - subject: "Re: Konqueror  Crash On NFL Site"
    date: 2005-12-02
    body: "works fine here, except for the fact i see and hear nothing... but it doesn't crash."
    author: "superstoned"
  - subject: "Re: Konqueror  Crash On NFL Site"
    date: 2005-12-03
    body: "Did have to search too for the audio, but on www.xtrasportsradio.com click to enter and then 'Listen Live' for left menu and then 'Click here to listen to XTRA Sports AM 570' in red letters in black rectangle. On \nwww.hot92jamz.com one of the items has a 'LISTEN LIVE' link and when clicked there comes a 'Click here to listen Hot 92 Jamz!' link"
    author: "koos"
  - subject: "Re: Konqueror  Crash On NFL Site"
    date: 2005-12-03
    body: "xtra sports whatever works fine here.\n\nsame with hot92jamz.com (gentoo, kde 3.5). using kmplayer."
    author: "superstoned"
  - subject: "Thanks!!"
    date: 2005-12-01
    body: "KDE 3.5 is very good and the most important - it is fast. Really fast!"
    author: "Milan Svoboda"
  - subject: "Very well done!"
    date: 2005-12-02
    body: "This is indeed the best version of KDE out in the wild so far!\n\nCongratulations to all that have contributed one way or another!\n\nJudging from the level of quality of this brand new version, one can only be rather optimistic about the future!\n\nThank you very much once again!\n\nNassos"
    author: "Nassos Kourentas"
  - subject: "Re: Very well done!"
    date: 2005-12-02
    body: "I hope now that KDE 3 is stable and optimized we can see some new and useful applications being developed for it. I am thinking business apps for which there is nothing like Quick Books or Act for small businesses depend on.  I know my buisness would buy them and even pay the equivelent price of the MS equivelent.\nI wish somebody with more brains and talent then me would copy almost verbatim the functionality of Act or Quickbooks and sell it to small business like mine.  They'd make a small fortune and do the world some good.\n\nKDE is such a great desktop.  Linux is a great OS so how about some great business apps. "
    author: "2rpi"
  - subject: "thanks"
    date: 2005-12-05
    body: "Thanks"
    author: "infosrama"
  - subject: "Where do i find rpm packages for redhat?"
    date: 2005-12-05
    body: "especially for redhat enterprise products, like AS3 and/or ES4?\n\nand no, konstruct is NOT an option. not on my office pc that i use for my daily work.\n"
    author: "Mathias"
  - subject: "Re: Where do i find rpm packages for redhat?"
    date: 2005-12-05
    body: "The KDE project doesn't create any binary packages.  The binary packages available on the download pages are provided by the distributors or individuals.  If you don't find any packages for your product this either means that the distributor didn't create any or that he didn't offer them for download on the KDE server for some reason (e.g. policy).  \n\nSo I suggest you look for them on the Redhat website and/or contact them about this issue. \n\n"
    author: "cm"
  - subject: "Re: Where do i find rpm packages for redhat?"
    date: 2005-12-05
    body: "http://kde-redhat.sourceforge.net in \"testing\" repository"
    author: "Anonymous"
  - subject: "Back to Konq"
    date: 2005-12-05
    body: "I'm giving in! I switched from Konq to Firefox somewhere around KDE 3.3, but Konqueror is again ahead of Firefox. I'm switching back, the heated hound can say what he wants."
    author: "Apollo Creed"
  - subject: "Slow startup for kopete"
    date: 2005-12-10
    body: "Why does it take so long for kopete to startup? For me it takes several minutes before i can start use it. It plopp up as an icon directly."
    author: "Nisse"
  - subject: "Re: Slow startup for kopete"
    date: 2008-07-28
    body: "Disable statistics plugin."
    author: "Steven J"
  - subject: "Re: Slow startup for kopete"
    date: 2008-08-04
    body: "Great recommendation. :-) It's the difference between 2 seconds and half a minute of starting up."
    author: "Maarten R\u00fctten"
  - subject: "Re: Slow startup for kopete"
    date: 2009-01-07
    body: "This is really great. This statistics plugin (which gather \"useful statistics\"), is nightmare. After disabling kopete starts is 3seconds! With this plugin enabled it was 2minutes."
    author: "pin007"
  - subject: "KDE 3.5 on Qt/Mac!"
    date: 2005-12-14
    body: "With this release, it is finally simple enough to install a pretty much working version of kdelibs and kdebase on a Mac without having to use the X11 server! This is a great accomplishment as now I can finally replace Finder with Konqueror retaining such things as Drag & Drop support and fonts that work.\nThanks to the KDE people."
    author: "Khaitu"
  - subject: "when can i find Debian packages ?"
    date: 2005-12-15
    body: "I have a KDE 3.3.2 in Debian linux (sarge,stable)\nwhere can i find the kde 3.5 packages for debian sarge ??\nthanks for advance!"
    author: "plazzmex"
  - subject: "Re: when can i find Debian packages ?"
    date: 2006-01-10
    body: "They have just entered unstable a few days ago."
    author: "JackieBrown"
---
The KDE Project is happy to <a href="http://www.kde.org/announcements/announce-3.5.php">announce a new major release</a> of the <a href="http://www.kde.org/awards">award-winning</a> K Desktop Environment. Many features have been added or refined, making KDE 3.5 the most complete, stable and integrated free desktop environment available.  For a quick look at some of the new features see the <a href="http://www.kde.org/announcements/visualguide-3.5.php">visual guide to KDE 3.5</a>.  <a href="http://kde.org/info/3.5.php">Packages</a> are available now for <a href="ftp://ftp.archlinux.org/extra/os/i686">ArchLinux</a>, <a href="http://kubuntu.org/announcements/kde-35.php">Kubuntu</a>, <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5/contrib/Slackware/10.2/README">Slackware</a> and <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5/SuSE/README">SuSE</a> or try <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> to build it yourself.

<!--break-->
<p>Notable changes include:
</p>

<ul>
  <li>Konqueror is the second major web browser to pass the Acid2 CSS test, ahead of Firefox and Internet Explorer</li>
  <li>Konqueror can also free webpages from adverts with its new ad-block feature</li>
  <li>SuperKaramba is included in KDE, providing well-integrated and easy-to-install widgets for the user's desktop</li>
  <li>Kopete has support for MSN and Yahoo! webcams</li>
  <li>The edutainment module has three new applications (KGeography, Kanagram and blinKen), and has seen huge improvements in Kalzium</li>
</ul>

<p>Stephan Kulow, KDE Release Coordinator, said: "The improvements made in the past year show how mature the KDE Project is. KDE is the most powerful desktop environment and development platform in the market. With huge changes expected in KDE 4, our next release, KDE 3.5 should provide users with the perfect productivity platform for the next couple of years."
</p>




