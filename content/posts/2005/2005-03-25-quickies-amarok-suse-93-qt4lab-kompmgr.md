---
title: "Quickies: amaroK, SUSE 9.3, Qt4Lab, Kompmgr"
date:    2005-03-25
authors:
  - "jriddell"
slug:    quickies-amarok-suse-93-qt4lab-kompmgr
comments:
  - subject: "AmaroK"
    date: 2005-03-24
    body: "I wish had more luck with AmaroK. It always crashes on me. I do not like the aKode, aRTs or the xine engine. I'd like to use gstreamer, but it always crashes. I thought KDE3.4 would be better but there has never been any luck. I have already submitted the trace below to the developers. Sorry in advance. It's too long. Here we go: -\n\namaroK has crashed! We're terribly sorry about this  :( \n\nBut, all is not lost! You could potentially help us fix the crash. amaroK has \nattached a backtrace that describes the crash, so just click send, or if you \nhave time, write a brief description of how the crash happened first.\n\nMany thanks.\n\nEngine:     gst-engine\nBuild date: Mar 18 2005\nCC version: 3.3.4 (pre 3.3.5 20040809)\nKDElibs:    3.4.0 Level \"a\" \nTagLib:     1.3.1\nNDEBUG:     true\n\n[Editor: backtrace has been removed, please use bugs.kde.org instead]\n"
    author: "charles"
  - subject: "Re: AmaroK"
    date: 2005-03-24
    body: "I don't think this is the place to post such messages. Plz fill out a bug repport at bugs.kde.org or post it at the kde forum : www.kde-forum.org\n\nrgrds,\nPhil"
    author: "Phil"
  - subject: "Re: AmaroK"
    date: 2005-03-24
    body: "Like he mentioned, he's already filed a report to the developers. He may be a bit disappointed that he's never had any \"luck\" with AmaroK...kind of venting?"
    author: "job"
  - subject: "Re: AmaroK"
    date: 2005-03-25
    body: "For amaroK (and probably most of KDE), bugs.kde.org is a place for bugs, not general tech support. Might find more luck in #amarok or the amaroK forums. Granted, I don't see what's wrong with xine... it uses less CPU then gstreamer and has less problems. If it works use it."
    author: "Ian Monroe"
  - subject: "Re: AmaroK"
    date: 2005-03-25
    body: "1) Though I can fully understand that it's kind of frustrating\nto not even be able to use Amarok properly because it always\ncrashes - you could *at least* have filed a bug and linked to that bug instead of posting endless backtraces here.\n2) Nevertheless to solve your problem you might want to consider\ncompiling gstreamer and gst-plugins by yourself instead of\nusing packages. Afterwards recompile Amarok. Only this way you\nmake sure that all fits together nicely. Packages can be\nincompatible sometimes. Especially my experience with SuSE 9.2 RPMs\nfor GStreamer was very bad - somewhat understandable if you know\nthat 9.2 (and its GStreamer packages) are not a bit dated.\nRecompiling did solve similar GStreamer problems for me. Before compiling Amarok make sure to test GStreamer functionality with sth. like\n\ngst-launch filesrc location=/home/foo/bar.mp3 ! spider ! audioscale ! audioconvert ! osssink\n\nMake sure you run gst-register first to register all plugins after\ncompilation! Run gst-inspect to see if everything is needed is there.\nI.E. Run:\ngst-inspect | grep mad\nTo see if the mad-plugin is there (better make sure to compile\nit when \"configure\"-ing gst-plugins\nAnd:\ngst-inspect | grep osssink\n\nDont use alsasink if you have problems - use osssink it's much more\nstable. You dont need to specify alsasink or osssink in Amarok - only\nwhen using the command-line. Amarok will detect this automatically.\n\nIf you dont already know (some Linux users unfortunately dont know\nthis) you should use \"checkinstall\" (goes with your distro) instead\nof \"make install\" to create an RPM/DEB/whatever package so you can\neasily remove/update this stuff.\nOn SuSE, edit /etc/checkinstallrc to say INSTALL=1 in the last line\nand afterwards run\ncheckinstall --fstrans=no \ninstead of checkinstall.\nAlways hit enter and gstreamer/gst-plugins will be installed as RPM.\nSounds complicated, but it isnt in fact. Just follow the above\nsteps one-by-one.\n\n\n\n"
    author: "Martin"
  - subject: "some screenies, vidcap"
    date: 2005-03-24
    body: "I some more Kompmgr eyecandy:\n\nBigfile warning - 14 megs: ftp://hatvani.unideb.hu/pub/personal/screenshots/kdetransp.avi\n\nScreenshot: ftp://hatvani.unideb.hu/pub/personal/screenshots/transparency1.png\n\nAmazing work! And it is pretty stable. Only problem I have is with mplayer if kompmgr and transparency is enabled. If you know of a solution please help. One solution is to use kmplayer, which seems to work (but there are some screen redraw issues). "
    author: "molnarcs"
  - subject: "Re: some screenies, vidcap"
    date: 2005-03-25
    body: "here some nice things Seth Nickell is working on:\nhttp://ftp.acc.umu.se/mirror/temp/seth/blog/xshots.html\n\nscreenshots suck, today we need MOVIES (lots of movies behind the above link)\n\n"
    author: "cies"
  - subject: "Re: some screenies, vidcap"
    date: 2005-03-25
    body: "Here's another one, now display contents while moving windows on:\n\nftp://hatvani.unideb.hu/pub/personal/screenshots/kcompmgr1.avi\n\nI was surprised at the performance of this thing - it works just fine, without any lag or something :) (previously when I tried enabling the composite manager, it was slow and buggy, but that was with xorg 6.8.1 and fluxbox). System is athlon xp 2400+, 512RAM, GeforceFX5200 with nvidia binary drivers, FreeBSD 5.4Prerelease.\n\n"
    author: "molnarcs"
  - subject: "Re: some screenies, vidcap"
    date: 2005-03-25
    body: "> previously when I tried enabling the composite manager, it was slow and buggy,\n> but that was with xorg 6.8.1 and fluxbox.\n\nI am on XOrg CVS with xcompmgr 1.1.1 and it's still slow and buggy. Am I doing something wrong ?"
    author: "Pope"
  - subject: "Re: some screenies, vidcap"
    date: 2005-03-25
    body: "I have a very similar system as you (2400+ Athlon running at 2G native, GeForceFX5200 w/128MB) but with Gigabyte mobo and 1 GB RAM running in dual channel (gotta love Gigabyte memory management).\n\nI am now looking at a KDE desktop within a LiveCD version of PCLinuxOS. Its very neat. This OS loads up with Firefox 1.0, Flash, MPlayer, etc etc. I was able to look at these AVI files in Firefox, off this LiveCD. This is too easy.\n\nGoing to try Amarak. This LiveCD includes about 15 other audio apps:\n  aRts builder, Audacity, Grip, Juk, kaboodle, KAudioCreator, KMid, KMix, KRec, KsCD, Noatun, Rezound, ripperX, streamtuner, Sunjammer, Timidity+++, Xmmms.\n\nWant to download the OS to try it?    \n   ftp.ussg.iu.edu/linux/pclinuxos/live-cd/english/preview/\n\n--\nHeymull"
    author: "heymull"
  - subject: "about  Kompmgr "
    date: 2005-03-25
    body: "I watched the  Kompmgr video, looks kool. How does the user switch focus from one window to another when they are superposed? I suppose it must by by rolling the mouse wheel, right? looks kool anyway and nice to see that Zack's gonna work full time on improving X and thx to trolltec for that :)"
    author: "Pat"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "Hi. This is the guy that made the video.\n\nIf by the user you mean me specifically, it's just \"Focus Follows Mouse\". The windows are only partially overlapping. \"Auto raise\" with a slight delay is a nice effect too, IMO."
    author: "bssteph"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "what do you mean \"Focus Follows Mouse\"? how does kwin knows you want to focus on the window that is behind another whithout even a click or wheel roll?"
    author: "Pat"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "\"Focus Follows Mouse\" means that when you move the mouse cursor over a window, it automatically brings that window into focus (possibly with a delay). That lets people focus on windows without bringing them to the front of the stack by clicking on them.\n\nThere are some patches to kwin/window decorations that allow you to switch window focus by using the mouse wheel over the title bar, though. You may be able to find them on kde-look.org, though they may be too out of date to work with KDE 3.4 and the like."
    author: "Dolio"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "\"\"Focus Follows Mouse\" means that when you move the mouse cursor over a window, it automatically brings that window into focus (possibly with a delay).\"\n\nthx but that still doesn't answer my question. when one window is over another, how can the focus be changed? did u watch the video? the mouse cursor is still on the other window and then focus change to the other window that is behind it. How can that be? what if the user don't want to change focus, will it change automatically?"
    author: "Pat"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "I suppose he used a keyboard shortcut in the video. (alt+tab)"
    author: "Narishma"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "Either that or Alt + Middle Mouse Button "
    author: "John Mouse Freak"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-25
    body: "He probably set a delay"
    author: "ac"
  - subject: "Re: about  Kompmgr "
    date: 2005-03-26
    body: "I'm not sure where the confusion is coming from here, but I didn't do anything special. I'll try to detail everything.\n\n- Konversation is on top of Konqueror\n- I move the mouse to Konqueror and focus changes (\"Focus Follows Mouse\"), but it is not raised. Konversation is still on top.\n- Konsole is also on top of Konqueror\n- I move the mouse to Konsole, same deal. Focus change.\n- I move the mouse back to Konqueror. I Alt-LeftClick to position the window. This is my first time clicking anywhere. Note that this does not raise the window. Konqueror is still under Konversation and Konsole.\n- I move back to Konversation and it is focused.\n\nDoes this help? When I move the mouse to a new window, the focus changes, but nothing is raised. No magic on my part... :)"
    author: "bssteph"
  - subject: "OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-25
    body: "I use SuSE 9.0 to 9.2 on various computers and the only problem is USB: It's flacky, unreliable and it sometimes crashes the system.\n\nDoes anybody know wether USB became better (or more stable) in more recent kernel revisions?\n\n"
    author: "Roland"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-25
    body: "Hardly. It's been 10 years since i bought a first PC with an USB port - and it\nstill doesn't work properly. Give it another 10, perhaps :/"
    author: "jmk"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-25
    body: "I use all sorts of usb devices (harddisks, mouse, hubs, ethernet to usb converter, ...) and all work flawless. I can unplug them and plug them in again, and still everything works as expected.\n\nI use gentoo though, and compile the kernel myself."
    author: "t b"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-26
    body: "well, I don't have any problems either, and I'm using suse and debian. they worked fine. so what's the problem?"
    author: "superstoned"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-26
    body: "Probably problems are related to hardware. I run both SUSE and vanilla kernels ( currently running 2.6.12-rc1-bk1 on x86-64 ) and seems that even with this latest kernel, sticking in USB memory stick causes machine to crawl for a few seconds. With SUSE kernel keyboard occasionally does not initialize during boot - and replugging it doesn't help."
    author: "jmk"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2005-03-28
    body: "USB works flawlessly for me in FreeBSD/KDE, but despite all the myths that it's ready for the desktop, USB is horribly flaky for me in Windows XP SP2 on a work system!!! In terms of USB mass storage, some actually freeze the machine until you yank them out. Others only freeze the machine when you try to \"remove\" them. And then the ones that work like to steal a mapped network drive. I've had nothing but heartache with USB under Windows."
    author: "Brandybuck"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-25
    body: "Apply those Suse-Online-Updates.\n\nUSB has never made problems here (Suse 9.0, Suse 9.2)\n\nBtw.: The Suse Admin handbook has some chapters about hotplugging and udev. Worth a read."
    author: "Hans"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-26
    body: "I have USB problems 50% of the time too but only when using Konqueror to copy files to or from the USB stick (freecom).  It just kills the machine stone dead either when copying data or when i've selected something to copy and right click and navigate through the menus to get to \"Copy to\". I have no problems copying via the command line."
    author: "Ian"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-27
    body: "Yeah, Konqueror seems to put a lot of stress into it, but I also had crashes with pure command-line copying (although not so often)"
    author: "Roland"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6.11)"
    date: 2005-03-28
    body: "Y'know, I've never had problems with SUSE and USB until tonight - about half an hour before I write this.  I could not get my Palm to sync - it kept disconnecting.\n\nTurns out the power cable was not plugged all the way into the hub.  Heh."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2006-06-01
    body: "Hi,\n\nCould you please tell me what software are you using to sync your Palm.\nI have a LiveDrive Palm device, PalmOS, USB connector only, and SuSe 9.0 upgraded:\nLinux tukey 2.4.21-297-smp4G #1 SMP\n\nOr should I upgrade to 10.0?\n\nThanks"
    author: "frederic"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2005-03-31
    body: "I just found this googleing for my USB Problem.\n\nThere seams to be a Serial USB - Visor / Palm support Problem in all Kernels up to 2.6.11 which sometimes manages to freeze the whole machine.\n\nI don't know any solutions for this :-(\n\nSo don't sync your palm on a productive machine..."
    author: "Benoit Panizzon"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2005-04-17
    body: "Yeah, I had problems on all my SuSE 9.1 systems *AFTER* I upgraded the kernel and the libs. Everytime I plugged ANY USB device, the system simply crashed. I'm now using SuSE 9.2 on all my systems, and everything works fine.\n\nBottom line: upgrading is not allways a good thing."
    author: "John the Ripper"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2005-04-27
    body: "Never had a problem on 9.0 and 9.1.  But after an upgrade to 9.3 and a hard crash during a hotsync with kpilot my usb mouse is no longer working."
    author: "rsf"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2006-02-22
    body: "I've been searching for clues a to why my new Compaq V2000 notebook won't launch USB ports or NIC with SuSE 9.3 or SuSE 10--but works fine with SuSe 9.1.  Kernel on SuSE 9.1 is 2.6.4.  Any thoughts...\n\n"
    author: "RLC"
  - subject: "Re: OT: Stable USB-support in SuSE 9.3 (kernel 2.6"
    date: 2006-12-16
    body: "I am also having trouble with usb on suse 10.1. I had support for usb and then did a reinstall as I needed to repartician. Since the reinstall I have been unable to get anything to work on the usb port lsusb follows.\nBus 004 Device 001: ID 0000:0000\nDevice Descriptor:\n  bLength                18\n  bDescriptorType         1\n  bcdUSB               2.00\n  bDeviceClass            9 Hub\n  bDeviceSubClass         0 Unused\n  bDeviceProtocol         1 Single TT\n  bMaxPacketSize0        64\n  idVendor           0x0000\n  idProduct          0x0000\n  bcdDevice            2.06\n  iManufacturer           3\n  iProduct                2\n  iSerial                 1\n  bNumConfigurations      1\n  Configuration Descriptor:\n    bLength                 9\n    bDescriptorType         2\n    wTotalLength           25\n    bNumInterfaces          1\n    bConfigurationValue     1\n    iConfiguration          0\n    bmAttributes         0xc0\n      Self Powered\n    MaxPower                0mA\n    Interface Descriptor:\n      bLength                 9\n      bDescriptorType         4\n      bInterfaceNumber        0\n      bAlternateSetting       0\n      bNumEndpoints           1\n      bInterfaceClass         9 Hub\n      bInterfaceSubClass      0 Unused\n      bInterfaceProtocol      0\n      iInterface              0\n      Endpoint Descriptor:\n        bLength                 7\n        bDescriptorType         5\n        bEndpointAddress     0x81  EP 1 IN\n        bmAttributes            3\n          Transfer Type            Interrupt\n          Synch Type               None\n          Usage Type               Data\n        wMaxPacketSize     0x0002  1x 2 bytes\n        bInterval              12\ncan't get hub descriptor: Operation not permitted\ncan't get device qualifier: Operation not permitted\ncan't get debug descriptor: Operation not permitted\ncannot read device status, Operation not permitted (1)\n\nBus 002 Device 001: ID 0000:0000\nDevice Descriptor:\n  bLength                18\n  bDescriptorType         1\n  bcdUSB               1.10\n  bDeviceClass            9 Hub\n  bDeviceSubClass         0 Unused\n  bDeviceProtocol         0\n  bMaxPacketSize0        64\n  idVendor           0x0000\n  idProduct          0x0000\n  bcdDevice            2.06\n  iManufacturer           3\n  iProduct                2\n  iSerial                 1\n  bNumConfigurations      1\n  Configuration Descriptor:\n    bLength                 9\n    bDescriptorType         2\n    wTotalLength           25\n    bNumInterfaces          1\n    bConfigurationValue     1\n    iConfiguration          0\n    bmAttributes         0xc0\n      Self Powered\n    MaxPower                0mA\n    Interface Descriptor:\n      bLength                 9\n      bDescriptorType         4\n      bInterfaceNumber        0\n      bAlternateSetting       0\n      bNumEndpoints           1\n      bInterfaceClass         9 Hub\n      bInterfaceSubClass      0 Unused\n      bInterfaceProtocol      0\n      iInterface              0\n      Endpoint Descriptor:\n        bLength                 7\n        bDescriptorType         5\n        bEndpointAddress     0x81  EP 1 IN\n        bmAttributes            3\n          Transfer Type            Interrupt\n          Synch Type               None\n          Usage Type               Data\n        wMaxPacketSize     0x0002  1x 2 bytes\n        bInterval             255\ncan't get hub descriptor: Operation not permitted\ncannot read device status, Operation not permitted (1)\n\nBus 001 Device 001: ID 0000:0000\nDevice Descriptor:\n  bLength                18\n  bDescriptorType         1\n  bcdUSB               1.10\n  bDeviceClass            9 Hub\n  bDeviceSubClass         0 Unused\n  bDeviceProtocol         0\n  bMaxPacketSize0        64\n  idVendor           0x0000\n  idProduct          0x0000\n  bcdDevice            2.06\n  iManufacturer           3\n  iProduct                2\n  iSerial                 1\n  bNumConfigurations      1\n  Configuration Descriptor:\n    bLength                 9\n    bDescriptorType         2\n    wTotalLength           25\n    bNumInterfaces          1\n    bConfigurationValue     1\n    iConfiguration          0\n    bmAttributes         0xc0\n      Self Powered\n    MaxPower                0mA\n    Interface Descriptor:\n      bLength                 9\n      bDescriptorType         4\n      bInterfaceNumber        0\n      bAlternateSetting       0\n      bNumEndpoints           1\n      bInterfaceClass         9 Hub\n      bInterfaceSubClass      0 Unused\n      bInterfaceProtocol      0\n      iInterface              0\n      Endpoint Descriptor:\n        bLength                 7\n        bDescriptorType         5\n        bEndpointAddress     0x81  EP 1 IN\n        bmAttributes            3\n          Transfer Type            Interrupt\n          Synch Type               None\n          Usage Type               Data\n        wMaxPacketSize     0x0002  1x 2 bytes\n        bInterval             255\ncan't get hub descriptor: Operation not permitted\ncannot read device status, Operation not permitted (1)\n\nBus 003 Device 001: ID 0000:0000\nDevice Descriptor:\n  bLength                18\n  bDescriptorType         1\n  bcdUSB               1.10\n  bDeviceClass            9 Hub\n  bDeviceSubClass         0 Unused\n  bDeviceProtocol         0\n  bMaxPacketSize0        64\n  idVendor           0x0000\n  idProduct          0x0000\n  bcdDevice            2.06\n  iManufacturer           3\n  iProduct                2\n  iSerial                 1\n  bNumConfigurations      1\n  Configuration Descriptor:\n    bLength                 9\n    bDescriptorType         2\n    wTotalLength           25\n    bNumInterfaces          1\n    bConfigurationValue     1\n    iConfiguration          0\n    bmAttributes         0xc0\n      Self Powered\n    MaxPower                0mA\n    Interface Descriptor:\n      bLength                 9\n      bDescriptorType         4\n      bInterfaceNumber        0\n      bAlternateSetting       0\n      bNumEndpoints           1\n      bInterfaceClass         9 Hub\n      bInterfaceSubClass      0 Unused\n      bInterfaceProtocol      0\n      iInterface              0\n      Endpoint Descriptor:\n        bLength                 7\n        bDescriptorType         5\n        bEndpointAddress     0x81  EP 1 IN\n        bmAttributes            3\n          Transfer Type            Interrupt\n          Synch Type               None\n          Usage Type               Data\n        wMaxPacketSize     0x0002  1x 2 bytes\n        bInterval             255\ncan't get hub descriptor: Operation not permitted\ncannot read device status, Operation not permitted (1)\n"
    author: "al"
---
OSDir.com has posted some <a href="http://shots.osdir.com/slideshows/slideshow.php?release=248&slide=1">screenshots of amaroK 1.2</a> showing off the eye candy from this feature packed music player.  ***  The software behind <a href="http://www.kde-apps.org">KDE-Apps.org</a> and <a href="http://www.kde-look.org">KDE-Look.org</a> has been <a href="http://www.kde-apps.org/news/index.php?id=165">rewritten with new features and new categories</a> and they have gained <a href="http://www.kde-apps.org/news/index.php?id=163">a new sponsor</a>.  ***  Novell has posted a <a href="http://www.novell.com/coolsolutions/feature/11878.html">review of SUSE 9.3</a> which features KDE 3.4.  ***  <a href="http://www.qt4lab.org/">Qt4Lab Data Plotter</a> is looking for beta testers.  *** <a href="http://bssteph.irtonline.org/kompmgr_and_kde_window_eyecandy">Kompmgr and KDE, window eyecandy</a> (<a href="http://www.google.com/search?q=cache:oq77yW9T_64J:bssteph.irtonline.org/kompmgr_and_kde_window_eyecandy+&hl=en&ie=UTF-8">Google cache</a>) has a video (<a href="http://ktown.kde.org/~endres/kompmgr and KDE - window eyecandy.avi">mirror</a>) of the fun you can have with KWin in KDE 3.4. ***  Finally <a href="http://www.kdedevelopers.org/node/view/933">congratulations to our own Zack Rusin</a> who will soon be working for Trolltech to "make sure that X11 is again a state of the art technology".







<!--break-->
