---
title: "KOffice 1.4 Release Candidate Announced"
date:    2005-06-02
authors:
  - "iwallin"
slug:    koffice-14-release-candidate-announced
comments:
  - subject: "First Post"
    date: 2005-06-02
    body: "First Post ;)\n\nBtw, Kongratulations (as suggested last time I post here :D ) for the release candidate of KOffce 1.4. Thanks to all KOffice developers :)\n\nCheers\n\nfyanardi"
    author: "fyanardi"
  - subject: "curious"
    date: 2005-06-02
    body: "nice work !! congrats !!\n\nhow many developers are working on Koffice now ? are there more now ?\nwhat sub-projects lack developers ?\n\ncurious,ch:,.\n"
    author: "ch"
  - subject: "Re: curious"
    date: 2005-06-02
    body: "I'm not sure how many people are hacking on KOffice right now -- there's some fluctuation. Since I became maintainer of Krita about a dozen developers have contributed to Krita -- some with lots of code, some with one essential patch, others with icons. And then there are the translators (who have a perfect right to clamor for my blood seeing the amount of trouble Krita gave them this time). \n\nKexi gets reasonable amounts of love. KChart has been picked up, too. There are quite a few people interested in KSpread, but that's the type of application you really need to have about half a dozen dedicated people working on. Kivio is already quite good, but I don't doubt that Peter would welcome someone to help him develop the application further.\n\nTwo applications that really need a strong and dedicated push are KWord and Karbon. \n\nKWord is quite good, but a word processor is a central application in an office suite and needs constant development. \n\nKarbon once had the chance to be the first and premier vector graphics application. Neglect has let Sodipodi and now Inkscape grab the lead, but if someone with a bit of stamina gets really enthusiastic he could make great strides. There's also an easy task for someone who wants to dive into Karbon: implement rendering vectors and fills with patterns. It's one of the things that still make Karbon crash.\n\nI'd really love to work together with a Karbon maintainer to make sure Krita and Karbon really fit together -- from basic widgets to sharing a rendering model so you can integrate a karbon layer in a Krita image and vice versa.\n\nI'm determined to stick to Krita, to resist getting distracted by other apps, because I believe I would never achieve my vision if I divide my effort. I'm not going to develop another app besides Krita. But cooperating to make sure that KOffice is also the creative suite that rocks is quite another thing. That's high on my list of priorities."
    author: "Boudewijn Rempt"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "I just wanted to say how glad I am for your work folks! I installed the beta some time ago, and I was amazed at the quality of krita - it is going to be a great replacement for gimp (at least for certain tasks).\n\nReally really nice work, thank you!"
    author: "molnarcs"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "> KWord is quite good, but a word processor is a central application in an office suite and needs constant development.\n\nDont get me wrong - but I consider KWord to be the least important part of KOffice (KSpread is the most important, since in business you more often work with money ehm... numbers than text).\nWell, let's see what KWord is used for: People writing important texts (for which they are paid) use Kile. People creating Presentations use KPresenter (contra OOo Writer which is used more often for this task than OOo Impress). And People editing pure Text use Kate. So, what need for KWord is there? None.\n"
    author: "german User"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "Er... I think you're wrong here. Kile is a great application, but I don't think it would see a lot of use for writing business letters, proposals, specifications, reports, or memo's. There's a lot more to business than numbers... And you just don't use a latex frontend in business, you use a word processor.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "i disagree. i expect a well-educated to be able to write LaTeX fluently. and so far i have not been disappointed. if not, get a new one.\ntheir job is to care of the context (since that's what gives money) not fiddling around hours on layout, fonts and so on.\nof course, you need a pretty large collection of templates and they are mostly spcific to your enterprise so there's no common pool to use, but once you have them using them is pretty fast and easy (and saves you from a lot of faux-pas)\nwell, theres one disadvantage: when getting letters and offers my eyes sometimes hurt looking at bad fonts and shabby layouts. esp when french accents and japanese hiragana are placed wrongly or even completely missing sometimes. it's a sing of bad management and a clear indication to not make deals with them. may seem a little harsh, but when economy is going down (as in germany) you have to focus on everything, even the tiniest things"
    author: "Benni"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "No doubt KWord and KSpread (with KChart) are the most important K*Office* applications.  They are very central to how an office is run.  Almost as important, but much less used, would be Kexi, the database application.\n\nIn fact (and sorry, Boudewijn), I would hesitate to say that Krita belongs in a pure office suite at all.  It would fit better in a DTP suite together with, maybe, Karbon and Scribus and some other programs of the same type or perhaps an artists suite.\n\nThat is not to say that I think that Krita should be thrown out of KOffice, only that such programs don't get much use in an office.\n\nAnd now to something completely different. As part of the KOffice team, I have some plans for the future.  One thing I would like to do is to make a survey of how KOffice is used, and which features are missing.  We should also look into previous investigations, e.g. from Microsoft, to find out how people do use office suites.  And then we should implement *these* missing features, not other features.\n\nWith the risk of making me unpopular here, I am going to pick an example.  There has been talk on the KOffice developer list about making KSpread use bignums so that virtually unlimited precision could be had.  To me, that sounds like a bad idea.  Unlimited precision is nice, but it would also mean a much more complicated code base and probably hamper other developments. What we need first is to actually get people to use KOffice before we go out and implement features that only a infinitesimal part of the users want.  \n\nInstead, we should implement list handling functions and fix annoying bugs because this is what keeps people from using the programs.  Microsoft found out, much to their surprise, that handling lists was the most common use of a spreadsheet, not making elaborate tables with formulas. And KSpread is still lacking in this area! And that a long buglist is making a program less popular is a given.\n\nWe need to establish a significant and growing user base that likes the speed of KOffice and that wants the tight integration between KDE and KOffice.  And we need to do this *before* we do fringe features.  Otherwise KOffice will drown in the upcoming massive deployments of Open Office."
    author: "Inge Wallin"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "I have never said that the definite precision values are to be done *now*.\n\nHowever KSpread used much memory to define a cell, as it used the values one after another instead using something in the idea of a QVariant. So this has/had to be changed (not sure if it was done or not in KOffice 1.4). So I thought that taking care about possibliy extending KSpread in such a direction should be taken care of.\n\nAlso when dealing with currency, as far as I know, you need exatcly 4 digits after the decimal. That too can be done with definite precision (just set it to 4).\n\nAs for KOffice \"drown\" in a OOo wave, that is for me exactly a reason to have functions that normal office suites do not offer.\n\n(As for when and who will actually implement define precision, that is a totally other question.)\n\nHave a nice day!\n\nPS.: for real numbers there is no inifinite precision usable on a computer, see for example pi."
    author: "Nicolas Goutte"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "Oh, I quite agree with you about Krita -- it's been discussed on our mailing list, too. It's that the KOffice framework makes a lot of features really easy to handle, like filters and templates and so on. But a tighter integration with digikam and Karbon is high on my list of priorities, and that's harder to do from KOffice. And we'd like to do releases more often than KOffice does.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "I think you seriously misunderstand common office workflow. The wordprocessor is central to almost everything.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: curious"
    date: 2005-06-05
    body: "Go away troll, why makes a stupid discussion 'Kile vs KWord'.\n\nThe word processor *is* the most important application of an office suite: it is the one that is the most used!\n"
    author: "renox"
  - subject: "Re: curious"
    date: 2005-06-03
    body: "> I'd really love to work together with a Karbon maintainer to make sure Krita\n> and Karbon really fit together -- from basic widgets to sharing a rendering\n> model so you can integrate a karbon layer in a Krita image and vice\n> versa.\n\nWonderful! :)"
    author: "Helder Correia"
  - subject: "Re: curious"
    date: 2005-06-04
    body: "And now for an energetic Karbon maintainer :-)."
    author: "Boudewijn Rempt"
  - subject: "Krita"
    date: 2005-06-02
    body: "Krita is truly amazing!!!! I can barely hide my excitement ;-)\nFor years - if not decades - many in the Linux community (especially\nthose not using Gnome) have wanted a replacement for GIMP with\nits annoying multi-windows interface cluttering your taskbar.\nNow we're almost there it seems. A fact I needed some to get used too -\nso long have we been waiting for a decent graphics editing app.\nFurthermore, CMYK support now might really become a reality in the near future. GIMP has promised this for ages for the \"next\" version but so far I dont know of any steps in terms of program design that might get it there apart from some dubios non-functional auxiliary scripts. So Krita is good for all those who really want to dump Windows+Photoshop. Until now - let's face it -\nthis is just not possible for everyone who must work with graphics\nprofessionally.\nKudos to everyone involved!!\n"
    author: "Martin"
  - subject: "Re: Krita"
    date: 2005-06-02
    body: "Well... <blush> and all that. But insanely great as Krita undoubtedly is -- there's a way to go before we've got the features and precision to beat Photoshop or Corel Painter. CMYK is pretty close indeed, and I had really some hope it would make it into the final release, but apart from the niggling bugs, there are some issues with making it not only usable, but also useful.\n\nThe main problem is that I haven't got access to anyone with the requisite domain knowledge. It's an interesting thing to work on, but it's also something I don't need, personally. I'm really pleased when someone tells me they fired up Krita and managed to paint without getting frustrated. That's where my personal thing is. \n\nI'm a linguist, a theologian, ultimately a Java coder for hire -- not a print professional. I haven't got a clue about the workflow from image to press...\nWhich means that initially, cmyk will mean nothing more than that -- having four additive channels instead of three subtractive in your image.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita"
    date: 2005-06-03
    body: "What about contacting the scribus team? They're knowledgeable abot the printing process and since it uses Qt, there may even be some pieces of code to get there?\n"
    author: "Richard Van Den Boom"
  - subject: "Congratulations!"
    date: 2005-06-02
    body: "I really love to see KOffice becoming better and better.. \nThanks to all the KOffice developers :-)"
    author: "Dario Massarin"
  - subject: "thanx"
    date: 2005-06-02
    body: "really thanks for the work. i really depend on Koffice, as its the only office suite i have :D\n\nso i'm very happy to see it get better.\n\ni used ms office today, 2003. well, that's some nice piece of work, i have to admit. not on every front, but it has some nice things, really. but hey, they have many people working on it - and koffice is free!"
    author: "superstoned"
  - subject: "Krita is awesome :)"
    date: 2005-06-02
    body: "Am not an artist, but tried it the other day and drew some silly stuff with it. (looked like a teched out Worms level...)\nfirst advanced paint app I could just pick up and draw with :)\n\n(is there any way to use the alpha channel in gradients, eg, have it grade from 100% transparent at one end to 0% at the other?)"
    author: "Illissius"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-02
    body: "Thanks for the compliment!\n\nAbout the gradients: try the autogradient section in the Tools paintbox. Right-click on the gradient bar to add or remove segments: set the opacity and the color for the endpoints of every segment using the color wells and spinboxes underneath the gradient bar. We don't have a facility for saving custom gradients yet, I'm afraid I have to admit.\n\nThe opacity setting in the gradient tool panel is for the gradient as a whole; I guess that's what you tried to use at first. \n\nThere are some usability issues with the current division between tool panels and Tools option sheets -- at this very moment I am dithering between reworking the tools/tool options system along a design we completed earlier but didn't have the time to implement, or making the tabs and sheets in the palette windows drag & droppable...\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-02
    body: "Ah, thanks, that's what I was looking for :)\n(was indeed looking at that opacity option, and the preexisting gradients... didn't notice the autogradient tab for some reason)"
    author: "Illissius"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-03
    body: "I share the growing interest in Krita, especially since the desire seems to have something closer to Painter than Photoshop.\nMy wife and I are using Gimp and the Gimp Gap plugin to make animation movies (you can see some images here, we're working on a real website : http://svdboom.free.fr) and we're rather satisfied by it, but the gimp team doesn't seem eager to go the paint tools way so having Krita doing it would be a wonderful complement for us.\nThe fact that Krita supports pipe brushes and xcf files is also very interesting, since we use heavily both. This would allow us to work basically the same way on the same files with both tools, using Krita for instance for layouts and Gimp-GAP for animation.\nHowever, I found some issues for this plan right now :\n\n* XCF import is not perfect. There are issues with transparencies, for instance. Do you plan to improve this in the future? Since XCF is an open format, why not using as default format? \n* Pipe brushes work but we use pipes defined in random mode : each a pattern is printed, one random layer of the pipe is selected. With Krita, is seems to me that  the layers are not selected randomly. This is annoying, as the main use of this great feature is to introduce some level of hazard which makes a drawing look less computer-generated.\n\nAnyway, I'm very looking forward to painting tools like watercolor in Krita. :-)\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-03
    body: "Cool work!\n\nWe currently use ImageMagick to import xcf files -- improving the imagemagick xcf filter is on the todo list, but hasn't been done yet. I decided to keep Krita's own file format for a couple of reasons, the most important being that we can add any kind of stuff that way. For instance, I'm working on a watercolor simulation. Things like the current state of the cellular automatons for drying the wet paint would have to be saved, too, otherwise images would dry immediately on saving. Basically, we have different needs from the Gimp for our file formats -- but I definitely need to work on the filter.\n\nYou're quite right about the pipe brushes. The pipe brushes problem is something I simply forgot to put on the todo list -- it was one of the first things I worked on. The thing is, the Gimp uses a parasite that determines how the brush is used, and I haven't yet implemented a parser for those parasites. And I was wondering whether I hadn't better implement compatibility with Paintshop Pro's pipe brushes. And, of course, you cannot create brushes, let alone pipe brushes with Krita at the moment...\n\nThis area needs a little work; I'll put it on the new todo list, and it might also be a good idea if you were to add both items (xcf imp/exp & pipe brushes) as bugs in bugzilla: that way you'll get informed if I get down to doing work on it.\n\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-03
    body: "Thank you for the nice comment. :-)\nNow that you said it, I understand why the outlook I get when importing XCF files is indeed very close to the ones I got when I tried to manipulate these images with IM. I should have thought about that.\nIt's funny how little support there is for the XCF format in open source, while it's one of the older free formats. That's too bad, IMHO.\nI don't think not being able to create pipe brush in Krita is that a problem, as long as you can use the ones created with Gimp or PSP, for that matters. I don't know the latter and what they bring to the painter, just remember that Linux users can easily fire Gimp to create new brushes, but are less likely to do that with PSP. :-)\n\nAnyway, I'll open the bugs (more like enhancements to me) right away.\nIs there kind of a roadmap of the things you want to implement?\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-03
    body: "Well, the Dimp developers explicitly discourage third party apps to implement or use xcf because they want to be free to change the format whenever they need it. It's not meant for an interchange format.\n\nThe current roadmap is at http://websvn.kde.org/trunk/koffice/krita/TODO.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Krita is awesome :)"
    date: 2005-06-03
    body: "OK, but they could at least provide a library supporting the various versions of XCF, a bit like libpng, libtiff or libjpg.\nI understand that it's their decision, but I find it too bad.\nOh, well.....\n\nThanks for the links. \n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Printing"
    date: 2005-06-02
    body: "How good is KOffice at printing?\n\nWith the current KDE, nothing prints as it should."
    author: "KDE User"
  - subject: "Re: Printing"
    date: 2005-06-02
    body: "Huh? IME kde printing works perfectly, indeed I use kprinter to print from things like mozilla and acroread. What problems are you having? Anyway, koffice uses the kde print system, so koffice will print as well or badly as it does."
    author: "mikeyd"
  - subject: "Re: Printing"
    date: 2005-06-02
    body: "KDE uses the exact same print system that Mac OSX uses.\n\nI have had no problems whatsoever with printing under KDE.  KWord (previous versions) has had problems with overlaid graphics, compositing them correctly on screen but printing them incorrectly, but that was an old bug in KWord, not in the print system."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Printing"
    date: 2005-06-03
    body: "If you are referring to the kspread printing issues, I think most of them were fixed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Printing"
    date: 2005-06-03
    body: "Wouldn't the printing quality also depend on the printer driver? For instance, I have an HP Photosmart printer and when I print digital images using the printer's own feature (you can plug the compactflash disk into the printer direct), the quality is fantastic. However, even when using hplip and the latest hp psc driver, I cannot print as good an image using software (KDE or GTk).\n"
    author: "Kanwar"
  - subject: "Re: Printing"
    date: 2005-06-03
    body: "... and sorry for posting the above twice. I tried to get the grammar with print quality v/s printing quality right, actually :-)"
    author: "Kanwar"
  - subject: "Improve KOffice!!!"
    date: 2005-06-03
    body: "by using koffice applications, we the users can improve it by reporting what's not working as expected, and also what's missing which is important to get our work done!\n\nplease use and report the problems, instead of just whining that it is just not up to the mark (like ms word or open office).\n\nbtw slackware 10.1 package:\nhttp://ktown.kde.org/~binner/klax/10.1/koffice-1.3.98-i486-klax.tgz"
    author: "fast_rizwaan"
  - subject: "foreign office files"
    date: 2005-06-03
    body: "AFAIK, MS Office 12 will use the XML file format. So will KOffice (OASIS). Will that make it easier to support MS Office files in KOffice?"
    author: "Claus"
  - subject: "Re: foreign office files"
    date: 2005-06-03
    body: "yes. the current .doc files are proprietary, and hard to read properly. the xml files should be easy. unless microsoft insists no certain restrictions. like \"you can't use this fileformat for free software\" or they embed certain binary, hard-to-read-but-essential parts in the fileformat."
    author: "superstoned"
  - subject: "Re: foreign office files"
    date: 2005-06-03
    body: "Easier doesn't depend on XML or not.\nIt depends if the format is documented well or not.\n\nCurrent MS Office formats would be easy to implement, if only they were documented. So, if the new office xml format isn't documented and available for free, then there's still the same problem.\n\nWell, that's what I think."
    author: "Tim Beaulen"
  - subject: "Re: foreign office files"
    date: 2005-06-03
    body: "KOffice has always used XMl for its files. It has just changed the XML file format. Using XML is just a base, it does not mean that a programm can read any XML file because it can read one type (similar with writing).\n\n(In comparison, you could tell that using XML is like telling \"this language uses Latin letters\". With that assumptution you can do a few more things that if you were not sure which kind of letters you can use. For example one could stupidly copy a written text, as one knows the letters, even if one cannot understand the language itself. But you can also see how many languages are based on Latin characters in the world and that people speaking one of these languages cannot understand the other. Similarly with XML.)\n\nAnd as written by other posters, the main problem for file formats is and remains the documentation.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: foreign office files"
    date: 2005-06-04
    body: "MS Office 12 will be using a (supposedly) open xml (but not OASIS) format.  Time will tell how \"open\" it really is, or how easy it will to interoperate with it."
    author: "Rex Dieter"
---
The KDE Project today announced the release of the <a href="http://www.koffice.org/announcements/announce-1.4-rc1.php">KOffice 1.4 Release Candidate</a>. If nothing disastrous is found in this release, it will be renamed and become KOffice 1.4. A <a href="http://ktown.kde.org/~binner/klax/koffice.html">Live-CD</a> has been created so that you can try out KOffice 1.4 RC without having to commit your hard disc to it. There are currently no binaries available for download of the release candidate. Read <a href="http://www.koffice.org/announcements/announce-1.4-rc1.php">the full announcement</a> and <a href="http://www.koffice.org/announcements/changelog-1.4rc1.php">the changelog</a> for more details.  OSDir has <a href="http://shots.osdir.com/slideshows/slideshow.php?release=353&slide=1">screenshots of the release</a>.










<!--break-->
<p><a href="http://www.koffice.org">KOffice</a> is an integrated office suite with more components than any other suite in existence. This release specifically introduces the database management application <a href="http://www.koffice.org/kexi">Kexi</a>, the image editor <a href="http://www.koffice.org/krita">Krita</a> and upgrades the major KOffice components to use the <a href="http://en.wikipedia.org/wiki/OpenDocument">OASIS Open Document format</a>, an industry standard allowing better interoperability across applications and platforms.</p>











