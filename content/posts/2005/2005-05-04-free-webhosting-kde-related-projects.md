---
title: "Free Webhosting for KDE Related Projects"
date:    2005-05-04
authors:
  - "ob\u00e9dard"
slug:    free-webhosting-kde-related-projects
comments:
  - subject: "Thank you"
    date: 2005-05-04
    body: "I'd like to thank Olivier for his hosting of our Konversation website & wiki. He has been very helpful with problems we faced and you might notice konversation.org is down now but its just a DNS problem. We hope to fix it soon. Anyhow kudos to Olivier for this great offer!\n"
    author: "Cartman"
  - subject: "Re: Thank you"
    date: 2005-05-05
    body: "I've had a lot of trouble with dns related to a lot of free sites sites lately, luckily i run my own dns and can set records to never expire manually.\ni'm not paranoid, but it seems to be a little to much lately. smth is definetly wrong."
    author: "Andy"
  - subject: "Big thanks also from us"
    date: 2005-05-04
    body: "The amaroK Team would also like to thank you. You rock, dude :)\n"
    author: "Mark Kretschmann"
  - subject: "thanks"
    date: 2005-05-04
    body: "Thank you for the contributions, they will benefit all KDE users.\n"
    author: "ac"
  - subject: "Re: thanks"
    date: 2005-05-04
    body: "Yes, very kind and thoughtful of you. Cheers!\n\n"
    author: "CTM"
  - subject: "Zope/Plone hosting?"
    date: 2005-05-04
    body: "I wonder, would it be possible to get hosting for a Plone Site (with Zope, of course). I am planning to make a song-lyrics-presentation-and-management-program, and sooner or later, I'm going to need some hosting :)"
    author: "Mikkel H\u00f8gh"
  - subject: "Re: Zope/Plone hosting?"
    date: 2005-05-04
    body: "I never liked Zope, but it would be possible to create an instance for your project, if it is KDE related of course."
    author: "Olivier B\u00e9dard"
  - subject: "Re: Zope/Plone hosting?"
    date: 2007-05-08
    body: "Plone hosting is available at http://www.universalwebservices.net/web-hosting/plone-hosting"
    author: "Phil L."
  - subject: "kdelook?"
    date: 2005-05-04
    body: "What about hosting for projects that want to be on kdelook, but the filesize download is too large for kdelook.org's limit of 500kb?\nThings such as icon themes, window decorations, styles, etc.?\ncheers,\n-Ryan"
    author: "Ryan"
  - subject: "Re: kdelook?"
    date: 2005-05-04
    body: "I can host any projects that are related to KDE, in any way. So yes, you could host your kde-look/kde-apps projects on my server! :)"
    author: "Olivier B\u00e9dard"
  - subject: "Re: kdelook?"
    date: 2005-05-04
    body: "Now that's music to my ears!  I'll be sure to spread the word."
    author: "Ryan"
  - subject: "Yeah!"
    date: 2005-05-04
    body: "Olivier, you rock man, seriously :))\nThanks in behalf of the whole community!"
    author: "BZ"
  - subject: ":)"
    date: 2005-05-04
    body: "Yes, Olivier is pretty kind. He is really great and give pretty good services. Don't be scare to ask him for help! He offers great help with the amarok's website and I want to thanks him for that even if Mark said it earlier."
    author: "Kenny Lemieux"
  - subject: "and kolab ?"
    date: 2005-05-04
    body: "thank you very much ! \nits great.\n\nwhat about a free Kolab2 server for KDE users ?\n\nthat would be very nice.\n\n"
    author: "somekool"
  - subject: "Re: and kolab ?"
    date: 2005-05-04
    body: "I don't have enough ressources right now to provide that. In a near future, I hope to get another server. Then I'll investiguate this possibility."
    author: "Olivier B\u00e9dard"
  - subject: "Re: and kolab ?"
    date: 2005-05-05
    body: "We do have a free kolab2 server for KDE users, currently KMail/Kontact and kde-docs use it, but I am happy to give kolab2 accounts to any kde realted project that needs it and wants to make use omf kolabs features.\n\nMatt"
    author: "mdouhan"
  - subject: "Re: and kolab ?"
    date: 2005-05-05
    body: "what about pleasure and testing purpose ?\nsomething we could open to any KDE USER ?\n\nnot only developer.\n\n\n"
    author: "somekool"
  - subject: "Re: and kolab ?"
    date: 2005-05-05
    body: "Well this I already provide, accounts valid for one week for testing and play"
    author: "mdouhan"
  - subject: "What server?"
    date: 2005-05-04
    body: "What type of server is that anyway?"
    author: "KDE User"
  - subject: "Re: What server?"
    date: 2005-05-04
    body: "The server is a dedicated Celeron 2.4ghz with 512Mb of RAM, linked to the internet through a 100Mbit full duplex port. It runs Gentoo Linux."
    author: "Olivier B\u00e9dard"
  - subject: "Kolab"
    date: 2005-05-04
    body: "I know someone has mentioned kolab already, but I second this.\n\nOn and off I have brought up the idea for kde to have a kolab server to have a central place to have contact information etc, but had no good place to put it.\n\nHow about it?\n"
    author: "JohnFlux"
  - subject: "Re: Kolab"
    date: 2005-05-05
    body: "I have a KDE kolab server as stated above, any KDE project who wishes to make use of it can have an account and start using kolabs features right away and join KMail/Kontact and KDE-docs who do so already\n\nMatt"
    author: "mdouhan"
  - subject: "thanks derek!"
    date: 2005-05-05
    body: "just to add my word of thanks... i love the digest. :)"
    author: "anon"
  - subject: ":)"
    date: 2005-05-05
    body: "Thank you, dude. I'm only a user, but I appreciate any effort to bring users a better free (as in freedom) technology :D"
    author: "c0p0n"
  - subject: "Just a precision..."
    date: 2005-05-06
    body: "If you search for my name on google (like some did) , you'll get results about another Olivier B\u00e9dard who is running a webhosting company. This is NOT me, it's a pure coincidence. I have nothing to do with that business, and I do not offer free hosting only to attract customers. It was just a precision, since I received emails with questions about the relation of this offer and that business."
    author: "Olivier B\u00e9dard"
  - subject: "New server is up and running !"
    date: 2005-06-14
    body: "Hello,\nmy new server is now up and running! :)\nSempron 2600+\n512Mb RAM\n80G Hard Disk\nGentoo Linux\n10Mbits dedicated link with a 1 Terabyte per month quota .\n\nNow it really rocks :) Now all I need is some website to host !"
    author: "Olivier B\u00e9dard"
---
If you are looking for web hosting for a KDE project, look no further. I am offering free services to all KDE-related projects.  A few months ago, the amaroK devs were looking for a new server to host <a href="http://amarok.kde.org/">their website</a>. Since I own a dedicated server, I offered them space to do so. A few weeks later Konversation was also looking for a server for <a href="http://www.konversation.org/">www.konversation.org</a>. So I decided to contact some KDE projects to offer them the same services and now I want to reach an even higher numbers of developers. 








<!--break-->
<p>If you need web services for your project such as web, e-mail, MySQL databases, FTP server, etc just contact me.  Currently, I am not hosting CVS or SVN repositories but that may happen in the near future. Please note that I am doing this all for free, it is my way to contribute to the KDE project. Contact me as Paleo on the Freenode IRC network or by <a href="http://www.pwsp.net/contact.html">e-mail</a>.</p>









