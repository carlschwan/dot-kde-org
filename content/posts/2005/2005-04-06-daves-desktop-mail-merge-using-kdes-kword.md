---
title: "Dave's Desktop: Mail Merge Using KDE's KWord"
date:    2005-04-06
authors:
  - "David"
slug:    daves-desktop-mail-merge-using-kdes-kword
comments:
  - subject: "The first thing I thought of..."
    date: 2005-04-05
    body: "Hello <name>,\nWe at the company of <company> have exclusive information about <stock> for you!  Please send us <amount> for the info to make you <biggeramount>!!!!\n\nRegards,\n<fakename>\n\nHehehe.  KOffice, the all in one solution for your scamming needs!\n\nJust kidding, thats just the first thing that sprang to mind.  Thanks for the article David."
    author: "Leo Spalteholz"
  - subject: "Re: The first thing I thought of..."
    date: 2005-04-06
    body: "You're welcome, Leo.  I see you've already thought of a use for it. :)\n"
    author: "David"
  - subject: "Too Bad"
    date: 2005-04-05
    body: "Kword is a nice little piece of software. Very nice. Unfortunately it is still suffering from severe printing problems, like reported in Bug #54227. I do have KWord installed, but I can't use it for anything that needs to be printed. For me, OpenOffice is the only choice at the moment."
    author: "Meneer Dik"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "If I knew more about this sort of problem, I'd try and help.  Unfortunately, I'm not a very good hacker.\n\nMaybe, if more interest in KOffice can be generated, there will be a bigger push for further development.\n\nThanks for taking the time to read the article, though."
    author: "David"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "I agree with you David, if more interest in KOffice can be generated, there will be a bigger push for further development. I subscribe to koffice-devel mailing list and I see that KOffice starts to gain more developers. I think if we really like this piece of software and want this to be more improved, we can help this project by helping the developers to spread about this software. I like your article, even though I never use mail merge ;) Thanks for the article David!"
    author: "Yanardi"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "Hacking is not required! I desperately need a moderately competent writer to help me out with the Krita manual. It would be really great if someone could do a introductory tutorial chapter: that could be published on a website, and then become Chapter 1 of the manual.\n\nI'd made a start myself, but I'm working really hard on getting watercolour painting into Krita before the freeze."
    author: "Boudewijn Rempt"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "I'm going to have to give Krita a try."
    author: "David"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "That would be great!\n\nThe binary snapshot I announced on my blog is now seriously out of date; we worked rather hard in the past week or two. If you have problems compiling Krita from cvs, please let me know; I think most of the real snags have been cleared away. I wasn't planning on doing another binary snapshot before this Monday's freeze."
    author: "Boudewijn Rempt"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "I'm using Kanotix (Debian) currently.  I've never compiled from cvs before, but it can't be that diffucult, can it?\n\nI'll give it a try this week/weekend.  I'm looking forward to seeing what all Krita can do.  I'll let you know (via e-mail) if I have any problems with the install."
    author: "David"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "Not hard at all -- as long as you remember to install the devel packages for lcms and ImageMagick, you should be all set to go!"
    author: "Boudewijn"
  - subject: "Re: Too Bad"
    date: 2005-04-06
    body: "These, and other font, problems appear to be in Qt.\n\nI patched the QT PostScript driver so that it gets the font names right.  But, I have not looked into the font spacing problem since it is necessary to change both the screen display and the printing.\n\nIt looks OK on the screen because hinting makes the glyphs larger when displayed at lower resolution but they when they are printed with GhostScript, the glyphs are printed the actual size without hinting and there is, therefore, too much space between the characters. :-(\n\nThere are three possible sizes for glyphs:\n\nScreen resolution hinted\nPrinter resolution hinted (Windows print driver compatibility like OO)\nActual font size not hinted\n\nCurrently, Qt will only do the first one.\n\nA fix is promissed for Qt-4.  Until then, there really isn't any solution to the problem.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "No Koffice updates?"
    date: 2005-04-06
    body: "long time no updates? no new features added?"
    author: "Fast_Rizwaan"
  - subject: "Re: No Koffice updates?"
    date: 2005-04-06
    body: "It won't take very long. Take a look at the release plan for version 1.4: \nhttp://developer.kde.org/development-versions/koffice-1.4-release-plan.html\n\n\n"
    author: "cm"
  - subject: "Re: No Koffice updates?"
    date: 2005-04-06
    body: "Feature/GUI freeze this Monday. No time to linger, must hack!"
    author: "Boudewijn Rempt"
  - subject: "e-mail merge and spreadsheets"
    date: 2005-04-06
    body: "Two things I'd like to see:\n\n* merging from data out of a spreadsheet, textfile or database\n* merging to electronic documents (pdf or html) and send them via e-mail \n  (to address in one of the fields)\n\nI've never cared about paper mailmerge, but e-mailmerge sounds useful to me...\n\n/Simon"
    author: "Simon"
  - subject: "Re: e-mail merge and spreadsheets"
    date: 2005-04-08
    body: "Hi,\n\nin current version from CVS HEAD you can use a spreadsheet from KSpread or the contacts stored in KAddressBook as data source... too sad that's not mentioned in the article.\n\nCiao,\nTobias"
    author: "Tobias Koenig"
  - subject: "Re: e-mail merge and spreadsheets"
    date: 2005-04-08
    body: "Too sad that not everyone runs CVS HEAD.\n\nToo sad that not everyone is familiar with the latest KSpread killer features.\n\nToo sad that not everyone knows how to bribe a KDE developer into writing good docu about his app.\n\nToo sad that not everyone is a \"researching user\", who clicks everywhere and uses the \"trial'n'error\" method to find out which things work and how they do so."
    author: "ac"
  - subject: "Re: e-mail merge and spreadsheets"
    date: 2005-04-08
    body: "My work is provided under a Creative Commons license.  Following it's guidelines, please feel free to take my HowTo and add to it.\n\nLet us know where to find it aftward, I'd be glad to read it.\n"
    author: "David"
---
Although KOffice may not yet be at the level of functionality of OpenOffice, it is still a pretty powerful office suite albeit with relatively little coverage. This is the first of my series of easy How-To's for <a href="http://www.koffice.org/">KOffice</a> that hopefully anyone can follow. The first How-To, including screenshots, shows you how you can easily <a href="http://www.virtualsky.net/daves/2005-06.htm">make use of the mail merge feature</a> in your <a href="http://www.koffice.org/kword/">KWord</a> documents.


<!--break-->
