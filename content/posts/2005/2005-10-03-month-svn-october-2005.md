---
title: "This Month in SVN for October 2005"
date:    2005-10-03
authors:
  - "canllaith"
slug:    month-svn-october-2005
comments:
  - subject: "Frist psot!"
    date: 2005-10-03
    body: "And just to make sure that everybody knows that we are <b>not</b> slave drivers, but very nice and sweet people. Bah!"
    author: "Inge Wallin"
  - subject: "Last picture is 404"
    date: 2005-10-03
    body: "The last picture of Karbon is not there or the link is wrong. Please fix.\nNice to see that stuff in the graphics domain is also happening with Karbon, not only Krita."
    author: "Phase II"
  - subject: "Re: Last picture is 404"
    date: 2005-10-03
    body: "Fixed. Thanks :)"
    author: "canllaith"
  - subject: "commit-digest?"
    date: 2005-10-03
    body: "commit-digest?\n\nNice report but what is going on with Derek Kite's Commit-digest?\n\nOn http://commit-digest.org last is from 2 Sep.\n\nm.\n"
    author: "m."
  - subject: "Re: commit-digest?"
    date: 2005-10-03
    body: "Yeah, right after the interview by people behind kde he disappeared.\nWhere did they capture him to ???\n\n\nAdrian"
    author: "Adrian"
  - subject: "Re: commit-digest?"
    date: 2005-10-03
    body: "I tried to help out with it for a couple times, but really don't have the time with school and life in general.  \n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: commit-digest?"
    date: 2005-10-03
    body: "yeah, that's really a pity :( \n\nThe whats new in SVN report is also great, but it's sad that I lost my weekly news of the commit digest."
    author: "Martin Stubenschrott"
  - subject: "Very Nice Format !"
    date: 2005-10-03
    body: "I enjoyed the tour to \"This month in SVN\". Very informative, very professional, very wasy on the eye, great work ! Thanks so much!"
    author: "MandrakeUser"
  - subject: "Re: Very Nice Format !"
    date: 2005-10-03
    body: "sorry, meant to say \"easy on the eye\", what a wasy I am, hahaha"
    author: "MandrakeUser"
  - subject: "Re: Very Nice Format !"
    date: 2005-10-03
    body: "It's actually \"very easy on the eyes\""
    author: "Joe"
  - subject: "Re: Very Nice Format !"
    date: 2005-10-03
    body: "Maybe he wears one of those cool pirate eye-patches. Arrr!!!"
    author: "Another user"
  - subject: "unique like with Qt4?"
    date: 2005-10-03
    body: "is there a qsort or function that get rids of duplicates in a QList\nsuch as the SGI function  unique(V.begin(), V.end())?"
    author: "Joe"
  - subject: "Re: unique like with Qt4?"
    date: 2005-10-03
    body: "Qt4 has an stl like iterators ,so can use any standard method want ."
    author: "ma"
  - subject: "IMAP filters in KMail"
    date: 2005-10-03
    body: "I'm especially happy about the ability to use client side filtering on IMAP accounts. This makes it possible for my wife to start using the flexibility of IMAP, but creating her own filters (instead of asking me to edit ~/.procmailrc ;) )\n\n3.5 COME TO PAPA! :)"
    author: "parena"
  - subject: "Some eye candy"
    date: 2005-10-04
    body: "Some eye candy from 3.5: http://www.kde-look.org/content/show.php?content=29557"
    author: "Zammi"
  - subject: "Re: Some eye candy"
    date: 2005-10-04
    body: "Thanks ! Very nice and useful. I don't personally like transparency in places like menues where you need to read text and it reduces the contrast. OTOH, this is brilliant, 'cause you don't really care to much for good contrast on the window frame. Moreover, you'd rather have the chance to see what's behind the frame most times. \n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Some eye candy"
    date: 2005-10-05
    body: "Transparency is not good at all the time. But we can make use of it with good/ balanced blurred level. This is something about it: http://baghira.sourceforge.net/blurring.shtml"
    author: "Zammi"
  - subject: "wallpapers"
    date: 2005-10-07
    body: "Has somebody a link to those cool new wallpapers?"
    author: "shiny"
---
The fourth edition and last of the KDE 3.5 series of <a href="http://www.canllaith.org/svn-features/svn-oct.html">This Month in SVN</a> is now ready for consumption. This month looks at new artwork, <a href="http://kopete.kde.org">Kopete</a> webcam support, <a href="http://kmail.kde.org">KMail</a> improvements and <a href="http://edu.kde.org">KDE EDU</a> as well as a glimpse at <a href="http://www.koffice.org">KOffice</a>. Next issues will be looking at KDE4 development. Get it while it's hot!


<!--break-->
