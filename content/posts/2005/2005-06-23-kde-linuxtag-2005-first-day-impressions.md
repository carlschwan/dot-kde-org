---
title: "KDE at LinuxTag 2005: First Day Impressions"
date:    2005-06-23
authors:
  - "jspillner"
slug:    kde-linuxtag-2005-first-day-impressions
comments:
  - subject: "tv team"
    date: 2005-06-22
    body: "hi,\ni have seen a ZDF (german tv channel) tv team on the booth fotos.\nDoes someone know from which tv-magazine they are, and when i can expect to see something from the linuxtag on TV?"
    author: "pinky"
  - subject: "Re: tv team"
    date: 2005-06-23
    body: "Maybe for \"Neues\" on 3sat? I don't think that there are 3sat branded microphones/teams."
    author: "Anonymous"
  - subject: "Re: tv team"
    date: 2005-06-23
    body: "Yes, this was a team from \"Neues\". If the scenes from our booth will be shown, you can see them this friday on 3Sat.\n\nCheers,\n  Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: tv team"
    date: 2005-06-23
    body: "Small correction:\n\"Neues\" is shown on Saturday, 17:00.\n"
    author: "Michael Goettsche"
  - subject: "Re: tv team"
    date: 2005-06-26
    body: "http://www.3sat.de/neues/sendungen/magazin/80683/index.html is similar to what was said in the TV report."
    author: "Anonymous"
  - subject: "Hmm"
    date: 2005-06-22
    body: "I hope that the present policians can be persuaded to act against software patenting in the EU. I recently saw that the economic-majority.com project gained much support over the last two days and having regard of the monday desaster."
    author: "Gerd"
  - subject: "Re: Hmm"
    date: 2005-06-23
    body: "YES !!! PLEASE KDE PEOPLE AT LINUXTAG , make a big sign : Against Software Patents !!!!! PLEASE there are many politicians who will see it !!!!\n\n\n"
    author: "ch"
  - subject: "Re: Hmm"
    date: 2005-06-23
    body: "perhaps even TV (ZDF, ARD) will show it on camera !"
    author: "ch"
  - subject: "Re: Hmm"
    date: 2005-06-23
    body: "Today I received a kind of helpcall from a European Parliament worker. The current problem is the astroturfing campaign in Brussels. Scania even hired a whole building in brussels for the Innovation day tomorrow (?). Many fake SMEs run around there. If you have a few days left, please come to Brussels or use your media relations. \n\nThe other side prepares for the final punch\nhttp://wiki.ffii.org/Eicta050622En\nhttp://wiki.ffii.org/Eei050527En\nhttp://wiki.ffii.org/Cii050622En\n\nand there is only one final SME conference from our side left\nhttp://wiki.ffii.org/Konf050629En\n\nOf course what really matters now are SME representatives that go to Brussels before it is too late or talk to their MEPs.  "
    author: "Andre"
  - subject: "Not worth the money for me"
    date: 2005-06-22
    body: "Why did they put an entrance fee on this year's LinuxTag? I would really have liked to come by and visit it, but as a student, I can hardly scrap together the money for the train ticket, and now I should also pay what, 25 Euros(?) entrance fee? That's ridiculous.\nI mean, if they wanted they could just put a fee on the presentations or workshops, where all the big company guys with the money want to go to... but why should I pay to see free software? Nah, that's not worth it for me (sadly).\n\nJust my 2 (Euro-)cents\n\n--Michael"
    author: "Michael"
  - subject: "Re: Not worth the money for me"
    date: 2005-06-22
    body: "> but why should I pay to see free software?\n\nbecause free software is about freedom and not about price. Linuxtag is not cheap and someone have to pay it at the end of the day.\nThere was also a \"invite-program\" were you can get a invitation and come to linuxtag for zero cost.\nI'm a student, too. I will go to linuxtag on saturday and i will pay for it. Why? Because i like linuxtag and i want to support all the great people who make this happen every year. I think 15EUR is ok. As a student i don't have much money. But if i go to linuxtag on saturday i don't go out friday night and also want to stay at home if i come back saturday evening. A normal weekend cost more than 15EUR. It's just a question about setting priorities."
    author: "pinky"
  - subject: "Re: Not worth the money for me"
    date: 2005-06-23
    body: ">because free software is about freedom and not about price.\n\nMy sentements exactly :-D\n\n~Macavity"
    author: "macavity"
  - subject: "Re: Not worth the money for me"
    date: 2005-06-23
    body: "\"I would really have liked to come by and visit it, but as a student, I can hardly scrap together the money for the train ticket, and now I should also pay what, 25 Euros(?) entrance fee? That's ridiculous.\"\n\nSo, KDE, Linux, X.org, Samba etc. etc. are not worth 25e to you? are they worthless?"
    author: "Janne"
  - subject: "Re: Not worth the money for me"
    date: 2005-06-24
    body: "I couldn't raise \u00a31million to save my life, but that doesn't mean my life is worth less than that to me :)\n"
    author: "JohnFlux"
  - subject: "Re: Not worth the money for me"
    date: 2005-06-23
    body: "Come on guys, no one is saying Free Open Source is not worth it or free as in beer. You have to realize that not all student have the money. Some need a break. LinuTag should have made it free for students or at least charge a small nominal fee. Have mercy on the poor students will you.\n\n  -Abe"
    author: "Abe"
  - subject: "Re: Not worth the money for me"
    date: 2005-06-23
    body: "For one day it is just 10 Euro if you have a student card. \nI was there yesterday and I really endjoyed it!\nAnd it was not only to \"see free software\" but much more (see their HP..)\nIt was definitly a good investment for me :-)\n\nMirjam\n\n"
    author: "Mirjam"
  - subject: "Flyers"
    date: 2005-06-23
    body: "Is there a link for the flyers source that I see on a pic? They look great and I need some for a Montreal booth I'll (wo)man soon."
    author: "annma"
  - subject: "stuffed konqi"
    date: 2005-06-24
    body: "Where are these to buy? Ive seen them everywhere but everyone onle seems to get one by luck. So who seels them?"
    author: "Andy"
  - subject: "Re: stuffed konqi"
    date: 2005-06-24
    body: "See http://www.kde.org/stuff/merchandise.php"
    author: "Anonymous"
  - subject: "Re: stuffed konqi"
    date: 2005-06-25
    body: "Some additional merchants: linuxland.de, ixsoft.de, emedia.de, tuxman.de - the latter has also an English shop interface."
    author: "Anonymous"
  - subject: "Re: stuffed konqi"
    date: 2005-06-24
    body: "Use the merchandise page at kde.org as a startingpoint (see the above comment). I learned that I have just about enough grasp of German to order stuff on-line:-) A small mistake with the shipping of some of the other items I ordered, may become a challenge for me to get corrected though. \n\nI just got mine today and it's just gorgeous:-) "
    author: "Morty"
  - subject: "Re: stuffed konqi"
    date: 2005-06-24
    body: "<a href=\"http://www.freibergnet.de/fgnet/scan/co=yes/fi=products/sf=prod_group/se=Fanartikel/op=eq/sf=category/se=KDE/op=eq/tf=description.html\">Quick link</a> for the lazies.\n"
    author: "Anonymous"
---
The annual KDE show at <a href="http://www.linuxtag.org">LinuxTag</a> has begun already. With talks about KCall and 'KDE: 3, 2, 1!', users had a great chance to learn about existing and new features of KDE. The booth was crowded as always. We were visited by politicians, entrepreneurs interested in deploying the Kiosk framework on Internet terminals, and for tomorrow a guided tour for pupils is planned so they can learn about how to effectively use their new desktop in school - which is KDE. 

<!--break-->
<p>Kurt Pfeifle did a great job at setting up a central infrastructure which includes print and NX servers which makes it easy to display KDE 3.4 and features of the upcoming 3.5 version in parallel. Questions about Qt 4, icon themes, and the cute big stuffed Konqi were answered by the booth helpers. As a special feature, a "live bug reporting" initiative has been announced, so people can report bugs live at the booth and by chance it will be fixed directly, if not they might be contacted by the developers who will show up later that week. Someone offered to print flyers for the project - anyone wants to create some flyers for KDE which include the latest and greatest features?
</p><p>
More exciting information will be posted here shortly. Stay tuned and 
enjoy <a href="http://www.kstuff.org/lt2k5/day1/">selected booth fotos</a>
in the meanwhile. Or even better: pay the KDE team on LinuxTag a visit and get in touch with the creators of your favorite desktop environment!</p>
