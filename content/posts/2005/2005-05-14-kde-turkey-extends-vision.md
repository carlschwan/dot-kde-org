---
title: "KDE Turkey Extends Vision"
date:    2005-05-14
authors:
  - "bmetin"
slug:    kde-turkey-extends-vision
comments:
  - subject: ":)"
    date: 2005-05-14
    body: "Merhaba, Merhaba, Mer-haaaa-baaaaa :)"
    author: "B\u00fclent Ersoy"
  - subject: "te?ekk\u00fcrler"
    date: 2005-05-14
    body: "thanks so much! I only wish they had an english translation of all those pages ;)"
    author: "Harald Fernengel"
  - subject: "local groups on the move!"
    date: 2005-05-14
    body: "Congratulations to the Turkish KDE community for founding \"KDE Turkey\". I look forward to hear more from your (local) efforts! \n\n\nCiao'\n\n\nFab"
    author: "Fabrice"
---
Free software developers in Turkey aim more success in the forthcoming months by enlarging its member base. <a href="http://www.kde.org.tr/">KDE Turkey</a> (in Turkish) was founded 6 months
ago with the aim to raise KDE usage, consciousness and also to be a central
point for KDE endeavour in Turkey.


<!--break-->
<p>Today KDE Turkey is responsible for Turkish localization of KDE. Besides localization efforts, KDE group members also create awareness with talks, IRC meetings, mailing lists, <a href="http://www.linuxmarket.org/esantyon.shtml">merchandise</a> (in Turkish) and with the attendance to various local organizations.</p>
<p>
Founded with the support of <a href="http://www.lkd.org.tr/about.us.php">Turkish Linux Users Association</a>,
KDE Turkey now cooperates with national Linux distribution <a href="http://www.uludag.org.tr/eng/">Pardus</a> in the localization field. Statistically, in 6 months the visitor counts for group's web sites have quadrupled. KDE Turkey's population is growing rapidly every day with its 26 active members and contributors.</p>
<p>
KDE Turkey will attend the <a href="http://senlik.linux.org.tr">4. Linux
and Free Software Festival in Turkey</a> (in Turkish), with Bar&#305;&#351; Metin's KDE Programming presentation.</p>
