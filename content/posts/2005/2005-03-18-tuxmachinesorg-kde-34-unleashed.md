---
title: "tuxmachines.org: KDE 3.4 Unleashed"
date:    2005-03-18
authors:
  - "Anonymous"
slug:    tuxmachinesorg-kde-34-unleashed
comments:
  - subject: "DotDoted?  Or is it just me?"
    date: 2005-03-18
    body: "\"An error occurred while loading http://www.tuxmachines.org/node/308:\nTimeout on server\n Connection was to www.tuxmachines.org at port 80\""
    author: "Corbin"
  - subject: "Re: DotDoted?  Or is it just me?"
    date: 2005-03-18
    body: "Konqueror is too fast to time out compared to other browsers, unfortunately.  The site is slow but responds."
    author: "Anonymous"
  - subject: "Re: DotDoted?  Or is it just me?"
    date: 2005-03-18
    body: "I just tried w/ FireFox and it timed out too...  I don't really need to read the review though since I already have 3.4..."
    author: "Corbin"
  - subject: "Re: DotDoted?  Or is it just me?"
    date: 2005-03-18
    body: "No, sorry, I'm just running out of bandwidth.  I'm not a big commercial site, it's just little ole me on a home server.  Most of the time it's not a problem, but when big sites link to it, I run out of pipe.  But I'd appreciate your trying back later.  Thanks to the gent who posted the text here.  \n\nthanks,\n-srlinuxx"
    author: "srlinuxx"
  - subject: "Re: DotDoted?  Or is it just me?"
    date: 2005-03-24
    body: "Uh, what?  You get a prize for cluelessness."
    author: "maestro"
  - subject: "Article Text (5, Informative)"
    date: 2005-03-18
    body: "Defined as a network transparent contemporary desktop environment for UNIX workstations similar to the desktop environments found under the MacOS or Microsoft Windows, KDE provides an easy-to-use highly customizable integrated graphical interface for today's most demanding tasks. These include email communication, newsgroup participaton, web surfing, instant messaging, graphic design and manipulation, multimedia capabilities thru audio and video applications, system monitoring, file managing, and even software package handling. Today we will look at the latest incarnation. \n\nKDE 3.4 is a reflection of the many years of development invested into the project with more shine and polish than ever experienced with any desktop environment in existence today. Beginning in October of 1996 with just a handful of developers it helped spawn such major projects as Mandrakelinux in 1998. Today there are an estimated 900 developers improving the approximate 5 million lines of code comprising the KDE desktop. \n\nToday features expand much beyond basic functionality to encompass some of the most advanced options imaginable. Some new features in 3.4 include:\n\nText-to-Speech API \nUse of .ogg Vorbis files \nRead support for XCF images \nSupport for passwordless wallets \nAdded accessibility features to the KPersonalizer \nLogitech mouse specific features \nAdd keyboard gestures for enabling AccessX \nDrag 'n drop between panels \nSystem Tray Icon Hiding \nReboot options in kdm \nMiddle-click functionality in konqueror \nCursor indication when hovering mailto: link \nSupport for the XComposite extension \nLimit on history size relaxed in klipper \nSupport for images in klipper \nAutolock feature in kscreensaver \nSupport for SVG wallpapers \nDrag & drop files or contacts on the chatwindow in kopete \nSupport incoming MSN handwrite messages in kopete \nContact behaviour statistics plugin in kopete \nKOrganizer Journal Plugin in kontact \nX-Face support and Smileys in kmail \nX-Face support in knode \nDNS-based service discovery \nIOslave abstraction for a trashcan and better implementation for other media\n\nOne of the most obvious things one might notice upon their login to 3.4 is the vastly improved speed at which it functions. Desktop start up time is decreased by my approximations of 50% over 3.3. The applications on my desktop open up in about one second. I usually have one instance of konqueror preloaded and I open it to a blank page, but it's opening is just to fast for this human to clock as it is almost instanteous. Kontact takes about one second and kcontrol about two.\n\nAnother improvement is the increased stability. Gone are the days of konqueror crashing on a java or flash site. No more are the crashes of kmail as it re-indexes or compresses a large folder. A distant memory are the crashes of knode due to a malformed character or large image in a usenet post. No longer does one experience freezes as one checks their klipper content. Past is the lagging as one tries to move around large files. It may have went through some growing pains over the years, but KDE has become mature, stable, and complete. We have finally reached desktop utopia.\n\n\n\nHowever, the most noticable improvement is the default look. Sporting an attractive wallpaper, jazzy icons and sexy window decorations, one almost hates to customize. But customize we shall and no one makes it easier than KDE. Even installing themes and icons are a breeze within the kde control center. Setting them for use is a no brainer. This is the first thing I do.\n\n\nOne can configure a wallpaper right from their desktop. In fact one can install new wallpapers right from their desktop. Right click on your desktop and choose Configure Desktop. Right there in the first heading Background, one can click the button Get New Wallpapers. This opens one of KDE's best new features, a Get Hot New Stuff dialogue box. In this application, Hot New Stuff contains choices of wallpapers from kde-look.org with tabs for Highest Rated, Most Downloads, and Latest. One can even get detailed information on their choices by clicking Details. Choose a wallpaper and click Install. Close the applet and navigate to your ~/.kde/share/wallpapers folder and choose one of your newly downloaded wallpapers. From this same Configure Desktop applet one can set their general desktop Behavior, Multiple Desktops, Screensaver and Display variables including power control. \n\n\n\nI have much more in mind when I customize, so I prefer to just open the KDE Control Center. I've always appreciated the integrated controls of the kcontrol application. KDE Control Center is just that, the main configuration hub for KDE containing all necessary modules for setting up a beautiful and functional desktop. \n\nAnother application that will be making use of Get Hot New Stuff is kopete. Soon one will be able to connect to kde-look.org and download new emoticons. Developer Will Stephenson says, \"Implementing this was incredibly easy - only 8 lines of code - and since KMail, Kopete and Konversation share the same emoticon themes, the other apps will be able to use the new emoticons too.\" \n\nkstars also uses the Get Hot New Stuff interface to download extra datafiles. Click, click, click to a more complete experience. Jason Harris says of kstars' new look and functionality, \"the layout is much less cluttered, and all of the data fields now use KActiveLabel(API|LXR), so the text can be copy/pasted with the mouse.\"\n\n\n\nNew in accessibility is some high and low contrast light and dark color themes with monochrome Flatmono icons. This can be a great advantage to individuals with visual impairments.\n\n\n\nKSayIt is a utility that simply speaks a given textfile or the actual content of the clipboard.\n\n\n\nThere are wonderful things in kontact in 3.4. One of which is the news aggregator is now part of kpim and includes http caching, appearance/font settings, uses kNotify for notifications, and KDEPIM's progressbar.\n\n\n\nBut most notable in kcontact is the support for smileys for your communications in kmail. :) I'm a sucker for the eyecandy.\n\n\n\nOther improvments include passwordless kwallet, support for gimp's native xcf format, and highlighted address bar to indicate encryption status in konqueror.\n\nI've saved the best for last. Transparency. Transparent windows is one of the newest trends in x11 and now KDE. It works fairly well but in default state it's limited, slow and a bit unstable. I wasn't able to get transparency above 75% and kde crashed out to the terminal more than once. In it's infancy transparency makes a beautiful effect. But this feature needs some more time to mature. I don't use it on an everyday basis, but it's nice a feature to include when showing off your desktop to windows users. \n\n\nThere are just so many new additions, features, and improvements that I can not possibly include them all. For a complete list of new features in 3.4 visit developer.kde.org. I can not stress enough how impressed I am with this latest version. I've said more than a few times how stable it is and I think to everyone that is the most important thing a desktop needs. The install from sources was effortless requiring a mere ./configure, make, make install. Most major distributions are already uploading their compatible versions to mirrors, but anyone can install these from the source. Sometimes details like that get overlooked in reviews, but I think it's very important for a package to compile cleanly without a lot of fuss and muss on the part the user. There is no reason to wait. Visit kde.org for a full list of mirrors. \n\nThis is most likely the last feature release in the 3.x series. Makes one wonder how they can possibly improve KDE enough to make a new major version. I for one can hardly wait for the next round."
    author: "Anonymous"
  - subject: "Re: Article Text (5, Informative)"
    date: 2005-03-19
    body: "\"But most notable in kcontact is the support for smileys for your communications in kmail. :) I'm a sucker for the eyecandy.\"\n\nFor the life of me, i can't find this feature. Where is it?\n"
    author: "Fred"
  - subject: "Re: Article Text (5, Informative)"
    date: 2005-03-19
    body: "settings > configure kmail > appearance > message window > replace smileys with emoticons."
    author: "srlinuxx"
  - subject: "whoops!"
    date: 2005-03-18
    body: "> Jason Harris says of kstars' new look and functionality, \n> \"the layout is much less cluttered, and all of the data \n> fields now use KActiveLabel, so the text can be \n> copy/pasted with the mouse.\"\n\nTrue, I did say that.  But I was talking about the new \nlayout of the KStars Details window:\nhttp://www.kdedevelopers.org/node/view/917\n\n...which is one of the first things I committed *after* \nthe 3.4 freeze ended.  So non-CVS users won't see it until \n3.5, unfortunately.\n\nThanks for the mention, though!  :)\n"
    author: "LMCBoy"
  - subject: "Re: whoops!"
    date: 2005-03-19
    body: "dang, sorry, how embarrassing.  Seems I mess up something every article.  If it's not a typo, then a grammer or spelling mistake and now wrong information.  Well, the interface did seem nice and clean, but I admit I couldn't figure out the copy/paste thing.  I thought it was a blonde moment.  :P  No wonder. :roll: I hope that's all I messed up this time.  :P \n\nthanks,\n-s"
    author: "srlinuxx"
  - subject: "Home = Applications"
    date: 2005-03-18
    body: "http://www.tuxmachines.org/gallery/kde3-4/kde34_14\n\nThe home directory link under \"Applications\" does not make much sense to me.\n\nEsp. Home (Personal files) is a strange description"
    author: "Jakob"
  - subject: "Re: Home = Applications"
    date: 2005-03-18
    body: "You're aware that this starts an application (Konqueror) within your home? :-)"
    author: "Anonymous"
  - subject: "Re: Home = Applications"
    date: 2005-03-18
    body: "yes, \"Applications\" is a silly place for it. really, the whole \"applications menu\" paradigm is broken IMO. there is no good way to do it. watch people who use MS Windows with the start menu and note how Microsoft keeps moving further and further away from an \"applications menu\". MacOS X doesn't even really have one (they use offer something different), and GNOME's breaking up the menu into three bits is a very nice band aid but not a fix.\n\nas for \"Home (Personal Files)\", that's a great description for what it is. the metaphore of your \"Home\" (versus the whole World (the network) or other homes on your block (other users on the machine)) is very strong and accurate. you keep your stuff in your house, so that's where your files are kept to: in your home.\n\ntry explaining \"My Documents\" to a four year old. try explaining why there's a \"My Documents\" to a new adult user... it's not easy, probably because it leverages _other_ computing metaphors (user permissions and the \"document\" concept). \"Home\" on the other hand is simple and clean and maps directly to the real world."
    author: "Aaron J. Seigo"
  - subject: "Re: Home = Applications"
    date: 2005-03-24
    body: "Uh, the home directory is just the directory at the top of your personal directory tree -- it's a \"home\" in the sense of a starting point.  The terminology is from the command-line unix of the 70's (and was probably in use even earlier); it is not at all analogous to your physical home, and your claim about mapping directly to the real world is silly blather."
    author: "maestro"
  - subject: "Re: Home = Applications"
    date: 2005-03-25
    body: "i appreciate your complete of comprehension and imagination."
    author: "Aaron J. Seigo"
  - subject: "All fine except..."
    date: 2005-03-18
    body: "It's a shame IMHO that a the newest KDE release again has the \"icons stays never on the place were the user has them placed\" problem.\n\n"
    author: "DireWolf"
  - subject: "Re: All fine except..."
    date: 2005-03-19
    body: "Damn that hurts\n"
    author: "Me"
  - subject: "Re: All fine except..."
    date: 2005-03-19
    body: "Um, wasn't this fixed in the latest Qt?"
    author: "ac"
  - subject: "Re: All fine except..."
    date: 2005-04-04
    body: "They work for me now.  I used to have the \"icons moving all over whenever I restart\" problem, but it went away at some point, I couldn't tell you if it was when I upgraded to 3.4 or before that."
    author: "kundor"
  - subject: "Konqueror Crashes & Burns"
    date: 2005-04-14
    body: "I recently installed Kubuntu. One of the reasons I installed it was KDE 3.4. Imagine my dissappointment when I found Konqueror crashing at the drop of a hat several times in the course of a single hour for no apparent reason whatsover. Konqurer has been notorious for this sort of thing for a long time and I just don't see evidence of it really being fixed. I've heard good things about 3.4 but if anything Konqueror seems worse than ever. Sorry folks, I really like KDE, but this is just woeful."
    author: "Jez"
  - subject: "Re: Konqueror Crashes & Burns"
    date: 2005-04-14
    body: "This is a known problem of Kubuntu's KDE 3.4\n\nhttp://www.ubuntuforums.org/showthread.php?t=26715\n\n\nKDE 3.4 (and of course Konqueror) is rock solid for me (not using Kubuntu)."
    author: "Anonymous"
  - subject: "KDE 3.5.2 konqueror bug ?"
    date: 2006-08-04
    body: "Most of the time, when i select text from a web page opened in Konqueror and hit CTRL + C it didnt copy anything :( When i press CTRL + V it pastes my earlier selection, not the one from Konqueror... IT DAMN ANNOYING!"
    author: "prowler"
---
<a href="http://www.tuxmachines.org">Tuxmachines.org</a> has one of the <a href="http://www.tuxmachines.org/node/308">first reviews of KDE 3.4</a> (<a href="http://www.tuxmachines.org.nyud.net:8090/node/308">coral cache</a>). From the article: <em>"KDE 3.4 is a reflection of the many years of development invested into the project with more shine and polish than ever experienced with any desktop environment in existance today."</em> Screenshots are also <a href="http://www.tuxmachines.org/gallery/kde3-4">available</a> (<A href="http://www.tuxmachines.org.nyud.net:8090/gallery/kde3-4">coral cache</a>).






<!--break-->
