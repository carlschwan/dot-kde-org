---
title: "People Behind KDE: Nuno Pinheiro"
date:    2005-08-09
authors:
  - "jriddell"
slug:    people-behind-kde-nuno-pinheiro
comments:
  - subject: "Right answer"
    date: 2005-08-09
    body: "> - Which T.V. show world would you fit right into?\n> \n> Star Trek.\n\nRight!\n\nWho wouldn't? :-)\n\nThough \"The Simpsons\" would also be an acceptable answer.\n\n"
    author: "Thiago Macieira"
  - subject: "is this oxygen site?"
    date: 2005-08-09
    body: "http://oxyx.oxygen-inc.com/"
    author: "fast_rizwaan"
  - subject: "Re: is this oxygen site?"
    date: 2005-08-09
    body: "No it is not, we are stil working hardly on the theme so no time for a page, any way we intend to make a presentation of it at the Akademy. "
    author: "pinheiro"
  - subject: "Re: is this oxygen site?"
    date: 2005-08-10
    body: "Of course he meant \"working hard\" ;-)"
    author: "MaX"
  - subject: "Nuno rocks!"
    date: 2005-08-09
    body: "He is really a helpful and passionate guy. Thanks for the cool artwork you made for us (KDE-NL). \n\nTake care'\n\nFab"
    author: "Fab"
  - subject: "You are cool Nuno Pinheiro!"
    date: 2005-08-09
    body: "liked your interview. how about some screenshots of your oxygen iconss, and you wallpaper, where can I get that?"
    author: "fast_rizwaan"
---
The latest <a href="http://www.kde.nl/people/nuno.html">People Behind KDE</a> interview is with <a href="http://www.kde.nl/people/nuno.html">Nuno Pinheiro</a>.  Nuno has been working hard making the <a href="http://kde.openoffice.org/">KDE OpenOffice</a> icon set.  The Portugese civil engineer is also working on the promised new Oxygen icon theme.  As usual we find out about the non-geek side too and meet his two dogs.


<!--break-->
