---
title: "OSNews Reviews KDE 3.4 Beta2"
date:    2005-02-17
authors:
  - "jriddell"
slug:    osnews-reviews-kde-34-beta2
comments:
  - subject: "movie?"
    date: 2005-02-17
    body: "you should make a movie of kde's new composite features. When your Windows go slowly transparent when you move them. And where passive Windows fade to 50%. This is just so awsome !!!!!\n\nkeep up the eye candy. we will have better eye candy than mac osx or perhaps we already have :-)\n\nch."
    author: "chris"
  - subject: "Re: movie?"
    date: 2005-02-17
    body: "Like http://dot.kde.org/1107971557/1108187210/ ?"
    author: "Anonymous"
  - subject: "Re: movie?"
    date: 2005-02-17
    body: "where is the movie there ? i only see the image. its not that buggy anymore."
    author: "chris"
  - subject: "Re: movie?"
    date: 2005-02-17
    body: "Then try http://bssteph.irtonline.org/kde_eye_candy_again"
    author: "Anonymous"
  - subject: "Not enough details, but good"
    date: 2005-02-17
    body: "Much more depth was needed, only a fraction of enhancements were listed, but in was decent.\n\nA movie would be great, people do not read so much, but a nice movie showing KDE 3.4's feature would truly be amazing."
    author: "Matt"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "The upcoming KDE 3.4 Feature Guide is planned to have flash movies."
    author: "Anonymous"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "That's GREAT!"
    author: "Anonymous"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Are you serious, where did you read this.\n\nThat would truly be remarkable!!! Every KDE release should have something lixe that.\n\nWill the feature guide onlf highlight new features or show off existing features from 3.3.2 as well? "
    author: "Al"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Only new."
    author: "Anonymous"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Why Flash?  Why not SVG?"
    author: "am"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Flash movies?  or flash movies?\n\nMacromedia Flash is proprietary crap that doesn't work on many of the platforms KDE works on.  I hope KDE folks will have the sense to avoid it and use open standards."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Do you want some cheese with that whine? If they do implement flash-movies, think of it as an added bonus. Right now we have NO demonstrations of the new capabilities. It's better IMO to have such demonstrations, even if they didn't work on all machines."
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "<em>Do you want some cheese with that whine?</em>\n\nNo, but a mature discussion of an important principle would have been nice.  I'm sorry if you fail to understand it."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "I did offer \"mature discussion\". right now we have NO demos of the new features. Nothing, nada, zilch! If KDE in the future did offer flash-movies that demonstrated the new features, it would be infinitely better than what we have right now, even if it didn't work on all the machines! And it wouldn't be THAT important in the end. We have done just fine without demos. Any demos that we might get would simply be an added bonus.\n\nWould it be good if they used some OS-tool for it? Sure! But maybe flash is the easiest way to do it? If you desire an OS-solution to this, why not create your own demos using some OS-tool to do it? What was that? You are not willing to do it? You want someone else to do it instead, but he must do it the way YOU want it to be done? O....K...."
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Still not mature.\n\nBut there are plenty of open source ways to do a demo, it only takes a moment to be concerned and choose the best approach.  Flash is NOT the easiest way to do it."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Perhaps if you suggested another way of doing it people could take you more seriously, but honestly, you do come across as whining for no good reason.\n"
    author: "Jim"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Well, excluding members of the community is called discrimination, and some of us think it's a VERY good reason to seek other alternatives.  I'm aware that others see no such issues, even in massive problems like one company's domination of the desktop market, but that's their problem, not mine.  I'm pointing this out in the hope that the people in question will take note, not in the hope of converting every dot.kde reader to ethical computing.  Take it or leave it.\n\nIf you really want to know, some of the options are discussed here:\n\nhttp://software.newsforge.com/software/04/08/16/2128226.shtml?tid=75\n\nIt sounds like the person in question is planning to use one of the tools mentioned: vnc2swf.  But it's more common and probably easier for everyone (including him/herself) to use xvidcap."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "> Well, excluding members of the community is called discrimination\n\nPlease learn the meaning of words before you use them.  It's not discrimination if you differentiate based upon merit.  If there's no other feasible way of providing the demo, then it's not unfair to provide it to only those that are capable of viewing Flash.\n\n> I'm aware that others see no such issues, even in massive problems like one company's domination of the desktop market, but that's their problem, not mine.\n\nThat has *nothing* to do with the topic at hand.\n\n> I'm pointing this out in the hope that the people in question will take note, not in the hope of converting every dot.kde reader to ethical computing.\n\nWhat's unethical about Flash?  It sounds like you are speaking from a position of ignorance.\n\n> It sounds like the person in question is planning to use one of the tools mentioned: vnc2swf. But it's more common and probably easier for everyone (including him/herself) to use xvidcap.\n\nMPEG videos are usually *much* larger in file size and bandwidth usage than Flash files.  That alone could be a deciding factor.  Furthermore, I believe most non-Windows systems aren't usually set up to play video clips embedded in web pages, but being set up to play Flash is much more common.\n\nSee how an actual discussion can occur when you provide suggestions instead of just whining?\n"
    author: "Jim"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: ">It's not discrimination if you differentiate based upon merit. If there's no other feasible way of providing the demo, then it's not unfair to provide it to only those that are capable of viewing Flash.\n\nYes, exactly.  You're making my point for me.  Prove that Flash is necessary, and it won't be discrimination.\n\n> That has *nothing* to do with the topic at hand.\n\nWell, perhaps I see underlying principles where you cannot.\n\n> What's unethical about Flash?\n\nThe discrimination that you just agreed about.\n\n> MPEG videos are usually *much* larger in file size and bandwidth usage than Flash files. That alone could be a deciding factor.\n\nI doubt the difference would be very much, considering the similarity and low bandwidth of content.\n\n> Furthermore, I believe most non-Windows systems aren't usually set up to play video clips embedded in web pages, but being set up to play Flash is much more common.\n\nYou're quite wrong about that.\n\n> See how an actual discussion can occur when you provide suggestions instead of just whining?\n\nI've been doing that all along.  You, on the other hand, have been making childish remarks like that one.\n\nThis discussion is pointless.  You don't seem to care to look at the principles involved, we're going round in circles, and I didn't address you with the original comment anyway.  I'm done with this.  Call me a whiner or whatever other childish remarks you like."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "\"Prove that Flash is necessary, and it won't be discrimination.\"\n\nThe video's aren't necessary in the first place, tehy are just optional bonus for those who might be interested. What if KDE offered NO videos at all? Would you then be happy? At least no-one would be \"discriminated\" then! oh no, we can't have any additional extras because someone, somewhere might not be able to use them! Hell, why do we offer screenshots? I mean, that clearly discriminate against those who surf with Links or Lynx!\n\n\"I've been doing that all along. You, on the other hand, have been making childish remarks like that one.\"\n\nYou have offered zero constructive criticism. You just said that they shouldn't use flash. But you failed to mention any alternatives."
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "\"Prove that Flash is necessary, and it won't be discrimination.\"\n\nThe video's aren't necessary in the first place, tehy are just optional bonus for those who might be interested. What if KDE offered NO videos at all? Would you then be happy? At least no-one would be \"discriminated\" then! oh no, we can't have any additional extras because someone, somewhere might not be able to use them! Hell, why do we offer screenshots? I mean, that clearly discriminate against those who surf with Links or Lynx! Why does KDE have sound-capabilities? It clearly discriminates those without sound-cards!\n\n\"I've been doing that all along. You, on the other hand, have been making childish remarks like that one.\"\n\nYou have offered zero constructive criticism. You just said that they shouldn't use flash. But you failed to mention any alternatives."
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "> Prove that Flash is necessary, and it won't be discrimination.\n\nSince you are the one accusing people of being discriminatory, the burden of proof is on you to prove that it isn't necessary; i.e. to suggest a suitable alternative.\n\n> Well, perhaps I see underlying principles where you cannot.\n\nNo, I suspect I see the \"underlying principle\" that you are on about; control by a company that can lock people out.\n\nThat isn't an underlying principle in this situation, since SWF is an open format with open-source implementations.\n\n> The discrimination that you just agreed about.\n\nThat's a dishonest thing to say.  Saying I \"just agreed\" implies that I said something that I certainly didn't say.\n\n> I doubt the difference would be very much, considering the similarity and low bandwidth of content.\n\nThere is no similarity.  VNC operates at a higher level than a simple video stream.\n\n> You're quite wrong about that.\n\nMy experience says otherwise, but fair enough, I haven't got any statistics to back that up with.  Do you?\n\n> I've been doing that all along.\n\nNo, you haven't.  That comment was in response to the first time I have seen you make an actual suggestion instead of mere criticism and abuse.\n\n> I didn't address you with the original comment anyway.\n\nThis is a public forum.\n"
    author: "Jim"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "> i.e. to suggest a suitable alternative.\n\nI have already done that.  I hope you are more capable of reading than the other person who has reacted badly to this suggestion.\n\n> That's a dishonest thing to say. Saying I \"just agreed\" implies that I said something that I certainly didn't say.\n\nAgreement comes in more forms than a direct \"I agree\".\n\n> There is no similarity. VNC operates at a higher level than a simple video stream.\n\nWrong.  Most animation formats are smart enough to only record what changes between frames.\n\n> No, you haven't. That comment was in response to the first time I have seen you make an actual suggestion instead of mere criticism and abuse.\n\nIn fact, I've been quite tolerant of the other poster's abuse, and you are becoming quite abusive yourself.  I have simply suggested a small change that would benefit everyone.  Granted, my details of the alternatives were not suggested right away, but that is only because I firstly assumed everyone would be aware of the alternatives, and secondly because you have both been so unwilling to even admit the problem that talk of solutions has been a long way off.\n\n> This is a public forum.\n\nPerhaps, but I was *replying* to a public post, not starting one.\n\nAnyway.  This has dragged on forever, and it's as silly as arguing about why Word formats are bad imho, so feel free to debate or condemn my comments amongst yourselves if you feel so inclined.  I have work to do."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-18
    body: "Um... Idle question:\n\nThe Flash plug-in is installed (at current estimates!) by 96% of the web browsing public.\n\nThis is more than any video encoder out there, perhaps excluding MPEG/MPEG-2 (although the numbers on these are by no means confirmed)!\n\nChoosing video over Flash almost certainly limits the number of people able to watch the movie. Choosing a codec with a decent compression ratio (Quicktime, WMV, RealVideo, DivX, Xvid) disenfranchises many more people who don't have the codec. Not to mention that DivX decoding consumes far more memory and has higher CPU load than rendering the Flash, this prevents people with more limited hardware from viewing.\n\nFinally, since video is far larger than VNC2SWF-created Flash files, you also limit access to those with high-bandwidth connections, which makes matters still worse.\n\nYou'll also notice that all of these formats require players, many with dubious EULA requirements. The only exception would be using OggTheora, which still has far too little market pentration to be viable.\n\nCongratulations! While berating the use of Flash as being unethical and discriminatory (< 3% excluded), you've managed to generate a solution (the use of full video), which disenfranchises around 75% of the web browsing public and worse excludes those with limited Internet connectivity and limited hardware, those to whom the costs of software purchase are the most onerous (the poor, populations of developing countries); the people who could most benefit from the use of KDE with free operating systems, such as Linux and *BSDs."
    author: "Luke Chatburn"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-18
    body: "> The Flash plug-in is installed (at current estimates!) by 96% of the web browsing public.\n\nIf you believe that, you'll believe anything.  You really imagine that only 4 percent of users:\n\n* have one of the many platforms that Flash isn't properly available for?\n* are oblivious to Flash's existence\n* are incapable of understanding the installation process\n* simply don't use websites that ever require Flash?\n* have upgraded to a recent version, rather than sticking with the buggy version that (possibly) came with their machine?\n* etc.\n\nMacromedia simply can't have that kind of market permeation, no matter what they like to tell you in their marketing blurb.\n\nNonetheless, I'm not talking about which is most common, I'm talking about which is most usable and accessible and ethical.  Big difference.\n\nBut nevermind.  This has gone on long enough.  If they want to make it accessible to people like me, they'll make a non-flash version.  If not, they won't.  I'm simply pointing out how their current choice will affect some of us, and there's no need for you to get reactionary about that."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "> What's unethical about Flash?\n\nWhat about this: I seem to remember that you need to accept their \"EULA\" which grants Makromedia \"officers\" the right to enter your home to check if you comply with their license. Yes, I know that other \"famous\" EULAs contain that as well, and also that this EULA doesn't survive one minute in an European court. But still, it reminded me too much of the GeStaPo knocking on my door, that I didn't install the flash player on my clean Linux. Unethical enough?"
    author: "Melchior FRANZ"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-18
    body: "> What's unethical about Flash? It sounds like you are speaking from a position of ignorance.\n\nOh my god!, should we repeat this in a free software weblog?. \n\nHave you heard about free software? Have you heard of [un] ethical issues involved in proprietary software? Do you know the motivations of free software? Do you know flash is a proprietary format? Do you know there are no free flash players? Have you read the flash license and restrictions? \n\nhttp://www.macromedia.com/software/flash/open/licensing/fileformat/\n\nDo you know you _cannot_ legally program/distribute a third party flash _player_, only encoders?\n\nSorry, who was in that \"position of ignorance\"?\n"
    author: "Ricardo Galli"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Is someone stopping you from creating such a demos and offering the to the KDE-folks? I'm sure they would gladly accept them. If you are not willing to do it, then you do not IMO have the right to whine if someone else does it on his/her own spare-time using a tool that you personally do not like. I mean, it's not like you paid for the demos. Someone did them for you, and they did it for free. You are not required to watch the demos, KDE will work just fine even without them.\n\nSo, do something to fix this issue or quit your whining!"
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "So far, your whole discussion has been fallacious.  I don't have to fix something myself to be able to point it out; that's a ridiculous thing to claim.  Although I'm well aware that's it's a common claim, and you probably just repeated it without thinking it through, which does happen.  Reporting bugs and requesting features is a very common way to get things done, and a perfectly valid one.  All the moreso, when it involves ethical things that need to be pointed out for the good of us all.  As it happens, I have my own project to work on, and would gladly welcome input on such issues, when it's ready for that."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "If someone does something on his free time, what right do you have to criticize him for it? I mean, if someone creates flash-movies to demonstrate new features of KDE, it's not like he's throwing stones at your windows. He's simply doing something on his free time that doesn't really affect you at all. Of course, you have the right to not watch the flash-movies, but I think it's pretty stupid to whine \"No! You shouldn't do that! You should do this instead!\". it's his time, he can use it any way he chooses to. If you are SO annoyed by the fact that he uses flash, you are completely free to make alternative videos, using the tools of your choice.\n\nYes, reporting bugs is a good thing to do. But this is not IMO a bug. Someone just decided to make videos that show off the features of KDE. And all you can do about it is whine? And besides, the developers are not REQUIRED to fix the bugs you report ;). They do this because they want to. It's not like you are required to pay for KDE.\n\nThis isn't really an \"ethical\" issue either. It might be if KDE was basing it's future on closed-source software. But they are not. Somneone is just offering (are they really? is there any confirmation to this claim?) completely voluntary thingie that is in no way required by KDE. Nothing is taken away from the users, but they are offered something extra. And they can choose to take advantage of it, or not to. It's up to them.\n\nYou wont get far by demanding that people spend their free time so that it fits your agenda. If you are so distressed by this, the constructive thing to do is to\n\na) Suggest an alternative tool (you have failed to do even this!)\nb) Do it yourself using the tool of your choice\n\nIt's not like you need a CVS-account for this. Just spend some time, create the movies and offer them to the KDE-folks. Or host them yourself. But I REALLY fail to see why you should get your panties in a bunch if someone else uses his spare time doing something that you may not personally approve. It's their choice, not yours."
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "> If someone does something on his free time, what right do you have to criticize him for it?\n\nThat has already been discussed.  Again, you're going round in circles -- to the point where I can't even be bothered reading beyond the first sentence of your comment.  The one part I just happened to look at:\n\na) Suggest an alternative tool (you have failed to do even this!)\n\nIs completely wrong, and you just replied to the post in which I did it before this reply, so it says a lot about the probable quality of the rest of your comment.\n\nWhy be so reactionary?  What are you afraid of?  That it may take 30 seconds to download a different tool, and another five minutes to learn, so that everyone benefits?  Sheesh.\n\nEnjoy your panic, I'll have no more part in it."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "\"Again, you're going round in circles -- to the point where I can't even be bothered reading beyond the first sentence of your comment.\"\n\nI guess not reading opposing comments is one way to discuss matters... \n\n\"a) Suggest an alternative tool (you have failed to do even this!)\n\nIs completely wrong, and you just replied to the post in which I did it before this reply, so it says a lot about the probable quality of the rest of your comment.\"\n\nYour suggestion was not there when I started replying. So it took quite a while before you got around to even making one suggestion on this matter.\n\n\"Why be so reactionary? What are you afraid of? That it may take 30 seconds to download a different tool, and another five minutes to learn, so that everyone benefits? Sheesh.\"\n\nSo it takes about 5.5 minutes to do? Then why don't you do it then? If this thing is so important to you, then surely you are willing to do your part?"
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "I believe that the reason to ditch xvidcap was that it did not capture the mouse cursor, or something like that... "
    author: "Henrique"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Ahh, thanks for that detail :)\n\nAre they using vnc2swf then, though?  Because there is another tool to generate standard video files from vnc, so, presumably, if any vnc can provide good enough recording, it should be possible to make something quite compatible from it?"
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "I believe so. You might search the kde-promo mailinglist archives to confirm.\n\nWhat is that other tool?"
    author: "Henrique"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "vncrec, along with transcode which will convert vncrec's output to standard video formats."
    author: "Jel"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Try reading the flashplayer license someday..."
    author: "mornfall"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-17
    body: "Who, me? What does the license have to do with this?"
    author: "Janne"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-18
    body: "Some feel Flash's license is not ethical and therefore refuse to run it on moral grounds."
    author: "Al"
  - subject: "Re: Not enough details, but good"
    date: 2005-02-18
    body: "Well, that's their choice. If they really feel so strongly about this issue, they are free to create their own demos using free tools and offer them to KDE-folks instead. But, like I already said elsewhere: these demos are 100% optional. The user doesn't really lose anything if he doesn't view them.\n\nMp3 also has restrictive license, yet just about all of us use it without ethical problems."
    author: "Janne"
  - subject: "Performance"
    date: 2005-02-17
    body: "No note on KDE performance, has it gottes slower this release or unchanged? It was slow enough last time, I definitely hope it hasn't gotten worse, KDE usually gets faster."
    author: "Al"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "The comments at OSNews note that changes in the latest GCC combined with work done for KDE 3.4 (configurable symbol visibility, if you care) has made KDE applications start up faster than they did before, if you or your distribution compile KDE in the right way."
    author: "Spy Hunter"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "but you should always think about : if you add more things it *never* can get faster. everything you add *takes* processor time.\n\nyou need to optimize to make things faster."
    author: "ch"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "KDE 3.1 to 3.2 got faster for me and so did 3.2 to 3.3. How do you explain that :p \n\nAnyway, I hope 3.4 is a lot faster too because KDE is still slower than XP here and  I do not like having second best in any category :("
    author: "Al"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "we're working on it. the new toolchain (gcc, glibc, etc), X and Qt developments  are and will continue to help us along to that end.\n\nbtw, when you say \"KDE is still slower than\", are you talking about... start up time from login? start up time of applications? operations of applications? window management? just curious. always nice to know what people perceive to be the case =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "What flags in configure do you advise to set to get maximum performance? I usually compile KDE myself  but knowing very little what the options usually do, I set things a bit by chance. I usually use \"--disable-debug --enable-pch --enable-new-ldflags\" or something like that, but don't really know if it makes any difference.\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "yes, those do make a difference. as do compiling for your arch, at least for things like the graphics routines. but even more important than the config flags is the quality of the toolchain that ships with your OS. these are getting better and kde is benefitting from it. the symbol visibility is a good example. we had to do a good amount of work to ensure our libraries were marked properly to support this, but without the support for it in the toolchain (in this case, the GNU compiler) we would never had this as an option. and it makes a good deal of difference.\n\nreally, the future of performance lies in 3 places:\n\na) the toolchain\nb) the technologies we commonly rely on (Qt, X.org, etc)\nc) our own optimization efforts\n\nall three are improving in ways we weren't seeing 3 or 4 years ago, really. and this is why each release has been getting more performant rather than less, despite the extra capabilities in KDE. KDE4 looks like it's going to continue this trend."
    author: "Aaron J. Seigo"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "hey, aaron, are you changing the default clock, by the way? the digital one looks quite a bit '80, and there are some complaints (from me, too, indeed) on osnews. a bit more modern look (styleclock is cool, but I guess the openGL dependency is a bit too much) would be nice. at least, change the default from lcd to plain or analogue..."
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "Plain clock looks so '70 :-)..."
    author: "Anonymous"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "mja, its not perfect either. but I think its (a bit) better than the digital clock. maybe just use the analoge clock? KDE has default an a huge kicker anyway, so it'l be cool.\n\nI'd prefer to split the kicker like the gnomes did, but I don't think that'll get into KDE 3.4 (if at all).\n\nbut the kicker pager is cool :D better than the normal pager (can you drag'n'drop windows using the pager, now? The kicker-pager from kde-look does it...)"
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "The fuzzy clock is very practical, fuzzy to the nearest 5 minutes.  Don't usually need any more detail than that.\nWould be nice to be able to set kicker transparent, but not the clock, though.\n\nThanks for one great release after another.\n"
    author: "Andy Marchewka"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "that  is possbile, just set a background color... the problem is more to get it transparant again, after you gave it a color once. I have no idea how to do that..."
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "set the background color to the window background. this work in 3.4 at least =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "aaah, thanx... that'll do it!"
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "for 3.4 i doubt it, due to font consistency issues across OSes.\n\nand yes, styleclock's OpenGL dependency is way over the top. and not just because not everyone has OpenGL libs, but also because not everyone has hardware accel'd OpenGL. and without that OpenGL just sucks (CPU..)\n\nin KDE 4 the current clock applet will be hitting the bitbin, however and wlil be replaced with something better. don't know what exactly yet, but the current clock just doesn't do it for me. it's the slowest of the default applets to load. it's also the biggest of the default applets."
    author: "Aaron J. Seigo"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "sounds good :)\n\nhope you can give it some love..."
    author: "superstoned"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "I use both windows XP and Linux. In windows XP Internet explorer loads in less than a second in KDE it takes around 3 seconds on first attempt after that its identical. The second slowness is when a directory has so many files in it like usr/lib it takes 4 seconds until the folder gets displays. In windows it makes no difference the windows explorer displays the files instantly. The third it is slower to load than windows XP however that is difficult to compare because in Linux there are other factors, for example on my laptop it takes only 30 seconds to load windows and I am off and working and in Linux from turning on the computer to starting work on it takes nearly 2 minutes that is very considerable difference even though I have turned most services off in Linux. \n\n\nI am using Fedora 3 and I am running KDE 3.4 beta 2. I can say that there is considerable speed improvement and its very noticeable and it looks beautiful like the new icon zooming. I think developers should be congratulated on that. Every Kde version so far has been getting faster than its predecessor and so have the other great programs in Linux that I use constantly (Inkscape, Gimp, Scribus). The only exception is Open Office that is just to slow to start up (nearly a minute that is a joke for what should be a very good programme) and that frustrates me so I don't use it I use instead Kword.\n\nKeep up the excellent work !!!"
    author: "Anonymous "
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "For the 3 seconds inital loadup, try enabling \"preload an instance on kde startup\" in performance tab under settings\". If the time displaying dirs with lots of files in is a problem, turning off previewing may help, however that might not solve it because kde (and anything *nix) has to look at the magic for every file to find out what it is, rather than just looking at the file extension."
    author: "mikeyd"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "Wouldn't caching this in file attributes help?  Are we ever going to see proper ACL and EA support in KDE?"
    author: "Jel"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "The thing is, preloaded instance forgot to clear session\ninformation (unless this behavior is intentional)"
    author: "cp"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "Agreed, comparing preload IE dll's with a not preloaded konqueror is not fair. Compare firefox start-up on both platforms instead.\n\nIIRC the last time I check the filemanager, two years ago or so, all icons were displayed immediately as blanks and then, like an after burner, the right icons were filled in. This is also how it works on windows. No idea why that is changed, might be the KIO generalization here, but I agree it's not comfortable to have to wait for a few seconds before being able to do a file operation.\n\nAnyway, I work at the office mostly on a dual boot 700MHz laptop. Currently I'm working again w/ W2k, and I find it quiet slow compared to debian's kde-3.3.2. Esp. the 'Start' menu is so annoying one has to wait on every menu for a second or so.\nBtw, this day I spent all morning to get windowsupdate working. It used to work, but now when it starts downloading/installing the updates, nothing happens (the progress bars on the download dialog stay at 0%). Although I consider myself a power user (at least on linux, where I have strace and friends), at windows I felt really helpless. Probably this installation is doomed in time .."
    author: "koos"
  - subject: "Re: Performance"
    date: 2005-02-18
    body: "Now that I think about it, the thins that I was reffering to were not so much KDE.\n\n- nasty dragging artifacts taht quickly dissapear\n- slow bootup time of OS\n- slower Firefox bootup than Windovs\n\nThough in KDE's case, I have also found some programs start up slower than they should. Like Kcalc, for example. But overall, when comparing only KDE programs, the difference in speed is not that great and mostly just at startup."
    author: "Al"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "> if you add more things it *never* can get faster\n\nthis is actually not true. and easily shown with the following thought experiment: there is a piece of software that takes 10s to complete its task. 5s of this is spent in one particular part of the code, which is sped up by a factor of 10. the software now takes 5.5s to complete its task. however, a new feature was added that takes 1s to complete. the software now takes 6.5s to complete its task. more features. faster than the last release.\n\nor. there is a piece of software that takes 10s to complete its task. a new feature it added that provides another way of accomplishing the same task in certain situations, but is 3x faster. when this new faster method is not applicable, the old method is applied. but when it is applicable, there is a savings of 6 1/3rd seconds. more things added, faster.\n\nand of course there is the issues of perceived speed. there are ways of making a computer _feel_ faster even though it isn't going any faster at all. the perception is all that matters. this can happen by reordering operations so that the software allows the user to interact with the application sooner (progressive rendering in web browsers, for example).\n\nlook at khtml as a great example. it has had features added to every release. and yet it continues to get faster. often because one of the above three concepts has come into play.\n\nit is accurate to say that adding new code without thought and not paying any attention to matters of optimization will cause a system to continually slow down. fortunately, this is not how things generally work in the KDE project.\n\npeace 'n luv, aseigo"
    author: "Aaron J. Seigo"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "What if the things you need to do can't be done?\n\nWhat if the added features permit you to do the task in less time?\n\nIt is good practice to get everything working before optimizing.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "Obviously you are no programmer.\n\nLet's take a very simple application. It holds a list of 1,000,000 names. It tries to find one particular name, so it searches through the list. This is very, very slow.\n\nNow lets add a thing: a hash table. This will take about five times as much code. It also needs more memory. But it sure is a LOT faster. Searches are almost instantanious.\n\nThis is a very simple example of how simple, clean software loses from larger, more complex software. This also hold true on a larger scale, the scale of a kernel, of KDE, of X.\n\nIt's just about being smart and it is most certainly not about removing code."
    author: "Erik Hensema"
  - subject: "Re: Performance"
    date: 2005-02-17
    body: "That's just plain silly.  Run time isn't a function of \"stuff\".  Many times optimizations require adding more code to a project."
    author: "Scott Wheeler"
  - subject: "Long live KDE"
    date: 2005-02-17
    body: "\"Long live the K Desktop Environment.\"\n\nSeems like a great release."
    author: "Kde user"
---
<a href="http://www.osnews.com/">OSNews</a> have written a <a href="http://www.osnews.com/comment.php?news_id=9737">review of KDE 3.4 Beta2</a>.  Highlights picked out include the new application welcome screens, the many improvements in KPDF, the new system ioslave and text to speech support.  The conclusion, "it looks like the developers have done a great job with this beta release".








<!--break-->
