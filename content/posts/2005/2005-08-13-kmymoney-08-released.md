---
title: "KMyMoney 0.8 Released"
date:    2005-08-13
authors:
  - "tbaumgart"
slug:    kmymoney-08-released
comments:
  - subject: "Online Banking"
    date: 2005-08-13
    body: "Really looking forward to KMyMoney 1.0 with that. :-)"
    author: "Anonymous"
  - subject: "Re: Online Banking"
    date: 2005-08-13
    body: "Well, the plugin structure of KMyMoney already supports Online Banking. There is a module which can be activated by \"./configure --enable-kbanking\". This needs the latest package of AqBanking.\nHowever, currently only OFX Direct Connect and German HBCI  are supported by AqBanking, and since I don't know where you are from I can't tell whether it would work for you or not...\n\n\nRegards\nMartin"
    author: "Martin Preuss"
  - subject: "HBCI"
    date: 2005-08-13
    body: "Without solid HBCI support this is all pretty useless for me.\nI stick with Moneyplex. It's the only non open-source app on\nmy PC."
    author: "Martin"
  - subject: "Re: HBCI"
    date: 2005-08-13
    body: "Why is there always a jerk who wants to mess up with a great KDE announcement? Those guys do not read anything, never make any contructive criticism and just spill out FUD..."
    author: "VTN"
  - subject: "Re: HBCI"
    date: 2005-08-13
    body: "If you want to know what HBCI is, see: \nhttp://www.hbci-zka.de/english/index.html\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: HBCI"
    date: 2005-08-14
    body: "Maybe instead of complaining, you could help the developers\nto support your specific HBCI format...\nor at least provide them the format and help them test it...\n\nThat would be a bit more constructive then just writting a complain.\n\n"
    author: "fp26"
  - subject: "Re: HBCI"
    date: 2005-08-14
    body: "Apparently it is part of OFX, at least according to \nhttp://linuxwiki.de/AqBanking (Sorry, it is in German but FinTS and HBCI are German.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: HBCI"
    date: 2005-08-15
    body: "Thanks for looking up HBCI in google (and confirming that the German linuxwiki made it on top of the result list), but in fact HBCI is just a different Protocol. OFX is one Protocol, and HBCI is another. The library AqBanking that you mentioned just happens to implement both of them. http://en.wikipedia.org/wiki/HBCI tries to be a useful explanation but falls a bit short of doing so. Well, HBCI is used only inside Germany, so the developers of such software mostly decided to write their documentation in German, too -- and that's what you can find on linuxwiki.\n\ncstim"
    author: "cstim"
  - subject: "Re: HBCI"
    date: 2005-08-15
    body: "What about RTFM: The comment right above your ignorant complaint says: \"There is a module which can be activated by \"./configure --enable-kbanking,\" and HBCI is supported by exactly this module. What was your trolling about again?\n\ncstim"
    author: "cstim"
  - subject: "compliments!"
    date: 2005-08-13
    body: "What a good software this is!\nEveryone will get addicted to this software after trying it :-) Either you are a student or a wannabe-manager, this software is for you.\n\nThis is not only good code, as many code nowadays is, but this is also:\n_useful_\n\n"
    author: "jumpy"
  - subject: "Re: compliments!"
    date: 2005-08-13
    body: "Exactly, I have been using it for all kind of live conversions (and amazed people on IRC/VoIP for my quick responces for queries about monies/funds we work on) of account values, not to mention when combined with kexchange running on side for currency conversions is a very nice tool... Can't wait to see how 1.0's on-line banking is gonna work out.\n\nI would actually like if kmymoney2 and kexchange would be stuffed to koffice, as I bet most offices are involved with monies/currencies all the time.\n\nNow just to have a plug-in for transferring project funds/account info between kmymoney2 and taskjuggler... Then on-line/virtual team management software (is there any free packages even existing for Linux/KDE?) for overall picture around the globe that you could zoom into down to cash account per project level (or to give a irc/voip/kcall to a team member)... That might even change quite a bit how teams working with non-Winblows would handle things.\n\nOh well, maybe yet again just one of my pipe dreams... ;-)"
    author: "Nobody"
  - subject: "Re: compliments!"
    date: 2005-08-15
    body: "Yes, agreed! I'm using it ever since 0.6x on my SuSE 9.2 box to track my private expenses. Even though i have to type in bank transactions manually due to lack of HBCI support (on the side of the bank, that is), i still benefit hugely from this software.\nI'd like to see more progress in reporting though. For example, to limit the bank transactions in a report to the most recently completed month, i have to choose \"custom\" and supply the date manually, which is a bit tedious. Better would be to have one of the smart options be something like \"current year until the last completed month\". The already announced charts will be most welcome, too.\n\nOther than that, once agein, fantastic work! Congratulations again!\n"
    author: "Robert"
  - subject: "Re: compliments!"
    date: 2005-08-15
    body: "You are invited to drop a feature request on https://sourceforge.net/tracker/?group_id=4708&atid=354708"
    author: "Thomas Baumgart"
  - subject: "Re: compliments!"
    date: 2005-08-16
    body: "Done. See here:\n\nhttps://sourceforge.net/tracker/index.php?func=detail&aid=1260311&group_id=4708&atid=354708\n\nThanks for the pointer!"
    author: "Robert"
  - subject: "I'll check this out asap, but..."
    date: 2005-08-15
    body: "I submitted a bug report for the UK translation and didn't hear anything back; the following release maintained the same bugs... not really nice.\n\nHowever, the software is outstanding and getting better with each release, so I'll stick with it :) I really wish they would hook into KCalendar to create reminders for due payments etc, so that Kontact would pick them up. Actually, this would be a great app to (optionally) integrate with Kontact."
    author: "Giacomo"
  - subject: "This just kicks butt it's so good :D"
    date: 2005-08-15
    body: "I've been through the deep dark woods of winblows and it's ntfs hell and I can say from many years in the dark I've found the Light through the branches and that is Linux and KDE. This application is just another super wonderfull design from a dedicated person bringing joy to our hearts! Keep up the excellent work!\nCheers from a californian LINUX, KDE, Kmymoney user.\nJohnny"
    author: "coffeecomet"
  - subject: "Great Fab"
    date: 2007-05-14
    body: "I was using Money on my windows box for all my banking needs. When money died I down loaded a trial of money 2007 only to find out that it would not open my back up files or the main money file. So I had a choice either fork over $50.00 dollars to microsoft for the deluxe version of problems. Or try something else.\n\nKmymoney is great I love it. Its wonderful as long as you remember to save often.\nI have been able to crash it a few times mainly my fault. But I still think you got the best money ware out there.\n\n"
    author: "DocW"
---
The <a href="http://kmymoney2.sourceforge.net/">KMyMoney</a> development team is proud to announce the availability of the <a href="http://prdownloads.sourceforge.net/kmymoney2/kmymoney2-0.8.tar.bz2?download">newest stable release</a> of KMyMoney, version 0.8. A lot of progress has been made since version 0.6 <a href="http://dot.kde.org/1087562563/">was released</a>. Existing features have been improved and many new features have been added. The following article gives you a short overview of them.




<!--break-->
<p><b>Investments</b><br>

KMyMoney now has the capability to manage basic investments such as
stocks, bonds, and mutual funds, and has the ability to retrieve price
updates from the Internet.</p>

<p><b>Reports</b><br>

Reports have finally been added (no charts yet though - that's next). The
reports are fully configurable and very powerful. Custom reports are
easily created and a link to any report can easily be placed on your
<i>financial summary</i> home page.<p>


<p><b>Multiple Currencies</b><br>

Support for more then 170 different currencies has been added. You can
even assign different currencies to different accounts and add transactions
between those accounts.</p>


<p><b>GnuCash File Converter</b><br>

A <a href="http://www.gnucash.org/">GnuCash</a> import file converter has been added which makes it easier for those GnuCash users who have been wanting to experiment with or use KMyMoney.</p>


<p><b>OFX Import</b><br>

KMyMoney now has the ability to import OFX files. OFX (Open Financial
Exchange) is an open specification for the exchange of financial data
over the Internet, allowing anyone to exchange financial data regardless
of the platform or financial software they choose to use. OFX is
supported by thousands of financial institutions and is becoming the
preferred method of exchanging financial data in several countries.</p>


<p><b>An Improved Look and Feel</b><br>

Probably the first thing that you will notice about KMyMoney 0.8
compared to 0.6 is the improved look and feel which includes a new
default icon set and other visual improvements. The new look gives
KMyMoney a much needed update and a lot friendlier feel. Colors are now
taken from your system settings which makes KMyMoney fit better with
your selected color scheme. Many of the dialogs and views have also been
improved both visually and functionally.</p>

<p><b>Encryption of your Data File through GPG</b><br>

You can now protect your financial data by using <a href="http://www.gnupg.org/">GPG</a> encryption from within KMyMoney. You are also given the option to encrypt your file with
a recovery key if you choose to, in case you loose your keyring/passphrase.</p>


<p><b>Calculator Widget when entering values</b><br>

Many people (accustomed to using one of the various commercial finance
managers), like entering values by using a 'calculator' widget. For
them, and anyone else who prefers to enter values this way, KMyMoney now
has one.</p>


<p><b>Completion of Data Within Edit Fields</b><br>

When selecting an account, category or payee KMyMoney will take the keys
you entered so far and reduce the displayed values in the drop down lists
to contain only those entries that match. This will speed up entering
transactions a lot.</p>


<p><b>Online Quotes for Stocks & Currencies</b><br>

Pull the latest price information for stocks and currency exchange rates
directly off the net from sources like Yahoo, Globe&Mail, MSN.CA or
Canadian Mutuals. You can even add your favorite source.</p>


<p><b>Support for VAT</b><br>

KMyMoney now offers support for "Value Added Tax" for those who live in
countries that use a VAT system.</p>


<p><b><i>Anonymous</i> File Format to Assist Debugging</b><br>

When issues do arise that you are unable to figure out, KMyMoney now has
the ability to <i>anonymize</i> a copy of your data file that you can then
send to one of the developers who can try to discover and fix the issue.
This is a useful feature when you need it.</p>


<p><b>Online Manual</b><br>

The KMyMoney user manual is now available online within KHelpCenter and
as a <a href="http://kmymoney2.sourceforge.net/kmymoney-user.pdf">PDF</a> file at the <a href="http://kmymoney2.sourceforge.net/">KMyMoney website</a>.</p>

 
<p><b>Plugin Structure</b><br>

KMyMoney now has a new plugin structure allowing developers to write
full-featured plugins to extend the functionality of KMyMoney. We invite
you to try it.</p>

<p>And of course, lots and lots of bug fixes.</p>

