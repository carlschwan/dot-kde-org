---
title: "KDE Commit Digest for July 22, 2005"
date:    2005-07-24
authors:
  - "dkite"
slug:    kde-commit-digest-july-22-2005
comments:
  - subject: "Statistics for the Week..."
    date: 2005-07-23
    body: "... is missing again."
    author: "Anonymous"
  - subject: "Re: Statistics for the Week..."
    date: 2005-07-23
    body: "Oh, so that's why I can read it without a horizontal scrollbar showing up in Akregator..."
    author: "David P James"
  - subject: "Re: Statistics for the Week..."
    date: 2005-07-23
    body: "From derek's blog:\n\"Next on the shopping list is an uninterruptible power supply. As rsync was rsyncing the svn repository, the power went off and on very quickly. I think I got a corrupted file system in that partition, and it will take a while to get a working repository. The digest is getting it's data from anonsvn.kde.org (don't worry, it's cached). There won't be any statistics until I get things fixed.\""
    author: "anonymous coward"
  - subject: "First results from Summer of Code"
    date: 2005-07-23
    body: "> Yolla Indria (yolla) committed a change to  in HEAD\n\n> initial version of libppt (Google SoC project)\n\nNice! Are there any updates as to whether this project is still on schedule, and how much can be done already? See\n\nhttp://developer.kde.org/summerofcode/pptimport.html"
    author: "ac"
  - subject: "eh ?"
    date: 2005-07-23
    body: "How comes the Linux Standard Base Project has declared GNOME as Standard Desktop for Linux ?\n\nhttp://www.linuxbase.org/modules.php?name=News&file=article&sid=61"
    author: "ac"
  - subject: "Re: eh ?"
    date: 2005-07-23
    body: "\"Following libraries are essential for many desktop applications. They are not currently under consideration due to lack of resources:\"\n\nreferring to Qt, among others.\n\nLack of resources is a nice way of saying lack of relevance and support in the real world.\n\nIn other words, don't worry.\n\nDerek"
    author: "Derek Kite"
  - subject: "Developer's Licences"
    date: 2005-07-24
    body: "For the slow class - I've just read the wiki on LSB, and the issues seems to be about the requirement for a developer to purchase a licence.  I did not think that to be correct.  \n\nMy understanding is that (a) if you want to develop free software Trolltech are cool, (b) if you want to develop proprietary software Trolltech want money. \n\nWhatever one might think of point (b), regarding point (a) IIRC, everyone from RMS down seems to agree that Qt meets a free software definition.\n\nAnyone care to enlighten me and if there is no need to enlighten me, to offer their explanation about why the licensing doesn't meet the LSB standard?  "
    author: "gerry"
  - subject: "Re: Developer's Licences"
    date: 2005-07-24
    body: "i don't get it... they should dis-allow the LGPL, as it is bad for free software (it allows development of proprietary software without ANY contribution to the community. i think that is bad. the QT way of licensing forces everyone to contribute, either with code or with money. you recieve. you help. period)."
    author: "superstoned"
  - subject: "Re: Developer's Licences"
    date: 2005-07-24
    body: "You misunderstand the function of the LSB.  They're an industry consortium primarily concerned with promoting a \"standard base\" for proprietary software vendors.\n\nUp to a point that's a good thing, but honestly most proprietary vendors don't really care much about which desktop libraries are installed as they care about where they're going to go when they're installed and that the ABI will stay stable.\n\nThe LSB's more-proprietary-than-the-proprietary-guys stance has cost them quite a bit in terms of community buy-in.  Roughly I'd say they have none in the OSS development community.  It's not a huge surprise that the GNU project specifically is very anti-LSB.  And that's a bit unfortunate since some of the conceptual goals aren't terrible."
    author: "Scott Wheeler"
  - subject: "Re: Developer's Licences"
    date: 2005-07-24
    body: "well, if the LSB ppl are so commercially controlled, they probably try to make Trolltech LGPL their Qt... won't work, i guess, as that'll cost trolltech most of their income. pitty they are so stubborn (the LSB ppl, not trolltech)."
    author: "superstoned"
  - subject: "Re: Developer's Licences"
    date: 2005-07-25
    body: "No! They are not commercially controlled, they live under some missconception that most commercially vendors have a problem with paying for their development tools. As said in the comment you answer to: \"most proprietary vendors don't really care much about which desktop libraries are installed as they care about where they're going to go when they're installed and that the ABI will stay stable\". Since serious business entities don't have a problem with it, they have either bought into the in the real-world-not-existing licensing problem or they are playing the politics/FUD game with the GPL/LGPL issue. "
    author: "Morty"
  - subject: "Re: Developer's Licences"
    date: 2005-07-24
    body: "Qt is free software now, as long as you don't want to sell proprietary software build on it (as we all know). Thing is I think most people think Linux is only good enough if it free (not as in beer not as in speech but as in cheapskate). For those that think so, paying for QT is a burden that would shoo developers away from Linux.\n\nMy 2 cents. \n"
    author: "Anon"
  - subject: "Re: Developer's Licences"
    date: 2005-07-24
    body: ">  My understanding is that \n>  (a) if you want to develop free software \n>      Trolltech are cool, \n>  (b) if you want to develop proprietary software \n>      Trolltech want money. \n\nThis cannot be stressed enough! it basically is a better GPL because it also (meaning additionally) allows for commercial software development (in _exactly_ the same way as it would be on a Borland/Microsoft/whatever platform)\n\ni guess this license scheme should be dubbed BGPL (better GPL), which would be the opposite of LGPL (guess what the L is for) ;)\n\nyes - ESR annoys me these days \n"
    author: "bangert"
  - subject: "Re: Developer's Licences"
    date: 2005-07-24
    body: "yeah, L stands for lesser :D"
    author: "superstoned"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "More information on this issue can be found on http://www.linuxbase.org/futures/ideas/issues/libqt/\n\n\n\n\n"
    author: "Anonymous Person"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "Because it's GPL so the LSB says it's not good. It doesn't meet their 'licensing criteria'.\n\nOf course, when wise people see that their criteria lead to absurd conculsions such as 'the/a real standard should not be th/ae real standard', they change their criteria to account for reality, they don't pretend to change reality to account for their criteria.\n\nWhich brings us to the conclusion that the LSB people are not wise :) Considering that they also lack methods of coercion (I do hope they lack them :)), they are not dangerous and can be safely ignored. \n\n "
    author: "mihnea"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: ">Because it's GPL so the LSB says it's not good. It doesn't meet their 'licensing criteria'.\n\nWhich makes me wonder what kind of kernel lsb wil use :o)"
    author: "rinse"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "The FreeBSD one of course."
    author: "ac"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "The LSB somehow changed their goals.\n\nAs far as I understand their original goal was to provide a common platform that ISVs could easily deploy their applications on.\n\nNowadays their goal is to provide a common platform that makes development as cheap as possible.\n\nSo while they initially (to my understanding) targeted the user machines, they now target the developer machines.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "All this licensing nonsense is getting tiresome.\n\nTelling somebody that a piece of software cannot be standard on GNU/Linux because it's being distributed under the GNU GPL is utterly absurd. We should no more debate this point.\n\nIt is obvious that some people with deep emotional malfunctions (obsessive-compulsive syndrome, i suspect), who for certain reasons only known to themselves and their likes hate Qt, are trying to come up with a conspiracy and kill Qt/KDE.\n\nThe point is, conspiracies don't work. Because reality is too complex for them, it's too complex for sophisticated plans. And if Adobe took them seriously, that was just a mistake on Adobe's side (of course, if many people repeat the mistake, certain regretable effects might occur, like bedirtyfying linux with primitive APIs)\n"
    author: "mihnea"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "KDE seems to be the most used open source desktop environement, KDE/Qt provide an excellent framework for developers to write applications. I don't think that it really matters what LSB/RedHat or whoever prefers, what really matters in the long run is technology. KDE/Qt IHMO offers developers an excelllent development framework and thus developers will chose KDE/Qt to develop applications and KDE/Qt applications will probably outdistance other applications in the long run because of that. And better applications means more users. Other toolkits can never outdistance KDE/Qt if they are actually technically inferior and if they make it harder to develop software using them. The only way to outdistance KDE/Qt is to create something that is technically superior and that makes large scale application development easier then with KDE/Qt. For now I don't see something like that."
    author: "Michael Thaler"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "\"..what really matters in the long run is technology.\"\n...said the marketing manager at Philips about their Video system...\n\n\"And better applications means more users.\"\n..was not exactly on the mind of the marketing manager at Sony when he was promoting VHS."
    author: "debian user"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "Porn works very well in KDE too, so I don't think it'll have the problems Video2000 had ;DDD\n\nCiao,\nRob!"
    author: "RobM"
  - subject: "Then prey tell"
    date: 2005-07-24
    body: "Why did Adobe, that has an investment in Qt (some of its MS-windows tools are now written with Qt, most notably photoshop album), has chosen GTK+ as the toolkit for their new Adobe Acrobat Reader for Linux ?\n\nThey surely didn't have to take developer licensing costs into account (having already made that investment) ? So, why GTK+ and not Qt ?\n\nIf I might hazard a guess, its because GTK+ made it into LSB and is expected to be available on LSB compatible platforms, while Qt isn't and integrating with it will potentially require more work. Unless Qt is added to LSB on the same level as Qt, we would possibly see more and more commercial applications forgo Qt in favor of GTK+."
    author: "Guss"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: "I once heard that there is only one developer working on it and that this developer simply prefered gtk. I don't know if this is true."
    author: "Michael Thaler"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: "acrobat reader is about 100 mb big onder Linux.\nSo I don't think they were interested in a lsb-compliant system, they just put everything they need into the package.\nOne of the consequences of adobes choise is that acroread does not interact well with Gnome, in dispite of the fact that they both use gtk.."
    author: "rinse"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: "So you all are basically saying that one of the largest software vendors in the world makes software development choices for one of its leading products, which are \n- based on personal preference of the developer in charge\n- inconsistent\n- stupid\n\nAnd by pure chance these lead to not choosing Qt. I somehow find it hard to believe.\n\nI think I have a better explanation: Adobe addresses a future where LSB compliant linux systems, containing libgtk+ (currently LSB still does not contain libgtk+) are the majority of Linux installations (regardless if any of these does or does not feature libqt), and in order to facilitate easy installation for user (and reduce binary size), they will have to build on GTK+.\nIn the mean time, they have to package the GTK+ binaries, but that requirement is expected to be lifted at a future date, at which point the Adobe Acrobat Reader installation will be significantly reduced in size and complexity and will interact better with the underlying desktop system (which may or may not be GNOME).\n\nAnd this is a shame."
    author: "Guss"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: "ldd /usr/bin/acroread\nnot a dynamic executable\n\nacroread is statically linked against gtk+, it does not matter if you have gtk+ installed on your system or not. Although acroread uses gtk+, it has its own toolbar and things like Navigation tabs -> Articles do not look like they are done using gtk. Acroread is definitely not written using gtk, the gtk-UI looks more like a quick&dirty hack to get rid of the old motif UI which had serious problems like a non-working mouse wheel. Maybe it is just easier to port an existing app to gtk then to Qt because Qt more or less is a full development framework and it probably does not work very well if you mix it with other UI code like the toolbar acroread uses? But anyway, acroread is definitely not the kind of application I want to see on Linux because it is quite obvious that Adobe did not care much about the UI and if it integrates with any of the major OSS desktops."
    author: "Michael Thaler"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: "> Maybe it is just easier to port an existing app to gtk then to Qt because Qt more or less is a full development framework and it probably does not work very well if you mix it with other UI code like the toolbar acroread uses?\n\nDoubt it.  See http://www.trolltech.com/products/qt/migrate/motif.html."
    author: "mart"
  - subject: "Re: Then prey tell"
    date: 2006-01-11
    body: "ldd /usr/bin/acroread\nnot a dynamic executable\n\nThis message is fully understandable as the \"acroread\" program is a bash script which calls a binary executable. On my OpenSuse 10.0 system, the binary is /usr/local/Adobe/Acrobat7.0/Reader/intellinux/bin/acroread. Inspecting it with ldd gives to me:\n\n        linux-gate.so.1 =>  (0xffffe000)\n        libBIB.so => not found\n        libACE.so => not found\n        libAGM.so => not found\n        libCoolType.so => not found\n        libAXE16SharedExpat.so => not found\n        libJP2K.so => not found\n        libResAccess.so => not found\n        libdl.so.2 => /lib/libdl.so.2 (0x4003e000)\n        libXext.so.6 => /usr/X11R6/lib/libXext.so.6 (0x40042000)\n        libX11.so.6 => /usr/X11R6/lib/libX11.so.6 (0x40050000)\n        libgtk-x11-2.0.so.0 => /opt/gnome/lib/libgtk-x11-2.0.so.0 (0x40149000)\n        libgdk-x11-2.0.so.0 => /opt/gnome/lib/libgdk-x11-2.0.so.0 (0x4043d000)\n        libatk-1.0.so.0 => /opt/gnome/lib/libatk-1.0.so.0 (0x404bf000)\n        libgdk_pixbuf-2.0.so.0 => /opt/gnome/lib/libgdk_pixbuf-2.0.so.0 (0x404d8000)\n        libm.so.6 => /lib/tls/libm.so.6 (0x404ee000)\n        libpangox-1.0.so.0 => /opt/gnome/lib/libpangox-1.0.so.0 (0x40514000)\n        libpango-1.0.so.0 => /opt/gnome/lib/libpango-1.0.so.0 (0x4051f000)\n        libgobject-2.0.so.0 => /opt/gnome/lib/libgobject-2.0.so.0 (0x40558000)\n        libgmodule-2.0.so.0 => /opt/gnome/lib/libgmodule-2.0.so.0 (0x40592000)\n        libglib-2.0.so.0 => /opt/gnome/lib/libglib-2.0.so.0 (0x40595000)\n        libpthread.so.0 => /lib/tls/libpthread.so.0 (0x4061d000)\n        libgdk_pixbuf_xlib-2.0.so.0 => /opt/gnome/lib/libgdk_pixbuf_xlib-2.0.so.0 (0x4062f000)\n        libc.so.6 => /lib/tls/libc.so.6 (0x4063e000)\n        /lib/ld-linux.so.2 (0x40000000)\n        libpangocairo-1.0.so.0 => /opt/gnome/lib/libpangocairo-1.0.so.0 (0x4075e000)\n        libcairo.so.2 => /usr/lib/libcairo.so.2 (0x40765000)\n        libfreetype.so.6 => /usr/lib/libfreetype.so.6 (0x407b2000)\n        libfontconfig.so.1 => /usr/lib/libfontconfig.so.1 (0x40820000)\n        libXrender.so.1 => /usr/X11R6/lib/libXrender.so.1 (0x40850000)\n        libpng12.so.0 => /usr/lib/libpng12.so.0 (0x40859000)\n        libz.so.1 => /lib/libz.so.1 (0x40898000)\n        libglitz.so.1 => /usr/lib/libglitz.so.1 (0x408ab000)\n        libXrandr.so.2 => /usr/X11R6/lib/libXrandr.so.2 (0x408cf000)\n        libXi.so.6 => /usr/X11R6/lib/libXi.so.6 (0x408d3000)\n        libXinerama.so.1 => /usr/X11R6/lib/libXinerama.so.1 (0x408db000)\n        libXcursor.so.1 => /usr/X11R6/lib/libXcursor.so.1 (0x408df000)\n        libXfixes.so.3 => /usr/X11R6/lib/libXfixes.so.3 (0x408e8000)\n        libpangoft2-1.0.so.0 => /opt/gnome/lib/libpangoft2-1.0.so.0 (0x408ed000)\n        libexpat.so.0 => /usr/lib/libexpat.so.0 (0x40912000)\n\n \n"
    author: "Maurizio Tomasi"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: ">And by pure chance these lead to not choosing Qt. I somehow find it hard to believe.\n\nThe fact that Qt is not a standard part of Windows did not stop them in using Qt for their photo album application.\n\nSo why should the fact that Qt is not a standard part of Linux (although afaik every respectable Linux distribution includes Qt by default) make them decide not to use it?"
    author: "rinse"
  - subject: "Re: Then prey tell"
    date: 2005-07-24
    body: "\"Why did Adobe, that has an investment in Qt (some of its MS-windows tools are now written with Qt, most notably photoshop album), has chosen GTK+ as the toolkit for their new Adobe Acrobat Reader for Linux ?\"\n\nBecause they had one developer working on it who preferred GTK, and what's more, he had absolutely no budget to go with it.\n\n\"They surely didn't have to take developer licensing costs into account (having already made that investment) ? So, why GTK+ and not Qt ?\"\n\nBecause it isn't economically viable for using Qt yet. When it is, desktop Linux will have arrived. I'm sure Adobe spent lots of money on development tools for Windows, so why they feel they should they feel they shouldn't do so for Linux?\n\n\"If I might hazard a guess, its because GTK+ made it into LSB and is expected to be available on LSB compatible platforms, while Qt isn't and integrating with it will potentially require more work.\"\n\nI doubt whether Adobe even knows the LSB exists, or even cares quite frankly. No one else does.\n\n\"Unless Qt is added to LSB on the same level as Qt, we would possibly see more and more commercial applications forgo Qt in favor of GTK+.\"\n\nThe reason why GTK is apparently chosen more for these sorts of things (not for a lot of *real* applications developed in companies out there though) is the fact that desktop Linux is not yet viable. Yes, you read that right - until Qt is more widely used and people see the need to pay for licenses, desktop Linux has not arrived."
    author: "David"
  - subject: "G.N.O.M.E gnome nerds offer medieval environment"
    date: 2005-07-24
    body: "Yes, you read that right - until Qt is more widely used and people see the need to pay for licenses, desktop Linux has not arrived.\n\n\nDesktop Linux has not yet arrived thanks to crappy gnome installed on major distros."
    author: "bb"
  - subject: "Re: G.N.O.M.E gnome nerds offer medieval environment"
    date: 2005-07-25
    body: "and that's a good thing, right? thanks to gnome, serious DE's under linux are not a monoculture. please leave setting deadlines for world domination to microsoft, and revel in the freedom of choice foss gives you. "
    author: "mark dufour"
  - subject: "Re: G.N.O.M.E gnome nerds offer medieval environme"
    date: 2005-07-25
    body: "\"Desktop Linux has not yet arrived thanks to crappy gnome installed on major distros.\"\n\nInstalled on what major distros? It certainly doesn't stop up to two-third of desktop users from using KDE."
    author: "David"
  - subject: "Re: Then prey tell"
    date: 2005-07-25
    body: "> Why [...] Adobe [...] has chosen GTK+ as the toolkit for their new Adobe Acrobat Reader for Linux ?\n\nGiven that the Acrobat Reader 7 for Linux ist a fat and slow piece of software, that's annoying to work with, I'd be really interested what would be Adobe's answer."
    author: "Carlo"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "I don't know why LSB have decided for gnome but \nKDE -- last year's leader -- has increased its dominance, growing from 44 percent to 61 percent of respondants. GNOME 27 percent last year and 21 percent this year\nhttp://desktoplinux.com/articles/AT2127420238.html\n\nincluding gnome as standard desktop is a fucked political reasons....."
    author: "ed"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "I doub some poll in a forum full of KDE geeks is accurated, in that case, remember tha in www.osnews.com GNOME was the favorite DE.\n"
    author: "DB"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "It also won at linuxquestions.org by a nice margin. And osnews.com poll was not very fair... if you remember, they closed it before many people had a chance to vote. "
    author: "Anonymous"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "<!--\ncustomer-lmm-3-177.megared.net.mx \n\"Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.7.6) Gecko/20050 405 Firefox/1.0 (Ubuntu package 1.0.2)\" \"200.66.3.177\"\n-->\nWhat im telling you, that those sites are visited by geeks, not the average user.\n<p>\nThe average user wont visit or vote in those places.\n<p>\nIf we take another stadistic, take for example distrowatch.com where ubuntu a full GNOME distro is in the number 1 position, kubuntu is to far from that.\n<p>\nPolls are not reliable, can be biazed, exploded, etc.\n<p>\nI don't believe the 60% of Linux user use KDE, I'd say a 40%.\n\n\n"
    author: "DB"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "\"What im telling you, that those sites are visited by geeks, not the average user.\"\n\nAre there average users using Linux? ;-)\n\nWhat matters anyway is that KDE is a very important DE, used by too many people to exclude it from a \"standard\". It simply won't happen, IMHO."
    author: "Anonymous"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "I think the same, but being the most used it doesn't mean that it has the be the one who will dicted the standars, it must be an important contributor, nothing else.\n\n\n"
    author: "DB"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "Just because Qt/KDE is not part of LSB, that does NOT stop distributions shipping it even if they are LSB-Compliant!\n\n\n\n\n"
    author: "Anonymous Person"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "> If we take another stadistic, take for example distrowatch.com where ubuntu a \n> full GNOME distro is in the number 1 position, kubuntu is to far from that.\n\nNot a good example since Kubuntu is not a fork of Ubuntu but part of the project.  I always wondered why they listed it seperately. \n\n\n> I don't believe the 60% of Linux user use KDE, I'd say a 40%.\n\nFunny, first you criticise polls and then you just make up numbers of your own.  How much more reliable can your estimate be? \n\n"
    author: "cm"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "\"Polls are not reliable, can be biazed, exploded, etc.\"\n\nWow, really?\n\n\"I don't believe the 60% of Linux user use KDE, I'd say a 40%.\"\n\nFunny. Based on what, exactly? Is there a poll or survey somewhere?"
    author: "David"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "<!--\ncustomer-lmm-126-189.megared.net.mx \"Mozilla/5.0 (Windows; U; Windows NT 5.1; es-ES; rv:1.7.8) Gecko/20050511 Firefox/1.0.4\" \"200.77.126.189\"\n-->\nDoes people needs to be defensive here?\n<p>\nWtf is wrong with all of you.\n<p>\nHe is just guessing you assholes.\n\n\n"
    author: "Rick"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "> assholes\n\nBzzzt.  You're out. \n\nGo away."
    author: "cm"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "And the overreaction award goes to.."
    author: "cKahl"
  - subject: "Re: eh ?"
    date: 2005-07-24
    body: "Wow. The LSB actually matter these days?! Given that most people are using KDE anyway, I doubt whether many people are going care whether their desktop is part of a standard base or not."
    author: "David"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "Is not about KDE, aren't you reading? it is about Qt, you can still use KDE but with GTK applications.\n\n"
    author: "Robert"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "I don't know what you're trying to say here... there's no KDE without Qt.\n"
    author: "cm"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "Don't confuse KDE qith Qt, one is a toolkit and the other a DE.\n\nKDE may exist in a GPL world and still survive in the comercial world because you can use GTK apps with KDE, Qt by the other hand...\n\n\n"
    author: "JB"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "> Don't confuse KDE qith Qt, one is a toolkit and the other a DE.\n \n\nI know pretty well what both KDE and Qt are.  \n\n\n\n> KDE may exist in a GPL world and still survive in the comercial world because \n> you can use GTK apps with KDE, Qt by the other hand...\n\nQt can survive in both, too, so what's your point? \n\nBut anyway, I don't *want* to use GTK apps, proprietary or not, but I'd like to see KDE ones.  I want to have apps that integrate nicely (design- and feature-wise) into KDE and make use of the great KDE features like IO slaves.\n\n"
    author: "cm"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "What affects Qt  not necesry afects KDe and what afects KDE don't necesry afects Qt.\n\nAbout the integration of applications  that's what freedesktop.org is for, it doesn't matter what DE are you using it will be integrated, that's the importance of KDE using Freedesktop standars.\n\n\n"
    author: "JB"
  - subject: "Re: eh ?"
    date: 2005-07-25
    body: "> What affects Qt not necesry afects KDe and what afects KDE don't necesry afects Qt.\n\nIt does.  KDE depends a lot on the great work from Trolltech and their sustained development effort.  I don't think it makes much sense to investigate them separately. \n\n\n> About the integration of applications that's what freedesktop.org is for, it \n> doesn't matter what DE are you using it will be integrated, that's the \n> importance of KDE using Freedesktop standars.\n\nFreedesktop is about *de-facto* standards (it puts stuff that is already widely accepted into written form).  I have little hope that there will be a VFS implementation that is acceptable to all (the IO slaves won't be accepted by GNOME because they depend on Qt).  So I don't see any such standard on the horizon.  \n\n"
    author: "cm"
  - subject: "Re: eh ?"
    date: 2005-07-26
    body: "No, fd.o is neither about standards nor de-facto standards, it's a test bed for creating and improving new technology which can be used by existing free desktop environments. The 'making standards' part is out of reach for fd.o, and promotion of existing technology at fd.o as well as an open ear for the existing free desktop environments' actual needs is rather lacking at the moment."
    author: "ac"
  - subject: "Re: eh ?"
    date: 2005-07-26
    body: "I did not talk about *making* standards.  Only you did.  \n\nI was talking about http://www.freedesktop.org/wiki/Standards . That's a list of the specifications that people working on freedesktop have written up, e.g. they recorded the way menus or icon themes are defined.  Note the remarks about the varying degrees of acceptance. \n\nYes, I know there's a software section (I guess that's the test bed you were referring to...). There's nothing there that offers the features of the IO slaves, let alone in a compatible manner.  \n"
    author: "cm"
  - subject: "Use of threads for tabs in konqueror?"
    date: 2005-07-24
    body: "Often when surfing with konqueror I middle-click on a link to open it in a new\ntab and then continue reading/scrolling the page I was on, but then, as the\npage in the new tab loads, the scrolling blocks for a short moment.\n\nThis is annoying, and I wonder if there are any plans to let each tab run in\nits own thread? That should help to improve this situation, or am I wrong?"
    author: "ac"
  - subject: "Re: Use of threads for tabs in konqueror?"
    date: 2005-07-24
    body: "first and for most tab in konqi are in different thread , the problem is with the plugins the plugin handler is blocking till it done ."
    author: "ma"
  - subject: "Re: Use of threads for tabs in konqueror?"
    date: 2005-07-25
    body: "Konqueror/khtml are in no way multi-threaded. Where did you read that nonsense, because writing multi threaded GUI in Qt3 is almost impossible, only one thread can access the Qt framework at a time.\nBlocking in khtml is mostly because the DOM tree needs re-layout or some script requires quite some CPU. Some plugins may block as well, but the ones for flash/video/java all use an external program for that, that doesn't block."
    author: "koos"
  - subject: "Re: Use of threads for tabs in konqueror?"
    date: 2005-07-27
    body: "then this is THE thing that should change... if KDE was more multi-threaded, it'd feel much faster..."
    author: "superstoned"
  - subject: "but we need LSB"
    date: 2005-07-24
    body: "i use KDE most almost all the time but i still think that lsb is the only future for linux in order not fragment like unix to a bench of incompatible OS's and to be able to get software makers porting theirs product to linux, only one port not as much ports as distros..\nso it's not just about the best and the most used.. its about the future of linux"
    author: "me"
  - subject: "Re: but we need LSB"
    date: 2005-07-24
    body: "In order for LSB to be what you claim it to be, it has to become a standard. But in order for it to become a standard, it has to formalize real, living standards. And since it is pretty much obvious that one can expect a Qt app to work in linux, not including Qt in the standard makes for a dead standard. It is not a standard for linux, it's a standard for something else.\n\nNow i don't kno whom you mean when you say that \"we need lsb\", but as long as the lsb doesn't act reasonably, it's not the linux community who needs it."
    author: "mihnea"
  - subject: "Re: but we need LSB"
    date: 2005-07-24
    body: "> so it's not just about the best and the most used.. its about the future of linux\n \nsorry for replying twice, but yes, the future of linux _is_ about the best and the most used"
    author: "mihnea"
  - subject: "Re: but we need LSB"
    date: 2005-07-24
    body: "A year ago people tried to push UserLinux as a standard for the corporate desktop, again based on gtk. In addition, UserLinux should not include qt in the base distribution at all. I suppose it completely failed or does anybody know what happened to it? I think the problem is, that these people always push what the like personanly and not what their customers like to use and that is, why they fail. On Windows, people develop using C++/Java/.Net, on MacOSX developers use Objective-C/Cocoa and the future of Linux should be based on C and gtk? How many companies will develop for Linux if they should do it with C/gtk? I really think the LSB people and others should start looking what people actually use to develop for Linux and then they should try to build their standards on this, otherwise they will just fail in the same way as UserLinux or other stuff that totally neglected reallity. "
    author: "Michael Thaler"
  - subject: "Re: but we need LSB"
    date: 2005-07-24
    body: "> How many companies will develop for Linux if they should do it with C/gtk? \n\nDepending on who you ask in the GNOME camp the reply to this will either be \"but we have language bindings\" or \"but we have Mono\"...\n\n"
    author: "cm"
  - subject: "Re: but we need LSB"
    date: 2005-07-24
    body: "\"its about the future of linux\"\n\nLSB should rather be about FreeBSD since according to their Selection Criteria at http://www.linuxbase.org/futures/criteria/ just like Qt also the Linux kernel is violating their 'License' rule as elaborated in the above link: \n\"The component should have at least one compliant implementation available under an Open Source license that also promotes a \"No Strings Attached\" environment for developers. This means that the developer would be able to develop and deploy their software however they choose using at least one standard implementation. This is interpreted to mean that at least one implementation is available under a license that meets the Open Source Definition but is *does not prohibit propriatry usage*. The rationale for this criteria is very similar to that of the LGPL.\"\n\nIt's particularly ironic in this regard that actually Qt prohibit proprietary use less (you can do that for money, otherwise its GPL) than the Linux kernel (you can only do that in userland, otherwise its GPL)."
    author: "ac"
  - subject: "[ot] KDE 4 media player?"
    date: 2005-07-25
    body: "Could someone please apprise me of the state of KDE 4 media player, and could it play all video and audio formats (including embedded media) and VCD/DVD etc.? Just eager to have a good media player which can do what too many media players are trying (kaffeine, codeine, kmplayer, kplayer, amarok, juk, etc.). thanks."
    author: "fast_rizwaan"
  - subject: "Re: [ot] KDE 4 media player?"
    date: 2005-07-25
    body: "also could we please stop coming up with \"multimedia\" players that cannot play video (how is it \"multi\"?) and image tools that cannot edit?\n\nso i have to stop amarok and open kplayer just to play a video. as for midi support in either of them..."
    author: "c"
  - subject: "Re: [ot] KDE 4 media player?"
    date: 2005-07-27
    body: "even better, can we kill ALL multimedia players? and just use music players to play music, and video players to play video? 90% of amarok's features are useless for video, as are most features a videoplayer should have. don't mix these two apps. i don't want to have to stop amarok to view a video. i want at most to hit pause (or just lower amarok's volume) and then click and watch the video in kmplayer, codeine or (as i prefer) in kaffeine.\n\nthe person that invented the 'multimediaplayer' should be burned."
    author: "superstoned"
  - subject: "Re: [ot] KDE 4 media player?"
    date: 2005-07-25
    body: "What you refer to is not a software design issue, it's a patent issue.  MPlayer with the codecs available at <a href=\"http://www.mplayerhq.hu/\">its homepage in Hungary</a> can smoothly play almost every video file my copy of WMP can after I've gotten codecs from all over.  Smoothly, I say, because I've taken great care to use hardware with driver support for hardware mixing and good video drivers to boot.  \n\nUnfortunately, those codecs aren't legal to download in many countries, and the fact that it makes life difficult for Unix users is just unfair."
    author: "D. Rollings"
  - subject: "Re: [ot] KDE 4 media player?"
    date: 2005-07-25
    body: "can you please list the countries where its illegal ?\n\nplease refer to the proper law and list the number of inhabitants.\n\n"
    author: "chris"
  - subject: "Re: [ot] KDE 4 media player?"
    date: 2005-07-25
    body: "Most I'll say, because MPlayer makes heavy use of codecs from Microsoft so redistribution of those becomes problematic according to most nations copyright laws. "
    author: "Morty"
  - subject: "Re: [ot] KDE 4 media player?"
    date: 2005-07-25
    body: "Assuming that you want those codecs for your FLOSS system, nearly all Western countries.  The problem is that there is no license agreement for such a distribution; they're targetted at software packages for Windows or the Mac.  It's shenanigans, of course."
    author: "D. Rollings"
  - subject: "Multimedia backend framework decided ?"
    date: 2005-07-25
    body: "Hello,\n\nHas kde decided on using multimedia framework ?nmm/gst etc ? since arts is not being used for kde 4 ? whats the status for multimedia on kde4 ?\n\ncheers"
    author: "pingu"
  - subject: "Re: Multimedia backend framework decided ?"
    date: 2005-07-25
    body: "There will be a simple abstracted multimedia API so that even if KDE 4 should default to a single multimedia framework exchanging it with another one or leaving such away won't be hard (the lesson learned from the current hard dependency on aRTs actually)."
    author: "ac"
---
Some highlights from <a href="http://commit-digest.org/?issue=jul222005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=jul222005&all">all in one page</a>):

<a href="http://uml.sourceforge.net/">Umbrello</a> adds a Ruby code generator.
<a href="http://edu.kde.org/kalzium/">Kalzium</a> now has a chemical equations solver.
New recurrence code for libkcal.
<a href="http://kopete.kde.org/">Kopete</a> adds support for receiving AIM buddy icons.
<a href="http://kopete.kde.org/">Kopete</a> supports Richtext formatting in Yahoo! Messages.



<!--break-->
