---
title: "aKademy 2005 Concludes on Sunday"
date:    2005-09-02
authors:
  - "mwelwarsky"
slug:    akademy-2005-concludes-sunday
comments:
  - subject: "Keep on the good work Vanessa"
    date: 2005-09-02
    body: "Hi Vanessa!\n\nWe (geeks community) have beautifull women too! Congratulations Vanessa ;)\n\n"
    author: "CJ"
  - subject: "Re: Keep on the good work Vanessa"
    date: 2005-09-03
    body: "Yup. And one day we will grow up finally and stop commenting about a woman's visual appearance instead of her other qualities. (OTOH, i remember the comment about Aaron Seigo's eyes the other day, so there's *some* balance already...)"
    author: "planetzappa"
  - subject: "Re: Keep on the good work Vanessa"
    date: 2005-09-03
    body: "I think we need more women commenting about the visual appearance of the men :)"
    author: "rinse"
  - subject: "Re: Keep on the good work Vanessa"
    date: 2005-09-05
    body: "Well, I'm not a woman.  But Antonio Larrosa is not too bad! ;-)"
    author: "Steve Bergman"
  - subject: "Nice Presentations"
    date: 2005-09-03
    body: "Really, really, really enjoying watching and listening to all the videos of the presentations. Thanks a bundle!"
    author: "Segedunum"
  - subject: "Thanks Guys!"
    date: 2005-09-04
    body: "I'd like to say a big thank you to everyone involved in organising the conference. Getting the KDE team together is like hurding cats, so I know it can't have been easy! Your hard work is much appreciated.\n"
    author: "Richard Moore"
  - subject: "Re: Thanks Guys!"
    date: 2005-09-05
    body: "Thanks. It will be really hard to top that next year, but we'll try :-)\n"
    author: "Matthias Welwarsky"
  - subject: "I'm very happy with all of you!"
    date: 2005-09-05
    body: "This year's aKadmy was the first time for me to get in touch with KDE. I can\u00b4t imagine a more interesting way of being introduced to you, wonderful people as you are, as by means of such a great event. Simply by reading about you wouldn't have brought me half of the impression that I have now. I'm impressed by your \"working spirit\" and by your ability of combining \"work\" and \"fun\". I'm very thanful for having had this great oportunity to meet you . Also I want to thank the Linux-Malaga team, that were the ones that actually called me to help in this conference. Last but not least I want to say that I'm already a great admirer of yours, I love your philosophy and I would be very happy to keep in contact with you. To inform you also that I'm open for doing some work for KDE with great pleasure, since I agree very much with what you are doing. Just contact me in the case. Nice greetings to all of you. In love, Vanessa.\n"
    author: "Vanessa"
---
The 2005 KDE conference <a href="http://conference2005.kde.org">aKademy</a> in Malaga, Spain, ends on Sunday 4th of September. Up to now the conference has been hugely successful with more than 250 visitors. The Users and Administrators Conference and the Developers and Contributors Conference have both concluded, the Coding Marathon is continuing for two more days.  A press conference was held this morning and the <a href="http://conference2005.kde.org/press-release.php">press release sums up the event so far</a>.








<!--break-->
<div style="border: thin solid grey; float: right; padding: 1ex; width: 310px;">
<a href="http://static.kdenews.org/danimo/aKademy2005_press_conference_1024.jpg"><img src="http://static.kdenews.org/danimo/aKademy2005_press_conference_300.jpg" width="300" height="225" /></a><br />
<a href="http://static.kdenews.org/danimo/aKademy2005_press_conference_1024.jpg">People left to right</a>: Vanessa Delgado, Mirko Boehm, Antonio Larrosa, Matthias Welwarsky
</div>

<p>The Beach Party on Saturday night will be the grand finale of this event before the conference participants will depart on Sunday. Until then there is a little more time left to continue improving KDE in night long sessions in the computer lab with free coffee available for everyone.</p>

<p>The press conference held by KDE e.V. and the aKademy organisers this morning informed the local media about this event, the KDE project itself and free software in general. A number of articles will appear in the national and local Spanish press in the following weeks.</p>






