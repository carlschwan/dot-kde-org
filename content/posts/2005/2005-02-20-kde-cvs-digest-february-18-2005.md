---
title: "KDE CVS-Digest for February 18, 2005"
date:    2005-02-20
authors:
  - "dkite"
slug:    kde-cvs-digest-february-18-2005
comments:
  - subject: "."
    date: 2005-02-19
    body: ">David Faure (faure) committed a change to kdebase/konqueror in HEAD\n>February 15, 2005 at 08:13:25 PM\n>\n>GUI: remove Cut/Copy/Paste buttons from the toolbars, as discussed at length on\n<kfm-devel\n\n\nBEST COMMIT EVER!\n\nNow let's head to that useless lock icon!"
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-19
    body: "we've already discussed this. it will happen in the next release. there are a pile of security tests that George Staikos has to verify against before that icon can be removed. but it's days _are_ numbered =)"
    author: "Aaron J. Seigo"
  - subject: "Re: ."
    date: 2005-02-20
    body: "thanx for the info :D"
    author: "superstoned"
  - subject: "Re: ."
    date: 2005-02-19
    body: "Hey, whatever, I use them all of the time. Looks like I'll be editing the Konq bars on first start up like I have for years."
    author: "Joe"
  - subject: "Re: ."
    date: 2005-02-19
    body: "WTF?! Why don't you use keyboard shortcuts or the right click menu?"
    author: "Al"
  - subject: "Re: ."
    date: 2005-02-19
    body: "Or even better with drag&drop and split views..."
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Why don't you use the buttons? Same question, really...."
    author: "Joe"
  - subject: "Re: ."
    date: 2005-02-20
    body: "I'll have to agree with Joe on this.. I used those when I was too lazy to use the keyboard. this is a change that I'm not overly happy to see happen. I can live without it but I would hardly consider it a highlight...."
    author: "Stephen"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Yeah.  Exactly.  All the people running around calling this the \"best change ever\" are the ones ruining the show for KDE.\n\nTalk about exaggeration."
    author: "ac"
  - subject: "Re: ."
    date: 2005-02-20
    body: "You also have the main menu and the context menu. In case you are too lazy to use the keyboard."
    author: "Christian Loose"
  - subject: "Re: ."
    date: 2005-02-19
    body: "Now for the inevitable question;\nWhat about all the other apps with these buttons in their toolbars? Kate, Kolourpaint, ... a lot of them.\n(Before anyone says c/p isn't important for file management and is for text editing: that isn't true. It's important for both.)"
    author: "Illissius"
  - subject: "Re: ."
    date: 2005-02-19
    body: "> (Before anyone says c/p isn't important for file management and is for text editing: that isn't true. It's important for both.)\n\nYes, it is true."
    author: "ac"
  - subject: "Remove those buttons!"
    date: 2005-02-20
    body: "No, those buttons are not important.\nBetter write a \"Did you know\"-tip that says that a selection of text is instantly copied to the cipboard and that the middle-mouse button pastes the selected text.\nKDE is Unix, not Windows!"
    author: "Xedsa"
  - subject: "Re: Remove those buttons!"
    date: 2005-02-20
    body: "Note, I said c/p is important, not the toolbar buttons for them."
    author: "Illissius"
  - subject: "Re: Remove those buttons!"
    date: 2005-02-20
    body: "Selecting text copies it to a different clipboard than those actions.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: ."
    date: 2005-02-19
    body: "Fark.  I can't believe the whiners won. \n\nYou're all helping to drag our beloved KDE down to the lowest common denominator with your pointless whining.\n\nI have no idea how the GNOME marketing people managed to train all of you into \"usability experts\" in such a short time, but I take off my hat to them.  GNOME are the master puppeteers of the Linux desktop market."
    author: "ac"
  - subject: "Re: ."
    date: 2005-02-19
    body: "So, according to you, a web browser should have cut/copy/paste buttons?"
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Cut/copy/paste has been removed from FILE MANAGEMENT ie KFM.\n\nThis is why I switched to KDE.  I couldn't stand no cut/copy/paste in Nautilus!  Now KDE is just becoming GNOME. :-("
    author: "ac"
  - subject: "Re: ."
    date: 2005-02-20
    body: "KDE has a \"Configure Toolbar...\" feature."
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Great.  Welcome to usability hell.  You take the most useful feature of KFM out of sight of users and tell them KDE has a \"Configure Toolbar...\" feature.\n\nGNOME has gconf, btw.  Doesn't make it suck any less."
    author: "ac"
  - subject: "Re: ."
    date: 2005-02-20
    body: "You can change toolbars with gconf? Please, stop trolling."
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Who cares, that's besides the point.  You can do pretty much anything with gconf.  It still sucks.  We are talking about usability here.  Or are you just interested in random content-less arguing?\n\nI object to KDE listening to whiners and propaganda and so-called usability experts.  I don't think it is good for the project.  What is the point of turning KDE into GNOME?\n\nWhere KDE was or could be great it is becoming merely ordinary, and worse, crippled usability-wise.  If that's what you want go use GNOME."
    author: "ac"
  - subject: "Re: ."
    date: 2005-02-20
    body: "most people never used these icons. there are numerous better ways of moving and copying files, like with the shortcuts, drag'n'drop (yep, you can keep the files over a folder, then the folder will open etc, you can drag'n'drop to other tab's, they'll open etc etc). the rmb opens a menu with a copy-to and move-to, by the way. and there are menu's with copy/paste in them. these buttons where overkill, and annoying. Reducing the amount of icons makes it (one day) possible to remove 1 toolbar, and integrate the locationbar in the one toolbar left (a la firefox).\n\nthe new 'view' icons, for local browsing, is another improvement that saves a few icons on local browsing.\n\nOf course, some people won't like this change, but most do."
    author: "superstoned"
  - subject: "Re: ."
    date: 2005-02-20
    body: "The solution will be that toolbar configuration is stored on each Konqueror Profile so that you can have an old profile with all the buttons and one for \"experimented\" users who don't need this buttons.\n\nMoreover the toolbars configuration dialog sucks a lot. For exemple, remove <fusion> from the main toolbar of Konqueror then try to had it again : impossible. There is too much toolbars configurable on this dialog and in reality only 5 can be shown. That's because a toolbar is composed of other toolbars. That's not easy to understand for users and eaven for me, user since KDE 1.0\n\nBut I am in the team who don't need this copy/paste/cut buttons :)"
    author: "Shift"
  - subject: "Re: ."
    date: 2005-02-20
    body: "\"most useful feature\"? Huh?\n\nIt's not like copy and paste support was removed from KFM. You can still use the keyboard shortcuts, or the context menu, or the regular menu. I have *never* used the toolbar buttons."
    author: "mmebane"
  - subject: "Re: ."
    date: 2005-02-20
    body: "> You take the most useful feature of KFM out of sight of users\n\nOh please.  You really think the copy & paste toolbar buttons are the most useful feature of KFM?  If you really think that, then you certainly don't bear any resemblance to normal users.\n\nIf you had said that it was an *often used* feature, then people could have a productive debate about the pros and cons.  But when you instantly revert to such blatant exaggeration, and accuse people of being whiners and spreading propoganda, it becomes impossible to do anything but write you off as somebody who is concerned about what he wants rather than what's good for normal users.\n\n"
    author: "Jim"
  - subject: "You are missing the point"
    date: 2005-02-22
    body: "Yes, KDE has a \"configure toolbar\" feature, however the point is that it's much easier to remove stuff you don't want (for streamlining or optimizing) than it is to add.\n\nAnd the flakiness of the configure toolbar feature isn't helping either.\n\n"
    author: "Roland"
  - subject: "Re: ."
    date: 2005-02-20
    body: "> Cut/copy/paste has been removed from FILE MANAGEMENT ie KFM.\n\nThey were also in konqueror web-browser profile. You haven't use konqueror in a while, have you?"
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-20
    body: "I am posting this from konqueror right now. I have not updated CVS yet, only reading log. Please stop following up with useless one-liners.\n\nOK."
    author: "ac"
  - subject: "i must love baiting trolls"
    date: 2005-02-20
    body: "The redundant buttons have been removed, the features are still there.  You should thank the KDE developers for forcing you to become more efficient at using your computer by learning the keyboard shortcuts.  Or if you still insist on being inefficient, the right mouse button menu is there, and it's closer to your mouse pointer than the old toolbar buttons.  If you really can't stand being that efficient, you can still use the edit menu with your mouse.\n\nThe rest of the world will enjoy their new, less cluttered, more relevant displays.  Baby steps to a better KDE."
    author: "Spy Hunter"
  - subject: "Re: ."
    date: 2005-02-20
    body: "> Cut/copy/paste has been removed from FILE MANAGEMENT ie KFM\n\nRead this: Cut/Copy/Paste have *not* been removed from the file management part. The *buttons* have been removed from the *toolbar*. \n\nYou can still use the main menu, the keyboard shurtcuts or the context menu to cut/copy/paste a file.\n\nPlease don't spread FUD. Thank you!"
    author: "Christian Loose"
  - subject: "Re: ."
    date: 2005-02-20
    body: "WRONG!! This is just the wrong way to go. Remove features because some \"usability\" experts believe it confuses users? Are we going the Gn*me way now? Guess what, I always enable the extra toolbar in Konqueror because I CAN'T HAVE ENOUGH ICONS in the toolbar.\n"
    author: "ac"
  - subject: "Stop your hollering!!"
    date: 2005-02-20
    body: "OMG, NO FEATURES WERE REMOVED! And it is not about confusion, it's about clutter and having a desktop you want to use. If you want to have every freaking icon there, go ahead, that's why you can edit your toolbars. But the rest of the world wants only the most often used icons in the toolbar. Do not try to impose your odd  preferences as the defaults.\n\nAlso, if GNOME does something better, copying it is not a bad thing. GNOME has far better usability then KDE, it's interface is cleaner and more consistent, it's documentation is better and this is all they focus on. Unfortunately, they have removed too many useful things in the process, they have made mistakes, but usability has improved. KDE's worst aspect is usability. KDE needs to compete, but in its own way, it is not making the same mistakes.\n\n"
    author: "Matt"
  - subject: "Re: Stop your hollering!!"
    date: 2005-02-20
    body: "I'm willing to go out on a limb and say there are users who don't know about keyboard shortcuts or right clicking (new users).  For them, those features might as well have been removed.  It's strange to me that whenever someone says he'd rather still have those buttons, someone tells him to customize his toolbar.  Umm.. someone who relies on those buttons is exactly the kind of person to have no idea how to do that.\n\nNow, in the name of consistency, are they going to disappear from other apps too?  I just don't understand what is so bad about having them there.  I've never used them, but they shout out to a new user that the functionality is there.  I don't see how their presence could confuse anyone.  Suddenly the purpose of the Back Button is no longer obvious because there is a Cut Button too?"
    author: "MamiyaOtaru"
  - subject: "Re: Stop your hollering!!"
    date: 2005-02-20
    body: "The kinds of users who don't know about keyboard shortcuts or right clicking are *also* not the kind of users who would click on a picture of scissors just to see what it would do.  They are certainly not the kind of users who need to use copy and paste for file management (and especially not \"cut\"!), when dragging icons is much simpler and easier to understand.\n\nThese are the kind of users who are scared that everything they do might break their computer and don't do anything unless somebody has already showed them that it's OK.  These are the kind of users who are extremely slow because they navigate through the application menu bar every time they want to, for example, save.  And I might point out that the items are *still* in their standard locations in edit menu, where they are easy to find even for new users because: 1. they are labeled, and 2. they have been there in every program, KDE and otherwise, since forever.  Does Mac OS X Finder have cut/copy/paste toolbar buttons?  No!  Does Windows Explorer have cut/copy/paste toolbar buttons by default?  No (not in XP)!\n\nThe presence of far too many toolbar buttons *does* confuse newbies.  My grandmother would probably faint if I tried to make her learn KDE 3.2's default Konqueror toolbars.  In addition, the extra icons add visual clutter and complexity that Konqueror already has too much of.  Removing these icons may not have a huge impact *by itself*, but it is a necessary part of the process of simpifying KDE, which is much more important for new users than providing them a \"quick\" access button on the toolbar for copy/paste (which is redundant anyway because it's slower than the right-click menu and *much* slower than keyboard shortcuts)."
    author: "Spy Hunter"
  - subject: "Re: Stop your hollering!!"
    date: 2005-02-20
    body: "> I'm willing to go out on a limb and say there are users who don't know about keyboard shortcuts or right clicking (new users).\n\nYou do know that we also have a main menu?"
    author: "ac"
  - subject: "Re: Stop your hollering!!"
    date: 2005-02-20
    body: "You are right GNOME have removed too much. But just the same I think it was the right way for them to go. Many of the features that was removed from old Gnome 1.x didn't conform to the new HIG, so in order to get a consistent GUI they had to go.As time goes by things will get added back in a more polished state.\n\nKDE would probably benefit from a similar cleanup. Not only are there many inconsistencies within KDE that needs to be fixed. The whole environment has changed since the inception of the KDE project. Back then KDE was the only vialble Unix GUI around. Now we also need to be able to interoperate with other environments like Gnome, XFCe and perhaps even MacOS-X and windows.\n\nFor a long time GUIs for Unix was an issue for highly experienced computer users such as developers and engineers. Now, Linux is on the verge of moving in on everybodys desktop and most of us think that this is a good thing. But for that to succeed focus need to shift from functionality to usability and interoperability. If we don't succed in that chances are that some other DE will be the one of choise to most people and KDE will be irrellevant."
    author: "Uno Engborg"
  - subject: "In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "Those buttons are essential for newbies. Now they are being asked to right-click or use keyboard shortcuts, neither of which is very obvious to somebody new to computers.\n\nI have first hand experience training people 65 and plus on Linux and know that this is a step backwards.\n\nThis seems to be out of that Doctorow novel where some supposed usability experts infiltrate projects till they drive them to the ground.\n\n"
    author: "ac"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "Or, you know, you could click on the edit menu, which is probably more intuitive than click on taskbar buttons, anyway."
    author: "mmebane"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "Those buttons are completely useless to newbies.  They will never click them unless they are experienced computer users who know what they mean already (then they're not newbies, are they?) or you tell them to.  Even if they do happen to click the copy button by themselves, when nothing immedately happens they will probably ignore it.\n\nIf you try to teach them \"file management your way\", and you like to use those buttons (which is apparently the case, though God only knows why), then of course you will teach them to use the buttons.  That doesn't mean that the buttons are necessary for newbies to learn Linux.  If for some reason their life depends on using copy and paste for file management (instead of the much easier to understand drag-and-drop), then they can use the edit menu, just like they would have to on OS X and Windows XP, which are the same way.  Or you can teach them the right-click menu (which is more useful in general), or the keyboard shorctuts (which is by far the fastest way to do it)."
    author: "Spy Hunter"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "Nonsense.\n\nCut and paste is a simple, basic idea that is understandable. You cut something from here, paste it there.\n\nHave you seen a newbie actually drag and drop something? That is painful. I consider it the most un-usable interface idea that has ever been dreamed up in a laboratory. Anyone I've seen doing it was shown the much easier, more accurate and less frustrating cut and paste method. My wrist gets sore whenever I do any drag and drop operations\n\nDrag and drop assumes a visible target. So you have to show the person how to open both source and target before considering doing any real work. How is that supposed to be simple?\n\nHere is another instance where what is disguised as usability is in fact imposing a way of working on people.\n\nI'm not certain whether this change applies to the file manager profile. I hope not.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "Yes, it does apply to the FM.  That's what boggles most."
    author: "ac"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "\"Have you seen a newbie actually drag and drop something? That is painful.\"\n\nI disagree, drag and drop seems perfectly natural to me. I've certainly seen beginners use drag and drop with NeXT OpenStep or Apple Cocoa with no problems. I find it harder to use in KDE because you can't drag an icon off a window without bringing the window into focus, which in turn means the window you're trying to drag it onto is no longer in focus. In Apple Cocoa you can drag an icon off a window and the target window is still at the front - that's certainly one thing that makes all the difference. UI's which are based on ubiquitous drag and drop can be much less modal, which is a good thing. \n\nThe metaphor 'cut', 'copy' and 'paste' makes no sense in the context of files. That metaphor is to do with designing pages layouts with glue pots and scraps of paper on a pasteboard. Whereas 'picking up' a file via drag and drop to move it somewhere seems more like the real world."
    author: "Richard Dale"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "You even didn't answer the question you quoted.\n\nI've seen a lot of office users in warehouses (those who manage the warehouse, enter orders etc.).\n\nThere are people who know to make a screen shot and then paste it into ... Excel. Yes, Excel.\n\nI've hardly ever seen one of those use drag'n'drop for files. Only cut/copy/paste and almost in all cases using icons in the toolbar.\n\nMany times - when I do some work there and I use the keyboard short cuts, I have been asked what buttons I had pressed. Although the short cuts are written nearby the menu entries of the edit menu. Obviously this is not used very much.\n\nClicking one of those Icons is the most simple way. Keeping the mouse button pressed while moving the mouse is a handling nightmare and IMHO another reason for serious long term injuries.\n"
    author: "Harald Henkel"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-20
    body: "Richard Dale said:\n\"I've certainly seen beginners use drag and drop with NeXT OpenStep or Apple Cocoa with no problems.\"\n\nHarald Henkel said:\n\"You even didn't answer the question you quoted.\"\n\nUmm, I thought I did..\n\nAre you talking about brain dead drag and drop support as on Windows, or drag and drop which works as in OpenStep or Mac OS X? \n\nAnd of course using properly implemented drag and drop doesn't involve injuries.\n\nIf you're right, I assume clicking on some text and dragging the mouse down to select a paragraph or whatever, will also cause major injuries to our users."
    author: "Richard Dale"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-21
    body: "I must admit, that I never worked with OpenStep or any Mac OS.\nI must also admit, that you DID answer the question. Have those beginners you saw been trained to use d'n'd on OpenStep or Cocoa ? Or did they figure that out on theirselves ?\n\nWhat's really that different between d'n'd on Windows and OpenStep or MacOSX ?\n\nThe problem with drag'n'drop is not so much a semantic one, but really a physical one, and that is basically not dependent of the way it is implemented. d'n'd consists of moving the mouse while being required to keep the mouse button pressed. Otherwise it wouldn't be d'n'd but some kind of cut'n'paste.\n\nYou don't realize after a couple of years, but like RSI the long term effect of this kind of movement is probably bigger than most people can imagine. Don't forget: It's only about 20 years that PC mice became common. And there already are a lot of people with real problems caused by mouse usage.\n\nWhat does properly implemented d'n'd do different than a bad implemented one, so that it can prevent those long term injuries, which I think it causes?\n"
    author: "Harald Henkel"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-21
    body: "\"Have those beginners you saw been trained to use d'n'd on OpenStep or Cocoa ? Or did they figure that out on theirselves ?\"\n\nI don't think you need to be trained to use drag and drop. I'm afraid I haven't really thought about how hard people find it to learn, just that I can never remember anyone having a problem.\n\n\"It's only about 20 years that PC mice became common.\"\n\nI've been using a mouse pretty much daily since 1984, and I've never had a mouse related problem. I have had back trouble that actually feels like 'sunburn' on the top of my hands from bad keyboards or bad seating arrangements though. The worst keyboard I've ever used was the original NeXT one - I used it for two weeks, and was off work for a month recovering. There was no 'spring' at the end of the key travel, and every character you hit was jarring. I've also found it's very important to sit directly in front of the screen, and make sure you don't have to twist to see it.\n\n\"What does properly implemented d'n'd do different than a bad implemented one,\"\n\nI think the problem with drag and drop on Windows, last time I used it a couple of years ago, was that it wasn't very widely implemented, so you never knew if something would work or not. KDE has pretty good drag and drop, although I seem to use it less than I do on Mac OS X - maybe that's just habit. But the fact you can't drag something off a window without bringing it to the front is pretty annoying to me."
    author: "Richard Dale"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-21
    body: "\"I don't think you need to be trained to use drag and drop. I'm afraid I haven't really thought about how hard people find it to learn, just that I can never remember anyone having a problem.\"\n\nDepends on what you mean by trained. I wouldn't say it's hard to learn. But it's not that intutive to Newbe computer users that they find just out easily.\n\nI also don't say, that RSI or similar mouse related injuries do hit every every-day-mouse-user.\nSome people smoke all their life and don't get any kind of cancer...\n\nYet, these injuries DO exist, and I think d'n'd can make it even worse compared to just clicking and moving the mouse. But RSI is not only a mouse related injury...\n\nReally, I would find it strange, if I could drag something off a window, which is not in the front. I mean, to drag something means, to click it first, and a click into a window should bring it to front. Breaking this for d'n'd would seem another usability issue to me. But maybe it's only habit... or ignorance (on my side).\n\nThere might still be some Windows programs out there, which do not support d'n'd at all. But this I think doesn't have much to do with the basic problems of this method.\n"
    author: "Harald Henkel"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-22
    body: "> I mean, to drag something means, to click it first, and a click into a\n> window should bring it to front. Breaking this for d'n'd would seem another\n> usability issue to me. But maybe it's only habit... or ignorance (on my side).\nahem. you don't click (mouse-down % up) to initiate a dnd action - you press-hold the button (mouse-down) and drag...\n(just figured out that I cannot drag text into this edit field with Konqi, what a shame....)"
    author: "thatguiser"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-21
    body: "Actually, you can drag something off of a window without bringing it to the front.\n\nTo do this go to \n   Control Center(kcontrol) ->\n      Desktop ->\n          Window Behaviour.\n\nThere are a few combinations that allow this to work properly, I personally use \"Focus Follows Mouse\" with all the checkboxes in the Focus group unchecked.\n\nMatt"
    author: "Matt"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-21
    body: "I don't want 'focus follows mouse'. If I'm dragging something off a window I just want to leave the window heirarchy as it was - I expect the target of the drag operation to be left unchanged, apart from responding to having something dropped onto it. Focus follows mouse is not a solution, it is a workround to some problem with X windows I think."
    author: "Richard Dale"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2006-09-11
    body: "I think the problem here is difference is culture. I am a Mac user and I love the drap&drop on Mac OS X. I think Mac users are using it because it works well. But Windows and KDE users might not used it very often if it is not well supported.\n\nBeing able to drag & drop from a window without bring it to front would be great in KDE. It is one of the thing that I miss from the Mac.\n\nClicking on a window must bring that window to front. With Drag & Drop this not an exception. This is because clicking means pressing the mouse button and release the mouse immediately while staying at the same location.\n\nWhen you drag and drop, you press the mouse button, you hold that mouse button, you stay in place a few milliseconds, you move the mouse to the destination and you release the mouse button. The 'stay in place a few milliseconds' is necessary because otherwise when you move the mouse directly is to make a selection which as slightly mouse gesture.\n\n\nMac OS X as it right. I hope KDE can learn from it."
    author: "Pierre Thibault"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-21
    body: "Shame on you. The Usability Police have spoken. Do as you are told and everyone will be happy. In the future the totality of the KDE interface will consist of a single large \"do it\" button, and it will mark the New Millenium of Utter Fulfillment."
    author: "Brandybuck"
  - subject: "Re: In the name of usability you are killing KDE"
    date: 2005-02-22
    body: "\"Now they are being asked to right-click or use keyboard shortcuts, neither of which is very obvious to somebody new to computers.\"\n\nthey can use keyboard-shortcuts. Or they can use context-menu. Or they can use the menubar. Or they can drag and drop. I think newbies will do just fine. And besides, why do you think that clicking a button (among several other buttons I might add) in the toolbar is easier than right-clicking and choosing the action from there?\n\nKeyboard-shortcuts will do fine as well. Espesially since they are used consistently across the desktop. Same key-combo cuts and pastes across wide variety of apps.\n\nAnd, like it or not: very very few people are \"new to computers\". And if they are, they shouldn't have any problems using drag and drop, menubar, shortcuts or context-menu. Toolbar-buttons are NOT any simpler than those are. And if they have never even used computers, how will they know to miss the buttons in the toolbar ;)? They will make do with whatever is offered to them.\n\n\"I have first hand experience training people 65 and plus on Linux and know that this is a step backwards.\"\n\nAnd I have first-hand experience with people who were at a loss with Konqueror, because it overwhelmed them with buttons. So what's your point?\n\nHell, if I had my way, Konqueror would only have the browsing-buttons (up, back, forward, home, stop)! Luckily for you, I'm not the one who decides these things ;)."
    author: "Janne"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Heh, isn't it funny? I've been customising the toolbar buttons for ages to suit my needs, but I always kept these three as usable. And actually when they include text below (as I think it should be by default for users' best comprehension), they take the least space of all."
    author: "uga"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Couldn't agree more. It is nice to see that things change to the better in applications that people actually use on a dayly basis.\n\nToo much effort have been spent in the past to get the perfect kcontol. Sure, at each attempt, it have improved significantly, but kcontrol is something that users use quite seldom. They usuallay configure their system once and that's it. When new versions of KDE comes out they usually keep old settings for most of the things.\n\nNow its time to focus on applications that people use frequently, if we could get people to save five minute a day on better userinterfaces in e.g. konquerer or kontact we would achieve much more than if configuration takes five minutes less.\n\nGiving less frequently used functions a less visible position in the GUI or as in this case removing unfrequently ways to do common things is a good start.\n\nWe should not be afraid to look at other DEs such as MacOS, Gnome or even windows to see what they do well, and try to make an improved version of it that fits in the KDE environment.\n\nAn example of things that could be borrowed from Gnome is the dual top and bottom panels. The screen edges are the provides the largest targets on the screen to hit by the mouse. why not make as much use of them as possible.\nEven the Gnome \"Program\", \"Action\" and \"Places\" menu is better than the current single K-menu as the menus gets shorter and more focused. We see the same idea in the double start menu of windows XP, but in my oppinion Gnome has implemented it better. The question is can we make it even better?\n\nAnother thing that would make life easier for normal users, would be if directories almost never enter, or at least should have no reason to enter, was\nhidden by default in konquerer unless they are logged in as root. Good candidates for hiding would be:\n/etc, /bin/, /lib, /boot, /selinux, /lost+found, /root, /usr/bin, /usr/lib and perhaps some more.\nThe files should also be hidden in file dialogs, exept for dialogs for creation of application starters that need to be able to reach all directories that contains executable files.\n\nThis behaviour should be configurable perhaps by adding a .hidden file or something. That .hidden file should decide what directoris that can be seen on a per user and group basis.\n\n\n \n\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-20
    body: ">Another thing that would make life easier for normal users, would be if\n>directories almost never enter, or at least should have no reason to enter,\n>was hidden by default in konquerer.\n>This behaviour should be configurable\nWhat are you talking about, this is already the case. When you open Konqueror in filemanger mode it opens in your home dir (/home/yourusername), already effectively hiding the directories you talk about. To get to those dir you have to deliberately click the Up icon twice or deliberately select Root Folder in the sidebar. \"Normal users\" will never encounter those directories unless they for some reason take actions to get to them. And in those cases hiding of directories behind a config option only makes whatever the users want too do harder. Actually ending up making it much worse for your \"Normal users\". I'll even give you a example, one of the most frequent files referenced when doing genereal support/troubleshooting is /etc/fstab. Hiding it make little sense.\n\nIn this case there are absolutely no reason to copy windows, besides the reason you have this functionality in windows are not to make it less confusing for users. The functionality was added to make it harder for users do destroy the system by deleting systemfiles from the windows directory. A non issue on *nix, since users don't have write permissions in the system and binary dirs. "
    author: "Morty"
  - subject: "Re: ."
    date: 2005-02-20
    body: "So, if hiding setting in config options make things harder, why have kcontrol in the first place. People could just go to the appropriate file /etc/ and make whatever changes they need assuming they have the correct permissions.\n\nI think that the problem is that we have different definitions of \"normal\" users. In my world, normal users are the ones who use a Xerox machine to copy a discette. They haven't a clue what /etc/fstab is, and even less on how to use it for diagnostics. They don't trouble shoot things. If something goes wrong, they call support. The support people should of course be allowed to see all directories just like they do today. \n\nAllowing ordinary users to trouble shoot things could actually prove costly, as it means that a lot of your employees will first try to fix things themselves instead of calling the sysadmin that could fix it for all users. \n\nYour assumption that users always stay in their home directory is also somewhat flawed. In many organizations there are shared resorces that may well reside outside the users home directorise. And even if there are not ordinary users may want to navigate to /tmp to create some temporary file. And I can assure you that a user that only have to choose from /tmp, /home, will have a much easier task finding that /tmp folder than the one having /bin/, /boot, /etc/, /lost+found, /proc, /dev,  /selinux, /sys, /sbin, and 10 or so more folders to choose from.\n\nMy basic idea is that the user should be able to see should as much as possible relate to their working world not to the computer system. If the user needs something in /usr/bin it should have an application starter and be found from some programs:/// folder or the K-menu. If there are things that the user needs to do in /etc that is a sign that the kcontrol settings arn't good enough net an indication that normal users need to see that directory.\n\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-20
    body: ">if hiding setting in config options make things harder, why have kcontrol in the first place.\nNonsens, in this case a it's about a option who does not make sense since the system in it's design already provides it. It has nothing to do with kcontrol.\n\nYou are right on one account, we have different definitions of \"normal\" users. You deffine \"normal\" users as complete computer illiterate. In my observations they are rather a lot more capable, since they actally use the computer to accomplice some tasks. And they are capable to follow instructions, verbal or written.\n\n>Allowing ordinary users to trouble shoot things could actually prove costly.\nAgain, nonsens. Since the user don't have permissions to do anything to the system. On the other hand he would be able to do things like change the desktop cdrom icon's path from /mnt/cdrom to /mnt/cdrom1 with the information from /etc/fstab. (with or without help from suport).\n\n> Your assumption that users always stay in their home directory is also somewhat flawed.\n>In many organizations there are shared resources that may well reside outside the users home directories.\nEven more nonsens. If you have \"normal\" users who are like you describe them, they need one or more admins. If the admins are competent enough to set up *nix boxes, and with shared resources. They are in the case of users having the problems you describe, also competent enough to make those available from the users home directory removing the \"problem\". And if the user need still more babysitting you can restrict them to their homedir with Kiosktool.\n\nUsing imaginary user scenarios with a marginal user class are rather useless when discussing usability. Afterall the goal in usability are to make the most number of persons able to get their tasks done as fast and easy as possible. Not to make the system idiot proof. "
    author: "Morty"
  - subject: "Re: ."
    date: 2005-02-20
    body: ">>if hiding setting in config options make things harder, why have kcontrol in the first place.\n>Nonsens, in this case a it's about a option who does not make sense since the system in it's design already provides it. It has nothing to do with kcontrol.\n\n\nThe system, allready provides a lot of things that we allready manage in kcontrol. We use kcontrol to have one place to manage and control various settings. Having more places to look in will not help users. It is also very hard to provide help to users if you admin the filesystem way.\n\n\n>You are right on one account, we have different definitions of \"normal\" users. You deffine \"normal\" users as complete computer illiterate. In my observations they are rather a lot more capable, since they actally use the computer to accomplice some tasks. And they are capable to follow instructions, verbal or written.\n\n\nEven if they can follow instructions, written or not. Who will pay for it? \nAssume you have 100 users. The sysadmin will do some task in one minute, it will take him 10 minutes to write the instructions in a way that his moderately experienced users can undurstand it. Each of the users will spend 10 to 15 minutes to read and understand these instructions. Some will succeed in performing the tasks directly, some will fail and call the sysadmin. Now a task that would have take one minute have taken at least 110, probably more minutes.\nThe time is most likely not billable. The way of administration you advocate is so windows.\n\n>>Allowing ordinary users to trouble shoot things could actually prove costly.\nAgain, nonsens. Since the user don't have permissions to do anything to the system. On the other hand he would be able to do things like change the desktop cdrom icon's path from /mnt/cdrom to /mnt/cdrom1 with the information from /etc/fstab. (with or without help from suport).\n\nA user should not have to manually scan the file systems for system icons. The dialog to change icons should directly provide some iconselector, not a general file selector. And if it is not system icons, they would be in some place where the user can access them even with hidden directories the way I suggested.\n\n\n>>In many organizations there are shared resources that may well reside outside the users home directories.\n>Even more nonsens. If you have \"normal\" users who are like you describe them, they need one or more admins. If the admins are competent enough to set up *nix boxes, and with shared resources. They are in the case of users having the problems you describe, also competent enough to make those available from the users home directory removing the \"problem\". And if the user need still more babysitting you can restrict them to their homedir with Kiosktool.\n\nSure, the sysadmin can have whatever resources appear within the home directory of each user. However, this sometimes pose a problem. How is the user to know that the resource is not entirely under his control? By putting the shared resouce somewhere else in the file system this becomes more clear.\n\n\n>Afterall the goal in usability are to make the most number of persons able to get their tasks done as fast and easy as possible. Not to make the system idiot proof.\n\nExactly. this is why we shouldn't have things that gets in our way. Its not about making the system fool proof. A user with a little skills could still fire up cate and look /etc/fstab if he wants to. But the need for this appears extremely rarely, so it doesn't matter if they are slightly more difficult if it makes everyday use easier. Reducing the number of available chosies in file dialogs does that. \n\nThe user should have all the information he needs when he needs it and only when he needs it. This is the same principles as you use in design of cockpits in modern aircrafts its not some kind of attempt to stupidyfy the user. Its about making more of the users intellectual capacities available to his work tasks. From what I understand most users of MacOS-X is quite satisfied with this behavior, there is no reason to believe that the situation would be much different for KDE users."
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-21
    body: ">We use kcontrol to have one place to manage and control various settings.\nAnd that's correct, but stop pretending I am arguing against kcontroll. What I am arguing about is adding an unnecessary configuration for something the design of the system already provides adequately. \n\n>Even if they can follow instructions, written or not. Who will pay for it?\n>The way of administration you advocate is so windows.\nAgain you take what I say out of context, I'm not saying you should make the users do the administrators job. I'm pointing out to you that in any normal corporate setting the \"normal\" users are not complete idiots, like you argue. A company with your kind of user would need a it-staffer for every 4 user or so. \n\n>By putting the shared resource somewhere else in the file system this becomes more clear.\nOr perhaps naming the directory: This_Directory_is_shared_by_whole_company. Please stop dreaming up nonexisting problems.\n\n>this is why we shouldn't have things that gets in our way.\n>But the need for this appears extremely rarely, so it doesn't matter if they\n>are slightly more difficult if it makes everyday use easier.\n>Reducing the number of available chosies in file dialogs does that. \nFrom a usability perspective those two first statements together are pure nonsens and they are contradictory. The third statement are by itself also non essential nonsens, it only marks you as a member of \"the too cluttered school of removal\". \n\nInstead of throwing nonsensical statements around, I'll argue by example (using today's popular topic). Take a everyday task like browsing this site, the fact is it makes no difference on my usability experience having 6, 12 or 18 icons in my Konqueror toolbar. It will have no impact of my understanding or speed which i read the pages(usage). Fewer icons may perhaps look better, but it does not impact on my use. Its only fluff. It may confuse a newbie slightly for for a few minutes, but since (based on estimates after watching real users) 95% or more of browsing are done using links/bookmarks/locationbar/back button they will really soon have learned to ignore the other 5, 11 or 17 icons. On the other hand, I once wanted to print out the content of a frame, not the whole page. Without the print frame icon I had probably used a manual cut/paste solution, looking for the function had never occurred to me if I didn't know of it's existence. And that's actual usability in practice.\n "
    author: "Morty"
  - subject: "Re: ."
    date: 2005-02-21
    body: "Well, what can I say.\n\nI could recomend some books on usability, but you would probably not read them, so whats the point.\n\nAnd yes, I do belong to the \"too cluttered school of removal\" that think less is more, and I am proud of it. \n\nUnix have been around for over 30 years, and the first time it gets any significant marketshare on everyday users desktops, it does so in a non cluttered form (MacOS-X). That should tell you something on what way to go.\n\nIf not the MacOS is example enough for you, look at Gnome. A couple of years ago KDE was the only boy in town to count on. It had a market penetration in the free desktop of about 50%. Today that market is schrinking rapidly in favor of Gnome. You almost can't open a webpage without being told how wonderful Ubuntuu, JDS or some other Gnome based Linux distro is.\n\nWho in their right mind would replace a system where things like kio-slaves that just works is replaced by a non functional gnome-vfs if there wasn't some reason?. But still it happens. I tell you. The reason is simplicity and elegans.\n\n"
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-21
    body: "Graphical user interfaces have been around for over 30 years, and the first time it gets any significant marketshare on everyday users desktops, it does so with MS Windows. That should tell you something on what way to go.\n\nNotice how funny your argument reads? :-)\n\n> A couple of years ago KDE was the only boy in town to count on. It had a market penetration in the free desktop of about 50%.\n\nI still have to see numbers which claim that KDE has now less than 50%.\n\n> Today that market is schrinking rapidly in favor of Gnome. \n\nReference?\n\n> You almost can't open a webpage without being told how wonderful Ubuntuu, JDS or some other Gnome based Linux distro is.\n\nYou're reading the wrong webpages or only those writing about the latest hype (JDS!? Get real). And IMO people are more excited about Ubuntu (only three 'u') because \"it tries to fix Debian\" rathen than because it has GNOME as (up to now sole) desktop environment.\n\n> Who in their right mind would replace a system where things like kio-slaves that just works is replaced by a non functional gnome-vfs if there wasn't some reason?\n\nNobody?"
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-21
    body: ">Notice how funny your argument reads? :-)\n\nOops, that certainly didn't come out right. A little too late in the evning I think. \n\nWhat I ment to do, was to draw your attention to Mac OS-X as the first resonably popular desktop unix system (have no idea why MS-windows slipped in). And by the way unix GUIs  have only been around since mid eighties.\n\n\n>> Today that market is schrinking rapidly in favor of Gnome.\n\n>Reference?\n\nWell, I have a consulting firm, I only have to look at what my customers use. \nSorry to have to inform you, this crave for simplicity and the shift to Gnome is a very clear trend. If KDE doesn't join in, it will be left behind, and others will decide the looks of the future free desktop. \n\nYou could argue that this is of no consequence, since KDE is GPL and as such it will never go away as long as there are users. Unfortunately, it isn't all that simple. The problem is that commersial software that get ported to Linux, FreeBSD or the soon to be free Solaris will use the dominant DE and such apps will stick out like a sore thumb when run on our favorite DE if it is built for Gnome. This will further enforce the trend of switching to Gnome.\n\n\n>> Who in their right mind would replace a system where things like kio-slaves that just works is replaced by a non functional gnome-vfs if there wasn't some reason?\n\n>Nobody?\n\nSigh, I only wish... \nEvidently usability is nowdays valued higher than technical perfection and that is not a good sign for KDE.\n\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-21
    body: "> Well, I have a consulting firm, I only have to look at what my customers use. \n\nOk, so nothing representative. Those things differ very much from country to country."
    author: "Anonymous"
  - subject: "oh, comeon.."
    date: 2005-02-22
    body: "stop being a troll, please. it's guys like you I hate discussing about anything most, because they take every argument, flip it in your mouth and pluck it into pieces, argue about the validity of fictional examples or exact source of some explaining figures. How about this: it is not about what you think it is."
    author: "thatguiser"
  - subject: "Re: ."
    date: 2005-02-21
    body: ">I could recomend some books on usability\nI'd suggest you go read some yourself, since usability is a lot more than throwing out unsupported statements of clutter and basing all arguments on a made up type of users. It has to be based on the real world, and actual usage patterns not the chanting of \"less is more\". As an example take a look at, http://www.openusability.org/reports/get_file.php?group_id=44&repid=33  that's professional usability. \n\nI'll suggest you read this quote by Ellen Reitmayr until you actually understands the real extent of usability. \"would be more easy for unexperienced users, but come up with many disadvantages for the others.\"(on why a simplification should not be done).\n\nAnd your MacOS \"example\" is not very convincing either btw, don't you think over 20 years of building their customer base has something to with marketshare. Their strongest point when marketing are still that they are Mac, not the Unix part of it. \n\nAs for your so called rapidly shrinking in favor of Gnome, that's also an usuported statement. In fact all statistic I have seen lately still puts KDE about 50%, as it has done the last 3-4 years. And the Ubuntuu hype are similar to the Xandros and Linspire generated hype for KDE, we saw a few years back."
    author: "Morty"
  - subject: "Re: ."
    date: 2005-02-21
    body: ">I'll suggest you read this quote by Ellen Reitmayr until you actually understands the real extent of usability. \"would be more easy for unexperienced users, but come up with many disadvantages for the others.\"(on why a simplification should not be done).\n\n\nI have no problem agreeing with that. However my suggestion have little to do with the advantages for unexperienced users contra disadvantages for more experienced ones. It would be an advantage to all users. \n\nTo have to deal with information you don't need is not good for any user. Not newbies, not experienced ones.  E.g its a good thing if your car tells you its time for service, but its not a good thing if it insists on doing so when you are close to colliding with something and have to have your mental focus elsewhere.\n\nI have worked with various Unix systems for almost 20 years, I still don't want to see things I only need for system administration when I do tasks that is  not related to system administration. Just like I don't want to see grep, awk, and other apps that is not suitable to run in a graphical context the like in my K-menu or even when I navigate my files through konqueror. If I want/need them I start a terminal window.\n\n\n\n>And your MacOS \"example\" is not very convincing either btw, don't you think over 20 years of building their customer base has something to with marketshare. Their strongest point when marketing are still that they are Mac, not the Unix part of it.\n\nI think you are on to something. You are right, Apple doesn't have success with  the Mac because it uses unix. It is because it enables their users to get their work done. The unix part is only there to provide a stable platform.\nThey don't give a darn about underlying unix file structures. they present the user with a GUI that works. They don't present the user with directories which contents can be better handled in elsewhere or with double clickalble applications that doesn't make sense in a graphical context. This is what makes MacOS special, and this that make people prefer it over various other DEs.\nYou have to remember that Sun, HP, IBM and others have also tried to get their share of the desktop in these 20 years using a more system centric approach, and they didn't have the same success.\n\n\n>As for your so called rapidly shrinking in favor of Gnome, that's also an usuported statement. In fact all statistic I have seen lately still puts KDE about 50%, as it has done the last 3-4 years. And the Ubuntuu hype are similar to the Xandros and Linspire generated hype for KDE, we saw a few years back.\n\n\nCan only hope you are right. It would be a big loss if KDE disappeared into the realm of the irrellevant. KDE holds far too much technical excelence to diserve that. "
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-21
    body: "> You are right, Apple doesn't have success with the Mac because it uses unix.\n\nFunny that you put \"Apple\", \"Mac\" and \"success\" in the same sentence:\n\n\"Apple Computer's worldwide market share fell to 1.8% in the third quarter of this year [2004] from 2.1%, and dropped to 3.2% from 3.6% in the U.S., according to figures from research company Gartner. The numbers also showed dramatic declines in the quarter-to-quarter growth rate of Macs sold while Apple's Windows-based competitors saw double digit increases in the U.S and an almost 10% rise worldwide.\"\n\n(http://www.macobserver.com/article/2004/10/29.6.shtml)"
    author: "Christian Loose"
  - subject: "Re: ."
    date: 2005-02-21
    body: "Success can be measured in other ways than market share. When was the last time you met an unsatisfied Mac user."
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-22
    body: "\"Sure, the sysadmin can have whatever resources appear within the home directory of each user. However, this sometimes pose a problem. How is the user to know that the resource is not entirely under his control? By putting the shared resouce somewhere else in the file system this becomes more clear.\"\n\nArrgh. I can't believe it.\n\nYou suggest a usability-nightmare (\"hiding\" stuff in the filemanager which runs completely against the purpose of the filemanager in the first place) for everybody to suit a very, very specialized case (the case in which some administrator is too stupid to put a symlink in the user's home directories or doesn't put a symlink because that would \"confuse\" users... Sheesh. The confusion argument has become a catch-all reason for everything.)\n\nGoddammit, it's a file manager. It's purpose is to manage files.\n\n\n"
    author: "Roland"
  - subject: "Re: ."
    date: 2005-02-21
    body: "> In my world, normal users are the ones who use a Xerox machine to copy a discette.\n\nAh, vintage memories."
    author: "Anonymous"
  - subject: "Re: ."
    date: 2005-02-21
    body: "It's also a total lie and fabrication.\n\nIf this is the kind of fictitious user you are targetting then don't be surprised when your userbase is just as fictitious."
    author: "ac"
  - subject: "Re: ."
    date: 2005-02-20
    body: "\"An example of things that could be borrowed from Gnome is the dual top and bottom panels.\"\n\nOh please God, no.\n\nMy laptop has a 1024x768 screen. Many people use this resolution or lower. Have you considered how much of the useable screen is going to be used up by a top panel, a lower panel, a window title, a menu line, one or two toolbar lines, maybe a row of tabs, and a status line ? You'll be left with a postbox-sized hole in the middle of the screen.\n\nI thought the idea was to try and NOT overload new users with too much information.\n\nTry using Gnome on a 1024x768 or 800x600 display (not TOO uncommon on laptops). A lot of your screen is used up by its overly-thick toolbars. This is one thing KDE does much better - at least the toolbars CAN be made thinner. Top and bottom panels visible by default will just make things worse.\n\nAs a thought, why not size toolbars, panels, etc, according to the screen resolution set (i.e. try and take up a (more-or-less) consistent proportion of the screen) ?\n"
    author: "anon"
  - subject: "Re: ."
    date: 2005-02-20
    body: "Actually, I'm writing this on a laptop with 1024x768 with a top and a bottom kicker panel.\n\nToday the standard kicker panel is two rows high. Splitting these two rows into two separate panels doesn't make it use more screen space, but they will be easier to reach. \n\nIt will also be esier to utilize auto hiding as you may want to have some of your panel applets auto hidden and some other always visible. So in this respect I think the situation would be improved rather than worsened.\n\n\nYour idea of scaling the toolbars according to screen size is very good, it would not only be a good on small screens but even more so on large screen resolution where controls have a tendency to get too small. I think the keyworkd here is Cairo."
    author: "Uno Engborg"
  - subject: "Re: ."
    date: 2005-02-21
    body: "I wonder how many people use kicker placed on the left or right. I do, but I have the feeling that it's not too popular, having seen the system tray broken in 3.4 Beta in vertical mode for quite some time.\n\nVertical space is usually more valuable - web pages, A4/legal pdf files,\njust about any kind of text file. The only thing I can think of which benefits from a wider screen are movies.\n\nStill kicker isn't really good to use in vertical mode. You can't make it very wide without wasting a lot of space for the K-menu icon and other big kicker icons - but this is both necessary and possible in vertical mode without hurting much, since the right hand side of the screen is underoccupied rather often, while I rarely see windows without a vertical scrollbar.\nIt would also be helpful if kicker could display window names in two rows in vertical mode. If kicker could be made work well in vertical mode, then we had plenty of space to waste."
    author: "uddw"
  - subject: "Re: ."
    date: 2005-02-21
    body: "About the hidding of directories thing, I totally disagree with that solution. That's just a work-around for the nightmare that the Linux distros' directory tree is. Following the way MacOSX went is the way to go IMO -- grouping and naming everything sanely. Some examples:\n/home -> /Users\n/usr -> /System\n/lib -> /Libraries\n/mnt -> /Devices\n\n By the way, something cool would be an hidden file (ie. .folder) in all those folders to allow internationalization and setting an icon for the folder."
    author: "blacksheep"
  - subject: "to the defenders of the \"holy buttons\" (ac... )"
    date: 2005-02-22
    body: "You people are freaking out over nothing. \n\n\"Trained ?\"\n\nTimes are advancing mostly every kid nowdays own a computer or have easy access to one, and wen you are young belive me youll take 5 minutes to learn how to drag and drop properly, use rmb, and keyboard shortcuts in that time.\n\nAbout the elderly folks or persons that never had any kind of contact with a computer and might have troubles learning. Thats the only ones that I can call newbs as its been labeled. Those are the kind of persons that dont actualy seek out how to use a computer, and if they must use it they will probaly get help out of it...\n\nAnd if they need help then I belive it wount be that hard to find someone to teach them how to use the rmb to copy and paste files properly.\n\nSo all those raging posts about the holly buttons on konqueror toolbar are pretty much pointless imho.\n\nWhat kde should focus is on having a clean apealing user interface, thats what will make tomorrows users, that are seeking an alternative to windows and have some computer knowlage, move to kde (why do you think everyone loves screenshots ? ).\n\n- Why do you think XP change its look so much ? why did they remove the clutter betwen versions ? (because theyr evil... yeah)\n- Why MacosX then (theyr creeps, ms bought them)\n- Gnome... (yeah right, the propaganda machine)\n\nAll I can say is that Im a geek that started with kde 1.1 and I still use kde. Then I joined university (3 years ago) and alot of ppl that never heard of linux were being forced to use it (no guis there, unix, apache, and db related stuff).\n\nGuess what. Most of the ppl I knew here and use linux at home to do theyr school work have gone with GNOME (why I ask them... they say it looked alot better than kde...). Even though I think feature whise its pretty damn crap compared to kde."
    author: "."
  - subject: "Best way to do it "
    date: 2005-02-19
    body: "I think this is by far the best way to do it:\n\n\"Next release after 3.4 will be 3.5. We use the four months till the QT 4 \nrelease for implementing outstanding features and usability \nimprovements, integrating new artwork from kde-look contests, improving \ndocumentation, completing translations, moving to subversion, \nexperimenting with a new build system, but don't do any QT 4 porting \nyet.\n\nThe day Trolltech releases the final version of QT 4.0 we branch and \nfreeze CVS (or rather subversion then) for the 3.5 release and start \nporting the HEAD branch (libraries and modules) to QT4. Then we proceed \nwith the alpha release scheme Stephan proposed until we have a KDE 4.\n\nNot doing 3.5 and 4.0 development in parallel avoids the problems I \nmentioned above and has the additional advantage that the QT 4 port \ncould be done as concerted effort by all developers, so that we quickly \nget to a state where everything compiles and is usable for development \nagain. Maybe something like a \"porting meeting\" could be held to do \nthis in the most efficient way. Maybe also Trolltech could help with \nthis, the QT developers won't have anything to do anymore after the QT \n4 release, after all ;-) \"\n\nThis would also mean that KDE would have something to cempete with GNOME 2.12, ATI and Nvidia will tell you how important that is ;)\n\nBut, it's not just that, I think it would be easier for the developers and much much better for the users."
    author: "Al"
  - subject: "Re: Best way to do it "
    date: 2005-02-19
    body: "Unfortunately someone is trying to hold off the developers from doing bugfixing for the KDE 3.4 release. For example discussions about KDE4 build systems are started, discussions about additional KDE releases and to take away the last second of spair time the whole KDE cource tree has been imported into an experimental Subseven server so that everyone is testing subseven now.\n\nActual bug count:\nKonqueror: 1763 - increased by 7 during the last week\nkdelibs:  329  - decreased by 2 during the last week\nkwin:  103  - increased by 4 during the last week\n\nCongratulations to the KDE-Pim team:\nkmail: 28 bugs less than a week ago\nkorganizer: 18 bugs less\n"
    author: "Xedsa"
  - subject: "Re: Best way to do it "
    date: 2005-02-19
    body: "> Unfortunately someone is trying to hold off the developers from doing bugfixing for the KDE 3.4 release.\n\nSh*t, you just disclosed our conspiracy..."
    author: "Anonymous"
  - subject: "Re: Best way to do it "
    date: 2005-02-19
    body: "Hehe ;-)"
    author: "Xedsa"
  - subject: "Re: Best way to do it "
    date: 2005-02-19
    body: "I'm sure you are here to help increasing the developers' productivity further by checking all those reports whether they still apply and respective validated or closed them."
    author: "ac"
  - subject: "Re: Best way to do it "
    date: 2005-02-19
    body: "Overall more bugs were closed in the last 7 days than new ones reported as reaction to KDE 3.4 Beta 2 - I don't understand what problem you have."
    author: "Anonymous"
  - subject: "Re: Best way to do it "
    date: 2005-02-19
    body: "> This would also mean that KDE would have something to cempete with GNOME 2.12\n\nKDE 3.4 can compete just fine with GNOME 2.1x\n\n> ATI and Nvidia will tell you how important that is ;)\n \n?"
    author: "Anonymous"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "yeah, I didn't get the \"ANI and Nvidia\" too.\n\nbut I do think, while KDE 3.4 can compete with gnome 2.12 (it beats it, I'm sure) it would be cool to have a KDE 3.5, just to finnish the 3.x in a really cool way. \n\naltough it's imho also a good thing to start on certain QT4 porting things. it would be esp nice if KDE-base and kde-libs would be in a QT4-ready state when KDE 3.5 hits the streets."
    author: "superstoned"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "> it would be esp nice if KDE-base and kde-libs would be in a QT4-ready state when KDE 3.5 hits the streets.\n\nIt will never be able to compile against both Qt 3.3 and Qt 4 at same time."
    author: "Anonymous"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "Qt 4 will come with a compatibility library that Qt 3 applications can link against.\n\nhttp://www.trolltech.com/newsroom/announcements/00000169.html\n"
    author: "Jim"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "\"Porting\" is not the try to make Qt 3 code run with Qt 4's Qt 3 compatibility library."
    author: "Anonymous"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "it doesn't have to, KDE 3.5 could be build against the same (or just slightly changed) KDE libs as KDE 3.4 is. There can be different branches, isn't it?"
    author: "superstoned"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "> ?\n\nThe Gnome camp plans to migrate GTK+ to cairo and glitz, hardware-accelerated (-> ATi and nVidia) vector graphics libraries. I assume he's referring to that announcement.\n\nHowever, this will take them a while, and Trolltech is talking about cairo integration in Qt4 as well. Bottom line, this is a non-issue for KDE 3.5."
    author: "Eike Hein"
  - subject: "Re: Best way to do it "
    date: 2005-02-20
    body: "thats not good, lets start before ! qt4 gets out so the final release for qt4 will be very very good and stable !!!\n\n"
    author: "chris"
  - subject: "New build system"
    date: 2005-02-20
    body: "After reading through the religious wars regarding which build system to choose, I must ask, have the KDE developers considered creating their own build system?  Some may immediately scoff at the idea of writing a complicated new tool when there are plenty of build systems out there.  However, I think the mere exercise of planning out a new build system will address many concerns the developers have argued and others they haven't even considered.\n\nWhile the developers argued viciously about the pros and cons of various build systems, there was very little discussion about what KDE actually needs.  How can the developers make an educated decision when they don't even know what the requirements are?\n\nSomeone said the build system needs the backing of a full programming language.  There seemed to be no debate about this statement.  I submit to you that if you need to hack together complex rules to build a project, perhaps the build system doesn't meet your requirements.  It would be easier to make that determination if we knew what the requirements were.\n\n"
    author: "Zippy"
  - subject: "Re: New build system"
    date: 2005-02-20
    body: "Unsermake was developed by our fearless release coordinator Stephan Kulow.\n\nDerek\n"
    author: "Derek Kite"
  - subject: "Not just code -- for 4.0 please rethink apps too!"
    date: 2005-02-20
    body: "Maybe in 4.0 we can get rid of some overlapping apps, and specialize a bit more, like:\n\nKuickshow, Kview -- Two different image engines (imlib and ...?). Lots of overlapping functionality, none is quick enough for quick viewing or complete enough for image management. Aim should be a complete image manager (Like Kimdaba, or Digikam, or Gwenview) with photo importing abilities, and a really quick image viewer (only one-shot, or, at most, one-shot and slideshowing).\n\nJuk, Amarok, Kaffeine, Noatun, Kaboodle -- Same as above. We need a media manager (amarok would be closer to that aim, if it weren't for GUI weirdness. Juk is not as complete, but okay) with support for ripping, visualization, etc., and a quick player (something like kaboodle, sans aRTs weirdness). I can't really tell if video capability is needed on the media manager, but, with a decent media backend, it should be so simple to implement (since we already have visual effects) that it wouldn't hurt to add just to please ppl who find it useful.\n\nKate, Kwrite, Kedit -- If Qt4 supports RTL decently, Kedit isn't needed anymore. Kate is something between Kwrite and Kdevelop/Quanta/Kile. I don't really see the need for both it and Kwrite -- If Kate is lightweight enough, there's no need for Kwrite, if it is heavyweight, just open Kdevelop.\n\nKSirc -- It is a usability beast. Konversation, although not perfect, is way more usable..."
    author: "Renato Sousa"
  - subject: "Re: Not just code -- for 4.0 please rethink apps too!"
    date: 2005-02-20
    body: "Rethinking apps within a new KDE context will only be possible for the apps which are actually within KDE's standard packages. Most of the ones you mention are not."
    author: "ac"
  - subject: "Re: Not just code -- for 4.0 please rethink apps t"
    date: 2005-02-20
    body: "And perhaps whats in the standard packages, needs rethinking. The fact that they currently arn't is not any reason not to consider them, or improved versions of them."
    author: "Uno Engborg"
  - subject: "Re: Not just code -- for 4.0 please rethink apps too!"
    date: 2005-02-20
    body: "If Kate Part gets BiDi support, KEdit may be deprecated, but KWrite/Kate pair will stay, KWrite is just a easy version of Kate, and I see no real clash between Kate and KDevelop/Quanta, me doesn't want to start an IDE or web development app to work on bash scripts or to play around with SML ;)"
    author: "Christoph Cullmann"
  - subject: "Re: Not just code -- for 4.0 please rethink apps t"
    date: 2005-02-21
    body: "I don't see why there needs to be an \"easy\" version of Kate. As far as I (and I would add any inexperienced user) can see at a first glance, Kate is Kwrite with MDI. If you just ignore the Sidebar, using Kate is no different from using Kwrite."
    author: "Renato Sousa"
  - subject: "Re: Not just code -- for 4.0 please rethink apps too!"
    date: 2005-02-20
    body: "kwrite is actually as lightweight as kate, and i didnt know about kedit\n\n\nafaik ksirc isnt cpp project so it must die\n\n"
    author: "nick"
  - subject: "Re: Not just code -- for 4.0 please rethink apps too!"
    date: 2005-02-20
    body: "> Maybe in 4.0 we can get rid of some overlapping apps\n> Juk, Amarok, Kaffeine, Noatun, Kaboodle\n\nIf you would install less 3rd party applications then you would have less overlapping applications."
    author: "Anonymous"
  - subject: "Re: Not just code -- for 4.0 please rethink apps t"
    date: 2005-02-21
    body: "Okay... should have reread the post.\n\nI cite Amarok and Kaffeine in that list because both are considered best in what they do by a fair amount of ppl (perhaps I should have included Kmplayer too, to crowd even more that list). It means that for some people, aside from KDE Apps (Juk, Noatun, Kaboodle), there are even more apps installed.\n\nAlso, Amarok have good features no other player has. It should be considered in 4.0, even if only to suck the good features of it into JuK. As it is now, JuK (at least for me) isn't enough of a music manager/player. Amarok on the other hand, is fairly complete, and, IMHO, the only thing that keeps it from KDEmm is GUI weirdness."
    author: "Renato Sousa"
  - subject: "Re: Not just code -- for 4.0 please rethink apps too!"
    date: 2005-02-20
    body: "On a lot of that, you are fairly accurate, although most of those are third-party, not core KDE apps.\n\nHowever, you're just completely wrong about Kate. Kate is better than KDevelop or Quanta. Yes, that's what I think. It does what I want, rather than a billion other things, and it loads in around a split second.. If it is removed in KDE 4, I will just stay on KDE 3.5 until someone adds it again. It really is that important to my desktop usage."
    author: "jameth"
  - subject: "naming schema"
    date: 2005-02-21
    body: "What about renaming some of the apps? Don't get me wrong, I love KDE for it's good design, but I am not convinced of the \"K\" naming schema. In some places it's ok for me (Konqueror, Kate, amaroK, ...). But IMHO it looks ugly if a name starts with two upper case letters (KDevelop, KMenu, KWrite, KWord...).\n\nMy suggestion would be to do a general renaming in such cases (e.g. kDevelop, or KDE Develop, whatever).\n\nWhat do you think?"
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-21
    body: "> What do you think?\n\nYou are caring too much about really unimportant things"
    author: "ac"
  - subject: "Re: naming schema"
    date: 2005-02-21
    body: "> You are caring too much about really unimportant things\n\nThat may be true in general, however, in this case I am not sure ;-)\n\nIt's not only that the names \"look ugly\". To me, it looks like a typing error and while I am not a psychologist, I wonder, if less technical users might have a problem to even start using KDE if the names are subconsciously classified as 'geeky' ;-)\n"
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-21
    body: "the names are very important, not the internal name,\nthat may be anything, I can live with that.\nAs long that a user does not see any reference to\nstupid choosen names (how wonderfull the aplication may be)\nexcept maybe in the about dialog.\n\nexample:\n\"krename\", a perfect name would be \"File Renamer\", menu entry in\nkicker should be the nice name, window title should be the nice\nname, documentation entry should be the nice name, web page for\nkrename should start with: \"File Renamer <small>for the kde desktop</small>\"\n\nIn the example, I've talked about \"kicker\", guess what, kicker is\nallright, it sound good, once you learn what it is, it sticks,\nsame for konqueror, and some others. K3b, I don't know, kind of works.\nkmail, why? it might be a perfect application name, but please,\nremove every reference to the name! It's silly."
    author: "pieter"
  - subject: "Re: naming schema"
    date: 2005-02-22
    body: "I see the argument, but am not convinced of it ;-)\n\nE.g., what about our three Text editors?\n\nText Editor 1 (-->KEdit)\nText Editor 2 (-->Kwrite)\nText Editor 3 (-->Kate)\n\nAnd all of this in the submenu \"Text editors\"? ;-)\n\nI also think, KDE should \"signal\" the user that this is a \"real\" KDE app, not an app that \"just\" works (more or less) well together with it (OOo).\n"
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-23
    body: "KEdit - get rid of it once Kate & KWrite get bidi.\n\nKWrite - \"Text Editor\"\n\nKate - \"Programmer's Editor\"\n"
    author: "Jim"
  - subject: "Re: naming schema"
    date: 2005-02-23
    body: "Only used the Text editors as an example, unfortunately there are more of them (multimedia players, picture viewer).\n\nBut the other argument is: do we (or better: the KDE project) want that everybody speeks of \"the Text Editor\" (now: KWrite), \"the Text Processor\" (now: KWord), or \"the File Renamer\"?\n\nOr is it better to give the KDE applications a KDE-ish name?\n\nLast not least, I don't think anyone can bring KDE in a special direction. But one can make some suggestions, how to make things better, maybe the application authors (those who decide on the names of their apps) change their mind..."
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-21
    body: "> It's not only that the names \"look ugly\". To me, it looks like a typing error and while I am not a psychologist, I wonder, if less technical users might have a problem to even start using KDE if the names are subconsciously classified as 'geeky' ;-)\n\nIMHO the \"less technical users\" should use the description in the KMenu.\n\nKWrite     => Text editor\nKGhostView => PS/PDF viewer\netc."
    author: "ac"
  - subject: "Re: naming schema"
    date: 2005-02-21
    body: "Yes they could. I tried it myself at the beginning. Those replacement strings are simply badly chosen. If I start the \"Text Editor\" I still want to know if it is beloved NEdit or just KWrite/Kate or KEdit. And yes - I don\u00b4t want to show both, appnames and their descriptions, in the \"KMenu\". That looks odd, too."
    author: "Sebastian"
  - subject: "Re: naming schema"
    date: 2005-02-22
    body: "If he or she already uses KDE, okay. But if not (and most users do NOT use KDE), the argument fails. Nobody is talking about the program \"Text editor\" or \"PS/PDF viewer\". We all are talking about KEdit, KWrite or Kate (or KGhostView or KPDF), don't we? And if 2 of 3 programs (program names) look weird (at least for some of us ;-) )... I am not sure this is good for KDE...\n\nAlso: If I as non-native-speaker have to pronounce \"KWrite\", it sounds a bit silly ;-)\n\nA suggestion would be to pronounce the \"K\" as in \"key\", but e.g. to change the name to \"kWrite\" (or \"key Develop\"/\"kDevelop\"). This way the \"k\" would get a meaning beyond of KDE. And the K in KDE would also get another meaning. KDE, the \"key Desktop Environment\"...\n"
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-23
    body: "> KGhostView => PS/PDF viewer\n\nWhy is this even in the K menu at all?  I've been using KDE since the pre-1.0 betas, and I can honestly never recall ever invoking it from the K menu.  I have, however, clicked on PS/PDF files to open them quite a lot.\n\nIt seems to me that the K menu should be a list of applications that users will want to run.  It also seems to me that reading a PDF is not something that many users consider to be \"running an application\".\n\nThings like Kate, KWord, etc, belong in there because they are often invoked outside of the context of a particular document.  KGhostview is different because you cannot create documents with it, so you'll always be invoking it within the context of a preexisting document - i.e. you'll be double-clicking on it in Konqueror and not running it from the K menu.\n\nDo any users actually need KGhostview in the K menu?  If not, or if they are an extreme minority, wouldn't it be better to omit it for clarity?  The last thing newbies need when they open the K menu is a bunch of meaningless gobbledegook getting in the way of the bits they actually need to get to.\n"
    author: "Jim"
  - subject: "Re: naming schema"
    date: 2005-02-23
    body: "Excellent idea... but IMHO it should be an option..."
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-23
    body: "Just keep in mind that the KMenu also shows what applications are available on the system. It's similar to the main menu of an application that shows what functionality it offers.\n\nI hoped that the TOM (Task oriented menu) would become reality, but it seems as if Aaron just doesn't have the time to finish it. :(\n\nhttp://www.urbanlizard.com/~aseigo/index.html.part"
    author: "Christian Loose"
  - subject: "Re: naming schema"
    date: 2005-02-21
    body: "I support this, not because I care about the names, but for the sake of keyboard navigation in the K Menu.  \n\nRight now, you have 10 apps starting with K in a menu, so you can't just hit the first letter in the name to go there.  You want KMail?  Well you have to hit K, which will get you to Kamuni (yes I made that up) and you still have to hit the down arrow a bunch of times to get where you want. \n\nIf the names don't start with K, then you just hit the first letter and most of the time you have the right one selected."
    author: "Leo Spalteholz"
  - subject: "Re: naming schema"
    date: 2005-02-22
    body: "In my SuSE distro is a feature that lets you hit two or more letters, and the first matching name is chosen (and the letters you entered are underlined).\n"
    author: "MM"
  - subject: "Re: naming schema"
    date: 2005-02-22
    body: "Non-issue since K Mrnu defaults to showing description (app name) and not the other way around."
    author: "ac"
  - subject: "Please, please, please ..."
    date: 2005-02-20
    body: "Lets fix the gmail bug first! It was killed in 3.3.2 but has managed to creep in again. I hope 3.4 does it or else i'll have to wait till 4.0 :-("
    author: "Kanwar"
  - subject: "my votes"
    date: 2005-02-22
    body: "since i dont know what bug i have to vote for i give my vote here :)\n\nplz make konq load gmail, so i can ditch mozilla (not that i hate mozilla, i just like to have 'one to rule em all')\n\n"
    author: "ac"
  - subject: "Re: my votes"
    date: 2005-02-22
    body: "http://bugs.kde.org/show_bug.cgi?id=83786\nhttp://bugs.kde.org/show_bug.cgi?id=94755\nhttp://bugs.kde.org/show_bug.cgi?id=93482"
    author: "Anonymous"
  - subject: "Re: my votes"
    date: 2005-02-22
    body: "Done ;)"
    author: "Carewolf"
  - subject: "QT 4"
    date: 2005-02-21
    body: "Is the difference between QT3.x and QT4 that huge. How much has to get changed?"
    author: "Jakob"
  - subject: "Re: QT 4"
    date: 2005-02-21
    body: "No, both QuickTime 3 and QuickTime 4 are very outdated by now."
    author: "ac"
  - subject: "Re: QT 4"
    date: 2005-02-21
    body: "More than between Qt 2.x and Qt 3.x, that's for sure."
    author: "Anonymous"
  - subject: "Re: QT 4"
    date: 2005-02-21
    body: "Porting to Qt 4:    http://doc.trolltech.com/4.0/porting4.html\nWhat's New in Qt 4: http://doc.trolltech.com/4.0/qt4-intro.html"
    author: "Christian Loose"
  - subject: "It's all about priorities"
    date: 2005-02-21
    body: "Well, it's all about priorities. Lost of work, all kinds of dependencies, constraints on time and personnel. Looks like a normal project to me :-)\n\nSeriously. I propose some sort of roadmap mangement. Put it al in bugzilla and count the votes or put a website online with all roadmap choices and a voting system. That way we get the most wanted stuff first, and that's the Open source (Bazar Style) way of doing this.\n"
    author: "Freek"
  - subject: "THANKS DEREK!"
    date: 2005-02-21
    body: "Actually i post this because i saw that _all_ the other post are in the '.' thread. i personally dont like the '.' tread.\n\nSo i make a THANKS DEREK thread, to thank our beloveth Derek for his work, since it seems every-posting-body forgot about it this time.\n\nLets make this thread as fat as the above one!\n\nTHANKS AGAIN DEREK :)\n\n_cies."
    author: "Cies Breijs"
  - subject: "Where is the old layout?"
    date: 2005-02-22
    body: "Why was it removed? It was autogenerated anyway, it didn't hurt anybody.\n\n:-(\n\nGive me back the table with the icons that immediately showed where something happened.\n\n"
    author: "Roland"
  - subject: "Re: Where is the old layout?"
    date: 2005-02-22
    body: "Ah, there is just a wrong link in the story, the layout is here:\n\nhttp://cvs-digest.org/index.php?issue=feb182005\n"
    author: "Roland"
---
In <a href="http://cvs-digest.org/index.php?newissue=feb182005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/?newissue=feb182005&all">all on one page</a>):  Kttsd adds support for Italian Festival voices.
<a href="http://uml.sf.net/">Umbrello</a> improves import from ArtisanSW, Visio, ArgoUML, Fujaba and NSUML.
<a href="http://www.koffice.org/kspread">KSpread</a> has a new insert calendar plugin.
<a href="http://www.konqueror.org">Konqueror</a> loses its Cut/Copy/Paste buttons.
KDE begins move to Subversion and discusses future roadmap.



<!--break-->
