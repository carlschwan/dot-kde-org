---
title: "KDE CVS-Digest for April 15, 2005"
date:    2005-04-16
authors:
  - "dkite"
slug:    kde-cvs-digest-april-15-2005
comments:
  - subject: "What about Subversion"
    date: 2005-04-16
    body: "Hasn't KDE switched to Subversion?"
    author: "Alex"
  - subject: "Re: What about Subversion"
    date: 2005-04-16
    body: "Not yet."
    author: "Anonymous"
  - subject: "Re: What about Subversion"
    date: 2005-04-16
    body: "Not yet, but real soon now (TM)."
    author: "cm"
  - subject: "All in one page"
    date: 2005-04-16
    body: "Good to see that the CVS-Digest is back again (if only until the subversion switch). I'd like to say though that I miss the \"all in one page\" link, which I find much more convenient.\n\nIt would be great if that link would reappear in the dot news messages."
    author: "Jakob Petsovits"
  - subject: "Re: All in one page"
    date: 2005-04-16
    body: "\"I'd like to say though that I miss the \"all in one page\" link, which I find much more convenient.\"\n\nIt's on the left side near the top of the digest.  I think it may set a cookie cause I only selected it once and now I always get all in one page views."
    author: "MrGrim"
  - subject: "Other good news"
    date: 2005-04-16
    body: "\"Zack Rusin to Finish Integrating Mozilla Firefox with KDE\"\n\nhttp://www.kdedevelopers.org/node/view/976\nand\nhttp://www.mozillazine.org/talkback.html?article=6419\n"
    author: "ac"
  - subject: "Awesome"
    date: 2005-04-16
    body: "But does anyone think it will make it in 3.5? That would be perfect!"
    author: "Alex"
  - subject: "Re: Awesome"
    date: 2005-04-17
    body: "Why should it be in KDE 3.5? The KDE releases have their own browser and are fine without a Mozilla dependency."
    author: "Anonymous"
  - subject: "Re: Awesome"
    date: 2005-04-18
    body: "31 megabytes: Firefox webbrowser. Includes nothing else.\n\n38 megabytes: Konqueror webbrowser. Includes file manager, application framework and world's premier desktop environment.Oh, and the browser is a drop-in component.\n\nThe choice is simple. Firefox seriously needs to work on its bloat."
    author: "Brandybuck"
  - subject: "Re: Awesome"
    date: 2005-04-17
    body: "I guess a lot earlier than Mozilla 3.5 ;)"
    author: "Kevin Krammer"
  - subject: "Krita new features"
    date: 2005-04-16
    body: "I'm more and more interested at what's going on the Krita front. What is this \"wet painting\" that is close to be added to it? Corel Painter-like realistic painting tools?\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Re: Krita new features"
    date: 2005-04-16
    body: "Wet painting is a watercolor paint simulation based on Raph Levien's Wet Dream.\nhttp://www.levien.com/gimp/wetdream.html\n\nUnfortunately it won't be in the next version, because it's not finished yet and Krita is now in feature freeze."
    author: "Sven Langkamp"
  - subject: "Re: Krita new features"
    date: 2005-04-16
    body: "What Sven said -- but I'd like to expand a bit on it. Corel Painter just works\nwith pixels, mangles them using various interesting formulas and so on. But that's just the same kind of thing as Photoshop or XPaint does: messing with pixels that are described as pure colour.\n\nWhat I'm most interested in (but none too competent to actually do) is do something new: instead of raster images or vector images, make images with simulated 'stuff' -- paint that has values for thickness, wetness, stickyness, all those parameners.\n\nI was _this_ close to getting a first example done, but just didn't make it..."
    author: "Boudewijn Rempt"
  - subject: "Something wrong with the statistics?"
    date: 2005-04-16
    body: "> Commits:\t0 by 0 developers, 0 lines modified, 0 new files"
    author: "Anonymous"
  - subject: "Re: Something wrong with the statistics?"
    date: 2005-04-16
    body: "Fixed now.\n\nDerek"
    author: "Derek Kite"
  - subject: "KTTS enhanced notification options"
    date: 2005-04-16
    body: "Thanks to Derek for all his hard work.  Much appreciated.\n\nThought I'd add that the KDE Text-to-Speech system has enhanced options for controlling speech of notifications (KNotify) from apps. You can now specify general notifications options as well as specify customizations per application  event.\n\nkdeaccessibility/kttsd\n"
    author: "Gary Cramblitt (aka PhantomsDad)"
  - subject: "KOffice development taking up again!"
    date: 2005-04-16
    body: "It is absolutely cool to see the many commits coming into KOffice recently. Some names of people contributing regularly were not known a few months ago in the KDE community.\n\nPart of the new activity surge may be due to the soon-to-come new KOffice release. But probably the main part is due to the fact that KOffice will have a great future on the KDE desktop, even when considering the current stamped going towards OOo. \n\nWith so many core functions in OOo-2.0 depending so heavily on Java, Sun was doing the OOo users and itself a dis-favor. [Or was it the \"independent\" OOo developers not on a Sun payrole who added the Java stuff?? Hmmm... are there any non-Sun engineers working on OOo code at all?!]\n\nKOffice is a very fast and lean Office suite, and at the same time very powerful. I like it, even if some apps are still very incomplete. Krita is a shining example for rapid development, even with few people actually writing the code. Boudewijn rocks! "
    author: "konqui-fan"
  - subject: "Re: KOffice development taking up again!"
    date: 2005-04-16
    body: "Well, maybe a little, although I'm rather a staid, even stodgy person, really. But I am quite proud of the select-by-color-picker tool... \n\nAnyway, what I wanted to say -- in the year-and-a-half since I've been hacking on Krita, I've been far from the only one. Patrick, Adrian, Sven, Bart, Dirk, Daniel, Casper, Cyrille, Michael, Danny, Melchior, Clarence, Roger, Saem have done a lot, too -- often fixing my mistakes -- and we've all built on the work of Michael (different Michael), Matthias, Andrew, Carsten, Toshitaka John and Patrick (same Patrick). It's a great group of people!\n\nOf course, thanks mainly to Cyrille, Krita is almost completely componentized -- even RGB is a plugin -- so it's really easy to get into. That, and Krita's still only about 40.000 lines of code.\n"
    author: "Boudewijn Rempt"
  - subject: "ShowFOTO is just amazing"
    date: 2005-04-17
    body: "The showfoto application which comes with digikam is just amazing, it is much better for viewing images in a folder. kuickshow and kview are dummies compared to showfoto. thanks Digikam developers for a nice image viewer."
    author: "fast_rizwaan"
  - subject: "Re: ShowFOTO is just amazing"
    date: 2005-04-17
    body: "check gwennview. but the best would be if these developers would work together to create THE BEST app."
    author: "superstoned"
  - subject: "Re: ShowFOTO is just amazing"
    date: 2005-04-17
    body: "They work togehter: Gwenview, Digikam and KimBaDa all share the same plugins (kipi-plugins). This is a big step in the correct direction."
    author: "anon"
  - subject: "Re: ShowFOTO is just amazing"
    date: 2005-04-17
    body: "IIRC, kipi-plugins are jsut for extending the programs functionality (html contact sheets etc.)\n\nWHat I'd like to see is a common image-format library/plugins. For example, KSquirrel (at apps.kde.org, a nice opengl imageviewer) uses its own plugin-library. Why cant a couple more apps start using this, so we only have to write [openexr|dng|other-raw]-decoders once?"
    author: "me"
  - subject: "Re: ShowFOTO is just amazing"
    date: 2005-04-17
    body: "The major differrence between showfoto and others KDE apps to show photographs is that showfoto support DigikamImagePlugins (not kipi-plugins, it's not the same way and this is have non sence!), and showfoto is a real image editor (simple but real)! Sure, the fonctionnalities must be improved in the future...\n\nA nice day\n\nGilles Caulier \ndigiKam project"
    author: "Gilles Caulier"
---
In <a href="http://cvs-digest.org/index.php?newissue=apr152005">this week's KDE CVS-Digest</a>:
<A href="http://digikam.sourceforge.net/">digiKam</a> adds two new effects plugins: blowup and photograph inpainting.
<a href="http://kmail.kde.org/">KMail</a> import filters: Evolution, Thunderbird, Sylpheed Claws and maildir.
<a href="http://www.koffice.org/">KChart</a> can export charts as bitmap files.
<a href="http://www.koffice.org/">KOffice</a> gets new icons.


<!--break-->
