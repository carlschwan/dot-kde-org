---
title: "People Behind KDE: Andr\u00e1s Mantia"
date:    2005-12-08
authors:
  - "tbaarda"
slug:    people-behind-kde-andrás-mantia
comments:
  - subject: "I know that guy!"
    date: 2005-12-08
    body: "I didn't even know Andras was being interviewed. Since Waldo and Tink moved to my neighborhood I actually got the pleasure of inviting my interviewer over for dinner. Anyway great interview and pictures! I think this interview was done a while ago because I'm looking forward to hearing he received and installed the Athlon64 939 pin 3200 I sent. \n\nOf course the usual plug goes here... I need to do a fund raiser but I've been really busy. Andras needs a raise! He is worth much more than he's being paid. Quanta users should want to keep him very happy as his resume looks really good after all these years on this project. ;-)\n\nOf course Andras did get the official Quanta shirt... Attached is a pic of us at 2 in the morning in Spain modelling our Quanta Shirts."
    author: "Eric Laffoon"
  - subject: "Re: I know that guy!"
    date: 2005-12-08
    body: "Thanks Eric! The interview indeed took place quite some time ago, some weeks after aKademy 2005 ended. Since than I've started to build a new computer, but I don't have the CPU you mention yet, so I have only the that old laptop now.\nAlso the number of caught mices have increased now (I counted 44) and unfortunately two more bird were caught. And the cat is bigger and bigger. ;-)\nStill he is very lovely. My wife told me that we should take him as well to that island..."
    author: "Andras Mantia"
  - subject: "Re: I know that guy!"
    date: 2005-12-10
    body: "We indeed feasted on Eric's already famous BBQ'ed Salmon and it was all I heard about it and even more!\n\nI didn't do the interview though. This summer I handed the web pages and the interviews over to the very capable hands of the KDE-NL team and as with everything that they do, they did a marvelous job with the interviews as well. I couldn't have wished for a better successor!\n\nI'm very proud of them, you rock guys!\n\n--\nTink"
    author: "Tink"
  - subject: "kdewebdev rocks"
    date: 2005-12-08
    body: "fine to see another member from the kdewebdev-team interviewed on the dot.\nwell done, Andras !\nin addition to your comments, I would like to mention that kdewebdev is still underrated, maybe partly because it still isn't listed on the kde main site (why ?).\nin any case, keep up the good work, and let's look forward to the interview with Michal :)\n\n"
    author: "hoernerfranz"
  - subject: "I like..."
    date: 2005-12-08
    body: "... the screenshot of Andras' desktop :)"
    author: "adymo"
  - subject: "Re: I like..."
    date: 2005-12-09
    body: "> ... the screenshot of Andras' desktop :)\n\nYeah? You would. ;-) Maybe I can find time soon to make mine look like that."
    author: "Eric Laffoon"
  - subject: "Rock on :)"
    date: 2005-12-08
    body: "I think People Behind KDE interviews are brilliant. I't nice to know that there are so much diversity with the people involved in KDE.\n\nps. It's just shame that all the people interviewed are giving the wrong answer to that questiong about Linus or Stallman ;)"
    author: "petteri"
  - subject: "Really a good man"
    date: 2005-12-09
    body: "He is a very good man: i had a discution with him about a bug in Quanta and he turned out to represent a very social man(the bug was about Romanian characters in Quanta and he is a Hungarian living in romania)"
    author: "zvonSully"
---
We know him as a Romanian developer for Quanta and contributor to several other KDE projects. His cat began to catch birds some time ago and he calls himself a local-patriot.  His wife may not be into KDE coding but he would make sure she was if he had to be stuck on an island with a KDE developer. Tonight's <a href="http://people.kde.nl">People Behind KDE</a> interview is with <a href="http://people.kde.nl/andras.html">Andr&aacute;s Mantia</a>.



<!--break-->
