---
title: "KDE-Artists.org: Bastian Salmela Interviewed, Kollaboration Success"
date:    2005-08-09
authors:
  - "jriddell"
slug:    kde-artistsorg-bastian-salmela-interviewed-kollaboration-success
comments:
  - subject: "You were a great help!"
    date: 2005-08-09
    body: "As the author of Kalzium I can only thank this project. I not only got mockups for the new interface but also about 10 new icons for Kalzium. In all that in pretty much no time. Thanks."
    author: "Carsten Niehaus"
  - subject: "Thank you Janet .. "
    date: 2005-08-09
    body: ".. for creating a place where artists can share their work. BWT I am really excited what the artists have in store for KDE 4. \n\nCiao'\n\n\nFab"
    author: "Fab"
  - subject: "August is the month to make Kontact...sexy!"
    date: 2005-08-09
    body: "The Kontact Icon Contest guidelines are now available at http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,352.new/boardseen#new\n\nBTW many many thanks to Lando who has contributed an unbelieve amount of time and energy into the KDE-Artists site."
    author: "Janet Theobroma"
---
<a href="http://www.kde-artists.org">KDE-Artists.org</a> has <a href="http://www.kde-artists.org/main/content/view/56/29/">interviewed KDE's Konqi artist Bastian Salmela</a>.  Basse talks about how he got involved in KDE artwork when he updated the model our maskot Konqi the Dragon.  He also reveals his next project to be <a href="http://orange.blender.org">Orange</a>, the first Open Source animated film.  Meanwhile the <ka href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/">Kollaboration forum</a> for artist/developer communication has some exciting projects on the go, read on for more.


<!--break-->
<h3>Kollaborations to Take Note of</h3>

<p><a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/board,43.0">Kollaborations for KDE 3.5 artwork</a> has been started. This is a great opportunity to contribute to the default look and feel of KDE. At a recent IRC meeting it was decided to improve the colours in the about:konqueror pages of 3.4 and reuse some of the elements. It was also decided to take the wave element and use it as a common theme throughout 3.5. At this point we need various versions of the wave element so as to achieve a better fit with the the interface as a whole.</p>

<p>Kontact has started a Kollaboration board, <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/board,31.0">Make Kontact Sexy</a>, for new icons and elements to spice up their corporate look. There will be an upcoming contest to create a new logo and icon for Kontact.

<p>Our newest Kollaboration board is for <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/board,47.0">Ept</a>, a new Debian package manager.  Suggestions for application icon welcome.</p>

<p>The quote of the month is from Andreas Gungl in regards to Kollaboration "Wow, it's impressive to see the feedback. It seems this platform is going to become a huge value for KDE art development."  Several Kollaborations have been completed among them are <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,27/expv,0/topic,140">KTorrent</a>, <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,26/topic,243">KBoggle</a> and <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,26/expv,0/board,18.0">Kalzium</a>. </p>

<p>Finally our <a href="http://kde-artists.org/main/content/blogcategory/0/62/">Guides and Tutorials</a> gained an article explaining <a href="http://kde-artists.org/main/content/view/54/">where to install your icons to</a>.</p>


