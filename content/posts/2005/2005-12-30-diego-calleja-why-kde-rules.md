---
title: "Diego Calleja: Why KDE Rules"
date:    2005-12-30
authors:
  - "jriddell"
slug:    diego-calleja-why-kde-rules
comments:
  - subject: "Old year blues"
    date: 2005-12-30
    body: "Having used kde for quite some time, doing some minor development as well, reading this makes me wonder if any glory is still to gain here. Is it time for something new (or how to keep developers interested)?"
    author: "koos"
  - subject: "Re: Old year blues"
    date: 2005-12-30
    body: "One word: QtRuby."
    author: "KDE User"
  - subject: "Re: Old year blues"
    date: 2005-12-30
    body: "Yeah, absolutely. Ruby brings back the fun to coding, it's a perfect match for KDE. It's just a matter of time until we'll see a huge KDE app written with Korundum. In fact it's so tempting that I'd like to participate, just looking for some nice ideas ;)\n"
    author: "Mark Kretschmann"
  - subject: "RAD with ruby and Eric3 or KDevelop"
    date: 2005-12-30
    body: "Thanks for the positive comments about the ruby bindings. I'm looking forward to seeing what people will do with Korundum - it should be possible to write apps which couldn't be done at all in C++. For instance, dynamically accessing SOAP or REST services is much, much easier in ruby - that's one possible 'killer feature' for KDE ruby programming.\n\nYou can use QtRuby/Korundum with both the Eric3 (from 3.8 onwards), and Kdevelop. They both have graphical ruby debuggers and integrated support for Qt Designer .ui files to ruby code. The version of PyQt/PyKDE with KDE 3.5 has support for QScintilla ruby syntax highlighting which you need for ruby support with Eric3.\n\nKDevelop has ruby KDE project templates which allow you to write 'proper' KDE apps, that can use resources like icons or .xml KDE gui files, and which you can start from the KDE menu."
    author: "Richard Dale"
  - subject: "Re: RAD with ruby and Eric3 or KDevelop"
    date: 2005-12-31
    body: "Instead, why not make SOAP and REST service\nmuch much easier to program in C++/Qt/KDElib =P\n\nJust my 2 cents."
    author: "fp"
  - subject: "amaRuby"
    date: 2005-12-30
    body: "amaroK 3.0 in Korundum. Let's do it. :)"
    author: "Ian Monroe"
  - subject: "Re: Old year blues"
    date: 2005-12-30
    body: "And another word: PyKDE. If KDE4 still depends on Python (by using SCons and bksys), it would be cool to be able to guarantee the PyKDE/PyQt bindings will be present on a system with a KDE install. I know it's one of the factors that holds me back when i'm thinking about writing a small KDE application.\nI can't help but think that KDE would benefit if the barrier required to enter was lower - more focus, promotion, and documentation for development in higher level languages like Python or Ruby. This is somewhere where KDE could really take the initiative.\n"
    author: "Cerulean"
  - subject: "Re: Old year blues"
    date: 2005-12-30
    body: "Bksys/Scons is just a build-time thing, so KDE4 won't depend on python.\n\nBut I agree, having like Korundum in kdelibs would truely give developers the freedom to code in the language of their choice."
    author: "Ian Monroe"
  - subject: "Re: Old year blues"
    date: 2005-12-31
    body: "Good point. Well designed interpreted languages like Python and others are a blessing (or they should be). Why ? Because I think there are many developers who have, say, one hour a day to contribute to an OS project. This is hardly enough with a compiled language, where getting the sources, recompiling, etc. takes a long time. But it should let you make serious contributions if you only need to get latest svn and then start coding immediately, and see the results immediately, and then push the cahnges to svn.\n\nAnd, for many, many applications, it does not mean a performance penalty, provided that the dirty work is done in c++ (by the Qt/KDE libs). \n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Old year blues"
    date: 2006-01-07
    body: "Sorry but I just can't agree with you here. \n\nStarting out with a new project might be easier in agile languages and of course each language has its strong points that makes certain things really easy to do compared to others, but I think that if you work on an existing project you can fix the same amount of bugs or add the same amount of small features in an hour in C++ as in Python or other interpreted languages.\n\nIn my experience as a occassional bug-fixer / small-feature-adder for some OSS projects written in C++ (non-KDE btw) you hardly spend any time compiling after the first time so it's absolutely doable.\n\nSo I don't think it necessarily better, easier or faster using agile languages instead of C++, what I do think is that it has a lot to do with using all available resources and knowledge. The fact is that there are a lot of developers out there that don't know C++ and (for all kinds of reasons) have no plans to learn it either. All those developers are basically \"lost\" to KDE and I do think it could help KDE greatly if we could get some of them to spend their time on making great apps."
    author: "Quintesse"
  - subject: "too bad he used NVU to do it"
    date: 2005-12-30
    body: "he could have used Quanta, another great KDE project."
    author: "Patcito"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "Only if it doesn't crash for so long!!!\n"
    author: "anilet"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "And the bug report number is...\n\nYes, VPL is not that stable, I know, still we try to fix it. But in source mode (which is the big power of Quanta) I would be interested to see a crash that is\nQuanta's fault."
    author: "Andras Mantia"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "Well, VPL is the most important part of Quanta. Doing things visually with little knowledge of HTML (well, I know some basic html 4.0) is where NVU rocks. "
    author: "anon"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "> Well, VPL is the most important part of Quanta. \n\nThis is a little hard statement, especially that:\n- VPL is here for not a long time (so it never was a central part, nor it is)\n- VPL is only for (X)HTML, while Quanta is for much more\n- from the feedback we (developers) get, source editing (HTML, XML, PHP), user actions, project management and things like that are more used than VPL.\n\nBut I understand why VPL is important for newbies, and we want to make it really usable, so again, report the bugs and we will try to fix them."
    author: "Andras Mantia"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "> from the feedback we (developers) get, source editing (HTML, XML, PHP), user\n> actions, project management and things like that are more used than VPL.\n\nWell, isn't because VPL sucks (so far?) -- I would love to use it, but it is just such bother, that I usually switch to source code. Program which would provide completness of HTML-editing of Amaya with human-useful interface would be most awesome!\n\nMatej\n"
    author: "Matej"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-31
    body: "VPL does some things good, like adhering to your DTD and not mangling your entire file to edit one node. As for the rest... Don't forget it relies on KHTML which has also been under heavy development to support VPL. We had to write a lot of code just to find out if some things would even work and the complexity of the procedural discussions will make your eyes glaze over. The first code base of both the KHTML code and VPL was thrown out for the second which is being tossed aside for the third. The third should benefit from some of Apple's Webcore code and it should finally get a contextural interface. Even with the initial benefit of the KHTML rendering engine the task of visual development done right is a MASSIVE undertaking. I really doubt most people have a clue how massive. \n\nBTW I reject the idea that people who are doing little more than paragraphs, italics and links can't learn to push buttons on a toolbar and then click preview. Frankly if you don't know how to write HTML you're not going to know what the hell you're doing when you want to create a stylesheet or advanced layouts. The idea that you can actually succeed without any knowledge or that learning the equivalent of a dozen new words is difficult are both sad. What Quanta does well is helps you learn as you go and takes your basic conceptual knowledge and empowers you to perform with the full power of specific knowledge. I want VPL to be the best tool for newbies, but the idea that you can somehow produce anything of quality with zero knowledge is a myth worthy of Microsoft PR and it's produced a lot of garbage on the web. That garbage is why it's so hard to make a browser that delivers a perfect experience.\n\nFWIW visual tools to date have universal failings.\n* They are hard coded and inflexible to the DTD so they could not write XHTML Strict. In fact if they are HTML 4.01 Transitional compliant it's likely only the most recent version.\n* They completely rewrite the entire file removing all formatting.\n* They are pretty much useless if you use PHP or other scripting.\n* They rarely produce finished quality results without some manual editing unless you are not all that particular about visual layout.\n\nOur development of VPL has followed our approach to Quanta, which is DTD independent and easily produces compliant mark up. NVU is a glorified HTML mail composer and they've had way more resources than us. It's foundationally ill equiped for an XHTML future. Of course it's target was MS FrontPage. PHP developers prefer Quanta over Dreamweaver side by side... which is an incredible achievement. VPL is NOT the most important part, but we hope to finally get it right by version 4. When we do we will have accomplished something no other visual development tool has by addressing the bullet points above. I hope it gets more recognition than just \"oh, it's easy to use now\"."
    author: "Eric Laffoon"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "Sadly NVU doesn't seems really impressive to me. Too much Michael Robertson marketing, too little improvements over Mozilla's original code to stand up the claims.\nEven more, after releasing 1.0 development stopped, even when it still crashes often and there is a lot of room for improvement.\nI like Quanta a lot more, yet visual mode is still beta quality at most.\nThe only thing really disgust me from Quanta is the huge menu bar and thing like that... Some UI care is needed...\n"
    author: "Shulai"
  - subject: "Re: too bad he used NVU to do it"
    date: 2005-12-30
    body: "I was released only this summer. And of course NVU is Mozilla Composer+.\n\nHowever I believe that Quanta's interface for VPL is worse than NVU, esp. on smaller screens."
    author: "gerd"
  - subject: "Some ideas are stupid"
    date: 2005-12-30
    body: " \"I recommend you two things: First, wait for KDE 3.5.1\" ... I say first wait to die and then .. speak with God ;)\n \"Transitions are never easy, radical switchs can be dissapointing no matter what software it is.\" ...\n "
    author: "zvonSully"
  - subject: "Re: Some ideas are stupid"
    date: 2005-12-30
    body: "Tell me why it's \"stupid\"\n\nWhen I was a Gnome user I tried to switch to KDE and it failed to do it a few times. The change of \"mentality\" is there, you need to think in another way and it's NEVER easy. I definitively recommend anyone who switchs to another desktop to do a gradual switch."
    author: "Diego Calleja"
  - subject: "Don't be so damn halfhearted!"
    date: 2005-12-30
    body: ">\"KDE uses too many resources\": Yes, that's true.\n>\"KDE forces me to install many libraries!\": Welcome to the world of code reutilization.\n>\"C++ sucks\": Indeed, C++ is not the greatest language invented...\n\nWhy do people like to say \"yeah, we suck....but other people suck too\".  Isn't this meant to be an article about why KDE rules?  If so, should we not talk about those things that make KDE rule?\n\nListing a few accusations that a few fanatic minorities like to shout about, and then defending them on the back foot doesn't help.\n\nIf you want to make an admission that \"yeah things aren't perfect here...or anywhere else\", then do so in the middle of a paragraph or somewhere that people don't care about, PLEASE don't do it in the opening sentences.\n\nIf your major battle strategy is based around defense, then you've already lost the war.  Just ask the French about WW2, and the Maggeno line."
    author: "halcyonCorsair"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: "Ah, pardon me, that should have been \"Maginot\", rather than \"Maggeno\"."
    author: "halcyonCorsair"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: "<i>Why do people like to say \"yeah, we suck....but other people suck too\". Isn't this meant to be an article about why KDE rules?</i>\n\n\nIt's meant to be an article about why kde rules, but it's not intended to be an article which lies people.\n\n\nBesides, people LIKES seeing people admiting things like that un public. THey trust you when you look \"sincere\" and there're more chances that they'll take you seriously than if you write an article saying only the good things of kde"
    author: "Diego Calleja"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: "Indeed, every writing class I've ever had told me to acknowledge the counter-agruments in your opinion paper. It makes your agruments stronger."
    author: "Ian Monroe"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: ">\"KDE forces me to install many libraries!\": Welcome to the world of code reutilization.\nActualy this is completly false, KDE only forces you to install QT, that is ONE library, while any GTK+ based desktop forces you to install 5 (gtk, atk, pango, cairo and glib). So this actually one of the strengths of QT (KDE)."
    author: "Andrei Slavoiu"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: ">Actualy this is completly false, KDE only forces you to install QT, that is ONE library\n\nand what about libdbus, libarts, kdelibs,... ?\n\n>while any GTK+ based desktop forces you to install 5 (gtk, atk, pango, cairo and glib). So this actually one of the strengths of QT (KDE).\n\nBut at the end it's not more or less. It's just another way to package things. One  throw everything together in one large lib and the other divide it in different logical packages.\nWhat is better? I don't know. I just know that Trotech started to divide Qt4 in different packages/libs too.\n\n\nThat's why i like this article a lot. It doesn't try to tell you the great story from the great knight who know everything and can do everything. It's a honest article about the beauty of KDE which doesn't lie about things which some people may consider as a disadvantage but explain it in a objective way."
    author: "pinky"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: ">\"KDE uses too many resources\": Yes, that's true.\n\nIndeed, a simple runlevel 1 bash uses far less resources (and i don't wanna miss it in an emergency) but KDE is why I spent some thousand buckazoids to buy a new usparc, so why waste the power? And wouldn't this be a little bit unfair to the devs, who spent years of free time to make it?\n\n>\"C++ sucks\": Indeed, C++ is not the greatest language invented...\n \nYeah, lisp rules, we know that. I have used quite some languages, starting with z80 asm for my doctor thesis, but I somehow stuck with C++, it's not perfect, but has that nice-and-everything-is-right feeling. Java is fine, very well designed and nearly perfect, but to be honest, I _hate_ it (although I earn my money programming in it)."
    author: "therehastobeone"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: "the fact java earns your income might very well be the reason you hate it :D\n\nanyway, i agree with you on the use of KDE - i had to run icewm lately, because KDE was broken. well, no fun. i wanted to drag a taskbar entry to another destkop with the pager... yeah, can't do that. then i wanted an 'always on top' button on my windowdecoration (which is ugly in icewm, no matter what theme you choose). no go again. of course, i miss having two panels (i need some space for the media applet, kdict and run command applets, and my favorite applications) and of course i miss the great KDE look."
    author: "superstoned"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: "Hey, I hate Java too!\nActually, I like the language, but I REALLY hate virtual machine, JDK and Sun (yes, I know about gcc-java and all). So I got a similar language (C++) and started coding with Qt4. What can I say? I'm sold.\nI belive most people don't like C++ because of the lack of methods/functions, but this is easily fixed if you use some nice toolkit like Qt."
    author: "Iuri Fiedoruk"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2005-12-30
    body: "the classpath project enables native compilation. Classpath made great progress this year.\n\nhttp://www.gnu.org/software/classpath/"
    author: "gerd"
  - subject: "Re: Don't be so damn halfhearted!"
    date: 2006-01-01
    body: ">Java is fine, very well designed and nearly perfect\n\nyour kidding of course... right? :S"
    author: "attendant"
  - subject: "Too many resources"
    date: 2005-12-30
    body: "> \"KDE uses too many resources\": Yes, that's true.\n\nNo, that's not true. It is obviously not true, because if it was true, nobody would use KDE. \n\nWould anybody use anything that's \"too whatever\", especially if there were other alternatives? People who think KDE uses too many resources don't use it. People who do use KDE may perhaps think it uses many resources, but they don't think it's too many - if they use KDE, they apparently think it's worth it.\n\nAnd, of course, there's also the question whether KDE really uses many resources and what the hell does many resources actually mean.\n"
    author: "Lubos Lunak"
  - subject: "Re: Too many resources"
    date: 2005-12-30
    body: "Agreed\n\nI recently installed the latest Mandriva (compiled with gcc4 ), and KDE is REALLY snappy. Then of course, if I configure every possible kde service, application and  applet to run at start up, login is obviously slower, and memory usage is larger. BUT that's because you are getting a zillion features for Stroustrup's sake ! \n\nAlso, it is INCREDIBLE how many people complain about memory usage, just because they don't know about cached memory. They think it is being used, while this memory is just there to be reused if needed, giving a huge performance gain (since memory access is orders of magnitude faster than _any_disk_ access). Oh well ...\n\nCheers !"
    author: "MandrakeUser"
  - subject: "Re: Too many resources"
    date: 2005-12-31
    body: "\"No, that's not true.\"\n\nWell, it is. I think what he meant is that \"KDE could do the exact same things it does today, but with less resources. But for some reason it needs more resources\". And the fact that the developers are still optimizing KDE and making it faster and less resource-hungy, proves that statement. Since KDE is been made faster all the time, it means that currently KDE is consuming more resources than it needs to consume."
    author: "Janne"
  - subject: "Re: Too many resources"
    date: 2005-12-31
    body: "then it is true for almost every application, and a very \"DUH!\" statement for a project as big as KDE."
    author: "superstoned"
  - subject: "Re: Too many resources"
    date: 2005-12-31
    body: "No, it is not. You may think whatever you want about what he meant, but the fact is that he wrote something, and that's not true. Period. As for the rest, sorry, but that's really naive view of things. Do you expect somebody to snap their fingers and magically make everything suddenly perfect? Perfect things don't exist in reality.\n"
    author: "L.Lunak"
  - subject: "Re: Too many resources"
    date: 2006-01-01
    body: "\"Do you expect somebody to snap their fingers and magically make everything suddenly perfect?\"\n\nNo. Where exactly did I say that I'm expecting anything of the sort? The fact that KDE is optimized all the time is a GOOD THING! I have no problems with KDE's performance, but I'll gladly welcome any performance-improvements the devels manage to give us. Yes, KDE could be faster and more efficient. It could consume even less RAM than it does. And if the devels manage to do that, great! But I don't really have any issues with the way things are today."
    author: "Janne"
  - subject: "Just my 0.2 cents. :o)"
    date: 2005-12-30
    body: "I personally agree with some of the technical issues put into the spot light as well as some of the \"usability\" examples mentioned, however, and although Diego, you probably meant well, I think that the text takes a somewhat ...aggressive? ...stand and that can well inspire negative feelings in those that are not KDE users.\n\nThat, on a whole is not a positive thing and doesn't serve anyone. What do you think Diego?"
    author: "Pascal Klein"
  - subject: "Re: Just my 0.2 cents. :o)"
    date: 2005-12-30
    body: "I tried to write an article about why KDE rules. I don't flame other desktops.\n\nYes, I wrote some words about Gnome - but only in the \"FAQ\" section, and I clearly said they're my opinion, and I clearly said I don't \"hate other desktops\". I removed some hard words about gnome and other desktops thanks to some suggestions BTW. What's wrong with saying that the embedded video player in the kde file selector rules, and that I switched to kde because Gnome and others don't have it? That's a fact, nothing else.\n\n\n\nThat said, I'm SURE some people will have negative feelings reading this article. But that's how people behaves, some people can't accept other people's opinion. And some people can't even accept objetive facts. If gnome guys write a example about how great gnome usability is, some kde troll will jump flaming BUT IT DOESN'T HAVE KIO AND KPARTS. Some people is just stupid, and the best thing you can do is to ignore them."
    author: "Diego Calleja"
  - subject: "Re: Just my 0.2 cents. :o)"
    date: 2005-12-30
    body: "haha, i love your english ;-)\n\nanyway, i agree with you - some ppl never change. they never see other ppl prefer other things. and gnome DOES have several advantages - tough imho the disadvantages outrun them anyway (duh, thats why i use KDE).\n\nbut there are still many features you didn't mention. split window in konqueror, drag'n'drop of konqi tabs, autocompletion in every inputfield, to name a few of my personal favorites. ow, and you can re-order and add to the buttons on the windowdecoration (i love the 'always on top' button). and you can give every window a shortcut, and set a universal \"window fullscreen\" shortcut (i use alt-enter for this - its lovely to have this option in ALL applications).\n\nand what about the great search function in kcontrol in KDE 3.5 (yeah it WAS in 3.4 i know)? makes navigating kcontrol much easier...\n\nbtw konqueror supports extentions, just like firefox - for years. quite some extensions are there, like speak-text, translate webpage, auto-reload and a list of pages open when konqi crashed last time.\n\nneed i go on? what about a 'edit toolbar' in EVERY kde app? :D there is so much to discover, one can go on and on.. shift-downkey for smooth scrolling in konqi and kpdf (i NEEEEEED this in kmail and the other kontact apps btw). works great with alt-enter/fullscreen if you want to read articles in kpdf. alt-enter to have it full-screen, make it fit with, shift-down (adjust speed with shift down/up again) and sit down & read.\n\nand what about the scrolling that just WORKS? in windoze, you can only scroll in certain applications without first having to CLICK the scrollbar!!! how stupid... you can't scroll in windows that don't have focus, and you generally can't scroll on the taskbar (volume, tasks), not over tabs, you can't increase text size in word (unless you first click) nor zoom factor (try all this in kword or konqueror). and yes, in KDE horizontal scrollbars scroll too - of course, if you mouse over a scrollbar, shouldn't it scroll? well, KDE is the ONLY desktop environment I know of where this works (but i admit, i only tried Mac OS X and windows XP, not gnome or others).\n\njust some ideas for another article :D"
    author: "superstoned"
  - subject: "Re: Just my 0.2 cents. :o)"
    date: 2005-12-30
    body: "I agree with all the \"likes\" that you mentioned and I'll add a couple favorites:\n\nRight click on maximize makes the window as wide as possible but doesn't change the height.  Middle click on maximize makes the window as tall as possible but doesn't change the width.  I use this all the time.\n\nAnd probably my favorite thing in KDE is the \"magnetic\" windows that align themselves as you drag a window close to another.  I find it very hard (annoying is probably a better word) to use desktops without this feature.\n\nBTW - I had a good time playing with the video preview in the file selector last night.  What never ceases to amaze me is the comprehensiveness of the tools in KDE.  For crying out loud, the file selector is basically a very powerful application in it's own right.  For instance, try resizing the preview pane of the file selector window.  The video preview automatically resizes without missing a beat.  This is a sign of good architecture - when things work like you expect them (hope for them) to.  "
    author: "AlfredNilknarf"
  - subject: "Re: On the video preview"
    date: 2005-12-30
    body: "Somehow the video preview failed, i.e. it shows nothing on videos. Pictures etc work.\nDoes one need to have a media player installed or something else (Kaffeine here; KDE 3.4)?"
    author: "Phase II"
  - subject: "Re: On the video preview"
    date: 2005-12-31
    body: "You need to first install KMplayer:\nhttp://kmplayer.kde.org/\n\nand then the video preview will work. Most distros (SUSE, Redhat, Mandrake, etc) don't install KMplayer by default, and some don't even offer it as a package for some unfathomable reason. "
    author: "Vlad C."
  - subject: "Re: Just my 0.2 cents. :o)"
    date: 2005-12-30
    body: "My own \"KDE fan and proud of it history\"\n\nEarlier this year I was asked to install a FTP app in a customer computer to send files to the main office.\nIt was a RHEL 2.1 with an ancient KDE 2.2 (It was also the server for an office wide app and I wan't allowed to touch it too much).\nI didn't download anything, just open a Konqui window, split it vertically, set the local folder in one side, the ftp folder in the other one, save that layout as a konqui profile and set an icon to open it automatically.\nVoila! Problem solved just with KDE power.\n"
    author: "Shulai"
  - subject: "Nice read..."
    date: 2005-12-30
    body: "...well balanced.\nThanks and all the best to you & everyone else on the dot for 2006!!\n"
    author: "Martin"
  - subject: "Objective"
    date: 2005-12-30
    body: "I found it an objective story, but that might just be my opinion. ;-)"
    author: "Dennie"
  - subject: "One sucky thing in KDE...  kmail!"
    date: 2005-12-30
    body: "No, this is not meant as a troll, but as a cry of despair.  Kmail sucks.  It has great potential - but there is a tiny little problem.\n\nIt's dirt slow on large volumes of email.\n\nI get aprox 800-1000 emails per day through various work-related mailinglists.  Most of them automated.  I've set up rules to filter them into various folders with kmail.  \n\nThis works okay.\n\nThen I'm off for a one week vacation...\n\nBetween six and seven thousand emails await.\n\nIt takes aproximately 60 seconds to fetch and filter 1000 messages.\n\nWhen trying to filter the 6000 messages, it has only chewed through 200 after 15 MINUTES.\n\nThere is something wrong there.  It seems like there's an O(n^2) algorithm in there somewhere.  Someone has pointed at klistviewer or something like that.. whatever -- in all cases, it's horrible.\n\nSomeone has said that it'll be fixed with KDE4.. but that's still a long way off.  It would be _Great_ if someone could do something about this in a 3.x update.  I feel the urge to move back to mutt more and more every day .. and quite frankly that sucks, as I love the KDE gui. :-)\n\n"
    author: "Anonymous"
  - subject: "Re: One sucky thing in KDE...  kmail!"
    date: 2005-12-31
    body: "until now HUGE data cannot be managed by KDE applications easily. Perhaps it was not intended that KDE applications would handle large volume of mails, huge text files, thousands of pages of kword document, etc.,\n\nI hope KDE 4 will be the best Desktop which will handle huge data stuff easily... good luck."
    author: "fast_rizwaan"
  - subject: "Re: One sucky thing in KDE...  kmail!"
    date: 2006-01-02
    body: "This is the most god-awful troll ever.\n\nPlease, back up ANYTHING you just said."
    author: "Joe"
  - subject: "Re: One sucky thing in KDE...  kmail!"
    date: 2006-01-03
    body: "Easy fix. Just move the SPAM handling to the very end of the filtering chain so that it only runs the mail through the spam filter if it doesn't match anything else. And then put a duplicate of the spam hander at the very top of the filter list so that the analyzed spam doesn't go through the entire list again."
    author: "Jason Clinton"
  - subject: "Re: One sucky thing in KDE...  kmail!"
    date: 2006-01-03
    body: "Sorry, i dont get it. Put the same spam filter on top and on bottom of filter list??"
    author: "Mike"
  - subject: "Re: One sucky thing in KDE...  kmail!"
    date: 2006-01-05
    body: "Uh.  Did you read what I wrote?\n\nIf it was spam handling that was the problem - it would take linearly longer time.  It doesn't.\n\n1. message is filtered fast (less than a second).\n10. messages is filtered fast (less than a couple of seconds).\n100. messages is filtered pretty fast (say, ten seconds).\n1000. messages is getting slow - say 3 minutes.\n7000. messages is VERY, VERY slow.  Processing the first 400 took 15 minutes(!)\n\nSo, to filter one week of email - if I've been on a vacation, I basically select the messages in bunches of about 1000 - and filter them in the background while doing other work.  That gets the work done fast enough.  Filtering them by selecting all and then starting to filter just doesn't work fast at all.\n\nThere seems to be an O(n^2) algorithm or something in there, instead of an O(n) algorithm - which it should be.  It's only noticeable on large bunches of email - which I'm unfortunate to get at work.\n\n"
    author: "Anonymous"
  - subject: "Re: One sucky thing in KDE...  kmail!"
    date: 2006-01-08
    body: "Chances are your filters are configured incorrectly. If you use complicated filters & don't set distinct points to stop processing, then your mail can potentially go through the list several times. This *may* be what is wrong. If not, I encourage you to submit a wish for further optimization via http://bugs.kde.org/"
    author: "Zak Jensen"
  - subject: "Konqueror preloading (instances) performance"
    date: 2005-12-31
    body: "Preloading konqueror instances won't really speed up konqi loading.\n\nTest: prelaoding enabled\n-------------------------\n1. click on HOME/konqueror button (on kicker) takes quite few seconds 3-5 seconds\nDoesn't really improve konqi loading... :(\n\nReal Preloading should be (example when preload konquror is disabled)\n---------------\n1. open 1 HOME/konqueror folder\n2. preen ctrl+n (new window)\n3. new window appears in just 1/2 sec. now this is snappy...\n\n"
    author: "fast_rizwaan"
  - subject: "Re: Konqueror preloading (instances) performance"
    date: 2005-12-31
    body: "You're welcome to use your personal voodoo magic skills to make that happen.\n"
    author: "L.Lunak"
  - subject: "Re: Konqueror preloading (instances) performance"
    date: 2005-12-31
    body: "did the voodoo magic ;) it is named \"fvisibility=hidden\" for gcc 3.4.5! really i hope KDE 4 + QT 4 + GCC 4 will be the ultimate Desktop performance..."
    author: "fast_rizwaan"
  - subject: "Re: Konqueror preloading (instances) performance"
    date: 2006-01-01
    body: "well, here it DOES make a difference, esp if my cpu is busy, preloading makes stuff much faster..."
    author: "superstoned"
  - subject: "KDE uses too much resources,  I need a KDE-Beatrix"
    date: 2006-01-01
    body: "I dig the general tone of the article. Every time I tried gnome and KDE, I find myself sticking to KDE after the initial awe for the Gnome desktop. \n\nHowever, KDE uses too many resources, and has way too many subprojects. \n\nI don't see why KDE would not be able to work as well as windows 2000 or windows 98 on my e-mail-internet 128 Mb, 266 Mhz Computer. \n\nThe only recent(?) distribution I found that works well on this hardware was Beatrix: a limited choice of industry standard software in a Gnome desktop. However, every time I install Gnome, after initial awe, I feel frustrated by not being able to do what I want. Even as a total newbie. KDE is able to give more eyecandy, but nothing I tried up to now comes close to Beatrix in being low on resources. Every distribution installs way too much software, users that need them can install themselves. \n\nThe look and feel of KDE is very well, but a lot of the packages (games!) freeze before doing anything. Throw them out of your core package. \n\nMoreover, as I understand it, KDE has its common APIs, should it not make more sense to have NO other dependencies in KDE applications, the way Mac does it? \n\nThanks.\n\n"
    author: "Geert"
  - subject: "Re: KDE uses too much resources,  I need a KDE-Beatrix"
    date: 2006-01-01
    body: "first about performance - why doesn't windows XP run fine on a 368 with 4 mb ram? i think it should, really. windows 1.0 did! why shouldn't XP? maybe it has some features windows 1.0 didn't, and these make things slower? they might consume memory? could it be? naaah, i don't think so...\n\nwell, KDE 3.5 can do more than windowsXP. of course, there are a few things windows XP can do and KDE can't - but there are many many things KDE can do and XP can't. thats natural, as Windows XP is build for 2001, and  KDE 3.5 is a 2005 OS - so it is made for 2005 computers. won't run on your old 368, sorry.\n\nseriously, KDE uses quite some resources. duh. you think it all comes for free, virtual desktops with their own background, custom window settings, window shortcuts, autocomplete and quicksearch everywhere, themabillity, systemwide spellcheck, KIOslaves (netwerk transparancy) etcetera? come on...\n\nOk, KDE isn't as fast as it could be. but thats mostly NOT KDE'S FAULT but the underlying toolchain. the Gnu Compiler GCC isn't really a c++ beast (which happens to be the language KDE is mostly written in). and also the underlying stack isn't as highly optimized for desktop usage as windows XP and Mac OS X. Xorg is far from the most efficient graphical server you'll find etcetera.\n\nKDE just offers a lot more functionallity compared to Gnome or windows 98/2000/XP, and that's what taking cpu and mem. wait for KDE 4, as hopefully GCC will be a bit more mature by then (gcc4 introduced a whole new optimization framework, which hopefully gets utilized a bit more with gcc 4.1/4.2) and the same goes for Xorg (XGL might bring us better hardware accelleration). also Qt4 will bring massive (>20%) decreases in memory usage, and work is being done on other things KDE depends on, like fontconfig (which kind'a sucks now).\n\nAgain, lots of KDE's performance troubles are in the underlying technology, and you wouldn't seriously expect KDE to start rewriting all this, isn't it? its a Desktop Environment, not an Operating System... it would be a waste of time and effort, sorry. (yes, this is meant to answer your last question)..."
    author: "superstoned"
  - subject: "Re: KDE uses too much resources,  I need a KDE-Bea"
    date: 2006-01-01
    body: "You don't have to rewrite it, just put the dependencies of programs that are not already in KDE in the package itself. If this is not possible with the current package management, than it might be a good idea to write one where it is possible. "
    author: "Geert"
  - subject: "Resources and windows"
    date: 2006-01-01
    body: "Yes it uses to many resources! Thats really true! Try run KDE on a 1GHz Pentium or similar machine and open a konsole. This takes around 10 seconds to start. What happens in this 10 seconds just to open a terminal? I think the developers should take more time on code optimization, especially optimization for speed and reliability. I get at least one kde crash a day. Just start a fvwm2 on your machine or try to install a old linux distribution from around kernel 2.2 and you can see how match faster this ones working on actual hardware.\n\nOn the other side kde misses a lot of features which are usual on windows desktops. Who want's diffrent desktop designs or even change the startup screen? THAT DOESN'T MATTER! But a function to arrange the icons on the desktop along a raster without bringing them all disordered would be helpful. Or the possibility to make the kicker wider than 256 pixels (I have a 3 monitor system (matrox parhelia) and the kicker upwards on the left side). Or PLEASE PLEASE this view/viewmode menu to a arrangement which is more user friendly. Windows for sample has it direct in the menu and not hidden in a submenu. About the bottom line try to hire a professional gui designer to optimize this menu chaos in konquerror and the other design bugs in kde.  What does it helps when konquerror supports multiple view profiles but no editor to customize it. Does the developer really think a average user goes to edit this text file (if he can find it)?\n\nTHAN the whole multimedia apps in kde. It mostly remembers me on the first windows media player and also have in common with it (the wmp) that it crashes all the time and sometimes it plays files and sometimes not.\n\nOn priciple i think the main developpers should install a second windows machine just to see how it could work. And then try to get a similar result on kde. Microsoft copied all their technologies from other ones. It's now time that the other ones copies a bit from ms.\n"
    author: "Roland Kaeser"
  - subject: "Re: Resources and windows"
    date: 2006-01-01
    body: "> This takes around 10 seconds to start. \n> What happens in this 10 seconds just to open a terminal?\n\nWhy do i get konsole open under 1 second? I have KDE 3.5.\nIn KDE 3.1.x series Konsole did take long time to open, about 3-5 seconds. 3.2.x series it gt much better times but 3.4.x series really blowed speed up so there is   not almoust any delay."
    author: "Fri13"
  - subject: "Re: Resources and windows"
    date: 2006-01-01
    body: "sorry, but WHAT are you talking about? you can arrange icons in 1000 ways, including every way windows could do it...\n\nprofiles? editor? what would you need an editor for? change your setup and save profile, thats what a profile is for... duh... every moron could use it, sorry...\n\nand they hid the window things in a submenu because nobody used it and the menu became too crowded. good choice, imho!\n\nand i already answered the performance moan above your post - to summarize:\n1. don't expect 2005 Desktop Environment with the performance of an 1998 OS, that's stupid.\n2. Lots of KDE's performance problems aren't KDE's fault, but due to the underlying stack of applications and systems - kernel, Xorg, libraries, C++ compiler peformance.\n\nHard work is being done on all these things - most KDE performance improvements are actually improvements in underlying structures, made in cooperation with KDE devs. Qt4/KDE4 will bring is at least 20% less memory usage, Hardware Accellerated display (Xgl) and a huge cleanup of the libraries, most probably increasing the 20% less memory to maybe even 30 or 40% (maybe a bit optimistic, but hey, one can dream?).\n\nyou're right on the multimedia track, but again - its mostly NOT kde's fault. there simply is NO default multimedia framework under linux, and KDE can't write its own (as it would just clash with the others). we'll just have to wait for gstreamer and the like to get stuff finally working well.\n\nthe dev's are working on usabillity, but again - there is nothing windows does better than KDE on this plane. c'mon, only because it has lots less features makes it look cleaner. you know those lovely config dialogues with several LAYERS of tabs in windows? impossible to navigate? ever seen that in KDE? sorry, but KDE can do 1000x what winXP can, and still isn't much more crowded (Konqueror might be the exception here, it is quite a bit more crowded compared to Explorer, but it can do much much more so i can forgive it)."
    author: "superstoned"
  - subject: "Re: Resources and windows"
    date: 2006-01-01
    body: "You are dealing with a severe error in configuration!  You *might* have a network issue; in some cases, I've seen misconfigured DNS produce these symptoms.  You also need to make sure your binaries are <A HREF=\"http://www.gentoo.org/doc/en/prelink-howto.xml\">prelinked</A>, your drivers aren't horribly off, and that you don't have a mangled and bloated font directory setup.  For some setups, disabling arTs and relying on hardware sound mixing helps a lot.  Seriously, I get nearly instant response from my hotkey bound to Konsole on my older Athlon 800.\n\nWhen I installed Linux on my laptop, I went through several iterations of window managers and environments, trying to get the leanest setup I could.  By the time I had all my regular applications loaded - FireFox especially, along with others - I realized that KDE would have reused its libraries more, been just as spry, and taken up no great amount of resources considering all the extra functionality it offered.  It's worth the time to get it right."
    author: "D Rollings"
  - subject: "Re: Resources and windows"
    date: 2006-01-02
    body: ">Yes it uses to many resources! Thats really true! Try run KDE on a 1GHz Pentium or similar machine and open a konsole. This takes around 10 seconds to start\n\n\nHmm, sounds like a configuration problem.\ni installed kde on several +/- 1 ghz machines with 128 mb ram or more, and konsole starts in just a second on all machines...\n"
    author: "rinse"
  - subject: "Re: Resources and windows"
    date: 2006-01-02
    body: "> This takes around 10 seconds to start.\n\nI'm working on an 1.2 GHz Intel with 256 Mb atm....\nI did not open konsole before (so no caching whatsoever).\nKonsole opens instantly. No way to measure this, as it's clearly less than a second. (KDE 3.5 on archlinux noodle)\n"
    author: "Thomas"
  - subject: "Re: Resources and windows"
    date: 2006-01-03
    body: "No, please don't do it. Dont feed the troll!\n\n...\n\nAhhh, too late :("
    author: "Mike"
  - subject: "Spell checker"
    date: 2006-01-01
    body: "\"The spell checker works everywhere inside KDE BTW.\"\n\nThen why didn't the author use it?? ;-) Or is it not so easy to use?\n\nDiego, I realize that English is not your first language, but wouldn't that make one even more inclined to use the spell checker? (Btw, your English is vastly better than my Spanish could ever be! Please don't take this criticism too harshly.) I guess my point is that any article will gain in readability and credibility from a bit of language checking/polish.\n"
    author: "Francis Burton"
  - subject: "Re: Spell checker"
    date: 2006-01-01
    body: "When we are forced as non-native speakers to use English language we shall enjoy our freedom to slaughter a language. :-)\n\nEnglish as a common means of communication will result in modifications of English. Language is a social tool and we take over control of that tool. There is no right or wrong English as language is under permanent development. \n\nFeel free to offer checking ressources if you are not pleased with it. Otherwise let the people just write how they want. As long as everybody understands what their text is about forget about the standards of your highschool English teacher."
    author: "gerd"
  - subject: "Re: Spell checker"
    date: 2006-01-02
    body: "Wow, yeah, this is a dumb statement by an obvious slacker who just wants to slide with sloppy language skills."
    author: "Joe"
  - subject: "Re: Spell checker"
    date: 2006-01-02
    body: "> slide with sloppy language skills.\ngrmblpf... (untwisting tongue)"
    author: "Thomas"
  - subject: "Re: Spell checker"
    date: 2006-01-02
    body: "\"obvious slacker\" ...ahemm\n\nI was 100% serious. \nYou insult people for no reason. \n\n"
    author: "Gert"
  - subject: "Re: Spell checker"
    date: 2006-01-03
    body: "Of course people are free to write as they please. That's how it should be. But if there is no right or wrong English, surely spellcheckers are redundant and therefore deserve not to be promoted. And while we are at it, we can get rid of highschool English teachers too! :-)\n"
    author: "Francis Burton"
---
KDE convert Diego Calleja explains <a href="http://www.terra.es/personal/diegocg/kde/index2.html">Why KDE Rules</a> by showing off some of its power features.  He starts by dismissing some myths about KDE then tells us about the application that brought him to KDE, <a href="http://amarok.kde.org">amaroK</a>.  Power features explained include KParts, DCOP and KIOSlaves.  "<em>I wrote this document to tell everybody why KDE is great, why it's worth using (great functionality), supporting (great development platform) and hacking (great design) and why you can expect many other awesome features from KDE 4."</em>




<!--break-->
