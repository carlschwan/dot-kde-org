---
title: "Aaron Seigo on the Upcoming OSDW in San Diego"
date:    2005-09-25
authors:
  - "manyoso"
slug:    aaron-seigo-upcoming-osdw-san-diego
comments:
  - subject: "Irony.."
    date: 2005-09-25
    body: "\"an interview with Aaron Siego\" - you probably want to correct his name :-)"
    author: "Michael Jahn"
  - subject: "Re: Irony.."
    date: 2005-09-26
    body: "...and while you're at it:  San Deigo -> San Diego "
    author: "cm"
  - subject: "Re: Irony.."
    date: 2005-09-26
    body: "I've long wondered how his last name is pronounced. Anyone?"
    author: "AC"
  - subject: "Re: Irony.."
    date: 2005-09-26
    body: "\"psycho\" ;)"
    author: "bsander"
  - subject: "Re: Irony.."
    date: 2005-09-27
    body: "it isn't that bad I hope :-)"
    author: "Boemer"
  - subject: "Re: Irony.."
    date: 2005-09-29
    body: "That i before e rule is just a load of BS apparently."
    author: "Ian Monroe"
---
The upcoming <a href="http://www.osdw.org">Open Source Desktop Workshop</a> in San Deigo is fast approaching.  Slides are being prepared, tutorials developed and <a href="http://www.osdw.org/cms/1011">registration is filling up fast</a>. <a href="http://www.osnews.com/comment.php?news_id=11988">OSNews</a> points to <a href="http://kubuntu.org/~jr/osdw.html">an interview with Aaron Seigo</a> by Wade Olsen about expectations for the upcoming inaugural event.  Find out if KDE will be visiting any more world renowned beaches in the months ahead.




<!--break-->
