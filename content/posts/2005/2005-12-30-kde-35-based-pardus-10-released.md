---
title: "KDE 3.5 Based Pardus 1.0 Released"
date:    2005-12-30
authors:
  - "bmetin"
slug:    kde-35-based-pardus-10-released
comments:
  - subject: "Go Pardus, go!"
    date: 2005-12-30
    body: "Pardus has been a real success after a long development period. The evolutionary approach in its underlying infrastructure is interesting and promising. Starting from 1.1, we will be looking for interested developers - after documenting the major parts, i.e PiSi, \u00c7OMAR and YALI. "
    author: "G\u00f6rkem \u00c7etin"
  - subject: "Good News!!! "
    date: 2005-12-30
    body: "I'm so glad to hear that at least some distro is using KDE based installer and package manager.\n\ncongrats! and Happy New Year!!!"
    author: "fast_rizwaan"
  - subject: "Re: Good News!!! "
    date: 2005-12-30
    body: "hey, what about suse? yast is all Qt... :D"
    author: "superstoned"
  - subject: "Re: Good News!!! "
    date: 2005-12-30
    body: "Yep, and the KControl integration is the cherry on top of the cake. ;)"
    author: "blacksheep"
  - subject: "Re: Good News!!! "
    date: 2005-12-30
    body: "Yes, but the question is why gui administration backends cannot be unified. The only way to do it is to integrate it into KDE so vendors need to privide interfaces. YaST is open source GPL. Hope it will get fully ported to Debian."
    author: "gerd"
  - subject: "Re: Good News!!! "
    date: 2005-12-31
    body: "> The only way to do it is to integrate it into KDE so vendors\n> need to privide interfaces.\n\nThat's false.\nFirst of all, YaST has been close software for quite a long time, only recently was GPL'd, and I remember the said integration at least since SuSE 7.0.\n\nThe thing is that the KDE libraries are under the LGPL. Therefore you CAN link closed software to them. You just need to pay Trolltech for the Qt commercial license."
    author: "blacksheep"
  - subject: "Re: Good News!!! "
    date: 2005-12-31
    body: "Non, in contrary to what you said YaST was never closed software but has been available with sources all the time. However before it was licensed under GPL it had a clause that you may only spread it in non-commercial ways and have to get code changes back to SuSE."
    author: "ac"
  - subject: "Re: Good News!!! "
    date: 2005-12-31
    body: "Okay. s/\"close source\"/\"not GPL compatible\"/g\nMy point remains."
    author: "blacksheep"
  - subject: "Re: Good News!!! "
    date: 2006-01-01
    body: "...more than one year ago\n\nvs.\n\n...only recently\n\nYaST was always \"open Source\".\n\nNow it is GPL. Time to integrate it and ship YaST backend components with KDE as default. \n\nWe have basically three - four important Desktop Linux families\n* Redhat\n* Suse/Novell with YaST\n* Mandrake\n* Debian\n\nThe user does not care whether configuration stuff is distribtion or desktop environment related. When I have a problem with my mouse I want to change the configuration. Both YaST and KControl modules are relevant here. We need only one module. \n\nThe Desktop Environment, here KDE, has the possibility to dictate the configuration backend. A power that has to be used because Distributors are unable to make the necessary changes on their own to get compatibility with other distributions."
    author: "gerd"
  - subject: "Re: Good News!!! "
    date: 2006-01-01
    body: "> yast is all Qt\n\nNo. The Qt frontend is, not more."
    author: "Anonymous"
  - subject: "Screenshots"
    date: 2005-12-31
    body: "For some screenshots, check Caglar's web page at http://cekirdek.uludag.org.tr/~caglar/pardus"
    author: "G\u00f6rkem \u00c7etin"
  - subject: "New Installer? New Package Manager?"
    date: 2006-01-11
    body: "Looks nice, but for more information:\nhttp://en.wikipedia.org/wiki/Reinventing_the_wheel"
    author: "Anonymous Coward"
  - subject: "Great Linux"
    date: 2006-02-17
    body: "This is a very good Linux distribution. I think it deserves atleast one test by everyone."
    author: "exe"
---
Turkish distribution <a href="http://www.uludag.org.tr/eng/">Pardus</a>, one of the first GNU/Linux distributions to feature KDE 3.5 as its desktop, has announced its first stable release.  All Pardus specific desktop applications, including the installer and package manager are developed using the powerful KDE and Qt libraries. KDE was chosen to be the default desktop environment for its extendability and close integration of applications.  Pardus is funded and developed by the <a href="http://www.uekae.tubitak.gov.tr/en/">Scientific &amp; Technological Research Council of Turkey</a> and expects to make some major KDE rollouts in the Turkish public sector in 2006.







<!--break-->
