---
title: "KDE to be Present at Wikimania 2005"
date:    2005-08-02
authors:
  - "trahn"
slug:    kde-be-present-wikimania-2005
comments:
  - subject: "wiki and kde"
    date: 2005-08-02
    body: "i want to point out the integration AMAROK<->WIKI , very nice ! (info for artists,albums and more) ...\n\n"
    author: "ch"
  - subject: "(hidden)"
    date: 2005-08-03
    body: "<i>(See HTML source for full thread)</i></td>\n<!--\nStatus: KDE Dot News Abuser\nTitle: GNOME the better Desktop\n\nWhy do the Wiki people keep supporting an overblown bloated desktop based on a commercial toolkit that developers need to pay for? GNOME is the only way to go and GNOME is the desktop that should be mentioned by Wiki. It's free as in free beer all companies support GNOME, nothing supports KDE. KDE will be dead in the next couple of years because no one will use it.\n-->\n<!--\n\n\n\n\n\n"
    author: "youknowmewell"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "So, why do you come to dot.kde.org? Why are you so interested in KDE news site? I don't even know of any GNOME news sites .... =)"
    author: "Kanwar"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "To be fair: http://news.gnome.org/\n\nBut your point stands... stop coming here trying to start flame wars (please) :)\n\nL."
    author: "ltmon"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "you troll? GNOME is only american style Desktop. GNOME will must be evil Desktop because use U.S.A. President Bush like stupid American."
    author: "kder"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "Well I for once can't live without the superb KDE framework which makes fish:, sftp:, smb:, ftp: etc. etc. a breeze to work with.\n\nFurther more I find the GNOME filehandler window (still found ind firefox, eclipse etc.) way to crappy compared to that of KDE which gives power to the people !!!\n\nSo please - don't tell me that GNOME is better than KDE..."
    author: "slott_hansen"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "Could someone delete this thread? It is very tiresome. Describing it as a troll dignifies it far too much.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "yes please delete it. gnome and kde are friends"
    author: "ch"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "> GNOME is the only way to go.\n\nWell, no. There is KDE, GNOME, Enlightenment, XFCE, Equinox, Rox, ... No monopolies, please. Let's work on standards, interoperation. Those are the only way to go."
    author: "Free Software user"
  - subject: "Re: GNOME the better Desktop"
    date: 2005-08-03
    body: "<!--\nIgnoring the rest of your comment and just replying to the question: Wikimedia continues to work with many other projects and entities.  Part of the statement upon the announcement of the APIs was that they hoped they are reusable by other projects, and that could certainly include GNOME.  So: KDE first because of happenstance.  The Wikimedia Foundation has no particular official preference between other free software projects.\n-->\n</tr>\n<tr><td>\n<table>\n<tr><td>\n\n"
    author: "DanKeshet"
  - subject: "Wiki integration"
    date: 2005-08-03
    body: "What could the Wiki cooperation bring along to KDE apps? I personally already use \"wiki:\" in Konqueror and Amarok will be integrated with Wiki, but what else is there that could benefit from Wiki data? I'm just curious.\n\n"
    author: "Chakie"
  - subject: "Re: Wiki integration"
    date: 2005-08-03
    body: "The most obvious chance for integration is KDE-EDU where scientific software like Kalzium and KStars can benefit from glossaries and articles. Apart from that we have some more ideas which would integrate Wikipedia with our framework in a more highlevel way. Stay tuned :-)"
    author: "Torsten Rahn"
  - subject: "Re: Wiki integration"
    date: 2005-08-03
    body: "Well (for me as a Wikipedia author) there comes a lot into my mind.\n\n1: KStars:\nIn KStars you can right-click on the single elements like stars, planets asteroids, globular clusters, galaxies and so forth. Currently you can directly get a photo of the object directly from photo galleries in the net but no text information. So it would be cool to embedd the thousands of Wikipedia articles to astronomical objects directly into KStars. This is a killer feature no star program currently provides. Have a look at KStars it IMHO the hidden star in kde-edu.\n\n2: Kalzium:\n* The same applies to articles about chemistry if they get ebmedded in Kalzium.\n\n3: AmaroK:\n* Information about artists (this currently works in AmaroK). If you have one time used this feature in AmaroK you will miss it in other players.\n\n4: Knowledge:\n* Knowlede is a new KDE offline Wikipedia reader program by Danimo. As an offline version of Wikipedia is very much (!) needed and the current German Wikipdia DVD was a very big sucess this is an important app for the coming KDE 4.\n\nI'm sure there are a plenty of other benefits as well. So I'm sure we guys at Wikimania will have a lot of things to do at Wikimania regarding KDE-Wikipedia integration.\n\nSee you tomorrow at Wikimania. ~~~~ ;-)\n"
    author: "Arnomane"
  - subject: "what for"
    date: 2005-08-03
    body: "kalzium, kstars, other kdeedu apps (kgeography), sidebar for Konq, KNode"
    author: "m."
  - subject: "Wiki was mentiont on TV in Germany"
    date: 2005-08-03
    body: "tonight I saw a report on the major TV Station (ARD) in the \"heute Jurnal\" (News Programm) about Wiki. Thex diden't say anything about KDE but still nice.\n\ngreetings,\nHermann"
    author: "Hermann"
---
<a href="http://wikimania.wikimedia.org/">Wikimania 2005</a>, the first international <a href="http://wikimedia.org/">Wikimedia</a> conference, will be held in
Frankfurt am Main, Germany, starting next Thursday and finishing on Monday.
Following the <a href="http://dot.kde.org/1119552379/">announcement</a> of the
Wikipedia-KDE Cooperation the KDE Project has been <a
href="http://lists.kde.org/?l=kde-edu-devel&m=112030715606527&w=2">
invited by Wikipedia</a> to join Wikimania 2005.


<!--break-->
