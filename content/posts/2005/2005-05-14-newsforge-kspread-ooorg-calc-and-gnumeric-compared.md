---
title: "NewsForge: KSpread, OO.org Calc and Gnumeric Compared"
date:    2005-05-14
authors:
  - "tkautto"
slug:    newsforge-kspread-ooorg-calc-and-gnumeric-compared
comments:
  - subject: "all have problems"
    date: 2005-05-14
    body: "Here is some anecdotal material.\n\nYesterday I was trying to export a spreadsheet to a text file with tab-delimited fields and no quotes. I thought this would be trivial, but alas. I was performing this task on a new SuSE 9.3 installation with the OpenOffice 2 beta (novellized version). This version has a nice export dialog where one can select the delimiter and the quotation symbol. Unfortunately, no file was written to the disk and no error message was given. I played around with this for a bit and decided to try out koffice's spreadsheet.\n\nThis didn't fare much better. KSpread (1.3.5) has the option to save to multiple sheets to one file. No matter what the value of this option is, all sheets are exported always. Also it is not possible to not have quotation marks in the output the only possible values for the quotation mark are ' and \", contrary to OpenOffice 2 where and character can by used.\n\nNext in line was gnumeric (1.4.3). I fired up YaST again to install it and opened my spreadsheet file. But again, a problem stopped me from exporting the file. Gnumeric does not allow one to set any options for exporting CVS files!\n\nSo in the end, I copied and pasted the data from OpenOffice into Kate with worked very well.\n\nFazit: The versions of the spreadsheet programs shipped with SuSE 9.3 are not able to perform the very simple, common task of exporting one spreadsheet to a tab delimited text file.\n"
    author: "jos"
  - subject: "Re: all have problems"
    date: 2005-05-14
    body: "Just out of curiousity: what are reasons for wanting other quotation marks in a CSV file?\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: all have problems"
    date: 2005-05-14
    body: "You would want other quotation marks so your exported document will work with other tools that expect a different quotation mark.\n\nIt is also worth noting that KSpread does not properly export or import CSV files.  Specifically, it does not properly encode quotes.  For example, if I have a field with:\n\nJoe \"crazy, crazy, guy\" Schmoe, Ph.D.\n\nKSpread will not store this field in a manner that will allow any tool to automatically recognize that it is a single field.  The CSV file should be encoded as:\n\n\"Joe \"\"crazy, crazy, guy\"\" Schmoe, Ph.D.\"\n\nBut KSpread encodes it as:\n\n\"Joe \"crazy, crazy, guy\" Schmoe, Ph.D.\"\n\nIf you import it back into KSpread it will come out as three fields:\n\nJoe\ncrazy\nSchmoe, Ph.D.\n\nNote that some of the data actually disappears.  It is worth noting that if I put the extra quotes in, KSpread opens the file correctly.  Gnumeric will read it as three slightly different fields:\n\nJoe crazy\ncrazy\nguy Schmoe, Ph.D.\n\nWhich is an accurate representation of the text file.  But of course, the CSV file itself is wrong.\n"
    author: "Zippy"
  - subject: "Re: all have problems"
    date: 2005-05-14
    body: "As a matter of fact either you're wrong or your example does not represent what you meant.\n\nJoe \"crazy, crazy guy\" Schmoe, Ph.D.\n\nshould be read as two fields:\n\nJoe \"crazy, crazy guy\" Schmoe\nPh.D.\n\nThis for the reason that anything between quotes should be handled as a single string, no matter what it contains. The purpose of such a behaviour is to allow arbitrary description strings to be included in a table (e.g. book titles, various descriptions of the listed items).\n\nKSpread 1.3.5 reads it incorrectly as \n\ncrazy, crazy guy\nPh.D.\n\nMoreover, it should handle quotes and apostrophes identically. However, it interprets the string\n\nJoe 'crazy, crazy guy' Schmoe, Ph.D.\n\nas three items:\n\nJoe 'crazy, \ncrazy guy' Schmoe\nPh.D.\n\nWhile this is what you want, it's still incorrect. The trick is to appropriately set the text delimiter in the import dialog, so that it won't match the delimiter you used. "
    author: "Obviously anonymous"
  - subject: "Re: all have problems"
    date: 2005-05-14
    body: "Yes, that's true, although Openoffice 1.1 served best at this point. But imagine you need to import data from a text file..."
    author: "Sebastian"
  - subject: "Re: all have problems"
    date: 2005-05-14
    body: "In Gnumeric, just save with the filetype name \"Text Export (Configurable).\"  This will give you a dialog with many options for exporting to CSV."
    author: "Brad"
  - subject: "Re: all have problems"
    date: 2005-05-14
    body: "Thanks for the tip! I'll try that next time.\n\n"
    author: "jos"
  - subject: "KOffice 1.4"
    date: 2005-05-14
    body: "What a pity that they tested KSpread of KOffice 1.3.5. KOffice 1.4 has much better charting facilities thanks to Inge Wallin. Plus the other improvements which were made in 1.4."
    author: "Bram Schoenmakers"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-14
    body: "1.4 is beta"
    author: "Gerd"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-14
    body: "Yes, but they used OpenOffice.org Calc beta build 1.9.83 too\n\nSo, it would have been nice if they had used KOffice 1.4 beta too."
    author: "tim beaulen"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-14
    body: "OpenOffice.org 2 too"
    author: "TF"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-14
    body: "but 1.1.4 is obsolete and 2.0 quite stable"
    author: "Gerd"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-14
    body: "Same thing goes for KOffice 1.4"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-15
    body: "> 1.1.4 is obsolete\n\nGiven the number of people using 1.1.4 in their daily work (including myself), I would definitely not say that it's obsolete.\n\nLet us all stick to reality here, ok?"
    author: "KDE User"
  - subject: "Re: KOffice 1.4"
    date: 2005-05-16
    body: "Wow, thanks!\n\nUnfortunately, that is not really true.  My main contributions do not come in the form of new features (although there are some: the new data editor, the row/column stuff, for instance).  Instead, I have mostly cut away non-working stuff and removed bugs.\n\nWe are currently down to 2 bugs in bugzilla now and with a little luck KChart could be out for 1.4 with no known bugs (yeah, right!)."
    author: "Inge Wallin"
  - subject: "Well, in terms of charting the review is very poor"
    date: 2005-05-14
    body: "I was trying to make some charts the other day and tried also the three tools. In the end only Gnumeric was able to do what I wanted. Ans what I wanted was very simple.\n\nI wanted to make a chart and then be able to edit the chart and add/remove data.\n\nKspread doess't allow this yes.\nOpenOffice (the lates beta) didn't allow me to edit th chart data. No matter what I did, the \"Chart Dada\" button/option was always disabled.\n\nIn the end, Gnumeric was the only too that allowed that.\n\nAftrer this, I needed to make a document with some tables from that spreadsheet, and I ended saving the document from Gnumeric in Office format, opening it with OpenOffice and then exporting the tables, to be able to make the document has I wanted.\n\nIndeed, each has it's own stength, and I'd really love if each was able to also do what the others do..."
    author: "Hugo Costelha"
  - subject: "Re: Well, in terms of charting the review is very poor"
    date: 2005-05-16
    body: "Yeah, that sucks.\n\nWhen you use KChart as a stand-alone application you can change data as much as you like.  You can add or delete rows or columns, and you can change your data points.\n\nBut when you embed KChart in KSpread, you use the spreadsheet as your data source and you cannot change the data from within KChart.  That is all good and well, but the problem is that you cannot change the area in KSpread that the chart is drawn from.  I really, really wanted to add that feature to 1.4, but I just didn't have time.  Sorry.\n\nThis is one of the things that I will implement first of all after the release. However, it has to be done in KSpread, not in KChart."
    author: "Inge Wallin"
---
<a href="http://www.newsforge.com/">NewsForge</a>'s contributor <a href="http://members.axion.net/~bbyfield/">Bruce Byfield</a> wrote a <a href="http://software.newsforge.com/article.pl?sid=05/05/05/1538249&tid=152&tid=93">hands-on comparison</a> between <a href="http://www.koffice.org/kspread/">KSpread</a> 1.3.5, <a href="http://www.openoffice.org/">OpenOffice.org Calc</a> beta build 1.9.83 and <a href="http://www.gnome.org/projects/gnumeric/">Gnumeric</a> 1.4.3-6. Seems like KSpread is doing pretty well - the charting capabilities were second to none, and reviewer mentioned KSpread to be a good choice for people making visual presentations. KSpread still needs love and care as handling lists was found to be limited.

<!--break-->
