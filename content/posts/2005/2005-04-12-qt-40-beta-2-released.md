---
title: "Qt 4.0 Beta 2 Released"
date:    2005-04-12
authors:
  - "binner"
slug:    qt-40-beta-2-released
comments:
  - subject: "What exactly has changed since version 3.3.X?"
    date: 2005-04-12
    body: "Now I know that QT4 has been making news occasionally for the past few months, but I am still curious as to what exactly is and will be new and improved with it. I have only found a few websites that discuss some base technologies but do not go into any depth as to what this will mean for the end user.\n\nAre there performance improvements, rendering improvements, accessibility improvements or anything else of the sort that one should expect? Are there any before and after screenshots that might illustrate such a thing?"
    author: "Anonymous John"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-12
    body: "Here ya go.\n\nhttp://doc.trolltech.com/4.0/qt4-intro.html"
    author: "am"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-13
    body: "Quoting the link:\n\n----8<------\n\nThe Tulip containers support the foreach keyword, a Qt-specific addition to the C++ language that is implemented using the standard C++ preprocessor. The syntax is:\n\nforeach (variable, container)\nstatement;\n\n---->8------\n\nWhoa, I know they extended the language before with 'emit' and stuff, but this change now, even if minor in practice, has quite an impact in the \"feel\" of the language. Trolltech seems to be really determined to put an end to the remaining suckiness in C++. Expect garbage collection in Qt 5.\n"
    author: "Hisham"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-13
    body: "How did you get that \"scissors-effect\" in your post???  That's neat!"
    author: "ac"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-13
    body: "Hmmm... seems to me that doing:\n\ntemplate <typename Iterator>\nvoid foo(Iterator iter) { statement }\nstd::for_each(container.begin(), container.end(), foo);\n\nreally isn't that much harder, let alone the stuff you can do with boost::lambda.\n\nIs it really worth it to screw with C++ for these fairly minor cosmetic changes?"
    author: "Christopher Smith"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-13
    body: "> Is it really worth it to screw with C++ for these fairly minor cosmetic changes?\n\nIt is. Try it and you will love it. The foreach is highly addictive."
    author: "Cornelius Schumacher"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-12
    body: ">Are there performance improvements, rendering improvements, accessibility >improvements or anything else of the sort that one should expect?\n\nYes to all of them. Example: Text-To-Speech (KTS) is now also working for a menustructure (was not possible in Qt3). For a developer a lot things changed for the enduser not to much. But of course: If you can code better apps with Qt4 compared with Qt3 this will be an \"effect\" for the enduser.\nQt is a lib which is used by developers. It's up the the developers to make use of it."
    author: "anon"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-12
    body: "> Are there performance improvements, rendering improvements, accessibility improvements or anything else of the sort that one should expect?\n\nYes, yes, yes, all of that and more. :-)"
    author: "Anonymous"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-12
    body: "Please note it's correctly spelled Qt (\"cute\"), not QT (\"QuickTime\")."
    author: "ac"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-12
    body: "I've always spelled it Qt (\"cutie\")\n      Paul.\n"
    author: "Paul Koshevoy"
  - subject: "Re: What exactly has changed since version 3.3.X?"
    date: 2005-04-13
    body: "Don't be so picky. Get a life."
    author: "Jim Paquette"
  - subject: "KDE preparation"
    date: 2005-04-12
    body: "As the final release date for Qt 4 approaches, how is the KDE organization preparing to set up its developers for success? \n\nAny groups (APPEAL-like) that will promote the relase and educate above the Qt documentation?\n\nA clear message at aKademy (if not already planned) on coding/porting/migrating would be good.\n\nWhere can people, like myself, go to help?"
    author: "Frank Dux"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "Is there a need to add something above the Qt documentation? I think it's very good."
    author: "Anonymous"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "Frankly, I can do without being \"set up\" for success by any organization. It sounds like a painful experience, if not something that belongs to the corporate world.\n\nThe most helpful thing would be to offer to help one project (and I don't object to it being Krita) remove all Qt 3'isms from the code. The change away from QValuePtr (if I remember that correctly) alone is going to take a few days...\n"
    author: "Boudewijn"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "For the people who do want to help out with this, the following url might be very helpful too:\nhttp://www.trolltech.com/products/qt/readyforqt4.html"
    author: "Tim Beaulen"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "\"Frankly, I can do without being \"set up\" for success by any organization.\"\n\nSo, is your implication that if you don't need it, no one else does?  That might be the case if you're the worst programmer.  \n\nHow self-deprecating of you.\n\nBut if that's not your message, then accept that some may find Qt's documentation insufficient as it applies to KDE, some may have trouble developing a task list of changes, and some may have trouble recruiting help over the next 12 months if they can't direct new resources to said information.\n\nThis change is more about collaboration than about find/replace and method signatures.  If it were otherwise, then this announcement wouldn't have even had the pertinence to be posted on the dot."
    author: "Frank Dux"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "I think he just disliked your manager talk.  Who on earth speaks like that in real life, other than a manager?  'set up for success'.  Heh."
    author: "Anon"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "Exactly."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "i agree. let's chase away all the managers. we don't want them.</sarcasm> ;-P"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE preparation"
    date: 2005-04-13
    body: "I certainly don't want managers nor do I want to be managed. This is strictly an amateur performance for me, done for fun.\n\nI'm not a college kid anymore. I don't have any need to play enterprise in my free time to feel grown-up. If I have to submit in my leisure time to the same inane wordwooze here as I have to in my job, I'll jolly well quit."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE preparation"
    date: 2005-04-13
    body: "lol ... managers aren't contagious ... they can come near and you won't catch some horrid disease.\n\nthere's a fine line between not having \"anti-open-source\" structured project management and being hostile to anyone who comes from a management background who comes around to look at the project.\n\nactually, no, there isn't a fine line. there's a big huge line that ought to be fairly obvious.\n\nwhen someone asks a question and does so using managerese they may just be communicating in the way that is most comfortable to them. their goal may simply be to get some information, not manage you.\n\nhave you ever played with one of those \"siamese fighting fish\" that flare up whenever they see anything that might be coming into their territory? they are rather cute."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE preparation"
    date: 2005-04-13
    body: "So, basically you say I'm a siamese fighting fish who is missing the obvious for poking fun at someone who's using language that would be right at home in a Dilbert cartoon?\n\nFrankly, I'll do all I can to keep that nefarious contagious disease away from my leisure time. You never know... Before you realize it you start tagging \"going forward\" onto the natural ending of your sentences yourself, and then it's too late. Your hair will part in the middle, your beard will wither in your chin and your tie won't uninstall anymore. But don't tell me I didn't warn you.\n"
    author: "Boudewijn Rempt"
  - subject: "Re: KDE preparation"
    date: 2005-04-13
    body: "\"So, basically you say I'm a siamese fighting fish who is missing the obvious for poking fun at someone who's using language that would be right at home in a Dilbert cartoon?\"\n\nEither that, or he's trying to pick you up. >;-)"
    author: "Anonymous Custard"
  - subject: "Re: KDE preparation"
    date: 2005-04-13
    body: "Reminds me of a story.\n\nA co-worker has a relative who has worked for years for a firm. Competent, designed many of the systems that keep the place running.\n\nHe got to work one morning, found his desk against the wall and two or three chairs in his office. Some eager manager type walked in, explained that this was done to encourage team work, collaboration and open exchange of ideas. People are encouraged to walk in and discuss things. He moved his desk to where it was, put the chairs in the hallway, escorted the eager manager type out by his arm and said (I'm paraphrasing) 'Get out. I've got work to do'.\n\nMy kind of man.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: KDE preparation"
    date: 2005-04-15
    body: "While I'm the first person to laugh at managers, it's a shame that the programmer didn't give it a try.  Of course it depends on the situation etc etc, but it doesn't sound to me to be that bad an idea."
    author: "JohnFlux"
  - subject: "Re: KDE preparation"
    date: 2005-04-16
    body: "What a fool. (The 'Get out. I've got work to do'-guy)."
    author: "MaXe"
  - subject: "Re: KDE preparation"
    date: 2005-04-13
    body: "/agreed"
    author: "Dan Ostrowski"
  - subject: "Re: KDE preparation"
    date: 2005-04-12
    body: "Well, there's already some stuff going on: http://www.kdedevelopers.org/node/view/936 for example.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "So..."
    date: 2005-04-12
    body: "...what is the plan now?\nReading the cvs mailing list seems that things are quite stagnant right now. No big changes are done, which is very strange after the long freeze for 3.4.\nWhat is the consensus on the next kde release? Will it be a 3.5 Application Release? When will do core developers start using Qt 4?"
    author: "Anonymous"
  - subject: "Re: So..."
    date: 2005-04-12
    body: "Seeing that things are moving over to subversion, I wouldn't imagine too much traffic would be seen on the cvs mailing list for long anyway.\n"
    author: "Ryan"
  - subject: "Re: So..."
    date: 2005-04-12
    body: "> Will it be a 3.5 Application Release?\n\nLikely\n\n> When will do core developers start using Qt 4?\n\nOnce there is a branch for it, be assured. :-)"
    author: "Anonymous"
  - subject: "Re: So..."
    date: 2005-04-12
    body: "At least there is a 3.5 feature plan."
    author: "hannes g\u00fctel"
  - subject: "Re: So..."
    date: 2005-04-13
    body: ">\u00abAt least there is a 3.5 feature plan.\u00bb\nWhere ? (nothing here for the moment: http://developer.kde.org/development-versions/)"
    author: "Capit Igloo"
  - subject: "Re: So..."
    date: 2005-04-13
    body: "Here: http://developer.kde.org/development-versions/kde-3.5-features.html"
    author: "Aaron Krill"
  - subject: "Re: So..."
    date: 2005-04-14
    body: "So, how does one contribute to this list?\n\nThe need to fix (sort out) the Crystal and HiColor icon themes has been more or less agreed to on the kde-artists@mail list, I have started working on it but don't seem to be able to get any further with it.\n\nThe mixed up icon themes are something of a mess and it is going to take considerable work to sort it back out.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: So..."
    date: 2005-04-14
    body: "Assuming that you have a CVS account, change developer.kde.org/development-versions/kde-features.xml"
    author: "Anonymous"
  - subject: "Re: So..."
    date: 2005-04-13
    body: "There is also a 4.0 feature plan - both contain mostly randomly spread delayed entries from 3.4 feature plan currently."
    author: "Anonymous"
  - subject: "Faster? Numbers?"
    date: 2005-04-12
    body: "So opposed to beta1, beta2 is actually supposed to be faster than Qt3? I'd like to see some numbers that prove this."
    author: "Anynomous"
  - subject: "Re: Faster? Numbers?"
    date: 2005-04-12
    body: "And I want to see your numbers that beta1 was not faster than Qt3 too. :-)"
    author: "Anonymous"
  - subject: "Eye-candy?"
    date: 2005-04-12
    body: "I searched through the documentation of Arthur, but I cannot seem to find anything similar to what GTK is doing with Cairo. I've seen some screenshots of Cairo-rendered GTK-Widgets. They look -very- cool and the technique is very modern. Vector-based graphics are the future, but the only real noticable painting-improvement in Arthur is Anti-Aliasing (I know all backends, but what if I didn't, what is new that I can --see--). Imho Vector-graphics and a theming-engine which makes use of that is something I would really like to see in KDE 4. Vector-graphics are the future. But is it possible at all with Qt 4?\n\nAm I missing something? Where are the noticable eye-candy-features, which make the average university-student go like: \"Wow\" as it works for Cairo and Avalon.\n\nBut, maybe I am missing something. Then I think trolltech should have created more and better examples of the eye-candy-possibilities. (Yes, I think the looks are very important if it comes to X-Clients.) :)\n\nSo, no direct flame or anything like that :), just (hopefully) a request for someone to clarify this a bit. :)"
    author: "mOrPhie"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "I though TT will have more than one painting backend, one of them being cairo...\nBeside that I am very sure TT will provide more and more screenshots / updated documentation / tech-demos and so one once they are in release-mode. As of now they are still working on the code itself. There are some month ahead of them!\n\nQt4 will allow developers a lot nice things including eyecandy, be sure of that!"
    author: "anon"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "\"I though TT will have more than one painting backend, one of them being cairo...\"\n\nIt doesn't even matter whether they do... If you look at the docs, it seems that writing one shouldn't be hard."
    author: "MORB"
  - subject: "Re: Eye-candy?"
    date: 2005-04-13
    body: "Well, writing one for Qt3 is not terribly hard either. It's just weirdly underdocumented.\n\nI wrote a Qt2 PDF backend but could never release it due to licensing issues (no good free PDF generating lib)."
    author: "Roberto Alsina"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "You are missing a lot of stuff. Such as, for example, sub-pixel positioning.\nAnd vector-based graphics died in favor of raster displays ages ago.\n"
    author: "SadEagle"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "I'm pretty excited to get sub-pixel positioning for KStars.  But I don't see anything about it in the description of Arthur here: \nhttp://doc.trolltech.com/4.0/qt4-arthur.html\n\nCan you provide a link?\n\nthanks,\nJason"
    author: "LMCBoy"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "Reply-to-self mode ON:\n\nWell, if you had *bothered* to read the actual class documentation for QPainter:\nhttp://doc.trolltech.com/4.0/qpainter.html\n\n...it would have been immediately *obvious* that yes, QPainter draw methods will accept floating-point pixel positions!\n\nHooray!\n"
    author: "LMCBoy"
  - subject: "Re: Eye-candy?"
    date: 2005-04-13
    body: "Jason, \n\nthis means that you can position your stars at the correct position and Qt (aka Arthur) will take care of the best \"real\" position so that rounding-error are reduced to a minimum and you do no longer have to put a million lines like this in KStars:\n\np.drawStar( (int)x , (int)y);\n\nCorrect?"
    author: "cniehaus"
  - subject: "Re: Eye-candy?"
    date: 2005-04-13
    body: "Hmm.  Maybe it does just mean that Qt will automatically round to the nearest pixel, saving me the trouble of recasting as int.  If so, this is less exciting.  I was hoping it would actually allow for sub-pixel moves, which is possible with antialiasing (i.e., openGL allows this).  This would eliminate the slight \"jitter\" as objects move across the sky, bouncing from pixel to pixel.\n"
    author: "Jason Harris"
  - subject: "Re: Eye-candy?"
    date: 2005-04-13
    body: "Well, since you can paint directly on an opengl surface with qt4's qpainter, you will automatically get opengl support just by porting to qt4.  After grepping the code for glEnable( GL_POINT_SMOOTH ), i didn't find it, but since there is no glDisable( GL_POINT_SMOOTH ), i think you could just make that call yourself, paint, and then you'll get anti-aliased points.\n\nAlso i think a call to glHint( GL_POINT_SMOOTH, GL_NICEST ) and glEnable(GL_SMOOTH) is needed.  Also blending needs to be enabled i believe.\n\nThis is mostly off the top of my head, so some of it could be inaccurate.\n\nMatt"
    author: "Matt"
  - subject: "Re: Eye-candy?"
    date: 2005-04-14
    body: "The AA code in Qt's OpenGL painter uses multisampling, not line and point antialiasing\n"
    author: "SadEagle"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "Actually, arthur is a vector-based rendering engine.\n\nThe page that describe arthur only describe what changed since qt3, but in qt3 it was already possible to apply transformations (rotation, scaling, etc.) to subsequent drawing operations.\n\nSince arthur, in addition to qt3, supports alpha and transformation of line width and brushes, as well as double-buffering, it has everything that should be needed to achieve those trendy animated eye-candy."
    author: "MORB"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "The immediate advantage of Arthur is a cleaner API and faster response."
    author: "Ian Monroe"
  - subject: "Re: Eye-candy?"
    date: 2005-04-12
    body: "Arthur supports several different types of backend. At the moment does not support Cairo (as I understand it, Cairo is still in a developer preview stage), however it does support OpenGL, which should allow for some powerful rendering.\n\nAs other comments have mentioned, Qt has supported vector drawing for quite some time."
    author: "Bryan Feeney"
  - subject: "Re: Eye-candy?"
    date: 2005-04-13
    body: "Why is cairo \"Vector graphics\" and Arthur isn't? If you can draw a line (circle, spline etc.) between Point A and B, then you have vector based description. \nGiven today's technology, these vectors have to be converted into a pixel-based representation, no matter if it's Cairo or Arthur. At least as long as you don't use an ancient vector display.\nTo be honest, I don't know cairo at all. It doesn't use a scene graph, or does it? This would make it conceptually different from Arthur, but then I would be happy that Trolltech didn't follow that path. \nOtherwise, what's the big difference? What - except for good taste - stops us from drawing our buttons with an animated and vectorized leopard fur look?\n"
    author: "uddw"
  - subject: "Re: Eye-candy?"
    date: 2005-11-18
    body: "Hi.\n\nOn Windows, there are a lot of good Qt 4.0 graphics demos in \n\nStart -> Programs -> [Qt] -> Examples and Demos\n\nSelect \"Demonstrations\"\n\nSee Vector Deformation, Affine Transformations, Path Stroking, etc.\n\nLooks like vector graphics to me.  And looks really nice.\n\n(I don't know where these are on other platforms.)\n\nTo me, supporting \"vector graphics\" is an abstract concept.  \n\nDo you want to view SWF or EMF files?  Do you want to view PDF files?  Do you want to print to PS/PDF?\n\nHere is my quick (uneducated) take on choices of various vector graphics formats in Qt:\n\nSVG -> use ksvg libs\nPDF -> use kpdf libs\nPS -> use PDF engine?\nEMF -> no idea; MS file formats are a pain\nSWF -> no existing Qt app (?)\nDXF (CAD) -> Open Cascade (Qt3-based)\n\nIf you need to do \"heavy duty\" vector graphics, maybe you can use something like   Open Cascade, which an open source CAD toolkit.  [http://www.opencascade.org/]  Unfortunately, this is Qt 3, but it is a fantastic example of how powerful Qt can be for graphics.  It will do BREP solids, meshes, irregular grids, etc. (basically anything a CAD system can do).  Then you can dump to a lot of different file formats.  I imagine PDF, SVG, etc. would not be incredibly difficult to support if it isn't there already. \n\nThere is also the Qt bindings for Coin3d for heavy-duty 3d graphics display.\n\nMaybe you can poke around for other 3rd party Qt graphics libraries.  There are a lot of others out there and GL support is really good.\n\nQt is pretty amazing for graphics work if you know where to look. I don't know nuthin about the GTK stuff. :)     \n\nGood luck."
    author: "jeremy"
  - subject: "WOW - Look at that changes!"
    date: 2005-04-12
    body: "It's unbelievable how beta2 has changed from beta1. I'm not talking about improvements here and there, but new cool stuff. Look at the Arthur Paint System for instance:\n> QImage has become a paint device.\nGreat! Kolourpaint, KPDF, <any other nae here> will have nicer graphics and lots of speedups due to this.\n> We extend our gradient support. It is now possible to specify multiple colors at various positions in a gradient and different spread methods. We have also added support for radial and conical gradients\nThat's juicy!\n> It is now possible specify a QBrush as the fill method for a pen. This makes it possible to for instance, have textured text quite easily.\nAny other words to add?\n\nYes, thanks TT! :-)\n"
    author: "canny"
  - subject: "Re: WOW - Look at that changes!"
    date: 2005-04-14
    body: "> It's unbelievable how beta2 has changed from beta1. I'm not talking about\n> improvements here and there, but new cool stuff. Look at the Arthur Paint\n> System for instance:\n>\n> > QImage has become a paint device.\n>\n> Great! Kolourpaint, KPDF, <any other nae here> will have nicer graphics and\n> lots of speedups due to this.\n\nThis is great but has QImage -> QPixmap conversion been sped up?"
    author: "Clarence Dang"
  - subject: "Re: WOW - Look at that changes!"
    date: 2005-04-15
    body: "You should always assume the QImage->QPixmap conversion is very slow, as it will be over a network.  QImage is local, QPixmap is on the server.\n\n"
    author: "JohnFlux"
  - subject: "Re: WOW - Look at that changes!"
    date: 2005-04-15
    body: "What network are you talking about?"
    author: "ac"
  - subject: "Re: WOW - Look at that changes!"
    date: 2005-04-16
    body: "> You should always assume the QImage->QPixmap conversion is very slow, as it will\n> be over a network. QImage is local, QPixmap is on the server.\n\nI'm optimising for the more common local case.  In Qt3, it was very slow - I'm wondering if it has improved."
    author: "Clarence Dang"
  - subject: "Developing"
    date: 2005-04-12
    body: "Heh...actually a dude and I have started to code an economy program with the Qt 4 libs.\nIt's called tribok. And what I have seen Qt 4 is much more better."
    author: "dikatlon"
  - subject: "Re: Developing"
    date: 2005-04-12
    body: "Are you creating the program using the model/view abstracts and other new concept or porting qt3-style programming/ideas?\nLooking forward to see your creation! have fun, guys :-)\n"
    author: "jumpy"
  - subject: "Re: Developing"
    date: 2005-04-13
    body: "For the moment, we are just porting the qt3 code to qt4...but we don't use these tools like qt3to4 to port. We will see what happends, at this base of code dev we have done there isn't really much to talk about yet.\n\nThanks for the encouraging reply :)"
    author: "dikatlon"
  - subject: "is Qt becomming his own programming language?"
    date: 2005-04-13
    body: "Hi,\ni have used Qt3 already for some programs and have now read a article about Qt4 on a german linux magazine.\nI'm a little bit upset. Qt has developed there own containers and so on for good reasons. But now Qt also develop his own \"language\", as i have seen there will be a foreach statement to iterate through Qt containers.\nI'm not sure if i should like this development. It seems like Qt becomes more and more not only a toolkit or framework but even design his own programming language based on C++."
    author: "eagle"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-13
    body: "if it means easier coding and less boring/weird stuff for the programmer, why not?"
    author: "Anonymous"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-13
    body: "It's just another way to iterate over the elements of a container. There are different ways to do this already, so I don't see what harm it could do add another method if it is much more clear than the others. Of course we can do everything in a pure STL compatible way, which is often even most efficient. But sometimes readability matters more."
    author: "uddw"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-13
    body: "yes, it's \"just another way\" but the question is if it is the right way to extend the language C++ with \"just another way\"?\nAs long as we talk about the toolkit/framework i have nothing against it if Trolltech needs a special container, data-type or keyword (e.g. emit,..).\nBut i'm not sure if it is the right way to introduce own statements like foreach.\n\nIf i develop a lot of GUI things with Qt for about one year and than someone ask me for a console-program i will start with C++ and will try to use things like foreach because i'm used to use this things. But it want work because this statement is not a C++ statement.\n\nI think basically we can think about it, if a statement like foreach is usefull in C++. But we should think about it in the C++ standard and not in our own way and ower own lib. Because i'm fear that this will \"pull C++ to pieces\" and C++ code will have more and more differences to C++/Qt code. I think this is a bad development because it weakens the C++ Standard in the long run. "
    author: "eagle"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-13
    body: "What if you try to use Qt functions?  How is that any different?"
    author: "Corbin"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-14
    body: "What if I program in Haskell, Ruby, or any other good language for a year, and then someone requires me to write a C++ program? I'll have to get back in the swing of things.\n\nIf the bigwigs on the C++ standards committee see a foreach statement, and decide it's good, and put it in the C++ standard, and compilers finally support it, then great, everyone can use it. But that would probably take years, if it happens at all.\n\nIn the mean time, people can use Qt, and allow themselves a nugget of elegance in the overall pain that is C++ programming. :)\n\nSome people like to do:\n\n#define unless(a) if(!(a))\n\nIs that also a capital sin? Why does C++ have macros, and why is the template language Turing complete if you're not supposed to do things like this?"
    author: "Dolio"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-14
    body: "> What if I program in Haskell, Ruby, or any other good language for a year,\n> and then someone requires me to write a C++ program?\n\nso you consider Qt/C++ as a own language if you compare my scenario with the scenario were someone programs in \"Haskell, Ruby, or any other good language and than go back to C++\"?\nThank you for confirm my statement."
    author: "eagle"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-14
    body: "\"What if I program in Haskell, Ruby, or any other good language for a year, and then someone requires me to write a C++ program? I'll have to get back in the swing of things.\"\n\nIn the case of ruby, you can write ruby Qt or KDE programs with QtRuby and the Korundum/KDevelop RAD environment. So you might not have to get back to C++ programming anyway.\n\nTuring Complete vs. Language usability?\n\nI'm sure that both C++ template language and ruby were designed by very clever people. The difference is that ruby was designed from the ground up to 'be usable', and indeed it is. C++ templates were not designed with usability in mind and are one of the hardest features of any programming language to understand properly."
    author: "Richard Dale"
  - subject: "Re: is Qt becomming his own programming language?"
    date: 2005-04-13
    body: "#define foreach(c,i) for (i = c.start(); i != c.end; i++)\n\nthis is not a new language!"
    author: "LuckySandal"
  - subject: "D  ??"
    date: 2005-04-13
    body: "\"own programming language based on C++.\"\n\nWhy not take D? D is like Java and C# but lacks a class library."
    author: "hannes g\u00fctel"
  - subject: "KDE project for D"
    date: 2005-04-13
    body: "http://users.tpg.com.au/smackoz/"
    author: "Andr\u00e9"
  - subject: "can we do this with Qt4 ? :)"
    date: 2005-04-13
    body: "I'm not a big fan of gnome but these one look really cool.\n\nhttp://www.gnome.org/~seth/blog-images/monkey-hoot/TransparentWindows.ogg\n\nhttp://www.gnome.org/~seth/blog-images/monkey-hoot/WorkspaceSwitching.ogg\n\nhttp://www.gnome.org/~seth/blog-images/monkey-hoot/WobblyWindows.ogg\n\nhttp://www.gnome.org/~seth/blog-images/monkey-hoot/WobblyWindowsIntro.ogg\n\nhttp://www.gnome.org/~seth/blog/xshots"
    author: "Pat"
  - subject: "Re: can we do this with Qt4 ? :)"
    date: 2005-04-13
    body: "The real question is: can we do this with today's XOrg?\n\nWe were on that front with Impresario, Zack's WM, but the project stopped after finding arch problems in that implementation. Luminocity will stop too.. they don't have realized the problem yet.\n\nBtw having Zack, with his experience on the field, hired at Trolltech and working on that stuff really gives us a gear more than our competitors."
    author: "jumpy"
  - subject: "Re: can we do this with Qt4 ? :)"
    date: 2005-04-13
    body: ">Luminocity will stop too.. they don't have realized the problem yet.\n\nCan you explain this? What problem?"
    author: "cniehaus"
  - subject: "Re: can we do this with Qt4 ? :)"
    date: 2005-04-13
    body: "http://www.kdedevelopers.org/node/view/814"
    author: "Illissius"
  - subject: "Re: can we do this with Qt4 ? :)"
    date: 2005-04-13
    body: "Unfortunately Zack does have a tendency to abandon projects when the going gets tough viz. Qt Mozilla."
    author: "ac"
  - subject: "Re: can we do this with Qt4 ? :)"
    date: 2005-04-13
    body: ">> Luminocity will stop too.. they don't have realized the problem yet.\n\nYou don't seem to realize that Lumonicity is just a concept.\n\nThe real thing (when it's integrated in Metacity) will be different and use the XGL server (thus fixing a number of issues), afaik.\n"
    author: "konqi"
  - subject: "Re: can we do this with Qt4 ? :)"
    date: 2005-04-16
    body: "What I found most impressive about those was the \"live\" pager, with constant updates not only for the windows on the current desktop but for those on the other desktops as well.  Very impressive.\n\nThe rest of the features I don't care about so much ;-)"
    author: "kundor"
  - subject: "Are the SERIOUS font related bugs in Qt-3 fixed?? "
    date: 2005-04-13
    body: "My only interest is whether the bugs related to font will be fixed.\n\nWill the PostScript driver be fixed to work as well as the PS driver in OpenOffice does?  Specifically, (a) will it be able to make scalable Type42 fonts from TrueType fonts and (b) will is support PS font metrics and printer resolution hinted font metrics.  Note that currently Qt supports only screen resolution hinted font metrics.\n\nWill the PostScript driver be fixed so that it gets the PostScript font names correct?  My patch fixes this, but it is really just a kludge.\n\nWill the font system be fixed so that it can properly deal with Type1 font names (Family, Weight, & Width)?  NOTE that this is a general bug that perhaps needs to be addressed elsewhere.  TrueType and Type1 font names are not the same when they contain a Width (and possibly some issues with some Weights other than Regular and Bold).\n\nWill the font system be fixed so that it can properly deal with font faces other than the 4 mode paradigm: Regular, Bold, Italic, Bold Italic?\n\nIMO, these are serious bugs and I am at a loss to see how Qt got to version 3.0 without their being fixed.  And, I think that part of this is a regression (I used to use Helvetica Narrow, but now it doesn't even show up in the KDE font selector).\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Are the SERIOUS font related bugs in Qt-3 fixed?? "
    date: 2005-04-13
    body: "I'm pretty sure that Trolltech would be more than delighted, if you would try out Qt4 Beta2 and test if those font bugs still exists."
    author: "ac"
  - subject: "Re: Are the SERIOUS font related bugs in Qt-3 fixed?? "
    date: 2005-04-14
    body: "I am going to try a build of KDE-3.4 BRANCH against Qt-4.  But ... (TM).\n\nSome of these issues are going to require changes in application code -- the font metrics issue is going to require application changes.\n\nCurrently some of the font faces work correctly and some don't.  Testing this is confounded by the fact that we don't know how much of the problem is in KDE and how much is in Qt.  I have been told that these are Qt problems, but I have no way of knowing for sure.  Perhaps some assistance from the maintainer of the KDE code would help.\n\nI have found that the problem with Oblique being listed (INCORRECTLY) as Italic is a KDE problem.  This hack appears to have fixed it:\n\ndiff -Naur kdelibs.old/kdeui/kfontdialog.cpp kdelibs/kdeui/kfontdialog.cpp\n--- kdelibs.old/kdeui/kfontdialog.cpp   2004-11-03 09:15:17.000000000 -0700\n+++ kdelibs/kdeui/kfontdialog.cpp       2005-04-13 17:45:33.000000000 -0700\n@@ -476,8 +476,6 @@\n         if(pos >=0) style = style.replace(pos,5,i18n(\"Regular\"));\n         pos = style.find(\"Normal\");\n         if(pos >=0) style = style.replace(pos,6,i18n(\"Regular\"));\n-        pos = style.find(\"Oblique\");\n-        if(pos >=0) style = style.replace(pos,7,i18n(\"Italic\"));\n         if(!styleListBox->findItem(style)) {\n             styleListBox->insertItem(i18n(style.utf8()));\n             currentStyles.insert(i18n(style.utf8()), *it);\n\nNews Flash: Italic and Oblique are not the same.\n\nAlso please note that some fonts have a face called: \"Normal\" so substituting for that is also questionable.  And, where does: \"Medium\" get replaced?\n\nIAC, I have over a thousand font so it will take a while. :-)  But, because I do have that many fonts, I can help with testing if someone is interested in fixing the problems.  Clearly, we do need to get these font name issues fixed.  It is not a usability improvement to have fonts displayed with the WRONG face names.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Are the SERIOUS font related bugs in Qt-3 fixed?? "
    date: 2005-04-14
    body: "> I have found that the problem with Oblique being listed (INCORRECTLY) as Italic is a KDE problem. This hack appears to have fixed it:\n\nYeah, those replacements look strange. Unfortunately the commit message for the change doesn't really give a detail reason for this. (https://svn.kde.org/viewcvs/trunk/kdelibs/kdeui/kfontdialog.cpp?rev=128434&view=markup)"
    author: "ac"
  - subject: "Re: Are the SERIOUS font related bugs in Qt-3 fixed?? "
    date: 2005-04-15
    body: "Is it possible, then, that my missing Helvetica faces are a KDE problem rather than a Qt problem as I was told?\n\nFor example, I have Helvetica Narrow installed (Type1).  This used to work in KDE 2.<something>, but now it doesn't show up in the KDE Font Selector dialog box.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Are the SERIOUS font related bugs in Qt-3 fixed?? "
    date: 2005-04-16
    body: "I now beleive that part of the font problem is in KDE -- rather than Qt -- code so I filed a bug report:\n\nhttp://bugs.kde.org/show_bug.cgi?id=103852\n\nIt was immediatly marked with Priority: VLO, which I presume is one small step above WON'T FIX.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Qt4 based Qtopia/Opie?"
    date: 2005-04-13
    body: "Any word on when we might be seeing Qtopia/Opie releases based on Qt4?  I'd love to see an HP iPAQ h3600 PDA/phone running Qt4 based Opie on Linux <drool>."
    author: "ac"
  - subject: "Re: Qt4 based Qtopia/Opie?"
    date: 2005-04-13
    body: "Work is being done on porting Qtopia to Qt Embedded 4. Opie has not yet begun, but will soon enough. There is a lot of work to be done jumping from version 2 to 4!\n"
    author: "Lorn Potter"
  - subject: "Re: Qt4 based Qtopia/Opie?"
    date: 2005-04-13
    body: "I hope you will be successful. Qt2 was a main reason for me to no longer develop stuff for Opie."
    author: "cniehaus"
  - subject: "build failed"
    date: 2005-04-13
    body: "Hi,\n\n my build progress failed, is there a possibility to get a binary package for Linux/Win?"
    author: "katakombi"
  - subject: "Re: build failed"
    date: 2005-04-13
    body: "What was the error?"
    author: "Aaron Krill"
  - subject: "Re: build failed"
    date: 2005-04-14
    body: "...\n...\n...\nIn file included from ../../include/QtGui/private/qpaintengine_x11_p.h:1,\n                 from kernel/qapplication_x11.cpp:67:\n../../src/gui/painting/qpaintengine_x11_p.h: In member function `virtual\n   QPaintEngine::Type QX11PaintEngine::type() const':\n../../src/gui/painting/qpaintengine_x11_p.h:107: error: `qt_x11Data' is not a\n   member of type `QPaintEngine'\nmake[3]: *** [.obj/release-shared/qapplication_x11.o] Error 1\nmake[3]: Leaving directory `/usr/src/app/qt-x11-opensource-4.0.0-b2/src/gui'\nmake[2]: *** [release] Error 2\nmake[2]: Leaving directory `/usr/src/app/qt-x11-opensource-4.0.0-b2/src/gui'\nmake[1]: *** [sub-gui-make_first-ordered] Error 2\nmake[1]: Leaving directory `/usr/src/app/qt-x11-opensource-4.0.0-b2/src'\nmake: *** [sub-src-make_first-ordered] Error 2\nkatakombi@1[qt]$ "
    author: "katakombi"
  - subject: "Qt Good!  Designer Bad!"
    date: 2005-04-15
    body: "Qt 4 is looking totally amazing, but I have to say that I am getting more and more disappointed by Qt Designer.  The drag'n drop signal/slot connector is totally worthless for any application with more than a dozen objects, the new SDI mode is a hack, and drag'n drop of objects is a cute feature but a total pain in the arse if you have a fairly sizeable project.  Designer is a huge part of Qt and its is not yet even close to being ready for release. Isn't this the last beta before we start RC releases?\n\nBobby"
    author: "brockers"
  - subject: "Re: Qt Good!  Designer Bad!"
    date: 2005-04-19
    body: "You will have to wait until Qt 4.1 for a mature Qt4 designer and the missing classes of Qt3 (QCanvas, ...) being ported."
    author: "Anonymous"
---
<a href="http://www.trolltech.com/">Trolltech</a> has released the second and <a href="http://www.trolltech.com/newsroom/announcements/00000200.html">final beta version of Qt 4</a>. You can download it from <a href="ftp://ftp.trolltech.com/qt/source/">ftp.trolltech.com</a> or from one of <a href="http://www.trolltech.com/download/betas.html">its mirrors</a>. The online <a href="http://doc.trolltech.com/4.0/">Qt Reference Documentation</a> has been updated. Qt 4 is currently scheduled for final release in late Q2, 2005, with an intermediate Release Candidate planned for May.


<!--break-->
<p>In addition to improvements to the five key technologies presented in
beta 1 - Arthur, Scribe, Interview, Tulip and Mainwindow - the second
beta version incorporates nearly all new features, tools and resources
that will appear in the Qt 4 final release. Features now available
in Qt 4 beta 2 include:</p>

<ul>
<li>Improvements to the Qt 3 to 4 porting tool and supporting documentation</li>
<li>Feature additions to Qt Designer, including support for MDI and
   SDI modes, and support for custom widgets</li>
<li>A new painting subsystem which allows device-independent rendering of pixel-exact images</li>
<li>An improved input method framework</li>
<li>Addition of XP (available under Windows only) and Motif styles</li>
</ul>

<p>Additional improvements have also been made to the Qt3Support
Library. Trolltech aims to maintain the Qt3Support Library for the
lifetime of the Qt 4 series, and will also support the Qt 3 series for
a minimum of two years beyond the release of Qt 4.</p>

