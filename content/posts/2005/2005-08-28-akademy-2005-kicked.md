---
title: "aKademy 2005 Kicked Off!"
date:    2005-08-28
authors:
  - "aseigo"
slug:    akademy-2005-kicked
comments:
  - subject: "Oh"
    date: 2005-08-27
    body: "Next time Bulgaria? Bulgaria is cheap and the price ratio of accomodation is better than in Spain. Your pics look like 3rd world youth hostels. Poor guys. At least you got net... "
    author: "George"
  - subject: "Re: Oh"
    date: 2005-08-28
    body: "Where are these pics?"
    author: "michael"
  - subject: "Re: Oh"
    date: 2005-08-28
    body: "http://wiki.kde.org/tiki-index.php?page=Pictures%20%40%20aKademy%202005"
    author: "Anonymous"
  - subject: "Re: Oh"
    date: 2005-08-28
    body: "The accommodation is excellent - I have my own kitchen, bathroom, room etc.  I could not be happier with it."
    author: "JohnFlux"
  - subject: "Novell Desktop"
    date: 2005-08-27
    body: "http://wiki.kde.org/tiki-index.php?page=Novell+Linux+Desktop\n\nVery intresting that Wine wasn't mentioned in their Novell Desktop effort. wine 0.9 is close. Yes, I know. wine development is obscure rocket science and Corel burned its money and its ass... But it matures. \n\n\"SuSE is their consumer brand, NLD is enterprise.\"\n\nMy personal opinion is that Novell Desktop is no trustworthy distribution. It will be a rebranded SuSE with different software, so there must be a support and time lag. Probably it will be bought by those who want the Novell trademark. But as an Enterprise I would not buy \"Novell Linux\", sounds like a spin-off. You always get support problems with those.\n\n\"Provide applications: Openoffice, Evolution, Firefox, Linux.\"  \n\nWell. SuSE already has it.\n\n\"What is unique about NLD compared to other distributions? ... The answer is that it's backed by Novell with support.\"\n\nSo just the brand? \n\n\"They had Ximian and SuSE. So they decided not to make a default at all, both have strengths and they have customers who use both.\"\n\n:-) Very funny. They bought a long established distributor with a lot of customers and a talented programmer's booth with a strong and dirty propaganda department. Not quite the same. "
    author: "Hans J\u00e4ger"
  - subject: "Re: Novell Desktop"
    date: 2005-08-27
    body: "'They weren't aware of WINE until AKADEMY @DOT.KDE when HANS pointed it out to them so they hope to work with them now.'\n\n:-)"
    author: "Harry Schnitzel"
  - subject: "Re: Novell Desktop"
    date: 2005-08-28
    body: "http://www.winehq.com/?issue=289\n\nWine now supports theming! This sounds very good.\n\n"
    author: "Gerd"
  - subject: "Re: Novell Desktop"
    date: 2005-08-28
    body: "It would have been far more interesting to hear what applications were missing and what people in the far, flung corners of Novell using their desktops for different things found were deficient. What things simply don't have an equivalent with desktop Linux? That would have been thoroughly informative for just about everyone. It seems as though this was the same old, same old though. Have they made any progress or learned anything at all?!\n\n\"They have worked to make sure it works with Novell groupwise. It already worked with MS Exchange. They found there are a lot of features in groupwise specific to groupwise and Evolution was getting bloated by stretching it to groupwise. So they released the Java client so people on Linux can get to their e-mail.\"\n\nSo that's a nice way of putting that it simply doesn't work with Groupwise?!! I'm confused by that one. To be perfectly honest, my experiences haven't been too favourable with Evolution in the past in terms of using groupware and IMAP stability. The way they were talking it was like Evolution has had full support for Groupwise. What is the status of Kontact?\n\n\"They had Ximian and SuSE. So they decided not to make a default at all, both have strengths and they have customers who use both.\"\n\nTo be honest I think they'll pay for that in the future. They need to pick some common technology and integrate the rest with it i.e. GTK in KDE or Qt in Gnome. Look at what Red Hat are doing to focus around Gnome and GTK so they can focus on doing what they want to do with Java development and other things. Novell/Suse will simply never be able to do what they need in a focused way. Let's face it, the worst thing that could happen is for it to fail and for them have to go wandering back to Windows.\n\n\"What is the benefit for a company running NLD with these applications, when Firefox, Openoffice and in the future Evolution run on Windows? Companies have a lot of reasons, possibly hit by viruses, heard its free and want to use it.\"\n\nThat's a pretty weak argument to be honest, and highlights a problem that I believe Mr. A. Seigo brought up a while ago about the wholesale porting of open source Linux/Unix applications to Windows. If that is what he said, it seems that Novell haven't thought this through enough.\n\nSounded like a very straange and rather indecisive presentation."
    author: "Segedunum"
  - subject: "talk downloads?"
    date: 2005-08-28
    body: "I hope we could download these great talks, developers talks above all.\n\nI live from 1 hour from M\u00e1laga but I cant go there because of my work :( Maybe next year!!"
    author: "biquillo"
  - subject: "Re: talk downloads?"
    date: 2005-08-28
    body: "There will be a video record archive available later."
    author: "Anonymous"
  - subject: ";-)"
    date: 2005-08-28
    body: "It is Andaluc\u00eda, not Andalusia :D\n\nI hope KDE developers are enjoying their instance in Spain, and also I hope I could go to the aKademy, I'm at only 1 hour by train of M\u00e1laga, but I can go :-("
    author: "asharrot"
  - subject: "Re: ;-)"
    date: 2005-08-28
    body: ":)\n\nEnglish is not Spanish - Spanish is not French - French is not German...\n\nSpain means Espan~a means Espagne means Spanien...\n\nAndalusia means Andaluc\u00eda means Andalousie means Andalusien...\n\nOf course, if you absolutely want to, you can say \"Andaluc\u00eda\" in English just as well - but it certainly isn't wrong to say \"Andalusia\".\n\n"
    author: "Nils"
  - subject: "OT: It's both, Andaluc\u00eda and Andalusia"
    date: 2005-08-28
    body: "Que paicha!\n\nActually, it's said Andaluc\u00eda in spanish, and Andalusia in English, thus being both off them correct. This is no surprise as there are many places that changes their names depending on the language. I.e.: Edimburgo (sp) vs. Edinburgh (en), Inglaterra (sp) vs England (en), etc. OTOH, it's a good idea for those foreigners that are now in Andalusia to know the spanish name, it must be handy when talking with locals.\n\nEnjoy yourself in M\u00e1laga!"
    author: "Eduardo Robles Elvira (Edulix)"
  - subject: "Re: ;-)"
    date: 2005-08-28
    body: "It doesn't matter how it is spelled or sounds, what matters is what it means and stands for. The couldn't have picked a better place for hold the aKademy. Follow this link to see why.\n<A href=\"http://www.andalus.co.uk/about.htm\"><B>Andalus</B></A>"
    author: "abe"
  - subject: "IPC?"
    date: 2005-08-28
    body: "Does anyone know what IPC is?"
    author: "Norbert"
  - subject: "Re: IPC?"
    date: 2005-08-28
    body: "inter process communication"
    author: "Anonymous"
  - subject: "Re: IPC?"
    date: 2005-08-28
    body: "DCOP, DBUS, Corba etc are examples of IPC.\n"
    author: "JohnFlux"
  - subject: "Re: IPC?"
    date: 2005-08-29
    body: "and don't you forget about DBUS"
    author: "MaxxCorp"
  - subject: "aKademy videos"
    date: 2005-08-28
    body: "The videos will be made available as soon as possible. We need to edit them to remove pauses at the beggining and at the end, as well as some other tweaks, but there are already some raw videos we uploaded here this morning:\n\nhttp://www.polinux.upv.es/mozilla/akademy2005/\n\nPlease don't publish the URL in other places yet until we finish editing the videos. We'll publish an announcement when they're ready."
    author: "Victor Fernandez (aKademy volunteer)"
  - subject: "Re: aKademy videos"
    date: 2005-08-28
    body: "Thanks"
    author: "User"
---
Following yesterday's rousing <a href="http://www.kde.org/areas/kde-ev">KDE e.V.</a> meeting, <a href="http://conference2005.kde.org">aKademy 2005</a> officially kicked off today with dual presentation tracks filled with content designed for users and system administrators. At the same time the hacking rooms were full of busy developers from morning until evening at which point everyone went to a party sponsored by <a href="http://www.novell.com">Novell</a>.


<!--break-->
<p>The opening address was delivered by a panel made up of representatives from the University of M&aacute;laga, the provincial government of Andalusia, and the president of KDE e.V.</p>

<p>Next up was Pete Goodall,
the product manager of the Novell Linux Desktop, who related Novell's experience with their Linux desktop migration effort. KDE project members followed this up with presentations on <a href="http://www.kolab.org">Kolab2</a>, the <a href="http://www.kde.org/areas/sysadmin">Kiosk desktop configuration management framework</a>, and the <a href="http://www.nomachine.com">NX</a>
terminal server solution. The second keynote of the day was given by
Ubuntu founder Mark Shuttleworth who emphasized the importance of
collaboration among open source projects to be successful.</p>

<p>All the while, KDE developers who were not busy presenting topics to those in attendance were working on a wide variety of projects in the computer labs. Many of the developers broke out into working groups over the course of the day to discuss and design KDE4 components such as IPC, scripting, art and desktop configuration in preparation of the week-long hackfest that starts Wednesday.</p>

<p>Be sure to visit <a href="http://www.planetkde.org">Planet KDE</a> to read live developer blogs from M&aacute;laga, the wiki for <a href="http://wiki.kde.org/tiki-index.php?page=Talks%20%40%20aKademy%202005">talk transcripts</a> and check back here daily for regular aKademy 2005 updates.</p>

