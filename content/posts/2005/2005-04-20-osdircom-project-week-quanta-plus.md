---
title: "OSDir.com Project of the Week: Quanta Plus"
date:    2005-04-20
authors:
  - "amantia"
slug:    osdircom-project-week-quanta-plus
comments:
  - subject: "PHP"
    date: 2005-04-19
    body: "The best thing about Quanta+ IMHO is that it does not try to\nforce all that VPL stuff on its users. If you want to use\nit just as a PHP editor: Works perfectly! The only thing I'm sometimes\nenvious of are Zend Studio's function parameter hints for\nuser-defined functions. You can include a file somewhere\nwith functions you wrote and when you are in the main file it\nactually tells you what parameters there are. Great!\nEven better would be a Delphi-like \"right-click->go to definition\".\nI dont know any PHP editor that can do that, though.\n\n"
    author: "Martin"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: ">Even better would be a Delphi-like \"right-click->go to definition\".\n\nDelphi-like? I supose you are talking about some delphi IDE (visual delphi? sorry for my ignorance). Actually kdevelop3 makes that, at least, for C++. I have seen some PHP project templates but I have not tested them. I use quanta for html/php and kdevelop for C++. Quanta looks pretty extensible, may be a ctags hack can make it work with PHP, if it's not doing it already. \n\n>I dont know any PHP editor that can do that, though.\n\nI dont know if kdevelop is a PHP editor, (for me \"cat << OEF\" is)\n\nNow it's time for certain people to talk about the KDE Great Big Editor One joining quanta and kdevelop. Or simply talk about what emacs/vim/notepad makes for them.\n\nPD: aal spelling mistakes are caused beacuse keyborad vendors ship all keis togeter"
    author: "someone"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "Delphi is a IDE. \nIt uses Object Pascal as language.\n\nAs Kylix is on IDE for Object Pascal and C++ for Linux.\n\nmore on: http://www.borland.com\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: ">It uses Object Pascal as language.\n\nThe language has been renamed to Delphi as well, some time ago."
    author: "yaac"
  - subject: "Re: PHP"
    date: 2005-04-21
    body: "nice to know, thank you."
    author: "Iuri Fiedoruk"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "with respect to php, quanta+ has an outstanding feature that is missing in nearly\nany other web/php editor: the integrated php debugger gubed.\nnot even DW has anything like this.\njust invaluable for serious php-development."
    author: "hoernerfranz"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "Well, integration of gubed is nice but there are quite a few people who would prefer an integration of xdebug."
    author: "Andreas"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "We need developers to do it... The debugger interface was designed that way that you can use different debuggers, not just gudeb. But right now only Gubed is supported because...the gubed author did the integration."
    author: "Andras Mantia"
  - subject: "Re: PHP"
    date: 2005-04-22
    body: "a tutorial \"Debugging PHP scripts with Quanta Plus and Gubed PHP Debugger\" can be found here:\n\nhttp://www.very-clever.com/quanta-gubed-debugging.php"
    author: "tom"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "I've been using Quanta+ since... well I don't even know!!\nIt's a long time, it was just a small text editor on build of kwrite with buttons for adding HTML elements. It was greatly improved since then, and I belive once Qt4 is out and KDE and Quanta+ ported to windows, it will be a great free (as both beer/speech) alternatice to the great Homesite program, and maybe even to Dreamevil.\n\nSo, the future is indeed more than ever shiny for Quanta. Way to go, and thanks for this great application."
    author: "Iuri Fiedoruk"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "> The only thing I'm sometimes envious of are Zend Studio's function parameter hints for user-defined functions. You can include a file somewhere with functions you wrote and when you are in the main file it actually tells you what parameters there are. Great!\n\nFor built in functions this is stored in the DTEP. I'd have to talk with Andras about this. The new object syntax auto completion was done with an external temporary DTEP file and I don't know if the hooks are still active but the ability to add this should not be difficult as it would only need to follow the XML definition used in the DTEP and Quanta would need to know to read it. In fact... technically you could place or link files into the DTEP files you have currently. I have written a PHP file to parse PHP docs and generate the DTEP files and it can also accept external data input. If you are willing to accept certain limitations like having to create the files and having them be global to all projects this is there now. Ideally we would like to be able to generate files for a project on demand.\n\nI note though that the parameters are included in the structure tree so they should be in the tip. Additional project DTEP files would enable additional information like the version specification we added for 3.4, though I'm not sure how it would be used. In addition to this you can also generate PHP docs for your project and have project documentation in the doc panel.\n\n> Even better would be a Delphi-like \"right-click->go to definition\". I dont know any PHP editor that can do that, though.\n\nThis should be easy enough to add. In fact both things you want are already in the document structure tab. Open a PHP file and click the structure view in the left. You will see groups for functions, variables, inclusions, objects and classes. If you look at a function you will see the parameters. If you see a file address you can expand these are included functions in the file they originate in. Quanta understands the scoped linking and if you click on that function it will open the file and take you there.\n\nSo both features you imagine to be unlikely exist, but in the structure tree. As for the rest of it we just have so many things we can do with our resources... and as it turns out those are more than most people expect. \n"
    author: "Eric Laffoon"
  - subject: "Re: PHP"
    date: 2005-04-20
    body: "The parameters do not appear unless the function/class is declared in a .tag file in the DTEP. This is what Eric says, that we might generated in the feature project specific DTEP tags from PHP classes and functions. Right now what you can get from Quanta is autocompletion for external functions, classes and class methods:\nincluded_file.php:\n<?\n  class includedClass {\n  function includedClass_ctor(int i)\n  {\n    if ($a) {\n      $b = $a;\n    }\n  }\n  \n  function includedClass_method2(string s)\n  {\n  }\n  \n  function includedClass_method1(int i, int j)\n  {\n  }\n}\n\nfunction externalFunc(int i) {\n}\n\n$includevar;\n\n?>\n\nmain_file.php:\n<? \ninclude (\"included_file.php\");\nclass qmyClass{\n\n  function qmyClass_ctor(int i)\n  {\n    $i = 0;\n  }\n\n  function qmyClass_method2(string s)\n  {\n  }\n\n  function qmyClass_method1(int i, int j)\n  {\n    $this-> [will show the methods of qmyClass]\n  }\n}\n\nfunction qmyFunction(int i, int j)\n{\n}\n\n $test = new [will show the qmyClass and includedClass]\n $q = new qmyClass;\n $f_included = new includedClass;\n\n $q-> [will show the qmyClass methods]\n $f_included-> [will shot the includedClass methods[\n ext [CTRL-SPACE will show the externalFunc declared in the included file]\n qmy [CTRL-SPACE will show the qmyFunction declared here]\n $ [will show all the used variables, including the ones from the included files, except of the \"i\" and \"this\" from inside qmyClass]\n?>\n\nClass member variable autocompletion and hints for the function/member arguments will come in the next versions.\nNOTE: autocompletion either pops up automatically in some cases (like when typing $ or ->) or you can invoke manually with CTRL-SPACE."
    author: "Andras Mantia"
  - subject: "Re: PHP"
    date: 2005-06-13
    body: "This is great, but it some what buggy if it not I that do something wrong.\n(This was also posted on the Quantra mailing list)\n\nI notice the following: \nif I do:\n$a = new aClass;\n\nthen start typing:\n$a->\nit will show all functions in the class.\n\nBut the following construction does not work with autocompletion:\n$a = &new aClass;\n$a = new aClass();\n$a = &new aClass();\n\nI don't know if this is something I can set up or if it is in the code. \nWill there be done anything about it? Its not a main couse for me, but rather annoying as the functionality for class compleate obvius is there...\n\n(I am using the one that was included in SuSE 9.3 Distro (3.4) If this is dealth with in a later version I appoligy for the ignoranse.)"
    author: "Aslak Berby"
  - subject: "Re: PHP"
    date: 2005-04-21
    body: "In Zend Studio 4 you can CRTL+click on any user defined function and it will go to the definition itself. "
    author: "SaCuL"
  - subject: "KDE Web Dev future"
    date: 2005-04-20
    body: "KDE Web Dev should be integrated with other KDE components to become even more powerful e.g. KDevelop + Quanta + Karbon + Krita + Kast"
    author: "Anonymous"
  - subject: "about katiuska"
    date: 2005-04-20
    body: "this is the program Eric is talking about http://kde-apps.org/content/show.php?content=17831\n:)"
    author: "Pat"
  - subject: "Not stable"
    date: 2005-04-20
    body: "Quanta seems really good at first but I never been able to use it seriously because it crashed every times I tryed to work with.\nI fill bug reports when I can but the most I can do to help now is to suggest making the app stable before adding new features.\n\nAm I the only one who think quanta isnt stable enough?"
    author: "AC"
  - subject: "Re: Not stable"
    date: 2005-04-20
    body: "Some version (especially 3.2.x) were indeed a little unstable, but 3.3.x and 3.4.0 should be much more stable (aside of some strange bug regarding the cvsservice usage which happens only in binary packages, not if you compile from source)."
    author: "Andras Mantia"
  - subject: "Re: Not stable"
    date: 2005-04-20
    body: "Here same problem. Some version working (when quanta starting) well and latest version I can't use. crash, crash. But Kate is too nice :) Bad taht crashing some syntax highlighting."
    author: "Kristo"
  - subject: "Re: Not stable"
    date: 2005-04-20
    body: "If it's crashes like in http://bugs.kde.org/show_bug.cgi?id=102996 you don't need to report, but read my previous comment and the comments for that bug.\nIn other cases you SHOULD report the crashes, otherwise we cannot fix them."
    author: "Andras Mantia"
  - subject: "Re: Not stable, VPL crasher"
    date: 2005-04-20
    body: "How about this one, Re: VPL crashes\nhttp://bugs.kde.org/99826"
    author: "Rex Dieter"
  - subject: "Re: Not stable, VPL crasher"
    date: 2005-04-20
    body: "Well, as  you could see we (Paulo) are working on it. "
    author: "Andras Mantia"
  - subject: "Re: Not stable, VPL crasher"
    date: 2005-04-20
    body: "We're working on it, though it's a little difficult because I can't reproduce it. I would appreciate if people could tell me other dists where it occurs besides Gentoo."
    author: "Paulo Moura Guedes"
  - subject: "Re: Not stable, VPL crasher"
    date: 2005-04-21
    body: "<BLOCKQUOTE>\nI would appreciate if people could tell me other dists where it occurs besides Gentoo.\n</BLOCKQUOTE>\n\nFedora Core 3, Red Hat Enterprise 4 both exhibit the bad behavior.  However, RedHat 9, Red Hat Enterprise 3 are fine."
    author: "Rex Dieter"
  - subject: "Re: Not stable"
    date: 2005-04-22
    body: "I think if people saying this could actually get perspective they would see the sad humor in such statements. First of all stability in general... People assume because computers are based on logic that they will operate with certainty and consistency rather than inconsistent and uncertain. The first thing you have to ask yourself is this. Do you believe the developers are happy producing a wonderful interface that runs like crap? Seems illogical, right? In fact Quanta almost never crashes for me, and I run the development version. \n\nMost instability I observe is introduced in packaging issues. In particular it is easy to encounter binary issues where programs may run, but run terribly. Some packagers hack up the source a fair amount and introduce problems in the process. Also there have been more than a few releases made of Quanta which were not our official release, or where a bug was found and there was a re-release in days that was not picked up. In addition to this people can have entire systems full of cruft causing problems by hacking things together where configuration and package management fails to adequately manage. Finally there are cases where what a user may do will encounter a bug that the developers don't because of the complexity of the program. These bugs we really need to hear about. Packaging bugs are a pain that we can't fix, but if you can't be sure if it is packaging it's better to report it.\n\n> I fill bug reports when I can but the most I can do to help now is to suggest making the app stable before adding new features.\n\nHere's the facts. I sponsor Andras full time to work on Quanta and he keeps it with one of the lowest bug counts of any application in KDE. During every release cycle roughly 1/3 of our anticipated features fails to get done because we are spending time fixing bugs. Your suggestion may be logical but it's not taking the facts into account. The bugs that are not fixed either are not introduced by us or are simply unknown to us.\n\n> Am I the only one who think quanta isnt stable enough?\n\nSadly I'm sure you are not... but whether it is stable on your system or somebody else's is not solely the result of the source code. Does that make sense now? So I would say you are in a small minority as most people experience Quanta as being very stable. \n\nI have to admit it bothers me when someone says something like this. It operates on the erroneous assumption that we will have a universal experience where too many external factors can break the best of programs. What bothers me is that it implies, however unintentionally, that the developers would intentionally, or through a lack of competence, knowingly produce junk. I doubt you intended to imply that and you should know is that this is not only illogical but not true. Having said that you should know that it is very important to us that you have a good user experience because we are proud of our project. So please help us to root out the problems we can fix and encourage packagers to do likewise."
    author: "Eric Laffoon"
  - subject: "Kommander is Great"
    date: 2005-04-20
    body: "What I really like about kommander is that it allows non-programmers to create useful frontendsto programs (encoders, decoders, etc), like Kaustika, etc.\n\nBut I have not found a GOOD KOMMANDER TUTORIAL (for newbies). Without the knowledge of how to create frontends, Kommander is wasted.\n\nAs it is said, \"You are not cured by just knowing the name of the medicine.\"\n\nGood Luck to Kommander Developers and users :)"
    author: "Fast_Rizwaan"
  - subject: "Re: Kommander is Great"
    date: 2005-04-20
    body: "The links at the bottom of this page (in the \"Useful Links\" section) may help.  At least, they helped me get started.\n\nhttp://developer.kdewebdev.org/"
    author: "ltmon"
  - subject: "BAD Tutorial"
    date: 2005-04-20
    body: "http://hektor.umcs.lublin.pl/~mikosmul/computing/articles/kommander-introduction.html\n\nThe \"Set Wallpaper\" kommander tutorial is very confusing, I can't understand many things there. And there is NO FORMATTING (BOLD, italic, underline, color etc) to make the tutorial easy to understand. It looks that i'm looking to plain text. I guess lack of formatting is what makes that tutorial unhelpful :(\n\nThe Disclaimer says it all ;)\n\nDisclaimer:\nThe author disclaims all warranties with regard to this document, including all implied warranties of merchantability and fitness for a certain purpose.\n\nAnd Certianly it is not fit for learning kommander ;) It's good but lacking formatting."
    author: "Fast_Rizwaan"
  - subject: "Re: BAD Tutorial"
    date: 2005-04-20
    body: "Okay, so you don't like the formatting of the tutorial that a user contributed. Maybe you could offer to help them format it? This is community software, which means you get it for free and people in the community (supposedly all of us are) help out... However it often feels like it means that I sponsor Michal Rudolf to develop it and we keep waiting for others in the community to show up. We had some help with docs and we got some help with tutorials.\n\nAs far as actually learning it, do you want to? Certainly it takes less effort to point out what we haven't done with our limited resources, but we hope not too much more effort to learn to do something. ;-) First of all there are included \"tutorial\" examples where you can see how to use each widget in a simple dialog where it's easy to examine in the editor. (We need to make these more obvious for those getting binary packages.) There are docs on http://kommander.kdewebdev.org, though admittedly they are mostly out of date. We will be working on the docs soon I hope and we welcome anyone who wants to help there. However we also have a mailing list...\n\nOn the Kommander mailing list you can ask any question and get help from the developers and other users. You can ask about features or make suggestions. You can even submit dialogs you're working on for debugging, analysis and advice. Since most small dialogs are under 40K this is easy. In short, we will do penance for our doc shortcomings by serving you directly and personally consulting you until you understand how Kommander works. Funny how people often don't read docs and just fire off a question with Quanta... I personally would pick the free consultant. Kommander is my baby and I will go the extra mile to see it succeed.\n\nOf course all of this overlooks that fact that once you have the basic theory down you need to know very little. There is a function browser you can call from the widget editor which will enable you to construct virtually every DCOP or scripting statement with a point and click, fill in the blank skill set. I use this all the time so I don't have to use the effort to remember things and not make typos. The basic concepts of Kommander are pretty simple so this means you can be making something pretty quickly.\n\nSo if you want to learn how to use Kommander it is in fact pretty easy. Granted it really needs better docs quickly and more tutorials but it is still very easy. I can attest from some of Pat's early emails with Katiuska that he absolutely did not think like a programmer coming to Kommander. After a few \"silly\" questions Pat was on his way. Interestingly I haven't seen a question from him in some time so to me this means that even though our docs are lacking, you can learn to program an application in Kommander in a matter of days with no more than half a dozen questions if you've never written a program of any sort in your life. It's been done and if someone else can do it so can you.\n\nWe are not trusting to luck for you to learn Kommander. We want you to succeed and we are doing our best to make tools available. If you want to succeed join our list and I promise you will be writing Kommander programs in no time. If you want to help that would be nice because we're working hard on what we consider essential improvements to Kommander too."
    author: "Eric Laffoon"
  - subject: "Re: BAD Tutorial"
    date: 2005-04-21
    body: "I'd encourage you to setup a KDEwebdev MediaWiki page, for all KDEWebdev related stuff.\nThe media wiki looks good (nice appearance) and you even can encourage users to improve its contents!"
    author: "anonymous"
  - subject: "Re: BAD Tutorial"
    date: 2005-04-21
    body: "Mediawiki has turned out really well for amaroK:\nhttp://amarok.kde.org/wiki/index.php/Main_Page\nMuch of the documentation (like the FAQ) and all the translations have been written by users. Its also a nice way to hash out development ideas, as an alternative to a mailing list thread.\n\nActually the appearance of mediawiki is one of the problems... its complicated to integrate it the rest of site. Mediawiki was written for Wikipedia, so stuff like that isn't such a priority. However, mediawiki's syntax and ability to organize large pages is well polished.\n\nThat said, the Kommander people really do give great support on their mailing list."
    author: "Ian Monroe"
  - subject: "Quanta is useless for me"
    date: 2005-04-20
    body: "IMHO web developers need a generic XML editor that can:\n1. validate documents using *any* DTD or XML Schema.\n2. suggest and autocomplete tags.\n3. create instances of documents from a schema\nKDE currently lacks such an XML editor.\nI don't like the \"tag soup\" approach of Quanta. It feels like a relic from the past."
    author: "Flavio"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-20
    body: "> IMHO web developers need a generic XML editor that can:\nQuanta as well as Kate XML Tools tries to be a generic XML editor.\n\n> 1. validate documents using *any* DTD or XML Schema.\nIt can do it with the help of xmlval.kmdr (a Kommander script) included with Quanta and present already on the DocBook toolbars. Of course you can use in other cases as well. And of course with a well written DTEP you get on-the-fly validation with the help of the structure tree and the problems view.\n\n> 2. suggest and autocomplete tags.\nIt does that.\n\n> 3. create instances of documents from a schema\nThis is missing.\n\n> KDE currently lacks such an XML editor.\n>I don't like the \"tag soup\" approach of Quanta. It feels like a relic from the > past.\nWhat do you mean about \"tag soup\" approach?? If you mean the .tag description files, we need it otherwise how would we know about tag, attributes and the relation between tags? And you don't have to write those tag files by hand (but you can and are suggested to fine tune them), but use the DTD->DTEP convertion dialog. Schema->DTEP conversion is not possible yet, but if you have a Schema->DTD converting application, you can do this in 2 steps.\n\nSorry, but aside of point 3 you are spreading wrong information."
    author: "Andras Mantia"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-20
    body: "hi andreas,\nyou are certainly right about quanta features. I'm sorry about any false statement :(\nAnyway the validation feature is too hidden and complicated, if you think this is the *most useful* feature for a web developer. By \"tag soup\" I mean that quanta seems designed to produce old style invalid html. It should not even let you save a document that does not validate.\n\n\n"
    author: "Flavio"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-20
    body: "I agree that it may be hidden. Regarding what it produces: it produces whatever you write. ;-) And what is in the .tag files. I don't say that they have all valid tag descriptions, but unless someone notices us about an invalid tag generation we cannot fix as they are just too many tags (DTEP packages) shipped with Quanta.\nAnd altough you can say it's even more hidden it is possible to configure Quanta to check the validity on save/on load/whenever you want with the help of Event Actions."
    author: "Andras Mantia"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-20
    body: "Not let you save an invalid document? I'm sorry, but that doesn't work in the real world ;) Have you ever tried programming a site that has to work in all the different browsers, including that nasty excuse for one Internet Explorer? There's no way of doing advanced designs and have it work in all of them at the same time without some nasty, non-standard IE conditionals. I would very much not like for Quanta+ to enforce that on me. A box explaining that it's non-standard, with that Don't Show Again checkbox we know so well, would work ok for me for this, but forcing it on the user, that's a very non-KDE way of thinking ;)"
    author: "Dan Leinir Turthra Jensen"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-21
    body: "Hogwash! Please check http://csszengarden.com/ and learn how to code to standards. It features a single clean xhtml page (no presentation, no tables, images, etc. ) that is entirely styled by css. Choosing a different design from the menu on the left simply changes the css. This shows the true power of css. Using this technique also makes lynx users happy.\n"
    author: "Jonathan Dietrich"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-20
    body: "Andras didn't mention but Quanta also enables real time structural validation for any DTD if you open the structure tree while editing. As far as Schema validation goes, our initial attempt at reading DTDs was to go with a Schema reading and DTD to Schema conversion. Our preference was to make Schema, not DTD, the basis of our internal validation. The problem is this... suggest to me a mature and functional library for this that we can access and our users can freely access. The tool must be able to handle all scenarios and operate on a diverse set of frameworks for whatever a user's Schema requirements are. If you know where to turn on this let me know. Everything we found was incomplete, often huge and not very versatile. In fact writing Schema tools for targetted use means deciding what your requirements are and picking a library based on that. I was frankly very dissappointed. Schema holds a wonderful promise but as far as I can tell it is not being realized today because it is splintered into divergent ideas and nobody has produced clean universal libraries. Writing the whole library is not practical, especially as when we looked XML Schema was nowhere near set in stone.\n\nAs for your other observation, what version are you running? I noted that into 3.1 and 3.2 our default toolbars reflected old style HTML. That changed in 3.3, but it's still academic as we develop those toolbars with user tools in the interface. All of Quanta has been designed to be completely user extensible... so saying what it is in this regard is reflecting the influence of commercial packaged software to be lazy because when you have it running it's pretty much at it's limits of what can be done with it. Quanta will never reach these limits as most of it's personality is developed at run time.\n\nAs far as enforcing validation on save... you can set Quanta up to do this easily with Project Event Actions. We will _never_ force this on you because I think it's fascist and because there are clear and valid reasons I know it will flood us with bug reports.\n\nThe big problem a lot of people have with Quanta is that it is \"not finished\" in the way they would like. If we take all those issues and put them on the wall how many \"ways\" will we find Quanta should be like? Nearly as many as there are people using it. The potential uses for Quanta are too diverse and given the tools available we have to fill a wide spectrum of uses. The only way such highly personal software can be perfect for you is if you customize it to your perfect ideal. If we do it we will have multiple custom configurations and you will probably still not like one. "
    author: "Eric Laffoon"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-21
    body: "\"It should not even let you save a document that does not validate.\"\n\nThere is an old saying about unix: \"it doesn't try to avoid you making silly things, for if it did, they it would avoid you doing smart things too\".\n\nWho do you thing is Lafoon (to name one) to know about YOUR requirements?  The program, of course should *suggest* and make proper way to be the easy way, but anything out of this would be stupid ingerence.\n\nStill, you can try Windows: there you surely will find the \"This Way Is The One And Only Way\" approach."
    author: "devil advocate"
  - subject: "Re: Quanta is useless for me"
    date: 2005-04-28
    body: "\nAdding my two cents here;\n\nWhat from my perspective would be the perfect solution for this is rigging up the DOM 3 Validation interfaces,[1] which is built for editing Documents while having validness introspection. A large advantage is that it is abstracted from a particular schema language, and allows DTD, W3C XML Schema, RELAX NG, or any other schema language to be used under the same roof. Here's a quote from its Introduction:\n\n'This module provides Application Programming Interfaces (APIs) to guide construction and editing of XML documents. Examples of such guided editing are queries like those that combine questions like \"what does the schema allow me to insert/delete here\" and \"if I insert/delete here, will the document still be valid.\"'\n\nKDE is badly missing a Schema implementation; an implementation which should be generic and not tied to Quanta or any other particular application. Quanta needs it for editing documents, and all \"next-generation\" XML stuff need it for data typing in the XPath data model. In other words, XQuery, XPath 2.0, XSL-T 2.0 all requires a Schema stack.\n\n\nAre you up for the task?\n\nAll it requires is a bit of C++ knowledge, an interest for XML technologies, and to be the one for bringing KDE to the next generation of XML.\n\nThe stack should likely be written against KDOM, kdenonbeta/kdom. There's a design/info doc found at kdenonbeta/kdom/docs/readme.xhtml[2], and folks at #ksvg can help with KDOM specifics. KDOM has everything from XML Catalogs to DOM 3 Load-and-Save, so it is a good base to build upon.\n\n\nCheers,\n\n       Frans\n\n1.\nhttp://www.w3.org/TR/DOM-Level-3-Val/\n\n2\nhttp://webcvs.kde.org/*checkout*/kdenonbeta/kdom/docs/readme.xhtml\n"
    author: "Frans Englich"
  - subject: "Re: Quanta is useless for me"
    date: 2005-07-06
    body: "Great, that's the way! Did anyone already engage in this? \n\nMartin\n"
    author: "Martin Honermeyer"
  - subject: "Ruby on Rails"
    date: 2005-04-21
    body: "Quanta should concentrate on getting support for Ruby on Rails rather than PHP. Get together with Richard Dale and create a kick-ass framework and development platform from desk application development to the server and to the web. If you want to capture Java developers, do that, and integrate better with KDevelop for end-to-end development."
    author: "David"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-21
    body: "I agree it would be a good idea to do something with Rails support in Quanta or KDevelop. I've only got as far as downloading the Rails gem, but I haven't actually tried it out yet. \n\nI was thinking mainly what's needed is adding a Rails project template to KDevelop, and making the KDevelop ruby debugger work with ruby server code running in the background like Rails does, and rhtml syntax highlighting in Kate. \n\nYou create a Rails project dynamically by just typing 'rails <directory name>', so I wasn't sure how fit in with the KDevelop way of doing things. \n\nI haven't used Quanta, so I don't know if that would be a better starting point than KDevelop."
    author: "Richard Dale"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-21
    body: "At some point we hope to merge frameworks so features can be shared. If this is a web development framework and you want to do something with it then by all means touch base with Andras an me."
    author: "Eric Laffoon"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-22
    body: "Yes thanks, OK I'll do that once I've got round to trying out Rails. I've never done any web development, other than a few static html tutorials for Korundum, so I know absolutely nothing about how you go about debugging dynamic web apps and so on.."
    author: "Richard Dale"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-21
    body: "\"Quanta should concentrate on getting support for Ruby on Rails rather than PHP\"\n\nSure it should!\nYou know better than those that indeed are spending their time coding Quanta+.\n\nHey, boys! David orders and you obbey, Arh!\n\n(I'll tell you a secret, David: Quanta+ is distributed under the GPL; you can take the code base and start coding your Ruby on Rails support TODAY!)"
    author: "devil advocate"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-21
    body: "\"You know better than those that indeed are spending their time coding Quanta+.\"\n\nWell, since capturing Java developers is a stated goal according to Eric.....\n\n\"Hey, boys! David orders and you obbey, Arh!\"\n\nErrrrr, no.\n\n\"(I'll tell you a secret, David: Quanta+ is distributed under the GPL; you can take the code base and start coding your Ruby on Rails support TODAY!)\"\n\nOh wow. Really?!"
    author: "David"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-21
    body: "Again, we need developers for such new features. It might be that for basic support it's enough to write a DTEP like the one for PHP.\nBut as I (or the others in the current team) don't work and know Ruby (and have no idea what Ruby on Rails is), we cannot do this alone. And right now we have lot of ideas in the areas that we know..."
    author: "Andras Mantia"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-22
    body: "+1: I fully support this!\n\nRuby has increase his popularity since Rails is there.\n\nRails is a simple MVC web framework, with a flexible controller layer (mixins ~\"AOP\"), responsive UI (AJAX), and a persistent OO model."
    author: "Veton"
  - subject: "Re: Ruby on Rails"
    date: 2005-04-22
    body: "This is less than strategic advocacy statement. I've used PHP since 2x and am moving to 5x and it is the most widely supported scripting language on the net today. However good Ruby may be it is absurd to suggest we change our focus. Think about it. Fortunately we were smarter than to design Quanta where we had to make such choices. Let's deal with facts. Quanta's support for PHP is NOT hardcoded. In fact it is part of our general parser. Support for other languages can be added by writing a Document Type Editing Package and if you collaborate with us we will tune the parser if we need to.\n\nHaving said all that my policy has always been that nobody works on features they don't actually use. My reasoning is simple. One key of FLOSS is that it scratches a developer's itch. When I try to guess where you itch odds are good I'll miss and end up with a mediocre result and we don't do mediocre. Since implementing support for languages is little more than XML and some Regexp, which we can help with, the question is if you are using Ruby on rails and why you aren't adding support? Well?\n\nI realized early on that we would never be able to cover the diversity of needs in the community so I specified that Quanta should be user extensible. It is frustrating that somehow people fail to get the point that asking others to do more and more just because they want it is NOT how FLOSS started and not how it will grow. \n\nAndras very politely said someone needs to step up. I will be a little more blunt. If you want to see a more feature rich program we will need less people saying \"you should do such and such\" and more people getting off their ass and saying \"I want to help add this feature into your framework\". My personal efforts have absorbed enough time to force me to put up a virtual barrier to sucking more time out of my business and with donations in 2005 running 1/3 as many as in 2004 and one less major sponsor I'm looking at thousands of dollars out of my own pocket for the project this year. I look at community like jury duty. Sometimes you have to step forward to make sure something happens. It's getting tiring that seemingly very close to 99.9% of people in the community seem to see work to make things happen as something somebody else should do more of. There are limits to what a few people can give of themselves.\n\nI'm happy to make our project address everyone's wishes, but it would be nice to get a little help doing it. Instead of suggesting we drop work on what we use maybe somebody else could drop some of their free time for a few weeks and help? "
    author: "Eric Laffoon"
  - subject: "Interested in our funding status? Help!"
    date: 2005-04-22
    body: "If anyone is interested... I really loathe having to do this. So far this year we've received 8 donations outside of sponsorships and we have one less large sponsor who was contributing $100 a month. While trying to advance my business and have resource capital I'm into my income for a sizable amount and that is before aKademy. At this rate I will need to come up with thousands of dollars by mid year or consider whether I can have two people sponsored. This year Andras had to do docs and Andras and I are doing the new web site. I see a few posts about how nice Quanta and Kommander are, but some tangible encouragement right now would be nice.\n\nFree software isn't free to produce. I personally spend enough time for a part time to full time job on our project and have put in thousands of dollars of my own money. If a fraction of a percent of our users contributed what amounted to lunch money once or twice a week it would be a huge turn around for us. So far this year we have had to turn down two speaking engagements and an effort to get developers together to merge Quanta with the Kdevelop framework. While I hope to be able to cover thousands of dollars in additional costs for the project by later this year I don't see why I should be under this kind of stress. Our users have been very supportive in the past and I wish I didn't have to keep asking to get that support. The need is pretty much always there.\n\nI can unequivicably say that our current situation is negatively impacting our efforts to bring you the best possible tool.\n\nUsers can help at http://kdewebdev.org/donate.php and I hope you consider a sponsorship as enough of these mean I may eventually be able to stop pleading for help. We really need the level of support we have had in the past, but this time I hope it's not the same people again, but some new people helping."
    author: "Eric Laffoon"
---
Last week, <a href="http://quanta.kdewebdev.org/">Quanta Plus</a> was selected as the Project of the Week on <a href="http://www.osdir.com">OSDir</a>. In the <a href="http://www.osdir.com/Article5016.phtml">interview with Eric Laffoon</a>, the project leader of Quanta+ and <a href="http://kommander.kdewebdev.org/">Kommander</a>, you can read about the past, present and future of the <a href="http://www.kdewebdev.org/">KDE Web Dev module</a>.




<!--break-->
