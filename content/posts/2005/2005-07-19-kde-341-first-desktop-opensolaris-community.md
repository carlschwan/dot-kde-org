---
title: "KDE 3.4.1 First Desktop From OpenSolaris Community"
date:    2005-07-19
authors:
  - "kriddell"
slug:    kde-341-first-desktop-opensolaris-community
comments:
  - subject: "Great work, well done..."
    date: 2005-07-20
    body: "Thank you, Stefan! \n\n@Posters: Please refrain from subtile hits within the Free Software camp. We are all out for the same, and that is computers owned by their owners, not some money-focused companies."
    author: "Some KDEler"
  - subject: "sounds good"
    date: 2005-07-20
    body: "KDE/FreeBSD\nKDE/HURD\nKDE/OpenSolaris\n\n"
    author: "Aranea"
  - subject: "Re: sounds good"
    date: 2005-07-20
    body: "Will there also be a\n\nKDE/Windows\n\nany time soon? Or at least a\n\nKMail/Windows\nKontact/Windows\nKonqueror/Windows\n\n?"
    author: "konqui-fan"
  - subject: "Re: sounds good"
    date: 2005-07-20
    body: "Yes."
    author: "Eha"
  - subject: "Re: sounds good"
    date: 2005-07-21
    body: "It already exists : http://kde-cygwin.sourceforge.net/"
    author: "guillaumeh"
  - subject: "Re: sounds good"
    date: 2005-07-21
    body: "Unfortunately so."
    author: "brockers"
  - subject: "Re: sounds good"
    date: 2005-07-21
    body: "Don't forget PC-BSD and NetBSD-Office."
    author: "Anonymous"
  - subject: "KDE on Solaris : Performances ?"
    date: 2005-07-20
    body: "Is KDE on Solaris faster than KDE on Linux ?"
    author: "Althazur"
  - subject: "Re: KDE on Solaris : Performances ?"
    date: 2005-07-20
    body: "Of course, that has to be tested on the same hardware platform; preferably x86."
    author: "a.c."
  - subject: "Re: KDE on Solaris : Performances ?"
    date: 2005-07-20
    body: "Faster, for what use cases, under which loads?\n\nGenerally speaking no. Linux makes a more responsive desktop today."
    author: "Gonzalo"
  - subject: "Re: KDE on Solaris : Performances ?"
    date: 2005-07-20
    body: "yes, and this is getting better. a tool is now in development[1] to measure responsivity and interactivity, so the in-kernel scheduler can be improved, and points of failure can be found.\n\n[1] http://ck.kolivas.org/apps/interbench/\nby Con Kolivas, http://members.optusnet.com.au/ckolivas/kernel/"
    author: "superstoned"
  - subject: "Re: KDE on Solaris : Performances ?"
    date: 2005-07-21
    body: "http://people.redhat.com/mingo/realtime-preempt/\n\nis probably what you really mean. \n\nCon is just writing a tool to messure stuff. the real hard work is being driven by Ingo Molnar and the rt people.\n\n"
    author: "masuel"
  - subject: "Re: KDE on Solaris : Performances ?"
    date: 2005-07-21
    body: "eh eh. realtime preemt IS important, but Con is also writing a CPU scheduler that is, in design, better than mainline. it hasn't been included in mainline, at first because it didn't get enough testing, now mostly because 'there already is a scheduler', altough that scheduler has far more code and complexity for the same thing (con's scheduler, staircase, tries to be interactive by design, not by 'hack-on'). maybe, when this measure tool is completed, mainline and Con can both tune their schedulers, and we'll see who is best...\n\nfor an interactive feel, many parts of the kernel are important. short codepaths are important, and pre-emption makes this possible. but if the wrong process then gets cpu, or it is wasted by kswapd - it won't feel very responsive... so we need at the very least a good CPU scheduler (current one is nice, but it can be improved), further work on CFQ (IO scheduler) and a better swap subsystem, as linux slows down to a crawl if you use more than 100% cpu (windoze handles this absolutely a lot better)."
    author: "superstoned"
  - subject: "Re: KDE on Solaris : Performances ?"
    date: 2005-07-21
    body: "not cpu, of course, but memory..."
    author: "superstoned"
  - subject: "Project Overview -\nOur principles"
    date: 2005-07-20
    body: "What I really like there in Opensolaris principles, This is what I consider true free software and free software developers and users.:\n\nWe will be inclusive. \n---------------------\nProposals will be evaluated based on technical merit and consistency with overarching design goals, constraints, and requirements.\n\nWe will be respectful and honest. \n---------------------------------\nDevelopers and users have the right to be treated with respect. We do not make ad hominem attacks, and we encourage constructive criticism. Our commitment to civil discourse allows new users and contributors with contrarian ideas an opportunity to be heard without intimidation.\n"
    author: "fast_rizwaan"
  - subject: "NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-20
    body: "Why remove the feature of Win key to k start menu? KDE going worse!?"
    author: "jay presco"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-20
    body: "Because cheating in games is considered to be unfair."
    author: "pay jesco"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-20
    body: "Because it was very unpopular, and took some huge hacks (option keys are not reported alone like normal keys). \n\nOnce it was deactivated as default, it was removed."
    author: "Allan"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "why discuss this here in this story instead of on bugs.kde.org or even panel-devel@kde.org where it's on topic? ;)\n\nas allan noted, it wasn't very popular and was a fairly nasty set of hacks. perhaps because it screwed with other people's key bindings. this isn't ms windows, after all. you can get this behaviour back, though, by unmapping the win key as a modifier in your X config and binding it to open the menu.\n\ni also find it amazing how any time any feature, no matter how broken, is changed, someone whinges about how it's a sign that kde is somehow going to the dogs. see: http://aseigo.blogspot.com/2004/11/features-drugs_23.html"
    author: "Aaron J. Seigo"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "We people aren't usability experts or hackers like you people.  We just want things to work.  \n\nWTF cares what hacks go into it?  When I press the menu button I want the menu.  Pretty damn simple.\n\nUnfortunately KDE will never understand this desire of normal people for things to just work, or our hatred and frustration when often used features  suddenly disappear without a trace, since it is made up of usability \"experts\" and hackers with a different world view from ours."
    author: "ac"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "Did you read what he said?? Its configuration stuff, so ask your distributor or learn how to config your system that it works without hacks:\n\nDelete the Win modifier:\nxmodmap -e \"clear mod4\"\n\nRemap Win key as e.g. F16:\nxmodmap -e \"keycode 115 = F16\" \n\nNow use KControl->Shortcuts to let KDE know that F16 is alternative for Alt+F1\n\nThats all. Bye\n\n  Thorsten\n\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "Assigning F16 to Win key does not allow other combination of Win+<key> for other shortcuts. like I can't have Win+E to 'launch HOME' or Win+W to konqueror or Win+q to konsole.\n\n\n>Aaron J Seigo wrote:  this isn't ms windows, after all.\n\nOh yeah dude, KDE is not ms windows, let's remove anything which resembles ms windows...\n\na) \"System\" which is like Windows' \"My Computer\". \nb) \"Konqueror\" which is like Internet Explorer+Windows Explorer,\nc) \"kolourpaint\" like mspaint.\nd) \"KOffice\" which is like MS Office\ne) \"Find files/folders\" which is like Find of windows 95.\n\nAnd your ass which is like Bill Gates!"
    author: "Jay Presco"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "While I agree with you that it should be possible to have the Win key as a single key and as a modifier (when Windows can do it, why shouldn't we be able to), you don't help your point by attacking people personally."
    author: "Christian Loose"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "My keyboard has two Win-Keys.\nIf you think KDE is Windows better stay by the original.\n\nBye\n\n  Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "> let's remove anything which resembles ms windows\n\nthat's not at all what i meant. so let me be a bit clearer for you: because KDE is not ms windows, we aren't beholden to follow it's every step. we can have things that may resemble windows for various reasons (ease of migration, they are good ideas, etc) but we don't have to copy everything (esp the bad ideas).\n\n> And your ass which is like Bill Gates!\n\nhaha. wow. best end to a tantrum on theDot yet!"
    author: "Aaron J. Seigo"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "Now now. If you know a sane way to re-implement your favored Win key behavior feel free to attache a patch here. The previous implementation was removed exactly because it was a very ugly hack and incompatible not only with KDE but also with the X Window System, nothing else."
    author: "ac"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "Sorry but this is just plain awful.  KDE simply does not get it at times."
    author: "ac"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "As others have said, it is a common confusion that KDE aims to please the user.  KDE is here to tickle developers, not users.  Without developers KDE is nothing, but without users KDE goes on its merry way.  Windows on the other hand purely exists to make as much money as possible, and this is not possible without loads of users.  Windows cares much more about users than it does developers. \n\nSo if the decision is between making users happy or developers happy, Windows chooses users and KDE chooses developers.  Hands down.  The focus is simply different in the Unix world - it's optimized for programmers.  \n\nJoel covers this topic very nicely:\n\nhttp://www.joelonsoftware.com/articles/Biculturalism.html\n\nIt's an interesting read and should tell you why Unix/KDE will never be for the user."
    author: "KDE User"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "Nice flamebait you set up there, but how does that fit with all the feature requests by users KDE developers implement all the time?! Most KDE developers aim to please users if it can be done while keeping the code standards high. Ugly hacks which drop the code standard get removed after some time, and that's what just happened here."
    author: "ac"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "This is *exactly* what I mean, sorry for any confusion.  It's a matter of priorities.  On Unix the priority is to optimize for programmer and on Windows the priority is to optimize for the user.  \n\nOne is not wrong and the other right.  It's just different philosophies.  I strongly recommend you read the Joel article."
    author: "KDE User"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "This kind of post is hilarious. I mean, I use it. It fits me. Is it that because every once in a while I write code I am not a user?\n\nI install Linux for dozens or hundreds of people a month. They use it. Many (most) like it. Are they not users because they use it at work?\n\nSure, I think I know what you mean (I also think it's mosty crap, but what the hell). I suppose \"Unix is not adequate for this specific kind of user I have in mind\" is not so satisfying to your inner troll.\n\nBut really, tone it down, dude, because you are not communicating."
    author: "Roberto Alsina"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "> KDE is here to tickle developers, not users.\n\nwrong. KDE is here to make development of it appealing to developers. the interface is here to server *users*, however. while there will be some users who don't feel well served (one shoe does not fit all feet), we do aim for the broadest user applicability possible.\n\nso while we do keep in mind that we mustn't chase off developers (but, for instance, requiring them to be slaves to every idiot on bugs.kde.org or else have their svn account revoked as if it were a corporate job), we do write our software for users.\n\n> http://www.joelonsoftware.com/articles/Biculturalism.html\n\nplease. ESR is irrelevant to the desktop and hardly relevant to linux or any other open source operating system in general. i disagreed with many of the points in his book and found it laughably out of date as far as his perspective on software development goes.\n\nESR does not represent me when it comes to devel, and i doubt he represents many (if any!) of the KDE devels in this manner either."
    author: "Aaron J. Seigo"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "> We people aren't usability experts or hackers like you people. We just want things to work. \n\n> WTF cares what hacks go into it? When I press the menu button I want the menu. Pretty damn simple.\n\n> Unfortunately KDE will never understand this desire of normal people for things to just work, or our hatred and frustration when often used features suddenly disappear without a trace, since it is made up of usability \"experts\" and hackers with a different world view from ours.\n\n\nThis is why you don't get it.  It was a hacked up feature, that wasn't even popular.  The thing is, KDE will survive without users, but it _cannot_ survive w/o developers.  So, if an underused feature reaks of bad design and problematic implementation, the only logical step is to remove it.  If KDE devels tried to hack up every feature into it that everybody wants, no matter how troublesome or useful it is, without thiking of the big picture, design, and overall quality of the architecture; KDE would not be nearly as appealing to work with, and there wouln't be as many people contributing to it.  In the end the user would lose."
    author: "Anonymous"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "> WTF cares what hacks go into it?\n\nthe users for whom it crippled KDE's key bindings, that's who. we happen to have more users than just you. given that you feel very strongly about supporting users, i hope you can understand why trying to support them is important to us.\n\n> Unfortunately KDE will never understand this desire of normal people for\n> things to just work\n\nnot only is this wrong, but it's completely meaningless. there is no monolithic \"KDE\", there is a large community of developers of which i am but one. and many of us DO understand the desire for things to \"just work\". that hackneyed phrase, however, has NOTHING to do with the \"win\" key popping up an app menu.\n\n> our hatred and frustration when often used features suddenly disappear\n\ni understand that very well. i've kept many features in kicker that are a paint in the rear to maintain and which i personally don't even like because of this. when something gets removed, there is a very good reason behind it."
    author: "Aaron J. Seigo"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "Let me repeat what other people have said: \"KDE IS NOT WINDOWS!\"\n\nTo clarify, Windows is not a standard that must be followed, it's merely another desktop. The Win key is not the \"menu\" key, it's only applicable to Windows. The only reason you have it on your keyboard is because Microsoft is a monopoly.\n\nThe only reason it seemes intuitive to you is because you've been trained on Windows. If you had been trained on Mac OSX or Solaris CDE (to keep on topic) then the behavior of the Win key would seem very odd. Do not confuse familiarity with usability.\n\nIf you want a clone of the Windows desktop, KDE is not it, so try elsewhere. But don't try GNOME either because it isn't a clone of Windows either."
    author: "Brandybuck"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "You might have luck with Lindows, which is the best of both worlds = KDE & Windows.  Should be ideal for this situation."
    author: "KDE User"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "I'm not a usability expert, nor a KDE hacker.\n\nI don't want the windows key active.  I hate it.  I was extremely pleased when that annoying 'feature' was removed from KDE.\n\nI hated microsoft for adding those keys in the first place.  They made my spacebar smaller.  I really love it when I can get my hands on keyboard without those annoying bastard-keys.\n\n"
    author: "arcade"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2007-05-20
    body: "I can't believe you jerks.  If you're not going to write a patch and help this guy out then shut up.  This is a GREAT feature, two years later, people still can't use a modifier as a shortcut.  This thread should be 1 post, the original poster asking for the feature.. and then maybe me 2 years later, a post going \"huh, still not done, I really want this\".   Instead, every other person in this thread is an IDIOT.  You kids probably tell your friends that the above \"work\" makes you KDE contributors.  Who cares HOW he asks for the feature, if you can read you can pull out the important information and ignore the rest and just be helpfull."
    author: "confused"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "I hate you and everyone that looks like you!!11\nin Winblows the win key works why cant you do it!!11!\nthis is why linux wont succeed on the desktop!1!"
    author: "JohnFlux"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "> in Winblows the win key works why cant you do it!\n\nMaybe your keyboard is just incompatible to Linux? You need one with a Linux key. :-)"
    author: "Anonymous"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-21
    body: "I've got here a Cherry keyboard and infact the Windows key was replaced with a Linux key out of the box! :) But what makes me suspicious is that on the back of the keyboard is an inscription that says \"The Windows logo is a registered trademark of Microsoft corporation\". Maybe Cherry forgot to rework the back of their keyboard when the redesigned it for Linux... :)"
    author: "Patrick Trettenbrein"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-22
    body: "so, is that penguin key of yours still working?\n\n;)"
    author: "bangert"
  - subject: "Re: NO WIN key in KDE 3.4.x!!!"
    date: 2005-07-22
    body: "Yes, but just because of a little tool that's shipped with the keyboard kalled 'Kkeyman'."
    author: "Patrick Trettenbrein"
---
KDE 3.4.1 is the first modern desktop environment being compiled, packaged and working fully on the <a href="http://www.opensolaris.org/">OpenSolaris</a> platform. The work has been mainly <a href="http://www.opensolaris.org/jive/thread.jspa?messageID=4385">done by our friend Stefan Teleman</a>. While KDE is known to compile out of the box on Solaris with GCC, using the Sun ONE Studio 10 Compiler still presents a challenge which requires a lot of patches. A <a href="http://impliedvolatility.blogspot.com/">list of georgeous screenshots</a> is probably what makes lots of people think <a href="http://blogs.gnome.org/view/calum/2005/07/18/0">"KDE seems to be ahead of the game already"</a>. Read on for an interview with KDE on Solaris lead Stefan Teleman.


<!--break-->
<p>In the discussions on the OpenSolaris forum it is quite interesting to see how delighted long-time CDE users react to their KDE initiation, and an inspiration to see many of the fulltime <a href="http://www.opensolaris.org/jive/thread.jspa?threadID=1151&tstart=15">Sun engineers congratulate the KDE developer community</a> as well as Stefan for this significant achievement. This release is only the <a href="http://www.opensolaris.org/jive/thread.jspa?threadID=1227&tstart=15">beginning of a long-term cooperation</a> with various developer groups working for a stable desktop offering in OpenSolaris, that includes <a href="http://solaris.kde.org">KDE on Solaris</a> as a first-class ingredient.</p>

<p><strong>To find out more about the KDE on Solaris project KDE Dot News spoke to Stefan Teleman about how he got started, why he loves Solaris and the new OpenSolaris Desktop project.</strong></p>

<div style="float: right; border: thin solid grey; padding: 1ex; width: 160px">
<img src="http://static.kdenews.org/jr/stephan-teleman.jpg" width="160" height="120" /><br />
Stefan Teleman, KDE on Solaris Leader
</div>

<p>A lot has happened since <a href="http://dot.kde.org/1064340634/">KDE first compiled on Solaris</a>. </p>

<p>Sun liked my work on getting KDE compiling with Solaris, so at the end of January of this year I was invited by them to join the OpenSolaris Pilot program. This then became the main site for OpenSolaris, the Free Software version of Solaris.</p>

<p>As a member of the OpenSolaris Pilot, I have been building KDE for
both SPARC and Intel/AMD. KDE is <em>enormously</em> popular amongst Solaris
users, so there have been a lot of expectations (as well as pressure and nervousness on my part) for the KDE Solaris (SunStudio compiler/native) ports,
from both inside and outside Sun.</p>

<p>This has the potential of becoming something very big for KDE.
There is an established expectation at OpenSolaris that i will provide
the KDE Solaris builds for both Sparc and Intel, with Sun Studio.
Sun gave me free licenses for SunStudio specifically to build and
maintain KDE, and serveral engineers at Sun from the kernel and
compiler teams have helped me a lot with my Solaris ports. So it is
truly a joint effort at this point, and Sun deserves a lot of credit
for how open and helpful they have been.</p>

<p>I do all this in my free time, because my real day job doesn't involve open source or KDE at all.  I live and work in New York City where I
write software for trading and valuing.  So, given the rather dry and stressful nature of my daily endeavours,
I need some creative counterbalance to all this. This is where KDE
comes into play. It's fun to work on, and it runs on Solaris, which
is my favorite UNIX. It's also quite difficult to port, because KDE
is not originally written for the Sun's compilers.</p>

<p>Why do I like Solaris? First and foremost it's
the best UNIX out there. Says me. Second, I've been writing
code on Solaris for about 10 years now, and before that SunOS. So part
of it is habit and part of it is really rather biased: when
you get used to working in a certain environment for 10+ years, every
day...  and this feels like the best possible fit ... how
easy would it be to let go of it? There's a link in Solaris to a not
so distant past when software, hardware and everything around them
were different. I was a small part of those times. And part of it
is because i like Sun as a company. They easily get a bad reputation, and
people tend to forget how much they have done for computing
and software engineering, and still do.</p>

<p>Why I like KDE? Because it looks beautiful, it's easy to use and because it's written in C++. Plus I have fun hacking on it.</p>

<p>There's something really interesting happening at OpenSolaris and KDE is part of it. So, i want to invite you to our <a href="http://www.opensolaris.org/jive/category.jspa?categoryID=29">discussion list</a>, drop by and say hi and maybe sign up and join. We want to build the coolest desktop for OpenSolaris.</p>





