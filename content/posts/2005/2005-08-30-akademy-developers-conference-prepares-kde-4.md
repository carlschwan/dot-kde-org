---
title: "aKademy Developers Conference Prepares for KDE 4"
date:    2005-08-30
authors:
  - "prodrigues"
slug:    akademy-developers-conference-prepares-kde-4
comments:
  - subject: "Labels vs dirs/files"
    date: 2005-08-29
    body: "Just wondering, are labels planned to replace (or be provided optionally) over the current directory/files handling paradigm for KDE 4."
    author: "blacksheep"
  - subject: "Re: Labels vs dirs/files"
    date: 2005-08-30
    body: "Well, the Tenor technology will be integrated into various apps that will easily allow stuff like labels. Dirs/files are here to stay though."
    author: "Ian Monroe"
  - subject: "Re: Labels vs dirs/files"
    date: 2005-08-30
    body: "What, no \"virtual folders?\" ;)"
    author: "SuSE_User"
  - subject: "Scribe"
    date: 2005-08-30
    body: "From Google's cache:\n\nhttp://64.233.187.104/search?q=cache:l_QDB6Or4YYJ:ftp.troll.no/products/qt/whatsnew.html\n\n<<\nScribe: The Unicode text renderer with a public API for performing low-level text layout\n The new text engine, Scribe, offers a public API for high-quality Unicode word processing with real WYSIWYG (What You See Is What You Get). It has been designed from the ground up bearing multilingual challenges in mind. One of Scribe's most visible benefits, unavailable in Qt 3, is proper font kerning on all platforms.\n>>\n\nBut now not there:\n\nhttp://www.trolltech.com/products/qt/whatsnew.html\n\nDoes anybody know what happened to WYSIWYG?\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Scribe"
    date: 2005-08-30
    body: "no cairo either?"
    author: "pewpew"
  - subject: "Re: Scribe"
    date: 2005-08-30
    body: "That doesn't mean there can't be in the future as Cairo matures, there's just no point in it right now.\n\nIt's amusing that some people go around acting as if Cairo is some sort of standard people are just expected to use."
    author: "Segedunum"
  - subject: "Re: Scribe"
    date: 2005-08-30
    body: "WYSIWYG is a trademark as far as I know."
    author: "Matze"
  - subject: "Re: Scribe"
    date: 2005-09-03
    body: "Actually, Scribe is a trademark (and an actual product) which nobody uses anymore.  Commercial product in 1986-1990, with syntax similar to TeXinfo.  Oddly enough, it's a software product for which there's very little info on the web -- but was used extensively at my school."
    author: "Krishna Sethuraman"
  - subject: "Re: Scribe"
    date: 2005-09-07
    body: "WYSIWYG is a mode for printers, which was programmed a long time ago...\n\nit means: What You See Is What You Get..\n\nthat means: What you see on the Screen is what you get on the Paper.\n\nOld printers, if you know, have functions to choose the type of font.\nToday you do it by the editor.."
    author: "codein"
  - subject: "QT 4"
    date: 2005-08-30
    body: "I am generally impressed with the new features of QT4, but I am very disappointed with the new designer application. Designer seems to have lost some significant features, here are just a few missing features (that were available in the previous version):\n\n* Menu Editor\n* Action List\n* Toolbars\n* Function (Slot) Editor\n* Source Editing\n\nThese are critical features. If there is some miracle option that unhides all this functionality then please let me know. Other wise, WHY? The new interface is great, but functionality wise it cannot compete with the previous version.\n\nI don't want to be seen as a critic, the new libraries are excellent. If these feature are still to be added it seems that the new version of QT 4 was released before it was completed."
    author: "Ian Whiting"
  - subject: "Re: Qt 4"
    date: 2005-08-30
    body: "The \"source editing\" part is clear. Qt Designer of Qt4 is meant to be integrated in existing IDEs (Integrated Development Environments), like KDevelop for KDE or Visual Studio for MS.\n\nFor the other points, probably the same reason apply.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Qt 4"
    date: 2005-08-30
    body: "All that's pretty clear, but it now does put a bit more pressure on KDevelop (or another IDE) to be good enough to work with Designer for Trolltech ;-)."
    author: "Segedunum"
  - subject: "Re: QT 4"
    date: 2005-08-30
    body: "as the other person noted, you're confusing designer with an IDE ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: QT 4"
    date: 2005-08-30
    body: "I think that the trolls confused themselves at first in the 3.x series. ;-)"
    author: "Andras Mantia"
  - subject: "broken links?"
    date: 2005-08-30
    body: "Is it only me or are the Transcript links to the Wiki broken?\n(just trying to open \"http://wiki.kde.org\" yields the same results):\n\nWarning: mysql error: Duplicate entry 'fe954ec8a68dc6cbf90cb676c6e02abf' for key 1 in query:\n\ninsert into `tiki_sessions`(`sessionId`,`timestamp`,`user`) values(?,?,?)\n\nin /home2/wikikde/public_html/lib/tikidblib.php on line 134"
    author: "Michael"
  - subject: "Re: broken links?"
    date: 2005-08-30
    body: "It happens to everyone at the moment."
    author: "Anonymous"
  - subject: "Re: broken links?"
    date: 2005-08-30
    body: "It works again."
    author: "Anonymous"
  - subject: "10 seconds?"
    date: 2005-08-30
    body: "Hey, it wasn't 10 seconds :). It was 6 seconds, and only because the damn laptop decided it wasn't going to do it in 5 like it had done repeatedly during my testing.\n"
    author: "Lubos Lunak"
  - subject: "Re: 10 seconds?"
    date: 2005-08-30
    body: "I'd very much like to know more about this!  Given that none of the transcription links seem to be working at the moment, I can't read up on it.  :-(\n\nPerhaps you could post a short summary of what changes you made to get your 5 (or 6) seconds.\n\n-- Steve"
    author: "Steve"
  - subject: "Re: 10 seconds?"
    date: 2005-08-30
    body: "You can start with the KDE performance tips page (http://wiki.kde.org/tiki-index.php?page=Performance%20Tips - doesn't work at the moment just like the rest of the wiki).\n"
    author: "Lubos Lunak"
  - subject: "Re: 10 seconds?"
    date: 2005-08-30
    body: "Hi Lubos - sorry, I forgot exactly how many seconds it was, and I didn't know of any notes that anyone had made. I'll get it right next time :-)"
    author: "Philip Rodrigues"
  - subject: "Re: 10 seconds?"
    date: 2005-08-30
    body: "I was watching on stream and it was a really good presentation. Your talk was very entertaining! You got good skills for presentation, I even raised my hand on questions :-)"
    author: "jumpy"
  - subject: "Back to Reality"
    date: 2005-08-30
    body: "This I found interesting from Eirik Chambe-Eng's keynote:\n\n\"ISVs want good support and quality, their sales people need to stress that even though it's open source it's backed by a commercial company.\"\n\nThat certainly rings very true with my experiences. Note that it is sharply different to the 'businesses and ISVs want to develop for nothing' mantra that some people have been pushing over the years."
    author: "Segedunum"
  - subject: "Re: Back to Reality"
    date: 2005-08-30
    body: "I guess you are reflecting on some heavy flame fests recently on OSNews? :)\n"
    author: "Chakie"
  - subject: "Re: Back to Reality"
    date: 2005-08-30
    body: "No, and I don't see why there were any heavy flame-fests because it is something that rings so true it's almost unreal. I've just picked up something sensible in the transcript from Eirik that I happen to agree with strongly. ISVs want to see strong, solid and reliable development software backed up with good support in the open source world. They are somewhat less interested in being able to develop everything for nothing (but that doesn't mean you shouldn't be able to and not have to use Qt though - something he actually came out and said from the transcript).\n\nTrolltech are a decent sensible company, making good money and have a solid target market and because of that are able to invest in KDE and open source software in a realistic way. Massive fair-play to them and to people like Aaron Seigo doing great work and all of us users who are benefitting. I see no reason whatsoever in the whole universe why that is not 110% sensible. I think they will now rightly reap the rewards of sticking by KDE and putting effort into GPLing Qt when other companies would almost certainly have cut and run with all the stuff flying around.\n\nI just happen to have strong views on the subject as sensible business models, especially with open source software and companies, are dear to my heart. The wise man built his house upon the rocks sort of thing. If other people think otherwise, well, that's up to them so it's not going to start here again ;-)."
    author: "Segedunum"
  - subject: "kdemm supporting gstreamer/nmm"
    date: 2005-08-30
    body: "So will kde4 have equal (audio/video) software packages (old kde packages with added gst-specific options?) to gstreamer's own packages, like gst-editor/gst-monkeysaudio/gst-player/cupid?\n\n\nPs. Does the kdepim4 going to have support for commonly available gps hardware via roadmap/gpsdrive fork, or for example ipodslave for ipodders as http://developer.kde.org/development-versions/kde-4.0-features.html don't have any mentioning about those, even as gps and ipod is a must for anyone living today... ;-)"
    author: "Nobody"
  - subject: "Re: kdemm supporting gstreamer/nmm"
    date: 2005-08-30
    body: "is this a hidden software request from someone who wants more free software ?"
    author: "ch"
  - subject: "Re: kdemm supporting gstreamer/nmm"
    date: 2005-09-01
    body: "Yep, yep.\n\nWould really really like to have such software integrated to a base kde as they (in my mind anyway) are the base equipment every geek should have, and at the moment it's pretty silly to need to compile bunch of backends to get few functionalities working between separated gtk/Qt apps. :-)\n\nWhy I keep myself anonymous is of course as I don't have enought free time to code (yet, maybe if I get fired some day...) on kde. :-(\n\nThat's why I would like to see an official support, rather than waiting for a long time until somebody like me (with only average C++ pseudo-knowledge) make some half-working piece of \"code\". :-D"
    author: "Nobody"
  - subject: "Re: kdemm supporting gstreamer/nmm"
    date: 2005-08-30
    body: "\"equal software packages\"?"
    author: "Ian Monroe"
  - subject: "Videos of these talks?"
    date: 2005-08-30
    body: "How hard would it be to record all/some of these talks at these events and distribute them over Bittorrent? There have to be lots of developers out there that would like to go to these but can't. A good video of these talks would be a great way to increase interest in these events. Or you could even sell DVDs and use the money for sponsorships of developers to either develop or go to future events. Just an idea."
    author: "Mateo"
  - subject: "Re: Videos of these talks?"
    date: 2005-08-30
    body: "The talks are up on the website.  Not sure where exactly, but they are there."
    author: "JohnFlux"
  - subject: "Re: Videos of these talks?"
    date: 2005-08-30
    body: "You can see live streams at http://stream.fluendo.com/akademy/, and the edited downloads will be available later.\n\nJohn."
    author: "odysseus"
  - subject: "Re: Videos of these talks?"
    date: 2005-08-31
    body: "Raw videos are now being uploaded, you can find them at http://ktown.kde.org/akademy2005/\n\nJohn."
    author: "odysseus"
  - subject: "skip kde4"
    date: 2005-08-30
    body: "how about skipping v.4 and going right on to kde 10.\n\nit sounds like the features coming up will easily justify a jump to kde10 :)"
    author: "john"
  - subject: "Re: skip kde4"
    date: 2005-08-31
    body: "Plain version numbers are passe. KDE is way behind the times, really; they've yet to go through the Windows evolution of:\n\n1) Dropping real version numbers in favor of naming things after years.\n2) Dropping years in favor of \"acronyms\" like XP ~ eXPerience.\n3) Dropping weird acronyms in favor of a randomly selected name (Vista).\n\nIf Linux ever wants to be taken seriously on the desktop, we need to drop sequential version numbering and go to these move innovative branding techniques! :)\n\nOf course, we could also go the Sun route, and say that KDE 4.x will be referred to as KDE 10. That seems to be working out well for them."
    author: "Dolio"
  - subject: "Re: skip kde4"
    date: 2005-08-31
    body: "Or we could do a mix of Apple and MS! :D\nKDE 4 -> KDE IV -> KDE Ivory"
    author: "Davide Ferrari"
  - subject: "Re: skip kde4"
    date: 2005-08-31
    body: "X means \"Sex\" among some marketing believers.\n\n\n\n\n"
    author: "Matze"
  - subject: "Re: skip kde4"
    date: 2005-09-01
    body: "Yeah, lets name KDE after gases (I'm serious!):\n\n Neon - KDE 4.0\n Argon - KDE 4.1\n Krypton - KDE 4.2\n Xenon - KDE 4.3\n Radon - KDE 4.4\n"
    author: "Willie Sippel"
  - subject: "Re: skip kde4"
    date: 2005-09-03
    body: "I like this proposal!\nPlease remember those names when people will be deciding the naming.\nReally cool stuff.\n"
    author: "jumpy"
  - subject: "Re: skip kde4"
    date: 2005-09-03
    body: "Or better yet, just look at http://periodic.lanl.gov/ or kdeedu::kalzium for ideas for numbering aliases (if kde decides to go that road)."
    author: "Nobody"
  - subject: "Re: skip kde4"
    date: 2005-09-04
    body: "Eum, you forgot Helium:\n\nHelium - KDE 4.0\nNeon - KDE 4.1\nArgon - KDE 4.2\nKrypton - KDE 4.3\nXenon - KDE 4.4\nRadon - KDE 4.5\n\nThen we should switch to Sun-versioning ;-)\n\nKDE 6 - KDE 4.6\nKDE 7 - KDE 4.7\nKDE 8 - KDE 4.8\n..."
    author: "Leo Savernik"
---
The 2005 KDE aKademy continued today with the opening of the
developer conference: two days of talks describing upcoming KDE
technologies, giving programming tips and, of course, plenty of
informal hacking and discussion sessions between the developers.  Today's talks included they keynote from Trolltech, a new multithreading scheduler library, meta-programming revisited and how to boot to KDE in 10 seconds.  Read on for more.





<!--break-->
<div style="padding: 1ex; border: thin solid grey; float: right">
<a href="http://static.kdenews.org/jr/akademy-2005-group-photo.jpg"><img src="http://static.kdenews.org/jr/akademy-2005-group-photo-wee.jpg" width="300" height="175" /></a><br />
<a href="http://static.kdenews.org/jr/akademy-2005-group-photo.jpg">aKademy 2005 Group Photo</a>
</div>

<p>Eirik Chambe-Eng of Trolltech opened the proceedings with a
keynote describing the company's progress in the past year, along with
some of the ways in which KDE and Qt help one another: KDE provides a
huge user base for Qt, while Trolltech returns the favour by
sponsoring several key KDE developers.</p>

<p>For those following the plans for KDE 4, Matthias Kretz explained
how the multimedia framework will develop (<a href="http://wiki.kde.org/tiki-index.php?page=Multimedia+API+Talk">Transcript</a>, <a href="http://conference2005.kde.org/slides/multimedia-api--matthias-kretz.pdf">Slides</a>). Work on designing the
framework has been ongoing since last year's aKademy in Ludwigsburg,
where ideas about the way to improve on or replace the powerful-but-ageing aRts
sound system were discussed. The main feature of the new 'KDEMM'
framework is a single KDE  API for multimedia access which will be
able to talk to multiple backends, such as aRts, <a
href="http://www.networkmultimedia.org/">NMM</a> or <a
href="http://gstreamer.freedesktop.org/">gstreamer</a>. 

<p>Coming up to lunchtime, performance was the theme, with Mirko B&ouml;hm
describing ways to use multithreading in KDE applications (<a href="http://wiki.kde.org/tiki-index.php?page=Multithreading+Talk">Transcript</a>), while in
the second track, Lubo&scaron; Lu&#328;&aacute;k presented a talk entitled 'KDE
performance'. At the moment, multithreading is rarely used in KDE, but
Mirko hopes to change that &ndash; where it makes sense and improves
the user experience &ndash; with
his experimental ThreadWeaver library. Lubo&scaron; showed how both
'real' methods and 'cheats' can be used (and already are) to improve
the performance of KDE applications. Most impressive was his
demonstration of a 900MHz laptop starting KDE in 10 seconds. He
admitted that this used some cheats, but it showed how there are many
ways to increase the speed of KDE and its applications.</p>

<p>Returning from lunch in the the nearby part of town, conference
attendees were disappointed to find Zack Rusin's intriguingly-titled
"Beauty and Magic for KDE developers" talk rescheduled to Wednesday
because of some technical problems, but Aaron Seigo stepped in to
present a demo of the new features of Qt Designer in version 4, and
how they can be best used to easily create usable dialogs for KDE
applications (<a href="http://wiki.kde.org/tiki-index.php?page=School+of+Designer+Talk">Transcript</a>).</p>


<p>Closing the day's presentations were demonstrations of three
new technologies: Simon Hausmann explained how Qt 4's "Scribe" text
rendering engine can be used for advanced features in KDE, while
Albert Astals Cid demonstrated the Poppler library (<a href="http://conference2005.kde.org/slides/poppler/index.html">Slides</a>, <a href="http://wiki.kde.org/tiki-index.php?page=Poppler+Library+Talk">Transcript</a>), which provides PDF
rendering, and is planned to be included in KPDF in the future. In the
third track, Mathieu Chouinard presented his work on M2, a tool for
deploying and managing multiple systems on a network.</p>

<p>With the talks over, the developers retired to the computer rooms
to keep working on their respective areas of KDE, or took the
opportunity to enjoy the warm temperatures and sun of M&aacute;laga, before
another full day of talks and presentations tomorrow.</p>





