---
title: "KStars Donates Prize Money to KDE"
date:    2005-05-15
authors:
  - "jriddell"
slug:    kstars-donates-prize-money-kde
comments:
  - subject: "That's great"
    date: 2005-05-15
    body: "Many thanks from me to the KStars team, that's really nice to hear."
    author: "Bram Schoenmakers"
  - subject: "They deserve it"
    date: 2005-05-15
    body: "I always felt that KStars was one of KDEs nicest applications. I\u0092m normally more of a Gnome guy but that sure is one app to envy. They deserve that prize money. :)"
    author: "Anon"
  - subject: "Re: They deserve it"
    date: 2005-05-15
    body: "Does it mapper whether it is a Gnome or a KDE app. Who cares about underlying toolkits."
    author: "He?"
  - subject: "re"
    date: 2005-05-15
    body: "me"
    author: "mario"
  - subject: "KDE e.V. financial"
    date: 2005-05-15
    body: "Hi, I'm inspired by this gesture of the KStars team. However, I would like a more detailed overview of where donated money, materials, etc. are planned to end up, and maybe others considering to make a donation have the same questions. Does KDE e.V. have an annual activity and/or financial planning/report besides the <A href=\"http://www.kde.org/support/thanks.php\">Sponsoring Thanks</A> that interested parties/contributors may read? \n<p>\nI just read what the KDE homepage says about <A href=\"http://www.kde.org/areas/kde-ev/\">KDE e.V. </A> . Unfortunately, there is only one online available meeting report from 2002. \n<p>\nBtw, is there an English translation available of the <A href=\"http://www.kde.org/areas/kde-ev/corporate/statutes.php\">Statutes and Articles</A> ? I would be happy to provide an English translation by an official translator if none has been prepared yet.\n"
    author: "Anonymous"
  - subject: "Re: KDE e.V. financial"
    date: 2005-05-15
    body: "As it was said the money goes for legal fees (fees to keep the eV alive, trademark registration and such), organizing conferences (even if the last report is about a 2002 conference, there were others, every year since them), sponsor KDE contributors to attend conferences, meetings, hacking sessions (not just the annual KDE world summit). So the money is turned back into the project, even if it is not so visible from outside."
    author: "Anonymous"
  - subject: "Re: KDE e.V. financial"
    date: 2005-05-16
    body: "perhaps an 1<->1 mapping of where the money was spend would be nice , who \"in person\" has access to the money and if he decides to take money from the account he can write it on the webpage.\n\n"
    author: "ch"
  - subject: "Re: KDE e.V. financial"
    date: 2005-05-15
    body: "> Btw, is there an English translation available\n\nIt's being worked on for some time."
    author: "Anonymous"
  - subject: "WoW"
    date: 2005-05-15
    body: "Respect </AliG> Nothing else needed to say :)"
    author: "cartman"
  - subject: "Re: WoW"
    date: 2005-05-15
    body: "> Respect </AliG>\n\nWouldn't that be \"restecp\"? ;)"
    author: "EHa"
  - subject: "Largest?"
    date: 2005-05-16
    body: "While USD1500 is a rather large sum to donate for the people behind KStars (well done), I find it kind of sad that that's one of the largest sums ever donated. Aren't there companies lining up to fund the e.V.? It sounds like a good cause if you have interest in the advancement of KDE (which many companies do)."
    author: "Haakon Nilsen"
  - subject: "Re: Largest?"
    date: 2005-05-16
    body: "if they do they would give money , so they dont , its easy - face it"
    author: "ch"
  - subject: "Re: Largest?"
    date: 2005-05-18
    body: "Companies like to sponsor concrete stuff getting much press like conferences. Not so much the many small misc expenses of KDE e.V."
    author: "Anonymous"
  - subject: "Re: Largest?"
    date: 2005-05-18
    body: "perhaps KDE conference sponsert by Heineken :-) and pizza hut"
    author: "ch"
  - subject: "Re: Largest?"
    date: 2005-05-18
    body: "Dunno if all developers want to eat fast food all week long. :-) The IBM vouchers for the first days were nice last time as was the refectory."
    author: "Anonymous"
  - subject: "Re: Largest?"
    date: 2005-05-22
    body: "What may equally unclear is the sponsoring by companies letting their employees work on KDE while retaining them on the payroll. Does not get much press coverage either, but at least the company can steer the effort towards its own interests. And this is nowhere in the donation list! \n\nAre there any statistics of how much KDE is worked on officially within the precinct of companies ?"
    author: "John"
---
Earlier this year <a href="http://edu.kde.org/kstars/">KStars</a> won <a href="http://dot.kde.org/1107087331/">the QtForum.org programming contest</a>.  Well the KStars developers decided the best thing to do with their money was to <a href="http://www.kde.org/support/donations.php">donate it to KDE</a>.  The US$1500 prize money is one of the largest donations to <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> to date.  KDE e.V.'s budget mostly goes towards legal fees and travel costs for allowing developers to attend conferences.  If you wish to be generous, and feel inspired by the KStars developers, you can do the same on the <a href="http://www.kde.org/support/support.php">KDE donations page</a>.

<!--break-->
