---
title: "Beauty and Magic for KDE, with Zack Rusin"
date:    2005-09-09
authors:
  - "binner"
slug:    beauty-and-magic-kde-zack-rusin
comments:
  - subject: "Mighty big promise!"
    date: 2005-09-09
    body: "Zack, \"Plasma will blow you away. Nothing you've ever seen or will see in the coming years will come even close to what you'll experience with Plasma. And that's a promise.\"\n\nNow that is some serious hype!  Not that I doubt you and Aaron and the rest can achieve it, but that is one heck of a promise.\n\nNow, what I want to know is... how is the Zack-Super-Snackerific-File-Manager coming along? ;) "
    author: "manyoso"
  - subject: "Re: Mighty big promise!"
    date: 2005-09-10
    body: "Zack talks like that all the time... pay no attention.  ;)"
    author: "ac"
  - subject: "Re: Mighty big promise!"
    date: 2005-09-10
    body: "at least he's a motivated open source guy then ;)"
    author: "Mark Hannessen"
  - subject: "SVG 1.2 with KSVG2??"
    date: 2005-09-09
    body: "\"I don't think I'll be releasing a huge secret when I say that Qt will soon natively support SVG 1.2 and will utilize it on many different levels (that includes support for animations!)\"\n\nThis is HUGE!  Will this be integrated with QT 4?  Is this native or will it be using KSVG2?  Finally SVG will be supported somewhere else, besides on mobile phones (SVGT).  \n\nI don't know how *soon* that can be since the 1.2 spec is changing all the time right now, but this is very encouraging news.\n\nGreat work and how about some screenshots of the presentation?"
    author: "am"
  - subject: "Re: SVG 1.2 with KSVG2??"
    date: 2005-09-09
    body: "> Will this be integrated with QT 4?\n\nthe quote is a little old but probably still valid:\n\n\"We intent to support SVG better in the future (possibly Qt 4.1)\"\nhttp://lists.trolltech.com/qt4-preview-feedback/2005-01/thread00062-0.html\n\n"
    author: "Christian Loose"
  - subject: "So Xgl isn't dead?"
    date: 2005-09-09
    body: "I thought or better read in the past that the development of Xgl was stopped (by RedHat?) but Zack says that Xgl is the future?"
    author: "Mario Fux"
  - subject: "Re: So Xgl isn't dead?"
    date: 2005-09-09
    body: "Jon Smirl is no longuer working on XeGL but xgl is still alive, and Zack made the promise to finish xegl when he'll have time."
    author: "Jean Roc Morreale"
  - subject: "Re: So Xgl isn't dead?"
    date: 2005-09-09
    body: "No worries, shouldn't take Zack more than an afternoon or two..."
    author: "manyoso"
  - subject: "Re: So Xgl isn't dead?"
    date: 2005-09-10
    body: "I have no doubt about that since I have read that he even adds impossible things to Qt ;) :\n\n\"If something is either impossible or incredibly hard to do I go off and make sure it works there before we add it to Qt.\""
    author: "furanku"
  - subject: "Qt and GTK people"
    date: 2005-09-09
    body: "Qt is the driving-force for Zack, to implement lowlevel features which Qt will benefit from. Has doing that took you to discussions with GTK/Cairo-people about whether they could benefit from it to? After all, we have 2 popular toolkits, not one. :)\n\nI'm a KDE user, but I'm just curious. ;)"
    author: "mOrPhie"
  - subject: "Re: Qt and GTK people"
    date: 2005-09-09
    body: "Probably it's just me, but I think if gtk people want some of those features, they should be the ones to pick them up and port them to gtk, not the other way around. "
    author: "Levente"
  - subject: "Re: Qt and GTK people"
    date: 2005-09-09
    body: "On the other hand, KDE and GNOME aspire to many of the same goals. Even if they try and approach them in very different ways, surely there is a means by which they can work together, so that they don't do the same job twice. It seems as if KDE and GNOME both need the underlying graphics technology to be capable of a certain number of things in order to enable them to accomplish their dreams, so in order to achieve this level of technology, it seems to make sense to me that they should co-operate."
    author: "Jack H"
  - subject: "Re: Qt and GTK people"
    date: 2005-09-09
    body: "Don't worry, they do...  it's called X.org ;)"
    author: "anon"
  - subject: "Re: Qt and GTK people"
    date: 2005-09-10
    body: "Haha! Well, I hope you're right. Are there really very many people from KDE & GNOME contributing to X.Org? I'd be very interested to know."
    author: "Jack H"
  - subject: "So many acronyms so little clue..."
    date: 2005-09-09
    body: "Wow. So many acronyms....so little clue what they all mean :-p All I can say is if it means a slicker, shinier, more visually rich linux desktop experience, bring it on!"
    author: "Tim Sutton"
  - subject: "Re: So many acronyms so little clue..."
    date: 2005-09-10
    body: "http://wiki.x.org/wiki/XorgGlossary"
    author: "Anonymous"
---
<a href="http://linuxtoday.com/">Linux Today</a> <a href="http://linuxtoday.com/news_story.php3?ltsn=2005-09-07-002-26-IN-KE-DV">points</a> to an <a href="http://users.ox.ac.uk/~chri1802/kde/zrusin-X-future.html">interview with Zack Rusin</a> which was conducted at <a href="http://conference2005.kde.org/">aKademy 2005</a>. Topics include his employment by <a href="http://www.trolltech.com/">Trolltech</a>, his contributions to <a href="http://www.x.org/">X.org</a> and upcoming technologies and eye-candy Zack demoed at the conference.  A <a href="http://www.golem.de/0509/40255-4.html">German version of the article</a> was published by <a href="http://www.golem.de">Golem.de</a>.







<!--break-->
