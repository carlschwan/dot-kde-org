---
title: "FOSDEM 2005: Developer Tools Interviews"
date:    2005-02-15
authors:
  - "jriddell"
slug:    fosdem-2005-developer-tools-interviews
comments:
  - subject: "I love kdevelop"
    date: 2005-02-15
    body: "I recently realized with some surprise that I was using KDevelop for work on Krita more and more; more than either vi or XEmacs. While the instability of XEmacs on SuSE 9.2 might have something to do with that, it's still... weird. But I like KDevelop, I liked it so much that I even tried to use it for Java, patching the java project module along the way. But I didn't have time to pursue that further, and now I do exactly as Alexander says: I use Eclipse for Java and KDevelop for C++.\n\nIs there, by the way, a way to split the main pane in two in ideal mode? It's quite hard to compare to files, currently."
    author: "Boudewijn Rempt"
  - subject: "Re: I love kdevelop"
    date: 2005-02-15
    body: "Yeah.. a split view is something _everyone_ wants. I think it'll have to wait for Qt4/KDE4 and a KMDI-less implementation however. In KMDI-land a tab is a document is a childwindow, and this makes creating a solution that's even remotely sane across all UI modes a challenge."
    author: "teatime"
  - subject: "Re: I love kdevelop"
    date: 2005-02-15
    body: "Well... Kate can do it, and that's inside something IDEAL-like, too."
    author: "Boudewijn Rempt"
  - subject: "Re: I love kdevelop"
    date: 2005-02-15
    body: "Yup, Kate does it.. by ripping out both the non-IDEAl modes and the tabbar. That's what it takes. Personally, I really want to do this too, but people are soo in love with that tabbar.. :(\n\nAn additional complication is that KDevelop would need a cross between Konqueror's ability of loading any type of KPart (which Kate can't) and Kate's ability to split views (while not instantiating a second kpart, which is what Konqueror does). \n\nAnd, to top it off, it needs to work for any KTextEditor plugin... "
    author: "teatime"
  - subject: "Re: I love kdevelop"
    date: 2005-02-16
    body: "Kate didn't remove the tabbar to be able to have split views, we had them before KMDI appeared. But I object to having pr document tabs in kate amongst others for that reason.\n\nKate didn't remove the other MDI modes for that reason either.\n\nShowing any kpart in kates split views would be really easy, putting a (kpart) widget in a splitter is not hard, wether you have a splitter or something else as the parent does no make managing your kparts/widgets more or less difficult."
    author: "Anders"
  - subject: "Re: I love kdevelop"
    date: 2005-02-16
    body: "\"But I like KDevelop, I liked it so much that I even tried to use it for Java, patching the java project module along the way. But I didn't have time to pursue that further, and now I do exactly as Alexander says: I use Eclipse for Java and KDevelop for C++.\"\n\nYes, I'm not sure whether it's worth spending time improving java support in KDevelop when your competing against guys with a million dollar a year budgets/full time staff. What makes it worse is that IBM's Eclipse CPL license is incompatible with the GPL, so we can't lift their code. I don't know what license Netbeans has - I assume it isn't the GPL.\n\nI've actually fixed the KDE java project app framework template in the cvs so it works, and the generated app compiles. And we have Boudewijn's ant support in there too, thanks to his patch. Then Marco Ladermann has written a java version of the uic tool 'juic' - if that was integrated in KDevelop (pretty straightforward) it would be more impressive than any visual design tool in Eclipse as far as I know.\n\nStarting from scratch to add better debugging (via the JPDA interface), code completion and java refactoring support would take a fair bit of effort though. \n\nI'd personally rather spend my time improving KDevelop ruby support, adding a python debugger like the ruby one, and some PyKDE project templates. The trolls have done such a good job at improving C++, that there is much less difference between java programming and C++ programming than there usually is. But Python Perl and Ruby are significantly faster to develop gui apps than either java or C++, even for Qt programming."
    author: "Richard Dale"
  - subject: "Re: I love kdevelop"
    date: 2006-03-18
    body: "Why not actually make use of Eclipse and improving KDE support for it? As I'm going to demo KDE on EclipseCon next week, I added a little bit of KDE-Java support to Eclipse, namely launching Designer on .ui-files and automatically building them using juic.\n\nI'm interested in the java-version of juic, where can I find it?\n\nI also added preliminary support for DCOP-Java.\n\nCheers,\nCarsten"
    author: "Carsten Pfeiffer"
  - subject: "OT: GPL'd Calendar and mail server from Novell"
    date: 2005-02-16
    body: "Maybe it's worth to add a new connector for Kontact ;-)\nhttp://www.hula-project.org/index.php/Hula_Server\nAnd I could not decide what Groupware to choose for with the current possibilities...\nI'd really like to setup Kontact on 5 clients with a Server, but I've no idea what Groupware/Mail-Server to choose. I fear a bit the complexity of Kolab. eGroupware was on my list, but this Hula thingy looks nice too...\n"
    author: "Thomas"
  - subject: "Re: OT: GPL'd Calendar and mail server from Novell"
    date: 2005-02-16
    body: "This \"Hula thing\" is good marketing. It needs a lot of work to make it a useful \"collaboration server\" (as they want it to be).\nOpenGroupware and OpenXchange as well as Kolab and eGroupware are currently much more advanced.\nBut if you can wait a year (or two?), Hula may be a nice groupware server...\n"
    author: "Birdy"
---
FOSDEM have published the <a href="http://www.fosdem.org/2005/index/news">interviews of the speakers from their developer tools track</a>.  Alexander Dymo and Harald Fernengel <a href="http://www.fosdem.org/index/interviews/interviews_dymo_fernengel">talk about KDevelop</a> including Umbrello integration and what might be in store for KDevelop 4.  Benoit Minisini <a href="http://www.fosdem.org/index/interviews/interviews_minisini">answers questions on Gambas</a> discussing how it compares to other IDEs and how programming is like a music composer writing a symphony.

<!--break-->
