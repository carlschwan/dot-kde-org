---
title: "Yzis Milestone 3 Released"
date:    2005-02-22
authors:
  - "pfremy"
slug:    yzis-milestone-3-released
comments:
  - subject: "Good to see progress"
    date: 2005-02-22
    body: "Nice to see that Yzis makes some good progress, some months ago I tested a yzis version, but was not really useable for a vim user, since too many advanced feature were missing (like text-objects e.g.), but I really hope this will change soon (although very unlikely, since vim advances as well).\n\nJust imagine, you could have a good vim-like editor in all/most multiline edits in KDE. :) (The vim kpart didn't really work for me - and kvim was/is just too slow for normal editing)."
    author: "Martin Stubenschrott"
  - subject: "Thanks"
    date: 2005-02-22
    body: "Never knew this existed, looks cool!"
    author: "Al"
  - subject: "This looks promising"
    date: 2005-02-22
    body: "I'd never known this existed.  I'm glad they got this publicity.  Now I hope they keep up the good work, because vim is my favorite and I've really missed having vim integration in KDE.  KVim is just too buggy to use, so I always have several konsole tabs & windows open, primarily for vim.  Keep up the good work!!"
    author: "Michael Dean"
  - subject: "...and kile? :P"
    date: 2005-02-22
    body: "Hi! I'm the Spanish translator of Yzis... it's very usable already but i can't wait for it to be embedded into kile!!!!!!!!!\n\n"
    author: "Rafael Rodr\u00edguez"
  - subject: "Re: ...and kile? :P"
    date: 2005-02-22
    body: "Yeah, me to!"
    author: "ac"
  - subject: "KHTML Form Editor Patch"
    date: 2005-02-22
    body: "The patch to open a KHTML form in a proper editor (Kate, Yzis, etc.) should be really pushed into post 3.4 Konqueror. When you work with all kind of web based CMS system (Zope, Wiki, Spip, blogs) it is really painful to edit large texts in an HTML form. What would be nice is a simple generic wysiwig editor for all these wiki-like syntax programs."
    author: "Charles de Miramon"
  - subject: "+ Kmail"
    date: 2005-02-22
    body: "Not to mention the KMail patch.\n\nIt really baffles me that KDE has had this nice, clean KTextEditor interface ofr so long, yet the two of the largest, most visible apps in KDE (KMail, Konqueror), do not take advantage of it.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: KHTML Form Editor Patch"
    date: 2005-02-22
    body: "Please vote for bug #37146 ( http://bugs.kde.org/show_bug.cgi?id=37146 ). It is precisely about this."
    author: "Jan Jitse"
  - subject: "Yzis?"
    date: 2005-02-22
    body: "How exactly are you supposed to pronounce that name?  I don't have even the slightest clue how to."
    author: "Corbin"
  - subject: "Re: Yzis?"
    date: 2005-02-22
    body: "why- zeeeee-----iiiii---sssss"
    author: "anon"
  - subject: "Re: Yzis?"
    date: 2005-02-22
    body: "uhm, \"the editor formerly known as vim?\" :)"
    author: "manyoso"
  - subject: "Re: Yzis?"
    date: 2005-02-22
    body: "Actually, \"why this\"."
    author: "anish"
  - subject: "Re: Yzis?"
    date: 2005-02-22
    body: "I don't know if thats the official way, but thats now what I'm calling it!"
    author: "Corbin"
  - subject: "Actually kvim was the reason i switched to kde"
    date: 2005-02-22
    body: "It's really nice to see this. Actually i am very used to the vi keystrokes and not having kmail and others using ktextpart is really annoying. Is there anything known when this will be regular in kde (or better debian sid?)\nThanks\nTim"
    author: "Tim"
  - subject: "Re: Actually kvim was the reason i switched to kde"
    date: 2005-02-22
    body: "Maybe a global option to set the default embeded text editor? Maybe options such as Yzis, Standard (the current one), and possibly more in the future.\n\nI probably wouldn't ever use Yzis because I don't know VIM's key layout (I prefer nano on the console since its much simpler).  Though if you knokw VIM's key layout you probably would love Yzis (since now that knowledge wouldn't be confined to the console)"
    author: "Corbin"
  - subject: "Re: Actually kvim was the reason i switched to kde"
    date: 2005-02-23
    body: "That's already there!  In the control center, go to \"KDE Components\", then \"Component Chooser\" (I hope this translation from German is accurate.).\n\nI don't know which KDE apps honour that setting, though.\n"
    author: "cm"
  - subject: "Re: Actually kvim was the reason i switched to kde"
    date: 2005-02-23
    body: "Jep this setting is there, but it is not honoured by kmail. Instead kmail got a imho useless html editor :(. But hey i could have fixed it for myself...\nanyway i'm just getting used to the qt toolkit and i have written my first applet which monitors the md raid on linux. So yes i think i will stay with kde  :).\nThanks for your ansewers. There are a lot interesting comments on the dot."
    author: "Tim"
  - subject: "compile error"
    date: 2005-02-22
    body: "Hi,\ni get this on slackware 10.1\n\nmain.cpp: In function `int main(int, char**)':\nmain.cpp:67: error: `LC_ALL' undeclared (first use this function)\nmain.cpp:67: error: (Each undeclared identifier is reported only once for each\n   function it appears in.)\nmain.cpp:67: error: `setlocale' undeclared (first use this function)\n\nWhat to do? thanks"
    author: "Markus"
  - subject: "Re: compile error"
    date: 2005-02-22
    body: "It looks like you are missing some header from gettext-devel or glibc-devel or something like that. Check that you have those two installed. I'm not at all sure about this though. \n\n~Macavity\n--\n\"And remember kids.. If it aint broke.. Hit it again!\""
    author: "Macavity"
  - subject: "Re: compile error"
    date: 2005-02-22
    body: "add #include<locale.h> to main.cpp\n\nThis bug is fixed in CVS head."
    author: "arael"
  - subject: "To the authors of KVim"
    date: 2005-02-22
    body: "\"vi-compatible editor from the authors of KVim\"\n\nWhy didn't finish the authors kvim? Vim, gvim and vim for Windows are great editor, kvim is pretty unusable. What is the reason for this, is vim hard to port?"
    author: "koos"
  - subject: "Re: To the authors of KVim"
    date: 2005-02-22
    body: "From http://www.yzis.org/viewcvs/trunk/README?rev=1494&view=auto\n\nHistory:\n========\nBefore working on Yzis, the authors (Mickael Marchand, Thomas Capricalli and\nPhilippe Fremy) had been working on GVim. GVim is clearly the best vi\ncompatible editor today. It contains tons of features, which are very clear\nimprovements upon the original vi: visual selection, unlimited undo, powerful\nsyntax highlighting, script language, splitted windows, ...\n\nWe did two things with GVim. First, we ported it on KDE and created KVim. The\nsecond step was to make KVim embeddable as an editor component into any KDE\napplication. The idea was to be able to use a vi editor anywhere: in KDevelop,\nin Kate, in KMail, ... We managed to complete both tasks but the second one\nwas very difficult to achieve and a number of problems could not be overcome.\nFor example, kvim can not have multiple windows on the same buffer, and thus\nwon't integrate in Kate. \n\nWhile working with the gvim code base, we have been comfronted with a growing\namount of difficulites:\n- there is a huge pile of C files without much documentation.\n- it is difficult to find one's way through the code. \n- the code was written in C and has very little abstraction, which make it\n  difficult to follow.\n- the vi engine is tied to the concept of console editor. We have to add hacks\n  after hacks to make it work as a graphical component (for example, we need\n  to fork a process, embed a graphical window, and run an event loop at full\n  speed while still not taking the whole CPU to just make the component work).\n- the main author of GVim is very reluctant to add any small change, even\n  those that won't affect the current behaviour\n- the codebase is very big and the author does not want to introduce any new\n  feature, in fear of breaking something.\n\nSo, on one side we had implementation limitations and problems, on the other\nside it was not possible to do any new developments on the editor. We\ndiscussed that with the GVim team and came to the conclusion that it was not\npossible to work with GVim to have a vi-like editor in KDE as a good\ncomponent.\n\nAt this point, the decision was simple. Either spend lot of time in working\naround limitations of GVim, or dropping the idea of a good vi editor component\nfor KDE or start a new vi editor. Kudos to Thomas Capricelli and Mickael\nMarchand who took over the third decision. This decision was taken shortly\nbefore Fosdem 2003. The design documents and the name were hacked during the\nFosdem.\n\nThey put up a website, a subversion server, mailing lists and started\ncoding. To avoid the many problems of gvim, we took the following decisions:\n1. clearly separate the vi engine from the gui\n2. use C++ to provide a clean design\n3. abstract the views, the buffer and the gui in the engine\n4. have a KDE gui\n5. have a text gui\n6. provide a C interface in case a hard-core C coder wants to contribute\n7. use some Qt classes for the engine. Qt would bring a string class that\nhandles unicode correctly (multibyte support is knightmare in gvim), efficient\nlists implementations, and other goodies. Moreover, we were very familiar and\nefficient with Qt.\n8. Use tinyQ as a backup solution for the people not willing to link with Qt\njust for a few template classes.\n\nOver the time, maintaining a C binding to C++ turn out to be quite tedious and\ntime consuming. It would also make the code quite complicated. So the C\nbinding was dropped and yzis started to develop slowly but steadily.\n\nWe also chose lua as a scripting language. Lua is easy to embed, easy to learn and quite powerful."
    author: "Philippe Fremy"
  - subject: "Re: To the authors of KVim"
    date: 2005-02-23
    body: "\"2. use C++ to provide a clean design\"\n\nWas any thought ever given to writing Yzis in a language like Python or Ruby?"
    author: "ac"
  - subject: "Re: To the authors of KVim"
    date: 2005-02-23
    body: "I love python but there are at least two reason not to use it for this project:\n- speed: redrawing and handling of complex requirements (syntax highlighting, ...) does not make it suitable for python\n- reusabilite: C++ is easier to re-use for various usage.\n\nHowever, writing a python binding would be trivial. With tools like boost, it is a matter of a few days. I am using that for binding some C++ code to python it is just so easy.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: To the authors of KVim"
    date: 2005-02-23
    body: "> knightmare\n\nNow this is taking the K naming convention a little too far, isn't it? :)\n\n"
    author: "cm"
  - subject: "This solves ..."
    date: 2005-02-22
    body: "...the answers to all the following wars/questions:\n\nKDE vs GNOME\nVI vs EMACS\nLINUX vs WINDOWS\nKMAIL vs THUNDERBIRD\nKONQUEROR vs FIREFOX\n\netc. etc.\n\nBut seriously, those screenshots look extremely class.  Keep up the good work guys!"
    author: "standsolid"
  - subject: "Re: This solves ..."
    date: 2005-02-23
    body: "Actually one of those recently got a answer, but to keep things interresting it was with a new war/question:\n\nVI vs KATE\n\n:-)"
    author: "Morty"
  - subject: "Why was lua chosen?"
    date: 2005-02-22
    body: "The title says it all really. Many of the other projects in KDE use python or javascript as the scripting language. It would be nice to see lua being optional with python as an alternative."
    author: "Scott Newton"
  - subject: "Re: Why was lua chosen?"
    date: 2005-02-23
    body: "python is too big to be embedded. It is usually quite complicated to embed the interpreter too.\n\nlua has been designed for easy embedding and has proven to be easy to use. Javascript would have been another good choice. I just happen to know that lua is good for embedding project, and there is a lot of literature on this, and the code was added in less than two days. I don't know if any javascript engine can do the same.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Why was lua chosen?"
    date: 2005-02-23
    body: "\nHo man... Yet another languaje. I love python, I have to use perl and PHP and now I need lua...\n\nAre there any plans to enable vim scripts to be used in Yzis ?"
    author: "Marioy"
  - subject: "Extremely exiting."
    date: 2005-02-22
    body: "Looks very, very promising. Can't wait for a stable release."
    author: "Kjetil Olsen"
  - subject: "Gentoo"
    date: 2005-02-23
    body: "Where are the gentoo ebuilds? :)\nI'm too lazy to do *anything* by hand :)"
    author: "unknown"
  - subject: "Re: Gentoo"
    date: 2005-02-23
    body: "http://bugs.gentoo.org/show_bug.cgi?id=48552\n\nor \n\nhttp://www.yzis.org/viewcvs/trunk/dist/gentoo/"
    author: "Philippe Fremy"
---
The <a href="http://www.yzis.org/">Yzis</a> team is glad to announce the Milestone 3 release of Yzis, the fast moving
vi-compatible editor from the authors of <A href="http://www.freehackers.org/kvim/">KVim</a>. A lot has happened since the M2 release in August 2004:
many new features have been added and bugs fixed, getting us closer to the full Vim feature set. Check for yourself on the <a href="http://www.yzis.org/screenshots/">screenshots</a>.

<!--break-->
<p>Yzis is developed using modern
technologies to move at a rapid pace: object oriented programming, Lua
scripting, syntax highlighting files in XML and unit testing.</p>

<p>
The core of Yzis is a reusable vi engine, which can be
embedded as an editor. Frontends are
provided for ncurses, a standalone KDE application and the KDE component system KParts.</p>

<p>
Yzis can be embedded directly into KDevelop and Quanta. Patches are available
to embed it as <a href="http://bugs.kde.org/show_bug.cgi?id=59481">the KMail composer</a>
and <a href="http://dev.inzenet.org/~panard/others">KHTML form editor</a></p>

<p>Yzis is already usable as a day to day editor. If your favourite Vim feature
or command is not supported yet, please drop us a note.  We will be happy to add it.</p>

<p>The next milestone release <a href="http://www.yzis.org/schedule">is planned</a> for summer
2005 and should be the last release before the stable 1.0. The first stable version
should be released at some point after KDE 4.</p>


