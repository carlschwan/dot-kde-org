---
title: "Trolltech Releases Qt 4.0"
date:    2005-06-28
authors:
  - "binner"
slug:    trolltech-releases-qt-40
comments:
  - subject: "KDE SVN branch working with QT 4"
    date: 2005-06-28
    body: "   (...) Work on KDE 4 has already started with making a development branch of KDE compile and run with Qt 4 (...)\n\nIs there any documentation or how-to about how can I compile and test that?"
    author: "efegea"
  - subject: "Re: KDE SVN branch working with QT 4"
    date: 2005-06-28
    body: "Look under /branches/work/kde4/ on anonsvn.kde.org."
    author: "Anonymous"
  - subject: "Re: KDE SVN branch working with QT 4"
    date: 2005-06-28
    body: "The modules are under trunk/branches/work/kde4\n\nYou need unsermake 0.4 (trunk/kdenonbeta/unsermake), if you are not using it already. (You can use the same version on KDE 3.x too.) Automake does not work (and will never work).\n\nYou need to compile Qt4 as debug not as release, otherwise it does not work.\n\nThere are perhaps other traps (and please do not await wonders...)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KDE SVN branch working with QT 4"
    date: 2005-06-29
    body: "will kde4 be the first release to fully support qt4? or is there any other *easy* way to get a kde desktop, with kdevelop that makes use of qt4?"
    author: "hannes hauswedell"
  - subject: "Re: KDE SVN branch working with QT 4"
    date: 2005-06-29
    body: "There will probably not be any easy way, as Qt4 is not source compatible to Qt3. (That is why Trolltech has made a porting tool.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Thanks"
    date: 2005-06-28
    body: "Many thanks for not being musicians. It was the first time in my life that i used remove file from inside amarok, instead of remove from playlist. The song is terrible. But i know that bad advertising is good, so everyone who read this will try the song anyway.\n\nPS: Thanks for Qt. But for this i have no words."
    author: "Henrique Marks"
  - subject: "Re: Thanks"
    date: 2005-06-28
    body: "At least it's better than the Free Software Song (http://www.gnu.org/music/free-software-song.html) :P"
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2005-06-28
    body: "But not better than John Ashcroft's \"Let the Eagle Soar\""
    author: "Anonymous"
  - subject: "Re: Thanks"
    date: 2005-06-28
    body: "Song was ok, but Dance was laughable ;), had a hearty laugh watching trolls dancing :D"
    author: "fast_rizwaan"
  - subject: "ha"
    date: 2005-06-28
    body: "Be sure to watch the .ogg or .mov with mplayer or somesuch to get the full music video experience. Personally I think its so lame that goes around and is actually incredibly awesome. ;)\n\n/me does the Qt 4 Dance"
    author: "Ian Monroe"
  - subject: "Re: ha"
    date: 2005-06-28
    body: "The guy speaks with the same horrible french accent as myself. Lol"
    author: "JC"
  - subject: "Congratulations!"
    date: 2005-06-28
    body: "Congratulation for the Qt4 release and the video rocks! :)"
    author: "cartman"
  - subject: "Torrents!"
    date: 2005-06-28
    body: "http://www.trolltech.com/torrents/qt-x11-opensource-desktop-4.0.0.tar.bz2.torrent\nhttp://www.trolltech.com/torrents/qt-win-opensource-desktop-4.0.0.mingw.exe.torrent  http://www.trolltech.com/torrents/qt-win-opensource-desktop-4.0.0.zip.torrent http://www.trolltech.com/torrents/qt-mac-opensource-desktop-4.0.0.tar.gz.torrent"
    author: "Anonymous"
  - subject: "Public Bug Tracker"
    date: 2005-06-28
    body: "Small addition, Trolltech has now a public bug tracker: http://www.trolltech.com/developer/tasktracker.html"
    author: "binner"
  - subject: "Re: Public Bug Tracker"
    date: 2005-06-28
    body: "Excellent, was about the time they finally opened that up."
    author: "ac"
  - subject: "Re: Public Bug Tracker"
    date: 2005-06-28
    body: "THANKS!"
    author: "cies breijs"
  - subject: "Just wondering"
    date: 2005-06-28
    body: "Any possibility that the new low-level style API will help spur development of a common widget set / backend for Qt & Gtk on freedesktop.org? Linux is in real need for a standard GUI, and having one would allow GNOME and KDE to work closer together, always a good thing!"
    author: "LuckySandal"
  - subject: "Re: Just wondering"
    date: 2005-06-28
    body: "I haven't looked in detail at what the low-level APIs provide, but some of them already have an equivalent for GTK. Whether or not we'll see them merge is something we'll have to wait and see, but it would be a good start.\n\nGlib provides container classes like Tulip.\nI'm fairly sure Cairo is similar to Arthur.\nPango is the equivalent text renderer to Scribe."
    author: "James Livingston"
  - subject: "Re: Just wondering"
    date: 2005-06-28
    body: "I doubt such a merge would be widely used unless it's both compatible with Trolltech's Qt and available for all systems theirs is (which is highly unlikely if GTK is to form the base of the merger). "
    author: "ac"
  - subject: "Re: Just wondering"
    date: 2005-06-28
    body: "I don't know if that would happen, but the new API sounds like a good thing for the Gtk-Qt Theme Engine."
    author: "mmebane"
  - subject: "oooh yeah..."
    date: 2005-06-28
    body: "common do the Qt4 dance...\n\nNO"
    author: "ac"
  - subject: "Very funny video"
    date: 2005-06-28
    body: "The song is great and the video is very funny, but in mov format kaffeine fails (extrange when I could watch other files in mov format), so I encoded to MPEG4 and mp3. \n\nCould I made a mirror of this video and mp3 on my own server? or there is any copyright restriction?\n\nBy the way, Qt4 looks nice :-)"
    author: "melenas"
  - subject: "Re: Very funny video"
    date: 2005-06-28
    body: "If I download the video it works in Kaffeine (but not KPlayer), but the audio doesn't work in either, oh well."
    author: "Corbin"
  - subject: "Re: Very funny video"
    date: 2005-06-29
    body: "Just tested and confirmed that KPlayer correctly autodetects and plays the video both locally and directly from trolltech.com. The codecs are ffh264 for video and faad for audio, MPlayer is 1.0-pre7. You may want to make sure you have an uncrippled MPlayer binary, like the one from Marillat (http://debian.video.free.fr/). Also make sure you have the correct version of libfaad, for example if you use Marillat's binaries you should also use Marillat's libfaad, rather than the one from rarewares for example."
    author: "kiriuja"
  - subject: "Re: Very funny video"
    date: 2005-09-03
    body: "i hope i like it"
    author: "macflurry"
  - subject: "Hopes for Adobe & its reader"
    date: 2005-06-28
    body: "With the release of QT4, I hope Adobe will now quickly work on, and release its reader based on QT4. Adobe's newest reader (ver. 7.0) for Linux sucks in the file-save or file-open dialog to say the least. Besides, I think that for those having nothing like *GTK* but a KDE environment, the reader will load faster than the current GTK based one."
    author: "charles"
  - subject: "Re: Hopes for Adobe & its reader"
    date: 2005-06-28
    body: "since adobe rewrote the reader from motif to gtk for the version 5->7 upgrade, I don't think they will change it again any time soon."
    author: "Martin Stubenschrott"
  - subject: "All of Adobe 2 Qt"
    date: 2005-06-28
    body: "How about porting all Adobe tools (including Dreaweaver) to Qt? \nLots of people would buy a Creative Suite on Linux today. I wonder if there are hidden reasons for not serving the Linux market (-- insert your favourite paranoid phantasy here --). In the long run KDE will fill the gap just as KPDF is already a wonderful replacement for Acrobat Reader. The only weak spot of KPDF is the improper handling of truetype font kerning. Qt 4 Scribe promises to fix it. \nAdobe will need to run on a couple of different platforms soon. The Windows platform is about to fork to Win32, .net and Avalon, the Mac will fork to Power and i386 hardware. I do not see an economic development strategy other than using Qt. \n\nGo KDE go!\n"
    author: "Roland Wolf"
  - subject: "Re: All of Adobe 2 Qt"
    date: 2005-06-28
    body: "> Adobe will need to run on a couple of different\n> platforms soon. The Windows platform is about\n> to fork to Win32, .net and Avalon, the Mac will\n> fork to Power and i386 hardware. I do not see an\n> economic development strategy other than using Qt.\n\nGood point, there are indeed too many toolkits on the horizon :)\n\nAdobe already has one (can i say: 'experiment') program using Qt, called Photoshop Album (http://www.adobe.com/products/photoshopalbum/). More info on that here:\nhttp://dot.kde.org/1046444343/\n\n\n\n\n"
    author: "cies breijs"
  - subject: "Re: All of Adobe 2 Qt"
    date: 2005-06-29
    body: "\"I do not see an economic development strategy other than using Qt. \"\nDon't quit your day job then."
    author: "illogic-al"
  - subject: "Re: All of Adobe 2 Qt"
    date: 2005-06-29
    body: "> How about porting all Adobe tools (including Dreaweaver) to Qt? \n\nSince it's closed source then of course Adobe would have to do that. Supposedly there was work in progress to port Dreamweaver to Linux a year or two ago. Where is it and where is the hype? Reading things like this are distressing to me personally. Quanta Plus is here _now_, already native KDE, is already superior for coding like PHP and already has a superior rendering engine with KHTML. Currently development work is being done to make a new generation visual mode, integrate with the KDevelop framework and enhance other capabilities like scripting with Kommander. It also has a fully DTD compliant markup engine and manages XML DTDs on the fly. One of the goals for KDE 4 is to manage XML with XSLT on the fly. All the feedback I've gotten from people who have used Dreamweaver and Quanta is that there are less and less reasons to use Dreamweaver. Quanta is not targetting a feature for feature replacement but is targetting a new generation tool with a goal of nothing less than being the best.\n\nIf you can get software that is free as in freedom and free as in beer and it is deeply native KDE, mature and based on a more advanced architecture would you prefer a commercial product? Since Quanta uses sponsored development a contribution of a lot less than the retail cost of Dreamweaver can go a long way to helping to make sure that when (if) it finally shows up on Linux you have no reason left to downgrade."
    author: "Eric Laffoon"
  - subject: "Re: All of Adobe 2 Qt"
    date: 2005-06-29
    body: "Hello Eric,\nAny plans to port Quanta to Windows and/or Mac?\n\nI think it would be a very good idea to expand the 'market' of the program, and now with the GPL version of the Qt it would be possible to offer it for free.\n\nI don't know how hard it will be because it probably has many KDE dependencies.\n\nThanks for supporting such a great software,\n                  Manuel"
    author: "Manuel Valladares"
  - subject: "Re: Hopes for Adobe & its reader"
    date: 2005-06-28
    body: "\"The reader will load faster than the current GTK based one.\" \n\nWell, if you have enough RAM (about 512Mb these days), the gtk components that you need will stay in the cache after being loaded from disk for the first time, and next time they'll be loaded from mem-cache, not from the disk. Note that your argument goes both ways right ? Some people use gnome-only. Adobe will definitely not release a product with two different libs just to keep as happy :-)))\n\nI still would love to see _everything_ written with the Qt/KDE libs. But that's _my_ preference and _my_ opinion. Other people like other libs, let's all coexist. Long life to freedesktop.org \n:-)\n\nCheers,"
    author: "MandrakeUser"
  - subject: "Re: Hopes for Adobe & its reader"
    date: 2005-06-28
    body: "Cool, then I , as a non KDE user can complain about Adobe using Qt and the slow startup and bloated file opener.\n"
    author: "me"
  - subject: "Re: Hopes for Adobe & its reader"
    date: 2005-06-28
    body: "KDE people tend to believe you should actually be able to find your files rather than having to click to open up a dialog and then get about 4 pixels to browse your filesystem with."
    author: "mikeyd"
  - subject: "Re: Hopes for Adobe & its reader"
    date: 2005-06-29
    body: "C'mon, let's not get into flaming. Both approaches have their strenghts, so let's just keep promoting the ones we like instead of lowering the other ones."
    author: "Jakob Petsovits"
  - subject: "Re: Hopes for Adobe & its reader"
    date: 2005-06-29
    body: "What's the strength of having a much smaller area to browse your files with? I agree that there are advantages to gnome and its way in some areas, but the file dialog is just stupid."
    author: "mikeyd"
  - subject: "A Very Good Move to GPL"
    date: 2005-06-28
    body: "Thanks Trolltech for releasing QT for windows and mac under GNU/GPL. Now we can see more QT applications in Linux, Windows and Mac too :)\n\nI wonder how KOffice for Windows would look like ;)"
    author: "fast_rizwaan"
  - subject: "Re: A Very Good Move to GPL"
    date: 2005-06-28
    body: "Qt/Mac is available under GPL for quite some time already now."
    author: "Anonymous"
  - subject: "Re: A Very Good Move to GPL"
    date: 2005-06-28
    body: "Qt was already licensed under GPL for Mac for quite a long time!\nWindows is a first."
    author: "blacksheep"
  - subject: "song sucks, but... "
    date: 2005-06-28
    body: "hey...\ndo the qt4 dance! %-}\n\nthis is beyond embarrassing... but I'm sure it was great fun for all those dancing around in the vid ;)\n\n - 42"
    author: "42"
  - subject: "Cute Four Dance"
    date: 2005-06-28
    body: "First there was linux\nThan there was Mac\nNow we put Windows\non the open source track...\n\nThe only thing better than that was the beta (alpha?) release of KDE codename krocodile theme... Go, Schnappi, go..."
    author: "Amadeo"
  - subject: "Funny dance"
    date: 2005-06-28
    body: "Just wondering, which one is Zack \"The Hacker\" Rusin? :)\nThe video's credits seq just says he is one of \"The Cute Four\"."
    author: "blacksheep"
  - subject: "Re: Funny dance"
    date: 2005-06-28
    body: "The cutest one of course. Still question open? OK, the most left after leaving the computer."
    author: "Anonymous"
  - subject: "Re: Funny dance"
    date: 2005-06-28
    body: "This guy at the bottom is Zack Rusin:\n\nhttp://www.kdedevelopers.org/node/view/1122"
    author: "suy"
  - subject: "Re: Funny dance"
    date: 2005-07-01
    body: "And why is there a credit for \"Stand-in for Zack\"?"
    author: "AC"
  - subject: "Re: Funny dance"
    date: 2005-07-01
    body: "Because there was a stand-in for Zack for \"within the computer\" scenes."
    author: "Anonymous"
  - subject: "PyQt GPL in Windows ?"
    date: 2005-06-28
    body: "Does the fact that there is now a GPL official Qt windows release mean that pyqt for windows will also have a GPL version ? That's gonna boost PyQt as a fantastic cross platform rapid dev. environment !\n\nMaybe even PyKDE, are there any plans to natively port the kdelibs to the evil empire ?"
    author: "MandrakeUser"
  - subject: "Re: PyQt GPL in Windows ?"
    date: 2005-06-28
    body: "Yup, PyQt4 will be GPL'd in Windows\n\nhttp://www.riverbankcomputing.co.uk/pyqt/roadmap.php"
    author: "smt"
  - subject: "Re: PyQt GPL in Windows ?"
    date: 2005-06-28
    body: "Cool, thanks!\n\nWin PyKDE of course will depend on what happens with Win KDE ... oh well, one step at a time\n\nPersonally, I only use windows at work 'cause I have no option. It sucks, and I'd love to be able to use some kde apps ..."
    author: "MandrakeUser"
  - subject: "Re: PyQt GPL in Windows ?"
    date: 2005-06-29
    body: ">Personally, I only use windows at work 'cause I have no option.\n>\n\nI feel you all too well.  HOWEVER, what is it you're lacking?  OOo has opened and write to (via Linux) any Company's files I have needed too (and export them to PDF if need be - for free).  File sharing? Samba.  Email?  If it's just Exchange you have to work with, there's plugins for that.  If you one of the lucky that are straight imap or even pop, then I don't see a problem there.  Is there something I'm missing?\n\nM.\n\nP.S.\nThis is *NOT* a flame.  I'm HONESTLY very curious what it is from holding you back.\n"
    author: "Xanadu"
  - subject: "Re: PyQt GPL in Windows ?"
    date: 2005-06-29
    body: "Not to be rude, but I suspect it's the lead system administrator.\n\nNot all of us can do what we want you know!\n\nMore seriously, there are more serious software issues than the ones you're thinking of. A lot of (surprisingly large) businesses use things like Access & Visual Basic or Lotus Notes to manage their data - there's no easy way of carrying that across to Linux, and the price of bespoke software is always going to be high. Also, some business rely on Windows only tools. Further, they often use fairly complex accounting software, for which equivalents are not readily available for Linux (KMyMoney is not an option!), and tools like Crystal Reports for complex reporting.\n\nEvent companies doing bespoke development may need Windows tools as their clients will use Windows: this means Delphi, C++ Builder or VC. Kylix is not a viable option (it's unsupported and alledgedly buggy) and the cost of Qt/Win, Qt/X11 and a solid Linux distro is greater than Qt/Win and Windows. Doing desktop development with Java takes too long (although the upcoming Netbeans may rectify that), and Java on the desktop has a bad rep which may put off customers.\n\nThe only way you could switch easily to Linux is if you're doing Java development, and use open-source tools for change-control and issue-tracking like Subversion and Mantis. For most \"normal\" organisations, a switch is a major deal. It's never just Office and email.\n\n"
    author: "Bryan Feeney"
  - subject: "Re: PyQt GPL in Windows ?"
    date: 2005-06-29
    body: "Right on target. I work for one of the largest financial organizations globally. I can't possibly choose what software I run, and what OS I run. I work within a mid-sizegroup, and development is done in winXP boxes. It is horrible, multi-tasking is slow as sh*t (evne with a dual processor fast box). But these decisions are not made by me as an individual, but at a higher level, taking many factors into account. We do use Linux, and we are moving the client side of what we do to Java, and if at some point all our clients here use the Java interface, we could possibly move 100% to Linux, at least my group. We'll see, it looks very unlikely ... \n\nThe only OS stuff I use is where I get to choose (Firefox, Cygwin for some shell stuff, Kdiff3 which provides Win* binaries, and that's about it)"
    author: "MandrakeUser"
  - subject: "Re: PyQt GPL in Windows ?"
    date: 2005-06-28
    body: "I think PyQt are going to become the real big winner because of this. It already looks like the PyQt use on X11 are increasing rapidly, adding Windows will probably multiply PyQt usage tenfold. A GPL, or proprietary if you so choose, RAD for all the major computing platforms(Win/X11/Mac) easily available, are going to get noticed. I'm looking forward to seeing the Python/PyQt/Designer/Eric3 combos starting to appear on CD/DVDs for mainstream windows magazines."
    author: "Morty"
  - subject: "COOL"
    date: 2005-06-28
    body: "me and my girlfriend - we loved the musicvideo... :D"
    author: "superstoned"
  - subject: "Re: COOL"
    date: 2005-06-28
    body: "Perhaps your name gives a hint as to why :)"
    author: "mikeyd"
  - subject: "Re: COOL"
    date: 2005-06-28
    body: "i have no idea what you are talking about :D"
    author: "superstoned"
  - subject: "Re: COOL"
    date: 2005-06-29
    body: "Weird. Mine loves it too. I think it's demeaning for computers, who are forced to reproduce it. But I kinda like it, too!"
    author: "Roberto Alsina"
  - subject: "Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-28
    body: "It is said so many times already, im gonna say it again: thank you trolltech for GPL'ing Qt/Win.\n\nFree Qt bindings (PyQt, etc.), some (maybe all some day) KDE technologies, generally more GPL software for win32...\n\n\nIm stuck with one more question: will more GPL software for win32 be good for out beloveth FreeSoftwareMovement? (are there developers to inspire on the win32 platform?)\n\n_cies."
    author: "cies breijs"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-28
    body: "It will be good when it makes people start questioning the Microsoft fee they nearly always pay when buying a computer."
    author: "ac"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "And it will be awesome for people like me that still have XP installed (FC3 only laptop, and dual boot Gentoo/XP on desktop, now most of the time use Gentoo on the desktop since my laptop makes my room to hot, I only reboot into Windows to play Warcraft 3 now).\n\nIn the past I had installed KDE-CygWin and used that for a while (pretty damn fast for being emulated), but if KDE apps are ported to native windows (like Konqueror and KIO-Slaves) then that will be REALLY good (I can install Konqueror on people's computer, and instead of using a web based file manager they can login via SFTP or FISH or worst case FTP).\n\nIt's really hard to grasp all the amazing technologies in KDE (KIO-Slaves alone is amazing), even after using KDE for several years now (since RedHat9, and for a few weeks with Mandrake 7.1)."
    author: "Corbin"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "Warcraft III is one of the first things I tried with Cedega.  It works just as fast under Linux without the Window."
    author: "James"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "> will more GPL software for win32 be good for out beloveth FreeSoftwareMovement?\n\nYes, as Free Software is not about spreading Linux (or KDE)."
    author: "Anonymous"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "No, as Free Software is not about promoting/supporting a proprietary, closed-source platform (Windows) that is hostile towards Free or Open Software."
    author: "ac"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "If you would be right then there is something missing in Free Software licenses which prohibit Free Software to run on \"proprietary, closed-source platforms\". Free Software is about promoting Free Software and that is not always a OS kernel or the desktop but can be also about individual applications."
    author: "Anonymous"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "GNU has worked well on any kind of operating systems. \n\nIf GNU had been restricted to free open-source operating systems, that would have been hard and, probably, we would not be here discussing about it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "> If GNU had been restricted to free open-source operating systems, that would have been hard and, probably, we would not be here discussing about it.\n\nIn the beginning that was an evil that had to be accepted. But it's also the reason why Hurd was started."
    author: "ac"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "oh look, my favourite topic</sarcasm> ;-P but i do feel the need to make a clarification here:\n\nit's not about open vs closed operating systems. please don't descend into that irrelevant land of straw men.\n\nno, it's about environments that are good for Open Source / Free Software and ones that are a trap for it.\n\nthe only good thing that can come of Free Software on Windows for open source is if we manage to snag the minds and hearts of Windows developers and they start developing Free software. Free Software on Windows can do all sorts of wonderful things for the users of Windows, the question is how to turn that around to ensure Open Source development continues to thrive and most importantly grow.\n\nif you think that the open source desktop can continue to exist in a meaningful way (e.g. AmigaOS or BeOS doesn't exist in a meaningful way anymore) forever with single percentage market share, then perhaps you have more faith in the good will of the desktop industry.\n\nIMHO the most important applications we can port to windows are kdevelop, umbrello, quanta and kexi. tools people use to build tools.\n\nbtw, the whole \"but open source apps on Windows lead to migrations\" theory has been pretty well tested at this point with OOo and Firefox and shown to be mostly hot air. it's not commodity apps that keep people away from Open Source friendly platforms.\n\ni understand the convenience factor for users who love KDE but are stuck on Windows at work or play, but convenience is not a useful metric in and of itself. think of other things in life that are convenient, like dropping garbage on the ground rather than in a trash can, but which aren't really all that great at a larger level.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "It depends on how you see it.\n\nTo go back again to the GNU tools, there are alive because there are so many ports of them. (I have found that there are even some ports for Sinclair QL.)\n\nBut by restricting the supported operating systems (or enviroments if you do not want it restricted to the OS), you do not get a base for a further live of the software.\n\nSure I would prefer that there would be more computers running with open source environments but I know that it is barely a thing of technical superiority but that it is about public relation stuff.\n\nAlso I do not expect that active feedback will increase because mostly people wanting to give feedback could already finding ways to give feedback today.\n\nAs for ecology, sure it is important too, but as open source, it is not mainstream either.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-29
    body: "let's try this one more time:\n\nit's not about closed vs open platforms\nit's about supportive vs hostile platforms\n\nand it's not about PR, either. it's about sustainability.\n\nnow, can you please explain to me what about the above is so hard to understand that after having repeated it for how long now people such as yourself still insist on discussing it as if it were a matter of PR to do with open source versus closed source platforms? what can i do to help make it obvious enough that it sinks in and the conversation can be had on the right basis?"
    author: "Aaron J. Seigo"
  - subject: "developers VS lame-complaining-users"
    date: 2005-06-30
    body: "the title sais it all...\n\n> it's about environments that are good for\n> Open Source / Free Software and ones that\n> are a trap for it.\n\nThe FreeSoftwareMovement depends on developers more that users (I seem to rememer you also saying that). So will the efford of port lead to new inspired developers, or new lame-complaining-users (that: install, complain, move away)\n\nthat's indeed the question...\n\nin the end i think someone will port kde over to the smelly platform. we will see what it brings eventually anyway.\n_cies."
    author: "Cies Breijs "
  - subject: "Re: Qt/Win GPL! But will it help freesoftware?"
    date: 2005-06-30
    body: "I don't think the migration theory has been sufficiently tested yet, for two reasons:\n\n1: If %OFFICE_SUITE%, %BROWSER%, and %EMAIL_CLIENT% are the only apps anybody uses, then OOo and the Mozilla family *should* be leading the charge. But the ecosystem is so much larger than that. And it's usually %OBSCURE_LITTLE_WIDGET_I_CANT_LIVE_WITHOUT% that adds a disproportionate amount of inertia. Most people can figure out MS Office->OOo, IE->Firefox, and maybe Outlook Express->Thunderbird, but not much else.\n\nI think there's an opportunity there for a sort of Migration Tracker that scans your Windows environment, and evaluates what it would take to make The Big Switch. Maybe it can say \"You can switch now! Here's the distros that include everything you need.\", or \"There's no equivalent for this yet, but if you switch to these open source Windows apps now, the rest of your system will be ready once we fill in the missing piece.\"\n\n2: The other side of all that is another opportunity: A Linux distro that *makes the migration for the user*, instead of \"Resize partitions to make room for / and /swap, and I hope your Windows partitions are FAT32!\" Wouldn't it be nice if a fresh install of Fedora picked up your user name, network settings, bookmarks, My Documents folder, iTunes library, wallpaper, etc. from the Windows partition that it just shrank, and set them all up for you for your first boot into Linux?\n\nAnybody in this audience is probably willing to do the under the hood work necessary to make Linux work for themselves. But we're still a long way from the \"Mom puts the CD in the drive and clicks GO\" stage."
    author: "Keith Russell"
  - subject: "Anyone managed to compile it yet on windows?"
    date: 2005-06-29
    body: "Haven't gotten it to compile yet on windows.  I extracted it and ran configure, which shows me the license and then asks if I accept.  I type 'y' and enter and it just boots me back to the command line without doing anything or displaying anything.  \n\nAnyone got this to work yet?  I have VS.NET 2003.."
    author: "Leo S"
  - subject: "Re: Anyone managed to compile it yet on windows?"
    date: 2005-06-29
    body: "OpenSource version will not work with VS.NET 2003\nYou need to use mingw with OpenSource Qt"
    author: "dkzm"
  - subject: "hmmmm"
    date: 2005-06-29
    body: "I have no idea how they will top that video for Qt 5.\n\nSo disturbing.  I'd rather not think about it yet can't help thinking about it. :-)"
    author: "Anonymous"
  - subject: "Congratulations"
    date: 2005-06-29
    body: "Hope QT4 lay the foundation for a rock solid KDE4 based Linux desktop that can change and SAVE the world. Congratulations to Trolltech, may your business flourish!"
    author: "Sagara Wijetunga"
  - subject: "Thank you"
    date: 2005-06-29
    body: "I also want to thank trolltech. Whenever a company or fellow consultant is looking for a c++ development kit, I always point them your way in large part because I appreciate businesses that work hard to do the right thing. \n\nI will try to continue to support you into the future. Thanks for all the wonderful technology. \n\nKDE 4 will take a while, but I have no doubt that it will rock!"
    author: "Eu"
  - subject: "Could this pave the way to a native win32 version?"
    date: 2005-06-29
    body: "I'm quite curious. I really like KDE. Whenever I use linux, I use it as my desktop environment. Does the release of a win32 version of gpl qt mean I might soon be able to replace Explorer on my windows machine with KDE?\n"
    author: "SJ Zero"
  - subject: "Re: Could this pave the way to a native win32 version?"
    date: 2005-06-29
    body: "I don't know if someone will port Konqueror, for sure it will not be the first application to be ported. Kontact has higher priority and likely someone will write a small khtml-based Qt Web browser anyway soon."
    author: "Anonymous"
  - subject: "Re: Could this pave the way to a native win32 vers"
    date: 2005-06-29
    body: "I don't really care about the timeframe if it's going to happen -- it's been ten years that I've been staring at the same windows interface, another couple won't kill me. I'm just curious about whether it could or will actually happen."
    author: "SJ Zero"
  - subject: "Re: Could this pave the way to a native win32 vers"
    date: 2005-06-29
    body: ">it's been ten years that I've been staring at the same windows interface, another couple won't kill me.\n\n Damn boy! That one nearly killed me! 8-D\n\n/me chuckles on\n\n~Macavity"
    author: "macavity"
  - subject: "you're so doomed and you don't know it.."
    date: 2005-07-02
    body: "The long term damage of using Windows that long *will* be irrecoverable, and yes, it *will* lead to a horrible death. Another couple of years will probably shorten the years you have to suffer though. So  save yourself the pain and be sure to use Windows every day and get greenhorn as it is released."
    author: "thatguiser"
  - subject: "prj.files in Qt 4"
    date: 2005-06-29
    body: "Hi, a question:\n\nIn old Qt versions the Designer managed prj. files, but now it seems that these functionality has been dropped?\nNow I have to manage 'em myself via text editor?\nIs there a reference on which keywords are available for .prj files? \nQMake is a powerful tool I don't want to miss!\n\n\n >8^) katakombi"
    author: "katakombi"
  - subject: "Re: prj.files in Qt 4"
    date: 2005-06-29
    body: "Read the documentation of qmake?"
    author: "Anonymous"
  - subject: "Great! I like it!"
    date: 2005-06-29
    body: "Great! I like it!"
    author: "Cavendish"
  - subject: "windows"
    date: 2005-06-29
    body: "On Windows Kdevelop plus GNU Compiler Collection would be a real Visual Studio Killer."
    author: "gerd"
  - subject: "Re: windows"
    date: 2005-06-29
    body: "Indeed - I really miss KDevelop when working with GCC on Windows.  The recent 3.2 version is very good.  I have tried Dev-C++ and Eclipse on the Windows platform, but they don't even come close."
    author: "Robert Knight"
  - subject: "Re: windows"
    date: 2005-06-30
    body: "Same here, Quanta Plus and Kdevelop are really missed on windows plataform.\nDon't take me wrong, I use Linux every day on my home computer, but at my university (that have like, but very old and badly configured, and I don't have the root pass) it would be really be good to spreat this kind of software, there are a lot of pcs already with firefox ;)"
    author: "Iuri Fiedoruk"
  - subject: "Tech"
    date: 2005-06-29
    body: "Beautiful. Absolutely beautiful. You guys are fantastic."
    author: "Lucas Thompson"
  - subject: "software developer"
    date: 2005-06-29
    body: "I don't think that trolltech have emphasized this enough: now with the GPL'd QT4 there will be an avalanche in the number of KDE/QT freesoftware available natively under windows. Not all, especially the ones that relay on Posix/Unix APIs which would continue to rely on Cygwin. IMO, this QT4 GPL would represent a pivot in the history of software for years to come. It is now, and only now, that we can effortlessly develop native c++ applications that can be compiled and run on all platforms; that marrigged with FOSS would be the killer technology for Desktop applications."
    author: "Kefah"
  - subject: "Easter Egg"
    date: 2005-06-29
    body: "And has everyone already found the easter egg in Qt 4 Designer? :-)"
    author: "Anonymous"
  - subject: "Re: Easter Egg"
    date: 2005-06-30
    body: "Err... no. Care to explain this...?"
    author: "cstim"
  - subject: "Re: Easter Egg"
    date: 2005-07-01
    body: "What to explain? I will not tell you how to activate it :-)..."
    author: "Anonymous"
  - subject: "Re: Easter Egg"
    date: 2005-08-07
    body: "Found =)\nAnd, i must say this.. you've a good photograph there, isn't it? Nice business cards ;)\n"
    author: "RockMan"
  - subject: "I really enjoyed the music video!"
    date: 2005-06-29
    body: "But then again, I'm blind and deaf.\n"
    author: "Nilus Vortalds"
  - subject: "Release schedule"
    date: 2005-06-29
    body: "IMHO is would be great if KDE4 could be out in early 2006. IIRC KDE 3.5 is\nplanned to come in september, and pretty much of Qt4 porting will be done by that\ntime (as most devs run both branches), no?\nSo don't let wait your users toooo long (as it happened with KDE 3.2).\n\nBtw I compiled today Qt4 on an AthlonXP2200, and it took 2 hours to compile, and\nthe demos run a little sluggish... :-(\nI have an Nvidia GForce 5200 with the latest binary driver on Linux 2.6.12."
    author: "ac"
  - subject: "Re: Release schedule"
    date: 2005-06-30
    body: "> IMHO is would be great if KDE4 could be out in early 2006.\n\nKeep on dreaming...\n\n> IIRC KDE 3.5 is planned to come in september\n\nThere is no schedule for KDE 3.5 yet.\n\n> and pretty much of Qt4 porting will be done by that time\n\nKDE 4 is about much more than just porting to Qt 4."
    author: "Anonymous"
  - subject: "Re: Release schedule"
    date: 2005-06-30
    body: "There was the idea to have KDE4 on KDE's 10th birthday in October 2006\n\nAs for KDE 3.5, it seems that it will be for this autumn. For September, it i too early, as August is a lost month for translations, as too many translators are taking their holiday.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "PostScript and WYSIWYG"
    date: 2005-06-30
    body: "I don't see anything about the PostScript problems:\n\n    Wrong font names in PS files.\n    TrueType fonts rendered as Type 42 bitmaps in PS files.\n\nI don't see anything about the WYSISYG problem:\n\n    Now WYGIWYS on the screen but, the fonts on the screen are hinted at the \n    screen resolution and the fonts that are printed have the actual font \n    metrics.  The result is too much space between glyphs -- sometimes the \n    spacing is uneven as well."
    author: "James Richard Tyrer"
  - subject: "Re: PostScript and WYSIWYG"
    date: 2005-06-30
    body: "Do you have to troll everytime? Even if fixed, do you think they mention every bugfix as highlight of a new release?"
    author: "Anonymous"
  - subject: "Re: PostScript and WYSIWYG"
    date: 2005-06-30
    body: "Make that previous post by \"Anonymous _Know_Nothing_\". :-)\n\nThese are not minor \"bugs\" -- this is really about a lack of features which I don't consider to be a \"bugfix\" issue.  They are serious issues that Trolltec is aware of, and, in the case of WYSIWYG, they specifically promised to fix for Qt 4 in previous posting to their web site.\n\nBut, I don't expect to see a mention of a bug fix.  I expect to see mention of the new features which eliminate these problems.  I see nothing about a new PostScript driver.  Nothing that the PS driver will now embed TrueType as scalable Type 42 fonts (a serious missing feature -- OO has this for example).  I see nothing about WYSIWYG display of fonts and printing with the actual font metrics.  \n\nThese are all expected new features NOT bugfixes!  The current KOffice applications are seriously crippled by the lack of these features -- what good is a wordprocessor that won't print correctly??\n\nThere is also a related bug.  Qt doesn't properly parse some Type 1 font names.  The TrueType and Type 1 font names for the same font are often not the same and Qt needs to deal with this.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: PostScript and WYSIWYG"
    date: 2005-06-30
    body: "dunno for sure, but I believe that in earlier talks about Qt4, the problems with the font kerning were supposed to be solved in the new version of Qt."
    author: "rinse"
  - subject: "Re: PostScript and WYSIWYG"
    date: 2005-06-30
    body: "http://websvn.kde.org/branches/work/kde4/qt-copy/src/gui/painting/qprintengine_ps.cpp?rev=414152&view=auto\n\nHave fun!"
    author: "ac"
  - subject: "Re: PostScript and WYSIWYG"
    date: 2005-06-30
    body: "Yes, this is the replacement for:\n\nqt-x11-free-3.3.4/src/kernel/qpsprinter.cpp\n\nCompare the two.\n\nIt has many changes, but it still (just quickly skimming through it) has some code that concerns me.  For example: \n\nIt still has the code to: \"//try to make a \"good\" postscript name\".  This code should be removed because it can't work.\n\nIt still depends on a list of common fonts.  This is a kludge which should be removed.  The only font name specific code that is acceptable is a fix for the TimesRoman problem.\n\nIt also depends on the 4 glyph faces (regular, bold, italic, & bolditalic) paradigm.  This doesn't work and should be completely redone.\n\nIt has been changed so much that the patch:\n\nhttp://home.earthlink.net/~tyrerj/files/qt-x11-free-3.3.3-PSfontname.patch.bz2\n\nno longer applies, but that doesn't mean that the problems have been fixed.\n\nBut, we shouldn't have to read the code to try to determine what the new features are.  I must presume that these issues have not been resolved.  If these problems are not fixed, I can only suggest that KDE tries to adapt code from Scribus so that KDE is able to print correctly."
    author: "James Richard Tyrer"
---
<a href="http://www.trolltech.com/">Trolltech</a> has <a href="http://www.trolltech.com/newsroom/announcements/00000209.html">released Qt 4.0</a> both under <a href="http://www.trolltech.com/products/licensing.html">commercial and GPL licenses</a> for X11, Mac OS X and MS Windows. It is the first time that a MS Windows GPL edition is available. To celebrate the release Trolltech employees have created a <a href="http://www.trolltech.com/video/qt4dance.html">song and a music video</a> (<a href="http://www.trolltech.com/torrents/qt4dance_medium.mov.torrent">Bittorrent download</a>, <a href="http://developer.kde.org/~hausmann/qt4dance_medium.ogg">Ogg Theora version</a>). Read the <a href="http://www.trolltech.com/products/qt/qt4info.html">Qt 4 Overview</a> and the online <a href="http://doc.trolltech.com/4.0/">Qt Reference Documentation</a> for more information. You can download Qt from <a href="ftp://ftp.trolltech.com/qt/source/">ftp.trolltech.com</a> or from one of <a href="http://www.trolltech.com/download/opensource.html">its mirrors</a>. Work on KDE 4 has already started with making a development branch of KDE compile and run with Qt 4.






<!--break-->
<p>Unlike previous Qt releases, Qt 4 is a collection of smaller libraries which also allowed the restructuring of <a href="http://www.trolltech.com/products/qt/pricing.html">commercial offers</a> into Qt Console for non-GUI development, Qt Desktop Light replacing the Professional and Qt Desktop comparable to Enterprise <a href="http://www.trolltech.com/products/qt/editions.html">editions</a>.</p>

<p>There are five new technologies that are <a href="http://doc.trolltech.com/4.0/qt4-intro.html">new within Qt 4</a>:</p>

<ul>
<li><a href="http://doc.trolltech.com/4.0/qt4-tulip.html">Tulip</a>, a new set of template container classes.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-interview.html">Interview</a>, a model/view architecture for item views.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-arthur.html">Arthur</a>, the Qt 4 painting framework.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-scribe.html">Scribe</a>, the Unicode text renderer with a public API for performing low-level text layout.</li>
<li><a href="http://doc.trolltech.com/4.0/qt4-mainwindow.html">Mainwindow</a>, a modern action-based mainwindow, toolbar, menu, and docking architecture.</li>
</ul>
<p>In addition, the following modules have been significantly improved since Qt 3:</p>
<ul>
<li>A fully cross-platform <a href="http://doc.trolltech.com/4.0/qt4-accessibility.html">accessibility</a> module, with support for the emerging SP-API Unix standard in addition to Microsoft and Mac Accessibility.</li>
<li>The <a href="http://doc.trolltech.com/4.0/qt4-sql.html">SQL module</a>, which is now based on the Interview model/view framework.</li>
<li>The <a href="http://doc.trolltech.com/4.0/qt4-network.html">network module</a>, with better support for UDP and synchronous sockets.</li>
<li>The <a href="http://doc.trolltech.com/4.0/qt4-styles.html">style API</a>, which is now decoupled from the widgets, meaning that you can draw any user interface element on any device (widget, pixmap, etc.).</li>
<li>Enhanced <a href="http://doc.trolltech.com/4.0/qt4-threads.html">thread support</a>, with signal-slot connections across threads and per-thread event loops.</li>
<li>A new <a href="http://doc.trolltech.com/4.0/resources.html">resource system</a> for embedding images and other resource files into the application executable.
</ul>

<p>The <a href="http://doc.trolltech.com/4.0/qt4-designer.html">Qt Designer</a> user interface design tool has been rewritten as a collection of interchangeable components. It now features support for MDI and SDI modes and supports custom widgets. The project editor and the code editor have been dropped.</p>

<p>Qt 3 based applications have to be ported to be able to run with Qt 4. A <a href="http://doc.trolltech.com/4.0/porting4.html">porting guide</a>, a <a href="http://doc.trolltech.com/4.0/qt3to4.html">porting tool</a> as well as a Qt3Support library for obsolete classes are provided. Trolltech aims to maintain the Qt3Support Library for the lifetime of the Qt 4 series, and will also support the Qt 3 series for a minimum of two years beyond the release of Qt 4.</p>

<p>Some <a href="http://www.trolltech.com/products/qt/known-issues.html">known issues</a> are listed which are expected to be fixed in upcoming maintenance releases of Qt 4.0. Qt 4.1 will features certain advanced Qt 3 features rewritten for Qt 4 which are now only available in the Qt 3 support library (eg. Qt 3 canvas, Qt 3 syntax highlighter). It is planned be released late in 2005.</p>







