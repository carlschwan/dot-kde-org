---
title: "KDE CVS-Digest for April 1, 2005"
date:    2005-04-02
authors:
  - "dkite"
slug:    kde-cvs-digest-april-1-2005
comments:
  - subject: "Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-02
    body: "Shouldn't that be Bitkeeper-Digest?  ;)\n\nActually, I suppose the next release will be the SVN-Digest."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-02
    body: "I thought the BitKeeper stuff was to do with \"April Fools\", I'm I wrong? Come on, let the KDE programmers/coders enlighten us here."
    author: "charles"
  - subject: "Re: Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-02
    body: "Of course it was \"April Fools\", we are moving to subversion"
    author: "Albert Astals Cid"
  - subject: "Re: Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-02
    body: "Totally April Fools - thus the emoticon.\n\nIt's a world wide web - April first is still going on in some places as I type this (and was for me when I added the above comment).\n\nSubversion is the new system.  Which makes me pleased, as I use and love svn."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-04
    body: "So let me ask, since you're a Subversion lover: what GUI frontends are there for Subversion? Is there a way to get Cervisia to handle it? The use of a GUI for certain functions is too convenient to give up without some sort of struggle."
    author: "Brandybuck"
  - subject: "Re: Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-05
    body: "Sorry, I just use the CLI tools.  I have an alias ss='svn status', and just do commits by hand.\n\nThat said, I seem to recall that Cervisia has support for Subversion in the CVS version... err... the repository version.  The news popped in on one of the KDE blogs that a developer had gotten it working.  I checked with 3.4, and it doesn't seem to support svn in that release, so I assume it will be in the next release... or you could try the version in the repository."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Bitkeeper, Bitkeeper make me a bit!"
    date: 2005-04-06
    body: "Firefox, Konqueror, Internet Explorer, w3m, lynx, etc.\n\nOnce you activated and configured the svn module in apache that is..."
    author: "PLJ"
  - subject: "great digest this week"
    date: 2005-04-02
    body: "I readed the digest just to discover that we're reaching perfection! Great modifies, great improvements, many speed gains around.. thanks a lot!\nAnd Derek: great selection of the best stuff that happened this week.\nReally had a good time reading it ^_^\n(good move to link the svn tutorial too. go svn!)\n"
    author: "me"
  - subject: "kdelibs/pics/crystalsvg"
    date: 2005-04-02
    body: "Less is more for 16x16. Discussed with ken and david. There is still room\nfor improvements.\nhttp://cvs-digest.org/?diff&file=kdelibs/pics/crystalsvg/cr16-action-up.png,v&version=1.7\n\n\n---\n\n\nErr... I'm sure this is a work-in-progress, right?\nBlack, boring and ugly icons do not belong to kde, right?\nRight? :("
    author: "Anonymous"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-02
    body: "> Black, boring and ugly icons do not belong to kde, right?\n\nyou're right! that is called gnome.\n"
    author: "ac"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-02
    body: "GNOME icons are better than that.\n\n\nBut putting that aside, I agree! Please don't make them all black.. it's not 'crystal' anymore :/"
    author: "konqi"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-02
    body: "woohoo! twice the fun!"
    author: "konqi"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-03
    body: "> GNOME icons are better than that.\n\nWell, even Windows 3.11 icons are better than those.\nPlease, revert those changes..."
    author: "ac"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-03
    body: "Not quite right, gnome is brown on khaki mixed with grey.\n\nProof: just have a peek at gnome.org"
    author: "ac"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-03
    body: "stop whining - do stuff instead :)\n\nhave you seen those in context? i am sure they look a lot better when seen in context...\n\nthese icons may not be perfect - but in the long run this (\"Less is more\") is a step in the right direction"
    author: "bangert"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-04
    body: "It's just experimental work in progress -- not worth a flame-thread."
    author: "Torsten Rahn"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-04
    body: "We always hear that excuse about KDE art.  It's just a work in progress.  We still end up with many bad icons replacing formerly good icons and ugly things like the out of place K logo in Kicker."
    author: "dc"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-04
    body: "What's wrong with the K-logo?"
    author: "uddw"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-04
    body: "It doesn't fit in the Crystal icon theme."
    author: "dc"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-04
    body: "Then make a crystal version of it."
    author: "Morty"
  - subject: "Re: kdelibs/pics/crystalsvg"
    date: 2005-04-04
    body: "The new K-logo is nice though.. :P\n"
    author: "konqi"
  - subject: "amazing digikam!"
    date: 2005-04-02
    body: "it's amazing how these guyz are adding new plugins every damn week! \nkeep up the good work, great app!\n\nPat"
    author: "Pat"
  - subject: "Re: amazing digikam!"
    date: 2005-04-02
    body: "a new one is under construction in my computer for Inpainting a photograph area (using CImg library). Coming soon...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: amazing digikam!"
    date: 2005-04-02
    body: "That's a nice library isn't it? It was the easiest Krita plugin I ever did."
    author: "Boudewijn Rempt"
  - subject: "Re: amazing digikam!"
    date: 2005-04-02
    body: "Boudewijn, i have implemented Save/Restore filter settings in Photograph Restoration digiKam image editor plugin. It's use the _same_ parameters like in your Krita CImg plugin. This will be cool if you can provide a same implementation in krita plugin using the same text settings format. Take a look in Restoration dialog plugin implementation here :\n\nhttp://docs.kde.org/en/HEAD/kdeextragear-3/digikamimageplugins/restoration.html\n\nI have too contacted the CImg Gimp plugin author for the same goal. Provide a common file format between digiKam/Krita/Gimp is important for users.\n\nToo, i have contacted the French CImg author for to have a detailled description of all CImg filter settings. He have send to me too a new implementation about Restoration filter. It's More speed...\n\nI have started a new Restoration plugin handbook. Take a look here (not yet updated in web page):\n\nhttp://docs.kde.org/en/HEAD/kdeextragear-3/digikamimageplugins/restoration.html\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: amazing digikam!"
    date: 2005-04-02
    body: "What an excellent idea -- I never thought of storing the parameters in a file, but I will certainly try to code support for that format into the Krita plugin before the KOffice freeze. I also notice you've added multi-threading support in your Digikam filter -- I'll carefully study your code and see whether I can do the same. I've already gone through Krita and marked the places where I'll need to add threading.  \n"
    author: "Boudewijn"
  - subject: "Re: amazing digikam!"
    date: 2005-04-04
    body: "Boudewijn,\n\nSave/Load Photograph Restoration settings implementation updated in CVS. Please take a look...\n\nA nice day\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: amazing digikam!"
    date: 2005-04-05
    body: "I will -- thanks!"
    author: "Boudewijn Rempt"
  - subject: "Re: amazing digikam!"
    date: 2005-04-03
    body: "it would be nice to implement the resizing function too from http://www.greyc.ensicaen.fr/~dtschump/greycstoration/demo.html it looks really great.\n\nPat"
    author: "Pat"
  - subject: "Re: amazing digikam!"
    date: 2005-04-03
    body: "Yes, it's planned by me. Let's me some days to do it (:=)))\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: amazing digikam!"
    date: 2005-04-05
    body: "digiKam image editor plugin 'Photograph Inpainting' done in CVS. Any previews can be seen at this url :\n\nhttp://digikam3rdparty.free.fr/Inpainting/\n\nAny feedback welcome...\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "KSVG2 DOM manipulation?"
    date: 2005-04-02
    body: "Ah, KSVG2 updates. Great.\n\nI have yet to check it out, but I hope KSVG2 will be ready in time for KDE4 and that it will allow applications to access and modify the SVG DOM directly (not through ECMA) and then re-render.\n\n(I really need a proper SVG library in KDE for Atlantik. My development branch stalled as soon as I required SVG for more dynamic game boards because of some new server features. The old custom board code in Atlantik was crap and KSVG1 lacked support for such DOM manipulations. The result being a very frustrated developer who took a break from KDE development alltogether.)\n"
    author: "Rob"
  - subject: "Re: KSVG2 DOM manipulation?"
    date: 2005-04-02
    body: "Wait a minute.  You're that Rob?  I thought you were just a troll who recently surfaced.  =)\n\nYou're the Rob who disappeared off the face of the planet?  The Olsen twins Rob?  The Kirsten Dunst Rob?  The George Bush Rob?  Oh my... I thought something bad had got to you."
    author: "Anonymous"
  - subject: "Re: KSVG2 DOM manipulation?"
    date: 2005-04-04
    body: "I've moved on to the Cali twins, but that's me! Glad to see my good old buddy Anonymous is still around!\n\nNothing bad happened: in fact, so many good things happened that I couldn't be arsed to get my DSL running again after the server harddrive broke down. And I still can't be, although I will put some of my stuff on-line again soon at regular IPP."
    author: "Rob"
  - subject: "Re: KSVG2 DOM manipulation?"
    date: 2005-04-02
    body: "Hi Rob,\n\nYes, this time the dom manipulating should work.\nWe can't blame anyone else this time, since all\nsupport code is written by ourselves ;)\nCheers,\n\nRob B."
    author: "Rob Buis"
  - subject: "Will the real Rob please step forward?"
    date: 2005-04-03
    body: "... and I thought all KDE developers were named Matthias ..."
    author: "Matthias"
  - subject: "Re: KSVG2 DOM manipulation?"
    date: 2005-04-05
    body: "Ah, great. Will have to check out kdenonbeta I suppose. Do you think KSVG2 will make KDE4?\n"
    author: "Rob"
  - subject: "Re: KSVG2 DOM manipulation?"
    date: 2005-04-04
    body: "But the website seems quite out of date? No mention either about ksvg2. So what's the difference with 1? :-)\n\nJust curious."
    author: "Quintesse"
  - subject: "Binding screenshots with PrintScreen"
    date: 2005-04-02
    body: "Folks, for professional reasons (i.e. I have no option) I am using windows xp at work now. It sucks as compared to KDE. But I disgress. The one thing that I found more than useful is the following key bindings:\n\n* [PrintScreen] copies a full-screen screenshot to the clipboard\n* [Alt]+[PrintScreen] same thing, but it captures the active window instead.\n\nThen, you just [ctrl]+[v]  in you mail editor or word processor and drop the screenshot. I can't tell you how much easier your life gets with these bindings. \n\nAre there similar bindings in KDE-3.4 ? I am running 3.2.3 (Mandrake's most recent stable)\n\nThanks and Cheers!"
    author: "MandrakeUser"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-02
    body: "I suggest you to visit http://bugs.kde.org/ and file your wish there as \"wishlist\"... :-)"
    author: "ac"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-03
    body: "Thanks for the post :-)\n\nI definitely will, but I wanted to check real quick if the functionality is available or being implemented, since my kde version is a bit outdated ...\n\nI'll wait a couple days and see if someone posts in this thread, and open a wish list if it makes sense ..."
    author: "MandrakeUser"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-03
    body: "Yes, it is in KDE 3.4, and combined with klipper it's even better than in Windows (it keeps record of all the screen caps you took). I found this out just today."
    author: "Pilaf"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-03
    body: "Well, that really cool, thanks a lot Pilaf! And thanks to all the others who posted in this thread. "
    author: "MandrakeUser"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-03
    body: "Those bindings work as you have described in KDE 3.4 and they did in the KDE version prior to 3.4."
    author: "charles"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-03
    body: "In KDE 3.4, [Alt]+[PrintScreen] will take a screenshot of the window which you can then paste into a new KMail message (it will actually end up as an attachment).  Just plain old [PrintScreen] didn't do anything for me.\n"
    author: "Zippy"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-03
    body: "On my machine, at least, 'Desktop Screenshot' is bound to Ctrl+PrintScreen by default. You could easily change that to just PrintScreen, though."
    author: "Dolio"
  - subject: "Re: Binding screenshots with PrintScreen"
    date: 2005-04-04
    body: "yes, and afaik, in kde 3.5 you can past the image in the message itself, instead of as an attachment :D"
    author: "superstoned"
  - subject: "Qt-Mozilla"
    date: 2005-04-03
    body: "It started soooo promising, but now as Zack has a new employer it might be definitely dead... or will TT want to sponsor it really?"
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "Don't you think that Lars and Zack now being together all day long will rather speed it up again?"
    author: "Anonymous"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "I have the impression that Zack started a lot of very cool things but didn't finish a lot of them....\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "Zack goes for stuff that really matters. Luckily isn't that sort of 'proud person' that starts a project and goes on and on defending it even if it's proven a poor project. And then he is locked up to that project. Qt is good stuff, and working on bleeding edge xfree is good too :-)\nAbout qt-mozilla.. I love khtml.\n"
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "Getting Mozilla/Qt is not poor but does matter for the business desktop."
    author: "Anonymous"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "> About qt-mozilla.. I love khtml.\n\nMe too, and I use it 95%+ in KDE.\n\nBut, Firefox being KDE friendly will help Firefox, and KDE...\n\nEnterprises may want to use Firefox for some reasons (e.g. their users had \nFirefox, Thunderbird and OO in windows, and they would migrate to Linux, so the \nusers would find the same applications), and if KDE copes well (File & Print \ndialogs, kdewallet), the better!\n\nBtw, Konqueror is faster and much more versatile, that's why I use it.\n\nUsers migrating from windows might use Fx for web, and konq for filebrowsing,\nuntil they notice that konq is a great browser aswell.\n\nSo it's good when Fx and konq are great. First to help users with a nicely into KDE intergrated browser. If they see the poor gtk file+print-dialogs they might \nnot be amused I fear.\n\nFx needs Qt."
    author: "ac"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "don''t need to bitch here but people usually call Firefox FF,  not Fx."
    author: "Pat"
  - subject: "Re: Qt-Mozilla"
    date: 2005-05-01
    body: "> people usually call Firefox FF, not Fx.\n\nNo they don't (or maybe they do, but it's wrong). See The Firefox FAQ [1].\n\n[1]: http://www.mozilla.org/support/firefox/faq#spell-abbreviate"
    author: "David House"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-04
    body: "\"Zack goes for stuff that really matters. Luckily isn't that sort of 'proud person' that starts a project and goes on and on defending it even if it's proven a poor project. And then he is locked up to that project.\"\n\nThat's a good one.\n\nAre you quoting this from his job resume?  =)"
    author: "Anonymous"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "They may even attract the interest of new developers, I think some of their other co-workers may have some skills with Qt.\n"
    author: "Morty"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "Some skills? If their co-workers don't have ultimate Qt skills then who else? :-)"
    author: "Anonymous"
  - subject: "Re: Qt-Mozilla"
    date: 2005-04-03
    body: "The people who send patches to TT? ;-)"
    author: "Hans"
---
<a href="http://cvs-digest.org/index.php?newissue=apr12005">In this week's KDE CVS-Digest</a>:

<a href="http://svg.kde.org/">KSVG2</a> can now do animations.
<a href="http://www.koffice.org/kexi">Kexi</a> gains read/write form support.
<a href="http://extragear.kde.org/apps/digikam/">digiKam</a> adds a photo restoration plugin.
New releases of <a href="http://kile.sourceforge.net/">Kile</a>, <a href="http://extragear.kde.org/apps/amarok/">amaroK</a> and <a href="http://www.kubuntu.org.uk/">Kubuntu</a>.
Get ready for the move to Subversion!


<!--break-->
