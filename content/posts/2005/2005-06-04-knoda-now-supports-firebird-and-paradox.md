---
title: "Knoda Now Supports Firebird and Paradox"
date:    2005-06-04
authors:
  - "hknorr"
slug:    knoda-now-supports-firebird-and-paradox
comments:
  - subject: "This sounds pretty impressive!"
    date: 2005-06-03
    body: "I know only little about *sql, all my stuff was one database managed by Ruby on Rails. But if knoda is really that good I should give it a try :)"
    author: "cn"
  - subject: "Kexi?"
    date: 2005-06-03
    body: "Does this mean there is more than one kde database app?"
    author: "anonymous"
  - subject: "knoda vs kexi"
    date: 2005-06-04
    body: "it looks like there is a little competition between knoda and kexi. Everytime the dot announces something about Knoda, kexi devs say \"oh but kexi has been doin it for years already\" or \"that's cool  but kexi form rules\" and then we get an article on the dot about kexi :)"
    author: "Pat"
  - subject: "Re: knoda vs kexi"
    date: 2005-06-04
    body: "It's sometimes cool to have competition... But does the teams plan some cooperation too? I think that a really complete DBMS Apllcation lacks now on KDE, Kexi and Knoda are almost beating it, but I think if having two teams working on separate sides don't complicate the efforts."
    author: "Ivan da Silva Bras\u00edlico"
  - subject: "Congrats Horst!"
    date: 2005-06-04
    body: "Knoda is coming along nicely. Michal and I have been distracted but we hope to finally finish our plugins for Kommander soon and it's great that it will inherit access all these databases. I have a dialog now plugging into a Quanta toolbar that enables me to construct SQL point and click for web pages that I plan on releasing as a demo for the plugin. It's great to be able to use so many databases. MySQL gets frustrating when you need a real database. ;-)"
    author: "Eric Laffoon"
  - subject: "What's with MaxDB"
    date: 2005-06-04
    body: "What's with MaxDB/SapDB? Isn't that an Open Source SQL server?"
    author: "Forgot my name"
  - subject: "Re: What's with MaxDB"
    date: 2005-06-04
    body: "It is supported via the ODBC driver\n\nHorst"
    author: "Horst Knorr"
  - subject: "How about drivers included"
    date: 2005-06-04
    body: "Hi,\n\nI recently tried to get KNoda (with postgress support) on my gentoo box, but it depends on the entire postgress database (which I dont really want to install). Is it possible to include just some kind of postgress driver? (from Java I'm used to having little JDBC drivers) - I just want to connect to a remote postgress database.\n\nThanks \nAnders"
    author: "anders"
  - subject: "Re: How about drivers included"
    date: 2005-06-04
    body: "This is not a dependency of knoda or hk_classes. You just need the postscript client library (libpq) and its header files installed. If the package you installed depends on more, you should get in contact with the rpm package maintainer / ebuild maintainer\n\nHorst"
    author: "Horst Knorr"
  - subject: "Re: How about drivers included"
    date: 2005-06-04
    body: "Wonderful. But I see how the text under \"requirements\" could be misunderstood.\n\nhttp://hk-classes.sourceforge.net/download.html#knoda\n\nThanks\nAnders "
    author: "anders"
  - subject: "my wish..."
    date: 2005-06-05
    body: "...is for developers to create a package with *all* dependencies so that installation can be as smooth as possible."
    author: "charles"
  - subject: "Re: my wish..."
    date: 2005-06-06
    body: "What you want is called \"Linux distribution\"."
    author: "Anonymous"
  - subject: "Re: my vote for simple installs"
    date: 2008-06-12
    body: ">> What you want is called \"Linux distribution\".\n\nHave you used a Mac before? When someone wants to install a program [eg. Thunderbird] on a mac they only very very rarely need to worry about dependencies. They install the program and it *works*.. and that is what the end user wants. All dependencies are taken care of already, they are either in the package or already part of the standard OS [for *nix ppl in guess that this setup would be 'statically linked'].\n\nAs an added bonus, when it comes time to upgrade to their brand new laptop they just drag the application package over to the new computer and it works! Dead simple - the way computers should be.\n\n.. and no, I am not some apple nut. I actually don't work on a mac, although I am surrounded by them and I am very impressed by their stability and ease of maintenance [I work as tech support].\n"
    author: "Yohans"
---
<a href="http://www.knoda.org">Knoda</a> is a database frontend for KDE.  With its latest release, Knoda introduces support for Firebird and Paradox databases, now supporting all open source SQL servers.  Besides managing tables and queries, Knoda also lets you create forms and reports, scriptable via Python.


<!--break-->
<p>
Supported database formats are:
<ul>
<li>DBase,</li>
<li>Firebird,</li>
<li>MS Access,</li>
<li>MySQL,</li>
<li>ODBC,</li>
<li>Paradox,</li>
<li>Postgres,</li>
<li>SQLite</li>
</ul>
Other enhancements are data aware image fields for forms, and the possibility to store and load connection information, so login can be much easier. Also some bugs are fixed.





