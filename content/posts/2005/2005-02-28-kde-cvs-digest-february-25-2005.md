---
title: "KDE CVS-Digest for February 25, 2005"
date:    2005-02-28
authors:
  - "binner"
slug:    kde-cvs-digest-february-25-2005
comments:
  - subject: "first?"
    date: 2005-02-28
    body: "nice job again. pity it came late, as the dot was down :D"
    author: "superstoned"
  - subject: "Re: first?"
    date: 2005-02-28
    body: "It was posted earlier, it was on planetkde.org during the downtime. Anyway, thumbs up Derek! Oh, and also thumbs up to the admins behind the Dot, since we're back online!"
    author: "raoulduke2"
  - subject: "Format of digest"
    date: 2005-02-28
    body: "I miss the nice table on the previous format of the digest, where\nall commits were separated by  subproject and by cathegory: bugfixing,\nnew features, optimization, etc.\n\nIs it possible to have that table back?"
    author: "anonymous"
  - subject: "Re: Format of digest"
    date: 2005-03-02
    body: "http://cvs-digest.org/index.php?issue=feb252005"
    author: "EvilPenguin"
  - subject: "Re: Format of digest"
    date: 2005-03-02
    body: "The problem is that the types other than bugfixes, etc. will not be indexed by the TOC. Backports, Branch development, Documentation, Graphics (icons, splashes, etc) Refactor (qt4 porting, app rewrites).\n\nThe amount of activity is so high I could either ignore most interesting work, ignore modules or applications (to the chagrin of the developers) or break everything up into smaller pieces. There were around 140 bugfixes last week. I doubt if very many readers wade through them all.\n\nIOW, I'm not supporting the old layout. Too constrained.\n\nDerek"
    author: "Derek Kite"
  - subject: "status notification list"
    date: 2005-02-28
    body: "This small change results in a vast speedup when joining large channels.\nBUG:99922\n\njust tested it, it's really faster now :)\n\nit would be great to get rid of the painful status notification list when we open a channel with hundreds of people we get huge tons of\n\nuser1 is now Away\nuser2 is now Away\netc....\n\nwhich is not very usefull and takes a long time (though less then before)"
    author: "Pat"
  - subject: "Fonts"
    date: 2005-03-01
    body: "I was surprised to see people saying 12 is a huge font size. Yes SuSE decreased it, and I put it back to 12. I'm using a 1024x768 definition in a 15\" TFT. "
    author: "John SUSE Freak"
  - subject: "Re: Fonts"
    date: 2005-03-02
    body: "This depends. Some X servers set wrong DPI value, but they are justified that \"most web pages are designed for 96 DPI\".\nSee this bugzilla entry for some discussion:\nhttp://qa.mandrakesoft.com/show_bug.cgi?id=8533\n\nIf you ask me, those web pages should be redesigned ;)"
    author: "vljubovic"
  - subject: "Woohoow"
    date: 2005-03-03
    body: "It's great to see so much progress!\n\n--\nTed Baker\ninfo@trendyhosting.com"
    author: "Ted Baker"
---
In <a href="http://cvs-digest.org/index.php?newissue=feb252005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/?newissue=feb252005&all">all on one page</a>): <a href="http://digikam.sourceforge.net/">digiKam</a> adds a film grain plugin. <a href="http://www.koffice.org/kexi/">Kexi</a> adds scripting bridge and startup shortcut files. <a href="http://developer.kde.org/development-versions/kde-3.4-release-plan.html">KDE 3.4</a> is being prepared for release.

<!--break-->
