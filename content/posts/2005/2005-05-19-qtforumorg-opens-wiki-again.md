---
title: "QtForum.org Opens Wiki Again"
date:    2005-05-19
authors:
  - "ckienle"
slug:    qtforumorg-opens-wiki-again
comments:
  - subject: "KDE Passport?"
    date: 2005-05-19
    body: "> The wiki is seamlessly integrated into QtForum.org and KDE-Forum.org.\n> That means if you have an account at QtForum.org or at KDE-Forum.org,\n> you can simply use your username and password from there and login\n> into the wiki.\n\nSound like a KDE Passport service is not far off.\n\n:)\n\n_c."
    author: "cies"
  - subject: "Re: KDE Passport?"
    date: 2005-05-19
    body: "Yeh :) \n\nNo - why should we \"force\" the user to register three times? \n\n:) I think the \"one password\" approach is pretty nice."
    author: "Christian Kienle"
  - subject: "Re: KDE Passport?"
    date: 2005-05-19
    body: "im not complaining. in fact i'd carry my KDE Passport everywhere..."
    author: "cies"
  - subject: "Re: KDE Passport?"
    date: 2005-05-19
    body: "Yeh just to make it clear :)\n\n"
    author: "Christian Kienle"
  - subject: "Like speaking of the devil..."
    date: 2005-05-20
    body: "http://it.slashdot.org/it/05/05/19/1451231.shtml?tid=95&tid=230&tid=172\n\nokay not a KDE passport but an Open/Free/Libre/... passport"
    author: "cies"
---
The <a href="http://qtwiki.qtforum.org/">wiki</a> of <a href="http://www.qtforum.org/">QtForum.org</a> has opened its doors again. After a long period of downtime because of spam, the QtForum.org team decided to switch to more robust wiki software. The result is that the wiki is now based on a modified version of Mediawiki. 




<!--break-->
<p>Modified because <a href="http://qtwiki.qtforum.org/mediawiki/index.php?title=User:Axeljaeger">Axel Jäger</a> and <a href="http://qtwiki.qtforum.org/mediawiki/index.php?title=User:Wysota">Witold Wysota</a> built in some great features:</p>

<ol>
<li>The wiki is seamlessly integrated into QtForum.org and <a href="http://www.kde-forum.org/">KDE-Forum.org</a>. That means if you have an account at QtForum.org or at KDE-Forum.org, you can simply use your username and password from there and login into the wiki.</li>

<li>If you publish Qt code and you use a Qt class like QApplication, it is automatically linked to the corresponding Qt documentation page. </li>

<li>Axel and Witold also built in Qt syntax highlighting.</li>
</ol>

<p>QtForum.org is supported by <a href="http://www.trolltech.com/">Trolltech</a>. Many thanks to Trolltech for that.</p>

<p>The wiki can be reached at <a href="http://qtwiki.qtforum.org/">http://qtwiki.qtforum.org</a>.</p>

