---
title: "aKademy 2005 Dates and Call for Papers"
date:    2005-03-22
authors:
  - "jriddell"
slug:    akademy-2005-dates-and-call-papers
comments:
  - subject: "Bad date for students"
    date: 2005-03-22
    body: "I think this is not a good date for any student, because these days they are preparing september exams (or ever doing these exams). Really strange that a University has approbed this days for the aKademy, because all people are really occupied with this."
    author: "Carlos"
  - subject: "Re: Bad date for students"
    date: 2005-03-22
    body: "All people? Well, here the exams are earlier in August. I guess it depends on how the education is organized in your country, maybe even at your university. You are assuming this will be the same everywhere, which clearly isn't true."
    author: "Andre Somers"
  - subject: "Re: Bad date for students"
    date: 2005-03-22
    body: "Not all the people are students, and not all the students needs to study for september ;)\n\nBy the way, is the best date in summer time and similar to the celebration date of the akademy 2004."
    author: "HnZeKtO"
  - subject: "Re: Bad date for students"
    date: 2005-03-22
    body: "...and not all of us has summer in August-September :)"
    author: "Asgeir"
  - subject: "And what about the feria?"
    date: 2005-03-22
    body: "And when is the \"Feria de M\u00e1laga\" (http://www.nerjanow.com/the_feria_at_malaga.htm) this year? Can we enjoy with this event or not?\n\nI don't know if many more developers and users can join us in akademy if it coincides with the feria, but for me, it's another reason to go ;-)\n\n"
    author: "melenas"
  - subject: "Re: And what about the feria?"
    date: 2005-03-23
    body: "It will be (99% sure) from August 14th - 21th, so you'll can enjoy it and take a rest during a few days before the aKademy takes place. ^_^"
    author: "eth0"
  - subject: "Re: And what about the feria?"
    date: 2005-03-27
    body: "Yes, but add a couple of days at the beginning. The feria will be from the 12th to the 21th\n"
    author: "Antonio Larrosa"
  - subject: "Good motivation to study for june!"
    date: 2005-03-22
    body: "Well, here in Belgium, if students did what they had to do in June, they don't need to repass their exams in September. So actually this years Akademy is a good motiviation for students :-)"
    author: "Anonymous Coward"
  - subject: "Re: Bad date for students"
    date: 2005-03-22
    body: "I was considering going, but my fall semester starts in late August. :/\n\nObviously you can't please everyone."
    author: "Ian Monroe"
  - subject: "Re: Bad date for students"
    date: 2008-03-18
    body: "I most definitely agree with this, I came to learn spanish in Spain and realised that courses end at different dates, virtually every campus or faculty has its own calendar. www.spanish-university.es. Here you can find several lective timetables"
    author: "Dominick"
  - subject: "What Format"
    date: 2005-03-23
    body: "How should these abstracts be submitted? By regular e-mail, or some other digital format? Open Office format, PDF etc.?"
    author: "David"
  - subject: "Re: What Format"
    date: 2005-03-27
    body: "The way to submit an abstract is by sending an email to the akademy-team address with an attached document in KWord, OpenOffice, PDF, or plain text format."
    author: "Antonio Larrosa"
---
KDE's World Summit, aKademy 2005, will be held in the <a href="http://www.uma.es">University of
Málaga</a> from <!-- Please don't abuse bold in the text --><em>Saturday 27th of August to Sunday 4th of September</em><!-- Please don't abuse bold in the text -->.  The summit starts with a 2 day user conference followed by a 2 day developers conference then a week long coding marathon.  If you plan on coming you should subscribe to <a href="https://mail.kde.org/mailman/listinfo/kde-conference">the aKademy mailing list</a>.  Help in finding sponsors is especially welcomed.  The call for papers is below.  <strong>Update:</strong> The KDE e.V. members meeting will be on Friday 26th.

<!--break-->
<h2>aKademy 2005 Overview</h2>

<ul>
<li>Module 0: <!-- Please don't abuse bold in the text --><em>KDE e.V. Meeting</em>, Fri 26 August </li>
<li>Module 1: <!-- Please don't abuse bold in the text --><em>KDE User and Administrator Conference</em>, Sat 27 to Sun 28 August </li>
<li>Module 2: <!-- Please don't abuse bold in the text --><em>KDE Developers and Contributors Conference</em>, Mon 29 to Tue 30 August</li>
<li>Module 3: <!-- Please don't abuse bold in the text --><em>Coding Marathon for KDE Developers and Contributors</em><!-- Please don't abuse bold in the text -->, Wed 31 August to Sun 4 September</li>
</ul>

<p>All KDE developers, documentation writers, translators, promoters, other
contributors as well as KDE power users, friends and supporters should consider
taking part in all or at least some of this grand KDE World Summit.</p>

<p>Like last year, we will also invite some well known hackers from other Free Software projects to participate.</p>

<p>The days specially geared towards users and system administrators are dubbed <em>UserConf</em>. This will be the first weekend.  The days scheduled for developers and contributors are named <em>DevConf</em> and will be held on Monday and Tuesday. There will be 200 seats available.</p>

<h2>aKademy Developers Conference: Call For Papers</h2>

<p>This is a Call for Papers and Talks at our KDE Contributors and Developers
Conference, aKademy 2005. We encourage you to submit proposals which deal with
current and future KDE-related development. We would like to have a special emphasis on making KDE an increasingly integrated desktop environment and ensuring it remains the best platform for third party applications by allowing them to make good use of KDE's powerful and advanced technologies.</p>

<h3>Topics of Interest for the Contributors Conference Presentations</h3>

<p>Consider the following:</p>
<ul>
<li> Do you have a particular expertise related to KDE development or a
      special KDE or external program that could be useful for your fellow KDE
      contributors?</li>

<li> Do you want to present a particularly cool KDE technology, a tool, a
      program or anything else that helps KDE development platform users to
      become more productive?</li>

<li> Do you have an exciting new idea to share with your fellow developers
      regarding KDE development?</li>

<li> Have you encountered an interesting development taking place outside of
      KDE's usual radar which you want to make known inside the KDE developer
      community?</li>

<li> Do you want to discuss a new Freedesktop.org related idea or an idea for the advancement of GNU/Linux desktop
      development in general?</li>
</ul>

<p>Then consider giving a talk or holding a mini-tutorial at the KDE Contributors
and Developers Conference (27th/30th August) or during the following Coding Marathon.</p>

<p>We solicit submissions for presentations and tutorials from, but not restricted
to the following fields. Everything KDE-related will be considered:</p>

<ul>
<li> Integration of non-KDE programs into the KDE desktop</li>
<li> KDE and Qt programming tricks</li>
<li> DCOP, KParts, KIO Slaves, KOffice, multimedia etc. development</li>
<li> Freedesktop.org topics</li>
<li> Programming tools</li>
<li> Programming patterns and development strategies</li>
<li> Project management issues</li>
<li> Internationalisation</li>
<li> Documentation</li>
<li> Quality assurance</li>
<li> Usability, accessibility, interoperability</li>
<li> Artwork</li>
<li> Promotion</li>
</ul>

<h2>Submission Guidelines</h2>

<p>Those proposing to present papers should take note of the following guidelines:</p>

<ul>
<li>You must submit a 150-300 word long abstract including the name
      and e-mail address of the author(s) to akademy-team<strong></strong><span>@</span>kde.org by Sunday, Jun 5.</li>
<li>The conference language is English.</li>
<li>Abstracts will be reviewed by the program committee based on content,
      presentation and suitability for the event.</li>
<li>By submitting an abstract you give permission to publish it on the
      conference web site, and probably in a printed conference booklet.</li>
<li>We expect you to have a digital version of your fully worked out talk
      ready by the time of the conference at the latest.</li>
</ul>

<p>Time slots will likely be scheduled for a 45 minutes talk and 15 minutes Q&amp;A.
In some cases, it may be possible to occupy two slots for a total talk time of
90 minutes or offer short presentations of 15 minutes each. Please note the
estimated talking time in your application.</p>

<p>Finally, "Birds of a Feather" (BoF) sessions can be announced in advance or
planned on the spot. A limited number of group rooms will be available for
BoFs. The following
week, used for the KDE Coding Marathon is especially recommended for BoFs,
Forums and Special Interest Group sessions.</p>

<h2>aKademy Users Conference: Call For Papers</h2>

<p>Consider the following:</p>

<ul>
<li>Do you have a particular expertise related to KDE usage or a special KDE
      program that could be useful for your fellow KDE users or any MS Windows
      user converting to a Unix desktop?</li>

<li>Do you want to present a particularly cool KDE technology, a tool, a
      program or anything else that helps Unix desktop users to become more
      productive?</li>

<li>Do you have an interesting success story or use case about KDE to tell
      which can help other users or administrators deploy KDE on desktop
      workstations?</li>
</ul>

<p>Then consider giving a talk or a tutorial at the KDE User and Administrator Conference.</p>

<p>We solicit submissions for presentations and mini-tutorials about the following fields (but not restricted to these).  Everything KDE-related will be considered. We aim to heavily target on how KDE fulfills the needs of administrators and users in corporate or governmental network environments when they decide to migrate to KDE:</p>

<ul>
<li>Showcasing KDE as the powerful integrated platform for third party programs that it currently is</li>
<li>KDE and Qt programs for Unix desktops and servers</li>
<li>DCOP, KParts, KIO Slaves, KOffice, multimedia etc usage in KDE desktop deployments</li>
<li>KDE service menus</li>
<li>KDE administration tools</li>
<li>Desktop installation, maintenance, update and support strategies in
        large scale enterprise environments</li>
<li>User management issues in large networks</li>
<li>User training</li>
<li>KDE power user tips and tricks</li>
<li>KDE kiosk mode</li>
<li>Internationalisation</li>
<li>Documentation</li>
<li>Quality assurance</li>
<li>Usability, accessibility, interoperability</li>
</ul>

<p>Especially welcome are live demonstrations which highlight one of our many (new or established) desktop killer applications or technologies. Also welcome are programs developed external from KDE itself, but showcasing the power of KDE integration capabilities or simply their benefits to users. </p>

<p>We encourage you to consider using <a href="http://www.klaralvdalens-datakonsult.se/?page=products&sub=kdexecutor">KD Executor</a> made by KDAB (which is free of charge for all KDE contributors) to create a short demo script covering your presentation, which may be re-used at other events and by other KDE contributors and promotors</p>

<p>As well as KDE developers we also encourage documentation writers, translators, promoters, deployers and other contributors and KDE power users to respond to this call for papers.</p>

<h3>Submission Guidelines</h3>

<p>Those proposing to make a presentation should submit a 150-300 words abstract
including the name and e-mail address of the author(s) to akademy-team<span>@</span><strong></strong>kde.org.
The presentation should be at least partly a live demo of the KDE
program you are talking about if applicable. Please indicate if the submission is
meant for the "Conference for KDE Contributors and Developers" or for the
"User and Administrator Conference" part.  The following information should be included:

<ul>
<li> Who is the target audience of the presentation/talk?</li>
<li> What are the specially highlighted features of the app/topic you are presenting?</li>
<li> Which features are you going to show live and how?</li>
</ul>

<p>The conference language is English, but we will also accept Spanish language submissions (considering that a large part of the audience for the Users Conference will be from Spain and not necessarily able to understand English well).</p>

<p>Abstracts will be reviewed by the program committee based on content, presentation and suitability for the event. By submitting an abstract you give permission to publish it on the conference web site, and probably in a printed conference booklet</p>

<h2>Speaker Incentives and Financial Compensation</h2>

<p>We only have a limited budget for this conference, and cannot generally pay
travel expenses or any other reimbursement or gratification to speakers. We can
provide accommodation and boarding at a very low price (expect about &euro;15 
per day for bed and breakfast, but do not expect luxury accommodation).
Please note that even if we can grant you a bursary, you will still be expected
to pay a part of the trip yourself, as well as your boarding and lodging.</p>

<h2>Important Dates</h2>

<p>Please submit your contributions no later than midnight ending Sunday June 5th.  Acceptance notificaition will be by Thursday June 30.</p>















