---
title: "NewsForge: KDE on Windows Philosophers Debate"
date:    2005-01-06
authors:
  - "ee."
slug:    newsforge-kde-windows-philosophers-debate
comments:
  - subject: "Pointless Argument"
    date: 2005-01-06
    body: "Arguing about whether or not KDE should be ported to Windows is rather pointless. If it's going to happen it will happen, and if it doesn't then it doesn't. Unlike proprietary desktops, KDE does not have the authority to tell KDE developers what to do. Even if every member of the core team thought it was a bad idea, they still couldn't stop someone from doing it. Or vice versa.\n\nPersonally I don't think it should be ported (though I would like to see a KHTML based browser available for Windows), but my opinion doesn't matter. KDE isn't a dictatorship. Heck, it ain't even a democracy, because the minority can freely ignore the majority."
    author: "Brandybuck"
  - subject: "Re: Pointless Argument"
    date: 2005-01-07
    body: "of course it isn't a dictatorship, and certainly if someone is willing to spend their time doing this it will happen.\n\nit does not make this, or any other, discussion pointless, however. openly discussing things is how people come to:\n\no Understand each other\no Coordinate with each other\no Create common goals and sets of focus\n\nthese are all very important dynamics when it comes to the health and continued progress of a project such as KDE.\n\nthe irony is that you say there's no point in discussing if you can't control people, and that the project doesn't have control over the developers. and yet the project stays on its feet without having to control everyone due to discussion.\n\nbut when you say \"even if every member of the core team thought it was a bad idea, they still couldn't stop someone from doing it\" you are both wrong and right. the project certainly _does_ have the ability, right and even responsibility to offer some guidance over what goes into KDE's CVS and ships with it. so a person can do anything they wish with KDE within the rights of the licenses used (GPL, LGPL, BSD, etc), but the project as a collective whole says whether or not it becomes part of KDE proper.\n\nas a side note, separating \"the KDE project\" and \"the KDE developers\" is a false dichotomy. they are one and the same."
    author: "Aaron J. Seigo"
  - subject: "Re: Pointless Argument"
    date: 2005-01-07
    body: "\"it does not make this, or any other, discussion pointless, however.\"\n\nI didn't say \"discussion\", I said \"argue.\" Slight difference :-)"
    author: "Brandybuck"
  - subject: "Re: Pointless Argument"
    date: 2005-01-07
    body: "> I didn't say \"discussion\", I said \"argue.\"\n\ndiscussion, debate, argument, <insert quasi-synonym here> ... 6 of one, half dozen of another?\n\ni'll be the first to admit that a lot of the discussion that's happened in the broader (read: user) community has not been overly useful to determining direction. when i first broached this topic in my blog last year i was speaking to my fellow developers, but the topic was quickly picked up and moved into the realm of the user community and taken as if i were addressing everyone. (note to self: start labeling blog entries with who the intended audience is.) while i'm sure it's been entertaining for people \"on the outside\" to toss around the conversation in armchair quarterback fashion, that part of the discussion likely hasn't had much impact on the direction of efforts of those actually involved with the project.\n\nthe conversation/argument/whatever _within_ the developer community is important and not useless. it may be a bit odd that these discussions happen in public, but that's because the logistical structure of open source is \"inside-out\" compared to more traditional models.\n\nthe benefit i see to pieces like the one Tom has written is that it serves as something of a summary of the arguments put forward by various people thus far. these are useful as touchstones when it comes to these discussions since they can extend over a good amount of time and be spread out widely on the internet. summaries bring all the value, whatever it may be, into one place.\n\nunrelated to the above posts, here's another piece of irony that this whole experience brought with it for me:\n\na lot of people have said to me that i can't dictate agendas to other people[1], and therefore shouldn't have written that blog entry and that we this thread should be dropped. i suppose it's ok to dictate as long as you're the dictator? ;-)\n\n[1] btw, \"dictating\" was never my goal or intention. i was attempting to open discussion on the matter. seems to have been successful. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Pointless Argument"
    date: 2005-01-07
    body: "Um, I don't think the minority can freely ignore the majority. Obivously anyone can freely make their own project, but it can't be called KDE anymore."
    author: "Ian Monroe"
  - subject: "Bollocks 'R' Us"
    date: 2005-01-06
    body: "Come on, do we need this sort of pretentious crap? Does it really further our cause? \n\nYes, Windows is a dog, sucking innovation/dollars out of the IT industry - but it is a very simple story to tell, and we don't need to bring in obscure Greek philosophers.."
    author: "Richard Dale"
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-06
    body: "I've seen this debate over and over again, in many forums.  This article presented it from all sides in a novel and entertaining way.  I thought it was a fun read, and that's why I submitted it to the dot.  \n\nWhat is this \"cause\" you refer to?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-07
    body: "First, let me say that I enjoyed the form of the article. It is refreshing to see cool philosophers discussing instead of heated arguments. But I find the debate quite restricted. It is just a reharsal of what Aaron has already said in his blog and on kde-promo.\n\nHowever, I find that there is a lot more that what has been presented in this article:\n\n- as a linux geek working in enterprise desktop, I am forced to use windows and I would enjoy using KDE, independantely of convincing anybody of the value of linux and fre software. So, my pleasure is sacrificed for a greater cause. That's ok but I want to remind it. Having KDE running on windows would allow me to contribute from my work place...\n\n- as a developer, I like my applications to be used by the maximum number of people. So I would like those users on windows to use it too (which is why I use PyQt).\n\nI also found false assumptions:\n- I do not develop software to spread the message of freedom, I develop it for fun and because I like sharing. Developing KDE on windows fits in both these goals. Unfortunately, this is the core of the reasoning of the article.\n\nFinally, one argument is bullshit:\n- what you develop for windows XP, 98 or 95 will always run on future versions of windows. Even windows 3.1 applications still run on windows. So if KDE runs on windows XP, it will run forever, just like any other windows program developed out there. Microsoft has been better at ensuring backward compability than any other software company in the world. And no software patent or licensing policy will ever change that because that would be liking shooting in one's foot.\n\nWhat KDE will not have is interoperability with .NET and stuff like that. But you know what: plenty of other windows applications won't either. And despite what microsoftee says, it does not bother the users. If you look at popular applications on windows like winamp or quick time, they have a completely different interface than typical applications, they do not use any microsoft technologies and still many users use them happily.\n\nSo my conclusion is that the philosophers have missed at least two arguments, used false assumption and made false claims. It does not make them rank very high as philosophers and with such poor material, their conclusion is void.\n\nAnd you know what ? It does not matter because everybody already has its own opinion priori to reading the article and I doubt that it will change."
    author: "Philippe Fremy"
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-07
    body: "> I do not develop software to spread the message of freedom\n\ntoo many people are reading this into it because they are used to \"Freedom\" being the frame of reference for these conversations, but it isn't the frame of reference here. i wish people would stop reading politics into this discussion that are not really there. it does, after all, help to understand the discussion if one understands what the topic of discussion is.\n\nmy original article was 100% about calculating the costs associated with these developments when it comes to mid-to-long term user base, developer base and utility. nothing about \"Freedom\" politics.\n\nso, to repeat one more time so this is painfully clear: this is not about Free Software politics.\n\n> what you develop for windows XP, 98 or 95 will always run on \n> future versions of windows. \n\nhow many people run a win95 app on XP because they want a win 95 app? how many would take the XP version over the 95 version given feature parity? i know the only reason we keep certain win98 apps around here is because we have no choice (e.g. they were never upgraded post-win98)\n\ni also suggest you look at the history of Novell producs on windows, Lotus1-2-3 or DR-DOS. you don't have to make an app un-runnable by sacrificing backwards compatibility to effectively break the application or make it irrelevant within it's given niche.\n\n> It does not matter because everybody already has its own\n> opinion priori to reading the article and I doubt that it will change.\n\nglad to see you're a mentally flexible individual ;-)\n\nbut seriously: how do you think opinions get formed? how does a project run by community (versus by dictatorship or by committee) coordinate itself?\n\nit isn't through silence. it isn't through people acting as lone wolves. and it certainly isn't through defeatist attitudes that boil down to \"why bother because you can't change anything.\""
    author: "Aaron J. Seigo"
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-08
    body: "> so, to repeat one more time so this is painfully clear: this is not about Free Software politics.\n\nIt was developed that way in the philosopher talk.\n\n> how many people run a win95 app on XP because they want a win 95 app? \nMany do. People do not like to change their habbits. If they started with a windows 95 app, they will continue to use it until a very compelling reason prevents it. Available upgrades are not one of those very compelling reasons.\n\n> how many would take the XP version over the 95 version given feature parity?\n\nWe were talking about the burden of maintaining KDE on windows XP vs on the future version of windows. So, if KDE is available in a more advanced fashion that windows XP, they will use it, else, they will use the windows XP version. But not that is even granted. As I sais, many simple users prefer to keep their habbits than upgrading.\n\n\n> i know the only reason we keep certain win98 apps around here is because we have no choice\n\nSo ? What's wrong with that ? The question was whether we should make these windows 98 applications available or not. I am still not convinced we should not.\n\nBesides, if I remember correctly, windows XP is only 30% of the running windows version at the moment and windows 98 is still about 30% of the existing windows. So KDE on windows would probably last a lot longer than any version of Qt.\n\n> you don't have to make an app un-runnable by sacrificing backwards \n> compatibility to effectively break the application or make it irrelevant within it's given niche.\n\nCould you be a bit more specific ? If I remember correctly Dr Dos was made uncompatible. I am not familiar with the others.\n\nIf you are trying to say that Microsoft might kill our product by cloning it, like they did for netscape or real player, I don't think it is a good argument: windows provides most of what KDE does, so people are not going to use KDE just for some convenience around the corner. There ought to be for them a better reason, like \"I like kmail better\", or \"it is just fun to use\" than \"I use it because Microsoft does not provide equivalent features app but if they did, I would switch immediately back to microsoft\".\n\n\n> and it certainly isn't through defeatist attitudes that boil down to \"why bother because you can't change anything.\"\n\nWell, I am glad to see that you are more optmistic than me. I have witnessed than free software people hold on to their opinions beyound reason. We do not act much out of reason and it has a lot of good consequences and a few bad.\n\nI also think that opinion do form quickly, so the lengthy the debate, the less convincing it is [sorty for my poor english]. If you want your message to get through, shorter sentences with clear points should help.\n\nSo correct me if I am wrong, your point is: KDE on windows is bad because:\n- it will kill most of the interest of switching to linux\n- it will run with inferior quality than on linux\n- it will be a burden to maintain\n- microsoft might kill it anytime\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-09
    body: "\"so, to repeat one more time so this is painfully clear: this is not about Free Software politics.\"\n\nWell, it isn't for _you_. When writing that article, as has been pointed out, I did want to approach it from that point of view. Of course that's not to say that many of the arguments rely on that assumption... as you've gone to some length to say, you advocate some of my arguments _without_ buying into the free software issues.\n\nI just thought I'd clear that up :-)"
    author: "Tom"
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-07
    body: "Next time, we'll be sure to try and book some more well known Greek philosophers."
    author: "Anonymous"
  - subject: "Re: Bollocks 'R' Us"
    date: 2005-01-07
    body: "What exactly is your point? Whatever opinion you might hold on porting KDE to Windows, the article is entertaining and well-written.\n\nDoes your anti-intellectualism further anyone's cause?"
    author: "Anonymous"
  - subject: "a question"
    date: 2005-01-06
    body: "Porting KDE to Windows would require porting of Qt as well, wouldn't it? If it would, I am definitely against it -- for the very practical reason that if Windows gets a Free version of Qt, and Trolltech goes out of business because of it, we won't have anyone to develop the toolkit for us. That would suck."
    author: "Illissius"
  - subject: "Re: a question"
    date: 2005-01-07
    body: "I don't think it would be such a big deal if the windows libs were released under gpl, qt doesn't make money with free project buying licences to port their apps to windows ( i don't even know if it is possible considering the gpl )\n\nA lgpl version would be more tragic"
    author: "polux"
  - subject: "Re: a question"
    date: 2005-01-07
    body: "I am assuming there is no GPL version of Win32 Qt because of a lesson learned by Trolltech when they released QtNC.  Note that there was only really one public release of QtNC, and that was in version 2.x.  I am assuming that the bean counters saw something they didn't like, and that is why there continues the current situation on Win32.\n\nNow we can argue that windows developers don't \"get\" OSS, and we can argue that \"Trolltech's business model is all wrong\" but at the end of the day the fact remains that it is probably not financially prudent for them to have a free as in beer version on Win32.  Welcome to what we call capitalism kids."
    author: "Ian Reinhart Geiser "
  - subject: "Re: a question"
    date: 2005-01-07
    body: ">Now we can argue that windows developers don't \"get\" OSS\n\nI think this would be wrong because:\n1. if they use GPL Qt for  internal development they do nothing wrong if they don't release there code to the public.\n2. Maybe someone will abuse the GPL Qt version but this can also happen on the GNU, BSD or Unix platform because there are also a lot of proprietary software developers. But i think this abuse could be recognized easily and Trolltech could easy \"fight\" against it.\n\nI think the real problem is like Carewolf has posted in this thread:\n\n\"The problem is that Trolltech makes most of their money from companies who use Qt for internal applications, not applications that are released to end-users.\"\n\nBut if Trolltech don't like that people can do what they wan't on their computer than you could argue that Trolltech don't \"get\" Free Software.\n\nBut i think the overall question is:\nWhy Trolltech makes most of their money from companies who use Qt for internal applications and don't get many customers who develop software for their customers? Why Trolltech can't make their Toolkit interesting for more software companies who develop software for other people and not for their own use?\n\nIf they would have more customers who develop software for their customers, than there would be no problem to release a GPL Qt for windows. Because this would nothing change for their customers.\n\nAnother question is:\nWhat will happen if the Qt-win32 port become successfully?\nI think it's really risky to base your business on the idea of hiding your software from a special group of people and release it as Free Software to another group of people, because everyone is free to port this software to the platform of the other users.\nSo i think the best way, also for Trolltech, would be if they try to get a \"better\" customer base so that they can't get in trouble if there is a GPL Qt for windows. Because it's only a question of time until such a project will succeed."
    author: "pinky"
  - subject: "Re: a question"
    date: 2005-01-07
    body: ">But i think the overall question is:\n\n>Why Trolltech makes most of their money from companies who use Qt for >internal applications and don't get many customers who develop software for >their customers? Why Trolltech can't make their Toolkit interesting for more >software companies who develop software for other people and not for their >own use?\n\nBecause it is almost impossible (READ REALLY F*CKING HARD AND WILL WASTE YEARS OF YOUR DEVELOPERS LIVES) to ship a C++ application external to your company.  Now there are ways around it (ie ship all your shared libs, static linkage, build for every possible permutation, etc...)  In short virgina, yer screwed.\n\nNow if you are doing everything internal, *bing* you control all your platforms and all your libs.  Distribution is a cakewalk and you go on with your day.\n\nWelcome to life with C++...  (FYI, Java is only slightly better, but then you are stuck with swing in all its glory)\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: a question"
    date: 2005-01-07
    body: "> Because it is almost impossible (READ REALLY F*CKING HARD AND WILL WASTE YEARS OF YOUR DEVELOPERS LIVES) \n> to ship a C++ application external to your company.\n\nYou mean cross-platform apps, right? Because we don't have any problems to ship our C++ Windows application to our customers.\n"
    author: "Christian Loose"
  - subject: "Re: a question"
    date: 2005-01-07
    body: "> You mean cross-platform apps, right? Because we don't have any problems to\n> ship our C++ Windows application to our customers.\n\nYes, if you are on a single platform you are okay, or if you use VC6.  The pain happens if you want to support XP and 2k with VC7 and up....  At least Qt links to system level stuff so if you write a Qt app with VC6 you don't need to ship anything other than Qt's dll... but from there it gets worse. \n\n/me mumbles something about msvcrt-flavor-of-the-week.dll.... and wanders off."
    author: "Ian Reinhart Geiser"
  - subject: "Re: a question"
    date: 2005-01-07
    body: ">Because it is almost impossible to ship a C++ application external to your company. Now there are ways around it (ie ship all your shared libs, static linkage, build for every possible permutation, etc...) In short virgina, yer screwed.\n\nBut which language were used for all the apps in the windows world? I think mostly C++ (maybe VC++). And as far as i remember, if i had to install a program on windows it's normal that there will be installed more than one file and a lot of these files are dlls. So this is not abnormal.\nSo the question keeps valid: Why they use VC++ or simular programming systems and not Qt? Why Trolltech can't get these customers?"
    author: "pinky"
  - subject: "Re: a question"
    date: 2005-01-07
    body: "Most software last I heard (about 95%) is custom developed for internal use only. From my personal experience I would put it at least at 80%. These are programs that companies pay to have custom developed only for them that are never used outside that company.\n\nI think that is what trolltech worries about. If there is a qt for windows that is gpl then you can safetly use it with the app gpled and nobody will ever see it outside that company and thus you comply with the gpl and don't pay trolltech anything. The problem I see with that is that if you spend a large ammount of money building a custom app you would want support for the libraries in it and they would want to be able to call trolltech anyways for help.\n\nHaving your developers spend hours or day figuring out something that a 10 minutes phone call can solve is just not very good from a time or money standpoint however I can see why trolltech is doing what it is.\n\nI also don't know how long it might be if ever before someone makes a gpl qt for windows that is the qt/x11 version ported over that still needs to use an x  server. I suspect that it has very few contributors and it requires a lot of work and qt is a moving target. For an example of thing kind of python look at cpython (the default thing that most people just call python) and jython the version of python that runs on the java vm. The jython and cpython version numbers mean the same thing so jython 2.1 is equiv to cpython 2.1 . The problem is that jython is at 2.1 and python is at 2.4 . Jython is pretty much about 2 years behind python for development and that is just a port also. The problem is that not enough people really care about it. I am sure we could find other examples also, just because something can be ported doesn't mean it will be and doesn't mean it will work very well simply because not enough people care about it."
    author: "Kosh"
  - subject: "Re: a question"
    date: 2005-01-07
    body: "Have you seen this: http://kde-cygwin.sourceforge.net ?\nand in particular, this page: http://kde-cygwin.sourceforge.net/qt3-win32 ?"
    author: "Colin"
  - subject: "who cares"
    date: 2005-01-06
    body: "if trolltech make a GPL Qt for windows I don't think its biggest clients such as photoshop and opera will use it, they will still pay for the non-free version because they won't release their software under the GPL right? So I guess GPLing Qt on windows shouldn't be that big loss for Qt but I may be wrong :) "
    author: "Joe"
  - subject: "Re: who cares"
    date: 2005-01-06
    body: "The problem is that Trolltech makes most of their money from companies who use Qt for internal applications, not applications that are released to end-users. In this case the companies can use a GPL product and still keep the source-code to themselves because they are the only end-users."
    author: "Carewolf"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "are yo sure about that?\nplus I guess most country other than US don't pay licences for internal application. And there are lots of applications that redistributes according to the trolletec site"
    author: "Joe"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: ">plus I guess most country other than US don't pay licences for internal application.\n\nDoes this mean also private persons don't have to buy a license for proprietary software if they use it \"internal\"?\nWhy all the people buy windows, MSOffice, .... licenses?\nPlease show me such a country, i don't think any country has such a low copyright-law."
    author: "pinky"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "No. It's a GPL matter.\nIf x develops software based on GPL software (Like Unix version of Qt, or its win32 port), and then distributes it, then x's app must be released as GPL.\nIf x is not a guy but an organization, if the app is deployed along the org is ok, is not considered as a distribution. I hear RMS saying this myself.\nThis is because GPL the x's app is theoretically GPL because it is a derived work, but as long as it's not distributed outside, it's ok, GPL is not about \" use\", and doesn't enforce distribution as Bill and SteveB want to make people believe.\n\nOn the other hand, proprietary EULAs usually say you must be a licensee in order to use their development tools, then the EULA can require payment per runtime copy or not, in the first case you always need to pay per seat, distributing it outside or not.\n\nWin32 Qt is in the second case, you pay $1500 per developer, but then you have no limits AFAIK.\n\nOne more thing: LGPL is less restrictive and allow to link LGPL libs against closed apps, in order to allow GNU platform becoming more popular. That's why Unix Qt is released under GPL, as they want to get some cash first, and be popular later.\n\n"
    author: "Shulai"
  - subject: "Re: who cares"
    date: 2005-01-08
    body: "I might be splitting hairs here...\n\n> If x develops software based on GPL software (Like Unix version of Qt,\n> or its win32 port), and then distributes it,\n> then x's app must be released as GPL.\n\nWell, yes. But this means that you only can keep your app un-GPL'ed if only the original developer uses it. As soon as you gove it to somebody else, (say, your co-worker, or the company that paid your for their in-house app), that's distributing, and hence it must be distributed in compliance with the GPL.\n\nNow, the point is this: The GPL only requires you to make the source code accessible to people who receive the software. That is: If A gives an app to B, A has to give B the source code (either ship it along, or upon request). But C, who never receives the software, has no right to its source code, either. So, I can develop an in-house application - even for a customer, not for myself - based on GPL software an be all fine and dandy if only I hand over the source code along whith the app, too, which is pretty standard fare in these circles. I don't have to make the source code - or the app - available to the world."
    author: "Annno v. Heimburg"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "forget the copyright-law, most countries don't respect them, that's what I was trying to say. The only license the smb I work for is the windows one and they got it with the coputer so. and this is in france. so I suppose other countries don't even pay the windows license."
    author: "Joe"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "Wrong, you can use a free software tool to create proprietary apps."
    author: "Brush up on your GPL Knowledge"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "With a GPL Qt you can't develop proprietary Software, because if you derive from a Qt class in your application you create a derivate work of the original code and than it has to be GPL licensed, too."
    author: "pinky"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "Only if you are directly modifying the source of the original project. Since you aren't, and especially with PyQT are only classing from the already compiled binary... you can make a proprietary piece of software."
    author: "Aaron Krill"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "Sorry, you are completely wrong and pinky is correct. You don't even need to derive a class - make any call to the GPL'ed API and your application must have a GPL compatible license. It doesn't make any difference whether it's in C++ or Python."
    author: "Phil Thompson"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "So, following your reasoning, there can be no commercial Linux applications (since they all make calls to the GPL'ed API that is the kernel)? That sounds like nonsence to me."
    author: "Andre Somers"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "No applications makes library calls to the linux-kernel. They use the hardware system call interface."
    author: "Carewolf"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "And Linus has actually explicit stated that the GPL does not apply to the use of Linux public API."
    author: "Morty"
  - subject: "Re: who cares"
    date: 2005-01-07
    body: "As others have pointed out, the kernel API is something different.\n\nWhy do you think there is the LGPL and the GPL? Why do you think Trolltech (and I) chose the GPL rather than the LGPL? Answer: to force people to buy a commercial version if they wanted to release non-GPL applications."
    author: "Phil Thompson"
  - subject: "Re: who cares"
    date: 2005-01-08
    body: "I'm not so sure. If I for example would develop a application around a apstract warpper and than develop a wrapper implementation in qt, windows etc. as library I must GPL the wrapper implementation(not the wrapper) but not the application which is only calling the wrapper. Its because the wrapper is not derived from qt. Its like the nvidia module in the kernel. And there is no exception for the GPL in the kernel for binary modules. I'm not a lawyer so I'm not 100% sure but it would be very hard to force the people to GPL the application. Maybe the user which is using the qt wrapper implemention is breaking the law but not the developer."
    author: "Marco Bubke"
  - subject: "Re: who cares"
    date: 2005-01-08
    body: "AFAIK its not about calling, its about linking and deriving. Otherwise you could not use the linux kernel. "
    author: "Marco Bubke"
  - subject: "Ok"
    date: 2005-01-07
    body: "KDE 4 win is great ;-))"
    author: "X0R"
  - subject: "Voting"
    date: 2005-01-07
    body: "Interesting voting:\n\nhttp://kde-apps.org/poll/index.php?poll=113"
    author: "Andy"
  - subject: "Konquerer development"
    date: 2005-01-08
    body: "Can you imagine the number of developpers a windows konquerer or koffice could attract? Look out, openoffice and mozilla atract all development because they are the biggest project. They are big because they are also on windows. "
    author: "Geert"
  - subject: "Re: Konquerer development"
    date: 2005-01-08
    body: "Did you think that KDE on Windows may be more popular than KDE on Linux? :-/"
    author: "mkkot"
  - subject: "Philosophy and code"
    date: 2005-01-09
    body: "Everybody here is talking about the user base, philosophy, qt license (the n+1 time) and all that. But...  is this tecnically aceptable?. Qt is OS-agnostic, but KDE is hardcore unix-centric. The real question here are: can you make a konqueror-setup.exe to install konqueror in a easy way? how dependant is KDE libs to dlopen, libc, glibc and other unix-only stuff? is the code going to grow to support special window cases? some (more) abstraction layers are going to hurt performance?\n\nMay be the port can take 50 (mythicals :) man months to get some hundred windows users, some dozens devs. Then the effort is better used bugs.kde.org, or polishing some existing apps (koffice or kdevelop some of them), or making some news.\n\nResuming:\n\n<b>is good from the code level?</b>"
    author: "peroxid"
  - subject: "a kde port to windows IS being worked on..."
    date: 2005-01-09
    body: "on http://kde-cygwin.sourceforge.net/\n\nthey already developed a 100% gpl port of qt3.3 for windows.\n\nas far as i am concerned trolltech can go bankrupt tomorrow, they have signed an agreement with kde, that in the event of bankruptcy or change of ownership of trolltech or major changes in development plans, qt will be released to the public under a bsd-style-license. \nwe dont need some company to develop a toolkit for us, we are the people, we are the community and we should always strive for independance (especially of non-transparent business plans of profit-orientated corporations).\nthis developement is happening, with a free qt port for windows and kde replacements for the qt-tools (kdevdesigner instead of qt-designer aso) most things are moving away from trolltech."
    author: "Hannes Hauswedell"
  - subject: "Re: a kde port to windows IS being worked on..."
    date: 2005-01-09
    body: "> hey have signed an agreement with kde, that in the event of bankruptcy or change of ownership of trolltech or major changes in development plans, qt will be released to the public under a bsd-style-license. \n\nThat's wrong. The sole criteria are continued releases of an Open Source edition."
    author: "Anonymous"
  - subject: "Re: a kde port to windows IS being worked on..."
    date: 2005-01-10
    body: "i am sorry, thats what i meant. just wanted to point out that bankruptcy or change of ownership are explicitely named as possible reasons for an end of free qt releases."
    author: "Hannes Hauswedell"
  - subject: "Re: a kde port to windows IS being worked on..."
    date: 2005-01-09
    body: "> we dont need some company to develop a toolkit for us, we are the people, we are the community\n\nCare to help me to identify you as part of the KDE community?\n\n> kde replacements for the qt-tools (kdevdesigner instead of qt-designer aso\n\nYou are aware that kdevdesigner is basically Qt Designer?"
    author: "Anonymous"
  - subject: "Re: a kde port to windows IS being worked on..."
    date: 2005-01-10
    body: ">  Care to help me to identify you as part of the KDE community?\n\nactually i consider anyone working with or on kde part of the community. i dont know what your elite criteria are or if i fit them... i do code opensource programs (in qt/for kde?), of which some will hopefully be useful to the community. talking about identity, you didnt even post your name....\n\n> You are aware that kdevdesigner is basically Qt Designer?\n\nwell right now they are pretty much the same thing thats right, but there is one difference: kdevdesigner is not maintained by qt, but by community developers, which is what all of my above post was about, if you read it..."
    author: "Hannes Hauswedell"
  - subject: "Guys------Come on!!!"
    date: 2005-01-10
    body: "I like playing with Linux iso's when I don't have anything else to do. I've tried a fair number of them. Suse is my favorite flavor, I actually bought 9.0. But the answer to your question is in the fact that you are even debating it! Most of us low buget computer wannabees will try it out, find out it doesn't measure up in response on basic things like media players and browser plugins and just get fed up and format the disk and reinstall \"Uncle Bill's Virus\". You could soooo increase the audiance by porting to windows, damn the developer's tools. I love KDE but I am not yet ready to give up on the krisp response from my 98se running on my old klunker. Please guys-----just do it already! It's going to happen anyway."
    author: "Tom Diggs"
  - subject: "The War of 2006"
    date: 2005-01-11
    body: "The Desktop war is about to begin on the release of LongHorn. You have a chance to invade and yet you guys ponder \"should we?\" The quicker we get KDE   on windows the quicker Bill Gates has to make his move and the quicker he makes his move the more flaws will come. We need to slow down Microsoft. I had weird nightmares with Bill Gates taking over the net using broadcasting licenses and owning the wires to your house. Once you understand the Brotherhood of Bill and E.U. then you would want to move KDE on Windows. Use KDE as a stepping stone. Develop killer p2p apps that are extremely easy and in a couple of years the KDE team should pull the plug on all Microsoft users and force them to switch over to linux in order to keep using upto date KDE. Do on to others as they do on to you. The whole SCO was Bill's idea!!! Now it's payback. Linux need's to play nice with Microsoft. Once the numbers start showing up (KDE users or GNOME users, LINUX,)make a Microsoft move for payback. Once mom and dad understand enough to run linux then the newborns will learn as second nature. I'm a newbie linux user. I've used suse 7.1 on and off and bought suse 8.1 professional had some troubles went back to win2000 then to Xp, now I'm ripping on suse 9.1 and upgrading to 9.2. I deleted my Xp partition and didn't backup anything. Damn Bill nightmares are creeping me out!!! I would have used linux since 98 if I was taught some basics. I've always liked SuSE and now with Novell in the picture there's nothing more then payback. hahaha! IBM and SuSE aren't they partners. oh ahh cuz IBM wanted payback. 2008 will be the year of payback on Microsoft, but mostly likely to late cuz the E.U. 2006 rule. Broadcasting laws, damn.........!!!!!!! "
    author: "nano"
  - subject: "Re: The War of 2006"
    date: 2005-01-30
    body: "\"Develop killer p2p apps that are extremely easy and in a couple of years the KDE team should pull the plug on all Microsoft users and force them to switch over to linux in order to keep using upto date KDE.\"\n\nYou forgotten, KDE is on GPL license. Anyone can give Windows new versions (or even better).\n\n-- \nmkkot"
    author: "mkkot"
---
<a href="http://newsforge.com/">NewsForge</a> is running a <a href="http://os.newsforge.com/article.pl?sid=04/12/23/1844249">Platonic dialog</a> considering the eternal question: "Should KDE be ported to Windows?".  The voices of Socrates, Glaucon and Thrasymachus present the various arguments for and against the issue, making for an easy to understand presentation of the various aspects of this classic debate that will likely pop up again with KDE 4.






<!--break-->
