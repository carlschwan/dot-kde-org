---
title: "Tenor: The Contextual Linkage Engine"
date:    2005-04-14
authors:
  - "dmolkentin"
slug:    tenor-contextual-linkage-engine
comments:
  - subject: "Some additional remarks"
    date: 2005-04-14
    body: "--> The article isn't very specific on Tenor's technology. It\n  does however hint at the main conceptual idea that drives\n  its development. \n\n--> The main purpose of the articles is to provide background \n  on *why Tenor has been started* at all, and why I, as a *user* \n  (not a C++ developer) see a very convincing use case for it. \n\n--> Tenor is now a joint project of Scott and Aaron J. Seigo, \n  it seems to me. (I neglected that aspect a bit in my full \n  article).\n\n"
    author: "Kurt Pfeifle"
  - subject: "Re: Some additional remarks"
    date: 2005-04-14
    body: "http://dot.kde.org/1109163846/\nScott Wheeler on desktop search"
    author: "Andr\u00e9"
  - subject: "Re: Framework re-use"
    date: 2005-06-14
    body: "\"With the content browser, you will be able to find and use information spread out across your system using the same semantics you use when thinking about or describing that information in day-to-day life. It will also allow you to find and build relationships within your information set.\"\n\nThis is the only info I could find in the article about what specifically Tenor aims at is.  It sounds much like GNOME Storage.  Does Tenor have a different goals?  If not, how will the actual frameworks differ?  Could some of the Storage effort be reused?\n\nCould the framework be (made) useful outside KDE (say on GNOME) and published on freedesktop.org?"
    author: "Phil Ganchev"
  - subject: "Beagle?"
    date: 2005-04-14
    body: "Tenor reminds me alot of gnome's beagle.\n\nCan't wait to see how the KDE team will implement this one!  It will surely be killer."
    author: "standsolid"
  - subject: "Re: Beagle?"
    date: 2005-04-14
    body: "AFAIU, Tenor will be able to find an image if I ask for 'an image that I got last summer as an email attachement by John', that's what makes Tenor a 'Context Link Engine' - in that case, with Tenor being part of the KDE framework, you'll receive the email with KMail and save the attached image. Tenor will register the save and try to gather the context, in that specific case it would associate the image to the email it came from, thereby also create a relation to the author of the email, the email's subject, date etc. It would also be able to extend the search following logical links, like 'all emails by John', 'all emails sent to John', 'all emails sent/ received that day' or possible hyperlinks inside that emails. Extending this to KAdressbook's geographic data, I could use the image to search for 'a website which link someone from France sent me via Jabber/ Kopete the same day I received this otherwise completely unrelated image'.\n\nBeagle is just a 'mere' desktop search with support for metadata, similar to Apple's Spotlight - but that will only be a subset of what Tenor will do...\n"
    author: "Willie Sippel"
  - subject: "Re: Beagle?"
    date: 2005-04-14
    body: "your example is a good one, but tenor goes even beyond that. i just blogged about a couple of the more non-obvious possibilities tenor opens up. =)"
    author: "Aaron J. Seigo"
  - subject: "Dashboard"
    date: 2005-08-23
    body: "How about a link to the blog entry, Aaron?  :-)\n\nI see only a very short mention of Kat on aseigo.blogspot, you were looking through the code and suggesting that clucene be used on 12 August.\n\nhttp://aseigo.blogspot.com/2005/08/akademy-callinng-all-ohioans-kat.html\n\nGnome Dashboard was very exciting a couple of years ago but it hasn't gone anywhere since (seems interest has moved over to Beagle).  From the few news items I've read, Kat/Tenor sounds like a great basis for such things and more in KDE.\n\nhttp://www.nat.org/dashboard/"
    author: "stfn"
  - subject: "Re: Beagle?"
    date: 2005-11-15
    body: "And what if I receive my email through Thunderbird? Or create a letter in OpenOffice? Will that be taken into account by Tenor as well? Please make that possible!\n\nTenor sounds like a great idea, but I ask you, don't make it too KDE-centric. It's poor enough that Gnome and KDE have developed two VFSs, and neither of it works at a layer that makes it accessible in konsole/terminal/xterm ;) No need for two search engines which are limited to their specific desktop.\n\nJust expressed my hopes and wishes,\nOliver\n"
    author: "Oliver Gerlich"
  - subject: "Spotlight can do all those things"
    date: 2006-02-28
    body: "Spotlight can do all the things you've listed."
    author: "Samsara"
  - subject: "ETA"
    date: 2005-04-14
    body: "It looks rather impressive. When will we see the first applications to make use of Tenor technology?\n\nRegards,\nJames"
    author: "James Nash"
  - subject: "Re: ETA"
    date: 2005-04-14
    body: "KDE 4, likely not before. About 1++ year."
    author: "Kurt Pfeifle"
  - subject: "Re: ETA"
    date: 2005-04-14
    body: "in a production quality, shipping release, yes.\n\nwe'll have demos and beta releases well before that, though =)"
    author: "Aaron J. Seigo"
  - subject: "Moving the data?"
    date: 2005-04-14
    body: "How will Tenor deal with parts or all of my data being moved to a different machine, or being stored away on a CD-ROM?\n\nMost people tend to use more than one machine, archive data and to buy new computers from time to time. It would be a shame to lose context data when doing so. \n\nOn the other hand, you would want to be able to drop Tenor data on your local machine and/or when giving data to others, according to your wishes. Otherwise Tenor would be a Big Brother of sorts."
    author: "Martin"
  - subject: "Re: Moving the data?"
    date: 2005-04-14
    body: "actually, Tenor is Swahili for \"Big Brother\". it will phone home to Scott if you live in Europe or to me if you are in North America (we still need Asian, African, South American and Oceania conspirators; applicants welcome) with all your linking secrets. All Your Tenor Are Belong To Us.\n\nok, seriously though. yes, there are privacy concerns and we've been talking about them at length. it's a perfectly tamable problem, but we won't see implementations of those solutions until we have the basics down first.\n\nas for filtering out related Tenor subgraphs, we should be able to get that almost \"for free\" since given any starting point, it's possible to create a subgraph specific to that node with a given \"radius\" or specificity.\n\nmoving Tenor data between machines won't be hard; merging two meshes effectively will be an interesting task, however, as will defining boundaries of visibility.\n\nremovable media will also be a bit tricky since there is greater ambiguity in addressing (\"ok, but the foobar.kwd on which CD?\"). it's a problem we haven't yet gotten to solving, yet. i don't see it as insurmountable, we're just not there yet in the project is all =) it's also a problem common to all such indexing services."
    author: "Aaron J. Seigo"
  - subject: "Tenor Conspirator Volunteer position in Cupertino"
    date: 2005-04-14
    body: "I volunteer for the position in of Tenor Conspirator in Cupertino.\n\n\nAlso, if not taken, I'd like to cover the Redmond position too, as well as Armdonk and Mountain View.\n\n\nIf you accept me, I'll start to submit a weekly \"Tenor findings CVS\" to this list. I am quite sure that the Microsoft/Redmond blokes will will have a close look at it. Will be interesting to see their \"mesh\" workss. The same for the IBM/Armdonk or Google/Mountain View.\n\n\nAll these big players are known for lifting off OSS technology for their won good.... So let's watch them while they are ag work."
    author: "konqui-fan"
  - subject: "Re: Moving the data?"
    date: 2005-04-14
    body: "Indeed, amaroK doesn't have a solution to the removable media problem, despite it being often requested."
    author: "Ian Monroe"
  - subject: "Re: Moving the data?"
    date: 2005-04-22
    body: "WRT the removable media issue: This question just reminded me of how well the good old Commodore Amiga handled removable media (with floppies though). You could have a desktop icon link to any file or application, and if you clicked on it the machine asked you to enter the disk DISKNAME into any drive.\n\nThat's something PCs still don't do half as well.\n"
    author: "Joachim Werner"
  - subject: "Re: Moving the data?"
    date: 2005-04-14
    body: "Apologies for getting a bit technical here, but I'm bad at explaining this stuff in other terms.  :-)\n\n> How will Tenor deal with parts or all of my data being moved to a different \n> machine, or being stored away on a CD-ROM?\n\nThe issue of how to export subgraphs (the base of the Tenor structure is a classical \"graph\" in the computer science sense) is one that I've been thinking about a lot recently.  It's still a few steps away, so right now it's just something that I kick around on long train rides and draw pictures in my notepad about, but it will be doable.\n\nActually exporting subgraphs isn't that hard -- what's hard is connecting that to the security model and being able to define or decide what is exported, how and to who.  I've been putting a lot of thought into that, but if I went into it here it'd just be a lot of fuzzy technical blabbing that I'd probably change anyway, so watch this space.  There will be more articles in the future that will cover the details as they become concrete."
    author: "Scott Wheeler"
  - subject: "Re: ETA"
    date: 2005-04-15
    body: "Wouldn't \"1++ year\" be equal to two years?"
    author: "ac"
  - subject: "Re: ETA"
    date: 2005-04-16
    body: "Actually, it evaluates to 1 ;-)"
    author: "kundor"
  - subject: "Re: ETA"
    date: 2005-04-16
    body: "With the promise that next time you ask the answer will be two years >:-)\n\n"
    author: "cm"
  - subject: "It's not a coincidence"
    date: 2005-04-14
    body: "Great stuff. It strikes me that KDE is in the almost unique position of having the technology, the development ethic and the organizational structure to accomplish such a turn-around, thanks to the tightness and code-reuse within the KDE platform. Which other player in the field could roll out such heavy-weight technology in a reasonably complete set of desktop applications at the same time - the enabling condition for being able to take advantage of contexts? Apple comes to mind, arguably, but the list pretty much ends there."
    author: "Eike Hein"
  - subject: "Re: It's not a coincidence"
    date: 2005-04-14
    body: "The KDE team did spend a lot of time in early versions getting the framework right with integration technology such as dcop, kio and kparts. \n\nI guess they (and we users coincidentally) are now set up to reap the benefits of this extra effort.\n\nL."
    author: "ltmon"
  - subject: "Re: It's not a coincidence"
    date: 2005-04-14
    body: "From my blog last week (http://www.kdedevelopers.org/node/view/953):\n\n\"KDE has the tightest coupling of application frameworks and applications of any desktop in existence.  We can't throw as much money or as many PhD's at a problem as proprietary desktops, but we can push new ideas out in a fraction of the time that they can.\""
    author: "Scott Wheeler"
  - subject: "Re: It's not a coincidence"
    date: 2005-04-19
    body: "OS X is pretty close.  IMHO Apple is starting with great usability and heading towards rich frameworks, while KDE starts with great frameworks and is heading towards usability.\n\nStill, while Apple spends developer time on interfacing with users and swallowing their ego when it comes to UI development and testing, there's still too much of the \"my way is the right way so frack off\" in the KDE world.\n\nWhile KDE folk can (and IMHO) should steal all the nicest bits of OS X UI, I'm sure there's folks at Apple who will look at KDE's cool tech with an ugly face and they'll steal those ideas and plop a great UI on them."
    author: "Anonymous"
  - subject: "Re: It's not a coincidence"
    date: 2005-04-23
    body: "I just got myself a Mac Mini - mainly because I needed a multimedia capable machine that still fits around my table, and because I wanted to make music. So here's the Linux user's view on OS X.\n\nYes, Apple did a lot for usability. But some of the things really get on your nerves in OS X, and I'd rather say the \"my way or the highway\" philosophy applies for OS X far more than for KDE. In KDE, there are usually several ways to achieve some task. In OS X, there is usually just one way. There are far fewer buttons and switches in applications, and this makes it easy for a beginner to get aquainted with the app, but it really gets on my nerves when I e.g. can only have a button for left OR right rotate in iPhoto's toolbar, not both.\nAnd Apple Mail starts downloading (caching) your whole IMAP account by default as soon as you configure it (about 800MB in my case), and cannot search in mails any more once you disable this. (Whatever is the IMAP SEARCH command for?)\n\nSo, I think somewhere there should be a middle ground. Apple has some *really* cool applications and although I miss some features in the iLife apps, they *are* good software. but the default user interface is just to ... barren ... feature wise. It's overloaded with special FX to compensate for that, though.\n\nGerman speaking users, please visit <a href='http://news.jensbenecke.de/node/851'>my blog</a> for a more in depth review. :-)\n\nJens"
    author: "Jens"
  - subject: "Tenor"
    date: 2005-04-14
    body: "Tenor sounds cool, but these are hard problems.  Is there actual code in a cvs somewhere to check out and take a look?"
    author: "Spy Hunter"
  - subject: "Re: Tenor"
    date: 2005-04-14
    body: "there are earlier drafts available that reflect sketches of various states of design.\n\nthe most recent version was in CVS, but is in the process of being moved. don't worry, as we have things for people to play with we'll let people know =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Tenor"
    date: 2005-04-14
    body: "What number of people does \"we\" include? Looking at the length of the authors list of beagle it will be surely hard to finish an even more ambitious project like tenor in time for KDE 4. What part will developers of the existing applications play? What can they do or at least think about today to prepare for tenor?"
    author: "uddw"
  - subject: "Re: Tenor"
    date: 2005-04-15
    body: "i've got a whole tribe of oompa loompas in the basement working on this. scott's working on the import permits for his own oompa loompa tribe. (Germany tends to be a bit more of a stickler on these things than Canada.)\n\nseriously though, there's a rather small number of us. and yes, the application developers will end up adding the vast bulk of value to it by building services on top of it.\n\nthings like the Content Manager will be a generic user interface to Tenor's stored information, and there will be many specific ones (e.g. annotations in pdfs and elsewhere).\n\nwe just have to have make the engine work, others will build upon that to expose the functionality. sort of like most of the rest of KDE's library-base technologies. =)"
    author: "Aaron J. Seigo"
  - subject: "Application support..."
    date: 2005-04-14
    body: "Soo.  Is this something that applications would need to support specifically?  Something like dashboard where applications supply \"hints\" as to the current context?  \n\nIt sounds so interesting, partly because, when someone talks about Beagle, or Spotlight, I immediately have some idea how one could implement such a system.  With Tenor, I can't even begin to think how it might work, code-wise. :)"
    author: "Leo S"
  - subject: "Re: Application support..."
    date: 2005-04-14
    body: ">Is this something that applications would need to support specifically?\nNo, this is KDE so it's customary to let the framework handle such things:-) Something like the way KIO-Slaves work today, they are available to all KDE applications without the need for any specific handling in the applications."
    author: "Morty"
  - subject: "Re: Application support..."
    date: 2005-04-14
    body: "> With Tenor, I can't even begin to think how it might work, code-wise.\n\nwe'll have APIs and examples for people to play with shortly.\n\nthe basic essence is that you take a piece of information, decompose it if necessary in an application-specific manner, and then feed it into Tenor along with a description of what \"gos together\". Tenor then builds additional relationships, if it can, and slots it into the larger mesh and allows this to be searched for and navigated through either independently or as part of a larger body.\n\nit's quite a bit easier to show with examples, which Scott and I have put several of together. once we're happy with the way the APIs look and the storage side of it is working, we'll start releasing developer previews. it'll become pretty obvious at that point =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Application support..."
    date: 2005-04-14
    body: "The more apps use tenor the more usefull it will be.\nSo, will it be kde-only or will other apps (Qt, gtk, any other) be able to feed data into it ?\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Application support..."
    date: 2005-04-15
    body: "there is no reason why any application couldn't fee data into it. there will likely be a Qt dependency (though not a GUI one) in the core Tenor engine, but i don't see that being a problem. i expect an IPC bridge to emerge, for instance, and if other app developers have a problem communicating with a non-gui but Qt using app then they may want to check their priorities =)  personally i see this as similar to the glib issue =)\n\nthe file indexer will not be tied into the Tenor engine, but use the API like any other application. so there could be multiple indexers written for different purposes, one of which could use KFMI (or something faster and with FTI-appropriate capabilities, preferably)\n\nand yes, it should be possible to write FireFox extensions and OOo plugins to tap into tenor, for instance.\n\nthere will also be a GUI library for KDE apps to use (stock interface elements and probably various KDE specific helper goo-gads) as well, but that should be a separate library from Tenor itself and so be of no concern to non-KDE apps."
    author: "Aaron J. Seigo"
  - subject: "Tenor KIO-slave?"
    date: 2005-04-14
    body: "Maybe I missed this part in the article, but... How will Tenor be implemented? Will there be a separate Tenor-app that you use, or will it end up being a KIO-slave? I could see benefits in having \"tenor://seigo kde-usability png\" in file-dialog for example. The example would find all png's that were sent to kde-usability by Aaron J. Seigo. yes, the example is pretty limited, but gets the idea across.\n\nOf course, there is a problem that KDE has even today: how do we tell the users about all the great technology KDE has? I still run in to KDE-users who do not know about audiocd:// or fish://!"
    author: "Janne"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "That's kind of the wrong way of looking at it -- \"Tenor\" isn't something that users will use directly; it's a framework, not a tool or a specific interface component.\n\nThe way that it will show up to users is simply applications that work \"smarter\", search that works better, desktop browsing (in applications or the file browser) that knows about relationships.\n\nSo, it's not a tool and it's not storage abstraction; it's a mechanism to make applications able to work with context.\n\nIn this sense -- libkio is something that makes it possible to work with various sorts of input and output in KDE.  Tenor is something that makes it possible to work with context.\n\nThere probably will be some specific new applications that emerge built completely around its capabilities, but that's the second step.  :-)"
    author: "Scott Wheeler"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "\"That's kind of the wrong way of looking at it -- \"Tenor\" isn't something that users will use directly; it's a framework, not a tool or a specific interface component.\"\n\nI was under the impression that Tenor is somekind of \"Spotlight on steroids\" :). As in: a set of technology that can be embedded in to the system. Maybe I should RTFA again :)."
    author: "Janne"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "Well, the article is mostly speculative.  I'm fundamentally an application developer (that ends up spending more than half of my time on framework components, so...) so when I explained this stuff to Kurt at one point I mostly talked in terms of what people will be able to do with this stuff.\n\nIn other words the things that Kurt mentions are the things that I've designed towards; concrete use cases, if you will.  But the user visible things will come as applications start to use the framework and new tools are written to take advantage of it.  I'll probably write a basic search tool to show off the possibilities and then let the interface guys have fun with it.  :-)\n\nBasically Tenor makes it easy to write a \"Spotlight on steroids\" -- like libkio and KHTML make it easy to throw together a basic webbrowser, but that's not what Tenor itself is.\n\nPart of the problem with explaining some of the stuff is that it's sometimes hard to imagine how something will work when there's nothing popular to really compare it to.  Most of the stuff that people think of is a subset of what Tenor makes possible."
    author: "Scott Wheeler"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: ">Of course, there is a problem that KDE has even today: how do we tell the users >about all the great technology KDE has? I still run in to KDE-users who do not >know about audiocd:// or fish://!\n\nI guess I am one of them. When I paste audiocd:// into the location bar on Konqueror, I get a dialogue \"Malformed URL audiocd://\" The same applies to fish://.\n\nIn my protocols config dialog, I have \"fish\" and it's checked, \"audiocd\" is missing. I am running kde3.4. Where is the documentation on these protocols? Doing a google search brings up so much information not so useful to a n00b like me. Thanx.\n"
    author: "charles"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "Charles.\ndo you have kde-multimedia installed? I guess you are just missing some part of KDE."
    author: "anon"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "It's \"audiocd:/\" and \"fish://\" needs to go with a hostname or an IP, e.g. \"fish://my.server.tld\"."
    author: "Daniel Molkentin"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "That did it, thanx! While I know what ftp:// and http:// or https:// can be used for, I wonder what use could be made of fish://. Since I have no system setup for the fish protocol, could you point me to one? Cb.."
    author: "charles"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-15
    body: "That's what's great about fish://, there's no setup, your linux machines are already fish servers (as long as they run an SSH server, which almost all do).  For any Linux or Unix machine you have a username, simply use fish://username@machinename to access the filesystem of that machine using that username.  Unfortunately, it doesn't work for Windows machines, for that you have to use smb://."
    author: "Spy Hunter"
  - subject: "Re: Tenor KIO-slave? - Wow!"
    date: 2005-04-15
    body: "I can only say wooooow! I never knew KDE has this feature and I guess I am missing a lot more. I guess I can put \"fish://username@machinename\" in the save dialogue to save a file on this machine if everything is setup. This kind of publicity is what we need for KDE. When I get back to the Linux box at home I will try it out! Thanx. What other exciting protocols/goodies are there to exploit?"
    author: "charles"
  - subject: "Re: Tenor KIO-slave? - Wow!"
    date: 2005-04-15
    body: "To get a list of all the available protocols:\n$ kcmshell ioslaveinfo"
    author: "blacksheep"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "Its audiocd:/, and fish://hostname or fish:/\n\nWhich distro are you using?"
    author: "jms"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "My only grip about KIO is that tar, rar, etc should be implemented as pipes or something like that. I mean, it should be possible to view a TAR file from a FTP. Dunno if this is being worked on. My suggestion for how the url would look like is something like this: scp://server.com/file.tar|tar .\n\nAnyway, yes, KIO is one of the best piece of software that lives in KDE. Really useful."
    author: "blacksheep"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "I've always thought something like ftp://ftp.server.com/file.tar#tar:/file/inside/it would make sense -- taking a cue from the webpage.html#anchor-to-jump-to syntax. Don't know whether it would conflict with anything else, though."
    author: "Illissius"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-15
    body: "It would be great except that somebody might use anchors named tar: in their html file, and even possibly name their html file with a .tar extension.  It would be hard to figure out what was meant in every situation."
    author: "Spy Hunter"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "It is actually, and honestly a very annoying/good thing (depending on what I want to do, which I don't expect KDE to be able to telepathically figure out) with my config is that it does show up as the opened tar file over ftp or http. (It's something I forget which under file types, show in embedded viewer as default I believe.)\n\nAnd tar, gzip, bzip all already are piped through eachother. The tar kio-slave only handles uncompressed tar files (As of at least KDE 2.x, as I last recall looking. I doubt it would have changed), and the appropriate gzip/bzip kio-slave is used automatically. \n\nIe, tar://home/user/example.tar.gz is really tar://gzip://home/user/example.tar.gz (or something like that) "
    author: "James L"
  - subject: "Re: Tenor KIO-slave?"
    date: 2005-04-14
    body: "Here's my proposal: stack the protocols at the beginning, using the username part of the standard URL format as the file path for the archive.  I think this could be implemented with no changes to the KIO framework, all the work would be done in the new IOSlaves.\n\nA simple one: open a zip file on an HTTP server.\n\nzip:/@http://server/file\n\nOpen a bzip2 compressed tar file on an HTTP server\n\ntar:/@bzip2:@http://server/file\n\nA complex example:  open a particular file in a passworded rar file through fish://.\n\nrar:/rardir/rarfile:rarpass@fish://user:pass@server/file.\n"
    author: "Spy Hunter"
  - subject: "Cat out of the bag too early?"
    date: 2005-04-14
    body: "Though I have lot of respect for both Scott and Aaron and their capabilities to pull this off, I think this article is mis-timed and full of vaporware. I am sure it will be delivered with all (maybe more) or most of the promised functionality, we have some of the best people. The article has 5 pages three of which are just a waste of space, and the last two pages offer so little information about the technology itself.\n\nThe only new bit I got was that we have a formal name for \"klink\" and its called \"Tenor.\" Rest was just a rehash of existing information that Scott has been talking about last few months.\n\nI would wait till the ideas take shape, fuzzies become clear, and we have some code to show. Till then, lets not raise the hopes of others. KDE is not known for pushing vaporware."
    author: "floyd"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "Since when is talking about upcoming KDE technologies 'vaporware'? KDE 4 is of major interest to people here on the dot including some of the new and kool ideas planned. When Aaron writes on his blog about the Appeal project some of his talk comes off sounding almost like hyperbole, but I have confidence in his and other kde devs abilities. Granted nothing is concrete yet in the project, but calling it vaporware is a bit of a stretch, since it is quite clear it is in the idea stage. Vaporware is implied complete code."
    author: "Greg Diebel"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "Blogging about something like this is the perfect way to get the idea out, not on the high profile \"dot.\" I did not object to the idea just the way it was pushed. Thank you."
    author: "floyd"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "You're flogging a dead horse here. This is something KDE doesn't do enough of in terms of talking about the projects and ideas they want to undertake and letting people know what's happening. Tenor is a bit more than just vapourware now, as you'd have understood if you'd read the article. What about WinFS or Hula? Neither of those actually work. At least no one is in any doubt as to what's going on here, and that these are people formulating solid ideas amd it says as much in the article. No, I don't think it's misleading.\n\nIf you have accusations of promoting vapourware then you should be complaining about that to the people who've consistently done that for about five/six years."
    author: "David"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "## KDE is not known for pushing vaporware. ##\n----\nKDE however is also known for being vastly inferior to Gnome so far. On the \"marketing\" side of things, not technology-wise....\n\nThis article (and some more I saw over the last months), and Aaron's blogs and postings here start to change this.\n\nAaron's hints also made me feel comfortable about the non-existance of any  \"vaporware\" danger regarding Tenor.\n\nSo what's wrong with The Bible having adviced us \"Do Good Things and Talk about Them!\" ??"
    author: "konqui-fan"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "I guess both you and the guy above missed my point. Sigh..."
    author: "floyd"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "Oh you're point? What was that, because I think it has been addressed adequately?"
    author: "David"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-14
    body: "\"Though I have lot of respect for both Scott and Aaron and their capabilities to pull this off, I think this article is mis-timed and full of vaporware.\"\n\nHyping the stuff KDE people are thinking and talking about a bit more doesn't hurt. It's something KDE actually hasn't done enough of. If you want consistent hype and vapourware constantly then you should be looking elsewhere ;).\n\nIt shows that KDE has thoughts on these matters and really wants to work towards them. Saying nothing too much does KDE some damage, as everyone thinks \"Well, what on Earth are KDE doing?\". Consequently, many people think the sun shines out of Beagle's rear, when it really does nothing special whatsoever - even if it does exist."
    author: "David"
  - subject: "Re: Cat out of the bag too early?"
    date: 2005-04-15
    body: "> I think this article is mis-timed and full of vaporware.\n\nwell, we aren't sharing things much publicly to date because we've been going through various revisions and research to get something we're comfortable with showing. it's not as far away as it may seem however. e.g. this isn't something you're going to wait 6 months to see, for instance, let alone for KDE4.\n\n> I would wait till the ideas take shape,\n\nwe've been working on that since last summer.\n\n> fuzzies become clear,\n\nthey're getting pretty darn clear to us by this point.\n\n> and we have some code to show\n\nindeed, and it's on it's way. one of our challenges is that as we get this ready for people to use, we need application developers aware of it and ready to poke at it and play with it. so we're trying to prep the development community a bit for its arrival as a \"ready to abuse API\". =)\n\nand as people have become aware of it, they become excited (e.g. Kurt, in this case) and want to tell others about it. that's cool. people get excited and wish to share."
    author: "Aaron J. Seigo"
  - subject: "OT: \"Finding\" Applications and KParts Plugins"
    date: 2005-04-14
    body: "When searching for a document in Konqueror or KFind, they both list the documents matching the search criteria.\n\nIf a \"search text\" is given, that the document must contain, Konqueror again only displays the documents it finds, and KFind additionally displays the first occurrence of the \"search text\". They both do not show <i>all</i> occurrencies of the \"search text\".\n\nOne has to open each document, one by one, by clicking it, and if a specific document is larger than a view lines, it is faster to use the applications built-in search functionality, entering <b>again</b> the \"search text\".\n\nFor now, I can think of 2 solutions to this \"problem\":\n\n1. Change KDE applications so they e.g. accept a parameter --find 'search text' parameter and then automatically open the search dialog which has the 'search text' already set according to the parameter. The user can then click \"Find\" to find the first occurrence of the search text (instead of entering the \"search text\" some dozen times).\n\n2. Change the KParts so they are able to provide buttons for searching in both directions, and maybe highlight all occurrencies of 'search text'. (Has Google a patent on this? :-))\n\nBoth, the changed applications, and the changed KParts, would somehow need to be \"marked\" as \"text finders\" (this does not make much sense for KPaint for example).\n\nWell, pasting the \"search text\" may do the trick as well :-)\n\nWhat do you think?\n\nSorry for my bad english.\n"
    author: "MM"
  - subject: "Re: OT: \"Finding\" Applications and KParts Plugins"
    date: 2005-04-14
    body: "Like http://lists.kde.org/?l=kde-devel&m=111324648032583&w=2 ?\nThe text of files will be extracted with plugins, which also annotate the extract text with structural information. This can be used by both the search tool (\"'blah' found in chapter 5\") and by the viewer to open the document at the corresponding position. \nUsing only the search string might break if the text extracted by kfind and the text found with the viewer doesn't match 100%. kfind must also be aware of the supported search options of the viewer (case sensitive, wildcards..)."
    author: "uddw"
  - subject: "Re: OT: \"Finding\" Applications and KParts Plugins"
    date: 2005-04-14
    body: "Well, this is really an implementation issue.\nThey show only the first line, because this way they can stop searching the file after the first match. And kfind provides only one listviewitem per found file, so only one matching line can be shown.\nI also already thought about suggestion 1)\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Very interesting!"
    date: 2005-04-14
    body: "This sounds really cool - makes it hard to wait for KDE4 :D\n\nKeep up the great work everyone!"
    author: "Joergen Ramskov"
  - subject: "Tenor and KIO-LOCATE"
    date: 2005-04-15
    body: "Tenor will be revolutionary and seems to be an amazing technology, but it won't be ready for at least a year.\n\nHow about until then, the KDE team at least includes KIO-LOCATE into KDE 3.5?"
    author: "Alex"
  - subject: "Re: Tenor and KIO-LOCATE"
    date: 2005-04-15
    body: "I hope for kio_locate in 3.5 too. (I compiled it on my own and use it daily).\n\nThe main reason it could not go into 3.4 proper was that its author wasn't in favor. He said he had no time (exams) for a few months to maintain it properly, but he'd do it from early summer.\n\nHope this materializes..."
    author: "Kurt Pfeifle"
  - subject: "Re: Tenor and KIO-LOCATE"
    date: 2005-04-15
    body: "Armin Straub, the author, has just asked for the inclusion in KDE. \nIt'll go in after the move to SVN.  IIUC into extragear.\n\nSee http://lists.kde.org/?l=kde-devel&m=111328905108759&w=2"
    author: "cm"
  - subject: "Dont forget wildcard-search , please!"
    date: 2005-06-17
    body: "The concept of Tenor sounds realy nice.\nI hope Tenor will make it possible to search with wildcards and operators, one thing I am missing in most of Desktop-Search-Engines."
    author: "Marc Seibert"
---
On <a href="http://www.linuxplanet.com">LinuxPlanet</a>, Kurt Pfeifle <a href="http://www.linuxplanet.com/linuxplanet/reviews/5816/1/">explains details about Tenor</a>, a framework that will facilitate new ways to locate and otherwise manage content. Learn about the background of Tenor, what differentiates it from "desktop search tools" and why this new approach is needed in a world of ever growing disk space. Additionally, the article provides some insights from <a href="http://developer.kde.org/~wheeler/">Scott Wheeler</a>, one of Tenor's authors and primary designers.




<!--break-->
