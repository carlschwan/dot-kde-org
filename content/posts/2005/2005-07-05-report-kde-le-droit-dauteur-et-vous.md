---
title: "Report: KDE at Le Droit D'auteur et Vous"
date:    2005-07-05
authors:
  - "amahfouf"
slug:    report-kde-le-droit-dauteur-et-vous
comments:
  - subject: "Unite"
    date: 2005-07-05
    body: "Stalinman has interesting ideas."
    author: "Kraig"
  - subject: "enough with the k-names!"
    date: 2005-07-05
    body: "Kimon?  Couldn't you guys choose a more original name already?"
    author: "Anonymous"
  - subject: "Re: enough with the k-names!"
    date: 2005-07-05
    body: "In case it wasn't clear, I was just kidding.  Nice work Kimon and Annma.  Those Kubuntu CDs look sweet."
    author: "Anonymous"
  - subject: "Re: enough with the k-names!"
    date: 2005-07-05
    body: "yes, Kimon has Greek roots and his name starts with K. Just a koincidence! ;-)\n\nThe Kubuntu CDs look very professional. People know about Kubuntu and those who took a CD will try it in a vast majority. Some people already ran Kubuntu.\nI realized that Kubuntu is THE distribution of 2005 so far (not meant as a distro war).\nAnd an EduBuntu is on the way with at least 13 KDE-Edu programs!"
    author: "anma"
  - subject: "Thank you Anne-Marie and Kimon ... "
    date: 2005-07-05
    body: "for demonstrating the KDE desktop at \"Le Droit D'auteur et Vous\". I am very happy that KDE people are at so many events nowadays. Also a BIG thank you for KDE-edu and its sweet apps, annma! And yes ... seeing Kubuntu CD's popping everwhere is very cool. \n\n\nFab\n\nBTW Kimon .. you are looking very serious on all of these pictures :)"
    author: "Fab"
  - subject: "Re: Thank you Anne-Marie and Kimon ... "
    date: 2005-07-05
    body: "The pictures were taken just after he arrived while I was already there for a while. I was amazed by his energy and the way he related smoothly with people. I already mentionned his knowledge of KDE and he answered better than me to questions. He did not panic when my laptop did not want to book (no X, no tty prompt) and eventually the laptop got it right. \nThanks to the Dot support as Kimon joined after the announce of this event was published.\n\nAnd thanks Kimon!\n\nAlso thanks to my family for postponing my birthday party and giving me my Sunday free! "
    author: "anma"
  - subject: "Thanks for the cd Anne-Marie"
    date: 2005-07-16
    body: "I actually got around to trying Kubuntu on an old PII-333 box and it installed like a charm. Well, it fudged the network device part so I cant go online but Im not putting blame anywhere yet since as a Mac-Win user I had NO clue what I was doing. None. Easy install and while it ran slowly it was much better than the Win98 that was on the machine before I erased the drive.\n\nStill, got around to seeing what was available on the desktop and showed it to my dad and his question was \"Why do I need windows?\"\nYou dont dad. As soon as we figure out the network problem and how to dual boot, every cpu in the family will be getting Kubuntu installed.\n\nAfter Red Hat a few years ago, I kept saying that I was going to try one of the easy new distros and even though I DL gigs of stuff, I never got around to it.\nAfter the cd stared at me for about 2 weeks I finally gave in and can not find a reason why I wont dual boot my cpu soon (still need win based apps). Well, I also gave in because I listen to enough Lugradio online and they keep pimping Ubuntu like its out of style.\n\nSo thanks Anne-Ma., good job KDE folks and Ubuntu to you too!!\n\nzeke"
    author: "zeke"
  - subject: "I SO had to go."
    date: 2005-07-05
    body: "Why did I sleep late...\n\n'Least if there's another one, I know for sure the GULUS (Groupe d'Utilisateurs Linux de l'Universit\u00e9 de Sherbrooke ==> Sherbrooke College Users Group, SCUG :) ) will at LEAST have a booth :)"
    author: "Renaud"
  - subject: "Re: I SO had to go."
    date: 2005-07-05
    body: "The GULUS was there, I had a very nice chat with them (you know how women are chatty so of course I went for a tour of all the kiosks). LUGs are very lively in the Montreal area, I myself go to the GULSE meetings when I have time.\nThis was not a big event by European standards but it was an important one as it's difficult to get people together in Quebec (distances).\nThere will be the SQIL in November (12 - 20) so you can already put that in your calendar! See you there!\n"
    author: "anma"
  - subject: "Multimedia"
    date: 2005-07-05
    body: "hi guys, \n\nCMAQ (Quebec Indymedia) will have video material from the day available soon -- I am in the process of editing it now. \n\nsince annma asked me to publicize the URL, here it is: \n\nhttp://quebec.indymedia.org -- http://www.cmaq.net\n\n\nsome pix by Omar have already been posted here: \nhttp://gallery.cmaq.net\n\n..take care  :)"
    author: "simms [quebec.indymedia.org]"
  - subject: "Re: Multimedia"
    date: 2005-07-05
    body: "Cool,\nWill there be a transcript of the talk available too?"
    author: "Ivor"
  - subject: "Re: Multimedia"
    date: 2005-07-29
    body: "no transcripts yet, but the audio is available :\nhttp://copyright2005.koumbit.org/snapshot/audio/\n\n"
    author: "Robin Millette"
  - subject: "why not ogg?"
    date: 2005-08-23
    body: "I was going to send this to friends who couldnt make it \nbut who the hell is going to be able to use .spx files?\nI know its a codec designed for speech but cmon, \nhow much better can it be than ogg?\nOgg files are no problem since they work in all OS, usually as a plugin to a player like Winamp and it is open source as well.\n\nInformation wants to be free but if no one else hears about it then all we are doing is preaching to the converted.\nI remember the studies years ago that used to say that if a page loaded up in over a certain amount of time, you'd lose X% of the people visiting the site.\n \nHell, I thought spx was a graphics format (it is) and really think that its counterproductive when we make people jump through hoops.\n"
    author: "darl m."
  - subject: "Re: Multimedia"
    date: 2005-07-05
    body: "Awesome, thanks a lot! The pics are superb!\nThanks to CMAQ, thanks to you for filming so professionnally. That's what I liked in this event: meeting not only IT related people but also lots of people who fight for freedom in the society: for free media, free education, same rights for all people, etc. People who do things, people who make a difference.\nAnd again thanks to Robin Millette for bringing us all together.\n\nWe'll try to make a transcript of the film, I'll post on my blog when any news about it.\n"
    author: "anma"
  - subject: "Re: Multimedia"
    date: 2005-07-05
    body: "Thanks for your request on public download of the film... I just didnt realize that Mr. Stallman would give his speach in french :-/\n\n>We'll try to make a transcript of the film, I'll post on my blog when any news about it.\n\nThat will earn you a cool-score of at least 10^100! :-D\n\n~Macavity"
    author: "macavity"
  - subject: "reading Konqi"
    date: 2005-07-05
    body: "KDEs artwork is really excellent.\nThe \"reading Konqi\" in one of the booth pictures is great!\n\nI found a .png version of it on the kubuntu website, but this is only 284 x 261 pixels.\nWhere can a find a higher quality version of the picture?\n\nThanks in advance.\n\nPeter\n\n\n\n\n"
    author: "Peter"
  - subject: "Re: reading Konqi"
    date: 2005-07-05
    body: "This artwork is from an artist nicknamed basse whom website is:\nhttp://www.kulma.org/linux/kde/\nfor KDE artwork.\n\nYou can find a huge Reading Konqi here:\nhttp://www.kulma.org/linux/kde/kone.php?categ=hires&kuva=konqi_kubudoc_hires.jpg\n\n"
    author: "anma"
  - subject: "Re: reading Konqi"
    date: 2005-07-05
    body: "High rendering is there:\nhttp://www.kulma.org/linux/kde/kone.php?categ=hires\n\nbasse, if you read this, how about a hanged Konqi for KHangMan? "
    author: "anma"
  - subject: "Re: reading Konqi"
    date: 2005-07-05
    body: "Are you violent? Let Konqi live!"
    author: "Anonymous"
  - subject: "Re: reading Konqi"
    date: 2005-07-05
    body: "Look forward to Konqi doing a new job on a revived site soon! :-)"
    author: "Anonymous"
  - subject: "Re: reading Konqi"
    date: 2005-07-06
    body: "Thank you.\nThis is great stuff. I think it will serve as my new desktop background.\n\nI also like the \"Office Konqi\" :-)\nAnd yes, of course let him live!\nBtw: Does a dragon have a gender?\n\nPeter\n\n"
    author: "Peter"
  - subject: "Re: reading Konqi"
    date: 2005-07-06
    body: "yes, Konqi is male and Katie is female ;)"
    author: "anma"
---
Last weekend KDE exhibited at <a href="http://copyright2005.koumbit.org/">Le Droit D'auteur et Vous</a> (Copyrights and You) a one day event in Montr&eacute;al.  KDE developers were there showing off KDE 3.4.1, the latest development version and handing out <a href="http://www.kubuntu.org/">Kubuntu</a> CDs.  There were lots of questions for the exhibitors and lots of praise for KDE too.  Read the full report below.
<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex"><img src="http://static.kdenews.org/jr/kimon-annma-montreal.jpg" with="303" height="294" /><br />Kimon and Annma</div>

<p>The day started with a small expo with booths. Following the <a href="http://dot.kde.org/1119478882/">article on the Dot</a> a few days ago, an amazing KDE user joined me to man the booth, Kimon. He speaks fluent French and English and has in depth knowledge of KDE applications. A fellow francophone and former KDE developer joined as well sporting a KDE shirt. We gave Kubuntu CDs away (special thanks to Jonathan who shipped those CDs from Scotland at such short notice). Some of the questions included KDE X.org interaction (such as 3D window managers), modules management (people would like to install more easily one package without the whole module which is what some distributions package currently) and the new Qt and KDE 4 release. KDE artwork was highly praised and several users mentioned that KOffice and the way KDE applications are all integrated make the difference for them versus other desktops.</p>

<p>Then Richard Stallman gave a talk about copyright, starting with the four freedoms mentioned in the GPL and explaining his views on the copyright problem. I know that not all people share his ideas but what he proposes is a different social ideology. There are several laws in different countries that threaten to tie our freedoms, laws about copyright and patents that we should fight if we care about freedom. You are free to try his ideas or not to so I will not debate about that.</p>

<p>The main thing I realised during the whole day was about education. Everyone has the right to have access to education and educational material should be free. One of the main successes of the last three years is Wikipedia showing that it is possible to achieve this.</p>

<p>I met other people in different areas, people working in building a free WiFi network in Montreal for example. Stallman's talk was in French and will be freely available soon.</p>

<p>We finished around a good beer in a cool caf&eacute;, listening to a Jazz band playing as part of the <a href="http://www.montrealjazzfest.com/">Monr&eacute;al Jazz Festival</a>.</p>

<p><a href="http://edu.kde.org/announcements/3July/images.html">Photos on edu.kde.org</a>.</p>