---
title: "KDE Dot News: Sponsored by OSU Open Source Lab"
date:    2005-12-16
authors:
  - "numanee"
slug:    kde-dot-news-sponsored-osu-open-source-lab
comments:
  - subject: "binary"
    date: 2005-12-16
    body: "Gentoo dodges a lot of issues binary distributions by having everything built from source. If binary compatibility is broken, that doesn't mean a half dozen packages need to be re-released. And USE flags replace multiple packages for the same software with different features.\n\n...but anyways all this makes binary packages probalematic for complicated software. Your neighbors Gentoo box might be quite different. You could create binary packages yourself on a non-production box and then install them with emerge."
    author: "Ian Monroe"
  - subject: "Re: binary"
    date: 2005-12-16
    body: "> It'll be worth investigating emerge binary packages, if those exist.\n\nIf you want to use mostly binary packages, don't use gentoo. Being a source distribution is the heart and soul of gentoo. All the flexibilty gentoo offers (using different USE flags to customize the packages to your needs, different CFLAGS to use processor specific features and optimizations, alltough the effect of these is often overrated, being able to reverse rebuild library dependcies, etc. ...) depends crucially on using a working gcc toolchain to install the packages.\n\nSo the way to go for a server, if that should use it's resources fully for the offerd services und not for compiling new packages, would be to have another gentoo box (or even a compiler farm, ou can easily integrate your laptop using distcc) producing the binary packages for the server, even crosscompilng for a different architecture is not a big problem.  "
    author: "furanku"
  - subject: "Re: binary"
    date: 2005-12-16
    body: "> It'll be worth investigating emerge binary packages, if those exist.\n\nMmm... I think I should add that you can try Gentoo installing it with precompiled binary packages... in order to avoid you compilations the first time. \n\nLater, if you like Gentoo, you can compile programs in the background while you use the system, in order to get an optimized system in speed and memory (you only load the drivers you need, and so on)."
    author: "FRank"
  - subject: "Re: binary"
    date: 2005-12-16
    body: "(you only load the drivers you need, and so on)\n\nWhat? Are you implying that other distros don't only load the drivers you need??"
    author: "blacksheep"
  - subject: "Re: binary"
    date: 2005-12-16
    body: "Just another gentoo ricer :-P"
    author: "Anon"
  - subject: "Re: binary"
    date: 2005-12-19
    body: "> Just another gentoo ricer :-P\n\nInstead of publishing unjustified comments, I would give you the advice of justifying them (if you can) :-P \n\nExcuse my English :-)\n"
    author: "FRank"
  - subject: "Re: binary"
    date: 2005-12-19
    body: "> What? Are you implying that other distros don't only load the drivers you need??\n\nI said that compiling for your system only install the drivers you need. For example if you use kernel binaries already prepared... you'll get drivers you don't need in your kernel."
    author: "FRank"
  - subject: "Re: binary"
    date: 2005-12-16
    body: "The USE flags are what Gentoo is all about (not '-funroll-loops').\n\nI love Gentoo but I probably would avoid it for a production server, Gentoo feels far more flexable than Fedora and SuSE to me (though it does make you look forward to new releases of KDE a whole lot less ;-)"
    author: "Corbin"
  - subject: "Re: binary"
    date: 2005-12-18
    body: "right up untill you want to uninstall something....\n\nGentoo has a lot of good things going for it, but I'd like to see the package management actually completed.  I shouldn't have to troll through the forums to find a third party script just to uninstall something + everything that depends on it - and then it shouldn't take hours."
    author: "mabinogi"
  - subject: "Re: binary"
    date: 2005-12-18
    body: "You don't need a script at all, it's build in portage;\nemerge $package_name -C && emerge --depclean && revdep-rebuild"
    author: "LB"
  - subject: "Re: binary"
    date: 2005-12-18
    body: "Hmmm... Intuitive.\n\nI can see why they didn't go for:\n\nemerge -r package_name"
    author: "Tim"
  - subject: "Re: binary"
    date: 2005-12-19
    body: "I usually just run \"emerge unmerge packagename\". Maybe the other commands listed in the parent's parent are a better general practice, but unmerging (uninstalling) doesn't require them. Not so unintuitive after all, at least for those who understand the commandline....\n\nP.S. \"unmerge\" is an alias for the flag \"-C\"\nP.P.S. There are GUI frontends for emerge, it doesn't have to be totally commandline."
    author: "Pingveno"
  - subject: "[OT] Jingle (VOIP) with kopete???"
    date: 2005-12-16
    body: "http://google-code-updates.blogspot.com/2005/12/jingle-bells.html\n\nJingle Bells: \nExciting things are happening around Google Talk today. First, Jabber has published the experimental draft Jingle specs, which extend XMPP for use in voice over IP (VoIP), video, and other peer-to-peer multimedia sessions.\n\nNow, I hope kopete can do voice and video communciation (p2p way) using jingle :)\n\n\nAnd many thanks for hosting \"dot\" at OSUOSL..."
    author: "fast_rizwaan"
  - subject: "Re: [OT] Jingle (VOIP) with kopete???"
    date: 2005-12-16
    body: "I guess you already put this in Kopete's bugzilla as a WISH, didn't you?\n\nPlease try not steal the thread anymore..."
    author: ":-?"
  - subject: "Good News"
    date: 2005-12-16
    body: "\"Xen is completely transparent to the typical VM user and if I didn't know better I'd think we had a dedicated machine\"\n\nThat's what it's there for.\n\n\"Gentoo system but it has been extremely easy to pull in and configure any extra software we needed\"\n\nIt certainly is a nice system to run as a server. The only reason why I run Suse is because many people around me need to have some graphical configuration tools......\n\n\"However, the fact that emerge compiles everything from source is starting to get a little old\"\n\nHow much do you emerge on a production server? I emerge very, very little these days."
    author: "Segedunum"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "i suspect you want to keep your system up-to-date, esp for security. so you'll have to emerge some stuff now and then. and for a desktop system, i can do this at night - i don't use it when i sleep anyway (do you?). but a server is working most (if not all) of the time, so having to emerge stuff (thus slowing its work) is imho a waste of cpu/mem/etc. i love gentoo, but wouldn't run it on a server."
    author: "superstoned"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "Note that OSUOSL has allocated us enough resources that so far it hasn't really been a problem.  I don't think it was noticeable to outsiders while I was compiling XEmacs, for instance.  It isn't a \"best practices\" approach however, as you point out."
    author: "Navindra Umanee"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "> I don't think it was noticeable to outsiders\n> while I was compiling XEmacs, for instance.\n\nYep. Just a \"The server is over a big load. Come back later.\" message."
    author: "David Costa"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "Nothing to do with it."
    author: "Navindra Umanee"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "Make sure you set \"PORTAGE_NICENESS=\"19\"\" in make.conf so your emerges will have as little impact as possible, also having ccache installed will reduce the time to upgrade a package (especially if the difference between the one installed and the new one is only a 1 line change to fix a bug)."
    author: "Corbin"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "Handy hints there. Also do the practical thing - schedule this kind of thing when there's as little activity on the system as possible. With all your experience of running the dot you'll know when this is."
    author: "Segedunum"
  - subject: "Re: Good News"
    date: 2005-12-16
    body: "\"i suspect you want to keep your system up-to-date, esp for security.\"\n\nOccasionally, but very little. You usually emerge on a test server first just to see what will happen of course, and do a quick search around for any problems before you do it. Compiling most popular server software doesn't take anywhere near as long as you'd think. Xen and VMware makes having a replica server a doddle.\n\n\"i love gentoo, but wouldn't run it on a server.\"\n\nI'm the opposite :-). Gentoo makes a great server, the tools are first rate as is the really nice documentation. On the other hand, I just really cannot be bothered to compile stuff on a desktop - and desktop stuff really does take an absolute 'age' to compile."
    author: "Segedunum"
  - subject: "Awesome"
    date: 2005-12-17
    body: "As an Oregon resident and a KDE user, I am very happy that OSU is supporting KDE. Kudos to OSU OSL!!!\n\nOsho"
    author: "Osho"
  - subject: "osuosl infrastructure manager comments on kde 3.5"
    date: 2005-12-17
    body: "http://staff.osuosl.org/~cshields/?p=128\n\nnice =)"
    author: "ac"
  - subject: "tcsh isn't in color?"
    date: 2005-12-19
    body: "On my Gentoo install, even the tcsh prompt is colorful."
    author: "Donnie Berkholz"
  - subject: "Re: tcsh isn't in color?"
    date: 2005-12-20
    body: "Good to know!  I suppose you either have a different version or something is broken on my install.\n"
    author: "Navindra Umanee"
  - subject: "Try setting your TERM to one with color support"
    date: 2005-12-20
    body: "\nTERM=linux\n\nshould do.\n\nMake sure ssh or Putty aren't messing up the term settings.\n\nDaniel\n\n"
    author: "Daniel"
  - subject: "Re: Try setting your TERM to one with color support"
    date: 2005-12-21
    body: "No luck..."
    author: "Navindra Umanee"
---
I'm happy to announce that KDE Dot News is now fully hosted and supported by the <a href="http://osuosl.org/">OSU Open Source Lab</a>.  OSUOSL have graciously provided us with both server and network hosting, although of course, OSUOSL has long been hosting us on their network while we had been sharing the <a href="http://www.arklinux.org/">Ark Linux</a> webserver.  As we outgrew the Ark Linux server and ran into resource limitations however, OSUOSL also graciously offered us new server hosting.  The dot is now significantly more responsive and we should definitely be seeing an improvement in uptime as well.  A big <a href="http://www.kde.org/support/thanks.php">thank you</a> to OSUOSL and all the <a href="http://osuosl.org/about/staff/">great guys</a> on their support team -- it's been a true pleasure working with you.






















<!--break-->
<p>
For those interested, the OSUOSL server hosting has been provided to us in the form of a <a href="http://www.cl.cam.ac.uk/Research/SRG/netos/xen/">Xen virtual machine</a> running <a href="http://www.gentoo.org/">Gentoo Linux</a>. I must say I am truly impressed by the combination.
<p>
Xen is completely transparent to the typical VM user and if I didn't know better I'd think we had a dedicated machine -- no doubt the OSUOSL staff are also to thank for making it all work so smoothly.  Performance has been fantastic although it will be interesting to see how things progress as the machine gains more users. 
<p>
I've never used Gentoo before but I am finding it to be a well thought out distribution.  Everything just works and it has some nice touches out-of-the-box such a colorised bash shell (unfortunately I'm a tcsh guy and experiencing colour in bash made me quite reluctant to go back to my black and white world).  We were initially provided with a fairly barebones Gentoo system but it has been extremely easy to pull in and configure any extra software we needed -- a simple emerge usually does the trick.  emerge even comes with an rpm-compatible command line interface in the form of epm.  However, the fact that emerge compiles everything from source is starting to get a little old -- nothing like waiting for XEmacs to compile on your production box.  It'll be worth investigating emerge binary packages, if those exist.
<p>
Finally, another thanks to <a href="http://www.arklinux.org/"><img src="http://www.kde.org/dot/Images/arklinux.gif" alt="Ark Linux" width="80" height="15"></a> for having hosted us for so long despite their limited resources.





