---
title: "A Fireside Chat on KDE Usability"
date:    2005-02-09
authors:
  - "aseigo"
slug:    fireside-chat-kde-usability
comments:
  - subject: "Extremely interesting"
    date: 2005-02-09
    body: "Thanks for this great `interview'!\nI'm looking forward to the new PIM and KDE 4.\n\nKDE doesn't have a good usability reputation, hopefully this will change soon :-)"
    author: "Quique"
  - subject: "kmail"
    date: 2005-02-09
    body: "why kmail use this  glaring colors as background for gpg mail?\nI think this is really bad. Couldn't kmail show more decent that a gpg key is correct?"
    author: "pinky"
  - subject: "Re: kmail"
    date: 2005-02-09
    body: "The colors are configurable (well, the background isn't but it's derived from the configurable OpenPGP colors), so if you don't like them change them.\n\nAnd to answer your second question: Maybe, but I doubt it. Colors are a very simple and very easy to learn metaphor for good/valid (green) and bad/invalid (red). Moreover, the frames indicate in a very intuitive way which parts of the message are signed, i.e. are the attachments also signed or is just the message text signed.\n\nAnyway, why don't you make a suggestion for a more decent way to show that the signature is valid?\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail"
    date: 2005-02-09
    body: "I think that's a very good setup. It makes it really obvious, like no other way I've seen does."
    author: "mikeyd"
  - subject: "Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "I also think KDE needs to pay much more attention to usability. For example, the Konqueror and KGet toolbars need a major rework. They contain too many things that may confuse a normal user. I think they should only contain the things that are used more often. For example, who uses the Cervisia toolbar button in Konqueror? I'm sure less than 1% of its users. If that's accesible from the menus, then it should be removed.\n\nA similar thing happens in KGet. It has the auto-off mode, auto-paste mode, auto-* mode buttons, while they are available from the Options menu. How often do people use it? Probably never. Removing them would make the toolbar easier to use and if someone wants to use that functionality it's still in the Options menu.\n\nSo here's my proposal for the Konqueror toolbar:\n\nhttp://www.polinux.upv.es/mozilla/varios/imagenes-otros/konqueror-simplificado.png\n\nAs you can see, it contains the location bar in the same place as the rest of the buttons, like Firefox does. It only has 6 buttons (from left to right): Up, Back, Forward, Home, Reload and Stop. Does someone really need something more than these? The rest of the options are still available from the menus, and I think this makes Konqueror much more easy to use.\n\nAbout KGet, this is my proposal:\n\nhttp://www.polinux.upv.es/mozilla/varios/imagenes-otros/kget-simplificado2.png"
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "You might want to note that Cervisia is only installed with the development packages; so the person installing the box clearly wanted development tools in there. For secretarial/light home use, the development packages wouldn't be there, and thus the Cervisia icon would be absent :)"
    author: "Luke Chatburn"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "You may note that the computer can be used by different type of users.  The same computer can be used by a developer type of user and some time later by an email/internet browser type only user...\n\nIn a thin client setup they could all be working on the same box at the same time...\n"
    author: "ac"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "Isn't that why userprofiles exist?"
    author: "Tim Beaulen"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "Yeah, but the Cervisia view button depends on the installed software not on any user-profile-depend setting."
    author: "ac"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "In such a setup, cervisia should only be installed under the developer profile. The other profile should not even have it in their K Menu, or anywhere else.\n\nKDE apps can easily be installed under multple prefixes. You can set multple directories in your KDEDIRS and when you start KDE it will \"just work\". So you set one global KDEDIRs, and then add the developer prefix tot he KDEDIRS of the developers.\n\nThe only thing this could be traced to is a bad sysadmin. There is nothing wrong with the cervisia icon being there.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "Bad Sysadmin as an excuse...\n\nI can't install one KDE application at a time using precompiled binaries, as nearly all distro use KDE's basic program tree.\n\nWhen I install kdeaddons from FreeBSD's ports, it installs to /usr/local and not ~/ therefor every plugin for konq, kate, etc. are automatically activated right away.  If I remove some of them manually afterwards, I risk upsetting the ports/package management systems.  This isn't unique to FreeBSD - the only way to get around this is to build KDE outside of package management systems, but you cannot reasonably expect most-everyone to do this.\n\nWhen I install kdeaddons, for exmaple, I'm doing it solely for html/css validators in konq and don't need all of the other plugins.  The other plugins bloat the programs as there is no way to easily unload the un-needed plugins from memory, as a user.\n\nWhat is really needed is a list of checkboxes for program extension plugins to enable/disable similar to the mime handling plugins for websites.\n--\nSo bored? read my boring blog at http://tblog.ath.cx/troy"
    author: "Troy Unrau"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "In almost any modern program if a feature is not used then it does not take up memory or cpu time. At worst on startup the program will check what features it has but it won't load them. The features just take up space on the disk and are loaded into memory on demand. There is no need to unload those things from ram since they are not in ram to begin with. \n\nI really get annoyed seeing this kind of comment over and over again. Just like people that look at top and decide how much memory a program is using based on that. Everytime there is an article on slashdot talking about x idiots post about how it is bloated, slow etc etc. They are wrong and have been wrong for a very long time and a lot of profiling has been done to prove it. Even with all of that stuff posted they still post that crap every time. This is really no different. It has been discussed on dot.kde.org many times in the past and I suspect the same people will keep talking about it in the future."
    author: "Kosh"
  - subject: "Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "The button for Cervisia is just an example. I think the best compromise would be making KDE have 2 working modes: basic and advanced. That could be configured in KControl, and I think it would be very easy to implement because you'd only need to load an XML file or another depending on the mode. The default would be \"basic\", because the advanced user is supposed to be capable of going to KControl and switching the mode, but the basic user might not know how to do it. In basic mode it would have the simplest toolbars, like in the screenshot. An usability study would have been aplied to them. The advanced mode would contain more objects, in a similar way like now. Anyway, it should still be coherent with the other programs."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "> The button for Cervisia is just an example\n\nand a poor one because it's already been dealt with in 3.4 ... how? check out beta2 and see if you can spot the difference ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "Aaron, I saw on kfm-devel that you proposed to remove copy, cut paste, print and all that clutter from konqueror (FINALLY!). Any news on that? I saw that basically everyone agreed, but I can't find a cvs commit about it.. or maybe i just missed it\n"
    author: "Anonymous"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "Everyone did not agree, because these buttons are very essential when using Konqueror as a file manager. They only get in the way when you are web browsing.\n\nAnd right now, Konqueror's view profiles don't allow you to have some toolbar buttons in one profile, and not in another.\n\nIf they did, then it would be a no-brainer.\n"
    author: "Jason Keirstead"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "I've been using KDE since 1998 and have not once used the cut/copy/paste icons on the toolbar.\n\nTo copy a file, I select them, CTRL-C, or RMB-copy.  Moving the mouse to the Edit menu or to the toolbar to accomplish this is far too much motion.\n\nMy preferred method is actually to split the konq window into two frames and drag-drop, as I get visual feedback as each file is copied/moved, etc.\n\nFor text, X's select-to-copy and MMB-to-paste has always fulfilled my needs.\n--\nThat bored? read my boring blog at http://tblog.ath.cx/troy"
    author: "Troy Unrau"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "> because these buttons are very essential when using\n> Konqueror as a file manager\n\nwell, it gos both ways. a print icon on the toolbar in file management is rather silly, but it makes decent enough sense in web browsing mode (at least, according to the usage study of web browser toolbars i did last year)\n\n> And right now, Konqueror's view profiles don't allow \n> you to have some toolbar buttons in one profile, and not in another.\n\ni already pointed out on kfm-devel that konqueror allows you to do exactly this. 3.3 shipped with konq-simplebrowser.rc, in fact.\n\n> If they did, then it would be a no-brainer.\n\ni'm glad you agree then. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "The keyboard shortcuts are far superior to the toolbar icons for quick access.  For people who don't know the keyboard shortcuts, they can use the edit menu or right click menu, which has the added bonus of displaying the keyboard shortcuts so they can be learned, unlike the toolbar icons.\n\nThe menu items serve three purposes: they inform the user that these actions are possible, they show the keyboard shortcuts, and they fufill the user's expectation of copy and paste options in the edit/right click menu, which is standard for every application.  The keyboard shortcuts serve one purpose:  fast access.  The toolbar icons are redundant.  They show the user that those actions are possible, but the menu already does that (and for ubiquitous actions like copy and paste, it is hardly necessary anyway).  They are not standard in file manager applications (see Windows XP Explorer, Mac OS X Finder).  They are slightly faster than the edit menu, but are slower than the right-click menu and *much* slower than the keyboard shortcuts.\n\nIn short, the toolbar icons for cut, copy, and paste are redundant, clutter up the interface unnecessarily, and should be removed."
    author: "Spy Hunter"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "very well said.\n\nNothing to add."
    author: "MaX"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-10
    body: "Unfortunately from what I can tell 'normal' (i.e. people that wouldn't reading this site, or anything other site that you would see 'computer' in) don't look in the menus.  They DO look at the toolbar's icons.  Some things in the toobar are useful, like even though I try and use keyboard shortcuts as much as possibly (which makes 'normal' people say 'wtf?'), I still often use several things in the toolbar, depending on the application.  Like in Konqueror I use Up, Back, Forward, Refresh, Stop, and Go; in Quanta I use Preview; in Kopete I use Close on the chat boxes; and in every program that has it (such as Quanta and Kopete) I use the spell check toolbar button.\n\nOften if it involves an 'F<1-12>' key I won't use it (too hard to reach w/ other keys, and too hard to know which your clicking), though I think thats more so a problem with keyboard design.\n\nFor the most part I agree that some of the fat can be removed from the toolbars, but ALWAYS allow them to be re-added manually by the users (just change the default configuration)."
    author: "Corbin"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-11
    body: "The problem is, you have to decide if you are going to make a system for your \"normal people\" or for people that read this site. If we should make it for your \"normal\" people a lot of other functionality would have to go as well. E.g. such users rarely know how to handle multiple desktops, so that functionality is only in their way, how many would be able to set up a printer, How many would be able to use KMail.\n\nIf we just make some parts of KDE simpler we will end up with a system that nobody wants to use. Not your \"normal\" people, not the experts. The trick must be hit an equal balance between functions and simplicty and apply that throughout the whole system.\n\n\n\n"
    author: "Uno Engborg"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-11
    body: "Right. But your \"solution\" would mean, that KDE (and the Linux Desktop) will stay forever in it's niche. \"Nobody\" is the vast majority of people, so it's rather needed to make KMail more usable for \"nobody\", which means less shortcuts, \"better\" buttons/toolbars/popupmenus - and an alternative \"expert\" mode.\n\nE.g. the Windows interface model for idiots ..err beginners(!!) with different dialog elements, reachable via <tab>, <tab>, ..., <tab>, <enter> is time consuming, but simple. Even the most stupid guy will get it. In contrast, in KDE it's relatively easy to find a shortcut in one application, that has a different meaning in others."
    author: "Carlo"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-11
    body: "@Uno: Um, sorry, should have read the last two sentences too before replying. :|"
    author: "Carlo"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2007-06-25
    body: "In 2007, I still believe toolbar configurations should be saved with profiles.\nIf not, very confusing spending hours playing with (complex) toolbars, to finally discover it is not possible to have one real \"file manager\" profile, and another \"Web browser\" profile.\nStill to be investigated......\nRgds.\n"
    author: "diorser"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "no, not in CVS yet. but i think there's a good chance there will be a commit soon."
    author: "Aaron J. Seigo"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "I didn't know it. I use KDE 3.3.2. Anyway I think you can get the idea."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Make KDE have 2 modes: basic and advanced"
    date: 2005-02-09
    body: "> I think the best compromise would be making KDE have 2 working modes: \n> basic and advanced.\n\nThis is a bad compromise and this proposal has been discussed often in the past.  You are maybe an experienced user for some applications, but for many\nother you can just be a total beginner.  It is just not possible to determine\nfor each and every application whether you're an experienced user or a beginner.\n\n"
    author: "ac"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-12
    body: "Plenty of people use development tools but don't use Cervisia. Even more, though, some people use Cervisia and as such need it installed but just don't want it in their toolbar.\n\nThe toolbars need to be support more controlled configuration."
    author: "jameth"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "Yes, I think these are great. and we won't miss the 'googlebar', if the konqi bar itself is set up to search on nonsense-url's.\n\nmaybe a 'googlebar' would be usefull, if it would search in *the current page*, be it files (narowing the view to matching files and maps) or a webpage (highlighting the searchterm)."
    author: "superstoned"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "the konqi bar itself is setup to search on nonsense-urls by default since the stone ages (KDE 2.x). It can be turned off if you do not want it to do that in the web shortcuts dialog box..."
    author: "Dawit A."
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "I think many people WOULD miss the googlebar. I know that konqueror can search through the location bar. Most people don't. It's not obvious. "
    author: "anon"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "About the Google bar, I haven't added it in the screenshot, but I think it could perfectly be there. We should try to see if the people uses it and then put it."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-09
    body: "Concerning the usability of the Googlebar\n\nBasically it is \"I can do that do\" programming with no thought to usability.\n\n1) Redundant functionality: The location bar does (almost) the same function.\n2) Clicking on the 'G' to get a drop down is not self-evident. \n3) No direct indication changing the default search engine is how you change the Googlebar's search engine.\n4) No option for multiple searches, which users will be expecting.\n5) No easy way to get rid of it.\n\n\nMy suggestion dump the combo box and turn it into a drop down toolbar button, using the location bar to enter data. Icon indicates default search engine. Tooltip indicates purpose.\n\nOptional functionality:\n - Hightlight this page (Google's multi-color highlighting) \n - Remembers last search engine selected. (need config option)\n - Context searches (e.g., search current directory for files)\n\nAlso on the web shortcut dialog add two checkbox columns.\nColumn 1 \"Add to Search button\"\nColumn 2 \"Add to Right Click Search\"\n\nColumn 1 is how the above toolbar button is configured\n Note: around six popular engines should be pre-selected.\nColumn 2 is to improve searching functionality directly from a web page.\n\nPros\n - No change to present location search functionality\n - One location to enter search data\n - Can use shortcuts or search button on same data\n - Drop down becomes \"memory aid\" for different searches\n - Easy access to multiple search engines.\n - Brings awareness to the fact web shortcuts even exist. (The hidden functionality of KDE)\n - Being a toolbar button it can be easily removed.\n\nCon\n - Unlike another idea (a \"Search\" button) it is not immediately self-evident.\n - It's \"different\" therefore moves people outside their comfort zone.\n\nComments?"
    author: "Phoenix"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "You shouldn't put the \"up\" button in Konqueror in the top-left because people confuse it (in my experience) for the \"back\" button, which most browsers place there, and then panic by its strange behaviour. Other interfaces might not be perfect either, but you should design interfaces by taking a user's previous experiences into account and having the \"up\" button there is guarenteed to confuse a big percentage of users."
    author: "Jonathan"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "Actually the \"up\" button should be hidden in the webbrowser-profile. Nobody really needs to go \"one up\" on a webserver and you can still do that by editing the path yourself."
    author: "mETz"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "Yes, you *do* need it. Try it here on dot.kde.org."
    author: "ac"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "Wow, one person in a thousand understands a confusing button and can use it on a few websites. Yeah, please never consider joining the usability team.\n\nThank you, schmuckie."
    author: "Joe"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "> Wow, one person in a thousand understands a confusing button and can use it on a few websites\n\nOh wow, so you asked a thousand persons if they understand the meaning of the up button. I'm pressed. Or are those numbers coming out of your a**.\n"
    author: "ac"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "And yet I find I use it probably more than I do \"forward\"."
    author: "teatime"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "Then add it yourself. You are one in a thousand."
    author: "Joe"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "Can't you even count?  He's two in a thousand and I'm three in a thousand.\n\nI'm starting to think you're the one in a thousand.  lolkthxbye"
    author: "ca"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-11
    body: "I'd guess I'm four then. And add in the fact that someting like 6-8 out of 10 KDE developers use it, and it's one of the useful power features making Konqueror unique among browsers. If you want to remove something from the browser profile, take the useless home button."
    author: "Morty"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "/me advocates just moving it to the right of the back/forward buttons, rather than the left."
    author: "Illissius"
  - subject: "Re: Usability in Konqueror and KGet"
    date: 2005-02-10
    body: "Don't worry about KGet... Kde 4 will see a completely rewritten and very usable kget. I am working on this...\n\nBye!\n\tDario"
    author: "Dario Massarin"
  - subject: "User monitoring"
    date: 2005-02-09
    body: "I would suggest talkback versions of software which log how an application is actually used and then submit the date from x month to a server for analysis."
    author: "Jakob"
  - subject: "OpenUsability needs PHP devs!"
    date: 2005-02-09
    body: "btw, while talking with Jan and Ellen earlier today they mentioned that OpenUsability.org really needs a PHP devel or two to help out with some customizations of the gforge software. if you're a web developer who'd like to help out Open Source, KDE and usability in general, here's a great way to do that! =)"
    author: "Aaron J. Seigo"
  - subject: "I'd like KDE to look more like the GUI in SimCity4"
    date: 2005-02-09
    body: "I'd like KDE to look more like the GUI in Sim City 4. Yes I do!!! It's artistic, funky, cool and pleasant to look at.\n\nKDE apps need a lot of cruft trimming from them. I know I'll get flamed to use GNOME, but there really are too many options, menu items and other wonderful stuff in a lot of apps. It's too confusing, makes it difficult to use IMO.\n\nHopefully these usability people will tackle the tough issues as well as the simple ones."
    author: "Mike"
  - subject: "Re: I'd like KDE to look more like the GUI in SimC"
    date: 2005-02-09
    body: "> It's too confusing, makes it difficult to use IMO.\n\nThanks for making me feel smart."
    author: "Anonymous"
  - subject: "Re: I'd like KDE to look more like the GUI in SimCity4"
    date: 2005-02-10
    body: ">I'd like KDE to look more like the GUI in Sim City 4. Yes I do!!! It's artistic, funky, cool and pleasant to look at\n\nSince it's not very unlikely most developers don't have a clue how Sim City 4 looks like this may be a problem. Why don't you have a go yourself.\nPerhaps starting with the window secoration\nhttp://www.usermode.org/docs/kwintheme.html\n\nOr if you make some real nice mockups, perhaps you could recruit some developers over at KDE-Look. "
    author: "Morty"
  - subject: "Re: I'd like KDE to look more like the GUI in SimCity4"
    date: 2005-02-10
    body: "\"there really are too many options, menu items and other wonderful stuff in a lot of apps.\"\n\nWhen I first heard about Windows XP (they had the name wrong at the time, instead of 'Experience', I heard \"Experienced\") I thought they were going to make an OS that is more configurable, designed for people that want to be able to configure EVERY thing in the OS.  When they released XP and I first saw it I thought \"Wow!\", simply cause it looked like you could actually skin the windows, even if it was a horribly skin (luna).  After using it for about 10 seconds I realized it wasn't any more configurable than all the previous version.  At the time I had never even seen a computer that wasn't running either Windows, Dos, or Mac OS.\n\nWhen I started using Linux (I switched almost completely about 2 years ago in september) I used KDE.  I <b>LOVED</b> how I could configure everything (even though I don't change about 99% of the options).  Every time they release a new version of KDE I always like to go through the control center and look at EVERY option thats changed (its also a good way to see how KDE has changed in the new version).\n\nAs I've been using KDE for longer and longer I've slowly been changing more options which make me work faster.  Like about 1 1/2 months ago I decided I should try to use tabs.  Now I use tabs ALL the time.  I never open a second Konqueror window unless I have more than about 10 tabs open in the first.  Today I set Kopete's main window to be shaded and always on top, since I have a panel at the top right thats always on top (KNewsTicker :-D), I have a line of space about the size of a window's top border that is always empty unless.  Now I just put my mouse in the top left area of the screen and Kopete's contact list open up, it save a good bit of space and makes it so I can get quick access to it."
    author: "Corbin"
  - subject: "Re: I'd like KDE to look more like the GUI in SimCity4"
    date: 2005-02-12
    body: "> I'd like KDE to look more like the GUI in Sim City 4. \n\nwon't happen. we're talking \"usable\" not \"appearing in the next William Shatner movie\"\n \n> KDE apps need a lot of cruft trimming from them.\n\nyes. we're working on it."
    author: "Aaron J. Seigo"
  - subject: "organization through disorganization"
    date: 2005-02-10
    body: "I am looking forward to KDE 3.4... kind of.  I am not new to KDE... I am use to the disorganized \"start menu\", the copy/paste toolbar options when you are surfing the internet with konqueror, the bloated applications...\n\nMost of the problems in my opinion are with the actual software written for KDE... most notibly Konqueror. sigh.\n\nI just wish the usability people would think WWOSXD.. What Would OS X Do?\n\nI'd just hate to see KDE ending up being the Jack of all trades, and the King of none.\nOr worse off, KDE becoming the King of a few trades... but each one being bloated to beat hell."
    author: "Brandito"
  - subject: "Re: organization through disorganization"
    date: 2005-02-24
    body: "Regarding \"What would OS/X do\", my personal view is \"too severely limit choices and flexibility in the name of limited usability improvements.\"\n\nMacOS/X needs \"advanced\" buttons so badly it's just excruciating. So many of its most annoying features simply cannot be disabled (long delay on dock hide/show, window animations, etc) without unsupported 3rd party tools.\n\nI think there's a reasonable approach to usability that retains configurability without bombarding the user with choices most won't care about. Providing simple dialogs that can be expanded to show more options is one of the most obvious approaches.\n\nI share your experience with KDE, especially regarding Konqueror (which I just can't handle), but I diagree with your preferred approach to solving it."
    author: "Craig Ringer"
  - subject: "Where are the usability improvements?"
    date: 2005-02-10
    body: "I use beta1, but I can't see any improvements since KDE2.\nNearly all usability problems of KDE are very old. \n\nNot only the apps have usability problems also the homepage.\nI wanted to know what has changed between beta1 and beta2 and tried these pages:\nhttp://www.kde.org/announcements/announce-3.4beta2.php\nonly 10% of this page are about beta2, but no detailed info.\n\nhttp://www.kde.org/info/3.4beta2.php\nonly downloadlinks."
    author: "Em"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-10
    body: "did you mean: \"a number of the issues i've noticed in KDE since the KDE2 days remain.\" because there have been improvements.\n\n<off topic>\nthis is a typical example of the sort of things that makes it really tempting for developers to say, \"i don't want to deal with users anymore.\" hyperbole, unwillingness to grant any credit .... why would someone want to listen to that? it's discouraging!\n\nin the IRC chat, you'll see that some of the usability improvements were mentioned (and even linked to so you can see them in your web browser!) and that a lot of the discussion was towards KDE4. but you seem to have missed that. the result is that some developers begin to question why they should bother speaking to users.\n\nas a user of open source software you have the rather unusual situation of being able to communicate directly with the people creating this mass-market software. in fact, that is such a cool thing that companies such as Sun and Microsoft are trying to imitate it with their developer blogs. but when people treat that communication channel carelessly, well ... it becomes hard to keep it open and productive.\n\nIMHO, communication between users and developers (either directly or through an intermediary) is vital to achieving the best software possible for a whole bunch of reasons. maybe take a moment to consider how the people on the receiving end of your communication might react to what's written. we're all part of a big team here, including the users, and better things happen when we work together =)\n</rant>\n\n(i went from <offtopic> to <rant> in 4 short paragraphs? yikes!)"
    author: "Aaron J. Seigo"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-11
    body: "Every time someone makes a more general complaint about a free program developers start lamenting that users are ungrateful and things like that.\n\nUsers have two possibilities when they find a problem:\nThey can complain, or they can use another program.\nIt works this way with open source software, shareware or commercial software.\n\nI complain, because I want that KDE has the best, and easiest to use programs.\n\n\n"
    author: "Em"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-12
    body: "users complain to me about all sorts of things all the time, no big deal and it's often how i notice a problem spot to begin with. but there's a difference between complaining and being ungratefully rude. a big difference. \n\nmoreover, i'd challenge the idea that the only resort for a user is to complain. how about \"engage in conversation\" as one obvious example? it's fairly easy to discuss things with someone when they are being reasonable and are open to exploration, even if the general topic is \"your code sucks, dude!\"."
    author: "Aaron J. Seigo"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-10
    body: "I don't know why I answer you, because you're clearly a troll. Anyways...\n\n>I use beta1, but I can't see any improvements since KDE2.\n>Nearly all usability problems of KDE are very old. \n\nYou're kidding right? Since the usability studies by Relevantive AG a lot has happened in KDE:\n\n- http://www.kdedevelopers.org/node/view/809\n- http://lists.kde.org/?l=kde-cvs&m=110701903725477&w=2\n- http://lists.kde.org/?l=kde-cvs&m=102115386904118&w=2\n- http://lists.kde.org/?l=kde-cvs&m=108541070113786&w=2\n\nJust to name a few.\n\n> Not only the apps have usability problems also the homepage.\n\nCalling the non-existant changelog a usability problem shows that you know this >< much about it.\n\nBTW do you prefer that the developer fix the bugs and implement your wishes? Or do you prefer that they write changelogs?\n\nHow about you sit down, go through the cvs digest and write one?\n"
    author: "Christian Loose"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-11
    body: "> lot has happened in KDE:\nSorry, but these are only minor improvements.\n\n> shows that you know this >< much about it.\n\nUsability is about effectiveness and efficiency.\n\nEfficiency means in this case how much effort does it require to find out what has changed.\n\nefficient way :  changelog\ninefficient way : go through the cvs digest, or download, compile and find out. "
    author: "Em"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-12
    body: ">Usability is about effectiveness and efficiency.\n\nYou make a real valid statement here, sadly none of your other writing reflects this. But it makes your claim about only minimal improvements since KDE 2 fall flat on it's face. The increased effectiveness and efficiency in the improvements to in KHTML alone, makes your statements false. Usability is much more than reducing the number of icons in toolbars, which for the most part are no more than cosmetics."
    author: "Morty"
  - subject: "Re: Where are the usability improvements?"
    date: 2005-02-12
    body: "Why, as a normal user, do you want to know what has changed to begin with? You shouldn't notice any difference unless for the better in which case the change sjould feel \"natural\". And usability is not about propaganda last time I checked its definition. So what the heck does your crap about missing a changelog but not being willing to check CVS changes (which is the only way you see all actual changes) have to do with usability?"
    author: "ac"
  - subject: "We Know. "
    date: 2005-02-10
    body: "\"On the other side, usability people know very little about Open Source\"\n\nThe \"they haven't discovered the joys of Open Source\" belief. That's one possibility. Another possibility is that we usability people know plenty about Open Source.\n\nWe usability people know that Open Source is pretty much dominated by a unix programmer culture that refuses to change the way it does things, constantly undermining itself by a \"backend-now, pasted-on frontend later\" mentality. \n\nWe usability people know that for years Open Source has long considered the field of usability to be BS; we don't have enough fingers to count the number of times we've heard prominant kernel hackers say about usability people \"I can't believe that some people actually get paid to criticize the work of others\". \n\nWe usability people know that if we contribute at all to any Open Source project, we will always be doing \"damage control\" for projects that already exist and will be limited in the good we can do because so much code has already been written. We know that it is pointless to try to explain to unix hackers the need to design user interface and work out user interaction at the beginning of the development process before any code is written, as this seems to violate unix hackers' core belief systems. \n\nWe usability people know that our contributions will be considered less valuable and less important than technical contributions like code. We know that if we make such contributions, we will be constantly second-guessed in our decisions by unix hacker types who know nothing about our field. \n\nWe usability people know that Open Source has continously demeaned and belittled end-users, the people who we want want to help, by attributing their frustration due to usability problems as them \"not wanting to learn\" or \"just being used Windows\". We've seen 30 years of the unix culture's glorification of things that are difficult. And we've seen 10 years of Open Source saying \"linux being hard to use is just a myth\". \n\nWe usability people know that if we confront Open Source with its history of shoddy usability after hearing the above statement, we will be accused of \"criticizing the work of volunteers\" and will be told \"quit whining about what you get for free\". We know that we will be told to \"go code it yourself\". And we know that the second we turn our back to walk away in disgust, the same exact people who just told us all these things will say that Linux is perfectly ready for your grandmother to use, has been so for years, and any statement to the contrary is Microsoft FUD. \n\nWe usability people know that even if we did have the skills to \"go code it ourselves\", and we forked Open Source software and made changes that improve usability, it would do no good. We know that Open Source's software announcement site, Freshmeat, would declare our work as \"merely a patch\" because they consider UI changes to be less significant than code changes. Our modified version of the software would be suppressed and would never see the light of day. Meanwhile, hundreds of distributions of linux based on a few changes to the kernel code would be accepted, announced, and embraced. We know that the old Open Source double standard of code being more significant than UI still holds, and even if Open Source tells us \"go code a better one yourself\", it's not like they actually mean it. \n\nWe know that Open Source's for-profit companies will spend billions on buyouts of technical companies that make things like compilers but will spend little or nothing for a dedicated usability department. \n\nI don't think that the problem is that we usability people know little about Open Source; I think the problem is that we know too much. Call me a troll if you want, but to say \"usability people aren't participating because they don't know anything about Open Source\" is ridiculous. \n\nErgonomica Auctorita Illico!"
    author: "Ilan Volow"
  - subject: "Re: We Know. "
    date: 2005-02-10
    body: "It doesn\u00b4t help that you are the most grandiloquent big-headed wannabe I have ever read. Go hack on \"directory free kde\". Got anything other than a search and replace done already?\n\nPerhaps usability people are cool. You aren\u00b4t.\n\nMore about this guy at his page. Including him speaking of himself on the third person, and his \"software\": \n\nhttp://ilan.clarux.com/software.html"
    author: "Roberto Alsina"
  - subject: "Re: We Know. "
    date: 2005-02-11
    body: "neat. a fork of KDE over a single word ... \"Directory\" vs \"Folder\" ... a change that KDE also made, so one can just grab a regular KDE release instead of this fork."
    author: "Aaron J. Seigo"
  - subject: "Re: We Know. "
    date: 2005-02-15
    body: "As usual, AJS missed the point.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: We Know. "
    date: 2005-02-10
    body: "Go take a look at the work done by people like Aaron Seigo (http://aseigo.blogspot.com/). He's rewritten some of KDE's usability disasters to be really quite nice (Kicker comes to mind). KDE devs are open to changes for usability's sake, just not changes for changes sake."
    author: "Simon Farnsworth"
  - subject: "Re: We Know. "
    date: 2005-02-11
    body: "heh. you disagree with a usability professional who (co-)runs their own successful usability company when they say that most usability people know very little about open source ... but then go on to prove them absolutely correct.\n\nyour notions of open source and the receptivity to usability efforts are dated. while your observations were rather accurate at the turn of the century, a lot has changed since then. you'll still find the odd antiusability open source developer out there, but in desktop projects they are no longer the norm. \n\nso i'd like to suggest that it's time to get those chips of our shoulders and recognize that there is the opportunity and the desire within the open source community to make things better. this is why so much successful usability effort is to be seen these days across the pantheon of open source desktop software."
    author: "Aaron J. Seigo"
  - subject: "Hacker mentality"
    date: 2005-02-24
    body: "To an extent, I can see where you're coming from - some folks are quite hostile to usability related comments and suggestions. My experience suggests that they're in the majority, and most people care about these things.\n\nHowever, I think that if you approach the issue poorly and with no diplomacy, you won't get a receptive response. Your post does not suggest that you'd be inclined toward dipomacy or tact.\n\nI also see a serious disconnect between a common usability view of \"remove all features not used by 90% of the user base\" and the hacker \"make it do everything\" view. There is a middle ground, but few seem willing to work toward it - and the fault lies on BOTH sides.\n\nSoftware can be provided with an interface that makes the common things easy to find, and the less common things availible if the user actually wants them. You don't have to take all the flexibility out, only place it better.\n\nI'm currently a developer on a project that is making active usability improvements as part of its normal development process. My experience so far is that this is the norm, rather than the exception."
    author: "Craig Ringer"
  - subject: "Fantastic"
    date: 2005-02-12
    body: "Usability is KDE's single greatest weakness, great to see it is being taken more seriously now!\n\nThough I was hoping KDE 4 will go by the new KHIG, not just have one ready."
    author: "Matt"
  - subject: "Re: Fantastic"
    date: 2005-02-15
    body: "> Though I was hoping KDE 4 will go by the new KHIG, not just have one ready\n\nrejoice, it will be going by the new HIG. there's little point in having a HIG unless it's deployed, and that is our goal for KDE 4.0 =)"
    author: "Aaron J. Seigo"
---
Recently, our very own <a href="http://www.kde.nl/people/fabrice.html">Fabrice Mous</a> asked if I might write an article about usability and KDE development. At first I was hesitant, and not just because I have a lot more hacking to get done before KDE 3.4 is released (which is soon). I often get asked about usability and the Open Source process, and even I sometimes get tired of having the <a href="http://news.zdnet.co.uk/software/linuxunix/0,39020390,39187111,00.htm">same old conversations</a> over and over. I thought that this time it would be refreshing to ask someone <i>else</i> these questions and see what <i>they</i> had to say. So I arranged to meet up with several people on IRC who are involved in software usability and the KDE project. Here's what ensued...


<!--break-->
<style type="text/css">
.aseigoname { font-weight: bold; color: #000077; }
.aseigo { color: #000077; }
.janname { font-weight: bold; color: #01776D; }
.jan { color: #01776D; }
.ellenname { font-weight: bold; color: #670077; }
.ellen { color: #670077; }
.petername { font-weight: bold; color: #770049; }
.peter { color: #770049; }
.florianname { font-weight: bold; color: #017773; }
.florian { color: #017773; }
.jorgname { font-weight: bold; color: #772002; }
.jorg { color: #772002; }

.firesideheader { margin-top: 1em; }
</style>

<table>
<tr><td colspan=2><h3 class="firesideheader">Introductions...</h3></td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Hi everyone! Let's start with some introductions, including your name and what you do with regards to usability and KDE.</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">I'm Ellen Reitmayr. and together with Jan M&uuml;hling I'm one of the maintainers of the new KDE Human Interface Guidelines</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">And you run a usability firm in Germany, correct?</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Yes, we run a <a href="http://www.relevantive.de">usability firm</a> in Germany. mostly, we do commercial stuff, like web, software, mobile, but if we have time, we spend it on Open Source projects, and mainly on <a href="http://www.kde.org">KDE</a>.</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">We also run a project called <a href="http://www.openusability.org">OpenUsability</a> where Open Source projects can host their software in order to get usability advice. There are a number of KDE projects hosted on <a href="http://www.openusability.org">OpenUsability</a>, such as <a href="http://pim.kde.org">KDE PIM</a> and <a href="http://koffice.kde.org/kivio/">Kivio</a>.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Aproximately how many projects in total are currently active on <a href="http://www.openusability.org">OpenUsability</a>, and how many usability specialists are involved?</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">There are around 20 projects at the moment and perhaps 6 or 8 usability people.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Such as yourselves ...</td></tr>

<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Yes. This weekend, <a href="http://www.kdevelop.org">KDevelop</a> also joined <a href="http://www.openusability.org">OpenUsability</a>! 8-)</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Wow, that's a good start indeed! Who else is with us today?</td></tr>
<tr><td class="petername" valign="top">Peter:</td><td class="peter">I'm Peter Simonsson, and I develop <a href="http://koffice.kde.org/kivio/">Kivio</a> (one of the applications working with <a href="http://www.openusability.org">OpenUsability</a>) and <a href="http://www.konversation.org/">Konversation</a>. As for the usability part I mostly implement what the others tell me is good. =)</td></tr>
<tr><td class="jorgname" valign="top">J&ouml;rg</td><td class="jorg">I'm Joerg Hoh, and I hang around on the #kde-usability IRC channel, helped organize <a href="http://conference2004.kde.org/">aKademy</a> and am active on various KDE email lists.</td></tr>
<tr><td class="florianname" valign="top">Florian:</td><td class="florian">I'm Florian, and I have been working for <a href="www.relevantive.de">Relevantive</a> for almost half a year and thus got in kontact with Jan, Ellen and <a href="http://www.kde.org">KDE</a></td></tr>

<tr><td colspan=2><h3 class="firesideheader"><a href="http://www.openusability.org/">OpenUsability.org</a></h3></td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">It's funny you should write &quot;in kontact&quot;, Florian, as a lot of usability work has been happening in the <a href="http://pim.kde.org">KDE PIM project</a>, and i have to say that it's really starting to show in the quality of the user interface. Perhaps you could relate how it's been working out with the <a href="www.kontact.org">Kontact</a> developers during the KDE 3.4 development cycle?</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Yes, for the KDE 3.4 release, Jan and Florian cleaned up <a href="http://www.kdedevelopers.org/node/view/845">some context menus, several of the dialogs</a> and there is a <a href="http://www.kdedevelopers.org/node/view/809">new recipient area in the composer</a>.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">The feedback on <a href="http://www.openusability.org">OpenUsability</a> shows that there is a strong interest in usability in KDE, and especially within the <a href="http://pim.kde.org">KDE PIM</a> project</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">How does the feedback cycle work between the usability professionals on <a href="http://www.openusability.org">OpenUsability</a> and the <a href="http://pim.kde.org">KDE PIM</a> developers?</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Well, we started doing some usability inspections of individual components. Based on that, we made suggestions on how to improve the usability of certain components, especially <a href="http://kmail.kde.org/">KMail</a>. Of course, those suggestions have to be discussed - that's what we have the <a href="https://mail.kde.org/mailman/listinfo/kdepim-usability">kdepim-usability mailing list</a> for.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">What form do these suggestions take? Verbal descriptions, mock ups, diagrams? All of the above?</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Hm, that's still a problem. We mostly upload them to <a href="http://www.openusability.org">OpenUsability</a> in PDF format, but we found that this format does not support discussions in a satisfying manner. The developers asked us to use plain text instead. But this does not support including mock-ups or screenshots. Therefore, we are working on an XML report format. The first version was released in October.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Florian is also developing a tool for card sorting, which is essential for getting proper menu names, hierachies and so on. He will make it available for use on <a href="http://www.openusability.org">OpenUsability</a>.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">What exaclty is &quot;card sorting&quot;?</td></tr>
<tr><td class="florianname" valign="top">Florian:</td><td class="florian">Card sorting is a means to gather information on where people expect menu entries in software, or navigation entries on websites.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">And how does it work exactly?</td></tr>
<tr><td class="florianname" valign="top">Florian:</td><td class="florian">People are presented with cards that each have a term on them and a set of group names into which these cards should be sorted. You do this with several times with different people and, after some clustering, you get a pretty good impression about where people expect things.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">I see. So <a href="http://www.openusability.org">OpenUsability</a> is not just engaging usability specialists and software developers in Open Source, it's actually creating tools and techniques that haven't previously existed, but which are sorely needed to do this work ...</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Yes, but as we lack developers for <a href="http://www.openusability.org">OpenUsability</a>, there is still a lot of work to do. However, we are making progress, I think. :)</td></tr>

<tr><td colspan=2><h3 class="firesideheader">Made For Each Other</h3></td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">The basic idea of <a href="http://www.openusability.org">OpenUsability</a> is to help developers get access to usability resources. These ressources are still lacking in Open Source development, because Open Source is attractive to developers but not yet for usability people. On the other side, usability people know very little about Open Source ... but if both can be brought together, we are very optimistic that a new way of developing Open Source can start. </td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">How does <a href="http://www.openusability.org">OpenUsability</a> help make getting involved with Open Source projects more attractive for usability people?</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">As a maintainer or a developer, you can post your project on <a href="http://www.openusability.org">OpenUsability</a>, thus saying: &quot;I would like to invite you to help me improve the usability of my software.&quot; This is an important sigal, because Open Source is generally  considered to be &quot;rather interested in code&quot;. The next step is to find usability people to care for the usability part of Open Source. It seems to work ... there are more and more projects joining, and there are more usability people wanting to contribute</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Peter, as a developer, perhaps you could share some perspective from the other side of the fence here. how does something like <a href="http://www.openusability.org">OpenUsability</a> change or alter the development process for you, and what sort of obstacles or challenges do you commonly see with regards to usability?</td></tr>
<tr><td class="petername" valign="top">Peter:</td><td class="peter">Hmm, well it hasn't changed my development process too much yet... that might be because I treat the usability inspection as a buglist.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">So it works well with the regular pace of development, if it's done during development as opposed to afterwards? This seems to be a common concern amongst developers that i speak with.. that it means huge changes and inconveniences in development</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">It does not necessarily have to change the development process. As a matter of fact, Open Source development is perfectly suited for integrating usability engineering principles: open communication, direct communication, &quot;release early and often&quot; (aka, rapid prototyping) ...</td></tr>
<tr><td class="petername" valign="top">Peter:</td><td class="peter">Yes, I don't see that any major changes have to be made to the development process.</td></tr>

<tr><td colspan=2><h3 class="firesideheader">Human Interface Guidelines (HIG)</h3></td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Earlier we talked about some tools coming out of <a href="http://www.openusability.org">OpenUsability</a>... Along those lines, Ellen, you mentioned that you're one of the maintainers for a new set of Human Interface Guidelines (or HIG) for KDE. Now, KDE has had a fairly <a href="http://developer.kde.org/documentation/standards/kde/style/basics/">basic set of guidelines</a> for quite some time...</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">... yes, but there was a usability meeting at <a href="http://conference2004.kde.org/">aKademy</a> in August 2004 where we decided that a new set of guidelines is needed.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Why was that, and what will this new set of guidelines offer?</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">One reason was that we found the former guidelines lacked a number of topics as well as detailed instructions.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Since KDE still does not have enough usability resources, a HIG is a very good means to get a usage style and a consistency of usage. By defining rules and guidelines it can be assured that many usability issues are recognized and solved while doing the GUIs in the initial phase. Also, we can make developers more aware of usability issues, so that they can decide when user feedback is needed, for example.</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">The goal of the new guidelines is that developers will know exactly how to implement a certain interface.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Catching things early is certainly important as it saves a lot of repeated efforts...</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Right, and it does not hurt much because in the beginning you can change things more easily without as much effort. The HIG will also be a sign to the world that KDE takes usability serious</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">On the one hand, it means that more examples are required, and on the other hand we need quick references. I think one of the major problems of the <a href="http://developer.gnome.org/projects/gup/hig/">GNOME HIG</a> is that it is too excessive and therefore not readable.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Yes, that's certainly an issue when it comes to Open Source developers, many of whom are doing this in their own spare time. Being able to quickly get answers and solutions is critical. I don't think we can expect developers to read a novel just to learn how to create a dialog properly.</td></tr>
<tr><td class="petername" valign="top">Peter:</td><td class="peter">I know I certainly wouldn't want to do that :)</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Yes, I think it is utopian to hope that developers will read a 500 page document. Therefore, the first thing we did was to come up with a format that allows the developer to scan important information, but also offers additional topics such as implementation notes, cross references and examples.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Cool! It does seem odd to me that even though we have the tools to create navigationally rich content, most of the HIGs out there are massive, linear tomes. It's exciting to see these sorts of innovations occuring!</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Well, we might do a usabillity test with the usability guidelines in order to ensure they actually can be used in the proper way :)</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">That makes perfect sense. Almost like an Escher drawing =)</td></tr>
<tr><td class="jorgname" valign="top">J&ouml;rg:</td><td class="jorg">I think good examples in the HIG are going to be crucial.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Why is that?</td></tr>
<tr><td class="jorgname" valign="top">J&ouml;rg:</td><td class="jorg">Because then you can do a quick look and find out &quot;how is this accomplished there&quot;. If you ask "Why should I do this?" then you really should read the styleguide (and often more literature :-)</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Knowing that many, if not most developers, will take the "copy and paste" route when it's available, it's easy to see how this approach will be valuable.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">One main idea of the new HIG is to have a situation based structure: I am now needing this and that GUI, what should i consider?</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Now as i understand it, the target timeline is to have the HIG ready for the KDE 4.0 release, correct?</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Yes, that's what we are heading for.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Right, it's a huge project, but this is realistic</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">It's still a ways off, which is good as there's likely a lot of work to do by the developers and usability people alike once the HIG has gone through draft revisions and is ready for Prime Time.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">... and we now have the infrastructure to get all the contributors in the boat.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">At what point will contributors need to start "getting on the boat", to borrow your phrase? Once there is a draft available, or are there things KDE developers can start doing now?</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan"><a href="http://www.kde.org/areas/guidelines/html/">The &quot;real version&quot; is living in cvs</a>, but there are now some colaberation tools like mailing lists and a wiki. Many guidelines must be discussed, since it touches KDE very much. Also, there must be some testing done on these guidelines, to be sure that they actually help improve usability. KDE is a community, so we don't want to write something in the chamber, but together.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">And these discussions will take place on kde-usability-devel and kde-core-devel as necessary, I imagine?</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Yes, in fact we are just about the point where we can start these discussions. The only thing will be to keep these discussions fruitful.</td></tr>

<tr><td colspan=2><h3 class="firesideheader">Looking Forward...</h3></td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">I can't wait! Looking at Kontact in 3.4, it hasn't lost any functionality which seems to be a common worry for many KDE users and developers when speaking of usability. It has certainly gotten a lot more streamlined and easier to use, though. As we look to apply these same sorts of processes to more of KDE leading up to the 4.0 release in concert with the new HIG ....</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">With regards to Kontact and features, this is an eternal fight. But loads of functions does not necessarily lead to poor usability ...</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Ok, so let's put on our dreamer's glasses and speak of the future yet unseen. What sorts of developments, advancements and improvements do you expect to see, or want to see?</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">If KDE wants to apply usability, there is a way ... a rather easy way ... all we need is some initial examples of how it works, some positive stories, some developers who like to see their baby more usable, and then things will come.</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">First of all, I'd like to see all of KDE more streamlined. There are still too many concepts confused with each other.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Too many concepts confused with each other ... what do you mean by that?</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">Different applications follow different usage concepts which causes inconsistency. A very simple example is the menu structure in Kontact. It was made up of a collection of individual applications, and each of them has slightly a different menu structure. For instance, there isn't a 'Find Contact' option in KAddressbook, but there is a 'Find' option in KMail.</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">That makes lots of sense. Hopefully we'll see more global consistency in 4.0.</td></tr>
<tr><td class="jorgname" valign="top">J&ouml;rg:</td><td class="jorg">We also should make the public make more aware of the topic of usability so everyone is looking forward to KDE 4. Then people will know &quot;they take usability serious, I can file bug reports and they will get resolved.&quot;</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">Also, companies or public institutions need usable software. If they hear that KDE 4 is getting <i>very</i> usable, this may be one more reason to decide for KDE.</td></tr>
<tr><td class="ellenname" valign="top">Ellen:</td><td class="ellen">We are working on it 8-)</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">So in the end everyone wins. KDE developers get help with usability, existing KDE users get a better experience and the KDE user base grows.</td></tr>
<tr><td class="petername" valign="top">Peter:</td><td class="peter">Sure hope so :)</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">I know that a lot of people are already looking forward to seeing advancements in the control center and Konqueror usabilty in 4.0. As a closing question, do you think that innovation in usability is realistic to expect from an open source project such as KDE?</td></tr>
<tr><td class="petername" valign="top">Peter:</td><td class="peter">I don't see why it shouldn't be possible.</td></tr>
<tr><td class="janname" valign="top">Jan:</td><td class="jan">I love to preach that Open Source is perfectly suited for innovation, especially with respect to usability. If you get developers convinced, you can change everything. Also, things can change much quicker than in traditional software development. I think that especially KDE has the perfect basic settings for usability. It's just a matter of will and of time. And it's fun ... ;-)</td></tr>
<tr><td class="aseigoname" valign="top">Aaron:</td><td class="aseigo">Great... thanks everyone for the great discussion! See you soon on <a href="http://www.openusability.org">OpenUsability</a>!</td></tr>
</table>





