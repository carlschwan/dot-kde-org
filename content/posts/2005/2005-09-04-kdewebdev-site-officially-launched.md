---
title: "KDEWebDev Site Officially Launched"
date:    2005-09-04
authors:
  - "elaffoon"
slug:    kdewebdev-site-officially-launched
comments:
  - subject: "Schedule in Malaga"
    date: 2005-09-04
    body: "Andras will be leaving Malaga Tuesday afternoon and I will be leaving Wednesday morning. This will give us time to spend together working on our plans for Quanta. I'm not sure if we will be able to get internet access but it looks possible. Currently I owe Andras more money than is in my Paypal account, but if anyone wants to help out I can withdraw money and pay him directly and save substantially over our costs to transfer funds to him in Romania. I hope everyone enjoyed the news from aKademy. I believe that our being here makes a huge difference in what we can do making Quanta and Kommander better. If I don't post soon, many thanks to all our supporters."
    author: "Eric Laffoon"
  - subject: "Re: Schedule in Malaga"
    date: 2005-09-05
    body: "Great. It is sad that funding developers is a problem. I remember the flash replacement guy, did he continue?"
    author: "Bert"
  - subject: "Re: Schedule in Malaga"
    date: 2005-09-05
    body: "Funding is and isn't a problem. Our users have been very generous in the past, but it is a hassle scheduling fund raising drives several times a year and I should have done one earlier. Right now at the end of aKademy we're competing with a lot of other news. I will have some other good things to announce soon and unfortunately it looks like I will need to mention our funding shortfall then too. At least it's good because people will be getting something new they can be excited about, and I expect people to be very excited about what is coming up in the next week or two.\n\nI wish I could give a better report for the Flash/SVG guy. Unfortunately it was not possible to put something together with him. I feel a great responsibility when I work between the community and a sponsored developer. I really have to make sure that both are getting what they expect and that it's good. in this case the developer was excited about being part of KDE... He just wasn't motivated to program anything and couldn't deliver an application that built. In fact he did not even get on our developer list and ask questions. He is a nice guy and I don't want to say anything unkind about him. He just didn't have the hungry \"I love to code\" personal make up that Andras and Michal have.\n\nRegrettably even if it did look like he would work out I still don't have the funding. My business is in a growth stage where removing capital now can cost ten times as much in a year. I think I need to look at consulting and setting up a charitable foundation to solicit companies, but these options take a great deal of time and money. If everyone who was using Quanta and saving money over buying a commercial program were to send a small amount and everyone making money with Quanta were to send 1/4 - 1/2 of what they would have spent on commercial software we would be ahead. Our developers would not be running PCs that were several years old and slow compiling. I could add applications like SVG/Flash. Users would be better served. We're not using the money to go drinking and stop programming until we sober up. ;-) We're family guys."
    author: "Eric Laffoon"
  - subject: "Re: Schedule in Malaga"
    date: 2005-09-05
    body: "Was it this one:\nhttp://f4l.sourceforge.net/\n\n\n\"If everyone who was using Quanta and saving money over buying a commercial program were to send a small amount and everyone making money with Quanta were to send 1/4 - 1/2 of what they would have spent on commercial software we would be ahead.\"\n\nGPL is not incompatible with the ASP shareware principle.\n\n"
    author: "gerd"
  - subject: "Re: Schedule in Malaga"
    date: 2005-09-07
    body: "ming.sf.net"
    author: "fennes"
  - subject: "Terrible pattern behind text"
    date: 2005-09-04
    body: "Terrible website, that decoration behind the text should have been switched with the background. Not a website that is very easy to read..."
    author: "boemer"
  - subject: "Re: Terrible pattern behind text"
    date: 2005-09-05
    body: "I agree. You do not put a background like that behind text. Ideally you do not put any background image behind text at all. Unfortunately this is another poor KDE site; the official site looks tacky to me as well. I don't mean to dump on other people's work, and I'm just an anonymous troll, but it doesn't look very good.\n\nP.S. It always helps to provide a bit of background info in a news article (I had no idea what KDEWebDev was until I checked it out)."
    author: "jon"
  - subject: "Re: Terrible pattern behind text"
    date: 2005-09-05
    body: "So if both sites look tacky to you then what do you think looks good? I remember lots of people liking black backgrounds, which happen to be hard to read if your monitor isn't real bright. There are two points to this... First of all whenever anything is discussed in KDE the most attention is given to visuals and eye candy. It's like some people would be as entertained by a lavalamp as a computer or the idea that information and work could be done are minor secondary thoughts. Whatever. Visual style is extremely subjective and utterly fickle. There is absolutely nothing you can do that will not have somebody saying it sucks. I'm just glad somebody likes it. Of course it's probably too much to ask if you looked at the content... who cares about content. My business site, http://kittyhooch.com, gets lots of compliments from cat owners as an awesome site, and it's way better than any other site I've seen for our market. I'm sure I would get a lot of geeks saying it sucks too.\n\nSo what sites have you done that I can look at and learn better style and design from or where is your alternate CSS file? That is really point two. We are going to put up some alternate CSS layouts and make this user selectable. Then again a real web snob would know they could load their own style sheets. ;-)\n\n> P.S. It always helps to provide a bit of background info in a news article (I had no idea what KDEWebDev was until I checked it out).\n\nThat's what we put the link there for. ;-) We've got a lot of users who know or know of Quanta. Of course some distributions didn't pull Quanta out of CVS because it didn't start with a K and the module now has other applications so we tried to make the short name descriptive."
    author: "Eric Laffoon"
  - subject: "Re: Terrible pattern behind text"
    date: 2005-09-05
    body: "I wouldnt call it terrible, thought I would agree with the previous poster that the diagonal lines in the background are distracting and are probably better removed. Id also suggest to remove the dark blue colour behind titles e.g behind \"Report from aKademy\" and rather use a slightly bigger bolder font for headings. Also I suggest to vertically center these headings as the date adjacent to these headings has been done.\n\nKeep up the good work guys!"
    author: "Tim Sutton"
  - subject: "Re: Terrible pattern behind text"
    date: 2005-09-05
    body: "We would like to provide different CSS styles for the site. Anybody wanting to help us is free to do, so if you have a real solution that would make it better, contact us on the links you can find on this site."
    author: "Andras Mantia"
  - subject: "Re: Terrible pattern behind text"
    date: 2005-09-05
    body: "Funny - I had precisely thought the opposite, that it gives a more pleasant look to the site. For me these backgrounds are plain enough not to have distracted me.\n\nOne thing, though, is the lack, imho, of screenshots for most applications. In particular kallery does not provide links to example galleries or screenshots. The textual description only gives a vague idea.\n\nthanks for your work!"
    author: "tendays"
  - subject: "Cool!"
    date: 2005-09-04
    body: "Looks good to me! I haven't used Quanta since before it had VPL but I've got a new project coming up so looking forward to using it again.  And, despite an earlier posters commments, I had no problems reading it."
    author: "David"
  - subject: "Re: Cool!"
    date: 2005-09-05
    body: "I think you will find it's changed a lot. Quanta has so many little features you don't see right away, you may want to join the mailing list if you want to explore it all."
    author: "Eric Laffoon"
  - subject: "multilingual"
    date: 2005-09-05
    body: "there is a huge problem with multilingualism of KDE websites.\n\nderived sites are usually outdated, so it becomes english only which is said.\n\nI would like to see a multilingual content management system. So when there is a say German trasnlation, german users get the translation displayed."
    author: "martin"
  - subject: "Re: multilingual"
    date: 2005-09-05
    body: "There is something like that for the main KDE website AFAIK. I don't know how much it is used though.\n I hope to get (or something similar) that also for our site."
    author: "Andras Mantia"
  - subject: "Re: multilingual"
    date: 2005-09-05
    body: "We explored several multilingual content management systems. We will be moving to a new virtual server soon and moving to PHP5 which will enhance our options for dealing with this. Unfortunately there is still the same problem for us. We have a place for people to subscribe to a developer account for the site and several people have, but now it's pretty much just Andras and myself doing it. I cannot translate to any language and Andras can do Hungarian and Romanian. Probably some other people could do translations and we want to work on insuring we have a good system.\n\nSo that pretty much brings me back to my usual point... You can complain about what others who volunteer their time (or use time paid for by those volunteering their money) or you can get involved. It's built by contributions, so if you are not contributing you could complain that not enough people were contributing, or that those that were contributing were not doing enough... I'm not sure if you were complaining, but you should know on thing. There is no lack of desire on our part to meet your every need. There is a substantial lack of resources though. In fact right now before I deal with anything else I need to deal with raising funds to pay developers for what they have done to date as well as allocate the amount of time I can volunteer on project work.\n\nI hope we can be multilingual, but there is only so much I can do."
    author: "Eric Laffoon"
  - subject: "Re: multilingual cms's"
    date: 2005-10-14
    body: "Multilingual ontent management services worth taking a cue from: <a href=\"http://www.kitsite.com/\">Kitsite</a>, <a href=\"http://www.chipka.com/\">Chipka</a>, <a href=\"http://www.dxm-cms.com/\">DXM CMS</a>."
    author: "John Dewey"
  - subject: "Knowit"
    date: 2005-09-05
    body: "The KDE 3.5 feature list tells that \"knowit\" will be part of the Webdev package. Are there any news on this."
    author: "fennes"
  - subject: "Re: Knowit"
    date: 2005-09-05
    body: "Michal, the knowit author is part of our team, but I'm not sure he has time for it now. But the plan still stands."
    author: "Andras Mantia"
---
The <a href="http://kdewebdev.org">KDEWebDev site</a> is being <a href="http://kdewebdev.org/news.php?newsID=3">formally launched</a>. It's been some time since people started making cracks about the old site, and now after all this work we don't expect to have reached everyone's ideals. This site however is on it's way to being exemplary, and we finally feel it's good enough to announce. We are not done with it either. We also have a news item about <a href="http://kdewebdev.org/news.php?newsID=4">what we're working on at aKademy</a>. Along with exciting steps toward true innovation in supporting developers with superior tools there is also a reminder that we have sponsored developers and we are behind this year in paying them. The small difference of a <a href="http://kdewebdev.org/donate.php">donation</a> from a few users adds up to big differences to us.



<!--break-->
