---
title: "KDE Commit Digest for June 24, 2005"
date:    2005-06-25
authors:
  - "dkite"
slug:    kde-commit-digest-june-24-2005
comments:
  - subject: "Good job"
    date: 2005-06-25
    body: "Good job, KDE-devs!"
    author: "rikva"
  - subject: "Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-25
    body: "The home:/ or media:/ ioslaves in reality are only KDE slaves ;) it can't be used with regular xterm/konsole/rxvt or non-kde applications.\n\njust goto media:/dev/hda5 or /dev/<partition> in konqueror and press F4 (to open terminal emulator (konsole)) what do you get? $HOME is what one get. It should get the /dev/hda5 as the link mounted in /etc/fstab file.\n\nAre there any plans to make KDE's ioslaves work with non-kde applications transparently? much like opening a file from media:/<partition>/filename.txt works with non-kde applications as expected!?\n\nAnd any plans to allow non-kde applications to have it working with their applications? and API or something... \n\njust a wishful thinking. And kudos to KDE developers for wonderful ioslaves :)\n\n"
    author: "Fast_Rizwaan"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-25
    body: "There was some work with FUSE and ioslaves. Don't know what the status is though.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-25
    body: "It's more or less unmaintained as far as I know...\nI should work on it again since FUSE evolved a lot recently,\nbut it was working quite well."
    author: "ervin"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-25
    body: "link: http://wiki.kde.org/tiki-index.php?page=KIO%20Fuse%20Gateway"
    author: "cl"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-25
    body: "What you're describing about opening a terminal emulator is fixed\nfor a while now and will be available in KDE 3.5 (I had to\nintroduce some new methods in kdelibs).\n\nMoreover I introduced facilities for applications to deal\nalmost correctly with them, even for some non-KDE applications\n(xmms should be ok for example), except maybe for DnD which\nsurely need some more work.\n\nI'm working on this kind of issues so that you could browse all\nthe important places of your system using... system:/"
    author: "ervin"
  - subject: "Thanks!"
    date: 2005-06-25
    body: "Thanks ervin, I appreciate the development and your effort to make System:/ and other ioslaves work. I have very high hopes with KDE 4.x for Unix on the Desktop!"
    author: "fast_rizwaan"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-26
    body: "Does that mean that finally the single most annoying thing\n(IMHO) in KDE is fixed?\nCan you go to a SSH site via fish, give your password via KWallet\nand  press F4 and you are directly logged in via SSH on the terminal?\n"
    author: "Martin"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-26
    body: "Oh man... that would make my day! :-)\n\n~Macavity"
    author: "macavity"
  - subject: "Re: Problem with home:/ and other media:/ ioslaves"
    date: 2005-06-26
    body: "Is that reported on bugs.kde.org already?"
    author: "ac"
  - subject: "Some semi-OT"
    date: 2005-06-25
    body: "Has anybody tried x.org cvs with the performance improvements that the Trolltech guys Lars and Zack have contributed?\nIs the Nvidia binary driver still working after these changes?\n\nAnd eventually, when will Qt-Mozilla/Firefox be buried?\nWas good to see the so promising work of making Gecko/Mozilla even better, but sadly it seems to have stopped since a long time :'-("
    author: "ac"
  - subject: "Re: Some semi-OT"
    date: 2005-06-25
    body: "> when will Qt-Mozilla/Firefox be buried?\n\nNever. There is someone paid for working on it afaik."
    author: "Anonymous"
  - subject: "Re: Some semi-OT"
    date: 2005-06-26
    body: "About the x.org question, I am not an expert on the subject, but since no-one else replied here goes: nvidia driver is a userland OpenGL implementation. Kernel changes can break it, since the driver makes calls to it, but it doesn't actually makes calls to X, rather X *uses* it. Can't see how could it could break it, especially not optimization code.\n\nAnyway, I believe Lars and Zack contributions were to speed up some graphical operations for graphical card/drivers that don't support 2D acceleration."
    author: "blacksheep"
  - subject: "Internationalization Status of German"
    date: 2005-06-26
    body: "I wonder why the Internationalization Status of German is never stated?"
    author: "Florian"
  - subject: "Re: Internationalization Status of German"
    date: 2005-06-26
    body: "Because it's a \"Top 10\" and German translation is on place 15?"
    author: "Anonymous"
  - subject: "K3B Super App"
    date: 2005-06-26
    body: "K3B is an absolute pleasure to use. Now with .12, it's even better. I want to thank the author(s) and just let them know how much they add to the Linux desktop. \n\nI mainly use it for data backup and retrieval. With my new DVD burner, I just backed up all my data on one DVD... very quickly and without a hitch. Nice work. Now, I want to try .12 ... but didn't you missplace the decimal point? ;-) It should really be 1.2 given the maturity of this wonderful application. Keep up the good work. It is greatly appreciated.\n\nStan Gatchel"
    author: "K3B .12"
---
In <a href="http://commit-digest.org/?issue=jun242005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=jun242005&all">all in one page</a>):

<a href="http://www.digikam.org/">digiKam</a> adds a golden mean photo editing plugin.
<a href="http://edu.kde.org/kalzium/">Kalzium</a> shows isotope and scientist information.
New home:/ IO slave. This IO slave displays all the home folders of the users being in the same group than you.
Many bugfixes in <a href="http://kmail.kde.org/">KMail</a>, <a href=" http://khtml.info/">khtml</a> and <a href="http://kopete.kde.org/">Kopete</a>.



<!--break-->
