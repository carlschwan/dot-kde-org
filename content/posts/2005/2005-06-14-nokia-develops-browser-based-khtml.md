---
title: "Nokia Develops Browser Based on KHTML"
date:    2005-06-14
authors:
  - "sky\u00f6stil\u00e4"
slug:    nokia-develops-browser-based-khtml
comments:
  - subject: "Minimo"
    date: 2005-06-13
    body: "Suprising, as N seems to be all G internally. So this probably means that Mozillas architecture sucks even more than expected (Minimo doesn't seem to fly even with all that money invested in it).\n"
    author: "Anonymous Coward"
  - subject: "Re: Minimo"
    date: 2005-06-13
    body: "It may be probably more with gossips about future Apple mobile."
    author: "m."
  - subject: "Re: Minimo"
    date: 2005-06-13
    body: "Surprising as it may seem, Mozilla/Gecko is not the official HTML renderer for GTK/GNOME."
    author: "Trejkaz"
  - subject: "Re: Minimo"
    date: 2005-06-14
    body: "Mozilla is not built on GTK, they have their own platform, Chrome/XUL. The Gnome project's Epiphany browser is the marriage of Gecko and GTK."
    author: "Annno v. Heimburg"
  - subject: "Re: Minimo"
    date: 2005-06-14
    body: "AFAICS Chrome/XUL uses GTK in turn, at least on Linux: \n\n> ldd /usr/lib/mozilla-firefox/firefox-bin | grep -i gtk\n        libgtk-x11-2.0.so.0 => /usr/lib/libgtk-x11-2.0.so.0 (0xb7ce5000)\n\n"
    author: "cm"
  - subject: "Re: Minimo"
    date: 2005-06-14
    body: "Which again is kind of overenginered for embedded uses. On the other hand maintaining widgets in its own renderer instead letting a toolkit handle them leaves more room for optimizations which is why the webkit team wants to abstract out the use of Qt widgets in their khtml fork."
    author: "ac"
  - subject: "Re: Minimo"
    date: 2005-06-14
    body: "> Suprising, as N seems to be all G internally.\n\nG as in Symbian?"
    author: "Anonymous"
  - subject: "Congratulations to the K team!"
    date: 2005-06-13
    body: "Guys, I just have to words. Congratulations to all my K brothers, and of course especially those involved in KHTML/KDOM/KJS. Remember, everything you have in your life is what you deserve. And you guys rock, keep it up!\n\nNow if I could afford one of those mobiles... :-P Okay, no, I'll better wait till I can hold a whole KDE-powered Linux in my palm."
    author: "Artemio"
  - subject: "Re: Congratulations to the K team!"
    date: 2005-06-13
    body: "I boycott Nokia for their pro Software Patent stance in Brussels. As long as they do not fire Tim Frain I see no reason to support Nokia. "
    author: "Gerd"
  - subject: "Another reason"
    date: 2005-06-13
    body: "Now I have a new reason for buying a Nokia phone when I need to replace my current one. I'm very satisfied with Nokia, but after reading this news message I like it even more. :)"
    author: "Bram Schoenmakers"
  - subject: "Re: Another reason"
    date: 2005-06-13
    body: "As long as they still are a major pro-software patent supporter in Europe you shouldn't be mislead by their action elsewhere. (Please note that the reason that they offered patents coverage to the original Linux kernel sources a couple weeks ago is that they would invalidate their own use of it through the GPL if they ever enforced their patents against it otherwise; same with KHTML/KJS now. But that still doesn't protect any OSS project they aren't involved in, so software patents are very dangerous even in cases like ambivalent and contracticting software patent supporters like Nokia is.)"
    author: "ac"
  - subject: "Nokia should have used QT"
    date: 2005-06-13
    body: "It would have been nice from NOKIA having gone with QT/KDE rather than GTK+/GNOME. Money haven't been an issue as they have shown by spending 50k USD to GNOME-Foundation and the 99USD sales of their 770 to developers. Someone being able to act like this could easily have purchased licenses from QT. Norway and Finnland are quite close together they would have helped to improve european Software and IT industry quite a lot. Keeping their BIP and BSP in their country too."
    author: "ac"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-13
    body: "Yes .. these things are not *really* about money or technology, they're about religion. It's the human nature. Have RedHat as a greatest example."
    author: "Anonymous Coward"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-13
    body: "Well i dont know...\n\nGNOME has got a lot of 'corporate backing' and, _I_FEEL_, it didnt work out for them. Their project structure is now quite hirachic and the project get pushed into something that has to look 'simple' so it can be 'sold' as simple. In the meanwhile the framework (libraries/ technologies) is not simple at all, and very inconsistent. Often different parts of the project cannot agree on the use of technologies/ libraries and then the wheel has to be invented again. By trying to attract corporate backing GNOME is putting itself in force fields that are not healthy for the project.\n\nThis is the total opposite of the KDE 'way'. Not that in KDE everyone easily agrees on using a certian technology, but when the decision is made it is supported to a certain level.\n\n\n\nI think when a capable handheld is running some GPL software it cant be too long before it will run other GPL software. If this handheld is going to be COOL, but, running some GPL software that is NOT COOL, it will not take long before there is a distribution of COOL software for that particular hardware and OS.\n\n\"Better to be a pirate than to join the navy.\" -- Steve Jobs\n\n\n\n\n\n"
    author: "cies breijs"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-14
    body: "Good points.  Too often people have spoken as if corporate backing for a free software project was some sort of definitive advantage that would make any other competing projects irrelevant.\n\nTruth is, significant corporate backing can be a powerful influence, and unless it is harnessed by a strong, well established, respected project leadership, it can make the project lose its direction and afflict it with all the problems of corporate projects (such as lack of long-term focus).\n\nGNOME was never the most organised or focused project, but left on its own it could have got better.  However, it got too much corporate backing too soon and now they are slaves of the \"get something out of the door\" and \"redefine the project every six months\" mentality.\n"
    author: "em"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-14
    body: "Good points, no Gnome bashing. However the Gnome framework also has advantages because of its bazaar style serving everybodies needs with bindings for x languages etc. I would like to see the same binding support for KDE. Often KDE bindings such as for Java exist but nobody seems to know about them or use them. \n\nFrom my perspective more KDE - Gnome bridges are needed. E.g applications such a Gnumeric with Chrytal Icons look very similar to KDE ones. I don't think it is good that we do not have common theming and colour schemes. Also the other GUI design specifications could be harmonised."
    author: "Gerd"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-14
    body: "When bindings are created but not used, should they be maintained? I dont think so. I do like the way KDE sticks to C++, Qt and its own libraries. I had a lot of installation nightmares with GTK/GNOME apps that where written in the most diverse programming languages. Spending time on maintaining bindings has to be worth it. I dont forsee any core KDE apps being written in anything but C++, that somehow keeps it simple -- allthough Python and Ruby rock the simplicity scene these days.\n\nI do agree on your points of shared (Icon) themes and GUI specs, but it has to be sane (contrary to the, IMO, insane button order debate[1]). There are gtk-qt[2] and plastig[3] but it will never really blend.\n\n[1] http://www.google.com/search?&q=gnome+button-order\n[2] http://www.freedesktop.org/Software/gtk-qt\n[3] http://www.kde-look.org/content/show.php?content=9724"
    author: "cies breijs"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-14
    body: "I think you are buying the myth that since Gnome is written in C it somehow magically gets bindings for all other programming languages. The reality is that KDE has more well maintained and up to date bindings. And the available bindings are for both the core toolkit and the desktop, Qt and the KDE libs. For Gnome most of the bindings are only for GTK+, while the bindings for the Gnome libs are either absent or outdated. The main reason the Qt/KDE bindings are kept up to date are the hard work the developers have put into making automatic tools for generating the bindings. Making the amount of manual labour needed for creating bindings for new versions of the libraries relatively small. "
    author: "Morty"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-14
    body: "\"I think you are buying the myth that since Gnome is written in C it somehow magically gets bindings for all other programming languages.\"\n\nActually it's moved on from that, these days it's \n\n\"..that you write a GTK# binding for Mono and somehow you magically get bindings for all other programming languages\"\n\nAnother variation:\n\n\"..you use a CORBA based component system and somehow you magically get bindings for all other programming languages\"\n\nI've found it's better to have just a few well maintained bindings, than large numbers of half finished efforts. So in KDE we have python, java and ruby, and I'm not sure we need much more. Maybe C# and perl too would be nice, but they aren't essential for KDE 4 in my opinion\n\nYou also need an IDE like KDevelop with support for your language, along with tutorials and documentation and a community of people who actually want to use the binding. For instance, I spent six months doing Objective-C bindings for Qt/KDE, only to find no one was slightly interested in using them."
    author: "Richard Dale"
  - subject: "Bindings"
    date: 2005-06-14
    body: "Well maintained bindings are the key, and just as important it has to be common knowledge. As an example take the Java bindings. I don't think it's used by many, even if it has a long history of being well maintained and up to date. The Python bindings are much more visible, and 3rd party applications have been emerging for some time. What the Qt/KDE bindings need are more hype, making more developers aware. Perhaps KDE 3.5 should become a hype the bindings release:-)\n\nYou also have Javascript in addition to the Python, Java and Ruby bindings you mentioned. And I thought Perl bindings was well maintained using Smoke, same as with Ruby. Since I don't follow the development of those to closely I may have gotten the wrong impression."
    author: "Morty"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-14
    body: "Well... I had a mild, academic interest in the ObjectiveC bindings, but I was very busy with PyQt at that time."
    author: "Boudewijn"
  - subject: "Re: Nokia should have used Qt"
    date: 2005-06-15
    body: "I would like to see and use KDE and QT binding for pure C.\nThey seem to exist, but nobody talks about them."
    author: "anonymous"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "Well, they should probably have used Linux also, eh?\n\nNo.\n\nBoth Linux and Qt suck on small devices such as mobile phones since neither is (out of) memory safe. I've worked with Qt embedded long enough to know.\nI'm not sure how KHTML handles this, but if it doesn't they can expect lots of problems on real world websites, which will ultimately crash the browser.\n(If it were linux based, it would usually kill a random process due to the infamous OOM killer)."
    author: "Jan Vidar Krey"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "linux is out of memory safe. It uses the \"OOM killer\" to avoid being trapped in such a situation. There is no OS that will allow you to use more memory that what you do have safely."
    author: "anonymous"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "But linux has a very bad habit of over-committing memory.\n\nA call to malloc() will not return 0 as it should when out of memory, \nresulting in a random process being killed when trying to access it.\nThere are ways to tune the OOM killer, and the overcommit behaviour, but still it does overcommit frequently. \n\nThis is very bad for embedded applications where memory usage is dynamic, such as in a browser. It's not as bad for other applications that use a fairly constant or predictable amount of memory."
    author: "Jan Vidar Krey"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "Wouldn't it be cool if Linux where Open Source so that an embedded device producer could change that behavior....Oops. ;)"
    author: "Christian Loose"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "Absolutely, that would be cool!\n\nAnd now, that we presumably have an oom safe kernel, we would need an oom safe libc (oops). Then we would need an oom safe toolkit (oops).\n\nNow, this is not trivial, but it's not too hard either, but it does take alot of time. So why not write an OS from scratch, and call it... Symbian?\n\n"
    author: "Jan Vidar Krey"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "Symbian is open source?"
    author: "ac"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "Have you tried\necho 2 > /proc/sys/vm/overcommit_memory\n\nor\n\nsysctl -w vm.overcommit_memory=2\n(can be placed in /etc/sysctl.conf from what I read on the web)\n\nIt also seems that there is (or was at a point) a \"paranoid\" mode with \"3\" instead of \"2\".\nSee this message for more details:\nhttp://www.gelato.unsw.edu.au/linux-ia64/0301/4565.html\n\n"
    author: "oliv"
  - subject: "Re: Nokia should have used QT"
    date: 2005-06-14
    body: "I did try that on a fairly recent 2.6 kernel a few months ago, but that still did not help much :("
    author: "Jan Vidar Krey"
  - subject: "GTKHTML  :o)"
    date: 2005-06-13
    body: "\nGTK WebCore, or -- they must have at least considered it -- GTKHTML :)\nhttp://gtk-webcore.sourceforge.net/doc/index.html\n\n\n"
    author: "cies breijs"
  - subject: "Re: GTKHTML  :o)"
    date: 2005-06-13
    body: "I don't know if they use it for anything or what plans they have for it, but WebCore has apparently been in Gnome cvs for a while now. According to this. http://cvs.gnome.org/viewcvs/gnome-webcore/"
    author: "Morty"
  - subject: "cool!"
    date: 2005-06-14
    body: "Now lets talk about a successor of DCOP that will be usefull for GNOME apps, and ditch D-BUS. It seems to be already ditched by Beagle, a GNOME app.\n\nhttp://www.archivum.info/dashboard-hackers%40gnome.org/2005-05/msg00018.html\n\nLet's innovate!\n\nKDE and GNOME can work together on a lot of issue. Both parties should just talk, ask and learn from eachother, before the wheel has to be reinvented in a different programming language/ library.\n\n[but i have to admit i dont know if there was any inter-desktopian-discussion before D-BUS was created]"
    author: "cies breijs"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "There was GNOME/KDE discussion all along.   KDE2 and GNOME were both planing on using COBRA for a long time.  Eventually the KDE hackers gave up on ever making it work (or should I say work fast enough to be useful), designed DCOP on a napkin (maybe not that simple, but just about), and implemented it in a afternoon or so.  A few months latter and KDE settled on DCOP for everything, much to the displeasure of the GNOME people.   \n\nGNOME seems to have given up on COBRA as well these days.   I don't know what their side of that story is.   The compromise is D-BUS, which fixes some of their objectsions to DCOP (though I'm not sure which), and undoubtedly fixes some things KDE learned after a few years of DCOP."
    author: "bluGill"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "No offense, but you don't know what you're talking about.  You know just enough of the facts to twist them into a perverted (false) story."
    author: "ac"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "Can you please tell us truth? Because i've got to think same things as parent do."
    author: "Ilyak"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "I disagree, it sounds an accurate summary of what happened to me"
    author: "Richard Dale"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "I would be really interested what KDE learned and what parts of DCOP it is supposed to fix. Especially since it seems that no KDE developer is involved ATM in the DBUS project (except for the Qt bindings)."
    author: "ac"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "DBUS uses a more sane marshalling format that is tied less closely to the Qt stream operators and Qt class names, it is also better in separating individual arguments which makes it easier to perform sanity checks. The message protocol is also a bit easier and more extensible than DCOP, leading to more flexibility in how messages can be handled. Asynchronous calls for example are easier to implement with DBUS, in DCOP it's a bit of a hack. The improved message protocol also makes it possible to use DBUS from threaded applications, the DCOP protocol has message sequence restrictions that makes that very difficult.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: cool!"
    date: 2005-06-14
    body: "Thanks Waldo!"
    author: "ac"
  - subject: "Deal with Opera?"
    date: 2005-06-13
    body: "What happened to the deal with Opera, will the two browsers (one WebKit based, the other Opera) be shipped side by side?\n\nhttp://www.cellular-news.com/story/12342.shtml\nhttp://operawatch.blogspot.com/2005/05/nokia-chooses-opera.html\n\nOr is the current press release...\nhttp://press.nokia.com/PR/200506/998214_5.html\n...only trying to make some news, while the default browser will be Opera?\n\n[IIRC Opera is also Qt based]"
    author: "cies breijs"
  - subject: "Re: Deal with Opera?"
    date: 2005-06-14
    body: "> [IIRC Opera is also Qt based]\n\nOnly very loosly - it draws buttons etc itself and just uses Qt for the menu!"
    author: "Jim H"
  - subject: "Re: Deal with Opera?"
    date: 2005-06-14
    body: "So Oprea doesnt use Qt as an C++ enhancement lib... like, it doesnt use the Signal-Slot mechanism?"
    author: "cies breijs"
  - subject: "Re: Deal with Opera?"
    date: 2005-06-14
    body: "They indeed have their own proprietary internal cross platform toolkit for all that (just like Mozilla and OOo incidentally), Qt is just used on Linux to make Opera look more fitting in."
    author: "ac"
  - subject: "Recent KHTML experience"
    date: 2005-06-13
    body: "KHTML/KJS seem more alive and kicking then ever.\nI remember not so long there was lots of buzz about\nGecko replacing KHTML etc.\nGreat that it's not so! Konq feels so much faster\nand KDEish IMHO.\nIn my company we've been developing a _very_ complex \nweb application for years now with tree views,\ntoolbars, dialog boxes, you name it. It almost feels\nlike a normal application that runs inside a window.\nUnfortunately it's for internal use only, so there is no demo.\nWe've focused on supporting IE and Mozilla. Dont talk about\nstandards here. They unfortunately dont work well enough if you really\nneed those bleeding edge features. So there is lots of trickery put\nin. Personally I always used Konq for surfing as it is much faster.\nSo I really would have liked to support Konq too for our web app.\nI gave up quickly because Konq used to not even show the line number\nof a JavaScript error before so I figured it would be to complex\nto find all those errors and lots of things just werent supported\nat that time. Only recently I tried again to support KHTML since\nKonq now shows the line number if you enable debugging.\nWhat's still stupid IMHO is that if you disable those dialog boxes\nyou lose the line number information as well. It is not shown if\nyou double click on the \"bug\" symbol. So you have to click aways\nlots of dialog boxes again and agin. Well, you get used to it.\nAnyway, to my amazement with only some minor glitches to fix and some things\nwhere Konq was right and our page just plain wrong, the main screen (which is already quite complex) did show up and work correctly. I really did not\nexpect this. The KHTML/KJS engine seems quite advanced, now.\nIn Safari the whole web app now works correct. Unforunately, there is\nstill one bug left in KHTML which spoils the whole thing: IFrames always\nappear on-top of everything else. Menus, dialog boxes disappear behind\nit. That makes our web app useless in Konq so far but I found there is\na bug filed in bugs.kde.org on this already so I guess it's just a\nquestion of time before Konq is supporting this. It's really come a\nlong way IMO and usage by Nokia confirms that it's a competitive\nand light-weight HTML/JS implementation.\n\n\n\n\n"
    author: "Martin"
---
In a <a href="http://press.nokia.com/PR/200506/998214_5.html">press release</a> today, <a href="http://www.nokia.com">Nokia</a> announced a new web browser in the works for the <a href="http://www.nokia.com/nokia/0,8764,46827,00.html">Series 60</a> application platform. The interesting tidbit about the browser is that it is based on Apple's <a href="http://webkit.opendarwin.org/">WebKit</a> which is of course based on KDE's very own <a href="http://khtml.info/">KHTML</a> (<a href="http://en.wikipedia.org/wiki/KHTML">wp</a>) rendering engine. KDE is fully acknowledged in Nokia's announcement. <strong>Update:</strong> Roland Geisler, Head of Strategy for the Series 60 Browser at Nokia, <a href="http://lists.kde.org/?l=kfm-devel&amp;m=111869894024245&w=2">wrote to the Konqueror mailing list</a>.





<!--break-->
