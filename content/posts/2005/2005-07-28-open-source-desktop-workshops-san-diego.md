---
title: "Open Source Desktop Workshops: San Diego"
date:    2005-07-28
authors:
  - "aseigo"
slug:    open-source-desktop-workshops-san-diego
comments:
  - subject: "Open Source Desktop"
    date: 2005-07-29
    body: "This sounds like a good workshop, but I have one question: were the GNOME and XFCE guys invited to participate?  \n\nIt seems like a Qt/KDE-only event.  That's fine, but the \"Open Source Desktop Workshop\" name would then make it sound more inclusive than it actually is.  On the other hand, if the GNOME and XFCE guys were invited and declined, then the KDE nature of it is no fault of the organizers.\n\n"
    author: "andrew"
  - subject: "Re: Open Source Desktop"
    date: 2005-07-29
    body: "> It seems like a Qt/KDE-only event.\n\nthis one is, yes.\n\n> That's fine,\n\nglad you approve ;)\n\n> but the \"Open Source Desktop Workshop\" name would then make it\n> sound more inclusive than it actually is.\n\ni'm not sure i see the problem here. realize that the people who stepped up to put this on are all primarily motived with regards to KDE. i'm not beholden to help every project in the open source world in the name of fairness, and the OSDW name is:\n\na) accurate (it's about open source desktop technology)\nb) resonates with the general development community \nc) leaves the door open for future expansion\n\ni should expand a bit on point (b). note that the name doesn't mention \"linux\" specifically because i didn't want to make people who work on Solaris, *BSD, MacOS or even Windows feel uninvited. also, everyone knows what \"open source\" and a \"desktop\" is even if they've never used KDE or one of the other open source desktops.\n\nas we grow these events i'd eventually like to see some people working on important desktop technologies that KDE uses presenting. this might include X.org devs, Mozilla devs or people working on things under the FreeDesktop.org umbrella ..."
    author: "Aaron J. Seigo"
  - subject: "Re: Open Source Desktop"
    date: 2005-07-29
    body: "Does KDE support XDS (XDND Saving protocol) ? Gimp has recently implemented it (2.3.x unstable branch) and Rox-Desktop has it. It would be nice to see XFCE and Gnome adopt it also."
    author: "oliv"
  - subject: "Re: Open Source Desktop"
    date: 2005-07-29
    body: "The Gnome/GTK people do the same thing. Everything they do is called 'The Linux Desktop'. Personally, I'd rather see the name KDE (K Desktop Environment) a bit more but the name is, nevertheless, accurate."
    author: "David"
  - subject: "Recordings?"
    date: 2005-07-29
    body: "Will there be recording of the presentations for those that cant be there?"
    author: "james"
  - subject: "Re: Recordings?"
    date: 2005-07-29
    body: "Oh god, I hope not..."
    author: "manyoso"
  - subject: "Re: Recordings?"
    date: 2005-07-29
    body: "I hope there will be. The stuff that's on looks awesome, and I certainly won't be able to get to San Diego. You'll be fine. Let's face it, no one is comfortable with doing talks."
    author: "David"
  - subject: "Re: Recordings?"
    date: 2005-07-29
    body: "> Let's face it, no one is comfortable with doing talks.\n\nwell, except freaks like me ;)\n\ni don't know if we'll be recording the sessions or not. i'll keep it in mind as we get to the business of getting the details of the session rooms in order."
    author: "Aaron J. Seigo"
  - subject: "Europe"
    date: 2005-07-29
    body: "Cant they hold a workshop in Europe sometime also? WOuld be intresting.. "
    author: "Magnus"
  - subject: "Re: Europe"
    date: 2005-07-29
    body: "unless it's an adjunct to some other event(s), it's probably not worth me flying out there for 2 days ;) but Daniel Molkentin and i were discussing the possibility of bringing these to Europe as well. Tom Chance, whose been helping with some of the PR side of things, also noted that this would be a good idea and that there's enough interest in Britain to hold a series of them there.\n\nso, we'll see =)"
    author: "Aaron J. Seigo"
  - subject: "locations"
    date: 2005-07-29
    body: "<I>If you can't make it to San Diego for this event, look for further OSDW events in 2006.</I>\n\nHopefully there will be more if the San Diego one kicks off to a good start."
    author: "Ryan"
  - subject: "Re: locations"
    date: 2005-07-29
    body: "that is exactly the idea =)\n\nplaces that are of interest to me right now for 2006 include Seattle, Toronto, Philadelphia and Austin. i'm open to suggestions of course =)"
    author: "Aaron J. Seigo"
  - subject: "Re: locations"
    date: 2005-08-02
    body: "he means Dallas, not Austin. Most everybody already knows that aseigo is bad at spelling. He's not too good with geography either. ;)"
    author: "mattr"
  - subject: "Re: locations"
    date: 2005-08-02
    body: "i'm half tempted to say that i meant austin just to impose a long drive on you ;)\n\nbut yeah, i mean dallas. i keep forgetting which city you are near. by the time this particular exercise is over, i'm sure i won't forget. actually, i found a way to remember: a very good high school friend of mine lives 30 mins north of dallas and will likely attend the conference as she's recently been getting more into software devel and enjoys those \"days off\" she gets when going to these conferences on work time ;)"
    author: "Aaron J. Seigo"
  - subject: "no publicity"
    date: 2005-07-29
    body: "Any reason this is only announced here and not elsewhere?"
    author: "KDE User"
  - subject: "Re: no publicity"
    date: 2005-07-29
    body: "What location do you have in mind? LWN.net, OSdir and PCLinuxOnline have the story. On Slashdot, OSnews, NewsVac and LinuxToday the submission is either pending or rejected.\n"
    author: "Anonymous"
  - subject: "Re: no publicity"
    date: 2005-07-29
    body: "Don't worry about it.  The real PR push hasn't happened yet.  Aaron is coordinating this with PR from Linspire and Tom Chance.  As you can read on his blog, the real PR push won't happen for another couple of days.  Not sure why it went up on the dot first, though..."
    author: "manyoso"
  - subject: "Re: no publicity"
    date: 2005-07-29
    body: "Why do you think would they post it the second time submitted if they declined publication the first time?"
    author: "Anonymous"
  - subject: "Re: no publicity"
    date: 2005-07-29
    body: "I'm not going to make a habit out of answering obvious questions, but I'll make an exception this one time.\n\nSee:\n1. http://science.slashdot.org/faq/editorial.shtml#ed200 \n2. http://science.slashdot.org/faq/editorial.shtml#ed300\n3. http://science.slashdot.org/faq/editorial.shtml#ed400"
    author: "manyoso"
  - subject: "Re: no publicity"
    date: 2005-07-29
    body: "i wanted to give the people here a bit of a heads up as i've been talking about this in my blog for some time and may of the people who read have been asking me about it. well, we finally got all the details nailed.\n\non monday there will be official press releases on the way so there will be more publicity for it at that point."
    author: "Aaron J. Seigo"
---
<a href="http://www.osdw.org/cms/1011">Registration</a> has begun for those wanting to attend the first <a href="http://www.osdw.org">Open Source Desktop Workshop</a> to be held on October 13th and 14th in San Diego, California!  Open Source Desktop Workshops are affordable educational events that bring top-flight Open Source desktop developers together with those who are looking to gain the skills necessary to join them. With <a href="http://www.osdw.org/cms/1009">presenters</a> from around North America speaking on a <a href="http://www.osdw.org/cms/1013">variety of practical topics</a> this will be an exciting and worthwhile event.









<!--break-->
<div style="border: thin solid grey; padding: 1ex; float: right; width: 167">
<img src="http://static.kdenews.org/jr/osdw.png" width="167" height="128" />
</div>

<p>Thanks to <a href="http://www.osdw.org/cms/1017">OSDW's sponsors</a> which include <a href="http://www.linspire.com">Linspire</a> and <a href="http://www.trolltech.com">Trolltech</a> as well as the speakers who are donating their time for this event, we have been able to keep the registration fee to an absolute minimum for this inaugural event: US$50 for two days of instruction including a breakfast table and lunch! Attendance is limited to the first 100 registrants, so be sure to register soon.</p>

<p>If you can't make it to San Diego for this event, look for further OSDW events in 2006.</p>









