---
title: "Final Voting in 2005 Linux Journal Readers' Choice Awards"
date:    2005-07-08
authors:
  - "Anonymous"
slug:    final-voting-2005-linux-journal-readers-choice-awards
comments:
  - subject: "Messed up category"
    date: 2005-07-08
    body: "Kontact for Office Program, way to go!"
    author: "blacksheep"
  - subject: "Re: Messed up category"
    date: 2005-07-08
    body: "An interesting thing would be Kontact as office application getting more votes than Evolution as email client..."
    author: "Anonymous"
  - subject: "KMail?"
    date: 2005-07-08
    body: "No KMail in the final.. although in the earlier round there was KMail vs Kontact in the e-mail client section. Duh."
    author: "Ivor"
  - subject: "Re: KMail?"
    date: 2005-07-08
    body: "Yeah, bad voting split. Last year it became third (http://www.linuxjournal.com/article/7724) too."
    author: "Anonymous"
  - subject: "Re: KMail?"
    date: 2005-07-08
    body: "Kmail and Kontact listed separately on the same ballot really killed Kmail's chances.  Whats more is its a ridiculous split that shows a distinct lack of understanding about exactly how Kontact technologies are incorporated.  What is more, LJ should have known better.\n\nBobby"
    author: "brockers"
  - subject: "Re: KMail?"
    date: 2005-07-09
    body: "Heh, Office is the place where Kontact is shown on my machine (not Internet, where KMail lives). If we put Kontact there, who is LJ to disagree?"
    author: "Brad Hards"
  - subject: "Changes"
    date: 2005-07-08
    body: "Even with the messed up categories I think there will be some nice results for the KDE camp. And I it will illustrate how things have started to change in the Linux world, and how KDE are central in that change. To make some predictions I'd guess Kate are going to come in on a nice second, not to far behind Vim. It will be close, but not surprising if amaroK actually beats XMMS. The gap between Gaim and Kopete are not going to be very big this year. And I suspect Subversion are going to win it's category this year, call it a hunch:-)"
    author: "Morty"
  - subject: "BAH!"
    date: 2005-07-08
    body: "It is really ridicolous how so-called\n\"experts\" can throw out such a list.\n\n<irony>\nThey did NOT list KDE as programming language!\n</irony>\n\nKDE e.V. should write a letter asking them\nto think about their ignorancy. If they want\nto promote free software they should NOT list\nOpenOffice.org and Kontact together - hell did\nany of them ever use any of the listed projects?\n\nMy comment to this awards: BAH! I t  - i s  - a  - s h a m e."
    author: "Nikolas Zimmermann"
  - subject: "Re: BAH!"
    date: 2005-07-08
    body: "Something went wrong, they had Kontact listed twice here:\n\nftp://ftp.ssc.com/pub/lj/Web/8266.txt\n\nboth as e-mail client and as office program.  It was probably eliminated as e-mail client (KMail took votes from Kontact and vice-versa) but not as office program. \n\nMaybe you should have complained before?"
    author: "ac"
  - subject: "Re: BAH!"
    date: 2005-07-08
    body: "> They did NOT list KDE as programming language!\n\nBut C++ can win after last year's winner C didn't even make it into the finale."
    author: "Anonymous"
---
The <a href="http://www.linuxjournal.com/article/8272">final round of voting</a> in the 2005 <a href="http://www.linuxjournal.com/">Linux Journal</a> Readers' Choice awards has begun. <a href="ftp://ftp.ssc.com/pub/lj/Web/RC/8272.txt">The final ballot</a> is based on the results of two previous rounds of open voting, and mostly the top two vote-getters in each category have made it to the final ballot. Eight KDE-related projects are still in the competition. The deadline for voting is July 28, the winners will be announced in the November 2005 issue of the magazine.

<!--break-->
