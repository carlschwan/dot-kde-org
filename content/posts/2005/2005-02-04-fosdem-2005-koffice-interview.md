---
title: "FOSDEM 2005: KOffice Interview"
date:    2005-02-04
authors:
  - "jriddell"
slug:    fosdem-2005-koffice-interview
comments:
  - subject: "paperKlip"
    date: 2005-02-04
    body: "that's what koffice is missing! a port of klippy! thanks for reminding us about it riddel!!!!!!"
    author: "anon"
  - subject: "Re: paperKlip"
    date: 2005-02-04
    body: "\"Konqi eats Klippy! Film at 11...\""
    author: "Tim Middleton"
  - subject: "w00t! Interview on the Dot = Good!"
    date: 2005-02-04
    body: "So far all of the interviews I've read on the Dot have been great, very informative, very entertaining, and I always learn about a new part of KDE.\n\nMy only request for the Dot would be MORE articles!  Reading the articles on the Dot is the highlight of my day (as far as stuff I read goes at least :-)."
    author: "Corbin"
  - subject: "Re: w00t! Interview on the Dot = Good!"
    date: 2005-02-04
    body: "Yes, dot interviews are good, since the interviewers obviously know the subject and don't ask all generic questions. "
    author: "Ian Monroe"
  - subject: "small comment there...."
    date: 2005-02-04
    body: "kexi does not have a mailling list, i wish it would.\n\n\n"
    author: "somekool"
  - subject: "Re: small comment there...."
    date: 2005-02-04
    body: "..as well as other KOffice apps. \n\nThere's one user-level mailing list... koffice@mail.kde.org and dev-level: koffice-devel@kde.org\n\n"
    author: "Jaroslaw Staniek (Kexi Team)"
  - subject: "Re: small comment there...."
    date: 2005-02-04
    body: "come on there are 15 developers , 1 list is more than enough"
    author: "ch"
  - subject: "Re: small comment there...."
    date: 2005-02-09
    body: "just to point out in the interview ....\n\n\"Well, there is Krita, Kexi and KPlato, which are more independent and even have their own mailing lists.\"\n\n"
    author: "somekool"
  - subject: "Audi interview"
    date: 2005-02-04
    body: "I think the OSS community should now be in position to provide an audio [or even video] interview by default. We have OGG and many players to choose from. I'd love to hear these wonderful people talk. What do the rest think?"
    author: "judith"
  - subject: "KOffice books"
    date: 2005-02-04
    body: "I think there should be nice books and trainings available written on KOffice. "
    author: "Terrence Doerthy"
  - subject: "Re: KOffice books"
    date: 2005-02-04
    body: "I have that on my TODO list somewhere ;)\n\nIf I manage to get the time needed I will work on training articles etc for KOffice in the near future. I also plan to write something for KOffice development, but that will have to wait until KOffice 2.0 since technology (Qt4/KDE4) will bring some changes - and honestly, I still need to learn more about KOffice before I can write really good development tutorials."
    author: "Raphael Langerhorst"
  - subject: "Kspread and graphs?"
    date: 2005-02-04
    body: "Can kspread plot graphs and trace a best fit line on it? Most of the students need to plot their data and put a line through it. Is it possible through kchart?\n\nI want to chuck out MS Excel which i run through crossover if i can find this functionality in kspread. Openoffice takes ages to load on my poor student system.\n"
    author: "joey"
  - subject: "Re: Kspread and graphs?"
    date: 2005-02-04
    body: "> I want to chuck out MS Excel which i run through crossover if i can find this functionality in kspread. Openoffice takes ages to load on my poor student system.\n\nI've heard of people using this combo for analysis:\n\nDB -> Octave -> gnuplot\n\nThis flow is more appropriate than a spreadsheet for large volumes of data. But, samples in student exercises tend to be relatively small. (If wanted, you could even use gnuplot for student exercises by placing the data in a text file[1]. This isn't the friendliest method, but it is still more powerful than what could be accomplished using a spreadsheet.)\n\n[1] http://www.cs.hmc.edu/~vrable/gnuplot/using-gnuplot.html\nAnother analysis system: http://www.r-project.org/\nMore: http://debianlinux.net/science.html"
    author: "d w"
  - subject: "Re: Kspread and graphs?"
    date: 2005-02-05
    body: "Why don't you use QtiPlot or LabPlot for that?\n\nhttp://www.kde-apps.org/content/show.php?content=14826\n\nhttp://www.kde-apps.org/content/show.php?content=9881"
    author: "Torsten Rahn"
  - subject: "Re: Kspread and graphs?"
    date: 2005-02-05
    body: "I use Octave to do curve fitting, it is an industry strength Matlab compatible software. It has all sorts of great statistics and curve fitting packages available."
    author: "Jonas"
  - subject: "just not enough devel"
    date: 2005-02-04
    body: "from the interview, it sounds like Koffice is bad shape, not enough developers, esp when you start comparing kword vs abiword, and gnumeric vs kspread, koffice sorta looks sad. oh well..\n"
    author: "ricky"
  - subject: "Re: just not enough devel"
    date: 2005-02-04
    body: "\"Then there are 3 to 5 developers improving the older KOffice applications like KWord, KSpread, KPresenter and Kivio.\"\n\nRight. That's very sad. On average about one developer per application; major, central, complex applications. What could Microsoft have -- 10.000?\n\nRealistically, no flame/FUD/trolling intended: If things stay this way, KOffice will never be a real alternative for serious use; not even for \"home\" users."
    author: "Martin"
  - subject: "Re: just not enough devel"
    date: 2005-02-04
    body: "Maybe he meant 3-5 ppl per app? Well, that means they are still alive, but it doesn't change the above conclusion."
    author: "Martin"
  - subject: "Re: just not enough devel"
    date: 2005-02-04
    body: "There can also be too many developers.\nYou can't all fix the same bugs or implement the same features.\n\nThis won't be a problem at the moment though, more people helping fixing bugs are always welcome.\n\nA lot of problems (bugs or wishes) are actually not that hard to fix or implement. So, I guess it's pretty easy to take one out of that list and try to create a patch.\n\nBy the way...\nMS does not have or ever had 10000 people working on Office.\nThat's just a fairytale.\n\nThey probably do have a core team of about 20 or 30 people working full time on it though."
    author: "Tim Beaulen"
  - subject: "Re: just not enough devel"
    date: 2005-02-04
    body: "Deciding on how many people really work on the core/older applications was not really easy. I mainly based this on the commit logs during the last couple of weeks.\n\nIn total I think there are about 10 people contributing to these applications. But most of the time some of the contributors are just not active. So if we take one point in time we might find about 3 to 5 people that are currently very actively working on KOffice. And... which surprises me day after day... these few people get actually quite some stuff done, it's really amazing (IMO).\n\nWatch the commit logs for some time, you will be surprised how KOffice evolves - DESPITE the few numbers of developers. Of course things would go even faster if there were more such heroes (as I am starting to call them) - everyone is invited.\n\nAnd... I'm using KOffice every day, I don't even have any other office suite installed. I really came to love it."
    author: "Raphael Langerhorst"
  - subject: "Re: just not enough devel"
    date: 2005-02-05
    body: "KOffice has had consistently ~150 commits a week for quite a while.\n\nThere are always more things to do with any piece of software, but KOffice seems to be hitting the sweet spot where it is useful to enough people to attract developers.\n\nKDE software seems to be sleepers. Nobody pays attention until suddenly they are the best. Watch this happen to KOffice in the near future.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: just not enough devel"
    date: 2005-02-04
    body: "well, more help would be nice, but its already a great office suit. I use it for all my daily work, and it does that very well. its faster than OO.o, and has all features I need. Also the KDE integration (KIO-slaves!) is great."
    author: "superstoned"
  - subject: "New app"
    date: 2005-02-04
    body: "I really really want a new kind of word processor. I truly loathe the MS Word type of word processor. I started writing a thesis using KWord a few months back, but switched to OOWriter since I couldn't input Japanese into KWord. The thesis is finished now, but it was incredibly frustrating to use oowriter. Just like MS Word, it always does things automatically, and half of the time it's not what I want and I just can't get it to do the right thing. I don't think I've ever felt so sympathetic with people actually punching their screens as when I tried to get oowriter to do what I wanted.\n\nI tried KLyx too, which seemed like a good solution. No Japanese support there either, so I had to give it up. And even if I had gotten that to work, it would still have needed a plugin for full Japanese support (furigana).\n\nI propose a new application: Edit your document directly through it's \"language\", while having the result updated in real time, as well as WYSIWYGically moving around things using your mouse. Not insanely complicated like lyx (with bizzarre font problems, nonfunctional plugins etc etc), not incredibly stupid like MS Word. And with asian language support; hell, even vertical writing.\n\nI'm considering getting familiar with QT 4 and starting an app like this.\n\nWho's with me? ;) (yeah I know, I'll let you know when I have some code to show)"
    author: "Apollo Creed"
  - subject: "Re: New app"
    date: 2005-02-04
    body: "In the future Quanta may be usable to do something like this. You can create a new XML-based language for word processing already, so you can write in the \"native\" language. We plan to introduce:\n- automatic (periodic or as you type) updating of the preview\n- support XML in the preview, rendered through an style sheet\n\nNavigating in the rendered version might be a challenging thing though. The katepart editor used in Quanta already supports japanese text introduction, and support for bidi (and I don't know, maybe vertical typing?) is planned as well.\nOf course, this will not happen before 4.0.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: New app"
    date: 2005-02-04
    body: "Interesting, and Quanta did come to mind when I thought about this. Perhaps I should base my effort on Quanta, I'll look into it."
    author: "Apollo Creed"
  - subject: "Re: New app"
    date: 2005-02-04
    body: "If you have good new ideas, why not talking directly to the kword folks?\nMaybe some of the problems you noticed in kword are known issues or are already in the TODO list.\n\nCiao!"
    author: "Dario Massarin"
  - subject: "Re: New app"
    date: 2005-02-04
    body: "Just a reminder: Would be cool if you'd consider TeX compatibility when starting the idea."
    author: "Anonymous"
  - subject: "Re: New app"
    date: 2005-02-04
    body: "Input Method (IM) for Asian characters was recently added to KOffice HEAD, meaning it will be available in KOffice 1.4.\n\nYou can even write from right to left... and you CAN do vertical text ;)  (just rotate the frame).\n\nIf there are things you would like to see implemented in KOffice it would be great to join KOffice rather than beginning a new application. But I know this is/was well intended :)  At least sending us a wishlist about ideas helps already."
    author: "Raphael Langerhorst"
  - subject: "Re: New app"
    date: 2005-02-04
    body: "> I really really want a new kind of word processor.\n\nThis has been brought up in the last article about KOffice as well (also the word processor!). Take a look at KWord, and probably read the first few pages of the KWord manual, which is excellent."
    author: "Raphael Langerhorst"
  - subject: "Re: New app KLyX"
    date: 2005-02-05
    body: "LyX's WYSIWYM approach is great, in theory, but got on my nerves for it's lack of seamless support for the ever expanding LaTeX class macros and styles.\n\nI use Kile.  It works great.  It automates a lot of LaTeX and comparing the code between LyX and Kile I prefer being able to refine my LaTeX to reduce any bloat or dealing with under underfull \\vbox or overfull \\vbox errors.\n\nI never had issues being American for localization issues but I'd imagine the lack of broad internationalization support would be annoying.\n\nI'm going to revisit LyX 1.4 when it's completed.  Hopefully, they will have full Memoir class support and all the other classes Wilson develops.  I highly doubt it, but I'll still check on its progress, over time."
    author: "Marc Driftmeyer"
  - subject: "Re: New app"
    date: 2005-02-05
    body: "Wordperfect has done this for years. It works well and many people prefer being able to see the control codes. It gets unwieldy with very complex documents though.\n\nEvery time I've used MSWord I got frustrated and wonder why it has become the standard. I don't think it's fair to compare KWord with MSWord. The don't work the same way, even if the same formatting choices are available.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: New app"
    date: 2005-02-07
    body: "Take a look at TeXmacs (www.texmacs.org). Despite of its name it isn't based neither on Emacs nor on TeX but provides some features you describe. Such as real WYSIWYG, structured documents and so on. \n\nIt uses TeX (Metafont) Fonts for rendering. I'm not sure though if it supports Japanese...\n\nTheir community is also very helpful. \n\nI wrote my master thesis and many more smaller documents with it and even used it once to create a conference poster. "
    author: "Benno Dielmnn"
  - subject: "Naive bystander question"
    date: 2005-02-05
    body: "Good interview.\n\nHopefully this comment isn't taken as flamebait.\n\nIf KOffice is using OASIS as it's underlying format, how hard would it be to extract out the OO code that does import/export to take advantage of it?\nThat would make KOffice the ultimate linux crossover app IF it was lightweight, stable and could import Office docs.\n\nIs this too hard to do?\n\nSort of like this guy is suggesting that someone do to OO...\n( http://asay.blogspot.com/2005/01/open-source-free-markets-and-forking.html )\nDo the same thing with KOffice....\n"
    author: "foobie"
  - subject: "Re: Naive bystander question"
    date: 2005-02-05
    body: "Importers/exporters in KOffice are pretty much separated from the actual application logic. This is not at all the case with importers/exporters in OpenOffice (last time I checked) where you'd first separate the code clean before even thinging of taking some of it into KOffice. The necessary effort is better spent directly at KOffice's importers/exporters."
    author: "ac"
  - subject: "Re: Naive bystander question"
    date: 2005-02-05
    body: "hmmm. but think how nice it would be if you could run\nword2oasis *.doc\nand have all your word docs in oo native format (without having to go through OO)...\nI guess it's just a pipe dream."
    author: "foobie"
  - subject: "Re: Naive bystander question"
    date: 2005-02-05
    body: "There is an online converter (OpenOffice running on the server,\neverything automized with macros)\n\nCheck out here :\nhttp://www.oooconv.de\nhttp://oooconv.free.fr/oooconv/oooconv_en.html"
    author: "jmfayard"
  - subject: "OOo converters"
    date: 2005-02-08
    body: "They look quite nice, but I'm already on .odt (the OASIS Open Document Text used by OOo 2.0), which is not supported by this forms."
    author: "Quique"
---
As well as the <a href="http://www.fosdem.org/2005/index/schedule">official talks</a> at FOSDEM we will also be hosting 5 talks in the <a href="http://www.fosdem.org/2005/index/dev_room_kde">KDE developers room</a>.  The KDE FOSDEM team interviewed the speakers to get some background.  The first interview is with Raphael Langerhorst whose talk is titled <em>"KOffice - Desktop Integration and Workflow Automation"</em>.






<!--break-->
<div style="float: right; font-size: smaller; margin: 1em;"><img src="http://muse.19inch.net/~jr/raphael-langerhorst-with-nephew.jpg" width="300" height="409" /><br />Raphael Langerhorst with his Nephew</div>

<p><strong>Please introduce yourself and your role in KDE</strong></p>

<p>I was born in 1983 and I live in Austria, somewhere between Linz, Salzburg and Passau (Germany) in the countryside; I am also a vegan (strict vegetarian) from birth on. The quiet place we have at home is very precious to me and I enjoy it a lot - sitting in the garden or in the woods, reading books or... working with my laptop.</p>

<p>We got our first computer when I was 6 years old, a 286. Currently I'm studying IT in Wels, after having spent 5 years on an electrical engineering school (with "Matura") and doing my national service at the Red Cross. During the technical school we learned C, C++ and Java. I also started to work on a couple of projects during this time.</p>

<p>Well, KDE development started with a patch for KSysGuard in December 2003. Then I saw the summary about KOffice from the Kastle meeting. It mentioned the move to the OASIS Open Document format. Seeing the compatibility between KOffice and OOo in the near future caught my interest. So I joined the KOffice mailing lists to check the progress of this "feature". Since then I sent various patches to the mailing list (KSpread, documentation, ...) and got a CVS account in July 2004 I think. I never had much time left for KOffice though, which makes me feel quite sad since I'm probably one of those who wouldn't mind full-time KOffice development.</p>

<p>When I have time for KOffice I spend it on bug hunting and documentation. In the near future (depending on available time) I might do more QA (Quality Assurance) work for KOffice and even promotion work - I just started as a KDE trainer for one of the larger training institutes in Austria.</p>

<p>I also play the piano and the violin, which I enjoy a lot.</p>


<p><strong>What is the current state of KOffice?</strong></p>

<p>Getting close to the 1.4 release! But there is still much to do (see next question). The current state is a stable 1.3 version of KOffice which had been released a year ago and had already its fifth minor release.</p>

<p>Today KOffice offers a stable office suite for the standard components. There are a few rough edges left though, which are being worked on of course. KOffice is lightweight and thus performs very well even on old hardware. Still it brings lots of features and good KDE integration. In particular the KDE integration is one of its greatest strength, which is vital for business environments.</p>

<p>For home users KOffice is an easy to use, lightweight office suite for daily work.</p>


<p><strong>What have been some of the recent developments in KOffice?</strong></p>

<p>Many smaller improvements have been made since KOffice 1.3. The main focus since the last release has been the implementation of the OASIS Open Document file format. Krita and Kexi are also improving fast and might be included in the next release.</p>

<p>KPresenter has recently got master pages support. There were also some design improvements of KSpread during the last months, and more radical changes are planned after KOffice 1.4.</p>

<p>I think that the most important features for the near future are the OASIS file format support and the additional KOffice components, Krita and Kexi. All this could already happen for the 1.4 release, but there is still work to do.</p>

<p>And recently David Faure became the KOffice Release Manager again.</p>


<p><strong>Kexi has a Windows version available, do you think KDE applications should be ported to Windows and are we likely to see this happen to other applications?</strong></p>

<p>In fact there were a few people talking about creating a native KDE on Windows port and some work is already done.</p>

<p>Whether applications should be ported to Windows or not depends on the point of view. Windows is a rather different platform than POSIX which makes porting not too easy. A lot of opinions have already been expressed. I just want to add to it that the developers should care for clean KDE code. I personally don't want to see it happen that larger code portions are changed just to fit on Windows. I would rather prefer to build on POSIX compliant extensions to Windows (Microsoft has some available!!) and make these a requirement for KDE. This would surely result in less dirty code AND in better operating system standard compliance.</p>

<p>Some more individual applications will likely be ported, but I don't think that KDE will be ported completely in the near future.</p>


<p><strong>How many people are currently working on KOffice</strong></p>

<p>Well, there is Krita, Kexi and KPlato, which are more independent and even have their own mailing lists.</p>

<p>All together there are about 15 people working on KOffice. 1 for KPlato, around 4 working on Krita and about the same for Kexi. Then there are 3 to 5 developers improving the older KOffice applications like KWord, KSpread, KPresenter and Kivio. Sadly Karbon14 and Kugar are not well maintained because of lack of developers. Other smaller parts like the KOffice Workspace or KFormula also have no particular maintainer. Two or three people are working on documentation. Additional help is of course always welcome and appreciated.</p>


<p><strong>What is the current roadmap for KOffice and when will we see the promised native OASIS file formats?</strong></p>

<p>There are no particular dates set for releases yet, but so far we have decided to release KOffice 1.4 with KDE3/Qt3 early this year. The next major release will be KOffice 2.0 which will build on KDE4/Qt4.</p>

<p>Much work has been put into the OASIS Open Document format and most of the implementation is done. But it will be a close race with the 1.4 release, also because the file format really needs much testing before we can switch to it as native format. So far the OASIS file format is only for Karbon14, KWord, KSpread, KPresenter and KFormula since OOo does not have equivalent components of the other KOffice components. Thus there has not been much effort (or human resource) to add specifications for such applications. It might be possible that KPlato comes up with an OASIS specification for project management though.</p>

<p>Both Kexi and Krita will be included in KOffice 1.4. KPlato will probably be included with KOffice 2.0.</p>

<p>When this has been accomplished KOffice is a really complete office suite, having integrating components for text processing, spreadsheets, presentations, flow charts, pixmap and vector graphics, report generation, database frontend and project management as well as formula editing and a chart engine. Including a common workspace!</p>


<p><strong>How do you see the relation between OpenOffice.org and KOffice?</strong></p>

<p>First, I don't see a competition between these office suites. Both have their strengths and are suitable for different situations. A very important advantage of both office suites is their common file format in the near future. This will make seamless document exchange possible and it won't matter which office suite is in use.</p>

<p>A big advantage of KOffice is its KDE base, which makes it more lightweight and integrated. OOo brings its own framework which makes the codebase bigger and harder to maintain, but it is necessary to be cross platform. And this is what makes OOo more suitable in mixed environments - OOo builds the bridge between Windows and Linux/Unix whereas KOffice might be a better choice in pure KDE environments. OOo is also a suitable bridge between many legacy file formats and the OASIS Open Document format.</p>

<p>Seen this way OpenOffice.org and KOffice really complement each other rather than compete.</p>


<p><strong>KOffice is missing a logo, do you have any suggestions?</strong></p>

<p>Yes, actually. Recently there had been some KDE Look contests for various applications that had no logo. I think we could launch such a contest to have a logo ready for KOffice 1.4. The results produced in previous contests are really good quality.</p>

<p><em>Editors note: if you have an idea for a KOffice logo, which should be in the crystal theme, read <a href="http://lists.kde.org/?l=koffice-devel&r=1&b=200501&w=2">the recent discussion</a>, put it on <a href="http://www.kde-look.org">kde-look</a> and add it to <a href="http://www.kde.me.uk/index.php?page=missing-icons">this missing icons page</a>.</em><p>

<p><strong>Have you ever considered adding a talking paperclip to KOffice?</strong></p>

<p>Honestly, no :)</p>






