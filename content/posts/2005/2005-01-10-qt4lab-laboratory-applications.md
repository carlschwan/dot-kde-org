---
title: "Qt4Lab for Laboratory Applications"
date:    2005-01-10
authors:
  - "Paolo"
slug:    qt4lab-laboratory-applications
comments:
  - subject: "Qwt"
    date: 2005-01-10
    body: "How do this differ from Qwt? qwt.sourceforge.net. \nQwt has existed for several years, and looks fairly stable. It has already seen use in other projects. Python bindings are awalible.\n\nI think a project like this are better of cooporating, start adding to Qwt instead of starting from scratch. As a added bonus you get instant userbase."
    author: "Morty"
  - subject: "Re: Qwt"
    date: 2005-01-10
    body: "Thanks for your reply.\nI'm very glad about your offer and I would like to think a bit about what you'we written. Before deciding, I would like to ask you if qwt uses Qt Designer plugin technology and if qwt is interested in adding avionic widget plugins. You know, with this project, I would like to provide some flight instruments like FLIGHT DIRECTOR INDICATOR (you can have a look at the URL:\nhttp://www.allstar.fiu.edu/aero/FltDirS.htm.) and so on...\nAt the moment I'm writing some simple plugins just for learning Qt and it can appear my widgets are similar to qwt widgets, but for the future I would introduce some \"flight simulator\" specific widgets for SCADA application in the avionic industry (my best experienced field).\nThank you very much and please accept my compliments for your work.\nBest Regards\npaolo\n\n\n"
    author: "Paolo"
  - subject: "Re: Qwt"
    date: 2005-01-10
    body: "Sorry to say I can't take any credit for Qwt, as I am only a potential user:-) Having looked at it several times and concluded \"this has to be useful\", but since I have not had the time or any relevant project that's about all I have done:-( But since I know of it's existence I was curious.\n\nQwt provides Qt Designer pluggins for Qt 3.0 and greater according to their website."
    author: "Morty"
  - subject: "The competition"
    date: 2005-01-10
    body: "I searched for SCADA and Qt - also a few years ago - and got only one result\nhttp://www.pvbrowser.org/pvbrowser/\nGood to see others are now joining the effort.\nI work at a SCADA company. The old technology was OpenVMS/C/Motif. The new is  Linux/Java/Web. To compete, the Qt/Qt4Lab widgets will need to work from a web page - either as plugins similar to applets or using a mechanism similar to Java Web Start."
    author: "colesen"
  - subject: "Re: The competition"
    date: 2005-01-10
    body: "Good to know you are working for the same application field.\nThank you very much for your reply and for the idea.\nBest Regards,\npaolo"
    author: "Paolo"
  - subject: "SCADA security?"
    date: 2005-01-10
    body: "I noticed that the SCADA link above does not mention security.\nSee http://www.sandia.gov/scada/home.htm\n\nDoes Qt4Lab address security in any way?"
    author: "Paul Leopardi"
  - subject: "Re: SCADA security?"
    date: 2005-01-10
    body: "Thank you for your reply.\nYou gave me an interesting link. In fact I'm interested about SCADA security and I will introduce security aspects in Qt4Lab as soon as the plugins are stable and the framework will be a bit more solid (now I'm just starting the project and I'm learning to work with Qt)\nBest Regards\npaolo\n"
    author: "Paolo"
  - subject: "Re: SCADA security?"
    date: 2005-01-11
    body: "You seem so polite Paolo.  I'm not sure if that is allowed on this forum...atleast I've never seen it."
    author: "Henry"
  - subject: "Re: SCADA security?"
    date: 2005-03-31
    body: "can also check out www.verano.com"
    author: "Bob Conley"
  - subject: "Widgets are ugly"
    date: 2005-01-18
    body: "Hello,\n\nFrom the screenshots I found the widgets really ugly. Also, some are not necessary, as there are similar in Qt or KDE, for example, KDE has a led-like (replaces PLed), Toggle (checkbox, state buttons, ... replaces PSwitch) and goes...\n\nFor an example of beautiful widgets, look at: http://www.patrickkidd.com/skel/pksampler\nThe guy models widgets in Povray and render them with great lights and textures.\n"
    author: "Gustavo Sverzut Barbieri"
---
<a href="http://www.qt4lab.org/">Qt4Lab</a> is an extension to the <a href="http://www.trolltech.com/products/qt/index.html">Qt</a> application framework from <a href="http://www.trolltech.com">Trolltech</a>. This project will be developed according to the Open Source philosophy and will be distributed under LGPL. 
Qt4Lab provides <a href="http://doc.trolltech.com/3.3/designer-manual-7.html">widget plugins</a> and utilities for Rapid Application Prototyping for developing <a href="http://ref.web.cern.ch/ref/CERN/CNL/2000/003/scada/">SCADA</a> applications. The application field is automotive/aerospace.
Widgets plugins are developed on GNU/Linux and are available for GNU/Linux and Windows NT/2000/XP. Release 0.1.0 is now available (<a href="http://www.qt4lab.org/screenshots.htm">screenshots</a>).



<!--break-->
