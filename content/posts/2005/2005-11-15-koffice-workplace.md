---
title: "KOffice In The Workplace"
date:    2005-11-15
authors:
  - "canllaith"
slug:    koffice-workplace
comments:
  - subject: "Fonts?"
    date: 2005-11-15
    body: "From the interview:\n\n\"KOffice has some limitations regarding font kerning, which is a Qt 3 related issue (so I am told) and will be fully solved when moving to Qt 4 and KDE 4 in 2006. So there are some fonts that don't print very well, and some do. If you have some experience you know which fonts to use and then the printout looks as good as with any other office suite.\"\n\nApparently, I have never stumbled upon those good fonts. Does anyone that has \"some experience\" feel like sharing a list of some fonts that work well with the current KOffice?\n\nThanks in advance!"
    author: "Martin"
  - subject: "Re: Fonts?"
    date: 2005-11-15
    body: "/me never had an experience with bad fonts."
    author: "eol"
  - subject: "Re: Fonts?"
    date: 2005-11-15
    body: "Really, I would also like to know of those fonts that print well.  The default sans serif fonts do not print well, the only reason I still use gnumeric instead of kspread.\n\nthanks in advance\n"
    author: "Ask"
  - subject: "Re: Fonts?"
    date: 2005-11-15
    body: "In general, with Qt-3, Type 1 fonts print better unless you configure Qt to NOT embed fonts in the PS files.  Note that if you do this, that there are configuration issues to set up you system so that GhostScript finds the fonts.\n\nIt appears that the spacing issues only occur when you use proportional spaced fonts so with spread sheets, using a mono-spaced font such as Courier should print well."
    author: "James Richard Tyrer"
  - subject: "Re: Fonts?"
    date: 2005-11-16
    body: "it'd be cool if there was a FAQ entry or wiki page summarizing the DO's and DONT's"
    author: "lazy bastard"
  - subject: "Re: Fonts?"
    date: 2005-11-15
    body: "There are a couple of fonts that work well. Bitstream Charter works quite well, also URW Palladio (but has no \u0080 sign). The luxi fonts are not very good with KOffice. I would say that about half the fonts are actually useable. So it's probably a good idea to just test some of them - and if you want to save paper, just create PDFs, they look pretty much the same as real printouts.\n\nRaphael"
    author: "Raphael"
  - subject: "Re: Fonts?"
    date: 2005-11-15
    body: "I made good experience with the Nimbus fonts, which can be used instead of Times New Roman, Arial and Courier New."
    author: "Christian"
  - subject: "Re: Fonts?"
    date: 2005-11-15
    body: "I second this. Use Nimbus instead of msttcorefonts, and you'll get good quality both on screen and on paper."
    author: "-"
  - subject: "Arrows in KWord?"
    date: 2005-11-15
    body: "Is there any way to draw up quick arrows ( --> ) in KWord?\nWhen I recently tried KOffice to write some text with a few quick flowcharts in it, I found the only way is to embed a Kivio frame and do it in there (encountered some bugs on the way doing so, too).\nI found it rather cumbersome, especially when I only wanted to draw up like three arrows as KWord already provides textboxes, so I could do a quick chart also in KWord.\n\nI found OpenOffice Writer has these arrows and other basic drawing functions.. well, they don't get imported with OpenDocument in KWord. On the other hand, embedded Kivio frames don't show up in OpenOffice. But I also didn't find a way to save Kivio frames as Kivio-files, once imprisoned.. sorry embedded in KWord. only .kwd or other text files.\n\nTo get back to the lines and arrows, is this a missing function? By design?"
    author: "Phase II"
  - subject: "Re: Arrows in KWord?"
    date: 2005-11-16
    body: "Its a missing feature, not by design but by planning.\nWe intend to take a long look at KWord for the next release to make it more stable and better maintainable first.\nI'm guessing that for KOffice 2.0 this will be a feature on our list of TODOs"
    author: "Thomas Zander"
  - subject: "I love KOffice for its 64bits support"
    date: 2005-11-15
    body: "KOffice has 64bits native version.\nAlso has OASIS OpenDocument file format.\nIt works perfectly in KDE.\nIt's stable and faster.\nAnd for windows people , KOffice also open .doc files.\n\nI think is good alternative to MS Office.\n"
    author: "Marlo"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-15
    body: ">And for windows people , KOffice also open .doc files.\n\nUnfortunately it doesn't handle .doc files which imbed equations :-(\nThis is the *sole* reason I can't use Koffice at school, since the math teachers post all the assignment in MS format, and only OOo gets this [reasonably] right.. \nIf this gets fixed with 1.5 I will regain much diskspace, save much time, and stop pulling my own hair out because of an ancient and idiocratic UI! :-D\n\nCheers, and thanks for all the wonderfull fish!\n\n"
    author: "Macavity"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-12-01
    body: "> Cheers, and thanks for all the wonderfull fish!\n\nShouldn't that be spelt \"fish://\" ? \n\n\nSCNR. "
    author: "cm"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-15
    body: "Sure it's faster, perfectly integrated in Kde, but it's not that stable.\nSome days ago i started writing something on kword, it crashed after about 2 minutes, then I reopened it and it crashed again after just three lines.\nKoffice has still a long way to go, but surely it will improve, it just need some time and Qt4."
    author: "`Diablo`"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-16
    body: "I think I had 2 crashes of KWord this whole year - and I use KWord heavily. So there must be something you do differently than I do (or I don't do), can you please please please tell us what makes KWord crash for you? In a reproducable way, otherwise it cannot be fixed (if the devs don't know what is wrong...).\n\nMeanwhile KOffice is not lacking much in terms of stability anymore. So it is very important that crashes and such serious things must be reported to get those few cases where it still crashes. The best way is to submit a bug report to bugzilla, and add a backtrace if you have one.\n\nAnd yes, Qt 4 will certainly bring various improvements.\n\nThanks a lot."
    author: "raphael"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-16
    body: "I've found the problem, so now I should be able to get rid of openoffice and use just koffice. It was a problem related to the aspell version i was using, I've found a bugreport on gentoo bugzilla. I should have took a look to that before."
    author: "`Diablo`"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-16
    body: "wow - this is crazy. never heard about that before...\nwhat is the bug number/link?\n\nperhaps it should become part of a FAQ. to bad koffice gets a bad reputation because of these incompatibilities..."
    author: "bangert"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-16
    body: "Here it is the link to gentoo's bugzilla: http://bugs.gentoo.org/show_bug.cgi?id=110339.\nI did it as reported there and it's working flawlessly now. "
    author: "`Diablo`"
  - subject: "Re: Crash reports"
    date: 2005-11-16
    body: "I could reproducably crash KWord - later on after some usage the crash number dropped a bit, probably because I remembered to work around the bugs..\n\nAnyway, are normal crash reports without debugging symbols useful enough for reporting, or only crashlogs from debugging versions?\n\nI hope you know what I mean.. or wait while I make KWord crash..:\n\"\n(no debugging symbols found)\nUsing host libthread_db library \"/lib/tls/libthread_db.so.1\".\n(no debugging symbols found)\n`system-supplied DSO at 0xffffe000' has disappeared; keeping its symbols.\n(no debugging symbols found)\n(no debugging symbols found)\netc.\n[Thread debugging using libthread_db enabled]\n[New Thread -1231898944 (LWP 23141)]\n(no debugging symbols found)\n(no debugging symbols found)\netc.\n[KCrash handler]\n#3  0xb5ff1d5e in KoRect::setCoords () from /usr/lib/libkofficeui.so.2\n#4  0xb61ef728 in KWFramePartMoveCommand::unexecute ()\n   from /usr/lib/kde3/libkwordpart.so\n#5  0xb600c0f9 in KoCommandHistory::undo () from /usr/lib/libkofficeui.so.2\n#6  0xb6123ee8 in KWCommandHistory::undo () from /usr/lib/kde3/libkwordpart.so\n#7  0xb600cef8 in KoCommandHistory::qt_invoke ()\n   from /usr/lib/libkofficeui.so.2\n#8  0xb7349c5f in QObject::activate_signal () from /usr/lib/libqt-mt.so.3\n\"\n...and so on. Happened while I was just messing around with a Kivio box in KWord. Would this be useful in a bug report with a short description?\n\n"
    author: "Phase II"
  - subject: "Re: Crash reports"
    date: 2005-11-17
    body: "Yes it would.  Including version numbers and all that please."
    author: "Thomas Zander"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-11-30
    body: "It's ok that Koffice opens .doc documents, but...what about writing to them? Opening a document is of no use if I can't write back changes I made...."
    author: "cafeina"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-12-01
    body: "KOffice writes .doc documents in the same way Microsoft implemented saving to Word 6 from Word 2000: as .rtf documents. Yes, that's right. Even MS does not have export filters for their various versions of Word, they use rtf but glue a .doc extension onto the filename. We plan to do the same for KOffice 1.5. Nobody will notice because rtf has exactly the same feature set as .doc."
    author: "Boudewijn"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-12-01
    body: "That's interesting... But if rtf and doc have both the same feature set, why do they both exist? Why M$ office doesn't use rtf instead of doc? It uses both, doesn't it?"
    author: "cafeina"
  - subject: "Re: I love KOffice for its 64bits support"
    date: 2005-12-01
    body: "Since .doc is basically a memory dump it's a lot faster than rtf. That's one reason, I guess. And I'm not sure whether embedded documents survive in rtf, I guess not."
    author: "Boudewijn Rempt"
  - subject: "Documents look different in KWord and OOwriter"
    date: 2005-11-16
    body: "A text document (with formatting) would look different (one page document in openoffice would become 2 page in kword).\n\nthis is due to using 75dpi by kword and 96dpi by openoffice and msoffice.\n\nshouldn't we set 96dpi by default for KDE X resolution for seamless text visibility among different applications?"
    author: "fast_rizwaan"
  - subject: "Re: Documents look different in KWord and OOwriter"
    date: 2005-11-16
    body: "KOffice uses the same dpi as the x server, so if you want KOffice to use another dpi change the dpi of the x server."
    author: "Peter Simonsson"
  - subject: "Re: Documents look different in KWord and OOwriter"
    date: 2005-11-17
    body: "The DPI setting just adjusts the size you see an A4 on screen at 100% zoom, it does not actually change the amount of text on that fits on there.\n\nThis is because a font is measured in points; which can be converted to millimeters just like the size of an A4 can be, both without knowing the DPI of the server.  And calculating the amount of text is therefore done without looking at the DPI.  Only when its going to be shown on screen will that be used.\n\nIf things don't fit; I think you have to look elsewhere.  Perhaps the font used in one application is not present in another."
    author: "Thomas Zander"
  - subject: "koffice cannot display equations in a MSWord file."
    date: 2005-11-16
    body: "koffice is just good. It is not only fully funcitonal but also\nstable. But it cannot display equatons in MS word file and it's\nthe reason I couln't choose it."
    author: "yd"
---
In an interview with KDE's Jess Hall, Raphael Langerhorst <a href="http://www.canllaith.org/articles/koffice-raphael-interview.html">talks about his experiences</a> using KOffice in a Microsoft dominated world. 

<i>"The question everyone is asking is, are Open Source office suites really able to survive in a Microsoft world? KDE developer Raphael Langerhorst's experience at home, university and in the business world shows us the future is bright for KDE's KOffice suite."</i>



<!--break-->
