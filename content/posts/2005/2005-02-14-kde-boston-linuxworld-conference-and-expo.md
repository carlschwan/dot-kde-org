---
title: "KDE at Boston LinuxWorld Conference and Expo"
date:    2005-02-14
authors:
  - "gstaikos"
slug:    kde-boston-linuxworld-conference-and-expo
comments:
  - subject: "So George"
    date: 2005-02-14
    body: "You consider yourself an east-coast US KDE hacker now?  What's with the candadian self-loathing? ;)"
    author: "manyoso"
  - subject: "Re: So George"
    date: 2005-02-14
    body: "Hm I didn't notice that I wrote US. :-)  No problem..."
    author: "George Staikos"
  - subject: "Re: So George"
    date: 2005-02-15
    body: "Oops, that was me added that.  Wanting to remove the ambiguity of which east coast it was I forgot that the coast is shared by more than one country.  Hope that didn't offend anyone."
    author: "Jonathan Riddell"
  - subject: "Re: So George"
    date: 2005-02-15
    body: "Indeed I was confused. :-)  Really no problem though..."
    author: "George Staikos"
  - subject: "Go KDE!"
    date: 2005-02-14
    body: "Wish I could visit, it's quite a long drive from Detroit. Keep up the good work! Perhaps I'll see y'all in NY (May 25-26) or San Francisco (August 8-11)."
    author: "Claire"
  - subject: "KDE 3.4b2 on SUSE 9.2"
    date: 2005-02-14
    body: "Now, someone please explain this to me. Kget just got stuck, and:\n\n:~> pidof kget\n31901\n:~> kill -15 `pidof kget`\n:~> kill -9 `pidof kget`\n:~> kget\nkget is already running!\n:~> pidof kget\n31901\n:~> ps -ef | grep kget\njmk      31901     1  0 Feb10 ?        00:00:18 kget\n\nWTF? I can certainly understand that it would get automatically restarted,\nbut how would it get exactly the same PID again? SUSE kernel broken?\n"
    author: "jmk"
  - subject: "Re: KDE 3.4b2 on SUSE 9.2"
    date: 2005-02-14
    body: "It didn't die when you killed it. \n"
    author: "Chakie"
  - subject: "Re: KDE 3.4b2 on SUSE 9.2"
    date: 2005-02-14
    body: "su -\nrm `which kget`\nshutdown -r now\n\nHTH"
    author: "Asdex"
  - subject: "Re: KDE 3.4b2 on SUSE 9.2"
    date: 2005-02-15
    body: " Dont do the above unless you understand what the command means (in wich case you wouldnt do it any way)... 8-D"
    author: "WARNING"
  - subject: "KDE strength"
    date: 2005-02-16
    body: "From list of nominees looks like only KDE community can stand in rivalry\nagainst commercial offerings.\n"
    author: "m."
  - subject: "Re: KDE strength"
    date: 2005-02-16
    body: "But it did win: nothing."
    author: "Anonymous"
---
Once again KDE will be exhibiting at the <a href="http://www.linuxworldexpo.com/">LinuxWorld Conference and Expo</a>.  This year the show has moved to the <a href="http://www.mccahome.com/hynes/default.aspx">Hynes Convention Center</a> in Boston and KDE can be found at booth <em>.ORG#3</em>.  Drop by for a demo of KDE 3.3 or the latest 3.4 beta 2, and meet some of the east-coast US KDE hackers.  The show floor is open from tomorrow (Tuesday Feb 15) until Thursday.  KDE is also a finalist for 2  <a href="http://www.linuxworldexpo.com/live/12/media//news/CC93466">Product Excellence Awards</a> for KDE 3.3 and Kopete.



<!--break-->
