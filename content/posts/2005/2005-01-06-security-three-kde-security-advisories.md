---
title: "Security: Three KDE Security Advisories"
date:    2005-01-06
authors:
  - "binner"
slug:    security-three-kde-security-advisories
comments:
  - subject: "Wow!"
    date: 2005-01-05
    body: "Wow, I'm already using since yesterday evening a patched KDE version, since Gentoo was really rapid to provide a new kdelibs ebuild!\nAnd, about the FTP injection, Internet Explorer suffers the same bug..let's see how rapid MS will be....*g*"
    author: "Davide Ferrari"
  - subject: "SP2"
    date: 2005-01-05
    body: "In Service Pack 2 the error is already fixed."
    author: "Bill the Weasel"
  - subject: "Re: SP2"
    date: 2005-01-06
    body: "So no good for users of older versions of MS Windows then...?"
    author: "DFJA"
  - subject: "Re: SP2"
    date: 2005-01-06
    body: "... Were previous builds of KDE fixed too?\n(From a Linux fan, windows user)\nCheers"
    author: "Luca Piccarreta"
  - subject: "Re: SP2"
    date: 2005-01-06
    body: "KDE does not release pre-built packages, but patches for fixing the problems found were released for KDE 3.2."
    author: "Henrique"
---
Three <a href="http://www.kde.org/info/security/">security advisories</a> have been issued by the <a href="mailto:security@kde.org">KDE Security Team</a> over the last days for two distinct vulnerabilities that have been found: all KDE releases up to and including KDE 3.3.2 are vunerable to a <a href="http://www.kde.org/info/security/advisory-20050101-1.txt">FTP KIO Slave Command Injection</a>. Another <a href="http://www.idefense.com/application/poi/display?id=172&type=vulnerabilities&flashstatus=true">xpdf Buffer Overflow</a> has been found affecting <a href="http://www.kde.org/info/security/advisory-20041223-1.txt">kpdf in all KDE versions</a> and also <a href="http://www.koffice.org/security/2004_xpdf_integer_overflow_2.php">all KOffice 1.3 versions</a>.


<!--break-->
