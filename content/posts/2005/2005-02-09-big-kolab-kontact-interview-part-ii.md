---
title: "The Big Kolab Kontact Interview - Part II"
date:    2005-02-09
authors:
  - "fmous"
slug:    big-kolab-kontact-interview-part-ii
comments:
  - subject: "thanks!"
    date: 2005-02-09
    body: "I was the guy with the sync-questions after the first part of this interview. Obviously, I now am enlightened. Thank you very much!"
    author: "me"
  - subject: "Interviews"
    date: 2005-02-09
    body: "I can't tell how much i like those interviews with the kde developers! Keep them coming, guys!"
    author: "Anonymous"
  - subject: "Re: Interviews"
    date: 2005-02-11
    body: "Ditto! They give invaluable information on both how things work community-wise and where to start coding (once i feel competend in C++.. sigh :-)... Besides the fact that its just fun reading in general.\n\n More Of Those Please(TM)!\n\n~Macvity\n\n--\n\"If I am able to see further, it is only because I am standing on the shoulders of giants\"."
    author: "Macavity"
  - subject: "Application integration"
    date: 2005-02-09
    body: "The disadvantage of integrating previously separate applications in a thin shell is that annoying differences remain. In the case of kontact, the differnences between KMail and KNode are especially annoying:\n\n* Different widgets for the treeview, folder and message panes. \n* Different keybindings for moving between messages (which wouldn't be so bad if customized keybindings weren't removed when upgrading to a new version).\n* Different focus model for the three panes.\n* Different header display.\n* Different menu layout for things like mesage threading.\n* Different approach to multi-threading -- KMail and KNode hang in different ways when fetching new messages, and besides, don't have the same progress widget in the statusbar.\n* KNode doesn't save its read status for messages when it isn't show on close; KMail does.\n\nAnd with the addition of Akregator, a completely new treeview has been added.\n\nOn the other hand, since about half a year or so, Kontact seldom crashes on me anymore, and it _is_ convenient, having all these applications in one window, so I still run it. And in any case, as long as Karbon and Krita don't share even a single design element, I shouldn't complain...\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "I guess you have send in bugreports? and if you or someone you know can be persuaded to help... its clear they can use some coding fingers :D"
    author: "superstoned"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "But are these bugs? I'm not sure: none of the issues is a problem with the component taken on its own, it's only in Kontact that apps clash. And even then, nothing breaks, I just get magically transported to the first posters whose name begins with a \"B\" if I want to go back to the previous message.\n\nIn any case, I'm already spend pretty much all my copious free time coding for KDE..."
    author: "Boudewijn Rempt"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "well, thanx, for the work... these things are very small problems, if at all, so I don't really mind :D"
    author: "superstoned"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "They count as Kontact usability bugs IMHO. File them, so that the respective developers are aware of the issue, and let someone with more time handle them for you (even if it's just a WONTFIX)."
    author: "Simon Farnsworth"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "I second that! Thunderbird 'integrates' Mail/Newsgroups/Feeds much nicer (if only it was based on Qt...).\nBut KMail is certainly not \"the best Open Source e-mail program existing today\". It's good, even wonderful at times, but Thunderbird is simply better."
    author: "an"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "Sorry, but you can't compare KMail with Thunderbird because Thunderbird is not an email client. It's an email-newsgroup-and-news-feed reader while KMail is \"only\" an email client. Why the Thunderbird developers decided to add functionality to Thunderbird which has absolutely nothing to do with email beats me. I thought they wanted to create a nice slick email client, but now they are again heading straight for the next big fat all purpose application just without the web browser functionality."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Application integration"
    date: 2005-02-10
    body: "Well.... Back in 1991, when I was still using Windows I got Internet access through uucp. Only, there was no decent email client nor a decent usenet client for Windows + uucp at the time. The most wide-spread apps actually deleted mail when it was read...\n\nSo I started coding, and created Dante. It was a combined usenet & email reader where folders could be usenet groups or mail folders; I reasoned that there was little actual difference between a mailing list and a usenet group except that for a usenet group you'd subscribe with your ISP's nntp server, and for a mailing list you'd need to send a subscribe mail to the mailing list admin address. I abstracted that difference away. Dante had a long treeview to the left, a subject/thread pane top-right and a message pane bottom-right...\n\nMind you, that was way before I had ever seen a GUI email client or any usenet reader except for Helldiver. There must be something really basic about the idea of combining both and about the three-pane layout that a complete newbie like me intuitiviely designed his application that way.\n\nOf course, when Dante was feature-complete and virtually bug-free, in 1993, I started to use Linux and had to go back to pine and tin, until KNode and KMail seduced me. I still use pine when I check mail from another computer than my main laptop, but I've never used anything else than KNode.\n\nI don't even hanker after having my mailing lists and usenet groups in the same view, anymore -- but it was the natural approach for me, when I was a newbie who had only ever done a little SNOBOL and Pascal at university."
    author: "Boudewijn"
  - subject: "Re: Application integration"
    date: 2005-02-10
    body: "I'm sorry but I have to disagree with you. KMail is a much better e-mail program. It simply has better features: integrates with more spam filters, has much better gpg support, better scriptability, and so on..."
    author: "Jonas"
  - subject: "Re: Application integration"
    date: 2005-02-10
    body: "Ya, I gotta totally disagree with you here.  Kmail wipes the floor with thunderbird.  Thunderbirds IMAP support is spectacular but on any other feature for feature comparision thunderbird looses to kmail\n\nBobby"
    author: "brockers"
  - subject: "Re: Application integration"
    date: 2005-02-11
    body: "Don't forget to consider how useful some of the features actually are for most people. ;-) It could be KMail has stuff which is there but not hugely useful in general.\n\n(Note that I'm not making claims either way - I use Thunderbird at work and mutt at home. The couple of times I tried KMail, I just didn't get on with it much, so I don't use it. Probably my fault more than KMail's.)"
    author: "Paul Walker"
  - subject: "Re: Application integration"
    date: 2005-02-12
    body: "Heh, I'm the exact opposite.  I can't live without KMail.  I tried both mutt and Thunderbird for a while, but always went back to KMail.  Its disconnected IMAP implementation is the \"best\" imo and I like its UI very much.  There are probably 10 \"small\" things I like about KMail that I can't remember now, but will remember if I start using another client.  Kudos to the devs."
    author: "Christopher J. Bottaro"
  - subject: "Re: Application integration"
    date: 2005-06-14
    body: "thunderbird can't beat kmail...thunderbird is heavy.(If you are in kde, kmail is much faster).and the news etc is useless for average user...I tried many email clients (actually i didn't like kmail first) at last i found that kmail is the great."
    author: "Renjith Rajan"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "KMail and KNode are converging, albeit extremely slowly due to the lack of developers on the side of KNode (which is currently a one-man-show if at all). Volker is doing what he can. Instead of complaining you should better thank Volker for what he has already done or, even better, give him a hand."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "Ingo, not every remark that's not praise is \"complaining\". It might -- just -- be an analysis of the way things currently are. On the other hand, telling people to shut up or do some coding every time they are even slightly critical of kontact is little more than a knee-jerk reaction, and not very likely to gain you new developers.\n\nI like KNode, I like KMail. I've been using both for years; I just feel that the combination of the two in Kontact is not quite worth a panegyric. Yet.\n\nAnd sure, it's impressive what one man or woman can do; I'm fortunate that I'm not the only one hacking on Krita or it would never get anywhere. Anyway, in a whole as large as KDE it is inevitable that there are bits and pieces that one feels are not quite done yet; and it is equally inevitably that one cannot hack on everything. Does that mean that one should limit remarks to ones own bit of code?"
    author: "Boudewijn Rempt"
  - subject: "Re: Application integration"
    date: 2005-02-10
    body: "Perhaps it would help to mention that most of the development efforts in KDE PIM and especially in KMail went into the integration and groupware functionality. There has been few capacity for visual improvements which were not coupled to these two issues.\n\nThere seem to be a lot of people out there who don't understand that the developers see a higher priority of the infrastructure at the moment, while minor GUI changes can wait for future releases. Reading the very same complaints (sorry - remarks) again and again is frustrating. There are enough wishes on bugs.kde.org for KMail. People should have a look and vote. That's it. Telling the developers again and again won't help if there's a lack in capacity."
    author: "Andreas"
  - subject: "Re: Application integration"
    date: 2005-02-10
    body: "I promise you, this is the last time I'll mention these few little niggles."
    author: "Boudewijn"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "> And with the addition of Akregator, a completely new treeview has been added.\n\nIt's actually code ripped out from kmail.. abeit.. one mode of kmail's treeview."
    author: "anon"
  - subject: "Re: Application integration"
    date: 2005-02-09
    body: "But it uses the alternate listview colour for alternate lines; which KMail doesn't..."
    author: "Boudewijn Rempt"
  - subject: "nice supprise"
    date: 2005-02-09
    body: "nice supprise to see my logo on this page :)"
    author: "cies breijs"
  - subject: "Re: nice supprise"
    date: 2005-02-09
    body: "I knew you would like that"
    author: "fab"
  - subject: "Don't shoot me"
    date: 2005-02-09
    body: "What about syncing with Windows CE / PocketPC / SmartPhone devices. I have both a Compaq ipaq (PocketPC 2003) and an Orange SPV-E200 (Windows SmartPhone) and mobile Windows devices are very common.\n\nSyncing with SynCE is not entirely straight forward and similar syncing to that possible with Palm would be a great plus.\n\nIan"
    author: "Ian Ventura-Whiting"
  - subject: "Kontact DOES NOT sync with egroupware"
    date: 2005-02-09
    body: "I must take a very strong exception with the claim that Kontact currently supports eGroupaware. I wish it did, but it doesn't.\n\nAs per the kontact application shipped with Suse 9.2 and Mandrake 10.1, the syncing with egroupware is broken. Creating a calendar entry, copies it to the server once, then twice, then four times and on and on until eternity.\n\nIf this is a configuration issue, I'd love to hear from anyone who has managed to make Kontact sync with egroupware reliably.\n\nOther than this, I love Kontact. If I could get it to work reliably with egroupware I could move tons of people over to a linux desktop.\n\nThanks for your good work.\n\nGonzalo"
    author: "Gonzalo"
  - subject: "Re: Kontact DOES NOT sync with egroupware"
    date: 2005-02-10
    body: "IIRC Suse 9.2 shipped KDE 3.3.0. Have you tried e.g. KDE 3.3.1? Have you contacted the developers? And keep in mind, they usually talk about the ongoing efforts, i.e. you should perhaps test KDE 3.4 Beta 1 or 2. You don't mention a specific version. But I doubt that the eGroupware support doesn't work in generall.\n"
    author: "ac"
  - subject: "Re: Kontact DOES NOT sync with egroupware"
    date: 2005-02-10
    body: "I cannot deploy a beta widely to users and I can only deploy something that comes with the backing of one of the distributions.\n\nThis is how the real world works. If something doesn't work at all, it should not be included, period. What's the sense of including fuctionality that is only going to get people down.\n\nThat's how i feel about it. And this is not a complaint about KDE in general, which I have always used and love. It is a specific complaint about the fact that in the stable versions of KDE today, egroupware-kontact do not sync or work well together."
    author: "Gonzalo"
  - subject: "Re: Kontact DOES NOT sync with egroupware"
    date: 2005-02-10
    body: "So what version do you use in fact? Suse 9.2 can have security patches applied. You can use their LinuKS service to get the latest stable KDE packages as well. At the moment they provide KDE 3.3.2 and it's quite easy to keep up to date even using YaST.\nStill you generally tell us that the sync with eGroupware doesn't work. Sure, it is limited, but perhaps your expectations were to high or you missed something? I think it's a bit hard to tell people that their software doesn't work at all if you cannot back it with some simple details."
    author: "ac"
  - subject: "Re: Kontact DOES NOT sync with egroupware"
    date: 2005-02-11
    body: "You'll be mad at me for bouncing you like a ping-pong ball ;) but this really is a distro problem. The major rule of open-source development is \"release early, release often\". It is completely normal to have broken functionality in FLOSS because if noone uses it then noone will want to send patches. If Novell/Suse think that it is not ready for release, they should remove eGroupware support from their packages.\n\nYou should complain to Novell/Suse or report a bug or support request with them."
    author: "Ted"
  - subject: "Re: Kontact DOES NOT sync with egroupware"
    date: 2005-04-09
    body: "I have the exact same problem and I think we must misuse it in some way. There probably is somewhere an option that allows us to be kept as the organizer of an event so that it's not recognized as a new event every time kontact tries to get data from eGroupware.\n\nOther than that, you should not forget that it does not sync data. If you cut the connection with the server you're left with nothing and since eGroupware does not export its Calendar data to the iCal format, you have to find and use an application that interrogates eGroupware using xmlrpc and converts it to iCal or acts as a syncml server. Not very convenient."
    author: "Cheapo"
---
<a href="http://dot.kde.org/1106909457/">As promised</a> KDE Dot News brings you the second part of the two-part interview about <a href="http://www.kontact.org/">Kontact</a> and <a href="http://www.kolab.org/">Kolab</a>. This part focuses on Kontact, the Personal Information Management suite for KDE made with the combination of KMail, KOrganizer, KAddressbook and other programmes.

<!--break-->
<!--[ About Kontact ]-->


<p align="center"><img src="http://www.kontact.org/pics/kontact_logo.png" alt="Kontact logo" border="0" width="450" height="90" /></p>

<p><strong>As Kontact is the Kolab client on the KDE platform how did Kontact happen? 
Which parties were involved and were there any sponsors involved?</strong></p>

    
<p><strong><em>Cornelius Schumacher</em></strong>: Kontact actually has a long history. It started as an experiment in 2000 
under the name <A href="http://webcvs.kde.org/kdepim/twister/">Twister</A>, was reintroduced in 2002 as <A href="http://webcvs.kde.org/kdepim/kaplan/">Kaplan</A> and finally 
started to become what it is today in 2003. Maybe the biggest single 
step to make Kontact happen was Don Sanders porting KMail to provide
a KPart for integration in Kontact.</p>

<p>The nice thing about Kontact is that it is only a very thin integration 
layer on top of the existing PIM applications which doesn't sacrifice 
the ability of the application to run stand-alone. So most integration 
is happening in the applications themselves and not in the Kontact 
framework. Details about the technical and social aspects of
Kontact as an application integration framework can be found in the <a href="http://www.kontact.org/files/kontact_freenix_paper.pdf">paper 
I presented</a> at the USENIX conference 2004.</p>   




<p><strong>What groupware servers does Kontact support?</strong></p>


<p><strong><em>Cornelius Schumacher</em></strong>: In addition to Kolab it supports <a href="http://www.egroupware.org/">eGroupware</a>, 
<a href="http://www.novell.com/products/openexchange/">SUSE OpenExchange Server</a> and more recently support has been added for the <a href="http://www.opengroupware.org/">OpenGroupware server</a>.</p>

<p>It also supports iCalendar over HTTP. That's what Apple iCal 
uses. It's hardly groupware, but it's useful for a lot of nice things 
(see e.g. <a href="http://www.icalshare.com">http://www.icalshare.com</a>). </p>

<p>Kontact now also supports <a href="http://www.novell.com/products/groupwise/">Novell GroupWise</a>. Thanks to the quality 
of the KDE framework it took us only <strong>one</strong> week to implement the basic 
functionality.</p>

<p>There is also some basic support for MS Exchange.  We will certainly see support for even more groupware servers in the future.</p>
    
<p><strong>What happened to support for Exchange2000 calendering in Kontact?
     </strong></p>

<!--[http://www.microsoft.com/sharepoint/]-->

<p><strong><em>Tobias Koenig</em></strong>: At the moment we have a halfway working solution for calendaring with Exchange2000. Since Ximian has released the source code of their Exchange connector it would be quite easy to write a connector for Kontact as well, but it seems no KDE developer is willing to do so at this moment.</p>

<p><strong><em>Cornelius Schumacher</em></strong>:  It is not missing much, so indeed if somebody would step up and take care of this code we would have Exchange support in Kontact in no time.  For KDE 3.4 this has been rewritten with a cleaner code base and is ready for trying out.</p> 
    
<p><strong>Why did you choose to reuse existing applications like KOrganizer, KPilot, 
     KMail, <a href="http://www.kaddressbook.org/">KAddressbook</a>, KNotes, KNode, etc .. for Kontact instead of building a new 
     application from scratch?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: KMail is the best Open Source e-mail program existing today and with KOrganizer and KAddressBook
    we already had two other important parts of a PIM solution. So why should we start from scratch?
    With KParts, XML-GUI and DCOP, KDE offers an incredibly cool framework which made it quite easy
    to integrate these applications without throwing away existing and well tested code, so it was
    clearly the way to go.</p>

<p><strong><em>Cornelius Schumacher</em></strong>: Tobias is right. All these existing applications are great on their own. 
They have healthy 
code bases and developer communities. To throw this away for a new 
application written from scratch would have cost us many years of 
development effort. The KDE component technologies make it so easy 
to integrate the applications there is no doubt that the Kontact 
approach is the right way.</p>  

    
<p><strong>What kind of syncing technology is used now in Kontact? What happened to <a href="http://kandy.kde.org/">Kandy</a>?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: For KDE 3.3 we introduced KitchenSync as a syncing framework for Kontact and every other
KDE application. KitchenSync is plugin based, so every developer can implement their own
plugin to add syncing support for new mobile devices.</p>

<p><strong><em>Cornelius Schumacher</em></strong>: We already have <a href="http://www.kpilot.org">KPilot</a>, which does a really nice job synchronizing Palm OS 
devices with KDE, and KitchenSync. KitchenSync is supposed to be the 
future all-purpose syncing solution, but it still has a long way to go 
before that becomes reality.</p>

<p><strong><em>Tobias Koenig</em></strong>: The development of Kandy has nearly stopped, as soon as the KitchenSync API has been stabilised,
Kandy will be rewritten as a KitchenSync plugin.</p>

<p><strong><em>Cornelius Schumacher</em></strong>: That's also the reason why I didn't spent much time on it as 
a stand-alone application anymore. Some time ago I received a large patch for Kandy, So it looks 
like it could get some life of its own again.</p>


   
<p><strong>Are there any plans to integrate other syncing technologies so it can sync with 
     types of handhelds other than just Palm Pilots?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: Of course, there is already a half working plugin to sync devices with the <a href="http://www.trolltech.com/products/qtopia/">Qtopia</a>/<a href="http://opie.handhelds.org/">OPIE</a> environment and support for SyncML is also in progress.</p>
    
<p><strong><em>Cornelius Schumacher</em></strong>: There might be some opportunities for 
working together with other projects on some common syncing framework 
(e.g. the <a href="http://www.freedesktop.org/Software/OpenSync">OpenSync initiative</a> on <a href="http://www.freedesktop.org">freedesktop.org</a>), but it's hard to 
keep track of what's happening in these areas. There are just too many 
and too special projects.  </p>  


<p><strong>How is Kitchensync progressing and how will it address syncing (in the future).
     What possiblities does it offer?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: With the Qtopia/OPIE, <a href="http://multisync.sourceforge.net">MultiSync</a> and SyncML plugins, we'll be able to sync nearly all kinds of
mobile devices. PIM data from Palm Pilots can be synchronized with the KDE desktop via KPilot,
but we plan to integrate it into KitchenSync for KDE 4.x.</p>
    
<p><strong><em>Cornelius Schumacher</em></strong>: As KitchenSync is supposed to become a generic syncing framework which can 
easily be extended with new plugins to support any specific device. The 
KDE 3.3 release contains an initial version which is able to 
synchronize addressbooks and calendars between KDE desktops. There is 
more to come, but as syncing is a really hard and ugly problem it's not 
progressing as fast as we all would like to see.</p> 


    
<p><strong>What do the Kolab and Kontact projects need the most now?</strong></p>

<p><strong><em>Tobias Koenig</em></strong>: We are always in need of developers and users for testing. Kontact needs a lot of usability improvements, with the help of
    <a href="http://www.openusability.org/">OpenUsability.org</a> we are currently working on it, further help is always appreciated.</p>
    
<p><strong><em>Cornelius Schumacher</em></strong>: Contributors. There are so many nice ideas which could be implemented, 
if we only had some more people doing actual work. The KDE framework 
makes it possible to do development in an extremely efficient way. It's 
amazing to see how far we have come with Kontact when taking account 
how few people we are and that most of the work is done in our 
spare time. That's only made possible by the high quality of the 
framework and the unique spirit of the KDE community. It's inspiring to 
work on Kontact and it's also a lot of fun. Everybody is welcome to 
join us and share the experience.</p>


<p><strong><em>Bernhard Reiter</em></strong>: Our next challenge will be to build a commercial (Free Software) infrastructure behind Kolab to grow it in the market and go the next steps with development. This requires people to be fair and give back financially some of the value they get when deploying Kolab, especially in larger installations.</p>
    
<p><strong>Where can people get more information about Kolab and Kontact?</strong></p>

    
<p><strong><em>Cornelius Schumacher</em></strong>: Point your browser to <a href="http://www.kontact.org">www.kontact.org</a> and 
<a href="http://www.kolab.org">www.kolab.org</a>.</p>

