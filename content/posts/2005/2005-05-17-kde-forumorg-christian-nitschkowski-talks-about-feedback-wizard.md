---
title: "KDE-Forum.org: Christian Nitschkowski Talks About Feedback Wizard"
date:    2005-05-17
authors:
  - "ckienle"
slug:    kde-forumorg-christian-nitschkowski-talks-about-feedback-wizard
comments:
  - subject: "Is HTML dead?"
    date: 2005-05-17
    body: "I'll comment here as I can't be bothered to register yet another forum account. Subject say it all really: what's the point of distributing this a PDF?"
    author: "Rob"
  - subject: "Re: Is HTML dead?"
    date: 2005-05-17
    body: "Have a look: \n\nhttp://www.qtforum.org/interview/interview.html"
    author: "Christian Kienle"
  - subject: "Re: Is HTML dead?"
    date: 2005-05-17
    body: ">> what's the point of distributing this a PDF?\n---\nAnother question one could ask: What's the point of creating that PDF on a Mac OS X system?\n\nThe answer, as I assume could be: because the content (the interview) was created on a Mac in the first place. The reason being, that the author thinks that Linux programs are *still* inferior to Mac programs. Or, maybe, perhaps, it is just that this week his current Scribus HEAD (selfcompiled) installation is broken?  "
    author: "anonymous"
  - subject: "Re: Is HTML dead?"
    date: 2005-05-17
    body: "Is everything else KDE based apps evil? :) \n\nI use the best of every world - Linux/Windows/Mac. And my Mac does creating PDFs really well."
    author: "Christian Kienle"
  - subject: "Re: Is HTML dead?"
    date: 2005-05-17
    body: ">> Is everything else KDE based apps evil?\n---\nNo, it's not.\n\nIt's like I assumed. Matter of personal preferences...  ;-)"
    author: "anonymous"
  - subject: "Re: Is HTML dead?"
    date: 2005-05-17
    body: "Yeh :) Hehe \n\nthen everything it fine. For the next interview - if there is one - I will make sure, that the html version is available at the same time.\n\nAnd I will check out the free alternatives again. :) "
    author: "Christian Kienle"
  - subject: "Great interview!"
    date: 2005-05-17
    body: "Thanks for this eye-opening interview - stuff like this highlights the great work being done in the less visible areas of KDE.\n\nI also liked the professional, magazine-style layout... Great stuff all round!\n\nI look forward to seeing more of these interviews in the near-future!\n\nDanny"
    author: "dannya"
---
The <a href="http://www.kde-forum.org">KDE-Forum.org</a> team has <a href="http://www.qtforum.org/interview/interview.html">interviewed KDE developer Christian Nitschkowski</a> (<a href="http://kde-forum.org/interview_feedback.pdf">original PDF</a>). In the past he has written a lot of software for KDE but now his focus is on something which should be very interesting for every KDE user... a feedback wizard, soon to be integrated with Kexi.  Since this is KDE-Forum.org, you can <a href="http://www.kde-forum.org/thread.php?threadid=11389">comment on the interview in the forum</a>.

<!--break-->
