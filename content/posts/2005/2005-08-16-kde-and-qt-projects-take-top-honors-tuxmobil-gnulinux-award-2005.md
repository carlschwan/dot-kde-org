---
title: "KDE and Qt Projects Take Top Honors In TuxMobil GNU/Linux Award 2005"
date:    2005-08-16
authors:
  - "kkrammer"
slug:    kde-and-qt-projects-take-top-honors-tuxmobil-gnulinux-award-2005
comments:
  - subject: "Ko/Pi"
    date: 2005-08-19
    body: "And you got my congratulations too.\n\nKO/Pi is very powerful together with a Zaurus device, syncing is a breeze. If I would buy a Zaurus, I would do that for Ko/Pi."
    author: "Bram Schoenmakers"
---
Werner Heuser from <a href="http://www.tuxmobil.org/">TuxMobil</a> announced 
this year's winners of the <a 
href="http://tuxmobil.org/linux_award.html">TuxMobil GNU/Linux Award 
2005</a>, which honors projects improving the Linux experience on mobile 
computers.
Two of the five awarded projects have ties to KDE: <a 
href="http://www.pi-sync.net/">KDE-Pim/Pi (Pi-Sync)</a> and <a 
href="http://www.ph-home.de/opensource/kde3/kwlaninfo/">KWlanInfo</a>, while 
another two use the Qt toolkit for their graphical interfaces.
Congratulations to all those involved in the winning projects!





<!--break-->
