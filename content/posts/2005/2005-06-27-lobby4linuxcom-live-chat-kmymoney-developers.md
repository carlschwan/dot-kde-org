---
title: "Lobby4Linux.com: Live Chat with KMyMoney Developers"
date:    2005-06-27
authors:
  - "fmous"
slug:    lobby4linuxcom-live-chat-kmymoney-developers
comments:
  - subject: "where is the chat?"
    date: 2005-06-27
    body: "I really want to congratulate to the developers (trying the app now and found it very good! It IS realiable (the stable version), I'll let it manage my accounts :-).\nBut where is the chat? Isn't is supposed to be nearly ~now ?"
    author: "jumpy"
  - subject: "Re: where is the chat?"
    date: 2005-06-27
    body: "http://lobby4linux.com/X7Chat/index.php\n\nMaybe.  Bad on the dot for not being clear.  =)"
    author: "KDE User"
  - subject: "That is..."
    date: 2005-06-28
    body: "If you can get it to compile."
    author: "Doug Laidlaw"
  - subject: "Re: That is..."
    date: 2005-06-28
    body: "It compiles here, what's your problem?"
    author: "Anonymous"
  - subject: "4 letters..."
    date: 2005-06-28
    body: "...and a punctuation mark:\nHBCI?\n"
    author: "Martin"
  - subject: "Re: 4 letters..."
    date: 2005-06-28
    body: "If you read the linked roadmap you will see that it's planned for version 1.0 and work on it has started."
    author: "Anonymous"
  - subject: "Re: 4 letters..."
    date: 2005-06-29
    body: "What about using Card-Readers (USB) ?\nI'm using matrica software \nhttp://www.matrica.de\nand it's working very nicely with my Kobil Card Reader under Debian and Slackware\nhttp://www.kobil.de/d/index.php?m=products\n\nThough, there's a working proprietary solution, it would be very interesting to have an open source alternative at hand (and to see it maturing to work with smart card terminals and HBCI). This would be really a full-fledged (secure!) online banking solution at an OSS level (at least for German online transactions)"
    author: "Thomas"
---
On 27 June, 2005 at 9:30 pm CST, <a href="http://lobby4linux.com/">Lobby4Linux</a> will <a href="http://lobby4linux.com/WordPress/?p=25">host a Live Chat</a> with two developers of <a href="http://kmymoney2.sf.net/">KMyMoney</a>. This personal finance application is quickly becoming the &#8220;Quicken Killer&#8221; for Linux. The <a href="http://kmymoney2.sourceforge.net/release-plan.html">upcoming KMyMoney 0.8 release</a> will introduce investment support, multi currency support, a report generator, file import of GnuCash and OFX files, and a plugin-interface.




<!--break-->
