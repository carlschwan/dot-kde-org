---
title: "Application of the Month: amaroK"
date:    2005-06-03
authors:
  - "jriddell"
slug:    application-month-amarok
comments:
  - subject: "Great app"
    date: 2005-06-03
    body: "amaroK is an amazing app, my friend has a play list with at least 10,000 mp3s (he has a total of over 47,000, I know its disturbing) and there isn't any lag in the user interface."
    author: "Corbin"
  - subject: "Re: Great app"
    date: 2005-06-03
    body: "Well, the handling of music is great, but there seems to be a general lag in all passive popups when it comes to UTF-8 encoded Japanese (probably other large character sets as well). Unfortunatly, this is mostly only noticable to me in amaroK and juK. It's been kinda bugging me for a while, I guess you can take this as my bug report...."
    author: "Levi Durham"
  - subject: "Re: Great app"
    date: 2005-06-03
    body: "Qt's unicode support is not that impressive when it comes to speed, Qt4 should be better in that respect."
    author: "cartman"
  - subject: "Re: Great app"
    date: 2005-06-03
    body: "I think this is a general issue with FreeType -- it tends to be a little slow when loading large character sets.  I see the same lag when doing (say) ls on a directory containing filenames with Japanese characters.  \n\nI did an informal test of this - some results, if you're interested:\n\nhttp://www.livejournal.com/~trythil/265350.html\nhttp://www.livejournal.com/~trythil/265702.html"
    author: "trythil"
  - subject: "Re: Great app"
    date: 2005-06-03
    body: "I guess I should say that the ls behavior I described occurs both in konsole and in a stock xterm, otherwise the freetype - ls association makes no sense :)"
    author: "trythil"
  - subject: "Works for me"
    date: 2005-06-03
    body: "amaroK has worked very well for me as it has progressed from the first version I used-1.0 (a touch buggy at times, liked to hate arts) to the very stable, usable, and thankfully ad-free player it is now. It is impressive that it can play virtually every kind of audio file out there without much ado at all."
    author: "SuSE_User"
  - subject: "amaroK"
    date: 2005-06-03
    body: "This application is my favorite.  It's simple to use and handles a large music collection with ease.  Thanks to all the developers who worked many hours to help make other's lives more enjoyable.  I am grateful to each of you who work on this project.  You have provided me with a music experience which brings me peace in my life.\n\nThank You.\n\nMark"
    author: "Mark K"
  - subject: "Graet ipod mini support ..."
    date: 2005-06-03
    body: "and would be much greater if the polaylist stuff would work.\nBest is however - they are working on it in the new release :) So - waiting a little bit and wishing the developers all the best for their efforts :)"
    author: "Joerg Mertin"
  - subject: "Agreed"
    date: 2005-06-03
    body: "Another satisfied amaroK user here. I had been using XMMS for a very long time, and hadn't really found anything I liked enough to replace it, until I finally tried amaroK two weeks ago that is.\n\nSome of the features I like the most:\n * Album cover images\n * Being able to update metadata for all songs in an album\n * Lyrics\n * Scripting\n\nGreat work, thanks!"
    author: "Paul Eggleton"
  - subject: "AmaroK is in a league of it's own"
    date: 2005-06-03
    body: "AmaroK was the final nail in the coffin for Windows XP for me. I'm now totally Linux. Well done guys!"
    author: "Andy"
  - subject: "much better than command-line mplayer"
    date: 2005-06-03
    body: "Before picking up amaroK I thought command-line mplayer was as good as linux media got :) \n\namaroK is *brilliant*. I reccommend anyone who likes to listen to music whilst working to check it out."
    author: "David House"
  - subject: "It beats all out there"
    date: 2005-06-03
    body: "I think it beats the hell out of other player, you have to use to see it.\nI also thanks you guys very much for amarok..."
    author: "Hugo Costelha"
  - subject: "Some \"clean-up\""
    date: 2005-06-03
    body: "What about some clean up? I mean, turning the \"play\" button to \"pause\" when in the \"playing\" mode and vice-vera. This space can then be used for a \"record\" button to record audio streams. When you return from work, you can then play the stream."
    author: "charles"
  - subject: "Re: Some \"clean-up\""
    date: 2005-06-03
    body: "a) You can already have a button which is a Play/Pause toggle.  You need to alter your toolbar settings. (Right Click > Configure Toolbars)\n\nb) There has been a lot of talk and many requests about stream recording, but we don't see this happening soon - sorry, there are too many issues to deal with."
    author: "Seb Ruiz"
  - subject: "Re: Some \"clean-up\""
    date: 2005-06-03
    body: "PLAY/PAUSE is GREAT!!\nIT would be nice to have this behaviour available through the kicker applet :-)"
    author: "Veton"
  - subject: "Re: Some \"clean-up\""
    date: 2005-06-03
    body: "MediaControl works this way now.  I assume you mean the system tray icon but this is a kicker applet. "
    author: "Anthony Moulen"
  - subject: "Re: Some \"clean-up\""
    date: 2005-06-04
    body: "Click with the middle mouse button to play/pause from the system tray :)"
    author: "Seb Ruiz"
  - subject: "Re: Some \"clean-up\""
    date: 2005-06-03
    body: "Seems to be a good \"Google Summer Code\" project, isn't it?\nOr is amaroK not architectured to do that?"
    author: "Sebien"
  - subject: "Re: Some \"clean-up\""
    date: 2005-06-05
    body: "Are we talking about the same version? I have ver 1.2 but cannot see what you say! Thanx."
    author: "charles"
  - subject: "great job"
    date: 2005-06-03
    body: "... and another person who loves amarok. its really an amazing player. i use the SVN at the moment, and it's lovely. nice work on the wiki, works very well. same with the sidebar, much better. and some other stuff that has been improved, keep up the good work!"
    author: "superstoned"
  - subject: "nice"
    date: 2005-06-03
    body: "/me kneels before the Wise amaroK Bot Overlord :p"
    author: "Joergen Ramskov"
  - subject: "amarok and tenor"
    date: 2005-06-03
    body: "what will happened when kde4 comes out with tenor? will amarok switch to tenor and drop the sqlite backend? "
    author: "Pat"
  - subject: "Re: amarok and tenor"
    date: 2005-06-03
    body: "I hope so. It depends on whether Tenor is something every KDE program can rely on being configured or not."
    author: "Ian Monroe"
  - subject: "Re: amarok and tenor"
    date: 2005-06-03
    body: "As an aside, I use amaroK on my laptop and switched to MySQL to try to solve an annoying problem (it wound up relating to certain ID3 tags and a setting in amaroK... the solution is on the amaroK wiki).  It didn't solve my problem, but it did substantially speed up amaroK searches and playlist creation by a factor of \"waiting... okay\" to instantaneous action.  Very impressive, but it highlights that SQLite, while convenient, is not an ideal solution.\n\nIn short: Got a large collection?  Switch to MySQL."
    author: "Evan \"JabberWokky\" E."
  - subject: "Great _player_"
    date: 2005-06-04
    body: "AmaroK is the best player that I've tried until now. It can do about everything I want to do with my music, except for these two things:\n\n* Order chronologically by album. I mean, group music by album, order albums by year, order music inside the album by number (for most bands I can just order by track number, then year, but I couldn't find out how to do it if you have more than one album in the same year. I hear this is somehow better in SVN\n\n* Mass renaming/tagging. This is the one thing for which I open JuK nowadays. I get a lot of albums in different naming/tagging schemes, and it is a matter of five minutes to rename and tag them in a consistent manner in JuK. AmaroK is good for manual tagging, but lags behind in Tag Guessing/File Renaming\n\nBut still, my favorite player, and I really wish it was in kdemm (so it could get the deserved visibility)."
    author: "Renato Sousa"
  - subject: "ID tags"
    date: 2005-06-15
    body: "I have found that after spending hours fixing ID tags... Amarok just turns around and cuts them short. Is it so odd that a song title has a HUGE name (about 100+ chars plus)?\n\nProbably the most annoying bug of amarok but otherwise the apps treats my 10,000+ collection of mp3/ogg perfect. No lag."
    author: "Juan"
  - subject: "Re: ID tags"
    date: 2005-06-15
    body: "> Is it so odd that a song title has a HUGE name (about 100+ chars plus)?\n\nYes.\n\nBesides what ID3 tag version did you use? Maybe Amarok is just displaying the ID3v1 version to you. That version only supports a song title of 30 characters."
    author: "ac"
  - subject: "Re: ID tags"
    date: 2006-09-12
    body: "100 characters is not \"weird.\" id3v1 is horrendously useless and amarok should most certainly not be writing them to a file.  It should have an option at least, to ignore id3v1.  It's the only thing that really annoys me about amarok."
    author: "Tim"
  - subject: "Hey! wtf with amarok.kde.org site ? "
    date: 2005-06-16
    body: "and what the current version of amarok ?"
    author: "vorph"
---
The latest application of the month is KDE's most feature-packed media player <a href="http://amarok.kde.org">amaroK</a>.  The overview takes a look at functionality including Audioscrobbler, cover management and scripting.  We also have an interview with amaroK's team of developers covering their development process, usability and accusations of being hopeless IRC junkies.  Enjoy application of the month in <a href="http://www.kde.nl/apps/amarok/">Dutch</a>, <a href="http://www.kde.org.uk/apps/amarok/interview.html">English</a>, <a href="http://www.kde-france.org/article.php3?id_article=138">French</a>, <a href="http://www.kde.de/appmonth/2005/amarok/">German</a> and for the first time in <a href="http://www.kde.org.tr/index.php?option=com_content&task=view&id=69&It">Turkish</a>.






<!--break-->
