---
title: "KDE e.V. Assembly Meeting 2005 Notes"
date:    2005-09-29
authors:
  - "jriddell"
slug:    kde-ev-assembly-meeting-2005-notes
comments:
  - subject: "Many thanks..."
    date: 2005-09-29
    body: "... for these notes from the eV meeting\n... for the reports you wrote during aKademy after each talk and you posted on the wiki. That means a lot for people who cannot attend and I know it's not an easy task. \nThat's awesome work, Jonathan.\n\nGood luck to the new board!\n"
    author: "annma"
  - subject: "KDE core team"
    date: 2005-09-29
    body: "The discusssion about creating working groups was interesting. My point of view is similar to Lauri Watts. I doubt that we have the manpower for this laundry list of groups. Creating groups with close membership can have negative effects. If we create for example a documentation group, new volunteers could think 'well documentation is taken care by this group, I won't participate or I don't want my work to be judged by this group' or we can have groups that become inactive because their members have left KDE for professionnal or personal reasons. It is the plague of Debian were they have a lot of non-functioning groups.\n\nI think a core team with people that work full time on KDE and have the best knowledge of the whole project is enough for a start. And I would add these rules for core team members :\n- if a core team member become inactive in the project, for example if he has not committed for the last 6 months, he should be automatically dropped.\n- no company can have a majority in the group. Novell and Trolltech should not control the team or at least they must do it with subtlety.\n-  The core team should have a majority of programmers but at least 1 artist, 1 writer of documentation, 1 translator so the different constituencies of the KDE fellows get represented.\n- the release manager could be part of the core team and have a double vote\n- To moderate the activity of the group and avoid a top-bottom syndrom they should vote only on resolutions drafted by the release manager and (co-)maintainers of modules. \n \nWhy not a 12 people group so we could call them the KDE Apostles\n \nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: KDE core team"
    date: 2005-09-29
    body: ">Why not a 12 people group so we could call them the KDE Apostles\n\nThat's why not. ;)"
    author: "teatime"
  - subject: "Gemeinn\u00fctzigkeit"
    date: 2005-09-29
    body: "Is KDE e.V. \"gemeinn\u00fctzig\"? If so, financial contributions to KDE lower tax expenses at least in Germany. So companies could financially contribute without a real financial loss.\n\nWhat I think is a major concern is lobbying in the political sphere which becomes increasingly important.\n\nI assume that KDE interests are affected by the new EU-IPRED2. \n\nhttp://europa.eu.int/eur-lex/lex/LexUriServ/site/en/com/2005/com2005_0276en01.pdf\n\nAnd what about fundraising proposals in general?\n\nWhat about offering KDE master thesis projects for students?\n"
    author: "gerd"
  - subject: "Re: Gemeinn\u00fctzigkeit"
    date: 2005-10-01
    body: "> Is KDE e.V. \"gemeinn\u00fctzig\"?\n\nKDE e.V. is \"gemeinn\u00fctzig\" according to its statutes. There is, however, still some paperwork needed to get official recognition for it. German burocracy..."
    author: "Olaf Jan Schmidt"
  - subject: "Re: Gemeinn\u00fctzigkeit"
    date: 2005-10-02
    body: "> I assume that KDE interests are affected by the new EU-IPRED2. \n\nIANAL, but, no, I don't see KDE interests affected by this. Reading Article 3: \"intentional infringements of an intellectual property right on a commercial scale\" does not sound like something that KDE is doing.\n\nHowever, since KDE e.V. holds a trademark on \"K Desktop Environment, KDE\", we might actually find ourselves on the other end of the gun at one point - whether we want it or not."
    author: "Matthias Welwarsky"
---
KDE's legal body <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> held its Annual General Meeting at aKademy 2005 last month.  <a href="http://www.kde.org/areas/kde-ev/meetings/2005.php">Notes from the meeting</a> are now available.  A new board was chosen voting in Cornelius Schumacher and Aaron Seigo, and continuing Mirko B&ouml;hm and Eva Brucherseifer.  Our thanks to the retiring board members Matthias Kalle Dalheimer and Harri Porten.  The meeting decided to create working groups to streamline KDE development and activity, <a href="http://www.kde.org/areas/kde-ev/meetings/2005-working-groups-discussion.php">notes from the meeting discussing working groups</a> are also available.  KDE e.V. is currently discussing how these groups will form and they will be announced soon.  All long term KDE contributors are encouraged to become <a href="http://www.kde.org/areas/kde-ev/members.php">KDE e.V. members</a>.


<!--break-->
