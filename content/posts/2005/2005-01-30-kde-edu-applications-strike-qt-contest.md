---
title: "KDE Edu Applications Strike in Qt Contest"
date:    2005-01-30
authors:
  - "dmolkentin"
slug:    kde-edu-applications-strike-qt-contest
comments:
  - subject: "Correction"
    date: 2005-01-30
    body: "\"Fist price\" should be \"First price\"\n\nAnyway, Congrats to both KStars and Kig! "
    author: "RMS"
  - subject: "Re: Correction"
    date: 2005-01-30
    body: "I think it even should be \"First prize\"... :-)"
    author: "price vs. prize"
  - subject: "Re: Correction"
    date: 2005-01-30
    body: "two spelling corrections in a row: priceless"
    author: "ac"
  - subject: "Re: Correction"
    date: 2005-01-30
    body: "typo fetishists: needless"
    author: "Anonymous"
  - subject: "Re: Correction"
    date: 2005-01-31
    body: "Not true. It's how you present. Anyone would tell you that."
    author: "Joe"
  - subject: "Anyone"
    date: 2005-01-31
    body: "Yes. Anyone is always right."
    author: "Martin"
  - subject: "Re: Anyone"
    date: 2005-02-01
    body: "I love being Anyone."
    author: "foo"
  - subject: "Did only KIG programs participate? (nt)"
    date: 2005-01-30
    body: "Did only KIG programs participate? "
    author: "Asdex"
  - subject: "Re: Did only KIG programs participate? (nt)"
    date: 2005-01-30
    body: "From the KDE-Edu project, I don't know what other programs participated but the contest was not restricted to the KDE-Edu project. The 3rd prizes are independant KDE applications and I suppose that there were also qt-only based applications. \nCongratulations to the winners! "
    author: "annma"
  - subject: "Re: Did only KIG programs participate? (nt)"
    date: 2005-01-30
    body: "Howdy all,\n\nyeah there were \"Qt only\" apps too.\n\ncya"
    author: "Christian Kienle"
---
Today <a href="http://www.qtforum.org/">QtForum.org</a>, a site dedicated to Qt development discussions, <a href="http://www.qtforum.org/thread.php?threadid=5081">presented the winners</a> of the QtForum.org programming contest award, sponsored by <a href="http://www.trolltech.com/">Trolltech</a>. The contest selected the best educational software written with the Qt libraries, and these two
programs from the <a href="http://edu.kde.org/">KDE Edutainment Project</a> took first and second prize, while third place was shared among two also KDE-based applications.







<!--break-->
<p>The first prize was awarded to <a href="http://edu.kde.org/kstars/">KStars</a>, the outstanding 
deskop plantarium. <a href="http://edu.kde.org/kig/">Kig</a>, the KDE interactive geometry 
application made the second place.</p>

<p><i>"Both KStars and Kig are beautiful, smart, well written, and wise and enjoyable teachers of
timeless subjects of interest and importance to every age. Both are first class in every
respect, going far beyond their peers in implementation and ideology"</i>, said Trolltech's
Scott Collins.</p>

<p>KStars, a desktop planetarium program, took the $1500 first prize. <i>"On behalf of the KStars
team, I'd like to thank the contest judges and Trolltech,"</i> said lead developer Jason Harris.
<i>"This recognition of our work is extremely exciting and gratifying. The KStars team decided
before entering the contest that we would donate any prize money to KDE e.V. We feel strongly
that we should use our success to nurture the further development of KDE, without which KStars
would never have been possible. Thank you very much to our users and fellow kde-edu
developers; you've all had a part in KStars."</i></p>

<p>Kig, a program for exploring geometric constructions, took the $750 second prize.
Maintainer Dominique Devriese was amazed: <i>"The Kig team and myself feel very proud of winning
this prize. Of course, we would have liked to come in first, but winning against KStars could
never have felt right ;). My personal thanks go out to the rest of the fantastic Kig team
(Maurizio and Pino to name two) and furthermore, we'd like to thank Kig users and bug reporters,
all KDE-Edu people, the contest jury and TrollTech and all of our fellow open source
developers."</i></p>

<p>The third place was shared by another two KDE educational applications: <a href="http://linux.perlak.com/project.php?prj_name=keymaster">Keymaster</a>, a typing
teacher, convinced the judges while vocabulary trainer 
<a href="http://ksalomon.sourceforge.net/">KSalomon</a> amazed the community. Both were awarded $500.</p>