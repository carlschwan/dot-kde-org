---
title: "KOffice Meets the Users"
date:    2005-11-04
authors:
  - "iwallin"
slug:    koffice-meets-users
comments:
  - subject: "The main problem"
    date: 2005-11-04
    body: "is that KOffice is still alpha quality software. Even the most stable of its programs, KWord, sometimes corrupts files unrecoverably. Specifically I've seen paste and undo do that. Happens to unreproducible, too, so no, I did not file bug reports. And then you go looking for your backup copy and discover that the autosave feature you set to autosave every five minutes saved exactly nothing. Still I'm sticking with KOffice, don't want anything with Java dependencies on my system."
    author: "KOffice user"
  - subject: "OpenOffice2 in debian sid main branch now!"
    date: 2005-11-04
    body: "If you're using Debian Sid or fedora (and I guess probably other distroz), you can safely use openoffice.org2 because they managed to compile it with GCJ and classpath.  So yes it does use code written in Java but it doesn't run using any proprietary code from Sun or anything else.\nThat being said, I never use it because the only time I need an Office suite is to read .doc files that people send me and Kword embedded in Konqueror is so fast and just ROCKS!"
    author: "Patcito"
  - subject: "OOo2?"
    date: 2005-11-05
    body: "OOo2.0 doesn't actually need Java for the basics. I installed Writer, Calc and Impress, and I am not hearing any complaints about that."
    author: "Morten Juhl Johansen"
  - subject: "Re: The main problem"
    date: 2005-11-04
    body: "Obviously bugs are easiest to fix when they are reproduceable, but if you cannot reproduce them, it is still worth posting bug reports with the backtraces (assuming you have them)."
    author: "Robert Knight"
  - subject: "Re: The main problem"
    date: 2005-11-04
    body: "backtraces are only for crashes...\n\nI basically agree with grandparent. Krita looks like its going to be great stuff, but I always run into little problems. I loath Gimp and Krita seems to have quite active development so I'm defintedly going to be following it.\n\nA couple of years ago, I was working on a KOffice presentation and it became totally messed up in regards to its fonts I couldn't figure out how to fix it. That was enough to make me never want to use it again."
    author: "Ian Monroe"
  - subject: "Re: The main problem"
    date: 2005-11-05
    body: "Please, if you run into those little problems again, either file a bug report (if it's reproducable) or contact me, either on #koffice, or by mail (which I prefer). I can only fix stuff if I know it's broken, and actually, hacking on Krita takes so much time that I hardly have a chance to actually use my application :-). It helps if you can compile & install the svn trunk version, though."
    author: "Boudewijn Rempt"
  - subject: "Re: The main problem"
    date: 2005-11-05
    body: "Actually, I feel that KWord has become much more stable recently - I have not had any crashes this time around.\nI do not use KDE, but I do use KOffice - as it is light compared to its features."
    author: "Morten Juhl Johansen"
  - subject: "Autosave path"
    date: 2005-11-04
    body: "Last time I checked the autosave path wasn't set by default, the path field was emphty. This means that the autosaving wasn't done (AFAIK), I don't know if this is fixed already because I set it manually and updated after that.\n\nAlso I think kword is a bit unstable. This can be very annoying if you are typing a big document without saving sometimes. :)\n\nFor the rest I really like koffice! thanks!"
    author: "David"
  - subject: "Home Use"
    date: 2005-11-04
    body: "I use KOffice at home.  Given the choice between OpenOffice, MS Office via Wine, and KOffice, I chose KOffice for the following reasons:\n\n- KOffice integrates best with my desktop (visual fit, file management, printing).  This is important to me, so I give it a lot of weight\n- KOffice is the most responsive suite on slower systems\n- I have confidence that the KOffice team will fix any shortcomings I find\n\nShortcomings I've found (that haven't been fixed) are:\n- Font display and printing (kerning issues) can be pretty bad.  This has sent me to MS Office via Wine a few times!  I understand that this is a Qt issue and look forward to a Qt4-based KOffice.  But please hurry! ;)\n- Table support in KWord is pretty bad.  I'd like easier control over placement & size of table elements, in particular\n- The rapid improvement of KSpread over the years has been wonderful, but the one feature I miss is cell labelling.  To my knowledge the blank textbox left of the function textbox accepts input, but serves no useful purpose.  I'd like to label cells and use those labels in my formulas\n\nHope that helps.  I also don't like the K-naming thing, but that's another issue ;)"
    author: "ac"
  - subject: "Re: Home Use"
    date: 2005-11-06
    body: "\"KOffice is the most responsive suite on slower systems\". Thats the great advantage of KOffice. For the next MS Office/Windows yu\u00b4ll need a DirextX9 graphic card - who wants such a heavy graphic card to make office work?\n\nAnd people  who want a big system can run OpenOffice. For Home Use and small buissiness issues we don\u00b4t need a bloated system. Espacially at KOffice, \u00b4cause there a just to few developers to fix all the bugs. I want for me a KOffice that don\u00b4t come with more and more features till it\u00b4s so big as OpenOffice, i want a stable program suite with that a can do my daily issues. All that KOffice needs for it, is to be with less bugs. And so long one feature after the other get added to KOffice there is no change to get rid of the bugs."
    author: "Nattydraddy"
  - subject: "for me, it's the interface"
    date: 2005-11-04
    body: "The default look of the interface is horrible in my view. With KDE, much can be done to kKOffice to look beautiful.\n\nThe typing area does not cover the entire window and the color used for the un editable area is so dull! This diverges from the \"normal\" applications MS-Wors StarOffice and WordPerfect! So, to users KDE looks strange. This does not help at all.\n\n\nThen we have the toolbars. Why won't the developeres merge those toolbars. Any attempt by a user to customize them might lead to a crash. In the end, may of those short tool bars waste screen real estate.\n\nMy 2 cents."
    author: "charles"
  - subject: "what i like and don't like"
    date: 2005-11-04
    body: "i only use kword (and, now and then, karbon).\n\nI like the fact its small and fast, and works good with kde.\nits also easy to use.\n\nbut the table support sucks, and fonts (esp kerning sucks) are hard to get looking well. looks great under suse 10.0, tough.\n\nwell, and for my girlfriend, .doc support is the most important point...\n\n\nanyway, for me, if you guys can fix table support in kword (even if it takes until KDE 4 :D) i'll be a happy user. i'll use it anyway, by the way. i don't make tables very often :D"
    author: "superstoned"
  - subject: "Re: what i like and don't like"
    date: 2005-11-05
    body: "> well, and for my girlfriend, .doc support is the most important point...\n\nHmmm, never tried that with the wife... ;-)\n\nSeriously, now, I work in a Windows environment. .doc support is paramount.\n\nI had some trouble to install Koffice on Mandriva 10.1 community version, but didn't get much time to work with it, as all my documents are sent to Windows-only users. Oo.o is a nobrainer, therefore.\n\nBut I will install Koffice at home, since it's oh so less bloated than Oo.o...\n\nNow, tables are extremely important. Even my resum\u00e9 is in table form. Many, many, many forms rely on correct table manipulation...\n"
    author: "Gr8teful"
  - subject: "Printing issue"
    date: 2005-11-04
    body: "While I agree that KOffice does not yet warrent a version number >= 1.0, the most serious problem remains the fact that KWord will not print proportional spaced fonts correctly.  Printing in other apps has the same problem but it is not as serious there.\n\nI do use KWord and Karbon for specific functionality that other apps don't have.  KWord will open PDF files and Karbon will open SVG files that other OS apps won't open or won't display correctly as well as convert EPS to SVG.\n\nI also do not like the startup dialog that demands that I choose a document format and has a bunch of irrelevant choices.  If implemented better, this might be OK, but I just turn it off.  I don't agree with the idea that an application must always have a document open.  It can cause problems if you have a document open and you have not given it a name yet."
    author: "James Richard Tyrer"
  - subject: "startup dialog (was Re: Printing issue)"
    date: 2005-11-04
    body: "I agree with your sentiment on the startup dialog. What I like about koffice is that it is lightweight and fast, but the startup dialog gets in the way if you just want to open a document quickly. Why not open a plain document with reasonable default settings if no document to open is given?"
    author: "ac"
  - subject: "Re: startup dialog (was Re: Printing issue)"
    date: 2005-11-04
    body: "I also think that the startup dialog only gets in the way. At least in my admittedly outdated KWord 1.2 the default action is \"cancel\", so just pressing return after the startup dialog appears simply quits KWord again."
    author: "ac"
  - subject: "Re: startup dialog (was Re: Printing issue)"
    date: 2005-11-04
    body: "So, select a blank A4 documet and check 'allways use this template' (that may not be the exact text), and you have what you ask for ;)"
    author: "Anders"
  - subject: "Re: startup dialog (was Re: Printing issue)"
    date: 2005-11-06
    body: "Yes, that is what I meant by turning it off."
    author: "James Richard Tyrer"
  - subject: "Re: Printing issue"
    date: 2005-11-05
    body: "I agree on the printing problem - it makes KWord unusable for me."
    author: "Thomas"
  - subject: "Re: Printing issue"
    date: 2005-11-06
    body: "I totally agree. I would *love* to be able to use KOffice, but the character-spacing problem makes any document look like shit.\n\nI always have my Gentoo system up-to-date, and every time a new KOffice version is released, I emerge it immediately, hoping that the problem has been fixed. Unfortunately, it is still there..."
    author: "Meneer Dik"
  - subject: "Kpresenter Kword"
    date: 2005-11-04
    body: "Kpresenter and Kword are my most used applications.  Kpresenter is a nice, lightweight and useful presentation application.  It exports to html extremely well and can accept many different image types.  It scales images quickly and effectively.  I do wish it would default to lock the aspect ratio when dragging the corner of an image to change the scale.  It would also be great if Kpresenter understood image transparency. \nI find the interface to be useful and fairly intuitive.  Insert slide should be a default button on a toolbar.\nMany times changing an option doesn't actually work.  I have to change something, hit apply, then change another property in order to have the first change appear.\nI do definitely prefer Kpresenter to the OOo alternative.\n\nI also use Kword exclusively for my word processing application.  It does exactly what I need quickly and well.  Printing is sometimes an issue.  I have no idea if it's the printer or the application.  I usually print to PDF first, then send that document to the printer.  Always looks great this way.\n\nSo, I don't have many complaints with the application's usability or performance.  Just a \"great work so far!\""
    author: "Stuart"
  - subject: "Kpart is what interest me in KOffice"
    date: 2005-11-04
    body: "What I like about KOffice is Kpart.  This technoloy has the potential to do similar thing that Apple's OpenDoc technology did. (I believe this was eventualy made open source when IBM and Novell joined in).  If this happens with KDE/KOffice then working would more intuitive and natural - a bit like pen and paper.  This would definately be the case with tablet/handheld computer.  Incidently artificial intelligents (agent software) would take KDE applications closer to this goal. This would mean applications like KWord would need to support irregular frame. Have a look at the Newton OS to see what I mean.\n\nThat how became I interested in the KDE GUI and KOffice.  My favoueite KOffice component at the moment is Krita.\n\nRock on."
    author: "Michael Wright"
  - subject: "Re: Kpart is what interest me in KOffice"
    date: 2005-11-04
    body: "KPart itself is part of KDE. (Konqueror is *the* tool for KParts.)\n\nHowever KOffice improves KPart for its needs. (That is why you cannot use a non-KOffice KPart in KOffice.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Kpart is what interest me in KOffice"
    date: 2005-11-04
    body: "any change this can be fixed for KDE 4? (eg add the changes to KDElibs for 4)"
    author: "superstoned"
  - subject: "Re: Kpart is what interest me in KOffice"
    date: 2005-11-04
    body: "I do not know the plan of KOffice for KDE4.\n\nHowever I am not sure if adding the features to kdelibs would solve everything. Probably each KPart will more or less always need to be adapted for KOffice. (But perhaps it could be made easier and could therefore interest parts of KDE that are normally not interested in having a dependency to KOffice.)\n\nHave a nice day!\n"
    author: "Nicolas Goutte"
  - subject: "Re: Kpart is what interest me in KOffice"
    date: 2005-11-05
    body: "Koffice should be able to embed any kpart\nhttp://bugs.kde.org/show_bug.cgi?id=55092"
    author: "Anonymous "
  - subject: "Re: Kpart is what interest me in KOffice"
    date: 2005-11-05
    body: "The improvment KOffice makes to KPart should move into the main KDE environment.  That should over come any problem of not being able to use non-KOffice KPart in KOffice.  Other applications that may benefit such improvement are Kontact, TaskJuggler and KDissert.  There bound to be other applications too."
    author: "Michael Wright"
  - subject: "Love KOffice"
    date: 2005-11-04
    body: "I think KWord is really stable. The only trouble I seem to have is importing Microsoft Word files. OpenOffice.org does it flawlessly, so I don't understand why there isn't a decent MS Word import filter. Other than that, I'm very happy with the whole suite. It's fast, light-weight, and perfectly integrated with KDE. So keep up the great work!"
    author: "Chris Evans"
  - subject: "Re: Love KOffice"
    date: 2005-11-04
    body: "- because OOo has used a lot of work to create and maintain the MS Office import (and export) filters\n- because KOffice cannot use OOo's filters, as they are not meant to be portable.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Love KOffice"
    date: 2005-11-04
    body: "But once we all use Opendocument the method would be\n\n- convert doc to Opendocument using a OO based conversion tool\n\n- open the converted Opendocuemtn file with Kword"
    author: "Gerd"
  - subject: "KWord"
    date: 2005-11-04
    body: "The only Koffice application I use regularly is KWord.\n\nMy experiences so far with KWord have been positive, but here are some things I would really appreciate:\n\nBetter handling of font spacing\nImproved support for MS Word docs\nA grammar checker\nMore grammatical tools in general (such as various document analysis tools that rate the reading level and such)\nA built-in KDict-like function with a thesaurus.  I'd love to be able to right-click on a word and select a synonym from a list."
    author: "Riskable"
  - subject: "KOFFICE"
    date: 2005-11-04
    body: "I try to use Kword for grad school.  I like the application's interface and speed.  But, the kerning issues sometimes force me to cut and paste into OpenOffice and finish up there.  If I could make the print-out look more professional, it would be my #1 choice.  But, in the mean time I don' it when my professors say my term papers look weird.\n\n--andy"
    author: "Andy"
  - subject: "KOffice"
    date: 2005-11-04
    body: "I wish I could use KOffice more, it is fast, functional and integrates well with my favorite desktop, KDE. I use KDE at work and home, as do a number of my co-workers. I tend to do most of my writing using Latex, so Kile is what I use the most, and it is great.\n\nFrom KOffice, Kivio and KSpread are my most frequently used applications. KSpread works just great, though I wish Kivio was better. The stencils in Kivio often do not look as good as the icons representing them. The icons should default to using colour and there should be more icons. The Cisco networking icons should be blue and not black. And the printouts don't look quite right. I would like to use it more, but often avoid using diagrams just because I know the result will not represent the effort I expended creating the diagram.\n\nPlease don't see my comments as overly negative, I believe it is the best there currently is for KDE.\n"
    author: "Fizz"
  - subject: "Kformula is frustrating"
    date: 2005-11-04
    body: "I haven't found any way to enter equations system (  \"{\" sign cant be stretched on  multiple lines  )  either intuitively nor in documentation"
    author: "nick"
  - subject: "Re: Kformula is frustrating"
    date: 2005-11-07
    body: "Have you tried by putting a column vector after the {, it worked for me. By column vector I refer to that icon composed of several little squares one over another."
    author: "marce"
  - subject: "KSpread date format"
    date: 2005-11-04
    body: "I use KWord sometimes to read .doc documents (well, I usually write in latex, so I do not write in it). And I would like to use kspread, however, I have one problem - I can not make it show date os only the day of week. I did not manage to specify custom format as well.\n\nAnother thing I would use is karbon, however, it crashes all around the place, so it seems as lost time for me and I found myself program called vrr.\n\nBut I must say, koffice is the fastest office suite that ever runed on my computer, counting even MS office 2000 on windows."
    author: "Michal Vaner"
  - subject: "Formulas"
    date: 2005-11-04
    body: "Like most people here I really appreciate the speed at which KOffice runs on my computer, especially compared with OpenOffice. The only reason I usually end up using Impress for my presentations its ability to easily include LaTeX equations using the OOoLatex macro (http://ooolatex.sourceforge.net). The key feature is that the LaTeX code is embedded in the formula object, so it can be easily edited at any time (in contrast to simple LaTeX -> Image converters like tex2im (http://www.nought.de/tex2im.html)).\n\nCan this be done also in KPresenter or in other KOffice applications? One could also imagine a whole range of embedded objects where this would  be useful, for example a graph created by a gnuplot script, which can be edited from within the office applications....\n"
    author: "ac"
  - subject: "Re: Formulas"
    date: 2005-11-04
    body: ".Bib files import. This is a useful contribution for all scientific writers."
    author: "Gerd"
  - subject: "I use Kivio"
    date: 2005-11-04
    body: "Personally I really love KOffice. A round complete nice set of Office tools to get work done. Once when I was at university I had to draw a lot of computer science diagramms and as most others I was using DIA to try getting this kind of tasks done. But I quickly figured out how immature DIA was, how broken, instable, not reliable. Also the results wasn't really satisfying. Once I saved some stuff and then went over for dinner and after I came back I wanted to load the stuff and DIA has shown that it saved broken information. So I ditched it in favor to Kivio and the stuff worked out of the box with the provided stencils. Kivio was stable, reminded me of Visio, looked, felt and worked the same way like Visio and the results was quite impressive also. I have one big wish, I would really like to see TaskJuggler inside KOffice and have the default one removed in favor to TaskJuggler. Also again when I first hit a projects management software I came across Mr.Project (later renamed to Planner) but it was a toy more or less, didn't offered a quarter of the features that MS.Project offered. But then I was shown TaskJuggler and felt in love it was the most impressive implementation of a great projects management software seen for open source. I think that Koffice does it right, the entire KDE desktop plattform does it right and thats why I love it.\n\nThanks to all developers who made Koffice and KDE possible. A great desktop, easy, simple and consistent. A desktop to get serious business work done."
    author: "A.A."
  - subject: "Re: I use Kivio"
    date: 2005-11-05
    body: "Kivio is great, but it does need some polish. Printing and stencils are two key areas.\n\nThere is already a project management system being developed as part of KOffice. It is called KPlato. It is not currently usable which is why it hasn't been included in KOffice and its development appears to be relatively stagnent."
    author: "Fizz"
  - subject: "Scripting"
    date: 2005-11-04
    body: "Is there a good tutorial how to use scripting in KOffice, preferably in Python?"
    author: "ac"
  - subject: "Re: Scripting"
    date: 2005-11-04
    body: "It makes months ago that I have not answered to this question. ;-)\n\nIf nothing has changed, the answer is: no, unfortunately.\n\nFor DCOP in KOffice, there seems to be only:\nhttp://www.koffice.org/developer/dcop/\n(which is very limited).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Scripting"
    date: 2005-11-05
    body: "Ouch. That's not too exhaustive. Is there a way to run scripts or macros from within, say,  KWord? Do you have to use KJSEmbed for that, or ist it possible to use other languages?\n"
    author: "ac"
  - subject: "Re: Scripting"
    date: 2005-11-05
    body: "Well, there's http://kde-apps.org/content/show.php?content=18638 which I created for precisely this reason. Provides a slightly more Pythonic abstraction around the DCOP interface so you can do some pretty decent scripty things, especially with KSpread.\n"
    author: "Cerulean"
  - subject: "Re: Scripting"
    date: 2005-11-05
    body: "Thanks for the information, this looks really promising. Is the development still going on? It seems like the last changes were quite a while ago.\n"
    author: "ac"
  - subject: "Re: Scripting"
    date: 2005-11-05
    body: "It covered most of the useful things present in the DCOP interface at the time - then there wasn't much to do but wait for more features. I may have a look at the applications and see if there is anything new available soon."
    author: "Cerulean"
  - subject: "Re: Scripting"
    date: 2005-11-05
    body: "This sounds so promising, it really deserves more publicity. Is there any chance that it will be included in the regular KOffice release?"
    author: "ac"
  - subject: "Re: Scripting"
    date: 2005-11-06
    body: "Did you contact us -- that is, the KOffice developers? I know I could use some help with creating DCOP interfaces! And I think this is very useful stuff. You obviously know more than I do about dcop."
    author: "Boudewijn Rempt"
  - subject: "Re: Scripting"
    date: 2005-11-07
    body: "I asked for a couple of features and bugfixes in #koffice @ irc.kde.org and they were promptly added/fixed. Haven't really discussed taking it further - didn't seem like there were many people using it :P"
    author: "Cerulean"
  - subject: "Re: Scripting"
    date: 2005-11-07
    body: "Perhaps more people would use it if we were to package it with KOffice? If you can help me extend Krita's dcop interfaces and provide such a nice python lib for Krita, I'd certainly want to include it in the release."
    author: "Boudewijn Rempt"
  - subject: "Font kerning"
    date: 2005-11-04
    body: "I like KWord for exposing the most used functions at places that are easy to find (insert image at a prominent place, and no need to open a dialog for inserting a page break). Not being \n\nThe biggest problem for me is really the bad printing output, which makes it hard for me to stick with KOffice (or, say, KWord, as most important app in there), because when I need OpenOffice for proper printing, I can use it for creating the docs as well.\n\nAlso, I remember having had problems with KSpread changing the format of multiple cells at once which were formatted differently. I also despise the weird tool handling in Karbon (when I select the text tool and click into the page afterwards, I would seriously expect to get a text box inside the page where I can enter text). I don't see enough reason to switch from Inkscape to Karbon, I guess it needs to improve dramatic to change that.\n\nKPresenter seemed quite cool to me, but lacks a bigger amount of templates. (You know, I estimate 90% of the non-corporate PowerPoint presentations to be based on one of the standard templates.)\n\nBut on the whole, KOffice is Really Cool (TM). I can't wait for 1.5 (for Krita) and 2.0 (for KWord's new Qt4 font kerning stuff). Seems like quite a long time for waiting on a pretty printer/pdf output."
    author: "Jakob Petsovits"
  - subject: "Re: Font kerning"
    date: 2005-11-05
    body: "Quote:\n\"I also despise the weird tool handling in Karbon (when I select the text tool and click into the page afterwards, I would seriously expect to get a text box inside the page where I can enter text). I don't see enough reason to switch from Inkscape to Karbon, I guess it needs to improve dramatic to change that.\"\n\n\nInkscape is indeed great, but it also has ten times more developers than Karbon ever had.\n\nKarbon will change though. And I personally would like it very much if people will try out Karbon regularly (especially people using other vector drawings a lot) and post their findings in the koffice mailinglist or post suggestions and bugs in the bug database (bugs.kde.org).\n\nThe more suggestions and bug reports we get, the better KOffice will be."
    author: "Tim Beaulen"
  - subject: "Re: Font kerning"
    date: 2005-11-06
    body: "Quote:\n\"Inkscape is indeed great, but it also has ten times more developers than Karbon ever had.\"\n\nThat's right. I didn't want my statement to be negative critisism, and I love seeing Karbon regaining some momentum again. If I had to restrict myself to one saying, it would be that I find it great that you get so much done with so little amount of developers. Go KOffice go!"
    author: "Jakob Petsovits"
  - subject: "Using it"
    date: 2005-11-04
    body: "Well, usually I get used to software by using it. for instance I had a cultural barrier from getting involved into tex. It was always very complicated and I knew there was a whole lot of stuff you had to learn. And it further seems people who know latex cannot explain their universe to beginners. It becomes to complex and that's what drives you away.\n\nBut there is another way and this is how I slipped in latex and finally got to know the universe by myself. I edited good existing .tex files and tried to build my own documents. And I got fast and good results, learned more elements of latex and so on. This is how I explored the software.\n\nwhen you look at a dictionary -- there are so many words in. A chinese student wanted to learn the dictionary by heart and refused to pratice the language before he knew at least half of the vocabulary. His friend went to parties and drank beer with us. Guess who was more succesful in learning our language. (I still wonder how I got to know all vocabulary of my language, I do not remember studying dictionaries as a child)\n\nSo the same applies to software such as Koffice. what is more needed than software are people who actually use Koffice to get their things done. And once they work with the software they will find glitches and ask for improvements but: how to get them to jump into the water and start swimming? The answer is easyy: A good set of well-designed sample files is needed which cover typical needs. Files to hack into. I mean naked software with functions and features only does not sell. What's needed for KOffice is cosyness for new users, the ability to play around and explore the features. Sample files and user community building are crucial for that."
    author: "Andre"
  - subject: "KOffice at home + work"
    date: 2005-11-04
    body: "I work at home, so I use KOffice for my personal things, but mostly for work. Problems I detected are mainly in KSpread: cell borders don't always work as expected (i.e. they won't show all the time the way I want), gnumeric import filter doesn't work, excel import filter doesn't work for complex spreadsheets (i know, i know, but they aren't that complex). For most of my stuff, it just does well.\n\nI think KOffice is great, but needs a LOT of work to be usable in a company. Importing from other suites is a must."
    author: "Sebasti\u00e1n Ben\u00edtez"
  - subject: "Re: KOffice at home + work"
    date: 2005-11-05
    body: "One irritating factor is when you have a simple formula, such as a column total, and then insert a new row somewhere in the scope of the column totals range. What would happen in MS Office is that the formula would be updated to include the new row, but not in KSpread."
    author: "A. Nonymous"
  - subject: "Re: KOffice at home + work"
    date: 2005-11-21
    body: "\"Cell borders don't always work as expected\" -\n\nthey  NEVER  work as expected.\n\nI love KWord, and also would love to use KSpread, but this renders it unusable for me:\n\nMy girlfriend is a teacher and needed a checklist for her class to keep attendance;\nvery simple: one column with names, and the rest empty boxes - \nafter trying to print this with KSpread for hours and only getting weird results I finally had to install OpenOffice for being able to do it ...\n\n"
    author: "Ned"
  - subject: "KOffice in a MS work environment"
    date: 2005-11-04
    body: "I work at Hewlett-Packard where the vast majority of users in my neck of the woods use MS-Office. I'm a KDE fan at heart and attempt to use KDE native tools wherever possible.\n\nMy primary need is a way to read powerpoint and then publish back revisions to powerpoint. I can't use KPresenter to do this. It crashes too much on .ppt files. I can use OO Impress to read 99% of the powerpoint files I get. I still have to use Powerpoint (in VMWare/Windows) to edit them in a way that everyone else in the company can read them with Powerpoint.\n\nI cannot use KWord for MSWord (.doc) files, it crashes or goes CPU bound too much. I can use OO Writer to read 90% of the MSWord files I get.\n\nMy favorite KOffice application is Kivio. I used to use SmartDraw on Windows for flowcharts and diagrams but Kivio does everything I really need. The main thing I miss from SmartDraw is the ability to double click on a text object and be able to edit the text in place, without requiring a dialog. I can get away with using Kivio because I publish all my documents in html (using Quanta+).\n"
    author: "Eric Soderberg"
  - subject: "Re: KOffice in a MS work environment"
    date: 2005-11-05
    body: "Why do you use vmware/windows ? \nTry to use crossover office+msoffice . you will find this combination softer on resources and faster than Openoffice :)"
    author: "vk"
  - subject: "kchart"
    date: 2005-11-04
    body: "I tried different time to use koffice but kchart is so awful that I never been able to pass the test period. \nI never been able to have what I want draw with it. It's far away from the equivalent software in gnumeric or openoffice for example. I think that will be a very good idea to use something like matplotlib to build a new chart software for koffice.\n\n"
    author: "humufr"
  - subject: "Re: kchart"
    date: 2005-11-07
    body: "It would be interesting to hear what exactly you want and what you cannot do with the current version."
    author: "Inge Wallin"
  - subject: "Default Background"
    date: 2005-11-04
    body: "KWord seems to follow my KDE Colour Scheme a little too close. In general it makes more sense for a word processor to default to black text on white background rather than light grey text on dark grey background (my current colour scheme). What makes this worse is any new frames that I add to the document print this way."
    author: "Danni Coy"
  - subject: "more frequent releases"
    date: 2005-11-05
    body: "As others have pointed out, in many areas koffice appears to be alpha \nor beta quality software. This is especially frustrating in situations, \nwhere you, really fast, want to create a small document - and because of \nan obscure bug, can't.\n\nSo even if I would file a bug (which I do, from time to time), it would \nprobably take two - three months before a fix reaches me... (I'll throw \nin a dumb one: even the linux kernel has more frequent releases!)\n\nI could imagine that koffice, especially the stable tree, could easily \n(automagically even) be released once a week and made available via \n(among others) klik...\n\nKind of like the way amarok handles it.\n\n(I don't know much about releases, but I care more about functionality \nthan press statements)\n\nAlso, have TestDrives, BugDays and such...\n\nThe kerning issue appears to be a big one - i don't think it's all \nTrolltechs fault, but if it is then IMHO they should fix this \nembarrassing situation... but then, this issue probably does not \ncome up to often.\n(IIRC qt-3.0 was promised to bring the fix)\n\nPersonally, I use koffice for writing invoices. For that I embed a \nspreadsheet into a word document. This works pretty good - you learn \nwhere not to step with time. I haven't had a look at kexi yet, but I \nhope to automate the process more in the future...\n\nI have not even tried to show koffice to my girlfriend, she finds \nso many bugs in openoffice and ms office - she'd be throwing around \nthings after 10 minutes of koffice usage.\n\nAnyway - I'd like to thank everyone who has, is and will be \ncontributing to koffice. It is a great piece of software and,\nas Inge has pointed out before, has huge potential."
    author: "bangert"
  - subject: "Crashes..."
    date: 2005-11-05
    body: "KOffice has a lot of potential. Unlike a certain other free office suite, it starts instantly instead of in 2 mins (OpenOffice.org) and actually uses native widgets. However, I have never seemed to be able to do anything in KOffice without causing a crash. Recently, I could not so much as open a document in Krita. Usually I get discouraged and stop using it."
    author: "Luke Sandell"
  - subject: "Re: Crashes..."
    date: 2005-11-05
    body: "A released version of Krita, or one you compiled yourself? Could you contact me so we can fix this problem for you?"
    author: "Boudewijn Rempt"
  - subject: "Comments"
    date: 2005-11-05
    body: "As a background: I work at lower management level in a medium-size company and use MS Office daily for making technical and commercial documents and presentations. Here are some comments:\n\nKword:\n-I use outline mode in MS Word a lot, but can't find it in KWord, also no way to quickly promote/demote paragraphs (Alt-left/right in Word)\n-Header/Footer does not seem to work, critical in business environment\n-where are picture/table captions, List of images/tables, indexes?\n-just an idea: scan document for TLAs etc (basically all words in caps), automatically generate a list of abbreviations from these\n\n-> not usable for technical documents, user manuals etc., could be OK for memo level stuff\n\nKSpread:\n-print preview font is not the same as in the sheet\n-this was also in earlier comment: no way to name the cells. Imagine programming with absolute addresses only instead of symbolic names!\n-although I have set time to 24h format in Control center, KSpread uses AM/PM format\n-adding times, like 1:30 + 3:40 does not work\n\n-> good for casual calculations, need some work to be real business tool\n\nKPresenter:\n-screen drawing is bit buggy and slow\n-single-clicking text does not make the it in edit mode, need to double-click, annoying\n-crashed when tried to demote/promote (alt-l/r)\n-show sidebar does nothing\n-no outline mode\n-ctrl-enter does not add slide or move from header to text\n-no slide design (e.g. text+image, title slide)\n-transition/object effects generally suck, they should not be on by default, no way to globally disable object effects like for transition effects\n-there is no way to skip the effect once it started\n-in presentation mode, sometimes the page is not displayed in final form, but some incomplete images are displayed, very unprofessional\n-I'm not with a projector now, so can't test it, but I like the PowerPoint's dual screen presenter view, where you can show the slides on the projector and a control panel on your laptop screen, is this possible?\n-no organisation chart tool\n-tables are better than PowerPoint\n-shortcuts: --> works, ==> doesn't\n-can't add drawing guides to master slide\n\n-> not usable for any business use, when presenting your company to outsiders, everything needs to be really polished and professional, never ever crash\n\nKivio:\n-European map is ancient (Czechoslovakia and Yugoslavia are no more, Baltic states are not part of USSR)\n-mind mapping chart would be nice\n-locking the tool with alt/shift-click needed\n\n-> looks OK, but I'm not expert here\n\nThis is with Koffice Debian sid 1.4.2-2, using KDE 3.4.2-4.\n\nThank you for you good work, I'm sure that with a little work you'll beat MS Office easily.\n"
    author: "L.. User"
  - subject: "Re: Comments"
    date: 2005-11-05
    body: "> -although I have set time to 24h format in Control center, KSpread uses AM/PM format\n> -adding times, like 1:30 + 3:40 does not work\n\nI assume this was in KOffice 1.4.x ? Both these things should work correctly in SVN version."
    author: "Tomas"
  - subject: "Re: Comments"
    date: 2005-11-06
    body: "I use outline mode in MS Word - Try kdissert (works with swx or html files)... Beats the hell out of using a word processor when planning documents."
    author: "Danni Coy"
  - subject: "Re: Comments"
    date: 2005-11-06
    body: "Thanks for the tip!\n\nI tried KDissert, here are some remarks:\n-in general, you get results faster than with MS Visio (ok, it's not the best mind mapping tool)\n-can't change line style\n-not integrated to KOffice\n-only basic tools available\n\nKDissert wouldn't probably cover my need for outline mode, as I use it not only in first plans, but mostly during editing. It's nice to promote large parts of document higher in heading hierarchy. I'd  like to see this feature in KWord/KPresenter as well.\n"
    author: "L. User"
  - subject: "Re: Comments"
    date: 2005-11-06
    body: "We really want KDissert (or the other app that does almost the same thing, forgot its name, forget my own name next) to become part of KOffice. In fact, Inge is a very voiciferous proponent of such a move and I bet it'll happen before 2.0 -- in which case we'll be able to generate skeleton KWord documents from the mind-map."
    author: "Boudewijn"
  - subject: "Re: Comments"
    date: 2005-11-09
    body: ">KPresenter:\n> -screen drawing is bit buggy and slow\nWhere do you have problems. This is a not clear enough for fixing it\n\n> -single-clicking text does not make the it in edit mode, need to double-click, annoying\nThat is so by design\n\n> -crashed when tried to demote/promote (alt-l/r)\nWhat do you mean by this. Do you have a backtrace?\n\n> -show sidebar does nothing\nworks here\n\n> -no outline mode\nwe have on in the sidebar, but it might not be what you want.\n> -ctrl-enter does not add slide or move from header to text\n> -no slide design (e.g. text+image, title slide)\ncan you explain a bit more in detail.\n\n> -transition/object effects generally suck, they should not be on by default, no way to globally disable object effects like for transition effects\nIf you think they suck why are you using them?\n\n> -there is no way to skip the effect once it started\nYes they are since kpresenter 1.4.\n\n> -in presentation mode, sometimes the page is not displayed in final form, but some incomplete images are displayed, very unprofessional\nCan you send me an example.\n> -I'm not with a projector now, so can't test it, but I like the PowerPoint's dual screen presenter view, where you can show the slides on the projector and a control panel on your laptop screen, is this possible?\nIt's not possible at the moment\n\n> -no organisation chart tool\n> -tables are better than PowerPoint\n> -shortcuts: --> works, ==> doesn't\nCan you please explain what you mean by that. Give an example\n\n> -can't add drawing guides to master slide\nIt is possible here. Go to the master slide and drag the help line out of the ruler. The guild lines are reworked at the moment so that they will be of more help in the next version. They are allready partly in svn.\n \n -> not usable for any business use, when presenting your company to outsiders, everything needs to be really polished and professional, never ever crash\nI found it much easier to use that OO-impress or Powerpoint and that was at version 1.2. :-)\n\nThorsten"
    author: "Zagge"
  - subject: "Create (more) KLIK packeges"
    date: 2005-11-05
    body: "klik://krita  is not enough! ;-)\n\nklik://kword\nklik://kword-svn\nklik://kspread\nklik://kspread-svn\n...\n"
    author: "Hans"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "Btw., klick://krita is old, isn't it?\nIt can't open jpeg files on my suse 9.3.\n\nSo I'm waiting for klik://krita-nightly\n\nIf you think this is to much work, maybe you could ask the Amarok developers how they do \nklik://amarok-svn-nightly\n\nThis amarok klik package is updated by about 11:00 UTC almost every day."
    author: "Hans"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "If it can't open jpeg files, there's something wrong with the klik image... I've found it harder than I assumed to create working kliks."
    author: "Boudewijn Rempt"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "It seems that libxsltexport.la is missing.\n\nMaybe this can help ( I tried to open a *png img (jpeg does not work, too)):\n\n./.zAppRun Desktop/krita_1.4.88.cmg\n[...]\nkoffice (lib kofficecore): kritapart.desktop found.\nkoffice (lib kofficecore): kritapart.desktop found.\nkoffice (filter manager): KoFilterEntry::query(  )\nkoffice (filter manager): Filter: KFormula-Export f\u00fcr LaTeX ruled out\nkoffice (filter manager): Filter: KWord-Export f\u00fcr LaTeX ruled out\nkoffice (filter manager): Filter: LaTeX-Exportfilter f\u00fcr KSpread ruled out\nkoffice (filter manager): WARNING: Huh?? Couldn't load the lib: Library files for \"libxsltexport.la\" not  found in paths.\nkoffice (filter manager): Filter: KOffice-Export f\u00fcr XSLT does not apply.\nkoffice (filter manager): Filter: HTML-Export f\u00fcr KSpread ruled out\nkoffice (filter manager): Filter: ASCII-Export f\u00fcr KWord ruled out\nkoffice (filter manager): Filter: HTML-Export f\u00fcr KWord ruled out\nkoffice (lib kofficecore): kritapart.desktop found.\nkoffice (lib kofficecore): KoDocument::openURL url=file:///home/mw/testbild.png\nkoffice (lib kofficecore): kritapart.desktop found.\nkoffice (lib kofficecore): KoDocument::openFile /home/mw/testbild.png type:image/png\nkoffice (filter manager): KoFilterEntry::query(  )\nkoffice (filter manager): Filter: KOffice-Export f\u00fcr XSLT doesn't apply.\nkoffice (lib kofficecore): WARNING: libtiff.so.4: Kann die Shared-Object-Datei nicht \u00c3\u00b6ffnen: Datei oder  Verzeichnis nicht gefunden\nkoffice (filter manager): ERROR: Couldn't create the filter.\nkoffice (lib kofficecore): KoMainWindow::addRecentURL url=file:///home/mw/testbild.png\nkoffice (lib kofficecore): [KoMainWindow pointer (0x8300e70) to widget krita-mainwindow#1, geometry=1024 x747+0+0] Saving recent files list into config. instance()=0x81156f0\n"
    author: "Hans"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "\"koffice (lib kofficecore): WARNING: libtiff.so.4: Kann die Shared-Object-Datei nicht \u00f6ffnen: Datei oder Verzeichnis nicht gefunden\"\n------------\n\nHere is the fault!\n\nYou can fix it locally on your system (but this is not very klik-like, interfering with the base system!):\n\n--> cd /usr/lib; su ; ln -s libtiff.so.3 libtiff.so.4\n\n\nThe real fix would have to be inside the klik package. If you know how to do it, create the same symlink inside the AppDir:\n\n--> cd /tmp/klik/krita; su; ln -s /usr/lib/libtiff.so.3 libtiff.so.4\n\nand repack the .cmg -- See http://www.kdedevelopers.org/node/1474 --> \"klik tips+tricks: how to hack (or fix) a klik bundle\"."
    author: "klik power user"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "I hope to find some time next week to update the klik package -- thanks for the pointers. Of course, if someone would want me to help maintain the thing, undying gratitude and a honourable mention in the about box would be their eternal reward!"
    author: "Boudewijn Rempt"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "I know Kurt started to work on a brand new KOffice klik package. But this got stuck for a few reasons, as he told me:\n\na) work-related (overload from his employer)\n\nb) he had some problems with khelpcenter not displaying manuals and translation strings from inside the bundle, but using the (old) system wide installed documentation instead\n\nc) he didnt think there was much of an interest from the KOffice developer side in having a kick-ass klik, so no support from them\n\nd) he therefor will not give up to work on a klik://koffice bundle altogether, but will give it less priority, and try to convince the KOffice developers again once he can show off something and proof his package works as he intends it to."
    author: "klik power user"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-05
    body: "Yes, I know about the khelpcenter problem -- the big problem there was that nobody knew a solution :-(. As for no interest from KOffice developers, the simple fact is that everyone is incredibly busy, so we were quite glad he was working on it. It also turned out that it's a problem if you've got KOffice installed already if you want to test the klik package. I should have installed a vmware image or something like that for testing the klik package -- but then again, my time isn't just limited, it's constricted like a boa constrictor who's trying to eating a crocodile."
    author: "Boudewijn Rempt"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-06
    body: "klik://koffice ??\n\nWhy should anybody want to have a klick://koffice?\nOnly because I want to do some image manipulation I don't want to downlaod kword, kspread, kexi,...\n\nIf I want to test kword I don't need krita, kexi,...\n\nklik is suited very well for testing purposes (svn version,...). If I want to use kword 1.4 or kspread 1.4 I would install the RPMs."
    author: "Hans"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-06
    body: "## \"klik://koffice ??\"\n----\nYes, why not?\n\nAfter all, there *is* an integrated KOffice suite, as opposed to single-applications-only. Didn't you know that?? Check it out, it's cool.\n\n\n## \"Why should anybody want to have a klick://koffice?\"\n----\nSilly question, honest.\n\nSilly, because nobody ever demanded *everybody* to wanting to have klik://koffice  (learn the spelling, BTW!)\n\nWhy should everybody have to download all the single apps individually, if he can have them all in one go?\n\n\n## \"klik is suited very well for testing purposes\"\n----\nRight you are.\n\n\n## \"If I want to use kword 1.4 or kspread 1.4 I would install the RPMs.\"\n----\nSo be it...\n\nBut what if you are a developer, or a translator, or a documentation writer? What if you are running the SVN code anyway, and need quick access to one or more of the previously released versions (without messing with your RPM package manager)??\n\nklik lets you use multiple version of the same software without making your package manager freak out.\n\nI hope you do not intend to dictate what Kurt works on. But I'll talk to him on IRC, and ask if he could also make a set of \"klik://single-koffice-apps-for-hans\" kliks."
    author: "klik power user"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-06
    body: "> ## \"klik://koffice ??\"\n> ----\n> Yes, why not?\n> \n> After all, there *is* an integrated KOffice suite, as opposed to\n> single-applications-only. Didn't you know that?? Check it out, it's cool.\n\nI know that koffice is an integrated office suite. And I did check it out.\nBut this resulted in the experience that (at least I) *never* embed Krita into Kspread or Kword into Konqueror or things like that.\n\nDo you really want to start up the koffice shell if you just want to edit an image with krita?\n\nDo you really want to embed krita into kword?\n\nIntegration is nice - but only when it makes sense.\n\n> I hope you do not intend to dictate what Kurt works on.\n\nDid I sound so? Sorry.\n\n> But I'll talk to him on IRC, and ask if he could also make a set of\n> \"klik://single-koffice-apps-for-hans\" kliks.\n\n:-)"
    author: "Hans"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-06
    body: "\"Do you really want to start up the koffice shell if you just want to edit an image with krita?\"\n-----\n\nNo, of course not.\n\nBut the private klik://koffice  Beta package that Kurt made availiable to me has the nice feature that, while it defaults to start \"koshell\" it can still start each and every of the contained applications in single application mode too.   ;-)\n\nI talked to him now (not on IRC, but on the phone), and he said he'll also offer your list of desired kliks as single apps:\n klik://kword\n klik://kword-svn\n klik://kspread\n klik://kspread-svn\n ...\nbut he had little time currently, can't do it on his own, and he'd need help.\n\nSo how/where can he contact you?\n"
    author: "klik power user"
  - subject: "Re: Create (more) KLIK packeges"
    date: 2005-11-06
    body: ">> But I'll talk to him on IRC, and ask if he could also make a set of\n>> \"klik://single-koffice-apps-for-hans\" kliks.\n\nHe talked to me on the phone (since he didnt succeed to find me responding on IRC).\n\nSo yes, it is possible to create all the klik bundles you like. Each and every single one of the KOffice suite. Even an \"-svn-nightly\" of each.\n\nHelp me with that. I can't do it all on my own. You could beoome a recipe maintainer for them. Being a recipe maintainer is very easy. Easy, because we will set up the initial recipe and make it work. You, you will only be looking at user feedback on klik.atekon.de, will maybe help maintain the wiki site for the klik app in question, and will modify the recipe to point to updated version of Debian packages.\n\nHow does that sound to you?\n\nAn example for you to see how it works and to test is here:\n\nklik://amarok-svn-nightly\n\n(to enable the download, type the recipe maintainer's email ian@monroe.nu when prompted for it.) - Look at what the recipe looks like from the maintainer's point of view:\n\nhttp://klik.atekon.de/maint/\nhttp://klik.atekon.de/maint/recipe.php?package=amarok-svn-nightly\n\nAll of what you will need to care for is the field \"External URLs\". That field describes the input ingredients to the recipe.\n\nEither build yourself the KOffice packages for Debian Sarge to feed klik with. Or find a friendly Debian packager whose work you could make available for klik.\n"
    author: "Kurt Pfeifle"
  - subject: "Use koffice in house"
    date: 2005-11-05
    body: "I use koffice in house solely, because he is fast, is easy and KOffice integrates best with my desktop:\n\n- Table support in KWord is pretty bad. I'd   like easier control over placement & size of   table elements, in particular.\n\nI use Kword (SUSE 9.3 with unicode UTF8) in Spanish and have problems with the orthographic corrector \n  myspell-spanish-20050118-4\n  ispell-american-3.2.06-463\n  aspell-0.60.2-3\n  ispell-3.2.06-463\n  ispell-spanish-1.5-245\n  ispell-british-3.2.06-463\n  myspell-british-20040208-32\n  aspell-32bit-9.3-7\n  \n\nI have voted by desire 95601, because I believe that it is very good idea\nhttps://bugs.kde.org/show_bug.cgi?id=95601\n\n\nKformula is frustrating\n\nKspreadsheet\nIt must improve the functions, diagrams and graphs.\n\n\nIt is good idea, to add a function to save the files with password."
    author: "Antonio"
  - subject: "It's a pity Kword is not stable enough :("
    date: 2005-11-05
    body: "I've installed Koffice in my AMD64 Debian sid box because OOffice.org is not ported to AMD64 architecture yet. I was really looking forward to using Koffice as it would look native on my KDE 3.4 desktop.\n\nAt the begining it's a bit difficult to find some of the features, as I am acustomed to OO.org's features arrangement.\n\nI find that the exporting module to OASIS files needs some work, especially for Spreadsheet files, because most of the times I have to rewrite most of the formulas in OO.org in my M$ Winsnooze partition.\nMoreover, the unstability of Koffice compromises its great features. :(\n\nI suggest that, as OO.org is opensource, the Koffice team should analyse some of the code for exporting and importing M$ Office files, as well as OO.org OpenDocument files, so that the team can get some new ideas or improve the existing code.\n\nDespite these drawbacks, I think that Koffice has a promising future if you keep up the good work. In fact, its main advantage is that it is waaaaaay faster than any office app. :)\n\nKDE rocks! Let's make Koffice rock too! :)"
    author: "Jos\u00e9 Su\u00e1rez"
  - subject: "Re: It's a pity Kword is not stable enough :("
    date: 2005-11-06
    body: "When the AMD64 port of oo.org is ready there is nice package for intergating oo.org with kde http://packages.debian.org/unstable/kde/openoffice.org-kde"
    author: "petteri"
  - subject: "School & Small Business Use"
    date: 2005-11-05
    body: "The three main programs we use at home are KWord, KChart and Karbon14.\n\n1. KChart has my biggest gripe.  It can't be used if you don't have a color printer.  There is no option for hatching or pattern fill, only colors. Not usable if you print to black & white.\n\n2. Karbon14 doesn't allow you to draw non-poly lines.  Just a simple point-to-point line is all I want.  In polyline mode, you should be able to constrain the segment to vertical or horizontal by using the shift or crtl key (pick one).  When picking individual points, holding the CTRL key should allow to add or subtract from my selection.  I want to pick 2 points!\n\n3. KWord is mostly good for my light use, and the few items I've encountered have been bugged."
    author: "Charles Hill"
  - subject: "Using KOffice"
    date: 2005-11-05
    body: "Letters and simple documents are easily done with KWord. It's just that they don't look very professional - but the font spacing problem has already been discussed.\n\nFor formulas I still prefer LaTeX and for complex or professional occasions OpenOffice. OpenOffice has direct PDF output while KOffice's PDF output via ps2pdf sometimes has bad fonts (bitmap fonts instead of vector fonts?). \n\nKrita's GUI is just great. It doesn't get in the way (like GIMPs floating windows) and leaves enough space even on small monitors (Krita's GUI is how Karbon _could_ look like)\n\n\nI did some flowcharting with Kivio to document a software project during an internship in a bank. Kivio was nice to work with, I got things done really quickly and Kivio never crashed or mailfunctioned.\n\nA few times I had to make slides with scientific content: formulas, flowcharts, plots and text with occasional Greek symbols.\nTo be fair: It wasn't with KOffice 1.4. However back then:\n- I couldn't enlarge formulas so I ended up with tiny formulas on large slides. In the end I used Tex -> dvi -> ps -> eps for formulas\n- It was uncomfortable to embed images (plots as EPS, formulas as EPS) because KPresenter couldn't remove white borders or cut images\n- It was uncomfortable to embed small KOffice objects, e.g. small flowcharts (to abuse Kivio as a clipart gallery): The embedded Kivio area just had the size of the flowchart and was a pain to work with.\n\n\nBut even if it's not perfect (yet), KOffice is needed: It's small and fast, it's pure KDE and the GUI's of its apps are all alike so you feel \"at home\"."
    author: "HP"
  - subject: "Koffice not so office"
    date: 2005-11-05
    body: "Koffice is nice. It runs much faster than OOffice and it's great for most of the office work but... better MS Office documents support would be nice. I study chemistry and we use Excel for some calculations. KSpread have problems here:\n- find result: fails on simple formula here the X is twice (or more?) in the formula...\n- charts: should be like in MS Office (look, high IQ when getting data :P)\n"
    author: "Piotr"
  - subject: "KSpread"
    date: 2005-11-05
    body: "Since my personal spreadsheets are very simple (no graphics, only data and math), it works very well. And I love its speed. I'm not very confortable the way KWord works but, in general, I do prefer KOffice over OO.\n\nThank you very much, KOffice developers!"
    author: "Patrick"
  - subject: "Why I use KOffice?"
    date: 2005-11-05
    body: "I use KOffice mainly because it's integrated into KDE. The main problems that I'm having is in KWord and KChart. In KWord I don't know how to make 2 or more columns by a simple way (also in the footer/header). And in KChart is with making graphs.\n\nI think that KOffice still has some problems, but it's going to get better with time, because of it's great developers that are sacrificing their free time programming it."
    author: "MasterMind"
  - subject: "Re: Why I use KOffice?"
    date: 2005-11-07
    body: "Making graphs as in mathematical plots, or are there other stuff that you want to do, but can't?"
    author: "Inge Wallin"
  - subject: "Re: Why I use KOffice?"
    date: 2005-11-07
    body: "Like in Electronics. For example if you are doing an graph of a characteristic of diode. You have Current and Voltage, and you have to get a graph that starts from zero. (X axis is Voltage and Y axis is current). But I haven't managed to do this. I only could change the label for X axis, but it wasn't right. If you know what I mean :P"
    author: "MasterMind"
  - subject: "fix the footnotes!"
    date: 2005-11-05
    body: "I like KWord and I use it every day. There are two things that I think need to be done to improve KWord:\n1. more stability in general\n2. fix the footnotes! I write may academic papers, and whenever I write long papers with many footnotes, the footnotes display incorrectly. This bug alone makes KWord unusable for many of my papers sadly. "
    author: "Richard Horlings"
  - subject: "Writing Screenplays..."
    date: 2005-11-05
    body: "Although I currently mostly don't use KOffice due to the fact that all my files are used every day in Windows, OS-X, and Linux, so I need OOo's .doc format output, there is something I'd really like KOffice to add:\n\nScreenwriting tools\n\nCurrently, the best open-source screenwriting tool is celtx [1], and that's a program written for the Mozilla Application Framework, so it has quite a lot of limitations. Once a system supports as much as KWord does, the transition to being able to do Screenwriting is relatively small, so it has always amazed me that there is so little in the way of OpenSource tools available for it.\n\nSince KOffice already has, far and away, the most tools of any office suite, I'd kinda like to see this one added as well. Also, screenwriting tools hit a market that can really benefit from open-source: poor people trying to get a first screenplay picked up and can't afford to pay $200 for a standard screenwriting tool.\n\n[1] http://www.celtx.com/"
    author: "jameth"
  - subject: "Using KOffice in a company"
    date: 2005-11-06
    body: "I'm administrating the computer infrastructure of a local company with 4 computers. They are using KOffice, and in particular KSpread, for their usual office documents.\n\nThe main reason for using KOffice is because it really integrates very nicely into the desktop, provides the familiar look and feel, can utilize lot's of KDE's infrastructure and is not very bloated. Common tasks can be done easily.\n\nThere are currently two aspects that should further improve in KOffice to make it even more usable:\n\n1) Cell format (especially border styles and cell merging) handling in KSpread.\n\n2) We are very happy that KOffice rapidly moves towards the OpenDocument Format, as it increases compatibility to the industry standard a lot. Since MS Office basically has to support this standard as well, the document exchange with MS Office will be just as easy as it is with OpenOffice.org.\n\n\nI'm also working together with a larger company (about 50 people) who is considering moving to a more open office suite in the future. If KOffice turns out to be the best of breed at that time it will be chosen."
    author: "raphael"
  - subject: "Re: Using KOffice in a company"
    date: 2005-11-06
    body: "correction in last paragraph: ... company (...) which is considering ..."
    author: "raphael"
  - subject: "I use KOffice instead of OpenOffice in Amd64 linux"
    date: 2005-11-06
    body: "I use Kubuntu Breezy for AMD64 , I try to use OpenOffice 2 but crashing a lot in an AMD64 system.\n\nBut KOffice works like charm in an AMD64 system , good work !!!"
    author: "JuanC"
  - subject: "database-gui"
    date: 2005-11-07
    body: "I'm working in the EMS-area and am looking for a database-gui to replace the msaccess-thing we use up to now.\nSince more than a year I'm waiting for kexi to become useful. Looks good, but is not useful yet.\nThanks\nHJ"
    author: "hr"
  - subject: "KOffice is great sofar, but lacks (basic)features."
    date: 2005-11-08
    body: "For writing papers, I still use LaTeX, because it has the most convenient way to adhere to the typesetting of a publisher. For alot of other things, I find KOffice a really nice suite to work with.\n\nI must say that KOffice is a comming along nicely, I prefer to use it over OO, since koffice has a better integration with the K desktop and generally runs faster (ie: no (legacy)java stuff to hog your system). Though I still have some gripes with it two of which come to mind:\n\n* Creating tables in KWord is still a major pain. Especially if you wish to customize the looks and layout of a table. You cannot simply resize an entire table, making all cells larger, while staying equally spaced. Also, if you did resize all cells manually, KWord tends to 'reset' the table and impose its own rules on your table. (usually undoing everything you were trying to achieve)\n\n* KChart (1.4.1) isn't even able to do draw simple scatter- and/or XY-plots. ie: have one column of data (X-coordinates) plotted against another column of data (Y-coordinates). I can see that the current capabilities are sufficient for some simplistic statistics, but if you want to include it for any (scientific) presentation in the field of engineering for example, it is completely useless. (I still have to import pictures made in gnuplot into kpresenter.)\n\nWhat KChart should minimally be able to support (in addition of the current plots) is:\n- XY-charts (column1 = X) (column2 = Y)\n- XY-charts with error-bars (column1 = X, column2 = DX, column3 = Y, column4 = DY)\n- scatterplots (similar to XY, only no lines between points)\n- some form of control on how 'points' look. (dots, boxes, triangles, diamonds etc...)\n\nI hope these remarks will contribute to further improving this promising project.\n\nKind regards,\nErwin\n"
    author: "Erwin Mulder"
  - subject: "Re: KOffice is great sofar, but lacks (basic)featu"
    date: 2005-11-10
    body: "I join your demand about the lack KChart features. Because of that, I have to use OOo. Anyhow, this wish has been well known for the developers for years, and hopefully will be satisfied soon."
    author: "devians"
  - subject: "freelance journalist"
    date: 2005-11-08
    body: "I use kword for nearly all my work. I like the integration with kde and the wordnet thesaurus. As others have mentioned, the default appearance of kword could use some work. Why not have a gray border on all sides like most other word-processing apps? My wife is an editor, and she would use kword if it had a \"track changes\" feature like OpenOffice or MSWord. That feature is vital for the work of editors, so if you would like to see kword used in publishing, you'll need to implement it at some point.\nI'm looking forward to future releases. Thanks for all your hard work.\n\nTim"
    author: "Tim Folger"
  - subject: "free software"
    date: 2005-11-08
    body: "What attracts me to KOffice is that it is supported by a true free/libre open-source software community (as opposed to a mixture of corporate sponsorship and open-source as in OpenOffice.org), it is fast and integrates well with KDE.\n\nWhat I don't like in KOffice is that it's not as mature as OpenOffice.org, yet, but I am sure it will mature soon. I use KOffice, especially KSpread and the graphics applications, in non-critical jobs. For mission-critical work I use OpenOffice.org, but what I really love is KOffice. I don't see the time to ditch OpenOffice.org in favour of KOffice when the latter becomes more matured software.\n\nI believe that KOffice will gain momentum if 4 things come into realisation:\n\nFirst, the software must mature and the community should pay more attention to end user perceivable quality in future releases. KOffice 1.4 was a good step, and I hope that future versions will be even better.\n\nSecond, KOffice must target Microsoft Windows users, because these are the users who urgently need fast office software, since both OpenOffice.org and Microsoft Office are slow. A fast KOffice under MS Windows could attract more users into the free software world, and could help them to migrate to GNU/Linux or BSD later.\n\nThird, KOffice must offer more and innovative office applications. I believe that accountancy software like KMyMoney (or a KDE version of Gnucash) should be incorporated into the KOffice suite. New applications not present in MS Office or OpenOffice.org could attract even more users.\n\nFourth, KOffice must attract more developers and provide development tutorials for new contributors.\n\nI am sure KOffice can become a major office application in the near future, and I wish good luck to everyone involved in this wonderful project!\n\nNSK, http://nsk.wikinerds.org/"
    author: "NSK"
  - subject: "Insert object from file"
    date: 2005-11-09
    body: "KOffice is promising but there is still a really large headroom for improvements. I'll list here a few:\n1/ Currently there is no way to embed object from files. Imaging you have work on this kivio diagram and you saved a local copy of it. Then, you start to make a  report (with kword) and suppose you want to include your previously created diagram. So far, I could not find a way of accomplishing this. I would be nice to have a menu like 'Insert->Object from file'. This way we would be able to include previous created charts,diagram,spreadsheet tables.... into kword (or kspread)\n2/ The above could be also solved if the copy and paste of object would be better controlled. Currently I can not select the previous made diagram, copy it (Ctrl-C) and past it in word (Ctrl-V). When you do this, you can only paste it as a image or as text (this gives you the actual xml code !?). The same holds when trying to copy&paste a selected region of spreadsheet.\n3/ I would be really nice to have tracking features. They way MS Word handles is in my opinion really good.\n4/ Some connection from latex formula would be nice. Are there large difficulties in adopting the same kind of rendering TeX is using for formulas... nothing beats a formula rendering of knuth's code. \n\nHope it helps. "
    author: "Christof"
  - subject: "using K Office"
    date: 2005-11-09
    body: "I use KOffice pretty much on a daily basis to keeptrack of things. I run a small computer business and the workspace saves me a lot of time when switching back and forth from an invoice to email and back to a letter. great app, what would be nice is a bigger selection of templates and such to get users going\n\nDan Buchanan\nwww.buchanancomputer.ath.cx\n\nGO LINUX!"
    author: "Dan Buchanan"
  - subject: "Re: using K Office"
    date: 2005-11-10
    body: "I like Koffice as well.  However, I've encountered the same problems as others with the file corruption issue.\n\nOpen Office is a bit more stable.  I would really like to see Koffice overtake Open Office because I really like Koffice. One critical application for me that is missing from Koffice is a database program. I need such a program in my home office and am forced to rely on Access through my virtual machine.  I would definitely ditch Access if there were a Koffice replacement."
    author: "Robert Debusschere"
  - subject: "Re: using K Office"
    date: 2005-11-11
    body: "Give Kexi a try :-)."
    author: "Boudewijn Rempt"
  - subject: "Latex formula in kpresenter"
    date: 2007-02-16
    body: "I would love to use kpresenter, except that I need the ability to render latex formula inside the slides easily. There is a macro to do that in OpenOffice. Kformula is not a good way to typeset formulas if you do that often. Besides that, the ability to copy and paste formulas from latex source is important. I personally believe that Kformula should be rewritten to work as a well integrated latex front end, but if not, latex formulas need to be a possibility too."
    author: "Guilherme"
  - subject: "Insert image on kivio"
    date: 2008-01-02
    body: "Can't find 'insert image' facility on kivio? when that will appear in kivio?"
    author: "tonny sofijan"
  - subject: "Re: Insert image on kivio"
    date: 2008-01-02
    body: "With KOffice 2.1: Peter Simonssen has been too busy with konversation and other projects to spend much time on Kivio. But all the basics are already done, it's just finishing the application that needs lots of work."
    author: "Boudewijn Rempt"
---
Are you using KOffice? What are you using KOffice for? Why did you decide to use KOffice? What are your main problems? We want to know who uses KOffice and we are especially interested in companies and people using KOffice applications in the course of their business.  We have done usability testing with OpenUsability on some of the KOffice programs and will be working more with them.  Now we want to reach our users directly and ask them what they think.







<!--break-->
<p>The KOffice hackers have been working on KOffice for quite a few years now and we've received quite a lot of great and useful feedback from users. KOffice is picking up a lot of momentum now and in the run-on towards the 1.5 release we would like to get to know our users a lot better.</p>

<p>So... Are you using KOffice? What are you using KOffice for? Why did you decide to use KOffice? What are your main problems? Please not too many comments about reading MS Office files and the difficulties of loading large files, we are continuing to work hard on these areas.</p>

<p>As we wrote above, we want to know who uses KOffice right now. We are <em>especially</em> interested in companies and people using KOffice applications in the course of their business. If you are using KOffice and want to help us, please leave a comment below or contact our marketing guy Inge Wallin (inge<span>@</span>lysator.liu.se) and give him your story.</p>

<p>Thanks!</p>







