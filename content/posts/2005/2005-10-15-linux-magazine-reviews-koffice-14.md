---
title: "Linux Magazine Reviews KOffice 1.4"
date:    2005-10-15
authors:
  - "brempt"
slug:    linux-magazine-reviews-koffice-14
comments:
  - subject: "Koffice Review PDF is here!"
    date: 2005-10-15
    body: "http://www.linux-magazine.com/issue/60/KOffice_1.4_Review.pdf"
    author: "fast_rizwaan"
  - subject: "xpdf shows garbage"
    date: 2005-10-15
    body: "use gv (ghostview) or kghostview to view the pdf. i wonder whether that's a non standard pdf file."
    author: "fast_rizwaan"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-15
    body: "Looks like a standard PDF to me, PDF v 1.3 according to the metainfo. And yes Kpdf shows it nicely."
    author: "Morty"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-15
    body: "Nope.  KPDF screws it up for me, too.\n\nOn another note... is Karbon being properly maintained again now?  It's completely useless on my PowerPC box, since it renders colours incorrectly (the white when you start it up is cyan, for instance).  The bug report has basically been ignored, and when I last contacted the maintainer, he advised me to use another program.  If it's going to stay at that level of functionality, I'd rather see it removed from KOffice, so that others realise an alternative is needed."
    author: "Lee"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-15
    body: "Yes, Karbon is maintained again. If you look at the release announcement (http://koffice.org/announcements/changelog-1.4.2.php) you see that there are a host of improvements to Karbon. However, the endianness issue is probably not easy to solve with the current Karbon rendering backend, especially when it is very likely that none of the Karbon developers has access to a ppc machine.\n\nWhen my powerbook was still working well enough I gave fixing the endianness issues a shot, and failed. Nowadays I'm not even sure that Krita is okay on ppc...\n\nFor KOffice 2.0, Karbon will likely use a completely different rendering engine -- KCanvas or Arthur or maybe even Xara's engine that's going to be opensourced really soon. That should solve all endianness issues in one fell swoop."
    author: "Boudewijn Rempt"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-16
    body: "i see the garbage in kpdf as well.."
    author: "Jonathan Dietrich"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-15
    body: "kpdf shows garbage to me too...and what's odd, the \"Print Preview\" shows the article just fine :-)\nI am using kpdf 0.4.2 with KDE 3.4.2 \n"
    author: "Leonardo Peixoto"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-15
    body: "That's odd indeed, anyway I use kpdf 0.5 from the 3.5 branch so perhaps that's why it works for me. I did not think the render part of kpdf had changed much since 3.4, but they may have slipped in some bug fixes:-)\n\nAnyway I did a md5sum of the pdf, does your match: 8f9e57be0809dbbc49a378a5ee3744e1. If it does, well then you have to wait for 3.5:-) (or Use KGhostwiev)."
    author: "Morty"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-15
    body: "The document is displayed fine here. Only the images suck a bit when you zoom in."
    author: "Bram Schoenmakers"
  - subject: "Re: xpdf shows garbage"
    date: 2005-10-16
    body: "It displays OK for me.  But, only OK, not really good.\n\nAdobe Acrobat Reader renders the fonts (which are embedded) somewhat better."
    author: "James Richard Tyrer"
  - subject: "erratum"
    date: 2005-10-15
    body: "It looks like the reviewer doesn't have a clue about open formats:\n\nfrom TFA: \"Version 1.4 still uses proprietary Koffice format\"\n\nKoffice format is fully open, free and documented. Just because it's not a standard doesn't mean it has to be proprietary."
    author: "Patcito"
  - subject: "Re: erratum"
    date: 2005-10-15
    body: "Conversely, proprietary doesn't mean closed, locked and undocumented; it just means that one entity \"owns\" -- is the proprietor of that file format. Which is true for the KOffice file formats. And documented... If you count a dtd as documentation, then, yes, it's documented, but I know for a fact that I've procrastinated on updated the dtd for the Krita file format for about a year now."
    author: "Boudewijn Rempt"
  - subject: "Krita"
    date: 2005-10-15
    body: "I recently installed Suse 10.0 and tryied to live a bit without gtk apps.\nAs I upgrated to KDE 3.2 beta1, everthing was fine, konqueror in this version is almost catching firefox, kopete is great, konqueror instead of gftp...\n\nBut then I got to krita instead of gimp. Krita is nice, I really liked it's interface and tools, but I missed one feature, the one I most use in gimp: Paste as New Image. Without this I can't be produtive in krita, so I installed gimp, too bad. But I hope this will be implemented soon.\n\n(OBS: I found the wish on bugs.kde.org and voted for it)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Krita"
    date: 2005-10-15
    body: "I think I implemented it already for 1.5 :-)"
    author: "Boudewijn"
  - subject: "Re: Krita"
    date: 2005-10-15
    body: "Great news!\nI hope a bete is out soon, so we can teste it :)\n\nThank you."
    author: "Iuri Fiedoruk"
  - subject: "Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "I know that OOo has a SOC going on and that AbiWord uses that project http://bobo.link.cs.cmu.edu/link/ . It would be cool to have it in Koffice too if that's not too much work :)"
    author: "Patcito"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "hehehehe , funny "
    author: "ch"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "I mean reusing Link Grammar  http://bobo.link.cs.cmu.edu/link in Kword should be too much work though implementing a whole new Grammar checker algorithm would be q bitch for sure :)"
    author: "Patcito"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "Why do you want to include it only in KWord? Why not make an implementation like with kspell, making it accessible from all over KDE. Then you would not only have spell, but grammar checker too in web forms when you use Konqueror. Cold be usefull for some, don't you think?\n  "
    author: "Morty"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: ">>Cold be usefull for some, don't you think?\nI totally agree."
    author: "Patcito"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "Implementing grammar checking as a KDE-wide service like KSpell should be a nice self-contained project for someone who wants to dive into KDE hacking -- maybe something for you? Could be a lot of fun and very educational."
    author: "Boudewijn Rempt"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "Actually I do want to learn KDE hacking. I'm learning Qt4 right now and once I have sufficient knowledge I'll learn KDE which will probably KDE4 by the time.\nHere is my first Qt4 project http://p80.free.fr/katiuskapagina/ (a reworked of a previous Kommander project of mine), it's very poorly written I guess but it's my first Qt/C++ app :). And yes that would be a great start for hacking in KDE but it seems a little too difficult for now :(\n\n"
    author: "Patcito"
  - subject: "Re: Grammer checker for Koffice plz"
    date: 2005-10-16
    body: "Don't worry about difficult: you'll learn as you go."
    author: "Boudewijn Rempt"
---
The <a href="http://www.linux-magazine.com/issue/60">November issue</a> of <a href="http://www.linux-magazine.com">Linux Magazine</a> presents a thorough review of KOffice 1.4 written by Marcel Hilzinger. While based on an older version of KOffice, <a href="http://dot.kde.org/1129013255/">KOffice 1.4.2 has just been released</a> with many fixes, the review is fair, thorough and balanced. It was useful too, within two hours of having read the review Thomas Zander committed the first fix for a problem mentioned by Marcel.  It is well worth a read, and not only because of the praise heaped upon <a href="http://www.koffice.org/krita">Krita</a>.


<!--break-->
