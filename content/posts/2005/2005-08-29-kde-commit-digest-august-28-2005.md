---
title: "KDE Commit Digest for August 28, 2005"
date:    2005-08-29
authors:
  - "dkite"
slug:    kde-commit-digest-august-28-2005
comments:
  - subject: "webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "thank you, thank you!!!"
    author: "Mark Hannessen"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "p.s. the \"kopete\" link is broken.\n\nit should be www.kopete.org\nnot http://dot.kde.org/1125268049/kopete.kde.org\n"
    author: "Mark Hannessen"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "Fixed link."
    author: "binner"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "Too bad that there are ony very few webcams left that work with Linux :-("
    author: "birdy"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "What do people actually use these webcams for? I had one and tried to use\nit to take a snapshot out of the window every minute, but the quality of the\nimages was simply abysmal. Same with a few models I've tested. None of the images was useful for anything at all, one could hardly recognise a car standing outside the window. :)\n"
    author: "Chakie"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "I personally don't use one, but I've talked to people with them.  Looking at the person you are talking to adds alot to the conversation, IMHO.\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "With family abroad, it makes you feel just that little bit closer when you talk."
    author: "Dan"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-30
    body: "Some of us live away from family and parents so its good to take a \"live\" look at them now and then ... in fact, until now, this was the *only* reason for me to boot into Windows. There is another project, gaim-vv (gaim-vv.sf.net), that promises webcam features but hasn't delivered yet. Go Kopete!"
    author: "Kanwar"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-31
    body: "While most webcams are indeed shit in terms of quality, there are a few good ones out there. I recommend the Logitech Quickcam Pro 4000.  640x480, great quality for a webcam, and works perfectly in linux.  Of course you will be paying more than for the bargain basement cams, but it is well worth it.  Got mine for just over $100 canadian"
    author: "Leo"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "Thank you indeed - this is fabulous news!"
    author: "Kevin Colyer"
  - subject: "Re: webcam!!!!!!!!!!!!!!!!!!"
    date: 2005-08-29
    body: "I hope they implement sound chatting in ICQ/MSN some day also. But I'm putting my hopes on google talk voice that google announced will be open soon."
    author: "Iuri Fiedoruk"
  - subject: "Emacs keys"
    date: 2005-08-29
    body: "I like Kate quite lot, but can't get my self using it regulary because I'm too familar with Emacs keybindings and indentation. I'm hoping that some day there will be Emacs-mode to Kate which contained those two features :)"
    author: "Petteri"
  - subject: "Re: Emacs keys"
    date: 2005-08-29
    body: "http://kde-apps.org/content/show.php?content=21706\n\nYou are welcome."
    author: "manyoso"
  - subject: "Re: Emacs keys"
    date: 2005-08-30
    body: "Thanks :) It seems to lack some of critical keybindings but it's a good start."
    author: "Petteri"
  - subject: "Which webcams are support with kopete?"
    date: 2005-08-29
    body: "I wonder which devices are supported in kopete/linux 2.6, could you suggest a URL for the supported webcams, please!"
    author: "fast_rizwaan"
  - subject: "Re: Which webcams are support with kopete?"
    date: 2005-08-29
    body: "http://www.linux.com/howtos/Webcam-HOWTO/devices.shtml\n"
    author: "Morty"
  - subject: "More EVIL Plans please ;)"
    date: 2005-08-29
    body: "Stephen Binner, thank you for making utilties menu as organized based on categories. I have some more evil plans for Internet menu, Graphics Menu, Multimedia menu etc.,\n\nGraphics/\n\n        Image Viewers/\n           Kuickshow\n           KShowimage\n           KView\n        \n        Image Editors/\n           Kolourpaint\n           The Gimp\n           Krita\n        \n        PS/PDF Viewers/\n           KPDF\n           XPDF\n           KGhostview\n           \n        Fax Viewers/\n           KfaxViewer\n\n----------------------\n\nThe Idea is simple, putting applications based on their \"Categories\", say for example Internet Menu which is way too cluttered we can make it uncluttered by creating submenus based on category name:\n\nWeb Browsers/\nKonqueror\nMozilla 1.7\nMozilla Firefox 1.0.6\nNetscape 7\nOpera 8.02\nGaleon\nDillo\n\nP2P & File Sharing/\nApollon\nKMLDonkey\nAMule\nSMB4k\nKTorrent\nAzureus\nLimewire\nGiftCurs\n\nInstant Chat & Messengers/\nKopete\nGaim\nXChat\nKSirc\nKonversation\nSkype\nQNext\n\nDownload Managers/\nKGet\nDownloader 4 X\n\n\nInternet Connection Dialers/\nKPPP\nKDSL\nRP-PPPOE\n\nFTP Clients/\nKFTPGrabber\nKBear\ngFTP\n\n\nI hope you got the idea ;) thanks."
    author: "fast_rizwaan"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-29
    body: "I think we're making the menus way too deep. The problem isn't the lack of categorization, we already have that. The problem is that we have too many applications. For example, look at your list of web browsers. Someone with that many browsers is probably a web developer, and thus able to handle editing his own menus.\n\nThe root menu should allow quick access to entries, but you don't have quick when you have to click -> scan categories -> click -> scan subcategories -> click-> scan more subcategories -> click -> scan enties -> click.\n\nThe menus certainly could do with some reorganization. But let's keep it sensible. If we have too many applications, let's put them under \"More Applications\". And no, I'm not suggesting Microsoft-style \"personalized\" menus. That's just evil.\n\n</rant mode=\"monday\">"
    author: "Brandybuck"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-29
    body: ">The problem is that we have too many applications. \n\nYes, This is how free software developers work, create too many similar applications which does partial work. Say konqueror, there are many sites which can't be accessed with it, likewise firefox is not that great for multimedia, Opera is good with \"Internet Explorer only sites\". Hence we (a typical GNU/Linux+KDE user) need many applications to get our work done.\n\nlikewise there are many Image editing application which has their own strong points, say kolourpaint which is very useful for fast editing/manipulation of images but lacks layers. krita is way too complex and slow and many a things can't be done with krita. gimp is complex too, but does the work.\n\nlikewire for Multimedia players, noatun, kaboodle, can't play VCDs or DVDs, Kaffeine could do that but Kaffeine is xine based hence it can't play few codecs which can be played by mplayer engine. kmplayer supports both xine and mplayer but behaves erratically, mp3 playback and slider won't work. Amarok is good but slow (heavy on resources) compared to juk, kplayer can play from network (smb, http etc transparently) better than kaffeine or kmplayer, but has VCD, DVD problem :( Real Player can't play videos.\n\nBecause of LACK OF GOOD (in performance, usability, features) applications, we have to depend on multiple applications.\n\n>The root menu should allow quick access to entries\n\nthis is where the kmenu flaw is, in the name of quick access, it has become a chaos, too many applications are available for GNU/Linux both GUI (KDE/GNOME/other toolkits) and Console based. And those are gems, each application is useful for a particular purpose. Hence categorized menus will make more sense instead of all applications scattered in one submenu.\n\n>If we have too many applications, let's put them under \"More Applications\". \nthat's not nice, because, clutter in the \"More Applications\", finding application there will be more annoying than finding \"Decently organized menus based on Categories.\"\n\nWe need nicely and appropriately categorized applications shortcuts in KMenu, so that things are easier to find and access than to get confused looking at cluttered menus with too many shortcuts. \n\nbtw, i'm not ranting ;)\n"
    author: "fast_rizwaan"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-29
    body: "I always use a totally overpopulated kde menu (still, I know what and where to find) because I like having everything in the menus.\n\nStill, I also use a run command panel applet, for very quick launching of applications. In fact, I use the run command applet and the konsole command line more than the menu."
    author: "Levente"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-30
    body: "> I always use a totally overpopulated kde menu (still, I know what and where to find) because I like having everything in the menus.\n\nThat's good, I also use overpopulated menus and use Alt+F2 to launch application. It is good that you and I know where and what is present in our KMENU...\n\nBut for not so acquainted KDE users or newcomers or ex-windows users or simply new to computer users, would find it (current overpopulated menu) horrifying. And I am concerned for those people who are new to KDE and need KDE for their day to day work without remember where and what is present.\n\nWe should be a little more selfless in this regard. a properly and nicely organized menus based on categories and sub-categories would make KMenu useful for new and not so new people alike.\n\nAnd new people could not understand the application by name kopete, gaim, etc., they are just way too GEEKY!!! even the description (category) causes trouble in a overpopulated menu.\n"
    author: "fast_rizwaan"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-29
    body: "for KMenu I've seen a lot of patches. there was one at LinuxTag I liked the most: if you use the left mouse button, it works as usual. if you middle-click on a category, lets say \"Graphics\" the most used program or the one on top of the submenu (configurable), maybe \"The GIMP\", is started, without any need to even let the submenu appear. right click lets you directly edit the submenu.\nmay seem simple, but with this you can stuff more entries into the same menu space."
    author: "nachbarnebenan"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-29
    body: "What about a compromise?\n\nBreak out into sub-categories as suggested, but also offer the default at the same level as the folder detailing alternatives...\n\nSo in this case you end up with 12 items total in the menu Internet (1 default for each sub category, and one sub menu per sub category)...\n\nThis effectively breaks \"More Applications\" into 6 organised directories.\n\nInternet/\n Web Browser (Konqueror)\n Web Browsers (Alternatives)/\n Konqueror\n Mozilla 1.7\n Mozilla Firefox 1.0.6\n Netscape 7\n Opera 8.02\n Galeon\n Dillo\n\n P2P & File Sharing (KMLDonkey) \n P2P & File Sharing (Alternatives)/\n Apollon\n KMLDonkey\n AMule\n SMB4k\n KTorrent\n Azureus\n Limewire\n GiftCurs\n \n Instant Chat & Messenger (Kopete)\n Instant Chat & Messengers (Alternatives)/\n Kopete\n Gaim\n XChat\n KSirc\n Konversation\n Skype\n QNext\n \n Download Manager (KGet)\n Download Managers/\n KGet\n Downloader 4 X\n \n\n Internet Connection Dialer (KDSL) \n Internet Connection Dialers/\n KPPP\n KDSL\n RP-PPPOE\n\n FTP Client (KBear) \n FTP Clients/\n KFTPGrabber\n KBear\n gFTP\n \n\n"
    author: "Jonathan Dietrich"
  - subject: "Re: More EVIL Plans please ;)"
    date: 2005-08-30
    body: "good idea, but I have a different idea for that too, that is to have a default application launch \"on click\" or \"double click.\"\n\nInternet/\n   Web Browsers/ <- clicking (or double clicking) should launch the preferred application. \n   File Sharing/\n\nlet's assume that we set these application for each sub-categories:\n\nWebbrowser         -> konqueror (or firefox as the user sets)\nP2p file sharing   -> KMLdonkey\nE-Mail Client      -> KMail\nFTP Client         -> KFtpGrabber\nChat and Messenger -> Kopete\n\nJust clicking/double clicking on the \"Submenu item\" \"Web Browser\", \"P2P File Sharing\", \"E-Mail Client\", \"FTP Client\" should start the default/preferred application (perhaps with some animation just like adding an applet in KDE 3.5 does).\n  "
    author: "fast_rizwaan"
  - subject: "how about proxy?"
    date: 2005-08-29
    body: "As a corporate junkie sitting behind firewalls all day, what's kopetes \nstate when it comes to http(s) proxy support with jabber (google talk)?\nIf the support exists ( ie. reads settings from konq ) didn't manage to\nget it going :("
    author: "Anonymous Coward"
  - subject: "Re: how about proxy?"
    date: 2005-08-29
    body: "in kde3.5 too according to the feature plan.\n(it's in the green section)\n\nhttp://developer.kde.org/development-versions/kde-3.5-features.html"
    author: "Mark Hannessen"
  - subject: "Re: how about proxy (and what about konqueror)?"
    date: 2005-08-30
    body: "Konqueror always has the username/password fields disabled for proxies (automatically ask as required is the only one enabled). What's more, I have tried  http proxy with username/password and it never seems to work. \n\nAny idea why this is so? Firefox, otoh, seems to have no issues engaging with proxy servers (including socks etc).\n\nThis is one area which needs immediate attention in konqueror if its to become a serious contender in corporate environments where proxy servers are almost a given."
    author: "Kanwar"
  - subject: "Speech recognition - very cool.  What engine?"
    date: 2005-08-29
    body: "I really like the way speech technology is being integrated directly into KDE - first KTTSd, now KHotKeys support for speech recognition.\n\nI'm curious as to what engine is used for this though - the Sphinx project has a very good speaker independent speech recogniser.  These aren't necessarily suitable for dictation but would rock for control.\n\nSince no training would be required, it'd not only be good for hotkeys-like shortcuts but also for voice control of any app by speaking the names of menus.\n\nHas anyone looked at something like this?  It'd be neat :-)"
    author: "Mark Williamson"
  - subject: "Re: Speech recognition - very cool.  What engine?"
    date: 2005-08-29
    body: "It looks like a roll-your-own speaker-dependant FFT+voodoo thing.  Lighter weight than sphinx, for sure, but lighter on the dependancies.  I have a speech recognition project on the back burner that should allow speaker-independant recognition with similar run-time overhead but hefty training overhead that I was planning to throw at the KDE community but it looks like Olivier beat me to the punch.  Thanks for adding this one."
    author: "Matthias Granberry"
  - subject: "Misleading MIME icon for KOffice files"
    date: 2005-08-30
    body: "Hm... I didn't know KOffice is in buisness of promoting \"Gay Pride\"\n\nhttp://commit-digest.org/?diff&path=/trunk/KDE/kdelibs/pics/crystalsvg/cr22-mime-koffice.png&revision=452471\n"
    author: "Daniel"
  - subject: "Re: Misleading MIME icon for KOffice files"
    date: 2005-08-31
    body: "You really should go out sometimes and watch nature."
    author: "Anonymous"
  - subject: "Re: Misleading MIME icon for KOffice files"
    date: 2005-08-31
    body: "rainbow colors represent each application in koffice suite. :)\n\nbtw, gay actually means happy!"
    author: "fast_rizwaan"
---
In this week's <a href="http://commit-digest.org/?issue=aug282005">KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=aug282005&all">all in one page</a>):

KTuberling gets Serbian sounds.  Configuring backgrounds per display in Xinerama implimented.  SoC projects progress.  Speech Recognition (for hot-keys) merges into KDE 3.5. Webcam support for MSN in <a href="http://kopete.kde.org/">Kopete</a>.  <a href="http://kate.kde.org/">Kate</a> adds syntax highlight support for /etc/fstab, /etc/mtab files... Rejoice!


<!--break-->
