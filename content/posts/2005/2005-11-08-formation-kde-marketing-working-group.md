---
title: "Formation of KDE Marketing Working Group"
date:    2005-11-08
authors:
  - "wolson"
slug:    formation-kde-marketing-working-group
comments:
  - subject: "Great news Wade"
    date: 2005-11-08
    body: "I'm glad to see this pushing forward.  Hopefully this will provide some much needed coordination and planning."
    author: "manyoso"
  - subject: "Good to hear"
    date: 2005-11-08
    body: "That's about hella time. Me, Segundunum and a few others are working our tail off in fighting for KDE on various sites."
    author: "AC"
  - subject: "Re: Good to hear"
    date: 2005-11-08
    body: "I wouldn't go so far as to say that - I've just been pointing out what I believe to be common sense. Believe me, I'm not just sticking up for KDE for the sake of it. As far as I'm concerned KDE is an economically viable desktop environment that is more than capable of making it in the world. Novell's new COO Ron Hovsepian likes cost effectiveness and viability as well, and trust me, this guy is not going to get taken for a ride. KDE as an open source desktop, a set of applications, a development framework and support from a company like Trolltech just makes perfect sense. Doing all of that stuff takes a lot of hard work, and KDE is just economically viable as I see it, plain and simple. It's not perfect, but fundamentally it is more than good enough to overcome any obstacles and to push on with new levels of innovation, thinking and technology. Failure of open source desktops == failure of KDE and nothing else.\n\nPeople then make what they will of those comments, which is usually a lot of the normal crap that makes little sense. You'd have though that after five years of consistent bollocks about KDE, and lies about what other people are doing (or not doing) you would have thought things would have changed - but no.\n\nIt's great to hear some coherent marketing happening for KDE, because basically, KDE isn't going to fall short of peoples' expectations. Most stuff actually works! It would be nice to throw some marketing material around for KDE, nice screenshots, cool technology, get ISVs who have been Windows-oriented interested (and considering the competition that's not hard) just to generally counteract 'other stuff'. Unfortunately, at times it is necessary to counteract a lot of this crap. In the run up to KDE 4 hype the damn thing to the hills, because you're not going to have to worry about it falling short."
    author: "Segedunum"
  - subject: "Re: Good to hear"
    date: 2005-11-08
    body: "Well said well said. Please join\n\n#kde-promo\n\nOn freenode.net. Would be cool to get more people helping here."
    author: "AC"
  - subject: "Re: Good to hear"
    date: 2005-11-09
    body: "There is a (not so) fine line between advocating and trolling.  Please stick to the advocating side (you mostly do so that's good)."
    author: "Leo S"
  - subject: "Re: Good to hear"
    date: 2005-11-09
    body: "One man's advocate is another's troll in things like this. I would just like an open source desktop that will work some time soon, and has the potential and framework to change, innovate and move things forward and does it in a way that's realistic."
    author: "Segedunum"
  - subject: "Re: Good to hear"
    date: 2005-11-10
    body: "Yep, just what KDE needs: More flamers invading websites and posting poorly written mentally unstable propaganda.\n\nHave you considered that KDE's long-held reputation for being stuffed full of hair-trigger idiots who resort to slander, libel and mindless flaming when anyone else fails to accept the \"One true way of KDE\" is actually damaging to the project? "
    author: "Ragnarok"
  - subject: "Re: Good to hear"
    date: 2005-11-10
    body: "Contradicting people while you know that they won't agree qualifies you as a troll, even if you mean to sound serious.\n\nI still can't understand why gnome people come to the dot *(the flamers, I mean, the rest are ok). Who in his serious mind goes to gnome sites to deride nautilus?\n"
    author: "sd"
  - subject: "Re: Good to hear"
    date: 2005-11-10
    body: "* Contradicting people while you know that they won't agree qualifies you as a troll, even if you mean to sound serious.\n\nMeaningless psychobabble.\n\n* I still can't understand why gnome people come to the dot *(the flamers, I mean, the rest are ok). Who in his serious mind goes to gnome sites to deride nautilus?\n\nKDE has a reputation (well deserved) for sending out its flying monkey hordes to attack anyone who disagrees. Most alarmingly of all, it's not just dimwit hangers-on either... a number of KDE developers have done it too. Just take a look at the recent Novell threads here (and on slashdot), and can you honestly tell me that the KDE project can be proud of the behaviour of its zealots? Now look back over recent history -- Red Hat, Richard Stallman, Sun, Bruce Perens, Ximian, and now Novell. The way KDE supporters behaved, you'd think those people and organizations were mass murderers... instead of just people who didn't agree/support the KDE project.\n"
    author: "Ragnarok"
  - subject: "Re: Good to hear"
    date: 2005-11-10
    body: "\" KDE has a reputation (well deserved) for sending out its flying monkey hordes to attack anyone who disagrees.\"\n\nMonkeys! Well that's funny. What do you think this is? And to say that Gnome has not had a centralised and project wide campaign of doing that, well, you need your eyes tested.\n\n\"Most alarmingly of all, it's not just dimwit hangers-on either... a number of KDE developers have done it too. Just take a look at the recent Novell threads here (and on slashdot), and can you honestly tell me that the KDE project can be proud of the behaviour of its zealots?\"\n\nKurt Pfeifle has turned out to be right so far. I simply think that an open source desktop needs something that is economically viable, and I've explained why. Some people obviously don't like it but that's a problem for them to deal with themselves.\n\n\"Now look back over recent history -- Red Hat, Richard Stallman, Sun, Bruce Perens, Ximian, and now Novell. The way KDE supporters behaved, you'd think those people and organizations were mass murderers... instead of just people who didn't agree/support the KDE project.\"\n\nRichard Stallman, Bruce Perens and Ximian in particular have not exactly behaved with the ethics that some in the open source community claim to cherish. They've stamped their feet, they wanted control of KDE when it first started and whinged about 'KDE's license' when they realised they couldn't, they've wanted their own way without giving rational reasons as to the decisions they're making (no, developing everything for nothing is not what an enterprise wants to hear Bruce) and they've made arguments about KDE consistently for years which KDE has not done the other way around.\n\nWhen was the last time you heard a KDE developer complain about Gnome and GTK's licensing, proclaimed KDE as a corporate desktop or asked Gnome people to cooperate with KDE through Freedesktop by adopting all of the stuff that KDE is adopting? When was the last time you saw Gnome developers back this 'interoperability' and 'brothers in arms' thing they keep coming up with by solid actions and raw code by coding something like QtGTK which integrates GTK apps into KDE, and could be done vice versa but no one on the Gnome side is interested?\n\nPlease don't try and take some sort of moral high ground here. You don't have it."
    author: "Segedunum"
  - subject: "Mr. Shifty Eyes"
    date: 2005-11-08
    body: "I've heard that Wade character has shifty eyes.  \nJ/K, Wade is an awesome guy.  I'm sure he'll take his ideas and move KDE into where it needs to be.  Congrats to ther others as well!\nLead KDE onto the desktops of millions."
    author: "Ryan"
  - subject: "Fantastic"
    date: 2005-11-09
    body: "Marketing and evangelism is what KDE needs most at this time."
    author: "Alex"
  - subject: "Re: Fantastic"
    date: 2005-11-09
    body: "It sure looks like KDE has some nice following - entire OpenSUSE thing seems to be somewhat dead. Hope SUSE does 'something' about it.."
    author: "Anon"
  - subject: "Re: Fantastic"
    date: 2005-11-10
    body: "Common sense and a calm rational approach to your image is what KDE needs at this time. \n\nLess of the \"hyperbole\" press release approach of the past, and more of the hardcore, quiet technical progress approach of GNOME would do you the world of good.\n"
    author: "Ragnarok"
  - subject: "OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "\"I have been the maintainer of the Suse kernel for more than a decade now,\" Mantel wrote. \"I'm very confident the Novell management will find a competent successor very quickly. After all, there are lots of extremely skilled people over there in the Ximian division.\""
    author: "?"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "It means SuSE is dead and buried."
    author: "reihal"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "This is quite sad. I was hoping it's not, but this seems to indicate that those warlords have finally hit the spot and done something bad.\n\nSo what's not dead and buried? Kubuntu? I wonder, I suspect I'd better plan some migrations :-("
    author: "?"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Hahah, where did you find that snarky comment from?  Basically it's a rather humourous potshot at Ximian from some high-ranking SUSE developer."
    author: "KDE User"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Yep. I got that, it's even one of the founders of SuSE. But I wonder why he's leaving SuSE and blaming Ximian for it. This looks like serious trouble, as in 'SUSE or whatever they call it is doomed'. "
    author: "?"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Isn't it obvious?  Ximian has been playing a dirty game of politics internally at Novell.  I thought this was already well-known.  A lot of people are not quiting over this."
    author: "KDE User"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Yes, but when key people who would be able to resist are quitting, it means that the Ximian guys are applying that old tactic of 'if you can't outsmart them, disgust them', so that finally they'll be the only ones left."
    author: "?"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "I meant \"now\" not \"not\" in my previous post.  I think the point is that people within SUSE do not see any hope.  Novell is a lost cause and a company very few take seriously these days.  Why should the SUSE people spend any more of their life there if they can do better?"
    author: "KDE User"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Makes sense. But SUSE 10 is so good, it's vastly better than all distros/releases I've seen.\n\nOf course, there's no point in continously waiting for the next bad/desperate decision of the Novell folks.\n\nSo my question was, what do I migrate to? :)"
    author: "ml"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-10
    body: "It's good because of KDE and all the KDE oriented people at Suse have have given it polish and a splash of colour."
    author: "Segedunum"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Novell == SCO for me. I treat them with the same respect from now on. Poor Novell suckers. I think Novell started killing off their own business. No money will ever stop the complaints coming from users and open source enthusiasts. The people in the world will from now on totally trash the name Novell. It will be set equal with the pigs from SCO, they will be treated the same way and their entire name will be worth nothing pretty soon."
    author: "ac"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-09
    body: "Not quite, my friend Winnetou, you see, SCO is a pig farm while Novell is just a bunch of geese (how do you call many  noisy geese looking for a way out in English? Is it a flock?)\n"
    author: "ml"
  - subject: "Really OT: Collective noun for geese"
    date: 2005-11-09
    body: "Believe it or not, the correct term is a gaggle of geese.\n\nNo, really."
    author: "Ralph Jenkin"
  - subject: "Re: Really OT: Collective noun for geese"
    date: 2005-11-09
    body: "After a gaggle of googling I believe you.\n"
    author: "ml"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-10
    body: "* Novell == SCO for me.\n\nWell, then it's good news that no-one cares what you think, outside of the small collection of flamers and irrational idiots on dot.kde.org, of course.\n\n*The people in the world will from now on totally trash the name Novell. \n\nAhem, don't you mean \"you\" will totally trash the name Novell.\n\n* It will be set equal with the pigs from SCO, they will be treated the same way and their entire name will be worth nothing pretty soon.\n\nThe infidels will be punished! Death to Novell! All you need now is a backpack full of explosives and a train ticket to Novell HQ. Get a grip will you."
    author: "Ragnarok"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-11
    body: "> Well, then it's good news that no-one cares what you think\n\nWhy did you reply then ?\n\n> outside of the small collection of flamers and irrational\n> idiots on dot.kde.org, of course.\n\nWelcome in the club then."
    author: "ac"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-13
    body: "\"Novell == SCO for me.\"\n\nIt's rather Novell == (R.I.P.) Corel. After their faux pas with switching to Gnome, I have no doubt now that Novell will end up like Corel. Similar incompetence and lack of focus of their decision makers. Corel went ahead against MS, Novell has decided to go against Red Hat and Sun.  Also, both received (settlement) money from MS ;-) "
    author: "AC"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-10
    body: "Chris Schlaeger seems to have left too. Ok, so I've made up my mind. This is a dead dog."
    author: "sd"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-10
    body: "When Novell bought Suse and the MP3 support was dropped (what simply means the distribution was crippled) I was about to switch to another distro. I've tried Gentoo (my notebook disk was too small and the CPU too slow) as well as Knoppix and Debian. Only because our company has been using Suse for years, I wanted to avoid double effort and eventually stayed. OpenSuse 10 was a positive surprise. But now, that the Suse people leave Novell, I'll no longer bet on 10.1 or whatever comes.\nIMHO the statement that Gnome is more user friendly and thus is better suited for business distros is complete nonsense. Suse managed to tweak the KDE in their distro quite well in the past. It wouldn't be that huge effort to spend a new profile to Konqueror to let it show less buttons in the toolbars. You can provide a different set of rc-files for other applications (say KMail) as well. You can use Kiosk...\nAnyway, bye bye Novell (and to be honest, I expect my co-workers to drop Suse as well).\n"
    author: "Andi"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-10
    body: "It could mean that openSuSe will be forked into a KDE only distribution with the name K Linux. \nK for KDE and Kameleon.\n"
    author: "reihal"
  - subject: "Re: OT: And what is this supposed to mean?"
    date: 2005-11-10
    body: "Not only is Hubert Mantel leaving, but Chris Schlaeger has left as well. It's pretty obvious that Ximian is taking over Novell for better or (rather) worse. My only hope is that those leaving start a new more sane business, pick up the opensuse source or something and show Novell how it should be done."
    author: "ac"
  - subject: "mailing list"
    date: 2005-11-09
    body: "A reply from kde-promo-bounces.org means my mail has been bounced? (pls excuse the stupidity of the question)\n"
    author: "ml"
  - subject: "A couple of marketing points"
    date: 2007-09-16
    body: "I submitted these points before, but probably somewhere in the technical formats, where they don't take as much notice of something that is not strictly technical.\n\n(1) KDE4 beta has a black screen, this may be cool to the technical people, but it is a turn off to the regular user.  Tell them to engage some professional people about the psychological effect of colours.  Black has an unconscious depressing effect.  Ubuntu is popular not only because of its easy functionality but because of it's colour.  Not everyone likes the colours consciously, but unconsciously these Ubuntu colours (gold theme) elevate your mood. KDE needs to rethink introducing a product with a black default colour scheme.  If they want to attract users, introduce it with a \"feel good\" default colour scheme.  I am a KDE fan, and the first time I saw this black I went Yuck!!!  On the live CD I wasn't able to change this, so I simply haven't used it. \n\n(2) The KDE Office Suite is always presented, on the KDE web pages and the Wikipedias minus any e-mail / PIM component.  Right away anyone who is looking for a serious Office Suite, that may be a professional or even an enterprise contender, won't give it a second look.  Because I know that KDE has the Kontact product, but the casual observer does not.  So why don't you include Kontact with all information about KOFFICE?  Just because the developers may be two separate teams, it does not mean that the products have to be separately presented to the public.   "
    author: "Tom Mclernon"
  - subject: "Two for the price of one..."
    date: 2007-11-26
    body: "I am new to Linux and really like both KDE and GNOME desktops, a painful dilema. I would like to see either a merger or a process developed by both desktop makers to create a dual plugin for people like me. \n\nI would like to have each manager on the boot strap so that I could boot into the desktop I want to use for the specific task at hand. I have been trying to find a solution in Linux-land. I am sure there are others like me and there must be some \"work-around.\" So far no luck... I might have to try and \"make\" it myself."
    author: "Mike"
---
The KDE Marketing Working Group has formed, after being proposed by the KDE community at <a href="http://conference2005.kde.org/">aKademy 2005</a>, with the aim of improving KDE's marketing and promotion efforts. Martijn Klingens, Sebastian Kügler and Wade Olson will be taking the lead in coordinating and implementing new practices, such as promoting releases more widely and running more exciting events booths. An initial charter has been created and approved by <a href="http://ev.kde.org/">KDE e.V</a> with the long-term goal of "coherent and strategic messaging around KDE".   The group is currently focused on establishing infrastructure and prioritizing tasks.


<!--break-->
<p>
The Marketing Working Group plans to be an extension of the various existing community processes in place, such as the Dot Editors, the kde-promo mailing list and media outlets. The Working Group is not a replacement for any of the existing systems, but rather fills the missing gap of coordination by providing a single entry point for anyone who has marketing and promotion questions and ideas.  A participative approach is planned for this Working Group; the goal is to support KDE developers and contributors who request assistance or advice on marketing needs.  Promoting, planning, branding and researching competitive products are just some of the services to be offered by the Marketing Working Group.
<p>
Those that are interested in participating in KDE's maturing marketing process, have general questions, or would like help with their project are encouraged to contact the Marketing Working group through the <a href="https://mail.kde.org/mailman/listinfo/kde-promo">kde-promo</a> mailing list.


