---
title: "New Qt3/Windows Free Edition and KDE/Cygwin 3.4.1 Snapshots Available"
date:    2005-06-15
authors:
  - "binner"
slug:    new-qt3windows-free-edition-and-kdecygwin-341-snapshots-available
comments:
  - subject: "difference to trolltech versions?"
    date: 2005-06-15
    body: "Hi,\n\n  what's the difference between these and the Trolltech versions of Qt?\nCan I use these builds to get Linux Qt projects compiled and executed under Win?\nOr is it kind of beta software?\nAre these versions GPL licensed?"
    author: "katakombi"
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-15
    body: "This is based on Qt/X11. The goal, AFAIK, is to provide an implementation fully compatible with the Qt/Win32 commercial version, only GPL. Right now I think would be considered still in beta state, but I'm not sure."
    author: "mmebane"
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-16
    body: "> Right now I think would be considered still in beta state\n\nWhy? I heard it works rather well already."
    author: "Anonymous"
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-16
    body: "Well, according to their roadmap, there's still several things not done yet.\n\nMaybe they just haven't updated the roadmap recently, I don't know. *shrugs* "
    author: "mmebane"
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-16
    body: "What do you read at which point in the roadmap they are? The frontpage says \"We are looking for developers to finish milestone 4 of the Roadmap\" and milestone 4 in the roadmap is \"Stage 7 - start native kde port\"."
    author: "Anonymous"
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-16
    body: "I am looking at all the TODOs in Milestone 3."
    author: "mmebane"
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-23
    body: "Lots of beta software works well, but still isn't anywhere near done. I wrote a QT app a while back and tried compiling it for Windows. Turns out QScrollviews were broken at the time. There's probably other stuff in there that is broken as well, and in something like Qt you can't really call it done until everything works.\n\nNot to knock the authors, I was shocked and amazed to see my app compile and run with no effort at all. It's an amazing accomplishment in itself to make as much progress as they have made. "
    author: "Greg "
  - subject: "Re: difference to trolltech versions?"
    date: 2005-06-15
    body: "They're all GPL. It's pretty much like anything under cygwin, most things work but it underperforms some of the time and it isn't so reliable. So I'd say beta, yes. The cygwin one is just the ordinary qt recompiled for cygwin, wheras the native port is a port of Qt back to windows so we have a gpled windows version, and IIRC is a bit kludgy and missing things. (but performance will be perfectly native)."
    author: "mikeyd"
  - subject: "stealing away trolltechs money?"
    date: 2005-06-15
    body: "Doesnt trolltech make money selling the windows version? is it me or is this like abusing the licenses.. yes i know its all legal but still"
    author: "hello"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-15
    body: "Trolltech is going to release a GPLed version of QT for Windows for QT4.  Also it is still licensed under the GPL so it works exactly the same as every other desktop (GPLed software can use the GPL version, everything else and you need a QT license).\n\nTrolltech makes money selling /all/ the versions, they just also give it away under the GPL on all platforms (had been all but windows, then they stopped for a while, and with QT4 they are going to do it for all platforms the same again)."
    author: "Corbin"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-15
    body: "thanks for explaining thats great news,  on the other hand..  when trolltech releases qt for windows in gpl that will make the project what this topic is about redundant and useless. right? \n\nbeside this i dont understand who would buy qt when its available for free.."
    author: "hello"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-15
    body: "You have it only for free if you put yur Source under the GPL. For non-free applications you have to buy it.\n\nNikN\n"
    author: "NikN"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-25
    body: "Not quite; you may license your code under a GPL-compatible license (e.g. LGPL, BSD-no-ad-clause, etc.) and then link it against Qt/GPL."
    author: "rqosa"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-15
    body: "> beside this i dont understand who would buy qt when its available for free..\n\nhttp://www.trolltech.com/developer/faqs/license_gpl.html?cid=20#q13\n\nQuote:\n\nQ: Why do I need to buy a commercial edition when I can get it for free?\n\nA: If you want to develop Open Source Software, you are welcome to use our Qt Open Source Edition. If you don't want to develop Open Source Software (for example to keep your source code secret or to produce commercial software), you must purchase a commercial edition of Qt. "
    author: "Christian Loose"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "Their FAQ is actually wrong on that point, and bad on a few others. If I keep my modification to myself (private or \"secret\") the GPL does *NOT* require me to release that source under the GPL. I only have to do that if I distribute or \"release\" it. See:\n\n[[ http://www.gnu.org/licenses/gpl-faq.html#GPLRequireSourcePostedPublic\nThe GPL does not require you to release your modified version. You are free to make modifications and use them privately, without ever releasing them. This applies to organizations (including companies), too; an organization can make a modified version and use it internally without ever releasing it outside the organization.\n]] \n"
    author: "Joseph Reagle"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "Maybe a bit misleading, but I wouldn't already state it as wrong.\n\nNormally if you develop something you will later distribute it, just because the developer is normally not the user.\nI would say in >99% of the development of closed source software this is the case on for these the FAQ is correct.\n\nSo what is the exact definition of a developer, developing software where he/she doesn't want to distribute the software to anybody and still keep the code secret?\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "Within the confines of an organization, the developer/user distinction is not as it is outside of an organization.\n<p>\nThis confusion goes round and round. For example, see this from nearly 4 years ago:\n<p>\n[[ <a href=\"http://dot.kde.org/1021838067/1021864614/1021872882/1021903290/1021905986/1021916367/1021916900/\">link</a>\nA lot of Trolltech's clients use QT for in-house development (software used only whithin the company) so they could use the hypothetical GPL'd QT for Windows and not release the source.\n]]\n"
    author: "Joseph Reagle"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "Huh, developer/user distinction?  You're developing software so that someone can *use* it.  Once you give that someone the software, you *have* to provide them with the source if the GPL is enforced.  And as soon as the user gets the software he can distribute it beyond your control.  It doesn't matter of the user is internal or not.  That user gets GPL software and can use it as GPL software.\n\nSo your company has no control over what its internal users do with the software.  They can distribute it to the outside world because it's GPL.  They can use the code in other GPL products of their own.\n\nHopefully I've managed to clear your confusion."
    author: "ac"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "> Hopefully I've managed to clear your confusion.\n\nNo, actually you added to it.\n\nYour opinion clearly contradicts the view of the Free Software Foundation: \nhttp://www.gnu.org/licenses/gpl-faq.html#GPLRequireSourcePostedPublic\n\nThey do make a distinction between *using* it inside an organisation and *distributing* it to the public.  \n\nGiving modified software to an employee of your own organisation does *not* count as distribution.  He does not receive the changes under the terms of the GPL and thus *cannot* redistribute it legally to someone outside the organisation.\n\nNote:  Modified source in the GPL FAQ means what is called a derived work in other places, so what is said in the FAQ applies to software written with the GPLed Qt. \n\n"
    author: "cm"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "Just to make it a little clearer:  \n\nFor an employee to use a software legally it's the *organisation* who has to have a valid license to the software, not the employee personally. \n\nSo the employee does not need to receive a license (here it's the GPL) to the code in order to use it legally. \n\nSince the employee does not have a license (i.e. has not received the modified software under the terms of the GPL) he cannot legally redistribute it to someone outside the organisation. \n\n"
    author: "cm"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "Who said that you have to use the GPL for your internal software in the company?"
    author: "ac"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "Then anyone you distribute the program to within your organization gains the GPL rights to distribute the program and source.  A company who wants to keep their code secret and conform to the GPL clearly can't do it."
    author: "ac"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "I'm not sure what part of the English text in the GPL and their FAQ you don't understand, but simply stating something about something that is contrary to the explicit text doesn't do it for me. A person acting as an agent on behalf of a company is different than an ordinary individual. In any case, even if I had to give a proprietary tweak I made on behalf of my company to another employee within that company, we can both be bound by contract to that company not release it publicly."
    author: "Joseph Reagle"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: ">we can both be bound by contract to that company not release it publicly.\nActually no, the GPL explicit disallows placing additional restrictions upon the GPL'ed material. Trying to apply a contract like that to GPL'ed material would break the GPL."
    author: "Morty"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "You misunderstood the secret part. It doesn't mean keeping the modification to yourself. It means distributing your application without source."
    author: "ac"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "> A: If you want to develop Open Source Software, you are\n> welcome to use our Qt Open Source Edition. If you don't\n> want to develop Open Source Software (for example to keep\n> your source code secret or to produce commercial\n> software), you must purchase a commercial edition of Qt.\n\nThis answer also seems wrong since your not allowed to use a GPL incompatible license that is \"open source\". I think this can be boiled down to the old \"open source\" vs \"free software\" naming issue.\n\nI think if Stallman had forseen all these naming issues around \"free software\", he'd've chosen another name."
    author: "cies breijs"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "> This answer also seems wrong since your not allowed to use\n> a GPL incompatible license that is \"open source\".\n\nIncorrect. Qt is released under GPL, QPL and commercial licenses. You use GPL for GPL programs, you use QPL for all other Open Source programs (like BSD, Apache, whatever), and commercial for closed source."
    author: "KOffice Fan"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "Stop repeating that lie.\n\nQt is GPL means you can license your code under LGPL, BSD (without ad clause) just fine.  You don't need QPL for that.  QPL is obsolete."
    author: "ac"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "So, which version of Qt do you use if you want to use BSD with the ad clause?\nOr if you want to use these : http://www.opensource.org/licenses/\n\nPlease stop pushing the belief GPL is the only thing that counts. Since QPL very much has a place.\n "
    author: "Morty"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "The revised BSD license is GPL compatible, so you can very well use GPL Qt with your BSD code.\n\nCheckout this license for other GPL compatible licenses:\nhttp://www.gnu.org/philosophy/license-list.html#GPLCompatibleLicenses"
    author: "blacksheep"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "s/this license/this list/"
    author: "blacksheep"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-17
    body: "> (for example to keep your source code secret or to produce\n> commercial software)\n\nQt (and MySQL for that matter) should stop using the term commercial software for proprietary software. You can very well use free software commercially. Two examples:\n1. Linux distributions: I use Suse that I bought from the store.\n2. Siteseed is a free software web development platform that is used by a Portuguese company to deploy websites for their clients with the source completely free.\n\nMore info:\nhttp://www.gnu.org/philosophy/words-to-avoid.html#Commercial"
    author: "blacksheep"
  - subject: "Re: stealing away trolltechs money?"
    date: 2005-06-16
    body: "> when trolltech releases qt for windows in gpl that will make the project what this topic is about redundant and useless.\n\nTrolltech will only release Qt 4 under GPL but no Qt 3.3. All Qt 3 based stuff would have to be ported to Qt 4 without this project before being possible available under Win32, and if KDE 3 based it will take well over a year to have KDE 4 ported/released before you can start. So working on Qt3/WFE and KDE3 based ports makes sense short-term."
    author: "Anonymous"
---
Two distinct efforts with the goal of making KDE applications run on MS Windows are progressing: The <a href="http://kde-cygwin.sourceforge.net/qt3-win32/">Qt 3/Windows Free Edition</a> project <a href="http://sourceforge.net/forum/forum.php?forum_id=471148">announced a binary release</a> of their native Win32 port of the GPL'ed Qt 3.3.4/X11 sources. And the <a href="http://kde-cygwin.sourceforge.net/">KDE on Cygwin</a> staff released a <a href="http://sourceforge.net/forum/forum.php?forum_id=473247">first snapshot of KDE 3.4.1</a> for use with the Cygwin POSIX emulation layer and the Cygwin XFree86 server.




<!--break-->
