---
title: "KDE 3.5 Beta 1 \"Kanzler\" Released"
date:    2005-09-22
authors:
  - "jriddell"
slug:    kde-35-beta-1-kanzler-released
comments:
  - subject: "kmail will rock"
    date: 2005-09-22
    body: "Can't wait for the asyn filters in kmail"
    author: "eze"
  - subject: "Re: kmail will rock"
    date: 2005-09-23
    body: "kmail used to rock more for me. I have a long list of folders for my professional correspondence. It used to be I could press m for move. Then the list of folders came up and when I pressed say p it would take me to the first folder starting with p. A very nice feature that for some strange reason has been removed.\n\nWhen I get a letter from a new person I make a new folder. That used to be easy, but now the new folder becomes a subfolder of the inbox. I have to leave the letter and go up to the top folder before I make the new folder, and try to remember the name while I do that. I realize the new way may be more logical, but the old way worked a lot better for me.\n\nI also have an intense dislike for the decision to move the mail directory to be a subdirectory of .kde I help some 40-50 people with their kde-desktop and I have done so for several years. Every so often we solve problems by removing the .kde directory to get a fresh start. It is just too complicated to try to look through all the config files. Now we of course have to be extremely careful with that since important data have been moved into the .kde directory. Lots of people combine the usage of kmail with say mutt, logging in to the mail with mutt when they travel, and using kmail when they are at home. That has been made less convenient. So I do not think it is all progress what is happening."
    author: "Erik K. Pedersen"
  - subject: "Re: kmail will rock"
    date: 2005-09-24
    body: "Well I don't know about the first two paragraphs, but I can't understand moving the folder into .kde either. Like you I have blasted mine out, or recommended to others to delete it when apps are acting up.\n\nYou can however change the location. Just open up ~/.kde/share/config/kmailrc and find the [General] section. Add the following line ->\n\nfolders=/home/chavo/.Mail\n\nChange the path of course to wherever you want. Then just put your mail folders in there and you're good to go.\n"
    author: "Andrew Rockwell"
  - subject: "Re: kmail will rock"
    date: 2005-09-24
    body: "Deleting .kde can be simply disastrous even if you don't have all your mail there. There are loads of other important data stored there, such as your calendar and contacts (by default, at least). If you really *have* to delete something, you might consider deleting only ~/.kde/config, but that is generally a bad idea too..."
    author: "Henrique Pinto"
  - subject: "Kanzler"
    date: 2005-09-22
    body: "Well this is a nice codename ...\n--tk from germany :)"
    author: "no name"
  - subject: "Re: Kanzler"
    date: 2005-09-22
    body: "Sorta political :)"
    author: "Sam Weber"
  - subject: "Re: Kanzler"
    date: 2005-09-22
    body: "and Beta 2 or the release might be called Kanzlerin. We'll find out..."
    author: "burki"
  - subject: "Re: Kanzler"
    date: 2005-09-22
    body: "How about Kaos?"
    author: "kujhg"
  - subject: "Re: Kanzler"
    date: 2005-09-22
    body: "KStoiber anyone?"
    author: "Matt T. Proud"
  - subject: "Krokodile"
    date: 2005-09-22
    body: "Remember KDE 3.4 beta 1: Krokodile?\n\nThe KDE code names -- your best source on current events in Germany."
    author: "Martin"
  - subject: "Update on the schedule ?"
    date: 2005-09-22
    body: "First things first: THANK YOU to all people involved in this release :-)\n\nA question: has a decision been made on when to target for the release of 3.5 final ? The release schedule still shows an \"undecided\" status ...\n\nThanks !\n-- MU"
    author: "MandrakeUser"
  - subject: "klik://kde-3.5-beta.cmg (i want that!)"
    date: 2005-09-22
    body: "From how I unserstand the recent hype around klik, the following plan should be possible to implement:\n\n1. Compile kdelibs+kdebase+kde[something].\n2. Get yourself a fairly recent xorg X server.\n3. Modify the standard klik wrapper script so it ...\n . . a) ...starts Xnest/(or, maybe nxagent, the resize-able and fullscreen-able Xnest)\n . . b) ...starts the KDE desktop inside the Xnest\n4. Pack everything into a .cmg file.\n5. Call it a deal and tell the world you have now an easy way to demo KDE-3.5 key features to them.\n\nAnybody already working on this?\n\nCheers,\nA coward"
    author: "ac"
  - subject: "Re: klik://kde-3.5-beta.cmg (i want that!)"
    date: 2005-09-22
    body: "you dont know how big this thing would be !!!\n\n\nch"
    author: "chris"
  - subject: "Re: klik://kde-3.5-beta.cmg (i want that!)"
    date: 2005-09-22
    body: "But I know.\n\nI tell you: it is do-able. \n\nAnd it will be able to run on any hardware where you can run a current KDE, and still have some 100 MByte virtual memory free, outside the swap area.\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: klik://kde-3.5-beta.cmg (i want that!)"
    date: 2005-09-22
    body: "Even more easy,\n\nWith NX technology desktop sharing will get real good soon.  You'll just be able to connect to a remote server running the latest and greatest."
    author: "Bobke"
  - subject: "Re: klik://kde-3.5-beta.cmg (i want that!)"
    date: 2005-09-22
    body: "Ah, good to hear about NX.\n\nAre you suggesting I should give up with the klik-able KDE-3.5 and instead setup an NX server with 3.5 running, and accommodate a user account on it for you?"
    author: "Kurt Pfeifle"
  - subject: "Re: klik://kde-3.5-beta.cmg (i want that!)"
    date: 2005-09-22
    body: "### \"(...) following plan should be possible (...)\"\n----\nYes, it is.\n\n.\n### \"Anybody already working on this?\"\n----\nYes."
    author: "Kurt Pfeifle"
  - subject: "Yeah!"
    date: 2005-09-22
    body: "Installed without any trouble and Just Works[tm]!\nUbuntu and KDE rock!"
    author: "rik"
  - subject: "Re: Yeah!"
    date: 2005-09-22
    body: "Damn straight.  No problems, just a straight, apt-get update, apt-get upgrade.\n"
    author: "Matt"
  - subject: "Re: Yeah!"
    date: 2005-09-22
    body: "Having some libc6-issues here holding back some packages...\n\nThe following packages have unmet dependencies:\n  amor: Depends: libc6 (>= 2.3.4-1) but 2.3.2.ds1-20ubuntu14 is to be installed\n\nI know it's OT, but any idea?"
    author: "Ineiti"
  - subject: "Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "Hi,\n\nafter I upgraded to KDE 3.4 (from IIRC 3.2) I lost the feature to automatically reload websites periodically (in the \"Extras\" menu). I use SUSE9.1 and all other menus in the Extras menu seem to be still available.\n\nHas this feature been user-friendlisized (removed by self-proclaimed usability experts) or is it still there (in 3.5)? If yes, in what package?\n\nThanks a lot for clearing this up.\n\n\n"
    author: "Roland"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "This is a plugin now. Can be (re-)activated in the settings."
    author: "Joachim Werner"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in"
    date: 2005-09-22
    body: "self-proclaimed usability experts is ok for me , as long as you dont mean it negativly. Kde as a whole is made by so called self proclaimed coders.\n\nIf someone is thinking about usability and remove a feature , its ok for me as long as he has thought about it. \n\nIf i like it or not is another matter. Dont think your way to use Software ist the best. You figure out anotherway oder just do the things different....\n\n\nmy opinion: I hate it if someone thinks bad of usability people just because he thinks his way of computerusage is the ultimate best way.....\nSo keep removing Things people use, only do it in a consistent and logical way.\n\nchris"
    author: "chris"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in"
    date: 2005-09-22
    body: "Self proclaimed coders can be seen to be coding, if they can't code their patches don't work and are rejected. Self proclaimed usability experts can make it less usable and get away with it.\n\"If someone is thinking about usability and remove a feature , its ok for me as long as he has thought about it.\"\nI don't think we should ever remove features unless no-one wants them. Having less options doesn't make the software more usable. Let gnome have the simple-is-better people, KDE can have all the features you ever need, that way there's sense to having the two environments with people choosing between them.\n\"If i like it or not is another matter. Dont think your way to use Software ist the best. You figure out anotherway oder just do the things different....\"\nSoftware should serve me, not the other way around. I'll get it doing what I want if I have to patch the source myself.\n\"my opinion: I hate it if someone thinks bad of usability people just because he thinks his way of computerusage is the ultimate best way.....\"\nI think badly of usability people because all they have done is made my programs less usable.\n\"So keep removing Things people use, only do it in a consistent and logical way.\"\nNo. If I wanted someone who thought they knew better than me what's usable taking away features I use, I'd be using gnome."
    author: "mikeyd"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in"
    date: 2005-09-22
    body: "I see 2 big problems with \"usability experts\":\n\n1) They talk a lot about the \"average user\", but they actually mean some (often imaginary) below-average user. They say when the so called \"average user\" doesn't need it, it's worthless. This way is stopping all innovation.\n\nMost usability-features, that set Unix apart (multiple desktops, a third mouse-button that is acutally useful, Unix-style copy/paste, etc.) all were made before the \"usability experts\" declared all non-novice features worthless.\nAnd if you look at tabbed browsing: It was \"usability experts\" who said all the time that MDI is \"just wrong\" and for a long time they also spoke against tabbed browing.\n\n2) They think \"usability studies\" that put complete beginners for half an hour before a computer are the only metric that is to be used. However the average user in the real world doesn't use a computer for half an hour, he will use it for YEARS, probably DECADES.\n\nAccording to usability tests all keyboard-shortcuts are worthless, because in the first half hour of use you won't use them. Keyboard-shortcuts come from a time where there were no usabilty experts, I doubt they would have a chance to get included today.\n\n\n\nThere are so many OBVIOUS ways to improve usability.\n\nFor example most mice have more than just 3 buttons+wheel. Wouldn't it be great to use the 4th mouse button for something useful? For example as another \"Alt\"-key? That way you could grab a window anywhere by pressing LMB+4thMB or resizing near the edges with RMB+4thMB.\n\nWell, that behaviour isn't in MacOS, it isn't in Windows and beginners don't crave for it in the first half hour of computer use - so forget it, it simply won't happen.\n\nAnother example: We all have \"Windows\"-keys on our keyboards, why not use it for some more than just popping up the K-menu?\nWouldn't it be great to have standardized shortcuts for \"maximize\", \"minimize\", \"next desktop\", etc.?\nUsing one of the most accessable keys for just one function is a waste in my opinion, so I use Windows+left/right to switch between desktops and Windows+up/down to switch between applications on a desktop. (What is currently Alt+Tab and Alt+Shift+Tab)\nIsn't it obvious that Alt+Shift+Tab is bad usability and something better should be introduced? Yes, keep Alt+Tab/Alt+Shift+Tab for compatibility, but there must be a better solution than that. And because applications use Alt-shortcuts and Ctrl-Shortcuts, wouldn't it be OBVIOUS to use Windows-key for everthing about the window-manager?\nAnd what about Alt+F4? Wouldn't be Windows+Esc be more logical and (gasp) intuitive?\nYes, I know I can set all this in kcontrol (and I did), but we all know that a cross-project standard about this would be just great.\n\nBut again: It's not in Windows, it's not in MacOS, beginners are unlikely to use it in their first half hour in computer use, so it ain't gonna happen.\n\nThat is EXACTLY what usability experts should do: Organize keyboard shortcuts, create standards (preferrably together with GNOME), use hardware that is available and currently unused, etc.\nReorganizing kcontrol - AGAIN - is just a waste of everybodies time.\n\n\n\n\n\n\n"
    author: "Roland"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in"
    date: 2005-09-22
    body: "KControl was a mess, now its fine. \n\nUsability discussions make al lot of sense.\n\nIt is not only newbie simplification, it is also about unification. E.g. always use the same keyboard shortcut for the wanted action."
    author: "gerd"
  - subject: "Start us up the clue-bat  ;-)"
    date: 2005-09-23
    body: "This is actually quite contrary to the way that usability design actually works.\n\n\"They talk a lot about the 'average user', but they actually mean some (often imaginary) below-average user.\"\n\nThe standard mechanism for user modeling is called \"personas\".  Personas can represent a non-technical user or a power user.  For a given persona you try to map out the tasks that you expect from such a user and then proceed from there.  See Tina's blog for more:\n\nhttp://tina-t.blogspot.com/2005/06/personas-part-1.html\nhttp://tina-t.blogspot.com/2005/06/personas-part-2.html\n\n\"They think 'usability studies' that put complete beginners for half an hour before a computer are the only metric that is to be used.\"\n\nAgain, really wrong.  Please look around and read some usability tests.  They almost always describe the type of users that they're testing and why.  It's usually not (or at least not exclusively) complete beginners.\n\n\"According to usability tests all keyboard-shortcuts are worthless [...]\"\n\nI bet you can't find a single usability test that suggests that.\n\nReally, you're not impressing people here by ranting about things which you don't even have the most basic clue about."
    author: "Scott Wheeler"
  - subject: "Re: Start us up the clue-bat  ;-)"
    date: 2005-09-23
    body: "\"Personas can represent a non-technical user or a power user.\"\n\nAnd that's completely nonsense because people have different levels of experience in different areas of computing.\n\nExample: Me.\n\nI'm a programmer, I have no problems administrating Unix-machines, but I use a word-processor maybe once a month and I have never done anything more than use \"bold\"/\"italic\"/\"underline\" and the font size in a word processor. I have absolutely no idea how to write chain letters, how to add footnotes or anything else in wordprocessing.\n\nSo what am I? Am I a power-user or a newbie?\n\nAnd you know what? I don't care about hundreds over hundreds of settings in a wordprocessor [b]BECAUSE I DON'T SEE THEM ANYWAY[/b]. And it's the same vice-versa: Newbies don't care about \"too many settings\" because they just use the defaults.\n\n"
    author: "Roland"
  - subject: "Re: Start us up the clue-bat  ;-)"
    date: 2005-09-23
    body: "Look, read the links.  You'll see that the personas aren't labeled \"power user\" or \"newbie\" they're labeled \"John Smith\" or \"Bob Villa\" or whatever.  They're meant to be constructions of semi-real people.  Your argument doesn't make any sense in that context.  And the bit about settings just completely indicates not really even understanding what usability is.  Hint:  it's not an alias for \"those guys that want to remove everything\"."
    author: "Scott Wheeler"
  - subject: "Re: Start us up the clue-bat  ;-)"
    date: 2005-09-23
    body: "Well, yeah, it's just that i don't see much improvement happening. Usability-improved menu-on-top is a kind of nightmare to handle now (since you can't really configure it without text editor anymore). IIRC kmail had \"check mail for this account\" in account context menu -- and it's gone. The context menu has 3 items. It's not like it was overcrowded or anything. And it kept me swearing while i was using kmail.\n\nToolbars in KDE need cleaning up badly for ages now. But still we don't have an usable toolbar setup as default (or do we? i have been setting up custom toolbars for so long now...). We still fiddle with kcontrol though. But for one, i spend virtually no time in kcontrol. I do spend time in konqueror though. And the toolbars and menus are generally just too big to fit into my little brain. And, uh, well, the less trained personae have _much_ more trouble finding anything than i have.\n\nAs for usability experts, i am yet to see any significant contribution. Kmail only got worse for me (actually, i just use mutt now). As i said, i don't really spend time in kcontrol, and when i do, it's because someone shuffled things around. I still don't use most of the default toolbars and reconfigure them in nearly every app. The default shortcuts are a bit silly, not using win-key as a modifier. The tradeoff seems to be getting users from windows retrained quickly, but incurring the same crappy scheme on complete newbies. The \"kde default for 4 modifiers\" makes just so much more sense. Three-level k-menu is on the edge of bearability -- it's constant reshuffling doesn't help either (good i can just use mini-cli...). On the improvements front, i didn't really notice anything worth mentioning.\n\nYeah, there is a problem with actually getting some feedback from those usability experts. From the projects on openusability, very few actually have some reports. And those that are there are pretty limited in scope.\n\nIn short, usability salvation is not coming anytime soon. And it's still mostly coders' job to get the interface as right as possible."
    author: "mornfall"
  - subject: "Re: Start us up the clue-bat  ;-)"
    date: 2005-09-23
    body: "Most of this I can go along with -- our usability effort as any sort of organized affair is still in its infancy.\n\nOne thing that is worth picking apart a little bit is the comment on the big problems.  As a programmer you can't start in the KDE project and start refactoring major library components.  You usually start with a 5 line patch somewhere that does something you thought was pretty cool at the time, but in the grand scheme of things is pretty irrelevant.\n\nIt's no big surprise that our usability folks start in more or less the same way.  We're just now starting to have a few people respected enough that they can actually question some of the bigger issues in KDE and have someone listen.\n\nOpenUsability naturally isn't a silver bullet that will solve all of our problems, but when you consider that it's mostly about half a dozen people they've had a significant impact.  If we support them they may grow into a larger group that can tackle larger percentages of KDE.\n\nOn KMail, well, I have to say that the improvements were subtle, but I feel in the right direction.  The folder properties dialogs were simplified, the filter dialog was split into a main and advanced tab and well, I don't remember other things off the top of my head, but well, good things.  :-)\n\nIn a nutshell I think it's important to be supportive of the stuff going on so that it will ideally grow into something that is on the whole more significant to KDE."
    author: "Scott Wheeler"
  - subject: "You're ranting"
    date: 2005-09-25
    body: "Sorry but your rant is not very interesting: usability studies usually defines the  expertise of the user they are targetting. Not all studies are for beginners.\nI bet that Adobe makes usability studies for photoshop, for example.\n\nAs for the transition from tabbed browsing, if you happen to stop ranting and look, you'll notice that what people were criticizing is MDI which is different from tabbed browsing: opera offers full MDI (with icons inside the window) but I only used it in \"tabbed mode\", konqueror and Mozilla only provides tabs and I never heard someone asking for full MDI..\n\nAlso IMHO, tabbed browsing is still immature, as for example when one tabs generate a popup on mozilla, it blocks me even though I may be looking at another tab.. It should be less intrusive, for example sound a beep, makes the tab flash two times and then stay red: now I know that something is asking for my attention on this tab, and *I* decide when I'll want to deal with it.\n"
    author: "renox"
  - subject: "About the windows key"
    date: 2005-09-25
    body: "> wouldn't it be OBVIOUS to use Windows-key for everthing about the window-manager?\nAgreed (and about the mouse button too).\nNote that in fact it is already possible to have this in a limitated way: I've configured my RHE3 to iconify all the windows with the win+D setup, unfortunately it only works for the current workspace, which is annoying when you have a window which is displayed on all the workspaces, and I've no idea how to change this behaviour, to iconify all the windows of all the workspaces..\n\n\n\n"
    author: "renox"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "It was always a plugin (see http://dot.kde.org/1054666264/1054718743/), what has changed is that now users have the option to choose which plugins are displayed.\n\n"
    author: "Richard Moore"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "I just checked and at least in my installation I can't find it in the \"plugins\" section of the settings. I also couldn't find it in other sections and in kcontrol. Where are these settings?\n\nThanks a lot"
    author: "Roland"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "Have you been looking at the right place? It's not \"Plugins\" in \"Configure Konqueror...\", it's in the Menu \"Settings/Configure Extensions\". Then choose the tab \"Tools\" and activate \"Auto Refresh Plugin\". I've sent you the German instructions via mail, so I'm a bit surprised that didn't help.\n\n@Richard: Yes, technically it was a plugin, but from a usability point of view I'd only call it a plugin if it can be configured."
    author: "Joachim Werenr"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "Thanks a lot, it's right in there!\n\n(BTW this is another example why we should have all settings in one place and never ever introduce \"advanced settings\" or even (shudder) a registry-clone)\n\n"
    author: "Roland"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "Isn't it the windows registry that keeps settings together nicely in one place in consistent and hierachical format? And regedit is not meant to be the primary interface for editing it btw. \nI yet have to understand this constant the-registry-sucks bashing. My .kde dir is polluted with leftovers from old programs too.\n"
    author: "uddw"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "\"Isn't it the windows registry that keeps settings together nicely in one place in consistent and hierachical format?\"\n\nYes it \"keeps it together\", wether the user wants it or not.\n\nSome of my settings are still from 1997 when I used *Solaris*. I still use some of those.\n\nHe, try to reuse anything from a registry from 1997.\n\nAlso the registry is very limited (knows only values and keys, unadequate for many, many settings) and undocumented.\n\n\"And regedit is not meant to be the primary interface for editing it btw.\"\n\nOh really, what's the great advantage about it? I see a lot of trouble and problems with the registry:\n\n- Incompatible between versions\n- Hard to extract settings for a single program\n- Not documented and documentable\n- When messed up very hard or even impossible to repair\n- Impossible to do (cp mysettings settings; /bin/program; cp factorydefault settings)\n- Needlessly adds another concept (The whole filesystem is a hierarchical tree-like structure, no reason to add another similar structure)\n\nI wouldn't find it so bad if there were some advantages that would outweigth all these problems. But I still haven't found it.\n\n\"My .kde dir is polluted with leftovers from old programs too.\"\n\nYeah, just do \"mv ~/.kde ~/.oldkde\", restart, then copy all the settings you really need from .oldkde to .kde\n\nYou will have a very hard time trying to do the same with the registry.\n\n\n\n"
    author: "Roland"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "IIRC KConfigXT only knows about keys and values, grouped by single-hierarchy sections. How is that not limited?\nThe key documentation is nice, but they are not included in the config file itself, so it's not really helpful without special tools. \n\nYou can dump registry subtrees to human-readable files and import them later if you feel the need to do that. \nYou can attach custom ACLs to every single item. \n\nI agree that it's easier to move files around that fiddeling with all kinds of special purpose tools. I guess that's just the windows philosophy, apart from the obvious performance reasons. \nReminds me of the reiser4 pseudofiles vs. extended attributes discussion btw.\n\nI really have no reason to defend the windows registry, but KDE's configuration system is not so far superior that the Windows Registry is pure crap in comparison."
    author: "uddw"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "The first part is already underway, I'm afraid.\n\nhttp://www.kdedevelopers.org/node/1454"
    author: "ac"
  - subject: "Re: Semi-OT: Automatic reload for websites gone in 3.4"
    date: 2005-09-22
    body: "Fair enough - I regard it as a plugin because it is optional if you choose install it."
    author: "Richard Moore"
  - subject: "Compilation question."
    date: 2005-09-22
    body: "Hello people,\n\nPlease excuse me for the stupid question, but how do I compile the beta through Gentoo ebuilds with all the debugging information enabled? I tried the 'debug' USE flag, but the tracebacks were still missing symbol names, so I must be missing something. What DOES the debug flag do, exactly (or, for non-Gentooists: just what does the --enable-debug=full configure option do)? How does the 'nostrip' Portage feature differ, and can I enable it only for certain packages?\n\nThanks, and sorry for the mildly offtopic post. I just really wish to do what I can to help make KDE 3.5 the smoothest KDE 3 ever. :)"
    author: "Anonymous Coward"
  - subject: "Re: Compilation question."
    date: 2005-09-22
    body: "Have a look at your C(XX)FLAGS some can make debugging deficult. I think it is stated in the manpages for GCC, which ones it is."
    author: "m_abs"
  - subject: "Re: Compilation question."
    date: 2005-09-22
    body: "Thank you! I read the manpage, which was insightful, so thanks for the useful pointer. My CFLAGS are fairly conservative, though, and shouldn't be a problem.\n\nWould you happen to also know 1) what --enable-debug=full does exactly, and 2) if I can turn on 'nostrip' only for compilation of certain packages?\n\nThanks!"
    author: "Anonymous Coward"
  - subject: "Re: Compilation question."
    date: 2005-09-23
    body: "You might want to look at the kde .ebuild's so you can see just what is happening at the ./configure stage. --enable-debug=full *should* be all you need, but it might be getting cancelled out somewhere."
    author: "John"
  - subject: "Re: Compilation question."
    date: 2005-09-26
    body: "Oh, I did! I just don't know what --enable-debug=full does exactly, nor how it relates with the 'nostrip' feature. Apparently the two are unrelated -- 'nostrip' is a purely compilation-time thing, and instructs the package management system not to strip the executables from its symbols, and --enable-debug is a purely KDE thing that does... other stuff -- and both probably need to be turned on for full debug support. So I did.\nI was just hoping that USE=\"debug\" would instruct Portage to also turn the 'nostrip' feature on and clear out compilation flags such as -fomit-frame-pointers, but apparently, it doesn't."
    author: "Anonymous Coward"
  - subject: "Re: Compilation question."
    date: 2005-09-23
    body: "Check the flags don't have something silly like -fomit-frame-pointers in them.\n\n\"Conservative\" still usually means more than your average distro with gentoo.."
    author: "Alistair John Strachan"
  - subject: "Re: Compilation question."
    date: 2005-09-26
    body: "Thank you kindly. :)\nI'm at -O2 optimization level, which turns off -fomit-frame-pointers on the architectures where it prevents debugging. I had -fomit-frame-pointers explicitely on before and turned it off before compiling. Thank you for the advice!\n\nThis being said, I'm having loads of issues with KDE 3.5, and I don't know how many are bugs and how many might be local configuration side effects. Do you know where I could discuss those without uselessly spamming bugs.kde.org? The Dot, some forum somewhere? Thanks. :)"
    author: "Anonymous Coward"
  - subject: "Livecd?"
    date: 2005-09-22
    body: "Is planned a new Klax release?"
    author: "Anonymous"
  - subject: "Re: Livecd?"
    date: 2005-09-22
    body: "No, not for this Beta."
    author: "binner"
  - subject: "Re: Livecd?"
    date: 2005-09-22
    body: "why not . i have been waiting for this ..."
    author: "alex"
  - subject: "Re: Livecd?"
    date: 2005-09-22
    body: "I can neither fork nor download hardware."
    author: "binner"
  - subject: "Re: Livecd?"
    date: 2005-09-22
    body: "Not time or bandwidth but - hardware? Surprising..."
    author: "uddw"
  - subject: "Klik?"
    date: 2005-09-22
    body: "When will the Klik version be available?"
    author: "rinse"
  - subject: "Re: Klik?"
    date: 2005-09-22
    body: "### \"When will the Klik version be available?\"\n----\nWorking on it. Probably for the first RC, in one or two or three weeks...\nBut no promise that I will succeed to make it work."
    author: "Kurt Pfeifle"
  - subject: "Re: Klik?"
    date: 2005-09-22
    body: "If you get a klik package from the whole of KDE it's a major feat. It would just be so da*n sexy and make it really, really easy to test for almost anybody. I hope you succeed. :)\n"
    author: "Chakie"
  - subject: "New SubVersion IOSlave, excellent!"
    date: 2005-09-22
    body: "In the best, most useful user tradition, I need to ask...\n\n\"So, how far away is the rsync:// ioslave?\"\n\n(-:\n\nI'd write one myself, if it didn't involve a day or few of getting up to speed on IOslaves first."
    author: "Leon Brooks"
  - subject: "Re: New SubVersion IOSlave, excellent!"
    date: 2005-09-27
    body: "What is the format for using the svn:/ ioslave?"
    author: "Ryan"
  - subject: "Debian packages"
    date: 2005-09-22
    body: "Does anybody know if the KDE's 3.5 packages are available for Debian unstable?\n"
    author: "Marcelo Barreto Nees"
  - subject: "It rocks"
    date: 2005-09-22
    body: "KDE 3.5 does rocks. Thanks alot to all involved. I didn't expect a prelim release to the great 4 to be anything other than an ugly hack :) j/k"
    author: "Ashes of Time"
  - subject: "SUSE 10.0 packages?"
    date: 2005-09-22
    body: "One thing I have always liked a lot about SUSE is that they kept providing high quality KDE packages right after every release. Now I cant find any, there is not even a kde-unstable apt repository for 10.0 any more. Does anyone know why this is not the case any more?\n\nthanks"
    author: "stephan"
  - subject: "Re: SUSE 10.0 packages?"
    date: 2005-09-22
    body: "SUSE will continue to provide those KDE packages. Until recently everyone was busy with polishing SUSE Linux 10.0 (with KDE 3.4.2), now the focus is to get KDE 3.5 Beta into public SUSE Linux 10.1 Alpha next week. Making packages build for older versions has lower priority (for SUSE 9.1 to 9.3 are first packages on ftp.kde.org). Building packages against still unreleased version 10.0 will start soon. :-)"
    author: "binner"
  - subject: "KDE 3.5 on SUSE 9.3 "
    date: 2005-09-22
    body: "And very nice it is too "
    author: "gerry"
  - subject: "Live CD"
    date: 2005-09-22
    body: "Do we have a live CD with the new beta?"
    author: "gerd"
  - subject: "Slackware packages?"
    date: 2005-09-23
    body: "Why does it take forever and a day for slackware packages to appear after each release? It never used to be so bad before.\n\nAnd don't you go telling me to compile my own because i'm on slackware...heh"
    author: "Whoever"
  - subject: "Re: Slackware packages?"
    date: 2005-09-23
    body: "Probably because the nice volunteer person who creates them has some time constraints?  Please note that the KDE project only hosts these binary packages.  The packaging is done by the distributors or some individual user of $DISTRO. "
    author: "cm"
  - subject: "Re: Slackware packages?"
    date: 2005-10-08
    body: "The compilation do not success with kdenetwork, koffice, kdeextragear-multimedia (which includes k3b) etc.,\n\nI've wasted a lot of time compiling kde 3.5 from alpha1 to beta, and always one package or the other causes much trouble.\n\nIt would be wise to wait atleast for KDE 3.5RC1 for all required packaged to get compiled.\n\nKDE 3.5 is awesome, no doubt, but there are many troubles to compile it. or try \"kdesvn-build\""
    author: "fast_rizwaan"
  - subject: "pmount requirement"
    date: 2005-09-23
    body: "Hello,\nI've heard that pmount is a requirement for flexible mounting of devices together with HAL and D-Bus. Despite that I've found no information about pmount on the KDE 3.5 requirement.\n\nFlorian"
    author: "Florian"
  - subject: "Re: pmount requirement"
    date: 2005-09-23
    body: "This is only partly true, as I haven't installed either package (they're a mess and the documentation is poor), and disc insertion notification still works.\n\nIt just takes longer (presumably it polls).\n"
    author: "Alistair John Strachan"
  - subject: "And it passes ACID2 test!"
    date: 2005-09-25
    body: "I tried out Konqueror 3.5 and to my surprise it rendered ACID2 test correctly making Konqueror second browser after development version of Safari (available through NightShift) that passes this test correctly. Nice to see Safari patches are reaching back to Konqueror nicely now.\n\nhttp://www.webstandards.org/act/acid2/\n"
    author: "asdf"
  - subject: "Will I ever get to change "
    date: 2005-09-25
    body: "Kmilo to display a smaller OSD on screen display for volume control.\n\n"
    author: "Anon"
  - subject: "Splashscreen"
    date: 2005-09-28
    body: "I just tried the beta and it looks great!\nJust one thing: the splashscreen is VERY ugly. I really hope it won't be in the final version, it gives KDE an amateurish look that should be avoided."
    author: "Anonymous"
---
The testing period for the next major KDE release has begun with the <a href="http://www.kde.org/announcements/announce-3.5beta1.php">release of KDE 3.5 Beta 1</a>, codenamed Kanzler.  This will be the last major release in the KDE 3 series so make sure it turns into the best one by downloading and testing today.  The <a href="http://www.kde.org/info/3.5beta1.php">3.5 Beta 1 information page</a> gives the <a href="http://download.kde.org/unstable/3.5-beta1/">download link</a> as well as an important warning on using Qt 3.3.5.  Packages are currently available for <a href="http://www.kubuntu.org/announcements/kde-35beta1.php">Kubuntu</a> or you can use <a href="http://developer.kde.org/build/konstruct/">Konstruct</a> to guide you through the compile.



<!--break-->
