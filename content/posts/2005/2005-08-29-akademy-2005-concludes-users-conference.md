---
title: "aKademy 2005 Concludes Users Conference"
date:    2005-08-29
authors:
  - "jriddell"
slug:    akademy-2005-concludes-users-conference
comments:
  - subject: "can't load wiki.kde.org"
    date: 2005-08-29
    body: "It's just me or it it everytime impossible to load?\nI tryied numberous times and it just loads and loads forever..."
    author: "Iuri Fiedoruk"
  - subject: "Re: can't load wiki.kde.org"
    date: 2005-08-29
    body: "I'm getting through :)"
    author: "Sam Weber"
  - subject: "Re: can't load wiki.kde.org"
    date: 2005-08-29
    body: "Maybe they locked out some IPs? I know some time ago netcraft had blocked brazilian ips. Here is my backtrace:\n 1  BrT-L10-smace701.dsl.brasiltelecom.net.br (201.14.223.254)  17.905 ms   22.478 ms   25.752 ms\n 2  BrT-G5-0-0-710-smace300.brasiltelecom.net.br (201.10.227.133)  29.672 ms   33.962 ms   36.839 ms\n 3  * * *\n(repeat same * * *)\n30  * * *\n\nCan't ping or telnet port 80.\nUsing a proxy server I was able to load it, very strange."
    author: "Iuri Fiedoruk"
  - subject: "Integration"
    date: 2005-08-29
    body: "\"integration\" is a real buzz. \n\nBut it deserves attention. \n\nMost of what we need is already there. \n\nWill rosetta get integrated into the KDE translation project? A more open framework and a web engine could faciliate translation by reducing entrance barriers."
    author: "Matze"
  - subject: "#akademy"
    date: 2005-08-29
    body: "Don't forget the #akademy IRC channel on irc.kde.org for interaction with participants or the Fluendo guys if there is a problem with the streaming."
    author: "binner"
  - subject: "xgl & exa"
    date: 2005-08-29
    body: "Will the fancy stuff that Zack showed (http://vizzzion.org/?blogentry=526) also\nrun well using exa on xorg?\n\nexa [3] seems to arrive on an average desktop much earlier (guess: end of 2005) than an opengl-based x-server...\n\n\nAnd, does anybody know if these other patches from Lars and Zack have been \ncommitted to xorg cvs:\n\n\n[1] \"xrender improvements\" (speedups between 2 and 8 over the old xrender)\n[2] \"gradients for xrender\" (nice for cairo and arthur I guess)\n\n\n[1]: http://lists.freedesktop.org/archives/xorg/2005-April/007446.html\n[2]: http://lists.freedesktop.org/archives/xorg/2005-May/008105.html\n[3]: http://lists.freedesktop.org/archives/xorg/2005-June/008356.html\n\nTT guys you rock and you make the desktop in 2006 look great in Linux!\nHasta la Vista baby.\n"
    author: "ac"
---
The <a href="http://conference2005.kde.org/userconf.php">KDE Users and Administrators Conference</a> has finished after two successful days of talks, discussion and partying.  Highlights from today include Boudewijn Rempt's Krita talk (<a href="http://koffice.kde.org/krita/akademy2005.pdf">PDF transcript</a>) and Dirk M&uuml;ller's <a href="http://wiki.kde.org/tiki-index.php?page=Firefox+KDE+Integration">update on Firefox/KDE integration</a>.  <a href="http://wiki.kde.org/tiki-index.php?page=Talks%20%40%20aKademy%202005">Transcripts for some</a> of <a href="http://conference2005.kde.org/sched-userconf.php">the talks</a> are now available and <a href="http://ktown.kde.org/akademy2005/">unprocessed videos</a> are being uploaded though edited versions will be available later.  Over the weekend there have been several spontanious BoF meetings with initial brainstorming for KDE 4 including the control centre, script languages and IPC.  The <a href="http://wiki.kde.org/tiki-index.php?page=Pictures+%40+aKademy+2005">photos page</a> has an increasing number of snapshots linked.  Monday sees the start of the two day <a href="http://conference2005.kde.org/devconf.php">KDE Developers and Contributors Conference</a>, live video streaming will be available from <a href="http://fluendo.com">Fluendo</a>'s <a href="http://stream.fluendo.com/akademy/">streaming server</a>, see the <a href="http://conference2005.kde.org/sched-devconf.php">schedule</a> for exciting talks yet to come.







<!--break-->
