---
title: "KDE Plugin in the Official OpenOffice.org Packages"
date:    2005-07-22
authors:
  - "jholesovsky"
slug:    kde-plugin-official-openofficeorg-packages
comments:
  - subject: "Screenshots"
    date: 2005-07-22
    body: "http://www.eikehein.com/writer.png\nhttp://www.eikehein.com/calc.png\n\nSexy."
    author: "Eike Hein"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "That looks very nice."
    author: "Tim Beaulen"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "I agree, but without improving the look of Linux fonts by default, Linux generally gets the \"ugly\" label. This does not help improve appeal! I always have to download the webfonts.sh script in addition to getting M$ fonts in order to have a somewhat good desktop. I also have to tweak the X11 resolution to get the dots-per-inch right. This is on *all* distros I have tired! Look at my slashdot.org page. We in the Linux world must improve the general appearance of our products in addition to all the technical advantages in order to attract new blood into the community."
    author: "cb"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Try SUSE, it downloads the correct fonts during the installation for you\n\nRinse"
    author: "rinse"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "That means that I must be online to have a decent looking desktop? Absurd! This is what should be fixed. That reminds me of the requirement that one must be online to have the most \"stubborn\" dependencies resolved! Absurd too especially when Apple, which is UNIX based, has no trouble with these issues."
    author: "cb"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: ">That means that I must be online to have a decent looking desktop?\n\nWell, the bitstream TTF fonts are also available on the installation disks. Never tried those, but perhaps they can make your desktop decent enough :)\n\n>This is what should be fixed\n\nGo ahead and donate some decent looking TTF fonts.\n\n>That reminds me of the requirement that one must be online to have the most \"stubborn\" dependencies resolved! \n\nMust be a problem with your configuration. Resolving dependencies via internet is only neccesary if you downloaded applications from internet that require additional packages that are not delivered with your distro.\nI guess you should have downloaded more packages before disconnecting from the internet.\nAlso, dependencies are resolved according to your configuration. If you want to use an offline source, like a directory or cd, you should tell your Linux-distribution to do so.\n\n>Absurd too especially when Apple, which is UNIX based, has no trouble with these issues.\n\nI'm not familliar wit MacOS, but i know that on Windows you would have the same problem.\nFor instance, if you install Gimp on Windows, you need to GTK as well, or Gimp won't work without this unresolved dependencie. Same goes for Ghostview, if you want to be able to open EPS-files, etc..\n\nAnd since Microsoft does not include Ghostview and GTK with their Windows-distribution, you probably need an Internet connection to download Gimp's dependencies.\n\nDunno if macOS ships with GTK, but if not, you would even need to use an internet connection to be able to use gimp on macos.\n\nSame probably counts for Qt-based apps, etc.\n"
    author: "rinse"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "On Windows and OS X most programs just throw EVERYTHING in with the installation, on OS X they hide in the .app 'file' (folder), on Windows they are either throw into the application directory, a system directory (system32, base windows folder), or vary rarely (only really seen with a GIMP installer), its in its own folder (like \\Program Files\\GTK, sometimes they let you choose, sometimes they aren't that nice).\n\nBecause of that you generally have no clue what is included with your system, and if a common library turns out to have a major vulnerability you have no clue what needs to be upgraded (on Linux and most other *nixs you would simply upgrade the library since things generally aren't statically compiled)."
    author: "Corbin"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "That reminds me of acrobat reader.\nIt's about 80-100 MB big when installed!! (on windows/linux)\n(but no dependency issues :)\n\nNow, how big is kpdf??\n"
    author: "rinse"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "> Now, how big is kpdf??\n\nWho cares?! You can buy a 300GB disc for $80. I agree that it's better to have one lib for each task but the problem is that Linux cannot handle that!\n\nYou (developpers) must understand that INSTALL is NOT about COMPILATION and I (and my mom) should NOT need to get lib_xpto_2.3.2.3.2.4.so that needs the new lib_kernel_x4.3.2-alpha.beta.zeta.so that was compiled with some new gnome_xk2gl.3.2.1.34.2 that, at least, is included in some rpm.\nThat's good for developpers not users.\n"
    author: "CJ"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "The problem is not disk. I have plenty of disk free left, even considering I compile most of the KDE sources with debugging information, and I have lots of stuff installed.\n\nThe problem is RAM. You cannot buy 10GB of RAM for $80. Even if you could, most computers simply can't handle them (the slots won't accept).\n\nSo, if you load 3 programs, each statically compiled, or with disjoint libraries (like MacOS X), you're kicking into the 300 MB level. Imagine if KDE were made like this.\n\nNo, I like telling apt or smart to install a package and downloading as little as 26 kB. As I said, I already have most of the stuff in my computer anyways, so why should I download again and again everything?\n\nAnd before you whine again, I agree there is room for improvement in the installer area for third-party applications. For distribution-provided applications, I think it's got to as good as it can get."
    author: "Thiago Macieira"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: ">Who cares?! \n\nI do :)\n\n\nYou can buy a 300GB disc for $80. \nNot in my country, a 50 GB disc kosts &#8364;75\n&#8364;75 may sound cheap to you, but for me it is quite a lot of money to spent om something i don't need to use if apps were better build (e.g. using shared libraries in stead of static.\n\nBesides, downloading a 2 mb dependencie for kpdf is much easier then downloading acroread, which is about 45 MB!! \nSo in stead of complaining that you need an internet connection to solve dependencies on Linux, you should complain about de need of having a *broad band* connection for downloading software for windows or macOS\n\n\n>I agree that it's better to have one lib for each task but the problem is that Linux cannot handle that!\n\nMust be a problem with your configuration, on my machine Linux handles that perfectly.\nIn fact Linux handles it better then Windows, because you can have several version of the same lib installed side by side, and can use symlinks if an app expects another version, while yours is compatible with it.\n\n>You (developpers) must understand that INSTALL is NOT about COMPILATION\nI'm not a developer :)\nBu i don't see much difference, if wether you compile or install new software, you still need to solve the dependencies.\n\n>I (and my mom) should NOT need to get lib_xpto_2.3.2.3.2.4.so that needs the new lib_kernel_x4.3.2-alpha.beta.zeta.so that was compiled with some new gnome_xk2gl.3.2.1.34.2 that, at least, is included in some rpm.\n That's good for developpers not users.\n\nI don't see a problem here.\nIf you or your mom wants to use lets say: kpdf, you just type something like 'apt-get install kpdf' and a moment later (assuming broadband), you have kpdf installed with all it's dependencies.\nNo need to look for lib-this and lib-that.\nAll taken care of automaticly.\n\nThe only time you get in trouble is when you try to install software like you are used to do on Windows.\nE.g. downloading something from whatever site, and then installing it.\nSince most of the time you need more then 1 package to get the software working, you'll get into trouble.\nBut again, if your packagemanager is configured correctly, it is most likely that even then the dependencies are automaticly solved.\n\n\n\n"
    author: "rinse"
  - subject: "Re: Screenshots"
    date: 2005-08-02
    body: "I just downloaded some Windows drivers for my HP nw8240 NB. \nYou won't believe these:\n\nATI X700 VGA driver: 24 MB\nBroadcom's Gbit Ethernet chip: 24.6 MB\nIntel chipset + Pro/Wireless: 10 MB\nSynaptics touchpad: 5.3 MB\n"
    author: "Monte Lin"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "\"OS X most programs just throw EVERYTHING in with the installation\"\n\nThat's not true. MacOSX does a good job on providing a set of standard libraries that should cover every need while also granting binary compatibility.\nThe idea is the same behind Java and their Classpath libraries.\n\nIf you need extra stuff, you either statically compile it into your application, if it is a small library, say a lisp parser, or provide the libraries in a MacOSX's package fashion."
    author: "blacksheep"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "I don't think that you need to be online to get a decent looking desktop. However, it is a somewhat disturbing trend that you need to go online to get some core functionality for your linux OS, because distributors don't ship it for license reasons. Fonts, audio and video codecs (even standard ones like mp3), graphics card drivers, and this list will probably only get longer as more and more new formats will not be GPL compatible.\n\nI'm not sure what the best solution to this problem is, it might be that the GPL must be adjusted. For example, I think it should be possible for distributors to ship the NVidia graphics drivers, even if their source is not public.\n"
    author: "ac"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Most of the problems you're alluding to aren't due to GPL-incompatibilities, but due to vendors not allowing those files to be distributed, or due to patent problems.\n\nSome winmodem driver manufacturers do not allow redistribution, meaning you can only get it from their site. It can't be included in your CD. The MP3 decoder is in a doubtful legal position, so distributions like Red Hat and Debian choose not to include it in order to be on the safe side.\n\nIn all of those cases, there is a solution: use Ogg Vorbis and don't buy hardware from vendors who choose to make draconian policies regarding their drivers. I am not even asking for them to open their drivers source code: just allowing redistribution would come a long way already."
    author: "Thiago Macieira"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "> Most of the problems you're alluding to aren't due to GPL-incompatibilities, but due to vendors not allowing those files to be distributed, or due to patent problems.\n\n> Some winmodem driver manufacturers do not allow redistribution, meaning you can only get it from their site. It can't be included in your CD. The MP3 decoder is in a doubtful legal position, so distributions like Red Hat and Debian choose not to include it in order to be on the safe side.\n\nWhatever the reason, fact is that Windows (and probably also OS-X, I don't know) ships with all the drivers/codecs/fonts/whatever a normal user needs. This is not the case for linux distributions, and that's bad (and getting worse).\n\n> In all of those cases, there is a solution: use Ogg Vorbis and don't buy hardware from vendors who choose to make draconian policies regarding their drivers.\n\nThis is a nice idea, but not always practical. For example, do you know a laptop where everything works out of the box (WLAN, standby, accelerated 3d gfx, modem) with a standard distribution, and that doesn't cost more than the mass market machines from Acer, Sony and the like?\n\n> I am not even asking for them to open their drivers source code: just allowing redistribution would come a long way already.\n\nI totally agree.\n"
    author: "ac"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "> For example, do you know a laptop where everything works out of the box (WLAN, standby, accelerated 3d gfx, modem) with a standard distribution, and that doesn't cost more than the mass market machines from Acer, Sony and the like?\n\nYes, Kubuntu 5.04 works without _any_ problem on my HP / Compaq nx6110 and a HP Pavilion 4300. Everything works, from sound, networking to 3D. So, go ahead and buy one if you want to support a vendor that uses Linux supported hardware.\n"
    author: "Raoulduke"
  - subject: "Re: Drivers"
    date: 2005-07-23
    body: "> Whatever the reason, fact is that Windows (and probably also OS-X, I don't know) ships with all the drivers/codecs/fonts/whatever a normal user needs. This is not the case for linux distributions, and that's bad (and getting worse).\n\nThis is not exactly true. While Windows ships with quite a broad set of drivers, especially 3D capable graphics drivers you mostly have to worry about yourself. Although most graphic cards ship with Windows drivers, they're often crappy enough that you have to download a newer version yourself. And if you buy a new version of Windows (long ago, wasn't it) you normally have to download quite a lot of drivers - if you're lucky and they exist.\n\nSo my point is that the driver problem is quite the same under Windows, although well-covered behind tons of complete systems shipped with Windows and all drivers installed."
    author: "Christian Meyer"
  - subject: "Re: Drivers"
    date: 2005-07-23
    body: "> So my point is that the driver problem is quite the same under Windows, although well-covered behind tons of complete systems shipped with Windows and all drivers installed.\n\nWell said. But it seems that even if a vendor wanted to provide a customized version of Linux for a specific machine (like all the Windows OEM versions that ship with almost all new computers) he couldn't do that, because many companies don't allow redistribution of their drivers."
    author: "ac"
  - subject: "Re: Drivers"
    date: 2005-07-24
    body: "> But it seems that even if a vendor wanted to provide a customized version of Linux for a specific machine (like all the Windows OEM versions that ship with almost all new computers) he couldn't do that, because many companies don't allow redistribution of their drivers.\n\nMaybe. But they might want to rethink their policy if they get something like \"We want to buy 1000 graphics cards of yours but it must ship with linux drivers\". (Yes, I'm an optimist. Sometimes.)"
    author: "Christian Meyer"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: ">For example, do you know a laptop where everything works out of the box (WLAN, standby, accelerated 3d gfx, modem) with a standard distribution, and that doesn't cost more than the mass market machines from Acer, Sony and the like?\n\nDo you know any where it works on Windows the way you describe it?\nI don't!\nIn every case something is either not working at all, not reliable or not with full functionality.\nDamn all drivers you get out of the box with your fresh new model and download the newest and hope the vendor gives you this supports.\nE.g. the laptop I'm using has on Win either a not working WLAN driver with WEP (the one shipped with it) or with the new driver one with the wrong language for dialogs (french instead of german, and yes I've choosen the german package).\n\nSuSe 9.2 was a pain to configure, with 9.3 it's easy and works like a charm."
    author: "Philipp"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: ">Whatever the reason, fact is that Windows (and probably also OS-X, I don't know) ships with all the drivers/codecs/fonts/whatever a normal user needs. This is not the case for linux distributions, and that's bad (and getting worse).\n\n\nFact remains, windows desktop doesn't ship with wordprocessors, spreadsheats and much more essential software. As long as Microsoft doesn't provide that windows have no chance on the desktop.\n\n"
    author: "Uno Engborg"
  - subject: "Re: Screenshots"
    date: 2005-07-24
    body: "> As long as Microsoft doesn't provide that windows have\n> no chance on the desktop.\n\nYou call 90% market share 'no chance'?\n\nArgh! How can people this stupid exist?"
    author: "Tim"
  - subject: "Re: Screenshots"
    date: 2005-07-24
    body: "I too was born without the sarcasm gene."
    author: "Troy Unrau"
  - subject: "Re: Screenshots"
    date: 2005-07-25
    body: ">Whatever the reason, fact is that Windows (and probably also OS-X, I don't know) ships with all the drivers/codecs/fonts/whatever a normal user needs.\n\nOh sure it does.  Virgin Windows installs are perfect for watching all my dixv, xvid, ogm, DVD, realmedia and quicktime content!  Uh, I mean.. WTF were you saying again?"
    author: "cKahl"
  - subject: "Re: Screenshots"
    date: 2005-07-25
    body: "But you pay for Windows. And that's why Microsoft can afford to pay for codecs. TurboLinux paid to license legal DVD codecs (CSS decrypters) and to license the codecs from Micorsoft, which is why they can provide them. The snag is you have to pay TurboLinux in turn.\n\nWhat's disturbing is that distributions, none of which are particularly cheap any more, are not chasing this up more. How hard would it be to license the MP3 codec from Fraumhofer and create an XMMS compatible, closed source library that could then be legally shipped? Or to license codecs from Microsoft for a media player?\n\nWhen I pay my \u008080 to SuSE for the Pro edition, I expect a bit of added value."
    author: "Bryan Feeney"
  - subject: "Re: Screenshots"
    date: 2005-07-25
    body: "It's ironic you mention SuSE Pro, their distro is nearly overkilled by plenty of 'value additions'. And why do you mention XMMS? That's a very outdated non-KDE app..."
    author: "ac"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "\"For example, I think it should be possible for distributors to ship the NVidia graphics drivers, even if their source is not public.\"\n\nAs a matter of fact, it *is* possible for distributors to bundle their product with nvidia closed drivers, they are redistributable. Distros just won't.\nThere are two reasons for this -- I'll name them after distros, but they just don't apply to those:\n- Debian: wants to keep non-freesoftware away of their distro.\n- Mandriva: wants to give more value into their packaged boxes and club versions.\n\nAnd nvidia doesn't break Linux license: they run in userspace. The same doesn't apply to ATI's."
    author: "blacksheep"
  - subject: "Re: Screenshots"
    date: 2005-07-24
    body: "Bzzt, wrong ;P\n\nzack@gaz ~ =) % lsmod | grep nvidia\nnvidia               3923580  12\nagpgart                34600  2 nvidia,via_agp\n"
    author: "zack"
  - subject: "Re: Screenshots"
    date: 2005-07-24
    body: "Obviously there is an interface module that is the thing compiled when you run the nvidia installer. This interface is open, not the driver's core that runs in userspace."
    author: "blacksheep"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "Before making stupid remarks you should probably know the facts. SUSE downloads Microsoft's TrueType fonts Arial, Times New Roman, etc. Downloading those fonts from microsoft.com is allowed but distributing the fonts with the CDs/DVDs is not allowed. So, yes, it means that you must be online to be able to download those fonts.\n\nApple has no trouble with these issues? Well, doh! Obviously they have enough money to buy licenses for decent fonts. You probably don't have any idea about the price for a license of a high quality font.\n\nAnother problem is (or was?) a patent-issue which forced commercial distributors to cripple the font renderers. So even if a good font is installed the font renderer doesn't use all the special hints which make the fonts look better especially in small sizes. Of course, you can compile a not crippled font renderer yourself. So this isn't a technical problem but a patent/license problem.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "> So this isn't a technical problem but a patent/license problem.\n\nBut it's a problem nevertheless. This might actually be the worst kind of problem, because it can't be overcome with the ingenuity and technical abilities of FOSS writers.\n"
    author: "ac"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "Linux doesn't need better fonts.\nIn by far the most cases you need good looking fonts only in the GUI."
    author: "Cornelius Clemens"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "I agree, but without improving the look of Linux fonts by default, Linux generally gets the \"ugly\" label. This percieved ugliness does not help improve appeal! I always have to download the webfonts.sh script in addition to getting M$ fonts in order to have a somewhat good desktop. I also have to tweak the X11 resolution to get the dots-per-inch right. This is on *all* distros I have tired! Look at my slashdot.org page. We in the Linux world must improve the general appearance of our products in addition to all the technical advantages in order to attract new blood into the community."
    author: "cb"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Well, yours looks butt-ugly because you don't have anti-aliasing turned on :-)\nCheck out my screenshot from my laptop. My Ubuntu desktop has even nicer fonts and anti-aliasing imho, but I cba getting a screenshot.\n\nKyle"
    author: "Kyle Gordon"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "jpeg format is really not the best to show-off font anti-aliasing"
    author: "Anonymous"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "At least web pages look pretty good in Konqueror on FreeBSD ;)"
    author: "GuZ"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Attachment got lost after preview..."
    author: "GuZ"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "I'm using Gentoo and my fonts look great (see attached slashdot screen grab).  The fonts look just as good on Fedora Core 3.\n\nOn Gentoo though I had manually enable anti-aliasing (KDE apparently defaults to off), though most distros afaik enable it by default (like Fedora and I think Kubuntu).\n\nThe font I'm using in that screenshot is Luxi Sans 10 (Bitstream Vera Serif 10 for the menus)."
    author: "Corbin"
  - subject: "sorry CB..."
    date: 2005-07-22
    body: "But i have to say you really did a neat job make that desktop of your really ugly... I see you use many different fonts that don't match. It's a nice piece of FUD[1]. I use many distro's side by side and none of them look this horrible by default. Well you sure made me laugh with that screenshot.\n\n[1]:\nhttp://en.wikipedia.org/wiki/Fud"
    author: "cies"
  - subject: "Re: Screenshots"
    date: 2005-07-23
    body: "WTF? are you still in 1996? I don't know what linux distro you are attempting to use, but all the major ones have this worked out, OK? This is a non-issue."
    author: "Tim"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Sexy indeed. Really good stuff.\n@emerging openoffice-bin-1.9.118.ebuild now\n ... will report on that later"
    author: "jumpy"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Done! Great to see the whole suite using my Style theme!\nAs stated, kde icons and kde file dialog are not included, but existing ones look good too :-)\nHere are my screenies:\n http://img99.imageshack.us/img99/8149/test7nk.png\n http://img347.imageshack.us/img347/5793/test16xs.png\n\nMany thanks to Jan Holesovsky! (http://artax.karlin.mff.cuni.cz/~kendy/blog)"
    author: "jumpy"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "\"As stated, kde icons and kde file dialog are not included\"\n\nWhat is the reason for this? Especially the file dialogs would be very nice.\n\nThanks a lot for this amazing effort!\n"
    author: "ac"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Afaik it's hard to get stuff accepted into the official OOo sources (style emulation existed first and only entered now), hence ooo-build to be able to manage all patches. "
    author: "Anonymous"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Out of curiosity, how long does it take you to build OO ? Must be a few hours, even with new hardware right ?\n\nThanks !"
    author: "MandrakeUser"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "In portage there is also precompiled version of openoffice. It takes only few minutes:\n\nFri Jul 22 09:03:07 2005 >>> app-office/openoffice-bin-1.9.118\n       merge time: 7 minutes and 41 seconds."
    author: "radfoj"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "So, it gets some sort of a binary delta and merges it ? Cool !"
    author: "MandrakeUser"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "Deltas of binary files aren't done, but recently you can do deltas of KDE's source so when there is a change you don't have to download as much (only the difference).\n\nThings like Mozilla, Firefox, and OpenOffice generally have binary packages available (I still prefer source over the bins, even if they take 10 to 100 times longer to install, they just feel more at home then)."
    author: "Corbin"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "I see your point. I think I'd use a mixture myself. Anyways, so what does  \"merge time: 7 minutes and 41 seconds\" really mean in the message above ? Time to install the full binary ?"
    author: "MandrakeUser"
  - subject: "Re: Screenshots"
    date: 2005-07-22
    body: "> Anyways, so what does \"merge time: 7 minutes and 41 seconds\" really mean in the message above ?\n\nDownload time of the binary files + installation (unpacking, moving)."
    author: "Eike Hein"
  - subject: "I emerged 1.9.118"
    date: 2005-07-22
    body: "I emerged 1.9.118, but dont know, what to do now. How can I do it to let OOo using KDE widgets?"
    author: "radfoj"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-22
    body: "It seem that I didnt understand it. One think I was before worrying about, it was e.g. \"save as\" or \"open\" dialog - why they didnt looks like as KDE dialogs or how to enable it. :-("
    author: "radfoj"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-22
    body: "Currently the Open and Save As dialogs aren't using the native KDE dialogs (according to a post farther up)."
    author: "Corbin"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-22
    body: "I have created an ebuild that is based on ooo-build:\nhttp://sayap.net/gentoo/openoffice-ximian-overlay.tar.bz2\n\nGive it a try :)"
    author: "j-kidd"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-22
    body: "Is it only me who sees that those icons are kind-of too big? If they were made smaller, and contrast increased, they'd look much better."
    author: "cb"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-25
    body: "Gentoo-Dev suka also works on that:\nhttp://dev.gentoo.org/~suka/overlay/\n\nMaybe you should contact him and coordinate the work on inofficial ooo-build-ebuilds."
    author: "Hanno"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-25
    body: "Hanno (geki :)), I know that suka has started working on it, but I think I should give my ebuild a little testings first before contacting him.\n\nIf you look at http://lists.ximian.com/pipermail/openoffice/2004-November/000587.html, suka said that he would try to use the ooo-build system but unfortunately it is still not the case. The ebuild I made is based on the ooo-build system which should make the job of maintaining the ebuild much easier.\n\nTo everyone who has tried the ebuild, can you please send a success/failure report to ooo.build at gmail.com? Of course, just take your time if it is still compiling for you. It takes more than 24 hours for a successful compile here :)\n\nAnd attached is a screenshot of higher contrast, smaller icons for cb."
    author: "j-kidd"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-26
    body: "Reading the ebuild, why do you use:\n\n! use flag && do_something\n\ninstead of\n\nuse flag || do_something\n\n</pedant-mode>"
    author: "genneth"
  - subject: "Re: I emerged 1.9.118"
    date: 2005-07-27
    body: "I don't know, it was originally an if-then block so I guess it was straight forward to rewrite it in such a way. Is there any advantage of using one way over the other?"
    author: "j-kidd"
  - subject: "what distros ?"
    date: 2005-07-22
    body: "So, is there a list of distros using ooo-build ? Is Mandriva using it ? How about Kubuntu ?\n\nMany thanks,"
    author: "MandrakeUser"
  - subject: "Re: what distros ?"
    date: 2005-07-22
    body: "Many have these desktop integration patches for OOo: SUSE (of course), Fedora, Kubuntu Breezy, ..."
    author: "Anonymous"
  - subject: "No sense"
    date: 2005-07-22
    body: "Whay is the \"Preview\" button an electrocutaded printer?\n\n"
    author: "JB"
  - subject: "Dark grey menus"
    date: 2005-07-22
    body: "Are the menus still dark grey and horrible looking?"
    author: "Eha"
  - subject: "Re: Dark grey menus"
    date: 2005-07-22
    body: "I guess the answer is \"no\". :-)"
    author: "Anonymous"
  - subject: "Re: Dark grey menus"
    date: 2005-07-23
    body: "Great! :D"
    author: "Eha"
  - subject: "support for kio-slaves"
    date: 2005-07-23
    body: "Hi!\nwould be greate if the kioslaves (sftp/fish/smb) would be fully supported to allow remote working.\noo118 hangs on opening and does not save to sftp (message protokoll not supported)\ncu"
    author: "fg"
  - subject: "java"
    date: 2005-07-23
    body: "what is that state of java usage in ooo2? has gcj been integrated yet to allow building on clean (as without comercial software) systems?"
    author: "hannes hauswedell"
  - subject: "Re: java"
    date: 2005-07-23
    body: "OO 1.9m120 still needs the JDK to build, at least on FreeBSD..."
    author: "GuZ"
  - subject: "OT: KDE on Fedora"
    date: 2005-07-23
    body: "In other news from Ottawa Linux Symposium: RedHat is considering moving KDE to\nextras on Fedora and RHEL5. Was said in yesterdays Fedora BOF."
    author: "Anonymous"
  - subject: "Re: OT: KDE on Fedora"
    date: 2005-07-24
    body: "It's been discussed for a while now already. Trust me, KDE will be happier in Extras. You'll still be able to install KDE during the Fedora install, too if and when the transfer happens."
    author: "zmc"
  - subject: "KDE-OOo slow?"
    date: 2005-07-23
    body: "Hi,\n\nI am using the Suse RPMs of OpenOffice.org 1.9.116. They include the Crystal Icons and the KDE plugin with the Native Widget Framework. But this Openoffice is much slower than a plain OpenOffice.org 1.9.116. Is the KDE Native Widget Framework really so slow? For example, it takes some seconds for the file dialog to appear, the file dialog of the unmodified OOo appeares nearly instantaneously. Even the menu bar shows some lag. It's like working over a 10 Mbit romote X11 connection...\n\nI hope this will improve, otherwise the KDE integration can only be seen as a prove of concept."
    author: "Asdex"
  - subject: "Re: KDE-OOo slow?"
    date: 2005-07-23
    body: "It's just a proof of concept, actual KDE integration is something different from what's done to that \"KDE Native Widget Framework\"."
    author: "ac"
  - subject: "Qt version"
    date: 2005-07-26
    body: "I get this error message:\n\nPlugin uses incompatible Qt library (3.3.4)!\n\nDo I need to use a specific Qt version?\n\n"
    author: "James Richard Tyrer"
---
A year and a half after launching the <a href="http://kde.openoffice.org/">KDE.OpenOffice.org Integration Project</a>, the KDE plugin with the Native Widget Framework has become part of the official <a href="http://www.openoffice.org/">OpenOffice.org</a> development packages. <a href="http://download.openoffice.org/680/index.html">Developer snapshot m118</a> is the first of the towards-2.0 milestones that contains it.









<!--break-->
<p>Please keep in mind that if you use a Linux distribution with OpenOffice.org packages based on <a href="http://www.go-oo.org/">ooo-build</a> (<a href="http://www.go-oo.org/wiki/index.php/Main_Page">Wiki</a>), you most probably already have this.  ooo-build also contains additional KDE integration features like <a href="http://artax.karlin.mff.cuni.cz/~kendy/ooo/icons/status">Crystal icons for OOo</a> (big thanks to Nuno Pinheiro and Robert Wadley!), and KDE file dialog.








