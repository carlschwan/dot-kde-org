---
title: "Qt 4 Designer Review"
date:    2005-06-04
authors:
  - "jthelin"
slug:    qt-4-designer-review
comments:
  - subject: "Text display parameters "
    date: 2005-06-04
    body: "Could a Qt expert please tell me where to look for where to set the Text parameters for a QTextDocument?  Specifically, how font hints will be used.\n\n"
    author: "James Richard Tyrer"
---
The <a href="http://qt4.digitalfanatics.org/">Qt 4 Resource Center</a> is active again and now sports <a href="http://qt4.digitalfanatics.org/articles/des2.html">a brand new follow-up</a> to the <a href="http://qt4.digitalfanatics.org/articles/i.html">previous Designer introduction</a>. Apparently, the <a href="http://trolls.troll.no/">Trolls</a> have been working hard and have pretty much improved the tool in every area.


<!--break-->
