---
title: "Tuxmachines: Interview with Roberto Cappuccio"
date:    2005-08-20
authors:
  - "binner"
slug:    tuxmachines-interview-roberto-cappuccio
comments:
  - subject: "Interesting inteview!"
    date: 2005-08-20
    body: "Kat is a really interesting project, as is Tenor. The potential both have to make the desktop experience more intuitive and convenient is enormous. At first I thought this was just KDE's response to Beagle jsut like when gDesklets was GNOME's response to Super Karamba, but it really seems to be much more!\n\n\"I'm searching for a sponsorship from a big software company, like the one Trolltech offered to Aaron Seigo. I need to work a lot to Kat and Tenor, but I also have to work for my university. If I could get a sponsorship, the evolution of Kat could be much more quick.\"\n\nThad definitely would be great. But I think until then, we should all help a little with donations!"
    author: "Alex"
  - subject: "Re: Interesting inteview!"
    date: 2005-08-20
    body: "> I'm searching for a sponsorship from a big software company\n\nLooks like Roberto was meant when Aaron wrote: \"one of the kat developers even claims to have an ego that eclipses my own.\""
    author: "Anonymous"
  - subject: "tried it"
    date: 2005-08-21
    body: "i tried Kat to scan my 512M usb key, it generated a 250M file to store the db. I wonder how much space tenor/kat db will take when it will scan my whole  disk... scary"
    author: "joe"
  - subject: "Re: tried it"
    date: 2005-08-21
    body: "Depending on how it generates that index, it may not be much larger.  Many types of indices grow quickly at first and then much much slower as similar data is fed into them.  It could also be a sparse file, which would make it show up as being that large large using ls, but it only takes a few kilobytes of actual diskspace.\n\nOut of curiosity, I created a sparse file and tested it using konqueror.\n\nevan@betty:~$ ls -alh test.dat\n-rw-r--r--  1 evan evan 251M 2005-08-20 19:43 test.dat\nevan@betty:~$ du -h test.dat\n204K    test.dat\n\nKonqueror sees it as being 251 megs, and shows no indication that it is actually 204k on disk."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: tried it"
    date: 2005-08-21
    body: "As it is done right now the db will be large. It contains a text copy of each document.\n\nFrom the source:\n\ndb->execDML(\"create table fulltexts (\"\n   ...\n   fulltextdata blob,\n   ...\n\nI haven't delved into the code enough to know what is actually happening.\n\nInterestingly many text retrieval systems store and reconstruct query results from their internal indexes.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: tried it"
    date: 2005-08-21
    body: "The first time I used Kat, it created a 1,5 gig large index file, while indexing my home directory.\nThat was all the space that was available on the /home partition, so Kat stopped indexing half way the process.."
    author: "rinse"
  - subject: "Re: tried it"
    date: 2005-08-22
    body: "Maybe people telling that we should use Reiser4 instead of an SQL db were right. At least with a file system indexing you dont need to duplicate any single text document, or do you? I dont really see the point of cataloging my hard drive if I need another hard drive to do so, that kinda sucks."
    author: "joe"
  - subject: "Re: tried it"
    date: 2005-08-23
    body: "the way kat does it right now is rather suboptimal. done properly you don't store an actual copy of the data."
    author: "Aaron J. Seigo"
  - subject: "Re: tried it"
    date: 2005-08-24
    body: "I was thinking of this today. How else would one get reasonable response time for browsing the results of a search?\n\nIf you depended on openoffice (for example) to display a search result, as opposed to a snippet of text extracted during idle moments, the experience would be rather unsatisfying.\n\nReading through the Kat code, seeing the broad conceptual outline of the application gives one pause. To become useful will require enormous effort. Even in the face of this daunting task, I find myself dreaming about text sorting and retrieval. There is something about the human/machine interface that is endlessly fascinating. In this instance it is about designing a machine that can make sense of human expression.\n\nDerek"
    author: "Derek Kite"
  - subject: "ahem"
    date: 2005-08-21
    body: "\"I have always been a Windows power user. I tried Linux a lot of times during the past years but I found it terribly unstable and mostly unusable.\"\n\n...\n"
    author: "Petar"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "I've been using *only* Slackware 10.1 for many months on my system, i do not agree with your opinion of \"terribly unstable\" and \"mostly unusable\". perhaps you could not configure your system according to your liking.\n\nTwo days ago I installed DesktopBSD, it's good, feels fast and does automatic configuration of devices, much like windows xp, or Linspire. Linux distro need to learn from BSD the good features of BSD.\n\nto get new software for DesktopBSD \nrun \n\"xhost +\" as user\n\"kcmshell dbsdpackages\" as root (su then this command)\n\nAnyways, KDE 3.4.2 feels good on BSD too :) GNU/Linux and BSD rocks!"
    author: "fast_rizwaan"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "It was a quote from the article.  Take it up with the interviewee. "
    author: "Dan"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "\"xhost +\" is a pretty stupid thing to do. If you must, then at least do \"xhost +localhost\"..."
    author: "NoneOfYourBusiness"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "Don't get me wrong. I love Linux and I would never turn back to Windows now.\nThe fact is that one or two years ago, as I firstly tried to install Linux on a desktop machine, the problems that arised were so many and so hard, they led me to aandon the idea of adopting Linux.\nI am speaking about missing drivers, overcomplicated installation procedures, patching and recompilations of kernels only to have support for a mouse...\nSix months ago I succeeded in installing Debian, following one of the many howtos that people generously put on the net. From then on, I have never abandoned Linux anymore and I started loving it.\nHow usable is an operating system which requires you to be a computer scientist to install? I'm not talking about myself, obviously. I am a computer scientist, for instance. I'm talking about \"normal\" people, like my sister, my mother and so on.\nThis was not meant as a criticism. It was meant, instead, as an enormous compliment to those who brought Linux to the point of being installable by \"normal\" people.\nThe thing that surprised me more was the pace of development. In a couple of months I have seen a huge increase in usability in Linux, especially regarding the installation procedure.\nBye"
    author: "rcappuccio"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "\"How usable is an operating system which requires you to be a computer scientist to install?\"\n\nWell, I use linux since 1998, and never needed any degree in science to install it :)\nI guess the trick is that you need to have a Linux compatible pc. \n\nI wouldn't let my mother install Linux though, but that also counts for Windows.\n\nUsing Linux can be easier then using Windows.\n\nMy mom uses SUSE 9.2 at the moment, and she has less trouble using it then she did have with Windows 95\n"
    author: "rinse"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "Mandrake, RedHat, Suse, Linspire... had fully graphical, much-easier-than-winxp installation for years now. Just basic knowledge of computer inner workings was/is required in order to install those distros on your machine, not to mention LiveCDs and their hardware compatibility and I won't even try to speculate how you concluded that Linux is \"terribly unstable\"... mostly unusable can be accepted from someone who's a winpoweruser, but unstable... ANd you're only right about the speed of development - it is amazing..."
    author: "Petar"
  - subject: "Re: ahem"
    date: 2005-08-21
    body: "I don't understand why people is so interested in installers. That process is just made once. I have several years old installations. So I finally don't see that kind of improvements. ( By the way, last week I had to install openSuse and well, it sucks a bit how slow is. Maybe I'm used to Fast and Clean Debian installs.. ).\n\nAnother thing, is that neither your sister nor mother don't know how to install Windows, or Linux or BSD or whatever.. They don't mind that.\n\nBut it's true that at the moment Linux has some usability problems. Work in progress :)\n\nCu"
    author: "KikoV"
  - subject: "Re: ahem"
    date: 2005-08-22
    body: "KikoV: \"I don't understand why people is so interested in installers.\"\n\nThe installation is often the first impression. When the installation demands a lot of the user, the user might be fed up and go back where it was.\n\nI exprerienced that too. 1,5 years ago I was nowhere, regarding KDE or GNOME. I had the choice: which one. I tried GNOME first. But I couldn't manage to install that beast. Dependency after dependency after dependency. (I was using Linux from Scratch those days). I couldn't manage to install it, so I decided to try KDE. It installed much easier, so I decided to stay with KDE. Pure installation. If GNOME's installation is/was easier, I would probably be a GNOME contributor by now.\n\nPoint is: installation *is* important."
    author: "Bram Schoenmakers"
  - subject: "Re: ahem"
    date: 2005-08-22
    body: "Take very, very good note of this entry.  *This* is how we get more contributors and how we risk getting fewer contributors (among other ways, of course)."
    author: "Inge Wallin"
  - subject: "Re: ahem"
    date: 2005-08-23
    body: "People base their contributions primarily on which project is easier to install in Linux from Scratch? :)"
    author: "Dolio"
  - subject: "Re: ahem"
    date: 2005-08-23
    body: "No, I did not really seek something to contribute :)\nBut from a KDE user I turned into a KDE contributor. If the criteria you mentioned applies, I could also have been a kernel developer, that's not so difficult to install, even with LFS. But kernel stuff is kinda boring, if you ask me :)"
    author: "Bram Schoenmakers"
  - subject: "Re: ahem"
    date: 2005-08-24
    body: "I don't know. The gigantic Reiser flame wars seem pretty intense to me. :D"
    author: "Dolio"
  - subject: "Re: ahem"
    date: 2005-08-22
    body: "Heheh, Gnome on LFS. Indeed, it's still as painful.\nOnly last week gave up trying to install some gnome app on one of my LFS boxes."
    author: "Ivor Hewitt"
  - subject: "Re: ahem"
    date: 2005-08-24
    body: "I feel sorry for packagers. In case some GNOME guy reads with us: put GNOME 3.0 in one tarball, and let the user do just extract, ./configure, make, make install :)"
    author: "Bram Schoenmakers"
  - subject: "Re: ahem"
    date: 2005-09-06
    body: "Ever heard of GARNOME? :)"
    author: "Theblues"
---
<a href="http://www.tuxmachines.org/">Tuxmachines.org</a> has published <a href="http://www.tuxmachines.org/node/2253">an interview with Roberto Cappuccio</a>. Roberto is a developer of the <a href="http://rcappuccio.altervista.org/">Kat Desktop Search Environment</a> which is an open source framework designed to allow KDE applications to index and retrieve files.



<!--break-->
