---
title: "The Importance of KOffice"
date:    2005-01-17
authors:
  - "rlangerhorst"
slug:    importance-koffice
comments:
  - subject: "Scripting in Kexi."
    date: 2005-01-17
    body: "In many applications logic is added to widgets and controls depending on selection. So what language will be used in adding business logic in Kexi?\n\nCb.. "
    author: "kb"
  - subject: "Re: Scripting in Kexi."
    date: 2005-01-17
    body: "Hi kb,\n\nI am working right now on this issue. There exists some code to access the KexiDB-functionality from within Python in koffice/kexi/scripting/python/*. Target will be a cross-scripting interface (named Kross  :-) that enables integration of different scripting backends. Please see http://www.kexi-project.org/wiki/wikiview/index.php?Scripting\nIf you like to get more infos or even spend some help just /join #kexi in irc."
    author: "Sebastian Sauer"
  - subject: "Re: Scripting in Kexi."
    date: 2005-01-18
    body: "I hope you'll add KScript support into Kross soon so the difference isn't all too huge."
    author: "ac"
  - subject: "Re: Scripting in Kexi."
    date: 2005-01-18
    body: "Hi ac,\n\nAny idear is welcome. I wrote your suggestion into our Wiki. Thank you."
    author: "Sebastian Sauer"
  - subject: "koffice is a copy."
    date: 2005-01-17
    body: "copying bad concepts sucks. change the way of editing documents. i for myself dont want another word. i dont like the concept of it. \n\nand by the way - the complete koffice is just a complete copy of msoffice and - dor krita adobe...\n\nmod me as troll if you want :(...\n\n"
    author: "undo"
  - subject: "Re: koffice is a copy."
    date: 2005-01-17
    body: "koffice is completely optional, its just something familiar for windows users"
    author: "Matthew Robinson"
  - subject: "Re: koffice is a copy."
    date: 2005-01-17
    body: ">i for myself dont want another word\nSince KWord is frame based, the basic concept is very different from programs like MS-Word and the like. Since it sounds like you don't know anything about KWord, it quite possible you are indeed a troll. But please enlighten us all, what is a good way to edit documents? Which concept should koffice copy?\n"
    author: "Morty"
  - subject: "Re: koffice is a copy."
    date: 2005-01-17
    body: "something more the scribus way . more desktop publishing orientated."
    author: "undo"
  - subject: "Re: koffice is a copy."
    date: 2005-01-17
    body: "try Koffice. you can do simple desktop publishing. scribus is more advanced, but it also seems more time has been put into scribus... so if you wanna help developing on Koffice, please, join. maybe you can even port some code from scribus to Koffice, as both are QT based..."
    author: "superstoned"
  - subject: "Re: koffice is a copy."
    date: 2005-01-17
    body: "Someting like hmmm... frames?\nHave you actually used KWord or just looked at the screenshots?\n\nI don't think going more in the direction of DTP than KWord or FrameMaker alredy does, are such a good idea. Whith DTP applications like Scribus, PageMaker and others the workflow are to much concentrated around making the layout, taking focus away from the writing.   "
    author: "Morty"
  - subject: "Re: koffice is a copy."
    date: 2005-01-18
    body: "Exactly the opposite is what should be done if anything.\n\nEver heard of separation of content and presentation?\n\nA DTP app is a DTP app and a word processor is a word processor. There's a reason for that."
    author: "Robert"
  - subject: "Calamus (for Atari ST)"
    date: 2005-01-18
    body: "Perhaps something like Calamus would be good? It was a DTP app that did a nice separation (at least for my taste) between content and presentation. It had a little word...mmm...editor (not a wordprocessor I think) good enought to write any text"
    author: "Miguel Angel Lopez"
  - subject: "Re: koffice is a copy."
    date: 2005-01-17
    body: "To some extent, all office suites are copies of MS Office at the moment, because interoperability with windows is still a key issue for other platforms. However, KOffice is better positions than most to come up with radical new ideas. If you've got some of these, why not contribute to KOffice? OpenOffice can follow the path of being the cross platfrom relyable MS Office clone, allowing KOffice to continue the KDE tradition of integrating a variety of unique new features in interesting ways.\n\nYou're only a troll if your comments aren't intended to constructive. Why not try describing some aspects of your ideal office suite in more detail."
    author: "Craig Ambrose"
  - subject: "Re: koffice is a copy."
    date: 2005-01-22
    body: "Excuse me!\n\nWordPerfect is NOT a copy of M$ Word.\n\nAnd, I believe that Quattro Pro is a copy of Lotus 123 NOT M$ Excel.\n\n-- \nJET"
    author: "James Richard Tyrer"
  - subject: "KOffice doesn't copy anything, why it sucks"
    date: 2005-01-22
    body: "Get your facts straight. Since when does KOffice look and feel like Word? This is why it is so clunky and hard to use."
    author: "Turd Ferguson"
  - subject: "OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-17
    body: "KDE integration of OpenOffice.org is better...\n\nit would be better to add frame and other koffice features to OpenOffice.org and merging it with koffice for a better Office suite.\n\nI find OpenOffice.org with KDE integration more useful than KOffice alone or just OpenOffice.org.\n\nSo, Please instead of reinventing the wheel for koffice, add KDE components to OpenOffice.org and make it useable bothways, as openoffice.org has more developers and userbase.\n\nOpenOffice + Koffice + KDE fancy stuff (DCOP, embedding etc). will be the best thing for KDE and Unix."
    author: "Fast_Rizwaan"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-17
    body: "i totally agree!\nopenoffice 2 will come with a fully functional qt gui(there already is a qt port ooo v.1.1.3 i think...), so why do i need koffice?"
    author: "Hannes Hauswedell"
  - subject: "Several Reasons"
    date: 2005-01-17
    body: "1. KOffice is orders of magnitude faster than OpenOffice, even if you use the Ximian optimized builds. I can't claim to know exactly why this is true, if it is bloat or poor design or what, but it is undeniable.\n\n2. OpenOffice is never going to be as integrated with KDE as KOffice. Even if it does get QT and KIO support, it will never use DCOP, or KScriptInterface, or KPart embedding.\n\n3. Choice. There is nothing wrong with more options. So many people wrongly assume that \"you are wasting time working on X when Y is already here, why not improve that\", these people do not understand open source. The people working on KOffice would liekly never work on Open Office, for one reason or another. So them working on KOffice is not a \"waste\" or anything else. They are just providing more options, some people will use it, some won't. At the end of the day, nothing is ever lost from their efforts, and there is in fact a huge gain, because even if no one but the developers use KOffice, there is now a huge new repository of great codebase to draw on for ideas.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Several Reasons"
    date: 2005-01-17
    body: "Full ACK to parent.\nBesides:\nIMHO it often shows if an app has been developed commercially first\nand then released as Open Source later. The strange connection of\nOpenOffice (.org - yes I know - grrr) with Java, the complicated building process, etc. are some things which possibly wouldn't be like that\nif OpenOffice had been programmed as Open Source from scratch.\nThe start-up time of Open Office is even with the 2.0 version totally \nunacceptable. This might of course be because Open Office cannot\nreuse the KDE components already loaded in memory but has to bring\nits own for lots of things. This fact does not help the user in any\nway though. I'm using OpenOffice for lots of things anyway, mainly\nbecause KOffice's table support is still in its infancy.\nBetter Microsoft Word import isn't really a reason for me to use\nOpenOffice because it's still so bad even in recent 2.0 beta versions\nthat it's not really an option for me. In this case \nI'm using M$ Word via wine which works like a charm. However, YMMV. \n"
    author: "Martin"
  - subject: "Re: developer and end-users - not the same person"
    date: 2005-01-20
    body: "I heard Microsoft has something like 10000 employees. Most of them are not developers. Yet Microsoft has something like or more than 90% market share when it comes to word processors, spreadsheets and so on. Their platform is unfree in every sense of the word, yet end-users \"love\" them. End-users get to do what they want, most of the time.\n\nNow, we want to address the issue of proprietary formats and unfree software. OpenOffice.org and KOffice are our horses and MS Office way ahead, closing on the finish line. Our horses came late to the race, and it's amazing how much they are gaining. Will it be enough? The race is long, so there are pit-stops, where the horses are fed. Now, do we have enough food aka developers for two horses to sway the race-track audience, or should we ask for more food to one of the two? Not on a permanent basis, but to satisfy end-users? I think it would be a good idea for the project managers of both projects to come together and discuss this. \n\n"
    author: "Anonymous"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-17
    body: "well, as told before, openoffice is a huge memory monster.\nkoffice is also more consistent in interface and behavior when using kde.\nother last..\n\npeople can work on what they want.\nkoffice surely isn't nothing\nI used it some time and found it sooo much nicer then openoffice.\nonly trouble for me is that it can't read msoffice files the way openoffice can.\n\nbut I am sure koffice can have a good future."
    author: "Mark Hannessen"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-18
    body: "Actually KOffice read (and rendered) a MS Word document waaaay better than OO.o and is from what I've tried, KOffice rendered better MS Word documents. My problem is it doesn't export to .doc files. So who am I to send my documents? Myself. Interoperability with MS Office is the key to the next succesfull office suite (just like interoperability with word perfect was for ms office back then). \nThe second problem I cam across was cross-document pasting (which openoffice already has): copy table from tabular program, paste into word editor program, then later edit the tab;e just as I would in the tabular program."
    author: "Adrian"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-18
    body: "Ever tried to send as RTF file?\nWhat do you think MS uses as file format for backward compatibility when you save in old versions (and are lucky if it doesn't crash)?\n\nIf you need, then just change the extension to .doc. -> Save and send -> Done"
    author: "tempa"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-17
    body: "Who controls the development process for openoffice?\n\nHow long would it take to become familiar enough with the codebase to make substantial changes compared to starting with KOffice?\n\nAre you serious when you suggest that there should be a monopoly in free office type software?\n\nThere is a good reason why development has continued on Koffice and Abiword and Gnumeric and the other office type apps.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-17
    body: "The KOffice developers have already put six or seven years of their lives into getting KOffice up to it's current state, do you really think they'd just throw all of that work away and start hacking on OpenOffice?"
    author: "Greg"
  - subject: "Re: OpenOffice.org + KOffice = KDE OFFice :)"
    date: 2005-01-17
    body: "There's been a fair bit of churn in the developer pool... Reginald Stadlbauer doesn't do any KOffice development anymore, and Krita is on its fourth lease of oxygen, I mean maintainership. And so on and so forth."
    author: "Boudewijn Rempt"
  - subject: "Hold your horses there, pard'ner."
    date: 2005-01-18
    body: "I have found that koffice is exactly what I need. It is useable and it is quick, light and I find intuitive. OO while more feature rich and MS Office complient, and strkes me as heading towards clunky. \n\nMy wife, OTH uses OO and scribus. She likes both depending on what she is trying to do. But she was comfortable with Pagemaker, Quark, FrameMaker, and MS Office, so that is not too surprising.\n\nBut do not be in a hurry to kill this. It strikes me the same way that others have pushed to kill konqi for web browsing (and want to push mozilla in).\nBut you are more than welcome to contribute to KDE/OO integration. "
    author: "a.c."
  - subject: "KOffice presentations"
    date: 2005-01-17
    body: "FYI, I'm doing a presentation mainly on possibilities of KOffice automation and KDE integration at the http://www.itnt.at (Vienna) on the 17th of Feb.\n\nAnd if nothing stops me I'll be at FOSDEM and doing a similar talk on KOffice, particularly on automation and integration (DCOP,...), focusing on developing such automation (with Kommander, etc.). This will be held in the KDE Developers Room - see http://www.kde.me.uk/index.php?page=fosdem-2005 for details.\n\nI feel like this is really a significant potential of KOffice that needs a bit of awareness.\n\nCheers,\nRaphael"
    author: "Raphael Langerhorst"
  - subject: "Some thoughts"
    date: 2005-01-17
    body: "Firstly, koffice's modifications to the way kparts work should be added to the \"real\" kparts as fast as practical, because they're really useful. It's got to the stage that new applications tend to provide koffice kparts rather than \"normal\" ones. While this is not really a problem, IMO it shows that koffice kparts are simply what kparts should be.\nSecondly, KSpread needs a lot of work. It's so far behind Excel, Gnumeric and OOo Spreadsheet in terms of formula support. Would it be practical to use the engine from one of those? If not, someone code in more functions, please, because at the moment it's really not useable for advanced spreadsheet stuff.\nThirdly, Kexi could really be the 'k'iller app if it stabilises and is reintegrated. There is nothing like it on linux, and the lack of an Access replacement is a real problem for some people who would otherwise be willing to move.\nAll in all, everyone program more for koffice."
    author: "mikeyd"
  - subject: "Re: Some thoughts"
    date: 2005-01-17
    body: "To me your advanced spreadsheet stuff sounds like some kind of mythical creature, as I have actually never seen any. On the other hand if you describe which functions you need for different tasks to the KSpread developers I am sure developing those functions will get a higher priority. Since real world user cases from real world users always trumps the vague \"since other does\" statements.\n\nI have nothing against Kexi, it actually looks like it's going to become a great application. The fact is Linux has actually had an Access replacement for years, even cross platform, it's called Rekall. Sadly I don't think an replacement solves that particular problem, it's more like need for a clone to let them keep on running those Access programs. \n\nIn my opinion database development tools like Access or Kexi does not belong in a office package. I suspect the main reason for Access inclusion in MS-Office was to help create a vendor lock-in for MicroSoft. And I have to admit it was a very successful move. "
    author: "Morty"
  - subject: "Re: Some thoughts"
    date: 2005-01-18
    body: "Well, it looks like Poisson distributions have been added since last time I looked, which is good. But in order to port my Excel spreadsheets I still need table lookups (main problem), and also the SUMIF function (like COUNTIF, but gives a sum of cells rather than just the number of cells matching a criterion). But the fact that another couple of things I would have listed here are actually present in the current version is an encouraging sign."
    author: "mikeyd"
  - subject: "Re: Some thoughts"
    date: 2005-01-18
    body: "I also think KSpread lacks features.\n\nWhat I really miss is better chart support, like in LabPlot or QtiPlot. Everytime I want to use advanced charts with regression curves or custom error bars, I have to use Excel. Even OpenOffice does not support that.\n\nOtherwise, I like the KOffice suite, especially the speed and KDE integration compared to OpenOffice."
    author: "TMG"
  - subject: "KOffice Rocks!"
    date: 2005-01-17
    body: "//Begin rant\n\nI guess I'm the only poster who actually thinks KOffice is a good thing. I believe KOffice can integrate with KDE better than OpenOffice.org will ever be able to. KOffice is also a very high-performance suite. OO.org is sluggish. Also, have you ever thought that KDE devs might actually LIKE working on KOffice and not OO.org?! Just because you want them to work on OO.org integration doesn't mean THEY want to. If you want to have OO.org integration, why don't you code it yourself? \n\n//End rant"
    author: "secretfire"
  - subject: "Re: KOffice Rocks!"
    date: 2005-01-17
    body: "I think I give the highest compliment to KOffice just about every day:\n\nI use it.\n\nIt has worked fine for me for many years, especially KWord."
    author: "Evan \"JabberWokky\" E."
  - subject: "Have a look at OO 2.0 alphas"
    date: 2005-01-18
    body: "OO 2 will rock.\n\nKOffice is an alternative and when OO and Koffice share the same file formats You can use OO filters for import and provide Koffice as an alternative editing program."
    author: "gerd"
  - subject: "Go KOffice!"
    date: 2005-01-17
    body: "I don't use KOffice much myself, since I'm not really an office software user, still I love KDE and love to see something like this under development. I do have the latest KOffice installed though, and always install new releases and give a try to every package just to see its state. Most apps open in a blink and seem quite stable, at least for the small jobs I've used them for.\n\nI'm really impatient about Krita this time, hope it becomes as good as the GIMP with time. Also, I don't know if Karbon14 is still mantained, but having a full-featured vector graphics app for KDE surely wouldn't hurt.\n\nAll in all I think KOffice is already quite good but most importantly it has great potential to become a top office suite. Let's see what it is like by KDE 4 with all that Qt4 has to offer :)\n\nBig thanks to all the great developers!"
    author: "Pedro Fayoll"
  - subject: "Re: Go KOffice!"
    date: 2005-01-17
    body: "There is a binary \"release\" of Krita you can just download, unpackage and attempt to run -- but it will interfere with your existing KOffice installation.\n\nAnd I, for one, have a hard time doing without KOffice. There is no free office suite with quite the breadth of KOffice. It has not just a word processor and a presentation app, like Apple's new iWorks, nor does it stop at adding a spreadsheet, vector drawing tool and database app, like OpenOffice, it also throws in a really great diagram editor (I use Kivio a lot, probably more than any other KOffice app), a pixel editor, a report tool and a planner.\n\nI only wish that Quanta and Umbrello would be KOffice parts, too...\n\nOn the other hand, KOffice has had a hard time. Moving to an OASIS file format was (and is) difficult, time-consuming and boring. And it takes developer time from making the apps more featureful, more stable and in general more polished. That's why I didn't spend all that much time dreaming up an OASIS spec for layered raster images -- it would have delayed Krita development with another year, and that's a conservative estimate, because I don't know whether I would have continued hacking on Krita in that case. There are also basic limitations in Qt that Scribus only could overcome through the use of an external canvas, and those hurt KWord development.\n\nAnd there's just not enough people interested in developing office software. OpenOffice is not so much a community project as the old StarOffice office (mostly staffed with new people nowadays, I hear) paid by Sun to work on the incredibly tangled codebase of StarOffice. I'm not sure how many different people have contributed to KOffice in 2004, but I'm fairly sure that I'm not the only who wouldn't mind being paid full-time to work on KOffice. It's a pity that the cooperation with Shawn Gordon from theKompany has petered out..."
    author: "Boudewijn"
  - subject: "koffice should get inspired by iWork"
    date: 2005-01-17
    body: "Let me start with saying that KOffice is really great! However, it can always be better.. :) \n\nI think it would be really nice with the same kind of integration between koffice and other application, as apple has between iwork and ilife. Apples solution seemes really nice thanks to the good integration. Watch the keynote speech (from apples webpage) and see for yourselves! :)\n\n"
    author: "Magnus"
  - subject: "Re: koffice should get inspired by iWork"
    date: 2005-01-17
    body: "# apt-get install iwork\nReading Package Lists... Done\nBuilding Dependency Tree... Done\nE: Couldn't find package iwork"
    author: "koos"
  - subject: "Re: koffice should get inspired by iWork"
    date: 2005-01-18
    body: "it's spelled iWork :)"
    author: "guy"
  - subject: "Reuse of OOo file filters?"
    date: 2005-01-17
    body: "As far as I can see, a main reason that people stick with OpenOffice.org is its superior .doc file support. I wonder if it's a lot of work to port the OpenOffice .doc filter to KOffice (maybe the other ones are as useful too?) in order to enable KOffice users to abandon OOo. If I remember right, there's even a command line tool in OOo, so if this is not too much work to separate it should bring more users (and maybe also developers) to KOffice.\n\nOf course, it would be better if the OpenOffice.org developers would split their file support into a separate library which would enable other Open Source office suites to keep the same high standard for importing files and work together on file support. Well, I don't want to know how long I have to wait for that.\n\nBut even without library, there's still the same destination format (OASIS) that both can use, so it can't be that difficult to import some \"foreign code\", or is it?"
    author: "Jakob Petsovits"
  - subject: "Re: Reuse of OOo file filters?"
    date: 2005-01-17
    body: "I think I once read that the filter code is really hard to comprehend (- and has most of its comments in german?!??)\n\nBut still, there are lots of competent german KDE-people...so maybe someone will do it.\n\nI guess that the original post was sort of a call for participation."
    author: "Anders"
  - subject: "Re: Reuse of OOo file filters?"
    date: 2005-01-18
    body: "Well...\nOpenOffice.org was once a german product, you know?"
    author: "SegFault II."
  - subject: "Re: Reuse of OOo file filters?"
    date: 2005-01-19
    body: "It still is.\n\nMost OOo developers are in Germany and secondly Ireland. And most users are probably in Europe."
    author: "Daniel Carrera"
  - subject: "Re: Reuse of OOo file filters?"
    date: 2005-01-18
    body: "As this is a FAQ, I am pointing to one of my old answers about the problem:\nhttp://lists.kde.org/?t=104164162100003&r=1&w=2\n\nI would not mind if someobody would prove that I am wrong but I doubt that it will happen so soon, especially as this OOo filter discussion is now years old.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "When will KDE get it?"
    date: 2005-01-18
    body: "Perception is reality.\n\nCurrently KDE is not marketing itself well and it is not creating the momentum such an excellent piece of software should have.\n\nThe website does not look attractive enough, there is not enough press and the fund raising efforts are pathetic. Take a page from GNOME on this, they are getting the corporate support and $$$ at a greater rate than KDE. Or are they? It doesn't really matter because that is the perception and business executives will make decisions baesd on these perceptions.\n\n1. The GNOME.org website is easy to use, clean and with a graphically intensive layout. KDE looks like any other lame and arid website. It doesn't please the senses and it doesn't entice new users. The fact that most KDE websites such as for Konqueror and Kmail are either out of date or terrible unappealing does not help either.\n\n2. KDE needs to better show off its corporate support and features as GNOME has. It needs to be obvious without searching past the front page.\n\n3. Fundraising sucks to. There was actually a wish on this http://bugs.kde.org/show_bug.cgi?id=63868 which got lots of votes. It proposed a way to make KDE more attractive to donate to. However, the developers kust didn't care about this issue.\n\nTo quote Stephan Binner:\n\n\"Nobody on kde-www sees a reason why this should stay open.\"\n\nAnd then it was marked as fixed, all the while, nothing changed.\n\nMeanwhile, GNOME has 1/3 of their front page dedicated to the GNOME Foundation and approximately half of the page is comprised of a graphic asking people to help out and support the project. They also have distinguishing names based on the amount you donate and also gifts. They make sure donors feel happy and people have an incentive to donate.\n\nSpreadFirefox also has very solid marketing efforts and as 20 million people will tell you, it's working.\n\nKDE needs to understand that half of what makes a successful product is marketing. (which creates momentum and acceptance) Please don't let KDE become the next great technology that never really saw the light.\n\nBTW: Please don't ask me to do my part and help. I have. Saying taht \"shut the hell up and do something\" type of remark won't solve the problem."
    author: "Tom"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Why doesn't KDE have an easily accessable donations page?  I mean it cannot be alot of work and it it brings in enough money to pay for some development costs its gotta be worth it.  I would like to see a commercial support page too.  I know I would donate money.  Heck you could put a \"donate to kde\" option on the darn help menu (something stupidly simple to do.)\n\nBobby"
    author: "brockers"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "http://www.kde.org/support/support.php\nhttp://www.kde.org/support/donations.php\n\nAs to the original poster, I think the GNOME homepage is a complete joke in terms of a website for a Free Software project."
    author: "ac"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "I newer said they didn't exist. I said they weren't prominent enough and are not implemented well. There is never a sense of immediecy like there is on gnome.org. When I look at GNOME's website I feel that they need me to donate, when I look at KDE's support page it only seems like it would be nice. That's one stark difference and it's stopping KDE from getting more $$$. (flashy graphics also help)\n\nAnyway, what is so wrong with gnome.org? It brought in more donations than KDE did and a lot of corporate sponsors too. (ie: Sun, Novell, and Redhat) \n\nAdapt."
    author: "Tom"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "> what is so wrong with gnome.org? It brought in more donations than KDE did and a lot of corporate sponsors\n\nCorporate sponsors because of gnome.org homepage? Get serious."
    author: "Anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "I think the GNOME website is incredibly ugly, and I dont particularly like the \"give us money\" attitude. I think that people are more likely to donate if requests are made with greater subtlety.\n\nThe KDE website looks nice, it's vast volume of content is made accessible via an effective navigational interface, and it provides a hell of a lot more useful information than the Gnome web site does. I do ocassionally get frustrated with the fact that such large portions of the KDE website are out of date.\n\nGnome corporate sponsorship has very little to do with the website. KDE suffers from a number of very peculiar licensing issues, and until they are dealt with, I cant imagine that any major company would want to risk potential lawsuits. There is a hell of a lot of senseless litigation going on right now, and KDE is an easy target."
    author: "anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Huh?\n\nPotential lawsuits like what? Easy target in what way? If this is about Trolltech in some way, maybe you could be specific or just lay off the FUD.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-20
    body: "qpl/gpl conflicts. Unless i'm sorely mistaken, this has been an ongoing problem for years now."
    author: "anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-20
    body: "Yes, you're sorely mistaken."
    author: "Boudewijn Rempt"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "sure ... lets improve the fundraising efforts, brainstorm a lot, and develop an action plan. For startes. Some data would be nice to know, like donations in 2004 versus a target for 2005 (if there is a target :-)."
    author: "Stefan"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Why are some people so eager to obtrude their 'help' for a topic when no help was requested? Why not help to test KDE 3.4 Beta 1 like requested for example? Why do you think that some more money (of course not yours, you want others to pay) will help KDE significantly?"
    author: "Anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-19
    body: "Don't be so quick to judge. I am testing Beta 1 and I have donated to KDE."
    author: "Tom"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-19
    body: "Well, I send my 50 Euros some weeks ago.\n\nMaybe KDE needs testers/coders/artists more than people who just give some cash.\n\nPerhaps the KDE-people really don't need money anyway? That's OK. For me it shows in low-key fundraining efforts, e.g. I received no thank-you mail (standard in fundraising if you want to cultivate your one-time donors into regular givers).  "
    author: "Stefan"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "What has your flaming to do with the KOffice story?\n\n> GNOME on this, they are getting the corporate support and $$$ at a greater rate than KDE.\n\nAnd what do they do with that $$$? They burn over 60.000 Dollar a year for \"administrative tasks\" (one full-time employee). Costs which KDE doesn't have currently because of volunteers.\n\n> 1. The GNOME.org website is easy to use, clean and with a graphically intensive layout.\n\nThe GNOME.org website is four fifth about \"Buy! Spend us money!\" at the moment. Not friendly.\n\nWhat do you know about KDE and KDE e.V.? If there would be a real need for much more money/a shortage there would be naturally other/more fund raising efforts."
    author: "Anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "I hear you saying that there is no real need for funding in kde but,earlier in this thread,there was a message from a koffice developer who said that it certainly would help and if some funding would bring some new features and more stability in koffice I would say there is real need for funding in kde.  "
    author: "larry"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "> earlier in this thread,there was a message from a koffice developer\n\nDunno to what posting you're referring to.\n\n> would help and if some funding would bring some new features and more stability in koffice I would say there is real need for funding in kde\n\nSure, fund/support KOffice developers. But KDE e.V. doesn't employ developers and as the original poster complained about KDE e.V.'s fundraising..."
    author: "Anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Why doesn't KDE e.V. do this?\nThis works very well for Quanta (AFAIK).\nIf I recall correctly Sebastian Trueg, the author of K3B was also payed by some well known distributor to work on K3B.\nI think it would be great if there would be more payed KDE developers.\nOr maybe documentation writers, or QA-people or whatever you can imagine and KDE could use.\n\nJust my 0.02 EUR"
    author: "SegFault II."
  - subject: "Re: When will KDE get it?"
    date: 2005-01-19
    body: "> Why doesn't KDE e.V. do this?\n\nHow would one decide which developer to hire and on what to work?"
    author: "Anonymous"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-20
    body: "Who would decide this?\nI don't know.\nI think this could be done by KDE representatives or core-developers.\n\nWhat to work on?\nBugs.kde.org: Most wanted features, Most hated bugs, etc."
    author: "SegFault II."
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Boudewijn at thread \"Go Koffice\":\n\n\"I'm not the only who wouldn't mind being paid full-time to work on KOffice.\"\n\n> Sure, fund/support KOffice developers. But KDE e.V. doesn't employ developers and as the original poster complained about KDE e.V.'s fundraising...\n\nOk. Maybe I should read the originals bit more thouroughly. I just thought that you were saying there is no real need for funding.\n\nMaybe it is just so that people working with kde do rely on more delicate ways to get funding. It is unfortunate if it leads to less funding for kde -projects."
    author: "larry"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Personally, I think GNOME's half-page 'give us money!!!11' ad is a bit pathetic. A small-to-medium sized graphic in the corner (slightly smaller than the 'related links' on this page) should be about right. Their page -is- a lot more aesthetically pleasing and professional looking, though."
    author: "Illissius"
  - subject: "Re: When will KDE get it?"
    date: 2005-01-18
    body: "Horses for courses, I guess. I've just been to both sites:\n\n* after recoiling in horror at the BIG SHOUTY TEXT which jumps straight at you from the GNOME site, it's hard to find anything without realising the small text at the top *isn't* part of the logo, it's supposed to be the navigation. Meanwhile, due to the large \"give us cash!\" graphic, all the other stuff requires scrolling to read\n\n* in contrast, I find the KDE one quite pleasant - all the links to important places are nicely organised on the left, where you start reading; you get a summary of what it is straight away, rather than a begging image; you can read the latest news without scrolling. On the right - where I still found it quite easily - is the search box.\n\nI'm not saying either of us is definitively right, but you might want to be a bit less free with the absolutes next time you talk about the page. ;-)"
    author: "Paul Walker"
  - subject: "Pls. use a good timing for spreading KOffice"
    date: 2005-01-18
    body: "There are two deadly errors for marketing open source software:\n1. Not to market it at all.\n2. To tell it is production ready when it is not.\n\nKWord will be a productive tool as soon as the kerning problem is solved (QT4!). I am writing mainly simple office letters. Still the print result of KWord is too ugly to be sent to clients. As soon as this is fixed KWord will be my primary text processor. Occasional crashes will not stop me from using it. \nPlease be careful with pushing the use of KOffice today because people might not give it a second chance. \nKOffice has a bright future. The vehicle which will bring a critical mass of users however is a production ready KWord. I am convinced that we will have this in a couple of months. This should be the time to sprad KOffice. \n\n\n"
    author: "Roland Wolf"
  - subject: "Re: Pls. use a good timing for spreading KOffice"
    date: 2005-01-19
    body: "It is rather hard to delay KOffice until it would be ready (whatever that would mean.)\n\nIf we do not release anything for years, users will get bored too and would not come again either (not counting that we would not get any new developer either).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Pls. use a good timing for spreading KOffice"
    date: 2005-01-20
    body: "You are right of course, the release cycle should be maintained. \nMy point was: Do not offer it as a KWord/OpenOffice replacement today because you will be able to do it very soon with a much better credibility. \nI can't wait to get KOffice on QT4. \n\n"
    author: "Roland Wolf"
  - subject: "kopenoffice ala firefox mozilla"
    date: 2005-01-20
    body: "hey,\n\nkopenoffice is out there and already working, even a kpart exists. interface is already kde native (via the OOo native widget framework). i think the best bet would be a stripped down kopenoffice ala firefox for mozilla where the things can be un-bloated.\n\nmeanwhile a better effort could be put on integrating current koffice offerings into OOo. that way the two projects will integrate and probably unite/merge.\n\njust imagine if OOo could have the already working office modules that OOo has no analogue (especially kexy).\n\nkword however as a mature and stable lightweight wordprocessor can still be kept as just a light weight processor for simple docs like M$ still keeps MS Write but offers the advanced MS Word.\n\nboth projects will benefit this way, on one hand all improvements to all modules available to both worlds, and on another will keep kde office apps portable to other platforms and even more platform and toolkit independent.\n\ni haven't checked in details the koffice parts code, but i beleive that if the 'design' is good it will as easy as OOo was ported to kde the koffice kparts to be ported to OOo architecture.\n\njust a suggestion"
    author: "Anton Velev"
  - subject: "The importance of ports"
    date: 2005-01-21
    body: "If kOffice is to suceed, it MUST work on multiple platforms. I mean sure, you can get it to work on windows, or mac OS, but it's a hack. It's slow and it's weird. I for one stopped using koffice as I switched to mac not because iWork was better, but because koffice is not avaiable for that platform. Port it and it will spread. I think it has the potential, koffice is a much better piece of software than OO.org, so please make it available for those of us who don't use Unix/X11."
    author: "Adam"
  - subject: "Re: The importance of ports"
    date: 2005-01-24
    body: "we dont need to port this.\n\nfirst you will run more than one OS when virtualisation comes in processor cores. And we need something to embed KParts over Network perhaps in IE.\n\nthen you never have to port any apps."
    author: "assfd"
  - subject: "KOffice sucks."
    date: 2005-01-22
    body: "Development should quit on it since it is so far behind Abiword/Gnumeric and OppenOffice.org\n\nKDE is the best desktop environment for Unix, but not its office suite. \n\nTake the rules for instance. Very ugly. \n\nAlso, document rendering is the worst of all of the office suites for linux.\n\nNobody cares about the OASIS Open Document Format, so don't waste your time supporting it.\n\nUsually I am one of the biggest KDE supporters around, but I think that KOffice is an embarrassment to the project and developers should quit wasting their time on it."
    author: "Turd Ferguson"
  - subject: "Re: KOffice sucks."
    date: 2005-01-22
    body: "> Nobody cares about the OASIS Open Document Format\n\nNobody? Like the European Union? Or OpenOffice.org?"
    author: "Anonymous"
  - subject: "Re: KOffice sucks."
    date: 2005-01-25
    body: "OASIS OpenDocument is the single most important step in the spread of FOSS. It is unique in that:\n\n1) It cuts right to the core of the issue: Vendor lock-in. The format is designed and intended to eventually break the dominance of .doc. And the \".doc monopoly\" is the *real* monopoly.\n\n2) It has a real chance of working:\n\n* It is technonologically capable of meeting all the demands you can throw at it. NO OTHER open format can be used for nearly as much as OpenDocument. From the most complex documments, complex layout, databases, mathematical formula, vector graphics, embeded applications, and any combination of those.\n\n* It is supported by OpenOffice.org, KOffice, OASIS, the European Union, ISO, and supposedly will be supported by IBM Workplace. The European Union has \"asked\" Microsoft to make Free filters between OpenDocument and MSXML, and Microsoft has grudgingly aceeded.\n\nAt OpenOffice.org we are very happy that KOffice is working with us and OASIS/ISO to make a new format to compete with .doc. One that's truly independent, owned and maintained by non-profit standards groups (OASIS and ISO).\n\nOpenDocument is in a position to become the new standard format for all kins of documents. It won't be easy, and it won't happen overnight. But we have a real chance to make this happen. The European Union has done a great job. I can't provide details, but I'm hopeful about the state of Masachusetts.\n\nAnd The success of OpenDocument would provide invaluable benefit to open source and Free Software. No vendor lock-in, bye bye monopoly.\n\nCheers,\nDaniel Carrera.\nOpenOffice.org Community Representative."
    author: "Daniel Carrera"
---
<a href="http://www.koffice.org">KOffice</a> is built on <a href="http://www.kde.org">KDE</a>. This advantage brings enormous potential in terms of integration, performance, scalability, and with the coming full support for the <a href="http://www.oasis-open.org/">OASIS Open Document Format</a>, even portability.  All these factors are vital for business deployments. The KOffice team is looking for <a href="http://www.koffice.org/support/">contributors</a> and <a href="http://www.koffice.org/developer/">developers</a> to significantly increase and utilize the potential of KOffice even further.
<!--break-->
<p>The functionality KOffice has already reached in its short life is significant. And still, KOffice has good performance and is fully usable on low-end hardware, which makes it suited for organizations and individuals. This could even save costs when upgrading or migrating the office software and old hardware can be reused.</p>

<p><i>Because KOffice is built directly on KDE, it can offer the best office experience on a KDE desktop.</i> Many excellent technologies already present in the KDE Framework are available for KOffice at almost no cost. Examples are:</p>
<ul>
<li>Automation via DCOP</li>
<li>Document location abstraction (remote file editing) via KIO</li>
<li>Fully object oriented design through the Qt and KDE Frameworks</li>
<li>Application Embedding through KParts</li></ul>

<p>Additionally KOffice extends the application embedding capability (KParts) of KDE to allow highly integrated documents. Such a close KDE integration also results in fast application load time, while still having all these features available.</p>

<p>Even full OASIS Open Document Format support is being worked on which is the same file format as <a href="http://www.openoffice.org">OpenOffice.org 2</a> will use. This will make data exchange with OpenOffice.org seamless while being a fully integrated KDE office suite. Building on an <i>approved and open document standard is a key feature today.</i></p>

<p>Much work has already been done and central office components like 
<B><a href="http://www.koffice.org/kword/">KWord</a></b>, <b><a href="http://www.koffice.org/kpresenter/">KPresenter</a></b>, <b><a href="http://www.koffice.org/kivio/">Kivio</a></b> and <b><a href="http://www.koffice.org/kspread/">KSpread</a></b> are quite stable with only a few glitches here and there.</p>

<p>And KOffice has many more applications to offer that will be included in next major releases:</p>
<ul>
<li><b><a href="http://www.koffice.org/krita/">Krita</a></b> (Pixmap Graphics)</li>
<li><b><a href="http://www.koffice.org/kexi/">Kexi</a></b> (Database Frontend)</li>
<li><b><a href="http://www.koffice.org/kplato/">KPlato</a></b> (Project Management)</li>
</ul>
<p>All these applications are in development and have a huge potential. Additionally some smaller utilities such as KFormula and KOffice Workspace are available.</p>

<p>As you can see, KOffice is close to offer a very complete office environment, completely integrated into your KDE environment and completely free.</p>

<p>The development of so many additional office components has of course raised the number of developers needed. We are looking for additional contributors to offer all KOffice components in a fully stable and feature complete state.</p>

<p>If you are interested in helping to make this happen, feel free to have a look at the <a href="http://www.koffice.org">webpage</a> and <a href="mailto:koffice-devel@kde.org">contact the developers mailing list</a>. Both, developers and documentation writers, are very welcome. There are also <a href="http://www.koffice.org/developer/tasks/">easy-to-get-started jobs</a> available. And in case you have special interests of any kind feel free to contact us.</p>
<br>

<p><b>References:</b></p>
<ul>
<li><a href="http://www.koffice.org">KOffice homepage</a></li>
<li><a href="http://www.koffice.org/contact">KOffice Contact page</a></li>
<li><a href="https://mail.kde.org/mailman/listinfo/koffice-devel">Register at the KOffice Developer Mailing List</a></li>
<li><a href="http://europa.eu.int:80/idabc/en/document/3524">OASIS Open Document format supported by the EU</a></li></ul>