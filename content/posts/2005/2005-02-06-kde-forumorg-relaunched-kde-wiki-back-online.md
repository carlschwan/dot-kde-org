---
title: "KDE-Forum.org Relaunched, KDE Wiki Back Online"
date:    2005-02-06
authors:
  - "numanee"
slug:    kde-forumorg-relaunched-kde-wiki-back-online
comments:
  - subject: "re:KDE"
    date: 2005-02-06
    body: "KDE have won Desktop Environment of the Year at LinuxQuestions.org MC Awards:)\n\nhttp://www.linuxquestions.org/questions/showthread.php?s=&threadid=286716\n\nKonqueror has won File Manager of the Year.\n\nCongratulations to everyone for the excellent work :)\n\n"
    author: "anon"
  - subject: "re:KDE"
    date: 2005-02-06
    body: "Quanta has won Web Development Editor of the Year at LinuxQuestions.org MC Awards:)\n\nhttp://www.linuxquestions.org/questions/showthread.php?s=&threadid=286716\n\nKdevelop has won IDE of the Year.\n\nCongratulations to everyone for the excellent work :)"
    author: "anon"
  - subject: "Kate came in second (after Vim)!"
    date: 2005-02-06
    body: "Kate is slowly approaching the lead position. Vim will be bet next year. Rest of the pack isnt interesting for me...."
    author: "good job!"
  - subject: "AmaroK too"
    date: 2005-02-06
    body: "Amarok is now a close second (31 %) of xmms (45 %). No doubt xmms\nwill be bet next year as more and more people start using it."
    author: "jmfayard"
  - subject: "Re: Kate came in second (after Vim)!"
    date: 2005-02-06
    body: "It's nice to see a modern editor at last getting the attention, and doing well. Kate/KWrite actually beats EMACS 2:1, way to go:-) "
    author: "Morty"
  - subject: "Re: Kate came in second (after Vim)!"
    date: 2005-02-06
    body: "Yes, and it's still less convenient than good ol' Nedit..."
    author: "Anonymous"
  - subject: "Re: Kate came in second (after Vim)!"
    date: 2005-02-06
    body: "That may be, but according to this poll Nedit finished last. Even beaten by Joe:-)"
    author: "Morty"
  - subject: "KMail beat Evolution to 3rd place; Thunderbird won"
    date: 2005-02-06
    body: "While Evolution still won last year ahead of KMail, this year it was falling back into 3rd place, letting pass KMail for the first time."
    author: "funny"
  - subject: "re:Awards - File management"
    date: 2005-02-06
    body: "Regards the File-management category at the LQ Awards, if you add KFM to Konqeuror, Konqueror has an even more convincing win over the competition.\n\nhttp://www.linuxquestions.org/questions/showthread.php?s=&threadid=272138\n\nKonqueror still won File-management anyway though, and came second in the browser category ;)\n"
    author: "anon"
  - subject: "re:Awards - File management"
    date: 2005-02-06
    body: "> Konqueror still won File-management anyway though, and came second in the browser category ;)\n\nKonqueror is an incredible browser. Now if only those few annoying bugs could get fixed..\n\nOne that annoys me most by the moment is this:\nhttp://www.tulli.fi/fi/03_Yksityishenkilot/02_Autoverotus/07_Prosenttitaulukko/index.jsp\n\nSee the drop-down menus flashing as it constantly reloads the menu entries. This started to happen around the time when 3.1 was released, and it still seems to exist on the cvs head (http bug perhaps?). Same bug comes up constantly with different sites (at least for me), even with internet-banking. And before anyone points out bugs.kde.org - yes this bug was reported roughly two years ago :/\n"
    author: "jmk"
  - subject: "re:Awards - File management"
    date: 2005-02-06
    body: "Hello,\n\ncan you tell us the bug number? I have saved the source to a local file and opened it (same problem), then I cut out more and more as long as the problem was reproduced. The next thing must be the traitor and the problem lies/is triggered by this code:\n\n<input type=\"radio\" name=\"type\" value=\"1\" onChange=\"document.mainFrm.submit();\" checked>\n\nI cannot fix it, but I could report it. \n\nI suspect that this onChange event is constantly happened. It may be as easy as to optimize the \"changed by init\" way.\n\nMore interesting is that close by there is \n\n<input type=\"radio\" name=\"type\" value=\"2\" onChange=\"document.mainFrm.submit();\">\n\nwhich does not trigger the problem. I can remove it, but it still reloads all the time (and a hell quick on local disk). \n\nWhen you submit this to the bug (or let me) I guess it will be resolved by 3.4 and I believe everybody can do this to non-working pages. :-)\n\nKay"
    author: "Debian User"
  - subject: "re:Awards - File management"
    date: 2005-02-07
    body: "> can you tell us the bug number?\n\nHmm, can't seem to find it anymore. May have gotten lost while changing ISP(?) few months ago. Feel free to report it again."
    author: "jmk"
  - subject: "re:Awards - File management"
    date: 2005-02-07
    body: "Found it:\nhttp://bugs.kde.org/show_bug.cgi?id=75338"
    author: "jmk"
  - subject: "re:Awards - File management"
    date: 2005-02-08
    body: "And the bug about gmail ... it still hasn't been fixed ... and i'm using KDE 3.3.92 build of SuSE!\n\nI still get that dreaded \"Oops. The operation could not be completed. Please try again in a few seconds\" error box!\n\n"
    author: "Kanwar"
  - subject: "Konqui won File Manager 1st Place"
    date: 2005-02-06
    body: "http://www.linuxquestions.org/questions/showthread.php?s=&threadid=272138\n\nAnd Konqui took 1st place in file manager category!"
    author: "konqui-fan"
  - subject: "re:Konq"
    date: 2005-02-06
    body: "Yep, congrats to David Faure and everyone else in the Konqi team - a fast, comprehensive file-manager :)"
    author: "anon"
  - subject: "Welcome Back!"
    date: 2005-02-06
    body: "If nobody else cares, I will say it: Great to have both sites back."
    author: "Anonymous"
  - subject: "Knoqi shortcut for KDE wiki?"
    date: 2005-02-06
    body: "Does this mean the Konqi developers will bless us with a KDE wiki search shortcut ala `wp:stirling engine`? hint, hint. :-)\n\nRegards, and Thanks,\n\nMark"
    author: "mark"
  - subject: "Re: Knoqi shortcut for KDE wiki?"
    date: 2005-02-08
    body: "I'm confused.  Konqueror already provides a wp: shortcut that points to wikipedia.  Is this what you were referring to?\n"
    author: "Zippy"
  - subject: "Re: Knoqi shortcut for KDE wiki?"
    date: 2005-02-08
    body: "No, he means he would like to have the same thing for the *KDE* wiki. "
    author: "cm"
  - subject: "Forums for API questions"
    date: 2005-02-06
    body: "There doesn't seem to be a good place to ask KDE API questions outside of #kde-devel. The kde-devel list seems a little inapproriate given that it also serves the purpose of discussing the development of KDE itself. \n\nNavigating through the API docs isn't always easy."
    author: "Ian Monroe"
  - subject: "Re: Forums for API questions"
    date: 2005-02-06
    body: "> The kde-devel list seems a little inapproriate \n\nkde-devel is the correct place.\n\n>  also serves the purpose of discussing the development of KDE itself\n\nNo, that is (mostly) kde-core-devel."
    author: "Chris Howells"
  - subject: "Re: Forums for API questions"
    date: 2005-02-06
    body: "OK, will do the next time I get stumped :)"
    author: "Ian Monroe"
  - subject: "Re: Forums for API questions"
    date: 2005-02-06
    body: "There is a forum for KDE development on QtForum."
    author: "ac"
---
Several weeks ago the KDE Dot News server was compromised.  After a prolonged and painful downtime, we're pleased to announce that <A href="http://www.kde-forum.org/">KDE-Forum.org</a> and <A href="http://wiki.kde.org/">KDE Wiki</a> are now back and functioning.  KDE-Forum.org is now in the capable hands of <A href="http://www.qtforum.org/profile.php?userid=1">Christian Kienle</a> of <A href="http://www.qtforum.org/">QtForum.org</a> who gracefully offered to take over after the former admin decided to pursue other interests.  I'd like to thank both these two as well as David Legg, <a href="http://www.ground.cz/">Lukas Masek</a>, <a href="http://www.arklinux.org/">Bernhard Rosenkraenzer</a>, our sponsor <A href="http://pem.iskra.org/">Pierre-Emmanual Muller</a> of <a href="http://www.cafewiki.org/">CafeWiki</a>, and the <a href="http://www.kde.org/info/security/">KDE Security Team</a> for helping in bringing back these services.  






<!--break-->
