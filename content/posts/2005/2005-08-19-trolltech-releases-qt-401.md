---
title: "Trolltech Releases Qt 4.0.1"
date:    2005-08-19
authors:
  - "binner"
slug:    trolltech-releases-qt-401
comments:
  - subject: "Thanks you trolltech"
    date: 2005-08-19
    body: "I'm waiting KDE 4 to start learning how to code for QT, I've tryied once but I was lacking the necessary skills (C++), even like this, I found that Qt3 was great. I belive Qt4, even more thanks to the Windows GPL version will rock more and more."
    author: "Iuri Fiedoruk"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "There is no need to wait for KDE4 for that.\nJust go and start learning Qt4 right now :-)\nWhen KDE4 comes out you already know Qt and can start coding for KDE4 right away. You can even help with KDE4 if you want.\nI'd suggest to learn Qt before diving into KDE technologies anyway, so don't lose time! ;-)"
    author: "lippel"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "I din't said I HAD to wait, but that I WILL wait, hehehe.\nOnce KDE4 is out binaries for Qt4 will be out for major Linux distros and it will be much easier to program using it.\nBut yes, there is no need for wait, you can compile Qt4 by hand or install the win32 version, it's just I'm too laizy for that ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "I doubt a little bit that you will become a great developer, if the lazyness starts here already, but you could install SUSE Linux 10.0. Qt4 packages do exist there. Now you have no further excuses anymore ;)"
    author: "Micha"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "Qt 4.0.0 packages also exist for 9.x SUSE versions (supplementary/KDE/update_for_9.3/development/)"
    author: "Anonymous"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "Micha: \"I doubt a little bit that you will become a great developer, if the lazyness starts here already\"\n\nAt school they teach me that good programmers are lazy. ;) A good programmer is always looking for the most efficient way to accomplish something."
    author: "Bram Schoenmakers"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-24
    body: "\"At school they teach me that good programmers are lazy. ;) A good programmer is always looking for the most efficient way to accomplish something.\"\n\nWell said ! :)"
    author: "dan"
  - subject: "Laziness"
    date: 2005-08-19
    body: "Undoubtedly he will be a great developer - as Larry Wall wisely commented, laziness is one of the three virtues of a programmer :-)"
    author: "Guss"
  - subject: "Re: Laziness"
    date: 2005-08-24
    body: "I like to lay back a bit and survey the field; this can appear to be laziness. Since he is busy with other, more important matters (like working to feed himself and passing his exams,) there is nothing wrong with waiting a bit to see how things develop. In the meantime he can start with reading QT 4 documentation.\n\nI'm not sure I'd want to go to SuSE 10.0 just to get QT 4. SuSE Pro 9.3 is very stable. I downloaded QT 4 and built it on SuSE 9.3, but then encountered some incompatibility with KDE.\n"
    author: "Adrien Lamothe"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "\"I doubt a little bit that you will become a great developer, if the lazyness starts here already\".\n\nYou know, it's not that I'm lazy at all, but I'm lazy at learning Qt4 because I have still to graduate on computer science, work to pay univercity, admin a cluster...\nSo, in the moments I have free, yes, I'm a hell of lazy ;)"
    author: "Iuri Fiedoruk"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-20
    body: "And there are also qt4 packages available in Debian"
    author: "Isaac Clerencia"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-24
    body: "one thing you shall learn early as a dev is, to install the packages you need yourself.\n\nthat is, go to trolltech.com, get your recent Qt4 (or just get qt-copy from KDE SVN), build it and use it.\n\nregards,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "The difference between Qt 3 and Qt 4 is limited to their API. And thats what API documentation is for, you can't expect yourself to memorize it. \n\nSo what I'm saying is, when it comes to learning Qt there isn't much reason to wait."
    author: "Ian Monroe"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "May I suggest you try Python and pyQT? I think you'll find both to be easy to learn, and highly rewarding. "
    author: "Kilroy"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "I suggest to try ruby =)"
    author: "Andrey Nikanorov"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "Both python and Ruby are great.  \n\nHowever python forces you to get into good habits (whitespace matters!) that will make you a better programmer.   Thus I consider it better for a first language.  \n\nIf you already know how to program, you are doing yourself a disservice if you don't know both.   (unless you know LISP)\n\nLanguage wars are fun, but in the end they are all turning complete, and therefore what one can do the other can do.  It is only a question of which makes the job easier.   "
    author: "bliGill"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "ruby sucks, python4ever"
    author: "sb"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-19
    body: "well that was a constructive reply :)\n\nFab"
    author: "Fab"
  - subject: "Re: \"Ruby sucks\""
    date: 2005-08-22
    body: "As I commented earlier on OSNews.com:\n\n> Somethings are praised best by a broad understatement\n> that not all can understand. Ruby should be (and is)\n> nutured by the ppl that know what it is capable of.\n> Shouting around how good it is only messes up the\n> userbase/community with n00bs.\n\nWhen I stared _really_ using Ruby I was amazed, clean consistent and powerfull.\nAnd the community around it is great!\n\nTo say that \"ruby sucks\" might be a good way to keep it hidden from the masses a bit longer.\n\nCheers,\nCies Breijs.\n"
    author: "Cies Breijs"
  - subject: "Re: Thanks you trolltech"
    date: 2005-08-24
    body: "QT can also be used from other languages, such as Python and Perl, so you don't need to use only C++!\n"
    author: "Adrien Lamothe"
  - subject: "QT4-devel Debian packages"
    date: 2005-08-19
    body: "Does somebody know where can I find the Debian packages (or if it exists)?"
    author: "Marcelo Barreto Nees"
  - subject: "Re: QT4-devel Debian packages"
    date: 2005-08-19
    body: "Qt 4.0.0 is already in SID so it's only a matter of days :-)"
    author: "Andr\u00e9"
  - subject: "Re: QT4-devel Debian packages"
    date: 2005-08-20
    body: "http://ftp.debian.org/debian/pool/main/q/qt4-x11/"
    author: "Marc J. Driftmeyer"
  - subject: "Re: QT4-devel Debian packages"
    date: 2005-08-22
    body: "If you would like to use the new and fresh sources from Trolltech you could just build the sources yourself, it's really easy:\n\n./configure --prefix=~/Programs/QT4/\nmake\nmake install\n\nthen when developing you use these env variables:\nPATH=~/Programs/QT4/bin/:$PATH\nQTDIR=~/Programs/QT4/\n\nsimple as that"
    author: "Baard"
  - subject: "Yes!!"
    date: 2005-08-19
    body: "X11\n- QWidget\n        Implemented support for window opacity.\n\nYes!  Thank you Trolltech and (most likely) Zack!\nThis is what I was waiting for :)"
    author: "Leo"
  - subject: "Re: Yes!!"
    date: 2005-08-19
    body: "QWidget::windowOpacity():\n\"...This feature is only present on Mac OS X and Windows 2000 and up.\"\n\nIs this all there is or did I miss something? Like \"Linux\" and \"alpha channel\" for instance?\n"
    author: "uddw"
  - subject: "Re: Yes!!"
    date: 2005-08-20
    body: "Er, how about \"introduction of top-level window transparency on X11\"?"
    author: "mmebane"
  - subject: "Re: Yes!!"
    date: 2005-08-20
    body: "I think they haven't updated the documentation.  If you look at the URL, it still says 4.0.  I don't know if they change the documentation for minor point releases.  Can't wait to try this out once I have a fast internet connection again."
    author: "Leo"
  - subject: "Re: Yes!!"
    date: 2005-08-22
    body: "Hmm? What has the kernel to do with transparency???"
    author: "Mark"
  - subject: "Re: Yes!!"
    date: 2005-08-22
    body: "For some sad reason people think KDE/Qt/X11 is linux only..."
    author: "randi"
  - subject: "Perceived speed?"
    date: 2005-08-20
    body: "I compiled it and ran qtconfig, but when I switch between the tabs\n\"Appearance\", \"Fonts\" etc I can almost see the tab being painted (or filled with\nthe widgets (comboboxes etc)\n\nSo, visually, I don't notice the promised speedup.\n\nAlso at startup I can see how all is painted.\n\nMy box is an XP2200, with an nv card using theirs drivers on xorg6.8\n(debian infact)\n\nAlso, qtconfig takes almost a second to start, from cache! First cold startup\nis 2-3 seconds...\n\n/me disappointed :-("
    author: "ac"
  - subject: "Re: Perceived speed?"
    date: 2005-08-22
    body: "I bet you linked the program with the _debug libs"
    author: "Baard"
  - subject: "Re: Perceived speed?"
    date: 2005-08-22
    body: "Unfortunately not, and I built it with opengl support also.\nWhat can the problem be?\n\nFYI:\n\nme@myhost:/usr/local/Trolltech/Qt-4.0.1/bin$ ldd qtconfig\n        linux-gate.so.1 =>  (0xffffe000)\n        libQt3Support.so.4 => /usr/local/Trolltech/Qt-4.0.1/lib/libQt3Support.so.4 (0xb7c26000)\n        libQtGui.so.4 => /usr/local/Trolltech/Qt-4.0.1/lib/libQtGui.so.4 (0xb7753000)\n        libpng12.so.0 => /usr/lib/libpng12.so.0 (0x416bc000)\n        libSM.so.6 => /usr/X11R6/lib/libSM.so.6 (0x419b5000)\n        libICE.so.6 => /usr/X11R6/lib/libICE.so.6 (0x4199c000)\n        libXi.so.6 => /usr/X11R6/lib/libXi.so.6 (0xb7734000)\n        libXrender.so.1 => /usr/lib/libXrender.so.1 (0x4154e000)\n        libXrandr.so.2 => /usr/X11R6/lib/libXrandr.so.2 (0x41791000)\n        libXcursor.so.1 => /usr/lib/libXcursor.so.1 (0x4177c000)\n        libXinerama.so.1 => /usr/X11R6/lib/libXinerama.so.1 (0xb7731000)\n        libfreetype.so.6 => /usr/lib/libfreetype.so.6 (0x4192d000)\n        libfontconfig.so.1 => /usr/lib/libfontconfig.so.1 (0x4165c000)\n        libXext.so.6 => /usr/X11R6/lib/libXext.so.6 (0x41881000)\n        libX11.so.6 => /usr/X11R6/lib/libX11.so.6 (0x41558000)\n        libQtNetwork.so.4 => /usr/local/Trolltech/Qt-4.0.1/lib/libQtNetwork.so.4 (0xb76e8000)\n        libQtSql.so.4 => /usr/local/Trolltech/Qt-4.0.1/lib/libQtSql.so.4 (0xb76b3000)\n        libQtXml.so.4 => /usr/local/Trolltech/Qt-4.0.1/lib/libQtXml.so.4 (0xb766d000)\n        libQtCore.so.4 => /usr/local/Trolltech/Qt-4.0.1/lib/libQtCore.so.4 (0xb7536000)\n        libz.so.1 => /usr/lib/libz.so.1 (0x41625000)\n        libpthread.so.0 => /lib/tls/libpthread.so.0 (0xb7524000)\n        libdl.so.2 => /lib/tls/libdl.so.2 (0xb7520000)\n        libstdc++.so.6 => /usr/lib/libstdc++.so.6 (0xb743a000)\n        libm.so.6 => /lib/tls/libm.so.6 (0xb7414000)\n        libgcc_s.so.1 => /lib/libgcc_s.so.1 (0xb7408000)\n        libc.so.6 => /lib/tls/libc.so.6 (0xb72d0000)\n        libexpat.so.1 => /usr/lib/libexpat.so.1 (0x416e3000)\n        /lib/ld-linux.so.2 (0xb7f16000)\nme@myhost:/usr/local/Trolltech/Qt-4.0.1/bin$                  "
    author: "ac"
  - subject: "Re: Perceived speed?"
    date: 2005-08-23
    body: "Don't you have some issues with your fonts, because it loads at the same speed on my *so old* laptop.\n\ntry fc-cache as superuser to update your fonts.\n\nHave a look there maybe:\nhttp://wiki.kde.org/tiki-index.php?page=Performance%20Tips"
    author: "bad_sheep"
  - subject: "Re: Perceived speed?"
    date: 2005-08-25
    body: "I also thought it was slow.\n\nI downloaded the open source edition for windows, and using Qt designer if you drag windows across the widget toolbox for example, it becomes sluggish and there is a white region where the window is redrawn (slowly).\n\nVery dissappointed with the speed on windows. X11 speed seems ok (with Qt 3 - can't get that much slower though can it?).\n\nAnd no there is nothing wrong with the machine. No font issues, no driver issues, and it is a fast machine (1.6 GHz/700MB RAM), so it is just Qt being slow.\n\nHope they improve it."
    author: "Tim"
---
<a href="http://www.trolltech.com/">Trolltech</a> has <a href="http://www.trolltech.com/newsroom/announcements/00000214.html">released the first bugfix release</a> for <a href="http://dot.kde.org/1119953618/">Qt 4, the major release</a> on which KDE 4 development is based on. Among the <a href="http://www.trolltech.com/developer/changes/changes-4.0.1.html">over 450 bug fixes and optimizations</a> are numerous improvements to raster engine, X11 engine and QPainterPath, significantly speeding a range of drawing processes and introduction of top-level window transparency on X11.

<!--break-->
