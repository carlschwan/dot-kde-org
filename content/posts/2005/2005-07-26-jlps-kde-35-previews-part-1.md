---
title: "JLP's KDE 3.5 Previews (Part 1)"
date:    2005-07-26
authors:
  - "jrepinc"
slug:    jlps-kde-35-previews-part-1
comments:
  - subject: "Good work!"
    date: 2005-07-26
    body: "A very nice preview. Thanks for doing the work.\n\n*dreaming*\nMaybe this could be used as basis for a KDE 3.5 feature guide like we had for the KDE 3.1 release (http://www.kde.org/info/3.1/feature_guide_1.html)."
    author: "cl"
  - subject: "Some feedback on the new features..."
    date: 2005-07-26
    body: "\"home:/\n\nKIOSlaves are a special feature of KDE that make it simple to work with files and folders over different protocols. You probably know some of them very well, like file, http or ftp. A new one has recently been added: home. When you open location home:/ in Konqueror you can now see all home folders of the users belonging to the same group as you do.\"\n\nThat is sheer brilliance! These are the little thoughtful touches that we need!\n\nGood stuff.\n\nI can't say that I could tell the difference between kicker's elegant and classic mode by looking at the screenshots.\n\nOne other thing, KDE 3.5 will be a seminal release for lots of people. Why? Because many organizations that currently run a KDE desktop will not rush to the KDE 4.0 series until it has been out for a good while, probably until KDE 4.1 or  \n4.2, which is logical if you consider the breadth and scope of the changes.\n\nTherefore, KDE 3.5 and KDE 3.5.x will be the default desktop of lots of people for up to three years, maybe longer. While much of the new and exciting work may be happening on KDE 4.0, the slow and gradual improvements in the stable series are both very much appreciated and needed. \n\nPlease keep this in mind, guys."
    author: "Gonzalo"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "> I can't say that I could tell the difference between kicker's elegant and classic mode by looking at the screenshots.\n\nLook closer at the tasbar applet buttons."
    author: "Anonymous"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "> When you open location home:/ in Konqueror you can now see all home folders of the users belonging to the same group as you do.\n\nI am worried that this might confuse users. Don't already have two meaning of \"home\", the user's home directory, and the home page for web browsing (btw, can we now have the \"home\" button work differently in Konqueror's file and webbrowsing modes?). And now we have a third location that's called \"home\".\n"
    author: "ac"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "Maybe call it homes:/ ?"
    author: "Anonymous"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "or group:/ ?"
    author: "Jason"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "Don't worry, it's meant to be used with system:/\nand it's then fully hidden to the user.\n\nThe user will deal with system:/users/ in the end\n(it's really home:/ but the user can't see it).\n\nI'm working hard to hide this kind of implementation\ndetails.\n<mode name=\"jedi gesture\">\nThe home:/ protocol doesn't exist.\nYou only need system:/\n</mode>\n=)"
    author: "ervin"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "<mode name=\"stubborn apprentice\">\nWhy does it exist, then?\n</mode>\n"
    author: "ac"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-26
    body: "Agreed, that name is seriously broken. In the desktop metafor home is the users home directory, and in *nix filesystems home is where all users have their private directories. Creating yet another, and proprietary to KDE, home is just plain stupid. If it is meant to show the group the users belong to, call it group or 'users' group. Don't invent yet another use of an already existing name. \n\nBesides who are this feature supposed to aid? Users new to *nix systems? Newbies or close to computer illiterate users, or those Joe averages? All user types not having the faintest clue about the concept of user groups anyway. It's one of the most ill conceived and badly named features ever added. It does not help inexperienced users, as it involves a concept they have no clue of. And for experienced users it add another meaning too a well known name and adds a new concept to an already established functionality. And hiding it for most users in a virtual ioslave hierarchy does not change that."
    author: "Morty"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-27
    body: "Yeah, call it \"group\", or something like that. It shows the group, don't it? \"home\" just confuses."
    author: "shiny"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-27
    body: "Actually, there are three concepts:\n\n    Web_Home (URL of your ISP's start page)\n    Home_Directory (HOME, /usr/<usr_name>)\n    Documents [or better: User_Files] (My Files, working directory, etc.)\n\nFor optimum usability, it should be easy for a user to tell these appart.\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: Some feedback on the new features..."
    date: 2005-07-27
    body: "Other people have already said that the name \"home\" is not a very good one, and I totally agree. I also don't understand what this is good for. What scenraio did the author have in mind? Just because I'm in the same group as someone else doesn't mean that I have read access to that person's home directory.\n"
    author: "anon"
  - subject: "ugly kicker frame still exists :("
    date: 2005-07-26
    body: "it is really sad, that Kicker still has got a white frame/border around it. a better way to handle that is: remove the white frame from the code and including a kicker-wallpaper which has got a white-line or frame in the wallpaper-pixmap. The strange thing is: current transparent kicker hasn't got any frame, well, it definitly shouldn't ;) But People which using a dark kicker-wallpaper might prefer a kicker without any frame. if the function resizing kicker by mouse is back again, then a frame is maybe needed, but then the frame of kicker should be maybe more thick and refelct the kicker-wallpaper color. If the reader of this comment has got no idea of what i mean, just check the look and behavior of windows-xp's \"kicker\", then you notice the diffrences.\n\nthanks"
    author: "anonymous"
  - subject: "Re: ugly kicker frame still exists :("
    date: 2005-07-26
    body: "what are you babbling about? in svn:\n\na) the frame is not white (it follows your colour scheme)\nb) the frame does not appear if you are using a wallpaper\n"
    author: "Aaron J. Seigo"
  - subject: "Re: ugly kicker frame still exists :("
    date: 2005-07-26
    body: "b) that's fine"
    author: "anonymous"
  - subject: "Re: ugly kicker frame still exists :("
    date: 2005-07-28
    body: "Ahhh! Finally, I can use these cool kicker backgrounds I've been making.\n\nI updated yesterday, but hadn't noticed this. \n\nThanks for all of your hard work Aaron."
    author: "Andrew P Rockwell"
  - subject: "systray and mouse overs"
    date: 2005-07-26
    body: "> I think it would be a good idea to also use them for system tray icons.\n\nthis won't happen until we have the new systray protocol (coming in kde4) .. sorry =/"
    author: "Aaron J. Seigo"
  - subject: "the autorun..."
    date: 2005-07-26
    body: "Hello,\n\nI really like KDE, it's my favourite environment. Some of the future KDE's stuff seem really good.\n\nBut there is one thing I don't like with KDE : the default look and feel is too close to Windows'. I like the ability to personalize my desktop, but the default settings seems to copy Windows. \nIs this on purpose ?\nDoes this mean that KDE is only a copy of Windows ? \nDon't KDE have any originality ? any \"sense of identity\" ?\nIs there an lack of creativity ?\n\nI think KDE have to build its identity. It don't have to look like Windows, OSX or whatever. I think Gnome or XFCE have an advantage. Have you ever heard people who try to describe the different Linux desktop tell \"KDE is the one closer to Windows\" ?\n\nI say that because of the new stuff : the autorun feature. It's exactly the same than Windows'. It's a really good feature, and I'm glad that KDE will have a stuff like this. But are we obliged to copy Windows ?\n\nFor exemple, why are people trying to do themes or \"improvement\" in KDE-Look.org which tend to make KDE like Windows ?\n\nBut don't think I don't like KDE... I've been using KDE for 4 years... and I really think it's getting better and better... But my opinion is that KDE have to be creative and original. I hope KDE 4 will begin the \"revolution of the desktop\"... (I don't think it will change a lot, but maybe plasma will help us to make the difference)\n\nThank a lot to all KDE developpers for your good work."
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Shut the hell up.  I'm so sick of this over and over."
    author: "manyoso"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Doesn't have The users feedbacks any importance anymore ?\n\nAs I said, it is just my opinion...\n"
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Yah, it is your opinion, but your opinion is crap.  Not only has it been done to death, but it really is an entirely worthless and void comment.\n\nAll you have is some vague 'impression' that KDE is 'too much' like Windows.  Well, I can write out a list of a hundred and one things right now where KDE is nothing like Windows.  1.  Copy & Paste  2.  Double Click VS Single Click  etc, etc.\n\nI'm sure if only we'd put the kicker at the top you'd be satisfied that, finally, we did something 'creative' and 'original'.  Oh, and egads if we'd put it on the right side of the screen or the left you'd be in orgasmic delight with all the 'creativity' and 'originality'.\n\nNevermind the fact that if you don't like how KDE looks and feels or want to make it the diametric opposite of Windows... you yourself can do that.  Feel free.  It's called the control panel and the configuration dialogs.  Have a ball.\n\nAs for decrying that others would want to exercise their control panels and configuration dialogs so their personal desktop looks and behaves more like Windows?  What the hell do you care?\n\nI know I don't."
    author: "manyoso"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "I've taken in consideration your point of view ... I won't try to tell you are wrong... (everyone's point of view is welcome)...\n\nBUT : \n---\n\ndon't think I'm a newbie... don't think I don't have modified my KDE (my settings are really different from the default ones)...\nI was just talking about a tendance I see more and more..."
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "What an ass!"
    author: "Jason"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "> Yah, it is your opinion, but your opinion is crap\n\n> What an ass!\n\nI don't understand very well...\n\nMaybe my message wasn't clear...\nI did NOT want to  be aggressive in any of my posts...\n\nWhy such reactions ?\n\nIf talking about this here is problematic, let's forget it... there is no need to fight..."
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Didier,\n\nI wasn't replying to you."
    author: "Jason"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "Unfortunately, some don't seem to realize that the user's feedback is the most important thing.\n\nHowever, I have to say that I don't agree with you -- but I won't be rude to you just because I disagree -- about KDE being too much like Windows.  We shouldn't make it different just to make it different.  There *are* many ways that KDE is different than Windows (it is not a copy of Windows like XPDE intends to be) but I see nothing wrong with the basic look being similar.  OTOH, if we can make usability improvements over Windows by changing the look, they we should do so."
    author: "James Richard Tyrer"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Total agreement. Shut the hell up."
    author: "Joe"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "There's your kde developer for you....being an asshole as usual."
    author: "Dave"
  - subject: "Re: the autorun..."
    date: 2005-07-28
    body: "Newsflash:\n\nKDE developer angered by man posting apparent troll about most trolled-to-death topic in KDE.\n\nFilm at 11!"
    author: "Dolio"
  - subject: "MODERATION needed"
    date: 2005-07-27
    body: "This whole thread is worthless. Please kill it and leave a message saying why it was killed. I do not know why ppl can be civil. I almost get the feeling that this  is at best a 2 person coversation, and at worst a one person conversation meant to look mean-spirited on KDE developers part"
    author: "a.c."
  - subject: "Re: MODERATION needed"
    date: 2005-07-27
    body: "He's right. It's been done to death. There are some ways in which KDE is like Windows (like even Gnome and Mac) and other ways in which it is nothing like Windows. The fact is, in a desktop environment every desktop environment will look vaguely like another.\n\nAs he says, just shut up."
    author: "David"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "\"look and feel is too close to Windows\"\n\nTwo points: \n\nOne: Is that a bad thing? If so, why? If something resembles Windows, is it automatically bad?\n\nTwo: Care to show some examples on how KDE is like Windows? Yes, there are SOME similarities between KDE and Windows. But there are similarities between all GUI's that use the WIMP-paradigm. \n\nSeriously: should some thing be done differently in KDE just so it wouldn't resemble Windows? Maybe KDE should get rid of the taskbar, since Windows has it as well? the Kmenu servers the same purpose as the Start-menu on Windows, so I guess it needs to go as well? Both also seem to have close-button in the top-right corner of windows, so I guess that needs to be changed as well.... And of course KDE should drop Autorun as well. Since Windows has the ability to run apps when the user starts the desktop, KDE must not have that ability. Otherwise it would be \"copying Windows\".\n\nWhat would KDE achieve with changes like that?\n\n\"For exemple, why are people trying to do themes or \"improvement\" in KDE-Look.org which tend to make KDE like Windows ?\"\n\nBecause they want to. And what do you suggest KDE should do about it? Tell them to stop? Why?"
    author: "Janne"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "As I have answered in the previous message :\n\nI'm not trying to convince everyone... I just tell you what might be wrong (in my point of view) and I have a little bit explained why (maybe tou haven't read it)...\n\nThere is now 3 persons who have answered me and told me I'm wrong... it's OK for me... I've not the absolute science...\n\nI've said what we can do to improove (again : in my point of view) KDE...\n\nJust another thing : \nI've tell nowhere that Windows' Look&Feel (or Windows itself) was bad... I have also talked about OSX (which is really beautiful)... I just talked about differences (which would be good for me)..."
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "But I don't see your point. really, I don't. Yes, KDE has some features that can be found in Windows as well. Is that a bad thing? Of course not! And KDE has tons of features that are NOT found in Windows!\n\nYou talk about \"improving\" KDE. But looking at your first post, those improvements would merely be changes to the UI so it would look different from Windows. Looking different from Windows is not a virtue by itself. Being different for the sake of being different is not smart IMO.\n\nWhat you basically said was that \"KDE looks like Windows, and we must change it so it looks less like Windows\". Well, that is not IMO a smart way of designing a UI. Designing KDE like that would also tie it to Windows, just in the reverse. Instead of \"cloning\" Windows, KDE would be avoiding Windows. But still, KDE would be defined by Windows and it's UI. But not by the similarities between the two, but by differences between the two. One of the defining features of KDE would then be \"it's as different as possible when compared to Windows!\". But why should it be? If Windows does something right, why shouldn't KDE do the same?\n\nWhat KDE should do is to create as kick-ass UI as possible. And I see them doing just that. Yes, there are similarities between KDE and Windows. Just as there are similarities between Windows and OS X, Gnome and others. "
    author: "Janne"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "> Yes, KDE has some features that can be found in Windows as well. \n> Is that a bad thing?\n\nHaving some stuffs like Windows is not a bad things, but you should read my others messages, this is not the point...\n\n\n> Designing KDE like that would also tie it to Windows, \n> just in the reverse.\n\nNot to copy doesn't mean to avoid or do the reverse...\n\n\nBut my reflexion was based on one impression : \"KDE seems to do copy Windows\" (I say it this way as a simplification)... but if you read the other messages, my impression is not shared by everyone. Moreover, the guy who is responsible for the new autorun feature has written a message and told that if the KDE's autorun looks like Windows', it was not on purpose...\nSo maybe my impression may be wrong about KDE's global evolution... I'm open minded, so I can understand it and the problem doesn't exist anymore...\n\n\nAnd even if there was an intention of copy, it wouldn't mean it was bad... the \"debate\" would have been opened. It would not really be a debate because this is not the right place, but only my suggestion about building an \"original line\" for KDE.\n\n\nBye.\n\n\nPS : I'm using some simplification for the problematic to be clear, but, of course, I've not said that whole KDE was the Windows' copy...\n"
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "I think that KDE should remove whole GUI, because Windows have one too."
    author: "accc"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "And I think that KDE should run away from linux, because Gnome on it. \nHow about OS KDE? ;)\n\n      "
    author: "Vik"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "thats exactly what osx is. its not better, jst different. \n\n\n\"Maybe KDE should get rid of the taskbar, since Windows has it as well\"\n\n-they made the dock, which confuses the hell out of me TBH.\n\n\"the Kmenu servers the same purpose as the Start-menu on Windows, so I guess it needs to go as well? \"\n\nthe apple \"bar\", moved menus from windows to the top of hte screen, moved the \"application\" button to the top instead of at the bottom like windows.\n\n\"Both also seem to have close-button in the top-right corner of windows, so I guess that needs to be changed as well\"\n\napple moved it to the top-left corner. \n\nwhy? just to be different. being like windows is not a bad thing IMO. if kde was as insecure as windows or as uncustomizable as windows, then yes, it definetly needs to be chagned. but changing something just for the sake of changing it will result in:\n\n1. wasting dev time\n2. confusion for the user\n3. something that will most likely end up less usable than before. "
    author: "drizek"
  - subject: "Re: the autorun..."
    date: 2005-07-31
    body: "Apple GUI was designed before the first Windows was created (see the Apple Lisa GUI, 1983)\n\nThen Windows copied them, but there was patent infrigments, or something like that..."
    author: "pepebotika"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "> I say that because of the new stuff : the autorun feature. It's exactly the same than Windows'. It's a really good feature, and I'm glad that KDE will have a stuff like this. But are we obliged to copy Windows ?\n\nI don't quite understand your point. You like the new feature, you're glad to have  it, so what's wrong with it? So what if Windows does the same thing? Would you rather not have it, just for this reason?\n\nOf course, if you had an idea how to do it *better* than in Windows, then I'm sure the developers would be very interested to hear about it."
    author: "ac"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "\"But there is one thing I don't like with KDE : the default look and feel is too close to Windows'. I like the ability to personalize my desktop, but the default settings seems to copy Windows.\"\n\nIn FreeBSD, a wizard guides you through the configuration process, and enables you to pick a predefined look&feel: Windows, Mac OS, CDE or KDE. Probably Linux distros feature such a wizard also.\n\nFeatures are not good because Windows implements them, but neither are they bad because Windows implements them. I for one don't have a problem with copycatting, as long as good features are copied, be it from Windows, GNOME or OS X."
    author: "St. Kevin"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "It's a KDE thing - the program is kpersonalizer.  it's run if there's no .kde folder."
    author: "JohnFlux"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "A bit too generic description, it runs if kpersonalizerrc: \"[General] FirstLogin=false\" is not set.\n"
    author: "Anonymous"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "By the way IMHO we should get rid of kpersonalizer.\n\nDevelopers should select reasonable look&feel defaults for user,\nand not ask them from a newbie.\n\nJust my 0.02 euros.\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "> By the way IMHO we should get rid of kpersonalizer.\n\nwhy?"
    author: "cl"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "In contrary, it should be heavily extended instead."
    author: "ac"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Well, I discovered this feature in MS Windows only recently and it was\n_after_ I took the decision to have a dialog like the one you'll have in\nKDE 3.5.\n\nThat's definitely not decided by copying MS Windows.\nMaybe you're right in some cases, but not this one. ;-)\n\nAs a side note, we use a dialog here because it was the best solution\nconsidering our current framework. Expect something completely different\nthanks to Plasma for KDE4."
    author: "ervin"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "What was wrong with just putting an icon on the desktop and letting me access it when I want to? Having things pop up in your face is generally annoying...\nIt lets you do nothing, I don't think, you couldn't previously -- (double) click the desktop icon for the default action, right click for the context menu for other actions -- and if there's two ways of doing things, one that involves popping up a dialog and another which doesn't, the other is usually correct, I think."
    author: "Illissius"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Take another look at that dialog, are you seeing that checkbox? Realize the meaning of it? You can set the preferred action for each type of device once, to what you prefer. You get it right according to your own preferences and not some arbitrary default."
    author: "Morty"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Figured I would get this reply.\nYes, I realize the meaning of it. Putting \"do not show this again\" checkboxes on all of the many dialogs that get popped up at you merely makes them less bad; not good.\n\nThe thing with configurability is it should be available to the user, but not forced upon them. Imagine having to explicity configure everything in kcontrol before using KDE; wouldn't be very fun. Same thing, smaller scale. That's what defaults are for. (And also why the defaults should be as good as possible: so the user has less need to change them.)\n\nIn this specific case, what I also dislike about it is it assumes that when I insert something, I want to do something with it /right now/, and that that something is one of the choices presented; this is quite often simply untrue. (If you need an example: inserting a USB thumbdrive to download something onto it; or for a more mundane one, when I simply want to finish the paragraph I'm reading first). Of course, in these cases you can say 'do nothing', but the computer should be just sitting there waiting for the user to tell it what to do, not taking initiatives on its own."
    author: "Illissius"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "And in this case it's pure and simple brilliant usability, the first time a event happens you may configure the computer to act the way you want it to every time the same event occurs. Since this specific case are one where there are countless different ways for the computer to act, according to the users preferred usage pattern. No default solution would be good enough and satisfy anything but a fraction of the users. Take your preferred way, it would be rather annoying or even illogical for a rather large number of users. But with this solution we all get our preferred solution, for each kind of device, with tree simple mouse clicks.  "
    author: "Morty"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: ">Having things pop up in your face is generally annoying...\n\nOK it's annoying when you have done nothing special, but it's quite predictable right after you plug your USB key or put a CR-ROM."
    author: "guillaumeh"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "> What was wrong with just putting an icon on the desktop\n> and letting me access it when I want to?\n\nnothing if you know exactly what to do with it every time, but many people don't. so as a power user, just skip over this feature. the rest of the user population will find things easier due to it, whilst you click on the icons. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "We could put Kicker on the top of the screen, and then we would be just like GNOME and nothing at all like Windows!\n\nSeriously. I've never understood why KDE is like Windows but GNOME is not. Both have a \"start\" button. Both have panels. Both have windows with titlebars on top. Both have icons on the desktop. But for some reason KDE managed to get a reputation of being Windows-like, but GNOME didn't. Why is this?\n\np.s. Of course, XFCE doesn't have this reputation because it started as a deliberate clone of CDE instead."
    author: "Brandybuck"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Simple, it's the color scheme. Both KDE and Windows are blueish while Gnome uses brown colors as default. It sounds ridiculous right? But sadly it's the biggest reason. It's rather easy to spot too. Look at reviews of Mandrake/Mandriva and how often it, KDE looks like windows appear in the folowing discussions. Then compare it to reviews of Suse, you never see that particular comment directed at the default Suse desktop. Sometimes as general rantingag towards KDE, but never Suse specific."
    author: "Morty"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "> Gnome uses brown colors as default.\n\nSince when? You only know Ubuntu, right?"
    author: "Anonymous"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "Nah, just forgot to add \"greyish blue or\" before the brown. But my argument are still correct tho."
    author: "Morty"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "Looks like there's no colors in Gnome at all. Everything is rather monochromic. Maybe it's because of gnome's \"designer\" eyes deviation?"
    author: "Vik"
  - subject: "Re: the autorun..."
    date: 2005-07-28
    body: "Well, actually, the \"monochromatic\" colour cheme of Gnome how you call it is one of the 2 points where Gnome outperforms KDE.\nAn interface should not be too flashy. Think about your car, your washing machine, your VCR etc. They all have un-agressive interfaces, so you can concentrate your attention on what you are doing (driving, choosing the washing temperature, programming for recording the movie tonight).\n\nOnce KDE will get rid of the flashy colours and icons, it will become more usable. Actually, the same is true for XP. Most of the collegues I know at work are in 2 groups:\n1- users who do not know how to change the look and feel of XP. They keep the default fisher price look.\n2- slightly more advanced users: they bring back the classic start menu, change the explorer (file manager) to look like win2k or nt one, change the widget style (to classic, or at least to the silver one) and change many other configurations hidden somewhere.\n\nAn interface/desktop must not focus the attention of the user. The user has to focus on his work (the letter he is writing, his accountancy stuff, the photos of the kids, the code he is writing, whatever).\n\nFor the curious, the second point Gnome is better that KDE is being addressed I think. Spaces in menubars, too many Konqueror useless buttons.\n\nApart from that, of course, KDE is lightyears away (speed, user-friendlyness, open/save window, configurability).\n\nLet the flames begin!"
    author: "oliv"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "I agree it is not a good thing if we just started copying the look of windows in every detail. On the other hand, it's neither a good thing to just do everything opposite of Windows. Even windows is userfriendly at some things.\n\nKDE is the most innovative desktop environment around if you look at it's code design and features, and shouldn't do or don't do things usabilitywise because of the behaviour of windows, mac os x, gnome or emacs. We should just make the most intuitive and usable settings default."
    author: "bas"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "If you were more specific on WHY it feels like windows you might be taken more seriously."
    author: "am"
  - subject: "Re: the autorun..."
    date: 2005-07-26
    body: "First, I'm not a GUi expert... I'm in fact quite bad (in reference to each time I've been involved in an interface in a program)... my point of view was based on a general impression and some people's feedback (forums, etc...)... and also on some of KDE's evolutions... (not necessarily bad)\n\n\n\nSecondly, at the beginning it was a way to discuss about a possible improvements... but some people here :\n\n_ think it's useless to discuss (I though this community was more open... Indeed, I've written this here because a KDE site... so I though it was the best place to talk about KDE... the point was not to tell that KDE sucks... but what it can be done ... or what have not to be done....)\n\n_ have misunderstood what I've said (someone even suggested as a critic against my message to remove all the GUI)... I've not a close or extreme point of view...\n\n\nBut I recognise I must have not expressed myself very well. Maybe my english is not good enough.\n\n\nSo your right : I can't been taken seriously...\n\n\nAs I said in a previous message, let's drop all this discusion... it's useless now...\n\n\nMy finals words will be (again) : great works to all KDE team... keep going.."
    author: "Didier"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "Yes, I want KDE to be exactly like Windows!!! Do you have any problem? I'ts good for ex-windows users. And I have been using KDE for 7 years since KDE 1.1 and Redhat 6.1.\n\nThe only bad thing about windows is NO-FREEDOM\n\n1. you cannot install on more than 1 system\n2. you cannot share the OS with your family and friends\n3. you cannot modify a program to suit your needs\n\nBut KDE is wonderful, it gives the essential necessity of humans - 'Freedom', and freedom means you can be good or bad, beautiful or ugly. If someone forces you to be good and beautiful then where is the freedom to be bad or ugly? Let KDE be made like Mac OSX, Windows, CDE, OS/2 or even Windows 3.1 ;).\n\nI love KDE and KDE developers for respecting the most humane value and that is \"freedom!\". Thanks guys, I love you for that!"
    author: "fast_rizwaan"
  - subject: "Re: the autorun..."
    date: 2005-07-27
    body: "Looks like Windows. more and more...agreed!"
    author: "blubb"
  - subject: "GCC 4.x & KDE 3.5"
    date: 2005-07-26
    body: "I see that you used mandriva 2006 beta with GCC 4.0.1. Was mandriva's GCC patched, or does KDE 3.5 work with vanilla GCC 4.0.1? I have tried KDE 3.3.2 and 3.4.x with vanilla GCC 4.0,1, but it miscompiles some parts and pukes on others. (like GCC 4.0.0)"
    author: "Steven"
  - subject: "Re: GCC 4.x & KDE 3.5"
    date: 2005-07-26
    body: "It should since AFAIK some KDE developers compile KDE trunk with GCC 4."
    author: "ac"
  - subject: "Re: GCC 4.x & KDE 3.5"
    date: 2005-07-27
    body: "KDE 3.4.1 works perfectly when being compiled with GCC 4.0.1 (Some newer files from SVN are required though). I suppose KDE 3.5 will compile with GCC 4.0.1 without a hitch"
    author: "Artem Tashkinov"
  - subject: "Re: GCC 4.x & KDE 3.5"
    date: 2005-07-27
    body: "I think all you need is the kdebase-3.4.1 gcc4 patch -\nftp://ftp.frugalware.org/pub/frugalware/frugalware-current/source/kde/kdebase/kdebase-3.4.1-gcc4.patch"
    author: "kyler"
  - subject: "Stop Gnome imposition"
    date: 2005-07-26
    body: "The LSB must include Qt or we will have to fight them\n\nhttp://www.linuxbase.org/LSBWiki/DesktopWG"
    author: "Dave"
  - subject: "Re: Stop Gnome imposition"
    date: 2005-07-26
    body: "It is possible you'll get what you are asking for since make xconfig needs Qt."
    author: "ne..."
  - subject: "Re: Stop Gnome imposition"
    date: 2005-07-27
    body: "isn't there another tool that's built with Gtk that does the same? I'd be surprised if there wasn't. And if this would be the only reason for merging in Qt I'm quite sure that someone would write a Gtk version pretty soon...\n"
    author: "Chakie"
  - subject: "Re: Stop Gnome imposition"
    date: 2005-07-27
    body: "make gconfig uses Gtk, that's has been there for a while!\n"
    author: "Anonymous Person"
  - subject: "Re: Stop Gnome imposition"
    date: 2005-07-26
    body: "As if the LSB matters anyway...  No one gives a damn about it.  "
    author: "KDE User"
  - subject: "Re: Stop Gnome imposition"
    date: 2005-07-27
    body: "No one gives a shit because up to two-thirds of KDE users use KDE fine, without regard for what is or isn't in the LSB. I doubt whether anyone knows what it is.\n\nIt's typical of what's gone on in the Unix world for many years \"Let's standardise everything and we can shut out the stuff we don't want\". People then go off and use what they want anyway, including Windows."
    author: "David"
  - subject: "Re: Stop Gnome imposition"
    date: 2005-07-27
    body: "Relax.\n\nThis does not stop distribution shipping KDE! (For years distributions having been shipping stuff that is not part LSB including those that are LSB-Compliant!)\n"
    author: "Anonymous Person"
  - subject: "Storage Media Notification"
    date: 2005-07-26
    body: "How do you make this work?\n\nWhen I insert a CD into my drive, nothing happens.\n\nI've the latest SVN of KDE, I've ivman and hald running.\n\nPS.: I'm using gentoo, but I don't think that makes a difference here."
    author: "Morten Sj\u00f8gren"
  - subject: "Re: Storage Media Notification"
    date: 2005-07-27
    body: "Control Center-> Service Manger, there I think you need to have KDED Media Manger and/or Media Notifier Demon running."
    author: "Morty"
  - subject: "Re: Storage Media Notification"
    date: 2005-07-27
    body: "It already is, but I've just found out that the whole detect devices doesn't work. Not sure what the name for this is, so I'll try to explain:\nIf I right on desktop select \"Configure Desktop...\" and go into Behavior -> \"Device Icons\", Select \"Show device icons:\" and mark all \"Device Types to Display\".\nNo devices is shown on my desktop, not in latest SVN nor in KDE3.4. I think that s why \"Storage Media Notification\" isn't working for lastest SVN.\n\nAnd it doesn't work for root either.\n\nThe only device icon I can get on my desktop is my USB flashpen.\n\nI'm not sure what's wrong here, I've a couple of ideas:\nI'm running a 98% native 64 bit OS on a AMD64 3000+ (might be that device icons doesn't work on AMD64).\nMy kernel could be screwed, I'm using 2.6.12-gentoo-r6, so I'm going to other kernels later.\n\n"
    author: "Morten Sj\u00f8gren"
  - subject: "Re: Storage Media Notification"
    date: 2005-07-27
    body: "I'm not sure, but you might need to have HAL installed. I have HAL on my Gentoo-machine, and KDE automatically detects media that I insert to the system."
    author: "Janne"
  - subject: "Re: Storage Media Notification"
    date: 2005-07-27
    body: "Don't think it's a AMD64 issue, but you have several other possible points of failure before the KDE part. Have you verified if the HAL and DBUS are set up correctly and works(from shell or something). And the versions of DBUS are important, as KDE 3.4 and the Qt-DBUS bindings only works with version 0.4.x of DBUS. If you don't want to muck around with that, try building KDE without DBUS supports as it then does some autodetecting magic on its own. It works rather well, and it's rather portable(no 64 bit issues as far as I know). "
    author: "Morty"
  - subject: "Re: Storage Media Notification"
    date: 2005-07-31
    body: "There is an USE flag on some KDE package that enables the media manager. Sorry I don't remember what is."
    author: "pepebotika"
  - subject: "Re: Storage Media Notification"
    date: 2005-08-08
    body: "Already had that, it's called \"hal\"\n\nFor somereason it didn't didn't work before, but aften trying upgrading/downgrading hal, dbus and ivman it works now in KDE3.4, well all except inserting data CD/DVDs. It does however not work in KDE3.5 svn, tried rebuilding and reinstalling three or four times.\n\nhal 0.4.8\nivman 6.4\ndbus 0.23"
    author: "Morten Sj\u00f8gren"
  - subject: "Thanks Jure Repinc !"
    date: 2005-07-27
    body: "You've explained the new features very well. Thank you, and I hope to see your reviews more on \"the dot.\""
    author: "fast_rizwaan"
  - subject: "Standard Notification architecture ?"
    date: 2005-07-27
    body: "Is there any plans to use a standard notification architeture ? I mean look at the msn/amsn dialog popup when some one send a message to you? I mean cant we have a framework so applications can use the standard notification? instead of each application having to use its own notification methods ?\n\nI believe there is some thing like this on macos (apple) dont remember what it is now ?\n\nI mean provide something like the amsn popup as a standard notification for kde/other applications. Any thing like this is being worked on for kde 4 ?\n"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "> Is there any plans to use a standard notification architeture ?\n\nDid you ever use KDE?"
    author: "Anonymous"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "Ahh its growl:\nthe info for it is here: http://growl.info/\n\ncheers"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "> Is there any plans to use a standard notification architeture ?\n\nKDE has a KDE-wide standard notification architecture since a very long time. It's called knotify. It provides differents means of event notification like sounds, dialog boxes or passive popups.\n\nThis is an interesting article how to use knotify in your own scripts:\nhttp://lukeplant.me.uk/articles.php?id=3"
    author: "cl"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "hi,\n\nI have known knotify for a while :) but instead of popups, couldnt we have the notifications of applications like the amsn user has logged in notification (probably at the taskbar area instead of the obtrusive popup.) The notification area on the corner of taskbar to me seems ideal for program notifications like the amsn user logged in or xyz says:.... \n\nAn example would also be the windows xp/2k identification when a usb or external device is connected but at a much higher level.\n\nThe colour of the popup on the corner of the taskbar would indicate the severity of the progam requiring the users attention.\n\nIt can be flexible and allow embedding of images, text, buttons and other items so as to make it flexible.\n\nIt could also provide notification of new usb images disconnected/connected and syslogs etc. \n\nThere also needs to be a cleanup of the ui (knotify) for creating such an application.\n\nProbably could also use superkaramba for notification\n\nAny comments"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "Submit a feature request at bugs.kde.org and you have my vote :)\n\nThe current framework lacks features in many areas so that developers began to invent their own solutions. Examples are the OSD of amaroK, bubble tray popups of kopete and the OSD of kmilo, just to name a few.\n\nA new framework should make visual notifications themeable and should solve a usability issue which seems to be very under underrated: Notifications disappear within a very short period of time and you have no chance to see them again. Think of people with bad eyes who can't read that fast or the average Joe User who doesn't look at the screen for a couple of seconds. One should be able to see the last few notifications again. Consider a flashing button in the tray or maybe a section of the sidebar, where the last few notifications are displayed (just an example).\n\nWhat I like best at KDE are the powerful frameworks (and therefore code reduction, fewer bugs and consistency), but knotify is definitly the one that needs a lot of work. I hope, someone picks up the work on it for KDE4."
    author: "Markus"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "Hello\n\nPlease vote for this bug, Some one has already made a wishlist for the growl like notification for knotify.\n\nlook for bug #108076\nhttp://bugs.kde.org/show_bug.cgi?id=108076"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "Done. Thanks for bringing this to attention and for copying our comments to the bugreport."
    author: "Markus"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-28
    body: "No problem Markus :) i forgot to add post by markus tho for the comment on the bug :)\n\nHopefully this one makes progress\n\ncheers"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-29
    body: "I just voted for it too =]"
    author: "DeadS0ul"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "The new mouse-overs in KDE 3.4 for static Kicker icons are very nice and polished looking. At the most simple, notifications could look as nice as this. (It is strange to roll the mouse over, for instance, the KMenu icon, then to roll it over the kmix docked icon; the yellow tooltip for the kmix mouse-over simply looks ugly.)"
    author: "d w"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-27
    body: "I think all this will be done for KDE 4. knotify can be enhanced to allow for all kind of notifications, bubbles and whatever you want..."
    author: "superstoned"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-28
    body: "I've already suggested something remotely similar on the Kollaboration forum:\n\nhttp://www.kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,141.0"
    author: "Willie Sippel"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-28
    body: "Ha Willie thats some great stuff you got there, I will add the url comment to the bug report :)\n\nthanks"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-28
    body: "Ohh one more important thing:\n\nWhy only use the lower right corner for notifications ?\n\nif we divide the desktop into 4 squares:\n(top right, top left, bottom left, bottom right)\n\nWe can use the \n\nTop right square for:\n1. Laptop Information/status updates ?\n\nTop left for:\n1. application OSD etc.\n2. what else?\n\nbottom left square for:\n1. dunno ?\n\nbottom right square for:\n1. system notifications\n2. device notifications\n\nIf these are standardised will be great and will disperse the notification areas acording to categories to different sections of the screen. These need to be standardised on the desktop so no matter what application/error generated it gets displayed in the appropriate section of the screen.\n\nThere needs to be a standard on the color of the notification:\nRed: Critical User needs to take care of if immediately\n\nYellow: User can continue working on his work, but will need to look at it at a later stage\n\nGreen: Application update success etc eg: yast update successfully compelted etc.\n\nDistribution developers can standardise this on their desktops for the color and the section of the screen for the types of notifications. \n\nI am looking forward to it Hopefully it will become reality.\n\nI will submit a bug report for this :) \n\nAny comments ?\n\nCheers"
    author: "pingu"
  - subject: "Re: Standard Notification architecture ?"
    date: 2005-07-28
    body: "added bug report \n\nhttp://bugs.kde.org/show_bug.cgi?id=109787\n\nPleaase vote for it\n\nalso comments?\n\n"
    author: "pingu"
  - subject: "KIO Slave \"home:/\""
    date: 2005-07-27
    body: "<<A new one has recently been added: home. When you open location home:/ in Konqueror you can now see all home folders of the users belonging to the same group as you do.>>\n\nIt was my understanding that after some discussion on kde-core-devel that this (so-called) \"feature\" was being dropped.  But, if not, the question is why would anyone want to do this (to see the HOME folders of other members in a group [groups?])?\n\nRemaining unresolved is the question of where the \"Documents\" folder fits in with this.  And, it is questionable if \"Documents\" is a good name for the default working directory for loading/saving files.  There is also the problem that this currently only works for KDE applications.  Yes I filed a bug (108510) for this, but the maintainer doesn't believe that it exists. :-(  Will this issue be fixed for 3.5?\n\nThe \"My Documents\" vs. HOME issue is an important usability issue that needs further design work.  Should there also be an I/O slave for \"Documents\"?  What should it be called (\"documents:/\" would be consistent but that name appears to confuse users that don't realize that \"Documents\" is for _all_ of their personal data files).  This is confounded by the current default where \"Documents\" = HOME -- this not how Windows and OS/X do it -- and things need to change based on whether \"Documents\" is set to HOME or somewhere else - $HOME/Files on my system.  Look at the code:  \"kdelibs/kio/kfile/kfilespeedbar.cpp\".  The same needs to be implemented in other places for consistency."
    author: "James Richard Tyrer"
  - subject: "Re: KIO Slave \"home:/\""
    date: 2005-07-27
    body: "> Look at the code: \"kdelibs/kio/kfile/kfilespeedbar.cpp\". The same needs to be implemented in other places for consistency.\n\nhttp://lists.kde.org/?l=kde-commits&m=112180489407884&w=2\n\nquote: \n\n\"Also provides an optional \"Documents Folder\" entry in\nsystem:/ if the document path is different from $HOME.\""
    author: "cl"
  - subject: "Re: KIO Slave \"home:/\""
    date: 2005-07-27
    body: "Yes, that is one place and I am aware of the commit.\n\nI haven't had a chance to test it yet (I am currently converting my source tree to branches/3.5).\n\nWhat about the other places that \"Home\" or \"Home Folder\" appears with the incorrect presumption that that folder is where the user stores their files?\n\n"
    author: "James Richard Tyrer"
  - subject: "Re: KIO Slave \"home:/\""
    date: 2005-07-30
    body: "OK, so now I have tried it.\n\nWhat the ... is that folder: \"Users Folders [sic]\" for?  What is the purpose and why does it exist?  Again, I thought that this useless *feature* was being dropped.\n\nWhy are we using the icon \"folder_important\" for \"Documents Folder?  Shouldn't we use a better name?\n\nI made the icons \"folder_personal.  They are here:\n\nhttp://home.earthlink.net/~tyrerj/kde/folder-personal-CR.tar.bz2\nhttp://home.earthlink.net/~tyrerj/kde/folder-personal-KDEClassic.tar.bz2\n\nPlease use them."
    author: "James Richard Tyrer"
  - subject: "Re: KIO Slave \"home:/\""
    date: 2005-07-29
    body: "How about room? :p, so we've got for example: /home/acount/room (and room is for all it's stuff, and in room we have for example documents; films; music; pictures....?"
    author: "Terracotta"
---
Every year, during the summer holidays, I regularly download the source code of the new version of KDE desktop that's in the works and compile it, test it and help with translation into Slovenian language. This year I've decided to also post previews of the development version of KDE on <a href="http://jrepin.blogspot.com/">my blog</a>. I'll try to put up as many screenshots as possible so that everyone can see what is coming in the future. <a href="http://jrepin.blogspot.com/2005/07/jlps-kde-35-previews-part-1.html">JLP's KDE 3.5 Previews - Part 1</a> is now available.
<!--break-->
