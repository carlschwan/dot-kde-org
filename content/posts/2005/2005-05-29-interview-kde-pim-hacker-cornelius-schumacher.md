---
title: "Interview with KDE-PIM Hacker Cornelius Schumacher"
date:    2005-05-29
authors:
  - "aleeuwen"
slug:    interview-kde-pim-hacker-cornelius-schumacher
comments:
  - subject: "Internationalisation"
    date: 2005-05-29
    body: "There always is a project participation entrance barrier. How can \"truck numbers\" be reduced. Translation should not be an easy task for average joe contributors, not require special skills such as CVS knowledge. I of am afraid of 80% translations of KDE main languages and it shall not become a bottleneck for KDE 4. Some premature web based contribution engines such as IRMA and pootle adress the important issue. However now the translation does not catch up quickly enough."
    author: "Gerd"
  - subject: "Re: Internationalisation"
    date: 2005-05-29
    body: "Hello,\n\ncan you explain more why internationalisation must be quick?\n\nIs it really going to be a disaster when it only catches up in 4.0.3?\n\nI personally see no reason why i18n cannot be done only once the bugs are being ironed out...\n\nThat said, I often see wrong/unappropiate translations into my language. What I would love to have is a \"tool\" to run and submit the change proposal to some list of people who take it and put it to cvs.\n\nKay"
    author: "Debian User"
  - subject: "Re: Internationalisation"
    date: 2005-05-30
    body: "It is one of the policy of KDE to release translations with the first stable version. (Yes, some other projects do otherwise.)\n\nAs for the \"tool\", you can use KDE Bugs: http://bugs.kde.org\n\nHave a nice day!\n\n"
    author: "Nicolas Goutte"
  - subject: "Re: Internationalisation"
    date: 2005-05-30
    body: "> That said, I often see wrong/unappropiate translations into my language.\n\nAnd how does that happen? Because some translators never run the application they translate or never the development version of it prior release. Those web translation interfaces intensify this effect additionally as translators may not even be interested in/run KDE. They get single strings presented and don't know how terms are translated elsewhere within the same application or elsewhere in KDE. And likely they don't even know the tools or lists/ways how to report ambiguous strings back to the developers of the for translation process aggregated projects.\n\n> What I would love to have is a \"tool\" to run and submit the change proposal to some list of people who take it and put it to cvs.\n\nhttp://bugs.kde.org exists: product=\"i18n\", component=your language."
    author: "Anonymous"
  - subject: "Re: Internationalisation"
    date: 2005-05-31
    body: "For reporting bugs in the translation, the Dutch team uses a html-interface:\nhttp://www.kde.nl/helpen/bugs.html\n\nwich can be used besides bugs.kde.org\n"
    author: "Rinse"
  - subject: "Re: Internationalisation"
    date: 2005-05-30
    body: "> Translation should not be an easy task for average joe contributors\n\nNo, I would prefer quality over quantity anytime.\n\n> not require special skills such as CVS knowledge\n\nYou don't need CVS knowledge for KDE (anymore). :-)"
    author: "Anonymous"
  - subject: "Re: Internationalisation"
    date: 2005-05-30
    body: "Only the I18N/L10N team leader needs a SVN account. The others do not and as far as I know, some teams are working that way (by dispatching the work).\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Internationalisation"
    date: 2005-05-31
    body: ">>There always is a project participation entrance barrier.\nWhich is mostly in the head of the people, I found that joining a team is quite easy, and that for every type of skill there is a job available.\n\n>>Translation should not (not?) be an easy task for average joe contributors, not require special skills such as CVS knowledge.\nTranslating/localizing software does not require special skills that are too hard for joe user. For example, the Dutch translation is coordinated by a cook. How hard can it be if even someone with no link to ICT, like a cook, can handle it?\n\n>Some premature web based contribution engines such as IRMA and pootle adress the important issue.\nThe problem with pootle and IRMA (AFIAK) is that it does not use a translation memory. If you have a large amount of applications translated, you can use those translations to translate new apps for about 40-60% (considering simple apps). This increases the consistency between apps and reduces workload for the translators.\n\n\n> However now the translation does not catch up quickly enough.\nKDE is shipped with 50 languages, which is quite a lot already. \nThe bottleneck in translating KDE is not the skills joe contributer should have in order to use the special tools for translating, it is the knowledge of the English/Native language, and the ability to figure out how a string is used in the application. Also, translation is very time consuming- not everyone has the time to participate in a translation project.\n\nBut anyone who prefers KDE in their own language should team up with their translation team. Even small contributions, like proofreading stuff or giving comments about the translation while using KDE, are more then welcome.\n\nYou can find your team on http://i18n.kde.org"
    author: "ac"
  - subject: "Re: finish the Kontact port to Windows"
    date: 2005-05-30
    body: "What is its state? Anybody else working on it?"
    author: "Anonymous"
  - subject: "OT: Linux & communism..."
    date: 2005-05-30
    body: "http://nat.org/2005/may/\n\nRed wine and a Rolex, I wanna work for Novell as well...\n\nMaybe I should just go on an create a \"Linux\"-company, maybe it'll be aquired :-D"
    author: "ac"
  - subject: "Re: OT: Linux & communism..."
    date: 2005-05-31
    body: "Maybe one of the advantages of KDE people is that they are somehow more \"free\"\nin their mind, and not under pressure of $10k+/month salaries and bound to \ncreate \"shareholder value\", but more \"desktop-user value\"..."
    author: "me 2 ac"
  - subject: "eGroupWare has GroupDAV support too"
    date: 2007-11-18
    body: "It's available in svn for the development branch and the stable 1.4 branch. We fully support now contacts, events and ToDos with full folder discovery for Kontact. It's planned to have a further folder hierarchy allowing to access the addressbooks and calendars of other users or groups (not yet implemented). The implementation has been tested with Kontact of cause and the Thunderbird GroupDAV addressbook pluging (aka SOGO connector from www.inverse.ca). For more information see http://www.egroupware.org/wiki/GroupDAV\nRalf"
    author: "Ralf Becker, eGroupWare project"
---
Our final interview <a href="http://dot.kde.org/1116452031/">in</a> <a href="http://dot.kde.org/1116675449/">this</a> <a href="http://dot.kde.org/1117225235/">series</a> with the hackers at the on-going <a href="http://pim.kde.org/development/meetings/nlpim1/index.php">Dutch KDE PIM meeting</a> is with none other than KDE-PIM module project leader of Cornelius Schumacher. Enjoy the interview.









<!--break-->
<div style="float:right; width: 150px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/cornelius.jpg" width="150" height="204" alt="Cornelius Photo" /><br />Cornelius at the KDE PIM Conference</div>
<p><strong>Cornelius please tell us who you are and what your role is in the
KDE project.</strong></p>

<p>I have been around for quite a while now. I recently found the first patch I 
contributed to KDE when looking through old mails. It was a fix for the 
development version of KOrganizer which made it not crash when starting 
up. This was during the porting from KDE 1 to KDE 2. Funnily enough I 
sent the patch to the KOrganizer mailing list exactly on this day six 
years ago.</p>

<p>My main focus has been KDE PIM from the beginning, but I have also done 
some stuff in the core libraries and other stuff like maintaining the 
KDE Help Center.</p>

<p>As my day job I work for SuSE/Novell.</p>

<p><strong>Can you
tell us a bit more about what you are currently working on? If you
could code 24h a day for six weeks in a row, what would be the thing
you would do?</strong></p>

<p>I'm currently working on usability and collecting ideas for KDE 4. I'm 
also experimenting with some new technology, e.g. Superkaramba. If I 
could have a couple of weeks of unexpected extra spare time for coding 
I would probably finish the Kontact port to Windows.</p>

<p><strong>We have seen several developers in interviews and blogs talk about the KDE PIM event in the Netherlands and what they are planning to work on
during the meeting. Do you have any plans or ideas for this meeting?</strong></p>

<p>There are two big goals I would like to achieve at the meeting. First, 
creating a roadmap for KDE PIM 4. Second, relaunching the KDE-PIM web 
pages with some fresh and rejuvenated content. But I'm sure there will 
also come up some new ideas at the meeting.</p>


<p><strong>I saw the attendance list of the PIM meeting and it seems that the people attending have had a tight relationship with each other for a while now.  Would it good to have some fresh blood in the group?</strong></p>

<p>We always had some people at the KDE PIM meetings which nobody has met 
in person before. It's true that there is a fairly stable core of 
people, but it's certainly possible to get fresh blood in the group. 
This time we for example have Frank Osterfeld of Akregator fame, who
is a new member of KDE PIM.</p>

<p><strong>We heard that some usability people are coming. What's their role
during the meeting?</strong></p>

<p>That's part of the quest to deeply integrate usability work in the KDE 
development process. We already had quite some success with it and I 
think usability has never been as present in the mind of the KDE 
developers as it is now.</p>

<p>We try to get usability feedback as early in the development process as 
possible, so that it can really have the impact which is needed to 
create high-quality user interfaces. Having around the usability people 
when thinking about KDE PIM 4 will be a great asset. I'm sure that KDE 
4 will be the most user friendly KDE ever.</p>

<p><strong>What can we expect from KDE PIM in KDE4? Is there some roadmap?</strong></p>

<p>There are lots of ideas floating around, but we haven't really started 
the concrete discussion about KDE PIM 4 yet. I expect the we will come 
up with a first roadmap at the meeting. </p>

<p><strong>What's the status of the submitted RFC GroupDav, a protocol the PIM
people came up with during the last meeting together with hackers
from Opengroupware.org?</strong></p>

<p>GroupDav is not a formal committee standard. It has been created from 
the needs for standardization which arose from actually implementing 
communication between OpenGroupware.org and Kontact. It's meant to be a 
pragmatic standard, which makes it easy to implement, both on the 
server side and on the client side without introducing much new 
technology.</p>

<p>The beauty of GroupDav is that it makes extensive use of existing 
standards and the existing development infrastructure, so that we can 
take profit of having the excellent WebDav and HTTP kio slaves and our 
iCalendar and vCard libraries.</p>

<p>GroupDav has been adopted surprisingly well. There is work going on to 
add GroupDav support to Evolution and SunBird, the Mozilla calendar 
project. With Citadel there also is a second server implementation.</p>

<p><strong>Isn't groupware in OSS becoming difficult now that Hula groupware
server is supporting CalDav instead of the GroupDav protocol?</strong></p>

<p>Quite the contrary. I think Hula is is a great thing for open source 
groupware. It's not the first open source groupware server, but it puts 
the focus on the right aspect, on creating a system which people love 
to use. This can get the groupware thing out of the dark corporate 
corner where it currently hides and annoys the people which are forced 
to use it. To be fair I have to say that Hula is not the only system 
which is trying to achieve that. Others are doing this as well.</p>

<p>About CalDav I would like to add, that there is no conflict between 
GroupDav and CalDav. GroupDav can be seen as the continuation of 
classical iCalendar over HTTP which KOrganizer has supported since version 
2.0 (released almost five years ago) and what Apple has tried 
to make proprietary with their "webcal" protocol. GroupDav is the pragmatic 
approach to create a protocol which takes more advantage of having a 
server, providing more fine-grained access and a solid base for syncing 
and caching data by exploiting the lesser known features of HTTP 1.1. 
CalDav is one more step. It adds more power, but on the other hand also 
hasn't the scope of GroupDav, as it only targets calendar data.</p>

<p>Once Hula has usable CalDav support, Kontact will support it. KDE PIM 
has a track record of implementing server access within days. Hula will 
be no exception. Let's see how things evolve, but maybe the 
Hula/Kontact combo will be the killer application which will bring 
groupware to the common user.</p>

<p><strong>Where do you think KDE PIM shines?</strong></p>

<p>What I like most about KDE PIM is the community. There are lots of great 
people and if you look at the commit digest KDE PIM usually is the most 
active KDE core module. It's really fun to be part of this community.</p>

<p>Another aspect is the excellent technology which has emerged from KDE PIM. It 
always has been a breeding ground for new KDE technology. For example 
KConfig XT or KNewStuff were born in KDE PIM.</p>

<p>Finally, there is Kontact which is a real killer application. It's kind 
of the essence of KDE PIM. I really enjoy all the nice comments and 
good feedback about Kontact. The best thing about Kontact from a 
developers perspective is that it has a very solid architecture. This 
provides the potential to quickly adapt to whatever future requirement 
will arise.</p>

<p><strong>What part of KDE PIM needs some serious attention?</strong></p>

<p>As a user I would put my attention on Kontact. That's where most of the 
exciting stuff will happen.</p>

<p>If your question is asking which areas of KDE PIM I think needs work, I 
would mention the KMail internals. Mail still is the heart of personal 
information management and systems like Gmail show that there can be 
quite some innovation in this area. Although it has improved 
significantly recently, the core of KMail still needs some work 
to make it as flexible as it could be, so that we can better focus on 
providing a usable, innovative and polished  user interface.</p>

<p><strong>But all the developers of KMail are already working as hard as they
can, as far as I can judge. How do you see that is going to happen?</strong></p>

<p>It's not about creating extra work. Cleaning up the code will quickly 
save us work, because it allows us to implement new features with less 
effort and do bug fixes faster and more reliable. Constant refactoring 
is an essential part of open source development, especially in KDE. KDE 
4 will provide us with the opportunity to do some of the stuff we havn't
dared to touch in the past because we will have to work on the code for 
Qt4/KDE4 porting anyways.</p>

<p><strong>What future do you see for KDE PIM?</strong></p>

<p>Well, I don't have my crystal ball at hand, but I feel that the future 
of KDE PIM will be bright ;-)</p>

<p><strong>So another thing we heard is that you are leaving to go to GUADEC in
Germany. Could you tell us more about that? Any more KDE people going
to be there?</strong></p>

<p>Some people have asked me, why are you going to GUADEC, aren't you a KDE guy? I tell them, yes, I'm a KDE guy and that's why I'm going to GUADEC. GNOME 
and KDE are the dominant desktop projects on Linux and other Unix 
systems. We have the same goals and there are lots of areas where we 
can work together. We should jointly go for the 98 percent of Windows 
desktops out there and not fight each other. A bit of healthy 
competition is quite useful though.</p>

<p>I will meet some people on GUADEC to talk about cooperation and 
standardization. One topic is a common documentation standard, another 
is syncing. There certainly will be others.</p>

<p>There will be a number of other KDE people at GUADEC. Have a look at the 
<a href="http://wiki.kde.org/tiki-index.php?page=KDE+%40+GUADEC+2005">KDE Wiki</a> for more details.</p>

<div style="width: 500px; padding: 1ex; border: solid grey 1px; text-align: center"><img src="http://static.kdenews.org/jr/cornelius-group.jpg" width="500" height="183" alt="KDE PIM Hackers" /><br />The KDE PIM Meeting Group Photo, see <a href="http://www.kdedevelopers.org/node/view/1108">Fab Mous's blog entry</a> for names.</div>

<p>More photos of the conference are available on kde.nl.</p>
<ul>
<li><a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2705/images.html">Friday</a>
</li>
<li><a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2805/images.html">Saturday</a>
</li>
<li><a href="http://www.kde.nl/agenda/2005-kdepim-meeting/pics/2905/images.html">Sunday</a>
</li>

