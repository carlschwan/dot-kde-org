---
title: "Linux Magazine: Konqueroring Worlds"
date:    2005-06-07
authors:
  - "dmolkentin"
slug:    linux-magazine-konqueroring-worlds
comments:
  - subject: "What is new ?"
    date: 2005-06-07
    body: "What is new in the article ? Nothing, i think. But anyway, it is nice to read that people are learning about the advanced Desktop they already have, but they didnt know about. I know so many Linux users that just use command line, and KDE for common tasks. Once they see what KDE can do, and the technology behind it, they start to say: WOW, so it IS better than windows.\n\nI just cant understand why people always complain about how big konqueror is, or how difficult it is to learn the advanced features. Well, if you are not an advanced user, or do not want to be, and want to use konqueror, you still can. This is the best software: advanced for those who want, simple for those who wants simple things. I think it is the sensation of hitting a wall: you are coming from the linux CLI world and see how good is the Linux Desktop, and you start complaining about the bloated Desktop. In one week you stop complaining and cannot live without it anymore."
    author: "Henrique Marks"
  - subject: "Re: What is new ?"
    date: 2005-06-08
    body: "I suppose I am different from quite a few Linux users as I came from using Windows and pretty much only using the GUI. I took right to using Konqueror and the graphical interface in KDE right away- and now am learning how to use the command line. I don't think the GUI is very bloated, but maybe it is because I'm comparing to Windows and its resource-hungriness. I guess it is all what you have to compare it with, and for me, KDE on top of the Linux kernel runs a lot better than explorer32.exe on top of the NT 5.1 kernel."
    author: "SuSE_User"
  - subject: "Missing feature"
    date: 2005-06-08
    body: "Until Konqueror 3.3, there was a little square in the bottom of the scroll bar that you could (un)select if you wanted, for instance, to sync the embebed terminal current working directory with the file manager one or not. I found it very useful to disable the sync sometimes... What happen with this feature?\n\nKeep up the good work!"
    author: "blacksheep"
  - subject: "Re: Missing feature"
    date: 2005-06-09
    body: "Looking at my konqueror (kde 3.4.1): it is still there :)\n\nRinse"
    author: "Rinse"
  - subject: "Re: Missing feature"
    date: 2005-06-09
    body: "Well, in my Mandriva's LE 3.3 KDE, there are no checkboxes to (un)sync filemanager and embeded console, as you can see appended. Maybe this is just Mandriva's issue."
    author: "blacksheep"
  - subject: "Re: Missing feature"
    date: 2005-06-09
    body: "I see, it is gone :o)\nPerhaps using another theme solves this?\nI use suse, but I don't expect that either distro changed konqueror to display/not display the checkbox.\n\nRinse"
    author: "Rinse"
  - subject: "Now if only I had an easy way of forcing fonts..."
    date: 2005-06-08
    body: "..  I mean, without having to have a degree in web design and css template writing :(\n\nI mean, just about every other browser in the history of teh intarweb has a \"override site fonts\" or \"use my fonts\" option, and not having that option is suck."
    author: "Mathew Hennessy"
  - subject: "Re: Now if only I had an easy way of forcing fonts"
    date: 2005-06-08
    body: "yes, i'm missing this too.\nBut i think font-handling is still a prblem of konqueror, khtml  or Qt. I'm not sure were the problem exactly is, but depending on selected font and font size konqueror have problems with displaying some characters, e.g. the quotation mark (not the default one, but this one who can be generated with special html code(&#8220;) and some more). The same happens on other application like knewsticker or kmail, therefore i think it's more an khtml or Qt problem than a konqueror problem...\n\nBy the way. Does someone use a panel on the top of the desktop? I want to use one, but if i use such a panel, the desktop-icons don't stay on there position and moved to another position everytime a start KDE, so i can't use such a panel. :(\nDoes someone know the problem or even a solution?"
    author: "pinky"
  - subject: "Re: Now if only I had an easy way of forcing fonts"
    date: 2005-06-09
    body: "Yes I have the same situation. I solved the icon problem partially by moving them to the right hand side then, after closing everything else, saving the session. Since I only ever save the session manually, the icons tend to stay where they are for a while."
    author: "drooghead"
  - subject: "Re: Now if only I had an easy way of forcing fonts"
    date: 2005-06-09
    body: "Have you tried disabling \"align to grid\"? I have a top panel as well and the icons stay in place."
    author: "Illissius"
---
<a href="http://www.linux-magazine.com/issue/56">This month's issue</a> of <a href="http://www.linux-magazine.com/">Linux Magazine</a> features an article about <a href="http://www.linux-magazine.com/issue/56/Konqueror_3.4_Tricks.pdf">working with Konqueror 3.4</a> (PDF). It presents the new features of this release and highlights the most interesting tricks of KDE's <a href="http://konqueror.kde.org/">universal browser</a>. Learn how to read man and info pages, how to customize Konqueror with view profiles and how to get the most out of KIO-Slaves.
<!--break-->
