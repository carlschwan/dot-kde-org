---
title: "KDE Kiosk Tutorial"
date:    2005-03-02
authors:
  - "ctennis"
slug:    kde-kiosk-tutorial
comments:
  - subject: "Excellent!"
    date: 2005-03-02
    body: "This is just the sort of thing we need to promote the excellent technologies that KDE offers. I feel sometimes that we have a lot of great features/technologies/options that aren't immediately visible to users, and therefore get undeservedly neglected. Tutorials like this should really help to remedy this - it'll definitely be getting a link in the User Guide"
    author: "Philip Rodrigues"
  - subject: "KDE cant possibly create enough hype around Kiosk!"
    date: 2005-03-02
    body: "It is great to see another thourough KDE Kiosk article/tutorial!\n\nToo rarely are written the documents describing the really world class KDE technology features.\n\n(I bet if the GNOME group had anything close to the power of Kiosk in their hands, the world would long have learned about it, even before the technology ever came into existence. For KDE, its developers and supporters seem to be far too shy to talk about the Good Things (TM) they have created. My dream of Linux desktkop world domination would be much more close today if we could combine the superior KDE technology core with the GNOME-type marketing machinery instead of the infighting that still happens all the time....)\n\nSigh."
    author: "konqui-fan"
  - subject: "Re: KDE cant possibly create enough hype around Kiosk!"
    date: 2005-03-02
    body: "the answer is for us to \"tell the world\"!\n\neveryone can help here. collect your list of favourite links to articles and technologies about KDE and be sure to share them often. if done with factual accuracy and an upbeat nature, it goes a long way to helping KDE out =)\n\nwe're getting better at this, one person at a time =)"
    author: "Aaron J. Seigo"
  - subject: "Validate"
    date: 2005-03-02
    body: "It was pointed out to me that there are a couple of little validity errors in the tutorial which make it render wonkily in Gecko-based browsers. The diff at http://users.ox.ac.uk/~chri1802/stuff/index.html.diff makes it validate, and appear correctly in Moz/Firefox/etc (AFAICT)."
    author: "Philip Rodrigues"
  - subject: "Re: Validate"
    date: 2005-03-03
    body: "Thank you - I've patched the tutorial."
    author: "Caleb Tennis"
  - subject: "Plug for a project"
    date: 2005-03-04
    body: "My company is working on a kiosk project that is based in Linux and one of the results of it has been an open source fullscreen browser built with KHTML that has an on screen keyboard, support for playing sounds and a couple other options right now.  For those interested in some sort of full screen presentation/public information kiosk, maybe it's worth looking into. http://kioskbrowser.sourceforge.net"
    author: "Ph0nK"
---
Originally appearing in <a href="http://www.linuxjournal.com">Linux Journal</a>, a new <a href="http://developer.kde.org/documentation/tutorials/kiosk/index.html">KDE Kiosk tutorial</a> is now available.  It provides a great starting point for those interested in learning how the powerful KDE configuration framework is set up, with further insight into how to centrally customize a desktop setup.  It also provides examples and pictures of various configuration options and demonstrates the many desktop restrictions available to an <a href="http://www.kde.org/areas/sysadmin/">administrator</a>.
<!--break-->
