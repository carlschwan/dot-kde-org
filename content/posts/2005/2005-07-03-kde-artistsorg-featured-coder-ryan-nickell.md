---
title: "KDE-Artists.org: Featured Coder Ryan Nickell"
date:    2005-07-03
authors:
  - "kteam"
slug:    kde-artistsorg-featured-coder-ryan-nickell
comments:
  - subject: "Hmm"
    date: 2005-07-03
    body: "My favourire Windows scheme is slate pcd + mystic colour scheme, it looks great."
    author: "peter"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "Any screenshots, or maybe a link to kde-look?"
    author: "ac"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "I use it on Win too because it looks great with the Safaris style skin for firefox. However you have to patch XP to use the Longhorn skins. "
    author: "Aranea"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "Hmmm, your web browser still has underlining of links?  That's so 1990s!"
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "yes, 1995"
    author: "Aranea"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "No, underlining links is basic usability."
    author: "Erik Hensema"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "yes, its one of the first things i turn on in konqueror when i install KDE."
    author: "superstoned"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "yes I do that as well"
    author: "ac"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "Um, it's just clutter and one of the reasons people have been criticizing KDE for looking cluttered.  You can just turn on hover which will underline the link at that time instead of all the time, the rest of the time the link should be indicated by color.\n\nUnderlining is used VERY RARELY in the publishing business for a reason you know."
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-03
    body: "Underlyning in Konqueror file manager was a mess as everything was underlined. But for web pages? "
    author: "gerd"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "See attached screenshot.  One of these is an unmitigated mess.  There is an excess of underlining that serves no function that the colored links don't already do.  The other is a professional presentation."
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "If one has any sort of a colour deficit at all (even a minor one) they will _not_ be able to tell what are the links and what aren't.\n\nYou're confusing usability with aesthetics.\n\nUnderlining isn't used in the publishing business because italics looks better, not because it's unusable. Underlining links is a clear way to indicate their existence. In your attached screenshot, I can pick out the links immediately in the lower, underlined, section, whereas it takes a bit longer - due to the poor contrast between navy and black - in the upper, non-underlined, section."
    author: "Bryan Feeney"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "some sites i read don't give links another color, the only way to see them is to underline them..."
    author: "superstoned"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "Those sites are unusable, but KDE can underline on hover!  It's great!"
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "yes, but i'm not willing to slowly move my mouse over all text to find out where the links are... the site that annoyed me most was the now closed http://www.expert-zone.com/. impossible to find the links to the news without konqi forcing to underline them."
    author: "superstoned"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "Haha, yes that site sucks so bad.  Thanks for a good laugh. :-)\n\nI don't know, maybe with CSS stylesheets you can force links to be a different color."
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "> You're confusing usability with aesthetics.\n\nAgreed.\n\nI don't have any kind of colour deficit whatsoever (I've done the tests), and I still think the underlined version is far more readable.  The difference in colours is too subtle.\n\nPeople saying underlines make things \"cleaner\" remind me of myself when I was a kid.  I used to reduce the size of the scrollbars and toolbars, use a small font size, use a funky colour scheme, etc.\n\nAfter a while, I figured out that smaller isn't faster, darker and \"cleaner\" isn't more readable, in fact most of the things I was doing to \"optimise\" my interface were actually counterproductive.\n\nBy all means, people should be free to make their desktops look good.  But usually it makes them *less* usable.  And a desktop that works well is more important to me than a desktop that looks pretty.\n"
    author: "Jim"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "Well it makes sense for people with failing eyesight or have accessibility issues.  It does not make sense for normal-abled people.\n\nWould you have an On-Screen Keyboard enabled by default just because it supposedly makes KDE more \"usable\" for a minority, even though it creates clutter?"
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "Perhaps that might be because the publishing business doesn't use hyperlinks?"
    author: "taj"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "Nope, as someone said it's because italics and sometimes bold look much better.\n\nAs for hyperlinks, heard of PDF?  No underlining there, my friend."
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-04
    body: "I'm glad you brought up PDFs. Most of the documents are created by people with limited hyptertext thinking, leading to most of them having serious hunt-the-pixel-to-find-the-hyperlink issues. This is the 21st century. Hypertext is not a 1980s graphical adventure game.\n\nIf we coopt bold and italics for hyperlinks, we are going to have to come up with new ideograms for headings, quotations and emphasis (traditionally denoted by boldness and italics). Underlined text? ALL CAPS? Animated dancing bears?"
    author: "taj"
  - subject: "Re: Hmm"
    date: 2005-07-05
    body: "Sigh.  Didn't we already cover this?  That's what the link colors are for.  You know?  Now, I yield to you that some people have disabilities, but then again we already covered this ground."
    author: "Anonymous"
  - subject: "Re: Hmm"
    date: 2005-07-05
    body: "> Sigh. Didn't we already cover this? That's what the link colors are for. You know?\n\nYou may want to re-read the whole thread.  Quite a few people disagreed with you on that.  And it wasn't only about people with disabilities... \n\nI didn't get the feeling that that case was closed. Nor is there a need to.  The Konqi option is not going away. \n\n"
    author: "cm"
  - subject: "About his win deco"
    date: 2005-07-03
    body: "He's the author of Smooth Blend \nI really like his Smooth Blend Window Decoration :). \n\n"
    author: "Micha\u00ebl Larouche"
  - subject: "What is your favorite browser? "
    date: 2005-07-03
    body: ">Ryan Nickell: Don't throw tomatoes at me for this one.. I use Firefox currently. \n\nWhat's better in firefox or which feature is missing in konqueror dude? Just wanna know what makes you like firefox. Could you answer that please... Ryan!?"
    author: "fast_rizwaan"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "how do you use mouse gestures in konq?"
    author: "pholie"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "You configure them under Accessibility/KHotKeys"
    author: "Anonymous"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "Most of the time I use Firefox too.\nThere are only three things I am missing from Konquerer.\n\nThe first one is a Javascript engine as fast as Firefox'. Konquerer's Javascript is so slow that I can't use it on several forums and other sites which I visit regualary because Konquerer freezes several seconds because of the scripts. With Firefox this doesn't happen.\n\nThe second one is the nice highlighting when searching. This is something I really like with Firefix.\n\nThe last one is plugins. There are some very cool Firefox plugins, like webdeveloper, dictionary lookup, etc. Very convenient.\n\nOtherwise Konquerer has gone a long way and is a great browser.\n"
    author: "Andreas"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "> The second one is the nice highlighting when searching\n\nDoesn't Konqueror highlight the text as well?"
    author: "Kevin Krammer"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "Do you mean \"/ Search while typing\" and \" ' Search hyperlinks while typing\"\n\n(I'm translated them, my konqueror is in spanish, it could be different)"
    author: "pepebotika"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "Yes, I mean the search-while-typing feature. Firefox has a checkbox on the search toolbar (the one below the document space) to highlight all occurenctes of the search string in the document. \n\nJust like 'set hlsearch' in vim."
    author: "Andreas"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "yes, this is very cool. also the way it says \"plugins are needed, download them here\", and the slightly better popup blocker integration. altough the windoze popupblocker allows to show a blocked popup, that's still missing in firefox AND konqueror."
    author: "superstoned"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-04
    body: "I think the fundamental problem is that Konqueror tries to be a file browser (http's just a fancy ioslav right ;-) ) and a file-manager at the same time. These are two separate tasks. Might I suggest the unthinkable, and promote the ideas of two apps for KDE4? One a dedicated file-manager, and one a dedicated browser? \n\nFor example, it was always very annoying that the home button could only link to your home dir or a homepage, but not both. Likewise, the existence of two appearance sections (albeit under different names) in the Settings dialog was always a bit odd.\n\nA side effect of this would be that it might reduce the number of menu items and configuration options, which are intimidatingly numerous and complex."
    author: "Bryan Feeney"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-04
    body: "I'm with you here :) I really like konqueror but it desperately need a clean up in both areas. When browsing web I need those webbrowser functionalities like history (and easy searching from it) etc. "
    author: "Petteri"
  - subject: "Re: What is your favorite browser? "
    date: 2005-08-06
    body: "I disagree, you can completely customize your konqueror to be just the file browser you want, and you can use another browser for your web, or you can even use others, why take away an option for those who happen to like the multitask way of konqueror, in favor of something that already exists???\n(although I partially agree on that home button stuff, but then again, there is something like the bookmarks-line where you can have your favorite bookmarks at hand)."
    author: "Terracotta"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-04
    body: "\"dictionary lookup\"\n\nYou can always use dict: $wordToLookup  \n\nCheers!"
    author: "am"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "Konqueror totally sucks with adblocking and pop-up windows. The configuration menu contains 100 useless option. "
    author: "SinDios"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "Konqueror badly needs a list of closed windows (in order of closure) which you can recover after you've closed them (accidentally or because you want to lok sth. up again you've just read).\n\nOpera has this, in v8 this even has a trashcan icon - so you can think of it like trash:/ on the desktop; you still have access to closed sites before they're irrecoverably lost (ok you can always torture your brain for hints on where you found the information you have in mind, or try to surf back to this specific location).  Just hit CTRL+Z and the tab is back. No, the general history of visited sites is not the same.\n\nAnd it needs some kind of crash handler which brings up the sites you we're browsing when the browser crashed at the next startup (restore session). Crashes do happen (through the application or by bad html) and it's very annoying when you've lost a whole surfing session with several tabs.\n\nSomeone else missing these, too?"
    author: "Phase II"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "> And it needs some kind of crash handler which brings up the sites you we're browsing\n\nkdeaddons has as \"Crashes Monitor\" Konqueror extension."
    author: "Anonymous"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "yep, and i guess its very easy to create an \"closed websites\" extention? please, i'd love to see one..."
    author: "superstoned"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "What about F9 + History (the clock)? I use it every now and then to go again to a website I've looked at some time before."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-03
    body: "usefull, but not half as usefull as it could be. mostly because it annoys me by constantly changing (yeah, obvious, but it still does, and that's not very usefull)"
    author: "superstoned"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-05
    body: "What about a search box and/or a lock history button?"
    author: "ninj"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-05
    body: "The history sidebar has a search box."
    author: "Anonymous"
  - subject: "Re: What is your favorite browser? "
    date: 2005-07-07
    body: "i don't want to SEARCH for the tab I just opened, the point is i want a FAST AND EASY way to re-open it."
    author: "superstoned"
  - subject: "like his non-kde apps.."
    date: 2005-07-09
    body: "surprised on the amount of non-KDE apps he uses, The Gimp, Openoffice, Evolution and Firefox. KDE maybe a good desktop environment, doesnt mean KDE has better applications."
    author: "Salsa King"
  - subject: "Re: like his non-kde apps.."
    date: 2005-07-09
    body: "Well, \n\n- Krita couldn't be taken seriously as competitor of Gimp before the release of KOffice 1.4 which is only some weeks old.  Let's see what the future brings here ...\n- OpenOffice still has much better MS Office filters.  So if that's important to you the choice is a no-brainer.  Startup is dog-slow though...\n- Evolution:  I don't think Kontact is any worse than Evo.  And he said he wanted to switch.  \n- Firefox:  It's a good browser and even cross-platform, so why not?  Makes a nice backup browser for me for the few pages that Konqi does not handle...\n\nOften people just use what they are used to.  It's not only about current quality. \n\nYou cannot take it for granted that KDE always has the best solution for any particular problem.  It's not like other developers are bad or stupid!  \n\nThe good news is that for many common sets of requirements it's possible to find a KDE app that's up to the task, and quite a few times it has even the best-of-breed solution (but that judgement is subjective, of course).  In addition to the functionality, the KDE apps have integration into and consistency with the KDE desktop and its other apps as strong points.\n\n"
    author: "cm"
---
The <a href="http://www.kde-artists.org">KDE-Artists website</a> is featuring an <a href="http://kde-artists.org/main/content/view/50/29/">interview with Ryan Nickell</a>, one of the current authors of <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> and <a href="http://kde-look.org/content/show.php?content=21107">Smooth Blend</a>. He talks about his baby SuperKaramba, the KDE community website <a href="http://www.kde-look.org/">KDE-look.org</a>, <a href="http://plasma.bddf.ca/">Plasma</a>, KDE 4 and he even answers some personal questions. Enjoy the interview!


<!--break-->
