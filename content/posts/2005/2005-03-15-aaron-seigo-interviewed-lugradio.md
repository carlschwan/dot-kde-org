---
title: "Aaron Seigo Interviewed on LugRadio"
date:    2005-03-15
authors:
  - "jriddell"
slug:    aaron-seigo-interviewed-lugradio
comments:
  - subject: "Pretty one sided guys"
    date: 2005-03-15
    body: "The Lug radio guys seem to be pretty one sided, but Aaron came across well (much better than the interviewer, it has to be said). One thing he didn't mention however, is that most of the commonly used freedesktop standards are based on KDE specifications and implementations in the first place. For example:\n\n- The .desktop files (based KDE's .kdelnk specification) \n- The system tray protocol (based on KDE's tray protocol) \n- DBUS, based on DCOP \n\nOthers developed elsewhere have been in use in KDE for years: \n\n- XBEL (used since KDE 2.1) \n- XDnD (used since KDE 2.0 IIRC) \n\nIt should however be noted, that just because a proposal is on freedesktop.org doesn't make it a standard, or even a good idea. There are (and should be) both good and bad ideas there, and the desktops can vote with their feet. \n\n(Note, I know I've posted almost the same thing on OSNews - they posted the same story).\n"
    author: "Richard Moore"
  - subject: "Re: Pretty one sided guys"
    date: 2005-03-15
    body: "Hello Rich\n\nThanks a lot for the topo. \n\nThe site is prettily hit right now. So no avail. This is one of those situations where speach2text would be a wonderful tool to have. What do you say? You're quite experimented with text2speach. Do you think that just dividing 1 by all the variables presented in festival would give the inverse code? ;-)"
    author: "Inorog"
  - subject: "Re: Pretty one sided guys"
    date: 2005-03-15
    body: "Well, the typical tts program sounds like a dalek, and the typical stt program requires you to talk like one.\n\nI'd say yes :)"
    author: "joe lorem"
  - subject: "Re: Pretty one sided guys"
    date: 2005-03-15
    body: "And they're rude, trying to interrupt Aaron multiple time (not mentioning stopping the interview so early ....)\n\nBah.... frustrating :)"
    author: "ac"
  - subject: "Re: Pretty one sided guys"
    date: 2005-03-15
    body: "nah, i don't they were being rude.. it's just the format of the show really: a big open chat. like a night at the pub. only more sober."
    author: "Aaron J. Seigo"
  - subject: "Re: Pretty one sided guys"
    date: 2005-03-15
    body: "No I don't think so either. Yer, OK everyone thinks that they're pro-Gnome, and they are, but you should have heard the interview they did with Miguel de Icaza a few weeks ago. They didn't exactly give him an easy ride - especially the Python guy! When he came up with the 10 most popular Mono apps on OSNews thing they pretty much laughed at him!"
    author: "David"
  - subject: "Re: Pretty one sided guys"
    date: 2005-03-15
    body: "all very good points Richard ... unfortunately there was only so much time and we covered a lot of topical ground so it wasn't possible to fully examine any of the topics really; just the nature of the format... thank goodness for web forums.\n\nwait. did i REALLY just say that? ;)"
    author: "Aaron J. Seigo"
  - subject: "damn, it's dotdotted"
    date: 2005-03-15
    body: "I don't suppose anyone has a mirror? :)"
    author: "Leo S"
  - subject: "Re: damn, it's dotdotted"
    date: 2005-03-15
    body: "I've uploaded a copy to a high-bandwidth server, for the greater good.. \n\nhttp://www.bluelinux.co.uk/~jefford/lugradio-s2e11-150305-high.ogg\n\nenjoy (-:"
    author: "je4d"
  - subject: "Re: damn, it's dotdotted"
    date: 2005-03-15
    body: "torrents at\nhttp://lugradio.bishopburton.ac.uk/"
    author: "evan"
  - subject: "Re: damn, it's dotdotted"
    date: 2005-03-15
    body: "Perfect.  Thank you."
    author: "Leo S"
  - subject: "Transcript?"
    date: 2005-03-15
    body: "Is there a transcript available?"
    author: "testerus"
  - subject: "Re: Transcript?"
    date: 2005-03-15
    body: "I think we can assume no."
    author: "Ian Monroe"
  - subject: "Dissapointing really"
    date: 2005-03-15
    body: "The interview was ok, it's great to hear Aaron's thoughts on these things, but the more I listen to this episode, the lower my opinion is of these guys.\n\nThey think KDE 4 should be rewritten from scratch?  C'mon.. I thought these guys had jobs as developers.  They should know better.\n\nKDE will be forked?  Eh?  By who?  I haven't heard of any actual developers being that dissatisfied with the project.\n\nKolab is bad because it interoperates well with KDE?  What kind of inane complaint is that?  I'd rather complain about the lack of marketing for Kolab.\n\nOn the other hand, they admitted some of their complaints about KDE were born from bias or based on old versions, so that's a start.\n\nThanks for the interview!"
    author: "Leo S"
  - subject: "Re: Dissapointing really"
    date: 2005-03-16
    body: "> Kolab is bad because it interoperates well with KDE? What kind of inane \n> complaint is that?\nIt's kind like how Microsoft application interoperate with only Microsoft applications.\n\nSorry for spreading any FUD.\n\n\n"
    author: "Anonymous Specutator"
  - subject: "Re: Dissapointing really"
    date: 2005-03-16
    body: "Okay, for a start, only one of us works as a developer.\n\nSecondly, we were just discussing the ideas of whether it's a good idea for KDE to be rewritten, or forked, or whatever.\n\nThirdly, we never said Kolab is bad because it interoperates well with KDE. We said it may not have had the publicity it deserves because it hasn't been marketed well and perhaps because it *only* interoperates well with KDE (which may be a misunderstanding on our part).\n\nIt was great to get Aaron on the show. We are all Gnome guys, it's true, but at least two of us started out as KDE users. We don't give anyone an easy ride - we see our job as asking the questions that Linux/open source users want to have asked. Aaron made a great contribution - thanks Aaron."
    author: "Matthew Revell"
  - subject: "Re: Dissapointing really"
    date: 2005-03-16
    body: "hey Matthew =)\n\nre: Kolab.. yeah, i sent an email to Jono about that. and a million other things. heh ;) \n\nit's not easy (impossible?) to keep up on top of everything and get all things accurate. i mean, that's why shows like LUG Radio exist, right? if everyone knew everything and we all agreed perfectly on all things.... what a boring life! ;)\n\nand yeah, i enjoyed being on the show. great fun. hope to do it again sometime. thanks for the opportunity to be a part of it all =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Dissapointing really"
    date: 2005-03-16
    body: "Fair enough.  Sorry, I get riled easily :D\n\nThanks for the show."
    author: "Leo Spalteholz"
  - subject: "Re: Dissapointing really"
    date: 2005-03-16
    body: "they also discuss rewriting gnome in an interpreted language. no need to take offence.\n\nthey bash eachother aswel, have you heard their ongoing ubuntu vs fedora topics"
    author: "ssam"
  - subject: "Great job, Aaron"
    date: 2005-03-15
    body: "Aaron,\n\nYou did a great job. You sounded reasonable and professional. The questions asked were so ridiculous that one wonders how you contained the laughter.\n\n1) Should KDE be re-written from scratch?\n\nOther questions were of the \"when did you stop beating your wife kind?\" and you were very good at exposing the bias in those questions and in a tactful and professional way.\n\nWell done.\n\nPs: Somebody should go back and listen to Nat Friedmann on lugradio. He comes across sounding \"like\" a valley girl -an 80s concoction of silliness and bad hairdo that was \"like totally\" silly.\n"
    author: "Eu"
  - subject: "Re: Great job, Aaron"
    date: 2005-03-15
    body: "Yes, he really did a great job handling some of their more \"interesting\" questions. Starting from scratch for KDE 4... I guess they're just used to the GTK way of doing things.\n\nAnd thanks for the amaroK shout-out. :P"
    author: "Ian Monroe"
  - subject: "Re: Great job, Aaron"
    date: 2005-03-16
    body: "We've never interviewed Nat Friedman :)"
    author: "Matthew Revell"
  - subject: "Re: Great job, Aaron"
    date: 2005-03-16
    body: "Nat Freidman was interviewed on the Linux Link's 'Tech Show.'  Which you can listen to at http://www.thelinuxlink.net/tllts/\nThey also have interviews with Doc Searls, and some others.\n\nSteve"
    author: "Steve Nicholls"
  - subject: "Re: Great job, Aaron"
    date: 2005-03-17
    body: "I listened to Aaron's interview with great interest, and I thought he came across very well. Even the lugradio presenters were impressed judging from their comments afterwards. Great stuff!\n\nRe the Nat Friedman LLTS interview: I'm listening to it now and I'd have to say I don't think you're being very fair. He sounds like a smart guy, and some of the ideas he talks about are quite interesting (eg. text messaging interface to your appointments - probably not a brand new idea, but a simple and potentially very useful one)."
    author: "Paul Eggleton"
  - subject: "New Adobe Reader"
    date: 2005-03-15
    body: "uses GTK2... is goddam slow an has a size of !110 MB! (unstripped)...\nHonestly, I was hoping Adobe uses Qt for a new release of their reader for Linux... Now it's GTK2\nThe new Kpdf is great, I won't need acroread for most pdfs. But more and more pdfs are made to fill out forms (this will eventually gain even more importance when there will be more \"e-government\")\nI was wondering, why there's no discussion about this on kde lists... I'd really like to hear some opinions about Adobe using GTK2 despite they're obviously already owning a license of Qt."
    author: "Me"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "as i understand the situation, Adobe has only one developer who works full time on the Linux/UNIX PDF reader. this is completely word-of-mouth, but from people that i generally trust, and given market share of Linux/UNIX desktops this makes sense. so the choice of toolkit was probably down to \"what does Fred know the best and feel most comfortable with?\" (i don't know if his name really is Fred, \"he\" might be Lisa for all i know =P )\n\nhowever, i wouldn't blame the speed and size on Gtk+ alone. if you look at the app, the developer uses Gtk+ for things like menus, utility dialogs and (presumably) event handling. but everything else is hand-rolled. he doesn't really use many of the Gtk+ controls in the actual application itself.\n\ni'm not sure why they chose this route. perhaps they simply \"ported\" the old Motif code over hoping to slowly transition it to something less ugly and as time goes on. perhaps the developer is simply a control freak and believes they can do everything better themselves. perhaps Adobe's internal development tools have some strange requirements. who knows.\n\nin any case, this goes a long way to explaining the huge size of the app.\n\nyes, i do think that this will result in Adobe's PDF reader declining in usage on Linux as the open source alternatives improve. KPDF has forms on its TODO list, and i do agree that its an important feature. as the Free readers move to a common library (the IMO ill-named \"poplar\") this should hopefully help distribute the efforts needed for form support.\n\nan Open and Free PDF reader is quite important to have. i would urge as many of us who use KPDF to rally behind those working on it and give them support in any way we can."
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "Poplar... wtf did they do, take Popular and remove a vowel?"
    author: "Aaron Krill"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "http://poppler.freedesktop.org/\nhttp://www.gotfuturama.com/Information/Encyc-41-Popplers/\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "ah right, a popular culture reference as opposed to the tree. hopefully users won't be faced with figuring out what libpoppler is due to using package managers (though somehow i'm not overly convinced that will be the case enough of the time), but as a developer i have to say i'm really unimpressed at having to carry about a huge wealth of naming knowledge in my head.\n\nthe more obscure our naming, the higher the barrier to entry and discovery. for libraries that will be shared between many projects, this is really only more true IMO.\n\nif you've ever heard military folk speak to each other, their unending use of acronyms makes it rather difficult to keep up. this sort of obscurity is not great when you actually want people to understand. like new developers.\n\nlibxml2? good name! taglib? decent name! lame? umm.... poppler? wtf?! \n\nhow about libpdf? or even libxpdf if libpdf is too generic?\n\nanyways.. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "Well I think mostly people won't have to worry about it so much since it will either be included in a download, or be a dependancy of the package.\n\nI agree though.  If I want to find the pdf lib, I'd search the package names for \"pdf\" and not \"poppler\".\n\nActually, this reminds me of the k-naming disaster.  It's getting better (amaroK is fine for example), but it sure is annoying in menus when every app starts with a K, defeating the whole point of using the keyboard to start an app quickly (by hitting the first letter of its name).\n"
    author: "Leo Spalteholz"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "> I'd search the package names for \"pdf\" and not \"poppler\n\nand apparently i would've looked for the homonym \"poplar\". and i even visited it's home page a few days ago -=P\n\ncool names are fun, but quickly get in the way."
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "KMenu defaults to showing \"description (app name)\" so your example of not being able to use a keyboard for access the apps due to too many names starting with K is non sense unless you changed the option or are using a distro which did so for whatever reason."
    author: "ac"
  - subject: "Sick request"
    date: 2005-03-15
    body: "Do any of the OSS pdf readers work on Windows (and linux)? I am doing some contracting for a company that is placing equipment in airliner cockpits. One of the items will contain pdf manuals for the jet. Apparently, the adobe reader was rejected as too buggy on windows, let alone Linux. Oh, kind of cool, they are switching to Linux from Windows to be able place equipment in the cockpit."
    author: "a.c."
  - subject: "Re: Sick request"
    date: 2005-03-15
    body: "if they are using Linux, why do you need the PDF reader on Windows? or are they still using Windows now and moving to Linux in the future?"
    author: "Aaron J. Seigo"
  - subject: "Re: Sick request"
    date: 2005-03-15
    body: "this system will run both linux and windows at the same time (think x-term). The manuals can be on either system. Far better to have an app that runs on both."
    author: "a.c."
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "One of the reasons any company like Adobe uses outdated components, rolls their own software and put everything into one package is simply because they cannot be sure what system their software will be installed on. It's the moving target thing. They just simply do not want to support an application that won't run properly when a desktop environment moves on in six months. That's something that hasn't been addressed at all with open source desktop environments."
    author: "David"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "if you are using a library like Gtk+ or Qt, then by all means compile it in statically or provide your own forked-and-branded build with your software. bundling your own libraries to avoid platform movement is no reason to reinvent toolbar widgets.\n\nmoreover, it's really not a big deal to say, \"Requires version X of lib Y or better\", especially if that library has sane binary and functionality compatibility policies (allowing newer versions to be installed)\n\n> That's something that hasn't been addressed at all with open source desktop\n> environments.\n\nKDE carries binary compat policies for its libraries that do address this issue. KDE 3.x is approaching its 3rd \"birthday\". if you can't manage to track a 3 year time span (which will likely end up being 4 years before KDE4 is out, and even long before KDE3.x compat libs are uncommon; look at how long SUSE has bundled KDE2 compat libs), there's something very wrong.\n\nthe real problems with compatibility lies further down the stack and lie primarily at the feet of OS vendors."
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "You should know better than I that libraries like Qt and GTK cannot be statically compiled because they are just too big.\nOr maybe that's why the Acrobat Reader binary is bigger than 100 Mb. :D\n\nAnyway, I don't really consider this an issue nomore, since desktop environment tend to release major breaking versions slower and slower. I guess re-compiling every ~1.5yrs is enough to keep up."
    author: "blacksheep"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "Skype offers various options.  \nhttp://skype.com/products/skype/linux/\n\nNotice the statically compiled version is only 8MB.  (3 MB larger than the dynamic one)"
    author: "Leo Spalteholz"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "> libraries like Qt and GTK cannot be statically compiled because they are just too big.\n\nStatically linked Opera is ~5MB.  The real problem with static linking is that you can't upgrade the library to get bugfixes or new functionality."
    author: "Jim"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "\"libraries like Qt and GTK cannot be statically compiled because they are just too big\"\n\nNot so. When you do a static linkage (with a semi-intelligent linker) and strip your binaries afterwards, you only end up with those functions that you actually use. So if your Qt app isn't using QTextEdit (for example), then it won't be included. Adobe's problem is that it's NOT statically linked (at least not the version I have)."
    author: "Brandybuck"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "There is another feature that I feel is important as well.  It would be nice if KPDF could create pdf's, especially from a scanner.  Adobe's pdf writer does this and it's very convenient.  I use it to make electronic copies of all my important documents, and being able to scan directly into the pdf writer makes archiving my documents a breeze.  As it stands now, to make a pdf in linux (at least what I have figured out so far) entails scanning to an image, importing into Kword (or Scribus, etc.), and then printing to pdf.  This is a bit cumbersome.\n\nWhere would I go to make a feature request for KPDF?\n\nPaul....."
    author: "cirehawk"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "> It would be nice if KPDF could create pdf's, especially from a scanner.\n\nscan into kooka, the KDE scanner program (it's in kdegraphics), then print directly from it to PDF. no importing necessary.\n\n\n> Where would I go to make a feature request for KPDF?\n\nbugs.kde.org"
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "> scan into kooka, the KDE scanner program (it's in kdegraphics), then print directly from it to PDF. no importing necessary.\n\nThanks Aaron.  I guess I never thought to check if I could print directly from kooka.  That should simplify things.  I'll see how easy it is to make multi-page pdf's.\n\n> bugs.kde.org\n\nI still will make a feature request.  I think it would be nice integrated into KPDF.  As it scans a page, it can just prompt for the next page until you end it.\n\nBtw, awesome job on the interview.\n\nPaul....."
    author: "cirehawk"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "There is http://bugs.kde.org/show_bug.cgi?id=90060 already. It's a bit more though - since kooka can use ocr tools, it would be cool if the ocr result would be included. Something similar is also on the kpdf todo-list btw."
    author: "uddw"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Thanks uddw.  Parts of that request confused me a little, but here is the relevant excerpt for me:\n\n\"This could make kooka a great tool to archive document. Then there could be a shortcut to scan, ocr, export-as-pdf a document in one run. It could also ask the user if he/she wants to append more pages.\"\n\nThis is EXACTLY what I was getting at.  Whether it's done in kooka or kpdf isn't so important I guess.  I thought it made sense for kpdf since it is a pdf tool/viewer.\n\nPaul....."
    author: "cirehawk"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Vote for it ;)"
    author: "uddw"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "KPDF is a pdf viewer not a editing tool."
    author: "Albert Astals Cid"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "I understand that it's a viewer.  My request is that it add pdf creation functionality as well.  Whether they add that functionality is one thing, but I don't think the attitude should be that since it's a viewer at this point one should not request more.  After all, I thought that's what the wish list was for.\n\nPaul....."
    author: "cirehawk"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "> as i understand the situation, Adobe has only one developer who works full time on the Linux/UNIX PDF reader\n\nI think the case is just: they default to what ever the market leader (redhat) defaults to. "
    author: "anon"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "They might not have a Qt licence for Unix/X11, maybe they only have Windows licences.\n"
    author: "Kevin Krammer"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "LOL. If they can't afford a Qt license, who can? (especially if they really have only one single developer for the Linux/Unix platform)."
    author: "Michal Kosmulski"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "If they have only one developer, maybe it's more an initiative of this one developer, and his boss' only accept it as long as it doesn't cost any additional money.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "They have licenced Qt/Win32 for Photoshop album.\n\nQt's main advantage is using it for crossplatform development.\nIf Adobe uses something different on Windows for the reader, they won't have the opportunity to use a shared codebase across platforms.\n\nLicencing Qt/X11 for just one tiny project might not be worth it, especially considering the fact that the important functionality of the application is the PDF rendering and that is very likely handled by some Adobe internal library.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Acrobat Reader is not a tiny project, and given the price of the devel license it would well be worth it. it's less about the rendering itself and all the widgets and other ease you get with it.\n\nof course, the developer rolled all their own widgets (poorly) themselves so ... "
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Isn't the old Acrobat Reader for *nix mostly Motif, with custom widgets to display the document/pdf part? If they use the old application as base I think it makes even less sense not to chose Qt, with the Motif to Qt migration tool and all. "
    author: "Morty"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "yes, it was all motif. and yes, it looked as ugly as v7 looks. at least v7 has a lot more functionality.\n\npersonally, i'm happy with KPDF. it even lets me decide to play the DRM game, or not. huzzah."
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "I didn't want to imply that the reader is a tiny project, but I guess that the greater part of the application is already implemented using Adobe internal tools/libraries and the new GUI is only a small part.\n\nUsing Qt for just the GUI might have looked more expensive than necessary (not my opinion either)\n\nMy main point was that Qt spares you from developing/porting a Linux version, because it enabled you to deploy your application on multiple platforms right away.\n\nThat's why we see Qt on Linux when the vendor is also newly implementing a Windows version or is really implemeting from scratch.\nIn the latter case all the other nice features of Qt (like networking, etc) save you lots of time and trouble.\n"
    author: "Kevin Krammer"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "Where can I see this 110 MB behemoth? I've just gone to the adobe website and downloaded linux-5010.tar.gz - it's only 9.8 MB in size?\n\nPaul."
    author: "Paul Koshevoy"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "Download is approximately 40MB.\n\nftp://ftp.adobe.com/pub/adobe/reader/unix/7x/7.0/enu/AdbeRdr70_linux_enu.tar.gz\nftp://ftp.adobe.com/pub/adobe/reader/unix/7x/7.0/enu/AdobeReader_enu-7.0.0-1.i386.rpm\n\n"
    author: "Kid Pogi"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-15
    body: "it was a \"closed\" beta (you had to sign up for it) and the bzip2 tarball was ~40MB IIRC. uncompressed it was much larger, obviously.\n\nthe 9.8MB download you refer to is verion 5. the version that uses Gtk+ is version 7 (they skipped version 6 on Linux/UNIX), which hasn't been officially released yet.\n\nthey are still working on it though. i saw a version of it that uses the ATK/SPI accessibility framework in January, for instance, which had been added quite recently at the point. the person showing it had built it from a CVS pull."
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "I don't mind the toolkit, or the size.  The app works well, generally speaking.\n\nWhat I do mind is the button order!  I'm thinking most Linux users may get a more consistent user interface experience by running the Windows version under Wine."
    author: "ac"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "When will GTK make the button order selectable? It's on their todo-list (as far as I know). Switching the button order was one of the worst ideas of GTK...\n"
    author: "Birdy"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "It's configurable in Gtk+ 2.6+."
    author: "Anonymous"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "It is selectable already, sort of, search for \"gtk-alternative-button-order\" on http://www.moeraki.com/pygtkreference/pygtk2reference/class-gtkdialog.html"
    author: "Waldo Bastian"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "So it seems to be selectable by the developer but not (yet) by the user - am I right? Would be nice to set the button-order of GTK-applications to the \"normal\" order when they are used \"within\" KDE or Windows.\n"
    author: "Birdy"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "> The app works well, generally speaking.\n\nunless you care about usability or accessibility.\n\nhonestly, this app is a real stinker. it's a great PDF renderer, but beyond that it's grotesque. Luis Villa ripped it nicely on planet.gnome.org recently, and he only hit _some_ of the issues. ug."
    author: "Aaron J. Seigo"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "KPDF is great but I need Acrobat nevertheless because some\nsimple functions just dont exist in KPDF yet. For example\nthere is no way to find out which fonts are present \nin a PDF doc and which of them are embedded. \n\n"
    author: "Martin"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Is that feature so important for you? You need to know which font you are looking at to understand the pdf?"
    author: "Albert Astals Cid"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Umm, yes - I'm working in an advertsing agency and need to check\nthe fonts of created pdfs quite often to find out whether there\nare still fonts in there that cant be put to prepress.\nThese things are absolutely basic needs for everyone involved in digital publishing. And it's digital publishing where PDF (and thus Acrobat) is used quite\na lot. Just ask the folks over at Scribus. There's a reason they still\nrecommend using Acrobat (Full Version) under WINE which isnt the best\nsolution IMHO. Of course it's a long way to go until any Open Source PDF\nreader is on par with that and my point was that it's often not really \nnecessary if KPDF supported sth. like transparency flattening but already\na major step forward to be able to do the same things that you can do\nwith the old-fashioned Acrobat Reader for Linux version.\nAnd to answer my sibling parent: Dont know if this is just trolling but I'm not looking for reasons not to use it, because I'm already using KPDF quite a lot.\nI'm just pointing out its major deficiencies in professional environments\nthat make it impossible to get rid of Acrobat Reader. Probably some \npeople dont like that. Sad.\n\n"
    author: "Martin"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "The funny thing is, you have now described a new feature. With both description of what it are used for and reasoned for why it should be implemented. It even sounds rather trivial to implement, if the next version of KPDF has this functionality it would not be a surprise. On the other hand had you done the same in bugzilla earlier, it may already have been there:-) Sometimes it pays to point out those major deficiencies in a clear and reasonable manner, in the correct places. "
    author: "Morty"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "I can assure you, I'm doing that quite often. I've filed lots of bugs and\nwishlist items - trying to make them short, clear and without repeately\nasking developers when they are implemented. Without being able\nto write application this is my little contribution to make KDE become a better desktop for us all.\nSometimes, I'm not writing a bug/wishlist item and I'm free to do so\njust like Open Source developers are free to not fix a bug or implement\nsome feature.\nOf course, then I cannot complain, that a wish isnt implemented yet.\nWhen you read again my post you will find: I did not complain.\nI did not write: \"Arrgh KPDF is missing feature x, I cannot use it\".\nI just stated a plain fact why KPDF is often not useable for me (and probably\nmany others who have to work with PDF professionally). I thought\nthis might be interesting to others who feel the same or this might\nstart an interesting discussion here about OpenSource and PDF.\nYou never know what discussions come up on the dot and this is what\nmakes it so great IMO. I think it's OK and in fact not that bad\nif discussions sometimes digress _slightly_ as long as it's about KDE.\nIt's a great way of talking casually about KDE-related stuff \nwithout forcing a fundamental discussion about KDE apps on developers.\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "I didn't mean to imply you was complaining. I merely tried to highlight (in a clumsy way), how hard it sometimes are for the developers to include the features users want. Since it was such a good example, with Albert Astals Cid (for those who don't know, one of the KPDF developers) not seeing the need of the feature you described. I'd guess not many did (me included), until you explained it. It's rather obvious too, but I would never have seen it without the explanation."
    author: "Morty"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Info about fonts is necessary to judge document portability."
    author: "m."
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "No comments, looks like there are people who just dig a lot just to find reasons not to use something."
    author: "VTN"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Ops... just just..."
    author: "VTN"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-16
    body: "Here's the new fonts dialog.\nYou'll have to wait for KDE 3.5. "
    author: "Albert Astals Cid"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-17
    body: "Incredible. If that wasnt fast I dont know what is.\nMany thanks - I'm waiting impatiently for KDE 3.5 now.\nKDE 3.4? Old-fashioned already ;-)\n"
    author: "Martin"
  - subject: "Re: New Adobe Reader"
    date: 2005-03-17
    body: "pdffonts (from xpdf package). And take a look at pdfinfo as well."
    author: "Matej"
  - subject: "Is KDE losing ground"
    date: 2005-03-16
    body: "\nI saw here in the dot that Adobe is being build on GTK2. and this for me is very sad. mostly because Adobe allready has a product build on top of Qt. So a question arises: Is KDE losing ground? (is it allright to say market share?)\n\nIf kde is more stable, feature rich, mature, etc. and Qt is a more comprehensive development library why are ther more and more applications being build with GTK/Gnome\n\nWith Ximian being bougth by Novell, Redhat sponsoring Gnome development and Sun chosing Gnome as their desktop eviroment I fear that no big names will get behind KDE development. and in over time KDE will be a second choise desktop enviroment\n\nI post this question in hope of hope... please tell me I m wrong."
    author: "Marioy"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-16
    body: "acrobat is gtk1, not gtk2"
    author: "anon"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-16
    body: "nvm"
    author: "anon"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-16
    body: "I don't think you should read so much out of this, for several reasons. It sounds like the Acrobat Reader for Linux are more or less a one developer job, and as such there can be several reasons for choice of toolkit(or even bad decisions:-). Adobe's Photoshop Album are Windows only, and most likely the developers are not in the same part of the company as the pdf developers (different locations, managers etc.).\n\nAs for Qt/KDE applications, you know there are already several both big and popular ones out there. And according to some rumors you have to wait and see:-)  "
    author: "Morty"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-16
    body: "\nRumors!!!! what rumors???"
    author: "Mario"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-16
    body: "> there are already several both big and popular ones out there\n\nWhich ones do you mean (closed source)? \n* Opera (do they still use Qt?)\n* Skype (not big, but popular)\n* ???\n"
    author: "Birdy"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "yes, opera still uses Qt.\n\nand just to answer the question without actually answering it.... Trolltech has 90 on staff, many (most?) of which are  full timedevelopers, with offices on several continents (just opened up one in China). add to this that Trolltech is private, over a decade old and profitable.\n\nhow do you think they manage all that without a LOT of people using their product for closed source development?\n\npeople who doubt the use of Qt in the commercial world either have trouble with business-related math or don't realize that Trolltech isn't a hobby project but a serious company with history and some size."
    author: "Aaron J. Seigo"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "Nicely put Aaron. \n\nIn addition it looks like most of Trolltech staff are located in Oslo and Palo Alto, California, not exactly low-cost areas. I didn't get very good grades in economics, but even I can see they must have a \"few\" paying customers:-)\n\nFrom their careers page they say over 100 employees, and they are currently hiring more. Sadly I don't think I have the right kind of skills needed for a job there:-("
    author: "Morty"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "Don't feel bad. One of the founders of the company said in an interview that he probably couldn't get a job there either.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-18
    body: "There is no doubt that there are several commercial customers for Trolltech. But there are only very few widely known commercial Qt applications.\n"
    author: "Birdy"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "one day i'll have to write something really clear about this topic, because IMO it is one of the least understood set of dynamics in the Open Source world in \"the community\". unfortunately certain people have leveraged the complexity of the situation and the relative lack of useful analysis to sew unfounded seeds of doubt about KDE.\n\nsuffice it to say for now that there are big names standing with KDE (did you see how much of IBM's open source desktop red book was about KDE? several sections covered only KDE technologies...), that Novell is not a GNOME-only shop (as convenient as it is to forget, they also bought SUSE remember?) and that Red Hat and Sun are not the most relevant companies in the desktop market. Sun's even been down this road before, and IMO with their promotion of Looking Glass and handling of JDS they are currently going for a repeat performance.\n\nto quote a certain popular American author, \"The report of my death has been greatly exaggerated.\" just because they said Twain was dead didn't make him dead. he was quite alive."
    author: "Aaron J. Seigo"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "I absolutely agree.\n\nSo far the only desktop business plans that are successful are Trolltech and some smaller players that also happen to contribute to KDE. Novell does servers. Redhat essentially walked away from the desktop (after alienating KDE developers).\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "Yes, please. Write something about it.\n\nIm a kde fan and I know my way arround a computer, but by no means Im a Linux/KDE evangelist so the information I receive is just what is posted in the popular news sites. And right now Im receiving little information on KDE."
    author: "Mario"
  - subject: "Re: Is KDE losing ground"
    date: 2005-03-17
    body: "Nope, I'm afraid you're getting the wrong end of the stick. When companies see the need to buy a Qt license and put some effort into the software, then we'll have arrived. As it is, there is simply no real business case for companies so the use GTK. The buying of Qt license will be an effective barometer."
    author: "David"
---
In <a href="http://www.lugradio.org/episodes/23">this week's episode of LugRadio</a> KDE developer <a href="http://aseigo.blogspot.com/">Aaron Seigo</a> discusses usability, freedesktop.org, groupware and <a href="http://dot.kde.org/1110652641/">Get Hot New Stuff</a>.  Start 28 minutes in to hear him debate with England's most famous non-KDE users.



<!--break-->
