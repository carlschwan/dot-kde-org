---
title: "Security: Advisories for KPDF, KOffice and Konversation"
date:    2005-01-21
authors:
  - "wbastian"
slug:    security-advisories-kpdf-koffice-and-konversation
comments:
  - subject: "Shared libraries"
    date: 2005-01-22
    body: "\"Both KPDF and the KOffice PDF import filter include their own version of xpdf\"\n\nWhat?!?  Isn't this exactly what shared libraries are for?"
    author: "anon"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "If xpdf was available as a shared library, it certainly would..."
    author: "Daniel Molkentin"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "Well, since KPDF and KOffice are both KDE package,\nmaybe there should exist only one shared library for xpdf within KDE?\n\nCode duplication sux 101\n\n"
    author: "fprog26"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "If xpdf was available as a shared library, it certainly would.."
    author: "Christian Loose"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "Wrong. If xpdf was available in KDE as a shared KDE wrapper library then it certainly would. There's absolutely no reason to have two copies of xpdf in KDE (at least not in the long run). I bet that those were not the last security problems which are discovered in xpdf:\n\nOctober 2004:\nhttp://www.kde.org/info/security/advisory-20041021-1.txt\nhttp://koffice.kde.org/security/2004_xpdf_integer_overflow.php\n\nDecember 2004:\nhttp://www.kde.org/info/security/advisory-20041223-1.txt\nhttp://koffice.kde.org/security/2004_xpdf_integer_overflow_2.php\n\nJanuary 2004:\nhttp://www.kde.org/info/security/advisory-20050119-1.txt\nhttp://koffice.kde.org/security/advisory-20050120-1.txt\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "What is even more puzzling is the fact that the kdegraphics module both contains an version of xpdf and try to link to parts of a external version. Since I don't have xpdf installed I get this funny mesage when using cvs.\n\nYou're missing pdfinfo. That means that you won't be able to\nsee additional informations about pdf files in konqueror.\nThe plugin for it will still be compiled, but won't work until\nyou install pdfinfo.\nYou can download it (inside the xpdf package) from\nhttp://www.foolabs.com/xpdf/\n\nI have been considering filing a bug on it, but haven't gotten around to it yet:-) "
    author: "Morty"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "One practical problem is where do you put the common library. Another problem is that the xpdf sources needs some slight modifications for the pdf->kword filter."
    author: "azhyd"
  - subject: "Re: Shared libraries"
    date: 2005-01-22
    body: "and one more problem is that in koffice it is not the same xpdf version ... I am working on that though."
    author: "azhyd"
  - subject: "Security"
    date: 2005-01-22
    body: "Fixed this potential remote_procedure_call-risk.\nNote: Older Linux versions are lesser hollowed."
    author: "llllll"
---
Three <a href="http://www.kde.org/info/security/">security advisories</a> have been issued this week. The first two are due to
<a href="http://www.idefense.com/application/poi/display?id=186&type=vulnerabilities">
a vulnerability</a>
that was discovered in
<a href="http://www.foolabs.com/xpdf/">xpdf</a>. Both
<a href="http://www.kde.org/info/security/advisory-20050119-1.txt">KPDF</a>
and the 
<a href="http://www.kde.org/info/security/advisory-20050120-1.txt">
KOffice PDF import filter</a>
include their own version of xpdf and as a result they too will require some updating. The third advisory involves
<a href="http://www.konversation.org/">Konversation</a> in which
<a href="http://www.kde.org/info/security/advisory-20050121-1.txt">
several security issues</a>
where discovered.



<!--break-->
