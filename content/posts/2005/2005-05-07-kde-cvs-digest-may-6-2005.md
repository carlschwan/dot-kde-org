---
title: "KDE CVS-Digest for May 6, 2005"
date:    2005-05-07
authors:
  - "dkite"
slug:    kde-cvs-digest-may-6-2005
comments:
  - subject: "SVN-DigesT?"
    date: 2005-05-07
    body: "Shouldn't this be SVN-Digest from now on?"
    author: "The_Ace"
  - subject: "=of course="
    date: 2005-05-07
    body: "when i read the \"of course\" i had to smile =)\n\n"
    author: "ch"
  - subject: "Konqueror Adblock"
    date: 2005-05-07
    body: "Ivor Hewitt over on the kfm-devel[1] mailing list has developed AdBlock for khtml.  Its now in its 6th revision.  I guess it hasn't been committed yet, but seems it will be real soon(TM)[2]\n\n[1]http://lists.kde.org/?l=kfm-devel&m=111454967004664&w=2\n[2]http://lists.kde.org/?l=kfm-devel&m=111533835616299&w=2\n\nHopefully i'll see this in KDE 3.5! (or maybe i'll just have to start building from cv...svn :D\n\n-Sam\n"
    author: "Sam Weber"
  - subject: "Re: Konqueror Adblock"
    date: 2005-05-07
    body: "It's already committed."
    author: "Anonymous"
  - subject: "Re: Konqueror Adblock"
    date: 2005-05-07
    body: "Neat!"
    author: "Sam Weber"
  - subject: "Re: Konqueror Adblock"
    date: 2005-05-08
    body: "\"Hopefully i'll see this in KDE 3.5!\"\n\nI was actually hopeing not to see KDE 3.5 at all.  What I'd really like to see is a push to KDE 4."
    author: "ac"
  - subject: "Re: Konqueror Adblock"
    date: 2005-05-08
    body: "Well, KDE 3.5 will mainly just be an app release as far as I understand it. Developers wanting to work on libs should be working on KDE 4 once its branch opens, I agree with this."
    author: "Ian Monroe"
  - subject: "Re: Konqueror Adblock"
    date: 2005-05-09
    body: "Will it be able to use AdBlock filter lists, such as Graham Pierce's excellent Filterset.G?"
    author: "mmebane"
---
In this week's <a href="http://cvs-digest.org/index.php?newissue=may62005">KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=may62005&all">all in one page</a>): 

HTML to <a href=" http://www.w3.org/TR/speech-synthesis/">SSML</a> (Speech Synthesis Markup Language) working in kttsd.
<a href="http://edu.kde.org/kstars/">KStars</a> adds ability to save observing lists.
Support added for opening <a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office">OASIS</a> templates directly with a <a href="http://www.koffice.org">KOffice</a> application.  And, of course, the CVS to Subversion transition took place.



<!--break-->
