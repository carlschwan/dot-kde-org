---
title: "KDE CVS-Digest for January 28, 2005"
date:    2005-01-29
authors:
  - "dkite"
slug:    kde-cvs-digest-january-28-2005
comments:
  - subject: "good news! freeze is coming soon.."
    date: 2005-01-29
    body: "..and Derek keeps reporting of great added stuff (compiling kopete now).\nThanks Derek :-)\n"
    author: "alien"
  - subject: "DigiKam"
    date: 2005-01-29
    body: "Nice to see the ability to add a border to a photo. Its really nice to have these when developing the photos.\n\nAnyone know if its possible in digikam to have the date/time added to the photo on in the lower right corner (taken from the metainfo from the photo)\n\n"
    author: "Mr Cam"
  - subject: "Re: DigiKam"
    date: 2005-01-29
    body: "I have planed to add a new plugin for digiKam image editor (in DigikamImagePlugin part) for 'add Text' to an image after 0.7.2-beta release (planed to the next week).\n\n'Add border' plugin is not optimum. I will added more border types in the future.\n\nRegards\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "Re: DigiKam"
    date: 2005-01-30
    body: "I do need of display some EXIF information on the picture also, so thanks! Would like if you can also print them as well (maybe with different setups, so I can set the info I need displayed, and those I need printed, and their position / color).\nI use my digicam for job tasks, and having the date \"printed\" on the photo I show my clients is much more \"convincing\" and immediate than tell them about EXIF information on the file ;)\nBtw, is it possible to use digikam as a image \"browser\" (browsing directories), like ACDsee can do, without the need of creating \"albums\"?\nThanks a lot"
    author: "Marco Menardi"
  - subject: "Re: DigiKam"
    date: 2005-01-30
    body: "Printing Exif informations is very interressing. Please make a file to kde.bugs.org !\n\ndigiKam provide an independant image browser named showfoto since 0.7.1.\n\nGilles Caulier"
    author: "Gilles Caulier"
  - subject: "German Internationalization status"
    date: 2005-01-29
    body: "Hi,\n\nwhat's actually the rate of translated strings in KDE for the German language? Is it really less than 87%?\n\nP.S.: thanks for the digest, I keep reading it for over a year now."
    author: "Martin Stubenschrott"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "German: 79.23\n\nLokk at: http://i18n.kde.org/stats/gui/HEAD/toplist.php\n\nWir brauchen noch Helfer!"
    author: "Dominik"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "Wow, I am wondering why this is so little. I am Austrian (so a native German speaker myself), but just don't want a German operating system. But I don't understand why SuSE e.g. as a German based linux-company doesn't just provide one translator, since if they want to get on Munich's office computers, they just need to have a German Linux."
    author: "Martin Stubenschrott"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "Is SuSE still German based? Have you been on their website recently? It redirects you to an oldfashioned Novell page. At least, the 'German' language section still contains a third German texts - \n\nYou know - it is like with Proctor&Gamble and Tempo. When they bought Tempo they didn't want to use their know-how - they wanted to kill an innovative product to prevent Tempo from invading the American market, not to improve their Kleenex. A real innovation's killer."
    author: "Anonymous"
  - subject: "Re: German Internationalization status"
    date: 2005-01-30
    body: "\"Is SuSE still German based? Have you been on their website recently?\"\n\nThey are of course now just a business unit of Novell. Nevertheless they are still headquatered in Nuernberg, with the bulk of their distribution developers sitting there. "
    author: "spezi"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "The statistics are from HEAD, which means that it is in constant flux. A couple of weeks ago the documentation people added some things, over the last two weeks there have been quite a few minor changes in the text. \n\nWhat you see with the statistics is the work pattern of the translators. Some obviously keep up with the changes as they occur. Others work in spurts. Some teams have a repository where they collect the work in progress, and periodically update the main repository, skewing the commit statistics.\n\nI would worry if the percentages were low a day before release.\n\nThese statistics are meaningless outside of their context.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "And HEAD not only of what is will be KDE 3.4 but also all kdeextragear modules, kdenonbeta and koffice."
    author: "Anonymous"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "Not even that. In HEAD it's less than 80% + 5 or 6% \"fuzzy\".\n\nIn 3.3 BRANCH we're at 99.2% though...\n\nBTW, there is a link in the digest to the complete translation status lists.\n"
    author: "Harald Henkel"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "The HEAD translating push will not really begin until after message freeze starts on Wednesday. That's when the translators really start, because they know no new strings should be added/modified after that point, unless a very special case arises.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: German Internationalization status"
    date: 2005-01-29
    body: "I know the theory.\n\nSo the translators in Estonia (>92%), Netherlands (>94%) and Sweden (>99%) - three counties with a total population somewhat smaller than Germany's - are stupid enough to have the translation (almost) complete before string freeze...\n\n;-)\n"
    author: "Harald Henkel"
  - subject: "Funny Bug Report"
    date: 2005-01-29
    body: "http://bugs.kde.org/show_bug.cgi?id=97769"
    author: "Anonymous"
  - subject: "Re: Funny Bug Report"
    date: 2005-01-30
    body: "I wouldn't call it funny.\nThe point is KDE has some performance problems, but the \"bug\" reporter doesn't have much clue it seems. It would be nice if he would help to improve KDE but the things he points out as reason for the performance issues are completely wrong (interrupt handling, screen updates). And he insists that he's right :-(\nTwo weeks ago I installed Firefox on my KDE box and damn, this thing starts slow. Almost as slow as OOo. Eclipse is also a memory hog. So these apps have definitely more performance problems than KDE does, which doesn't mean the KDE is perfect performance-wise.\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: Funny Bug Report"
    date: 2005-01-31
    body: ">I wouldn't call it funny.\n\nI know that everybody has a own sence of humor, but I had a few very loud laughs when reading the report."
    author: "AC"
  - subject: "Re: Funny Bug Report"
    date: 2005-01-31
    body: "Actually its hilarious. The guy obviously doesn't know what he is talking about.\n \nI like how it turns into German and French at the end."
    author: "Ian Monroe"
  - subject: "Re: Funny Bug Report"
    date: 2005-01-31
    body: "> I wouldn't call it funny.\n\nYou're right. I would simply call him \"troll\" :)"
    author: "Davide Ferrari"
  - subject: "Re: Funny Bug Report"
    date: 2005-02-01
    body: "He isn't really a troll because he actually believes he is right.\n\"anoying\", yes. :)"
    author: "blacksheep"
  - subject: "Split translators from programmers"
    date: 2005-01-30
    body: "I think you shouldn't show all developers statistics in just one place. I mean, in the end, the top developers will be the translator coordinators because obvious reasons."
    author: "blacksheep"
  - subject: "Re: Split translators from programmers"
    date: 2005-01-31
    body: "And shouldn't their substantial contributions be noted?\n\nThe success of the internationalization effort for KDE is worthy of note. It surpasses almost all commercial offerings. It enables people to use KDE as much as any other contribution. It illustrates the wide community support that KDE has engendered.\n\nThe top developers can cheat if they really want their names on top.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Split translators from programmers"
    date: 2005-01-31
    body: "Well, its like how they split off children's book from the New York Times bestseller list due to Harry Potter. Children's books benefit from the increased recognition, while adult books get a better chance of being a bestseller due to the lack of Harry. So, they both benefit."
    author: "Ian Monroe"
  - subject: "Re: Split translators from programmers"
    date: 2005-01-31
    body: "I never suggested to eliminate any of the statistics, just to split programmers and translators coordinators commits graphs.\nI just don't think it makes much sense to keep them together. It's just a suggestion..."
    author: "blacksheep"
  - subject: "Kpdf minibar"
    date: 2005-01-30
    body: "I've seen in the digest that you've added a mini-bar, and although I haven't seen yet, it sounds great. I'd like to propose another feature to the \"progress\" bar. When searching, show a point in this bar where an occurrence has been found. Clicking in one ocurrence point leads you to that page of the document. Or maybe, instead of show one point per occurrence, show one point per page in different colors (i.e., green=1, yellow=2-5, red=5+).\n\nThank you for your great app!!"
    author: "David C"
  - subject: "Re: Kpdf minibar"
    date: 2005-02-01
    body: "It would be better to add your wish in bugs.kde.org so it doesn't get lost."
    author: "Andre Somers"
  - subject: "Re: Kpdf minibar"
    date: 2005-02-01
    body: "Added to bugs.kde.org. Thank you for warn me:\nhttp://bugs.kde.org/show_bug.cgi?id=98334"
    author: "David C"
---
In <a href="http://cvs-digest.org/index.php?issue=jan282005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=jan282005">experimental layout</a>):

<a href="http://digikam.sourceforge.net/">Digikam</a> adds an image border tool.
<a href="http://kopete.kde.org/">Kopete</a> oscar_rewrite merged into HEAD.
Plus many bugfixes and improvements in <a href="http://quanta.sourceforge.net/">Quanta</a> and Kopete.



<!--break-->
