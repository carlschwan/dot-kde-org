---
title: "Report: FreeNX at LinuxTag 2005"
date:    2005-06-29
authors:
  - "dmolkentin"
slug:    report-freenx-linuxtag-2005
comments:
  - subject: "NX is an amazing product"
    date: 2005-06-29
    body: "I installed it on my gentoo computer at work several months ago, and let a co-worker connect in order to use some programs that he didn't have.  The performance was unbelievable.  We were utterly unable to tell that he wasn't working on a local machine, as far as the responsiveness goes.  That was never the case with X, VNC, or remote desktop.  Unfortunately I haven't been able to try it out over the internet yet, though, since our company firewall apparently blocks the necessary ports..."
    author: "Ellis Whitehead"
  - subject: "Re: NX is an amazing product"
    date: 2005-06-29
    body: "This stuff is promising! I have tried it several times, and it is fast. Over T1, it works very well, and, over DSL, it is quite fast (on par with using an older local computer). But, with the free servers, there are kinks to be worked out. For me, most noticable was a broken (or hard-to-use) suspend feature that made accessing the same session difficult if I switched computers throughout the day. (The only other limitation is that of the X server used: a few months ago, an x.org version of X11 was hard to find for Windows, and I think not bundled with the NX client.) This is amazing work for what is (or was?) Bash script glue!"
    author: "d w"
  - subject: "Re: NX is an amazing product"
    date: 2005-06-30
    body: "\"[...] free servers [....]. For me, most noticable was \n a broken (or hard-to-use) suspend feature that made \n accessing the same session difficult if I switched \n computers throughout the day.\"\n\nThis has improved dramatically over the last 2 releases. You must have used an older FreeNX version then.\n\n\n\n\"[....] a few months ago, an x.org version of X11 was \n hard to find for Windows [....]\"\n\nThe NoMachine NX Client for Windows installs a Windows X server to be used by the NX sessions. It is currently based on the Cygwin XFree86 X server. (And yes, it therefor installs the cygwin.dll too...)\n\n\n\"[....] I think not bundled with the NX client.\"\n\nNot bundled with the Linux, Solaris or Mac OS X NX Clients. Because there you have the native, or \"shipped by the OS vendor\" X servers already. But it *is* bundled with the Windows NX Client.\n\n\n\"[....] what is (or was?) Bash script glue!\"\n\nIt still is  ;-)"
    author: "ac"
  - subject: "Re: NX is an amazing product"
    date: 2005-06-29
    body: ">> ...our company firewall apparently blocks the necessary ports... <<\n--------\n\nYou should only need to have port 22 (the SSH port) accessible from the inside to the outside world (or vice versa, if you want to connect from outside to the inside NX server). If you check the \"Enable SSL encryption for all traffic\" in the NoMachine NX Client, this is the only port ever used by NX. If you un-check that option, NX may use multiple ports for file sharing, printing. audio forwarding, etc.\n\nBut it is still very frequent that network administrators block access of outgoing connections to port 22 (after all, it *is* a security risk, given the fact that you could create a reverse tunnel with it.)\n\nSo there is another option: make the NX server's SSH daemon to also listen to port 443 (this is usually the HTTPS port). Just add the line \"Port 443\" to the sshd_config file. Then, in the NoMachine NX Client configuration make it use that port 443. Because fact is, most company networks allow outgoing connections to port 443 without any additional check and interference (talk about the actual security skills of those very same admins who nevertheless block port 22 connections... )\n\nAnd no, kNX (the proof-of-concept type of KDE NX Client didn't support the \"Enable SSL encryption for all traffic\" reliably, last time I checked....\n\nCheers,\nKurt\n"
    author: "Kurt Pfeifle"
  - subject: "Re: NX is an amazing product"
    date: 2005-06-30
    body: "> But it is still very frequent that network administrators block access of \n> outgoing connections to port 22 (after all, it *is* a security risk, given \n> the fact that you could create a reverse tunnel with it.)\n> \n> So there is another option: make the NX server's SSH daemon to also listen to \n> port 443 (this is usually the HTTPS port). Just add the line \"Port 443\" to \n> the sshd_config file. Then, in the NoMachine NX Client configuration make it \n> use that port 443. Because fact is, most company networks allow outgoing \n> connections to port 443 without any additional check and interference (talk \n> about the actual security skills of those very same admins who nevertheless \n> block port 22 connections... )\n \nNote that this is not only a technical question.  Doing this may get you fired in some organisations, for circumventing the security measures (well, after all that's exactly what this trick is about).  Be sure to read your organisation's security policy. \n\n"
    author: "cm"
  - subject: "Re: NX is an amazing product"
    date: 2005-06-30
    body: "Well, if the network admins do not want to allow connections to port 443 with any protocol, they should block it. After all, they do it already with port 22 in this case, no?"
    author: "ac"
  - subject: "Re: NX is an amazing product"
    date: 2005-06-30
    body: "Then you'd move to port 80.  Nothing is gained.  They can't block that if web access is to be allowed (except with a smarter firewall that inspects the traffic -> that would need many more &#8364;&#8364;&#8364;).  \n\nAs I said, this is not only a technical problem and not about competent or incompetent admins.  There's no such thing as 100% security and it's always a tradeoff (security, cost, convenience, level of service).  But circumventing the security measures that *are* there is acting *actively* against the security interests of the organisation and I can see why some have regulations in place that get you in trouble if what you're doing is noticed...\n\nBTW:  I *am* allowed to use port 22 from where I work, but other things are blocked and we do have such a non-circumvention policy. \n\n"
    author: "cm"
  - subject: "ssh tunneled"
    date: 2005-06-30
    body: "Hi Ellis, nice to read you again.\n\nTry fully ssh tunneled (there's a config option available). If you have ssh opened at all in your firewall, it should work."
    author: "Inorog"
  - subject: "Re: ssh tunneled"
    date: 2005-06-30
    body: "Hi Christian ;)  Nice to read you again too.  In fact, a couple days ago I remembered an old interview with you by Tink. ;)\n\nSSH is open on our firewall, so I went to try out the tunneling, but found that the NX server isn't working right now.  Anyhow, I'll check it out soon.  Thanks to you and Kurt for the tip."
    author: "Ellis Whitehead"
  - subject: "QEmu (KQemu)"
    date: 2005-06-30
    body: "Just compiled and installed QEmu, wow a good free (open source) PC emulator. I was surprised to see my own System boot up in the emulator too while it's already running ;). One can also boot from CD and setup Linux or Other (Windoze) OS! cool.\n\nTo boot from CDROM:\nqemu -boot d -user-net -macaddr 'aa:bb:cc:dd:ee:ff' -hda '/dev/hda' -cdrom '/dev/cdrom'\n\nTo boot from hardisk (replace /dev/hda with your partition like /dev/sda):\nqemu -boot c -user-net -macaddr 'aa:bb:cc:dd:ee:ff' -hda '/dev/hda' -cdrom '/dev/cdrom'\n\nAnd KQemu is a good frontend to do that configuration! wow, opensource rocks!"
    author: "fast_rizwaan"
  - subject: "Solaris X success story?"
    date: 2005-06-30
    body: "Lately I've been trying to revamp an old SunBlade with NX. After several compilations to satisfy FreeNX dependencies and some modifications of FreeNX scripts (e.g. pgrep for pidof) I came to the point of no error message but also no NX session. Somehow nxagent or whoever could not start a session or a terminal on the NX server machine. Did anyone succeed in doing this? (Installing Linux on this machine is not an option because of licensing issues of programs I want to run on the Sun machine.)\nI tried to activate the error log of FreeNX, but it did not work. So I am pretty clueless why it fails."
    author: "Jochen Puchalla"
  - subject: "Re: Solaris X success story?"
    date: 2005-06-30
    body: "There is a Solaris patch in BerliOS Patch Tracker, which is important to add for Solaris. I did not make it into 0.4.1, because I just \"forgot\" it :-/.\n\nI think you did not patch the \"getparam\" functions in nxnode/nxserver, but those are very important for functioning of FreeNX.\n\nThe patch can be found at:\n\n http://developer.berlios.de/projects/freenx/ -> Patch Tracker(Currently seems to be down ... :-( )\n\ncu\n\nFabian"
    author: "Fabianx"
  - subject: "Re: Solaris X success story?"
    date: 2005-07-01
    body: "Fabianx, maybe you can then make a 0.4.2 release with that fix included?\n\nAlso, another Solaris related question: is there already a Solaris packaging for the \"pkg-get\" tool (as created by the www.blastwave.org project) in the works?"
    author: "courious reader"
  - subject: "Great talk -- and rather 2000 visitors!"
    date: 2005-06-30
    body: "Just some nitpicking: The talk at LinuxTag (no pun intended) was in the great hall with roughly 2000 seats, which were almost completely filled. So there were rather some 2000 visitors to that talk and they all enjoyed this really much. \n\nEspecially when Kurt started playing Solitaire inside the WinXP that was running on the IBM mainframe lpar accessed through the NX client, and how he explained what Internet Explorer is (\"for those of you who don't know: This is just like Konqueror\").\n\n(I noticed these numbers because I gave a talk in that very same room right after this one but with much less audience. Sigh.)"
    author: "cstim"
  - subject: "how stable?"
    date: 2005-06-30
    body: "How stable is this supposed to be right now? I've been playing with it a bit and the client segfaults allmost every time. And if it doesn't, connecting takes forever.\nNXCLIENT - 1.4.0-91 // (NoMachine)\nNXSERVER - Version 1.4.0-04-CVS OS (GPL) // (freenx)\nboth on Debian/unstable.\nIt's rather frustrating. I seem to be the only one with that problem, and debugging a binary only program is rather difficult..."
    author: "michael"
  - subject: "Ferret bugs anyone ?"
    date: 2005-06-30
    body: "http://www.kdedevelopers.org/node/view/1213"
    author: "ac"
  - subject: "Kubuntu debs"
    date: 2005-06-30
    body: "What is the deb src for the kubuntu version being used? I only see:\n\n     0.3.1-2~5.04ubp1 0\n        500 ftp://ftp2.caliu.info hoary-extras/main Packages\n"
    author: "Joseph Reagle"
  - subject: "Re: Kubuntu debs"
    date: 2005-07-02
    body: "I've built the deb for kubuntu FreenNX 0.4.0/NX-1.4 and described it here http://stateless.geek.nz/2005/06/27/building-freenx-04-on-ubuntu-hoary/. Quite straight forward and works very very well.\n\nFreeNX 0.4.1 based on NX 1.4 is straight to built. Just copy the debian/ directory over and add an entry to the changelog (debchange -v 0.4.1).  Building NX 1.5 debs are tricky. I'm hoping someone else comes out with a diff soon, and saves me the effort of trying it myself."
    author: "Nicholas Lee"
  - subject: "Does FreeNX work with Gnome too?"
    date: 2005-06-30
    body: "Does FreeNX work with Gnome too?\n\nFrom what I read, this cool application seems to be occupied by KDE hackers. I am (still) a Gnome user myself (I am just much more familiar with it), but I have deep respect and admiration for what KDE does in its amazing offensive for innovation. But couldn't the FreeNX developers take into account other desktop environments too? Please?"
    author: "ac"
  - subject: "Re: Does FreeNX work with Gnome too?"
    date: 2005-06-30
    body: "It should work with Gnome too, there is nothing KDE specific apart from user interface integration."
    author: "Leandro DUTRA"
  - subject: "Re: Does FreeNX work with Gnome too?"
    date: 2005-07-02
    body: "Works fine with Gnome for me.  Just as blisteringly fast.  KDE, Gnome and CDE are the predefined options, and there's also a custom option if you want to run something else in specific."
    author: "CK"
  - subject: "Amazing show + Printing confusion"
    date: 2005-06-30
    body: "I was at the show, and it was really great. I enjoyed it very much, and came out convinced that now that this NX stuff works reliably, KDE could really gain big wins on the overall computer workstation/desktop market, by teaming up with companies that sell hardware for \"server based computing\" (IBM, HP, Intel -- maybe even Sun).\n\nHowever, I am a bit confused by this bit in the report:\n\n#  \"(....) the new 1.5.0 NoMachine NX core libraries' (GPL'd) snapshot \n#   that now gives lightning fast remote access, single application \n#   windowed sessions, and seamless, true zero-config CUPS printing.\"\n\nAre you saying that the secret of the \"seamless printing\" demo is hidden in the NoMachine 1.5.0 NX lib source code?\n\nFrom what I understood during the talk, that is not the case. The showcased seamless printing for FreeNX was not dependent on the NoMachine 1.5.0 snapshot, and is different from NoMachine's own current printing implementation. \n\nFreeNX's way to do it seems to be based on a \"socks\" proxy library, pre-loaded with the \"LD_PRELOAD\" mechanism, that transparently re-writes all \"ipp://localhost:631/(...)\" URLs/accesses to a different port. (And each NX user gets an SSH-portforwarding from his local NX client computer with a different port for each user; say this is 10631 for one of them....) On the forwarded port a NX session \"sees\" the printers for its respective NX client machine. Every time a user accesses seemingly \"localhost:631\" the pre-loaded socks proxy lib in reality re-directs to \"localhost:10631\" where the real CUPS daemon from the NX client was forwarded to...\n\n* Is my understanding of the demonstrated seamless printing correct?\n\n* Is this feature includeded in the 0.4.1 FreeNX release?\n\n* Will this concept also be used by NoMachine and their commercial NX server?\n\nSascha\n(who congratulates the NoMachine and FreeNX developers for their great achievemnts)\n"
    author: "Sascha"
  - subject: "Re: Amazing show + Printing confusion"
    date: 2005-06-30
    body: ">> * Is my understanding of the demonstrated seamless printing correct?\n-------\nYes, by and large.\n\n \n>> * Is this feature includeded in the 0.4.1 FreeNX release?\n-------\nNo. It was just a proof-of-concept demo. The full and complete implementation will go into FreeNX 0.5.0 or 0.6.0 very likely.\n\n(BTW, this socks based NX re-direction library was proposed as a Google Summer of Code Bounty. It is very unfortunate that it wasn't accepted :-| ... It looks like the bounty selection committee just didn't understand the value of it -- but I bet every single one of their members will be using it with great joy and benefit in the near future nevertheless. The fact that Fabian now can earn himself that bounty will probably slow him down considerably as he now has to look for other paid student jobs to work on...)\n\n\n>> * Will this concept also be used by NoMachine and their commercial NX server?\n-------\nWe hope so. But for NoMachine to accept it, the implementation needs to be very clean and well-tested. Their code quality requirements are very high.\n\nBe aware that this NX printing concept currently only works in CUPS <--> CUPS environments (i.e. were CUPS is present on the NX client as well as on the NX server: Linux, Mac OS X and possibly Solaris -- but not MS Windows, where there will still be the need to use other print paths, most importantly SMB/Samba). But here, it promises to be *much* superior, more stable and more easy to handle than everything known from the proprietary world of Terminal Services (MS WTS, Citrix/ICA or Tarantella).\n\n\nCheers,\nKurt\n \n"
    author: "Kurt Pfeifle"
  - subject: "OpenUsability and FreeNX? How does that go togethe"
    date: 2005-06-30
    body: "Daniel reports that \"currently the OpenUsability project uses NX technology for its support of various Open Source development efforts\".\n\nCould someone elaborate on that, please? I can not find any hint on NX when visiting the www.OpenUsability.org website...."
    author: "courious reader"
  - subject: "Re: OpenUsability and FreeNX? How does that go tog"
    date: 2005-07-01
    body: "Its quite easy:\n\nImagine having to compile and install the newest KDE CVS on several computers to do some usability tests on the software.\n\nJan (from Relevantive) told me something like: \"You know, you have to reinstall the computer everytime you want to do new usability tests. And then you want to test another (older) KDE Version and then [...]\"\n\nImagine now some KDE enthusiasts managing several KDE versions including a nightly for the Usability people (and perhaps even Translators) ...\n\nYou can share the work through one or several central servers managed by people who know their stuff, which can be used by people who do for example usability. Or by other people that might be not so technically inclined to compile KDE HEAD CVS every day ;-).\n\nOr imagine looking if a Bug still exists, looking at newest Features / Interfaces / ...\n\nHope it got a bit clearer.\n\ncu\n\nFabian"
    author: "Fabianx"
  - subject: "Possible to run Freenx as normal user"
    date: 2005-07-01
    body: "Hey\n\nIs it possible to make a freenx setup without root privileauges?\n"
    author: "Mee"
  - subject: "Re: Possible to run Freenx as normal user"
    date: 2005-07-01
    body: "Not only is this possible.\n\nThis is even the default setup."
    author: "Kurt Pfeifle"
  - subject: "Re: Possible to run Freenx as normal user"
    date: 2005-07-01
    body: "Ok, but according to this root access is needed, right?\n\nhttp://www.gnomeuser.org/documents/howto/nx.html\n\nAm I not understanding things correct?\n"
    author: "Mee"
  - subject: "Re: Possible to run Freenx as normal user"
    date: 2005-07-01
    body: "* To *install* FreeNX, you need root privileges, yes.\n\n* Once it is installed, setup and working, FreeNX does not run with root privileges.\n\nI hope I understood your question now."
    author: "Kurt Pfeifle"
  - subject: "Almost perfect"
    date: 2005-10-04
    body: "FreeNX is incredibly fast, tunnels over SSH, and can even theoretically forward sounds.\n\nOnly, two issues:\n\n-it's a little bit unstable.  No biggie, it's getting better.\n-It can't resume a session that was started at the desktop, and you can't resume a remote NX session by sitting back down at the desktop.\n\nThese two things are all that is left holding it back from being a Windows Terminal Services killer."
    author: "GDorn"
  - subject: "Re: Almost perfect"
    date: 2005-10-04
    body: "Both your points are not true, according to my book.\n\n* First, it is quite stable for me.\n\n* Second, I can suspend and resume sessions on the same client \n. machine as well as on a different one (session migration).\n\nMaybe you are using an older version??"
    author: "pipitas"
---
One of the "missing LinuxTag stories" should be telling about the <A 
href="http://www.linuxtag.org/vcc/details.pl?id=241">FreeNX presentation</A> 
done by <A href="http://www.pl-forum.de/NB2/images/indiv/lt2004_pfeifle_franz.jpg">Fabian 
Franz and Kurt Pfeifle</A> in the biggest hall with 500 visitors present. 
The talk culminated in the official release of <A 
href="http://freshmeat.net/projects/freenx/">FreeNX-0.4.1</A> (download <A 
href="http://download.berlios.de/freenx/freenx-0.4.1.tar.gz">here</A>).






<!--break-->
<p>During the full hour of live demoing, not only did they showcase very fast remote GUI 
access to various KDE desktops. Amongst these was a brandnew <A 
href="http://www.kubuntu.org/">Kubuntu</A> installation on an IBM 
mainframe derivative (a <A href="http://www.openpower.org/">Power5/PPC64</A> machine) running
a very nice polished KDE 3.4.1. Kurt and Fabian had recently ported FreeNX to that platform. 
The two also had obvious fun with running <i>"KQemu with a Windows 2000 OS inside the remote NX 
to Kubuntu-on-Power5 session"</i> in front of their puzzled audience. 

Other demos included the <A href="http://www.nomachine.com/sources/development/">new 
1.5.0 NoMachine NX core libraries' (GPL'd) snapshot</A> that now gives lightning fast 
remote access, single application windowed sessions, and seamless, true zero-config 
CUPS printing.</p>

<p>Kurt mentioned the fact that one of the accepted Google Summer of Code Bounties
is a new KDE NX Client to be deeply integrated into KDE, which he hopes will be successfully 
completed within 3 months.</p>

<p>The <A href="http://mail.kde.org/pipermail/freenx-knx/2005-June/001416.html">release 
announcement</A> on the <A href="https://mail.kde.org/mailman/listinfo/freenx-knx/">FreeNX-kNX 
mailing list</A> has this to say about the newly supported features, amongst many other things:</p>

<dl>
<dt><i>stable rootless nxagent:</i></dt>
  <dd>This feature adds roundtrip suppression to single application window mode
  sessions. (Before, single applications benefitted from NX compression and 
  NX caching only -- thus they were "faster" than any other remote GUI with 
  single apps, but slower than a full-featured desktop session).
  To activate, use the ENABLE_ROOTLESS_MODE parameter in node.conf.
  (Single applications like the KDE groupware client "Kontact" on Windows 
  for example have never been so fast.)</dd>

<dt><i>fullscreen toggle mode / seamless resize with mouse drag:</i></dt>
  <dd>This feature enables to resize the NX window by dragging its borders with
  the mouse. It also allows to press "[Ctrl]+[Alt]+[f]" which then toggles
  between fullscreen and normal windowed mode. Works _just_ great (at least
  from Linux NX Client to Linux FreeNX server).</dd>

<dt><i>applications keep running while in suspended state:</i></dt>
  <dd>Before, all contained applications where sent to sleep upon the suspending
  of a session. (This made f.e. your compilation running in a konsole window
  pause, or your IRC log containing gaps, or your kmail program not reacting
  when you tried to shut it down with dcop commands). Now, all applications
  stay awake and keep running when a session is suspended.</dd>
</dl>

<p>Kurt hinted at the fact that (Free)NX servers (or peer-to-peer NX sessions in
between developers, translators, artists and documentation writers) could be 
a major win for future KDE development. He showed also, how currently the
<A href="http://www.openusability.org/">OpenUsability project</A> uses NX 
technology for its support of various Open Source development efforts (amongst 
which are not only KDE, but also several Gnome applications) -- albeit
their hardware is by far insufficient and currently only serves as a 
"proof-of-concept" purpose for the viability of the <A 
href="http://conference2004.kde.org/cfp-devconf/kurt.pfeifle-nxaccelerateskdedev.php">basic
concept</A>.</p>





