---
title: "Aaron Seigo Interviewed on TLLTS Podcast"
date:    2005-09-15
authors:
  - "jriddell"
slug:    aaron-seigo-interviewed-tllts-podcast
comments:
  - subject: "He can talk, he can talk!"
    date: 2005-09-15
    body: "nice 2 hear u aaron!"
    author: "Dr. Zaius"
  - subject: "Re: He can talk, he can talk!"
    date: 2005-09-15
    body: "aarons talk starts at minute 22 btw."
    author: "Dr. Zaius"
  - subject: "Re: He can talk, he can talk!"
    date: 2005-09-15
    body: "Unless you've got a slow machine."
    author: "ac"
  - subject: "(humour) "
    date: 2005-09-16
    body: "Is Aaron KDE alpha-geek or what?!"
    author: "athleston"
  - subject: "Re: (humour) "
    date: 2005-09-16
    body: "He's the best we've got in hype and marketing :)\n\nMany KDE hackers are kinda shy or just too geeky to make any sense."
    author: "carewolf"
  - subject: "Re: (humour) "
    date: 2005-09-17
    body: "i'll be first to jump for joy when we find 2 or 3 others who are able and willing to do face-time stuff so that i can go back to other things again =)\n\ni really consider this situation much like usability once was: there was a time when i was one of those people (there were a couple =) spear-heading usability in kde. it eventually gained traction as a concept in the project and people much, much better at usability than i am with more time and zeal for it than i have appeared and are now doing a really nice job with it. i've largely moved on to other things happy that usability, something we *had* to have covered, is now in _very_ competent hands. i still do usability stuff in kde, but i'm hardly as seemingly central to usability efforts in kde as perhaps i once was.\n\none day public appearances / interviews / plasma-esque stuff will be exactly the same, and i'll go find something else to pound my head mercilessly against. it's probably what i'm best at. hmm.. if i'm alpha-anything, perhaps i'm the alpha-headsmacker.\n\nit would explain the loopyness and occasional headaches. ;)"
    author: "Aaron J. Seigo"
  - subject: "Linux Show people "
    date: 2005-09-16
    body: "Amazing the one of the guys inteviewing Aaron seems not to know Superkaramba or not using KDE, he says \"SuperK running in W.., if I am not mistaken\".  "
    author: "NS"
  - subject: "Re: Linux Show people "
    date: 2005-09-16
    body: "He corrected himself later on and stated he was thinking of Konfabulator, which was recently purchased by Yahoo!."
    author: "p0z3r"
  - subject: "Re: Linux Show people "
    date: 2005-09-18
    body: "But he still mistakenly thinks that they were the original idea, when the original Mac had a similar concept.  \n\nThere was a http://daringfireball.net/2004/06/dashboard_vs_konfabulator\n"
    author: "Evan \"JabberWokky\" E."
---
Last night KDE developer <a href="http://aseigo.blogspot.com/">Aaron Seigo</a> was interviewed on the live weekly Podcast <a href="http://tllts.info/">The Linux Link Tech Show</a>.  <a href="http://tllts.info/dl.php?episode=101">Episode 101</a> is now available for download.  Topics discussed include what happened at KDE's recent <a href="http://conference2005.kde.org">aKademy</a> conference, training new developers through <a href="http://www.osdw.org">Open Source Desktop Workshops</a>, the future of the desktop with <a href="http://netdragon.sourceforge.net/ssuperkaramba.html">SuperKaramba</a> &amp; <a href="http://plasma.kde.org/">Plasma</a> and the problems of moving from proprietary to free software.  Start 29 minutes in to hear an hour of Aaron.


<!--break-->
