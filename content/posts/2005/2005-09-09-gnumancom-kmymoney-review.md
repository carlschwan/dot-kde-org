---
title: "GNUman.com: KMyMoney Review"
date:    2005-09-09
authors:
  - "binner"
slug:    gnumancom-kmymoney-review
---
<a href="http://gnuman.com/">GNUman.com</a> has published a <a href="http://gnuman.com/programs/kmymoney.html">review of KMyMoney</a>, the personal finance manager for KDE. The author is impressed: "<i><a href="http://kmymoney2.sourceforge.net/">KMyMoney</a> is a great program, I&#8217;ve tried GnuCash previously and had a heck of a time learning it, but not with KMyMoney.</i>"





<!--break-->
