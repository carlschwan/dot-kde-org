---
title: "KDE Commit Digest for June 3, 2005"
date:    2005-06-04
authors:
  - "dkite"
slug:    kde-commit-digest-june-3-2005
comments:
  - subject: "First post!"
    date: 2005-06-04
    body: "Hey, this is no more called CVS-Digest! :-)\nThanks, Derek!"
    author: "Metamatics"
  - subject: "Re: First post!"
    date: 2005-06-04
    body: "It's already called that for one month now?"
    author: "Anonymous"
  - subject: "Re: First post!"
    date: 2005-06-04
    body: "However, it's not fully translated yet obviously, the 'endnote' on the digest page still says:\n----\nCopyright Derek Kite, 2004. Code and layout is Licensed under the GPL. Commit logs are owned by the respective authors.\nCVS-Digest is not a product or publication of KDE e.V.\n----\nnote the CVS-Digest."
    author: "Martin Stubenschrott"
  - subject: "Reduction from 100 to 8"
    date: 2005-06-04
    body: "\"Wilfried Huss (whuss) committed a change to /trunk/KDE/kdegraphics/kviewshell/plugins/djvu in HEAD\nJune 02, 2005 at 01:29:05 PM\n\nAdd an efficient implementation of the getText() method.\nThis reduces the time needed to search through a 200 page document from\nabout 100 seconds to about 8 seconds.\"\n\nHmm, this sounds very good. I wonder how google does it. Searching the net with google is faster than searching my own hard drive."
    author: "marten"
  - subject: "Re: Reduction from 100 to 8"
    date: 2005-06-05
    body: "it does not search- it is already pre-searched"
    author: "ch"
  - subject: "About Plasma"
    date: 2005-06-04
    body: "Plasma seems to be one of the keys of KDE4 and we all should thank Aaron and\nsend him load of kudos for his excellent work and innovation. This guy rocks!\n\nLooking forward to have the sexiest desktop in 2006, kicking lornhorn's and\nother asses :-)"
    author: "ac"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "\"kudos for his excellent work and innovation\"\n\nWhat innovation are you talking about?\n\nIs nothing but a copy of Apple's dash board with some variations.\n\n"
    author: "someONE"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "Maybe you should read what Plasma is about before making such claims. Plasma is not a small toy like Dashboard, residing on top of the desktop - it _is_ the desktop. \n\nBTW, the applet system of Plasma is based on (Super)Karamba, which is even older than Apple's Dashboard announcements. So maybe Dashboard is a copy of Karamba (or Konfabulator, Samurize...). :-)\n\n"
    author: "Willie Sippel"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "At least it wasn't named Kplasma.\n\nEnought with the Ks."
    author: "someONE"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "Uhm, SuperKaramba preceeds DashBoard.\n"
    author: "SadEagle"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "since when did dashboard support Python, Ruby or Java?\nor since when did it also do the dock and desktop menubar?\nor since when did it allow you to drag a widget onto the desktop, and then onto a panel?\nor since when did dashboard have graphical elements like applet extenders?\n\nnow, i'm not going to be brash and say that Plasma is 100% innovation. much of it is derivative, like pretty much everything else out there. hell, dashboard is derivative ;) but if we're going to say Plasma is derivative, let's at least give proper credit for the sources of derivation.\n\nand dashboard ain't it. ;)\n\nat the end of the day that is all semantics anyways. this isn't a game of who came produce the \"most innovative thingy\" it's all about how compelling an environment we can create. one that is exciting to use, but also elegant, powerful and easy to get into. i'd choose hitting that target and being 100% derivative any day over missing that goal and being completely \"innovative\". so i'm personally not so hung up about those terms.\n\nbut if someone perceives something to be \"innovative\" it probably is because to them, it IS. innovation isn't about being \"brand new\" or \"completely different\", it's about bringing ideas from the fringe (which may also mean \"brand new\" or \"different\") to new audiences.\n\nfor instance, creating an affordable computer that is ruggedized and made culturally appropriate for the working class in countries like India and China is by no means producing something brand new or different (we have affordable computers, we have ruggedization, etc) but doing so in a way that brings those qualities embodied in a product to that market *is*. \n\nanyways. i'm rambling. =) kisses."
    author: "Aaron J. Seigo"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "\" since when did dashboard support Python, Ruby or Java?\nor since when did it also do the dock and desktop menubar?\nor since when did it allow you to drag a widget onto the desktop, and then onto a panel?\nor since when did dashboard have graphical elements like applet extenders?\"\n\n\nWhat part of \"some variations\" didn't you understand? the essense is the same."
    author: "someONE"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "Anyway Im glad to see this on KDE, great work."
    author: "someONE"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "Isn't it rather that Dashboard is a copy of other older existing projects? Why must everything Apple does somehow be so innovative? They borrow good ideas just like everyone else."
    author: "Chakie"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "> the essense is the same\n\nas someone who is familiar with the public APIs of dashboard and intimately familiar with what Plasma is based on and where it is going, i'm going to have to respectfully, but with absolute authority, disagree. they are not the same, in essence or otherwise. you will see elements in Plasma which remind you of dashboard, but that's a small part of the whole and not even the core aspect of Plasma. not by a long shot.\n\nit's like trying to say that vim and KPresenter are in essence the same because you type text into both of them.\n\nwhen you actually start using KDE4 you'll understand =)"
    author: "Aaron J. Seigo"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "What part of \"Apple's dash board is nothing but a copy of konfabulator with no variations whereas Plasma provides tons of new features\"  don't you understand?"
    author: "Pat"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "> What part of \"some variations\" didn't you understand?\n\nI guess it's the part \"some\", in this context implying few, minor, and insignificant.\nYou'll have a hard time finding _any_ piece of software that is not just something old + \"some\" variations. \"Some\" of those variations do make a significant difference, however."
    author: "tfry"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "> innovation isn't about being \"brand new\" or \"completely different\"\n\nYes, it is.  That is precisely what it is.\n\nPlasma might be a good implementation of something.  It might be the best implementation of something.  But that doesn't mean it's innovative.\n"
    author: "Jim"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "No it's, it's introducing something new to a culture, something composed of customs, rites, or perhaps a commercial product."
    author: "Saem"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "> That is precisely what it is.\n\nthat's precisely one possible meaning of it. it's not the only means, nor even the most common form, of innovation. pricing, form factors, market appropriateness.... these are all forms of innovation. when Microsoft, and in fact most for-profit, public tech companies say \"they innovate\" they rarely mean they come up with something brand new. what they usually mean is that they are taking ideas that aren't available as a whole to the market they service and rectifying that.\n\nMaddog Hall gave a nice little monologue on this during an open Q&A session at the Trans Pacific Open Source Conference in January, actually =)"
    author: "Aaron J. Seigo"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "Hello Aaron,\n\nI just wanted to let you know that I see _your_ vision and attitude as innovative. Plasma is going to be innovation by:\n\nEmpowering the User to do what he wants (with some easy scripting).\nEmpowering the Developer to give others what they want.\n\nSomehow reminds me of Free Software...\n\nYours, Kay"
    author: "Kay"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "that is exactly my idea. plasma might not spur very much new and innovative ideas, but its the designers i trust. i'm sure aaron and his crew (the other kicker and esp the superkaramba developers!) can create something that IS special, and will be innovation."
    author: "superstoned"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "Aaron, whatever you mean to say when you say \"innovate\" - use some other word.  That's simply not what \"innovate\" means.  Look it up in the dictionary if you need to.\n\nI am not saying that whatever you claim to be \"innovative\" isn't high quality, the best of its kind, or whatever, I'm just saying that it isn't *innovative*.  You are using the wrong word.  Use some other word.\n\nYou wouldn't start calling a red car purple, and when people complain, redefine the word \"purple\" to mean red, would you?  So why are you trying to do that with the word \"innovative\"?\n"
    author: "Jim"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "Jim, \n\nmaybe you should follow your own advice, look the word up in a dictionary or two...\n\nHere are some definitions I found: \n\nhttp://grants.nih.gov/grants/funding/phs398/instructions/p3_definitions.htm :\n\"Innovation. Something new or improved, including research for (1) development of new technologies, (2) refinement of existing technologies, or (3) development of new applications for existing technologies.\"\n\nhttp://www.ucs.mun.ca/~rsexty/business1000/glossary/I.htm :\n\"a conscious sequence of events, covering the whole process of creating and offering goods or services that are either new, or better, or cheaper than those previously available.\"\n\n\nI.e. not *only* \"brand new\" or \"completely different\".\n\n"
    author: "cm"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "or how about Webster's:\n\n\"  2. To change or alter by introducing something new; to\n      remodel; to revolutionize. \"\n\nor WordNet:\n\n\"innovate\n     v : bring something new to an environment; \"A new word processor\n         was introduced\" [syn: introduce]\"\n\nJim: you've bought into the marketing lies i spoke about in my blog yesterday. it's time we started thinking critically about things rather than pandering to the agendas of entities with large media reach and very limited self interests. a good start is to take back our vocabulary so we can communicate with each other meaningfully. i don't care what Apple or Microsoft or Sun or $COMPANY has told you innovate means, the word, as you note, has a meaning. certain interests have tried to redefine it to exclude our efforts so they can market against us effectively. that's crap and i'm not going to sit by quietly and let us be 1984ed.\n\nwake up! rise up!"
    author: "Aaron J. Seigo"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "You'll note that all the dictionary definitions revolve around the word \"new\".  The idea that something doesn't have to do new stuff to be innovative is ludicrous.\n\nMarketing lies?  If anything, I'm *less* inclined towards that perspective than you.  You are doing the same thing as those guys: using \"innovative\" as a synonym for \"something good\".\n\nSomething doesn't have to be innovative to be good, and something doesn't have to be good to be innovative.  Novelty and quality are two orthogonal attributes.\n\n> certain interests have tried to redefine it to exclude our efforts so they can market against us effectively.\n\nThat is because they are pushing the idea that novelty and quality are the same thing.  By trying to misuse the word innovative to fight against their propoganda, you have implicitly condoned their misuse of the word innovative.  You are *helping* them doublespeak!\n\nIf you accept that innovation and quality are two distinct concepts, their claim that their competition is not innovative is simply not interesting, since it's *quality* that matters.  Once you legitimise their conflating of the two concepts, you are lending their argument weight, since then they can easily point out areas where their competition is not innovative and use that as an argument that their competition is low quality.\n"
    author: "Jim"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "erm, where did i mention \"quality\"? you're going further and further afield here, Jim.\n\nas for the definitions revolving around \"new\", you're right. they use it consistently as in \"bring a technology to a new market\" or \"combining existing ideas in new ways\". \n\nyour original assertion was that \"innovative\" means \"brande new and completely different\", and that's simply not true."
    author: "Aaron J. Seigo"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "I always find it funny when people stubbornly try to defend their point also they have been proven wrong.\n"
    author: "ac"
  - subject: "Re: About Plasma"
    date: 2005-06-06
    body: "Yup.  Is it really so hard to say, \"oh, sorry, I was wrong\" once in a while?\n\nC'mon, when you are blatantly and unquestionably proven wrong, just admit it.  It won't kill you I promise.  And this is coming from a typically stubborn german :)"
    author: "Leo S"
  - subject: "Re: About Plasma"
    date: 2005-06-06
    body: "In German there exists a nice word that sums up everything you have said: \n\nIt is pure \"Haarspalterei\" (English: hairsplitting).\n\nThere is even a German word that says the same with a natural KDE-like K (that letter you like most...) at the beginning. Hint it starts with \"Korinten\". :p\n\nHave a nice day. Code more - talk less and let others code. ;-)"
    author: "Daniel Arnold"
  - subject: "Re: About Plasma"
    date: 2005-06-06
    body: "Now, wouldn't Korinthenkacker be a nice name for a new KDE app?  Three Ks! \nWell, maybe not... :)\n\n\n\n"
    author: "cm"
  - subject: "Re: About Plasma"
    date: 2006-01-17
    body: "Can't... resist... comment...\n\nTo innovate is to introduce something new, to rennovate is to improve something that already exists.  Just because companies like Microsoft have perversely misued the term doesn't make it acceptable.   \n\nMicrosoft's slogan is more accurately a \"Freedom to rennovate\"."
    author: "Me"
  - subject: "Re: About Plasma"
    date: 2005-07-18
    body: "You guy's with your dictionary posts conveniently left out:\n\n\n---------------------\nin\u00b7no\u00b7vate\nv. in\u00b7no\u00b7vat\u00b7ed, in\u00b7no\u00b7vat\u00b7ing, in\u00b7no\u00b7vates\n\n    To begin or introduce (something new) for or as if for the first time.\n\n\nv. intr.\n\n    To begin or introduce something new.\n\n---------------------\n\nNow tell me how innovate means mere change where it's new only to one project. Aaron - How has Jim bought into marketing lies when marketers always use the word 'innovate' to trick people into thinking that X company is the first to come up with Y idea? Did MS innovate with XML? Of course not - they sure like to claim that they do/have. It seems to me that everyone else has bought into the marketing lies and misuse the word just like the companies. I don't see anything innovative with plasma - that does not mean it will not be - I will not be able to judge until it comes out. "
    author: "csm"
  - subject: "Re: About Plasma"
    date: 2005-07-18
    body: "> You guy's with your dictionary posts conveniently left out:\n \n[*one* definition of innovation snipped]\n\n\nI left it out because it's obvious and I didn't deny that meaning.  Jim OTOH denied any other meaning than his own (and you seem to, too) which is wrong according to several of the dictionaries.  One of the dictionaries even calls making a product merely *cheaper* an innovation (i.e. making it accessible to more people) so really I don't see your problem.  \n\n"
    author: "cm"
  - subject: "Re: About Plasma"
    date: 2005-07-18
    body: "Oh, I'm not saying it's wrong. Everything in life is subjective. All I meant to say was that definitions outside of that one are the ones that seem to have been set by marketing powerhouses. Personally it doesn't really matter and the topic is rather moot. Innovation is a word that is thrown around too much especially in a corporate word where cliches exist like \"think outside of the box\" in the IT world generally most are skeptics these things. In order to get the real definition of the world we'd have to look at a dictionary published before the industrial revolution. If they let bootylicious in nowadays - who knows how many alternate definitions exist for innovation ;)"
    author: "csm"
  - subject: "Re: About Plasma"
    date: 2005-07-18
    body: "One could make the point that word definition changes based on where you speak the language. The contextual usage plays a huge part in what the word means to a person.\n\nI used the Merriam Webster dictionary to find a meaning for the word innovation. It very simply put the meaning to be:\n\n\nPronunciation: \"i-n&-'vA-sh&n\nFunction: noun\n1 : the introduction of something new\n2 : a new idea, method, or device \n\n\nlink: http://www.m-w.com/cgi-bin/dictionary?book=Dictionary&va=innovation\n\nBy this definition Jim is right. However, the english language is a very inspecific beast. He is using a more common and maybe more common-american usage of the word. Maybe the choice of words should be reconsidered when calling something that ties together large amounts of ideas that have already been developed innovative. It is not really new, but it is Good. So the point that Good does not mean Innovative is true. You really do not ever have to innovate to make a Good product that benefits people.\n\n\nI also agree with him that while Plasma probably will be a Good Thing, it most likely will not be extraordinarily innovative. Taking known concepts and forming them into a coherent smooth pretty whole is a Good Thing but that idea has been done for years. "
    author: "matt"
  - subject: "Re: About Plasma"
    date: 2005-06-04
    body: "Plasma seems really interesting, but I think it would be even more cool if some of the great ideas of Slicker (http://www.slicker.org/) would be incorporated. I really like the idea of cards and the slider. Slicker is described in detail on the Slicker homepage in the about section and in the documentation section. I downloaded Slicker a while ago and there is actually some working code, but work on Slicker seems to have stopped. I would really like to see some of the ideas of Slicker to be implemented in KDE4's new kicker. "
    author: "Michael Thaler"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "as far as i understand the plans for plasma, some of this will be in it. especially more freedom in designing panels, and also attaching them to and stacking them onto each other. well, that's essencially what slicker is about. plasma can do even more :D"
    author: "superstoned"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "Where can I read about this Plasma you guys are talking about?"
    author: "Petteri"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "http://plasma.bddf.ca/"
    author: "Anonymous"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "And Aaron J. Seigo's blog entries: \n\nhttp://aseigo.blogspot.com/2005/06/plasma.html\nhttp://aseigo.blogspot.com/2005/06/innovation-plasma-website.html\n\n"
    author: "cm"
  - subject: "Re: About Plasma"
    date: 2005-06-05
    body: "Cuul. Thanks for the links. Sound were promising, can't wait for first release :)"
    author: "Petteri"
  - subject: "KPlato icons"
    date: 2005-06-05
    body: "The new icon is nice, but what happened to the HiColor icons.  The links <a href=\"http://commit-digest.org/?diff&path=/trunk/koffice/kplato/pics/hi16-app-kplato.png&revision=421789#/trunk/koffice/kplato/pics/hi32-app-kplato.png\">here</a> are dead.\n\n"
    author: "James Richard Tyrer"
  - subject: "Kopete"
    date: 2005-06-06
    body: "[i]...Kopete adds webcam receiving support for Yahoo...[/i]\nFine!!! someone knows if full webcam support is possible in Kopete? I think the toughest part would be the handling of an image device and not the generic image showing stuff. is there any advances on that?\n\nWhat about MSN webcam support btw? I see on the KDE 3.5 feature list that it's already planned. Can anyone give the status of that?\n\nThank you guys :)"
    author: "NabLa"
---
In <a href="http://commit-digest.org/?issue=jun32005">this week's KDE Commit Digest</a> (<a href="http://commit-digest.org/?issue=jun32005&all">all in one page</a>):

<a href="http://www.koffice.org/kexi/">Kexi</a> supports CSV import.
kttsd adds support for Cepstral voices.
<a href="http://kopete.kde.org/">Kopete</a> adds webcam receiving support for Yahoo, implements global identity for all the IM services and support for Skype is in progress.
KTorrent adds search capability.
<a href="http://extragear.kde.org/apps/datakiosk/">Datakiosk</a> adds prompts for SQL queries and search.

<!--break-->
