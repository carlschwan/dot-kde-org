---
title: "TUX Magazine: Entering International Characters"
date:    2005-01-18
authors:
  - "mgagn\u00e9"
slug:    tux-magazine-entering-international-characters
comments:
  - subject: "Useful trick"
    date: 2005-01-18
    body: "I might just purchase TUX :)"
    author: "Tom"
  - subject: ":)"
    date: 2005-01-18
    body: "Funny reading this for those of us foreign people who always used dead keys :)\nNow what I'd really like for more complicated characters is something like:\n\n- Press Special Key.\n- (cursor look changes)\n- Type character name/code\n- (something like \"lambda\")\n- Press Return\n- And you got yourself a lambda \n\nI've heard we can do this with X, not sure how though. Native KDE suport (eventually using X already built in capability) would be cool though, for easier set up, cursor effects, etc.\n"
    author: "John Dead Keys Freak"
  - subject: "Re: :)"
    date: 2005-01-18
    body: "You could use gucharmap, search for lambda and copy the symbol to where you want it."
    author: "testerus"
  - subject: "Re: :)"
    date: 2005-01-18
    body: "John wants an easy way to enter special keys. In this particular case adding a Greek keyboard layout, switching to it with Ctrl+Alt+K, pressing l and switching back to his normal keyboard layout would be much easier than starting an extra application, search the symbol and then copy and paste the symbol.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: :)"
    date: 2005-01-18
    body: "That is only true if you don't have to search where the lambda resides on a greek keyboard. If you use gucharmap (which you could invoke with a shortcut), all you have to do is hit Ctrl + F, type lambda, hit Alt+C to close search dialog, hit Enter to add the lamda sign to \"text to copy\", hit Alt+C to copy and Strg+Q to close gucharmap and finally press Ctrl+V to enter the sign in the application."
    author: "testerus"
  - subject: "Still don't know how to enter &#1077;&#769;"
    date: 2005-01-18
    body: "Wow, I thought this is the right article at the right time for me. But it deals only with entering western letters, mostly precomposed diacritical marks.\nWhat I was looking for is a hint on how to enter combining unicode accents using a cyrillic keyboard layout. Any pointers?"
    author: "testerus"
  - subject: "Re: Still don't know how to enter &#1077;&#769;"
    date: 2005-01-18
    body: "Yudit ? (www.yudit.org)"
    author: "capit. Igloo"
  - subject: "Re: Still don't know how to enter \u00d0\u00b5\u00cc\u0081"
    date: 2005-01-18
    body: "Well, using a special editor and using copy&paste is not very convenient and time consuming. What I was looking for is a way to enter combining characters in a kde application (in my case kwordquiz). Entering the cyrillic letter and pasting the combining accute does not work."
    author: "testerus"
  - subject: "Re: Still don't know how to enter \u00d0\u00b5\u00cc\u0081"
    date: 2005-01-19
    body: "don't use copy&paste, save your text in a file (in utf-8) and then re-edit it with a kde app.\nI know it's not a very convenient way, but for years now, I find nothing better, the system of yudit is so wonderful (the dream : the same system, but integrated in KDE)."
    author: "capit. Igloo"
  - subject: "Re: Still don't know how to enter \u00d0\u00b5\u00cc\u0081"
    date: 2006-06-20
    body: "Hold down \"alt gr\" and start hitting random keys.  Lots quicker if you routinely use particular special characters but don't want to keep messing around with keyboard layouts."
    author: "Jim"
  - subject: "Re: Still don't know how to enter \u00d0\u00b5"
    date: 2005-01-19
    body: "Thanks to the polish article I added dead_acute, dead_grave and combining_acute, combining_grave to the russian keymap. Seems to work fine in KDE."
    author: "testerus"
  - subject: "Support for manual xmodmap modification?"
    date: 2005-01-18
    body: "This is really cool, but it overwrites my xmodmap for adding support for the \"History back/forward\" keys and making Caps Lock a second Escape key. Is there anyway to incorporate that?\n\nRight now, I call xmodmap with a file of this content:\n! Page left and right (Thinkpad keyboard)\nkeycode 234  =  F19\nkeycode 233  =  F20\n! Make Caps_Lock another Escape Key\nremove Lock = Caps_Lock\nkeysym Caps_Lock = Escape\nadd Lock = Caps_Lock"
    author: "Dominic"
  - subject: "Re: Support for manual xmodmap modification?"
    date: 2007-07-15
    body: "Did you get any reply? How did you solve the problem?"
    author: "Anonymous bin Ich"
  - subject: "Mmmm..."
    date: 2005-01-18
    body: "...not really news for us international users I guess.\nWe wouldn't have been able to write texts all the time if we\ndidn't know that ;-)\nBTW: A default SuSE installation has Shift+AltGr(=RightAlt) as compose key\nby default. Just press Shift+AltGr. Then _release_\nboth keys (I'm mentioning this, because you usually don't use modifier\nkeys without an additional key). Then press the accent (accent gave,\naccent circumflex, diaresis, etc). Then press a key (A,E,I,O,U,N,Y, \ndid I forget any?). Works perfectly here at work for entering Turkish,\nSpanish, French, etc... without making all those accent keys \"dead\"\nwhich is quite annoying when programming. If you have LOTS of text\nto enter a different keyboard in the appropriate language is better\nthough. My colleagues are using them and thanks to cordless keyboards\nthis isnt the plugging-nightmare it once has been.\n"
    author: "Martin"
  - subject: "Compose key"
    date: 2005-01-18
    body: "For international characters you can use the compose key (often Windows one).\n\nCompose + O + E = \u008c\nCompose + ? + ? = \u00bf\nCompose + n + ~ = \u00f1\nCompose + o + / = \u00f8\nCompose + E + ' = \u00c9\n....\n\nHere I am at work on a Windows so I can't use Compose key and I miss it :)\n\n\n\n"
    author: "Shift"
  - subject: "Re: Compose key"
    date: 2005-01-18
    body: "Yes, this is the only key I really miss from Unix/Sun keyboards. I don't understand why all these new \"multimedia\" keyboards with extra keys have not added a compose button for some international correspondence."
    author: "Carewolf"
  - subject: "Re: Compose key"
    date: 2005-01-18
    body: "You can add it yourself with xmodmap ;)\n"
    author: "ac"
  - subject: "Re: Compose key"
    date: 2005-01-18
    body: "There's no need to use xmodmap -- you can select a key to be used as Compose through kcontrol: \n\nRegional & Accessibility -> Keyboard Layout -> Xkb Options -> Compose Key. \n\nWorks p\u00e9rf\u00e8ctly ... and there's no need to keep on changing between different keyboard layouts. Also, if you want to create your own layout, there's a nice introductory article about Xkb here:\n\nhttp://desktops.linux.com/desktops/04/06/03/1558258.shtml\n "
    author: "jon"
  - subject: "Re: Compose key"
    date: 2005-01-18
    body: "you can set change this to your liking in the xkb options. I use the right-alt as compose key: right-alt - \" - e makes \u00eb... normally, \" - e is just \"e :D\n\njust found out, really nice."
    author: "superstoned"
  - subject: "Re: Compose key"
    date: 2005-01-19
    body: "It is there, it just has a silly flag on it."
    author: "Ian Monroe"
  - subject: "and the question mark?"
    date: 2005-01-18
    body: "I have already setup en_US with deadkeys. I has *almost* all the additional caracters I need except the opening question mark... Anyone knows how to solve it?"
    author: "ochnap2"
  - subject: "Re: and the question mark?"
    date: 2005-01-18
    body: "http://hektor.umcs.lublin.pl/~mikosmul/computing/articles/custom-keyboard-layouts-xkb.html\nThis  tutorial on XKB might help you."
    author: "Michal Kosmulski"
  - subject: "Re: and the question mark?"
    date: 2005-01-18
    body: "a) you could use Compose + ? + ?\nb) you might want to fill an enhancement request against x.org\n(https://bugs.freedesktop.org/enter_bug.cgi?product=xorg)"
    author: "testerus"
  - subject: "Funny..."
    date: 2005-01-18
    body: " On The dk-latin1 keymap there are two buttons for modyfying what kind of accents a given vaul will have. You either press, shift-press or altgr-press the button before you hit the next letter.\n\n Theese are modyfier then letter combinations: \u00f3 \u00e9 \u00e1 \u00fa and \u00f6 \u00eb \u00e4 \u00fc\n Theese are shift-modyfier then letter: \u00f2 \u00e8 \u00e0 \u00f9 and \u00f4 \u00ea \u00e2 \u00fb\n These are altgr-modyfier then letter: \u00e3 \u00f5\n\nCant this behavior be ported to other keymaps?\n\n~Macavity\n\nPS: to make a single tilde i have to press altgr-modyfier then space"
    author: "Macavity"
  - subject: "Slightly off-topic"
    date: 2005-01-18
    body: "IS there a way to map a Gamecontroller on a keyboard or a analog gamecontroller mouse?\n\n--- \n\nI usually get my \u00e9 with ` + e "
    author: "gerd"
---
Being a guy with an accent on one of the letters of his name, you can imagine that I probably spend a lot of time entering so-called "special characters" in my documents and e-mails. Short of keeping a document with these letters already written, then copying, and pasting them, entering an &eacute; can be amazingly time consuming. Worst of all, while OpenOffice.org lets me click Insert, Special Character to select from a list, not all applications have a handy list of characters to choose from. For everyone out there who routinely has to enter special characters or letters with accents, I'm going to give you <a href="http://www.tuxmagazine.com/node/1000044">a great KDE trick</a> to use that will ease the pain.




<!--break-->
