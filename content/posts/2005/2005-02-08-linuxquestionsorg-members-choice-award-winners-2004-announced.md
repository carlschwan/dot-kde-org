---
title: "LinuxQuestions.org: Members Choice Award Winners 2004 Announced"
date:    2005-02-08
authors:
  - "wbastian"
slug:    linuxquestionsorg-members-choice-award-winners-2004-announced
comments:
  - subject: "Chart"
    date: 2005-02-08
    body: "here a small chart of the entries that were in the race for 3 years...\nSee for your self :)\n\n_cies.\n\n\n[yes, i know it is made with Excel, because of my horrible controverial university]"
    author: "cies"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "uhum... i meant to say \"horribly conservative\" about my university :)"
    author: "cies"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "So only Kate is gaining some ground..."
    author: "Martin"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "You always have to look a little closer on this kind of numbers. Take Konqueror and KMail for instance. They lost some ground, but nothing compared to last years winners who lost a whole lot more. Both Firefox and Thunderbird are newcomers this year, and they gained most users from Mozilla and Evolution.\n\nAnd Kate is not gaining some ground, it's exploding:-) Twice as many votes as EMACS, how many had laughed if someone had said so 2-3 years back."
    author: "Morty"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "Konqueror have lost a lot of ground apparently. More than half its votes in 3 years."
    author: "Carewolf"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "But look where Galeon and Epiphany are. They have together not a third of Konqueror's (as web browser) votes."
    author: "Anonymous"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "Umm, yes I was little hasty. But my point still stands, Konqueror actually does pretty well compared to the rest of the compettition. All loses towards Firefox.Look at what they lost:\nKonqueror 46%\nOpera 59%\nMozilla 85%\nGaleon 92%\nNetscape 94%"
    author: "Morty"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "The engine is quiet good. It's just a misconception, that there will be a noticable change, when you may be able to choose Gecko instead Khtml. The problem is still the usability of Konqueror (aside some buggy kio slaves). And no, I don't roll myself in Gnomes HIG fetishism. :)"
    author: "Carlo"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "> Konqueror have lost a lot of ground apparently. \n> More than half its votes in 3 years.\n\nIt's not much Konqueror which loses anything, but more that 2004 was the year of the Firefox (Score 77.12% !!), especially with his popular success on windows. This is actually a good news for Konqueror, since Firefox success will influence the web towards the W3C standards. It is now economically silly to keep a IE-only website.\n\nAs an example, we keep up to date a blacklist of commercial french websites that did not work at all with Linux/Firefox/Konqueror/Whatever. The pressure of Firefox's new marketshare helps a lot to convince those silly webmasters, 52 of those websites are now corrected.\n\nhttp://w3blacklist.flashtux.org/index.php?search=1&w3lang=&w3desc=&w3url=&w3sev=&w3status=2\n\nThis in turns open the way for Konqueror's success. "
    author: "jmfayard"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "It's not strange for me that Konqueror and Kmail goes down in ranking. They are great applications, but in the same time they have so many unresolved, sometimes very basic issues (Konqueror about 1700 bugs unresolved, KMail about 600) that such a situation will continue to happen.\n\nDevelopers add great new features to these apps, but they unfortunately sometimes forget about basic things which simply doesn't work well. Let me give a few examples: KMail doesn't show e-mails which embed images 'by cid' (simply speaking all adds e-mails which I get are totally unreadable - bug #93038), Konqueror doesn't show some pages properly - look i.g. www.autocom.pl, Outlook Express import doesn't work properly in KMail etc, etc. Previously mentioned issues doesn't exist in Firefox and Thunderbird and I think that's why these apps are so succesful althrough maybe not so good and feature-full. There are also still usability issues in KDE like Konqueror buttons, Control Center and so on.\n\nAbove mentioned problems can stop people to switch to Linux/KDE, because it could be very frustrating process (I know - I made it a few weeks ago :-) ).\n\nMaybe it would be good idea to make one release of KDE (in each release cycle) for just a bugfixes and usablity improvements. These release could have shorter release cycle and be concentrated ONLY on fixing outstanding bugs and making KDE more usable...\n\nSo will we see KDE 3.5 - with ONLY bugfix and usability release? I wish it would happen...\n"
    author: "Aarti"
  - subject: "Re: Chart"
    date: 2005-02-08
    body: "First you cannot tell that the 600 KMail bugs are only very basic issues. Second, I can tell that Thunderbird has also some very basic issues that caused me to install Opera to my parent's old PC (so old, that Linux/KDE was out of question), because it cannot be set up to correctly work in offline mode, useful when you have a dial-up connection. For me this is a very basic feature that I expect from a mail client.\n Usability wise I also cannot say that I was happy with it, as it didn't let me configure the toolbar as I wanted, the default configuration being useless (again because of the dial-up setup).\n I found KMail nice and usable, altough it has some things I just don't like and cannot understand (why does it reply to the sender as well when I reply to a list and I use Reply. Yes, I know reply to list, but my fingers press R automatically. And yes I know I can change the shortcuts, still...).\n I'd rather say that Thunderbird became so popular because Firefox got lot of publicity and it's made by the same community. And Firefox is certainly popular  these days and had a very good and big amount of press.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "> I found KMail nice and usable, altough it has some things I just don't like \n> and cannot understand (why does it reply to the sender as well when I reply \n> to a list and I use Reply. Yes, I know reply to list, but my fingers press R\n> automatically.\n\nWhat version of KDE are you using? this bug has gone since ages, I think it was around 3.2.x"
    author: "Davide Ferrari"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "CVS HEAD. In some cases (only for some persons), the sender is also included in the reply, in some cases it's not. Yes, I should notify the developers (and I will), altough I have the strong feeling that they have a good explanation that it behaves correctly. ;-)\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "Andras, you should really know the correct address for bug reports. And no, the dot is not the correct forum for this.\nAnyway, if those messages contain a Mail-Followup-To header then don't bother to notify us. This header is supposed to be the successor of the Reply-To header and should usually be set by the mail client if the sender is not subscribed to the mailing-list the message is sent to. It makes all those \"I'm not subscribed so please CC me\" superfluous."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "Yes I know, and it was really just a sidenote. I planned to notify you only if I'm sure that something is wrong. I have two issues with KMail (one more aside of this) and I will write some reports once it is clear what happens.\n\nAndras"
    author: "Andras Mantia"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "You're new here aren't you? ;-)\n\n> It's not strange for me that Konqueror and Kmail goes down in ranking. They are great applications, but in the same time they have so many unresolved, sometimes very basic issues (Konqueror about 1700 bugs unresolved, KMail about 600) that such a situation will continue to happen.\n\nI doubt this had much to do with it as other applications simply were \"hot\" and also familiar cross platform. I suspect that will change after KDE 4.0. Anyway here is the thing about bugs. You assume that less is better, but in fact it's too complex to make that statement without lots of qualifiers. Under ideal circumstances less is better, but if there are duplicate and less relevent bugs and a developer has to spend forever putting them to bed instead of fixing as many as possible on a priority basis you could reach a paradox. In fact if you were to create formulas for bugs you would factor in features, users, complexity and developers. From there you would have expected numbers with ongoing development and a drop in bugs with new software might indicate less users filing bugs and more undiscovered bugs. It's not good to over simplify and point to open bugs. Also, email attachments by cid? I'm not familiar. Where and how this is practiced could be a factor. One thing, for MS proprietary formats there is a program on kde-apps.org that converts them nicely.\n\n> There are also still usability issues in KDE like Konqueror buttons, Control Center and so on.\n\nIs this what you read from those looking for something to find wrong in KDE or do they create a problem for you? Usability indicates a user and you are a user. While it's important what your first impression is it is also important how well it works for you after the first impression, weeks and months later when you want to do something.\n\n> Maybe it would be good idea to make one release of KDE (in each release cycle) for just a bugfixes and usablity improvements.\n\nWow! You know what else would be cool? If people tried to understand things they wanted to change before trying to suggest changing them. Would you agree that would make sense? I would think so, yet there is this epidemic on the web of people making suggestions based on total ignorance. BTW ignorance is not meant as an insult as it's the natural state of anyone arriving in a new place. Let me enlighten you. There are three components in a KDE release number. First is the primary version number, second is the major point release and third is the minor point release. So 3.4 is a major point release which will be followed by the 3.4.x series and eventually 4.0. The first number signifies potentially a major foundational change. Typically it goes with the upgrade to a new version of the Qt toolkit. The second number is where incremental improvements are made to the program and user interface strings can be updated up until the freeze so that translations can be done. The third number, most of KDE releases, are in fact bug fix releases. Expect some time around May or so to see 3.4.1 as a bug fix release.\n\n> So will we see KDE 3.5 - with ONLY bugfix and usability release? I wish it would happen...\n\n'fraid not. There will be no 3.5 but there will probably be a 3.4.4 given the time to go to 4.0. However for all the subsequent releases in 2005 after 3.4 they will almost certainly all be bug fix releases... but that doesn't mean you and others won't be drooling over 4.0. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "Sorry Eric, but you missed the point. I understand very good what the release cycle of KDE is like. My proposition was posted just to say that KDE needs more attention in areas that maybe are not so shiny for developers, but very important for 'normal' users. \n\nI would not say that I am not-computer-educated user. I am at least intermediate-level computer user (or maybe even advanced? - I am working as programmer in computer company. BTW, we use Linux + KDE.). So that's not a very big problem for me that there is too much buttons on Konqueror toolbar. But last time I installed Mandrake for my aunt: it was a problem for her that in OpenOffice toolbars were different than in M$ Office. So I can understand very good problems, which bad usability can makes. And even for me finding something in Control Center is somtimes a problem, because functionality is not logically divided into modules.\n\nSo, maybe release of KDE 3.5 is not a good idea, but definitely there should be something done to improve basic functionality of KDE. \n\nAnd the last, but not the least: I think that KDE is great, much better than G**** and definitelly better than W******. Thanks for all developers for their great work!\n"
    author: "Aarti"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: ">But last time I installed Mandrake for my aunt: it was a problem for her that in OpenOffice toolbars were different than in M$ Office.\n\nBesides that this is not related to KDE the issue here is familiarity, not usability."
    author: "ac"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "And even so: if ordinary users desire toolbars like the ones on MS Word, then everybody should shut up about there being too many buttons on any toolbar in any KDE application. When I see people using Word I'm always amazed that the toolbars seem to take more than half of their screen..."
    author: "Boudewijn Rempt"
  - subject: "Re: Chart"
    date: 2005-02-09
    body: "of course the konqueror line only shows the web browsing votes, not the file manager votes. as a file manager, konqueror is getting a lot more props, having finished out first this year."
    author: "Aaron J. Seigo"
  - subject: "GOOD POINT"
    date: 2005-02-09
    body: "Mozilla/Firefox suck at file browsing/managing. Not to speak about the way Konq does the intergration with KParts/NescapePlugs/KIO_SLAVES.\n\nKonq just rocks!\nKHTML rocks! (yes, it is zippy)\nKIO_SLAVES mega rock!\n\nKonq is too kool for those awards :)"
    author: "cies"
  - subject: "repost?"
    date: 2005-02-08
    body: "Is this article a repost?  The awards were thoroughly announced and discussed here already: http://dot.kde.org/1107647369/"
    author: "ac"
  - subject: "Re: repost?"
    date: 2005-02-08
    body: "Well, no. That posting was just hijacked to discuss the awards."
    author: "Martin"
  - subject: "Disturbing"
    date: 2005-02-08
    body: "When looking at the result i find it slightly disturbing that good old KDE 1.x apparently is still in widespread use. And quite popular by the look of it, ending up with a nice 4.th place in the file manager category for KFM with over 11% of the votes. That ain't bad for a application with the latest release 5 years ago:-)  "
    author: "Morty"
  - subject: "Re: Disturbing"
    date: 2005-02-08
    body: "The poll first contained only KFM, Konqueror as option was added later."
    author: "Anonymous"
  - subject: "Re: Disturbing"
    date: 2005-02-08
    body: "Well shouldn't they then have cleaned up the result page and given Konqueror the correct 42% of the votes for File Manager of the Year then? "
    author: "Morty"
  - subject: "Re: Disturbing"
    date: 2005-02-08
    body: "Yes they should have - it was a mistake - they added KFM first - then Konqueror a while later - people were voting for KFM whilst thinking about the Konqueror file manager."
    author: "anon"
  - subject: "Cmon.."
    date: 2005-02-08
    body: "Such a load of balls. Obviously not enough people voted, how could xmms get 1rst place. And wheres gnome, this is not paying credit where credit is due...\nAnd vi best editor?\n\n--start rant--\n\nP.s. IMO kde is moving to fast for its own good and is looking less and less polished every realese. How about actually putting some thought into the menus rather than cramming every last thing in.\n\n--end rant--\n"
    author: "brk3"
  - subject: "Re: Cmon.."
    date: 2005-02-08
    body: "GNOME is second after KDE. The GNOME people were hard at work voting for their desktop.  "
    author: "ac"
  - subject: "Re: Cmon.."
    date: 2005-02-08
    body: "My big beef with GNOME is the lack of an educational menu. gperiodic and whatnot get tossed in \"Other\".\n\nLack of educational software and integration with the menu structure is one reason why I throw KDE on machines I setup for my wifes classrooms. KDE currently kicks royal butt in this area. \n\nAlso I have gone back to school and I'm finding for students at University KDE is superior to GNOME as well. \n\nI could be wrong but it appears GNOME is ignoring one big massive area. OO.o is nice, but it is not the only tool requried by far. Kile for example is nice but GNOME, IMHO, won't do a LaTeX editor because \"normal\" people don't use LaTeX. \n\nOh, and I KNOW some schools are using GNOME. I hope since Ubuntu's goal is educational (I *think*) that this area might be addressed. More software is better for all. \n\nANyways, I am blabbing. Later.\n"
    author: "Jeremy"
  - subject: "Re: Cmon.."
    date: 2005-02-09
    body: "Vi/vim works, and my finger know it.   My fingers also know emacs.   I don't want to learn more interfaces.  AT least for programing C/C++ I find the built in vim and emacs modes work better than kate.\n\nThat said, I have used kate a little, and it isn't that bad.   Just that it isn't worth learning when vim and emacs already exist and work for me.  There is no compelling reason to switch."
    author: "bluGill"
  - subject: "yo"
    date: 2005-02-10
    body: "Thats all well and good, its just I dont think that coming into 2005 a command line editor like vi should be voted best editor. If its best programmer's editor then that should have been specified. It may be best in regards to those kind of features, but in reality Linux is now a capable desktop enviroment being used for normal everyday use more and more. And we're saying that this is the best we have to offer? I know where a windows user would be running if faced with vi. Back to windows that where! :p"
    author: "brk3"
---
<a href="http://www.linuxquestions.org/">LinuxQuestions.org</a>
<a href="http://www.linuxquestions.org/questions/showthread.php?s=&threadid=286716">
announced</a>
this week the results of their annual Members Choice Awards.
Just like in
<a href="http://dot.kde.org/1073779562/">previous</a>
<a href="http://dot.kde.org/1046679118/">years</a> KDE proved once
more to be the most popular desktop environment. In the individual categories KDE applications made a strong presence as well.








<!--break-->
<p>Award winners:
<ul>
<li>Desktop Environment of the Year - KDE (58.25%) [2003: 55.93%]</li>
<li>Web Development Editor of the Year - Quanta (50.88%)  [2003: 49.26%]</li>
<li>IDE of the Year - KDevelop (37.77%)</li>
<li>File Manager of the Year - Konqueror (30.59%)</li>
</ul></p>
<p>
Honorable second places:
<ul>
<li>Browser of the Year - Konqueror (8.34%) [1st Firefox, 3rd Mozilla]</li>
<li>Editor of the Year - Kate (17.24%) [1st vi/vim, 3rd Emacs]</li>
<li>Audio Multimedia Application of the Year - amaroK (31.15%) [1st XMMS, 3rd Rhythmbox]</li>
<li>Office Suite of the Year - KOffice (7.92%) [1st OOo, 3rd GNOME Office]</li>
<li>Window Manager of the Year - KWin (19.74%) [1st Fluxbox, 3rd MetaCity]</li>
<li>Mail Client of the Year - KMail (21.79%) [1st Thunderbird, 3rd Evolution]</li> <li>Messaging App of the Year - Kopete (21.85%) [1st Gaim, 3rd aMSN]</li>
</ul></p>
<p>See  also
<a href="http://www.linuxquestions.org/questions/forumdisplay.php?s=&forumid=62">
the complete results</a>.</p>



