---
title: "KDE Commit Digest for July 8, 2005"
date:    2005-07-09
authors:
  - "dkite"
slug:    kde-commit-digest-july-8-2005
comments:
  - subject: "try keychain"
    date: 2005-07-09
    body: "very usefull indeed\n\nhttp://www.gentoo.org/proj/en/keychain/index.xml"
    author: "bangert"
  - subject: "Re: try keychain"
    date: 2005-07-09
    body: "the same thing can also be done using kerberos v5,\nit is much more work to set up.\nbut for a large company probably more rewarding in the end.\n"
    author: "Mark Hannessen"
  - subject: "Kdevelop a part of KOffice?"
    date: 2005-07-09
    body: "I have found a commit related to KDevelop among the KOffice commits.\n\nCheers\n\nZoltan"
    author: "Zoltan Bartko"
  - subject: "Re: Kdevelop a part of KOffice?"
    date: 2005-07-09
    body: "Looks like just a miscategorized commit."
    author: "Anonymous"
  - subject: "So does that mean...."
    date: 2005-07-09
    body: "that kghostview is going? Or will we just have two programs that do the same thing?<P>Also, not directly relevant, but why does the k menu put adobe reader under office but kpdf under graphics?"
    author: "mikeyd"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "One of the google summer of code projects was to create a universal viewer for KDE, which was going to be based on KPDF.  Possibly this is the beginnings of that project.\n\nL."
    author: "ltmon"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "No.\n\nIf you look at the actual changelog you'll see that kpdf when invoked with a postscript file actually just invokes ps2pdf if it's installed. So it's not a really new feature but just an automated conversion by a 1-line shell script, and I think it's not a good idea to do it that way.\n\nAt least the user should get informed with a little dialog box that kpdf can't display postscript files, but can offer a on the fly conversion to pdf. This will prevent people from using the wrong application and filing bug reports about different displayed documents in kghostview and kpdf, or complainig why kghostview displays postscript so much faster than kpdf."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "Please no popups."
    author: "gerd"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "So you really don't want to inform the user if he's about to start a shell script \"ps2pdf\" which starts a shell script called \"ps2pdf12\", which calles a shell script called \"ps2pdfwrt\", which just starts ghostscript with the pdfwrite output device. That will obviously just work if ghostscript is installed and ghostscript has it's own well done KDE frontend called kghostscript in the same kde-graphics package that kpdf comes with.\n\nAnd you simply don't want the user not to get informed about this mess, because you don't like dialog boxes, even if they have a check box saying \"Don't show this message again\"?\n\nSo why do we have kdvi? This could be easily replaced by kghostscript, just call dvips! Ahh ... well of course that will also end up whith kpdf. So you would be happy typing your LaTeX Document, and wait for each each corrected typo 60 seconds until it shows up in the previewer?\n\nSorry if that sounded too rude, but I'm a bit afraid that people try to make things simpler than they really are. One viewer shell with different backends for different file formats would be OK, but don't start this on-the-fly conversion hacks!"
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "I fully agree, why should the user be informed of this implementation detail.  That's ridiculous and pop-ups for no good reason are simply bad. It should all happen quietly behind in the scenes and I fully agree with you that only one viewer is needed for PDF/PS/DVI/... not different ones."
    author: "ac"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "I am a user and I want to be informed.\n\ndvi files, when converted with the wrong parameters, can look quite ugly as pdf, there are several things to take care of when you convert between these formats. And it's not a \"implementation detail\" if you silently convert a file to another format where no 1:1 conversion is possible. You can convert LaTeX for example dirctly to pdf, but then you can't include eps figures anymore. On the other hand you con convert dvi to ps and ps to pdf but you have to take care of the fonts. So if I want a preview I *need* to see which file is correct converted and which is problematic.\n\nIf the KDE viewer start to convert them silently without telling me, they are *useless* to me.\n\nBeing a PhD student in a scientific subject, working with LaTex, postscript and pdf files is part of my daily work, so I am specially affected, even more as I'm also the admin of our workgroups network. So, please, let the people who have to work with these formats decide how they want them displayed. The last thing I need is a nosy KDE that converts my documents without telling me, and finally I'm lost with my pdf presentation when I have to give my talk, because I have to use the Windows Laptop at the presentation site.\n\nPlease leave that \"The DE is cleverer than the user\" approach to the windows world!\n"
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "Erm KDE isn't converting your documents without telling you.  If you're fool enough to open a postscript file with KPDF instead of KGhostView, then KPDF will show you the file.  IT DOESNT CHANGE THE FILE ON THE DISK.  If you don't like it, don't do it.  Use the right tools for the right job.\n\nThe rest of us aren't interested in how to compile eps or other PhD concerns and are glad to have all this latex mess handled intelligently by KDE."
    author: "ac"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "It was you who said that there should be just one viewer for dvi/ps/pdf files, and converting them to display them whithout a notice would be OK.\n\nI am not talking about changing the file on the disk, but about *displaying* the file on the disk.\n\nFunny that you admit that you don't have anything to do with the \"LaTeX mess\" as you call it, but know the best way to view these documents and that \"the rest of us\" sees it the same way --- you seem to be quite sure to talk for all KDE users. And relax, I'm not asking you about \"PhD concerns\" ... Actually I think it's of no use to discuss at all further with you."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "> I am a user and I want to be informed.\n\nThe problem is the application doesn't know when a user wants to be informed. The last I like is stupid a popup, blocking my workflow. Displaying some information in the status bar or even better a line of tabs in a log window together with a terminal, a search window and e.g. one for meta information of the file at the bottom similar to Kate, wouldn't be bad, though.\n"
    author: "Carlo"
  - subject: "Re: So does that mean...."
    date: 2005-07-09
    body: "The application should inform the user when it does somthing which probably wasn't intended by the user and could give results not expected by the user.\n\nSo IMHO kpdf, when asked to show a postscript file, should show a dialog like \"kpdf can't show postscript files directly, but can convert them on the fly to pdf and display that\". Of course there should be a radio button \"don't show this message again\" and a help button leading to a page that explains the details, give a hint to kghostview and mention possible problems whith the conversion."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "If KPDF can convert files on the fly, it should just do that.  Why should it bother you with a pop-up?  What is wrong with ps2pdf anyway?  It is exactly the Ghostscript renderer is it not?  The advantage you get in PDF is you can select the text, etc, so it might be better to convert to PDF anyway.\n\nThere is no LaTeX involved as someone else was saying, just a straight conversion from ps to pdf using ghostview."
    author: "KDE User"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "Now it's getting all mixed up: I mentioned just LaTeX as that would be the next logical step. Let's view dvi files by adding a filter that just calls dvipdf (or dvips and then ps2dpf) to display dvi files in kpdf. And ac seriously replied that it would be a good idea to have just one viewer and the conversion is OK, so I guess he would welcome that approach.\n\nThe problem is that all these conversions don't give you automatically good results. Yes it is ghostscript that converts ps to pdf, but if you take the time and google a bit for \"Converting Postscript to pdf ps2pdf\" you'll find reports about missing inline images, crappy fonts, unprintable documents, ... after the conversion. These problems occur more often when your source document is a LaTeX generated dvi, converted to ps. And I have seen such documents, so to me it is problem if the direction of development of the viewers in KDE would be to have just kpdf and do everything else by conversion."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "Nope, it's absolutely uninteresting for the user - apparently --you ;) - which way kpdf is internally using to display a file. It's important to have easy access to the necessary information, when something doesn't work."
    author: "Carlo"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "No, see the reply above. Do you really think it is a good idea to convert your pdf-converted postscript file back to postscript, when you press the \"Print\" button in kpdf? This is not just abuse of resources, this just calls for troubles!"
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "Why?  PostScript file is already available, just print that.  You could convert from postscript to PDF to postscript, but there is no need.  Unless you are editing the postscript file, then you will probably save to PDF anyway.\n\nDo you think KPDF should pop-up a warning saying that it is not Adobe Acrobat and that there may be errors in rendering PDFs and the correct solution is to use Adobe Acrobat to view PDFs? :-)"
    author: "KDE User"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "To question 1) That's not logical an will be a pitfall for unexpierienced users. Do you want to tell them: You can view postscript files in kpdf but better do not print them from there! In fact it's a usual workflow, to open a file in a previewer to verify that it's the correct one and print it from there. Of course kpdf could be clever enough to print the original postscript, but in that case it's not printing what it has displayed.\n\nTo question 2) No, and that has nothing to do with the topic.\n\nI wonder what would you think of a picture viewer that converts all opened images internally to jpeg before displaying them.\n\nEither you say converting to pdf is good enough so the user shouldn't know about all that back an forth conversion (and I explained you that there are problematic files) or you say you have to be a fool to use it at all, so why has kpdf this 1 line shell script hack at all, if it isn't the right program to diplay postscript."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-12
    body: "To answer 1) Instead of converting it back to PS, doesn't KPDF already \"know\" that it opened a PS file? Why not track that information, and send the *original* file to the printer if the user decides to print?\n\nTo answer 2) It does have something to do with the topic. The topic started out as \"obnoxious dialog\" vs \"no obnoxious dialog\". E.g more information vs. less. So, as long as your argument is you want to be alerted when something isn't right, you may as well be informed that KPDF has issues when opening some files.\n\nIf the program handles the problem in a manner that avoids the problem you speak of, then the user can stay blissfully uninformed, and you can still have postscripty goodness. Everyone is happy, neh?"
    author: "Zak Jensen"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "Behind-the-scene conversions of file format is very common in the Unix world. Isn't this how the linux print system works? You can \"lpr\" just about anything, and the OS automagically applies a series of filters to produce something that your printer might understand. I would find it really annoying if I could only print postscript files directly, just because this is the only format my printer happens to support.\n\nOf course there is a possibility that the default conversion rules do not give the best possible result, but then I can still try to convert it to postscript manually.\n"
    author: "ac"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "I agree with Furanku: if I want to view a postscript file I want to view that, not the same file converted in PDF (because they can be different). If I need that file in PDF format then, fine, I convert it according to my needs.\nIt would be probably useful to have Kpdf display postscript files, but using kghostview as backend (I don't know whether there is already a kpart for that) so that what we see is really the postscript file as it was meant to be displayed.\nIn case it could even use Kdvi as a backend to display dvi files, but without conversion (because different formats have different uses and different strengths)."
    author: "Klaus"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "PostScript and DVI don't support PDF features.  If you don't want to use KPDF, then don't...  it seems simple enough?"
    author: "KDE User"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "Printing in Linux means postscript printing because in old times most of the Unix and printer vendors decided to agree on the language Adobe specifically developed for this purpose. So you *have to* convert your document to ps to print it. Because of that conversion, graphical printing application like kprinter offer you a \"print preview\", to verify that you get the desired results. In that case I agree that it would be ridiculous to ask the user if he wants his document converted to postscript.\n\nThe situation with kpdf converting postscript to pdf is different. We have a proper postscript viewer. It's just confusing (and uneccessary) to view postscript documents in the wrong application by converting them silently. But if you want to implement that OK, I'm just asking for a small dialog and beg not to go this road further to a every app shows every format by coverting behind the scenes mess. As I said this scheme could be easily extended to dvi files, there is also a script called pdf2ps. Now would you want kghostscript to convert pdf to ps to show them? That would be completely silly because ghostscript contains a pdf interpreter and kghostview can display pdfs natively right now. So how far do we want to play that game, until nobody knows what is really displayed and all files show up finally broken by some on the fly conversion?"
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "The easiest way would be to have a kghostview plugin for KPdf.\n\nwhat users need is interface consistency. PS will die out on the long run anyway. The easier solution is to improve the conversion tool."
    author: "Gerd"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "I agree on the first thing, the unified postscript and pdf viewer, I guess that's what kparts are for. Much more elegant than converting files silently.\n\nI disagree that postscript is a dying language. We need and have a powerfull page description language. Why throw it away? Ask your local printing shop what they think of customers delivering thier documents as pdf's. It will be accepted, but allmost all these high end printing and cutting machines accept postscript (some of the cutters maybe just HP-GL), AFAIK none of these machines them speaks natively pdf. What's wrong with the \"pdf for document exchange\", \"postscript for printing\" approach? If it's not broken, don't fix it!\n\nAnd we should encourage postscript usage, because the alternative would be that Microsoft establish thier metro-print-path. And that would mean the return of the old GDI printers problems, which just come with drivers for Windows."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-10
    body: "You said it.  I agree with you.  The interface of kpdf is slightly better \nthan kghostview.  So it would be a nice idea to use kpdf interface shell \nwith different backends for rendering pdf, ps ,dvi etc.  Each file type \nis best displayed in the native viewer rather than using behind the scene \nconversion.  "
    author: "Asok"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "I so agree with this.. Why convert from one format to another behind the scenes (perhaps introducing errors in the display) when KDE already has a viewer for the source format?  Use that as a backend if nothing else, what else are kparts for?"
    author: "MamiyaOtaru"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "Because PS does not support PDF features that make KPDF so interesting.  The viewer you say KDE already has is based on ghostscript.  The conversion from PS to PDF is done with the very same thing, ghostscript, so don't worry."
    author: "KDE User"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "Sorry, you still didn't get it. Nobody wants to take features away from kpdf. Nothing will change if you use kpdf as a viewer for pdf. What we want is: postscript files should be displayed directly by ghostscript's postscript interpreter. This will render the postscript code to a pixmap that is displayed.\n\nThe situation with the conversion is: postscript file will be interpreted by ghostscript's postscript interpreter, but not rendered into a pixmap. Instead they will be translated into pdf. This pdf will be interpreted by the pdf interpreter in kpdf. In some points postscript and pdf are close enough that there is no problem, in other (for example font handling, or embedded graphics) this can cause problems. There are even more pathological cases where a pdf is something completely different then a ps. Take for example the famous postscript snowflake. Everytime it is interpreted by a postscript interpreter it will show you a new snowflake. If you convert this into a pdf, it will be always just the one snowflake that was generated during the conversion. And we now have two interpreters involved, where one could do the job.\n\nWe have a good postscript interpreter, so why should we abuse that to generate pdf, if it can do the job without that? It only causes cpu usage and raises the probality that something goes wrong. It's simply nonsens! Esp. when you take a look on the fact, that ghostscript also has an own pdf interpreter for itself, independent from kpdf."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "what you don't seem to get is the fact you can't search for text in a pixmap... you can't select and copy it... if you convert the PS to PDF, you can do these. That's why the dev's do the conversion. embedding kghostview wouldn't make it possible either.\n\nthank god we have google, and someone is working to make kghostview and most other document vieuwers obsolete... :D"
    author: "superstoned"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "The fact that kghosview hasn't implemented the search and copy to clipboard functionality doesn't mean that it's not possible. Just try ps2ascii (which also calls ghostscript) and you get better results then after the conservation to pfd and th graphical selection. Here's an short example of a german LaTeX generated Document: First ps2ascii:\n\n\nW\"ahle Zylinderkoordinaten r = (r, OE, z) mit dem Leiter bei r = 0. Magnetisches Feld eines unendlich langen geraden Leiters:\n\nwhich is a perfect reproduction of the text, notice the german Umlaute in TeX notation, except for the greek phi!\n\nNow the copy and paste from the converted document within kpdf:\n\nW\u00a8hle Zylinderko ordinaten r = (r, &#966;, z ) mit dem Leiter\n  a\nbei r = 0.\nMagnetisches Feld eines unendlich langen geraden Lei-\nters:\n\nThe umlaut is unusable with the letter in the line below the word, a mysterious space appears in the middle of a word and a hyphenation in the word \"Leiters\" is still present. Needless to say in which version to search would give better results ...\n\nSo don't tell me there's nothing lost during the conversation. That is this \"The Desktop Environment is smarter than the user\" approach I don't like. Use the right tools, and for postscript the right tool is ghostscript not kpdf!"
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "well, after all, kghostview is suposedly associated with .ps, not kpdf? and for a 'casual' user, with this kpdf way it is easier to search through .ps files, i'm afraid."
    author: "superstoned"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "\"False failing\" searches can have more bad consequences then no possibilty to search at all. If my computer tells me that word isn't in the document I tend to believe him. Maybe then I've missed exactly that information that I needed so badly.\n\nSo we agree that it's a somewhat unsatisfactory solution to view postscript in kpdf? We have\n\n- the waste of resurces,\n- two interpreters and three shells involved,\n  with all thier possibilties of bugs, security issues due to temporary files, hard to track down bugs, etc. ...\n- the printing problem,\n- the problem that you don't see the file on the disk but a converted one,\n- the possible image and font issues,\n- the problem with the corrupted text extractation,\n- the possibilties of false failed searches,\n- the abuse of ghostscript, when it has it's own pdf interpreter\n\nAll I asked for in the beginning was to show up a little dialog that warns the user if he opens a postscript document with kpdf. After all this discussion, I'm even more sure that this would be a good idea.\n\nAnd finally it's not in the Unix philosophy, that made Unix and Linux so great operating systems: \"Do just one thing, but do that right.\"\n\nI'm not sure how complicated it would be to implement a search and copy&paste function in kghostview, maybe ghostscript needs some patches, but that's definitly the way to go! Then turn it into a kpart and unite kpdf and kghostview and maybe kdvi and other viewers under one graphical viewing shell.\n\nAs long as that isn't done this hack is OK, if we are honest to the user and warn him about then possible problems. And I hope I convinced you that there are some.\n\nI now have to leave for some days, so, sorry, if I don't reply anymore, thanks to all for the (partially hard, ac ;) ) discussion."
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "well, then you didn't read the article very well, the user IS informed. i'm not very sure how, but some kind of dialogue or something like that is mentioned."
    author: "superstoned"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "I'm still there ;).\n\nYes I just now read the comment on the blog, AFAIK the message is a simple \"converting from ps to pdf\". I'm not sure if that is enough. That isn't mentioning any of the above problems and sound as if it would be a purely technical detail, which isn't interesting to the user. And it looks loke the user hasn't (at least for the first time) to confirm that he wants it anyway. So maybe he just oversees that message if it's a short document and he has a fast computer.\n\nBut maybe I'm a bit hard-bitten after all that discussion.\n\nBut now I really have to leave ... "
    author: "furanku"
  - subject: "Re: So does that mean...."
    date: 2005-07-11
    body: "bye :D"
    author: "superstoned"
  - subject: "State of --visibility-hidden"
    date: 2005-07-09
    body: "Is that compiler option now fully usable by KDE?\nWhat are the results in terms of startupspeed and memory usage?"
    author: "ac"
  - subject: "Re: State of --visibility-hidden"
    date: 2005-07-09
    body: "> Is that compiler option now fully usable by KDE?\n\nYes.\n\n> What are the results in terms of startupspeed and memory usage?\n\nFaster, and slightly smaller memory print maybe."
    author: "Anonymous"
  - subject: "Re: State of --visibility-hidden"
    date: 2005-07-09
    body: "I was thinking the support for visibility was disabled due to problems... I could be wrong, maybe..."
    author: "LuisMi Garc\u00eda"
  - subject: "Re: State of --visibility-hidden"
    date: 2005-07-09
    body: "No. Better check for buggy compilers instead.\n\nFeature itself was not broken, just few distros backported it to 3.x branch improperly."
    author: "m."
  - subject: "Re: State of --visibility-hidden"
    date: 2005-07-09
    body: "> Faster, and slightly smaller memory print maybe\n\nAny numbers, especially on that \"faster\"? :-)\n"
    author: "ac"
  - subject: "Kdevelop conference"
    date: 2005-07-09
    body: "http://www.kdevelop.org/mediawiki/index.php/Linux_Desktop_Development_and_KDevelop_Developers_Conference_2005\n\nAny report from the kdeveloper konf?"
    author: "gerd"
  - subject: "Re: Kdevelop conference"
    date: 2005-07-09
    body: "http://adymo.blogspot.com/2005/07/kdevelop-conference.html\n\nTip of the day: read planetkde.org ;)"
    author: "Anonymous"
  - subject: "Personal wishlist for focus."
    date: 2005-07-11
    body: "My personal wishlist for KDE at the moment is centered on two things:\n - Optimize! Optimize! Optimize!\n - Squash some bugs.\n\nNew features would be great, but KDE is already feature rich.  It would be wonderfull if things went smoothly all the time in addition.  As a nice example - take a look at bug 51484, reported in 2002.  It's only gotten better this week!\n\nSeriously.  Some things are horribly slow in KDE, while not with other applications.  Take 'kontact' as an example.  It's a great interface, but it tends to be horribly slow - especially if you use IMAP with kmail.\n\nI'm sure some people will say \"send us bug reports!\", but really, when things are consisently slow when accessing IMAP folders on a SuSE OpenExchange server from a SuSE machine, I would think a lot of poeple would notice it - including developers.  \n\nAs an example, if I write a little fetchmail script to just pop all email from the server - either using pop3 or imap, then use procmail to filter things - everything is retrieved and filtered within seconds.  If I use IMAP through kmail - the same operation takes minutes to achieve, unless it fails for obscure reason in the middle of the retrieval - just hanging forever.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: Personal wishlist for focus."
    date: 2005-07-11
    body: "> horribly slow - especially if you use IMAP with kmail\n\nHave you tried disconnected IMAP?\n\nIt has improved my experience with IMAP based accounts a lot.\n"
    author: "Kevin Krammer"
  - subject: "Re: Personal wishlist for focus."
    date: 2005-07-11
    body: "> when accessing IMAP folders on a SuSE OpenExchange server from a SuSE machine, I would think a lot of poeple would notice it \n\nI bet not many developers have access to an OpenExchange server at all."
    author: "Anonymous"
  - subject: "Does not matter"
    date: 2005-07-11
    body: "I use kontact on Mandrake (well, Mandriva) conecting to a postfix set-up. It is VERY slow."
    author: "a.c."
  - subject: "Re: Personal wishlist for focus."
    date: 2005-07-11
    body: "> New features would be great, but KDE is already feature rich. It would be wonderfull if things went smoothly all the time in addition. As a nice example - take a look at bug 51484, reported in 2002. It's only gotten better this week!\n\nAmple time to fix it, right? Where can I find your patch?"
    author: "ac"
  - subject: "Re: Personal wishlist for focus."
    date: 2005-07-12
    body: "> Ample time to fix it, right? Where can I find your patch?\n\nNowhere.  \n\nIf you live in Oslo or Stockholm though, you'll find yourself in a position to demand 2 beers per 20% speedup in kontact, for the following two areas:\n\n - Applying filters on an IMAP inbox folder w/1000 emails per day, filtering to local folders.\n - Retreiving all IMAP mail.\n\nSo optimize it and I'll buy you beer. ;)"
    author: "Anonymous Coward"
  - subject: "Re: Personal wishlist for focus."
    date: 2005-07-12
    body: "well, the kde dev's optimize all the time, so i guess that'll happen. if it'll be enough, tough, i'm not sure. but KDE WILL get faster anyway, as advancements are made in GCC and other parts of the toolchain KDE relies on. and KDE4 will get you a nice speedup, too, while introducing lots of features at the same time. and please join the dev's testing KDE 4, so you can find some bugs and weak points like awfull slowness in certain scenario's."
    author: "superstoned"
---
Some highlights from <a href="http://commit-digest.org/?issue=jul82005">this week's KDE Commit-Digest</a> (<a href="http://commit-digest.org/?issue=jul82005&all">all in one page</a>):
<a href="http://kpdf.kde.org/">KPDF</a> can now open PS files.
<a href="http://www.koffice.org/kexi/">Kexi</a> form designer supports drag and drop of database fields to create forms.
<a href=" http://www.koffice.org/krita/">Krita</a> now has a pixelize filter, bumpmapping and watercolor painting.
KRDC now has KWallet support.
<a href="http://krecipes.sourceforge.net/">KRecipes</a> improves printing.
Also bug fixes and speedups in <a href="http://khtml.info/">khtml</a>.


<!--break-->
