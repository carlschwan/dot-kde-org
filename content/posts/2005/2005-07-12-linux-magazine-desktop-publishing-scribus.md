---
title: "Linux Magazine: Desktop Publishing with Scribus"
date:    2005-07-12
authors:
  - "binner"
slug:    linux-magazine-desktop-publishing-scribus
comments:
  - subject: "Usual Scribus question on dotty..."
    date: 2005-07-13
    body: "How is Scribus related to KDE besides that they both use the same base toolkit?\n\nAnd for that matter, has optional Scribus integration into KDE progressed since last time there was a Scribus announcement here on the dotty (when some Scribus developers asked what KDE integration would look like)?"
    author: "ac"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-14
    body: "Scribus is pure Qt."
    author: "Saem"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-14
    body: "Everything that benefits Qt based application pool benefits KDE."
    author: "Anonymous"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-14
    body: "We have made investigations into the requirements of KDE integration. Nothing will happen in the 1.2.x series. We will do whatever we can to integrate into the platforms we now run on with 1.3.x. KDE integration will come in some sorts at some point in time, and I know thats up in the air but its not exactly trivial. The advances in 1.3.x cvs are much more important DTP wise than KDE integration at this point in time."
    author: "Craig Bradney"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-14
    body: "That's cool to hear. Is the KDE integration effort outlined or coordinated somewhere? It's barely mentioned in the roadmap for 1.3.x, and I don't know what investigations were made and what are the results.\n\n(I personally wish that effort could be more closely coordinated with KDE so that optional KDE support for pure Qt apps will become much easier to include with KDE 4, ideally similar like done with KDE themes which subclass Qt API and thus include Qt apps as well. Also more apps should be able to profit of Scribus' excellent SVG and PDF engines.)"
    author: "ac"
  - subject: "Re: Usual Scribus question on dotty..."
    date: 2005-07-16
    body: "Yes, this is really important. Scribus always looks like an alien app on the KDE desktop. And of course KDe integration means also better interoperability."
    author: "Gerd"
  - subject: "Scribus looks nice"
    date: 2005-07-14
    body: "I've done some small tests with it in the past, and while it does require some learning for someone not used to DTP apps, it seems like a very capable application. The last version I tried was a bit slow, with my AMD 2600+ with 1G it felt quite sluggish. I compare to apps like OO or the dead Linux version of FrameMaker (which was really fast even on my old P2-333). \n\nI've decided to do our next wall calendar (we do one for each Christmas to annoy our relatives) with Scribus instead of OO, so that will for me be the real test wether I can learn to use and like it as well as wether it is fast enough on \"normal\" hardware.\n"
    author: "Chakie"
  - subject: "Re: Scribus looks nice"
    date: 2005-07-14
    body: "OO cannot be compared with Scribus. Scribus is a DTP app and OO is not. \nWhich version of Scribus did you test? It works ok here (sure not slow) and I only have 1200MHz and 512mb RAM. Remember that 1.3.x is still very beta. I am using 1.2.2.1 and that works very well here.\n\n"
    author: "Carsten Niehaus"
  - subject: "Re: Scribus looks nice"
    date: 2005-07-14
    body: "I did not create a newspaper, only smaller documents, and my experience is that it feels faster that OO on my Laptop. Which is a 500Mhz P3 with 192MB memory."
    author: "Karl M."
  - subject: "Re: Scribus looks nice"
    date: 2005-07-14
    body: "Of course it can't be compared technically, but for what I want to do I can compare how the apps work for my needs. Previously OO has been the best tool for creating a table for the days of the month, importing an image, adding in some info for some days, adding in the month name and a comment for the image. That's it. If Scribus does that better I'll use it, if not, I'll stay with OO.\n"
    author: "Chakie"
  - subject: "great articles"
    date: 2005-07-14
    body: "These articles are quite informative, not only about Scribus, but about page layout and publishing in general."
    author: "anonymous"
  - subject: "Scribus is great but..."
    date: 2005-07-15
    body: "Let me first thank the developers of scribus, who have done an amazing job in very short time. The biggest drawback that I have seen is the current lack of an undo function. Without it, doing any complex desktop layout is nearly impossible.\n\nWhat's most strange about this is that they have found time to implement much more complex functionality, yet they have not done this, which is absolutely essential. This is the only thing preventing our media group from using scribus right now.\n\nThanks and I hope to see this soon or to be proven wrong in the follow-up comments."
    author: "Gonzalo"
  - subject: "Re: Scribus is great but..."
    date: 2005-07-15
    body: "From the today released development release Scribus 1.3.0 changelog: \"There is a new undo system, which includes an undo palette that has the undo history with details of each action. Undo also has object specific capabilities. The number of undo/redo steps can be set in the preferences.\""
    author: "Anonymous"
  - subject: "Re: Scribus is great but..."
    date: 2005-07-15
    body: "Thanks for the compliments.\n\nYou will be pleased to know we just released 1.3.0 as a technology preview which has a very sophisticated Undo palette with a graphical depiction of actions which can be done or undone. \n\n1.3.0 is the beginning of the long road to the next stable version of Scribus.\n\nMore details: http://www.scribus.org.uk/index.php"
    author: "mrdocs"
  - subject: "a usage question :)"
    date: 2005-07-15
    body: "If this is wrong place to ask questions, I apologize...\n\nIs there a way to make a picture, or a picture frame actually, go across two pages in Scribus? Facing pages, of course...\n\nDoing it manually like splitting the picture in two, making two frames... doesn't count.\n\nI tested Scribus the other day, and I was pretty impressed :) If it can do what I asked above, and if, when creating PDF, it manages colors the way I need it to, I might throw Quark to garbage can, and stop playing with InDesign :-)"
    author: "moi"
  - subject: "Re: a usage question :)"
    date: 2005-07-15
    body: "Try our new 1.3.0 relaease, in this version it's possible, along with a bunch of\nnew cool features.\n\nFranz Schmid\nScribus-Developer"
    author: "Franz Schmid"
  - subject: "Re: a usage question :)"
    date: 2005-07-16
    body: ":-)\n\nNice :)\n\nNow, I must ask you about the color management thing... I was told by people in a printing house, that black letters (or perhaps black color in general) must not include all the cmyk colors, but only black. When you are printing to pdf with Acrobat Distiller, you can make this setting and all is fine (In Acrobat Pro., you can chech this by turning colors on and off)\n\nIs this possible with Scribus?\n\nI am not trying to be negative, honestly. I am just evaluating real-world usability of Scribus. Thank you for providing this great alternative, and please keep up the fantastic work."
    author: "Moi"
  - subject: "Re: a usage question :)"
    date: 2005-07-16
    body: "A. Not in every case must black be 100% K. There are different flavors of black depending on paper, content and other issues. There is \"Warm Black\" \"Cool Black\" and the exact recipe not standarized for all printer/designers.\n\nB: Yes this is possible with Scribus\n\nC: The 1.3.x devel series will have even more capability with these kind of needs.\n\n"
    author: "mrdocs"
---
<a href="http://www.linux-magazine.com/">Linux Magazine</a> regularly makes all <a href="http://www.linux-magazine.com/Magazine/Archive">articles from older issues</a> online available in PDF format. Recently added is a three-part tutorial about creating a newspaper with <a href="http://www.scribus.org.uk/">Scribus</a>: after <a href="http://www.linux-magazine.com/issue/49/Linux_for_Layout_Part_1_Scribus.pdf">introducing the basics</a>, you can learn <a href="http://www.linux-magazine.com/issue/50/Linux_for_Layout_Part_2_Scribus.pdf">how to layout page objects</a> and it ends <a href="http://www.linux-magazine.com/issue/51/Linux_for_Layout_Part_3_Preparing_for_Press.pdf">talking about templates and PDF</a>. Scribus recently <a href="http://www.scribus.org.uk/modules.php?op=modload&name=News&file=article&sid=99&mode=thread&order=0&thold=0">released  version 1.2.2.1</a>.





<!--break-->
