---
title: "Dutch KDE Team Co-Releases Localized KDE-Live CD"
date:    2005-05-18
authors:
  - "Fabrice"
slug:    dutch-kde-team-co-releases-localized-kde-live-cd
comments:
  - subject: "Live CDs over rated?"
    date: 2005-05-19
    body: "Is it me, or does it seem that everyone is releasing a \"live CD\" for every little app or customization out there?\n\nWhat's next? A Live-CD version featuring the latest Kate?\n"
    author: "RMS"
  - subject: "Re: Live CDs over rated?"
    date: 2005-05-19
    body: "> What's next? A Live-CD version featuring the latest Kate?\n\nHow about one for amaroK? Oh, wait. We have that already. :-)"
    author: "Anonymous"
  - subject: "Re: Live CDs over rated?"
    date: 2005-05-19
    body: "Well it is not only the Live-CD but also the documentation shipped on it. In the case of this Dutch Live-CD we managed to squeeze in more then 60 howtos [1]. So I think you might say this Live-CD is featuring our latest and greatest howtos. I think that would justify a Live-CD :) No?\n\nCiao'\n\nFab\n\n[1] http://www.kde.nl/doc/"
    author: "Fabrice"
  - subject: "Re: Live CDs over rated?"
    date: 2005-05-20
    body: "What's wrong with having a live cd in Dutch?\nThere are about 20 miljon Dutch speakers out there, so there is enough audience for it :o)"
    author: "ac"
  - subject: "Re: Live CDs over rated?"
    date: 2005-05-21
    body: ".... you never tried the kubuntu liveCD, right?\n\nhttp://www.kubuntu.org\n\n "
    author: "Kunbuntu-freak"
  - subject: "3.3.2?"
    date: 2005-05-19
    body: "This is great and all (Since I don't speak that language it is only in an abstract way), but why not use the latest? KDE 3.4 is really nice, once again better than any previous release."
    author: "bluGill"
  - subject: "Re: 3.3.2?"
    date: 2005-05-20
    body: "You think that building a live cd is done in 1 night :)"
    author: "ac"
  - subject: "Re: 3.3.2?"
    date: 2005-05-20
    body: "Depending on your experience, whether you have to compile or binary packages are available, and the installation to produce the live CD from already exist this is possible. Only the initial effort/learning curve is big."
    author: "Anonymous"
  - subject: "Re: 3.3.2?"
    date: 2005-05-21
    body: "nope, you don't need such long, maybe if you build also livecd for ppc & amd64 \n"
    author: "Hans Wurstd"
---
<a href="http://www.kde.nl">The Dutch KDE team</a> has released a fully Dutch localized live-CD assembled jointly with <a href="http://www.mandrivaclub.nl">the Dutch Mandriva Club</a>. The live-CD allows you to try out KDE without installing anything and loads in Dutch by default, making it useful to demo to all those parents and business people.  The CD comes shipped with a full KDE 3.3.2 release plus it includes extra applications like <a href="http://www.xs4all.nl/~jjvrieze/kmplayer.html">KMplayer</a>.  Where the CD really shines is that it includes the full set of HOWTOs and tips from the <a href="http://www.kde.nl/doc">KDE Dutch documentation</a> website.  This rich set of documentation on everything KDE users of various levels will appreciate is fully localized for a Dutch speaking audience.



<!--break-->
<p>The Live CD ships a wide range of languages besides Dutch, non-KDE applications like <a href="http://gaim.sourceforge.net/">Gaim</a> and <a href="http://www.mozilla.org/firefox">Firefox</a> and it uses a filesystem named Unionfs allows you to store any change in settings on your USB pen to save it across reboots.</p>

<p>The CD was assembled for KDE-NL by the friendly people from <a href="http://www.mandrivaclub.nl">Mandrivaclub.nl</a> and the ISO is available for free download on a number of sites:</p>

<ul>
<li><a href="http://ftp.nluug.nl/ftp/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL/">
http://ftp.nluug.nl/ftp/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL</a></li>

<li><a href="http://ftp.surfnet.nl/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL/">
http://ftp.surfnet.nl/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL</a></li>

<li><a href="ftp://ftp.nluug.nl/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL/">
ftp://ftp.nluug.nl/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL</a></li>

<li><a href="ftp://ftp.surfnet.nl/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL/">
ftp://ftp.surfnet.nl/pub/os/Linux/distr/mandrakeclubnl/mcnllive/KDENL</a></li>


<li>rsync://ftp.nluug.nl/mandrakeclubnl/mcnllive/KDENL</li>
<li>rsync://ftp.surfnet.nl/mandrakeclubnl/mcnllive/KDENL</li>

</ul>

<p>You can also read the <a href="http://www.kde.nl/archief/2005-05-06-KDENL-liveCD/index.html">Dutch press release</a>.</p> 


