---
title: "The Social Structure of KDE Development"
date:    2005-02-01
authors:
  - "Tom"
slug:    social-structure-kde-development
comments:
  - subject: "usability unimportant?"
    date: 2005-02-01
    body: "\"For most developers, design and usability are unimportant because they are used to handling complex software when programming.\"\n\nCareful there, Andreas...If this was true, KDE would be a complex, unusable mess, wouldn't it?  You also imply that KDE devs do not listen to the usability team.  As a \"peripheral\" KDE dev and a monitor of kde-core-devel, I find nothing to support this claim.  In fact, I think it's widely understood among KDE devs that usability is one of our most important goals...\n\n"
    author: "LMCBoy"
  - subject: "Re: usability unimportant?"
    date: 2005-02-01
    body: "I noticed that even when developers are publicly moaning at kde-usability they are implementing solutions which are discussed there."
    author: "m."
  - subject: "Re: usability unimportant?"
    date: 2005-02-04
    body: "About usability and contact between developers and users is an interesting problem. My impression was that developers say: if you have an idea code it! What do you think? Do you take ideas from users? When and Why?\n"
    author: "Andreas Brand"
  - subject: "Re: usability unimportant?"
    date: 2005-02-05
    body: "I guess the best way is thinking of it as inward marketing: every idea proposed by a user need to be sold to the right developer. That means the user has to get the right developer to think that realizing his proposal is a good idea and worth taking the time for. In that regard the usability list is pretty much Nirvana since many people talk there without trying to contact developers, neither respecting the current state of development nor trying to present a vision in a step by step way which would make it easier to see what exactly needs to be realized and what's there already. Most developers are able to pick up ideas easily when the above is avoided, the requesting person makes a honest effort easying the realization for a developer through different means (e.g. having at least some base understanding how the whole system works) and the idea seems good enough. Easy as that really."
    author: "ac"
  - subject: "Re: usability unimportant?"
    date: 2005-02-08
    body: "I am not a developer but user with some successes to 'modify' KDE programs.\n\nYou have to:\n- have good arguments\n- contact right developer\n- make contact in good time\n\nI noticed that developers are quite eager to do code themselves when you send them complete description what should be done. They don't have to think about\nboring details just code exactly what you have described.\n\nThey become angry when you write: This is bad - change it.\n\nBut when you describe in very detailed way:\n1. Why it is bad (with examples/scenarios)\n2. What solution should like - down to strings, icons, etc.\n\nThere is very big chance of implementing your proposal."
    author: "m."
  - subject: "Re: usability unimportant?"
    date: 2005-02-08
    body: "What about that most users do not know how to term accuratly what they want? And what about that most users do not have enough knowledge about coding and kde?\nI know that they could write in bugzilla what they want. does it really help? and how is in the usability list coped with this contributions? \ni have seen that at sourceforge the os-project phpsurveyor has a survey where the people could write what they want for the future? would it work for kde?\n"
    author: "Andreas"
  - subject: "New members"
    date: 2005-02-01
    body: "I find myself quite amazed to see new members joining and filling gaps left by others. I arrived in the project myself like an alien landing on the planet Earth. I immediately felt at ease with everyone and I found the community very friendly. I now try to encourage newcomers as much as I can. A new KDE-Edu developer today gained a cvs account, he worked on Kiten, sent a few patches and then ported it to KConfig XT. What helped him as well are the mailing lists and he got quick answers on a few technical points. \nI meet lots of amazing people and my only regret is not to have met them in real life yet. "
    author: "annma"
  - subject: "Re: New members"
    date: 2005-02-04
    body: "As an engineer, I would say that my experience is the opposite.  I felt like an earthling landing on an alien planet.  Or, actually a person from a planet populated by engineers landing on a planet populated by hackers.\n\nThe problem remains.  Hackers don't' understand the engineering method, and I find that although I understand some of the social organization of hackers all too well, this does not mean that I am able to work with some of them.\n\nI have had some positive experiences with some that understand that a detailed description of exactly what is wrong with something is a great help and the Artists have been quite supportive eventhough I state up front that I am a draftsman not an artist.\n\nBut, with many of the hackers, I still feel that I am unwelcome on an alien planet -- why would anyone truly interested in improving the product consider a bug report, a suggestion, a patch, or an offer of help, an insult or an annoyance?\n\nThis inability for either me, the engineer, or them, the hackers to bridge this culture gap is our mutual loss. :-(\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Recruitement"
    date: 2005-02-02
    body: "This was an interesting read:\n\n\"So far, new, self-motivated members were recruited via Linux conferences or via articles in the media. Most recruited members are young and highly qualified IT experts who come from Northern Europe. Why not try to get further IT experts in other countries e.g. the Americas, India or China? And why not direct recruitment to both older IT experts and to non-IT experts? The non-IT-experts could be introduced through translation and work on graphics/sounds. However, they need to enter a certain channel in order to get more involved, e.g. from translation to software development.\n\nLooking upon the ways in which recruitment is pursued and managed, a special group for this task is missing. This group could steer the recruitment efforts in internet forums, on- and offline journals, etc.\""
    author: "ac"
  - subject: "Interesting article"
    date: 2005-02-02
    body: "That was a really interesting article, thanks Tom."
    author: "ac"
  - subject: "Take me to your leader"
    date: 2005-02-02
    body: "What is surprising about KDE is the lack of organisation and direction. This may be more due to the nature of the project than those who would attempt to control it. KDE is so broad that very seldom are there competing implementations where someone would have to impose a choice. The core libraries are based on Trolltech's work and vision, which imposes a style upon the rest of the codebase. From there the services provided are often solutions to obvious problems; IPC, transparency by ioslaves, kiosk for lockdown, etc. The apps are filling obvious needs, and anyone looking for something useful to do has almost too many choices. So the only controls needed are a certain discipline in the repository, and an attempt at putting together releases.\n\nAny one of the subprojects could absorb all the talent and effort that the whole of KDE consumes. Konqueror, PIM, KOffice (or even KWord alone) the multimedia, graphics etc. If that was to happen, ie. if KWord attracted 200 or so active developers, then some kind of organisational scheme would be necessary.\n\nForm follows function. I suspect that as KDE matures, there may be a need for tighter control.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Take me to your leader"
    date: 2005-02-02
    body: "Your post sparked a thought in me. What would happen if each release began to have a certain functional focus. To take your example, what would happen if suddenly everyone made Koffice the focus of the next release. \n\nI think a veritable jump in development, stability and features would ensue and this sort of intensive hacking could be a boon to the parts of kde that are in need of hackers. Even reserving a week or two every six months in which everyone was asked to help in improving a certain functional set of apps. \n\nOne release the focus could be Koffice, the next one Kmultimedia, the next one, it could be stability, bugs, stability, bugs.\n\nI realize that ultimately people work on whatever they want to work, but I still think that highlighting certain areas to focus on would be good for the whole of KDE.\n\nJust some ideas."
    author: "Gonzalo"
  - subject: "Re: Take me to your leader"
    date: 2005-02-02
    body: "I hate to take a negative view of an idea that _could_ make a big difference: have all the developers focus on one area, test the heck out of it, polish everything till it shines, make all the icons pretty and consistent, and get the docs up-to-date and everything.\n\nToo many cooks spoil the broth. Learning curve. Herding cats.\n\nHaving 600 people committing in one area is just plain too messy. Here you _do_ get quality control issues -- there are maybe 8 people who understand KMail, for instance, and anyone else committing there is going to need extra scrutiny from the people best suited to judge the effects of such a commit. \n\nYou can't force people to work on some particular area, and even having some announcement \"this release is going to focus on koffice\" doesn't push people towards actually working on that area. At most, it heightens user expectations, and that's even more of a letdown. Didn't we try this already, either with PIM or with Office?\n\nThis is not to say that a similar idea works but _at a smaller scalle_. Look at the Osnabrueck meetings for the PIM guys -- 12 guys, one room of laptops, and a weekend of hacking. It's in its third year, and we're organizing an additional meeting in .nl in may (still looking for travel sponsors, too). I'd suggest you try it as well -- get a case of beer, some friends with coding skills over, and just pick an application (kolourpaint? kcalc?) and work on it. Write down the bugs you encounter immediately, then dive into the code. At this scale, you can be fantastically productive."
    author: "Ade"
  - subject: "Re: Take me to your leader"
    date: 2005-02-02
    body: "It's something I want to try to organize this summer hols for Krita -- I've got room for all Krita developers the week that my kids go on summer camp with the church. That's the last week of July. (Another week would be fine, too, there's room enough anyway, but people will have to doss on the floor in the living room.) Beer is no problem either :-). Perhaps I should start warming Krita hackers to this idea right away..."
    author: "Boudewijn Rempt"
  - subject: "Re: Take me to your leader"
    date: 2005-02-05
    body: "> pick an application (kolourpaint? kcalc?) and work on it. Write down the \n> bugs you encounter immediately, then dive into the code. At this scale, you \n> can be fantastically productive.\n\nWhat needs to be done with either of those apps?  They both appear finished.\n"
    author: "jfvg"
---
<a href="http://www.newsforge.com/">NewsForge</a> has published <a href="http://programming.newsforge.com/article.pl?sid=05/01/25/1859253">an interview I conducted with Andreas Brand</a>, a sociologist who is studying the free software world. I met him at <a href="http://conference2004.kde.org/">aKademy</a> last year, where he was in the middle of research on KDE as an example of an open source project based upon collaboration without hierarchies. Read the full interview to learn more about his views on KDE's development model, volunteer recruitment and retention, motivation, work distribution and more.



<!--break-->
