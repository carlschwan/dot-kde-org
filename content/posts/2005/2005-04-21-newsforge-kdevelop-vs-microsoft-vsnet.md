---
title: "NewsForge: KDevelop vs Microsoft VS.Net"
date:    2005-04-21
authors:
  - "brockers"
slug:    newsforge-kdevelop-vs-microsoft-vsnet
comments:
  - subject: "for Windows"
    date: 2005-04-21
    body: "I think Kdevelop for Windows + qt4 and the GNU toolchain could be a real blow to Microsoft's Visual Studio."
    author: "Andre"
  - subject: "Re: for Windows"
    date: 2005-04-21
    body: "I think MS's Visual Studio's strongest point is support for .Net.  I have a copy of VS.Net 2003 (edu edition) for school because I have a class on VS (its on using VS.Net to write VB and C++, I just want it for the C++ part)."
    author: "Corbin"
  - subject: "Re: for Windows"
    date: 2005-04-21
    body: "I think MS Visual Studio + Visal Assist X still beats Kdevelop. \nWithout VA KDevelop is better. \nI'm a linux guy, but at work I develop a cross platform application, I work under Linux the others under MSVC+VA, and I have to admit their editor is better.\n\nBrain"
    author: "Brain"
  - subject: "Re: for Windows"
    date: 2005-04-21
    body: "Ok, I don't use VA but I've looked at their web page.\nVA: \"Suggestions and Acronyms\"\nKDevelop: Abbreviations (both manual and automatic), warning! automatic is not enabled by default\n\nVA: \"Enhanced Syntax Coloring\"\nIIRC modern kate editor part does exactly the same.\n\nVA: \"Improved Intellisense\"\nYes, code completion in KDevelop needs more work.\n\nVA: \"Underlining of spelling mistakes\"\nYou can spell check using kate editor part spellcheck plugin (but IIRC it will not highlight errors).\n\nAnd finally:\nVA is for 99$\nKDevelop is for 0$ ;)\n"
    author: "Alexander Dymo"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "Code completion is a killer feature.  Coding without it (or with an inferior version) is painful after you've gotten used to it.  That is my \"most wanted\" feature in an IDE.  Excellent fast context-sensitive documentation search and code browsing would be a close second, debugging would be third, and code refactoring would be fourth.  Project management, source control integration, and GUI design are somewhere underneath those.\n\nI haven't used KDevelop lately, but I am under the impression that code completion and debugging are generally inferior to Visual Studio.  That means for me Visual Studio is still better.  If just those two features were improved, I would probably prefer KDevelop.\n\nHopefully instead of just playing catch-up with visual studio, KDevelop can leapfrog ahead.  For example, if KDevelop added a \"rewind\" capability to its debugger (I guess it would have to be added to GDB first) then that would make it way better than Visual Studio.  It doesn't seem like it would be very hard to do, if you recorded checkpoints in the program's execution (though it might take a lot of memory).  "
    author: "Spy Hunter"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "While KDevelop (through GDB) is not as nice a debugger as MVS.Net; .Net is not nearly as nice a API to develop for.  Anyone I know who has worked on Qt and MFC or .Net will pick Qt. It's really a best-of-bread API.  And try GUI application development in VS and Qt Designer; its not even close.  KDevelops overall feel is much nicer than VS, and I have never had problems with its code completion.\n\nBobby"
    author: "brockers"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "One can also program with Qt and VS ... (best of two worlds?)\nBut code completion _is_ inferior. It only works for libraries one extra makes a database for. In VS it simply works without doing extra configuration.\nOne really annoying thing in KDevelop is that that it still doesn't remeber it's GUI-settings :-(\nOn the other side bookmarks in VS are a joke - simply unusable.\nAnd one tiny little bonus in VS's debugger: if a breakpoint is reached, the IDE-windows comes to the front. KDevelop stays in the background forever until the developer notices, that a breakpoint was hit.\n"
    author: "Birdy"
  - subject: "Re: for Windows"
    date: 2005-04-23
    body: "While MFC is of course horrible, I have to say that .NET is not too bad.  Using C# and Windows Forms is a decent way to make a small Windows application.  For the most part, the APIs are simple and consistent.  Often you can code up a new feature using only Intellisense, without ever going into the documentation, because the names of classes and functions are easy to figure out and make sense.  C# is nice because of properties, bounds checking, and garbage collection.  Performance has not been an issue at all for me.\n\nThe biggest problem with .NET is probably that it is incomplete; large parts of the Win32 API are not wrapped with nice .NET classes, meaning that you have to fall back to ugly MFC type stuff to do many important things, made even harder by the fact that you have to translate from C to C#.  Also, some features common in modern applications are simply not available in Windows Forms, such as docking toolbars.  Another problem is that the framework is not Model-View-Controller oriented; in fact it almost seems to discourage a MVC design, especially when using the GUI designer.\n\nSo QT is probably still much better when developing large, performance-critical applications with complex UIs.  But don't discount .NET so easily.  In fact, this summer Windows Forms 2.0 is adding more functions including docking toolbars, and C# 2.0 is adding generics, which means that C++'s only real remaining advantages are slightly better performance by default and possibly tighter manual memory management."
    author: "Spy Hunter"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "Even more of a killer feature is live syntax checking. Eclipse does this and it really convenient. While your typing, syntax is checked and incorrect code highlighted. In fact more is checked. Unknown variables and classes are also highlighted. This really helps to speed up coding because you don't have compile before you see all the obvious errors.\n\nAlso if you remove a function from a class, immediately an error icon is shown on all files using that function.\n\nFurthermore, code is compiled in the background and the program preloaded. This means that test launches of the program are very fast (for java).\n\nImplementing such features are amazing and the Eclipse people did an excellent job. To achieve such a feat for C++ code is more difficult than for Java because the semantics are less strict. (e.g. every public java class has it's own file and there are no headers, only .java files).\n"
    author: "Jos"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "\"Even more of a killer feature is live syntax checking. Eclipse does this and it really convenient. While your typing, syntax is checked and incorrect code highlighted. \"\n\nKDevelop has this for C++"
    author: "Richard Dale"
  - subject: "Re: for Windows"
    date: 2005-04-23
    body: "And for pascal."
    author: "Alexander Dymo"
  - subject: "Re: for Windows"
    date: 2005-04-27
    body: "\"\"Even more of a killer feature is live syntax checking. Eclipse does this and it really convenient. While your typing, syntax is checked and incorrect code highlighted. \"\n \nKDevelop has this for C++\"\n\nHas it?  I only started using KDevelop last weekend, but so far it seems rather inferior to Eclipse for Java.  I've read so much praise for KDevelop that I suspect I must be missing something.  How do I enable this feature?"
    author: "Andreas Pagel"
  - subject: "Re: for Windows"
    date: 2005-04-28
    body: "KDevelop is much better for C++ than it is for Java. Maybe use Eclipse for Java coding, and KDevelop for other languages or for very small Java projects.\n\nStart typing some C++ and you should see a red icon with a cross in it when the currently line is syntactically invalid. I don't think you have to turn the feature on."
    author: "Richard Dale"
  - subject: "Re: for Windows"
    date: 2005-05-02
    body: "Thanks for the reply.  I realise I wasn't clear in my previous message - I meant that I had started to use KDevelop for C++, and was comparing it to Eclipse for Java.\n\nI see now that the syntax checking does work, just that it's not as comprehensive as Eclipse's.  I tried entering an undefined variable and nothing happened (Eclipse would underline it), but when I enter random rubbish the red cross icon does appear.\n\nAndreas."
    author: "Andreas Pagel"
  - subject: "Re: for Windows"
    date: 2005-04-23
    body: ">> Excellent fast context-sensitive documentation search and code browsing would be a close second\nWell, I think that this is the area where KDevelop is one of the best IDEs. Our quick open functions and KDevelop Assistant are very useful. Of course, you should have documentation installed so KDevelop could find it (it does that automatically for qt/kde/gtk/gnome/other devhelp docs)."
    author: "Alexander Dymo"
  - subject: "Re: for Windows"
    date: 2005-04-25
    body: "i am deaf from nigeria very pleasure to join with you, i want to read something from you like computer engineering , exchange micrsoft, hardware engineering and others. i am student of hardware for two months will be  finish for six months till i hear from you thanks. please send me p.o. box 458 kaduna."
    author: "mukaila hassan"
  - subject: "Re: for Windows"
    date: 2005-04-21
    body: "Well, MS has .NET, we have GTK, Qt, KDE, GNOME, whatever? The importance of .NET on Unix is still questionable. There are many equal technologies. If c# and mono will get more attention, I'm sure somebody will write a KDevelop language support plugin for c#/mono."
    author: "Alexander Dymo"
  - subject: "Re: for Windows"
    date: 2005-04-21
    body: "I'd prefer KDevelop for Windows + Qt4 + MinGW - GNU autotools ;) Same for Linux though."
    author: "Alexander Dymo"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "It may be a blow to Microsoft Visual Studio but it will almost certainly not help Linux/BSD adoption.  I know of at least two developers who work on KDE/Qt on Linux because Qt is free and they have access to a great Application Development Environment for nothing (KDevelop)... but they have have told my outright that \"if KDE, KDevelop,and Qt were available on Windows I wouldn't be running Linux anymore.\"  That is why these KDE-to-Windows ports scare the daylights out of me. \n\nBobby\n\n"
    author: "brockers"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "Well quite. If you're porting this stuff to Windows, what's the point in an alternative platform? Worse, people are porting desktop applications to a platform where they don't fit, duplicate an awful lot of technology, don't integrate very well and are on a platform where one company gets to dictate to you through technology what you can and cannot use to develop those applications."
    author: "David"
  - subject: "Re: for Windows"
    date: 2005-04-22
    body: "Also as a developer, am I going to want to work on a product that is mainly used by windows users (if that happens), when I'm rooting for the Linux team?"
    author: "JohnFlux"
  - subject: "debugging"
    date: 2005-04-21
    body: "does anyone knows if these features are or will be available in Kdevelop or is that a GDB issue?\nhttp://programming.newsforge.com/comments.pl?sid=45994&cid=110824\n\nPat"
    author: "Pat"
  - subject: "Don't try to hide..."
    date: 2005-04-21
    body: "The guy who wrote the article seems a bit stupid to me: he tries to hide all project filenames and code in his screenshot - but leaves the titlebar with the full path of the file in it just as is.\nSee http://www.newsforge.com/blob.pl?id=2c6a5a6f8a3e42a889d8364f5d729e21"
    author: "praseodymium"
  - subject: "Go KDevelop, go!"
    date: 2005-04-21
    body: "And don't loose your faith because of the comments at Newsforge. But the debugger can be improved indeed. It does some strange things from time to time that I cannot understand..."
    author: "Andras Mantia"
  - subject: "Re: Go KDevelop, go!"
    date: 2005-04-22
    body: "\"And don't loose your faith because of the comments at Newsforge.\"\n\nYes, I've never come across quite such rude and un-called for comments. I hope Olaf Piesche wasn't put off by all the personal attacks, and I'd like to thank him for doing an interesting and fun review. \n\nI started off complaining about rudeness, but ended up calling one of those creeps a 'Fascist'. I suppose the best tactic is just to ignore them."
    author: "Richard Dale"
  - subject: "Re: Go KDevelop, go!"
    date: 2005-04-30
    body: "Hmmmm. I read the comments at News Forge, and I have to say, I think most were right on. It was a pretty lame review. And as far as calling one of the creeps a facist, I'm not sure you understand the meaning of the word. It was clear to me that the News Forge crowd, a decisivly pro-linux group, thought that the review lacked any real effort. I don't think demanding integrity and honesty in comparing Linux based applications to similar apps in Windows constitutes facism. On the other hand, trying to suppress such dissent, which your comments at News Forge seemed to be trying to do, might be construed as such. I'm not sure I would be proud of calling the commenter a fascist, if I were you. Especially considering that you threw that out as \"the last word\"."
    author: "Orion"
  - subject: "Re: Go KDevelop, go!"
    date: 2005-04-30
    body: "> I'm not sure I would be proud of calling the commenter a fascist, if I were you. \n\nI'm pretty sure he isn't.   Isn't that *why* he stopped commenting?\n\n"
    author: "cm"
  - subject: "Re: Go KDevelop, go!"
    date: 2005-04-30
    body: "Seems like you don't know Godwin's Law (http://en.wikipedia.org/wiki/Godwin's_law)\n"
    author: "ac"
  - subject: "This is pure silliness.."
    date: 2005-04-21
    body: "KDevelop is wonderful, it truly is.  But it is not any where close to MS VS.NET.  The tools that come with VS are insanely great and not to mention the coding time feedback and assistance (online error catching and intellisense) is spectacular.  I won't get into compiler feed back since this isn't KDE's fault.  Kdevelop is definitely a very good IDE, but MS VS.Net is basically the best IDE ever made for C/C++/VB/C#, with the only draw back being that it's MS specific.\n\nFor Java, nothing touches JetBrain's IntelliJ, NOTHING!  The feedback, error checking, debugging, refactoring, it's insanely great!  Not to mention how mind bogglingly fast that thing is.\n\nKomodo is probably best for Python and Perl.\n\nWhile Zend ekes them out for PHP.\n\nIf you give IDEs an honest chance and forget the political baggage for a second, then you'll agree, for the most part.  This isn't categorical superiority, merely overall.\n\nThat doesn't detract from Kdevelop and say Eric, no, they're great, they're just against some very strong and established competition.  Not only that, but they're getting there and in some cases getting further!  I love them, but let's be honest, please!"
    author: "Saem"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-21
    body: "> online error catching\nOn KDevelop it's called \"Problem reporter\". And it works.\n\n> intellisense\nOn KDevelop it's called \"Code completion\". It works since KDevelop 3.0 but it has been very improved in KDevelop HEAD\n\nRegards\nAmilcar Lucas"
    author: "Amilcar Lucas"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-22
    body: "MS' is superior that was my whole point!  So it may have something like it, doesn't mean it's anywhere as good."
    author: "Saem"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-24
    body: "You are far more offensive... look at your title: \"... pure silliness\""
    author: "randomidiot"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-22
    body: "> online error catching\n\nAm I missing something?  You mean it will point out errors as you type, like Eclipse does?  I work with Visual studio 2003 all day, but I've never seen this.  And frankly, intellisense is not impressive.  It likes to tell me that functions and variables are available when they are not, or just randomly refuse to show me anything, even though the code compiles cleanly.\n\nMaybe I've been spoiled by Eclipse, but VS.NET is not all that great in my experience."
    author: "Leo S"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-22
    body: "In addition:\n\n - Refactoring with VS.NET sucks.  And by sucks I mean, its impossible.\n - VS.NET likes to get confused about its own resource files that it generated just moments before from one of its own templates.  It will absolutely refuse to open them.  This required a reinstall (which takes at least an hour or two).\n - The user interface is slow, and will hang for a few seconds once in a while for absolutely no reason (ok, I was trying to scroll down in my code).\n - Keyboard shortcuts are inconsistent.  Sometimes you need an emacesque chord (CTRL-K CTRL-C to comment), sometimes a simple shortcut (CTRL-F5), and sometimes 3 keys (CTRL-SHIFT-B).  \n - Which reminds me, the comment function is incredibly stupid.  If you select a block of code, and hit CTRL-K CTRL-C to comment, it will sometimes comment it with /* */ and sometimes with // on every line.  This seems to happen randomly.  Trying to uncomment this block of code will only work if it was commented with //.  The Uncomment command is blissfully unaware of the /* */ that the Comment command just added.\n\nAhh. now I feel better..  let all the rage out.... :)\n\n"
    author: "Leo S"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-22
    body: "To be fair, I just fired up KDevelop to see if I can't find something to bitch about.  Sure enough, I found something.\n\nWhy are there tabs for CVS and Subversion, as well as a context menu item for Perforce, even though I am not using any of these, nor do I even have them installed?\n\nAlso, code completion is quite weak.  But the problem reporter rocks!\n"
    author: "Leo S"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-22
    body: "Just noticed, the tabs for CVS and Subversion and Perforce are not there in 3.2 if I don't use those VCS's.  Nice."
    author: "Leo S"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-22
    body: "The interface has some quibbles, though I don't encounter much slowness.  But perhaps that's just my machine -- PIII 866 with 512MB RAM. ;)\n\nIn anycase, C++ intellisense is a very hard problem, but I haven't heard of folks having half the issues you're talking about, things tend to be pretty flawless so long as you exercise some caution, you have to, because well C++ is crap to parse live.\n\nRegardless, VS.NET is still the C++ IDE."
    author: "Saem"
  - subject: "Re: This is pure silliness.."
    date: 2005-04-23
    body: "My work machine is a P4 1.7GHz w/ 512MB ram.  VS.NET sometimes is quite slow to respond.  Many people say the same, VS6 was fine.\n\nMy issues with Intellisense are quite real and quite repeatable, also on my colleagues laptop.  Intellisense is good, better than KDevelop's code completion, but it's not great."
    author: "Leo S"
  - subject: "Re: This is pure silliness.."
    date: 2005-11-26
    body: "Humm...\n\nCTRL-K CTRL-C Comments with // if you select all full lines, and with /* */ if your selection starts somewhere and ends somewhere not selecting the FULL lines. There is no random there.\n\nFor the uncommenting, if you make the SAME selection that you used to apply the comment, CTRL-K CTRL-U works great. \n\nNot knowing how to use a feature does not justify bitching about it. Maybe if you were to code such a feature you'd do it better? I find the way it works very reasonable and very good, if you understand how to use it.\n\nAs for the shortcut-key stle, most of them are the same as in previous versions. What if they'd keep changing the shortcuts every version to keep it consistent, would that be better? Oh, but then you'd be complaining like \"The shortcuts keeps changing in each versions! How Stupid!\". If you don't like the keys, you can customize all of them to something you like better anyway.\n\nMy two cents..."
    author: "Antoine"
  - subject: "Re: This is pure silliness.."
    date: 2005-05-18
    body: "I absolutely agree. \nOnly people who've never used Eclipse like VS 2003. VS's intellisense doesn't work well 50% of the time. Trying to find references to objects is a real pain! You have to learn reg.exp and do a text search to find anything. Just try Control-click on any var in Eclipse and you will see the power of a real dev environment :). And its free! And it easy to add functionality like unit test support! \n3 Cheers for Eclipse!\n\nI have to use VS too, and in my opinion, VS is crap and expencive. \n\nYannik Klich "
    author: "Yannik Klich"
  - subject: "Re: This is pure silliness.."
    date: 2005-06-08
    body: "I have to disagree here\nAlthough Eclipse looks very promising, it is not even near VS.NET 2003 + Whole tomato software's Visual Assist. Eclipse has a more robust auto complete than Visual Assist, however this tends to be inconviniet for C++ - It is usually not possible to parse the code while the programmer is wrting it - it won't be a valid  C++ code. And it is not possible to know members of template classes (though it would be possible to deduce them from the template instatiations). Visual assist makes a lot of heuristics, but manages in > 90% to give me the proper sugggestion as I type. On the other hand, CDT's (2.x) code completion does not work for most of those cases. \nAnd eclipse is too slow (Athlon XP 3.2 Ghz 1Gb of ram) under linux, and unpleasent slow under windows. So I prefer to stick with VS.NET for windows and Kdevelop for linux.\nKdevelop is nicer than Eclipse in the sence that it is much faster, I can rearenge the keyboard shortcuts in no time (Great work for that) and from my experience it is like some alpha build of Visual Studio 5, so I feel convinient withg it. VS still rocks and I doubt KDevelop will ever catch up - quality software requires money, to pay all those programmers that have to finish the nasty details of an end-user product. And KDevelop's funding is in my opinion very limited compared to VS's one.\nBut anyway - keep up the good work. Kdevelop is in my opinion on second place in the C++ IDE rank list - after VS - and is one of the few products for linux that I like\n\nStefan Popov"
    author: "Stefan Popov"
  - subject: "Re: This is pure silliness.."
    date: 2008-06-27
    body: "You're comparing VS.NET to KDevelop? Compare Mono to it instead, it's much closer, considering it's actually managed. And who uses managed C++ anyways?? It's got to be one of the most useless and annoying bindings ever."
    author: "Shaun Reich"
  - subject: "kdevelop refactoring"
    date: 2005-04-22
    body: "hi!\n\nI'm using kdevelop at university for some c++/qt software. Except for the occasional crash, I like it very much. I just have one question:\n\nHow can I rename classes? Right now, I have to rename the cpp and h on disk and search and replace the class name, then change the other file's includes etc. Isn't there an option for that in KDevelop?\n\nthanks!"
    author: "me"
  - subject: "KDevelop is great -"
    date: 2005-04-22
    body: "If only someone bothered to fix the memory leaks and made sure the features work reliably - meaning no random crashes and taking a machine with 1Gb Ram down. I think KDevelop suffers from bloat of features aggravated by lack of bug fixing."
    author: "frus"
  - subject: "Re: KDevelop is great -"
    date: 2005-04-22
    body: "For me KDevelop is more stable than VS 2002..."
    author: "Birdy"
  - subject: "Re: KDevelop is great -"
    date: 2005-05-17
    body: "Use HEAD and you'll see the bug fixes comming in.\nIf you do not update the bugs wont go away.\n\n"
    author: "Amilcar"
  - subject: "Re: KDevelop is great -"
    date: 2005-05-20
    body: "I've been using KDevelop at work for 18 months and i *Hate* it.  Every time a new version comes out, they introduce more bugs than they fix.  With my current version I can't add or remove files from the project *duh*????"
    author: "Den"
  - subject: "Re: KDevelop is great -"
    date: 2005-06-18
    body: "Well, I've seen that problem too, although I've always attributed it to the fact that I'm using a symlinked working copy (via aegis).\n\nAnyway, I've added scripts to aegis.conf so that whenever I add/remove/move file via aegis, the project.kdevelop.filelist gets modified too. Then I just reopen the project in kdevelop and that's it. Once you get used to things it's doesn't do much harm, especially since I run the aegis stuff via commandline anyway (http://aegis.sf.net).\n\nI was actually hoping to fix those and other things for the SoC, but I didn't hear back yet.\n\nCheers, Kuba"
    author: "Kuba Ober"
  - subject: "KDevelop vs. .NET"
    date: 2005-04-22
    body: "I use .NET and Codewright at work for custom code development.  The only reason I use .NET is to build .DLLs.  I use Codewright because that's what they purchased for a IDE to use with gcc running under Cygwin.  At home I use KDevelop, and I consider it to be better than both.  I love it!!!!\n\nMy two cents:\n\n.NET is too compilcated to setup the project build environment properly.  The menu layout is horrible, and file management within a project sucks.  Who decided to track project files based off the current project directory instead of absolute links?  Or even re-linking the files in the project if the project file moves or when I restart?  And why doesn't the built in debugger work properly, if at all, when I've got VB, C++, and C# code in the same app?  \n\nCodewright is a good editor better than .NET, but lacks the nice build environment features of KDevelop.  Plus it's now being EOLed by Borland.  If I had a second PC, I'd slap on Linux w/KDevelop for my gcc compiled code.  It also lacks a debugger.  I'd also like to try KDevelop on QT-Win32."
    author: "Dave Maurer"
  - subject: "Re: KDevelop vs. .NET"
    date: 2007-11-23
    body: "Are you kidding about Kdevelop? It doesn't even work. I've never got passed the am-prog-lib tool error"
    author: "bill"
  - subject: "The EMACS world ..."
    date: 2005-04-22
    body: "I know a good number of developers who like using GNU/EMACS for development (independent of the platform). There are lots of elisp extensions that exist which seem to provide similar features as VA. The semantic bovinator ( http://cedet.sourceforge.net/semantic.shtml ) derivatives try to provide a project interface (which can be made better with a little more work.)"
    author: "Beta"
  - subject: "Ruby and KDevelop"
    date: 2005-04-22
    body: "How good is Ruby-support in KDevelop? Are there frontends for RDoc or ri to browse documentation? I can't find syntax-highlighting for .rhtml-files (html-files with Ruby-commands like <% puts \"Test\" %>)\nI can create Ruby-projects and even Korundrum-projects. They work out of the box which is pretty cool. But some features don't work (for example, I can 'run' a project, nothing happens. Starting the very same file from the command line works) and I couldn't spot any advanced features like those above.\n\nI am using KDevelop 3.1 on Gentoo. I am pretty sure that KDevelop is better that this and that I simply misconfigured something. Perhaps you could give me some pointers :-)\n\nAs of now I am using gvim to code Ruby but would like to switch to KDevelop in theory!"
    author: "anon"
  - subject: "Re: Ruby and KDevelop"
    date: 2005-04-22
    body: "\"Are there frontends for RDoc or ri to browse documentation? I can't find syntax-highlighting for .rhtml-files (html-files with Ruby-commands like <% puts \"Test\" %>)\"\n\nNo, not yet. A graphic front end to Interactive Ruby (irb), would another really useful feature.\n\n\"But some features don't work (for example, I can 'run' a project, nothing happens.\"\n\nI'm not sure what's gone wrong here - click on the ruby icon and your program should run.\n\n\"I am using KDevelop 3.1 on Gentoo.\"\n\nKDevelop 3.2.x ruby support is much improved. And KDevelop 3.1 didn't have any Korundum project templates although you talk about them, so I'm not sure which KDevelop you're actually using. Does your version have a ruby source code debugger and Qt Designer integration for instance?"
    author: "Richard Dale"
  - subject: "Re: Ruby and KDevelop"
    date: 2005-04-22
    body: "You are right, this is 3.2.0 (and KDE 3.3.2). I have a debugger. I will look at the problems in the next few days. In case I can't find out what is wrong I will file a bug-report for it.\n\nQuestion: If I am doing an app as simple as puts \"test\": Where should this output go to?\n\nThanks for the infos, Richard!"
    author: "anon"
  - subject: "Re: Ruby and KDevelop"
    date: 2005-04-22
    body: "\"Question: If I am doing an app as simple as puts \"test\": Where should this output go to?\"\n\nThe output should appear in the 'Application' tab. It also shows the command used to invoke the ruby program - so maybe you could try typing exactly the same command from the konsole to investigate why the 'run' button doesn't work."
    author: "Richard Dale"
  - subject: "While I like kdevelop..."
    date: 2005-04-22
    body: "it doesn't hold a candle to VisualStudio yet imho, particularily with C++.\n\n- inferior code completion\n- vastly inferior debugger\n- a bit more cluttered\n- things just feel less polished\n\nI hate most Microsoft software, but VisualStudio is very good. "
    author: "anon"
  - subject: "Code parsing"
    date: 2005-04-22
    body: "I keep wanting to work with the on-the-fly-syntax-parsing as I have a few ideas that I think I can implement.  Everytime I work up the energy to do so, there's always a rewrite of the code parsing code that will be completed real soon and some of the other extras that I need that will also be done real soon now.\n\nI'm really excited about kdevelop, and hope that I get to implement some of my ideas one day."
    author: "JohnFlux"
  - subject: "something"
    date: 2005-04-23
    body: "something that would be great, tough not just in kdevelop, but also in the desktop as a whole, would be what this guy is talking about: projects. being able to save your environment, and re-open it later on. combine this with a certain 'context' for tenor... :D\n\nhttp://www.oreillynet.com/pub/wlg/6911"
    author: "superstoned"
  - subject: "something"
    date: 2005-04-23
    body: "something that would be great, tough not just in kdevelop, but also in the desktop as a whole, would be what this guy is talking about: projects. being able to save your environment, and re-open it later on. combine this with a certain 'context' for tenor... :D\n\nhttp://www.oreillynet.com/pub/wlg/6911"
    author: "superstoned"
  - subject: "one missing feature"
    date: 2005-04-23
    body: "There is one feature which i can only find in Emacs and i would love to see it in kdevelop. I don't know the exact name, but i call it (auto-indent by typing)\n\nFor example if i type:\n\nfor (int i=0; i < 10; i++)\n   std::cout << i;\n\nto indent the std::cout line i have to press 'tab'. On emacs i can type the code line by line and don't have to care about indention. Emacs indent and un-indent completely automatically.\n\nIf kdevelop could do this, i would be ready to switch from emacs to kdevelop.\n"
    author: "pinky"
  - subject: "Re: one missing feature"
    date: 2005-06-18
    body: "IIRC kdevelop 2.x at least used to do it, but it required you to use explicit braces. I fixed some of that code just at the end of 2.x's lifecycle. 3.x uses katepart and I didn't (yet) have time to tweak it, although the source seems to be much more tweakable than kdevelop 2.x's copy-n-paste of kedit (or whatever was it)."
    author: "Kuba Ober"
  - subject: "Things that drove me crazy in KDevelop"
    date: 2005-04-26
    body: "I for the life of me could not find anyway to add lib paths, include paths, and libraries to a project.\nI was trying to use it with FLTK and just could not find any way to add those or any docs that told me how to.\nI ended up using Anjuta because it was easy to setup to work with FLTK.\nI know not everyone uses FLTK but I need to and I could not get Kdevelop to do it.\n\n"
    author: "LWATCDR"
  - subject: "Re: Things that drove me crazy in KDevelop"
    date: 2005-05-17
    body: "Did you read the FAQ? probably not!\n"
    author: "Amilcar"
  - subject: "Re: Things that drove me crazy in KDevelop"
    date: 2005-05-17
    body: "Actually I probably did I searched through the help files, the online documentation, and even searched with google trying to figure it out. However even if I did over look it. It is simply easier to do in Ajunta. Even for an IDE ease of use is a good thing. If I wanted super powerful but hard to use I would use emacs.\n"
    author: "LWATCDR"
  - subject: "Re: Things that drove me crazy in KDevelop"
    date: 2005-05-17
    body: "Well, looks like you overlooked it.\nAjunta is a good IDE too, so if it makes you happy, use it.\nBut we won prizes in competitions where we competed against emacs, vi, Ajunta and Eclipse.\n"
    author: "Amilcar"
  - subject: "Re: Things that drove me crazy in KDevelop"
    date: 2005-05-17
    body: "I really wish I could use kdevelop. If you could point me to where in the documentation it is that would be great. BTW if you look at the FLTK forums you will find that I am not the only one that over looked it. I have writen code on the old Commodore 64, Amiga, MS-DOS, Windows, and Linux. And have used just about every development system known. kDevelop looks great for KDE, QT, and even GTK development. It just seems to be a pain to use with FLTK. Not all that many people use FLTK but I have to for an embedded Linux project I am working on. Please do not think of this as bashing KDevelop I find it very impressive but no program is perfect. I frankly wish I had the time to learn the source so I could help out. Maybe when this project is done I will. We just finished a great regular expression wizard for our windows product that would be nice to have an IDE. When I have time porting it to kde might be a good way to get my feet wet.\n"
    author: "LWATCDR"
---
<a href="http://www.newsforge.com/">NewForge</a> has a <a href="http://programming.newsforge.com/article.pl?sid=05/04/12/1357202">side by side comparison</a> of <a href="http://www.kdevelop.org/">KDevelop</a> and <a href="http://msdn.microsoft.com/vstudio/">Microsoft Visual Studio .Net</a>.  The article compares general application capabilities in the areas of UI, editing, compiling, frameworks, integration,  and documentation.  The result: <i>"KDevelop is more feature-rich than Microsoft's product... if Visual Studio existed as a native Linux application? I'd still rather use KDevelop."</i>

The KDevelop team has been doing some amazing work.  It's nice to start seeing this application get the recognition it deserves.  Great job guys!


<!--break-->
