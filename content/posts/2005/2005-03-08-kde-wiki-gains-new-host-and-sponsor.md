---
title: "KDE Wiki Gains New Host and Sponsor"
date:    2005-03-08
authors:
  - "luci"
slug:    kde-wiki-gains-new-host-and-sponsor
comments:
  - subject: "Congrats!!!"
    date: 2005-03-08
    body: "Wonderful wiki site... Thanks."
    author: "Fast_Rizwaan"
  - subject: "MediaWiki?"
    date: 2005-03-08
    body: "I'd love to see KDE wiki use MediaWiki. It's look and feel, and with this the whole *user-experience* is *a lot* better. The current one always looks ugly to me.\nA plain design, like wikipedia uses it, with the KDE logo on the top left would do.\n\nYes: I know - what counts is the *content*, but the appearance alone already scares off users, I fear :(\n\nI fear a switch from tikiwiki to mediawiki isn't that easy, so it probably will not happen - just my 2ct... :)"
    author: "unknown"
  - subject: "Re: MediaWiki?"
    date: 2005-03-08
    body: "The look could certainly be better.\n\nYou don't have to switch to MediaWiki to change the look though."
    author: "ca"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "well, you would need to in order to get the wiki-syntax people are used to. Granted, transfering would probably be more work then benefit.\n\namaroK has a relatively new mediawiki at http://amarok.kde.org/wiki"
    author: "Ian Monroe"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "The benefits could be greater actually.\nAside that mediawiki with mediabook.css looks very clean,\nwith media wiki, people would feel \"at home\" and would\nprobably contribute more than what we currently have.\nMany thanks to those who actually contributed (and yes, I did\nmake a small contribution), but the wiki is today rather poor.\n\nJust compare the \"Stay Informed about KDE Applications !\" \nparagraph on the main page http://wiki.kde.org/tiki-index.php\nwith the equivalent on wikipedia\nhttp://en.wikipedia.org/wiki/List_of_KDE_applications \nor an entry of that list with the great wiki for amarok.\n\nOh yes, and we could borrow information from wikipedia\n(or they from us)."
    author: "jmfayard"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "> with media wiki, people would feel \"at home\" and would probably contribute more than what we currently have.\n\nCan you please elaborate this thesis so that I have a chance to understand it?"
    author: "Anonymous"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "Not sure what the previous poster meant exactly, but for me the current KDE wiki (twiki?) looks much more confusing than the average mediawiki equivalent. Currently it looks like a combination of a forum / discussion board and a place to write about KDE. Additionally there are polls, number of online users and so on. In short: the current start page looks completely overloaded. \n\nAlso note that the syntax differs from the mediawiki syntax. Mediawiki, the software used for Wikipedia is probably by far the most known wiki syntax. This means that most persons who edit a wiki probably know the mediawiki syntax and it would make it a lot easier for them if they do not have to relearn a new syntax. Last time I checked I even had problems finding the \"watchlist\" equivalent of mediawiki. Furthermore I am not sure what support the current wiki engine has for multiple languages. The mediawiki engine is designed to support more than one language and allows interlanguage links.\n\nDespite the critics a big thanks to Luci for his work! "
    author: "MK"
  - subject: "Re: MediaWiki? -- a job for konq?"
    date: 2005-03-09
    body: "It would be nice to have an option in Konq to have a wsiwyg replacement for textarea. For example, we could specify that a textarea of a certain name, on a certain domain, is for editing media-wiki style, and have the wysiwyg editor accordingly. (have it working for the most popular ones: BBcode, MediaWiki, TWiki etc...)\n\nI believe the textarea is the most frustrating HTML widget, it would be nice to have option to make it more useable. (ie, the wisywig editor, kgpg integration for webmail, etc...)"
    author: "ac"
  - subject: "Re: MediaWiki? -- a job for konq?"
    date: 2005-03-11
    body: "Support in Konqi for the features required by http://tinymce.moxiecode.com/example.php?example=true would be very cool, too (not wiki-related, but still wysiwyg). I don't know the details why this doesn't work in Konqi at the moment, it currently only works with the usual suspects: Mozilla, MSIE and FireFox."
    author: "cm"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "Even easier would be to disable the \"double click to edit\" misfeature which is a usability nightmare."
    author: "charles samuels"
  - subject: "Re: MediaWiki?"
    date: 2006-10-28
    body: "My comparison of MediaWiki and Twiki, for use for a corporate knowledgebase.\n\nhttp://geeklog.blogspot.com/2006/10/wiki-comparison-mediawiki-and-twiki.html"
    author: "Geek Head"
  - subject: "Re: MediaWiki?"
    date: 2008-05-27
    body: "@GeekHead FYI your link is open to invited readers only"
    author: "anonymous"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "agreed.. there has been a mass exodus from tikiwiki to mediawiki simply because of the L&F of Mediawiki. However, it IS possible to pair down tikiwiki. See freedesktop.org for example. "
    author: "anon"
  - subject: "Re: MediaWiki?"
    date: 2005-03-09
    body: "freedesktop.org uses MoinMoin"
    author: "Anonymous"
  - subject: "RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "So I see the majority doesn't like the current state, and many are used to how  mediawiki works.\n\nQ: Can we switch?\n\n- Maybe have tikiwiki and mediawiki both parallel for a time and move the content step by step? Then, I'd even care to add contents then... :)"
    author: "unknown"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "If we are taking a vote, mine would be for switching to mediawiki.\n\nAlso, can I be assured that my content is \"safe\" on the new wiki machine?\nAre the drives RAID5? Are there tape backups? all that good stuff?\n\nI guess we won't be able to measure the reliability of the new machine\nuntil we use it for awhile.  The old setup was just too unreliable\nand frustratingly slow.  Here's hoping for a better KDE wiki!"
    author: "Allen"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "Yeah, the two services Dot and Wiki were really bogging the original machine down.  It's a relief to now have separate hosting solutions for the two.\n\nI think the backup situation is pretty good for the Wiki.  It's taken care of by the hosting company asmallorange, as far as I know.  Luci and Jason will know more.\n\nI wouldn't expect the Wiki to be switching to MediaWiki any time soon, if ever, if I were you guys.  Not having used TikiWiki myself, what's so bad about it?  The look is something that can be solved I assume, so that really shouldn't be a big deal."
    author: "Navindra Umanee"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "The problem is not the look IMHO. Twiki is a pain to edit. MediaWiki feels simpler, and most people are used to it (since it is more popular, and used on wikipedia.org)"
    author: "ac"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: ">Also, can I be assured that my content is \"safe\" on the new wiki machine?\n>Are the drives RAID5? Are there tape backups? all that good stuff?\n\nYes you can, I'm not even attempting to managing the hosting myself but let the experts at http://asmallorange.com take care of what they are good at. They have: \n\n# Dual Xeon servers with 2GB of RAM and SCSI hard drives (presumably with RAID5 as they don't skimp on anything else)\n# Daily and weekly off-site backups\n# Redhat Enterprise Linux installed for enterprise-class performance\n# 99.5% uptime guarantee\n\nAnd just to make you feel extra warm and fuzzy you can read about their drool inducing data center:\n\n\"A Small Orange hosts all our clients in a state of the art datacenter facility located in Dallas, Texas which offers complete redundancy in power, HVAC, fire suppression, network connectivity, and security. The datacenter facility sits atop multiple power grids driven by TXU electric, with PowerWare UPS battery backup power and dual power generators onsite. The HVAC systems are a combination of glycol, chilled water, and condenser units by Data Aire to provide redundancy in cooling coupled with 8 managed backbone providers. Twelve more third party backbone providers are available in the building via cross connect. Fire suppression includes a pre-action dry pipe system including VESDA (Very Early Smoke Detection Apparatus) with over 600 smoke detectors. Needless to say, with these sorts of facilities, you can rest assured that your website is in good hands.\""
    author: "Jaseone"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "Warm, Fuzzy.\nThanks for the info."
    author: "Allen"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "> Are the drives RAID5? Are there tape backups? all that good stuff?\n\nFunny that you are requesting that for the Wiki when it doesn't exist for more important KDE servers like cvs.kde.org afaik."
    author: "Anonymous"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "If we were discussing other KDE servers I'd voice the appropriate concern.\n\nIn the case of the CVS server I'm not as concerned because of the\ndistributed nature of the data, and because content I create/modify\nlives under my control and then is transfered upstream.  I hate\nit when I am give my content directly -- as in the case of a wiki -- and\nthen data goes poof! And all I get for my time and work is an apology."
    author: "Allen"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "> So I see the majority doesn't like the current state\n\nThe Wiki will for sure not change its software because of some random (did you contribute, will you ever?) and anonymous postings of MediaWiki fanboys to the dot."
    author: "Anonymous"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "What about a poll on the kde-wiki then? \n\nIt would be interesting what the majority of the existing editors says about a change. I have edited kde-wiki in the past, but stopped mostly because it looks too confusing for me and because I am not happy with the syntax--I sometimes edit wikipedia articles (from time to time even the KDE ones). IIRC Luci is a tikiwiki developer thus convincing him to switch would be kind of hard ;-)\n"
    author: "MK"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-09
    body: "> IIRC Luci is a tikiwiki developer thus convincing him to switch would be kind of hard ;-)\n\nSo you would prefer to fire him and find a new Wiki admin?"
    author: "Anonymous"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-10
    body: "No, I suggested allowing a vote on the current wiki to let the editors decide. I just pointed out that in case of a majority voting for mediawiki this would probably not please Luci for obvious reasons. This discussion is just about the wiki engine _not_ about persons. And to make this very clear (again): Luci is doing a great job -- thanks a lot!\n"
    author: "MK"
  - subject: "Re: RFC: switch to MediaWiki!"
    date: 2005-03-10
    body: "I can assure you that Luci and Navindra are doing a fabulous job to keep this stuff up and running for us all."
    author: "David"
  - subject: "Re: MediaWiki?"
    date: 2005-03-10
    body: "Well, my only complaint with the wiki engine behind the site is in the editing page. Syntax: not a problem. Final display: easy to change. The editor: uh-uh.\n\nThe editor has, along the top, smiley buttons. Smiley. Buttons. This isn't a forum. This isn't a chat client. The last thing we want in the KDE wiki are lots of animated faces making the entire product look like it was thrown together by twelve-year-olds.\n\nAnd the buttons for the other editing options? They're along the left edge. Hidden. And, once they are unhidden, they spill out in a bit of a jumble.\n\nSo, yeah, remove the smiley options and move the side buttons to the top, unhidden.\n\nHowever, and I want to emphasize this, those are just little nitpicks about an overall good site. Thanks for putting the site together and keeping it running."
    author: "jameth"
---
I'm pleased to announce that the <a href="http://wiki.kde.org/">KDE Wiki</a> has been moved to
a new hosting solution sponsored by our very own <a
href="http://www.jasonbainbridge.com/">Jason Bainbridge</a> of the
the KDE Web Team.  As you might have noticed, we had outgrown the previous server which had been hosting both the Dot and the Wiki.  After extended downtime
and performance issues often related to having both services on the
same machine as well as limited administration resources, <a
href="http://navindra.blogspot.com/">Navindra Umanee</a> and I decided to
search for an alternate host for the Wiki.

<!--break-->
<p>
Thanks to Jason, we now have a dedicated server for the Wiki which
ought to mean better uptime, better performance, and better
administration, for both the Wiki and the Dot services from now on...
or so we hope. ;-)
</p>
<p>
I am thrilled with the new arrangements, and would again like to thank
Jason and everyone involved with the move. Also many thanks to all
the people behind our previous hosting arrangements, including our
previous sponsor <a href="http://cafewiki.org/">CafeWiki</a> and Navindra for their kind support and helpfulness.
</p>







