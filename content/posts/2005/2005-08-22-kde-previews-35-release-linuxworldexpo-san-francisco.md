---
title: "KDE Previews 3.5 Release at LinuxWorldExpo San Francisco"
date:    2005-08-22
authors:
  - "csamuels"
slug:    kde-previews-35-release-linuxworldexpo-san-francisco
comments:
  - subject: "Seems Awesome"
    date: 2005-08-22
    body: "I can't wait for the full release. :)"
    author: "Alex"
  - subject: "Kolf!"
    date: 2005-08-22
    body: "I love that game!"
    author: "Segedunum"
  - subject: "Re: Kolf!"
    date: 2005-08-23
    body: "Jason, i understand it's your baby.... but kitten is your baby too.\n\nand its awesome, show it to the world\n\n"
    author: "Mathieu Jobin"
  - subject: "Re: Kolf!"
    date: 2005-08-29
    body: "indeed. I showed off kiten too! There were two people who asked about inputting Japanese on KDE, and I did my best to explain a plan of attack, including kiten :)\n\noh, and my title I'm trying to show on my badge is 'Kolfinator'. Which is really stupid, I'm sorry.\n\ncheers\nJason"
    author: "Jason Katz-Brown"
  - subject: "great writeup"
    date: 2005-08-22
    body: "awesome article covering the event njaard.. love that \"charles samuels\" touch of humour, wit and innuendo.\n\nthrowing themselves at you, indeed ;) best of luck with that!\n\nit is also great to see the various companies and projects helping out with things. muchly appreciated and needed."
    author: "Aaron J. Seigo"
  - subject: "Re: great writeup"
    date: 2005-08-22
    body: "la la la\nI am innocent!\n"
    author: "Blau Zahl"
  - subject: "Re: great writeup"
    date: 2005-08-25
    body: "ja, ja, jaja...\n\nI *bet* you are!\n\ncheers, Kurt"
    author: "pipitas"
  - subject: "Drag and drop"
    date: 2005-08-25
    body: "For the first time, I actually managed to make my Windows-loving coworker impressed: I tried dragging and dropping an image from Opera into Konsole, and Konsole let me copy the image with a single click. When I tried again, I got a popup telling me there already was such a file, and would I like to rename it?\n\nI love KDE!"
    author: "Runar"
  - subject: "4.0"
    date: 2005-08-27
    body: "so .. when is 4.0 released? i also couldnt find info about this"
    author: "ll"
  - subject: "Re: 4.0"
    date: 2005-08-27
    body: "when it's done..."
    author: "ac"
  - subject: "Re: 4.0"
    date: 2005-08-27
    body: "There was an idea about releasing it for KDE's 10th birthday in October 2006 (yes, 2006).\n\nOf course the exact release date will depend on how the development goes forward. It could be that the real release date will be (much) later.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "its time"
    date: 2005-08-28
    body: "its time for me to upgrade... still running kde 3.1"
    author: "yannick"
  - subject: "KDE really makes it work!"
    date: 2005-08-30
    body: "the KDE booth did get quite busy, and rightfully so. i am constantly trying to convince people that K makes Ubuntu Kubuntu! currently completely enamored with Desktp BSD, though. KDE makes BSD kool! keep up the good werk\n\nregards from the linuxprinting.org booth\n\nuli"
    author: "mandraks"
---
The K Desktop Environment was pleased to see its users recently at the <a href="http://www.linuxworldexpo.com/">LinuxWorld Conference and Expo</a>.  Users were excited to learn about KDE 4.0, some of the hot new stuff in KDE 3.5, and thousands of Linux users got out of the house for a change!
















<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex">
<a href="http://ktown.kde.org/~charles/lwce2005/p8090002.jpg">
<img width="180" height="135" src="http://static.kdenews.org/danimo/lwesfbooth.jpg" border="0" align="center"/><br />KDE booth: ready to serve</a></div>
<p>
Guests to our booth had questions like <i>"So, when is KDE 4.0 going to be released?"</i> Some also wondered what kind of <a href="http://www.canllaith.org/svn-features/kde4.html">new features</a> it would have.
</p><p>
Some users were more concerned with the immediate future, KDE 3.5.  They wondered <a href="http://jrepin.blogspot.com/2005/07/jlps-kde-35-previews-part-1.html">what it might have</a>, and when it will be released (scheduled for the end of October).
</p><p>

As always, the report wouldn't be complete without <a href="http://ktown.kde.org/~charles/lwce2005/">pretty pictures</a>, which include, among others, Jason showing off his baby, Kolf, over, and over, and over again.  Or the great big KDE sign at the Mepis booth.

</p><p>
This year, we'll love to thank the angels at <a href="http://www.xandros.com/">Xandros</a> who went more than out of their way to help KDE have an exciting presence there, providing computers and Xandros Linux boxed sets for our guests.  We had some further equipment provided by <a href="http://www.novell.com">Novell</a>.  The kind people at <a href="http://www.mepis.com/">Mepis</a> also donated some Live CDs.  However, first and foremost, we want to thank all our users who donated various sums - these contributions go a long way in allowing us to have a presence at San Francisco (and elsewhere in the US) year after year.
</p><p>

I'd also like to offer my thanks to the LinuxPrinting booth who printed hundreds of our <a href="http://ktown.kde.org/~charles/kdeagain-1.5.pdf">brochures</a>; to the <a href="http://www.debian.org">Debian</a> booth which kept us company, made excellent neighbors, and provided technical advice; the <a href="http://www.ltsp.org">Linux Terminal Server Project</a> lent us a pretty looking thin terminal running <a href="http://www.kubuntu.org/">Kubuntu</a>; and of course, to those at the Gnome booth, for practically throwing themselves at us :-).
</p>



