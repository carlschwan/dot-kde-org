---
title: "LinuxTag 2005: KDE Applications for Firefox"
date:    2005-06-25
authors:
  - "dmolkentin"
slug:    linuxtag-2005-kde-applications-firefox
comments:
  - subject: "Disruptive technology :)"
    date: 2005-06-25
    body: "Awesome. I've been waiting for this for quite a while."
    author: "Eike Hein"
  - subject: "nice"
    date: 2005-06-25
    body: "I don't like Firefox very much, I prefer Konqueror. But IMO this is great to show how flexible KDE is and how is it could be for non-KDE applications to interact with KDE."
    author: "Patrick Trettenbrein"
  - subject: "Re: nice"
    date: 2005-06-25
    body: "*how easy it could be for non-KDE applications to interact with KDE."
    author: "Patrick Trettenbrein"
  - subject: "XParts?"
    date: 2005-06-25
    body: "Is this using XPart technology?"
    author: "KDE User"
  - subject: "Reminds me of Reaktivate :)"
    date: 2005-06-25
    body: "http://www.konqueror.org/announcements/reaktivate.php\n\nJust this time the other way around"
    author: "Kevin Krammer"
  - subject: "Re: Reminds me of Reaktivate :)"
    date: 2005-06-25
    body: "In other words, yet another cool but dead KDE technology.  :-)\n\nOn the bright side, KDE invents so many cool technologies that eventually one of them is a wild success.  KDE has been flirting with Mozilla/Firefox for ages but nothing has ever stuck yet."
    author: "KDE User"
  - subject: "Re: Reminds me of Reaktivate :)"
    date: 2005-06-25
    body: "Well, the situation is different. Who with a sane mind would want ActiveX on an otherwise secure system? :)"
    author: "Kevin Krammer"
  - subject: "Re: Reminds me of Reaktivate :)"
    date: 2005-06-26
    body: "> In other words, yet another cool but dead KDE technology. :-)\n\nYeah but... who really needs ActiveX support, anyways? I understand the need for Flash and Java, but AFAIK ActiveX isn't a very popular browser feature (or maybe I've been visiting the wrong (right? :)) sites)."
    author: "Another user"
  - subject: "Re: Reminds me of Reaktivate :)"
    date: 2005-10-13
    body: "Shockwave?\n\nPlease help me with reaktivate. I need it very badly.\nThanks"
    author: "Forrest"
  - subject: "Good for All..."
    date: 2005-06-25
    body: "I also prefer konqueror for its unicode support and filemanager/webbrowser/network_browser switchability.\n\nThis is commendable endeavor by our KOOL KDE Developers to even help non-KDE application and firefox is one such adorable application which needs KDE's helping hand :) Good Luck, Have fun!!!"
    author: "fast_rizwaan"
  - subject: "Kaffeine and real streams :("
    date: 2005-06-25
    body: "kaffeine always crashes konqueror when trying to play a real stream. And there seems no development either :(\n\nkaffeine doesn't support KDE 3.3 or 3.4, then what's the use of it! To crash konqueror!?\n\neven the kaffeine-cvs07 version crashes konqueror! Has anybody got kaffeine working nicely on KDE 3.4.x?\n\nI'm unhappy with kaffine cause it is the only player which could play everhting thrown at it! but now real streams won't work even worse it crashes konqueror!"
    author: "fast_rizwaan"
  - subject: "Re: Kaffeine and real streams :("
    date: 2005-06-25
    body: "Kaffeine works fine :-), it will only crash when you close it but that was in Suse 9.1, 9.2, and even in 9.3. Instead of Kaffeine you can use MPlayer. www.mplayer.hq."
    author: "Theodorus Coenen"
  - subject: "Re: Kaffeine and real streams :("
    date: 2005-06-25
    body: "mplayer works very well using KMPlayer as the Konqueror plugin.\n\nKMPlayer is one of the few things I install from source for exactly that purpose.\n"
    author: "Kevin Krammer"
  - subject: "Re: Kaffeine and real streams :("
    date: 2005-06-25
    body: "You should try kmplayer !"
    author: "arjanw"
  - subject: "Now if we could getgecko into konqueror..."
    date: 2005-06-25
    body: "I like Konqueror interfac more than Firefox, as I do like KDE more than gnome/gtk, but konqueror engine is still a child in front of gecko.\nSo if the gecko into konqueror makes on kde 3.5 with kpaers embeded on it...WOW, it will make me happy ;)\n"
    author: "Iuri Fiedoruk"
  - subject: "Re: Now if we could getgecko into konqueror..."
    date: 2005-06-25
    body: "> So if the gecko into konqueror makes on kde 3.5 with kpaers embeded on it.\n\nLikely not for KDE 3.5."
    author: "Anonymous"
  - subject: "Re: Now if we could getgecko into konqueror..."
    date: 2005-06-26
    body: "A Gecko kpart will never be part of KDE since that would currently introduce a dependency on a complete Mozilla/Firefox installation (for that matter the Mozilla Foundation really needs to get their act together finally and release an unified Gecko runtime environment for all their products)."
    author: "ac"
  - subject: "Re: Now if we could getgecko into konqueror..."
    date: 2005-06-26
    body: "Well, correct me if I'm wrong, but can't you compile KDE with or without it (by configure script detecting it), and then, the binaries simply detect if Mozilla libraries are present on the system or not, if it was compile with support to it?\n\nSomething like when you run a program that uses vbrun300.dll, but just fo0r some minor part, so it can run without it also?"
    author: "Iuri Fiedoruk"
  - subject: "my trouble with konqueror..."
    date: 2005-06-26
    body: "...is that the tool-bars always \"refuse\" to stay where I want them. I always change its tool-bar to look like that of Firefox by moving items from one toolbar to another and merging those that I have to....this works for very few times before everything begins to be placed anywhere. This wastes valuable screen estate and makes Konqueror even ugly! It has been the case in all distros I have used. How does one solve this?\n\nTo make matters worse, attempting to re-arrange them results in nothing at all! Re-merging will not work at this point."
    author: "cb"
  - subject: "Re: my trouble with konqueror..."
    date: 2005-06-26
    body: "Be sure that you save the result as the profile you are usually using."
    author: "ac"
  - subject: "Re: my trouble with konqueror..."
    date: 2005-06-27
    body: "no, what he says is correct. I've always experienced the exactly same problem my distro since I remember.\nIt's a headache!\nTool bar management in Konqueror is due to a revision, I think.\n\nAnother thing I'd like changed (or better, implemented) is an option to independently configure the double click behaviour on File Types when in the File Management and Web Browser modes of Konqueror. This is another cause of frustration for my daily usage.\n"
    author: "ggabriel"
  - subject: "Re: my trouble with konqueror..."
    date: 2005-06-27
    body: "I have always saved my sessions - ALWAYS. What frustrates me most is the fact that  attempting to redo the whole exercise again will NOT work once the toolbars \"forget\" their settings. But I must add that sometimes, Konqueror will remember these setings magically. Filing a bug report is not very useful because reproducing the behavior is almost impossible."
    author: "charles"
  - subject: "Re: my trouble with konqueror..."
    date: 2005-12-21
    body: "My trouble with konqui is that some images always fail to load on a page ... or the stylesheet does not seem to have applied. I am on a fairly snappy broadband so I wonder what the issue is? Even kde-look.org does not load without some images 'broken' (which in reality exist)!\n\nOh btw, I am using KDE 3.5 on Kubuntu although I have seen the same behaviour on PCLinuxOS, Fedora etc."
    author: "Kanwar"
  - subject: "KDE-Wallet-Integration in Firefox"
    date: 2005-06-25
    body: "This is feature I would really like to see with Firefox. KDE-Wallet is more advanced then the integrated password-mananger. Also it would be helpful, since I am using both browsers, so I would only have to type in the password once."
    author: "Mark"
  - subject: "Re: KDE-Wallet-Integration in Firefox"
    date: 2005-06-25
    body: "You want a big huge annoying dialog that you have to cancel every time you go to website with forms?  :-)"
    author: "KDE User"
  - subject: "Re: KDE-Wallet-Integration in Firefox"
    date: 2005-06-25
    body: "No, he wants a centralized place to store password on his machine so that he doesn't have to remember which of several different passwords he used on each website. And he wants to be able to use that whether he's in Firefox or Konqueror.\n\nFirefox already has dialogs that ask if you want them to remember your password all over the place, but it can't share that information with KDE apps."
    author: "Dolio"
  - subject: "Re: KDE-Wallet-Integration in Firefox"
    date: 2005-06-26
    body: "Looks like a respective plugin should be written for Firefox. That's certainly not KDE's job though."
    author: "ac"
  - subject: "Re: KDE-Wallet-Integration in Firefox"
    date: 2007-03-09
    body: "Well, normally I would say yes.   But when an app becomes as important and popular as firefox, sometimes it switches who has to do the work to make things work together.   Or both sides can say it's the other guy's job and I can be posting this note 20 months later, of course.\n\nSimply having KDE wallet be able to fill in Firefox's master password would be enough, since right now FF asks for that every time you run it, and it does sometimes crash, so that can be several times a day.   Truth is the firefox password manager does far more password management for me than KDE wallet does, so if KDE wallet wants more adoption, it could very well fall to the platform to do the work rather than the app.  So it goes."
    author: "Brad Templeton"
  - subject: "Re: KDE-Wallet-Integration in Firefox"
    date: 2007-03-15
    body: "I really like the kde wallet for its security, portability, and ease of use and I would like to see a Firefox plugin to support it.  I'm surprised no motivated individual has done this yet.  I'm guessing the KDE Wallet has a nice simple API to access it and request logins and passwords from it when needed.\n\nWouldn't it involve not too much more than substituting the KDE Wallet for the back end of Firefox's login/password storage system?\n\nSounds like an interesting project to me."
    author: "Justin"
  - subject: "Re: KDE-Wallet-Integration in Firefox"
    date: 2008-04-19
    body: "No that would not do it, since Firefox does not give a damn about the kde wallet manager. So it would not make any difference if the kde wallet could enter FF's master password, because the wallet is not being asked when you browse to a password site using FF. Also, these people who are asking for a wallet integration into FF want to have a centralized place to store their password, which I personally completely agree with. Your suggestion does not do that. And btw you wouldn't say that the centralized place to store passwords in a K desktop should be the FF password manager, would you?? It is NOT KDE's job to adapt.\n\nIt seems like people are too caught up in this whole 'whose job is it?!?' question. What I think is: there is NO ONE you could say 'that is their job' to concerning this matter. A centralized place for passwords would be very nice indeed, but since the KDE development does not seem to be aware of this, perhaps some other hobby programmer would have to do it. I presume this would be possible with yet another FF plugin similar to those other ones floating in dozens around in the net.\nI really would love to try it myself, but unfortunately my programming abilities are very limited..."
    author: "guest"
  - subject: "KDE filebrowsing for Firefox still missing"
    date: 2005-06-26
    body: "This doesn't solve the most annoying problem, namely that the file selector in Firefox is still not integrated into KDE and Firefox can't do things like:\nBrowse Floppies\nBrowse Network files with fish:/, smb:/, etc\nBrowse Storage Media\n\nSo I can't \"Save As\" a page to a location outside of the root file system (like a currently unmounted Floppy, Network Location, or Storage Media).\n\nNot to say that the KDE Open File dialog is perfect, but it's better than what GNOME has to offer. One thing that the KDE file dialog still lacks is a \"Rename\" option when a person right clicks a file (although it is possible to rename the file by choosing \"Properties\"). Not very user-friendly.\n\nSimilarly, you still can't rename a folder from Konqueror's tree view, even though the \"Rename\" option appears when right-clicking. (see http://bugs.kde.org/show_bug.cgi?id=80584)\n\nIf there's one thing I love about Windows is that all applications' File-Open dialogs are the same and can do everything that File Explorer can. And File Explorer shows *everything* (Network, CD, Floppy, Local Folders) as nodes in a well thought-out Tree View."
    author: "Vlad C."
  - subject: "Re: KDE filebrowsing for Firefox still missing"
    date: 2005-06-26
    body: "Partly agree. Windows open/save dialog is cool because you can even\ncopy, paste, etc in that dialog. But I like KDE's dialog because\nof the left-hand customizable icon bar and because it saves its size\nso you can make it nearly full screen. I dont want to see other content\nanyway when I'm about to select a file. Never understood why that dialog\nuses to be so small on Windows. And, of course, you can seemlessly\nbrowse/edit/etc FTP, SSH, etc. files. So, right now, that dialog nearly\ndoes everything I want. But I agree, renaming via properties is really\na bit complicated.\n"
    author: "Martin"
  - subject: "Re: KDE filebrowsing for Firefox still missing"
    date: 2005-06-27
    body: "Just press F2 to rename"
    author: "someone"
  - subject: "Re: KDE filebrowsing for Firefox still missing"
    date: 2005-09-18
    body: "You can change the list of icons in a Windows file dialog with TweakUI."
    author: "Rich"
  - subject: "Re: KDE filebrowsing for Firefox still missing"
    date: 2005-06-26
    body: "I hope you reported that to Mozilla's Bugzilla since it's their job to introduce something like that (unless you want to propose a KDE fork of Firefox which would be ridiculous)."
    author: "ac"
  - subject: "Re: KDE filebrowsing for Firefox still missing"
    date: 2005-06-27
    body: "I took your advise and filed a bug with Firefox:\nhttps://bugzilla.mozilla.org/show_bug.cgi?id=298848"
    author: "Vlad C."
  - subject: "Re: KDE filebrowsing for Firefox still missing"
    date: 2006-06-26
    body: "https://bugzilla.mozilla.org/show_bug.cgi?id=298848\n\nis \"resolved\" the resultion is \"WONTFIX.\" Great? "
    author: "anonymous"
  - subject: "How about the JS support of kparts"
    date: 2005-06-26
    body: "embeded is not too difficult, if you give kparts NS api support.\n\nbut how about the javascript support?\n\nas we know, kparts use LiveConnect extension to support Javascript Interactive in Konqueror.\n\nso we can control the plugin in HTML and javascript code.\n(kaffeine did not supply js support, kmplayer does&#65292;I had made a JS support for Kaffeine, it can support most of the JS functions as same as Windows Mediaplayer And Realplayer.)\n\nbut how about this implementation? \n\nfor more media plugin interface, pls view the Window Media player and RealPlayer plugin JS interface."
    author: "cjacker"
  - subject: "Re: How about the JS support of kparts"
    date: 2005-11-26
    body: "You say you've added js support to kaffeine, did you send that back upstream to the kaffeine maintainer? Reason I ask is this is vitally important to get certain sites working right (www.musicindiaonline.com and www.yahoo.com's new media streaming stuff are ones that I can think of atm as that is the number  one bug that my company gets the most complaints about....)"
    author: "Gary Greene"
  - subject: "Related"
    date: 2005-06-26
    body: "How far is this related to last years work for Qt-Mozilla?"
    author: "ac"
  - subject: "Re: Related"
    date: 2005-06-26
    body: "I think not related."
    author: "Anonymous"
  - subject: "59225"
    date: 2005-06-26
    body: "Tobias, bug 59225 is now yours. ;-)"
    author: "George Staikos"
  - subject: "integration...."
    date: 2005-06-26
    body: "this is something that firefox(and thunderbird) lacks...even within gnome envrionment.\nMaybe they should start to support at least DBUS to connect other apps(like address books, or even other mozilla apps) without dirty hacks.\nThe file chooser dialog represents a major problem, because they need to support both KioSlaves and GnomeVFS, maybe if there will be a common solution mozilla will start to support it\nJust my 2\u00a2\n"
    author: "ra1n"
  - subject: "Re: integration...."
    date: 2005-06-26
    body: "Agreed, but sadly it looks like the Mozilla Foundation mostly only cares about Windows as the lowest common denominator."
    author: "ac"
  - subject: "Nice, but almost useless"
    date: 2005-06-27
    body: "Sorry, but I get most of the webs \"active\" content diplayable under linux just fine in firefox using the well known available plugins plus mplayer plugin.\n\nOn the other hand the nspluginviewer (sometimes plus konqueror) crashes all the time for me. Of course this is the fault of bad web authors, bad plugins and bad packagers of my distro - but it makes KDE just look bad, as firefox works well with the same content.\n\nJust my 0.02 \u0080\n"
    author: "bela"
  - subject: "embed khtml?"
    date: 2005-06-27
    body: "can I embed konqueror, i.e. khtml?  I imagine a association of al .html files with it.. that would be great.   after decko is intergated in konq, one could finally embed it in itself...  "
    author: "silversun"
  - subject: "Re: embed khtml?"
    date: 2005-06-29
    body: "I wonder how hard it is to tell mozilla to use a plugin to view HTML?  Seems like that'd be an unusual thing to do.\n\nThough it would not be hard to put up a page somewhere with a fake extension and mime type (.khtml or something) to test the concept."
    author: "anonymous"
  - subject: "*NOT* a new Idea!"
    date: 2005-06-28
    body: "This has existed for GNOME since May 2003:\n\nhttp://www.nongnu.org/moz-bonobo\n\nSo it's not such a new idea at all."
    author: "Rufus"
  - subject: "Re: *NOT* a new Idea!"
    date: 2005-07-04
    body: "who said anything about a new idea?"
    author: "rinse"
  - subject: "KPDF needs to be improved"
    date: 2005-10-03
    body: "Hello,\n     I am a Linux (Fedora core 2) user - more specifically, I use KDE as my desktop. First let me congratulate you on bringing this long missed feature of integrating a pdf viewer with web browser which is standard in the windows counterpart. \n\nNow coming to the issue of what is lacking in KPDF. I have found that Gpdf is more usable when viewing PDF documents (though it has crashed plenty of times) because of the following:\n\n1) Kpdf does not recognise links (web links or internal links). Gpdf does.\n2) Font rendering is not up to the mark. The documents opened in kpdf looks crude. Much better (antialiased?) font support is there for Gpdf. \n\n... this may be because I am using a version of kpdf which was bundled with fedora  core 2 (older version). \n\nThough gpdf also has its drawbacks. I have found that some pdf documents created using openoffice.org, which when opened in gpdf  are garbled, but opens fine in kpdf. But most of the time gpdf works. \n\nI am sure these wrinkles will be sorted out in kpdf by the time KDE 4 is released.\n\nRegards\nRavi\nMy Blog - http://linuxhelp.blogspot.com [All about Linux]"
    author: "Ravi"
  - subject: "Re: KPDF needs to be improved"
    date: 2005-10-03
    body: "> ... this may be because I am using a version of kpdf which was bundled with fedora core 2 (older version). \n \nIndeed. \n\nIIUC Fedora Core 2 came with KDE 3.2.  In the meantime KPDF has been greatly improved so I strongly recommend upgrading. \n\nNote:  KDE version 3.5 will be out soon, so you may want to wait a little:  \nhttp://developer.kde.org/development-versions/kde-3.5-release-plan.html\n\n"
    author: "cm"
---
Today the KDE project previewed a new technology that allows arbitrary <a href="http://developer.kde.org/documentation/tutorials/kparts/">KPart-based</a> KDE applications to be embedded into <a href="http://www.mozilla.org/">Mozilla</a>'s <a href="http://www.mozilla.org/products/firefox/">FireFox</a>. Thus, KDE applications can soon be used to show rich content such as PDF files using <a href="http://kpdf.kde.org/">KPDF</a> or display multimedia files with <a href="http://www.xs4all.nl/~jjvrieze/kmplayer.html">KMPlayer</a> or <a href="http://kaffeine.sourceforge.net/">Kaffeine</a>.








<!--break-->
<p><div style="float: right; border: thin solid grey; padding: 1ex"><a href="http://static.kdenews.org/danimo/firefox_and_kpdf.jpg"><img width="250" height="144" src="http://static.kdenews.org/danimo/k_firefox_and_kpdf.jpg" border="0" /></a><br />KPDF inside Firefox</div>
<i>"Once again, KParts has proven to be a flexible component technology that makes development easy even when dealing with non-KDE applications"</i>, said Tobias K&ouml;nig, author of the KPart-Firefox integration.
</p><p>
 As the code was created on the KDE booth at LinuxTag, it is not yet in the <a href="http://websvn.kde.org/">KDE repository</a>. However K&ouml;nig, best known as the author of <a href="http://pim.kde.org">KAddressbook</a>, is planning to check in the code soon.</p>







