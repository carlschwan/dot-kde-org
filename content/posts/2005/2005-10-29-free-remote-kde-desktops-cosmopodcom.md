---
title: "Free Remote KDE Desktops from CosmoPOD.com"
date:    2005-10-29
authors:
  - "jriddell"
slug:    free-remote-kde-desktops-cosmopodcom
comments:
  - subject: "Wow."
    date: 2005-10-29
    body: "what else is there to say?"
    author: "Matthias Welwarsky"
  - subject: "Here's what you can say."
    date: 2005-10-29
    body: "Well, it should be noted that logging onto the server is impossible at present! The message is:\n\n\"Authentication failed for user <user_name>.\" This is after following the instructions!\n\nGot it?"
    author: "cb"
  - subject: "Re: Here's what you can say."
    date: 2005-10-29
    body: "well, _I_ can't say. Setting up the session works like described after downloading the nxclient package for suse 9.3. Authentication works with the data I provided when creating the account.\n"
    author: "Matthias Welwarsky"
  - subject: "Nice Success Story for KDE and... NoMachine"
    date: 2005-10-29
    body: "This is indeed really good news to hear! It shows how well thought out and implemented the KDE architecture is.\n\nI found it astonishing to learn from Stephen's answers that there are mainly \"non-techie\" users so far which have subscribed to this service.\n\nHowever, one thing should be corrected: the interview's introductionary sentences link to the FreeNX website. But in fact, CosmoPOD seems to use not FreeNX, but an *Evaluation* license of the commercial NoMachine NX server.\n\n(Even if they used FreeNX, they'd still be using NX code that is 99% from the NoMachine labs. FreeNX is \"only\" a very thin layer of code gluing together the GPL'd parts of the NX Core libs and utilities which were created by NoMachine).\n\nCheers,\nKurt [FreeNX Team]"
    author: "Kurt Pfeifle"
  - subject: "Connection to X server cannot be established"
    date: 2005-10-29
    body: "I registered, downloaded the applet, Authenticated but i get this funny error \"Connection to X server cannot be established.\"\n\nWhat could be the problem?"
    author: "Fast_Rizwaan"
  - subject: "Re: Connection to X server cannot be established"
    date: 2005-10-29
    body: "Display Type             Session ID                       Options  Depth Screen         Status      Session Name\n------- ---------------- -------------------------------- -------- ----- -------------- ----------- ------------------------------\n\nNX> 147 Server capacity: reached for user: maarizwan\nNX> 105 Start session with: --session=\"session\" --type=\"unix-kde\" --cache=\"8M\" --images=\"32M\" --link=\"adsl\" --kbtype=\"pc101\\057us\" --nodelay=\"1\" --encryption=\"1\" --backingstore=\"never\" --geometry=\"1024x722\" --media=\"0\" --agent_server=\"\" --agent_user=\"\" --agent_password=\"\" --agent_domain=\"\" --screeninfo=\"1024x722x16+render\" \nNX> 280 Ignoring EOF on the monitored channel\nNX> 280 Ignoring CLOSE on the monitored channel\nNX> 599 Reached the maximum number of concurrent sessions on this server\nNX> 500 ERROR: Last operation failed.\nKilled by signal 15."
    author: "Fast_Rizwaan"
  - subject: "Re: Connection to X server cannot be established"
    date: 2005-10-29
    body: "> NX> 599 Reached the maximum number of concurrent sessions on this server\n\nWell, there you have it.  I guess that was to be expected.\n\n"
    author: "cm"
  - subject: "Great news, but the server is going to be full"
    date: 2005-10-29
    body: "Now that the news are published, the server will be full forever. But this is a first glimpse of the future. Why do you need web services, if you can have desktop services ?\n\nThanks for the server."
    author: "Henrique Marks"
  - subject: "Re: Great news, but the server is going to be full"
    date: 2005-10-30
    body: "A lot of competitors will pop up, now that the cat is out of the bag.\n"
    author: "reihal"
  - subject: "Optional paid service ?"
    date: 2005-10-29
    body: "I wonder if they have in mind giving an optional paid registration with no ads, and where you can buy extra space/bandwidth as needed ... it would be good I would think ! "
    author: "MandrakeUser"
  - subject: "KDE-Dotted!"
    date: 2005-10-30
    body: "When I clicked on the cosmopod.com link, I got a message saying the service was unavailable.  I suspect the added traffic has clogged up their server - it's been KDE-Dotted!  Give it time.  (I'll be blogging it myself.)"
    author: "Matt Smith"
  - subject: "Well, the marketing drive worked"
    date: 2005-10-30
    body: "I hope that the guy who made this thing work (or not for most of us who cannot connect) will expand his service to allow more people to use it. I cannot wait to try it out."
    author: "Ryan"
  - subject: "login"
    date: 2005-10-30
    body: "Does anybody had an successful login?\n\nno name"
    author: "no name"
  - subject: "anybody successfully logged in ?"
    date: 2005-10-30
    body: "just a question. I did not have luck.\n\nno name"
    author: "no name"
  - subject: "Re: anybody successfully logged in ?"
    date: 2005-10-30
    body: "Yes, I did. (And check just now -- I still can)."
    author: "Kurt Pfeifle"
  - subject: "testdrive"
    date: 2005-10-30
    body: "if all you want to do is try the technology, nomachine run a test facility http://www.nomachine.com/testdrive.php where you can use a kde desktop remotely (on a server in Italy) for a week to try things out.\n\nIMHO cosmopod needs to change its setup. At the moment there is no incentive for anyone, once logged on, to log off again. ISTM there'll have to be charges on an hourly basis or whatever.\n\nMakes you think though, dunnit? Do I actually need a local desktop, or can I run most/everything on a central server accessible from anywhere with an internet connection and an nxclient?"
    author: "Peter Robins"
  - subject: "From CosmoPOD"
    date: 2005-10-30
    body: "Thanks for the nice comments, regarding the logon issues I am working on them I am just having huge amounts of people registering at the moment."
    author: "Stephen Ensor"
  - subject: "Re: From CosmoPOD"
    date: 2005-10-30
    body: "\nDoes anybody know or assume why this connection fails?\n\n\"NX> 148 Server capacity: not reached for user: kdeuser\nNX> 105 Start session with: --session=\"cosmopod.com\" --type=\"unix-kde\" --cache=\"8M\" --images=\"32M\" --link=\"adsl\" --kbtype=\"pc105\\057de\" --nodelay=\"1\" --encryption=\"1\" --backingstore=\"never\" --geometry=\"1280x977\" --media=\"0\" --agent_server=\"\" --agent_user=\"\" --agent_password=\"\" --agent_domain=\"\" --screeninfo=\"1280x977x24+render\" \nKilled by signal 15.\"\n\nThanks.\n\nno name\n\n"
    author: "no name"
  - subject: "Re: From CosmoPOD"
    date: 2005-10-30
    body: "I see the same message. I thnk they just got too many people looking at this service. \n\nMakes you wonder though: buy one big machine, give your parents, friends, uncles and autns an account, and then never ever have to come by to fix up spyware, viruses, etc. Never have to be bothered to tell them to not click away the virusscanner update dialog. "
    author: "Ber"
  - subject: "Re: From CosmoPOD"
    date: 2005-10-31
    body: "Actually, I have already done exactly that using SuSE 10.0 on an AMD 64 box.  Using freeNX, I set up old boxes in each of the kids rooms and put up a wireless network, now everyone has the equivalent of a new computer.  NX is fast enough that there is very little if any delay for normal computing over our wireless network, although for games, the kids generally log directly into the main server.  I also set up an account for my mom, who lives in another state, and for people at work who are interested in learning about Linux, but aren't quite ready to install yet. FreeNX is the greatest software since the web browser!\n\nCheers "
    author: "Tim"
  - subject: "Re: From CosmoPOD"
    date: 2005-11-01
    body: "### \"FreeNX is the greatest software since the web browser!\"\n----\n\nI agree -- to a large degree.\n\nBut it is so, because the foundation of FreeNX, the NoMachine NX Core libraries and utilities, were released under the GPL, and not kept proprietary by their creators.\n\nOn our own, the FreeNX development team could never, ever -- I repeat: never, ever! -- have created a thing like FreeNX is now.\n\nCheers,\nKurt  [FreeNX development team member]"
    author: "Kurt Pfeifle"
  - subject: "Good Inital Idea"
    date: 2005-10-30
    body: "This will be a great service for some users.  However, the problem with this service is the access method.  I won't be able to install the NX client on all systems such as airport terminals.  But all of these devices have one thing in common - they have a web browser.\nI think access to a desktop from the browser w/o having to install custom software is what would really provide for a pervasive platorm.\n"
    author: "noname"
  - subject: "Re: Good Inital Idea"
    date: 2005-10-30
    body: "NX has a java applet client, so no need to install any software (if you can install the software you should)."
    author: "Corbin"
  - subject: "Re: Good Inital Idea"
    date: 2005-10-31
    body: "also, this should of cours be available by default in airports etc :D"
    author: "superstoned"
  - subject: "auth filed"
    date: 2005-10-30
    body: "followed instructions,\n\nauthentification failed when trying to connect to cosmopod.com on port 443 for the unix kde ...\n\n\n:| aww man"
    author: "dirm"
  - subject: "NX on usb stick"
    date: 2005-10-30
    body: "Didn't try yet, but Nomachine claims the nxclient can be installed on a usbstick and can as such be used on different computers.\n\nGreat software, I use it to surf safe at home : run an encrypted session from wireless laptop on home desktop."
    author: "GOAN"
  - subject: "Connection timed out"
    date: 2005-10-30
    body: "I'm trying to connect from the office.  Is this the reason why I can't connect?  I will try to connect from home with my DSL connection.\n\nDisplay Type             Session ID                       Options  Depth Screen         Status      Session Name\n------- ---------------- -------------------------------- -------- ----- -------------- ----------- ------------------------------\n\nNX> 148 Server capacity: not reached for user: bakalao2k\nNX> 105 Start session with: --session=\"cosmopod.com\" --type=\"unix-kde\" --cache=\"8M\" --images=\"32M\" --link=\"lan\" --kbtype=\"pc102\\057en_US\" --nodelay=\"1\" --encryption=\"1\" --backingstore=\"never\" --geometry=\"800x600\" --media=\"0\" --agent_server=\"\" --agent_user=\"\" --agent_password=\"\" --agent_domain=\"\" --screeninfo=\"800x600x16+render\" \n\n"
    author: "bakalao2k"
  - subject: "I have something better"
    date: 2005-10-31
    body: "I provide a full free e-mail service with 100 Gb mail box, remote KDE desktop with all applications needed with 250 Gb remote home directory etc. over NX.\n\nThere is a little problem: nobody can connect to my server and never will be able to do so.\n\nBut, it is a good idea, isn't it?\n\nIdiots who advertise for a service or a feature they can't provide. And I am a idiot who downloaded a thing and losted two hours of my precious time trying to connect.\n\n\n\n"
    author: "PR"
  - subject: "Re: I have something better"
    date: 2005-10-31
    body: "\"Idiots who advertise for a service or a feature they can't provide. And I am a idiot who downloaded a thing and losted two hours of my precious time trying to connect.\"\n\nDude, take a chill-pill. I really fail to see what grounds you have for your whining, considering that the service is free. They are merely overwhelmed by the demand at the moment, and that is why you can't connect. So you \"losted\" [sic] two \"precious\" hours trying to connect. Well boo-hoo! I have \"losted\" hours of time downloading/compiling/configuring KDE, where can I get a refund because I have \"losted\" so much time?\n\nAre these people \"idiots\" because they apparently offer a kic-ass service for free, and they are overwhelmed by the demand? Would thing be better if they did NOT offer this service?"
    author: "Janne"
  - subject: "Re: I have something better"
    date: 2005-10-31
    body: "I feel the same Janne, I also \"losted\" too much time using free software put out there by idiots who like to share there time and knowledge, how idiots they are ;-)\n\nTo the original poster, just grow up and learn some decency.\n\nTo the providers of this free service, THANK YOU."
    author: "MandrakeUser"
  - subject: "Re: I have something better"
    date: 2005-11-01
    body: "'I really fail to see what grounds you have for your whining, considering that the service is free.'\n\nWhining? Service? Free? There is no service at all you idiot. People can't connect to the server. Period. \n\nWhen you compile KDE you get desktop environment. That's your 'refund'. In that case you don't waste your time.\n\n'Are these people \"idiots\" because they apparently offer a kic-ass service for free, and they are overwhelmed by the demand?'\n\nYes, they are idiots becouse they should have told us that their bandwidth is very limited and that only a few can get connection if they are lucky. They advertise on their website that everybody can get remote desktop. Which is not true."
    author: "PR"
  - subject: "Re: I have something better"
    date: 2005-11-01
    body: "\"Whining? Service? Free? There is no service at all you idiot. People can't connect to the server. Period.\"\n\nOh I see. You feel that you are somehow entitled to this service, even though you haven't paid for it? And if you can't use it for some reason (like, because they are overwhelmed by demand), you start to bitch and moan. \"I'm an user, and I demand that you give me access to this free service NOW! You have an obligation to satisfy my demands, even though I haven't given you a thing in return!\". Here's a clue: You are NOT entitled to one damn thing! These people are offering a service for free. They are NOT asking for money (if they were, you might have some grounds for complaining), and you are NOT forced to use the service in the first place!\n\nSeriously, your whining (yes, you are whining) reminds me of the cancer that plagues the free-software movement. There are people out there who develop software and give it to others for free. And if that software is less that perfect for some people, then those people start to whine that \"hey idiots! This software you gave to me for free has these bugs, and this feature doesn't work! Fix it! NOW!\". Same thing here: we have people here offering a voluntary service for free. And they apparently have some problems coping with the demand. And then some idiots start to complain \"you idiots! You service doesn't work! You suck!\". I for one applaud these people for their effort. But you apparently think that even though they give this service for free, you are entitled to make demands. Well, you are not.\n\n\"Yes, they are idiots becouse they should have told us that their bandwidth is very limited and that only a few can get connection if they are lucky. They advertise on their website that everybody can get remote desktop. Which is not true.\"\n\nUh-huh. So I guess that you have now been mortally offended because there is this free service out there that you can't use at the moment? Someone, somewhere is enjoying their remote desktop right now, for free. And because that someone is not you, you start to whine.\n\nLike I said: this is a free service that is not forced upon you. You do not have to pay a thing to use it. I really, REALLY fail to see what grounds you have to complain about this thing. \"but I losted some time trying to connect!\". Well, boo-frigging-hoo! Cry me a river."
    author: "Janne"
  - subject: "Re: I have something better"
    date: 2005-11-01
    body: "Though I agree that the origial poster should hav lowered his (her) voice, you are incoreect to say that a free service does not entitle anyon to anything.\n\n'Free' as in beer should never ever mean that it is bad. Too often I hear people nagging that gimp|ooorg|evolution|inkscape will never reach the level of their commercial counterparts. We all know this is not pers\u00e9 true. It can be untrue, if the developers, users and designers of that appliaction or service step over that 'its free thus it does not have to be good' mentality. \n\nFree (beer) does simply not mean it can be bad. Free (beer) ONLY meens you don't have to pay for it. "
    author: "Ber"
  - subject: "Re: I have something better"
    date: 2005-11-01
    body: "\"Though I agree that the origial poster should hav lowered his (her) voice, you are incoreect to say that a free service does not entitle anyon to anything.\"\n\nSuppose that I spend few evening whipping up some code, put in on a website and say \"here, you might find this interesting, take it and use it if you want to\". And then someone comes along and says \"hey idiot! You code is broken! I demand that you fix it!\", am I obligated to do it? Is the user entitled to working software? No. Just because I decided to give something away for free does not mean that the people who receive my gift are entitled to make demands. Sure, they can say \"it would be cool if it had feature xxxxxx\" or if they make simple bug-reports. But making DEMANDS is completely out of the question!\n\nThat is what is happening here. Some people decided to offer a service for free. And when there are some initial teething-problems, some people start shouting \"hey idiots! Your service sucks! Fix it!\". Would the whiners be happy if this service was not offered at all, free or otherwise?\n\nI'm not saying that free software (or free anything) is automatically bad. Quite the contrary. Some of the best pieces of software are free. What I AM saying is that just because you received something for free, does not mean that you are entitled to make demands to the people who gave you that gift."
    author: "Janne"
  - subject: "Re: I have something better"
    date: 2005-11-02
    body: "\"Suppose that I spend few evening whipping up some code, put in on a website and say \"here, you might find this interesting, take it and use it if you want to\". And then someone comes along and says \"hey idiot! You code is broken! I demand that you fix it!\", am I obligated to do it?\"\n\nNo, but at least that someone got to /try/ the software... :-/ Cosmopod shure was quick to accept new users even though it could not hope to host them. That's the problem. 3 Days later and still no login...\n"
    author: "Jacx"
  - subject: "Re: I have something better"
    date: 2006-05-23
    body: "heh, google does a good job with how it releases it's new free services...\n\nthey let you join the waiting list\nand then eventually they give you a code to sign up\n\nwhen they are able to (hopefully) be able to provide you with the service they are giving away..."
    author: "Bobb Simul"
  - subject: "Re: I have something better"
    date: 2005-11-01
    body: "Janne: Just ignore him/her, you'll never \"win\" - your time is much better spent doing other things than discussing with trolls..."
    author: "Joergen Ramskov"
  - subject: "Re: I have something better"
    date: 2005-11-02
    body: "Yeah, I just losted 10 minutes reading all of this."
    author: "X_Nihilo"
  - subject: "Re: I have something better"
    date: 2005-11-02
    body: "me too!\n\"\"losted\"\" 15 minutes because of that idiot. This idiot keeps the name PR, but doesn't know a single bit of PR(public relation) ;-)"
    author: "vk"
  - subject: "Re: I have something better"
    date: 2006-01-29
    body: "I have a different perspective to the ass they call PR.\n\nIt actually took me longer to read through this thread than it did to sign up and successfully log on. Can you refund my losted time please."
    author: "Shagpile"
  - subject: "Sorry about the login problems"
    date: 2005-10-31
    body: "I know it must be very frustration with the login problems but we are addressing the problem and getting together some big iron to make the service something really special.  This is a very long term project so it is not going to be going away."
    author: "Stephen Ensor"
  - subject: "Re: Sorry about the login problems"
    date: 2005-11-02
    body: "Please update the progress here."
    author: "reihal"
  - subject: "Re: Sorry about the login problems"
    date: 2005-11-16
    body: "Stephen, How can we invest in your idea or business?\n\nThanks\n\nCurious from the States\n\nBrian"
    author: "Brian DeConti"
  - subject: "No ad impressions from me"
    date: 2005-10-31
    body: "I love KDE, but I don't use KOffice and Konqueror.... I use OpenOffice and Firefox no matter what OS or desktop I'm using.\n\nYou can say \"shut up, it's free, he's being generous\" but 200,000 Google Ads impressions a day isn't generosity, it's opportunity.  A LOT of opportunity.  And until his ad revenue buys him enough horsepower to run mainstream apps, he won't be getting any of it from me.\n"
    author: "raindog"
  - subject: "Re: No ad impressions from me"
    date: 2005-11-01
    body: "\"You can say \"shut up, it's free, he's being generous\" but 200,000 Google Ads impressions a day isn't generosity, it's opportunity. A LOT of opportunity. And until his ad revenue buys him enough horsepower to run mainstream apps, he won't be getting any of it from me.\"\n\nYou are not giving them any money. So there are few tiny ads here and there. Big deal. And that ad-money is propably just pocket-change, nowhere nearly enough to buy lots of bandwidth and lots of servers.\n\nSeriously: people these days seem to think that they are entitled to everything. And if they get things for free, they feel that they are still entitled to something. It used to be that when someone gave you a car for free, you were really grateful to the person who did it. But now-a-days it seems that if you get a free car, you are supposed to whine that \"Dude, WTF is this?! You aren't giving me any free gasoline with this car! Nor are you giving me any spare parts! And this car isn't the latest model! I demand more! You are entitled to satisfy my whims!\".\n\nThe world is going downhill, and fast..."
    author: "Janne"
  - subject: "Just curious."
    date: 2005-10-31
    body: "I haven't had the opportunity to try this service yet but, I am curious about how to do certain things. For instance, how would I go about printing my Word document through this service? Also, with no access to floppies or CD-ROMs, how would I save my files beyond the 1GB that is provided? Finally, what about other peripherals such as scanners, barcode readers, memorystick readers, cameras and USB drives, are these useable with this service and if so how?\n\n"
    author: "Curious George"
  - subject: "Re: Just curious."
    date: 2005-10-31
    body: "<flame>\n\nIndeed. You cannot access any printer connected to that physical computer. You cannot put floppies in it either, nor listen to music on it.\n\nDude, this is a _remote_ server. Just see it as a secured FTP server, with your own additional remote desktop on it. You can bring files to you, which is great. Today you can bring remote graphical apps' display, which is greater. But you cannot bring the physical devices. Now if that is not enough, just don't use it.\n\nIf you want to print something, then you hopefully have a local OS and a printer. So SCP your document back and print it, simple enough.\n\nJust in case you wanted a whole physical computer for free: this is not. Just set up your own Nomachine server at home, with your favorite printer and DVD burner on it.\n\n</flame>\n\nSorry for the flame, but I'm tired of reading people asking more and more."
    author: "David Ammouial"
  - subject: "Re: Just curious."
    date: 2005-10-31
    body: "NX can tunnel printers and the sound, so you could use your local sound and printing, and theres a KDE Kio-Slave to access the host machine's filesystem (the computer you're at)"
    author: "Corbin"
  - subject: "Security from bills and ickets"
    date: 2005-10-31
    body: "I must say, how wonderful that a locked down terminal service is available to people of the Internet. The novelty makes the mind boggle. I look especially forward to getting bills and plane tickets this way. First, since I cannot access the bills to print, I will have difficult time paying them, which is fine - I like to keep my money. Second, I cannot get a printed copy of tickets, which means I cannot leave on boring business trips anymore. Thank you, keep up the good work!"
    author: "Ambrose Bierce "
  - subject: "Re: Security from bills and ickets"
    date: 2005-10-31
    body: "NX will forward your printer (and sound, if they are setup to be forwarded), so yes, you can print them."
    author: "Corbin"
  - subject: "Re: Security from bills and ickets"
    date: 2005-11-01
    body: "Ah, but you see, it is not my printer I am speaking of. What antics I'm willing to perform on the firewall settings on my personal computer is one thing. Quite another is what would be sensible to offer at a publically accessible Internet kiosk."
    author: "Ambrose Bierce "
  - subject: "Re: Security from bills and ickets"
    date: 2005-11-01
    body: "If the Kiosk features an NX client, and also has a printer (how many have printers???), then they it most likely will be able to print.  I've never tried to use the printing feature (don't have one anymore), but most likely it will become easier with time (if its not already brain dead simple).\n\nAlso you wouldn't have to adjust the firewall settings (everything can be tunnelled through SSH)."
    author: "Corbin"
  - subject: "Re: Security from bills and ickets"
    date: 2005-11-02
    body: "You are restating my original argument - I cannot print useful items while on the move from a secured thin client. It is highly unlikely that you will come across a kiosk with the necessary features:\na) an NX client - this is solvable with the NX-applet, but does require a Java-enabled browser with the correct version at the kiosk.\nb) an SMB/CUPS service running on the kiosk to include the local printer, which is then shared with the NX-server - which as described is fairly locked down.\nc) SSH on the kiosk."
    author: "Ambrose Bierce "
  - subject: "Re: Security from bills and ickets"
    date: 2005-11-02
    body: "If I understand NX correctly there are quite a few points that you got wrong.  \n\nA Java-enabled browser, a kiosk setup that allows outgoing connections to port 22 and a printer that is accessible to the *local* machine should be enough.  \n\na) The NX client applet opens an ssh-encrypted connection to the server.  \nb) The NX server forwards the printing request through that very connection to the client. \nc) The NX *client* does the printing locally.  No printer is \"shared [directly] with the NX-server\", no lockdown or firewall comes into play. \n\n(To the people in-the-know: Please someone correct me if any of this is wrong.)\n\n\nOf course, if the kiosk doesn't have any printer you're out of luck, but that would hardly be a problem specific to NX. :)\n\n\n---\nOne potential issue:  Are java applets allowed to print in a normal setup / has a regular user the right to allow that? \n\n"
    author: "cm"
  - subject: "host is down ...."
    date: 2005-11-02
    body: ":(\n\n"
    author: "wannabe using ....."
  - subject: "Good product, some suggestions"
    date: 2005-11-03
    body: "Tried out the product. I think this is a novel idea and it may really take off. Here are a few of my simple suggestions.\n\n - Add Firefox. Konqueror is great, but it still has problems on some pages.\n\n - Add OpenOffice. Same reason as above for KOffice.\n\n - Install the flash plugin. Sure, some people disllike flash, but it is needed for navigation on many sites nowadays.\n\n - It would be *super* if you were allowed to use klik:// to install apps in your userspace. I don't see how this would really be a security problem either as long as the root filesystem has restrictive permissions.\n\n - I wish I could use KControl to change some of my personal preferences (I hate the bouncy icon mouse cursor).\n\nThat is it for now. Best of luck with the software.\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Good product, some suggestions"
    date: 2005-11-03
    body: "As he indicated adding Firefox and OO.o would increase memory usage which would mean more investment into RAM for them per user. Maybe they should instead invest that money into streamlining the configuration and supporting specific improvements in Konqueror and KOffice."
    author: "ac"
  - subject: "BillG. is reading the Dot!"
    date: 2005-11-03
    body: "Whoever was it that said that Microsoft and Bill Gates don't care about KDE or the Dot website -- he was completely wrong. See here:\n\nhttp://www.zdnet.com.au/news/software/soa/Gates_We_re_entering_live_era_of_software/0,2000061733,39220359,00.htm\n\nBillG. must have read this Dot article and acted promptly.   ;-)\n"
    author: "ac"
  - subject: "Do it youself"
    date: 2005-11-07
    body: "Does anyone know a good guide they would recomend about building your one FreeNX server on the internet?\n\nI this way we can use alle the programs we want from any computer, including the memory hungry programs like OOo and firefox."
    author: "Joe S\u00f8rensen"
  - subject: "Re: Do it youself"
    date: 2005-11-07
    body: "> I this way we can use alle the programs we want \n> from any computer, including the memory hungry programs like OOo and firefox.\n\n... and you keep control of your own data (this is *not* to imply anything bad about CosmoPOD.com). \n\n"
    author: "cm"
  - subject: "Cosmopod gone?"
    date: 2005-12-17
    body: "Cosmopod.com does not seem to be with us any longer.  Has it moved, or does anyone know what happened?\n\n"
    author: "Roger Hiles"
  - subject: "Comsopod WHOIS registration info"
    date: 2005-12-19
    body: "WHOIS Search Results for: COSMOPOD.COM\n\n  \nThe data contained in Go Daddy Software, Inc.'s WHOIS database,\nwhile believed by the company to be reliable, is provided \"as is\"\nwith no guarantee or warranties regarding its accuracy. This\ninformation is provided for the sole purpose of assisting you\nin obtaining information about domain name registration records.\nAny use of this data for any other purpose is expressly forbidden without the prior written\npermission of Go Daddy Software, Inc. By submitting an inquiry,\nyou agree to these terms of usage and limitations of warranty. In particular,\nyou agree not to use this data to allow, enable, or otherwise make possible,\ndissemination or collection of this data, in part or in its entirety, for any\npurpose, such as the transmission of unsolicited advertising and\nsolicitations of any kind, including spam. You further agree\nnot to use this data to enable high volume, automated or robotic electronic\nprocesses designed to collect or compile this data for any purpose,\nincluding mining this data for your own personal or commercial purposes. \n\nPlease note: the registrant of the domain name is specified\nin the \"registrant\" field. In most cases, Go Daddy Software, Inc. \nis not the registrant of domain names listed in this database.\n\n\nRegistrant:\nstephen ensor\n57 winterfold close\nlondon sw19 6le\nUnited Kingdom\n\nRegistered through: GoDaddy.com (http://www.godaddy.com)\nDomain Name: COSMOPOD.COM\nCreated on: 26-Aug-04\nExpires on: 26-Aug-06\nLast Updated on: 20-Jul-05\n\nAdministrative Contact:\nensor, stephen steve@iqula.com\n57 winterfold close\nlondon sw19 6le\nUnited Kingdom\n00247791047477\nTechnical Contact:\nensor, stephen steve@iqula.com\n57 winterfold close\nlondon sw19 6le\nUnited Kingdom\n00247791047477\n\nDomain servers in listed order:\nPARK13.SECURESERVER.NET\nPARK14.SECURESERVER.NET\n\n\nRegistry Status: ACTIVE \n"
    author: "bakalao jones"
---
<a href="http://www.cosmopod.com">CosmoPOD.com</a> offers free remote KDE desktops over <a href="http://freenx.berlios.de/">NX</a>.  Anyone can sign up to have their own desktop accessible from any computer with a network connection.  CosmoPOP uses KDE's <a href="http://extragear.kde.org/apps/kiosktool/">Kiosk</a> framework to ensure security for their system.  To find out more about the service and why KDE was the chosen desktop, KDE Dot News spoke to the man behind CosmoPOD, Stephen Ensor.  Read on for the interview.





<!--break-->
<div style="border: thin solid grey; padding: 1ex; float: right; width: 300px">
<img src="http://static.kdenews.org/jr/cosmopod-wee.png" width="300" height="234" /><br />
The KDE desktop on CosmoPOD
</div>

<p><strong>What services does cosmopod.com offer?</strong></p>

<p>CosmoPOD.com is a personal online desktop that offers 1GB of online storage, one may run CosmoPOD.com on any computer that is connected to the internet and may do all the common tasks a modern desktop has to offer. My long term vision for CosmoPOD is: one day you will walk through an airport or tube station and touch a public screen it will log you in instantly via your fingerprints and in a wink you will be on your full screen easy to use desktop, a little later your cell phone will bling and you can call up a mobile version of your same desktop and check your email IM etc. Users won't have a constant stream of popups telling them to update their systems and applications and security will be all there and up to date, if a next big thing app hits the market it will already be there for the users.</p>
 
<p><strong>How did you get the idea for this?</strong></p>
<p>I was playing with many different Linux distros for a while and was contracting at the time in London, I was e-mailing many documents between work and home and I found myself duplicating a lot of work because of the different versions. I thought a central remote desktop that I could access from anywhere would be the answer. I also recalled arriving in London and e-mailing files back and forth and trying to edit them on the internet caf&eacute;'s computers was a nightmare. So I really wanted it for myself and thought it would be really cool if anyone in the world could have one free too. I also thought with security becoming such a big issue and most normal users just don't know how to keep their systems secure, people would want a secure place to store and do their office related work.</p>
 
<p><strong>How many people are currently using cosmopod on a frequent basis?</strong></p>
<p>Per day I get around 150 people coming on and off their desktops, around 30 new registrations, about 300 visitors to my website and serve up about 200,000 Google ads. All this has been gaining good speed and I expect this to pick up quite a lot when I start a marketing drive.</p>
 
<p><strong>Why did you choose KDE?</strong></p>
<p>Funny you ask I was actually looking to use Gnome first but after much reading and testing I found KDE to be snappier and have a smaller memory footprint, the supporting applications were mature and well integrated and Konqueror was great! But the tipping point came when I started to look into lockdown features, KDE became the clear winner with the Kiosk Admin Tool. There are a few things I would still like a fast KDE start up and KOffice is not as feature full yet as OpenOffice (which runs too slowly and takes up too much memory to be used) so that is the trade off there.</p>
 
<p><strong>What applications does CosmoPOD's KDE offer?</strong></p>
<p>CosmoPOD offers all the applications a modern desktop required, a MS compatible office suite, e-mail, browser, IM, html editors, games etc. I generally install any applications I get e-mailed by users if they are not the same as ones already installed. I don't want 5 IM clients for example.</p>
 
<p><strong>What changes does cosmopod's set up have over a typical home user KDE setup?</strong></p>
<p>The only difference is the lock down so that users cannot install their own applications and move the menus around, they also cannot access floppy and CD drives as this is now a networked desktop.</p>
 
<p><strong>How easy was it to lock down the KDE install?</strong></p>
<p>This was very easy once I discovered the Kiosk Admin Tool, installed it and it makes it pretty clear what is going on in a nice GUI, there are still a few problems with configuring the menus on different distros but otherwise it makes things very easy especially the way it handles users and groups.</p>
 
<p><strong>Have you had any feedback on experiences of non-KDE users on this KDE desktop?</strong></p>
<p>Yes and no.  People are always very happy with the service and it's been great having such keen feedback, I don't get much discussion from the users regarding what technology is being used.  They are generally non-techie and just use it and the applications on it. I think locking stuff up helps so the users don't remove their taskbars, menus and buttons, KDE is great in that the K applications are quick and well integrated into the desktop. I try and keep application choice down to a minimum so as not to confuse users and have a few ideas to simplify things further in the future.</p>
 
<p><strong>How do you pay for the service?</strong></p>
<p>I keep a bit of real estate on the right hand side of the screen where I serve up Google ads soon to be based on what the user is reading similar to Gmail. Before I started with setting up the service i did many spreadsheets and forecasts to see if the model held and it does quite well, I don't yet have the contextual thing going yet but it is in the pipe line. We are also exploring putting branding images inside windows and menus.</p>
 
<p><strong>What sort of hardware does this run on?</strong></p>
<p>Very standard and cheap x86, we are looking into renting processing power as there are certain times when we spike but that is a while away.</p>




