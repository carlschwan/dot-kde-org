---
title: "The Linux Box Show: Aaron Seigo on KDE's Future"
date:    2005-04-06
authors:
  - "jriddell"
slug:    linux-box-show-aaron-seigo-kdes-future
comments:
  - subject: "plans for making KDE 4 the leader for usability"
    date: 2005-04-06
    body: "The phrase \"the leader for usability\" makes me think of an article about the Autopackage vision from Mike Hearn.\n\nhttp://autopackage.org/ui-vision.html\n\nThe non-existance of a *easy* packaging system for apps in the user space is a huge problem imho.\n"
    author: "kubu"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-06
    body: "users dont have to mess around with packages or even (de)installing of smth. thats admins work\n\nin terms of usability kde is imho already at the top\nespecially like the kio-slaves, i make one for isbn (gives you as short desc), when its polished up a bit i will put it on kde-apps"
    author: "andy"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-07
    body: "> thats admins work\n\nin controlled environments this is quite true.\n\nin the home and SMB markets it's a rather different story."
    author: "Aaron J. Seigo"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-07
    body: "On a side note, one area where I have seen kde really shine is its kiosk mode for locking down computers. It has allowed us to roll out sunray services across our university. I simply cannot sing the praises of kde enough for making our secure deployments possible."
    author: "ac"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-07
    body: "could you sing them into an article for enterprise.kde.org? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: plans for making KDE 4 the leader for usabilit"
    date: 2007-10-12
    body: "Yes, please sing those praises. I would really like to know how to get kde kiosks running on sunrays."
    author: "faf"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-07
    body: "So, my dear geek, you mr. Joe Sixpacks had just bought your brand new 700&#8364; computer at the mart...who is the admin now? the one who sold you the PC? no one? or maybe YOU, the poor user?\nNot everyone is living in a corporation or an university...so this kind of phrases are really weak when it comes to non professional world."
    author: "Davide Ferrari"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-08
    body: "with your own comp your the admin, ok. but then you have to learn the same things as every other admin too, at least to a certain degree. badly managed home systems are one of the most dangerous things in existence; no need to know the exact vm table layout but installing kde from source (not cvs) with or without konstruct shouldn't pose any problems; btw, there are people called \"distributors\" who take care of packaging, so you only have to click a button for an update\n"
    author: "andy"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-08
    body: "> but then you have to learn the same things as every other admin too\n\nwe can keep telling ourselves this, but reality says something completely different. if we are fine with never making it into the small business and home markets, let's just keep repeating this to ourselves so we feel ok.\n\n> badly managed home systems are one of the most dangerous things in existence\n\nso let's make it _easy_ to manage home systems _properly_. we're actually behind both Apple and Microsoft on this one by a long ways.\n\n> installing kde from source (not cvs) with or without konstruct shouldn't pose\n> any problem\n\nhehe.. i wish.\n\n> so you only have to click a button for an update\n\nthese tools aren't yet easy enough. i find them a delight. my non-techie friends find them a nightmare."
    author: "Aaron J. Seigo"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-09
    body: ">> badly managed home systems are one of the most dangerous things in existence\n> \n> so let's make it _easy_ to manage home systems _properly_. we're actually behind both Apple and Microsoft on this one by a long ways.\n \ni guess, 95% of all home-brewn gnu/linux systems are better managed than 50% of alle _commercially_ used windoof-systems and 95% of all home-used windoof systems COMBINED\nso who's behind who now?"
    author: "andy"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-09
    body: "(86% of all statistics are made up on the spot ;)\n\nthis is due to a couple reasons: first, most Linux users are more competent than most Windows users. this helps. the other factor is that Unix is inherently better designed for this kind of management.\n\nwhat we lack, and where we are behind, is exposing our management concepts to end users in a way such that the _average_ computer user who's currently using something like Windows (poorly) can utilize them.\n\nwe have good things, but they are manual, hidden and/or esoteric. that's what we need to address."
    author: "Aaron J. Seigo"
  - subject: "Re: plans for making KDE 4 the leader for usability"
    date: 2005-04-10
    body: "I'm so glad that someone so deeply involved in KDE development as you think this kind of things :)\n\nTo all the geeks out there: no one is going to break your toy, what KDE (and Linux world in general) need is an additional, bypassable layer, useful for non-tech users. But if you've got the skill, you can bypass it and manage your box in the best way you think."
    author: "Davide Ferrari"
  - subject: "Re: plans for making KDE 4 the leader for usabilit"
    date: 2005-04-07
    body: "The soltion to this 'problem' is NOT having a decentralised random 'hunt the binary' system. This will lead to unpredictable messes of systems with .exe soup and random versions of libraries, like on windows.\n\nA package management system is a feature (that windows doesn't have), not a weakness of unix."
    author: "Robert"
  - subject: "Re: plans for making KDE 4 the leader for usabilit"
    date: 2005-04-07
    body: "You obviously do not understand autopackage, what it is and what it does. Read up their FAQs to educate yourself."
    author: "Eu"
  - subject: "Re: plans for making KDE 4 the leader for usabilit"
    date: 2005-04-07
    body: "I have been following the autopackage project since the beginning."
    author: "Robert"
  - subject: "I'd be happy with..."
    date: 2005-04-06
    body: "Desktop icons that were in the same place when I logged in as they were when I logged off.\n\n[/gripe]\n\nYes, there's a bug filed.  For a year or so now...\n\nOther than that, though, things have been working well and only getting better (although that's a lot like Mrs. Lincoln saying: \"Other than that, the play was good!\").\n"
    author: "ac"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-06
    body: "i'd like to see that issue fixed as well (though i thought it was with a patched Qt and KDE 3.4?), but to be honest, the idea of \"the desktop is a dumping ground for icons\" is waaaaaaaaaay past its prime. i can think of 10 better uses for the desktop than a static file manager view."
    author: "Aaron J. Seigo"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-06
    body: "Like?"
    author: "Joe"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-06
    body: "there are two primary problems with the \"desktop icon\" concept, IMHO:\n\nfirst, it's completely static. well, almost completely static. the icons change when you mount devices or plug in a camera, USB stick or other bit of removable media (assuming you have it set up right, anyways). but our desktops are anything but static.\n\ntake the trash icon for instance. things are coming and going all the time in trash:/ (well, at least here they are; i'm a bit of a multitasker and create lots of temporary files that hit the bin a few days or weeks later). why shouldn't the trash representation on the desktop be equally dynamic? it should show me how many files are in the trash and how many Kb is being used, it should \"expand\" to show a small \"window\" when i hover over it showing me a list of files that are in it ... it should let me know when it's reaching \"capacity\".\n\nor take project based work. if we knew what files / windows belonged together in a project, we could have a \"project corral\" that appears on the desktop widget showing files, actions and destinations that relate to the project you are currently working on.\n\nthe desktop should be a useful starting point and a helpful destination, two things it basically sucks at right now. we have the compute power to do it \"right\" now, and hopefully the imagination to match.\n\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-06
    body: "You didn't provide even one \"way better use of the desktop\" than as a static file manager even though you claimed to know 10.\n\n(Actually your trashcan-example is merely a refinement of the basic old static file manager paradigm)\n\n"
    author: "Roland"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "> You didn't provide even one\n\ni assume you didn't grok the concept of the dynamic project based icon \"corral\" then. \n\ni also didn't list the other primary problem i see with the \"icon dumping\" desktop concept, just the issue of its static nature. the other issue is that we don't allow for anything other than icons (well, and wallpapers, which can be painted by a program).\n\nthere's really no reason the desktop couldn't, or shouldn't, provide access to embedded applications. the desktop space is a bit special in that it's often covered by other windows, but it's also special because it's the \"lowest\" thing in the interface. so you can put things there and know that they are always there. this is similar to the concept space that panels occupy, except that panels are (usually) always the \"highest\" thing in the interface.\n\nso while panels should contain the most important dynamic objects that you just can't live w/out (thereby justifying their \"always top priority\" existence) the desktop should contain items that are very useful to have but not always required.\n\nkaramba and friends show some of the possibilities, but their offerings generally are not used for user interaction (with some notable exceptions). i'd sooner have my RSS feeds, IM status, weather and a little \"Run: \" box on my desktop with a trash icon that expands to a fully usable miniinterface showing its contents than the current static icon fare.\n\nin any case, i stuck with icon-related ideas in the previous post as they don't require a huge leap of imagination and are pretty non-controversial while highlighting how we have stopped way short of the possibilities by allowing just icons. that and i'm not going to write a book on the topic here in a web forum =P\n\nif you apply your imagination just a bit you can probably lead yourself to wondering how other common icons on the desktop could be made much more useful by reinventing them as something non-static.\n\nso let me turn it back to you.. what genius ideas for desktop-plane improvements can you suggest? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "My paradigma would be: the desktop is obsolete. Our main operating GUI system is the webbrowser. All setting navigation via local html files. I do not want application for settings such as the Control center to mess up my application menu, I want a Control Center which is embedded in my webbrowser. Html does only offer limited options for controls  but you can use embedded apps that open in your browser tab. Html navigation enables you to customize your personal navigation and it is pretty easy, everybody can use the web navigation. just put a link to your needed setting application, your files and so on on your browser start page. Why do I need an application with its own window to move around my desktop. Browser tabs are fine and if I needed a shell I can start a shell applet in that web browser. \n\nWindows transformed the desktop to a window with \"wallpapers\". But a desktop is your desktop. I want information applets about my email inbox, easy access to contact data, my online time etc. on my desktop. I do not care about which application does the task. I do not want to move application windows around my desktop.\n\n"
    author: "hmm"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "YEAH! 100% agree, you're absolutely right. In fact Windows spread a really horrible use of the desktop, but this must be stopped! WTF do I have to access the desktop (which is 95% of the time covered by other USEFUL windows) to get access to removable device or the trashbin? They are meant to stay in an alwys visible place, such as the kicker (and the trash and media applets are great for this!)\n"
    author: "Davide Ferrari"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "\"it should show me how many files are in the trash and how many Kb is being used, it should \"expand\" to show a small \"window\" when i hover over it showing me a list of files that are in it ... it should let me know when it's reaching \"capacity\".\"\n\nToo late. I filed a wishlist item for this more than a year ago."
    author: "Joe"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "Too late for what?"
    author: "Ian Monroe"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-06
    body: "Can you tell me the first 5? ;-)\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-06
    body: "Agree only partially.\nThe grand-parent said he would like to see the icons at the\nsame position at re-login. No matter what other metaphors are\ninvented for the desktop - a desktop will always be, well, a desktop.\nAnd do you think it's cool if someone would shuffle your office desktop\naround over night? BTW: We've got some cleaning personnel at our company\nwho upset me regularly with mixing all my papers...\nSo the basic principle is: The desktop will always look how you\nleft it when you come back (i.e. log on). Guess that's undisputed.\nI agree with the \"dumping ground\" part. There could be a lot more\nto a desktop. Extensions like SuperKaramba show the way. Things like\nthat have to be integrated natively and not in a scripting language\nconsuming way too much resources for a permenant desktop.\nWhat I'm missing since Windows 3.0 times with \"Program Manager\":\n\"Program Groups\". It's simply horrible that I cannot create rectangular\nwindow-like \"drawers\" on my desktop where I can put my stuff in.\nSth. like the unfortnately abandonded Slicker-project with stackable Card\nfor the desktop would be great. IMO the project has been too ambitious\ntrying to create cards for each and everything. A much simpler thing\nwould already make a big difference. Create cards where you can put\nyour desktop icons in. These can be rolled up and down. This way\nI can roll-down and roll-up cards specific to say Multimedia or\nWeb Development adapting my desktop to the needs at hand. When doing \nWeb Development I have all required apps: Quanta, KColourChooser, KRuler,\nGIMP etc. This makes even more sense when using this to organize\nyour documents on the desktop. So In a way the whole \"dumping ground\"\nproblem you mentioned sums up to the demand for better icon grouping\nand hiding.\n\n\n\n\n\n\n\n"
    author: "Martin"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "So you want multiple desktops each with a different set of icons on them."
    author: "Sean Brown"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-07
    body: "Not really, because I want to (re-)combine my groups of icons \narbitrarily but what you propose would be at least a little\noportunity to organize my desktop. Right now there is no way\nwhatsoever to organize one's desktop except putting all the\nstuff into app groups which is way too complicated especially\nconsidering the fact that you cannot do this with D&D like on Windows.\n\n"
    author: "Martin"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-08
    body: "\"hen doing Web Development I have all required apps: Quanta, KColourChooser, KRuler, GIMP etc.\"\n\nWell, quite exactly what I do.  My fifth workspace shows open Quanta, TkCVS and a Konsole.  Since TkCVS already has filetypes asociated it works like a tiny file manager (I can double-click on a png file and it will open the Gimp, for instance, or a txt file and then it will open Kwrite) and it stays there from login to next login.\n\nOn other news, workspace one opens Kontact; number two Konqueror, number four a superuser console, number six Karm and some Knotes and number three is empty awaiting for \"other tasks\".  This approach is a bit RAM spending, but I could achive the same with some half a dozen icons wich in turn launched the needed application-set for any given workspace.\n\nThe real problem here is that the workplace paradigm has been here say ten or twelve years and on this time most of the people hasn't even made the effort to learn how to write the simplest script (hell, even Windows 3.1 did support this via pif files) but they didn't matter loosing hours and hours and hours doing by hand lots of things easily automatable or whining about their workplace don't doing things it already does (and have been doing for ages)."
    author: "devil advocate"
  - subject: "How to keep device icons in place"
    date: 2005-04-08
    body: "If you mean device icons like floppy, CD, etc., then the solution is to uncheck them in Control Center > Desktop > Behavior > Device Icons tab.  Then simply re-create the icons for the devices you want on your desktop by right-clicking > Create New > Device.  Setting up the device icons this way will keep them in place for you.  Hope that helps.  "
    author: "nlkrause"
  - subject: "Re: How to keep device icons in place"
    date: 2005-04-08
    body: "Thanks for the advice, but no--I'm talking about application icons."
    author: "ac"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-08
    body: "This has been fixed for me, by either the latest qt or the latest kde or both."
    author: "kundor"
  - subject: "Re: I'd be happy with..."
    date: 2005-04-10
    body: "I'm using Gentoo with KDE 3.4 and QT 3.3.4 and I'm no more experiencing this issue. It's a QT related issue AFAIK and with QT 3.3.4 (maybe with some already available patches that Gentoo kindly applies) is resolved. My desktop icons are there on every boot, and they don't move themselves :)"
    author: "Davide Ferrari"
  - subject: "Not funny"
    date: 2005-04-06
    body: "It is not as annoying as LUGradio, but why does he have speak that way?\nI makes me sleepy and the jokes are not funny."
    author: "Carewolf"
  - subject: "Re: Not funny"
    date: 2005-04-06
    body: "i'll practice my Seinfeld for the next one ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: Not funny"
    date: 2005-04-06
    body: "I am talking about the host and you know it ;)\n\nAfter hearing this, I don't think LUG-radio is so bad. They just need at better KDE attitude."
    author: "Carewolf"
  - subject: "Re: Not funny"
    date: 2005-04-06
    body: "there was a host? =P"
    author: "Aaron J. Seigo"
  - subject: "Re: Not funny"
    date: 2005-04-06
    body: "Yes, 127.0.0.1"
    author: "Frank Dux"
  - subject: "Re: Not funny"
    date: 2005-04-07
    body: "I know what your talking about. Really its an issue of their being only one host on the Linux Box, versus the locker room of LUG Radio. Only if there could be a show as entertaining as LUG Radio that didn't ask questions like whether KDE was going to be rewritten. And apparently none of them compile the kernel. Which is fine for a normal joe user, but these guys are hosting a Linux show, they need to drink more of the kool-aid."
    author: "Ian Monroe"
  - subject: "Very intresting"
    date: 2005-04-06
    body: "I enjoyed listening to it and really liked the interview.  I'm probably gonna listen to the previous 5 shows now and I already added his RSS feed to KNewsTicker!"
    author: "Corbin"
  - subject: "Transcript"
    date: 2005-04-06
    body: "a transcript, probably full of typos, can be found here:\n\nhttp://aseigo.bddf.ca/?pID=1221"
    author: "Aaron J. Seigo"
  - subject: "Re: Transcript"
    date: 2005-04-06
    body: "Now that's really cool! Now that's really cool! Joy, joy, joy!!\n\nWhich industrious soul did that? Aaron, giv'ver a huge hug!"
    author: "ac"
  - subject: "Re: Transcript"
    date: 2005-04-06
    body: "\nUnfortunately, this little announcements hidden in the comments will not be seen by many.\n\nDid you consider to make a separted Dot news item from this? Are you intending to submit this to Linuxtoday, Newsforge, LWN.net, OSnews, Slashdot?\n\nWhat Aaron says is well worth being seen by many around the KDE community, users as well as developers.\n\nI, personally, will show it to our IT boss, who recently mentioned that he pondered to use Mac OS in some corners of his department, since \"KDE is not yet there\" (though he likes OSS/Linux more than Apple+proprietary SW."
    author: "mountebank"
  - subject: "Re: Transcript"
    date: 2005-04-06
    body: "> Are you intending to submit this to ...\n\nplease feel free to submit this to websites you feel would be a good venue for it. i always feel a bit odd submitting things that i appear in myself for broader coverage elsewhere. i figure that if it's interesting, someone else will feel the urge to spread the word.\n\n>  who recently mentioned that he pondered to use\n> Mac OS in some corners of his\n> department, since \"KDE is not yet there\" (though he likes\n> OSS/Linux more than Apple+proprietary SW\n\nthat's twice now that i've heard this exact sentiment in as many weeks. crazy. i'd love to know why your boss thinks \"KDE is not yet there\", because unless we really understand what the issues are we are not well empowered to address them (or ignore them, as the case may be ;)\n\nand if there is anything i can do to help with KDE adoption in your shop, please let me know and i'll see what i can do, either myself or by mustering up appropriate resources in your area as best i can."
    author: "Aaron J. Seigo"
  - subject: "Re: Transcript"
    date: 2005-04-07
    body: "> i'd love to know why your boss thinks \"KDE is not yet there\",\n\nHe plays with Knoppix for 20 minutes after each release. (I always have to download and burn it for him). He thinks it is great technology, but he has difficulty \"forget\" his doubleclicking routine.\n\nHis son didn't fancy the games Knoppix included too much, and got bought one of the new Sony (or whatever) Playstation thingies.\n\nHis daughter first had an iPod and now wishes to get an iBook, and he'll buy it to her, should she complete her A Levels with good enough results.\n\nBTW, he now is also a fan of http://klik.berlios.de/ which he thinks should be the way of how software installations work. He just loves that feature on the new Knoppix-3.8 that lets him one-click-install all software (with the help of UnionFS + a permanent home on a USB stick + klik). \n\nHe also loves NX on the Knoppix. (He wrote to their developers in Italy asking them to make that available on the Mac platform too...) \n\nI have high hopes that he will use Knoppix a bit more often now. I will make it so that he can log into his office computer -- WinXP :-( -- from home, running Knoppix with the help of NX.\n\nHe thinks Kicker and zooming icons suck. \n\nHe sees OS X at his brother's (who is a graphical artist). That guy told him Apple had great server hardware too.\n\nMy boss isnt a particularly technical person. He is a manager type with an open mind. He hates Microsoft.\n"
    author: "mountebank"
  - subject: "Re: Transcript"
    date: 2005-04-07
    body: "so to sum up: his family is Mac oriented (daughter, brother) so they influence him, he wants easier software installation and has some issues getting used to new ways of doing things. that's not bad; nothing insurmountable. =)\n\n> He thinks Kicker and zooming icons suck. \n\nzooming icons are gone in 3.4."
    author: "Aaron J. Seigo"
  - subject: "unfortunatly..."
    date: 2005-04-11
    body: "i believe you misunderstood this. you got the rove right, thats for sure. but when people said zooming icons suck, i believe they meant to say: \"they suck, improve them!\" not \"they suck, remove them...\"\nwasnt someone having this idea about multiple effects, maybe via plugins? way to go, icon mouseover candy fx useable everywhere. eh? "
    author: "thatguiser"
  - subject: "Re: unfortunatly..."
    date: 2005-04-11
    body: "bleh. you know what, if you keep harrassing me about this ... it won't change anything =)\n\n> \"they suck, improve them!\" not \"they suck, remove them...\"\n\ni covered why this wasn't realistic in great detail in my blog. you may not agree, but you'd be wrong. i appreciate your fanaticism over this feature, it's heartwarming and cute. it's also energy misplaced.\n\n> wasnt someone having this idea about multiple effects, maybe via plugins\n\nyes, that someone was me. maybe \"someone\" would like to implement that? oh right, that'll be me too in KDE4. =/\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: unfortunatly..."
    date: 2007-09-08
    body: "Unfortunately*"
    author: "Haxzar"
  - subject: "Re: Transcript"
    date: 2005-04-07
    body: "At one of the company I worked for, we had two Dell with Linux Slackware \nand a MacOS X rackmount G5 xserve...\nhttp://www.apple.com/xserve/\n\nI can tell you just one thing, MacOS X might be great for the desktop,\nespecially for photoshop artist who wants simplicity,\nbut even with fink (which takes forever to install anything much like Gentoo), \nLinux or BSD is much better for a server configuration.\n\nAlso, who needs a GUI to install anything on a xserve ?!\nwhen the only thing you can *normally* access remotely is an SSH prompt ?\n\nOne funny part, was trying to install a GUI package remotely,\nwhere the \"installer wizard\" needs some *clicks*...\nQuite difficult when the xserve is only accessible remotely via SSH.\n\n[With some sort of VNC clients and a Mac it's feasible tough]\n\nIn the end, even our Mac fanatic who bought the xserve \n*and was really proud of it because he tought it was soooo cool!!!*\nwanted to get rid of his xserve, after we encountered some irritating problems \nthat we didn't have on our default installation Linux box.\n\nMy Conclusion: Apple is really good at marketing.\n\n\n\n\n\n"
    author: "fprog26"
  - subject: "Submitted to Slashdot"
    date: 2005-04-06
    body: "I submitted the story along with a link to the transcript to slashdot, it'll probably be accepted, its a great little interview!\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: Submitted to Slashdot"
    date: 2005-04-07
    body: "Note that the suggestion listed Slashdot as the last. So next is OSnews?  :)"
    author: "ac"
  - subject: "Re: Submitted to Slashdot"
    date: 2005-04-07
    body: "Apparently it didn't get in. Try to substitute \"Aaron J. Seigo\" with \"Miguel de Icaza\" and I'm sure they'll let it in."
    author: "Anonymous"
  - subject: "Re: Transcript"
    date: 2005-04-06
    body: "Thank you :)\nI dislike audio-interviews, if only because I have to turn off the music and pay attention so I don't miss anything..."
    author: "Illissius"
  - subject: "Re: Transcript"
    date: 2005-04-06
    body: "That sound quality was horrible. And probably no more than 200 people will listen through it. Even if it is only for the bandwidth they don't have...\n\nBeing able to *read* this interview, is much more pleasant. Hopefully 10 times as many will read (at least bits) of it."
    author: "anon"
  - subject: "various thoughts.."
    date: 2005-04-06
    body: "..about this interview.\n\nAaron, you rock ! That's the kind of publicity KDE needs :-) \n\nAbout your words about KDE4: IMO it's quite brave to talk right now about the applications which will be part of KDE4... it's still so far away (in terms of work).\n\nAbout Appeal: sounds interesting, and it seems there are people involved which will ensure that it stays alive. Hopefully it will attract new contributors to KDE :-)\n\nAbout kOffice: we need developers ! \n\nAbout porting apps to windows:I'm very very glad that cygwin exists. It enables us at work to use the free eCos RTOS. I'm glad that eclipse exists for the same reason.\nOTOH I'm also glad that kdevelop (linux-only) and cutecom (linux-only, ok, I wrote it) exist, since both apps are better than their equivalents on windows and thus are arguments for switching to linux instead of staying on windows. For the same reason it's ok for me that cygwin (bash, gcc, etc.) run so slow under windows. I can say \"switch to linux and it will run much faster\". Maybe in this sense porting apps to cygwin is really a good idea.\nUnfortunately I can't say this about OOo. It will also run damn slow on linux :-/\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: various thoughts.."
    date: 2005-04-06
    body: "> about KDE4: IMO it's quite brave to talk right now about the applications\n\nwhich is why i tried not to predict too many specifics, but rather just describe the current conditions that we are dealing with. everything i mentioned is being developed right now, so it seemed safe enough to talk about those things. i'd have loved to go on a wild ride of imagination, but i agree that that would be over the line =)\n\n> Hopefully it will attract new contributors to KDE\n\nthat's at least one of its goals, yes =)\n\n> About kOffice: we need developers ! \n\ngrow the user base and you'll get more developers. not just from the users, but because it will be a higher profile project. let's think about how to accomlish this.\n\nglad you liked the interview =))"
    author: "Aaron J. Seigo"
  - subject: "Re: various thoughts.."
    date: 2005-04-06
    body: "Well I am going to try to get into contributing to/hacking on KDE.\n\nI think this post ultimately inspired me to try it.\nhttp://aseigo.blogspot.com/2005/04/trying-something-new.html\n\nIf Aaron can dedicate half his work days time to KDE, there's no reason why I can't do a couple hours here and there.\n\nPlus, I gotta represent BC, Canada!  ;)\n\n"
    author: "Leo Spalteholz"
  - subject: "Re: various thoughts.."
    date: 2005-04-07
    body: "that's terrific news! =)\n\nwhere in BC? i lived on the Sunshine Coast for .. *counts* 13 of my years on this planet (not all in a row, though). i LOVE the coast. and i swear that one day i will be back out there =)\n\nDerek Kite who does the kick-ass weekly CVS Digest and Ivan who does Ukrainian translations for KDE are both in BC. we've got three dots on the worldwide.kde.org map for Western Canada... let's add some more =)\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: various thoughts.."
    date: 2005-04-07
    body: "I'm in Victoria.  Doing a co-op work term at the university there, where I'm also taking computer engineering.\n\nSunshine coast is definitely nice. :)  I haven't lived there, but I rode down it on my bike once.  Very scenic.\n\nWell don't count me in as a dot yet ;) It'll take me a while to get acclimatized to KDE.  I do mainly C++ development at work, but I haven't worked with QT or the KDE libs before.  Should be interesting.\n\n"
    author: "Leo Spalteholz"
  - subject: "Re: various thoughts.."
    date: 2005-04-07
    body: "Leo: if you are ever in the interior, get in touch.\n\nDerek (in Nelson)"
    author: "Derek Kite"
  - subject: "Re: various thoughts.."
    date: 2005-04-07
    body: "Salmon Arm > Everybody else!\n\n:P"
    author: "Sheldon"
  - subject: "Re: various thoughts.."
    date: 2005-04-07
    body: "Lol, not there now, but have family still in Oliver"
    author: "Crouching Tiger"
  - subject: "Western Canadian representation..."
    date: 2005-04-07
    body: "Heh, I'm in BC, too, lower mainland to be precise.  I thought I was largely alone. *makes the funny \"west syde\" gesture with his hand*\n\nI'll have the summer to start getting more comfortable with KDE development, right now I just download apps, compile them and fix little personal quibbles with them here and there, getting used to things."
    author: "Saem"
  - subject: "Re: Western Canadian representation..."
    date: 2005-04-07
    body: "Fixing personal quibbles ?\nSounds interesting.\nHow about posting the patches to kde-devel ?\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Western Canadian representation..."
    date: 2005-04-08
    body: "Honestly, it isn't worthy.\n\nI still need more practice.  And just because I'm contending with them doesn't mean their a problem.  I very well could be the issue -- off center thinker perhaps.\n\nBesides, when I do start submissions, it'll likely start with documentation."
    author: "Saem"
  - subject: "Re: various thoughts.."
    date: 2005-04-07
    body: "> About porting apps to windows [...] since both apps are better than their equivalents on windows and thus are arguments for switching to linux instead of staying on windows.\n\nOne reason not to port to windows: The problem with Linux isn't the desktop or the apps, most of them are great! It's the middleware that causes most problems, the layer between the kernel and the desktop.\n\nUsers trying Linux don't have problems with the desktop, but configuring aspects of their system in /etc, making their ATI card work, scanner, SATA drives, or a multi-boot if the system has a SATA and IDE drive, etc...\n\nBringing the KDE desktop to Windows doesn't solve our Linux problems, and we gain nothing. :-("
    author: "Diederik van der Boor"
  - subject: "Content manager and klink"
    date: 2005-04-07
    body: "How is the content browser related to klink? Will it be a simple interface to the klink framework?\n"
    author: "Sven Langkamp"
  - subject: "Re: Content manager and klink"
    date: 2005-04-07
    body: "yes, Tenor[1] will be (at least part of) the engine that the content browser will draw upon for what it displays and how it displays it. it will also be a place we experiment with some new interaction concepts. so while Tenor will be an important part of the content browser, there'll be more to it than that =)\n\n[1] klink was never meant to be a permanent name. we're using Tenor instead. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Content manager and klink"
    date: 2005-04-07
    body: "Be careful with those temporary working titles.  :)\n\nThe project name Kroupware stuck long after the product Kolab had emerged.  And of course the slashdot audience still complained about the name... I think now it's over but it took quite some time. "
    author: "cm"
  - subject: "Human Interface!!! Shortcuts are a pain in KDE"
    date: 2005-04-07
    body: "The keyboard accelerated shortcuts are not easy to work with, for example \n\n1. A Right-Handed person would use mouse with right hand and Left hand fingers are free for keyboard typing, those are (asdfg,qwert,zxcvb,12345). The keybindings are really not suitable for the Right Handed persons for most of the Menu (Desktop Menu, Properties menu, etc).\n\nLike if i want to access Properties of Desktop, i have to do\n\n1. Right Click on the Desktop (RMB clicking mouse with my right hand)\n2. now the shortcut kde provides is 'P' which is difficult to press with left hand.\n\nfor Human Usability and efficient work the keys should be:\n\n1. Properties 'R' to access Properties(preferences). \n2. Rename     'E'\n3. Delete     'D'\n4. Cut        'T' \n5. Copy       'C'\n6. Paste      'P' it should be 'A' or 'S'\n7. Move       'V'\n\nit should be same with the Left-Handed Person, like using (poiuy,lkj;h,/.,mn,[],-09876) keys on the Right Hand, and using mouse with the Left Hand. \n\nSo, KDE must look at human approach for doing quick and efficient work."
    author: "Fast_Rizwaan"
  - subject: "Re: Human Interface!!! Shortcuts are a pain in KDE"
    date: 2005-04-07
    body: "When you right click on the desktop, why don't you select the 'P' option with the mouse?\n\nI'm left handed, and I don't like the idea of having a different set of shortcuts to a right handed person. Anyway, not every keyboard is qwerty, and some might have keys on the other side.\n\nPersonally, I don't like using the keyboard at the same time as the mouse - I prefer to use one or the other. That's one of the reasons the one button mouse on the Mac so annoying. You can easily replace the one button mouse on Mac OS X, but there is no control panel setting to make it left handed. I've downloaded a suitable Macally mouse driver, but now in order to activate a RMB option (ie the left button for me), you have to click the other mouse button - yuck! KDE 3.4 is more usable than Mac OS in that respect, you can switch the mouse to left handed with a standard control panel option.\n\nEither I'm in double handed touch typing mode, or I'm using the mouse. I copy and paste by selecting with the left mouse button, and selecting a copy or paste menu option with the right - I very rarely use cntrl+c or cntrl+v."
    author: "Richard Dale"
  - subject: "Re: Human Interface!!! Shortcuts are a pain in KDE"
    date: 2005-04-08
    body: "Since you are left-handed it's pretty obvious why you only rarely use ctrl+c/v. You'd either have to move your left hand from the mouse to the keyboard or you'd have to move your right hand to the left side of the keyboard. Both is impractical.\n\nBut I as a right-handed person often use ctrl+c/v because it's extremely easy for me since my left hand anyway stays over the left side of the keyboard when I grap the mouse with my right hand. I don't even have to look on the keyboard to press ctrl+c/v. So for me this is very practical. But to be honest most of the time I simply paste the selection with the middle mouse button. Moreover, I never use the keyboard shortcuts to access an option of a context menu which I have opened with the right mouse button. Thus I don't really see the problem of the OP."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Human Interface!!! Shortcuts are a pain in KDE"
    date: 2005-04-08
    body: "Couldn't you just middle click and save yourself some trouble?\n\nAlso:  The ctrl-insert copy shift-insert paste shift-delete cut combo works better for left-handed people ;-)"
    author: "kundor"
  - subject: "About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "I was wondering how KDE was going to handle the task of migrating to QT 4.0. To me this seems like a huge task, as QT 4.0 realy breaks away from QT 3.X in its design philosophy in several key ares like threading and QOBject issues, seperation of GUI and non GUI QT libraries, QActions and on the SQL side. \n\nI know QT 4.0 is supposed to be backwards compatable in theory, but not sure how it works in the real world.  Early reports for the QT dev community seem to imply that it is not easy. Also, reading the email of the QT 4.0 dev forum shows this code to be really  an alpha release and not a beta.  This is not a knock on the Trolls, just a realization of how much the code has changed\n\nSo how will KDE handle this migration.  I think KDE should wait a bit for QT 4.0 to mature, I mean it took three releases of QT 3 to get it where it is now.  \n\nOne area I would like to see KDE apps improve upon is utilizing QT 4.0 multi treaded implimentation. Would be especially valuable on new multicore CPU's almost upon us. Also it wouuld be nice to utilize QT's new Render Engine, but this would be a huge rewrite for applications like KOffice, would it not.\n\nKDE might even think of splitting away from QT at this point as it would allow the to control their own destiny. \n\nI hope with 4.0 QT and KDE can settle down a bit, so that commercial apps can eventually be seen for KDE.  Right now I think KDE is a bit to dynamic for any commercail company such as the makers of Act, and Quick books to even contimplate migrating to.\n\nAlso it would be nice if KDE could embrace buisness a little more, to get a company like IBM, HP or Sun behind it to help get KDE more acceptable for business and enterprize markets.  \n\nI love KDE, and I am part of a team trying to develope a business application written in QT 3.3.  The more we look at KDE the more we feel that this really should be the preferred platform, but to do so is a trendous risk because we are so scared of the API being changed so much.  Also to really integrate our application with KDE takes a tremendos  amount of resources to train a programmer, as the documention and tutorials are so lacking.  DCOP, Kparts, are really wonderful but what good are they if nobody can use them.  If you look at any of the freeware apps being made nobody seems to be using them. Could it be that nobody needs them or they are just to hard to learn with no documentation.  \n\nKDE is a great product, and I thank the volunters who have made it so.  I raise these issues and concerns as I realy want KDE to be succeed even more.\n\n\n\n"
    author: "sparky"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "\"KDE might even think of splitting away from QT at this point as it would allow the to control their own destiny. \"\n\nWithout Qt, many of KDE's advantages would vanish. I think Qt is the way to go. It's clearly the best. And KDE chooses only the best."
    author: "Alex"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "\"I know QT 4.0 is supposed to be backwards compatable in theory, but not sure how it works in the real world. Early reports for the QT dev community seem to imply that it is not easy.\"\n\nDuh, that is why it's Qt 4.0! If it was really backwards-compatible, it would be Qt 3.x!\n\n\"it took three releases of QT 3 to get it where it is now.\"\n\nWell, it took four releases (3.0, 3.1, 3.2, 3.3) ;). But that doesn't mean that 3.0, 3.1 and 3.2 were crap. Of course 3.3 is better than they are, but that's how it usually works in software. Newer versions are usually better than old versions. Qt 5.0 will propably be alot better than Qt 4.0, should KDE wait for 5.0 instead?\n\nAs to migration to Qt 4.0. They are not migrating yet. They are making plans for it, but very little code has been written AFAIK. And it's a good thing to start planning this early, instead of late. Move to 4.0 will be quite big and it takes time to do it right. Even if Qt is still Alpha-software, they can already see how it does things and what it offers and plan accordingly. But doing a full-blown migration right now would be too early. And they are not doing that. It's still long way off.\n\n\"One area I would like to see KDE apps improve upon is utilizing QT 4.0 multi treaded implimentation. Would be especially valuable on new multicore CPU's almost upon us. Also it wouuld be nice to utilize QT's new Render Engine, but this would be a huge rewrite for applications like KOffice, would it not.\"\n\nWell, I think the plan is to take advantage of thed new features 4.0 offers. If they didn't, what would be the point to migrate to 4.0?"
    author: "Janne"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "> I love KDE, and I am part of a team trying to develope a business application written in QT 3.3. The more we look at KDE the more we feel that this really should be the preferred platform, but to do so is a trendous risk because we are so scared of the API being changed so much.\n\nKDE provides backward-compatibility for its API for a major version. So an application written for KDE 3.0 would still work with KDE 3.4. KDE 3.0 was released in April 2002. Approximate 4 years (KDE 4 is at least one year away) of API stability sounds good to me. :)\n\n> Also to really integrate our application with KDE takes a tremendos amount of resources to train a programmer, as the documention and tutorials are so lacking. DCOP, Kparts, are really wonderful but what good are they if nobody can use them.\n\nThere could of course always be better documentation, but IMHO KDE's API documentation (see api.kde.org) is already pretty good. And there are tutorials on developer.kde.org for the major KDE technologies. Also with the KDE applications you have good code examples how to use the API. \n\nWhat else do you need?"
    author: "Christian Loose"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "> Approximate 4 years of API stability sounds good to me.\n\n10 years would sound even better. And that's about what commercial environments provide.\nSo if you develop an application today, how long will it work? Quite some while Qt3-libs and KDE3-libs will be shipped together with KDE4. But a currently developed application should work for longer than only 2-3 years..."
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "AFAIK you could still ship the Qt3 & KDE 3.x libs with your app. It's like shipping the VB3 runtime dll. ;-)"
    author: "Christian Loose"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "That's what most commercials currently do - they link statically :-(\n"
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "If you have the source, you are good for 10 years. That's the huge business advantage that Free Software offers.\n\nCommercial environments don't provide 10 years of support. For instance, I was doing y2k work on a NeXTSTEP system developed in 1992/93 and a lot of the small companies selling NeXT software had either gone out of business by 1999, or were bought up by Sun and no longer sold NeXT software. If you only have a binary for your date selection widget or whatever, you are stuffed. You can't verify if it is y2k compliant, and you can't port it to a more modern architecture."
    author: "Richard Dale"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "if you develop for qt 4.0/kde 4.0 now (well, within a year, and you can also influence the current kde 4.0 development, a nice + over non-free software), assuming kde 5 will also be able to do kde-4 compatibility, you'll be able to use it for at least 6 or 7 years. then, you can always still use the old qt version (ship the lib with it)."
    author: "superstoned"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "> if you develop for qt 4.0/kde 4.0 now\n\nNobody can develop for KDE4 _now_. It's only possible to start with it in about a year. And interest in influence of KDE develop may be _very_ low. So what to do when one has to develop _now_?\nThe best solution so far is static linking.\n"
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "KDE3 libraries (not the entire set of applications and whatnot, just the libs) will likely ship alongside KDE4 installs for quite some time. KDE releases are parallel installable which offers a good amount of flexibility for supporting apps written against older releases.\n\nthe challenge comes if you are trying to provide closed source COTS software, as the desktop environment on Linux still isn't well enough defined to really do so with complete confidence, which is why most seem to be sticking to Qt or Gtk only right now."
    author: "Aaron J. Seigo"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "But 10 years isn't really something most commerical software can aspire to either. Most Win2k programs won't run on Win95, and that's about 6 years diff (roughly '94 to 2000). You're really getting into the heavy duty HW/SW when you start peeking out over 10 years. I suspect the majority of people that KDE appeals to are those who have 5 year or shorter life expectancy for their software. It's a disposable world, and it's not KDE that aggravates this thinking. Anyway, like it's been stated elsewhere in this thread, being FOSS gives anyone the opportunity to just build the necessary stuff into their own app or link it if they are FOSS scared and distribute the binaries. And KDE is written to allow older libs to co-exist, so apps can likely assume at least one major release backwards compatibility. Not too shabby a design overall, and definitely will support 98% or more of the population that cares. Or not?"
    author: "Pete"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "Most Win2k applications do run on Win95. But the really important fact is, that nearly all Win95 applications run on Win2k and WinXP. And they most propably will run on the next Windows version too.\nI agree that FOSS has huge advantages, but stability of API currently is not one of these. Currently most distributions do not ship with KDE2 libraries anymore (at least they are not installed by default). And KDE2 is \"dead\" for 3 years only. So an application build and shipped on top of KDE2 just 3.5 years ago does not work anymore."
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "That most distributions don't ship with KDE2 libraries anymore is hardly our fault. All PC games include the necessary DirectX libraries. So why shouldn't a KDE 2 app include the KDE 2 libraries (and the Qt 2 libraries)?\n\nMoreover, all security fixes are still ported to KDE 2.2.2. So KDE 2 is even still supported to a certain degree.\n\nAnd, last but not least, until not too long ago I still ran a KDE 1 application. And I didn't even have to install KDE 1 since I still had it from a very old installation.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "> That most distributions don't ship with KDE2 libraries anymore is hardly our fault\n\nIt does not matter who's fault it is. Fact is, that there are no KDE2 libs.\n\n> why shouldn't a KDE 2 app include the KDE 2 libraries (and the Qt 2 libraries)\n\nWhat's needed then, would be a mechanism (installer) that looks if the libraries are installed already or not. But yes, that would be an option. The more easy way is to link static, and that's what's currently done mostly :-(\n"
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: ">> That most distributions don't ship with KDE2 libraries anymore is hardly our fault\n>It does not matter who's fault it is. Fact is, that there are no KDE2 libs.\nThe reason no distribution ships the KDE2 libs are the fact that there are no need. I don't think there exist one KDE2 application worth any attention, who are not ported to KDE3 or have an equivalent application for KDE3. Making the need for KDE2 libs rather small."
    author: "Morty"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "> I don't think there exist one KDE2 application worth any attention\n\nApplications written for some special tasks may not be known by many people. But there may exist more of them, if the API would be available for longer than a few years. I guess that's one reason why we see only a few commercial applications on Linux.\n"
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "Some special task applications may exist, on the other hand I'd guess the support departments of the distributions would be among the first to hear about any problems in this regard. Leading to inclusion of the older libs for backwards compability, afterall they have done this for years with glibc.\n\nAs for your fear this leads to fewer commercial applications, the way commercial application vendors work it should actually work the other way. Since it's a way to force the users into an upgrade cycle, generating even more revenue. The only commercial companies who don't love such a schem are the game company's."
    author: "Morty"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-11
    body: "Why people don't understand that when it comes to so particulary product and so big corporations that could develop their own apps, in an open source environment they could do whatever they wnat. They are not tight down to binary compatibility, they have full access to sources, they can provide KDE2 libs by their own, or statically compile them into their apps. There's no problem at all.\n\nToo many people confuse every-day software with specific-task software. They are *completely different* approaches, you can't apply the same development mode to them."
    author: "Davide Ferrari"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-19
    body: "IMHO, if there are very few commercial applications under Linux, this is:\n* because the percentage of computers running Linux is small\n* because the shift is threatening companies\n* because their employees are not used to use it\n* because they don't have it at home\n* because they don't have the choice to refuse EULAs, get refund and buy an easy to install distro with support \n* because OEM softwares undoubtedly constitute \"tying\" -- at least according to French laws -- but the law is not applied when it comes about OEM softwares.\nUnfortunetly, I don't have the answer to \"why do the French executive power do not applied the law to protect consumers and French software companies (such as Mandriva)?\"\n\n$ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ $ \nhttp://wiki.aful.org/GdTDetaxeEnglish"
    author: "Futal"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "I still don't see the problem. If your app only works with KDE2 then just ship the libraries with your app. You don't have to statically link against them.\n\nI mean KDE breaks its backward-compatibility about as often as Qt and there are a lot of commercial Qt apps out there. So why shouldn't this also work for commercial KDE apps?"
    author: "Christian Loose"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "Indeed, a Photoshop Album user doesn't even know that they're using Qt."
    author: "Ian Monroe"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-08
    body: "> just ship the libraries with your app. You don't have to statically link against them\n\nWhat's the benefit of shipping the shared libraries over shipping them static linked?\n\n> there are a lot of commercial Qt apps out there\n\nAnd most of them have Qt static linked :-("
    author: "Birdy"
  - subject: "Re: About Qt 4.0 and a ramble"
    date: 2005-04-07
    body: "> Also it would be nice if KDE could embrace buisness a little more\n\nit's coming =)\n\n> as the documention and tutorials are so lacking\n\nwithin the general KDE project we've identified this (and a few other line items) as being areas we need to address. something a few people are working on is \"KDE U.\" which is a task oriented body of example driven tutorials that arranged in a graduating stack from \"the 101s of KDE dev\" through to the advanced topics such as \"tieing KParts together using DCOP\".\n\n> If you look at any of the freeware apps being made nobody seems to be using\n> them\n\nthat's not really accurate. what is accurate is that many free software apps are not very complex and so have limited need for things like KParts. but there are a LOT of open source apps that do use KParts, and particularly DCOP. as you move to more commonly useful technologies, such as XMLGUI, many more use those. it's just a matter of what the needs of the app are."
    author: "Aaron J. Seigo"
  - subject: "My proposal"
    date: 2005-04-08
    body: "KDE needs something like Java or Mono.\n\nWhy not take D http://www.digitalmars.com/d/\n\nand create a qt class library for it."
    author: "hannes g\u00fctel"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "Why? I'm not saying it would not be nice to have, but it does not bring anything new. KDE already has mature and stable bindings to two world class higlevel languages, they may not have the hype of Java and Mono, but Python and Ruby are just as capable. \n\nNever heard anyone contemplating bindings for D before, so why don't you give it a try and make some nice Qt/KDE bindings. For what I know KDE may become the killer environment for D programming.  "
    author: "Morty"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "KDE has java bindings. I've regenerated them for every KDE release since about KDE 2.1. Just that everyone forgets about them.\n\nOn the other hand, python and ruby are more suitable languages for RAD development. \n\nI just can't see the point of writing bindings for a statically typed systems programming language like D or C#. They're pretty much as complicated as programming with the Qt variant of C++ that KDE uses. Qt adds so much convenience features to vanilla C++ that it's effectively another language. About the only thing that C# or D would add is automatic garbage collection. Qt mostly manages memory by having the QObject tree, where a parent widget will delete its children, so memory management isn't too much of a headache anyway."
    author: "Richard Dale"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "As an anonymous fan of yours :-), did you ever find a job?  Hope you can continue your excellent bindings work."
    author: "Anonymous"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "\"did you ever find a job?\"\n\nI haven't got around to looking yet. I'm suffering from 'envelope opening phobia' at the moment because of a mental block I've got with the tax people. So that is completely preventing me from doing anything until I get over it, and successfully shut my company down.\n\nAnyway, apart from that I'm very proud of the bindings work and thanks for the complement."
    author: "Richard Dale"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "Good news, a friend came round this afternoon and opened my mail for me - things are not as bad as I thought.."
    author: "Richard Dale"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "I don't forget about them! It's just that after a hard day hacking Java at work, C++ is kind of refreshingly honest :-). But combine the java bindings with gcj and you've got a very nice platform to create binaries in almost the same way as with C++ but without the memory management headaches. The only thing missing is good automake/autoconf integration."
    author: "Boudewijn Rempt"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "\"The only thing missing is good automake/autoconf integration\"\n\nLast time I tried, the KDE version of libtool didn't work with gcj - it tries to use some compile option which doesn't exist. Also the build of kdebindings should use gcj by default rather than a Sun jvm. But I really hate going near that autoconf stuff."
    author: "Richard Dale"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: ">Just that everyone forgets about them.\nSorry,seems like I'm one of those. Or perhaps more precisely, all the \"hype\" of the Python and Ruby bindings overshadow to the point I ignored them. That's sloppy of me, when the Java bindings are the ones with the longest history of being kept up to date. \n\nAnd the language bindings pages on developer.kde.org are not very clear, it's hard to find out the status of the bindings from there. I'd like too see a common status page describing both status of the bindings and perhaps which/number of classes supported in the different modules. A page to point people when they inquire about bindings."
    author: "Morty"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "It shouldn't depend on one person, ie myself, to make everything happen. Other people can join in and make gcj build the java bindings, update the developer's corner site, improve KDevelop java support and so on. A lot of it isn't that hard. A self sustaining KDE java community hasn't formed - I don't know why. There is a similar problem with the java-gnome bindings, so as far as I can see it isn't to do with me personally.\n\nOften people write huge essays on 'what's wrong with this or that feature of KDE', when they could have easily written a tutorial and actually improved KDE with the same amount of effort.\n\nThe barrier to entry for the KDE project is surprisingly low, and I don't think most people realise that. It is easier than you think to make a difference. I remember when I got my first KDevelop patch accepted - it was really exciting. But I had been making a global trading system more reliable as my day job, which is seriously difficult. I actually got more of a buzz from fixing a KDevelop C++ class parser problem, than getting a Reuters Triarch real time feed fixed. Shame about the difference in the money you get paid :).."
    author: "Richard Dale"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "So true, and when thinking on it I'm puzzled why the Java bindings haven't generated more traction, leaving everything to you. Since you say the Gnomes have the a similar situation, I'd guess the Java coders are lost somewhere out in the lands of AWT and Swing. And any developer coming this way either go native with C++ or more RAD like with Ruby/Python, leaving Java in some kind of no-mans-land.\n\nGetting the first patches accepted are exciting, and as you point out not at all difficult. There are lots of small quiet places in the KDE code where one can make a difference, there are no need to start with fixing Konqueror or some of the big complex parts. Not that I will call myself developer, but I'm getting rather good at if'ing:-)"
    author: "Morty"
  - subject: "Re: My proposal"
    date: 2005-04-08
    body: "My experience closely resembles yours.\n\nStill, I can think of a few reasons qt-java hasn't taken off -- the main gcj/classpath hackers are very gnome oriented. Mark Wielaard, the classpath maintainer, has been a close colleague of mine for about five years. He's always used Gnome, and uses gnome-java for his gui hacks. Gnome-java has more buzz among the java-interested... Of course, when you're using Gnome, Java feels like a godsent. And even so, they're so busy with Swing, Awt, Cairo, SWT and whatever...\n\nAnd with qt-jave or C++/Qt, there's no so much difference. 'm not singularly more productive either in Java or C++ _if_ I'm allowed to have Qt with my C++. Without Qt, productivity goes down the drain. In fact, I'm surprised myself at the amount of code I've been able to shift for Krita. Most of it is crap -- the good bits have been delivered by the other Krita hackers -- but it's been a lot of code.\n\nI have always been even more productive with PyQt than with either Java or C++/Qt, but Python is not on for my day job, and it's simply not appropriate for a paint application. PyQt/PyKDE is great for things like email clients, home-banking apps -- all the gui things where the standard widgets fit your needs.\n\nBut in the end, it's as you say -- helping out with a manual, a tutorial, a clever hack, an intelligent bug report or even the offer to test three broken versions of Krita on your specific installation is surprisingly easier than moaning and whining, takes less time and is more rewarding."
    author: "Boudewijn Rempt"
---
<a href="http://thelinuxbox.org/">The Linux Box</a> has interviewed <a href="http://aseigo.blogspot.com/">Aaron Seigo</a> on their <a href="http://thelinuxbox.org/index.php?subaction=showfull&amp;id=1112764056">latest episode of The Linux Box Show</a>.  He discusses <a href="http://dot.kde.org/1112736422/">Appeal</a> and the plans for making KDE 4 the leader for usability, development and cool eye candy.  Specific topics he covers include KControl, package management, KOffice and using high level programming languages.  Start 5 minutes in for a brief history of KDE and 10 minutes in for the interview.  "There's not going to be anything cooler than an interview with Aaron Seigo".
<b>Update:</b> Aaron provided us with a <a href="http://aseigo.bddf.ca/?pID=1221">transcript<a/> of the interview.

<!--break-->
