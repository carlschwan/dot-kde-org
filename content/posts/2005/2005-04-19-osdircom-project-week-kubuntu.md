---
title: "OSDir.com Project of the Week: Kubuntu"
date:    2005-04-19
authors:
  - "dmolkentin"
slug:    osdircom-project-week-kubuntu
comments:
  - subject: "Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Hi KDE-people,\n\nI have noticed that there is quite much Kubuntu-promotion here on KDE-news. So I have tryed and compared it to my favorite Distro Kanotix  http://www.kanotix.de which also ships KDE 3.4 . Compared to Kanotix Kunbuntu is slow, Installation is complicated and very slow. It could handle two CD-Drives and took me more than our. Luckily at a day later Kanotix with KDE 3.4 came out, so I switched back. It recognises all my hardware without problems. Boots in less then 15 secounds and the installation of the whole system took a little more than 10 minuites. So why all this Kubuntu-hype? Would it be fair two anounce also other distros (not only Kanotix of course) which ship with KDE?\n\nMark"
    author: "Mark"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "\"why all this Kubuntu-hype?\"\n\nBecause Ubuntu (and Kubuntu) are the \"talk of the town\" right now. Both are rapidly increasing their userbase and both are progressing at a fast pace. It seems to me that Kanotix hasn't made any huge splash in the Linux-community. It's just YALD.\n\nAs for me, I use neither."
    author: "Janne"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Hello Mark, \n\nI agree, Kanotix is nice, too.  I think the reason why there is an article about Kubuntu but not about Kanotix is that there are some KDE contributors involved in Kubuntu development (well, that and the current hype about (K)ubuntu).  So I see this more as a question of awareness than one of discrimination (or whatever you'd like to call it).  \n\nWhy don't you write a nice article about your experiences with Kanotix and the KDE 3.4 in it?  I'm sure it won't be turned down by the dot editors (maybe contact them first to explain what you're planning...).  \n\n"
    author: "cm"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Yes, Kanotix is awesome.  It's the best OS (Linux or otherwise) I've ever used.  It's the one that I promote.  \n\nI think that is the key... promotion.  Keep telling people about it, post about it on your blog, write reviews for it, link to it on your website.  If you listen to any Linux Internet radio shows, ask them to do a review of Kanotix.  What ever you can think of doing to get the word out, do it.\n\nAlso, don't forget to send Kano a little donation to show your support as well. "
    author: "David"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "> Would it be fair two anounce also other distros (not only Kanotix of course) which ship with KDE?\n\nThis story is not an announcement, it's an interview. If you have something nice about how KDE is integrated in a distribution, how it is modified or setup then you can click on the \"contribute\" link in the top left."
    author: "Anonymous"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "> Would it be fair two anounce also other distros (not only Kanotix of course) which ship with KDE?\n\nOf course, but we don't get many stories from distributions.  I've never heard of Kanotix and their website doesn't explain much (at least on the English language page).\n"
    author: "Jonathan Riddell"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Kanotix is Knoppix and Debian/sid based, therewith includes a rather uninteresting outdated KDE 3.3.2. Kubuntu on the other side is one of the first distributions including KDE 3.4."
    author: "Anonymous"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Just to set the record straight....  Kanotix's latest release 2005-02 includes KDE 3.4  I've been using it for a couple weeks now and it's really great.\n\nThis information is available on the Kanotix homepage.  Just read the latest announcements.\n"
    author: "David"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "I read http://distrowatch.com/kanotix which names KDE 3.3.2 in the latest listed release.\n\nAnd my understanding was that Kanotix is similar to Knoppix but being a pure Debian/sid instead of a mixture of sid and packages from elsewhere. This doesn't seem to be true anymore then as according to http://packages.debian.org there is no KDE 3.4 within sid."
    author: "Anonymous"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "But obviously some people use it with 3.4 packages.  And it has a nice live CD with a working HD install.  I don't see why an article or interview about it would be uninteresting to the dot audience if someone interested in the distro were to write one. \n\n"
    author: "cm"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "\"Kanotix...includes a rather uninteresting outdated KDE 3.3.2.\"\n\nReally? The release notes say it has KDE 3.4.0.\nhttp://kanotix.com/files/kanotix/"
    author: "mmebane"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "> I've never heard of Kanotix and their website doesn't explain much\n\nI just found this mini-review of Kanotix on distrowatch:\nhttp://distrowatch.com/weekly.php?issue=20050418#review\n\n"
    author: "cm"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Kanotix comes with a beta of kde 3.4, not the final version. Check the package version lists on the webpage."
    author: "Anonymous Coward"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "Actually the big story here is about Ubuntu chose to ship with only Gnome, and people stepped forward to put together a KDE option. Interestingly the KDE option has garnered as much interest and demand as the original Gnome only setup.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-19
    body: "That's because, as we all know, KDE is a better desktop manager... right? ;o)"
    author: "David"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-20
    body: "For my purposes it is. Others may choose Gnome or other DM's.\n\nThere are continual attempts to 'rationalize' free software, meaning cut out duplication. This comes from business types who would like to harness the work of others for their profit. Anyone is free to do that, but to suggest that everyone should work for them without being paid is rather presumptuous.\n\nSo we have kde/gnome gtk/qt, mysql/postgresql, php/perl/python, etc. Even this short list illustrates different software/libraries that serve specific interests or needs. Guess what. People have different subjective preferences. So there is free software that suits almost everyone.\n\nI'm always pleased when these efforts to 'simplify' fall absolutely flat in failure. In the end we have better software that serves our needs far better than a single project ever could.\n\nIt goes both ways. Knoppix was originally KDE, due to the personal preferences of the fellow who put it together. And Debian. The Gnomes put together Gnoppix. Other distros decided to get into the game, so almost everyone has live cd's now. And remarkably, it has become reasonably easy to put together a live cd that suits the purpose of one or a few people. This is good.\n\nA vibrant and healthy economy, ecosystem, community, or whatever is always confusing, noisy, with the odd bit of yelling, but mostly everyone going about benefitting themselves and others by their work. Free software is like that. It is healthy.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Why only Kubuntu-promotion?"
    date: 2005-04-20
    body: "The guy or one of the guys who did gnoppix was hired by canonical, they are now working on Kubuntu.\n\nI personally like both Gnome and KDE, in many ways, the interface to Gnome apps are clearer.  Though, when I need to get something done some of them really fail me, because I usually break the \"average\" use case and the GUI doesn't facilitate that. In KDE, the interface can overload you, with too many things squished together, I don't necessarily mean spacing, I mean disparate things put together for no reason other than supporting some features.  I'm all for features, as long as they're grouped logically around their inter relationships and workflows.  It'd be interesting to break down Amarok to see what lessons can be taken away from that, I've never found an app so insanely useful after honestly 2 minutes of fiddling with it, the feedback was amazing, I was lost for about 10 seconds and then as soon as I tried doing something it was entirely helpful at convey what's next or possible."
    author: "Saem"
  - subject: "kuser"
    date: 2005-04-19
    body: "I read \u00abGenerally though, we need to work on system tools to get a better package manager, update notifier and system configuration\u00bb : IMHO one big problem is that kuser (under kubuntu) can't create a new user without crashing before..."
    author: "Capit Igloo"
  - subject: "Re: kuser"
    date: 2005-04-19
    body: "That's fixed for KDE 3.4.1."
    author: "Anonymous"
  - subject: "Re: kuser"
    date: 2005-04-19
    body: "Ok, I don't find the bug report myself so thanks for you reply (http://bugs.kde.org/show_bug.cgi?id=101974 is too general, perhaps it's totally an another crash problem).\n\nCan we expect that KDE 3.4.1 will be an official update to Kubuntu Hoary ?"
    author: "Capit Igloo"
  - subject: "Re: kuser"
    date: 2005-04-19
    body: "> Can we expect that KDE 3.4.1 will be an official update to Kubuntu Hoary ?\n\nIt's on my todo list.\n"
    author: "Jonathan Riddell"
  - subject: "Re: kuser"
    date: 2005-04-19
    body: "> \u00abIt's on my todo list.\u00bb\n\nok, and thanks for all your very good work."
    author: "Capit Igloo"
  - subject: "Re: kuser"
    date: 2005-04-19
    body: "THAT'S great! I hate the fact most distributions don't offer these bugfix releases from KDE (and other project). I understand kde 3.5 won't be as a official kubuntu 5.04 package, but kde 3.4.x should be available, when you upgrade..."
    author: "superstoned"
  - subject: "kubuntu still gnomish"
    date: 2005-04-19
    body: "I like kubuntu but if I want to install mozilla-firefox, kubuntu wants to install me gnome-vfs and a whole lot of useless-to-kde gnome dependencies so I had to download it by hand. I guess firefox is not the only one there.  \nStill I love kubuntu it's really fast and beautiful (I love the kde theme).  I think  Mark was talking about the livecd, Kubuntu and ubuntu livecds are slow compared to knoppix and derivatives but I think that is being worked on, plus xUbuntus are not meant to be used as livecds any way."
    author: "Pat"
  - subject: "Re: kubuntu still gnomish"
    date: 2005-04-19
    body: "I submitted a bug report about this:\n\nhttps://bugzilla.ubuntulinux.org/show_bug.cgi?id=8680\n\nThere's no reason firefox must include all of these gnome dependencies. It's one of the things that will eventually get ironed out.\n\nStephen"
    author: "Stephen"
  - subject: "Distro Tools ?"
    date: 2005-04-20
    body: "I tried the live CD the other day, I was really impressed.\nKubuntu has great potential: large (and growing) community,\naimed at ease of use, great looks, fast, clean, simple. And it\nis on the bleeding edge of things :-) (a topic I am getting \na bit disappointed about with Mandriva, which IMHO should\nrelease a bleeding edge free distro every six months, and\na commercial, yearly distro + services for the corporate\nworld, but I disgress)\n\nThe two things it's lacking (as compared to other modern, \"easy\" \ndistros such as SUSE and Mandr(ake)iva is a graphical installer\nand distro specific tools, some sort of a control center. I know\nthat the graphical installer is scheduled for the next release.\nBut how about a set of configuration tools ?\n\nThank you Mark and the Kubuntu community. Cheers!"
    author: "MandrakeUser"
  - subject: "Re: Distro Tools ?"
    date: 2005-04-21
    body: "Kynaptic?\n\nIt already has a rather nice graphical installer.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Distro Tools ?"
    date: 2005-04-21
    body: "Well, yeah, kinaptic is really useful, but I was aiming at a graphical\ninstaller for the distro itself (when you install from a CD for the first time)"
    author: "MandrakeUser"
  - subject: "Re: Distro Tools ?"
    date: 2005-04-21
    body: "Isn't Mandriva (what a terrible name) targeting personal computer as much if not more then business?"
    author: "Ian Monroe"
  - subject: "Re: Distro Tools ?"
    date: 2005-04-21
    body: "Yep. Although they are trying to target also the business sector. \nThey already made some deals with government organizations \nif I am not mistakenm, at least in France.\n\nIn fact, that's what the purchase of Conectiva is all about. To \ntry to get into selling Linux services in Brazil, a Country\ngoing strongly into open source. \n\nOh well ...\n"
    author: "MandrakeUser"
  - subject: "Please, no configuration tools..."
    date: 2005-04-20
    body: "Hello everyone!\n\nI got Kubuntu yesterday, and in the past I used Ubuntu. I must say that I like the look of KDE 3.4 very much, it's kinda friendly and very impressive. And it's really clean, just like Ubuntu too. I wouldn't like it if they'd integrate config-tools like YaST or DrakConf or something like that, because it would kill the feeling of having a simple, well configurated and easy to configurating distro. Meanwhile I prefer configurating per configfile-editing, but who would do it like this further if there'd be a config-tool? Why I don't like such ****? Because it destroy's a lot of feeling, like I said a few lines ago... And YaST f.e. overwrites all configurations done by editing a config-file, and that's totally unreasonably. If I wanna have phun by using patronizing tools, I can switch back to windows... I wanna work with my OS, not only install-1stconfig-run run run..."
    author: "Olliander"
  - subject: "Re: Please, no configuration tools..."
    date: 2005-04-20
    body: "But say that the config tools were there (for people like me who don't \nhave time or don't like to edit config files). You could still configure\nthings by hand. Nobody would force you to use them. It is exactly this elite attitude what is preventing Debian from getting off the techi world. Kubuntu, \nI would expect, will help bring Linux to the masses. \n\nAs for the clean look, Ark Linux config tools are cool and qt based. In fact,\nI would think of a python, GUI agnostic set of tools. And two GUI's, a Gnome\nand a KDE based. There are pythong bindings for both. There :-)\n('course it is easy to say than to do :-) )\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Please, no configuration tools..."
    date: 2005-04-20
    body: "> YaST f.e. overwrites all configurations done by editing a config-file\n\nThat's an urban legend nowadays."
    author: "Anonymous"
  - subject: "Re: Please, no configuration tools..."
    date: 2005-04-20
    body: "Yast does not overwrite configurations."
    author: "Evan \"JabberWokky\" E."
  - subject: "It already has one: debconf"
    date: 2005-04-23
    body: "Debconf is the thing that asks questions upon installing a .deb package (although only some of the packages use it; xserver-xorg is one). See \"man dpkg-reconfigure\" and \"man 7 debconf\" for further details."
    author: "rqosa"
  - subject: "Only Debian based distro to install on Toshibas"
    date: 2005-04-20
    body: "I've got a Toshiba Port\u00e9ge 4010 and an Lucent WLAN-card. Up to now Kubuntu 05.04 is the only Debian based distro (and most anything else based either) that installed flawlessly out-of-the-box onto my laptop with networking via the PCMCIA WLAN-adapter and sound & screen working perfectly. \n\nNow that is a good reason to promote Kubuntu/Ubuntu in it self, but when you add to it a well packaged KDE & Debian APT's power in package management it really the best thing after sliced bread!\n\n++ Ray"
    author: "Raymond"
  - subject: "Weird Dependencies on Kubuntu"
    date: 2005-04-20
    body: "Hi,\nI just installed kubuntu.\nLooks nice, but when i try to remove unneccesary software with kynaptic or apt-get, I get weird dependencies:\n\nubuntu-base depends on mutt\nkdepim depends on kpilot\nkdenetwork depends on kopete\nkdegraphics depends on gwenview\nlinux-kernel depends on dash\nkdm depends on lipstick theme\nkate depends on esound\nkubuntu-desktop depends on kernel-headers.\n\netc. etc..\n"
    author: "ac"
  - subject: "Re: Weird Dependencies on Kubuntu"
    date: 2005-04-21
    body: "Um, kopete and kpilot are part of kdenetwork and kdepim, respectively. \n\nSome of those others are a little odd."
    author: "Ian Monroe"
  - subject: "Re: Weird Dependencies on Kubuntu"
    date: 2005-04-21
    body: "> kdepim depends on kpilot\n> kdenetwork depends on kopete\n> kdegraphics depends on gwenview\n\nkdepim, kdenetwork and kdegraphics are \"meta packages\" that are mostly empty and only have dependencies on other packages.   \n\nSo *installing* kdegraphics, for example, will cause all dependent packages (including gwenview in this case) to be installed without you having to install them all one-by-one.  That's a handy shortcut. \n\nOTOH, *uninstalling* kdegraphics does *not* mean uninstalling all the graphics-related KDE programs!   So uninstalling gwenview will cause the meta package kdegraphics to be uninstalled, too, but not in turn the rest of the graphics apps.  \n\n\n"
    author: "cm"
  - subject: "Re: Weird Dependencies on Kubuntu"
    date: 2005-04-21
    body: "_unless_ you use aptitude... it rocks"
    author: "Brammo"
  - subject: "Re: Weird Dependencies on Kubuntu"
    date: 2005-04-21
    body: "Did I understand you correctly, you're saying that aptitude also uninstalls dependencies when a meta package is uninstalled? \n\nI think in the case we have here the user actually *wants* to uninstall only single apps, not all dependencies of the meta packages.  \n\n"
    author: "cm"
  - subject: "Re: Weird Dependencies on Kubuntu"
    date: 2005-04-22
    body: "Ok, TNX !"
    author: "ac"
  - subject: "Kubuntu Review @ NewsForge"
    date: 2005-04-21
    body: "http://os.newsforge.com/article.pl?sid=05/04/19/200205&tid=2"
    author: "Anonymous"
  - subject: "Re: Kubuntu Review @ NewsForge"
    date: 2005-04-21
    body: "Nice review (although the point about rpm being a disadvatage is plain\nwrong, they probably never used urpmi, rpmdrake, apt-rpm, etc)\n\nThey also mention the point I made above, about the lack of config tools ..."
    author: "MandrakeUser"
---
<a href="http://www.kubuntu.org/">Kubuntu</a>, which saw its <a href="http://dot.kde.org/1112950808/">first release</a> in April of this year, is a highly integrated Linux distribution featuring the KDE desktop. <a href="http://www.osdir.com">OSDir</a> has declared Kubuntu "project of the week" and is <a href="http://www.osdir.com/Article5089.phtml">carrying an interview</a> with two of the project's primary contributors, Andreas Müller and Jonathan Riddell. They share insights on Kubuntu's creation, its current state and their plans for the future.



<!--break-->
