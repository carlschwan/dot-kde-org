---
title: "amaroK Live 1.3 Released with Open Music"
date:    2005-10-04
authors:
  - "tteam"
slug:    amarok-live-13-released-open-music
comments:
  - subject: "Comes with non-free software: MP3 and Flash player"
    date: 2005-10-04
    body: "It's a great concept, but unfortunately it comes with MP3 software (which is patent-encumbered in some countries) and appears to come with the proprietary Macromedia Flash player as well.  Perhaps there are other non-free programs on the CD I have not seen.  As a result, this is merely gratis software, not free software.\n\nHas anyone made a derivative of this CD that is strictly free software?  I'd be interested in evaluating that.  The remastering directions from the Amarok live CD site say that the scripts build \"a new iso image that doesn't boot\"."
    author: "J.B. Nicholson-Owens"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-04
    body: "It also says we're working furiously on a fix. Which we are, so stay tuned."
    author: "leo"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-10
    body: "and we now know what the problem is.  See the amaroK Live forum at http://amarok.kde.org or the amaroK Live site at http://amaroklive.com for an explanation and how to fix it."
    author: "oggb4mp3"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-04
    body: "Re-mastering isn't for adding and removing software regardless, AFAIK."
    author: "Ian Monroe"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-04
    body: "The re-mastering script I mean to say."
    author: "Ian Monroe"
  - subject: "Re: Comes with non-free software: MP3 and Flash player"
    date: 2005-10-04
    body: "Well, the only non-free software is the flashplayer, and we debated heartily about including it.  Since it was distributable, and many websites use flash nowadays, and since the user is very likely to surf while listening, we elected to include it for a better user experience.\n\nAs for the mp3 stuff, all the software that can decode mp3's is released under the GPL, so the software itself is free, and there is only 1 mp3 on the disk, which is the introduction by Bill Goldsmith from Radio Paradise.  Everything else is vorbis.  This file was provided to me as mp3, and in hindsight maybe I should have transcoded it to ogg.\n\nAnyway, it seems to be the general understanding that decoders are legal, it's the encoders that have a patent problem, and since we're not encoding, but decoding, I think no one will get arrested ;)"
    author: "Greg Meyer"
  - subject: "Fun with patents."
    date: 2005-10-04
    body: "> it seems to be the general understanding that decoders are legal\n\nThe same patent naturally covers both.  It's more that the patent holders basically aren't enforcing it for decoders.  That said, it was no accident (though I don't like it either) that both Redhat and SUSE have removed mp3 decoders from their distributions for legal reasons."
    author: "Scott Wheeler"
  - subject: "Re: Fun with patents."
    date: 2005-10-04
    body: "It seems to be the general understanding that Thomson and the Frauenhofer Institut don't want money from free software distributors.\n\nOnly redhat has removed mp3 support, Suse provides mp3 support as a download option. Nearly all other distributions ship with mp3 software - so it seems to be no problem to include mp3 support in the amarok live CD."
    author: "Hans"
  - subject: "Re: Fun with patents."
    date: 2005-10-24
    body: "Ubuntu doesn't ship with mp3 software either."
    author: "anonymous ;)"
  - subject: "Re: Comes with non-free software: MP3 and Flash player"
    date: 2005-10-04
    body: "<I>It's a great concept, but unfortunately it comes with MP3 software (which is patent-encumbered in some countries)</I><P>RTFGPL. The software simply can't be distributed in such countries, if that would make it non-free there. There's no way to bring free software to a non-free country.<P><I>and appears to come with the proprietary Macromedia Flash player as well. Perhaps there are other non-free programs on the CD I have not seen. As a result, this is merely gratis software, not free software.</I><P>The songs are not allowed to be commercially distributed, so there's no way you can have the cd be free without losing the whole point."
    author: "mikeyd"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-04
    body: "So OGG is not supported???"
    author: "ehemm"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-04
    body: "Yes it supports ogg.  Everything on the CD is ogg.  \n\nOgg is the container, vorbis is the format."
    author: "Greg Meyer"
  - subject: "Re: Comes with non-free software: MP3 and Flash pl"
    date: 2005-10-04
    body: "\"Ogg is the container, vorbis is the format.\"\n\nYou probably mean: \"Ogg is the container, vorbis is the codec.\""
    author: "dina"
  - subject: "Rating music in amarok?"
    date: 2005-10-04
    body: "Now that I am listening to magnatune tunes with amarok\nI would like to be able to rate the music in amarok so that I know which song I liked and how much.\nWould that be an interesting feature ? is that doable ? I know that amarok stores already statistics about songs but, to my knowledge, without user inputs. Now amarok may also take user inputs  a bit like google mail and its star system.\n\n\nThe rationale behind it is that  as you listen to new (unknown) music, say from magnatune, you may not recall which one you liked best. Being able to quickly rate the song will allow a quick sort on the preferred song and then allow to buy them from magnatune or other record label."
    author: "Christophe"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-04
    body: "Amarok tracks ratings automatically based on how often you start a song, how often you listen to it all the way through, and how often you skip it, which usually gives an indication of how well you like it.  You can manually edit Amarok's score values by right-clicking the playlist headers, enabling \"Score,\" then right-clicking the score value for any track and editing."
    author: "kundor"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-04
    body: "Regarding the automatic statistics I agree that they provide some kind of information how much you like a song.\nI don't think editing the score by hand from 0 to 100 is the best that can be done, at least visually looking at a numeric value is not that appealling.\nThe score(0-100) could be viewed as its value (now) or as 0 to 5 stars as an approximation and give the users an immediate feed back about the song.\nThe star system could then give a quick estimation of the song score from the user point of view.\n\nScore 0 - 0 star\nScore 1-20 1 star\nScore 21-40 2 stars\nScore 41-60 3 stars\nScore 61-80 4 stars\nScore 81-100 5 stars\n\nThis way you can mark songs say from magnatune and perhaps then buy the preferred ones.\n\nNow it is just an idea. I am already very happy with amarok :)"
    author: "Christophe"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-04
    body: "The scores are listed as stars in the home tab in the context sidebar. (At least in 1.3 CVS from 2-3 days ago)."
    author: "freqmod"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-04
    body: "There has always been the five star thing in the context browser. That is since that theme was created. The five star is just hiding the regular bar graph with some CSS."
    author: "madpenguin8"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-05
    body: "That's something that kind of bugs me - the fact that deciding you want to listen to something else right now is treated the same as skipping a song because you don't want to hear it.\n\nThere doesn't seem to be any way of stopping one track and starting another without it affecting its rating, and manually editing it misses the point.\nMaybe there needs to be a \"skip because I don't like it\" button, rather than just assuming that not listening to something all the way through means you don't like it.\n"
    author: "mabinogi"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-07
    body: "\"Maybe there needs to be a \"skip because I don't like it\" button, rather than just assuming that not listening to something all the way through means you don't like it.\"\n\nso amarok shouldn't assume that you don't like a song, if you actually don't like it? ;-)\n\ncheers,\nmuesli"
    author: "muesli"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-07
    body: "This gets said often, but frankly, if you don't want to listen to something right now, then you are saying \"I don't like this as much as this other song right now\" and it adjusts the score accordingly. The adjustment is a statistic so if this is only a temporary dislike for that track, with time, the score _will_ be accurate.\n\nThe automatic system will not reflect how much you like everything instantly, but with time, it does work."
    author: "Max Howell"
  - subject: "Re: Rating music in amarok?"
    date: 2005-10-10
    body: "Great point Max.  Again, let's not confuse a user rating system, which amaroK does not have, with a statistical scoring tool that tells the listener what their preferences are."
    author: "oggb4mp3"
  - subject: "Re: Rating music in amarok?"
    date: 2006-07-27
    body: "in Amarok (1.4.1):\n\nFor 5 start system: <settings><onfigure amarok> then click <use ratings> in Components section.\n\nOr unclick for original ratings system and just add to view by right-clicking column headers in main window.  "
    author: "Claypole"
  - subject: "temporary mirror"
    date: 2005-10-04
    body: "until the bandwidth is gone\n\nhttp://mirror.fizzelpark.com/amarok\n\nlocation: cologne, germany"
    author: "bangert"
  - subject: "ITunes"
    date: 2005-10-04
    body: "From my point of view ITunes is the better player. It's usability is great. I hate apple fanatics but this program is as simple as it should be. Way to go for Amarok."
    author: "ehemm"
  - subject: "Re: ITunes"
    date: 2005-10-04
    body: "I tried ITunes a while back - After being an Amarok user for a long time.\nI honestly can't see what all the fuss is about. I have heard everybody raving about iTunes and how good it is. Am I missing something....\n\nWhat does iTunes specifically do better than amarok"
    author: "Danni Coy"
  - subject: "Re: ITunes"
    date: 2005-10-04
    body: "\"It's usability is great.\"\n\nIMHO its 'usability' only appears great because of lack of features and configurability.\n\n\"Way to go for Amarok.\"\n\nWeren't you just saying iTunes was better?  You confused me by this."
    author: "Corbin"
  - subject: "Re: ITunes"
    date: 2005-10-04
    body: "If you want something simple, try JuK instead and don't look at amaroK. amaroK is a feature packed audio player, that's always been the intention. No way the developers are going to remove all the features."
    author: "Bram Schoenmakers"
  - subject: "Re: ITunes"
    date: 2007-04-13
    body: "I installed iTunes once on Windows a while ago.  I removed it immediately when I found out that the gui button in the upper right *actually opened my cd tray*.  That was indication enough to me that these people didn't have a clue about music on computers."
    author: "Zack Glennie"
  - subject: "privacy options"
    date: 2005-10-05
    body: "i would love to be able to turn off all inet activity of amarok \nusing simple option."
    author: "alekibango"
  - subject: "Re: privacy options"
    date: 2005-10-07
    body: "just don't download any covers, disable last.fm support... and that should be it. in which way we infrige your privacy is still puzzling me, though.\n\ncheers,\nmuesli"
    author: "muesli"
  - subject: "Patents huh"
    date: 2005-10-08
    body: "Personaly i dont care about them. I own all my mp3s and they took me 5years to compile into mp3s. (records etc) Personaly I am discusted at how many people actualy care anymore. Oh we have a monoply now we want to kill you all. One day patents will choke everything including cars food software hardware. Laser pens for entertainment and betting Yes I patent infringed but I dont care any more. What about the game patent etc. They thought piracy is a problem. Can i patent a virus that steals money from patents being approved or has someone already done that.\n\nMicrosoft are laughing at us all cant you tell."
    author: "storm"
  - subject: "Amarok"
    date: 2005-10-09
    body: "amaroK is a fantastic feature-rich player that I personally love\n\n\nnow I can promote this anywhere, great work!"
    author: "w7"
  - subject: "Re: Amarok"
    date: 2005-10-09
    body: "Thanks, thats the whole point of amaroK Live. :)"
    author: "Ian Monroe"
---
The <a href="http://amarok.kde.org">amaroK</a> team would like to announce the immediate release of version 1.3 of the innovative <a href="http://amaroklive.com">amaroK Live CD</a>. This complete operating system is a unique collaboration between Free Software and Open Music that runs entirely from a single CD.  The live CD comes with a fully functional copy of the amaroK music player bundled with tracks from <a href="http://magnatune.com">Magnatune</a>, the German artists <a href="http://www.paniq.de">Paniq</a> &amp; <a href="http://www.arje.de/">Snooze</a> and the Norwegian performer <a href="http://www.ugress.com">Ugress</a>.













<!--break-->
<p><div style="border: thin solid grey; width: 300px; margin: 1ex; padding: 1ex; float: right">
<img src="http://static.kdenews.org/jr/amarok-live-cd.jpg" width="300" height="300" /><br />
amaroK Live 1.3
</div>Based upon the KDE-centric <a href="http://pclinuxos.org">PCLinuxOS</a>, amaroK Live is not a complete Live CD distribution as much as it is a demonstration of an extremely cool audio player.  This combination reinforces the concept that amaroK Live 1.3 is the perfect collaboration between Free Software and Free Music (free as in freedom). Music selections on the CD span many genres, from Electronica to Rock, Jazz and Classical.
</p>

<p>The tracks contributed by Magnatune are graciously licensed to us under the attribution/non-commercial/share-alike (by-nc-sa) version of the Creative Commons license, so these specific music tracks are both free in the sense of freedom and free in the sense of no cost. Magnatune is a revolutionary concept founded in 2003 as an independent, online record label that hand selects its own artists, sells its catalogue of music through online downloads and print-on-demand CDs and licenses music for commercial and non-commercial use.
</p>

<p>The other tracks found on the CD can also be legally shared for non-commercial purposes.
</p>

<p>Screenshots of amaroK Live 1.3 can be found on the  <a href="http://amaroklive.com/index.php?page_id=113&amp;language=en">amaroK Live website</a>.
</p>

<p><b>About Magnatune:</b>
Founded in 2003, Magnatune is an independent, online record label that hand selects its own artists, sells its catalogue of music through online downloads and print-on-demand CDs and also licenses music for commercial and non-commercial use.
</p>

<p>Based on the principle that "We are not evil," the company offers fair-trade music to consumers by equally sharing all revenue from the sale of albums with artists and allowing artists to retain full rights to their music. All music can be previewed free of charge with a "try before you buy" philosophy. Customers can also choose how much they want to pay for the music with pricing ranging from $5-18 for a downloadable album or print-on-demand CD.
</p>

<p><b>About amaroK:</b>
amaroK is a music player for KDE with an intuitive interface. amaroK makes playing the music you love easier than ever before - and looks good doing it. It is Free Software, licensed under the GNU General Public License and is available for most GNU/Linux distributions.
</p>

<p>Since amaroK can use many different engines to decode and output, it can be used in just about any desktop environment.  Xine is the default engine, but Arts, GStreamer and Real/HelixPlayer are all options, including the Network Multimedia framework (NMM).
</p>

<p><strong>CD Download:</strong><br />
HTTP: <a href="http://sluglug.ucsc.edu/pub/amarok/amarok_live-1.3">http://sluglug.ucsc.edu/pub/amarok/amarok_live-1.3</a><br />
BitTorrent: <a href="http://aurora.gkmweb.com:6969">http://aurora.gkmweb.com:6969</a>










