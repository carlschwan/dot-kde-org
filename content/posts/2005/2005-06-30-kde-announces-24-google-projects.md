---
title: "KDE Announces the 24 Google Projects"
date:    2005-06-30
authors:
  - "tmacieira"
slug:    kde-announces-24-google-projects
comments:
  - subject: "Cool!"
    date: 2005-06-30
    body: "Now if these project will really be done in two month (which will happen i hope) kde will get a lot of really great improvements in a very short time. \n\nGreat! \n\nI am looking forward to see this stuff on my desktop."
    author: "Karl M."
  - subject: "Re: Cool!"
    date: 2005-07-01
    body: "Now, I wonder what would happen if we could organize that 4 to 6 times a year ?\nProductivity jump ?"
    author: "Mathieu"
  - subject: "Re: Cool!"
    date: 2005-07-02
    body: "Well, it wouldn't be called \"Summer of Code\" anymore :-P"
    author: "Thiago Macieira"
  - subject: "Re: #13"
    date: 2005-06-30
    body: "For point #2, wouldn't it make more sense to add Qt/KDE code generation support to the Eclipse VE project than have Eclipse just externally launch Qt designer?"
    author: "David Walser"
  - subject: "Re: #13"
    date: 2005-07-01
    body: "I for one quite dislike VE or SWT-Designer. Qt Designer so much more robust and easier to use.\n\nI actually had thought something the other way round, using Qt Designer to create SWT code, e.g. via uic.sf.net.\n"
    author: "Carsten Pfeiffer"
  - subject: "Re: #13"
    date: 2005-07-02
    body: "Well that would be fantastic as well."
    author: "David Walser"
  - subject: "Go for it!"
    date: 2005-06-30
    body: "After reading through the project descriptions, I admit, I'm impressed by the amount and quality of ideas.\n \nI'm looking forward to \"GTD for Kontact\" now ;-)\n\"Implement hyperlinkage between Kontact objects.\"\n\"Implement real attachments to KOrganizer items, as opposed to only storing URIs.\" - ...drool...\n\nGood luck to all!\n\nThomas"
    author: "Thomas"
  - subject: "is there a list of rejected ideas?"
    date: 2005-06-30
    body: "n/t"
    author: "emaN"
  - subject: "Re: is there a list of rejected ideas?"
    date: 2005-06-30
    body: "No."
    author: "Anonymous"
  - subject: "Re: is there a list of rejected ideas?"
    date: 2005-07-01
    body: "We've made an attempt to create such. There are People are free to add new entries\n\nit can be found at\nhttp://current.spookey.net/mediawiki/index.php/RejectedIdeas\n\nor an older version:\nhttp://summerofrejects.pbwiki.com/index.php?wiki=RejectedIdeas"
    author: "Dmitry M. Shatrov"
  - subject: "Can I be the one relaying stop-energy?"
    date: 2005-06-30
    body: "many of these projects look like nice buzzwording, and won't deliver. Some verge on the useless: \nOkular? Like we don't have already zillions of modularized viewers and players... \nWriting a  KDE application not in C++? ahem, Python bindings anyone? \nDistributed Application Markup Language sounds like a .NET ripoff, with security scenarios so awful I don't want to even start thinking about it.\nLabel Browser is just the last fashion in The Quest For Perfect Metadata: people don't classify their stuff, but geeks simply won't give up the fight.\nKnoware is a work that will require at least two years, not months...\nKamion is partly already implemented in KitchenSink (and please fix that first, it really needs some help)\n\nDon't get me wrong: I hope everyone succeeds and I'm sure people will have fun trying; however, these submissions for the most part don't seem to be either realistic or original... the best ones are \"coherent\" progressive work on KOffice and Konqueror elements; it might as well be that the \"visionary\" ones have been chosen by the Google guys as a way of taking the piss with the submitters.\n\nAnyway... good coding to everyone! :)"
    author: "Giacomo"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-06-30
    body: "Writing a KDE application not in C++? ahem, Python bindings anyone?\n\nYes, but they are not an application.\n\nWriting KDE apps in Java is an interesting idea.  It would be even more interesting if the GNU Java compiler supported the current Sun Java implementation.\n\nSpecifically, if both were fully supported, we could use the Apache SVG code instead of writing out own.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-06-30
    body: "Hi,\n\nI submitted the Knoware proposal.  Believe me, you aren't the only skeptical one.\n\nBut think about it: building a huge and useful database is the part that will take a while.  This is not part of the proposal; that part is up to users.  The proposal is for a project that will have the smarts to build and analyze the database.  I'd say building a highly accurate spam filter is even a harder use of Bayes algorithms, would you say that takes two years?\n\n--\nBrian Beck\nAdventurer of the First Order"
    author: "Brian Beck"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-06-30
    body: "> Okular? Like we don't have already zillions of modularized viewers and players... \n\nWell, the idea is based on suggestions of the KPDF/KGhostview developers if I remember correctly.\nA viewer which combines all their features into one application is obviously not available yet.\n\n> Writing a KDE application not in C++? ahem, Python bindings anyone?\n\nAny bindings would have been OK. You have to let the students decide which one they want to write their application in.\nMy choice would have been Java as well.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-07-01
    body: "> A viewer which combines all their features into one application is obviously not available yet.\n\nEvince is available for Gnome"
    author: "bob"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-07-01
    body: "http://www.gnome.org/~jrb/files/evince-mail.png\n\nIt even supports mail.  Evince is probably comparable to Konqueror as far as being a generic shell interface."
    author: "Anonymous"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-07-01
    body: "No, that was a joke.\n\nhttp://webwynk.net/jrb/#1120196417\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-07-01
    body: ">Okular? Like we don't have already zillions of modularized viewers and players... \n\nIf you were to read the background, you would understand why.  It's taking the existing brilliant kpdf frontend and turning it into a generic document viewer that can allow viewing of pdf, ps, chm, dvi, doc, kwd, sxc, ppt, whatever with a single interface for the users regardless of the document type.  That is a DOCUMENT viewer with decent navigation and previews, etc, which is a very different beast to an image viewer.  To me, it sure as hell beats the current situation where I need to learn 3 different interfaces to view my pdf, djvu and ppt files.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-07-01
    body: "I think it is a great idea to have a unified document vieuwer. kword etc embedded in konqueror isn't that great, as I always want to/try to edit documents in there... i already asked the dev's to make it r/w, but this is better - a real distinction between vieuwing and editing a document!"
    author: "superstoned"
  - subject: "Re: Can I be the one relaying stop-energy?"
    date: 2005-07-09
    body: "Hey Label browser is a cool idea. Why do u think only people will clasify stuff. I think this guy also has in his mind about applications classifying them. look at the proposal. It sounds great. If u are one of those who dont classify things then u are one of the very few who hate gmail.\nEven the other projects are good. The thing is you havent gone through them properly.\n\ngenius"
    author: "genius"
  - subject: "Duplication?"
    date: 2005-06-30
    body: "Don't 14 and 15 boil down to the same thing?"
    author: "Me"
  - subject: "Re: #4"
    date: 2005-06-30
    body: "Some of this should be simple since most of the Konqueror (note that Konqueror is not a web browser) UI already has an XML interface that controls about 90% of the interface.  I hope that someone is working on the other 10% :-)\n\nHowever, the Firefox XUL user interface is not all good.  Firefox does not support the FreeDesktop standard for icons (which KDE uses).  I point out to P.S. that as a KDE app that Konqueror with a XUL based UI will need to continue to support the KDE/FD icon system rather than having the icons in JARs like Firefox.  \n\nSo, he will have to solve the icon issue.  When he does, this could also be ported to Firefox. "
    author: "James Richard Tyrer"
  - subject: "Re: #4"
    date: 2005-07-01
    body: "I don't think it's aimed at making it possible having XUL based UI for Konqueror, it sounds like it geared towards making Konqueror accept XUL based objects. Like plugins and such. Not making the UI of Konqueror in XUL, but making it possible to run XUL plugins, like the ones made for Firefox, native in Konqueror."
    author: "Morty"
  - subject: "Re: #4"
    date: 2005-07-01
    body: "As far as I know - and that's only peripherally - Firefox's XUL plugins are only possible /because/ the entire UI is written in XUL. So one follows from the other.\nRather, I think the goal is XUL in webpages, but I'm not the one doing it so obviously can't say with certainty =)."
    author: "Illissius"
  - subject: "Unfortunately..."
    date: 2005-06-30
    body: "my proposal \"Haskell Bindings for KDE\" was denied. But I'm not complaining about it, I suppose the developers did their best deciding what's needed.\ncheers"
    author: "Peter"
  - subject: "Re: Unfortunately.."
    date: 2005-06-30
    body: "Just because you're not getting paid, doesn't mean you can't do it :-)\n\nI'm just sayin..."
    author: "Antonio"
  - subject: "Re: Unfortunately.."
    date: 2005-07-01
    body: "I know, and I'm planning to do so, but it will take a lot longer since I'm forced to work on something else (far less interesting) that pays the bills  in the mean time...\nMaybe I will be on time for KDE 4 ;-)"
    author: "Peter"
  - subject: "Re: Unfortunately..."
    date: 2005-07-01
    body: "I wonder why you didn't propose Prolog bindings... now *that* is interesting!\n;)"
    author: "Giacomo"
  - subject: "Re: Unfortunately..."
    date: 2005-07-01
    body: "I can imagine it now....\n\nYou start a KDE application, and after waiting for 30 seconds a single dialogue box appears:\n\n\"No.\"\n\n;)"
    author: "ltmon"
  - subject: "Re: Unfortunately..."
    date: 2005-07-01
    body: "LOL"
    author: "superstoned"
  - subject: "Re: Unfortunately..."
    date: 2005-07-01
    body: "Well, just notice that we basically have no applications written in Java (or any of the other languages for which we have bindings with the arguable exception of Python) though we've had Java bindings for a while.  And a lot of people know Java.  Haskell honestly just be an exercise in generating bindings, not something that would be actively used for application development."
    author: "Scott Wheeler"
  - subject: "wow"
    date: 2005-06-30
    body: "#10 sounds very good. It is an important usability task where most De fail"
    author: "gerd"
  - subject: "NX vs. X12"
    date: 2005-07-01
    body: "Frome what I hear NX is great, lightning fast compared when compared to VNC and X11 but in the long run doesn't it make more sense to fix the X11 client/server protocol?  I understand that this would be a massive undertaking not only from a technical standpoint (creating a new protocol) but also from a political standpoint (reaching a consensus on how the new standard would look and work).\n\nNow I'd like to say that I'm not one of those guys that says we need X12 because X11 has been arrond since September 15, 1987.  If it ain't broke don't fix it.  The thing is we've learned a lot since '87, we have more processing power (compression costs less) and our usage paterns have evolved.  Do you remember computing in '87?"
    author: "ac"
  - subject: "Re: NX vs. X12"
    date: 2005-07-01
    body: "The most successful standards often don't come from a commitee designing them, but from one individual effort that is so good or popular that everyone else copies it.\n\nWith the core of NX being free, if it proves worth it's hype and a lot of people start using it, it could quite easily become part of \"X12\" without having to design by commitee or rub a whole lot of people the wrong way.\n\nL."
    author: "ltmon"
  - subject: "KDE did it first!!!"
    date: 2005-07-01
    body: "Congratulations for KDE team for posting the result approved projects with title and details, in summer of code first!!!\n\nWe know that there are lot of projects approved in SoC for open source communities, yet some of the communities has less priority in posting what projects approved and details.\n\nThis only shows that KDE.org is truly for all of us, not just for developer but users as well.\n\nLong live python plugin, as a scripting for KDE.\n\nWell done! KDE Team."
    author: "NS"
  - subject: "Re: KDE did it first!!!"
    date: 2005-07-02
    body: "If you want to be petty, NetBSD announced theirs last week http://www.netbsd.org/Foundation/press/soc.html."
    author: "Michael"
  - subject: "Re: KDE did it first!!!"
    date: 2005-07-04
    body: "Sorry, probably, I am BSD is part of my vocabulary, I am into linux and more interested in applications in general. \n\nThanks for your correction and link.\n\nAny other announcements in SoC Results? Topics and detailed description\n\nNS"
    author: "NS"
  - subject: "Re: KDE did it first!!!"
    date: 2005-07-04
    body: "Python Software Foundation (PSF) got one now:-)\n\nhttp://wiki.python.org/moin/SummerOfCode\n\n\n\n"
    author: "NS"
  - subject: "Wow"
    date: 2005-07-01
    body: "There are LOT of interesting project, but if Knoware will be complete and functional it will be one of the more interesting!"
    author: "Davide Ferrari"
  - subject: "Re: Wow"
    date: 2005-07-01
    body: "Thanks for showing interest!  I'm keeping a development blog here: http://blog.case.edu/bmb12\n\n--\nBrian Beck\nAdventurer of the First Order"
    author: "Brian Beck"
  - subject: "#1 and #5 will bring Kontact into another realm :)"
    date: 2005-07-01
    body: "Great!\n\nGo Kontact, go!"
    author: "m."
  - subject: "Tenor and #14 and #15"
    date: 2005-07-01
    body: "If could for the Tenor (the new KDE linkage technology) exists would it be valuable for the projects #14 and #15.\n\nI'm really curious about Tenor but heart nothing about it for some time now.\nDoes anybody knows more about its status?"
    author: "Mario Fux"
  - subject: "Re: Tenor and #14 and #15"
    date: 2005-07-01
    body: "Tenor won't be delivered in time for the students' work. So using it could be very difficult for them.\n\nNot that some of them haven't suggested using it -- quite to the contrary."
    author: "Thiago Macieira"
  - subject: "Re: Tenor and #14 and #15"
    date: 2005-07-01
    body: "but maybe he can help writing it, at least outline some things it needs to do..."
    author: "superstoned"
  - subject: "My whish list for KDE/Google"
    date: 2005-07-01
    body: "It is just a very short whish list... it would be really great if KDE/Google:\n\n- create a KDE/Linux client for Google Earth (and maybe integrate it with GPSDrive)\n- create a KDE/Linux client for Google movies\n\njust my .02 cents\n\nRegards,\nTimur\n"
    author: "Timur"
  - subject: "Re: My whish list for KDE/Google"
    date: 2005-07-01
    body: "well, google maps should be doable, as google has released the API. maybe the same will go for Google movies, as DVD John aparently already cracked it :D\n\nbut i'm sure it'll take some time."
    author: "superstoned"
  - subject: "Re: My whish list for KDE/Google"
    date: 2005-07-01
    body: "well the latter should be easy since google posted the code of their work on VLC:\nhttp://code.google.com/patches.html"
    author: "ra1n"
  - subject: "kdevelop win32 port rejected?"
    date: 2005-07-01
    body: "man, they rejected the windoze port of kdevelop... i kinda doubt it would have been accomplished in the time, but it sure sounded good...\nthen again if kde4 is supposed to be ported alltogether, we will hopefully get kdevelop to win-platforms"
    author: "hannes hauswedell"
  - subject: "brings a tear to my eye."
    date: 2005-07-02
    body: "In a world filled with so much bullshit that we still have noble projects such as this. Free for all to use and done purely in the spirit of innovation and furthering of knowledge.\n\nAivars"
    author: "Aivars"
  - subject: "Re: brings a tear to my eye."
    date: 2005-07-02
    body: "\"Free for all to use and done purely in the spirit of innovation and furthering of knowledge\"\n\nUm...\n\n- Notoriety\n- Bragging rights\n- Cash payout\n- Future consulting gigs\n- Etc.\n\nI wouldn't say that it's \"done purely in the spirit of innovation and furthering of knowledge.\"\n\nEspecially since:\n\n1) There is *very* little innovation here (\"innovation\" is a word that's been tossed around so much that it's practically lost its meaning)\n\n2) This isn't so much about furthering knowledge - there are plenty of good coders out there who could implement many of the features listed - it's a question of time and resources rather than ability, so I see little that contributes to \"furthering of knowledge\" except perhaps for the coders directly involved\n\nHate to sound like a cynic, but someone has to keep his head on straight around here..."
    author: "Rory"
  - subject: "Re: brings a tear to my eye."
    date: 2005-07-02
    body: "Amen to that, bro!"
    author: "DeadFish Man"
  - subject: "nice"
    date: 2005-07-04
    body: "Some of these more practical/useful ideas and improvements are really cool and welcome.  Like the additions to Konqi and KOffice.  I dont know how I feel about some of the more \"conceptual\" ideas.  I guess we'll see how it all turns out.\n\nI really doubt some of these will be anywhere near completed by the schedule outlined in the proposals.  Also I guess I am kind of interested how were the successful applicants picked.  I mean building a fully integrated KDE NX client as described in the design document is a pretty daunting task to do it right.  Yet when I see \"The Project will be completed by the end of August.\" followed by \"I have started to learn Qt programming, but have not yet actually completed a project with it, and look forward to doing so.\"  Well, forgive me if I am a little sceptical, but maybe it's because I am pessimistic by nature.  I've been programming w/ Qt for almost 2 years, and with KDE for a year, and if I was applying, I wouldn't have applied for this bounty because I feel it's out of my league.  But anyway, it doesn't really matter.  \n\nI wish the best of luck to all the projects, and I am sure some great things will come out of this."
    author: "timmeh"
  - subject: "Implement support for OASIS XLIFF 1.1 in KBabel "
    date: 2005-07-04
    body: "The translation  of supporting documentation  ( i.e. User Guides, Reference Guides, Help scripts, Installation Guides, Training Material, etc.) is just as important and these are usually produced in word processing tools, NOT development tools. Therefore a feeder to/from KBabel and word processing tools  should be available.\n\n"
    author: "Nick Anastasi"
  - subject: "Re: Implement support for OASIS XLIFF 1.1 in KBabel "
    date: 2005-07-04
    body: "First KBabel must be freed from its gettext PO file orientation or XLIFF cannot be correctly supported at all.\n\nAs for word processing, personally I do not mind if KBabel gets filters from/to RTF and from/to OASIS Open Document. But here too, you cannot do it correctly if the only format really available in KBabel is PO. (Then, of course, somebody will have to implement the different wordprocessor filters. May be the filter codes in KOffice can help here.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "hi"
    date: 2005-07-15
    body: "If I want to take part in the project, what should I do?\n"
    author: "sailaja"
  - subject: "Re: hi"
    date: 2005-07-15
    body: "First improve your English text comprehension, then understand that there is no possibility to join this year's Summer of Code anymore."
    author: "Anonymous"
  - subject: "hai"
    date: 2006-07-11
    body: "Hai \n    My name is Sirisha. I am studying MCA(fifth semester). I am very enthusiastic about these projects. \n\n   I want to konw the full details of this projects. Is there any conditions to apply this?(age limits etc.,). If i want to do the project How to apply?\n\nPlease send details to my mailid.\n\nThankyou,\nbye\n\n\n\n\n "
    author: "sirisha"
---
The KDE Project and Google announce the 24 KDE projects selected for the
"Summer of Code" project. The lucky students and the KDE e.V. will receive a
total of $120,000 if they can complete their projects in the allotted two
months.






<!--break-->
<p>The accepted projects span accessibility work, improvements to the office and
personal information management suites, and innovations to KDE architecture.
Much anticipated projects include one addressing VoIP in KDE, and a unified
document viewer to handle multiple formats with a plugin architecture for third
party vendor extensions.</p>

<p>Twenty year old Piotr Szymanski, one of the selected students, said: "<em>I am
really happy about developing this bounty. I worked on the KDE Polish
translation team for a couple of years, and now, whilst studying computer
science, I can get paid to create a much-needed free software application. I'm a
bit of a mix of a programmer, mathematician, artist and a philosopher, a kind of
renaissance guy.</em>"</p>

<p>Eva Brucherseifer of the KDE e.V. board added: "<em>The selection process was
very difficult due to so many good applications.  This is a great opportunity
for students to join the KDE community and to spend the summer on something that
will stay.</em>"</p>

<p>The full list of selected projects and students:</p>

<ol>
<li><A href="http://developer.kde.org/summerofcode/kontactscript.html">Common scripting/plugin subsystem for Kontact</A> - Kun&nbsp;Xi</li>

<li><A href="http://developer.kde.org/summerofcode/varanalysis.html">Framework and proof of concept of a write-time analysis of the value of a
variable in C++ in the KDevelop UI</A> - John&nbsp;Tapsell</li>

<li><A href="http://developer.kde.org/summerofcode/knx.html">Fully integrated KDE NX Client</A> - Christopher&nbsp;Cook</li>

<li><A href="http://developer.kde.org/summerofcode/xul.html">To give Konqueror, the KDE Web Browser a complete XUL implementation</A> - Philip&nbsp;Scott</li>

<li><A href="http://developer.kde.org/summerofcode/gtd.html">GTD (Getting Things Done) for Kontact</A> - Rafa&#322;&nbsp;Rzepecki</li>

<li><A href="http://developer.kde.org/summerofcode/xliff.html">Implement support for OASIS XLIFF 1.1 in KBabel</A> -
Asgeir&nbsp;Frimannsson</li>

<li><A href="http://developer.kde.org/summerofcode/pagedmedia.html">Implementation of HTML/CSS paged media in KHTML and Konqueror</A> -
Allan&nbsp;Sandfeld&nbsp;Jensen</li>

<li><A href="http://developer.kde.org/summerofcode/kspreadengine.html">Improve computation engine of KSpread</A> - Tomas&nbsp;Mecir</li>

<li><A href="http://developer.kde.org/summerofcode/voip.html">Integration of VoIP/Video-Conferencing into Kontact/KDE</A> - Malte Böhme</li>

<li><A href="http://developer.kde.org/summerofcode/kamion.html">Kamion &#8212; User State Migration Tool for KDE</A> - Milan&nbsp;Mitrovic</li>

<li><A href="http://developer.kde.org/summerofcode/daml.html">KDE Framework Addition: Distributed Application Markup Language</A> -
Iain&nbsp;Dooley</li>

<li><A href="http://developer.kde.org/summerofcode/runtimeobserver.html">KDE runtime observer: help to debug Qt/KDE applications by showing very easily runtime relations between objects: Signals, heritage, containment</A> - David&nbsp;Moreno&nbsp;Montero</li>

<li><A href="http://developer.kde.org/summerofcode/eclipse.html">KDE support for Eclipse</A> - Oleksandr&nbsp;Dymo</li>

<li><A href="http://developer.kde.org/summerofcode/labelbrowser.html">Label Browser: a new concept in desktop browsing</A> - Ramakrishna.R</li>

<li><A href="http://developer.kde.org/summerofcode/livingkde.html">Living KDE: an experimental idea of using Tag and search concept to
organize user documents</A> - Sachin&nbsp;Gupta</li>

<li><A href="http://developer.kde.org/summerofcode/sidebar.html">A New Sidebar for Konqueror</A> - John&nbsp;Doyle</li>

<li><A href="http://developer.kde.org/summerofcode/nokey.html">Nokey: an accessibility application designed to facilitate complete control of a computer using only the movement of the mouse pointer</A> - Leo&nbsp;Spalteholz</li>

<li><A href="http://developer.kde.org/summerofcode/okular.html">oKular - A powerful unified viewer application for KDE with a plugin system and backends for most popular formats</A> - Piotr&nbsp;Szymanski</li>

<li><A href="http://developer.kde.org/summerofcode/pptimport.html">PowerPoint import filter for KPresenter</A> - Yolla&nbsp;Indria</li>

<li><A href="http://developer.kde.org/summerofcode/speech.html">Speech recognition in KHotKeys</A> - Olivier&nbsp;Goffart</li>

<li><A href="http://developer.kde.org/summerofcode/sensornetworks.html">Spreadsheet Programming Interface for Sensor Networks</A> -
James&nbsp;Horey</li>

<li><A href="http://developer.kde.org/summerofcode/knoware.html">Visionary application - Project Knoware</A> - Brian&nbsp;Beck</li>

<li><A href="http://developer.kde.org/summerofcode/visualhistory.html">Visual History for Konqueror</A> - Dianfei&nbsp;Han</li>

<li><A href="http://developer.kde.org/summerofcode/javakbd.html">Writing a KDE application not in C(++): onscreen keyboard utility written in the Java programming language and utilizing the KDE/Qt framework</A> - Jack&nbsp;Lauritsen</li>

</ol>


