---
title: "KDE Chooses BitKeeper for Source Code Repository"
date:    2005-04-01
authors:
  - "dmolkentin"
slug:    kde-chooses-bitkeeper-source-code-repository
comments:
  - subject: "WTF?"
    date: 2005-04-01
    body: "Ah!!! \n\nOK, today is April 1st. Pufff, you've scared me."
    author: "Ricardo Galli"
  - subject: "Re: WTF?"
    date: 2005-04-01
    body: "Shit! That really scared me :O"
    author: "EHa"
  - subject: "OMFG..."
    date: 2005-04-01
    body: "NO!!!!!!\n\n;)"
    author: "James Smith"
  - subject: "Hmm..."
    date: 2005-04-01
    body: "Could have had me, except the \"GNU kernel\" bit really was trolling a bit too far..."
    author: "Jon"
  - subject: "Re: Hmm..."
    date: 2005-04-01
    body: "This is factual.  The GNU project has officially adopted the Linux kernel in favor of Heard.  I hurd RMS say this myself."
    author: "ac"
  - subject: "Re: Hmm..."
    date: 2005-04-01
    body: "> I hurd RMS say this myself.\nROFL... wuaha ... ! ! ! great"
    author: "Me"
  - subject: "One of the funniest fools' day jokes ever!"
    date: 2005-04-01
    body: "Larry McFly, Stephan Coolio, Cervisia removed...\n\nGreat stuff guys, you really made my day. Who was the mastermind behind this one? Daniel Molkentin, are you guilty as charged?"
    author: "Amadeo"
  - subject: "Re: One of the funniest fools' day jokes ever!"
    date: 2005-04-01
    body: "Yes, this is a good one... I wonder if any news sites will pick this up as \"real\" like some did with last years joke.  That even had a cvs project that backed it up :-D.  \n\n-Sam\n\nP.S - I liked the wiki entry, nice touch!"
    author: "Sam Weber"
  - subject: "Re: One of the funniest fools' day jokes ever!"
    date: 2005-04-04
    body: "Looks like RootPrompt has posted it today; it being April 4 now, either they were fooled, or they celebrate April Fool's WEEK.\n\nhttp://rootprompt.org/article.php3?article=8524"
    author: "Tim Middleton"
  - subject: "Re: One of the funniest fools' day jokes ever!"
    date: 2005-04-01
    body: "It was quite the team effort. :)"
    author: "Anonymous"
  - subject: "Re: One of the funniest fools' day jokes ever!"
    date: 2005-04-01
    body: "I kicked off the idea, but I really have to credit Kurt, Jonathan and Navindra for making it perfect and adding their own twists."
    author: "Daniel Molkentin"
  - subject: "great one"
    date: 2005-04-01
    body: "You actually got me there for a while... prepare to be slashdotted though, I can't wait to read the outrage that's going to happen there!!"
    author: "hp"
  - subject: "Re: great one"
    date: 2005-04-01
    body: ">>> ...prepare to be slashdotted.....  <<<\n\nI dont think so. \n\nThe authors gave the game away be using the weird name modifications, and making it a too obvious April Fool's story with some of the details. The first comment noticed the fake already.\n\nBut anyways -- very nice plot. I spilled my coffee by laughing too loud and suffering from convulsions when reading it, even if the it was obvious from the start that \"something is wrong with this.\""
    author: "kprinter-fan"
  - subject: "Re: great one"
    date: 2005-04-01
    body: "LinuxToday posted it."
    author: "ac"
  - subject: "Re: great one"
    date: 2005-04-01
    body: "And LWN.net"
    author: "Anonymous"
  - subject: "Re: great one"
    date: 2005-04-02
    body: "The circle is now complete.  Slashdot has it now."
    author: "ac"
  - subject: "Too bad!"
    date: 2005-04-01
    body: "It's April 1st, but it would not have been a bad idea."
    author: "Alex"
  - subject: "I hope..."
    date: 2005-04-01
    body: ".. that the BitMover guys know what a joke is and won't try to find a legal way to harras KDE for this stunt ;-)"
    author: "Inorog"
  - subject: "Re: I hope..."
    date: 2005-04-01
    body: "Don't think that will be a problem as he obviously has a sense of humor, did you not see his proposed \"no whiners license\":-)"
    author: "Morty"
  - subject: "Re: I hope..."
    date: 2005-04-01
    body: "Don't worry.  We all thought it was very funny.\n\n-Wayne"
    author: "Wayne Scott"
  - subject: "Re: I hope..."
    date: 2005-04-07
    body: "It is probably a coincidence that this press release came just four days later:\nhttp://www.bitkeeper.com/press/2005-04-05.html\n\nIf not, then they really didn't find it funny at all.\n"
    author: "Kasper"
  - subject: "Re: I hope..."
    date: 2005-04-07
    body: "It's a coincidence, http://lwn.net/Articles/130681/ talks about \"a conflict over the last month or two\"."
    author: "Anonymous"
  - subject: "Re: I hope..."
    date: 2005-04-10
    body: "Great, I post one single comment to this forum. And three days layter I start receiving spam on that address. Shouldn't it be possible to hide the email address from the page?"
    author: "Kasper"
  - subject: "Re: I hope..."
    date: 2005-04-10
    body: "People still get spam?  Try Spamassassin..."
    author: "ac"
  - subject: "Re: I hope..."
    date: 2005-04-10
    body: "Spamassassin catches about four hundred spams a day for me. Spambayes run via KMail on the rest of my mail catches another sixty. That leaves about thirty spams that end up in my inbox every day."
    author: "Boudewijn"
  - subject: "Ha! You don't fool me.... nonono"
    date: 2005-04-01
    body: "It's gonna be subversion soon... wugawaga"
    author: "Me"
  - subject: "KDE BASED ON MONO?? WHAAA???"
    date: 2005-04-01
    body: "\"Novell executives gave the impression that the Gnome and KDE open source desktop environments are not quite up to competing with Windows, but it is getting excited about the version of KDE that will accompany SuSE Linux 10 next year. This is based on Mono, another Novell takeover, which aims to provide a development environment that will run Java and Microsoft.net on Linux. The demos look fantastic but the timing of the launch may clash with Microsoft's release of Longhorn.\"\n\n- The Guardian"
    author: "Alex"
  - subject: "Re: KDE BASED ON MONO?? WHAAA???"
    date: 2005-04-01
    body: "I guess that's why Miguel de Icaza got a CVS account: http://lists.kde.org/?l=kde-cvs&m=111233809718017&w=2"
    author: "Anonymous"
  - subject: "Re: KDE BASED ON MONO?? WHAAA???"
    date: 2005-04-01
    body: "Allowed him by Davbin Muellow, no less..."
    author: "Boudewijn Rempt"
  - subject: "Re: KDE BASED ON MONO?? WHAAA???"
    date: 2005-04-02
    body: "Bizarre!"
    author: "David"
  - subject: "speaking of Aprils fools"
    date: 2005-04-01
    body: "Is this one as well (they can't be serious, right):\nhttp://www.guardian.co.uk/online/story/0,,1448108,00.html\n\nQuote from the text:\n\nNovell executives gave the impression that the Gnome and KDE open source desktop environments are not quite up to competing with Windows, but it is getting excited about the version of KDE that will accompany SuSE Linux 10 next year. This is based on Mono, another Novell takeover, which aims to provide a development environment that will run Java and Microsoft.net on Linux. The demos look fantastic but the timing of the launch may clash with Microsoft's release of Longhorn.\n\nHey, i've been in a business meeting with them where they state that WinD0wS\nRuLeZ, but hey - what's a Mono based KDE? :)\n"
    author: "jmk"
  - subject: "It's not Aprils fools..."
    date: 2005-04-01
    body: "It was posted on Thursday March 31, 2005."
    author: "Alex"
  - subject: "Re: It's not Aprils fools..."
    date: 2005-04-01
    body: "never heard about time zones ?"
    author: "GMT"
  - subject: "Re: It's not Aprils fools..."
    date: 2005-04-01
    body: "it was the 31st in britain, so it's probably not aprils fools indeed. the reporter probably didn't know what s/he was writing"
    author: "ac"
  - subject: "Re: It's not Aprils fools..."
    date: 2005-04-02
    body: "So what, in Germany,Italy,France,Spain,Belgium,... it was 1st April since the date is  31st,March > 11PM GMT\n"
    author: "GMT-1,GMT-2,GMT-3,.."
  - subject: "Jumping the gun"
    date: 2005-04-01
    body: "I always bite on these because it's still March 31st here in Oregon for a few hours. However the names got progressively worse and by Kalle it was way over the top, not to mention a few other things. Thankfully now all I have to be concerned with is rewriting everything in Mono. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: Jumping the gun"
    date: 2005-04-01
    body: "I'm glad I'm not the only one in Oregon that fell for it. "
    author: "Kinema"
  - subject: "Blogs"
    date: 2005-04-01
    body: "\"While we expect some belated opposition from within our developer community to show up in the next few days...\"\n\nYou bet!  Can't wait to see what KDE bloggers at http://planetkde.org are going to say about this.\n\n"
    author: "Jeff"
  - subject: "Re: Blogs"
    date: 2005-04-01
    body: "What KDE bloggers on <a href=\"http://planetkde.org\">planetkde.org</a>? (try it)"
    author: "Carewolf"
  - subject: "Re: Blogs"
    date: 2005-04-01
    body: "Those sneaky Gnome devs.\n<p>\nI'm sure they're just feeling pretty good about themselves over at <a href=\"http://planet.gnome.org\">http://planet.gnome.org/</a>\n<p>\n(Alas, no HTML allowed in postings on the Dot.  Thank goodness for highlight-and-middle-click though.)"
    author: "Jeff"
  - subject: "Re: Blogs"
    date: 2005-04-01
    body: "I must say, the quality of this years jokes is pretty good :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: Blogs"
    date: 2005-04-01
    body: "True, G12 story over at the Gnome place was particularly good. Not only all the work gone into making it, but the introducing of the revolutionary compulsory subscriptions system was a really great. Perhaps something for KDE too, if they don't have a patent pending or something that is:-)\nhttp://www.livejournal.com/users/davyd/139147.html"
    author: "Morty"
  - subject: "Re: Blogs"
    date: 2005-04-01
    body: "Oh boy, you KDE and GNOME guys really got me laughing my ass off! This is a great April fools joke! :)\n"
    author: "konqi"
  - subject: "planet.gnome.org"
    date: 2005-04-01
    body: "Haha!  I just tried to load http://planet.gnome.org/ as well.  :-)"
    author: "ac"
  - subject: "Re: Blogs"
    date: 2005-04-01
    body: "Lol the G12 project (gnome 2.12) is the best.\n\nFar ahead from Microsoft with their super-high-tech credit card system.\n;o)"
    author: "JC"
  - subject: ":-)"
    date: 2005-04-01
    body: "Good one, Daniel! Had me fooled for a moment - fortunately I skimmed other articles before replying so the date settled in. :-)\n"
    author: "Rob"
  - subject: "Cool!!!"
    date: 2005-04-01
    body: "Very cool. I esp. like Kalle Chrysler Daimler :-)"
    author: "Piotr Gawrysiak"
  - subject: "Doh! Got me too..."
    date: 2005-04-01
    body: "I read the first line and, being the geek I am, got side-tracked by the link.  I read the entire thread to see what the problem resolution was.  When I saw they fixed the problem (short version: merging 70K files used to take 512MB, now uses 8MB) I came back to find out if it was really a joke.\n\nAnd I swore last year I wasn't gonna get suckered into any lame April Fools posts.  That's what I get for staying up till 5:30 in the morning.\n\nCheers,\n\nChipper02"
    author: "Chipper"
  - subject: "Excellent!"
    date: 2005-04-01
    body: "I started my day with a good laugh!!!"
    author: "annmath"
  - subject: "Oh cool!"
    date: 2005-04-01
    body: "<a href=\"http://lists.kde.org/?l=kde-cvs&m=111236615812956&w=2\">http://lists.kde.org/?l=kde-cvs&m=111236615812956&w=2</a>"
    author: "Derek Kite"
  - subject: "Re: Oh cool!"
    date: 2005-04-01
    body: "It's a joke, right? :/"
    author: "Anonymous"
  - subject: "Re: Oh cool!"
    date: 2005-04-01
    body: "Probably :)\nI can't see anything in the CVS about koassistant"
    author: "JC"
  - subject: "Re: Oh cool!"
    date: 2005-04-01
    body: "Pheeeeew ;)\n\nThat was a good one :)\n"
    author: "Anonymous"
  - subject: "Re: Oh cool!"
    date: 2005-04-01
    body: "This is cool. Whenever I start to write a letter I think \"man, I wish a little animated character would inform me of what I'm doing.\" So far on the GNU kernel, this has been a pipe dream. Thanks KOffice!"
    author: "Ian Monroe"
  - subject: "Bitkeeper vs. SubVersion"
    date: 2005-04-01
    body: "I think that bitkeeper, since it isn't GPL, is way better than subversion. Good choice!\n\n;-)"
    author: "mOrPhie"
  - subject: "Re: Bitkeeper vs. SubVersion"
    date: 2005-04-02
    body: "Subversion isn't GPL either. It's even more free than GPL. It uses an Apache/BSD style licence."
    author: "Tim "
  - subject: "This is funny too..."
    date: 2005-04-01
    body: "Go to planetkde.org\n\nAnd now go to planet.gnome.org.\n\nVery funny guys!"
    author: "Alfons Hoogervorst"
  - subject: "Re: This is funny too..."
    date: 2005-04-02
    body: "Ha, ha. Nice one!"
    author: "David"
  - subject: "Kalle Chrysler Daimler"
    date: 2005-04-01
    body: "I thougth his real name Kalle Daimler Chrysler, isn't it?\n\nMNN"
    author: "MNN"
  - subject: "Re: Kalle Chrysler Daimler"
    date: 2005-04-01
    body: "I think Chrysler is his maiden name, then he got married to Daimler."
    author: "Anonymous"
  - subject: "Not a good idea. Have you tried GNU Arch?"
    date: 2005-04-02
    body: "I don't think that this change is good idea, we \nshouldn't just look at the pragmatic aspects.\nIf we want to promote free software, we should  \nuse free software. Not say \"Do what we say but\nnot what we do\".\n\nIf the free source management software tools are\nnot good enough, the right thing to do would be \nsupport the impovement of free software tools.\n\nKDE is one of the bigest open source projects, \nthis is not a good signal.\n\nThere exists other alternatives, here there is an interesting\ncomparison.\n\nhttp://better-scm.berlios.de/comparison/comparison.html\n\nHave you made some tests with GNU Arch?\n\nhttp://www.gnu.org/software/gnu-arch/\n\n(I don't know how well-tested or scalable is it,\nhowever some free-software projects are already\nusing it).\n\nsee also\n\nhttp://regexps.srparish.net/www/\n\nsome comment on it at http://www.xouvert.com/\n\n\"The clean design of arch makes it trivial for anyone who downloads our source code to create their own local \"branch\" for development, keep it under revision control, then have their modifications merged, with complete history, back upstream at some point in the future. This is next to impossible to do with CVS.\"\n\n"
    author: "Pablo De N\u00e1poli"
  - subject: "Re: Not a good idea. Have you tried GNU Arch?"
    date: 2005-04-02
    body: "Erm...\nhave you actually read the article?   Including the date it was posted on? \nAnd the comments? Especially the ones with \"April\" in the headline? :)\n\n"
    author: "cm"
  - subject: "Re: Not a good idea. Have you tried GNU Arch?"
    date: 2005-04-02
    body: "Oops, the comments with \"April\" in the title were about something else.  \n\nStill, don't worry, you've been preaching to the converted...\n\n"
    author: "cm"
  - subject: "What now?"
    date: 2005-04-06
    body: "http://kerneltrap.org/node/4966\n\nno more free bitkeeper.  Is this going to affect KDE?"
    author: "Shawn Gordon"
  - subject: "Re: What now?"
    date: 2005-04-06
    body: "Could it be that you're posting five days too late? :-)"
    author: "Anonymous"
  - subject: "Re: What now?"
    date: 2005-04-06
    body: "5 days late for what?  Did you read the story?  It's not a joke, Linus has weighed in at http://linuxtoday.com/developer/2005040602726OSKN"
    author: "Shawn Gordon"
  - subject: "Re: What now?"
    date: 2005-04-06
    body: "Did you read http://dot.kde.org/1112318366/? That's a joke."
    author: "Anonymous"
  - subject: "Good one"
    date: 2005-04-07
    body: "Great April 1st post, that had me fooled for a minute :)"
    author: "Micah"
---
Due to <a href="http://svn.haxx.se/dev/archive-2005-02/0897.shtml">severe problems</a> with the scheduled migration of KDE's massive source code repository from the <a href="http://www.gnu.org/software/cvs/">CVS</a> revision control system to <a href="http://subversion.tigris.org/">Subversion</a>, the KDE project has decided to opt for the <a href="http://www.bitkeeper.com/">BitKeeper</a> source control system as the more pragmatic choice.   The full press release follows; further details on what this will mean to KDE developers and contributors requiring repository access <a href="http://wiki.kde.org/tiki-index.php?page=KDE+BitKeeper+HowTo">will be posted</a> shortly to the <a href="http://wiki.kde.org/">KDE Wiki</a>. 














<!--break-->
<h2>KDE Chooses BitKeeper for Source Code Repository</h2>

Yesterday, the <a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a> signed an agreement with <a href="http://www.bitmover.com/">BitMover, Inc.</a> to allow all KDE developers to use the latest BitKeeper client tool at no charge.  The arrangement comes in anticipation of KDE's full-scale adoption of the BitKeeper source control suite.
<p>
The KDE project had been using CVS for a number of years, but due to persistent and crippling limitations it was finally decided to convert the massive source repository to Subversion, a next-generation CVS clone with fewer limitations.  Unfortunately, due to many unresolved issues and technical problems with Subversion, the move has proven impossible.
</p>
<p>
<i>"A repository of KDE's size poses issues that the creators of Subversion would never have thought of,"</i> declared KDE release dude Stephan Coolio.</p>
<p>
After an intense internal debate, it was finally decided that BitKeeper would be the most appropriate choice for a new revision control system, given its proven superiority and track record in the Open Source community.  BitKeeper has enjoyed wide-spread success and praise as the official source code repository for the GNU/Linux kernel.
</p>
<p>KDE's repository will now be hosted on the same server as the GNU kernel.
</p>
<i>"We are glad to support KDE by helping it move to BitKeeper. The Linux kernel developers have proven the reliability of BitKeeper in distributed development and KDE will now be able to take full advantage of that,"</i> said BitKeeper author Larry McFly.  
</p><p>
KDE e.V. board member Mirko Bohemian stated, <i>"Following our licencing deal with BitMover, we expect our developers will be twice as productive, just as the GNU kernel developers are now."</i> Linus Torvalds was not available for comment.
</p>
<p>
The only significant drawback of the deal is that KDE developers will not be
allowed to work on or contribute to any other source control systems as
mandated by the BitKeeper license. To comply with this requirement, KDE has
temporarily removed <A href="http://www.kde.org/apps/cervisia/">Cervisia</a> from the kdesdk module until the CVS support can be replaced by full BitKeeper functionality.
</p>
<p>
However, like the GNU/Linux kernel repository, KDE will be available through a
read-only CVS interface for anyone preferring not to use BitKeeper for idealistic reasons.
</p>
<p>
As a matter of pragmatism, the KDE project believes it is time to move forward and embrace next-generation software source control.
</p>
<p>
Kalle Chrysler Daimler, President of the KDE e.V. Board commented, <i>"While
we expect some belated opposition from within our developer community to
show up in the next few days, this move was really the only sane and pragmatic choice for the KDE project. We are now in good form to move towards KDE 3.5 and KDE 4.0 in the coming months."</i>








