---
title: "Interviews on KPDF Usability Work"
date:    2005-06-21
authors:
  - "swheeler"
slug:    interviews-kpdf-usability-work
comments:
  - subject: "kudos to you 2 developers!"
    date: 2005-06-21
    body: "KPDF is wonderful applications, but I am bothered by 2 things, error handling and rendering, which is slow.\n\nCan poppler help in improving the KPDF performance?\n\nKPDF needs to handle errors much like KGhostview, see this:\n\nhttp://www.gurumaa.com/pdfs/soul_curry_apr05.zip\n\nKPDF simply hangs when the sour_curry_apr05.pdf (inside the zip file) is opened. Though it's a sort of bug report, and it is not an error/bug on kpdf part but it would be nice for kpdf to handle errors without hanging. \n\nif we open the same file with KGhostview, we get:\n   **** This file has a corrupted %%EOF marker, or garbage after the %%EOF.\n   **** The file was produced by Corel PDF Engine Version 11.633:\n   **** please notify the author of this software\n   **** that the file does not conform to Adobe's published PDF\n   **** specification.  Processing of the file will continue normally.\n\nand the file opens atleast with kghostview. Thank you for a wonderful and useful application!"
    author: "fast_rizwaan"
  - subject: "Re: kudos to you 2 developers!"
    date: 2005-06-21
    body: "I know that you want better error handling in general, not just for this specific file, but here \"soul_curry_apr05.pdf\" is displayed by kpdf 0.4.1 whithout any problems and mucj quicker than by kghostview 0.20 using ghostscript 7.07.1."
    author: "furanku"
  - subject: "Re: kudos to you 2 developers!"
    date: 2005-06-21
    body: "i also have kpdf 0.4.1, but i can't see it loading. Do i need to install any other packages/libraries? i'm using slackware 10.1 with klax 10.1 packages."
    author: "fast_rizwaan"
  - subject: "Re: kudos to you 2 developers!"
    date: 2005-06-21
    body: "Hmmm... hard to say, if you don't see any error messages :( I'm using gentoo here. Maybe it's not the place to discuss support questions here, but I attach the library dependencies (ldd /usr/kde/3.4/bin/kpdf) of my kpdf installation as a file."
    author: "furanku"
  - subject: "is speed usability too?"
    date: 2005-06-21
    body: "then kpdf should get fast as original acroread. Rendering is a little bit slow. All other things are really nice!\n\nKeep up this good work!"
    author: "anonymous"
  - subject: "KPDF is faster than acroread"
    date: 2005-06-21
    body: "The KPDF that comes with KDE 3.4 is faster than acroread 7. I leave it to the reader of this comment to work out whether that's a testament of KPDF's snappiness or acroread's sluggishness."
    author: "Annno v. Heimburg"
  - subject: "Re: KPDF is faster than acroread"
    date: 2005-06-21
    body: "lol, acroread 7 is fast on all kind of documents\nhttp://bugs.kde.org/show_bug.cgi?id=99486\n"
    author: "testerus"
  - subject: "Awesome presentation mode"
    date: 2005-06-21
    body: "Hi,\n\nGreat work on KPDF by everyone involved. I think KPDF's presentation mode is its one of the best feature. The only two feature that I find missing (for which I still have to use acroread) are:\n\n1. Sub-pixel anti-aliasing. Really useful for laptops and LCD Screens. \n2. Support for rotation.\n\nBoth of these are very useful for reading e-books on laptops.\n\nthanks,\nOsho"
    author: "Osho"
  - subject: "Re: Awesome presentation mode"
    date: 2005-06-21
    body: "The presentation mode is indeed excellent (I like the blue \"clock\" indicating where we are in the document). For consistency, it should be the standard presentation mode for gwenview !!!"
    author: "Veton"
  - subject: "Continuous viewing?"
    date: 2005-06-21
    body: "Is continuous viewing already implemented?\n\nOr *PDF stuff under all Linux GUIs still stuck with page by page [CENSORED] mode?\n\nLast time I was checking - year ago, SUSE 9.2 - KPDF, XPDF, gview - were still far from anything usable. All were failing simple test: in any location press PageDown, press PageUp - and it must be in precisely location as before. Navigation is just terrible. And I'm not talking about search. And keyboard support - few support space as page down. And most of them try to mimic Windoz with its stinking focus, so pressing space is just plain dangerous: you never know where focus is and what going to happen when you press space. Awful.\n\nI wish Apple had ported Preview of theirs to Linux: fast, simple and convenient. Very very very very good pdf viewer. Best one I have ever seen.\n\nUnder Linux if one cannot extract text from pdf - e.g. using pdf2text - reading PDFs are dead business otherwise... The tools are stuck in *STONE* *AGE* and supposed to be extinct by now. I'm just wondering what you people are using them for? Besides sending content to printer they are just useless.\n\nSo I expect Linux still has no decent eBook reader? Or is there anything new appeared?\nAnd search - does search work? or as usual it just present but finds nothing?"
    author: "Philips"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "You should seriousy install a recent version of KDE again....."
    author: "ac"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "Just to clarify if you install a recent version of kpdf you should be pleased to notice...\n\n-Continuous and Facing viewmodes\n-Reactive user interface optimized for contents\n-Two different ways to search for text\n-Selection and copy on textual or graphics areas\n-Accessibility: text to speech and colors changing\n-Thumbnails with as-you-type filtering\n-Presentation mode complete with page transitions\n-Good printing support\n-Save and restore the state of each document between sessions\n-Smart memory management with background preloading\n-Internal compositing engine with transparency support\n-Over 50 other improvements\n(http://kpdf.kde.org/)"
    author: "anonymous"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "Wow. Thanks. \nGone downloading.\n\nP.S. Bookmarks? Is content index implemented? That's one /normally/ missing feature.\n\nP.P.S. If you guys will implement in some way user defined  bookmarks - as opposed to predefined bookmarks tree - that would be great. For now Adobe wants one to buy Adobe Acrobat - think of it  - just to be able to add bookmarks. Silly, isn't it? And no other pdf viewer provides them. Wouldn't be just great if I would be able to make bookmarks for places of PDF I have to work often with, so I do not have to search for them all the time. Why so obvious feature not yet made to any pdf suit? Imaging every day crawling thru 1.5k pages pdf with no bookmarks. Brrr."
    author: "Philips"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "\"P.S. Bookmarks? Is content index implemented? That's one /normally/ missing feature.\"\n\nEven xpdf has this, and link support."
    author: "TRauMa"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "I think the parent was refering to the creation of bookmarks, not the use of already created bookmarks.  What is wanted is the ability to edit PDF files at least minimially in a visual mode.  Think: \"mozilla -edit\" for PDF."
    author: "egkamp"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "You can edit PDF files in KWord."
    author: "ac"
  - subject: "Re: Continuous viewing?"
    date: 2005-06-21
    body: "\"Import and Export\" instead of \"edit\" would be more accurate imo..."
    author: "Anonymous"
  - subject: "Export wish"
    date: 2005-06-22
    body: "here is the export wish http://bugs.kde.org/show_bug.cgi?id=103568\nPlease vote fotr it or help the devs"
    author: "zvonSully"
  - subject: "KParts and Images..."
    date: 2005-06-21
    body: "Hi!\n\nReading the KParts and preview stuff in the great article, I am thinking about one program or feature I am missing using KDE. I miss something like a Image Viewer, that does its job like the one in Windows. Currently I use Kuickshow, but I am not satisfied with it. The image is always very big, it is not in the center of the screen, I have no opportunity to view the next or previous picture with the mouse; I need the Page Up and Page Down keys on my keyboard. If I use the embedded viewer, I have a large image, and scrolling, scrolling, scrolling...\n\nIs there a program available that does something like the Windows Image Viewer does? (Displays the image in the center of the screen, scaled to a \"nice\" size, abillity to view next picture, previous picture, rotate picture, print picture... with the mouse??) \n\nI am not a Coder, but a program like that shouldnt be to hard to code  i think. I would like to suggest it otherwise, maybe the KDE Image Viewer?? ;)\n\nHave a nice day :-D\n\nJan"
    author: "Jan"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "In Kuickshow, you can use the mouse wheel to go to the next or previous image in the same directory.\n\nPerhaps you should look at Gnewview (however I have no idea if it has any or all features that you want.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "Hi Nicolas,\n\nI have a Notebook and I use the touchpad, no way using the mouse wheel ;-)\n\nI know and like Gwenview, but it is too complex for what I want ;-) I have something very simple, small and easy in mind....\n\n;-)\n\nJan"
    author: "Jan"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "gwenview has a kpart, that allows you walk through your images like a dia show. There are two kparts, one with a sidebar containing the images in the current directory and one without the sidebar.\nThe kparts are very simple, and with one hit on the icon in konqueror you can switch between the gwenview view and konqueror view"
    author: "Rinse"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "I created a very fast and bad mockup abowhat I am thinking about...ut "
    author: "Jan"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "Well, it is not that different from the kpart of gwenview, see attachement:"
    author: "Rinse"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "Hmm, wrong button :)"
    author: "Rinse"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "I mostly use gqview, because that provides most of the functionality I need. I don't think there's an equivalent for kde (w.r.t. usability)\n\n/Simon"
    author: "Simon"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "As well, too many features for me ;-)"
    author: "Jan"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "...and gqview has the fastest zoom function of all picture viewer.\nSimply <Ctrl> + mouse wheel to zoom of highres pictures in real time.\n"
    author: "Markus Gans"
  - subject: "Re: KParts and Images..."
    date: 2005-06-21
    body: "Try KView.  That works much better than Kuickshow for me, and has almost all of the features you asked for, screen-centered, next and previous images available by button (or keyboard, naturally with KDE's keyboard shortcut configurability, which I've taken advantage of to remap next/previous to space/backspace), image rotation, etc, without having too MANY image editing features when I primarily want a viewer. \n\nZoom is /slightly/ more complex, but only due to how it can be customized.  There are zoom-in/out buttons, and a combo-box with standard size selections and the ability to type in arbitrary sizes.   One can configure kview to zoom the image to the window (as you likely would on a laptop), the window to the image (as I do, with dual 2048x1532 400x300mm viewable screens to work with), or whatever works best.  One useful feature is that in window to image zoom mode, KView \"remembers\" the size I select, from one image to the next.  Thus, if I set it to 200% zoom for one image, the next is still 200% zoomed.  Likewise of course if I set 50% zoom.\n\nOnly two frustrations, and they aren't enough to stop my use.  (1)  KView evidently wasn't designed with dual screen use in mind.  While most applications honor my KDE preferences as to what screen to open on, KView insists on initially opening centered across the total work area, which on dual screens means split across them!  While centering across the workspace would often be desired, KView needs a configuration option to (a) always open centered on the working screen (b) always open centered on a specific screen (c) always open centered on the total work-area (current implementation) (d/e/f) always open in the upper-left-corner of the above (useful when viewing many images of widely differing sizes), and (g) conform to KDE's global window settings (should be the default).  At minimum, the KView window placement behavior should be enforceable with the KDE Specific Window Settings dialog, which it now ignores, so one can /force/ the desired behavior from KDE/KWin, even if KView doesn't contain the options internally.  Luckily, however, KView WILL maintain a new window location if I move it once opened, even as the window resizes to fit the various viewed images, tho it reverts to split over the two screens the next time I open it. <grrr>  If it tried to center for each image, I'd quickly find a new favorite image viewing app!\n\n(2) If I'm viewing an image and rotate it, then go onto the next, KView asks if I want to save changes.  KView is primarily an image VIEWER, NOT an image EDITOR.  It's not named KEdit (or KImageEdit).  Thus, while saving the rotated image is an acceptable /option/, the default behavior should be to simply view the image, no prompting to save the changed image when advancing to the next/previous image.\n\nOne of the other advantages of KView, as opposed to some of the other suggestions you've gotten, is that it's part of the the KDE-base install, as part of the kdegraphics package.  Thus, it's always in sync with the latest KDE version and doesn't require installing anything extra.\n\nDuncan\n"
    author: "Duncan"
  - subject: "white balance of albert's picture is too dark"
    date: 2005-06-21
    body: "easily solved by showfoto, see attachement :)"
    author: "Rinse"
  - subject: "KPDF using acrobat features?"
    date: 2005-06-21
    body: "If I compile a document with pdflatex and the package \"hyperref\", I get support for all sorts of acrobat reader extensions, such as opening at a particular page, the default view size, fullscreen, bookmarks, etc.  \n\nWill kpdf ever support any of these - or is it just a wrapper for xpdf?\nk.\n"
    author: "kdc"
  - subject: "Re: KPDF using acrobat features?"
    date: 2005-06-21
    body: "Where's the whish you reported about it? Does it have a pdf document attached?"
    author: "Albert Astals Cid"
  - subject: "java script"
    date: 2005-06-21
    body: " Can KPDF in future run java-script?\n\n I create pdf with scribus and the posibility wath offers java-script is very interesant but i only run it in Acrobat Reader."
    author: "nonius"
  - subject: "Re: java script"
    date: 2005-06-21
    body: "Where's the whish you reported about it? Does it have a pdf document attached?"
    author: "Albert Astals Cid"
  - subject: "Page rotation"
    date: 2006-06-20
    body: "Will kpdf provide this option some day?"
    author: "aaki"
  - subject: "Re: Page rotation"
    date: 2008-02-20
    body: "It is for me the only feature to keep acrobat reader on my system !\n\nXpdf has already that feature too...\n\nIt would be very good to have it soon !!!\n\nthanks for your very good work Kpdf :)\n\nRegards"
    author: "Newbeewan"
---
During recent conversations with some of the members of the <a href="http://www.openusability.org/">OpenUsability</a> project, some of the usability work on one of the more exciting applications in KDE, <A href="http://www.openusability.org">KPDF</a>, was brought to my attention.  I managed to catch up with Florian, from OpenUsability, and Albert, one of the KPDF maintainers to talk a little about themselves and their work and about the usability review and followup in KPDF.
<!--break-->
    <hr/>

    <div class="section">
      <img class="photo" src="http://static.kdenews.org/content/kpdf-usability-work/albert.jpg" alt="photo of Albert" align="right" border="1"/>
      <h1>Albert Astals Cid</h1>
      <h3>KPDF Developer</h3>

      <p class="scott"><strong>Scott: <i>Could you give a little bit of information on your background &mdash; where you're from, if you're studying or working, what, where, etc.?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> I'm from Barcelona, in Catalonia (Spain). I'm studying &mdash; well not really &mdash; as in two weeks I will present the final project of my computer science degree and then I'll look for work, so if anyone wants to offer a good job let me know.  ;-)</p>
      <p class="scott"><strong>Scott: <i>How did you get involved in KDE and eventually KPDF?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> I started in KDE translating it to Catalan and I still doing that when I have time. Then I began sending small patches for bugs I found and implementing some features like showing the thumbnail of the current page in the top left part of kghostview and helping Luis Pedro implement thumbnails for all pages. I got involved with KPDF last summer when someone asked why KPDF was still using XPDF 2.x when 3.x was out already and I tried to do it.</p>
      <p class="scott"><strong>Scott:</strong>What would you say is your role in KPDF right now?  I know that there was a lot of impressive work that went on prior to KDE 3.4 &mdash; to what extent were you involved in that?</p>
      <p class="albert"><strong>Albert:</strong> Well, right now I'm doing more mantainership work than implementing lots of cool new features like we did in the KDE 3.4 time frame &mdash; like fixing bugs and working with other people like Poppler developers. Enrico Ros is the one that is primarily developing new features like annotation support (which may not be ready for KDE 3.5). In the KDE 3.4 development time, the work was 50% mine and the other 50% from Enrico; I did the dirtier work like XPDF 3.0 integration and all the benefits that gave us in rendering and things like enabling search, etc. Enrico did the fancier stuff like the continuous view mode.</p>
      <p class="scott"><strong>Scott: <i>Popper is the Freedesktop.org XPDF fork?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> Yes, Poppler is the fork of XPDF 3.0 code base that is being developed under the umbrella of Freedesktop.org. It is already much better than the XPDF 3.0 code. For example, it uses libjpeg and zlib natively to decode DCT and Flate encoded streams instead of the XPDF code. That is better because those libraries are very widespread and they have had possibly more speed optimization and security checks than the XPDF code.</p>
    </div>

    <hr/>

    <div class="section">
      <img class="photo" src="http://static.kdenews.org/content/kpdf-usability-work/holehan.jpg" alt="photo of Florian" align="right" border="1"/>
      <h1>Florian Gr&auml;ssle</h1>
      <h3>OpenUsability Usability Engineer</h3>

      <p class="scott"><strong>Scott: <i>And the same to you Florian -- could you tell us a bit about your background?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> I am student of &quot;Computational Visualistics&quot; (a mixture of computer science, arts, software ergonomics and what have you) at the University of Koblenz, Germany.</p>
      <p class="scott"><strong>Scott: <i>Is that where you're from?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> I am actually from Ellwangen, a <i>very</i> small town in the south of Germany.</p>
      <p class="scott"><strong>Scott: <i>How did you come across the OpenUsability project and when did you get involved there?  Did you decide to contribute to KDE or OpenUsability first?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> I came across OpenUsability more than a year ago. It was a time when I was interested in KDE and Open Source usability in general. I noticed that the usability efforts of KDE weren't really centralized, the main activities stuck to the mailing list which we all know didn't make usability work easy. so I wanted to contribute, do something and contacted the OpenUsability project and finally got in touch with KDE and Open Source usability.</p>
      <p class="scott"><strong>Scott: <i>OpenUsability is something that we've seen a bit of information on recently in the KDE world.  Could you give a short description of the project and where it is right now?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> As the web page states, &quot;OpenUsability.org is a project that brings Open Source developers and usability experts together&quot;. On the one side there are Open Source projects that realize that usability is a main factor to make a quality piece of software. On the other side usability experts realize that working with Open Source projects is a real chance to get involved with the development process at a pretty early stage. At least that was my motivation to get involved with KPDF.</p>
    </div>

    <hr/>

    <div class="section">
      <h1>Working Together</h1>
      <h3>KPDF Usability Review</h3>
      <p class="scott"><strong>Scott: <i>So how did you guys meet up?  Was that something that happened through OpenUsability?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> Kurt Pfeifle suggested that I ask for usability help on OpenUsability. I thought that we did not have serious problems with usability but an expert might find some. So I went there, registered the project and in one or two days Florian was already working on doing a KPDF usability review.</p>
      <p class="florian"><strong>Florian:</strong> I was checking the news section at OpenUsability when I read that KPDF was looking for usability advice. I had the feeling that KPDF was an important application within KDE and spontaneously decided to join.</p>
      <p class="scott"><strong>Scott: <i>How long ago was this?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> That was two months ago.</p>
      <p class="albert"><strong>Albert:</strong> <a href="http://www.openusability.org/forum/forum.php?forum_id=303">Here</a> you can find the news item.</p>
      <p class="scott"><strong>Scott: <i>What kind of feedback does OpenUsability provide to application developers?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> First developers can expect a so called &quot;usability inspection&quot;. This means that a usability expert is evaluating the application with regard to the platform guidelines and usability heuristics. If problems with the interface are found the issues are noted down with suggestions and some explanations so that the developers can follow the rationale behind it.</p>

      <p class="scott"><strong>Scott: <i>Well, I guess I mean something a little more concrete &mdash; one of things you hear sometimes from developers is that usability work is more or less completely subjective opinions.  &quot;user control&quot;, &quot;freedom&quot; and &quot;efficiency&quot; all sound great, but how do you evaluate those?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> whenever I come across a possible problem I try to figure out whether it actually is one and whether it matches a given heuristic like the ones of <a href="http://en.wikipedia.org/wiki/Jakob_Nielsen_(usability_consultant)">Jakob Nielsen</a>.  Then I figure out how important the issue is by imagining the user's work flow and common tasks. Above all it needs experience.</p>

      <p class="scott"><strong>Scott: <i>Was KPDF your first project to work on from OpenUsability?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> No, I am also a member of the Konversation OpenUsability project. I helped with designing the KMail folder properties a few months ago as well.</p>

      <p class="scott"><strong>Scott: <i>Could you give a little background on the methods?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> Basically there are several aspects a good interface should fulfill &mdash; like preventing errors before they happen and the user has to deal with them or giving the user control and freedom over the application (and not vice versa), offering an efficient interface and so on.  During the heuristic evaluation the usability expert checks whether these requirements are met and sorts out possible problems</p>
      <p class="scott"><strong>Scott: <i>What were the first steps?  So, Florian did a usability report, but how did that transfer into addressing those issues in development?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> Well, after he made the report we met several times in the #kpdf IRC channel to discuss some of the issues. The discussion had three main parts: the first were things that I agreed with him and were easily solvable &mdash; those fixes went into out source code quite quickly, the second part were problems I did not understand or I did not think were usability problems &mdash; we discussed those and I &quot;lost&quot; in some items and Florian &quot;lost&quot; in others. The bigger problems were with the third group &mdash; the third group involved things that were not easily fixable like <a href="http://www.openusability.org/forum/forum.php?thread_id=325&amp;forum_id=304">issue 1.4</a>:</p>
        <blockquote>
          <p><strong>1.4</strong> The label of the entries in the &quot;left panel&quot; are only visible if the panel is resized to fit their width. To prevent users from having to resize it a way to show the labels independently should be found.</p>
          <p><strong>Suggestion:</strong> On a mouseover use a tooltip to reveal the label text when it can't be fully displayed.</p>
        </blockquote>
      <p>That cannot be solved from inside KPDF because it's not possible with Qt. So, I made a patch and sent it to <a href="http://www.trolltech.com/">Qt people</a>, unfortunately it was not accepted. Other problems involved some kind of new feature or fix inside KDElibs, as KDElibs is more open and reachable for a KDE developer that could be easily solved.</p>

      <p class="scott"><strong>Scott: <i>Of the things that were actually implemented &mdash; what were some of the more interesting ones?  Which items surprised you?</i></strong></p>

      <table align="right" border="0" cellpadding="0" cellspacing="10"><tr><td>
      <table align="right" border="1" cellpadding="0" cellspacing="0" border="1"><tr><td>
      <table cellpadding="3" cellspacing="3" border="0">
	<tr>
	  <td><img src="http://static.kdenews.org/content/kpdf-usability-work/kpdf-old-thumbnail-bar.png" alt="screenshot before usability work" align="right"/></td>
	  <td><img src="http://static.kdenews.org/content/kpdf-usability-work/kpdf-new-thumbnail-bar.png" alt="screenshot after usability work" align="right"/></td>
	</tr>
	<tr align="center">
	  <td><i>Before</i></td>
	  <td><i>After</i></td>
	</tr>
      </table>
      </td></tr></table>
      </td></tr></table>

      <p class="albert"><strong>Albert:</strong> One of the things I first though was not worth implementing but after doing now think is pretty important is <a href="http://www.openusability.org/forum/forum.php?thread_id=325&amp;forum_id=304">issue 1.1</a>.  It is related to how the current page gets marked in the thumbnail list. Previously only the space around the page margin was highlighted. Florian suggested the thumbnail should not fill the whole width of the thumbnail list so you could see also the top and the sides of the thumbnail highlighted. That improved the quick visualization of which is the currently selected page a lot as with the previous scheme you where not sure if it was the page below or above the highlighted number.  <i>(see screenshots)</i></p>


      <p class="scott"><strong>Scott: <i>Ok, this one's for both of you &mdash; were there any real annoyances in dealing with "the other kind"?  Often there's clear frustration between our, uhm, louder group of folks on the KDE-usability list and developers &mdash; how does that compare to your current work?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> Well, the work has moved at a quick pace and been quite productive but there are always some differences.  Like, for example, when Florian told me how much he hated how KPDF merged its menus with Konqueror and told me, &quot;We should find a better way of doing that.&quot; I explaining him that this can not be done on the KPDF side as it is really a <a href="http://en.wikipedia.org/wiki/KPart">KPart</a> issue and that should be discussed and programmed at a higher level.</p>

      <p class="scott"><strong>Scott: <i>Yes, I've wondered about that too and how it works with these reports.  Users &mdash; and usability people, I would assume &mdash; just see applications, not components and libraries and such.  You've already mentioned a couple of cases where that caused problems (Qt, KParts) &mdash; any comments on how things like that can be addressed in the future?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> Well, it's a difficult thing to solve, the best thing is trying to make libraries and classes as most flexible as possible (yes, even more than they currently are) so that each application can adapt them to their own uses because each application can have a different focus or way of working and as such needs to use a class in a special way that potentially was not thought of by the person that created it.</p>
      <p class="florian"><strong>Florian:</strong> On the KParts side I have the feeling that it's not too clear what they are meant for &mdash; are they meant as a preview or a feature reduced application within the main application (i.e. Konqueror)?  I can imagine that it is pretty difficult for users to sort out when to use the embedded KPart and when to start the actual application behind it and what the benefits of the embedding actually are. It must be pretty irritating to see Konqueror's toolbars and menu entries change and hop around depending on which view state it's currently in. So I would like to see them mainly as some sort of quick preview for images, PDFs and such files. Then it's our job to find out which interactions these files have in common (like zooming in and out or rotating) and find a solution to present them in a way users can easily understand. They shouldn't have to wonder whether the respective action is meant to deal with Konqueror as an application or the embedded KPart view as a subpart of it. One thing that's still unknown is whether users are really using the KParts in the first place... I hope KDE 4 will have room for such thoughts.</p>

      <p class="scott"><strong>Scott: <i>Are there any other major challenges that come to mind in working together like this?  And connected to that, do you see this way of working as something that you both feel was beneficial?  Is this something that can work in Open Source in general?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> Sure. The benefits of Open Source Software for usability engineers is the immediate response one gets directly from the developers and not through some obscure marketing manager. It's an opportunity to get in touch with software development at a very early stage. That's something usability engineers normally can only dream of. So I can only encourage other usability engineers to get in contact with Open Source projects. It's fun after all and <i>the</i> chance to practice one's usability skills. The great majority of developers aren't as stubborn as one may first think and indeed willing to accept constructive input. On the other side I hope developers realize that usability engineers don't try to be super-smart or want to know everything better. In the end we are both interested in making Open Source software better.</p>
      <p class="albert"><strong>Albert:</strong> Well, the main challenge is you cannot code impulsively because it will probably end with a thing that has a few usability issues, so I ask Florian when I want to implement something UI related, that has the problem that he may not be available in that moment and the "coding impulse" you had finishes without having coded anything :-D</p>

      <p class="scott"><strong>Scott: <i>So your work together is ongoing rather than a one-off sort of thing?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> It's a steady flow of mutual exchange I guess.</p>
      <p class="albert"><strong>Albert:</strong> Of course. I try to consult Florian about substantial changes we make so that he does not have to redo the usability inspection over and over again from zero. We just readjust it little by little.</p>
      <p class="florian"><strong>Florian:</strong> I felt very happy every time I read in a svn log &quot;fix usability issue #&quot;</p>

      <p class="scott"><strong>Scott: <i>How would you compare the importance of a review to say, ongoing cooperation?</i></strong></p>
      <p class="florian"><strong>Florian:</strong> A review is important so that both developers and usability engineers can get to know each other and sort out which way to work together is best. But it is only a start. Equally important is a constant cooperation. It's not like after one review every possible usability problem is found and solved. Open Source applications are constantly changing and need to be rechecked frequently. That's why the so called &quot;usability life-cycle&quot; thinks of usability as a partner during the whole development process.</p>
      <p class="albert"><strong>Albert:</strong> Well ongoing cooperation brings discussion with it; you end with a better implementation as compared to working without that ongoing discussion and "fixing things" afterwards because in the later case you already have two people's ideas there.</p>

      <p class="scott"><strong>Scott: <i>Ah, that reminds me of another issue &mdash; how have you guys communicated mostly?  Email?  IRC?  OpenUsability forums?  It seems that Florian is also reading the SVN commit list?</i></strong></p>


      <p class="florian"><strong>Florian:</strong> I am checking out KPDF on a frequent basis to keep track of the changes and compare to older versions and also recheck my suggestion.</p>
      <p class="albert"><strong>Albert:</strong> Well, the communication is mostly in IRC, me interchanged a few mails in the beginning because IRC is too much direct and e-mail helps start things going when you don't know each other. The OpenUsability forum is only used to keep track of the implemented and missing usability fixes.</p>
      <p class="florian"><strong>Florian:</strong> I think the forums at OpenUsability.org are a good way for others to get a feeling of what usability work looks like.</p>

      <p class="scott"><strong>Scott: <i>Ok, so, wrapping up &mdash; any comments that you'd like to make on KPDF, OpenUsability or interaction between the two?  Are there any other "big" steps from here on either in the interaction or the projects themselves?</i></strong></p>
      <p class="albert"><strong>Albert:</strong> I'm happy to see that there are new projects that bring a different types of people to into the KDE community. Basically KDE the community is / was composed of high-tech, knowledgeable people, but most of us don't have other important skills like usability expertise (OpenUsability) or artistic skills (KDE-artists.org). Bringing those people inside the KDE community gives us a better product as usability and looks are very important.  It also gives us a broader base to share and discuss ideas that will end in having more innovative programs in the end.</p>
      <p class="scott"><strong>Scott: <i>Thanks guys!</i></strong></p>
    </div>

    <hr/>

    <div class="section">
      <p class="scott"><strong>More information on OpenUsability is available at their <a href="http://www.openusability.org">web site</a>.  There will also be a <a href="http://www.linuxtag.org/typo3site/freecongress-details.html?&amp;L=1&amp;talkid=198">talk</a> at <a href="http://www.linuxtag.org/">LinuxTag</a> later this week, as well as an OpenUsability <a href="http://www.linuxtag.org/typo3site/projects.html?&amp;L=0">booth</a>.</p>
      <p>More information on KPDF can be found at its <a href="http://kpdf.kde.org/">web site</a> or in its recent feature as the <a href="http://dot.kde.org/1115581145/">application of the month</a>.</strong></p>
    </div>







