---
title: "KDE-Files.org Launched"
date:    2005-05-16
authors:
  - "jriddell"
slug:    kde-filesorg-launched
comments:
  - subject: "Great, great, great"
    date: 2005-05-16
    body: "This is really a great idea!\n\nThe KDE community rocks! \n\nMany thanks to the guy behind all this, Frank Karlitschek. He is doing an amazing job."
    author: "ac"
  - subject: "Re: Great, great, great"
    date: 2005-05-16
    body: "I agree.  I am truly astonished and pleased beyond degree at how the KDE community is evolving.  This is one of the most vibrant open source communities, and many thanks to people like Frank are due."
    author: "anon"
  - subject: "Re: Great, great, great"
    date: 2005-05-16
    body: "I agree , really great idea . \neach day it's become more fun to use kde."
    author: "maor"
  - subject: "The KDE Community seems stronger than ever!"
    date: 2005-05-16
    body: "I am truly amazed to see KDE going from strength to strenght, despite previously some many rich companies (Sun, RedHat, Ximian,...) having openly opposed KDE and bolstered the competion (which seems to be deeply split now over their future way to go). And I mean it is really a pity that the other big desktop seems so deeply in touble -- after all, they introduced many good ideas also from which KDE development benefitted, and they were always a competitor that required efforts on the KDE side to stay ahead.\n\nIt seems the next big competitors on the desktop to compare KDE to are Mac OS X and Longhorn...."
    author: "anonymous"
  - subject: "Re: The KDE Community seems stronger than ever!"
    date: 2005-05-16
    body: "Indeed, the last thing KDE needs to worry about is lack of competition."
    author: "Ian Monroe"
  - subject: "Re: The KDE Community seems stronger than ever!"
    date: 2005-05-16
    body: "Troll."
    author: "ASD..."
  - subject: "Very, very, very cool!"
    date: 2005-05-16
    body: "Thanks Frank! This means that users can contribute even more to improving the KDE desktop, just by using its applications :) . I hope Edutainment and Kile get added soon, too."
    author: "Claire"
  - subject: "Just a suggestion"
    date: 2005-05-16
    body: "It would be nice to add a section for Scribus templates."
    author: "Ignacio Monge"
  - subject: "That's hot"
    date: 2005-05-16
    body: "Wouldn't it be an idea to use KNewStuff together with this website? Now we have a central place for storing this kind of files, so we don't need 100 different websites for 100 applications which use KNewStuff."
    author: "Bram Schoenmakers"
  - subject: "Re: That's hot"
    date: 2005-05-16
    body: "There are two different requirements and purposes at play.\n\nKNewStuff is for application extensions that wouldn't be shipped with the application for some reason. The educational module has language extensions outside of the i18n system where it doesn't make sense to package them all together. KStars has star maps with license issues. These are best handled by the maintainers.\n\nKDE-Files.org is for user generated templates and the like.\n\nThey may come together at one point but there are different requirements for each.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: That's hot"
    date: 2005-05-16
    body: "at the core they are essentially the same, however: grabbing new/updated content from a centralized server.\n\nwe've learned a fair amount about what GHNS needs to support in the last several months and should be well capable of serving both the needs of extension distribution, content updates and add-on browsing in a future release."
    author: "Aaron J. Seigo"
  - subject: "Re: That's hot"
    date: 2005-05-16
    body: "> Wouldn't it be an idea to use KNewStuff together with this website?\n\nRead the story on kde-{files,apps,look}.org, it's mentioned there as future option."
    author: "Anonymous"
  - subject: "Kivio"
    date: 2005-05-16
    body: "What about kivio stencils? It would be great to have more stencils available. \n\nThe kghns-engine is really cool and will be more and more useful with kde-files.org. Great job! :)"
    author: "mOrPhie"
  - subject: "Website addresses"
    date: 2005-05-16
    body: "I'm just curious, why kde-look.org, kde-apps.org and kde-files.org  instead of themes.kde.org, apps.kde.org and files.kde.org? I know that themes.kde.org exists, but what about the others?"
    author: "slomoVizion"
  - subject: "Re: Website addresses"
    date: 2005-05-16
    body: "Because they prefer to be an independent website? Not that having a kde.org domain is very limiting but you have some rules you need to obey. We have many examples of independent websites like KDE-apps.org but also local group websites like www.kde.de/www.kde.org.uk/www.kde-france.org. These websites use their own website style and do their own branding. \n\nCiao' \n\nFab"
    author: "Fabrice"
  - subject: "Amazing"
    date: 2005-05-16
    body: "I'm a KDE user in a daily basis for only 6 months. And all the progress I have seen in only this 6 months is quite amazing. KNewStuff rocks, and I just can say that you're in the right way. Thanks for this great work."
    author: "Ivan"
  - subject: "Weird selection of file types"
    date: 2005-05-16
    body: "I just wanted to upload a template I did for our LUG some time ago, but the file selection for the upload is quite strange: You can directly upload KWord, KSpread etc. files, even RPMs but you cannot directly upload OpenOffice files - you need to zip them (although they are already zipped). Can someone change this?"
    author: "Daniel"
  - subject: "Re: Weird selection of file types"
    date: 2005-05-16
    body: "This seems to be a bug. I will fix it this evening.\n\nCheers\nFrank"
    author: "Frank"
  - subject: "great stuff!"
    date: 2005-05-16
    body: "kde rocks all the way!\nsomeone should really add this to links on the main page. :)\n\n\u00bb Community\nKDE Apps\nCommunity Wiki\nDeveloper Journals\nKDE Forum\nKDE Look\nPlanet KDE\n\nkeep up tha good work ;)"
    author: "Mark Hannessen"
  - subject: "Kmail and KHotNewStuff"
    date: 2005-05-16
    body: "when will we be able to use KHotNewStuff in kmail to get scripts for getting webmail?"
    author: "Anon"
  - subject: "Re: Kmail and KHotNewStuff"
    date: 2005-05-17
    body: "Well, when do you plan to send us the patch to implement this :)"
    author: "ac"
  - subject: "Re: Kmail and KHotNewStuff"
    date: 2005-05-17
    body: "Why not use freepops instead? There's no point to re-invent the wheel."
    author: "Bram Schoenmakers"
  - subject: "additional field"
    date: 2005-05-17
    body: "I think it would be great if uploading any content you have to specify in with language it is. Also every user should be able to filter the content by language. If this is not gonna happen in a few weeks it's really hard to find content which is in your own language.\n\nred"
    author: "redrat"
  - subject: "Re: additional field"
    date: 2005-05-18
    body: "Good Point"
    author: "Macavity"
  - subject: "Security?"
    date: 2005-05-17
    body: "Are there any statements if and how it will be granted that nobody con upload malicious code to KDE-Files? I just remember there were some discussions about that when KHotNewStuff was introduced some weeks ago, but I don't know if the developers agreed on some standards to keep the system \"clean\" --- and I'm bit surprised to see the site already online.\n\nOf course, one could already upload a compromitted screensaver or window decoration to kde-look, but that would still need a user to start the make process and install the infected code. But KDE-Files' audience are the real users. \n\nAfter we all made our jokes about outlook users who doubleclicked on every attachment, KHotNewStuff now offers KDE users to install add-ons to the KDE applications with just one mouse click?! I hope that these add-ons have to pass some kind of security audit, otherwise KHotNewSTuff is a really bad idea! End even if they have to do, just think of the latest firefox security hole, ... I'm not to sure if the whole idea is really that good."
    author: "furanku"
---
KDE's latest community website <a href="http://www.kde-files.org/">KDE-Files.org</a> has gone online. The site is a central exchange platform for all sorts of documents and document templates. Users can collaborate, discuss, vote and share documents.  Some examples of files you could share are your jogging result spreadsheets, OpenOffice.org presentation templates or <a href="http://www.koffice.org/kexi/">Kexi</a> DVD Databases. In the future users will be able to use the <a href="http://www.kstuff.org/">KHotNewStuff system</a> to access KDE-Files.org directly from the applications.
<!--break-->
<p>The user database of KDE-Files.org is shared with <a href="http://www.kde-look.org/">KDE-Look.org</a> and <a href="http://www.kde-apps.org/">KDE-Apps.org</a>.</p>

<p>Currently KDE-Files.org accepts KOffice and OpenOffice.org templates, <a href="http://korganizer.kde.org/">KOrganizer</a> Calendars, <a href="http://uml.sourceforge.net/index.php">Umbrello</a> files, <a href="http://edu.kde.org/kstars/">KStars</a> data and <a href="http://quanta.kdewebdev.org">Quanta</a> scripts.  If you are an application maintainer and think KDE-Files.org would be useful for file related to your program please contact <a href="http://www.kde-files.org/usermanager/search.php?username=Frank">Frank Karlitschek</a>.  Developers are also encouraged to make use of the KHotNewStuff feeds from KDE-Files.org so your users can download the latest and greatest files at the click of a button, contact Frank for details.</p>

<p>KDE-Files.org is now <a href="http://www.kde-files.org/ad/advertising.php">looking for site sponsors</a>.</p>





