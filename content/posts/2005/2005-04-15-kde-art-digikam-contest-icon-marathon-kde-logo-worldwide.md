---
title: "KDE Art: digiKam Contest, Icon Marathon, KDE Logo Worldwide"
date:    2005-04-15
authors:
  - "jtheobroma"
slug:    kde-art-digikam-contest-icon-marathon-kde-logo-worldwide
comments:
  - subject: "bring art into the heart of KDE"
    date: 2005-04-15
    body: "the KDE art community is so vibrant these days. and with people like Theobroma and Frank enabling it with these websites and initiatives to help bring artists and developers together, future releases of KDE are only going to look that much beautiful, and done right will also help make it more usable.\n\ni was lucky enough to get a sneak preview and tour of the new artist's web site and ... WOW ... it's nice. great work Janet!\n\nthese are all important steps, IMHO, to bring art closer to the core of KDE so as to allow people with artistic talent and skills to bring their passions to bear as much as the coders do.\n\nnow ...... to go take some pictures of KDE at the Calgary Tower and the Olympic ski jumps =)"
    author: "Aaron J. Seigo"
  - subject: "Re: bring art into the heart of KDE"
    date: 2005-04-15
    body: "yes, it's so amazing to see so many artists with such enthusiasm! My own KDE-Edu programs KLettres and KHangMan will have a great new look while being also improved in usability for 3.5. Today I got for KLettres sounds for the Luganda language which is an African language. Thus I requested my dedicated artist Danny an African background to match this :-) See how worldwide KDE is known, used and improved by contributors! \nThanks Janet and Frank for your great involvement, thanks to Danny for his dedicated work with the KDE-Edu team and thanks to all the artists that make the difference and bring a smile on my face when I start KDE!"
    author: "annma"
  - subject: "Re: bring art into the heart of KDE"
    date: 2005-04-16
    body: "The improvements in the artwork since I've been involved in KDE is remarkable. Kudos to those who have built a community of artists.\n\nDerek"
    author: "Derek Kite"
  - subject: "The WORST KDE Application of All Time: KREC"
    date: 2005-04-15
    body: "Since 2 years KRec is not practical for doing Large Audio Recording, and it is the only KDE's Recording tool...\n\nI'm using Slackware 10.1 and KDE 3.4, Still I feel that KRec is just useless for Audio Recording except for a REAL stupid user.\n\nWhy, you would ask!?\n\nThis is why:\n\n1. TOO MUCH ABUSE OF DISK SPACE: \n  a. RAW data takes a lot of disk space\n  b. Imagine how much disk space will KREC's RAW (temporary file) will take to record a 2 hour radio? broadcast, or a movie audio (using arts) for 111 minutes?) \n\nCan't you do Direct-to-Disk mp3 or ogg encoding like SOX!!!\n\n2. WASTAGE OF Precious TIME:\n   a. Time is required to Record Audio.\n   b. And Converting/Exporting requires MORE time and that's outrageous.\n      (file:///tmp/kde-rizwaan/krecidIUdb.tmp to file:///home/rizwaan/hello.krec)\n   c. And still your KREC file is not usable in Media Players!!! What a GREAT Application format!\n\n3. Confusing Interface:\n   a. What doesn \"Attack\", \"Release\", \"Thres\", \"ratio\", \"output\" means?\n   b. Bypass? surgery or what?\n\nLook, You may be a great programmer/developer, and you are developing using the greatest technology (arts) but definitely you are not wise enough to understand what an application is supposed to do:\n\n1. Getting things done!!! [ this is where you're stuck!!! by my own way]\n2. Getting it done in Time!!! (remember the cliche 'Time is Precious'?)\n3. Getting it done Efficiently!!! \n4. OUT PUT must be usable instantly! \n\nThank you! and I really wish and hope that you make KRec USABLE for Novice-to-Average users!!! And my sincere apologies for any bad feelings!!!\n\nYours Friendly,\nRizwaan.\n"
    author: "Fast_Rizwaan"
  - subject: "Re: The WORST KDE Application of All Time: KREC"
    date: 2005-04-15
    body: "Did you consider to file a bugreport or contact the author of krec about hour concerns?\n"
    author: "ac"
  - subject: "Re: The WORST KDE Application of All Time: KREC"
    date: 2005-04-15
    body: "looks like your talking to meeee ??? why ? i am just a normal reader of kdenews :-( i dont know your post wasted my PRECIOUS time :>"
    author: "chris"
  - subject: "One word"
    date: 2005-04-15
    body: "KRadio\nhttp://kradio.sf.net\n\nUnfortunately development stalled but even in this state\nit is perfect app. Compilation is troublesome (you need latest snapshot\nwith patch for 3.4...)"
    author: "m."
  - subject: "Re: The WORST KDE Application of All Time: KREC"
    date: 2005-04-15
    body: "Look, you may be great at ranting about an application in a completely unrelated web forum, but you are definitely not wise enough to understand what a user of free software is supposed to do:\n\n1. Be grateful that someone else but his/her time and effort into an application and was willing to give it to you for free.\n2. Contribute to the development by\n- filing bug reports and wished at the appropriate place\n- contributing code/documentation/artwork/translations/whatever to the project\n"
    author: "unbelievable"
  - subject: "Re: The WORST KDE Application of All Time: KREC"
    date: 2005-05-08
    body: "I am still trying to record a 30 minute radio broadcast with KRec (I started a couple of hours ago) and I understand the emotion behind what you said, though I must say it seems a little blunt. \n\nI was aware of the shortcomings of KRec before I started (from gossip around the net) but it is the only game in town, that I can find. Though there are many problems with it, at least it is getting the job done. Perhaps later when I get some spare time I might have a look at the code and see if I can find solutions to my problems.\n\nTo your list of things that would be nice to improve I would add the apparent limit of 10 minutes recording time I keep bumping into. That makes the time taken to export the 10 minutes to .wav before starting the next part VERY tedious indeed. Another thing you didn't mention was that it doesn't always save your recording, even if it thinks it has. I have two empty .krec files to prove it.\n\nIf I could have got Realrekord to work at all then believe me, I would not be struggling with KRec, but there it is. Thank goodness for KRec."
    author: "Richard"
  - subject: "Re: The WORST KDE Application of All Time: KREC"
    date: 2006-10-07
    body: "heard of Audacity?\nand stfu!\ndo your own KRec, if you can!\noh.. stop! you can't!\n\nyou are the very STUPID user as stated in your post."
    author: "the_killer"
  - subject: "openclipart.org"
    date: 2005-04-15
    body: "I would like to see more people helping on openclipart.org especially there is need for a PHP/PERL guru. But even more SVG-cliparts are very welcome."
    author: "MaX"
  - subject: "Re: openclipart.org"
    date: 2005-04-15
    body: "Year, I whould like to see a nice small KDE-ClipartBrowser for the openclipart.org  cliparts."
    author: "Susi"
  - subject: "Re: openclipart.org"
    date: 2005-04-15
    body: "seems wise to wait until Milestone 18, unless their document management system in Milestone 13 has a sane web service API.\n\nperhaps some of their users could even recommend that they start providing a GetHotNewStuff feed =)"
    author: "Aaron J. Seigo"
  - subject: "Re: openclipart.org"
    date: 2005-04-15
    body: "It looks like there aren't any KDE people involved in that projekt. I think it would be wise to have some there, so that e.g. GetHotNewStuff or kde-look.org is direct integreted into the document management system."
    author: "MaX"
  - subject: "Re: openclipart.org"
    date: 2005-04-16
    body: "let's see if we can fix that.\n\nhttp://aseigo.blogspot.com/2005/04/ocal-and-building-bridges.html"
    author: "Aaron J. Seigo"
  - subject: "Re: openclipart.org"
    date: 2005-04-16
    body: "For people that are lazy, heres a link to Aaron's post\n\nhttp://lists.freedesktop.org/pipermail/clipart/2005-April/002754.html"
    author: "Sam Weber"
  - subject: "Why?"
    date: 2005-04-15
    body: "Sorry, but what's the point of KDE Everywhere? I wouldn't mind seeing pictures from KDE where it featured in events or conferences, but taking a photo of a KDE logo somewhere in the world just isn't interesting and looks pretty stupid in my opinion (I don't mean to be harsh, but it does look really silly). :-S"
    author: "Bob"
  - subject: "Re: Why?"
    date: 2005-04-15
    body: "I guess you shouldn't take everything so serious. It's just fun. If you don't like it, don't participate. \n\nSounds easy, doesn't it??"
    author: "ac"
  - subject: "Why \"Why?\" ?"
    date: 2005-04-15
    body: "I was about to post a picture of me wearing nothing but a KDE logo, but I take it this won't be appreciated anymore :( I dare not offend the BOB! The horror!"
    author: "Max Howell"
  - subject: "Re: Why?"
    date: 2005-04-15
    body: "Somehow I have to agree. Yes, it's rather stupid if proposed that way.\n\nWhat I'd find amusing would be doing some \"KDE warchalking\". Just what people do with wireless, but pointing to kde users/companies. Now that would be fun to go anywhere on this world and see a chalked wall saying \"kde users inside\" or \"kde business!\""
    author: "~~"
  - subject: "Re: Why?"
    date: 2005-04-15
    body: "it's called having fun. some of us enjoy it. you don't have to, though, and that's totally cool. you don't have to look at them or add a picture of your own; you can ignore it completely and imagine that it doesn't exist. that way those of us who get a chuckle out of it can go on having our fun, too =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Why?"
    date: 2005-04-15
    body: "Point well delivered. The choice of words was good too."
    author: "charles"
  - subject: "Re: Why?"
    date: 2005-04-16
    body: "To be fair to me, I didn't say you should get rid of it, I asked what the point of it was (maybe there was one but I didn't know it?), offered what I think would be a similar but more interesting alternative and included \"in my opinion\" so I didn't sound commanding. Just because I say an idea doesn't appeal to me, doesn't mean people should start implying I don't like others having fun. :-S"
    author: "Bob"
---
The KDE artist community has been busy recently. The winners of the <a href="http://www.kde-look.org/news/index.php?id=155">digiKam contest</a> have been announced, <a href="http://www.kde-look.org">kde-look.org</a> has had a major update and the KDE logo is on a worldwide tour.   Coming soon are The First Annual Icon Marathon, a completely new and improved KDE Artist website and introducing Kollaboration, where your art meets their code!  Read on for details.













<!--break-->
<h2>Improved KDE-Look and KDE-Apps</h2>

<p><a href="http://dot.kde.org/1075252150/">Frank</a> has updated the server and software behind <a href="http://www.kde-look.org">KDE-Look.org</a> and <a href="http://KDE-apps.org">KDE-Apps.org</a>. The <a href="http://www.kde-look.org/news/index.php?id=173">new server</a> is faster and sports much more capacity.  The new software has several improvements over the old version.  The core is completely rewritten in PHP 5 for more speed and flexibility and a new caching system is reducing the MySQL load.  The new frontend features are:</p>

<ul>
<li>New content categories.</li>
<li>A new content filtering system. You can now filter out wallpapers.</li>
<li>A content subscription system to keep you up to date with your favourite applications.</li>
<li>Plus a lot of bugfixes and small improvements.</li>
</ul>
 
<p>The new categories include:</p>
<ul>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=7">SVG Wallpapers</a></li>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=8">Theme Manager</a></li>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=23">Emoticon Themes</a></li>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=24">Kopete Styles</a></li>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=40">KDM Themes</a></li>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=50">Howto Documents</a></li>
<li><a href="http://www.kde-look.org/index.php?xcontentmode=44">KDE Everywhere</a></li>
</ul>
 
<h2>digiKam Contest</h2>

<p>The winners from the <a href="http://kde-apps.org/content/show.php?content=9957">digiKam</a> contest are... <a href="http://www.kde-look.org/usermanager/search.php?username=Skeezo">Skeezo</a> and <a href="http://www.kde-look.org/usermanager/search.php?username=frodrigo">Frodrigo</a>.  Skeezo and Frodrigo will both be receiving a 256MB memory card and a t-shirt from <a href="http://www.cafepress.com/revelinux">Revelinux Gear</a>.</p> 

<p><img src="http://kde-look.org/content/pre1/20907-1.png" width="320" height="240" alt="Superimpose Template - Rose-Rose" /> <br />
<a href="http://www.kde-look.org/content/show.php?content=20971">Superimpose Template - Rose-Rose</a></p>

<p>They submitted the bulk of the new <a href="http://extragear.kde.org/apps/digikamimageplugins/superimpose.png">Superimpose Templates</a> that will make creating images with digiKam even more fun! Thanks goes out to all who were involved. digiKam is still accepting Superimpose Templates for their next release, please e-mail your images to the <a href="http://lists.sourceforge.net/lists/listinfo/digikam-users">digiKam mailing-list</a>. For a detailed explanation of how to make Superimpose Templates see the <a href="http://www.kde-look.org/news/index.php?id=155">contest rules</a>.</p>

<p><img src="http://kde-look.org/content/pre2/20834-2.jpg" width="400" height="300" alt="Superimpose Template - Tux Party" /> <br />
<a href="http://www.kde-look.org/content/show.php?content=20834">Superimpose Template - Tux Party</a></p>

<h2>Icon Marathon</h2>
 
<p>Have you ever wanted to see your artwork in the official KDE? Now here is your chance with The First Annual KDE Icon Marathon! At the request of the <a href="http://aseigo.bddf.ca/cms/1134">Appeal project</a> the marathon is now scheduled for May. The challenge will soon go out to all artists to make as many new KDE icons as possible in a two month period. As a side note our contests have attracted some pretty generous sponsors, which means juicy prizes!</p>

<p>Special note to KDE developers: this is the time to look over your apps and see if you have any missing icons. Please email your icon requests to <a href="http://mail.kde.org/mailman/listinfo/kde-artists">the kde-artists mailing list</a> and include as much detail as possible.</p>
 
<p>The marathon will take place on the new KDE Artists website which will be debuting very soon. This will also mark the roll-out of Kollaboration where your art meets their code! If you have ever wanted to see your window decoration or other art work in a working application Kalloboration will help  that and more. This all started last year with a discussion on KDE-Look which lead to Theobroma and landy's <a href="http://theobromas.blogspot.com/2005/01/kollaboration.html">Kollaboration proposal</a> to the KDE community at large. We have all worked very hard to bring this new concept to life. Looking out for Kollaboration on the new KDE Artists website.</p>

<h2>KDE Logo Worldwide</h2>
 
<p>Last but not least our new <a href="http://www.kde.org/stuff/clipart/">KDE logo</a> is going on a voyage around the world. Visit  <a href="http://www.kde-look.org/index.php?xcontentmode=44">KDE Everywhere</a> to see its travel pictures and add your own.</p>

<p><img src="http://www.kde-look.org/content/pre1/22571-1.jpg" alt="KDE Logo in Germany Photo" width="225" height="300" /><br />
<a href="http://www.kde-look.org/content/show.php?content=22571">KDE Logo in Berlin</a></p>











