---
title: "IBM Publishes Migration Guide From Windows to Linux"
date:    2005-01-12
authors:
  - "vfern\u00e1ndez"
slug:    ibm-publishes-migration-guide-windows-linux
comments:
  - subject: "Huray."
    date: 2005-01-12
    body: "Uuuuuuuu ... first post\n\nGood thing. Not so much as a valuable document as a signal to enterprises."
    author: "Adrian"
  - subject: "Strange migration"
    date: 2005-01-12
    body: "Why does IBM recommend a migration from an operating system to a kernel?\n\nIt would be much more logical to migrate to another operating system like GNU."
    author: "Mr. Ouzie-Bouzie"
  - subject: "Re: Strange migration"
    date: 2005-01-12
    body: "I kde.news the right forum for this particular debate? I think not.\n\n"
    author: "Apollo Creed"
  - subject: "Re: Strange migration"
    date: 2005-01-12
    body: "I think it is the right place to discuss. Any mention of KDE as a desktop alternative and KDE tools by \"better-known\" companies and media should be encouraged. Frankly this is what KDE is lacking as compared to GNOME. A little bit of hype never hurt anyone :-)"
    author: "Kanwar"
  - subject: "Re: Strange migration"
    date: 2005-01-12
    body: "I think you don't get  it. Apolo Creed is taking about the fact that Mr. Ouzie-Bouzie is bitching about calling linux gnu/linux. Given his name \"Mr. Ouzie-Bouzie \", I guess he's probably trolling."
    author: "Pat"
  - subject: "Re: Strange migration"
    date: 2005-01-13
    body: "He is clearly trolling, all here know the correct term are KDE/Linux."
    author: "Morty"
  - subject: "Re: Strange migration"
    date: 2005-01-12
    body: "> Why does IBM recommend a migration from an operating system to a kernel?\n\nThe migration guid is hardly specific to Linux or to GNU. Almost everything mentioned in the migration guide is true for BSD as well. It would therefore best be named: \"Migrating Windows to KDE or GNOME\".\n\nFor a worker in a company that has migrated from Windows to KDE, it doesn't matter whether the sysadmin chose to run Linux+GNU or BSD or whatever below the user interface."
    author: "falonaj"
  - subject: "Re: Strange migration"
    date: 2005-01-14
    body: "So how many people work in your company? Any side-effects of this?"
    author: "Adrian"
  - subject: "Re: Strange migration"
    date: 2005-01-13
    body: "linux has a kernel, but the term is now used by all (except Mr Ouzie-Bouzie and some FSF stalwarts) to describe the complete distro that is distributed by the likes of Red Hat, Mandrake, Debian, &c.\n\nI remember several years ago hearing Free Software Foundation folk explaining that the GNU operating system was going to be based on Carnegie-Mellon's hurd, not linux, then later hearing Richard Stallman explaining that linux should be called Gnu linux, but the linux community stuck with the FSF's original position, that the operating system could only be called Gnu if it had the hurd as its kernel. Since the linux community doesn't have corporate positions on topics such as this, there isn't any mechanism for changing it. linux has been the name of the operating system for more than ten years now. When folk wish to distinguish the kernel as such they normally refer to it as \"the linux kernel\"."
    author: "Richard Royston"
  - subject: "Re: Strange migration"
    date: 2005-01-14
    body: "Nice post!\n\n I'm tired of this old debate, and I think it does more harm then good to keep bitching over it. As in: We all want GPL'ed (or similar) software to be the defacto of the world. But keeping this old flamewar alive is actually spreading FUD whithin our own ranks.\n I know this as I recently had the following question asked: \"Should I go for a GNU/Linux or a pure Linux disto?\". Needless to say I nearly laughed out loud at first, but when I came to think of it, it actually makes sense... from a newbee's point of view :-/\n\nQOTD: \"Burry the hatched! Democracy has decided that the name of the child is Linux, but all of us in the know, know where it all began and what's really the backbone of it all\". ;-D\n\n~Macavity\n\n--\nAnd remember kids: \"If it ain't broke.. Hit it again!\"\n"
    author: "Macavity"
  - subject: "Re: Strange migration"
    date: 2005-01-14
    body: "\"It would be much more logical to migrate to another operating system like GNU.\"\n\nThen what would one use as a kernel?  HURD?  Then, you'd have to use GRUB; but LILO is so much prettier and less intimidating.\n\nJust a little bit of irony.  Linux is, whether Stallman likes it or not, common parlance for a system that uses the Linux kernel; the Linux kernel itself is called The Kernel, a name not used for other kernels.\n\nPlus, GNU/Linux is unfair to the other tools that people use.  Beside glibc, I doubt there are any other GNU tools that the average desktop user uses.  Even the display manager will hide bash from the non-sysadmin.  But X, KDE, Mozilla and other tools are integral to desktop and corporate Linux."
    author: "Keith"
  - subject: "Re: Strange migration"
    date: 2005-01-19
    body: "Once more I come to think that the german word \"Klugscheisser\" has the aggression I miss on the english \"wisecracker\"\n"
    author: "Dexter Filmore"
  - subject: "Good News!"
    date: 2005-01-13
    body: "I like the way Appendices C and D talk about application scripting, Kommander, kdebindings, DCOP etc and some of the features that make KDE a great scripting and development environment for writing custom (ie in house) applications. \n\nI think KDE is usually seen as a bunch of pre-written applications with accompanying desktop, and not enough emphasis is given to the fact that it's also a killer application framework. And especially if you don't necessarily have to write your apps in C++ via the various projects in kdebindings.."
    author: "Richard Dale"
  - subject: "Migrate to BSD"
    date: 2005-01-13
    body: "KDE runs on it ..."
    author: "ooga"
  - subject: "Linux usability at its best!"
    date: 2005-01-13
    body: "Wonderful.  IBM tells corporate users to use GIMP - surely the face of linux usability!  Just wait till they start trying to draw lines.\n\nWell, at least they're ahead of Novell which also tries to get business users to unmount floppies and cds..."
    author: "c"
  - subject: "Re: Linux usability at its best!"
    date: 2005-01-13
    body: "If you want to *draw* lines, use a *drawing* application like Inkscape ;-)\n\nFor all its many usability flaws, one nice thing about The GIMP is that it doesn't follow Adobe's tendency to mix painting and drawing tools together."
    author: "Tom"
  - subject: "Re: Linux usability at its best!"
    date: 2005-01-13
    body: "http://www.newtown.tased.edu.au/computingweb/gimp/lineeffects.htm\n\nSeems pretty simple to me.\n\nOr use the Gimp's vector drawing abilities."
    author: "Leo Spalteholz"
  - subject: "Re: Linux usability at its best!"
    date: 2005-01-15
    body: "> Seems pretty simple to me.\n\nAnd that's why there needs to be a tutorial on it."
    author: "fsdf"
  - subject: "funny"
    date: 2005-01-20
    body: "The funny thing is that the document has been created by the WINDOWS version of  the acrobat distiller."
    author: "Octav"
---
<a href="http://www.ibm.com/">IBM</a> has published a guide to help <a href="http://enterprise.kde.org/">enterprises</a> migrate their systems from Windows to Linux. It pays a special attention to the users who want to migrate from MS Office to equivalent Linux-based products. The guide also covers the use of <a href="http://extragear.kde.org/apps/kiosktool/">Kiosk tool</a> to limit the configurability of the KDE desktop and contains an appendix about desktop automation and scripting talking about KJSEmbed, DCOP, Kommander and other tools. The title of the guide is "<a href="http://www.redbooks.ibm.com/redbooks/pdfs/sg246380.pdf">A Practical Planning and Implementation Guide for Migrating to Desktops Linux</a>" and may be directly downloaded in PDF.
<!--break-->
