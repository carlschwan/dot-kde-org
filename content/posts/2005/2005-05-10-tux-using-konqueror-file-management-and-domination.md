---
title: "TUX: Using Konqueror for File Management and Domination"
date:    2005-05-10
authors:
  - "jriddell"
slug:    tux-using-konqueror-file-management-and-domination
comments:
  - subject: "Lots of attention"
    date: 2005-05-09
    body: "Seems like KDE is getting a lot of attention lately.\n\nIs KDE finally konquering the desktops?"
    author: "KDE User"
  - subject: "Re: Lots of attention"
    date: 2005-05-09
    body: "> Is KDE finally konquering the desktops?\n\nYes"
    author: "bonobo"
  - subject: "Re: Lots of attention"
    date: 2005-05-10
    body: "XFCE is growing too! That's great, since for low end PC like mine, it really is an appropriate choice. However, I'm still using E, for speed and good compatibility with Rox-Filer. (And yes, KDE is good too, definitely faster than Gnome on my Athlon 600MHz)"
    author: "oliv"
  - subject: "Re: Lots of attention"
    date: 2005-05-09
    body: "it already did, its the most used desktop under unix... of course it has to beat windows, i guess. but that has more to do with the marketing/buisiness techniques from microsoft than with just quallity. if everyone was free to choose its kernel, DE and all other software (eg if it was just like \"apt get install NT-kernel\") most ppl would use KDE I think. but you can't choose, as you are locked in to M$. I don't have OpenOffice, nor firefox, nor flash or other non-free software. well, its doable, but hard sometimes - not being able to open word documents, or view certain webpages.\n\nso. sometimes, i'm having a hard time. why? microsoft. yep, and i don't even use it - still it troubles me. basterds."
    author: "superstoned"
  - subject: "Re: Lots of attention"
    date: 2005-05-10
    body: "http://gplflash.sourceforge.net/ to view flash.\nhttp://www.koffice.org to read doc heck even openoffice2 is freesoftware if you run the fedora build compiled with gcc4 and gjc. :)"
    author: "Pat"
  - subject: "Re: Lots of attention"
    date: 2005-05-10
    body: "well, the reason I don't use OO is that its just damn slow, while its only advantage is it's slightly better at reading m$ word doc's.\n\nfor gplflash, well, doesn't work for me, mostly... and slows konqi heavilly. well, I just emerged it again, let's see how it works today :D"
    author: "superstoned"
  - subject: "Re: Lots of attention"
    date: 2005-05-13
    body: "Try AbiWord for *.doc files.  It's fast and works great 4 me.   Cheers."
    author: "joel"
  - subject: "Re: Lots of attention"
    date: 2005-05-13
    body: "you're right, abiword is a very nice wordprocessor. i think i'll emerge it..."
    author: "superstoned"
  - subject: "TUX Magazine"
    date: 2005-05-09
    body: "The TUX Magazine is very nice. Thanks for the link :)"
    author: "Tom"
  - subject: "Why do you prefer Konqueror over Krusader?"
    date: 2005-05-09
    body: "I am curious, why do people prefer to use Konqueror over Krusader for file management?\nhttp://www.konqueror.org/features/filemanager.php\nhttp://krusader.sourceforge.net/"
    author: "testerus"
  - subject: "Re: Why do you prefer Konqueror over Krusader?"
    date: 2005-05-09
    body: "0. Konqueror comes with KDE as the default file manager\n1. not everyone is familiar with nor likes twin-panel file managers\n2. Krusader's interface is even a bit more geeky than Konqueror's.\n\nthis is not to say that Krusader is not a good application, these are just the resasons i would guess for Konqueror being more popular."
    author: "Aaron J. Seigo"
  - subject: "Re: Why do you prefer Konqueror over Krusader?"
    date: 2005-05-09
    body: "i agree. krusader is cool, but i just don't need it - mostly i don't even use half of konqi's features. and i like the webbrowser-and-filemanager-in-one..."
    author: "superstoned"
  - subject: "Re: Why do you prefer Konqueror over Krusader?"
    date: 2005-05-09
    body: "3. It looks more like Windows Explorer (in the default setup). ;)"
    author: "ac"
  - subject: "Re: Why do you prefer Konqueror over Krusader?"
    date: 2005-05-10
    body: "well, that's sort of what i was getting at with point #1 =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Why do you prefer Konqueror over Krusader?"
    date: 2005-05-11
    body: "point [0] is very good. \nmaybe it's time to add another filemanager? to allow the users who need that kind of thing a good choice?\nhowever, comming to think about it, windows doesn't come with a twin-panel alternative either.\n;-)"
    author: "anonymous coward"
  - subject: "Re: Why do you prefer Konqueror over Krusader?"
    date: 2005-05-10
    body: "\"I am curious, why do people prefer to use Konqueror over Krusader for file management?\"\n\nBecause different people like different things? You may like Krusader, and Krusader is a fine app, no question about it. But that doesn't mean that EVERYBODY should prefer Krusader over Konqueror. The two apps do the same thing (manage files, although Konqueror can do other things as well), but they do it differently. Some people prefer the Konqueror-way of managing files, while others prefer Krusader-way of doing it."
    author: "Janne"
  - subject: "my problem with Konqueror"
    date: 2005-05-09
    body: "My problem with Konqueror is that, whenever I am in the `Web browsing` mode, and I click the `home` button, I am directed to my home directory [on the local file system]. While I do not doubt which mode I am in at that particular time, (Web browsing), I wonder why I am switched to my home directory. What is the purpose of \"Settings-->Load View Profile-->File Management\", which does the same thing? How would I get to my home page www.konqueror.org by clicking? Is there a way to change this?"
    author: "charles"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-09
    body: "> My problem with Konqueror is that, whenever I am in the `Web browsing` mode,\n> and I click the `home` button, I am directed to my home directory [on the\n> local file system].\n\nThat's no problem. That't the supposed behavior. When I click on the 'home' button I want to show up in my homedir and not in my prondir :)\n\n> How would I get to my home page www.konqueror.org by clicking? Is there a way\n> to change this?\n\nI doubt www.konqueror.org is your homepage but well who knows :)"
    author: "bonobo"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-09
    body: "kde-forum.org is a very nice place for those kind of questions :)"
    author: "Rinse"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-10
    body: "This bug is all about your problem: http://bugs.kde.org/show_bug.cgi?id=8333 This bug was reported in the year 2000."
    author: "ac"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-10
    body: "How would I get to my home page www.konqueror.org by clicking? Is there a way to change this?\"\nYou could bookmark the page, enable the bookmark toolbar in Konqueror and click the bookmark."
    author: "Claus"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-10
    body: "That is so typical. The Question was why the heck is there a filemanager mode and a web-mode but the \"home\" button always brings up the local $home.\n\nand the answer is that this is not a bug and 10 ways of working around this annoying behaviour.\n\nstill the question got no answer.\n\nagain. i'm using konqueror in filemanager mode. i'm pressing \"home\". my local $home get's shown. perfectly fine.\n\ni'm in web mode. i'm pressing \"home\". What i expect is to get to my homepage. not to my local $home. why is that so hard to understand?"
    author: "manito"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-11
    body: "It's not hard to understand. It should be the default behaviour because of consistency IMO."
    author: "c0p0n"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-11
    body: "It is not hard to understand.  It is, however, hard to understand why you think your opinion overrides mine; I expect to have a button do the same thing when I click it.\n\nYou can configure it to your preference, just don't expect that your preference should be default.  It's not \"the way it should be\" as you seem to think it is."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-11
    body: "The problem is: why the heck do we need a \"homepage\" button? who uses it? really, this \"homepage concept\" is something I can't understand, I think it is only a habit nowadays, because in every browser on earth there's a homepage button. But I don't see it usefullness. Do you need to go quiclky to a page on internet? bookmark it and put it in a bookmark toolbar. You're not limited to one shortcut.\n\nI for one would remove entirely the \"home\" button in web browser mode."
    author: "Davide Ferrari"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-12
    body: "Excuse me Davide, but this is not useless at all.  I am an IT administrator and I have a lot of IT related stuff to solve in a single day.  In order to find the appropriate solutions, I use Google as the entry point to the Internet all the time.  When you get used to it, you quickly find your way right to the point using Google. \n\nAnyway, you just cannot imagine how often I use the same website.  I will not bother myself with another toolbar eating up some precious screen display space just to be able to get to Google in a single click!  \n\nI totally agree with manito.  If there's no way to get an Internet home page with that button, I'll live with it.  If there is one and nobody seems to know the answer, I'll look for it until I find out by myself. No one can judge the preferences of anyone else as one have its own good reasons to want something specific. manito probably don't care at all about what you are thinking of that button!  Hehehe Yeah I know.  He most likely don't care more about MY opinion on YOUR post.  ;-)\n\nActually, it's been years I work with Windows and I am new to Linux.  I have quite a lot of questions to be answered, but for some time I will be looking to answer this one.  If I find something interesting, I'll post it here.\n\nFrank"
    author: "Frank Zyxy"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-12
    body: "Nah, still useless :)\n\nTyping \"gg:<what you want to look up at google>\" is faster than opening the google  homepage...\n\n"
    author: "Henrique Pinto"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-16
    body: "It seems everyone (stubbornly?) seemed to have missed it in this thread, but this IS considered a bug since the year 2000 and is discussed about here: http://bugs.kde.org/show_bug.cgi?id=8333"
    author: "ac"
  - subject: "Re: my problem with Konqueror"
    date: 2005-05-12
    body: "Hi charles, manito, and all of you desperately looking for a web home page with the \"Home\" button,\n\nI really don't know why I (and you) may have overlooked this, because now that I know, it is really obvious.  Open a Konqueror instance and go in the menu Settings -> Configure Konqueror.  Right when the window appears, you are in the \"Behavior\" settings. On top, it is written \"You can configure how Konqueror behaves as a file manager here\" and this is probably what have misled many of us.  In the \"Home URL\" text field, the should be a tilde by default.  This leads the Home button to your user's home folder.  Instead, just type any Internet address you want! (I recommend http://www.google.com ... ... yeah as if Google really needed another link to their website to improve their own PR!) Click the OK button and you're in business!  The only thing is that it will be your home page even if you're in \"File Management\" mode. Personally I don't mind at all.\n\n(I am using Konqueror on SuSE 9.2 Pro, just in case you would have a different version and could not find it there...)\n\nFrank, the Konqueror's conqueror"
    author: "Frank Zyxy"
---
Issue 2 of <a href="http://www.tuxmagazine.com/">TUX Magazine</a> includes a 6 page article on everything you could know about using Konqueror as a file manager.  Tux is a magazine for new GNU/Linux users and the article covers split windows, different file view modes and how to find lost files.  The magazine is free to subscribe to and available as a PDF download.


<!--break-->
