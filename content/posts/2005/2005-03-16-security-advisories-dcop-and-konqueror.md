---
title: "Security: Advisories for DCOP and Konqueror"
date:    2005-03-16
authors:
  - "wbastian"
slug:    security-advisories-dcop-and-konqueror
comments:
  - subject: "idn phishing a minor problem? "
    date: 2005-03-16
    body: "Is problem No 2 really a \"minor\" problem?\n\nThese homographic phishing attacks can have very bad consequences, and since everybody seems to talk about them they are also known to the bad guys. Since this is not a sophisticated high tech attack, the geeks among us tend to take security issues like that not too serious, but for the simple desktop user it doesn't matter if he gets a victim of a clever or a stupid hacker.\n\nI also don't understand the suggested solution. Can one explain in more details why this is a secure way to close that problem?"
    author: "furanku"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "From the Security Advisory (first link):\n\nFor KDE 3.4 KDE and the Konqueror webbrowser have adopted a\nwhitelist of domains for which IDN is safe to use because the\nregistrar for these domains has implemented anti-homographic\ncharacter policies or otherwise limited the available set of\ncharacters to prevent spoofing.\n"
    author: "ac"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "I know this idn phishing attacks are hard to prevent, but isn't that just saying \"We're not responsible anymore!\", or a bit more exaggerated \"Closing holes by blaming someone else\"?\n"
    author: "furanku"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "Sure, but the thing is, it's very easy for e.g. the german .de registrar to limit names to something like [a-z]+[\u00f6\u00fc\u00df] because that is what people in germany are interested in, registering domain names that match names as they are commonly used in their language.\n\nThe french .fr registrar may want to do the same for [a-z]+[\u00e9\u00e8\u00e0\u00e7] It's quite unlikely that someone in france will want to use a '\u00df' in its name.\n\nSo if the registrars take the responsibility to limit registrations to a sensible subset, the problem is quite easy to solve, while it is virtually impossible to solve at the broswer level alone."
    author: "Waldo Bastian"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-17
    body: "OK, this is not KDE specific anymore, but I don't think that is a practical solution. If I just think of immigrants, who want a homepage with a proper spelling of their name, international companies with branches in foreign countries, fan club sites for foreign artists, common foreign words with accents in languages which don't use that accent, etc ...\n\nIn summary: The world is too small nowadays to restrict .de websites to [a-z]+[\u00e4\u00f6\u00fc\u00df]. What about all the Ren\u00e9e's who want a homepage, what about www.citro\u00ebn.de, www.bj\u00f6rk-fanclub.fr, ..."
    author: "furanku"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-17
    body: "URL should ideally be easily enterable by citizens of the specific country's keyboard's standard layout. I'd consider the resulting restrictions 'natural', and the decision to handle this with a whitelist on KDE's site is an excellent solution to this problem."
    author: "ac"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-18
    body: "OK, thanks for the information!\n\nI still think that companies will use automatic redirection (www.citroen.de -> www.citro\u00ebn.de) to keep their holy corporate identity. But time will tell if the whitlisting will be effective, and hopfully I'm wrong ;)"
    author: "furanku"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "The solution basically delegates the responsibility to the domain registrar for a given TLD to make sure that nobody can registrate a domain name under that TLD that is visually similar to another domain name. IDN will only be enabled for TLDs with registrars that accept that responsibility."
    author: "Waldo Bastian"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "and how do you know that the tld registrar has done so?\nand how can you be sure that he actually does, instead of just pretending?\n"
    author: "Mathias"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-17
    body: "> and how do you know that the tld registrar has done so?\n\nUser feedback and what Opera uses.\n\n> and how can you be sure that he actually does, instead of just pretending?\n\nUser feedback"
    author: "Anonymous"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "The similar-looking IDN characters isn't any different than 0 and O looking similar or, for that matter, l and 1.  It's an issue, but with many benefits and no real way to get around it."
    author: "Keith"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "No-- it's less like 0 and O and more like 0 and 0.\n\nAn example:  http://www.shmoo.com/idn/"
    author: "jaykayess"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-03-16
    body: "Right. And that's why KDE no longer supports IDNs for .com domains."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: idn phishing a minor problem? "
    date: 2005-11-20
    body: "One of the biggest arguments out there against IDN is the Phishing argument. This has now largely been negated by ICANN banning registeration of mixed character scripts that are likely to cause confusion.\n\nHowever, another side of the story has been put. It is clear that many words in local characters have multiple representations when transliterated, often with more than one system, into Latin Characters. Each of these ambiguities offers an opportunity for a Phisher to conduct his Scam. Unlike the problem of eliminating the use of rogue cyrillics in Latin scripts, I see no easy solution to this problem, as each of the transliterations are in a single script and therefore legitimate. Indeed, each could have legitimate usuage, but surely often won't.\n\nThe argument therefore develops into the imperative of introducing IDN to prevent Phishing Scams in Asia. Without IDN, it is likely that the confusion over how to transliterate will result in a Pandemic of Scamming, the scale of which will be unprecidented! I feel that we should no longer be silent on the issue of Phishing as IDN undoubtedly will hold the moral high ground on this issue."
    author: "David Wrixon"
  - subject: "IDN"
    date: 2005-03-16
    body: "Would it be possible to highlight IDN characters in Konqui's status bar? Then at least we could see whether an a is really an a or bait on the end of a fishing line."
    author: "Flitcraft"
  - subject: "Re: IDN"
    date: 2005-03-16
    body: "Um, that would hardly stop phishing unless you happened to know what the highlighted characters meant."
    author: "Ian Monroe"
  - subject: "Re: IDN"
    date: 2005-03-16
    body: "True, but it might prevent clicking on a link that looks innocuous at first glance."
    author: "Flitcraft"
  - subject: "Re: IDN"
    date: 2005-03-18
    body: "I really like the idea of Konqi giving some sort of visual feedback like that.  Maybe accompanied by a status bar icon like the lock, with a popup text notification on mousever of the icon that would detail the potential risk.\n\nMark."
    author: "Mark Taff"
  - subject: "In other news..."
    date: 2005-03-16
    body: "KDE 3.4 was released today, see slashdot for more info."
    author: "Iuri Fiedoruk"
---
Three <a href="http://www.kde.org/info/security/">
KDE security advisories</a> have been released today that address minor problems that were brought to the attention of the KDE security team over the last few months. All these issues have been fixed in KDE 3.4, for older KDE versions <a href="ftp://ftp.kde.org/pub/kde/security_patches">patches are available</a>.




<!--break-->
<p><ol>
<li>The SUSE security team alerted us that a malicious local user can 
<a href="http://www.kde.org/info/security/advisory-20050316-1.txt">
lock up the dcopserver of arbitrary other users on the same machine</a>
by stalling the DCOP authentication process.</li>
<li>A problem that affected all browsers that support International Domain Names (IDN) and that has been widely publicized already is that the IDN support makes
Konqueror
<a href="http://www.kde.org/info/security/advisory-20050316-2.txt">
vulnerable to a phishing technique known as a Homograph attack</a>. This problem has been solved by only supporting IDN in those domains for which the domain registrar
enforces a homographic character policy.
</li>
<li><a href="http://www.kde.org/info/security/advisory-20050316-3.txt">
The dcopidlng script is vulnerable to symlink attacks</a>, potentially
allowing a malicious local user to overwrite arbitrary files of a user when
the script is run on behalf of that user. This only affects users
who compile KDE or KDE applications themselves.</li>
</ol>
</p>

