---
title: "DistroWatch: Jonathan Riddell Interviewed about Kubuntu"
date:    2005-08-01
authors:
  - "canllaith"
slug:    distrowatch-jonathan-riddell-interviewed-about-kubuntu
comments:
  - subject: "Simplebrowser"
    date: 2005-08-01
    body: "That simple browser interface looks really good. Mayby the idea could be worked a bit more and ingrated to Konqueror so that when browsing web the interface would autodetect the mode and konqueror would morph it self to a nice webbrowser :)"
    author: "Petteri"
  - subject: "Re: Simplebrowser"
    date: 2005-08-02
    body: "It's been in konqueror for ages. Just load it up (via the Settings>Load View Profile menu) and overwrite the Web Browser profile with it. I created a customized version of it probably a year ago, because i don't need 40 buttons ;)"
    author: "zmc"
  - subject: "Daily"
    date: 2005-08-01
    body: "I wanted to try out one of the daily releases, but they only work on 64bit cpus. whats up with that?"
    author: "drizek"
  - subject: "Well done..."
    date: 2005-08-02
    body: "Whilst Kubuntu has a ways to go to catch up with some of the more mature distros (most notibly with the bad package manager gui and a lack of system configuration tools) I really like what they've done so far.\n\nFast and light, up-to-date packages released often and nice, clean default settings set it apart from other distros.  Hardware and media detection is better than most other distros I've tried also.\n\nCan't wait for Breezy.  Keep up the good work.\n\nCheers,\n\nL."
    author: "ltmon"
  - subject: "Re: Well done..."
    date: 2005-08-03
    body: "> lack of system configuration tools\n\nIt has debconf. See \"man 7 debconf\" and \"man dpkg-reconfigure\" for further information. The man page even says that there's a KDE-based frontend for it, although it seems to missing from the Ubuntu Hoary package. It is present in the Debian Sarge package, though: http://packages.debian.org/cgi-bin/search_contents.pl?searchmode=filelist&word=debconf&page=26&number=4\n\nBTW, why do people keep complaining about {Ku,U}buntu and Debian lacking system configuration tools? Maybe debconf needs more promotion."
    author: "rqosa"
  - subject: "Re: Well done..."
    date: 2005-08-04
    body: "Quick, how do I reconfigure the screen resolution using debconf? And how did you know that?"
    author: "Roberto Alsina"
  - subject: "Re: Well done..."
    date: 2005-08-04
    body: "dpkg-reconfigure xserver-xorg\n\nI know that because, when one runs, for example, \"apt-get install foo\" where foo is a package which uses debconf, the debconf front-end being used will say \"Configuring foo\"; so, to change the cofiguration later, run \"dpkg-reconfigure foo\".\n\nI haven't ever used any of the graphical front-ends to apt, so I don't know if they provide an easy way to launch dpkg-reconfigure. Maybe some extra work is needed there to make debconf more accessible to people who don't like the shell."
    author: "rqosa"
  - subject: "Re: Well done..."
    date: 2005-08-04
    body: "I rest my case. It's quite non-discoverable.\n\nA regular user is never going to find it.\n\nAt *least* there should be a menu, or a list somewhere saying what configures what."
    author: "Roberto Alsina"
  - subject: "Re: Well done..."
    date: 2005-08-04
    body: "> A regular user is never going to find it.\n\nWhy not? It seems simple enough to me; if one wants to change the settings for the X server, one reconfigures the package which provides the X server. Furthermore, the configuration questions automatically appear during installation of the package, so they will definitely be seen at least once.\n\nI tried using Conectiva 10 once, and one thing about it that frustrated me was that packages might not be configured correctly after installing them (with the RPM version of apt-get), and the appropriate configuration management utility is in a separate package, and it can be difficult to find which one it is. Compared to this, the debconf way seems much more \"discoverable\" to me.\n\n> At *least* there should be a menu, or a list somewhere saying what configures what.\n\nThis is a job for Kynaptic or somthing similar. Maybe there's a program that already does this?"
    author: "rqosa"
  - subject: "Re: Well done..."
    date: 2005-08-04
    body: "Well, if I have to explain this...\n\na) The user doesn't want to change the settings for his X server. He wants to change the resolution of his display. The need to know that there is such a thing as an X server is useless complication already.\n\nb) How is the user who has heard the term \"X server\" going to guess that the package providing it is called xorg-xserver? By looking at the descriptions of the 1300 installed packages?\n\nc) In any useful setup, the questions will be asked during the installation of the system, not during an independent \"installing the X server\" step, so the user will probably *not* remember the exact name of the package. Not to mention the likely chance of the user not having installed the system himself.\n\nd) I never got to Conectiva 10 (I left the company around Conectiva 7 ;-) but I can tell you that on SuSE, the setup tool is always yast, and that in Fedora/RHEL/CentOS/Scientific Linux/TAOS it is always system-config-something and that they are all in the menu anyway.\n\nOn the other hand, on Debian, they are nowhere. Not on any menu. Not an icon in some place. You have to know the magic words. And there is no list that I know of the magic words you have to use for common config tasks. That's *not* discoverable.\n\nAnd no, it's not a job for kynaptic, because that would have the following problems:\n\na) It mixes the install-software and the configure-software functionality, which are not related at all in the users mind\n\nb) It doesn't make the useful configuration any easier than the useless.\nSince *every* package can be dpkg-reconfigure'd, the ones that actually are useful are a tree, lost in the forest. It's inconfigurability by obscurity.\n\nc) If there is such a tool, Debian sure doesn't mention it prominently enough, I think (I must be wrong ;-)\n\nIn short, you are thinking like a linux geek. I recognize it. I am one, too :-)"
    author: "Roberto Alsina"
  - subject: "Re: Well done..."
    date: 2005-08-05
    body: "> a) It mixes the install-software and the configure-software functionality, which are not related at all in the users mind\n\nDoesn't YaST do exactly the same?  \n\n"
    author: "cm"
  - subject: "Re: Well done..."
    date: 2005-08-05
    body: "Well, never claimed yast to be perfect! ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Well done..."
    date: 2005-08-05
    body: "> a) The user doesn't want to change the settings for his X server. He wants to change the resolution of his display. The need to know that there is such a thing as an X server is useless complication already.\n\nWell, with the XRandR extension, the screen resolution can be changed while the server is running, so there's no need to change a configuration file and no need for debconf. KDE has (if I understand correctly) a front-end to XRandR in the form of Control Center -> Peripherals -> Display, and this is included in Kubuntu and Debian, so one can't complain about lack of configuration utilities there.\n\n> b) How is the user who has heard the term \"X server\" going to guess that the package providing it is called xorg-xserver? By looking at the descriptions of the 1300 installed packages?\n\nLike I said before, debconf will display the name of the package it's configuring, so when the X server was first installed, every screen of configuration questions related to video settings would have had \"Configuring xserver-xorg\" at the top.\n\nAlso, it's not too difficult to search for it:\n\n$ apt-cache search 'X\\.Org' | grep erver\nxorg-driver-synaptics - Synaptics TouchPad driver for X.Org server\nxserver-xorg - the X.Org X server   <--- HERE IT IS\nxserver-xorg-dbg - the X.Org X server (static version with debugging symbols)\nlibroxen-zopegw - Zope relay module for the Roxen Challenger web server\nxcin - Chinese input server in X11\nxcin2.3 - Chinese input server (Big5) for XA+CV in X11.\nxcingb - [Obsolete] Chinese input server (GB) for Crxvt in X11.\nxprt - X print server\n$\n\n(In my case, I remembered it because when I \"apt-get dist-upgrade\"d from Warty to Hoary, apt-get listed \"xserver-xfree86\" as one of the packages which would be upgraded, and I had to tell it explicitly to get the X.Org server instead.)\n\n> c) In any useful setup, the questions will be asked during the installation of the system, not during an independent \"installing the X server\" step, so the user will probably *not* remember the exact name of the package. Not to mention the likely chance of the user not having installed the system himself.\n\nWell, that's what \"apt-cache search\" and \"dpkg -S\" are for.\n\nIn the case of someone else having installed the system, then would it be true that that same person is responsible for changing the settings? Ok, in the case of things like screen resolution changes, maybe the user can do that (especially because this doesn't require changing the X server config file anymore, see above), but look at these:\n\nhttp://www.gnome.org/projects/gst/screenshots/networking.jpg\nhttp://www.gnome.org/projects/gst/screenshots/boot.jpg\nhttp://www.gnome.org/projects/gst/screenshots/runlevel.jpg\n\nIn the business-desktop use case, the person(s) responsible for installing the operating system probably wouldn't even allow most users to change settings like these. Even where the user is allowed to change things, it's not very likely that someone who hasn't installed the system would want to be changing settings like these.\n\n> d) I never got to Conectiva 10 (I left the company around Conectiva 7 ;-) but I can tell you that on SuSE, the setup tool is always yast, and that in Fedora/RHEL/CentOS/Scientific Linux/TAOS it is always system-config-something and that they are all in the menu anyway.\n\nNot neccesarily. They're in the menu if they're installed, but what if they haven't been installed? When I installed Ark Linux, I didn't use the ISO (I have neither broadband nor a CD writer); instead, I installed Debian from floppies, installed rpm2cpio, downloaded RPMs, used rpm2cpio to extract their contents into a directory, chrooted to that directory to check that there were enough things there to run bash and rpm, ran \"rpm --initdb\", re-installed all of the RPMs using \"rpm -U\" so that they would be in rpm's database, then moved the contents of the chroot directory into the real root directory and vice-versa. (Also, when I installed Conectiva 10, I did so by \"apt-get dist-upgrade\" from this Ark Linux.) A problem with doing this with an RPM-based distribution is that packages don't know how to configure themselves, so the resulting system always acted weird in some ways, which I assume was caused by bad configuration files.\n\n(Side note: when I installed FreeBSD years ago, it's installer had the ability to install by downloading everything over a PPP connection. I'd like to be able to install Linux distributions this way, but I've never seen one that can.)\n\n> On the other hand, on Debian, they are nowhere. Not on any menu. Not an icon in some place. You have to know the magic words. And there is no list that I know of the magic words you have to use for common config tasks. That's *not* discoverable.\n\nJust now I looked at Synaptic's package description, and it says that it can reconfigure packages. I would think that it would be on the menu if it were installed.\n\nFurthermore, by the same reasoning that \"it's not on the menu\", one could say that apt-get isn't discoverable, and yet I don't remember seeing complaints that \"Debian/Ubuntu lacks package installation tools\".\n\n> And no, it's not a job for kynaptic, because that would have the following problems:\n> \n> a) It mixes the install-software and the configure-software functionality, which are not related at all in the users mind\n\nWell, it could be split off into a separate program. However, I don't agree that install-software and configure-software functionality are unrelated, for the reason I said in my previous message: many packages must be configured in order to be useful, so when I install one, I want to configure it right away, and I don't want to have to go looking for some separate program which might or might not be installed already.\n\n> b) It doesn't make the useful configuration any easier than the useless.\n> Since *every* package can be dpkg-reconfigure'd, the ones that actually are useful are a tree, lost in the forest. It's inconfigurability by obscurity.\n\nThere's an obvious solution to that: have the user interface list only the packages which actually have configuration questions. It could also have a \"bookmarks\"-like functionality for quick access to the most-often-reconfigured packages.\n\n> c) If there is such a tool, Debian sure doesn't mention it prominently enough, I think (I must be wrong ;-)\n\nIf its package description is correct, Synaptic is such a tool:\n\n     Besides these basic functions the following features are provided:\n      * Search and filter the list of available packages\n...\n      * Configure packages through the debconf system\n...\n\nAs for Debian not mentioning it prominently enough, it seems to me that Synaptic is well-known as a program for installing packages, but is less well-known as a configuration manager. So, yes, Debian (or Ubuntu) should publicize it better. (They should publicize debconf itself better, too.)\n"
    author: "rqosa"
  - subject: "One more thing:"
    date: 2005-08-05
    body: "Ubuntu Hoary includes GNOME System Tools (in \"main\")."
    author: "rqosa"
  - subject: "Re: Well done..."
    date: 2005-08-05
    body: "Ok, this is starting to be long enough to be a usenet thread, so I will try to keep it short.\n\na) xrandr is not good enough because it changes it for the session only. You can use something like krandrtray and make it redo it every login but it still doesn't fix, for example, a bad default resolution in kdm.\n\nb) I thought we were talking about regular users.\n\n> Also, it's not too difficult to search for it:\n \n> $ apt-cache search 'X\\.Org' | grep erver\n\nIs not exactly trivial, you know. Not to mention that you are searching for X.Org that, again, the user may never have heard of.\n\nc) You know, in most places you can buy a CD for about the cost of a hamburger that will have your linux distro of choice already burned in it. That would avoid using such a weird install mechanism.\n\nThe problem you had was that most RPMs you installed never ran their post-install scripts.\n\nd) The default install of all the systems I mentioned install all basic configuration tools. If you specifically ask *not* to install them, it's your problem to get them later. Debian is pretty much the opposite.\n\ne)You say:\n\n> There's an obvious solution to that: have the user interface list only \n> the packages which actually have configuration questions. \n\nWell, sure. It's not there, though.\n\nI'm outta here.\n"
    author: "Roberto Alsina"
  - subject: "Re: Well done..."
    date: 2005-08-05
    body: "> c) You know, in most places you can buy a CD for about the cost of a hamburger that will have your linux distro of choice already burned in it. That would avoid using such a weird install mechanism.\n\nWhy should I have to have a CD whose contents will quickly become outdated? That seems wasteful to me. (Ok, there are CD-RWs, but where can I take my CD-RW disk to get an ISO installer image written to it?)\n\n> The problem you had was that most RPMs you installed never ran their post-install scripts.\n\nOh really? Using rpm2cpio won't run the post-install scripts, true, but I re-installed *EVERYTHING* using \"rpm -i\" (or maybe \"rpm -U\", I'm not sure). I don't understand why \"rpm -i\" wouldn't run the post-install scripts.\n\nI think it's more likely that the problems I had were because RPM-based distros rely on the installer to do some \"special magic\" at install time. I think this is on of the main reasons why RPM distros don't have an equivalent to \"apt-get dist-upgrade\" (or in the case of the RPM version of apt, I doubt that \"dist-upgrade\" works as well as in the .deb version)."
    author: "rqosa"
  - subject: "Re: Well done..."
    date: 2005-08-06
    body: "Well, the cost of a CD is USD 0,15. If you buy one with your distro of choice, it may be about USD 2. I am willing to bet the time it took you to work around not having the CD is worth more than that.\n\nAnd no, the installer doesn't do any magic at all. I know, I have seen the code. For example, Anaconda, the most used one, simply installs everything, then it calls a few of the system-config-* tools to let you configure the basic stuff.\n\nYou can do the exact same thing, of course.\n\nRPM distros *do* have the exact equivalent of apt-get dist-upgrade.\n\nIn fact, it *is* apt-get dist-upgrade, if you want, and it works just fine. I updated, for example, a RH8 to CentOS4 that way.\n\nSure, you can't update Ark to Fedora and expect it to work flawlessly, but there simply aren't two Debian-based distros that are so different."
    author: "Roberto Alsina"
  - subject: "Re: Well done..."
    date: 2005-08-06
    body: ">  For example, Anaconda, the most used one, simply installs everything, then it calls a few of the system-config-* tools to let you configure the basic stuff.\n\nWell, I'd consider the use of system-config-* to be special treatment, because that doesn't happen when installing an RPM.\n\n> In fact, it *is* apt-get dist-upgrade, if you want, and it works just fine. I updated, for example, a RH8 to CentOS4 that way.\n\nEvery time I've dist-upgraded with the .deb version of apt-get, there were debconf questions asked for many packages, in order to keep the settings the same even though the new configuration file will have different contents. In my experience, rpm would sometimes rename the file in question to \"filename.rpmsave\" and other times would keep the old file in place and put the new file at \"filename.rpmnew\". I would think that the .deb way of handling this situation is more reliable.\n\n> Sure, you can't update Ark to Fedora and expect it to work flawlessly, but there simply aren't two Debian-based distros that are so different.\n\nOnce I tried to \"yum update\" from RH9 to WBEL3, and the resulting WBEL3 installation had some problems.\n"
    author: "rqosa"
  - subject: "KGX?"
    date: 2005-08-02
    body: "\"Pure KGX\"\n\nWhat does that mean?"
    author: "Leo"
  - subject: "Re: KGX?"
    date: 2005-08-02
    body: "_K_DE / _G_NU / Linu_x_\n    \n"
    author: "cm"
  - subject: "Re: KGX?"
    date: 2005-08-02
    body: "KDE\nGNU\nX"
    author: "c"
  - subject: "Re: KGX?"
    date: 2005-08-02
    body: "Not according to this: http://en.wikipedia.org/wiki/KGX\n\n"
    author: "cm"
  - subject: "Re: KGX?"
    date: 2005-08-03
    body: "> Not according to this: http://en.wikipedia.org/wiki/KGX\n\nso i changed it...    ;-P\n\n</joke>\n\ncies breijs"
    author: "troll..."
  - subject: "Watching progress"
    date: 2005-08-02
    body: "Hello!\n\nOne thing that's been \"annoying\" me about kubuntu is that it's kinda hard to follow the development progress of Kubuntu.. Would be cool to have some feeds/pages of new deb's getting into kubuntu, bugs getting fixed, and other stuffs that would make the whole kubuntu thingy more transparant.. The wiki is unsufficient for this IMHO. Otherwise, this distro rocks! It's been my main desktop distro for months!\n\nKeep up the good work!\n\nCheers, B."
    author: "Bart Verwilst"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "damn i use the word \"kubuntu\" a lot :p"
    author: "Bart Verwilst"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "The breezy-changes mailing list has all the new package uploads.  The kubuntu-bugs mailing list has all the beastie reports.  Most of the development happens on IRC because of the reduced lag compared to mailing lists.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "Oh cool :d I knew it had to be somewhere ;) BTW, do you have any idea how's the status with the bootsplash thingy? Will that make it in breezy in time? I know it's not kubuntu specific, but still :d The debian init script system isn't the prettiest thing to look at while booting ;)\n\nThanks!"
    author: "Bart Verwilst"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "usplash has recently been written by mjg59, you can find details on how to test it on the ubuntu-devel mailing list (not sure the date, it was written during debconf).  If it's reliable it should get into breezy.\n"
    author: "Jonathan Riddell"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "I second that. It has also been my major complain. Those initial problems with kde-datalibs update, KUser crashes, Admin mode not working in KControl, Kopete MSN not working... have been pretty bad (even if it wasn't always Kubuntu's fault). It would have helpfull or at least reassuring if a web page listing all those quirks had been put up.\nThat it nonetheless has remained my distro of choice says a lot (good :)\n\nJust a list with current important bugs or problems and a \"next release goals list\" (just a general idea) would be welcome. The information at udu.wiki.ubuntu.com is too much and too unclear.\n\nKubuntu as the KDE's developers personal distribution would be great\n\nKeep up the good work!"
    author: "Miq"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "https://wiki.ubuntu.com/KubuntuHoaryReleaseKnownProblems\n"
    author: "Jonathan Riddell"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "Thanks, that's a fine page.\n But don't you think it should be more visible from http://www.kubuntu.org.uk/ ? It's a bit difficult to find there (click at \"Kubuntu 5.04 Announcement\" then scroll down to \"Feedback and Helping\"). If kubuntu.org is to be some sort of portal for the distro it should have this important information very visible. Plus some kind of Feature plan for next release.\n\nIs there information about ept? (the substitute for Kynaptic and Kapture). That would be most interesting. I think the only reason I have the GTK libraries installed is Synaptic. I might be able to help in the project."
    author: "Miq"
  - subject: "Re: Watching progress"
    date: 2005-08-02
    body: "The \"admin not working in kcontrol\" problem is solved with kde 3.4.2"
    author: "guillaumeh"
  - subject: "Re: Watching progress"
    date: 2005-10-22
    body: "Unfortunately not! At least, it is still in Kubuntu Breezy (KDE 3.4.3). Not everybody experiences this bug but it is a real blocker for any newbie, especially since the new systemsettings kdesu window doesn't show anymore the name of the kcmshell command.\n\nhttp://bugzilla.ubuntu.com/show_bug.cgi?id=8681"
    author: "Futal"
  - subject: "KDE 3.4.2 on Amd64"
    date: 2005-08-02
    body: "Does anyone know if KDE 3.4.2 will be available for amd64?  Currently the packages.gz for amd64 doesn't exist on any of the mirrors.  Seems to be i386 only."
    author: "Leo"
  - subject: "Re: KDE 3.4.2 on Amd64"
    date: 2005-08-03
    body: "I don't have any plans to make them, mainly because I don't have access to an amd64.\n"
    author: "Jonathan Riddell"
  - subject: "System Settings as replacement for kcontrol"
    date: 2005-08-07
    body: "That system settings looks sweet! (Obviously mac inspired!) Where can I get more ingo on it? Is it apt-gettable yet?\n\nThanks\n\nTim"
    author: "Tim Sutton"
---
This week <a href="http://distrowatch.com/">DistroWatch</a> <a href="http://distrowatch.com/weekly.php?issue=20050801#interview">interviews our own Jonathan Riddell</a> about <a href="http://www.kubuntu.org/">Kubuntu</a>, the KDE-centric <a href="http://www.ubuntulinux.org/">Ubuntu</a> flavor. He reveals to us how he started with Kubuntu, what problems they've encountered and what his plans are for the upcoming new version.






<!--break-->
