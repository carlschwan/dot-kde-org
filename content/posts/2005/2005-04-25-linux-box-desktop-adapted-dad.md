---
title: "The Linux Box: Desktop Adapted for Dad"
date:    2005-04-25
authors:
  - "jriddell"
slug:    linux-box-desktop-adapted-dad
comments:
  - subject: "Full ACK"
    date: 2005-04-25
    body: "Linux is in contrast to popular belief much more user-friendly \nespecially for absolute beginners. It doesnt matter if installing\nsoftware or drivers is more difficult or not on Linux. My mother\nwouldnt want to install software herself anyway. She even doesnt\nwant to save and open files. She enters her letter and prints it out.\nAnd often people claim that there are too many configuration options\nin KDE. Doesnt matter for my mum: She would never use sth. like\nControl Panel. But for me all those options make it so much easier\nto create an environment where she can safely click around without\ndestroying something.\n"
    author: "Martin"
  - subject: "Single click my son !"
    date: 2005-04-25
    body: "I can hear the cry of joy of the many in the usability list that defended the single-click: it _is_ better!\n\nMy Dad can has a hard time making double-click as well. What is stranger to us, geeks, is that he does not know when to single click or double click. He may double click a menu, single-click a file to try to execute it, ...\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Single click my son !"
    date: 2005-04-25
    body: "Hm... putting it like that it does seem like single-click may be better for completely new people.\n\nThough if you take away my double-click, I will be pissed cause I like being able to select files without executing them (and hovering over them and waiting half a second like in windows is too slow)."
    author: "Corbin"
  - subject: "Re: Single click my son !"
    date: 2005-04-25
    body: "I'm not saying you should, or one way is better.  But just in case you didn't know and wanted to know...\n\nYou can ctrl or shift click to select it.  If it's a list view, don't click on it, just on the same line to select it.  You can drag a selection box on it, or just right click on it."
    author: "Saem"
  - subject: "Re: Single click my son !"
    date: 2005-04-26
    body: "Yes -- but how do you do the old fashioned single click. The one where previous selections are cleared and the new item is selected? This is often what I want, if nothing else to make sure I don't operate on some file that is not currently visible by accident.\n\nThe best I have found is to press ESC, then CTRL-click. But I just noticed that it does not work on the Desktop (in Fedora w/ KDE 3.3)."
    author: "Martin"
  - subject: "Re: Single click my son !"
    date: 2005-04-26
    body: "If you want to deselect something and select something else at the same time, just drag a box around the one you want selected and it'll get rid of the previous selection."
    author: "Saem"
  - subject: "Re: Single click my son !"
    date: 2005-04-26
    body: "Same experience here. In addition -- the double click has caused my parents to mess up things on many occasions. \n\nWhy? Because, in anticipation of a double click, their muscles will tense up in order to be able to deliver two clicks in fast enough succession to count as a double click for the machine. This tension will then be released in the form of uncontrolled mouse motion, that will often result in a drag and drop operation + an extra click or two at random locations. Even if I'm watching over their shoulder I'm not always able to reconstruct what happened and undo the damage. The single click is just more relaxed!"
    author: "Martin"
  - subject: "Re: Single click my son !"
    date: 2005-04-26
    body: "It's so true!!  I remember once an older gentleman I was helping told me some files just disappeared from his computer!!  Since I had seen him unintentionally drag files around when trying to double click, I was able to find them quickly in one of the neighboring folders.  It's cute and funny to me, but those were really important documents and he was very worried.  Also, I couldn't even get him to understand what he did wrong."
    author: "Henry"
  - subject: "Re: Single click my son !"
    date: 2005-04-26
    body: "Same over here, my mum used to have win'95, and everytime i checked the computer, she had all kinds of new files/directories on her desktop. Most of the time they were created while she tried to start an application by double clicking on an icon.\nwhile doing so, the mous usually  moved, and by accidently clicking the wrong button, new icons were created :o)\n"
    author: "ac"
  - subject: "Anti hand-tremors"
    date: 2005-04-26
    body: "Hi all,\n\nCan be this implemented via software?\nhttp://informationweek.com/story/showArticle.jhtml?articleID=159900169"
    author: "David C"
  - subject: "Re: Anti hand-tremors"
    date: 2005-04-26
    body: "It is possible to do this. I don't know what sort of data would be required though. IBM made a device to do this for portability reasons - it is not dependant on any OS."
    author: "dryclean_only"
  - subject: "Nice article. BTW..."
    date: 2005-04-26
    body: "> but he has got lots of card games\n\n You don't need to install lots of card games. If KPat doesn't support all you want, just install PySol. :)"
    author: "blacksheep"
  - subject: "Re: Nice article. BTW..."
    date: 2005-04-26
    body: "My mom loves PySol! and nethack falcons eye."
    author: "jrm"
  - subject: "Article offline?"
    date: 2005-05-02
    body: "I'm getting the message\n\n\"Can not find an article with id: 1113601124\"\n\non thelinuxbox.org on following the link provided. Is there a backup/alternative location somewhere?"
    author: "Guido Winkelmann"
  - subject: "Re: Article offline?"
    date: 2005-05-05
    body: "The article appears to have been moved. I don't know how The Linux Box deals with articles.\n\nYou can find it here on my server:\n\nhttp://www.iredale.net/articles/desktop-adapted-dad-1.html\n\nand home server:\n\nhttp://iredale.dyndns.org/unix/desktop-adapted-dad.html\n\nHope this is helpful - by the way there will be a follow-up article.\n"
    author: "Adam Trickett"
  - subject: "You can do it!"
    date: 2005-11-07
    body: "I have setup a Gentoo/KDE box at home for my family to use. So far they have managed to do some work/internet browsing on it without any complains. And my Dad was happy that it didn't crash while he was working on it ;)"
    author: "Herb Molenda"
---
Adam Trickett reports on his experiences of setting up KDE for his father in <a href="http://thelinuxbox.org/index.php?subaction=showfull&id=1113601124">Desktop Adapted for Dad</a>.  His article on <a href="http://thelinuxbox.org/">The Linux Box</a> shows the range of features KDE has for someone with poor eye-sight and reduced hand coordination.  He concludes by saying "modern Linux desktops are highly configurable, and with thought can be adapted for most people".



<!--break-->
