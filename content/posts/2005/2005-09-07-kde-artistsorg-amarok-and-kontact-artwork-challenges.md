---
title: "KDE-Artists.org: amaroK and Kontact Artwork Challenges"
date:    2005-09-07
authors:
  - "lando"
slug:    kde-artistsorg-amarok-and-kontact-artwork-challenges
comments:
  - subject: "Free recording tools"
    date: 2005-09-07
    body: "What is used by musicians for free music? \n\nRosegarden?\n"
    author: "fennes"
  - subject: "Re: Free recording tools"
    date: 2005-09-07
    body: "Rosegarden is mainly for use in composing and synthesizing AFAIK.  Probably \naudacity[1] is your best choice for actual recording.\n\n\n[1]http://audacity.sourceforge.net/"
    author: "Sam Weber"
  - subject: "Re: Free recording tools"
    date: 2005-09-08
    body: "ZynAddSubFX is used by many as a synthesizer. It really is amazingly powerful tool. It really dropped my jaw when I first heard all the rich sounds it can produce. http://zynaddsubfx.sourceforge.net/\n\nHydrogen is a really promising drum machine. http://www.hydrogen-music.org/"
    author: "Jani Huhtanen"
  - subject: "Re: Free recording tools"
    date: 2005-09-07
    body: "Ardour\nardour.org\n\nThere is no comparison, not even Audacity can hold against this baby."
    author: "ac"
  - subject: "Re: Free recording tools"
    date: 2005-09-07
    body: "I've never seen that before, looks very good.\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: Free recording tools"
    date: 2005-09-08
    body: "The only problem I have with ardour is the interface, not only is it GTK 1.x\nit's _butt_ ugly. I'd love to see an high end multi track recording software\nwritten in QT. Maybe Muse will become that someday?\n"
    author: "Anonymous Bastard"
  - subject: "Re: Free recording tools"
    date: 2005-09-09
    body: "see muse and rosegarden\n\nardour has still the most features, but you'll need a soudeditor like adacity along with it (to convert files)\n\n"
    author: "cies breijs"
  - subject: "Re: Free recording tools"
    date: 2005-09-07
    body: "Take a look at all the jack compatible apps on http://jackit.sourceforge.net/apps/ \n\nAll jack apps can have their inputs and outputs plugged into each other, making a very powerful audio platform."
    author: "Robert"
  - subject: "Amazing Kontact Logos && live CD"
    date: 2005-09-08
    body: "Wow, amazing logos! Can't decide which looks the best. I enjoyed reading Frank Schoep account with his ideas shaping up!\nBravo to all the artists!\n\nAs for amaroK Live CD, can we buy it or is it only on download? I absolutely need one to show around! Such a great idea to pack it with free music! "
    author: "annma"
  - subject: "Re: Amazing Kontact Logos && live CD"
    date: 2005-09-08
    body: "\"As for amaroK Live CD, can we buy it or is it only on download?\"\n\nGreat idea! Do you know of a service that would handle burning and selling the CDs for us?\n"
    author: "Mark Kretschmann"
  - subject: "Re: Amazing Kontact Logos && live CD"
    date: 2005-09-08
    body: "Perhaps lincd.com or www.edmunds-enterprises.com/linux/ or cheapbytes.com or any of the other numerous places which already sell cheap linux CDs would offer the Amarok live CD if it was suggested.  I'll bet they would be especially happy to do so if Amarok agreed to publicise the availability of said CDs on the dot or amarok.kde.org or other amarok-related places."
    author: "Spy Hunter"
  - subject: "Re: Amazing Kontact Logos && live CD"
    date: 2005-09-08
    body: "There actually is cafepress-type service for CDs and books, I forget its name though."
    author: "Ian Monroe"
---
The amaroK developers are currently working on the 1.3 release of their <a href="http://www.leinir.dk/temp/amaroklive/index.php?page_id=0">live 
CD</a>. If you are an Open Source artist and are interested in creating the amaroK 1.3 live CD artwork then join the <a 
href="http://kde-artists.org/main/index.php?option=com_smf&Itemid=48&expv=0&topic=436.msg2562#msg2562">7 day artwork challenge</a>.  Excellent prizes await the winner.  The CD will include songs from <a 
href="http://magnatune.com">Magnatune</a>, the "open music record label". These songs are "free as 
in free music".  In other artwork-related news the <a href="http://kde-artists.org/main/component/option,com_smf/Itemid,48/expv,0/topic,352.0">logo contest for Kontact</a> has come to an end with over 60 individual pieces of artwork submitted by 24 artists. The winner, who will receive a Wacom Graphic tablet, will be announced soon.






<!--break-->
