---
title: "KDE CVS-Digest for February 11, 2005"
date:    2005-02-12
authors:
  - "dkite"
slug:    kde-cvs-digest-february-11-2005
comments:
  - subject: "old layout"
    date: 2005-02-12
    body: "It seems to have been forgotten, so here it is: http://cvs-digest.org/index.php?issue=feb112005\nthanks for the great work, btw :)"
    author: "Illissius"
  - subject: "Re: old layout"
    date: 2005-02-12
    body: "Thanks. The new layout sucks badly. :)"
    author: "Carlo"
  - subject: "Re: old layout"
    date: 2005-02-12
    body: "\"all in one page\" is there, on the left."
    author: "superstoned"
  - subject: "Re: old layout"
    date: 2005-02-12
    body: "Even that doesn't make it as useable as the old one."
    author: "Mikhail Capone"
  - subject: "Amen!"
    date: 2005-02-12
    body: "I hate to pull an AOL, but this deserves it.   The new layout sucks badly.\n\nI don't like waiting to read the next page, and with the new layout that is what needs to happen.   With the old layout everything loads in a tab while I look for the next link to load in a different tab.   \n\nBack when the web was new some people had no clue how to scroll so something like the new layout made some sense.  Today most people have a scroll wheel and are used to scrolling."
    author: "bluGill"
  - subject: "Re: Amen!"
    date: 2005-02-13
    body: "> Back when the web was new some people had no clue how to scroll so something\n> like the new layout made some sense. Today most people have a scroll wheel \n> and are used to scrolling.\n\n*ROTFLOL*  That's the funniest thing I've read today...\n"
    author: "Navindra Umanee"
  - subject: "To Derek"
    date: 2005-02-13
    body: "I think, Derek, that you underestimated the number of people who read the entire Digest from top to bottom every week.  It seems strange, but when I read the digest I do it too.  So there's really no point in separate pages for everything for us.  Though it's probably a good idea to keep the option.\n\nAlso, you did such a good job with the color scheme of the old Digest that the dull gray of the new one pales in comparison.  Many places in the new digest could use some more padding in the layout; for example the sidebar text which goes too close to the edge of the window and too close to the main content.  Also the gray bars of the section subheadings should be significantly larger than their text.  For some reason with the color scheme of the old digest this didn't matter as much, but with white text on a gray background it looks cramped with no padding.  I am viewing the digest in Firefox so perhaps things look different in Konqueror.  I like the \"Table of Contents\" icons from the old Digest as well."
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-13
    body: "OK, I put my money where my mouth is and did some work myself.  Here is my proposal for a CVS-digest design using elements from both the old and new designs.  It uses CSS for layout instead of tables.\n\nhttp://www.cs.hmc.edu/~jdarpini/cvsdigestproposal.html\n\nThe changes are overall pretty minor, but I think the result is more pleasing to the eye.  It was designed in Firefox though, so you may have to tweak some positions for Konqueror.  Hopefully not; the CSS I used should be pretty generic."
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-13
    body: "It would be great if the left menu floats with the view."
    author: "Marius"
  - subject: "Re: To Derek"
    date: 2005-02-13
    body: "From my POV it's not better. There's still the left pane, which takes away precious space."
    author: "Carlo"
  - subject: "Re: To Derek"
    date: 2005-02-13
    body: "Append &noleftcolumn to the url. There are some combination of browsers and screen resolutions that result in a horizontal scroll bar.\n\nAnd if you don't want the statistics tables &nostatistics, useful for plucker or handheld. \n\n&all sets a cookie for all in one page preference.\n\nAnd I might as well add issue=latest is a link to the latest issue.\n\nYes, yes I'll get some links done soon.\n\nSpyHunter's proposal is interesting, I'll look further. One issue that may cause a problem is the width of the left column if icons are included. kdeplayground-artwork is almost too wide. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: To Derek"
    date: 2005-02-14
    body: "Category names don't have icons next to them in my proposal, only the links.  If \"kdeplayground-artwork\" was in the sidebar it wouldn't have an icon next to it.\nI replaced \"optimize\" with \"optimizations\" which is wider but more grammatically consistent; you could reverse this to make it less wide.  The sidebar could also be made less wide by using a smaller or not bold font, if that is really a big problem.  However, the only resolution which has a problem is 800x600, and in my second proposal (below), a horizontal scrollbar is avoided in all cases."
    author: "Spy Hunter"
  - subject: "the left bar..."
    date: 2005-02-14
    body: "... could just *end*after it's content? and the main content could just *flow* around it? that's an easy one..."
    author: "thatguiser"
  - subject: "Re: To Derek"
    date: 2005-02-14
    body: "I don't understand why you think the space is precious.  The vast majority of text in the Digest doesn't wrap, and so takes up a constant amount of space which isn't affected by the sidebar.  If you are using any sane monitor resolution the entire page will fit horizontally on the screen just fine.  If your resolution is 800x600 or you're using a huge font, it might not all fit on the screen at once, but in this case you can scroll to the right *once* and read the entire digest without the sidebar even being on the screen. (Furthermore I might point out that narrow columns of text have been scientifically proven to be faster to read than very wide text.  Making the digest narrower would increase its readability.)\n\nNevertheless, I have made a second proposal to address your concern.  The pane is on the right and only extends down about a page, after which it disappears.  (Unfortunately, fixing it in place is impractical for two reasons: firstly is is taller than the screen so if it wasn't scrollable you wouldn't be able to see it all, and secondly using position:fixed causes scrolling to become painfully choppy.)\n\nhttp://www.cs.hmc.edu/~jdarpini/cvsdigestproposal2.html"
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-14
    body: ">I don't understand why you think the space is precious.\n\nBecause I need to scroll a lot more, instead skimming/reading large chunks and pressing the page down key a few times.\n\n>Nevertheless, I have made a second proposal to address your concern.\n\nThank you, while it's not better for me, your code is more Firefox friendly than Derek's, which results in pages exceeding the width of the browser."
    author: "Carlo"
  - subject: "Re: To Derek"
    date: 2005-02-15
    body: "You don't scroll a lot more.  Once again I will point out that most of the digest text does not dynamically word-wrap, and therefore will not get longer no matter how big the sidebar is.  Furthermore, since I removed the sidebar for most of the page, I don't understand why you still say \"it's not better for me\" when I removed your major complaint.  I can't improve it for you further unless you tell me what you still don't like."
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-15
    body: ">You don't scroll a lot more. \n\nThat's your POV, not mine.\n\n>when I removed your major complaint\n\nI complain about the column with the links. Is this so hard to understand?"
    author: "Carlo"
  - subject: "Re: To Derek"
    date: 2005-02-16
    body: "It's not a \"point of view\", it's a fact.  I measured it:  on a 1024x768 screen, using Firefox, the new sidebar makes the page TWO LINES longer.  You can't possibly complain about having to scroll TWO additional lines on a page which is thousands of lines long.  You complained that the column took up too much space.  I eliminated 99% of the space that the column used to take up, and your complaint is no longer valid.  If you have a different, valid complaint, about the sidebar or anything else, I would be happy to hear it and correct it."
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-16
    body: "> It's not a \"point of view\", it's a fact.\n\nDepends on the quality of your eyes. I wear glasses and if I'd stress them with small fonts, a telescope would be needed soon. You speak with someone, who, while using large font sizes by default, always clicks on the \"print version\" link, if available and often increases the font szie additionally if the text is a bit longer. \n\nHaving only a few \nwords in one line\nwhile reading sucks.\nDo you agree?"
    author: "Carlo"
  - subject: "Re: To Derek"
    date: 2005-02-16
    body: "It doesn't depend on the quality of your eyes or anything else.  The sidebar doesn't get wider when you increase the font size, and it disappears after the first two or three pages (out of 70 or so) no matter what.  The text is only *slighly* narrower for the first few pages, not drastically narrower, and the rest of the digest is completely unaffected.  I have measured the vertical size increase of the digest and it is negligable.  Your complaint has been fully addressed."
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-16
    body: ">Your complaint has been fully addressed.\n\nIt's not. Thanks for your efforts, but I don't see any sense in it to let you tell me, what I see."
    author: "Carlo"
  - subject: "Re: To Derek"
    date: 2005-02-17
    body: "I'm not telling you what you see, I'm telling you what I see.  You haven't told me that you see anything different.  What do you see that's different from me?"
    author: "Spy Hunter"
  - subject: "Re: To Derek"
    date: 2005-02-13
    body: "The new layout, all-in-one view is OK, I just want the table of contents back."
    author: "mmebane"
  - subject: "Re: Amen!"
    date: 2005-02-13
    body: "For those averse to scrolling down, oh maybe half a page, and seeing the link 'All in one page', the link is now moved to the top. No more need to exercise the middle finger.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Amen!"
    date: 2005-02-13
    body: ">No more need to exercise the middle finger.\n\nEven thumbs up can be misunderstood depending on the cultural enviromenment. ;) Sounds like you need to hear once more how much your work is appreciated: Thank you, Derek."
    author: "Carlo"
  - subject: "OT: interesting browser benchmark"
    date: 2005-02-12
    body: "Using outdated konq though, but still. Interesting read.\n\nhttp://www.howtocreate.co.uk/browserSpeed.html"
    author: "jmk"
  - subject: "Even faster:"
    date: 2005-02-12
    body: "Thanks (after years of waiting ;-)\n---------------------------------------------------\nGermain Garand (ggarand) committed a change to kdelibs/khtml/khtml_pagecache.cpp in HEAD \n February 05, 2005 at 11:27:58 AM\n \nno need to delay chunks that much\nBUG: 78575\n\nRefer to bug 78575 - Cache is 2x slower than normal page load\n---------------------------------------------------"
    author: "Asdex"
  - subject: "Re: Even faster:"
    date: 2005-02-12
    body: "I always thought back and forward were too slow in Konqi.  This patch makes things feel much faster, and according to the test page attached to that bug, it isn't just placebo effect :D"
    author: "MamiyaOtaru"
  - subject: "Re: OT: interesting browser benchmark"
    date: 2005-02-12
    body: "What I still do not understand is why Konqui puts \"fullscreen\" under settings.\n\nWell, with my Suse my DSL speed problem originated from Suse's default setting, the name server thing etc."
    author: "Bert"
  - subject: "Re: OT: interesting browser benchmark"
    date: 2005-02-12
    body: "Which speed problems do you mean? I am using Suse and DSL, too, but have not seen any slowdown (besides the slow back/forward implementation of konqueror)"
    author: "Asdex"
  - subject: "Re: OT: interesting browser benchmark"
    date: 2005-02-13
    body: "Because the standard place for Fullscreen is under the Settings menu.  No, it doesn't much make sense, and I've read that the HIG group has already changed it, which means that all the apps will be revised (by 4.0?) to put Fullscreen in a more sensible location (I'd guess View).\n\nOr, to restate: \n\n  o It follows the standard just like all other KDE apps\n  o The standard is bad, *but*... \n  o The standard has already been changed and...\n  o The apps will soon follow."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: OT: interesting browser benchmark"
    date: 2005-02-13
    body: "> o The apps will soon follow.\n\nSoon? Probably in one and a halve years.\n\nOne of KDE's major drawbacks is that you cannot upgrade programs (e.g. konqueror) independently from KDE releases. So you will have to wait for KDE 4.0."
    author: "Asdex"
  - subject: "Re: OT: interesting browser benchmark"
    date: 2005-02-14
    body: "Geez, you make it sound so bad. This is about the placing of a menu item. Lighten up!"
    author: "Paul Eggleton"
  - subject: "Re: OT: interesting browser benchmark"
    date: 2005-02-14
    body: "change the order of the entries in `kde-config --prefix`/share/config/ui/ui_standards.rc and `kde-config --prefix`/share/apps/konqueror/*rc (and whichever other apps you wish) and you won't have to wait until 4.0"
    author: "Aaron J. Seigo"
  - subject: "OpenOffice-KDE 1.1.4 WHEN?"
    date: 2005-02-13
    body: "When will we see a Kdefied OOo 1.1.4? This is really needed. At least if KDE doesn't want to look bad."
    author: "Alex"
  - subject: "Re: OpenOffice-KDE 1.1.4 WHEN?"
    date: 2005-02-13
    body: "I don't really know, but I guess it won't take long. depends on the distribution you use. but the KDE-ification won't be really ready until OO.o 2.0 is out.\n\nAnd there is always Koffice... Koffice (2.0?) on KDE4/QT4 will give OO.o a hard time, I think :D"
    author: "superstoned"
  - subject: "Re: OpenOffice-KDE 1.1.4 WHEN?"
    date: 2005-02-13
    body: "Does it work if you use the KDE-Filepicker from OpenOffice_org-kde-1.1.3-16\ntogether with Openoffice 1.1.4?\n\nNevertheless, OOo 1.1.3 works quite well. Just download OOo (and OpenOffice_org-kde) from ftp.suse.com\n\nBut\n- the KDE file open/save dialog needs one or two seconds longer to show up than the standard OOo dialog\n- KIOslaves are not supported.\n\n"
    author: "Asdex"
  - subject: "Stupid Question"
    date: 2005-02-13
    body: "What's the difference between DigikamImagePlugins and the KIPI plugins? I thought the latter were created to avoid application-specific plugins."
    author: "AC"
  - subject: "Re: Stupid Question"
    date: 2005-02-14
    body: "Afaik DigikamImagePlugins contains those plugins which are not implementable via the current KIPI plugin interface."
    author: "Anonymous"
  - subject: "Re: Stupid Question"
    date: 2005-02-15
    body: "From http://www.kde-apps.org/content/show.php?content=16082\n\n--- quote ---\nKipi plugins : used by an image management program like digiKam or Kimdaba for process actions to images collection (Batch process images or e-mail images for example)\n\nDigikamImagePlugins : used by digiKAm image editor for process filters on unique RGBA image (like in Gimp)\n--- quote ---\n\nDoes this mean that there is still no general plugin API for image transformations that can be applied to single images? That would be too bad, couldn't this be done within the KIPI framework?"
    author: "AC"
  - subject: "Win32 tools and build support added to kdelibs"
    date: 2005-02-14
    body: "does that mean it'll actually compile on win32 now? that'd be sooo kool... have to checkout the kde-cygwin page at once..."
    author: "thatguiser"
---
In <a href="http://cvs-digest.org/index.php?newissue=feb112005">this week's KDE CVS-Digest</a>: Win32 tools and build support added to kdelibs. <a href="http://digikam.sourceforge.net/">digiKam</a> adds undo support for image editing. Kipi adds EPS image file format. KPDF begins work to support annotations. KDE now sports a new logo. Plus many bug fixes in preparation for the release.



<!--break-->
