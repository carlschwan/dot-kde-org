---
title: "Call for Location of aKademy 2006"
date:    2005-11-23
authors:
  - "jriddell"
slug:    call-location-akademy-2006
comments:
  - subject: "hmm"
    date: 2005-11-23
    body: "Netherlands or Northern Germany would be nice.\n\nRegardless where it will take place: Don't forget to get a greetings keynote from the local mayor and communicate the event to local press. Such events are great to promote Free Software in the hosting cities. "
    author: "gerd"
  - subject: "Re: hmm"
    date: 2005-11-23
    body: "Indeed, Netherlands would be nice. We (KDE-NL) have been toying with that idea, but we decided to wait for this until 2007 or later. It's a hell of a job, and the months May-June-July (when all the preparation take place) are quite occupied for us."
    author: "Bram Schoenmakers"
  - subject: "Re: hmm"
    date: 2005-11-24
    body: "That would be nice indeed. When reading the requirements document I thought that the campus of Twente University could probably easily accomodate that. Only downside is that there is no major airport really nearby; it's a two hour train ride from Amsterdam. Still, there are probably lots of other places that would do well for hosting it, this is just one I live very near to :-)"
    author: "Andre Somers"
  - subject: "Re: hmm"
    date: 2005-11-26
    body: "It is possible to submit a location that does not match all the criterion. It might still be an interesting opportunity. And if not for Akademy, there are other smaller dedicated meetings that can be held for KDE as well."
    author: "Philippe Fremy"
  - subject: "14th Oct 2006 should be the date"
    date: 2005-11-23
    body: "KDE 10th anniversary: 14th Oct 2006 shoul be the date for aKademy 2006.\nSee founding post: http://groups.google.com/group/de.comp.os.linux.misc/msg/cb4b2d67ffc3ffce.\n\nThe deadline for submitting an aKademy 2006 proposal is December 15th 2005, which  I find too short notice. Why not delay a bit by about a month. This date may be based on the fact aKademy normally held during August. If decide to havce it on October, we have extra 2 months. \n\nKDE 10th anniversary must be a major event. By then KDE 4.0 must be out. That's another reason to celebrate. Good luck guys."
    author: "Sagara"
  - subject: "Re: 14th Oct 2006 should be the date"
    date: 2005-11-23
    body: "> By then [14 Oct. 2006] KDE 4.0 must be out.\n\nIt would indeed be nice, but I wouldn't use the word \"must\"...\n"
    author: "LMCBoy"
  - subject: "Re: 14th Oct 2006 should be the date"
    date: 2005-11-23
    body: "If we want to celebrate KDE's birthday, than we should go to its root:  Tuebingen, Germany.\n\n\n    Heiner\n"
    author: "Heiner Lamprecht"
  - subject: "Re: 14th Oct 2006 should be the date"
    date: 2005-11-23
    body: "Indeed. T\u00fcbingen actually meets ALL of the requirements easily (well, if there's an organizing team, anyway). And I think it would be easy to convince the university to sponsor some rooms for the event. Then, hardware requirements would be met without even asking sponsors like TT or Intel. OTOH, 2004's aKademy was in Germany, and I don't know if people would like to come to Germany two years later. Just a thought ..."
    author: "hoirkman"
  - subject: "Re: 14th Oct 2006 should be the date"
    date: 2005-11-23
    body: "> See founding post: \n> http://groups.google.com/group/de.comp.os.linux.misc/msg/cb4b2d67ffc3ffce.\n\n\"I admit the whole thing [KDE} sounds a bit like fantasy. But it is very serious from my side. Everybody I'm talking to in the net would LOVE a\nsomewhat cleaner desktop. Qt is the chance to realize this. So let us \njoin our rare sparetime and just do it!\"\n\nThats quality, kudos Matthias and all the other programmers, i've been using KDE for years and had never heard about this post.\n\nSorry to be off-topic.\n\n--\nTony"
    author: "Tony Espley"
  - subject: "You can come to my house!"
    date: 2005-11-23
    body: "I am hereby inviting all kde users, hackers, mentors, guru's, artist, musicians, writers, geniuses, and general linux/kde public to come to my house for akademy 2006!\n\nI live in the USA in the fine state of Massachusetts in which all this opendocument politics is currently happening.  Good news: free beer and you finally get to personally congratulate the Mass. government for supporting open formats. Bad news: I live in the hills. There is NO fast speed internet at all and only one person can use the dial-up connection at a time (if no one is using the phone). I should also mention that you can only get a cellphone connection if you stand outside in the cold.  Your lucky its November though, because there is no connection when there are leaves on the trees!\n\nI welcome you all to my humble home!\n\n\np.s. Please understand this is humor and even though I would be overjoyed to have kde *people* come to my place, I think we'll have to limit it to about 4 at a time and only 1 or 2 that can actually sleep here.\n\nNonetheless, I offer a home for those who need a place to stay for a short while :)\n- Vlad Blanton\n"
    author: "Vlad Blanton"
  - subject: "Re: You can come to my house!"
    date: 2005-11-23
    body: "Get a hold of Red Hat and MIT to see if they'll fund your glorified KDE house party!\n\nBtw, geiseri, why don't you hold it at your house near philly?"
    author: "Ryan"
  - subject: "Re: You can come to my house!"
    date: 2005-11-23
    body: "Hey, its been done before :)\nAdamn and I have been kicking around the idea of a New Years party this year, but there was also talk of a Plasma hack session since we have a few developers in the area.  If people would like to have a smaller North American meeting we can score a meeting hall.  I can do the leg work locally, I just don't have the time to plan it in detail."
    author: "ian reinhart geiser"
  - subject: "Re: You can come to my house!"
    date: 2005-11-25
    body: "Sounds great! :)\n\nIt'll be my first time to meet all of you.\n\nAs long as its somewhat close to the east coast I should be able to make it. Otherwise, it might be tricky.\n\n- Vlad Blanton"
    author: "Vlad Blanton"
  - subject: "Definately"
    date: 2005-11-23
    body: "Should most definitely be held in the Twin Kities (St. Paul and Minneapolis in MN, USA for those who don't know ;P)"
    author: "Narg"
  - subject: "Re: Definately"
    date: 2005-11-23
    body: "I'd go for that, seeing as I live about an hour away - I'd even attend.   If you want to try to organize it let me know.   I'll help, but I refuse to take charge (I'm bad at organizing things in real space)."
    author: "bluGill"
  - subject: "Location"
    date: 2005-11-23
    body: "Personally, I prefer somewhere I can attend (being 16 kinda sucks in traveling). I propose, Canberra, Australia. Although the likelihood of this happening is remote, it is a very lovely city. :)"
    author: "Pascal Klein"
  - subject: "Re: Location"
    date: 2005-11-24
    body: "You have GOT to be kidding, right?  I worked in Canberra for 4 months and can safely say it is the most boring city on earth :-)  Then again, there would be no distractions from the conference...\n\nJohn."
    author: "odysseus"
  - subject: "Re: Location"
    date: 2005-11-24
    body: "Probably because you spent four months working.\n\nCanberra may not be Sydney or Melbourne, but there is certainly plenty to see and do.  I've lived here for 13 years now and haven't had a complaint, and if your feet ever get that itchy, it's only three hours to Sydney.\n\nAnd of anywhere in Australia, Canberra would make the most sense for this sort of event, as there's quite a concentration of OSS people in the area, and it's not too far away from either Sydney or Melbourne, and by October it's warm enough, whilst not being too hot for those used to a more European climate.\n\nBut I suspect the level of sponsorship required to have aKademy in Australia would be prohibitive, but the ACT government is always very keen to get events into the city, so government grants would probably be possible for those willing to make the effort.\n"
    author: "mabinogi"
  - subject: "Re: Location"
    date: 2005-11-24
    body: "After LCA 2005, everyone here is burned out. \n\nPlus the cost to get to Europe (for aKademy 2005) was pretty steep. I had to fly to the UK to get to Malaga - the flights to .au only go from a few places in .eu...\n\nIf there is interest, we could do a KDE.local session in Canberra."
    author: "Brad Hards"
  - subject: "Re: Location"
    date: 2005-11-24
    body: "I'd be very interested in doing something, local based here in down under. I reckon the ANU could probably provide space for it.\n\nCheers,\nPascal Klein"
    author: "Pascal Klein"
  - subject: "Napoli"
    date: 2005-11-23
    body: "Well I propose Naples in Italy it's a warm city and the food are very nice. We have got the best pizza of Italy and so much nice place."
    author: "Giovanni Venturi"
  - subject: "Iceland"
    date: 2005-11-23
    body: "I'd like to see one held in Iceland. Unfortunately I don't have any people to help organizing and I don't really know anybody who has experience with that but it would be fun to have it here."
    author: "Stefan"
  - subject: "Re: Iceland"
    date: 2005-11-25
    body: "This would be an expensive but cool destination, and it surely doesn't take that long to get there from the USA. Plus, it has the symbolic virtue of being situated between North America and Europe."
    author: "Anonymous Python"
  - subject: "Bulgaria"
    date: 2005-11-23
    body: "I`m about to propose Sofia (in Bulgaria) for an aKademy 2006 place. We`ll form the team, there is a nice conference site and all we need is a sponsor. :)"
    author: "Ilia"
  - subject: "Re: Bulgaria"
    date: 2005-11-25
    body: "I like Sofia as an option."
    author: "Calle"
  - subject: "USA!"
    date: 2005-11-23
    body: "Somewhere in the Pacific Northwest. Seattle, Portland, something like that. Hell, hold it in Victoria B.C. Just make it accessibe to us United Statesmen."
    author: "Aaron Krill"
  - subject: "Re: USA!"
    date: 2005-11-24
    body: "Yeah! In the front of the Microsoft Campus - this will be nice! >:)"
    author: "vicbrother"
  - subject: "Re: USA!"
    date: 2005-11-26
    body: "I'd love to see it held in Calgary, Alberta, Canada, cowtown of asiego. Because then I could attend. =)"
    author: "James Smith"
  - subject: "Re: USA!"
    date: 2005-11-28
    body: "Haha.  Dunno why you mentioned Victoria, BC, but I'm up for that! Then I can just walk over from my place :)"
    author: "Leo"
  - subject: "Tennessee"
    date: 2005-11-23
    body: "I would like to hold it in Murfreesboro, TN.  We are a small town housing a university (Middle Tennessee State University, http://www.mtsu.edu ) and are only 45 minutes from Nashville.  The campus has the largest student body in the state.  The city has more restaurants per sq. mile than any other city in the country.  And if you've never been to Tennessee, it is GORGEOUS!  The campus is wonderful.  I have been trying to get a small group of other computer science majors and such to get involved for a month now.  I'm still trying to round up a good sized team though.  The problem I forsee is the university president rejecting my proposal.  The other problem is lodging.  If we can hold it in the summer I MIGHT be able to convince the school to open up the dorms (they'll mostly be empty).  I'm a supervisor with the campus catering food service department so that probably won't be problem.  If anyone lives near here, please get in touch with me."
    author: "Carl"
  - subject: "Re: Tennessee"
    date: 2005-11-24
    body: "somewhere down here in Australia would be nice! eh? eh? :P"
    author: "petervdm"
  - subject: "Re: Tennessee"
    date: 2005-11-25
    body: "All though I love the USA and always enjoy staying there I guess there are some Coders who don\u0092t want to travel to the US because of various political reasons. Anyway I would love to come to Tennessee.\n\nCalle\n"
    author: "Calle"
  - subject: "Re: Tennessee"
    date: 2005-11-26
    body: "Yeah, the US is under a crap-storm politically with the rest of the world right now.  Anyway, I don't think I can get a proposal together for the university, get it approved, and still have enough time to get in my proposal with KDE.  Maybe I can shoot for 2007.  We'll see how things play out."
    author: "Carl"
  - subject: "Singapore"
    date: 2005-11-24
    body: "Go to Asia. It's the biggest market with the highest growth rates. Settle for Singapore and get some funding from IDA (<a href=\"http://www.ida.gov.sg/\">http://www.ida.gov.sg/</a>).\n\nAs a major air-hub it's easy to get there and the costs for hotel and food are reasonable. Everyone speaks English and getting around town is easy. "
    author: "Forgot my name"
  - subject: "Re: Singapore"
    date: 2005-11-29
    body: "Then a local LUG or similar organization should put together a proposal. A location like Singapore could work if funding for travel could be obtained."
    author: "Ian Monroe"
  - subject: "REDMOND, WA!"
    date: 2005-11-25
    body: "This is the best place:\n\nhttp://www.ci.redmond.wa.us/\n\nThe conference can be held in one of these facilities:\n\nhttp://www.ci.redmond.wa.us/insidecityhall/parksrec/facilities.asp\n\nA wonderful location with lots of people in the area who are interested in IT!\n.\n\n"
    author: "reihal"
  - subject: "London"
    date: 2005-11-25
    body: "Financially, London is probably the best location. What?! Let me explain.\n\nLodging should not be a problem: the city has plenty of affordable hostels as well as fine(r) hotels. The amount of (cheap) flights serving the city is unmatched in Europe or even world-wide. Savings based on the ability to fly into London at virtually no cost could easily offset the relatively higher cost of living. The social event, well, London IS a social event. There is no public transportation like LT and price-capped Oyster passes ensure low travel costs.\n\nThe biggest problem would be to find an appropriate accomodation for the event itself, but the KDE UK team might be able to help out with that. The UK economy is very flexible so finding sponsorships would be easier than in many other parts of Europe. I'll do some research on congress halls myself, it would be hard to imagine no suitable location could be found in Europe's premiere business city.\n\nOh, best of all: English gals and blokes are the hottest on the globe. ;-)"
    author: "Rob"
  - subject: "Re: London"
    date: 2005-11-25
    body: "http://www.congresscentre.co.uk/facility3.asp\n\nI've asked for some quotes."
    author: "Rob"
  - subject: "Re: London"
    date: 2005-11-25
    body: "True.\nThe travelling cost is a truly a good Point. With those prices I would even take a visit for a day or two from Germany."
    author: "Calle"
  - subject: "Re: London"
    date: 2005-11-25
    body: "London is a very cool city but since it is the most expensive city in Europe, I say no thank you."
    author: "cl"
  - subject: "Re: London"
    date: 2005-11-25
    body: "Actually, London can be quite affordable if you know the right places to go. In my experience it is definitely on par with all of Holland (Amsterdam, Rotterdam), Madrid, Barcelona and Brussels. And far cheaper than Oslo, Copenhagen, Stockholm.. well, basically all of Scandinavia. Cheaper than the centre of Paris as well. At Camden market 2 quid buys you a full meal and groceries need not be expensive either, Tesco is okay and if you go to places like ASDA you'll save loads of money as well. Don't confuse \"many tourist traps\" with \"expensive\".\n\nThat said, obviously I realise it can't compete in terms of cheap living with Nov\u00e9 Hrady, but could anything? In the end the total costs matter and as normally the bulk of money goes to transportation there is a serious case to be made of travelling to a major transportation hub.\n\nAnyway, I'll just keep looking for conference halls and will send in a proposal if I think I have a decent one to make. Just trying to take away the prejudice that London is expensive because I've travelled enough to know it's not. :-)"
    author: "Rob"
  - subject: "Re: London"
    date: 2005-11-26
    body: "One of the Universities might have cheap accommodation. I was thinking particularly of City U, home of XINU.  Plus there might be a number of ways to get sponsorship by some tech firms and other organisations.\n\nIf you are looking for help consider me in\n\n"
    author: "gerry"
  - subject: "Re: London"
    date: 2005-11-26
    body: "For those of you wondering about XENU:\nhttp://en.wikipedia.org/wiki/Xenu"
    author: "helpful"
  - subject: "not that helpful "
    date: 2005-11-27
    body: "for those of you wondering about XINU it's a recursion of Xinu is not Unix "
    author: "gerry"
  - subject: "you're wrong"
    date: 2005-11-27
    body: "I think you're confused.  You are thinking of GNU which stands for GNU's Not Unix.  See this link: http://www.answers.com/topic/gnu-1"
    author: "ac"
  - subject: "Re: you're wrong"
    date: 2005-11-27
    body: "No, he's not. See http://en.wikipedia.org/wiki/Xinu\n\nThere are tons of these recursive acronyms. \n\n\n"
    author: "cm"
  - subject: "Re: London"
    date: 2005-11-27
    body: "Typo or bad joke? "
    author: "cm"
  - subject: "Re: London"
    date: 2005-11-29
    body: "Your comparing London to other large cities and capitals, they're not the only and certainly not the cheapest options.\n\nThough actually I don't know why I'm agruing this point, I think an akademy in London would be awesome. :)"
    author: "Ian Monroe"
  - subject: "Re: London"
    date: 2005-11-29
    body: "You might be able to get something for 2 quid in Camden, but I'm not at all sure you would want to eat it! Or maybe you have a cunning plan to give everyone food poisoning and take over KDE for your own dark purposes...\n\nThat said, London genuinely would be a good location if a cheap enough conference venue could be found. "
    author: "OcularSinister"
  - subject: "Re: London"
    date: 2005-12-02
    body: "Never gave me food poisoning. :-)"
    author: "Rob"
  - subject: "Bulgaria"
    date: 2005-11-26
    body: "Well, in Bulgaria is cheaper that in London, currently is cheaper than in the European Union, we have a good conference sites, and we match all the requirements."
    author: "Ilia"
  - subject: "Portland Maine, easy choice"
    date: 2005-11-27
    body: "I think that Portland Maine would make for an excellent choice \nas the next location for Akadamy 2006.  It meets all the requirements \nand would be probably one of the most fun locations.  The city is beautiful\nand the people are very nice.  It also just 2 hours up the road from Boston!\nIn late August all through October, Maine is at it's best climatewise.\nI've added some links for whomever to check out.\n\nhttp://www.ci.portland.me.us/\n\nhttp://www.portlandmaine.com/\n\nhttp://www.portlandjetport.org/\n\nhttp://www.mainehotels.org"
    author: "Caleb O'Connell"
  - subject: "Re: Portland Maine, easy choice"
    date: 2005-11-27
    body: "Ooo, did I forget to mention it's close proximity to world \nrenowned Colleges and Univeristies?\n\nhttp://www.bowdoin.edu\nhttp://www.bates.edu\nhttp://www.colby.edu\nhttp://www.harvard.edu\nhttp://www.mit.edu\nhttp://www.bc.edu\nhttp://www.dartmouth.edu\nhttp://usm.maine.edu\nhttp://www.umaine.edu/"
    author: "Caleb O'Connell"
  - subject: "Any decision"
    date: 2006-02-03
    body: "Is there any decision about Akademy 2006 yet?"
    author: "Juri"
---
The next annual central meeting of the KDE community, aKademy 2006, is looking
for a location. The event consists of the general assembly of <a href="http://ev.kde.org">the KDE e.V.</a>, a
KDE developer conference and a multi-day hacking session. The main goal of
the event will be to shape the upcoming new major step of the K Desktop
Environment, KDE 4. If you are interested in hosting this large and exciting
free software event, please consider to submit a proposal to the board of the
KDE e.V. which will act as a co-host.


<!--break-->
<p>There is a <a href="http://ev.kde.org/akademy/requirements.php">list of requirements</a> for the aKademy location. If you
want to apply as host for aKademy 2006 please submit a concrete proposal to
kde-ev-board<span>@</span>kde.org. This proposal should include information about how the
requirements for the location will be addresses, who will be the local
organising team and a responsible person acting as contact to the KDE e.V.
and head of the local organising team. The deadline for submitting an aKademy
2006 proposal is December 15th 2005.</p>

<p>If there are any questions please feel free to send them to the KDE team
taking care of organising aKademy at akademy-team<span>@</span>kde.org or the board of the
KDE e.V. at kde-ev-board<span>@</span>kde.org.</p>

<p>Past aKademy events took place in <a href="http://events.kde.org/info/kastle/">Nove Hrady</a>, <a href="http://conference2004.kde.org">Ludwigsburg</a> and <a href="http://conference2005.kde.org">Malaga</a>. Where will we be next year?</p>

