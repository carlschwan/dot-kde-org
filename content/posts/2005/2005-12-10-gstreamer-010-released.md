---
title: "GStreamer 0.10 Released"
date:    2005-12-10
authors:
  - "criddell"
slug:    gstreamer-010-released
comments:
  - subject: "win32 codecs"
    date: 2005-12-10
    body: "I read somewhere about support for win32 codecs in gstreamer, but don't know the URL anymore.\nDid someone know this URL?\n\nThanks :)\n"
    author: "anonymous"
  - subject: "Re: win32 codecs"
    date: 2005-12-10
    body: "The project is called pitfdll (http://ronald.bitfreak.net/pitfdll.php). \n\nI don't think it's been ported to GStreamer 0.10 yet though"
    author: "Jan Schmidt"
  - subject: "Re: win32 codecs"
    date: 2005-12-10
    body: "Actually, I take that back - http://cvs.sourceforge.net/viewcvs.py/pitfdll/pitfdll/ChangeLog?rev=1.20&view=log says differently (about having been ported to 0.10)"
    author: "Jan Schmidt"
  - subject: "Re: win32 codecs"
    date: 2005-12-10
    body: "thank you!! :)\n"
    author: "anonymous"
  - subject: "speaking of amarok"
    date: 2005-12-10
    body: "I use amarok with Xine engine cause it's much  faster on my  old CPU, is the new gstreamer as fast and light than  xine?"
    author: "Patcito"
  - subject: "Re: speaking of amarok"
    date: 2005-12-10
    body: "Possibly, yeah. Note that the GStreamer engine in current amaroK versions (> 1.3.1) has also become quite performant. But we hope it's gonna be even faster with 0.10, no doubt.\n"
    author: "Mark Kretschmann"
  - subject: "Re: speaking of amarok"
    date: 2005-12-10
    body: "I beg to disagree. In my experience, GStreamer has been unusable while attempting to play streaming media even in the latest AmaroK! It will buffer up to 100% and re-buffer again and again till for ever! I have always relied on the Xine engine which has never dissappointed me in any way. This has made me wonder why it is bundled with Amarok by default yet it's deficient in the streaming department, leaving out Xine which just works."
    author: "charles"
  - subject: "Re: speaking of amarok"
    date: 2005-12-10
    body: "I hope you have filed bugreports about that so that people have a chance to fix it for the next version."
    author: "John Doe"
  - subject: "Re: speaking of amarok"
    date: 2005-12-10
    body: "Last time I checked, Xine was the default for amaroK"
    author: "rinse"
  - subject: "Re: speaking of amarok"
    date: 2005-12-11
    body: "Yep, xine has a higher plugin priority then gstreamer (so if you have both installed, xine will be used)."
    author: "Ian Monroe"
  - subject: "Re: speaking of amarok"
    date: 2005-12-10
    body: "I want to say I had the exact same experience with streaming audio. Its two seconds of audio, one second of silence. Definately not usable. Hopefully this version will be better."
    author: "LuckySandal"
  - subject: "Re: speaking of amarok"
    date: 2005-12-11
    body: "I've found that just isn't the case, it uses maybe 50% CPU on my 800mhz duron just to play an mp3, wheras xine and especially arts hardly need any."
    author: "mikeyd"
  - subject: "Network Transparancy?"
    date: 2005-12-10
    body: "For me, the timing for this release was pretty coincidental. I was doing some web searches during some free time today looking into what multimedia backends could be used with remote X applications. At first NMM seemed to be a pretty good fit, but they don't really support amd64. Someone patched it to build on that platform, but they state that it doesnt work over the network from amd64->x86. \n\nIt looks like gstreamer has a tcp src/sink. I'll probably be testing it soon.\n Has anyone had any success with gstreamer or any other audio backend for remote X applications? (over ssh?)\n"
    author: "John M."
  - subject: "Re: Network Transparancy?"
    date: 2005-12-12
    body: "Xine with esd backend works out best for me, right now. As I exclusively use thinclients at home, I have to rely on networked multimedia heavily and did try several alternatives (nas, gstreamer + esd, nmm, even OpenSSI with a remote /dev/dsp).\n\nThis applies to audio only, video is a different story (synchronized audio+video, 100MBit LAN not enough for streaming decoded video). NMM should do the trick here, in the future.\n\nIf you have limited bandwidth (ssh?), you should try NMM, as decoding is done there at the audio sink.\n"
    author: "J\u00f6rg Bakker"
  - subject: "KDE wishlist"
    date: 2005-12-10
    body: "Okay: my KDE desktop wishlist\n\n- real Multimedia support for video and audio plus editing tools\n- grammar checker\n- desktop search framework and proper metadata\n- unified freedesktop registry namespace (elektra??)\n- cross desktop icon theming"
    author: "manes"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "Classical thread hijacking :)\n"
    author: "Mark Kretschmann"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "\"- real Multimedia support for video and audio plus editing tools\"\n\nDo you mean Real audio support? If yes then thats already available (may need the win32codecs, or to install the native realplayer/helix).  There are some video editing tools on kde-apps.org (not sure how good they are, and not sure if there are any good audio editing tools).  What exactly do you mean by 'tools'? (like for cutting scenes and adding special effects? or just like transcoding from one format to another and editing tags?)\n\n\"- grammar checker\"\n\nDon't hold your breath for a /good/ one (expect focus follows mind before that).\n\n\"- desktop search framework and proper metadata\"\n\nKat+Tenior framework will provide exactly that (Tenior is using Kat as the base engine instead of making its own)\n\n\"- unified freedesktop registry namespace (elektra??)\"\n\nIf you mean a unified configuration system for desktop applications, then thats possible since I remember people working on a crossdesktop configuration standard (though again don't hold your breath for elektra, 'registries' aren't that popular, especially since the best known registry makes everyone cry)\n\n\"- cross desktop icon theming\"\n\nThe Tango naming standard (which Oxygen ix following IIRC) should solve that issue."
    author: "Corbin"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "\"The Tango naming standard (which Oxygen ix following IIRC) should solve that issue.\"\n\nActually, that's the freedesktop.org icon naming standard that Tango has chosen to use and which KDE4 was probably going to use anyway.  It bugs me slightly that Tango has grabbed such a mindshare for supposedly taking the lead on this.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "Don't be -- it helps no one if KDE got a bigger mindshare on the idea -- the contributors know who they are, and everyone that cares know who they are. By having FD.o push it without any hint of desktop bias helps to get the gnome and xfce, openbox, enlightenment, etc people involved and supporting it, and that will benefit everyone."
    author: "genneth"
  - subject: "Re: KDE wishlist"
    date: 2005-12-11
    body: "That's exactly what I meant, the spec is an FD.o project, so the credit and focus should go to FD.o, rather than to a Gnome project...\n\nJohn."
    author: "Odysseus"
  - subject: "Re: KDE wishlist"
    date: 2005-12-12
    body: "\"rather than to a Gnome project...\"\n\n... which Tango is not.\n"
    author: "Reply"
  - subject: "Re: KDE wishlist"
    date: 2005-12-12
    body: "> ... which Tango is not.\n\nLook at this page: http://tango-project.org/The_People\nCan you tell me which developer works on KDE? Answer: none.\nCan you tell me which developer works on XFCE? Answer: none.\nCan you tell me which developer works on E17? Answer: none.\nCan you tell me which developer works on GNOME? Answer: all."
    author: "Anonymous"
  - subject: "Re: KDE wishlist"
    date: 2005-12-12
    body: "Looking at that page the answer is that only one of those people is acutally a devloper of any sort, and there are only 4 who have a GNOME history.\n\nBut hey, thanks for coming out and adding to the FUD."
    author: "ryan"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "I thought that Tango was the name of the standard, and also included an icon set and stuff."
    author: "Corbin"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "Tango is an icon and desktop theme project.\n\nhttp://tango-project.org/Standard_Icon_Naming_Specification\n"
    author: "James Richard Tyrer"
  - subject: "Re: KDE wishlist"
    date: 2005-12-10
    body: "\"- unified freedesktop registry namespace (elektra??)\"\n\nIf you mean a unified configuration system for desktop applications, then thats possible since I remember people working on a crossdesktop configuration standard (though again don't hold your breath for elektra, 'registries' aren't that popular, especially since the best known registry makes everyone cry)\n\nOpenLDAP anyone?"
    author: "Mitra"
  - subject: "Re: KDE wishlist"
    date: 2005-12-11
    body: "> OpenLDAP anyone?\n\nThat really would make me cry.\n\nI have no problem with a registry concept for configuration, but OpenLDAP would be massive overkill - at least for local configuration.\n\nI can see where it would be useful for roaming profiles, but even in that case you'd want to copy all the configuration to a local cache on login, and then store the changes on logout. (Or maybe on a finer-grained level - at app startup and shutdown).\n"
    author: "mabinogi"
  - subject: "Re: KDE wishlist"
    date: 2005-12-11
    body: "Actually, it would be useful to have a plug-able arch (think PAM). Personally, I would like to see my data separated from config files. I hate the fact that I have to do a bit of work to get somebodies data into a new KDE archs (I find it better to start with the defaults) ."
    author: "a.c."
  - subject: "Re: KDE wishlist"
    date: 2005-12-12
    body: "Actually I think KConfig is supposed to be backend independent, but no other backend than the ini file one has been developed. If I remember correctly it should be possible to write backends that use things like LDAP, PAM or SQL. Try searching kde-core-devel, there was some discussion a while back(Well, 2-3 years or something:-)"
    author: "Morty"
  - subject: "Re: KDE wishlist"
    date: 2005-12-12
    body: "> Kat+Tenior framework will provide exactly that (Tenior is using Kat as the base engine instead of making its own)\n\n\nThe approach is insufficient. The \"engine\" should not be desktop dependent, but a daemon, able to contact other boxes as well (given the user has the right to search those). The GUI should just be the sugar. What does it help when I can search my local box, but not my data server, which doesn't run KDE at all?! Shall the data to be indexed go over NFS or SMB or what?"
    author: "Carlo"
  - subject: "Re: KDE wishlist"
    date: 2006-09-18
    body: "When I think about KDE, I see everything thick and big (thick borders, big buttons). I'd love to see everything thinner and more professional in KDE 4.\n\nCharles."
    author: "Charles"
  - subject: "Yippee!"
    date: 2005-12-10
    body: "GStreamer has saved me on a number of occasions when I've had to knock up custom gst-launch pipelines."
    author: "Robert"
  - subject: "Nice marketing campaign!"
    date: 2005-12-10
    body: "But Xine just works (even without the merketing blabla like \"..the best just got better\")."
    author: "Marc"
  - subject: "Re: Nice marketing campaign!"
    date: 2005-12-10
    body: "I agree with previous posts, Xine is currently the most functional multimedia engine. GStreamer is not as responsive and compiling it is not as easy. Anyway I hope it is now usable so I can keep the Kubuntu defaults for amaroK and Kaffeine. Anyway stop the marketing-speak."
    author: "Flavio"
  - subject: "Re: Nice marketing campaign!"
    date: 2005-12-10
    body: "Yeah. It's true. GStreamer with amarok on my 7.1 audio sounds like ... Xine is just much better. I'm not c++ developer but if Xine is similar framework to GStreamer please take Xine into new KDE"
    author: "what? changelogs ony for final??"
  - subject: "Re: Nice marketing campaign!"
    date: 2005-12-10
    body: "Have any of you tried amaroK with NMM? How does it compared functionality and performance wise to Xine/GStreamer/aRts?"
    author: "Morty"
  - subject: "Re: Nice marketing campaign!"
    date: 2005-12-11
    body: "Regarding your complaint about Changelogs only for final, there are full ChangeLogs for all the modules through the full 0.9.x development cycle.\nYou find them at - http://gstreamer.freedesktop.org/releases/"
    author: "Christian"
  - subject: "Re: Nice marketing campaign!"
    date: 2005-12-12
    body: "Xine is just a playback engine.\n\nGstreamer is an entire multimedia framework."
    author: "Robert"
  - subject: "Kaffeine compatibility"
    date: 2005-12-10
    body: "I have tried to compile last kaffeine version with gstreamer part (gstreamer 0.10) but, during the execution of the configure script it says \"Gstreamer found\" but finally it says that it won't compile the Gstreamer part (?). \n\nHas anyone experienced the same problem?"
    author: "One kde user"
  - subject: "Re: Kaffeine compatibility"
    date: 2005-12-10
    body: "My version of kaffeine has the gstreamer plugin and it doesn't seem to ever work for any video files, so just stick with Xine (IMHO Xine is far better than GStreamer)"
    author: "Corbin"
  - subject: "Re: Kaffeine compatibility"
    date: 2005-12-11
    body: "GStreamer 0.10 isn't source compatible with apps written for gstreamer 0.8."
    author: "Ian Monroe"
  - subject: "Thanks for paying attention"
    date: 2005-12-10
    body: "My apologizes but I don't want the GNOME stuff to be part of KDE due to my personal political opinion."
    author: "ac"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-10
    body: "KDE is about software, not about politics.\nGStreamer is not GNOME specific, although it is the default MM framework in GNOME."
    author: "St. Kevin"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-11
    body: "ac is being ironic ;)"
    author: "Ian Monroe"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-12
    body: "Yet it dependent upon Glib, which is a core GNOME library. It doesn't mean we have to use GNOME for KDE, but it does mean that it makes KDE somewhat dependent on GNOME's release quirks. If you thought rebuilding KDE everytime there was a new KDE release, just wait until you have to do it with every GNOME release!"
    author: "Brandybuck"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-13
    body: "Glib isn't Gnome, like GStreamer isn't Gnome. There are even commandline tools using glib.\n\nbtw. Parts of KDE already depands on glib."
    author: "Susi"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-13
    body: "To be more precise, glib was part of the GTK (the GIMP toolkit). Later it has been split off and is now low-level C library that is used by many C projects."
    author: "cl"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-14
    body: ".. but it's still GNOME!"
    author: "ac"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-15
    body: "No. Glib and GTK are not part of GNOME, just as Qt is not part of KDE."
    author: "SR"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-10
    body: "Yeah so do I, not because of your 'personal political opinion' but because Mondays never fall on Tuesdays (and vice versa).\n\n"
    author: "Empty Head"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-10
    body: "I agree. Please keep glib out of KDE! Glib is EVIL!\n"
    author: "k"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-11
    body: "This could be a clever way to konvert some of those evil gnomesters to see the light."
    author: "reihal"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-11
    body: "Well, you won't have to, that's the beauty of the new KDE-MM api, you get to choose your back-end, KDE4 won't dictate it to you.\n\nJohn."
    author: "Odysseus"
  - subject: "Re: Thanks for paying attention"
    date: 2005-12-12
    body: "pray tell, how on EARTH is gnome political??\n\nposterchild for kde's reputation as the desktop of choice for the socially inept."
    author: "ryan"
  - subject: "High latency?"
    date: 2005-12-10
    body: "I've just noticed that when I'm using amaroK with gstreamer I get a lot of skips when I'm playing music and doing normal things like switching desktops even if there is not much running in the other desktop (1 or 2 konsoles). I'm using the stock kubuntu kernel which is fully preemptible. Anyone know of anything else I can do to get rid of these skips or is it just gstreamer?"
    author: "Tormak"
  - subject: "Re: High latency?"
    date: 2005-12-10
    body: "Try the Suse-kernel - they have some nice desktop-patches in it."
    author: "Martin"
  - subject: "Re: High latency?"
    date: 2005-12-10
    body: "Which audio engine are you using?\n\nIn the past (0.8 and before), whenever I've used the alsasink output plugin, it's been terrible. It takes nearly 100% of the cpu time, and any other activity causes skips. In fact, I'm totally unable to play video acceptably with alsasink, because there aren't enough cpu resources left to decode the video fast enough (and this is on an Athlon64).\n\nIf you can, try switching to osssink (which can still use alsa in actuality, since alsa emulates the oss interface). That's always solved cpu use/skipping issues with gstreamer for me. Maybe 0.10 has fixed the alsasink issues, though. It would certainly be nice."
    author: "Dolio"
  - subject: "Re: High latency?"
    date: 2005-12-10
    body: "Yes I think I am using the alsasink. I'll try switch to the oss sink and see how it goes. Thanks for the tip!"
    author: "Tormak"
  - subject: "Re: High latency?"
    date: 2005-12-12
    body: "Switched to osssink but still have skipping problems (though not as bad as w/ alsasink)."
    author: "Tormak"
  - subject: "Re: High latency?"
    date: 2005-12-20
    body: "I just discovered an engine that doesn't skip at all. ARTS."
    author: "Tormak"
  - subject: "Re: High latency?"
    date: 2006-02-04
    body: "On my setup (kubuntu with KDE 3.5.1) gstreamer+arts crashes amarok.\ngstreamer+alsa and gstreamer+oss suffer from extremely heavy skipping.\nThe xine and the arts engines both cause amarok to crash.\nNot a happy situation. :-("
    author: "Matthias"
  - subject: "GStreamer == DRM"
    date: 2005-12-10
    body: "They plan to add support for DRM...\n\nhttp://blogs.gnome.org/view/uraeus/2005/12/3/0\n\n\nPlease, keep KDE free from GStreamer."
    author: "ac"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-10
    body: "It looks like Fluendo are adding DRM to their streaming server solution. Who can blame them to create a products that there paying customers might want. Looking at their website, it does not seem that Fluendo has (yet) a lot of paying customers. If they are not realistic, they will burn their investment money quickly.\n\n\nMy problem with GStreamer is the same that was mentionned by other posters, I have never been able to have it play any sound on my desktop. Maybe the new version is changing this problem. Maybe I have not tried hard enough because xine and arts are good enough for me. But having something that work out of the box, with an easy interface for parametering, and that will not cause tons of frustrated emails on support mailing list should be a key point when deciding which multimedia platform we adopt as default."
    author: "Charles de Miramon"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-13
    body: "So if you ask any paying customer: \"do you want DRM support?\" you think they'll answer \"yes\"? I think they'll ask you WTF you're talking about.\n\nSo then you ask: \"Do you want companies to decide what music you may listen, how you may listen to it, and where, thus limiting your freedom?\" you think they'll answer \"yes\"?\n\nI think no one wants DRM, except the people that get rich from it. Which is around 0.0005% of the world's population.\n\nDon't think those 0.0005% knows what's best for us, and don't call them the paying customers. In the end, the consumers will have to pay."
    author: "aac"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-11
    body: "For heaven's sake!\n\nIf you don't have any technical clue, please STFU!\nThey're planning to do some plugins which implement DRM for DRM-using media files (encrypted wmv crap). There will be no DRM in the core, there will still be all the \"normal\" plugins. No one is forcing any kind of DRM on you if you don't want it. That's why they are plugins!\n"
    author: "Bausi"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-11
    body: ">They're planning to do some plugins which implement DRM for DRM-using media files (encrypted wmv crap).\n\ni think it depends on what kind of DRM is implemented. If it is just a \"decrypting-function\" to allow people with drm files to enter their password/key to play the file than i think it is ok, because it enable people to use theri files.\nBut if it is a kind of DRM with forbid some kind of uses, i don't want to see it in any GNU/Linux application! For example i don't want a pdf reader wo tells me that i can't print the pdf-file and i don't want a mm-framework wo tells me that i can listen only 3 times to a media-file!"
    author: "pinky"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-11
    body: "If a Linux (or a media framework, or whatever) supported some kind of DRM, companies will start using it. And we all know that companies' interests are not the same than users' interests. That's the problem, plain and simple.\nI don't blame GStreamer for supporting that -- their \"corporate\" backends (Nokia, Sun, ...) are throwing at them big money. I blame who wants to support GStreamer.\nAnyway, if after more than 6 years of development the GStreamer developers have not been able to make a stable (i.e. not crashing) release (or even match the functionality of Xine), I think the future is bright: GStreamer will hardly be a success. The incompetent GStreamer developers are our strongest allies."
    author: "ac"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-11
    body: "i hope you don't use kpdf, because it has also DRM functionality!\nDon't get me wrong. I'm absolutely with you and your argumentation. But i don't understand why people talk about it in context with a mm-framwork which will maybe be the default in the future and forget DRM which is actually in KDE (e.g. kpdf)!"
    author: "pinky"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-11
    body: "this KPDF drm can be turned on an of at compile time, and i don't think many distributions will deliver it on. it is a feature, because some ppl wanted it - but you have to explicitly enable it."
    author: "superstoned"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-12
    body: "And that's the case with Gstreamer, you don't get DRM unless you specifically install the plugins, which are not even a part of gstreamer.\n"
    author: "Reply"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-12
    body: "That's not the problem. The problem is that now, \"thanks\" to GStreamer, Linux is a DRM-enabled platform. I have yet to see DRMs that actually benefit the user and not the companies.\nIf Fluendo wants to put DRM in because their \"sponsors\" like Nokia and Sun want to do that, that's fine -- we'll have the proof that GStreamer is not a user-oriented project but a money-oriented project.\nAnd, while Gnome will have no choice but use GStreamer since Fluendo is on the advisory board (or whatever they call it), I think we can be smart enough to leave that crap out of KDE."
    author: "Anonymous"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-12
    body: "as long as the DRM doesn't restrict the user more than 'before' (eg it allows playing formats otherwise unavailable) it's no problem. but if the DRM restricts certain things other plugins allow (for example), its a bad thing. imho we should just wait and see..."
    author: "superstoned"
  - subject: "Poor troll"
    date: 2005-12-12
    body: "Seriously, you're an idiot and/or a poor troll. You have just said:\n- kpdf is fine because \"KPDF drm can be turned on an of at compile time\"\n- but GStreamer is not, even though the DRM support is an optional plugin, and as a result \"Linux is a DRM-enabled platform\".\n\nDid you know that GCC could be used to write DRM software that might make Linux DRM-enabled platform? Oh no, best find another compiler.\n\nSeriously, do you blame every application author that provides plugin functionality for the plug-ins produced by third-parties? None of us like DRM, but your point is irrelevant. Nice try to find a problem where there is none. \n"
    author: "Dan"
  - subject: "Re: GStreamer == DRM"
    date: 2006-01-01
    body: "amen.\nTo all those who are bitching and moaning about DRM, here's the funny bit, the linux kernel supports DRM and TCPM. I suppose we should rip that out of the kernel so we can all stick it to the man! YEAH, WE'D BE SO HARDCORE, BECAUSE OUR SYSTEM OF CHOICE CANT RUN ON THE \"MAINSTREAM\" SYSTEMS. WE'RE REBELS.\n\nSeriously, gimme a break, DRM has too much interest vested in it to be just cast aside, might as well have some support ready so when it does become the status quo, you can proudly say linux can run on a DRM platform or gstreamer can play those pesky drm'd wmv's.\n\nStop being such fundies, you're worse than the religious nutcases.\n\nI dont like DRM, but it's inevitable, no matter how much power is vested against it, there are too many interests within the companies that MAKE the industry and all its standards. With the attitude of \"ew, evil corporations made it! dont support it\" linux will quickly fall behind. Funny how all the \"good\" corporations are the ones who give linux some recognition, despite how underhanded they could be (IBM anyone?) but in the end they see a cheap platform for their hardware to run on without any proprietary shit they have to pay for to develop drivers for.\n\nIn the end, just stop bitching about DRM, it in itself isnt evil, what it can be used for can be evil, which is why having DRM support in OSS is good, since we can play the game against the assholes who would like to think that you should use something or watch something when THEY want you to."
    author: "Shadowpillar"
  - subject: "Re: GStreamer == DRM"
    date: 2007-03-14
    body: "DRM simply does not work.  If a movie can be watched it can be copied.  Period.\n\nIf a DRM decryption module is made open source, then any half-competent programmer can remove all the DRM restrictions and use the decrypter to transform the movie into an unencrypted format.  If the module is not open source, it requires a highly competent programmer, but that's the only difference.\n\nSome of the ridiculously complicated crap being implemented in hardware will make this a slightly more complicated problem, but still trivial for the expert programmer to defeat.\n\nThat's worth remembering.\n"
    author: "Nathanael Nerode"
  - subject: "Re: GStreamer == DRM"
    date: 2005-12-12
    body: "if you dont have DRM files , your dont have DRM - if you have DRM file and a license , you would be glad to play it. !!!!\n\n"
    author: "ch"
  - subject: "GStreamer with Slackware - NO no no no"
    date: 2005-12-10
    body: "Never had a good luck to play with gstreamer, all applications which uses gstreamer 0.9 crashes like kaffeine, amarok, kmplayer. \n\nI wish you luck with GStreamer, perhaps it will replace arts with its cross-platform capabilities.\n\nI want any sound and/or video application to be able to play simultaneously without bothering with sound engine, ala alsa."
    author: "fast_rizwaan"
  - subject: "Re: GStreamer with Slackware - NO no no no"
    date: 2005-12-11
    body: "well, alsa has actually done this quite nice. if your hardware supports hardware mixing you can play as many sounds as you like at a time even when playing songs with both oss and alsa api's. I had a soundblaster live in my previous pc, and that card supported it, I don't know about other cards.\n\nif your hardware doesn't support it you can still use software mixing by putting the following lines in /etc/asound.conf\n\npcm.!output {\n type dmix\n ipc_key 1234\n slave {\n  pcm \"hw:0,0\"\n  period_time 0\n  period_size 1024\n  buffer_size 8192\n  rate 48000\n }\n}\n\npcm.!input {\n type dsnoop\n ipc_key 1234\n slave {\n  pcm \"hw:0,0\"\n  period_time 0\n  period_size 1024\n  rate 48000\n }\n}\n\npcm.!duplex {\n type asym\n playback.pcm \"output\"\n capture.pcm \"input\"\n}\n\npcm.!default {\n type plug\n slave.pcm \"duplex\"\n}\n\npcm.!dsp0 {\n type plug\n slave.pcm \"duplex\"\n}\n\nctl.!mixer0 {\n type hw\n card 0\n}\n\nI found it somewhere on the internet and use it on my laptop that can't do hardware mixing. it works quite good for me..)\n\nthis will allow any number of alsa sound streams to play at the same time. I wasn't able to play using both oss and alsa api's at the same time but at least it let's you play multiple alsa streams at the same time. most programs seem to support alsa nowadays so this becomes less and less of a problem.\n\n"
    author: "Mark Hannessen"
  - subject: "Re: GStreamer with Slackware - NO no no no"
    date: 2005-12-12
    body: " \"Iwant any sound and/or video application to be able to play simultaneously without bothering with sound engine, ala alsa.\"\n\nKeep in mind that arts, gstreamer and others not only serve as sound server, but also as backend to play the different file formats for the applications you use.\nFor instance, amaroK, kaffeine, noatun, juk can't play ogg-files, nor can Alsa.\nSo you still need gstreamer, xine, arts or another backend that plays the file and sends  the output to e.g. Alsa"
    author: "ac"
  - subject: "Nope..."
    date: 2005-12-11
    body: "I hope that I can build my KDE 4 from the sources with the option to disable it.\nI wouldn't like to have DRM o gstreamer...\nWhy we should aceppt that?\nThen, we can fork Gstreamer! -  Would be such a nice ideia..."
    author: "Zack"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "The DRM stuff will be done as an add-on package. So you (or your distribution) will make the choice of wether to install it. You can of course also de-install the DRM stuff also, but then you will not be able to playback DRM'ed audio and video files (unless there are some open source libraries which are able to crack them)."
    author: "Christian"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "In that case, I hope that the distros makes the packages this way, usually, The distros makes separeted plug-in packages (E.g:Debian with gstreamer...), So I hope, that you're right and that they continue, besides, I hope that DRM doesn't become a dependence package for KDE Multimedia system. So we can continue with no freedom problems..."
    author: "Zack"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "you are a kde-developer, right?\nSo why you are against a DRM modul for gstreamer if KDE already using a pdf viewer with DRM integration? For me that makes no sense, gstreamer is (maybe) the feature but KDE has already today a \"DRM problem\". IMHO you (== the KDE and kpdf devs) should solve the present before complaining about a possible future."
    author: "pinky"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "You mean in KPdf like, Settings->Configure KPdf->General and uncheck Obey DRM limitations. You see, noting to be solved. Bad troll."
    author: "Morty"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "not everyone who hadn't known this option is a troll!"
    author: "pinky"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "no, but this comment doesn't have much to do with the topic (some are afraid gstreamer will limit its users and make things impossible. this is NOT the option in Kpdf - it has to be turned on on compile-time, and can then still be turned off in the config..."
    author: "superstoned"
  - subject: "Re: Nope..."
    date: 2005-12-12
    body: "So essentially it's no different from choosing not to compile the Fluendo plug-in at compile-time? \n\nAfter all, for the majority of end-users, the decisions whether to include DRM support in KPdf, GStreamer etc. will be have already been made by the maintainers of their distribution of choice. \n\nPeople's irrational fear of gstreamer is about as sensible as being terrified that KPdf will prevent you from reading your PDFs in the future."
    author: "Dan"
  - subject: "Re: Nope..."
    date: 2005-12-12
    body: "In this case you are totally wrong, since they work completely opposite. If  you choose to not use the GStreamer DRM plug-in you will no longer have access to DRM enabled content. While you in KPdf you can turn it off and have unrestricted access to DRM enabled content. Rather a big difference."
    author: "Morty"
  - subject: "Re: Nope..."
    date: 2005-12-17
    body: "That's about DRM itself, nothing to do with GStreamer.\n\n\"If you choose to not use the GStreamer DRM plug-in you will no longer have access to DRM enabled content\"... and if you choose to use xine or any other engine that doesn't support DRM then you will no longer have access to DRM enabled content, and without even the option of installing a plug-in.\n\nGstreamer DRM support simply gives you an extra option (freedom) that you have not with others.\n"
    author: "Red"
  - subject: "Double Standards"
    date: 2005-12-12
    body: "Disclaimer: Due to the high volume of \"Gnome is evil\" posts, I'll add that I use both KDE and Gnome and I have no real preference.\n\nOk, you can get KPDF to ignore DRM limitations. That is great :)\n\nWhy is this acceptable, yet GStreamer using an *optional* plug-in to allow playback of DRM'ed files unacceptable? \n\nI really dont see the problem here. If you are unlucky enough to have DRM'ed media, install the plug-in and enjoy. If not, you dont need the plug-in.\n\nDRM playback is needed if we want more people to jump ship from Windows. Joe Sixpack will want to play the songs he bought off iTunes, so we should have the option to do this. Note the word *option*. The moment DRM becomes part of gstreamer core, Ill stop using it."
    author: "The Grum"
  - subject: "Re: Double Standards"
    date: 2006-01-25
    body: "If you're talking about getting people to jump ship from Windows, it's not going to be Joe Sixpack we get first, it would be wonderful if it was, but it's going to be the curious and disenchanted. Do you think that people will switch based on cost? When they've already had to pay for their OS with their machine. Will they switch because it's easier to use? Unlikely. Among other reasons, switchers - the disenchanted - probably aren't that interested in having the same kind of restrictions imposed upon them.\n\nWhen I first heard about this, and I appreciate that I'm a little behind here, I was hoping that this framework / plugin facility was going to be similar to the way that DeCSS is used, it can't easily be included with distributions but it can be distributed in an 'underground manner', so that we can still use our stupidly restricted content that we can't buy in another format. If I ripped a DVD to play on my Tapwave Zodiac, I don't care for it to still be restricted, thanks. Similarly, if I *had* to buy an album using iTunes or <WMAShop> the first thing I'd need to do would be remove the restrictions, anyone would want to given the choice. Supporting DRM in FOSS is removing that choice.\n\nI appreciate the Wine analogy, but no-one really believes that the existence of open source software is threatened because software makers don't port to it. Maybe Adobe Photoshop is a million times better than GIMP, but there's always GIMP. If the only way to listen to music is on restricted devices because even free systems support it, then... that's the end. Of course, there's no open source clones of pieces of art like music albums! The fears with DRM are that an incredible amount of our culture (media- images, audio, video, software I guess)  will only be available in restricted ways. DRM won't go away if we support it.\n\nI'm fed up with people sending me WMA and WMV media, not because it's a pain to install the codecs for it, but because it's a bit wrong to use things like that if everyone can't partake. Presumably lots of people would like to just give up and install the binaries for the codecs, which is fair enough for them. Would it not be better to reserve some sort of inclination for ...\n\nLook, what I mean is, lets not pander to restrictions, lets have a framework instead called SWITCH (perhaps it can stand for something) that is illegal and uses every power we have available to convert documents, audio and video in any format into unencrypted, clean formats, hopefully lossless. Convert WMV to MPEG2 or even something un-patented. Lose some quality, but hey - web clips suck anyway. Shouldn't have used WMV in the first place. Clean out someones iTunes collection to FLAC so they can keep what they've paid for on their free system, or play it on their non-iPod. Remove the restrictions by whatever means, don't support them."
    author: "Sam Denison"
  - subject: "Re: Nope..."
    date: 2005-12-11
    body: "> you are a kde-developer, right?\n\ni think you may be thinking of a different zack. =)\n\nas for drm in kpdf, we have resolved it: it's optional."
    author: "Aaron J. Seigo"
  - subject: "Already decided"
    date: 2005-12-12
    body: "It sounds as if GStreamer has already been decided and approved for KDE 4.0. Is this true? Since it is dependent on Glib 2.0, is there any pressure on GNOME to keep the Glib ABI stable?\n\np.s. The GStreamer page says that it is also dependent upon glibc. As a non-Linux user, I'm hoping that was a typo."
    author: "Brandybuck"
  - subject: "Re: Already decided"
    date: 2005-12-12
    body: "The plan is to make KDE4 independent of multimediaframework through KDEMM, making it possible to switch between aRts, NMM, GStreamer or whatever. \n\nAs for glibc, it's the standard C libraries. It's rather hard to find any C code not depending on it. So my guess it's not a typo:-)"
    author: "Morty"
  - subject: "Re: Already decided"
    date: 2005-12-13
    body: "he said glib and not glibc!"
    author: "ac"
  - subject: "Re: Already decided"
    date: 2005-12-13
    body: "Read again: \"The GStreamer page says that it is also dependent upon glibc.\""
    author: "cm"
  - subject: "Re: Already decided"
    date: 2005-12-13
    body: "\"glibc\" is GNU libc, and as far as I am aware, is only present in Linux (and Hurd) systems. Any code that uses glibc's non-standard extensions will be problematic on non-linux systems. Hence my concern.\n\nI am hoping this is a typo. If not, and GStreamer becomes a KDE dependency, then I will have to start hunting around for another desktop."
    author: "Brandybuck"
  - subject: "Re: Already decided"
    date: 2005-12-13
    body: "Then it will depend on whether or not GStreamer uses glibc's non-standard extensions. It's supposed to run on BSD, so perhaps not.\n\nBesides GStremer will not become a KDE dependency, read the part about KDEMM again:-)"
    author: "Morty"
  - subject: "Re: Already decided"
    date: 2005-12-15
    body: "Glibc is also used by GNU/kFreeBSD (the GNU system with the FreeBSD kernel).\n\nGstreamer works with other C libraries, like FreeBSD.\n"
    author: "Reply"
  - subject: "DRM discussion"
    date: 2005-12-13
    body: "by all this DRM discussion, some people have mentioned the kdpf drm implementation. Does someone knows a source were i can get a pdf with some DRM restrictions? I just would like to see the DRM options of kpdf in action.\n\nMaybe someone could even create a small test pdf with some drm?\n\nThanks!"
    author: "penny"
  - subject: "Xine is GPL, so all DRM _must_ be Open Source "
    date: 2005-12-13
    body: "Stick with Xine because it is GPL and all its plugins (including DRM-infested ones) would _have_ be open source. That means anybody can remove the DRM portion and be left with a fully-functional plugin.\n\nThe same cannot be said about GStreamer, which is LGPL and whose DRM plugins will most likely be proprietary, closed-source, and controlled of the big media corporations.\n\nThe only way to keep DRM's nasty claws off KDE is to ensure that users can change the code, and the only way to do that is to chose a tool that is GPL.\n\nOther GPL'ed alternatives include MPlayer and VLC, although I'm not sure how they compare to Xine. Could someone who has worked with these backends comment on whose architecture is best thought-out and whose code is cleanest/easiest to maintain?"
    author: "vladc6*yahoo*com"
  - subject: "Re: Xine is GPL, so all DRM _must_ be Open Source "
    date: 2005-12-13
    body: "i agree on this GPL/LGPL point. i prefer the GPL over the LGPL, just because of this. glad Qt is GPL (or proprietary, but in that case the company has to pay so it benefits the FOSS community anyway)..."
    author: "superstoned"
  - subject: "Re: Xine is GPL, so all DRM _must_ be Open Source "
    date: 2005-12-15
    body: "Nobody cares about you GPL fascists."
    author: "Rick"
  - subject: "Re: Xine is GPL, so all DRM _must_ be Open Source "
    date: 2005-12-15
    body: "It's not about GPL, it's about DRM.  GPL is just a tool to circumvent DRM.  Then again maybe you are a DRM fascist and this is exactly what you don't want."
    author: "ac"
---
The <a href="http://gstreamer.freedesktop.org/">GStreamer</a> developers have just released <a href="http://gstreamer.freedesktop.org/documentation/gstreamer010.html">version 0.10 of the GStreamer multimedia framework</a> into the wild, and their coders' fingers will never be the same. <i>"Thread-safety, RTP/VoIP support, automatic registry maintenance, twice the performance, and a whole lot more...the best just got better. A highly flexible, cross-platform, and GUI-independent multimedia framework, GStreamer takes your media, chews it up, and spits it out into aural and visual paradise. Especially targeted at GNU/Linux and Unix operating systems, the GStreamer team has been working with certain members of the KDE community for a long time. With this release we have a stable, extendable and robust multimedia solution."</i>  In conjunction with <a href="http://wiki.kde.org/tiki-index.php?page=Multimedia+API+Talk">KDE MM</a>, GStreamer is one of the possible multimedia frameworks that will be available for KDE 4.










<!--break-->
<div style="float:right; border: thin solid grey; width: 305px; padding: 1ex; margin: 1ex;">
<img src="http://kubuntu.org/~jr/dot/gstreamer.png" width="305" height="86" />
</div>

<p>Please read the release announcement to get an overview of what is new and improved in the 0.10 series. The input and feedback of the KDE community would be much appreciated as the GStreamer developers continue their breakneck pace towards the holy grail of GStreamer 1.0.</p>

<p>GStreamer is a generic multimedia framework based around the concept of
media pipelines linking elements, providing support for all manner of
things. In GStreamer you'll find plug-ins supporting multimedia file
formats, firewire and USB cameras, sound cards, windowing systems,
transcoding, networking, audio and video transformations and much more.</p>

<p>Mark Kretschmann, project lead of the <a href="http://amarok.kde.org">amaroK</a> music player said "<em>I am looking forward to porting amaroK over to GStreamer 0.10. Many of the problems our users experienced with the 0.8 version seem to be addressed in GStreamer 0.10, especially the responsiveness issues we faced. So to make it short, GStreamer 0.10 is gonna be a blast, I'm totally into it!</em>"</p>

<p>Aaron J. Seigo commented on behalf of <a href="http://ev.kde.org">KDE e.V.</a>, "<em>multimedia is a central theme for desktop computing, so making meaningful strides towards open source media solutions that provide what application developers as well as users need is critical. Recognising how non-trivial software that addresses this problem space in a portable and open manner is, and given that several KDE applications provide GStreamer support already today, we are happy to see the milestones that are being met by the GStreamer project. The future of multimedia in Open Source just keeps looking better and better. Congratulations on a successful release!</em>"</p>











