---
title: "Report: KDE at Latin America Free Software Install Festival"
date:    2005-04-04
authors:
  - "dp."
slug:    report-kde-latin-america-free-software-install-festival
comments:
  - subject: "Kubuntu"
    date: 2005-04-04
    body: "Kubuntu is wonderful but it is no real supported Ubuntu branch but a unsupported fork to please those who dislike the Gnome policy. Kubuntu has to organise itself independendly and get companies involved. I think it would be nice to see a kind of merger of the Knoppix and the Kubuntu effort. "
    author: "aranea"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "> Kubuntu is wonderful but it is no real supported Ubuntu branch but a unsupported fork\n\nYou're talking shit. Of course there is no branch and no fork because Ubuntu and Kubuntu use the very same repository. And Ubuntu's KDE packages are supported, otherwise they wouldn't be in the Hoary 'main' repository."
    author: "Anonymous"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "But ubuntu defaults to Gnome as SuSe defaults to KDE. Only few people will download kubuntu that is a second choice option, a kind of \"special build\" hidden on the website."
    author: "aranea"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "> But ubuntu defaults to Gnome as SuSe defaults to KDE.\n\nAnd Kubuntu media default to KDE. The point is that you now have the choice, both pre- and post-install and don't have to use as previous the sole desktop.\n\n> Only few people will download kubuntu\n\nYou don't have to download \"Kubuntu\", installing the kubuntu-desktop package will do the same.\n\n> a kind of \"special build\" hidden on the website.\n\nNot hidden on kubuntu.org, just help to spread the word of choice to Ubuntu users."
    author: "Anonymous"
  - subject: "Re: Kubuntu"
    date: 2005-07-07
    body: "I did install Ubuntu on my system and grew fond of the KDE enviroment by live CD's of other options. I looked to installing the KDE enviroment on Ubuntu. I opened synaptic and chose kubuntu-desktop among other various KDE packages. After installation, I saw no change, except in the way my system logs off - it doesn't allow me to chose to shutdown from there, and the logon screen.\nI tried to figure out why the KDE did not install correctly, but am downloading the Kubuntu install CD right now. I heard Kubuntu tends to work better with wireless devices and prefer the feel. I also heard the a few tweaks are necessary due to differences in the coding.\n\nHoping for a good turn out.\n -Mike"
    author: "Mike"
  - subject: "Knoppix != Kubuntu"
    date: 2005-04-04
    body: "Knoppix is a live cd distro.\nKubuntu ain't."
    author: "blacksheep"
  - subject: "Re: Knoppix != Kubuntu"
    date: 2005-04-04
    body: "To be more correct, Kubuntu also has a live cd."
    author: "Anonymous"
  - subject: "Re: Knoppix != Kubuntu"
    date: 2005-04-04
    body: "For Kubuntu there are both Live CD and Install CD. See kubuntu.org."
    author: "Andreas Simon"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "Kubuntu is very much an \"official\" ubuntu project:\n* http://lists.ubuntu.com/archives/ubuntu-users/2005 -January/019774.html\n\nMatt Zimmerman :\n\n\"This is an official Ubuntu project aimed to provide an excellent\nKDE-oriented distribution based on Ubuntu.\"\n\n* There are Canonical employees who have been hired to work on Kubuntu,\n* KDE is not in universe but in main (which means it's officially supported)\n* and as far as I know Mark Shuttleworth encouraged work on Kubuntu and seems to be quite happy with the current result (especially with Kubuntu's attractive konqi models). \n* You need Kubuntu to get http://www.ubuntu.com rendered standards compliant correctly (including shadows). ;-)\n\nSo basically you are just spreading FUD and misinformation.\n"
    author: "Torsten Rahn"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "http://www.kubuntu.org.uk/"
    author: "kubu"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "I really don't mind the existance of Kubuntu, but I don't think that's how the future should be. Distros like Debian, Gentoo, Suse, Fedora, Mandrake and those classic general purpose distributions are free to support every GUI. But ubuntu is nice because it focuses on a specific branch, makes choices for you.\nI don't mind the fact that they've got KDE packages, that's okay, but in my opinion it's going in the wrong direction, create a new kde oriented distribution instead that focuses on kde tools, and has more reliance on what is integrated into kde to create a lighter experience.\nThe freedom of choice doesn't mean that every distribution should have everything...!\n\nMy default desktop is kde, so i'm not writing this because of a gnome preference..."
    author: "SF"
  - subject: "Re: Kubuntu"
    date: 2005-04-04
    body: "> But ubuntu is nice because it focuses on a specific branch, makes choices for you.\n\nKubuntu also makes (application) choices for you."
    author: "Anonymous"
  - subject: "Software Libre Revolution!"
    date: 2005-04-04
    body: "Great to see pictures of Salvador Allende and Che Guevara on the wall next to the Software Libre poster. Progressive politics is alive and well in Chile, unlike here in the UK, where we had some of the most depressing right wing populist election campaigning recently :("
    author: "Richard Dale"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "Actually the fact that we noticed the posters during the event worried us. Those are no \"progressive\" politics. Socialists evoluted and right now are just like european social-democrats. I am not a socialist but the current goverment is ok and has 78% popularity. Allende and Guevera is marxist socialism & communism. They represent no more of 2% or 3% of the population, have no congress representation, still use the violence to express themselfes, and the worst thing, they never were able to be consistent here.\n\nAttacking Pinochet and defending Fidel Castro, you could recognize that they are not democrats. For me, it was funny that after they treated every right-side person as facist or nazi (without arguments) their own leader Salvador Allende is suspected to be nazi, from a research done using his own doctoral thesis and his writings when he was Health minister before being president, where he affirmed jews and arabians were race-predetermined to have more posibilities to be delinquents and promoted esterilization of \"alienated-people\".\n\nDuring the event, an administrative guy from the cultural centre (it was a communist centre, no a open one) expulsed a friend of mine who came to the installfest, just because he was drinking coca-cola, and coca-cola == bush for those stupids. Even if he was using nike shoes and the electricity of the centre come from spain investiments.\n\nEven more, communists abuse the free software concept because it matches their economical ideas, so they try to take on free software idea. The sad part is that they forget it is a digital world and not anatom-based one, so free software plays better with free-economies and open markets.\n\nSo che guevara and Allende is not welcome here. The fisrt one being a face just to put in t-shirts for coolnes for european kids, the second one being who destroyed our country economy and produced a division that still remains.\n\nPeople who helped Allende are now renewed and imitate social-democrat goverments with a liberal market economy. I am not from their side, but I like the way they are doing things, because is the same way Chile would do things. Except communist, who would try to destroy democracy, economy, family, church, and impose stuff.\n\nSo please, people from Europe, you are victims of propaganda. If you want to see what happens here. Come here. I am not happy of the political side of the event because free software should always be promoted in a neutral manner, using its own advantages."
    author: "Duncan Mac-Vicar P."
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "\"Socialists evoluted and right now are just like european social-democrats.\"\n\nYou are lucky; our socialists have evolved into 'neoliberals' such as Tony Blair, they are not european social-democrats. He is a more of a fan of Margaret Thatcher, than Willie Brandt.\n\n\"Attacking Pinochet and defending Fidel Castro, you could recognize that they are not democrats.\"\n\nI don't pretend to know as much about Chilean politics as yourself, but very few people in Europe would regard Fidel Castro as more of a tyrant than Pinochet. I've never heard these views about Allende being a Nazi. As he was shot by Pinochet's forces, he wasn't allowed to let his arguments speak for themselves, whether or not you thought they were right or wrong.\n\n\"so free software plays better with free-economies and open markets.\"\n\nYes, if a free market is one where large companies can't shut down smaller companies by using software patents, or use unfair means such as accountancy fiddles and so on.\n\n\"I am not happy of the political side of the event because free software should always be promoted in a neutral manner, using its own advantages.\"\n\nThis is the message of 'Open Source', not Software Libre. I wouldn't say that Richard Stallman is a man of the left or the right, but he most certainly does have a strong political viewpoint. One of the reasons I personally write Free Software is because I want to help change the World for the better, not advance some short term business advantage. Open Source attempts to eviscerate the Free Software movement, by removing the Freedom aspect in order to make it more business friendly. All those right wing ego-maniacs such a Eric Raymond have done is screw things up by encouraging a proliferation of incompatible 'Open Source' licenses."
    author: "Richard Dale"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: ">I don't pretend to know as much about Chilean politics as yourself, but \n> very few people in Europe would regard Fidel Castro as more of a tyrant \n>than Pinochet. I've never heard these views about Allende being a Nazi. \n>As he was shot by Pinochet's forces, he wasn't allowed to let his arguments >speak for themselves, whether or not you thought they were right or wrong.\n\nJust to remark the ignorance of foreign people about our politics, thanks to international propaganda. Allende wasn't shot, he commited suicide, using a gun that was a gift from Fidel, and that is certified by his own personal doctor. There weren't pinochet forces, it was the chilean army itself as requested by the country. Pinochet in other countries is the evil itself. I am not saying he could be good or bad. It had both good and bad things, but the evil view is just international propaganda. Probably, the only picture you have seen out of chile of him, is one where he has evil face and dark glasses. Propaganda.\n\nThe views about Allende being a nazi aren't views, but ideas in his own writings.\n\nIts sad every time the word Chile appears in international topics, the politcs topic is raised, and with total ignorance.\nI kind of forgive US people about ignorance of our politics because they often don't even know where Chile is due to their education model. But europeans should understand the propaganda and information factor, and understand Mexico and Latin America *are* conservative countries, and not use the same optic-glass to analyze our politics. Also, I feel frustrated that those ideologies whch call themselfes tolerants, try to *evil*ize whatever is different from their point of view. For me communism is wrong. But for other people market, money, business, church, is just evil. Even an innocent nerd drinking coca-cola.\n\n\n\n"
    author: "Duncan Mac-Vicar P."
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "\"Allende wasn't shot, he commited suicide, using a gun that was a gift from Fidel, and that is certified by his own personal doctor.\"\n\nInteresting, you might be right:\nhttp://news.bbc.co.uk/2/hi/americas/3089846.stm\n\nI'm still not convinced that Pinochet was a very nice person."
    author: "Richard Dale"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "> I'm still not convinced that Pinochet was a very nice person.\n\nDon't convince yourself. it is not a black/white topic, but a very grey one. Anyway, I suggest moving the topic to other place ;-)\n "
    author: "Duncan Mac-Vicar P."
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "A Brazilian opinion here:\n\nYeah, Pinochet was a \"bad guy\", agreed. No good done to the chilean economy will ever justify the political killings done by the military regime in Chile.\n\nBut that does not make Allende a good guy. Allende didn't have the majority of the votes when elected, and tried to do some radical stuff, supported by Fidel, the dictator. So read a bit about Allende and Pinochet from conservative *and* leftist sources, and try to make up your mind.\n\nI did that. And my personal (and provisional) veredict is that Allende is also responsible for the dictatorship in Chile: his policies in the time leading to the coup were dividing the society in Chile, and creating the backdrop for Pinochet. And Fidel, trying to export his revolution, was very important in the division process, as the risk of a non democratic socialist / communist coup like in Cuba was also there."
    author: "Amadeo"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "\".. as the risk of a non democratic socialist / communist coup like in Cuba was also there.\"\n\nWhen Fidel and his fellow Cuban revolutionaries threw out the dictator Batista in 1959 they weren't Marxists. It was certainly very democratic - everyone hated Batista, that isn't in question. Batista was just like General Franco in Spain, an arsehole. And they've just removed the last Franco statue from a public place, history has hated Franco and his Fascists - good riddance. In 1953 Fidel wrote 'History Will Absolve Me' while in jail after the assault on the Moncada barracks. In my opinion it will.\n\nHowever, because of the American blockade the Cuban revolutionaries were forced to turn to Marxism in order to get support from Russia. They certainly didn't start off as Marxists. Fidel has probably long outstayed his welcome, but in my opinion that doesn't take away from what he achieved in the 1950's."
    author: "Richard Dale"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "But when he was in Chile with Allende, he was already a dictator.  We was killing dissidents (including some who were with him in Sierra Maestra) who didn't agree, trying to export the revolution, and with all the local (cuban) repression mechanisms in place (secret police, etc...).\n\nFidel is just another ruthless dictator. Who knows who Allende was going to be. I don't. But Fidel presence certainly wasn't helping to establish peace in the chilean society. Fidel stayed for months.\n\nNot that I agree with Pinochet, far from it. As I said, nothing justify the political killings he endorsed. Just as nothing justify the political killings Fidel endorsed (and endorses untill today)."
    author: "Amadeo"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-05
    body: "> They certainly didn't start off as Marxists.\n\nThat's actually not correct -- Fidel didn't start off as a Marxist, but the other two central figures of the Cuban revolution, Che and Raul, were very much Marxists prior to the overthrow of the Batista regime."
    author: "Scott Wheeler"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-05
    body: "Money, bussiness and church are not evil. Torture is evil. Murdering people because of a different politic view is evil. Where I live there are plenty of chilean people who had to leave the country when Pinochet got to power, and they can still show you the marks of torture, so I dont need any \"propaganda\" to make me believe that the Pinochet regime was indeed evil.\n\n\n"
    author: "Sergio"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-05
    body: "Nobody denies that. What I am trying to say is there is no good and bad side here. Communists killed people (specially policemen) and were armed. It was a war. There was even a senator killed by communists *in democracy* after Pinochet left power. Nobody denies Pinochet regime horrible crimes too. My worries is that all people involved in tortures and executions is in the jail (hundred of militars) but they judge process never end. While involved communists are free. The murdereds who killed the senator Guzman in democracy even escaped the high security jail *in helicopter* and where received as \"political persecuted\" -persecuted in democracy?- by Belgium and other European countries (double standard as they wanted pinochet to be extradited to judge him).\nI am not trying to denie crimes or convince yourself of a political view, but to show you both sides did horrible mistakes, but the propaganda resulted in this double standard."
    author: "Duncan Mac-Vicar P."
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-14
    body: "Duncan, tu opini\u00f3n, matizada y todo, seguramente, gracias las  \"revelaciones\" de los \u00faltimos a\u00f1os acerca del r\u00e9gimen de Pinochet, no deja de ser la de la derecha tradicional chilena, defensora de Pinochet hasta la impostura. Por cierto con m\u00e1s porcentaje que los comunistas que tanto hablas, pero totalmente impresentable al tratar de defenderlo. El resto es paja.\n"
    author: "Alan"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-14
    body: "Si me especificas con que frase estoy defendiendo alg\u00fan r\u00e9gimen. En mis posts anteriores justamente recalqu\u00e9 mi postura de no justificar absolutamente ninguna de las posturas de las cuales coment\u00e9, sino dando mi punto de vista cr\u00edtico a ellas. Hay gente que cree que por no ser partidario de alguna ideolog\u00eda entonces apoyas completamente otra. Bueno, respecto a tu comprensi\u00f3n de lectura no puedo hacer nada, pero no pongas palabras en mi boca.\n\n"
    author: "Duncan Mac-Vicar P."
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-04
    body: "\" \"so free software plays better with free-economies and open markets.\"\n\n Yes, if a free market is one where large companies can't shut down smaller companies by using software patents, or use unfair means such as accountancy fiddles and so on.\"\n\nNevertheless, only in a democracy we can demonstrate w/o have to worry for our lives. It's true that big companies are getting aristocratic tendencies. We can only hope the masses see through this before it can only be undone by revolution, which could lead to some kind of dictature.\nIn the netherlands it used to be too expensive being rich, but nowadays that's not the case anymore. Leaders of big companies are getting lots of bonuses even if they fired lots of their employees.\nIMO the major problem with the free market as we know it, is that companies can grow to big, have too much power. In the past those companies would have been split up, but we're in a world market now. Bill G. can get away with it because after all, it's good for the USA itself. So we need global awareness of this, eg. via OSS, and some UN of business afairs and deregulations.\n"
    author: "koos"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-05
    body: "\"Nevertheless, only in a democracy we can demonstrate w/o have\n to worry for our lives.\"\n\nYes, only in great democracies <A href=\"http://news.bbc.co.uk/1/hi/world/europe/1450090.stm\">like</a> <A href=\"http://www.commondreams.org/headlines01/0729-03.htm\">Italy</a> and\n<A href=\"http://www.wsws.org/articles/2001/jan2001/imm-j05.shtml\">Australia</a>.\n\nTruly, the freedom to protest is not the right to safety, nor\nis it the right to choose how you will be governed.  If you\noppose the freedoms of the rich in any 'democratic' country\nyour own freedoms are granted only at the discretion of the\nstate.\n\nInternationally, it is the 'democracies', not dictatorships, which\ndeliver death and destruction to foreigners.  Dictators don't go\nattacking their neigbours without the <A href=\"http://www.gwu.edu/~nsarchiv/NSAEBB/NSAEBB62/\">green</a>\n<A href=\"http://csmonitor.com/cgi-bin/durableRedirect.pl?/durable/1999/05/27/p23s3.htm\">light</a> from the big 'democratic' leaders.\n\n\"So we need global awareness of this, eg. ...  some UN of business\n afairs and deregulations.\"\n\nYou mean the WTO?  (Or do you mean the <A href=\"http://www.corporateeurope.org/icc/un.pdf\">UN</a> itself?)\nCentralising the corporate takeover of the global economy in the WTO\nmade too attractive a target for democratic protest ...  so these\nthings are now handled government-to-government in bilateral\nagreements.  Too many meetings to coordinate mass rallies.\nNo great gatherings of world leaders to make headlines.\n"
    author: "Jonathan Maddox"
  - subject: "Re: Software Libre Revolution!"
    date: 2005-04-05
    body: "> http://news.bbc.co.uk/1/hi/world/europe/1450090.stm\n\n\"shot dead as he attacked police in their car\" \n\"who chose to protest violently\"\n\nYeah, that's what I call exercizing one's democratic rights.\n"
    author: "ac"
  - subject: "KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "Latin American countries are moving to Free Software because is cheap, they don't code open sourse software and they won't buy Qt licenses, to expensive.\n\nThat's why GNOME is a hit in Brazil, no licenses issues with close sourse applications.\n\nsimple.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "I should not feed the troll, but I can't help myself:\n\nIf you do not have any facts or something constructive to say, get lost. I am tired of the clueless license discussions. Gnome is very popular in Mexico, but in the rest of Latin America, the picture is mixed. Conectiva (in Brazil) is mainly a KDE distro, and there are many KDE developers in Brazil.\n\nQt is free as in GPL. Deal with it. If you want closed source apps, buy Qt, it is worth it. If you want free apps, use Qt, it is free. And with Qt/KDE 4, KDE apps will be available for Windows, easing the transition from Windows to Linux, giving KDE apps the real edge. kdelibs will be available and well tested with Qt in all platforms, giving more depth to Qt. Just watch what the Kexi guys are doing, it will be soon true for all KDE apps."
    author: "Amadeo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "It is not that simple. The goverment sponsored move is due to the need for independence, and to promote local business. A local minibusiness doing Linux support for goverment is money that stay in the country.\n\nThe goverment don't need to buy Qt licenses if they develop everything under the GPL. Even companies can develop internal software under the GPL. Only companies distributing software and selling it without source and GPL terms, need to buy per/seat developer licenses. \n\nQt licenses are expensive only for hobby programmers, but not for real projects. I imagine you already read the quick cost analysis George Staikos did (http://www.staikos.net/~staikos/whyqt/).\n"
    author: "Duncan Mac-Vicar P."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "No, Latin American goverments do not write that much GPL applications(specially Venezuela), they don't like the idea of share they code for security reasons."
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Then they surely prefer closed-source Microsoft Windows for security reasons? *SCNR"
    author: "ac"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "And no, In Latina America comercial software companies are to small to buy Qt licenses, and there's no point of using GPL applications if they cannot sell them."
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Do you consideer US$ 1500 per developer expensive for any project? (it can be used for as many projects as you want). Even with 1 developer (you), a price of US$ 100 per copy, whith 20 copies you win US$ 500. If you can't sell 20 copies of your software, As George said, you can't code for a living, or, take a look at your business plan.\n\n"
    author: "Duncan Mac-Vicar P."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "1500*5 developers = 7,500 dls.\n\nPrograming with .Net = 0.0\n\nPrograming with GTK = 0.0\n\nQt is an option, an expensive one.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "No, you have to include the cost of opportunity == time saved of using Qt, which can be used for another contract work, Using Qt allows to save months compared with GTK. Either you add that cost to the GTK option as a \"suck cost\", or substract it to the Qt option as an advantage.\nFor .NET the difference is less than with GTK, but this option is discarded for development over Linux. Also you did not include the license of the form designer and build tools. (Only the compiler is available for free). You also could use SharpDevelop. But still Qt gives you portability, which should be included as a long term cost, or you can also include the cost as the cost to update Windows in the future.\nAs you see, I will not choose you to evaluate a development project cost ;-)\nIt remembers me the Mexico goverment guys who were doing a 2 year development project in C/GTK/PostgreSQL. You know the result. It is more cheap for them doing it from scratch in Qt/QtSQL/Postgres than trying to finish it and fix all the memory leaks. But they can't tell that to their boss. Ian Geiser remembers the situation very well."
    author: "Duncan Mac-Vicar P."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>> Using Qt allows to save months compared with GTK.\n\nAll dependes of the developer skills, anyway, you are more productive with .NET than any Qt tool.\n\n>>> Also you did not include the license of the form designer and build tools. \n\nI uses I SharpDevelop to write .NET/Mono applications, it is Free and rocks.\n\nAnyway Visual Studio is a lot more cheaper than a Qt Licenses.\n\n>>It remembers me the Mexico goverment guys who were doing a 2 year development >>project in C/GTK/PostgreSQL. You know the result. It is more cheap for them >>doing it from scratch in Qt/QtSQL/Postgres than trying to finish it and fix >>all the memory leaks. But they can't tell that to their boss. Ian Geiser >>remembers the situation very well.\n\nMaybe theye weren't the right develepers for the project, they did choose C, but now there are python bindinds for GTK, there's also GTK#, C is not a limitation anymore.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">I uses I SharpDevelop to write .NET/Mono applications, it is Free and rocks.\n\nAha, the GNOME troll shows its bias."
    author: "Amadeo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Mono is illegal copy of .NET!  You will pay very heavy price for this to Microsoft lawsuit!"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>Mono is illegal copy of .NET! You will pay very heavy price for this to Microsoft lawsuit!\n\nIs not true, please seek for information before make your astatements, there is a nice mono in fag in www.go-mono.org\n\nAnd if that's the case Qt and KDE are violating Microsoft patentes too, since Microsoft has even patent the grouped windows KDE, mouse click, context windows buttons etc, something that Qt/KDE uses too in that case it is risky to use it too.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "It is true.  Do not believe Miguel.  Miguel lie before and Miguel lie now. Only lies in that fag on go-mono.org. Only believe if you have agreement with Microsoft.  KDE/Qt are safe there are no valid patent against!\n\n.NET is very big trouble as small developer you should stay away from Mono.  ;-)"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "let's assume everything you said about licensing concerns is true (i have my doubts, but let's just assume that anyways)... what is the connection between writing a Gtk+ app and not using KDE? or... conversely, what's the issue with writing a Qt/KDE app and not using GNOME? \n\nit seems people do it ALL the time and that with every single passing year it works better and better.\n\nthe whole \"licensing ergo desktop\" domino theory stinks as hopeful thinking, probably on the part of whomever started this silly meme a few years ago."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "Using KDE to run GTK based applications? no use, KDE is to complicated for the masses, they better can use GNOME with they GTK applications to have the advantage of easy to use.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "*deep breath so as not to flame you to a cinder*\n\ndo you know who you, as a GNOME fan, benefit the most by slamming KDE? everybody except for GNOME and KDE! Microsoft, Apple and everyone else looking to put a desktop environment into the marketplace thanks you and your inability to recognize your friends from your enemies. congratulations.\n\nmoreover, if KDE is too complicated for the masses, why have professional usability studies between Windows XP and KDE 3.3 shown otherwise? and why do more of the \"masses\" you speak of use KDE than GNOME? the answer is that KDE, despite your protestations, is not too complicated for the masses. i'll be the first to stand here and say that it can be better; so can GNOME, so can Windows, so can MacOS X. they each have strengths and weaknesses which enable and hinder their use by \"the masses\".\n\nbut you and the rest of the destructive anti-fans in the broad ranging open source community need to get your priorities straight and your facts checked. every day i struggle against the unnecessary and inexcusable damage people like you create for the open source desktop when it comes to public perception. even if you are simply trolling for fun, it has real (negative) consequences for all of us here. bah, humbug to that! >:-("
    author: "Aaron J. Seigo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "Im as much GNOME fan as you are a KDE fan.\n\nAbout the easy to use, well, we are talking about people who don't know even how to use Windows well, KDE is harder to use than Windows (I don't care what you say but is true), not having HIG guide lines it is costing a lot to KDE, but tha's not the main problem, the main problem is the Qt license cost.\n\nIm just saying what I think, KDE has no chance in Latin America.\n\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "KDE HIG:\n\nhttp://developer.kde.org/documentation/standards/kde/style/basics/\n\nWow, you're really taking a beating.  Everyone is proving you wrong today.  Good day for KDE advocacy..."
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>>ttp://developer.kde.org/documentation/standards/kde/style/basics/\n\nAre you talking about that unfinished not tested with no background studies HIG?\n\nAre you serious?\n\nCome on.\n\nread this:\n\n\"many of our user interfaces are a mess. they are featureful and interesting, but many times lack the quality of \"clarity\"\".\n\nThose are the words of Aaron Seigo a KDE developer, if can admit it, why can't you?\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Oh you mean the background studies that prove you should fuck around the button order and use a spatial file browser?  Please, are you serious?  Take a user poll sometime and find out how many people hate the GNOME HIG. The GNOME HIG was more dictated by corporations looking for a profit than anyone concerned with polling users. \n\nThe KDE HIG is by the people for the people and is supported by the SUPERIOR KDE framework.   GNOME HIG is hit and miss (but mostly miss), KDE HIG is right on target.\n\nGNOME development is also a mess.  This is why Miguel is wasting all his time on cloning .NET.  \n\nMiguel has admitted that GNOME development sucks.  Why can't you?"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> background studies that prove you should fuck around the button order \n\nThat has been GNOME's original usability sin.  They destroyed a very important part of *consistency* on the UNIXoid desktop. They spoiled it for everyone not running a GNOME-only or a pure non-GNOME environment.\n\n"
    author: "cm"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>Oh you mean the background studies that prove you should fuck around the button >>order and use a spatial file browser? Please, are you serious? Take a user poll >>sometime and find out how many people hate the GNOME HIG. The GNOME HIG was more >>dictated by corporations looking for a profit than anyone concerned with polling >>users.\n\nThe fact you don't like the buttons order or the spatial way doesn't give you the right to say it is wrong, I like the button order even I use that oreder in my appications and my customers are more pleased.\n\n>>The KDE HIG is by the people for the people and is supported by the SUPERIOR KDE >>framework. GNOME HIG is hit and miss (but mostly miss), KDE HIG is right on \n>>target.\n\nWhat people are you talking about? KDE geeks? there's is diference from final users and the \"people\" you say.\n\n>>GNOME development is also a mess. This is why Miguel is wasting all his time >>on cloning .NET. \n\nThis makes me think that you really have no idea about the mono project, if GNOME development it is a mess too well is not my problem, tell me in what part of my comment I mentioned GNOME HIG or GNOME development structure? none? well, that is because you are confusing this threat as a KDE vs GNOME subject, im talking about usuability and licenses and what is the best for Latin American countries trying to uses open sourse toolkits.\n\nI wont even reply to you anymore, abviously you have no idea of what you are talkin about.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Yes the button order and spatial is very wrong.  It breaks usability in GNOME very bad.  My customers don't like this!  I too am developer and I tell them to use KDE which they like since KDE HIG is superior to GNOME HIG.\n\nThe GNOME .NET problem is an even bigger threat.  You as a developer should understand this!  What happen when Microsoft ask you for money to use their .NET patent?   Your country will become even poorer because of this lawsuit!\n\nBrazil government don't like that GNOME steal from .NET and cause problems.  It is better they use KDE for better usability and it is not illegal like GNOME .NET."
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>Yes the button order and spatial is very wrong. It breaks usability in GNOME >>very >bad. My customers don't like this! I too am developer and I tell them to >>>use KDE which they like since KDE HIG is superior to GNOME HIG.\n\nYour customers have their point of view mine have theirs.\n\n>>Yes the button order and spatial is very wrong.\n\nShow me the document where it way how the order of the buttons must be.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> Im as much GNOME fan as you are a KDE fan\n\nbut we're not the same kind of fan, you see. i don't run around the internet fighting and fud'ing against \"the other guys\". that's because i realize that this is counter productive to ALL of our efforts.\n\nregardless of the accuracy of what you think, there are productive ways to express it and non-productive ways. you're showing an excellent example of the latter. whether you realize it or not, you're hurting GNOME as much if not more than you are \"telling the truth as you see it about KDE\" by doing this. and why do i care that you hurt GNOME? because what hurts GNOME hurts KDE, and vice versa. from a general public perspective we, the open source desktop projects, are in this TOGETHER.\n\nthen again, perhaps you don't care about the success of the open source desktop in the real world; perhaps posting inflammatory messages on the internet is a more important thing to you. =/\n\nnow, i'm not saying we all have to pretend to love everything everyone else does. i'm simply suggesting there are positive ways to express our opinions in a way that doesn't necessarily come at the expense of others."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Well I didn't think I expressed in a wrong way, I mean, if you gonna \"sell\" a product, you have to tell the true, pros and cons.\n\nIf you say product is completly perfect (Telling all the pros w/o the cons) then you are cheating your customers.\n\nAnd this help to make the final product better.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Have you looked at an advertisement lately?  The way to sell a product is to only talk about the pros.  Marketing based on full truth will only sell to a tiny niche.\n\nBut regardless, constructive criticism is appreciated, but yours is definitely not constructive.  Saying KDE is no good because of the QT license is stupid.  First of all, that topic has been beaten to death, and secondly, no one on this forum has the power to change this.\n\nSo what, exactly, are you trying to accomplish by making inflammatory remarks about this subject on this site?  Even if everyone agreed with you, it would change nothing, so you might as well not post at all.\n\n"
    author: "Leo Spalteholz"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Yes, the topic has been beating up before, but in differenct subjects, never about KDE being inplanted in Latin American countries.\n\nMy statement haven't been inflamatory, Im just pointing some flaws that may have inplantation of KDE in Latin America, if users in this forum can't answer in a smart and educated way w/o insulting, is not my problem.\n\nIm a developer who lives in Latin America and makes a living of it, if something like this \n\n\n>>>Saying KDE is no good because of the QT license is stupid.\n\n\nIs not stupid, hiding information  to 3Th. World Users about possible licensing issues is not only stupid, it hypocrit.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> Is not stupid, hiding information to 3Th. World Users about possible licensing issues is not only stupid, it hypocrit.\n\nJust re-checked and the pricing and licencing information are still reachable on Trolltech's website, maybe there is a problem with your internet connection/browser/proxy?"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>Just re-checked and the pricing and licencing information are still reachable on >>Trolltech's website, maybe there is a problem with your internet >>connection/browser/proxy?\n\nAnd where is the link in the KDE webpage to that page?\n\nI don't see it, I know because you are telling me, but what about the others?\n\nGood, now if only you specific to Latin American users that KDE is on top of Qt technology and put a link to the prices, that will do it.\n\nThx.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Hmm, you are right, the link to the Trolltech Website is in the FAQ and the developer documentations.\n\nMaybe the authors of the website assumed that everybody knew that KDE uses Qt?\n\nIt should perhaps be more visible on the frontpage of the developer website developer.kde.org\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Wow, you are right, sorry.\n\nThx.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "No need to apologize, it is really too hidden, at least on the developers site.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "The link is there.   Are you blind?  Check in KDE Family.  Or use Konqueror with its superior usability to Firefox.  Konqueror simply works sometimes in Firefox you are blind and don't see everything on the page maybe because of bad pop-up blocking.\n\nMaybe the other Latin American users are too poor as you are saying to come on the internet although they have old computer.  The developers are almost begging on the street trying to sell their commercial software, all of them can't yet afford internet access anyway and have to use pirated Windows copy which is full of spyware.  This is why they cannot surf the internet and read information.  Brazil government speak against this. :-("
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> And where is the link in the KDE webpage to that page?\n\nWhat does KDE have to do with the pricing and license information of Qt?"
    author: "ac"
  - subject: "swapping planet kde/gnome"
    date: 2005-04-05
    body: "Indeed, I loved the Aprils Fools joke of swaping Planet KDE/Gnome since it showed that really we're on the same team."
    author: "Ian Monroe"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> Im just saying what I think, KDE has no chance in Latin America.\n\nGood thing our contributors from Latin America think different :)\nBut maybe there are different Latin Americas on this planet, maybe we are not even talking even the same planet?"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Well, they are Latin Americans who write GPL code mostly for hobby, what about all the developers in Latin America that make a living from writing comercial software?\n\nRight now they are migrating to .NET with a cost of less than $300 dls, paying $1,500 to migrate is just to expensive for them and out of the question.\n\nIf they are gonna program in Linux Im sure they will use GTK.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "you mean like http://infosial.com who make FacturaLUX (http://FacturaLUX.org), an ERP suite using Qt?\n\ni don't even live there or keep up with the South American software scene as much as i probably should, and even i know about FacturaLUX. i'm sure there are many others. like Connectiva. ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>you mean like http://infosial.com who make FacturaLUX (http://FacturaLUX.org), an ERP suite using Qt?\n\n\nYou are talking about thos big companies that can buy Qt liceses and are just a small minirity?\n\nWhat a bout the small companies of 1 to 5 employes that cannot buy them?, are they gonna get relegated? who is going to help them? TrollTech will sell them liceses in $300 dls? no?\n\nWhat can they do?\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "A fact you don't seem to grasp is the fact that close to zero of the small companies you like to talk abut, do development based on selling copies of their application like the \"big\" softwarehoses. They make their living selling custom applications to limited number of customers, most likely different for each customer. The customer pay for the solution and support, not the application. In such scenarios there are ample opportunity to either factor in the cost of any tools needed (eg Qt) in the contracts, or doing the whole project as GPL. Using the GPL will not hurt you, since most customer are not in the software distribution business. And distribution a specialized software solution usuall only benefits their competition so this will newer happen. In addition using the GPL gives you access to a incredible amount of quality code, which can help you finish the project both quicker and cheaper(more profit). "
    author: "Morty"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>They make their living selling custom applications to limited number of customers, most likely different for each customer.\n\nThat's what I was talking about\n\n>>The customer pay for the solution and support, not the application.\n\nYes and expensier licenses make expensier final products.\n\n>>Using the GPL will not hurt you, since most customer are not in the software distribution business. \n\nJust the fact that that will hurt my support to the sustomer, I I give my software as GPL im srewed, Other will take it my customers will dump me and other will take advantages of the program I made with all effort.\n\n\n\n\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> Just the fact that that will hurt my support to the sustomer, I I give my software as GPL im srewed, Other will take it my customers will dump me and other will take advantages of the program I made with all effort.\n\nLOL! We didn't know that you're selling a shareware notepad clone."
    author: "ac"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Totally wrong, if you are not in the business of screwing your customers and use them as cash cows that is. Like delivering a crappy product needing constant support or similar. \n\nYou don't give them the application you sell it to them, GPL allows and encourage this practice. If you base your income on the eventual supportcontract you get when selling the application, you only have to keep the customer happy with your performance and effectively. Given you are not pricing yourself unreasonable compared to your competition. In cases like this you have to be real bad to make your customers switch, since they have already made an investment in you. Getting another contractor up to speed with routines, functionality and code are inconvenient and costs money and time. All heavy factors making the customer stay with you.   "
    author: "Morty"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "You are way too focused on your personal problem with free software.\n\nI'd say most of our Latin American developers are professional software developers and if they think spending their precious spare time on learning and working with some technology is ultimatively a waste of resources, they wouldn't do it.\n\nOf course there are developers who want to restrict themselves to Windows as much as possible and of course they cheered upon the rise of .Net\n\nOf course there are developers who like to believe that they are the only ones who can create certain tools so user will even pay small amounts of money for them (also known as the shareware misconception, i.e. plenty of gratis toolkits on Linux yet not very much shareware)\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Im sure there are some of them, but remember Latin American countries are poor contries Qt liceses for the masses is out of the question.\n\n>>You are way too focused on your personal problem with free software.\n\nHell no, I love free software , I use it, since GNUMERIC to INKSCAPE in my Windows PC, I hate the fact to pay an expensive licenses to a company in Norway, Microsoft with all its flaws can sell it more cheaper and GTK with all its flaws allow me to save money.\n\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">I hate the fact to pay an expensive licenses to a company in Norway.\n\nAnd you have nothing against people paying expensive licenses to you, I suppose... Now who is the hypocrite?"
    author: "Amadeo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>And you have nothing against people paying expensive licenses to you, I suppose... Now who is the hypocrite?\n\nNo I don't, if they but it, is because they have money and can buy it, and that is their problem, Im talking as a developer in Latin America who cannot pay for them and like me there are many, that IT IS my problem.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "You will have big problem with Mono.  Just wait for Microsoft to knock on your door.  ;-)"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Don't worrie, Im ready =).\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> Im talking as a developer in Latin America who cannot pay for them and like me there are many, that IT IS my problem\n\nWell, then *don't buy* the Qt license. Do you really think that anybody here cares about *your* choice of toolkit?\n\nIf you can't afford a license and you are also afraid to ask Trolltech for a discount, then that is okay. But please stop this trolling.\n\nThanks"
    author: "ac"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "You said \"KDE has no chance in Latin America\"\nThen I said obivously our Latin American developers think the opposite.\nThen you suggest that their opinion doesn't count because its a hobby, to which I pointed out that they are very likely not investing time and other resources in something they don't believe in having value for their professional life as well.\n\nBut as you insist that the price for a specific Qt licence makes their professional opinion about the future of a technology null and void, I guess that there is a huge difference in which part of Latin America you live.\n\nAnd perhaps the customers where you live are only willing to pay for licences and never for any service. Poor IBM must have closed all outlets there.\n\n> Microsoft with all its flaws can sell it more cheaper\n\nAs far as I know there is no Microsoft product comparable to Qt.\nMostly because they have strong interests to bind developers to their platform, so I find it highly doubtable that they should really offer an application framework which allows at least Windows+Unix/Linux development.\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>Then you suggest that their opinion doesn't count because its a hobby\n\nI dind't say they opinion doesn't count', got read it again  and don't put words I didn't say, those developers have all my respect.\n\nThey of view is totally diferent from some one who makes a living of comercial software, if you knew how much a developer in Latin America earns a year you would be surprised.\n\n>>But as you insist that the price for a specific Qt licence makes their professional opinion about the future of a technology null and void, I guess that there is a huge difference in which part of Latin America you live.\n\nAll the countries have the same common problem, they are poor and want to reduce cost, not make them bigger and .NET/GTK it is cheaper.\n\n>>>Poor IBM must have closed all outlets there.\n\nIBM is a company that can pay for Qt licenses and give services to bussiness who have money enoght to pay for support to IBM, what about the small bussines who can't pay for IBM support?\n\n>>As far as I know there is no Microsoft product comparable to Qt.\n\nThey don't need to develpe one. .NET cover the needs and is cheaper.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> I dind't say they opinion doesn't count'\n\nTrue, you didn't, but at the moment I brought them as an argument, you put them down to \"hobby\" status, while most if not all of them are prefessional developer like you claim you are too.\n\n> if you knew how much a developer in Latin America earns a year you would be surprised.\n\nI don't know, never ever claimed I did, all I said was that our developers from over there have a different opinion and I guess they know.\nAnd if knowing how much a developer earns in Latin America and still believe that investing resources into KDE than I make assumptions on why their view differs so much from yours.\n\n> what about the small bussines who can't pay for IBM support?\n\nThat's the point!\nIBM can make huge profits offering service to big businesses, so why can't anyone else make profits offering services for small business (logically on a different price scale)\n\n> They don't need to develpe one. .NET cover the needs and is cheaper.\n\nLast time I checked Microsoft didn't have an implementeion for any other platform than their different Windows platforms.\nSure that is some kind of multiplatform as well, but not the kind of multi platform I was thinking of, which at least involves Linux :)\n\nBut I am not following that closely, if you can point me to either a download page on microsoft.com or a press release on their website where they have at least a Linux port, I take everything back"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>And if knowing how much a developer earns in Latin America and still believe that investing resources into KDE than I make assumptions on why their view differs so much from yours.\n\nThat's the problme you don't seem to understan, many Latin American developers can't invest in KDE because they are poor and doesn't have the money, tha's what Im trying to tell you they can barely survive qith the money they make to buy expensive Qt licenses, and why buy a Qt licenses when there are another cheaper options for they needs?\n\n>>IBM can make huge profits offering service to big businesses, so why can't anyone else make profits offering services for small business (logically on a different price scale)\n\nBecause:\n 1.- they don't have the money to invest in Qt licenes like IBM\n 2.- Expensive licenses make final product expensier.\n 3.- there are already chepar options.\n\n>>Last time I checked Microsoft didn't have an implementeion for any other >>platform than their different Windows platforms.\n\nWell then I recomment to go to an cheaper option like Novell or Sun Java Desktop, but not Qt/KDE.\n\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "I do understand that there is a huge difference in financial situations between Europe and Latin America that's why I based my comments on the opinions of the KDE developers in Latin America as they had posted them on this topic.\n\nI do believe that they know the local situation a lot better than I do, I even believe they know it as good as you do.\nAnd I am really sorry but I they claim something that differs from your claim, they have higher credability for me.\n\n> 1.- they don't have the money to invest in Qt licenes like IBM\n\nI am afraid I can follow you thoughts here. How are Qt licences related to IBM service contracts?\n\n> 3.- there are already chepar options.\n\nVery good. So other companies already do make profit with cheaper support contracts, that was my point, wasn't it?\n\n> Well then I recomment to go to an cheaper option like Novell or Sun Java Desktop\n\nSo I take it that your information about Microsoft offering something comparable to Qt was all wrong?\n\n\n\n\n"
    author: "Kevin Krammer"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "> what about the small bussines who can't pay for IBM support?\n\nWell that's okay. But do you see any of those small businesses troll on the IBM forums that their support contracts are too expensive?"
    author: "ac"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-06
    body: "Hi, i am in Venezuela, and most of us use KDE.\n\nWith globalization, it doesn't matter if KDE \"fails\" in latin america. As long as i can download it from here, it has succeeded. So shut up your keyhole, troll."
    author: "Anonymous Coward"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "http://www.freedesktop.org/Software/gtk-qt"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "[i]Qt is free as in GPL[/i]\n\nNot everyone, special goverment, likes to write GPL applications, the alternative is to pay $1500 per developer and in LatinAmerica that is out the Questions.\n\nGNOME is more popular in Brazil than KDE, the Telecentros sponsored by goverment are running on GNOME desktop, and companies usually uses Goverment line ups.\n\n\nKDE simple, has no chance in Latin America.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "Not quite agree with your statement.\nFirst of all, been around whole Brazil in last year ( and again starting this month ) going invited to talk about KDE and been fully sponsored, i saw the adoption of both desktops equally accepted.\nIs a matter of fact that much of the installed hardware park is too old to accept ANY of the modern desktops like KDE or Gnome, and that's why we have a lot of computer users ( not regular users ) using XFCE, windowMaker or lightweight things like Black/Fluxbox.\nDuring my travels, i saw the Telecentros ( which is implemented in part by the company which i work ) using Gnome, in the same way as i see major governments using variants of Kurumin ( a knoppix based brazilian distro ) and last year large part of ITI using KDE on Mandrake Linux.\nThe reality is that never exists such prediposition for one or another desktop. The old \"GPL\",\"Non-Free\" blame is just reduced to some intransigent niches which thanksfully not represent the majority of IT migration serious teams.\nAt least i can mention some educational ONG's preparing to migrate to KDE in S\u00e3o Paulo to earn the benefits of KDE Edu project, and the advent of Kontact with their flexibility of groupware access been more than well accepted here.\nPlease let your unbiased comments off to not make nice people outside here think we are a bunch of morons"
    author: "Helio Chissini de Castro"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "it seems you think GNOME is free and not GPL while KDE/Qt is free and GPL.\nfreedom speaking, GNOME and KDE are the same, they're both GPL.\nSo you say that government does not like GPL then that Telecentros sponsored by government runs GNOME in Brazil then from that you deduce KDE has no chance in the whole Latin America! Wew, what a neat demonstration, i'd like having your deducting power. \nAnd \"Not everyone, special goverment, likes to write GPL applications, the alternative is to pay $1500 per developer and in LatinAmerica that is out the Questions.\"  so no GPL and no license? hmm, what's left? ah, I see, GNOME!"
    author: "anon"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-04
    body: "Im just putting the disadvantages of using KDE, yes the licenses IT IS an issue.\n\nIt is cheaper to keep using Microsft Windows than switch to KDE.\n\nPresident Silva from Brazil he wantys to use Free software to not give money to a monopoly like Microsoft, what's the diference in giving the Money to a Monopoly in USA than giving it to one in Norway?\n\nI really think KDE has no chance in Latin America at all.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "http://www.conectiva.com.br/"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Conectiva Linux is a Mandrake like distros it ships with all the DE possible.\n\nAnd I couln't read where it says the develope with Qt software, (yes, I can read portguese)\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Mandrake is phasing out GNOME but you didn't get a press-release on that did you.  Look at how many Mandrake products come with only KDE.  Conectiva has always favored KDE.  Period.  \n\nAseigo admitted this, why can't you?"
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "BTW, aren't they the creators of Synaptic, a GTK application?"
    author: "ASD"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Kynaptic is maintained by both Conectiva and Debian and fully supports KDE.\n\nhttps://oops.kerneljanitors.org/repos/synaptic/branches/kynaptic/\n\nAlso see that GTK-Qt link I provided."
    author: "dc"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>Kynaptic is maintained by both Conectiva and Debian and fully supports KDE\n\nI didn't say they didn't, I said they were the creator of a GTK application, and most of there develpment is GPL, so IM talking about people who doesn't want to write GPL applications.\n\n>>>Also see that GTK-Qt link I provided.\n\nGTK-Qt only makes GTK based application to use KDE dialogs or themes, it wont cut the number of buttons in Konqueror or other KDE cluttered KDE applications.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Using the cost factor too justify using windows rather than KDE are actually one of the most stupid arguments I have ever seen:-) Since a commercial Qt  don't cost more than about 10-15 windows licenses (Additionally to do anything usefull on windows you need apps, which cost money making this number even lower). And there are no place in the world where commercial development with a ratio of 15 users per developers makes any economic sense whatsoever, expect when the finished product are very highly priced. And in those cases absorbing the cost of Qt licenses are not a big problem anyway.\n\nTake Brazil as an example I'd guess if they take 10% of what they are paying Microsoft today and give to Trolltech instead, they have development licenses for all developers in Brazil both private and government. They would even have some to share with developers in neighboring countries. \n\nAs for your argument about monopoly's, you have to realize Norwegians are much nicer than those pesky American capitalists:-)"
    author: "Morty"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">\"Not everyone, special goverment, likes to write GPL applications\"\nProof that governments do not like to write KDE apps? Do you have any?\n\n>\"GNOME is more popular in Brazil than KDE\" \nProof? Statistics? A screenshot of one deployment does not count. BTW, I am Brazilian, and I don't think GNOME is more popular. Kurumin, for instance, is a widely used debian based distribution that is based on KDE. On the bookshops, there are several magazines with linux CD's, most of them with KDE.\n\n>\"KDE simple, has no chance in Latin America.\"\nYou are a troll (who does not respecty facts or logic), and you have no idea what you are talking about. Now come back with some facts, or better, don't come back at all."
    author: "Amadeo"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>Proof that governments do not like to write KDE apps? Do you have any?\n\nMy best friend works for goverment, they DO NOT LIKE gpl code to much.\n\n>>Proof? Statistics? A screenshot of one deployment does not count. BTW, I am >>Brazilian, and I don't think GNOME is more popular. \n\nThat's weird, if you live in Brazil how come you don't know that?\n\n>>>You are a troll (who does not respecty facts or logic), and you have no idea >>>what you are talking about. Now come back with some facts, or better, don't >>>come back at all.\n\nNo im not a troll, Im sure my vision is more open than yours, my concern is the $1,500 licesne for comercial applications, Im a developer in Latin America and I make a living of it, Im sure my point of view count as much as any concerned programer who doesn't have the money to play $1,500 per developer to acompany in Norway."
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>>Proof that governments do not like to write KDE apps? Do you have any?\n \n>> My best friend works for goverment, they DO NOT LIKE gpl code to much\n\nIf they don't like GPL code, then they should not be using Linux, it is GPL all the way :o)\n"
    author: "ac"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: "Best post in the entire thread."
    author: "Illissius"
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>If they don't like GPL code, then they should not be using Linux, it is GPL all >>the way :o)\n\nLinux is a Kernel not a programing tool kit, wake up and release that most of the developers on Latin America won't mess with it.\n\nI have no problems with GPL code, I have issues with Qt expensive licenses.\n\nAnd you should know people who develope small aplications to sell them, dont release them as GPL, some one can make software in his free time and release it as GPL, but if it has to develope an application to sell it nd make a living of it, maybe he don't want it to be GPL and what options does he have? buy an expensive Qt license with money that maybe he doesn't count with.\n\n"
    author: "ASD..."
  - subject: "Re: KDE IN LATIN AMERICA WONT WORK..."
    date: 2005-04-05
    body: ">>Proof that governments do not like to write KDE apps? Do you have any?\n>My best friend works for government, they DO NOT LIKE gpl code to much.\nThere are two answers to that.\n-Your friends and their superiors are affected by some anti GPL FUD, clueless, incompetent or just to lazy to care. Perhaps a combination of those, it's fairly common among government employees all over the world.\n-Or they are contractors hired by the government and don't want to kill their cash cow, by not creating a vendor lock in situation."
    author: "Morty"
  - subject: "In summary"
    date: 2005-04-05
    body: "Since this thread got a bit confused and sidetracked, let me summarize it.\n\nSo, our friend ASD is saying that given 2 alternatives for cross platform development, developers with limited financial resources will always choose the free (as in beer) one.\nASD says, no matter how insignificant the QT license may be on a grand scale, GTK will be cheaper.  This is of course true from a simplistic standpoint.\n$1500 * developers + wages (for QT) > $0 * developers + wages (for GTK)\n\nHowever, the counterarguments are numerous and convincing\n1.  QT is of higher quality (just compare documentation and the implementation on MS Windows, you cannot seriously argue this point).\n2.  QT will allow you to get your work done faster (and therefore cheaper) because the equivalent code with QT has fewer lines of code.  (see http://phil.freehackers.org/kde/cmp-toolkits.html, there are others like it)\n3.  QT offers far more features than GTK does.  QT is not just for GUI.\n4.  QT license gives you professional support, GTK does not.\n5.  You can use the free GPL version of QT even for commercial applications.  Common practice amongst reputable custom development houses is to provide the source along with the application they develop.  The GPL only requires you to distribute source to those who get the binary.  Your customer can keep the software entirely to themselves, which would be in their best interest anyway.\n6.  Native QT support on Mac OS X is far far better than GTK.  No contest.\n\n\nAs for .NET, it is not cross platform (at least System.Windows.Forms isn't), so it cannot be compared to QT and GTK.\n\nSo, do you realize why people don't accept your complaint now?"
    author: "Leo Spalteholz"
  - subject: "In summary is a wrong statement re. GPL!"
    date: 2005-04-05
    body: ">>> The GPL only requires you to distribute source to those who get the binary.<<<\n\nNot true.\n\nIt also requires you to distribute (upon request) the source to those whose source you used. That would be Trolltech.\n\nOther than this, I agree with your summary."
    author: "Anonymous Son"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-05
    body: "Oh.  I wasn't aware of that.  Can you point me to the section of the GPL specifying that term?  I couldn't find it (although I admit I didn't search exhaustively).\n\nI was basing my statement mainly on this GPL FAQ entry:\n\n Does the GPL require that source code of modified versions be posted to the public?\n    The GPL does not require you to release your modified version. You are free to make modifications and use them privately, without ever releasing them. This applies to organizations (including companies), too; an organization can make a modified version and use it internally without ever releasing it outside the organization.\n\n    But if you release the modified version to the public in some way, the GPL requires you to make the modified source code available to the program's users, under the GPL."
    author: "Leo Spalteholz"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "He is just confused and mixing in a clause from the QPL.\n\nActually this is the clause in the QPL making the GPL fanatic cry out and declare the QPL not compatible with the GPL."
    author: "Morty"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "Nice to see you present yourself so un-confused and convincingly clear then."
    author: "Anonymous Son"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: ">>> Can you point me to the section of the GPL...? <<<\n\nSorry, no. I am too lazy/tired right now. I hope you understand....  ;-)\n\nLook for the spot where it says that you as the author of original code can aske anyone who uses this code under the GPL, and releases this modified code somehow to an outside entity must make the modifications available to you.\n\nYou are right in that not *anybody* can ask for the source, just the ones who received binaries under the GPL. But also the original authors can claim this.\n\nOnly fair enough, isn't it?"
    author: "kmail lover"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "Consider this:\n-- You are the author of a piece of software. It is all your code.\n-- You release it to the public. As source code.\n-- A company takes the source. Improves it slightly. \n-- Company sells binaries of it. \n-- Company complies to the GPL by also giving source code to its customers   (should they want this).\n-- Company makes lots of money. Really lots. From what is mostly *your* work. \n-- Making money this way is OK. Nothing wrong with this: Compliant with GPL.\n-- Now *you* ask company to give you the source/their modifications.\n-- Company declines. They say \"No, sorry, dude! Buy our binaries, and \n   you'll also get the source.\"\n\nWouldn't that be a flaw in the GPL? Can you imagine such a flaw in this wonderful license? Doesn't the GPL demand to \"give back\" the source \nmodifications once they are distributed to \"the public\"? Can you imagine\nthat the GPL would so much neglect you, the original author?\n\nIt is left as an excercise to the reader to find the paragraphs in the\nGPL wording which do in fact support what I popularize here. Then paste\nthem to illuminate everone else."
    author: "Anonymous Son"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "> Wouldn't that be a flaw in the GPL? Can you imagine such a flaw in this wonderful license? \n> Doesn't the GPL demand to \"give back\" the source \n> modifications once they are distributed to \"the public\"? Can you imagine\n> that the GPL would so much neglect you, the original author?\n \n\nI don't see a problem with that.  If that company makes that much money (and you didn't) it is very probable that *their* work made the difference.  They get to choose who to distribute their important contribution to (at first!), but of course under the GPL. \n\nNo one can keep the *customer* from redistributing the code, though.  So the requirements of the GPL as I understood it are satisfied. \n\n\n\n> It is left as an excercise to the reader to find the paragraphs in the\n> GPL wording which do in fact support what I popularize here. Then paste\n> them to illuminate everone else.\n\nIMHO it's up to you to do that. After all it's you who made that claim.  Leo Spalteholz already quoted a section from the GPL FAQ that suggests that it is not true (indirectly, by mentioning only the *user* (the one the modified software is distributed to), not the *original author*). I haven't found anything that supports your claim in the GPL.  \n\nMaybe you're right, but up to now all we have as \"proof\" is your posting on the dot. I'm afraid you'll have to back that up if people are to believe you (and not by just stating things like \"But wouldn't it be unfair to the original author if ...\").\n\n"
    author: "cm"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "Don't confuse modifying sources with mixing with a library. What you say is true only if you modify Qt itself."
    author: "Duncan Mac-Vicar P."
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "So a company linking their code against my GPL code wouldnt allow my to claim their source from them? \n\nStrange reasoning you are proposing here...."
    author: "Anonymous Son"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: "If a company makes changes to a program and uses it only internally then you are not entitled to request the sources.  The GPL FAQ explicitely states this (see the section quoted by Leo above).  So the thought of modification without giving out the source to the original author is not as strange as you suggest.  \n\nI know the GPL sees distribution and internal use of modified versions as fundamentally different things so this is no proof.  But it's a related situation:  A company uses your code in a modified form and you're not entitled to claim the source from them.  \n\nBut where does the GPL talk about special rights of the original author anyway?   The spots I could find talk only about the *user* of the modified version.  That is the person the code is distributed to. \n\n"
    author: "cm"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-06
    body: ">>> the thought of modification without giving out the <<<\n>>> source to the original author is not as strange as <<<\n>>> you suggest. <<<\n\nRight. If they don't distribute outside their organization, yes.\nIf they do, no.\n\nIf you go back a few postings, towards the start of the thread, it says:\n\n---------------------------------------------------------------------\n...anyone who uses this code under the GPL, and releases \nthis modified code somehow to an outside entity....\n---------------------------------------------------------------------\n\n---------------------------------------------------------------------\n"
    author: "mountebank"
  - subject: "Re: In summary is a wrong statement re. GPL!"
    date: 2005-04-07
    body: "I know very well that the thread was about releasing code to an external entity:  To the customer a.k.a. user (*he* is the one who has a right to receive the source code including all the modifications, no doubt about that, that's at the core of the GPL).  \n\nI just wanted to see a proof (e.g. a quote from the GPL or the GPL FAQ) that says that the original author of the software has any claim to the source code of the modifications if neither the author or the modifications nor the customer who got the modified source are willing to give it to him.   Because that's what \"Anonymous Son\" had claimed! \n\nSee also: \nhttp://www.gnu.org/licenses/gpl-faq.html#GPLRequireSourcePostedPublic\nhttp://www.gnu.org/licenses/gpl-faq.html#CanIDemandACopy\nIt doesn't talk about any special rights of the original author, and not about the \"general public\", but only about the program's users, i.e. the customer you distributed the modified software to.\n\nThe intent of my post you replied to was to demonstrate that the handwaving \"proof\" by Anonymous Son just doesn't cut it.  He tried to make the thought of modifying the source without giving out the changes sound ridiculous.  It isn't, and the GPL FAQ even mentions an example for that (although this case is about internal use). \n\n"
    author: "cm"
  - subject: "Good by.."
    date: 2005-04-05
    body: "The discution is interesting but I have to go, sorry if I wont be able to reply future Threats.\n\n"
    author: "ASD..."
  - subject: "Re: Good by.."
    date: 2005-04-05
    body: "Woohoo!!\nThe troll has been beaten :)"
    author: "ac"
  - subject: "Re: Good by.."
    date: 2005-04-05
    body: "KDE's community is simply no match for GNOME community.\n\nGNOME community has been overrode by companies who have chased away all the GNOME users by making bone-headed decisions like using the wrong button order on Unix and spatial file browsing.  It is sad but true.  Now Miguel is turning GNOME into a .NET clone and there will be a big fight between Novell and Sun over .NET and Java.  The GNOME community will fall apart even more.\n\nAll that will be left is one brazillian GNOME user called ASD...  and he will be very sad and lonely.  ;-)"
    author: "dc"
  - subject: "Re: Good by.."
    date: 2005-04-05
    body: "From reading his comments/trolling he looks to me as already being sad and lonely. Friendless and lonely, reduced to troll KDE news sites to get any attention, rather sad if you ask me. "
    author: "Morty"
  - subject: "Re: Good by.."
    date: 2005-04-05
    body: "I don't think ASD is Brazilian. If he was, he would know about Conectiva, Kurumin and brazilian KDE (or kernel, since he said latin america does not care for the kernel) developers, like Marcelo, Helio, Arnaldo, Thiago, Henrique, etc...\n\nJust a troll, move on..."
    author: "Amadeo"
  - subject: "1500 USD Qt License - What Does It GIve You?"
    date: 2005-04-05
    body: "Does a Qt license entitle 1 developer to using Qt only for the version available at the time of purchase, or is it for all Qt versions, future and current as well as support from Trolltech forever?\n\n"
    author: "Alex"
  - subject: "Re: 1500 USD Qt License - What Does It GIve You?"
    date: 2005-04-05
    body: "\nFuture versions get discounts IIRC. Future support should be consideered in costs. But as .NET it is the same situation, and GTK has no support. No need to compare here, except for the fact that TT support is very well rated.\n"
    author: "Duncan Mac-Vicar P."
---
Last Saturday saw the first <a href="http://installfest.info/default.en">Latin America Free Software Install Fest</a> held simultaneously in 74 cities and 12 countries.  KDE was present at the <a href="http://www.cdsl.cl/flisol/flisol-final.jpg">Santiago location</a> for installation assistance and a talk by Maurucio Bahamonde on KDE 3.4.  We offered <a href="http://www.kubuntu.org">Kubuntu</a> Live CDs to try out the desktop and the team offered help to install.



<!--break-->
<p>The event took place in the artistic "El sindicato" Culture Centre, which organiser <a href="http://www.cdsl.cl">CDSL</a> had got use of. Visitor numbers were higher than expected.</p>

<p>amaroK and KDevelop were the programs that caused most of the "woo!" sounds, and the whole desktop was very highly rated by the people who tested it on our demo machine.</p>

<p>KDE was represented by <a href="http://www.kde.cl">KDE Chile</a> members Matias Fernandez (Developer of KoolDock, Author of KGo! and KGoogleApplet), Mauricio Bahamonde (Author of Kopete text to speech plugin), Matias Valdenegro, Duncan Mac-Vicar (Kopete developer and official KDE representative in Chile) and Sebastian Sariego.</p>

<p>Enjoy the pictures of the event:</p>

<ul>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10020649gg.jpg">Duncan Mac-Vicar and Mauricio Bahamonde</a></li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10020658xh.jpg">KDE Chile team showing a KDE 3.4 desktop enabled PC</a> (really it was Duncan's laptop hidden under the desk, a 17" monitor and a usb keyboard/mouse).</li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10020813hf.jpg">People</a></li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10020974dr.jpg">Crowd, install fest and talks in parallel.</a></li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10020996ht.jpg">More people.</a></li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10021026mt.jpg">Mauricio's talk </a>about KDE 3.4 and the KDE project.</li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10021067kk.jpg">Matias</a> doing the job of pressing the space key when Mauricio raised his eyebrow  to mean 'next slide please'.</li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10021141ri.jpg">KDE Chile logo.</a> (designed by Sebastian Sariego, printed by Mauricio Bahamonde).</li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10021157hq.jpg">Logo of organisers Free Software Promotion Centre.</a></li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10021237tu.jpg">Mauricio installing Kubuntu</a> for a user.</li>
<li><a href="http://img87.exs.cx/my.php?loc=img87&image=10021386vt.jpg">kde.cl team</a>: Matias Valdenegro, Mauricio Bahamonde, Duncan Mac-Vicar and Matias Fernandez. Missing: Sebastian Sariego.</li>
<li><a href="http://img87.exs.cx/img87/1297/10021511ys.th.jpg">Sharing a beer</a> with gnome.cl people.</li>
</ul>

