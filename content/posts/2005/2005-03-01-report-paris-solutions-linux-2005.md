---
title: "Report of Paris Solutions Linux 2005"
date:    2005-03-01
authors:
  - "cmiramon"
slug:    report-paris-solutions-linux-2005
comments:
  - subject: "Links"
    date: 2005-03-01
    body: "Here is the french version :\nhttp://www.kde-france.org/article.php3?id_article=132\n\nHere are the pictures :\nhttp://www.kde-france.org/gallery/solutions_linux_2005"
    author: "gerard"
  - subject: "where is it?"
    date: 2005-03-01
    body: "I couldn't find any 299&#8364; PC with KDE on http://www.planete-saturn.fr/"
    author: "Pat"
  - subject: "Re: where is it?"
    date: 2005-03-02
    body: "They don't sell online. You have to go in their shops to find it. There is no Plan\u00e8te Saturn near my place, so I could not check."
    author: "Charles de Miramon"
  - subject: "looks like a fun time!"
    date: 2005-03-01
    body: "looks like it was enjoyable! i see David was flashing his patented smile about a lot there =) i'm sure it was as fun as it looks ... and if they need a home for that monitor they were using to show KDE on, i'm sure i could give it all the love it needs.\n\nand nice to see KDE in the other booths as well, such as Novell."
    author: "Aaron J. Seigo"
---
<A href="http://www.solutionslinux.fr/">Solutions Linux</A> trade show is the French annual rendez-vous of Free Software technologies and their commercial applications. This year, it ran from February 1st to February 3rd. Like preceding years, <A href="http://www.kde-france.org/">KDE-France</A> was present and benefited of a free booth in the "Associative Village". 











<!--break-->
<p>Located at the "desktop crossroad", we were surrounded by 
our friends of <A href="http://www.gnomefr.org/">GNOME</A>, <A href="http://fr.openoffice.org/">OpenOffice.org</A> and <A href="http://www.mozilla-europe.org/fr/">Mozilla</A>. The 
booth crew installed a computer equipped with a large 
screen and during three days, we demonstrated KDE 3.4 
(then in its beta days) which shined as a very stable 
Krokodil. A lot of happy and friendly users came to 
have a chat and a glance at the new features.</p>

<p>In the commercial section of Solutions Linus, many KDE 
desktops were on demonstration especially the corporate 
desktop solutions of <A href="http://www.novell.com/fr-fr/products/desktop/">Novell</A> and <A href="http://www.mandrakesoft.com/business/corporate-desktop">Mandrake</A>. 2004-2005 was 
also the first year when KDE entered modestly the 
French mainstream computer market. A French multimedia 
discounter, <A href="http://www.planete-saturn.fr/">Planète Saturn</A>, in offering in his outlets 
a KDE PC for 299 &#8364;. The French government has also 
launched a <A href="http://www.delegation.internet.gouv.fr/mipe/projet.htm">1 &#8364; / day notebook program</A>. A scheme to 
help students buy a notebook by giving some subsidies. 
One of the notebook on the list is running KDE.</p>

<p>On the KDE booth, around 15 developers, translators, 
promoters and passionate users took turns. Watch photos 
of us <A href="http://www.kde-france.org/gallery/solutions_linux_2005">here</A>.</p>

<p>KDE and <A href="http://www.kde-france.org/">KDE-France</A> are always looking for new volunteers. If you are French speaking and interested in developing, translating, promoting, creating digital art, organising activities, drop us a mail on our <a href="mailto:kde-francophone@mail.kde.org">list</a> and we will help you find your way in the KDE project. If you have no time, still consider <A href="http://www.kde-france.org/article.php3?id_article=91">enlisting</A> 
in the KDE-France association. Your money will be wisely spent.</p>
