---
title: "Open Letter to Alan Yates of Microsoft"
date:    2005-09-24
authors:
  - "iwallin"
slug:    open-letter-alan-yates-microsoft
comments:
  - subject: "Thank you, Inge.  I'm sure this will help."
    date: 2005-09-24
    body: "I am sure that Mr. Yates will be greatly reassured and uplifted by your letter.  Hopefully, this will assuage the horrible anxiety Microsoft has suffered and continues to suffer, in the wake of this OpenDocument announcement, for it's good friend... the honorable State of Massachusetts."
    author: "manyoso"
  - subject: "Re: Thank you, Inge.  I'm sure this will help."
    date: 2005-09-25
    body: "Watch out!  The Massholes will soon jump on you for mistaking their Commonwealth as a State."
    author: "James"
  - subject: "Re: Thank you, Inge.  I'm sure this will help."
    date: 2005-09-27
    body: "Yes Massachusetts is a Commonwealth. One of only four in the US. The others are Pennsylvania, Virginia, and Kentucky. Whether or not they will jump on this mistake is anther question...;)\n\n"
    author: "Brian"
  - subject: "It's Final - MA Goes With Open Document"
    date: 2005-09-24
    body: "As you may have heard, Massachusetts yesterday made its final decision to standardize on the OpenDocument format for all productivity applications. Check out http://www.groklaw.net/article.php?story=20050923142231938 for some comments."
    author: "Haakon Nilsen"
  - subject: "Reply  to Mr Yates"
    date: 2005-09-24
    body: "The approach you rebut here seems rather tired - haven't we all been here before? (e.g., http://www.theregister.co.uk/2002/05/19/ms_in_peruvian_opensource_nightmare/). \n\n Does anyone who isn't a fanboy react any differently to the way journalists did here? http://www.theregister.co.uk/2004/08/16/msoft_newham_10yr_deal/ \n\nIndeed, the penny is beginning to drop in ordinary society http://technology.guardian.co.uk/online/insideit/story/0,13270,1550922,00.html\n\n "
    author: "Gerry"
  - subject: "Re: Reply  to Mr Yates"
    date: 2005-09-24
    body: "\"The approach you rebut here seems rather tired - haven't we all been here before?\"\n\nNo, because if you'd actually read it he's talking about KOffice and the fact that it supports the Open Document format. It is not a derivative of Star Office.\n\nAnd when you can pay Cap Gemini and all sorts of other 'consultancies' a serious amount of money to go to these very impressionable local authorities and tell them the error of their ways, then they're going to buckle somewhat. Certainly in the UK, government agencies are very weak when it comes to listening to consultancies and large software companies. I've struggled to wonder how they came to use Star Office at all (and they weren't using it everywhere) in the first place.\n\nHowever, you can only go doing that for so long, and eventually, over the next ten years we'll feel the effects of Open Document and the efforts of those promoting it. Fifteen to twenty years ago or more, large companies charging a great deal of money for proprietary networking solutions and stacks was the norm. No one thought that, or those companies, would ever end. These days, if you promote a proprietary networking protocol over TCP/IP, or any IP standard protocol, you'll simply be laughed out of business. How many of those companies are still around or actually selling those products?"
    author: "Segedunum"
  - subject: "Opendocument"
    date: 2005-09-24
    body: "The intresting issue is: What will it take to convince Microsoft to support Opendocument?\n\nIf we attack on all levels and fora in lobbying it must be possible to convince politicians of Opendocument.\n\nBut: do we have an advocacy paper?"
    author: "gerd"
  - subject: "Re: Opendocument"
    date: 2005-09-24
    body: "But would we want MS to adopt Opendocument? Wouldn't they just make their version slightly incompatible and turn things into a mess?"
    author: "Berra"
  - subject: "Re: Opendocument"
    date: 2005-09-25
    body: "Well then we'd know wouldn't we? It would also quite clearly be Microsoft's fault that they are breaking something, rather than creating their own formats, expecting others to use them and and whinging \"Oh, but all these other office suites simply aren't compatible and are implementing our open standard wrong!\"\n\nMicrosoft sees this as rather dangerous territory, and it's clear that their knees are shaking quite a bit over how to deal with it."
    author: "Segedunum"
  - subject: "Re: Opendocument"
    date: 2005-09-25
    body: "> and it's clear that their knees are shaking quite a bit over how to deal with it.\n\nI sadly highly doubt it. to get their knees to shake youd have to place a 50mt nuke in Mr. Gates office, which is controlled by a embedded linux system (which means double danger). THAT would show them fear, nothing else.\nI guess they will just place a big \"loser\" stamp above Massachusetts on their big world-domination... ehm market-share map and have a good laugh.\nGetting Seattle out of lockin would probably rise their attention, but maybe not. They will just throw their hordes of zombies... ehm lawyers at whoever annoys them. finito."
    author: "Andy"
  - subject: "Re: Opendocument"
    date: 2005-09-25
    body: "... 50mt nuke ... embedded linux system\n\nembed a Microsoft system and see Bill really shake, rattle, and roll\n\nhttp://tepy-at-houston-lake.blogspot.com/"
    author: "tepy"
  - subject: "Re: Opendocument -- MS failed with Java"
    date: 2005-09-25
    body: "Microsoft had adopted Java and introduced \"extensions\" that only worked well for Windows. Sun brought MS to task and was forbidden to use the Java name, and had to pay a big fine. The response was the creation of C# and .Net, which is not doing well. The world is aware.\nIf MS now tries to corrupt the OpenDocument standard with extensions that work well only for Windows, there will be jurisprudence to support a similar prohibition. This time, the world public will clearly notice this fact."
    author: "Mario Miyojim"
  - subject: "German blog post"
    date: 2005-09-24
    body: "http://www.netzpolitik.org/2005/microsoft-ubt-kritik-an-opendocument/"
    author: "AC"
  - subject: "Letter to Massachusetts"
    date: 2005-09-24
    body: "Perhaps there should also be a letter to the fine people of Massachusetts congratulating them on their decision and letting them know all about KWord and letting them know of its Open Document support... perhaps someone over there could present them with a box full of Kubunty CD's ?\n"
    author: "Ivor Hewitt"
  - subject: "Re: Letter to Massachusetts"
    date: 2005-09-24
    body: "Who moved the 'y'. Obviously Kubuntu.... although Kubunty does have a nice ring to it."
    author: "Ivor Hewitt"
  - subject: "Textmak import OpenDocument also"
    date: 2005-09-24
    body: "The newest version of Textmake from Softmaker http://www.softmaker.de at least can import OpenDocument format and as far as I know they will fully support it in future versions. "
    author: "Anony Maus"
  - subject: "Re: Textmak import OpenDocument also"
    date: 2005-09-24
    body: "That's ***not*** what they told me.\n\n> I have some questions about the forthcoming version of Textmaker.\n>\n> [1] \"NEW: Imports OpenOffice.org (1.x and 2.0) and OpenDocument\n> documents\"\n>\n> What about EXPORT to same???? We need 2-way conversion for proper\n> document exchange.\n\n\"We wrote an import filter to read a lot of files. An export filter is a lot of\nwork, about the same work as an import filter. So at first we decided to write\nin import filter. ***We don't know if we will do an export filter in the future.***\" [emphasis mine]\n(20. Sep. 2005)"
    author: "Rex Bachmann"
  - subject: "It's a political issue..."
    date: 2005-09-24
    body: "This seems to be a political issue initiated from the Commonwealth of Masssachusetts probably to gain more freedom of selecting more than one software product for the future for governmental work and obligations. The right and only way in my opinion..."
    author: "Lochball"
  - subject: "Re: It's a political issue..."
    date: 2005-09-24
    body: "Hmm I'da never considered document retention and retrieval political."
    author: "stumbles"
  - subject: "Re: It's a political issue..."
    date: 2005-09-25
    body: "> Hmm I'da never considered document retention and retrieval political.\n\nIt's political to the extent that it's economic public policy, and therefore politicians are involved. Anytime significant amounts of money, or influence that may translate into money, are involved there will be politics."
    author: "don"
  - subject: "How about \"free speech\"?"
    date: 2005-09-26
    body: "It took the Peruvian official's letter to make this plain to me (and, I suspect, to a lot of people), but there's a serious matter of freedom involved here.  The recent embarassment of FEMA and the PTO over IE-only web pages underscored its importance as well:  the ability to communicate with government is incontestably an aspect of free speech - certainly more so than porn, or spam, or telemarketing or lots of other detestable things that have hidden behind the First Amendment.\n\nThe real, non-negotiable issue for a sovereign State (or Commonwealth, though to the rest of us they're a State, thank you very much) is that requiring citizens commmunicating with government to use a file format that is controlled by a single private party, whose use requires a license from that party, is unacceptable.\n\nThe reasons would be merely theoretical had not that very party itself provided ample proof that they not only could but would make unacceptable use of their advantage."
    author: "Brian Thomas"
  - subject: "MS will just support OpenDocument =)"
    date: 2005-09-24
    body: "That's an easy task for them.  Just one month worth of work.  They'll feature discriminate;  when selling to Mass, they offer that to them, when selling to everyone else, it would just be Office 12 format only.  Unless all of US and Europe decide to mandate this.  Then, hehee, Microsoft will be forced to support OpenDocument as a default standard.  =)\n\nAnother tactic Microsoft would take is to attack the inferiority of OpenDocument, that it didn't include advanced features in today's office productivity.  This may be their only strong point.  "
    author: "Zero"
  - subject: "Re: MS will just support OpenDocument =)"
    date: 2005-09-24
    body: "OpenDocument doesn't have inferiorities, it just doesn't have some features that can be easily adopted after they are approved and agreed to by its committee. OpenDocument is XML based and the \"X\" stands for eXtensible, meaning, it is flexible and can be enhanced at will and any features and capabilities that it doesn't have yet can be added very easily. So where are the inferiorities? MS Yates would like you to believe it is but in actuality, OpenDocument is very powerful."
    author: "abe"
  - subject: "Re: MS will just support OpenDocument =)"
    date: 2005-09-24
    body: "OpenDocument has a few problems, like formula's in SpreadSheets. But I think it is a good start, maybe in a version 2.0 of OpenDocument, they can solves such problems."
    author: "Boemer"
  - subject: "Re: MS will just support OpenDocument =)"
    date: 2005-09-25
    body: "Can you please be more specific? Someone from OASIS wrote that in 2004 too and they had to bang their heads a lot to solve it. But nobody spits out _what_ that problem actually was."
    author: "Andy"
  - subject: "Spreadsheat Formula: undefined"
    date: 2005-09-25
    body: "If I understand the issue right, the format and meaning of formulae in the spreadsheats is not defined in the standard. So products are left in the open, implementing some or other look-alike of the Excel's formula implementation. Portability? Interoperability? Scriptability? Forget it.\n\nTo keep ti short: they are undefined.\n\nBu I did not really go and checked with the standard text, so I might be completely wrong."
    author: "Anonymous Coward"
  - subject: "Re: MS will just support OpenDocument =)"
    date: 2005-09-25
    body: "I'm not familiar with the spec, but I think most opinions on the weaknesses for spreadsheets have come from this article:\n\nhttp://blogs.gnome.org/view/mortenw/2005/06/16/0\n\nIt appears that some fairly important things are left missing or underspecified by the spreadsheet specification."
    author: "Jess Sightler"
  - subject: "Re: MS will just support OpenDocument =)"
    date: 2005-09-26
    body: "points raised seem valid (though i haven't read the specification to verify them), but i don't get one thing - why didn't he give his input when od was created ?\n\nalso, i would really like to see a response and/or explanation to these points by od-tc member, anybode seen something like that ?"
    author: "richlv"
  - subject: "Re: MS will just support OpenDocument =)"
    date: 2005-09-26
    body: "Regarding the inferiority of OpenDocument, I'd like to point out that all versions of MS Word, past and present, also support reading and writing plain text files and rich text files (RTF).  MS doesn't have a problem writing to file formats that don't support all of Word's features.. they only have a problem writing to formats that are close enough that they threaten to loose the stanglehold of their proprietary document formats."
    author: "Pete"
  - subject: "heh"
    date: 2005-09-24
    body: "Marketing koffice to MS.. I hope his response will also be posted here  (if any)"
    author: "me"
  - subject: "The terrible injustice"
    date: 2005-09-24
    body: "I am also concerned by the fact that OpenDocument's only representative on Windows is OpenOffice. Together with microsoft, I find it terribly unjust that we are sort of forced to use the only one available fully-featured application for creation of the OpenDocument documents. \nI hope Microsoft will correct this terrible injustice by implementing OpenDocument support into its Office line."
    author: "Daniel"
  - subject: "Re: The terrible injustice"
    date: 2005-09-25
    body: "You know, everyone seems to forget there are OTHER Windows office products than MS Office.  WordPerfect for example.  All that needs to happen is these other native Windows office products support the OpenDocument format and there will be more than one native product doing so.  IBM could even get in there and tell their Lotus division to get the Lotus SmartSuite updated and support the format.\n\nIn addition, MS stating there's really only one codebase supporting OpenDocument is rather specious.  After all, MS Office is the only product which supports all the features of the Office document formats.  That would eliminate Office as a product, under Yate's argument.\n\nMark"
    author: "Mark"
  - subject: "Re: The terrible injustice"
    date: 2005-09-25
    body: "And Microsoft wanting to keep government as well as corporate entities locked into MSWord format is different,......... how?\n"
    author: "LaGrosse"
  - subject: "Re: The terrible injustice"
    date: 2005-09-26
    body: "You may want to read the post you replied to once again.  \nMaybe after reading: http://en.wikipedia.org/wiki/Irony\n\n"
    author: "cm"
  - subject: "Re: The terrible injustice"
    date: 2005-09-25
    body: "And Microsoft wanting to keep government as well as corporate entities locked into MSWord format is different,......... how?\n"
    author: "LaGrosse"
  - subject: "Re: The terrible injustice"
    date: 2005-09-28
    body: "Doesn't AbiWord (www.abiword.org) support the OpenDocument format?  It's avaiable for MS-Windows as well."
    author: "David"
  - subject: "Remember this?"
    date: 2005-09-24
    body: "http://dot.kde.org/1069632528/"
    author: "George Staikos"
  - subject: "Re: Remember this?"
    date: 2005-09-25
    body: "Goodness. Was it that long ago?"
    author: "Segedunum"
  - subject: "Moving to Windows??"
    date: 2005-09-25
    body: "First off, excellent letter.  Secondly, sorry if I'm a little behind the times here, but KOffice is moving to Windows?  Does this mean that it will lose most of it's KDE functions (DCOP, for instance) and be ported?  What about QT?"
    author: "Vlad Grigorescu"
  - subject: "Re: Moving to Windows??"
    date: 2005-09-25
    body: "The QT libs have been ported to windows and they are now under the gpl with version 4."
    author: "nemo3383"
  - subject: "Re: Moving to Windows??"
    date: 2005-09-25
    body: "I think the letter is referring to when KOffice (as well as the rest of KDE) will be ported to Qt4 and thus be legally portable to Windows."
    author: "Dhraakellian"
  - subject: "Re: Moving to Windows??"
    date: 2005-09-25
    body: "For a few years now people have been porting KDE/QT3 to Windows (at first to Cygwin then going for native), so that means when KDE is ported to QT4 the port to Windows shouldn't take to much more work."
    author: "Corbin"
  - subject: "Re: Moving to Windows??"
    date: 2005-09-25
    body: "Qt is available for Windows"
    author: "rinse"
  - subject: "What Ms is working on"
    date: 2005-09-25
    body: "some nice developer's insights of microsoft:\n\nOutlook Express to be renamed windows Mail, finally adds spam filter / Video\nhttp://channel9.msdn.com/showpost.aspx?postid=116711\n\nsharepoint\nhttp://www.sharepointblogs.com/dustin/archive/2005/09/14/3503.aspx\n\nScreenshots of Office 12\nhttp://pdc.xbetas.com/?page=o12preview1    "
    author: "gerd"
  - subject: "Re: What Ms is working on"
    date: 2005-09-25
    body: "> Screenshots of Office 12\n> http://pdc.xbetas.com/?page=o12preview1\n\no.k., \n\n1. this will definitively frighten nearly 100% of their \"normal\" users to hell.\n\n2. some IT admins in business may already try to estimate costs and (their own) time to explain the hordes of typists the new UI... (\"yes, baby, that button is now just at another position on the screen. Yes, I know it used to be up there, but now it's a bit more left... \")\nbtw.: did you notice the little cross on the top right corner of the tabs on the screenies? You can actually close the tabs! Yeah! This will be total horror for all admins around the globe as every typist will eventually press it \"Ayayay... my word vanished.. Dunno, what I've done, but now it looks so different, I'm sorry, I fucked it up...\"\n\n3. in the bigger companies they already book the \"retraining lessons\"... Well, we have the big wallet's over here and who cares anyway...\n\n4. Some geeks may simply think, \"fuck' em, they replaced menus with tabs, but so what, as long as it's somewhat usable\"\n\n5. On windows, after 3+ month everybody got used to it and will have to use both menus and tab interfaces for a transition periode of at least 2 years, as most windows software will change their UI to resemble MS's new \"invention\" (just to look more \"up to date\", ya know)\n\n6. my humble opinion: I don't think you'll save a second when typing a letter using tabs instead of menus. It all boils down to a different \"look\" using some more pixels for the UI.\n\n7. It's o.k. to give the UI more space on the screen (bigger icons, buttons, menu), because screen resolution is now better then 10 years ago. KDE's UI has been (moderatly) changing over the years and won't stop to do so in the future. I strongly \"plead\" for something like an \"extended Toolbar\" much like what we already have (configurability), plus resembling some of the functionality of MS new UI (but without the possibility to accidentally close a tab)"
    author: "Me"
  - subject: "Re: What Ms is working on"
    date: 2005-09-25
    body: ">1. this will definitively frighten nearly 100% of their \"normal\" users to hell.\n\nI don't know: it was said the same thing for the 'fisher price' default look of WinXP but I think that a majority of the home users are using the default (many enterprise configures it at classic mode though).\n\nOne could say it is because that they have no clue of how to do the change (or fear that it would 'break things'), still they managed to use the new look without too much fuss I think.\n\nSo, I don't expect users to have too much problem, only an even bigger reluctancy to switch, but that has been the case since Office97: further version have bring nearly nothing useful (except maybe the groupware features, but very few users use them).\n"
    author: "renox"
  - subject: "Re: What Ms is working on"
    date: 2005-09-28
    body: "I see that Sharepoint, Exchange and the BizTalk Server are going to grow together. At least, they will be highly complementary. And there is a need, too:\n\n- The companies will want email access through the web, not only through outlook.\n- They will want to have file data exchange.\n- They will want to have calendaring and ressource planning, possibly via the web... \n- They will want to have workflow planning that is tightly integrated in email communication. \n- They will possibly want to have a neat corporate website which is easy to constuct. Preferably as a modular portal solution, and Sharepoint will have to do that. \n\nBut nevertheless, I also see from colleagues who work for M$ that the giant is nervous. OpenSource is tackling M$ on the desktop now, and the community is constantly getting better. At the same time, users are reluctant to learn new applications all over again and feel well-served by existing software (with maintenance expiring). And a great number of Computing Science students have been brought up on Linux, not on Windows. \n\nIn this sense we should really start to work on applications and on the age-old problem of application integration. We will have to see what apps are running in the enterprises and how we can attach to them. The OpenSource world will have to become an application integration world. BTW, why don't we shed more light on OpenSource ERP and CRM software? This is a core reason for small companies to stay on Windows. "
    author: "Markus Heller"
  - subject: "Re: What Ms is working on"
    date: 2005-10-04
    body: "> The OpenSource world will have to become an application integration world.\n> BTW, why don't we shed more light on OpenSource ERP and CRM software? This\n> is a core reason for small companies to stay on Windows.\n\nI strongly second that! Some month ago, I already did a prototype of an ERP-solution with Gambas, which is planned to be tranformed into a real KDE-app. It could fit some 1-20 employees, but not more. As I have my own \"company\" (5 employees) I have some experience, but not enough time to work on it (to be honest I have no free time atm).\n"
    author: "Me"
  - subject: "Wikipedia"
    date: 2005-09-25
    body: "Great news. \n\nOpenDocument is a major opportunity for KDE. If you have some time available, consider writing a bit about it on Wikipedia in your mother language. We actually have :\n* http://en.wikipedia.org/wiki/OpenDocument (very complete)\n* http://fr.wikipedia.org/wiki/OpenDocument (complete, my humble contribution)\n* http://de.wikipedia.org/wiki/OpenDocument (not too bad, but need some updates)\n* http://pl.wikipedia.org/wiki/OpenDocument \n* http://th.wikipedia.org/wiki/OpenDocument \n* Nothing in other languages"
    author: "jmfayard"
  - subject: "Re: Wikipedia"
    date: 2005-09-25
    body: "In Spanish:\n\nhttp://es.wikipedia.org/wiki/OpenDocument"
    author: "TxemaFinwe"
  - subject: "Re: Wikipedia"
    date: 2005-09-26
    body: "I made a Wikipedia article of OpenDocument in Finnish.\n\n*http://fi.wikipedia.org/wiki/OpenDocument\n\nI tried to make it as useful as english version. I'll add more stuff when I have more time to do so."
    author: "Antti Aspinen"
  - subject: "To make a point"
    date: 2005-09-25
    body: "Just some clarification for the first poster.\nQuote: \"the honorable State of Massachusetts.\"\n\nMassachusetts is actually a Commonwealth.\n-C"
    author: "Chris"
  - subject: "Re: To make a point"
    date: 2005-09-26
    body: "I've always been curious about this. What exactly is the difference between the labeling \"State\" and \"Commonwealth\" exactly? Never got a clear precise defination.\n\nThx"
    author: "Johann Assam"
  - subject: "Re: To make a point"
    date: 2005-09-27
    body: "In the case of the four commonwealths in the US there does not seem to be much difference.  \nSee http://en.wikipedia.org/wiki/Commonwealth#United_States\n\n"
    author: "cm"
  - subject: "Re: To make a point"
    date: 2005-09-26
    body: "I've always been curious about this. What exactly is the difference between the labeling \"State\" and \"Commonwealth\" exactly? Never got a clear precise defination.\n\nThx"
    author: "Johann Assam"
  - subject: "koffice maybe ... "
    date: 2005-09-25
    body: "in practice there are not many if any OpenDocument applications that are out of beta that run natively on OSX or Windows. \n\nOpenOffice will be out soon-ish, but koffice still wont run natively on windows or MacOS. (where MS office does now).\n\nAlan might have a fleeting point as of right now.  "
    author: "scot"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-25
    body: "Not really, since he is using the new office format as base for his argument. And since MS Office 12 is not yet released, it's not available anywhere. Not like OpenDocument which is, in the way of public available betas of OpenOffice.   "
    author: "Morty"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-25
    body: "Yes Alan is evil, i fully concede that ;) but m$ has office 2003 that has an XML file format now across the entire suite, the next rev will have semi-open document format across both Mac and Windows. I dont see a day coming soon that there will be a competing suite that runs on both OSX and Win32. OpenOffice does not run natively on OSX. \n\nI am a Mac head and i would love something (anything ) to read all of the files that openOffice can output. "
    author: "scot"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-25
    body: ">I am a Mac head and i would love something (anything ) to read all of the files that openOffice can output.\n\nThen I really think it's time for you to start bugging Apple about it. They already use XML in their new office thingie, iWorks??. Or whatever it's called. So if you Mac heads get together and ask/demand support for it, it's really no reason for Apple not to:-)   "
    author: "Morty"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-26
    body: " OpenOffice 1.1.5 handles OpenDocument and has been out for a couple of weeks.  Just install Linux on your Mac and run OpenOffice.  See, it is simple."
    author: "Ralph"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-25
    body: "www.neooffice.org\n\nNeoOffice/J is OpenOffice in a java wrapper.\n\nNot completely native (depending on your definition of native) but very close.\n\n"
    author: "Lefty"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-25
    body: "neooffice is on the 1.1.4 code base, does not yet understand open document formats and they dont know when /if this will happen. \n\nAgain, making my point that maybe Alan has a point in that nothing uses this, on his perceived platforms (win32 and OSX) , anything that is not beta. \n\n"
    author: "scot"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-26
    body: "You keep repeating this, but nothing is using the \"open\" ms format either making your point void. That the current implementation of Office is able to use a XML format does not make any difference, the released versions of OpenOffice eg. 1.x series also uses XML."
    author: "Morty"
  - subject: "Re: koffice maybe ... "
    date: 2005-09-25
    body: "> in practice there are not many if any OpenDocument applications \n> that are out of beta that run natively on OSX or Windows.\n[...]\n> Alan might have a fleeting point as of right now.\n\nAlan mentioned KOffice himself and didn't bring up the fact that it is not available on Windows or the Mac (yet).  His point wasn't about operating systems but about support by different implementations, i.e. acceptance of the format by different projects / vendors. \n\n"
    author: "cm"
  - subject: "Codebase?  Who Cares?"
    date: 2005-09-25
    body: "Besides, what does having the same codebase have to do with it?  The worry is having the same company or organization control all the programs able to use Open Document.  If Star Office, Open Office, KOffice and IBM Workplace were all owned by one company, that would be something to worry about.  And even then, the Open Document format is still open.  Someone could still make a new program to read and write it."
    author: "Todd"
  - subject: "Re: Codebase?  Who Cares?"
    date: 2005-09-26
    body: "I agree.  That's an important point that has been overlooked in the whole discussion.  \n\nBut it's been so obvious that MS hasn't done their homework and wrong facts about KOffice have been spread, so it's not surprising that this open letter and the discussion are about their screwup and the correction of these wrong facts. \n\nBesides you can interpret Mr. Yates' argument as implicitely referring to the technical soundness of the OpenDocument format.  Then it *is* important to have more than one independent implementation as it demonstrates that OpenDocument is viable as a common format. \n\n\n"
    author: "cm"
  - subject: "Re: Codebase?  Who Cares?"
    date: 2005-09-26
    body: "Yes, and if you open the Help/About dialog on M$ Exploder, you see that even MS Internet Explorer (TM) uses the same codebase as NCSA Mosaic.\nOther sources (I don't know which, at the moment) have it that even Windows NT (TM) shares some of its codebase with BSD. Many W3C standard implementations (and I am sure there are MS implementations among them) share more or less code with some reference implementation. So what is Microsoft Corp. telling us other than it is driving another anti-Open-Source FUD-campaing at the moment?"
    author: "post-perpetruator"
  - subject: "Crap!"
    date: 2005-09-26
    body: "This is all crap... Koffice is the most what? Me cago de la risa! Come on! be serious!"
    author: "Gogo"
  - subject: "Re: Crap!"
    date: 2005-09-26
    body: "If you don't like it or not, it is the most COMPREHENSIVE office suite.\n\nI know this is marketing as I know the problems, but the statement is correct and not crap."
    author: "Philipp"
  - subject: "Re: Crap!"
    date: 2005-09-26
    body: "Pues cagate de la risa, porque KOFFICE ROCKS!!!"
    author: "Rubentm"
  - subject: "Re: Crap!"
    date: 2005-09-27
    body: "Here, have some toilet paper."
    author: "Anonymous"
  - subject: "Forget M$"
    date: 2005-09-27
    body: "I thought the letter was just fine.  I especially liked the line: koffice will likely run on windows as well.  Kind of like \"we're coming in your yard too, Billy boy!\"\nHonestly, I've been operating free of Microshaft software for years now; the only reason I ever use it is out of necessity- compatibility issues with a law office that uses Word Imperfect, which no longer runs on Linux.\nLet's face it: Microsoft and the open source community are exact opposites anymore.  Open source developers generally have one interest in mind: getting the product to a usable state and getting it out where folks can use it.  Microsoft has slightly more complicated motives: making a lot of money, selling advertising space on bundled PCs (you should have seen the load of ads that popped up the first time I booted this machine, before I FORMATTED the hard drive!) and policing and controlling at every turn how \"their\" software is sold, bought, used and even disposed of.  And if you don't do it right, watch out.  If this were a less free society, each person even accused of pirating Microsoft software might be thrown in the Lubyanka or something.  Honestly, I get sick of hearing the views of big corporate bullies on the state of open source.\nOne thing that I can actually imagine Microsoft developers having pioneered would be the rigid piracy countermeasures embedded in the software.  Heaven forbid that intellectual property should be shared as freely as speech, of which it is only really another form.\n(If anyone reading this works at any of the above mentioned establishments, equal commiserations in any case.)"
    author: "some guy"
  - subject: "M$ panicking just for OpenDocument, image KDE 4 "
    date: 2005-09-27
    body: "M$ is panicking and worried about OpenDocument, just image the release of KDE4 (before MS Vista) with KOffice that runs in Windows as well. Then averages users will wonder why in fews months why they are still using Windows with KDE4, instead of Linux."
    author: "NS"
  - subject: "Why not XML?"
    date: 2005-09-27
    body: "I just browsed through Yates' letter and it seemed Massachusetts had difficulties to decide between OpenDocument and XML format. Does anybody know what where the key arguments to turn down XML? \n\nNorm"
    author: "Norman"
  - subject: "Re: Why not XML?"
    date: 2005-09-27
    body: "I do know XML is *the* buzz word but damn, what do you think is the basemant for the OpenDocument format?"
    author: "Davide Ferrari"
  - subject: "Re: Why not XML?"
    date: 2005-09-27
    body: "The OpenDocument format is in fact XML that adheres to specified schema definitions, but pure XML is not suitable for binary data like e.g. bitmap images embedded in a text document, or audio and video clips that might come with a presentation document. An interchangeable file format, however, must account for such things.\nIn the beginning, there actually where discussions about using one big XML file per document with binary data embedded as BASE64-encoded CDATA sections, but this approach was dropped in favour of a different solution: The OpenDocument definition consists of a set of XML schema definitions AND a definition of a directory tree that contains the various individual, but related (and possibly linked to each other), files that can make up an office document.\nIn addition, OpenDocument also specifies how this structure is to be delivered, more precisely, it specifies that this directory structure be packed into a zip file which then is the final document file. In addition to the user data, e.g. the typed text and images of a word processor document, each OpenDocument file contains metadata that describes the user data to processing applications and ensures proper interpretation, display, and editing of the contents."
    author: "post-perpetruator"
  - subject: "Re: Why not XML?"
    date: 2005-09-27
    body: "It was not XML, it was MS XML ;)\n"
    author: "Saren"
  - subject: "Yes, you're right"
    date: 2006-01-22
    body: "every body nows that Microsoft isn't right. you are right not him. he is crazy,\nstupid etc.:))"
    author: "9 Rich Guy"
---
In his reply to the <a href="http://www.mass.gov/Aitd/">Massachusetts decision</a> to use only documents in <a href="http://en.wikipedia.org/wiki/OpenDocument">OpenDocument</a> format, the Microsoft manager Alan Yates <a href="http://www.mass.gov/Aitd/docs/policies_standards/etrm3dot5/responses/microsoft.pdf">writes</a>:
(paraphrased) <i>Star Office, Open Office, KOffice and IBM Workplace are all derivatives of the same codebase. Thus there is only one program that supports Open Document, and that is illegal.</i>
This is, of course, not true, and here is an open letter written by KOffice Marketing Coordinator Inge Wallin on behalf of the KOffice team which clarifies these facts.




<!--break-->
<p>Open Letter to</p>

<p>Alan Yates<br />
General Manager<br />
Microsoft Corporation</p>


<p>Dear Mr Yates,</p>

<p>
It is with great interest that I have followed the debate that started
with the Massachusetts decision to
only exchange data with other parties in an open format, namely Open Document.
I must say that personally I find the reasoning behind the decision to
be sound, but I fully support your right to disagree with this
sentiment.
</p>
<p>
In your rather long, and doubtlessly well researched, <a
href="http://www.mass.gov/Aitd/docs/policies_standards/etrm3dot5/responses/microsoft.pdf">reply</a>
to the declaration, you make many points which I will not address
here, since <a
href="http://business.newsforge.com/article.pl?sid=05/09/21/0811222">others</a>,
better suited than me, have <a
href="http://www.tbray.org/ongoing/When/200x/2005/09/10/Mass-Opposition">already</a>
<a
href="http://www-128.ibm.com/developerworks/blogs/dw_blog.jspa?blog=384&ca=drs-bl">done
so</a>.  There is, however, one point where I feel that you have been
gravely misinformed by your research staff.
</p>

<p>
That point is the following.  On page 7, and continuing on page 8 you
write: 
<blockquote>
  <i>The draft policy identifies four products that support the
  OpenDocument format: Sun's StarOffice, OpenOffice.org, KOffice, and
  IBM Workplace.  In reality, these products are slight variations of
  the same StarOffice code base, which Sun acquired from a German
  company in 1999. The different names are little more than unique
  brands applied by the vendors to the various flavors of the code
  base that they have developed.  In essence, a commitment to the
  OpenDocument format is a commitment to a single product or
  technology.  This approach to product selection by policy violates
  well-accepted public procurement norms.</i>
</blockquote>
</p>

<p>
I understand your worries, but fortunately I am able to put your mind
to rest: <a href="http://www.koffice.org/">KOffice</a> is in fact
<b>not</b> related to StarOffice or OpenOffice.  It is a completely
separate product, and a very fine one at that.  One of our team
members, David Faure, was an active party in the creation of the OASIS
OpenDocument standard, and KOffice was the first office suite that
publicly announced support for it.
</p>

<p>
Just to add a bit to your knowledge of KOffice, I would like to
mention a few points:
</p>
<ul>

<li>KOffice is the <b>most comprehensive</b> of all office suites in
existence, comprising no less than 11 different components in one
well-integrated package.</li>

<li>These components include <b>core office applications</b> like KWord,
KSpread and KPresenter, but also <b>creativity applications</b> like Krita
(an advanced pixel based drawing tool), Kivio (flowcharts), Karbon
(vector based drawing) and Kexi, an integrated environment for
<b>database applications</b> not unlike to your own Access.</li>

<li>KOffice is very well <b>integrated into KDE, the multiple award
winning desktop environment on Linux</b>, Solaris and other UNIX
variants.</li>

<li>KOffice is <b>fully network-transparent</b> and all components can send
documents as mail, print to PDF files and store and load documents
from countless different network servers.</li>

<li>Last, but not least: <b>Within a year, KOffice will likely run on
Windows</b> as well.

</ul>

<p>
In case you think that even two competing products will not be enough
to satisfy the "<i>well-accepted public procurement norms</i>", I can assure
you that they will soon not be alone.  The fine word processor
AbiWord and the spreadsheet program Gnumeric, <a
href="http://www.mail-archive.com/gnumeric-list%40gnome.org/msg00322.html">will
also soon support Open Document</a> due to an independent effort by a Nokia research lab.
</p>

<p>
I am sure that you are now much calmer, and if you want to know more,
you can always go to the <a href="http://www.koffice.org/">KOffice
website</a>.  You can also write to the <a
href="https://mail.kde.org/mailman/listinfo/koffice-devel">KOffice mailing list</a> and ask your
questions there.
</p>

<p>
Respectfully,
</p>

<p>
Inge Wallin<br />
Marketing Coordinator<br />
On behalf of the KOffice Team
</p>




