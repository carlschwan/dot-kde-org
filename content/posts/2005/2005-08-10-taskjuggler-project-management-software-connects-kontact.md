---
title: "TaskJuggler Project Management Software Connects to Kontact"
date:    2005-08-10
authors:
  - "cschlaeger"
slug:    taskjuggler-project-management-software-connects-kontact
comments:
  - subject: "TaskJuggler & KPlato?"
    date: 2005-08-10
    body: "Sorry if my question is a bit stupid, but what's the difference between TaskJuggler and KPlato? I only know that both are project management software."
    author: "fyanardi"
  - subject: "Re: TaskJuggler & KPlato?"
    date: 2005-08-10
    body: "Kplato should be part of Koffice suite with better integration. But it seems that nobody had time to work on it."
    author: "JC"
  - subject: "Re: TaskJuggler & KPlato?"
    date: 2005-08-10
    body: "TaskJuggler is separated into backend and frontend (with the latter being a comparably new addition). KPlato has no such separation afaik."
    author: "ac"
  - subject: "Re: TaskJuggler & KPlato?"
    date: 2005-08-11
    body: "now they need a gnome and/or gtk front-end, and a web front-end. IMHO, it is useful to have a common back-end being shared."
    author: "a.c."
  - subject: "Wow!  Looks really good..."
    date: 2005-08-10
    body: "Wow!  Looks really good... I'll have to try this out for my own taskjuggling needs ;)"
    author: "manyoso"
---
Even though the <a href="http://www.taskjuggler.org/">TaskJuggler</a> Team considers the <a href="http://www.taskjuggler.org/FUDforum2/index.php?t=msg&th=2602&start=0&S=d6a908fdcee905acb02daa146d195a79">just released 2.1.1 version</a> of their Project Management software mostly a bug fix release, this release is a major milestone with respect to KDE integration. But before I dive into the details, let me give you some background on the TaskJuggler project.


<!--break-->
<p>The Project was started in 2001 by some <a href="http://www.novell.com/linux/suse/">SUSE</a> developers and project managers when they were faced with logistical challenges in their development processes due to a growing number of products and involved parties. During their evaluation of existing project management solutions they did not find anything that met their feature needs, their budget and their OS requirements. So the TaskJuggler project was founded and only 4 months later the first version was officially released under the GNU GPL.</p>

<p>TaskJuggler revolutionizes the genre of project management tools by using a textual description of the project breakdown, the resources and their interdependencies. It also focuses on optimizing the resource allocation rather than just being a graphical Gantt chart editor. Early version started as a pure commandline software but a <a href="http://www.taskjuggler.org/ide.php">full featured KDE front-end</a> (<a href="http://www.taskjuggler.org/screenshots_ide.php">screenshots</a>) is now available as well.</p>

<p>Over time the software became more and more sophisticated and TaskJuggler is now the most advanced Open Source project management software available. It contains many features like multi scenario comparison, time zone support, user definable data fields, risk management, shift support and multi-user/multi-project support that put it en-par with high-end closed source project management solutions.</p>

<p>It has made its way into many Linux distributions and has been adopted and used by many companies already. Some universities even use TaskJuggler for their Project Management classes.</p>

<p>The just released 2.1.1 release adds support to generate iCalendar files. This allows the project manager to generate individual or team schedules and distribute them to all involved parties for automatic integration into their calendars. The <a href="http://korganizer.kde.org/">KOrganizer</a> part of <a href="http://kontact.org/">Kontact</a> will read the generated iCalendar file and merge it with the existing dates in the calendar and TODO views. KOrganizer can also be started directly from the TaskJuggler user interface.</p>


