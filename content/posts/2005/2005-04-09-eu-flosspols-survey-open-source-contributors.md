---
title: "EU FLOSSPOLS Survey of  Open Source Contributors"
date:    2005-04-09
authors:
  - "taj"
slug:    eu-flosspols-survey-open-source-contributors
---
The EU-funded <a href="http://flosspols.org/">FLOSSPOLS project</a> is carrying out a survey of participants worldwide. This is a follow-up to the <a href="http://flossproject.org/report/">original</a> <a href="http://en.wikipedia.org/wiki/FLOSS">FLOSS</a> survey in 2002, one of the first and most comprehensive surveys of developers - who they are, how they work and why they do it. The <a href="http://flosspols.org/survey/survey_part.php?groupid=skd">new survey</a> aims to provide an update, include new developers and other participants, and answer some of the questions that were raised by the first one: in particular, on the role of developer communities for learning skills and employment generation, and on the low level of participation by women.


<!--break-->
