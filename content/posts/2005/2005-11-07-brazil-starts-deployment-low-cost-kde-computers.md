---
title: "Brazil Starts Deployment of Low Cost KDE Computers"
date:    2005-11-07
authors:
  - "hcastro"
slug:    brazil-starts-deployment-low-cost-kde-computers
comments:
  - subject: "1st post!"
    date: 2005-11-07
    body: "And from a brazilian!\n\nHope things really get done right this time."
    author: "Cl\u00e1udio Pinheiro"
  - subject: "Re: 1st post!"
    date: 2005-11-07
    body: "Yeah... i still don't feel ok since the 'pc conectado' thing."
    author: "Tomaz Canabrava"
  - subject: "Re: 1st post!"
    date: 2005-11-07
    body: "What happened to it?"
    author: "Roland"
  - subject: "Re: 1st post!"
    date: 2005-11-07
    body: "How much is that?"
    author: "AC"
  - subject: "Re: 1st post!"
    date: 2005-11-07
    body: "(Figures in USD)\nThe agreed price limit is less than $500. There are some tax benefits for PCs under ~ $1000, as I recall.\n\nBut the market already has $400 entry-level offers. For that you get a local Linux distro or Windows Starter Edition (a very limited version) on a common (e.g., Sempron 2200, 128M RAM, 40 GB HD, 15\" monitor.\n\nIt is still very expensive for poor families. As the article states, only the long (24 months) government loan will make it affordable . Of course, despite the relatively low interest rates, it will become a lot more expensive, but that's the sad way it is over here...\n\nPC Conectado (Connected PC) was an initiative to get ISPs to accept low cost accounts (like $3/mo) for 7,5 hours per week (or something to that effect). This hasn't worked very well. It's uninteresting to vendor and buyer. But we have free ISPs over here: you just pay for the telephone line usage (which is _very_ cheap late at night). I hope someone creates a low fare for the poor, otherwise many people will sleep very late. This is kinda stupid IMO.\n\nThat's it. The Linux version is quite common, some are Red Hat based, some Slackware based and others are Kurumin- (really Knoppix-) based. Openoffice.org etc. etc. Pretty stable stuff.\n\nLet's see how this unfolds. Enter the media now: they can promote it or kill it.\n"
    author: "Gr8teful"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "As a Portuguese, I'd like to give my 2 cents to my Brazilean pals about what I feel you should be doing:\n\nInstead of trying to fix things from up, start from down and go upper. Don't sell guns to everyone, take the land from the rich people and give it to the \"landless\" people (\"os sem-terra\") and, for god sake, deliver them birth control methods for free, create an attractive wellfare system for unemployed people in the cities so that they don't become drug sellers and women don't emmigrate to Portugal to become prostitutes, raise the taxes exponencial so that the richs pay a lot and the poor nothing, don't feed soccer teams with public money, and don't kill the damn Amazonia (it's the lung of the world)!\n\nNot being able to go out at night, authentic wars in cities, mass robery, having cops dropping death in their houses, having people protecting cars and houses with war technology like steel and glass gun-proof, etc, etc are the problems that you need to be fixing!\n\nShit, invest in stuff that matter. Having computers in every home doesn't matter. Studies are all pointing in the direction that computers are actually harmfull for education. Notice that I'm not saying not to invest in hardware or software industry, just that there is no need for giving computers to poor people. START BY GIVING THEM FOOD, CLOTHES AND A JOB."
    author: "Pedro Oliveira"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "OH MY GOD why did you not say this earlier!\n"
    author: "Reply"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "I agree. This is SOOOOO on topic!"
    author: "James"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "\"Don't sell guns to everyone\"\n\nPedro don't you know? Politicians prefer unarmed peasants."
    author: "Sun"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "Haha. It seems we have people posting from the 17th century."
    author: "Robert"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "Oh right, I forgot. In the 21st century well educated people know that only the military can be trusted with guns."
    author: "Sun"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "Are you American? it's funny how all of them think they need guns at home in case the gov turns evil and the brave people of America would need to defend itself against that nevercoming threat. Meanwhile, guns are only used to kill each others, amongst the poorest, youngest and innocents in most case. This is why the US gov is so pro guns (besides the lobby stuff), is that guns allow to have less poor people in the country, half of them gets killed by guns shot and the other half goes to jail for shooting. I'm amazed how American people are so naive to believe their gov is pro guns because of their benevolent \"let's give the power to the people in case we politician turn evil\" thing, come on! stop believing every thing your gov tells you!"
    author: "Patcito"
  - subject: "Re: 1st post!"
    date: 2005-11-10
    body: "Germany in the 19e0's was a modern democratic state. How much different would 1940s Germany have been if jews had owned guns? They probably wouldn't have stopped the Nazis, but it would have slowed them down. It would have allowed a resistance movement to enable an earlier allied victory.\n\nSometimes the politicians do turn evil."
    author: "Brandybuck"
  - subject: "Re: 1st post!"
    date: 2005-11-11
    body: "Actually there are enormous numbers of people using guns to defend their homes.   The number of robberies and attacks on homes defended by guns is far lower than on unarmed homes."
    author: "TomL"
  - subject: "Re: 1st post!"
    date: 2005-11-10
    body: "\nSun, the 18th. century called, wants its fantasies back.\n\n   \"Oh right, I forgot. In the 21st century well educated people know that only the military can be trusted with guns\"\n\nNo, in the 21st. century anybody except some morons know that if push comes to shove and you have to fight your own army, you'll need something heavier that handguns and hunting rifles. \n\nBelieve me, in my country we had such a fight not so many years ago, and the army won. Very easily. \n\nCheers,"
    author: "Carlos Cesar"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "First I feared you were going to be patronizing, which was bad.\n\nThen i discovered that, actually, you were going to be offensive, which is worse. After that you just went downhill and, fortunately, sounded silly and uninformed.\n\nAs a Portuguese all I can say is: Thank God, you do not speak for all (or even a significant part) of the portuguese people.\n\nRegards,\nLuis\n\nPS: That program is quite interesting. Spreading open source software to the population will increase the exposure to that kind of software. Might help with it's acceptance rate, instead of maintaining the hold Microsoft has on the computer industry. Although, 500 USD is still quite expensive."
    author: "Luis"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "What can I say? Your arguments have shot mine to the ground.\n\n> As a Portuguese all I can say is: Thank God, you do\n> not speak for all (or even a significant part) of the\n> portuguese people.\n\nOf course not. Portuguese people want Brazil to stay an underdeveloped country to have some cheap vacations and to get some cheap whores."
    author: "Pedro Oliveira"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "UHUM!!!\n\nVery nice who you posted this... and as a brazilian, i have the ideal answer to you criticism:\n\n1 - Most of brazilian illegal imigrants yet prefer USA insead of Portugal. OW! i almost forgoten! A pletora of portuguese people lives on Brazil as aliens because they failed to get good employment on Portugal. About migration rates of Brazil, is bare minimum is you put side by side with Mexico, Argentina, China, India and nations of north africa.\n\n2 - Most of portuguese citizens eat your cultural trash, ear brazilian music, see brazilian shows... know brazilian artists and writers... and brazilians about portuguese culture?! The only portuguese who i ever sawed so far is the nobel winner Jos\u00e9 Saramago. You country are being assimilated by brazilians. And you know this is a recend problem reported by some extreme right politicians in Portugal.\n\n3 - Almost all brazilians agree about violence being overhyped by media. Small and medium cities, who most brazilians lives on, have very low criminality levels. Rio de Janeiro is a special case because they work as a bridge by international drug dealers (mostly Colombians) to Europe and USA.\n\n4 - People becoming drug dealers are mostly medium class playboys with a small brain beleaving on impunity. Poor people who lives on sluns are just the shield of druglords or just that: honest poor people.\n\n5 - The IBGE have several statistics about the origin of violence on Brazil... you are Portuguese!! go to IBGE site, download the data and read it. The biggest cause of violence in Brazil is passional crimes, fight between buns inside some bar... and goes on... not robbery, gang fight or policial violence. Go to http://www.ibge.gov.br/\n\n6 - Amazonia is just resources... and resources need to be used if demanded. We are trying to exploit it on a sustentable manner, but yet we failed on this for now. O.o Take a look on http://www.sivam.gov.br/\nWe also created a extensive satelitte system to monitory other places of Brasil with have protected by law biodiversity.\n\n7 - We do not are so poor, in fact the present installed computer base in Brazil by CIA Worldfact Book is 14 millions (and this is outdated 2003 data!)... this is more than the whole Portugal population. And dial-up internet is dying fast... a 8mbps DSL connection is sufficient cheap for any medium class family afford. Do you want a proof?! Look these two broadband service provider http://www.speedy.com.br http://www.virtua.com.br/\nAnd also we have low cost 500kbps plans.\n\n8 - Most of brazilians are medium class, and the biggest brazilian problem is for now obesity, not hunger. In a country with 184 millions of citizens, with a not so powerful economy, the fact we have only 25 millions on poverty, is almost a miracle! Look some data about China, Russia and India.\n\n9 - The landless moviment (called MST) is political, in fact their leaders are acused several times by the midia and international ONGs for bad faith againts their supporters, like charging some \"fee\" from the \"poor people\" who want to participate, and if you refuse to pay, you lost your land or have your life threatened. And they frequently refuse to talk with the governament to find a solution, because a solution is a bad thing for their \"bussiness\".\n\n10 - Birth control methods are already free and well divulged, thanks. You can get a condom for free on any public hospital, or require to SUS (the administration of public healthcare system) to schedule a surgey for sterilization. The only thing who is forbbiden is abortion. Go to \nhttp://portal.saude.gov.br/saude/\n\n11 - The wellfare system do not have a easy solution, the ratio between retired and working class are too big, this make the system work with their finances on red. Since money do not grow on trees, we need to find something to pay. And this is a mundial problem, due the increase of live expectation. A proposed solution is to ban (or reduce) the public wellfare system, and let the banks run a private system. Search on google about \"Reforma da CLT\" (CLT is Consolida\u00e7\u00e3o das leis trabalhistas)\nIf you want to know more about ths system current working here, go to: http://www.mte.gov.br/\n\n12 - Weapons selling are heavily regulated by state. And the national weapons industry mostly work on high-end war equipament used exclusively by military. Ordinary people are also forbidden to carry weapons on any public space. If you buy a weapon, it is to stay on your home. Go to http://www.mj.gov.br/seguranca/desarmamento.htm"
    author: "Felipe Bugno"
  - subject: "Re: 1st post!"
    date: 2005-11-08
    body: "<i>Most of brazilians are medium class</i>\n\nOk, gimme a brake.. c'mon..."
    author: "XjjaX"
  - subject: "Re: 1st post!"
    date: 2005-11-09
    body: "In every country most of the people are medium class....\n"
    author: "Dm"
  - subject: "Re: 1st post!"
    date: 2005-11-09
    body: "(I'm a brazilian)\nI agree with 1 2 5 7 8 10 \n"
    author: "Dm"
  - subject: "Re: 1st post!"
    date: 2007-05-30
    body: "First of all your English is sub-standard grade, learn to speak & write the language first before you spout this load of Bull****.\n1) I am in the unfortunate position of having been born in this rubbish dump called Brazil.\n2) Brazil has no culture of its own for a start, there are more slum dwellers and \"untermensche\" than anywhere else in the world.\n3) Brazilians are arrogant, filthy and in general dishonest, the country is run by a drunk moron that was voted into power by idiots.\n\nBefore you run any European country down pick up that trash that you are so proud of out of the gutter first.\n\nBest of all get educated, learn about the world then open your mindless \"trap\"\n"
    author: "Jurgis"
  - subject: "Re: 1st post!"
    date: 2005-11-09
    body: "They aren't giving  computers to poor people...\nThey are selling to \"not so poor people\""
    author: "Dm"
  - subject: "This is true"
    date: 2005-11-07
    body: "Eve on the city I live (Santa Maria RS) that is just e medium to small town, there are already computers on shops running some strange and new linux distros that are being created to match the minimum requiriements of the pc for all program.\nIt's very strange to see linux running on machines in a shop, you know, I never tought really this day would come so fast. I just hope people acctually use it instead of installing a pirate windows copy."
    author: "Iuri Fiedoruk"
  - subject: "Clearing up"
    date: 2005-11-08
    body: "Just to make it clear, any company can join the program and sell computers with the tax and loan benefits. Regarding software, the rule is that the computer has to ship with free software, so in theory a company can use GNU/Linux, a free BSD, etc, and any desktop environment.\n"
    author: "Reply"
  - subject: "reinstalled"
    date: 2005-11-08
    body: "Just wondering...\n\nHow many of these computers will be reinstalled with an illegal version of Windows XP as soon as they are sold? I am not convinced that pushing linux/KDE systems because it is a cheap solution is the way to promote our desktop. I would want users to choose KDE, not be forced to use it. Any problems will, I think, immediatly reflect back on KDE (that's the face of the computer they see, right?)."
    author: "Andre Somers"
  - subject: "Re: reinstalled"
    date: 2005-11-08
    body: "\" I would want users to choose KDE, not be forced to use it\"\n\nand  I would like users to chose Windows,not be forced to use it!"
    author: "Patcito"
  - subject: "Re: reinstalled"
    date: 2005-11-08
    body: "Thank you for exposing the parent poster fallacies, which amount to the following:\n\n\"I would like the user to choose kde, let's force-feed them windows and then leave them to their own devices if they want a computer that runs free software\"."
    author: "Gonzalo"
  - subject: "Re: reinstalled"
    date: 2005-11-09
    body: "I guess that's a fair point."
    author: "Andre Somers"
  - subject: "Useless, inane and a bad idea"
    date: 2005-11-09
    body: "Knowing Brazil (and Portugal), I can say this is just another feed-the-fat-cats program. A few government well-connected people will get the benefits (perhaps they will even give away the computers to themselves) and nothing will come out of it except waste of money.\n\nNot to speak of the obvious problem of a government dumping goods on the economy and taking the place of legitimate private enterprise. Do you think that there are really urgent reasons why Brazil's poor need computers so the government has to step in, to balance some kind of grievous social inequality?"
    author: "el_chato"
  - subject: "Just a small step guys... don't throw stones on us"
    date: 2005-11-10
    body: "Brazilian's people problems and facts are for them to slove, as any people of any country.\n\nAs a 100% genuine BraSilian(Paranaense e p\u00e9 vermeio! Yeah!!! \\o/) I can say this: we gave good cientists, students and professinals of computer related science, so, WHY not giving the CHANCE... CONDITIONS... for these hidden people to contribute? Yes, we have so many hardships, but we handle them! We fight! We search CONDITIONS to make this possible! So, giving the CONDITIONS, not the candy, for the people to get they resources, we will grant them the RIGHT to evolve, contribute with CHOICES.\n\nFinalizing what i am saying... BraSil rulez. We, BraSilian people rulez. As any other citizen of EARTH rulez. And, the most important, Free and OpenSource Developers RULEZ! \\o/ do not matter of what country! why? because we are CITIZENS OF EARTH MAN! The OpenSource EARTH! ;)\n\n_______________________________________________________\nCountries? Not needed. Just respect... for starters! ;)\nFinal Strike - Still recycling his c/c++ skills and studing KDE's apps and docs\nusing Slackware and Debian \\o/"
    author: "Final Strike"
  - subject: "paz e amor... peace and love"
    date: 2005-11-10
    body: "Vamos parar de discutir em ingl\u00eas? \u00e9 rid\u00edculo!\nN\u00e3o tem sentido, n\u00e3o vamos mudar nada, n\u00e3o vamos perder nosso tempo.\nJ\u00e1 perdi o meu lendo e escrevendo isso e n\u00e3o quero que mais algu\u00e9m perca seu tempo por aqui. V\u00e1 ler um livro, plantar batatas ou trabalhar.\nAcabou. X\u00f4.\n\n\n\n\n\n\n\n\nthis is the end.\n\n\n\n\n\n\ni mean it.\n\n\n\n\n\n\n\n\n\n\n\n\n\n\ntchau!\n\n\n\n\n\n\n\nchega!"
    author: "Cidad\u00e3o Quem"
  - subject: "Re: paz e amor... peace and love"
    date: 2005-11-10
    body: "Lindo ......\n"
    author: "Amilcar"
  - subject: "Portugu\u00eas p/ Brasileiros/Portuguese for brasilian"
    date: 2005-11-11
    body: "Ohhhh Yessss\n\n\u00e9 bom, o som do portugueiiissss"
    author: "alex brasileiro"
  - subject: "2 cents from Portugal"
    date: 2005-11-21
    body: "just wanted to say i'm absolutly shocked by what our brazilian \"friend\" has to say about the portuguese.\nit is easy to see just by reading his enraged charges that this person is not properly educated, by the english used and by the spelling presented, how can you write about a people( the portuguse) putting them down as low- lives, saying your people would rather emigrate some were else because portugal isn\u00b4t good enough for you smater and hoolier that thou brazilians when it is you that in fact cant find means of life without turning to us.\nby the way, i work in a respectable hotel and every single night i have to turn away brazilian whores that knock on our door looking for clients or just for a few minutes out of the rain-- wouldn\u00b4t mind if they would have gone to the US instead, and all the decent portuguse women that have caught dirty deseases because of them wouldn\u00b4t mind eather. note i\u00b4m just saying if we take advantage of you it is because you do the same and as the saying goes\" quem est\u00e1 mal que se mude e ... para bem longe.\""
    author: "patricia"
  - subject: "Re: 2 cents from Portugal"
    date: 2005-11-21
    body: "I'm amazed you would complain about his spelling and make conclusions about his missing education, see \"portuguse\", \"some were\", \"smater\", \"hoolier\", \"cant\", \"deseases\", \"eather\".\n\nNice own goal there.  \n\nAnyway, IMHO nothing of this belongs on dot.kde.org, please stop it, both sides.\n\n"
    author: "cm"
  - subject: "hiltonrio@aol.com"
    date: 2007-02-28
    body: "I found funny, if not abolute ignorance, the absolute misunderstand of the economic Brazilian, I live and work in NYC and we do have obviously investments in Brazil.\n\nJust 2 datas that would impress\n\n1- The size of the medium class in Brazil is the size of the whole country of Germany\n2- China, in proportionally has more slums than Brazil, 38% in China (metropolitan areas) vs 36% in Brazi.\n\nGo figure why Brazil has got such a bad marketing ..."
    author: "Hilton De Paoli"
---
A new inititive by the Brazillian government will see low cost KDE based computers on sale throughout the country from next week.  The <a href="http://www.brazzilmag.com/index.php?option=com_content&task=view&id=4170&Itemid=49">Computers for All</a> scheme will bring "cheap and accessible" computers following a recent law cutting taxes and encouraging affordable financing for low income buyers of computers preinstalled with Free Software operating systems.  Several companies are involved in the scheme with most using KDE desktops.  The Ministry of Science hopes for half a million machines to be sold in the next 4 to 6 months.




<!--break-->
<p>Brazilian company <a href="http://www.positivo.com.br">Positivo</a> will start to deploy 2,500 machines a month next week installed with an OEM KDE desktop based on <a href="http://www.mandriva.com">Mandriva</a>.  They expect to reach 10,000 sales per month by the end of this year.</p>

<p>"With this partnership with Mandriva Conectiva we keep our philosophy to offer home users easy to use products created and represented by companies with credibility and unquestionable reliability.", says Hélio Rotenberg, director of Positivo Informática.</p>

<p>The company has sold 10,000 computers in the last few months with KDE based distribution <a href="http://www.insignesoftware.com/default.php">Insigne</a>, before changing to Mandriva.</p>





