---
title: "Observations from KDE Trunk and Qt 4"
date:    2005-06-09
authors:
  - "mproud"
slug:    observations-kde-trunk-and-qt-4
comments:
  - subject: "KDE leads the future in Desktop Environments"
    date: 2005-06-09
    body: "I am compiling KDE svn trunk on a nightly basis with my own scripts and it really rocks. I am here to thank everyone from the KDE team (and the cool people from #kde-devel) for their carefully work on keeping svn trunk always in shape. There are rarely any issues with svn trunk and it usually compiles over night without any issues. Rarely you need to do some manual work to have it finish the compile and the committs to svn trunk are usually tested and work. Thanks to the people for having created a nice, round and feature complete Desktop Environment for the Open Source world to get serious business, science and normal work done. Stuff that feel mature, complete, consistent and truly integrated."
    author: "A. A."
  - subject: "Re: KDE leads the future in Desktop Environments"
    date: 2005-06-09
    body: "You should give kdesvn-build, located in kdesdk/scripts, a try, it can't do everything yet (l10n doesn't quite work). But, all in all, it's a great script. And I'm not just saying that because i wrote the code to make it do apidox.\n\nLevi\nhttp://www.freepgs.com/levidurham/"
    author: "Levi Durham"
  - subject: "Re: KDE leads the future in Desktop Environments"
    date: 2005-06-09
    body: "I know about it but still prefer using my own ones. They are also available for other people to use as alternative."
    author: "A. A."
  - subject: "decoration and theme"
    date: 2005-06-09
    body: "It is nice of the person for doing screenshots,\nbut me thinks that it is better for doing them\nin the default window decoration, theme and\niconset.\n"
    author: "naomen"
  - subject: "Re: decoration and theme"
    date: 2005-06-09
    body: "Yeah, at least follow the standards:\n\nhttp://i18n.kde.org/doc/screenshots.php"
    author: "phil"
  - subject: "Re: decoration and theme"
    date: 2005-06-10
    body: "I think that these instructions are meant to apply to screenshots created for documentation purposes. The instructions must be old because they make reference to XFree86 instead of XOrg.\n\nAnyway way to be an ass about wanting to regulate screenshots that are not even official and given at the convenience of those who make them or spend their time compiling the software to make them."
    author: "jui jim"
  - subject: "Re: decoration and theme"
    date: 2005-06-10
    body: "okay, but...\n\nWhat happens when an outsider (non-KDE-geek) grabs these images. All they can think is, THAT'S the next KDE?"
    author: "ac"
  - subject: "Looks good!"
    date: 2005-06-09
    body: "I cannot stress enough how important ad-blocking is.  I'd never consider using any browser that didn't have something similar to Firefox's AdBlock.\n\nI think Konqueror would also gain a lot more users if it was easier to extend.  The extensions I have installed are the main thing keeping me on Firefox.\n\nWith Firefox, you can just write a bit of Javascript and XML.  With both Firefox and Opera, you can add features by writing a few lines of Javascript with Greasemonkey/user Javascript.\n\nHow much quicker would this Konqueror ad-blocking have come about if Konqueror was as easy as Firefox to extend?\n\nDoes anybody know if there is ongoing development of KaXUL?  Or has it been abandoned?\n"
    author: "Jim"
  - subject: "Re: Looks good!"
    date: 2005-06-09
    body: "well actually the extensions in firefox is also the source of their biggest security holes they have (hopefully _had_).\nI prefer not having 'dynamic' extensions if I lose security.\nI am fed up of all these browsers that in the name of extensibility drops basic security measures.\nFirefox really disappointed me in that area.\nThese days, security in browsers should always be first and I have to say that konqui is one of those too few browsers I have never had major security concerns.\n\nas soon as you allow a dynamic thing to come into your browser, you definitely open new security bugs.\n\njust my 2c.\n\nCheers,\nMik\n"
    author: "Mickael Marchand"
  - subject: "Re: Looks good!"
    date: 2005-06-09
    body: "> well actually the extensions in firefox is also the source of their biggest security holes they have\n\nWhat's your basis for claiming that?\n\nhttp://www.mozilla.org/projects/security/known-vulnerabilities.html#Firefox\n\nDoesn't look like many of them are related to extensions as far as I can see.\n\n> as soon as you allow a dynamic thing to come into your browser, you definitely open new security bugs.\n\nWell then Konqueror is no good either, unless you switch off Java, Javascript, plugins, etc.\n"
    author: "Jim"
  - subject: "Re: On the current state of Konqueror"
    date: 2005-06-09
    body: "Agreed, Konqueror needs to be able to be extended. I'd like to see Konqueror to become as good as Opera as a browser. I still use Opera for all its small conveniences which is also where I find Koqueror (KDE 3.3 here) still lacking.. something like the session thing could be done as an extension, the rest probably needs to be done in Koqueror itself --  some kind of wishlist here:\n\n- save & restore sessions (Operas *.win files), also session restoring for crashes\n- mouse gestures (admittedly I didn't test KHotKeys yet but an Opera compliant preset would be nice)\n- the hopefully soon arriving panning (aka continuous smooth scrolling)\n- and some more settings as how many connections to allow for one site and overall\n\nProblems I still have with Konqueror:\n- different kind of tabs than Opera (again). This also applies to Firefox. The advantage of the more pushbutton like tabs in Opera is that they can be minimized so you can change which tabs you want to see and when. Any chance for it being implemented?\n- fullscreen not really implemented? It's just half a fullscreen, menus, address bar (ok might be useful)and status bar still there. Will a full fullscreen be done where only the page can be seen? To me both seems useful.\n- revenge of the kparts: currently an image is shown in a gwenview tab here where I can email or compress it or copy it to a location with a rightclick. Somehow this is lacking important functions for the image (on rightclick) like save it, copy it, copy url and properties. Don't really know if Konqueror can't show images on its own or if these functions are missing in the Gwenview kpart.\n\nKonqueror is really good and usable apart from that, but at least for me needs still some tuning to make it first choice as a browser."
    author: "Phase II"
  - subject: "Re: On the current state of Konqueror"
    date: 2005-06-09
    body: "Konqueror has \"panning\", Shift+(UpArrow|DownArrow), at least in 3.4. It also has mouse gestures vie KHotkeys, and it works great and you can configure it as you like.\n\nIt also has sessions, called \"View Profiles\"."
    author: "m."
  - subject: "Re: On the current state of Konqueror"
    date: 2005-06-09
    body: "Konqueror has sommth scrolling (like firefox), just use the Suse RPMs\n(or integrate Suse's patch into SVN)"
    author: "Hans"
  - subject: "Re: On the current state of Konqueror"
    date: 2005-06-10
    body: "But no panning on third mouse button which triggers the notorious x clipboard.\n\nok the Profiles seemed more like (and probably are) for setting the layout, but perhaps could be fleshed out, with the history of the saved sites. Wasn't that obvious under the settings tab. (usability to the rescue).\n\nThe stuff noted under \"problems\" remains.\nEspecially Konqueror loading an image in a Gwenview kpart should be emphasized.\nThis might be handy on a local filesystem, but it's just unusable on a website.\nI'd just need about four basic operations on rightclick (save image..., copy image, copy image url, image properties)\nand all four are gone because the picture is embedded in the viewing part of an application. Instead I can rotate it now in the browser.. *sigh*\nDon't know anymore how it was handled before I installed Gwenview."
    author: "Phase II"
  - subject: "Re: On the current state of Konqueror"
    date: 2005-06-10
    body: "I think it's indeed Gwenview that decides what options are shown there.  \n\nIf you don't want Gwenview to be used for embedded viewing you can tweak that in the control center under KDE Components -> File Associations (for each image type in question):  Just change the \"Service Preference Order\" on the \"Embedding tab\" of each image type.  The entry on top is used. \n\nThat way you can keep Gwenview installed and still use something else inside Konqueror.  \n\n"
    author: "cm"
  - subject: "Re: On the current state of Konqueror"
    date: 2005-06-14
    body: "Konqueror does have panning on the third mouse button.\n\nSettings\n> Configure Konqueror\n  > Web Behavior\n    > Mouse Behavior\n      > Middle Click Opens URL in Selection\n\nThat's enabled by default, but if you turn it off, it does scrolling just as in Firefox, IE and, I assume, Opera."
    author: "Dolio"
  - subject: "How to switch on the ad blocker"
    date: 2005-06-09
    body: "Hi Jim\nI am using Konqueror with an open source ad-blocker:\nhttp://www.privoxy.org\nIt removes all the blinking stuff from pages. The filter can be tailored to your needs. Give it a try. \nRegards, Roland"
    author: "Roland Wolf"
  - subject: "Re: How to switch on the ad blocker"
    date: 2005-06-09
    body: "Hmmm, that doesn't sound useful.  If it removes all the bl*nking stuff from the page, what's left?"
    author: "ac"
  - subject: "Re: How to switch on the ad blocker"
    date: 2005-06-09
    body: "It's more useful and powerful than that, but it requires quite a bit of tweaking to adjust to your needs."
    author: "Anonymous"
  - subject: "Re: How to switch on the ad blocker"
    date: 2005-06-10
    body: "You don't blinking say?  =)"
    author: "ac"
  - subject: "Re: How to switch on the ad blocker"
    date: 2005-06-09
    body: "Sorry Roland, Privoxy just doesn't compare.  With AdBlock, I see something annoying, I right-click, and block it.  Messing around with a proxy every time I see something annoying is too much hassle, I'd rather just put up with the ads.\n"
    author: "Jim"
  - subject: "Re: Looks good!"
    date: 2005-06-09
    body: "Firefox extensions create a lot of security holes and make it unstable. I'm the developer of Plastikfox and extensions usually step one on top of other forcing me to do dirty hacks to make it work. They have full access to all the parts of the browser, which is very dangerous. Also, extension developers don't usually care not to do things that could break other extensions. They just care their extension works but sometimes two extensions try to access the same resource causing problems. The result is a bunch of bloated dirty code running on your computer that could explode in your face at any time. If they were implemented in a clean way it would be a point but they don't. That's the reason I don't like extensions and I wouldn't like Konqueror to get that too, or at least in a similar way to Firefox. Perhaps if extensions were implemented in a way similar like plugins are (being them all independent and one extension not having access to others' variables an code) it would be much better."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: Looks good!"
    date: 2005-06-09
    body: "> Firefox extensions create a lot of security holes and make it unstable.\n\nWhat is your basis for saying that?  As I pointed out to another person who said the same thing, the facts don't seem to agree with your opinion.\n\n> That's the reason I don't like extensions and I wouldn't like Konqueror to get that too\n\nErm... Konqueror has been extendable for years.  What I'm talking about is making it *easier* to extend.\n\nRight now, you need to extend Konqueror in C++, and that opens up a hell of a lot more ways for an extension writer to screw up than Firefox extensions.\n"
    author: "Jim"
  - subject: "Re: Looks good!"
    date: 2005-06-10
    body: "and why did you think you'd find bugs related to 3rd party extensions in the mozilla bug database?\n\nThe trouble with Firefox extensions is that they've given full access to the browser, but are generally only written and maintained by one person.  Often someone with very little experience.\nThey just don't have the same level of QA as the browser itself.\n\nObviously there are exceptions, but the Firefox extension mechanism opens itself up for a whole lot of half arsed buggy, insecure extensions."
    author: "mabinogi"
  - subject: "KDE Start MENU"
    date: 2005-06-09
    body: "So, at last after many years, KDE Developers got the courage to have Icon+Text!  Congrats, but make it beautiful, attractive, customizable, animated. Support MNG and GIF for K button. Allow 4 state png/mng for k buttons! unfocused, focused(hover), pressed (focused), pressed (not hover).\n\nCongrats on becoming bold!"
    author: "fast_rizwaan"
  - subject: "No text, please!"
    date: 2005-06-09
    body: "Please, no text in the K-Menu! Or at least a text different than \"Applications\", I don't want a Gnome clone and Gnome people will say we're dropping our pants. Also make it a configurable option so people can disable the text (I will do!)."
    author: "V\u00edctor Fern\u00e1ndez"
  - subject: "Re: No text, please!"
    date: 2005-06-09
    body: "a) it is configurable\nb) it's not on by default\n\ni turned it on by default for one week (and turned it back off at the end of that period of time) so that the code path for button text would get wide testing. apparently these screenshots where taken during that week =)\n\none of the problems with having as many features, several of them \"hidden\", in an application such as kicker where people tend to run it in a more-or-less static configuration is that getting good testing coverage can be difficult. so i occasionally turn things on or off by default in SVN to get some testing by those who follow svn. KConfigXT has made this easier since i just modify an XML file and it doesn't write default values to the rc; both features are nice for this =)\n\nunfortunately there isn't much that a test suite could automate here, either, since most of the \"output\" of kicker is graphical and interactive in nature requiring a user to try things and interpret the results as \"expected\" or \"buggy\". kicker behaviour is also sensitive to things like X configurations, including having save-unders enabled, Xinerama setups, dual-head configurations and screen resolutions."
    author: "Aaron J. Seigo"
  - subject: "Re: No text, please!"
    date: 2005-06-09
    body: "nice...\n\nthe word \"Applications\" takes way to much space in the kicker.\nit's a bad thing to do. please keep the UI clean."
    author: "Mark Hannessen"
  - subject: "Re: No text, please!"
    date: 2005-06-11
    body: "agreed 100%"
    author: "agreed"
  - subject: "Re: No text, please!"
    date: 2005-06-13
    body: "++"
    author: "light"
  - subject: "feel?"
    date: 2005-06-09
    body: "how does the new kde *feel* ?"
    author: "ch"
  - subject: "Re: feel?"
    date: 2005-06-09
    body: "It feels somewhere between a yellow."
    author: "Illissius"
  - subject: "Re: feel?"
    date: 2005-06-09
    body: "ehehe , i meant , could you somehow distinguish between qt3 and qt4 toolkit  (more responsive, more colors , more ..... i dont know)\n\nor perhaps its just for the programmers under the hood . ??\n\n"
    author: "chris"
  - subject: "Re: feel?"
    date: 2005-06-09
    body: "Current svn trunk is for 3.5, so it's still Qt3. It feels mostly like 3.4 did, a bit more gnomeish (not a bad thing) due to the 'add applet' dialog and the hidden option to have taskbar buttons take up the full length of the taskbar.\n\n(The Qt4 branch probably feels not much more than buggy at this early stage, though I haven't tried it...)"
    author: "Illissius"
  - subject: "Re: feel?"
    date: 2005-06-09
    body: "AFAIK the Qt4 branch doesn't even compile yet. Though it wouldn't suprise me if it already does, given the pace of development."
    author: "Erik Hensema"
  - subject: "Yuck"
    date: 2005-06-09
    body: "I was trying to enjoy the new Qt but all I could think was \"Arrgh, motif's coming back!\" Seriously, that theme is _horrible_."
    author: "mikeyd"
  - subject: "Re: Yuck"
    date: 2005-06-09
    body: ">I would encourage the viewer to ignore the rest of the desktop, for I use \n>KDE's CDE/Motif theme, which many people find unattractive\n\nuhm....? ;-)\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Yuck"
    date: 2005-06-11
    body: "I did my best, but it's that bright horrible red, draws the eye even when you're trying to ignore it."
    author: "mikeyd"
  - subject: "Vertical text Sucks..."
    date: 2005-06-09
    body: ">>(KMix has a separate tab for device switches.)\n\nJust like GNOME too.\n\nAnd why dont get rid of th annoying Vertical Text?\n\n"
    author: "Tim"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-09
    body: "+1. I agree totally. This need some plasma ray =)"
    author: "Veton"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-09
    body: "What ever happened to the improvements suggested here:\nhttp://kdelook.org/content/show.php?content=16962\n\nI love that sidebar with HORIZONTAL text!\nI think the last I heard is that some of this was accepted, but some also required Qt changes, which couldn't be done.\nToo bad really, it looks really nice.\ncheers,\n-Ryan"
    author: "Ryan"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-09
    body: "It doesn't scale well."
    author: "Anonymous"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-09
    body: "In what respect?  The length of text/languanges?"
    author: "Ryan"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-09
    body: "Trying to get the sidebar to \"scale\" is clearly a case of overengineering.  There are wayyyy too many tabs if you need it to \"scale\" beyond 3 or 4.  Usability wise that would just be suicide to have too many tabs.  Well I guess it's suicide right now already. =)\n\nThis is why the horizontal design is superior.  It solves the right problem not an invented \"scale\" issue."
    author: "ac"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-10
    body: "You forgot the uninvented i need to run over the full screen height to switch tabs suckage. Forget it. It's no solution. And it's annoying when you misclick. Go think again (and come with something usable next time, please)."
    author: "mornfall"
  - subject: "Re: Vertical text Sucks..."
    date: 2005-06-09
    body: "Nooo, please don't get rid of vertical text tabs! I like that concept very much!"
    author: "Mikos"
  - subject: "may I ask, what distro?"
    date: 2005-06-09
    body: "nice preview.  I really like how things are coming along.  The vector demo looks really nice.  there is also some qt4 preview stuff at the digitalfanatics qt4 website, but it's geared more towards programmers.\n\nkinda off topic, but I'm just curious, what distro are you using? \n\n"
    author: "Timmeh"
  - subject: "Re: may I ask, what distro?"
    date: 2005-06-09
    body: "Sorry im an idiot. its right in the article \n\n\"KDE 3.5 was built on my machine from the main trunk of KDE's subversion tree on a machine running Gentoo stable, using KDE's CDE/Solaris/Motif themeset.\"\n\nsomehow missed it.\n"
    author: "Timmeh"
  - subject: "Re: may I ask, what distro?"
    date: 2005-06-09
    body: "Timmeh!!"
    author: "ac"
  - subject: "Qt4 SUCKS!!! Please go back to Qt3!"
    date: 2007-08-16
    body: "While attempting to port a medium-sized program to Qt4, I noticed a SEVERE drop in performance. To diagnose this droppage, I wrote a very small, simple program that depicts two rectangles bouncing around on the screen, and reports the framerate (which is calculated as an average over 5 seconds).\n\nUnder both versions of Qt, the program used a QTimer widget to control the movement of the rectangles. The Qt3 version can simply draw the rectangles directly from the QTimer handler, while the Qt4 version must generate a paintEvent, which I tried both with the \"update()\" method (which supposedly waits until Qt's main loop to call paintEvent normally), and with the \"repaint()\" method (which supposedly calls paintEvent immediately, which is supposed to be faster but more likely to flicker).\n\nOn my machine (A 1-GHz Celeron with a low-end Radeon, resolution 1400x1050, 16bpp), both versions run fast when the window is at its default size (49 FPS-- the target framerate).\n\nWhen the window is maximized, however, Qt4 takes a 39% performance hit (19 FPS), while Qt3 takes no hit at all!\n\nIn larger programs with lots of widgets (such as the program I was porting when I first noticed the problem), I notice that any use of a timer-updated widget drags the program to a near halt.\n\nI have attached the program, so you can see for yourself that Qt4 really is inferior. It must be built with the $QTDIR environment variable set appropriately. Two Makefiles are required because MOC doesn't pay attention to #ifdef directives. You must run 'make clean' between building the demo with Qt4 and Qt3.\n"
    author: "Daelin the Cruel"
  - subject: "Re: Qt4 SUCKS!!! Please go back to Qt3!"
    date: 2007-08-23
    body: "You are right. But how much qt4 really sucks you will know only if you try to build a real world application. I'm trying to get a working tree view for very large data sets - the Troll versions tend to break down with a o(n**2) behavior or  other insanities. And the sophisticated model/view pattern is ... bullshit! The documentation is worthless; without looking at the source code you wont ever understand how things should work.\n\nCode less. Create more?\nCode less. Think more!\n"
    author: "cornelius sohn"
---
Since KDE migrated to Subversion, I have been creating semi-weekly development builds in the hopes of finding bugs to report.  Notable new features include Konqueror's new adblocking mechanism and Kicker's new applet manager.  Since everyone likes screenshots, I created two articles with screenshots from my observational usage: previews of <a href="http://process-of-elimination.net/?q=kde_3_5_observations">KDE's trunk code</a> and <a href="http://process-of-elimination.net/?q=qt4_observations">Trolltech's Qt 4</a> alongside brief documentation of what one is looking at.

<!--break-->
