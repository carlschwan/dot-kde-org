---
title: "Nokia Engineers on KHTML, Collaboration and aKademy"
date:    2005-10-17
authors:
  - "prodrigues"
slug:    nokia-engineers-khtml-collaboration-and-akademy
comments:
  - subject: "preprocess html?"
    date: 2005-10-16
    body: "\"To give an example, in browsers Javascript needs to be executed at the point that it is found in the markup, resulting in blocking the parser. External Javascript files take a much longer time to arrive at the client over GPRS.\"\n\nWhy can't web pages be preprocessed by the wireless provider, before being sent to the phone? E.g. make all JavaScipt and CSS inline (and maybe even execute some of the JS), remove tags that won't be displayed anyway (Flash, Java, some formatting), to make HTML simpler and smaller?\n"
    author: "Dima"
  - subject: "Re: preprocess html?"
    date: 2005-10-16
    body: "You would still need a powerful rendering engine on the phone; otherwise you would have no chance of rendering complex webpages anything like how they appear on your computer screen (which is the goal).  These phones have the processing power to run a real web browser, all that Nokia really needs to do is port one.  So they are."
    author: "Spy Hunter"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "If the code is changed at the provider it is changed for all browsers. If I connect my notebook to the net via cellphone and use a normal desktop-browser like Konqueror I wouldn't really like that..."
    author: "Ascay"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "If it is implemented sensibly it would be done using a proxy server - and you would have the choice of using the proxy or not."
    author: "David Dorward"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "I could see it being quite sensible for one to simply inline all the css/javascript. Which ideally wouldn't change the rendering. (I can think of one situation where it might off the top of my head, but I think that can be handled inline.)"
    author: "James L"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "That seems like a really good idea to me.\n\nPerhaps you should implement this as a product :)"
    author: "a. p. non"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "Your dream already exists :)\n\nMy company has created a product called PocketSurfer.\nhttp://www.pocketsurfer.com"
    author: "JC"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "Opera already does this with its Opera Mini application suite (http://www.opera.com/products/mobile/operamini/). It requires a concerted effort by both manufacturers and phone providers though, and there's still severe levels of rivalry in the business that make it hard to roll out something like that."
    author: "Bryan Feeney"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "\"Why can't web pages be preprocessed by the wireless provider, before being sent to the phone?\"\n\nDo you really want the WAP fiasco all over again? I prefer my network dumb and trasparent and the smarts in the endpoint, thank you.\n"
    author: "Oren T"
  - subject: "Re: preprocess html?"
    date: 2005-10-18
    body: "No, WAP has nothing to do with his idea. WAP wasn't about the server. WAP just brought a new markup language (WML) more suitable for mobile phones than HTML.\n\nWAP wasn't succefull for two reasons:\n- few websites published WAP versions (I don't blame webmasters, it just wasn't an easy task);\n- the user needed to specify a different URL than he would for the regular version."
    author: "blacksheep"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "This is basically how WAP works."
    author: "Alex"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: "This is what we do in my company. The product is called PocketSurfer\nWe process html before sending to the wireless device.\nhttp://www.pocketsurfer.com"
    author: "JC"
  - subject: "Re: preprocess html?"
    date: 2005-10-17
    body: " \nBecause that would break browser caching on the phone. For example, a cnn.com's HTML content probably changes more often than its CSS or JavaScript. If the CSS and JavaScript are in separate files from the HTML, the browser can cache them."
    author: "cpeterso"
  - subject: "kde components?"
    date: 2005-10-16
    body: "Nice to see Nokia use Webcore, but to say that \"KDE could soon be making its way into your mobile phone\" is a little presumptious, insn't it? They're only using the de-kde'efied khtml from Apple... There's even a Gtk-version, developed by Nokia: http://gtk-webcore.sourceforge.net/\n"
    author: "Foo"
  - subject: "Re: kde components?"
    date: 2005-10-17
    body: "khtml is a KDE technology, ported or not. Also khtml had few as possible dependencies from the very beginning which makes it easy to port. What is presumptious about being a little proud that a KDE technology is getting widely used outside KDE itself after all those time naysayers said KDE would only reinvent and isolate?"
    author: "ac"
  - subject: "yep"
    date: 2005-10-17
    body: "yep and the windowing system they use on their linux tablet is totally gnomish or at least gtk based, looks cool though http://www.maemo.org/screenshots.html"
    author: "Patcito"
  - subject: "i second this!"
    date: 2005-10-17
    body: "this type of open palmtop OS Nokia is building is more than a bit gnomish, yet they needed a leaner HTML/JS render so they came out on Apple's WebCore (based on KHTML/KJS)...\n\nThey ported it to GTK:\nhttp://gtk-webcore.sourceforge.net/\n\nNokia allready proted KHTML/JS away from a lot of Qt dependencies.\n\nSo the bottom line?\nIf this device is as open as it looks, it will run several mini distro's...\nThen maybe one will be more KDE related.\n\nYet apps delivered for the default platform will probebly be GTKish.\n\nBut this is a guess...\n\nIt looks like a very interesting platform though.\n\nMy .02EUR\nCies Breijs.\n"
    author: "Cies Breijs"
  - subject: "Re: i second this!"
    date: 2005-10-17
    body: "From what I've seen, you're talking about two different things.\nThe linux tablet uses a gtk interface indeed, but the browser is a customized version of Opera.\nThe device on which will run this KHTML-derived engine will be Symbian based.\n\nSo two very different things."
    author: "Anonymous"
  - subject: "Re: yep mixed up"
    date: 2005-10-17
    body: "Indeed, thanks for pointing out...\n\nThe WebCore port to GTK was originally done as a feasability study by Nokia for the InternetTablet770. The 770 will run Maemo.org (linux+gtk).\n\nThe Nokia Series60 will not run Linux bus symbian and will have neither Qt nor GTK, but will run WebCore by default.\n\nSorry,\n_cies."
    author: "Cies Breijs"
  - subject: "KHTML2 & KSVG2?"
    date: 2005-10-17
    body: "Very nice to see KHTML is now widely used. Btw, what about KHTML2 & KSVG2? What's the status of them now? Are we going to have KHTML2 & KSVG2 in KDE4? And will next generation of Webcore be based on KHTML2?"
    author: "fyanardi"
  - subject: "Re: KHTML2 & KSVG2?"
    date: 2005-10-17
    body: "afaik the answers are \"working on it\"; \"getting in shape\"; \"yes\"; \"most likely\"."
    author: "superstoned"
  - subject: "Re: KHTML2 & KSVG2?"
    date: 2005-10-17
    body: "Wow, great to hear that! Thanks to all KHTML developers!"
    author: "fyanardi"
  - subject: "Re: KHTML2 & KSVG2?"
    date: 2005-10-18
    body: "What would be great is XUL support.  Then you could write great webapps to use in KDE."
    author: "JJK"
  - subject: "Re: KHTML2 & KSVG2?"
    date: 2005-10-18
    body: "or just use firefox and save you the hassle."
    author: "lynxy"
  - subject: "Re: KHTML2 & KSVG2?"
    date: 2005-10-18
    body: "why use firefox, if konqueror is there? it works much better with other KDE apps, is faster, and looks better..."
    author: "superstoned"
  - subject: "Re: KHTML2 & KSVG2?"
    date: 2005-10-18
    body: "There's also no reason at all you couldn't use XUL in Konqueror."
    author: "Segedunum"
  - subject: "Great news, but still there is something to do..."
    date: 2005-10-18
    body: "Wow, Nokia is making great steps in the open source world. I like the platform, how easy is to develop applications (in java or c++) and the fact that it now supports os components (even when they are so customized for their devices) can be  a new boost for our community (marketing that spread the kde/gtk/linux name around the world, and programming skill/technology that can improve many projects).\n\nBut one thing really lacks, and I don't understand why... there is no way (other than hack with wine) to develop nokia/symbian applications using nokia tools.\n\nNo matter if you use java, c# or c++, series 40 and 60 sdk can be used only on windows. There is support for eclipse in the latest series 60 developers's suit, but it only works on windows...\n\nThey even provide you with a version of gcc, and java libraries only on windows..."
    author: "Mario"
  - subject: "Re: Great news, but still there is something to do..."
    date: 2005-10-20
    body: "There is a site for getting a subset of the S60 SDK functionality going on Linux:\n\nhttp://www.ki-ag.de/pages/tech/SymbianSDK/symbian_sdk_6.1_on_linux.html\n\nIt's a pain though that the emulator doesn't work on Linux, and there are a bunch of other small tools that don't work.\n\nIf you choose to use Series 60 python though, there are quite a lot of Linux/Unix tools that make it easier."
    author: "taj"
  - subject: "Re: Great news, but still there is something to do"
    date: 2005-10-20
    body: "Hi!\n\nThanks for the link, I'll give it a try.\n\nAs a side note, I found a good alternative for java (it's java only, but with Python and Java and the link you give we are pretty well covered):\n\nhttp://www.j2mepolish.org/\n\nI need to check it out, but it seems that this library takes care of all the aspect related to graphics in j2me products, it works in midp 1.0 and cdlc 1.0 adding support for midp 2.0 to old devices, it can be customized for specific platforms, but can be used with virtually the same results on the sun wireless toolkit (so we don't need nokia/motorola/what else anymore for java programming under linux).\n\nHaving an emulator at hand for testing is a good think, but this way one can test the application on an emulator on windows just before the release.\n\nThank you again for the reply,\nMario"
    author: "Mario"
---
So, you have KDE on your desktop and laptop systems, you've heard
that Apple uses KDE components for its web browser <a href="http://www.apple.com/safari/">Safari</a> and you've
tried <a href="http://www.konqueror.org/embedded/">Konq/Embedded</a> on your handheld. But did you know that KDE could
soon be making its way into your mobile phone? At <a href="http://conference2005.kde.org">aKademy</a> in August
David Carson and Deepika Chauhan from Nokia presented the work they've
done in integrating KDE components into the latest version of the
company's mobile phone software.  I recently caught up with them to ask about the technical side of their work, why they chose KDE components and their experiences of aKademy.





<!--break-->
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 2ex"><a href="http://static.kdenews.org/danimo/davidanddeepika.jpg"><img src="http://static.kdenews.org/danimo/davidanddeepika_small.jpg" width="300" height="220" /></a><br /><a href="http://static.kdenews.org/danimo/davidanddeepika.jpg">David and Deepika at their talk in Malaga</a></div>

<p><b>Can you describe briefly what you're working on within
Nokia?</b></p>

<p>We are working on the Series 60 Browser application for the S60 platform.
The <a href="http://www.series60.com/">Series 60 Platform</a> is the software platform that runs Series 60 devices, which are also called smart phones. The Series 60 Platform consists of a user interface for interacting with the device's data and software, and programs (Series 60 applications) that provide advanced phone functions such as messaging, the calendar, browsing, etc. The
Series 60 Platform runs on top of the open Symbian OS operating system.
In addition to existing applications, users can also install additional
applications.</p>

<p><b>What technical challenges do you face in that work?</b></p>

<p>Mobile devices are constrained by ROM, RAM, network latency and
bandwidth, display, input mode, and less powerful CPU as compared to
desktop world. We need to make some changes to take into account these
constraints. The code execution behaviors which are correct in the
desktop world do not always work well on mobile device.</p>

<p> To give an example, in browsers Javascript needs to be executed at
the point that it is found in the markup, resulting in blocking the
parser. External Javascript files take a much longer time to arrive at
the client over GPRS. In the worst case scenario, the consecutive
multiple external javascript references cause the requests to go out
serially. Similar problems are encountered in CSS processing since
rendering blocks on external CSS. We do not encounter these problems
when the Javascript and CSS are inline.</p>
 
<p><b>What parts of KDE are you using to help you achieve your
aims?</b></p>

<p>We are using WebCore and JavaScriptCore components from Apple's Safari
Browser which are based on KHTML and KJS components respectively that
were developed by the KDE community.</p>

<p><b>Why did you choose KDE over the competition?</b></p>

<p>We have been developing a proprietary browser for Series 60. We found
that we have been focusing a lot of resources on dealing with issues
like rendering and script execution - issues that have already been
dealt with by open-source components. We decided that since browsing is
a complex technology, we should not re-invent the wheel. We started
investigating the available open-source solutions and decided to go with
a KDE-based solution for primarily 3 reasons:
</p>
<ol>
<li>Series 60 devices are constrained by ROM/RAM. WebCore/KHTML and
JavaScriptCore/KJS provide rendering engines that use very small RAM/ROM
footprint. That was really a clincher for us.</li>
<li>WebCore/KHTML and JavaScriptCore/KJS provide a very clean
architecture enabling easy ramp up of resources.</li>
<li>WebCore/KHTML and JavaScriptCore/KJS are very fast. The startup time
and general performance of the browser is quite good.</li>
</ol>

<p><b>How do you see the relationship between the KDE project and your
team at Nokia developing?</b></p>

<p>We would like to find a model where we can collaborate with  the  KDE
community focused towards browsing related solutions.</p>

<p><b>More generally, where do you think mobile technology is going in
the near future, and what part will open source software like KDE
play?</b></p>

<p>
With the advent of faster networks and more capable mobile devices we
expect to see a lot of development in the mobile software area. We hope
that in the future the KDE community will be interested to look beyond
desktop software and considers taking on projects in the mobile software
space. We see Nokia's new open source browser as a good project to
spark the open source community's interest in mobile software
applications.</p>

<p><b>What was your experience of aKademy?</b></p>

<p>We had a great time at aKademy, and we got much more out of it than we
ever anticipated. We came to aKademy since we wanted to thank the KDE
community for the great components created by them that form the basis
of the future Series 60 browser, meet some of the contributors in
person, and share with the community our experiences of building a
browser around WebCore/KHTML and JavascriptCore/KJS. The conference gave
us a better understanding of the working model of KDE. We hope that we
can work together with KDE on the mobile browser.  We have observed a
lot of excitement among developers in contributing to the mobile
applications and we hope the community can bring their innovations to the
mobile platform.</p>

<p><em>Many thanks to Deepika and David for taking the time to talk to me.</em></p>























