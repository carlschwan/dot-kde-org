---
title: "NewsForge: Karamba, What an Attractive Desktop!"
date:    2005-03-24
authors:
  - "jsnyder"
slug:    newsforge-karamba-what-attractive-desktop
comments:
  - subject: "A bit misleading"
    date: 2005-03-24
    body: "Hi all\n\nI really don't mean to be rude, but I think this introduction is a bit misleading (almost slashdot-like). The amount of \"themes\" available is very minimal and the article doesn't make it sound very attractive to a beginning user.\n\nI tried the mandrake 10.1 rpm of superkaramba, but I was unable to get it to work properly, it crashed at some mouse-clicks. The icon doesn't inspire much confidence and the user interface just sucks.\n\nSo to summarise:\n- It could do with more themes\n- It would be nice to have a selection of themes (using rss) available for download and automatic installation\n- Access to the welcome screen from the menu\n- A better description of what it does more than a theme from kde-look\n\nCheers\n\nSimon"
    author: "Simon"
  - subject: "Re: A bit misleading"
    date: 2005-03-24
    body: "I'm still waiting for anything else than sensor temperature, weather and rss feeds.\nAs for the kicker replacements, wether karamba or not, they all are nicer, but they also are slower or more difficult to use than kicker."
    author: "hmmm"
  - subject: "Re: A bit misleading"
    date: 2005-03-24
    body: "The developer supported 10.1 rpm is hosted here:\nhttp://rpm.borgnet.us/10.1/media/RPMS/\nI don't know what version you used, but I've not seen any reports about crashing from mouse clicks.\nAny features you'd like to see can be posted on our sourceforge project site so that way we can keep track of them.\nhttp://sourceforge.net/tracker/?group_id=21351&atid=371892\n\nI've emailed the author concerning the Python2.4 problems and notified Olaf that the next release will have the fix and will be out as soon as we can get stable.\n\ncheers,\n-Ryan"
    author: "Ryan"
  - subject: "Re: A bit misleading"
    date: 2005-03-24
    body: "Ok, tried the RPM you mentioned... This one starts up with the welcome screen, but when you close it, it's gone, nothing in kicker and when you start it again, the welcome screen again. So I guess it can't initialise something...\n\nFurthermore, I can download a theme, but only after a bunch of clicks and very good reading of all the text. That doesn't help, because nobody reads manuals ;-)\n\nHow do you install one of these themes, where to unpack the tarball?\n\nThis is all very confusing, not something you want to advertise to unsuspecting users of KDE, which is arguably the most mature complete desktop system for Linux.\n\n/Simon"
    author: "Simon"
  - subject: "Re: A bit misleading"
    date: 2005-03-26
    body: "You can unpackage your downloaded themes anywhere you want.  When the welcome screen comes up, you have to click on Open Theme.\nThen browse to the directory you unpacked and click on the .theme file there.\nSimple as that.\nIf you continue to have problems, please come to our forums as we help people all the time.\nhttp://sourceforge.net/forum/forum.php?forum_id=67470\ncheers,\n-Ryan"
    author: "Ryan"
  - subject: "Re: A bit misleading"
    date: 2005-03-24
    body: "Uhmmmm... you must be looking into the wrong place. You think 588 themes is _MINIMAL_? That's the count I get in kde-look"
    author: "~~"
  - subject: "The same for me"
    date: 2005-03-25
    body: "I tried it some weeks ago in Debian and I got the same impression.\n\n-I didn't find any useful theme, only weather viewers and CPU sensors.\n-Anyway, I tried them (and some music players) and they were normally crashing after some seconds."
    author: "kAnonimous"
  - subject: "Usability"
    date: 2005-03-25
    body: "I'd like to see what the usability bods say about stuff like Karamba."
    author: "GldnBlls"
  - subject: "Re: Usability"
    date: 2005-03-25
    body: "Well, it can't be too terrible, considering that Apple is releasing their own equivalent (in Tiger?), and we all know that Apple would never make a usability faux pas. :)"
    author: "Dolio"
  - subject: "Re: Usability"
    date: 2005-03-25
    body: ":-). I've often wondered whether the people who laud OS X as the bees knees and the lobster's toes in terms of usability have actually used the inconsistent mess adorned with fuzzy fonts and no keyboard shortcuts to speak off that is OS X... Give me KDE any day!"
    author: "Boudewijn"
  - subject: "I don't understand the problems above..."
    date: 2005-03-26
    body: "I've been using superkaramba for almost as long as it exists, and it's great. indeed, there are quite a few monitoring tools for it (and if course I use one, to see my cpu, network, memory and disk info). but also the amarok-album view tool, the Liquid Weather ++, and an rss reader. and the to-do tool. they are all very usefull, and there are plenty of different variations of these. I hope superkaramba gets true transparancy (xorg) soon, as it'll speed things up quite a bit I guess :D"
    author: "superstoned"
  - subject: "Re: I don't understand the problems above..."
    date: 2005-03-26
    body: "I would love to move it to the true transparency and remove the dependancy on kde's krootpixmap, but I've not found enough documentation yet.\n-Ryan"
    author: "Ryan"
  - subject: "Re: I don't understand the problems above..."
    date: 2005-03-26
    body: "well, thanx at least for thinking about it :D\n\nI think it'll be allright, as the new xorg features are liked very much - hope they'll get stable asap..."
    author: "superstoned"
  - subject: "Karamba all the way!"
    date: 2005-03-26
    body: "The article mentions \u00abWindows application Samurize or Konfabulator for Mac\u00bb. What are the plus and minus of SuperKaramba related to them? And to Gnome's alternative?\nJust wondering, cause I really like SuperKaramba and I'm curious what more could it do.\n\nBy the way, I hate the system tray icon. :/ I don't really hate it, but, you know, I find it annoying that I've lots of docked apps in there (8). I don't really think SuperKaramba needs it. I mean, you can just setup with a right click on a theme, and when no theme is open, there's the welcome screen."
    author: "blacksheep"
  - subject: "Re: Karamba all the way!"
    date: 2005-03-26
    body: "In kde 3.4 if you right click on the system tray you can select which icons are hidden in the system tray.\nThere is at least one workaround.\n-Ryan"
    author: "Ryan"
  - subject: "Re: Karamba all the way!"
    date: 2005-03-26
    body: "That's awesome! I didn't know that -- I'm still using 3.2."
    author: "blacksheep"
  - subject: "Re: Karamba all the way!"
    date: 2005-03-28
    body: "Blacksheep, gnomes alternative to SuperKaramba is GDesklets, http://gdesklets.gnomedesktop.org"
    author: "ricky"
  - subject: "Re: Karamba all the way!"
    date: 2005-03-28
    body: "Argh!  More g-naming stupidity.  Can't those guys come up with original names?"
    author: "ac"
---
<a href="http://software.newsforge.com/article.pl?sid=05/03/15/1151204">Newsforge has an article</a> on how to use  <a href="http://netdragon.sourceforge.net/">SuperKaramba</a> to turn your great-looking KDE desktop into something amazing. "<em>Just with a standard KDE install, I get the occasional 'Hey, that looks nice' from friends. When I discovered a program named Karamba for KDE a while back, that suddenly changed to outright 'Wow!'</em>"




<!--break-->
