---
title: "digiKam Artwork Contest"
date:    2005-02-11
authors:
  - "finiteman"
slug:    digikam-artwork-contest
comments:
  - subject: "Kids desktop environment?"
    date: 2005-02-11
    body: "Funny feature, should be merged with ktuberling ;)\n\nNow, will we also have superimpose templates for KPhone? \"Sshhhh..sshhh Hey buddy, call ya from the beach, it's fun here with all the girls ssshhh sshhh\" \n\nAnd finally we will need superimpose templates for video conferences :)) Animated videos for the background, like a konqui running cross your room, I would like that. Or putting yourself into a tv show... or whatever. Imagination is the limit.\n\nNo offense, really, there will be use for it. Guess what KDE demo points at fairs will start to attract visitors with from now on :))\n"
    author: "Friedrich"
  - subject: "Re: Kids desktop environment?"
    date: 2005-02-12
    body: "There was a 3D object recognition tool for video which used some good edge and corner detection and a webcam to fairly effectively model a room. Then, things such as orcs could be placed in it in real time and made to walk about fairly effectively. Maybe your video templates aren't so far distant?"
    author: "jameth"
  - subject: "that's hilarious"
    date: 2005-02-11
    body: "I have to say the KDE community is feeling more and more fun each day.  I love it!  You can just feel the momentum of this project. =)"
    author: "ac"
  - subject: "Great!"
    date: 2005-02-11
    body: "This is a really great idea.\nIt shows that the Linux community has finally arrived \non planet earth ;-) and writing apps that are\nfun to use for all of us - giving the novice (Linux) users\nuseful results from their computer.\nNot so long ago a lot about Linux was like having a microwave oven\nwith lots of different buttons for destroying eggs and\npeforming physical experiments but without anyone ever thinking\nabout using it to prepare a meal.\nAfter many network packet analyzers and window managers - finally\nprograms are arriving that are:\na) user-friendly\nb) comprehensive\nc) professional\nAmaroK, K3B, DigiKam, KolourPaint, etc\n(Flame bait: IMHO you can say this about _way_ more KDE than Gnome apps.)\nAnd to all the sceptic geeks out there I can only say \nthat all this is a good thing because network analyzer won't magically\ndisappear but our favorite OS will gain a lot more momentum.\n\n"
    author: "Martin"
  - subject: "Re: Great!"
    date: 2005-02-12
    body: "> Not so long ago a lot about Linux was like having a microwave oven\n> with lots of different buttons for destroying eggs and\n> peforming physical experiments\n\nahahahahahaaha! \n\nthat was good. thanks for the laugh! hehe... \"egg destroyer button\" ... now to find a way to use that in my blog =P\n\n(and yeah, i agree with what you wrote 100%)"
    author: "Aaron J. Seigo"
  - subject: "Re: Great!"
    date: 2005-02-12
    body: "I totally disagree - its not flame bait to say KDE > Gnome on dot.kde.org. :P"
    author: "Ian Monroe"
  - subject: "Flamebait it is!"
    date: 2005-02-12
    body: "Of course it is flamebait!\n\nIt belittles KDE on its own web site. \n\nHow can you dare to praise KDE, and use less than 100 characters and only one sentence to do so?"
    author: "konqui-fan"
  - subject: "Re: Flamebait it is!"
    date: 2005-02-12
    body: "LOL\n\ngood thread :D\n\nBut I agree with the first poster, this is work that will set KDE's applications apart from the other FOSS applications.\n\nthese kind'a things where what FOSS always missed: the finnishing touch. easy to templates, and alot of them :D\n"
    author: "superstoned"
  - subject: "Great"
    date: 2005-02-12
    body: "These artwork contests are inspiring more and more artists to get involved with apps, and improving apps' artworks. Very good. "
    author: "anon"
---
Photo management application <a href="http://digikam.sourceforge.net/">digiKam</a> and <a href="http://www.kde-look.org">KDE-Look.org</a> are teaming up to have a <a href="http://www.kde-look.org/news/index.php?id=155">contest for the best new Superimpose Templates</a> which will be included in digiKam's next release.  The prizes are a digital memory card and kde-look t-shirt.

<!--break-->
<p>With digiKam's new Superimpose Templates you can make it look as though you have gone to exotic places without leaving the comfort of your desk.  You can create totally killer postcards to keep on your fridge because they make you look so popular. You to can even have the body of a film star without those confusing gym memberships.</p>

<p><img src="http://revelinux.com/files/digikam_contest_sm.jpg" width="372" height="219" /></p>

<p>The Superimpose plugin allows you to use PNG template files 
containing borders, frames, and composite images that can be added to or 
superimposed on other images, hence the name Superimpose Templates. The 
currently selected image acts as a background and the templates can then be 
selected and overlaid on the image. You will be able to see the image 
underneath the transparent areas of the template. These templates can be 
created using paint programs such as The Gimp or Inkscape.</p>

<p>With this new feature you can create some great images including 
photocards. There is a <a href="http://digikam3rdparty.free.fr/digikamimageplugins-2005/superimposetemplate.png">screenshot 
of this plugin in action</a> on the <a href="http://docs.kde.org/en/HEAD/kdeextragear-3/digikamimageplugins/superimpose.html">Superimpose 
Templates section of digiKam's manual</a>. Download the latest version of <a 
href="http://kde-apps.org/content/show.php?content=9957">DigiKam</a> and the 
<a href="http://kde-apps.org/content/show.php?content=16082">plugin pack</a> 
to experience this and many more of digiKam's awesome new features.</p>

<p>The categories for this contest are:</p>
<ul>
<li>Special events or holidays - birthday, new baby, Christmas etc</li>
<li>Places - towns, monuments, nature scenes</li>
<li>Hobbies - sports, computers, etc</li>
<li>Cartoons</li>
<li>Miscellaneous</li>
</ul>

<p>The 1st place entry will win a 256MB digital memory card and a cool new t-shirt from <a href="http://cafepress.com/revelinux">Revelinux Gear</a> and the top two entries from each category will be included in the next release of digiKam.</p>

<p>Our contest is open to all artists. For more details see our <a href="http://www.kde-look.org/news/index.php?id=155">contest guidelines</a>.</p>

<p>To check out the <a href="http://www.kde-look.org/content/search.php">latest entries</a> visit <a href="http://www.kde-look.org">kde-look.org</a>.</p>

<p>This contest will end on March 28th 2005, so make sure you stop by.</p>

<p>For developers interested in having their own artwork contest please submit your request to <a href="https://mail.kde.org/mailman/listinfo/kde-contests">kde-contests</a>.</p>

