---
title: "Appeal Website Launched"
date:    2005-08-13
authors:
  - "aseigo"
slug:    appeal-website-launched
comments:
  - subject: "Greedy for it "
    date: 2005-08-13
    body: "KDE makes my computer fun to use, but its beauty makes it fun to show my computer to others.  Good luck.  "
    author: "Gerry"
  - subject: "Re: Greedy for it "
    date: 2005-08-14
    body: "Agreed! These things look very interesting!"
    author: "Knut"
  - subject: "Re: Greedy for it "
    date: 2005-08-15
    body: "But what about non-KDE applications? Applications like Inkscape, like Evolution. Usually there is not even a Icon theme for them and no bridge to apply it. And real Gnome applications look just ugly because they apply a different colour scheme."
    author: "Bert"
  - subject: "Re: Greedy for it "
    date: 2005-08-15
    body: "There are several things you can do to make other applications look better under KDE. With different degree of complexity.\n\nThe easiest one are the color issue, in the color theme module select the option \"Apply colors to non-KDE applications\". Most applications will follow this, unless they have hardcoded colors or are broken in other ways.\n\nThe second step are using the GTK-Qt theme engine, this makes GTK2 programs use Qt/KDE widget styles. This makes dynamically linked GTK programs use the same style as your KDE programs, mainly all Gnome and most GTK applications.\n\nThe third one with icons are little harder, but doable if the programs follow the fdo.org icon specification. If no one has done it yet, you have to pack it up yourself. But it's perfectly doable and I doubt it is necessary to make new icons as you can use the ones already provided by KDE, they are more numerous than the stock Gnome iconset. You only have to match the correct KDE icons to the Gnome ones. "
    author: "Morty"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "would be a good project for Appeal to automize this.\n\nAt least everybody at KDE artist seems to work with inkscape."
    author: "aile"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "Most of this is already there. The only big missing piece are the KDE icon theme for Gnome, and I think making it are more a job for the Gnome side. Or a colabrating task for the -Look comunity. Really no need to involve Appeal at all, it's even doable before the 3.5 release. \n\nAs for putting much work into the look and feel of non KDE programs, I don't think it should be a very high priority. As the quality and diversity of KDE applications increases, the need for it becomes smaller and smaller. One of the few remaining areas where the native KDE solution are lagging, are the mentioned inkscape. Hopefully Karbon can get some help from the momentum of Krita. "
    author: "Morty"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "\"KDE native\" is funny. Inkscape is a Java application. I do not see a reason why GTK application should not integrate into the KDE desktop, despite Gnome fanatics. Linux/KDE is no QT only environment. \n\nI once saw Abiword, Evolution and another app with a Crystal Icon theme and it looked almost like a KDE app. It is important for an \"appeal\" of KDE that application which users run do not look alien. \n\nOpenOffice, inscape etc. are examples.\n\n\nOther issues: how to test the \"completeness\" of an Icon theme?"
    author: "aile"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "> Inkscape is a Java application.\n\nGood joke."
    author: "Anonymous"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "Why you think KDE native is funny I can't really say, but by being native you get all the added benefits of the KDE environment. Not that it's any reason for not making GTK or other apps look good, but I don't see reasons to invest much work in it. When in most cases the native solutions are just as good, or better than the non-native ones. BTW Incscape is a GTK application written in C++ using gtkmm. \n\nMaking OpenOffice look and behave good with KDE are already in the works and being paid for by Suse. And your second example of Inscape, I already explained you only have to make the icon theme by copying and renaming some already existing icons. \n\nTesting the \"completeness\" of an icon theme are easy, you compare the filenames of the icons in the standard set to the ones in your new icon theme. When you have the same number of icons with the same names, it's complete."
    author: "Morty"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "KDE will use your GNOME menu entries and icons provided that they are in:\n\n        $KDEDIR/share\n\nI have GNOME and KDE installed in different directories so I added links to create a common \"share\" directory: \"/usr/share\""
    author: "James Richard Tyrer"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "That's rather what he don't want:-) The thing is he don't want Gnome menu entries and applications to use Gnome icons, he wants them using KDE style icons. \n\nSince you obviously have bothered to dig into the icon specs, perhaps you can comment on this. I'll use Inkscape as the example here too. If you have $KDEDIR/share/inkscape, symlink or otherwise. One brute force approach to make it use KDE icons are to replace any icons in that dir with a KDE one. Is that all that's needed?"
    author: "Morty"
  - subject: "Re: Greedy for it "
    date: 2005-08-16
    body: "Perhaps I didn't quit understand the question -- perhaps I didn't wake up yet. :-)  But the information is true and explains how this is integrated.  Obviously, having an icon in the menu is preferable to having none.\n\nThe only way to have different icons is to have different icons.  With InkScape, how would you make a Crystal style icon?  I have a set of HiColor (includes SVG) if anyone wants them:\n\nhttp://home.earthlink.net/~tyrerj/kde/pics/InkScape-HiColor.tar.bz2\n\nPerhaps making them blue would be sufficient. :-D\n\nActually, for the menu icons you would put the KDE icons with the same name as the GNOME icons in the correct KDE icon directory and these will be used in preference to the GNOME ones.  \n\nI believe that a CrystalSVG icon theme for GNOME does exist.\n\nhttp://www.crystalgnome.org/icons.htm\n\n"
    author: "James Richard Tyrer"
  - subject: "Too much noise about vapour ware!"
    date: 2005-08-13
    body: "Please, you are overdoing. KDE, Free Software in general, may have too less marketing. But still it did evolve. Perhaps just because it did not make false promises but tell \"Use on your own risk, may not work.\" And works even better than most proprietary one does. That is where the trust comes from.\n\nBut you are creating a lot of expectations. Without having things to show. Only plans. I can only strongly advise you to better stay with the traditional attitude, good old understatement. If you want to tell people how fine KDE is show what you have, not what you want to have.\n\nGood luck."
    author: "Old time KDE user"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-13
    body: "> If you want to tell people how fine KDE is show what you have\n\nThis is not about current KDE but about what is intended to start with KDE 4[.0].\n\nSome people develop a vision for KDE and you complain about vaporware. *sigh*"
    author: "Anonymous"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-13
    body: "Do you think it's better developing towards a target, aiming a common direction, or going random?\nHaving this creates stronger community, aligned views, better consistancy for the user and highen the average of available software.\nLong life appeal! :-)\n\nP.S: link plasma to plasma.kde.org and say it's a part of the global kde appeal project."
    author: "jumpy"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-13
    body: "> link plasma to plasma.kde.org and say\n> it's a part of the global kde appeal project.\n\noops, you're right: it was linking to the old address (plasma.bddf.ca) from the appeal site. and i've added a note in the footer of the plasma site as well. thanks for pointing these things out =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-13
    body: "Appeal is not a marketing effort, it is a social experiment in consciously engaging in coordinated, open and multidisciplinary development.\n\nsome people seem to disdain vision, perhaps because they've seen other people abuse it as a cheap marketing gimmick instead of being a means to describe the path one is on to others also involved in the same struggles.\n\nsome of the things i, and others, are involved in require coordinated efforts between a large number of people across a large number of projects. you don't achieve that by not engaging in conversation about it or by allowing misconceptions and ill communication drive things. if a team would developer around konqueror and clearly state the mission and the direction they have set themselves upon maybe the interface issues would dissolve and be addressed for KDE4. to date, konqueror's interface is a jumble specifically because no global vision was applied to it.\n\nsee, you are suggesting building skyscrapers without the building crew talking to each other or even having a blueprint by which to follow. you might be able to build a toolshed that way, but not an office building.\n\nyou are right that we have gotten this far with our current techniques of randomness and secrecy. but we started hitting the ceiling of what that was capable of producing when it comes to integrated desktop environments a year or three ago. we need to break through to the next level and that in part requires doing certain things a bit differently.\n\nlike having, expressing and engaging in statements of vision.\n\nyou are correct that at the end of the day one needs to be walking the walk. or are you suggesting that i'm sitting here churning out words only?"
    author: "Aaron J. Seigo"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-13
    body: "amen."
    author: "superstoned"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-14
    body: "I think it would be useful to have:\n\n1) a mailing list:  kde-appeal@kde.org and http://lists.kde.org/?l=kde-appeal\n2) an IRC channel: #kde-appeal\n\nMaybe the same for plasma?\n\n kde-plasma@kde.org\n#kde-plasma\n\nWhat you think =)\n\nAt least a mailing list would avoid the noise made by other unrelated topics...\nand also kde-core-devel is not accepting input from KDE users =(\n\n\n"
    author: "fp26"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-14
    body: "appeal already has a mailing list and an irc channel.\nplasma already has a mailing list and an irc channel.\n\nhuzzah! =)"
    author: "Aaron J. Seigo"
  - subject: "Only one word..."
    date: 2005-08-15
    body: "WHERE? WHERE? WHERE!!!!!! =P\n\nFOR GOD SAKE WHERE!!!!!!!!!!!!!!!!!!!!!!!!!!"
    author: "fp26"
  - subject: "Re: Only one word..."
    date: 2005-08-15
    body: "Well I followed the links to the web site and clicked \"Contact\"... http://appeal.kde.org/wiki/Contact"
    author: "Chris Howells"
  - subject: "Re: Only one word..."
    date: 2005-08-15
    body: "Plasma's Developer: Get Involved Page is here:\n\n    http://plasma.kde.org/cms/1086\n\nthe Appeal contact page is here:\n\n    http://appeal.kde.org/wiki/Contact\n\nseeing as both mailing lists are pretty well stocked with subscribers, i think they are decently easy to find.\n\nsee you in #appeal and #plasma =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Only one word..."
    date: 2005-08-16
    body: "I see but why they are not listed \nin the main KDE list of KDE mailing list\n\nhttp://lists.kde.org/?w=2\n\n"
    author: "fp26"
  - subject: "Re: Only one word..."
    date: 2005-08-16
    body: "That's a list of the lists archived there (btw an external site) and no list of existing lists."
    author: "Anonymous"
  - subject: "Re: Too much noise about vapour ware!"
    date: 2005-08-14
    body: "But you really read the site? And you still didn't notice those tech-demo or video demos the do provide already?\n\nWell, read again.\n\nHowever, I *do* really like the page's design :)\nAnd yeah, good luck ;)\nChristian."
    author: "Christian Parpart"
  - subject: "Too bad...."
    date: 2005-08-13
    body: "that it doesn't render properly in konqueror. (The \"Create an account or log in\" text doesn't fit on the border at the top. I know, bugreport it, just thought it slightly amusing that a kde project website doesn't work properly in the kde browser)"
    author: "mikeyd"
  - subject: "Re: Too bad...."
    date: 2005-08-13
    body: "odd. it works here, but i'm using trunk/ (what will be 3.5) and have decent fonts installed (not sure which if either of those things makes a difference)"
    author: "Aaron J. Seigo"
  - subject: "Re: Too bad...."
    date: 2005-08-13
    body: "My fonts are pretty decent but I'm still on 3.4. It could well be a khtml flaw that has been fixed, the engine's being improved all the time. I also have a rather large resolution if that's likely to make any difference (if the border's a relative size and the font has a point size or something, that could well be it)"
    author: "mikeyd"
  - subject: "Re: Too bad...."
    date: 2005-08-13
    body: "Tried it on both soon to be 3.5 and good old 3.2.3, and they look to behave similar. It's all dependent on font and fontsize. When the fonts get bigger the border does not resize, so I'd guess some peoples default will show up wrong:-) The border at the bottom does behave correctly. Try hitting \"Enlarge font\" a few time and you'll see. "
    author: "Morty"
  - subject: "Tenor licensing, Version control"
    date: 2005-08-13
    body: "All I can say is WOW! The website has a treasure trove of ideas, and the fact that you're brainstorming at this stage will surely lead to a unified field for KDE4.\n\nI noticed that all the source files for Tenor are licensed LGPL \u0096 I think licensing it GPL would better ensure that your code is not linked to non-free, closed source, proprietary software. You can always change from GPL to LGPL later, but going the other way around would be a lot harder. Plus, you (as the authors) will be in a stronger position to financially negotiate with interested closed-source clients :)\n\nReading the section Hide The File System::Transparent Version Control, it struck me that Hans Reiser (lead developer of the ReiserFS filesystem) was thinking along exactly the same lines. Here's part of what he had to say: \n\n\"\"\"\nVersion control definitely belongs in the filesystem. Filesystems manage files, and that should include managing file versions. Our support for transactions and compression should make it easier to implement version control in Reiser4.\n...three things [need to be] changed simultaneously: 1) it was integrated into the filesystem, 2) it was free, 3) it was easy to understand for average users. It should also be as well integrated into apps as version control is in MS Word.\n\"\"\"\n(the whole interview is at http://interviews.slashdot.org/article.pl?sid=03/06/18/1516239)\n\nI think the optimal solution (in terms of least code duplication, fastest performance and earliest release date) would be for KDE to handle the app integration for version control (mostly GUI stuff) and let ReiserFS do the actual file managing."
    author: "Vlad C."
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-13
    body: "> I think licensing it GPL would better ensure that your code is not linked to non-free, closed source, proprietary software.\n\nWho says that KDE wants to prevent that? And for entering kdelibs it has to be LGPL according to KDE's license policy."
    author: "Anonymous"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "How can kdelibs be LGPL?  Since QT is GPL (w/o buying a license), and kdelibs links against it wouldn't that mean that kdelibs has to be GPL?  Or does it mean that it acts as if its GPL, _unless_ you have a qt license (and in that case it acts like LGPL)?"
    author: "Corbin"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "That's just successful GNOME FUD that everything linked against Qt GPL has to be GPL.  Your code can be BSD, GPL and still link against GPL Qt, and that's not even counting the additional licenses you can use with QPL Qt."
    author: "KDE User"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "That's not GNOME FUD .. it is just *plain* FUD. No reason to say it comes from the GNOME camp. \n\n\nFab"
    author: "Fab"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "> How can kdelibs be LGPL? Since QT is GPL (w/o buying a license), \n> and kdelibs links against it wouldn't that mean that kdelibs has to be GPL?\n\nNo, it wouldn't.  The GPL and the LGPL are compatible, and that's enough.  \n\n\nNote:  OTOH, the fact that kdelibs is LGPL does *not* offer a way to circumvent the GPL license of Qt and to create proprietary KDE software.  For that there is the combination \"proprietarily-licensed Qt (buy one) + LGPLed kdelibs + your proprietary code\".  That case is also perfectly alright from a legal POV. \n\n"
    author: "cm"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "You can also look at http://www.gnu.org/licenses/license-list.html for GPL-compatible licenses.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-15
    body: "Qt/X11 is also licensed under QPL which is not as strict as the GPL. It mostly specifies that your program must be open source, which is the case for the LGPL."
    author: "Philippe Fremy"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-13
    body: ">I think the optimal solution (in terms of least code duplication, fastest >performance and earliest release date) would be for KDE to handle the app >integration for version control (mostly GUI stuff) and let ReiserFS do the >actual file managing.\n\nKDE wants to work with most/all Linux/UNIX filesystems."
    author: "mihnea"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-13
    body: "KDE can use a database (mysql whatever) when there is no advanced filesystem."
    author: "superstoned"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-13
    body: ">KDE wants to work with most/all Linux/UNIX filesystems.\n\nI hope that's not at the expense of desktop performance or user experience. Trying to be all things to all systems (at least from the beginning) will invariably bog KDE down and make it less appealing than proprietary systems like M$ Windows and Apple OS X. Please read Hans Reiser's response to a similar comment by Linux Torvalds:\n\nhttp://www.ussg.iu.edu/hypermail/linux/kernel/0409.0/0448.html\n\nThe key is to get a task to work outstandingly with a particular combination of tools and then gradually expand support for less-capable filesystems and OS's. I think this strategy would work well because KDE's version control development can be split into two relatively independent components: 1) the backend that manages file version archiving and 2) the frontend that provides GUI access to the archives from KDE apps.\n\nI suggest that KDE development initially focus on a frontend (#2) that interoperates with ReiserFS 4's version control system. Once that combination is out, it can act as a proof of concept (and a functional one for Linux/ReiserFS users ;-) \n\nIf, after that, there is still a market for version control outside of Linux/ReiserFS, KDE development can proceed to focus on a FS-independent archive backend (#1). I suspect the creating the backend will be harder than the frontend, so it would be best to let filesystem developers iron out the details first.\n\nPlease note that finishing the KDE frontend first will likely *not* interfere with with subsequent development of the backend \u0096 if anything, it will speed it up because the backend will then know exactly what the frontend expects from it."
    author: "Vlad C."
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "What I think you fail to understand (not necessarily true) is that ReiserFS 4 is not going to be the following things:\n\n1. Available to the common public. I am not aware of any distribution stock kernel that supports it yet. It is not in the Pengui Pee Kernel, is it in -mm kernel, I think not?\n\n2. Ready before KDE. Because of 1, KDE 4 is going to be in the hands of people earlier than Reiser 4. This is chicken and egg problem. Reiser 4 will not be in distributions because no killer app needs it.\n\n3. Acceptable requirement. If you separate front end back end, why start or focus on a back end that is so rare? You will need another performant backend anyway. In no way is it going to be that the FreeBSD or Windows users of KDE4 will not get to use this. \n\n4. Instead, with Tenor, people are going to able to use FUSE (something that is being merged in the Linux kernel and going to present in distributions soon) to mount Tenor KIO slaves. \n\n5. ReiserFS 4 is the wrong approach. Actually FUSE is the better approach. FUSE is a plugin interface for filesystems, whereas with ReiserFS 4 you must follow Namesys in Code (specific to Linux), Storage Format (not the same as ext2 and so my data is dramatically less safe) and plugins. When all you want is probably the plugin power.\n\nIn summary, I would say, it is not necessary to solve the problem anywhere near the kernel. Instead, KDE can solve the problem completely inside the framework and then we use already available means (FUSE-KIO) to provide it to the lower level tools.\n\nThat removes a dependency from the KDE perspective. One that would otherwise slow things down. Reiser 4 as the solution to metadata problems has been vaporware too longer already...\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "<i>5. ReiserFS 4 is the wrong approach.</i>\n\nActually, it's not. It is far better, and faster, to have a capable very capable filesystem that can do more. If you look at what a filesystem stores and what people want to do with these new metadata systems the goal are one and the same. Andrew Morton is very, very wrong when he talks about a filesystem doing simple stuff and then layering crap on the top. It is simply logical to get the flesystem to do the work it is good at.\n\nHowever, only Be has had the ability to integrate things totally like this. Every other OS has had to compromise in some way. Not even Microsoft could manage it. They layered WinFS on top of NTFS and decided not only that the performance was crap but that it just wasn't going to work, and canned it."
    author: "Segedunum"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "Hello,\n\nthe thing about a \"capable very capable\" filesystem is that it introduces things where they may not belong to. You can see that from that Linux has a VFS (virtual filesystem) layer and then ReiserFS has all the same and more only level deeper again.\n\nFor what exactly? For having plugins. Overall ReiserFS 4 is an abstraction of the internal Linux VFS and it will make it cheap to develop filesystem plugins that do all kinds of arcance things. That is great for Namesys and I deeply admire the achievement.\n\nBut abstracting something virtual... well. Don't blame Andrew for not thinking of that as good design. It happens frequently with these kinds of patches that they have to go through some changes. Like e.g. the VFS should do more of what Reiser4 does. That way, every filesystem stands to gain.\n\nThe possible performance hit of doing what Reiser4 as via FUSE, I don't buy into it at all. The metadata stuff is going to be housekeeping about lots of small things. Like the Email address you received something from, rather than the 1MB attachment itself. You will hardly have lots of big files in that.\n\nThe metadata is fairly constant too and the most important thing is: In user space, with e.g. using a relational or object database, or your own file format and so on, you can optimize your data layout according to your application needs. \n\nThat is going to be most critical to performance. Not the question of \"find metadata of somefile in somefile/\" or some other means.\n\nThe thing about Linux is that \"mv somefile othername\" may kill the metadata connection. Unless of course I am doing it through KDE. Or in some directory that is a mount of FUSE.\n\nFor my own documents, I may use /home/kay/Documents being a mount of my home through a KIO slave like home:/\n\nThat said, I fully trust things to work out well. And, the thing is, KDE 4 will be an Alpha/Beta relatively quick after 3.5 release, likely with Tenor and usable now on many systems beyond Linux. And probably performant too.\n\nThe metadata thing of BeOS was not like Tenor, was it? It was more like, if every mp3 file in the filesystem had its id3 tags as searchable attributes. But saving the mp3 from an email, would you know where it came from?\n\nThe one thing about Tenor is not the technics of how things are stored. What is stored, why, and how can it be searched without me explaining ever much to the system. Every tag that I have to provide (except file name) is not going to work...\n\nYours, Kay\n\n\n"
    author: "Debian User"
  - subject: "I think you're missing the point..."
    date: 2005-08-15
    body: "that is an FS are basically weak a DBMS.  Reiser4 merely embraces this and it's Andrew who is too short sighted in this situation to understand that.  \n\nReiser4's brilliant design speaks volumes about how poorly things are done elsewhere, if you've looked at the architecture and realise how natural it is, you'll soon find yourself seeing the current state of affairs as poor, in light of it.\n\nOf course, Linux has little to do with meritocracy."
    author: "Saem"
  - subject: "Re: I think you're missing the point..."
    date: 2005-08-16
    body: "Hello,\n\nwell, well. Reading the documents about Reiser4, can you hint me at anything specific that makes ReiserFS more close to an DBMS than say ext3?\n\nAnd regarding the merit. ReiserFS was the first journaling file system on Linux (2.4.1 if memory serves right, ext3 made it only in at 2.4.15, that's one virtual machine code change later). Indeed, an achievement.\n\nBut that doesn't make everything right. In particular, Hans took the VFS like it was, saw the shortcomings... and worked around them. I fully understand those, who don't see themselves in charge of any one particular filesystem, but the VFS, to rather solve existing problems in VFS.\n\nThat said, sooner or later, I predict that Reiser 4 is going to be merged. It is just not going to be a \"eat my code\" thing.\n\nYours,\nKay\n\n"
    author: "Debian User"
  - subject: "Re: I think you're missing the point..."
    date: 2005-08-16
    body: "Reiser4 doesn't expose the functionality, but it can roll forward and back changes, on the data itself, not just keeping a meta data journal.  There is more, but I can't recall at this point in time.\n\nYour argument about VFS goes with the assumption that the VFS is the right way of doing things, I don't think it is, I think Reiser4's design illustrates that because he seems to solve the entire problem much more cleanly, not to mention transparently and in a performant fashion."
    author: "Saem"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-15
    body: "two things about file system approaches:\n\n- the most interesting data available on the desktop for contextual linkage does not exist in the filesystem. you have to add it.\n\n- filesystems are not portable. they probably don't be within the next decade, either. KDE is portable. highly portable. this means filesystem specific approaches are not feasible.\n\nnow, once we've got a production ready system together... if someone wishes to write a Tenor backend that targets $COOL_FILESYSTEM, by all means have at it =) i think it would be pretty cool."
    author: "Aaron J. Seigo"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-16
    body: "Like I said - you don't really ever get what seems to be the ideal solution, not in any OS set up ;-). what you've described sounds like the best approach possible to me."
    author: "Segedunum"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-14
    body: "Since ReiserFS 4 and FUSE are totally different things, comparing them are rather pointless. FUSE are more or less useless in this context, FUSE is simply put OS equivalents to KIO-Slaves. They are simple userspace filesystems. Using them for metadatastorage they are no different than storing the metadata in a standard database on top the filesystem, while the more efficient approach of ReiserFS you do it in the filesystem. \n\nBesides why you would you want to use FUSE, when the platform independent way of doing it already are to use only KIO-Slaves. Tying it to a single platform technology like FUSE makes little sense. And using KIO-FUSE in KDE makes absolutely no sense since you can use the KIO-Slave directly. KIO-FUSE are only a way for non KDE applications to use KIO-Slaves through FUSE. "
    author: "Morty"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-15
    body: "Hi,\n\nI totally agree with that FUSE should not be a means to implement anything.\n\nI just wanted to point out that rather than requiring ReiserFS to store the metadata, it indeed would make much more sense, to make the KDE metadata engine available through a KIO-FUSE mount.\n\nIn the ReiserFS approach, Reiser4 becomes a dependency to Tenor.\n\nIn the other, which will be really done, KDE uses whatever it finds best suited to the job and creates a pseudo-filesystem to publish the metadata along with the files.\n\nThat way the circle will be closed. Solving too much inside the filesystem is bound for trouble.\n\nYours, Kay"
    author: "Debian User"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-16
    body: "Happy to work with anyone who wants to add version control to Reiser4, I do indeed think it would be easier to do it in reiser4 than in other filesystems......"
    author: "Hans Reiser"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-16
    body: "\"Happy to work with anyone who wants to add version control to Reiser4, I do indeed think it would be easier to do it in reiser4 than in other filesystems......\"\n\nThank you very much for being open to cooperation with KDE in implementing file version control. \n\nIt remains to be seen whether KDE developers will opt for a completely filesystem-independent approach (which might have the drawback of not being transparent/portable to non-KDE apps), or opt for integration with ReiserFS 4, which would have a better chance of providing all apps on the system with access to file version data."
    author: "Vlad C."
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-16
    body: "\"It remains to be seen whether KDE developers will opt for a completely filesystem-independent approach , or opt for integration with ReiserFS 4.\"\n\nI'd assume they'll let the backend decide that, i.e. offer some database backend for systems/partitions where no ReiserFS 4 is available and a ReiserFS 4 backend where it is."
    author: "ac"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-16
    body: "well, i'm afraid the kde dev's might not be up to writing this and the other things tenor will need as plugins for reiser4. however, maybe linspire or suse will sponsor such an effort, as they (want to) include reiser4.\n\nbut first i think reiser4 must get into the kernel, and as long as reiser4 duplicates and extends the current VFS so much, this might take some time (or some feature-cutting from reiser4). these innovations should first be merged into the kernel, then the filesystem can follow, then tenor can use it.\n\nbut KDE4 is still at least a year (likely more) away... so we'll see."
    author: "superstoned"
  - subject: "Re: Tenor licensing, Version control"
    date: 2005-08-16
    body: "btw i'd love to see this sponsorship, as i believe doing tenor the reiser4 way is The right thing to do (TM), and the most performant way. also, i think we'll be relying on this metadata more than we can ever imagine, and a database gets easilly corrupted. if i can't find my files because my database borked for the sixth time this year... i'd rather see it safe in an atomic filesystem!"
    author: "superstoned"
  - subject: "www.google.com/linux"
    date: 2005-08-13
    body: "Why is google linux not listed as one of the selectable\nsearch engines.  Url is: http://www.google.com/linux"
    author: "RB"
  - subject: "Re: www.google.com/linux"
    date: 2005-09-08
    body: "just hello"
    author: "yadollah"
  - subject: "Re: www.google.com/linux"
    date: 2007-05-01
    body: "what is linux\ncan\"t you me explination about that\n"
    author: "krishna"
  - subject: "Tenor vs. kat"
    date: 2005-08-13
    body: "So should the kat <https://infserver.unibz.it/kat/> be considered usable only with kde3 and that tenor is going to be the default desktop search engine for the kde4, and virtually obsoletes kat?"
    author: "Nobody"
  - subject: "Re: Tenor vs. kat"
    date: 2005-08-13
    body: "This might answer your question :-)\n\nhttp://aseigo.blogspot.com/2005/08/akademy-callinng-all-ohioans-kat.html\n"
    author: "Torsten Rahn"
  - subject: "Re: Tenor vs. kat"
    date: 2005-08-13
    body: "Thanks, it sure did... :-D"
    author: "Nobody"
  - subject: "Re: Tenor vs. kat"
    date: 2005-08-13
    body: "Kat might develop into tenor, as the upcoming 'tenor-team' will in part consist off members of the current kat-developers. they are working on the api and the capabilities, and as kat is already a nice application, it is more likely the tenor developers team up with them, instead of simply throwing all the work away."
    author: "superstoned"
  - subject: "Re: Tenor and kat"
    date: 2005-08-13
    body: "Tenor is a framework and Kat is an application which might use the Tenor framework one day -- I see no conflicts there :-)"
    author: "Torsten Rahn"
  - subject: "Re: Tenor and kat"
    date: 2005-08-13
    body: "kat also has some framework aspects to it (the file system indexer, full text extraction..), and we're working together to take advantage of the work that has already been done. the kat framework parts are being improved upon and we'll be adding the contextual linkage aspects on to it."
    author: "Aaron J. Seigo"
  - subject: "Re: Tenor and kat"
    date: 2005-08-14
    body: "kat framework will be enough evolved when, looking at the properties tab of a file that was moved many times into my hd, it will say \"downloaded from the internet ($URL) 16 days ago\" or \"your friend sue gave sent this to you. track all the other files sent by her\".. fanta-framework?\n"
    author: "jumpy"
  - subject: "Re: Tenor and kat"
    date: 2005-08-14
    body: "you've just described the contextual linkage side of it that Tenor is concentrating on. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Tenor and kat"
    date: 2005-08-14
    body: "Holy **** dude. I know that's what you mean when you talk about a contextual linkage system, but when it gets described to you it sounds wonderful. I really hope it can be achieved and you don't get bogged down.\n\nThe strange thing is, Microsoft and Apple are spending billions in research on this stuff and they're no closer to having a working solution - probably due in part to some historical reasons and the existing software they're working with. The open source world seems to be making great strides in this area, on much less of a budget (:-)) and could really take the lead in this area."
    author: "Segedunum"
  - subject: "Exciting News, but needs a front-page KDE.org link"
    date: 2005-08-14
    body: "I am really excited about the development of the Appeal project.  It is a great thing to see such a level of organized collaboration within the KDE community.  As the process develops further I hope to see a page off of the Appeal website that gives a list of tasks or small jobs that need attention.  Also, I would like to see on the KDE.org main page under the KDE Projects a link to the Appeal website.  Thanks to all the developers working on this as a way to improve the KDE experience.   "
    author: "Jonathan Lee"
---
After a few months of quiet activity, the Appeal desktop project has rolled out a <a href="http://appeal.kde.org/">new project website</a> that documents what <a href="http://appeal.kde.org/wiki/People">everyone</a> has <a href="http://appeal.kde.org/wiki/Projects">been up to</a> and <a href="http://appeal.kde.org/wiki/Concepts">discussing</a>. Since the <a href="http://dot.kde.org/1112736422/">the last public announcement</a> many things have occurred, including another Appeal meeting being held in Germany. The project is looking forward to sharing its experiences and gathering input from the general KDE developer community at <a href="http://conference2005.kde.org/">aKademy 2005</a> later this month.




<!--break-->
