---
title: "KDE 3.4 Beta 2 \"Keinstein\" is Waiting for You"
date:    2005-02-10
authors:
  - "skulow"
slug:    kde-34-beta-2-keinstein-waiting-you
comments:
  - subject: "No rock och K Einstein?"
    date: 2005-02-09
    body: "Fun name. :)"
    author: "Chakie"
  - subject: "Re: No rock och K Einstein?"
    date: 2005-02-09
    body: "Years ago, when I upgraded to a K6 I renamed my old 386 machine to \"keinstein\"  ;-)\n"
    author: "cm"
  - subject: "Livecd anyone?"
    date: 2005-02-09
    body: "I am willing to test it, but I don't want to burn my cpu compiling it :)\nDo you know if some live cd is/will be out with beta2?"
    author: "Anonymous"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-09
    body: "Likely there will be at least one in the next one or two days."
    author: "Anonymous"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-09
    body: "Yes, a LiveCD would rock!\nI think onebase Linux had a KDE Beta Live-CD before.\n\nThat's probably the best way for \"occasional testers\" to have a look and report bugs.\nThough I'm running SuSE at the moment (downloading and) installing KDE rpm by rpm can be a pain on this distribution from my experience.\n\nAnd since there is no apt4rpm repository (or did I miss something?) and no debs/repository at all for Debian/(K)Ubuntu I'd definitely give a Live-CD a try."
    author: "Christoph Wiesen"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-09
    body: "See:\n\nhttp://linux01.gwdg.de/apt4rpm/\n\nI have kept my SuSE 9.1 install bleeding edge through this.\n\nSearch around on this page for components (hint... there's a kde and a kde-unstable component):\n\nhttp://linux01.gwdg.de/apt4rpm/home.html"
    author: "ltmon"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-10
    body: "Of course - used that before. The age... :-/\nThanks for the info!\n\nSo for anyone with SuSE 9.1 or 9.2 (following info is for 9.2) it's really easy to get KDE Beta 2 via apt:\n\nFollow Guru's easy-three-steps how to install apt4rpm:\nhttp://linux01.gwdg.de/~pbleser/article/install_apt4rpm.php\n\nThen add the following line to /etc/apt/sources.list:\nrpm ftp://ftp.gwdg.de/pub/linux/suse/apt/ SuSE/9.2-i386 kde-unstable kde\n\nYou need both \"kde-unstable\" and \"kde\" because \"kde\" contains the current QT version (3.3.4) needed by KDE 3.4 Beta2.\n\nThen do \"apt-get update\" and install whatever KDE components you want.\nI've issued the command \"apt-get install kdebase kdepim kdelibs qt3\" and it will fetch 65MB for now - that's ok even on dial-up (no DSL where I live...).\n\n\nP.S. It *might* be that you need more components inside your sources.list if you want to fetch some more KDE packages with recent dependencies, so just in case here's what my sources.list contains:\n\nrpm ftp://ftp.gwdg.de/pub/linux/suse/apt/ SuSE/9.2-i386 update security base suser-guru kde kde-unstable"
    author: "Christoph Wiesen"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-10
    body: "Just add ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_9.2/yast-source as a new package source in Yast, start the Yast Software Installation, user Filter:Package Groups, select group \"zzz\", then choose from the menu Package->All in this list->Update, if newer version exists."
    author: "Lurchi"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-10
    body: "Well, people obviously have different feelings about what\nis \"a pain\".\nIn addition to the ways mentioned below I prefer to do\nit the manual way. It's basically a matter of taste:\n\n1) copies all files + 1 internationalization file from\nthe FTP-folder\n2) Run rpm -Uvh --replacefiles --replacekpkgs *.rpm in that folder\n\nSome novice Linux users I know thought it was a pain because they tried to\ninstall those packages one-by-one and ended up in dependency hell.\nIf you use \"*\" rpm will sort this out for you.\nInterestingly, you dont really need to shutdown KDE. Just do it in an open terminal. I've never had any major problems. Close all important documents, though and reboot immediately afterwards.\nAdvantage of doing it in KDE is: You can have YasT running and quickly\ninstall all missing packages (if there are any new dependencies).\nJust put YaST and the terminal window side by side.\nDon't find that very complicated -  just copy files and enter command \nbut YMMV.\n\n\n\n"
    author: "Martin"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-10
    body: "Having used Debian based distributions for the last 1 1/2 year, I'd say that, yes - what you describe is a pain for me and exactly what I meant.\nFetiching _all_ rpms just to be sure that nothin's missing is annoying. Sorting out all the *-devel packages during download is as well.\n\nI still think nothing beats apt - not even in SuSE."
    author: "Christoph Wiesen"
  - subject: "Re: Livecd anyone?"
    date: 2005-02-11
    body: "You may try if <a href=\"http://ktown.kde.org/~binner/klax/\">Klax</a> works for you.\n"
    author: "binner"
  - subject: "upgrading from beta1"
    date: 2005-02-09
    body: "is it safer to  \"make uninstall\" kdebeta1 before or is ok to just install on top of it?"
    author: "Pat"
  - subject: "Re: upgrading from beta1"
    date: 2005-02-09
    body: "In my experience you can get weird bugs by installing over a previous install (e.g. you might get duplicate desktop files in different places) but your mileage might vary."
    author: "ac"
  - subject: "kdnssd doesn't compile anymore"
    date: 2005-02-09
    body: "while it compiles just fine on beta1 I get this with beta2:\n\nMaking all in ioslave\nmake[1]: Entering directory `/home/pat/kdebeta3.4/beta2/kdenetwork-3.3.92/kdnssd/ioslave'\n/usr/share/qt3/bin/moc ./dnssd.h -o dnssd.moc\nif /bin/sh ../../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I../.. -I/usr/include/kde -I/usr/include/qt3 -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT -D_FILE_OFFSET_BITS=64  -Wnon-virtual-dtor -Wno-long-long -Wundef -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -Wchar-subscripts -Wall -W -Wpointer-arith -Wwrite-strings -O2 -Wformat-security -Wmissing-format-attribute -fno-exceptions -fno-check-new -fno-common -fno-exceptions -fno-check-new -fno-common -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_STL -DQT_NO_COMPAT -DQT_NO_TRANSLATION  -MT dnssd.lo -MD -MP -MF \".deps/dnssd.Tpo\" -c -o dnssd.lo dnssd.cpp; \\\nthen mv -f \".deps/dnssd.Tpo\" \".deps/dnssd.Plo\"; else rm -f \".deps/dnssd.Tpo\"; exit 1; fi\ndnssd.cpp: In member function `bool ZeroConfProtocol::dnssdOK()':\ndnssd.cpp:129: error: 'class DNSSD::ServiceBrowser' has no member named '\n   isAvailable'\ndnssd.cpp:130: error: `Stopped' is not a member of type `DNSSD::ServiceBrowser'\ndnssd.cpp:134: error: `Unsupported' is not a member of type `\n   DNSSD::ServiceBrowser'\ndnssd.cpp: In member function `void\n   ZeroConfProtocol::newType(KSharedPtr<DNSSD::RemoteService>)':\ndnssd.cpp:322: error: `browsedDomains' undeclared (first use this function)\ndnssd.cpp:322: error: (Each undeclared identifier is reported only once for\n   each function it appears in.)\nmake[1]: *** [dnssd.lo] Error 1\nmake[1]: Leaving directory `/home/pat/kdebeta3.4/beta2/kdenetwork-3.3.92/kdnssd/ioslave'\nmake: *** [all-recursive] Error 1\n\nanyone? ( i use mDNSResponder-87)"
    author: "Pat"
  - subject: "Re: kdnssd doesn't compile anymore"
    date: 2005-02-09
    body: "It seems you have kdelibs from beta1 installed. Those functions your compiler is complaining about was added between beta1 and beta2. Btw: how do you compile beta2? By hand or using konstruct?"
    author: "Jakub Stachowski"
  - subject: "Re: kdnssd doesn't compile anymore"
    date: 2005-02-09
    body: "by hand on debian sid, its easy."
    author: "Pat"
  - subject: "Re: kdnssd doesn't compile anymore"
    date: 2005-02-10
    body: "So got it to compile?"
    author: "Jakub Stachowski"
  - subject: "Re: kdnssd doesn't compile anymore"
    date: 2005-02-12
    body: "yes, got it compile. thanks :)"
    author: "Pat"
  - subject: "no debian binaries. :("
    date: 2005-02-09
    body: "I never expect them, but it's nice when there are some."
    author: "c"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-09
    body: "With Gentoo you'll never have this problem. :)"
    author: "ac"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-09
    body: "but with debian u don't need to wait hours for every package u want to install. i only do compile the kernel and kde by hand cause I care getting the latest. plus you can still apt-build on debian if you really want to compile everything. don't mean to start a debianVsGentoo war though :)"
    author: "Pat"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-10
    body: "As far as I know apt-build doesn't take the march/mcpu/etc options into account simply because no one ever completed apt-build to do that (I think it's unmaintained right now?!). So I can't see a point of compiling any package with apt-build as it will be just as fast as the binary.\n\nBut please enlighten me if that has changed, as I am using Debian and would love to see it work. "
    author: "mo"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-11
    body: "if someone is using Gentoo for speed reasons then they're using it for the wrong reasons.\n"
    author: "mabinogi"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-12
    body: "dpkg-reconfigure apt-build\nYou can select architecture, -Ox and any aditional flags for gcc a make."
    author: "Petr Balas"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-12
    body: "I know you can do that, but it is not taken into account when compiling. But then again, only if that hasn't changed recently"
    author: "mo"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-10
    body: "Why not? \n\nThe Beta sources are not in portage and Gentoo doesn't provide any binaries of it either."
    author: "Tom"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-10
    body: "The beta sources ARE in portage, beta1 and beta2, monolithic and split ebuilds :)"
    author: "TeXTer"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-12
    body: "It would be very cool if debian sid binaries were provided for KDE alphas. It would allow lots of people to test it without pain. This would mean more bug reports. As a sysadmin of my lab, I would be ready to install them on several computers.\n\nSteph"
    author: "St\u00e9phane"
  - subject: "Re: no debian binaries. :("
    date: 2005-02-17
    body: "i totally agree, is there somebody who is willing to make these binary packages ?"
    author: "\u00dcllar"
  - subject: "Re: no debian binaries. :("
    date: 2005-03-04
    body: "I've tried to build kde 3.4 rc1 into deb packages but there's a problem. KDE provides all we've needed to create packages but the version number are wrong, for exemple, arts is arts-1.2.92 or kdelibs is kdelibs-3.2.92 :(\nI've changed for arts but for kdelibs I don't see where I must change the number."
    author: "saintshakajin"
  - subject: "Re: no debian binaries. :("
    date: 2005-03-04
    body: "Ive found some rc1 packages, they can be downloaded there:\nhttp://pkg-kde.alioth.debian.org/kde-3.4.0/\n\nThey are not yet completed but kdegraphics, kdemultimedia and kdeaccessibility are. That means the new kpdf (which is really great) and text-to-speech\n\nSteph"
    author: "St\u00e9phane Magnenat"
  - subject: "Missing QT for Suse"
    date: 2005-02-10
    body: "qt3 >= 3.3.4 is needed by kdelibs3-3.3.92-1\n\nAbove says all :("
    author: "Iuri Fiedoruk"
  - subject: "Re: Missing QT for Suse"
    date: 2005-02-10
    body: "The README tells you where to get it.\n\n> Above says all :(\n\nYep, you didn't read the README."
    author: "Anonymous"
  - subject: "Re: Missing QT for Suse"
    date: 2005-02-10
    body: "This is some rpm dependancy. KDE 3.4 does NOT require qt3.3.4\nThere is no such thing in the README too.\nI'm compiling the kdelibs sources and everything is fine . (for now :) )"
    author: "joro"
  - subject: "Re: Missing QT for Suse"
    date: 2005-02-10
    body: "> There is no such thing in the README too.\n\nThere is, read again: ftp://ftp.kde.org/pub/kde/unstable/3.3.92/SuSE/README"
    author: "Anonymous"
  - subject: "Re: Missing QT for Suse"
    date: 2005-02-11
    body: "The Qt package has been on SUSE's download page since 1 Feb.  Having been well cheesed off by their Christmas kernel, I feel a need to mention this."
    author: "gerryg"
  - subject: "Compile error"
    date: 2005-02-10
    body: "test/kde3.4-beta2/lib -ltag -lkhtml -lartskde ../akode/lib/libakode.la\n/home/kde-test/kde3.4-beta2/lib/libartsflow.so: undefined reference to `g_assert_warning'\n/home/kde-test/kde3.4-beta2/lib/libartsflow.so: undefined reference to `g_return_if_fail_warning'\ncollect2: ld returned 1 exit status\nmake[5]: *** [juk] Error 1\nmake[5]: Leaving directory `/home/kde-test/konstruct/kde/kdemultimedia/work/kdemultimedia-3.3.92/juk'\nmake[4]: *** [all-recursive] Error 1\nmake[4]: Leaving directory `/home/kde-test/konstruct/kde/kdemultimedia/work/kdemultimedia-3.3.92/juk'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/home/kde-test/konstruct/kde/kdemultimedia/work/kdemultimedia-3.3.92'\nmake[2]: *** [all] Error 2\nmake[2]: Leaving directory `/home/kde-test/konstruct/kde/kdemultimedia/work/kdemultimedia-3.3.92'\nmake[1]: *** [build-work/kdemultimedia-3.3.92/Makefile] Error 2\nmake[1]: Leaving directory `/home/kde-test/konstruct/kde/kdemultimedia'\nmake: *** [dep-../../kde/kdemultimedia] Error 2\n\nAny help? It's a glib issue, I guess. LD_LIBRARY_PATH is set to ~/kde3.4-beta2/lib"
    author: "Eike Hein"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "Make sure you install the latest GNOME before installing KDE!\n\nAlternately, if you copy/paste the last command you can try adding /usr/lib/libglib.a to the end and compile GuK manually."
    author: "ac"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "Konstruct should fetch all required dependencies, including glib - and it has. It looks like the linker ends up using different files, but I don't get why. The environment variables *seem* to be set up correctly,"
    author: "Eike Hein"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "On what distribution is that? http://bugs.kde.org/show_bug.cgi?id=94975 describes the same problem for someone compiling k3b on a SUSE 8.2 updated to KDE 3.3.2."
    author: "Anonymous"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "Gentoo Linux - I did Google for the problem, but couldn't find an easy fix."
    author: "Eike Hein"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "Why are you using Konstruct on Gentoo instead ebuilds? That doesn't make much sense to me."
    author: "Anonymous"
  - subject: "Re: Compile error"
    date: 2005-02-11
    body: "Well, because Gentoo didn't have Beta 2 ebuilds at the time, also Konstruct allows me to very easily put everything in a test user account's home folder, self-contained."
    author: "Sho"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "Within kde/kdemultimedia/ append \" --enable-new-ldflags\" to the CONFIGURE_ARGS line and run \"make clean\"."
    author: "binner"
  - subject: "Re: Compile error"
    date: 2005-02-10
    body: "Thanks!"
    author: "Eike Hein"
  - subject: "Beta 2 and Probs"
    date: 2005-02-10
    body: "Hello,\n\ni've downloaded and installed KDE 3.4 Beta 2 on my SuSE 9.1 System over KDE 3.3, but i had only problems with it :( my keyboard does not work on KDE 3.4, so i couldn't use the kwallet to type in the password for kopete etc.. and KMail couldn't send any E-Mails because of an error.. that's all strange errors i didn't have with KDE 3.3..\n\nregards\n"
    author: "jiin"
  - subject: "Re: Beta 2 and Probs"
    date: 2005-02-10
    body: "Did you install their Qt 3.3.4 rpm?"
    author: "Anonymous"
  - subject: "Re: Beta 2 and Probs"
    date: 2005-02-10
    body: "oh, i've forgotten to read the readme before installing kde 3.4 beta 2, now it works :) thank You!\n\n"
    author: "jiin"
  - subject: "Re: Beta 2 and Probs"
    date: 2005-02-10
    body: "i'm running Suse 9.2 and my keyboard stopped working here to after installing the suse rpms. i suppose this problem is specific to the suse rpms?"
    author: "ac"
  - subject: "Re: Beta 2 and Probs"
    date: 2005-02-10
    body: "The SUSE README has been updated with:\n\n * People with keyboard input problems after reboot should call\n   /opt/kde3/bin/genkdmconf --no-old --in /etc/opt/kde3/share/config/kdm/\n   and restart kdm. You can boot into runlevel 3 (no kdm) with entering\n   \"3\" at the boot prompt from Grub or Lilo.\n"
    author: "Anonymous"
  - subject: "Re: Beta 2 and Probs"
    date: 2005-02-10
    body: "thanks, this solves my problem!"
    author: "ac"
  - subject: "translating the solution"
    date: 2005-02-11
    body: "Having solved the Christmas kernel problem the long stupid way round (I subsequently discovered) I though I'd try man or --help to find out what this rather cryptic solution was doing, however no explanation was available - anyone care to offer an idiot's guide? \n\nBTW 3.3.92 is marvellous"
    author: "gerryg"
  - subject: "Re: Beta 2 and Probs"
    date: 2005-02-14
    body: "With the latest packages in the apt repository this no longer solves the keyboard problems I experience: most keys work fine, but that alt key doesn't work in KDE application and the del key doesn't work in KMail... \n\nI downgraded to the default 9.2 KDE -- 3.3.0, and, well, there's more difference than you'd believe. I already miss all the nice 3.4 improvements."
    author: "Boudewijn Rempt"
  - subject: "screenshots??"
    date: 2005-02-10
    body: "Hey guys, I hate to be the first to ask for them, but you know it's a must, isnt? :)\n\nthanks\n\nSaulo"
    author: "Saulo"
  - subject: "Simple"
    date: 2005-02-10
    body: "Find any screenshots for 3.4 beta 1\n\nReplace the '1' with a '2' using KolourPaint.\n\nSeriously - screenshots of new things are nice, but there are hardly any new features in Beta 2 that were in Beta 1, and no new apps.... it is mainly bugfixes, as you would expect."
    author: "Jason Keirstead"
  - subject: "Re: Simple"
    date: 2005-02-10
    body: "Doesn't matter.  He's got a point.  Screenshots always good.  Me want. =)"
    author: "ac"
  - subject: "Re: Simple"
    date: 2005-02-10
    body: "> there are hardly any new features in Beta 2 that were in Beta 1\n\nOh, there are. Just find them :-)..."
    author: "Anonymous"
  - subject: "Re: Simple"
    date: 2005-02-10
    body: "Just type \"about:\" in Konqueror."
    author: "Asdex"
  - subject: "Re: screenshots??"
    date: 2005-02-11
    body: "This one is cool :)\n\nhttp://www.kdt.internet.v.pl/jamlasica/img/kde392/kwin_plus_kompmgr_shadows_and_cool_gfx_effects.jpg"
    author: "kdt"
  - subject: "Gmail ..."
    date: 2005-02-10
    body: "Does gmail work with Konqueror yet? I use Firefox *only* because of gmail. At least on my SuSE partition (which I use as  my primary desktop) gmail does not load in Konqueror. However, with Mandrake 10.2 beta1, it works. Is it a SuSE thing? Is there any base library i should update/install to get it working?\n\n--"
    author: "Kanwar"
  - subject: "Re: Gmail ..."
    date: 2005-02-10
    body: "Gmail seems to work fine for me in Konqueror on Fedora Core 3 KDE 3.3.2.  I don't really 'use' Gmail though, so I may not be noticing features your talking about."
    author: "Corbin"
  - subject: "Re: Gmail ..."
    date: 2005-02-10
    body: "Thanks for the feedback. I haven't checked Fedora Core 3 yet. However, it seems to  me that the \"bug\" was fixed in 3.3.2 but seems to have mysteriously cropped up in 3.4 betas.\n\nCheers."
    author: "Kanwar"
  - subject: "Re: Gmail ..."
    date: 2005-02-10
    body: "http://lists.kde.org/?l=kde-cvs&m=110777629308510&w=2\n\nyou are not the only one having problems.\n\nDerek"
    author: "Derek Kite"
  - subject: "Beta 2?!?!?!"
    date: 2005-02-10
    body: "Wow, I setup a chroot on my Gentoo machine w/ KDE beta 1, and first ran 'startkde' today, and apparently Beta 2 is already out (I compiled a WHOLE gentoo install, and screwing around trying to FreeNX to work in a chroot, amoung other things before thinking \"just try 'X :1' today)!\n\nI guess I need to reboot my other computer back into gentoo, and install beta2 now.  All I really have to say about beta1 the few minutes I used it was no stability problems and WOW.  I guess the new Xorg stuff was meant for KDE 4 right?  I was also hoping to see rich text support for Kopete on AIM (that was the reason I actually installed the beta haha) :-(.  I like the new splash screen though, looks great!"
    author: "Corbin"
  - subject: "Re: Beta 2?!?!?!"
    date: 2005-02-10
    body: "> I guess the new Xorg stuff was meant for KDE 4 right?\n\nWhy? It's contained within Beta 2.\n\n> I like the new splash screen though, looks great!\n\nThe final release will of course have another one."
    author: "Anonymous"
  - subject: "list of fixed bugs since beta1 available?"
    date: 2005-02-10
    body: "Is there a list of changes since beta1 available?"
    author: "jm"
  - subject: "Re: list of fixed bugs since beta1 available?"
    date: 2005-02-10
    body: "There is no complete list, but you may read http://cvs-digest.org/ for the weeks since Beta 1 and until Beta 2 got tagged."
    author: "Anonymous"
  - subject: ":)"
    date: 2005-02-10
    body: "YAY!\n"
    author: "rSl"
  - subject: "KMail under KDE 3.4 Beta2"
    date: 2005-02-10
    body: "Hello,\n\ni have problems to send any E-Mails under KMail.. even if i want to send, there comes an error-message like this:\n----\nDas Versenden ist fehlgeschlagen:\nAuthentifizierung fehlgeschlagen, An error occured during authentication: SASL(-4): no mechanism available: No worthy mechs found wird nicht unterst\u00fctzt\nDie Nachricht verbleibt im Postausgang, bis Sie entweder das Problem beseitigt haben (z. B. falsche Adresse) oder die Nachricht aus dem Postausgang entfernen.\nDas folgende Transportprotokoll wurde benutzt:\njiin\n----\nsorry, this is a german error message, hope, that someone can translate it \nso, what can i do?\n\n"
    author: "jiin"
  - subject: "Re: KMail under KDE 3.4 Beta2"
    date: 2005-02-10
    body: "ok, no problem anymore - i've read, that KMail uses now the cyrus-sasl-packages, so after installing these, now it works :-) Thank You all @KDE for the great Work!\n"
    author: "jiin"
  - subject: "WARNING: Seemingly encrypted msgs can be sent unen"
    date: 2005-02-10
    body: "Due to a stupid bug automatic encryption is broken in the Beta 2 release of KMail/Kontact and messages which appear to be encrypted can in fact be sent unencrypted. Therefore whenever you want to send an encrypted message please explicitely request encryption in the message composer. You might also want to use \"Send later\" and then check whether the message in the outbox is really encrypted before you send it."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: WARNING: Seemingly encrypted msgs can be sent unen"
    date: 2005-02-10
    body: "Link to patch?"
    author: "Anonymous"
  - subject: "Re: WARNING: Seemingly encrypted msgs can be sent unen"
    date: 2005-02-11
    body: "The patch:\nhttp://webcvs.kde.org/kdepim/kmail/messagecomposer.cpp?r1=1.42&r2=1.43"
    author: "Ingo Kl\u00f6cker"
  - subject: "Slackware Packages?"
    date: 2005-02-10
    body: "Where can i find the Slackware 10.x packages? thanks."
    author: "fast_rizwaan"
  - subject: "Re: Slackware Packages?"
    date: 2005-02-10
    body: "On ftp.kde.org in the next few days."
    author: "Anonymous"
  - subject: "Web browsing vs file management"
    date: 2005-02-11
    body: "So, there's an issue I have with my current setup (3.1.5) and it appears to be in 3.4 beta 2 as well.  I'd like images to be embedded when I'm browsing, like they are in any other web browser.  However, I'd like them to open up in a program of my choosing when I'm file browsing.  In essence, I'd like to choose what to do based on which ioslave is being used.  sftp://, file://, etc should use an external program, http://, https://, etc should use embedding.  Is this possible and I'm just missing how?\n\nAnd yes, I know I can middle click to open in another program... But I'd like to stick with left click if I can."
    author: "KDE User"
  - subject: "You're Right!"
    date: 2005-02-11
    body: "I also feel like that konqi should open http:// and https:// and other network protocol in itself, but local file:// ftp:// should get opened with external application.\n\nreally are there are plans or wishes for this. i would support this feature."
    author: "fast_rizwaan"
  - subject: "Re: You're Right!"
    date: 2005-02-11
    body: "The first thing i do when i move in to a new kde is to make rules for this with the file asociacion tool:\n\n file://*.jpg -> Kuickshow\n http*://*.jpg -> Embedded viewer\n\nBut yes... i *REALLY!!!* think there should be some sort of one click way to make KDE distinguish between local and network files... In fact i want ALL my local files to open in a new window.. it makes little sense to read a html article in a window which is split and has a tree view on the left... \n\n~Macavity"
    author: "Macavity"
  - subject: "Re: You're Right!"
    date: 2005-02-11
    body: "Hi, I can't seem to get that to work when I try adding file://*.jpg as a filename pattern.  Is there something else needed to make it work?"
    author: "KDE User"
  - subject: "A little too buggy? Beta3"
    date: 2005-02-11
    body: "It seems too me a little to buggy to be the last beta, so i'd like to see another. What do you think about this?\nI can't play ogg with juk (and many other in the forum of gentoo)\nI can't see svg icons in the new konqui presentation\nI had my profile (new, created just from zero for beta2) lost 2 times in a day. Unfortunatly I still didn't understood why.\nKorn gives problem with opening a new program after it check for mails and can't set the background after the mail.\nThe new kicker effect contrasts with the kmenu botton (ex: if i click fast on the kmenu, before the effect has appeared, the effect cover my kmenu and makes it go away).\nI don't like that the name under the icons in the desktop get abbrevieted (configurable? I din't find an option...).\nKwallet had some problem with passwords (kmail continued to ask it, even if it was in kwallet) and I had to delete kwalletrc to solve the problem.\n\nI already reported some of this bugs and I'm looking for some information to report better the other and to check if they are already reported, but I still think they are too many for the last planned beta..\nso, what do you think about a beta3?\nNB: I'm not complaining about the number of bugs in bugs.kde.org!\nI'm talking about everyday problem that I encountered trying this beta. \nSorry for my English :)"
    author: "Giovanni"
  - subject: "Re: A little too buggy? Beta3"
    date: 2005-02-11
    body: "> The new kicker effect contrasts with the kmenu botton\n\nalready fixed =)"
    author: "Aaron J. Seigo"
  - subject: "Re: A little too buggy? Beta3"
    date: 2005-02-11
    body: "Yeah, i saw later on bugs.kde.org..thanks Aaron :)"
    author: "Giovanni"
  - subject: "KDM and more"
    date: 2005-02-11
    body: "I have the following problems:\n\n*KDM: I can't input any characters anymore (like password), I can't even do a ctrl+alt+F2 to switch to console anymore, Can't reboot anymore from KDM, after a while the keyboard is locked, when using XDM there are no problems at all.\n*Starting the first time it complains about the fact that /home/jeroen/.kde/.kde allready exist. KDE won't start for me, starting it as another user is no problem. Deleting /home/jeroen/.kde/.kde solves the problem  (why was that file there?). The file /home/jeroen/.kde/.kde/share/apps/ksplash/rctheme is created afterwards. Strange location for that file.\n*Contact is very buggy, it crashes once in a while.\n\nThe KDM problem is a very serious one.\n\nCheers,\n\nJeroen\n"
    author: "Jeroenvrp"
  - subject: "Re: KDM and more"
    date: 2005-02-11
    body: "Already answered, http://dot.kde.org/1107971557/1107997960/1108033671/"
    author: "Anonymous"
  - subject: "Re: KDM and more"
    date: 2005-02-11
    body: "Thanks for the info. Unfortunaly that is not the only problem. Another problem is the Kwallet integration in Kmail. After relogin it asks for passwords again and also it won't sent messages through the Gmail-smtp-server anymore. It asks for a password (it is allready in the prefs with the option 'remember password' on), but after giving the password an error pops-up with the message theat there is am authentification problem.\n\nAnyhow I am in the progress going back to 3.3.2, until things are better. I also must say that I had less problems when running the 3.3 betas.\n\nCheers,\n\nJeroen"
    author: "Jeroenvrp"
  - subject: "Re: KDM and more"
    date: 2005-02-11
    body: "Have got the same problem.\nPlease, report this to the devs or give me the permission to copy your description in a bug report.\nThanks :)"
    author: "Giovanni"
  - subject: "Re: KDM and more"
    date: 2005-02-11
    body: "This was also already mentioned above. Most likely you are missing some of the cryus-sasl-* packages."
    author: "Ingo Kl\u00f6cker"
  - subject: "Missing features?"
    date: 2005-02-11
    body: "Is it just me, or has KDE removed features for the sake of usability? \n\nFor example; Where has the document relations toolbar in Konqueror gone? I use that all the time! Sure, it may confuse some new users of KDE. But some of us use it all the time! How can I re-enable it? I don't see it in the toolbars menu!\n\nAlso, how do I bounce messages with Kmail?? That killed off more spam than anything! But now, they decided to 'clean' the menus a bit. Where has this feature gone?!?\n\nThis is in Keinstein, BTW.\n</rant>"
    author: "ac"
  - subject: "Re: Missing features?"
    date: 2005-02-11
    body: "> Where has the document relations toolbar in Konqueror gone? How can I re-enable it?\n\n\"Settings/Configure Extensions...\" (document relations is in kdeaddons)"
    author: "Anonymous"
  - subject: "Re: Missing features?"
    date: 2005-02-11
    body: "Thanks! I'd still like the Bounce feature back, though."
    author: "ac"
  - subject: "Re: Missing features?"
    date: 2005-02-11
    body: "the document relations bar was not removed for \"the sake of usability\". it was removed because it was very buggy, a complete annoyance for most people and not actively maintained. it seems it may have found a new maintainer (good!) and will be getting cleaned up."
    author: "Aaron J. Seigo"
  - subject: "Re: Missing features?"
    date: 2005-02-11
    body: "I really doubt that bouncing messages killed any spam. About 99.9% of all spam has a forged From address. Therefore a bounce will almost never reach the real sender of the spam. Instead the bounce will go to the poor soul the spam software chose as sender address.\n\nSince KMail doesn't have access to the mail envelope (which contains the real sender address) we removed the useless bounce feature from KMail. It shouldn't be difficult to \"re-add\" this feature by creating a special filter which calls a script converting a message to a bounce message and puts it in the outbox.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Missing features?"
    date: 2005-02-11
    body: ">I really doubt that bouncing messages killed any spam.\n\nAs much as you may doubt it, it actually does kill spam. At least, it did for me.\n\n>About 99.9% of all spam has a forged From address.\n\nAbout 58.7% of statistics are made up.\n\n>It shouldn't be difficult to \"re-add\" this feature by creating a special filter which calls a script converting a message to a bounce message and puts it in the outbox\n\nFor you, maybe. I haven't the first clue how to write such a script."
    author: "ac"
  - subject: "Re: Missing features?"
    date: 2005-03-04
    body: "create a special filter which...\n....calls a script which...\n........converts a message.\n\nCare to show us how to do it? I could use it!\n\nI miss the bounce feature. It was useful for other things than\njust spammers.\n\nWho decides on these apparently arbitrary changes? Does a voting\nmechanism exist? I'm starting to dread the after-effects of doing\nKDE upgrades. As often as not, there are as many unpleasant as\npleasant changes.\n"
    author: "JC"
  - subject: "Re: Missing features?"
    date: 2005-07-24
    body: "I also am missing the bounce feature *so* much :-( \n\nWould it be enabled back in the forthcoming releases? Or would you care to elaborate on the \"script\"?"
    author: "Shantanu Sharma"
  - subject: "Re: Missing features?"
    date: 2005-09-01
    body: "Never mind. Use \"Message->Forward->Redirect\" (or shortcut: \"E\") for bouncing messages."
    author: "Shantanu Sharma"
  - subject: "Re: Missing features?"
    date: 2007-02-03
    body: "Boune may have been a \"useless\" feature, but it sure felt good. I miss it, too. Okay, so maybe I need to get out more if using bounce gave me jollies and a mind satisfaction. Still, I miss it. The notion of a script is intriguing. Is there no one willing to help those of us who lack skill as script writers? "
    author: "andrew"
  - subject: "Nice KWin Transparency Support Video"
    date: 2005-02-12
    body: "Found this one by accident: http://bssteph.irtonline.org/kde_and_kompmgr_transparent_inactive_windows"
    author: "Anonymous"
  - subject: "Re: Nice KWin Transparency Support Video"
    date: 2005-02-12
    body: "Cool, but since the flash plugin keeps crashing the browser when composite extension is active, this is a problem than a feature"
    author: "ra1n"
---
There is no shortage of news this week for those of you following the dot, but to top it all off, I'm pleased to announce that the KDE Project has released another beta for the highly-anticipated KDE 3.4. And again, we're asking you to give it a good testing and report all problems you find on <A href="http://bugs.kde.org/">bugs.kde.org</a>.   A <a href="http://www.kde.org/announcements/announce-3.4beta2.php">full announcement</a> is available as well the <a href="http://www.kde.org/info/3.4beta2.php">info page</a> listing source and binaries. Your input is valuable to us and will determine the success of this <a href="http://developer.kde.org/development-versions/kde-3.4-release-plan.html">upcoming</a> <a href="http://developer.kde.org/development-versions/kde-3.4-features.html">major release</a>.




<!--break-->
