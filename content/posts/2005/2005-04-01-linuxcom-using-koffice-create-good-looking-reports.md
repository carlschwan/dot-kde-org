---
title: "Linux.com: Using KOffice to Create Good-Looking Reports"
date:    2005-04-01
authors:
  - "jriddell"
slug:    linuxcom-using-koffice-create-good-looking-reports
comments:
  - subject: "Speaks to the need for an updated Kugar!"
    date: 2005-04-01
    body: "It is quite funny as just today I started looking at improving the reporting capabilities of datakiosk.(1)  Currently, datakiosk integrates kugar reports in a very convenient and nice way.  Actually, Kugar has a fairly robust reporting engine, the big deficit is in the Kugar designer.\n\nIf no one else *cough* kexi people *cough* gets around to it I'll eventually be replacing the kudesigner.\n\n1. http://extragear.kde.org/apps/datakiosk/\n"
    author: "manyoso"
  - subject: "KChart, rows and columns and KOffice 1.4"
    date: 2005-04-01
    body: "Nice article, although a bit short.\n\nI just want to mention that the next upcoming version of KChart can chart data in columns as well and not just in rows.  This is of course also usable from within KSpread.\n\nAll this and much more will be in KOffice 1.4 whose feature freeze is next monday, i.e. a little more than a week from now. Of course, all the other components of KOffice will also be out in new and shining versions.  The activity on Kexi and Krita right now is nothing less than amazing.\n\n(If everything goes as I hope, then KChart 1.4 will be published with ZERO (0) known bugs.  Eat that, konqueror and kmail!  :-)  )\n"
    author: "Inge Wallin"
  - subject: "Waah!"
    date: 2005-04-01
    body: "Krita has two open bugs that I cannot close before the freeze -- both wishlist items. One is about dragging & dropping the tabs of the palettes, and the other about locking layers... I had started on both, but I thought that a select-by-colorrange dialog was more needed.\n\nBut those bugs keep nagging. I so wanted a clean bugzilla... But maybe they count as bugfixes that can be done during the beta?\n"
    author: "Boudewijn Rempt"
  - subject: "Re: Waah!"
    date: 2005-04-01
    body: "Wishes don't count.  Only real bugs."
    author: "Inge Wallin"
  - subject: "Re: Waah!"
    date: 2005-04-01
    body: "Phew! My honour is saved."
    author: "Boudewijn Rempt"
  - subject: "KOffice will survive"
    date: 2005-04-01
    body: "by Supporting OASIS open document format completely.\n\nCopying features from OpenOffice.org like Drop Cap is not there in Kword."
    author: "Fast_Rizwaan"
  - subject: "Soo disappointed :-("
    date: 2005-04-05
    body: "Reading the headline, I was excited to find that _finally_ someone found a real world use for KOffice. Hey, I thought, cool scripting ahead. After all, these are the features that people get excited about: workflow automation in KDE. Using dcop, it should be fairly easy to link the applications together and produce a good looking document with just a mouseclick.\n\nSo I thought.\n\nBut then I read the article. I cannot express how disappointed I was. This is what we call \"ein d\u00c3\u00bcnnes Brett bohren\" in Germany. A shitty ksh script to reformat some lines in a text document :-(\n\nThis is soo ridiculous."
    author: "Matthias Welwarsky"
  - subject: "Soo cheap! :-("
    date: 2005-04-05
    body: "So, Mathias -- \n\n-- why dont you simply write a kick-ass KWord/KOffice HOWTO yourself??\n\nYour moaning is soooo cheap. \n\nGet off your butt and do something about it. \n\nUsing your superiour brain, it \"should be fairly easy to\" for you to scribble such a document \"together and produce a good looking document with\"in just a few hours, no? You could show us all, how you can \"drill the thick beams\". Otherwise, shut up!"
    author: "kmail lover"
  - subject: "Re: Soo cheap! :-("
    date: 2005-04-06
    body: "First of all: I'm not cheap :)\n\nI just don't understand how _such_ a cheap article is worth a dot story. Is The Dot in such a desperate need of news that every p**p on the net needs to be linked here? Care for quality! If linux.com has no other content to publish, it's their problem.\n\nWhat does this tell people outside of KDE? \"They're just a bunch of kids, jumping up and down full of joy about their small achievements\".\n\nSomebody needs to stop navel-gazing all day long.\n"
    author: "Matthias Welwarsky"
---
<a href="http://www.linux.com">Linux.com</a> is featuring an article about <a href="http://applications.linux.com/applications/05/03/28/2329249.shtml?tid=13&tid=39">using KOffice to create good-looking reports</a>.  The article covers importing from a database into <a href="http://www.koffice.org/kspread/">KSpread</a>, preparing the data for <a href="http://www.koffice.org/kchart/">KChart</a> and formatting it in <a href="http://www.koffice.org/kword/">KWord</a>.





<!--break-->
