---
title: "KDE 3.4 Release Candidate 1"
date:    2005-02-28
authors:
  - "binner"
slug:    kde-34-release-candidate-1
comments:
  - subject: "Excellent"
    date: 2005-02-28
    body: "Just has I have time to get back in to KDE! "
    author: "dankers"
  - subject: "live-cd"
    date: 2005-02-28
    body: "The live CD is really great!\nBut i have found a bug (it's not a KDE bug).\nThe network configuration: i have a dhcp server in the network, but slax give me everytime the same ip adress (192.168.1.6) but my network ist 192.168.0.x\nI can insert in netconfig what i want i get only the same default ip.\nThe only option to set up the network is by hand with ifconfig.\n\nDoes someone else have simular experiences?"
    author: "pinky"
  - subject: "Re: live-cd"
    date: 2005-02-28
    body: ">> Does someone else have simular experiences?\n\nYep, same here"
    author: "konqi"
  - subject: "Re: live-cd BUG"
    date: 2005-03-01
    body: "Same here"
    author: "Daniel"
  - subject: "Re: live-cd"
    date: 2005-09-15
    body: "Same here too."
    author: "nantuko"
  - subject: "Great Job SUSE"
    date: 2005-02-28
    body: "Nice to see SUSE releasing binaries even for RCs! Keep up the good work!\nAh and now we know next version will be 9.3 :)"
    author: "John SUSE Freak"
  - subject: "Re: Great Job SUSE - konqueror"
    date: 2005-03-01
    body: "I have posted a graphic of konqueror as I like it. I wish SuSE adopts something similar. I am also glad SuSE releases binaries promptly, taking Mandrake's place. Needless to say, SuSE is now my top distro since I can count on the latest and the best everytime."
    author: "charles"
  - subject: "Re: Great Job SUSE - konqueror"
    date: 2005-03-01
    body: "I like everything except the Serif UI fonts.  To each his own, but I think you might actually win someone over with the same settings and Sans-Serif fonts."
    author: "ac"
  - subject: "Re: Great Job SUSE - konqueror"
    date: 2005-03-01
    body: "Me also. I've changed from Mandrake to Suse because of the up to date KDE binary packages.\nNow I can keep Suse 9.1 for a while because I can update KDE often, so I don't need to update the entire distro."
    author: "Iuri Fiedoruk"
  - subject: "error in kdebase compilation"
    date: 2005-02-28
    body: "when trying to compile kdebase I get this error:\n\nminicli.cpp: In member function `void Minicli::saveConfig()':\nminicli.cpp:242: error: no matching function for call to `KDesktopSettings::\n   setHistory(QStringList)'\nkdesktopsettings.h:895: error: candidates are: static void\n   KDesktopSettings::setHistory(QString)\nminicli.cpp:243: error: no matching function for call to `KDesktopSettings::\n   setTerminalApps(QStringList&)'\nkdesktopsettings.h:933: error: candidates are: static void\n   KDesktopSettings::setTerminalApps(QString)\nminicli.cpp:244: error: no matching function for call to `KDesktopSettings::\n   setCompletionItems(QStringList)'\nkdesktopsettings.h:952: error: candidates are: static void\n   KDesktopSettings::setCompletionItems(QString)\n\nchecking the latest cvs solved it :)\nI think it has to do with a kdesktopsettings(h.cpp) according to some guy on #kde"
    author: "Pat"
  - subject: "Re: error in kdebase compilation"
    date: 2005-02-28
    body: "I can compile the kdebase from the tarball without errors or patches :). \n\nRemember to compile arts, kdelib and kdebase in this order. I can only help if you tell me exactly how you built it.\n\n"
    author: "Anonymous"
  - subject: "Re: error in kdebase compilation"
    date: 2005-02-28
    body: "kdelibs, arts, kdebasen this is the order I compiled it. but that's ok now I compiled cvs version of kdebase and it worked fine."
    author: "Pat"
  - subject: "Hey"
    date: 2005-02-28
    body: "Welcome back the dot!"
    author: "David"
  - subject: "Slackware binaries ??"
    date: 2005-02-28
    body: "Is there any binaries to slackware ? Im using kde 3.4 beta2 from klax binaries, but did not found then (klax binaries) to slackware 3.4 rc1.\n\nAny hint would be good :)\n\nSee ya !"
    author: "Douglas"
  - subject: "Re: Slackware binaries ??"
    date: 2005-03-01
    body: "I uploaded my packages. Same comments and download location as in <a href=\"http://dot.kde.org/1108288130/1108327746/1108458961/\">this comment</a> apply.\n"
    author: "binner"
  - subject: "Keyboard doesn't work on Klax"
    date: 2005-02-28
    body: "I downloaded Klax, burned it, rebooted, login etc all working fine but as soon as I started X11 the Keyboard stopped functioning. It seems I'm not the only one with that problem as there was a similar post in the last news entry about Klax but noone could help then.\n\nThe Gentoo Forums (the only source of quality support for Linux; at least unless you're willing to pay - and I don't even use Gentoo) had some postings that seem to indicate that it's a problem with the mouse driver/usb support which would mean that there's no way to fix it.\n\nAnybody else who had the problem and found a way to get a working keyboard?"
    author: "noone"
  - subject: "Re: Keyboard doesn't work on Klax"
    date: 2005-03-02
    body: "> Anybody else who had the problem and found a way to get a working keyboard?\n\nI have, on two levels actually with SUSE 9.2 running on x86-64 HW. First problem is that directly booting to runlevel 5 does not work. When KDM starts, keyboard stops working. However, if i boot to runlevel 3 and start kdm manually, it works.\n\nSecond problem is that after successful login to KDE 3.4RC1 kontacts initial dialog ( asking imap password ) does not accept any input. Closing the dialog and pressing ctrl+l helps - new dialog works as expected.\n"
    author: "jmk"
  - subject: "Re: Keyboard doesn't work on Klax"
    date: 2005-03-03
    body: "Same problem here.  If you find a fix e-mail me unixpenguin2004 [at] earthlink [dot] net.  Keyboard loses responsiveness on x86 amd athlon on nforce2/shuttle motherboard with nvidia geforce 6800 graphics.  Standard ps2 keyboard usb mouse."
    author: "James Cornell"
  - subject: "Re: Keyboard doesn't work on Klax"
    date: 2005-03-03
    body: "Updated to 2.6.11 mainline kernel - haven't seen the problem since."
    author: "jmk"
  - subject: "Re: Keyboard doesn't work on Klax"
    date: 2005-03-10
    body: "See http://dot.kde.org/1108288130/1108820315/1110440177/ for how to make your keyboard work."
    author: "Anonymous"
  - subject: "Keyboard problem with KDE 3.4"
    date: 2005-03-24
    body: "After installing KDE 3.4, as soon as kdm starts the keyboard is blocked and cannot be used any more. This is not specific to Klax: I have experienced the same problem with SUSE 9.1. Although the solution I have found will unfortunately not help Klax users, I mention it here because it may be useful to others.\n\nReboot in level 3 (on SUSE, type '3' in the boot options line at startup) and log in as root. Backup your configuration files in the kdm/ directory (just in case), and then type:\n\n# genkdmconf --no-old --in /etc/opt/kde3/share/config/kdm\n\nOf course, depending on your distribution, you will need to use the proper path to the kdm/ directory in the above command.\n\nThis should solve the problem.\n\nAs for KDE 3.4, congratulations to all contributors and thank you!"
    author: "Micha\u00ebl"
  - subject: "Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-02-28
    body: "first of all, thanks binner for the livecd! it was very useful -i don't have time to build kde by myself, so this cd was god-given :)\n\n- it's 2005! can a browser come with the pop-up blocker disabled by default?\n- the knotes kpart in kontact is very poorly done. if there are no notes, all it displays is a white window without telling nothing, and one can't even click or right click to create a new note. the only way to do that is with the toolbar button.. very unintuitive. funny that the \"new\" command is available when right-clicking an *exisisting* note!\n- the default height of the fileselector is too small!\n- the new artwork is great!\n  two minor details\n    - in the wallpaper, the lighter text around the \"K DESKTOP ENVIRONMENT\" looks really bad, the first time i saw it i thought my monitor was broken..\n    - in the logout window, the kde logo is really out of place..\n- kpdf is wonderful, a real pleasure to use. but it starts maximized by default, very unprofessional. and when i click the \"restore\" button on the titlebar it goes down to a very small size. something in between would be much better.\n- too many \"yes/no\" buttons still around, for example the \"quit korganizzer\" one or the \"ktts not configured\" in kpdf.\n- why do i need a \"stop\" button in the file manager?\n\nanyway, KDE 3.4 rocks!!"
    author: "Anonymous"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-02-28
    body: "-> bugs.kde.org\n   \nif you write your stuff there , it will be forwarded to the correct maintainer...\n\nif you write it here, some user see it and perhaps a developer , but its unlikely you reach the right person."
    author: "chris"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-"
    date: 2005-02-28
    body: "Sounds like valid comments.  \nPlease, please, check bugs.kde.org if these are reported, and if not, report them.\nThanks."
    author: "Leo Spalteholz"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-"
    date: 2005-02-28
    body: "I'm not sure if bugs.kde.org is the right place for this kind of comments. They're not \"bugs\" nor \"wishlists\". Many of them are just personal taste opionions... Anyway, if some KDE developer will tell me to submit them as bug reports, I will surely do that."
    author: "Anonymous"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-"
    date: 2005-02-28
    body: "I think you should submit them. at least some of them. and you could go to a KDE developers IRC channel, and discuss the popup blocker thingy..."
    author: "superstoned"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-02-28
    body: "> why do i need a \"stop\" button in the file manager\n\nusually you don't. but until we separate the XMLGUI rc files for the different profiles, we can't do much about it. it'll come though. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "It also needs to recognize whether it's working over a network or not. If I can't hit stop for my NFS shares, I may need to use the kill cursor much more often."
    author: "jameth"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "> but until we separate the XMLGUI rc files for the different profiles, we can't do much about it.\n\nIf the functionality isn't there, then what is the meaning of \"XMLUIFile=konq-webbrowsing.rc\" in the simple browser profile?\n\nI thought that this is the special XMLGUI rc file that defines the simpler toolbar for this profile.\n"
    author: "Christian Loose"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "yes, the per-profile XMLGUI file support is there, but nobody has actually created separate rc files for each profile and then tested them in under common circumstances. it's not so much \"not doable\" as just \"not done as of yet\""
    author: "Aaron J. Seigo"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "Ah, okay, I misunderstood you. Thanks for the clarification!"
    author: "Christian Loose"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-02
    body: "Do you need help? Can I participate / contribute to this?\nThis is one of my favorite itches that need scratching in KDE.\n(see also http://bugs.kde.org/show_bug.cgi?id=28331#c23)"
    author: "Jens"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-02
    body: "we can always use more hands =)\n\nthis will be an issue that gets addressed post 3.4 and most of the work will be coordinated via the kfm-devel mailing list.. hope to see you there =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-03
    body: "Thanks. :-)\nI'll probably read it via Gmane if that's possible.\n\nI would also like to fix the issue about some icons just not being configurable (properly) in the toolbar config dialogs, because they are \"dynamically\" created (see the above bug report, http://bugs.kde.org/show_bug.cgi?id=28331#c23) ... ;)\n\nMaybe we can even adopt the method Firefox and OSX use: Show a windows with all available toolbar icons and let users drag&drop them onto their favorite toolbar.\n\n"
    author: "Jens"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-02-28
    body: "- it's 2005! can a browser come with the pop-up blocker disabled by default?\nYes\n\n - the default height of the fileselector is too small!\ngrab a corner and make it bigger :o)\n\n - why do i need a \"stop\" button in the file manager?\nfor if you want konqueror to stop loading a very large directory, or stop rendering a gaziljon thumbnails."
    author: "ac"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "excuse-me friends, but can someone tell-me if am I dreaming or have I listened something about a pop-up blocker in upcoming konqueror??? Is it there?"
    author: "Saulo"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "Its been there for a while...although sorta hidden for the untrained eye.  Look under settings-->configure konqueror-->java & javascript-->go to the javascript tab-  look down :) -global policies-->and put \"open new windows\" to smart.\n\n-Sam"
    author: "Sam Weber"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "thanks Sam!"
    author: "Saulo"
  - subject: "JUSUS CHRIST!"
    date: 2005-03-01
    body: "KDE is crazy on options and insane defaults.\n\nIf that's what you have to do to get pop up blocking, how many damn users do you think will know that!!? Some may not even know that Javascript has anything to do with pop ups, my mom certainy has no clue. KEEP IT SIMPLE STUPID!!! KDE is being killed by all these dumb defaults (like no pop up blocking by default) and inordinate amount of poorly designed options, options that don't have the average user in mind.\n\nHell, the KDE developers even want an option for this! http://bugs.kde.org/show_bug.cgi?id=59791\nThat's just f*cking ridiculous if they wanta  usable environment. Perhaps they should read this http://www106.pair.com/rhp/free-software-ui.html There will always be 1/1 milllion that wants an option for something, but it is ridiculous if KDE clutters Kcontrol with even more disorganized and confusing options of which half of nobody givs a damn about.\n\nI love KDE, but its usability is shit when it comes to Kcontrol, menus, and its defaults suck too. There is no need for an option on every damn thing and stop thinking like a programmer and think like a clueless user."
    author: "Al"
  - subject: "Re: JUSUS CHRIST!"
    date: 2005-03-01
    body: "but how do you _really_ feel? ;)"
    author: "Aaron J. Seigo"
  - subject: "Re: JUSUS CHRIST!"
    date: 2005-03-01
    body: "The bug you reference:\n> http://bugs.kde.org/show_bug.cgi?id=59791\n\nSays:\n>The plan is to enable it by default (and I do not plan to add a GUI option \n> to disable it). Disabling is therefore only available for C++ programmers who \n> have special needs if they want to use the KListView class but without the \n> shading.\n\n"
    author: "JohnFlux"
  - subject: "Re: JUSUS CHRIST!"
    date: 2005-03-01
    body: "opps, you are right, didn't say the very end messages"
    author: "JohnFlux"
  - subject: "Re: JUSUS CHRIST!"
    date: 2005-03-01
    body: "Developers thinking as clueless users will not result in better software, how many clueless users have you seen contributing to KDE?\n"
    author: "Rob"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "Imho the java part of the javascript settings should be moved to the plugins entry (the two have nothing in common anyhow - well, the name),\n\nthe java&js should be called popups&javascript, the global javascript policies should be moved to the top, followed by the domain specific and the global settings at the bottom (let's face it most people won't want to deactivate javascript).\n\nI'd also combine Behaviour, Appearance and Previews&Metadata in one \"File Manager\" entry, and Cookies, Cache, History and Browser Identification in a \"Privacy\" entry. This way the settings menu won't look as overwhelming when you open it for the first time =)"
    author: "noone"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-02
    body: "How about creating a mockup of your idea with Gimp or Qt Designer and then send it to the kde usability mailing list?\n"
    author: "ac"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "Konqueror was one of the first browsers ever to have pop-up blocking."
    author: "ac"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "And probably *the* first file manager to have it!\n;)"
    author: "ac"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "It's been there since about Konqueror 2.1. "
    author: "Sad Eagle"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "I'm using KDE since 3.0.x, but I just found out, that konqueror has a pop up blocker. I had no clue that javascript has anything todo with pop ups!"
    author: "hiasll"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "You are right there, it is kind of hidden. Perhaps a popup/wizard kind of thing the first time a popup is encountered would solve it, like mozilla have. Or just set it on by default, easiest solution. Don't think there will be many bugreports from people not getting popups:-)"
    author: "Morty"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "atleast one site I use regulary (buying creadit for my phone www.ezones.sk) use a bloody popup to show a new window of my bank. Stupid, stupid, stupid, took my a while to figure out why is not shown.\n\nWould a bloody icon (like the key-encrypted one) be sooo confusing distracting?"
    author: "miro"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "Yeah, my bank has something similar on the new \"improved\" version for some function, but they still keep the old one available. Luck. Sometimes I think the only working solution is to implement physical punishment for stupid webdesigners. Not that it will result in any less stupid web designs, but it will spread the pain some:-)"
    author: "Morty"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "KDE 3.4 shows a \"bloody icon\" when a popup window is suppressed."
    author: "Anonymous"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "- blocker by default:\nAgree, but we need to let the user know something was blocked when it happens, and let him easily see what was blocked and unblock it if he wants to. The current state in terms of usability is pretty bad. See bug report: http://bugs.kde.org/show_bug.cgi?id=85246\n\n- stop button in file manager:\nUseful if you have a few thousand files in a dir, possibly images, and you have previews on. Nice tobe able to make it stop loading.\n"
    author: "John SUSE Freak"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-01
    body: "> - blocker by default:\n> http://bugs.kde.org/show_bug.cgi?id=85246\n\nhttp://bugs.kde.org/show_bug.cgi?id=99041 is the more appropriate report."
    author: "Anonymous"
  - subject: "Re: Here's my unasked nitpicks-list about KDE 3.4-rc1"
    date: 2005-03-28
    body: "Thank you Sam Weber and Jusus Christ.  I am a new Linux/Fedora Core 3 user (Dec. '04).  I love the power of Konqueror and just found out through your writings how to enable the popup blocker in konquerer.  Just a short thanks for helping out new people.  I am enjoying the power of Linux and Konqueror. While there of many options for configuration, I love those choices.  Thanks again."
    author: "Mark Koljack"
  - subject: "Slackware Packages please!!!"
    date: 2005-02-28
    body: "using livecd is not good for me... because it won't allow smoother performance like installed Linux System. everthing is just slow... LiveCds are great for emergency repair etc. but not for testing and working with KDE...\n\nPlease provide packages...\n\nklax is slackware based!!! then can't we use those packages in Slackware 10.x?\n"
    author: "Fast_Rizwaan"
  - subject: "Re: Slackware Packages please!!!"
    date: 2005-02-28
    body: ">  then can't we use those packages in Slackware 10.x?\nABI issues maybe\n"
    author: "Anonymous Specutator"
  - subject: "Re: Slackware Packages please!!!"
    date: 2005-03-01
    body: "I am awaitting for slackware 10.x packages too..."
    author: "WereWulph"
  - subject: "Re: Slackware Packages please!!!"
    date: 2005-03-01
    body: "Im going to build some tomorrow and see if I can get them accepted."
    author: "jju"
  - subject: "Re: Slackware Packages please!!!"
    date: 2005-03-01
    body: "Well, kpdf is faster from CD than the 3.3 version on my disk. Can't wait for 3.4 final =)"
    author: "noone"
  - subject: "Pop-under."
    date: 2005-02-28
    body: "Loads of websites seem to be able to focus their tab automatically (surely that shouldn't be allowed), and create pop-under adverts. The site linked to as 'show-stoppers' does this."
    author: "Tim"
  - subject: "Re: Pop-under."
    date: 2005-03-01
    body: "That's likely the result of a security feature: It could happen previously that Web sites request focus to their form elements but their page/tab is not visible. Imagine opening a site in a background tab and you accidentally typing/sending your password to the wrong site!"
    author: "Anonymous"
  - subject: "Ugly"
    date: 2005-02-28
    body: "Ugly new icon today.  It doesnt fit with any of the other icons on the site.  You should use the same style for all icons."
    author: "Anonymous"
  - subject: "JuK \"Now playing\"?"
    date: 2005-02-28
    body: "Is there any way I can get rid of the now playing bar in JuK?  All it does is waste screen space."
    author: "EvilPenguin"
  - subject: "Re: JuK \"Now playing\"?"
    date: 2005-03-01
    body: "Not yet, see (and vote for) http://bugs.kde.org/show_bug.cgi?id=98195 ."
    author: "TMG"
  - subject: "New webpage logo"
    date: 2005-02-28
    body: "Geat new webpage logo. Looks very neat."
    author: "KDE User"
  - subject: "Looks fantastic."
    date: 2005-02-28
    body: "Oh and is this the new look of KDE?\n\nIt looks nothing but great. Coherrent and beautiful. Will definitely make a good first impression."
    author: "Dunhammer"
  - subject: "Re: Looks fantastic."
    date: 2005-03-01
    body: "Which major distribution still ships KDE with the default look though?"
    author: "Navindra Umanee"
  - subject: "Re: Looks fantastic."
    date: 2005-03-01
    body: "Gentoo for sure. Propably Slackware and Debian as well"
    author: "Janne"
  - subject: "Re: Looks fantastic."
    date: 2005-03-01
    body: "Yes, Slackware ships KDE with default look, which is great."
    author: "138"
  - subject: "nice"
    date: 2005-02-28
    body: "The new look is nice, but:\n\n1) I liked the slightly darker blue much better (in the new konqueror, control center and other introduction screens)\n\n2) I like the older 'System' icon (on the desktop) with the monitor and computer (see the previous release of Slax) better than the icon that's shown in the screenshots at Tuxmachines, which looks alien on the desktop because the other icons are not shown at that angle.\n"
    author: "konqi"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "Have you had a look at this kontrol center pic? The info asks the user to click on the \"Run as root\" button below....which button does not exist! I attempted to file a \"bug\" or correction on this but then realised that it is a very old issue! What is going on? Could the developers tell us which  \"Run as root\" button they mean? Why not just change that text as needed? I also appreciated the removal of the cut/paste buttons. They should have gone further and removed the \"up\" button too - just my opinion."
    author: "charles"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "The controll center issue is a real bug all right, but it  is not a KDE bug. That's a YaST2 module and therefore a Suse config module, and no part of KDE. And the correct place to file a bug report are to Suse.\n\nYour other issue is with the \"up\" button, personally I find it a important feature which increase usability. And it looks like several people agree, both developers and users. It is one of the features who sets Konqueror apart, and makes it unique. "
    author: "Morty"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "\" I also appreciated the removal of the cut/paste buttons. They should have gone further and removed the \"up\" button too - just my opinion.\"\n\nare you crazy??? that is one of my favorite button in konqueror, i use it all the time.\nvery usefull on ftp, fish and even http when u're reading article http://dot.kde.org/1109582845/1109628047/1109634705 on the dot just double click the up icon and you're back to the dot home page not to mention file browsing.\n\n"
    author: "Pat"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "I hope you realize that, that \"up\" button will not work on all sites including www.cnn.com, that is after navigation through lots of links. What about writing code to check whether it will work before displaying it? For ftp sites, its functionality could be enabled by default."
    author: "charles"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "Huh, the up button always works.  If I'm here: /a/b/c and I hit the up button it takes me to /a/b.  That's the way that button works, it's not a back button.\n\nIf it offends just remove it.  :)"
    author: "brian"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "It works correctly on cnn.com. For instance, go here: http://www.cnn.com/WORLD/ and then press Up button."
    author: "blacksheep"
  - subject: "This is what I mean: -"
    date: 2005-03-01
    body: "I was at this link on CNN. http://www.cnn.com/2005/LAW/03/01/scotus.death.penalty.ap/index.html and reading the latest story. Clicked the \"UP\" button and came to this link: - http://www.cnn.com/2005/LAW/03/01/scotus.death.penalty.ap/ with no problem. In fact, the story/page did not change.\n\nStuff happened when I clicked the \"UP\" button once again to get to this link: - http://www.cnn.com/2005/LAW/03/01/. The attached graphic represents what I saw. Now, if you see the attachment, you will agree with what I mean. The provision of buttons by Konqueror which might not work *all the time* will definitely create confusion to \"Joe Six Pack.\"\n\nIf you are a teacher you will agree with me. Those who need it can surely put it back. Or if Konqueror is in FTP mode, then the button can be useful. Once again, I suggest it be removed by default or be shown only when its usage will not result into errors. This is for the benefit of the average user which user KDE and Linux are trying to impress. Geeks are not forgotten here.\n\nFirst impressions are very important."
    author: "charles"
  - subject: "Re: This is what I mean: -"
    date: 2005-03-01
    body: "It worked perfectly.  It says \"page not found\".  You have to be dumb as rocks not to understand that message.  If you are a teacher you will agree with me."
    author: "Anonymous"
  - subject: "You are blaming the wrong developers"
    date: 2005-03-01
    body: "This is a bug with CNN, that they have not provided an index to content in that directory, even though there is obviously publicly-accessible content in that directory. Please file a bug report with them instead.\n\nPlease do not advocate breaking Konqueror to \"fix\" (actually work around, clumsily) a bug with CNN's web authorship skills. The \"up\" button is quite useful and works as intended. The GUI is clear to indicate what it is supposed to do (the URL updates in the URL bar), and behavior is exactly what is expected."
    author: "Anonymous"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "And what is about all the dead links and typo infected entered URLs? What about back and forward button not working due to use of dynamic content not working with that concept? Should Konqueror be a smartass in all those cases and try to prevent the user to even try those out by telling them they are stupid and just can't know better?"
    author: "ac"
  - subject: "Re: nice - but what about this?"
    date: 2005-03-01
    body: "> What about writing code to check whether it will work before displaying it?\n\nWhat about the user thinking whether it will work if he short-clicks the button or if he has to hold the button to open its popup?"
    author: "Anonymous"
  - subject: "Why -Wmissing-prototypes for C++?"
    date: 2005-02-28
    body: "When I build KDE I get warnings about -Wmissing-prototypes:\n\ncc1plus: warning: command line option \"-Wmissing-prototypes\" is valid for Ada/C/ObjC but not for C++\n\nWhy is -Wmissing-prototypes defined in the makefiles?\n\nI am using gcc 3.4.1\n\n/Egon"
    author: "Egon Larsson"
  - subject: "konqui toolbars"
    date: 2005-03-01
    body: "(Toolbars)...Sometimes they will merge, sometimes they will not....even after putting the <merge> item into the tool bar \"current actions\" list. Sometimes, an agreed to setting, even after being saved in the settings->save view profile...., does not seem to work. My toolbar, similar to Firefox's is disorganized now, and I cannot take it back to what it used to be. To make matters worse, nothing seems to work! This has been the case since the 3.1 version."
    author: "charles"
  - subject: "unsermake for i18n"
    date: 2005-03-01
    body: "kde-i18n-de requires \"unsermake\" to build.\nI tried to compile it from kdenonbeta from CVS but it failed for some reason I dont know. \n\nWhy? I never had a problem building it with konstruct."
    author: "Tim_"
  - subject: "Re: unsermake for i18n"
    date: 2005-03-01
    body: "It's a problem inside the kde-i18n tarballs which shall be fixed for final release."
    author: "Anonymous"
  - subject: "Re: unsermake for i18n"
    date: 2005-03-01
    body: "Thanks for the info!"
    author: "Tim_"
  - subject: "Could you please take a look at bug 91652"
    date: 2005-03-01
    body: "\nBug 91652: Pressing Enter on Submit button doesn't emit onclick JS event\nhttp://bugs.kde.org/show_bug.cgi?id=91652\n\nI would really consider it a showstopper for 3.4 as I also like to submit forms by  pressing Enter key, not using mouse. Searching around in bugs.kde.org shows that this same bug is reported many times from a different points of view."
    author: "pezz"
  - subject: "Re: Could you please take a look at bug 91652"
    date: 2005-03-01
    body: "Checked that even Safari 1.2.4 (v125.12) acts correctly ie as any other popular browser."
    author: "pezz"
  - subject: "Re: Could you please take a look at bug 91652"
    date: 2005-03-01
    body: "Congrats KDE dev.... You had made KDE into such a great desktop environment that not being able to submit forms by pressing Enter key had became a showstopper."
    author: "ac"
  - subject: "configure options question"
    date: 2005-03-01
    body: "What does \"--enable-closure\" do?\nI tried to compile the rc1 on a Slackware-current using for all packages (to try) \"--disable-debug --enable-pch --enable-new-ldflags --enable-closure --without-arts\" but kdegraphics, kdepim and kdenetwork do not compile here for me. \nWithout this option, at least kdegraphics compiled (I haven't tested the other packages yet).\n\nBest regards,"
    author: "Richard Van Den Boom"
  - subject: "Slackware 10.1 - KDE 3.4rc1 packages"
    date: 2005-03-03
    body: "KDE 3.4 Release Candidate 1:\n\nhttp://ktown.kde.org/~binner/klax/10.1/\n\nDownload all .tgz files there and in the Dependencies folder. these klax packages works like charm in slackware 10.1. \n\nMany thanks to Klax guys for providing the packages. Thanks people u rock."
    author: "Fast_Rizwaan"
---
On February 26th 2005, the KDE Project <a href="http://lists.kde.org/?l=kde-announce&m=110943281419778&w=2">announced the first release candidate</a> of KDE 3.4. Compile <a href="http://download.kde.org/unstable/3.4.0-rc1/src/">the sources</a> (<a href="http://www.kde.org/info/requirements/3.4.php">KDE 3.4 requirements list</a>, <a href="http://developer.kde.org/build/konstruct/">&quot;Konstruct&quot; build script</a>), download the &quot;Klax&quot; i486 <a href="http://ktown.kde.org/~binner/klax/">GNU/Linux Live-CD</a> (375 MB) or the first <a href="http://download.kde.org/unstable/3.4.0-rc1/">contributed binary packages</a>. More packages may follow later. Please test the <a href="http://developer.kde.org/development-versions/kde-3.4-features.html">new features</a> and <a href="http://bugs.kde.org/">report all bugs</a> so that we can identify the <a href="http://www.wordreference.com/definition/show-stopper">show-stoppers</a> to be fixed before the final release <a href="http://developer.kde.org/development-versions/kde-3.4-release-plan.html">planned for 16th March</a>. OSdir.com is the first to have <a href="http://shots.osdir.com/slideshows/slideshow.php?release=265&slide=1">screenshots of KDE 3.4 RC 1</a> and <a href="http://www.tuxmachines.org/gallery/view_album.php?set_albumName=KDE_rc1">tuxmachines.org</a> shows how customizable its look is.

<!--break-->
