---
title: "Quickies: Win32 Kontact, Awards, Hurd, Kubuntu Bounties, Linspire Job"
date:    2005-06-14
authors:
  - "jriddell"
slug:    quickies-win32-kontact-awards-hurd-kubuntu-bounties-linspire-job
comments:
  - subject: "Kontact ported to Windows"
    date: 2005-06-14
    body: "Good!\n\nI wonder what KDE apps will find their way to the smelly platform when Qt4 gets GPLed.\n\nBTW: Evolution is reported to compile on windows.\nhttp://gnomedesktop.org/node/2278\n "
    author: "cies breijs"
  - subject: "Re: Kontact ported to Windows"
    date: 2005-06-14
    body: "Evolution compiles on Windows? So? Kontact does already run on Windows due to the folks of kde-cygwin. Three days ago the first snapshot of KDE 3.4.1 for cygwin was released (http://sourceforge.net/forum/forum.php?forum_id=473247\n):\n\"Posted By: habacker\n Date: 2005-06-11 12:14\n Summary: First snapshot of KDE 3.4.1 for cygwin available \n\nToday the first snapshot release of KDE 3.4.1 for cygwin is released. It is compiled from recent kde svn sources and contains all application from the kdebase package like konqueror, kate, kicker and mostly applications from kdepim like kmail, korganizer, kontact and friends.  \n  \nFrom the kdesdk package there is umbrello included. \n  \nAs special feature a tool named lnk2desktop is available, which imports explorer startmenu items into kicker.  \n  \nYou can download this snapshot from  \n http://kde-cygwin.sf.net/snapshots/kde/kde3.4/\"\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Kontact ported to Windows"
    date: 2005-06-14
    body: "In that case Evolution runs already on Windows too via cygwin too, Evolution its making its way to run native on Windows the same Kontact is trying to.\n\nso please get the facs.\n\n"
    author: "TIM.."
  - subject: "Re: Kontact ported to Windows"
    date: 2005-06-16
    body: "it was just a little (BTW) note... no offense :) "
    author: "cies breijs"
  - subject: "unbelievable"
    date: 2005-06-14
    body: "How many great news!!! I'm Amazed!!\nGreat times, aren't they ?? :-)\n( Take a look at this artwork I just tried in amarok:\nhttp://www.kde-look.org/content/show.php?content=25178 )\nVive-la-KDAB"
    author: "jumpy"
  - subject: "Some say KDE 3.5 is slower than 3.4.1..."
    date: 2005-06-14
    body: "Is this true?\n\nIf so, why and will the final be this way? One of the most important things about KDE IMO is speed, and I would hate for it to get any slower."
    author: "Alex"
  - subject: "Re: Some say KDE 3.5 is slower than 3.4.1..."
    date: 2005-06-14
    body: "Huh?  How can KDE 3.5 be slower than anything it's not out yet.\n\nNo offense intended, but if you're concerned about speed why not develop some benchmarks and check out KDE HEAD on a regular basis and report your findings.  That could be helpful."
    author: "aardvark"
  - subject: "Re: Some say KDE 3.5 is slower than 3.4.1..."
    date: 2005-06-14
    body: "No"
    author: "Leo S"
  - subject: "Re: Some say KDE 3.5 is slower than 3.4.1..."
    date: 2005-06-15
    body: "From many perspectives, it is wise to worry about quality first...then optimize.\n\nBesides, the product is not even close to beta...have some patients..."
    author: "Rick"
  - subject: "Kde 3.5 won't be out for a while"
    date: 2005-06-14
    body: "And I'm sure everyone will pay close attention to performance, I wouldn't worry about it."
    author: "Daniel"
  - subject: "Re: Kde 3.5 won't be out for a while"
    date: 2005-06-14
    body: "if everybody thinks that, it won't be very fast :D"
    author: "superstoned"
  - subject: "Office applications"
    date: 2005-06-14
    body: "Interesting that KOffice was mentioned among the choices, but not Abiword and Gnumeric. \n"
    author: "Inge Wallin"
  - subject: "Re: Office applications"
    date: 2005-06-14
    body: ">>office program: KDE Kontact\n>>office program: KOffice\n>>office program: LaTex\n>>office program: LyX\n>>office program: OpenOffice.org\n>>office program: Scribus\n>>office program: StarOffice\n>>office program: Mozilla Sunbird\n\nRight, that poll sucks, no Gnumeric, no Abiword.\n\nThat poll is biased.\n"
    author: "TIM.."
  - subject: "Hurd"
    date: 2005-06-14
    body: "That Hurd port seem really nice. Mayby couple of year more and then I can get my Debian GNU/Hurd running with my favorate desktop environment ;)"
    author: "Petteri"
  - subject: "Even KDE runs on Hurd"
    date: 2005-06-14
    body: "Even KDE runs on Hurd. See the announcement [1] and a screenshot [2].\n\n[1] http://lists.debian.org/debian-hurd/2005/06/msg00056.html\n[2] http://www.h1.org/~ncryer/kde.png"
    author: "Mario Fux"
  - subject: "Re: Even KDE runs on Hurd"
    date: 2005-06-17
    body: "Qt runs on the L4 microkernel directly (under a DROPS environment), which is the kernel that Hurd is being ported on, deprecating GNU Mach.\n\nhttp://fiascodobrasil.codigolivre.org.br/qtporting/\n\nAny volunteers for a KDE port? :)"
    author: "Josef Spillner"
  - subject: "Re: Even KDE runs on Hurd"
    date: 2005-06-17
    body: "> Any volunteers for a KDE port? :)\n\nToo late, it seems... :)\n\nhttp://lists.debian.org/debian-hurd/2005/06/msg00056.html\n\n"
    author: "cm"
  - subject: "Re: Even KDE runs on Hurd"
    date: 2005-06-17
    body: "Oops, sorry.  Please forget the previous post..."
    author: "cm"
  - subject: "Shouldn't that be..."
    date: 2005-06-14
    body: "Kuickies?\nI keed, I keed"
    author: "mikeyd"
  - subject: "Linspire hiring for Kopete?"
    date: 2005-06-16
    body: "That seems pretty odd, being that they are a big sponsor of the gaim project.  I wonder what they're up to?"
    author: "iKDE"
---
KDE/Qt company KDAB will <a href="http://partners.trolltech.com/news/00000008.html">port Kontact to Windows</a> once Qt 4 is out. *** Linux Journal are holding their <a href="http://linuxjournal.com/article/8266">Readers' Choice Awards</a>, make sure you vote for KDE. *** Some elite Debian hackers <a href="http://lists.debian.org/debian-hurd/2005/04/msg00082.html">ported Qt to the Hurd</a>. *** Kubuntu released a <a href="http://kubuntu.org/special-cds.php">Live CD with KDE 3.4.1 and KOffice 1.4 beta</a>, and if you missed out on the Google bounties you may be interested in the <a href="http://udu.wiki.ubuntu.com/BreezyBounties">Kubuntu Bounties</a>. *** Finally congratulations to Matt Rogers on <a href="http://matt.rogers.name/cgi-bin/pyblosxom.cgi/KDE/linspire.html">getting a job with Linspire</a> to work on instant messenger program Kopete.

<!--break-->
