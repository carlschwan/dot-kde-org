---
title: "KDE to Migrate to bksys/SCons Build System "
date:    2005-09-11
authors:
  - "jriddell"
slug:    kde-migrate-bksysscons-build-system
comments:
  - subject: "Dependencies"
    date: 2005-09-11
    body: "So, does that mean that KDE mandatorily requires Python as dependency?\nOr does this only matter on source-based distros like Gentoo?"
    author: "Jakob Petsovits"
  - subject: "Re: Dependencies"
    date: 2005-09-11
    body: "python will be a requirement to build from source. but python is found on pretty much every modern UNIX-y OS that has even a tangential desktop slant and is easily added to those OSes that don't ship with it."
    author: "Aaron J. Seigo"
  - subject: "Re: Dependencies"
    date: 2005-09-11
    body: "only as a build dependency, not a runtime dependency"
    author: "sb"
  - subject: "Re: Dependencies"
    date: 2005-09-12
    body: "Yes, that's what I wanted to know, thanks.\n(Although I'm one of those who would like\nto see it as runtime dependency too :)"
    author: "Jakob Petsovits"
  - subject: "BK?"
    date: 2005-09-11
    body: "Thought BK sttod for bit keeper for a sec - wondered why KDE developers wanted to open that can of worms again :)"
    author: "john"
  - subject: "Re: BK?"
    date: 2005-09-13
    body: "can of worm?? BK is/was one of the best available vcs, too bad zealots complained about the license ..."
    author: "chouimat"
  - subject: "Sounds Good"
    date: 2005-09-11
    body: "Sounds good to me. The existing build and tools chain is an absolute pain to work with at any time. With this and the use of things like unit tests should make KDE a very nice desktop to develop for, and develop with than it is already. Done right this could certainly attract more developers and contributors to KDE come KDE 4."
    author: "Segedunum"
  - subject: "KDE is on the ball"
    date: 2005-09-11
    body: "First Subversion, now a new build system.  What's next?  Ditching Bugzilla for Trac?  ;-)  \n\nThis is great work so keep it up!"
    author: "am"
  - subject: "Re: KDE is on the ball"
    date: 2005-09-12
    body: "We can only hope :-)"
    author: "Roberto Alsina"
  - subject: "Re: KDE is on the ball"
    date: 2005-09-19
    body: "Yes, please! trac is really nice..!"
    author: "Jonas"
  - subject: "distributed compiling (distcc)"
    date: 2005-09-11
    body: "Is it possible to build using distccd with scons like \"unsermake -p -j4 CXX=distcc\"\n\n"
    author: "fast_rizwaan"
  - subject: "Re: distributed compiling (distcc)"
    date: 2005-09-11
    body: "scons supports parallel compilation natively (and even does a much better job than automake, about even with unsermake).  scons also includes support for notating targets that produce files as a side effect, which can break parallel builds.\n\nAs far as overriding CXX that way, I'm not sure, it's easier just to put distcc's gcc symlinks first in the PATH anyways though."
    author: "Michael Pyne"
  - subject: "Re: distributed compiling (distcc)"
    date: 2006-01-10
    body: "Yeah, if bksys is anything like scons. (Im talking about scons here because I've been using it for a while now.)\n\nIn the toplevel SConstruct file, you can say\n\nenv = Environment(\n  CXX = 'distcc g++',\n  ....\n)\nor:\nenv.Replace(CXX = 'distcc g++') # ccache distcc g++ works as well is you don't wnat to use scons caching.\n\nwhen running scons say: scons -j N where N is the number of jobs you want to use.\nIt should work better then make -j N"
    author: "Ben"
  - subject: "KDevelop integration?"
    date: 2005-09-11
    body: "How are the issues (hard to parse existing Python build scripts since they are too powerful to catch all possibilities) raised in the past on the mailinglists about integration of the new building system into KDevelop to be solved? Strictly limiting API/syntact to the amount understood/supported by KDevelop? With everything not complying being thrown out from the SVN? Who is going to monitor this?"
    author: "ac"
  - subject: "Re: KDevelop integration?"
    date: 2005-09-11
    body: "From what I understand bksys will also be able to support a XML description of the target and its build dependencies, etc.  The idea is to make it possible to do most of what you'd need to do using XML alone (which is machine-parsable, and can therefore be understood by KDevelop).  Not sure how successful it will be, but there are KDevelop guys already working on it."
    author: "Michael Pyne"
  - subject: "Re: KDevelop integration?"
    date: 2005-09-12
    body: "Well, one idea was to embed the python interpreter in KDevelop and then query scons directly via it's API.  With the information returned from the API we could write out a SConstruct file that KDevelop could understand.\n\nUnfortunately, this does not seem possible.  SCons API is nearly impossible to understand and it doesn't seem that the relevant information (a list of sources, etc, etc) are maintained in any member variables we can get access to.\n\nSo, probably what mpyne said above ;)"
    author: "manyoso"
  - subject: "Hooray..."
    date: 2005-09-12
    body: "...for not choosing CMake!"
    author: "Carlo"
  - subject: "Re: Hooray..."
    date: 2005-09-12
    body: "Agreed.. I like Cmake, but it's syntax is hideous.. I had bad experience with that while building VTK (Visualization Toolkit)"
    author: "smt"
  - subject: "porting now"
    date: 2005-09-12
    body: "Would it make any sense to move amaroK 1.4 to bksys? Could we do so and keep autoconf? Sounds like most of the TODO list for bksys is configuration related.\n\nOf course, I can't imagine amaroK is that much more complicated then kdegames if at all. Its just that there are several features that have to be enabled/disabled.\n\n(BTW, codeine is another KDE project using scons)."
    author: "Ian Monroe"
  - subject: "Re: porting now"
    date: 2005-09-12
    body: "There is support for enabling/disabling features in bksys, we use it in Rosegarden. It can be improved, but it's perfectly useable as it is. As for using autoconf in parallel, yes, no problem, both are independant, but there's little reason to do so."
    author: "Guillaume Laurent"
  - subject: "porting happening"
    date: 2005-09-14
    body: "Just for people keeping score, Max Howell has begun writing the SConscript's for amaroK."
    author: "Ian Monroe"
  - subject: "ant?"
    date: 2005-09-12
    body: "Hello,\nwhat is about the Java based Apache ant? Why is this not suitable?\n\nFlorian"
    author: "Florian"
  - subject: "Re: ant?"
    date: 2005-09-12
    body: "Because it is bullshit.\nBecause it requires a JVM.\nBecause it makes average cases easy, some simple cases very hard and really big cases almost impossible. ;-)\n\nMore seriously, ant misses generic dependency handling which is a big drawkack, IMHO.\n\n"
    author: "Jean"
  - subject: "Re: ant?"
    date: 2005-09-12
    body: "and you really, really, *REALLY* don't want to manually edit build files written in xml."
    author: "Guillaume Laurent"
  - subject: "HP-UX"
    date: 2005-09-12
    body: "Hopefully this will help with my goal to use KDevelop on HP-UX.  The main problem is -no-undefined being passed to libtool, which on HP makes it build static libs (hard to have a plugin thats built as a static lib).\n\nWill BKSys be eliminating libtool?"
    author: "Tom Matelich"
  - subject: "Re: HP-UX"
    date: 2005-09-13
    body: "Yes it does."
    author: "anonymous"
  - subject: "Re: HP-UX"
    date: 2005-09-13
    body: "You always need to edit the generated libtool to not use the wrong options for -no-undefined. I've used a simple script. \n\nI tried making libtool people fix it, but they refused to understand what I told them."
    author: "Allan Sandfeld"
  - subject: "Re: HP-UX"
    date: 2005-09-17
    body: "Try again. The libtool people have grown very responsive to bug reports & patches in the last months. There was a case where people were complaining that libtool was eating up build time during the linkage of gcj in the gcc tree for example. Lots of talk about ditching libtool. Then the libtool people fixed libtool to spend next to nothing on that task, and everyone moved on.\n\nI find that trying again every now and then is a pretty good way to get my patches & proposals into libtool.\n\ncheers,\ndalibor topic"
    author: "Dalibor Topic"
  - subject: "Absolutely brilliant news!"
    date: 2005-09-13
    body: "I've been using bksys for my projects for a while now. It is a wonderful build system it brings a smile to my face to use it where using automake/autoconf would always bring my hours of despair. Great choice to those who made it. Grats to the developer :-)\n\nAlso it means the source tarballs shrink dramatically. My tarballs for Filelight went from 600kB to 60kB. Bandwidth savings for all!"
    author: "Max Howell"
  - subject: "Re: Absolutely brilliant news!"
    date: 2005-09-13
    body: ">  My tarballs for Filelight went from 600kB to 60kB. Bandwidth savings for all!\nSo you have to finish drinking coffee in 0.03 rather than in 0.3 seconds to keep pushing? :)\n\nlg"
    author: "Erik"
  - subject: "Working with scons"
    date: 2005-09-13
    body: "Lately we were doing some testing to the scons build too we've but found it very difficult to work with since it was not so java oriented. Therefore I was wondering how did you handle the Java build in your system?. Did you implement the required changes/addiotions yourself or got some adequate reference from the Scons dev team?"
    author: "Shiri Danan"
  - subject: "Oh no, this is definately a shot in the own leg"
    date: 2005-09-13
    body: "If you allow, I am not a big fan of stuff that requires Python. It was a big pain to have Python stuff together with GNOME and I hoped that KDE would stay entirely free of it, not just runtime, but also buildtime. I feel a bit sad to say the truth because I've been following the Scons discussion on IRC for quite a while now amd came to the personal conclusion that Scons has absolutely no advantage over Auto* not to mention that this requires a new learning curve to the developers to keep on with the stuff and then in what aspects does it make stuff easier ? I was seriously considering keeping KDE as my main desktop of choice once I set up a new source based setup of my Linux system and Python was one of the main reasons that I wanted to get rid off. 40 mb of Python trash for just some config files - what's next MONO ?"
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-13
    body: "* Size: 100KB (miniSCons included) vs 600KB (autotools)\n\nNot to forget 40mb of Pyhton which makes it 40mb and 100kb. Autotools is still smaller :)"
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-13
    body: "But you'll quickly reduce that saving once you start generating or distributing configure scripts, and Python is installed as standard on most distros, anyway. Don't you think it's better to pay the cost up front rather than every time you download or create a new package?\n"
    author: "Anonymous Python"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-13
    body: "> and Python is installed as standard on most distros, anyway.\n\nThis is placebo talk and a very bad excuse too. On most modern distros we have MONO and JAVA installed too, so why not make it a dependency for KDE as well ? Oh and even more distros and developers use auto* because it's a defacto standard in professional building of source based software - but no, let's make Scons a new unnecessary dependency because someone said so."
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-13
    body: "On the other hand it removes the dependency on Perl .. and also M4, GNU/Make, libtool, automake, autoconf.\n\nWhat KDE or autotool-based project are you maintaining once again ?"
    author: "anonymous_curious"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> On most modern distros we have MONO and JAVA installed too, so why not make it a dependency for KDE as well ?\n\nBecause there's no compelling reason to do so. Scons is a compelling reason to depend on Python. Qt is a compelling reason to depend on C++. Et cetera.\n\nYour excuse for the use of autotools of, \"everyone else does\" is worse. If scons is significantly better, then it should be used, and the fact that most distributions ship Python simply shows that the Python dependency is a non-issue for the great majority of people. Why aren't you lambasting KDE for moving to Subversion? Most other project haven't, and CVS is like a de facto standard."
    author: "Dolio"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "a) We don't know if Scons is really that significantly better. The reasons for switching away from autotools was because a significant amount of KDE developers were not willing to read the manuals so they could set up their stuff correctly.\n\nb) Python may be shopped with most distributions and I don't deny that but it's not required on most distributions. It's an option to be installed for people who desperately need it to hack up some small scripts.\n\nc) Python will not be a build dependency anymore because as soon as Python is detected by the KDE scripts as soon it starts to compile optional python files too and installs them (which is not wanted).\n\nd) If you read the autotool documents correctly then you would figure that autotools has to serve on a lot of POSIX compliant systems with dozens if not hundrets of different toolchains, compilers, headers and whatever. It's not limited to linux, it's not limited on windows, it's not even limited on operating systems such as MorphOS, AmigaOS (which are totally NON POSIX). That are the big advantages of autotools. You enter configure and then make afterwards. But you of course know perfectly the difference between scons and auto* since you never ever read a damn doc about it.\n\nSo now name up some advantages of scons over auto* and I will reply in a few hours to crush all of them down."
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> So now name up some advantages of scons over auto*\n\nnot only does scons not make me cringe every time i look at it, but i can even reasonably hack on it without become a scons wizard.\n\nit also means that instead of every 3rd party app shipping a couple of megs of configuration structure (have you SEEN how big configure, aclocal.m4, acinclude.m4, etc is these days?!), they can ship a tiny little script.\n\nwhen your app is only a few hundred KB (if that) of source code, the current situation is just silly and a bandwidth waster.\n\nscons does the job, does it well, does it with less black magic and does it smaller."
    author: "Aaron J. Seigo"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "\"The reasons for switching away from autotools was because a significant amount of KDE developers were not willing to read the manuals so they could set up their stuff correctly.\"\n\nHeaven help anyone who has to read the autotools manuals! I've done a fair amount of autotools work and whilst I see the benefits in small scale projects, having to write shell script-like code with bizarre tricks that only seasoned bash coders find acceptable is not exactly how I envisaged spending the 21st century. Of course, most autotools-based projects blow most of the benefits away by using automake, thus generating a configure script with 20000 system tests just to compile a \"hello world\"-level program.\n\n\"Python may be shopped with most distributions and I don't deny that but it's not required on most distributions. It's an option to be installed for people who desperately need it to hack up some small scripts.\"\n\nThe increasing recognition of dynamically-typed programming languages is another trend that passed you by, then?\n\n\"But you of course know perfectly the difference between scons and auto* since you never ever read a damn doc about it.\"\n\nI've read the \"damn doc about it\" and, as I said, it has its place. But given that SCons has a certain amount of heritage in a project to make better and more usable development tools, and given that is extensible using a modern programming language, I'd suggest that you seriously reconsider those advantages whether you seek to publicly (and ridiculously, in my view) \"crush all of them down\" or not."
    author: "The Badger"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> The increasing recognition of dynamically-typed programming\n> languages is another trend that passed you by, then?\n\nYou mean (and I am trolling here) the same trend that KDE keeps passing such as dbus, hal, libnotify, gstreamer, cairo, glitz, libburn, pkgconfig, poppler, startup-notification and all the other standards found on fdo ? But yet create their own set of stuff to avoid being compared with GNOME ?\n\nIf I recall back times when I read some archives of XDG ML then well, I find really curious reasons raised by KDE devels to NOT USE them. The same justified reasons I accept people to dislike something because its from the evil opponent the same way I would expect people to accept my dislike of Python as depedency. Maybe both reasons are braindead but they are at least reasons, the one has those the others has others.\n\nI don't want to bash or attack anyone here because I believe everyone has a good right to have his or her own opinion but please don't make it sound like Python is the trendsetter in languages. So why not using MONO to develop core KDE apps ? Anyone missing the trend of .NET and C# ?"
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> [...] and all the other standards found on fdo ?\n\nhttp://www.freedesktop.org\n\n\"freedesktop.org is not a formal standards organization\"\n\"Unlike a standards organization, freedesktop.org is a \"collaboration zone\" where ideas and code are tossed around\""
    author: "Anonymous"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-15
    body: "Saying that people don't use auto* because they are not bothering with the manuals is stupid. It's like saying people prefer wearing shoes because they are not bothering to form calluses in their feet."
    author: "Roberto Alsina"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "Well, maybe it wouldn't be such a bad idea to require Mono, Java, Ruby and Python. >:-)\n\nAt least it would open up the KDE platform to developers who use those languages and environments, and it would take away the pressure to use C++ for everything just because the other bindings are \"optional\".\n\nLet me ask you a question: if Scons was written in Perl, C or C++, would you still have the same reservations? I believe you would, and this makes your arguments about Python secondary to the ones you're trying to make."
    author: "Anonymous Python"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-13
    body: "Except you don't have to distribute python along with your software, while you do have to distribute the auto* generated files. Come on, a build-time dependency on python is hardly a problem in practice."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> Come on, a build-time dependency on python is hardly a problem in practice.\n\nIt's not just a build time dependency.\n\n;--- reply from another comment\nc) Python will not be a build dependency anymore because as soon as Python is detected by the KDE scripts as soon it starts to compile optional python files too and installs them (which is not wanted).\n"
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "*sigh*. Look, if you really want to save every cpu cycle and HD block you have, I suggest you switch back to twm. Better yet, stick to the console. Your complains are really not making any sense beyond \"python baaaad, whaaaaa\"."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "You are not making any sense either, you see that I dislike Scons because of Python - which you seem to have an even bigger issue with - and you start to discredit my comments by making silly replies."
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-15
    body: "That would be only silly if that weren't possible to disable."
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "Either you are misinformed, or your distribution has terrible python packages.\n\nMy own research on my local system shows:\n\nclee ~ % aptitude show perl autoconf automake1.9 m4 libtool  | grep 'Uncompressed Size'\nUncompressed Size: 11.5M\nUncompressed Size: 1507k\nUncompressed Size: 1602k\nUncompressed Size: 348k\nUncompressed Size: 2380k\n\nThat's about 16MB for perl, m4, autoconf, automake, and libtool on my system.\n\nclee ~ % aptitude show python2.4 | grep 'Uncompressed Size'\nUncompressed Size: 9191k\n\nSo, it's a little over half the size of perl + m4 + autoconf + automake + libtool.\n\nPlus, you claim that the autotools work perfectly on every system ever, except that it's patently untrue; the vast majority of the problems our porters have on MacOS X and Win32 systems are directly because of autotools.\n\nBenjamin Reed, the main developer behind the MacOS X port, said in this message (http://lists.kde.org/?l=kde-core-devel&m=111866452210421&w=2) on the Build system for KDE4 thread (http://lists.kde.org/?t=111863688400002&r=1&w=2):\n\n\"Writing tests in scons/etc. is considerably easier than doing it in M4\nand shell.\"\n\nJaroslaw Staniek, the main developer on the Win32 port,  said (http://lists.kde.org/?l=kde-core-devel&m=111866757017220&w=2) in the same thread:\n\n\"SCons would be my favourite. Among others, there's one more advantage: with python scripts I wont run off with win32 PIDs on cygwin. For now oferflowing PIDs counter forces me to (sic!) reboot.\"\n\nSo the \"BUT THE AUTOTOOLS ARE CROSS-PLATFORM!\" argument falls on its face. They're unusable on non-UNIX systems for real projects, especially ones the size of KDE.\n\nThere are other serious issues with the autotools, as well, which is the main reason that we're interested in moving away from them. Such reasons include the fact that the autotools make it very difficult (if not impossible) to properly support parallelized builds, and the fact that they are difficult and unintuitive to use, especially \"properly\". So much so that you have to read \"dozens of documents\" on them in order to make use of them.... (half of these documents, incidentally, are for obsolete versions, and the behaviours have changed significantly in some aspects depending on the version that you're using.)\n\nSo, to sum up:\n\n1) The autotools have serious issues with their \"cross-platform\" support\n2) The autotools lack features we desire, such as easy extensibility and easily parallelizable builds\n3) The autotools use a variety of languages, none of which is simple or well-known by the vast majority of our developers.\n4) The autotools actually take up *more* space on disk, both in the installed packages required and in the amount of space required for our autotools infrastructure (the 600kb vs 100kb argument)\n\nI look forward to you \"crushing my arguments into dust\" though. Try to remember to cite actual sources, instead of just using rhetoric. "
    author: "clee"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "I don't have any beef with SCons, but I'm not sure why you included perl in the autotools size measurement?  What does perl have to do with anything?"
    author: "kundor"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "Because the autotools require Perl to run, and Perl is bigger than Python.\n\nTry running 'rpm -q --requires autoconf' or 'aptitude show -v autoconf' (depending on which distribution you're on)."
    author: "clee"
  - subject: "Re: Oh no, this is definately a shot in the own leg"
    date: 2005-09-13
    body: "Using bksys instead of auto* on my projects has prevented me from having to commit suicide. Which at the very least is an advantage for me, and I'd like to think some other people too.\n\nDevelopers have to learn a new build system, true. But after 3 hours of using it they will very quickly understand that their lives will be infinitely better forever more.\n\nThe users will benefit from quicker configure checks, smaller downloads and not having to deal with the horrendous autotools if the build breaks. I don't think a build-dependency on python/SCONS is a problem for 95% of users or developers."
    author: "Max Howell"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> Using bksys instead of auto* on my projects has prevented me from\n> having to commit suicide.\n\nIf you'd read the auto* documents then you wouldn't need to commit suicide."
    author: "ac"
  - subject: "Re: Oh no, this is definately a shot in the own le"
    date: 2005-09-14
    body: "> If you'd read the auto* documents then you wouldn't need to commit suicide.\n\nI wouldn't say that reading the auto* documents is an experience that's going to cheer you up, however."
    author: "The Badger"
  - subject: "clueless troll"
    date: 2005-09-15
    body: "* KDE 4 already required python (Unsermake) and for months\n* The poster \"ac\" is not a KDE developer (it even looks like he works on Gnome)\n\nDo not feed the troll, Thanks."
    author: "anonymous_curious"
  - subject: "Re: clueless troll"
    date: 2005-09-15
    body: "a) Unsermake was also used by KDE 3 for people who liked using it, but there was no nesessarity for it too since people were doing fine using autotools for getting KDE compiled. So basicly no python requirements.\n\nb) Could you please refer me to the site on *.kde.org where it's written that you must be a developer to participate to KDE ? You are just attacking all the people who keep contributing to KDE (including me). We are not developing KDE by providing code (nontheless I am coder and developers but work on other stuff) but the KDE participants are still able to contribute with translations, webpresentations, documentations, audiovisual stuff and other things such as bugreporting and excessive testing. Still though I am skilled anough to talk my mind out of things that bothers me, after all the sentence 'free software as in free speach and free beer is still valid'.\n\nFor the part of not feeding the Troll. Please thanks for the advise, I am not going to feed you any further but I think it would have been fair to give you at least a normal answer.\n\nTill now I thought that KDE people were much more open minded for feedback given by others. It would be a pity to pop this bubble.\n\nI recall good enough that some weeks ago people kept praying the pester about Scons on IRC, at least I had that feeling (might be wrong but that time I surely had) I keep wondering myself what kept them changing so fast. Or is it just because you can flame ahead someone ?"
    author: "ac"
  - subject: "Re: clueless troll"
    date: 2005-09-15
    body: "Do you by a chance mix up this board with a mailing list where all this discussion already took place? Either way you are late with your concerns and not involved enough to make those who are believe in your case."
    author: "ac"
  - subject: "Re: clueless troll"
    date: 2005-09-15
    body: "Oh and by the way I never mentioned any KDE 4 since KDE 4 doesn't exist as of now. People started porting things from KDE 3 to QT 4 starting maybe 1 month ago or so and this will be KDE 4 one day. So why bother for things that don't exist yet.\n\nAnd for being acused to mortify scons and python loving people, please take my apologizes but reading about autotools being shit, perl sucks, etc. of course is not offending those who write it or contribute to it. My bad.."
    author: "ac"
  - subject: "Re: clueless troll"
    date: 2005-09-15
    body: "Fact: people have started working on kde4 at least 4 months ago http://www.kdedevelopers.org/node/1094\n"
    author: "anonymous"
  - subject: "Cross compiling"
    date: 2005-09-13
    body: "I must say this change is worrying me a bit.\nDoes the new build system provide cross compiling capabilities?"
    author: "Luciano"
  - subject: "Re: Cross compiling"
    date: 2005-09-13
    body: "That would not be a regression, as currently KDE cannot cross-compile.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Cross compiling"
    date: 2005-09-13
    body: "Maybe, but the build system is shared by the kdenox project, which needs to be built with a cross compiler.\nI suppose I will have to stick with the autostuff, then."
    author: "Luciano"
  - subject: "Re: Cross compiling"
    date: 2005-09-13
    body: "There is much more control over the compilation variables with this new system, so it will not be a problem."
    author: "anonymous"
  - subject: "Oh no..."
    date: 2005-09-13
    body: "Autoconf/Auromake was already pretty bad, but at least (sometimes) only bash was required, now I will need to install another one of those scripting languages I really don't need nor want to have installed. Ok ok, so bash is probably not the most elegant way to program something, but at least bash is compact in the sense that it doesn't spill a shitload of files all over the filesystem (often in the wrong places). I was pretty pissed when I found out one had to have perl installed to get Gtk and GIMP to build. Has Jam been considered as an alternative?"
    author: "Me"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "Yes, pretty much all alternatives have been considered, and the only serious contenders where scons/bksys and cmake.\n\nSecond, have you ever tried to maintain an auto*-based build ? I have, and like anyone who has I can tell you how incredibly brain-damaged this system is. And it does \"spill a shitload of files all over the filesystem\", much more than python.\n\nAs for not wanting to install python, seriously... what's the big deal. Apparently you're resisting it for purely \"religious\" reasons, well, too bad."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "No, I don't maintain any auto-* based build. This would be a nightmare, I DO agree with you on that.\n\nAs for not wanting python installed, that's not religious at all, it's just that I think that integrated solutions, neat and clean is a better way to go than \"just install everything anyone could every want by default\". I don't mind python or ruby or TCL or ECMAScript or whatever. People need to settle on just ONE DEFAULT ONE, integrate it the best possible in the system. But I think the situation where just everyone decide they'll use their favorite scripting language is more than suboptimal. It is OK however when the scripting language is used through a plug-in (that is choice) and you don't force those who don't need/want to use ten thousand scripting languages to install them. On the other hand I would choose TCL or Ruby over Perl and Python because these two last ones take up 50 megs of disk space."
    author: "Me"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "Are you serious?\n\nFirst, you're upset over 50 megs of disk space? Hard drives cost 50 cents per _gigabyte_ these days. Python costs you two and a half pennies to store. Give me your address and I'll send you a quarter, so you can store 10 copies of Python, and stop worrying. :)\n\nSecond, it's no more feasible to pick one scripting language and make it fit everyone's preferences than it is to pick either Gnome or KDE and make everyone happy. You wouldn't like it if someone decided that Gnome was The Linux Desktop and forced (somehow) everyone to stop working/using KDE, would you?\n\nThird, this is only a build-time dependency. If you're so concerned about not having Python installed, then get a pre-built version of KDE.\n\nFourth, is there actually a mainstream Linux distribution that doesn't include Python? I thought it was almost as standard as Perl and a C compiler."
    author: "Dolio"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: ">First, you're upset over 50 megs of disk space? Hard drives cost 50 cents per >_gigabyte_ these days. Python costs you two and a half pennies to store. Give >me your address and I'll send you a quarter, so you can store 10 copies of >Python, and stop worrying. :)\n\nThe main point was the first part of the reply but you chose to ignore it for some reason.\n\n>Second, it's no more feasible to pick one scripting language and make it fit >everyone's preferences\n\nNo realistically, it's just about not wanting to take a decision not to hurt the feeeeeeelings of 2 or 3 people.\n\n> than it is to pick either Gnome or KDE and make >everyone happy. You\n> wouldn't like it if someone decided that Gnome was The >Linux Desktop and\n> forced (somehow) everyone to stop working/using KDE, would >you?\n\nKDE, GNOME, Linux, *BSD, [add your favorite open source project here] will never succeed if there's no standards, integration, consistency. If it's going to be eternally just bits from here and there slapped together with no coherence whatsoever, that is if everyone has their way.\n"
    author: "Me"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> No realistically, it's just about not wanting to take a decision not to hurt the feeeeeeelings of 2 or 3 people.\n\nNo. People invent various languages because they're unsatisfied with the existing ones, and think that theirs is better. Programmers gravitate towards languages that they prefer. Until you turn everyone into mindless automatons, there will never be a single language that everyone prefers over every other, and therefore, there will always be more than one language.\n\nIt's not a decision that anyone can make or enforce.  If you think otherwise, you must be living in some alternate reality that I'm not aware of.\n\n> KDE, GNOME, Linux, *BSD, [add your favorite open source project here]\n> will never succeed if there's no standards, integration, consistency.\n> If it's going to be eternally just bits from here and there slapped\n> together with no coherence whatsoever, that is if everyone has their way.\n\nFirst of all, that's bull. Windows has just as much inconsistency, lack of integration, and lack of standards compliance as Linux, if not more. That has not prevented it from capturing and maintaining a hold on around 90% of desktop PCs.\n\nSecond, it's not as if Gnome, KDE and whoever don't work together on standards and integration when it makes sense. However, there are some things that are impossible to integrate. Qt is not GTK, and it probably never will be. As long as there are people who prefer each, they will continue to exist, and no amount of whining or philosophizing is going to change that. It's also not going to prevent Linux from making inroads on the desktop, because it doesn't really matter to most people.\n\nPeople coming from Windows are used to inconsistency. They're used to using programs with 7 different widget sets, and a myriad of different user interface decisions; they can live with two on Linux if they absolutely must. Suggesting that this is a significant stumbling point of Linux in the view of the average person is laughable. Linux desktops are already much more consistent than Windows desktops, yet that fact isn't harming Windows in any significant way.\n\nSimilarly, the average person doesn't care whether their programs are written in Python, Perl, Ruby, C++ or Brainfuck (let alone what languages are required for the build system of their programs). All they care is that they work. And at 2.5 cents or less per language, they can afford to have all of those installed, so that programmers can work in whatever they find suits the task best. Suggesting we should get rid of all but one because you find it to be somehow philosophically cleaner is nonsense."
    author: "Dolio"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "Yeah, and people need to settle on one single desktop, and stop making bugs in their code, etc... ain't gonna happen, in the meantime let's be a tad bit pragmatic. Python is quite well enough established for this to be a reasonable choice.\n\n(and choosing Tcl over Python because it takes less disk space is simply asinine)."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "> Yeah, and people need to settle on one single desktop, and stop making bugs \n> in their code, etc... ain't gonna happen, in the meantime let's be a tad bit \n> pragmatic. Python is quite well enough established for this to be a\n> reasonable choice.\n\nWithin one particular distro yes, it would make sense to pick one single desktop.\n\nBesides I use Slackware, Python is a not a dependancy at all, it's there though, if you want it you can have it. But you're free not to install it if you don't need it.\n\n> (and choosing Tcl over Python because it takes less disk space is simply\n> asinine).\n\nReligious? Anyway we'll talk about that again in a few months when another scripting language will be all the rage..."
    author: "Me"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "> Yeah, and people need to settle on one single desktop, and stop making bugs \n> in their code, etc... ain't gonna happen, in the meantime let's be a tad bit \n> pragmatic. Python is quite well enough established for this to be a\n> reasonable choice.\n\nWithin one particular distro yes, it would make sense to pick one single desktop.\n\nBesides I use Slackware, Python is a not a dependancy at all, it's there though, if you want it you can have it. But you're free not to install it if you don't need it.\n\n> (and choosing Tcl over Python because it takes less disk space is simply\n> asinine).\n\nReligious? Anyway we'll talk about that again in a few months when (yet) another scripting language will be all the rage..."
    author: "Me"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "> Besides I use Slackware, Python is a not a dependancy at all, it's there though,\n> if you want it you can have it. But you're free not to install it if you don't\n> need it.\n\nI fully agree with you."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> Religious?\n\nNo, I've used both, Tcl doesn't scale (in terms of lines of code) as well as Python.\n\n> Anyway we'll talk about that again in a few months when (yet) another scripting language will be all the rage...\n\nOh please. There are only 3 \"viable\" scripting languages : Perl, Python and Ruby. Perl is thankfully fading away, Python has its quirks but is the most mature, and Ruby is gaining ground but still needs \"packaging\" (its libs need to be documented for instance). Even though Ruby has been around for a long time it has only received wide exposure for about three years now. When was the last time a new scripting language was actually \"all the rage\" ? Python is as standard as a distrib component can get, get over it."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> Perl is thankfully fading away\n\n\nAny values to back this up ? Perl is stronger than ever these days."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "None whatsoever, I gladly admit this is based both on wishful thinking and random observation (I see more headlines about Python and Ruby than about Perl, and Larry Wall's \"Apocalypses\" about the upcoming Perl 6 are, hmm, well...)."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> None whatsoever, I gladly admit this is based both on wishful thinking and\n> random observation (I see more headlines about Python and Ruby than about Perl.\n\nAnd I keep hearing more about Ruby than Python these days so what."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-25
    body: "True, even though Python is more used, Ruby is \"in\" those day.\nStill Ruby will need more than a web framework to sustain the hype.\n\nAnyway I fully support Guillaume in hoping that Perl will die, though it will take time it is probably more used than Python+Ruby together.\nBut there are troubles in the Perl world: they don't manage to make Perl6, which (IMHO!) sucks also anyway.."
    author: "renox"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "Perl was all the rage in the late 90s with the web boom and the widespread emergence of web apps, for which Perl was usually the language of choice.  By the end of the 90s Java was eroding the high end of that market and PHP the low end.  As a general purpose scripting language Python and Ruby weren't as established.\n\nI also think that some of the shift has been due to paradigm shifts in people learning to program.  Despite it being possible to do OOP with Perl, it's ugly; Perl really is just better suited for iterative programming.  In the mid-90s new programmers were starting off with iterative languages, so Perl was a more natural transition.\n\nToday, on the other hand most new programmers start with object oriented languages and learn to think in object oriented terms.  As such it's natural that those people would gravitate towards object oriented scripting languages such as Ruby and Python.\n\n(And I should note at this point that I know Perl very well and don't know Ruby or Python -- so this isn't a Python-love-fest, just something I've observed.)"
    author: "Scott Wheeler"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "> get over it.\n\nWhy should I?"
    author: "Me"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "Because you won't be the one to fix it anyway?"
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "> the only serious contenders where scons/bksys and cmake.\n\nBesides people from KDE no one else ever heard of scons or bksys.\n\n> Second, have you ever tried to maintain an auto*-based build ?\n\nI did and I am still doing.\n\n> I have, and like anyone who has I can tell you how incredibly brain-damaged this system is.\n\nThere are good documents people should consider reading, if you are not willing to spent time reading the auto* documents then you are not suited to make good files that work perfectly with your software. The problem with KDE rejecting auto* is that everone I've met so far complained about auto* but in reality it's because no one really spent time reading the manuals, they all want some quick hacks so it works and they start to copy stuff from other modules and wonder why it doesn't really work. How comes millions of open source and free software except the stuff inside KDE works perfectly fine with auto* ?\n\n> As for not wanting to install python, seriously... what's the big deal.\n> Apparently you're resisting it for purely \"religious\" reasons, well, too bad.\n\nThat's not the point. I want to have and keep my choice of what I want to have installed on my system and not you decide what I should install. Scons will be only used in KDE and no one else in the world will probably give a damn about it except KDE. This is another step of alienation again from the KDE team of what is standard in open source. Standard still is the auto* stuff. If Scons would have been that great then by now the majority of project have switched to it and this even outside of KDE... And the reality of course looks differently.. Personally spoken for myself I do feel upset for this Scons stuff it has only been decided to be good because the author and some other two followers kept praising the hymn out of it."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "> That's not the point. I want to have and keep my choice of what I want to have \n> installed on my system and not you decide what I should install. Scons will be only\n> used in KDE and no one else in the world will probably give a damn about it \n> except KDE. This is another step of alienation again from the KDE team of what is\n> standard in open source. Standard still is the auto* stuff. If Scons would have \n> been that great then by now the majority of project have switched to it and this \n> even outside of KDE... And the reality of course looks differently.. \n\nthat's a bad argument. every new system, good as it is, needs one of two things to become \"famous\" (or standard if you like):\n1) a LONG LONG time of proving itself, slowly.\n2) a big \"showcase project\" \n\nwhen (2) happens, and big project starts using the new system, two things happen:\n1) system gets better, since a lot of testing is put into it\n2) system can get a chance to prove itself much much faster\n\nso, in this case, the project for bksys can be kde, and it might just prove itself better then autohell.\n\nhonestly, i don't have a clue about bksys but i know auto* is pain. \n"
    author: "Shie Erlich"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "Why are you ignoring and skipping comments and arguments and then come up with own replies that are far OT to what's actually written ?\n\nTell me the benefits of Scons over Auto* and I return here in a couple of hours crushing all your arguments to dust.\n\n;--- from another comment.\nd) If you read the autotool documents correctly then you would figure that autotools has to serve on a lot of POSIX compliant systems with dozens if not hundrets of different toolchains, compilers, headers and whatever. It's not limited to linux, it's not limited on windows, it's not even limited on operating systems such as MorphOS, AmigaOS (which are totally NON POSIX). That are the big advantages of autotools. You enter configure and then make afterwards. But you of course know perfectly the difference between scons and auto* since you never ever read a damn doc about it.\n"
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "If something works as well without reading docs like you get the other to work only after spending much time reading docs the former is surely the more comfortable solution, no?"
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "Can you tell us which autotool-based project you are maintaining ? Thanks."
    author: "anonymous_curious"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> Besides people from KDE no one else ever heard of scons or bksys.\n\nI don't think you quite know what you're talking about here. bksys is an scons extension specific for KDE. And if you haven't heard of scons, well, I hope the rock you're living under is comfy.\n\n> There are good documents people should consider reading [...].\n\nFirst of all, I think the people who wrote all the autotools-based build framework for KDE (S. Kulow being one of the first) are knowledgeable enough about it. Second, if a build system requires you to do more than quickly browsing through a few example files to be understood and used on your code, then it's just plain too complicated. A build system should never get in the way of development, and should not require extensive reading. That's plain broken.\n\n> How comes millions of open source and free software except the stuff inside KDE works perfectly fine with auto* ?\n\nYou don't know anything about KDE's build system, do you ?\n\n> I want to have and keep my choice of what I want to have installed on my system and not you decide what I should install.\n\nYes. Whatever. If you don't mind, we'd like to move on, but feel free to remain on your high horse."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> And if you haven't heard of scons, well, I hope the rock you're\n> living under is comfy.\n\nThere is no need to get sarcastic here. I initially heard about Scons through the KDE team and 2 people spending all their time on IRC convincing everyone how great it is while 3/4 of the developers kept crushing all their arguments to dust on IRC. If Scons would have been that great then by far 1/5 of the open source or free software world would have switched to it.\n\n> First of all, I think the people who wrote all the autotools-based build\n> framework for KDE (S. Kulow being one of the first) are knowledgeable enough\n> about it.\n\nSee, you simply answered one of my arguments above. From this sentence I understood it so. S. Kulow is the only skilled person around the KDE team who understands the autotools stuff. Which clearly says that the others who work on KDE (please pardon I don't want to sound offensive now) never bothered to read the documents of said toolchain. This most likely includes you too otherwise you would have realized that:\n\n;--- from another comment.\nd) If you read the autotool documents correctly then you would figure that autotools has to serve on a lot of POSIX compliant systems with dozens if not hundrets of different toolchains, compilers, headers and whatever. It's not limited to linux, it's not limited on windows, it's not even limited on operating systems such as MorphOS, AmigaOS (which are totally NON POSIX). That are the big advantages of autotools. You enter configure and then make afterwards. But you of course know perfectly the difference between scons and auto* since you never ever read a damn doc about it.\n\n> Second, if a build system requires you to do more than quickly browsing\n> through a few example files to be understood and used on your code, then it's\n> just plain too complicated.\n\nThere are dozens (if not millions) of example files for autotools floating around all over the net. Dozens of manuals, howto, tutorials. But then there is that 'reading' and 'understanding' problem again. What good does all tutorials, docs and howtos give if people won't spent time reading them ?\n\n> A build system should never get in the way of development, and should not\n> require extensive reading. That's plain broken.\n\nIt doesn't since it's part of the development process.\n\n> You don't know anything about KDE's build system, do you ?\n\nI do and I consider it a hack with all the *.in.in files that get concatenated to an *.in file which then get passed through the autotools to generate working configure and makefiles. Of course the best example in howto NOT use the autotools. A clear sign that the whole concept of the toolchain has not been understand correctly.\n\n> Yes. Whatever. If you don't mind, we'd like to move on, but feel\n> free to remain on your high horse.\n\nPlease pardon but that's the worst bit of ignorance that I've ever read from your side. You keep ignoring comments done by me and some other guy here, you only reply on the things you see fits best in your picture and pull other text under the carpet and you make Scons sound so ultimatively great without giving one example in what areas it improves.\n\nI would like to call you up for a challange to write a nice Scons tutorial and have a real life comparison between where you think Scons is better than autotools instead braging out how great it is. Until now I haven't seen anything about it except 2 people hyping hymns about it. The autotools* toolchain work on all sorts of systems, taking acount of all types of different hardware, toolchains, compilers, it even works on non POSIX systems such as Windows, AmigaOS, MorphOS and whatever other OS' and the best, it doesn't require Python.\n\nPlease also stop talking about HARDDISKSPACE is so cheap. This clearly shows that you live in a wasteful world and never really used a small system which is fast, maintainable and functional. The point is to have small systems and not monster systems. My entire system here is just 1.5 gb of space this includes X11R6, teTeX, Linux (+ Kernels), GNOME, Evolution, Abiword, Firefox, my home dir with all my files. Tar'ed up the entire System would be smaller than 590 mb of size and you can be sure I am not missing anything. I listen to music, watch movies, look at pictures, do documents, print, even play games. The point for using KDE is not just because of its superior architecture because I could save again 300 mb by simply switching from GNOME to KDE and getting rid of Python and the docbook crap. If you live ok with having a 240 gb harddisk and live perfectly by installing everything you find then it's your choice. But my choice (talking about choices here) is to keep and prefer small systems - a point you seem to not understand.\n\nKDE 3.5.x perfectly works without Python, I am not missing anything, everything works and I would have enjoyed to see the same for KDE 4.\n\nSometimes I have the feeling (and please apologize here) that KDE is walking alongside of the entire open source and free software movement (I said it's just a feeling). It starts with the way how KDE installs the files (pulling all includes in the top include dir, installing all libs without versioning) while the majority of open source and free software projects agreed to install their headers nicely sorted in subdirs so people know they are all keep together, give their libraries good versioning, make use of man files, info files. But under KDE all is stuffed in as it shows up. But ok chances are great that with KDE4 these issues are dealt with, there is a top voted bugreport for this on bko which is in the first 10 places of most voted bugs.\n\nAnd now with Scons, the vast majority of project imigrated to autotools these days this even includes XOrg and a few other really large and major projects without any problems (XOrg and Mozilla are quite big ones and yet they seem to handle the oh so bad autotools perfectly). And again KDE need to walk alongside with a silly buildenvironment."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> I initially heard about Scons through the KDE team\n\nWhenever you look for an alternative to 'make', scons comes up pretty quickly. I heard about it a couple of years ago I think.\n\n> If Scons would have been that great then by far 1/5 of the open source or free software world would have switched to it.\n\nThere's such a thing as inertia, and the Open Source community excels at it. Have you bothered to look at http://www.scons.org/refer.php ? Plus there's little doubt that KDE adopting it will give it a lot of spotlight.\n\n> the others who work on KDE (please pardon I don't want to sound offensive now) never bothered to read the documents of said toolchain\n\nYes, because they shouldn't have to. Again, a development tool should not get in the way of writing code. You should not have to waste time understanding it or making it fit your needs, otherwise it's broken, period. Yes, it's a part of the development process, but a part that must be kept as small as possible.\n\n> Dozens of manuals, howto, tutorials\n\nI've googled my share of autotools docs in the 5 years I've maintained RG's buildsystem, and those never turned up. I must have been blind or not entered the right keywords.\n\n> A clear sign that the whole concept of the toolchain has not been understand correctly.\n\nFeel free to explain Coolo how he's an idiot. Please don't forget to include patches.\n\n> Scons sound so ultimatively great without giving one example in what areas it improves.\n\nI've switched RG's build system from autocrap to scons/bksys. I had very little problems doing so, Thomas was very responsive to all requests, and the end result is a build system that's faster, lighter, and both easier to use ('scons' compared to 'make -f Makefile.cvs && ./configure && make') and maintain. For instance after 5 years we still had recurring problems with the simple addition of a '-fexceptions' flag to g++. Or adding a new QObject would require restarting 'make -f Makefile.cvs' otherwise you'd get obscure link errors. So we're saving time both for us developers (less problems in building the whole thing, less time wasted in answering users who have trouble in building it) and users (easier for them to build it).\n\n> This clearly shows that you live in a wasteful world and never really used a small system which is fast, maintainable and functional.\n\nDude, 1.5 Gb ?? Do you have *any* idea how wasteful that is ? Back in the 80's my //gs had a 40 Mb (read that : MEGA-bytes) HD, all with OS, GUI, and office stuff. And even back then that was already considered a waste of resource (after all the apple //e was just as useful with a couple of 128Kb floppies). 1.5Gb is a super-computer which should be serving dozens of users. And you have all that just for you ? What a waste.\n\nNow seriously, if you can't spare the $50 a 80Gb costs these days, tough luck, but on my side the limited resource is *time*, not HD space, so I'm not going to waste it because you insist on using a 10 year old system. I don't care about your choices, I don't even care about leaving you choices when it diverts me from writing useful code for real non-geek users, and the same reasoning is applied by all sane open-source developers. It's pointless to hurt the majority of real users by wasting time just to satisfy the silly needs of a few uber-nerds.\n\nAs for the whining about KDE not following standards, yes, let's all stick to C (after all if you can't read on how to write OO code in C, you've no business programming, right ?) and Motif (which was standard 10 years ago)."
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "> Dude, 1.5 Gb ?? Do you have *any* idea how wasteful that is ?\n\nPlease stop that sarcasm, we all know that we talk about 2005 and not what was 25 years ago with out 20 or 40mb harddrives. I come from these times and know pretty well. The demands and requirements nowadays are a hell lot differently and 1.5 gb for Linux measures are quite small but yet a full featured system.\n"
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "You're putting down the work of pretty much everybody here who's trying to provide useful software for people, pompously saying that \"people should read the autotool docs\", or that KDE's build system is an example of misunderstanding the tool chain, that standards should be respected and other stereotypical statements usually coming from people who have no experience in software development, all this because you cling to an obsolete system, and you expect me to remain polite or even take you seriously ?\n\nJust what software are you working on, so we can all download it and bask in the light of your infinite wisdom ?\n"
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "There is no need for you to get offensive.\n\na) I apologized many times that the stuff I write shouldn't be taken as offense. It just is my very own opinion about what I see and what I hear.\n\nb) I very well respect the work KDE developers have done to provide a serious plattform for good software and integrated desktop experience.\n\nc) I still believe that the problems around autotools is just a *placebo* thing that got repeated so often till everyone started to believe it while this argument is plain holdless. Personally I believe that the autotools system is quite mature and works perfectly. The problem is that people need to learn it, to read documents, read tutorials and of course that people do help each other to get around of children's disease. You say the programmer should concentrate on programming. I say that to learn programming you need to learn the language, the api, the system, the architecture and so on. That's quite a lot of what you need to learn to start getting something serious done. I talk from quality software here and not banal stuff as written in teaching books. Since I am a long years experienced programmer I do know what I am talking here. To learn autotools you need to be willing to spent time reading about it. The same would be a valid sentence for Scons and anything else that is new.\n\nd) I never said that Coolo is a moron (as you described it). I only said that Coolo seem to be the only person who get used to the autotools (non)issue because he was willing to read about it and skilled enough to handle the issues for the entire KDE software that exists. Look one person alone to fix everything. Now imagine everyone would have at least half of the autotools skills of Coolo and by now no one would complain about it.\n\nc) I probably asked you a couple of times now to name at least a few 'advantages' of Scons over autotools and yet you keep insulting me on a public forum like dot.kde.org for no reasons. And still I also said (I hope I did) that I have no issues with Scons per se. I have issues with the requirements of Python as buildtime. Not that it just is an requirement at buildtime, you also build other .py files and install them from other KDE components (that you usually won't get installed and compiled if no Python is found). So what was a buildtime requirement ends up in becoming a runtime requirement as well.\n\nd) Please don't ask me on what software I was working on because it's by far offtopic to this conversation and would only end up in deflecting from the real problem. I by far have programmed, worked, maintained far more software than many other people around I believe, I must admit that personally I have only 3 or 4 programs that require the autotool chain, the rest usually were Makefile based, some Imake based stuff from former times and a shitload of ASM 68k software that from mechanism and build procedure works a bit differently (handled through a macro assembler, IDE, editor, debugger usually written with ASM-One on former Amiga times). Today I maintain some parts of the Linux Kernel, work on my own build system, some own software that I keep working at times and some other things.\n\nBut please would you start to be willing to name a few advantages of Scons over autotools ? Since you seem to be a clever guy that keeps defending Scons like it's written by you (maybe it is who knows) you might be willing to come up easily about a few advantages. It's your luck that python recently got ported to systems like AmigaOS4 or MorphOS otherwise we wouldn't be able to use the glory Scons system without Python but we would have been able to use autotools since 1996. I don't want to know how many non posix systems exists where you don't have python existing.\n\nAnd please, I would respect you even more if you would reply in a mature way to me, even if I am an anonymous person I still try to be respectful to you and be as honest as even possible. Something you probably wont see every day from AC's."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "OK, you're right, I'll try to calm down.\n\n> You say the programmer should concentrate on programming. I say that to learn programming you need to learn the language, the api, the system, the architecture and so on.\n\nI agree with that, but the build system is not, or should not, be part of this. Would you use a compiler which required extensive reading just to understand how to compile a single C file ? Unless you had no choice, you'd probably prefer it to be as simple as \"cc foo.c\", right ? It's the same for the building system, or the text editor, the IDE, the version control system, the documentation generation system, etc... I make a clear distinction between development tools and development platform (for instance, C++/QT/KDE, or Java, or C#/.NET). Learning a platform is mandatory of course, even though it should also be as simple as possible. But your IDE, compiler, and build system should never ever get in the way, and should not require you to dive into tons of tutorials and stuff just to get simple things done. And in that regard, the autotools fail utterly, while scons is rather successful.\n\n> Now imagine everyone would have at least half of the autotools skills of Coolo and by now no one would complain about it.\n\nAnd everyone would spend time on autotools rather than on their code. Again, time is the critical issue.\n\n> I probably asked you a couple of times now to name at least a few 'advantages' of Scons over autotools\n\nI did in my last post. To sum up, it's just much simpler to maintain and use, simply not having this 2-level generation of files is a huge win. Also having all in one script (configuration and building) helps immensely. It also handles conditional compiling very easily. Just try it for yourself, you'll see.\n\nNote that I'm not a huge fan of scons, I prefer Ruby over Python, but the only ruby-based build system (rake) is even more exotic than scons is, and doesn't handle configuration tests. But scons is still much better than autotools.\n"
    author: "Guillaume Laurent"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "ac, you want a few advantages of scons over auto*? (Haven't others already given some, like that its simpler to customize, or the links to the experience with scons on other platform? You seem to constantly ignore them and demand other advantages. On the  other hand you complain about misinterpreting if only a small part of you posts is not directly answered.) \n\nAnyway, here are a few facts:\n\n-) The ability to build one target with all its dependencies (and only the dependencies). E.g. in kdepim you can build only korganizer/korganizer and the required libs (libkcal/libkcal.so, libkdepim/, etc.) are also built, but the rest of kdepim won't be built.\n\n-) Do parallel builds in a compile farm (like -jN in automax) without launching the linker N times locally, too (which basically locks up your machine).\n\n-) Dependencies (on libs in other dirs, generated headers in other dirs, etc.) are really generated from the actual code, not implicit like in automake (where the order of the subdirs is relevant because they will always be compiled in the order given).\n\n-) No PID overflow on systems like windows (see Jan's mail, link above, which you constantly ignore)\n\nOf course, this list if not exhaustive.\n\nOne thing you claim is that no-one except KDE apps use scons: How about id Software (Doom) using scons, or blender using scons on all their platforms (Linux, Windows using both Microsoft Visual C and Cygwin, Solaris, Mac OS X, IRIX and OpenBSD)?\n\nAlso, by now we all seem to agree that coolo has a fairly good knowledge about the auto* systems etc. Why do you think was he the first to start a replacement for automake for the KDE build? \nYes, he's the guy who wrote unsermake (which is now recommended to build KDE; and btw it is a python script...) to cure the problems he couldn't overcome with the auto* toolchain.\n\nSince you don't seem to use KDE anyway (let alone build it...), why is it so important to you whether the KDE build needs python (scons) or perl (autoconf)? \n"
    author: "Reinhold"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "First of all, I would be thankful if you could stop saying that I am permanently ignoring things, which I clearly don't. I haven't ignored those mails if you refer to clee's posting which contains a bunch of links. To say the truth I kept reading them and even replied to clee but that mail didn't made it to dot.kde.org for some unknown reasons.\n\nI also mentioned that scons is by far useless on systems that don't have python and there are a few of them (maybe even hobbiest systems) which are not unix like and not posix conform (which they mustn't). These systems however have perl and the autotool chain. So talking about cross plattform, it doesn't make sense to use something regardless how great it is if the host system don't support python. But this just as a sidenote and also not the worries I tried explaining. Again since you seem to have ignored more of my writings than I igored a few links posted by someone. My problems are not Scorns related, my problems are the dependency of python that I'd like to avoid. And no I also don't tell developers what to use as some others told me on IRC, I only asked whether the autotools stuff can still be supported for those who dislike having python around.\n\nThere are also people who keep telling me that perl is bigger than python and started doing some weak calculations that with scons I can get rid of perl, autoconf, automake, m4, libtool and only keep python and scons. This would be nice in a real life scenario but if you have ever dealt with building up Linux systems then you realize how many damn packages (there are tons of them) do require perl, m4, automake, autoconf, libtool and other things. So I am not able to get rid of them. The calculation 100kb scons vs. 600kb autotools is wrong too, saying that I can save 30mb perl + 6 mb autotools, libtools, autoconf is wrong as well. Correctly said it has to be (for the upcoming KDE and for the underlaying system to get build)\n\nPerl 30mb\nLibtool 3mb\nAutoconf 2mb\nAutomake 1mb\nM4 1mb\n\nAnd now with the dependency of python you can add\n\nPython 40mb\nScons 100kb\n\nOntop of that. Till now even by reading the Links above I don't see the problems with autotools on Cygwin, Solaris, Mac OS X, IRIX and OpenBSD and honestly I do believe that these are just excuses. I don't see any issues since there were no issues posted. Only the reminder that it would be better to use scons because they had issues with autotools, but which ones are not mentioned. But then I only bothered reading the Links posted there.\n\nInstead of slamming the entire autoconf, automake, m4 and libtool people (since I get accused for slamming the scons people) why not helping them to improve autotools to become better, fix the bugs that exists ?\n\nThe thing with Id-Software and Blender should be seen differently:\n\na) I don't have to compile Id-Software, most of their stuff is propritary for years anyways till it gets released open source one day (if we refer to doom here)\n\nb) Blender is something not really necessary for my system, it's more or less a small tool that can be converted to configure if someone likes doing this.\n\nc) KDE is a different story I somehow expect it to seamlessly fit into the unix world, fit into the way and common arrangements of installing files (headers, libs, manfiles, infofiles) as most other (basicly everything these days) are doing. KDE still does not:\n\nhttp://bugs.kde.org/show_bug.cgi?id=86986\n\nAnd I would also have loved to see that KDE feels more unix'ish beneath it's desktop. Better autotools support, better dealing with libtools, better organization of the files. Manfiles, touching of files when they are installed (tons of files are still copied). These are the things I'd really expected. Right now where we finally get rid of all the different build mechanisms such as Imake (which isn't bad by the way) where we get rid of custom written Makefiles that are hard to deal with. Right now where major solutions such as XOrg started switching to autotools. Finally I thought that everything would pull on one rope and start moving to one direction.\n\nI also prefer KDE for not depending on so many languages for getting its core build. There are bindings that people can use to code tools in the language they prefer but it was no dependency for building KDE from source which made it an interesting thing because you could delete bloat around it (no requirements for extra doocbook installation, no requirements for python, you also need curious mono dependency and other nasty thing, you simply ran a small kde build script which executes configure and you are done).\n\nThis little rant is also not about cheap diskspace or why I and others have to justify why we should or should not install python. It's simply because we still have the choice to shape our Linux systems the way we want it. We can easily add python, maybe ruby one day and then mono because someone had a brilliant idea, but we don't see the reasons for installing big bulky packages of nonsense just because some small script requires it otherwise there would be no progress further to get your desktop of choice compiled.\n\nOf course Scons may be the better choice over autotools and that's still not what worries me, but I would really have prefered to stay without python. What's so hard to understand here ?"
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "> I only asked whether the autotools stuff can still be supported for those who dislike having python around.\n\nAs you have now volunteered to maintain the autotools based KDE build system and be available for any third party developer that might have questions regarding autotools, can I look forward to see your improvements in the 3.5 release or do you need more time, given that you have to maintain it also for the 4.0 branch\n\nYou are really taking on a huge job here, there should be more contributors like you\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "I still wait for some of my autotools patches to get committed to SVN they are waiting on bko for quite some months now. Even asking back whether it's ok to committ them didn't return any answer so far."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "What bug number? (So that I can check what has happened?)\n\nAs for commiting yourself, as it is probably in kde-common/admin, you cannot do it.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "Well, I tried to work with autotools and tried to read the documentation. I didn't have success. You need to learn:\n-m4\n-libtool\n-autoconf\n-automake\n\nThese are just too many things to keep them all in your head if you use them only from time to time.\nWhile I myself prefer cmake over scons, scons will definitely an improvement over autotools.\n\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "But these tools m4, libtool, autoconf, automake are existing for how many years now ? You can ask basicly everyone to help you with it, you can even go to the next chinese fish store to ask some linux guru to help you while you probably won't find that many people helping you with Scons problems. But then again, Scons isn't really my problem. Python is (as if I haven't said this two dozen times now)."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "\"But these tools m4, libtool, autoconf, automake are existing for how many years now ?\"\n\nFor so long that we're all born with inate knowledge of them?\n\n\"You can ask basicly everyone to help you with it, you can even go to the next chinese fish store to ask some linux guru to help you\"\n\nDream on! Makefile and autotools gurus may be able to help you, but they're nowhere near as common as you make out. Even if the Internet is littered with autotools files, as you suggest elsewhere, that doesn't necessarily make troubleshooting any easier, as anyone with any experience of TeX authoring will tell you.\n\n\"while you probably won't find that many people helping you with Scons problems.\"\n\nSure, specific SCons experience is likely to be limited, but at least there's a chance that people will either get the language or get up to speed with it, rather than getting bogged down with the usual horseplay that shell-inspired toolsets entail.\n\n\"But then again, Scons isn't really my problem. Python is (as if I haven't said this two dozen times now).\"\n\nIf only to contradict yourself in two dozen other places. But to distill this objection to Python down, it would appear that you see the installation of a tool you don't use as \"wasteful\" - in which case I imagine that you roll your own minimalistic Linux distribution in order to avoid all the other wasteful stuff that gets installed. Or don't tell me: you've said all this but actually run Fedora Core?!"
    author: "The Badger"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: ">You can ask basicly everyone to help you with it,\n\nI'll take your word for it. I encountered a small case a few days ago, can you please explain it to me. Why does it behave this way? I had a build error in the kdeaddons package when building noatun-plugins/blurscope. For some reason it decided to try linking to KDE files(libkdeprint,libkparts etc) in /usr rather than where KDEDIR is(/opt). I have some old KDE binaries in /usr as a backup solution when SVN builds to /opt fails, but no -devel RPMS so it does not find any .so files to link with there. Making one small change to the Makefile.am made everything build correctly. So please explain to me why these two lines works differently? \n\"noatunblurscope_la_LDFLAGS = $(all_libraries) -module -avoid-version -n o-undefined `$(SDL_CONFIG) --libs`\"\n\"noatunblurscope_la_LDFLAGS =  -module -avoid-version -n o-undefined `$(SDL_CONFIG) --libs` $(all_libraries)\"\n\nWhy does one make it link against /usr and the other against /opt? "
    author: "Morty"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "If I understand your problem correctly which I assume I do, then this is clearly the old known precedence problem (where to search first).\n\nBut sadly it's not just libtool or gcc alone. They search in the default paths for libraries first. So if you have older libraries sitting in /usr and have the new ones compiled and installed in /opt/kde then you can be sure that libtool and gcc seeks for libraries in /usr first rather than in /opt/kde.\n\nSomething you can not even solve with LD_LIBRARY_PATH. The correct solution would be to remove the old libraries in /usr anyways to avoid the problems. I encountered these issues some years ago when maintaining my own well known buildscript for another desktop environment.\n\nThis is no Makefile issue, nor is it a configure or autotools issue. It's an issue of precedence of where the compiler and linker or libtools is searching first.\n\nIIRC JHBuild on freedesktop.org provides a patch for libtool to search in ${prefixdir}/lib first. But then these bugs are solvable it's just a matter of time that someone writes a bugreport and sents it to the maintainers so they can add it. Nothing wrong about that."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "If the problem is old and known and there is a patch around, why is it necessary to write a bug report and send it to the maintainers?"
    author: "Roberto Alsina"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "The patch comes bundled with the jhbuild package, I don't know why it wasn't forwarded and I never bothered to ask either. Could be that it was sent to them and rejected, who knows."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "> This is no Makefile issue, nor is it a configure or autotools issue. It's an issue of precedence of where the compiler and linker or libtools is searching first.\n\nThat doesn't answer the question, why the change fixed the problem. Why does putting $(all_libraries) at last posititon change the precedence?"
    author: "Christian Loose"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: "I can not answer this without knowing the whole problem case. This also requires to know what system he uses, what's installed on it, what he really did, etc. Giving an direct answer to this over dot.kde.org without knowing all facts would be far to unserious. But all I know so far is that this is not an autotools issue. It's of course clear that if I force the precedence to be differently that stuff may work as in this case."
    author: "ac"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: ">The correct solution would be to remove the old libraries in /usr anyways to avoid the problems.\n\nNo it's not the correct solution, for several reasons. First the ability to have several versions of KDE installed is a feature, and I don't think removing the stable version for unstable/svn version would be particularly popular by some of the users on this box(me included:-). \n\n>This is no Makefile issue, nor is it a configure or autotools issue. It's an issue of precedence of where the compiler and linker or libtools is searching first.\n\nHow come simple changes to Makefile.am make it work or not, without any other changes anywhere. If it had been that simple the rest of the package would also have had failed building, and not only the one lone directory. As $(all_libraries) are positioned both places in the rest of the package. And the same can be said of the 10 other KDE source packages which builds correctly in the same environment, not having any precedence problems. Personally I think such unpredictable behavior in the build systems clearly confirms it has major issues."
    author: "Morty"
  - subject: "Re: Oh no..."
    date: 2005-09-14
    body: "To quote Mozilla's webpage on why they're not using another build system like scons or ant:\n\"Mainly, because no one has implemented these systems for Mozilla.\"\n"
    author: "Reinhold"
  - subject: "Re: Oh no..."
    date: 2005-09-16
    body: "Though this thread is probably long since dead, I can't resist chiming in for a rant:\n\n> There are good documents people should consider reading, if you are not \n> willing to spent time reading the auto* documents then you are not suited to \n> make good files that work perfectly with your software. The problem with KDE \n> rejecting auto* is that everone I've met so far complained about auto* but\n> in reality it's because no one really spent time reading the manuals, they \n> all want some quick hacks so it works and they start to copy stuff from \n> other modules and wonder why it doesn't really work. How comes millions of \n> open source and free software except the stuff inside KDE works perfectly\n> fine with auto* ?\n \nAnd this is precisely, where the problem is. I want to develop cool software. I don't want to spend hours wrestling with makefiles. I want them to just work. And when they don't work, I want to be able to understand, why not, and fix them, without having to read a week's worth of docs.\n\nAlso, I do want to just \"copy stuff from other modules\", maybe not blindly, but the fact alone that this is non-trivial is just plain bad. I have literally spend days of my life pondering layer after layer of files, auto-generated from files, auto-generated from files, each generation process using a different language - all just to track down some small annoying bug preventing the build on some other people's systems (where it went fine for me, so much about portability).\n\nAnd yes, I have read the docs. Not from start-to-end, I admit, but trying desparately to find a pointer to my problems. Generally with little luck. In the end, so far, I've always found a solution, so, yes, I guess that goes to say, auto* can do about anything you want, if you just use it right. And millions of projects have somehow managed to tame the beast, eventually. But at what cost?\n\nAdmittedly, I don't know scons much, and I'm sure it has its quirks, needs testing on exotic architectures, etc, maybe even one or two more years to be considered mature. But at least: It's consistent within itself: Using only one language instead of four. It's supposed to be modular - hooray! - finally we will actually be able to understand _and influence_ what's going on in the build process without years of studying auto-stuff.\n\nModularity and consistency - if you can't achieve those in your framework, I'm sure any programmer will tell you, then it's simply flawed. Better ditch it sooner rather than later."
    author: "tfry"
  - subject: "Re: Oh no..."
    date: 2005-09-13
    body: "> I was pretty pissed when I found out one had to have Perl installed to get Gtk and GIMP to build.\n\nDid you know that building KDE programs from the repository in KDE3 required Perl and M4 ? Did you know that unsermake was (and still is) required to build KDE4, and that it was Python-based too ?\n\n> Has Jam been considered as an alternative?\n\nYes, and Ant too, and many other tools."
    author: "anonymous_curious"
  - subject: "Re: Oh no..."
    date: 2005-09-15
    body: ">Did you know that building KDE programs from the repository in KDE3 required Perl >and M4 ? Did you know that unsermake was (and still is) required to build KDE4, >and that it was Python-based too?\n\nI don't build KDE from repository, but from the standard packages, like most people who build KDE do. Since I've never had to use 'unsermake', I didn't even notice it so I don't feel concerned really.\n\n> Yes, and Ant too, and many other tools.\n\nDid you check out Haiku's build system?\n"
    author: "Me"
  - subject: "Using it as a non developer."
    date: 2005-09-14
    body: "Like many people I don\u0092t develop KDE but I do compile it in order to use it.\n\nWill I still be able to do:\n\n./configure \nmake\nmake install\n"
    author: "non-developer"
  - subject: "Re: Using it as a non developer."
    date: 2005-09-14
    body: "Yes.\n\nThere are wrappers so it does not change anything for users, except for the \"make -f Makefile.cvs\" (before configure) which is removed."
    author: "anonymous"
  - subject: "Re: Using it as a non developer."
    date: 2005-09-14
    body: "It would be really advantagous to me if it made the configure faster too. It keeps on checking stuff over and over again for each program I compile, and when you're compiling KDE, there are a LOT of programs...\nI just hope KDevelop will make it easy to work with this buildsystem and convert programs from auto* to this new buildsystem. "
    author: "Andre Somers"
  - subject: "Re: Using it as a non developer."
    date: 2005-09-15
    body: "What you don't understand about autotools and why it requires so long for configuring is that you limit your view on Linux based systems only. Forgetting all the eventuality of other systems such as solaris, windows, cygwin, morphos, amigaos, etc. They all have different things that needs to take account for. It's a huge complex thing and all eventualities needs to be taken care of. So the slowness as you see it is no slowness of the scripts, it's the slowness because all eventualities has to be taken into account. That's why autotools is so strong and that's why it works perfectly on a huge number of systems, even non POSIX and non UNIX systems."
    author: "ac"
  - subject: "Re: Using it as a non developer."
    date: 2005-09-15
    body: "Weird. When I tried building a bunch of auto* based software on Solaris 9, I ended having to install bash, GNU m4, perl, GNU awk, and GNU make or they wouldn't even run their configure scripts properly.\n\nIt's probably just me, I suppose."
    author: "Roberto Alsina"
  - subject: "Re: Using it as a non developer."
    date: 2005-09-15
    body: "the configure script which is supposed to be distributed with a package requires only a bourne-compatible shell plus some shell tools. it should _not_ require autoconf, automake, perl or m4."
    author: "yaac"
  - subject: "Autools work perfectly on many systems ?"
    date: 2005-09-15
    body: "I have worked on HP-UX and Tru64 and autotools rarely worked as expected. Bash or zsh or coreutils would not build properly on Tru64 for example.\n"
    author: "anonymous_curious"
  - subject: "Re: Using it as a non developer."
    date: 2005-09-15
    body: "> It would be really advantagous to me if it made the configure faster too. It keeps on checking stuff over and over again for each program I compile, and when you're compiling KDE, there are a LOT of programs...\n\nIndeed. We are thinking at the moment on a re-using the Qt data somehow (qmakespec).\n\n> I just hope KDevelop will make it easy to work with this buildsystem and convert programs from auto* to this new buildsystem. \n\nA proof of concept for kde3 is available at:\nhttp://freehackers.org/~tnagy/pmanager-0.6.4.tar.bz2\nKdevelop only needs to create an xml file that bksys can parse to build the targets.\nI have exposed api for writing a Makefile.am parser but it looks like developers were not really interested in doing it.\n\nPManager is too limited at the moment, and it will be rewritten once the KDE4 port is ready.\n"
    author: "Ita"
  - subject: "Sendmail"
    date: 2005-09-15
    body: "Everyone should use sendmail because it's always been used and if you can't understand the config files then you obviously are too dumb to read the various documentation.  Distros should never ship non-standard MTA's such as postfix even if they are easier to set up because everyone already knows the sendmail config files and you could ask anyone on irc and they'd be able to answer it for you.\n\n</sarcasm>"
    author: "Backwards"
  - subject: "Re: Sendmail"
    date: 2005-09-24
    body: "LOL - well-put!\n\nI'm not much of a Python fan, but if the KDE devs can decide on it, then, oh well!  That's their decision.  Now we get to look forward to years of complaining and bitching about a choice of base dependencies.  Heck, maybe RMS will find some obscure reason that the Python licensing scheme violates the GPL so people can hate KDE all over again. *wink*"
    author: "regeya"
---
At the build system BoF at aKademy it was decided to start moving the KDE 4 build system from <a href="http://www.kde.me.uk/index.php?page=unsermake">unsermake</a> to the <a href="http://www.scons.org/">SCons</a>/Python based system <a href="http://www.kde-apps.org/content/show.php?content=19243">bksys</a>.  To find out more about this important future technology, KDE Dot News talked to its lead developer Thomas Nagy about the reasons behind the change and what it will mean for KDE developers.



<!--break-->
<div style="float: right; width: 130px; border: thin solid grey; padding: 1ex">
<img src="http://static.kdenews.org/jr/ita.jpg"><br />
Thomas is ita on IRC
</div>

<p><strong>Please introduce yourself and your role in KDE.</strong></p>

<p>My name is Thomas Nagy (I am also known as 'ita' on
freenode). I have spent some time writing some KDE
utilities (solvers, small games) and adding
some features to KDE applications such as KOffice and Kalzium
when i was still a student. Since i graduated i have
spent more time trying to create complete applications
like <a href="http://freehackers.org/~tnagy/kdissert/">kdissert</a>, a mind-mapping tool for the creation of
documents and its build system <a href="http://freehackers.org/~tnagy/kdissert/bksys.html">bksys</a>.

<p><strong>What is SCons and what is bksys?</strong></p>

<p><a href="http://www.SCons.org/">SCons</a> is a software construction tool which is used to
build and install programs. It is written in Python
and its design is modular, making it easy to extend
for use with unsupported compilers or complicated
build rules.</p>

<p>bksys (as in Build Kde SYStem) extends SCons and aims
at replacing the autotools. It has been written in the
main place for compiling and installing kdissert and
became more modular over the time. The most important
feature is the object-oriented style API that can be
accessed from either Python code or throughout XML
description files.</p>

<p>Some more documentation describing the framework can
be found in the the slides from my <a href="http://conference2005.kde.org/slides/software-construction-tools-talk--thomas-nagy.pdf
">aKademy 2005 presentation</a>.</p>

<p><strong>Why is bksys separate from SCons?</strong></p>

<p>bksys aims at replacing completely autotools for
common projects, so this goes way beyond SCons' main
goals.</p>

<p><strong>In which way are they better than autotools?</strong></p>

<p>There are many improvements over the autotools, but I
will try to describe the most striking ones:</p>

<ul>
<li>Reliability: targets are rebuilt if compilation
flags are changed or if md5sums of the files changed</li>
<li>Size: 100KB (miniSCons included) vs 600KB
(autotools)</li>
<li>Simplicity: one language (Python) versus at least 3
(m4, posix sh, make) for autotools (GNU/make,
automake, autoconf, libtool); no intermediate file to
generate for the compilation process (Makefile.in,
Makefile)</li>
<li>Configuration: the configuration process is fast,
easy to control and to modify</li>
<li>Flexibility: SCons can be extended easily thoughout
modules. Besides, helpers can be written easily to
help programmers (take all source files in a
directory, or check automatically the consistency of
the builds for example). SCons also has excellent
support for win32 and OSX platforms, it handles many
languages like Java or Fortran out of the box.</li>
<li>Evolution: the API and the code written for
compiling the targets are re-usable so projects using
it will not be locked.</li>
</ul>

<p><strong>How do they compare to <a href="http://www.kde.me.uk/index.php?page=unsermake">unsermake</a>?</strong></p>

<p>Unsermake is a replacement for make and automake only.
Bksys replaces the whole autotool chain.</p>

<p><strong>Can you summarise the build system BoF at aKademy?</strong></p>

<p>Several candidates were evaluated. QMake is way
too limited and cannot be used at all. Unsermake is
not to be kept, as it is only a partial solution and
emulating the behaviour of automake and make consumes
a lot of resources. Two solutions remained in
competition for the future KDE build system: CMake and
SCons.</p>

<p>CMake is a Makefile generator like QMake; it uses a
macro language which is known to present several risks
as non-standard targets arise frequently in KDE. On
the other hand SCons is much more flexible as it is
based on Python, and was proven to work on fairly
complex applications already (KDE Games, Rosegarden,
..). SCons was then chosen and the conversion of
kdelibs 4 has started.</p>

<p><strong>So what work needs to be done now?</strong></p>

<p>So far, the main goal of bksys has been to replace
autotools for usual KDE projects. Now for kdelibs the
situation is a bit different as the tool will be the
base of all KDE applications, so a new framework has
to be designed, in particular:</p>

<ul>
<li>We have to agree on a new set of API (stardard
targets, naming, features...)</li>
<li>New bksys modules have to be written for detecting
the system configuration (full autoconf replacement)</li>
<li>Some work has to be done for writing config.h files</li>
<li>More helpers will have to be written to make
programmers lifes easier (scan for sources
automatically..)</li>
</ul>

<p>At the very moment, the bksys scripts have been added
to KDE SVN, and 3 main directories in kdelibs 4 are
already compiling (dcop, libldlt, kdefx). There is
some documentation in <a href="http://websvn.kde.org/trunk/KDE/kdelibs/bksys/design">trunk/KDE/kdelibs/bksys/design</a>.
The main aspects of the future framework are
presented, and some questions are left opened to
discuss.</p>

<p>In the past, the kdegames module was converted in
about 2 days, but for kdelibs it will take a lot more
time as even after the framework is complete some
reverse-engineering will have to be done on the
Makefile.am to achieve the conversion. I am striving
at the moment to get a first version of the framework
ready (most configuration modules complete and a few
folders compiling) to show how the new system will
look like and the main advantages of using it.</p>

<p><strong>Who is going to do the work?</strong></p>

<p>.. or who is working on it already? Three people have
committed code on bksys in svn so far, Simon
Haussmann, Stephan Kulow and I. Interesting ideas have
been raised on the IRC, and I am awaiting
more comments, when the first version of the framework
is ready for testing (a request for comments will be
posted).</p>

<p><strong>Are non-KDE projects taking up SCons/bksys?</strong></p>

<p>Most projects using SCons do it directly and use their
own framework, like Blender or Rekall.
The projects using the bksys layer i know of are
almost all KDE-oriented: <A href="http://www.rosegardenmusic.com/">Rosegarden</a>, kio-locate,
<a href="http://kde-apps.org/content/show.php?content=14597">skim</a>...</p>


