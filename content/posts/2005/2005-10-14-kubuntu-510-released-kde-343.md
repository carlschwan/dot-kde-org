---
title: "Kubuntu 5.10 Released With KDE 3.4.3"
date:    2005-10-14
authors:
  - "jriddell"
slug:    kubuntu-510-released-kde-343
comments:
  - subject: "Just installed it..."
    date: 2005-10-14
    body: "It seems cool so far. I only have some trouble mounting disks. Good pork!"
    author: "Qlew"
  - subject: "dist-upgrade"
    date: 2005-10-14
    body: "Im not so good at debian (I come from Fedora an Mandrake), so how about some links/instructions fo \"aptitude dist-upgrade\""
    author: "Mario"
  - subject: "Re: dist-upgrade"
    date: 2005-10-14
    body: "edit /etc/apt/sources.list, replace everything that says 'hoary' with 'breezy', then do\n\nsudo apt-get update && apt-get dist-upgrade\n\nShould work :)"
    author: "Narg"
  - subject: "Re: dist-upgrade"
    date: 2005-10-14
    body: "Don't forget the second sudo!\n\nsudo apt-get update && sudo apt-get dist-upgrade"
    author: "Dave"
  - subject: "Jigdo DVD download"
    date: 2005-10-14
    body: "Is there any jigdo file for the Kubuntu /DVD/ available? TIA"
    author: "me"
  - subject: "guidance"
    date: 2005-10-14
    body: "Is Guidance going to replace KControlCenter? Is there also a gnome frontend available?"
    author: "a"
  - subject: "Re: guidance"
    date: 2005-10-14
    body: "Is Guidance going to replace KControlCenter?\n\n-> No, actually Guidance can be used in kcontrolcenter :)\n\nIs there also a gnome frontend available?\n\n->No, why?"
    author: "rinse"
  - subject: "Re: guidance"
    date: 2005-10-14
    body: "Is there also a gnome frontend available?\n \n ->No, why?\n\n->>>I guess it's because it requires PyKDE Extensions "
    author: "Patcito"
  - subject: "Re: guidance"
    date: 2005-10-15
    body: "Which is quite normal for a kde application written in python :)\n\nRinse"
    author: "rinse"
  - subject: "kde 3.4.3?"
    date: 2005-10-14
    body: "It feels kind of like a bastardized kde install. And a lot of these applications that I'm apt-getting like quanta are not even showing up in the k menu."
    author: "Nymous"
  - subject: "???"
    date: 2005-10-14
    body: "I installed it yesterday and Quanta is right there at the Development menu."
    author: "Humberto Massa"
  - subject: "Re: ???"
    date: 2005-10-14
    body: "Perhaps I should do a clean install. A development menu never even showed up for me.\n\nI assumed an apt-get dist-upgrade from an RC install would get me up and running."
    author: "Nymous"
  - subject: "Re: ???"
    date: 2005-10-14
    body: "Could be cruft in your $KDEHOME. Spring-clean it :)\n"
    author: "Coolvibe"
  - subject: "Re: kde 3.4.3?"
    date: 2005-10-14
    body: "By bastardized I'm referring to a lot of little things like replacing kcontrol in all the menus. Double clicking menu bars to maximize, choosing open office instead of koffice, and so on.."
    author: "Nymous"
  - subject: "Kubuntu sucks"
    date: 2005-10-14
    body: "Kubuntu ships KDE, right, but it really is a very much broken KDE."
    author: "gerd"
  - subject: "Curious: why?"
    date: 2005-10-14
    body: "I am using kubuntu since the Hoary release. Yesterday I dist-upgraded and everything kept working alright (I am in front of the computer for 4+ hours/day at home). That's why I'm curious. Even the Brasilian Portuguese l10n is reasonable."
    author: "Humberto Massa"
  - subject: "Re: Curious: why?"
    date: 2005-10-14
    body: "I guess some people upgraded too early, and got a corrupted kde desktop"
    author: "rinse"
  - subject: "Re: Kubuntu sucks"
    date: 2005-10-15
    body: "In my present suspicious mood, I wonder whether this isn't actually done on purpose.\n\nThey advertised the first release as a KDE version of Ubuntu, which it was, a bad KDE version (it was crashing like a mad cow, unless one upgraded KDE).\n\nAnd now, upgrade to 3.4.3 on the day of release and ship it untested? But-but-but... This is nonsense! :)\n\nNo, I won't try it again."
    author: "no_more_untu"
  - subject: "Re: Kubuntu sucks"
    date: 2008-03-11
    body: "When i start up kubuntu it goes to ubuntu server 7.1\nthis is a piece of crap"
    author: "Erik"
  - subject: "Re: Kubuntu sucks"
    date: 2008-04-18
    body: "wat do you expect from a debian based distro? Kubuntu is just vanilla kde with a different look. "
    author: "Tux"
  - subject: "Re: Kubuntu sucks"
    date: 2008-10-14
    body: "I tried the latest kubuntu releases, all of them have issues and faults that are not easily fixed or documented well enough to fix efficiently. The problem with desktop linux is the poor and confusing documentation that comes from so many different sources. As much as I hate to admit it, the winner is still Windows, at least you can work around most problems, and get yourself up and running fairly quickly, kubuntu has been nothing but a pain in the ass since I installed it, and after the upgrade I tried last night broke grub, my windows partitions now will not boot at all. Thanks for the effort, but until the linux community settles on a standard that works with all distros, I will use XP as my main OS."
    author: "D Barker"
  - subject: "Re: Kubuntu sucks"
    date: 2008-12-29
    body: "Wow, it really does suck. I could get Ubutu to work as advertised. But the god forsaked KDE version was absolutely unforgiveable. Linux will never succeed in ousting windows if they can't make it so it \"just works\". God I love my Mac."
    author: "QuantuMystic"
  - subject: "Does it has...."
    date: 2005-10-14
    body: "1. Kernel 2.6\n2. GCC 3.4 or 4.x (for fvisibility, kde better performance)\n3. development packages (sources for kde and X, libpng, etc)\n4. hal+dbus+pmount\n\nI'm manually compiling the above stuff on slackware.\n\ncome on patrick, gift us kernel 2.6 !!! :-)\n\n"
    author: "Fast_Rizwaan"
  - subject: "Re: Does it has...."
    date: 2005-10-17
    body: "evan@betty:~$ uname -a\nLinux betty 2.6.12-9-386 #1 Mon Oct 10 13:14:36 BST 2005 i686 GNU/Linux\nevan@betty:~$ gcc --version |head -n1\ngcc (GCC) 4.0.2 20050808 (prerelease) (Ubuntu 4.0.1-4ubuntu9)\nevan@betty:~$ hal\nhald                    hal-find-by-capability  hal-find-by-property    hal-get-property        hal-set-property        halt\nevan@betty:~$ dbus-\ndbus-cleanup-sockets  dbus-daemon           dbus-launch\nevan@betty:~$ pmount\npmount      pmount-hal\n\nAll the above are default settings and packages.  And yes to development, although not by default.  apt-get can automatically set up all build dependencies and grab source for a given package, so it's not really necessary out of the box. "
    author: "Evan \"JabberWokky\" E."
  - subject: "Kubuntu does NOT suck"
    date: 2005-10-14
    body: "Kubuntu exemplifies perfectly how the KDE 3.4 branch should have worked by default. Its set of default settings are a blend of Ubuntu-ish behaviours so that it more closely resembles its cousin, but also combined with certain other expected features of a standard computer desktop (read: a lot of people coming from Win****).\n\n- Double-click window title bar will maximize it. Shade is available via the System Menu on any window, plus Shortcut keys. Do you maximize or shade more often? Most people probably maximize more often.\n\n- Simplified Konqueror Profiles. Of course! Konqueror in Kubuntu appears simpler and less crowded. I would venture to say it is more usable. But it has not lost any advanced features. Remember, the sidebar can viewed by pressing F9.\n\n- System Settings is a great replacement for KControl. At least it makes sense! And nearly everything is accessible from System Settings. Certain things, like Konqueror Settings, have been removed, since they belong with Konqueror. Give it a chance, and let someone who is inexperienced play with both. Remember that KControl is also available and is still a supported app.\n\n- Guidance modules are shown in System Settings and KControl. These modules are excellent, and should be in any *nix installation of KDE. (Obviously they wouldn't be helpful on other platforms.)\n\n- The KMenu is so sweet compared a standard KDE installation. Compare the number of menu entries in standard Debian KDE or Slackware to Kubuntu. In Kubuntu you actually have a usable menu that you can deploy in schools and homes without much training. \n\n- WAF (wife acceptance factor) of Kubuntu is 7. It would be higher if it didn't take so much work to get propietary formats (DVD's, mp3s, quicktime, etc.) working. I can get them working easily, but someone who doesn't know what the heck they're doing will certainly have trouble."
    author: "Philip"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-14
    body: ">WAF (wife acceptance factor) of Kubuntu is 7. It would be higher if it didn't \n>take so much work to get propietary formats (DVD's, mp3s, quicktime, etc.) \n>working. I can get them working easily, but someone who doesn't know what \n>the heck they're doing will certainly have trouble.\n\nI played with the live CD of the previous release and loved it. I didn't\ninstall it because Mandriva has some features that were unavailable in Kubuntu\n\n* Control Center\n* Graphical installer (I don't usually install, just upgrade, but in new machines you need to install)\n* Easy addition of \"non-free\" software using plf sources\n\nThe latest has to do with your WAF point. I searched the other day\nin the kubuntu wiki how to get all these things installed and it is \nreally discouraging at this time. Many command line recipies, one for\neach format/program, etc. I guess all of this will be consolidated\nsoon in a \"one click\" sort of solution.\n\nAnyway, I love what I've seen of adept in the screeies, and I really \nthink pyKDE is the way to go for config tools, they are in the right path. \n\nKubuntu has a very strong community and some financial backing from good Mark.\nFOr the next release, all of my objections will probably be fully addressed,\nand I would have no reason no to switch. Especially now that Mandriva is\ngoing for a 1 year release cycle !\n\nCheers"
    author: "MandrakeUser"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-14
    body: ">* Control Center\n\nKDE has a control center already and with guidance and adept you have it all.\n\n> * Graphical installer \n\nKubuntu already has a graphical installer, it's just not X based but who cares it's still really easy to manage. As an example Gentoo doesn't have a graphical installer (although I know there are projects to solve this that already exist). \n\n> * Easy addition of \"non-free\" software using plf sources\n\nI hope that will never happened, the main reason I'm using it is because I want to get away from proprietary software. What's the point in using a free OS to install non-free software??? I don't get it :) Though I think it's ok to use free tools like mplayer and VLC that are fully free but doesn't respect patents laws of some \"non-free\" countries."
    author: "Patcito"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-16
    body: "Hi Patcito\n\n> KDE has a control center already and with guidance and adept you have it all.\n\nYes, but the Mandrake CC (Suse's too) lets you configure other things, in some sense it is complementary with KDE's. In Mandriva,you hace a \"configure your desktop\" (KDE's CC) and a \"configure your computer\" (Mandriva's CC). It makes a lot of sense. Kubuntu seems to be on the way of integrating everything with the help of guidance, it looks promising!\n\n> Though I think it's ok to use free tools like mplayer and VLC that are fully\n> free but doesn't respect patents laws of some \"non-free\" countries.\n\nThat's more like what I meant :-) Although I do have some non-libre software. Flash to let my son play with Dora, SpongeBob et al, the NVIDIA drivers ... no much more I think. We only use linux at home.\n\nCheers!"
    author: "MandrakeUser"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-20
    body: "Actually, Kubuntu has an official graphic installer and package manager.  It's called Adept.  It is Qt based, possibly KDE (it look KDE, even down to the Help menu options... but the menu does not appear in the OSX style menu I use, which is generally a tip-off that an app is Qt rather than KDE).\n\nIt replaces the working but feature dry kynaptic from earlier versions. \n\nSee a screenshot: http://shots.osdir.com/slideshows/470_or/29.png\n\nThere is also a Quicksliver like application called Katapult, which I have not used much (my fingers are programmed for Alt-F2, or more often either my open konsole or the kicker commandline I have)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-25
    body: "It is a KDE app. The OSX menu doesn't work because it's running as root, through kdesu. I know, evil."
    author: "mornfall"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-14
    body: "> Kubuntu exemplifies perfectly how the KDE 3.4 branch should have worked by \n> default.\n\nDid you test a recent Suse? They have one of the best integrated KDE desktops.\n\n> - Double-click window title bar will maximize it. Shade is available via the \n> System Menu on any window, plus Shortcut keys. Do you maximize or shade more \n> often? Most people probably maximize more often.\n\nYes, most people probably maximize more often. But that's the reason there is a maximize button in the title bar. If you hide the shade functionality nearly no newbie will discover it.\nI think the only button missing is the \"stay-on-top\" button. I always have to manually add it to the window title bar.\n\n> - Simplified Konqueror Profiles. Of course! Konqueror in Kubuntu appears \n> simpler and less crowded.\n\nWould have been better if they sent the patches to the konqueror mailinglist.\n\n> I would venture to say it is more usable. But it has not lost any advanced \n> features. Remember, the sidebar can viewed by pressing F9.\n\nUhh, you say that konqueror's file manager profile does not show the sidebar with the directories? Argh i did not know that Sun's Gnome people work on Kubuntu!\n\n> - System Settings is a great replacement for KControl. At least it makes \n> sense! And nearly everything is accessible from System Settings. Certain \n> things, like Konqueror Settings, have been removed, since they belong with \n> Konqueror. Give it a chance, and let someone who is inexperienced play with \n> both. Remember that KControl is also available and is still a supported app.\n\nIf you configure KControl in a proper way (see Suse) there is no difference to the new system settings.\n\n> - The KMenu is so sweet compared a standard KDE installation. Compare the \n> number of menu entries in standard Debian KDE or Slackware to Kubuntu. In \n> Kubuntu you actually have a usable menu that you can deploy in schools and \n> homes without much training.\n\nCompare the Kubuntu menu to Suse and you will see that SuSE's menu is much more sophisticated.\n\n- WAF (wife acceptance factor) of Kubuntu is 7. It would be higher if it didn't take so much work to get propietary formats (DVD's, mp3s, quicktime, etc.) working.\n\nSuse 10 supports mp3 out of the box. (Suse 9.3 did not. You had to do an online update to install the multimedia-option packs with mp3 support)\n\n"
    author: "Hans"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-14
    body: ">Suse 10 supports mp3 out of the box. (Suse 9.3 did not. You had to do an online >update to install the multimedia-option packs with mp3 support)\n\nOkay, you win! SuSE probably has an as good if not better KDE desktop. But really, I was comparing Kubuntu to the totally free alternatives (Debian/Slackware/etc.)\n\nI'm sure LinSpire is great, but it costs money. I'm sure SuSE is great, but it costs money. Although I would be willing to try OpenSuSe if it didn't use RPMs.\n\nBY THE WAY, there is a one-click script that is going around in the community which will download all multimedia necessities for proprietary formats. Currently it is only in french. http://olwin.free.fr/serendipity/index.php?/archives/1-Easy-Kubuntu-0.4-beta.html\n\nI plan on mirroring the code on my own site in English, and hopefully the KUDOS guide will pick it up as well."
    author: "Philip"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-15
    body: "> I'm sure SuSE is great, but it costs money.\n\n You can get SuSE from here:\nhttp://www.opensuse.org/Download\n\n There are two editions: OSS and Evaluation. The former is free of proprietary software, the last is the one shipped on retail boxes.\n\n> Although I would be willing to try OpenSuSe if it didn't use RPMs.\n\nOpenSuSE is a community, not really a distro. SuSE OSS is the distro.\n\nAnyway, why are so many people against RPMs?\nEven if SuSE used Deb packages, they wouldn't still be compatible with Debian's or Ubuntu's..."
    author: "blacksheep"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-26
    body: "People are against RPMs because of dependancy hell.  I hear great things about SuSE and my only reason I don't use it is because of RPMs."
    author: "John Flux"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-26
    body: "Nowadays with yum and apt-rpm you don't have \"dependency hell\" anymore."
    author: "Paul Eggleton"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-14
    body: ">Simplified Konqueror Profiles. Of course! Konqueror in Kubuntu appears simpler >and less crowded. I would venture to say it is more usable. But it has not >lost any advanced features. Remember, the sidebar can viewed by pressing F9.\n\nhuh??  Dirtree is too advanced now?  \n\nI used to think it was a good idea to try to pander to newbies.  Lately I've realized that it's pointless.  Newbies will always be clueless.  Hardly anyone can use Windows.  I had to make 3 trips to my grandpa's last week to show him how to drag more than one file at a time to a CD to burn it.  (\"press mouse button, drag to encompass the files you want, release mouse button\").\n\nPeople have trouble with the most basic things.  I begin to believe it is self defeating to try to pander to Joe User.  He still won't get it, and you will probably end up ticking off those who do know what they are doing by making their decisions for them.\n\nInstead of removing toolbar entries, let me detail something that might actually help a new user.  Open up Kooka.  Press any of the little 'x's on the top right of each component, or use the handle to drag any of them.  Move them around, turn them on and off, then try to get back to the original configuration.  \n\nMy grandfather got in trouble with that, and I could manage to drag them back to how they were originally.  I had to delete kookarc to get the default layout back (and ended up making it read only so it doesn't happen again.)\n\nIf you want to help new users, disable dragging and turning off components of kooka (and similar apps) by default.  I would be willing to bet that the number of people who accidentally move part of the app around and can't get it back far outweighs the number of people who appreciate being able to arrange Kooka just so.\n\nHave the 'x's and handles disabled by default (like window's \"lock taskbar\").  Have a setting to enable rearranging somewhere for those who simply must, but don't torture new users by allowing them to inadvertently put preview mode into a separate tab.  Disable rearranging the GUI unless specifically asked for, that would help far more than removing toolbar buttons for increasing font size. [/rant]"
    author: "MamiyaOtaru"
  - subject: "Re: Kubuntu does NOT suck"
    date: 2005-10-18
    body: "> - Double-click window title bar will maximize it. Shade is available via the \n> System Menu on any window, plus Shortcut keys. Do you maximize or shade more \n> often? Most people probably maximize more often.\n\nGood point. \nBut a button is missing, else <i>if you hide the shade functionality nearly no newbie will discover it</i>.\n\n> I would venture to say it is more usable. But it has not lost any advanced \n> features. Remember, the sidebar can viewed by pressing F9.\n\nWell, well, the sidebar is now hided in Kubuntu's KDE and it is back in Ubuntu's Gnome. \n\n \n"
    author: "Zero heure"
  - subject: "Katapult!"
    date: 2005-10-14
    body: "I am doin' the massive upgrade as we speak and look forward to KATAPULT to fling my productivity skywards! Yeah!"
    author: "Jonathan Dietrich"
  - subject: "fsck rpms"
    date: 2005-10-30
    body: "first, fsck rpms\n\nwhy do people hate rpms? use a debian based distro for awhile and you'll understand, no really you will, until then, you're just pounding sand with a noodle.\n\nsecond, omgwtflol suse kde better omg no yuo :P\n\nhow long has suse had to work with kde vs. the kubuntu people? christ on a robotic crutch on satan's layaway plan, give me a break, kubuntu will mature with how it works with kde just as suse had plenty of time to mature with kde.\n\nthird, the 'distro blah' is better for 'reason blah' is old and tired\n\nplease stick to the topic of said distro and quit trying to ride on one distro's news article with a whoring out suggestion/link fsck fest for other distros, it's really quite pathetic."
    author: "anonymous cowherder"
  - subject: "won't load"
    date: 2005-11-03
    body: "I installed kubuntu breezy fresh, after i downloaded and burned the iso. It starts off well but it stalls when it tries to start the hotplug subsystem, whats up with that? and how do i fix it?"
    author: "ho;lmes"
  - subject: "A very easily broken distro."
    date: 2008-05-12
    body: "I like the look of kubuntu, but it is easy to break and not so easy to fix..\nfor example, I cannot login as root from the main login screen, so  Iconsole login as root, when I end my root session and try to login as myself, it says ICeauthority error, and I cannot login as KDE no longer works.. What kind of OS are you throwing out there? You guys need better real world examples before you release this, without any kind of warranty.. Come on, if you want to be taken seriously you will fix things like this and release a stable and user friendly OS, windows Xp is still the best desktop OS for the average user. I use it as my main OS and try to use kubuntu  as often as I can, but when I run into crap like this, what the hell is the point?"
    author: "vanislander"
  - subject: "Re: A very easily broken distro."
    date: 2008-05-12
    body: "You aren't supposed to use root; use sudo (kdesudo for gui applications) for all your privileged-command needs."
    author: "Jonathan Thomas"
---
<a href="http://www.kubuntu.org">Kubuntu</a> has <a href="http://kubuntu.org/announcements/breezy-release.php">released Kubuntu 5.10</a>, including the newly released KDE 3.4.3.  Significant additions in this release include <a href="http://web.ekhis.org/adept.html">Adept</a> package manager and <a href="http://www.simonzone.com/software/guidance/">Guidance</a> configuration tools.  The Kubuntu 5.10 Live CD also includes the best KDE on Windows software: <a href="http://www.kexi-project.org/">Kexi</a> and <a href="http://www.pi-sync.net/">KDE PIM</a>.  <a href="http://www.kubuntu.org/download.php">Download now</a> and test out the newest KDE.


<!--break-->
