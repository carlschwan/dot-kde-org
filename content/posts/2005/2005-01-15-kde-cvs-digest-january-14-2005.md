---
title: "KDE CVS-Digest for January 14, 2005"
date:    2005-01-15
authors:
  - "dkite"
slug:    kde-cvs-digest-january-14-2005
comments:
  - subject: "Thanks Derek!"
    date: 2005-01-15
    body: "Hi you god digest-creator! I'm proud to be the first to thanks for this other issue that I haven't read yet, but the chance to be the first at commenting was so good :-=)"
    author: "First one!"
  - subject: "The first ...."
    date: 2005-01-15
    body: "will be the last ....\n\n\nat least :)"
    author: "Matthias"
  - subject: "Thanks!"
    date: 2005-01-15
    body: "Thanks Derek!"
    author: "Pedro Fayolle"
  - subject: "Thanks"
    date: 2005-01-15
    body: "Thanks!"
    author: "Matt"
  - subject: "Strange usability effect with KDE"
    date: 2005-01-15
    body: "Hi,\n\nAfter setting up KDE as the desktop for my parents its still quite interesting to see how things work. KDE is there first computer experience and its getting better and better. Of course many KDE-things are stripped down do make it easy.\n\nBut they have a problem with KDE Save-icon. The problem is simple, they dont know what this symbol is about cause they dont know anything about floppy disks :-)\nMaybe the usability team could think over and come with better symbols for loading and saving.\n\nNow I'm testing 3.4beta. Looks nice :-) \n\nBye\n\n  Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-15
    body: " I've pondered a great deal about this one too.. in just a few short years 3.5\" floppies will be a thing only displayed in IT-museums. I havent used one for more then a year myself, and my mom calls everything a CD, and gets annoyd when i tell her that its a DVD-RW ;-D\n\n So i, for one, think you have a valid point here. The problem is that i cant even come up with a *bad* examble of what to replace it with :-/\n\n Any ideas? It would be great if KDE was the first to be \"innovative\" on this one, and have all the rest copy it!\n\n~Macavity"
    author: "Macavity"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-15
    body: "A goalie stick :)\n\n"
    author: "Jason Keirstead"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "A safe :-)\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "I know you mean it funny (and it is), but the problem with such an icon is that it does not work in all languages."
    author: "Reemi"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "The similarity between the words \"save\" and \"safe\" is not the only reason to use a safe as the save icon.  It gives the impression of a safe place to hold your files.  When you click the button your file will be put in a secure place, on disk instead of in memory, where it will not be affected by crashes or power outages.  This meaning works in all languages.  I like the idea of a safe for the save icon."
    author: "Spy Hunter"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Hey, that's a good idea.  Unfortunately changing it in KDE will meet with LOTS of resistance.  But you could make your own icon set.  Call it \"icons for parents\" :-)"
    author: "Spy Hunter"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "This is a great idea! How about bringing it up on kde-artists mailing list to get it changed?"
    author: "Max Howell"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Change the open icon to being an arcing arrow exiting a folder and the save icon to being an arcing arrow entering a folder. (I don't know what to do for save-as)\n\nI have attached a sample I threw together in a sum total of five minutes. Any end result would be better.\n\nSomeone else recommended a safe, which I would avoid. The reason is that a safe will be wanted when kde has an infrastructure for encrypting a file easily and swiftly, and also saving a file with encryption, something like what MacOS-X has right now. Since that is a good feature to gain, there is no reason to overlap with the obvious icon for it."
    author: "jameth"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Very good concept!!!"
    author: "John Icons Freak"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Great idea! And more unique than a floppy (and that's something OSS needs - unique features).\n\nI thought about something quite similar - arrow pointing to \"disc\", eg circle, for load ( ->O ), arrow pointing away for save ( <-O ), as the 'folder' concept might be to abstract. But still, you're idea should be even better, as a 'disc' matches, CD's, HDD's, FD', but not, for example, USB sticks..."
    author: "Willie Sippel"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "For save-as, from a folder to a folder?"
    author: "James Smith"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "That would imply move, I think."
    author: "jameth"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "Not that I can think of a better idea, but I think there's a big problem with yours: The icons for open and save can't look too similar, because they're (usually) sitting right next to each other on a toolbar. Having two folder-like things with only an arrow differentiating between them would be a bad idea.\n\nBut, like Macavity, I can't think of even a *bad* idea, other than yours. :-)"
    author: "roiem"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "I agree.  Both folders and arrows are already overused in icons.  Arrows often sound like a good idea but simply lead to confusing, ambiguous, and non-memorable icons.  Arrows already mean \"forward\", \"back\", \"up\", \"undo\", \"redo\", and more.  If you also count the arrows on scrollbars and drop-down menus the number really starts growing.  We need something distinctive and memorable for such an important option as \"save\".\n\nJust for fun, I opened Microsoft Word and counted all the arrows in the toolbar icons.  I counted 19, just in the default toolbar set!  Two icons actually have *three* arrows each!  Of course that's only a small sampling of the total number of toolbar icons.  For example, the picture toolbar which is not on by default has a further 10 arrows in it!  That number *also* doesn't include the numerous other arrows used in the interface, such as in the scrollbar and in drop-down menu buttons.  There are 28 (!) of those, for a total of 47 (!!) arrows visible to you whenever you open up a Microsoft Word window!)"
    author: "Spy Hunter"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-22
    body: "Okay, so what about this suggestion:\n\nSave is the document going into a filing cabinet. Being filed away for later use is essentially equivalent to being saved and I think that most people understand filing cabinet images.\n\nOpen can then be taking a file out of a folder, which conveys much the same idea but cannot be visually confused with the other. (I think this is actually unchanged from what it is now, so only save would change)\n\nAnd, as I said before, placing it in a safe could then be used as an icon for saving with encryption. Assuming that a blank sheet, possibly with a flare, I have not looked lately, still is the icon for a new file, all instances are covered without any visual overlap or semantic overlap and all icons should translate to other languages fine (disclaimer: I do not actually use any other languages, I am just guessing)."
    author: "jameth"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Maybe the idea of saving a document is what needs changing.\n\nWhat if it was called (and implemented as) storing and naming? A document or some data produced in an application shouldn't need saving. The application should be able to keep the current state safely. What the user needs to do is to be able to name the document, ie. 'proposal to Gidget Corp. for production of widgets'. It may or may not need storing, ie. copying somewhere other than the default location. It could be stored on some kind of portable media, on a server somewhere. All the while existing in a /home/user/Documents (or whatever) directory by default.\n\nI've been saving documents I produced since around 1986 or so. Saving work in progress against crashes or power outages. Why do I still need to do it? Isn't this a reflection of a low level api? Why not abstract the api with something that makes sense and solves a bunch of usability problems at the same time.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Oh, when KDE was a infant I had discussions this Carsten Pfeifer what this Windows/GEM/etc-loading/saving is about. I came from an Acorn Archimes with the great and innovative RiscOS. There no application has a load icon and saving means dragging a small document icon from your app to your filer - \"save\" and \"save as\" as one intuitive visual action. IMHO fileselector and konqueror filemanager is duplication. ROX (http://www.newplanetsoftware.com/xds/) has this approach in Linux world. Its quite interesing that Desktop Linux in 2005 is in some points still not better than RiscOS from 1987!!\nhttp://www.riscos.org/riscos/intro2.html\n\nBut it is very hard to speak \"revolution\" if you cannot show the code. Thats ok and thats the way community works. Following KDE since 0.4 it has come a good way and in the meantime I prefer an open system more than a closed source RiscOS. But I still miss sometimes _great_ programs like TechWriter.\n\nBye\n\n  Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "> IMHO fileselector and konqueror filemanager is duplication.\n\nI brought exactly the same issue up on kde-usability already at least two times without any response. Strange since on other topics there is so much talk there.\nThe problem is, I don't really know how to \"connect\" the application which intends to load a doucment with a konqy instance in the GUI.\n\nSimply saving all 5 minutes and on exit and removing the \"Save\" action...\nSounds very interesting and is probably the right thing to do. A problem will be that just about any existing application has \"Save\" and most people won't know how to save their files if there is no \"Save\".\nSecondary issue: writing the xml file and zipping it is quite cpu-heavy (what I remember from the last time I created a presentation with kpresenter), so this might interfere with the user interface responsivness.\nIdeally the auto-saving should be combined with a version management, so that everytime a document is opened and changed the old version is still accessible from the version control.\n\nBye\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Not just any version control, but something using change sets.\n\nBasically, at some predetermined frequency a change set is stored, and added to the document one is editing.  The beauty is that once the document is closed and then reopened, those change sets are still there, one can in essence save undo actions.  Furthermore, one can rewind and forward in time and \"branch\" to create new instances of the file.  One could also finalize the file to convert it from a set of changes to one single file.\n\nWith a file system like Rieser4 and its plug-in mechanism, I wonder if this could be, relatively easily, incorporated directly into the file system and made to run very fast.\n\nWhat the user manages and how, and what the operating system manages and how, needs greater consideration at this point in time.\n\nThis along with strong support for rich meta-data would put KDE in a leadership position in terms of innovation, there really would be no competition.  From a business perspective this would be an incredible boon, one would allow for easier document management at the worker level and the other would allow for easier document management at the company wide level, respectively."
    author: "Saem"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "whoa, if some ppl could start coding on this... I fully agree with you!"
    author: "superstoned"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "Then we could also remove the \"Open\" action from applications :-)\nTo open a file, select it in the file manager.\nSaving happens automatically.\n\"Save as\" becomes \"Name the file\".\nEach time a document is opened, a new \"session\" is started and the old version is committed to cvs/svn/darcs/whatever.\nSo there can be a menu entry \"Load earlier version of this document\"\n\nThis would be a quite radical change, but worth trying.\n\nBye\nAlex"
    author: "AleXXX"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "One part is missing: How to name a file?\n\nYou can be a little more radical and say \"In a would, where everyone used to know how to find data in the internet you can use auto-filenaming and do searching by meta-tags\". I do not really believe this database storage. Files and folders still have a future. But try to teach a newbie about files, paths and trees: The newbie does unterstand google way faster than the organization of his home-folder.\n\nThe only newbie confusing thing about google is, why does every link leads to ebay? ;-)\n\nBye\n\n  Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "You're right. I'd like to change all Saving to Storing in my code... it's only a naming change, but lefts room to different way of thinking. And I agree with your point.\n\nIn KPDF for example if you do any change to a file (like set bookmarks or other stuff) those are stored to hd in every 5 minutes and on document close. The user don't even know that something is saving.. he only reopens a pdf and all the changes he did are there. Simple as that.\n\nEnrico\n"
    author: "Enrico Ros"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: " :-) \n "
    author: "-"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "\"What if it was called (and implemented as) storing and naming? A document or some data produced in an application shouldn't need saving. The application should be able to keep the current state safely\".\n\nI understand your point, and I tend to agree. On the other hand, many times I experiment with my documents or whatever and only save them if everything turns out ok, otherwise I simply revert. This wouldn't be possible any more. Yes maybe we can come up with something even better, but it's a problem that must be considered."
    author: "John Usability Freak"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Use versioning as Lyx does?"
    author: "Sergey"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Indeed. What you are doing in that case is a rudimentary version control. Again there must be better ways than having to deal with a low level api.\n\nMost of the foundation is already there with undo/redo.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "Yes some simple version control system was my first thought too, just not sure how exactly it should be implemented. Haven't looked at LyX yet, I use emacs for LaTeX."
    author: "John Usability Freak"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: "As long as we have the system where a file is saved, I think that we need the \"Save\" function.  The best we can do is to add autosave every so many minutes as a background process.  This should not overwrite the original document, but add an extension to the name.\n\nThere exists the idea of continuously updating a working copy of the file on disk by breaking the file into blocks and using a tree structure and a file with index keys.  I think that this was developed in order to be able to edit large documents with small amounts of memory, but it might have other applications as well.  The idea which I have read about allows one deletion, addition, or substitution in a block with a reference to a new block for the addition or substitution.  A second edit would force a restructuring of that block -- adding a new block if it was needed.  But, this could be extended to allow N undo operations.  Using this method, the disk operations are kept to a minimum but a current copy is always on the disk and can be recovered if there is a power failure, or crash.  \n\nWith this system, conversion of the working copy to the stored file format could occur automatically when the application was closed.  Otherwise, there is nothing to save if you only want the working copy.  You might also want to be able to retain the working copy if it contained the information for N undo operations until you had finished work on the document.\n\n-- \nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "Well, you have the right idea.\n\n I have started several times, a re-write of the \"file dialog\" (as well as posted a number of times here). \n\n\n Instead, it should be a \"get from...\" and \"send to...\". Sending the data/file/stream to either a DB, disk drive/folder, web server, person (via e-mail, im, fax, etc), printer, or fax machine, you are doing the same thing. That is you are sending data to either a physical or logical entity. The transport should not be critical, but the \"naming\" should be. the file dialog should be a plug-in approach, which will take either a stream (file, audio, video), a page (picture, printer page, etc.), or a table (row set). \n\nLikewise, there should be a description of how to on-the-fly convert. That is a graphics program saves in png, how do you get to a gif? Simply a convertor.\n\nThe dialog should have plug-in pages that correspond with kios, so that others can change them. That means that google (or yahoo, or msn, or whoever) can change the plug-in that describes the searching on the web. If a user likes if, they can download it and install it.\n\nFinally, the settings should be bookmarkable. The Location menu should be nothing but bookmarks. Of course, I would start it off with a few presets, such as \"Get from ...\", \"Get from local Folder\", \"Get from e-mail\", and  \"Send to ...\", \"Send to printer\", \"Send to person...\", etc."
    author: "a.c."
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "Hmm, does not really look simple :-) Compare yours with the save-dialogue in:\nhttp://www.riscos.org/riscos/intro7.html\n\n(RMB on the icon opens the \"change filetype...\" menu and a simple return means save to the old location.)\n\nI know this type of d&d is a different metaphor for saving/loading but I used it for 8 years and its so much stronger than the fileselector approach. You have to use it a few days!\n\nI really like this thread! But in a few days we get new topics on the dot and all of this discussion is gone. This is the wrong place!\n\nThanks for this interessing discussion!!!\n\n  Thorsten  "
    author: "Thorsten Schnebeck"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "The problem is that the riscos is simply saving back to the disk. It is not hitting a number of different physical/logical paradigms. \n\nAs to the complexity of this, it struck me that one of the bottom buttons should be simple page vs. all pages. When coming up with \"Get From...\" it should show all pages, but when showing \"Get From Scanners\", it should show just the scanner page.\n\n\nBut yeah, this should be moved to the lists."
    author: "a.c."
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "which is the correct list to go to for this? kde-core-devel? Or is there a better place?"
    author: "a.c."
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-17
    body: "Don't think so. As long as there is no developer, who has code, you will have problems when talking about concepts in devel lists. You can try\nhttp://usability.kde.org/\nhttps://mail.kde.org/mailman/listinfo/kde-usability/\n\nBut the better way is to have some code!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Strange usability effect with KDE"
    date: 2005-01-16
    body: ">But they have a problem with KDE Save-icon. The problem is simple, they dont know what this symbol is about cause they dont know anything about floppy disks :-)\n\nToo true. Also the icon doesn't show the status of the document. Instead often a second sign/icon, typically a floppy as well, in the statusbar is used (redundant imho). I for one would prefer a folder, toggling between red and green (that's very universal, except the {,colour-}blind), depending on the status of the document."
    author: "Carlo"
  - subject: "Avalon"
    date: 2005-01-16
    body: "What Framework has KDE to counter Windows Avalon strategy?\n\nIs there a integrated vector, 3d etc. Framework?"
    author: "Gerd"
  - subject: "Re: Avalon"
    date: 2005-01-16
    body: "Don't read too much on Heise-Online ;-)\nAvalon has a different scope. You have to ask trolltech or xorg. KDE works one level higher.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Avalon"
    date: 2005-01-16
    body: "Why do you think it needs countering, especially if countering in your view seems to mean \"do the same thing\"?"
    author: "Andre Somers"
  - subject: "Re: Avalon"
    date: 2005-01-16
    body: "most of these things are already possible in linux. KDE 3.4 will support SVG quite a bit better than previous releases, and I guess it won't get worse in KDE 4.0. Xorg already supports 3d acceleration, with transparency and shadows rendered by the video card.\n\nkde 3.4 will support some of Xorg's features, and that won't get worse, too :D"
    author: "superstoned"
  - subject: "Re: Avalon"
    date: 2005-01-16
    body: "Have a look around freedesktop.org, and in particular at Xorg and Cairo."
    author: "Tom"
  - subject: "Re: Avalon"
    date: 2005-01-17
    body: "Have you seen the recent Avalon demo video that was floating around? \n\nIf you've had much experience with 3D APIs, you'll have seen that it is basically OpenGL-style syntax with XML rather than a language source file.\n\nIt might be marginally easier for a few people to manipulate, but in general terms, I think it offers very few additions to 3D development, and the inclusion of OpenGL into QT many moons ago allows most things to be done.\n\nThe trouble is, 3D development is slow and painstaking. It poses even more usability problems than 2D work, and to be frank, no-one knows how to actually use 3D objects to create a more effective working environment. People still use their computers for 2D tasks (word processing, browsing 2D documents on the web, e-mail, 2D canvas images, DTP), and figuring out if 3D developments can and should fit into that is a question for long term research. \n\nMacOS X has been out for four years now, and so far, it only uses 3D routines to warp windows when it is shrinking them down to the dock.\n\nNow, working out some way to save KNote notes with a document, that followed it around and appeared when the document was opened... That would be a far more productive feature :) Just as an example off the top of my head."
    author: "Luke Chatburn"
  - subject: "Icon placement problems"
    date: 2005-01-16
    body: "Have the icon placement problems been fixed yet?"
    author: "Kde User"
  - subject: "Re: Icon placement problems"
    date: 2005-01-16
    body: "Yes."
    author: "Pedro Fayolle"
  - subject: "No"
    date: 2005-01-16
    body: "No.\nFor examle:\n- Hiding and showing kicker leads to a ~3 Pixel movment of the desktop icons"
    author: "hans"
  - subject: "Re: Icon placement problems"
    date: 2005-01-16
    body: "This icon placement problem is also very annoying in KDE!\nMake this one feels like in Gnome, please! Thanks you a very very much!\n\nThey went up and down like I'm skiing... ;-)\n"
    author: "anonymous"
  - subject: "I Want..."
    date: 2005-01-21
    body: "a kmail with html mode. e.g. when i can be abble to write this: \n<B>Hello kde,</B>\n<P>I'm a <I>kdero</I>. I like <a href=\"http://dot.kde.org/\">dot.kde.org</a>.........</P>\n\nin mail body. And it transform into html when I send it.\n\n\u00bfWHEN?. \u00bfWHERE can I suggest it?.\n\nVery Thank you"
    author: "kdero"
  - subject: "Re: I Want..."
    date: 2005-01-21
    body: "Open a wish for it on bugs.kde.org"
    author: "Carewolf"
  - subject: "Re: I Want..."
    date: 2005-01-21
    body: "thank you"
    author: "kdero"
---
In <a href="http://cvs-digest.org/index.php?issue=jan142005">this week's KDE CVS-Digest</a> (<a href="http://cvs-digest.org/index.php?newissue=jan142005">experimental layout</a>):

<a href="http://www.kdevelop.org/">KDevelop</a> implements KScript interface.
<a href="http://edu.kde.org/kstars/">KStars</a> adds more device support, scripting and observing lists.
<a href="http://digikam.sourceforge.net/">digiKam</a> adds superimpose template.
KDM adds sessreg support.
<a href="http://pim.kde.org/">KDE PIM</a> adds support for custom pages in the incidence editors.
<a href="http://kdepim.kde.org/components/knotes.php">KNotes</a> implements search.
<a href="http://kontact.org/">Kontact</a> adds ability to select default startup part.
<a href="http://www.koffice.org/kexi">Kexi</a> adds database forms with record navigation.


<!--break-->
