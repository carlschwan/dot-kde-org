---
title: "LWN.net: KOffice Heads Toward 1.4"
date:    2005-05-13
authors:
  - "jriddell"
slug:    lwnnet-koffice-heads-toward-14
comments:
  - subject: "another review of a KOffice component"
    date: 2005-05-13
    body: "This review points a few weaknesses of KSpread 1.3.5. Are they solved in KOffice 1.4 ?\n\nhttp://software.newsforge.com/article.pl?sid=05/05/05/1538249&tid=152&tid=93\n"
    author: "foobar"
  - subject: "Re: another review of a KOffice component"
    date: 2005-05-13
    body: "Well, yes and no.\n\nTo make a very long story short, I would like to say that you will see incremental improvements for almost all of the points made in the article, but taken as a stand-alone program KSpread is still behind OOcalc and Gnumeric.\n\nWhere KOffice really shines are in these 3 areas:\n - its comprehensiveness\n - its light weight\n - its integration in the KDE environment. \n\nI haven't made a deep investigation of OO.o 2.0 so I could be wrong here, but my impression is that KOffice is the most comprehensive free office suite in \nexistence.  This is of course measured in number of components, not features. :-)\n\nAnd if I may speculate a bit, I think that the clean codebase of KOffice in comparison to Open Office will allow it to enjoy a quicker development in the future, especially if we can get some more programmers on board. \n\nThe big Java debacle won't help to endear them to new developers either, so I believe that KOffice has a very bright future, especially in the light of KDE:s increased popularity from about 40% to 61% as shown in a recent survey.\n\n"
    author: "Inge Wallin"
  - subject: "Krita"
    date: 2005-05-14
    body: "I was quite impressed by krita, but it took so long to generate a version that looks usable.\n\n\nSmall but usable software projects grow fast, ambitous projects that are always 70% never catch up. Well, lazarus and classpath may be exeptions, they reched the critical usable stage recently."
    author: "Gerd"
---
LWN has <a href="http://lwn.net/Articles/134720/">reviewed the recent KOffice 1.4 beta</a>.  They look at the existing applications in the suite and the new additions of image editor <a href="http://www.koffice.org/krita/">Krita</a> and database manager <a href="http://www.koffice.org/kexi/">Kexi</a>.  The conclusion is that "<em>the KOffice 1.4 release will be a significant move forward for KOffice</em>".  David Faure has also <a href="http://lwn.net/Articles/134845/">commented on the file incompatibility</a> (<a href="http://kdab.net/~dfaure/lwn-dfaure-letter.html">non-subscriber copy</a>) issues mentioned in the article.





<!--break-->
