---
title: "Knoda Meets MS Access"
date:    2005-04-27
authors:
  - "hknorr"
slug:    knoda-meets-ms-access
comments:
  - subject: "I had been waiting for that!"
    date: 2005-04-27
    body: "Very helpful features! \nThis will help me to move some legacy databases from MS Access to Linux.\nThanks!"
    author: "Matt"
  - subject: "BSD?"
    date: 2005-04-27
    body: "Does this compile on Freebsd yet?"
    author: "Ziggy"
  - subject: "Re: BSD?"
    date: 2005-04-27
    body: "On FreeBSD:\ncd /usr/ports/databases/knoda; make install\nDon't forget to upgrade your ports tree to the most current version"
    author: "AlecN"
  - subject: "Re: BSD?"
    date: 2005-04-28
    body: "Ever heard of FreeBSD ports?  And if you're going to go all BSD on us, maybe you can learn how to fix source compilation errors while you're at it?"
    author: "ac"
  - subject: "Re: BSD?"
    date: 2005-04-29
    body: "The last 2 versions i tried out of ports failed misreably. Thus why i asked my question.\n\nAs far as fixing it myself, i have a day job.. no time for that."
    author: "Ziggy"
  - subject: "Kexi?"
    date: 2005-04-27
    body: "Does this app have the same goals as Kexi?\n\nWhich is more complete?"
    author: "KDE User"
  - subject: "Re: Kexi?"
    date: 2005-04-28
    body: "I'm not sure if it has the same high goals as Kexi, but it certainly works more easily and reliably, from what I've seen so far of both.\n\nGive it a try.  Knoda is a deceptively powerful little thing, which gets much less press than it deserves :)"
    author: "Lee"
  - subject: "Sorry but Knoda is second..."
    date: 2005-04-27
    body: "No offence, but commit dates for working features compared:\n\n---Kexi: (cvs log from kdenonbeta/keximdb/)\ndate: 2005/03/06 18:02:24;  author: martin;  state: Exp;  lines: +0 -0\nThis is keximdb, the MDB file migration driver for Kexi.\n\nMDB files are the native database format of MS Access (and\nalso some other MS applications).  This driver can be used\nby Kexi's migration framework to convert simple Access\ndatabases in to native Kexi databases.\n\nUse the keximigratetest program, or use 'Import' - which is\nunder Kexi's file menu if you follow the instructions at\nhttp://www.kexi-project.org/wiki/wikiview/index.php?AdvancedBuildNotes\n - to test.\n[...]------------------\n\n(subsequent commits also mentioned in KDE CVS Digest, http://cvs-digest.org/index.php?newissue=mar182005)\n\n---Knoda: (cvs log from cvs.sourceforge.net:/cvsroot/hk-classes/)\ndate: 2005/03/27 16:33:44;  author: knorr;  state: Exp;\ndriver for .mdb files (MS Access) added\n-------------------\n\nAnd BTW, we're importing data and then -opening, not 'opening native' MDB files. Let's avoid vapourware-like statements. :)\n"
    author: "Jaros\u00b3aw Staniek (Kexi Team)"
  - subject: "Re: Sorry but Knoda is second..."
    date: 2005-04-28
    body: "\"This driver can be used\n by Kexi's migration framework to convert simple Access\n databases in to native Kexi databases\"\n\nDoes this mean that complexe Access databases can't be converted? I wonder how advanced are those mdbtools."
    author: "Pat"
  - subject: "Re: Sorry but Knoda is second..."
    date: 2005-04-28
    body: "Read 4.1 at:\n\nhttp://www.kexi-project.org/wiki/wikiview/index.php?KexiFAQ#4._Project_&_Data_Migration\n\n"
    author: "Jaroslaw Staniek"
  - subject: "Knoda isn't much great"
    date: 2005-04-27
    body: "The main problem I have with it (using 0.7 on Suse 9.2) is that the interface is very confusing.\n\nKexi looks fine, but it's hard to find without koffice package, and when I tryied to compile it agains my KDE 3.4, it just didn't worked."
    author: "Iuri Fiedoruk"
  - subject: "Re: Knoda isn't much great"
    date: 2005-04-27
    body: "1. For users convenience there are also separate releases outside KOffice, with packages following these quidelines:\n\nhttp://www.kexi-project.org/wiki/wikiview/index.php?HintsForMakingKexiPackages\n\n2. E.g. Debian, thanks to great cooperation, is well supported for 10 architectures: http://packages.debian.org/unstable/kde/kexi\n\nRegards"
    author: "Jaros\u00b3aw Staniek (Kexi Team)"
  - subject: "Re: Knoda isn't much great"
    date: 2005-04-27
    body: "why is kexi still labelled 0.1beta? it looks really fullfeatured and last time i checked it was pretty stable."
    author: "Pat"
  - subject: "Re: Knoda isn't much great"
    date: 2005-04-27
    body: "psst, the jump is planned in a few days or so... "
    author: "Jaros\u00b3aw Staniek (Kexi Team)"
  - subject: "Re: Knoda isn't much great"
    date: 2005-04-27
    body: "cool :) thanx for the great work, I've tried kexi when it was first posted on the dot before the big framework rewrite and I also tried it last year. I think i'll give a try this time again. I hope Qt4 will make your port to windows easier too."
    author: "Pat"
  - subject: "Re: Knoda isn't much great"
    date: 2005-04-28
    body: "Yes, I knew there was a separated release, the one that I compiled and didn't worked was this one.\nTo avoit talking nonsense I'vbe even tryied to compile from CVS, much better, but still can't open a mysql database, and postgres support dosen't compile, even that I have the devel package.\nBut working with a file database seems ok and works."
    author: "Iuri Fiedoruk"
  - subject: "Fantastic!"
    date: 2005-04-28
    body: "It has been over a year since last I looked at what was happening on the Linux Desktop database front. I had foolishly though that nothing had changed and that the best I could hope for was one of the GUI frontends to MySQL. Now I see this!\n\nWOW! There are now three desktop database applications, Kexi, Knoda and Open Office Base and the K programs can read MS Access files. FANTASTIC!"
    author: "None"
  - subject: "Re: Fantastic!"
    date: 2005-04-28
    body: "You didn't look very hard a year ago then, or else you should have found Rekall. It has been around since the KDE/Qt2 days. It has seen production use for several years, and it is available as GPL. "
    author: "Morty"
  - subject: "Re: Fantastic!"
    date: 2005-04-28
    body: "It wasn't available under GPL one year ago..."
    author: "Tobias Koenig"
  - subject: "Re: Fantastic!"
    date: 2005-04-28
    body: "It was, Rekall has been GPL since 17 november 2003. http://dot.kde.org/1069090495/"
    author: "Morty"
  - subject: "Re: Fantastic!"
    date: 2005-04-29
    body: "Rekall is nice but, to my knowledge, it is not able to read MS Access files. Did I miss something?"
    author: "None"
  - subject: "Network/Systems Engineer"
    date: 2005-05-04
    body: "I think that this app's existence is great.  I don't use MS Access myself, but given the desktop popularity of Microsoft Access thanks to its bundling with MS Office Professional, I think it's a terrific thing for more than one client to be able to open Access databases, particularly one that's Free Software.\n\nKudos to you, and may your application, and any other application that does something similar, be a great success!  You already are a success at this point anyway for creating Knoda, as is anyone else who has done similar work.\n\nThank you, and well done.\n\n--TP"
    author: "Terrell Prude', Jr."
  - subject: "Great NEws"
    date: 2005-05-07
    body: "It\u00c5\u009b just favulous to know that several databse app\u00c5\u009b are now there to read MS Access"
    author: "Ferdaus Al Amin"
---
<a href="http://www.knoda.org/">Knoda</a> is a database frontend for KDE. Besides tables and queries, Knoda lets you create forms and reports, scriptable via Python. With its latest release, Knoda has introduced a driver for MS Access databases (mdb files). Knoda is the first KDE-based database frontend to be able to read MS Access databases natively and is getting closer to its goal to be a full replacement for MS Access.




<!--break-->
<p>The mdb driver is based on <a href="http://mdbtools.sourceforge.net/">mdbtools</a>.  Another driver has also been added to support read-write access to Dbase (.dbf) files which is based on <a href="http://linux.techass.com/projects/xdb/">Xbase</a>.</p>

<p>Also introduced with this version of Knoda is the possibility to import and export to and from different database servers.</p>

<p>Drivers for <a href="http://www.mysql.com/">MySQL</a>, <a href="http://www.postgresql.org/">PostgreSQL</a>, <a href="http://www.sqlite.org/">SQLite</a> and
<a href="http://www.unixodbc.org/">ODBC</a> are also available.</p>









