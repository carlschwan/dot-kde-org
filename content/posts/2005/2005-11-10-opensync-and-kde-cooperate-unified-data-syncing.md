---
title: "OpenSync and KDE Cooperate on Unified Data Syncing"
date:    2005-11-10
authors:
  - "cschumacher"
slug:    opensync-and-kde-cooperate-unified-data-syncing
comments:
  - subject: "Does anyone knows"
    date: 2005-11-09
    body: "where does the kitchensync name come from?"
    author: "Patcito"
  - subject: "Re: Does anyone knows"
    date: 2005-11-09
    body: "probably a developer joke.\nsoftware that can do allmost everything is often refered by \"this application contains everything but the kitchensync\"\nAnd of course, the name contains the famous 'K' and the phrase 'Sync' :)"
    author: "rinse"
  - subject: "Re: Does anyone knows"
    date: 2005-11-10
    body: "ans what is a kitchensync? (excuse my french)"
    author: "Patcito"
  - subject: "Re: Does anyone knows"
    date: 2005-11-10
    body: "It's a corruption of \"kitchen sink\" as in the English saying \"everything but the kitchen sink\"."
    author: "Paul Eggleton"
  - subject: "Re: Does anyone knows"
    date: 2005-11-10
    body: "k thanx, and for those that are as clueless as me in english and still didn't get what is a kitchen sink, here it is http://images.google.com/images?q=kitchen%20sink   :)"
    author: "Patcito"
  - subject: "Re: Does anyone knows"
    date: 2005-11-10
    body: "Wo! I didn't expect that link to show hot chicks in sinks.\n\nfetishes these days..."
    author: "Vlad Blanton"
  - subject: "Re: Does anyone knows"
    date: 2005-11-10
    body: "It was an old joke/phrase about ppl packing. You packed everything except the kitchen sink. That is, you have far more than you need.\n\nBut in this case, KitchenSync is simply a play on words of Kitchen Sink. It has nothing to do with the phrase. I would guess that it was the only thing that they could think of that contained a K and Sync (apparently, synk was either taken, meant something in a different language, or was just not like)."
    author: "a.c."
  - subject: "Re: Does anyone knows"
    date: 2005-11-12
    body: "The joke does apply to KDE, and I think that the name is intended to play up to this. As in:\n\n\"KDE includes everything, including the KitchenSync\"\n\nI think it was convceived shortly after 3.0, when there were cries that KDE was bloated & before the Kwhatever naming scheme became accepted."
    author: "another ac"
  - subject: "chant"
    date: 2005-11-09
    body: "Yeah i like this !!!! go kde dev's go !!!"
    author: "ch"
  - subject: "KitchenSync kills kitchensync"
    date: 2005-11-09
    body: "So the long-anticipated, but stalled, kitchensync project has finally been killed. And replaced by KitchenSync, the OpenSync frontend?\n\nIs this the correct reading, i.e. has all the underlying kitchensync architecture been demolished?"
    author: "ac"
  - subject: "Re: KitchenSync kills kitchensync"
    date: 2005-11-10
    body: "There is no code from the old KitchenSync or its underlying libraries used in the new solution. OpenSync is based on the same design concepts that our old code was, but it's actively maintained and developed, more stable, and it's desktop-independent. So we decided to not duplicate efforts but go with a unified solution.\n"
    author: "Cornelius Schumacher"
  - subject: "Re: KitchenSync kills kitchensync"
    date: 2005-11-10
    body: "Excellent decision Cornelius, thanks for all the work!"
    author: "MandrakeUser"
  - subject: "Great work"
    date: 2005-11-10
    body: "I read about this earlier on the OpenSync website - well done to all involved.\n\nLately I've been working on an OpenSync plugin for syncing with Opie, and I have to say I'm very impressed with the design - simple and yet still quite flexible. It's also not tied to any particular major library (other than glib) so it can be adopted across the entire open source spectrum. OpenSync has got to be the best way forward for universal syncing."
    author: "Paul Eggleton"
  - subject: "hmm"
    date: 2005-11-10
    body: "\"KPilot is planned to be replaced once all functionality is available with the new framework in an equivalent way.\"\n\nReminds me of the editor that is just kept for BiDi..."
    author: "gerd"
  - subject: "Re: hmm"
    date: 2005-11-10
    body: "Yes, and the old kitchensync, that is now no more, was also supposed to replace KPilot."
    author: "Martin"
  - subject: "speaking of which"
    date: 2005-11-10
    body: "that's cool we're gonna get it in KDE4 and by the way, when does 3.5 comes out? it's been a little while since beta2, getting impatientttt hereeee :)"
    author: "Patcito"
  - subject: "Re: speaking of which"
    date: 2005-11-10
    body: "See http://developer.kde.org/development-versions/kde-3.5-release-plan.html\n\n"
    author: "cm"
  - subject: "Re: speaking of which"
    date: 2005-11-10
    body: "Better yet, get off your butt, run the beta, and report bugs if you find them!\n\nI've been running it since it came out.  GREAT work devs! Way to go!\n\nChipper"
    author: "Chipper"
  - subject: "Re: speaking of which"
    date: 2005-11-10
    body: "Hmm,\n\naccording to the release plan the RC was released yesterday, but afaik it wasn't. Is there any specific problem with the Beta that has to be fixed or is it just a normal delay / nothing special?\n\n\n"
    author: "Christoph"
  - subject: "The 1.0 release.."
    date: 2005-11-10
    body: "Will I be able to sync korganizer with my WinCE phone with the 1.0 release, or do I have to wait for KDE4 until that is implemented? I really prefer Kontact/Korganizer over Evolution, but for now I'm stuck with the latter due to the syncing support.."
    author: "bsander"
  - subject: "Re: The 1.0 release.."
    date: 2005-11-10
    body: "We plan to release a GUI for KDE 3.5 slightly after KDE 3.5 gets released. You definately don't have to wait for KDE4."
    author: "zecke"
  - subject: "Re: The 1.0 release.."
    date: 2005-11-10
    body: "Yes, but I was talking about this:\n\n\"The first official stable 1.0 release of OpenSync is planned for the beginning of 2006. The KDE based frontend will be released in parallel to OpenSync 1.0. This release will contain support for syncing Palm Pilots, SyncML, Evolution, Kontact, files and IrMC capable devices like mobile phones.\"\n\nWhich doesn't mention WinCE (though earlier in the article it was mentioned). So I was wondering if the WinCE plugin is going to be available with that release or with a later release, and if so, when?\n\nThanks :)"
    author: "bsander"
  - subject: "Re: The 1.0 release.."
    date: 2005-11-11
    body: "As far as I know, MultiSync (the codebase on which OpenSync is based) already supports WinCE (via the SynCE library). So when OpenSync comes out, it will most certainly support WinCE. Given that the KDE part will only be a frontend, then it should be able to seamlessly support WinCE as well.\n\nThis is an excellent decision. Supporting all these devices is difficult and duplicated efforts are a waste of already-thin resources. The status of KDE tools for WinCE devices has always been sub-par, and this could finally solve the situation. Great move!"
    author: "Giacomo"
  - subject: "Yeah ;)"
    date: 2005-11-10
    body: "Yeah and Finally we have a solid syncing framework to use ;)"
    author: "zecke"
  - subject: "Syncing with other users"
    date: 2005-11-10
    body: "Friends. Here is my situation. We use linux at home, each person has an account.\nWe need to sync with our PDA's , and that's working great with Kontact.  Only \nthing we are missing is: we would like to share/sync part/all of our appointments\nand contacts. Maybe part of them. Say, just the \"family\" group of contacts. We \nwant to have the same kontacts on this group, and it is multiplication of work for\nall of us to enter each contact in their account.\n\nI know that this is feasible with a groupware solution, but this seems overkill\nfor 3 users, none of which has the time to set up a server, etc. Is there any\nsimpler solution in the horizon ? Maybe with KitchenSync ? Or may be a simple\nsolution for a quick and basic groupware miniserver for a home desktop within\nKDE ? Something nice with a config wizard and pretty interface ?\n\nMany thanks in advance!"
    author: "MandrakeUser"
  - subject: "Re: Syncing with other users"
    date: 2005-11-10
    body: "You can share data by sharing files:\nFor sharing contants export all contacts you want to share as vCard 3.0. Move the exported vCard file to a folder all of you can access. Now add this vCard as a second address book (Settings->Show Extension Bar->Address Books, then click on the Add button, select File, enter the file name).\n\nFor the appointments you can do it similary, i.e. export to iCalendar and add the calendar file via Settings->Sidebar->Show Resource View as \"Calendar in Local File\".\n\nIf you also want to access you data from remote locations then all you need is an account on an IMAP server. There's no need for a fullfledged groupware server.\n\nFor more information: kdepim-users at kde.org."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Syncing with other users"
    date: 2005-11-10
    body: "Thanks a lot Ingo! I'll take a look tonight. \n\nIn principle it looks like this solution would imply having duplicate entries, when the same contact is entered by two users (say me and my wife). Unless\nthe info is somehow merged, but then again, I'll take a look and see  ...\n\nCheers!\n\n"
    author: "MandrakeUser"
  - subject: "Re: Syncing with other users"
    date: 2005-11-10
    body: "Just a guess, but can't you just add a shared contacts resource that's readable and writable for all your users?"
    author: "Andre Somers"
  - subject: "Thanks guys, how about WinCE."
    date: 2005-11-10
    body: "First, thanks for the good work. Syncing data is getting more important every day.\nAllthough WinCE devices are mentioned in the article, they do not appear with the supported devices for the 1.0 release, and in my opinion that is really a missed opportunity.\n\nFor Palm devices there is a lot of excellent software, but not so for WindowsCE/Windows Mobile based devices. Where I live (The Netherlands) those are by far the most used PDA's, and honestly, my Palm can't beat the features of even a modest Ipaq.\nThe only disadavantage is that it is near impossible to synchronize the agenda with Linux. And the agenda is the most elementary feature of those devices...\nA few attempts have been made, but nothing really works.\n\nDecent \"ActiveSync\" support would make OpenSync/Kitchensync the new standard overnight."
    author: "CAPSLOCK2000"
  - subject: "Re: Thanks guys, how about WinCE."
    date: 2005-11-10
    body: "waht is active sync ? some windows \"standart\" :-) ?"
    author: "ch"
  - subject: "Re: Thanks guys, how about WinCE."
    date: 2005-11-10
    body: "Dont worry. Support for windows ce devices is definitely on the roadmap. There already exists a synce plugin. its in the subversion repository if you would like to try it. i just didnt release it with 0.18 since i dont know if it is stable enough already.\n\nBut the author of the synce tools is already working on a replacement called midasync which will make the synchronization of windows ce device a lot better.\n\nArmin"
    author: "Armin Bauer"
  - subject: "always amazed at..."
    date: 2005-11-10
    body: "...the amount of geekiness in KDE. Don't get me wrong, I like it, but my girlfriend, mum, or friends are not so fond of it. What am I talking about here? On OpenSync website, there are a couple of screenies presenting KitchenSync; one of them is 'select plugin' dialogue, and it has an entry that says 'plugin to synchronize files on the local filesystem'. Oh, yeah, gimme some more... This nice definition could really use a substitution, something like 'sync files on your computer' perhaps? And the next one is even worse... Only the palm syncing makes a comprehensible entry. Work on this lads, and make KDE even better :)"
    author: "Petar"
  - subject: "Re: always amazed at..."
    date: 2005-11-11
    body: "A pda is a computer as well though."
    author: "ac"
  - subject: "Re: always amazed at..."
    date: 2005-11-11
    body: "Nope. A PDA for normal people is an electronic agenda on steroids.\nComputer == PC.\n"
    author: "Davide Ferrari"
  - subject: "Very cool!"
    date: 2005-11-11
    body: "This is one of the last things I miss from the windows world. My mobile phone contains my \"life\"...okay, maybe not quite, but I do use the calendar and addressbook quite a bit - loosing that info would be quite annoying. \n\nThank you, thank you, thank you! I look forward to using this :)"
    author: "Joergen Ramskov"
  - subject: "Desktop Sync"
    date: 2005-11-11
    body: "Much more I want sync Kontact adressbook laptop <--> desktop <--> homedesktop."
    author: "Syncar"
  - subject: "default config?"
    date: 2005-11-12
    body: "Hmm, do I overlook something? I see no default config files for kresource or kio_pim?!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "speaking of syncing."
    date: 2005-11-12
    body: "Is there some standard way to sync several kmail/kontact installs? I run kde on three different machines, and I would like my mail folders, addressbook and whatnot all in sync."
    author: "namor"
  - subject: "Re: speaking of syncing."
    date: 2005-11-14
    body: "check out KitchenSync, it already does that between KDE installs"
    author: "Giacomo"
  - subject: "Re: speaking of syncing."
    date: 2005-11-14
    body: "The address book and calendar, to-do list, etc, can all be stored on an IMAP server.  Configure kmail->Misc->Groupware->Enable IMAP resource functionality, then adding a \"Kolab server\" address book resource will give you shared contacts without the hassle of syncing."
    author: "Yrrebnarg"
  - subject: "Syncing bookmarks, notes and passwords"
    date: 2007-10-23
    body: "In addition to calendar and address book I would like to synchronize also konqueror's bookmarks, kwallet's passwords, kopete's contact list and knotes. Will Kitchensync do that or is there a better way?"
    author: "Romans"
---
The <a href="http://www.opensync.org">OpenSync</a> and <a
href="http://www.kde.org">KDE</a> teams have joined forces to create a unified
library to synchronize data from mobile devices with the data on the desktop.
OpenSync, the successor to the MultiSync project, provides a modular
desktop-independent synchronization platform. It can be extended by plugins to support additional devices and data types. Plugins for the most commonly used devices and applications such as <a href="http://www.kontact.org">Kontact</a>,  Palm, Windows CE, mobile phones and more are already available or under development. KDE has now adopted OpenSync as the base for its future synchronization tools.





<!--break-->
<div style="float: right; width: 300px; border: thin solid grey; padding: 1ex; margin: 1ex;">
<a href="http://kubuntu.org/~jr/dot/kitchensync-synchronizing.png"><img width="300" height="214" src="http://kubuntu.org/~jr/dot/kitchensync-synchronizing-wee.png" border="0" /></a>
<a href="http://kubuntu.org/~jr/dot/kitchensync-synchronizing.png">KitchenSync using OpenSync</a>
</div>

<p>With KDE making use of the desktop-independent OpenSync project, a unified
syncing platform becomes a reality. Other desktop applications and projects
already use OpenSync or have signalled interest in using it. With OpenSync as
a unified platform vendors of mobile devices and other parties interested in
integrating with the desktop syncing platforms have a single point of contact.
By writing a simple OpenSync plugin they enable desktop users to sync their
devices with all other devices supported by OpenSync and most applications used
on the desktop, independent of the desktop environment.</p>

<p>At the end of August leading members of the OpenSync and KDE projects met for a
three-day <a href="http://www.opensync.org/wiki/OpenSync-KDE">coding session</a>
at Nuremberg in Germany, sponsored by <a href="http://www.suse.com">SUSE</a>. At the meeting a KDE based graphical user interface for OpenSync was created. A presentation of the results of this meeting is available on the OpenSync website. The code for the KDE frontend, codenamed KitchenSync can be found in the
<a href="http://websvn.kde.org/branches/work/opensync-integration/">"opensync-integration"
branch</a> in KDE's Subversion repository. It has been constantly improved over the last few weeks and already represents a usable application to operate the OpenSync backend. Not only can it be used on the KDE desktop, but also on all other GNU/Linux or UNIX desktops.</p>

<p>The combination of OpenSync and the new KitchenSync frontend will replace
great parts of the current variety of KDE syncing solutions by a unified
approach. The old versions of KitchenSync, KSync, Kandy, libksync have already 
been removed from the code base for the upcoming next generation of the KDE
desktop, KDE 4. KPilot is planned to be replaced once all functionality is
available with the new framework in an equivalent way.</p>

<p>The first official stable 1.0 release of OpenSync is planned for the beginning
of 2006. The KDE based frontend will be released in parallel to OpenSync 1.0.
This release will contain support for syncing Palm Pilots, SyncML, Evolution,
Kontact, files and IrMC capable devices like mobile phones.</p>




