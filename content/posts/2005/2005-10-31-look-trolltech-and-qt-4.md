---
title: "A Look at Trolltech and Qt 4"
date:    2005-10-31
authors:
  - "jriddell"
slug:    look-trolltech-and-qt-4
comments:
  - subject: "\"Public domain code\"?"
    date: 2005-10-31
    body: "In the computerworld article the author makes the following comment:\n\n\"Hence, there is a lot of public domain code that uses Qt.\"\n\nI guess there still is a lot of confusion about the important difference between Free Software and the public domain - even amongst proffessional IT journalists.\n\nL."
    author: "ltmon"
  - subject: "Re: \"Public domain code\"?"
    date: 2005-10-31
    body: "Quite so! It is especially dangerous to forget the distinction in that kind of context. I believe it was trying to say that it makes Qt easier to approach when you don't know anything about it, because there is a lot of code out there that you can access. This is about as true for GPL'ed code as it would be if the code was public domain - but if one thinks that a piece of code is in the public domain, then one would assume that it would be fine to incorporate this code into proprietary software, and ship it without the source. Obviously this could lead to violations of the GPL, so maybe it would be a good idea to email the author of the article and notify them of their error."
    author: "Jack H"
  - subject: "Re: \"Public domain code\"?"
    date: 2005-10-31
    body: "\"Moreover, the choice of the BSD license makes it less likely that Trolltech would ever want to change its licensing, since if it did so another company could take the latest release of KDE under the BSD license and start developing its own proprietary product as a competitor.\"\n\nThey don't seem to have a firm understanding of licenses. Most of KDE is licensed under either LGPL or GPL, so this wouldn't be possible right?"
    author: "Sam Weber"
  - subject: "Re: \"Public domain code\"?"
    date: 2005-10-31
    body: "You need to read the entire related section:\n\nIn 1998, keen to cement its relationship with the KDE community, Trolltech and KDE e.V. created the KDE Free Qt Foundation. This enshrines an agreement between KDE and Trolltech that gives the Foundation the right to release Qt under a BSD-style license if Trolltech doesn't continue the development of the GPL edition of Qt. If Trolltech does continue in its benevolent role, then the KDE community continues to benefit from having a professionally developed toolkit. Should Trolltech ever go bankrupt, or be bought out (something that could become an issue as Trolltech is rumored to be going public soon), and cease to be so free software-friendly, the KDE community would be able to continue Qt development. Moreover, the choice of the BSD license makes it less likely that Trolltech would ever want to change its licensing, since if it did so another company could take the latest release of KDE under the BSD license and start developing its own proprietary product as a competitor.\n\nSo what this is saying is that QT is under a GPL license, but should Trolltech disappear, the existing code base (at that time) will be released under a BSD license, so that development by the community can continue, rather than them going under/being bought/whatever, and taking the code with them."
    author: "hanover"
  - subject: "Re: \"Public domain code\"?"
    date: 2005-10-31
    body: "But its still saying that KDE could be taken under the BSD license (and turned closed-source), which isn't factual."
    author: "Sam Weber"
  - subject: "Re: \"Public domain code\"?"
    date: 2005-10-31
    body: "You are right.  Tom Chance, the author, said himself in the first comment at the bottom of the page that the sentence should read \"another company could take the latest release of Qt under the BSD license\". "
    author: "cm"
  - subject: "Zinc"
    date: 2005-10-31
    body: "I remember being disappointed by Qt when I first used it (many years ago now) because unlike Zinc, it didn't support textmode.\n\nThen KDE came out and I started using X much more often.  kfm was a great filemanager for the era."
    author: "Evan \"JabberWokky\" E."
  - subject: "What a poort article"
    date: 2005-10-31
    body: "From TFA:\n\nQt's GUI toolkit is unique in that it uses the underlying operating system's widget set to provide the interface. As a result, applications have a native look and feel.\n\nUnique just like <a href=\"http://www.wxwidgets.org/\">WxWidgets</a> is then :). I'm glad that, according to this article, Qt4 has moved away from this silly roll-your-own widget thing on Linux and used the standard OS widget set. I suppose it must use Gtk... or maybe Tk?\n\nWhat a poorly written article!"
    author: "Rich W"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: ">I'm glad that, according to this article, Qt4 has moved away from this silly\n>roll-your-own widget thing on Linux and used the standard OS widget set. \n\nFor Linux at lest this is correct, on Linux it uses Qt."
    author: "Morty"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: "On Linux it is a native toolkit; on Windows or OS/X it hooks into their native toolkits.  I believe this was true of Qt v3 as well."
    author: "jaykayess"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: "There are two \"special\" themes in Qt, \"Windows XP\" and \"Aqua\", which are only available on their respective platforms and which use the native widgets. They are also the default themes for these platforms. However, you can still use any of the other available themes or write your own silly roll-your-own theme, even under Windows or Mac OSX.\n\np.s. I think the article confused one of the other cross platform toolkits, which says it uses \"native\" widgets, but then goes and uses GTK under X11. Sigh."
    author: "Brandybuck"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: "WxWidgets is this portable MFC thing, isn't it? Well, I can understand that because of this it is not even considered...\n"
    author: "MM"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: "It uses something called API layering, where they layer their own API on top of the native system API. It's somewhat slower, as calls are routed through an extra layer, and there is simply no getting away from the fact that cross-platform programming with it is a royal pain. Like all toolkits that think they're clever (and people that think they're the answer) by using the native system API, they can only provide the lowest common denominator of functionality that, at times, barely works itself. Subclassing to eternity is not my idea of productive either."
    author: "Segedunum"
  - subject: "Re: What a poort article"
    date: 2005-11-01
    body: "I heard someone describe WxWidgets as \"All the ugliness of MFC macro hell without the tools (visual studio) to help you\" and I'd have to agree.  WxWidgets is a remarkably complete toolkit with all sorts of functionality that extends past the GUI, but it just isn't pleasant to program in.  And I dunno about the rest of you, but life is too short to use toolkits that make programming a chore."
    author: "Leo S"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: "You might be right about WxWidgets, I don't know. But the point here is surely that Qt covers MUCH more than just GUI widgets. Cross-platform development has to include much more than just the GUI."
    author: "Shaheed"
  - subject: "Re: What a poort article"
    date: 2005-11-01
    body: "Amen. I had to port some simple socket code from Solaris to Windows last week. Aaargh! If it were Qt code I was porting, it would have taken all of five minutes to complete, and with none of annoying maintainer-unfriendly #ifdef's."
    author: "Brandybuck"
  - subject: "Re: What a poort article"
    date: 2005-10-31
    body: "\"Qt's GUI toolkit is unique in that it uses the underlying operating system's widget set to provide the interface. As a result, applications have a native look and feel.\"\n\nWell no, that's not true. Qt uses GUI-emulation to get the best of both worlds of having a native look (although it does use the drawing and graphics natives of the system), whilst at the same time actually making the toolkit maintainable cross-platform. It doesn't use native toolkit calls at all. You actually have a realistic chance of moving from one platform to another without the annoying bugs, whilst at the same time getting a native look. SWT is a good example of something that has this large maintainability/bugs/native/cross-platform trade-off, and it's a huge piece of full-time work. Think about that the next time you download Eclipse for nothing. On the other hand, in the past you've had something like Swing which has leaned in the cross-platform direction but without any native look. Trolltech seem to have struck the right balance there.\n\nFrom that perspective, Qt is unique.\n\n\"Qt4 has moved away from this silly roll-your-own widget thing on Linux and used the standard OS widget set.\"\n\nLol. In terms of KDE Qt is the native widget set, and I hardly think Trolltech is going to give people the wonderful performance and benefits of GTK."
    author: "Segedunum"
  - subject: "gtk-qt"
    date: 2005-11-02
    body: "So how does this compare to the gtk-qt engine for people using GTK apps who want them to look like the rest of their Qt/KDE apps?\n\nAnd, on the flip side of this, is there likely to ever be a Qt/KDE theme that mimics GTK's theme?  (not that I'd use it personally, but it might give non-KDE users a little less about which to complain)"
    author: "Dhraakellian"
  - subject: "Re: gtk-qt"
    date: 2005-11-02
    body: "\"So how does this compare to the gtk-qt engine for people using GTK apps who want them to look like the rest of their Qt/KDE apps?\"\n\nWell nothing really. That's just a theme for GTK that just translates to KDE's underlying widget set.\n\n\"And, on the flip side of this, is there likely to ever be a Qt/KDE theme that mimics GTK's theme?\"\n\nNot sure. I suppose if there's enough demand from GTK users to use KDE apps there might be. Currently, the Gnome and GTK people seem absolutely not keen on integration, even if there are better apps to be had:\n\nhttps://bugzilla.novell.com/show_bug.cgi?id=104454\n\nWhich I find strange. Most distributions have KDE, Qt and GTK installed alongside along with perhaps a handful of GTK apps running inside KDE, and no one gets pedantic about it. I certainly don't care as it helps when you need an app with a bit of extra functionality you need, even if I do use KDE apps most of the time. Here they're talking about depending on nothing else than Gnome and GTK, and be damned.\n\nSo, would someone care to tell me where this KDE NIH syndrome thing comes from then, because I'm simply not seeing it?"
    author: "Segedunum"
  - subject: "Re: gtk-qt"
    date: 2005-11-02
    body: "> https://bugzilla.novell.com/show_bug.cgi?id=104454 \n> [...]\n> Here they're talking about depending on nothing else than Gnome and GTK, and be damned. \n\n\"going backwards.\" \n\nWow.  There's childish GNOME zealotry at work in that bug report.  I just hope that's not representative for the whole GNOME community.  Actually, I don't think it is.  Zealots are only the most vocal people. \n\n"
    author: "cm"
  - subject: "QT's Windows XP and Aqua Style"
    date: 2005-10-31
    body: "can't we use those styles from qt in Linux/X11? Is there any way to enable the Aqua and/or Windows XP style for X11?"
    author: "fast_rizwaan"
  - subject: "Re: QT's Windows XP and Aqua Style"
    date: 2005-10-31
    body: "Those 2 themes are special because they use the OS's normal widgets (which makes it so you can't just port the theme, you would have to rewrite them)"
    author: "Corbin"
  - subject: "Re: QT's Windows XP and Aqua Style"
    date: 2005-11-01
    body: "Perhaps the Windows XP style could be ported to use WineLib. :-)"
    author: "James Richard Tyrer"
  - subject: "Re: QT's Windows XP and Aqua Style"
    date: 2005-11-01
    body: "It would probably be far easier for someone just to make it.  And last I heard WineLib was dead."
    author: "Corbin"
  - subject: "QT 3.3.6 release?"
    date: 2005-10-31
    body: "When can we expect QT 3.3.6, I need devanagri bug fixes available with qt 3.3.5 and above. KDE can't be compiled with QT 3.3.5 but with 3.3.6 i hope it will be flawless."
    author: "fast_rizwaan"
  - subject: "Re: Qt 3.3.6 release?"
    date: 2005-11-01
    body: "KDE 3.5 can be built with it. There will be no more KDE 3.4.x releases."
    author: "Thiago Macieira"
  - subject: "qt 3.3.6 snapshot and kde 3.5beta2"
    date: 2005-11-01
    body: "using qt-3.3.6 snapshot, i have compiled arts, kdesupport, kdelibs, kdebase, kdemultimedia, kdenetwork and now it is compiling kdepim :)"
    author: "Fast_Rizwaan"
  - subject: "just ported ~4000 lines of code to qt4"
    date: 2005-11-01
    body: "getting it to compile and run with the q3support lib took me about half an hour.\n\nremoving the support lib dependcy took me about 2 hours.\n\nyet the features im really waiting for:\n - QUnitTest framework (i can download that already as an addon)\n - the new canvas widget\nare promised for qt 4.1...\n\nit seems faster overall (also compile time, can that be true?); but what can i say, im not benchmarking it.\n\n_c."
    author: "cies breijs"
  - subject: "Re: just ported ~4000 lines of code to qt4"
    date: 2005-11-01
    body: "It depends what functionality you used in Qt3.   I had a lot of trouble porting some stuff to Qt4.  For example, making a rounded rectangle path.  With Qt3 I used a QPointArray and it was fairly straightforward, but the port to Qt4 bungled it up, even with the Qt3 support libs.  It's not so good that some of the Qt3support classes don't work the same as their Qt3 counterparts.  I mean, isn't that the point?\n\nOther than that, Qt4 is really awesome.  I'm using it for all my new projects now."
    author: "Leo S"
---
Two recent articles cover the success of Trolltech and their product Qt 4, on which KDE 4 will be based.  <a href="http://business.newsforge.com/article.pl?sid=05/10/03/155235">Trolltech: A case study in open source business</a> looks at the continued growth of the company based on dual licenced Free Software.  The article describes what KDE and Trolltech gain from each other, including user feedback to Trolltech and sponsored developers for KDE.  The <a href="http://www.computerworld.com.au/">Australian Computerworld</a> declares that <a href="http://www.computerworld.com.au/index.php/id;502640359;fp;16;fpid;0">Qt 4 raises the bar for cross-platform app dev tools</a>.  They cover the separate modules of Qt 4 and the cross-platform quality, giving it a 9.2 out of 10 approval rating.


<!--break-->
