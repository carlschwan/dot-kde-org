---
title: "KDE Commit Digest for May 20, 2005"
date:    2005-05-22
authors:
  - "dkite"
slug:    kde-commit-digest-may-20-2005
comments:
  - subject: "Old Digest Layout"
    date: 2005-05-21
    body: "Is there any way to get to the old digest layout any more? I liked it more than the current one."
    author: "mmebane"
  - subject: "Re: Old Digest Layout"
    date: 2005-05-23
    body: "Whats wrong with this one:\nhttp://commit-digest.org/?issue=may202005&all"
    author: "ac"
  - subject: "Re: Old Digest Layout"
    date: 2005-05-23
    body: "I liked the color scheme better, I liked not having a sidebar, but I _really_ liked the table cross-referencing app types with commit types. I could see at a glance, say, what KDE sections got optimizations, and jump straight to the given optimization section."
    author: "mmebane"
  - subject: "Re: Old Digest Layout"
    date: 2005-05-24
    body: "I miss it too.  Far easier to find the optimization sections, which I've always been most interested in."
    author: "arcade"
  - subject: "rss?"
    date: 2005-05-22
    body: "An rss feed for commit-digest.org would be nice! ;)\n\nKeep up the good work :D\n\nRafael Rodr\u00edguez"
    author: "Rafael Rodr\u00edguez"
  - subject: "Re: rss?"
    date: 2005-05-22
    body: "On my list of TODO's.\n\nDerek"
    author: "Derek Kite"
  - subject: "Bug in the digest?"
    date: 2005-05-22
    body: "I refuse to believe that there was only a single commit for KOffice that was interesting enough to include during the whole week. There must be a bug somewhere.\n\n"
    author: "Inge Wallin"
  - subject: "Re: Bug in the digest?"
    date: 2005-05-22
    body: "Looks like the other commits are hiding at\n\nhttp://commit-digest.org/?issue=may202005&category=Koffice\n\n(note the lower-case 'o' in Koffice)"
    author: "Jeffrey M. Smith"
  - subject: "Re: Bug in the digest?"
    date: 2005-05-23
    body: "But there is no way to get to that from the sidebar."
    author: "Inge Wallin"
  - subject: "Re: Bug in the digest?"
    date: 2005-05-23
    body: "It should work now.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Bug in the digest?"
    date: 2005-05-24
    body: "Thanks!\n\nAnother little bug:\nYou seem to replace newlines in the commit logs with nothing.  PErhaps it would be better to replace them with a space instead."
    author: "Inge Wallin"
  - subject: "Who's Eric?"
    date: 2005-05-22
    body: "A lot of times when I see commits for kdom and ksvg they have something like \"patch by eric\" or \"eric likes it better this way\" etc. comments.  Well, who's this mysterious Eric character who doesn't have a svn account? :)\n\nAnyhow, Thanks Eric! (as well as all you other kdom programmers) :)  Nice to see khtml2 work actually starting."
    author: "Ac"
  - subject: "Re: Who's Eric?"
    date: 2005-05-22
    body: "Eric Seidel, employed by Apple on daytime, contributing to several projects: http://eseidel.com/hacking.html"
    author: "Anonymous"
  - subject: "Re: Who's Eric?"
    date: 2005-05-22
    body: "Hi,\n\nYes, Eric helps us a lot with ksvg2. In fact\nhe helped us some times in the past too with\nksvg1 even though that may not have been\nthat visible.\nRight now he gives great advise, almost\nall of his design suggestions are excellent\nand make particularly KCanvas much nicer\nIMHO.\nIndeed there are right now quite some kdom/ksvg2/kcanvas\nhackers (could always be more though :)), and I want to\nthank all of them!\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Who's Eric?"
    date: 2005-05-22
    body: "I have no idea what kdom/ksvg2/kcanvas is. If I Google for it I only see technical information. Could you please explain what a \"ordinary user\" can do with kdom/ksvg2/kcanvas? Thanks!"
    author: "LB"
  - subject: "Re: Who's Eric?"
    date: 2005-05-22
    body: "The reason why you only see technical info, is because they are all \"lower level\" (from a user perspective) components. This is my understanding of them, given what little I know.\n\n * kdom handles the dom (document object model) which provides the internal (code) representation of web pages (if not other things as well), improvements to it will improve konq\n\n *  ksvg2 is for drawing SVG (scalable vector graphics) to the screen. SVGs are line art, infinite resolution images. This will be used by konq and other applications in the future.\n\n * kcanvas is for drawing to the screen. Improvements to this will make things easier for developers to do pretty things, not to mention make them run faster.\n\n"
    author: "Jonathan Dietrich"
  - subject: "Re: Who's Eric?"
    date: 2005-05-23
    body: "* kcanvas is for drawing to the screen. Improvements to this will make things easier for developers to do pretty things, not to mention make them run faster.\n\nJust to expand - ksvg2 will use kcanvas.  Also any hypothetical mathml implimentation probably would too."
    author: "JohnFlux"
  - subject: "Re: Who's Eric?"
    date: 2005-05-24
    body: "Hi John,\n\nActually I am not sure mathml and KCanvas would fit. I wouldn't\nbe surprised if qt4 already offers all you need for mathml regarding\nfonts and text support. In fact IIRC there already was done a mathml\nsolution in qt sometime ago.\nAnyway, this is not the place to discuss it I guess, so mention it in #svg if\nyou want to talk about mathml :)\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Who's Eric?"
    date: 2005-05-24
    body: "Trolltech provides a commercial widget for mathml 2.0\nhttp://www.trolltech.com/products/solutions/catalog/Widgets/qtmmlwidget/\n\nKFormula is capable to load and display mathml embedded into oasis documents, but I don't know how much of the specification is supported. With tex fonts it does look quite nice.\n"
    author: "Sven Langkamp"
  - subject: "This, of course, is the wrong place for this .. "
    date: 2005-05-24
    body: "... but I'll post it here anyways.\n\nGeneral \"gripe/feature request/whatever\" for KDE. \n\nWhile the 'desktop environment' and 'kde experience' is getting better and better, there are a few areas where I really would like some optimization.\n\nKonqueror and KHTML .. page rendering is often horribly slow compared to other browser.  In addition, \"all of kde\" or at least the application I launched konq from seems to lag quite a lot when I've clicked a link.  I'm on a pretty modern laptop with plenty of processing power - but this still annoys me.  When it's finished the results are excellent, but the system is sluggish while it's working.  I far prefer to just use firefox.  (I use SuSE 9.3 and KDE 3.4)\n\nKDE PIM is getting really nice, but is far too slow when switching between the various main tasks (email, addressbook, calendar, and so forth).\n\nSome major optimizations in those two areas would _greatly_ benefit the usability of KDE.  That, and the IMAP kioslave needs some more work.  It's gotten far better than in 3.3, but still :-)\n\nI really, really look forward to the day when I can switch over from firefox to konqueror and say 'konqueror is better'.  I also really, really look forward to when I can use kmail and say 'kmail is better than mutt'.\n\n'better' means faster, while doing the right thing.\n\nWhen it comes to usenet, I look forward to the day knode is as fast as pan, while both is slower and more useless than slrn.  pan is quite nice, but crashes too frequently (and ... it's a gnome app).\n\n"
    author: "arcade"
  - subject: "Just to add some comments."
    date: 2005-05-24
    body: "Not that it's good to reply to oneself, but to highlight a graphical few apps that does the really deserves the name \"killer app\" :\n\n - firefox.  Fast, efficient, with the user-friendly plugins such as ad-block.\n - ethereal.  Excellent packet sniffer.\n - pan.  One of the best graphical newsreaders I've ever used -- albeit, it crashes too frequently\n - irssi.  Text based.  The best IRC client I've ever seen.\n - gnumeric.  Excellt spreadsheet tool.  Wish kspread, which has a much better user interface, would become as feature rich and so forth as that one.\n - mplayer.  the best video player ever.  To have that as a backend to a KDE program would be SO cool.\n\nI'm not bashing KDE though.  KDE has one of the best window managers I've ever used.  kStars is one of the coolest programs I've seen in a long time.  kOrganizer is getting Really Good (but needs optimizations and quite a few bugfixes).  konqueror has lots upon lots upon lots of really cool stuff, but far too much of it feels 'raw' and not ready for production as you easily find bugs in it or its plugins.\n\nKeep up the good work!  But please, more killer apps!  And!  Fix the eyesores (Things that Is Really Cool, but are buggy if you play around with it for two minutes).\n\n"
    author: "arcade"
  - subject: "Re: Just to add some comments."
    date: 2005-05-25
    body: "It's not KDE that is so slow, it's suse 9.3\nIf you don't belive that make the comparison a try out e.g. Kubuntu and you will see that it's *much* faster.\n\nKMPlayer is a kde frontend for mplayer."
    author: "ac"
  - subject: "Re: Just to add some comments."
    date: 2005-05-25
    body: "and if you want it faster than Kubuntu, try Slackware... ;-)\n"
    author: "anonymous"
  - subject: "Re: Just to add some comments."
    date: 2005-05-25
    body: "doesn't really make a difference, try it. kubuntu is almost as fast as you can get it (without starting to optimize yourself, like you can with gentoo).\n\nand @ the first poster, I think you really should try kubuntu. at least for me firefox is way slower than konqueror..."
    author: "superstoned"
  - subject: "DVD-Players-Online-Store"
    date: 2005-11-29
    body: "In our shop you can buy anyone DVD products: DVD Changers, DVD Players, DVD Recorders, DVD-VCR and Other DVD Combos, Portable DVD Players."
    author: "Masha"
  - subject: "Sport DVD sets"
    date: 2005-10-31
    body: "Online sport-related DVD store. DVD about Auto Sports, Baseball, Basketball, Hunting and more...."
    author: "James"
  - subject: "Accessories for Cameras"
    date: 2005-11-17
    body: "Accessories for Cameras - site with a wide range of accessories for your optics."
    author: "Kolin"
---
Following the move to Subversion, KDE CVS Digest, our weekly review of the code changes to KDE, was renamed to KDE Commit Digest;  this week we also have a new domain at <a href="http://commit-digest.org/">commit-digest.org</a>.  In <a href="http://commit-digest.org/?issue=may202005">this week's digest</a> (<a href="http://commit-digest.org/?issue=may202005&all">all in one page</a>):  <a href="http://knode.sourceforge.net/">KNode</a> rewrites article viewer, KViewShell adds basic printing support, <a href="http://developer.kde.org/~wheeler/juk.html">Juk</a> adds a cover manager and KDE4 porting continues apace.  



<!--break-->
