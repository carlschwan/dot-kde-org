---
title: "aKademy 2005: Ready For Your Registration"
date:    2005-05-05
authors:
  - "Fabrice"
slug:    akademy-2005-ready-your-registration
comments:
  - subject: "Hostel"
    date: 2005-05-06
    body: "Too bad we can not book the hostel for more than the conference. I've ordered my planeticket for the 22. of august and booked a bed at Picasso's Corner Backpackers Hostel for the first few days."
    author: "Carewolf"
  - subject: "Re: Hostel"
    date: 2005-05-06
    body: "You should've taken this as a sign that you should explore more of Andaluc\u00eda. ;)"
    author: "Ian"
  - subject: "Re: Hostel"
    date: 2005-05-08
    body: "arsa!"
    author: "c0p0n"
---
Registration to KDE's aKademy conference is now open. As <a href="http://dot.kde.org/1111445162/">previously announced</a> <a href="http://conference2005.kde.org/">aKademy 2005</a> is to take place at the <a href="http://www.uma.es">University of M&#225;laga</a> from Saturday 27th August to Sunday 4th September, with a KDE e.V. members-only meeting on Friday 26th. Everyone is invited to join the conference in M&#225;laga. Go ahead and <a href="http://conference2005.kde.org/registration.php">register that you are coming</a>.



<!--break-->
<p><img src="http://conference2005.kde.org/pictures/logo.png"  width="350" height="205" align="right" border="0" /></p>

<p>Registration is currently only open to free software contributors and private participants. Registration for corporate delegates will be opened on the 12th of June. So KDE contributors are encouraged to register as soon as possible.</p>

<p>See you in M&#225;laga!</p>









