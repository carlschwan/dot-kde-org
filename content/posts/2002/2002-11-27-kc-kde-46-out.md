---
title: "KC KDE #46 is Out"
date:    2002-11-27
authors:
  - "zrusin"
slug:    kc-kde-46-out
comments:
  - subject: "javascript debugger"
    date: 2002-11-26
    body: "That javascript debugger in Konqueror is a _great_ idea and that finance application in KDE CVS would be _really_ cool, but as already said by developer the new proposed tabs look needs to be improved.\n\np.s. KC KDE almost at 50!\n"
    author: "anonymous"
  - subject: "khacc not in koffice."
    date: 2002-11-26
    body: "I have been thinking that perhaps there should be a khome:\nBookcase, Album, Khacc, etc.etc.\nPerhaps tie things together so that when a book/music/video/etc is added to khacc, then it also asks if users wants it in bookcase.\nHow about an insurance type program that uses album for storing pix into?"
    author: "a.c."
  - subject: "no mention of 3.1 or Kroupware?"
    date: 2002-11-27
    body: "According to the \"release schedule\" document, the decision on whether to release 3.1 was supposed to happen yesterday.  Just wondering about that one.\n\nBut the bigger issue to me is.... what's the status of the Kroupware project?   I thought it was supposed to release at the end of next month.  With a really big, and paying customer... I'm just interested in knowing how this team is doing."
    author: "Jeff"
  - subject: "Re: no mention of 3.1 or Kroupware?"
    date: 2002-11-27
    body: "Hope they've decided on a better name for it too. My money's on Konvergence."
    author: "Someone"
  - subject: "Javascript"
    date: 2002-11-27
    body: "Anybody know of a way to either launch a Konqueror window with no menubars whatsoever and accessing an html on the harddrive?  Or alternatly, to execute javascript *without* a konqueror instance already running?\n\n--\nEvan"
    author: "EJW"
  - subject: "Re: Javascript"
    date: 2002-11-27
    body: "You can use the javascript interpreter standalone using kjsembed. The next version of this will include a command line tool for running scripts (#!/opt/kde3/bin/kjs) which will be pretty cool.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "qtella?"
    date: 2002-11-27
    body: "Qtella (qtella.sf.net) already has very nice tabs with close buttons on them.  That might work as a starting point... (Qtella has a very nice interface, this screenshot from their site does *not* do it justice).\n\n--\nEvan"
    author: "EJW"
  - subject: "Re: qtella?"
    date: 2002-11-27
    body: "Let me guess, you didn't look at the bug comments on http://bugs.kde.org/show_bug.cgi?id=46726 did you? Please read my comment on the bottom of that page. \nThis also applies to all future comments about tabs in Konqueror :)\n\nZack"
    author: "Zack Rusin"
  - subject: "Re: qtella?"
    date: 2002-11-27
    body: "Read the note on the bug report regarding the single glyph situation. I don't know what the QTab stuff is capable of, or what replacement plans are in place, but...\n\nYou might want to check out the close button functionality of the truly excellent \"multizilla\" (http://multizilla.mozdev.org/) chrome for mozilla. It uses the favicon glyph on the tab, but when you hover over the tab it changes to a \"x\" close button... extremely convenient, and looks great (you don't have to be looking at a dozen redundant \"x\" buttons on every tab, yet the functionality is on each tab which is more convenient than a close button off to the side).\n\n"
    author: "Ragica"
  - subject: "Re: qtella?"
    date: 2002-11-27
    body: "Shift-Left and Shift-Right should switch between tabs please.  Just like in Konsole.  Shift-Up and Shift-Down in konqi is magic."
    author: "ac"
  - subject: "Re: qtella?"
    date: 2002-11-27
    body: "> Shift-Left and Shift-Right should switch between tabs please.\n\nYou can configure it with \"Configure Shortcuts...\" - but you will lose ability to mark text within html form line edits and text areas."
    author: "Anonymous"
  - subject: "Too bad I can't make it build"
    date: 2002-11-27
    body: "I'm using Redhat 8 *duck* and I'm pretty desperate for a good gnutella client."
    author: "Rimmer"
  - subject: "Re: Too bad I can't make it build"
    date: 2002-11-27
    body: "Why not use SuSE?"
    author: "ac"
  - subject: "I haven't heard of a Suse gnutella client"
    date: 2002-11-27
    body: "Seriously... what is your point?"
    author: "Rimmer"
  - subject: "Re: I haven't heard of a Suse gnutella client"
    date: 2002-11-27
    body: "\"I am an anti-GNOME and anti-RedHat troll and I will always be anti-GNOME and anti-RedHat no matter how much GNOME and KDE love each othe.\""
    author: "bc"
  - subject: "Re: I haven't heard of a Suse gnutella client"
    date: 2002-11-27
    body: "Hey, If RedHat has problems compiling and installing apps, I'd say that would be a reason to ::promote favorite distro here::.\n\nI dunno - I only use it for servers at work, and I have the UberMax RedHat Network solution that sends me updated packages.  The last time I used it as a workstation was around 5.1.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: I haven't heard of a Suse gnutella client"
    date: 2002-11-28
    body: "He isn't having problems compiling and installings apps, he is looking for a good Gnutella client!"
    author: "Stof"
  - subject: "Re: I haven't heard of a Suse gnutella client"
    date: 2002-11-29
    body: "Sorry - I think I got confused with another thread wherein somebody was having trouble compiling Qtella and attributed it to Redhat.  Never mind - I've never used RH 8 and have no opinion one way or the other on it... I was just suggesting a distro very similar that compiles things correctly out of the box.  This guy didn't have any problems compiling... I just had problems keeping my discussion threads straight.  :)\n\n--\nEvan\n\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Too bad I can't make it build"
    date: 2002-11-27
    body: "Why not use Debian or Gentoo?\n\nSuSE and Redhat (and Mandrake) are all useless crap."
    author: "cc"
  - subject: "Re: Too bad I can't make it build"
    date: 2002-11-27
    body: "Yes ! Use Debian ! KDE 2.2 rulez !\n\nDisclaimer: that's a joke :) Don't shoot me please."
    author: "Julien Olivier"
  - subject: "Why Redhat?"
    date: 2002-11-27
    body: "I can't use Gentoo (I only have a dial-up connection)\n\nDebian has old software (KDE 2 for heaven sake)\n\nLibranet/Xandros ect is expensive (otherwise I would try it)\n\nBesides Redhat rocks (except for KDE stuff)."
    author: "Rimmer"
  - subject: "Re: Why Redhat?"
    date: 2002-11-27
    body: "What about Mandrake ?"
    author: "Julien Olivier"
  - subject: "Re: Why Redhat?"
    date: 2002-11-28
    body: "> Debian has old software (KDE 2 for heaven sake)\n\nNot true.\nKDE3.0.5 for stable and unstable\napt-get able from download.kde.org or one of its mirrors.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: Why Redhat?"
    date: 2002-11-28
    body: ">Not true.\n>KDE3.0.5 for stable and unstable\n>apt-get able from download.kde.org or one of its mirrors.\n\nUnofficial packages. there are no official KDE3 packages because \"Debian is about to migrate to GCC3.2\". And before that it was because \"Debian is about to migrate to Xfree 4.2\" (Well, Debian got Xfree 4.2 only 9 months late)\n\nLuckily I'm about to move to Gentoo. At least they have cutting-edge software (something that used to be true for Debian as well)."
    author: "Janne"
  - subject: "Re: Why Redhat?"
    date: 2002-11-28
    body: "Debian in instable will migrate to GCC3.2, stable won't.\n\nOf course they are unoffical packages, but they are apt-get available on KDE's official site, done by their usual packagers.\n\nOfficial enough for me :)\n\nCheers,\nKevin\n\n"
    author: "Kevin Krammer"
  - subject: "Re: Why Redhat?"
    date: 2002-11-28
    body: "\"Debian in instable will migrate to GCC3.2, stable won't.\"\n\nYep. I would guess Stable gets GCC 3.2 sometime in 2009."
    author: "Janne"
  - subject: "Re: Why Redhat?"
    date: 2002-12-02
    body: "I used gentoo on a dialup :) I used the stage3 to get things going, and then started downloading. Get some rest, it will take a while. The only down side is I had some trouble with file corruption."
    author: "Echo6"
  - subject: "Re: Too bad I can't make it build"
    date: 2005-03-19
    body: "hmm... KDE is pretty much a standard now. It's the standard in Xandros, which easily runs everything you were talking about now... :D"
    author: "wow is this all old and outdated."
  - subject: "Re: qtella?"
    date: 2002-12-13
    body: "Hello:\n  so far could not install or compile any version of Qtella, \non RedHat 8.0.\nDoes it exist a version of Qtella which will install on RH 8.) ?\nThanks\nAndre G-"
    author: "Andre"
  - subject: "tab features"
    date: 2002-11-27
    body: "How about adding the mozilla feature: middle click - open link in new tab.  It is a pain to have to right click, or reach over to the keyboard to open a new tab."
    author: "theorz"
  - subject: "Re: tab features"
    date: 2002-11-27
    body: "its already there, try enabling 'open new windows in tabs instead of new window' in browsing behavior, and that'll do the trick"
    author: "daaKu"
  - subject: "Re: tab features"
    date: 2002-11-27
    body: "And be sure to specify you don't want focus to go to the new tab by default."
    author: "ac"
  - subject: "Re: tab features"
    date: 2002-11-27
    body: "In contrast to you I always have my left hand near the keyboard and love to selectively open new URLs in tabs when holding Ctrl and quickly closing them with Ctrl-w. :-)"
    author: "Anonymous"
  - subject: "Konqueror tabs are fine"
    date: 2002-11-27
    body: "... just do something about the memory usage!\n\nIt starts up using too much memory, and then it leaks like sieve!"
    author: "cbcbcb"
  - subject: "Re: Konqueror tabs are fine"
    date: 2002-11-27
    body: "i'd like to see a possibility for dragging things onto the tabs, like you can drag things onto the klauncher window buttons, so that the tab under the cursor becomes active.\n\nalso dropping thing on tabs would be cool:\ndrop a url on a tab: open it\ndrop a file on a tab: copy/move/link the file to the dir (if it's a dir)"
    author: "jos"
  - subject: "KOffice components"
    date: 2002-11-27
    body: "I read the text about the kexi and khacc.\n\nkexi is already to be included in KOffice package and khacc not.\n\nI don\u00b4t agree with this decision.\n\nKexi is a very good toll, but is for developers, and have to be included but:\n\n\"Don\u00b4t forget the final user\"\n\nLet\u00b4s give an option to the KDE users against Money/Quicken/GNUCash.\n\nI think all efforts to have a full featured office package is important, including financial programs."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: KOffice components"
    date: 2002-11-27
    body: "If I have correctly understood, koffice only features apps that help you create a document. Kexi will be included because it can help you merge database information into a document like a letter or - I guess - a spreadsheet.\n\nKhacc, although it might be a great app, isn't aimed at document creation. So, like David sayed, it would be great to have it in KDE CVS but not in koffice CVS."
    author: "Julien Olivier"
  - subject: "Re: KOffice components"
    date: 2002-11-27
    body: "I understand now your point. I are right.\n\nIt will be better to include it in KDE CVS.\n\nThanks"
    author: "Paulo Junqueira da Costa"
  - subject: "Re: KOffice components"
    date: 2002-11-27
    body: "Which is why I suggested up higher createing a \"khome\". Apps that work together to accomplish something for the home user."
    author: "a.c."
  - subject: "Re: KOffice components"
    date: 2002-11-27
    body: "More betther than I\u00b4m thinking. A KHome package will be more interesting..."
    author: "Paulo Junqueira da Costa"
  - subject: "Re: KOffice components"
    date: 2002-12-05
    body: "or kdeextragear?\n\nAlex"
    author: "alex"
  - subject: "Thanks for KC"
    date: 2002-11-27
    body: "Just wanted to say big thanks for bring us KC KDE again. Without it, I have little idea of what's going on since I don;t really have the time to read everything. the summary that is KC is hugely appreciated.\n\nThanks for all your hard work\n\nAndy Cheung\nwww.andycheung.com "
    author: "Andy Cheung"
  - subject: "LinuxToday"
    date: 2002-11-28
    body: "Hey Zack R., did you ever find out from Zack B. what's going on with KC KDE and LinuxToday?  None of the last 3 issues or so seem to have been posted."
    author: "Navindra Umanee"
  - subject: "Re: LinuxToday"
    date: 2002-11-30
    body: "I submitted the last two KC KDE's to linuxtoday.com myself and they ignored me. I don't why are they doing this, or why aren't they posting it when submitted by someone else. I'll probably email them sometime this week to ask about it.\n\nZack"
    author: "Zack Rusin"
  - subject: "Re: LinuxToday"
    date: 2002-11-30
    body: "Any idea why Zack B. is not submitting it anymore?  Maybe it would be good to check with him too."
    author: "Navindra Umanee"
  - subject: "Doping for gold"
    date: 2002-12-05
    body: "It's not smoked reindeer meat, it's surstr\u00f6mming."
    author: "reihal"
  - subject: "what version of konqueror has tabs?"
    date: 2003-02-27
    body: "Which version of konqueror has tabs?"
    author: "zim"
---
After a rather long break <a href="http://kt.zork.net/kde/archives.html">Kernel Cousin KDE</a> is back, and <a href="http://kt.zork.net/kde/kde20021126_46.html">KC KDE #46</a> is out. This edition discusses everything from KOffice news, a new <a href="http://kopete.kde.org/">Kopete</a> plugin, a <A href="http://www.xs4all.nl/~jjvrieze/snapshot2.png">JavaScript debugger</a>, improving <a href="http://131.215.82.160/screenshot1.jpg">tabs</a> in Konqueror, <a href="http://i18n.kde.org/">i18n kudos</a> for the Swedish team, and much more.
<!--break-->
