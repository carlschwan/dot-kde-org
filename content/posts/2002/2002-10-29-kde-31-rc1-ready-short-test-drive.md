---
title: "KDE 3.1 RC1: Ready for a Short Test Drive"
date:    2002-10-29
authors:
  - "Dre"
slug:    kde-31-rc1-ready-short-test-drive
comments:
  - subject: "Highcolor Icons"
    date: 2002-10-29
    body: "\"if you are wed to the hicolor icons, please note that they have been moved to the kdeartwork package\"\n\nAargh! These are wonderful icons. They have a classic and simple look without being ugly. Eye candy is good, but candy is desert. Sometimes a sensible main course is what is desired. At least keep hicolor in the kdelibs package."
    author: "David Johnson"
  - subject: "Re: Highcolor Icons"
    date: 2002-10-29
    body: "I disagree.  kdelibs should not be bloated with redundancy.  That's the whole reason theres a seperate package for the extra icon sets."
    author: "Jeremy Huddleston"
  - subject: "Re: Highcolor Icons"
    date: 2002-11-01
    body: "If we're worried about redundancy, then why wasn't Crystal put into -artwork instead?"
    author: "David Johnson"
  - subject: "Re: Highcolor Icons"
    date: 2002-11-01
    body: "Because we need atleast one icon set in the main kde distribution.  You can't have redundancy without something to be redundant about =)"
    author: "Jeremy Huddleston"
  - subject: "Re: Highcolor Icons"
    date: 2002-10-29
    body: "Agreed.  All the new \"eye candy\" to me is just clutter.  I don't like keramik and I don't like the crystal icons.  Last I checked the hicolor default style was still part of KDE, so that's good, but it's unfortunate that the hicolor icons are now gone.  You can bet I'll be picking them up to use as my main set."
    author: "KDE User"
  - subject: "Re: Highcolor Icons"
    date: 2002-10-29
    body: "They are not GONE, they are moved to a new package. That makes them an optional part of a KDE installation, but will still be very commonly distributed."
    author: "Nick"
  - subject: "Re: Highcolor Icons"
    date: 2002-10-30
    body: "i think it is important to remember that while there are those who will still install and use the HiColor theme and icons, there are many who will now go with the KDE default look. many already use a different style than the default (e.g. litev3 is very popular, which btw  is now the default style for local displays)\n\nso while it means some will not use the default anymore, many others now will. judging by overall response, probably many more others."
    author: "Aaron J. Seigo"
  - subject: "Re: Highcolor Icons"
    date: 2002-10-30
    body: "er, and by \"local displays\" i meant \"locolor displays\" *sigh*"
    author: "Aaron J. Seigo"
  - subject: "Re: Highcolor Icons"
    date: 2002-11-01
    body: "I'm not really \"wed\" to the \"HiColor\" icons, what I AM wed to is my icons!\n\nThere is more than one issue here:\n\nI don't like the 3D folders and so I use the old 2D folder icons.  I now have quite a few folder icons for various purposes many of which consist of the 22 pixel icon combined with the 32 pixel folder icon for 32 pixels and a half size 22 pixel icon combined with a plain color background the same color as the folder (I use FFDCA8 for all folders, but have nothing against using different colors) for the 16 pixel icons.  Note that it occured to me that this could be done automatically.  That is, for example, there is a directory: \"~./kpackage\" so it could automatically have an icon as I have made for it.\n\nMany (non-kde) programs come with an icon or I have 'borrowed' one.  These look best with the HiColor icons.  They look very much out of place with the Crystal Icons.\n\nWhat happened to usability.  Everaldo's icons may be great art, but at 32 pixels they are not very usable.  They look fuzzy and lack contrast.  They simply not easy to distinguish from similar ones.\n\n--\nJRT\n\n"
    author: "James Richard Tyrer"
  - subject: "Nah, Crystal isn't SVG"
    date: 2002-10-29
    body: "Title says all. Still, it's a really nice icon set, I've been using it with KDE 3.0.4 for a long time now."
    author: "TheFogger"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-29
    body: "The delivered version is not, but all icon sizes are automatically generated from SVGs."
    author: "Anonymous"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-29
    body: "I'd really like icons that scale with the size of the panel. That and 37 pixel wide desktop icons ;)"
    author: "zelegans"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-29
    body: "There is a SVG variant of Crystal, just don't know if it's already in 3.1."
    author: "qwerty"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-30
    body: "Hi, \n\nYeah, at the current point of time the name of the theme is a bit misleading. Actually we _do_ have vector data for most (about 70-90%) of the crystal-icons. And technically it _would_ be possible to use them on the fly as well as pregenerated. It's just that ksvg2png while giving excellent results for 98% of all icons needs more testing and that we still have some icons left for which SVG-data doesn't exist yet.\n\nSo instead of releasing something that hasn't been properly tested we decided to use Pixmaps (which have been partially autogenerated from the SVGs) instead of using the \"real\" data.\n\nTo make the transistion easier we decided to name the theme already \"Crystal SVG\" to make the transition easier. Wait for some news concerning this topic which will be released soon.\n\nRegards, \nTackat\n"
    author: "Tackat"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-30
    body: "Wow, cool. I always a associated SVG with a rather cartoonish look, not \"shiny\" like Crystal is. I didn't know that you could create such icons with SVG."
    author: "TheFogger"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-30
    body: "To my opinion SVG is one of the best web-technologies to watch, as a graphics-exchange format, as a graphics format for mobile devices, as a printer description language (combined with other XML-technology, such as XSL-FO), etc. ...\n\nI therefore think that the KDE team should give SVG top priority as a base-technology for the KDE-desktop (SVG enable all KDE-applications) and as an integral part of the Konqueror browser. Combined with other XML technology and scripting, SVG allows very useful applications and is for the first-time a fully documented vendor-neutral graphics format as an exchange format between different applications and platforms. I am very happy that ksvg (svg.kde.org) already exists - but I think that it should have more support and priority within the KDE team than it has now.\n\nCheck f.e. http://www.svgopen.org/, http://www.kevlindev.com/ and www.carto.net to see some useful applications.\n\nAs a conclusion:\nSVG for icons is nice - but with SVG you can do much more and it should really have more weight within the KDE project!\n"
    author: "Andreas Neumann"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-31
    body: "As a ksvg developer, I am very happy with these comments :) As a\nmatter of fact, I'll forward them :)\nI looked at carto.net and it looked very nice, but I havent really found\nthe time to study it.\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-31
    body: "Hi Rob,\n\ncan you recommend some svg graphic tools for creating svg icons? Tackat & everaldo use Adobe Illustrator, but I don't want to use proprietary tools, especially not Adobe products (you know the story when Adobe lawyers threatened KDE with lowsuit for using name KIllustrator). So, can you recommend an open source graphic editor which can export svg fromats that can be read by ksvg?\nI tried Sodipodi, Karbon etc, but the results were poor. Either their exported svg files can't be read by each other or can't be read by ksvg properly.\nNow we have a situation that the next default icon theme for KDE 3.1 is being created by artists who are running windows or Mac platform with a graphic tool from a company known for threatening KDE and other open source projects.\n\nRegards,\n\nantialias"
    author: "Antialias"
  - subject: "Re: Nah, Crystal isn't SVG"
    date: 2002-10-31
    body: "Hi,\n\n>I tried Sodipodi, Karbon etc, but the results were poor. Either their exported >svg files can't be read by each other or can't be read by ksvg properly.\n>Now we have a situation that the next default icon theme for KDE 3.1 is being >created by artists who are running windows or Mac platform with a graphic tool >from a company known for threatening KDE and other open source projects.\n\nI agree, it is not ideal.\nBest thing to do IMHO is to support the oss alternative, karbon14, by\nsending in bug reports and wrongly exported svg files, then we can fix them. Believe me, we are really aiming to help the artists on a nice vector drawing app, that is our first major goal, just tell us where we fall short currently :)\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Compiling"
    date: 2002-10-29
    body: "I'm compiling RC1 on my home system from work as we speak. Can't wait to get home and try it. Ran into a few compile snags - one with kfontinst and the locations of freetype, one with kmail and a missing mStartupFolder declaration, and one large mess in kmidi. I fixed the first two - I just bypassed compiling kmidi since I don't use it.\n\nI suspect the first and last are more related to problems with my setup than with KDE itself. But as for the kmail error... I can't see that working without help on other systems... ?"
    author: "Vic"
  - subject: "Re: Compiling"
    date: 2002-10-29
    body: "Missing mStartupFolder declaration? I thought it was a duplicate declaration, patch to fix this:\nhttp://kdewebcvs.pandmservices.com/cgi-bin/cvsweb.cgi/kdenetwork/kmail/kmmainwin.h.diff?r1=1.147&r2=1.148"
    author: "Anonymous"
  - subject: "Re: Compiling"
    date: 2002-10-29
    body: "Well - I certainly had to add one, since there was none\n\nmStartupFolder is undeclared\n\nAfter adding it to the list of QStrings in kmmainwin.h it finished up happily."
    author: "Vic"
  - subject: "Re: Compiling"
    date: 2002-10-30
    body: "Yeah, seems like two people added it to fix without coordination after tagging. :-)"
    author: "Anonymous"
  - subject: "Re: Compiling"
    date: 2002-10-30
    body: "It seems natural to me that the tarballs should be compiled cleanly before being released, to catch these things :) It may only be an RC, but we still want as many people as possible testing it, I presume. It was a simple fix, but other people may not think so. :)"
    author: "Haakon Nilsen"
  - subject: "Re: Compiling"
    date: 2002-10-31
    body: "Thanks for the tip.. I needed that "
    author: "Darren"
  - subject: "Why Realese Candidate? ;)"
    date: 2002-10-29
    body: "Well the beta2 is as stable as KDE 3.0.4. Never had any crashes. Again perfect work from the KDE team.\n\nDim"
    author: "Dimitri"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-29
    body: "Check out the feature status page to see what isn't quite finished: http://developer.kde.org/development-versions/kde-3.1-features.h\n\nAnd there are still minor bugs to take down (encapsulated messages in kmail, replys to html messages n kmail... get on the reporting, and some font problems to name some I've seen), so start reporting them so KDE 3.1 is as bug free as possible"
    author: "Jeremy Huddleston"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-29
    body: "Check out the KDE 3.0 feature page to see what's not yet finished for 3.0. Remember, coders are lazy in updating documentation."
    author: "Anonymous"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-29
    body: "HAHA!!! Good point, but I noticed it was updated about 2 (3?) weeks ago, so it's atleast a starting point =).  Plus there's that nice quick link to the critical bugs on top."
    author: "Jeremy Huddleston"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-30
    body: "I have some issues with Konqueror file manager mode with linux 2.5 series\nkernels, I use 2.5.44 currently and when i start konqueror it just hangs, the web browser mode works perfectly though\nI use offical debian packages for beta2 from kde.org\nANybody experienced this issues?"
    author: "Anonymous"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-30
    body: "mmmmm.... I'd like to give it a try, but 2.5.x series of the kernel really frighten me... aren't there any chances of destroying the filesystem or similar? My laptop uses only acpi, and I really need some upgrade, I hope they have done it right for 2.6.\n\nSorry, I think the question might be out of topic...\n\n"
    author: "myself"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-31
    body: "Well\nI use 2.5.44 on the laptop myself\nI have got everything to work including ACPI\nsupport, I haven't had any issues so far\nexcept the konqueror hang\nwhich is kinda strange given it only hangs\nin file manager mode"
    author: "Anonymous"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-31
    body: "Thanks a lot for the info. Maybe this weekend.... after I do backups ;-)"
    author: "myself"
  - subject: "Re: Why Realese Candidate? ;)"
    date: 2002-10-31
    body: "I experienced exactly the same a while ago \nwith a KDE version from CVS \n(on a PIII desktop machine on a 2.4 Linux kernel).  \nSo maybe the freeze is not related to the kernel \nor the laptop.\n\nSome days later the problem was just gone.\nI've no idea what the reason has been.\nI cannot remember doing anything about it \nlike deleting my ~/.kde"
    author: "cm"
  - subject: "Critical Patch"
    date: 2002-10-29
    body: "A patch was committed today to address a critical bug, that I don't think got into RC1 (I'm not 100% positive about that, but I'm pretty sure).  I advise you guys to apply it before testing out RC1.\n\nhttp://bugs.kde.org/show_bug.cgi?id=48923"
    author: "Jeremy Huddleston"
  - subject: "Re: Critical Patch"
    date: 2002-10-29
    body: "Or simply keep in mind what http://www.kde.org/info/3.1.html says:\n - Deleting an icon on the desktop deletes all the contents behind it Bug #48923 ) "
    author: "Anonymous"
  - subject: "Re: Critical Patch"
    date: 2002-10-29
    body: "yes, but I'm saying it would probably be better to apply the patch rather than remembering not to delete stuff =)"
    author: "Jeremy Huddleston"
  - subject: "KDE 3.1b2 was slow at startup"
    date: 2002-10-29
    body: "Isn't there a way to improve KDE start speed ?\nIt's very too slow !"
    author: "suggestion man"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-29
    body: "Yes, just update your hardware :-)"
    author: "BesserWisser"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-29
    body: "That is not an acceptable answer (even though you're joking)\n\nBut the original poster may want to check inside his startkde script and delete services he does not need.\n\nAlso, if you compile it with a newer version of gcc (or use Intel's), it will run faster.\n\nI do wish the developers would try to make speed more of an important area."
    author: "antiphon"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-29
    body: "I'm a little confused about compiling KDE with a compiler that's different from the system's default compiler. gcc 2.9x and gcc 3.x are binary incompatible. Does that mean I have to recompile all libraries that KDE depends on with the new compiler? What about system libraries such as glibc?\n\nIt would be nice if some expert can write a two-paragraph \"HOWTO\" for ignorant folks like myself."
    author: "Snarf"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "Only libraries that are written in C++. That includes the obvious Qt, and FAM, if you have it ( a lot of people miss that one ). There may be others, but I don't know of any. "
    author: "Sad Eagle"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "This is not something that should be attempted if you don't know what you're doing. If you want to use the new compiler I strongly suggest you upgrade to one of the newer releases of your distribution rather than trying to do it yourself. To gain the benefits you're looking for you need to upgrade glibc, gcc, binutils etc. and all C++ libraries.\n\nRich.\n\n\n\n"
    author: "Richard Moore"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-29
    body: "Runtime and startup time are very different things.\n\nUpgrade to glibc 2.3 to improve startup time (with Prelink).  The startup time problems you see in KDE are the results of problems in ELF and gcc.  glibc 2.2.5 featured comb-relocs which speeds it up 30%-50%.  prelinking should make relocation time negligible.  Also something that kdeinit does.\n\nIntel's compiler won't make KDE run or start faster, it doesn't excel at code like KDE's (it prefers math-intensive stuff).\n\ngcc 3.2 is about 15% faster for run-time.  Yes, you need to recompile all your libs for it."
    author: "Charles Samuels"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "\"The startup time problems you see in KDE are the results of problems in ELF and gcc. glibc 2.2.5 featured comb-relocs which speeds it up 30%-50%. prelinking should make relocation time negligible.\"\n\nIt may make relocation time negligible, but startup time is not solely caused by relocation.\n\n"
    author: "Johan Veenstra"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "of course it isn't, but it is a large chunk of the problem. KDE can always use more optimizations and continues to get them. there are limits, however.\n\nof course, having seen how fast the latest SuSe was even on ancient hardware, i'm quite impressed at how fast it can be when properly built and integrated."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "Well, kdeinit doesn't help you much with dlopen'ing libs, though, so linking time is still major -- on my box w/ combreloc loading KHTMLPart, kjs_ecma, etc., takes a noticeable amount of time. Ditto for libkonq_sound - dlopening it seems to cause a 1/3 second lag, although I do not recall whether my profile-point would have caught it initialization (i.e. presumably connecting to aRtsd), so some of it may not be dlopen itself in this particular case"
    author: "Sad Eagle"
  - subject: "Glibc upgrade"
    date: 2002-11-02
    body: "How hard is it? Can I do './configure --prefix=/ && make install' and expect it to work? Will I break existing apps? Do I have to recompile everything?"
    author: "Stof"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-29
    body: "I don't know what your problem is: Update binutils, glibc, gcc, your hardware, XFree, KDE & Qt, delete fonts, compile from source, change compilation settings, ..."
    author: "Anonymous"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "Try poking around the startkde script and remove the bit that scans for plugins.  I've told people to do this and be delighted - cutting the startup time to 50% or even 33% of what it was.  I'm still of the opinion that it should be forked off in the startup, and executed after 60 seconds.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "It shouldn't be there at all by default.  Please file a bug for its removal  and have it run by demand in some config panel.\n\n50% slowdown is *serious*.\n"
    author: "ac"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "Don't file it to KDE, it's afaik a Mandrake messing of startkde."
    author: "Anonymous"
  - subject: "Re: KDE 3.1b2 was slow at startup"
    date: 2002-10-30
    body: "Here is the fastest setup I ever had :\n-gcc3.2 + new binutils (compiled together)\n-recompile new glibc\n-recompile your c++ libs, png, jpeg, etc, \n-recompile X\n-recompile qt with --no-g++-exceptions (and -qt-gif,-thread, etc)\n-recompile all kde with --disable-debug and --enable-final\n\nyour system will be really faster.\n(but ok it's time consumming and not so easy to do)\n\nHope this helps"
    author: "socialEcologist"
  - subject: "Compiling it now"
    date: 2002-10-29
    body: "I'm compiling now! Since I've just installed Redhat 8.0, I thought I'd\njust skip their KDE alltogether :>\n\nIt's been ages since I last compiled KDE from source, it's really grown ;)\nkdelibs took some 4-5 hours on my rusty old celeron433mhz. But it'll be\nworth it, 3.1 looks sweet.\n\nAlso, thanks to the nice people on #kde-users at irc.kde.org for their patient\nsupport :)"
    author: "Haakon Nilsen"
  - subject: "NO! Why remove them!"
    date: 2002-10-30
    body: "They are simple and fir in with all linux apps. Crystal isn't even 1/4 done, there are so many apps that need to be crystalized!\n\nLeave Hi-Color in there for those who want a complet desktop. Please!"
    author: "Dont remove them please"
  - subject: "Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-10-30
    body: "Latest KDE-CVS needs a Qt-3.1pre (qt-copy). And it is soo unstable compared to the lastest possible KDE-CVS+Qt-3.0.5. Qt 3.1 seems to have problems with e.g. font-handling. Using a current qt-copy e.g. Konqi crash too often with stuff like this:\n\n[New Thread 1024 (LWP 1469)]\n0x41015569 in wait4 () from /lib/libc.so.6\n#0  0x41015569 in wait4 () from /lib/libc.so.6\n#1  0x410913f8 in __DTOR_END__ () from /lib/libc.so.6\n#2  0x40ece402 in waitpid () from /lib/libpthread.so.0\n#3  0x406720c0 in KCrash::defaultCrashHandler (sig=11) at kcrash.cpp:235\n#4  0x40ecc134 in pthread_sighandler () from /lib/libpthread.so.0\n#5  <signal handler called>\n#6  0x40dfca2d in XGetFontProperty () from /usr/X11R6/lib/libX11.so.6\n#7  0x409aab9e in QFontPrivate::fillFontDef () from /usr/qt/3/lib/libqt-mt.so.3\n#8  0x409b09ed in QFontPrivate::initFontInfo ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#9  0x409b245a in QFontPrivate::load () from /usr/qt/3/lib/libqt-mt.so.3\n#10 0x409b148c in QFontPrivate::loadUnicode () from /usr/qt/3/lib/libqt-mt.so.3\n#11 0x409b179d in QFontPrivate::load () from /usr/qt/3/lib/libqt-mt.so.3\n#12 0x409a8e37 in QFontMetrics::QFontMetrics ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#13 0x41bcfdcc in khtml::Font::update (this=0x84d0f20, devMetrics=0x80657a8)\n    at font.cpp:194\n#14 0x41be1780 in khtml::CSSStyleSelector::styleForElement (this=0x831c348, \n    e=0x84d0a98, state=0) at cssstyleselector.cpp:362\n#15 0x41b5c5ac in DOM::ElementImpl::attach (this=0x84d0a98)\n    at dom_elementimpl.cpp:318\n#16 0x41b6b8f9 in KHTMLParser::insertNode (this=0x8474310, n=0x84d0a98, \n    flat=false) at htmlparser.cpp:308\n#17 0x41b6b7cd in KHTMLParser::parseToken (this=0x8474310, t=0x8474214)\n    at htmlparser.cpp:267\n#18 0x41b748a9 in HTMLTokenizer::processToken (this=0x84741e0)\n    at htmltokenizer.cpp:1561\n#19 0x41b72f9d in HTMLTokenizer::parseTag (this=0x84741e0, src=@0x84742ec)\n    at htmltokenizer.cpp:1094\n#20 0x41b73ab9 in HTMLTokenizer::write (this=0x84741e0, str=@0xbfffe91c, \n    appendData=true) at htmltokenizer.cpp:1348\n#21 0x41b284b2 in KHTMLPart::write (this=0x83ddf48, \n    str=0x84c1078 \"<TABLE CELLSPACING=\\\"0\\\" CELLPADDING=\\\"0\\\" WIDTH=\\\"130\\\" BORDER=\\\"0\\\">\\n<TR>\\n<TD BGCOLOR=\\\"#eeeeee\\\" WIDTH=\\\"5\\\"> &nbsp; </TD>\\n<TD BGCOLOR=\\\"#eeeeee\\\">\\n<FONT FACE=\\\"Helvetica, Arial\\\" COLOR=\\\"#333333\\\" SIZE=\\\"-1\\\"><B>Sear\"..., len=203) at khtml_part.cpp:1399\n#22 0x41b261bf in KHTMLPart::slotData (this=0x83ddf48, kio_job=0x82f9be0, \n    data=@0xbfffed54) at khtml_part.cpp:1109\n#23 0x41b3d00a in KHTMLPart::qt_invoke (this=0x83ddf48, _id=9, _o=0xbfffeab0)\n    at khtml_part.moc:344\n#24 0x409e01d4 in QObject::activate_signal () from /usr/qt/3/lib/libqt-mt.so.3\n#25 0x40183c21 in KIO::TransferJob::data (this=0x82f9be0, t0=0x82f9be0, \n    t1=@0xbfffed54) at jobclasses.moc:728\n#26 0x40174090 in KIO::TransferJob::slotData (this=0x82f9be0, \n    _data=@0xbfffed54) at job.cpp:737\n#27 0x40184487 in KIO::TransferJob::qt_invoke (this=0x82f9be0, _id=18, \n    _o=0xbfffebd4) at jobclasses.moc:807\n#28 0x409e01d4 in QObject::activate_signal () from /usr/qt/3/lib/libqt-mt.so.3\n#29 0x40168d73 in KIO::SlaveInterface::data (this=0x8404448, t0=@0xbfffed54)\n    at slaveinterface.moc:195\n#30 0x401674ed in KIO::SlaveInterface::dispatch (this=0x8404448, _cmd=100, \n    rawdata=@0xbfffed54) at slaveinterface.cpp:246\n#31 0x40166ed8 in KIO::SlaveInterface::dispatch (this=0x8404448)\n    at slaveinterface.cpp:191\n#32 0x40164c2d in KIO::Slave::gotInput (this=0x8404448) at slave.cpp:221\n#33 0x401665f9 in KIO::Slave::qt_invoke (this=0x8404448, _id=4, _o=0xbfffee68)\n    at slave.moc:114\n#34 0x409e01d4 in QObject::activate_signal () from /usr/qt/3/lib/libqt-mt.so.3\n#35 0x409e0365 in QObject::activate_signal () from /usr/qt/3/lib/libqt-mt.so.3\n#36 0x40c31904 in QSocketNotifier::activated ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#37 0x409f678d in QSocketNotifier::event () from /usr/qt/3/lib/libqt-mt.so.3\n#38 0x40996256 in QApplication::internalNotify ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#39 0x40996054 in QApplication::notify () from /usr/qt/3/lib/libqt-mt.so.3\n#40 0x40611290 in KApplication::notify (this=0xbffff378, receiver=0x8319718, \n    event=0xbffff0b0) at kapplication.cpp:441\n#41 0x4097b157 in QEventLoop::activateSocketNotifiers ()\n   from /usr/qt/3/lib/libqt-mt.so.3\n#42 0x4095dcd2 in QEventLoop::processEvents () from /usr/qt/3/lib/libqt-mt.so.3\n#43 0x409a621e in QEventLoop::enterLoop () from /usr/qt/3/lib/libqt-mt.so.3\n#44 0x409a6186 in QEventLoop::exec () from /usr/qt/3/lib/libqt-mt.so.3\n#45 0x409963d9 in QApplication::exec () from /usr/qt/3/lib/libqt-mt.so.3\n#46 0x416ab774 in main (argc=2, argv=0x805e500) at konq_main.cc:130\n#47 0x0804dbd7 in launch (argc=2, _name=0x805e77c \"konqueror\", \n    args=0x805e78f \"\\001\", cwd=0x0, envc=1, envs=0x805e7a0 \"\", \n    reset_env=false, tty=0x0, avoid_loops=false, \n    startup_id_str=0x805e7a4 \"susi;1035933340;324242;23568\") at kinit.cpp:547\n#48 0x0804ecd7 in handle_launcher_request (sock=7) at kinit.cpp:1023\n#49 0x0804f491 in handle_requests (waitForPid=0) at kinit.cpp:1189\n#50 0x080506b0 in main (argc=3, argv=0xbffff934, envp=0xbffff944)\n    at kinit.cpp:1534\n#51 0x40f88671 in __libc_start_main () from /lib/libc.so.6\n\nBye\n\n  Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-10-30
    body: "I have a similar problem with KDECVS.. I get this everytime I close konqueror:\n\n0x40fde739 in wait4 () from /lib/i686/libc.so.6\n#0  0x40fde739 in wait4 () from /lib/i686/libc.so.6\n#1  0x4105b340 in sys_sigabbrev () from /lib/i686/libc.so.6\n#2  0x40e2fa73 in waitpid () from /lib/i686/libpthread.so.0\n#3  0x405eaeb0 in KCrash::defaultCrashHandler(int) ()\n   from /opt/kdecvs/lib/libkdecore.so.4\n\nI dunno if this is my fault, but I didn't have this problem a couple of months back.."
    author: "John Morris"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-10-30
    body: "Don't confuse dot.kde.org with bugs.kde.org."
    author: "Anonymous"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-10-30
    body: "These are known bugs AFAIK. OK, a posting a complete backtrace is maybe a little bit too long ;-) but the question in the title is valid.\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "Yes"
    date: 2002-10-30
    body: "You answered your question yourself with the first sentence. :-)"
    author: "Anonymous"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-10-31
    body: "Oh, so only praise, and not problems, are welcome here?"
    author: "Neil Stevens"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-10-31
    body: "*beep* *beep* *beep*"
    author: "TrollDetector"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-11-01
    body: "The sensitivity on our new detector is a little high.  It's supposed to only go off for trolls, but sometimes it goes off for grumpy gnomes.  Any way we can recalibrate it?\n\nBut...the beep message IS pretty funny!"
    author: "ac"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-11-03
    body: "it's about putting such reports where they will actually do some good, e.g. bugs.kde.org. they aren't nearly as effective or useful when posted as a comment to dot.kde.org."
    author: "Aaron J. Seigo"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-12-10
    body: "...and a month later, the original question was never answered. Ah well."
    author: "spacefiddle"
  - subject: "Re: Will KDE-3.1 depend on a Qt-3.1?"
    date: 2002-12-10
    body: "Who can read has an advantage: http://dot.kde.org/1035902091/1035933509/1035950008/1035958915/1035964613/"
    author: "Anonymous"
  - subject: "Artsd"
    date: 2002-10-30
    body: "Hi. Is it only me? or does anyone had experience artsd being a little \nbit buggy (kde-3.1rc1). It plays well for about 10 secs,then makes a noise, continues to play well, then makes a noise, and so goes on.\n\nI compiled it with gcc 3.2\n\nThanks\n"
    author: "Jes\u00fas Antonio S\u00e1nchez A."
  - subject: "Re: Artsd"
    date: 2002-10-30
    body: "Nope, been working just fine for me. Sounds like a hardware-specific issue.\n"
    author: "Richard"
  - subject: "Re: Artsd"
    date: 2002-10-30
    body: "Hmm, sounds like you are listening to the Rolling Stones ...\nYour description fits perfectly ):-\nTry mozart instead ))::--"
    author: "thefrog"
  - subject: "Re: Artsd"
    date: 2002-11-02
    body: ":-D"
    author: "KDE User"
  - subject: "Re: Artsd"
    date: 2002-11-02
    body: "+5, Funny!\n"
    author: "Xanadu"
  - subject: "Re: Artsd"
    date: 2002-10-30
    body: "I've noticed the same yesterday on my Mandrake 9.0/KDE 3.0.3 desktop. I don't know the reason for sure but the only thing i've done since the sound worked fine was installation of HSF soft modem drivers (My modem device is Pentagram Hex 2 based on Conexant chipset). So I can suppose that my problem is caused by buggy modem drivers.\n\nMarcin"
    author: "mar_10"
  - subject: "Re: Artsd and its future"
    date: 2002-10-31
    body: "I hope it's only you, because don't count on getting many aRts bugs fixed. It's largely a \"one man project\" and such projects are badly hurt if that \"one man\" loses interest. That _seems_ to be happening to aRts. Check the kde-multimedia archives for the last month and judge for yourself. I could be wrong, actually I hope I am."
    author: "Joe"
  - subject: "Re: Artsd and its future"
    date: 2002-10-31
    body: "Do you expect the one man to have conversations with himself on the mailing-list? :-)"
    author: "Anonymous"
  - subject: "Re: Artsd and its future"
    date: 2002-10-31
    body: "Check kdenonbeta/arts for recent additions to arts."
    author: "Carsten Pfeiffer"
  - subject: "Re: Artsd and its future"
    date: 2002-11-01
    body: "But I get a little worried when messages like\n\nhttp://lists.kde.org/?l=kde-multimedia&m=103588908821958&w=2\n\ngo unanswered.\n"
    author: "Joe"
  - subject: "Re: Artsd and its future"
    date: 2002-11-05
    body: "Matthias Welwarsky has taken care of that issue already."
    author: "Carsten Pfeiffer"
  - subject: "Re: Artsd [is the same]"
    date: 2002-11-06
    body: "I don't think artsd has changed since KDE 3.0... at least, the version # has been the same for the last few KDE updates..."
    author: "Luke-Jr"
  - subject: "Re: Artsd"
    date: 2003-02-09
    body: "I have exactly the same problem using arts 1.1.49/KDE 3.1 on SuSE Linux 8.1. I use a Intel 82801. Playing MP3 files using noatun or xmms (arts plugin) results in noise with increasing volume after 1-2 minutes (or even 10-15 minutes) Noise will not stop until I terminate the player or restart artsd. Also when streaming sound or video using realplay, noise occurs after several minutes.\n\nUsing xmms with stopped artsd and OSS output plugin works well.\n"
    author: "Benjamin Stocker"
  - subject: "Re: Artsd"
    date: 2003-04-18
    body: "I have the same problem and even turning off artsd and playing mp3's on xmms through OSS does not help :("
    author: "Maxim"
  - subject: "Word wrap"
    date: 2002-10-30
    body: "I've been hearing that the RC1 release is supposed to have soft word wrap which supposedly will even respect indentation. Can anyone tell me if this is true since I don't have the pipe or the time to get RC1 up and running?\n\nIt has been absolutely ridiculous that line-wrapping in Un*x has been so rudimentary that we haven't been able to do this. Only the August HTML editor has word wrapping so far as I know but even its wrapping plays tricks w/the keyboard like Emacs's and GNOMEs crappy character wrapping does.\n\nI couldn't believe it when I found out that UNIX had no soft wrapping editors. Please tell me that this is no longer true! As it is now, I have to fire up Windoze programs under Wine to edit my HTML and keep my sanity :("
    author: "antiphon"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "Kate in HEAD/RC1 does support soft wrap -- View-> Dynamic Word Wrap ( not to be confused with the static word wrap stuff in the settings dialog); I am not sure what you mean by respecting indentation with that, though. However, I'd be *really* surprised if EMACS didn't support softwrap, seeing how it has just about everything imaginable in there. IIRC, Vim has a softwrap mode on by default (although it's handling of arrows confused me somewhat when I tried it, I think)\n"
    author: "Sad Eagle"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "<i>I am not sure what you mean by respecting indentation with that, though. However, I'd be *really* surprised if EMACS didn't support softwrap, seeing how it has just about everything imaginable in there. IIRC, Vim has a softwrap mode on by default (although it's handling of arrows confused me somewhat when I tried it, I think)</i>\n\nWhen I say respecting line indentation, I am referring to that if a line is indented 4 tabs, when it is soft-wrapped, the wrapped portion of the line will be in alignment with the indentation of the first.\n\nEmacs does have _line_ wrapping but it is rather primitive (like vim and GNOME's) in that it is character based and not word based. Thus your words will be broken up if they're at the end of a line. Also, both have irritating keyboard motions, i.e. if you want to move down to a virtual line, you cannot do so by pressing a keystroke but must use the mouse (or some other irritating variation)."
    author: "antiphon"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "Sorry,\nI've been using vi for ages, and it has always had word wrap\n:se wm=5 to WORD wrap at 5 characters from the right border\nAutoindent has been part of VI sine (at least) the mid 80s\n:se ai\n\nVim has improved this a bit with it's smart indent option.\n\nEmacs has had what you want for ages as well (at least xemacs, I've never tried gnu emacs)"
    author: "Gerhard"
  - subject: "Re: Word wrap"
    date: 2002-11-02
    body: "\"Emacs does have _line_ wrapping but it is rather primitive (like vim and GNOME's) in that it is character based and not word based.\"\n\nMm, nope. I use XEmacs on a regular basis, and it's line-wrap mode never breaks up a word. I don't think that it respects indentation (at least, not with the Fundamental major-mode), but I'm sure there's a way to make it do what you want.\n\nBut you want to start out with:\nM-x auto-fill-mode"
    author: "Ian Eure"
  - subject: "Re: Word wrap"
    date: 2006-09-26
    body: "I think the emacs M-x auto fill mode you are talking about inserts a soft return which then becomes part of the text stream.  So, if you narrow margins or lengthen them, for instance, the text does not re-wrap.  What the person way up stream on this thread was looking for really does not exist. \n\nOne way to implement the desired feature may be found at this web site:\n\nhttp://penguinpetes.com/b2evo/index.php?title=howto_make_emacs_use_soft_word_wrap_like&more=1&c=1&tb=1&pb=1\n\nI have not tried this so I cannot attest to it.  It is important to me. If I can get it to work, I can use emax/xemax, otherwise, not really...\\"
    author: "g laden"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "No Unix editor with soft word-wrap?\nThe _best_ editor available --> EMACS <-- is a typical unix app! \nI never found an editor with comparable auto-indent-capabilities,\nso this is still my preferred choice for all kind of programming jobs (C/C++, HTML,\nTeX and so on)\n"
    author: "Ruediger Knoerig"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "How do you enable soft word-wrapping in EMACS?  The default is hard wrapping (not even word wrapping in programming modes)."
    author: "not me"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "Those who have recommended emacs to me have not read my original post in which I said that emacs (and xemacs and vi and GNOME) feature a primitive CHARACTER-based wrapping which breaks up words in the middle and puts them onto the next line. This is irritating and very confusing trying to explain to a person who knows about computers but has never used Un*x why such a basic thing cannot be done right.\n\nThere is absolutely no reason why the wrapping should be based on characters rather than on words. I'm hoping that KDE doesn't follow in this bad precedent."
    author: "antiphon"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "I know that both vi(m) and (x)emacs both have ways to automatically format a section of text into 80 columns.  I'm sure it's customizable, too.  And I wouldn't be surprised if there's a way in each of the editors to have it do that automatically - it's just a matter of learning how to use the program properly.  "
    author: "fubar"
  - subject: "Wrong in vim."
    date: 2002-10-30
    body: "'linebreak' 'lbr'\tboolean\t(default off)\n\t\t\tlocal to window\n\t\t\t{not in Vi}\n\t\t\t{not available when compiled without the  |+linebreak|\n\t\t\tfeature}\n\tIf on Vim will wrap long lines at a character in 'breakat' rather\n\tthan at the last character that fits on the screen.  Unlike\n\t'wrapmargin' and 'textwidth', this does not insert <EOL>s in the file,\n\tit only affects the way the file is displayed, not its contents.  The\n\tvalue of 'showbreak' is used to put in front of wrapped lines.  This\n\toption is not used when the 'wrap' option is off or 'list' is on.\n\tNote that <Tab> characters after an <EOL> are mostly not displayed\n\twith the right amount of white space.\n"
    author: "Moritz Moeller-Herrmann"
  - subject: "More hints for vim"
    date: 2002-11-03
    body: "I have a file called updown.vim that makes vim behave almost like notepad.\nSome features are still missing, like indentation after wrap and the status bar (it will then show the paragraph number instead of line number...)\n\n\" Up, Down, Home and End keys in normal and insert mode\nmap <up> gk\nimap <up> <C-o>gk\nmap <down> gj\nimap <down> <C-o>gj\nmap <home> g<home>\nimap <home> <C-o>g<home>\nmap <end> g<end>\nimap <end> <C-o>g<end>\n\n\" Don't break words in middle\nset linebreak\n\n\" Show incomplete paragraphs even when they don'f fit on screen (avoid @'s)\nset display+=lastline"
    author: "yuu"
  - subject: "Re: More hints for vim"
    date: 2003-11-02
    body: "THANK YOU SO MUCH\n\nI have been trying to figure out how to get it to move the cursor by visual lines rather than stored-in-the-file lines.  And the fucking hiding of paragraphs was driving me insane.  I tried some other keybindings recommended to fix the visual lines thing, but they did not work half the time.  Your method is flawless.\n\nThank you, thank you, thank you !\n\n - raven morris\n\n"
    author: "Raven Morris"
  - subject: "Re: More hints for vim"
    date: 2007-02-09
    body: "That is a fantastic tip - you just (well, in 2002) solved the very last thing that was really annoying me about vim!  :-)  Thanks very much!"
    author: "James"
  - subject: "Re: More hints for vim"
    date: 2008-07-01
    body: "Ditto. Thanks a million."
    author: "Stephan Sokolow"
  - subject: "Works great"
    date: 2003-05-06
    body: "It works great, you rock"
    author: "Anonymous"
  - subject: "Re: Word wrap"
    date: 2002-11-14
    body: "Youre really cute...i KNOW that emacs is a very good editor and bla blah..\nsimply say how to enable the word wrap and howto set the margins !\nlike : M-x auto-fill-mode , left margin via (options-> advanced-> emacs ->  editing ->fill -> left margin)(under xemacs)"
    author: "Count"
  - subject: "Word wrap with emacs"
    date: 2004-08-10
    body: "Word wrap in editors like editplus / context and so on is fortunately different to the word wrap in emacs. emacs (via auto-fill or M-q) adds a kind of line-break that unfortunately prevents built in incermental search (STRG-F), search-replace, regexp search, 3rd party search (e.g. grep) etc from finding the string \"xxx (linebreak) yyy\" when you are searching for \"xxx yyy\". To me, that really is a pain in the ass. To me, that is THE reason for changing the tool, as a non-destructive (!) word wrap is essential (for me) when working with normal text. \n\nI have been searching the net for a workaround or even for ANY way to make the search / replace / grep oversee the added linebreak and haven't found it yet. Help is greatly appreciated but I am not expecting any.\n\nRalph"
    author: "Ralph Buse"
  - subject: "Re: Word wrap with emacs"
    date: 2005-10-12
    body: "Don't suppose you could just write your own? Its real easy, instead of counting '\\n' as a character, ignore it. For that matter, you could ignore anything that you don't want to be a break. I, for one, rewrote word count so that it didn't count hyphens as seperate words. "
    author: "Jon"
  - subject: "Re: Word wrap with emacs"
    date: 2006-09-11
    body: "Use word-search-backward and word-search-forward. It would be great if this were the default in text-modes or when auto-fill is enabled."
    author: "1052"
  - subject: "Re: Word wrap with emacs"
    date: 2007-01-29
    body: "You might also want to consider longlines-mode (in Emacs 22 or http://www.emacswiki.org/cgi-bin/emacs/download/longlines.el)"
    author: "JanR"
  - subject: "Re: Word wrap with emacs"
    date: 2007-03-12
    body: "I am still studying Emacs, but they say that \"C-s RET C-w\" is able to find phrases even if they are separated by newlines."
    author: "Alex"
  - subject: "Re: Word wrap"
    date: 2002-10-30
    body: "KDE's text editors all have word wrapping IIRC, so does GNOME 2.0's gedit. And just about any formatted text editor for Unix - KWord, StarOffice, OpenOffice, Mozilla Composer, etc. - has word wrapping."
    author: "Trevor"
  - subject: "Re: Word wrap"
    date: 2002-10-31
    body: "The talk is about *soft* word wrapping."
    author: "Anonymous"
  - subject: "Re: Word wrap"
    date: 2002-10-31
    body: "Well, I have been informed that the GNOMEs have fixed their soft word wrapping in their 2.x versions. (I don't really like the direction they've taken in the 2.x versions so I don't use it that much, if at all; too MacOS < 10 for me.) \n\nI still haven't had any RC1 users tell me if KDE editors will have this feature yet."
    author: "antiphon"
  - subject: "Will there be Debian packages?"
    date: 2002-10-30
    body: "Will there be Debian packages of the RC?  I'd like to test them, but I don't fancy uninstalling all my Debian stuff and compiling all the new stuff."
    author: "Plato"
  - subject: "Re: Will there be Debian packages?"
    date: 2002-11-04
    body: "http://shakti.ath.cx/debian\n\nBut I don't think they are official..."
    author: "Macolu"
  - subject: "ChangeLog"
    date: 2002-10-30
    body: "is there any place to find a list of fix being apply to 3.0.9 from 3.0.8 ?\n\njust curious....\n\nthanks\n\n"
    author: "somekool"
  - subject: "Anyone compiled it successfully in Redhat 8.0?"
    date: 2002-10-30
    body: "I got an errror message when I compiled the QT3.1.\nis there something I missed?\n\n/usr/bin/ld: cannot find -lXft\ncollect2: ld returned 1 exit status\nmake[1]: *** [../lib/libqt-mt.so.3.1.0] Error 1\nmake[1]: Leaving directory `/usr/local/qt-3.1/src'\nmake: *** [sub-src] Error 2"
    author: "ciicii"
  - subject: "Re: Anyone compiled it successfully in Redhat 8.0?"
    date: 2002-10-30
    body: "Try installing Xft-devel."
    author: "Haakon Nilsen"
  - subject: "Re: Anyone compiled it successfully in Redhat 8.0?"
    date: 2002-11-02
    body: "That doesn't work either.  I am having the same problem and I have Xft-devel installed.  I get this same error in Redhat 7.3 :-(.  Any help on this?\n\n"
    author: "Biz"
  - subject: "Re: Anyone compiled it successfully in Redhat 8.0?"
    date: 2002-11-04
    body: "ln -s /usr/X11R6/lib/libXft.so.1.2 /usr/X11R6/lib/libXft.so\n\nand then, in your qt-copy directory, after you make -f and configure, run:\n\nfind . -name Makefile | xargs perl -pi -e 's,-lXft,-lXft -lXft2,'\n\nThe linking might be optional, but I'm not about to rebuild qt to find out..."
    author: "Red Hat Lover"
  - subject: "thank you..."
    date: 2002-10-31
    body: "...guys for testing this non final version. i wait for debians official packages, but because of you, they will be rock stable!!! "
    author: "L1"
  - subject: "thank you..."
    date: 2002-10-31
    body: "...guys for testing this non final version. i wait for debians official packages, but because of you, they will be rock stable!!! "
    author: "L1"
  - subject: "panel"
    date: 2002-10-31
    body: "what i forgot: kde is really cool, but i hate one thing: this thousands of little arrows in panel. they are ugly and useless. i got rid of most of them in kcontrol, but this ones where you get this \"Move,Remove,Preferences,Panel\"-menu are still there. can i comment out some lines in sourcecode to remove them??? please, help ;-)"
    author: "L1"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "I absolutely agree with you there.  Those little arrows all over the place don't look good.\nI know that they are useful in terms of users being able to see that they can move stuff about  but they look silly.\n\nThere should be a control panel option -> Hide All Arrows."
    author: "Gibler"
  - subject: "usability at work"
    date: 2002-10-31
    body: "This is a trend with the usability people.  They seem to think more clutter is a good thing.  Same thing applies to repeating the same instructions and text all over the place.\n\nIt's *not* a good thing.  It's a careful balance."
    author: "ac"
  - subject: "Re: usability at work"
    date: 2002-10-31
    body: "actually, our real goal is to piss off people who post anonymously to web boards."
    author: "Aaron J. Seigo"
  - subject: "Re: usability at work"
    date: 2002-10-31
    body: "Apple, the usability gods, avoid clutter too.  \n\nOnce you learn something you don't need to read instructions over and over again.  \n\nIt eventually becomes counter-productive."
    author: "ac"
  - subject: "Re: usability at work"
    date: 2002-11-03
    body: "find, ignore my joke.\n\nsecond, Apple has shown with OS X that they are no longer \"usability gods\". they have some good ideas still (and occassionally great ones) but they are far more fallible than they ever have been.\n\nin any case, your \"once you learn something\" argument falls down at one particular point\" you have to learn it first. and that was the entire problem with removing panel applets: people never learned how to do it. they needed help learning (and remembering). they got that help. problem went away.\n\nnow we just get to put up with the occasional whine of \"i don't like how it looks\""
    author: "Aaron J. Seigo"
  - subject: "Re: usability at work"
    date: 2002-11-04
    body: "I wouldn't ignore those whines.  I am sure there is some fundamental UI rule that says clutter and redundancy is bad.  The whines are just one symptom of violating that rule."
    author: "ac"
  - subject: "Re: usability at work"
    date: 2002-10-31
    body: "Retard! Opening his stupid mouth and fear posting with his real name."
    author: "Timothy Seymour"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "this has been taken care of for a while...here's how to make them fade away.\n\nclick on one of those little arrows, choose \"Panel Menu > Configure Panel...\"\n\nclick on the \"Advanced Options\" button at the bottom.\n\ncheck the \"Fade out applet handles\" box and then click OK a couple of times.\n\neasy, huh?"
    author: "Elliott Martin"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "KDE 3.1 has an GUI option to fade out the applet handles. In KDE 3.0 you had to change kickerrc."
    author: "Anonymous"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "as others already mentioned, you can fade out the applets via the panel config dialog. hopefully that is satisfactory.\n\nas to why those arrows are there: there were endless reports of problems with removing applets from the panel. people just didn't clue in that you could right click on the applet handle! when the arrows were added, the number of such reports dropped practically to zero. they may annoy a few people, but they can be turned off and that is far better than a good % of users not being able to figure out how to remove applets."
    author: "Aaron J. Seigo"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "Ok, but check out bug #49969.\n\nAndras\n"
    author: "Andras Mantia"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "i just assigned it to myself. it's a bug in the control panel, not inherent to kicker. now the question is whether it will get fixed for 3.1, or 3.1.1 ... =)"
    author: "Aaron J. Seigo"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "it is possible to hide the applet-handles, yes, but, why do they still leave a space ?! that's really unfinished.. that should be simple accessed and work similar to WindowsXP's \"Fix Taskbar\", that is easy accessible by right-klick on the taskbar !!!  in KDE you need to open \"Kontrolcenter\", that's so heavy! really Big Work. hope things like that will be finally equal to \"MS Windows\", since, there is no copyright on that feature! and it is just logical to change that.\nthx"
    author: "mononoke"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "the patch for this logical change is available where?"
    author: "Aaron J. Seigo"
  - subject: "Re: panel"
    date: 2002-11-01
    body: "Do it yourself, it's opensource afterall. >:-p"
    author: "foo"
  - subject: "Re: panel"
    date: 2002-11-03
    body: "you've got it backwards: mononoke wants the feature, s/he should provide the patch. it isn't something high on my priority list (lots of other things are, though). the things that bother(ed) me the most about kicker have mostly been fixed, hopefully we'll get to the remaining annoyances for 3.2."
    author: "Aaron J. Seigo"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "With KDE 3.0.4 I can rightclick the panel and choose 'preferences'. Just make sure you click an empty region of the panel."
    author: "Matthijs Sypkens Smit"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "i don't know something about the XP way, but in my opinion. i like the applet-handles (especially with keramik), but not the arrows. so this fade out thing should only fade out the arrow, not the hole applet-handle. the empty area is better then a.-h. with arrow, but not perfect. and hey, to discuss about such stupid things shows the high quality of kde 3.x!!!"
    author: "L1"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "ok, thank you for all this informations!"
    author: "L1"
  - subject: "Re: panel"
    date: 2002-10-31
    body: "In ~/.kde/share/config/kickerrc\nin the [General] section\nput the following:\n\nFadeOutAppletHandles=true\n\nand the little vertical bars w/ the arrows will be faded in and out, so they're not visible unless you hover over them.\n\n-steve"
    author: "donuthole"
  - subject: "KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "Is there some explaination of why a tarball that won't compile was released?\n\nIs this  GCC or Qt issue?\n\nI am using GCC 3.2 and Qt 3.1 Beta 2 and I consistently get this error:\n\nkhtmlview.cpp: In member function `virtual void KHTMLToolTip::maybeTip(const QPoint&)':\nkhtmlview.cpp:247: no matching function for call to `KHTMLToolTip::tip(QRect&, QString&, QRect&)'\n\n????\n\n--\nJRT\n"
    author: "James Richard Tyrer"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "You need a newer Qt than beta2 - i.e. Qt-copy or a recent rsync snapshot for that function..."
    author: "Sad Eagle"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "This doesn't appear to be a very good idea to me -- releasing  a release candidate that requires an Alpha version of Qt.\n\nIn any case, I see nothing here:\n\nhttp://www.kde.org/info/3.1.html\n\nto indicate this problem.\n\nPerhaps at least a snapshot known to work would be a good idea.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "> releasing a release candidate that requires an Alpha version of Qt\n\nAlpha? Qt 3.1 already had a Beta2 release and its current state is RC1.\n\n>  Perhaps at least a snapshot known to work would be a good idea.\n\nLike ftp://ftp.kde.org/pub/kde/unstable/kde-3.1-rc1/src/qt-copy-3.1_20021024.tar.bz2?"
    author: "Anonymous"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: ">> releasing a release candidate that requires an Alpha version of Qt\n\n> Alpha? Qt 3.1 already had a Beta2 release and its current state is RC1.\n\nWe would all like to think that the development process results in software improving monotonically, but I think that we all know that this is not the case -- regressions DO occur.  There can be no assurance that the current snapshot is as stable as the last Beta.\n\nI checked the TrollTec web site about 10 minutes ago and couldn't find that RC1 to download. \n\n>> Perhaps at least a snapshot known to work would be a good idea.\n\n> Like: \n\n>ftp://ftp.kde.org/pub/kde/unstable/kde-3.1-rc1/src/qt-copy-3.1_20021024.tar.bz2?\n\nYes, too bad that that link isn't posted on the 3.1 info page with the 3.0.9 tarballs.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "Uhm ? Sorry, you like to try out KDE 3.1 RC1 (which is also more or less something to test) but you refuse to get and compile QT 3.1 RC1 ? What's wrong with you ?"
    author: "Martin Gacksche"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "> I checked the TrollTec web site about 10 minutes ago and couldn't find that RC1 to download. \n\nTrollTech doesn't offer release candidates to the public, but makes them available to their friends at KDE. Get current qt-copy and you have Qt Free Edition 3.1 RC2.\n\n> Yes, too bad that that link isn't posted on the 3.1 info page with the 3.0.9 tarballs.\n \nYes, that could be improved. But this is no normal release, I was even surprised to see the 1 week living release candidate announced so public on the dot."
    author: "Anonymous"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-03
    body: "Well, latest RC2 seems to have stabilized my combination of KDE-3.1pre, Qt-3.1pre,Xfree, Freetype. Compilation on a gcc3.2+glibc2.3.1 system was without problems. Now it looks better. Hey, I can even launch kcalc again! :-))\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KdeLibs WON'T Compile"
    date: 2002-11-01
    body: "Actually, that's a pretty good idea, since Qt 3.1 beta 2 had some serious bugs for KDE.  I don't think you would be able to do any meaningful testing for the RC since there are some more visible bugs in that Qt version. As for this method the error was about: IIRC, that was added by request of KHTML hackers, so fix some bugs with tooltips in webpages. \n\nAnd, BTW, qt-copy is generally what is known to work with KDE well -- since if anyting is broken, patches are typically applied to fix it. And this is why, in fact, Qt snapshots are used like this -- so Qt bugs affecting KDE, if any, can be found and fixed before a Qt release happens. "
    author: "Sad Eagle"
  - subject: "More on icons"
    date: 2002-11-02
    body: "After compiling and installing: kdeartwork, I found that I still had icon trouble.\n\nDid anyone consider backward compatibility??\n\nIt seems that: \"hicolor\" is now called: \"kdeclasic\" and \"lowcolor\" is now called \"Lowcolor\".\n\nSo KDE can't find my icons. :-(\n\nSo, I know, just rename the directories.  I already did that.\n\nBUT, applications that install icons in a directory other than the application's are going to be looking for: \"hicolor\" & \"locolor\".  So, links are necessary.  But, not only for the global directory, but for all of the user: $HOME/.kde/share/icons/\" directories as well.\n\nMy suggestion is to change it back immediately before the problem spreads.\n\n--\nJRT "
    author: "James Richard Tyrer"
  - subject: "Re: More on icons"
    date: 2002-11-02
    body: "Some icon problems have been addressed after RC1, for rest http://devel-home.kde.org/~larrosa/iconthemes.html may help understanding."
    author: "Anonymous"
  - subject: "Re: More on icons"
    date: 2002-11-02
    body: "OK.  So, the global icons are not a problem.\n\nHOWEVER, there is a serious bug here.\n\nThe icon loader did NOT find: \"#HOME/.kde/share/icons/hicolor/\" which is were OpenOffice (for one) installs its icons, and I presume that any locally installed application should install its icons.\n\nHas this been fixed?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: More on icons"
    date: 2002-11-03
    body: "OpenOffice icons display fine here with CVS and either Crystal or KDE Classic themes."
    author: "Anonymous"
  - subject: "wheeeeeeeeeee!"
    date: 2002-11-03
    body: "most annoying bug of kde is fixed! now you can wheelscroll in konquer without having to worry about comboboxes on pages!! It was really annoying on sites like slashdot."
    author: "montz"
  - subject: "Re: wheeeeeeeeeee!"
    date: 2002-11-03
    body: "I liked this *feature*. Agreed, implementation was bad. It should have better be changed to work only when one presses e.g. Ctrl instead of turning it off completely."
    author: "Anonymous"
  - subject: "what about \"Second\"?"
    date: 2002-11-03
    body: "Slowness is mentioned in the article, and I do notice it while typing some input in konsole, or in any HTML formular field like this one or at google.com.\nCompare typing a line in konsole to typing a line in xterm and a line in the konqueror addressfield, then delete these inputs via backspace - there're lags and delay, but only in konsole.\n\nDoes anyone noticed this either?\nI didn't have these problems in older versions of KDE, not even in Beta 2.\nAlso if you uninstall klipper, it doesn't go away. \nMaybe a bug?\n\n"
    author: "Joerg de la Haye"
  - subject: "Re: what about \"Second\"?"
    date: 2002-11-03
    body: "Try unsetting LC_ALL. If anyone has a hint how to solve this problem, please add your solution to http://bugs.kde.org/show_bug.cgi?id=49837."
    author: "Anonymous"
  - subject: "Re: what about \"Second\"?"
    date: 2002-11-05
    body: "That's now fixed in Qt 3.1 final and the qt-copy distributed with KDE 3.1 RC2."
    author: "Anonymous"
  - subject: "Runs fine on OpenBSD"
    date: 2002-11-03
    body: "just finished compiling it on my OpenBSD/macppc machine.\nruns smothly on a 500mhz G4.\nklipper tends to crash from time to time though, i recon it will be fixed \nin due time to the release aye?. "
    author: "Jimmy Mcnamara"
  - subject: "One thing I love about Nautilus"
    date: 2002-11-04
    body: "are the smooth, rounded edges around the icon name (when the icon has been clicked once to highlight it).  In KDE, a highlighted icon has a plain box around the icon name.  Even worse, the icon name isn't even centered properly in the box.  Any chance of this getting changed in KDE 3.1? :)"
    author: "Rimmer"
---
The <a href="http://www.kde.org/">KDE Project</a> yesterday
<a href="http://www.kde.org/info/3.1.html">announced</a> the release
of KDE 3.1 RC 1.  This release, while important, will have but a short lifespan (RC 2 is scheduled for next Monday), and so binary packages are not planned.
A couple of points to consider:  First,
if you are wed to the hicolor icons, please note that they have been moved
to the kdeartwork package; the other packages ship only with the new modern
and attractive Crystal-SVG icon theme.
Second, Klipper users who experience slowness or possible crashes in Konsole
or KMail with this release should try disabling the Klipper syncing options,
and then check
the <a href="http://www.kde.org/info/3.1.html">KDE 3.1 Info Page</a>
about reporting results.  Please give this release a thorough testing
so KDE 3.1 will be good and ready on schedule!    A short but informative
<a href="http://promo.kde.org/newsforge/kde-3.1.html">preview</a> of
the much-improved KDE 3.1 is available on the <a href="http://promo.kde.org/">KDE Promo</a> site.





<!--break-->
