---
title: "LinuxTag 2002: KDE on Apple iBook and Sun UltraSPARC"
date:    2002-06-05
authors:
  - "kpfeifle"
slug:    linuxtag-2002-kde-apple-ibook-and-sun-ultrasparc
comments:
  - subject: "iBook G4 ??"
    date: 2002-06-05
    body: "Is it a beta device or something? Coz there's not G4-based iBook! It is either G3 iBook or G4 PowerBook"
    author: "KAtiOS"
  - subject: "Re: iBook G4 ??"
    date: 2002-06-05
    body: "Hmmm, that's how Kurt wrote it.  \n\nI bet the main difference between an iBook and a PowerBook is that they're different colours. Either that, or one of them is transparent. :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: iBook G4 ??"
    date: 2002-06-05
    body: "Sorry --\n\nI guess PowerBook would have been right....\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "GCC or Sun Fort"
    date: 2002-06-05
    body: "Has KDE on Solaris been compiled with GCC or the Sun Fort CC compiler?"
    author: "Loranga"
  - subject: "cups via fink??"
    date: 2002-06-05
    body: "If he (they?) used fink to install KDE on the iBook, how did they get cups support?  I'm *really* missing native cups support for KDE, as I could probably get my wife to switch over to KOffice, if it had it.  Then I could drop the *coughwearingmyeyepatchcough* version of M$ Office I have on it now B-)"
    author: "David Bishop"
  - subject: "Re: cups via fink??"
    date: 2002-06-05
    body: "Presently KDE from Fink is not compiled against libcups. I only discovered later. After I wrote to the Fink  people, they were very responsive and hope to offer a CUPS-enabling update soon.\n\nHowever, I compiled CUPS on the Mac OS x and it prints just fine from the commandline and also offers its printers to other KDE clients. You just can't use \"kprinter\" locally with CUPS...\n\nI hope to get an update during LinuxTag from the Fink guys...\n\nCheers,\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: cups via fink??"
    date: 2002-06-06
    body: "I'm not sure who from fink is gonna be at linuxtag, but I'm beta-ing new kde packages now, and hope to release new binaries tomorrow.  The new ones all have --enable-final working (and so are about 1/4 the size of the old) and also have cups enabled now that there's a supported cups fink package.\n"
    author: "Ranger Rick"
  - subject: "Re: cups via fink??"
    date: 2002-06-10
    body: "We used gcc-3.1 on Solaris8. I am in the process of uploading pkg and tar.gz files to download.kde.org, they'll be publicly available somewhen tomorrow.\n\nIf SUN sponsors hardware and a compiler license to me, I'd also try Forte packages and/or gcc 64bit packages...\n\nGreetings,\neva"
    author: "eva"
  - subject: "kde 3.0.1 and Solaris 8"
    date: 2002-06-07
    body: "When will\\should we expect the KDE3.0.1 install suite for Solaris 8?\n\n"
    author: "richard"
  - subject: "Re: kde 3.0.1 and Solaris 8"
    date: 2002-06-10
    body: "If everything works fine: tomorrow :-)\n\neva"
    author: "eva"
  - subject: "Compiler Notes for Solaris8"
    date: 2002-06-10
    body: "My reply to the compiler question didn't work, so here you get the answer in a new thread:\n\nWe used gcc-3.1 on Solaris8. I am in the process of uploading pkg and tar.gz files to download.kde.org, they'll be publicly available somewhen tomorrow.\n\nIf SUN sponsors hardware and a compiler license to me, I'd also try Forte packages and/or gcc 64bit packages...\n\nGreetings,\neva"
    author: "eva"
---
UltraSPARC 60 with Sun Solaris and Apple iBook G4 PowerPC with Mac OS X -- these are amongst the diverse hardware and OS platforms on which KDE will be presenting its latest achievements to the world <a href="http://dot.kde.org/1022367370/">this week in Karlsruhe</a>, Germany. Both installations will be running <A HREF="http://www.cups.org/">CUPS</A>, to demonstrate how <a href="http://printing.kde.org/">KDE Print</a> can provide a great environment for an enterprise printing solution across different platforms.
<!--break-->
<p>The KDE Team will be presenting these two ports of its latest release at <A HREF="http://www.linuxtag.org/">LinuxTag 2002</A> for the first time to a large public audience:</p>
<ul>  <li><b>KDE main booth</b>: KDE 3.0.1, freshly compiled, and running on a Sun UltraSPARC 60 (Graciously loaned by Sun Microsystems, Inc.)</li>

  <li><b>KDE Print, LinuxPrinting.org and Danka partner booth</b>: <a href="http://dot.kde.org/1022869694/">KDE 3.0.1 ported</a> by the <a href="http://fink.sourceforge.net/">Fink team</a> and running on an Apple iBook G4 with Mac OS X (Sponsored by Apple Inc.)</li>

</ul><p>Moreover, both systems will be loaded with a <a href="http://www.cups.org/">CUPS</a> installation, to show the new interoperationality with <a href="http://www.samba.org/">Samba</a>, including printer driver download via "point and print".</p>

<p>The KDE Team is proud of these achievements. These presentations provide tangible proof that KDE is not a "Linux-only" desktop environment, but remains one for the whole realm of Unix...</p>
<i>Kurt Pfeifle,<br> 
KDEPrint Team</i>