---
title: "KDE Reports from LinuxTag 2002"
date:    2002-06-15
authors:
  - "rmolkentin"
slug:    kde-reports-linuxtag-2002
comments:
  - subject: "Thanks for the report"
    date: 2002-06-14
    body: "Good to hear how well things go over there in Europe. Thanks for the report!\n\nRob: Those aren't tigers. They are leopards. Come over here and you can see them in the wild. ;-) Elephants and giraffes, too.\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Thanks for the report"
    date: 2002-06-15
    body: "And that cute thing with the black eye patches is a meerkat, isn't it? (I know it's one of the O'Reilly animals.)\n\nUwe probably knows...."
    author: "Otter"
  - subject: "Re: Thanks for the report"
    date: 2002-06-17
    body: "Okay, fixed both the tiger and meerkat annotations. If anyone has more anecdotes that match the pictures, let me know!"
    author: "Rob Kaper"
  - subject: "More photos..."
    date: 2002-06-14
    body: "... are linked at <A href=\"http://media.linuxtag.org/\">media.linuxtag.org</a> . I hope someone has a high detail photo of all developers grouped?"
    author: "Anonymous"
  - subject: "Outlook trap?"
    date: 2002-06-15
    body: "\"Martin Konold's speech \"Escaping from the Outlook trap\" contained some very interesting plans for the future of personal information management with Linux/KDE targeting both server and client side.\"\n\n\nI'd be interested to know more about this... any transcripts?"
    author: "Mike Hearn"
  - subject: "Re: Outlook trap?"
    date: 2002-06-26
    body: "http://www.erfrakon.de/vortrag/LinuxTag2002/index.html\n\n(German only, sorry)"
    author: "Daniel Molkentin"
  - subject: "Grilled Tux"
    date: 2002-06-17
    body: "Where can i find this grilled Tux picture which was part of the keynote ?\nI can't remember the URL :("
    author: "Martin"
  - subject: "Re: Grilled Tux"
    date: 2002-07-08
    body: "It was created by Agnieszka Czajkowska (www.imagegalaxy.de), but I don't think you can find it there. But of course all images are contained in the keynote presentation available at  http://www.klaralvdalens-datakonsult.se/Public/publications/keynote-linuxtag-2002.html."
    author: "Anonymous"
---
<i>A full report by Daniel Molkentin follows.</i> LinuxTag 2002 is over, but the memories will remain: about 40 KDE developers
gathered in Karlsruhe, Germany to represent the project and to socialize
with each other. Several developers have placed their pictures of the event
on-line. Take a look at <a href="http://konsole.kde.org/LinuxTag2002/">72
pictures by Stephan Binner</a> (<a
href="http://capsi.com/~cap/konsole.kde.org/LinuxTag2002/">mirror</a>), <a
href="http://www.cip.biologie.uni-osnabrueck.de/niehaus/fotos/lt2002/images.html">
81 pictures by Carsten Niehaus</a> or <a
href="http://capzilla.net/photobook/2002-06-05-karlsruhe/">156 pictures by
Rob Kaper</a>, including a lengthy report on the booth gatherings and social
events. Have fun.. we did!
<!--break-->
<h3>Report: KDE at LinuxTag 2002</h3>
by <a href="mailto:molkentin@kde.org">Daniel Molkentin</a>
<p>June, 9th (Karlsruhe, Germany). This year's <a href="http://www.linuxtag.org">LinuxTag</a> was once again a successful gathering for over <a href="http://capzilla.net/img/photobook/2002-06-05-karlsruhe/karlsruhe-092.jpg">sixty</a> KDE developers from all over Europe who presented their <a href="http://www.kde.org/awards.html">award-winning</a> desktop environment to more than 13,000 interested visitors at "Europe's largest OpenSource Event". As a special guest, David Faure (of Konqueror and KOffice fame) attended the fair to share opinions with
other KDE developers.</p>
<p>On six dedicated demopoints, the LinuxTag visitors were invited to learn about the current <a href="http://www.kde.org/info/3.0.1.html"
>KDE 3.0.1 release</a>, Personal Information Management (<a href="http://pim.kde.org">KDE Pim</a>), the <a href="http://edu.kde.org">KDE Edutainment project</a>, the award-winning <a href="http://www.kdevelop.org">KDevelop</a> IDE as well the <a href="http://developer.kde.org
/development-versions/kde-3.1-features.html">future of KDE</a>. The <a href="http://opie.handhelds.org">Opie Team</a> showed their impressive achievements on syncing KDE with their amazing handheld environment. </p>

<p>In addition to the demopoints, the booth provided a hacking area where KDE Developers fixed bugs and implemented new features suggested
 by the visitors. Powered by an ad-hoc <a href="http://www.trolltech.com/products/teambuilder/">Teambuilder</a> farm, the developers could
 easily recompile even larger chunks of code in almost no time.</p>

<p>In direct neighbourhood of our booth, KDE's Kurt Pfeifle was among the staff of the Linux Printing booth. He presented <a href="http://
printing.kde.org/">KDEPrint</a> using an Apple Powerbook running KDE on top of MacOS X. By employing CUPS and the Internet Printing Protocol, Kurt showed the ease of Linux printing to an astonished crowd.</p>

<p>At the booth of the <a href="http://www.bsi.bund.de/">Bundesamt f&uuml;r Sicherheit in der Informationstechnik (BSI)</a>, the KMail developers Marc Mutz and Ingo Kl&ouml;cker and the employees of <a href="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens Datakonsult</a>, <a href="http://www.intevation.net">Intevation</a> and <a href="http://www.g10code.de">g10 code</a> showed the latest version of
 <a href="http://kmail.kde.org/">KMail</a> with S/MIME support and the overall improved encryption features. On that occasion, Marc Mutz initiated a key-signing session where members of almost all present open source projects took the opportunity to extend the <a href="http://wwwkeys.pgp.net">web of trust</a>.</p>

<p>Among the interested vistors was Brigitte Zypries, secretary of state who was especially interested in the KDE EDU Project. As a surprise, a team from the german TV station 3Sat came to portrait the Opie Project. It will be aired on Monday, Juni 17th in the TV show <a href="http://www.3sat.de/neues/">"Neues"</a>.</p>

<p>In the free conference, Ralf Nolden's talk about "the enterprise desktop for the home user", Kurt Pfeifle's speech about "modern printing for a KEnterprise Desktop Environment" and Karl-Heinz Zimmer's talk about the Aegypten Project earned a vast amount of interest. Developers had the opportunity to attend the speeches of Jesper Pedersen about Qt Designer and Ralf Nolden who presented "Developing with <a href="http://www.kdevelop.org">KDevelop</a>" in 3 different flavors: "beginner", "advanced" and "embedded". Martin Konold's speech "Escaping
from the Outlook trap" contained some very interesting plans for the future of personal information management with Linux/KDE targeting both server and client side.</p>

<p>A large amount of photos have been taken by various developers:

<ul>
<li><a href="http://konsole.kde.org/LinuxTag2002/">72 photos by Stephan Binner</a> (<a href="http://capsi.com/~cap/konsole.kde.org/LinuxTag2002/">mirror</a>)</li>
<li><a href="http://www.cip.biologie.uni-osnabrueck.de/niehaus/fotos/lt2002/images.html">90 photos by Carsten Niehaus</a></li>
<li><a href="http://capzilla.net/photobook/2002-06-05-karlsruhe/">156 photos by Rob Kaper</a></li>
<li><a href="http://arachni.kiwi.uni-hamburg.de/~harlekin/lt2002/images.html">Even more photos by Maximilian Rei&szlig;</a></li>
</ul>
Lots of fun and opportunities to meet and win new friends have been taken on- and offside the fairground guaranteeing the KDE project a healthy social basis for further successful cooperation and  development.
</p>
<p>Last but not least, the KDE LinuxTag team would like to thank the following companies for their support:
<ul>
<li><a href="http://www.danka.de">Danka Deutschland</a> for the TFT, several useful equipment and for printing the business cards</li>
<li><a href="http://www.hp.com">Hewlett-Packard</a> and <a href="http://www.sharp.de">Sharp</a> for equipping the Opie team with their handhelds</li>
<li><a href="http://www.kde.org/kde-ev/">KDE e.V.</a> and its donators, for the budget we've been given to finance the fair</li>
<li><a href="http://www.kernelconcepts.de">KernelConcepts</a> for producing wonderful KDE Pins (soon available <a href="http://www.kernelconcepts.de/products/tuxlisteneu.shtml">online</a>) and donating parts of the profit to the KDE project.</li>
<li><a href="http://www.klaralvdalens-datakonsult.se">Klar&auml;lvdalens Datakonsult AB</a> for sponsoring 10 tickets for the social event
</li>
<li><a href="http://www.linuxnewmedia.de">Linux New Media</a> for donating 30 <a href="http://www.linux-user.de/Info/kde3.html">DELUG CDs</a> containing KDE 3.0 packages to sell as merchandising material.</li>
<li><a href="http://www.marktundtechnik.de">Markt + Technik</a> for sponsoring 1 social event ticket</a>.
<li><a href="http://www.suse.com">SuSE</a> for 2 Demopoints, TFT Displays and 2 social event tickets</li>
<li><a href="http://www.sun.com">SUN Microsystems</a> for making available an UltraSparc 60 running Solaris and sponsoring 5 tickets for the social event</li>
<li><a href="http://www.trolltech.com">Trolltech</a> for the TeamBuilder copy used in the Hacking Area</li>
</ul>
</p>

<p>The KDE Team is looking forward to LinuxTag 2003 hoping it will be as successful and nice as all LinuxTag events have been up to now. Thanks again to all the sponsors and visitors. See you in 2003!</p>