---
title: "People of KDE: Kristof Borrey"
date:    2002-04-01
authors:
  - "wbastian"
slug:    people-kde-kristof-borrey
comments:
  - subject: "iKons"
    date: 2002-04-01
    body: "Thank you for iKons.  Is iKons in KDE3?  It would be a good idea because it appeals to GNOME fans who think KDE has ugly icons.\n"
    author: "KDE User"
  - subject: "Re: iKons"
    date: 2002-04-01
    body: "Yes, it's in kdeartwork package."
    author: "Anonymous"
  - subject: "Re: iKons"
    date: 2002-04-05
    body: "It also appeals to KDE fans who think GNOME has ugly icons. :\u00ac)"
    author: "Malcolm Hunter"
  - subject: "okay, I give up..."
    date: 2002-04-01
    body: "what in the *heck* is a \"pyama\"?  Some weird, European-bastardization of pyjama? Enquiring usian minds want to know :-)"
    author: "David Bishop"
  - subject: "Re: okay, I give up..."
    date: 2002-04-02
    body: "Blame Tink :-)\n\nMaybe a typo (or a copy & paste bug? :-): \"Limwire\" instead of \"Limewire\""
    author: "Andy Goossens"
  - subject: "Sus&Wis"
    date: 2002-04-02
    body: "Willy and Wanda? Please give me \"Sus & Wis\" instead :-) Love that comic, but I've grown up :-("
    author: "Andy Goossens"
  - subject: "Re: Sus&Wis"
    date: 2002-04-04
    body: "\"from the tante-sidonia dept.\" :-)"
    author: "Andy Goossens"
  - subject: "Monopoly"
    date: 2002-04-02
    body: "People who like both KDE and Monopoly, like Kristof, will be pleased to know <a href=\"http://capsi.com/atlantik/\">Atlantik</a> will be in kdegames for KDE 3.1."
    author: "Rob Kaper"
---
In this week's episode of 
<a href="http://www.kde.org/people/people.html">The People Behind KDE</a>, we travel to 
<a href="http://www.leopoldsburg.yucom.be/">Leopoldsburg</a> in
<a href="http://belgium.fgov.be/">La Belgique</a> where we meet
<a href="mailto:kborrey@skynet.be">Kristof Borrey</a>. 
Although unknown to many, Kristof is an exceptional talented graphic artist and is author of the <a href="http://users.skynet.be/bk369046/icon.htm">iKons</a> icon theme. When Kristof doesn't spend his time drawing, he works on his 
<a href="http://users.skynet.be/bk369046/ebusiness.htm">thesis</a> about
the use of Linux in E-business. Read more about Kristof
<a href="http://www.kde.org/people/kristof.html">here</a>.

<!--break-->
