---
title: "The KDE Cafe Opens Doors"
date:    2002-03-23
authors:
  - "numanee"
slug:    kde-cafe-opens-doors
comments:
  - subject: "irc"
    date: 2002-03-23
    body: "We certainly can have a #kde-cafe on irc too. This topic thing on #kde, which pretends that the channel is for development discussion, limits (sometimes) free discussion"
    author: "Philippe Fremy"
  - subject: "Re: irc"
    date: 2002-03-23
    body: "We do have one.. #kde-users"
    author: "Neil Stevens"
  - subject: "Re: irc"
    date: 2002-03-26
    body: "AFAIK #kde-users is for users asking questions and not for chitchat. A #kde-cafe channel doesn't sound like a bad idea."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: irc"
    date: 2002-03-26
    body: "Needless to say, the channels is already created :-)\n\nConnect to your favorite OPN server (find one on http://www.openprojects.net or just use irc.openprojects.net) and come /join #kde-cafe!"
    author: "Erik Hensema"
  - subject: "was happy and now unhappy."
    date: 2002-03-23
    body: "KDE-Cafe cool ! i was so happy to see that news. but i get so disappointed to learn that KDE-Cafe is a mailling list.\n\nwe need an other news site. only one isnt enough for our big KDE project and all of us who addictly and unconditionally love KDE.\n\nI need more news, everyday.\n\ni need to parse avec news site to see if someone talk about KDE.\n\ni want to know developpement evolution.\n\ni want more of KDE in my life.\n\n"
    author: "Somekool"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "sorry\n\ns/avec/every\n\n"
    author: "Somekool"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "FreeKDE.org will return soon, so you'll have that extra news site."
    author: "Rob Kaper"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "you sure ? \ni think Neil still searching for a place to host where he could have a root login.\n\n"
    author: "Somekool"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "If all goes smoothly freekde will be up in 2-4 weeks.\n\nOf course, any number of things could go wrong... :-)"
    author: "Neil Stevens"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-24
    body: "Well I am sure slashdot will be happy to publish any of Neil's rants about decisions made by KDE coders.   "
    author: "Prankster"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-25
    body: "Yeah, they'd post the Opinions section, but they won't post Helpcenter, News, KDE Universe sections.\n\nSo I still need freekde back up."
    author: "Neil Stevens"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "Sorry, I can see how I built up your expectations and then killed it.  I will update the article to make it clearer that we are talking about a mailing-list.\n\nWe definitely need more news here on the dot though.  There are other sites like the upcoming freekde.org, gui-lords.org and pclinuxonline.com which often have KDE related stuff.\n\n-N.\n\n"
    author: "Navindra Umanee"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "\"Sometime over the next few days, pclinuxonline and gui-lords are going to merge websites and host the site on the eastwind server.\""
    author: "Anonymous"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "yeah ! as you said dot.kde.org need more news. and i will extand that by saying that it also need to be improoved.\n\ni suggest that dotsy include a \"Ask dotsy\" just like Slashdot have.\n\nuser could then ask their questions about the kde project. for example i would like to know whats a kde release coordinator... i can imagine some of their jobs, but im not sure at all. if someone could answer whats is it, it could be interresting. I imagine someone saying what is needed to do, whats they do, and past and current release coordinator could add their comment about their feeling and experience. \n\nanother suggestion is a cvs history/commit parser.\na news section could be cvs news.... about which package change on theese day.\nfor people running cvs version,... its interresting to know whats its updated and when ;)\n\nlast suggestion... i wonder why dot.kde.org dont have a poll yet.\n\nthanks.\n"
    author: "Somekool"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-23
    body: "Oh ! i forget one ....\n\nif someone have time to :\n     browse news site, like linuxtoday, linux.com, gui-lords, slashdot,\n     zdnet, etc. to find out if someone talk about KDE.\nit would be very cool if this work could be done by someone.\n\nthe dot need more news, and i think its the job to dot.kde.org to point us to the news.\n\nand please.... dot.kde.org reader .... post what you see, what you hear, what you think.\n\nsee ya \n\n"
    author: "Somekool"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-24
    body: "Check http://www.kde-look.org/ also.\n"
    author: "ac"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-25
    body: "I agree. Mailing lists sucks. \n\nUse the web so *I* can choose when I have time to read it or not! \n\nIt would also be interesting to try a good (streaming) chat system for this \"Cafe\". Maybe there will be a lot of discussion, maybe there won't but you don't know until you've tried. IRC is a bit too geeky, invites hoards of script-kiddies to break into you computer and can't be used from everywhere..."
    author: "Johan"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-26
    body: "> Use the web so *I* can choose when I have time to read it or not! \n\nEver heard of mailing list archives? http://lists.kde.org/?l=kde-cafe&r=1&w=2\n\n> IRC is a bit too geeky, invites hoards of script-kiddies to break into you computer and can't be used from everywhere...\n\nFUD"
    author: "Anonymous"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-26
    body: "kde-cafe is also available via the news feed (news.uslinuxtraining.com, read-only). So you don't have to subscribe to the mailing list if you just want to read it from time to time."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: was happy and now unhappy."
    date: 2002-03-26
    body: "I'm the admin of news.nl.linux.org, I can create one or more KDE newsgroups without trouble. It'll even be propagated to other servers (though not many have the nlo.* hierarchy yet).\n\nThe server is readonly, but you can get an account for free. Visit http://news.nl.linux.org for details\n\n</spam>"
    author: "Erik Hensema"
  - subject: "Qt runtime error"
    date: 2002-03-23
    body: "Hi. I compiled the recent rc from kde 3, but when it tries to run\nkicker it marks an assert error on file qgarray.cpp. I even tried to\nfollow the compiling advice in README.qt from the qt source (the snapshot\nthat came in the release). Could anyone help me. My distro is\nSlackware 8.0 with gcc-2.95 and glibc-2.1.5. Thanks.\n"
    author: "Jes\u00fas Antonio S\u00e1nchez A."
  - subject: "Re: Qt runtime error"
    date: 2002-03-24
    body: "Hmm, did you get this error befor or after you subscribed to the KDE Cafe? ;-)\n\nOk seriously, this question is way off topic over here. The best place to ask this is in te right mailinglist kde-linux@mail.kde.org to subscribe, sent a mail with subscribe as subject to kde-linux-request@mail.kde.org\n\nKind regards, Rinse"
    author: "rinse"
  - subject: "STL"
    date: 2002-03-24
    body: "STL is not evil!, it is my friend, :)"
    author: "Jes\u00fas Antonio S\u00e1nchez A."
  - subject: "Re: STL"
    date: 2002-03-24
    body: "The STL is merely a neccessary evil."
    author: "Carbon"
  - subject: "Re: STL"
    date: 2002-03-25
    body: "Necessary. And with Qt, not even that."
    author: "Rob Kaper"
  - subject: "IRC channel or online chat? Or both?"
    date: 2002-03-26
    body: "IRC channels are unreachable by those of us with a firewall. My systems admin won't open up the IRC ports, so I would only be able to use a web chat. I could design a very simple IRC-Web bridge that would allow users on the web to chat with the IRCers. For the chat program a simple perl chat program that has a flat file chat log would work for that.."
    author: "Aaron Krill"
---
Lo and behold <a href="mailto:tink@kde.org">Tink</a>'s latest community undertaking, the <a href="http://mail.kde.org/mailman/listinfo/kde-cafe">KDE Cafe</a> mailing-list.  In her own words: <i>"KDE-Cafe is the virtual chill-out zone of KDE, a cross between Slashdot and IRC, a cross between the local pub and the opinions page of your newspaper. Specially designed for KDE addicts like you."</i>  So there.  More details ahead.
<!--break-->
<br><br>
<hr>
<br><br>
<blockquote>
<pre>
<b>Date:</b>    Mon, 11 Mar 2002 13:59:02 -0700
<b>To:</b>      kde@kde.org
<b>From:</b>    Tink &lt;<a href="mailto:tink@kde.org">tink@kde.org</a>&gt;
<b>Subject:</b> KDE-cafe
</pre>
<p>
With great pride we present to you the "KDE-cafe".
</p>
<p>
KDE-cafe is the virtual chill-out zone of KDE, a cross between
<a href="http://slashdot.org/">Slashdot</a> and IRC, a cross between the local pub and the opinions page
of your newspaper. Specially designed for KDE addicts like you.
</p>
<p>
You know those endless discussions about <a href="http://www.derkarl.org/why_to_tabs.html">Tabs vs. Spaces</a>? Which
<a href="http://kate.kde.org/">editor</a> works best? Why STL is evil? Whether Microsoft should be
broken up in 10000 pieces or Bill Gates should become president of
the U.N.? These discussions have finally found a place of their
own on KDE's latest mailing-list: KDE-cafe. There is one rule: don't
hit each other with the chairs, play it fair.
</p>
<p>
Of course, KDE-cafe is also a great place for sharing your latest
vacation pictures with your KDE friends or posting interesting links
to stories about KDE, life or the universe.
</p>
<p>
Behind the tap of the KDE-cafe you will find nobody less than our own
Tink, famous for <a href="http://www.kde.org/people/people.html">"The People Behind KDE"</a> series.
</p>
<p>
We hope to see you soon at <A href="mailto:kde-cafe@kde.org">KDE-cafe@kde.org</a>. Simply mail <a href="mailto:kde-cafe-request@kde.org">kde-cafe-request@kde.org</a> with "subscribe" in the subject to join.
</p>
</blockquote>
<p>
<i>Or else try the newfangled <a href="http://mail.kde.org/mailman/listinfo/kde-cafe/">web interface</a>.</i>
</p>