---
title: "KDE Dot News: We're Back In Business"
date:    2002-03-29
authors:
  - "numanee"
slug:    kde-dot-news-were-back-business
comments:
  - subject: "Oh, Ghod... I *have* to do it..."
    date: 2002-03-29
    body: "\nFirst Post!\n\n(I hope that my many quality posts in the past will be taken into account - this is just a joke, you may now return to real discussion.)  >;)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Oh, Ghod... I *have* to do it..."
    date: 2002-03-29
    body: "*LOL*  Good one...\n\n\"Ghod\" huh?  Are you one of those alt.sysadmin.recovery's?\n\n"
    author: "ac"
  - subject: "Re: Oh, Ghod... I *have* to do it..."
    date: 2002-03-29
    body: "\n   It's a term from Fandom.  It's an all purpose, gender neutral, religion agnostic, non-blasphemous term.  Many of the terms in use on the internet that date way back come from Fandom, particularly those on usenet.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Yeah, it's up again..."
    date: 2002-03-29
    body: "But we stll don't have HTML commenting :-)"
    author: "Carbon"
  - subject: "Hey ..."
    date: 2002-03-29
    body: "It's better and faster than ever before!\n\n(Well, someone had to post it ...)\n\ntom"
    author: "tom"
  - subject: "Dot problems"
    date: 2002-03-29
    body: "Why do you use Zope? I guess that wasn't the first time you had problems."
    author: "Anonymus"
  - subject: "Re: Dot problems"
    date: 2002-03-29
    body: "Yep, but not Zope based.\n\nSee: http://dot.kde.org/1016696443/1016838575/1016906619/1016929201/"
    author: "BugPowder"
  - subject: "Re: Dot problems"
    date: 2002-03-29
    body: "If you had used Zope/Python maybe you wouldn't ask that question. :-)\n\nIt's really a quite neat concept (with implementation to back it), check it out.\n"
    author: "Navindra Umanee"
  - subject: "troy@kde.org"
    date: 2002-03-29
    body: "/me waits for the dot to get slashdotted when slashdot posts a link to the dot.\n\noh the joys of having a desktop used by millions... :)\n\nTroy\ntroy(at)kde(dot)org"
    author: "Troy Unrau"
  - subject: "Re: troy@kde.org"
    date: 2002-03-29
    body: "\nIt's happened.  Several times. It's not pretty.\n\nMaybe a static page at http://dot.kde.org/ when Slashdot hits, containing all the actual info that 99% of the visitors are looking for, and then a link \"This way to the dot\" might help.  Of course, that implies that someone with full access gets there within a reasonable amount of time after /. posts the link.\n\nQuite often, people just follow the link to read a static press release or something.  Some thought might be given to this in anticipation of 3.0.  As much as I hate a splash page to a site, a static splash with information shielding the dot itself for people who are interested in more detailed information would probably be a good idea.  Traffic's gonna get hard-core heavy shortly.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: troy@kde.org"
    date: 2002-03-29
    body: "Evan: Maybe a static page at http://dot.kde.org/ when Slashdot hits\n\nMaybe making the home page \"dynamically static\" for good? Then you don't have to wait for Slashdot to make an \"emergency procedure\". :-)\n\nWhat I mean is, every request to the home page returns a static document, that is updated in the server when something changes (a new story, an increse in the number of comments in some story, etc)."
    author: "Marcelo"
  - subject: "Re: troy@kde.org"
    date: 2002-03-29
    body: "Zope *has* stood up to Slashdotting on its own in the best of times, this despite the alledged inefficiencies of Squishdot on top of it. :-)\n\nBtw, isn't it already \"dynamically static\" right now?  I believe it is, and I'm banking on this helping with future /.'ing and the like.\n\nHmmm there seems to be something wrong with cookies, my email address keeps getting dropped.\n"
    author: "Navindra Umanee"
  - subject: "Re: troy@kde.org"
    date: 2002-03-30
    body: "Navindra: Btw, isn't it already \"dynamically static\" right now? I believe it is\n\nWell, you seem to be the administrator (or are you?), not I, so you should know better. :-)"
    author: "Marcelo"
  - subject: "Re: troy@kde.org"
    date: 2002-03-31
    body: "But the Dot hasn't.  On a few occasions... and the way it goes down is generally with the front page displaying (about 50%-70% of the time), and the rest of the site just not coming up.  I'm just saying that if all the information that people are looking for is on a static home page, rather than having them click though to the article, it would alleviate load.\n\nDon't get me wrong - the Dot is one of my favorite sites, and I like the way Zope works.  I think it's perfectly fine, but I've been here when /. starts pouring traffic onto the site... or rather I *haven't* been here, because the page just times out.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: troy@kde.org"
    date: 2002-03-31
    body: "Yes, I know very well our past track-record. :-)\n\nI'm hoping we finally got it right...\n"
    author: "Navindra Umanee"
  - subject: "Re: troy@kde.org"
    date: 2002-04-01
    body: "\nWe're all behind you... the Dot is so good we just can't bear to be without it, even for a day.  :)\n\n:: Fingers crossed for the release ::\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: troy@kde.org"
    date: 2002-04-01
    body: "cache would help the dot withstand /.ing, IMHO\n\nif you havent already, enable RAMCacheManager wherever possible.\nand using squid to frontend zope and deliver much of the contents would seriously help\n\nmy .02"
    author: "kedai"
  - subject: "Re: troy@kde.org"
    date: 2002-03-29
    body: "me/ waits for the slashdot to be dotted when dot posts a link to the slashdot"
    author: "JP"
  - subject: "There is some support for \"Avatars\" in Zope?"
    date: 2002-03-29
    body: "There is some support for \"Avatars\" in Zope?"
    author: "Goddard"
  - subject: "Zope can be tuned for spikes and high load ..."
    date: 2002-03-29
    body: "... but I'm not sure squishdot can.\n\nSwishdot is coming soon though!"
    author: "NameSuggesterEngine"
  - subject: "Re: Zope can be tuned for spikes and high load ..."
    date: 2002-03-29
    body: "What kind of tuning are you referring to?\n"
    author: "Navindra Umanee"
  - subject: "Subliminal?"
    date: 2002-03-29
    body: "\"Subliminal: You perceive the dot to be better and faster than ever before.\"\n\nWhat does this mean exactly?\n\n "
    author: "gek"
  - subject: "Re: Subliminal?"
    date: 2002-03-30
    body: "Now that, my good and dear friend, is comedy to a degree that reaches beyond the scope of a single human mind.\n\nOnly by linking minds in the distributed conscience that is the coalescence of all thought, do subliminal messages make laughter bubble.\n\nThus, as the water buffalos roller-skate down the sides of tea-cups, so goes the days of our lives. \n\nTherefore the dot is better and faster than ever before.\n\nTroy\ntroy(at)kde(dot)org"
    author: "Troy Unrau"
  - subject: "Zope"
    date: 2002-03-30
    body: "\"Corrupt ZODB\"  LOL\n\nWhat if we could store all of our code and data\nin a single file on the filesystem, and call it the\nZODB?  \n\nZope has some cool things going for it, but I tend\nto lose all respect because of that design decision.\n\n"
    author: "anon"
  - subject: "Re: Zope"
    date: 2002-03-30
    body: "Yeah, what it should do is keep it all in a MySQL database, and then corrupt that :-)"
    author: "Carbon"
  - subject: "Re: Zope"
    date: 2002-03-31
    body: "Erm, it can..."
    author: "Greg Fortune"
  - subject: "Re: Zope"
    date: 2002-03-30
    body: "What if we could store all our files on a single partition and then corrupt that?  *LOL*\n\nZope can read from the filesystem anyway."
    author: "ac"
  - subject: "Back in Business"
    date: 2002-03-30
    body: "It's a good thing you didn't call it Open for Business, or else you'd probably be getting sued right about now. :-)"
    author: "Neil Stevens"
  - subject: "Oh no!"
    date: 2002-04-01
    body: "Look what I found on http://www.kdevelop.org/\n\n\n01. April 2002 - Project closed? After vehemently led discussions on the core mailinglist about the break of with development of the technical pioneer project Gideon alias KDevelop3 in favour of implementing all features in KDevelop2 the team is finally splitted. Some developers has already left the team. The development is stopped and a well-known software company has offered to buy all software licenses of KDevelop.\n"
    author: "reihal"
  - subject: "Re: Oh no!"
    date: 2002-04-01
    body: "\nDarn it, and I was looking forward to the next release - it was supposed to support creation of apps in qtcons.\n\nhttp://qtconsole.nl.linux.org/\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Oh no!"
    date: 2002-04-01
    body: "01. April 2002"
    author: "me"
---
After a brief hiatus that seemed closer to an eternity, I'm pleased to report that the dot is back and all is good.  A big "Thank You!" goes out to Chris McDonough and Jim Fulton of <a href="http://www.zope.com/">Zope Corporation</a>, Chris Withers of <a href="http://www.squishdot.org/">Squishdot</a> fame, and the wonderful Zope community at #zope for their excellent support throughout this matter. 
<!--break-->
<p>
We appear to have mysteriously experienced a  minor, though troublesome, corruption of our main database file.  Fortunately, thanks to some  Zope DB transaction magic and a not-so-recent good backup, we were able to recover, with zero data loss, all <A href="http://dot.kde.org/articles">464 articles</a> and 18965 comments (as of this reading). And while we did lose about two days worth of precious posting time during the read-only phase, I'm confident you guys will push us ever closer to the 20,000 (quality) comment mark by the time KDE3 is out.
<p>
<font color="#999999"><i>Subliminal: You perceive the dot to be better and faster than ever before.</i></font>


