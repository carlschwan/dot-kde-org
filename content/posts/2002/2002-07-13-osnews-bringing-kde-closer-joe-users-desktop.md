---
title: "OSNews: Bringing KDE Closer to Joe User's Desktop"
date:    2002-07-13
authors:
  - "jlanbo"
slug:    osnews-bringing-kde-closer-joe-users-desktop
comments:
  - subject: "A well written article with quite a few good ideas"
    date: 2002-07-12
    body: "Thanks Eugenia for the feedback.  I found your critique to be very informative.  Judging from the response on kde-core-devel, I think you will be pleased with the outcome ;-)\n\nRegarding your taskbar problems...  I fail to see how autoresizing kicker based upon the dynamically changing taskbar is either aesthetically pleasing (sounds horrible to have this moving/resizing kicker to me) or enhances usability?  How does a shortened kicker enhance useability?  It seems to me that this simply waists space.  Try and drag an icon towards the space on the right or left of a shrunken kicker... you will find that no icons can live there, because kicker is actually there, it's just been rendered transparent to the background.  Nevertheless, if you want this feature, try convincing the kicker/taskbar developers, but I think you'll have a hard time if you rely upon useability arguments ;-)\n\nAlso, I like your criticisms for kde lacking integration with some of the underlying systems.  KDE already relies upon X11 so how hard would it be to create a new KControl module for X11?  Why would this be an error?  Thoughts?"
    author: "Adam Treat"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-12
    body: ">I fail to see how autoresizing kicker based upon the dynamically changing taskbar is either aesthetically pleasing\n\nAsk the MacOSX users, who love it. ;-)\nIf some users do not like that by default, the devs could add a option on the Taskbar panel to autoresize Taskbar on demand. :)"
    author: "Eugenia"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-12
    body: "Well, unless I'm wrong, it appears a part-time MacOSX user is being asked. Most of your points looked well thought out and logical, but I still fail to see how a resizing taskbar is anything but confusing and distracting. The little squares of space that are freed looks unusable. I can just throw my mouse diagonally to the bottom left and it will end up on the K - I don't have to adjust based on current conditions.\n"
    author: "Ian Monroe"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-12
    body: "Sure. But this is not the default KDE option. The default KDE does not need that option. Only the people who would like to somewhat emulate the OSX Dock."
    author: "Eugenia"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-13
    body: "i like it when the K menu is nearer at the center of the screen , for this reason i think i resize my kicker too . i Dont want to look at the down left\nof my screen , at every time.\n\nmy eyes are centered in the middle of the screen.\n\n\nchris"
    author: "chri"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-14
    body: "There is no need to resize the panel to do that, though -- you can always move the K button.."
    author: "Sad Eagle"
  - subject: "Re: A well written article with quite a few good i"
    date: 2002-07-12
    body: "I don't know about that... I know a lot of MacOSX users that install Tinker Tools so they can anchor the dock to the side or bottom.  Otherwise your icons (especially trash!) are in different places depending on what you happen to be running at a given time, which is hell on muscle memory.\n\nI can understand wanting to shrink it, but wanting to shrink it, center it, *and* have it auto-resize totally breaks the UI as far as I'm concerned...  It breaks the \"mile high menu\" idea."
    author: "Ranger Rick"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-24
    body: "Eugenia,\nI see that you like the OS X dock, but from what I've read from usability experts (like Tog), the dock is a usability mess. Why would you *want* this? "
    author: "Rodriguez"
  - subject: "Re: A well written article with quite a few good ideas"
    date: 2002-07-13
    body: "> KDE already relies upon X11 so how hard would it be to create a new KControl module for X11?\n\nYou didn't install kdeadmin of KDE 3.1 alpha1? It contains a new X11 KControl module."
    author: "Anonymous"
  - subject: "Re: A well written article with quite a few good i"
    date: 2002-07-17
    body: "I agree with your reasoning. One thing I can't stand on a computer screen is unnecessary visual distractions - and would certainly qualify. I've seen dialogs that resize themselves based on their contents (clicking from tab to tab) and this is, in  my opinion, a most unfortunate development. It looks tacky. Leave kicker alone. : )"
    author: "Tom"
  - subject: "complete mailing list archives"
    date: 2002-07-12
    body: "BTW, here is the complete list of comments on the ongoing subject on the kde-core-devel list and on the kde-usability one. :)\n<p>\n<A href=\"http://lists.kde.org/?t=102643264300001&r=1&w=2&n=7\">http://lists.kde.org/?t=102643264300001&r=1&w=2&n=7</a><br>\n<a href=\"http://lists.kde.org/?t=102642703000008&r=1&w=2\">http://lists.kde.org/?t=102642703000008&r=1&w=2</a>"
    author: "Eugenia"
  - subject: "fonts"
    date: 2002-07-12
    body: "I rather like the Bitstream Charter font, when X11-Font hinting is enabled.  When it's not, I rather find ALL the AA fonts to be nasty...  (That's the Keith Packard XFT-Quality patch.)  In fact, one of the reasons I use KDE is because the AA Bitstream Charter is so very nice."
    author: "satai"
  - subject: "Re: fonts"
    date: 2002-07-13
    body: "I don't know if this seems like a strange suggestion, but I was wondering if the KDE League or United Linux or whoever could commision a type (that's type as in typographer) designer to create a set of open, properly hinted, kerned, screen friendly, truetype fonts for the open source world.\nhttp://www.will-harris.com/msfont-hint.htm\n\nAnti-aliasing looks nice with larger text but can tend to be an eyesore when smaller fonts are displayed.  Have you've ever noticed that on a win32 box most of the fonts displayed at any given time are /not/ anti-aliased?  That's because within a certain point range the anti-aliasing is turned off and the hinting alone is used to give the text a crisp pixel font look.\nhttp://www.minifonts.com/fontlist.html\n\nI think a lot of the KDE developers (*bless* you hard working folk) miss this point---as seen in a KHTML bug (overriding Xft antialiasing off switch).\n\nLegibility is an issues as well.  I always wonder why people always set Arial for most of their defaults.  I know most everybody has ran into the problem of being able to read what's in an edit box when the font is set to Arial (as well as not being able to partially select the bits your want---usually involving the letter 'i').  If you look at Georgia and Verdana related to Times New and Arial you'll set that the first set looks fatter than the second set.  That's for legibility.  Verdana and Georgia were /designed/ to be screen fonts.  That means good x-height and letter spacing.\nhttp://www.will-harris.com/webtype/readable_type.html\n\nYeah yeah... I know there are free web fonts from Microsoft that everybody and their dog has a utility to display the EULA and download but to be able to just install a distro and have a good set of crisp screen fonts ready to roll and well... free.\n\nhttp://www.freetype.org/patents.html \nThere's also the patent issue with Apple with regards to hinting. I figure distro's could be nice enough to at least /look/ into licensing from Apple, despite how much they would hate to. (especially Red Hat considering they're moving into an IP business model as well) http://slashdot.org/article.pl?sid=02/05/25/0123222\n\nMore fun type reading...\nhttp://www.fordesigners.com/xheight/matthewcarter.cfm?cfid=353367&cftoken=53132575\nhttp://www.typophile.com/articles/gillespie/"
    author: "ca"
  - subject: "Re: fonts"
    date: 2002-07-13
    body: "I don't think that will *ever* be possible. Good-looking Unicode TrueType fonts costs *tons* of money. Only the biggest and richest companies can afford that. And it takes at least a year to design one."
    author: "Stof"
  - subject: "Re: fonts"
    date: 2002-07-13
    body: "Could you give an estimate of how much money a set of 'Good-looking Unicode TrueType' costs?\n\nIf users and developers of KDE, GNOME, Mandrake, Redhat, debian, etc.. etc.. get together I doubt the sum would be significant per person."
    author: "wvl"
  - subject: "Re: fonts"
    date: 2002-07-13
    body: "Sure,\n\nand you now how expensive it gets if you pay for the development of the amount of source code involved in running KDE on a GNU/Linux System?\n\nCheersm\nThorsten"
    author: "Thorsten"
  - subject: "Re: fonts"
    date: 2002-07-13
    body: "Somehow there's a difference between code and a font -- I know a Truetype font consists of bytecode that's run on an interpreter, that's not the point -- but creating a font doesn't use the  same part of the brain as creating a library or an application. Perhaps it's that creating a good font is almost exclusively  fine-tuning, polishing, smoothing, and fine-tuning again. It's ultimately extremely tedious -- more tedious than coding an application. More tedious even than wrapping up a release. A lot of people like coding. Precious few people like endlessly fiddling with the shape of an A.\n\nJust try it, for a joke. Download pfaedit, and create a font with only a good, usable, readable, well-hinted captial O with low-resolution bitmaps added. Then imagine how much time you would spend if you wanted to add all the other 65.000 Unicode characters. And then try to calculate how much time you would have to spend to add a non-proportional, and a sans-serif version of your font. I'm thankful every day that we've got the fonts we have -- the Adobe fonts and especially the B&H fonts."
    author: "Boudewijn Rempt"
  - subject: "i had the same problem (taskbar size and kicker)"
    date: 2002-07-12
    body: "and the only way i managed it was to use a combo of kicker, external kicker and kasbar.\nI have my main kicker down with a few huge icones.\nI have an external bar on the left for applets and mini icons for not so importants links.\nI have kasbar on the right which i use as the taskbar. It gets bigger with apps and take no place otherwise.\nall those are on auto hide.\n\n"
    author: "jaysaysay"
  - subject: "Context Menu in File Browser"
    date: 2002-07-12
    body: "What I would love to see is something like XP's active context elements when using browsing your hard disk. Having a context menu at a RMB click is nice, but it is so much nicer to have context-aware options just one click away and VISIBLE on the screen. Same goes for searching...\n\nThis could be integrated in the sidebar but could also be placed directly inside the browsing window (see XP).\n\nI know some people will hate this idea, but despite me being an absolute fan of KDE, when I first worked with XP that feature really hit me as being great for both beginners and experienced users.\n"
    author: "Anonymous"
  - subject: "Hate it with a..."
    date: 2002-07-16
    body: "Purple Passion.  There are many more options on the right-click menu, not just a few dumbed-down ones, so I just the right-click menu.  I much prefer just the preview and file info on the left pane."
    author: "Chris Bordeman"
  - subject: "Ready for Joe Average"
    date: 2002-07-13
    body: "I think KDE is already as perfect as it can get. The problem now lies in configuration of the underlying system. You know, things like installing drivers, installing software, configuring X, and other things like that.\n\nTo install software, there are 3 ways:\n1. Packages (RPM, deb)\nWindows user tell you that packages are not userfriendly because they cause dependency hells. If you point to Red Carpet or something, they will whine about that it isn't included in the distribution by default, and is therebefore not userfriendly.\nOne person even told me that he spend hours just to find out that you have to doubleclick on an RPM to launch KPackage, and concludes that it's not userfriendly.\n\n2. Installation binaries (JRE, Kylix, LimeWire...)\nMost of these binaries rely on a terminal, so you have to enable the execution bit and then type ./foo\n\n3. Compile from source code\nWhen you tell people to type in \"./configure && make install\" they will yell that it's too unuserfriendly and conclude that it's primitive and behind the times because you have to type things instead of clicking & drooling.\n\n\nYou may want to read these threads to see how people still look at Linux:\nhttp://www.megatokyo.com/ubbthreads/showflat.php?Cat=&Board=UBB1&Number=481629&page=&view=&sb=&o=&fpart=3&vc=1\n\nhttp://www.megatokyo.com/ubbthreads/showflat.php?Cat=&Board=ukyotechtalk&Number=612872&page=0&view=collapsed&sb=5&o=31&fpart=2 (search for \"Linux is not a desktop OS\")\n\n\nAnd not to mention all the \"X is slow\" claims.\n\"It takes 20 calls in XFree86 just to draw a rectangle.\"\n\"The X protocol is outdated and inefficient.\"\n\"XFree86 is a slow implementation of X.\"\n\"Resize or move a window and you'll see what I mean. It's slow.\"\n\"XFree86 is internally very inefficient. Drawing operations do not get hardware accelerated.\"\n\n\nAnd as long as Windows is alive, there's no way Linux can be a \"true\" competitor on the desktop. Lots of people say this: \"I don't care what Microsoft does. They write good code. Windows XP is extremely good. That's all that matters.\"\nSo people don't care about MS taking away their civil rights: \"It's not like it will help a fuck if you don't use their products\".\n\n\nJust my 2 eurocents..."
    author: "Stof"
  - subject: "Re: Ready for Joe Average"
    date: 2002-07-13
    body: "speaking of userfriendlyness, those huge URLs are killing the layout.\n"
    author: "pioupiou"
  - subject: "Re: Ready for Joe Average"
    date: 2002-07-13
    body: "If there are shorter URLs I would have used those."
    author: "Stof"
  - subject: "OT: Shorter links"
    date: 2002-07-15
    body: "Check out \nhttp://www.makeashorterlink.com/"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Ready for Joe Average"
    date: 2002-07-13
    body: "That's a long-standing Konqueror bug. Konqueror should wrap plain paragraphs of text to the window width, like Mozilla or Netscape or Opera or IE or Icab or any other browser, instead of to the longest line in the text, whether that's\na <pre> text or an insanely big word or a gif of 2000 pixels."
    author: "Boudewijn Rempt"
  - subject: "Re: Ready for Joe Average"
    date: 2002-07-13
    body: "> You may want to read these threads to see how people still look at Linux:\n\nthe solution here is two fold: 1) continue to improve our systems. 2) help educate others through hands-on experience, since much of what they currently babble on about it ill-informed gibberish. \n\nunfortunately, most people who work with computers are relatively intelligent, they just don't like to leave their comfort zones (the bane of many a geek). instead they prefer to make assumptions about things they know absolutely nothing about. we can change that. not by arguing back at them on web boards (which is a waste of time, IMO) but by introducing these technologies to as many places as possible. get people using KDE on Linux and watch the bitching and moaning disappear. there is a lot more to whine about on Windows than there is on Linux, though you'd probably never know it judging by people's comments. \n\njust don't expect the same people who are accomplishing Solution #1 to also be able to accomplish Solution #2. \n\n> And as long as Windows is alive, there's no way Linux can be a \"true\"\n> competitor on the desktop. \n\ni would love to think you are kidding or just slipped into a coma on this one. \ni suppose as long as UNIX/Linux is alive Windows could never become a true competitor in the server market either. whatever.\n\nbtw, how is a '\"true\" competitor' different from a 'true competitor'? \n\nas for the civil rights issues (which is only a part of the Freedom aspect, really), they aren't the sole things Free software has going for it. the home user is largely unasailable right now for a number of marketing reasons, i agree. however the more important corporate and governmental clientelle are very ready for Free software and they are adopting it.\n\ndon't look to the living rooms, look to the board rooms. the living rooms will follow naturally from there."
    author: "Aaron J. Seigo"
  - subject: "Re: Ready for Joe Average"
    date: 2002-07-13
    body: "> And not to mention all the \"X is slow\" claims.\n\nI know it's probably flamebait, but I have to respond to this...\n\n\n> \"It takes 20 calls in XFree86 just to draw a rectangle.\"\n\nI have not counted, but I'll take your word for it. But, the number of calls in themselves don't matter it's the speed of those calls that matter (yes, I know there's some function call overhead)... Have you got any profiling data showing how slow/fast this is?\n\n\n> \"The X protocol is outdated and inefficient.\"\n\nI don't agree. The protocol is old, but that does not nessesarily mean it's outdated or inefficient. Actually if you have ever tried using MS Windows remotely (Terminal Cerver, Citrix, VNC etc..) you'll know what slow is - then try it over a slow modem link, it's hell. In my experience X is a /lot/ faster on remote displays (and if you tunnel it through SSH for the compression bennefits it's even better - for most apps if they are not too latency sensitive).\n\n\n> \"XFree86 is a slow implementation of X.\"\n\nIt might be compared to others, but don't blame X itself for that. \n\n\n> \"Resize or move a window and you'll see what I mean. It's slow.\"\n\nIt could be faster, but on modern hardware it's certainly more than fast enough (at least the speed has not bothered me for years).\n\n\n> \"XFree86 is internally very inefficient. Drawing operations do not get hardware accelerated.\"\n\nBut it's steadily getting better with every new release - XFree86 4.x is /way/ better than 3.x - things are moving in the right direction, and some drivers are hardware accellerated, just use appropriate hardware.\n\n\nI think what I'm trying to say is that X may have some flaws, but it also has a lot of bennefits. I'll try to list them and I'd like to hear your comments...\n\n- It's a well established standard, meaning that a huge amount of apps support it.\n- It's flexible. Supporting remote and local displays, different window managers, runs on multiple operating systems etc.\n- It has resonably to good performance (depending on implementation and hardware ofcourse), especially networked performance compared to other alternatives.\n- It is portable.\n- It is stable (at least in my experience) - current implementations have had years to stabilize.\n- Using modern toolkits like GTK+ and QT it is quite simple to write applications for.\n- It supports a wide range of input devices.\n\nAll in all, I think X is a powerfull tool and I don't think it needs to be replaced.\n\n\nI look forward to your comments :)\n"
    author: "Jesper Juhl"
  - subject: "Arrogant Moron"
    date: 2002-07-16
    body: "1.  If the tool is not bundled w/ the system and very obvious (and \"KPackage\" on a menu item is NOT obvious), then it is NOT user-friendly.  So how is someone to install \"Red Carpet,\" MUCH less know it exists or choose it from among several other \"good\" system update utilities.\nAlso, if I double-click on a package, I don't want to see its contents in KPackage, I want it to *freaking* install, or at least ask me if I want to install it or view it (along with a plain-english description of the package).\n\n2.  You mean I have to tell the system I want to be able to execute the file?  Why the hell can't I download it?  (Yes, I know the answer, but this is still stupid and very unfriendly behavior).\n\n3.  As far as compiling your package, why in the HELL should I have to do that?  Isn't the computer supposed to be helping ME?  There is NO reason why a modern decently-well thought-out OS should require compiling an app.\n\n4.\n>So people don't care about MS taking away their civil \n>rights: \"It's not like it will help a fuck if you don't \n>use their products\".\nOh Jesus!  So what exact 'civil right' is MS taking away from you???"
    author: "Chris Bordeman"
  - subject: "Re: Arrogant Moron"
    date: 2002-07-16
    body: "> Arrogant Moron\n\nYeah sure, blame me. Ever heard of this thing called \"manners\"?\n\n\n1. What in the world are you talking about?????\n\n2. Don't tell that to me, tell the guys who designed Unix and ask them why they did that.\n\n3. Ah so every developer out there has to buy every version of Linux/FreeBSD/OpenBSD/NetBSD/MacOS/MacOS X/Windows/IRIX/DOS/HP-UX/BeOS/AIX/FooBarOS and provide binaries for them?\nApparently the reason is *portability*.\n\n4. Let's see... things like DRM prevent you from copying things, even for own use. You can't even create backups. The new WMP license allows MS to look around in your system and change things. Look in the Slashdot archives."
    author: "Stof"
  - subject: "Deskotp, not Dummies"
    date: 2002-07-13
    body: "<i>One person even told me that he spend hours just to find out that you have to doubleclick on an RPM to launch KPackage, and concludes that it's not userfriendly.</i>\n\nWell, KDE brings UNIX to the desktop. That's the charter. Not to bring UNIX to dummies. First of all, by default KDE requires only single clicks unless you specify Windows-like behaviour in the personaliser which is run when you first start it.\n\nIf that user did not know that double-clicking on a desktop/filemanager objects launches the associated application, he could not have gotten far in Windows either.\n"
    author: "Rob Kaper"
  - subject: "Re: Deskotp, not Dummies"
    date: 2002-07-13
    body: "The problem is that 90% of the users are dummies. And unless everything is \"dummy-friendly\", people will always say that Linux is \"far from ready for the desktop\".\n\nI highly recommend you to read the 2 URLs I posted."
    author: "Stof"
  - subject: "Re: Deskotp, not Dummies"
    date: 2002-07-13
    body: "you just neatly stepped around his last statement, that being that if the person didn't know how to double click on something to get it going they wouldn't get far anywhere, not even windows. which means that's a pretty damaged statement.\n\ncare to comment on what he actually said?"
    author: "Aaron J. Seigo"
  - subject: "Re: Deskotp, not Dummies"
    date: 2002-07-17
    body: "<i>I'll</i> comment, gladly.  I find that argument blatantly fallacious.  Whether that user is acquainted with Windows standards or not is completely irrelevant.  Now, I'm not defending the person that didn't know his clicks, as I know no details, but, seeing this thread, I must say that Stof makes a valid point without drawing in the specifics of that user's assumed knowledge.  Realistically, how does it matter whether that user is familiar with Windows or not?  Surely, the user reached a drastic conclusion over something as trivial as clicking behaviour, but don't discredit Stof simply because he ignored something that didn't matter -- his point, that most users would be classified as \"dummies,\" still stands, regardless.\n\nIf you disagree, fine, but please base your rebuttal on something a bit more concrete, for the sake of integrity.\n\nIf that came off as flame-ish, I'm sorry, I certainly don't mean it that way."
    author: "dingodonkey"
  - subject: "Re: Deskotp, not Dummies"
    date: 2002-07-20
    body: "Unfortunately, it does appear that many users are \"dummies\".\n\nSo, they should read \"KDE for dummies\".  If this hasn't been published yet then it should be written as a HOWTO.\n\nI found that example interesting because this \"dummy\" apparently didn't think that the GUI was like Windows.\n\nBut, on the other hand, I have answered many questions on various news groups which could (or actually do) start out by saying that Linux isn't Windows.\n\nVery odd that the one thing that is like Windows (like the Mac OS actually) and the \"dummy\" for some reason generated the false idea that it would be different.\n\nI don't think that we can expect to make KDE ready for \"dummies\".  Teller said that no matter how foolproof you make something that there is always some fool that is more fool than the proof.\n\nThe empowerment that comes from owning your own desktop (rather than it being controlled remotely from Redmond) can only come from some understanding of it.  The 90% of the users that supposedly don't ever configure anything on the Windows desktop will gain very little with KDE.\n\nI don't know if we need \"KDE for dummies\" but a good tutorial is certainly necessary.  I suppose that \"dummies\" wouldn't read it. :-(\n\n"
    author: "James Richard Tyrer"
  - subject: "don't care about opinions of users"
    date: 2002-07-13
    body: "So, I hope now nobody says ever again that KDE developer don't care about users feedback and opinions from users :-)\n\nSeems like we will see many usability improvements in the beta. Maybe the KDE team should have more people that just care about this topic."
    author: "Norbert"
  - subject: "Re: don't care about opinions of users"
    date: 2002-07-13
    body: "> Maybe the KDE team should have more people that just care about this topic.\n\nnobody was ever penalized for stating the obvious, but there are bonus points if you can figure out how to accomplish having more people that care about this topic involved in KDE development!"
    author: "Aaron J. Seigo"
  - subject: "XFree86 != X11; Linux != UNIX"
    date: 2002-07-13
    body: "While I agree that the control over the screen settings that you get from KDE is less than perfect, I'm not convinced that KDE can, in an isolated way, do that much about it.\n\nXFree86 is currently controlled by a configuration file, which is only read when XFree86 is started. This configuration file is derived from the \"standard\" X11 configuration file.\n\nI.e. the X11 configuration file on Tru64 UNIX is definitely not the same as the X11 configuration file on Linux. And I'm sure that the XI Graphics X11 server does not have the same config file as XFree86.\n\nThe \"best\" solution would surely be for X11 to have some API that can be used to set the various attributes (in much the same way that xgamma can be used to set the gamma correction). The API needs to be a standard part of all X11 implementations - including XFree86.\n\nI don't see a problem with XFree86 pushing ahead and developing this standard (in the same way that XRender only exists in XFree86 and not X11R6.x - yet). Once the standard is there then KDE can support it. Particularly as David Dawes of the XFree86 project was making noises about doing this sort of thing in a recent interview.\n\nThe same sort of issues apply to keyboards, mice and possibly other peripherals...\n\n... so I'm not sure that KDE can solve the issues nicely whilst continuing to be a Desktop Environment for UNIX - rather than for just Linux and XFree86."
    author: "Corba the Geek"
  - subject: "Re: XFree86 != X11; Linux != UNIX"
    date: 2002-07-13
    body: "according to some traffic on the XFree lists i've read, they are indeed working on a way to adjust settings w/out restarting the X server since they realize how important this is."
    author: "Aaron J. Seigo"
  - subject: "Re: XFree86 != X11; Linux != UNIX"
    date: 2005-12-09
    body: "i want to install knoppix on hard drive. can its support .dat file or mp3 format files too if yes where to downloded knnopix linux for hda "
    author: "jubair"
  - subject: "right click on objects"
    date: 2002-07-13
    body: "One thing that could be improved IMHO is the behavior of right \nclicking on objects. It could be done a little more consistently, \ngiving the user SOLUTIONS. For instance: a user has an image he/she\nlikes displayed in Konqueror. Right clicking on the image, should give the \noption to save this image as a wallpaper, and launch kcontrol in the\n\"background\" section. Things like that make the user comfortable.\n\nIt would be nice if the user knew that right clicking on \"things\"\nwill give her/him a set of options of what to do with this \"thing\"\n(an icon, a filename, etc) I know this is mostly done, but a little\nmore work on that area would be great. In particular, if the \nobject represents information that can be handled by/associated to\nseveral applications, right clicking the object should offer access to\nall these applications in a friendly manner IMHO. Again, this is partially\naddressed with mime types, through the \"open with\" menu option in konqueror.\nBut it would be great to see it work across the desktop in a more \nconsistent manner.\n\nBTW, a couple of the suggestions from Eugenia's article  have to\ndo with right-clicking.\n\nCheers !\n-- Leo"
    author: "Leo"
  - subject: "Re: right click on objects"
    date: 2002-07-13
    body: "i think what you are referring to would be handily covered by \"service menus\". these are simply .desktop files that describe an action for files of a certain mime type. this is the sort of thing that even non-developers can get into. below is an example of a service menu that allows you right click on an image and set it as your background:\n\n========\n[Desktop Entry]\nServiceTypes=image/png,image/jpeg\nActions=setAsBackground\n\n[Desktop Action setAsBackground]\nName=Set As Background Image\nExec=/bin/sh -c \"dcop `dcop \\`dcopfind -l kdesktop\\` background` setWallpaper %U 6\"\n=======\n\nsave the stuff between the '====' liness $KDEDIR/share/apps/konqueror/servicemenus/saveAsBackground.desktop to try it out... (one could even modify it to first copy the image to $KDEHOME/share/wallpapers and set the background to use that so if they user moves the image they don't lose their background)\n\ntake a look at the other servicemenu .desktop files, then play around with making a few, they are really fairly simple!"
    author: "Aaron J. Seigo"
  - subject: "Re: right click on objects"
    date: 2002-07-14
    body: "some things i left out in the above example:\n\nyou can define an icon to appear in the menu by name (leave off the .png or whatever) by adding an Icon=iconname line to the service menu .desktop file.\n\nyou can define multiple actions per .desktop file, just make the Actions= line a list of items and have one [Desktop Action <name>] block for each...\n\nhappy hacking =D"
    author: "Aaron J. Seigo"
  - subject: "Re: right click on objects"
    date: 2002-07-15
    body: "Do you have a place where I should point konqueror to get other such great tips?\n\nDoes such a place exist? I'm sure there are lots of such things that are unknown to 80% of KDE users, though a lot of these users could use it and share some configuration files......\n\nRaph"
    author: "Raph"
  - subject: "Re: right click on objects"
    date: 2002-07-15
    body: "now  this is VERY interesting! I tried out your example and I must say I was hooked! are there any tutorials/docs you can point me to on how to write servicemenus (i am not a developer)?\n\nthanks,\n\nfred"
    author: "fred"
  - subject: "Gnome vs. KDE"
    date: 2002-07-14
    body: "First - I am not a troll.  I am *not* here to say 'KDE is better/GNOME sucks etc etc.'  I am *not* here to advocate either desktop environment.  That being said, I continue to my point.\n\nMany people prefer GNOME over KDE and vice versa.  This is of course, their choice.  It prevents users from having to fit in a mould of what everyone thinks they should like.  I've heard a number of posts (not necessarily here) saying 'GNOME 2 sucks.  KDE is light years ahead'.  My question to you, Linux desktop community, is: \n\n\"What features are in KDE 3 that are not in GNOME 2?\"  \n\nI would *please* like reasonable responses - no trolling/flaming.\n\nI thank you for your time.  I hope that Linux does make it to the desktop and that soon users will be able to get Linux powered (heck YEAH) computers."
    author: "Anon Man"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "KDE has many applications, GNOME2 has almost nothing it's just too new and unstable.  KDE looks nice, GNOME doesn't.  KDE development is the best GUI/desktop programming environment on Linux.  GNOME/GTK programming really sucks.  KDE is community centered and not project development and direction isn't controlled by any one company.  GNOME is development is primarily Red Hat/Sun/Ximian, community involvement has been practically killed.\n\nOne final word:  Konqueror.  This browser is the best I have ever seen bar none.\n"
    author: "ac"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "Oh...ok.\n\nI was hoping for something like:\n\n* Right click menus are not context senstive in all apps\n* ...\n* ...\n\nThere are a lot of GNOME 2 apps.  The GNOME 2 release was meant to be a desktop release only, not desktop + apps release.  Just setting the record straight.\n\nP.S. gtkmm is C++ based - you don't have to work in C\n\nPoint taken about the development environment though - I agree, so far GNOME 2 does not yet have an IDE.  I have heard one is in the works, but apparently that will take some time to pull through\n\nI didn't know community involvement was low in GNOME.  In fact, I think community involvement is just as spirited as it is here! :)\n\nThanks for the answers anyways.  I welcome more comments.  To those who have used both GNOME 2 and KDE 3, I vlaue your opinions.  Thanks."
    author: "Anon Man"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-16
    body: "As someone who's tried both gtkmm and QT, I really have to say that there is a huge difference -- they aren't just substitutes because they're \"both C++.\"\n\nPut simply, gtkmm is just not well tested, developed or architected in my opinion - it is very frustrating to use compared to QT.  Not being of the native language of GNOME, I suspect it will continue to be something of a 2nd-rate citizen.\n\nFor a C++ developer, the obvious choice is QT/KDE and on the flipside, GNOME if the developers experience is with C.  I think it really creates something of a technical-cultural divide.  If the native languages themselves were the same, it would be far easier to cooperate and integrate improvements from the other side.\n"
    author: "Another Anon-person"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "Ah, the usual Anonymous Cowards/Slashdot reasons again.\nI'm not even going to comment on what you say since most of them have been *proven wrong* for a long time now.\nPlease, if you have to troll, don't do it so obvious."
    author: "Stof"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-02-15
    body: "I'm not too sure about KDE being better looking than Gnome. KDE 3x looks great, but the Gnome 2x desktop looks better ... atleast from all the screeshots I've seen posted for both. I currently use KDE 3.1 (hoping to upgrade to 3.2 soon) but the screenshots I've seen posted on http://www.gnome.org/start/2.0/screenshots/ are very impressive. Nautilus looks awesome (from the screenshots).\n\nAll this said.. KDE's toolkit rocks.\n\nSo lets assume for now that they both look very nice. So which one's better? Which one's faster? Which one has smoother looking fonts? With all the \"common\" features on, which one takes up less memmory? Instead of pointing and saying \"that suXors!\", how about we come up w/ a better way of comparing the two!? :D.\n\nI'd be VERY interested in some quality feedback on this one :).\n\nCheers!\n- V"
    author: "V Patel"
  - subject: "Re: Gnome vs. KDE"
    date: 2007-10-12
    body: "I only used Konqueror for several months when I was running Knoppix.  But I found it to be slow slow slow.  On the other hand, I quickly became addicted to Opera when I tried it.  Can Konqueror do customized searches from the url?  Can you download torrents in one click, just like a regular download?  Not to mention the fact that they were the first to use tabbed browsing.  Just my opinion: I wish more people would give Opera a try. "
    author: "Karma"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "> I would *please* like reasonable responses - no trolling/flaming.\n\nOK, here's an opinion for a GNOME user. I don't have KDE 3 (yet), because I'm short on disk space right now.\n\nKDE offers some features that GNOME doesn't and vice versa.\nKDE has an integrated office suit, GNOME has a collection of individual programs (but that doesn't mean the GNOME ones are worse).\nKonqueror is a better web browser than Nautilus with Mozilla component is (the app adapt itself better; you can rightclick on a link in Konqueror, but you can't do that in Nautilus), except in terms on rendering.\nDefault KDE looks better than default GNOME. However, default GNOME doesn't look as cluttered as default KDE is (all the gradients tend to overwhelm me at first).\nKDE has support for KDE 2.x's status dock (duh), but GNOME 1 is still stuck with 1.x's status dock. I can't find a status dock in GNOME 2.\nIn GNOME, you can easily switch window manager. In KDE you have to set the WINDOWMANAGER environment variable or something.\nNautilus has support for emblems on files. I haven't seen this in Konqueror.\n\nAll in all, I don't think they differ too much. GNOME is cleaner, and simpler, while KDE is more advanced, more integrated and looks more like Windows.\nI wouldn't say one is better than the other, they are just different.\nI think I'll stick with GNOME 1.4 for quite a while."
    author: "Stof"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "> In GNOME, you can easily switch window manager.\n\nNot in Gnome 2.0 anymore. I think we should compare the latest released versions."
    author: "Anonymous"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-09-26
    body: "That's b.s.\n\nIt took me all of about 2 minutes to switch out sawfish and replace it with metacity when I was goofing around with window managers on Gnome 2.0.  I'm using the Debian unstable branch if anyone's curious."
    author: "Anonymous Coward"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-09-26
    body: "Good job for replying to a year and a half old post. It was true then btw. "
    author: "AC"
  - subject: "Re: Gnome vs. KDE"
    date: 2006-04-02
    body: "Yeah, I know.  Necroposting isn't cool.  :P "
    author: "zombor"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "> \"What features are in KDE 3 that are not in GNOME 2?\"\n\nCan you say menu editor :-)?\n\nThere is a whole bunch of things and I'm sure everyone would miss something when trying to create a list. My try: more sophisticated file dialog/text editor/bookmark editor and control center, configurable toolbars, meta-information infrastructure, modular multimedia framework, cups printing integration, font installer, more kio/vfs slaves, regular expression editor, analogue clock applet, RDF news ticker, integrated office suite, integrated file-/webbrowser, translations to more languages, a fast and slim window manager and atm of course several programs which have no released to Gnome 2 ported version (browser, mailer, organizer, IRC client, news reader, meta-office, IDE)."
    author: "Anonymous"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "Thank You.\n\nYour comments were much more in line with what I wanted to hear.  I appreciate your taking the time to do this :)\n\nConcerning the file dialog - it will be coming in gtk 2.x I believe.  The GNOME developers have heard this complaint and they agree that the current one is *very* sparse.\n\nModular multimedia framework.  Gstreamer.  I *sincerely* hope that this will be the future oof multimedia in GNOME.\n\nYou are right - I have not yet found a menu editor.\n\nPS. Galeon 2 is actually available - I think they're cleaning up some last minute issues.  XChat (IRC), Pan (newsreader) have been available for some time now."
    author: "Anon Man"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "I know that at the latest Gtk 2.2 shall bring a new file dialog and future Gnome releases will improve (already working) several mentioned points: 2.0.1 menu editor, 2.2 Gstreamer & Metacity and that there is work to integrate their office programs. Just talked about today and Galeon/XChat/Pan for Gnome2 are only development versions just now. If you would have asked \"What features are in Gnome2 that are not in KDE3\" you would have got as answer SVG and tabbed browsing although it's already implemented in KDE 3.1 Alpha1. :-)"
    author: "Anonymous"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "> Can you say menu editor :-)?\n\nNautilus is GNOME 2's menu editor. Yes, you can finally create desktop entries in Nautilus now."
    author: "Stof"
  - subject: "Re: Gnome vs. KDE"
    date: 2005-01-11
    body: "lmao,What the heck are you talking about."
    author: "bob jones"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-07-14
    body: "You have to try both to find out which one does fit your needs better. There are often not major features but details which makes the difference: The \"Run Command...\" dialog which has more options, the screenshot utility which allows you to shot only a single window. If the Gnomes praise their usability, ask why they removed launch feedback in Gnome 2. And why, if you select 5 files in Nautilus to make them read-only it popups 5 properties dialogs. Try the same for 1000 files (yeah, there is a warning). And hell, there are also people out there who choose their desktop because of their liking of the default colors or style."
    author: "Alf"
  - subject: "Re: Gnome vs. KDE"
    date: 2002-11-01
    body: "These views are interesting. I have always been interested in supporting a superior philosophy (in a personal opinion) for a given time, and switching if I find another superior philophy on Desktop (as opposed to Window) Managers.It is important to stick with one over time, and switch when one \"personally rates higher\" than another. I have had a quite a bit of non-development, everyday usage of both GNOME 2 and KDE 3. Here is what I know as of now, and am still considering which one finally to use: (ratings out of 10)\nCATEGORY                  GNOME 2   KDE 3\nFirst-time Use              9        7\nGeneral Usability           8        8\nFeatures and Functions      6        9\nApplication Purpose         6        8\nApplication Quality         9        7\n                          \nTotal                       38       39\n\nNow these numbers are just numbers I picked based off of what I think, and each category could have a different weight. For example, Application quality, in my opinion, is very important and I like applications that do what they claim very well. So this would change the totals to make gnome equal or slightly higher than kde. But for now, I will breifly discuss each category I think of:\n\nFirst-time Use:\nGnome is very nice with this because things are very clean and easy to figure out upon first login. The takbar and menus are not too cluttered and contain all needed information. It is great if you are a previous Windows or Mac user to convert to Linux with because (at least in the Mandrake 9.0 edition), you have  a top menu bar with a running programs menu on the top right, and a taskbar familiar to windows users (all on first login...).\n\nGeneral Usability:\nI think that once one settles into a desktop manager, the usability is about the same for both. As of now, I can log into both DMs and completely have full confidence in the usability of both. I should clarify that usability implies the ease of use of the desktop for everyday things like browsing the internet and checking email and newsgroups efficiently using the DM tools.\n\nFeatures and Functions:\nKDE just seems to have a lot more. Their file management and configuration schemes are much more featured and provide all kinds of configurability and functionality. They are quite good at that. Simple features like storing the setting of a window by right clicking on the titlebar, or viewing properties of multiple, random selected files. There is more that I can't think of off the top of my head now.\n\nApplication Purpose:\nI like KDE's application suite. They are separate, yet integrated into each other (like KOFFICE or KMAIL/KADDRESSBOOK/KORGANIZER..) and the theme very well. I like the look and feel and the ease at which I could perform configurations. GNOME, on the other hand, has many applications, but doesn't integrate them as well, in my opinion (where integration is really a personal user preference).\n\nApplication Quality:\nGnome is just so good at outputing things that work cleanly. For example, frustrated with getting KPilot and its conduits to work (if anyone can help, please email me :) ), I tried out evolution and its conduits. They worked beautifully, right away. I know evolution isn't directly gnome, but it's quite prevalent as a gnome 'featured application.'  Gnome-terminal is clearly superior (in my opinion) to K-Shell simply because it loads up fast (everyone likes a fast-loading terminal) and is easily configurable. This is very nice. Other good applications include AbiWord and GNumeric. They just seem to work for most of my purposes. Although gnome, in my opinion, has superior working applications on the whole, KDE seems to be cathing up, and they have more potential. For example, KMail is extremely configurable, shortcut-keyed out the wazooo and works great. KDE has its set of reliable applications.\n\nPerhaps the reason why KDE's application quality is a little less superior in my opinion is the fact that they just have more. I would say that both DMs have an equal number of quality applications, but KDE simply has that much more that are still being tweaked.\n\nOK, I hope this doesn't start some trash-talking or anything. I just thought I'd give my outline to date. I would also like to mention that I have gone throught he WindowManager (Namely Fluxbox and WindowMaker) phase, and have determined formyself that DMs are superior for my purpose (that's a whole 'nother topic, though).\n\nInterested in the responses,\nGeet"
    author: "Geet"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "i think both the newest versions of both gnome and kde are a heavy handed slap in the face to the open source/free os community. the attempts by both at homogenization, and (l)user-friendliness destroy what little flexibility these overweight systems had to offer. especially in light of bluecurve, these are typical symptoms of a bloatware windows-izing of the *nix desktop. new features and stability implimentations in both of the desktops are barely noticable beyond all the eyecandy they provide. open source and free os's will never be mainstream because there would be nothing special to them if they were. with a copy of the latest propriety/monopolistic software etched on the hard drive of every system rolling off the line, it would be ludicrous to ask the average person to fire up a slackware box and do anything but stare at the login. so the other alternative is to dumb it down enough to make it easy for people to swallow. i see more and more people turning to fluxbox, window maker, or some other environment to escape either kde or gnome. the strongest argument for linux *bsd fillinablank is that you can pull a computer out of a dumpster and have it running one in no time. an argument which is totally negated by both kde and gnome. i understand that the days of a 66mhz workstation are gone, but when it won't run respectably on 3 year old hardware, a serious problem is ocurring\n\nps -- jesus! i could compile a kernel in the time it takes to start kde :P"
    author: "turboginsu"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "The question is obviously relative on the type of person using the desktop,  i'd have to say  windows users  and people who just want something easy to use but slow go with KDE.  KDE's menus and applications take way longer to load than gnome's and i REALLY hate the hotkey settings trying to be all windows user friendly in kde.. (gnome 2 seems to have done the same thing just recently)  but for the most part i think they are both slow and clunky as a desktop environment,  if you want speed go with something like turbo says, fluxbox, windowmaker.. or justa bout anything NOT gnome, kde, or enlightenment.   i like the way gnome looks and how you can configure it.  KDE has many more options but most are useless or crash whenever you apply them, i've not seen gnome's panel crash even once since i've used it.. but kde on the other had has mucho problems with stability for their components.  sure they respawn everytime but who wants to see that damned bomb error message popping up anytimg you change something?  (not me)     as for the statement  about redhat slipping into a user friendly eyecandy heap of slow code....  sad but true ;   if you want eye candy.. an easy to use computer.. get a mac with OSX.. they arent THAT expensive..  if you want to use an operating system with speed and reliabilty get linux..  and if people are too braindead or just too lazy to use something other than KDE / GNOME   the linux community is going to find itself being a big mess and a struggle to forever be more user friendly.. with less configurability ... more corporate influence  ... less speed...  a bulkier  script kiddy (mandrake) operating system.  sure its good to get the average person  way from the normal winblows OS and into something new but come on  you dont have to sacrifice the entire platform  for the rest of them..   the world will never be able to change people who dont care and want to just use windows with the \"d00d it does everything i NEED it to\" attitude.. so why not focus on the real users who actually care about performance, speed, reliability, stablility, etc..   we're going down, and gnome / kde  are the leaders in this fall.     what more do you need than a menu, taskbar and some widgets?   i mean is it really worth it?\n\n--orbital"
    author: "orbital"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "stfu troll. \n\nk thx."
    author: "fault"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "It's funny how people (or in this case, the original poster pretending to be several different people) reply to old articles such as this, though (this article was posted what, eight months ago?) phew."
    author: "fault"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "oh but troll and i ARE the same person (incase you didnt catch the joke wise guy)"
    author: "orbital"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "I'll troll it up in linux land,  if you like how kde functions maybe you should give windows a try?  :D "
    author: "Troll"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-30
    body: "or maybe someone posting stuff on here spawned the interests of people when before everyone assumed it to be dead?  just a thought"
    author: "orbital"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-31
    body: "of course this is the the international brotherhood of aryan kde users, so anyone who doesn't say \"KDE FUKEN R00LZ D00D GNOME IS A GODDAMN PILE OF SHIT IF YOU USE ANYTHING BUT KDE YOU ARE LESS THAN HUMAN\" is going to catch hell from everybody who even looks at this site. so who is the troll?  if this is just some place where people can talk about how great kde is and nothing else then you should just save everyone time and effort and just play with your kde and jerk off without bothering anyone else."
    author: "turboginsu"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-31
    body: "kde > you\n\nnuff said"
    author: "fault"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-01-31
    body: "kde !=< window maker/GNUstep!!!!"
    author: "turboginsu"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-02-03
    body: "of course, without either kde or gnome a valuable peice of open source would be lost. without the programs and libraries these projects have contributed, linux, and X for that matter would be a great deal more sparse (i would be much less inclined to do anything on linux if everything were drawn in monochrome athena widgets). the point i was trying to make is that these systems are crumbling under their own weight, and one of the major selling points of free OS's might be crushed underneath them."
    author: "turboginsu"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-02-24
    body: "OH MY GAWD!! ::shifts into first gear:: VROOM!! CRASH!!!"
    author: "oRbital"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-07-13
    body: "First of all I'm going to come at you from and enduser point of view.  I develop, but not for Gnome or KDE.  So wtih that in mind.\n\nKDE has better menu-editing and more applications (although albeit kind of useless for me)\n\nGnome2 has better speed and flexibility\n\nI personally despise the windowish look of KDE.. it just comes off as too bright.\nGnome2 is simple and clean looking...\n\nRedhat 8 & 9 got it right.  Use the Gnome 2 interface with some of the KDE funcitionality and programs built in... and with apt-get RPM... I have all the useful tools I need.  \n\nScreenshots below.\n\nhttp://65.96.54.113/images/my-desktop.png\nhttp://65.96.54.113/images/my-desktop2.png\nhttp://65.96.54.113/images/my-desktop3.png"
    author: "9mind"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-07-13
    body: "> Gnome2 has better speed and flexibility\n \nFlexibility? Like in \"amount of offered options\"? :-O\n\n> I personally despise the windowish look of KDE.. it just comes off as too bright. Gnome2 is simple and clean looking...\n\nAnd what shall your three screenshots with three different styles show? That you're able to change Gnome's style but not KDE's to a simple clean and looking style?\n\n> Use the Gnome 2 interface with some of the KDE funcitionality and programs built in...\n\nGnome 2 interface with KDE functionality? Please elaborate.\n\n> and with apt-get RPM... I have all the useful tools I need. \n \napt4rpm is neither inventend by, specific to or default on RedHat.\n\nI'm really wondering why you continue this one year thread."
    author: "Anonymous"
  - subject: "Re: Gnome vs. KDE"
    date: 2003-07-13
    body: "Are you two celebrating the first birthday of the article you reply to? =P"
    author: "Datschge"
  - subject: "Re: Gnome vs. KDE"
    date: 2005-10-07
    body: "2 and a half years later it's still funny.."
    author: "Mark"
  - subject: "Re: Gnome vs. KDE"
    date: 2007-09-16
    body: "What's linux?"
    author: "jon"
  - subject: "Re: Gnome vs. KDE"
    date: 2007-10-12
    body: "Linux is an operating system based on Unix.  If you don't know what an operating system is, then it is probably safest to say it does what Windows does for your computer, but it is free and far superior.  \n\nIf you want a more thorough and at times humorous explanation, google: \"In the beginning was the command line\". If you want to see it for yourself, try ubuntu.org.  You can run Ubuntu Linux (and many others, most notably Knoppix) from a disk without installing it on your system."
    author: "Karma"
  - subject: "Re: Gnome vs. KDE"
    date: 2007-11-11
    body: "Thank you. You are a nice person. I have installed openSUSE on a virtual machine, but I still need to download the kernel source so that I can compile VMWare tools and make the resolution better."
    author: "jon"
  - subject: "Re: Gnome vs. KDE"
    date: 2008-04-22
    body: "Mr. Pebbles trying to get rid of Maggila Gorilla, Ya just can't"
    author: "Snipples"
  - subject: "Impressed"
    date: 2002-07-14
    body: "I am really impressed with the feedback this article is getting from this board.  When I saw that it made the front page, I assumed that there would be a whole slew of flames from defensive developers, but instead you KDE guys never fail to impress me (a GNOME user) on your positive responses and willingness to listen to critiques.\n<br>\nThe article, by the way, is great, and the GNOME developers should also take a look at it."
    author: "Chris Parker"
  - subject: "Kicker default looks"
    date: 2002-07-14
    body: "at low resolution 800x600; is very annoying. why can't kpersonalizer also have\nan option like: \n\n1. no buttons \n2. no logout/lock screen applet\n3. no task \n\nwith only K button, Desktop Access button, Quick launch, Task bar, System Tray and Time applet. that would be more pleasant.\n\nalso the docked taskbar looks ugly. instead of a sunken applet it should be equal like other buttons. taskbar looks more like system tray :(\n\nno hover box, in kicker quick launch: if you put your mouse cursor over the toolbar buttons they get highlighted, but quick launch buttons do not show responsiveness."
    author: "Rizwaan"
  - subject: "attachment"
    date: 2002-07-14
    body: "kicker at 800x600"
    author: "Rizwaan"
  - subject: "Re: Kicker default looks"
    date: 2002-07-15
    body: "You can remove any buttons you like...  personally, i have about 3 or 4 icons for my most frequently-used apps, the system tray, time applet, and windows list (instead of taskbar)...  i don't like the logout/lock thing (personally, i have no use for it)...\n\npersonally, i like the docked taskbar with the sunken effect...  it's a \"container\" for application entries, so it makes sense to be sunken...\n\nthat's what i like about KDE3...  if you don't like something, change it...  and it's not at all hard or un-intuitive to rtclk->remove..."
    author: "glandix"
  - subject: "Joe Don't like ugly Menubar"
    date: 2002-07-14
    body: "The menubar in KDE mostly is not in alignment with toolbar. say this basic style or QT windows one (see attachment). The first image shows how non-aligned menubar and toolbar is. whereas the second image shows the menubar and toolbar aligned. if basic style adds a single black line at the bottom of menubar then menu bar and tool bar would look pleasant."
    author: "Rizwaan"
  - subject: "Re: Joe Don't like ugly Menubar"
    date: 2002-07-14
    body: "I love it!  That is the way it needs to look.  Without that line, it looks detatched and out of place.  Good observation!"
    author: "Brian"
  - subject: "Re: Joe Don't like ugly Menubar"
    date: 2002-07-15
    body: "So true, the new menubar/toolbar look you proposed looks so much clearer...\n"
    author: "Anonymous"
  - subject: "Re: Joe Don't like ugly Menubar"
    date: 2002-07-16
    body: "I'm with you on this, if menu and toolbar are on the same level it looks cleaner and more slick."
    author: "Tar"
  - subject: "Please don't touch the tear-off handles!"
    date: 2002-07-15
    body: "Like most features that are not in Windows, the tear-off handles are in constant danger of being questioned, removed and marginalized.\n\nPlease do not change the size of the tear-off handles, the \"solution\" in the article is much too small.\n\n"
    author: "Roland"
  - subject: "Installation"
    date: 2002-07-15
    body: "I'm running Mandrake 8.3 Alpha (Cooker) on my maschine. What do I need to install KDE 3.1 Alpha and how do I do the installation? Why isn't there something like an update.exe to update the system with a single mouseclick?"
    author: "Athlon"
  - subject: "Re: Installation"
    date: 2002-07-15
    body: "There is no update.sh (exe) file because:\na) you want the control over the update\nb) kde only gives you the source. So they will have more time to fix the bugs;)\nc) Nobody has written a \"all dist kde installer\" (there are some scripts that take the source and compile / install it)\n\nWait for packages from Mandrake. I don't know if there are some.\nIf you found some install it the way packages are installed on your system.\nFor Example:\nOn suse it is rpm -Uvh --nodeps --force /usr/src/kde31_alpha/*.rpm\nOn debian it is dpkg --install /usr/src/kde31_alpha/*.deb\n\nEvery dist has general package installation help on it's homepage.\nAlso have a look at the realease notes:\nhttp://www.kde.org/announcements/announce-3.1alpha1.html#binary\n\n\nhave fun\nFelix"
    author: "halux"
  - subject: "Re: Installation"
    date: 2002-07-16
    body: "If you want to run Alpha quality software, then you better know how to be able to compile and install it from source. If that's too big a challenge (there's a guide on www.kde.org) then I suggest you wait until KDE 3.1 final is released later this year.\n\nThe only reason you'd want to install 3.1 at this point in time is to help fix bugs. And trust me, there are also some compile bugs - not all of it builds very well yet.\n"
    author: "anonymous"
  - subject: "Re: Installation"
    date: 2002-07-16
    body: "I doubt you can ever update a system using a .exe with a single mouse click. Just after reading the EULA, for example, you have to click once. You have to click the \"Next\" button a few times, too.\n\nYou should know that:\n\n1) Cooker is unstable, use for testing only!\n\n2) KDE 3.1 Alpha is unstable, use for testing only!\n\nBack to your question: there's no .exe. There's something better, though, called urpm, and the rpmdrake interface to it. As soon as KDE 3.1 Alpha enters Cooker you'll be able to upgrade your system. You also have the freedom to build KDE yourself, which isn't hard if you read the documentation.\n\n"
    author: "Carg"
  - subject: "Please do NOT such STUPID stuff like \"My Books\"..."
    date: 2002-07-16
    body: "People should really learn what they are doing and \"where\" they store there stuff (the underlying file system semantics).\n\nIt's a false assumption to think that the earth would be a better place if all file system knowledge is hidden.\n\n-Dieter\n\n-- \nDieter N\u00fctzel\nGraduate Student, Computer Science\n\nUniversity of Hamburg\nDepartment of Computer Science\n@home: Dieter.Nuetzel@hamburg.de"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Please do NOT such STUPID stuff like \"My Books\"..."
    date: 2002-07-16
    body: ">People should really learn what they are doing and \"where\" they store there stuff (the underlying file system semantics).\nIn Windows, people don't learn that much - and still succeed to do their work, provided there is somebody doing the admin work.\n\nPeople don't care where 'My Documents' or 'My Books' \nare located physically - as long as it's one click away.\n I've heard of people storing their documents in the \"Recycle Bin\" of Windows ( before the introduction of \"My documents\") - purely for convenience reasons. \n\"You open the directory with one double click, and all your documents are there\". Double-click, scroll, double click - and here you go. \nBTW, the UNIX file system structure is suited very well just for this very matter - hide underlying filesystems from the user. \nYou may keep e.g. the /usr directory together with the /, or on some separate partition, do this partition ext2 or ext3 or reiser, or even on the NFS partition - it doesn't matter, for you it's all the same /usr. \nWhy don't you go further in these abstraction? All office work is kept in 'My Documents', and it's no matter whether it is part of your home directory or some NFS partition. Why should a regular non-admin user know, that /home/this_very_user is on /dev/hda6 partition, and this partition is of type reiserfs? How would it help him to complete his office work?"
    author: "Alex"
  - subject: "Default Icon Sizes in Kicker"
    date: 2002-07-16
    body: "I largely agree with Eugenia; a lot of what she has said makes me regret not vocalising my own usability peeves with KDE sooner (and, soon, I may).  However, there is one main point on which I cannot help but feel compelled to disagree:\n\nDon't use a QuickLauncher by default.  I don't think that is the ideal solution for the average user by any means.  I do, however, strongly agree that the default panel is a mess, especially on lower resolutions.  I would propose, rather, that it be cleaned up.  Perform a survey: find out which features, on by default, are used by most new users, as new users are the ones least apt to perform extensive peconfiguration.  Realistically, how many new users make use of KDE's virtual desktops frequently?  I don't know of any, to be honest -- and I've introduced a good half-dozen people to KDE.  Also, why put a trivial application like KWrite down there?  If you want some form of text editor, wouldn't it be more appropriate to put a \"major\" flagship application, such as KOffice?  Otherwise, it seems (to me) to be a mere waste of space.  As for the terminal, I am assuming that anybody knowledgeable enough to use it frequently would know how to access it, so that isn't a necessary icon.  I'm not saying it needs to be pitched, but it's worth consideration.  Another point: who really uses that button that minimises everything, exposing the desktop?  I honestly don't know if *anybody* does.  So you see, it isn't so much (for the average user, for whom the default is instended -- right?) a matter of making things smaller, but making them fewer.  Only the used essentials belong down there: e-mail, browsing, home, etc.  I'm iffy on terminal -- I know that I personally use it, but I'm unsure.  Should the virtual desktops perhaps be disabled by default?  Another point for debate, surely, they could be reduced to a single desktop by default and the panel removed.  And those shutdown/lockscreen buttons...  Personally, I remove them and use the ones that are at the bottom of half of the main menut in KDE3.  I also don't use the klipboard thing, because it was nonsensical and that gibberish that popped up in the corner all the time drove me NUTS.  The striped clock also bothered me, I had to go in and manually set it to a preconfigured setting that matched the rest of the panel.  Why is that not default?\n\nOf course, some of my statements may have been obsoleted by the latest releases, but the point regardless is that the default panel simply needs to be weeded out."
    author: "dingodonkey"
  - subject: "Re: Default Icon Sizes in Kicker"
    date: 2002-07-16
    body: "I agree with almost all of your points.  I've been using Gnome for the past two years, and just made the switch to KDE last week.  I love it!  However, I found the default Kicker config odd.  The first five things to go:\n\n1) Kate button\n2) Desktop show button (who uses that??!?)\n3) Home dir button (it's on the desktop already!)\n4) Pager\n5) Clipboard/calendar thingies\n\nI've never used multiple desktops, and find that pager a waste of launcher bar space.  So I have a couple of program launcher buttons, the tasklist, logout/lock buttons, clock, and weather.  And that's it.  Nice and clean, IMHO.\n\n"
    author: "Stealthboy"
  - subject: "Re: Default Icon Sizes in Kicker"
    date: 2002-07-17
    body: "I prefer to keep at least 65% of the space open on my panel for the taskbar, myself.  Which is what I think most users expect: ample taskbar (I don't know what the KDE types call this, if it's not the same) space.  I've been using KDE since early 1.x, and have configured it this way ever since they merged it into the panel.  But I never stopped to think (until reading this article) that the average Joe Shmoe won't take the time to do that right away, if at all.\n\nPersonally, I also prefer to keep icons off the desktop, but that is purely a personal preference.  As of now, I have only the Trash bin on my desktop -- and would rather have it on the panel.  This is because I'm one of those guys that has 10+ things running at a time, and can never see more than 15% of his desktop.  For that reason, I also prefer my home button on the panel.  Certainly, it does not need to be in two places, though.  It's all a matter of preference.  Myself, I would like to see KDE make the bold move of placing all essentials in the panel, removing all (except maybe trash and devices) from the desktop, and cleaning out the panel.  That would make for a more elegant first impression, which, of course, is the most important thing in attracting new users.\n\nKDE has a lot of features, and that's great -- but they don't all need to be flaunted from the start.  The saturation effect on mouseovers, for example, was wise use of advanced features by default.  It's time now to decide what's most important, what would make the desktop look best to the average new user, for whom the default is intended.  Again, I propose that the KDE team polls first-time users (not experienced ones!) about what they prefer, and make educated decisions based on that.  Not just web-based polls, but real person-on-person interaction.  This is something that I'd be willing to volunteer my time doing.\n\n...But enough of my rambling..."
    author: "dingodonkey"
  - subject: "Re: Default Icon Sizes in Kicker"
    date: 2002-09-04
    body: "I use the show desktop icon!!!!!!!!!!!!!!1\n\nand I use it a lot"
    author: "JOhannes Arnstad"
  - subject: "Re: Default Icon Sizes in Kicker"
    date: 2002-07-18
    body: "I don't think I would've known about the multiple desktops without the pager being on the kicker, so I'd say keep it in the default for sure.  I personally can't stand not having multiple desktops anymore, but I would've never started using them without seeing the feature first.  I also think that the show desktop is something a windows user would expect, and the terminal is something a Linux user just expects to see there, they they should both stay too.  It doesn't take to long for an experienced user to remove the icons they are not using."
    author: "anonomous monkey"
  - subject: "KDE Paperbasket"
    date: 2002-07-17
    body: "A small question. Is it possible to put the trash-icon in KDE onto the panel and then use it from there with all the functionality? Even in real life, I prefer not throwing the trash onto my desk.\n"
    author: "Alex Ivarsson"
  - subject: "KDE needs speed, it's just too slow."
    date: 2002-07-18
    body: "I tested KDE2/KDE3 om a AMD 450 with 384 MB RAM, and it runs too slow to be usable. Windows 98 is faster.\n\nGnome 2's nautilus is much faster, Gnome apps are generally much faster than KDE/qt applications. \n\nThe KDE project should focus on optimizeing the code rather than implementing new features. \n\nI use the Gnome desktop, and I use it to run the KDE apps I need. "
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: KDE needs speed, it's just too slow."
    date: 2002-07-18
    body: "I run KDE (2.2.2 I think) on a laptop (a fujitsu-siemens) with\n266Mhz intel and 96MB ram. It works pretty ok. How frequently do\nyou usually start up new applications? Every second?"
    author: "OI"
  - subject: "Re: KDE needs speed, it's just too slow."
    date: 2002-09-13
    body: "I have a Celeron 300 and my kde runs a WHOLE lot faster than gnome does, but that's still not saying anything, win 9x NEVER ran so slow.\n\nI did a pretty much default install of red hat 7.2 and I've tryed to turn off as many services as I can and I've still got pretty much a dog.\n\nAnyone have any suggestions?\n\nThanks"
    author: "malcolm"
  - subject: "Gnome1 on Celeron 300."
    date: 2002-10-03
    body: "The default RedHat 7.2 Gnome install uses Nautilus1 as the default filemanager. Nautilus1 is the slowest app I ever seen, even Konqueror is way much faster. \n\nOption1:\nInstall Ximian Gnome and use GMC (instant even on 100 MHz) as desktop/filemanger.\n\nOption2:\nInstall the Rox Desktop (works great with Gnome1) from http://rox.sourceforge.net/ - it's also instant.\n\nOption3:\nUpgrade to Gnome2, where Nautilus is way optimized and nearly instant @400MHz."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: KDE needs speed, it's just too slow."
    date: 2004-03-05
    body: "I agree that KDE 3 is very slow and almost unusable on older systems like Pentium II. Win 98 is noticeably faster.\n\nKDE may run smoothly on a beeding-edge machine, but I suggest that the KDE project use slower computer when programming KDE (eg: Pentium II) so they can realize that some part definitely need speed improvement. On a fast machine, it is harder to notice a badly designed algorithm.\n\nThey are still a lot of people using older systems."
    author: "Daniel Lavoie"
  - subject: "KDE needs speed, it's just too slow."
    date: 2002-07-18
    body: "I tested KDE2/KDE3 om a AMD 450 with 384 MB RAM, and it runs too slow to be usable. Windows 98 is faster.\n\nGnome 2's nautilus is much faster, Gnome apps are generally much faster than KDE/qt applications. \n\nThe KDE project should focus on optimizeing the code rather than implementing new features. \n\nI use the Gnome desktop, and I use it to run the KDE apps I need. "
    author: "\u00d8yvind S\u00e6ther"
---
User Interface designer <a href="http://osnews.com/editor.php?editors_id=1">Eugenia Loli-Queru</a> has written <a href="http://osnews.com/story.php?news_id=1347">a thoughtful and well-illustrated article</a> for <a href="http://osnews.com/">OSNews</a> where she discusses which parts of KDE are good and which ones could use some polishing. <i>"What is a good User Interface? Well, in order to answer that, we will have to take into account that different people like different shapes, colors and functionality. This article is just my personal opinion, how I would like KDE to evolve in the future. I am sure that other users would like to see other, different types of evolution. However, we can't deny the fact that some basic rules of UI design should never be ignored."</i>  The developers have started <a href="http://lists.kde.org/?l=kde-core-devel&m=102642696926288&w=2">a discussion</a> (<a href="http://lists.kde.org/?t=102643264300001&r=1&w=2&n=7">thread1</a>, <a href="http://lists.kde.org/?t=102642703000008&r=1&w=2">thread2</a>) based on this article.
<!--break-->
