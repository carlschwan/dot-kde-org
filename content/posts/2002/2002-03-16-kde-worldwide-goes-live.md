---
title: "KDE Worldwide Goes Live"
date:    2002-03-16
authors:
  - "numanee"
slug:    kde-worldwide-goes-live
comments:
  - subject: "nice map"
    date: 2002-03-17
    body: "The map is a nice idea. It could be expanded to include KDE users but just use another color.\n"
    author: "ac"
  - subject: "Re: nice map"
    date: 2002-03-17
    body: "europe is already cluttered enough with little dots, don't you think? =)"
    author: "Aaron J. Seigo"
  - subject: "Re: nice map"
    date: 2002-03-17
    body: "... or a registration system for kde users (name, country, city  etc...)"
    author: "me"
  - subject: "One place per person"
    date: 2002-03-17
    body: "Why is it that this \"anonymous coward\" seams to be living both in sweden and the US. I can only be one place at a time."
    author: "Johsua"
  - subject: "Nice idea but..."
    date: 2002-03-17
    body: "This is a nice idea but when I look at Europe I can hardly read any names there. Too cluttered. Zooming would be nice, and selecting by country would be nice too. Any plans to do that?"
    author: "antialias"
  - subject: "Re: Nice idea but..."
    date: 2002-03-19
    body: "Zooming is for Europe is planned, yes. I just need to find the time to implement it (I'm busy with exams + other stuff at the moment)."
    author: "Chris Howells"
  - subject: "anonymous coward"
    date: 2002-03-18
    body: "Wow, although I'm aware that code speaks more than words, this seems a bit fishy.\n\nThe anonymous cowards should be removed from the map, since they are most likely users just putting themselves on the map for kicks.\n\nThe real developers would not have any qualms about putting their real name (or at least their irc nick) up on the map.\n\nMore specifically, if anonymous cowards are allowed, it's more likely that the map will just reflect people that pass through the site to take a look - and since it's a developer map, they shouldn't be there.\n\nJust my two devalued canadian cents...\n\nTroy\ntroy(at)kde(dot)org"
    author: "Troy Unrau"
  - subject: "Re: anonymous coward"
    date: 2002-03-18
    body: "The anonymous cowards are caused by a bug. Not every \"anonymous coward\" on the map actually wrote that. Some wrote their real name and the script replaced it with \"anonymous coward\".\n\nThere are 3 dots in Sweden. They are at 3 of the most important university cities (Link\u00f6ping, Uppsala and Ume\u00e5). All got converted to \"anonymous coward\"."
    author: "Erik"
  - subject: "Re: anonymous coward"
    date: 2002-03-18
    body: "Actually, I think one of them is me, in K\u00f6ping (not Link\u00f6ping)\n\nI certainly didn't mean to be anonymous :)\n\n"
    author: "Lauri Watts"
  - subject: "Re: anonymous coward"
    date: 2002-03-18
    body: "OK, so then it is Link\u00f6ping, K\u00f6ping and Ume\u00e5. (Zooming would be very nice.)"
    author: "Erik"
  - subject: "Re: anonymous coward"
    date: 2002-03-18
    body: "Well, yeah, since I'm quite some distance from Uppsala!\n\nWas a logical guess, who'd expect a KDEer out here in the middle of nowhere :)\n\n"
    author: "Lauri Watts"
  - subject: "Re: anonymous coward"
    date: 2002-03-19
    body: "> The anonymous cowards are caused by a bug\n\nAnd how did you work that out when you don't know the details of how the map is produced?\n\n> Some wrote their real name and the script replaced it with \"anonymous coward\"\n\nAlmost certainly wrong. The script is an extremely simple bit of perl. If it didn't work for some of them, it wouldn't work for all of them."
    author: "Chris Howells"
  - subject: "Re: anonymous coward"
    date: 2002-03-19
    body: "> And how did you work that out when you\n> don't know the details of how the map is\n> produced?\n\nOne does not have to know the details of a system to identify bugs. It is enough to observe it produceing wrong output.\n\n> Almost certainly wrong. The script is an\n> extremely simple bit of perl. If it didn't\n> work for some of them, it wouldn't work for\n> all of them.\n\nThen how come people got their names converted to \"anonymous coward\" when they asserted afterwards that they did not submit anonymous?"
    author: "Erik"
  - subject: "Re: anonymous coward"
    date: 2002-03-20
    body: ">Then how come people got their names converted to \"anonymous coward\" when\n>they asserted afterwards that they did not submit anonymous?\n\nA slight error in the database (a tab delimtted text file) meant that certain entries didn't have all the necessary fields (probablly caused when I cut and pasted the details from the e-mail). It was interpreting this as meaning that the user wanted to be anonymous. This is fixed now. Thanks for pointing out."
    author: "Chris Howells"
  - subject: "Re: anonymous coward"
    date: 2002-03-20
    body: ">Then how come people got their names converted to \"anonymous coward\" when\n>they asserted afterwards that they did not submit anonymous?\n\nA slight error in the database (a tab delimtted text file) meant that certain entries didn't have all the necessary fields (probablly caused when I cut and pasted the details from the e-mail). It was interpreting this as meaning that the user wanted to be anonymous. This is fixed now. Thanks for pointing out."
    author: "Chris Howells"
  - subject: "Re: anonymous coward"
    date: 2002-03-23
    body: "So will the anonymous cowards be removed so that the people can resubmit now when it works?"
    author: "Erik"
  - subject: "Names?"
    date: 2002-03-18
    body: "Is it possible to implement some sort of a mouseover thingie with javascript and the like?\n\n/me is not a web guy heh\n"
    author: "exa"
  - subject: "Re: Names?"
    date: 2002-03-20
    body: "*cough*javascript*cough*evil*cough*\n\nErm no, probably not. Sorry ;)"
    author: "Chris Howells"
  - subject: "The PSQL Global Development Group's map"
    date: 2002-03-21
    body: "If you want some inspiration while developing the next version of the map, take a look at \"The Global PostgreSQL Development Group\"'s map in http://developer.postgresql.org/index.php\n"
    author: "rabalde"
---
<a href="mailto:howells@kde.org">Chris Howells</a> wrote in to
announce his latest project, <a href="http://worldwide.kde.org/">KDE
Worldwide</a>, to the world.  KDE Worldwide wants to promote KDE
internationally by providing information and to assist those
interested in the <a href="http://i18n.kde.org">localised and translated</a> versions of KDE.  A worldwide <a
href="http://worldwide.kde.org/map/">developer map</a> is currently in the
making -- if you are a contributor/developer, please help complete it by <A
href="http://worldwide.kde.org/map/form.phtml">submitting</a> your
coordinates.  There's a set of relevant <a
href="http://worldwide.kde.org/links/">links</a> and those of you
interested in this project can join the <a
href="http://worldwide.kde.org/mailinglist/">mailing list</a>.  In
particular, Chris is looking to host more international content, including material such as <i>any language</i> tutorials, articles,
screenshots, and content that could help enhance the international
community's appreciation and experience of KDE.
<!--break-->
