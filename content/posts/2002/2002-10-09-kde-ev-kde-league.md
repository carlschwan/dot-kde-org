---
title: "KDE e.V. on the KDE League"
date:    2002-10-09
authors:
  - "mdalheimer"
slug:    kde-ev-kde-league
comments:
  - subject: "Transparency"
    date: 2002-10-08
    body: "Maybe it's not productive but it helps people trusting. Could it be counterproductive to disclose such books ?"
    author: "Anonymous"
  - subject: "Re: Transparency"
    date: 2002-10-09
    body: "The books are disclosed to all KDE e.V members if I understand what Kalle is saying.  If you are a KDE contributer you can apply for membership.  As far as I'm concerned, this is all well and good, and nobody else's business."
    author: "KDE User"
  - subject: "Re: Transparency"
    date: 2002-10-09
    body: "> Maybe it's not productive but it helps people trusting.\n\nTrusting means you don't have to ask.\n\n> Could it be counterproductive to disclose such books ?\n\nYes it could. For one thing, suppose I am a contributor, I might just not want to parade my charitable work around. There are a few people like this. Beyond that I could be a company who does not wish it to be publicly exposed that I contributed because maybe I don't want M$ or other M$ oriented companies to see it and cancel my contracts. For that matter, maybe it could create tension with a contributor's GNOME support. Who knows? Certainly I would be really ticked off and unlikely to pay up if it was posted that I was $10,000 in arrears.\n\nThese are just a few reasons that don't take much thought. Don't you think they would have registered IRS 501 (c)(3) if they wanted to go that route? Possibly the people who put together the league gave it more thought than you have?\n\nAnother thing that is irritating is people asking why it hasn't done more? True it would be more obvious if you looked at the books but huge numbers for the average household are pathetic for the average corporation. It reminds me of the old saying \"We've done so much with so little for so long we can now do anything with nothing forever\". Of course that is tongue in cheek. The only relevent question is have they been operating to the best of their ability under their charter. I think this is being answered. As far as finances go, I often wonder how many people can even put finances like this in perspective instead of just looking at a number and relating it to their finances. That can certainly lead to wild specualtion and irrational expectations as to what could be done with a given sum. Again it's all second guessing and diverting from acomplishing the tasks at hand.\n\nHow much difference does a balance sheet and the persuant arguments make in relation to whether honest and intelligent people are making their best effort to support what they are passionate about? If you want to see the books wave a check for $10,000 in front of them and ask to see. I'm going to assume that core developers and major contributors have more rights in their company than an observer. What exactly is wrong with that?"
    author: "Eric Laffoon"
  - subject: "Re: Transparency"
    date: 2002-10-10
    body: "I see no point in reporting who has donated how much the last 12 months.\n\nBut regular reporting of what KDEe.V. and KDELeague has done (and spent) the last months could build up trust and makes it easier for responsible persons to argue for supporting KDE financially.\n\nMore transparency: Yes. \nDisclosing financial details: Not necessary."
    author: "saildrive"
  - subject: "Contributions."
    date: 2002-10-08
    body: "Why is that people who haven't given any money to the KDE League want to know all about it's books? Just because KDE is open source doesn't mean they have to report their finances to the planet. Would you ask SuSE or Ximian to disclose their finances? They are not public firms...they answer to those who have put money up. If you haven't then I would suggest minding your business."
    author: "Ben Rosenberg"
  - subject: "kde contributors have a right to be"
    date: 2002-10-08
    body: "Perhaps not legally (TBD), but certainly from\na community standpoint.  Take the longstanding\nproblem of not having enough developers and speakers\nat Linux conferences.  What, if anything, has the\nKDE League done to help?"
    author: "interested"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: "Are you are a developer or speaker?  I didn't think so, because if you were, you would know how the KDE League has helped in the past."
    author: "KDE User"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: "I don't think that the more casual developers (perhaps 80% of KDE's current 838 kde-common/accounts in cvs), know about much, if anything the league has done. \n\nLooking at the league's webpage (http://www.kdeleague.org/), in the announcements section, I see exactly ONE press release. The press release is the press release announcing that it was formed. :-)\n\nAnyways, this issue has been brought up before. Practically every post concerning the league in the KDE mailing lists (not many posts :) ) has to do with it's inactivity. \n\nSee these threads:\n\nhttp://lists.kde.org/?l=kde-promo&m=99107057932462&w=2\nhttp://lists.kde.org/?l=kde-devel&m=101359851401548&w=2\nhttp://lists.kde.org/?l=kde-promo&m=99168911205947&w=2\n\nI won't mention recent threads because of dep's article, or threads from when the league was announced."
    author: "fault"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: "Thanks for the links. Informative.\n\n-----------------\nAndreas Pour 2001-06-01 replies to \nEva Brucherseifer :\n\n> But all people tell me is, \"there is no money for making\n> flyers for linux expos, there no money for this, there is no money for that\".\n> What happens to the money. I thought (maybe this was stupid) that KDE League\n> exists in order to provide promotion and promotion material.\n\nThe KDE League has a quite small budget.  Promotion is very expensive. \nWe have had to make some hard decisions about what to try to get into\nthe budget, and many things that are important will not get funded. \nFrom the outset the most important thing was getting media coverage for\nKDE, as generally this is the cheapest and most effective promotional\nmethod, and you can expect this to be the focus of the KDE League.\n\n> Why don't KDE have professional posters and flyers (instead of these\n> home-made not very-colorful 300dpi ink jet prints). Why don't we have CDs to\n> sell? Where are the KDE t-shirts to sell? What about the marketing?\n\nRegarding posters and flyers, the way it looks (we will know more in the\nnext few weeks when the budget meeting ends) the League will have a\nbudget item to support things like this.  However the infrastructure to\nsupport this -- from information on the website to perhaps a form people\ncan use to submit requests -- is not yet in place, and, as I mentioned\nearlier, volunteers to help implement this are welcome.\n\n[...]\n> So, what happened already in the 6 months KDE League exists?\n\nOrganizational things to a large degree; as well as fund-raising. \nUnfortunately we have been hampered by a series of things that have to\ngo in a certain order and each piece takes some time.  Inside the next\nmonth you really should see a lot more activity from the League."
    author: "anonymous"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: "Hi anonymous (nice name ;-),\n\nyou quote my answers from more than one year ago... since then I was involved in the organisation of several events and expos where KDE had a booth. \nToday we have already the second version of very nice high-quality flyers, made with the help of the League and paid by the League. We have T-Shirts (we already had then, but I didn't know), posters, pins and some other stuff. We also have more ideas in our pipe, but time is lacking and the League must become functional again first.\n \nGreetings,\neva"
    author: "Eva Brucherseifer"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: ">Today we have already the second version of very nice high-quality flyers, >made with the help of the League and paid by the League.\n\nBut this is EXACTLY what people is dying to know !\nHell, is it so hard to drop a line in a \"facts\" section on kdeleague.org ?\n\n\"KDE League to provide high-quality KDE flyers\". Period."
    author: "anonymous"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: "Check this out:\nhttp://dot.kde.org/994930585/\nI didn't do a further search on other events, but maybe you'll find more on the dot. Unfortunatly I forgot to mention them for this years Linuxtag :-((\n\nYou'll also find some info in the Promo FAQ at http://events.kde.org \n\n"
    author: "Eva Brucherseifer"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-09
    body: "You probably also should have a look at KDE's release announcements and do a search on \"League\". Here are some:\n\nhttp://www.koffice.org/announcements/announce-1.2.phtml\nhttp://www.kde.org/announcements/announce-3.0.3.html\nhttp://www.kde.org/announcements/announce-3.1beta2.html\n\nThey are done by Andreas Pour in behalf of the League and they are helping KDE a lot. And don't forget: The League is doing promotion for KDE, not for the League itself.\n\nGreetings,\neva"
    author: "Eva Brucherseifer"
  - subject: "Re: kde contributors have a right to be"
    date: 2002-10-22
    body: ">And don't forget: The League is doing promotion for KDE, not for the League itself.\n\nI could kiss ya right now sweetie :D\nThe less we hear about The League and the more KDE news that just magically pops up everywhere the better!\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Contributions."
    date: 2002-10-08
    body: "SuSe and Ximian are _for profit_ and are not related to KDE in any way. Oh, by the way, money is not the only manner to contribute to KDE and if a league wears KDE's name I want it to be transparent and open."
    author: "Anonymous"
  - subject: "Re: Contributions."
    date: 2002-10-08
    body: "I find it amusing someone named \"Anonymous\" asks for transparency.\nDo you feel irony when it bites you?"
    author: "coolo"
  - subject: "Re: Contributions."
    date: 2002-10-09
    body: "Irony only bites when a good point isn't being made, don't you think? He has a point about transparency - but I also see the point that some contributors may not want to disclose their support."
    author: "David"
  - subject: "Re: Contributions."
    date: 2002-10-22
    body: "I would't... no matter if it was just frigging \u00805... at least not without my personal say-so.\n\nHOWEVER i think *The League* should feel free to get an external accountist to \"copy\" the books and substitute all names into, say \"Donater000001\"\n\n/kidcat\n"
    author: "kidcat"
  - subject: "!Required, but transparency should be the goal..."
    date: 2002-10-08
    body: "I do not subscribe to the idea that the KDE League has done anything wrong (except miss a routine filing date), but I still think transparency is the answer here.  \n\nAll of this is a big waist of time that could be easily solved if the league would agree to make it's financial situation more transparent and perhaps explicitly detail the ways in which the league has/will fullfill its charter."
    author: "Adam Treat"
  - subject: "illegal"
    date: 2002-10-08
    body: "The whole *point* of the \"Linux and Main\" article is that is doesn't matter whether you want to open the financial records, or not.\n\nYou have to, by law.  \n\nOne of the joys of being a non-profit organization.\n"
    author: "anon"
  - subject: "not illegal"
    date: 2002-10-08
    body: "> You have to, by law.\n> One of the joys of being a non-profit organization\n\nThis would be true if the KDE League was a nonprofit. It's not. \n\nA nonprofit has tax-exempt status with the IRS, and a lot of responsibilities come along with such a status. The KDE League (as I understand it) is a \"not for profit\" corporation. i.e. it does not make a profit as any excess funds are put back into the League's activites, but for tax purposes it's just another corporation.\n\nIn terms of disclosure of finances, it does not have any responsibilities above any other corporation, so it is not obligated to disclose this information, except perhaps to it's members.\n"
    author: "KDozer"
  - subject: "Re: not illegal"
    date: 2002-10-09
    body: "In the United States, nonprofit and non-for-profit corporations are legally the same thing (check http://www.nonprofits.org/npofaq/01/09.html). \n\nSo, the question is, is the KDE league non-profit (who may or may not have gotten tax excempt status) or a standard corporation?"
    author: "Ert"
  - subject: "That'd make a big change to their bylaws..."
    date: 2002-10-09
    body: "If you are right the League is violating against their own bylaws in section 1.1: \"The name of this non-profit corporation is the International KDE League, Inc.\" (http://www.kdeleague.org/bylaws.php)\nPlease also note that this sentence is actually the only one where the term \"non-profit\" is used regarding to the League's state at all..."
    author: "Datschge"
  - subject: "Re: not illegal"
    date: 2002-10-09
    body: "They are not tax exempt right?  There is no legal basis at all for them to be forced to open their books to hostile forces like you and Anonymous.  If there is, and you know so much about law why don't you sue them and shut up?"
    author: "KDE User"
  - subject: "Re: not illegal"
    date: 2002-10-09
    body: "> They are not tax exempt right? There is no legal basis at all for them to be forced to open their books to hostile forces like you and Anonymous. \n\nDepends on the state. In some states, nonprofit organizations are required to open their books. In some states, this is to donors only; in others, it applies to the general public. "
    author: "fault"
  - subject: "Well..."
    date: 2002-10-09
    body: "As far as I understood the whole thing KDE e.V. is a non-profit organization while KDE League isn't. And the reason why both are separated is that KDE e.V. stays independent from profit seeking companys while any company can contribute to KDE League without having their contributions to be discloused to the public. And if they want to contribute publicly they could still contribute to KDE e.V. directly. \n\nSeems like you could forget KDE League since it's \"only\" a club of supporters who don't want any publicity...?\n\nAnd well, tell me if I'm wrong."
    author: "Datschge"
  - subject: "Re: illegal"
    date: 2002-10-09
    body: "The statement by A. Pour says that:\n(1) KDE League is not a Tax Exempt (\"non-profit') organization for\n    US Federal Tax purposes.\n(2) Apparently for the purposes of Delaware State corporation law,\n    it is \"non-profit\" which apparently means it is not liable\n    for a late filing penalty fee for missing some annual due date.\n\nSo it is *not* required by Federal Law to disclose its accounts to\nthe Public.\n\nSo far, no-one has claimed that Delaware law requires puiblic disclosure.\nPresumably not (?) since Delaware aims to be very\n\"corporation friendly\" to profit from fees paid by corporations\nthat chose to incorporate there.\n\n\n"
    author: "Disinterested  Observer"
  - subject: "Exactly, transparency!"
    date: 2002-10-08
    body: "I'm a minor KDE contributor. Here's my impression of the situation, which roughly agrees with that of everyone else I've discussed this with:\n\n* The KDE League is raising money, supposedly in the tens of thousands of dollars, by trading on KDE's name.\n\n* The KDE League has not visibly accomplished anything besides a prominent contribution to the Microsoft settlement debate. Its web site is stagnant and Kalle admits it's done little recently. That doesn't mean it hasn't done anything but....\n\n* ...whenever any question is raised, Andreas Pour responds by saying that it's all a secret and he can't tell us about all the good things that are being done. Or anything else.\n\nSomething has to give. Either it needs to be clear to the KDE community that the League is doing something useful, or the people in charge need to level about what's going on or they better get used to hearing complaints.\n\nMy guess is that after the Linux gold rush dried up, the League members felt they had better things to do than give Andreas piles of money, Granroth and Schlager got bored and went off to do something else and Andreas was left with a domain name and not much to do. If that's the case -- tell us! Whatever happened -- tell us!\n\nI think we all trust Kalle and I'm glad to see he's admitting there's a problem and trying to whip things into shape. Dennis Powell is an idiot who uses computer journalism as a club to manage his grudges, but if things start to improve from here, credit him for poking the anthill. "
    author: "Otter"
  - subject: "Re: Exactly, transparency!"
    date: 2002-10-09
    body: "i agree about the transparency with regard to the actions of the League. being as opaque as they are, it makes it very difficult for the community to work with, support, or even know how to feel about the League. i don't think that the League has done anything to sully the name of KDE to this point, they just haven't done as much as some may have hoped.\n\nas for finances, the people who put $$ into the KDE League are the ones who have the right to a say in how those dollars are spent. it isn't our money, it's theirs. if they know how it is being spent and are happy with it and it isn't being used to harm KDE, then who are we to complain? until i put some of my own cash into the basket, i have no expectations with regard to the transparency of the finances of the organization.\n\nif the League was operationally transparent it would put some added onus on the League w/regards to taking action and it would be easier to get the community to support it. this in turn would probably make it easier for the League to drum up $$ from members as they could see a direct corelation between investment and return. it would also probably quiet most (if not all) demands for financial transparency."
    author: "Aaron J. Seigo"
  - subject: "Re: Exactly, transparency!"
    date: 2002-10-09
    body: "Very well put.\n\nMy concern isn't to know where every dollar came from and went. Like you said, it's not our business, and like I said, I doubt that money has been mishandled, much more likely that it never came in the first place.\n\nWhat is important is that it be made clear whether the KDE League has done or is doing anything useful. Kalle acknowledges that its performance has been underwhelming and, except for one troll, I don't see anyone here with a positive impression of it. I've come away from this feud with a much clearer understanding of the Delaware bureaucracy and the disclosure regulations for non-profits and not-for-profits than I have of what the KDE League is or does.\n"
    author: "Otter"
  - subject: "Re: Exactly, transparency!"
    date: 2002-10-09
    body: "If you're a KDE contributor than apply for membership to KDE e.V and view the books yourself.  Why should the books be open to hostile forces?"
    author: "KDE User"
  - subject: "Re: Exactly, transparency!"
    date: 2002-10-09
    body: "What does the KDE e.V have to do with the league, other than possibly being a booster/hinderence, as Kalle said."
    author: "fault"
  - subject: "Re: Exactly, transparency!"
    date: 2002-10-09
    body: "Read the article instead of shooting blinding please."
    author: "KDE User"
  - subject: "Re: Exactly, transparency!"
    date: 2002-10-09
    body: "> Read the article instead of shooting blinding please.\n\nI already did, thank you. ;-)\n\nAnyways, to elaborate a bit, the KDE e.V, while having influence (3 and 6 Board seats) in the League, is in the process of picking a person for the league. \n\nSo, instead, one would have to pay $5,000/$10,000 (CE Membership) or $1,000/$2,000 (CA Membership). I sincerily doubt that anyone cares that much to pay that much money. \n\nI don't think it's such a big deal anyways; as long as someone from the league speaks up about what the league is exactly doing. The league might be doing wonders for the promoting of KDE; the public just wouldn't know and articles like dep's would be given credibility (what's happening now).\n"
    author: "fault"
  - subject: "What Otter said."
    date: 2002-10-09
    body: "I agree completely with Otter. I am also a minor KDE contributor (maybe 2500 lines of code in a KDE app) and my only feelings are that anything KDE does should be completely transparent. I am sure Andreas is a good man but he has 36 thousand reasons to maintain the status quo. In related news, I thought all bitching by Tink et al about RH's Bluecurve was ridiculous. KDE as a project seems to lack confidence in it's own offering. We also need to cooperate more with gnomers.\n"
    author: "CameraShy"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "> I agree completely with Otter.\n\nSame\n\n> I am also a minor KDE contributor...\n\nSame\n\n> and my only feelings are that anything KDE does should be completely transparent.\n\nSame\n\n>  I am sure Andreas is a good man but he has 36 thousand reasons to maintain the status quo.\n\nYup, dre has done many things good for KDE, but I think there is a feeling that the KDE league hasn't really ever done much. This feeling existed before dep's articles.\n\n> In related news, I thought all bitching by Tink et al about RH's Bluecurve was ridiculous. KDE as a project seems to lack confidence in it's own offering.\n\nYeah, it seems to be RH-bashing. Other distros have done it too.\n\n> We also need to cooperate more with gnomers.\n\nYup, although this is a gradual process."
    author: "fault"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "> I thought all bitching by Tink et al about RH's Bluecurve was ridiculous.\n\nA) it wasn't primarily about bluecurve (the theme)\nB) WTF does this have to do with the League? or: why are you bringing this up yet again outside of trying to start an unecessary flame war? it's been discussed to death already. the horse is dead. stop kicking it.\n\n> KDE as a project seems to lack confidence in it's own offering.\n\nhow do you define \"KDE as a project\"? do you mean those who create KDE? if so, I think there is a healthy amount of confidence in what KDE is. it isn't to the point of blind pompousness, but there deffinitely is pride and confidence among those who create KDE, in my experience.\n\n> We also need to cooperate more with gnomers\n\nin which regard? many technologies are shared, and many more will be. there won't be cooperation on some things due to differences in technological vision and that is to be expected. however, interop continues to move forward.\n\nthat said, i'm still waiting (as are others, some of them GNOME devs) to hear back from some key GNOME devs on HIG issues. we had started down a pathway of cooperative efforts on that topic, but it has apparently been ignored by those who would be able to give it the go ahead in the GNOME camp. perhaps they are stuck for time right now, or don't care, or missed the many emails (many from their own devs) on the topic, i don't know. this just goes to show that sometimes cooperation is attempted and it doesn't work out immediately. this doesn't mean there is a lack of interest or effort, though. just that sometimes things take time or aren't shared equally by both as important.\n\nbtw, understand that the RH thing was not about GNOME/KDE relations. so if this wasn't simply yet another random comment meant to spawn flames and was actually meant to cohere with your Tink vs. RH \"observations\", you're rather off-base."
    author: "Aaron J. Seigo"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: ">>>>>>\nI agree completely with Otter.... my only feelings are that anything KDE does should be completely transparent.\n<<<<<<\n\nI'm actually not arguing for that as a general principle. If, say, core developers discuss issues and make decisions in private -- AFAIC, they've earned our trust enough to be able to do that.\n\nIf the KDE League had a track record of success, or at least outward signs of activity, I couldn't care less about how they're spending  their money. But they don't have a track record that makes, \"I can't tell you that!\" an acceptable answer for everything."
    author: "Otter"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "Ahem, funny how you clamor for transparency and then lie when throwing around people's names, claiming that one particular person whom I respect deeply was \"bitching\" about BlueCurve. I challenge you to find documentation of that. And please don't tell me that technical and artistic criticism is unwrarranted. I'll, for instance, criticize the BlueCurve QT style for what it is - software released with plainly visible bugs and thus obviously w/o proper testing, which has caused me to loose all faith in RH Q&A\n\n"
    author: "SadEagle"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "Why are KDE folks so reactionary? You've lost ALL faith in RH Q&A? I mean, come ON. \n\nI think you'll find in the broader world few people buy this KDE garbarge vis a vis Redhat and their myrid failures. Look at the reviews and story write-ups in more mainstream venues. Many people find RedHat's Q&A'ed distro easier and more reliable to use than rolling their own, and a good number find the work RH in terms of Q&A valuable. \n\nJust so KDE folks are very clear on this point, people such as Alan Cox are employed by RedHat, and in Alan's case a good portion of his job is to Q&A other people's patches. I find that Q&A work helpful, and have more faith in folks like Alan then I do in someone like SadEagle.\n\nTake care. "
    author: "Anonymous"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "Why are we so reactionary? How long have you followed KDE? Do you understand how open source should work, and how it currently *IS* working. Redhat:\n\na.) Doesn't give a damn about us, and has proven it time again. Granted they lent us the machine at the Linux World Expo in CA (2002), it didn't even startup when we got it. We ended up having to call them over to fix it, that made a lot of us really nervous because that was suppose to be our main machine, and we already had people at our booth, but I'll let them off the hook on that one.\n\nb.) Employs like 2 KDE people, compared to like a LOT more Gnome developers\n\nc.) Pretend to ignore us, as if we're some linux-product-from-the-1990's-when-no-one-cared-about-linux.\n\nI could go on and on, but I really shouldn't have to. People claim that a lot of the KDE people are mad at Redhat because they are successful, and they are gravely mistaken. We (KDE Developers) don't have our own companies, so why do we care if they're successful? As obsurd as some people think this claim is, Redhat will turn into the Microsoft of Linux, and it's not JUST because they are successful. Their business practices are getting worse by the minute.\n\nAlso, some people say that SuSE is just as bad since their Gnome support is just as bad as Redhat's KDE support. Hah! At least SuSE packages their Gnome right. At least they don't cripple it and remove credit's towards KDE developers. At least they don't replace Konqueror with Mozilla. KDE is an INTEGRATED desktop. All or none IMO.\n\nWe're talking about REDHAT Q&A, not the Q&A preformed by employees employed by Redhat. It wouldn't even suprize me if the OFFICIAL Q&A department didn't spend more than 5 minutes testing KDE."
    author: "Nick B"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "> Also, some people say that SuSE is just as bad since their Gnome support is just as bad as Redhat's KDE support. Hah! At least SuSE packages their Gnome right. \n\n\"right\" is quite subjective. SuSE packages GNOME as well as Redhat packaged KDE previously.\n\n>  b.) Employs like 2 KDE people, compared to like a LOT more Gnome developers\n\nThe reverse is true with SuSE and GNOME, however, I don't see any SuSE-hate from the GNOME community.\n\n\n> c.) Pretend to ignore us, as if we're some linux-product-from-the-1990's-when-no-one-cared-about-linux.\n\nThe reverse is true about SuSE and GNOME.\n\n> At least they don't cripple it and remove credit's towards KDE developers. \n\n1. How did they cripple it?\n2. About boxes are still there, which has credit for developers.\n\n> At least they don't replace Konqueror with Mozilla. \n\nKonqueror is not just a web browser. They put Mozilla in kicker because they beleive that it can display more pages properly, which I personally agree with. They aren't doing stupid things like replacing Konqueror with Nautilus, for example. Anyways, they did the same things to Gnome.\n\n> KDE is an INTEGRATED desktop.  All or none IMO.\n\nThe key thing is \"IMO\"\n\n> Their business practices are getting worse by the minute.\n\nHow so? Remember that Redhat employs a very large amount of developers that have worked on open source software (mostly non-KDE, non-GNOME related). A friend of mine was hired by RH last week, and he brought his mandrake-running laptop with him :-)"
    author: "fault"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "The application about boxes do *not* credit all developers. \nThey do not credit proofreaders,\nThey do not credit consistency checkers,\nThey do not credit developers who contributed \"merely\" bugfixes,\nThey do not credit the developers of the libraries, without which \nthe application simply wouldn't do much.\n\nThe About KDE box does all that. And please don't tell me about token additions of the same entry 4-levels deep in some panel preference menu.\n\n\nAnd well, \"cripple\" isn't a good term, I agree. It's emotional, imprecise, and charged. I think \"reduce stability of\" is better. \n\nAnd frankly, your claim that *many* people *hate* RedHat is rather short of substantiation. Sure, there are some, but I don't see that beyond 2 or 3 people at most. I, for one, am simply sad to see a company that has done a lot of great work decide to play a visionary of the desktop (which is great), and forget about all the grimey parts of the work. Like testing, bugfixing, freezes, responsible release schedules, etc. And heck, of course I'll not be of a very favorable opinion of them when I see other distros's packagers spend hours doing the dirty and hard work fixing small and large bugs, while all they do is introduce new ones for dubious gains. Because ultimately, it's the maintenance that's most of development. \n\nBTW, like many people, you also assume that people don't critize other distrubitions. They sure do, just some so-called \"journalists\" don't make a big deal of that.\n"
    author: "Sad Eagle"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: "> The application about boxes do *not* credit all developers. \n\nYeah, I see your point.\n\n> And frankly, your claim that *many* people *hate* RedHat is rather short of substantiation\n\nWell, I've never said this.\n\n> forget about all the grimey parts of the work. Like testing, bugfixing, freezes, responsible release schedules, etc\n\nYes, indeed. Of course, rh seems to have a thing with unstable X.0 releases :/\n\n> BTW, like many people, you also assume that people don't critize other distrubitions. \n\nOf course people criticize other distros. However, there is a history between KDE and Redhat that makes little things become emotionally charged."
    author: "fault"
  - subject: "Re: What Otter said."
    date: 2002-10-09
    body: ">>> Well, I've never said this.\n\nOuch, sorry. My apologees, it was someone else and I mis-attributed it to you. \n\n\n"
    author: "Sad Eagle"
  - subject: "For those not following.."
    date: 2002-10-09
    body: "This all started with dishonest allegations that there was inpropriety going on behind the scenes of KDE League.  Lie after lie have been exposed in the articles of dep, but all of a sudden people think he is a martyr and are taking the chance to attack KDE.\n\nThis is very sad.  Very very sad indeed.  Who knew there were so many jackals out there."
    author: "KDE User"
  - subject: "Re: For those not following.."
    date: 2002-10-09
    body: "> Who knew there were so many jackals\n\nmore users, more jackals :)\n\nI hope kde developpers are not too emotional.\n\nthank for kde 3.1b2, beautiful job !\n"
    author: "thierry"
  - subject: "League fails to market...itself"
    date: 2002-10-09
    body: "Its ironic that the KDE League has failed to present\na good public image of itself by:\n\n- not disclosing information about its inner workings,\ne.g. finances\n\n- not publicizing any of its accomplishments\n\nNow the latest irony - criticism of the league,\nenabled by the facts above, actually does PR damage\nto KDE itself.  \n\nIt's an unfortunate set of circumstances,\nto say the least.  I hope credibility can be\nre-established, both inside and outside the\ncommunity, and that the League will be able to\ndo more _visible_ good for the KDE project in\nthe future.  "
    author: "steve"
  - subject: "\"At this point and with the current knowledge...\""
    date: 2002-10-09
    body: "> The Board of KDE e.V. has at this point and with\n> the current knowledge absolutely no reason to believe\n> that there are any irregularities in the bookkeeping\n> of the KDE League.\n\nInteresting phrasing.  Does this mean that the Board of KDE e.V. \nhas gone over the KDE League's financials and sees nothing wrong,\nor that the board hasn't seen the League's financials yet?\n\ncheers,\nCharles\n\n[Disclosure: I'm a Gnome programmer who likes both Gnome and KDE. \n No disrespect to KDE or KDE programmers is intended in this message.]"
    author: "Charles Kerr"
  - subject: "Re: \"At this point and with the current knowledge...\""
    date: 2002-10-09
    body: "I'd like to encourage you to develop for KDE!  I think you will be pleasantly surprised by how easy it is, and how clean the API is.  Don't let the C++ scare you away, it's well worth it."
    author: "KDE User"
  - subject: "Re: \"At this point and with the current knowledge."
    date: 2002-10-09
    body: "> I think you will be pleasantly surprised by how easy it is, and how clean the API is. \n\nYou could say the same things about GNOME/gtk, you know :-)\n\nIt's a wonder that the free software community has produced two great desktops."
    author: "fault"
  - subject: "Re: \"At this point and with the current knowledge."
    date: 2002-10-09
    body: "I disagree, I feel the GNOME/gtk API is splintered and messy but this is not a topic for this site."
    author: "KDE User"
  - subject: "Re: \"At this point and with the current knowledge."
    date: 2002-10-09
    body: "> but this is not a topic for this site.\n\nAgreed. It's all about opinions, after all, KDE User."
    author: "fault"
  - subject: "What Did the League Do?"
    date: 2002-10-09
    body: "The heck with the money. I wish dre would just tell us what the KDE League has been doing the past two years to promote KDE.  Or if it hasn't been doing anything, would he just tell us why.  I think a good explanation would satisfy most of the people who are complaining. As long as he won't talk about that, it doesn't matter what he says on other topics like not-for-profit status, many people are going to be unhappy.  But if he explained what the KDE League has been doing, that would settle for most people."
    author: "Les Brunswick"
  - subject: "The Bottom Line"
    date: 2002-10-09
    body: "First and foremost,\n\nI love KDE.  It's a wonderful desktop, and genuinely filled with wonderful people.\n\nI also have wanted to contribute (financially) to KDE since I'm not a programmer and can't really contribute code-wise.\n\nNow,\n\nWhy haven't I?\n\nContribute to the KDE League?  Hell no.  Since its inception, I have never been educated on (a) what it does, (b) what it's purpose is, (c) what its plans are, and more importantly (d) what is has *done*.\n\nContribute to KDE e.V.?  HOW?  And I pose the same questions for KDE e.V. as I do for the KDE League (above).\n\nI work hard for my money, and am fairly philanthropic when I can be.  The American Red Cross, The American Cancer Society, The Dianne Fossey Foundation, the Democratic National Committee, and National Public Radio all get the majority of my donations each year.\n\nFor open source, I have (and do) contribute substantially to the Perl Foundation, dyndns.org, and to FreeBSD.\n\nBut why haven't I written one check to the KDE League or KDE e.V.?  Because, unlike all the other organizations that I've mentioned above, neither of the two satisfies any of my criteria.\n\nI'm not in the habit of willfully throwing my money away, and it doesn't matter is an organization is 501(c)(3) or not.\n\nIn my eyes, the KDE League is nothing more than a clique of KDE Developers who formed a group to raise money for themselves.  Open your god-damned books if you want my money, damnit.  I don't want to know a list of contributors, but an un-audited financial statement, done in accordance GAAP will more than suffice.  I just want to make sure I'm not donating $500 to an organization that spends 80% of its revenue on the salaries for the freakin' board.\n\n--AKU"
    author: "Another KDE User"
  - subject: "Re: The Bottom Line"
    date: 2002-10-09
    body: "Use http://www.kdeleague.org/contribute.php to contrib to KDE e.V (as well as the league)."
    author: "fault"
  - subject: "Good Point!"
    date: 2002-10-09
    body: "No question.\n\nI work in the non-profit world, and we see a HUGE number of abuses in this area, the organizations with constant exuses usually turn out to have some ugly laundry. \n\nThey use their money to promote themselves, hire folks who work with them on other projects, subsidize offices they use for other business and the list goes on. \n\nOne sees these issues even with established groups like the United Way and the Red Cross. \n\nThe common excuse of privacy of our donors is also surprising, since anyone who has taken Accounting realizes you can disclose a basic set of financial statments without destroying donor confidentiality. It's even possible to file a form 990 which avoids exposing anyone, and a 990 is a lot more than anyone is asking.\n\nSuch a shame that KDE is associated with this type of crummy behavior. "
    author: "Anonymous"
  - subject: "Re: The Bottom Line"
    date: 2002-10-09
    body: "I agree with you that we don't know what the KDE League and the KDE ev have done in the past, and that I wouldn't put my money in here to support KDE.\n\nHowever, I completely disagree with you when you say:\n\"In my eyes, the KDE League is nothing more than a clique of KDE Developers who formed a group to raise money for themselves.\"\n\nI don't think there are many KDE developers inside the KDE League. It seem to have been formed to provide a way of promoting KDE by very big companies. I dislike their lack of transparency on their actions, but they never pretented that they would be transparent. And more importantely, nobody ever asked _you_ to put money in.\n\nNow the KDE ev seems to be shaping good. So if you want to support KDE financially, it looks like a right place to me.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: The Bottom Line"
    date: 2002-10-09
    body: "> In my eyes, the KDE League is nothing more than a clique of KDE Developers\n> who formed a group to raise money for themselves.\n\nOh! This is an outrageous accuse and I have no respect for somebody throwing mud like this. The only KDE developers that are directly connected with the league are those appointed by the KDE e.V. (thus by the KDE community) to veto on the KDE League board about how KDE is promoted using League's material means.\n\nThe league was created at the initiative of KDE-connected businesses, in order to offer a practical way to promote KDE. There's nothing in there related to KDE development. The league's only role is to _promote_ KDE, nothing more. No sponsorship for development, no luxury jets or cars for onerous developers, nothing else than PR!\n\nI personally don't care if you contribute to KDE with code, money, sheer friendship or you don't. I don't even care if you hate KDE as a concept. But you have no right in this world to throw mud at me.\n\nSheesh! This world is so full of crap!"
    author: "Inorog"
  - subject: "Re: The Bottom Line"
    date: 2002-10-09
    body: "If KDE League is all about promotion and has done no wrongdoing, why is it so hard for them to disclose what they've spent the money on?  That can be done without disclosing who the donors of that money are.  Why, when asked about it, does the KDE League basically come out with a \"no comment\"?  If they aren't paying large amounts to executives, then what part of your expenses can't be exposed?  Even a hint as to why things must be kept secret would be nice.  If all of the money really did go to promotion of KDE (through pamphlets, events, booths, etc), then just say so instead of saying \"none of your business\".  \n\nSince most people don't even know what the KDE League does, it would be a positive PR move to educate the community about what the League really does.  The mud flinging and bad PR articles would disappear really fast if there was nothing left to speculate about.  To most people right now, it appears that the only publicity the League has done for KDE is through their mention in the Linux and Main articles (and no, any publicity isn't good publicity in this case).  If the KDE League is really about good PR for KDE, then they'll do their job and fix this mess, whether the mess is their fault or not."
    author: "anon"
  - subject: "What could the League do?"
    date: 2002-10-09
    body: "There has been alot of talkas about what the League does and what it could do. I agree that we need more information regarding it's activities and plans for the future.\n\nI think that besides promoting KDE in trade-shows and the like, couldn't the League actually sponsor KDE-developers financially? Take for example Mosfet. He had to cut down his work on Linux because he needs a job that pays his bills. What if League sponsored him (and other developers) so that he could work on KDE full-time?"
    author: "Janne"
  - subject: "Re: What could the League do?"
    date: 2002-10-10
    body: "I doubt there is enough money to pay developers. And Mosfet is the most unsteady one."
    author: "Anonymous"
  - subject: "Professor"
    date: 2002-10-09
    body: "Well if the kde league was supposed to get good press for kde, and only succeeded in getting bad press for itself, it certainly has been a failure. The PR situation for KDE is rather disastrous at the moment. apps.kde.org has been down for a long time. linuxtoday no longer reports kde apps, only gnome apps.\nKDE for all practical purposes just lost one distribution, Redhat. They cannot come out and say outright that they are dropping kde because they have too many customers using it, but the purpose is completely clear, witness that the only KDE-developer  they had left in protest."
    author: "Erik K. Pedersen"
  - subject: "Re: Professor"
    date: 2002-10-09
    body: "Red Hat HASN'T dropped KDE and it's NOT their intention nor is it in their interest to do so. OSNews.com have some pretty good articles about this. IIRC Havoc Pennington also explained on people.redhat.com why they made the changes they made to KDE.\nSure they made really dumb mistakes, but they are not out to get KDE.\nIn my mind, they do a better job with KDE than SuSE and Mandrake do with Gnome."
    author: "Fabrice Colin"
  - subject: "What about broken laws?"
    date: 2002-10-09
    body: "These \"sites with questionable reputation\" have made the following points, that are not being addressed by the above statement:\n\n- the KDE League is legally non-existent because it has missed to pay some fee\n\n- the KDE League as a non-profit organziation seems to be obliged by law to open their books to ANYONE who wants to have insight. (additionally, there seems to be no legal difference between \"non-profit\" or \"not-for-profit\"!)\n\nCould anyone with credibility please make a final statement on that?\n\nAt last, my personal question:\nDuring the period of \"dysfunctionality\", has any of the League's money been spent?\n\nBest regards,\nHeiko\n\n\n"
    author: "Heiko Stoermer"
  - subject: "Re: What about broken laws?"
    date: 2002-10-09
    body: "See Dre's responce where he addresses (and generally settles/appropriately dismisses) all of these:\n\nhttp://marc.theaimsgroup.com/?l=kde-cafe&m=103409240927911&w=2"
    author: "Scott Wheeler"
  - subject: "Re: What about broken laws?"
    date: 2002-10-10
    body: "Do I have credibility?  I don't know.\n\nBut, there are no laws broken.\n\nI do have some experience as I was treasurer of a small 501(c)(3) corporation for 3 years.\n\nIf you are a non-profit corporation in Arizona, you need to fill out a one page form and send them (then) $10.00 each year.\n\nIt is my understanding that it is similar with Deleware.  This isn't a big deal and they had already filed it (late).  When this was cleared up, their status as a corporation was restored.\n\nThere is no proper legal definitions for: \"non-profit\" and \"not-for-profit\" but there is a clear difference between \"non-profit\" and \"tax-exempt\".\n\nA ordinary (non-tax-exempt) non-profit coproration is about the same as a for-profit corporation except that they were not organized for the purpose of making a profit to be distributed to their shareholders.  They have no reporting requirements other than as stated above and they must pay State and Federal corporation tax.\n\nThen there are tax-exempt corporations that qualify under IRS 501(c).  Such corporations are exempt from State and Federal taxes except for tax on \"unrelated business income\".  \n\nThe other reporting requirements apply only to tax-exempt non-profit corporations.\n\nIn addition, 501(c)(3) corporations (like the Free Sofware Foundation) are considered to be \"public charities\".  Contributions to such corporations are tax deductible.  They must provide financial information which is publicly available.\n\nBut, since the KDE League is not tax-exempt they have the same public reporting requirements as a for-profit closed corporation.  Which is NONE.\n\nThere is one small question that I don't know the answer to.  Is the anual filing with the State public information?\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What about broken laws?"
    date: 2003-03-07
    body: "please sent me these articles"
    author: "Rob Cradle"
  - subject: "take the fight to the enemy "
    date: 2002-10-09
    body: "It's true what Erik K. Pedersen says. To the outside world it looks like things are coming appart here, Redhat is following the same old trick of software firms .... Embrace, extend, extinguish in order to make crappy Gnome the dominant desktop. \n\nOne thing that I've got to say is that linuxandmain.com is a pile of shit ....\nthey hardly scratch themselves to put up a story unless it's one that's badmouthing KDE. \n\nIt's funny the way people react though, everyone would prefer to sulk and bitch \nrather than take the initiative ...  Why don't we create Redhat rpms that work \nthe way KDE is supposed to ! Give Redhat the 2 fingered salute that the  wholeheartedly deserve and stop KDE from being a second class citizen !!!!!\n\nKindest Regards\n\nBryan\n\n\n"
    author: "bryan"
  - subject: "Overblown"
    date: 2002-10-09
    body: "This whole controversy du'jour is getting out of control. People are trying to build ski-slopes on molehills. Fact: KDE League appears to be dysfunctional. So what? They don't have any of your or my money. Fact: KDE League was late in one filing. So what? I was one day late with my electric bill yesterday."
    author: "David Johnson"
  - subject: "Re: Overblown"
    date: 2002-10-10
    body: "Not so!  The KDE League has funneled money to its dysfunctional organization.  Now if The KDE League didn't exist, that money would have flown into the KDE Project by setting up something like the GNOME Foundation.  The GNOME Foundation has transparent discussions and decisions, and has elected officials.  It also arguably enriches GNOME by having an area where major contributors to GNOME can meet and work towards similar goals for the project.  KDE needs an organization like this.  Not one that takes thousands of dollars away from developers of KDE."
    author: "chris felton"
  - subject: "Re: Overblown"
    date: 2002-10-10
    body: "I demand you publish the books of the GNOME foundation immediately.  I call your bluff.  The GNOME foundation discussion is nothing but a bunch of crock, you won't have me believe this is really what happens there?  Have you even read those \"discussions\"?  I didn't think so.  You have a selective memory as to how the KDE League spent the money, the KDE developers don't."
    author: "ac"
  - subject: "Re: Overblown"
    date: 2002-10-11
    body: "Why don't you.  Quote from GNOME Foundation\n\n\"Love GNOME? Want to give back to the community of mostly volunteer developers who have worked so hard to make GNOME the powerful, flexible, friendly, fun desktop that it is? Don't miss out on this unique opportunity to be part of GNOME. Becoming a Friend of GNOME is easy.\n\nBecome a Friend of GNOME. Your donation goes to the GNOME Foundation, helping provide development, education and promotion for GNOME worldwide. Events such as the GNOME Summit are supported through the generosity of our Friends of GNOME.\n\nGNOME Foundation is a tax-exempt, non-profit 501(c)(3) organization and all donations are tax-deductible in the USA\"\n"
    author: "chris felton"
  - subject: "Re: Overblown"
    date: 2002-10-11
    body: "> The GNOME foundation discussion is nothing but a bunch of crock, you won't have me believe this is really what happens there?\n\nI don't think so-- the GNOME foundation has worked pretty well for it's lifetime. \n\n\nBut anyways, the grandparent poster was wrong; a gnome foundation for KDE would probably not be a good thing. KDE is much more decentralized than that, and it works well. GNOME has a steering committee in the GNOME foundation, and it works well for them. \n\nThe grandparent poster was also wrong in comparing the KDE league with the GNOME foundation. The GNOME Foundation pretty much works as a controller of GNOME _and_ the equivalent of KDE e.V. The KDE league, on the other hand, is mostly a buisness league and has no buisness (heh) in the development of KDE itself. Most discussions in the KDE League would be about finance related issues, while most discussions in the GNOME Foundation are about development and the direction where GNOME is going (read the minutes of the GNOME Foundation).\n\nAnyways, I was a doubter about the effectiveness of the KDE league up until a few days ago. It _has_ done many things, and although somewhat inactive the last few months, is still important. It could, however, issue more press releases about what it's actually doing. Shameless self-promotion is never bad for corporations; it might even get the league more sponsors."
    author: "fault"
  - subject: "This story has still not been cleared up."
    date: 2002-10-10
    body: "The KDE League bylaws http://www.kdeleague.org/bylaws.php clearly state that it is a nonprofit, and the article makes the case that it is not a 501(c)(3) organization. Is it, then, a 501(c)(6) organization (a business league)? \n\nIf so, there are, indeed, public disclosure requirements imposed by the IRS. See http://www.irs.gov/charities/article/0,,id=96103,00.html and read the last paragraph. Yes, this page applies to all nonprofits, including business leagues, as http://www.irs.gov/charities/business/index.html makes clear. If, in fact, the KDE League is a \"business league\", Dennis Powell (no matter how much of a jerk you or I might think he is, and believe me, I'm not a fan) was within his legal rights to ask for disclosures. He is not be entitled to the full books, but he is entitled to \"the last three annual information returns\". \n\nIf the KDE league is not a 501(c)(6) either, then I don't see how it can be a legal nonprofit at all, in which case they owe Delaware filing fees that haven't been paid. \n\nYou can't just say that you're an ordinary corporation that doesn't expect to make money. With such a status you have to pay filing fees to the state of Delaware, and Delaware is now saying that the league doesn't owe them. So which is it? Either KDE League has to pay Delaware or they have to give Dennis Powell their annual information report. One or the other. \n\nIf I'm wrong, then it must be the case that the KDE League has some alternate legal status that I'm not familiar with. If so, what is it? \n\nI'm not saying this to attack KDE. KDE and the KDE League are distinct entities, and I'm not seeing any evidence that the KDE League is serving the interests of KDE's developers or users. Any responses should leave the personality or beliefs of Dennis Powell out, as they are irrelevant. "
    author: "Sir Bard"
  - subject: "cleared"
    date: 2002-10-11
    body: "It has been cleared, read slashdot.  DEP is pretty humiliated right now! :-))"
    author: "anonymous"
  - subject: "Re: cleared"
    date: 2002-10-15
    body: "It's far from clear."
    author: "Anonymous"
---
There have been rumours recently about the financial situation of the KDE League, as well as criticism of its apparent inactivity.  With KDE e.V., the KDE developers' organisational body which controls the KDE League, being recently resurrected, we will now be able to blow new life into the KDE League. [<strong>Ed</strong>:  <em>Matthias Kalle Dalheimer was recently elected a KDE e.V. board member and the KDE e.V. President.</em>]
<!--break-->
<p />
<hr size="1" noshade="noshade" width="95%" />
<p />
There has recently been gossip about the working and financial situation of 
the KDE League. "News" sites with questionable reputation have run stories 
about financial irregularities in the KDE League that entirely lack in 
foundation. This journalistically irresponsible behavior is endangering the 
reputation of both the KDE League as well as KDE e.V. and the KDE project as 
a whole.
<p />
The Board of KDE e.V. has at this point and with the current knowledge 
absolutely no reason to believe that there are any irregularities in the 
bookkeeping of the KDE League. The Board of KDE e.V. does not believe it is 
productive to disclose the books of the KDE League to the general public, but 
is of course willing to authorize the KDE League to disclose the books to 
elected KDE e.V. members (see below).
<p />
The Board of KDE e.V. acknowledges that the KDE League has been mostly 
dysfunctional the last few months. This is partly due to lack of enthusiasm 
on the part of the KDE League members, partly due to KDE e.V. having been 
dysfunctional itself (since KDE e.V. has a huge influence in the KDE League, 
it can, willingly or non-willingly, block the work of the KDE League).
<p />
The members of KDE e.V. are currently determining who will represent them on the Board of the KDE League so that a budget for the KDE League can be passed and the KDE League can take up its work again.







