---
title: "KDE Konquers Korea"
date:    2002-01-15
authors:
  - "Dre"
slug:    kde-konquers-korea
comments:
  - subject: "RE: KDE Konquers Korea"
    date: 2002-01-15
    body: "IMO, this is a good thing (TM), since it bring Linux (and KDE) to the people.\nHopefully, others will follow (German Bundestag)\n:)"
    author: "bg"
  - subject: "Condradulations Hancom and Shawn"
    date: 2002-01-15
    body: "Way to go Hancom and theKompany I knew your products would help bring KDE to the masses.\n\nCraig"
    author: "Craig"
  - subject: "closed file formats?"
    date: 2002-01-15
    body: "Hi Shawn,\n\non Linuxtoday you did not answer the question if the file formats of Hancom office are closed - as they are in M$-Office.\n\nHow is it?\n\ncheers,\n\nmarty"
    author: "ac"
  - subject: "Re: closed file formats?"
    date: 2002-01-15
    body: "I think at least the Kivio one will be open.\n\nIt would be great to have filters to convert between Handcom and KOffice.  I am still convinced that both can live together happily ever after.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: closed file formats?"
    date: 2002-01-16
    body: "Well... \nSee this Dot quotation :\n\nQuestion :\n\n>Re: KDE Studio Gold \n>by Carbon on Thursday January 25, @11:13PM \n>\n>Er, what I meant by what do you think of k3studio, I meant it's flowcharting \n>capabilities compared to kivios, and what you think of it's recently added \n>ability to import kivio stencils.\n\nAnswer of theKompany's president :\n\n>Re: KDE Studio Gold \n>by Shawn Gordon on Thursday January 25, @11:33PM \n>\n>Wow, didn't even notice they had put either one of those features in. Too bad \n>they wasted the time with the Kivio stencils since that isn't the final \n>format and the data is copywritten.\n\nNot really the friendly remark.\nOTOH, - I hope this is not too much of a taboo to say it - all commercial Kivio editions as well as KDEStudios are still GPLed software (merely because *was* GPLed is a wording that GPL denies).\nSo even if theKompany seems to feel very uncomfortable now with the GPL, they may consider importation/exportation to KOffice as a logical consequence of things past ?... I don't know."
    author: "Germain"
  - subject: "..."
    date: 2002-01-15
    body: "Good news !\nMassive governments adoption is definetly Linux's best chance... \nThey simply hate to rely on Microsoft... \n\nFrance is also pretty close to make the move (foreign affairs ministry as already jumped). It was stated several time by government representatives that Free software's philosophy was \"very close\" to the republic's ideals (liberty, equality, fraternity).\n\nIn the vast majority of countries, if you bring free software in the government, you bring it at school. And if you bring it at school, you most certainly bring it at home.\nThus if free software is gaining approval of it's desktop environments and office applications, the next step is obviously somewhere near http://edu.kde.org... \nThat is : good and numerous educational software."
    author: "Germain"
  - subject: "Seems a done deal"
    date: 2002-01-15
    body: "The key word here is that the government _has_ made the purchase. According to the press release, it sounds like the contract has been signed.\n\n120,000 seats may be small compared to some of the loftier Linux installations always rumored to be in the works in Mexico, but 120,000 birds in the hand are worth a few million in the bush. ;-)"
    author: "Otter"
  - subject: "Wonderful"
    date: 2002-01-15
    body: "At least in Korea, this may be the proverbial spark that will lighten the Linux revolution (ahem, excuse the pathos *sniff*). Seriously, this is probably the biggest deal for Linux since IBM joined the boat. What is essential now is that HancomOffice is actually better or at least as good as Microsoft Office (I am certain that KDE itself is fairly well-suited for the job). If productivity analysis will reveal the decision to be economically sane, much larger installations in Korea will likely follow, and the Korea case will be one that will be analyzed carefully by all countries thinking about moving to Linux. Too bad OpenOffice can't play the role of Hancom yet, I would prefer an open-source office solution, and I know that OpenOffice will eventually be good, whereas I don't know if HancomOffice is good."
    author: "bad_actor"
  - subject: "Clipboard"
    date: 2002-01-16
    body: "Ok ... Will the mass be using the broken clipboard in KDE 2 where there is no paste over select?  Imagine the tech support call from 120,000 users trying to copy a URL from KMail, and paste it in Konqueror.\n\nJust a thought ...\n\n--kent"
    author: "Kent Nguyen"
  - subject: "Re: Clipboard"
    date: 2002-01-16
    body: "Hopefully, they'll just *click* the darned thing. :)"
    author: "jd"
  - subject: "Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "I wouldn't say \"way to go Hancom\"... Better say: Way to go, KDE!!!\n\nRight on time as well, with so many GNOME Office reaching 1.0 status lately.  For a moment there I thought they were going to overtake KDE. Unfortunately (that is, IMHO), Hancom office isn't free software. It would be really nice if more people would help improve KOffice instead of adding more eye candy to kde-core etc.\n\nGreetz\nSteven"
    author: "Steven"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "Your FSF people are such clever little Trolls. It was Hancom that made the deal to get KDE on those desktops so go Troll somewhere else."
    author: "KDEUser"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "I really don't see this as a troll.  In fact I think that this attitude is essential for me continue to be interested in Linux.  \n\nI use Open Source / Free Software because I think it's a good thing--that's why I develop it too.  I don't just use Unix, I use GNU/Linux because I like being able to hack on the things that I use.  I'm glad that KDE is getting some attention out of this, but I wouldn't really be that excited if it were an announcement like \"120,000 computer have switched to Solaris.\"  If really don't see a lot of reason to be excited about a group switching from relying on one proprietary system to another.  I think a lot of the excitement around this has mostly just been spite towards Microsoft.\n\nSo, some clarification:  I don't hate closed source software, companies or people including Hancom, theKompany, Trolltech, Sun or Microsoft.  I just don't see anything special about people relying on closed source software.  That's what they've been doing for the last 40 years.  In fact I think that all of these companies have done some good things, including making some great contributions to Open Source.\n\nI guess I don't want to see Linux become \"just another platform.\"  Linux is defined by its idealogical roots and I hope it sticks to those.  Linux isn't as much a commercial competitor to Windows as it is an idealogical competitor and I think the latter is more important.  It seems that this recent event was mostly a commercial win for Linux.  So hopefully this will cause plurality in the desktop market, but even more I hope that it's a stepping stone to moving to a truely Free platform."
    author: "Scott"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "Sure the office software has changed from one non-free system to another, but at least the OS has made the switch.\n\nView it as a stepping stone. Without this step they would probably never consider going for a completely free system."
    author: "David"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "Like I said you FSF people sure are clever little TROLLS."
    author: "KDEUser"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-17
    body: "Blah, blah blah you Richard Stallman Limmings all sound the same. Group think is all ways a bad thing. Time to group up people and think out side the box. Going from one slavery to another is not an answer. Just ask the Russians circa 1920s."
    author: "Desktop maniac"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-17
    body: "So by \"one slavery to another\" you mean closed source to closed source, right?\n\nActually I'm not really a FSF guy--not completely anyway.  I'm somewhere inbetween OSI and FSF.  \n\nI think closed source software could potentially be acceptable, but I don't want to deal with it and really don't care about it.  So I guess I have a passive dislike for proprietary software--I just don't care about it--whereas the FSF has an active dislike.\n\nOh, and another point.  Groups that think alike are great.  Groups that collectively don't think are bad.\n\nMethinks too many Linux newbies have forgotten that Linux isn't just another platform (and ignore why Linux exists in the firstplace).  And a couple of them have decided to troll here [slaps own hand for responding to trolls]."
    author: "Scott"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-17
    body: "Always closed source is slavery and always open is also slavery. We need choice not some fanacics forced software utopia."
    author: "Desktop maniac"
  - subject: "Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "I wouldn't say \"way to go Hancom\"... Better say: Way to go, KDE!!!\n\nRight on time as well, with so many GNOME Office reaching 1.0 status lately.  For a moment there I thought they were going to overtake KDE. Unfortunately (that is, IMHO), Hancom office isn't free software. It would be really nice if more people would help improve KOffice instead of adding more eye candy to kde-core etc.\n\nGreetz\nSteven"
    author: "Steven"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "IMHO, GNOME is not the enemy, GNOME is free software and I wish them well. The enemy is micro$oft and their dirty monopolic tricks. I currently use KDE, but I would like to see GNOME grow stronger too. \n\n-- Leo"
    author: "Leo"
  - subject: "Re: Kongrats KDE!  Next up: \"KOffice replaces Hancom\""
    date: 2002-01-16
    body: "No, to further clarify, the enemy is closed source software, and in particular closed source software that hurts the software industry (and thus, computer science in general). If M$ open sourced all their apps tomorrow, they would become pretty popular with me. I still wouldn't use their software unless it became much better (UNIX just has a better design, imho) but that would help everyone in general. \n\nSheesh, M$ would still make a killing doing tech support, certification, and selling copies of the manual to OEMs, not to mention their game console, various perhiperals, and other misc. stuff.\n\nKDE isn't really about competiton anyways. Ask any developer (btw, I'm not a developer personally, beyond submitting a few bug reports, but I hang out in #kde occasionally) and they'll probably tell you a few of :\n\n- Because coding is fun\n- Because I want to help UNIX become a better desktop platform\n- Because I wanted a particular feature that was missing, and just plain added it myself\n\nBut most of all...\n\n- Because Konqi is just so dang cute!"
    author: "Carbon"
  - subject: "using KDE for their daily productivity?"
    date: 2002-01-16
    body: "They'll hardly be using KDE for their daily productivity.  While they may be using Konqueror to waste time here and there, it seems obvious that they are going to be using Hancom's products for their work.  That'w why it was bought.\n\nHard to call it a blow for freedom when the centerpiece of the purchase is no more free than Microsoft Office."
    author: "Neil Stevens"
  - subject: "Re: using KDE for their daily productivity?"
    date: 2002-01-16
    body: "No, it's slightly more free. The doc specs are less of a moving target, making filters a good option. Besides, it's freer then Microsoft Office because it uses components (Linux, X Free, etc) that are free, at least in the Linux version."
    author: "Carbon"
  - subject: "Re: using KDE for their daily productivity?"
    date: 2002-01-17
    body: "You just a FSF Troll!"
    author: "Desktop maniac"
  - subject: "file formats"
    date: 2002-01-16
    body: "Shawn Gordon,\n\nI'm *really* happy to hear those good news, since I wish your company all the best. I also really have no problem with many of your products being closed-source, since we all know you need to make some money.\n\nWhat I'd like to know is whether HancomOffice's file-format is \"open\" or not. They don't seem to mention that on their website, and although HancomOffice's file-formats probably weren't your decision, I thought you might know.\n\nthanks for your (probably very quick ;) reply!"
    author: "me"
  - subject: "File Formats"
    date: 2002-01-16
    body: "Shawn,\n\tNormally, you are very good about handling questions conerning what is going on (I think that you get too much into with the trolls, but that is a different matter). But asking about the file formats of hancom is NOT a troll. I asked over on LT and I see others asking here. Is Hancom a closed format? If so, will Hancom be opening the format soon? If not, why?\n\nthanx\ng.r.r."
    author: "a.c."
  - subject: "Re: File Formats"
    date: 2002-01-22
    body: "sorry, I've been very busy lately and haven't been on the dot in some time.\n\nI don't know what Hancom is going to do with their file formats, or what they look like.  My concern is over my 4 applications in the suite at the moment, and there is nothing special about our file formats, it's not like with Presenter, Sheet and Word.\n\nSo I can't answer the question - send an email to Hancom and ask them about it directly - and if you need an answer from me, it is better to send me an email if you want to make sure to get a response.  I try to keep up on these talkbacks, but it isn't always easy"
    author: "Shawn Gordon"
  - subject: "At least, some of them ARE GPL."
    date: 2002-01-17
    body: "Look at http://www.hancom.com/en/product_service/easydb0904.html\nHancom EasyDB it's just TheKompany's Rekall.\n\nAlso http://www.hancom.com/en/product_service/envision0904.html\nit's Kivio\n\nhttp://www.hancom.com/en/product_service/builder0904.html\nHancomWebBuilder it's just Quanta Plus.\n\nhttp://www.hancom.com/en/product_service/quicksilver0904.html\nHancomQuicksilver looks like Aethera.\n\nSo at least 50% of their products are from TheKompany. As I know, all of them\nare GPL'd, so i think we can see HancomOffice going to freedom :)\n\nLastly, I want to point this :\n\n- Development Tool and Library \nHancomWord : Wine-20000326.tar.gz \n\nSo, seems their word processor it's just a Windows app. running under wine. Oh well.\n\nI'm currently downloading HancomOffice, so i will give it a try.\n\n--{@ Alex Perez"
    author: "Alex Perez"
  - subject: "Re: At least, some of them ARE GPL."
    date: 2002-01-17
    body: "Um, Hancom Linux 5.x had Hancom Word using Wine, 6 is a complete qt app...  ;-)"
    author: "Ill Logik"
  - subject: "Re: At least, some of them ARE GPL."
    date: 2002-01-17
    body: "> So at least 50% of their products are from TheKompany. As I know, all \n> of them are GPL'd, so i think we can see HancomOffice going to freedom :)\n\nThe versions included with HancomOffice are not GPL'd. So no freedom in sight."
    author: "someone"
  - subject: "Re: At least, some of them ARE GPL."
    date: 2002-01-22
    body: "they are Rekall, Kivio mp, Quanta Gold and Aethera.  The only ones that are GPL is Aethera, none of the others are GPL.\n\nYes, Word 5.2 used WINE, but Word 6.0 in HancomOffice 2.0 is a port to Qt like the rest of the suite.\n\nWhere are you downloading from?  There isn't a public demo at the moment, it's supposed to go up in a couple weeks or so."
    author: "Shawn Gordon"
---
<a href="http://www.hancom.com/">Hancom Linux</a> recently
<a href="http://www.hancom.com/en/news/press02_0111.html">announced</a>
that the Korean government has procured 120,000 copies of
<a href="http://www.hancom.com/en/news/press02_0111.html">HancomLinux
Deluxe 2.0</a>, which includes, among other things, KDE 2 and
<a href="http://www.hancom.com/en/product_service/office.html">Hancom Office</a>.
This means that many more office workers will soon be using KDE for their daily
productivity.  The government, which will migrate the equivalent of 23% of the
annual Microsoft Windows based PC procurement, is quoted as estimating that
they will "save 80% of what an equivalent purchase of Microsoft products
would have cost."   See also the stories at
<a href="http://slashdot.org/article.pl?sid=02/01/12/1730249">Slashdot</a> and
<a href="http://www.theregister.co.uk/content/4/23667.html">the Register</a>.
Way to go, Hancom!

<!--break-->
