---
title: "KDE Usability Team Takes First Steps"
date:    2002-05-15
authors:
  - "cstr\u00f8mmen"
slug:    kde-usability-team-takes-first-steps
comments:
  - subject: "Awesome"
    date: 2002-05-15
    body: "Now if only kcontrol got smaller size-wise...my 1024x768 feels so crammed with default kcontrol size. I really like the small gnome2 config dialogs(of course they have less stuff), but they are tiny/tidy. Also the no-apply button feature is kinda cool, how results can be seen right away(not sure if kde needs this), there seems to be a need for a revert button in gnome now :)\n\nAnyway, can you guys look at kicker itself now? Stuff like modifying the \"K\" graphic, assigning shortcuts to it, modifying the konsole icon to exectute aterm, stuff like that is kind of tricky. Also the whole kde vs non-kde buttons are confusing, it should all be streamlined. And yes, I dont know the solutions, I just know whats inconvenient :(."
    author: "Judge Dread"
  - subject: "Re: Awesome"
    date: 2002-05-15
    body: "you can change the execute field of the konsole icon to aterm easily.\n\n"
    author: "somekool"
  - subject: "Re: Awesome"
    date: 2002-05-15
    body: "Keyboard shortcuts for the kicker menu have been implemented in CVS (also a result of discussion on kde-usability)."
    author: "Hamish Rodda"
  - subject: "Re: Awesome"
    date: 2002-05-15
    body: "Sweet.\n\nI noticed that when I went from 2.1 to 2.2, the Esc key no longer closed the K menu.  I have to actually go down and click it again to close it.  Dunno if this is still the case in KDE3, but if I find out it is, I guess I'll go file a report with you guys."
    author: "A person"
  - subject: "Re: Awesome"
    date: 2002-05-15
    body: "ESC seems to work here"
    author: "dpash"
  - subject: "Re: Awesome"
    date: 2002-05-17
    body: "Do you probably use Qt 2.3.2? This version had a bug which causes the behaviour you describe. Downgrading to Qt 2.3.1 will fix this problem.\n\nIngo\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Awesome"
    date: 2002-05-15
    body: "the \"K\" graphic: there are icon sets that offer alternatives. what exactly is wrong with with current graphic?\n\nshorcuts: as hamish said, there are shortcuts for the KMenu (and as a result the bookmark and quickbrowser menus). the mechanism employed there is rather innovative IMO, i haven't seen it implemented that same way anywhere else as. hamish gets the bulk of the credit on that one since he wrote the code. there aren't shorcuts for navigating the rest of the panel, though. and that is a huge shortcoming that does need to get addressed.\n\nbuttons: the wording on the add/remove menus has been made a bit clearer, but there is more work that [c|sh]ould be done with that whole bit of kicker as well."
    author: "Aaron J. Seigo"
  - subject: "Re: What is wrong with K"
    date: 2002-05-15
    body: "Absolutely nothing is wrong with the current graphic.However I would like to be able to change menu icons and not have them tied to the main K menu system. There is no way to right click the K and select a new graphic that way. Also I dont see a way to add your own menus to the kicker(gnome does that & cde has 1-level menus). Ie one can add menus from the K menu, but you run into the same problem of having to have it in the K menu.\n\nie, I really like the way gnome/cde/xfce panels let you create custom menus on the panel, without forcing any restrictions on you."
    author: "Judge Dread"
  - subject: "Re: What is wrong with K"
    date: 2002-05-16
    body: "you can \"fake\" this by using a quick browser button and some creative symlinking. but it is indeed a hack. this exact thing was mentioned on the usability mailing list not too many days ago. someone just needs to write the code (it really shouldn't be that hard)"
    author: "Aaron J. Seigo"
  - subject: "Re: Awesome"
    date: 2002-05-15
    body: ">I really like the small gnome2 config dialogs(of course they have less \n>stuff), but they are tiny/tidy.\n\nI think one of the big things the Sun/Gnome usability team did was reduce the number of options. KDE should follow that example. Pick sensible defaults and maybe even support additional config options that aren't in Kcontrol (but are carefully documented somewhere!!) and keep the config dialogs manageable and unintimidating.\n\n>Also the no-apply button feature is kinda cool, how results can be seen \n>right away(not sure if kde needs this), there seems to be a need for a \n>revert button in gnome now .\n\nThis would be a great addition to KDE, and probably not difficult to add."
    author: "Otter"
  - subject: "Re: Awesome"
    date: 2002-05-16
    body: "The problem is, every time you remove an option someone complains; so cutting any options is a very difficult decision. For example, look at people not happy with the button size option getting removed in the new kcm_kicker. \n"
    author: "Sad Eagle"
  - subject: "Re: Awesome"
    date: 2002-05-17
    body: "Maybe we should have a newbie mode that would hide a lot of advanced options.\nMaybe not...  "
    author: "aThom"
  - subject: "Weird.."
    date: 2002-05-15
    body: "This doesn't look like new changes, the screenshots look like KDE has been for a long while.."
    author: "Rob Kaper"
  - subject: "Re: Weird.."
    date: 2002-05-15
    body: "Doh. My browser wasn't wide enough to see the \"after\" shots. Oops."
    author: "Rob Kaper"
  - subject: "Re: Weird.."
    date: 2002-05-15
    body: "yeah, apologies on more-than-horrid \"web design\". i threw that together so people on the usability list could grab the patches and look at screenshots while we were working on it. it wasn't really meant to end up on a public news site. it's mildly embarrassing to have something that ugly posted =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Weird.."
    date: 2002-05-15
    body: "It does illustrate the changes quite well, i think you were right not to use too much time on the web page.\n\nGood work guys.\n"
    author: "troels"
  - subject: "good stuff"
    date: 2002-05-15
    body: "as said"
    author: "Ez"
  - subject: "Re: good stuff"
    date: 2002-05-15
    body: "Certainly. Thanks to Aaron et.al and keep up the good work."
    author: "Claus Rasmussen"
  - subject: "General KDE usability comments"
    date: 2002-05-15
    body: "Firstly, how about changind the default ordering of buttons in the title bar, so that the CLOSE button is far away from the MINIMISE, MAXIMISE and MENU buttons? Or is it just me who ended up accidentaly closing programs, when all I wanted to do is see more of it (maximise)?\n\nSecondly I remember seeing (probably my friends windows XP comp), was showing the scaled down screenshots of the actual program, when cycling between windows (Normally invoked by ALT-TAB and you get an icon and what's in the titlebar of the window). Bit like what the pager applet does but invoked by ALT-TAB and thus not taking up screen space all the time. I liked it. What do people think?"
    author: "David Siska"
  - subject: "Re: General KDE usability comments"
    date: 2002-05-15
    body: "It's not just you. That one of the first things I always change:\n\nControl Center -> Look & Feel -> Window Decoration -> Buttons\n"
    author: "michael"
  - subject: "Re: General KDE usability comments"
    date: 2002-05-15
    body: "> Firstly, how about changind the default ordering of buttons in the title bar\n\nThis is already possible to change if you want to, go to kcontrol, look&feel, window decorations and then change it on the second tab there. The reason for the default placements are many, but mostly that this order seems to be what most people are used to.\n\n> I remember seeing (probably my friends windows XP comp), was showing the scaled down screenshots of the actual program, when cycling between windows\n\nThis is a feature from the Powertools package for Windows XP, it's a good one, and it's a good idea for KDE, but it's a very resource-demanding feature. My laptop (which is a PIII 750) slows down terribly when using it.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: General KDE usability comments"
    date: 2002-05-16
    body: "Microsoft, the company that introduced this particular joy, puts a small spacer between the maximise and close buttons, so there less of a chance of making a mistake. Maybe introducing a \"separator\" into the button configuration, the same way as it exists in the toolbar configu might help. This stuff all sounds pretty cool acutally, I'll try and get involved once college is over.\n\nBTW, are the KDE coders planning on implementing the recommendations of the Sun user study? I've glanced through it, and most of it seems applicable.\n"
    author: "Bryan Feeney"
  - subject: "Re: General KDE usability comments"
    date: 2002-05-17
    body: "there _is_ a spacer in the window decoration button configuration dialogue, its just up to the window decoration itself to actually impliment the spaces.\n\nMay be better produdtion wise if you just check the dialogue :P"
    author: "anon"
  - subject: "Re: General KDE usability comments"
    date: 2002-05-17
    body: "Hmm. I posted that in college where we're using KDE 2.0 (and boy is it slow and painful) and that option doesn't exist. It might be in 2.2, which is what I use at home. Maybe I should wait till I get my hands on 3.0.1 before I start talking! :-/"
    author: "Bryan Feeney"
  - subject: "Use Laptop"
    date: 2002-05-16
    body: "\nin windows decoration choose laptop the closin button is not even on the same side as the others ;-)"
    author: "Moulinneuf"
  - subject: "Re: Use Laptop"
    date: 2006-01-09
    body: "on newer kde versions (here i have 3.4.3), you can switch in the window decorations dialog to the buttons tab. there you can choose to \"use custom titlebar button positions\". then you can move the buttons around as you like.\n\nthe downside was that this was selected by default, overriding the default layout and thus laptop style did not move the close button to the left.\n\nso, either switch the option off and use laptop - or use whatever style you like and move the buttons around..."
    author: "dave"
  - subject: "What else do they have up their sleeve?"
    date: 2002-05-15
    body: "The usability.kde.org website was a little...spartan...when it came to details about what the usability team has in store. I'm running 2.2.2 (still waiting patiently for those .debs), so I don't know if their suggestions for Konsole and Kicker were implemented. And since the archives are a little too big for quick perusal, would someone mind giving a Reader's Digest Condensed version of what's going on?\n:Peter"
    author: "Peter Clark"
  - subject: "Re: What else do they have up their sleeve?"
    date: 2002-05-15
    body: "the website is still under construction as far as i know (seemingly perpetually). the real fun is happening on the mailing list where things are discussed and done.\n\nas for the usability reports, things get implemented when someone writes the code. step one is to identify usability issues, step two is to figure out solutions for those challenges, step three is to code it. there are lots and lots and lots of people who are willing to help with step one, some who are willing and capable of helping with step 2 and rather few who do step 3.\n\nthe usability project is attempting to get more effort into steps 2 and (especially) step 3. developers are needed, but so are documenters, designers and testers.\n\nas for what is being looked at right now: the minipager, kicker kcm, misc other kicker issues, desktop kcm... "
    author: "Aaron J. Seigo"
  - subject: "Re: What else do they have up their sleeve?"
    date: 2002-05-15
    body: "There seems no (working) way to access the usability reports announced on the frontpage."
    author: "Anonymous"
  - subject: "Nice Stuff, and a suggestion"
    date: 2002-05-15
    body: "The concept of import/export, files, printers, database are all the same thing. These should not be seperated. An application should use 1 to 3 kio's (file stream, page, && table/sql). The conversion between fileformats should be external to the application. During the open/save dialogs, the applicable choices should be allowed.  Likewise, the printer should simply be part of output (it should be a page kio). \nThis would allow :\n1) a clean seperation of import/export from application, \n2) new ideas to be easily added in the future (perhaps via a plugin on the open/save dialog)\n3) the constant re-use of import/export code. If somebody creates an output of png, and a 2'nd person creates a conversion of png->jpeg, then the application is now able to save in jpeg. Likewise, if app does a page kio of PS, and somebody follows up with PS->tiff, then a nice fax interface is easier. Or perhaps PS->mng, etc.\nFinally, the Location menu should be easily configurable with shortcuts to these. Allow a user to add what they want. I don't believe in paper, so I have no need for a printer. But I would love to send the data directly to kmail or kopete to send a file directly to someone."
    author: "a.c."
  - subject: "Re: Nice Stuff, and a suggestion"
    date: 2002-05-15
    body: "I think this a great idea; I was thinking along these lines myself. There are all kinds of possibilities: e.g. in KMail, when attaching a file, an \"Attach as type...\" option; so we can store our documents as KWord files then simply \"Attach as type... Word\" or \"Attach as type... PDF\" as required."
    author: "Julian Rockey"
  - subject: "Ewwww "
    date: 2002-05-15
    body: "Guys/Gals, The new Location config really really really sucks!\n\nDid a quick test with a non-geek and they were totaly clueless about what the hell it was, much less how to make it work.\n\nYou should label the locations buttons IMHO\n\n"
    author: "JRM"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "That, or replace the monitor image with an image of just the desktop, and label the whole thing 'Desktop Location of Panel' with a caption of 'Click the button closest to the location where you would like the panel to appear' or something like that."
    author: "Carbon"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "agreed!  it confused me, I had to look at the before screen a couple times before figuring it out..."
    author: "Bill"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "Rather than render the endire desktop, just draw a \nrepresentation of the current panel on the display.\n\nThis would help people figure out what specifically\nis changing on this menu.\n\nYou could also render tiny/small/normal and length options as well.\n\nThe only option that is not obvious is the \"expand to fit \nrequred space\"\n\nOverall, everything looks very slick, even more than before.\n\nEd\n"
    author: "Ed"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "> You should label the locations buttons IMHO\n\nthe plan is to add icons to them.\n\nCarbon said:\n> That, or replace the monitor image with an image of just the desktop\n\nand how would that be clearer? putting an image of the current desktop background in the monitor is something i'd like to get to (ala the background kcm), but i don't see how losing the monitor would improve anything.\n\n> with a caption of 'Click the button closest to the location where you would > like the panel to appear' or something like that.\n\nit would be nice to be able to avoid any long-winded explanations. labeling the buttons w/icons will probably help quite a bit. besides, users don't read do they? ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "The most irritating is, that they are located outside of the monitor!\nYes: i know you recycled the monitor image from the screensaver configuration... \nAnd yes, im a nitpicker, but in usability one has to nitpick. :) "
    author: "Lenny"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "I think the buttons should somehow reflect the \"length\"-setting. As it is now, it's as if length was set to 33%. A larger length setting would make the buttons overlap each other though...Perhaps they could have different offsets from the monitor image or be in different colours."
    author: "ac"
  - subject: "Re: Ewwww "
    date: 2002-05-16
    body: "the panel in the monitor grows/shrinks with both the length and size parameters. so you can see how it will approximately look with the alignment, size and length you have chosen. it's fun to play with the sliders =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Ewwww "
    date: 2002-05-16
    body: ">> That, or replace the monitor image with an image of just the desktop\n\n>and how would that be clearer? putting an image of the current desktop background in the> monitor is something i'd like to get to (ala the background kcm), but i don't see how losing the monitor would improve anything.\n\nWell right now a normal user might think that the buttons on the screen might replace their controls on their moniter (up,down,left,right etc..) Without the monitor image it wont look like it's a hardware setting.\n\nAgain IMHO :-)"
    author: "JRM"
  - subject: "Re: Ewwww "
    date: 2002-05-16
    body: "they are in the kde panel control panel, it says \"you can configure the appearance of the panel here\", and it's in a box labelled \"Position\". if they think it's their monitor settings (for what exactly?) then i don't think anything would be clear enough for them.\n\nbtw, that same monitor is used in the background setter and screensaver chooser. consistency."
    author: "Aaron J. Seigo"
  - subject: "Re: Ewwww "
    date: 2002-05-15
    body: "I thought it was dead obvious, you chose a screen spot (left, right, top, bottom) and the position of it within that screen spot if your bar is not at 100% (left, center, right)."
    author: "Abdulla"
  - subject: "Possible suggestion"
    date: 2002-05-15
    body: "I know everyone's full of these, but I'd like to see how other people feel about this, and for various reasons, I can't get a mailing list account.\n\nAnyways, one thing that bugs me about KDE 3 is that annoying sidebar in the file open dialogs. I'm not going to get huffy about how it's like Windows (not that being like Windows is much of a plus, either), but there is something I do dislike about it: it wastes screen space, which I find valuble for actually attempting to locate the file I'm looking for! The stuff that appears in the sidebar should be under the pop-up menu on top, anyways, since it doesn't take up any extraneous room up there. Or, better yet, make it optional. What do you guys feel about this?"
    author: "Carbon"
  - subject: "Re: Possible suggestion"
    date: 2002-05-15
    body: "it is optional, in the gear menu (at least in CVS)"
    author: "Aaron J. Seigo"
  - subject: "Re: Possible suggestion"
    date: 2002-05-15
    body: "It's already optional in CVS and there exists a patch to make it optional in KDE 3.0."
    author: "Anonymous"
  - subject: "Re: Possible suggestion"
    date: 2002-05-16
    body: "I agree. It was sickening seeing that useless only-for-the-sake-of-being-windows-like useless menu side graphic (which i always hated in windows too).\nGlad to hear it is optional in CVS... looking forward to 3.1!"
    author: "Raga"
  - subject: "Re: Possible suggestion"
    date: 2002-05-16
    body: "The \"useless menu side graphic\" is already optional in KDE 3.0: K Menu Layout/Show Side Image"
    author: "Anonymous"
  - subject: "Re: Possible suggestion"
    date: 2002-05-21
    body: "First I think you are mistaken as to what the original poster was complaining about.  You are refering to the side graphic, I think he is referring to the icons at the side of the save as and open dialogs which have folder shortcuts. \n\nWhile for many Windows-like features I would agree with you, but this one I definately disagree.  That side panel is rather useful for me.  I keep project documents separated and I have different icons there for different projects and it is very easy when I download something to save it in these different directories without clicking through multiple folders to get there.  While I will admit that the buttons are much larger than they need be, I feel this is a great feature, and I am glad that it is at least removable for people who aren't interested in it.  \n\nAnd there is one other Windows like feature I would like to see, and that is a highlighted icon strip in application menus.  I like having the icons a background color different from the text background, it helps separate them out and eases scanning, at least for us dyslexics out there. I thought I had seen this in some screen shots somewhere but haven't been able to find it, maybe it is a theme or something, or maybe it was just the KDE graphic that runs up the side of the Kmenu. \n\nJust as an FYI, Microsoft has whole teams of usability folks out there, they do some things very well in their design, it would be stupid not to take hints from what they do right in building KDE, it would be even more stupid not to take hints from what they do wrong.  Configurability of options is key to keeping different people happy, we all work in different ways.  Microsoft locks you in to their idea of what is the best way to work, sure the usability people found it was good, but it doesn't always work for all of us.  I must say that I am getting rather used to the quick start menu in XP now, I only run about 5 applications in Windows anyway, so it was easy to pin them to the short menu without having to see all the sub menus and such. Maybe another thing we could learn from Microsoft.  Maybe someday they will learn to make a good OS from the Linux folks, or how to make a powerful and configurable desktop from KDE and GNOME.  \n"
    author: "Anthony "
  - subject: "Re: Possible suggestion"
    date: 2002-05-22
    body: "> And there is one other Windows like feature I would like to see, and that is a highlighted icon strip in application menus. I thought I had seen this in some screen shots somewhere but haven't been able to find it\n\nIt's a widget style thing: \".NET style\" and upcoming Keramik style have it."
    author: "Anonymous"
  - subject: "Re: Possible suggestion"
    date: 2002-10-19
    body: "> I think he is referring to the icons at the side of the save as and open dialogs which have folder shortcuts. \n\nSure. Recently I installed Mandrake 9.0 and also stumbled over these annoying icons on left side of Save (and some other) dialogs. KDE FAQ and Google search didnt tell me how to get back to normal Save, without this microsoft-mimicking.\n\nHow do I switch them OFF forever, PLEASE tell me somebody! Linux is great in its configurability, so there should be easy way to get rid of them. \n\nHarry\n"
    author: "Harry"
  - subject: "Hide Button Size?"
    date: 2002-05-15
    body: "At first glance it seems the option to change the hide button size disappeared. It need that!"
    author: "Anonymous"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-15
    body: "Good point. I concur.\n"
    author: "ac"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-15
    body: "I actually suggested removing that piece of crap. Choose a sensible size and that's it. No reason to configure things like that. kcotnrol is already full of useful options, no need to add stupid ones.\n\n(If you want, document how to change it in the config files)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-15
    body: "> I actually suggested removing that piece of crap. Choose a sensible size and that's it.\nI don't agree with you. The so called sensible size is very much a matter of taste \n(also with regard to having hiding arrows or not).\nCheers!\nBeat"
    author: "tuxraver"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-15
    body: "In that case they should hide it in an 'advanced settings' dialog with some other minor-tweak options and warn the user that they probably don't want to change these options. Thay way you still have a relative easy way of changing it, but the chances of confusion are minimised."
    author: "Matthijs Sypkens Smit"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-15
    body: "Then why did you make it huge with KDE 2/3 when it was okay in KDE1????  You are to blame for this.  It was good then you make it too wide and force everyone to size it down.  Now you take away the option????\n\n"
    author: "ac"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-16
    body: "not every option can be in the control panels; at least not in the main ones. otherwise we will have waaaaaaay too many to EVER have a usable system simply because <1% of users may want to change a certain thing.\n\nthere are three options:\n\n 1) remove the setting totally\n 2) remove the setting from the UI, leave support for it in the program\n 3) put it in an \"advanced settings\" dialog that you access from a button\n\nin this case i chose #2 since there wouldn't be much else in the advanced settings dialog if i chose #3. you can still set the button width by editting the kickerrc file. but having it in the main UI was crowding things, was not very apparent as to what it did and is a rather non-standard type of setting (where else do you get to set the *width* of a specific button?)"
    author: "Aaron J. Seigo"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-16
    body: "nitpicky, but #3 would be better"
    author: "fault"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-17
    body: "give me 2 more features to put in an \"advanced\" dialog and i'll do it ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Hide Button Size?"
    date: 2002-05-19
    body: "I'd suggest keeping the size option and incorperating the enable/disable as the as part of the size settingd, by having the lowest setting say \"off\".  As has already been suggested, I'd keep this in an advanced dialog."
    author: "Clint Silvester"
  - subject: "What about the last tab? (Extentions)"
    date: 2002-05-15
    body: "Hi!\n\nThis Child-Panel stuff etc. is not very intuitive. Would be nice if all panels share the same config options and dialogs.\nBut the new dialogs are really an improvement!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: What about the last tab? (Extentions)"
    date: 2002-05-15
    body: "I agree in the Child-Panel, but I think there should be an option to have several Panels, each with their own settings.\nOne could have no panel or three or whatever.\nOfcourse with an option to use another panels settings.\n\nThus i could have a K-button just as a auto-hidden button in the bottom-left corner, with red background. (huge panel)\nAnd an centered green penel with Kmail, Konqui, Kate, Kword, Konsole and Mozilla buttons. (large or medium panel)\nAnd a huge panel with red background in the bottom right corner with the clock. (auto-hide)\nAnd then a small panel with taskbar and a system tray at 100% width in the top. This panel also green.\n\nFor me now, I have to have a medium green auto-hiding \"Liquid-striped\" panel with K-button and applications buttons as well as the clock. And the cild-panel with KDE normal colorings in the top with system tray, news ticker and a taskbar."
    author: "Syllten"
  - subject: "Re: What about the last tab? (Extentions)"
    date: 2002-05-16
    body: "yes, the plan is to remove the extensions tab and treat the extension settings as if they were just other panels (albeit some of them of special types, such as taskbars, kaskbars, etc...)\n\nprogrammatically a child panel is different than the main panel, but from a user's perspective they are the same when configuring position, etc... so that is how it will be in the control panel.\n\nit just takes time, and right now i'm at a tech conference all day for the next few days (demoing kde! =))"
    author: "Aaron J. Seigo"
  - subject: "I'm impressed"
    date: 2002-05-15
    body: "I like how KDE is comming along, and it's nice to see that it improves and that things like this is worked upon. I am still not using KDE myself (because I don't have an extra computer that is fast enough to play with it) yet though. Just a little pat on the shoulder and \"nice work, keep it up\".\n\nAbout the placement of the kicker. Just a little thought:\n\nIn windows (jippie ^_^) you can simply grab the taskbar and move it to where ever you want it. They had a problem that people accidently moved it, so a \"lock taskbar\" option was added. Pretty simple and easy.\n\nSo how about removing the picture of the monitor, put a picture of the current desktop background there instead, and put a little image of the kicker on the small dekstop image. Then make it possible to move it around to which side one wants to (making it snap to the sides, graying it out while moving etc).\n\nThe downside one might say is that a so called non-geek wouldn't know how to do it. I'd like to (try) to qoute Einstein here \"Make it as simple as possible, but not more so\" (I probably got the words totally wrong;)). Moving the kicker isn't the first thing these people will do, so don't try to make the dialog overly obvious. The problem here is that it's a custom control that doesn't appear anywhere else (yet). Is there other similar options/cases when a control such as this is useful? Then it could become much more useful.\n"
    author: "John Gustafsson"
  - subject: "Re: I'm impressed"
    date: 2002-05-15
    body: "\"\" In windows (jippie ^_^) you can simply grab the taskbar and move it to where ever you want it... Pretty simple and easy. \"\"\n\nThis works in KDE as well."
    author: "Jon"
  - subject: "Re: I'm impressed"
    date: 2002-05-15
    body: "Ah, even better. Then even the windows-haters will know exactly what I mean and have used it. So in short, the same thing but in miniature;)\n\nStuff like this makes me wanna try KDE even more. Now for a way to get hold of a machine (I don't dualboot for, well, a bunch of reasons). Unless there is a viable and fairly easy way to get KDE 3.x up and running under windows/dx8.\n\nOk, now you must be wondering why I want it on Windows. It's because my interest in KDE is purly as a toy to play around with. I have a general interest for desktops etc. This does not mean that I don't think that KDE is a great desktop to use, just that wanna play with it;)"
    author: "John Gustafsson"
  - subject: "Give it a try!"
    date: 2002-05-15
    body: "Try Knoppix!\nhttp://www.knopper.net/knoppix/index-en.html\n\nIts a great \"I-want-to-try-Linux-but-I-dont-want-do-change-my-windows-setup\"-distribution. So far its only a KDE-2.2.2 and yes: KDE3 is better, nicer, faster. But it the perfect distribution to test yourself if you like Linux/KDE or not because it does not touch your harddisk.\n\nGive it a try!\n\nBye\n\n  Thorsten\n\n "
    author: "Thorsten Schnebeck"
  - subject: "Re: Give it a try!"
    date: 2002-05-15
    body: "It looks like a nice idea, if it wasn't for the fact that I want KDE3 and the ability to mess around with the code and code new stuff for it. I've used Linux since '95 and know I don't want it on my only fast desktop computer (if nothing else it makes it hard to use Devstudio .NET) as it doesn't offer me anything I don't already have.\n\nI hope this can help others who wants to try it out though. I must admit to feeling a bit sheepish. It was a good suggestion after all. Now I can just hope someone will donate a computer to me or something;)\n\nCheers,\nJohn"
    author: "John Gustafsson"
  - subject: "Re: I'm impressed"
    date: 2002-05-15
    body: "\"So how about removing the picture of the monitor, put a picture of the current desktop background there instead, and put a little image of the kicker on the small dekstop image. Then make it possible to move it around to which side one wants to (making it snap to the sides, graying it out while moving etc).\"\n\nI like that idea... a lot. Then the mini-taskbar could display the effect of the other options in the dialog also. You would have to make sure it was obvious that the mini-taskbar could be dragged around, though. I wonder how the usability guys would feel about that?\n\nBTW, nice job on the new kicker options! "
    author: "VegasDave"
  - subject: "Re: I'm impressed"
    date: 2002-05-15
    body: "Maybe a simple short text would be enough? For example:\n\nhttp://forgoil.rsn.bth.se/kde/\n\n(You have to imagine a mouse pointer moving the kicker from top to left. Yes I always have the taskbar/kicker/whatever at the top of my screen, and yes I can't utterly stand gimp which I find a complete pain to do anything with, so you have to look at my horrible HTML + png ;))\n((http://www.little-gamers.com <-- that is where the picture is from, it's my on my desktop so I only saw fit that I used that one))"
    author: "John Gustafsson"
  - subject: "Re: I'm impressed"
    date: 2002-05-16
    body: "while i'm tearing apart the extensions tab, i'll be looking at adding the current background to the monitor (just like in the background kcm). and the other options (length and size) on that tab are shown in that preview. you really have to try it out to get the full effect."
    author: "Aaron J. Seigo"
  - subject: "Icons and fonts"
    date: 2002-05-15
    body: "There are two things that I would like to see the usability project take care of:\n\n1. Icons: Collect all icon settings in one place (zooming, size, animation, etc.)\n\n2. Fonts: Collect all font settings in one place (including Konqueror's and kdesktop's)\n\nPlease.\n"
    author: "Joe"
  - subject: "One suggestion"
    date: 2002-05-15
    body: "In 3.0, I liked that the \"Configure Desktop\" & \"Configure Background\" were combined in the root-window context menu (when you right-click on the desktop).\n\nMy preference (i'm sure its been mentioned before, but anyway)... still, my preference would be to combine all the \"Look & Feel\" configurations to one dialog box when you right-click on the desktop."
    author: "Jeff Dooley"
  - subject: "Great job! Only that position dialog..."
    date: 2002-05-15
    body: "somehow does not fit-in well... I do not know \"( I would say this really need some other idea."
    author: "MPatton"
  - subject: "Great Job"
    date: 2002-05-15
    body: "I'd just like to say I think the new design is a lot better, its cleaner and easier to use, very approachable and practical, a nice cleanup of it all which was really needed, thank you KDE Usability Team."
    author: "Abdulla"
  - subject: "800x600 resolution and KDE"
    date: 2002-05-15
    body: "the Configuration dialogs are way too big at 800x600 resolution. So, I would appreciate a smaller configuration dialogs which won't hide the 'OK' 'Cancel' and 'Defaults' buttons when not-maximized or maximized.\n\nEspecially the \"Find File\" Dialog box is very big and can't be resized to a smaller size, which is again very user unfriendly.\n\nTheme Manager must include:\n\n1. Style (the theme and style creators have to write shell scripts to install individual themes and styles). it would be wonderful if style be a part of theme\n2. Fonts (as a part of theme, fonts will make a nice addition in theme manager).\n3. kicker position/configurations (like kicker background, size etc., can also be a part of theme).\n\nthanks..."
    author: "Rizwaan"
  - subject: "Very good job indeed"
    date: 2002-05-16
    body: "Everything is very logical and less crammed, very nice."
    author: "Mat Colton"
  - subject: "\"hide button size\" option"
    date: 2002-05-16
    body: "Original message by \"Moritz Moeller-Herrmann\" on Wednesday May 15, @02:27PM:\n\"I actually suggested removing that piece of crap. Choose a sensible size and that's it. No reason to configure things like that. kcotnrol is already full of useful options, no need to add stupid ones.\n (If you want, document how to change it in the config files)\"\n\nIt is not \"good idea\" to remove \"hide button size\" option.\nGood idea is to make everyone change everything in their desktop.\nDo not limit my/yours/others freedom. It is Windows-way to tweak some things in \"registry\", not unix. e.g. If you make GUI interface, it must have GUI controls to change something. If you make console-UI (TUI) then it is \"o.k.\" to change something in .conf files.\n\nI hope someone heard me, Thanks."
    author: "me"
  - subject: "Re: \"hide button size\" option"
    date: 2002-05-16
    body: "> It is not \"good idea\" to remove \"hide button size\" option.\n\nthe option is still there. it just isn't in the GUI. you can't have every possible option under the sun configurable via controls in the GUI: it makes for horrendously unusable systems.\n\nmore to the point: how much is it really going to hurt you (or anyone else) now that you can't set the exact width of the hide buttons without opening a text editor? is the width of the hide button so important that usability, understandability, the size of the UI, etc, etc should take a hit? or are we being just a bit reactionist here?\n\n> It is Windows-way to tweak some things in \"registry\", not unix. e.g.\n\nyou don't have to change it in a registry, you change it in a text config file. that's VERY unix. if you don't think it is, then you obviously haven't been around UNIX very much (.Xdefaults anyone? ;-) \n\non the other side of the coin, Apple \"Land of the GUI\" Computers understands the need to leave out unecessary options from the graphical tools to preserve usability. there are dozens of settings in OS X that you can't set using the config dialogs but that you can via the command line.\n\nso... how about we focus for a second on the fact that with all the room that was bought due to the redesign we now have room for a new (and often requested) k menu item display format: Description (Name)"
    author: "Aaron J. Seigo"
  - subject: "Re: \"hide button size\" option"
    date: 2002-05-16
    body: "A \"Show hide button\" checkbox is enough IMHO. It's ok to leave the \"width-control\" in the configfile...\n"
    author: "Per Wigren"
  - subject: "Re: \"hide button size\" option"
    date: 2002-05-18
    body: "What I really miss from the GUI, but exists in the config file is an option to hide the handles for tha applets... perhaps this should go into the advanced dlg that Aaron was mentioning possibly creating."
    author: "Troy Unrau"
  - subject: "Re: \"hide button size\" option"
    date: 2002-05-18
    body: "Hi Aaron,\nyou are right, it's impossible to include all settings in a GUI. But what about a button to open the config-file (with e.g. kwrite) with all possible settings, so everybody see \"there's a lot more power for configuring\"???"
    author: "Marc Tespe"
  - subject: "Suggestion"
    date: 2002-05-16
    body: "\nI suggest to add some text to the location frame of the position tab. Something like \"Click on the rectangle to set the panel's position\". It is not obvious out of the box what these frames are for, if you are not familiar with this control panel.\n\nUsually some small text helps to a great extent to understand some feature. Do not hesitate to add some, any clever and usable your layout is.\n\nAnd congratulation for the good work.\n\n"
    author: "Philippe Fremy"
  - subject: "Software Engineer"
    date: 2002-05-16
    body: "Looks great! I agree with most of the suggestions on this page.\n\nI would really like to be able to edit the K menu (ie application from this dialog). It would be cool if there were a button (called \"Edit Menus...\" or something similar) on the Menus tab in the K Menu section which could launch kmenuedit. This way you can configure the entire panel from this dialog. Otherwise you have to go to System/Menu Editor in the K menu to edit them (on RedHat anyway)."
    author: "Nicholas Allen"
  - subject: "Re: Software Engineer"
    date: 2002-05-17
    body: "*very* good idea... i'll add it to my notes for the next round of changes. thanks =)"
    author: "Aaron J. Seigo"
  - subject: "Window placement is messed up in KDE"
    date: 2002-05-17
    body: "I really hate the way windows pop up all over the place.\nIf you open a new message in Kmail it should be centered on the screen, not in the lower or upper corners.  You then have to move it to the midddle to work with it."
    author: "Tony Caduto"
  - subject: "Re: Window placement is messed up in KDE"
    date: 2002-05-17
    body: "You know that ypu can change the window placement policy, right?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Window placement is messed up in KDE"
    date: 2002-05-17
    body: "> You know that ypu can change the window placement policy, right?\nIn KDE 2.2.2 (I''m using Debian) you have only Smart, Cascade and Random at disposition, so no centered window placement is possible (something I would like to use as I am also disturbed by having windows popping up un-centered). Question: has centered windows placement been introduced in KDE 3.0 (haven't found an indication in the changelog)?\n\nTuxraver"
    author: "tuxraver"
  - subject: "Re: Window placement is messed up in KDE"
    date: 2002-05-20
    body: "Also what about a \"Last place I left it\" placement policy. I really wish applications would remember where you left their windows last, and what size they were, and return there when re-launched. Subsequent \"New Window\" type operations could cascade."
    author: "John Allen"
  - subject: "Re: Window placement is messed up in KDE"
    date: 2002-05-17
    body: "\"You then have to move it to the midddle to work with it.\"\n<br>\nYou do? I have no problems using any of the open windows, no matter where they are in my screen."
    author: "Janne"
  - subject: "Re: Window placement is messed up in KDE"
    date: 2002-05-25
    body: "You can't use windows which are not centered? Hmm; perhaps you should buy a old and small monitor where this becomes possible. Or simply switch to a lower res.  I'm not sure the things you want are usefull to people that actually use their whole monitor.."
    author: "LOL"
  - subject: "Re: Window placement is messed up in KDE"
    date: 2004-07-28
    body: "This is an old post, but I was looking to solve this same problem. I'm using Mandrake 10 with KDE 3.2. There is indeed a Center option now in the Moving tab of the Window Configuration tool."
    author: "Matt"
  - subject: "Need better Help system"
    date: 2002-05-17
    body: "Linux needs a unified help backend system. I constantly find myself using Windows when programming python because of the docs based on the Winhelp system.\nAs it stands now most docs it\u0092s not much more sophisticated then plain html. Fortunately most new Linux docs are docbook based so the foundation is there it\u0092s just a nice consistent help system with a good search and indexing that\u0092s missing. I know GNOME also uses docbook so maybe a common help system backend could eliminate the need to launch Nautilus when just to view help on a small gnome app in KDE and vice versa. \nI\u0092d like to work on this but I\u0092m not a C++ coder but I\u0092d love to help out with other tasks.\nBTW. The ability to view man and info pages in kongu kicks ass.\n"
    author: "Fredrik C"
  - subject: "Re: Need better Help system"
    date: 2002-05-17
    body: "Download Dox from http://dox.berlios.de, together with the Python\ndocs there. Then you never have to use Windows again for\nprogramming Python ;-)"
    author: "Bernd Gehrmann"
  - subject: "Re: Need better Help system"
    date: 2002-05-17
    body: "> Linux needs a unified help backend system.\n\nDebian's docbase... Wheee.."
    author: "fault"
  - subject: "Re: Need better Help system"
    date: 2002-05-21
    body: "Thanks, I'd like to try it but if it exsists it's sure well hidden. I'm running debian(Woody) and searched the apt-cache, the web site , google and mail list without any luck.\nCould give me a direction?"
    author: "Fredrik C"
  - subject: "Re: Need better Help system"
    date: 2002-05-21
    body: "Sorry, just found it, doc-base was the magic word."
    author: "Fredrik C"
  - subject: "Another suggestion..."
    date: 2002-05-17
    body: "One more idea I had: The placement of the panel should use toggle buttons around the side of the monitor. This way you can see which one is selected (ie the one you clicked would remain pressed down). I also think a picture of the KDE desktop would be more appropriate than a monitor."
    author: "Nicholas Allen"
  - subject: "No \"Enable background tiles\" global option"
    date: 2002-05-21
    body: "In panel-> look & feel, in background tiles, the global option to turn off/on background tiles is missing. Why is this useful feature being removed ? This is definitely required. Why should the user change 6 different buttons to \"No tile\" when the same effect can be achieved simply by deselecting the global option ?"
    author: "Tok"
---
The <a href="http://usability.kde.org/">KDE Usability</a> Team has come out with the biggest and most visible change yet. Aaron J. Seigo recently <a href="http://lists.kde.org/?l=kde-usability&m=102115458504523&w=2">announced and committed</a> an almost complete re-organisation of the Kicker KControl module with accompanying updates to Kicker. Illustrative screenshots of this important first step can be <a href="http://www.realityx.net/kde/usability/kicker/">found here</a>.  For more news of upcoming improvements be sure to check the <a href="http://lists.kde.org/?l=kde-usability">kde-usability archives</a>.
<!--break-->
