---
title: "FOSDEM.Org: Interview with Richard Dale"
date:    2002-01-18
authors:
  - "numanee"
slug:    fosdemorg-interview-richard-dale
comments:
  - subject: "This is cool stuff"
    date: 2002-01-18
    body: "Now all we need is someone to use it...\nAre there any plans to make a uic type tool for these bindings?  I have used the pyuic one for python and it rocks.  Maby when I get some time I can do some examples in Objective C.  Either way, it would be cool to see more of these things available.\n\nWhile we are at it, has anyone thought about working on an \"Object Proxy\" to allow for mixed language enviroments.  KScript has support for one, but so far only JS Embed uses something like that nature.  \n\nJust a thought\n\t   -ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This is cool stuff"
    date: 2002-01-18
    body: "A uic style tool for each language is no longer needed with Qt3 - you can use the factory class (QWidgetFactory IIRC) to load a .ui file on the fly. This is the way JSEmbed handles dialog loading and I've found it works fine.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: This is cool stuff"
    date: 2002-01-18
    body: "I was exploreing this with the shell script engine, and the python script engines, but it seemed like there might be a better way.  The largest problem I see with this is the need for \"yet another file\"  to include with the script.   I still need to research it more though.  The one thing I really wanted to gain was the ability to dynamicly add and remove objects from the C++ side without having to rely on libraries or compiled code.  The other issue I found was I could not seem to be able to pass in my own XML on the fly.  \n\nOne experiment I tried involved a dcop interface to a factory class and a container class.  It was randomly responsive, so I really would like to explore it more.  It would be cool to get to the point where via dcop one could create a full form based application in a meta language.  By no means would this replace KMail or Konqi, but it would make those 5 minute convience applications a reality.  \n\nFor my limited example I had the following dcop calls\ndcop factest-1558 createInterface new myClass\n\nThis created a new instance of myClass in the test application and then\ndcop factest-1558 myClass-0 foo \n\nThis accessed the instance of myClass and called its member function foo.  Granted now most of this is limited to what the factory class knows how to create.  The other downside is this is slow....\n\nI wonder if using qtui and sending in XML would be faster...  It would be neat to do a \n#!/bin/bash\n#create the form\ndcop genericui-1111 createForm $SOMEXML\n#wait until a okay or continue button is clicked\ndcop genericui-1111 waitForOkay\n# dump the values, and do something with them\nVALUELIST=`dcop genericui-1111 getValueList`\nfor value in $VALUELIST\ndo\n echo `dcop genericui-1111 getValue $value`\ndone\n\nEh?\n\nI have found that for a few of my clients about 90% of there internal software is formbased VB, and that is what keeps them in Windows land.  Something along these lines may make a tempting migration path...\n\njust thoughts...  maby post 3.1 this will see the light of day...\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This is cool stuff"
    date: 2002-01-20
    body: "The main advantage of using the .ui file and widget factory is the ability to create the dialog graphically. If you want to create things on the fly then you can use another method of QWidgetFactory to create individual widgets. A dcop interface to this method would give you the abilities you want.\n\nRegarding the forms based applications, watch this space! I've already started one (and simple forms are working).\n\nRich."
    author: "Richard Moore"
  - subject: "Re: This is cool stuff"
    date: 2002-01-20
    body: "Same here, but my current rub is i need a light dynamic way to proxy objects through to dcop.  There are some classes that will get me about 75% of the way there, but due to limitations of what the QMetaObject stores I only have access to properties, signals and slots.\n\nCurrently I can so no other way to proxy the object other than bindings (very hard to maintain) or extending the metabobject (seems to add bloat).  What is your plan of attack?  If only C++ had a way to grab the Vtable :P\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This is cool stuff"
    date: 2002-01-21
    body: "Could something clever be done with libffi? \n\nThe bindings have '*Bridge' classes as proxies, but they don't have callbacks for all methods only really important ones like event handlers. You're right about extending the metaobject adding bloat, because you have to flatten the complete class hierarchy into each Bridge/Proxy class. So rather than just needing callbacks in the current class, you need them for all the methods in the parents too.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: This is cool stuff"
    date: 2002-01-21
    body: "Well ideally if one could proxy the public member functions then they could  use the QT metaobject to get the callback handlers.\n\nAbout the metaobject bloat, you can still keep the hirachy because  you can just call your parents methods from the table as the last argument, similar to the way dcop works.\n\nStill this is far from ideal but I will HAVE to research this libffi, by the lose information I hvae found thus far it looks like the most promiseing lead yet.\n\nBasicly what we want is a dynamic way to query the vtable of an object at runtime. \n\nOne last issue is handleing references to non-native data types, I actually have a system in place now that is similar to dcoprefs.  The cool thing is if this all got tied together one could interface with KDE/QT just by importing one object and accessing the native code from there....\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: This is cool stuff"
    date: 2002-01-20
    body: "BTW if you want to create XML on the fly and send it to the factory you can use the method that takes a QIODevice and write your XML to a QBuffer.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: This is cool stuff"
    date: 2002-01-19
    body: "\"Are there any plans to make a uic type tool for these bindings?\"\n\nYes, I've pretty completed a patch for the uic tool to generate Java. But it depends on polish() working as a callback, which needs to be added the next time the Qt bindings are regenerated. The bindings also need a Java Qt Designer plugin for syntax highlighting, but that doesn't look too difficult to do.\n\nI'm afraid I haven't told anyone much about the Objective-C bindings because there is a show-stopping run time link problem, which means I still haven't got the KDE Objective-C app 'Hello World' working yet. The Qt 3 Objective-C bindings work ok though.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Perl bindings ?"
    date: 2002-01-19
    body: "Dear Richard,\n\nThank you for the very good work !\n\nI would like to know what is the current state of KdePerl/PerlQt ? Is there still someone working on it ?\n\nLast time I checked the CPAN (1 month ago), it was still KDE 1.1 /Qt 1.4 bindings ... there was an option for Qt 2 but it seems PerlQt doesn't compile anymore on newer Qt versions (2.3+).\n\nIt's really too bad... I would love to code KDE apps (or at least Qt) with Perl ...\nIs there any hope that it will raise again ?"
    author: "Germain"
  - subject: "Re: Perl bindings ?"
    date: 2002-01-20
    body: "I haven't had a look at the KdePerl/PerlQt code. Does it use SWIG to generate the bindings? The interface files for SWIG take a lot of maintenance effort to look after. It might be easiest to add a perl bindings generation option to the kalyptus utility to generate the code. But use the existing perl bindings runtime 'as is'.\n\nThe first priority for me as a scripting language is Ruby. So I'm using Nobuyuki Horie's Ruby bindings as a starting point (they're are a bit out of date too), and trying to generate much the same code the SWIG does, but using kalyptus instead. I've just checked it into the CVS, usage 'kalyptus -fruby <Qt/KDE headers>'. Someone might be able to use that as a starting point for perl bindings generation - just copy kalyptusCxxToRuby.pm to kalyptusCxxToPerl.pm, and add an option '-fperl' to generate perl bindings in the 'kalyptus' perl source.\n\nThe main technical problem with languages like perl and ruby is that they don't have method overloading. For instance, in the QWidget class there are two methods called QWidget::setMinimumSize(), one which takes a QSize as an argument and one which takes two ints instead. In ruby you can only have one method with the name setMinimumSize(), so the bindings need to be able to dispatch to the correct C++ method according to the type of the first ruby argument. Or Horie's solution to the problem is to rename one of the methods 'setMinimumSizeInt()' to include the type of the first arg in the method name.\n\nThere is a similar problem with just one constructor name 'initialize()' in ruby, but there are usually several different constructors for a class in both Qt and KDE. So I would like to be able to automatically generate the correct 'dynamic despatch on first arg type' code, so that the methods wouldn't need either renaming, or the dynamic despatch code fixed up by hand. That's the main thing left to do for the kalyptus ruby option.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Perl bindings ?"
    date: 2002-01-20
    body: "Hello Richard,\n\n>Does it use SWIG to generate the bindings? \n\nI didn't look at kdePerl but no, QtPerl is incredibly good code : handcrafted and pure C++ !! There is not a single auto-generated line !... must be the most efficient thing possible with Perl but a pure Nightmare to maintain...\n\nIt seems very unlikely that someone else than the author could hack it for present or upcoming versions of Qt - and it seems the author has ceased to work on it. What a pity...\n\n>The interface files for SWIG take a lot of maintenance effort to look after.\n\nSure, SWIG is very good for small projects, but here one would have to add a bunch of perl code to provide a decent and clean interface... not to mention the signal/slot stuff...\nNot to mention that Parrot/Perl 6 will demand a full rewrite of everything (gosh, may be I'm sentimental but I really dislike the new syntax... this awful underscore thing ! argh...</pointless plug>)\n\n>Someone might be able to use that as a starting point for perl bindings \n>generation - just copy kalyptusCxxToRuby.pm to kalyptusCxxToPerl.pm, and add \n>an option '-fperl' to generate perl bindings in the 'kalyptus' perl source.\n\nVery nice ! I'm diving into it to see if there is a chance I could help...\n\n> The main technical problem with languages like perl and ruby is that they \n>don't have method overloading. \n[...]\n>Or Horie's solution to the problem is to rename one of the methods \n>'setMinimumSizeInt()' to include the type of the first arg in the method name.\n\nThis looks like a good solution...\nWith perl, it should be possible to AUTOLOAD the good method by appending the \"blessed\" C++ type of the first argument to the method name and then issuing a goto &$newmethodname... just an idea... maybe it's too much overhead..."
    author: "Germain"
  - subject: "Re: Perl bindings ?"
    date: 2002-01-21
    body: "\"Very nice ! I'm diving into it to see if there is a chance I could help...\"\n\nGreat! That's especially welcome from a Perl hacker, as you might be able to help tidy my messy code a bit, I'm just 'bluffing my way through' with minimal perl knowledge :). I've started working on a yapp grammar to parse the arguments to a method, which might make it better. At the moment that's parsing's done with regular expressions, so a bit of a mess..\n\nHopefully, you can use some of the runtime code for the existing perl bindings even if they weren't autogenerated.\n\nYou can have multiple passes through the methods in a class with kalyptus, which makes it easier to aggregate together code generated for all methods with the same name. SWIG is just one pass, so it wouldn't be possible.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Perl bindings ?"
    date: 2002-06-29
    body: "Ruby/Qt by Nobuyuki Horie has now hand-written code which checks the types of the arguments to a method and distinguishs this way which C++ funktion to call. Perhaps this could be reused?\nAnother importent point the overwriting of Qt's methods in Ruby. As Martin Man pointed out at the ruby-qt mailinglist (ruby-qt@yahoogroups.com), their should be inherited classes for every Qt class which overwrite every public and protected virtual method and call the Ruby methods if they exist. It's no fun hacking ruby-qt if it's impossible to override Qt's methods.\n\nOne comment about kalyptus: It generates code like this:\nrb_define_method(cQString, \"operator[]\", VALUEFUNC(_wrap_QString_operator[]), 1);\nwhich is obviously wrong, as the method's name in ruby must be simply [], not operator[].\n\nAnd finaly, one newby question: How is kalyptus supposd to work? I mean, how should I compile the generated code? First of all I do\n$ mkdir ruby-qt; cd ruby-qt\n$ kalyptus -fruby $QTDIR/include/*.h\n\nbut how to go on? All the generated files refer to ruby VALUEs, without #including <ruby.h>. And QWidget.cpp knows about cQObject, without knowing it's declaration.\n\nThanks so far,\nHeiner\n\nP.S. This thread is quite old, I just hope anybody will notice this post..."
    author: "Heiner"
  - subject: "Importand work"
    date: 2002-01-18
    body: "Personaly i think, that peoples like You made great job to KDE. Yours work is much more importand then Mosfet and other - i_am_graphics_or_other_artistics_talented_person - people. Sorry about my english, but i really mean it and this is importantd to me.\n\n\nP.S. I promise, that i will send my next comment when i will lern english :)."
    author: "Dyzio"
  - subject: "Re: Importand work"
    date: 2002-01-18
    body: "Your english is fine, and I agree with you whole heartedly. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Importand work"
    date: 2002-01-18
    body: "What's important is not to compare the work of different programmers that work on different things, but the fact they're bothing providing us with great free software."
    author: "Carg"
  - subject: "Re: Importand work"
    date: 2002-01-19
    body: "Thankyou! And I agree that everyone should cut out the 'mosfet knocking'. The KDE release schedules are tight and can be difficult to meet - and he had a problem syncing his work with the schedule. Then he pulled out for a while to reconsider his approach - so what! My reaction to the receent news on the .dot about mosfet's latest was just to immediately regenerate the java KImageEffect bindings, not to complain he'd done something 'individualistic'.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "What license does my application need?"
    date: 2002-01-18
    body: "Hi,\n\nwhen I am using the Java bindings, does my application have to be GPL or can it\nstay closed source?\nDo I have a different way of interacting with KDE or KDE applications than with the bindings if I don't want to change my license to GPL?\n\n\nMaybe the interview answers this questions, but I somehow can't access it.\n\nThanks"
    author: "JavaNovice"
  - subject: "Re: What license does my application need?"
    date: 2002-01-18
    body: "IIRC, the bindings are LGPL. That means the bindings do not require you to open your source. However, as you use QT, you need to obey its license which is GPL/QPL. Thus your program will still have to be open source unless you buy a QT commercial license."
    author: "ac"
  - subject: "Re: What license does my application need?"
    date: 2002-01-19
    body: "The C and Objective-C bindings are LGPL, and the Java bindings are GPL'd. I don't think you can write code compatible with the commercial Trolltech Qt license at the moment, only GPL. I had thought of adding a clause like this to the license from the GNU classpath project, but I think linking against a commercial Qt library would cause it to fail the test below:\n\n\"Classpath is licensed under the terms of the GNU General Public License with the following special exception.\n\nAs a special exception, if you link this library with other files to produce an executable, this library does not by itself cause the resulting executable to be covered by the GNU General Public License. This exception does not however invalidate any other reasons why the executable file might be covered by the GNU General Public License. \n\nAs such, it can be used to run free as well as proprietary applications and applets.\"\n\n-- Richard"
    author: "Richard Dale"
  - subject: "What is that patch like?"
    date: 2002-01-19
    body: "What is the nature of the manual edits? How much do they weigh?\n\nI wonder this because I'm considering making bindings for another language.\n\nThanks,"
    author: "exa"
  - subject: "Re: What is that patch like?"
    date: 2002-01-19
    body: "You can have a look at the patches - please email me and I'll send them. About 1 in 5 of the generated sources need some manual attention. \n\n 392 -rw-rw-r--   1 duke     wheel      396160 Oct  6 13:20 qtjava-0.20-cpp.patch\n 460 -rw-rw-r--   1 duke     wheel      465447 Nov 20 17:50 qtjava-0.21-cpp.patch\n\n 200 -rw-rw-r--   1 duke     wheel      197326 Oct  5 13:13 qtjava-0.20-java.patch\n 200 -rw-rw-r--   1 duke     wheel      197710 Nov 21 16:35 qtjava-0.21-java.patch\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: What is that patch like?"
    date: 2002-01-21
    body: "Perhaps the easiest way to see what manual fixup edits are needed is to generate the bindings code:\n\n$ kalyptus -fjava <Qt/KDE headers>\n\nThen use kompare to compare '.cpp' and '.java' files you've generated locally, with what's in the CVS.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: What is that patch like?"
    date: 2002-01-21
    body: "Perhaps the easiest way to see what manual fixup edits are needed is to generate the bindings code:\n\n$ kalyptus -fjava <Qt/KDE headers>\n\nThen use kompare to compare '.cpp' and '.java' files you've generated locally, with what's in the CVS.\n\n-- Richard"
    author: "Richard Dale"
---
Our friend Raphael Bauduin <a href="http://www.fosdem.org/interviews/1609.html">has interviewed</a> KDE bindings guru, Richard Dale, on <a href="http://www.fosdem.org/">FOSDEM.Org</a>. <i>"The bindings are regenerated for each KDE release. I take a patch of the manual edits that were needed for the previous release. Then run the new headers through kalyptus, and apply the patch of manual edits. Recently, it took about a week and a half to regenerate the bindings for C/Objective-C/Java, after about two months worth of KDE C++ header changes from the previous release."</i>
Richard has done an excellent job, almost single-handedly paving the way for new and exciting KDE bindings.  If C++ is not your forte, be sure to investigate his work and check the <a href="http://developer.kde.org/language-bindings/index.html">KDE bindings page</a> for other sparse details.
<!--break-->
