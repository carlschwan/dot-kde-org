---
title: "Freehackers.org Interviews KDevelop Team"
date:    2002-08-16
authors:
  - "sky\u00f6stil\u00e4"
slug:    freehackersorg-interviews-kdevelop-team
comments:
  - subject: "Noncurrent interview"
    date: 2002-08-16
    body: "In the meantime HEAD only contains \"Gideon\" because it was found to become mature."
    author: "Anonymous"
  - subject: "Best quote from the article"
    date: 2002-08-16
    body: "\"First I was just a bugger and bothered the list\"\n\nAt least he wasn't a dirty bugger.\n"
    author: "Phillybo"
  - subject: "Will there be an alpha-Release of KDevelop 3.0 ?"
    date: 2002-08-16
    body: "I noticed on www.kdevelop.org in the changelog that in start of August the code taken to prepared it for an alpha-Release.\nAnybody got an idea if this alpha will show up in the near future ?\n\nIs the CVS-Version of Gideon(KDevelop 3.0) already usable to test it on a c++-project ?\nHas anybody tried to compile it? What is your impression? Does it already has all features of KDevelop 2.1?\n\nIf anybody has tested it ... please give some feedback\n\nBye\n     Jens Henrik"
    author: "Jens Henrik"
  - subject: "Re: Will there be an alpha-Release of KDevelop 3.0 ?"
    date: 2002-08-16
    body: "In KDE 3.1 beta1."
    author: "Morty"
  - subject: "Re: Will there be an alpha-Release of KDevelop 3.0 ?"
    date: 2002-08-16
    body: "On LinuxTag 2002 in Karlsruhe we presented a copy of Gideon\nand I was very much impressed. Matthias showed me some really\nnice features and what I saw worked very nice. But I didn't \ntry to code on that machine."
    author: "Carsten"
  - subject: "Other platforms"
    date: 2002-08-17
    body: "Just curious, will there be versions for Win 32 and/or Mac OSX and would they neeed to use the X11 version of QT on these platforms?"
    author: "Fredrik C"
  - subject: "Re: Other platforms"
    date: 2002-08-17
    body: "Although I'm not involved in the project, the usage of kparts and other kde-features will make it at least difficult to provide a qt-only version. As far as I know, there doen't exist a kde3 environment for osX or win32.\n\nSo, it *could* be possible to have versions for those, but it is not very likely.\n\n( hey, but at least that's a reason more for using kde3  ;] )"
    author: "Anonymous Coward"
  - subject: "Re: Other platforms"
    date: 2002-08-17
    body: "\nOSX gets regular, up to date KDE releases just like any Linux distribution.  They are put out by Fink (fink.sf.net).\n\nWin32 only has KDE 2.x, and it's not fully functional, works inside a special window (the XFree86 desktop), etc.  But then, KDE is a environment for *nix, and always has been.  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
---
In a recent <a href="http://www.freehackers.org/interviews/kdevelop-2002-08/">interview</a> conducted by <a href="http://www.freehackers.org/">freehackers.org</a>, the hard working people behind the <a href="http://www.kdevelop.org/">KDevelop IDE</a> talk about the history of the project and their contributions. Light is also shed on the somewhat confusing set of branches that exist for KDevelop and the exciting features of the upcoming Gideon (a.k.a. KDevelop 3). The second part of this interview is due next week.

<!--break-->
