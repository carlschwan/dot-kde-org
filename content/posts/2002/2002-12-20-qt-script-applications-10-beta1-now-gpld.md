---
title: "Qt Script for Applications 1.0 beta1, now GPL'd"
date:    2002-12-20
authors:
  - "numanee"
slug:    qt-script-applications-10-beta1-now-gpld
comments:
  - subject: "QT Script"
    date: 2002-12-19
    body: "This looks like it would be very useful. Can you write whole Qt apps with it, or is it limited to just playing a supporting role to C++?"
    author: "AC"
  - subject: "dcop"
    date: 2002-12-19
    body: "how much does this overlap with DCOP?"
    author: "me"
  - subject: "Re: dcop"
    date: 2002-12-20
    body: "Not at all. It does overlap with kjs though, and to a certain extend also with wrappers for other scripting languages (because qsa came up with a very simple way to allow a scripting langage to access C++ objects)."
    author: "Tim Jansen"
  - subject: "ARexx"
    date: 2002-12-19
    body: "So this is kinda like ARexx-ports in old Amiga programs, right? If so, cool."
    author: "ac"
  - subject: "Re: ARexx"
    date: 2002-12-26
    body: "> So this is kinda like ARexx-ports in old Amiga programs, right? \n> If so, cool.\n\nWell, it looks like...\n\nHowever, it is all but cool ! Consider this: In 2002 we get an IPC/process-control scripting system, that is just like what was available in 1990 on AmigaOS. That is 12 years behind the time.\n\nI can forgive the inventors of this QT/KDE approaches, assuming they never used an Amiga. But hey ! There were so many serious problems with ARexx for any who was doing a little more.\n\nTodays choice should be a completly platform and application(-environment) scripting-server, better call it API-server, that neither requires you to learn YASL (yet another scripting language) nor is bound to a certain UI toolset.\n\n'dcop' somewhat could have been something like this, assuming, that it is possible to convert SOAP and CORBA calls. Of course, that is rather complex.\n\nWhat Linux (not GTK+/Gnome or QT/KDE) need is a standardized general and system- (even network-) global API server. Applications could add their own functionality (command set) to it just as did the good old 'ARexx host-port' in AmigaOS applications, while the possibility to address these API calls would be granted to any language.\n\nGo abstract ! Not specific again...\n\n.jon (long time (still) Amiga user and ARexx coder)"
    author: ".jon"
  - subject: "KJS?"
    date: 2002-12-19
    body: "Haven't we been able to do something similar for a while with the embedded version of KJS?"
    author: "ac"
  - subject: "Re: KJS?"
    date: 2002-12-19
    body: "Yeah, I'm not really informed too well on this. Maybe Rich can say how QAS and KJS relate to each other.  Of course there's DCOP in there too."
    author: "Navindra Umanee"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "Looking over the documentation the two seem to be virtually identical.\n\n- QAS has a nice built in script editor which kjsembed does not, but kjsembed can load designers ui files anyway and it would be easy to add.\n\n- KJSEmbed has facilities for working with KActions (I will soon be committing a new version which will load XML files that define actions). QAS doesn't have this, but could be made to.\n\n- KJSEmbed has the full Javascript language available, while QAS only has a subset. eg. KJSEmbed can use the standard javascript Math and RegExp facilities.\n\n- KJSEmbed can access the DOM from a KHTML part so it can work nicely with HTML and XML documents.\n\n- KJSEmbed is LGPL while QAS is GPL.\n\nHope this sheds some light.\n\nRich.\n\n"
    author: "Richard Moore"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "Is KJSEmbed already able to call the slots of a QObject?\n"
    author: "Tim Jansen"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "Yes, you can access the slots of objects as if they were normal js methods, and access the properties as if they are normal js properties. You can also navigate the object tree to get at child widgets etc. Not all of this is in cvs yet as I am finishing off an API cleanup and documentation exercise, but now that HEAD is open again I'll commit it.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "Oh, wow.  Did we ever announce this?\n\nWhere is the documentation for KJS?  I tried to find it on developer.kde.org but it wasn't where I thought it would be (http://developer.kde.org/language-bindings/tools/index.html)."
    author: "Navindra Umanee"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "The docs aren't online at the moment (until recently they weren't even written!). You'll be pleased to hear that part of my documentation exercise it a tutorial for the dot.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "I've generate the docs and uploaded them to http://xmelegance.org/kjsembed/ Note that they are unfinished and there are now some better ways to things - I've been using the library in xmelegance so I've been improving the API as I go along.\n\nThe new XMLActionClient class is particularly nice - here's an example script that loads some actions using it:\n\n<!DOCTYPE actionset>\n<actionset>\n<header>\n    <name>test_actions</name>\n    <label>Test Actions</label>\n</header>\n<action>\n   <name>file_quit</name>\n   <type>KStdAction</type>\n   <script type=\"js\">app.quit()</script>\n</action>\n<action>\n    <name>show_sidebar</name>\n    <type>KToggleAction</type>\n    <label><text>Show &amp;Sidebar</text></label>\n    <icons>tree_win</icons>\n    <shortcut>Ctrl+T</shortcut>\n    <script type=\"js\">window.toggleSidebar()</script>\n</action>\n<action>\n    <name>show_messages</name>\n    <type>KToggleAction</type>\n    <label><text>Show &amp;Sidebar</text></label>\n    <icons>output_win</icons>\n    <shortcut>Ctrl+M</shortcut>\n    <script type=\"js\">window.toggleMessages()</script>\n</action>\n\nCheers\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: KJS?"
    date: 2002-12-20
    body: "Yeah GPL sux, it shoud be bsd (or lgpl) for commercial use.\n<p>\ncheck <a href=\"http://dot.kde.org/1037348700/1037568690/1037621798/1037624498/1037631211/1037638725/1037642772/\">this</a>"
    author: "Anton Velev"
  - subject: "Re: KJS?"
    date: 2002-12-21
    body: "The examples in the article you referred to, are flawed.\n\nFor example it doesn't matter for an application which license kicker is based on if it just wants to dock to the system tray.\n\nThe application just creates its tray handler and tells the window manager that it wants it docked into the system's tray.\nThe tray area might be provided by kicker.\nNo linking, no license problem.\n\nSame for the embedding example.\nThe application links against the LGPL interface, the system decides which component to load.\nThe application only links against LGPL code, no license problem.\n\nThat's why most (all?) KDE base libraries, including koffice libs, are LGPL.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "GPL makes sense commercialy"
    date: 2002-12-23
    body: "It doesn't make sense for Trolltech to distribute their developments as LGPL or BSD-style : that way commercial applications don't need to license the Trolltech code == 'Trolltech can't make money' , which in turns translates to 'Trolltech does not develop new stuff' == 'Everyone suffers'.\n\nYou want Trolltech to make money. Q.E.D."
    author: "Trestop"
  - subject: "Re: GPL makes sense commercialy"
    date: 2002-12-23
    body: "Further, TrollTech could, in future, aim for compatibility with the KDE\nKJSEmbed, so that a common foundation is found between the two projects.\nThe LGPL nature of KJSEmbed leaves this open as a possibility without \naffecting TrollTech's GPL license on their scripting product.\n\nThus there is an incentive for Qt to improve both, and to pass some\nof their (older) innovations back.  Essentially, they can, on the \nGNU/Linux/*NIX/whatever/else/should/go/here... end of things\naim to be a short-term commercial edge over the KDE solution.\nThis would bring money into the system, via developers who would\nwork for that money.\n\nJust a thought, though a lot of my thoughts are rather silly at the moment.\n\nMerry Christmas,\n\nJohn"
    author: "John Allsup"
  - subject: "Re: KJS?"
    date: 2002-12-21
    body: "Yep, and the Python language bindings too. There's two implementations of scripting extensions to Qt right there. Why develop _another_?\n"
    author: "Richard"
  - subject: "Re: KJS?"
    date: 2002-12-21
    body: "I don't think you understand what this is for.  This is for application extensibility for an application already written in C++.  Besides, who is going to teach the users Python to do that?  You think Python is a simple language for a newbie?  JavaScript everybody already knows and is very simple to use and modify even if you don't understand much about it."
    author: "ac"
  - subject: "wow..."
    date: 2002-12-19
    body: "this reminds me of applescript"
    author: "fault0"
  - subject: "Other languages?"
    date: 2002-12-19
    body: "It would be cool if this framework could be adapted to load support for other languages like Python as plugins. The GUI looks great!"
    author: "$c0rpi0n"
  - subject: "Re: Other languages?"
    date: 2002-12-20
    body: "Actually extending this to python would be quite easy. You just need to make an equivalent of the JSObjectProxy class in KJSEmbed and you're done. It is only a few hours work.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Other languages?"
    date: 2002-12-21
    body: "Could you do this? I am waiting forever for a good way to script Python for KDE, since IO want to build a fat client for the Python-based Zope application server. Please do that; I will test it for you! :-)"
    author: "Stephan Richter"
  - subject: "Re: Other languages?"
    date: 2002-12-24
    body: "PyKDE already lets you write applications (and otherwise \"script\") KDE.\n"
    author: "Richard"
  - subject: "Why?"
    date: 2002-12-20
    body: "Why on Earth did they develop Yet Another Goddamn Language?\n"
    author: "Richard"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "Why on earth do people make comments about things they don't know much about?\n\nLast I checked, ECMAScript/JavaScript/JScript was in wide use in those exotic programs called web browsers.\n"
    author: "Sad Eagle"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "Read it again. \"based on\". They've implemented their own.\n"
    author: "Richard"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "My turn again ;-). It's a subset, not really new language. Read just a tad further ;-)\n"
    author: "Sad Eagle"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "Hey leave Tad out of this!  : )"
    author: "anon"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "Creating an entirely new language would have been vastly better than sub-classing Javascript.  Javascript is, simply put, the most terrible language to have ever plagued the computing world.  Javascript is a case of a worthless technology being paired with a powerful, popular technology, and thereby gaining ubiquity.  This echos Microsoft's success, who rode in on IBM's coattails.\n\nIn any case, I expected better from the Qt folks."
    author: "Sean Russell"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "simple question: what make you believe that javascript is a bad language ?"
    author: "cylab"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "Why is JavaScript bad? Because 10000 web designers didn't understand it and/or used it to create broken web pages? \n\nJavaScript is a beautiful object-oriented scripting language that, unlike all other popular languages, uses a prototype-based object system which is much easier to learn and understand for a newbie, IMHO.\n(prototype-based = you don't write classes. Instead you edit a object in real time, add methods and so on, and then clone it)\n"
    author: "Tim Jansen"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "Ignoring, for the moment, that there isn't a single *good* implementation of the ECMAScript interpreter; ignoring, for the moment, that the design of JavaScript encourages bad code design; I'll focus on what I feel the real weaknesses of JavaScript are (and I freely admit that some/many of these are unmeasurable, and therefore, opinions):\n\n* The language rules are ambiguous and anti-PoLS.  Example:\n\n   a = x.y\n\nWhat does this mean?  Is it a method evaluation?  Is it a method object assignation?  Is it a method variable evaluation?\n\n* Arbitrary rules for variable declarations... to \"var\" or not to \"var\"?  A further source of confusion, yet another opportunity for bug introduction, yet another opportunity for bad code.\n\n* The strongest arguments *for* JavaScript are for what you can do with it.  That is, it is lighter-weight than Java, and you can do client-side processing.  This isn't an argument for JavaScript; this is an argument for *some* scripting mechanism on the client-side.\n\n* DOM.  DOM sucks, and JavaScript uses DOM.  DOM is heavy and unintuitive.  The actual API behavior seems to vary between browsers.\n\n* JavaScript looks like it was designed by a committee.  Oh, wait... it *was* designed by a committee.  While this looks like a cheap shot, it is actually my strongest criticism.  Both DOM and JavaScript are unintuitive, and therefore require a high cost of entry as well as a high cost of maintenance.\n\n* JavaScript is brittle.  It is brittle because the specification is poorly written.  Poorly written specifications are difficult to implement.  Difficult implementations are error prone.  Therefore, all of the problems that result from bad implementations of the interpreter can be traced back to the bad (ambiguous), formal, specification, and can thereby be attributed to the language itself.\n\n* I can't forgive the lack of a formal class definition mechanism.  Loose typing is useful, runtime class extension is a nifty feature, but ad-hoc classes is a terrible idea.  It makes source maintenance a nightmare, and this in itself is enough of a reason to classify JavaScript as a bad language."
    author: "Sean Russell"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "a = x.y\nassigns the value of x's member y to a. What's so difficult about it? \n\nThe var rule is easy: use var if you want to create a new variable that has the scope of the current block. Otherwise you don't need var.\n\nHow could DOM be made easier? If its implementation is bad, this doesn't mean that the language is bad (and thus a bad choice for QSA). \n\nAFAIK JavaScript was not designed by a comittee, but by Netscape.\n\nAnd the lack of a class definition is possibly JavaScript's best feature, because it allows relatively powerful features in a very small language. Also note the word 'Script' in JavaScript. No one said that you should create huge programs with it. "
    author: "Tim Jansen"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "> Ignoring, for the moment, that there isn't a single *good*\n> implementation of the ECMAScript interpreter;\n\nCan you please tell us what kind of problems you have found in all ECMAScript implementations out there ?\n\n> * Arbitrary rules for variable declarations... to \"var\" or not to \"var\"? A\n> further source of confusion, yet another opportunity for bug introduction,\n> yet another opportunity for bad code.\n\nAn implementation is free to warn about a missing \"var\". The QSA interpreter does so, other implementations don't even compile code without it.\n\n> * DOM. DOM sucks, and JavaScript uses DOM. DOM is heavy and unintuitive.\n> The actual API behavior seems to vary between browsers.\n\nWhat does the DOM have to do with ECMAScript ?\n\n> JavaScript is brittle. It is brittle because the specification is poorly\n> written. Poorly written specifications are difficult to implement. Difficult\n> implementations are error prone. Therefore, all of the problems that result\n> from bad implementations of the interpreter can be traced back to the bad\n> (ambiguous), formal, specification, and can thereby be attributed to the\n> language itself.\n\nCan you point us to any ambiguities in the specification ? The maintainer would certainly be interested to know. Apart from the know problems listed at\n\n http://www.mozilla.org/js/language/E262-3-errata.html\n\nI couldn't find anything wrong after reading through the standard a dozen times.\n\n> * I can't forgive the lack of a formal class definition mechanism.\n> Loose typing is useful, runtime class extension is a nifty feature,\n> but ad-hoc classes is a terrible idea.\n\nQSA is following the \"strict mode\" upcoming Edition 4 of ECMAScript. It knows about classes, typed variables, access specifies and everything you can dream of. Check out the current draft here:\n\n  http://www.mozilla.org/js/language/es4/index.html\n\nHarri."
    author: "Harri"
  - subject: "Re: Why?"
    date: 2004-04-08
    body: "Why do you people defend JavaScript? It really is a pile of crap. The DOM is a pile of crap too. IMO you should use client side scripting sparingly.\n\nAre you criticizing him just because his argument may not be \"well-formulated\" to you?\n\nIf so than you are giving JavaScript a defense that it does not deserve. JavaScript is a complete pile of crap, as is every other client-side scripting technology. There simply are not any good ones.\n\nClient side technologies make the Web what it shouldn't be: unportable.\n\nGet a life instead of nit-picking the details of someone's argument.\n\nSean Russel I applaud you.\nThis message board is a pile of sh*t."
    author: "Steve"
  - subject: "Re: Why?"
    date: 2004-04-09
    body: "Would you care to restate your questions without the insults? If you do, then I'm willing to answer them. Grow up.\n"
    author: "Richard Moore"
  - subject: "Re: Why?"
    date: 2006-12-21
    body: "As opposed to what?  Ad-hoc browser-specific behaviour?  Or maybe you'd prefer no behavioural control at all, in which case we should toss :hover, :focus, :active and a few other pseudoclasses out of CSS... oops, then people won't know if what they're hovering over is really a link or not!\n\nUnportable?  HTF does client-side scripting make the Web unportable, outside of stupid, badly written browser-specific scripts which really should be tossed.  Screen and projection clients mostly support EMCAScript decently; handheld doesn't, but that's to be expected (which means using client-side code on handhelds is plain STUPID), and I still wonder why people expect EMCAScript to work in aural and print modes.  Uh, print is STATIC, people!  And dynamic updating of aural is confusing and downright stupid.\n\nSites should not depend on client-side scripting, but no client-side scripting doesn't make anything better.  In fact, it generally makes things worse, because then one layer of the program design is missing."
    author: "Dark Phoenix"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "And it is used not only in web browsers, but in industrial environments as a glue language. So Qt is definitely making the right move here.\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "i prefer the cross-language nature of DCOP. the tool shouldn't *ideally* impose the language but let you choose it...\nto me it's a setback from at least the objectives of DCOP scripting (didn't try it yet in the real world, so i can't tell for its current status)."
    author: "emmanuel"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "But dcop is quite limited and only useful for very simple scripts. It doesnt help you when you create a Qt/KDE GUI, or extend a class. And I wouldnt want to use it to access complex data."
    author: "Tim Jansen"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "But my main point is - there's already extremely right bindings to Qt for Python, a very mature language. They've gone and developed their own language from scratch. Give it a few generations and it might be as bug-free as Python, but you still wouldn't have access to the extraordinarily large libraries that Python already has. \n\n*shrug* it just doesn't make sense to me.\n"
    author: "Richard"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "NO!  Your point is wrong.  This is not a language from scratch.  This is based on standard ECMAscript and is designed to be used for application extensibility by the USER.  Not necessarily for general purpose bindings by the developer.\n\nMaybe you don't understand the concept of application extensibility?"
    author: "ac"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "Which already-developed ECMAscript implementation did they use? Which off-the-shelf already-existing codebase did they use to implement their runtime? My point is that they have to develop that from scratch. Sure, they followed the ECMAscript _standard_, but that ain't code.\n\nAnd python's trivially embedded as an application scripting language.\n"
    author: "Richard"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "Your point is that they had to develop from scratch?  Sorry, I don't care for your point.  *yawn*\n\nPython is trivial to embedd as a Qt scripting language?  Uh-huh.  Then why didn't you already do it?  I suggest you try it first, then try to deploy it, and then come back with proven arguments."
    author: "ac"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "Of course it is trivial. See http://www.thekompany.com/projects/vp (although the code isn't currently maintained)."
    author: "Phil Thompson"
  - subject: "Re: Why?"
    date: 2002-12-22
    body: "You don't care about the lengthy process we now have to go through to debug their implementation of the language, rather than use another, already mature language? Are you willing to even point at ONE existing ECMAscript implementation that's relatively bug free?\n\nOh wait, there's always KJS, which is mature. Hey, and as someone's already pointed out, it does what this new QSA is designed for. Are you beginning to see my point yet?\n"
    author: "Richard"
  - subject: "Re: Why?"
    date: 2002-12-22
    body: "> You don't care about the lengthy process we now have to go through \n\nWe?\n\n> Oh wait, there's always KJS, which is mature.\n\nYou realize that the primary author of KJS is working\nfor Trolltech? ;-)\n\n> Hey, and as someone's already pointed out, it does\n> what this new QSA is designed for. \n\nNo, you just show that you haven\u00b4t understood what QSA is.\nKJS is just a ECMAScript interpreter, which is just a\ncomponent in a comprehensive scripting framework.\n"
    author: "Bernd Gehrmann"
  - subject: "Re: Why?"
    date: 2002-12-24
    body: "So QSA uses the existing KJS implementation?"
    author: "Richard"
  - subject: "Re: Why?"
    date: 2002-12-20
    body: "This is what ECMAscript was designed _for_.  Period."
    author: "ac"
  - subject: "Re: Why?"
    date: 2002-12-21
    body: "As others pointed out it is not yet another language. It just has a new name because it offers an extended (Qt) API on top of the ECMAScript language. The standard is actually designed with this goal: a fixed language core embedded into a host environment. What objects make up this extension environment is up to the implementor. Can either be a DOM API in a browser or any objects in an application.\n"
    author: "Harri"
  - subject: "Comparable to M$ VisualBasic?"
    date: 2002-12-20
    body: "Is it wrong to see QTSA as a system-independent alternative to VB? If yes IMHO it will boost the acceptance of\nQt & KDE since VB seems to be the language for those who aren't programmers but want to write simple programs / extend existing programs from time to time. "
    author: "Ruediger Knoerig"
  - subject: "Re: Comparable to M$ VisualBasic?"
    date: 2002-12-20
    body: "Well though i write asm, c++ and c i still think sometimes there is better to use VB than start writing comple MFC or QT that takes pointless time. So do say that VB programmers are not programmers is lame :P"
    author: "Margus Eha"
  - subject: "Re: Comparable to M$ VisualBasic?"
    date: 2002-12-22
    body: "Sorry, I meant people which don't have much experiences & knowledge in information science but want to adapt / extend existing applications. Yes, you are all-right - to say it with Stroustrup: Choose the correct language for your problem. "
    author: "Ruediger Knoerig"
  - subject: "Re: Comparable to M$ VisualBasic?"
    date: 2002-12-20
    body: "Right now, it is an alternative to VBA, not VB. (With some work it would probably be usable for stand-alone apps though)"
    author: "Tim Jansen"
  - subject: "Python...."
    date: 2002-12-20
    body: ".... plus pyQT/pyKDE does this stuff already, doesn't it?\n\nAlthough I must admit, the dropdown for the intellisense looked really juicy in that screenshot!"
    author: "Tsali"
  - subject: "Re: Python...."
    date: 2002-12-20
    body: "No it doesn't.\n\nWith PyQt, you have to write a .sip file for each C++ class you want to\nmake available, and compile that. This is not only time-consuming and a \nmaintainance burden, it also produces considerable bloat. The idea of Qt\nScript is to use the introspection features and the marshalling code\nwhich is produced by moc anyway. No extra code to write."
    author: "Bernd Gehrmann"
  - subject: "Re: Python...."
    date: 2002-12-20
    body: "It sounded to me from reading their FAQ that Trolltech was writing bindings into their C code anyways to make this thing work.\n\nThank you for your clarification.\n\n"
    author: "Tsali"
  - subject: "Re: Python...."
    date: 2002-12-21
    body: "QSA can access all slots of a QObject and its properties. So if you want to make a non-QObject accessible, you either need to convert it to a QObject, or write wrappers. "
    author: "Tim Jansen"
  - subject: "Re: Python...."
    date: 2002-12-21
    body: "This is somewhat misleading.\n\nIf you read the QSA docs you see that you are encouraged to write a new QObject sub-class that only exposes the functionality of your application that you want the user to see. This is more complicated and time consuming than writing a .sip file for your class instead and will generate a similar amount of bloat.\n\nIt's missing the point anyway. PyQt is an alternative to C++ for application development - it's not an alternative to QSA. As has been pointed out elsewhere it would be fairly trivial to produce a library that uses Qt's introspection features to create Python objects and feed them to the interpreter. Such a library would complement PyQt - and you get the benefit of using a single language for both application development and user scripting."
    author: "Phil Thompson"
  - subject: "Re: Python...."
    date: 2002-12-22
    body: "> If you read the QSA docs you see that you are encouraged to write\n> a new QObject sub-class that only exposes the functionality of your\n> application that you want the user to see.\n\nWho says that you have to follow that recommendation? In my experience,\nscripting interfaces are most useful if they expose _all_ interfaces\nof the application. That makes Emacs so flexible. In general, scripting\ninterfaces written by someone who thinks he knows which interfaces are\nuseful tend to suck.\n\n>This is more complicated \n\n??? QObjects are something a Qt programmer writes every day, so it\nis absolutely trivial.\n"
    author: "Bernd Gehrmann"
  - subject: "Re: Python...."
    date: 2002-12-22
    body: "> > If you read the QSA docs you see that you are encouraged to write\n> > a new QObject sub-class that only exposes the functionality of your\n> > application that you want the user to see.\n\n> Who says that you have to follow that recommendation? In my experience,\n> scripting interfaces are most useful if they expose _all_ interfaces\n> of the application. That makes Emacs so flexible. In general, scripting\n> interfaces written by someone who thinks he knows which interfaces are\n> useful tend to suck.\n\nIf you want to expose all the interfaces then you have to hope that everything is a signal or slot and that you haven't implemented something useful as an ordinary public or protected method - otherwise you still need your wrapper class.\n\nYou also need to think carefully about who your scripters are. If you have a vertical market application and use QSA to allow your system implementors to configure the application for a particular customer then it makes sense to expose the full interface. If your scriptors are non-programmer end users then you probably need to think about restricting what's available to them - otherwise you just create huge problems for your support department.\n\n> >This is more complicated \n\n> ??? QObjects are something a Qt programmer writes every day, so it\n is absolutely trivial.\n\nI didn't say it was complicated to write a new wrapper class, I said it was more complicated than writing a .sip file for an existing class."
    author: "Phil Thompson"
  - subject: "Re: Python...."
    date: 2002-12-23
    body: "> If you want to expose all the interfaces then you have\n> to hope that everything is a signal or slot and that\n> you haven't implemented something useful as an ordinary\n> public or protected method - otherwise you still need\n> your wrapper class.\n\nNo, you just have to declare the method a slot, that\u00b4s\nall. And in the other direction, if you want to hide\nsomething, you just declare it private. No wrapper\nclass is needed for that.\n\n> I didn't say it was complicated to write a new wrapper\n> class, I said it was more complicated than writing\n> a .sip file for an existing class.\n\nAnd I say it\u00b4s not more complicated, it\u00b4s trivial.\nIn particular, you have the additional benefit that\nthe interface is reusable by the DCOP bridge for\nout-of-process automation, by any other bridge (like\nthe one I use in Dox), and that qmake or am_edit\ntake care of the build issues."
    author: "Bernd Gehrmann"
  - subject: "QT just filling in the gaps"
    date: 2002-12-20
    body: "Since QT is trying to become a cross platform API, I think this move is a first step in filling in the missing peices - the greatest of which is the lack of a full featured HTML renderer.  They can't just tell a customer to load KHTML since this implies a lot of other dependencies and may break the cross platform feature.  QT has got to recreate alot of the KDE work as new QT features so as not to be dependent on KDE."
    author: "Timo"
  - subject: "Re: QT just filling in the gaps"
    date: 2002-12-20
    body: "Actually, the core of KHTML is not even dependent on Qt. That's why it has been ported to platforms such as Atheos. I suspect the maintainance overhead is the main reason it's not in Qt.\n\nRich.\n"
    author: "Richard Moore"
---
Trolltech <a href="http://www.trolltech.com/company/announce.html?Action=Show&AID=116">has announced</a> a new beta of <a href="http://www.trolltech.com/products/qsa/">Qt Script for Applications</a>.  In the words of the blurb: <i>"The QSA toolkit is made up of the following components: QSA library, which the developer of the C++ application uses to make their Qt-based applications scriptable. 
Qt Script, an easy-to-learn, multiplatform interpreted scripting language. Qt Script is based on the ECMAScript standard (as is JavaScript) . Qt Scripter, a multiplatform IDE which developers can make available to their end-users. Qt Scripter (<a href="http://www.trolltech.com/images/qsa/qsa_designer.png">screenshot</a>) can be used to write and edit code, to visually design forms, and to run and debug scripts."</i>  With this release, QSA is now available under the GPL.  Very cool stuff and should bring a whole new level of application <a href="http://developer.kde.org/language-bindings/tools/index.html">extensibility</a> to KDE/Qt applications that need it. <b>Update: 12/20 18:09</b> by <a href="mailto:navindra@kde.org">N</a>:  Looks like we forgot about KDE's own <a href="http://dot.kde.org/1040319889/1040329437/1040334024/1040387004/">KJSEmbed</a> (<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/kjsembed/README?rev=1.3&content-type=text/x-cvsweb-markup">README</a>, <a href="http://xmelegance.org/kjsembed/">doc</a>).
<!--break-->
