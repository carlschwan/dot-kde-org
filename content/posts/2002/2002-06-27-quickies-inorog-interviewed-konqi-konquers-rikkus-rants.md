---
title: "Quickies: Inorog Interviewed, Konqi Konquers, Rikkus Rants"
date:    2002-06-27
authors:
  - "numanee"
slug:    quickies-inorog-interviewed-konqi-konquers-rikkus-rants
comments:
  - subject: "Thanks Rikkus!"
    date: 2002-06-27
    body: "Thank you, Rik, for your very well written response to dep! You spoke off my\nheart.\n\nCiao,\n  Michael"
    author: "Michael"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-27
    body: "Yeah, thanks Rik.  \n\nI have to admit that despite me, dep really managed to get my spirits down with his bullcr*p FUD, ruining a whole evening for me.  Although, it was only a matter of time after he posted that anti-german article...\n\nAlready feeling better with a new KOffice out.  :)"
    author: "Navindra Umanee"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-27
    body: "Huh? An anti-German article?? Did I missed something? Do you have a link?\n\nBTW, congrats to the Gnomes. I like this kind of desktop competition. Its good for KDE to see another solution for system programming problems as a kind of inspiration and motivation. So...\n\nGo Gnome, go KDE!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-27
    body: "Mail me, I'll send you the link.  I don't really want to spread his message of hate here."
    author: "Navindra Umanee"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-28
    body: "Why hating, isn't loving or respecting a better solution?\nCollaborate, not fight.........."
    author: "Maarten Romerts"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-28
    body: "Actually, I disagree, it wasn't a very good response, it talked in generalities and rebuted no specific claims DEP had just calling them \"difficult to disprove untruths\" and dismissed them. Possibly that attitude is what DEP was referring to in the article? Finally, you cannot claim DEP was feigning \"some kind of friendliness\" towards KDE as for the past 3 or 4 years, it was hard to find anyone lessKDE friendly. Are you saying that this was all some sort of act?"
    author: "Hmmm"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-28
    body: "It's about not descending to dep's level.  Dep is at the lowest level."
    author: "ac"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-28
    body: "Oh, thats a good excuse. Seeing as the DEP's article gave examples and rikkus' one flamed DEP, called him a liar and many other things, I think I know which article was \"the lowest level\""
    author: "Hmmm"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-28
    body: "Rikkus didn't say anything about dep at all."
    author: "ac"
  - subject: "Re: Thanks Rikkus!"
    date: 2002-06-29
    body: "It was a sly dig at him. Who else has written such an article lambasting KDE, or is it just a coincidence that Rikkus wrote his article less than a week after DEPs?"
    author: "hmm"
  - subject: "You asked for it!"
    date: 2002-06-29
    body: "<blockquote>Once things are done a certain way, headed in a certain direction,\nit's really tough to alter them. That's what keeps Windows on millions of\nmachines whose owners hate that operating system. And it's what keeps KDE on my\ndesktop, at least for the moment.</blockquote>\n\n<p>The KDE project is like Microsoft.</p>\n\n<blockquote>I have been using KDE as my Linux desktop ever since the\nday[...]</blockquote>\n\n<p>I'm an old timer, so I know what I'm talking about.<p>\n\n<blockquote>I kept using XFMail, Netscape, StarOffice, NEdit, and whatever else\nI'd been using all along -- I didn't even, at first, employ KPPP as a dialer,\nthough I'd migrate to it quickly and to KMail soon thereafter.</blockquote>\n\n<p>I didn't actually use KDE 1, apart from the window manager bit.</p>\n\n<blockquote>The reason I might make the switch back is that after many months\nusing it, I've come to the conclusion that KDE3 kind of sucks.</blockquote>\n\n<p>I don't like KDE 3.</p>\n\n<blockquote>Initially, KDE3 was going to be little more than KDE2 ported to\nQt3. That is what it should have been. Then feature creep set in.</blockquote>\n\n<p>They made some relatively minor changes to the user interface which I didn't\nlike.</p>\n\n<blockquote>Then came an unhappy, \"we know more than users do,\" attitude on the\npart of developers.</blockquote>\n\n<p>So I whined on a mailing list and was shocked to find that everyone\ndisagreed with my opinions. <em>My</em> opinions !</p>\n\n<blockquote>changes were made; none that I can find that add to the utility of\nthe system</blockquote>\n\n<p>All these <a\nhref=\"http://developer.kde.org/development-versions/kde-3.0-features.html\">changes</a>\nwere stupid.</p>\n\n<blockquote>But I think that much of what I don't like about KDE3 is more a\nchange in the attitude of the KDE developer community than anything else; the\njump to Qt3 need not have been as wrenching as it was.</blockquote>\n\n<p>People found my constant whining and refusal to listen annoying.</p>\n\n<blockquote>Take a look at freshmeat under email clients: you'll find a forest\nof GNOME apps, but KDE is pretty limited.</blockquote>\n\n<p>It's much better to have 10 half-finished applications than 1 perfect\none.</p>\n\n<blockquote>Yes, it's possible still to run third-party apps written to another\ntoolkit or for another desktop under KDE. But what's the point? So that one may\ntake advantage of KDE3's delightfully quirky, paste-what-it-damn-well-pleases\nclipboard, which worked in KDE2, protestations to the contrary notwithstanding,\nbut which is tremendously screwed up now?</blockquote>\n\n<p>Bad KDE people conform to <a\nhref=\"http://www.freedesktop.org/standards/clipboards.txt\">this</a>.</p>\n\n<blockquote>Over the years KDE has had no greater advocate than I have\nbeen.</blockquote>\n\n<p>I have trolled the KDE mailing lists for a years, demanding instant\nattention whenever I couldn't get something to work, due to my holy 'reporter'\nstatus.</p>\n\n<blockquote>I long hoped that KDE would remain the benchmark Linux desktop. No\nmore.</blockquote>\n\n<p>I have a new best friend now.</p>"
    author: "Anonymous"
  - subject: "Deja-Vu"
    date: 2002-06-27
    body: "Is the Tibirna interview a new one? It only talks about old stuff and I think I read it somewhere years ago. And HierMenus seems old news too, the page was created in April (perhaps it was only mentioned in Kernel Cousin KDE then)?"
    author: "Anonymous"
  - subject: "Re: Deja-Vu"
    date: 2002-06-27
    body: "The interview is new, Cristian is old.  :-)  The HierMenus stuff is old, but we never posted it here and someone submitted it."
    author: "Navindra Umanee"
  - subject: "Garnome?"
    date: 2002-06-27
    body: "What's Garnome? The link just takes me to a bunch of tarballs..."
    author: "AC"
  - subject: "Re: Garnome?"
    date: 2002-06-28
    body: "See the homepage: http://www.gnome.org/~jdub/garnome/."
    author: "Evandro"
  - subject: "Re: Garnome?"
    date: 2002-06-28
    body: "\"GARNOME is a new distribution of GNOME, based on the GAR ports system by Nick Moffitt. Right now, it builds from the recent GNOME 2.0 Desktop tarball releases, and includes some extra software that has been ported to the GNOME 2.0 platform.\""
    author: "Anonymous"
  - subject: "Re: Garnome?"
    date: 2002-06-28
    body: "Okay, so what does a GNOME distribution have to do with KDE?"
    author: "AC"
  - subject: "Re: Garnome?"
    date: 2002-06-28
    body: "Garnome can build Gnome as well as KDE 3.0 & 3.01"
    author: "Guy"
  - subject: "GARNOME features KDE 3.0.1 ?"
    date: 2002-06-28
    body: "whats that mean ? \n\n"
    author: "KDE fan"
  - subject: "# make dep clean"
    date: 2002-06-28
    body: "I think it's pretty clear that dep has mistaken the changing attitute of\nKDE developers to _him_, as a change in their attitude to everyone.\n\nI feel a bit bad for the GNOME guys now as it seems he will be writing\na lot more \"constructive\" articles about them now, instead of KDE...\n\nanyway what I really wanted to say was, bye dep..."
    author: "taj"
---
A new <a href="http://www.geocities.com/moddingden/">"modding site"</a> has <a href="http://www.geocities.com/moddingden/kdeinterview.htm">interviewed</a> our own <a href="http://www.kde.org/people/cristian.html">Cristian Tibirna</a>, one of the first KDE core developers. <a href="mailto:pwiegers@linuxmail.org">Paul Wiegers</a> wrote in to point out that Konqueror has <a href="http://www.webreference.com/dhtml/column65/">gained more recognition</a> in the Internet community, this time in the form of support for HierMenus. The same site also features <a href="http://www.webreference.com/dhtml/column65/2.html">a flattering introduction</a> to Konqueror with screenshots.  Out of frustration, our own <a href="http://rikkus.info/">Rik Hemsley</a> has written up an article on <a href="http://rikkus.info/dirty_reporting.html">dirty reporting</a>.  And, last but not least, congrats to the Gnomes on their new release!  The <a href="http://www.gnome.org/~jdub/garnome/download/?N=D">latest GARNOME</a> even features KDE 3.0.1.
<!--break-->
