---
title: "KDEnews.UNIXcode.org Launched!"
date:    2002-06-27
authors:
  - "rkaper"
slug:    kdenewsunixcodeorg-launched
comments:
  - subject: "Unhappy name"
    date: 2002-06-27
    body: "For me there can be only one \"KDEnews\", http://kdenews.org"
    author: "Anonymous"
  - subject: "Re: Unhappy name"
    date: 2002-06-27
    body: "Yeah, I agree it's sort of unfortunate, although they make a point of capitalizing/spacing differently.  Apparently they are up to rebranding the site in the future...\n"
    author: "Navindra Umanee"
  - subject: "Re: Unhappy name"
    date: 2002-06-28
    body: "I hope they get rid of those useless frames too."
    author: "Kamil Kisiel"
  - subject: "Re: Unhappy name"
    date: 2002-06-28
    body: "It wasn't a frame actually, it was a div that was set to have fixed positioning.  And yesterday I switched it to absolute it because it got in the way of reading comments.\n\nAs a nice side effect of the change, khtml renders the site much faster when scrolling."
    author: "Neil Stevens"
  - subject: "Re: Unhappy name"
    date: 2002-06-28
    body: "One suggestion: KDE.board, as everyone (after registration) is allowed to pin stories."
    author: "Anonymous"
---
A new information and discussion hub for KDE <a href="http://kdenews.unixcode.org/?node=News&action=article;4">has been launched</a>, called <a
href="http://kdenews.unixcode.org/">KDEnews.UNIXcode.org</a>, or simply
KDEnews. A major difference from other KDE-related sites is the publically available submission queue. Registered users decide most of the content, not the administrators.  KDEnews is currently featuring a couple of announcements including <a href="http://kdenews.unixcode.org/?node=News&action=article;5">a call</a> for a new <a href="http://multimedia.kde.org/">KDE Multimedia</a> strategy meeting, and <a href="http://kdenews.unixcode.org/?node=News&action=article;1">information</a> on the upcoming LWCE 2002.
<!--break-->
<p>
The level of control administrators have at KDEnews is primarily to regulate the community by preventing and, if necessary, correcting abuse of the system. This distribution of control is there to allow the KDEnews to be more responsive than an administrator-edited site.</p>

<p>
While it is likely the site administrators will be active users,
the policy is to only suggest articles for publication through the regular
submission queue, with the obvious exception of the content necessary for the launch of the site. For more details on KDEnews, read the <a href="http://kdenews.unixcode.org/?node=News&action=article;4">full announcement</a>.</p>

<p>
Feel free to start submitting your news right now and keep an eye on the queue so you can vote for suggested articles and have them published!</p>