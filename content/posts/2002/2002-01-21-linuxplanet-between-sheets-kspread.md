---
title: "LinuxPlanet: Between the Sheets with KSpread"
date:    2002-01-21
authors:
  - "numanee"
slug:    linuxplanet-between-sheets-kspread
comments:
  - subject: "Uhh..."
    date: 2002-01-20
    body: "Navindra, please don't ever title a dot story \"Between the sheets\" again, even if it is a fairly good pun. :-)"
    author: "Carbon"
  - subject: "Re: Uhh...What?"
    date: 2002-01-20
    body: "did you not read the article?\nthe name of the series on LinuxPlanet is called\n\"Between the sheets\""
    author: "Brad C."
  - subject: "Re: Uhh...What?"
    date: 2002-01-20
    body: "Ah, k, it's not Navindra's fault then. In that case:\n\nLinuxPlanet, please don't ever name an article \"Between the Sheets\" ever again :-)"
    author: "Carbon"
  - subject: "Re: Uhh...What?"
    date: 2002-01-21
    body: "Whats so bad about \"Between the sheets\"? Sorry my english is not so good and perhaps this is the reason why I don't know what you're talking about. Please let me (and some others too as I guess) know what's so bad about this title...\n\nThanks\nPietz"
    author: "Pietz"
  - subject: "Re: Uhh...What?"
    date: 2002-01-21
    body: "Between the sheets is a phrase normally accompanied with the name of some lucky girl ;-) _Please_ don't make me have to spell this out!"
    author: "Bryan Feeney"
  - subject: "Re: Uhh...What?"
    date: 2002-01-21
    body: "Actually, \"Between the sheets\" wouldn't be nearly so bad if the program wasn't named \"KSpread\". My colleagues and I got a real kick out of that name when we first saw it. It just sounds sexual (\"OK, spread!\"). Personally I think the name should be changed... then again, maybe I have sex on the brain. :)"
    author: "Bud"
  - subject: "Re: Uhh...What?"
    date: 2002-01-21
    body: "Thanks. I got it. LOL\nDon't think about changing the name of KSpread then ;-)"
    author: "Pietz"
  - subject: "Missing features?"
    date: 2002-01-21
    body: "Actually, I've never really used a spreadsheet as more then a table. Can someone report through personal experience what exactly it is that a spreadsheet is used for seriously? I understand things like functions and result seeking (alter cell a until cell c, through some overly complex series of functions, becomes 1.23) but, it seems to me at least that that sort of thing is more appropriate for a short, scripted program then for some kind of meta-application. That's probably because I'm missing something, though."
    author: "Carbon"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "> Actually, I've never really used a spreadsheet as more then a table. Can \n> someone report through personal experience what exactly it is that a \n> spreadsheet is used for seriously? \n\nArgh! I hate people doing this, \"I need to do a table, so let's start Excel\". Obviously many people have never noticed that they can use their word processor for tables as well. Out there in the windows world there seems to be some kind of stupidity contest for misusing spreadsheets. I have seen people using Excel for project managing (without using any calculations, they only made a list of tasks), bug tracking (as if they never heard of databases), sending pictures (people seem to feel uncomfortable sending a picture as jpeg or png, so they create a new excel file and put the image in there) and, my favorite, GUI design (they made dialogs using Excel tables)...\nand most of these people have studied computer science and/or worked for a very large and well-known \"technology consulting\" company...\n\n>I understand things like functions and result seeking (alter cell a until\n>cell c, through some overly complex series of functions, becomes 1.23) but,\n>it seems to me at least that that sort of thing is more appropriate for a\n >short, scripted program then for some kind of meta-application. That's \n>probably because I'm missing something, though.\n\nFirst of all you don't need to know how to program to use a spreadsheet, its much simpler to use. And spreadsheets are not only used to get a result, you also use it to show how you got it. For example, you need to create a budget and have a list of positions. You can put them in a table and let the spreadsheet calculate the sum automatically. Then you can play around with the numbers, add a new position here, reduce the spendings for this. Whatever you do, the results will be automatically updated. And this is even more useful if you have more complicated calculations."
    author: "Tim Jansen"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: ">Argh! I hate people doing this, \"I need to do a table, so let's start Excel\".\n\nNo, by table I meant a static table. As in, making a table of a few numbers simply to do something like a single chart, no dynamic stuff.\n\nI can see your point about using spreadsheets to quickly mess with data though. And now that I htink about it, I just demonstrated another good thing about spreadsheets; chart capability. I haven't seen an easy-to-use chart library for any scripted language (though I'd be happy to be corrected on this point.)"
    author: "Carbon"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "In 2000 I wrote a cash flow forecast in Microsoft Excel, with a month-by-month 5 year cash flow and performance overview. Each month had carry over financials and performance data from the previous month, and each year had carry-over financials and performance data from the previous years. Each year was a separate spreadsheet file, and it was pretty neat how updates to an earlier year trickled down throughout a series of spreadsheets. \n\nAlthough I never sought outside investment in my business concept (I financed the project myself), developing the spreadsheet taught me much about valuable a powerful spreadsheet application can be to a project."
    author: "Sean Pecor"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: ">I have seen people using Excel for project managing (without using <br>\n>any calculations, they only made a list of tasks)<br>\n<p>\nFunny you should make this complaint.  I've recently been reading <a href=\"http://www.joelonsoftware.com/uibook/fog0000000249.html\">\"User Interface Design for Programmers\"</a> on the <a href=\"http://www.joelonsoftware.com/\">Joel on Software</a> site, and he talks a bit about the design process of Excel when he used to be a designer:\n<p>\n<quote>\nIn the days of Excel 1.0 through 4.0, most people at Microsoft thought that the most common user activity was doing financial what-if scenarios, where you do things like change the inflation rate and see how this affects your profitability. \n<p>\nWhen we were designing Excel 5.0, the first major release to use serious activity-based planning, we only had to watch about five customers using the product before we realized that an enormous number of people just use Excel to keep lists. They are not entering any formulas or doing any calculation at all! We hadn't even considered this before. Keeping lists turned out to be far more popular than any other activity with Excel. And this led us to invent a whole slew of features that make it easier to keep lists: easier sorting, automatic data entry, the AutoFilter feature which helps you see a slice of your list, and multi-user features which let several people work on the same list at the same time while Excel automatically reconciles everything.\n</quote>\n<p>\nSo it just goes to show that sometimes the wrong way can be the right way (no I don't think that makes sense, but I couldn't think of anything to finish the post with)   :)\n</p>"
    author: "Damien Byrne"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: ">>When we were designing Excel 5.0, the first major release to use serious activity-based planning, we only had to watch about five customers using the product before we realized that an enormous number of people just use Excel to keep lists.<<\n\nThe major problem is that Excel does not scale very well for this. Once you have a few thousand lines in Excel it will get REALLY slow. And, of course, it lacks features like searching. People would be better off they did the lists in Access, but it seems like most people dont even know what Access is good for..."
    author: "Tim Jansen"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "mmmm.. Smells like recursion to me:\n\nThe major problem is that Access does not scale very well for this. Once you have a few thousand lines/records in Access it will get REALLY slow and keep corrupting. And, of course, it lacks features like searching. People would be better off they did the lists in a proper DBMS, but it seems like most people dont even know what a proper DBMS is good for..."
    author: "Corba the Geek"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "AFAIK you can use any ODBC data source, not only the Jet database engine."
    author: "Tim Jansen"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "My tongue was in my cheek.\n\nThe real problem with access is that people with no programming or database design experience tend to create one. Then want to scale up their super little projects to proper company wide multi-user things.\n\nAt which point you have to re-write it from scratch using proper design and tools.....\n\nAt which point the original author gets cross because you are re-creating their pet project. And so on..."
    author: "Corba the Geek"
  - subject: "Re: Missing features?"
    date: 2002-01-23
    body: "I know all about the slowness of access.  My boss had a contact manager with 10,000 records in it.  that was used for not only contact manager but also project management in our office.  it worked for a while.  but the overhead of access is too large.  its a pain to keep access open all of the time, and it takes to long to start up.  thats not even to mention the use of it by 15 people at a time.  Which is why it is redesigned in two parts, well the contact stuff was put into ACT 2000, the project management in python using a mysql python, which gives us a nice crossplatform ability."
    author: "Paul"
  - subject: "Using complicated Excel sheets in KSpread"
    date: 2002-01-21
    body: "Have you tried to use that complicated spreadsheet in KSpread? I tried importing my budget Excel sheet into it, and wasn't very impressed. The big thing for me was my currency is Swiss Francs, but KSpread only supports US $, so it just converted them all. (It looked exactly the same in Gnumeric.)\n\nI'd be interested to know if your sheet works in KSpread (or Gnumeric or Open Office Calc, for that matter.)"
    author: "Frank"
  - subject: "Re: Using complicated Excel sheets in KSpread"
    date: 2002-01-21
    body: "Not true. You can change the currency used in KControl, and KSpread will respect these changes."
    author: "Chris Howells"
  - subject: "Re: Using complicated Excel sheets in KSpread"
    date: 2002-01-22
    body: "Perhaps, but it isn't respected on import of an Excel .xls file. And that's where I saw the problem. :)"
    author: "Frank"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "I do real estate development as a job, and so I frequently have to use a spreadsheet in order to manage and organize the information available. \n\nFor example, tracking employment levels over various categories and over time, and then charting the data into something that displays trends.  As another example, being able to enter various financial data for a property, and then have the spreadsheet compare similar properties side by side, all while calculating capitalization ratios, cash-on-cash ratios, and gross and net income.\n\nAs a downside, I am not yet satisfied with KSpread as a professional level spreadsheet, and so I still use Excel. I could probably force the data to fit within KSpread's limitations, by why should I? As an aside, I am an OS X user. \n\nThe biggest KSpread shortcomings, in my opinion:\n\n1. Lack of real built-in formulas. I do not want to build present-value and future-value formulas myself.\n2. Lack of pivot tables and pivot charts.\n\nAnd of course reliable Excel import/export. Of course nothing much can be done there."
    author: "Ian Zepp"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "> And of course reliable Excel import/export.\n> Of course nothing much can be done there.\n\nother projects have accomplished this quite succesfully. there is no reason aside from lack of developer interest, time and effort that this can not be done eventually."
    author: "Aaron J. Seigo"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "As a software engineer, I feel that the only good thing a spreadsheet gives you is the easy visualisation of the figures. Apart from that, the programming aspects are very limited and lead to maintenance issues rather quickly. The means of referring to data using cell references, and the subsequent need to have various absolute and relative column and row notations (eg. \"$G99\", \"H$14\"), suggests that for complicated activities or sheets which may change their dimensions frequently, spreadsheet application programs are going to be tiresome to use for such activities after a very short period of time (although a certain amount of discipline might help, but this extra effort could be better spent using a programming language instead).\n\nI would rather see scripting languages improve their visualisation tools, possibly through the development of visualisation environments for such languages. These environments would be as comfortable as spreadsheet applications, but provide access to \"real\" data structures. Surely, doing anything complicated in a spreadsheet is like writing KDE in an early version of BASIC..."
    author: "Paul Boddie"
  - subject: "Re: Missing features?"
    date: 2002-01-21
    body: "I agree, at least with your sentiment about current spreadsheets. However, in order to make a good visualization for a scripted language would probably require making a language specifically designed to be visualized :-). What might be better is if KSpread or some other app had the ability to reference a cells or groups of cells by dynamic handles. For instance, you could name a region of cells, or determine the location of cells using some sort of dereferencing (Perl style). This would allow a spreadsheet to become more dynamic, while still also being usable as a traditional spreadsheet tool. What do any KSpread or other spreadsheet developers think of this? Yeah, I know I'm being an armchair designer, but it's so much fun! :-)"
    author: "Carbon"
  - subject: "Re: Missing features?"
    date: 2002-01-22
    body: ">The means of referring to data using cell references, and \n>the subsequent need to have various absolute and relative \n>column and row notations (eg. \"$G99\", \"H$14\"), suggests \n>that for complicated activities or sheets which may change \n>their dimensions frequently, spreadsheet application programs \n>are going to be tiresome to use for such activities after a \n>very short period of time...\n\nExcel does an excellent job of automatically readjusting references as you change the sheet layout. (And unlike most of the \"smart\" features in Office, it rarely gets in your way.) I'm surprised so many people are skeptical about spreadsheets. I use them constantly for storing and analyzing experimental data, and couldn't live without Excel. I even do my taxes in it. ;-)\n\nIMHO, it's the best thing to come out of Microsoft, as long as you run it on a Mac."
    author: "Otter"
  - subject: "Re: Missing features?"
    date: 2002-01-22
    body: "As a student, spreadsheets played an important role in our dorm. You see, we had 8 people who eat together and each day one of those 8 persons bought dinner. Instead of going through the hassles of paying that person the day he bought dinner, we used a spreadsheet where everyone had a running total of the food he had bought minus the price of all the meals he had eaten. Of course sometimes someone doesn't eat at the dorm, and sometimes someone invites a friend over, in which case that person had to be charged 0 resp 2 meals. Without a spreadhsheet this would have been an impossible task. :-)\n\nOh, and we also used it to for the beer-accounting. Since usually not all the\nbeer was accounted for the missing beers were charged pro-ratio to the consumers. Again, a usefull task for a spreadsheet.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Missing features?"
    date: 2002-01-22
    body: "Only a german would \"manage\" his beer with a spread sheet ;)\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Missing features?"
    date: 2002-01-23
    body: "Hey, let's get this straight. I'm not German. I'm Dutch.\n<br><br>\n( See also <a href=\"http://www.broese.net/english/holland/lowsky.html\">http://www.broese.net/english/holland/lowsky.html</a>,\n<a href=\"http://www.broese.net/english/holland/roughguide.html\">http://www.broese.net/english/holland/roughguide.html</a> and\n<a href=\"http://www.oranje.nl/\">http://www.oranje.nl/</a> )\n<br><br>\nCheers,<br>\nWaldo"
    author: "Waldo Bastian"
  - subject: "slow response of kspread"
    date: 2002-01-21
    body: "Since upgrading to KOffice 1.1.1 and KDE 2.2.2, KSpread is indeed taking ages to load and save a kspread document that I had created in earlier KSpread (KOffice 1.0). So maybe the author is having the same problem. I too use Mandrake 8.1 and their perticular package could be a problem too!  Another thing I observed was that my document was much smaller with KSpread 1.0.. on opening and saving in KSpread 1.1, the document became much bigger! Right now I am assuming that there is some problem opening/saving KSpread 1.0 documents.\n\nFor KSpread to become successful, its very important IMHO that older version documents be opened flawlessly!!\n\nSarang"
    author: "sarang"
  - subject: "Re: slow response of kspread"
    date: 2002-01-21
    body: "I had the same problem with my application, caused by a slow XML parser in Qt and output text streaming ( converting to UTF-8 ) - the same problem is in KSpread I think. KSpread tars/gzips the output and this probably doesn't make things faster. File format design is a next important thing ( attributes should be stored for the whole areas, not for the each cell ). I moved from DOM parser to SAX,memory problems were gone, but the speed remained the same. One solution could be to allow users to choose binary/xml file format."
    author: "Rumcajs"
  - subject: "Re: slow response of kspread"
    date: 2002-01-21
    body: "Make sure you submit bugreports about this, providing such problematic documents by mailing NNNN@bugs.kde.org once you know NNNN (the bug report number).\n\nFor one, I don't have any old KSpread document anymore, to test this."
    author: "David Faure"
---
<a href="http://www.linuxplanet.com/">LinuxPlanet</a> is running <a href="http://www.linuxplanet.com/linuxplanet/reviews/4013/2/">a review</a> of <A href="http://www.koffice.org/kspread/">KSpread</a>.  The article is nicely written, warmly positive, but also points out some of the more serious missing features of KSpread, such as a lack of spreadsheet functions.     <i>"As far as the interface goes, everything was simple and clean. There is a function drop-down menu if you want it, or you can use a Formula Editor to build your functions. Using the Formula Editor gave you the distinct advantage of receiving an explanation of what each function was and also brought up ways to further modify the function at hand."</i>  I'm far from an office person myself (hence don't have any real KSpread test cases), but I could not reproduce the claim that opening modest-sized KSpread files was slow, on my old Celery running <A href="http://www.mandrakesoft.com/products/81/gaming-edition">Mandrake 8.1 GE</a>.
<!--break-->
