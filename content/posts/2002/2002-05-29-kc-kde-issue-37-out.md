---
title: "KC KDE Issue 37 is Out"
date:    2002-05-29
authors:
  - "numanee"
slug:    kc-kde-issue-37-out
comments:
  - subject: "Nice work + KMathSomething"
    date: 2002-05-29
    body: "Nice work, hopefully someone took Aaron's succession and will try to make it as good :)\nBtw, regarding the math app, it's great not to integrate such small apps in the KDE head, they can live separately. And I doubt anyone interested in maths would require such a 1st year exercise (regarding the CAS part, not the interface). What is badly needed is a fronted to a _real_ CAS, like maxima http://www.ma.utexas.edu/maxima.html or maybe yacas http://www.xs4all.nl/~apinkus/yacas.html.\nAnyway, keep up the good work..."
    author: "HB"
  - subject: "Re: Nice work + KMathSomething"
    date: 2002-05-30
    body: "Wow, I had no idea such advanced CAS programs were available under the GPL!  Why is everyone re-inventing the wheel?  Yacas in particular seems well-suited to KDE integration since it is in C++ already, and it is in a useable state.  The OS X front-end looks sweet.  Here's my vote for a nice KDE CAS front-end."
    author: "not me"
  - subject: "Re: Nice work + KMathSomething"
    date: 2002-05-30
    body: "Yes, I've told that myself a thousand times already.\nIt's a shame that there is no GUI frontend to CAS like yacas or maxima and Numerical Computation Systems like Ocatve. \nWhy reeinvent the wheel, so much effort was put into those systems already. Just take some kde developers put them together with the Yacas/Maxima/Octave people so they can make a nice GUI for those apps. I bet many more Universities would use them.\nFortunately, in the CAS department, there is MuPad which is free (kind of, it has a special free license to most people), but not GPL.\n\nJ.A.\n\n"
    author: "J.A."
  - subject: "Re: Nice work + KMathSomething"
    date: 2002-05-31
    body: "It's a bit sad (at least for me :( ) that nobody remembers Kalamaris anymore. You can read about it at <a href=\"http://devel-home.kde.org/~larrosa/kalamaris.html\">kalamaris.html</a> with screenshots at <a href=\"http://devel-home.kde.org/~larrosa/kalamaris_screenshots.html\">kalamaris_screenshots.html</a>\n<p>\nI even used its core engine to evaluate surface functions in KBillar (at <a href=\"http://devel-home.kde.org/~larrosa/kbillar.html\">kbillar.html</a>, screenshots at <a href=\"http://devel-home.kde.org/~larrosa/kbillar_screenshots.html\">billar_screenshots.html</a> ), so I think it's quite flexible.\n<p>\nOk, I recently abandoned all my kde work, but I wouldn't mind retaking kalamaris after my exams if someone helps a bit (so far, I've been the only author together with two persons who sent me small patches, in total a patch of less than 15 lines).\n<p>\nI hope I don't refrain anyone from joining the KMathCenter team, but as you said, I hope people interested in more advanced mathematics can join Kalamaris, which intends to be a Mathematica-like application.\n<p>\nGreetings,"
    author: "Antonio Larrosa"
  - subject: "Keramik Default Style?"
    date: 2002-05-30
    body: "Can anyone confirm the rumour that Keramik is slated to be the next default style/decoration for KDE?\n\nMan, that ought to raise some eyebrows!  Hopefully it would shut-up the kde-is-butt-ugly trolls forever. :)\n"
    author: "Navindra Umanee"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "No. It would be hated by too many people as a default style."
    author: "Anonymous"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "You think?  Apple seems to have nothing but fans with their Liquid (ha! ha! I mean, Aqua) interface.  \n\nKDE's HiColor Default has served well, but it's starting to look dated, I think it would be worth seeing what the reaction to Keramik as default in KDE 3.1 is.  If it's bad, then the default can always be changed back in KDE 3.2.   Bad plan? :)"
    author: "Navindra Umanee"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "I can think of one good reason why not to use it as a default -- it consumes more screen space than the current default, and even with a small window decoration, 75 dpi fonts, and a kicker with the smallest icons possible, some dialogs in KDE still don't fit well onto the screen at 800x600.  IMO, with default settings for everything, and using 75dpi fonts, every dialog should fit on screen, with some allowance for the user to use a larger window decoration.  And before you tell me to use a higher screen resolution, I can't -- my laptop's LCD screen maxes out at 800x600."
    author: "Chad Kitching"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "These are all usability bugs that can be fixed.\n"
    author: "ac"
  - subject: "Re: Keramik Default Style?"
    date: 2002-06-01
    body: "Well, mark me down as one of the people that doesn't like Aqua.  I find its design to be distracting and very cartoony.  I don't know if the Mac lets you change the interface.  At least with KDE I can pick what I like, so ship whatever you want as the default."
    author: "jmalory"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "how about no default theme, and let people choose in kpersonalizer on kde-first-run"
    author: "fault"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "That\u00b4s a very bad usability decision. It\u00b4s like saying to the user \u00a8We couldn\u00b4t make a decision of a default style, so choose one yourself.\u00a8\n"
    author: "Carg"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "and what does it default to if someone decides not to run kpersonalizer or just hits \"Next\"? or is the plan to not let them log in until they make up their mind? heh...\n\nthere is always a default. it is just a question of how much you notice it.\n\nand let's not even get into the fact that kpersonalizer is supposed to make general look 'n feel config easier, not more detailed, for the general user base."
    author: "Aaron J. Seigo"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "Mandrake does not run KPersonalizer either.  You could say they hijacked it with their own GTK version with their own inflexible KDE default."
    author: "ac"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "<i>[ed: This is joe99]</i>\n<p>\n> Hopefully it would shut-up the kde-is-butt-ugly trolls forever. :)\n<p>\nSo instead of ripping off Windows 9x/2000 ui/look for the last few years ...you want a theme that rips of XP and some MacOSX as default?!.. kde will *always* look ugly, no matter how good your theme or hack is. (and public opinion wont change)\n<p>\nMeta-themes is gonna own you all."
    author: "Vitamin C"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "i'll bite on the troll...\n\n> So instead of ripping off Windows 9x/2000 ui/look for the last few years ...you want a theme that rips of XP and some MacOSX as default?!.. kde will *always* look ugly, no matter how good your theme or hack is. (and public opinion wont change)\n\nJust like gnome2's look is a rip of kde 2.0 to kde 3.0, everywhere from icons to the default metacity titlebar. "
    author: "fault"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "You\u00b4re not biting, you\u00b4re trolling yourself. You joined his club.\n"
    author: "Carg"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-31
    body: "icons? cmon..you're joking right? thats one of the reasons why KDE has always been ugly, icons...Gnome icons are so much nicer. We have Tigert to thank for the pretty icons,..if you look at those icons from the screenshots, its a direct ripp from XP. Im sure if i pointed this out to M$, they would be rather interested in sue'ing this guys ass..."
    author: "Vitamin C"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-31
    body: "You're talking about GNOME1 icons?  Hahaha.\n\nEven tigert and miguel don't like them.  They have both expressed disgust at these messy icons.  Look at their new icons, more like KDE than GNOME!!"
    author: "ac"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "I don't get this Keramik looks like xp thing.  To me the only similarity is the color.  The shape looks quite different.  While xp has pretty much a gradient (modified a bit for a 3d look) and text, Keramik has a sort of dual overlapping bar thing going on.  \n\n...while I am talking about color, what is with all the blue.  Everything seems to be going blue.  Blue looks so cold to me.  Personally something like the scalable gorilla icons and this Keramik style colored to match would look much more warm and inviting."
    author: "theorz"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-30
    body: "It doesn't, this Vitamin C is a known troll.  meta-themes gives him away.  Keramik is a terrific concept."
    author: "ac"
  - subject: "Re: Keramik Default Style?"
    date: 2002-05-31
    body: "I find it pretty nice.\nApart from that, sometime ago I heard about GIMP to ve GUI independent. When will this happen? I mean, will GIMP for KDE will ever be possible?\nRegarding to keramik, actually to Crystal icon set, IMHO the \"heart\" for bookmarks doesn'look very professional :-)"
    author: "lanbo"
  - subject: "I just downloaded Keramik"
    date: 2002-05-30
    body: "Wow.  Now if only I could get Mozilla to follow along."
    author: "ac"
  - subject: "new KC editors"
    date: 2002-05-30
    body: "I just wanted to say THANK YOU to the new KC editors!\nI am extremely happy that some people continue this mailing list summary.\nIt is a great chance to get a close look into the KDE development.\n\nThanks for your work!!!!\n\nSebastian"
    author: "Sebastian Eichner"
  - subject: "Thanks in advance"
    date: 2002-05-30
    body: "Wow! KC KDE is back!\nI waited a long time for this! i was an regular reader of kernel cousins. but suddenly kde wasn't covered anymore. it was hard for me to find another resource which provides such good coverage of the things going on in kde.\nit was always a good mixture between developer news and application information.\n\nhowever the new editors will do their work (i didn't look at it by now, but will a few clicks later), i'm thanking them for their effort!\n\nkeep on working!\n\nSimon"
    author: "Simon"
  - subject: "Thanks a lot!"
    date: 2002-05-30
    body: "I really missed these - thanks a lot for continuing making it possible for us to easily follow KDE progress!\n\nKeramik looks pretty cool!"
    author: "Joergen Ramskov"
  - subject: "Keramik is so great !"
    date: 2002-06-01
    body: "What's the name of  those icons in Keramik shot2&3 ?\nThey're like so cool!\nWhere do i get them ?"
    author: "root"
  - subject: "Re: Keramik is so great !"
    date: 2002-06-01
    body: "Crystal - http://www.kde-look.org/content/show.php?content=1586"
    author: "Anonymous"
  - subject: "I hate Aqua!"
    date: 2002-06-01
    body: "Am I the only one sick of all these Aqua rip-offs? While KDE 2.x had a bunch of nice, original themes (TeaX, DotNET, etc) KDE 3.x seems to have just have ripoffs of QNX, WinXP, and OS X. What's really missing in KDE on the looks front is a gorgeous, ORIGINAL theme. I use KDE. I like KDE. I don't want KDE to look like something else, I want it to look like KDE!\n"
    author: "Rayiner Hashem"
  - subject: "Re: I hate Aqua!"
    date: 2002-06-02
    body: "I think you're rather confused.  \n\nDotNET appears to be based primarily on the appearance of Windows 2000 and the Whistler beta versions of Windows XP, while TeaX is very much based on the QNiX style code, and is very similar in apperance.  \n\nWhat exactly is the problem with KDE3's appearance then?\n\nDesigning a new \"original\" theme is not as easy as it sounds you know."
    author: "Sam"
  - subject: "Re: I hate Aqua!"
    date: 2002-06-02
    body: "Perhaps there are a lot of Aqua \"rip offs\".  However, Keramik doesn't exactly\nhave that Aqua look to it.  Keramik doesn't look like its made of glass.  It looks like something else.  Making an exact rip-off of MS Windows or MacOS isn't desireable because MS and Apple won't be too happy.  However, combining their good UI aspects along with some originality is fine.  KDE shouldn't be different just to be different, it will only confuse its users and especially Windows/Mac converts.  Because MS uses COM doesn't mean that KDE can't use an improved version of it.  Because Apple has a standardized place for its menubar doesn't mean that KDE shouldn't.\n\nBesides if you want a completely new and original theme for KDE, then create it.\nI'm sure you created this new theme that would make everything 1,000,000x better, it would be included in KDE.\n"
    author: "Anonymous Hero"
  - subject: "Re: I hate Aqua!"
    date: 2002-06-03
    body: "> Keramik doesn't look like its made of glass.\n> It looks like something else.\n\nCeramic, maybe?"
    author: "Rob Kaper"
  - subject: "no more panel background"
    date: 2002-07-25
    body: "Since I've installed Keramik, whenever KDE starts up, I have my selected panel background, but once the splash screen hits 100%, it goes back to the old grey background.  This only happens under Keramik also.  Anyone know how to fix this?"
    author: "gmar"
---
<a href="http://kt.zork.net/kde/">Kernel Cousin KDE</a> returns from its <a href="http://dot.kde.org/1019877950/">brief hiatus</a> with two new rewly editors in charge, Juergen Appel and Zack Rusin, and <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kckde/kckdestaff.html?rev=1.3&content-type=text/html">a new team</a> of contributors working directly on the <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kckde/">kckde module</a> in CVS.  <a href="http://kt.zork.net/kde/kde20020522_37.html">Issue 37 covers everything</a> from the new and exciting <a href="http://www.surakware.net/projects/kmathcenter/">KMathCenter</a> (<a href="http://www.surakware.net/projects/kmathcenter/shots.xml">screenshots</a>) to <a href="http://www.kdedevelopers.net/kopete/">Kopete</a> discussions, Kicker improvements, and a brand new <a href="http://www.kde-look.org/content/show.php?content=1961">Keramik window decoration</a> (<a href="http://www.kde-look.org/content/preview.php?file=1961-1.png">shot1</a>, <a href="http://www.kde-look.org/content/preview.php?file=1961-2.png">shot2</a>, <a href="http://www.kde-look.org/content/preview.php?file=1961-3.png">shot3</a>). Wooo!
<!--break-->
