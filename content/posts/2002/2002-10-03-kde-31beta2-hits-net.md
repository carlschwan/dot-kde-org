---
title: "KDE 3.1beta2 Hits the 'Net"
date:    2002-10-03
authors:
  - "Dre"
slug:    kde-31beta2-hits-net
comments:
  - subject: "KFile Plugins"
    date: 2002-10-03
    body: "I've written some plugins for Konqueror to display details of Java, Python, Lyx, abd Bibtex files in the filemanager popups and property tabs. I'd like some wider testing and feedback. The source is downloadable from http://users.uk.freebsd.org/~grrussel .\n\nThanks"
    author: "George Russell"
  - subject: "Re: KFile Plugins"
    date: 2002-10-03
    body: "Ah, nice ideas. I'll try it when I go home."
    author: "fault"
  - subject: "Re: KFile Plugins"
    date: 2002-10-04
    body: "Sounds interesting. Thanks a lot!\nAny changes that this code goes into the KDE repository? kdeaddons perhaps?"
    author: "Marco Krohn"
  - subject: ":%s/kate/kvim/g"
    date: 2002-10-03
    body: "<i>a KVim plugin for KDevelop</i>\n\nHey, this might win me over to kdevelop...\n"
    author: "Jos"
  - subject: "Re: :%s/kate/kvim/g"
    date: 2002-10-03
    body: "i'm afraid you need kdevelop 3.0, which has not seen even an alpha release yet. it seems this code has a long way to go :O/\n(disclaimer: never tried the code, i just give a mere *impression*)\nhowever i use daily this component to have konqueror open text files embedded in the vim component :O)\nand it even works with kde 3.0, don't need 3.1b2 :O)\n(it's the vimpart project of kdeextragear-1).\n\nemmanuel"
    author: "emmanuel"
  - subject: "kdevelop with makefiles?"
    date: 2002-10-04
    body: "Completely unrelated, I know.  But while we're discussing kdevelop, does anybody know if you can now (or will ever be able to) use kdevelop on a makefile-based project?\n\nI know kdbg can be used independently of the IDE (or at least could the last time I tried), and that's probably enough for me.  But kdevelop would be nice too."
    author: "Rob"
  - subject: "Re: kdevelop with makefiles?"
    date: 2002-10-04
    body: "as far as i know, kdevelop3 aka gideon supports automake/conf, qmake, and other build systems, for instance ant for java. it's really extensible..."
    author: "emmanuel"
  - subject: "Re: kdevelop with makefiles?"
    date: 2002-10-05
    body: "Unless I misunderstand what you want, Kdevelop-2.x has supported that for quite some time now. (Project -> Generate project file...). Try it. :)"
    author: "teatime"
  - subject: "Yay!"
    date: 2002-10-03
    body: "Ooooh! Shiny! *poing*\n\nOh, wait, wrong forum. Great work, guys!"
    author: "Kiki"
  - subject: "Bug statistic"
    date: 2002-10-03
    body: "Even over 2100 bugreports closed (not fixed) since the switch to Bugzilla."
    author: "Anonymous"
  - subject: "Re: Bug statistic"
    date: 2002-10-04
    body: "You know, it looks like bugzilla is really helping bugs getting the attention they deserve.\nEveryone can simply go to bugzilla and say: it's already fixed, close this bug please, mr. admin :)"
    author: "protoman"
  - subject: "Cool"
    date: 2002-10-03
    body: "Looks awesome. Really cool stuff you've got here!"
    author: "KDE User"
  - subject: "nice feature"
    date: 2002-10-03
    body: "what i expect too is \"a detailed list view with meta-information about files in the side-bar\".\ni saw screenshots a long time ago, it looked great and seemed useful too. i hope it's really what i think it is :O)"
    author: "emmanuel"
  - subject: "nice feature"
    date: 2002-10-03
    body: "what i expect too is \"a detailed list view with meta-information about files in the side-bar\".\ni saw screenshots a long time ago, it looked great and seemed useful too. i hope it's really what i think it is :O)"
    author: "emmanuel"
  - subject: "Re: nice feature"
    date: 2002-10-03
    body: "wait and this is good too!\n\"integration of file meta-information and multimedia files searches in file search utility\"\n\ni think i relates to http://devel-home.kde.org/~pfeiffer/kmrml/, http://devel-home.kde.org/~pfeiffer/kmrml/screenshots/\n\nif this is not innovation, i don't know what is... i hope it works well :O)"
    author: "emmanuel"
  - subject: "Re: nice feature"
    date: 2002-10-03
    body: "The meta \"info list view\" is implemented and working. But there is no corellation to the sidebar. Meta infos only show up in tool tips and the properties dialog too."
    author: "Anonymous"
  - subject: "Vim KPart in kdeextragear-1"
    date: 2002-10-03
    body: "Hi guys,\n\nthe vim kpart is scheduled to be distributed in KDE 3.2 (kdeaddons pkg), \nbut for now it's available in the kdeextragear-1 module of KDE's CVS and it's not in 3.1.\nBut the current version of the kpart works fine, so you can give it a try anyway :) (I use it every day), it's known to work with kdevelop, kwrite, konqui and some other apps. In 3.2, hopefully we will have it in kmail too :)\n\nMik"
    author: "Mickael Marchand"
  - subject: "Re: Vim KPart in kdeextragear-1"
    date: 2003-12-04
    body: "I'd be grateful if someone can atleast make a \ntar ball for >= 3.1.1. available, as it'll be sometime\nbefore 3.2 is part of any distro or is officially released."
    author: "Sapta Girisa"
  - subject: "Re: Vim KPart in kdeextragear-1"
    date: 2003-12-30
    body: "Hi Girish,\nThis Nag from Bapatla, hope you can recognize me ...I am trying to find you from different sources but finally today I got your Name in KDE.NEW. If you are not Sapta Girisa and not studied in Bapatla College of Arts and Sceince then please ignore this email.\n\nSorry for the trouble...I am looking for my best friend.\n\nThanks,\nNagaraju Dogiparthi \nndogiparthi@rediffmail.com"
    author: "Nag"
  - subject: "Re: Vim KPart in kdeextragear-1"
    date: 2003-12-30
    body: "Who wants to have a friend who doesn't know what email is?"
    author: "Anonymous"
  - subject: "for experts?"
    date: 2002-10-03
    body: "hi , just wanted to say \" please dont make kde expert-ware \", you are implementing tons of features , i dont think thats the way to go.\n\n"
    author: "chris"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "Oh, shut up!"
    author: "ddd"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "KDE IS FOR EVERYONE ! \n\nmany kde user/developer use vim, so having a vim kparts is just the way to go.\n\ni think we can expect KDE to keep is user friendliness.\n\n"
    author: "somekool"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "Now we just need KEmacs so that we can have a real editor ;)\n\nLet the editor wars begin!"
    author: "anon"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "The problem with embedding emacs is it's like embedding GNOME or something.  You get a mail client, tetris game, web browser, psychologist, file manager, etc, all at the same time.  Emacs isn't exactly suited for embedding."
    author: "not me"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "That's why there is a difference between the kvim application and the kvim part. Only one is used for embedding. While many people would prefer using the kvim part in applications, I think people would generally prefer a kemacs application. \n\nMuch more suited to a emacs kpart would be something like joe or jed (can't remember which), which is a lightweight emacs."
    author: "fault"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "Come on.  The reality is you can strip all of that out by removing the respective .elc/.el files and only provide limited functionality.  Did you know that you can do VI bindings via the VIPER module in Emacs?  OK, so why don't we use it since it's heavily developed and upgradeable?  Religion is probably the number one reason why.  Another is language -- everyone thinks lisp is old.  \n\nAnyhow, the future is more expandable with a KDE emacs equivalent.  I guess you could write a Kate plugin to do this stuff, but why?  Why do we keep reinventing the wheel?  Maybe I'll write another editor with the Numbers 1 2 3 4 as directional keys just so I can integrate Ruby as an embeddable scripting language.\n\nIt's like artsd vs esd vs oss.  I mean, come on!  I gotta 1.6GHZ machine running nothing but noatun and konsole.  And my music STILL skips!!  And because I did some funky update to the www.math.unl.edu version of kde for redhat 7.3, the ogg/vorbis stuff got upgraded to where xmms can't use it.  So, guess what, I'm using mpg321 ... after killing artsd of course.  I hope 250 of the bugs they fixed were centered in artsd.\n\nlater.\n"
    author: "Bill"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "I've got an 800MHz machine running three simultaenous compiles and I haven't noticed my music skip yet.  I have artsd set to run with realtime priority.  I am also using kernel 2.4.19 with Robert Love's preemptable kernel patch."
    author: "Jiffy"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "This is such phooey I cannot believe it.  Everytime I approach this topic with anyone, they always mention they have Kernel vWhiz.Bang.Baz or some other godsent sound system and it magically fixes artsd.  That's so wrong.  If esd, oss, et al can handle it with non-preemptible kernels, then artsd has a design problem.  Period.\n\nYeah, artsd is network-transparent, blah blah.  No excuse.  It's as if they started with the waveform of the sound and tried to objectively describe it using C++. He he, good luck.  Maybe when we get beowulfs-in-a-box on the market, it'll be okay and this won't be a discussion any more.  How about integrating artsd into Java?\n\nOut."
    author: "Bill Gates"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "Ok. I'm using aRts on a 700MHz box (overclocked to 770MHz), kernel 2.4.18 without any special patches. I can compile three KDE CVS-modules and still listen to my music, without any hickups whatsoever. Does that satisfy you?\n\nThe reason might be that my box has enough RAM (384MB won't be filled during the compile jobs, so no swapping required) and uses a separate harddisk for the KDE-sources I want to compile."
    author: "Jonathan Brugge"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "Some suggestions for your mp3 skipping problems:\n\n- run a pre-emptable kernel\n- tune your HD with hdparm.  Turning on UDMA and tweaking a bunch of other options completely eliminated my mp3 skipping problems.  I have a 400MHz P2 running KDE 3.0.3 under Debian unstable (512MB RAM, tho)\n\n"
    author: "balerion"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "Well, frankly I don't know how much of a difference the kernel actually made.  OSS is built into the kernel.  I believe esd uses realtime priority.  For all I know, things work well because I use realtime priority.  Another possibility is the size of your audio buffer (mine is 232 milliseconds).  The bigger your audio buffer, the longer artsd can wait before processing more sound.  You can change both settings using the soundserver panel in kcontrol.\n\nThe preemptable kernel should only make a difference when your machine is busy doing other things.  Let's say you're updating the slocate database.  Slocate constantly accesses the disk.  When a process is accessing the kernel (i.e. reading a directory from disk), the kernel will not switch to another task, so artsd has to wait longer before it can process more sound.  The preempt patch allows the kernel to switch processes even when another process is accessing the kernel.  The result is an all-around more responsive system.  Under heavy load, the preempt patch makes a \"Whiz.Bang.Baz\" difference.\n\nI don't know how realtime priority works, but like the preempt patch, it tries to make artsd run more frequently."
    author: "Jiffy"
  - subject: "Re: for experts and too slow"
    date: 2002-10-06
    body: "You guys simply don't understand the problem..\nit is kde having a performance problem. If it continues to be so slow Gnome will make it since multimedia stuff simply won't run with KDE as people expect it. It is so much slower than Windows that it is hard to imagine that anyone without having religous reasons will prefer KDE over Windows.\nAnd forget about your stupid patches. If it doesn't work with the standard kernel users won't care. Or do you really think the avarage user is going to patch its kernel for listening to mp3????\n\nI wish you good luck and some professionality! Finally!\njust a linux user"
    author: "just_linux_user"
  - subject: "Re: for experts and too slow"
    date: 2002-10-06
    body: "Everything starts as a patch and ends in released products. This was true for the C++ speed deficiencies of gcc and the linker and will be true for the  scheduler and other things in the kernel."
    author: "Anonymous"
  - subject: "Re: for experts and too slow"
    date: 2002-10-13
    body: "heh. i always get those stupid responses too. it's impossible.\narts is slow! i have tried preempt, low latency and everything but still i get glitches in sound. esp when switching apps."
    author: "montz"
  - subject: "Re: for experts and too slow"
    date: 2002-11-30
    body: "I realize I'm more than a month late on this, but aRts doesn't work around OSS bugs.  OSS bugs plague chipsets like ess1371.  Everyone out there just works around the bugs.  But not aRts.  Oh, no.  The author is above that.  aRts is apparently so important that leaving the bug workarounds out will get driver writers in gear and busy fixing their drivers.\n\nYeah, right."
    author: "Shane Simmons"
  - subject: "Re: for experts and too slow"
    date: 2002-11-30
    body: "It *does* workaround OSS bugs. That's what the \"Threaded OSS\" driver is all about."
    author: "Sad Eagle"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "I had this problem too, and I can honestly say its not the kde software that caused problems, and my machine is a p3 700 256 mb ram.  In my mandrake days playing music was hopeless on kde and less hopeless on xmms but it still skipped on there too.  I then managed to install gentoo on my machine and tried playing music and have never had it skip to this day.  Often its the packaging thats the problem more than the software."
    author: "bonito"
  - subject: "Am I missing something?"
    date: 2002-10-04
    body: "I'm running an older Knoppix distri from CD (first in August I think, which contains KDE 3.0.2) on my old AMD K6-2 300mHz with 228MB RAM of which afaik 60MB are used as ramdisk etc. And guess what, no skippings at all. Before this I used to have KDE 1.x on my computer until I noticed that I was still using Windows most of the time since KDE was so slow in most regards (and yes, music skipped back then). \n\nKnoppix rocks, and I'll toss it on my harddisk as soon as there's a release which includes KDE 3.1. =)"
    author: "Datschge"
  - subject: "Re: Am I missing something?"
    date: 2002-10-06
    body: "How do you put knoppix on your harddisk? It is meant to be a distro that runs from cd, isn't it? \n\nregards,\n\n    A.H."
    author: "Androgynous Howard"
  - subject: "Re: Am I missing something?"
    date: 2002-10-07
    body: "this is a german tutorial (with many screenshots) to install knoppix at a harddisk.\nmaybe this can help u anyway!\n\nhttp://www.pl-berichte.de/berichte/knoppix_hdinstall/knoppix-hdinstall.html\n\ngreetings\ngunnar"
    author: "gunnar"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "Actually, when compared to Windows or MacOS, KDE+Linux lacks may features, ranging from telephony APIs to network authentication. Many of them are 'under the hood' or well-hidden somewhere in the application menu. If KDE+Linux really wants to be an alternative, it must have them all (or better: have even more). No company will decide for an operating system that fulfills only 90% of their needs.\n\n\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "What sort of network authentication API is mission. Linux has eks. LDAP, RADIUS, PAM, NIS and tons of different other network autentication API's ! When you talk about API's they are udally well hidden 'under the hood'. API's is ment to be used by programmers not by the enduser. The API's in Windows is not a part of a application menu. \n"
    author: "Peter Antonius"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "But how are these systems integrated? Can you log in on a workstation, and then access all fileservers, printers, databases, webservers and custom applications without logging in again? Under Windows with Active Directory this is possible. Under KDE+Linux there are implementation for protocols that you could use to build such a solution, but nothing that works right out-of-the-box, is integrated in a desktop environment or easy to set up."
    author: "Tim Jansen"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "Whereas Active Directory works right out of the box. ALL complex environments will require qualified people to initially configure the systems and generally require day to day maintenance to keeps things sane.  "
    author: "Bill Gates"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "Compared to KDE/Linux it is pretty easy to do. On Linux systems you are on your own, you can be happy if you find a few scripts to configure the directory. The KDE libs do not support network authentication (yet :).\n"
    author: "Tim Jansen"
  - subject: "Re: for experts?"
    date: 2002-10-08
    body: "KDE does not need network authentication support. I'm logged into a workstation with no local user accounts at all right now. All the user accounts are stored in a LDAP server, and I use libpam-ldap and libnss-ldap to authenticate against it. KDM doesn't know anything about this, it just calls the lower-level getpwent() & friends and works.\n\nI'm able to access file shares via NFS with autofs, and printers via CUPS. I really don't see the problem here."
    author: "Ian Eure"
  - subject: "Re: for experts?"
    date: 2005-10-19
    body: "Bill is right.  Buy MS products."
    author: "Paul Allen"
  - subject: "Re: for experts?"
    date: 2002-10-03
    body: "Whereas Active Directory works right out of the box. ALL complex environments will require qualified people to initially configure the systems and generally require day to day maintenance to keeps things sane.  "
    author: "Bill Gates"
  - subject: "Re: for experts?"
    date: 2002-10-04
    body: "> Under Windows with Active Directory this is possible.\n\nJust thought you'd like to know, the SAMBA team is working on Active Directory client support [1].\n\n---\n\n[1] http://techupdate.zdnet.co.uk/story/0,,t481-s2122363,00.html"
    author: "Jiffy"
  - subject: "I get errors trying to install the Mandrake rpms"
    date: 2002-10-03
    body: "I get the following error trying to install:\n\nerror: failed dependencies:\n        libpng12.so.0   is needed by libarts3-1.1.0-0.beta2.1mdk\n\nurpmi libpng:\neverything already installed\n\nAnybody knows how to fix this(without --force)."
    author: "Andreas Joseph Krogh"
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-03
    body: "urpmi libpng-devel"
    author: "Paul Seamons"
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-03
    body: "I had the same problem, and I found the solution : we must update our libpng, see that page :\nhttp://www.linux-mandrake.com/en/updates/2002/MDKSA-2002-049.php?dis=8.2\n\nI used that mirror :\nhttp://ftp.belnet.be/packages/mandrake/updates/8.2/RPMS/\nand I download libpng3 and libpng3-devel.\n\n(but a problem still remains with timidity++ for me, and I had to uninstall timidity++ before installing)"
    author: "Capit Igloo"
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-04
    body: "I reply to myself : those rpm package seems to be very bad : almost all doesnt't work :((\nI think compiling himself is a better solution."
    author: "Capit Igloo"
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-04
    body: "I installed libpng3 and libpng3-devel from ftp://ftp.sunet.se/pub/os/Linux/distributions/mandrake/updates/8.2/RPMS/\nThey seem to work ok\n\n--\nAndreas"
    author: "Andreas Joseph Krogh"
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-04
    body: "any reason why all the control modules are gone with the mandrake stuff???"
    author: "a.c."
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-05
    body: "the same here :(\ni don't understand it! why these rpm packages are even worse than for beta1? there were also some missing modules for kcontrol and this is almost empty of them! \nit seems we have to compile beta2 from source ourselves :(\n\ni was too much expecting that it will be repaired with new beta but i'm very dissapointed about these MDK packages now.\nluci"
    author: "luci"
  - subject: "Re: I get errors trying to install the Mandrake rpms"
    date: 2002-10-05
    body: "Thanx for confirming. I was starting to wonder."
    author: "a.c."
  - subject: "Debian packages"
    date: 2002-10-03
    body: "are there any sarge packages available? "
    author: "L1"
  - subject: "Re: Debian packages"
    date: 2002-10-03
    body: "Go here to find out...\n\nhttp://mypage.bluewin.ch/kde3-debian/"
    author: "Dale"
  - subject: "(X)Emacs support"
    date: 2002-10-03
    body: "Is an (X)Emacs support planned for KDevelop? Just curious...\n\nHeiner"
    author: "Heiner"
  - subject: " KDENetwork Packages for SuSE"
    date: 2002-10-03
    body: "Hi,\n\ni got suspicios, because kmail wasn't starting for me. So i checked ftp dir, and found no kdenetwork for x86 8.0 and 8.1 SuSE packages. Is that correct?\n\n\tMarkus"
    author: "MarkusK"
  - subject: "Re:  KDENetwork Packages for SuSE"
    date: 2002-10-04
    body: "I'm experiencing the same problems with SuSE 7.3.  The network package is available as source, so I assume it's been missed off by mistake.\n\nI've just tried building from source as well, although it can't seem to find the correct version of QT. :-("
    author: "David Grant"
  - subject: "Re:  KDENetwork Packages for SuSE"
    date: 2002-10-04
    body: "same prob here.\n\nSolution: used KDENetwork RPMs from Beta1. \nworks fine for me.\n\nhave fun\nH."
    author: "Hermann"
  - subject: "Re:  KDENetwork Packages for SuSE"
    date: 2002-10-04
    body: "Attached is a kdenetwork spec file for SuSE.  You need to download the KDE 3.1Beta1 source RPM so that you can get the patches necessary to compile this, install the source RPM, then download the Beta2 tar file, cp the Beta2 tar file to /usr/src/packages/SOURCES, then place this spec file in /usr/src/packages/SPECS.  \n\nAfter you have done the above cd to /usr/src/packages/SPECS and run rpm -bb kdenetwork3.spec\n\nIt will take a while but it should compile, and will place some RPMS in /usr/src/packages/RPMS/i386.  Install these and all should be right in the world again. I have no place to upload my network files or I would simply share my rpms for SuSE 8.1.  Hopefully this will help out some people."
    author: "Anthony"
  - subject: "Re:  KDENetwork Packages for SuSE"
    date: 2002-10-06
    body: "There are since today kdenetwork packages available, now only kdesdk is missing."
    author: "Anonymous"
  - subject: "SuSE RPMs"
    date: 2002-10-03
    body: "Hi,\n\nI got a problem with the SuSE8.0 RPMs. When I start the filemanager it crashes with following message:\nMutex destroy failure: Device or resource busy\n\nWhen I start the filemanager as root I get the same errormessage, but konq doesn't crash.\n\nAny idea?\n\nThanx,\nAlex\n\n"
    author: "Alex"
  - subject: "Snapshot theme as \"default\"? Please NO."
    date: 2002-10-03
    body: "My \"problem\" is not that it looks so Win... like but it's \"pretty\" colored, overloaded and ugly.\n\nWhere is the \"old\" clarity of KDE?\nA desktop should be useful and \"clear\" in the first place.\n\nNo offence about all the great new stuff in the back!\n\n-Dieter\n\n-- \nDieter N\u00fctzel\nGraduate Student, Computer Science\n\nUniversity of Hamburg\nDepartment of Computer Science\n@home: Dieter.Nuetzel at hamburg.de (replace at with @)"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Snapshot theme as \"default\"? Please NO."
    date: 2002-10-03
    body: "What exactly prevents you from using different colors, less icons, etc.? Just because the default is somewhat colored, overloaded and (?)ugly doesn't mean you can't change it to whatever you consider more \"clear\".\n"
    author: "L.Lunak"
  - subject: "Re: Snapshot theme as \"default\"? Please NO."
    date: 2002-10-03
    body: "All styles and icon themes of \"old\" KDE are still contained."
    author: "Anonymous"
  - subject: "Re: Snapshot theme as \"default\"? Please NO."
    date: 2002-10-03
    body: "To both of you.\n\nAll you posted is vaild and I know that.\nBut I talked about the \"default\" behavior.\n\nIt should be as \"clear\" as possible.\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Snapshot theme as \"default\"? Please NO."
    date: 2002-10-04
    body: ">It should be as \"clear\" as possible.\n\nYes, this is your opionion, but not mine and not the one of a lot of others (which would even be the majority).\n\nYou can discuss this as well on usability.kde.org if you still like to."
    author: "Philppp"
  - subject: "Re: Snapshot theme as \"default\"? Please NO."
    date: 2002-10-03
    body: "I find the theme very nice. It's very good to see KDE not trying to imitate Windows or MacOS, but have something new.\n\nAnd i personally like Keramik very much. It's very nice and though does not affect the `feel` in a bad way like WinXP's Luna or OSX's Aqua, and yes, i've used all three of them.\n\nAs L. Lunak already said - what prevents you to set up your own style? Your own colours, theme, look & feel, etc.. Isn't that what KDE tries to provide?"
    author: "Hoek"
  - subject: "Re: Snapshot theme as \"default\"? Please NO."
    date: 2002-10-04
    body: "How about a slashdot theme?"
    author: "Bill"
  - subject: "Mandrake 9.0"
    date: 2002-10-03
    body: "Anybody with 9.0 rpms out there yet?"
    author: "Paul Seamons"
  - subject: "Great, but still too slow"
    date: 2002-10-03
    body: "KDE 3.1beta 2 is great, it looks like KDE has finally 'grown up'. But it's still not instant (and probably never will be, anyway). \n\nRegarding speed: On AMD400, Gnome2 is usable and KDE3 is unusable (way too slow). On Athlon 2000+, Gnome2 is instant and KDE3 is usable. \n\nSo my prayer is that the KDE developers concentrate on optimizeing for performance once KDE3.1 is stable. \n\nBy makeing KDE usable on old, slow computers, KDEs potential user-base would increse substantial. \n\nAnother idea is to have a fist-time-you-run-kde setup-tool where you select/detect computer speed and apply settings for (a) slow computer (b) fast computer and (c) hot-of-the-line computer.\n\nFeatures and flashy effects are great, but to me, INSTANT is equally important (no waiting period whatsoever, ever).\n\nLook at the gui on mobile phones. Would users accept a 10 minute waiting period when they open their phonebook?"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "> So my prayer is that the KDE developers concentrate on optimizeing for performance once KDE3.1 is stable\n\nYou haven't compiled it yourself with gcc 3.2, or?\n\n> Another idea is to have a fist-time-you-run-kde setup-tool where you select/detect computer speed\n\nSuch thing (kpersonalizer) exists since KDE 2.2, don't blame KDE if your distribution cripples KDE."
    author: "Anonymous"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "Is KDE noticeable faster if you compile it with gcc 3.2 instead of gcc 2.x\nand how long does it take to compile it? (i use a PII 450Mhz)"
    author: "Kosmo"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "Yes, of course it's faster. Search google on how you can optimize your CFLAGS/CXXFLAGS, download latest binutils, latest KDE, latest GCC, and compile.\n\nCompiling kdelibs, kdebase, and kdenetwork on an AMD 1200 Mhz/ 256 MB RAM takes ~ 3 hrs. But it's worth it, trust me.\n\nOr, better, look at Gentoo (http://gentoo.org) if you don't have it already. It's a source distribution optimizing everything for your CPU and compiles it automatically, the speed difference is incredible."
    author: "Hoek"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "> Or, better, look at Gentoo (http://gentoo.org) if you don't have it already. It's a source distribution optimizing everything for your CPU and compiles it automatically, the speed difference is incredible.\n\nI wonder how much of it is a placebo effect. I noticed almost speed difference between Gentoo and Debian on the same machine. On the other hand, I noticed a good deal of difference between gcc 2.9x and gcc 3.2."
    author: "fault"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-04
    body: "Yeah, I wonder about this too.  I have an Athlon4 1GHz with 384MB of RAM.  I notice Gento 1.4 with gcc 3.2 to be a bit faster than Debian, but night and day compared to SuSE or MDK (and to a lesser degree, RH.)\n\n"
    author: "Ben"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-04
    body: "I personally think that much of it *is* a placebo effect. I did notice a slight difference, but nothing revolutionary.\n\nHowever, switching from gcc 2.9.X to gcc 3.2 really made a *huge* difference, especially when using the \"-fomit-frame-pointer\" flag to gcc. I read somewhere that bzip2 runs about 25% faster with that single change to the compile flags, and after recompiling my Gentoo distro I'm inclined to think that it's like that with most programs. Especially KDE startup times decreased noticably, but the desktop also felt much \"snappier\".\n\nUnfortunately some programs are extremely unhappy with -fomit-frame-pointer. One particular version of binutils got completely hosed and left my system unable to compile anything. Much fixing of this has gone on in Gentoo over the last few months, so I'm just about ready to try it again. :)\n\n"
    author: "whocares"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-04
    body: "If you compile kde from sources, don't forget to disable the debug (--disable-debug) and --enable-final to have smaller objects (it will also load faster)\nand -mcpu=i686\n\nI'm not using gcc-3.x yet, but it's already working fast, really fast.\nSometimes I even don't have to click on konqueror icon and konqueror is already loaded :-))))"
    author: "JC"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-06
    body: "configure: error: unrecognized option: -mcpu=i686"
    author: "luci"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-06
    body: "When I suggest -mcpu=i686, it's not an option in ./configure but flags :\nexport CFLAGS=\"-O2 -march=i386 -mcpu=-i686\"\nexport CXXFLAGS=\"-O2 -march=i386 -mcpu=-i686\"\nand then configure, make && make install\n\nSorry, I should have mention that before :)"
    author: "JC"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-06
    body: "it's ok, thanks for explaination :)\ni will try it."
    author: "luci"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "Come on! I'm running KDE3.1b2 on a 266 MHz machine and it is =very= usable, even when I'm compiling stuff in the background. Your computer must be badly set up. Of course, my KDE is self-compiled, but nobody keeps you from doing the same."
    author: "Melchior FRANZ"
  - subject: "Argh !"
    date: 2002-10-03
    body: "My old HP Omnibook with 233 Mhz and 96 Mb Ram runs KDE 3.0.3\nand it's reasonably fast. I'm running SuSE 8.0. The only thing I\nmodyfied was the startkde skript. I removed all SuSE specific stuff and\nnow even login to KDE is fast enough :)\n"
    author: "Thomas"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "This issue has been discussed to death and beyond. It's not like developers aren't aware of the issues you mention, but it certainly isn't as easy as just dropping everything at hand and just \"concentrate on speed\". \n\nI can imagine KDE developers are pretty damn tired of hearing this (and I suspect they long ago stopped reading comments about KDE speed).\n\n\n"
    author: "ac"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "i understand your point, but you also want to see them developing with new PeeCees in mind.<br><br>try running xp on a Athlon400..."
    author: "sorry"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "An athlon 400 does not exist. It started at 500mhz.\nWindowsXP does run pretty fast on a (slot-A) Athlon 600 though. You've just got to turn things like auto-indexing off. Of course, that's more because of the slow hard drive than the processor."
    author: "fault"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-03
    body: "I run it on KDE3 on a 266Mhz laptop and it runs fine.  Sure the \"loading\" is alot slower than Gnome, but who cares.  It's like 2 seconds....not gonna kill anybody.  Actually I fine the memory is a bigger factor in speed.  I guess KDE was a little slow in moving windows around back when I had 128MB ram...but now that I got 512MB it's really fast.  But I think X is the one actually handling all the window moving stuff anyway.  That laptop has 96MB and kde runs fine, but java is slow!!"
    author: "George Bush"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-04
    body: "Hm.. my laptop has even a slower CPU (233 Mhz) and 96 Mb Ram.\nWith KDE I've a filemanager and a webbrowser and built-in\nsftp-client (I like this kio-slave a lot !)\nAll components load almoast immediately (really)\n\nO.k. standard Gnome filemanager (do not even try to _think_ of Nautilus. This beast won't load in half an hour on my weak laptop) loads faster than\nKonqu does... (at least for the first start)\n...but to be fair I'd have to compare Nautilus with Konqu...\n\nFor webbrowsing I can not use Mozilla (another beast that kills my time).\nIf I want to have gecko I have to use Galeon (and even galeon has to load\nthe gecko engine)\n\nSo I'm better of with KDE at the moment\n"
    author: "Thomas"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-04
    body: "I could not understand why AMD400 is slow for KDE3. I run KDE3.0.3 Keramic on Armada 1500c Celeron 366Mh with 64mb RAM. I even run KDE 3.1 beta on it. It works fine. I find Gnome2 a bit slower than KDE3. I could assure you that KDE 2.1 is slower than KDE 3. I guess XFree86 on this AMD400 is not properly configure. Do a bit of reading on what graphic you got and visit xfree86.org for how to configure it properly."
    author: "NS"
  - subject: "Re: Great, but still too slow -> QT 3.1?"
    date: 2002-10-04
    body: "For the first beta I compied QT 3.1 beta 1 and then compiled KDE.\nFor some reason things seemed much more responsive then than with\nQT 3.0.5???\nBut now I see that the requirements say that B2 won't work with QT\n3.1 B1 (or B2). There was only a small glitch in the UI with QT 3.1\nB1 and KDE 3.1 B1.\n\nH\u00e5vard"
    author: "H\u00e5vard"
  - subject: "Re: Great, but still too slow -> QT 3.1?"
    date: 2002-10-04
    body: "It says that it should work with Qt-3.1-b2 so what's the problem?\n\nregards\nCharlyw"
    author: "Karl G\u00fcnter W\u00fcnsch"
  - subject: "Re: Great, but still too slow"
    date: 2002-10-06
    body: "funny, i'm sitting on a p166 running kde3 right now, and it's fine... it just need rams (it's kinda nasty in 32meg, but in 64meg it's all good(tm)).\n\t-jj"
    author: "jaymz"
  - subject: "Re: Great, but still too slow"
    date: 2003-03-26
    body: "How on earth does it work fast on a pentium166?\nI have a Pentium III 550Mhz Coppermine with 128mb ram using gentoo and it works slow\nThe main problem is that it takes way too much memory and swaps like a maniac.\nWhen it doesn't swap it works really fast, but it always swaps so it always works slow.\n\nIs there any way reduce the memory consumption or something like that?\nAm I doing something wrong? my gentoo version is 1.4rc1, I emerged kde by running 'emerge kde'\nMy CFLAGS and CXXFLAGS are \"-march=pentium3 -O3 -pipe -fomit-frame-pointer\"\nPeople here with 350mhz processors say it works really fast for them, then I don't understand what's wrong with my system."
    author: "laz-e-coyote"
  - subject: "great but...."
    date: 2002-10-03
    body: "kde is great and i use it for all my work!!! thanx\n\nbut - who is choosing names such KRfb and KRdc for desktop sharing????\n\noh man. is it so difficult to name it deskshare or something else u can remember?\n\nanyway. looking forward to kde3.1.\n\n-gunnar"
    author: "gunnar"
  - subject: "Re: great but...."
    date: 2002-10-03
    body: "KRfb and KRdc are internal names, you will never see them (unless you call them by command line and look at the source code). The public names are 'Desktop Sharing' and 'Remote Desktop Connection'. \nRfb stands for Remote FrameBuffer, the first version was based on an app called x0rfbserver. Rfb is also the official name of VNC's protocol. Rdc stands for Remote Desktop Connection."
    author: "Tim Jansen"
  - subject: "Re: great but...."
    date: 2002-10-07
    body: "thanx.\ni still didnt have the time to run this new beta. thanx for explaation\n-gunnar"
    author: "gunnar"
  - subject: "Compileing: Some users will have some problems."
    date: 2002-10-03
    body: "Error 1: kdeprint/cups will not compile if you got XFree compiled from Source (also affects some distributions).\n\nSolution 1: Enter '#define HasZlib  YES' into the host.def file when you compile XFree to avoid this error. If you already compiled XFree, the solution is to do a 'rm /usr/X11R6/lib/libz.a'.\n\nError 2: The kdepim package will not compile. There is no GLOBAL include library named kdateedit.h included on line 27 in dateedit.h by recurrenceedit.cpp in line 54.\n\nSolution 2:\nCopy kdateedit.h from ../kdepim-3.0.8/libkdepimin to the $KDEDIR/include before you compile this package.\n\nThis is, ofcource, a beta, so shit happens. \n\nI got the impression from a developer that 'if a user fails to build xfree correct then it is the users own fault.'. My opinion is that ./configure should detect as many strange configuration errors as possible and adapt. \n\nhttp://oyvinds.tk/"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Compileing: Some users will have some problems"
    date: 2002-10-04
    body: "> I got the impression from a developer that 'if a user fails to build xfree correct then it is the users own fault.'. My opinion is that ./configure should detect as many strange configuration errors as possible and adapt. \n\nSorry for the problems, but\n\n1. This IS Beta, this is known to have bugs. Please report them (thanks, you did), best would be bugs.kde.org\n\n2. It will be impossible to handle all possible configurations. If you know such magic stuff for autoconf, your patches are welcome. Use kde-devel for posting or join the team :-)\n\n3. Coolo is doing a great job. If he's missing something, fine with me. I don't know anything about all the configuration stuff and I'm glad to have some who knows. If there is a mistake, okay, can be fixed and done.\n\nNo need to blame or thinking there are wrong attitudes. Mistakes happen and as long as someone notices theme the right way, they will be fixed.\n\nPhilipp"
    author: "Philipp"
  - subject: "Idea for RedHat users"
    date: 2002-10-03
    body: "So normally I'm here saying \"I wish I had RedHat RPMS\" but now that I'm running \"nullified\" KDE, I have a different idea.\n\nAlthough KDE people are in no way responsible for RedHat's f*ck-ups, they may be able to do things to improve their image to the RedHat-using world that only knows nullified KDE, and they don't want to change distros.  Instead of providing RedHat RPMs for every KDE milestone, I think it would be interesting if some industrious KDE coder made \"Corrected RedHat RPMS\" that address the version of KDE most recently vandalized by RedHat in an official release.  Or even \"Differential Corrected RedHat RPMS\" that only fix what's broken.\n\nFor example, RedHat 8.0 features a nullified KDE 3.0.3.  A corrected 3.0.3 version could be released, and no more would be released until the next RedHat release.  This would be considerably less frequent work than providing RedHat versions for all recent milestones.  Also, since RedHat will have to periodically resync their branch with the main KDE tree, it will become increasingly easy to figure out what they're breaking and change it back.\n\nAnyway, yes it's a lot of work still.  But it would be a big public relations improvement over the current situation, when the world at large thinks we're just whining about a theme change (they really think we can just switch from Bluecurve to Keramik and it will stop being broken).  If even some of the rest of the world sees everything RedHat broke, maybe things will improve.\n\nAnyway, just an idea.  Not even any code to back myself up.  But I think it's worth thinking about."
    author: "ac"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-03
    body: "Yes, this is indeed a very interesting idea.\n\nI hope KDE developers / webmasters / packagers also think this way and present `Corrected KDE packages` for RedHat users.."
    author: "Hoek"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-03
    body: "The thing is most KDE coders would rather code than produce rpms. KDE rpms for RedHat are produced by industrious users, you might want to target them instead. I would prefer that those who do not like what RedHat does either get their rpms from another source or compile the source.\n\nAddressing your 'image' comment, I do not think KDE has anything to prove to RedHat users. I say this as a long time RedHat user. I use KDE and prefer it over the other desktops environments I have used. But then, I am quite comfortable compiling stuff. Now, if you were to volunteer to provide these rpms, I am sure the effort would be appreciated but the community. I believe all you would need to do is find the source rpms and compile them on an RH8.0 system. If source rpms are hard to come by, use the official tarbols and make a spec file. Good sources of info on this would be Texstar or Rex Dieter."
    author: "ne..."
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-03
    body: "Yeah.. that bit of patching KDE to comply with freedesktop.org-standards are the worst piece of vandalism I've ever seen.\n\nIt even (shock horror) includes unified panel-notification between GNOME and KDE, so developers won't have to code them twice."
    author: "Gaute Lindkvist"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "A closer inspection of the site freedesktop.org would show that it is hosted by Red Hat.  So I guess we can infer that it was easy for them to comply with their own standards."
    author: "Paul Seamons"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "Oops my bad - read further in:\nhttp://www.freedesktop.org/about/\n\n\"Nothing on this site should be taken to be endorsed by any entity, commercial or otherwise; including Red Hat, Inc. or any of the desktop projects. freedesktop.org does not have a list of supporters or sponsors because it is an informal collaboration site. This site is what its contributors make it. \""
    author: "Paul Seamons"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "That still doesn't change anything on the fact that some of the standards on freedesktop.org are just \"standards\" (for example, the systray spec is version 0.1 and I doubt even GNOME fully implements it). But that of course can't prevent RedHat from claiming that are making KDE more standards compliant."
    author: "L.Lunak"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "Red Hat has patched GNOME and KDE to comply with the systray spec. These will be merged into GNOME and I hope they will be merged into KDE as well."
    author: "Gaute Lindkvist"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "http://lists.kde.org/?l=kde-core-devel&m=103305535112182&w=2"
    author: "Anonymous"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "Great! At least some people are mature about this."
    author: "Stof"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "You're right, of course.  The items you mentioned are much worse than, say, needing to go to the command line to shut the computer down, or, say, needing to go to the command line to edit the KDE menu, or, say, the fact that the KDE font installer has been completely broken, or, say, the fact that the KDE login manager tools don't seem to have any effect on your login process.  These are trivial issues.  Practically \"features\" even.\n\nYes, unified panel-notification is a much, MUCH bigger deal.  Thank you so much for bringing that earth-shattering discovery to our attention."
    author: "ac"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "\"needing to go to the command line to edit the KDE menu\"\n\nSo Red Hat didn't do as good a job with this as Mandrake. Mandrake has also added their OWN menu-system that is not the same as KDE-standard.\n\n\"needing to go to the command line to shut the computer down\"\n\nTotally untrue. Even if there aren't any menus about it in Red Hat KDE, you can just log out and choose shutdown from the login-manager\n\n\"the fact that the KDE font installer has been completely broken\"\n\nThat is a pretty sad and buggy side-effect of Red Hat patching KDE to go with the new and soon to be everywhere fontconfig-system. The system is really much better than the current one, and will hopefully be added to KDE-standard sooner rather than later.\n\n\"KDE login manager tools don't seem to have any effect on your login process\"\n\nGNOME uses GDM by default. It is a nice and good-looking login-manager, and frankly I couldn't care less wether Red Hat uses GDM or KDM. People still have a choice of Desktop Environment, and Red Hat's KDE is actually pretty nice."
    author: "Gaute Lindkvist"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "> the fact that the KDE font installer has been completely broken\n\nAre you sure it's not because the KDE font installer doesn't support fontconfig?\nAnd why use the font installer if you can throw all your fonts in ~/.fonts?"
    author: "Stof"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "You miss 2 things entirely here:\n1. Proposal's aren't standards. If RH wants to see them adopted by KDE and to become standards, and they provide patches that implement them -- that's great, and it's good that they did it with SysTray stuff, if only in a weird half-backwards manner.\n\n2. Even if something is the best idea in the world, it's not always good to changes sources to include it. Why? Because writing stable software is hard, and freezes exist for a reason. KDE3.0.x has been hard frozen for more than 5 months -- the *only* changes made to the source code were bugfixes, and it shows. KDE3.0.3 and the upcoming 3.0.4 are much better desktops than 3.0.0 was. But the stuff you allude to isn't bugfixes at all, and is of various degree of invasiveness (the icon stuff is minimal; since the icon spec proposal is basically KDE icon theme format; the tray is more invasive, but localized, and the vfolder stuff is insanely invasive, triggering massive renames, etc.); thus there is a significant potential for introducing noticeable regressions. \n\n\tThere is a big tradeoff here -- one must weight the potential for things going really wrong (which they'll nearly certainly will, as they have gone wrong with less invasive MDK-menu changes, for instance), and the potential benefit to users and to the distrubutor (I certainly understand why RH, MDK or Debian don't want to provide separate menu entires for every concievable desktop in each package). And here, the issue of qualification and Q&A comes in. Would you agree that the more experienced a developer is, and the more resources are spent testing changes the smaller the chance that something will go wrong? \n\nWell, it just happens that RedHat8's version of BlueCurve has about 9 or 10 bugs that I found in about 15 minutes of examination; many of them are kind of cornercase and it's not at all surprising that they weren't detected (seeing that they have 0 experience and allocated very little time to develop a bug-free widget style), and there'd probably be at least a dozen more really cornercase ones I didn't notice and which would take a while to notice; but about 2 or 3 of those bugs are blatantly obvious, and really should have been flagged by Q&A people. They weren't. I'll not even comment about the default kstylerc, since I am afraid I'd get rather impolite (HINT: If you switch from BlueCurve away to a KDE Style like HC/Default, dotNET, or the CVS Snapshot of Keramik that's out of date by about half a dozen bugfixes, make sure to go to the effects tab and reset the transparency settings to something sensible -- if I understand their file setup right, the defaults would be absolutely and utterly useless unless one feels that KDE shows popup menus too quicky). Now, hope you enjoy your desktop experience; and I hope RedHat made all those changes without breaking anything, since otherwise users will suffer the consequences.."
    author: "Sad Eagle"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "It's interesting idea, that's right. But where is the advantage for KDE?? It's a lot of work for a distributor who don't want support him?? What about the other distros, then they asking too \"Make my packages. I must pay for that and RedHat get it for free\". I think that's the wrong way."
    author: "Marc Tespe"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "How many times have you seen someone say \"I'm going to try out Linux 7.3\" or \"Linux 8.0\".  To a lot of people *including reviewers who should know better but don't* RedHat IS Linux.  And furthermore, RedHat's KDE is the only KDE they will ever see.  Sure it's wrong.  But you certainly can't solve the problem by ignoring it.\n\nWhen a recent RedHat (pre-8.0) release came out, I remember the reviewers IMMEDIATELY crowing about how wonderful the new KDE interface was.  Does anyone else remember?  It was in practically every review.\n\nWell, imagine a review from the future of RedHat 8.1: \"The new Gnome interface is slightly better than the last version, but the new KDE interface--what happened?!?  KDE apparently no longer supports anti-aliasing!  There does not appear to be any printer integration in KDE 3.1!  What are they thinking?\"\n\nDo you really think a few angry e-mails combined with the knowledge that they're wrong is going to fix this?  What about when KDE is referred to as \"You know, that interface that crashes all the time...\"\n\nLike it or not, RedHat is KDE's showcase to a lot of the world.  Yes, the world should know better.  What better way to educate the world than to have a very public resource for more knowledge about all of the errors in RedHat's KDE, and a way to fix them?"
    author: "ac"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "I think you are making the mistake of equating the USA or maybe even North America as being the whole world. In Europe, SuSE and Mandrake are 'fighting' it out. In South America, RH is not king. Africa, Asia and Australia I am not sure about. What you might say is true is that in the USA, RH has a large percentage of the marketplace for Linux. Especially when you consider how the suits look at things. However, what you speculate might happen in RH8.1, I do not think will come true. RH will not stoop that low. At worst, they may drop KDE. RedHat's image is what will be on the line, not KDE."
    author: "ne..."
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "Sure, but for those of us living in North America, it's kind of an important part of the world ;-)"
    author: "Sad Eagle"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "> How many times have you seen someone say \"I'm going to try out Linux 7.3\" or \n> \"Linux 8.0\". To a lot of people *including reviewers who should know better but\n> don't* RedHat IS Linux.\n\nMost people I've seen *know* that RedHat is not the only Linux. If reviewers are as stupid as AOL users then that's their problem.\n\n\n> Well, imagine a review from the future of RedHat 8.1: \"The new Gnome interface \n> is slightly better than the last version, but the new KDE interface--what \n> happened?!? KDE apparently no longer supports anti-aliasing! There does not \n> appear to be any printer integration in KDE 3.1! What are they thinking?\"\n\nAnd people think \"Bah, Linux sux!\", switch to Windows XP and never look back again. RedHat loses potential costomers and profit.\n\nI don't know what reviews you read but in all the reviews I've ever read, the reviewer has at least a little understanding of what's below the Click&Drool interface and knows about the existence of Xft and that the GNOME/KDE projects are independent. Clearly your reviewers are just idiots.\n\n\n> Like it or not, RedHat is KDE's showcase to a lot of the world.\n\nYou're making RedHat bigger than they really are. The vast majority of Linux users are not that ignorant, otherwise they wouldn't be using Linux in the first place. Desktop Linux users are scattered among RedHat, Mandrake, SuSE, Debian, <insert other popular distros here>. RedHat != Microsoft, and will never get the same kind of power that Microsoft has.\n\n\n> Yes, the world should know better. What better way to educate the world than to have a\n> very public resource for more knowledge about all of the errors in RedHat's \n> KDE, and a way to fix them?\n\nK button->Panel->About KDE?"
    author: "Stof"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-04
    body: "Uhuh. Ac is actively trying to kill RedHat again."
    author: "Stof"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-05
    body: "After putting Red Hat on my desktop I was expecting to be dissapointed.  I had heard all of the press by how bad Red Hat had fucked up KDE by changing around all sorts of default KDE applications and putting in GNOME applications.\n\nWell, surprisingly enough - I love the new Red Hat.  It is not for the way that Red Hat changed around applications, but how KDE and GNOME applications have the same look and feel.  I use KDE and GNOME applications next to eachother and being able to have a consistant look and feel just feels a whole lot better.\n\nI think that Red Hat has some good ideas, if not just about having the two desktops fit together and give me, the user, the idea that I am using one cohesive application.\n\nJust my $0.02."
    author: "Chris Parker"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-05
    body: "> I had heard all of the press by how bad Red Hat had fucked up KDE by changing around all sorts of default KDE applications and putting in GNOME applications.\n\nSearch KOffice (btw only 1.2rc1) in the menus or try to start it when running the GNOME desktop. It's missing/fails miserably on my test installation here."
    author: "Anonymous"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-05
    body: "Yea, I heard that KOffice was missing from the GNOME menus (not really an office app user myself, so I just kept the default OO install).  Was there a bug report filed on this app?  I sent in a general bug report that a whole lot of KDE apps were not showing up in the GNOME menus, and it looks like they fixed most of that.  I will give them the benefit of the doubt and assume that it wasn't intentional. "
    author: "Chris Parker"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-05
    body: "Is there ANY reason for them to do that intentionally? What do they gain by \"killing KDE\"? The only things they will get are tons of new anti-Linux trolls, lots of negative publicity and lose potential customers."
    author: "Stof"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-06
    body: "> Is there ANY reason for them to do that intentionally? What do they gain by \"killing KDE\"? The only things they will get are tons of new anti-Linux trolls, lots of negative publicity and lose potential customers.\n\nAnd Mandrake came up from a happy accident of adding a default KDE to Red Hat years ago and has since overtaken them by some counts on US desktops. RH has gotten really bad press like the recent event where they asked KDE people to showcase RH even though the sponsored developers in KDE are from SuSE and Mandrake and RH has done nothing but pour money into GNOME development. Not that one can fault them for who they sponsor, but it's telling when you consider that their desktop push coincides with GNOME 2 and could have easily begun with KDE 3. \n\nOf course there's more history than this. Where have you been? I'll never buy RH nor let my friends without recommending Mandrake instead. However RH continues to wrap up lots of sweet corporate contracts. So I guess they can affort to piss away a lot of people like me who bought a box in 1999 and will never spend a dime on them again."
    author: "Eric Laffoon"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-06
    body: "And the reason why RedHat chose not to include KDE in the first place was because QT wasn't Free Software. KDE *was* included in RedHat 6.0 and up.\n\nI don't get you people... 50% complains that Linux will never succeed on the desktop because of *lack of corporate support*, and the other 50% complains that GNOME sucks because \"they're taken over by evil corporations\". Sigh...."
    author: "Stof"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-07
    body: "> KDE *was* included in RedHat 6.0 and up.\n\nCome on. There was *one* RedHat developer for packaging the entire KDE. And no support at all.\nOh, and this developer (Bernhard Rosenkraenzer a.k.a bero) has left RedHat. He didn't want to be involved in a dubious crippleware.\nhttp://lists.kde.org/?l=kde-devel&m=103294657505181&w=2\n\n\n\n"
    author: "anonymous"
  - subject: "Re: Idea for RedHat users"
    date: 2002-10-08
    body: "> And the reason why RedHat chose not to include KDE in the first place was because QT wasn't Free Software. KDE *was* included in RedHat 6.0 and up.\n\ncheck the dates, it doesn't work out right. RH shipped QT1 (with KDE1), that was still under QPL. Only QT2 was under GPL.\n\nRH shipped KDE simply because they were loosing \"market shares\" to Mandrake. And their poor KDE packaging has awlays helped Mandrake a lot. RH8.0 is just another bad step in that direction, but i think they don't care anymore.\nThere is a very simple truth i see nowhere. I'm almost sure that Sun, Redhat and co are pushing GNOME for one very simple reason: GNOME uses GTK, that happens to be under LGPL. QT is under GPL (and only QT/X11). You have to pay to Trolltech if you want to develop proprietary software for KDE. You don't have to pay anyone if you want to do it for GNOME. RedHat and folks are aiming at corporate business, where some apps don't have any free software counterpart (or where the free software counterpart is thousands light years from that), and probably will never have. So, what do you think they will do ?\n\n1- push GNOME as much as they can\n2- do everything so that KDE looks like GNOME, and is compatible with GNOME, even if that should break KDE (see point 1).\n\nPay attention: no big conspiracy theory here, just flat reasoning leading to obvious consequences."
    author: "loopkin"
  - subject: "KDE is mature"
    date: 2002-10-04
    body: "I haven't tested KDE-3.1beta2 yet, but I'm running KDE-3.0.3\nFinally there's nothing that bothers me anymore.\n(I know of some bugs, but they don't interfere with my daily usage,\nso for me everything is a-ok)\nTruly, for my daily desktop chores, everything works fine.\nLinux ownz!\nTnx developers.\n"
    author: "dwt"
  - subject: "Feature plan update??"
    date: 2002-10-04
    body: "Been a while since the feature plan page has had an update.  It'd sure be nice for us non-developer types to follow along with what all is going on.\n\nhttp://developer.kde.org/development-versions/kde-3.1-features.html\n\nAny chance for an update to this now that Beta2 has hit the streets?  Love to know what all to look for in testing it out over here in FreeBSD land.\n"
    author: "Metrol"
  - subject: "Re: Feature plan update??"
    date: 2002-10-08
    body: "me too ;)\n"
    author: "Richard"
  - subject: "RedHat Null"
    date: 2002-10-04
    body: "One thing I really liked with RedHat was, that I could download the latest kernel from ftp.kde.org, copy the .config to the new kernel directory, do some selections anc compile, the same with KDE and much other stuff. I found this a bit difficult in other distros because of strange patches.\nWith the null version things has changed, I cant even compile the kernel. It faisl due to missing includes, damn I thought, lets try to compile KDE so it looks more \"KDE'ish\", but ney.. KDE would'nt compile either (and damn the Nullified version is ugly!)\nWell, Ive gotten a new distro with gcc-3.2, it must be really fast I thought.\nSo, I made a race between my two identical machines. (They normally booptup within the same time, only a few seconds apart) this time it was way big difference!!\nThe old one (with the same services enabled) booted way faster!!! So the nullified version if way gone from my machines, and I'm happily back to SuSE,\n\nIf anyone ever gets to know why kde wont compile on RH, I'd like to know so I can make alternative RPMS to the nullified version."
    author: "Jarl Gjessing"
  - subject: "Re: RedHat Null"
    date: 2002-10-04
    body: "KDE 3.1 Beta 2 compiled just fine on my new Red Hat 8 system (tarball sources). I have some minor issues with the kdepim package, but everything else compiled with no changes, and it is very responsive.  I was also able to compile Beta 1 on a (null) system.  You must have been missing some packages.\n\nAs for the kernel rebuild, it does seem to be broken on RH 8.  I haven't had a chance to see why..."
    author: "Anonymous"
  - subject: "Kolf Courses"
    date: 2002-10-04
    body: "Hi all,\n\nJust a reminder that new in this release is Kolf, a minigolf game.\n\nhttp://katzbrown.com/kolf/\n\nYou can design your own courses (read the http://katzbrown.com/kolf/About/secret/editing.html documentation), or download more courses from the Kolf Courses page: \n\nhttp://katzbrown.com/kolf/Courses/User Uploaded/\n\nYou can also upload your own courses:\n\nhttp://katzbrown.com/kolf/Courses/Upload/\n\nI like comments: jason@katzbrown.com\n\n\nThanks so much,\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Kolf Courses"
    date: 2002-10-07
    body: "Hey -- I really, *really* like Kolf! It's replaced Mahjong as my favorite game to play when I want ten minutes off from programming.\n\nI'll read the editing tutorial and perhaps even contribute a course or two.\n\nThanks for the game!"
    author: "Shamyl Zakariya"
  - subject: "Re: Kolf Courses"
    date: 2005-05-06
    body: "this is the shiznit...i been playin this nizzle whenever i need a break from smoking the wizzle...yo send me some more courses fo shizzle....help a brother out\n\nrepra-Z-ent my nizzles"
    author: "snoop d-o-double-gizzle"
  - subject: "Re: Kolf Courses"
    date: 2005-05-06
    body: "That's quite poetic, snoop d-o-g."
    author: "anon"
  - subject: "Re: Kolf Courses"
    date: 2005-05-12
    body: "fo' sho' foo. That is tha most poetic shizzle you'll eva see in yo' cracker lyfe fo' hizzle. Now, down with some of them kolf courses or imma 187 on yo' ass n pimp yo ride, biznatch. Peace, holla, repre-Z-ent."
    author: "Snoop D-o- double gizzle"
  - subject: "How about LSB-packages for KDE 3.1?"
    date: 2002-10-04
    body: "I wonder if there are plans for relasing LSB-compliant RPM for KDE 3.1. Now that all major ditributions support this standard, we can work on one real good RPM for all distibutions. It should make live a lot eassier for all people.\n\nCheers!"
    author: "Maarten Rommerts"
  - subject: "Re: How about LSB-packages for KDE 3.1?"
    date: 2002-10-04
    body: "Really for all? I thought Red Hat is now (again) incompatible to all others due to the glibc snapshot."
    author: "Anonymous"
  - subject: "Re: How about LSB-packages for KDE 3.1?"
    date: 2002-10-05
    body: "Redhat's new glibc \"snapshot\" is backwards compatible. Apps compiled for the LSB libs should work without problems, at least regarding glibc. \n\nWhat you cannot do is compile stuff on the new Redhat with the shiny new glibc headers and expect them to work on all distros. You'd have to install compatibility libraries, use a chroot to build or another distribution (Debian oldstable or stable are good systems, they're ususally not using the latest libs).\n\n-Malte"
    author: "Malte Cornils"
  - subject: "Re: How about LSB-packages for KDE 3.1?"
    date: 2002-10-05
    body: "What does the LSB have anything to do with binary compatibility?"
    author: "Stof"
  - subject: "Re: How about LSB-packages for KDE 3.1?"
    date: 2002-10-04
    body: "All major distos using the Redhat rpms ...\nDon't forget those who are using .deb, .tgz, and others"
    author: "JC"
  - subject: "Re: How about LSB-packages for KDE 3.1?"
    date: 2002-10-05
    body: "Just install rpm and install those packages. Not very clean and no dependency handling, but better than nothing. :)"
    author: "Spark"
  - subject: "Re: How about LSB-packages for KDE 3.1?"
    date: 2002-10-20
    body: "No standart - no future!\nPlease support LSB"
    author: "Jo"
  - subject: "A wrong icon??"
    date: 2002-10-05
    body: "Hey, there is a mimetype icon named sownd.png in icons directory. There is a icon named sound.png too.\nIt is located in $KDEDIR/share/icons/crystal/64x64/mimetypes/sownd.png.\nIs it a mistake or a real icon??"
    author: "Daniel Dantas"
  - subject: "Re: Should really SMALL BUGS be reported?"
    date: 2002-10-05
    body: "[x]$ cd /opt/kde/share/icons/crystal/64x64/mimetypes\n[x]$ ls -al sownd.png \n-rw-r--r--    1 root     root         4756 aug 20 15:43 sownd.png\n[x]$ ls -al sound.png \n-rw-r--r--    1 root     root         4756 aug 20 15:43 sound.png\n[x]$ diff sownd.png sound.png \n[x]$ \n\nI'll bet you Svalbard (who want's it anyway) this is a typo error, they _are_ the same files. I haven't reported this to bugs.kde.org, maby someone should, or maby it's a too insignifficant bug.\n\nShould small bugs like this be reported?"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Should really SMALL BUGS be reported?"
    date: 2002-10-05
    body: "Every bug should be reported.\nHow will the maintainer notice it otherwise?\n"
    author: "Philipp"
  - subject: "Re: Should really SMALL BUGS be reported?"
    date: 2002-10-05
    body: "I really don't report small bugs like this in the bugzilla. I think this will overload the developers with small things. Where is the better place to report bugs like this??"
    author: "Daniel Dantas"
  - subject: "Re: Should really SMALL BUGS be reported?"
    date: 2002-10-07
    body: "Bugzilla is still the best place (I'm a developer of KSpread):\nSeveral reasons:\n1. I want to look at one place only. Other places I may find, but don't look at frequently, or the other developers don't look at. And I'm working in my freetime only.\nSo maybe I see the bug, but don't have time at the moment. Others don't see it. And 2 weeks later I forget to look there again.\n\n2. Bugs get lost otherwise. Even if you write to a app specific mailing list, there is no tracking involved. I normally don't look on mails which are older than one week and even if I would look, I wouldn't know if they are fixed by someone else already.\n\n3. To get involved into programming, for newbies it's often the first step to look into the bug tracking system. So some \"easy\" bugs help them getting involved. It wouldn't help them if there were only the hard things (this was the way I got involved: Fixing some small bugs).\n\n4. Maybe someone fixed your bug due to a notice somewhere. Later another one mention it in the BT system, because he didn't know it is fixed. Now I as a developer see the bug, test it and find it's working. This is the most unpleasant situation for me, as I'm now not sure if it is a local problem, a distribution problem or whatever. So this will steal time, which I better have spent for other bugs.\n\n4. Last but really not least: Hey, when I look into the BT system, I want to have some easy things as well. It's so much pleasant to write to the commit: Fixed bug #123. If there would be only the hard parts, I may get disappointed and look less frequently into the system. Even bughunting can be enjoyable if you at the end can fix some bugs and not only one per week.\n\nSo write them to the BT system, yes oneliners are really welcome too!"
    author: "Philipp"
  - subject: "Re: A wrong icon??"
    date: 2002-10-05
    body: "It's now gone."
    author: "binner"
  - subject: "[OT] KC KDE?"
    date: 2002-10-05
    body: "Offtopic, but what happened to the Kernel Cousin for KDE?\n"
    author: "AC"
  - subject: "Re: [OT] KC KDE?"
    date: 2002-10-05
    body: "a few month ago, there wasn't a problem of maintainer ?\n:-(\n"
    author: "JC"
  - subject: "Sad news... KDE League found dead :("
    date: 2002-10-05
    body: "<a href=\"http://www.linuxandmain.com/modules.php?name=News&file=article&sid=247\">I just heard the sad news on talk radio.</a>  KDE League Inc. nonprofit corporation was found dead. Even if you didn't understand how the League was really supporting KDE, there's no denying its contributions to the promotion and development of free software. Truly an Open Source Icon."
    author: "kdeleague == dead"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-05
    body: "http://lists.kde.org/?l=kde-core-devel&m=103380337729334&w=2"
    author: "Anonymous"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-05
    body: "Ah.Yet another insidious KDE mud throwing article from Linux and Main.\nNothing surprising, after the abject thing their webmaster did.\n\nLooks definetly like an extreme right wing american patriots HQ.\nHow disgusting.\n\n\n\n\n"
    author: "germain"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-06
    body: "> Looks definetly like an extreme right wing american patriots HQ.\n> How disgusting.\n\nMan that's just upsetting garbage! It's bad enough I come home to read things from people I once thought were friends of KDE like that article. It has been no fun reading dep for a while and this was just not what I wanted to read. Then I read crap like this. Here's a news flash. The Quanta plus project is headed up by a middle aged guy with a ponytail who sells catnip... and could very well be called a right wing American patriot.\n\nTo people who don't like Americans I say get the chip off your shoulder, stop living in stereotype land and consider a journey thorugh history. What fascist dictator would be pushing you around if not for the good old US of A? And what does this have to do with KDE?\n\nI have enough problems without bad news. I am trying to figure out how to sponsor a developer who has been working full time for months on open source projects with nearly no support. I'm trying to figure out why less than 0.01% of our user base will chip in a few dollars to help me pay for guys with incredible talent to work full time cheap. All the volunteer coders are just too busy with life to do much, so much for that open source myth.\n\nNow I guess I should thank you for reminding me that to top it all of there are a number of people cheering the project that I love willing to hate me because of where I live. Thanks. I will forgo the urge to find out and denegrate your nationality as I find such behavior reprehensible. Maybe I should just say that I was always taught that it was un-American to speak ill of other people's nationalities. The US is a \"melting pot\" of nationalities... so your kinsmen are here and proud of both countries. Go figure."
    author: "Eric Laffoon"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-06
    body: "> To people who don't like Americans I say get the chip off your shoulder,\n<snip>\n> Now I guess I should thank you for reminding me that to top it all of there\n> are a number of people cheering the project that I love willing to hate me\n> because of where I live.\n\nWhy do you take it personal? I said \"people at Linux and Main looks like right wing patriots\".\nDid I speak about North Americans as a whole ?\n\nFrance had 18% of people voting for a right wing extremist named Le Pen. If you say 'Le Pen is a right wing extremist', I'll agree wholeheartedly - I won't feel like you mean \"all French are right wing extremists\".\n\nThis webmaster just happen to put has headlines on a general 'Linux news' site insulting articles comparing explicitly the project you sherish as a HQ of Nazis, articles about good ol'times where, as a child, he was playing with weaponry and GI relics in a so-called 'bookshop', being taught \"war stories\" by the owner and his friends.\nBut obviously I overlooked. It was just an innocent joke and he didn't mean it. We are all joking.\n\n> And what does this have to do with KDE?\n\nAgreed. This whole thread is OT. \n\n\n"
    author: "Germain Garand"
  - subject: "OT Thread - problem with the Dot?"
    date: 2002-10-07
    body: "Agreed, this whole thread is OT.  Maybe there would be less OT threads if the ppl running dot.kde.org posted more news once in a while.  Is the problem that people aren't submitting enough?  Or are the people maintaining the site just unwilling to put up anything that isn't happy happy news of a release?  I know that putting up articles about RedHat-KDE or LinuxandMain's story is a great way to ensure flame wars and trolls, but by not posting anything we just get flame wars and trolls underneath the articles that do get posted.  The noise in threads like this one would probably be minimized if we had more news to talk about, and a more relevant place to argue about issues, if that is what people must do."
    author: "anon"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-08
    body: ">>What fascist dictator would be pushing you around if not for the good old US of A?\n\nWho told you to \"save\" us ? The remaining of the reply was mostly acceptable, but this is not."
    author: "Anonymous"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-08
    body: ">>What fascist dictator would be pushing you around if not <snip> the good old US of A?\nBetter that way."
    author: "anonymous"
  - subject: "Re: Sad news... KDE League found dead :("
    date: 2002-10-08
    body: "You would have preferred not to have been saved?\n\nIn fact, however, all of the free governments of Europe and the U.S.A. signed a treaty so, indeed, there was some asking going on Europeans' part.\n\nI say this as a non-American and non-European. It is sad that so many Europeans and Americans hate each other."
    author: "Ji"
  - subject: "KDE League found alive :)  -  LinuxAndMain dead :)"
    date: 2002-10-05
    body: "Now can we have an article discussing this mess instead of a thread in this article?  At least there is an update on /. now.  I used to think that all of the press Linux was getting recently was a good thing, but there have also been a few \"journalists\" who have been completely full of crap.  They seem to want to just stir things up a bit with their speculation and lies.  Too bad those sites are getting so desperate for hits that they resort to crap like this.  Eventually nobody will take them seriously, much like that crap site MozillaQuest."
    author: "Anonymous"
  - subject: "pixie+"
    date: 2002-10-05
    body: "i know development of pixie is frozen because mosfet is gone (does someone knows details?) - just one question .. is someone able to compile pixie+030 on kde31b2? ... trying like hell here ...\n\nrgds\nmarc'O"
    author: "marco puszina"
  - subject: "Finally someone ported some screensavers ..."
    date: 2002-10-06
    body: "After wandering around in my freshly compiled KDE 3.0.8 i discovered some recent OpenGL-screensavers! yes, that's how to impress me, truly ... hopefully more of them are coming."
    author: "Mathis Moder"
  - subject: "Re: Finally someone ported some screensavers ..."
    date: 2002-10-06
    body: "i found \"Kflux\", \"Kblob\" and \"Keuphoria\", since KDE3.1 Beta1.\nreally great stuff! that makes KDE keep on rocking!\nthx"
    author: "mononoke"
  - subject: "KRfb"
    date: 2002-10-06
    body: "Am I the only one who dislikes the new \"invitations\" system of KRfb?  I don't want to invite people to use my computer, I want it set up so *I* can use it remotely.  I know that you can set this up through the control center, but I really liked the old way, where you started KRfb and you got that little icon in your tray (the tray sucks beyond suck, by the way, applets are much better, but I digress).  Now I can't just run KRfb when I want to use VNC, I have to go through kcontrol and then when I don't have my icon maybe I'll forget that VNC is running, etc."
    author: "KDE User"
  - subject: "Re: KRfb"
    date: 2002-10-08
    body: "This is not a use case that I wanted to solve with krfb. The main intention was to let other either friends with invitations, or administrators with real accounts, control the system.\n\nYour use case is a different one, and I dont think it can be served well by krfb - there will be better solutions for this. Unless somebody adds a lot of stuff to XFree (and we forget the existance of other/older x11 servers), it will not be possible to have a much better performance with 'sharing' VNC servers. Nor will things like changing the cursor be possible. krfb is probably not the right solution for your problem. \n\nSolutions are:\n- (available immediately) set up a regular vncserver and edit ~/.vnc/xstartup to start KDE instead of the terminal+twm\n- (for 3.2, I hope) krdc will have an embedded X11 server and can log in on a remote computer via ssh and start a KDE session. With this, krdc can connect to every computer that has KDE installed and a sshd running, with the same interface that is used for VNC. Only the performance sould be better.\n- (in the future) the XFree guys are working on an extension that will allow to migrate X11 apps from one server to another, even if they have different resolutions. When Qt supports this, you can take your desktop whereever you go.\n\nBTW it would quite easy to write an applet that controls krfb using dcop, if you really want to do this."
    author: "Tim Jansen"
  - subject: "Re: KRfb"
    date: 2006-09-09
    body: "how do i forward this program so it has access to my extrernal ip so my friends can access this when i need help, or me form another loacation not on my network? if u have an answer either email me or reply. mallozjc@optonline.net"
    author: "JC"
  - subject: "Re: KRfb"
    date: 2006-09-19
    body: "you have to log into your router and foreward a port to your internal ip (ie 192.168.15.1) \n\nvnc uses 5900 i think as the default http  port so when your friends want to log in, they type in your EXTERNAL ip (64.xxx.xx.xx... or whatever your isp gave you) and add a :5900 at the end, and your router will re-direct that to your machine and the vnc will ask for auth.\n\n... it all depends on your setup on the network at home..."
    author: "Jeffrey Jones"
  - subject: "What's wrong with the KDE apps page???"
    date: 2002-10-07
    body: "OK, I heard something about an hardware upgrade, but does that take weeks? And the following message (you get it when trying to connect to the server) is more emberassing than informative...\n------\nServer Down Until Sunday\nThe following servers are currently down for a major hardware upgrade:\n    * www.kde.com\n    * apps.kde.com\n    * lists.kde.com\n    * promo.kde.org\nWe expect service to be restored at 12:01 am, Sunday, August 19, 2001.\nWe apologize for any inconvenience. \n-----"
    author: "Ralexx"
  - subject: "Re: What's wrong with the KDE apps page???"
    date: 2002-10-08
    body: "http://lists.kde.org/?l=kde-www&m=103402086301833&w=2"
    author: "Anonymous"
  - subject: "Problem with KDE-NETWORK on Solaris"
    date: 2002-10-09
    body: "Well I've been fighting the build process on my Solaris 9 Desktop for about a week \nnow and it seems that I'm at a stopping point. I've managed to build all of the packages except for KDE-NETWORK. 1st problem with it is that it doesn't untar all of the way. This is where it stops.\n\nx kdenetwork-3.0.8/libkdenetwork/tests/data/codec_quoted-printable/wrap, 2521 bytes, 5 tape blocks\ntar: directory checksum error\n\n\nSo next I try untarring it on a Linux machine then ftping it over to my Solaris box for the build. After I do that I run ./configure and that goes to completion. But when I run make, I get a bunch of Makefile errors like this:\n\nmake[2]: Leaving directory `/export/home/tsweets/kde/beta2/kdenetwork-3.0.8/doc'\nMaking all in kdict\nmake[2]: Entering directory `/export/home/tsweets/kde/beta2/kdenetwork-3.0.8/kdict'\nMakefile:310: *** missing separator.  Stop.\nmake[2]: Leaving directory `/export/home/tsweets/kde/beta2/kdenetwork-3.0.8/kdict'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/export/home/tsweets/kde/beta2/kdenetwork-3.0.8'\nmake: *** [all] Error 2\n\n\n\nFirst thing, who is packaging the source up? Can the Makefile problem be related? Remember I have already downloaded and built the entire KDE 3.1 Beta2 source except for KDE-ADDINS and KDE-BINDINGS.\n\nThanks\nTony\n\nBTW: Line 310 in the Makefile looks like -\n@AMDEP_TRUE@#DEP_FILES =  $(DEPDIR)/dcopinterface_skel.P  $(DEPDIR)/kdict.all_cp\np.P  $(DEPDIR)/actions.Po $(DEPDIR)/application.Po \\\n        @AMDEP_TRUE@    $(DEPDIR)/dict.Po $(DEPDIR)/main.Po \\\n        @AMDEP_TRUE@    $(DEPDIR)/matchview.Po $(DEPDIR)/options.Po \\\n        @AMDEP_TRUE@    $(DEPDIR)/queryview.Po $(DEPDIR)/sets.Po \\\n        @AMDEP_TRUE@    $(DEPDIR)/toplevel.Po\n\n\n"
    author: "Tony Sweets"
  - subject: "Debian woody"
    date: 2002-10-09
    body: "*Debian: \n   *Debian stable (woody): Intel i386 \n\nWhen it will come back again?"
    author: "guest"
---
Yesterday the KDE Project
<a href="http://www.kde.org/announcements/announce-3.1beta2.html">announced</a>
the release of KDE 3.1beta2, the third (and final) development release of the
<a href="http://developer.kde.org/development-versions/kde-3.1-features.html">KDE
3.1 branch</a>.   On top of the large number of improvements over KDE 3.0 which have already been
<a href="http://www.kde.org/announcements/announce-3.1alpha1.html#changes">announced</a>, this release offers
<a href="http://www.kde.org/announcements/announce-3.1beta2.html#changes">a
number</a> of significant improvements, such as a new Exchange 2000<sup>&reg;</sup>
plugin for KOrganizer and a KVim plugin for KDevelop
(<a href="http://www.freehackers.org/kvim/shots/kdevelop.png">screenshot</a>).
In addition, release coordinator Dirk Mueller notes that
<em>over 1,000 bugreports on <a href="http://bugs.kde.org/">bugs.kde.org</a>
have been fixed in the last 4 weeks</em>.
Please run this release through its paces so that KDE 3.1 will be the best
we can make it!  Thanks to all for the hard work in getting this release out.



<!--break-->
