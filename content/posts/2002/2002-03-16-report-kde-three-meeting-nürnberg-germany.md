---
title: "Report: KDE Three Meeting (N\u00fcrnberg, Germany)"
date:    2002-03-16
authors:
  - "Dre"
slug:    report-kde-three-meeting-nürnberg-germany
comments:
  - subject: "KDE Three?"
    date: 2002-03-16
    body: "Wasn't the meeting in Trysil, Norway, before the release of KDE 2.0 called KDE Three?\nShouldn't this be called KDE Four then?"
    author: "Meni Livne"
  - subject: "Re: KDE Three?"
    date: 2002-03-16
    body: "That was called KDE Three Beta :)"
    author: "Simon Hausmann"
  - subject: "Compiling code at KDE Three"
    date: 2002-03-16
    body: "I'm sure I remember reading on one of the *-devel lists that there was a system in place at KDE Three which considerably sped up the time it took to recompile the KDE code.\n<br><br>\nWas this system <a href=\"http://freshmeat.net/projects/doozer/\">Doozer</a>?<br>\nIf not, what was used?<br>\nWhat kind of hardware was it running on?<br>\nAnd about how long did it take to compile a base system from scratch (e.g. qt-copy + arts + kdelibs + kdebase)?<br>\n<br><br>\nGo ahead: make me jealous!"
    author: "davec"
  - subject: "Re: Compiling code at KDE Three"
    date: 2002-03-18
    body: "No, it was a tool developed by TrollTech, teambuilder, which is probably going to be a closed-source commercial tool (they've got to make a living)...\n\nI'm still hoping KDE developers could use a free version of that tool, e.g. with machine-related license numbers to prevent redistribution (like Insure did). It's only useful with a bunch of machines connected together, obviously.\n"
    author: "David Faure"
  - subject: "Are KDE hackers KDevelop users?"
    date: 2002-03-17
    body: "How many of the programmers there were using KDevelop?\nAnd how many used non-KDE programs for hacking?\nI'm just curious... ;-)\nCiao,F@lk"
    author: "F@lk Brettschneider"
  - subject: "Re: Are KDE hackers KDevelop users?"
    date: 2002-03-17
    body: "Mostly vi and xemacs, IIRC :} Oh and I remember some kate and nedit users."
    author: "Carsten Pfeiffer"
  - subject: "why?"
    date: 2002-11-23
    body: "do I suck"
    author: "wannabe"
  - subject: "Number of commits"
    date: 2002-03-17
    body: "It's funny how two people can take the same statistic - the large large number of commits during the meeting - and see it as evidence of something different."
    author: "Neil Stevens"
  - subject: "Re: Number of commits"
    date: 2002-03-17
    body: "Not really. Some people are optimists, some people are pessimists, some people are realists and others just like to stir shit up. Given that variety among people, it is not suprising at all that different individuals will interpret the results of the meeting in different ways. Fortunately we don't need to rely on forcasts and interpretations, since we will know for certain how useful everything really was when KDE 3.0 is released.\n\nThe danger of prognostication is that the future may prove you wrong."
    author: "Aaron J. Seigo"
  - subject: "Re: Number of commits"
    date: 2002-03-17
    body: "Danger?  My *goal* was that by writing what I wrote, that things would change (oh, like the release slipping a great deal as it has), and that the changes would make me wrong."
    author: "Neil Stevens"
  - subject: "Users on the meeting?"
    date: 2002-03-17
    body: "Hi,\n\nAFAIR there where just developer on the meeting. Maybe usability (which is imhbo the reason of making a gui) would from a bunch of \"normal\" users. . .\n\n-j"
    author: "Jan"
  - subject: "The nameless"
    date: 2002-03-18
    body: "Who are those guys kneeling on the floor? The users?"
    author: "reihal"
  - subject: "Re: The nameless"
    date: 2002-03-18
    body: "There's only one that I can recognize on the floor : David Faure, laughing in the middle... "
    author: "thibs"
  - subject: "Re: The nameless"
    date: 2002-03-18
    body: "<A href=\"http://devel-home.kde.org/~danimo/kdemeeting/group/group.jpg\">group.jpg</a> is not broken/has all names."
    author: "Anonymous"
  - subject: "Re: The nameless"
    date: 2002-03-18
    body: "No, it seems to be missing a few names of those in the upper row."
    author: "Carbon"
  - subject: "Question"
    date: 2002-03-19
    body: "(I have a sneaking suspicion I've posted this before... I hope not) Does the latest KHTML CVS code render vBulletins correctly? KDE2.0 used to but it's been broken ever since KDE2.1.\n\nAnd, sorry for being lazy but my computer sucks and I dont want to tie up half a Gb of disk space and eight hours waiting for the CVS to compile. I spend a lot of time on vBulletins... a fix would be much appreciated. (try reading a thread on the vbulletin.com forum if you want an example; compare with what it looks like in Mozilla)"
    author: "Somebody"
  - subject: "Re: Question"
    date: 2002-03-19
    body: "You don't have to checkout CVS or program.  What you do have to do is post a proper bug report via the standard bugs.kde.org interface.  And, even better, you should filter out the vBulletins site down to the tags/script that isn't working.  Then and only then can these people put a finger on something.\n\nI know it sounds weak ... but remember how much $$$ you paid for Konq.  We need not only strong developers that we do have, but also more proactive users willing to narrow bugs down to the \"line of code\" (whether that's C++,Makefile,admin scripts,HTML,Javascript).  The latter two happen to not need 1G of CVS files.\n\nOut..."
    author: "J"
  - subject: "Re: Question"
    date: 2002-03-19
    body: "Well, if I have to compile Qt3 then KDE3 base and libs from source then this is how much it takes up; I'd use the debian Qt3 package but word has it that's broken. I can't really submit a bug report if I can't even check the error is still there, best I can do is to perhaps trim out the offending HTML code to an example and then offer a comparison with say the Mozilla rendering (unless it's already been fixed...)"
    author: "Somebody"
---
<a href="http://www.kde.org/people/cristian.html">Cristian Tibirna</a> has <a href="http://www.kde.org/announcements/kde-three-report.html">written up</a> a summary of the KDE Three Meeting, held in N&uuml;rnberg, Germany from Febuary 25 - March 4, 2002.  Through the commitment of a group of active KDE developers (<a href="http://devel-home.kde.org/~danimo/kdemeeting/group/full-size-group.jpg">group picture</a>, <a href="http://devel-home.kde.org/~danimo/kdemeeting/">thumbnails 1</a>, <a href="http://www.suse.de/~cs/KDE3-Meeting/images.html">thumbnails 2</a>), a large number of bugs were fixed, promising to make 3.0 KDE's best-tested and most stable .0 release yet.  Thanks to all the developers for their great work, and to Cristian for the summary.

<!--break-->
