---
title: "People of KDE: Falk Brettschneider"
date:    2002-03-04
authors:
  - "wbastian"
slug:    people-kde-falk-brettschneider
comments:
  - subject: "f@lk doesn't work ;( with IRC"
    date: 2002-03-04
    body: "Hmm, /me wonders what fATlk would really mean ;-)\n\n*SCNR*\n\n"
    author: "zapalotta"
  - subject: "Re: f@lk doesn't work ;( with IRC"
    date: 2002-03-05
    body: "It means: f@lk @ lk"
    author: "F@lk"
---
Come sit along our campfire, kids! In this week's installment of
<a href="http://www.kde.org/people/people.html">The People Behind KDE</a>,
we finally meet someone who not only isn't
afraid to sing, but who even brought his own guitar with him!
So come join us and learn a thing or two about <a href="http://www.kde.org/people/falk.html">Falk Brettschneider</a>, 
famous for 
<a href="http://www.geocities.com/gigafalk/qextmdi.htm">QextMDI</a>
and his contributions to 
<a href="http://www.kdevelop.org/">KDevelop</a>.


<!--break-->
