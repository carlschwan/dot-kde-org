---
title: "The Register:  Fabulous Fonts in KGX"
date:    2002-10-26
authors:
  - "Dre"
slug:    register-fabulous-fonts-kgx
comments:
  - subject: "A little whining"
    date: 2002-10-26
    body: "Ah.. what's so special about the last four screenshots? Is this about disabling AA for small fonts? Other than that, I can't think of anything special with those screenshots. I think the fonts are ugly. "
    author: "ac"
  - subject: "Re: A little whining"
    date: 2002-10-26
    body: "Yea, sound like a joke :-)\nThe screenshots _are_ ugly!\nSorry, but I stay with my own setup. \n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: A little whining"
    date: 2002-10-26
    body: "The difference between the screenshots in the article and your screenshot is that the fonts on the former are sharp, and the fonts on your screen shot look like your monitor is broken... :)\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: A little whining"
    date: 2002-10-26
    body: "Will watch it next week on a CRT-Monitor ;-)\n\nBut I think this sharp but crappy look makes me happy. *Me* critizises the \"colored\" look of AA-fonts: Letters of a  e.g. black text have no homogen tone but one letter looks more gray another one looks more black. But its getting better and better... \n\nBye\n\n  Thorsten \n "
    author: "Thorsten Schnebeck"
  - subject: "Re: A little whining"
    date: 2002-10-26
    body: "(grrr)  ... don't makes me happy...\n\nof course ;-)"
    author: "Thorsten Schnebeck"
  - subject: "Re: A little whining"
    date: 2002-10-28
    body: ">Will watch it next week on a CRT-Monitor ;-)\n\nHuuaa! Next time I will make a photo and not a screenshoot! - forget the attachment, it does only reflect the reality when you have an High-DPI-TFT monitor.\n\nBye\n\n  Thorsten\n"
    author: "Thorsten Schnebeck"
  - subject: "Poll about your favourite browser"
    date: 2002-10-26
    body: "The current poll at http://www.stud.uni-potsdam.de/~ksommer/linux4user/ is about your favourite browser, if you are interested."
    author: "Erik"
  - subject: "Er, huh?"
    date: 2002-10-26
    body: "Just a glace at his two Openoffice screenshots showed me this guy is seriously whacked. The second screenshot, which he claimed is \"more refined\" is clearly much more jagged than the first. A simple look with Xmag sees the only difference between the two is that the second has anti-aliasing turned off.  Same with the\"results\" screenshots at the end.... they look like crap compared to my fonts in KDE, and I didn nothing speccial. Just apt-get install msttcorefonts in Debian. There is no anti-aliasing going on at all in these screenshots, they look horrible.... What is the point of this article exactly?"
    author: "Jason Keirstead"
  - subject: "Re: Er, huh?"
    date: 2002-10-26
    body: "Seems like the guy had AA enabled from the beginning, and thought that he didn't because he's one of those who don't like AA fonts. Then he fscked around with freetype, and managed to cripple it enough to make it stop working completely. When he saw the result he thought \"wow, nice AA fonts!!!\" ;)\n\nSeriously though, the window titlebar font in the screenshots looks good.\n\n"
    author: "ac"
  - subject: "Re: Er, huh?"
    date: 2002-10-26
    body: "No if u look it the title bar fonts and menus that he is talking about. Yes he has turned aa of. But thanks to him my open office looks as good as star office. And I didn't know that star office and open office don't use the system fonts. "
    author: "Mark Hillary"
  - subject: "Don't they screen these guys"
    date: 2002-10-26
    body: "Ok, i know this has been said but this guy is not very bright. The first OO.org screen shot had aa turned on and the rest that were supposed to be better looked like crap without aa turned on. Seriously, how can you think it looks better? I would at least like to see someone do an article to make it look better. i know sometimes i've had problems with oo.org fonts looking bad but i know kde looks good for me."
    author: "AJ"
  - subject: "Article isn't that good.."
    date: 2002-10-26
    body: "This article isn't that good. There are much better information aviliable on the web (such as the Font Deunglification HOWTO) -- I can't understand how this got on KDE Dot News as well as SashDot. It's that bad, not to menton higly biased toward KDE. It would be nice if it included information on making it work with non-KDE programs aswell (especially since many people use GNOME/GTK-based programs and non-KDE browsers like Mozilla).\n\nThis is just your typial PC World-style article that just goes on and on about things the writer dosn't even know much about in the first place.\n\nThat's my 2cents, anyway...\n\n- James"
    author: "James Pole"
  - subject: "Re: Article isn't that good.."
    date: 2002-10-26
    body: "> It would be nice if it included information on making it work with non-KDE programs aswell\n> (especially since many people use GNOME/GTK-based programs and non-KDE browsers like\n> Mozilla).\n\nEven Mozilla's default is anything else than GTK. You need to compile support for GTK in but you stil deal with the XUL Widgetset. With GNOME browser you hopefully don't want to say GALEON now since GALEON is anything else than a full countable Webbrowser."
    author: "AC"
  - subject: "Re: Article isn't that good.."
    date: 2002-10-27
    body: "AC said:\n\n\"With GNOME browser you hopefully don't want to say GALEON now since GALEON is anything else than a full countable Webbrowser.\"\n\nHuh?  I'm assuming you're trying to take a shot at Galeon, but maybe you should run your posts through a Kspell and Krammer KChecker next time, lest someone get the wrong idea..."
    author: "JPerrin"
  - subject: "Re: Article isn't that good.."
    date: 2002-10-27
    body: "I doubt that English was the their first language. You were essentially taking a blow the belt shot at them as well. The \"krammer\" part didn't help your cause much either.\n\nNote: I don't agree with original poster's shot at Galeon. Galeon is a complete _web browser_. More complete than perhaps anything else in Linux, mostly because it's a web browser only. Phoenix is also a webbrowser only, but is not as featurefilled as Galeon is (yet). \n\nIt's true that Galeon doesn't have it's own rendering engine, but Konqueror (technically) doesn't either. So using original poster's definition of webbrowser, Konqueror is much less of a complete webbrowser than Mozilla (which is tied to Gecko), is. Konqueror is not tied to either gecko or khtml, and is not essentially even a webbrowser, but accepts the paradigm of being one. "
    author: "Jay Roberts"
  - subject: "Re: Article isn't that good.."
    date: 2002-10-26
    body: "> It's that bad, not to menton higly biased toward KDE. \n\nWhat is your problem?  The first two screenshots show OpenOffice and the Gimp . . .\n\n> It would be nice if it included information on making it work with\n> non-KDE programs aswell (especially since many people use\n> GNOME/GTK-based programs and non-KDE browsers like Mozilla).\n\nand the last two Mozilla.  I mod you down to -1 for troll :-/."
    author: "Ace"
  - subject: "This is what he means"
    date: 2002-10-26
    body: "The point of the author is, in my view:\n\n1. Antialiasing is great, but disable it for point sizes between, say, 8 and 14\n2. But now we have a problem. The fonts between 8 and 14 points (which have no antialiasing now) look ugly. This is because the bytecode interpreter is not turned on by default. The bytecode interpreter greatly enhances the look of fonts when they 1) are not antialiased and 2) when they actually contain any bytecode to  be interpreted!\n3. Solution to 2): turn on the bytecode interpreter.\n\nFor the difference between bytecode interpreter on/off, have a look at my page: http://elektron.its.tudelft.nl/~rbos36/mdkfreetype2.html\n\nSome fonts contain bytecode (like arial, verdana, tahoma etc.) but some do not (for example, Luxi). So it will work only for a subset of the TrueType fonts you have installed.\nWhen you use antialiasing for all point sizes, there is no point in using the freetype2 bytecode interpreter and you can leave your freetype2 as it is.\n\nA point which I don't understand is that he tells his readers to turn on \"Use sub-pixel hinting\". Does someone know what this means? If it means the same as subpixel antialiasing, then only use this for LCD screens!\n\nLast words: if you want good font quality, use RedHat 8.0! See also the attachment."
    author: "Ronald"
  - subject: "Re: This is what he means"
    date: 2002-10-26
    body: "I disagree. All my fonts are 10pt and they look much beter using AA than without, IMO. And these are the MSTT fonts as well.\n\nPersonally, I think all these people who think AA fonts look bad at thes emoderate range sizes (10,12pt)  need to get a better monitor with a lower dot pitch or something. They look absolutely great from where I sit."
    author: "Jason Keirstead"
  - subject: "Re: This is what he means"
    date: 2002-10-26
    body: "Or alternatively you need to get a monitor that's less blury :-)\n"
    author: "Sad Eagle"
  - subject: "Re: MSTT Fonts"
    date: 2003-08-30
    body: "MSTT Fonts"
    author: "eeosm"
  - subject: "Re: MSTT Fonts"
    date: 2004-04-10
    body: "MSTT Fonts"
    author: "manoj "
  - subject: "Re: MSTT Fonts"
    date: 2005-11-01
    body: "I am needing Source MSTT and would like to know information of where it could lower the same one.\n\nAtt.\n\nM\u00e1rcio"
    author: "M\u00c1RCIO"
  - subject: "KGX?"
    date: 2002-10-26
    body: "A search in that article doesn't return any occurence for the acronym KGX. What is this?"
    author: "Inorog"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "_K_DE _G_NOME _X_\n:)"
    author: "Peter Clark"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "KDE + Gnu + linuX\n"
    author: "Tim Jansen"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "KGX is a complete misnomer here because you can do the same things on the half-dozen other non-Linux OS's that run recent Xfree's and KDE.\n\nExamples are FreeBSD and Solaris."
    author: "shm"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "I'm not taking sides on this (if it's a worthwhile invention, people will use it, otherwise they won't), but -- that's precisely the point, to come up with terminology for a KDE/Linux system. What is erroneous is when people refer to KDE as Linux software.\n\nThe closest analogy I can think of is LAMP (Linux/Apache/MySQL/PHP). Except maybe for Apache, all of those could be replaced with something else to get the same function."
    author: "Otter"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "Kde Gnu/Linux?"
    author: "zelegans"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "It's Andreas Pour's attempt to push his KDE League propaganda, is all it is.  That's why my earlier comments about it (and agreeing reply by an Alain) was deleted."
    author: "Neil Stevens"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "\"It's Andreas Pour's attempt to push his KDE League propaganda, is all it is\"\n\nWith friends like you, who needs enemies?"
    author: "Janne"
  - subject: "Re: KGX?"
    date: 2002-10-26
    body: "Funny, I didn't know I ever claimed to be a Friend of Andreas Pour or the KDE League.\n\nI have high hopes for KDE e.V., but that's another story."
    author: "Neil Stevens"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "I meant KDE in general. It seems to me that all you do all day long is bitch and moan about everything."
    author: "Janne"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "Well, what I've done in KDE is relative.\n\nBut I'm posting with my name, so you can look at what I do.  As far as we know, you're just some anonymous voice attempting to smear me."
    author: "Neil Stevens"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "Anonymous? My name is visible in the posts I make. And the fact that you have contributed code to KDE doesn't change the fact that you seem to be a whiner."
    author: "Janne"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "It might be that there are anonymous voices accusing you for moaning around... but at least I am not anonymous and I must admit that sometimes I can not help but feel kind of frustrated reading your comments:\n\nTo me you sometimes (not allways!) seem to be more interested in sounding cool than in writing something constructive.\n\nNOTE:  Nobody here is interested in knowing about you loving or not liking Andreas and it would be nice to concentrate on arguments instead of making silly comments about him.\n\nThanks for considering taking my opinion into account.  ;-)"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "My comment *was* focused on what he's doing.  What I think of him personally is irrelevant to anything here.\n\nThis site is on the KDE domain, the KDE homepage, and at times claims to speak for KDE.  So I think, as an active KDE contributor, I have a right to question the use of it, just as anyone else.\n\nWhy complain on the site?  Well, the admins of this site keep their mailing list secret, so there is no alternative."
    author: "Neil Stevens"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "You can use kde-promo@kde.org for open discussion concerning the dot.  I already told you this."
    author: "Navindra Umanee"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "I agree, two posts (at least...) were deleted, that is to say censored.\nYes, it seems that this site helps some KDE league propaganda for imposing this KGX acronym, where the K of KDE is associated with a G and a X, and it is not neutral... \nIt is sad...\nThank you Neil for your voyance and for going in an other direction."
    author: "Alain"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "> I agree, two posts (at least...) were deleted, that is to say censored.\n\nOne was (by me), it just happens that children don't get reparented.\n\nAnd we often remove troll posts, due to the lack of a moderation\nsystem."
    author: "Dre"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "I wish I could meet with a few of the anti-KGX people, or at least talk on the phone with them.  I don't think I'll ever understand the reasons you dislike the term so much.  I think it's handy - it refers to KDE plus GNU/Linux, a completely Free software desktop (often also free), as opposed to KDE in general, which can be used on everything from OSX to AIX... pretty much anything with X.  ;)\n\nSeriously - KDE is a Desktop Envionment that runs on a variety of platforms, some of which are quite expensive and proprietary.  KGX refers to a Free and or Open Source software system.  I'd say BSD variants (other than BSDi) also count, although the more common configuration lining up on corporate desktops appears to be Linux.\n\nKDE as an Environment (i.e., kio_slaves, KParts, DCOP, etc) can (although there are currently few examples) run on headless embedded systems or palmtops (not quite a desktop).  I already have Apache make DCOP calls (via dcop through PHP) to arrange Noatun playlists on my server which has nothing plugged into its video card.\n\nSo KDE as an development environment, or KDE as a desktop on proprietary systems (like OSX) is quite different than KDE+GNU/Linux, which is what many people immediately think of when they think KDE.  KGX is a good term to make people realize that KDE is a *bigger* thing than just a \"Linux desktop\".\n\nSo I'm not sure why some people seem to think the term KGX diminishes KDE...\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "\"I'll ever understand the reasons you dislike the term so much.\"\n\nBecause the attempt is to use \"KGX\" as a substitute for \"KDE\". The above article is applicable to KDE on operating systems other than LiGnuX, Gnulix, GNU/Linux, or Linux.\n\nWhen non-Linux people see the acronym \"KGX\", they will think, \"oh, this information is not for me\", which is wrong."
    author: "David Johnson"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "KGX stands for KDE Gnu/linux and uniX."
    author: "ac"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "I don't get it. The KGX phrase is only published here and on a draft page of kdeleague.org explicitely not for circulation. It doesn't make sense to push a phrase or brand when you've got nothing to show for it. Search on Google for KGX, you'll find some of the discussions here, the draft, and nothing more. So I doubt this is an attempt to push KDE League propaganda.\n\nThus I still don't know why the term is used. In fact, it alienates the Dot from KDE. KDE (as a project, KDE Inc, the developers) would never show a specific preference for GNU/Linux. Making it seem like KDE is primarily designed for Linux is an insult to our non-Linux developers and users.\n\nI don't expect this is intentional propaganda from Andreas, because it wouldn't make sense if it were. I guess he just likes the concept personally and doesn't realize how silly it is to mention KGX as long as it is an empty concept.\n"
    author: "Rob Kaper"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "> It doesn't make sense to push a phrase or\n> brand when you've got nothing to show for it. \n\nRob, I'm not pushing it, I'm simply using it.  When it's been circulating\nfor a while we can get a better idea how people feel about it.  The jury is\nstill out.  Sorry I can't launch a million dollar advertising campain\nwith it ;-).\n\n> In fact, it alienates the Dot from KDE. KDE (as a project, KDE Inc, the\n> developers) would never show a specific preference for GNU/Linux. \n\nThe article was entitled \"Fabulous fonts in *Linux*\".  What alienates people\nis this type of negativity.  If you have better ideas, work with them,\nbut please stop knocking down others who are trying to improve things.\nSure it's easy to attack people who are trying something new, but is it\nhelpful?\n\n> I don't expect this is intentional propaganda from Andreas . . .\n> doesn't realize how silly it is to mention KGX as long as it is an\n> empty concept.\n\nPropaganda:  how can use of KGX be propaganda?  Geez.\n\n\"Empty concept\"?  It's not empty, you know exactly what it means.\nOr is \"monopd\", etc. an \"empty concept\" too?"
    author: "Dre"
  - subject: "Re: KGX?"
    date: 2002-10-27
    body: "People who use monopd call it monopd. Nobody except you actually uses the phrase KGX so as long as long as your KGX writeup is still an unpublished draft I see no point in mentioning it here.\n\nRead my previous reply again, and note I *don't* accuse you of pushing something or making propaganda. Finish the draft and publish it and at least the term is properly introduced and can be linked and so on. The other comments show that apart from some KDE insiders nobody knows what KGX is.\n\nMaybe even more important: the register indeed calls the article \"Fabulous Fonts in Linux\". Renaming that to KGX in the Dot header is bad, especially as the article talks mostly about OpenOffice and FreeType and only mentions KDE by the coincidence the author is familiar with it, by no means is it a specific KDE guide.\n\nI probably would've objected against posting in the first place, as the article isn't all that special, especially not for KDE. But it was posted rather soon after the submission - as if nice fonts are groundbreaking news that can't wait a day.\n\nPS: I *am* working with better ideas, on http://kdenews.unixcode.org/\nPPS: Don't mistake critism on an idea or situation as a personal attack.\n"
    author: "Rob Kaper"
  - subject: "article approval time"
    date: 2002-10-28
    body: "While this article isn't the best, it isn't the worst either.  It's not like a whole lot of harm has been done by publishing an article on fonts.\n \nI'm *totally* opposed to introducing these additional delays in approving dot articles. We need *more* articles, not less.  If you want to help, submit more articles or improve submitted ones in favour of slowing things down with bureaucracy.\n"
    author: "Navindra Umanee"
  - subject: "Re: article approval time"
    date: 2002-10-28
    body: "Then start posting every submitted article to kde-promo@kde.org\n\nYou say that list is for this site.  Prove it."
    author: "Neil Stevens"
  - subject: "Re: article approval time"
    date: 2002-10-28
    body: "What the hell for?  Even the editors list doesn't get spammed like that.  Please come back with some real contributions.\n"
    author: "Navindra Umanee"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "What comments were deleted?"
    author: "Navindra Umanee"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "I remember making a comment here about Dre trying to advertise KDE and Stevens scaring them away. How that one, which did include a winking emonticon, was worth getting censored is beyond me. *shrug*"
    author: "Datschge"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "I never even got to see yours... I saw Alain's, but that's it."
    author: "Neil Stevens"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "Then there's at least hope that you are more humorous than the one who deleted it. ;P\n\nSeriously though, if anyone really thinks it's necessary to hide away certain posts from other people then better include a rating system into dot.kde and let the public decide what to hide (I'd prefer a positive rating system, ie. post with the most points/recomendations are shown at the top and those with the least at the bottom).\n\nSame could be done with the articles themselves so Stevens can rate all other articles to get the \"Fonts in KGX\" article out of sight asap. ;) (that's joking, ok!?)"
    author: "Datschge"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "I would love a system like in OSnews where all the \"moderated\" posts are not deleted but the editor, but instead moved to a new subpage. You can click on \"View Moderated Posts\". "
    author: "shm"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "http://dot.kde.org/1035582174/1035612842/\n\nThat and all its children disappeared.  I suppose it could be worse, it could have been cleared and replaced with some namecalling."
    author: "Neil Stevens"
  - subject: "preserving children"
    date: 2002-10-28
    body: "What namecalling?  All you do is comment out the troll with the standard <!-- and -->, put a note about it and leave the children.\n"
    author: "Navindra Umanee"
  - subject: "Re: preserving children"
    date: 2002-10-28
    body: "Your \"notes\" usually involve namecalling of the contributor.\n\nOf course, it was Andreas who deleted my comment.  Is it only you who leaves notes?"
    author: "Neil Stevens"
  - subject: "Re: preserving children"
    date: 2002-10-28
    body: "Contributor, huh?  Give me a freaking break.  Please show me what you are talking about before you say that. \n\nAnd besides, I don't have infinite patience to deal with abusers, their happiness is not my priority, and if they pissed me off enough and wasted my time enough for me to insult them, so what?\n\nWhere were you when I was trying to get cooperation on the KDE Dot News community FAQ, anyway?  http://dot.kde.org/1024894942/\n\nI'm off."
    author: "Navindra Umanee"
  - subject: "Re: KGX?"
    date: 2002-10-28
    body: "I agree completely, especially for the reasons Rob said. And I thought something was quite wrong as soon as I saw the article, and did not need to read any other comments to give me this opinion.\n\nAs another slightly-related issue, the article really could be written a bit better - why did it have to be posted as news?"
    author: "Jason Katz-Brown"
  - subject: "Non-TT"
    date: 2002-10-27
    body: "But is it possible to get AA for non-TT fonts ? Because i never saw good-looking screenshots with TrueType fonts (whatever OS it ran on), I dont get why people absolutely wants MSTimesNewRoman and MSArial when we have AdobeTimes and AdobeHelvetica which looks really better (but only at fixed size).\nWhy always focus on those ugly TT fonts ? Is it the only way to get AA ? And how does LateX does, since it looks great in xdvi ?"
    author: "Benji2"
  - subject: "Re: Non-TT"
    date: 2002-10-27
    body: "YES, it is possible to get Anti-Aliasing for real fonts too - not only for the common surrogates called 'Truetype' - actually the only thing that is 'true' about that is the truth that Microsoft did not want to pay for real fonts' licenses.  :-D\n\nplease see here: http://www.enfusion-group.com/~adrian/docs/Anti-aliased_X/"
    author: "Karl-Heinz Zimmer"
  - subject: "Re: Non-TT"
    date: 2002-10-28
    body: "You can enable AA for vector fonts, that is TrueType, or Type1. They are defined as a set of curves and are mapped to the screen every time they're drawn. This way the AA algorithm can decide what to blur where. With bitmap fonts, all you get is a set of pixels and you can do no AA there.\n \nAnd LaTeX uses TeX as its typesetting engine. TeX uses its own font system (also outline-based), designed, if I'm not wrong, by Donald Knuth himself, and of incredible quality. I think there is only one really complete font available for TeX, though. "
    author: "someone"
  - subject: "Re: Non-TT"
    date: 2002-10-28
    body: "To learn more about how to produce or obtain 1st quality fonts for TeX please look here:\n\nhttp://www.tex.ac.uk/cgi-bin/texfaq2html?label=useMF\nhttp://www.tex.ac.uk/cgi-bin/texfaq2html?label=keepfonts\nhttp://www.tex.ac.uk/cgi-bin/texfaq2html?label=getbitmap\n\nand here:\n\nhttp://www.tex.ac.uk/cgi-bin/texfaq2html?label=usepsfont\n( + following pages)"
    author: "Karl-Heinz Zimmer"
  - subject: "Withdraw"
    date: 2002-10-27
    body: "Is it possible to withdraw such a licence agreement of MS. And would they be obliged to pay for the withdrawel to the users.... Anyone keen on this private law case?"
    author: "Hakenuk"
  - subject: "xft2"
    date: 2002-10-28
    body: "Will the upcoming Qt (3.1) use Xft2?\n\n"
    author: "Per Wigren"
  - subject: "Re: xft2"
    date: 2002-10-28
    body: "Yes, it's already included in the current Qt 3.1 Beta. Look at trolltech's homepage for reference."
    author: "Datschge"
---
<a href="http://www.theregus.com/">The Register</a> is running a
<a href="http://www.theregus.com/content/4/26770.html">short tutorial</a> on
everyone's favorite obsession:  getting fonts to look their best.
Included are instructions for re-compiling the <a href="http://freetype.sourceforge.net/index2.html">FreeType</a> engine
to get an optimal look, something distributions often fail to do due to
potential licensing problems.
Another interesting thing I learned from the article:  although MS has
removed the web fonts from their servers, apparently the original
license permitted unlimited non-commercial redistribution in unaltered
form, and hence a <a href="http://sourceforge.net/">SourceForge</a> <a href="http://corefonts.sourceforge.net/">project</a> has sprung up
which provides the fonts and instructions for installing them.  All-in-all,
a worthwhile read for font aficionados.

<!--break-->
