---
title: "Boson 0.6: A New Milestone"
date:    2002-06-17
authors:
  - "smacdonald"
slug:    boson-06-new-milestone
comments:
  - subject: "I tried it with my roomate.."
    date: 2002-06-17
    body: "it plays very nice. As with my opensource'd games, however, it could use better graphics imho.\n\nAlso, how about a nice little isometric view ;)"
    author: "flea"
  - subject: "RedHat packages"
    date: 2002-06-17
    body: "And for all of you who want \"game out of the box\", \nthere are unofficial RPM packages prepared for RedHat 7.3\nLook at the apps.kde.com announcement:\nhttp://apps.kde.com/na/2/info/vid/7448\n"
    author: "krzyko"
  - subject: "Re: RedHat packages"
    date: 2002-06-17
    body: "... Or simply:\nhttp://sourceforge.net/project/showfiles.php?group_id=15087&release_id=93970\n"
    author: "Andi"
---
After a bit of a hiatus, <a href="http://boson.sourceforge.net/">Boson</a> (<a href="http://boson.sourceforge.net/screenshots.html">screenshots</a>), the Real Time Stragegy <a href="http://www.kde.org/kdegames/">game</a> for KDE, is back in swing. Since development restarted in late 2001, the game has been completely rewritten and ported to <a href="http://www.heni-online.de/libkdegames/">libkdegames</a>. As a result of the recent development, Boson 0.6 has been released. The full announcement is below.

<!--break-->
<p><strong>Release of <a href="http://boson.sourceforge.net">Boson</a> 0.6, a Real Time Strategy (RTS) game for the <a href="http://www.kde.org">K Desktop Environment</a>.</strong></p>

<p>
After a year long hiatus (the last release being in October, 2000), Boson was revived by a new set of developers. It is now completely
ported to KGame in libkdegames, which provides player and network management. Boson 0.6 is a complete rewrite from its predecessor,
with not a single line of code remaining untouched.
</p>

<p>
Boson still requires a minimum of two players, as there is not yet an artificial intelligence. The game is still under heavy development and
0.6 is merely a milestone rather than a fully playable game. We are in need of more developers, especially graphics artists. If you have any
spare time and the ability to code in Qt/C++ or know your way around a graphics program, please feel free to pop in and offer a hand.
</p>

<p>
Because of recent changes in the source code, the 0.6 release does not include the map editor (see <a href="http://sourceforge.net/mailarchive/forum.php?forum_id=6890">here</a> for an explanation). We have, however, been working very hard to ensure that this the only
limitation compared to the last release (0.5). Due to the massive overhaul necessary to bring Boson to this point, a changelog was not
possible.
</p>

<p>
  <strong>Major changes and additions since version 0.5:</strong>
  <ul>
    <li>auto-shoot: Defensive buildings automatically shoot at enemies when they are in range. Normal units do not do this yet.</li>
    <li>split views: you can split a display and play at several places on the map at once</li>
    <li>full network capability (please note that single player mode is quite boring without an AI)</li>
    <li>fog of war</li>
    <li>mineral/oil harvesting (experimental)</li>
    <li>fullscreen mode</li>
    <li>Map and scenario files are now XML based (was originally binary). All units can be configured using KConfig syntax</li>
    <li>music, sound and speech</li>
    <li>pathfinding (thanks to some excellent work by it's hacker!)</li>
    <li>Most units actually have "sense", e.g. if your Comsat station is destroyed your minimap will disappear.</li>
  </ul>
</p>

<p><strong>Dependencies</strong></p>

<p>
  To compile and install Boson, you will need (other than the usual compiler tools):
  <ul>
    <li>Qt 3 (see <a href="http://www.trolltech.com/">http://www.trolltech.com</a>)</li>
    <li>KDE 3 (see <A href="http://www.kde.org/">http://www.kde.org</a>)</li>
    <ul>
      <li>kdelibs</li>
      <li>kdegames (at least libkdegames)</li>
      <li>kdemultimedia for sound support (might not compile without it, as we didn't test)</li>
    </ul>
  </ul>
  Please note that you need the development files (e.g. headers) of all these packages. We do not (yet) provide binary packages.
</p>

<p><strong>Installing Boson</strong></p>

<p>
There are two methods of installing Boson (from tarballs). The easier way is to grab boson-all-0.6.tar.bz2. However, that file is very big, and
if you are on a slow connection, you may wish to just get the code and data packages. The required packages can be found <a
href="http://sourceforge.net/project/showfiles.php?group_id=15087&release_id=93970">here</a>.
</p>

<p>
Let me say that again: download either boson-all or both boson-code and boson-data. Do not waste your time downloading all three!
</p>

<p>
If you decided to grab the individual packages, you can optionally download the music files for Boson <a href="http://boson.eu.org/music/">here</a>.
</p>

<p> 
  <strong>The installation itself:</strong>
  <blockquote>
    <strong>Decompress the package(s):</strong><br />
    % tar xjvf &lt;package name&gt;<br />
    <strong>cd into each directory and:</strong><br />
    % ./configure<br />
    % make<br />
    <strong>As root:</strong><br />
    # make install
  </blockquote>
</p>

<p>
  <strong>Tarball checksums and sizes for the paranoid:</strong>
  <tt>
    <br />
    File name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Size&nbsp;&nbsp;&nbsp;
    MD5 Checksum
    <br />
    boson-all-0.6.tar.bz2&nbsp;&nbsp;&nbsp;27.6 MB 92e5b16f3bdc7d31238d23d298793e15
    <br />
    boson-code-0.6.tar.bz2&nbsp;&nbsp;501 kB&nbsp;&nbsp;3d7b8b3967c5ada20b0ae80c56517aad
    <br />
    boson-data-0.6.tar.bz2&nbsp;&nbsp;10.9 MB 0e470b3f089424da3dc1cb05a0230a6b
    <br />
    boson-music-0.6.tar.bz2 16.6 MB f777b7a232ef55eb23c0b547fc335873
  </tt>
</p>

<p>
The Boson Team
</p>
