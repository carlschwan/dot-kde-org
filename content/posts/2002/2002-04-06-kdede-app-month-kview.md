---
title: "KDE.de App of the Month: KView"
date:    2002-04-06
authors:
  - "Dre"
slug:    kdede-app-month-kview
comments:
  - subject: "kview!"
    date: 2002-04-06
    body: "KView is great!  It's one of those little apps that are frequently overlooked, like konsole, or kmail (to a lesser extent).  KDE is *nothing* without these fantastic apps.  (ok, so, kmail isn't one of those \"little\" apps--but it is certainly taken for granted..)"
    author: "Charles Samuels"
  - subject: "Re: kview!"
    date: 2002-04-06
    body: "You are absolutly right, and yesterday when I was installing KDE3 on my SuSE system(Can't wait for my 8.0 copy) I was just thinking that \"hey this Kmail client is really quit nice!\" I have always been seeking and wanting a more outlook like mail client but when you think about it Kmail really has become quite the killer Email client under Linux. What I mean is that it's stabel \"rock solid\" and you got all the functionallity an Email client should have so you are absolutly right the developers of Kmail definantly deserves more credit!! Thanks Kmail developers!!!!"
    author: "DiCkE"
  - subject: "Re: kview!"
    date: 2002-04-06
    body: "There are only some \"little\" things in KMail/Kaddessbook.\n\nKMail mix languages when you send a \"reply\" (date/time) and the date/time fields are broken when you use other then the standard formats.\n\nKaddressbook do not hold the wide and order of the user defined data fields.\nSecond the \"save as\" data field do not hold the user defined format (surname, name for example).\n\nBug reports are going.\n\nRegards,\n Dieter\n\nBTW Apart from that KMail is GREAT. I do _NOT_ want a unified mail/news client."
    author: "Dieter N\u00fctzel"
  - subject: "Thanks to Klaus"
    date: 2002-04-06
    body: "Besides the great \"App of the Month\" Series, Klaus also does most of the maintaining of www.kde.de.\nThank you, Klaus.\n\nDD"
    author: "zapalotta"
  - subject: "Does it come with KDE 3.0?"
    date: 2002-04-06
    body: "I mean people would be aware of it more if kview is bundled with KDE3.\nPersonally, I only know the software which came bundled with KDE. Is there\nany other QT/KDE projects which are good but not well known by majority of\nusers?"
    author: "Tom"
  - subject: "Re: Does it come with KDE 3.0?"
    date: 2002-04-06
    body: "Kview IS bundled with KDE3... "
    author: "Movitz"
  - subject: "Re: Does it come with KDE 3.0?"
    date: 2002-04-06
    body: "Thats whats appsy is for. It has a scoring system, so you can see which apps are worth looking at."
    author: "Psiren"
  - subject: "Re: Does it come with KDE 3.0?"
    date: 2002-04-07
    body: "Take a look at PIXIE, www.mosfet.org/pixie a ACDSee like program..."
    author: "Goddard"
  - subject: "Re: Does it come with KDE 3.0?"
    date: 2002-04-07
    body: "if you want to burn cdrom, take a look at \nkreatecd (easy to use) :\nhttp://www.kreatecd.de/index.php\n\nregards"
    author: "thierry"
  - subject: "I think I've missed something"
    date: 2002-04-07
    body: "Ok its an awkward image viewer with no browsing capabilities and a VERY limited range of manipulation tools which causes it to fail in both areas. I hate to criticise open source projects but credit where its due. Gwenview is not far off being the best image viewer I've used and should, (IMHO) be the default KDE viewer. Perhaps KView should concentrate more on being an easier alternative to GIMP for basic image manipulation"
    author: "peter jeffries"
  - subject: "PixiePlus rocks"
    date: 2002-04-07
    body: "For handling large collections of pictures mosfet's PixiePlus is awesome tool. People who own e.g. digital camera, PixiePlus is what they want to use to browse their mighty picture collections :)\n\nGood work with KView also, but check PixiePlus 0.3 at http://www.mosfet.org/pixie/."
    author: "samppa"
  - subject: "Re: PixiePlus rocks"
    date: 2002-04-07
    body: "Konq plus Kuickshow work for me as well.. :-)\n"
    author: "Rob Kaper"
  - subject: "Re: PixiePlus rocks"
    date: 2002-04-29
    body: "sweet - that Pixie looks just like Thumbs-Plus (formerly Thumbs-Up). Linux is looking better and better ;-)"
    author: "gaffo"
  - subject: "Re: PixiePlus rocks"
    date: 2003-01-01
    body: "It's true, this is a great program.  This is the only program I have with SuSE 8.1 that will make batches of thumbnails all at once. "
    author: "mowo2000"
  - subject: "It's a pain to open a lot of images"
    date: 2002-04-08
    body: "KView really needs an open directory function (with a recurse sub-directories option)"
    author: "gergi"
  - subject: "Konqueror does a beautiful job, (as does KView :)"
    date: 2002-04-09
    body: "Your Ultimate Web Browser  -  Konq. yur descTop\n\n1. Click on  [Preferences]+[Look & Feel]+[Desktop] \n\n2. Now checkmark, [Show Previews for:].  Enjoy!\n\nKView is excellent for slideshows.   Very fast &\nuseful for scanning images to your scanner, plus\nadding brightness and gamma effects to dull or dark\nimages.\n\nKDE3 is so entirely integrated now. You can do \nanything from anywhere. Lovely work KDE Team!. \n\nTIP:  \"NoAtun\" - Multimedia Player. Add KDEAddons\nfor extra plug-ins and c/o the gorgeous themes!.\n\n-chow\n"
    author: "linuxkid"
---
<a href="mailto:staerk_at_kde.org">Klaus St&auml;rk</a> informs us that
the <a href="http://www.kde.de/">German KDE website</a> has
<a href="http://www.kde.de/appmonth/2002/kview/index-script.php">announced</a>
the April 2002 <a href="http://www.kde.de/appmonth/">App of the Month</a>:
<a href="http://www.ph.unimelb.edu.au/~ssk/kde/kview/">KView</a>.
As usual, the useful (German) review includes a screenshot-laden
<a href="http://www.kde.de/appmonth/2002/kview/beschreibung.php">description</a>
of KView, as well as a
<a href="http://www.kde.de/appmonth/2002/kview/autor.php">note about</a>,
and an
<a href="http://www.kde.de/appmonth/2002/kview/interview.php">interview
with</a>, its maintainer,
<a href="mailto:kretz@kde.org">Matthias Kretz</a>.
Find out more about your favorite image viewer and the talent behind it.

<!--break-->
