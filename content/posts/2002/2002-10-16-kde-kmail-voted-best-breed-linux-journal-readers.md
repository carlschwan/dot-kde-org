---
title: "KDE, KMail Voted Best of Breed by Linux Journal Readers"
date:    2002-10-16
authors:
  - "Dre"
slug:    kde-kmail-voted-best-breed-linux-journal-readers
comments:
  - subject: "Pit-Nicking"
    date: 2002-10-16
    body: "\"Grab the November 2002 Linux Journal issue (#103) from your nearest newsstand to read more.\"\n\nJust nitpicking, but you're gonna have to wait two weeks before you can do this (source: the Linux Journal calendar, in issue 100)"
    author: "Anon."
  - subject: "Simply the best"
    date: 2002-10-16
    body: "Congratulation to the KDE and Kmail teams.  Kmail is simply the best e-mail client I've used.  I hope Kroupware (can we change the name please?) won't mess with the simplicity of Kmail."
    author: "Ned Flanders"
  - subject: "Re: Simply the best"
    date: 2002-10-17
    body: "I agree the note in (). Kroupware is not a good name."
    author: "Action"
  - subject: "Re: Simply the best"
    date: 2002-10-18
    body: "Kroupware does not sound like a healthy choice."
    author: "Pingo"
  - subject: "Gnutella?"
    date: 2002-10-16
    body: "KMail is really the best email client.\nMozilla? Don't think so.\nGnutella? It's too  slow. Even my computer science professor says it sucks."
    author: "KDE User"
  - subject: "Re: Gnutella?"
    date: 2002-10-16
    body: "Change your teacher. Gnutella is not mail client."
    author: "foo_head"
  - subject: "Re: Gnutella?"
    date: 2002-10-17
    body: "I think that he refering to the article. \nGnutella is mentioned as Favorite Distributed File Sharing System."
    author: "Jas"
  - subject: "Re: Gnutella?"
    date: 2002-10-19
    body: "Yes, you're right.\nGnutella as a file sharer\nand Mozilla as a browser"
    author: "KDe User"
  - subject: "Re: Gnutella?"
    date: 2002-10-16
    body: "Yes, actually gnutella sucks as _e-mail client_ ;-)\n"
    author: "p2p client"
  - subject: "Re: Gnutella?"
    date: 2002-10-17
    body: "I dunno -- right now my IMAP server is down and P2P'ing my mail would be an improvement over that.\n\nJust title it \"Britney Spears XXX topless\" and it'll be there in no time!"
    author: "Otter"
  - subject: "Re: Gnutella?"
    date: 2002-10-17
    body: "KMail is the best.... of what sample?\nSure, it gets well up there near the very top of the Linux mail client league, and beats LookoutDistress hands down. But Demon's Turnpike product is still sufficiently much better to keep me running a 'doze box."
    author: "FussyGuy"
  - subject: "A few features are still  underdeveloped  in KMail"
    date: 2002-10-16
    body: "On the whole i think that KMail is a powerful email client and I'm generally pleased using Kmail. But there are a few severy shortcomings, especially for power users with ten thousands of mails:\n\n- There is no quick search comparable to Mozilla mail.\n\n- The results of a \"search messages\" Operation can't be deleted/moved/copied en block. This is a feature I'm desperately missing in Kmail and so I'm meditating about switching to Mozilla.\n\nI prefer retrieving to searching.\n"
    author: "Georg"
  - subject: "Re: A few features are still  underdeveloped  in KMail"
    date: 2002-10-16
    body: "In KDE 3.2 you will be able to do everything with the results of a \"search messages\" operation what you can currently do with the messages in one folder. Actually the results will be displayed in special \"Search folders\". The cool thing is that you can keep those \"Search folders\" and that they will be dynamically updated each time new messages arrive or messages are deleted.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: A few features are still  underdeveloped  in KMail"
    date: 2002-10-16
    body: "*droools*"
    author: "Kent Nguyen"
  - subject: "Re: A few features are still  underdeveloped  in KMail"
    date: 2002-10-18
    body: "Dude. This is just how BeOS handled mail. Through persistant queries."
    author: "Rayiner Hashem"
  - subject: "Re: A few features are still  underdeveloped  in KMail"
    date: 2002-10-18
    body: "Whoa!  Way cool!"
    author: "JoJoMan"
  - subject: "Re: A few features are still  underdeveloped  in K"
    date: 2002-10-19
    body: "these are known as Vfolders in the gnome world.  Evolution has had these for a while, and i've found theme extremely useful.  for example, i have a vfolder entitled \"Last 24 hours,\" which displays, oddly enough, all the mail that i'v received in the last day.  \ni'm really excited about kmail, however.  i've been leapfrogging back and forth between gnome and kde for the last three years, and hope to jump back to kde once 3.1 or 3.2 is released."
    author: "matt"
  - subject: "Re: A few features are still  underdeveloped  in K"
    date: 2002-10-17
    body: "Also, I wish you could sort by status...and smoother Eudora/Outlook import abilities..."
    author: "Joe"
  - subject: "Re: A few features are still  underdeveloped  in K"
    date: 2002-10-17
    body: "> I wish you could sort by status\n\nClick the subject column header until it is renamed \"Subject (status)\""
    author: "NoName"
  - subject: "mail - client"
    date: 2002-10-16
    body: "Give sylpheed a try http://sylpheed.good-day.net/\n\nO.K. not realy a native kde programm ;-)) , anyway - the best and the fastest from my feeling."
    author: "foo_head"
  - subject: "Kmail's great, but..."
    date: 2002-10-16
    body: "I have v.3.04 crashing a lot with TLS. :("
    author: "Shaman"
  - subject: "Re: Kmail's great, but..."
    date: 2002-10-16
    body: "I've been happily  using incoming IMAP + TLS for quite some time.  Or are you talking SMTP over TLS?\n\n-- rex"
    author: "Rex Dieter"
  - subject: "Re: Kmail's great, but..."
    date: 2002-10-16
    body: "Did you submit a bug report including a backtrace?"
    author: "Ingo Kl\u00f6cker"
  - subject: "maildir compatibility"
    date: 2002-10-16
    body: "I'm pleased with the improvements in 3.0; but still, I'm really looking for cross-client compatibility.  When I open Maildir with KMail, it marks *all* the messages as read!  Evo and Mutt then get confused.  I really want to use KMail and get off Evolution, but I can't yet because it conflicts with procmail...\n\n(If anybody has any suggestions, they'd be much appreciated...  :)"
    author: "satai"
  - subject: "Re: maildir compatibility"
    date: 2002-10-17
    body: "KMail and procmail:\nhttp://docs.kde.org/3.0/kdenetwork/kmail/faq.html#id2836108\n\nI'm using procmail to prefilter spam and KMail's filters to filter the incoming messages into the appropriate folders.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: maildir compatibility"
    date: 2002-10-18
    body: "Amen... I WISH Kmail would work properly with Maildir.  Evolution can.  Why can't Kmail?  \n\nIE I have qmail delivering to my mailfolders all prefiltered and everything.  If KMail would just check for new messages every X minutes in the maildir folders, I'd be HAPPY!"
    author: "jstuart"
  - subject: "Re: maildir compatibility"
    date: 2002-12-01
    body: "I agree.  You wouldn't think this kind of behaviour would be too much to ask.  I just want to be able to choose between mutt and kmail, but it's actually very hard to find a set up with Maildir where they can BOTH work reliably."
    author: "tom"
  - subject: "Re: maildir compatibility"
    date: 2002-12-01
    body: "I agree, this would also allow the use of offlineimap using the offline imap program."
    author: "psharp"
  - subject: "KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-17
    body: "KDE, the choice of reactionary 12 year old zealots!\n\n(see how long it takes for the K-Censors to remove this...)"
    author: "noone"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-17
    body: "Jealous?\n\nRinse\n"
    author: "rinse"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-17
    body: "Yes, KDE appeals to everyone unlike GNOME which chases its own users and developers away.  You want proof?"
    author: "anonymous"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-17
    body: "Interesting hypothesis. Let's test it... Let's see... I'm 45, assemble coherent thoughts, respond rather than react and use and develop on KDE. I just got an email last week from a retired teacher with a PhD (many years my senior) who was very happy with his latest compile of Quanta. This dog don't hunt.\n\nKDE... the choice of sophmoric 14 year old trolls for irrational bombasts? Gotta be. Mom got you this computer and all we got was this lame flame bait. Better luck next time. Okay, go back to the Britney Spears fan page now. Come back when you start shaving, okay?"
    author: "Eric Laffoon"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-17
    body: "> (see how long it takes for the K-Censors to remove this...)\n\nWhat's wrong with moderating trollish/flamebait/offtopic posts?\nEditors, remove parent :)"
    author: "fault"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-22
    body: "do you live in a communist state?\n"
    author: "air-wave"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-22
    body: "> do you live in a communist state?\n\nNo I don't, but it would seem that a truly communist state would have the least number of editors and moderators.\n\nLook up the definition of moderators and editors in a dictionary. Yes, it's censuring, and yes, they have the right to do it.\n\n\nk, thx."
    author: "fault"
  - subject: "Re: KDE and Kmail choice of gibbering 12 year olds"
    date: 2002-10-22
    body: "er, I meant censoring, not 'censuring'"
    author: "fault"
  - subject: "Well it hasn't appeared on Slashdot yet..."
    date: 2002-10-17
    body: "But it's currently showing all the signs of a good Slashdotting... err... dot.kdeing?\n\nLooks like the dot has quite a readership these days."
    author: "My Left Foot"
  - subject: "Re: Well it hasn't appeared on Slashdot yet..."
    date: 2002-10-20
    body: ">Looks like the dot has quite a readership these days.\n\nYa! And most of us would like even the smallest bits of news (even the only-half-way-kde-related ones) to be posted... so we dont have to go to the \"slashed up dot\" ;-D\n\nPlayfully\n/kidcat"
    author: "kidcat"
  - subject: "I must be using a different Kmail"
    date: 2002-10-17
    body: "First: congratulations to Kmail and KDE.\n<BR>\nBut...\n<BR>\nI have been using Kmail for a few months and I keep running\ninto difficulties, most of them having to do with Kmail refusing\nto throw away messages I marked as deleted (yes, I have tried\nall tricks in the FAQ and in the bug reports).\n<BR>\nAdmittedly I have a complicated mail setup: at home I read\nmail on a disk on host X mounted with NFS to host Y, and\nevery time I go to my work, I 'scp' the various mailfiles\nand indexes to host Z, which is my PC at the university. \nAnd yes, host Z runs one of the stable KDE's (3.0.3) and\nat home I have 3.1 beta 2.\n<br>\nSomehow I feel that the file- and directory setup of Kmail\nis too complicated. With the other mailclients I am familiar \nwith, it never was a problem when I shifted files around by \nhand. Heck, that is what an xterm is for, isn't it? \n<br>\nAnyway, I keep running in problems and I am amazed that nobody\nelse does. I cannot be *that* stupid, and even if I am, Kmail\nand KDE should be usable for stupid people too, just as the \nNetscape 4.6 client that I used before Kmail, was very usable.\n<br>\nHaving said that, I must thank Ingo Kloecker for\nhis patience; he really must be fed up with my whining, but\nhe keeps answering patiently.\n<p>\nPaai"
    author: "Hans Paijmans"
  - subject: "Re: I must be using a different Kmail"
    date: 2002-10-18
    body: "Why not just run secure imap on your home machine, and connect to it from work? No  copying required."
    author: "Psiren"
  - subject: "Re: I must be using a different Kmail"
    date: 2002-10-23
    body: "You're not alone. \n\nKMail is much, much better than it used to be (I'm using a stock Mandrake 9.0 now) but I still end up falling back to Mozilla. Kmail crashes about once an hour, and doesn't work properly after that unless I restart KDE. It's also very slow in browsing my mail folders (which are very large). Mozilla never crashes and is much faster ... \n\nOh and before people ask about backtraces etc., no I haven't submitted a bug report for a while because recompiling knetwork with debugging would take an age (slow laptop), and getting sources over dialup is just horrible.\n\nI'd love to use KMail, and I'll keep trying it again each time I upgrade Mandrake, but it's not stable enough yet (for me anyway)."
    author: "Ed Moyse"
  - subject: "Attachment mark"
    date: 2002-10-17
    body: "If kmail could indicate by an icon or whatever that a message holds attachment, it will be cool (or better than anything else). I know, it's very basic but lacks in kmail. Strange."
    author: "Thierry"
  - subject: "Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "I used Gftp and yesterday I discovered Kbear. What a wonderful program, far better than gftp, I immediatly adopted it !\n\nAnd I don't understand : why a such program is not in KDE ?\nAnd certainly some useful others also need to to be in it, like Kwav, KreateCD or KOnCD...\n\nI wish a new KDE Package, called, for example \"KDE tools\".\nYou want to promote KDE, this is a good way !\n\n(and please, don't say that it is the work of Red Hat or Mandrake, it needs at first the help of KDE !)"
    author: "Alain"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "It exists - it's called \"Extra Gear\" (a slightly punny title).  It's non-official KDE applications that are synced with KDE releases and available to be packaged by the various distros at the same time KDE releases occur. (Of course, they are always available, but I tend to upgrade my apps when KDE upgrades).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "It seems very confidential. I searched on Google, I only found the page http://mail.kde.org/mailman/listinfo/kde-extra-gear for subscribing to a mailing list...\nNothing seems said in the KDE sites about this \"KDE Extra Gear\", I see no rpm.\n\nI spoke about someting like \"KDE games\", available for any KDE user, not about such a confidential (but certainly interesting) thing. Perhaps it's only a beginning, I hope it's a beginning..."
    author: "Alain"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "Try http://extragear.kde.org, but still in work. :-)"
    author: "Anonymous"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "Thank you. Yes, in work, and still few things convincing. For example :\n- Kbear is not here\n- KreateCd is here, KonCD no. However, there are rpm for KonCD, but no rpm for KreateCD (as I searched few days ago), so I think that KonCD would be a priority...\n- It is written \"There are multiple possible reasons for an application to not be in one of the KDE modules, for example: duplication of functionality\nvery specialized\" But KBear, KreateCD/KonCD are not in duplication and the functionnalities are not specialized. So I do not understand that these programs are not directly in some KDE modules.\n\nIn conclusion, I hope that not specialised and not in duplication programs will go in KDE moduls, the others in KDE Extra Gear, which is interesting, of course."
    author: "Alain"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "\nWell, there *is* the possibility that the author of KBear doesn't want to be released with KDE - that she or he enjoys the freedom of a relaxed schedual, etc.  You have to remember that Free and Open Source software is a community of developers, and each project has it's own directions and choices.  The KDE project did its part by extending the invitation to projects outside of KDE's scope, but that isn't a mandate that all KDE applications *have* to become part of Extra Gear - simply an offer.\n\nAnd as for your late sentence, KDE Extra Gear *is* a KDE module - that's the whole point.  It's the module for \"everybody else's applications\" - there's the standard KDE gear (\"gear\" as in 'stuff' a la 'fishing gear', not the mechnical toothed wheel), and then there is the extra gear.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "Yes, I understand about the choice of authors. Nevertheless, I think that a FTP client or a burning CD program is not an \"extra\", they have to be inside the \"standard\", because anybody needs such a program...\n\nI see in Mandrake gFTP and xcdroast, I think that they are bad programs when I compare with Kbear and KonCD, absent of the distro. Why absent ? Because Mandrake has bad taste and because KDE doesn't include in its standard these good essential programs... \n\nAnd, again about the choice of authors, I doubt that they don't want to include their program in the distros..."
    author: "Alain"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2003-01-19
    body: "You get my full agreement!!!!\n\nWe need more tools in the kde stadart packages!\n\nIt's very poor for a desktop system that it has no functional \nmultimedia support like mplayer and xine!!!\n"
    author: "Matthias Fenner"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "kwav?\nThis program is dead for 4(?) years now, and only available for KDE 1.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "[about the now dead Kwav]\nArgh... Is there another similar KDE program ? Or even a Gnome one, or else... ?"
    author: "Alain"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "Have a look at audacity:\n\nhttp://audacity.sourceforge.net/\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2002-10-19
    body: "Thank you, it's interesting, however limited (for a windows user of such a tool) and not very ergonomic (for a KDE user). It lacks such a KDE tool..."
    author: "Alain"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2003-05-28
    body: "I've tried KBear myself. It sucks big time. The interface is too cluttered and compilcated. The whole thing kept crashing left and right. Still searching for a decent KDE ftp client. Have to open console and use ncftp everytime. For me ncftp rocks!!\n\nNandz.\nhttp://home.iitk.ac.in/student/nanda"
    author: "Saurabh Nanda"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2005-01-30
    body: "I prefer Kasablanca, very clean and lightweight - http://kasablanca.berlios.de/"
    author: "Jakamoko"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2005-09-04
    body: "using suse9.3 whick comes with kbear for some reason. It sucks. crashes etc. I use gftp which just works. Wish there was filezilla for *nix"
    author: "t"
  - subject: "Re: Kbear best FTP client - For a new KDE package"
    date: 2006-02-05
    body: "KBEAR!!\nyeah it sucks big time. its in suse 10.\nIT CRASHES, IS MESSING UP WITH POLISH FONTS (GETTING SOME SQUARES INSTEAD OF LETTERS) AND IT'S MESSING UP WITH THE KICKER AND BAGHIRA STYLE. THIS SOFTWARE IS CRAP INDEED.\n\nTHX\nTIGGY"
    author: "TIGGY"
  - subject: "GIFs!!!"
    date: 2002-10-19
    body: "Hey, why does dot.kde.org still use GIFsinstead of PNGs ??\n\n--> http://www.burnallgifs.org"
    author: "Somebody"
  - subject: "Re: GIFs!!!"
    date: 2002-10-19
    body: "Yes, and PNGs are smaller too."
    author: "Stof"
  - subject: "Re: GIFs!!!"
    date: 2002-10-19
    body: "Not always - I preferentially use PNG, but don't have a horrible problem with using GIF.  Sometimes the GIF is smaller.\n\nNow if only all browsers supported a full range of alpha transparancy rather than thresholding it - then I'd be happy.  :(\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GIFs!!!"
    date: 2002-10-19
    body: "Practically all graphical browsers support gifs, even the free ones, and even the extremely old ones.  Why don't you write them and tell them to stop supporting gifs?  \n\nThat would be better and if new browsers came without gif support, I would ditch backwards-compatibility in favour of forwards-compatibility.  Another reason for ditching the gifs of course is if they pose a real practical problem.  Is there any reason to claim that our gifs are not legitimate?\n\nIn any case, it is easy to fall back on the original PNGs.\n"
    author: "Navindra Umanee"
  - subject: "Re: GIFs!!!"
    date: 2002-10-20
    body: "> Why don't you write them and tell them to stop supporting gifs? \n\nIt is explain here : http://burnallgifs.org/\n\n(but I have a site and I keep the gifs, I only use png for new images... Perhaps when I will do a big reorganisation... I remember there is an easy program, gif2png...)"
    author: "Alain"
  - subject: "Kmail is the best? What?"
    date: 2002-10-23
    body: "I'm a GNOME user but respect the work that is/was put into KDE.  But I have to say, I almost fell out of my chair when I read that Kmail was voted best email client.  Was Evolution not reviewed?  Even with Evolution being a complete Groupware product, it's email client was far superior to Kmail (from where I'm sitting).  Please don't take this as flame, I'm just a little confused/stunned...  "
    author: "Mike Hill"
  - subject: "Re: Kmail is the best? What?"
    date: 2002-10-24
    body: "> it's email client was far superior to Kmail (from where I'm sitting). \n\nI don't think this is necessarily so with recent versions of kmail. \n\n> Was Evolution not reviewed? \n\nI beleive it was one of the choices. It's a shame that LinuxJournal didn't list second and third like last year (http://www.linuxjournal.com/article.php?sid=5441)"
    author: "fault"
  - subject: "Re: Kmail is the best? What?"
    date: 2002-10-24
    body: "I find that evolution has a cluttered and complicated user interface. Kmail's is much nicer. I find Kmails pop support to be good and the imap to be average. As I mostly use pop and rarely imap, I think Kmail is pretty good for me. Maybe evolution is a good choice for groupware or imap (never tried evo imap). I think in the future (3.2), kde will catch up on the groupware and imap support, so things will even up on those fronts, which is good."
    author: "ac"
  - subject: "Re: Kmail is the best? What?"
    date: 2003-06-18
    body: "I have tried Evolution and even upgraded it twice to see how fast it was growing. I can compare it with Outlook and kmail (a little).  To start, \n1. Evolution delivery and/or read confirmation. \n2. Cutting and pasting from calendar to email (or vice versa) is not in the menu or right clickable. It can be done with keyboard shortcuts but isn't documented.\n3. Evolution will not offer you to receive mail from any single source. just all or nothing. The work-around is to disable the ones you don't need.\n\nOutlook simply surpasses Evolution but Ximian is doing not so bad job. They just need to put out an upgrade with some 'wow!' to it. Kmail has more (so far) and as far as the term 'Groupware goes', isn't KDE in a sense Window Manager 'ware'? It really shouldn't matter if the programs are all under one roof. In fact, to me 'groupware' means that you have to put up with other things that you don't need while you are checking your email or calendar.\n\nAlso, what's 'Red Carpet' all about? Does your RPM need an RCM?"
    author: "rkm"
  - subject: "Re: Kmail is the best? What?"
    date: 2002-10-24
    body: "Okay, I had to check this out.  KMail has improved a lot since the last time I saw it.  It supports HTML now I see (that should tell you how long it's been, or is that new with KDE3?).  I see it's still using KDE's address book, which leaves a lot to be desired when compared to evo's contact manager.  The UI is nice and simple...I like that (agreed evo's could use some cleaning up).  I guess I'm just partial to having my calendar, contacts, task, and oh yea...the most excellent Summary page all within one click.  But for an email client, KMail has come along way.  Good job KDE team!\n"
    author: "Mike Hill"
  - subject: "Re: Kmail is the best? What?"
    date: 2004-01-31
    body: "KMail is just a mail client, and a very good one. If you want something that acts like evolution you should check out kontact that is using different clients as KParts and will give you a better end result: Summary (Kontact), Mail (KMail), Contacts (k address book), ToDo (Korganizer+Kontact), Calendar (Korganizer), News (Knode) and Notes. It is much better than evolution and you have the same single interface... you don't need to use separate applications in separate windows. KMail also facilitates access to groupware functionality for IMAP or other. \nI think that even kaddressbook is much better than evolution.\nAnd best of all... kde always compiles without problems. Evolution needs packages that are considered obsolete since gnome-2.4.\nIt's true, there are still problems with kmail (panic when something's going wrong with the remote IMAP folders) and I haven't figure out exactly how to use the IMAP Resource Folder Option but I think that kontact beats evolution by far... There was no visible evolution in Evolution lately while kontact applications went through a radical transformation."
    author: "TIC"
  - subject: "Re: Kmail is the best? What?"
    date: 2002-10-25
    body: "me too - evolution rocks"
    author: "me"
  - subject: "Re: Kmail is the best? What?"
    date: 2007-10-19
    body: "I noticed that evolution is done by Novell, which creates SuSE.  Doesn't SuSE use KDE by default?  I wonder if evolution maybe doesn't have as much attention as it could get? Maybe that's what did it.\n\nAnyway, Kmail is actually now part of a KDE groupware client: kontact.\n"
    author: "Jeff"
  - subject: "My only issues with Kmail"
    date: 2002-10-24
    body: "1.  Kaddressbook sucks.  No way to sort by last name that will stay.  And, your preferences dont stay.  What I mean is, in Kaddressbook you get a list of all your contacts with the \"file as\" \"email\" \"business phone\" columns etc. all squished together.  So, I resize them but they don't stay.  Are either of these things addressed in KDE 3.1?  Please, someone tell me.\n\n2.  No reply button when you open up a message.  Yes, I know you can right click or type \"R\" or whatever, but I like buttons.  No reply button.\n\n3.  No way to eliminate the preview pane."
    author: "John"
  - subject: "Re: My only issues with Kmail"
    date: 2002-10-25
    body: "Much of kaddressbook has been rewritten for KDE 3.1."
    author: "Anonymous"
---
<a href="http://www.linuxjournal.com/">Linux Journal</a> has <a href="http://www.linuxjournal.com/article.php?sid=6380">announced</a> the winners of its 2002 (8th Annual) Readers' Choice Awards.  According
to Linux Journal, <em>[a]lmost 6,000 Linux Journal readers visited
the Linux Journal web site and voted on their top choices in 25
categories</em>.  KDE took the honors in the <em>Favorite Desktop
Environment</em> category, and <a href="http://kmail.kde.org/">KMail</a>
in the <em>Favorite E-mail Client</em> category.  Grab the November
2002 <em>Linux Journal</em> issue (#103) from your nearest newsstand
to read more.

<!--break-->
