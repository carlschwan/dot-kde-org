---
title: "Kernel Cousin KDE:  Issue #35 Is Out"
date:    2002-03-19
authors:
  - "Dre"
slug:    kernel-cousin-kde-issue-35-out
comments:
  - subject: "My favourite line..."
    date: 2002-03-19
    body: "\"While not as desirable among organic creatures, having multiple back ends is a highly sought-after feature for digital creations\"\n\nLOL."
    author: "david hj"
  - subject: "elf assumptions"
    date: 2002-03-19
    body: "is there any effort moving forward to run kde on non elf based unicies ? specifically i would like to run KDE on OS-X (with XFree86 installed of cource)"
    author: "mr mr"
  - subject: "Re: elf assumptions"
    date: 2002-03-19
    body: "Or why not compile it with Qt/Mac? Hope Trolltech will release a non-commercial Qt/Mac like Qt/Windows..."
    author: "Loranga"
  - subject: "Re: elf assumptions"
    date: 2002-03-19
    body: "This isn't assurance of anithing.\nThere is a qt free version for windows, but no kde for the system."
    author: "protoman"
  - subject: "Re: elf assumptions"
    date: 2002-03-20
    body: "While it's not yet fully working, check out\n\nhttp://kde-cygwin.sourceforge.net/\n\nHave fun!\n-Arondylos"
    author: "Arondylos"
  - subject: "Re: elf assumptions"
    date: 2002-03-20
    body: "This issue just came up on kde-devel:\n\n<http://lists.kde.org/?t=101605491100001&r=1&w=2>"
    author: "Otter"
  - subject: "Maintainership"
    date: 2002-03-20
    body: "For some reason, I can't help but imagine if the kernel functioned like kde did...\n\nThe way that the maintainership of Krayon changed with the equivalent of 'Ugh. Can I do that?' should be a model for all things open source.\n\nThat just rocks.\n\nTroy\ntroy(at)kde(dot)org"
    author: "Troy Unrau"
  - subject: "Re: Maintainership"
    date: 2002-03-20
    body: "This is pretty much unique for KDE project. Every other large project rests\non a very authoritative regime although they all claim to be democratic.\n\nThat is the number 1 reason for joining KDE project if you'd ask me.\n\nI also have a conjecture to make here. Better programmers are more\nopen minded and care about the code more than they care about fame\nor reputation. Whenever there is a large lameness coefficient in a\nproject, you will see people who are ego-driven, and involved in\ntasteless conspiracies... This conjecture can be extended to other\nprofessions/organizations such as companies and universities at your will.\n\n\nCheers,"
    author: "exa"
  - subject: "Re: Maintainership"
    date: 2002-03-20
    body: "In that case, I guess you believe that the Linux kernel project has a very large lameness coefficient. You see, I have been reading Kernel Traffic for over two years now and it has become my weekly soap opera. I don't think there is a project which has more ego-driven flame-fests than the kernel.\n\nSome people say to that: \"well, the really top hackers don't do it.\" These people (a) only think of Linux, Alan and a few others, and (b) have never taken the time to read up on Linus' flames. They should, it's damn funny at times:\n\nhttp://kt.zork.net/kernel-traffic/kt19990819_31.html#9"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "KOffice should get more attentions"
    date: 2002-03-20
    body: "Why doesn't KOffice get that much coverage ? Albeit XLST, Krita, Karbon were mentioned in previous KDE-KC installments, there's nothing to say about KPresenter being WYSIWYG, new filters (MS Write, PalmDoc, AmiPro), redesign of Kontour, the latest cool background spellcheck, forthcoming release of 1.2 beta....\n"
    author: "ac"
  - subject: "Re: KOffice should get more attentions"
    date: 2002-03-20
    body: "KC KDE covers the mailing lists. if it doesn't appear there, it doesn't get covered. even then, there are usually a few hundred threads spanning a dozen or so lists to choose from each week.\n\nif you think that KOffice should get more coverage in KC KDE i would more than welcome your contributions and include them in future editions. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KOffice should get more attentions"
    date: 2002-03-20
    body: "Well, that was my list :-P AFAIK these are mentioned in the koffice-devel mailing list, I just followed the archive."
    author: "ac"
  - subject: "More coverage? Yes, but not yet!"
    date: 2002-03-20
    body: "It might be a good thing that there hasn't been much KOffice coverage until now, because it's still a beta (alpha?) stage project IMHO. I really think the revision numbering should be < 1.0 ATM!\n\nI've spent a good few hours with 1.1.1 on MDK8.1 and reluctantly looked elsewhere for the time being.\n\nHOWEVER there's a good future for KOffice because: \n\n1) Star/Open Office still suffers from being far too heavyweight an app for many users and doesn't feel that intuitive to me. Does anyone really know what Sun's plans for this suite are?\n\n2) I like Gnumeric and GIMP (fiddly to use!), but Abiword has never inspired. There's also the fact that many KDE users will lean towards KDE apps if they possibly can.\n\n3) I'm a bit puzzled by the current Hancom Office 2.0 evaluation edition - bits seem excellent, but other parts seem very underdeveloped. I'm also not sure what the Kompany is up to with regards to this.\n\nSo most of the competition, though more developed, is not exactly there yet. \n\nKWord will be excellent once WYSIWYG is sorted and filters improved - there's no question that frames make this app stand out. \n\nKSpread is ok, KPresenter is fine, Kivio looks nice but doesnt seem usable without stencil sets, most of the other progs show promise. Then there's Kugar... hmm... is there any useful documentation on this one anywhere?\n\nOnce KOffice apps reliably support WYSIWYG and better filters, promote like mad and release regular, frequent updates - that will generate a lot more interest than there is ATM.\n\nI'm really hoping 1.2 will be the break through release!"
    author: "Rich"
  - subject: "Re: More coverage? Yes, but not yet!"
    date: 2002-03-20
    body: "Doesn't make sense. KC-KDE is about development and every moves in KOffice CVS should be considered as development, too. We're talking about what happened in the CVS and mailing lists, not about the release version.\n\nYes, KWord 1.2 will have fully support for WYSIWYG and a few new filters."
    author: "grin"
  - subject: "Re: More coverage? Yes, but not yet!"
    date: 2002-03-20
    body: "Doh - you're quite right, I was getting a bit carried away.\n\nOf course it should be promoted on the development lists, sorry for not switching my brain on before posting.\n\nGuess I'm getting excited by the thought of the 1.2 release!"
    author: "Rich"
  - subject: "Re: More coverage? Yes, but not yet!"
    date: 2002-03-20
    body: "http://developer.kde.org/development-versions/koffice-1.2-release-plan.html"
    author: "ac"
  - subject: "Do any of the developers us FreeBSD"
    date: 2002-03-22
    body: "... ?  Please no one say \"BSD is dying\" .... sigh.  \n\nIf there's a port or pkg of KDE3.0 you'd be sure to link to it.\nSo the question is anyone building regularly on FreeBSD?\n\nAny \"desktop performance\" hack or improvements for FreeBSD kernel w/ KDE?  On Linux I had the preempt patch working nicely with kde2.2 and it was fairly snappy (except on launch but ...).\n\nKDE 3 really looks nice and has more and more hackers, documentors, translators and testers! (but are any using FreeBSD ;-) ... or NetBSD"
    author: "FreeBSD"
---
In <a href="http://kt.zork.net/kde/kde20020308_35.html">this week's</a>
edition of <a href="http://kt.zork.net/kde/index.html">Kernel Cousin KDE</a>,
<a href="mailto:aseigo@olympusproject.org">Aaron J. Seigo</a>
covers the KDE 3 RC releases, the controversy about KDE 3 release
coordination, progress with <a href="http://www.koffice.org/">KOffice</a>,
trouble on the documentation front, and the new
<a href="http://qtcsharp.sourceforge.net/">Qt C# bindings</a>.
Kudos to Aaron who, as usual, has wrapped a week's worth of development news
up in a succinct and pleasurable, yet informative, read.
<!--break-->
