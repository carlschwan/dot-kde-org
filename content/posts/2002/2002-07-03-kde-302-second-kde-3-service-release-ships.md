---
title: "KDE 3.0.2:  Second KDE 3 Service Release Ships"
date:    2002-07-03
authors:
  - "Dre"
slug:    kde-302-second-kde-3-service-release-ships
comments:
  - subject: "Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "title tells it all."
    author: "abemud"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "Yes, go KDE!, but what's with the hostility to Red Hat?\n"
    author: "Navindra Umanee"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "why does red hat suck specifically?\n\nI don't use red hat, I am a Mandrake user.  \n\nIs it red hat's relative lack of support for KDE? or is it somthing else?\n\n"
    author: "L.D."
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I use RedHat, and I like it.  And I use KDE on RedHat, which means I have to put up with a lot of shit from RedHat people AND KDE people.\n\nBut basically, the deal is that RedHat never releases updated KDE updates.  What you get on the CD is what you're stuck with until the next full RedHat release.  They prioritize Gnome over KDE, which is a perfectly valid^H^H^H^H^H interesting choice.\n\nI like their other decisions though.  They're conservative on the right things and cutting edge on the right things, as far as I'm concerned.  But it's a pain to be a RedHat KDE user."
    author: "ac"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "That's true, I only know one single developer working on KDE at Red Hat.  :(  He obviously can't do everything but I think we can safely applaud him for trying.  Red Hat has tons of G developers on the other hand...\n\nHow can we convince Red Hat to devote more resources to KDE?  Any ideas?"
    author: "Navindra Umanee"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "Yes, I love Bero too and shower him with kisses from abroad.\n\nThe best way to get RedHat to devote resources to KDE is to just be a better desktop environment.  KDE is great but it isn't perfect yet.  Well, I haven't downloaded 3.0.2 yet...maybe I'm wrong ;-)"
    author: "ac"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I don't think good has anything to do with it, unfortunately.  KDE has been comparing favourably to the competition for a long time now, yet Red Hat hasn't bulged in the slightest."
    author: "Navindra Umanee"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "It can't just compare though - it has to be flat out, hands-down better.  People invest much time learning the ins and outs of their desktop environment, and how to be productive in it.\n\nI've tried frequently to switch from GNOME to KDE, but I can't.  Not because one is better than the other (To wit - KDE may be 'better' than GNOME).  But because KDE doesn't work quite the way I want it to.  And I'm very picky..:)"
    author: "gengis"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "If you're so picky and so happy with GNOME1, I'm sure you'll be pulling your hair out with GNOME2, eh!"
    author: "Navindra Umanee"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I agree completely. I have considered switching to other distributions (usually Mandrake or Debian, depending on the time of day:), but I think I will stick with RedHat, despite KDE pains. Why? A few reasons:\na) I like a number of their choices in what software they choose, apart from preferring GNOME to KDE :(\nb) Most proprietary software for linux comes pre-compiled for RedHat, so I know I can run things that don't provide source.\nc) A fair amount of open-source software is also precompiled on RedHat.\nd) The lure of Debian's apt-get is reduced by the fact that I now have apt-get for rpm, and combined with the Freshrpms tree and various others, apt-get can get me pretty much anything I need.\ne) The hassle of switching, when I have already built custom scripts for various things on my system, frankly isn't worth it.\nThat last point basically boils down to \"I'm used to it, and I don't feel like learning the quirks of a new system just yet\"\nAnyway, the lack of KDE RedHat rpms is quite annoying.\n"
    author: "Matthew Kay"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "With b), c) and e) you should have no problem with Mankdrake as it's perhaps the most RedHat-compatible distribution out there."
    author: "Anonymous"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "Same goes for SuSE - I switched to it over Mandrake because SuSE offered RPMs faster for more packages, and were more bug free.  Specifically the KDE 2.x series of RPMs.  Before I switched, I even tried using Cooker.  SuSE just seemed better at the time (and still does, but I haven't used Mandrake for about a year).\n\nI've never run into problems installing RH rpms on a SuSE system, either.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Debian (was: Re: Kde rocks, redhat sucks)"
    date: 2002-07-03
    body: "\"I have considered switching to other distributions (usually Mandrake or Debian, depending on the time of day:)\"\n\nSteer clear of Debian. They aren't actually cutting edge when it comes to KDE. I run KDE unstable. And guess what? It still doesn't have KDE3! How many months has it been already? Same thing goes for Xfree 4.2.\n\nWell, I have already made up my mind. I'll be moving to Gentoo in not so distant future."
    author: "Janne"
  - subject: "Re: Debian (was: Re: Kde rocks, redhat sucks)"
    date: 2002-07-03
    body: "gentoo is fantastic! as a former debian fanatic, after installing gentoo, I never looked back. I'm happy now that debian unstable didn't get kde3 for such a long time, or else I'd never have tried gentoo :-) it still hasn't got as many packages as debian, but it's groing ever more rapidly.. I love it that you get to choose yourself what you want, not just from a selection that is called 'unstable.' I just 'emerged' kde 3.0.2, although the default is still at 3.0.1. you just have to 'unmask' kde 3.0.2 and it's the new default. you have to love the ability to chose :-) the gentoo kernel sources are also great.. preemptible, low-latency patches.. :-P and of course, compiling everything from source is 'the-only-way' ;-)) pim zou het zo gewild hebben."
    author: "mark dufour"
  - subject: "Gentoo"
    date: 2002-07-03
    body: "Gentoo is great. It compiles everything from source including KDE, so it runs quick optimised on my machine. It will automatically upgrade your software overnight if you ask it to. The only reason I came to this site to check the 3.02 changelog was because I noticed I was running 3.02 this afternoon. Gentoo takes a long time to install, but once installed you never have to worry about keeping your system updated again.\n\nPhillip."
    author: "Phillip"
  - subject: "Re: Gentoo"
    date: 2002-07-03
    body: "Gentoo would be great if you could get a DVD/CDRs of the complete distro - its currently unusable unless you've got an xDSL link. Gentoo on a 56K takes days to download :(\n\nHopefully someone at Gentoo or a kindly Linux redistributor will see the light and sort something out one day - its a hell of a lot quicker to update if you can initially build from a local copy!"
    author: "Rich"
  - subject: "Re: Gentoo"
    date: 2002-07-04
    body: "See the 6/14/2002 post here: http://www.gentoo.org/, entitled \"Gentoo Linux T-shirts, caps, mugs and CDS. Oh, my!\""
    author: "Bill"
  - subject: "Deb packages for 3.0.2. are available"
    date: 2002-07-03
    body: "The Debian KDE maintainer has 3.02 packages (those were available over a week ago), that are apt-gettable (see http://calc.cx/kde.txt for apt-lines). It pays to pop in to the debian-kde list every once in a while:-)"
    author: "cobaco"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-07-03
    body: "So where is Xfree 4.2? Where are OFFICIAL KDE-debs? they have had MONTHS to get the packages in the official unstable-tree!"
    author: "Janne"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-07-03
    body: "If I remember correctly, the debs won't be uploaded to unstable till after the gcc 3.1.x transition,\nsee http://lists.debian.org/debian-kde/2002/debian-kde-200206/msg00072.html for the message from the maintainer explaining this. \n\nAs for official kde-debs, these debs are from the debian kde maintainer, and these debs are the ones that will go into unstable, once the gcc 3.1. transition is made. That's about as official as it gets.\n\nAs for Xfree, debian has to port Xfree to several architectures, work on 4.2. has started (see http://www.debianplanet.org/article.php?sid=696&mode=thread&order=0&thold=0 for more info), they should work fine on i386, but still need lots of work for the architectures not supported by Xfree while supported by Debian."
    author: "cobaco"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-07-04
    body: "As for XFree. Branden Robinson (head of the official Debian X-Strike force) said once,\nthat the debian packages are the source for every port ot other than i386 architectures.\nOn the last Linux Tag in Karlsruhe one of the XFree people said that as well. \nSo that why it takes longer for debian, if you have to have it stable on more than one\narch it takes some time to get it out. Redhat, SuSe and all the others have quite a nice\njob here, only i386 (and maybe a bit of Sparc) and thats it. Debian runs on i386, ppc,\narm, sparc, ultrasparc, mpis, mips-el, 68k, my washing machine and my microwave oven.\nSo it sometimes takes a bit longer.\n\nQuams (Who is using debian since 5 years in an production environment nad is often\nquite happy that some things take their time ...) "
    author: "Quams"
  - subject: "Debian xfree4.2-packages for i386++  are there"
    date: 2002-07-04
    body: "hi,\n\nplease go to http://people.debian.org/branden\nthere are experimental pre-release of XFree86 4.2.0 Debian packages available for\ni386, M68K, big-endian MIPS, IA-64, HP-PA and PowerPC.\n\ni run woddy on an athlon, put the deb-src line in /etc/apt/sources.list\nand compiled the packages from source optimized for my athlon (*), without any problems. \n\napt-get install pentium-builder\nexport DEBIAN_BUILDARCH=i686\napt-get -b source xfree86\n\nThanks, ISHIKAWA-san! Thanks, Branden. Thank you all, GNU/Debian!\n\n(*) or at least for pentiumII, athlon-optimized code is only avaible in gcc3.0"
    author: "holger"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-07-05
    body: "How about you stop flaming and engage your brain before your tongue? Look at http://www.debianplanet.org; KDE packages have been available since 3.0.1 publicly (previously the packages were unofficial, and only if you knew where to find them, which involved asking the maintainer), and XFree86 4.2 packages are available.\n\nYou, sir, are an idiot."
    author: "Debian user"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-07-05
    body: "So, where are the official packages? Why aren't those packages in Sid? I have known that there are KDE3-debs floating in the net, but why aren't they in Sid?\n\n\"You, sir, are an idiot.\"\n\nThank you, I love you too."
    author: "Janne"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-07-05
    body: "Hmm, the maintainers seem to be very busy with the release of woody at the moment... that's why some things take more time at the moment.\n\n--\n\nStephan\n"
    author: "Stephan Hachinger"
  - subject: "Re: Deb packages for 3.0.2. are available"
    date: 2002-11-27
    body: "if there is somebody who tries to help accomplishing a task / helping others (as for the debian maintainers, who offer their freetime working on that unpaid) and \"give the other users a helping hand\" some people tend to be unthankful idiots who try to rip the arm. \n\nIf it takes too much time for you, Janne, choose an commercial package like redhat, then you can ask them what they did with your money.\n\nYours,\n\nLMH"
    author: "lmh"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "If you paid any attention to RedHat, they take a stance very simmilar to Sun and other major for-profit *nix vendors. The only release updates if there is a damned good reason (i.e., a bug/security fix). The latest OpenSSH vunerability is a good example. They did not release the fixed OpenSSH 3.4 to RedHat Network (and their FTP site). They back ported it to OpenSSH 3.1p1 (this is RedHat 7.3). They take great pains to ensure that customers can depend on a very stable, bug/security vulnerabilty free server. They have not (and likely won't) release GNOME2 updates, either. Now you may be able to install GNOME2 out of RawHide, but that is at your own risk. The same appies to KDE."
    author: "forehead"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "> The only release updates if there is a damned good reason (i.e., a bug/security fix).\n\nhttp://www.kde.org/info/3.0.1.html lists security problems, so RedHat should release updates."
    author: "Anonymous"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "Ah well,\n\ndon't blame Bero or RedHat...they do quite nice work. \nBero is the only one who packages kde rpms for redhat...but this is not his main work.\nafter all, kde is not the default desktop for RH...it's gnome.\nBut, why not building kde rpms for rh7.3 by yourself ? it's quite easy and sometimes more stable than the rh provided rpms...\n\n\\sh"
    author: "Shermann"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "> after all, kde is not the default desktop for RH...it's gnome.\n\nno, i think the default attitude towards desktop from RH is yelling around \"Linux is not ready for the desktop\", as their CEO did, and as they're server-oriented business...\n\nThe truth is:\n\"Linux is ready for the desktop, RedHat is not\""
    author: "loopkin"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "loopkin,\n\nwhat Matthew say is not the truth of the world.\nIn general, Unix was ready for the desktop since Emacs was born ,-)\nLinux was ready for the desktop, since StarDivision decides to port StarOffice to Linux.\nThe problem is, we don't have a real lobby.\nIt's right, that RH is server-oriented, 'cause there is much more money to make in the moment.\nBut believe me, in one or two years Linux will come to the desktop (I hope we have then translucent support under X ;))) with a thunderstrike...believe me\n\n"
    author: "Shermann"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-06
    body: "<i>The truth is:\n\"Linux is ready for the desktop, RedHat is not\"</i>\n\nYou'll be surprised how many people still think that Linux is not ready for the desktop.\nTheir arguments:\n- None (simply claim that Linux isn't ready for the desktop).\n- Too hard to learn and configure (\"WHAT? I have to use obscure Unix commands like xf86cfg?\"?).\n- Too hard to install software (\"Don't you get it? I shouldn't HAVE to type in ./configure!!!!\", or \"Yes, I spent a whole day just to figure out how to install an RPM!\")\n- XP blows GNOME and KDE away.\n- \"Linux is currently at a state where Windows 3.1 was.\""
    author: "Stof"
  - subject: "More reasons"
    date: 2002-07-09
    body: "I've also heard:\n\n-desktop environments crash too much\n-poor support for new hardware\n-lack of third party applications\n-lack of fully featured web browser\n-relearning shortcuts, tricks, etc. makes switching difficult\n-poor cut and paste support\n-lack of integration of applications\n-Windows XP is much prettier"
    author: "Raymond"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "\"after all, kde is not the default desktop for RH...it's gnome.\"\n\nNot on my install of RH, it isn't!\n\n(I only installed GNOME to get at the libraries that various applications require.)"
    author: "Paul Boddie"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "No you didn't understand me.\nDefault Desktop of RH is gnome, cause they provide to the GNOME community some coders. Same applies to Sun.\n\nSo Gnome is supported better then gnome at RH\n\n\\sh\n"
    author: "Shermann"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I'd like to compile myself, but RH usually applies quite a few patches to kde.\nAre you compiling from srpm, or from .tar?  What config switches do you use?\nI'm running RH7.3."
    author: "nbecker"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "if you want, i can send you my spec files for it.\n\nregards,\n\n\\sh"
    author: "Shermann"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "> But basically, the deal is that RedHat never releases updated KDE updates.\n\nHaven't you been to http://www.RPMfind.net?\n\nMy RedHat 7.3 system, which came with KDE 3.0.0, is now running KDE 3.0.1 thanks to RedHat's update site. Just do a search for kdelibs or whatever package you need, and you'll find official RedHat versions for them.\n\nThere is currently a 3.0.2 version of all the KDE packages, but they're CVS snapshots. I'd expect the final versions to be available within a few days.\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I suspect that you are referring to the \"Rawhide\" RPMS. These aren't official in anyway\n\nFrom the rawhide README:\n\n-------------------------------------------------------------------\n\nRaw Hide Can Be a Bit Tough to Chew on So Run at Your Own Risk (and Enjoyment)\n\nThese releases have not been quality tested by Red Hat's Quality Assurance\nteam. They may not boot. If they boot, they may not install. If they install,\nthey may not do anything other then waste CPU cycles. If anything breaks,\nyou most assuredly own the many fragments which will be littered across your\nfloor.\n\nIt may not be possible to upgrade from Red Hat to Raw Hide, from Raw Hide \nto Red Hat, or from Raw Hide to Raw Hide! If a stable upgrade path is\nimportant to you, please do not use Raw Hide.\n\nDO NOT USE THESE RELEASES FOR ANY WORK WHERE YOU CARE ABOUT YOUR APPLICATION\nRUNNING, THE ACCURACY OF YOUR DATA, THE INTEGRITY OF YOUR NETWORK, OR ANY\nOTHER PURPOSE FOR WHICH A RESPONSIBLE HUMAN WOULD USE A COMPUTER. (But then\nagain what would be the fun of hacking Linux if there wasn't some risk\ninvolved. ;-)....)\n-------------------------------------------------------------------\n\nIt is probably \"safer\" to download the Raw Hide SRPMS and do rpm --rebuild to make sure that it is linked with the libraries that you already have on your system - in this way you will minimize any dependencies that you might otherwise have on the unstable rawhide stuff."
    author: "Corba the Geek"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "Okay, so I guess \"official\" is too strong a word. Perhaps \"official pre-releases\"? \"Official betas\"?\n\nAnyway, my point was that packages are available, and compiling from the source isn't the only option for RedHat users. While the RawHide packages are not intended to be a continuous upgrade cycle, they're quite useful for updating critical packages between releases. For instance, I used them to upgrade from KDE 3.0.0 to 3.0.1 and never had a single problem.\n\nYou say that the RawHide packages have not been tested, and that's true. But none of the other vendors' packages have been tested either. How could they be, since KDE 3.0.2 just came out yesterday?\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "We release to vendors before users (and the branch is always available from cvs) so testing can occur before the release. We use the time delay as a final test in case anything got missed.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "So correct me if I'm wrong, but the rawhide KDE RPMS are compiled with GCC 3.1, which is binary-incompatible with GCC 2.x used for RedHat 7.3.\n\nSo does that mean I'd have to recompile the whole RedHat 7.3 distro from source using the GCC 3.1 compiler in order not to have compatibility problems with the rawhide RPMS?\n\nIf so, I hardly see how having to recompile your distro from source rates as anywhere near as easy as downloading an RPM, which is all you have to do for Mandrake or SuSE."
    author: "ac"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "The latest KDE RPMs are for GCC 3.1, but it's not a major problem. You just have to make sure that two libstdc++ RPMs are installed: one with the 2.96 libraries and the other with the 3.1 libraries. The 2.96 works with your 7.3 distro, and the 3.1 works with any new stuff you've got.\n\nThis is nothing new. Consider what RedHat has done with the Qt library. I can run Qt programs compiled for Qt 2.x as well as Qt 3.x, since I have both the qt and qt2 packages installed. Having two packages makes a lot of sense because otherwise I'd have to recompile everything I had linked to Qt 2.x after upgrading to Qt 3.x.\n\nThere are other examples: python and python2, for instance. RedHat seems to go to a lot of trouble to provide legacy packages that allow old programs to work without recompiling, so I really don't know where all this RedHat-bashing is coming from.\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-05
    body: "i've actually got a working 3.0.2 distro (read post below), but i went through and first did what you're doing w/ both libstdc++ packages installed, and it wouldn't work. then i went through and upgraded libstdc++, glibc, gcc, and much more to the new rawhide versions, also grabbing all the compat- packages so stuff kept working. even then i still got 'libDCOP.so.4: undefined symbol \"some reasonable symbol w/ garbage on the end\"' when running apps. \n\nat this point, i don't really care, it works, but however you found to get it working doesn't seem to work for everyone. your points about having multiple versions of packages installed is true, but in this case it seems to have not been done correctly. \n"
    author: "john"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-06
    body: "At the very least, you'd have to update FAM. Having libraries in memory that link to different version of libstdc++ is a very bad idea. "
    author: "Sad Eagle"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "how did you manage this? i've been trying to get the rawhide kde3.0.1 packages to run, and they keep on puking on linker issues (the munged symbol names don't match up). \n\nany clue on how to get around this?\n"
    author: "john"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I think a previous commenter answered this.  I presume you're trying to use the binaries.  These were compiled using gcc 3.1.  They will not work on a 7.x based system without reasonably significant other upgrades"
    author: "Rw"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "right, i actually did go through the whole process of upgrading to the rawhide libstdc++3, gcc 3.1, glibc, plus all the compat-* pacakges. after much dependency graph walking, i got them all installed, and the kdebase/kdelib 3.0.2 binaries *still* would not run.. \n\ngrrr... its so irritating..  \n\ni'm a long time redhat fan, and this might actually push me from their distro.\n"
    author: "john"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "This incompatibility isn't RedHat's fault, although driving their poor users to the extreme of mix-and-matching RPMS is.\n\nWhen mixing rawhide RPMS into your non-rawhide software, you've got to make sure they're binary compatible versions (check the major revision number: 7.1 and 7.2 are binary compatible, 7.3 and 8.0 are not).  The next RedHat will be 8.0, the major version change indicating broken binary compatibility.  Don't mix and match major version numbers.  It's bad.  And if you don't know what the next version number is gonna be?  Ask.  (Okay, so everyone [including me] though 7.3 was gonna be 8.0.  And www.distrowatch.com is calling the next RedHat release 7.4, but I'm right this time.  The next RedHat is 8.0)\n\nBut the same could be said for any distro.  Every development version of every major distro is switching to GCC 3.1 (because it's better) but they're also breaking backwards compatibility at the same time.  So don't throw Mandrake development binaries into your current Mandrake box or you'll break it, the same way you just broke RedHat.\n\nOf course, Mandrake users don't have to RESORT to pulling RPMS off their development versions.  But that's another story."
    author: "ac"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "I'm not sure that the binary compatibility issue is such a big problem. Currently I've got two versions of the libstdc++ package installed: 2.96 and 3.1, and they coexist without any problems that I can see. My RedHat 7.3 programs link with 2.96 and the RawHide ones link to 3.1. There seems to be a pretty clean upgrade path.\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "I can't remember what all packages I had to install. Send me an email with the errors you're getting and I may be able to help. I can also give you a list of my installed packages if you'd like.\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "redhat sucks because it doesn't provide packages. Yes, I'm compiling now but that's not the point. Got to find some time and dump redhat. 4th of July might be the perfect time! Gentoo should be the lucky one."
    author: "abemud"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "Yeah, DeadRat sucks really bad.\n\nGentoo or LFS are good choices."
    author: "annonymous"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "> redhat sucks because it doesn't provide packages.\n\nDoesn't *anybody* here use rpmfind.net??? You can get updated KDE packages there. Refer to my previous post.\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I suppose you haven't tried installing those RPMS, Trevor.  I'd like to know what sort of difficulties you get from running GCC 3.1-compiled binaries on an otherwise GCC 2.96RH-compiled system.  If it actually works, that's a simple solution.  However, I have a hunch using rpmfind.net in this case is the fastest possible route to an unusable system."
    author: "ac"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "I've done it already. All I know is that it works for me. Refer to my other messages for an explanation of why.\n\nTrevor\n"
    author: "Trevor"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-03
    body: "I'a a rawhide user... and kdebase-3.0.2-0.cvs20020627.1.i386.rpm and kdelibs-3.0.2-0.cvs20020625.1.i386.rpm are available for at least a week.... let's see the final 3.0.2\n\nI don't think I'll wait too long..."
    author: "Marcos Tolentino"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "I *DUMPED* RedHat yesterday. These guys are operating in a \"corporate level\". I just been to a job interview for computer technician vacancy where they cleared that they don't do work for home users because they're just time consuming and non profit. Companies like that.\n\nI now use Conectiva Linux 8.0\nI am a proud brazilian. Also 5 times World Cup champion. "
    author: "DBArros"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "> I am a proud brazilian. Also 5 times World Cup champion.\n\nYou are a soccer professional and played in Japan?"
    author: "Anonymous"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: ">> I am a proud brazilian. Also 5 times World Cup champion.\n\n>You are a soccer professional and played in Japan?\n\nYou would never understand what that means...\n\n[]\u00b4s\nanother proud brazilian"
    author: "Marcos Tolentino"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: "> Also 5 times World Cup champion.\n\nReally? I thought that the last few winners were UK, France and Belgium. I don't recall Brazil entering the Tiddilywinks World Cup at all.\n\nUnless you meant a different 'World Cup'.. you should really specify which game."
    author: "Damocles"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-05
    body: "Dude, what the hell is Tiddilywinks?\n"
    author: "Carg"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-05
    body: "It involves men in bars wearing skirts and eye contact.  I think Mosfet has a page about it.\n\n:-P  (And Mosfet is cool by me - I've been doing Rocky Horror for long enough I didn't bat an eye when I saw his pics.  Otherwise, that would have been tiddilywinking, I think.)\n\nOh, and... Alt-F2 and typing gg:tiddilywinks will get you the answer.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-05
    body: "> Dude, what the hell is Tiddilywinks?\n\nPlayers take it in terms to push the edge of one plastic disc onto the edge of another and 'ping' the latter disc into a cup. The winner is the first to ping an agreed number into the cup.\n\nSee http://directory.google.com/Top/Games/Hand-Eye_Coordination/Tiddlywinks/?tc=1 for details."
    author: "Damocles"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-04
    body: ": I am a proud brazilian. Also 5 times World Cup champion. \n\nY'know, if you're a sports star, you might think about using your name to promote KDE.  Even if you just mention it in interviews, it would look good.\n\n--\nEvan \"Who is less dense and more ironic here\""
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-05
    body: "Every single Brazilian in the world are PENTA!\n"
    author: "Carg"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2002-07-05
    body: "*YAWN*"
    author: "Anonymous"
  - subject: "Re: Kde rocks, redhat sucks"
    date: 2004-10-29
    body: "I have used redhat since 5.2 days and I have seen it get progressively worse.\nMy main gripe is there changing of packages and abandoment of others\nchange is good, yes but why get rid of basics....pine, mc...for a few.  KDE is my preference, but redhat kde is no good.  I use suse now and do not regret the switch.  "
    author: "JAS"
  - subject: "Which Game?"
    date: 2002-07-03
    body: "The SuSE \"SORRY\" file is amusing, but which game are they referring to?  Wimbledon, I'd guess...\n\n--\nEvan\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Which Game?"
    date: 2002-07-03
    body: "They refer to the world champinship soccer final. I would guess yul live in the U.S.A?"
    author: "Anony Mouse"
  - subject: "Re: Which Game?"
    date: 2002-07-03
    body: "Silly me, I thought it was finished, and Brazil won.  Maybe I'm underestimating how long it takes to get these packaged or overestimating how long they have the source before they have to have it done.  :)  \n\nWhile I played football when I was a young kid (forward, mostly), I got much more into cricket in my teens, and now I don't really follow any sports.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Which Game?"
    date: 2002-07-03
    body: "<i>While I played football when I was a young kid (forward, mostly), I got much more into cricket in my teens, and now I don't really follow any sports.</i>\n\nI guess that's the whole point. If you live outside the USA you dont have to follow sports in order to know the football results. If there is a goal you HEAR it (unless you dont have any neighbours). If your countries team or one of the larger ethnic minorities in your country wins, you can see it on the street because of the cars that drive through the streets, blowing their horns and waiving their countries flag out of the window, for at least one day. Just after the final even germanies most staid TV news magazine only reported about the world cup in the first 10 mins of the 15 min show. Heck, after Italy lost even their parliament debated whether they should fire the coach..."
    author: "AC"
  - subject: "Re: Which Game?"
    date: 2002-07-03
    body: ":: If you live outside the USA \n\nAhem.  Who said I live in the US?\n\nFor that matter, I have no idea when the SuperBowl is, or the World Pennant or whatever for baseball, and I lived in Florida, USA for quite a long time.  Nor did I ever know the winners, although I did see plenty of people flying flags on their cars and such.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Which Game?"
    date: 2002-07-07
    body: "People watching around the world:\n\nSuperBowl - 100 million\nFIFA World Cup final - almost 2 billion\n"
    author: "Carg"
  - subject: "Re: Which Game?"
    date: 2002-07-04
    body: "Or, when your team loses, your car parked outside is doused in gasoline and several maltov (sp) cocktails are thrown on it just for kicks.  Or, better yet, if one of your players make a mistake, you hear about the murder in the morning newspaper.\n\nInternational sports is just that, international.  There's always politics involved, it sucks.  Oh my god, Americans made it to the 8th place, \"Yeah! We'll show them we're good at every sport!\"  Or, \"American sucks!\"  \"Die America!\"   Blah blah blah blah blah...  whatever...\n\nThe fact is that we're all on the same boat: Earth.  Okay?  I'm so sick of all the pain and suffering that goes on, just because of someone's political agenda.  Or, worse, to save face from a publically made comment.  And don't tell me that China/Asia is solely afflicted with this, it's all over...\n\nOut."
    author: "Bill"
  - subject: "Re: Which Game?"
    date: 2002-07-04
    body: "I guess you do to? You do know the game is called football everywhere else, right?\n"
    author: "Rob Kaper"
  - subject: "Downloading packages"
    date: 2002-07-03
    body: "I notice from the changelog that only arts, kdelibs and kdebase have changes.  \n\nDoes this mean that I can get away with just upgrading these 3 packages or do I have to download and reload the whole lot again?"
    author: "Charles Gauder"
  - subject: "Re: Downloading packages"
    date: 2002-07-03
    body: "> I notice from the changelog that only arts, kdelibs and kdebase have changes. \n\nChangelog is wrong. Developers are missing discipline to log everything, every package has changes.\n\n> or do I have to download and reload the whole lot again?\n\nPerhaps the same dot reader as usual will create and offer diff files?"
    author: "Anonymous"
  - subject: "Re: Downloading packages"
    date: 2002-07-03
    body: "Simply take the diffs (3.0.1-3.0.2). They are in experimential state, but feel free to try them. http://devel-home.kde.org/~danimo/diffs/.\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "What I'd like to see..."
    date: 2002-07-03
    body: ".. is a unified way for KDE, GNOME (and Wine?) apps to add things to the traybar.\nIf I run korganizer in GNOME or gnomeicu in KDE, just to make two examples, the traybar-icons doesn't show up in the traybar, or they open in a separate small window.."
    author: "Wigren"
  - subject: "Re: What I'd like to see..."
    date: 2002-07-03
    body: "We've already discussed an extension to the NET WM spec for exactly this. Expect the problem to disappear RSN.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: What I'd like to see..."
    date: 2002-07-03
    body: "strange, when i used everybuddy it put'd its tray icon just fine in my kde tray..\n"
    author: "kervel"
  - subject: "Finally, they 've been killed (those pesky bugs)!!"
    date: 2002-07-03
    body: "Waouw, this release is such an improvement over the previous one(3.0.1).\n\nThe ksplash crash... disappeared\nNoatun... finally working (first time on my machine)\nAn annoying Ark bug when closing .tar.gz opened via konki... disappeared\nKonki... faster when opening a new windows with the middle button\nKghostview crash when PgDowning too fast... disappeared\n\nand this gives the ultimate desktop experience on linux...\n1000 TX for all this worK to KDEckers\n"
    author: "seb"
  - subject: "Great"
    date: 2002-07-03
    body: "Yes, kde is great and many bugs are gone.\nNow I will wait for 3.0.3 and I hope it fixes the save file in kate over fish/ssh bug :(\n\nhalux"
    author: "halux"
  - subject: "Re: Great"
    date: 2002-07-03
    body: "I can do that with current cvs release, so it seems to be fixed.\n\n\n\tbeutelin"
    author: "beutelin"
  - subject: "Re: Great"
    date: 2002-07-03
    body: "What cvs branch are you using. I am using KDE_3_0_BRANCH.\nUpdated today."
    author: "halux"
  - subject: "Re: Great"
    date: 2002-07-03
    body: "Do you mean the error message that says  \"This file could not be saved.  Please check if you have write permission ?\"  when you attempt to save file using fish ?  If so, it has been annoying me as well ; so I will see if I can take care of it.\n\nDawit A."
    author: "Dawit A."
  - subject: "Re: Great"
    date: 2002-07-03
    body: "Yes, that is the message.\nThank you very much.\n\nhave fun\nFelix"
    author: "halux"
  - subject: "Re: Great"
    date: 2002-07-05
    body: "Someone already beat me to it :)  It is fixed in the current \ndevelopment branch for KDE 3.1.\n\nRegards,\nDawit A."
    author: "Dawit A."
  - subject: "Re: Great"
    date: 2002-07-07
    body: "Cool, it works, thanks.\n\nWill there be a backport ?\n\n\nhave fun\nFelix"
    author: "hal"
  - subject: "Source diffs"
    date: 2002-07-03
    body: "For those that want them, source diffs will shortly be available at http://www.hmallett.co.uk\n\n-- \nHywel Mallett"
    author: "Hywel Mallett"
  - subject: "Searching in KDE help center"
    date: 2002-07-03
    body: "What has happened to the useful htdig search of documentation using the helpcenter?\nI seem to remember it was available back in the KDE 2 days. \n\nI find the current help center hard to use. I hate hierarchical menus---especially the default Mandrake ones---as its never clear which category something should be in.\n\nI think one of the best features in kcontrol is the way you can type in keywords to find the relevant options.\n\nAnyway, this isn't a particular criticism - I think KDE3 has been excellent and am looking forward to installing this new release."
    author: "James Marsh"
  - subject: "Re: Searching in KDE help center"
    date: 2003-03-30
    body: "1'st of all I like KDE, and it's getting better all the time. But, if you are ever gonna beat Microschaft, you must  develop a context sensitive help system?\n\nBecause dummies like me need it.\nIt's way too hard to find anything with your current menu system.\n\nPeople like me need help with simple things like finding files, a simple way to install programs, and having the programs that are installed appear on the menu bar. I struggled to get the program installed, and now I can't find it.\n\nSure you probably think I'm stupid, but I'm one of thousands or more who want to leave Windows, but I don't have the time to hack my way thru every obstacle that Linux presents.\n\nWhen you people have a Linux question, (and I know all of you do from time to time,) you just ask the person in the next room or wherever.\n\nBut people like me are alone in our quest......\nNo one to ask.......\nAnd after a weekend of frustration, it's time to give up and go back to Windows again......\nThis is the 6'th time I have tried to get Linux to work for me...\n\nCan't you see?\n\nSincerely,\nGene"
    author: "Gene A."
  - subject: "***AWSOME! A Solution for Redhat Users!!!***"
    date: 2002-07-04
    body: "check out:\nhttp://www.math.unl.edu/~rdieter/\n\nThis guy kindly has compiled and uploaded rpm packages of kde 3.0.2 for redhat.\n\nIf you run apt-get (check freshrpms.net if you don't)\n\njust add\n\nrpm     http://www.math.unl.edu/linux/redhat/apt 7.2/$(ARCH) kde3\n\nto your sources.list, then apt-get update/upgrade\n\nenjoy! *TONS* of thanks to Rex Dieter!\n"
    author: "john"
  - subject: "Re: ***AWSOME! A Solution for Redhat Users!!!***"
    date: 2002-07-04
    body: "I love smart people. They make everything so easy for me."
    author: "ac"
  - subject: "And it works, too!"
    date: 2002-07-04
    body: "I just installed the RedHat RPMS and they work great!  Plus, two of my most hated bugs are gone! (documents saved to the desktop were positioned below the bottom of the screen and dropdown menu arrow click area was way too small)\n\nThe only odd thing is that my Keramik theme titlebar now looks more drab than it used to.\n\nThanks Rex Dieter!  I wanna have your babies!"
    author: "ac"
  - subject: "Re: And it works, too!"
    date: 2002-07-05
    body: "You're welcome.  (-:"
    author: "Rex Dieter"
  - subject: "Re: ***AWSOME! A Solution for Redhat Users!!!***"
    date: 2002-07-06
    body: "Many thanks to Rex Dieter for the 3.0.2 binary RPMs from another RedHat user!  I was beginning to think that switching to another distribution was the only way of getting the latest KDE made as binaries.  I don't have the time to sit waiting while my system wades through compiling megabytes of source code, so binary RPMs are really important.  There were some bugs in 3.0.0 like https not working in Konqueror and some cut and paste problems in the kmail editor which were annoying and had me looking out for an update."
    author: "David Herbert"
  - subject: "God Bless you :)"
    date: 2002-07-10
    body: "Dear Rex Dieter,\n\nThank you very much. it's people like you who make us feel good by helping others. thanks."
    author: "Asif Ali Rizwaan"
  - subject: "Re: ***AWSOME! A Solution for Redhat Users!!!***"
    date: 2002-07-13
    body: "Thank you very much from Germany!\nGreat work!\n\nCliff"
    author: "Cliff"
  - subject: "Re: ***AWSOME! A Solution for Redhat Users!!!***"
    date: 2002-08-04
    body: "Caro Rex Dieter, (sorry if I don't speak english)\ntante grazie anche dall'Italia. Hai reso felice un altro RedHat dipendente.\nSaluti \nMamagio\n\nDear Rex,\nmany tanks from Italy. You have be happy another RedHat user.\nBye"
    author: "mamagio"
  - subject: "Most hated bug #3 still here..."
    date: 2002-07-04
    body: "Damn, not perfect yet!\n\nTooltips STILL prevent you from clicking what's underneath them.  Since they insist on popping up just as you're about to click, they should at least have the courtesy of being click-transparent!\n\nOther than that, though, very nice release."
    author: "ac"
  - subject: "Re: Most hated bug #3 still here..."
    date: 2002-07-04
    body: "Yes! That's really annoying... I end up turning off tooltips, but, then again, need them for programs I don't know what the icon is doing...\nI hope it get's fixed.\n\nBTW, where can you get the list you said of most hated bugs?"
    author: "JL Lanbo"
  - subject: "Re: Most hated bug #3 still here..."
    date: 2002-07-04
    body: "Heh, the list I've been referencing is in my head ;-)\n\nHowever, we have a new candidate: Konqui filemanager can't follow symlinks to folders???"
    author: "ac"
  - subject: "Re: Most hated bug #3 still here..."
    date: 2002-07-05
    body: "Disregard the symlink \"bug\".  Konqui can't follow symlinks to folders IF YOU DON'T HAVE THE RIGHT PERMISSIONS.  So, er, it's not really a bug.  Well, maybe a \"there could be a more descriptive error message\", but that's splitting hairs.\n\nBut that tooltip thing, THAT'S a real bug.  Annoying bugger at that."
    author: "ac"
  - subject: "Re: Most hated bug #3 still here..."
    date: 2002-07-05
    body: "I too have disabled tooltips. I get the information I need in the status bar anyway. Most of the information in the tooltip is also in the status bar. The more I think about it I think tooltips for files should be disabled by default. At least make the delay longer, you should only get a tooltip when you really want a tooltip, not just when you move your mouse slowly over a bunch of files."
    author: "Joe"
  - subject: "No antialias in SuSE-7.3 packages"
    date: 2002-07-04
    body: "The QT in this release, at least for SuSE-7.3 doesn't support antialiased fonts nor icons... Had to downgrade QT to the one that came with KDE-3.0 to get desktop back to it's normal glory."
    author: "Roope"
  - subject: "Re: No antialias in SuSE-7.3 packages"
    date: 2002-07-05
    body: "This is because SuSE in all their glory decided to shut off Xft support in QT3 for all 7.3 users because there's a bug with it when used with XFree86 4.1.\n\nHowever, if you're one of the smarter ones and have upgraded to XFree86 4.2, there's no bug.\n\nBut still, you're not given that option if you have 4.2, they packaged it without Xft support for all 7.3 users because of this.\n\nPackager is adrian@suse.de . If you have XFree86 4.2 and want anti-aliasing back, tell her! This makes no sense \"punishing\" everyone using 7.3 with a nasty-looking desktop just because of idiots who won't upgrade X. Who in the hell is upgrading KDE without upgrading X as well?"
    author: "Someone Else"
  - subject: "Re: No antialias in SuSE-7.3 packages"
    date: 2002-07-06
    body: "This glory was enforced by people who are not reading READMEs, but are able to spam. More spam will not help anybody. And I am a him, newbie."
    author: "Adrian"
  - subject: "I have try it..."
    date: 2002-07-04
    body: "Konqy is looking way faster in displaying web pages. But it still need 4 to 5 seconds to start (K6 550, 256Mb,Mandrake 8.2 + Mandrake package for 3.0.2).\nBut there is still two big bugs that shows in the first minute of use.\n\n1) When I click on the Konsole icon in the bar, the icon is \"frozen\" and can not be used anymore as long as the konsole programm is not closed (is it a mandrake bug??)\n\n2) When I am using konqy as a file manager, I like to view the files with all the information (size, date, permission ...). In this mode, when I select several files and when I drag then to move then in another directory, the selection changes as I am \"flying\" over other files. This bug is just visual but terribly ugly. I think that it exists since (at least) Kde 2.0.\n\nThe increase in speed for Konqy is a good new. I hope the project will continue in this direction."
    author: "Murphy"
  - subject: "Re: I have try it..."
    date: 2002-07-04
    body: ">) When I click on the Konsole icon in the bar, the icon is \"frozen\" and can not be >used anymore as long as the konsole programm is not closed (is it a mandrake bug??)\n\nYes that is (partly) a mandrake bug. Somebody was stupid enough to create a konsole-xft in kde 2.2.x and if you upgraded from kde 2.2 you still have that\nentry in the kicker panel. Kde3 doesn't have any konsole-xft so kicker has a problem. The KDE bug here is that kicker cannot handle this gracefully.\n\nchange the entry into konsole and everything should be ok\n\nd"
    author: "danny"
  - subject: "Re: I have tried it..."
    date: 2002-07-04
    body: "Thanks, it works.\n\nAnd for the other bug, have you an idea why it is still there?\n\nBy the way, does someone know why 'alt F2' does not work any more??\n\nMurphy"
    author: "murphy"
  - subject: "Re: I have try it..."
    date: 2002-07-05
    body: "Are you using GLibc 2.2.5? Objprelink isn't exactly perfect at times, and glibc 2.2.5 is a tad faster than it since it's a real solution. Be warned that upgrading to glibc 2.2.5 pretty much takes a completely new distro version to support it (unless you're really tricky)."
    author: "Nick Betcher"
  - subject: "Re: I have tried it..."
    date: 2002-07-05
    body: "Well, thanks for the advice but I am not very tricky. So I will wait for the next Mandrake..."
    author: "murphy"
  - subject: "new RedHat beta ships with KDE 3.0.2 !"
    date: 2002-07-04
    body: "The new Red Hat Beta (codename Limbo), which according to the release notes\ncontains \"the latest desktop technology\" (no more details - tasty understatement ;-)\nis in fact bundled with a KDE 3.0.2 snapshot !\n \nRead the full annouce here : http://linuxtoday.com/news_story.php3?ltsn=2002-07-03-019-26-NW-RH-SW\n\nKudos to Red Hat (and most probably to Bero) for this one\n\nG."
    author: "germain"
  - subject: "Re: new RedHat beta ships with KDE 3.0.2 !"
    date: 2002-07-05
    body: "They're fibbing just a little.  If you actually LOOK at the RPMS in the beta, you'll see they are still mostly 3.0.1.  I'm sure they'll be fully updated to 3.0.2 for the next beta/release though..."
    author: "Rex Dieter"
  - subject: "Debian Pkgs"
    date: 2002-07-04
    body: "I can't wait for the debian packages to come out in 2005!"
    author: "Sam"
  - subject: "Re: Debian Pkgs"
    date: 2002-07-05
    body: "Or, how about you stop being an idiot, and start looking at http://calc.cx/kde.txt? These packages have been out since just before 3.0.1, and are even announced on DebianPlanet. Before you bitch and moan (just like dep) about it not being in sid, the maintainer chose not to upload them to save people like yourself the trouble of a fucked-up upgrade."
    author: "Debian user"
  - subject: "Re: Debian Pkgs"
    date: 2002-07-05
    body: "Do you know when it will be officially ported to sid?\n\nDave"
    author: "gibby"
  - subject: "Re: Debian Pkgs"
    date: 2002-07-05
    body: "When hell freezes over."
    author: "Nick Betcher"
  - subject: "Re: Debian Pkgs"
    date: 2002-07-09
    body: "Hmm...Last I checked, the KDE team provided packages of text, which most of us would call Source tarballs! (You may have heard of them.  Judging from many posts here, not many KDE users have, but I could very well be wrong, and hope that I am :) )  So, what you could do is stop trolling and BUILD YOUR OWN!  Hell, people may actually start taking you seriously if you do.  Hmm...What a concept!"
    author: "Anti Troll"
  - subject: "KDE rules!"
    date: 2002-07-05
    body: "With all the people out there who bash KDE flaming on and on, I'd like to take a second to express my sincere appreciation to all the KDE developers.  KDE rules thanks to all you guys.  You have given us *nix users a truly powerful and unique gift.  I'll never use Gnome or MS Windows again... I mean that!\n\nKeep on rockin'... you ARE appreciated!\n\nJonathan Graham\nLinux User since 1999\n"
    author: "Jon"
  - subject: "Re: KDE rules!"
    date: 2002-07-09
    body: "I second this!  Although I don't use KDE (and probably never will again (don't ask, plz)), I do appreciate all the work that has gone into the DE.  The environment is clean and polished, even though I do think some work could be done to speed things up (probably more on troll-tech's end tho).  Keep up the good work, I reccommend this to anyone who is switching to Linux :)"
    author: "Justin Hibbits"
  - subject: "Xinerama bugs still present."
    date: 2002-07-05
    body: "Tried out 3.0.2 but it still has major xinerama bugs. The panel doesn't freeze straight away anymore, but it does freeze after any of the buttons are clicked and the kde control center still wont run in xinerama mode. These a major bugs and they should have been fixed by the second maintenance release. Poor effort people! If these bugs aren't fixed by the time the next generation of distributions comes out with kde3 then kde could lose its entire xinerama userbase."
    author: "Jared"
  - subject: "Re: Xinerama bugs still present."
    date: 2002-07-05
    body: "> Poor effort people!\n\nMaybe do something about it: Donate graphic cards and screens to your favorite KDE developer. \n\nOr even better, look at the problem yourself. Do some debugging. You didn't even post a _detailed_ bug report."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Xinerama bugs still present."
    date: 2002-09-21
    body: "True... but it's not like it's hard to reproduce... try running control center with dual monitors or konsole... they will not run.  Windows don't repaint and apps behave strangely...  would be nice to get a fix... pleeeeaassee"
    author: "Thomas Mayer"
  - subject: "Re: Xinerama bugs still present."
    date: 2002-07-05
    body: "I saw a xinerama entry for kde 3.1. From the feature list:\nXinerama configuration module and more correct support, George Staikos and others\n\nAt the moment red (in kdelibs).\n\n\nhave fun\nFelix"
    author: "halux"
  - subject: "Re: Xinerama bugs still present."
    date: 2002-07-05
    body: "Although it's red, I think he is working on it as we speak. If I'm not mistaken there were some more Xinerama fixes just committed in the last few days."
    author: "Nick Betcher"
  - subject: "Re: Xinerama bugs still present."
    date: 2002-07-05
    body: "thanks to puetzk putting in the necessary efforts and others helping him test and debug it under various circumstances, kicker is now xinerama aware, functional and safe."
    author: "Aaron J. Seigo"
  - subject: "why so different distributions?"
    date: 2002-07-05
    body: "  looking on ftp.kde.org for binary packages i noticed that every version is available for totally different distributions:\n\n3.0.0 : FreeBSD, Irix, RedHat, YellowDog\n3.0.1 : Gentoo, Slackware, Solaris, Turbo\n3.0.2 : Conectiva, Mandrake, SuSE, Tru64\n\n  is there a reason for this? every version makes some (few) people happy, but eventually everybody will be happy (at least once :) ). rigth now i'm not quite happy, i'll have to spend at least a day compiling the latest version ... :|\n\n.costin"
    author: "costing"
  - subject: "Re: why so different distributions?"
    date: 2002-07-05
    body: "Because the binary packagers aren't always available to package for each distro, so some distros (and other OS's) suffer at certain times. If you know how to package, become an official package manager for KDE... help out! :)"
    author: "Nick Betcher"
  - subject: "Re: why so different distributions?"
    date: 2002-07-05
    body: "kde releases source, vendors release binaries.\n\neach distribution/vendor (or their userbase) is responsible for providing binaries for their platform. not all distributions build binaries for every revision release. if you don't like how your vendor is handling this, get in touch with them and/or help build packages for that platform.\n\n"
    author: "Aaron J. Seigo"
  - subject: "What about one LSB-package in the near future?"
    date: 2002-07-05
    body: "Over about a few months the first distubutions come out wich will follow the LSB-moddel (LSB = Linux Standard Base). Soon all major distibution will follow. \nAt that time there is no need four al those differen binarys. \n\nPerhaps it might be a good idea for KDE to put out their own version, based on  standard LSB-rpm's.\n\n"
    author: "Maarten Romerts"
  - subject: "Re: why so different distributions?"
    date: 2002-07-05
    body: "Small correction -- Gentoo is on the 3.0.2 release as of, oh, yesterday.  Gotta love source-based package systems :)"
    author: "dwyip"
  - subject: "Re: why so different distributions?"
    date: 2002-07-06
    body: "SuSE has always packages ... check your list .."
    author: "micha"
  - subject: "Re: why so different distributions?"
    date: 2002-07-06
    body: "I guess he didn't see SuSE packages for 3.0[.1] because they were removed from FTP to save space."
    author: "Anonymous"
  - subject: "I don't mean to sound stupid but..."
    date: 2002-07-08
    body: "How do you change the default browser in KDE?  The reason I'm asking is that I just switched from Mozilla mail to KMail because KMail understands my file associations.  However, I still prefer Mozilla to Konqueror for regular web browsing.\n\nWhen I click on a link in a KMail message it used to open Konqueror.  I switched the file association for text/html to Mozilla, and now the link KINDA opens in Mozilla--but not entirely.\n\nMozilla opens of a cached copy of the link on the local drive, not the actual link!  And if the site serves cookies, Konqueror asks me if I want to accept/reject the cookies before Mozilla even tries to do anything.\n\nSomehow I think I'm missing something.  Can anyone help?"
    author: "ac"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2002-07-10
    body: "Just right-click on any html file and choose 'edit file types' there you can move mozilla up. or more easy will be to 'when opening an html file' choose 'open with' and choose 'mozilla' if not present enter the 'mozilla' command and click on 'remember file association'"
    author: "Asif Ali Rizwaan"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2002-10-22
    body: "Hm, I did just that, mozilla is on top, but still, I see the behaviour described in the parent post... I must admit that I've tweaked it a bit, it says \nmozilla -remote openURL(%u,new-tab) &\nit is _really_ important that it opens in a new tab... \n\nPreferably, what it should do is to open mozilla if it isn't allready running, if it is running, open in a new tab. I guess a shell script is needed to achieve that, anybody got something like that?"
    author: "Kjetil Kjernsmo"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2003-01-04
    body: "I just wrote such a script this very night!\n\nIt's written in Perl, and it simply strips\nthe single quote characters from the beginning\nand end of the %u input string that KDE calls\nmozilla with.\n\nI named it:\n\nkdeLoadMozilla\n\nAnd it can be called with the following syntax:\n\nkdeLoadMozilla %u new-window\n\nor\n\nkdeLoadMozilla %u new-tab\n\nInstant mozilla satisfaction!\n\nLet me know if you still want it.\n"
    author: "Jesse D. Guardiani"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2003-02-13
    body: "I've decided to make this script available for download via anonymous FTP.\n\nYou can get it here:\n\nftp.wingnet.net/pub/scripts/kde/kdeLoadMozilla\n\nEnjoy!\n\n"
    author: "Jesse D. Guardiani"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2003-04-07
    body: "Thanks!"
    author: "Nacs"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2003-05-02
    body: "When you set up the command for KMail to run in your file association, simply put:\n\n/usr/bin/mozilla -remote \"openurl(%u,new-tab)\"\n\nThe openurl thing needs to be in quotes to work.  Hope this helps."
    author: "Brian"
  - subject: "Re: I don't mean to sound stupid but..."
    date: 2003-09-16
    body: "The link given above seems to be down.\nHere's a solution using the script :\n\n#!/bin/sh\nMOZILLA=\"/opt/MozillaFirebird/bin/MozillaFirebird\"\nif $MOZILLA -remote \"ping()\" 2>/dev/null\nthen\n        echo \"Mozilla already launch\"\n        location=\",new-tab\"\n        exec $MOZILLA -remote \"openURL($1$location)\"\nelse\n        exec $MOZILLA \"$@\"\nfi\nexit 1\n\nChange MOZILLA acording to your need. \nThen put this in your file associations :\n/path/to/fire.sh %u\n\nMick\n"
    author: "mick"
  - subject: "URGH.. confusing "
    date: 2002-08-07
    body: "I am running Red Hat 7.3, I'm having a horable time getting my DVD drive to work and my flakey sound.\n\nI'm a windows convert. Please dont bash me. Least I'm trying here to get away from Microsoft. These packages are totaly confusing.. Cant they make a single installer to upgrade? This package wants that installed first.. OIE... I'm trying but its just so cryptic. \n\nPlease help me!!!\n\n"
    author: "mario"
---
The KDE Project today announced the availability of KDE 3.0.2,
the second maintenance release
of the KDE 3.0 series.  <a href="mailto:mueller@kde.org">Dirk Mueller</a>,
the KDE 3 release coordinator, explained that
"<em>a number of stability and useability enhancements have been
backported from the active KDE 3.1 branch to the KDE 3.0 codebase and
bundled in this update. We recommend that all KDE 3 users update
to this newest, stable release.</em>"  More details are in the
<a href="http://www.kde.org/announcements/announce-3.0.2.html">announcement</a>,
or jump directly to the <a href="http://download.kde.org/stable/3.0.2/">download directory</a>.
Congratulations to the KDE developers on another great release!  And stay tuned:
the first KDE 3.1 alpha release, this one complete with a slew
of cool new features (such as the widely requested tabbed browsing
in Konqueror), is due out next week already!



<!--break-->
