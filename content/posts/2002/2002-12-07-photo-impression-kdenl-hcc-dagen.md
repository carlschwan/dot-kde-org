---
title: "Photo Impression of KDE.nl @ HCC Dagen"
date:    2002-12-07
authors:
  - "fmous"
slug:    photo-impression-kdenl-hcc-dagen
comments:
  - subject: "Way to Go"
    date: 2002-12-07
    body: "Good job this is the kind of thing that all of us can do a local computer events. I would prefer to demonstrate KGX on a KDE only Distribution like Lycoris/LX, Xandros or LindowOS. Mandrake does there config in gtk so it's not the best distribution to show case the flexibility of KGX. Mandrake made a gamble a while ago that Gnome would become the dominate desktop so they decided to do there config utilities in gtk. Unfortunately for them KDE stayed dominate and actually grew its desktop share. Now they don't have consistency. I'm not saying anything like Mandrake sucks or anything like that i'm just saying its not the best way to showcase KDE. Please wackos this isn't a troll post so don't bother attacking.\n"
    author: "Kraig"
  - subject: "Re: Way to Go"
    date: 2002-12-07
    body: "Of course it's not a troll. A convicts are always 'innocent'..."
    author: "Jare"
  - subject: "Re: Way to Go"
    date: 2002-12-09
    body: "Kraig,\n\nIf you like to do some promo as well please contact events.kde.org (Eva) to let her know which event you are plannng to attend. \n\nAlso if you want to check out the possiblities of doing some promo work for kde see some of the mailinglist on promo.kde.org and events.kde.org.\n\n\nTake care\n\n\nFab\n\n"
    author: "Fabrice"
  - subject: "Good Work!"
    date: 2002-12-07
    body: "Thank you for a job well done, Fabrice."
    author: "anon"
  - subject: "Re: Good Work!"
    date: 2002-12-09
    body: "Yep, good work Fabrice!\nBTW I like your new rough image (ie. beard). Does this mean\nyou are serious about becoming a KDE/UNIX hacker ? :-)"
    author: "Rob Buis"
  - subject: "Re: Good Work!"
    date: 2002-12-09
    body: "Well Rob ... I needed a shave, but this whole weekend was just too hectic. No I am not *really* becoming a KDE/UNIX hacker. :-P\n\nBTW I got some info on that article in Linuxmagazine. Rob Kaper wil write something.\n\nTake care\n\nFab"
    author: "Fabrice"
---
The <a
href="http://www.kde.nl">dutch
 KDE team</a> armed with Konqi, a laptop, some flyers, and a lot of enthusiam, was present at the <a
href="http://www.hccdagen.nl/">HCC Dagen</a> computer event in Netherlands November 22-24.  We managed to garner a fair amount of attention for our favorite desktop as you can see from <a
href="http://www.xs4all.nl/~leintje/kde-nl/hcc2002/hcc2002_e.html">this photo report</a> (click on the images for comments).
<!--break-->
