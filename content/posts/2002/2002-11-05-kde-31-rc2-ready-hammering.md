---
title: "KDE 3.1 RC2: Ready For A Hammering"
date:    2002-11-05
authors:
  - "dmueller"
slug:    kde-31-rc2-ready-hammering
comments:
  - subject: "Great !!"
    date: 2002-11-05
    body: "Too bad i'm over cvs right now, i got desperated and started to dl cvs a couple hours ago, i hope i can get some of those mentioned bugs on kdebindings corrected :-)\n\nGreat work KDE team !"
    author: "GaRaGeD"
  - subject: "Re: Great !!"
    date: 2002-11-05
    body: "After you finish dl-ing the cvs version, just do cvs update -d in kdelibs, kdebase .... dirs  ..."
    author: "Droop"
  - subject: "kdebindings"
    date: 2002-11-05
    body: "I know that Marcus was working on QtC and QtJava just a few days ago.  Marcus and David were also working to fix a compilation problem with smoke.  Nick, is also working to integrate the latest Qt# into cvs before the release."
    author: "Adam Treat"
  - subject: "Re: kdebindings"
    date: 2002-11-05
    body: "Great news. I was afraid kdebindings would be excluded from KDE3.1, after it (smoke, QtC and probably others) proved pretty impossible to compile under Qt3.1 (I even tried fixing the code, but new problems arose time and again). I for one am looking to write KDE frontends to some Java projects of mine, so I'm relieved to hear it's being worked on."
    author: "Haakon Nilsen"
  - subject: "Re: kdebindings"
    date: 2002-11-05
    body: "I'm glad somybody is looking at those java bindings! I made an application in Java which is useless now, because the Java bindings where broken. \n\nThe qtjava bindings of KDE release 3.0.4 didn't work. Richard Dale, who wrote and maintained the java bindings, has left the scene. Now the javabindings mailing list is afwul quit :(\n\nI wrote some code which made it possible to use DCOP under Java. The DCOP bindings for Java allready made it possible to invoke DCOP ojbects. My additions made it possible to implement a DCOP Object in Java. I added this stuff to the kdejava bindings. Since the mailing list for kdejava only containts spam lately, these additions didn't make it to the CVS repository. \n\nI would like to get in contact with somebody to see how these addition can make it to KDE's CVS\n\nGert-Jan van der Heiden"
    author: "Gert-Jan van der Heiden"
  - subject: "QtJava"
    date: 2002-11-05
    body: "Just to clarify: AFAIK, Marcus was working to make them compile.  He was not and is not interested in maintaining them.  At this point the KDE/QtJava, KDE/QtC, Objective-C, dcop*, Ruby*, bindings are _not_ being actively maintained.\n\nThe only bindings I know which have active developers are Qt#, PerlQt and PyQt.  On the Qt# front: we are busy with a full scale refactor of the binding generator.  We now have our own c++ parser which produces an XML representation of the Qt API written in C#.  We also have a new binding generator called binge which feeds on the XML representation and is thought of as a replacement for kalyptus.  The advantage of the XML representation and associated tools is the ability to target the XML representation which only would need to be updated per Qt/C++ release.\n\nIMHO, the different binding projects should concentrate on consolidating.  Kalyptus and the different bindings that depend upon it are horribly difficult to update from release to release.  My hope is that Qt# will mature and allow the binding developers to concentrate on one binding.  Languages would then be added by creating compiler frontends for DotGNU and Mono."
    author: "Adam Treat"
  - subject: "Qtopia - [WAS: Re: QtJava]"
    date: 2002-11-10
    body: "with qtopia running on more and more and being super flexible and workable as a UI in itself ... (i'd rather login to a server via Konqui or Moz and get a java-applet representation of my account that looks like Qtopia and runs in my browser than use VNC etc). Java and Qt are crucial for a good deve environment.\n\nI see gcc's java support helping to convert Koffice from KDE to Qt-java apps that will run on my Zaurus or my Paron\n\nhttp://www.cdlusa.com/products/m-idphone.shtml\n\nciao and rule!"
    author: "java not #"
  - subject: "Re: QtJava"
    date: 2002-11-10
    body: "<i>the different binding projects should concentrate on consolidating</i>\n\nConsolidating on the Binge development or on Qt#? \n\nI can see the benefit in using the former, though I'm a little surprised that with so much other XML RPC work it was necessary to create what sounds like WSDL. There  are of course already many WSDL binding tools, both Schema->Language and Language->Schema. \n\nAs for the latter, the idea that open source developers will converge on some kind of Microsoft CLR clone is naive at best. The reality is that there are today a large number of enterprise-level tools and applications written in Java for Linux, from Eclipse and JBuilder to WebLogic and WebSphere, and to propose neglecting support for this established and complete environment in favour of some half-baked, high-risk Dotnet clone is simply a non-starter.\n"
    author: "Ulrich Kaufmann"
  - subject: "Noatun fixes"
    date: 2002-11-05
    body: "Currently, Noatun is pretty horribly borked. No other KDE app gives me as much trouble as Noatun. When it isn't segfaulting, it's choking on ShoutCast playlists. And it's still horribly slow to start and very fragile (try loading several thousand songs into a playlist). Anybody working on this for 3.2?"
    author: "Rayiner Hashem"
  - subject: "Re: Noatun fixes"
    date: 2002-11-05
    body: "If you have a lot of songs, there is a file system based playlist called Hayes that you can add you mp3 directory and it automatically updates whenever you add songs to the disk. I find it handles large playlists fairly well. here is the url: http://freekde.org/neil/hayes/"
    author: "Echo6"
  - subject: "Re: Noatun fixes"
    date: 2002-11-05
    body: "I've tried that, and it's still quite unstable at times. It just seems to me that Noatun is a consistantly ignored aspect of KDE. The impression from the Noatun developers themselves, that Noatun is done and finished doesn't help any :("
    author: "Rayiner Hashem"
  - subject: "Re: Noatun fixes"
    date: 2002-11-05
    body: "Have you ever tried to load an mp3 from a network fs like NFS, played it succesfully, then disconnected and loaded noatun again? Noatun will try too much time to load that file even if you simply opened noatun without any particular request.. The only thing that I found to do is to open noatun config file and erase the line where is the call for that file.\nThis is the reason why MPLAYER is a LOT better (yes it has to do nothing with KDE, but there is no competition).\nAnd yes, It gives the sensation that noatun isn't at the same level of KDE in general."
    author: "GOo"
  - subject: "Re: Noatun fixes"
    date: 2002-11-06
    body: "Yes, Charles thinks Noatun is done, but it's his job.\n\nI think Noatun has room to improve, so that's why I work on Hayes.\n\nIf you have problems with Hayes, do mail me about it.  I want to improve Hayes.  Don't let your impression of Noatun carry over to my Hayes work."
    author: "Neil Stevens"
  - subject: "Re: Noatun fixes"
    date: 2002-11-06
    body: "Hayes look cool. Any plans for including it in KDE?"
    author: "isNaN"
  - subject: "Re: Noatun fixes"
    date: 2002-11-06
    body: "It has been turned on three times by three different people (me, then Rob Kaper, then Kevin Puetz).  Charles Samuels turned it back off every time.\n\nHe has his reasons, but I'll leave it for him to explain them, for fear of getting it wrong."
    author: "Neil Stevens"
  - subject: "Re: Noatun fixes"
    date: 2002-11-06
    body: "That sucks... I think it was really great!\n\nNo more XMMS for me! :=)"
    author: "isNaN"
  - subject: "Re: Noatun fixes"
    date: 2002-11-07
    body: "I'm glad you like it.\n\nI woudln't say was, though.  Hayes isn't going to disappear anytime soon. :-)\n\n1.1.6 will see many improvements to shuffling."
    author: "Neil Stevens"
  - subject: "Re: Noatun fixes"
    date: 2002-11-07
    body: "that will be great! shuffling is its weak point right now. imho :) good work. I havent had too much trouble with its stability. the only real problem i have w/ all 3.x series up to beta2(havent gone further) is that the session management does not work on noatun. it will not start up after saving the session w/ it running."
    author: "Echo6"
  - subject: "Re: Noatun fixes"
    date: 2002-11-07
    body: "Perhaps you can put it in kdeextragear-1.\nsome application in kdeextragear are very good.\n\nk3b comes to mind !\n\nregards"
    author: "thierry"
  - subject: "Re: Noatun fixes"
    date: 2002-11-08
    body: "kdeextragear-* are not part of KDE.  In fact, it's one of the few portions of the CVS whose express purpose is to be forever out of the KDE release."
    author: "Neil Stevens"
  - subject: "Re: Noatun fixes"
    date: 2002-11-06
    body: "Yes, Noatun always seems to behave in an odd way, even for the simplest things. For example, loading of playlist files from xmms seems to result in the comments in such files appearing as track entries in the playlist in Noatun."
    author: "Paul Boddie"
  - subject: "Binary Packages ?"
    date: 2002-11-05
    body: "Is there going to be a binary package release for this version ?"
    author: "_deadfish"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "No."
    author: "Anonymous"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "no, because kde 3.1 final is right around the corner.. too much work. this is meant for people willing to test anyways, and that means ppl who compile from src :D"
    author: "asf"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "Nah, not really. I'd be willing to test it if there were rpms, but I'm not going to compile it. IMO it would be nice to have binaries to get rid of packaging related bugs.\n\nJ.A."
    author: "J.A."
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "Wrong view: No binaries => no packaging related bugs. :-)"
    author: "Anonymous"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "> Wrong view: No binaries => no packaging related bugs. :-)\nYes, I understand your point, that way developers can concentrate on actual kde bugs instead of waisting their time tracking bugs that end up beeing distro related. I understand but I don't agree. Most people will install 3.1 using distro specific binaries, for them it doesn't really matter wether bugs related to packaging or not.\nIf I recall correctly there were quite a few packaging problems in Beta2. No more testing means that we don't really know how the final packages will turn out. Yes, I know binaries would slow down the process, but still, I think it would be worth it.\n\nJ.A.\n\n\n"
    author: "J.A."
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "Maybe a possible strategy could be, releasing the final version of the sources with a preliminar version of the binaries. Release a final stable binary version only one or two weeks later.\nI really don't like the fact the binaries for the stable kde release are in fact, not (IMO) sufficiently tested and potencially buggy.\n\nJ.A."
    author: "J.A."
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "Yeah, I remeber when I used redhat... KDE packaes where buggy as hell.\nMandrake even have a policy to update their packages iwth bug fixes if any, even that some time have passed sinse release.\n\nMaybe a rc_rc_kde-3.1 could be made just to test packaging, and then (one week later) when the bugs where fixed, release final? It would be really good, at least for distros that do care about kde."
    author: "protoman"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-06
    body: "Binary packages for what? Solaris? Debian GNU/Linux? i386? PPC?\nWhy don't you make binaries available for your favorite platform,\nand others can use 'em? That's construcutive and helps the testing.\n"
    author: "Anon"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "Perhaps cooker will get binaries, we got some for RC1.\nI think that only having sources doesn't help to have a lot of beta testers.\nOn the other side, people who get and compile sources will probably send better bug reports."
    author: "Mooby"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-05
    body: "> On the other side, people who get and compile sources\n> will probably send better bug reports.\n\nYeah, or they'll send invalid ones due to their inability to compile things properly, or simply having a shafted environment. :)\n"
    author: "Wade"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-06
    body: "i guess not, but that's bad.\n\ni would happily give it a try and report bugs, but i won't mess up my system with a self-compiled kde. there was a time, when i compiled all my programs myself. but not anymore. if i ever find a non-packaged program i just package it before installation.\n\nno time and resourced to do so for kde. conclusion: no testing on my side. just waiting on the final release without my contribution...\n\nand i'm quite sure, there will be package-related bugs on the \"final\" version, as we didn't test it in an rc..."
    author: "simon"
  - subject: "Re: Binary Packages ?"
    date: 2002-11-08
    body: "Don't be silly. The only pre-release testing that takes place on most Unix world software is by developers and experienced users who know how to compile from source. And you wonder why KDE (or any other number of complex systems) is so rough at the edges, despite the _appearance_ of sensible release management... \n"
    author: "Anonymous Coward"
  - subject: "How does one go about packaging this ?"
    date: 2002-11-08
    body: "I have often wondered as to how does one package kde for a distribution. I can compile KDE (not very well - need lots of luck). But how does one do the packaging trick ? I figure that you need to create a bunch of rpms with the correct distribution specific locations etc. But isnt there more in terms of code changes to be done for a distribution. For eg, Mandrake has this unified menu system. How do I support that ? \n\nYeah, I know, RTFC, blah blah, read the code. Is there any other way ? \n\nIf any of you people who know how this works and can send some info or even better write an FAQ, I would be more than willing to do the actual task. \n\nAny thoughts ?"
    author: "_deadfish"
  - subject: "Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "One thing I really like about Nautilus are the smooth, rounded boxes around the name of an icon when it has been highlighted.  It gives Nautilus a very polished, pleasing look.  The boxes in Konqeror just don't look as good.  The worst part is that icon names (in Konqueror) are NEVER centered in the \"highlight\" box.\n\nI was also wondering if the kasbar could have an option to look more like a standard taskbar.  By this I mean the icons for minimized windows would be longer and thinner (allowing more of the name of the window to be shown).  I really (and I mean really) love the thumbnail of minimized windows that pop up from the kasbar.  However, the kasbar won't be adequate replacement for a standard taskbar (for me at least) unless you have a better idea what each icon is BEFORE moving the mouse to bring up the thumbnail.  Maybe it's just me?"
    author: "Rimmer"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "As for the desktops icons, I noticed that too when I played with Nautilus recently.  I was like why does this feel so nice, when it doesn't really do anything special.  I think you are right, it's thinks like the icon highlighting and font shadows.\n\nWhy don't we have font shadows?"
    author: "Enrique"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "probably the reason for all projects missing features. Noone has coded it yet ;-).\n\nCongrats to the KDE-team, this looks like a very nice and polished desktop.\n\nPersonally I use GNOME, but I'm happy that there is also a KDE-project that does very well, because this means that the two projects can focus in different directions and that way make more *NIX/people happy than any single project could do on it's own."
    author: "Gaute Lindkvist"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-06
    body: "heh, gnome and kde seem to be focusing on mostly the same thing"
    author: "asf"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Why don't we have Window shadows?\n\nlike the Menue shadows?"
    author: "Shadows"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-07
    body: "It's not feasible.  Menus are only around for a short time so if their shadows don't change when the stuff under them changes, you won't notice.  Also, menus never resize.  Windows are around for a long time, so their shadows need to update when things change under them to look consistent.  However, there is no efficient way to tell when the shadow needs to be updated.  Also, when resizing a window smaller, the uncovered parts of windows underneath haven't been painted yet so there is no way to know what they will look like to calculate the shadow.  When X gets a transparent window extension it will handle all this internally, which is much more efficient since it knows everything there is to know about how the screen looks at all times.  Then we can have window shadows and translucent noatun skins and all that good stuff."
    author: "Spy Hunter"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-08
    body: "when is the transparency extention to X going to be written.. i've been hearing about it for nearly 2 years now :-("
    author: "hmn"
  - subject: "KInda Funny"
    date: 2002-11-05
    body: "It really took me a long time to figure out why I liked GNOME so much.  I spent a week switching back and forth from KDE to GNOME (after installing RedHat 8).  Nautilus really is beautiful... Unfortunately GNOME is missing a few things (like a real file selector and show desktop button) which are difficult for me to live without.\n\nI'm hoping the Konqueror guys will spend less time on browser features and more time on the file manager."
    author: "Rimmer"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Hmmm.. My No1 Look&Feel problem with Konqueror (as file browser) is the background pattern. It doesn't look very good, especially with Keramik."
    author: "Tim Jansen"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "\"View/Background Image...\" allows you to change it."
    author: "Anonymous"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Then _only_ think I like about Nautilus is that smb:// works.\nWish Konquereor would get better support for that. Currently, \nit krashes, hangs, uses 100% CPU, fails, and its idiotic\nto start lisa by hand first. Lisa is also too idiotic in its scanning of the local\nnetwork. Not funny having a few thousand clients do that on a class B net."
    author: "anon"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "I plan to work on a scalable file-sharing solution in 3.2..."
    author: "Tim Jansen"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Great. Remember security and authentication.\nI'm dreaming of an (optional) data encryped filesystem with\nKerberos authentication. Looks like NFSv3/4 with gss-rpc might solve that,\nbut I would also like ordinarey users easily browse them from something\nlike Konqueror.\n"
    author: "anon"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Yes...\nhttp://www.advogato.org/person/tjansen/diary.html?start=14"
    author: "Tim Jansen"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Probably we are using different Konquerors: \n\nmine is fast, clean, stable and supports smb:// pretty fine. \nand lisa works fine too (otherwise -- who else would browse the hetwork looking for available nodes and their resourses?\n\n;-D"
    author: "SHiFT"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-05
    body: "Hi,\n\nyou don't have to start lisa by hand, do it from an init script.\nIf there are several instances of lisa running in one network, only one will do the scanning, the other will retrieve the information from this one. If something doesn't work perfectly, there might be also one or two additional instances of lisa which scan (but this never happened in practice to me).\nEven more recommended is to start lisa only on one dedicated machine in the network, and then not use \"lan:/\" but \"lan://the_dedicated_machine\"\n\nWhich smb-ioslave do you use ? The one using libsmbclient or the one wrapping smbclient ?\nKDE 3.1 RC2 ?\nWhich samba version ?\nYou can contact me per private mail.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-06
    body: "Stop whining about non-KDE stuff being better than\nKDE pleeease."
    author: "KDE User"
  - subject: "I know it's difficult for the KDE trolls...."
    date: 2002-11-06
    body: "But\n\n1) The Nautilus icons look better.\n\n2) There is no reason Konqueror icons couldn't look as good."
    author: "Rimmer"
  - subject: "Re: Couple of Requests for Future KDE Releases"
    date: 2002-11-06
    body: "why?"
    author: "asf"
  - subject: "Why what?"
    date: 2002-11-07
    body: "I don't understand what your asking.\n\nSorry :)"
    author: "Rimmer"
  - subject: "Recent screenshots anywhere?"
    date: 2002-11-05
    body: "nt"
    author: "Janne"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-05
    body: "Posted one to sfnet.tiedostot."
    author: "jmk"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-05
    body: "Thanks, I'll take a look once I get back home :)"
    author: "Janne"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-05
    body: "How did I get there?\nFor what I found on google, it's a newsgroup, right?\nWhat's the address?"
    author: "protoman"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-05
    body: "sfnet.tiedostot\n\nThat screenshot looked good :). Altrough I never understand why people drag the panel to the side. Bottom or top for me."
    author: "Janne"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "> Altrough I never understand why people drag the panel to the side. Bottom or top for me.\n\nSimple: i don't want wide-screen web-surfing. Most of the web sites seem to be designed so that they dont use the space on the sides, at all. "
    author: "jmk"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "I can be wrong... but don't I need a .com/.org/.net in the name?\nI mean, what is the server dns or ip address? Hot do I get there?"
    author: "protoman"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "It is a newsgroup called sfnet.tiedostot. It is NOT a website, therefore it does not have .com or anything like that. you do not access it using a web-broswer but with a newsgroup-reader (like Knode)."
    author: "Janne"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "Then when I try to subscribe, I get a \"unable to resolve hostname\" :)\n\nI don't know if internet there (whatever you are) is different than here, but here at brazil, you need a real server name (one that you can ping, it dosen't have to be a web browser, but a internet server) for newsgroups.\n\nMaybe it's like AOL, where you don't have to type complete addresses, just keywords, but I, I do need a real, valid, name or DNS."
    author: "protoman"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "It's not a news server, but a newsgroup. You need to subscribe to a news server which has the sfnet.tiedostot newsgroup on it.\n\ndave"
    author: "dave"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "Ok, now we are understanding each other :)\nCan you supply me a server address that contains it, please?"
    author: "protoman"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "news.jippii.fi, news.cs.hut.fi, news.song.fi, news.welho.com, .."
    author: "jmk"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "\"I don't know if internet there (whatever you are) is different than here, but here at brazil, you need a real server name (one that you can ping, it dosen't have to be a web browser, but a internet server) for newsgroups.\n\nMaybe it's like AOL, where you don't have to type complete addresses, just keywords, but I, I do need a real, valid, name or DNS.\"\n\nContact your ISP and ask the address to their newsgroup-server. Configure your newsgroup-reader with that information, locate sfnet.tiedostot (the server might not have it though) and subscribe to it.\n\nHave you ever used newsgroups before?"
    author: "Janne"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "Sure I did, I was asking for a server for that lists.\nIt's not usual here, in Brazil for multiple news servers having same lists, sorry."
    author: "protoman"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "When you write or read text, a portrait screen is better than a landscape one because it more looks like a page (letter or A4 format). Of course, it's not really \"portait\", but the more it is, the more it will display text.\nExemple : here, when I write this text, suppose that I have many many things to write, I will see more text if the panel and the tool bar are on the side.\n\nI have found on the Kde apps site a fantastic xmms-noatun applet to put in the panel. Happily there is a \"verticalblue\" theme, but more vertical themes would be good... I hope that this applet will be included in the KDE 3.1..."
    author: "Alain"
  - subject: "Re: Recent screenshots anywhere?"
    date: 2002-11-06
    body: "That's real slick, slim. But next time, do us all a favour and include the \"news://\" to avoid all the confusion..."
    author: "Idiot"
  - subject: "Funny rendering of web page"
    date: 2002-11-05
    body: "I'm using head instead of the KDE 3.1 branch. Does this page render funny in the branch too:\n\thttp://www.dpreview.com/forums/read.asp?forum=1012&message=3701394\n\nWhen setting the Browser Identification to IE you are suppose to be able to click the image to have it expanded and the menus to the left doesn't work at all for certain items and they are horrible slow...\n\nRegards, Mita"
    author: "Mita"
  - subject: "Re: Funny rendering of web page"
    date: 2002-11-05
    body: "There is no such thing as a KDE 3.1 branch yet."
    author: "Anonymous"
  - subject: "Re: Funny rendering of web page"
    date: 2002-11-05
    body: "I ment 'KDE_3_1_0_RELEASE'"
    author: "Mita"
  - subject: "Re: Funny rendering of web page"
    date: 2002-11-05
    body: "This is not the only example where pages are rendered \"funny\".\nTake a look at www.megatokyo.com\n\nAfter reading a bit through the sites source code, though, I stopped wondering why it is displayed incorrect by Konqueror and started wondering why it is displayed correctly by other browsers.\n(Take a look at line 136 of the source - there the page is loading an image with a width of 1 pixel and one with a width of 700 pixels into a td with a fixed width of 700 pixels....)\n\n\tGuido"
    author: "Guido Winkelmann"
  - subject: "the changes in Crystal look good!"
    date: 2002-11-05
    body: "liked the new back and forward arrows in Crystal. much better than the old green arrows.\n\nhowever, the icons in kicker and on toolbars (not only when using Crystal) in general suffer from a strange thing: whenever an icon has an antialiased border, the border seems to render withour partial transparency, resulting in pixelated ugly icons.\n\ncould this be due to the fact that i couldn't compile qt with Xft support on my RH8.0? which reminds me - has anyone managed to do so on RH8.0?"
    author: "dvirsky"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "I have had it with Redhat on the desktop. I'm going to keep my servers running\non Redhat for the time being but my laptop and desktop are moving over to Mandrake 9 as soon as I get it in the mail. Only about half of kde apps compile nicely on \nredhat. I can't even recompile the kernel on 8.0. There is no mp3 support. KDE is\ndespite what linuxandmain.com say completely horrible and reduced to the lowest common denominator. I'm sick of this I want a system that does what I want it to\ndo not what some marketing gimp in redhat thinks is a good idea.\n\n--B"
    author: "bryan hunt"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "yup, you're right. for me, this is (almost) the last straw with RedHat. I'm conetmplating moving to gentoo, debian or suse. i don't really care for mandrake, though. i've tried moving to 9.0 but it simply wasn't as good as RH8.0."
    author: "dvirsky"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "I stopped using RH a few years ago.  I never really did care for the distro and now with the games they are playing with KDE and the whole shell thing as of late...\n\nAnyway, I had fallen in love with MDK and used them for the past couple years.  I then setup a \"beat me up\" box and bought SuSE.  OMG, I thought I didn't like RH...  Anyway, the point of this rant is Gentoo is GREAT.  I have it running on all my Linux machines now.  OK, yes, it can take a couple days to get up and running, but man, I gotta say, I've NEVER used such a smooth running machine ever.  It's fast as all get out, and SMOOOOOOOTH.  \n\nThe only thing with Gentoo is be prepared to roll up your sleeves and get your hands wet.  There is very little \"hand holding\" like the more main-stream distros.  YOU do (basically) everything.  The package manager does what's left (and it's quite good too).\n\nJust my 2 cents.\n\n\n"
    author: "Xanadu"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "Do what I did -- Compile KDE yourself! This takes some time, but is not really hard, and you get the latest KDE with the original artwork (which is much nicer than anything RH could come up with). So you get the best of two worlds; a stable OS (RH is much more polished than some other distros out there), with a great desktop."
    author: "Haakon Nilsen"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "Been there, I used to do that back in the redhat 6 days, no problem at all \n(well appart from the time i forgot to add ssl support) but I've tried recently \nusing 7.3 and kde 3.1 alpha and more recently with 8.0 and kde beta and had no \nsuccess, I think a lot of it has to do with different versions of automake etc.\nMaybe I should accept the bleeding edge for what it is though. \n\nIf you can get it to satisfactorily compile for you perhaps you should write \na howto and put it on the net because I know that I certainly am not the only \nperson having problems with redhat/kde from source.\n\n--B\n\n"
    author: "bryan hunt"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "I'm not bothering with writing HOWTOs, because all I did was follow the standard instructions. This was KDE3.1rc1, which had one (known) compile problem in kdenetwork which I was able to fix (and also a bunch of problems in kdebindings, which I was not able to fix (but submitted a bug report), but that's hardly a package you're likely to need). But generally, if you run into problems, help is usually just a visit to #kde or #kde-users on irc.kde.org away. :)"
    author: "Haakon Nilsen"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "Amen to that. I'm running Mandrake, but i've had no success compiling KDE 3.1 either. The Source always bombs at KDE Base, the binaries Bomb even after they appear to have successfuly installed without problems. When I try to log into KDE3.1, my screen turns grey and the session dies in less than ten seconds, and I'm returned to the login manager. I pride myself on being able to install any piece of software with enough time and effort, but I'm spent on 3.1. It doesn't play nice with my Mandrake 8.2 or 9.0 distros and i've temporarily decided to just give up and wait for the final release. It kills me to say that."
    author: "Guppetto"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-06
    body: "I've been using kde3.1 on mdk8.2 since the alpha release without problems. The most difficult part was compiling qt with the right options since its image support has changed a bit (more options) in compilation. Other than that it worked very well. \nLately I've installed rc1 binaries from cooker on a fresh 9.0, same, no problems."
    author: "jaysaysay"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-05
    body: "just compiled it on my openbsd box (Apple TiBook G4)\nruns smothly. way to go team!\nfor those of you having trouble with it on linux...\ndrop linux, thats where the problem lies.\nmost of the *BSD\u00b4s are usefull as desktop machines nowadays\nwith splendid soundsupport and accellerated X.\nopenbsd just got a new release (3.2) that rocks.\n\nyours sincerery\n\n/ Jimmy  "
    author: "Jimmy"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-06
    body: "I don't know if you're uninformed or simply trolling, but regardless I'm going to spend a bit of my time and say you're off on your rocker here, plus explain why I think so.\n\nLinux is the problem? Bah! Whereas I can use and appreciate the various *BSD versions, problems as commonly reported by users seldomly lie in the OS itself, but in the applications and libraries bundled with it. If you have a vendor-modified library (as is common amongst some of the larger Linux distros), you are *bound* to run into problems in those, eventually, as soon as you use anything which isn't bundled or supplied by aforementioned vendors. This is however not a problem of Linux itself, but a problem with vendors shipping with odd library hacks, without clearly stating they're doing so.\n\nYou can blame it on Linux as much as you like, but it would likely look the same way if, say, FreeBSD was distro-based. Personally, I've only occasionally run into problems compiling or running KDE - always caused by breakage in GCC and friends; this goes both for OpenBSD, FreeBSD and Linux, and I'm sure I might occasionaly run into similar problems if I would run, say, Solaris. \n\nI honestly don't understand why people are so anal-retentive about what OS people are running. If FreeBSD, OpenBSD or whatnot works for you, all the power in the world to you for that, and if you run Linux, hey, cool. But if you feel the need to knock an OS, at least *read up* on the subject before you open your mouth.\n\nOr, to formulate it as a question, where exactly in Linux is this problem you see?"
    author: "HuskyPup"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-07
    body: "Intriguing. Last time I had a Mac (early this year, sold my iBook in April), I was checking out OpenBSD and it did not have accelerated X yet.\n\nHow well does it run? Not that I'm going to have the money to get a PowerBook anyway; my next laptop is probably the cheapest Banias I can get next year. No siree, no P4 for me!"
    author: "Michel"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-07
    body: "Could not compile Qt with Xft support here either :(. I'm compiling GARNOME to try out the pre-2.2 Gnome development series and that compiles its own standard Xft (downloaded from fontconfig.org) so I might try compiling KDE 3.1 CVS against it.\n\nHad problems trying to play Uplink (www.introversion.co.uk - try it, great game!) as well. They figured out it's due to some incompatible library RH shipped and produced a patched library to LD_PRELOAD, but then I find I could only run the program in a window.. arrgh\n\nPlus dvd::rip requiring a work-around due to RH's buggy perlIO module. Like you, I'm thinking of moving for a while - but it's probably going to be to Knoppix. Debian + auto hardware detection and recent KDE (3.0.3, there are unofficial KDE 3.1rc builds announced in debian-kde though).\n\nOh, ps, if you still use MP3, freshrpms.net is the place to go to get xmms-mp3 and other goodness :)\n\nRegards,\n\nMichel"
    author: "Michel"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-07
    body: "dudes, I've just installed mandrake 9 \n\n1) Encrypted filesystem support is a choice at config time\n( I created a 100mb /enc system )\n2) It detects my Zaurus straight away, no need for kernel patches\nand kernels which refuse to compile. \n3) I know there will be rpm's for it when kde 3.1 comes out \n( I've haven't tried compiling from source on mdrk yet)\n4) The only one bug that I've found with it so far is that\nit does not install the /lib/libnss_wins.so.2 (enables you \nto use windows name resolution ( handy on a windows network \n(ie ping windows-pdc actually works )) I'm gonna get the samba\nsource and compile that one myself ( I gotta do it anyway cause\ni need to set up samba ldap pdc )\n5) Mandrake feels like a finished product.\n6) Mandrake kernel has framebuffer support so now i can get to \ndevelop some stuff for the zaurus using Qtopia environment.\n\nIn short my redhat days are numbered\n\nBest Regards\nBryan"
    author: "bryan hunt"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-10
    body: "To get Xft support to work in RH 8.0, first dump the existing RPM's for fontconfig and Xft. Then go here:\n\nhttp://fontconfig.org/\n\nand install all of the fontconfig stuff from source. The programs are small, so it doesn't take long. Then build KDE from source. If you install the fontconfig stuff (Xft, Xrender & fontconfig) you will have much better luck overall. The RPM versions are iffy."
    author: "Jim"
  - subject: "Re: the changes in Crystal look good!"
    date: 2002-11-07
    body: "I'm using RC1 at the moment on Debian Unstable, and I get the same problem. Some of the small icons appear unreasonably jagged around the edges. I'm still waiting on RC2 debs."
    author: "Wrenkin"
  - subject: "no jpeg on thumbnails and Desktop"
    date: 2002-11-05
    body: "Hi, i'm unable to display jpeg images on Desktop, and thumbnails are not being \ndisplay with that format (rc1 and rc2). And yes, I did compile qt with the suggested option\n(-system-libjpeg, -system-libpng, etc). Does anyone have the same problem?\n\nThanks\n"
    author: "Jes\u00fas Antonio"
  - subject: "Re: no jpeg on thumbnails and Desktop"
    date: 2002-11-05
    body: "Try recompiling using the qt-included libs... and not specifying plugin or built-in for image formats... I had this problem with RC1 and leaving out mention of image formats made it work right (they compile as plugins by default)\n\n./configure -qt-gif -no-exceptions -thread\n\nI left everything else as default.\n"
    author: "Vic"
  - subject: "Re: no jpeg on thumbnails and Desktop"
    date: 2002-11-06
    body: "I had the same problem (Gentoo 1.4-rc1).  I'm recompiling Qt now per your directions.  But my question is... why, and what is the difference?  What broke between my \"system-jpg\" lib and this version of Qt/KDE ?  Why is it recommended to use these system libs rather than the included qt libs?  "
    author: "Jeff"
  - subject: "Re: no jpeg on thumbnails and Desktop"
    date: 2002-11-05
    body: "you need to add the plugin path for qt run qtconfig look at the last tab."
    author: "32"
  - subject: "screenshots"
    date: 2002-11-05
    body: "Hi!\n\nPlease, show us some screenshots. Thanks in advance!\n\nBye,\nmathjazz\n\n"
    author: "Matjaz Horvat"
  - subject: "SVG support?"
    date: 2002-11-05
    body: "The planned SVG graphics format has been quiet for the last few months.  Does/will KDE3.1 have full SVG support or will that come in KDE 3.2?"
    author: "Craig Williamson"
  - subject: "Re: SVG support?"
    date: 2002-11-06
    body: "It won't make it into 3.1"
    author: "perraw"
  - subject: "Mandrake"
    date: 2002-11-05
    body: "Binarys in cooker now!!\n\nWOHOO!!"
    author: "isNaN"
  - subject: "Re: Mandrake"
    date: 2002-11-06
    body: "have you try it ?\nit doesn't work here..."
    author: "cyprien"
  - subject: "Re: Mandrake"
    date: 2002-11-06
    body: "I almost tried it. But rpmdrake would only offer to update some kde* packages, and _not_ qt. It didn't look good (of course it is perfectly OK for cooker to be broken) so I decided to wait for 3.1 final. There will probably be ML 9.0 binaries posted by then, and it'll only be one or two weeks from now I hope ..."
    author: "NewMandrakeUser"
  - subject: "Re: Mandrake"
    date: 2002-11-06
    body: "works beatifull!\n\nJust remember to upgrade libqt3 else if will fail horribly..."
    author: "isNaN"
  - subject: "Re: Mandrake"
    date: 2002-11-07
    body: "Just download all KDE 3.1rc2, (lib)QT and Arts packages to a directory and do urpmi *\n\nIf there's anything missing, it will notify you. Just download that RPM, put it in the same directory and try urpmi again...\n\nI love urpmi :)"
    author: "Paul PARENA van Erk"
  - subject: "What about qt 3.1?"
    date: 2002-11-05
    body: "I'm curious about qt3.1 which will be used in KDE 3.1. What's different? Have the fontconfig and xft2 patches been applied and tested?\n\nI'll find out when I get home from work, as I set it all to build before I went to sleep last night (and it was still churning when I left for this morning ;)\n\nNonetheless, I'm curious about changes in qt 3.1\n\nAnybody care to enlighten me? The trolltech pages weren't as detailed as I'd hope."
    author: "Shamyl Zakariya"
  - subject: "Re: What about qt 3.1?"
    date: 2002-11-06
    body: "Yes, Xft2 and FontConfig both accounted for. Aside from that, it's a maturity release. Things seem to be just a hair smoother, stuff is a little more polished, and Qt Designer has a *much* more friendly interface.\n\nThere are two changelogs here:\nhttp://www.trolltech.com/developer/changes/3.1beta2.html\nhttp://www.trolltech.com/developer/changes/3.1beta1.html"
    author: "Rayiner Hashem"
  - subject: "Re: What about qt 3.1?"
    date: 2002-11-06
    body: "I'll have to experiment with how qt3.1 is compiled -- it's probably a matter of configure options. I'm running rc2 right now, thanks to Gentoo, but I've had several apps crash, and on the backtrace it's obvious it's font related. The drawback to gentoo is that the simplicity of its build system removes you from the process of examining the output of ./configure --help so you can pick the best options.\n\nWell, more time spent compiling..."
    author: "Shamyl Zakariya"
  - subject: "Re: What about qt 3.1?"
    date: 2002-11-06
    body: "I'm using Gentoo as well. RC2 has been rock solid so far, not a single crash. But if you want, you can examine the output of configure. Just go to the ebuild you want in /usr/portage. Say you want to configure qt-3.0.5-r1.ebuild. Go to /usr/portage/x11-libs/qt, then type 'ebuild qt-3.0.5.ebuild unpack' This will unpack the sources to /var/tmp/portage/qt-3.0.5-r1. Go to that directory, than to 'work', then to 'qt-3.0.5' (or whatever directory the sources get unpacked to), and do your ./configure and make. After the make is complete, do an 'echo \" \" > /var/tmp/portage/qt-3.0.5-r1/.compiled' (ie, make a file called .compiled, with any contents, in the top level directory for the particular ebuild). Now go back to the ebuild, and type 'ebuild qt-3.0.5.ebuild merge' Portage will detect that you compiled the package yourself, skip the unpack and build steps, and merge in the compiled package."
    author: "Rayiner Hashem"
  - subject: "Re: What about qt 3.1?"
    date: 2002-11-07
    body: "Now this is a gem for us Gentoo users!  Thx"
    author: "rbh"
  - subject: "Re: What about qt 3.1?"
    date: 2002-11-07
    body: "Fantastic -- I've been running gentoo since april or so and while I love it, I do treat the build system as magic pixie dust and pretty much leave it alone. When I need to compile something custom, I have copied its archive from /usr/portage/distfiles and (re)built it myself. For example, just two days ago I rebuild kdelibs and kdebase to use the really cool menubar kicker applet at http://lists.kde.org/?l=kde-devel&m=103644337025010&w=2 (I can't figure outhow to make a link on the dot). If I had known I could unpack the sources, tweak them, then let portage install them and not confuse its timestamp system I would have.\n\nThanks for the instructions. Portage is great, but almost too featureful for me to properly use -- I'm really much more of a slackware type ;)"
    author: "Shamyl Zakariya"
  - subject: "KDE & Prelinking?"
    date: 2002-11-06
    body: "Is any work being done to make KDE work with prelinking? It seems to be that one of the biggest gripes people have with KDE is the slow startup of apps. Prelinking would eliminate that problems. The problem is that KDE doesn't work with prelinking.\n"
    author: "Janne"
  - subject: "Re: KDE & Prelinking?"
    date: 2002-11-06
    body: "Sure does. \nFetch latest prelink from ftp://people.redhat.com/jakub/prelink\nIt is part of RH8.0 too"
    author: "Mita"
  - subject: "Re: KDE & Prelinking?"
    date: 2002-11-06
    body: "It does? Then what is this about:\n\nhttp://forums.gentoo.org/viewtopic.php?t=21117\n\nOr is it that Xfree doesn't work with prelinking? Anyone here run prelinked KDE? How's the performance?"
    author: "Janne"
  - subject: "Re: KDE & Prelinking?"
    date: 2002-11-08
    body: "From what I understand the binutils 2.13 enable some kind of prelinking by default. I tried the prelinking stuff before but didn't really notice a difference in start-up time."
    author: "repugnant"
  - subject: "3.1 Final ... when ?"
    date: 2002-11-06
    body: "Will someone please post here when a release date is decided for 3.1 final ?. Thanks !"
    author: "NewMandrakeUser"
  - subject: "Re: 3.1 Final ... when ?"
    date: 2002-11-07
    body: "According to the release plan at http://developer.kde.org/development-versions/kde-3.1-release-plan.html, 3.1 may be \"final\" on monday (but binary packages won't be ready until a week after), or if it's found to be not ready, RC3 is released and 3.1 goes final the Monday after (18 Nov). My hunch is that there won't be no RC3 :)"
    author: "Haakon Nilsen"
  - subject: "Re: 3.1 Final ... when ?"
    date: 2002-11-07
    body: "Thanks a lot Haakon, let's see what they decide next monday :-)"
    author: "NewMandrakeUser"
  - subject: "/usr/bin/meinproc segmentation fault"
    date: 2002-11-06
    body: "Hi,\n\nWhen I compile kmess 0.9.7 with kde3.1 the /usr/bin/meinproc --check --cache index.cache.bz2 ./index.docbook gives me segfault. Anyone knows why (bug?)? (with kde 3.0.3 this not happen)\n\nThanks,\n./noc"
    author: "Nocturno"
  - subject: "Re: /usr/bin/meinproc segmentation fault"
    date: 2002-11-21
    body: "I've the same problem, but I don't know where is mistake. Did you find the mistake? I'm using KDE 3.0.5. With version 3.0.4 everything was OK.\n\nThanks a lot,\n   Petr"
    author: "Petr Jodas"
  - subject: "Re: /usr/bin/meinproc segmentation fault"
    date: 2002-11-24
    body: "I had the same problem with kde 3.0.5 and fixed it by updating to libxml2-2.4.28 from ftp://xmlsoft.org/.\n\nGood luck !"
    author: "jp"
  - subject: "Re: /usr/bin/meinproc segmentation fault"
    date: 2005-05-03
    body: "I'm trying to compile KDE 3.4, I have libxml2-2.6.13, and I have segmentation fault on same place...Any ideas?"
    author: "Lt_Flash"
  - subject: "great release"
    date: 2002-11-06
    body: "This is a great release. Thank you :)\nIt feels real stable and polished."
    author: "Kohn"
  - subject: "Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2002-11-07
    body: "I upgraded this system to Win2K and was pleasantly surprised at how muc hbetter it worked that with win98 (despite low memory).  It seems to have *much* improved algorithms for swapping and memory use etc.  Whne I tried gentoo with all the whistles (preemptive kernel patch etc.) I was very disappointed with KDE3.0.2's speed. Very unsnappy.\n\nAre there any improvements in this situation eminent?"
    author: "Curious Windows user"
  - subject: "Re: Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2002-11-07
    body: ">Are there any improvements in this situation eminent?\n\nyes.. gcc-3.2, glibc-2.2.5 (+freinds) and kde-3.1 compiled from sources with -Os matches w2k response-wise on my old celly500 wiht 128megs if u set them to use equivilent amount of eye-candy. But if u turn on all the flower-power that kde-3.1 has to offer it will of course be less responcive (since it has more then w2k). I have not tried the preemptive patch yet.. but from what i hear it speeds up GIU-response quite a bit :) so maybe i can sqeeze kde to be as fast as w2k even *with* all the gadgetry turned on :)\n\n/tigertooth\n\nRemember: cool stuff takes cpu-time.. and RAM.. and disk I/O.. and gfx-ram <-> RAM overhead."
    author: "tigertooth"
  - subject: "Re: Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2002-11-07
    body: "There were definite issues with the speed of kde 3.0.2, but do understand that a poorly compiled kernel can lead to serious slowdowns. Also, the use of imon in the newer gentoo kernels, patch r10 and higher or the lostlogic kernels adds some speed to the whole system for apps that use fam (ala konqueror). KDE 3.1 is faster all around. I was much impressed with the speed increase from kde 3.0.2 to kde 3.1_beta1 which was my upgrade path. Now I'm using gnome 2.1.1 with xft2 and I love it! I'm a kde person, but these new fonts, coupled with how fast gnome is just blows my mind. However, KDE is a better system overall, and might always be that way. Cudos to both development teams! And yes, 3.1 is faster than 3.0.x. and I expect 3.2 to be faster."
    author: "karl11"
  - subject: "Re: Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2002-11-07
    body: "heck no. kde 3.1 is even slower. i seem to be getting by ok with a 750mhz duron & 320mb ram.. the jury is still out on that one, though.. i just upgraded my ram from 256mb to 320mb 5-6 hours ago because after a couple days usage my box started swapping to disk (i usually only reboot it when it starts swapping and slows stuff down).. \n\ni have a 1.47ghz pc with 1.5gigs of ram that runs kde 3.0.4, though.. its uptime is usually like 2 weeks or so.. very smooth stable system.. i don't think i've ever resetted it due to a software problem.. certainly never resetted due to any hardware limitations, either.\n\nremember the good 'ol days when coders would optimize their code? ahh.. memories.."
    author: "static"
  - subject: "Re: Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2002-11-07
    body: "Hey, that sounds like a memory leak. If you can find which application is causing it please report it. It could even be unrelated to KDE (some other app).y Otherwise ... you can restart X from the login manager (KDM). That may (or may not) help you avoid rebooting ..."
    author: "NewMandrakeUser"
  - subject: "Thanks for replies"
    date: 2002-11-10
    body: "This gives me HOPE! :-)\n\nI think rebooting because of swap must be due to a bug too ... even I know you don't have to do that with Unix and Linux :-)  The way to find out is to check if memory use keeps climbing!  \n\nOn my computer (old) it's slow right from the start and swap is frequent ... But now from these comments it seems with new KDE, Qt, compiler, kernel, patches, linker it may get better!"
    author: "Curious Windows user"
  - subject: "Leaks ... (and stabilty)"
    date: 2002-11-10
    body: "It's my impression that there isn't enough bodies and/or time to do real Q/A like leaving KDE up and running for weeks with scripts automagically causing test-cases to run and doign me-profiling.\n\nInstea\n\nThis has been good sof far but with KDE getting more complex and more big organizations usign it some serious profiling, speed work and deep bug squashing is needed so we get:\n\n- faster start up (launching apps is *painful* even on fast machines)\n- stability for long uptimes as a desktop system ... my iBook (which gets *alot* of use) is now on over 5 months uptime (some in sleep mode while transporting etc) with apps up and running in various modes for months at a time and with near *perfect* session management (I log out and log back in to hear my mp3's playing at eactly the same place). This is waht I used to use Unix for since Windows and Apple were relative toyz in the stability department.\n\nIt's sad that the situation has actually reversed: both XP and MacOS work much better than in the past while I can't get a new install of Linux/KDE to run sfor more than a few days or a week (as a desktop - server side things are great). I get a crash or lockup of the whole X UI or even the system (to say nothing of individual apps which simply cannot cut it against Outlook or Apple user apps).\n\nWill KDE/Gnome etc ever *stabilize development* long enough for real industrial strength debugging and stability work to be done? This what gave Emacs and apache their legendary stability. It seems now that Unix apps are perpetually *new* and just don't benefit from years of development and debugging that Windows applications do. For this to go back to normal the endless bleeding edgeness of KDE/Gnome must *STOP*.  You can't get solid stability if the whole dev. process isn't stable ...\n\nJust some thoughts not meant as a flame.\n"
    author: "Need to take one now actually"
  - subject: "Re: Leaks ... (and stabilty)"
    date: 2002-11-11
    body: "I agree with your comments completely. Since I've been using NT5, I've had a more stable desktop environment than recent KDE or GNOME versions. Nothing too drastic, but I just don't have the same kind of confidence with KDE as a whole than I do with Windows 2000. They can't be directly compared of course but still, it's a pity.\n\nRonald McDonald.\n"
    author: "Ronald McDonald"
  - subject: "Re: Leaks ... (and stabilty)"
    date: 2002-11-11
    body: "It seems you guys have a grave problem, but I don't think it's KDE. The desktop I use here have uptimes of more than 1 month, and it has 3 X sessions running, one with Gnome, one with KDE, and one with XFCE. The X server never got down since I installed Xfree 4.2.1 CVS a long time ago, and Gnome or KDE never got the system or X server down. I don't even stop them when I change the underlying libs (gtk+ or glib or others). Even with so bad treatment, they do not crash, but can start to malfunction ! I compile all my stuff from source, and use gcc 3.2. I think that people which have problems like you are misguided if you think that's the desktops that are not stable enough. The only stability problems I had was with Nautilus crashing every other day (and then it restarts itself immediately :) ). Since I upgraded to gtk+ 2.1.2, it seems to no longer crash...\nTo sum up, here, stability is perfect, and I never had a problem with launching apps (though it was slow in KDE 2, I don't have any problem since I switched to gcc 3.x, except the early gcc 3.x times )\nAs my two desktops are so stable that they don't crash on me, I thought all issues were resolved, but as others reported crash, I wonder sometimes if it's not also hardware related, as I should be the less stable of all, compiling all the latest from source..."
    author: "Ookaze"
  - subject: "Re: Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2002-11-08
    body: "I'm running KDE on a 450 w/ 128 and I have all of kde built with debug symbols and KDE runs without any problems on my box.... Better than KDE 2 ever did."
    author: "perraw"
  - subject: "Re: Is a 500 mhz machine with 128 megs RAM enough?"
    date: 2004-01-22
    body: "its enough for doin da basic pc programing buy not good for playing the high intense games"
    author: "Master Juvenile"
  - subject: "Licq"
    date: 2002-11-07
    body: "I'm using the Mandrake Cooker RPM's and Licq is acting up. VERY weird. If I set myself away (or N/A or anything else, but online/offline) it gives me the automatic reply dialog, but when I click OK, it won't go away. I have to exit Licq to get rid of it. Also, instead of pressing <CTRL>-<ENTER> once, I have to press it twice to send a msg... ???"
    author: "Paul PARENA van Erk"
  - subject: "Re: Licq"
    date: 2002-11-07
    body: "I got the same problem after compiling and installing RC2! Maybe some event handling change in Qt caused this."
    author: "Andreas Zehender"
  - subject: "Re: Licq"
    date: 2002-11-11
    body: "dont use licq :-p SIM is a much better app IMHO... "
    author: "chocobo_greens"
  - subject: "Re: Licq"
    date: 2002-11-11
    body: "LOL :)\n\nSweet, I tried it, it's quite good. Better than Licq IMHO as well now. :) If functions better, looks better... of only it wouldn't have huge linespacing when writing messages and pressing enter. :)\n\nI'm using SIM from now on. Thanks for the tip! :)"
    author: "Paul PARENA van Erk"
  - subject: "Showstopper! Kde 3.1 RC2 lost its mind :)"
    date: 2002-11-07
    body: "Check the attached file...\nBy the way, clicking on 'Yes' didn't produce satisfactory results :)\n\nCiao,\nGiovanni"
    author: "Giovanni"
  - subject: "Re: Showstopper! Kde 3.1 RC2 lost its mind :)"
    date: 2002-11-07
    body: "The file didn't get attached, so here it is:\n\nhttp://www.communicationvalley.it/download/wtf.png\n\nCiao"
    author: "Giovanni"
  - subject: "Re: Showstopper! Kde 3.1 RC2 lost its mind :)"
    date: 2002-11-07
    body: "That's damned funny!  ROTFL!\n"
    author: "Nigel Stewart"
  - subject: "Release candidate with known problems?"
    date: 2002-11-07
    body: "What on earth is the point of saying something like \"don't worry about bug x or y, they'll be fixed in the final release\"?  This is a *release candidate* ffs!  Any known bugs should be fixed before making it available, unless it's been decided that they will be put off until the next version.\n\nA release candidate isn't a reminder to fix the showstoppers before final release - that's what alphas and betas are for.  A release candidate is what you put out because you think it's ready, and just want to make sure."
    author: "Jim Dabell"
  - subject: "Re: Release candidate with known problems?"
    date: 2002-11-07
    body: "I fully agree, there are more than 3500 known bugs to fixed, IMHO, this release candidate is a beta3.   "
    author: "frederic heem"
  - subject: "Re: Release candidate with known problems?"
    date: 2002-11-08
    body: "If you wait 'till all known bugs are fixed, you would NEVER release any software. For some small projects maybe, but not for a big project like KDE. face the facts, there will always be bugs."
    author: "Janne"
  - subject: "Re: Release candidate with known problems?"
    date: 2002-11-11
    body: "There is a difference between fixing all known bugs and fixing all the important bugs.  16 grave bugs, 19 major bugs, and a ton of crash bugs ... at least the grave  and major bugs shold be considered \"showstoppers\".  They either need to be: tagged as LATER or WONTFIX, downgraded (only if deserved), or resolved.  Then and only then should KDE consider releasing 3.1"
    author: "Anonymous Coward"
  - subject: "Re: Release candidate with known problems?"
    date: 2002-11-09
    body: "The KDE project, in its role as a Microsoft wannabe (that wasn't an insult, it was a statement of fact), is going to show itself as guilty of all things from feature creep to style over substance to feature over bugfix release cycles, et al. It is not motivated, nor, some may argue, needs to be motivated, by the strict engineering practices of more critical projects. Users are used to frequent browser crashes, whether Konqueror or Opera or IE or Netscape or Mozilla. Filling in a form with lots of text? Do it in emacs first. Opening lots of windows? Perhaps it's time to fire up Opera so when your system explodes you won't have to search for them all again. This is the current state of desktop software release engineering."
    author: "Anonymous Coward"
  - subject: "Re: Release candidate with known problems?"
    date: 2002-11-09
    body: "I agree, at the very least there should be a RC3 if bugs are found in RC2."
    author: "Jesper Juhl"
  - subject: "./configure misses things"
    date: 2002-11-08
    body: "In compiling kdebase, configure gave me a warning that I needed libart and libxslt. I downloaded those things and tried a clean configure again. It still said they weren't there. I never did figure out how to make it see them. \n\nAnd in trying to build kdegraphics, make errored out on libkscan because I didn't have Xsane installed. Fine, I thought, I don't need that program. So, I went back and ran:\n\n./configure --without-kamera --without-kooka --without-libkscan\n\nI could see that, as it configured, it completely ignored these flags. So, when I tried to build again, it errored out. Apparently, there is no way to tell configure that I don't want these packages, despite what ./configure --help says."
    author: "Jim Philips"
  - subject: "Re: ./configure misses things"
    date: 2002-11-11
    body: "<< In compiling kdebase, configure gave me a warning that I needed libart and libxslt. I downloaded those things and tried a clean configure again. It still said they weren't there. I never did figure out how to make it see them. >>\n\nI suspect that you probably installed them with prefix /usr/local and it expects to see them with prefix /usr, or vice-versa."
    author: "Robin Green"
  - subject: "Re: ./configure misses things"
    date: 2002-11-12
    body: "Don't forget to remove config.cache"
    author: "dwt"
  - subject: "Re: ./configure misses things"
    date: 2002-11-13
    body: "The problem with configure for kdelibs missing libxslt and libarts is solved. It's a packaging issue. When configure appears to miss a package that you know is installed, you can bet that there is a *-devel version of that package that contains the files configure is looking for. When I installed the devel packages, it quit complaining."
    author: "Jim Philips"
  - subject: "Is clearing the history bug in konqueror fixed? "
    date: 2002-11-08
    body: " Unfortunately I don't have the time to compile the binaries myself to check this out, but I was told this bug was fixed in the cvs code back when 3.0.2 was out. Has it made it into the build yet? The bug is selecting empty contents on the konqueror location bar does indeed remove the history completions, but exiting konqueror and restarting it restores the history (as if the changes aren't being saved). This of course is highly dangerous because anyone using your browser can see exactly what sites you've been to and there is no way to remove them.\n"
    author: "David"
  - subject: "Re: Is clearing the history bug in konqueror fixed"
    date: 2002-11-10
    body: "Seems like you been surfing p00rn :P~~"
    author: "matti"
  - subject: "Re: Is clearing the history bug in konqueror fixed? "
    date: 2003-04-21
    body: "Yes,\nempty contents is working. phew...\nWas looking all over settings/configure before I found that feature too, thanks to your post.\n\nUsing 3.1.1"
    author: "Nic da n00b"
  - subject: "MAKE IT FASTER!!!!!!!!!!!!!!!!!"
    date: 2002-11-11
    body: "Make it faster, KDE IS SLOW, not as much as OS/X but it's slow. That's way I'm still using Gnome1.4+GMC\n\nAnyway, I don't know why, but Gnome2 still looks \"more professional\" than KDE3. Maybe it's that Windowish feeling KDE has, thate makes as if it was a copy of windows."
    author: "Anonymous"
  - subject: "Re: MAKE IT FASTER!!!!!!!!!!!!!!!!!"
    date: 2002-11-11
    body: "If you are complaining about speed, and you like the look of GNOME2, you should stick with GNOME2.  It is faster than GNOME1.4 (which is being deprecated in favor of GNOME2 anyway)."
    author: "Anonymous Coward"
  - subject: "Re: MAKE IT FASTER!!!!!!!!!!!!!!!!!"
    date: 2002-11-11
    body: "it's faster because it has 50% of the features of GNOME1.4."
    author: "ac"
  - subject: "I am here to say thank you:)"
    date: 2002-11-12
    body: "Very Great Work! I've just finish compiling a fresh LFS4.0 and KDE3.1rc2. The speed of kde3.1 is impressive. And I love the icons:)\nThank you guys for this wonderful work. I am looking forward the final release."
    author: "From China"
---
The <a href="http://www.kde.org/">KDE Project</a> today released <a href="ftp://ktown.kde.org/pub/kde/unstable/latest/">KDE 3.1 RC2</a>, probably
the last release candidate for <a href="http://www.kde.org/info/3.1.html">KDE 3.1</a>.
A good number of showstoppers in RC1 have been fixed, and the new default Crystal-SVG icon set has been polished based on the valuable feedback received.  Nevertheless, please give this RC2 another round of thorough testing to make sure all the major wrinkles have been
ironed out.  Please note that the kdebindings package still suffers from
compilation problems in large parts, but this will be fixed by the final
release.  Thanks to the community for the feedback so far, let's keep it up
and make the 3.1 release everything that is expected!
<!--break-->
