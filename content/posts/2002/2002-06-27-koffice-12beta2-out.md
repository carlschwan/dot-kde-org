---
title: "KOffice 1.2beta2 is Out!"
date:    2002-06-27
authors:
  - "numanee"
slug:    koffice-12beta2-out
comments:
  - subject: "KOffice for MacOSX"
    date: 2002-06-27
    body: "It's also been released to Fink unstable (no binary packages, those will be released with the upcoming KDE 3.0.2 release, doing lots of building right now =)."
    author: "Ranger Rick"
  - subject: "56 or 41 languages ?"
    date: 2002-06-27
    body: "what causes the difference?"
    author: "ferdinand"
  - subject: "Re: 56 or 41 languages ?"
    date: 2002-06-27
    body: "I doubt any of these languages is up-to-date yet as there was no message freeze before the beta was tagged."
    author: "Anonymous"
  - subject: "footnotes"
    date: 2002-06-27
    body: "FOOTNOTES ! yay! Finally, Kword is coming to the scientists....\n\nMarc"
    author: "Marc"
  - subject: "Not really"
    date: 2002-06-27
    body: "Well neither the word *.doc  filter nor the *.rtf filter import any footnotes at all.\n\nAlso the GUI for footnotes sucks, you have to configure EVERY SINGLE fottnote, instead of just entering it and changiong sensible defaults where appropriate. Well, that will hopefully be corrected. [HINT: Standard for Insert footnote should be autmatically numbering, nothing else!]\n\nWell, I guess that's why it is a beta... :-)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Not really"
    date: 2002-06-27
    body: "Too bad and I was really looking forward to write a short paper with Kword :(\n\nMarc"
    author: "Marc"
  - subject: "Re: Not really"
    date: 2002-06-27
    body: "Huh? You don't have to configure every single footnote. Press Enter on the dialog that comes up when inserting a footnote - just like in e.g. openoffice (and probably other suites too).\nThe \"configure\" button in that dialog does bring up a GLOBAL footnote config dialog, where you can choose the type of numbering they should have etc.\nSome footnotes can have a custom text (like (*) or (i)), and the other still have 1, 2, 3... that's why that's in the dialog for every footnote.\nBut pressing Enter does what you want, anyway.\n\n> Well neither the word *.doc filter nor the *.rtf filter import any footnotes at all.\nWell, one thing at a time. How could they do that before, since KWord didn't have footnotes at all? Now they _can_, as soon as someone makes them.\n"
    author: "David Faure"
  - subject: "Re: Not really"
    date: 2002-06-27
    body: "Yup, looks nice. A keyboard-shotcut without the menu and press an extra enter-key would be nice ;-) Is there an easy way to go back to the main text after entering the footnote?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Not really"
    date: 2002-06-28
    body: "You can always associate a shortcut with a menu item (\"configure shortcuts\"), but you'll still get the dialog of course.\nDoes any other word processor have a shortcut for inserting a footnote without showing the dialog?\nFor reference, openoffice and MSWord both show a similar 'small dialog' when doing Insert / Footnote.\n"
    author: "David Faure"
  - subject: "Re: Not really"
    date: 2002-06-28
    body: "In the impossible-to-figure-out Word shortcut dialog, there is an extra 'insert-footnote-automatically' (or something like that) that doesn't pop up the dialog. This doesn't appear anywhere outside of the shortcut dialog though, however it is very very useful. So just making another KAction with no dialog would be fine.\n\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: Not really"
    date: 2002-06-28
    body: "Ah, forgot to answer about jumping back. That's in the RMB menu (anywhere on the footnote frameset IIRC). No keyboard shortcut though - although here again \"configure shortcuts\" might even work for this action."
    author: "David Faure"
  - subject: "footnote implementation"
    date: 2002-06-27
    body: "OK, maybe I was too fast in handing out judgement. I am normally using lyx and there entering a footnote means I start writing the footnote, not choosing the numbering type in my 20th footnote in a row. Why configure it at all? 90% of the time people want numbers.\u00b9 If they don't, let those 10% go to the menue to configure the footnote.\n\nThis is the same concept as the starting dialog of kword, which I don't like either, even though I understand the rationale behind it.\n\nI think the other post about a way to jump back and forth between text and footnote is insightful (clicking on the footnote number is used in OO.o I think).\n\nApart from the filters deficits kword looks excellent and is a speedy sob.\n\n\n--\n\n\u00b9 Totally grabbed out of the air number.... :-)"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: footnote implementation"
    date: 2002-06-28
    body: "Look at openoffice and msword - they both have that dialog when inserting a footnote.\nI'm happy to remove it (and let it be accessible on RMB only), if that's what people want, though.\n"
    author: "David Faure"
  - subject: "Re: Not really"
    date: 2002-06-28
    body: "File a bug report..."
    author: "Norbert"
  - subject: "Re: Not really"
    date: 2002-06-28
    body: "Well neither the word *.doc filter nor the *.rtf filter import any footnotes at all.\n\nI don't know what about beta2 but KWord from current CVS can import footnotes from RTF documents. I implemented it a few days ago.\n"
    author: "Tomasz Grobelny"
  - subject: "Scientists?"
    date: 2002-06-27
    body: "More like anyone above 3rd grade"
    author: "meme"
  - subject: "Re: Scientists?"
    date: 2002-06-27
    body: "Sometimes I wonder about some scientists. They will know how to cure cancer soon, but how do I format that floppy disk again?\nBut if 3rd Graders can understand it then maybe there is hope for my supervisor !\n\nMarc"
    author: "Marc"
  - subject: "Re: Scientists?"
    date: 2002-06-27
    body: "Well, that's what they call 'division of labor'. For a top-notch cancer researcher it's just easier to give that floppy to one of his graduate students than to format it by himself. Nothing wrong with that. And who still uses floppies anyways?"
    author: "kavau"
  - subject: "Re: Scientists?"
    date: 2002-06-27
    body: "> And who still uses floppies anyways?\n\nLots of people use floppies. For example, anyone trying to install an OS on a box without a CD drive, or without a BIOS that supports CD booting. Or, anyone who wants to get a few hundred kilobytes of data to an unnetworked box and doesn't want to waste time looking for a machine with a CD burner. Or, anyone with an old unnetworked box who wants to get data out of that machine and into something else. Or, someone wanting to set up a diskless terminal server system without an expensive bootrom writer. Floppies are great because they're expendible and universal. They aren't perfect, but they don't have to be.\n\nLong live the floppy! :-)\n\n</rant>"
    author: "Carbon"
  - subject: "Re: Scientists?"
    date: 2002-06-27
    body: "You're begging the question. Who has a box without a CD drive? Or without a BIOS that supports CD booting? Or an unnetworked box?"
    author: "Chuckie Schumer"
  - subject: "Re: Scientists?"
    date: 2002-06-27
    body: "I do. Next question? ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Scientists?"
    date: 2002-06-28
    body: "<OT>\nI bought a 12 year old SparcStation IPC last weekend, and spent a night installing debian with 2 floppies. (the only 2 good ones I could find)\n\nIts only 25mhz, but hell, I even got the BNC (thinnet) tranceiver working, and installed X4. ;)\n\nHeh, kinda cool considering it only has a monochrome video card, and that I have to use mousekeys in X since the mouse it broke. Elephant 19\" monitor though ;)\n</OT>"
    author: "John Hughes"
  - subject: "Re: Scientists?"
    date: 2002-06-28
    body: "Cool, how does XFree86 perform on this machine?\n\nMarc"
    author: "Marc"
  - subject: "Re: footnotes"
    date: 2002-06-27
    body: "What I miss the most is something on the line of endnote/reference manager for linux. Something that can download references from the web databases (such as http://www.pubmed.org), manage these references in one or many databases and format them in the document for a given journal. I would do it myself, but I lack both the time and expertise do build such a thing.\n\nIt would also be good to have embeded latex in koffice. You could use it to build complex symbols and equations in documents, legends in graphics, etc... Something like the TeX plugins for sketch.\n\nBTW: Congrats for the LaTeX export filter! That is something I was craving for quite some time :)"
    author: "apoptoz"
  - subject: "Re: footnotes"
    date: 2002-06-27
    body: "Ever heard of pybliographic? Combined with LyX it's a dream."
    author: "fperez"
  - subject: "Check out pybliographer"
    date: 2002-06-27
    body: "It has pubmed searching and works very well with my 300 citations and lyx. It is of course bibtex based."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Check out pybliographer"
    date: 2002-06-28
    body: "Yes, I know pybib and latex. But why not include something like that for koffice? A pybibliographer frontend, I belive, would be much apreciated.\n\nIt's just a na\u00efve sugestion."
    author: "apoptoz"
  - subject: "Re: footnotes"
    date: 2007-09-05
    body: "I know this is an old old thread, but hey.\n\nOpenOffice does have the possiblity to create footnotes on the fly w/o the dialog (i mapped this to Ctrl-F in German Version of it (Choose->Tools->Customize->Keyboard->Insert->Footnote directly or sth like that), in EN use sth else as this is taken bei \"Find/Replace\")..\n\nAlso, jumping from text to footnote can be mapped in a similar way using \"Navigation->To Anchor/Footnote\" e.g. to Ctrl-J. Just highlight the fn you want to work on and hit it :) \n\nTo go back, use Pg-Up and that's it. \n\nI think that's the way it should be, though in don't know how it's done in KWord, sorry. So this is a bit on the off-topic-side-of-things.\n\nCheers\nJeremias"
    author: "JR"
  - subject: "Applications for statistic uses"
    date: 2002-06-27
    body: "Koffice beta2 is out.. great news! On the other side I simple ask: \nare there people interested in development of kparts to link every\nkoffice applications (especially Kspread) with R (..from R-Project - www.r-project.org)?\n\nCould be great too.. have an app like SPSS or S-Plus.. or an external extension\nto Kspread to do this.\n\nDevelopers hurry up! R need a GUI.. and KDE/QT is perfectly for this! ;)"
    author: "Daniel San"
  - subject: "Re: Applications for statistic uses"
    date: 2002-06-28
    body: "Rohan Sadler (rsadler@agric.uwa.edu.au) posted this: \n<p>\n<BLOCKQUOTE>What I want to know is this: can anyone give me a quote on what it will\ncost to develop a RWindows clone of the Minitab GUI. This GUI would\nsupport initially the simple six (EDA, probabilities and quantiles of\ndistributions, t-tests,one-way anova, chi-square, and simple linear\nregression), and have the potential to develop into the next level of\nstatistical analysis (glms, multivariate methods, time series and\nspatial - analytical problems common across our faculty). If the cost of\ndevelopment is comparable to present licence maintenance fees at FNAS\nthen I think our small group can argue for its adoption.</BLOCKQUOTE>\n</p>\n<p>\nTo the r-help mailing list.  If anyone is interested in kludging up and supporting a simple GUI, <B>on windows</B> for money, email him, not me.\n</p>\n"
    author: "Nels"
  - subject: "Re: Applications for statistic uses"
    date: 2002-06-29
    body: "Cool! I'm a statistician student and actually we work with SPSS. I'm surprising\nthat universities pays a lot of money with fee and didn't realize a basic tool for this\npurpose. R exists but is unusable from console; i think that a simple GUI could\nsolve a lot of problem and all the KDE apps could benefit of statistical models\nand a stabilized-killerapp like R with script capabilities.\n\nGNOME gui exists.. mmm well, if this could be called gui! Why not KDE?\nI'm not a developer but i think that existing code is there for the other\nR-gui project, how many time could be spent to do this KDE/Qt gui?"
    author: "Ogg Demon"
  - subject: "Re: Applications for statistic uses"
    date: 2002-06-29
    body: "GNOME gui for R: blah.."
    author: "Ogg Demon"
  - subject: "Re: Applications for statistic uses"
    date: 2002-06-29
    body: "Well.. look these:\n\n<h2>Developers info:</h2>\n<a href=\"http://developer.R-project.org\">http://developer.R-project.org</a>\n<h2>Possibility:</h2>\n<b>GDAL - R bindings to Frank Warmerdam's Geospatial Data Abstraction Library</b>\n- <a href=\"http://keittlab.bio.sunysb.edu/R/GDAL/\">http://keittlab.bio.sunysb.edu/R/GDAL/</a>\n\n"
    author: "AnastaSia"
  - subject: "KSpread Question"
    date: 2002-06-27
    body: "I am at work now, so I am anxious to get home and try out the new KOffice beta2.  I do have a question though about KSpread.  In 1.2beta1, I can't figure out how to center a spreadsheet on the page when priniting.  I've looked in the page setup but can't seem to find it anywhere.  This should be easy, but either it's not possible (which I can't believe since I consider this a basic feature) or I am a dummy and can't find it (most likely this option lol).  At any rate, can someone tell me how this is done?\n\nCongrats KOffice team on the new release, and thank you for continuing to give us a better office suite with each release.\n\nPaul....."
    author: "Paul Hawthorne"
  - subject: "Re: KSpread Question"
    date: 2002-06-28
    body: "I have added some more print functionality to KSpread, but couldn't finish all of what I wanted to in time. \nSorry, centerd output is on my list but wasn't ready when we encountered feature and message freeze. This has to wait for KO 1.3 :-/\nDito for \"fit to nxm pages\"."
    author: "Philipp"
  - subject: "Re: KSpread Question"
    date: 2002-06-28
    body: "Thanks for the reply Phillip.  At least now I know it's not something I was overlooking.  Things are looking good though, and I appreciate all you guys work.  Any suggestions of an easy workaround to get my sheets centered?  Thanks again for all you guys hard work.\n\nPaul....."
    author: "Paul Hawthorne"
  - subject: "Re: KSpread Question"
    date: 2002-06-28
    body: "There is no direct way to center. \nYou can only do it manually either by adjusting the page borders or by a \"dummy\" column (no borders, white background). Yes, this is no fun and everything but professional.\nBut you should at least place a wish item in the buck tracking system, so you get informed when this function is in cvs."
    author: "Philipp"
  - subject: "no good..."
    date: 2002-06-27
    body: "...I'm still waiting for a decent SVG support.\nHow can I wait to user ksvg on kde 3.1 if I have to use a gnome app to create svg icons?"
    author: "protoman"
  - subject: "Re: no good..."
    date: 2002-06-28
    body: "Karbon14 has svg export. It's not perfect but only bugreports and suggestions helps improve it.\n"
    author: "Lenny"
  - subject: "Re: no good..."
    date: 2002-06-28
    body: "And about import?\nSVG import was already on the last beta, but I could not open any svg file, so I can't work directaly with svg files."
    author: "protoman"
  - subject: "Obligatory RedHat notice"
    date: 2002-06-28
    body: "Functional RedHat RPMS can be found at www.linux-easy.com/daily/.  They're automated CVS builds, but I've installed them and preliminary testing shows that they seem to work okay.\n\nThank you Bero-Wan Kenobi.  You're our only hope.\n\nFirst impressions: way better than 1.1.  Lack of true WYSIWYG turned me off to KWord until just now.  It is a very welcome improvement.  And not as crashy!  That's always nice (maybe that was RedHat packaging).  Import filters aren't perfect.  There was no filter for the MIME type application/x-msword! I certainly hope that was packaging problem!  The WP filter isn't pretty, but it's better than nothing.  Excel filters are better but still not wonderful, but natively, it seems to work great.  Then again, I'm not exactly pushing the limits yet.  KWord and KSpread are all I need to be happy.  Import/export filters are really my only complaint so far."
    author: "ac"
  - subject: "Re: Obligatory RedHat notice"
    date: 2002-06-28
    body: "About filters, yes it could be really improved. However, we're lack of developers here. Reading format spec, coding the filter, testing, and stuff like take a quite amount of time. If you understand C++ and Qt/KDE and are willing to help, you're very welcomed. Documentation writers and QA people are also very valuable. At minimum, should you experienced bugs, please report to bugs.kde.org. And join us at koffice-devel@kde.org"
    author: "ariya"
  - subject: "Re: Obligatory RedHat notice"
    date: 2002-06-28
    body: "I'm probably most qualified to file bugs and leave it at that.  I'm an English major.  I can read code within limits, though.  Maybe I'll try to look at the filter code and see if it's dumb enough for me ;)\n\nI do realize, though, that it's unfair to criticize a product due to its inability to correctly reverse-engineer a competitor's complicated and always-changing formula, when basically the competitor doesn't have to worry about reverse-engineering anything from anyone else and can just spend its time tweaking the formula to piss everyone off.\n\nNevertheless, filters remain KOffice's most noticeable shortcoming.  Not that it's the end of the world.  But it does mean two word processors for me.  One to use personally, and another to open attachments from other people."
    author: "ac"
  - subject: "Re: Obligatory RedHat notice"
    date: 2002-06-28
    body: "To make you happy: KSpread will get WYSIWYG in 1.3, too. \nWe didn't have the time to put it in this release.\n\nAbout filter: I think enhancing the Excel filter is easier. So have fun with it :-) \nWe would like to hear from you :-)"
    author: "Norbert Andres"
  - subject: "Re: Obligatory RedHat notice"
    date: 2002-06-28
    body: "> Import filters aren't perfect. There was no filter for the MIME type\n> application/x-msword! I certainly hope that was packaging problem!\nIt seems that the input fiter is for application/msword instead of x-msword\nTry to add a new mimetype application/msword and it should work.\n\n\nAndrea\n"
    author: "Andrea"
  - subject: "Input Filters"
    date: 2002-06-28
    body: "I suppose that all of these 'filters' are useful to some.\n\nBut, what I would really like is PostScript input.\n\nI presume that this would need to use GhostScript.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Input Filters"
    date: 2002-06-28
    body: "Karbon14 has PS/EPS import by the way. The screeny at the bottom shows the\nimport of a \"dynamic\" PostScript file (with loops and rotations):\nhttp://www.koffice.org/karbon/screenshots.phtml\n\nAnd yes, we use ghostscript and don't have our own ps-interpreter (yet). ;)"
    author: "Lenny"
  - subject: "Re: Input Filters"
    date: 2002-06-29
    body: "Dumb!\n\nBut, I can't figure out how to do that on 1.2Beta2.\n\nSo, I can't say if that will do what I need or not.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Input Filters"
    date: 2002-06-30
    body: "First: most postscript files work, but for example the famous tiger.eps doesn't work yet.\nSecond: it may help if you rename *.ps to *.eps. This needs to be fixed, that's right."
    author: "Lenny"
  - subject: "Re: Input Filters"
    date: 2002-07-01
    body: "Ah, ok. Werner found a bug. Please check the next beta."
    author: "Lenny"
  - subject: "Why not command line Universal Filters ?"
    date: 2002-06-28
    body: "First of all, CONGRATS for a new release !\n\nSecond point. I always thought it is a waste of time to duplicate efforts  \nin  writting filters for the different office suites (Koffice, OpenOffice,\nGnome office apps, etc.). I really don't understand why it is not possible to\nhave command line universal filters, and higher level front ends to interface \nthe filters with each application. But basically, one should be able to run \nsomething like \"doc2kword ms-word-file kword-file\" from the command line.\nAnd then the different applications would call this command with \nappropriate/convenient parameters to convert files as the user requests it.\n\nThat would also help migration in the corporate environment. Imagine:\n\"doc2kword *\" and all your \".doc\" files get converted to a more decent format ;-)\n\nI guess my question is: why is this not implemented this way ?. Lack of cooperation ?.\nOr is there a technical reason ?.\n\nThanks,\n-- Leo"
    author: "Leo"
  - subject: "Re: Why not command line Universal Filters ?"
    date: 2002-06-28
    body: "Lack of time is the reason.\nOr let me phrase it in another way: \nStay tuned, this is not a real new idea and the people are working on such concepts already, but no code has been done yet. \nSee also: http://xml.openoffice.org/standardisation/"
    author: "Philppp"
  - subject: "Re: Why not command line Universal Filters ?"
    date: 2002-06-29
    body: "You have already a command line tool for filters in KOffice. It is called koconverter.\n\nJust try:\n  koconverter ms-word-file.doc kword-file.kwd\n(the extensions are important!)\n\nAs for supporting wildcards, well, I suppose that someone could write a script around koconverter. However, I do not know of anybody who has done it yet!\n\n-\n\nAs for your question about \"universal filters\", well, the problem is that is sounds easier than it is.\n\nIf you want to convert from format F1 to your word processor format F2 and you have a intermediate format Fi, you need filters for F1 to Fi and Fi to F2. If you have another word processor F3, you just need a Fi to F3 filter to convert F1 to F3.\n\nAt first glance, it seems to be easier than to write a direct F1 to F3 filter. However, someone still has to check that converting from F1 to F2 and from F1 to F3 work correctly. And if one of them does not, programmers are not as free as for a direct filter, because of the limitation of the Fi intermediate format and because that changing any filter to/from Fi has potential side effects on all other filters from/to Fi.\n\nAnother problem is to define this intermediate Fi format. A try is on the way. Only future will tell if it goes well.\n\nHave a nice day/evening/night!"
    author: "Nicolas Goutte"
  - subject: "sorry"
    date: 2002-06-28
    body: "I am sorry but I have to ask...\n\nWhat is that status of grammar checking?  Are there any projects working on this?  Are there any documents on this topic?  If I wanted to help with this feature where would I go?"
    author: "theorz"
  - subject: "Re: sorry"
    date: 2002-06-28
    body: "grammar checking is technically very difficult to do. and its (i think) different for every language. so i don't think you'll see it anytime soon.\n(i never heard of an OSS project doing that, maybe i didn't listen well enough)"
    author: "kervel"
  - subject: "Re: sorry"
    date: 2002-06-29
    body: "I've seen a few, and even used one (it may have been Public Domain, as it was many many years ago) that was very good.  I seem to recall that there was at least one article on the subject (using a free, where free ~= Free) grammar checker in Dr. Dobb's Journal.\n\n--\nEvan \"Listening to Hour of Slack\""
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: sorry"
    date: 2002-06-29
    body: "The original Symantec grammar checker (Grammatik) used regular expressions and a Lexx type engine to check grammar.\n\nIt would appear that this type of engine could be written for any word processor.  You would need the regular expressions for it to work.\n\nIf you want to see this, it is in WordPerfect 8 for Linux.\n\nA better grammar checker would use YACC and this would be more difficult to write.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "KWord cannot insert rows into an existing table!"
    date: 2002-07-13
    body: "I compiled the latest koffice, fired up kword to create a doc that contained a table. While writing, I decided I needed another row in the table. Then, to my great chagrin & surprise, found that this is impossible to do! The requisite menu oitems are greyed out :-(\nHow on earth did this get overlooked in the QA effort???"
    author: "Antonio D'souza"
  - subject: "Re: KWord cannot insert rows into an existing table!"
    date: 2002-07-13
    body: "Make sure you have selected cell(s), not cell content."
    author: "Anonymous"
---
KOffice 1.2beta2 is out, sporting an impressive <a href="http://www.koffice.org/announcements/changelog-1.2beta2.phtml">number of changes</a>, with improvements all around the board including substantial filter enhancements, footnotes in KWord, and templates in KSpread.  <i>"This release, which is available in
  <a href="http://i18n.kde.org/teams/distributed.html"><strong>56</strong></a>
  languages, includes
  a frame-based, full-featured word processor
    (<a href="http://www.koffice.org/kword/">KWord</a>);
  a presentation application;
    (<a href="http://www.koffice.org/kpresenter/">KPresenter</a>);
  a spreadsheet application;
    (<a href="http://www.koffice.org/kspread/">KSpread</a>);
  a flowchart application; 
    (<a href="http://www.thekompany.com/projects/kivio/">Kivio</a>);
  business quality reporting software;
    (<a href="http://www.thekompany.com/projects/kugar/">Kugar</a>);
  and two vector-drawing applications (<em>alpha</em>)
    (<a href="http://www.koffice.org/kontour/">Kontour</a> and
     <a href="http://www.koffice.org/karbon/">Karbon14</a>).
  Additionally, KOffice includes robust embeddable charts
    (<a href="http://www.koffice.org/kchart/">KChart</a>)
  and formulas
    (<a href="http://www.koffice.org/kformula/">KFormula</a>)
  as well as a built-in thesaurus (KThesaurus)
  and numerous import and export
    <a href="http://www.koffice.org/filters/">filters</a>."</i>  Read <a href="http://www.koffice.org/announcements/announce-1.2-beta2.phtml">the full announcement</a> for details.  Huge congrats to the <a href="http://www.koffice.org/">KOffice</a> team for their hard work and dedication, and kudos to <a href="http://www.kdeleague.org/">Dre</a> for writing the announcement.  Wooo!
<!--break-->
