---
title: "Several KDE Web Sites Migrated"
date:    2002-10-09
authors:
  - "chowells"
slug:    several-kde-web-sites-migrated
comments:
  - subject: "enterprise.kde.org"
    date: 2002-10-09
    body: "http://enterprise.kde.org looks broken."
    author: "Anonymous"
  - subject: "mysql database problem"
    date: 2002-10-09
    body: "The transfer isn't quite finished as we haven't finished setting up the  mysql database on the new host.\n\nApologies for any inconvenience."
    author: "Chris Howells"
  - subject: "Re: nje problem"
    date: 2002-12-25
    body: "How i can copy a MySQL databse to another computer without dump. Like attach in SQL Server. "
    author: "Saimir"
  - subject: "I thought RedHat were evil?!?!"
    date: 2002-10-09
    body: "Wait, all our bitching and moaning about the evil redhat conspiracy to krush kde doesnt look so iron klad what with the fact that redhet are donating bandwidth to us.  gosh do we look silly!"
    author: "noone"
  - subject: "Re: I thought RedHat were evil?!?!"
    date: 2002-10-09
    body: "Actually, Bero sneaked us in the backdoor on his personal server.  I doubt Red Hat US would have been particularly happy about it...  but who knows."
    author: "Navindra Umanee"
  - subject: "Re: I thought RedHat were evil?!?!"
    date: 2002-10-10
    body: "> I thought RedHat were evil?!?! \n\nHrm, no.\n\n> gosh do we look silly!\n\nUh, no actually."
    author: "Chris Howells"
  - subject: "Re: I thought RedHat were evil?!?!"
    date: 2002-10-10
    body: "Read the article:\n\ns/are/were\n\n"
    author: "Roland"
  - subject: "Re: I thought RedHat were evil?!?!"
    date: 2002-10-11
    body: "RedHat Germany were also much more friendly towards CUPS than were \nRedHat  US. They run it on their internal network since two years. I believe, \ntheir friendliness to KDE is also much older than that in their US parent \ncompany."
    author: "Kurt Pfeifle"
  - subject: "Re: I thought RedHat were evil?!?!"
    date: 2002-10-13
    body: "maybe not so silly after all."
    author: "patrick_darcy"
  - subject: "apps.kde.org"
    date: 2002-10-09
    body: "what about apps.kde.org ?!!?\n\nwhen will it be avilable again?!!"
    author: "Mohasr"
  - subject: "Re: apps.kde.org"
    date: 2002-10-09
    body: "There was no http://apps.kde.org, read http://apps.kde.com for progress information."
    author: "Anonymous"
  - subject: "Re: apps.kde.org"
    date: 2002-10-10
    body: "http://apps.kde.com is what i mean :-)"
    author: "Mohasr"
  - subject: "Re: apps.kde.com"
    date: 2002-10-11
    body: "It's now available again. And a new Kernel Cousin KDE is also ahead. Back to old strength. :-)"
    author: "Anonymous"
  - subject: "Errr...."
    date: 2002-10-10
    body: "Doesnt sourceforge.net let you use their bandwidth/space ?\nJust wondering..."
    author: "cartman"
  - subject: "Heres apps...."
    date: 2002-10-11
    body: "Shh, secret link.\n\nhttp://apps.kde.com/if/2/latest\n\n\n- NiGHTSFTP"
    author: "NiGHTSFTP"
  - subject: "I want the old logo back !"
    date: 2002-10-11
    body: "The new one sucks."
    author: "reihal"
  - subject: "Re: I want the old logo back !"
    date: 2002-10-11
    body: "I don't think it sucks, but I think the text (KDe.NEWS got the Dot?), should be centered a little bit better with the logo. "
    author: "fault"
---
Several recent unfortunate incidents, including a security breach,
have meant that a number of KDE web sites such as <a href="http://printing.kde.org/">printing.kde.org</a>, <a href="http://edu.kde.org/">edu.kde.org</a>, <a href="http://enterprise.kde.org/">enterprise.kde.org</a>,
and <a href="http://worldwide.kde.org">worldwide.kde.org</a>, have 
been forced to move to a new host. Previously these were hosted by 
<a href="mailto:bero@kde.org">Bernhard "Bero" Rosenkraenzer</a> on his server,
with bandwidth provided by <a href="http://www.redhat.de/">Red Hat Germany</a>. We would like to express 
our gratitude to Bero for all the time and effort he put into 
administering the server to allow the hosting, and to Red Hat for 
for supplying the bandwidth.


<!--break-->
