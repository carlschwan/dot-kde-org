---
title: "KVim Reaches Stable Milestone"
date:    2002-04-10
authors:
  - "numanee"
slug:    kvim-reaches-stable-milestone
comments:
  - subject: "great!"
    date: 2002-04-11
    body: "being able to work with vim from within kmail and other kde applications would be like a dream come true for me.. I'd give up kde 3 for this feature (if I had it)"
    author: "mark dufour"
  - subject: "Re: great!"
    date: 2002-04-12
    body: "The current CVS of KVim works with KDE 3 (only)."
    author: "Triskelios"
  - subject: "Using Kpart?"
    date: 2002-04-11
    body: "Okay, maybe I am just missing something, but how do you use KVim in Konqueror?  I have downloaded/compiled/installed the complete KVim package and can use KVim as stand alone without any problems(look great!).  But I can find no way to use the KPart in Konqueror.  Any instructions would be very much appreciated.  \n\nThanks!"
    author: "Kevin"
  - subject: "Re: Using Kpart?"
    date: 2002-04-11
    body: "I'm an emacs user myself so I haven't tried this, but I'd imagine you just need to change the preference order in your file associations. Open up the settings dialog and choose the file associations section, then look for text/plain and see if you've gained a KVim option under the embedding tab.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Using Kpart?"
    date: 2002-04-11
    body: "Thanks, but that is just it...I don't have a KVim or a Vim option under the embedded tab, so I am unsure of where to look next."
    author: "Kevin"
  - subject: "Re: Using Kpart?"
    date: 2002-04-11
    body: "the instructs say after running testVim go to the /kvim/vimpart/kde-version directory and run ./configure, make, make install. the problem is that there is no configure script in that directory. i tried make -f Makefile.cvs, but it errored out. so i'm looking for a little help with this too. "
    author: "jhollett"
  - subject: "Re: Using Kpart?"
    date: 2002-04-11
    body: "It would help if you posted the error messages. Maybe you have an older version of automake and autoconf installed?\n\nHere it worked just fine."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Using Kpart?"
    date: 2002-04-11
    body: "using \n\nslackware 8 distro\n*** Concatenating configure tests into acinclude.m4\n*** Creating list of subdirectories in subdirs\n*** Searching for subdirectories...\n*** Retrieving configure tests needed by configure.in\n*** Scanning for include statements\nautoheader: error: shell error while sourcing /tmp/ah6249/traces.sh\nmake[1]: *** [cvs] Error 1\nmake: *** [all] Error 2\n\ni then installed newer automake & autoconf \n\nnew automake  1.61 & autoconf 2.53\n*** Concatenating configure tests into acinclude.m4\n*** Creating list of subdirectories in subdirs\n*** Searching for subdirectories...\n*** Retrieving configure tests needed by configure.in\n*** Scanning for include statements\naclocal.m4:672: error: m4_defn: undefined macro: _m4_divert_diversion\nautoconf/lang.m4:173: AC_LANG_RESTORE is expanded from...\naclocal.m4:672: the top level\nautoheader: autom4te failed with exit status: 1\n at /usr/local/bin/autoheader line 163\n make[1]: *** [cvs] Error 1\n make: *** [all] Error 2\n\ncommand: make -f Makefile.cvs"
    author: "jhollett"
  - subject: "Re: Using Kpart?"
    date: 2002-04-11
    body: "Okay, I figured out that the error I was getting when doing the 'make -f Makefile.cvs' was from having an older version of autoconf.  After upgrading it worked great, thanks.\n\nOne note, in the output from doing the 'make -f Makefile.cvs' with autoconf 2.13 you get the following message:\n\n*** YOU'RE USING AUTOCONF 2.13. We suggest updating.\n*** autoconf 2.5x works best for now, we will drop\n*** support for autoconf 2.13 soon.\n\nQuickly followed by some errors that halts the make.  After tricking the make into getting by these errors, I was then presented with this message:\n\nFATAL ERROR: Autoconf version 2.50 or higher is required for this script\n\nI think it would be a little more useful if the first message just said this to begin with and then halted instead of trying to continue and saying that support will be dropped \"soon\" as it appears to have already been dropped.\n\nThanks again for getting kvim going!"
    author: "Kevin"
  - subject: "And next ?"
    date: 2002-04-11
    body: "You'll be happy to know that we're working on kvim-6.1, that should be released not so far from now. Featuring a sync with vim-6.1 and lot of bug fixing from the kvim-6.0 release. Also, we've started to talk with the kdevelop people to see how to make the vim part a KTextEditor, stay tuned.\nYou can come and see us on #freehackers or #kde (on the openproject network)"
    author: "Thomas Capricelli/orzel"
  - subject: "Great news :("
    date: 2002-04-11
    body: "This is great news. And I am jealous (the very first time) on all ppl who have chosen Vim.\n\nToo bad I hat it like a disease.\n\nI also hate all these amateurish editors being integrated into the UI, something that got invented on WindOwS and just proves how many Linux programmers are converted Windows programmers/users.\n\nAnother such stupid Windows-Inheritance is that also in Gnome and KDE the Filemanager handles directories just as they would be documents.\n\nOf course, this does not change my passion with KDE a slightest bit ;-)\n\nSo, when do we see KEmacs and its according KPart ?\n\nI still do not understand what this is/was about it.\n\nIt shouldn't be so hard to wrap around it, export all the elisp functionality to dcop and have it embedded in KDevelop...\n\nUhuhuh...;-)"
    author: "jon"
  - subject: "Re: Great news :("
    date: 2002-04-11
    body: ">Another such stupid Windows-Inheritance is that also in Gnome and KDE the Filemanager handles directories just as they would be documents.\n\nI'm kinda curious about what you mean by this. How would you like it to be done?\n"
    author: "jd"
  - subject: "Re: Great news :("
    date: 2002-04-11
    body: "I guess he means that there's only one filelist in most filechoosers, where he would like to see a list containing the directories and another one (next to it) with the actual files. Though the current behaviour doesn't hinder me, that seems like a good idea. Though directories are in fact just another file, they differ quite a lot from the user's point of view. I believe the old KDE1 did it this way: at least my KLyX that I still use has a separate dirlisting left of the files."
    author: "Jonathan Brugge"
  - subject: "Re: Great news :("
    date: 2002-04-11
    body: "Hey, you know what, I love KDE because it is so fantastically flexible and configurable -- in this case :\n\nRight click in the fileselector window, and you are given the option (amongst others) to have dirs and files separate.\n\nIn fact, try right-clicking on all widgets : there is a wealth of config possibilities in all of them !"
    author: "hmmm"
  - subject: "Re: Great news :("
    date: 2002-04-12
    body: ">Hey, you know what, I love KDE because it is so fantastically flexible and >configurable -- in this case :\n\nWell, it is configurable only on the surface, superficially...\n\n>Right click in the fileselector window, and you are given the option (amongst >others) to have dirs and files separate.\n>In fact, try right-clicking on all widgets : there is a wealth of config >possibilities in all of them !\n\nYes, it is a begniing. Especially the embedded console is a good idea.\n\nBut this still is an excuse of a filemanager, not more. It seems to me you never used a 'real' filemanager, you would see how much this is worlds apart.\n\nThe only really good filemanager I came acrosson Linux was Worker and MidnightCommander. \n\nThe v6 version of DirectoryOpus is (Windows only, commercial) here:\nhttp://www.gpsoft.com.au/Intro.html\nAnd some screenshots are here:\nhttp://www.gpsoft.com.au/preview.html\n\nA screenshot, demonstrating the power of filemanagement on AmigaOS:\nhttp://www.ruhr.de/home/kily/gfx/dopus55s.JPG\n\nNo eyecandy, though. Note, that each of the four listers you see here is a normal window, they have been arranged (probably by a keypress) to occupy the screen in this state. Of course all is supported (cut,copy&paste, drag&drop) as well as doing all kind of funny things (all basic tasks like copy,move,rename,link,archive,dearchive) and whatever by the buttons you see. Also note, that the \"listers\" can be used in icon-mode as well.\n\nI still do not see any way to archive a bunch of files with Konqueror without opening the embedded console or maybe drag&drop into an opened archive.\n\n\nOh, and since we are at screenshots and my favourite computer system, have a look at here:\n\nhttp://www.trotta.de/club/index2.html\n\nQuite nice for an OS from 1992 ?!"
    author: "jon"
  - subject: "Re: Great news :("
    date: 2002-04-12
    body: "Directories /are/ files.  They're just files with links to other files.  In fact, rather than separate out directories, they should be handled like files even more than they are, and files should look a lot more like directories.\n\nDirectories should be able to have mime types and associated applications.  I'd like to be able to create NeXTSTEP-like .app folders that are executed like applications.  Files should have directory attributes; I'd like to create associations between files, so that files can be browsed like directories.\n\nThe separation of directories and filesystems is a limited metaphor.  BoOS, with its DB-based FS was a step in the right direction, but encountered some engineering problems.  A very good example of what filesystems /should/ look like is at http://www.reiserfs.org/whitepaper.html.  In the short term, artificially increasing the similarity between file nodes and directory nodes is a good thing to do."
    author: "Sean Russell"
  - subject: "Re: Great news :("
    date: 2002-04-16
    body: "You mean that it should be possible to specify drop actions like running\na certain program when a file of a certain mime-type is dropped on a folder?\n\nThis would be a very good idea, but this is not somthing that should be handled\nby a GUI like KDE. This should be handled at filesystem level. That way we get\nno inconsitent behavior if files are moved or copied with other tools or programs\nthan the filemanager."
    author: "Uno Engborg"
  - subject: "Re: Great news :("
    date: 2002-04-12
    body: ">I'm kinda curious about what you mean by this. How would you like it to be >done?\n\nWell, just check it out. Open up Konqueror in Filemanager mode.\nThe most blatant hint you will get (at least if you use the german locale) is the very first menu, it is named: \"document\" (or maybe \"file\" in the english locale.\n\nNow you should get it.\n\nIf still not, have a look at the \"Edit\" menu. You find the Cut,Copy&Paste items there. Typical for a document-application. (I think, however, that a \"real\" filemanager should have these as well, since it is a good idea, but it should not be the only way to copy/move files besides drag&drop.)\n\nThis kind of interface to access directories was invented by Microsoft. It is a good idea onthe one hand, since it adds consistancy to the user interface.\n\nOn the other side it is a horrbile idea, since a directory is completly different from a two document like a Word file, spreadsheet or DTP doc.\n\nAlso the treeview is not the best way to represent deeply nested directories. As soon as it is deeper than three or four levels you find yourself resizing the tree-panel already. And this just to name one of the many misconcepts in this.\n\nAlso, with these programs (IExplorer, Nautilus and Konq) it is difficult to do power-filemanagement, since the windows are designed in a way, that it is impossible to have several open (on a normal reso like 1024x768 or 1280x1024) without cluttering each other, at least in parts (I am talking of more than 3 or four windows here).\n\nEven than, there is simply no way to add user-defined buttons/menus/shortcuts or popups, one of the most important feature of any decent filemanager.\n\nA \"real\" filemanager has at least two identically set panes next to each other. Even better four (two on each side but breowsable) even better - as many as you like, layouted in a fashion that allows quick and fast access to many such panes (or better name them \"listers).\n\nThere are some attempts to give Linux a good filemanager. \nKrusader, which is too simple.\nGentoo (the filemanager, not the distro ;-)) which is better.\nEven better is Worker.\n\nNote, that Worker and Gentoo claim to try to imitate a program they reference as \"Dopus\". Which is short for \"DirectoryOpus\" the mother of all Filemanagers (it was introduced on AmigaOS and was fully scriptable and configurable. On AmigaOS, configuration of an app means more than moving floating panesl around and adding another theme, it measn usually tailoring all buttons and menus with commands/scripts the user wants.).\n\nDopus got even better in V5 (and now on Windows it is reviewed as one killer application onmany sites, it is v6). On Amiga it replaced the whole desktop and offered as many views/listers as one wanted. I would like to see a similare approach (without loosing KDE as desktop) with kparts and whatever."
    author: "jon"
  - subject: "Re: Great news :("
    date: 2002-04-12
    body: "Try menu Settings -> Load Profile -> Midnight Commander\n\nAs a matter of fact, Konqueror is quite customizeable, you can rearrange its windows  as you like."
    author: "ac"
  - subject: "Re: Great news :("
    date: 2002-04-13
    body: "I have commented on that already.\n\nThis \"MC\" mode is not really what I had in mind. You might want to read my previous posts (a few lines up) to see what I am talking about.\n\nUnless you did not see it, please do not suggest Konqueror's MidnightCommander mode.\n"
    author: "jon"
  - subject: "Re: Great news :("
    date: 2002-04-15
    body: "Actually, the first menu is named Location in the English version (at least in 2.2.2). Also, it's important to note that Konqueror is _not_ a file manager. It's a multipurpose app. CC&P is certainly appropriate for a web browser, which Konqueror is, and for many other things that Konqueror does (like PDF viewing, etc, etc). However, I do agree that Konqi's interface is a little contrived at parts. I remember the Konqueror usability study, that had some great ideas that were consequently ignored. Strange, that...\n\nAlso, add TkDesk to the list of file managers that are good. Practically everything is scriptable. It doesn't have that multi-pane mode on by default, but you can set that easily enough.\n\nYeah, it should at least be possible to remove the file manager part and replace it, the same way that you can replace KHTML with Gecko for HTML rendering (though hopefully a little bit more stable :-)."
    author: "Carbon"
  - subject: "Re: Great news :("
    date: 2002-04-21
    body: "\"Actually, the first menu is named Location in the English version (at least in 2.2.2).\"\n\nAh, okay, thanks for reporting this. So I do not fall on my nose in any later \"rants\" ;-) \n\n\"Also, add TkDesk to the list of file managers that are good. Practically everything is scriptable.\"\n\nThat sounds good. I will check it.\n\n\"Yeah, it should at least be possible to remove the file manager part and replace it, [...]\"\n\nYes, a new filemanaging KPart would be nice."
    author: "jon"
  - subject: "I can't read it."
    date: 2002-04-11
    body: "It's a nice idea. But I can't seem to configure it with any color scheme that is even remotely readable. Using gvim for now."
    author: "Jim"
  - subject: "Re: I can't read it."
    date: 2002-04-11
    body: "Yes, having just tried it, I must admit that I had the same problem.\n\nI use gvim with the default colour scheme, and that's fine for me, but kvim with the default scheme seems to not have the same amount of contrast...the colours are all a little too light, the dark blue colour scheme was the closest to being useable for me, but I prefer a white background."
    author: "Stuart Herring"
  - subject: "Re: I can't read it."
    date: 2002-04-11
    body: "have you tried the full vim package ? or only the kvim binary ?\n(any screenshot i could see to compare your kvim and your gvim ?)\n\nCheers,\nMik\n\n"
    author: "Mickael Marchand"
  - subject: "Re: I can't read it."
    date: 2002-04-12
    body: "I grabbed the latest development version from CVS, and installed using the kvim copy of the vim runtime.\n\ngvim is 6.1 as provided in debian unstable.\n\nIt appears that it's no so much that the colours are bad, but the the interpretation of the syntax highlighting is different.\n\nI've posted screenshots on http://www.cumulo-nimbus.com/vim\n\nI verified that default.vim was identical in both gvim and kvim, and I copied the c.vim from the gvim installation to the kvim one, so they were the same too (though they only differed by a couple of lines relating to comments originaly anyway)\n\nYou can see that the colour test pages are nearly identical, whereas the c syntax examples are quite different.\n\nAnother thing I noticed is that kvim seems to handle fonts weirdly...when you select a font in the font selection dialogue, the one you actually get is different, and I'm unable to get the same font I use in gvim at all....though font issues may be more of a problem with the KDE libraries in general...."
    author: "Stuart Herring"
  - subject: "Re: I can't read it."
    date: 2002-04-12
    body: "thanks a lot for the screenshots, now i know what i have to fix ;)\n\nthis is a really 'strange' bug, it will need some investigation\nwhat I can't understand is why the color test is ok whereas the real C syntax highlighting looks different, maybe i forgot something in syntax highlighting in KVim.\n\nConcerning the fonts, \nI tried to stay X (and gvim) compatible and keep compatibility with XFLD font names : courier-bitstream-*-*-*-14-r-m-*-*-*\nI know think it was an error as QT badly handles these (look qfont.html for more precision about it)\nso, soon in CVS (probably monday), you'll have only QT fonts supported (and better supported), then any setting saved in your ~/.gvimrc will have to be rewritten like : \"courier [bitstream]:10\" , XFLD names won't be supported anymore. \nThat means if you want to save your font setting for kvim in your ~/.gvimrc, you should use something like : if has(\"gui_kde\") set guifont=courier [bitstream]:10\nso that it does not give an error when you start gvim (gtk vim) ;)\n\nbut this is not yet ready so stay tuned :)\n\nCheers,\nMik\n"
    author: "Mickael Marchand"
  - subject: "Re: I can't read it."
    date: 2002-04-12
    body: "I highly suspect it is messing with the background hint.\n\nVim has two background modes: dark and light, and sets its colorscheme accordingly. "
    author: "Philippe Fremy"
  - subject: "What I find strange..."
    date: 2002-04-11
    body: "...is the argument in the FAQ that there was not enough space in KDE's CVS repository. Where's that from? There's gigabytes of free space on the CVS server..."
    author: "anonymous"
  - subject: "Re: What I find strange..."
    date: 2002-04-11
    body: "I used to see while using anoncvs\n\ncvs could not create /home/kde/tmp/fkjbgosdvjb: no space\n\nOr something like that. Maybe that's what they are talking about?"
    author: "ac"
  - subject: "Re: What I find strange..."
    date: 2002-04-11
    body: "I discussed with someone on IRC which explained me that the CVS server was quite full at the time we wanted to add the Vim runtime package in CVS (note that full KVim CVS is more than 16 Mb). cvs.kde.org may have a lot of space, probably some mirrors don't have so much.\n\ncheckouting kdenonbeta already take hours ;), adding the 16 Mb of KVim would not have helped it. Morover, KVim cannot be anymore considered as a alpha or beta program, that seemed logical to us to have our own repositery. \n\nBut don't worry Tuxfamily is a good hosting place :)\nCVS is fast and has a lot of space, this is all KVim needs :)"
    author: "Mickael Marchand"
  - subject: "How to download ?"
    date: 2002-04-11
    body: "I have looked at the KVIM homepage but I fail to find any mention of where to download the source. ftp.tuxfamily.org seems only to contain some Exim files and a mirror of the linux kernel sources. \n\nDid I miss something ?"
    author: "Claus Rasmussen"
  - subject: "Re: How to download ?"
    date: 2002-04-11
    body: "you probably missed the links on our download page ;)\ncheck our download page carefully, section \"What do you need ?\", there are links but these are not displayed correctly (no underline): the filenames are the links, click on them.\nWe will correct this ;)\n\nSorry for the inconvenience,\nMik\n"
    author: "Mickael Marchand"
  - subject: "Re: How to download ?"
    date: 2002-04-11
    body: "> the filenames are the links, click on them.\n\nYep, thats it ! I have waited for KVIM soo long, so I was quite frustrated being unable do download the thing :-) \n\nThanks for your good work.\n\n -Claus"
    author: "Claus Rasmussen"
  - subject: "I had a dream"
    date: 2002-04-12
    body: "... of having vim in my html textareas. I could edit wiki (http://twiki.org) \nwith vim. Will that be possible having a kvim part?"
    author: "wiki"
  - subject: "Re: I had a dream"
    date: 2002-04-12
    body: "it looks like you(someone ? a khtml coder ?) will have to hack this into khtml ;)\nbut this is not yet supported\n\nMik"
    author: "Mickael Marchand"
  - subject: "Re: I had a dream"
    date: 2002-04-13
    body: "As an even better solution, right click on a textarea to get an option of \"edit With...\".  Then you can use a nice editor to spellcheck and so on.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: I had a dream"
    date: 2002-04-14
    body: "Well... It's good that this works at all, but I wouldn't go so far as to call it \"even better\". Having the choice of using a real editor instead the basic text widget is good. \"Even better\" would be what he suggests: not being confronted with a basic, feature-less text widget, but having a real editor from the get-go, anywhere and everywhere.\n\nWould make working with Zope much nicer too."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Trolling around"
    date: 2002-04-12
    body: "EMACS rulez!\nVi SUCKS!"
    author: "troller"
  - subject: "Re: Trolling around"
    date: 2002-04-15
    body: "Ah but ViM rulez!!!!!!!!!!!!!!"
    author: "Bob"
  - subject: "Great! But I want a KXemacs!! :)"
    date: 2002-04-12
    body: "Yes it is great news :))\nAfter using vim for a long time, I end up moving to Xemacs though.\nI still use vim sometimes, mostly when I'm working in console, but vim\nis just not enough for my needs.\nSo, yes I love the idea, many people will use it a lot, I'll probably use\nit sometimes, but I want a KXemacs too!!! :-)\n\nJ.A"
    author: "J.A."
  - subject: "KDE Icons, Where are they?"
    date: 2002-04-14
    body: "Hi There.\n\nFirst of all, great work! I had been waiting for this to happen for a long time and always knew that it would be just a matter of time. We have also startded up porting Emacs to QT/Kde but it is an enormous task.\n\nAbout the icons in your screendumps, how do you get Kvim to pick up the KDE icons instead of the one's from GVim? Has anyone gotten Kvim with KDE icons instead of GVim? This totally throws of the uniformity of this wonderful application.\n\nTanks,\n\nJargs"
    author: "jargon"
  - subject: "Re: KDE Icons, Where are they?"
    date: 2002-04-14
    body: "It would really makes sense to have a K[x,]emacs. I think it's probably even more difficult than porting vim. I won't do it, though, I really don't know emacs..\nWith respect to the icon, we know lot of people want the kde icon. We were actually using them, but as some vim icon are missing from kde\nstandard icon, we had to switch back to vim/gvim icons. We'll try to get the missing ones."
    author: "orzel"
  - subject: "vim probs"
    date: 2002-04-14
    body: "Hey, does anyone know why I keep getting the following errors when I load vim? Something changed a bit ago and I don't know what, but other users don't get this.\n\nviminfo: Illegal starting char in line: ^?ELF^A^A^A\nviminfo: Illegal starting char in line:\nviminfo: Illegal starting char in line: \u00d5^F\nHit ENTER or type command to continue\n"
    author: "Kev Kde User"
  - subject: "Re: vim probs"
    date: 2002-04-14
    body: "Hey, better report this kind of error on our mailing list : \nkvim-dev@freenux.org. Seems like you are editing some exe. How do you load kvim ? Is the part or the whole application ?"
    author: "orzel"
  - subject: "Re: vim probs"
    date: 2002-04-14
    body: "No - this isn't specific to kvim - it's been happening to me for a bit on \nany vim session. There must be some binary in one of my config fils, but I'm not enough of a vim expert to know yet. Was just wondering if this struck anyone.\n"
    author: "Kev Kde User"
  - subject: "Re: vim probs"
    date: 2002-04-16
    body: "you seem to have an elf file setup so it is read as a config file. \n\nIt will be in ~/.vimrc ... first cat this and check that it is a text file and then follow the links from there ... somewhere you will be reading in a binary file"
    author: "samuel"
  - subject: "Re: vim probs"
    date: 2002-04-15
    body: "\nLooks like your .viminfo file is an elf executable. You can simply delete it, viminfo only stores some kind of session information."
    author: "Philippe Fremy"
  - subject: "Re: vim probs"
    date: 2005-12-07
    body: "when i open any file using vi editor, when i quit at the command line, it is giving \"cant write viminfo file [NULL!]\"\nI ve deleted .viminfo file also. it repeats.\n\nsuggest me the best solution to this prob.\n"
    author: "srinivasu maddula"
  - subject: "Re: vim probs"
    date: 2006-12-21
    body: "the above given solution works for me.\n\ni.e. remove all .viminfo files present in your home directory.\n\nI use following command in my home directory:\n\nrm -r .viminf*"
    author: "ICE"
  - subject: "Re: vim probs"
    date: 2007-05-10
    body: "Its due to short of memory space.\nWhen u dont ve memory to write, how a vim can write?\ndelete some junk files and do again\n-cheers"
    author: "Srinu"
  - subject: "Re: vim probs"
    date: 2002-08-07
    body: "Same happend to me. I use SuSE-Linux 8.0. Don#t why, but the .viminfo went corrupt. so I just copied a .viminfo from another user, of course this was an original, not changed .viminfo-file. Everything was o.k. The question' that remains is: why did this happen?\n\nyour Paul"
    author: "Paul Raab"
  - subject: "Re: vim probs"
    date: 2002-08-13
    body: "I meet the same problem after I hasn't cleanly reboot the Linux OS(for example reboot by using reset button:).\nThe .viminfo file maybe become binary when system restart and automatic fsck failed."
    author: "Mike"
  - subject: "Re: vim probs"
    date: 2003-02-25
    body: "Deleting this file worked for me on Win2k with gvim 6.1\n\nMy disk filled up a little while ago. I think this is what caused the problem with me."
    author: "Sacha"
  - subject: "Re: vim probs"
    date: 2008-05-07
    body: "After removing Vim7.1 I then searched for any instances of viminfo by using wildcard search *viminfo*. I found about 3 instances of it after uninstall. Once removed I reinstalled Vim7.1 and it worked fine! I started having problem after bad install of XP Service Pack 3. "
    author: "vim_man"
  - subject: "[unrelated]"
    date: 2002-04-15
    body: "is there any kde database(mysql) editor avalible? "
    author: "Andreas"
---
Thomas Capricelli, Philippe Fremy, and Mickael Marchand are pleased to present the first ever stable version of <a href="http://www.freehackers.org/kvim/">KVim</a>, finally bringing us <i>"the power of VIM with KDE's friendliness"</i>.  In case that means nothing to you, <a href="http://www.vim.org/">VIM</a> stands for Vi IMproved and has become the defacto standard version of Vi on most Linux distributions. Vi is the traditional, ever popular, text editor on UNIX systems which can sometimes be <a href="http://www.cs.mcgill.ca/~navindra/editors/#tth_sEc7">a challenge to new users</a>.  As the names imply, VIM adds many powerful improvements over Vi, and KVim wraps the whole thing up in a <a href="http://www.freehackers.org/kvim/screenshots.html">nice KDE interface</a>.  Best of all, KVim offers a <a href="http://developer.kde.org/documentation/tutorials/kparts/index.html">KPart</a> for out-of-process embedding in KDE.  The KPart can currently embed either of GVim or KVim in Konqueror, but further work is required before proper support for <a href="http://www.kdevelop.org/">KDevelop</a>, <a href="http://kmail.kde.org/">KMail</a> and <a href="http://kate.kde.org/">Kate</a> is available. It also <a href="http://www.freehackers.org/kvim/faq.html">looks like</a> KVim will be integrated into the main VIM source distribution in the future.  Great news!
<!--break-->
<p>
<i>Side note:  Interestingly, one of the first articles ever on KDE Dot News concerned the <a href="http://dot.kde.org/969515551/">first developer release</a> of KVim.  It generated quite a bit excitement at the time, although it seems we jumped the gun on the KDE/XEmacs rumours. :-)</i>
</p>