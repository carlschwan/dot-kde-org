---
title: "People of KDE: Chris Howells"
date:    2002-01-30
authors:
  - "Inorog"
slug:    people-kde-chris-howells
comments:
  - subject: "Strange question"
    date: 2002-01-30
    body: "I wonder, has anyone answered anything but \"The dishwasher\" to the question about who washes the dishes? It seems that KDE developers have a particular distaste for washing dishes manually.\n\nHmm, I wonder what the tally is for BSD or Linux kernel hackers, or GNOME developers perhaps..."
    author: "Carbon"
  - subject: "Re: Strange question"
    date: 2002-01-30
    body: ">I wonder, has anyone answered anything but \"The dishwasher\" to the question about who washes the dishes? It seems that KDE >developers have a particular distaste for washing dishes manually.\nSimply because answering my wife or girlfriend is not politically correct ;-P"
    author: "Chouimat"
  - subject: "Re: Strange question"
    date: 2002-01-30
    body: "Err, if you had the choice between using a dishwasher and doing it by hand, would you choose to do it by hand?  You think it's strange that people like conveniences?  I'll bet they wash their clothes in a machine, too ..."
    author: "KDE User"
  - subject: "Re: Strange question"
    date: 2002-01-30
    body: "Well, that depends. Not everybody has a dishwasher. Myself, give a choice between a top of the line dishwasher and washing machine or a top of the line SGI workstation, well.... there'd be no contest :-)"
    author: "Carbon"
  - subject: "Re: Strange question"
    date: 2002-02-02
    body: "Well I remember years and years ago ;-)), when Waldo and I just had our own apartment, doing the dishes together was a great way of catching up on eachothers day and have some nice chats.\nNow, we have a dishwasher and I must admit it has create a void in our relationship ;-)\nOn the other hand it has created a new bond, teaching how to stock the dishwasher. \nWhat is it with men and aspecially developers that they seem incapable of lining up cups and plates?\n\nJust pondering.......\n\n--\nTink\n"
    author: "Tink"
  - subject: "Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "I am sure you miss the speed of KDE 1, I wonder how could you work on KDE 2.x on such a decent PC for KDE 1 and not for KDE 2.x which needs 500+MHz and still it won't surpass the speed of KDE 1 :( \n\n\nKDE3 = speed of KDE1 + features of KDE 2..."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Are you insane?\n\nKDE2 runs fine on an AMD K6 233!\n\n"
    author: "Charles Samuels"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Hey, its Asif we're talking about. he has to get his dig in on KDE2 at least once per story. if it isn't speed that he's complaining about, it's a laundry list of features. on a positive note, he manages to consistently post off-topic."
    author: "Aaron J. Seigo"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "As long as he's consistent... :)"
    author: "Charles Samuels"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "He probably ....\n\n1) compiles Qt with exceptions\n2) compiles a totally unoptimized KDE\n3) has apache, ftpd, samba, cupsd, sshd, inetd, and a few other services running in the background\n4) confuses startup time with runtime performance\n5) has a ton of bookmarks slowing Konq 2.x startup further\n\nwith the net effect of his KDE feeling as slow as a dog.\n\nMaybe he also uses gcc 2.91, making things worse yet."
    author: "Neil Stevens"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "I don't know this guy at all but..\n\nShouldn't KDE have decent performance *without* optimizing it? \n\nI know for sure that he's not the only one with the opinion that KDE got bad performance.\n\nNope, I'm not expecting an interesting discussion out of this.\n\nregards,\nFrans"
    author: "Frans"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Not true. The thing about it is, KDE provides only source. The types of optimizatinos he's referring to all occur in the compile or runtime stage, which the kde project doesn't have direct control over.\n\nBesides, often KDE is blamed for the wrong thing. Before saying that KDE is slow, try:\n\n- Patching your kernel to be preemptive\n- Compiling your kernel with the nifty-fast Intel compiler\n- Compiling your kernel, XFree86, and any other base software you use with maximum compile time optimizations.\n\nThat should help considerably."
    author: "Carbon"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "My kernel is preemptive(2.4.17), compiled with ordinary gcc 2.95.3. Xfree 4.1 compiled from source, no optimization(except i686).\n\nCompiling glibc would probably help some..\n\nBut nothing of this affects memory usage.. right?\n\nAnd my definition of \"optimization\" includes fast, clean, small  written code combined with good design/system architecture. I'm not saying that KDE hasn't the stuff I mentioned(I actually don't know); no one really explain to me why KDE consumes so much resources.\n\nregards,\nFrans"
    author: "Frans"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "KDE Consumes VERY little resources!\n\nHalf of our users say: \"I want animated cursors and neon window borders and 6 different sets of icons, all at the same time!\"\n\nThe other half say: \"KDE is too slow\" or \"KDE takes too much memory\"\n\nWell here are a few facts:\n\ta) memory usage is proportional to features.  You just CAN'T have something with a lot of features be as fast as something with minimal features (so as long as the one with minimal features isn't slow as hell because of lousy coding :)\n\tb) top is wildly inaccurate at reporting memory usage.  Pretty much the only thing it's useful for is CPU usage, and even then it's not very accurate.\n\n\nKDE developers try to take the middle ground.  We try to have a fair amount of features (so it doesn't get TOO slow).  But we also write tight, fast code, sometimes at the sacrifice of features.\n\nNaturally, this causes lusers, such as yourself, Asif, to complain about either speed or features, since you've not mastered the fine art compromise.\n\nThis compromise means that KDE takes a minimum of 64mb of ram, but somehow, we manage to squeeze in about 80mb worth of features.  I've used it for a long time with 64mb.  It worked fine so as long as I didn't run gdb or gcc.\n\nHow about you stop complaining and thank us for the fantastic job we've done.. and then move to afterstep or windowmaker which might take less memory, because it has fewer features?\n"
    author: "Charles Samuels"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Hallelujia! \nI, for one, am blown out of the water that KDE is as fast as it is, given what it does, how well it does it, and the fact that it's largely *NIX cross-platform. Congrats, to the KDE folks. To the whiners, go run twm.\n\nAnd, I'd like to ask, how bad is it *really* when something takes, say 3 seconds to start vs 2?"
    author: "Shamyl Zakariya"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Hey.. Shamyl and Charles, we're one the same side. I've submitted numerous bugreports and working on fixing bug #37250. I know, it's a _very_ basic thing (not that basic to me, I'm still on the \"C++ for Dummies\" level which means I need to look up how to construct a while loop..) but it's the idea that counts.  \n\nAm I accepted now?\n\n> And, I'd like to ask, how bad is it *really* when something takes, say 3 >seconds to start vs 2?\n\nWhy ask me? Isn't it a fact that alot of people whines about performance? If you put 3s vs 2s in a situation I will give you my opinion. +-1sec in KDE startup means nothing but rendering a HTML page is a whole other thing. Don't you agree?\nKDE is fast if you _know_ how to do it, if you're a developer. In my opinion, the only thing that's interesting is how good the official releases included in distributions perform ,causes that's what the majority of the users will run.(well, perhaps not right now but KDE's target audience is those who run the big distributions without the knowledge how to apply all the fancy speed tweaks).\n\n\nI've posted an extensive post on kde-linux regarding KDE's performance, let's continue there. I would really be glad if you read my post and perhaps commented:\n\nhttp://lists.kde.org/?l=kde-linux&m=101216791307872&w=2\n\n(btw, did I at all mention top? I'm just listening to the harddisc swapping like a mad dog ;)\n\nOh, one last thing; Perhaps I am a troll, perhaps(probably) I am just flaming and posting nonconstructive. BUT, that's not my intention. Do you think I am trolling? Tell me, and I'll make a better try next time.\n\nbest regards,\nFrans Englich"
    author: "Frans"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "If the distributions don't come with optimizations, then the problem is not with KDE, it is with the distros.  Go complain to them.  They can and should provide the most optimized KDE possible, but the KDE project cannot influence how they compile their packages.\n\nKDE developers already think about speed, and they don't need you to tell them to do so.  From their point of view you are just another person whining about performance (though I see you are working to become a developer, which is great).  There are no easy explanations for exactly why KDE takes up X MB of RAM or takes X seconds to do action Y.  The developers can't just tell you \"oh, the reason KDE is slow is _this_.\"  Of course people are working on making KDE faster, for example Waldo was doing several performance-related things for KDE 3.  And the GCC guys are working on their end of things with the new prelinking stuff.  "
    author: "not me"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "That sounds fair enough.\n\nAnyway, as always these discussions at the dot always freak out..\n\nregards,\nFrans"
    author: "Frans"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-02-01
    body: "\"KDE's target audience is those who run the big distributions without the knowledge how to apply all the fancy speed tweaks\"\n\nIs this generally accepted to be true?  I always assumed that since most KDE developers are volunteers, that they are their own target audience...aren't they making the DE that *they* want to use, first and foremost?\n"
    author: "LMCBoy"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-02-02
    body: "\"It is our hope that the combination UNIX/KDE will finally bring the same open, reliable, stable and monopoly free computing to the average computer user that scientist and computing professionals world-wide have enjoyed for years.\"\n\nQuoted from http://www.kde.org/whatiskde/index.html\n"
    author: "Bill"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Well, I for one like KDE a lot.  I recently reinstalled my work machine, and I kept my old OS-rotted Windows partition, but have yet to reinstall Windows cause KDE works so well.\n\nOf course there are improvements we can make, that's why people are still interested in getting involved, but it does pretty well these days.  I'm particularly hoping the usability people can help make KDE a sleeker to use. \n\nAs far as the startup times go, isn't that largely the linker, and that whole objprelink business?  Supposed the gcc/g++ people were looking at that.  Anyone know if any progress has been made?\n\nEE"
    author: "Eric E"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2003-10-28
    body: "i'm not shure but i think something is wrong with my machine. i use an amd duron 1.3 Ghz, with i GB of sdram and an nvidia geforce fx 5200. the hardware accelerated driver from nvidia are also installed. \n\ni use kde, i compiled it from source and i'm very happy with it's look, it's features. but i can't run opengl apps like quake3, wolfenstein on kde. i tried with the fastest preferences in the game, lowest resolution... no chance, it is not gamable. on afterstep, i have no problems with the games, best-quality-preferences are really usable. \n\ni wonder if kde would be faster if i'd install from binary packages. \n\nbut anyway, i use kde, i like it, i can change to afterstep just for gaming.\n\ncongratulations to all the kde developers, well done!\n\ngreetings"
    author: "tizz"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "ugh ?\n-patching your kernel to be preemptive.\nthis is to get LOWER LATENCIES. your computer won't be faster.\n-since when can i compile my kernel with a non-gcc compiler ?\n-those compile time optimisations won't do a lot.\n\nplease. We're not encoding mp3's or playing movies here. We're running a DE.\n"
    author: "ik"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: " -patching your kernel to be preemptive.\n this is to get LOWER LATENCIES. your computer won't be faster.\n\nIt won't calculate faster, but it will respond faster if under load..\nIt will feel faster and more responsive, okay?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "Pre-emptive patching will help the system to respond faster, including the DE, if it's under load, or if it's doing something time sensitive (like aRts)\n\nYou can compile the kernel with a non-gcc compiler. STFW.\n\nCompile time optimizations will help. It probably won't be more significant then, say, 10 or 15%, but it will be noticable, especially if they're applied to the things that are doing the most low-level work (kernel, XFree, etc)."
    author: "Carbon"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "How you compile is as important as what code you're compiling.  Try khtml/konqueror compiled with egcs 1.1.2, then try it with gcc 2.95.3, then see if you still wonder how important the build is to KDE performance."
    author: "Neil Stevens"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "Hmm.  Which one is faster?\n"
    author: "Navindra Umanee"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-02-01
    body: "Building any release software without optimizations with gcc is a very bad\n idea -- since according to gcc developers themselves [see gcc mailing list archives], the code produced by gcc without optimizations is very slow, while\ncode compiled with even simples one is good.\nThat is hardly KDE's fault, of course, and it's simply a quirk of the compiler.\n\nFurther, by default, gcc will build code for a 386...\nThe chip in your machine is very different, and can do things\nmuch faster even with slight rearrangements on the instruction stream..\n\n"
    author: "Sad Eagle"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: ">4) confuses startup time with runtime performance\n\nInteresting point. KDE often gets bashed of its slow startup time, but runtime performance is much more important. I don't really know why the startup time is so important.\n"
    author: "Loranga"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "Agree.. I never close my apps, having Konq/Kmail/Konsole running all the time. The only time I need to restart them is when they crash (which means very rarely. ;))\n"
    author: "Johnny Andersson"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "Njaard, how about 'KDE2 runs fine on an AMD K& 233' and without 'Are you insane?'\n\nAsif doesn't deserve being called 'insane', just because he stated that his KDE is slow. \n\nSome people experience that KDE is slow just because they use too many animations. For example, if people have slow animation on shade/restore checked they will have feeling that overall performance is lousy.\nMany people save Konqueror profiles with urls and they complain it takes 20 seconds to start Konqueror.\n\nKDE is extremely fast even without objprelink compilation, but everyone should know how to tweak it. Maybe we need a web-site (a new one in our kde.org family) with good advices about tweaking and optimizing KDE. If we had that you wouldn't need to read again and again posting where people complain about KDE being slow.\n\nHere:\nLogin to KDE 2.2.1 = 6 seconds, log out of KDE 2.2.1 = 3 seconds, starting Konqueror <1 sec."
    author: "antialias"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-02-01
    body: ">>Maybe we need a web-site (a new one in our kde.org family) with good advices about tweaking and optimizing KDE. If we had that you wouldn't need to read again and again posting where people complain about KDE being slow.<<\n\nThe problem is that those people who are complaining don't seem to be those who do something against it... "
    author: "Anynomous one"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-30
    body: "This discussion ***again***..... :-("
    author: "Johnny Andersson"
  - subject: "Re: Hardware: Pentium II 300MHz, 192MB RAM."
    date: 2002-01-31
    body: "Asif, was that you who came by the booth today with the list of demands for KDE 3? \n\nI can't believe I missed you -- it would have been like enacting the Dot in real life! \n\n"
    author: "KDE Booth at LWE"
  - subject: "IRC Nick"
    date: 2002-01-30
    body: "He should change his nick to C++-Howells ;-)"
    author: "Steven"
  - subject: "Re: IRC Nick"
    date: 2002-01-30
    body: "I'm also learning C++ at the moment. The KDE source is very nice to learn new things. I know Java and have a basic knowledge of OO design.\n\nA week ago, I was browsing tru webcvs and I found Kalypso in kdenonbeta that is supposed to install KDE from scratch (using Qt). Chris was making it. It still very basic and doesn't do much. So, I gave him some advice.\n\nA young person who can bring a lot of new ideas into KDE, that can't be bad :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "More People of KDE, please"
    date: 2002-01-30
    body: "(I was eating Marabou Chocolate when I read the interview! :) Coincidence?)\n\nIs it possible to extend the interviews to two per week? It's really interesting to see who the people behind the cvs commits are!"
    author: "Loranga"
  - subject: "Re: More People of KDE, please"
    date: 2002-01-30
    body: "Yes, in one way that would be good.\n\nOTOH, it's nice seing that Tink pics different people, not just those who \"deserve it the most\". I think it is really important.\n\nregards,\nFrans"
    author: "Frans"
  - subject: "Re: More People of KDE, please"
    date: 2002-01-30
    body: "Of course, it doesn't have to be people behind the cvs commits, KDE people in general is also interesting. "
    author: "Loranga"
  - subject: "Re: More People of KDE, please"
    date: 2002-02-02
    body: ">>I was eating Marabou Chocolate when I read the interview! <<\n>> Is it possible to extend the interviews to two per week.<<\n\nWell maybe if you would have send me the Marabou Chocolate ;-)\n\nNo I don't think so, now it's fun, 2 times a week probably gets boring.\nBesides that, it's not easy to get people to submit their answers and send a photo.  \nSome people even never send a photo and don't get published at all and some people just never send their answers back.\nIt often takes weeks of harassing and flaming and stress ;-))\n\n--\nTink"
    author: "Tink"
  - subject: "Strange Question II"
    date: 2002-01-30
    body: "~ If you are a smoker, does it ever happen to you that your cigarette sets your ashtray on fire? How often?\n\n\nThis no good question as well!"
    author: " "
  - subject: "Re: Strange Question II"
    date: 2002-01-30
    body: "I think Tink is just expecting an answer along the lines of \"Well, I haven't ever set my ashtray on fire, but one time I turned my CPU into a little puddle of melted silicon when I....\". The interviees can really be more original then they are now, can't they?\n\nAlso, here's an idea: since the questions are the same for everybody, what's to prevent Tink from just making the Q&A accessible with a web interface to KDE developers? That way, any developer that wanted to be \"interviewed\" could do so at their leisure."
    author: "Carbon"
  - subject: "Re: Strange Question II"
    date: 2002-01-30
    body: "No good.. You'd have to pretty damn self-promoting to do a thing like that. :)\n"
    author: "Johnny Andersson"
  - subject: "Re: Strange Question II"
    date: 2002-01-30
    body: "> ~ If you are a smoker, does it ever happen to you that your cigarette sets \n> your ashtray on fire? How often?\n\n> This no good question as well!\n\nHey, my favourite question, you wouldn't believe how often this happens to me,\ncoding and ashtray-depleting (sp?) are mutualy exclusive activities!\n:-)\nregards,\ntom"
    author: "tomte"
  - subject: "Re: Strange Question II"
    date: 2002-01-31
    body: "getting used to smoking while you code or sit in front of the computer is the best way to start smoking 3pkgs per day.\n"
    author: "tester"
  - subject: "Thanks to Chris"
    date: 2002-01-30
    body: "Great interview! I am glad Chris is featured in People of KDE because he does a very good job! Chris works on various KDE websites: webcvs is very useful and he set up edu.kde.org in a very short notice.\nHe is always willing to help and KDEEdu would not have had such a good start without him.\nIn the name of the kdeedu team: thanks, Chris!"
    author: "annma"
  - subject: "Try the new Linux O(1) scheduler and -aa2 VM..."
    date: 2002-01-31
    body: "..or maybe the -rmap VM and you shoud \"see\" a difference without recompilation of the KDE source. But it can't harm anyway...;-)\n\nRegards,\n Dieter"
    author: "Dieter N\u00fctzel"
---
From the old british kingdom, <a href="mailto:tink@kde.org">Tink</a> brings to you this week the feelings and the thoughts of <a href="http://www.kde.org/people/chrish.html">Chris Howells</a>. One of the youngest members of our community, Chris is an excellent proof of the kind of diversity and richness that KDE's people bring to the project. In the distant time of glory and legend when KDE got started, Chris was too young to even care. Yet he grew to appreciate what we do and he decided to get involved in web design and, recently, to learn about writing code for KDE. A typical sample of the <a href="http://www.kde.org/people/people.html">"People behind KDE"</a> series.
<!--break-->
