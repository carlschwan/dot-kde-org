---
title: "KDE 3.0RC3: Prepare to Fall in Love"
date:    2002-03-21
authors:
  - "Dre"
slug:    kde-30rc3-prepare-fall-love
comments:
  - subject: "RH packages"
    date: 2002-03-21
    body: "Anyone knows when Redhat packages will be ready ?"
    author: "Anonymous"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "On many mirrors, the i386 dir is empty...\n\nonly here http://gd.tuwien.ac.at/pub/kde/unstable/kde-3.0rc3/RedHat/i386/\nI see 47 packages... but, it's a complete distro???"
    author: "huntz"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "> I see 47 packages... but, it's a complete distro???\n\nNo, it's a part of complete KDE release packaged the new RedHat way into several small packages."
    author: "Anonymous"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "That's just great... My computer isn't powerful enough to compile from source (read: don't want to spend a whole day to compile it), so I use binaries most the time. 2 days ago I switched to Mandrake because it has better KDE support (and KDE 2.2.2 feels much faster now :-). I always wanted to test the new 3.0 (Beta/RC) but RH never released binaries for it. And now suddenly RH does, and Mandrake doesn't :-(\n\nI guess I have to wait for Mandrake to release 3.0 final... or RC3 if they want to package it. No, I won't test an old Beta..."
    author: "Andy Goossens"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "... or you could wait and switch (again ;-) to SuSE 8.0. It is annouced to be released with KDE 3.0 final."
    author: "sikasso"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "While I'd like that also, the lifespan of this one isn't very long.  They're looking for last minute bug testing from the main source branch, so getting reports on a specific RPM version might not be as helpful for now.  \n\nI'd prefer a 2 week test cycle myself, including binaries, but I'm not a KDE developer so my opinion matters naught."
    author: "Anonymous"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "Most likely a month or two after final. They are usually a little behind. When you consider the qa that must go into building a package that will work 99% of the time, this is a fairly reasonable amount of time.\n\nthx,\nNeil"
    author: "Neil Davis"
  - subject: "Re: RH packages"
    date: 2002-03-21
    body: "BTW, I get:\n\n[root@dimi KDE3rc3]# rpm -ivh *.rpm\nerror: failed dependencies:\n        libgphoto2.so.2   is needed by kamera-3.0-0.rc3.1\n        libgphoto2_port.so.0   is needed by kamera-3.0-0.rc3.1\n        libusb-0.1.so.4   is needed by kamera-3.0-0.rc3.1\n        libxml2 >= 2.4.12 is needed by kdebase-3.0-0.rc3.1\n        cups-libs >= 1.1.12 is needed by kdelibs-3.0-0.rc3.1\n        libusb-0.1.so.4   is needed by kooka-3.0-0.rc3.1\n        libusb-0.1.so.4   is needed by libkscan-3.0-0.rc3.1\n\nThe important stuff here is\n        libxml2 >= 2.4.12 is needed by kdebase-3.0-0.rc3.1\n        cups-libs >= 1.1.12 is needed by kdelibs-3.0-0.rc3.1\n\nWhere can I get them from? :)\n\n\n\n\n"
    author: "themetaguy"
  - subject: "Re: RH packages"
    date: 2002-03-22
    body: "Check www.rpmfind.net"
    author: "bd"
  - subject: "Re: RH packages"
    date: 2002-03-22
    body: "But I did. I searched\n\nhttp://rpmfind.net\n\nas well as Bero's\n\nhttp://www.linux-easy.com\n\nNada. At least for RedHat 7.2\n"
    author: "themetaguy"
  - subject: "Re: RH packages"
    date: 2002-03-22
    body: "The problem is that the only packeger on redhat that cares a lot about kde is bero, and he's always using a distro with all updates from rawhide.\nThis means RedHat rpms are not for kde 7.2, but for rawhide.\nThis is why I'm going to chance for mdk 8.2 soon :)"
    author: "protoman"
  - subject: "Re: RH packages"
    date: 2002-03-22
    body: "Can anyone tell me the correct rpm commands to install KDE3 on RH 7.2. I get a \nlot of conflicts and dependancy problems.\n\n"
    author: "BOBS"
  - subject: "Re: RH packages"
    date: 2002-03-22
    body: "But what conflicts and dependancy you get? "
    author: "Tyro"
  - subject: "Re: RH packages"
    date: 2002-03-23
    body: "Below are the outputs from my two attempts of installing KDE3 on a freshly installed RH 7.2 machine. using rpm -U *.rpm and rpm -U --nodeps *.rpm.\n\nOne combination of rpm options (which I've unfortunately forgotten) did appear to install KDE. After reboot I had a new login screen, with icons for each user. Unfortunatly KDE was not an available session type. This led me to reload RH and try again.\n\nAnty advice on avoiding these problems is welcome.\n\nCheers\n\nBob\n\nThe intial attmpt gave me this\n\n[root@snooker kde3]# rpm -U *.rpm\nerror: failed dependencies:\n        libcups.so.2   is needed by kaboodle-3.0-0.rc3.1\n        libcups.so.2   is needed by kamera-3.0-0.rc3.1\n        libgphoto2.so.2   is needed by kamera-3.0-0.rc3.1\n        libgphoto2_port.so.0   is needed by kamera-3.0-0.rc3.1\n        libusb-0.1.so.4   is needed by kamera-3.0-0.rc3.1\n        libcups.so.2   is needed by kcoloredit-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeaddons-kate-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeaddons-kicker-3.0-0.rc3.1\n        kdenetwork-libs >= 3.0 is needed by kdeaddons-knewsticker-3.0-0.rc3.1\n        knewsticker >= 3.0 is needed by kdeaddons-knewsticker-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeaddons-konqueror-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeaddons-noatun-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeadmin-3.0-0.rc3.1\n        librpm-4.0.4.so   is needed by kdeadmin-3.0-0.rc3.1\n        librpmbuild-4.0.4.so   is needed by kdeadmin-3.0-0.rc3.1\n        librpmdb-4.0.4.so   is needed by kdeadmin-3.0-0.rc3.1\n        librpmio-4.0.4.so   is needed by kdeadmin-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeartwork-3.0-0.rc3.1\n        libcups.so.2   is needed by kdeartwork-screensavers-3.0-0.rc3.1\n        libxml2 >= 2.4.12 is needed by kdebase-3.0-0.rc3.1\n        libcups.so.2   is needed by kdebase-3.0-0.rc3.1\n        libqt.so.3   is needed by kdebase-3.0-0.rc3.1\n        qt >= 3.0.3 is needed by kdelibs-3.0-0.rc3.1\n        cups-libs >= 1.1.12 is needed by kdelibs-3.0-0.rc3.1\n        libcups.so.2   is needed by kdelibs-3.0-0.rc3.1\n        qt-devel >= 3.0.3 is needed by kdelibs-devel-3.0-0.rc3.1\n        libcups.so.2   is needed by kdelibs-devel-3.0-0.rc3.1\n        libcups.so.2   is needed by kdemultimedia-arts-3.0-0.rc3.1\n        libcups.so.2   is needed by kdemultimedia-kfile-3.0-0.rc3.1\n        libcups.so.2   is needed by kdetoys-3.0-0.rc3.1\n        libcups.so.2   is needed by kdvi-3.0-0.rc3.1\n        libcups.so.2   is needed by kfax-3.0-0.rc3.1\n        libcups.so.2   is needed by kfile-pdf-3.0-0.rc3.1\n        libcups.so.2   is needed by kfile-png-3.0-0.rc3.1\n        libcups.so.2   is needed by kfract-3.0-0.rc3.1\n        libcups.so.2   is needed by kghostview-3.0-0.rc3.1\n        libcups.so.2   is needed by kiconedit-3.0-0.rc3.1\n        libcups.so.2   is needed by kmid-3.0-0.rc3.1\n        libcups.so.2   is needed by kmidi-3.0-0.rc3.1\n        libcups.so.2   is needed by kmix-3.0-0.rc3.1\n        libcups.so.2   is needed by koncd-3.0-0.rc3.1\n        libcups.so.2   is needed by kooka-3.0-0.rc3.1\n        libusb-0.1.so.4   is needed by kooka-3.0-0.rc3.1\n        libcups.so.2   is needed by kpaint-3.0-0.rc3.1\n        libcups.so.2   is needed by kruler-3.0-0.rc3.1\n        libcups.so.2   is needed by kscd-3.0-0.rc3.1\n        libcups.so.2   is needed by ksnapshot-3.0-0.rc3.1\n        libcups.so.2   is needed by kuickshow-3.0-0.rc3.1\n        libcups.so.2   is needed by kview-3.0-0.rc3.1\n        libcups.so.2   is needed by kviewshell-3.0-0.rc3.1\n        libcups.so.2   is needed by libkscan-3.0-0.rc3.1\n        libusb-0.1.so.4   is needed by libkscan-3.0-0.rc3.1\n        libcups.so.2   is needed by noatun-3.0-0.rc3.1\n        libartsflow_idl.so.0   is needed by kdegames-2.2-2\n        libartsflow.so.0   is needed by kdegames-2.2-2\n        libkmedia2_idl.so.0   is needed by kdegames-2.2-2\n        libmcop.so.0   is needed by kdegames-2.2-2\n        libsoundserver_idl.so.0   is needed by kdegames-2.2-2\n        libDCOP.so.1   is needed by kdepim-2.2-1\n        libDCOP.so.1   is needed by koffice-1.1-5\n        libDCOP.so.1   is needed by kdeutils-2.2-2\n        libDCOP.so.1   is needed by kdenetwork-2.2-7\n        libDCOP.so.1   is needed by kdenetwork-ppp-2.2-7\n        libDCOP.so.1   is needed by kpppload-1.04-29\n        libDCOP.so.1   is needed by kdebindings-2.2-2\n        libDCOP.so.1   is needed by kdebindings-kmozilla-2.2-2\n        libDCOP.so.1   is needed by licq-kde-1.0.3-7\n        libDCOP.so.1   is needed by kdegraphics-2.2-1\n        libDCOP.so.1   is needed by kdbg-1.2.1-5\n        libDCOP.so.1   is needed by kdesdk-2.2-2\n        libDCOP.so.1   is needed by kdevelop-2.0-1\n        libDCOP.so.1   is needed by kdegames-2.2-2\n        libDCOP.so.1   is needed by cervisia-1.4.1-2\n        libDCOP.so.1   is needed by kdebindings-perl-2.2-2\n        libDCOP.so.1   is needed by kdepim-cellphone-2.2-1\n        libDCOP.so.1   is needed by kdepim-pilot-2.2-1\n        libDCOP.so.1   is needed by kdebindings-python-2.2-2\n        libDCOP.so.1   is needed by quanta-2.0-0.cvs20010724.2\n        libkab.so.3   is needed by kdepim-2.2-1\n        libkab.so.3   is needed by kdeutils-2.2-2\n        libkab.so.3   is needed by kdenetwork-2.2-7\n        libkab.so.3   is needed by kdepim-cellphone-2.2-1\n        libkdecore.so.3   is needed by kdepim-2.2-1\n        libkdecore.so.3   is needed by koffice-1.1-5\n        libkdecore.so.3   is needed by kdeutils-2.2-2\n        libkdecore.so.3   is needed by kdenetwork-2.2-7\n        libkdecore.so.3   is needed by kdenetwork-ppp-2.2-7\n        libkdecore.so.3   is needed by kpppload-1.04-29\n        libkdecore.so.3   is needed by kdebindings-2.2-2\n        libkdecore.so.3   is needed by kdebindings-kmozilla-2.2-2\n        libkdecore.so.3   is needed by licq-kde-1.0.3-7\n        libkdecore.so.3   is needed by kdegraphics-2.2-1\n        libkdecore.so.3   is needed by kdbg-1.2.1-5\n        libkdecore.so.3   is needed by kdesdk-2.2-2\n        libkdecore.so.3   is needed by kdevelop-2.0-1\n        libkdecore.so.3   is needed by kdegames-2.2-2\n        libkdecore.so.3   is needed by cervisia-1.4.1-2\n        libkdecore.so.3   is needed by kdebindings-perl-2.2-2\n        libkdecore.so.3   is needed by kdepim-cellphone-2.2-1\n        libkdecore.so.3   is needed by kdepim-pilot-2.2-1\n        libkdecore.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libkdefakes.so.3   is needed by cervisia-1.4.1-2\n        libkdeprint.so.0   is needed by kdepim-2.2-1\n        libkdeprint.so.0   is needed by koffice-1.1-5\n        libkdeprint.so.0   is needed by kdeutils-2.2-2\n        libkdeprint.so.0   is needed by kdenetwork-2.2-7\n        libkdeprint.so.0   is needed by kdegraphics-2.2-1\n        libkdeprint.so.0   is needed by kdevelop-2.0-1\n        libkdeprint.so.0   is needed by kdegames-2.2-2\n        libkdeprint.so.0   is needed by quanta-2.0-0.cvs20010724.2\n        libkdesu.so.1   is needed by kdepim-2.2-1\n        libkdesu.so.1   is needed by koffice-1.1-5\n        libkdesu.so.1   is needed by kdeutils-2.2-2\n        libkdesu.so.1   is needed by kdenetwork-2.2-7\n        libkdesu.so.1   is needed by kdebindings-2.2-2\n        libkdesu.so.1   is needed by kdebindings-kmozilla-2.2-2\n        libkdesu.so.1   is needed by licq-kde-1.0.3-7\n        libkdesu.so.1   is needed by kdegraphics-2.2-1\n        libkdesu.so.1   is needed by kdbg-1.2.1-5\n        libkdesu.so.1   is needed by kdesdk-2.2-2\n        libkdesu.so.1   is needed by kdevelop-2.0-1\n        libkdesu.so.1   is needed by kdegames-2.2-2\n        libkdesu.so.1   is needed by cervisia-1.4.1-2\n        libkdesu.so.1   is needed by kdepim-cellphone-2.2-1\n        libkdesu.so.1   is needed by kdepim-pilot-2.2-1\n        libkdesu.so.1   is needed by quanta-2.0-0.cvs20010724.2\n        libkdeui.so.3   is needed by kdepim-2.2-1\n        libkdeui.so.3   is needed by koffice-1.1-5\n        libkdeui.so.3   is needed by kdeutils-2.2-2\n        libkdeui.so.3   is needed by kdenetwork-2.2-7\n        libkdeui.so.3   is needed by kdenetwork-ppp-2.2-7\n        libkdeui.so.3   is needed by kpppload-1.04-29\n        libkdeui.so.3   is needed by kdebindings-2.2-2\n        libkdeui.so.3   is needed by kdebindings-kmozilla-2.2-2\n        libkdeui.so.3   is needed by licq-kde-1.0.3-7\n        libkdeui.so.3   is needed by kdegraphics-2.2-1\n        libkdeui.so.3   is needed by kdbg-1.2.1-5\n        libkdeui.so.3   is needed by kdesdk-2.2-2\n        libkdeui.so.3   is needed by kdevelop-2.0-1\n        libkdeui.so.3   is needed by kdegames-2.2-2\n        libkdeui.so.3   is needed by cervisia-1.4.1-2\n        libkdeui.so.3   is needed by kdepim-cellphone-2.2-1\n        libkdeui.so.3   is needed by kdepim-pilot-2.2-1\n        libkdeui.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libkfile.so.3   is needed by kdepim-2.2-1\n        libkfile.so.3   is needed by koffice-1.1-5\n        libkfile.so.3   is needed by kdeutils-2.2-2\n        libkfile.so.3   is needed by kdenetwork-2.2-7\n        libkfile.so.3   is needed by kdebindings-2.2-2\n        libkfile.so.3   is needed by kdebindings-kmozilla-2.2-2\n        libkfile.so.3   is needed by licq-kde-1.0.3-7\n        libkfile.so.3   is needed by kdegraphics-2.2-1\n        libkfile.so.3   is needed by kdbg-1.2.1-5\n        libkfile.so.3   is needed by kdesdk-2.2-2\n        libkfile.so.3   is needed by kdevelop-2.0-1\n        libkfile.so.3   is needed by kdegames-2.2-2\n        libkfile.so.3   is needed by cervisia-1.4.1-2\n        libkfile.so.3   is needed by kdepim-cellphone-2.2-1\n        libkfile.so.3   is needed by kdepim-pilot-2.2-1\n        libkfile.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libkhtml.so.3   is needed by kdenetwork-2.2-7\n        libkhtml.so.3   is needed by kdevelop-2.0-1\n        libkhtml.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libkio.so.3   is needed by kdepim-2.2-1\n        libkio.so.3   is needed by koffice-1.1-5\n        libkio.so.3   is needed by kdeutils-2.2-2\n        libkio.so.3   is needed by kdenetwork-2.2-7\n        libkio.so.3   is needed by kdebindings-2.2-2\n        libkio.so.3   is needed by kdebindings-kmozilla-2.2-2\n        libkio.so.3   is needed by licq-kde-1.0.3-7\n        libkio.so.3   is needed by kdegraphics-2.2-1\n        libkio.so.3   is needed by kdbg-1.2.1-5\n        libkio.so.3   is needed by kdesdk-2.2-2\n        libkio.so.3   is needed by kdevelop-2.0-1\n        libkio.so.3   is needed by kdegames-2.2-2\n        libkio.so.3   is needed by cervisia-1.4.1-2\n        libkio.so.3   is needed by kdepim-cellphone-2.2-1\n        libkio.so.3   is needed by kdepim-pilot-2.2-1\n        libkio.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libkparts.so.1   is needed by kdepim-2.2-1\n        libkparts.so.1   is needed by koffice-1.1-5\n        libkparts.so.1   is needed by kdeutils-2.2-2\n        libkparts.so.1   is needed by kdenetwork-2.2-7\n        libkparts.so.1   is needed by kdebindings-2.2-2\n        libkparts.so.1   is needed by kdebindings-kmozilla-2.2-2\n        libkparts.so.1   is needed by kdegraphics-2.2-1\n        libkparts.so.1   is needed by kdevelop-2.0-1\n        libkparts.so.1   is needed by kdegames-2.2-2\n        libkparts.so.1   is needed by quanta-2.0-0.cvs20010724.2\n        libkspell.so.3   is needed by koffice-1.1-5\n        libkspell.so.3   is needed by kdeutils-2.2-2\n        libkspell.so.3   is needed by kdenetwork-2.2-7\n        libkspell.so.3   is needed by kdesdk-2.2-2\n        libkspell.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libkssl.so.2   is needed by kdepim-2.2-1\n        libkssl.so.2   is needed by koffice-1.1-5\n        libkssl.so.2   is needed by kdeutils-2.2-2\n        libkssl.so.2   is needed by kdenetwork-2.2-7\n        libkssl.so.2   is needed by kdebindings-2.2-2\n        libkssl.so.2   is needed by kdebindings-kmozilla-2.2-2\n        libkssl.so.2   is needed by licq-kde-1.0.3-7\n        libkssl.so.2   is needed by kdegraphics-2.2-1\n        libkssl.so.2   is needed by kdbg-1.2.1-5\n        libkssl.so.2   is needed by kdesdk-2.2-2\n        libkssl.so.2   is needed by kdevelop-2.0-1\n        libkssl.so.2   is needed by kdegames-2.2-2\n        libkssl.so.2   is needed by kdepim-cellphone-2.2-1\n        libkssl.so.2   is needed by kdepim-pilot-2.2-1\n        libkssl.so.2   is needed by quanta-2.0-0.cvs20010724.2\n        libksycoca.so.3   is needed by kdepim-2.2-1\n        libksycoca.so.3   is needed by koffice-1.1-5\n        libksycoca.so.3   is needed by kdeutils-2.2-2\n        libksycoca.so.3   is needed by kdenetwork-2.2-7\n        libksycoca.so.3   is needed by kdebindings-2.2-2\n        libksycoca.so.3   is needed by kdebindings-kmozilla-2.2-2\n        libksycoca.so.3   is needed by licq-kde-1.0.3-7\n        libksycoca.so.3   is needed by kdegraphics-2.2-1\n        libksycoca.so.3   is needed by kdbg-1.2.1-5\n        libksycoca.so.3   is needed by kdesdk-2.2-2\n        libksycoca.so.3   is needed by kdevelop-2.0-1\n        libksycoca.so.3   is needed by kdegames-2.2-2\n        libksycoca.so.3   is needed by cervisia-1.4.1-2\n        libksycoca.so.3   is needed by kdepim-cellphone-2.2-1\n        libksycoca.so.3   is needed by kdepim-pilot-2.2-1\n        libksycoca.so.3   is needed by quanta-2.0-0.cvs20010724.2\n        libqtmcop.so.0   is needed by kdegames-2.2-2\n[root@snooker kde3]#\n\nThe 2nd attepmpt gave this *****************************\n\n[root@snooker kde3]# rpm -U --nodeps *.rpm\nfile /usr/lib/libvcard.la from install of kdelibs-3.0-0.rc3.1 conflicts with file from package kdepim-2.2-1\nfile /usr/lib/libvcard.so.0.0.0 from install of kdelibs-3.0-0.rc3.1 conflicts with file from package kdepim-2.2-1\nfile /usr/share/mimelnk/application/x-abiword.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kchart.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kformula.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kivio.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kontour.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kpresenter.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kspread.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kugar.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-kword.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/application/x-quattropro.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/image/x-msod.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/image/x-wmf.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/image/x-xfig.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/share/mimelnk/text/x-csv.desktop from install of kdelibs-3.0-0.rc3.1 conflicts with file from package koffice-1.1-5\nfile /usr/bin/kcolorchooser from install of kcoloredit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kcoloredit from install of kcoloredit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/kcolorchooser.la from install of kcoloredit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/kcolorchooser.so from install of kcoloredit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kcolorchooser.desktop from install of kcoloredit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kcoloredit.desktop from install of kcoloredit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kfind from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/bin/kpm from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/share/applnk/Kfind.desktop from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/share/doc/HTML/en/kfind/index.cache.bz2 from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/share/doc/HTML/en/kfind/index.docbook from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/share/icons/hicolor/32x32/apps/kfind.png from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/share/icons/hicolor/48x48/apps/kfind.png from install of kdebase-3.0-0.rc3.1 conflicts with file from package kdeutils-2.2-2\nfile /usr/bin/kfract from install of kfract-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kfract.desktop from install of kfract-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kfract/index.cache.bz2 from install of kfract-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kfract/index.docbook from install of kfract-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kghostview from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkghostview.la from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkghostview.so from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kghostview.desktop from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/apps/kghostview/kghostviewui.rc from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/apps/kghostview/kgv_part.rc from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kghostview/index.cache.bz2 from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kghostview/index.docbook from install of kghostview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kiconedit from install of kiconedit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kiconedit.desktop from install of kiconedit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kiconedit/index.cache.bz2 from install of kiconedit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kiconedit/index.docbook from install of kiconedit-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kpaint from install of kpaint-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kpaint.desktop from install of kpaint-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kpaint/index.cache.bz2 from install of kpaint-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kpaint/index.docbook from install of kpaint-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kruler from install of kruler-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kruler.desktop from install of kruler-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/apps/kruler/eventsrc from install of kruler-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kruler/index.cache.bz2 from install of kruler-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kruler/index.docbook from install of kruler-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/ksnapshot from install of ksnapshot-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/ksnapshot.desktop from install of ksnapshot-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/ksnapshot/index.cache.bz2 from install of ksnapshot-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/ksnapshot/index.docbook from install of ksnapshot-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kview from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/kview.la from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/kview.so from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkviewpart.la from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkviewpart.so from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kview.desktop from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/apps/kview/kviewui.rc from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kview/index.cache.bz2 from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kview/index.docbook from install of kview-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kviewshell from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkmultipage.la from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkmultipage.so.0.0.0 from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkpagetest.la from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkpagetest.so from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkviewerpart.la from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkviewerpart.so from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkviewpart.la from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkviewpart.so from install of kviewshell-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kdvi from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkdvi.la from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkdvi.so from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kdvi.desktop from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/apps/kdvi/kdvi_part.rc from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kdvi/KDVI-features.dvi from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kdvi/KDVI-features.tex from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kdvi/index.cache.bz2 from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kdvi/index.docbook from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kdvi/optionrequester1.png from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/doc/HTML/en/kdvi/optionrequester2.png from install of kdvi-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kfax from install of kfax-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkfax.la from install of kfax-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkfax.so from install of kfax-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/applnk/Graphics/kfax.desktop from install of kfax-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkscan.la from install of libkscan-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/lib/libkscan.so.1.0.0 from install of libkscan-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/bin/kooka from install of kooka-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/share/apps/kooka/kookaui.rc from install of kooka-3.0-0.rc3.1 conflicts with file from package kdegraphics-2.2-1\nfile /usr/include/kde/devselector.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/dispgamma.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/gammadialog.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/img_canvas.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/kgammatable.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/kscandevice.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/kscanoption.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/kscanoptset.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/kscanslider.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/massscandialog.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/previewer.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1\nfile /usr/include/kde/scanparams.h from install of libkscan-devel-3.0-0.rc3.1 conflicts with file from package kdegraphics-devel-2.2-1"
    author: "BOBS"
  - subject: "Re: RH packages"
    date: 2002-03-23
    body: "check rpmfind.net\n \n\nAs example \nfor libcups.so.2 you need install this package\n  be http://rpmfind.net//linux/RPM/rhcontrib/7.1/i386/cups-1.1.10-3.i386.html\nfor  libgphoto2.so.2  - ftp.gmd.de/mirrors/redhat.com/redhat/linux/rawhide/i386/RedHat/RPMS/gphoto2-2.0-2.i386.rpm\n(In this case first I go to the rpmfind.net and chaeck libgphoto2.so.2 thin i see\n3 package   libgphoto2-2.0-2mdk.ppc.rpm \n            libgphoto2-2.0-2mdk.i586.rpm\n            gphoto2-2.0-2.alpha.rpm   \nbut we need package for RH, so i go to ftpfind.com and check gphoto2-2.0  and \n find this  ftp.join.uni-muenster.de/pub/mirrors/ftp.redhat.com/redhat/linux/rawhide/i386/RedHat/RPMS/gphoto2-2.0-2.i386.rpm)\nand  e.t.c.\n\nSo and of cause you need to install qt 3.0.3\nAnd never use --nodeps it's danger!!!\nIf you have more question u can mail me \nGood luck!\n \n\n              \n\n\n\n\n\n\n \n"
    author: "Tyro"
  - subject: "Re: RH packages"
    date: 2002-03-25
    body: "Could you please post an URL where one can find the qt-3.x package for Red Hat 7.2 on an i386?   I've been looking around RPMfind.net and I can find it for almost any distro (including Alpha64) but not for that one.\n\nThanks in advance!"
    author: "Ricardo J. M\u00e9ndez"
  - subject: "Re: RH packages"
    date: 2002-03-26
    body: "http://www.linux-easy.com/daily/RPMS/qt-3.0.3-0.cvs20020319.1.i386.rpm\n\nhttp://www.linux-easy.com/daily/\n"
    author: "Tyro"
  - subject: "Re: RH packages"
    date: 2002-03-26
    body: "Thanks!"
    author: "Ricardo J. M\u00e9ndez"
  - subject: "Help...."
    date: 2002-03-30
    body: "Install do not work... the dep packages is very big... I'm have amost of problems here... why?\n\nthe package send xxxx depend xxxx2 to work... you get xxxx2...\nthe package send xxxx2 depend xxxx to work??????????????? :P\n\nWhat command line is correct... \"rpm -ivh *.rpm\" or \"rpm -Uvh *.rpm\" or another...?\n\nThere is not have finish... is a forever loop... Please help me!!!!!! :(\n\nBest regards\n\nJean"
    author: "Jean Pierre"
  - subject: "Re: two (semi-public) release candidates"
    date: 2002-03-21
    body: "Nice to learn that a new RC is out.... BUT WHY DOESN'T KDE MENTIONS THE RC ON THE WEB SITE?\n\nI mean... the last announcement was for the Beta2.\nI've seen no RC announcement so far (even on the archives on the mailing-lists I've just browsed).\n\nWhat's the purpose of a RC? Having the programs to be tested by the biggest number of people, or keep them secret?\n\nI don't get it, can someone explain me. I'm a moderator on a news site, and we have passed every single news on Mandrake 8.2 betas and RC. We would have done the same for KDE if we had heard of these RC (with some official announcements, like changes...)"
    author: "oliv"
  - subject: "Re: two (semi-public) release candidates"
    date: 2002-03-21
    body: "Well, actually they were announced. But because they were semi-public you won't find them on the news-page. Just look on the developerpages or the release-schedule. Everything is there. And the best: appointed times are almost always abided... ;-)\n\nCU\n\tMarcel"
    author: "Marcel"
  - subject: "Re: two (semi-public) release candidates"
    date: 2002-03-24
    body: "Well, at least translators were notified about\ntagging RC-releases, these were well-known there.\nAnyways, I don't think there were binary packages,\nso that's the reason they were semi-public."
    author: "Juraj Bednar"
  - subject: "Way to go KDE!"
    date: 2002-03-21
    body: "Of course, I wont be running KDE3 in a while. Knowing Debian, it will take a while before they make packages available. But what I have learned of KDE 3 sounds REALLY promising! New features, new eye-candy, more speed... What more could you ask for?\n\nI hope Konqueror finally has the one feature I have been waiting for. When right-clicking on a thumbnail (for example), and choosing \"new window\", it should open it in new Konqueror-windows (well duh!) and not ask what app to use to open it. If I choose \"new window\" in Konqueror, naturally I mean new Konqueror-window."
    author: "Janne"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-21
    body: "Compile it from source. Debian's kde packages introduced new bugs, and were slow to be released (esp if you use stable). It may take a few hours to compile, but you get KDE now, and you get it optimised for your system, and it is easy to do."
    author: "eze"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-22
    body: "But then you've got to recompile with every release and in KDE world, that's pretty often! The beauty of Debian is the easy maintenance. Upgrading is sooo simple; one would hate to interfere with that!"
    author: "justMe"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-23
    body: "So install it into a non-Debian path (like /opt/kde) and keep the recent qt-copy out of /usr/lib and you're set. When Debian releases the packages, just install the packages and rm -rf /opt/kde. This way you get all the betas and release candidates you can shake a stick at and still keep up with the Debian way when they catch up."
    author: "cosmo"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-21
    body: "That right-click thing should already work in 2.2.\nMaybe it's the application/bmp vs application/x-bmp misnaming?\nI noticed it and fixed it in CVS recently.\nOr does it do that for all kind of images ?\n\n(Also, make sure that the setting in \"File Associations\" is still \"Use embedded viewer\" for images).\n"
    author: "David Faure"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-22
    body: "Thanks, I check out those settings today :). It has been with just about all kinds of images (gif, jpg, png etc.). That has been just about the only thing that has annoyed me in Konqueror (well, there are some javascript-problems with some sites, but I expect those to be fixed in 3.0)."
    author: "Janne"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-22
    body: "I made sure that the images in question use the embedded viewer but no help (that setting was for left-click action, I haven't found setting for right-click action). I also made sure that the imagetypes in question are opened in Konqueror, but it doesn't help. It seems like it tries to open new Konqueror-window, but it never does :(. Oh well, that's a minor issue and I can live with it."
    author: "Janne"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-22
    body: "If you want to open an image in a new Konqueror window, not in a external viewer, then don't choose \"new window\" in the popup menu, but use the middel mouse button in stead. Works fine for me ;)\n\nRinse"
    author: "rinse"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-23
    body: "Thanks for the tip!\n"
    author: "cgen"
  - subject: "Re: Way to go KDE!"
    date: 2002-03-23
    body: "Thanks, it works :). Also, since I wasn't able to get the RMB to work, I decided to revert my settings to their default values. After I did, the RMB suddenly started to work! Talk about strange!"
    author: "Janne"
  - subject: "Re: Way to go KDE!"
    date: 2002-04-01
    body: "If you want to run KDE - use alien to convert the Red Hat RPM file to Deb files ;-)\n\nThanks,\nAlex "
    author: "Alex Howells"
  - subject: "Speed & Conq questions"
    date: 2002-03-21
    body: "I have two quick questions about kde3,rc since I can't install it at the moment.\n\n1.  It was mentioned that Konqueror and the rest of KDE3 are \"fast, fast, fast\".  What is the reason for such a speed increase?  Is it obj-prelinked?  If not, can additional gains be made by obj-prelinking?\n\n2.  Now to my personal pet-pieve bug in Konq.  I am wondering if the \"style=display:...\" is fixed in Konq 3.0.  It was almost fixed in 2.2.x where it worked 50% of the time.  For instance, does the sidebar in www.vbrad.com work?  It makes heavy use of \"style=display:...\" to show and hide collapsible lists.  Can anyone confirm whether it is fixed or not?\n"
    author: "Frank Rizzo"
  - subject: "Re: Speed & Conq questions"
    date: 2002-03-21
    body: ">I am wondering if the \"style=display:...\" is fixed in Konq 3.0. It was almost fixed in \n>2.2.x where it worked 50% of the time. For instance, does the sidebar in www.vbrad.com \n>work? It makes heavy use of \"style=display:...\" to show and hide collapsible lists. Can \n>anyone confirm whether it is fixed or not?\n\nYes, it appears to work. "
    author: "Brent Cook"
  - subject: "Re: Speed & Conq questions"
    date: 2002-03-21
    body: "I tried out the site you mentioned and it seems to work with my CVS build from 08/03/02. Not supprising since Javascript/DOM has been much improved. Everything is much more responsive in KDE3, even without compiling with --enable-Objprelink (it has, in previous CVS builds, caused segmentation faults, so I have stayed away from it for now)"
    author: "eze"
  - subject: "Re: Speed & Conq questions"
    date: 2002-03-22
    body: "Does objectprelinking work yet?\n\nI found that everytime I compiled kde or qt with objprelinking the programs instantly seg faulted when I ran them.  Qt wouldnt compile because moc and uic were segfaulting.  This happened when compiling beta2 and since then I havent bothered to try prelinking RC1, RC2 or RC3.  RC3 is good but it needs objprelinking to make it super fast.\n\nIm using gcc 2.95.4 20011006 (Debian) according to kde.\n\n~Mike"
    author: "Mike"
  - subject: "Re: Speed & Conq questions"
    date: 2002-03-23
    body: "This happened to me too, using Mandrake-7.1. The reason is apparently a too old libc. "
    author: "jd"
  - subject: "Re: Speed & Conq questions"
    date: 2002-03-23
    body: "It used to works on Redhat 7.1 but now im running Debian unstable. \nI just finished compiling Kde from cvs today so Ill try prelinking it next time.\n\n~Mike"
    author: "Mike"
  - subject: "Re: Speed & Conq questions"
    date: 2002-03-22
    body: ">1. It was mentioned that Konqueror and the rest of KDE3 are \"fast, fast, fast\". What is the reason for such a speed increase? Is it obj-prelinked? If not, can additional gains be made by obj-prelinking?\n\nObj-prelinking is a compile stage optimization. KDE provides source. Whether or not\nany version of KDE is obj-prelinked is at the mercy of the packagers, who are the only\nones who provide binaries (since KDE does not). I dunno if it still works with KDE3 tho."
    author: "Carbon"
  - subject: "Re: Konq questions"
    date: 2002-03-23
    body: " As far as I can tell the sidebar at http://www.vbrad.com works fine in this instance of konqueror... (3.0beta2) and I've been observing similar behaviours on (yeeha) my tdcanadatrust (webbanking) site that were NOT present in 2.2.2 ... \t\t"
    author: "Alistair Tonner"
  - subject: "Keramik style?"
    date: 2002-03-21
    body: "Does anyone know how to get this mysterious keramik style going? It doesn't show up in the control-center but there's a themerc-file in the kstyle dir. Any ideas?\n"
    author: "DennisJ"
  - subject: "Re: Keramik style?"
    date: 2002-03-21
    body: "  IIRC, you should go to kdelibs/kstyle/keramik and then make ; make install manually \n"
    author: "Cristian"
  - subject: "Re: Keramik style?"
    date: 2002-03-25
    body: "The keramik style has been removed from KDE 3.0 as it wasn't ready for release."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Keramik style?"
    date: 2002-04-04
    body: "you are probably busy, but i had to write a response here.  \n\nyour keramik style is beautiful.  wow.  please don't give up on it.  i'll just have to get along with liquid until this is finished.  not that liquid is bad.  it was my favourite until i saw keramik.  but i am really looking forward to the finished version of keramik.  it almost makes me want to run kde on my win2k partition.  \n\ngood work.  "
    author: "subgeek"
  - subject: "Any screen shot??"
    date: 2002-03-21
    body: "As usual.. any screenshots?? I don't have enough bandwidth to download by would like to see how kde glorify. :)\n\nlong live kde.\n-dmn"
    author: "dmn"
  - subject: "Re: Any screen shot??"
    date: 2002-03-21
    body: "http://www.kde.org/screenshots/kde300shots.html"
    author: "Anonymous"
  - subject: "Re: Any screen shot??"
    date: 2002-03-22
    body: "\nWow. Really nice !! :-) ...\n\nToo bad those new \"logout\"/\"lock screen\" icons have such a taste of Windows XP. I feel they are quite in contradiction with the default look and design...\nIs it just me ?\n"
    author: "Germain"
  - subject: "Re: Any screen shot??"
    date: 2002-03-23
    body: "I agree!  They seem very XP-y, and do clash with the rest of the design.\n\nWhat's more, some app icons (such as Kmail) seem to have undergone an XP/OSX high-resolution makeover, particularly with the absence of the traditional \"colored-in black outline\" cartoon look typical with pre-XP/OSX OS's, but other icons (such as KWord) have kept the old look."
    author: "Leo"
  - subject: "Re: Any screen shot??"
    date: 2002-03-24
    body: "Actually, if you look, the only app icon that have deviated from the regular app icon style is the KMail icon. This seems to be because the KMail icon is from Everaldo's glassy icon set, for some reason, whereas the others are not.\n"
    author: "Carbon"
  - subject: "Re: Any screen shot??"
    date: 2002-03-25
    body: "Please note that the KMail icon has again been reverted to the old one. The new icon will be part of Everaldo's crystal icon theme.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Any screen shot??"
    date: 2002-03-22
    body: "I hate it, too. And why these bubbles around the arrow icons of konqueror?\nThis looks quite ugly with some themes (like e.g. liquid)..."
    author: "benno"
  - subject: "Re: Any screen shot??"
    date: 2002-03-22
    body: "I found those pretty cool. But if you don't like the standards icons, you can use the Slick-icons or Ikons. They are both included in KDE3.0\n\nThere are also some cool new backgrounds"
    author: "JC"
  - subject: "So how do you really feel about it?"
    date: 2002-03-21
    body: "What gets me is that although this new version is in release candidate mode, SuSE has jumped the gun and is now selling version 8.0 with whatever snapshot build of KDE3.0 it has its hands on. Doesn't that seem a bit premature? "
    author: "Wolf Blitzer"
  - subject: "Re: So how do you really feel about it?"
    date: 2002-03-21
    body: "\"What gets me is that although this new version is in release candidate mode, SuSE has jumped the gun and is now selling version 8.0 with whatever snapshot build of KDE3.0 it has its hands on. Doesn't that seem a bit premature?\"\n\nUmmmm, no. SuSE ANNOUNCED that SuSE 8.0 with KDE 3 will be available in april (I forgot the exact date). By that time KDE has already been released. SuSE is NOT selling 8.0 yet!"
    author: "Janne"
  - subject: "Re: So how do you really feel about it?"
    date: 2002-03-21
    body: "Who says, they're selling it NOW ?\n\nGo, look at their homepage. They're goin' to sell it about mid April.\nAnyway, fairly fast ;-)\n\nRegards,\nHarald\n"
    author: "Harald Henkel"
  - subject: "Re: So how do you really feel about it?"
    date: 2002-03-21
    body: "They sell NOW, but deliver LATER (mid-April)."
    author: "Anonymous"
  - subject: "Re: So how do you really feel about it?"
    date: 2002-03-22
    body: "SuSE Linux 8.0 will contain the final KDE 3.0 as it will be released by KDE later this month. You can order SL8.0 online at http://www.suse.com now and you will be one of the first to get your hands on this shiny new desktop.\n\nBe carefull though, those CDs might still be hot :-)\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: So how do you really feel about it?"
    date: 2002-03-23
    body: "No, they will deliver it on April 22nd. Plenty of time to mkae sure it's correct."
    author: "Syllten"
  - subject: "Re: So how do you really feel about it?"
    date: 2002-03-23
    body: "No, they will deliver it on April 22nd. Plenty of time to mkae sure it's correct."
    author: "Syllten"
  - subject: "underwhelmed....."
    date: 2002-03-21
    body: "I really liked beta2 so I was a bit disappointed with how thr RC3 turned out; all of the packages are not to be found in the relevant directory, and to my impression it is much slower and less responsive - crashing through lack of memory when using other apps. Given others' positive response I wonder if this is specific to the SuSE RPM's. (Using SuSE 7.3, 64 MB+550mhz)"
    author: "Craig Nelson"
  - subject: "Re: underwhelmed....."
    date: 2002-03-21
    body: "I actually experienced the same with suse 7.2 rpms.\n\n- It seems slower than my last compiled rc2.\n- I cannot get AA work and somehow Verdana 10 looks much smaller and uglier\n  than before... (worked fine with rc2)\n- They missed kdenetworks and kdeaddons (this is however a SuSE and no kde\n  problem)\n- Still many crashes... (especially kicker)\n- konq scrolls sluggish when a site has a large or a repeating\n  background image ( e.g. http://phpbuilder.com )"
    author: "yves"
  - subject: "Re: underwhelmed....."
    date: 2002-03-21
    body: "> I wonder if this is specific to the SuSE RPM's. (Using SuSE 7.3, 64 MB+550mhz)\n\nIt might be the rpms. Another factor is RAM. As far as I remember KDE 3 is supposed to be better at conserving resources than KDE 2. One of my systems does not recognize more than 64 MB or RAM even though it has 256 MB and a 700 MHz athlon. I was adding the modification command at boot to get it to see all the memory and I went to use it and found it was running slower than a 500 MHz AMD K6 with 128 MB of RAM. (My wife had decided to reboot it) In simple terms RAM is about 1,000 faster than your hard disk so moving to 128 MB or more will really speed your system up no matter what! For a few dollars you could feel like you have a new system."
    author: "Eric Laffoon"
  - subject: "Re: underwhelmed....."
    date: 2002-03-21
    body: "What font are they using in those screenshots? My AA never comes out look that sharp!"
    author: "Rayiner Hashem"
  - subject: "Re: underwhelmed....."
    date: 2002-03-21
    body: ">What font are they using in those screenshots? My AA never comes out look that sharp!\n\nI think it's fresh version of freetype2 (+bytecode support). No other magic.\n(Some distributions have many version of libfreetype and even you install newer libfreetype there could be an older one which came with XFree 4.x).   "
    author: "Jaana"
  - subject: "Re: underwhelmed....."
    date: 2002-03-22
    body: "Actually, I recompiled FT2 with the bytecode interpreter long ago. Still doesn't look that sharp. It must be the fonts."
    author: "Rayiner Hashem"
  - subject: "Re: underwhelmed....."
    date: 2002-03-23
    body: "Sincerly, do you find those fonts pretty ?\n\nNo doubt, I hate AA, even on their screenshots.\nWill always disable it, it reminds me too much\nfuzzy PDFs in Acrobat...\n"
    author: "Christian"
  - subject: "Re: underwhelmed.....[now better]"
    date: 2002-03-23
    body: "I've managed now to resolve most of the problems by reinstalling the rpms....\n\nAn enduring problem is using mozilla with rc3 (works well on other desktops); konqueror is fine but with mozilla it crashes the desktop."
    author: "Craig"
  - subject: "Re: underwhelmed....."
    date: 2002-03-23
    body: "Very strange!\n\nI'm overwhelmed, really. I'm also using SuSE 7.3, but on a 400 MHz with 198 Mb. It's awesome here, much faster than KDE2. No complaint at all. And I love the edutainment packages, especially ktouch.\n\n"
    author: "Jos"
  - subject: "i need libkab.so.4"
    date: 2002-03-21
    body: "i installed the suse7.2 packages. where can I get that lib, I want my kmail!!! ;-(\n\nrobert@notebook:~ > kmail\nkmail: error while loading shared libraries: libkab.so.4: cannot load shared object file: No such file or directory\n\nnotebook:/opt/kde3/lib # l libkab*\n-rwxr-xr-x    1 root     root         1106 M\u00e4r 19 13:04 libkabc.la*\nlrwxrwxrwx    1 root     root           16 Jan  7 15:40 libkabc.so -> libkabc.so.1.0.0*\nlrwxrwxrwx    1 root     root           16 Jan  7 15:40 libkabc.so.1 -> libkabc.so.1.0.0*\n-rwxr-xr-x    1 root     root       238897 M\u00e4r 19 13:16 libkabc.so.1.0.0*\n-rwxr-xr-x    1 root     root         1469 M\u00e4r 19 13:28 libkaboodlepart.la*\n-rwxr-xr-x    1 root     root       138214 M\u00e4r 19 13:41 libkaboodlepart.so*\n\n"
    author: "Robert Penz"
  - subject: "Re: i need libkab.so.4"
    date: 2002-03-22
    body: "found the problem ... the kde network rpms are missing for suse!!!"
    author: "Robert Penz"
  - subject: "Re: i need libkab.so.4"
    date: 2002-03-25
    body: "Same problem with RedHat.  I downloaded the packages that were on the kde site when this beta was first released.  I guess they weren't done posting them.  I downloaded what I was missing from redhat.  All seems well now.\n\nKDE3 looks pretty cool!"
    author: "AC"
  - subject: "keyboard shortcut to start app"
    date: 2002-03-22
    body: "Is there a way to start an application with a shortcut? Where is it configurable?\nI want to open a new xterm with ALT+T, mutt with alt+m. \n\nI found lots of things for which I can configure the shortcut, but I didn't find a way to create a new action for  a new shorcut....\n"
    author: "adm"
  - subject: "Re: keyboard shortcut to start app"
    date: 2002-03-22
    body: "Try the menu editor (^K/Configure Panel/Menu Editor... ) and look for the entry of your preferred app. The last entry on the config page for any item is \"Current key\"."
    author: "Inorog"
  - subject: "Re: keyboard shortcut to start app"
    date: 2002-03-22
    body: "Isn't it possible to create a shortcut to start an application not in the menu?\nIf not, I 'll live with it. If yes, it would be great :-)"
    author: "me again"
  - subject: "Re: keyboard shortcut to start app"
    date: 2002-10-07
    body: "Yes, it's possible to do this.\nAs root, create a file under /etc/X11/applnk\nFor example, I created /etc/X11/applnk/Utilities/xtermc.desktop\ncontaining:\n\n[Desktop Entry]\nName=Colour XTerm\nComment=Colour XTerm\nExec=/home/matt/utils/bin/xtermc.sh\nTerminal=0\nType=Application\n\n(Which runs a colour xterm with my peculiar preferences)\n\nI then used the K/Configure Panel/Menu Editor as described above to set the Current Key for this shortcut.\n\nHTH,\nMatt"
    author: "Matt Gumbley"
  - subject: "What about a splashscreen?"
    date: 2002-03-22
    body: "Hi folks!\n\nI think KDE 3 is really lovely and you're all doing a great job!  But I also think it's a bit strange there is no splash screen for KDE3 (yet). I mean the one for KDE 2 was really cool and it's an important part of the eye candy, especially to convince new users. It will be the first (graphical) thing they see when booting a pre-installed KDE3-machine (After KDM of course ;-).\nThe KDE-community has so many artists who do wonderfull things with images, so can anybody explain me why there isn't any splashscreen yet? \nIt's as far as I know one of the real showstoppers by now.\nAnother question, are there any links to the splashscreen made by Qwertz, which is mentioned in the KDE-feature list?\n\nRegards,\n\nJasper"
    author: "Jasper Boot"
  - subject: "Re: What about a splashscreen?"
    date: 2002-03-22
    body: "Fixed."
    author: "Tackat"
  - subject: "Warning, Newbie alert."
    date: 2002-03-22
    body: "Hi.\nI want to compile KDE3.0 on a machine that does not have an internet connection.\nCurrently I'm using SUSE 7.2.\nWhere can I go to find out \na)what files I need \nb)how to install them from a zip disk."
    author: "SlipperJim"
  - subject: "Re: Warning, Newbie alert."
    date: 2002-03-22
    body: "Well, you need either SuSE 7.2 RPMs or source files. If you can stand\ncompilng from source (takes a while, especially on slow comps, but\ncan often result in a faster KDE) then go and download the source\ntarballs, and put those on the disc. If you want to use RPMs, you\ncan get those too, probably from SuSE's site (though I hear they have\nsome problems).\n\n Installing from a zip disk is exactly the same\nas installing from anywhere else on SuSE, just install the RPMs\nusing the rpm command. "
    author: "Carbon"
  - subject: "KDE3 KICKS ASS!!!!"
    date: 2002-03-22
    body: "GREAT AWESOME JOB KDE-DEVELOPERS!!!!\n\nYou have created a spectacular Linux Desktop Environment!!!\n\nThank you very much.. for an awesome linux desktop...\n\nCheerz,\nMagnus\n\n"
    author: "Magnus Jonsson"
  - subject: "Missing kdenetwork.rpm for SuSE 7.3 ?"
    date: 2002-03-22
    body: "I am really, REALLY impressed how smooth the new RC is running! Actually, this has been the first time multimedia stuff, sound etc. worked \"out of the box\" .. at least for me. Great Stuff! \n\nOne thing: I can't find the Network package for SuSE 7.3 .. am I blind, stupid, or is it really missing ?"
    author: "Denis"
  - subject: "Re: Missing kdenetwork.rpm for SuSE 7.3 ?"
    date: 2002-03-22
    body: "nope.\nThere was a compilation error on monday when the rc3 tarballs were ready. The tarball from tuesday is ok but Suse didn't update (add) the network package since.\n\nBut, who is using Kmail anyway ??? ;-)))"
    author: "JC"
  - subject: "Re: Missing kdenetwork.rpm for SuSE 7.3 ?"
    date: 2002-03-23
    body: "I do. KMail is simple and excelent mailer, especialy for us, users of non-latin1 charsets."
    author: "fura"
  - subject: "Re: Missing kdenetwork.rpm for SuSE 7.3 ?"
    date: 2002-03-24
    body: "Have a look at: ftp://ftp.kde.org/pub/kde/unstable/kde-3.0rc3/SuSE/7.3-i386+kde\n\nBut kdegraphics is still missing. So I compiled it for myself.\n\nWorks nicely.\n\nRegards,\n Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "RedHat QT package?"
    date: 2002-03-22
    body: "I tried installing the kdelibs package on RH 7.2 (only kdelibs because I wanted to see where the files were going to be placed), and I got the following failed dependencies:\n\nqt >= 3.0.3 is needed by kdelibs-3.0-0.rc3.1\narts >= 7:0.9.9 is needed by kdelibs-3.0-0.rc3.1\ncups-libs >= 1.1.12 is needed by kdelibs-3.0-0.rc3.1\nlibartsflow.so.1   is needed by kdelibs-3.0-0.rc3.1\nlibartsflow_idl.so.1   is needed by kdelibs-3.0-0.rc3.1\nlibkmedia2_idl.so.1   is needed by kdelibs-3.0-0.rc3.1\nlibmcop.so.1   is needed by kdelibs-3.0-0.rc3.1\nlibqt-mt.so.3   is needed by kdelibs-3.0-0.rc3.1\nlibqtmcop.so.1   is needed by kdelibs-3.0-0.rc3.1\nlibsoundserver_idl.so.1   is needed by kdelibs-3.0-0.rc3.1\n\nNone of those really worries me except for the one about QT. I don't see a package for QT in the RedHat directories. Are the libraries in another package?\n\nI hope I'm not going to have to compile QT from source... it takes forever on a 300mhz machine."
    author: "Anonymous"
  - subject: "Re: RedHat QT package?"
    date: 2002-03-22
    body: "I used Bero's http://www.linux-easy.com\n\nI loaded the bin RPM:\nhttp://www.linux-easy.com/daily/RPMS/qt-3.0.3-0.cvs20020319.1.i386.rpm\n\nworks great. If you want to compile it yourself, try the src one:\nhttp://www.linux-easy.com/daily/SRPMS/qt-3.0.3-0.cvs20020319.1.src.rpm\n\nGood luck!\n\n"
    author: "themetaguy"
  - subject: "Re: RedHat QT package?"
    date: 2002-03-22
    body: "Too bad Bero doesn't provide an RPM for kde-edu...hopefully he will once KDE3 is released for real."
    author: "Jason"
  - subject: "Re: RedHat QT package?"
    date: 2002-03-22
    body: "go to redhat.com, then click on the download link, at the bottom of the page they'll be a link for redhat mirrors, find a mirror that has rawhide, this is redhats most upto date release thats being worked on before they release it as beta.  theres upto date rpms there.  also search google for the missing files and usually you can find the package they come from.\n\n"
    author: "zep"
  - subject: "Re: RedHat QT package?"
    date: 2002-03-23
    body: "try redhat's rawhide.  get on ftp.redhat.com and go to the rawhide rpms, these are the most up to date rpms for redhat that aren't supported.  this is where i got qt."
    author: "zep"
  - subject: "It's not fast on a PII-233 w/ 64mb RAM"
    date: 2002-03-22
    body: "got a room full of those :-("
    author: "NotSpeedyBox"
  - subject: "Re: It's not fast on a PII-233 w/ 64mb RAM"
    date: 2002-03-22
    body: "Have you checked RAM prices lately?"
    author: "reihal"
  - subject: "Re: It's not fast on a PII-233 w/ 64mb RAM"
    date: 2002-03-23
    body: "For enterprise, it's not easy to upgrade all the machines.\nA good and optimized system must run on a little machine (PentiumMMX / 64Mo)."
    author: "cosmicflo"
  - subject: "Re: It's not fast on a PII-233 w/ 64mb RAM"
    date: 2002-03-22
    body: "\nThen you'd better stick to fvwm or similar, because none of the modern GUI's are going to run fast with that little memory.  You need to grow your hardware to match the growth of the software.  That's life.  "
    author: "Neondiet"
  - subject: "Re: It's not fast on a PII-233 512MB RAM either"
    date: 2002-03-23
    body: "It's not just a RAM issue ... Running X with KDE you are going to hit swap just sitting idle if you have only 64megs! Win95 maybe 98 will run ok on those kind of machines so why switch. Maybe using them as \"X terminal\" type systems would be better?\n\nLinux as a server is cool and efficient on older hardware. Linux as a desktop demands lots of horse power (like SGI,  Sun etc etc).\n\nAS A WORKSTATION LINUX (and BSD) DOES NOT WORK ON OLDER HARDWARE ... use that older hardware for tiny little servers or rip out the RAM from half of them and sticky it in the other half ;-)"
    author: "NameSuggesterEngine"
  - subject: "Re: It's not fast on a PII-233 w/ 64mb RAM"
    date: 2002-03-23
    body: "Or half a room full of P2-233s with 128 MB RAM..."
    author: "not me"
  - subject: "Re: It's not fast on a PII-233 w/ 64mb RAM"
    date: 2002-03-25
    body: "Not using anti-alising for fonts makes a lot of difference with this kind of configuration. Try switching the anti-aliasing off."
    author: "Bojan"
  - subject: "Fastest browser on earth"
    date: 2002-03-22
    body: "/me thinks Opera has no right to claim to have the fastest browser on earth anymore :) The new Konqi simply rocks. How are they doing that? It seems they read minds and preload all pages I want to look at :)"
    author: "/me"
  - subject: "Re: Fastest browser on earth"
    date: 2002-03-22
    body: "hahahaha\nThat was a funny one, I admit it, i was laughing for about 5 minutes...\n\nKDE RoX!!!!!!!!!!!!!!!!!\n\np.d. Im the only mexican who uses kde (you know... miguel de icaza... GNOME.... awk)\n\nj0rd1"
    author: "j0rd1"
  - subject: "Re: Fastest browser on earth"
    date: 2002-03-25
    body: "What's wrong with awk? ;-)"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Fastest browser on earth"
    date: 2003-06-03
    body: "What were you talking about.....awk??"
    author: "Elijah Extreme"
  - subject: "Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2003-06-03
    body: "I am In search for the wolrd fastest browser..So If you're a software designer and know how to make a browser & in some ways made it a speedy browser which would be the best of the best, please make me the first one to know about...\nElijah Extreme(jayjay242424) contact me if you got the world's fastest browser...\nhotmail::jayjay2424@hotmail.com\nyahoo!::jayjay242424@yahoo.com\nhurry up! im tired of slow IE's and Netscape\n\nIm jayjay242424."
    author: "Elijah Extreme"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2003-06-21
    body: "try opera,crazy browser,fast browser pro v6.1"
    author: "dreams"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2003-08-02
    body: "Hey thanks...But i dont think they're fast enough"
    author: "Elijah Extreme"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2003-11-05
    body: "How about mozilla browser"
    author: "iseng"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2004-02-20
    body: "Get Bluto 5.0 (Built for Speed). No installation required, fits on a floppy and nothing is faster! Get it from http://blackhat.27south.com"
    author: "Bluto"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2006-02-12
    body: "mozilla suckssssssssssssssssssssss.\n"
    author: "mitul"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2008-08-29
    body: "Mozilla today in 2008 is rated worlds fastest and most lightweight browser."
    author: "SamsungGuy"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2006-08-12
    body: "try kmeleon man faster and high internet security"
    author: "so"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2004-12-02
    body: "i have a famous browsers that are called: Avant browser, Mozilla Firefox browser.\ni reckon these are one of the most popular browses and they search anything in seconds if u hav an internet booster.\n\nAvant browser- i think is one of the fastest and modern style Browser.\n\nmozilla Firefox- is not modern but a simmple Browser that just looks plain but in the other hand its fast"
    author: "ProDeliver"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2005-02-05
    body: "So am I. I am good at using search engines to find information, yet I cannot find anything about small fast powerful text browsers that use a mouse, in four search engines using multiple key words. I have searching for days. If you found one, please let me know what it is by email. If any program creators come across this board, please get to work on creating the above described browser straight away. I think people will buy it as it will compete with Internet Explorer and Netscape, and all the other so-called fast browsers, as they are all slow in particular when sites slow down. I'm not interested in graphics, just the text. All I want at top is Save As and Favorites."
    author: "David Carroll"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2005-02-05
    body: "Ps. I did find one browser, a text browser with no hyperlinks and which uses the keyboard instead of the mouse. It is called Lynx and you can find it in the google search engine. It opens fast but I coulnd't figure out how to use it. I have emailed the creator to ask him to develop it to use a mouse. There is also a fast browser called OffByOne which uses the mouse, but I found it to be a bit unstable and did not always make posts. It was good for reading posts though, on a board that often slowed right down, and I had the graphics turned off. The Enigma browser and Smart browser were a little bit faster than Internet Explorer, but were still too slow for what I'm looking for. I think you can get all of the browsers that I've mentioned here at Computerworld Browsers."
    author: "David Carroll"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2005-02-05
    body: "Computerworld Browsers. Not all in this list are browsers, and this is not the first page: http://tinyurl.com/3ty7d\n"
    author: "David Carroll"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2006-05-09
    body: "Hello people ... I think Maxthon is fater than fire fox, I'm useing it right now.\nI'm working for HP canada, all Agents here prefer useing  Maxthon. \n\nIt's really good try it."
    author: "Mustafa Kamal"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2006-05-09
    body: "mozilla fire fox is the world fastest browser i have tried it and found best far better than opera\n"
    author: "asim"
  - subject: "Re: Looking for the THE WORLD'S FASTE"
    date: 2006-07-04
    body: "http://blackhat.27south.com and opera are the best. I test this kinda stuff for a living. ;)"
    author: "John"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2006-07-05
    body: "hey try avant browser its fast and free\nif you find somethin better let me know k thanks"
    author: "mason"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2007-04-14
    body: "don't think you'll see this cause its 2007 but ace explorer is the fastest among all those i have tried:Mozilla Firefox, Internet Explorer 7, Opera9.20, Avant Browser. currently downloading konqueror and bluto but i don't think they'll be faster than ace cause ace really does done loading in the blink of an eye. will let you know again if konqueror or bluto is faster...(=. wow it feels weird to be talking to someone who posted in 2003, 4 years back. like talking to someone from the past X)=)"
    author: "someone from 2007"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2007-04-14
    body: "alright im wrong X/...finished downloading bluto (konquerer cannot be downloaded) and did a small test:(get to google from yahoo homepage)\n~RESULTS~\ninternet explorer 7: did it in 5 seconds\nace explorer: took 4 seconds\nbluto: done in 3 seconds or even quicker...\n\n"
    author: "someone from 2007"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! HALLA"
    date: 2008-08-05
    body: "Try PhaseOut, it's cool and fast."
    author: "Manish"
  - subject: "Re: Looking for the THE WORLD'S FASTEST BROWSER! H"
    date: 2009-01-05
    body: "I have literally tried nearly every single browser there is, faster than both opera and mozilla is google chrome, yes its hard to believe but once you try it you won't go back, :), give it a go "
    author: "Oli"
  - subject: "Re: Fastest browser on earth"
    date: 2004-01-30
    body: "I have been told that Safari is the fastest - BUT you need a Mac to use it :\u00ac\\\n\nlinks-graphic for Linux Mandrake is supposed to be even faster :\u00ac\\\n\nBut being a mere Windows walla I make do with Mozilla Firebird 0.7+ (but it's the dog's so far)"
    author: "kevan"
  - subject: "Re: Fastest browser on earth"
    date: 2004-07-26
    body: "Nope, safari would be the fastest ON A MAC, because it's based on konqueror.  But Konqueror is certainly faster than Safari.  Probably due to the simple fact of Safari using Mac OS 's heavy GUI."
    author: "Lee"
  - subject: "Re: Fastest browser on earth"
    date: 2004-03-26
    body: "Isint konqueror fast enough?"
    author: "PZ"
  - subject: "Re: Fastest browser on earth"
    date: 2004-04-07
    body: "Konqueror is fast so, thats all you need (unless you are used to other browser, or you dont have linux)."
    author: "nnn"
  - subject: "Re: Fastest browser on earth"
    date: 2005-04-18
    body: "Hai...I tried to use Bluto browser...It told an error message...That is in screen shot..."
    author: "Nehemiah"
  - subject: "Re: Fastest browser on earth"
    date: 2005-04-27
    body: "All u think ur browser is fastest well my fav browser is firefox but the fastes browser is opera 8.0 i have opera 8.0 but i am so much used to firefox i love their custamisable and above all its a open source software + a free ware if u still doubt which is the fastest browser go to the following and check ur brosers speed and check out the top results \n\nhttp://www.24fun.com/downloadcenter/benchjs/benchjs.html\n\nDefanitly OPERA8.0 is the fastest BUT FIREFOX RULZ!!!!\n\nFrom a big firefox faaaaaaaaaaaaannnnnn................."
    author: "neolivz"
  - subject: "Re: Fastest browser on earth"
    date: 2005-04-27
    body: "Hmm.  Your favourite browser seems to drop letters at random, supports punctuation only half of the time and if it does then it prints them multiple times in a row.  \n\nA strange beast you have there.  You may want to check for an update... :-P\n\n\nBTW:  You've got to be kidding.  The test you linked hardly measures browser speed.  It's only about JavaScript speed.  And it completely mis-detected the CPU speed of my system...\n\n"
    author: "cm"
  - subject: "Re: Fastest browser on earth"
    date: 2004-10-27
    body: "Try Avant browser \nit is fast \nand very handy"
    author: "SATAN"
  - subject: "Re: Fastest browser on earth"
    date: 2007-01-07
    body: "\nFOR FIREFOX\n-Mozilla Firefox is much more standards compliant.\n-Mozilla Firefox has less \"per window\" memory & CPU usage\n-Mozilla Firefox releases new builds more quickly.\n-Mozilla is open source (many, many options)\n-Mozilla is good on many OSs, not just Windows (Opera has difficulties on some Linux distros)\n-Because it is open source, Mozilla cannot be commercilized and cannot be put to the discretion's of a single company.\n\nFOR OPERA\n-It is a bit more lightweight (In terms of install files and completed install)\n-Not open source.  Exploits are not seen by public, therefore not by hackers.\n-Email Client is much more lightweight than Thunderbird.\n-Usually stands up higher in terms of cold and warm starts during tests.\n\nI personally use Mozilla, but I am sure there is more going for Opera than I put here.  I keep an open eye, but so far, Mozilla does a much better job at most tasks.  (IMHO)"
    author: "Aaron Clay"
  - subject: "Re: Fastest browser on earth"
    date: 2007-02-04
    body: "I've used Advant and I found it fast but it crashes quite often.  Also my friend has found that Advant is notably faster than Opera on dial up.  \n\nIts always the graphics that seem to take up the time so lately I have been trying out Lynx, a text based browser.  I'm finding thats super fast on 60% of the pages and the other 40% of the pages won't load properly. Too bad.  Gonna try to use Cygwin with w3m on a windows platform to see if I can get some result there.\n\nIf anyone can add to my experimentation, please let me know.\n\nAl  "
    author: "Albert Lam"
  - subject: "Re: Fastest browser on earth"
    date: 2007-02-20
    body: "I've checked some of the browsers in windows 2000, (Maxthon 2.0 beta, Netscape Navigator 8.1.2, Opera 9.10, Mozilla FireFox 2.0.0.1, Internet Explorer 7) finally i found the greatest browser. Yes it's FireFox. FireFox is much more Faster than any other  browsers i used in my PC. The Opera Passes Acid 2 Test for browsers, but it not supports all the PDF format files but FireFox did. No answers for other browsers "
    author: "Prabhu.P"
  - subject: "Re: Fastest browser on earth"
    date: 2007-03-01
    body: "Regarding you post:\nOpera is more standards compliant than firefox, it has passed the acid2test, firefox has not.\nOpera takes less memory, much less. If you do not believe me download Opera and see for yourself\nFirefox is open source, and that is its main advantage over opera, a philosophical reason. Open source software is my software. Does not make it faster or better performing though.\nOpera I cannot come to terms with your statement that exploits cannot be seen by hackers. Hackers if they really want, can crack the code and find vulnerabilities, and the general public will not be any the wiser. Therefore, not being Open source is a security disadvantage. However, still Opera is at present the fastest browser.\nBrowser comparison: http://freewebsoftwarereviews.blogspot.com\nBrowser speed comparison:http://www.howtocreate.co.uk/browserSpeed.html\nBTW, I do not want to flame a Opera vs Firefox war, both are cool. For me Opera is better though"
    author: "Suren"
  - subject: "Re: Fastest browser on earth"
    date: 2007-03-24
    body: "I am having the same problems in deciding as everyone else seems to be collectively pondering. Naturally we need to get the most from our slower net hookups like dial up but I have been using various browsers and find that they vary in usage from time to time. Has anyone had the experience that rarly the data stream comes in so fast that one can not keep up with the pages flow? This has happend to me about 5 times in the last 6 years and I stayed up for two days one time watching the action. Is this a server doing some kind of work on the system-or have we been sold a bill of goods concerning the differences between cable -DSL- and Dial up which I have the latter. Is it possible that there is enough bandwidth on dial up that a good pc under the right conditions can really zoom on the so called dial up connection? billw86 "
    author: "billw86"
  - subject: "problem"
    date: 2002-03-22
    body: "i compile kde2-rc2 and qt-3.0.2 from source,\nbut when i start kde3 none of the icons show up.\nfrom the console, it says \" incompatible libpng \"\nthe libpng in my system is version 1.2.1.\ni don't understand i did compile qt3 as explained in  http://www.kde.org/install-source.html\n"
    author: "hw"
  - subject: "Re: problem"
    date: 2002-03-25
    body: "You need libpng 2.x."
    author: "Ingo Kl\u00f6cker"
  - subject: "Mandrake 8.2 RPMs"
    date: 2002-03-22
    body: "...anyone listening over there?"
    author: "Anonyomoose"
  - subject: "Re: Mandrake 8.2 RPMs"
    date: 2002-03-22
    body: "There were KDE 3 rpms announced on cooker mailing-list. Look out for them!"
    author: "Anonymous"
  - subject: "Re: Mandrake 8.2 RPMs"
    date: 2002-03-26
    body: "I didn't found anything in the cooker mailing list. If you can tell me the link I would appreciate it a lot.\nThx in advance !"
    author: "Anonymous"
  - subject: "Re: Mandrake 8.2 RPMs"
    date: 2002-03-26
    body: "http://www.mandrake.com/en/archives/changelog/2002-03/msg00744.php\neg here, ftp://ftp.uni-bayreuth.de/pub/linux/Mandrake-devel/contrib/i586/"
    author: "Anonymous"
  - subject: "Re: Mandrake 8.2 RPMs"
    date: 2002-03-26
    body: "I didn't found anything in the cooker mailing list. If you can tell me the link I would appreciate it a lot.\nThx in advance !"
    author: "Anonymous"
  - subject: "libkfile.so.3?"
    date: 2002-03-22
    body: "I run RedHat 7.2. I tried installing by rpm but i run into a lot of problems.\nThe library that appears to be missing is libkfile.so.3? Is this  suppose to come with kdelibs? if so why is it not on my system ( i installed kdelibs). Where can i find the library? Can anybody provide some advice? thank you in advance.\n"
    author: "kde user lost"
  - subject: "Re: libkfile.so.3?"
    date: 2002-03-22
    body: "You probably are having conflicts with a previously installed version of kde.\n\nTry either removing the old version, or setting up one set of environment variables for one user, and creating a second user to run kde - that way the libraries don't get crossed :)\n\nTroy\ntroy(at)kde(dot)org\n"
    author: "Troy Unrau"
  - subject: "Re: libkfile.so.3?"
    date: 2002-03-25
    body: "I had all kinds of problems like this.  I think I started downloading the rpms before they were all posted.  Redhat also has them on their ftp site in their lastest beta area.  After I got all the RPMS I all is now well!"
    author: "AC"
  - subject: "KDE2 vs KDE3"
    date: 2002-03-22
    body: "wuts ze difrens b/w 'em?"
    author: "Yamamba"
  - subject: "Re: KDE2 vs KDE3"
    date: 2002-03-22
    body: "One word: CHANGELOG.\n\n(duh)"
    author: "Nosferatu"
  - subject: "Re: KDE2 vs KDE3"
    date: 2002-03-25
    body: "The new version is better."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Threads"
    date: 2002-03-22
    body: "Did someone know about NGPT (Next Generation POSIX Threading) support in Qt\\KDE?\n\nhttp://oss.software.ibm.com/developerworks/opensource/pthr say:\n12 March 2002: Linux Kernel 2.4.19pre3 now has the NGPT support built in. Using this kernel and higher, you will no longer need to patch the Linux kernel for NGPT support."
    author: "me"
  - subject: "KDE-Graphics for Suse-7.3 ?"
    date: 2002-03-22
    body: "All is in the title: no kde-graphics for suse-7.3. Can I download it anywhere ?\n\nPS: (how) can I run Yast2 within KDE3 ?"
    author: "Julien Olivier"
  - subject: "Re: KDE-Graphics for Suse-7.3 ?"
    date: 2002-03-22
    body: "Re: Yast2?\n\nWell, KDE2/3 are binary incompatible. You can wait for SuSE 8.0."
    author: "Stephan Oehlert"
  - subject: "Re: KDE-Graphics for Suse-7.3 ?"
    date: 2002-03-22
    body: "Just two questions about Suse-8.0:\n\nwill it feature a KDE3-compatible version of kwinTV ? I really like this app.\n\nwill it be possible to totally update a Suse-7.3 to Suse-8.0 (Yast2, KDE, Kernel...) via Online Update or will I have to buy a new box ? (if online update is possible, when will it be possible ?)\n\nThanks."
    author: "Julien Olivier"
  - subject: "Re: KDE-Graphics for Suse-7.3 ?"
    date: 2002-03-23
    body: "1) I have no idea.\n\n2) No it won't. This wouldn't make any sense at all since SuSE wouldn't make any money any more. If you want this feature, use Debian.\n\nLatez"
    author: "Stephan Oehlert"
  - subject: "What do you mean exactly?"
    date: 2002-03-23
    body: "For RC3, which I'm running right now with 7.3, SuSE made a kdeartwork rpm. It's got lots of cool wallpapers and icons, especially from KDE-Look.org. \n\n"
    author: "Ty"
  - subject: "Re: What do you mean exactly?"
    date: 2002-03-25
    body: "But kdeartwork doesn't have kghostview and kview, does it ? Those packages go into kdegraphics and I can't find them."
    author: "Julien Olivier"
  - subject: "Yes, you are indeed correct"
    date: 2002-03-25
    body: "I was wondering where they were! I got kdeartwork and kdegraphics confused. Thanks for reminding me.\n\nNow that you mention it, a lot of the KDE utilities seem to have not been included in RC3 rpms...\n\nI dunno what to do, except just wait for the final, or compile from CVS."
    author: "Ty"
  - subject: "YaST2 in KDE3"
    date: 2002-03-23
    body: "Yep. YaST2 is statically linked to whatever Qt libs it needs (like Opera can be), so it's not necessarily dependent upon what version of Qt you have.\n\nI've upgraded to KDE3 RC3 and Qt 3.0.2, and KDE2 and Qt2 apps still run fine. The libs get installed into different directories (as do KDE2 and KDE3) so you don't have the problem of a newer version overwriting the older one.\n\nYaST2 runs nice 'n' spiffy in KDE3."
    author: "Ty"
  - subject: "Re: KDE-Graphics for Suse-7.3 ?"
    date: 2002-03-24
    body: "I've compiled it mysef. Works nicely.\n\nRegards,\n\tDieter\n\nftp://ftp.kde.org/pub/kde/unstable/kde-3.0rc3/SuSE/7.3-i386+kde\n\nkdenetwork3 is still there."
    author: "Dieter N\u00fctzel"
  - subject: "Do you have problems with sound in RC3?"
    date: 2002-03-24
    body: "Sounds work fine in KDE2, but in RC3 some sound events don't work while some do. Is any other SuSE user - or user of any other distribution - having this problem? \n\nAlso, KHTML wants to antialias everything at all sizes. This was not a problem in KDE2; it rendered in accordance with my XftConfig file (non-AA for certain font size range). KHTML in RC3 doesn't obey this; it AAs all font sizes. Non-KTML is AA'ed or non AA'ed appropriately."
    author: "Ty"
  - subject: "Typo"
    date: 2002-03-24
    body: "That last sentence should read, \"Non-KHTML stuff (like menus, buttons, UI in general) is AA'ed or non-AA'ed in accordance with XftConfig."
    author: "Ty"
  - subject: "Re: Do you have problems with sound in RC3?"
    date: 2002-03-25
    body: "AFAIK, it's a bug in Xft triggered by the way KHTML handles font sizes.. There might be workaround in final, not sure..\n"
    author: "Sad Eagle"
  - subject: "Re: Do you have problems with sound in RC3?"
    date: 2002-03-25
    body: "For the sound event problems, I noticed it and reported it. It seems that it's an artsplay problem. Try running artsplay from a terminal: it'll crash artsd !"
    author: "Julien Olivier"
  - subject: "Re: Do you have problems with sound in RC3?"
    date: 2002-03-25
    body: "I sent 'em an email, too. \n\nWhenever I run artsplay from a terminal, it jsut returns without doing anything. Then when I run it again, it complains about not being able to connect to the sound server. Now, I do have a firewall running, but only on my ethernet interface, not lo (did that once, and discovered lots of thing didn't work :o) ).\n\nI wonder if it's a source-code level problem or a packaing problem? Someone suggested that I recompile my SDL libs, maybe get an updated version. I haven't tried it yet. It is that, though, then that would mean the problem in on SuSE's end of the line, instead of KDE's.\n\nAre any other distros having problems? \n"
    author: "Ty"
  - subject: "KDE3 is too slow"
    date: 2002-03-22
    body: "how on earth, can you say KDE3rc3 would be fast, fast, fast ???\non my 600MHz PIII, all SCSI with 380meg and kde3rc3 built from source is even slower than kde2.2.2\n (built as release, with all the recommended options)\n logging in takes for ever\n switching virtual desktops takes half a second for all apps to show up\n slidebars for konsole, kompare, ... are extremely slow. the text is miles away from the slidebar position\n launching applications still needs a lot of time\n drag and drop for Icons on the desktop still makes the icon run behind the mouse pointer\n selecting text also is not as fast as moving the mouse\n only browsing the file system preforms better now, but this should not even be necessary to talk about.\nI don't mind the minor quirks like konqueror does not get the mouse pointer right on slidebars or similar, but why do you spend so much effort on special features (animating icons, themes, themes, themes) rather than improving the basic (look &) feel = performance?\nThere would be a lot of developers in my company, willing to switch to linux/kde, but not if it is ten times slower than windows2000/XP. ... and nobody will spend money for 2GHz machines, just for animated icons ...\nIt once was a really fast system, but now I'm really dissapointed seeing Linux going this direction and tired of waiting for a usable desktop."
    author: "arabbon"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-22
    body: "I'm running the same configuration with all SCSI.\nI compiled both KDE-2.2 and KDE-3.0\nKDE3 is faster than old release. I agree that is not as fast as some windows apps but a big effort has been done.\n\nI didn't tried yet pre-objlink with KDE-3.0 (only KDE-2.2). It's improving speed but some stuff like javascript does not work.\nMay you should swith to gcc-3.x to have better results.\n\nJC"
    author: "JC"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-23
    body: "No. Do not switch to gcc 3.\nIt simply doesn't compile C++ correctly (bad code generation).\nMaybe 3.1 will works, but there are other problems than\nvirtual classes to cope with even with 3.1.\n"
    author: "Christian"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-23
    body: "i tried kde3 on my laptop. It has 266 non mmx cpu and 48 megs of ram... and it runs fairly smooth, even with _all_ effects running.\nSo, i'm not sure about your report, but i think that maybe the problem does not sits in kde itself.\n\nAbout the whole kde, well... i didn't saw (in the front face... behing the curtain seems really different) a lot of really exceptional improvements. I think kde really needs:\n\n1) something that is able to deal with the plenty configuration of the system, from users to peripherals to apache/ftpd conf... and so on. For now a lot of these are arranged by some applications from mandrake, redhat, suse and so on, but i think it's the wrong direction. The best idea should be sort like a daemon for a uniform configuration mode that deal on one side with different config files, and on the other side with different config dialogs (so this daemon could be contacted from gnome/distro specific config dialogs. maybe gconf is the answer?)\n\n2) more pluggability... i was wondering if kde allows a simple toy like embedding something (like an image) in a rbm popup on a file.\n\n3) something that really deal with installation of kernel drivers and so on...\n\n4) something really kool that remove the windows/macos desktops from the earth's face.\n\ni'm not doing critics on kde developer work. They are great and their code is great too. I'm only reporting something that, imho, should go in the todo list to grant some future enhancement of linux/bsd on the desktop.\n"
    author: "munehiro"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-23
    body: "It must be something wrong with your setup!\n\nI have PIII 500, just IDE and 256MB and KDE3rc3 *is* running fast. In fact I think it has least 25% faster feel than KDE2.2.2 and that for free! Think about how much you have to pay for hardware to speed up your computer by this figure :)\n\nChristian"
    author: "christian"
  - subject: "Re: KDE3 is too slow (YOU ARE ABSOLUTELY RIGHT)"
    date: 2002-03-24
    body: "These guys in KDE3 mini-forum can be only... guess who... the 'actual' KDE developers! KDE3 is slower than KDE2 and plus, there are many many many bugs to be fixed. The next major KDE release for me perhaps will be 3.2.2. But SPEED is definitely something that is not promising."
    author: "Reality Check"
  - subject: "Re: KDE3 is too slow (memory????)"
    date: 2002-03-24
    body: "Check your allocated memory... some  2.2 kernels didn't automaticaly recognize more tthan 64Mb... try free and top on your system. "
    author: "Roger"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-25
    body: "Compared to the 2.x versions it is fast. It still is relatively slow on my machine (550MHz PIII, 786MB) compared to win2k. \naRTs really improved. With the new version I can listen mp3 and move the mouse. \nMost apps start within 4 seconds. In the prior versions I often started apps twice because I thought the click wasn't recognized. \nIf the developers continue to improve performance like that one of future releases will be really useable.\n"
    author: "Michael"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-25
    body: "4 secs is a looooooooooong time!!\nI have a PII 350Mhz 224Mb Ram. Kde2.2.2 here and no application takes that long to get started!!!\nIf I had to wait that much for apps to get started I'd probably change to another Desktop Env.\nJ.A."
    author: "J.A."
  - subject: "Re: KDE3 is too slow"
    date: 2002-09-07
    body: "I run suse 8.0 and kde3 on a 1.9 GHz, 768 Mb and the apps start within 3 sec. for me. XP is much faster. "
    author: "fn"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-25
    body: "my results have been varied. At work it is super speedy on my PIII-450 256MB (faster than kde-2.2.2). At home it is pokey on my K6II-450 (slower than kde-2.2.2).\n"
    author: "eze"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-29
    body: "I simply can't beleive this. Either you've fucked up badly during installation, \nor you're not telling the truth.\n\nI run KDE on three machines - one celeron 300@450/384, one celeron 466/128 and \none AMD Thunderbird 800@880/256. \n\nI've compared KDE 2.2.2 with 3.0b2 (that's right; beta 2!) thoroughly, and the \n3.0b is MUCH faster in almost all aspects: change between desktops is\nvirtually instantaneous, there is no latency when moving objects, and selecting \ntext is virtually instantaneous. In 2.2.2, though, my results are the same as\nthe ones you describe. (Opening a new application is, in all cases, very slow\n(usually around 4 secs) but that seems to be inheritant in KDE - I surely hope it will \nget better).\n\nActually, my 466 celeron with 128 MB RAM running 3.0b2 (Matrox G200 8 MB)\nis MUCH snappier than my 300@450 w. 384 RAM (nVidia GForce 2 32 MB, factory \ndriver).\n\nThe OS in all cases is Mdk 8.2 final.\n"
    author: "G\u00f6ran Jartin"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-31
    body: "I also run Mandrake 8.2 final.\nApplications take at least 4/5 seconds to start, even small application like\nthe calculator!!! During this delay the harddisk is silent.\n\n(tested on AMD Athlon 1200Mhz/512MB RAM and AMD Athlon 800 Mhz/320 MB RAM)\nThese are fast machines on which I do not expect to wait for my calculator\nto pop up!!\n\nI also do have a Mandrake 8.0 installation on the 1200Mhz PC with an older\nKDE (2.1.x I think) which launches applications without noticable\ndelay (REALLY!!). \n\nThe launchtime of applications in the newer KDE's is irritating enough (for me)\nto keep me from doing 'production' work on the 8.2 Mandrake installations.\n\nI also noticed that GNOME 1.4 is almost as slow as KDE (if not slower) so\nmaybe there's something else causing these strange delays (newer kernels?, \nLatest XFREE?, Some new Mandrake 'features'?)\n \nIs there a SUSE 7.3 user out there experiencing the same startup delays?\n"
    author: "R.Zikken"
  - subject: "Re: KDE3 is too slow"
    date: 2002-03-31
    body: "I think you have a problem. There is no way that KDE should be THAT slow. \nI have checked repeatedly on smaller apps (systems as in my previous post).\nOn KDE 2.2.2, the calculator starts in ~1.5 seconds, less than 1 second on \nKDE 3.0b2. \n\nThis from a clean startup - a little bit longer with both StarOffice 5.2 and \nGimp (with one 1meg picture) running. (Of course, I wasn't really DOING anything \nat the moment, the other apps were just loaded.)\nMaybe a second or so longer time when I have been working for a few hours. \n\n\nSame with other small KDE apps like KJots - starts in 1 - 2 secs. KWrite starts \nin less than two seconds; noticeably faster on KDE 3.0, on the machine with \nless RAM and a much slower video card!\n\nI have run every version of Mdk since 7.0 and every verion of KDE since 1.1.x.\nI have never seen a backlash like the one you're talking about, so I guess that \nit's either an installation problem or a hardware problem."
    author: "G\u00f6ran Jartin"
  - subject: "Re: KDE3 is too slow"
    date: 2002-04-01
    body: ":: Is there a SUSE 7.3 user out there experiencing the same startup delays?\n\n   Not I.  Like others have said before, if you're experiencing this problem, it's a rather severe and rare or undocumented problem.  Try opening up konsole and run a few programs like \"ls\" and see if there is a delay - try a few times and see if the delay goes away after the frist time.  But really, you need to jump on a Mandrake mailing list for this kinda stuff - the dot really isn't a venue for tech support.  Good luck, though.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE3 is too slow"
    date: 2002-05-24
    body: "I am wondering how you can say that starting calculator is fast if it takes 1 1-5 s\nit takes more on my machine p4 1.3g, but however even 1s e an age for calculator,\ncompare it to windows"
    author: "tapak"
  - subject: "Re: KDE3 is too slow"
    date: 2002-08-03
    body: "i imagine that this news item was posted eons ago.. i found it in a google search.. i'll reply to it anyway..\n\ni used to have a problem with kde apps talking 4-5 seconds (or more) to load on my computer.. the harddisk was silent as in your case.. it just sat there doing nothing taking it's time, apparently.. actually, many non-kde apps were taking a lot longer than usual as well. when i booted my computer, sometimes mysql and apache would take up to 10 seconds or so apeice to load..   \n\ni lazy and put up with it for quite a long time (i hardly ever reboot the box and always keep lots of apps open anyway).. it turned out to be the ram speed setting in the bios.. i was using 133mhz ram but had it set at 100mhz in the bios.. so i set it to 133 and stuff started loading fast again (about twice as fast, at least.. perhaps faster)..\n\noh.. and after that i also recompiled kde with some sort of magical switch (--objprelink or something) that shaved about 33% of the load time off of kde apps.. the switch might be obsolete by now, though.. i think it was some hack to deal with slow c++ library loading issues with older versions of the ld linker or something.. not sure exactly.."
    author: "static"
  - subject: "Re: KDE3 is too slow"
    date: 2004-03-12
    body: "my computer is too slow"
    author: "duval"
  - subject: "KDE is too too slow I agree"
    date: 2003-05-13
    body: "I have an ECS desknote A928. It has P4 1.6Mhz, 256MB SDRAM running 333Mhz and a 40GB harddrive. I found it wierd this phoenomena. I have installed it for more than 10 times already and it was always slow. It takes like a minute or so just to log-in. But there was one time i installed it and it runs perfectly fast (But the was the one and only time it did). I wonder what the hell is wrong.. I tried installing it for another 20 times, selecting and unselecting some packages that I thought would have caused the delays, but i still cant find what's wrong. I hate it. Even the other GUI's the same.\n\nI dont think its the bios setting of RAM speed because I didnt changed anything in the bios during that one time when it run fast. I am still searching for the answer. I hope there are someone who can help/."
    author: "Karl Franco"
  - subject: "Re: KDE3 is too slow"
    date: 2004-08-21
    body: "I have exactly the same problem. I'm running kde 3.2.2 in a gentoo system with kernel 2.6.5.\nWhen i installed it for first time it worked very fast. But now it takes about 1 minute to finish loading everything. The only change i did was the use of the program \"prelink\" to speedup the applications loadtime. \u00bfCould it be the problem?."
    author: "Cristian"
  - subject: "Re: KDE3 is too slow"
    date: 2006-12-08
    body: "I am running KDE 3.5.5 P4 3.4 GHz. Kernel 2.6.17. \nI got exactly the same problem. After I first installed KDE\nit is fast. Then I installed some packages (gcc, g++, vim, etc)\nit became extremely when launching a program.\n\nI re-installed KDE and the same thing happened.\nI cannot figure out what is going on. "
    author: "Chunhua"
  - subject: "Re: KDE3 is too slow"
    date: 2002-07-18
    body: "Windows Calculator loads instantly on a pentium I 166mhz with 32mb of ram, so why does KDE Calculator takes 1.5 seconds to load?!\nKDE developers should stop messing around with the graphical crap, and start dealing with the most important subject => SPEED!!!\nCurrently, linux has nothing to offer to the home user, except for it's lower price. If you compare Linux to Windows 2000, You'll see that Windows 2000 is faster than Linux in every aspect, Windows Explorer loads instantly while Konqueror takes 4 seconds to load, It's outrageous to wait 4 seconds for a file manager to load. Windows Media Player plays DivX videos with much better performance then Linux media players, like MPlayer, Xine, or Noatun.\nUntil Linux won't take care of this speed problem, it has no chance to replace Windows as a desktop operating system."
    author: "albert"
  - subject: "Re: KDE3 is too slow"
    date: 2002-12-04
    body: "this is very true!!!!!!!!\n\ni got a p4 2gb, 256mb ddr, geforce4 64mb, 82.3gb hdd 40X lg cdrw and a pioneer dvd. under mandrake 9.0\n\none it doesn't like dvd's\ntwo yeah loads up mega fast but as soom as i open any one app turns sluggish\nthree the kde games/ toys make it worse\n"
    author: "feathersmgraw"
  - subject: "Re: KDE3 is too slow"
    date: 2003-02-09
    body: "I don't know whether your problem has anything to do with this (sorry, I'm a newbie), but it corrected it for me... All programs used to take at least 10-15 seconds to load, often up to a minute or two for the big ones like Konqueror. This made no sense to me as the system resource monitor said I was using only a small portion of my processor and I still had some unused RAM (I have an 1800 XP with 512MB DDR RAM).  It turned out that adding the hostname of my computer (jupiter) to the line in /etc/hosts totally fixed this problem:\n\n127.0.0.1\tlocalhost.localdomain\tlocalhost\tjupiter\n\nI have to admit, I don't see why it mattered. (I am also surprised Mandrake 9.0 didn't do this for me automatically.) I guess there must have been some process running in the background that had a high prioirty?  I hope someone who has this problem will come upon this message.  I thought for a while that KDE and GNOME just weren't as fast as Windows!"
    author: "steve"
  - subject: "Re: KDE3 is too slow"
    date: 2003-06-25
    body: "It worked!!\nmy /etc/hosts was something like this\n\n10.0.0.2 router\n127.0.0.1 localhost\n\nthe first was the address of my dsl modem.\nthe 10.0.0.2 was not reachebla and needed more\n\nI removed the first line and all is mutch quicker whithout to restart X!!\nIt seemes it was automatic reloaded, because I'm not restarting any process. (just wonder how)\nMaby there are some local tcp connections used from KDE and it takes some time to resolve the internal domain names if an address in this file is unreachable.\n\nMy new /etc/hosts is\n\n127.0.0.1 localhost.localdomain localhost europe\n\nOS Mandrake 9.1 KDE 3.1 all updates and patches related to KDE from Mandrake update\n\nHope this helps\nThanks for the idea steve!\nGeorgi"
    author: "Georgi"
  - subject: "Re: KDE3 is too slow"
    date: 2004-07-11
    body: "Hi,\n\nThanks guys. My kde (and gnome) was moving agonsingly slowly when I installed Suse 9.1 and couldn't see what to do.\n\nI'm using a dhcp that calls my pc something like pc-000097 which was not in the /etc/hosts file. I added it in (as an alias to localhost) and voila! Everything is running normally.\n\nNearly binned the distribution on the problem so thanks again.\n\nSome search terms I use in google that I used which might help pick this up if anyone else is having problems as it took me a while to find you (suse kde slow opening loading).\n\nCheers"
    author: "Moray"
  - subject: "Re: KDE3 is too slow"
    date: 2004-08-09
    body: "Hi, ive just installed suse 9.1 pro and its also a bit slow.\n\nMoray, could you expand a bit on what you did as Im not sure what youve done. Im using DHCP too. Im using a router, i set my hostname up during the install, is the hostname that dhcp uses/grabs different?\n\nCheers\nDan"
    author: "dan Reed"
  - subject: "Re: KDE3 is too slow"
    date: 2004-10-25
    body: "Oh man, what a difference!  I have had this problem in the past.  If your localhost points to a non existent hostname (or your hostname is not listed as a local hostname in /etc/hosts and it is not a real hostname), KDE just becomes unusable.  I am burning again now that I have made the change!"
    author: "Dan Allen"
  - subject: "Re: KDE3 is too slow"
    date: 2005-01-14
    body: "Fantastic. Your suggestion worked! \nI had just updated SuSE 8.2 pro with a free version of 9.1 from a magazine. Allmost everything had gone well after a rather slow update - I also did remote update via a DSL connection to ensure I'd got latest software. However, I found KDE was much slower than before, often taking mintes to open an app.\nI thought it was a disk problem at first, but having checked network settings and updated as you suggest, all is back to normal now."
    author: "John Bishop"
  - subject: "Re: KDE3 is too slow"
    date: 2003-03-30
    body: "I was having the same problem with windows taking as much as a minute to load, but after I changed the hosts file, it was like night and day things now pop up in less than 2-3 sec. \n\nTake care,\nPaul"
    author: "Paul"
  - subject: "Re: KDE3 is too slow"
    date: 2003-04-02
    body: "I've a similar problem - which I haven't resolved yet.\n\nOrginally, I didn't have sound working but everything loaded promptly. Once I got sound working, i got the problems that are described in general here (althought changing the hosts file doesn't fix anything)."
    author: "Peter Smith"
  - subject: "Re: KDE3 is too slow"
    date: 2002-05-30
    body: "Why don't you try turning some of the settings down??\n\nIf you put it on the same level as Win2k/XP it will be faster.\n\nI have the exact same config as you except for 512 Ram and no SCSI and it FLIES for me. (Debian package) Perhaps you need to spend a little more time configuring before complaint shold be warranted.\n\nTo compare Win2k/XP to KDE# is a bit of a stretch considering the true benefit lies in the O/S running behing the GUI. If you are stressed with KDE and you want blazing speed you should try a desktop manager like Blackbox or Ice.You will find these minimal in features but abundant in speed.\n\n At least with Linux you have a choice of GUI's  :)\n\ncheers\n\nPS I know this an old post but the above opinion bares relevance even today."
    author: "inch9"
  - subject: "Re: KDE3 is too slow"
    date: 2004-03-31
    body: "I put KD3 on a pentium.  4    3.6  533 mhz.  512ram. I am new to kd but the price was right. I have had all windows systems. And it seems to run as fast as xp.\n\n  Is  there any bench mark programs for linux? I would like to see what it looks like. \n\nI also would like to say if I went and bought all the software that this comes with and the xtras you get. On top of the operating system!!!! it would have cost me 1500 bucks.\n\n       I still have windows xp on the hard drive dual boot.But haven't booted to it since installing KD3\n               Keep up the good work who ever contributed to this great system.\n\n                      "
    author: "Jimmy"
  - subject: "my ibm preloaded  xp  is too alow"
    date: 2006-09-14
    body: "Hello\n\ni am shishir.\n\ni have a latest laptop, which has ibm preloaded software.\n\n\nits takes to much time in booting.\n\ni have reinstalled it by ibm factory state. but after two days it will get slower.\n\ntell me waht to do.\n\n\ni have scanned in with norton antivirus and Xoftspy Spy remomer tool\n\npls give me advice.\n\n\nthanks \n\nshishir\n9871349323"
    author: "shishir"
  - subject: "Re: KDE3 is too slow"
    date: 2003-01-07
    body: "Don't take it personally guys. It's taken me years to pluck up the courage to use linux fulltime, and now I am totally hooked. I'm using KDE3.0 from the SUSE 8.1 Distro, and although I love it, it is true. KDE is painfully slow. It's taught me patience and that is a good thing :)\nDon't scream at me or ignore this!!! I've tried turning settings down, I've tried pretty much everything (inc. ram settings etc.) but the problem is RELATIVE.\nObjectively speaking Windows (on a dual boot) is MUCH faster. Funnily enough, so is WindowMaker, so is IceWM, so is Gnome. I stick with KDE because I have become so accustomed to it, but it IS heavy running.This DOES represent a problem for the developers in the sense that it WILL make KDE look bad to windows users in terms of first impressions - which is a terrible pity. \n\n"
    author: "another reality check"
  - subject: "Re: KDE3 is too slow"
    date: 2006-02-11
    body: "Don't worry there is nothing wrong with your computer KDE, is slow.\nYou were saying you would need a fast PC computer I have 4 such computers\nand KDE, is slow on all of them. Out of frustration I keep on going back to Windows 2000 professional. I have used KDE, since its beginnings It is a lot of work to make such an interface and it seems so ungrateful to mention that it is slow but it is slow.\n\nI no longer have KDE, on my main computer I now have it on a P4VM800\nPC with a 1000 MB and the usual blah blah blah. The most frustrating thing you will come across are people telling you it runs perfectly on their computer people will do this because they treat computers like motor vehicles my car can do blah blah blah. If they had a piece of string and two baked bean tins they would tell you that KDE worked perfect on that. Such people are not worth the time of day. Don't worry about it there is nothing wrong with your computer KDE is slow. "
    author: "Philip Davidson"
  - subject: "."
    date: 2002-03-22
    body: "[Dickmode On]\nSpring actually started on the 20th this year.\n[Dickmode Off]"
    author: "cupoftea"
  - subject: "Re: ."
    date: 2002-04-01
    body: "In which country?"
    author: "turdle-wobbler"
  - subject: "Take it easy... :)"
    date: 2002-03-22
    body: "I understand you are excited about KDE3 release, but remember this is mainly a port to QT3. Yes, Konqi renders web pages much better, it is faster  (but Galeon, Opera, Mozilla, Netscape 6.2 are fast as well), and overall GUI has been improved.\nAnd please, words like 'truly magnificent', 'simply spectacular', 'I am in total awe', are maybe a little bit too much :) Koffice has not been ported yet (a lot of development work is expected here), edutainment module is in heavy development, a lot of people still complains about speed (even those who have Pentium III 600 MGz with 256 RAM) especially when starting applications etc. etc. Things are not ideal and things are far from 'spectacular'.\n\nRegards,\n\nantialias"
    author: "antialias"
  - subject: "Re: Take it easy... :)"
    date: 2002-03-22
    body: "> I understand you are excited about KDE3 release, but remember this is mainly a port to QT3.\n\nNot true, perhaps fifty to fifty port to new features (which are introduced now because of binary compability).\n\n> Koffice has not been ported yet (a lot of development work is expected here),\n\nNot true, see KOFFICE_1_1_BRANCH_KDE3 branch. Look on ftp.kde.org, SuSE provides KOffice 1.1.1 for KDE 3 rpms."
    author: "Someone"
  - subject: "Re: Take it easy... :)"
    date: 2002-03-22
    body: "If you can state your opinion about KOffice not being ported and KDE3 being slow on on a 600MHz machine with 256M RAM, both being false, why can't he state his opinion that KDE is good and fast?\n"
    author: "ac"
  - subject: "Re: Take it easy... :)"
    date: 2002-03-23
    body: "'Not ported yet' and 'not being ported' are two different things.\nI said lot of people complain about KDE being slow, they probably know what they are talking about. Personally, I think KDE is fast (not as fast as MS Windows on my machine), and I never said that he couldn't 'state his opinion that KDE is good and fast'. He used words like 'spectacular' etc. I don't see anything spectacular, sorry."
    author: "antialias"
  - subject: "Loading Instructions"
    date: 2002-03-22
    body: "I'd love to try KDE 3 out but do not have time at the moment to work through\nresolving depenencies and conflicts. If anyone has simple instructions for installing \non a freshly loaded RH 7.2 machine, I'd appreciate it.\n\nThanks\n\nBob"
    author: "BOBS"
  - subject: "Re: Loading Instructions"
    date: 2002-03-24
    body: "One word : debian\n\ndependency problem on debian ? run apt-get install dependancy ... dep solved"
    author: "Oelewapperke"
  - subject: "(almost) completely off-topic"
    date: 2002-03-23
    body: "I've been interested in python lately, and have heard 'round the net that zope is the \"killer app\" for python.  So, I started paying attention to what the sites I frequent run, and of my (short) list, the Dot is the only one.  It is also by *far* the one that is down the most.  Is this pure coincidence?  I never know if it is a zope error, some database thing, or something else entirely (hardware?), but the little admin message on the front page leads me to believe it is zope.  Well, anyways, the point of this is, is zope really this unstable (in which case I will run away, fast) and what are other peoples experiences with it?\n\nThanks, and have a nice day.\n\nP.S.  If this is deemed too offtopic, please feel free to email me."
    author: "David Bishop"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-23
    body: "I am not sure of how off-topic it is. And I am not sure about Zope. But I have been using Python for OO programming and I say it is beautifull. Python + KDE should be an ideal platform to quicly develop small size KDE apps. I have the feeling that KDE can benefit a lot by promoting the use of the python bindings. Why ?. Development is a snap in python. So, for a small app it could make sense to reduce the development time in a factor ... 3 or more, at the cost of a performance loss."
    author: "Leo"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-23
    body: "Well, I am truly liking python, and love the idea of zope (combine the power of python with a webserver!).  And, I've been poking a bit at PyQt, and finding it much more friendly than perl/gtk ever was.  However, I have to admit the dot is making me somewhat hesitant to trust any \"real\" website to zope.  Well, as I mentioned to someone in a private mail regarding this, I will simply have to install it on one of my machines and test it thoroughly for a few weeks, and then see how I feel about it.\n\nThanks for the reply, it was much appreciated.\n\nD.A.Bishop"
    author: "David Bishop"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-25
    body: "Hm. If I get it right, I have to buy QT-licences if I program with PyQT as I have to with each KDE-program I write, if I (or my company) don't want to give the sourcecode away or be completely uncommercial. For this reason I start trying wxWindows instead of QT but am not sure, if a wxWindows-application integrates well into KDE. Has someone experience with this problem? (To me it's a pity that such a great free-software product doesn't make use of free software as a GUI-Toolkit. QT cannot be seen as \"free\" because one company controls the development and it is not possible to make non-GPL applications without paying enormous fees [for a small shareware-developer])"
    author: "Michael"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-23
    body: "Haven't noticed zope.org being down much. Maybe if you look around here...\nhttp://www.zope.org/Resources/ZSP\n\nThere's a few places you can get free zope sites to play with also. Of course you can't really do any \"real\" development on them, as far as your own products or external methods, but they can be useful and instructive playgrounds.\n\nhttp://www.zopesite.com , http://www.freezope.org , http://www.nipltd.net , and of course http://www.zope.org itself."
    author: "Tim"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-23
    body: "(getting even more off-topic)\n\nDo you mean \"killer app\" as in: this app kills all confidence in python? I once caused a crash of zope on www.freezope.org by running a ballot-stuffing script on a poll that a friend of mine had set up there. Zope crashed after only 200 votes have been submitted. (absolutely no hostile intentions by me btw, after apologizing to the very friendly admin there everything was ok)\n\nYeah, I know that this incident is no proof for any suspicions about zope whatsoever, still I just had to tell you ;)\n\nWhat's maybe more relevant is that developing for the web with zope has not that much to do with python. The bigger part that has to be learnt will be the API's and zope way of doing things, not the language itself. Just the same that applies to doing web development with OpenACS and it's language - tcl.\n\n"
    author: "Olle"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-24
    body: " I've been working professionaly with Zope for 1.5 year now.\n\nI've worked on big portals, sites and all, and have written \nthings ranging from DTML scripts and ZPTemplates to ZClas-\nses and Python Products (all previous are Zope terminology).\n\nI found it to be extremely stable. I can't even think how a \npoll script could hang up Zope, even if it was a seriously \nbrain damaged one!\n\n As for \"killing all confidence in Python\", Guido (python's \ncreator) is working for Zope Corporation for a while now.\n\n So there!\n\nP.S regarding the \"dot.kde.org\" being down a lot, one\ncould do a search on the postings by the dot.kde.org admins, \nto see what the ACTUAL problems are. It would yield:\n\n------\n\n   Sorry guys, the dot went down again this weekend after \n   51 days of uptime on the new server. We suspect that \n   the problem is an overworked harddrive.\n\n------\n\n   The dot was down again this weekend as many of you \n   had noticed, and while I myself was blissfully enjoying \n   Halifax. This time it was the result of somebody exploiting \n   a security hole in our apache configuration and DoS'ing us \n   in the process.\n\n------\n\n   For what has seemed like an eternity, but really was closer to \n   a week, our beloved dot, as well as the kde.com sites, were down.\n   Now, thanks to the tireless efforts of Dre and MieTerra, the \n   server has been moved across the country and is again up and ready \n   to serve. Many thanks to all those who made generous offers to host \n   or help us in the meanwhile.\n\n------\n\n   As the Dot has been getting more and more successful, and more \n   and more popular, we've been attracting script kiddies and trolls \n   like flies. As Dre previously reported: \"On March 13, 2001, at 1:33 \n   am EST, someone using the anonymizer.com service succeeded in \n   putting malicious Javascript code into one of the posts. While the \n   code was relatively harmless -- it changed all links on the page to \n   point to a shockingly disgusting porno site -- we feel this security \n   lapse requires us to disable all posting until the problem in the \n   Squishdot code is solved.\" \n\n------\n\nSo in each and every case it has nothing to do with Zope being\nbuggy or anything.\n\n(Zope is also used in Gnotices, the Gnome equivalent of the Dot).\n"
    author: "BugPowder"
  - subject: "Re: (almost) completely off-topic"
    date: 2002-03-24
    body: "> I found it to be extremely stable. I can't even think how a\n> poll script could hang up Zope, even if it was a seriously\n> brain damaged one!\n\nWell, it was exactly as brain damaged as someone who clicks on form submit button and reload in the browser as fast as possible.\n\nBut it was propably a misconfiguration on the webserver that caused the crash, not Zope itself being unstable. I did not mean to imply that.\n\n\n> As for \"killing all confidence in Python\", Guido (python's\n> creator) is working for Zope Corporation for a while now.\n\nBut I guess this is some sort of sponsoring, like Alan Cox hacking the linux kernel while working for Red Hat? Or is Guido actually working on Zope?"
    author: "Olle"
  - subject: "QT runtime error"
    date: 2002-03-23
    body: "Hi. I compiled the recent rc from kde 3, but when it tries to run\nkicker it marks an assert error on file qgarray.cpp. I even tried to\nfollow the compiling advice in README.qt from the qt source (the snapshot\nthat came in the release). Could anyone help me. My distro is\nSlackware 8.0 with gcc-2.95 and glibc-2.1.5. Thanks."
    author: "Jes\u00fas Antonio S\u00e1nchez A."
  - subject: "Re: QT runtime error"
    date: 2002-03-25
    body: "> Could anyone help me. My distro is\n> Slackware 8.0 ...\nHere, with slackware8.0, glibc 2.2.5, gcc2.95.3\nI have no problem to build kde3rc3\nwith the following step:\n-> download qt3.0.2 from www.trolltech.com\n-> configure qt with\n./configure -shared -qt-gif -thread -sm -system-zlib -system-libpng -system-jpeg\n-> compile qt\n-> configure kde with\n./configure --prefix=/opt/kde3 --with-qt-dir=/path_to_qt_on_your_disk\n-> compile and install arts, kdelibs, kdebase, ...\n\nsome time ago I have done an installation from cvs (including kdesupport)\nand don't know if kdesupport have solve some unresolved dependencies ?\n\nin the hope this help you\n\nregards.\n\nthierry\n"
    author: "thil"
  - subject: "system requirment"
    date: 2002-03-23
    body: "What is the system requirment for KDE3 ?\n\nKDE2 is running on a Pentium1 processor and what about KDE3 ?\nMany people and enterprises have Pentium / PentiumMMX class machine."
    author: "cosmicflo"
  - subject: "Re: system requirment"
    date: 2002-03-24
    body: "The most important  system requirement is memory. 64Mb is about the minimum, but I would recommend at least 128Mb. \n\nKDE3 does not seem to have higher system requirements as KDE2, in fact KDE3 will probably run somewhat better than KDE2 on the same hardware.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: system requirment"
    date: 2002-03-29
    body: "I have tested KDE3 on my P180MMX/128Mo with no KDE effects :\nIt's slow.\nA windowmanager like Icewm is very faster (->better).\nPlease, optimise performances of KDE3 !"
    author: "cosmicflo"
  - subject: "I'm impressed"
    date: 2002-03-24
    body: "Sorry guys, I can't say anything bad about this new KDE3 release, other than possibily a little about a few 'inconsistencies' with Koffice, but I'm sure they will go away soon, also.\n\nI'm extremely impressed with KDE3.  I've been building the CVS releases for a while, and it amazes me at how stable the releases are - from one time to the next.  I personally like KDE3, and think its probably the first \"desktop GUI\" that respectably compares with Windows (ugh! did I say that word?).  A number of people that are running on other X-window systems have been suitably impressed when I roll in with my laptop, running KDE.  I have people who are running that other \"32-bit OS\" who have begun to ask me how they get the 'kool' graphics and things on their system.  You wouldn't believe how many people have never heard of 'gimp', or even Linux for that matter, and are amazed at what you can do without \"bro' Bill's\" influence in their machines.\n"
    author: "David Dudley"
  - subject: "KDE 3.0     INSTALLATION  SURVIVAL GUIDE"
    date: 2002-03-24
    body: "\nthank you everybody, for every effort of making KDE a reality,...  my problem always was i can't install the new  version of KDE i'm always using the KDE system comming with the Linux distribution ( Mandrake...:) my favorite..one).. when I try once to install a difference Version of KDE  , i have to download a different QT 3.0   and recompile it with some - XXXXX...  parameters....but never succeeedded..\n\nPlease...    HELP me for a Survival Guide of tunning/installing the QT 3.0... and KDE 3.0 RC .\n\nIT  will be Greatfull if  you helping in  a SHORT  Survival GUIDE for installing the KDE 3.0 rc3 and its prerequests...QT 3.0 library.....\n\nThanks"
    author: "KED LOVER"
  - subject: "Re: KDE 3.0     INSTALLATION  SURVIVAL GUIDE"
    date: 2002-03-26
    body: "women.kde.org ...go tutorial in the projects section..."
    author: "Patrick"
  - subject: "Re: KDE 3.0     INSTALLATION  SURVIVAL GUIDE"
    date: 2002-03-31
    body: "If you don't wish to be helpful, just be quiet instead!\n\nStephen"
    author: "stephen"
  - subject: "Debian Release"
    date: 2002-03-25
    body: "Can I get a release for Debian somewhere?"
    author: "Mikhail"
  - subject: "Once again the RH rpms fails to work for me"
    date: 2002-03-25
    body: "What to do.\nI prefer RH since I think their /etc/* and config files are the most sane ones around, but WHY are they so anti KDE?\nI am free to have my own opinion and for no particular reason, I hate the Gnome desktop. I dont know why, I dont care why, but it just makes me irritated.\n\nInstalling new KDE releases on RH has always been an \"adventure\", usually it resulted in reinstalling prehistoric X and KDE 0.000001 before it would start working again.\nThis time I thought, hey they have RH rpm's they link to, perhaps, perhaps, perhaps this time I can safely install it without having to reinstall RH from the CD after it has crashed and burned and fckd my entire disk this time?\n\nWell, it has improved, it just complains with about a million failed dependencies on crap that should be upgraded with rpm --upgrade *.rpm\nstool. \nI dont want to spend a day or two searching for crap before I can upgrade KDE.\n\nWhat distribution should I switch to to get it working reliably and that would work when I upgrade? KDE is more important to me than the distro.\n "
    author: "anonymous"
  - subject: "Re: Once again the RH rpms fails to work for me"
    date: 2002-03-25
    body: "I have SuSE 7.3 on one partition. downloading the rpm's and typing rpm -i *.rpm worked without any problem whatsoever. kde 3 started immediately, using my kde 2.2 settings, and I haven't encountered any problems after that. it's totally amazing :) although I would love to recommend Debian, they still haven't included kde 3 (which is, well,  totally incomprehensible)"
    author: "mark dufour"
  - subject: "Re: Once again the RH rpms fails to work for me"
    date: 2002-03-25
    body: "I use Debian unstable, and it works fine most of the time. A few times it fucked up my X config, but I always got it working again. You could also use a more stable debian version and get the latest KDE from some special apt-source.\n\nUsing APT (the debian package system) you can easily keep your system updated. You'll never have to spend a day downloading rpms again. Just select Special->APT:Debian->update and then ...->upgrade in KPackage and your distro is as good as new! Or just write \"apt-get install programX\" and you get that programX (including dependencies) that your friends were talking about...\n "
    author: "Johan"
  - subject: "www.iht.com"
    date: 2002-03-25
    body: "Am I the only one to get my desktop messed up with strange stripes in all programs after visiting International Herald Tribune - www.iht.com?\nI'm suspecting that something might be wrong with the way Konqueror handles Java but maybe it's unique to my istallation?\n\nPS\n\nThe real mess comes when you slect something from the dropdown menues!"
    author: "Sten Holmstr\u00f6m"
  - subject: "Re: www.iht.com"
    date: 2002-03-25
    body: "runs fine for me using a checkout from March 21."
    author: "eze"
  - subject: "HTML fails on one page! (Try it with mozilla)"
    date: 2002-03-25
    body: "WOW. This one really does exceed my expactaions based upon previous kde3 releases. Really fast and snappy. Konqueror snaps up in appx 1 sec, where it took upto 2 sec in kde 2.2.2. Als much faster than previous releases of KDE 3, where it was definately slower than kde222. \nOn small problem however. It might be a bug:\n\nThe page http://www.tweakers.net does not come through properly anymore in KDE 3 (all versions from beta 1 till now). It does in all other linux browsers weher javascript is enabled. maybe a small error..\n\nI have the error in redhat rpm's as well as the one I compiled frm source. For the rest this is all realy great!!!\n"
    author: "Rukhoven"
  - subject: "HTML fails on one page! (Try it with mozilla)"
    date: 2002-03-25
    body: "WOW. This one really does exceed my expactaions based upon previous kde3 releases. Really fast and snappy. Konqueror snaps up in appx 1 sec, where it took upto 2 sec in kde 2.2.2. Als much faster than previous releases of KDE 3, where it was definately slower than kde222. \nOn small problem however. It might be a bug:\n\nThe page http://www.tweakers.net does not come through properly anymore in KDE 3 (all versions from beta 1 till now). It does in all other linux browsers weher javascript is enabled. maybe a small error..\n\nI have the error in redhat rpm's as well as the one I compiled frm source. For the rest this is all realy great!!!\n"
    author: "Rukhoven"
  - subject: "compiling KDE"
    date: 2002-03-25
    body: "Hi there!\n\nI downloaded and compiled KDE beta1 and beta2(only libs) and saw that there is no libksycoca.so after compiling beta2. I can't believe that some of the libs were skipped or stuffed into one new library between two beta releases. This is also one reason why I can't compile kde-base-beta1 with kde-libs-bet2. Are they even source imcompatible?\n\nNIC"
    author: "Nicolaus Andratschke"
  - subject: "Re: compiling KDE"
    date: 2002-03-25
    body: "Maybe you should go for KDE RC3?\nIn my case it has much improved performance!!"
    author: "Rukhoven"
  - subject: "KMail Request"
    date: 2002-03-25
    body: "KDE3 look awsome.  Speed is great on my system P866.\n\nOne request, in KMail\n\nI would like to see a way to change to color of the highlighted lines in the main mail window.  I'm talking about how kmail highlights every other line.  I would like to change that from white to something else.\n\nIs there a better place to post these types of comments?\n\nOther than the above I love it!!!!!!!!!"
    author: "AC"
  - subject: "libkfile.so.3 Problems"
    date: 2002-03-25
    body: "Seems I'm not the only one having problems with libkfile.so.3.  It is nowere on my system.  Anyone know what redhat package it is supposed to be in.  Seems a lot of kde apps want this darn file."
    author: "Just-Me"
  - subject: "love the new konqueror.......but....."
    date: 2002-03-25
    body: "Flash seems to still not behave right under konqueror I was hoping this would be fixed on this version."
    author: "qgriffith"
  - subject: "Re: love the new konqueror.......but....."
    date: 2002-03-26
    body: "Same here...loads and starts to play the flash, then crashes.....\n\nShe seems a little faster though!!!"
    author: "AC"
  - subject: "KDE 3.0"
    date: 2002-03-26
    body: "I think KDE 3.0 is OLD looking!. Mac OSX's GUI is the standard now for Unix based GUI's. KDE 3.0 doesn't even come close. Give it up KDE!!. You will NEVER top Mac OSX in looks or functionality. Apple blows away KDE in everything. So much for Linux on the desktop and open source!!!. I'll use OSX or Windows XP!!. THE MODERN DAY OPERATING SYSTEMS!!.   "
    author: "Bob "
  - subject: "Re: KDE 3.0"
    date: 2002-03-26
    body: "http://www.kde-look.org/\n"
    author: "ac"
  - subject: "Re: KDE 3.0"
    date: 2002-03-26
    body: "Tux sayeth:\n \nFeed not the Beast of Redmond nor it's minion trolls.\nFor they are an Abomination."
    author: "linwins"
  - subject: "Re: KDE 3.0"
    date: 2002-03-26
    body: "Amen... Besides, let's not forget where those guys from redmond and the fruit lovers got their inspiration.. They didn't invent it themselves, you know :o)"
    author: "Shoikan"
  - subject: "Re: KDE 3.0"
    date: 2002-03-26
    body: "I wonder why you even bother and come to the site, dedicated to such a shitty desktop, and tell us you'll never use it..."
    author: "Bojan"
  - subject: "Re: KDE 3.0"
    date: 2002-03-29
    body: "Good for you. Now be quiet."
    author: "Peace Loving Satisfied KDE User"
  - subject: "Re: KDE 3.0"
    date: 2002-08-04
    body: "I totally respect your opinion and value your input to this thread. Now sit down and shut up.\n\nTHx\n-Leenix"
    author: "Leenix"
  - subject: "Re: KDE 3.0"
    date: 2002-12-24
    body: "I am a Mac user!  And I like KDE!  So does my Mom, we both find it very attractive and useful.  And a nice augmentation to OS X via a rootless X11 and fink.  I like some of the programs in Gnome but the desktop....not so much.  \n\nAs a Mac user since '78 I extend MY apology for the chucklehead's comments.\n\n\nI like the Aqua gui but honestly I wish I could get rid of it on an occassion and at least have a choice of the look OS X had when it first came out as a server.  I liked that one.  "
    author: "chris c"
  - subject: "Crashes :-("
    date: 2002-03-26
    body: "Looks wonderful, but kcminit crashes on startup, and then ksplash crashes too. At least it loads, and seems to be working. Maybe it's just something I broke on my system due to too many experiments. I used objprelink, --enable-fast-malloc=full in kdelibs, and --enable-final. Anybody else has problems with that? I will try to submit a bug report later."
    author: "Vadim"
  - subject: "Re: Crashes :-("
    date: 2002-03-26
    body: "> I used objprelink, --enable-fast-malloc=full in kdelibs, and --enable-final.\n\nYou use too much experimental and unsupported stuff to complain."
    author: "Anonymous"
  - subject: "Re: Crashes :-("
    date: 2002-03-26
    body: "Umm, if the stuff it's there I guess it's supposed to be used, right? --enable-final never caused me problems with previous versions, and objprelink seems quite widely used too. Maybe it's the malloc one, but I checked the URL in the malloc.c file and the latest source is already in KDE.\n\nI think complaining is exactly what I have to do, those are nice features I'd like to be stable."
    author: "Vadim"
  - subject: "Re: Crashes :-("
    date: 2002-03-26
    body: "The Splash screen crashes with the SuSE 7.3 rpms too.\nSound isn't working either."
    author: "Sagdalon"
  - subject: "printing"
    date: 2002-03-26
    body: "Anyone else not able to print ever since the upgraded to KDE3.0?  I printed right before I upgraded now I can't at all and all the errors are just can't talk to to the printer.  (yes i checked the printer lol)  Gnome won't print either so it may not be a problem unless somehow my cups is hosed."
    author: "qgriffith"
  - subject: "Re: printing"
    date: 2002-03-31
    body: "well, maybe it's a bit stupid, but have you checked the status of CUPS?? Maybe it's not running...."
    author: "ZIOLele"
  - subject: "KDE 3.0RC3 on Red Hat: Prepare to Fall, period."
    date: 2002-03-26
    body: "\nso i've just finished installing 3.0rc3 on RH7.2 and kicker (the kde panel) and kmail are crashing beautifully.\n\nhere's what kicker spits up:\n\n(no debugging symbols found)...(no debugging symbols found)...\n(no debugging symbols found)...[New Thread 1024 (LWP 3105)]\n0x41153ca9 in __wait4 ()\n   from /lib/i686/libc.so.6\n#0  0x41153ca9 in __wait4 () from /lib/i686/libc.so.6\n#1  0x411cf6b4 in __DTOR_END__ () from /lib/i686/libc.so.6\n#2  0x40ff26f3 in waitpid (pid=3106, stat_loc=0x0, options=0)\n    at wrapsyscall.c:172\n#3  0x40787382 in KCrash::defaultCrashHandler () from /usr/lib/libkdecore.so.4\n#4  0x40ff0a85 in pthread_sighandler (signo=11, ctx=\n      {gs = 7, __gsh = 0, fs = 0, __fsh = 0, es = 43, __esh = 0, ds = 43, __dsh = 0, edi = 134826064, esi = 0, ebp = 3221216376, esp = 3221216320, ebx = 1097371116, edx = 3221216336, ecx = 0, eax = 45, trapno = 14, err = 4, eip = 1096930442, cs = 35, __csh = 0, eflags = 66054, esp_at_signal = 3221216320, ss = 43, __ssh = 0, fpstate = 0xbfffd9c0, oldmask = 2147483648, cr2 = 49}) at signals.c:97\n#5  <signal handler called>\n#6  0x4161d48a in KPanelApplet::KPanelApplet () from /usr/lib/libkdeui.so.3\n#7  0x4146c984 in KMiniPager::KMiniPager ()\n   from /usr/lib/libkminipagerapplet.so.1\n#8  0x4146bad6 in init () from /usr/lib/libkminipagerapplet.so.1\n#9  0x400dd6ff in PluginLoader::loadApplet () from /usr/lib/libkickermain.so.1\n#10 0x4007496e in InternalAppletContainer::InternalAppletContainer ()\n   from /usr/lib/kicker.so\n#11 0x4007cd9b in PluginManager::createAppletContainer ()\n   from /usr/lib/kicker.so\n#12 0x400672a3 in ContainerArea::loadContainerConfig () from /usr/lib/kicker.so\n#13 0x40063379 in ContainerArea::initialize () from /usr/lib/kicker.so\n#14 0x40061ac5 in Panel::initialize () from /usr/lib/kicker.so\n#15 0x40060e4a in Kicker::Kicker () from /usr/lib/kicker.so\n#16 0x4005ff3a in main () from /usr/lib/kicker.so\n#17 0x410b5627 in __libc_start_main (main=0x804851c <main>, argc=1, \n    ubp_av=0xbfffe934, init=0x80484e4 <_init>, fini=0x8048690 <_fini>, \n    rtld_fini=0x4000dcc4 <_dl_fini>, stack_end=0xbfffe92c)\n    at ../sysdeps/generic/libc-start.c:129\n"
    author: "fabio"
  - subject: "Re: KDE 3.0RC3 on Red Hat: Prepare to Fall, period"
    date: 2002-05-05
    body: "I have  --exactly--  the same problem :o(\nAt first I have installed only kdelibs ans kdebase and everything seemed to start and run Ok,\nLatter on I have installed all of the other packages from KDE3.0 and BOOM ... the kicker crashes ... Any clue? I am running the same environment as Fabio.\n\nThanks!\nJerome"
    author: "Jerome"
  - subject: "Source diffs"
    date: 2002-03-26
    body: "People are usually crying out for source diffs by now, but in case anyone's interested, the diffs between 2.2.2 and 3.0rc3 are almost as large as the 3.0rc3 tars, and they don't include the images. So if you wanted diffs, you might as well just download the tars - and be prepared to wait if you're on a slow link. It's worth it (IMHO)\n\n-- \nHywel Mallett"
    author: "Hywel Mallett"
  - subject: "Some display problems in Konqueror"
    date: 2002-03-29
    body: "I found that, 2 of the most visited hungarian site doesn't work correctly with Konqueror.\n\n1. www.freemail.hu, the most popular hungarian freemail system. It has about 500.000 registered users. It is not possible to attach a file, when you write an email. Tha Java Script window pop up, and show the upload process, but after the file is uploaded to freemail.hu, the progressbar stops, and the javascript window remain displayed forever, instead of disappearing....\n\n2. www.fn.hu, a very popular hungarian economy newspaper. This page is fragmented in Konqueror, and the selection menu is completely broken, located at the top of the page....\n\n3. I never could not get started Java. Neither nor Sun Java 1.3.1  or Kaffe. I'm sad a little bit.\n\nAnother problem for me: the folder tree in KMAIL. I don't  want Inbox, Sent, Drafts etc. to be at the top level of the tree! I need a \"Local Mail\" or similar at the top level -- just like in Mozilla Mail --, and Inbox, Sent etc. must be subfolders in that.\n\nIt is very embarassing for me, that my IMAP folders are at the same level, then Inbox, Sent, etc....\n\nI think, Mozilla Mail has a clear conception with the folder tree, KMail should follow it.... Now Kmail folder tree is as stupid, as Outlook Express's.\n\nFinally: KSCD and Noatun should be integrated. In this case, KDE will have a feature rich media player, like XMMS.\n\nNow XMMS is the winner :("
    author: "N\u00c9METH Bal\u00e1zs"
  - subject: "the $64000 question is:"
    date: 2002-03-31
    body: "can you copy text to the konsole yet?\n\n"
    author: "symbiote"
  - subject: "Re: the $64000 question is:"
    date: 2002-03-31
    body: "Couldn't you paste text into kosole with your middle mouse-button in KDE2?\nI could, as I can do also in KDE 3...."
    author: "Sebastian Greiner"
  - subject: "Re: the $64000 question is:"
    date: 2002-03-31
    body: "Couldn't you paste text into kosole with your middle mouse-button in KDE2?\nI could, as I can do also in KDE 3...."
    author: "Sebastian Greiner"
---
The <a href="http://www.kde.org/">KDE Project</a> has announced the release
of KDE 3.0RC3 (on this, the first day of Spring).  The release comes a month after the release of KDE 3.0beta2, with
two (semi-public) release candidates, as well as a <a href="http://dot.kde.org/1016258226/">well-attended 1-week hacking session</a> for fixing problems, appearing in the interim.  KDE 3.0rc3 can be downloaded through KDE's <a href="http://download.kde.org/unstable/kde-3.0rc3/">load-balancing system</a> (<a href="http://download.kde.org/unstable/kde-3.0rc3/src/">source code</a>, <a href="http://download.kde.org/unstable/kde-3.0rc3/RedHat/">RedHat packages</a> (only partially uploaded at this time), and <a href="http://download.kde.org/unstable/kde-3.0rc3/SuSE/">SuSE packages</a>).  Additional packages should become available in the coming days, both on KDE's and some distributions' servers. Other than for show-stopper fixes, this release candidate will be released
early next month as KDE 3.0.  Personal impression:  love at first sight,
<strong><em>it simply rocks</em></strong>!









<!--break-->
<p /><p>
As much as I hate to admit it, with my
busy schedule I have not been able to keep up the KDE 3 releases.  But yesterday morning I installed KDE 3.0rc3 and, to be honest, it is truly magnificent!
<a href="http://www.konqueror.org/">Konqueror</a> is fast, fast, fast!
Never seen anything like it (except maybe Lynx) in
the main browsers - even
long pages in my <em>Most Often Visited</em> list all but instantly popped into
place.</p>  
<p>
The rest of KDE 3 is simply spectacular, too.  Everything is snappier,
from menus (despite the addition of cool menu icons) and dialogs (these pop up <em>much</em> faster) to applications, and the look is even more professional than KDE 2.  Wowwww, I am in total awe.  Superb, excellent, amazing job, guys, KDE 3 absolutely rules!
</p>
<p>
Due to the imminence of the 3.0 release, for this release candidate only, please post crashes and <em>grave</em> bug reports directly to <a href="mailto:kde-devel@kde.org">kde-devel@kde.org</a> rather than using the conventional <a href="http://bugs.kde.org/">bug reporting system</a>.  Let's see who of you can make KDE 3.0 even better by finding any elusive shot-stopper bugs before the end of the week! 
</p>

