---
title: "KDE-CVS-Digest for December 20, 2002"
date:    2002-12-23
authors:
  - "dkite"
slug:    kde-cvs-digest-december-20-2002
comments:
  - subject: "Holy Cow!!!"
    date: 2002-12-23
    body: "What amazes me is that there is this much to talk about, week after week.  Good on ya, KDE devs!"
    author: "David Bishop"
  - subject: "Re: Holy Cow!!!"
    date: 2002-12-23
    body: "630 commits since thursday evening when I did the last digest. If you look on lists.kde.org at the kde-cvs list, you will see roughly 1500 or so a week.\n\nHoly cow indeed.\n\nDerek"
    author: "Derek Kite"
  - subject: "Great job again"
    date: 2002-12-23
    body: "This weekly digest is a joy to read, Derek.  Stuff like this is just as important as the implementation of code and bug fixes.  Kudos for your consistency at keeping this project going!"
    author: "Caleb Tennis"
  - subject: "Kate?"
    date: 2002-12-23
    body: "I don't know why Kate have been ignored in the CVS digest lately.\n\nDuring the last week, roughly, we have\n\n\n- Implemented Bug 33583: show vertical line at columns the user chooses (http://bugs.kde.org/show_bug.cgi?id=33583)\n- Implemented Bug 50530: ability to lock file, so no changes are allowed (http://bugs.kde.org/show_bug.cgi?id=50530)\n- Implemented Bug 34098: when saving files no backups are created. (http://bugs.kde.org/show_bug.cgi?id=34098)\n- Added wrap symbols on dynamically wrapped lines\n- Fixed bracket matching to work with folded regions\n- Fixed gotoline, find oa to work with folded regions\n- Added a first menu command to utilize code folding (collapse all toplevel regions)\n- Gotten syntax hl importing/merging to work (basically, utilizing this follows shortly)\n- Gotten started on a projects framework\n\n\n... and probably much more\n\n-anders"
    author: "Anders"
  - subject: "Re: Kate?"
    date: 2002-12-23
    body: "Anders:\n\nThe code folding and bracket matching will be in next week. Cutoff last week was sometime thursday local time.\n\nI thought I got the stuff on locking and backups. Hmm. Sorry. I guess I looked at the commit logs and it didn't click.\n\nBTW, Kate is coming along very well. I use it for my projects and scripts and like it very much. \n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Kate?"
    date: 2002-12-24
    body: "Wow! (Seriously)\n\nThanks for the good work.\n"
    author: "Anonymous"
  - subject: "Re: Kate?"
    date: 2002-12-25
    body: "What kind of scripting does Kate have nowadays? I'd like to to a lot of scripting for my editing needs, and Emacs *can* do it just fine, but I've never really liked Elisp that much. I've heard that Kate can use Python for scripting, is that true? Well, anything that is procedural and not Perl (too gory for a scripting language) is good for me. :)\n\nI've looked for an alternative editor for a long time, but so far nothing compares to the power of Emacs in the hands of an experienced user. But I could grow to like Kate if it had good scripting and Emacs keybindings (not just C-a, C-e but *extensive* Emacs keybindings). I actually think Kate is a project I'd like to contribute to, but it's a bit overwhelming to get into the process.\n"
    author: "Chakie"
  - subject: "Re: Kate?"
    date: 2002-12-26
    body: "There is no real scripting in kate itself atm, there was plans to add it (along with the whole kdescripting aproach in kdelibs), but we have no real working macro recording or scripting interface atm, I guess.\n\nIf you want to help out, just contact me (cullmann@kde.org) ;) Help is always wanted\nand thankfully accepted ;P\n\ncu\nChristoph\n\nP.S.\nMerry Christmas ;)"
    author: "Christoph"
  - subject: "Re: Kate?"
    date: 2002-12-26
    body: "Cullman where you asleep?\nKate scripting works, its just we are missing script engines.  So far in KDE nonbeta we have a perl engine, and im still finishing up the python one.\n\nSo far there is only a bash shell script one, but that was just to get things started.\n\nCheers\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Kate?"
    date: 2002-12-26
    body: "I know that there is some infrastructure behind, but until now now real \n\"usable\" scripting for the normal users (e.g. with macro recorder or any kind\nof helper to create your own scripts)\nThat you can (somehow) install somewhere scripts that will be picked up by the app menus is not that perfect for the normal guys ;)\n\nYour kscript work is nice, but we need some more stuff to get it visible for the end user.\n\ncu\nChristoph"
    author: "Christoph"
  - subject: "Re: Kate?"
    date: 2002-12-26
    body: "I have been looking into methods of recording, the big problem im running into are figureing out that a menu action maps to a dcop event.  The QAccessable classes offer some hope, but I really have give some more thought on the subject.\n\nCheers\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "Re: Kate?"
    date: 2002-12-26
    body: "Personally I'd be happy to be able to write small textual \"modules\" or scripts that are saved in, say, <tt>~/.kde/share/apps/kate/scripts/</tt> and picked up by Kate either automatically or through some command. Kate could also run some predefined script file at startup (like <tt>.emacs</tt>) and allow me to bind a script to a certain key combination. I don't really need macro recording, just a way to write/code scripts that do stuff for me, and which can optionally access user input. The scripts should also be allowed to access the buffer list and the buffer contents. Some kind of standard library of common text manipulation functions would also be nice to have, along with some other stuff. Hmm, ideally I'd like to script with Python, thus getting the full power of the Python standard library to operate on Kate buffers. Of course PyKDE could be integrated so that the scripts can pop up simple dialogs (parsed from <tt>.ui</tt> files) to ask the user simple queries. And so on and so on ad nauseam goes the featuritis...\n\nDon't get me wrong, I think Kate is great, and *no* editor so far does what I want.\n\nWell, I'm an old Emacs user, and I like it the Emacs way, if it wasn't for Elisp. :) These are just things that would be fun to have in Kate.\n\nI don't really know too much about the infrastructure behind KDE scripting, but I've so far really seen no KDE applications use any kind of scripting, so is there any scripting engine or similar in KDE? \n\nI have a few things I'd like to do for Kate, so it's a shame that KDE development is so darn complex, even for someone who has used Qt for years. Every little thing is automagic in some way, and you can't just \"add some code\" to anything without knowing the whole glorious KDE infrastructure with all the magic bells, whistles and horns. Ok, this is just my personal pet dislike about KDE development. I've tried for years to get into it, but failed every time. There is just too much that you have to know for even the simplest application or fix. :( I don't care about DCOP or some magic config files that somehow get parsed, not magic menus or magic toolbars. Not magic themes nor magic directories with magic resources nor thousands of magic shared libraries all over the place. And docs for KDE3 are pretty rare.\n\nOh well...\n"
    author: "Chakie"
  - subject: "Re: Kate?"
    date: 2002-12-27
    body: "I am curious as to what things you would like scriptable in kate? What scripts do you use in emacs that you would like in kate?\n\nI haven't looked at kate alot, but one of the things I really liked about delphi was the ability to set up small 'scripts' that ran when you typed a sequence of letters and a control sequence. I used to set delphi up so that if I typed 'fori<ctrl-shift-a>' it would setup a for loop with an i variable as the counter for me, and set the prompt after the 'to' part. I would love something like this for c++\nfori<ctrl-shift-a> results in\nfor ( i = i; i <put cursor here>; i++)\n{\n\n}\n\nLittle things like this would be nice :)  \n\nWhat types of things do you have emacs doing that you would like to see kate doing? What elisp hacks would you like 'ported'.\n\n\nTom h."
    author: "tom"
  - subject: "Re: Kate?"
    date: 2002-12-27
    body: "I'd like to be able to create functions for common boring things. Like create a method in a class. The function should ask for the name, params, return value etc, and then create the method in the .hh file and add a stub to the .cpp file along with Doxygen comment headers etc. Or create a new class. Or insert some structures for XML. Anything really. I've done a lot of that in Elisp, and it's not fun to maintain and even get to work at all. \n\nBut, you don't really need scripting until you're a power user that \"lives\" in your editor for hours each day.\n\nAnd not just \"hacks\". I want to be able to do full programming in a scripting language. Maybe something like Python or Qt Script for Applications would be nice.\n"
    author: "Chakie"
  - subject: "Re: Kate?"
    date: 2002-12-28
    body: "It sounds like you are looking for similar things as I am.\n\nI too have similar elisp code that I have gathered from across the internet to do similar things. Unfortunately, I don't grok elisp well, so I can't alter or maintain them. I would love to see something like qtscript or python in there for doing similar stuff.\n\nOther things that come to mind are database operations. ie: building classes or structures from a database. Something like this could be coded in python fairly easily.\n\nI wonder what it would take to do something like this? I haven't looked at the kate code yet. Maybe it is time to check it out.\n"
    author: "tom"
  - subject: "Re: Kate?"
    date: 2002-12-29
    body: "I think you need a framework for managing and running scripts within Kate (interpreter and script manager), a way for Kate to expose internal structures (from C++) to the scripting language (buffers, settings, project browser) and internal functions (load/save/create buffers, manage menus, keybindings etc). I think some of these things can be done quite automated, but some need a lot of work.\n\nOf course, these are the thoughts of a naive user, and may not apply at all."
    author: "Chakie"
  - subject: "Re: Kate?"
    date: 2002-12-29
    body: "You'll be able to do this using KJSEmbed thanks to its KParts::Plugin. The code will be released to CVS this week.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Kate?"
    date: 2002-12-30
    body: "GREAT! I will be taking a look.\n\nThanks Richard"
    author: "tom"
  - subject: "konqueror preloading"
    date: 2002-12-28
    body: "konqueror preloading works great ! congrats !\n(konqui starts in less than 1 second now here instead of 3-4)\n"
    author: "ik"
---
A <a href="http://members.shaw.ca/dkite/dec20.html">new edition</a> of KDE-CVS-Digest is out.  This week we cover updates on the security audit, <A HREF="http://kroupware.kde.org">Kroupware</A> issues, bugfixes and lots of new features in <A HREF="http://www.kdevelop.org/">KDevelop</A>, <A HREF="http://www.konqueror.org/">Konqueror</A>, <A HREF="http://printing.kde.org/">KDEPrint</A>, <A HREF="http://edu.kde.org/kig/">Kig</A> (a program for exploring geometrical constructions), as well as <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/proklam/arch.txt?rev=1.1&content-type=text/x-cvsweb-markup">Proklam</a> updates (see <a href="http://www.schmi-dt.de/kmouth/index.en.html">KMouth</a> also) as a further step towards improving <A HREF="http://accessibility.kde.org/">accessibility</A> capabilities in KDE.  Enjoy!
<!--break-->
