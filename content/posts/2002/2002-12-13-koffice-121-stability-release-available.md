---
title: "KOffice 1.2.1: Stability Release Available"
date:    2002-12-13
authors:
  - "Dre"
slug:    koffice-121-stability-release-available
comments:
  - subject: "What is the status of Kivio"
    date: 2002-12-13
    body: "I noticed there has not been much development on Kivio. I checked out the demo version of Kivio mp and it looks very usable, but it does not integrate at all with KDE, which would make me not consider to buy it...\n\nIs there anyone working on the free version of Kivio or is this project pretty much dead?\n\nRegards,\nStephan\n\nPS: Compiling KOffice CVS now (hopefully KSpread will finally compile)."
    author: "Stephan Richter"
  - subject: "Re: What is the status of Kivio"
    date: 2002-12-13
    body: "It's unfortunate that Kivio has stalled as part of KOffice. KivioMP, on the other hand, seems promising (I have it & use it), but still has a *long* way to go to becoming anywhere near as powerful as Visio. When the Kivio source was donated to KOffice, there was some conflict over who was going to be responsible for maintaining it (or at least make it not break against KOffice libs), and the ball was pretty much dropped then and there. Stencils aside, OpenOffice.org has much more powerful diagramming capabilities. We'll see where KivioMP goes (hopefully when they address my bug reports/feature requests)...it just goes to show that it's quite difficult to adopt code."
    author: "Eron Lloyd"
  - subject: "Re: What is the status of Kivio"
    date: 2002-12-13
    body: "Actually there is no \"conflict\". The reality is more that there is nobody who volunteered to work on the GPL version of kivio, the one that's in KOffice.\n\nIt's not that difficult to adopt code, it simply needs a maintainer :)\nAnyone?"
    author: "David Faure"
  - subject: "Great, thank you all"
    date: 2002-12-13
    body: "This is great. KOffice is now very usable for 'simple' homeuser tasks, personally I use Kword for writeing letters, job applications and that sort of thing, and KSpread to keep track of my home economy. \n\nIt's still not usable for professional work (i.e. big cooperations), but it is definitivly getting there. \n\nAnd it still lacks good templates (But don't worry, I don't know how to program, but think I'll manage to make some templates.. maby even good enough to be included in future releases)\n\nA Big Thank You to all who made it"
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Why is it not usuable for professional work? Just for my interest..."
    author: "F@lk"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "> Why is it not usuable for professional work? Just for my interest...\n\nbecause it can not write m$office file formats. sad but true... :-(\n\n\nIf KOffice wants to gain interoperability and flexibility, it should\nuse the same file formats than OpenOffice, as default, and not cook\nit's own soup which works only in koffice and nowhere else.\n\n(This has been reported as bug/wishlist already)"
    author: "yglodt"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Sure - I agree,\n\nKOffice should use existing file-formats. KWord should use OpenOffice-Writer formats and Krita should use XCF (the GIMP-format) as default-format because everyone is using GIMP with Linux (so you can choose which application you want to use with your XCF-file) and this GIMP-format is tested very well.\n\nSo far\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Check out the mailing-list archive as there are quite discussions on these standard-office-document-format topics. So far, volunteers don't show up yet."
    author: "ariya"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Cos KWord barfs at almost any MS Word doc that uses more than just plain text. Given that I have zero chance of convincing Windows addicted co-workers (with whom I have to share documents) to use Linux let alone KWord, I have little use for KWord in my corporate environment.\nWhich is a shame really as I rather like it - much snappier than OpenOffice (which incidentally also has refuses to open about half the MS Word docs I receive)."
    author: "Graham O'Connor"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Next major version will sport a fresh new MS Word filter. In addition, you might want to file a bug with your offending MS Word docs at http://bugs.kde.org in order to help the developers."
    author: "ariya"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Di you mean the latest wv2? I compiled wv2 and KWord the other day and at least some stuff works fine now. I guess I should do some more testing to help you guys out.\n\nRegards,\nStephan"
    author: "Stephan Richter"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Yup, wv2. Should you like to help, then please report any kind of bugs with the filters, such as DOC files it can't open and so on."
    author: "ariya"
  - subject: "Re: Great, thank you all"
    date: 2002-12-13
    body: "Er, but it is. Since all the documents that I release outside of the organisation are PDFs - which KDE can generate very easily - it doesn't matter what I use internally."
    author: "AccUser"
  - subject: "Re: Great, thank you all"
    date: 2002-12-14
    body: "It takes almost an infinte time to do stuff.  For example, try making a series with 10000 elements & compare with other spreadsheets.\nNeil.\n"
    author: "Neil Koozer"
  - subject: "Re: Great, thank you all"
    date: 2002-12-14
    body: "Please add this to the Bug Tracking system, as there is not detailed description available yet.\n\nWe focus to solve the bugs users are describing, so as long as you don't mention them, they will not be fixed with high priority.\n\nPhilipp"
    author: "Philipp"
  - subject: "Re: Great, thank you all"
    date: 2002-12-15
    body: "Each time a new version comes out, I do a few checks, and every time so far it seemed far too non-functional for any bug reports to be of value.  Here are some timing results (on an AMD XP 1800+):\n\nDelete one number from a single cell:   1.5 minutes\n    (in a column of 32767 numbers)\nSort one column of 32767 numbers:       10 minutes\n\nNeil.\n\n"
    author: "Neil Koozer"
  - subject: "Re: Great, thank you all"
    date: 2002-12-14
    body: "I use it for printing invoices for my clients. There are lots of things, that don't work as expected (at least in 1.2, I'm downloading 1.2.1 right now). When I print cells, they don't print exactly as I wrote it. It's not WYSIWYG. For example when I have one cell, that's close to right margin, it prints well (i.e. all of it is printed), but I get also one other page, when there's just right end of these right margin cells. The printing and WYSIWYG in Kspread sucks (but it was greatly improved in Kword).<p>\nWhat I don't like about office is, that there's no good format to exchange data with MS world. How do I send spreadsheet to MS using guy, when I want him to modify it? (I send invoices as PDF files, so there's no problem, there's no need to modify them, but when I want someone to make changes and send it back, there's no way).<p>\nWhat I would really love is some common document format for spreadsheets and text documents with images, tables and stuff. OpenOffice.org formats look reasonable. If anyone made import and export filters for open formats for MS Office, man -- that would rock!"
    author: "Juraj Bednar"
  - subject: "Re: Great, thank you all"
    date: 2002-12-15
    body: "So you volunteer?\nI bet the developer want that features, too. But no volunteer no code...\n\nBTW: KSpread has no WYSIWYG. This is something for the 1.3 release.\nAnd what about filing bug reports at least..."
    author: "Peter"
  - subject: "Re: Great, thank you all"
    date: 2002-12-15
    body: "If I could write windows code, I would start the project, but I'm not that skilled. It was just an idea, I didn't say, that it has to be done, but it would be great, if someone with ability to code this finds this idea useful....<p>\nOn the other side -- do you know any other way to exchange documents with MS-* people? HTML? (they need to change the data)."
    author: "Juraj Bednar"
  - subject: "Re: Great, thank you all"
    date: 2002-12-16
    body: "RTF format. I think KWord can read and write it and RTF is used by MS Word 2000 to simulate Word 95 and Word 97 files."
    author: "Peter"
  - subject: "Re: Great, thank you all"
    date: 2002-12-16
    body: "HTML is good for this. I meant for spreadsheet document exchange. What to use instead of XLS?"
    author: "Juraj Bednar"
  - subject: "Re: Great, but not for professional work?"
    date: 2002-12-18
    body: "I have put people who use MS Office every day. Not the usual people\nwho use know about 20% of the functions and use 5% of them, but\npeople who know about 99% of MS Office features and use 60% of them.\n\nAfter 10 minutes, they complain 'feature X doesn't work correct' or 'feature Y is missing'. And I have found alot of things to complain about myself. But this is not the place to report feature request.. (DO give feature requests and DO complain, and if you know how, DO contribute...)   (-:\n\nKOffice will be a replacement for MS Office, but it's just not there yet (And OpenOffice is no better..). "
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Great! - but..."
    date: 2002-12-14
    body: "Koffice is great for most things, and I'd rather use it than Openoffice.\n\nBut:\nWith small fonts (size 4) I can't sqeeze the lines close enough together.\nThe margins aren't read from the printing system (cups) - shouldn't they be?\n"
    author: "Niels"
  - subject: "KOffice Development Seems Slow"
    date: 2002-12-14
    body: "It seems that KDE is moving MUCH faster then KOffice.  I assume there just aren't enough developers?  I'm surprised that companies like Mandrake and SuSe aren't focusing more on KOffice.  A fast, elegant, and (well) integrated office suite would be a great selling point in the battle against Redhat.  I just can't believe that Openoffice is the future."
    author: "Rimmer"
  - subject: "Re: KOffice Development Seems Slow"
    date: 2002-12-14
    body: "Check the HEAD branch."
    author: "anon"
  - subject: "Re: KOffice Development Seems Slow"
    date: 2002-12-14
    body: "Mandrake employs two KOffice developers (soon only one farther)."
    author: "Anonymous"
  - subject: "Re: KOffice Development Seems Slow"
    date: 2002-12-16
    body: "What do you mean by \"(soon only one farther)\"?"
    author: "anon"
  - subject: "Re: KOffice Development Seems Slow"
    date: 2002-12-16
    body: "Only one will be employed continuously."
    author: "Anonymous"
  - subject: "Re: KOffice Development Seems Slow"
    date: 2002-12-18
    body: "Read for yourself: http://www.fosdem.org/index/interviews/interviews_faure"
    author: "Anonymous"
  - subject: "UPDATE: RedHat and Mandrake RPMs"
    date: 2002-12-16
    body: "More binary packages are available now, check out the announcement again for more links.\nThe RedHat-8.0 packages have been generously provided by Andreas Pour.\nAnd thanks to Laurent Montel for the Mandrake-9.0 packages, too :)"
    author: "David Faure"
  - subject: "Re: UPDATE: RedHat and Mandrake RPMs"
    date: 2003-01-27
    body: "Can some 1 please tell me the exact website to update my verion 7.1 of red hat to 8.0"
    author: "EMIN3M"
  - subject: "UPDATE: RedHat and Mandrake RPMs"
    date: 2003-05-31
    body: "if there exists any update for Redhat 9 or Mandrake 9.0 i wanna be implied??"
    author: "Bulent"
---
The <a href="http://www.kde.org/">KDE Project</a> today
<a href="http://www.koffice.org/announcements/announce-1.2.1.phtml">announced</a>
the release of KOffice 1.2.1.
KOffice 1.2.1 is a stability and enhancement release, with the principal improvements
over KOffice 1.2, released last September, occurring in the spreadsheet
program (<a href="http://www.koffice.org/kspread/">KSpread</a>).
A <a href="http://www.koffice.org/announcements/changelog-1.2.1.phtml">list
of changes</a> and <a href="http://www.koffice.org/releases/">notes about
the release</a> are available at the KOffice
<a href="http://www.koffice.org/">web site</a>.  More binary packages
should be available tomorrow.  Enjoy!
<!--break-->
