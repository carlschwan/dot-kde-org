---
title: "Kernel Cousin KDE #31 Is Out"
date:    2002-01-24
authors:
  - "Someone"
slug:    kernel-cousin-kde-31-out
comments:
  - subject: "Note from the Kalzium author"
    date: 2002-01-24
    body: "The Kalzium link in the topic doesn't work right now (though the images do). I'm posting on behalf of author, Carsten Niehaus <cniehaus@gmx.de> (whose browser isn't working), who states that, \"For some reason my admin disabled the php_mod for apache. I've already written him that he should reactivate it. I hope it is fixed ASAP.\" For now, please see http://kt.zork.net/kde/kde20020118_31.html#1 . Sorry for any inconvenience."
    author: "Triskelios"
  - subject: "Icon Tearoffs"
    date: 2002-01-24
    body: "The new icons with the tearoff look are nice. But I fear that they'll be to 'busy', especially done on 32x32 icons."
    author: "David Johnson"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-24
    body: "They should use <A href=\"http://www.kde-look.org/content/show.php?content=602&PHPSESSID=74b6c198db6c81b6296e4f353071aaff\">iKons 0.5x.</a>\nThey look much cleaner.\n"
    author: "MW"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-25
    body: "Uh, am I really the only one who thinks the default icons *rewl*?  How strange.  These icons are just so professional and no-nonsense.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-25
    body: "You're not the only one.  I think it is just the people who don't like the default icons who comment.  All the people who like them don't say anything usually.  I like them, but I never get involved in these discussions about them."
    author: "not me"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-30
    body: "The default icons are beautiful. And with KDE3 and Qt3 and alpha-blending\nthey are unbeatable. I hate icons which you can't distinguish from each\nother. In KDE the meaning of each icon is clearly visible, and that's what they're for. AND they're nice, too.\n\n:-)\n\nTim"
    author: "Tim"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-25
    body: "Default icons are default icons. Basta. I don't want to discuss quality of iKons here, but quality of our default iKons has been proved. Any other icon themes are welcome if they are: 1. not rip off, 2. clean and elegant, 3. consistent 4. finished in all sizes. iKons don't match any of these four requirements.\nAnd, please, stop talking about what KDE should use as default icon theme if you don't know what you are talking about."
    author: "anonymous"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-25
    body: "Oh, anonymous@kde.org ;_)"
    author: "as"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-25
    body: ":-) Could you post _with_ e-mail-address next time if you feel so much responsible for our icons? Actually there are a lot of icons in IKons which I'd like to see in 'hicolor' (which is the default-icontheme). \nStill you are very much correct about the four requirements you made for the inclusion of icons in kdelibs.\n\nGreetings,\nTackat-who-was-still-dancing-salsa-with-a-beautiful-girl-at-12:24-AM\n\nKDE-artist-team"
    author: "Tackat"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-24
    body: "it does look a little nicer, but i don't get the motivation of the redesign. why are there tearoffs? (on one side only?)"
    author: "me"
  - subject: "Re: Icon Tearoffs"
    date: 2002-01-24
    body: "Basically, because it looks a little nicer, and because it distinguishes the file icon a bit more (makes it contrast more with, say, device or directory icons)."
    author: "Carbon"
  - subject: "tackat@kde.org"
    date: 2002-01-25
    body: "There are two reasons why I added them:\n\n- because it looks a bit nicer (actually I did a survey among 30 people who had to choose between grey, blue and skin-orangebrown colored mimetype-papers and between with and w/o tearoffs.The version currently chosen was the one people liked best.\n\n- it makes it easy to distinguish files which are stored in ASCII-format (with tearoff) from files which are stored in binary-format (w/o tearoffs)\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: tackat@kde.org"
    date: 2002-01-25
    body: "damn, tackat, you really seem to have put some thoughts into this ;)\n\nThanks for all your great work! Just remember, there's no other who has succeeded in making icons that fit all of the above requirements. Logical conclusion is, you r0x0r the most!"
    author: "me"
  - subject: "Tearoff on the left side instead?"
    date: 2002-01-25
    body: "Hi!\n\nHow about placing the tear offs on the left side of the icons instead on\nthe right side? I'd guess that'd look even better, but that's MHO only.\n\nFor 32x32-sized or smaller icons one should consider not to add the tear offs.\nOther icons have a higher level of detail for lager sizes as well.\n\nGreetinx,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "back to the speed issue...."
    date: 2002-01-25
    body: "http://www.infoworld.com/articles/hn/xml/02/01/24/020124hnborland.xml\nLooks like Borland is going to release C++ sometime soon (well, depends on what you understand by soon, sure). Maybe their C++ compiler, if they do a Open Edition of it, can help solving the problems of g++ and KDE speed?"
    author: "Iuri Fiedoruk"
  - subject: "Re: back to the speed issue...."
    date: 2002-01-26
    body: "http://www.open-mag.com/754088105111.htm\n\nThis one is interesting too! I know it's not a kde problem this bussiness of compilers once they are not the ones that compiles KDE, BUT if KDE can compiles with better compilers and KDE team asks for packagers I'm sure they would make use of better compilers. :)"
    author: "Iuri Fiedoruk"
  - subject: "Re: back to the speed issue...."
    date: 2002-01-31
    body: "Hmmm... I'm not sure what to make of that article. Since my research involves writing lots of numerical code, speed is a critical issue for me. Excited by the open-mag article I tried out the Intel C++ compiler, and frankly the results are rather disappointing. Code compiled with g++ 3.0.3 seems to consistently run about twice as fast. The test CPU is a Pentium III (800MHz). Perhaps, the fact that all of my computations are done with doubles rather than floats may be cramping the optimization possibilities (e.g. vectorization) for the Intel C++ compiler, but the fact remains that (for me) g++ 3.x seems to be a better choice for now. I would be interested in comparing the performance on a Pentium IV machine, but I do not have access to one yet. All in all, because of my experience, I'm not sure if KDE would benefit from Intel C++.\n\nJust my two cents.\n\nYR"
    author: "YR"
  - subject: "Shadows"
    date: 2002-01-31
    body: "just to say that the orientation of the light is not the same for all the icons, look at the textfile icon and the dir icon, the shadows have different orientations.\nMaybe it should be corrected (maybe I haven't understood anything of the discussion).\nThanks"
    author: "a"
---
<a href="mailto:aseigo@olympusproject.org">Aaron J. Seigo</a> and <a href="mailto:valiant@web.de">Juergen Appel</a> deliver again with <a href="http://kt.zork.net/kde/kde20020118_31.html">Kernel Cousin KDE #31</a>. This week's summary includes talk about chemistry application <a href="http://www.cip.biologie.uni-osnabrueck.de/niehaus/linux/kalzium.php">Kalzium</a> (<a href="http://www.cip.biologie.uni-osnabrueck.de/niehaus/linux/shots/kalzium1.png">1</a>, <a href="http://www.cip.biologie.uni-osnabrueck.de/niehaus/linux/shots/kalzium2.png">2</a>)  as a candidate for <A href="http://edu.kde.org">kdeedu</a>, KDE artists' discussions (have a <A href="http://devel-home.kde.org/~tackat/status1.png">peek</a> at the new tear-offs on text icons), word wrapping in dialogs, <a href="http://www.carillonis.com/kdiradm">KDirAdm</a> - an LDAP administration tool, animated icon support in KDE 3.0, and more.
<!--break-->
