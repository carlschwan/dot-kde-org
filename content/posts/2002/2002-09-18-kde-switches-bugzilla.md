---
title: "KDE Switches To Bugzilla"
date:    2002-09-18
authors:
  - "d"
slug:    kde-switches-bugzilla
comments:
  - subject: "Nice"
    date: 2002-09-18
    body: "I read somewhere a mail about how GNOME switched to Bugzilla, but it seemed like they had some more problems with getting it to work. Seems KDE had an easier time migrating to Bugzilla.\n\nI can't really comment on the merits of Bugzilla vs. \"the old system\", but it has to be A Good Thing(tm) as so many high profile projects use it?"
    author: "Chakie"
  - subject: "Re: Nice"
    date: 2002-09-18
    body: "Gnome helped KDE by providing the conversion scripts they have used so that KDE didn't have to start at the beginning."
    author: "Anonymous"
  - subject: "Re: Nice"
    date: 2002-09-21
    body: "I am pleased to see some cooperation between the 2 projects..."
    author: "TunaBomber"
  - subject: "Re: Nice"
    date: 2002-09-18
    body: "I haven't used any Bugzilla setups besides Mozilla's -- but I found it far, far easier to report and respond to bugs in the old KDE system than for Mozilla. There were a zillion options, mostly incomprehensible to anyone not familiar with the code.\n\nThe new KDE system seems much simpler, though, so I guess my objections were with the way Mozilla had configured the system rather than with the system itself.\n"
    author: "Otter"
  - subject: "KBugBuster"
    date: 2002-09-18
    body: "Is KBugBuster working with the new system again?"
    author: "Peter"
  - subject: "Re: KBugBuster"
    date: 2002-09-18
    body: "Not yet, but there is work in progress to make it a universal Bugzilla [offline] client."
    author: "Anonymous"
  - subject: "KDE should use some of the Gnome bugzilla reports"
    date: 2002-09-18
    body: "hi,\n\nThe Gnome bugzilla has a number of useful bugzilla reports, some of which I am the author of.\n\nThe KDE team might find it helpful using these on their bugzilla.\n\nThe reports are at:\nhttp://bugzilla.gnome.org/reports.cgi\n\nThe source code of the cgi is at:\nhttp://cvs.gnome.org/lxr/source/bugzilla/\n\nthanks,\nWayne Schuller"
    author: "Wayne Schuller"
  - subject: "Re: KDE should use some of the Gnome bugzilla repo"
    date: 2002-09-18
    body: "Wow, very nice. "
    author: "me"
  - subject: "Re: KDE should use some of the Gnome bugzilla repo"
    date: 2002-09-18
    body: "Cool stuff indeed. Would like to see that on KDE Bugzilla as well. =)"
    author: "Datschge"
  - subject: "Re: KDE should use some of the Gnome bugzilla repo"
    date: 2002-09-19
    body: "Note to self: Funny how people can cooperate here and some posts below there is still a troll trying to start a flame war. =P"
    author: "Datschge"
  - subject: "Re: KDE should use some of the Gnome bugzilla reports"
    date: 2002-09-18
    body: "Thanks, I took the weekly summary and changed it a bit for KDE:\nhttp://bugs.kde.org/weekly-bug-summary.cgi"
    author: "Daniel Naber"
  - subject: "Re: KDE should use some of the Gnome bugzilla reports"
    date: 2002-09-18
    body: "Can you please add a parameter that let you choose how many top entries are shown? And something like the output table of product-target-report.cgi would be fine only for products (Bug Count by Component and Severity)."
    author: "Anonymous"
  - subject: "Great work"
    date: 2002-09-18
    body: "At last fast and accurate bug queries !\nExcellent duplicates handling !\nCC on bugs !!\n\nI love it :)\nThanks to Stephan and all other people who made it possible...\n\nTwo questions I have :\n- Isn't the three letters limit a bit high ? How would one check for \"CSS\" in khtml for instance ? Wouldn't it be possible to use stop words instead, in order to allow acronym searches ?\n\n- Is the infamous whineatnews.pl daemon running ? :-D\n\nG."
    author: "germain"
  - subject: "Re: Great work"
    date: 2002-09-18
    body: "> Isn't the three letters limit a bit high ?\n\nYes, but we'd need to recompile MySQL and re-setup the tables to change it. I think this can wait - just submit a wishlist bug report :-)"
    author: "Daniel Naber"
  - subject: "KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Bugzilla is the GNOME bug reporting tool ! It's been created by Mozilla, these GTK servants ! Why can't our coders come with something of their own ?\nI don't want to report bugs with this GNOME tool !"
    author: "KingOfDesKtop"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "File a bug report about that :-)"
    author: "Daniel Naber"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-21
    body: "hehehehe... What an appropriate response!"
    author: "Dawit A."
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Mozilla is totally independent from Gnome.\nMozilla (and Netscape) don't use GTK, they use their own APIs.\nGnome is using Bugzilla for submiting bugs, so what?!\n"
    author: "Ricardo Cruz"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Actually not sure about that... when I to try start the Mozilla 1.1 binary in a fresh environment without GTK, it complains about the GTK library not being present.."
    author: "Julian Rockey"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "You can compile mozilla to use QT instead of GTK, one of the big advantages of having an abstract toolkit.\n\nIf you're interested, here are the recommended configure options for Qt Mozilla:\n\n--without-gtk\n--with-qt\n--enable-toolkit=qt\n--disable-tests (required because Qt Mozilla does not include some extra\nwidgets used by the tests, but not Mozilla itself)\n\nYou can also use:\n--with-qtdir=<path_to_QTDIR>\notherwise $QTDIR is used.\n\n--\nGF\n"
    author: "GF"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Mozilla is competely independent from any graphical toolkits. On X11, it can use xaw, gtk, qt, as a backend. It just happens that you are using a binary compiled with gtk (which is standard on X11). It's not really a gtk app, however. It doesn't even use gtk's event loop. It does use gdk for drawing."
    author: "fault"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Uhm, nice troll. Bugzilla is probably the best free bug reporting/collecting software to exist.\n\nIt's not tied to GNOME at all, but\nbesides, what's wrong with using GNOME tools?\n\nOther things in KDE use gnome-derived things.. anyways, why the hatred of GNOME? It's completely misplaced. Hate Microsoft if you want to hate someone. "
    author: "fault"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Ummm.... I think he was just making a joke!"
    author: "Jeff"
  - subject: "I hope so...."
    date: 2002-09-18
    body: "KDE and GNOME should work together, instead of fighting. Only then we make a difference in user-friendly desktops against Micosoft. \n\nAnd by the way: flame-trowing against Bill Gates is much more fun!"
    author: "Maarten Rommerts"
  - subject: "Re: I hope so...."
    date: 2002-09-19
    body: "\nMake merge, not war.\n\n"
    author: "NetPig"
  - subject: "Re: I hope so...."
    date: 2002-09-19
    body: "But, WTF is GNOME adopting such alot non-desktop libraries, even GIMP is seen as a GNOME application by lots of users, but it isn't.\nI am talking about libraries like libxml2, libxst. They're really backend libraries, wich has n0thing to do with GNOME. But they're told to be GNOME libraries. \nAnd, what about mc (Midnight Commander)? This is a console application, a Norton Commander clone. Why is this part of GNOME? GNOME just took the right to adopt mc to be its initial standard file manager. aha. nice. a console filemanager for the desktop as standard gui FM ;)\n\nAnd, of course, I do not see BugZilla as a GNOME application. It has a different homepage, it has a different CVS repository, and I really can't connect it to GNOME anyway. So BugZilla is not part of GNOME.\n\nWell, it's not that I don't like GNOME. I like it. It's good to have concurrents (to KDE, etc). But what I really don't like is its history, why it has been created, and how it adopts all the gtk based apps and other independant libaraies and console apps.\n\nCheers,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: I hope so...."
    date: 2002-09-19
    body: "> But, WTF is GNOME adopting such alot non-desktop libraries, even GIMP is seen as a GNOME application by lots of users, but it isn't.\nI am talking about libraries like libxml2, libxst. They're really backend libraries, wich has n0thing to do with GNOME. But they're told to be GNOME libraries.\n\nWell, these libraries were developed for GNOME in the first place. Other apps saw that they were useful and use them too. KDE does this too.. for example DCOP. It's a infastructure library, not a desktop library. \n\n> And, what about mc (Midnight Commander)? This is a console application, a Norton Commander clone. Why is this part of GNOME? GNOME just took the right to adopt mc to be its initial standard file manager. aha. nice. a console filemanager for the desktop as standard gui FM ;)\n\nWell, Miguel De Icaza wrote mc, so I guess he has the right to do it. This is the concept of copyright. :P\n\n> And, of course, I do not see BugZilla as a GNOME application. It has a different homepage, it has a different CVS repository, and I really can't connect it to GNOME anyway. So BugZilla is not part of GNOME.\n\nBugZilla was developed for usage in MoZilla. Notice why BugZilla has the \"Zilla\" part in it yet? ;0 BugZilla is as tied to GNOME as it is to KDE.\n\n> Well, it's not that I don't like GNOME. I like it. It's good to have concurrents (to KDE, etc). But what I really don't like is its history, why it has been created,\n\nI agree. I don't think GNOME should have ever been created. People should have worked on Harmony instead ;) But of course, I don't think GNOME is going to die any time soon either.\n\n> and how it adopts all the gtk based apps and other independant libaraies and console apps.\n\nThat's called code reuse. It has been done in KDE too."
    author: "fault"
  - subject: "Re: I hope so...."
    date: 2002-09-19
    body: "> Well, these libraries were developed for GNOME in the first place. Other apps saw that they were useful and use them too. KDE does this too.. for example DCOP. It's a infastructure library, not a desktop library. \n\nYes, except that libxml was initially created out of GNOME, it came to GNOME later. But anyway, as you mentioned it, I would really like to see DCOP standalone as well. Why? Because I think DCOP is more that just for the D (Desktop) and it could then be used in other applications as well (non GUI based, e.g. a daemon). I would like to use DCOP in my applications, but depending on it would even produce ugly dependencies, lets say you develop a server using DCOP, this would require to have kdelibs installed on the server as well, even if you'll never have a desktop there.\n\n> Well, Miguel De Icaza wrote mc, so I guess he has the right to do it. This is the concept of copyright. :P\n\nYou're right ;)\n\n> BugZilla was developed for usage in MoZilla. Notice why BugZilla has the \"Zilla\" part in it yet? ;0 BugZilla is as tied to GNOME as it is to KDE.\n\nExactly. I just kept this in mind at time of my last writing because I read this in any post before :-P\n\nCheers,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-18
    body: "Why are you such a wanker?  \n\nInstead of being so knee-jerk in your reactions, why don't you try thinking before you start spouting off?  \n\nBugzilla is not a Gnome tool, Gnome just uses the tool to help report/track/fix bugs.  Even if it were a gnome tool, who cares?  Again, why are you such a wanker?"
    author: "Pete"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-30
    body: "He's joking, for god's sake!"
    author: "Klaus Mertelz"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-19
    body: "Are you somehow implying that GNOME is The Evil Enemy(tm)?\n\n\n\nWhy?"
    author: "Stof"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2002-09-19
    body: "> Why?\n\nBecause he's a troll, and trolls don't need any real reasons.\n\nOr he might be a zealot.. which KDE and GNOME need, but of course, they shouldn't go around bashing the other desktop. Perhaps bashing the real Evil Enemy(tm)."
    author: "fault"
  - subject: "Re: KDE switches to GNOME ??!?"
    date: 2003-01-10
    body: "\"The real enemy\" isn't any specific desktop environment.\n\"The real enemy\" is apathy, hypocrisy, and idiocy.\n\n\"The competitor\" might be a closer attribute; but still,\nI think we should recognize the fact that KDE and Gnome\nboth share very vital, even if meaningless to some,\nlibraries. And in that same respect, help one another\ncohesively when a developer from one team or the other\nfinds a bug in one of these libraries, and thus takes\nactive steps to see that the bug is remedied properly.\n\nAgain, I ask; aren't we more likely, as developers of\nopen source, to help one another than we are to be enemies\nto one another?"
    author: "Sta"
  - subject: "will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-18
    body: "I wonder if all reported bugs will be fixed BEFORE the 3.1 release"
    author: "frederic "
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-18
    body: "In which decade do you want 3.1 to be released?"
    author: "Anonymous"
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-18
    body: "I prefer waiting for a stable KDE than using a buggy one. I agree that a minor bug can wait, but at some point, they have to be fixed, otherwise, KDE will end up having thousands of bugs. Where I work, we do not release software with critical, high and medium known bugs, otherwise, customers won't a penny for it.    "
    author: "frederic "
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-18
    body: "> I prefer waiting for a stable KDE than using a buggy one.\n\nNobody force you to use a version, which you consider as unstable.\n\n> otherwise, KDE will end up having thousands of bugs.\n\nKDE has thousands of bugs just now. Quick, dump the version you use!"
    author: "Anonymous"
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-19
    body: "Even M$ Windows has tons of bugs, they're just unknown or hidden. GNOME has bugs, everything has. The question is, how critical are they. I'm working with KDE's CVS HEAD version and it's pretty stable, so: even a desktop having such alot of bugs can be stable anyway ;)\n\nGreets,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-20
    body: "So your company only releases bug-free code? Sounds like you're living in Never-Never Land.  No software is \"free from bugs\". If you weigh the advantages of releasing code that isn't terribly bug free with the advantage that if you release a \"buggy\" version you might find MORE bugs than all your in-house testing would find in an almost infinite time.  You are limited to what hardware you have in-house as opposed to a \"cheap\"  (I mean a wide variety of machines/hardware that you haven't paid for) supply of different environments that could prove that your \"bug-free\" program is not what you think it is.\n\nAnyone who says that their program is bug-free or even using your words \"critical,high and medium known bugs\" is either a fool or an idiot. \nWarning \n<FLAME BAIT>\nWhich one does you or your company fall into?\n</FLAME BAIT>\n \nSometimes it might be advantagous to release \"buggy software\" in order to get a wider environment for testing purposes or even feedback about how some similar problem was solved or a work a round was discovered that the developers were confident that couldn't happen.\n\n"
    author: "Scott Manson"
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-20
    body: "Read carefully before you say my colleagues and I are a bunch of fool or idiot, so I repeat again : the company do not release software with KNOWN bugs, obviously, there are some hidden bugs !!! Customers have access to our bug tracking system (TestTrack), so one can't lie about the number of KNOWN bugs.\n  "
    author: "frederic "
  - subject: "Re: will all bugs be fixed for the 3.1 release ?"
    date: 2002-09-21
    body: "It's a constant cycle.  Developers fix bugs.  Users report them.  All of the bugs will never be closed unless users stop reporting them for a few months (which would be bad).  \n\nIt's for all practical purposes impossible to not have open bugs with a huge user base and a public code repository.  What if 20 bugs are reported on the day of the release, wait?  Until when?  What if during that time new bugs are introduced into CVS and reported?  What then?\n\nThe release schedule is the only way around this.  We try to go in phases:  add features several months before the release and try to fix things in the months before the release.  \n\nYour previous statments seem to indicate that you're a programmer; start digging through the (KDE) bugs database and fixing things then!  If you want to see KDE released with as few bugs as possible, then start helping!\n\nBugs don't get magically fixed.  They take time to sort through and fix.  \n\nAnd just in case you're not satified, don't worry we won't don't expect you to pay a penny for it.  ;-)"
    author: "Scott Wheeler"
  - subject: "neato"
    date: 2002-09-18
    body: "Wow, the way KDE has Bugzilla configured is much better than the way Mozilla has it configured.  Mozilla's Bugzilla is far too cluttered and clunky to use.  KDE's is much cleaner and nicer.  The inclusion of QuickSearch on the front page is especially nice, Bugzilla's query form needs some major usability work.  I would only suggest making the choices \"Search Bugzilla\" and \"A much more complex search\" instead of the other way around :-)\n\nThe KDE bugs team has been doing a great job.  The original bug reporting system was quite good, it just didn't have the feature set that bugzilla has.  Now it seems we have the best of both worlds!"
    author: "not me"
  - subject: "Re: neato"
    date: 2002-09-19
    body: "The only reason Bugzilla is seen by the masses to be So Much Better than debbugs is because KDE was using an age-old version of Bugzilla (about 3 years old IIRC), and certain people refused offers of help to upgrade it, and implement systems like the PTS, that would completely match (and, IMHO, surpass) Bugzilla. All I can say that the options weren't properly considered; it's like comparing a 1991 Lancer to an SS Commodore, whereas if you used an up-to-date Evo, you'd have a chance against the Commodore ..."
    author: "Daniel Stone"
  - subject: "Re: neato"
    date: 2002-09-19
    body: "I think the reason Bugzilla is seen by the masses to be So Much Better than debbugs is that the masses have never heard of debbugs.  I certainly hadn't until you mentioned it just now, even though apparently that's what KDE was using for some time.  In contrast, now you can't visit a page at bugs.kde.org without reading the word \"Bugzilla\" at least three times.  Maybe you need to look into ways of getting better branding for debbugs :-)\n\nP.S.  I assume you meant \"KDE was using an age-old version of _debbugs_,\" not Bugzilla..."
    author: "not me"
  - subject: "Re: neato"
    date: 2002-09-19
    body: "The biggest problem was that things didn't happen in real time with ddebugs (send mail, wait for answer, pages regenerated the day after...). With bugzilla it's all immediate, thanks to the SQL backend.\nHas this changed in ddebugs?"
    author: "David Faure"
  - subject: "Vote!"
    date: 2002-09-19
    body: "It's your duty to vote to help identify the most annoying bugs for KDE 3.1 and most requested features which then may be implemented first for KDE 3.2 or higher. See http://makeashorterlink.com/?A574317D1 for the current highest voted bugs and wishes."
    author: "Anton"
  - subject: "AC, where are you?"
    date: 2002-09-19
    body: "[sarcasm]\nAC, it's time to cut & paste your \"Bugzilla is not userfriendly\" and \"you suck\"-posts again!\n\nAC? AC! Where are you?\n[/sarcasm]\n\n\nBut really... who cares wether KDE uses Bugzilla or not? Just because it's created by the Mozilla project and is used by GNOME doesn't make it The Ultimate Evil(tm)."
    author: "Foo"
  - subject: "I leave KDE."
    date: 2002-09-20
    body: "Amen, KDE is doomed. In the past I've seen to much GNOME shit moving to KDE. It's time for me switching over to GNOME completely then. Goodbye people it was really nice with you but seems that GNOME is starting to dominating everywhere. I can't belive this, it started 1 year after KDE and now KDE people is using their code all overwhere.\n\n- Xfree CVS has a shitload of GNOME stuff now (libxml2, pkgconfig, xcoursors, fontconfig, xft2) all done by GNOME people such as Daniel Vaillant, Keith Packard, Havoc Pennington.\n- KDE is using GNOME libraries now, like libxml2, libxslt, libart_lgpl and librsvg\n- Now KDE uses that shitty Bugzilla which is a nightmare to use. Which requires me to create another fucking account only to post some fucking bugs. As if I don't have enough accounts already.\n\nI don't understand why KDE is starting to suck so much. I'm totally disapointed."
    author: "Martin Behrens"
  - subject: "Funny."
    date: 2002-09-20
    body: "Moving to GNOME because it's shit? And where are the people complaining that every wheel gets reinvented again and again?"
    author: "Datschge"
  - subject: "Re: I leave KDE."
    date: 2002-09-20
    body: "> GNOME people such as Daniel Vaillant, Keith Packard, Havoc Pennington.\n\nKeith Packard isn't a gnome \"person\" exactly. He's a XFree86 \"person\". \n\nI'm not sure who Daniel Vaillant is, but if you mean Daniel Veillard, the author of libxml(2), he has nothing to do with libxml(2) being in xfreecvs. It was imported by Keith Packard as a dependancy to pkg/fontconfig. It's a damn good XML library. It's probably the best free/open one. It's a lot better than expat, at least. \n\n> KDE is using GNOME libraries now, like libxml2, libxslt, libart_lgpl and librsvg\n\n> Now KDE uses that shitty Bugzilla which is a nightmare to use. \n\nOh god. Once again, Bugzilla has nothing to do with GNOME! GNOME uses it. That's all. KDE is in the same position with Bugzilla as GNOME is. GNOME used debbugs until several years ago (late 2000 I beleive) like KDE did until very recently. \n\nBugzilla is tied to Mozilla, not anything else. Again, Bugzilla is one of the best free/open bug reporting/collecting packages available. It's definatly the most customizable. I agree that it has usablility problems in the default configurations, but it is very configurable. Usability is as good as the interfaces to bugzilla made. \n\n> KDE is using GNOME libraries now, like libxml2, libxslt, libart_lgpl and librsvg\n\nBecause perhaps it saves in programming time? The libraries you mentioned are not tied architecturally in any way to GNOME, and they are all great libraries. Why shouldn't KDE use them?\n\nI think you have a bad case of the \"Not Invented Here\" (NIH) Syndrome."
    author: "fault"
  - subject: "Re: I leave KDE."
    date: 2002-09-20
    body: "Come on man! Don't be so foolish! It is a good case if GNOME and KDE start working together, instead of only looking inside our own minds. Please remeber that the true power of Linux (and all open-source) is collaboration and sharing of code. \n\nIf KDE and GNOME want to develop some stuff (like libarys) together, then we can same time, improve quality, improve intergration and make a stand against fragmention. And by tis I mean an overwelmig amount of all sort of diffent protocools, technologies, standards, toolkits, ect etc. Does this make things better? No! It is better to develop a few but realy good pieces of sotware, that intergrates and works well together.\n\nPlease, next time look a bit further and think about the real reason of Linux's power.....Colaboration and working tohether."
    author: "Maarten Rommerts"
  - subject: "Re: I leave KDE."
    date: 2002-09-30
    body: "Ok, I'm not sure any more whether this guy is joking or not. In any case, using as much code as you can from others is A Good Thing. As long as KDE is faster than Gnome, and has better integrated applications, I will use it. Think of it like that: If people not directly working for KDE (e.g. the Gnome Team) produce 75% of KDE's code, the KDE team can concentrate more on the 25% that make the actual user experience of KDE."
    author: "Klaus Mertelz"
  - subject: "Neat Bugzilla features"
    date: 2002-09-20
    body: "I'm one of the Bugzilla developers. Here are some neat Bugzilla features that you might want to use:\n\nhttp://bugs.kde.org/duplicates.cgi\n- list of the most frequently reported bugs (those with the most duplicates). Good for potential bug-filers to look at.\n\nYou can change multiple bugs at once, when logged in with sufficient privileges - see the option at the bottom of the buglist page. Great for mass-reassignments if a coder leaves.\n\nIf you change the columns on your buglist (colchange.cgi), Bugzilla remembers which ones you last chose.\n\nIf you use Mozilla (perhaps quite unlikely round here), there's a Bugzilla sidebar with a load of neat features. However, it needs a small bit of JS to fire off the installer, which has been removed from the front page. If you want it, ask for it back.\n\nYou can save regularly-run queries (see the bottom of query.cgi) and have them appear in your page footer, for one-click access to \"Bugs Assigned To Me\" or something like that.\n\nhttp://bugs.kde.org/userprefs.cgi\nYou can control exactly when Bugzilla sends you email, about what events - head over to the user preferences.\n\nYou can get bugs as XML:\nhttp://bugs.kde.org/xml.cgi\nor buglists as RDF (add &format=rdf to your query string.)\n\nIndividual comments are hyperlinked using anchors, so you can refer to them in emails etc. Bugzilla also autolinkifies comments in several neat ways, so text such as \"bug 123, comment 4\" or \"http://www.google.com\" will become a link without you needing to type any <a href> nonsense.\n\nHope those are useful :-)\n\nGerv\n\n"
    author: "Gervase Markham"
  - subject: "Re: Neat Bugzilla features"
    date: 2002-09-20
    body: "> http://bugs.kde.org/duplicates.cgi\n\nThanks, I think we have an even better solution for that which avoids duplicates before they are submitted: our wizard uses the summary's words to search for similar bugs. It uses MySQL full-text search for that, i.e. bugs with most similar summaries will be most relevant and thus be listed on top.\nYou just have to add a full-text index for that. The code for the wizard is here: http://webcvs.kde.org/cgi-bin/cvsweb.cgi/bugs/bugz/"
    author: "Daniel Naber"
  - subject: "Re: Neat Bugzilla features"
    date: 2002-09-21
    body: "the RDF feature is sweet! now i can watch my bugs scroll across my panel in the newsticker. heh... "
    author: "Aaron J. Seigo"
  - subject: "Re: Neat Bugzilla features"
    date: 2004-03-01
    body: "How to handle test cases in bugzilla ?\n"
    author: "Bijumon V Gopalan"
  - subject: "Finally"
    date: 2002-09-21
    body: "Great move. That old bug tracking system was _really_ bad."
    author: "MrFoo"
  - subject: "Re: Finally"
    date: 2002-09-21
    body: "... but the old one was simple to use to report bugs. the new one requires login."
    author: "."
  - subject: "Re: Finally"
    date: 2002-09-21
    body: "No more closed bugs with just a (Done), links between duplicates, easier queries to find out if a bug as already been reported, the possibility to follow bug reports by adding yourself to the Cc. I think a simple login requirement is nothing compared to the benefits."
    author: "MrFoo"
  - subject: "Offline editing bugs"
    date: 2002-09-29
    body: "- no offline posting of bugreports\n\n- not easy for beginners\n\n- overloaded for small project modules\n\n- no annonymous posting\n\n- gui is crap"
    author: "Hakenuk"
  - subject: "Re: Offline editing bugs"
    date: 2002-10-02
    body: "> - not easy for beginners\n\nhttp://bugs.kde.org/wizard.cgi\n\nAnd bug reporting is not supposed to be for beginners. A small amount of GOOD bug reports is better than large amounts of bad reports.\n\n\n> - overloaded for small project modules\n\nMakes it easier to seperate things.\n\n\n> - no annonymous posting\n\nPrevents people from posting crap.\n\n\n> - gui is crap\n\nUnless you're the developer."
    author: "Stof"
  - subject: "Re: Offline editing bugs"
    date: 2002-10-14
    body: "> > - no annonymous posting\n> \n> Prevents people from posting crap.\n\nNot only that, but also gives the developer a hand to get information not in the bug report itself.\n\nAnd having a mail alias for bugreports doesn't sound bad in my ears.\n"
    author: "Peter"
  - subject: "Re: Offline editing bugs"
    date: 2003-08-30
    body: "hay,its me who wants a lesson and theory of off-line editing and on-line editing.could you help me please? becouse i need to be more educated in this field.can you help me?   "
    author: "Amanuel Zaid"
---
We have recently switched our entire <a href="http://bugs.kde.org/">Bug 
Tracking System</a> to <a href="http://bugzilla.org/">Bugzilla</a>.   Unlike the old system, Bugzilla is based on <a href="http://www.mysql.com/">MySQL</a> and 
thus enables <a href="http://bugs.kde.org/query.cgi">advanced search 
functions</a> and offers many other features such as email notification and voting. However, for access to the more advanced features and for bug and comment submission, users will need an account. Fortunately, the <a 
href="http://bugs.kde.org/wizard.cgi">bug wizard</a> will automatically create an account when used for the first time.  All existing bugs from the old system have been migrated to the new system thanks to the efforts of Stephan Kulow.
<!--break-->
