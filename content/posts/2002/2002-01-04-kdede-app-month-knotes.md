---
title: "KDE.de App of the Month:  KNotes"
date:    2002-01-04
authors:
  - "Dre"
slug:    kdede-app-month-knotes
comments:
  - subject: "Monthly?"
    date: 2002-01-04
    body: "Is this something new or have they've been doing this for some time?  Anyhow, nice work Michael :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Monthly?"
    date: 2002-01-27
    body: "Hi Christian,\n\nno, this is not really new. Cornelius Schumacher startet this forum in October 2000 - unfortunately there was a short break then until Cornelius suggested me to relaunch the \"Application of the month\" in september 2001. So it restarted in October 2001 :-)\n\nRegards,\n\nKlaus"
    author: "Klaus Staerk"
  - subject: "knotes rules"
    date: 2002-01-05
    body: "I love those pretty sticky notes on my screen."
    author: "exa"
---
<a href="http://www.kde.de/">KDE.de</a> (KDE's German-only site) has
<a href="http://www.kde.de/appmonth/2002/knotes/index-script.php">selected</a>
<a href="http://pim.kde.org/components/knotes.php">KNotes</a>
as the <a href="http://www.kde.de/appmonth/index-script.php"><em>Anwendung
des Monats</em></a> for this January, 2002.
Along with a
<a href="http://www.kde.de/appmonth/2002/knotes/beschreibung.php">screenshot-rich
description</a> of KNotes (basically, it's a desktop &quot;sticky-note&quot;
taker which has been very nicely integrated into KDE), the honor features a nice <a href="http://www.kde.de/appmonth/2002/knotes/interview.php">interview</a>
(<a href="http://babelfish.altavista.com/">Fishy</a>
<a href="http://www.kde.com/devel/interviews/brade-interview-en.html">English
translation</a>)
with KNotes maintainer
<a href="http://www.kde.org/people/michaelb.html">Michael Brade</a>.
Nice work, Michael!

<!--break-->
