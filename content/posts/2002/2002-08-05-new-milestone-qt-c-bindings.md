---
title: "New Milestone for Qt C# Bindings"
date:    2002-08-05
authors:
  - "niko"
slug:    new-milestone-qt-c-bindings
comments:
  - subject: "What's up with the Perl bindings?"
    date: 2002-08-05
    body: "I know there was work on new Perl bindings, has anything happened with those?\n"
    author: "Anon"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-05
    body: "I'll second that - I know quite a few perl developers who aren't going to suddenly take up C#, python etc just because perl isn't flavour of the month at the moment.\n\nCome on kde folks - kde is so cool, its a crime not to have bindings for as many languages as possible :)"
    author: "Rich"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-05
    body: "OK I'll reply to myself.\n\nYeah, I know its Trolltech I should be ranting at since these are QT bindings :)"
    author: "Rich"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-07
    body: "In fact, the KDE bindings are built with a modified kdoc, as kdoc is already able to parse the C++ code and build a tree of it, it would have been stupid to rewrite this functionnality from scratch. This modified version of kdoc is called Kalyptus.\nWith this tools, KDE developers (such as David Faure) were able to generate Python, Perl, Ruby, C#, C, Java, ... bindings for KDE and Qt.\nJust go on http://barney.cs.uni-potsdam.de/pipermail/kdevelop-devel/2002-May/007952.html , to see that I am right.\n\nAs you've read it above, there are already Perl bindings for KDE and Qt, just go on http://developer.kde.org/language-bindings/index.html or http://search.cpan.org/search?dist=PerlQt.\n\nI hope I solved your problems.\n\nHave a nice day, all !\n\n--\n\"As a computing professional, I believe it would be unethical for me to advise, recommend, or support the use (save possibly for personal amusement) of any product that is or depends on any Microsoft product.\""
    author: "Chucky/OutMax!"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-07
    body: "Good to see that something is there, but the most recent revision I can find on CPAN is PerlQt-2.105.tar.gz dated 7th March 2000.\n\nKDE/QT3 had been out for some time now but the KDE developer page states:\n\n\"Getting your perl scripts to work with KDE. This page is not done yet, but there are Qt-2 and KDE-2 perl bindings on CPAN. There is work going on to update those to Qt3.\"\n\nLooks to me like perl 6 will be here before up to date QT bindings - and we all know how long that's going to take :)\n\nAnyway, thanks for the reply."
    author: "Rich"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-07
    body: "QtPerl is ready. \nIt's fully working and has a complete ui compiler for Designer files.\n\nWe are now in the process of writing documentation, furbishing astounding screenshots, etc...\nIt should be released next week.\n\nCheers,\n\nGermain"
    author: "germain"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-08
    body: "Wonderful!\n\nI retract all my posts on this subject - you never know, might even be able to give something to KDE (perl based, of course) in the future instead of just take :)\n\nThanks for the good news,\nRich"
    author: "Rich"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-08
    body: ">you never know, might even be able to give something to KDE (perl based, of >course) in the future instead of just take :)\n\nWell, the initial release will only provide bindings for Qt... KDE is the next step ! (not too far, hopefully)\n\nAlso -I'm really nuts- it is not QtPerl (though I like this name better) but indeed PerlQt, as it follows the naming of previous bindings for Qt 2.\n\nCheers,\n\nG."
    author: "germain"
  - subject: "Re: What's up with the Perl bindings?"
    date: 2002-08-09
    body: "> Well, the initial release will only provide bindings for Qt... KDE is the next step ! (not too far, hopefully)\n\nDamn - never mind though, something's way better than nothing!\n\nI played around with perl wxWindows/GTK a bit but never really got into it - definitely prefer KDE/QT, though I appreciate the cool stuff the Gnome folks are doing with Gnome2.\n\n>Also -I'm really nuts- it is not QtPerl\n\nCan't say I noticed ;->"
    author: "Rich"
  - subject: "Mono website"
    date: 2002-08-05
    body: "It would be nice if they put a link to the Qt# website\nas they have done with gtk# don't you think?"
    author: "AI"
  - subject: "Re: Mono website"
    date: 2002-08-05
    body: "Yes, they really should do this.  Nobody talks about Qt over there though."
    author: "KDE User"
  - subject: "Re: Mono website"
    date: 2002-08-05
    body: "To be fair, Go-Mono.org does have a link to Qt# on there resources page.  Gtk# is there prefered GUI toolkit which should be of no surprise :-)\n\nI've found Miguel and the Ximian/Mono team to be nothing but entirely gracious and helpful.\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "Re: Mono website"
    date: 2002-08-05
    body: "Not even a news item on their site?"
    author: "KDE User"
  - subject: "Re: Mono website"
    date: 2002-08-05
    body: "Go-Mono.org does have a Qt#-comment on the first page as well.\nOf course, it's not a surprise but must feel a little bit\nstrange for the Qt#-team."
    author: "AI"
  - subject: "Re: Mono website"
    date: 2002-08-06
    body: "Ximian is cool."
    author: "KDE User"
  - subject: "Re: Mono website"
    date: 2002-08-17
    body: "No... no... nononono. Ximian and Miguel are evil. They are not followers of the holy cause of TrollTech everywhere (tm).\n\nXimian must die - they just use VC money to copy TheKompany and smash the benevolent Godhead of TrollTech. Shawn Gordon tells me so.\n\nYou must not post nice comments about Ximian/Miguel here, it is heresy and a banning offence. Consider yourself warned.\n"
    author: "KDE Zealot"
  - subject: "KDE - MS Office ?"
    date: 2002-08-05
    body: "What does it mean?\nWill next MS Office releases (based on C#) integrate with KDE????\nThat would be great!\nAnhybody could explain a little bit what does Qt C# bindongs mean?"
    author: "Anonymous"
  - subject: "Re: KDE - MS Office ?"
    date: 2002-08-05
    body: "There is no way the next MS Office releases will be based on C#.  Just not gonna happen."
    author: "KDE User"
  - subject: "Re: KDE - MS Office ?"
    date: 2002-08-06
    body: "The next version of Office does not run under the CLR, it does however uses .NET Web Services. So in other words, it woulnd't run on Mono. It would still be a Win32 application."
    author: "Rajan Rishyakaran"
  - subject: "What's up with Python bindings, DnD KDevelopness "
    date: 2002-08-05
    body: "etc. etc.  \n\nOn e should be able to popup a KDevelop QtDesigner window and drag and drop a Python <---> DCOP GUI clicky controller thingy application of some sort inside 5 minutes eg:\n\n\nBoss: Need custom app with cool widgets for querying 4 different databases dumping into spreadsheet and doing fancy printing/faxing/PDF conversion of the resulting charts. Need it tomorrow, slave!! (boss goes off on other rant ...)\n\nDeveloper: [flips open laptop in boss's office opens KDevelop/QtDesigner]\n\n[Developer drag and drops some Qt<-->Python<-->DCOP querying app together with DB access widgets (built-in to Qt no??), another 3 buttons to export to KSpread via DCOP, another button to use DCOP and scripted KSpread and CUPS (which rules to world) with choices \n\n    []  Fax query results to: [________] \n    []  Create PDF\n    []  E-Mail PDF to :  [__________]\n\n        Print\n        =====\n\n    []  HQ Printer (vai IPP)\n    []  Mail room printer              \n    []  Legal Dept. printer\n\n    []  View streaming video from gym change rooms:  M:[]  F:[]\n\n \n....]\n\nBoss:  ... [finishing rant]  and furthermore we will convert all DBA's KDE workstations to XP!!!\n\nDeveloper:  Oh ... I just finished developing the application you asked for.\n\n\nBoss: Wah??!! Lemme see that you genius!!"
    author: "Imaginative Python Lover"
  - subject: "when it is a pleasure to program in VB?"
    date: 2002-08-06
    body: "When they give it up and support Python.\n\nI'll second that."
    author: "caoilte"
  - subject: "PHP Bindings?"
    date: 2002-08-06
    body: "My dream is that someday I'll be able to write QT apps in PHP.\nI'm learning C++, but PHP is soooooooo much nicer and easier than C++ that it would be more than welcome to being able to use QT with it.\n\nCurrently Gtk works with PHP/CGI, but I'm a QT fan, so I hope it will someday work with it :)"
    author: "protoman"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "PHP is not easier. \n\nPHP seems easier while you learn it.\n\nPHP will be considerably harder once you start doing real world stuff.\n\n"
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "Hummm, I tought managing databases was a real world (tm) thing.\nPHP is really easier than C++ and can do things that C++ code do as well as it (but losses on speed, sure).\nBesides, I don't said I was going to build windows or games with it. PHP is good for writting some small apps as moo/mud clients, d20 player generators, etc.\nAnd all those things are real world man :)"
    author: "protoman"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "PHP is simple. Simple != easy.\n\nNow, python, on the other hand, is simple AND easy ;-)\n\nPS: if it is not to build \"windows\", hat is the point of PHP in a qt related thread?"
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "I metan m$ windows ;)\n\nWell, in my case I think PHP is simple and easy as you think about Phyton that I think isn't easy....so each person have it's prefereed language and let's not start a language war here :)"
    author: "protoman"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Python *is* actually easier to learn that PHP. The syntax is easier, data handling (lists, maps etc) are easier to use etc. Nothing wrong with PHP, but it is harder to learn, especially for a newbie. Just think of the weird semicolons \";\" you have to sprinkle all over, but not on every line."
    author: "Chakie"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "PHP means \"PHP Hypertext Preprocessor\". Since when is a hypertext preprocessor considered a full programming language?"
    author: "Stof"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Can't a language evolves besides it's original name?\nDid you already see what python and perl means? Tehir names means nothing acctually, where made just to create a regular word with each letter.\nThis means they are bad? Sure not!\nC means just C, the languace after A and B. C is crappy because it's name?\nShould it be: GLTMEOA\n(Great Language That Makes Everthing, Or Almost)?\nSURE NOT! No need for it.\nNow, please stop just talking bad of PHP.\nSeems like your guys think that if someone says PHP is good, he means \"all other languages are crappy\". :("
    author: "protoman"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "No, but that preprocessor part in a name tells you what was the purpose when it was designed and how it could likely effect the design of the language.\n\nPersonally, I think PHP is (just) C with better string handling. If that's good or bad depends on person."
    author: "Marko Samastur"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "I'm not talking about it's name, I'm talking about it's function! It's function is exactly what the name says: to preprocess things.\nI have a hard time believing that a preprocessor can be used to create GUIs."
    author: "Stof"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "I mean: believing that a preprocessor is an effective tool to create GUIs."
    author: "Stof"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "You have no idea what PHP is.  Look into the Zend VM and the PHP compiler.  Yes, PHP 1.0 started out as a perl script and a small C program.  Since 4, it's a powerful development environment that allows the creation of command line, ncurses and gtk applications, and has a wide library of loadable binary modules that can also be accessed via a CPAN like mechanism (called PEAR).  \n\nAnd if you doubt that it could have turned into a nice language from such simple beginnings, consider that C++ started out as a header file with a bunch of C Preprocessor directives.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "C++ did NOT start like that.\n\nYes, some C++ compilers are \"preprocessors\" that generate C code.\n\nNo, it is not the usual C preprocessor."
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-08
    body: "Funny, back in 1983 when Bjarne Stroustrup created CFRONT, it started out as a simple set of header files.  By the time he was writing about it in Dr. Dobbs, Byte or whatever I first heard about it back when, it had a simple namespace mangler, but in introducing the new variant of C in his articles, he would say that it started out as some neat CPP hacks (I think he also mentioned its CPP heritage in the CUJ article when they were looking to finalize the language spec, or right afterwards).  It wasn't until 1990 or thereabouts that \"real\" C++ compilers appeared.  I know - I was an avid C user (to the ludicrious level of writing my own tiny C in asm) throughout the 80s, and followed all the variants and compilers very very closely. \n\nBe that as it may, C++ is not the issue here.  PHP is.  PHP would make a fine candidate for Qt and KDE bindings.  My MP3 organizer is written in PHP, and currently uses curses.  While I personally prefer Ruby or C, PHP is not a terrible language in terms of being a primarily procedural language with a slight smattering of OOP concepts.  It also happens to be the universal data juncture tool, even better than perl, and with a syntax saltier than perl's syntax (a Good Thing, IMO).  PHP has a very detailed and flexible library system and a CPAN like system called PEAR.\n\nNo reason not to do it, and it may encourage KDE development, which is a good thing, last I checked.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: PHP Bindings?"
    date: 2002-08-17
    body: "No KDE bindings for PHP, hence it is EVIL. Once bindings are written it becomes blessed and holy. Roberta knows the truth.\n"
    author: "KDE Zealot"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-20
    body: "Whoa. Mispelling a name so it has a different gender.\nWhat a pinnacle of wit."
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-12-15
    body: "I am php programmer and I would thank for qt and kde bindings of php,\nI have a idea (might be silly) there is wrapper SMOKE on qt and kde where\nclasses and functions and other stuff can be \ncalled from it I think a perl bindings is based on it and it is working.\nI hope I helped in this discussion.\ntroby.\n\n"
    author: "troby"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Python's name is not an acronym. Python is called python. Not P.Y.T.H.O.N.\n\nPython means python. It is a hommage to monty python."
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Ok, I tought I said, not let's start a war. :(\n\nI think Python is trash, junk, sh*t. Does this removes any merits of it or will stops you liking it? NOOOOOOOOOO!\nSo why instead of saying PHP is bad MY language is better don't you people just keep quiet?\nIf someone makes PHP bindings for QT will you people being forced to use it instead of Perl or Python? SURE NOT!\nC'mon!"
    author: "protoman"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Relax. When someone tells you python is good, if you ask him, he CAN provide examples of software developed using it, and explain why the language has helped him being effective in the project.\n\nPython advocates can provide backup for their claims.\n\nPHP advocates, on the other hand, write \"sh*t\" in their responses.\n\nGo, take some linden, relax."
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-17
    body: ">PHP advocates, on the other hand, write \"sh*t\" in their responses.\n\nWhereas you write \"sh*t\" instead. Big difference zealot boi.\n"
    author: "KDE Zealot"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-17
    body: "Funny that you call me zealot, KDE Zealot. Does that mean we are relatives?\n\nAll I meant by what you quote is that the previous poster simply called python \"sh*t\" without giving any argument whatsoever.\n\nOn python\u00b4s behalf, I can point out coherent syntax, simple to use object orientation, rich class library, readability, a simple extension and embedding \nmechanism, good Qt bindings, and JYTHON (python in the JVM).\n\nIn all those aspects, I think Python is a better language than PHP for\nreal world usage.\n\nNow, anyone can call this \"sh*t\". But it is going to take a whole lot more\nthan a silly dot post to make that charge stick."
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-17
    body: "That is \"wiggle\" again."
    author: "Navindra Umanee"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-18
    body: "Actually, it's not. Is that why you banned all Freeserve users from posting again, beause someone posted three sarcastic messages lampooning the zealot nutcases who inhabit your site? Just how stupid are you, anyway?\n\nClue: If I wanted to play this game, I could rotate through a couple of hundreds proxies, and a dozen or so free ISPs. In fact, with a dumbass like you in charge, I could probably get most of the UK internet population banned from this site with a little effort.\n\nThere are just some people who haven't the smarts or the temper to run a forum site, and you are the perfect example. Think yourself lucky that this is little more than a five minute time-wasting exercise for me.\n"
    author: "KDE Zealot"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Ah, well, while we're at pluggin languages : Ruby."
    author: "Guillaume Laurent"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-07
    body: "Ruby is cool.\n\nI am too entrenched in what I know to switch, but should I ever feel a need to learn a new language, Ruby would be a big candidate :-)"
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-08
    body: "If you know C++, Perl or Python, learning Ruby is a matter of a few hours. I don't think I've ever used a language which was such a delight to program with."
    author: "Guillaume Laurent"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-08
    body: "Well, python does take 2 hours to learn, too.\n\nIt takes a little loner to learn to use it effectively, of course.\n\nBut ok, you convinced me, there goes the third sunday of August!"
    author: "Roberto Alsina"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-16
    body: "Then there's Rebol ;-)"
    author: "Kuba"
  - subject: "Re: PHP Bindings?"
    date: 2004-09-30
    body: "would you please define ...\n\"PHP will be considerably harder once you start doing real world stuff.\"\n\n"
    author: "somekool"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "Ugh, i really hope i wont have to read the code for any QT applications written in php, or even perl. Those languages tend to promote bad code. (as in, hard to read and maintain)\n\nWhile i like perl and php i would never use them for gui apps.\n"
    author: "troels"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "Haskell promotes good code :)\nKDE/Qt Bindings for Haskell, Haskell support in Guideon... that would be really nice. \n\nwater"
    author: "Water"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "What makes bad code are bad programmers ;)\nWell documented code can even be bad that will work once anybody can \"fix\" it easily.\nAnyway I like the possibility to write PHP code in some ways as it let's. So you can write code similar to Perl, C, C++, etc. Just choose and make it clean and well documented."
    author: "protoman"
  - subject: "Re: PHP Bindings?"
    date: 2002-08-06
    body: "> What makes bad code are bad programmers ;-)\n\nI agree. However, with other languages, ONLY bad programmers write bad code ;-)"
    author: "Roberto Alsina"
  - subject: "There's more demand for Java, but...."
    date: 2002-08-06
    body: "There are literally thousands of Java applications out there that could usefully be ported to KDE. People's time would be much better spent supporting this route than fantasizing about cloning Dotnet - you really think MS is going to allow this to happen?\n\nJava/KDE bindings *have* been created, but unlike most libraries these are licensed under the GPL. This is having a most unfortunate effect. Big name semi-commercial projects such as the IBM Eclipse IDE *do* want to support KDE/Qt (Eclipse uses its own UI library called SWT) but the GPL license prevents this, since Eclipse code also appears in commercial form in the WebSphere product line. So they continue with Gtk+ and KDE stays on the back-burner.\n\nThe result of this available-but-not-usable situation is that KDE is losing momentum in the key area of Java applications - the GPL libraries can only be used for the (generally small) GPL applications. \n\nOf course it would be I'd be happy if IBM were to compensate the developer of the Java/KDE binding, I hope they or some other companies do. However, at the moment, the road ahead for Java on KDE is blocked and the 'workaround' of C# is fraught with danger."
    author: "Ulrich Kaufmann"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-06
    body: "Do you mean that the Java-bindings for Qt are GPL:d? Only GPL?\nThis clearly shows the problem with GPL:d libraries."
    author: "A"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-06
    body: "The use of the GPL (rather than LGPL) was no accident, therefore it is no more a 'problem with the GPL' than breaking the speed limit on the road is a problem with speed restriction signs."
    author: "Ulrich Kaufmann"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-06
    body: "You could well, you know, ask the author, instead of complaining on a website.\n\nOn the other hand: the Qt/Java bindings still may have some code from my old QtC, and that is ONLY available under the GPL, so remember to ask every copyright holder.\n\nOn the third hand, if IBM so wants to use Qt, they can relax the license on THEIR code.\n\nAfter all, supposing I was the sole copyright holder (I don't even know if I am one), what has IBM done for me lately?"
    author: "Roberto Alsina"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-06
    body: "As it apparently wasn't obvious from my post, the author *has* already been contacted by IBM and currently is not willing to change the license. As I have said, I have no quarrel with the author - good luck to him/her/them in getting a return on their investment. It's the effect on the rest of us that's the problem.\n\nI'm afraid I'm not in a position to speculate what IBM might or might not have done for the author(s) concerned. However, IBM has contributed very significantly to Linux and open source - overall I guess some give and take would be reasonable."
    author: "Ulrich Kaufmann"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-06
    body: "Uh, you do know that the person you are replying to is the original author of the QtC bindings that both qtjava and Qt# rely upon.  Richard Dale extended these bindings and created qtjava.  I do not think Richard would have a problem relicensing his code under LGPL or X11.  I don't presume to speak for Roberto.\n\nYou should also know that Qt# is licensed under the GPL, so we have the same problems with commercial applications as the other bindings.  We are working to change this and as long as the Trolls agree we'll be able to develop cross-platform apps with Qt#.  Of course even if Qt# were LGPL or similar license, a commercial developer wishing to link to Qt# (and thereby Qt/C++) would still need purchase a license from Trolltech."
    author: "Adam Treat"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-07
    body: "Just to clarify, as far as I am concerned, Richard Dale is the one who deserves all the credit, and whatever he says goes.\n\nIf he was contacted and refused, he must have his reasons. I am sure IBM could convince him if they really wanted.\n\nBut what is the point? To do proprietary apps, you still need to buy Qt.\n\nI suppose dual-licensing under GPL/QPL would be better, since it allows use of other licenses beyond the GPL. If Richard agrees, of course, but I am not in the business of convincing people."
    author: "Roberto Alsina"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-07
    body: "If there's some special significance in the previous statement about Qt/Java from Roberto then please feel free to enlighten me, meanwhile either you or the Eclipse contributors appear to have misunderstood Richard Dale's position. \n\nHere is the discussion from the IBM Eclipse list - if a way forward can be worked out that would be very welcome news.\n\n\nFrom: David Goodenough <david.goodenough@btconnect.com>\nNewsgroups: eclipse.tools\nSubject: Re: KDE SWT binding?\nDate: Thu, 01 Aug 2002 11:46:01 +0100\nOrganization: D.G.A. Ltd\nLines: 22\nMessage-ID: <aib3b2$psi$1@rogue.oti.com>\nReferences: <ahsd87$e1h$1@rogue.oti.com> <ahsdvt$ed5$1@rogue.oti.com> <ahsf2n$etl$1@rogue.oti.com> <aiap0f$l08$1@rogue.oti.com>\nNNTP-Posting-Date: Thu, 1 Aug 2002 10:42:43 +0000 (UTC)\n\nMartin M\u00f6bius wrote:\n\n> Guillermo Castro wrote:\n> \n> \n>> So it's a licensing issue, then...\n> \n>> I don't understand much of the licensing restrictions. I know SWT is\n>> under the CPL, and QT is GPL. KDE/Java binding is also GPL. Is the CPL a\n>> more restrictive license? where/who can clarify what would be needed to\n>> make eclipse run as a 'native-looking' kde app?\n> \n> Perhaps contact the author of the kde/java bindings. I assume licences can\n> be changed by the author, but I do no really know.\n> \n> martin\nI have already done so.  He is unwilling to change the license because he\nis in the business of making money from writing JNI bindings and does not\nwant to release these ones under LGPL to match the KDE libraries.  I \nsuppose if someone were to dangle some money under his nose he might.\n\nDavid"
    author: "Ulrich Kaufmann"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-08-07
    body: "OK, then. He doesn't want to change it.\n\nIBM can relicense eclipse just as easily as he can relicense his code.\n\nAnyway: the licensing on Qt bindings is tricky.\n\nThe binding-generating tool is GPL.\n\nBut what about the generated code? The generated code is a derived work of Qt, probably, since they are generated from Qt headers.\n\nAs such, I don't know if Richard can change the license of that code, which may be under a dual QPL/GPL license.\n\nIBM should ask a lawyer and/or pay Richard."
    author: "Roberto Alsina"
  - subject: "Re: There's more demand for Java, but.... Really?"
    date: 2002-08-07
    body: "How do you know that they actually want to support KDE? I find that most/all commercial companies just don't care about Qt/KDE, they all go with Gtk, which is a shame. If IBM really wanted to support KDE they probably could, but the serious will to do it is probably not there."
    author: "Chakie"
  - subject: "Re: There's more demand for Java, but.... Really?"
    date: 2002-08-07
    body: "Hmmm... which commercial companies are all going with GTK???? "
    author: "Ralex"
  - subject: "Re: There's more demand for Java, but.... Really?"
    date: 2002-08-07
    body: "Ximian.\n\nBut then again, TheKompany goes with Qt."
    author: "Stof"
  - subject: "Gobe Producive"
    date: 2002-08-07
    body: "Gobe productive, a really great, fast, feature complete office suite is also gtk based, as is the official Yahoo instant messanger, loki games (mainly the loki installer, which will hopefully live on even now loki's dead) netscape also uses gtk(internally), and i guess even Mandrakes setup tools could be considered in this category (they're gtk, and last time i looked they weren't gpl) Openoffice is also shifting to using gnome's accessablity toolkit (but not gtk).(IBMs eclipse was also mentioned above) - So, lots (relatively).\n\nHowever i've also found that quite a few commercial apps still use Motif (or something that looks just as bad) examples of this are Codewarrior (which is an otherwise great IDE) realplayer, and acrobat reader.\n\n\n"
    author: "me"
  - subject: "Re: Gobe Producive"
    date: 2002-08-08
    body: "There are a lot of Qt based stuff. Hancom Office is Qt based.\nLook at trolltechs homepage. There are a lot of applications\nwhich require a strong solid base that use Qt. Mainly, the\napps I've seen for gtk are smaller. Qt is the best base to build\na larger project upon. Of course, for a smaller app (yahoo messenger)\nit might not be worth the price, but in the long run Qt is excellent."
    author: "A"
  - subject: "Re: Gobe Producive"
    date: 2002-08-08
    body: "And then again, nothing stops you from using gtk-apps in KDE.\nMy impression though, is that gtk is more a toy while qt is professional."
    author: "A"
  - subject: "Re: Gobe Producive"
    date: 2002-08-09
    body: "That's like saying Linux/*BSD are toys and that Windows/Solaris/CommercialUnix are professional.\n\nOK, GTK+ isn't C++, but it's a very, *very* nice C-based object oriented (!) GUI tookit."
    author: "Stof"
  - subject: "Re: Gobe Producive"
    date: 2002-08-10
    body: "> GTK+ isn't C++, but it's a very, *very* nice C-based object oriented (!) GUI tookit.\n\nProgramming GUIs in C is like being mauled by a hippopotamus.\n\nDoing it using GTK+ is like being mauled by a nice hippopotamus."
    author: "Roberto Alsina"
  - subject: "Re: Gobe Producive"
    date: 2002-08-12
    body: "> Programming GUIs in C is like being mauled by a hippopotamus\n> Doing it using GTK+ is like being mauled by a nice hippopotamus\n\nIs there even a point, now when we have Qt?"
    author: "A"
  - subject: "Re: Gobe Producive"
    date: 2002-10-02
    body: "Yup, Gtk definatly has it's uses. It's nice in small situations (like loki's installer), when you need the library statically linked.\n\nAlso, when you need to develop propreitary software and are unwilling to pay for a properitary-usage copy of Qt."
    author: "fault"
  - subject: "Re: There's more demand for Java, but.... Really?"
    date: 2002-08-07
    body: "Good question, particularly as the earlier posting I quoted was not from an IBM person. However, Adrian Cho is at Object Technology International, owned by IBM and the source of Visual Age, and below is a response from him. They're interested, but are \"caught\" by both Java/KDE and Qt licensing.\n\nFrom: \"Adrian Cho\" <adrian_cho@oti.com>\nNewsgroups: eclipse.tools\nSubject: Re: KDE SWT binding?\nDate: Mon, 29 Jul 2002 11:34:41 -0400\nOrganization: Object Technology International, Inc.\nNNTP-Posting-Date: Mon, 29 Jul 2002 15:33:39 +0000 (UTC)\n\nAre you talking about KDE or Qt?  The OTI/IBM team has an initial port of\nSWT Qt but it will not be released until we complete further investigation\nof the legal issues due to licensing incompatibilities.\n\nGTK+ is under LGPL and hence the SWT GTK+ binding is LGPL'ed.  Qt however is\nunder the QPL and this license is problematic for us.\n\nAdrian\n\n\"Guillermo Castro\" <mandrake@freeshell.org> wrote in message\nnews:ahsd87$e1h$1@rogue.oti.com...\n> Hi,\n>\n> I've been searching all around looking if someone is developing the\nKDE-SWT\n> binding. So far, I haven't found anything. KDE is not even mentioned on\nthe\n> main SWT page as a future supported platform.\n>\n> Are there any plans to create this? If not, I would try to do it myself.\n>\n> Thanks,\n>\n> --\n> Guillermo Castro\n> Java Developer\n"
    author: "Ulrich Kaufmann"
  - subject: "Re: There's more demand for Java, but.... Really?"
    date: 2002-08-07
    body: "So, the problem is not with Richard's bindings being under the GPL, but with Qt being under the QPL?\n\nThe QPL doesn't forbid much in the way of usage, as long as the code using the QPLd code is under a free license. The big exception is that the GPL collides with pretty much any other license (never legally tested, just the FSF claims).\n\nSo, as an exception, Qt is also available under the GPL for that reason.\n\nPerhaps, as usual, the situation would be clearer if those actually doing the stuff talk to each other.\n\nDoes this SWT binding link to Qt? I guess yes.\nDoes it link to other code owned by IBM? I guess yes.\n\nNow, the only reason why there can be a legal prolem are:\n\na) The binding license is incompatible with the QPL (in other words, it is not free)\n\nb) The other IBM code has a restrictive license that is incompatible with the QPL (ie: THAT code is not free)\n\nIn no case is the restriction being imposed by Qt's license, but by the other code's.\n\nIf IBM intends it to be free software, or open source software, they probably CAN do it.\n\nIf they don't, probably they can't.\n\nBut without knowing the code and the licenses involved, it is silly to efven guess."
    author: "Roberto Alsina"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-10-01
    body: "So,\n\nFrom all the posts, I haven't found anyone trying to come up with a solution. I think java developers would benefit from a java/kde binding. If the current binding has a restrictive licence, why not create a new binding with a less restricting licence, like LGPL? It has been done before, you know, and that's the beauty of OSS.\n\nAny takers?"
    author: "Guillermo Castro"
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-10-01
    body: "http://developer.kde.org/language-bindings/java/index.html\n\nTo quote: \"Java support for KDE is now fairly significant, you can add applet support to your KDE applications, and with the Qt/KDE Java bindings you can even develop KDE apps in Java. Issues relating to Java support for KDE should be discussed on the kde-java mailing list (though applet issues are usually discussed on kfm-devel)\".\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: There's more demand for Java, but...."
    date: 2002-10-02
    body: "I think you misunderstood me.\n\nI know there are Java Bindings for QT/KDE, but the problem is that those are under a license that doesn't allow the Eclipse team to create a SWT-KDE port. Since the binding's author doesn't want to change the license terms, there are two options now for Eclipse:\n\n1.- Avoid porting SWT to KDE, and use GNOME and Motif for linux (which is what they're doing right now)\n\n2.- Create a Java-KDE Binding from scratch (or let someone else do it).\n\nSince they're happy with 1 and we can't change the license, we won't see eclipse running on KDE unless we create our own binding.\n\nI hope I made myself clear now..."
    author: "Guillermo Castro"
  - subject: "Does qt# integrate with KDevelop & Designer?"
    date: 2002-08-07
    body: "the samples seem to indicate code written in Kate - does or will a developer have the ability to develop programs in the KDevelop and Designer ide using the qt# language?"
    author: "John Taber"
  - subject: "Re: Does qt# integrate with KDevelop & Designer?"
    date: 2002-08-07
    body: "no. not at this point anyway...\n\na number of people have brought up the idea of integrating uic/designer with Qt# on our IRC channel.  if you would like to see this then I suggest you talk with marcus on #qtcsharp or the list...\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "Question for Adam"
    date: 2002-08-09
    body: "Nice work!\n\nAdam, can you get Qt# integrated as a main component into the Mono distribution?\n"
    author: "KDE User"
  - subject: "Re: Question for Adam"
    date: 2002-08-09
    body: "It is possible, but we'll have to eliminate the dependency upon libqtc first...  I don't know what advantages there would be in hosting cvs under mono though.  That is what you mean isn't it?\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "Re: Question for Adam"
    date: 2002-08-09
    body: "I want them to promote Qt# as part of the Mono project just as much as they are promoting GTK#.  Why do you have to eliminate the dependency on libqtc?"
    author: "KDE User"
  - subject: "Re: Question for Adam"
    date: 2002-08-09
    body: "Ximian is not in the business of developing for the KDE Desktop.  They have sponsored and founded the Mono project, so wishing that they would promote Qt# as much as Gtk# is a only going to lead to frustration.  They have been very gracious with there time and attention to all the questions and problems we have faced with Qt#.\n\nIt has been a wonderful experience working side by side with the Ximian/Gnome developers and I wish to emphasize the positive instead of dwelling upon the differences.  Besides, Qt# should work equally well with Portable.Net as well as Mono.\n\nNow, the dependency upon libqtc is a nasty one, because it prevents us from becoming a truly cross-platform lib.  libqtc is licensed under the GPL which disallows linking to any other version of Qt besides Qt/X11.  It is also very difficult to maintain.  Hope this clears the air a bit."
    author: "Adam Treat"
  - subject: "Re: Question for Adam"
    date: 2002-08-10
    body: "What is libqtc exactly and why is it needed?  Qt is GPL in the first place..."
    author: "KDE User"
  - subject: "Re: Question for Adam"
    date: 2002-08-10
    body: "libqtc is a C binding for Qt.\n\nThe idea is that languages link easier to C libraries than C++ ones, so libqtc works\nas a C buffer, and you link language->C->C++\n\nI don't much see how C# is going to link to C++ classes without it or something similar, though, but I know nothing about C# ;-)\n\nThis approach is the same used by Kylix, too.\n\nAlthough Qt is GPL, Qt is not ONLY GPL.\n\nQt is also under the QPL, and under a commercial license, and under a free-as-beer-sometimes license.\n\nSince qtc is GPL, you can not use it with a Qt licensed under any of the other licenses.\n\nHere`s an idea. If IBM or Ximian or whoever pays me 2500 U$S, I promise to write a new QtC under a BSD license.\n\nMany better programmers can do a better job, but not many can do it cheaper or claim to have done it before ;-)"
    author: "Roberto Alsina"
  - subject: "Qt license question"
    date: 2002-08-12
    body: "A small question. Is there a possibility to develop closed source\nfreeware programs with Qt? Shareware (what's the difference anyway)?"
    author: "A"
  - subject: "Re: Qt license question"
    date: 2002-08-12
    body: "Of course you can.\n\nIt's gonna cost you, though."
    author: "Roberto Alsina"
  - subject: "Re: Qt license question"
    date: 2002-08-13
    body: "I guess that's the philosophy here.\nIf you use open source, you support open source."
    author: "A"
  - subject: "Re: Qt license question"
    date: 2002-08-16
    body: "Although that has nothing to do with Qt, small closed-source shareware/freeware projects seem to be a waste of time in >90% of cases. The mentioned >90% of them die, and there's almost no benefit to the society (nor to the original programmers). For small projects that are meant to stay small, open source (ideally GPL) is almost always a good way to proceed.\n\nAFAIK, there are only a few relatively small \"shareware\" type projects that have stayed in the marketplace for a longer time and had generated useful revenue. WinZip comes to mind.\n\nThere have been a beautiful shareware utilities for DOS, some of them quite worth porting to unix nowadays. Yet they have been a waste of time from *today's* perspective since there's no easy way to reuse and expand them, even if we forget about licensing problems. Even though the utilities were useful and all, they are now in bit-heaven. The companies that did them don't exist, some of the programmers that did them may have even retired or became air traffic controllers by now. And I doubt that most of these small shareware shops had short-livedness, or temporary benefit, as one of their goal. I do understand that some shareware authors might have done shareware to temporarily repair their home budgets, but that's a tiny percentage of all shareware makers.\n\nTo summarize: if it's a small freeware project, there's no point in not making it opensource: freeware doesn't generate revenue. And you can always make the project double-licensed, so that all outside contributors know that their code may be used in a commercial way. After all, what is the other point of keeping freeware closed-source? The only imaginable point is to make it sell for money, and that's where double-licensing works perfectly.\n\nIf it's a shareware project, the question is whether shareware is something you want to do. Yet, in many cases shareware can work anyway all right with open source and generate same or even greater revenue that it would without being open source."
    author: "Kuba"
---
<a href="http://qtcsharp.sourceforge.net">Qt# 0.4</a> has been <a href="http://qtcsharp.sourceforge.net/release-0.4">released</a>!  Qt# is a set of cross-platform C# bindings for <a href="http://www.trolltech.com">Trolltech</a>'s Qt GUI toolkit that is currently targeted towards <a href="http://go-mono.com">Mono</a> and <a href="http://www.dotgnu.org">Portable.NET</a>. Along with some initial <a href="http://qtcsharp.sourceforge.net/apidocs/index.html">API documentation</a>, code samples, tutorials and bugfixes, there have been a lot of improvements over 0.3, including support for events, multiple custom slots, object tracking and even preliminary support for Microsoft.NET. Download <a href="http://prdownloads.sourceforge.net/qtcsharp/qtcsharp-0.4.tar.gz?download">here</a> -- some screenshots can be found <a href="http://qtcsharp.sourceforge.net/screenshots.html">here</a> [<b>Ed:</b> <a href="http://qtcsharp.sourceforge.net/screen/scribble-0.4.png">wow</a> and <a href="http://qtcsharp.sourceforge.net/screen/cross-platform.png">wow</a>], and Debian apt sources <a href="http://chemlab.org/~nick/apt-sources.txt">here</a>.   Interested parties should also feel free to drop by #qtcsharp on irc.OpenProjects.net.
<!--break-->
