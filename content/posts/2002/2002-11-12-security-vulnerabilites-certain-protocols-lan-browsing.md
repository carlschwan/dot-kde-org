---
title: "Security:  Vulnerabilites in Certain Protocols, LAN Browsing"
date:    2002-11-12
authors:
  - "Dre"
slug:    security-vulnerabilites-certain-protocols-lan-browsing
comments:
  - subject: "additional download location"
    date: 2002-11-12
    body: "may I add:\n<p>\nan updated version of lisa as a single package, which can be compiled independent from Qt and KDE is available at <A href=\"http://lisa-home.sourceforge.net\">lisa-home</a>.\nIt's version 0.2.2.\n<p>\nBye<br>\nAlex\n"
    author: "aleXXX"
  - subject: "Security through clarity"
    date: 2002-11-12
    body: "KDE is getting security advisories these days. It seems that security people are looking at the source. This is an indication of KDE's increasing popularity.\n\nOf course, having a security advisory is never good news, but it is still better to have one than to leave the hole."
    author: "Philippe Fremy"
  - subject: "Re: Security through clarity"
    date: 2002-11-12
    body: "But as you can see, from the number of replies, nobody cares..."
    author: "AC"
  - subject: "Re: Security through clarity"
    date: 2002-11-12
    body: "<I>But as you can see, from the number of replies, nobody cares...</I>\n\nHummm.... A troll who spends time monitoring the KDE site. That in itself indicates that quite a few people care, one way or another. Keep up the good work KDE."
    author: "a.c."
  - subject: "Re: Security through clarity"
    date: 2002-11-12
    body: "I think people care. There is not much to say about this topic, though. Security bugs happen, it's just a fact of life. So you get the fix and move on. :-)"
    author: "AC"
  - subject: "Re: Security through clarity"
    date: 2002-11-13
    body: "What is there to say. They advise and I follow. Why shoul I make comments?"
    author: "zelegans"
  - subject: "Re: Security through clarity"
    date: 2002-11-13
    body: "What's to say? An advisory comes out, and we pay attention. No comments needed...\n"
    author: "Richard"
  - subject: "Compilation errors with kdelibs 3.0.5 and gcc 3.2"
    date: 2002-11-13
    body: "I'm using Debian sid and I compiled kde 3.0.4 with gcc 3.2 with no problems but when I try to compile kdelibs 3.0.5 I get this errors:\n\nmake[5]: Leaving directory `/usr/src/kdelibs-3.0.5/obj-i386-linux/kdeprint/cups/cupsdconf2'\nmake[5]: Entering directory `/usr/src/kdelibs-3.0.5/obj-i386-linux/kdeprint/cups'\n/bin/sh ../../libtool --mode=link gcc  -DNDEBUG -O2     -o make_driver_db_cups  make_driver_db_cups.o -lz ../libdriverparse.a ../../kdecore/libkdefakes.la\ngcc -DNDEBUG -O2 -o .libs/make_driver_db_cups make_driver_db_cups.o  -lz ../libdriverparse.a ../../kdecore/.libs/libkdefakes.so\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QMetaObjectCleanUp::setMetaObject(QMetaObject*&)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QMetaObjectCleanUp::~QMetaObjectCleanUp [in-charge]()'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::qt_property(int, int, QVariant*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::customEvent(QCustomEvent*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::qt_invoke(int, QUObject*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::~QObject [not-in-charge]()'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::activate_signal(int)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::qt_emit(int, QUObject*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::eventFilter(QObject*, QEvent*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::staticMetaObject()'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QMetaObjectCleanUp::QMetaObjectCleanUp[in-charge]()'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::qt_cast(char const*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QIODevice::at(unsigned long)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `operator delete(void*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QIODevice::~QIODevice [not-in-charge]()'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::childEvent(QChildEvent*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QIODevice::atEnd() const'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::disconnectNotify(char const*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QIODevice::at() const'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `__cxa_pure_virtual'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::setProperty(char const*, QVariant const&)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::removeChild(QObject*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QIODevice::readLine(char*, unsigned long)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::event(QEvent*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::property(char const*) const'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QMetaObject::new_metaobject(char const*, QMetaObject*, QMetaData const*, int, QMetaData const*, int, QMetaProperty const*, int, QMetaEnum const*, int, QClassInfo const*, int)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::timerEvent(QTimerEvent*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `vtable for __cxxabiv1::__vmi_class_type_info'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::insertChild(QObject*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `KAsyncIO::virtual_hook(int, void*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::setName(char const*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `typeinfo for QIODevice'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QIODevice::readAll()'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::connectNotify(char const*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `QObject::checkConnectArgs(char const*, QObject const*, char const*)'\n../../kdecore/.libs/libkdefakes.so: undefined reference to `typeinfo for QObject'\ncollect2: ld returned 1 exit status\nmake[5]: *** [make_driver_db_cups] Error 1\nmake[5]: Leaving directory `/usr/src/kdelibs-3.0.5/obj-i386-linux/kdeprint/cups'\nmake[4]: *** [all-recursive] Error 1\nmake[4]: Leaving directory `/usr/src/kdelibs-3.0.5/obj-i386-linux/kdeprint/cups'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory `/usr/src/kdelibs-3.0.5/obj-i386-linux/kdeprint'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/usr/src/kdelibs-3.0.5/obj-i386-linux'\nmake[1]: *** [all] Error 2\nmake[1]: Leaving directory `/usr/src/kdelibs-3.0.5/obj-i386-linux'\nmake: *** [build-stamp] Error 2\n\nAlso my compiler is:\n\n/usr/src/kdelibs-3.0.5# gcc -v\nLeyendo especificaciones de /usr/lib/gcc-lib/i386-linux/3.2.1/specs\nConfigurado con: /home/packages/gcc/3.2/gcc-3.2-3.2.1ds4/src/configure -v --enable-languages=c,c++,java,f77,proto,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-java-gc=boehm --enable-objc-gc i386-linux\nModelo de hilos: posix\ngcc versi\u00f3n 3.2.1 20021103 (Debian prerelease)\n\nThanks for your atention.\n"
    author: "Josep"
  - subject: "Re: Compilation errors with kdelibs 3.0.5 and gcc "
    date: 2002-11-13
    body: "These references are all provided by the QT library, which actually is called\nsomething like 'libqt-mt.so.3.x'. So i am missing the '-lqt-mt' flag in your\nlinkage (line starting with 'gcc -DNDEBUG'). Probably the configure script\ndidn't find QT, but then it should fail with an error.\nI remember when i used to play ;c) with debian, using the qt deb's results in\na strange location for the libs and includes (libs: /usr/lib/qt; \ninc: /usr/include/qt) but all apps need to know where the QTDIR is, that holds\nsomething like $QTDIR/lib and $QTDIR/include. I solved it by creating a dir like\n'/usr/local/qt' as my actual QTDIR and within it i placed symbolic links, e.g\n'ln -s /usr/lib/qt ./lib'. Same for includes.\nIf this is all garbage forget about me posting it here, and, oh well, check that you have QT installed (at all/properly)... :cP"
    author: "/mrc/"
  - subject: "Re: Compilation errors with kdelibs 3.0.5 and gcc "
    date: 2002-11-14
    body: "I have got a similar problem with lpr driver on Slackware-8.1. The file kdelibs-3.0.5/kdecore/libkdefakes.la created is wrong, its entry dependency_libs\nmust be dependency_libs='-lqt-mt -lkdecore' ."
    author: "Andrey V. Panov"
  - subject: "Re: Compilation errors with kdelibs 3.0.5 and gcc "
    date: 2002-11-15
    body: "Thank you, now all compiled fine."
    author: "Josep"
  - subject: "Re: Compilation errors with kdelibs 3.0.5 and gcc "
    date: 2002-11-24
    body: "Josep, how did it compile fine? I am trying to do a \"compile package\" for sourcemage so kde 3.0.5 will compile automatically, and this has kept me stumped. I need a configure option or a bash/sed/etc command I can run after I expand the kdelibs tarball, or between configure and make...\nThanks a lot!"
    author: "jose bernardo"
  - subject: "Re: Compilation errors with kdelibs 3.0.5 and gcc "
    date: 2002-11-26
    body: "Expliciting, I need to know how to change the generation of kdelibs-3.0.5/kdecore/libkdefakes.la, so I can automatically make all compile when getting to the make step.\nI hope I made sense this time..."
    author: "jose bernardo"
  - subject: "KDE developers are a little naive..."
    date: 2002-11-13
    body: "In the recent versions of KDE, some interesting network programs appeared in KDE, eg the ability to share user directories via a panel applet, the network discovery facilities provided by LISA, etc...\nWhile these programs have noble goals (simplify the use of the network for users), it looks like their developers are not completely proficient with the security problems related with network servers programming.\nNetwork server programming is a hard beast, look at the number of bugs of well-known servers (e.g. sendmail, bind, etc...) - and these servers are developed by programmers with lots of expertise in that area.\nI think KDE developers are a little naive because they \"reimplement\" network server facilities within KDE, without having the necessary expertise. This immediately causes security bugs, as the recent KDE history shows...\nSo, I humbly suggest them NOT to reimplement network server from scratch; please use more tested and reliable software (e.g. apache, samba) and implement a KDE addon/GUI interface/whatever to simplify their use.\nThis is more or less the path followed by MacOsX: if you want to share a directory, the system modifies the configuration file of the Apache webserver installed on the computer. Apple did not reimplement a file server just for this..."
    author: "Federico Cozzi"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-13
    body: "Hi\n\nI totally agree with you. But I'd like to add that if you use Mandrake, you'll see that it becomes quite ridiculous because you have two ways to share files: one way provided by Mandrake and based on NFS and one based on KDE file sharing applet. So you have a good way to do that (mandrake's way) and KDE's way to do that. For the user, it's another stupid thing about Linux (people often don't know that Linux!=Mandrake!=KDE). I know this is Mandrake's fault because they should remove KDE file sharing applet. BUT, why don't KDE developers include Mandrake's NFS utility into KDE CVS ? It's GPL so it can be included. But no... we have two tools for the same thing..."
    author: "Julien Olivier"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-13
    body: "The KDE developers did not reimplement all services.\n\nFor LiSA, they just redistribute it and added a configuration module. The file sharing works via modifying the smb.conf file. For this you have to have the \"fileshare...\" tool installed, which is developped by Mandrake. And it uses samba to provide the server functions.\n\nCiao, matthes"
    author: "matthes"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-13
    body: ">  For LiSA, they just redistribute it and added a configuration module.\n\nThe distinction between \"just redistributing\" and \"including\" is blurry... \nLisa is part of KDE packages since v 2.something, is developed by a KDE developer and is used only by KDE (to my knowledge); it even has a control module. I would say it is part of KDE...\n\n> The file sharing works via modifying the smb.conf file\n\nNo it does not work this way, at least on KDE3.0. There is a panel applet (kpf, part of kdenetwork) which implements a minimal HTTP server.\n\nYou are speaking about Mandrake's KDE (modified to use samba), or KDE 3.1 which includes Mandrake's code to modify the configuration files of Samba and NFS daemon. This looks to me the best way to provide file sharing services, I'm happy that KDE adopted this way.\n"
    author: "Federico Cozzi"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-13
    body: ">There is a panel applet (kpf, part of kdenetwork) which implements a minimal HTTP server.\n\nYes, and what is this panelaplet suposed to do? Kpf is quik and easy way to temporary share some files to the net. The key here is temporary, it's meant to be set up and taken down with ease and not to run as a permanent file share service. Only look how it's implemented, a minimal HTTP server running as a panel applet. Not exactly a high performance or scalabel way to do file sharing, but quik, easy and uncomplicated as it was designed to be. To be naive is to think it's developed to replace file sharing services like NFS or samba.  \n"
    author: "Morty"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-14
    body: "Well, lisa is part of KDE, but can be used completely independent.\nI didn't find time yet to write a Gnome vfs module.\nAnd give us the chance to learn about secure programming.\nI'd say lisa is now quite secure. It's only a small project, I think it must be less than 1000 LOC. It is possible to get it secure.\nActually I'm happy the problems were found :-)\n\nBye\nAlex, the author\n"
    author: "aleXXX"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-13
    body: "> it looks like their developers are not completely proficient with the security problems related with network servers programming.\n\nDo you think apache has never had a security bug? If you do, you're a bit naive."
    author: "asf"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-13
    body: "> Do you think apache has never had a security bug? If you do, you're a bit naive.\n\nNo, the naivety is thinking that \"apache has bugs and lisa has bugs\" implies \"apache and lisa are at the same level of security\" (just to name two random projects).\nApache is an old, well tested software. The most trivial bugs have already been fixed. You can't say the same of lisa (for example).\nFor example, apache runs as user \"nobody\". You can't (easily) get root access via apache. Lisa does not have this level of security."
    author: "Federico Cozzi"
  - subject: "Re: KDE developers are a little naive..."
    date: 2002-11-14
    body: "But kpf does."
    author: "Roberto Alsina"
  - subject: "mmmmm ..."
    date: 2002-11-14
    body: "Too much eye candy and bad security ....\ni'm switched to GNOME 2 ..."
    author: "Anonymous....."
  - subject: "Re: mmmmm ..."
    date: 2002-11-15
    body: "cool. using any open sourced desktop is fine.. just dont switch to osx/windowsxp :)"
    author: "asf"
---
The KDE Project today issued two security advisories which affect KDE
versions 2.1 through KDE 3.0.4 (and also through <a href="http://dot.kde.org/1037074615/">KDE 3.1 RC3</a>).  The
<a href="http://www.kde.org/info/security/advisory-20021111-1.txt">first
advisory</a> concerns the <code>rlogin://</code> service and, for affected
KDE 2.x systems, the <code>telnet://</code> service.  The
<a href="http://www.kde.org/info/security/advisory-20021111-2.txt">second
advisory</a> concerns the <code>LISa</code> and <code>resLISa</code>
network browsing applications.  Binary packages for
KDE 3.0.5 should be available by early next week (check the <a href="http://www.kde.org/info/3.0.5.html">KDE 3.0.5 Info Page</a>); in the interim it is
recommended to disable the affected services or upgrade from the source
code or patches.

<!--break-->
<p />
The KDE Project today issued two security advisories which affect KDE
versions 2.1 through KDE 3.0.4 (and also through <a href="http://dot.kde.org/1037074615/">KDE 3.1 RC3</a>).  The
<a href="http://www.kde.org/info/security/advisory-20021111-1.txt">first
advisory</a> concerns the <code>rlogin://</code> service and, for affected
KDE 2.x systems, the <code>telnet://</code> service.  These services
are implemented via .protocol files.  On affected systems, the implementation
allows a carefully crafted URL in an HTML page, HTML email or other
KIO-enabled application to execute arbitrary commands on the system using
the victim's account on the vulnerable machine.  The
<a href="http://www.kde.org/info/security/advisory-20021111-2.txt">second
advisory</a> concerns the <code>LISa</code> and <code>resLISa</code>
network browsing applications.  The resLISa daemon contains a buffer
overflow vulnerability which potentially enables any local user to obtain
access to a raw socket if <code>reslisa</code> is installed SUID root.  The lisa
daemon contains a buffer overflow vulnerability which potentially enables
any local user, as well a remote attacker on the LAN, to obtain root
privileges.  In addition, a remote attacker potentially may be able to gain
access to a victim's account on the affected machine by using a
<code>lan://</code> URL in an HTML page, HTML email or via another KDE
application.  Links to the source code for KDE 3.0.5, which fixes these
problems, as well as patches to KDE 3.0.4 and instructions for disabling
the affected services are provided in the advisories.  Binary packages for
KDE 3.0.5 should be available by early next week (check the <a href="http://www.kde.org/info/3.0.5.html">KDE 3.0.5 Info Page</a>); in the interim it is
recommended to disable the affected services or upgrade from the source
code or patches.

