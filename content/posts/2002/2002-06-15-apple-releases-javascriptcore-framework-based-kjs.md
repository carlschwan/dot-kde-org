---
title: "Apple Releases JavaScriptCore Framework Based on KJS"
date:    2002-06-15
authors:
  - "mstachowiak"
slug:    apple-releases-javascriptcore-framework-based-kjs
comments:
  - subject: "Congratulations"
    date: 2002-06-14
    body: "Wooohoo!  Way to go Apple!  Nice compliment paid to KDE.\n"
    author: "Anonymous"
  - subject: "Change of opinion"
    date: 2002-06-15
    body: "At one point apple was persecuting KDE for having a Trash bin.  Now their using our code in their OS? That's a bit of a change of opinion on their side as to the merits of KDE.\n\n<flash forward>\nPress Release:\n\nApple Computers has announced the release of their next generation OS 11.\n\nFeatures include: Core based on BSD, as seen in OS/X.\nNew GUI: KDE has been selected as the default GUI for OS/11.  We chose this user interface its simple and efficient code.\n\n</flash forward>\n\nwell, maybe not - but one can dream.\n\nTroy Unrau\ntroy@kde.org"
    author: "Troy Unrau"
  - subject: "Re: Change of opinion"
    date: 2002-06-15
    body: "Newsflash: KDE has a Trash icon.  Still.\n"
    author: "Anonymous"
  - subject: "Bah"
    date: 2002-06-15
    body: "Try defaulting Liquid in KDE 3.1 and see how friendly Apple *really* is."
    author: "Neil Stevens"
  - subject: "Re: Bah"
    date: 2002-06-15
    body: "Keramik rather.\n"
    author: "Anonymous"
  - subject: "Re: Bah"
    date: 2002-06-17
    body: "Personal opinion. Many people like Liquid better. There's a poll on kde-look and the two are about even. Keramik is up 1% last time I looked. I personally don't like Keramik's huge widgets, and they look blocky to me. It also doesn't support mouse-hover. After using Liquid the interface seems dead without it. I expect things to light up. \n\nMost people seem to like Keramik because it's new and doesn't look similiar to MacOSX. They just want something different. But I have yet to hear someone say why it is better than Liquid based on it's own merits other than it's something different from everything else. The window manager looks much nicer but I think the Liquid widgets are much better. They are smaller, less clunky, and more interactive. \n\nBut to each his own. Liquid was always controversial but it brought a lot of cool stuff to Linux for the first time."
    author: "KLover"
  - subject: "Keramik style trolls"
    date: 2002-06-17
    body: "Heh, since when did Keramik get such a large group of trolls behind it? It seems every time someone posts that they like Liquid some troll has to pop up and say \"Use Keramik\" or something similiar. I get the feeling some people are trying to hype it as the new default interface for KDE3.1 by bashing the currently most downloaded style. Well, with 70K+ downloads I must of done something right... ;-)\n\nIf you like Keramik, that's cool! I like it, too :) I think it is turning out to be a very nice style. That doesn't mean there has to be all this trolling, tho. That is beginning to annoy me. Many people like Liquid as well - the two styles are quite different. I personally am glad KDE is adopting more advanced features I first developed for Liquid, the current default I wrote a long time ago and it's really showing it's age. Even if you don't like how Liquid works before you go around trolling remember it was the first style to support translucent menus, (other than a gradient style I never released), alphablending widgets, dynamic pixmap tile recoloring, and lots of other cool stuff. "
    author: "Mosfet"
  - subject: "Re: Keramik style trolls"
    date: 2002-06-21
    body: "I did not intend to bash Liquid!  Liquid is very fast and advanced, Keramik still alpha."
    author: "Anonymous"
  - subject: "Re: Keramik style trolls"
    date: 2002-06-23
    body: "\nHey Mos, any chance that Liquid will become the new default theme for KDE?\n\nCheers and great work.\n\n"
    author: "Pengo"
  - subject: "Re: Keramik style trolls"
    date: 2002-06-23
    body: "Nope. When I first made Liquid I tried to contribute it to KDE CVS since it was by far the most advanced Linux style anywhere at the time. It was rejected because some people saw the round glossy buttons and immediately got scared of Apple. Of course, by now everyone who has used Liquid knows it doesn't look or operate like MacOSX, but people had a knee-jerk reaction and without ever using or contributing alternate artwork rejected it from even being included in CVS. This is one of the reasons I decided not to develop for KDE CVS anymore.\n\nI of course continued to develop it outside of KDE and it became by far the most popular KDE theme, (over 80,000 downloads on KDE-Look alone, not couting people who downloaded it directly from my page). The other developers then decided to write a new style for KDE3.1 with Liquid's advanced features but with different artwork, and that's how Keramik came about."
    author: "Mosfet"
  - subject: "What about QuickTime? Heh..."
    date: 2002-06-15
    body: "<a href=\"http://www.dbhome.dk/antialias/konqueror-quicktime.png\">http://www.dbhome.dk/antialias/konqueror-quicktime.png</a>\n<p>\n<a href=\"http://www.dbhome.dk/antialias/konqueror-quicktime2.png\">http://www.dbhome.dk/antialias/konqueror-quicktime2.png</a>\n\n"
    author: "antialias"
  - subject: "Re: What about QuickTime? Heh..."
    date: 2002-06-29
    body: "What theme/style are you using? and what WM style? "
    author: "Dude"
  - subject: "Re: What about QuickTime? Heh..."
    date: 2002-06-29
    body: "It's Keramic, no idea about the style.  Or the icons, which are what interest me...\n\nIncidently, will Keramic ever be dynamic so the color can be changed?  I have a mild aversion to powder blue.  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: What about QuickTime? Heh..."
    date: 2002-06-30
    body: "Keramik decoration and Liquid (Mosfet's) style."
    author: "Theme/style"
  - subject: "Contribution..."
    date: 2002-06-15
    body: "From the announcement :\n> All Apple changes will be contributed back to the mainline version.\n\nIf this is real, that's pretty cool !"
    author: "Aur\u00e9lien"
  - subject: "license of KJS ?"
    date: 2002-06-15
    body: "KJS is under a BSD-like license, LGPL or GPL (or even artistic) ?"
    author: "Capit Igloo"
  - subject: "Re: license of KJS ?"
    date: 2002-06-15
    body: "LGPL"
    author: "domi"
  - subject: "Re: license of KJS ?"
    date: 2002-06-17
    body: "I'm surprised that they're using anything GPL in their OS.  They generally try very hard to avoid that, instead preferring BSD style licenses.  I guess they must've considered kjs vastly superior to anything else otherwise they'd have chosen an alternative."
    author: "anon"
  - subject: "Re: license of KJS ?"
    date: 2002-06-17
    body: "LGPL, not GPL... I think by the way they don't really say when they use BSD style stuff... they can use it without any problem.  The fact that they use LGPL libs, in my opinion shows they want to work on their open source reputation by giving back to the community... which is _very good_ of course :)"
    author: "domi"
  - subject: "Partnership?"
    date: 2002-06-15
    body: "\"...and is provided as Open Source as part of our partnership with the KDE community.\"\n\nSo does this mean that Apple has some kind of formal partnership with KDE now? Is this just take, take, take, or does it mean that KDE will be allowed to include Aqua-like styles in return, for example? ;)\n\n(Yes, I realize their changes to KJS will be donated back to KDE, but this should be expected anyway :)\n"
    author: "Haakon Nilsen"
  - subject: "Re: Partnership?"
    date: 2003-01-20
    body: "uh.... NO!  That would be to use a direct analogy like Rolls Royce letting GM put the RR grill on Cadillacs just because the got their engine/tranny combo from them.  Not trying to use a perjoritive comparison since I LIKE KDE!!!!  But it is the nearest real world comparison that my brain came up with at 12.37 am..\n\nThe look of the GUI is like their grill it is what visually sets it off from other \"cars\" on the road so to speak.  It would dilute their brand, harm their business, share holders, and employees.  \n\nIf people really want that goooey... buy the dang machine!  You don't have to buy a new one (they are pricey.. IMHO worth it but not everyone feels that way and thats cool), the prices on used machines that are capable of running OSX are pretty dang reasonable more so if you use Xpostfacto/unsupported x.\n\nSure there may be some perceived performence issues but depending upon what you are doing with your machine they may not be that noticeable.  \n\nAnd it does two things:  Rewards someone for their work (we all like to be paid in most cases for making something), and two, unless you have built your own Intel/AMD system saves you the hassle of trying to get the MS license money back for an OS that you the Linux user are NOT going to use anyway.\n\nHeck if people would save the cash and pony up the money to get the GUI that everyone wants (even the XP people see the aqua-xp sites out there that go as far as putting the Apple in the upper left corner and use an \"about this mac\" info app), the cost of the systems just might go DOWN..... Apple knows that their customers want to pay less for them and from what I have heard thru the channels... would like to charge less!  But there is a point that you can't drop below per unit if you do not have high volumes of sales if you want to recover your costs and make a profit.  It's like the B2 bomber to use a somewhat flawed analogy... you can buy like 100 of them for a billion a pop or you could buy 100 billion for a dollar a pop.\n\n\nOr to put it another way:  Every home built and Apple system out their denies M$ their lifes blood CASH.  Since IIRC from the DOJ testimony... apple accounts for between 24-33% of MS revenues ( on a 3% market share..RIGGGGHTTT), and another 30-45% is made from OS and software bundle revenues.\n\nIf you really hate MS vote with your wallet."
    author: "Chris c"
  - subject: "Oh really?"
    date: 2002-06-15
    body: "Bah is all I say. To be honest, I don't give jack about what Apple thinks of KDE, compliments or not. The question is - are they going to give back, or are they just going to take?\n\nSo far, their record has not been great. They took most of UNIX and produced with it Darwin. Darwin is open source, fine except doesn't the world already have enough free UNIXen? There's Linux, BSD and now Darwin. I haven't seen anyone just using Darwin yet, there's no point. What makes OS X unique is all the stuff they keep to themselves.\n\nNow they're at it again, this time they are taking KJS. Okay, so if they make any changes, they'll have to give them back as patches, but they will of course be unusable - the changes will be to make kjs compatible with OS X, not something that concerns most people here much. I'd be happy if for instance as a contribution back to the community, they added support for say JavaScript 2 to kjs, but I haven't seen any mention of this or anything similar, and I seriously doubt it'll happen.\n\nI'm getting somewhat annoyed with Apple now. I have no problem with them using open source software, but so far they haven't contributed back anything much of worth (as far as I can tell)."
    author: "Mike Hearn"
  - subject: "Re: Oh really?"
    date: 2002-06-15
    body: "You should Read Before You Write (TM).\ncheck out \nhttp://developer.apple.com/darwin/\nfor the open source projects Apple is engaged in.\nI don't really know all that much about Apple, and I don't like what they have\ndone with their UI ( not working with KDE, specifically, as well as keeping it\nclosed source ) either, but they do seem to be engaged quite thoroughly in open\nsource stuff.\nbye\ndomi\n"
    author: "domi"
  - subject: "Re: Oh really?"
    date: 2002-06-17
    body: "Wierd, I couldn't get through to the dot for days.... anyway.\n\nYeah, I do read before I write actually, and I know what Apple are up to in the open source community. Let's see what their contributions are shall we? (from their website)\n\n-  \"Darwin is the core of Mac OS X. On its own, it is a complete BSD-derived operating system.\"   Darwin is basically YAUK (yet another unix kernel). We already have Linux and all the *BSDs. Why exactly do we need another?\n\n- \"The Streaming Server includes everything you need to create multi-platform server solutions that include QuickTime Streaming capabilities.\"   A streaming server for a proprietary media format for which there are no open source players. Way to go Apple.\n\n- \"Common Data Security Architecture is the basis for security features in Mac OS X\"    It's for OS X, which is a proprietary OS, so this is of no use to us.\n\n- \"OpenPlay is a cross-platform network abstraction layer designed to simplify the task of creating programs which communicate across multiple computers.\"    A networking abstraction library. Check out SourceForge/FreshMeat, they're ten a penny.\n\n- \"HeaderDoc is a tool for generating API documentation from comments embedded in C, Objective-C, and C++ headers\"    A header generator! Woohoo!\n\n- Some documentation\n\n- And last, but by no means least ..... Chess.app! BRING IT ON! ;)\n\n\nSeriously, that's not a very impressive list. The only major \"contribution\" has been Darwin, bascially a version of BSD with a microkernel, and opinion is split on whether that's a good thing or not. It doesn't have the hardware support of Linux either. The rest is all just backing up their own products! \n\nDon't get me wrong, I have no problem with companies using open code, as long as they contribute something back (js2 support would be good in this case). Apple aren't doing that, and as far as I'm concerned that makes it moral, if not legal, theft."
    author: "Mike Hearn"
  - subject: "Moral theft? Not even close - Re: Oh really?"
    date: 2002-06-17
    body: "You're certainly entitled to your dismissive opinion of Apple's relatively modest Open Source initiatives. But to suggest that Apple's use of a licensed piece of software in accordance with the terms of that license, and in a manner which publicly praises the creators of that software, hardly strikes me as moral theft.\n\nYou said, \"I have no problem with companies using open code, as long as they contribute something back...\"  \n\nCompanies that use LGPL software have NO obligation to contribute in kind, over an above the terms stipulated by the license agreement. \n\nExcerpt from http://www.gnu.org/licenses/why-not-lgpl.html\n\n\"Using the ordinary GPL is not advantageous for every library. There are reasons that can make it better to use the Library GPL in certain cases. The most common case is when a free library's features are readily available for proprietary software through other alternative libraries. In that case, the library cannot give free software any particular advantage, so it is better to use the Library GPL for that library.\"\n\nIf you make something and explicitly give it away, how can you call someone a thief for accepting your gift? \n\nApple's own development efforts do not harm KDE in any way, while Apple's acknowledgment of the quality of KDE's software is a significant PR coup for KDE. And Apple is putting a KDE friendly platform in millions of houses across the US. It's possible that more people will be acquainted with KDE because of Mac OS X (via Fink) than through other means.\n\nI just don't see the harm here. If I'm missing something, please correct me.\n"
    author: "Mike McCafferty"
  - subject: "Re: Moral theft? Not even close - Re: Oh really?"
    date: 2002-06-17
    body: "Perhaps theft was too strong a word. My point was that the open source community was not built by people taking code and giving back minor modifications because they had to. It was built by people who said - this is useful, so I'll give something new and of worth back.\n\nWhat rankles is exactly that - Apple are doing only the minimum of what they are legally obliged to. That's not exactly in the open source spirit is it?\n\nOh, and as for OS X being KDE-friendly, I think that's taking things a bit far. There is a version of KDE for Windows, does that mean Windows is KDE friendly? Or that people will use it? I seriously doubt that. Basically the main selling point of the Mac is its user interface, the fact that KDE can run on it is a cool achievement technically, but I have a feeling it'll be a cold day in hell before more people use KDE on a Mac than on Linux.\n\nThe harm is caused by Apples attitude more than anything else. Their attitude is \"we'll take what we want and that's where it stops\". They've basically used a truckload of open source (volunteer developed) software for their own profit, and not given anything of much worth back in return. Yes yes, I know the people who wrote the BSDs knew that might happen and are happy with companies making a profit from their work, that's not what I mean. What I mean is that it should be a give-take relationship, not take-take.\n\nLook at RedHat - they are making cash from taking Linux and selling it. They're making money off of free software, and in return they have given back. RPM is widely used. SuSE employ lots of KDE hackers, as do Mandrake. In fact, most Linux companies give back stuff to the community. Where is Apple in all this, considering that they have benefitted from the open source movement more than most Linux companies have?"
    author: "Mike"
  - subject: "Re: Oh really?"
    date: 2002-06-17
    body: "ok,\nyou're right,\nin that they haven't really done the things the open source community needs,\nand their only objective with their open source efforts seems to be to get easy\naccess to good software (KJS most notably), and to advance their market position\n(the streaming server would be a good example)\nI think though that you can't really expect much more from Apple, they have to make\nmoney, and can't afford that Linux gets too much of the advantages that MacOS has,\nsince Linux has much potential to become serious competition for them too.\n\nI do think that their recent adoption of some open source software is good for the\ncommunity as KJS will hopefully gain from Apple's input.\nDidn't they also adopt CUPS, the GNU GPL printing system also used by KDE.  If that's\nthe case, then the situation of linux printer support might improve a lot since it shouldn't\nbe too easy to port CUPS printer drivers for MacOS to linux (correct me if i'm wrong here),\nor even provide a MacOS emulation layer...\n\nbye\ndomi\nPS: if i don't answer here anymore, it's because i have gotten sick of this stupid web\ninterface, i very much prefer newsgroups..."
    author: "domi"
  - subject: "Re: Oh really?"
    date: 2002-06-17
    body: "I understand that Apple has to make money, and have no problem with that, although I wish they didn't have to make all their stuff proprietary to do so.  Really, I don't think there's much of an excuse - if you use open source software to help make a profit, you give back. RedHat manage it, so do many other companies.\n\nWe'll see how much KJS gains from Apples input. If they do contribute something significant, then I'll eat my words, but their past performance indicates they won't - for instance there is a mighty impressive 1 bug reported by/assigned to an @apple.com address."
    author: "Mike"
  - subject: "KJS avaible as standalone package ?"
    date: 2002-06-17
    body: "I wonder if KJS is avaible as a standalone package already ? While evaluating scripting languages I found KJS way simpler and better than Mozilla's SpiderMonkey, so it would really be nice to see a KJS package which other projects could use without depending on KDE..."
    author: "Marc Haisenko"
  - subject: "CVS"
    date: 2003-06-12
    body: "Check CVS ... you can use it standalone ... also check out the bindings subdir for a surprise"
    author: "Me"
  - subject: "Jumping the gun."
    date: 2002-06-24
    body: "All of you who think that Apple just takes and doesn't contribute are jumping the gun a bit IMHO. Apple does plenty of things good for open-source community... adopting it at all for Mac OS X is just one thing. Do you have any idea what that move did for open-source? There were ass tons of people who didn't even know what open-source was until Apple said thye were going to support it.\n\nYou should also take a look through the Darwin bug tracker to see what changes they're making to the Darwin source tree before you accuse Apple of stealing... they are making plenty of changes that get sent upstream.\n\nJava is another that comes to mind... Sun liked Apple's code so much they decided to include it in a new version of Java coming soon. That isn't necessarily open-source, but it doesn help the community in many ways. More streamlined and faster code == good thing.\n\nJust because Apple has a history of being a big closed-source meanie doesn't mean that things can't/won't change.\n\nGive them a chance.\n\nzero"
    author: "michael"
---
<A href="http://www.apple.com/">Apple</a> has released the source to 
<a href="http://www.opensource.apple.com/projects/others/JavaScriptCore-0.2.tar.gz">
JavaScriptCore</a>, a JavaScript framework based on kjs. It will be used by various applications in upcoming Mac OS X releases. <i>"JavaScriptCore is a private API which may be used by future Mac OS X  
applications such as Sherlock, and is provided as Open Source as part  
of our partnership with the KDE community. JavaScriptCore will be used  
by Sherlock in an upcoming O/S release, and possibly by other Apple  
software in the future. Apple chose kjs as the basis of its JavaScript  
technology for its simple and efficient code."</i> The full announcement is <a href="http://www.opendarwin.org/pipermail/kde-darwin/2002-June/000034.html">here</a>.
<!--break-->
