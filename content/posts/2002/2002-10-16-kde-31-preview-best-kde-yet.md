---
title: "KDE 3.1 Preview: The Best KDE Yet"
date:    2002-10-16
authors:
  - "Dre"
slug:    kde-31-preview-best-kde-yet
comments:
  - subject: "Kmail plugins"
    date: 2002-10-15
    body: "How do I use KMail plugins? Where are they??"
    author: "Tuxie"
  - subject: "Re: Kmail plugins"
    date: 2002-10-16
    body: "right now the only plugins in kmail are cryptography plugins. these plugins enable S/MIME support in addition to the built-in inline-PGP/GPG support. more info can be had at: http://www.gnupg.org/aegypten/development.en.html\n\n3.2 may bring more plugin goodness for KMail."
    author: "Aaron J. Seigo"
  - subject: "Re: Kmail plugins"
    date: 2002-10-16
    body: "Actually the two cryptography plugins enable PGP/MIME (signing/encryption of attachments) support and S/MIME support in addition to the built-in inline-PGP/GnuPG support (which can't sign/encrypt attachments).\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "terrific!!!!"
    date: 2002-10-15
    body: "I want my KDE 3.1!!!!!!  *drool*"
    author: "KDE User"
  - subject: "Desktop sharing not for switching work-stations"
    date: 2002-10-15
    body: "Actually Desktop Sharing has not been designed for those who use several workstations. That's a different use case that I hope to take care of in 3.2. It has been designed for support issues or just to show something to your co-worker who's sitting in another office...\n\nbye...\n"
    author: "Tim Jansen"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-15
    body: "Actually, I find it quite useful as a remote control, althoguh it does hurt X pretty badly (using an older version). Any plans to reduce cpu utilization and further enhance this use?\n\nthanks\n-js"
    author: "js"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-15
    body: ">>Any plans to reduce cpu utilization and further enhance this use?<<\n\nThis is not possible without a X11 extension that would notify me of changes on the screen.\n\nbye...\n"
    author: "Tim Jansen"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-16
    body: "I guess you try updating just the active window (or at least updating it more often than everything else). Alternatively you could try updating a rectange around the mouse cursor.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-16
    body: "No, I scan the whole screen. The scanning is triggered by a idle QTimer. In each run I test every 32nd line. When I detect one or more changes on a scanline, I'll fetch these parts as 32x32 blocks, get the height of the changed region and then prepare an update (adjacent modified blocks will be merged). The problem is that it takes a long time to scan the complete screen, so the server scans permanently.\nPreferring an active window or the mouse pointer, like the Windows VNC server does, may improve the latency, but it will not lower the CPU load.\n"
    author: "Tim Jansen"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-16
    body: "What about the VNC XFree86 extension at: http://sourceforge.net/projects/xf4vnc?\n\n--A.C."
    author: "Anonymous Coward"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-16
    body: "There are tw problems with xf4vnc: \n- the XFree drivers need to be modified for that. xf4vnc has not been ported to all GFX cards, for example there is no support for the nv driver. And with binary drivers, like the nvidia driver or the new Radeon drivers,  using xf4vnc is not possible at all. Nor will it work on older XFree versions or x11 servers on commecial OSes. So I would still need to maintain the original code\n- it's very inflexible. I already have extended the VNC protocol (to transmit cursor movements), and I plan a number of further changes for authentication and better integration with KDE. I can't do this when the protocol stack resides in the X11 server\n\nA X11 extension that reports screen changes is probably the easier and more flexible solution for the problem."
    author: "Tim Jansen"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-15
    body: "The article was pretty light on the details.. and tooted KDE 3.2 even.. which inherently downplays this release a bit. It would be nice if there was a more in-depth look at the changes from 3.0 to 3.1. Eg. dcop enhancements, updates to each application, etc. Something that compels users to upgrade with an argument other than \"it looks prettier!\" \n\nNot that I'm complaining though, this is better than nothing!\n\n\n\n"
    author: "Joe"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-15
    body: "It's called a teaser, genius.\n"
    author: "KDE User"
  - subject: "Re: Desktop sharing not for switching work-stations"
    date: 2002-10-16
    body: "Read this for \"a more in-depth look at the changes from 3.0 to 3.1\":\nhttp://developer.kde.org/development-versions/kde-3.1-features.html\n\nAt least the items which are marked as finished and probably most of which is marked as in progress will be in KDE 3.1.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Video Thumbnail Generator?"
    date: 2002-10-15
    body: "Where will I find this goodie? Couldn't find it in 3.1 beta2."
    author: "Roger"
  - subject: "Re: Video Thumbnail Generator?"
    date: 2002-10-16
    body: "open konqueror in file browser mode. go to the View menu, select Preview and you should see an option for Video. select it and then view a directory with video clips in it. you'll get a little thumbnail of the video!"
    author: "Aaron J. Seigo"
  - subject: "Re: Video Thumbnail Generator?"
    date: 2002-10-16
    body: "In my view->preview menu there is no Video to select. There is Pictures and Images though. Although I selected everything I didn't get any Videothumbnails. Probably a problem of the SuSE-rpm's I installed or with some config from my old KDE 3.0.1?\n\nI'll wait for 3.1 final then.\n\n"
    author: "Roger"
  - subject: "Re: Video Thumbnail Generator?"
    date: 2002-10-16
    body: "shouldn't be a config issue, but it could be something to do with the SuSe RPMs... the video preview is part of kdemultimedia, in case you didn't install all the kde packages."
    author: "Aaron J. Seigo"
  - subject: "Video Thumbnails BROKEN in 3.1beta2"
    date: 2002-10-22
    body: "...in fact, I only just fixed it today. So I'm afraid you'll have to wait for RC1, or revert to beta 1 :)"
    author: "Simon MacMullen"
  - subject: "Could have been more in-depth..."
    date: 2002-10-15
    body: "The article was pretty light on the details.. and tooted KDE 3.2 even.. which inherently downplays this release a bit. It would be nice if there was a more in-depth look at the changes from 3.0 to 3.1. Eg. dcop enhancements, updates to each application, etc. Something that compels users to upgrade with an argument other than \"it looks prettier!\" \n\nNot that I'm complaining though, this is better than nothing!\n\n"
    author: "Joe"
  - subject: "Re: Could have been more in-depth..."
    date: 2002-10-15
    body: "It's called a teaser, genius.\n"
    author: "KDE User"
  - subject: "Still KDE 1 look ..."
    date: 2002-10-15
    body: "Look at the colour preview, it *still* looks like KDE 1. How hard can it be to adapt it to the current window decoration?\n\nhttp://static.kdenews.org/mirrors/qwertz/kde31alpha/s13.png\n\nReported long ago as KDE bug #32101 ... It's not a big deal, I just think it's getting a bit embarrassing for KDE."
    author: "Gaute Hvoslef Kvalnes"
  - subject: "Re: Still KDE 1 look ..."
    date: 2002-10-15
    body: "But it's cute and historical!  "
    author: "KDE User"
  - subject: "Re: Still KDE 1 look ..."
    date: 2002-10-18
    body: "Your grandmother is \"cute and historical\", this thing out loud sucks!"
    author: "Anonymous Coward"
  - subject: "very nice"
    date: 2002-10-15
    body: "Good work from the PROMO site that is partly sponsored by the League. KDE 3.1 is lookin' very good."
    author: "anonymous"
  - subject: "Mirror site..."
    date: 2002-10-16
    body: "I put up a mirror (<a href=\"http://www.ofb.biz/mirror/kde31preview/\">http://www.ofb.biz/mirror/kde31preview/</a>) of the piece on our server at OfB.biz. Hopefully that will help since the promo.kde.org server is currently swamped from all the /.ers coming to it.\n<p>\n  HTH,<br>\n    Tim"
    author: "Timothy R. Butler"
  - subject: "KAddressBook!"
    date: 2002-10-16
    body: "KDE3.1 (beta2) is great; no doubt about it.\nBut KAddressBook is a usability disaster. What happened?"
    author: "Happy KDE User"
  - subject: "Read this: http://hiro-tan.org/switch/"
    date: 2002-10-16
    body: "<a href=\"http://hiro-tan.org/switch/\">LOL!</a>"
    author: "me"
  - subject: "Re: Read this: http://hiro-tan.org/switch/"
    date: 2002-10-16
    body: ":-)))"
    author: "Thorsten Schnebeck"
  - subject: "Re: Read this: http://hiro-tan.org/switch/"
    date: 2002-10-16
    body: "\"After eight years as a Gnome owner\"\nWhat is she talking about?"
    author: ".ac"
  - subject: "Re: Read this: http://hiro-tan.org/switch/"
    date: 2002-10-16
    body: "This is Microsofts Mac-to-Windows campaign. Seems they substituted Gnome for Mac and KDE for Windows. Pretty clever. :)"
    author: "ac"
  - subject: "Re: Read this: http://hiro-tan.org/switch/"
    date: 2002-10-16
    body: "Don't take this serious. Compare it with this:\nhttp://216.239.53.100/search?q=cache:JmwQcVoG-ucJ:www.microsoft.com/insider/opsystems/windowsxp_setup.asp+windowsxp_setup.asp&hl=en&ie=UTF-8\n\n(((-:\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Read this: http://hiro-tan.org/switch/"
    date: 2002-10-16
    body: "That's a weird joke. Strange humour."
    author: "KDE User"
  - subject: "Still no Xft2 & fontconfig... :("
    date: 2002-10-16
    body: "Qt/KDE need to get Xft2 and fontconfig support as soon as possible. I recently switched from Debian to Redhat 8.0 *only* because of such a support, fontconfig makes font support/management so much better, better than what Windows have. Just imagine the world where KDE/Gnome/OpenOffice/Mozilla have unified font management infrastructure! Let's conquer the desktop world!\n"
    author: "Zdgrom"
  - subject: "Re: Still no Xft2 & fontconfig... :("
    date: 2002-10-16
    body: "That's strange.\nThere is nothing in the world which could make me switch from debian to Red Hat."
    author: "Lenny"
  - subject: "Re: Still no Xft2 & fontconfig... :("
    date: 2002-10-16
    body: "Of course if you don't use Gnome, Open Office or Mozilla then you don't care about a unified font configuration..."
    author: "whatever"
  - subject: "Incorrect.  Qt3.1 supports Xft2/fontconfig."
    date: 2002-10-17
    body: "Taken from the Qt 3.1beta 1 changes file (http://www.trolltech.com/developer/changes/3.1beta1.html):\n\nQFont on X11\n\nImprovements: Safe handling of huge font sizes. Added support for the new Xft2 font library on XFree-4.x.\n\nKDE 3.1 is expected to be using Qt 3.1, so KDE 3.1 will use Xft2 if available.  QED."
    author: "My Left Foot"
  - subject: "Re: Still no Xft2 & fontconfig... :("
    date: 2002-10-18
    body: "Switching from one DISTRO to another DISTRO because of a library option??? That seem rather bizarre to me.  Just install the library yourself.  It's not like you have to use their binary packages and are not allowed to compile your own stuff...  I've been using the same base install for about 3 years now (slackware 7) with practiclaly everything updated now from a source compile.  You have control over your install once you've installed the distro; take advantage of it."
    author: "Eradicator"
  - subject: "KDE && 800x600 == bad"
    date: 2002-10-16
    body: "Am I the only one that is noticing a rather annoying trend in KDE development. I mean, if you run your desktop in 800x600 resolution you will often get into trouble because more and more KDE apps fail to work in 800x600. Including sometimes even konqueror itself. You get missing buttons, buttons out of reach. We don't all have the ability to use such high resolutions!"
    author: "IR"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-16
    body: "I'm noticing a similar trend.\n\nKDE should take a break from fancy graphics and concentrate on the real issues at stake like usability and OS integration. The work done by the Gnome team on these points has been very impressive lately. They are definitely \"getting it\" and we're not. (And don't get me started on the mostly cosmetic changes made by the \"Usability Team\"!)\n\nDo we want KDE to be the prepubescent geek's desktop environment or do we want a solid, usable development framework others can build upon and work with?"
    author: "Grumpy"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "go on, start with the \"mostly cosmetic changes made by the 'Usability Team'\". i'd love to hear it. perhaps you don't know half of what the usability project has done. or perhaps you do and it just isn't up to your personal expectations, in which case we'd love to have you help out because the only way we're going to get faster at addressing all the issues is with more bodies.\n\nas for the general 800x600 issues, there are a couple of devels who keep an eye on this and do try to make things works. for instance, kmail has a couple of bad dialogs. these will be fixed in 3.2 (didn't get to those ones in 3.1)\n\nthankfully, the overwhelming majority of people run resolutions higher than 800x600 these days, so this isn't a show stopper, just something we need to be vigilent about.\n\nas for \"KDE should take a break from fancy graphics\", please see: http://kdemyths.urbanlizard.com/viewMyth.php?mythID=51"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: ">>>go on, start with the \"mostly cosmetic changes made by the 'Usability Team'\". i'd love to hear it.\n\nActually I am very impressed with their efforts. Perhaps screen resolutions should get higher testing priority.\n\n>>>as for the general 800x600 issues, there are a couple of devels who keep an eye on this and do try to make things works\n\nSo this means a trent can be noticed where low screen resolutions seem to be more and more ignored by KDE developers. Just because your making open source software, does not mean you should start to ignore lower resolutions.\n\n>>>thankfully, the overwhelming majority of people run resolutions higher than 800x600 these days, so this isn't a show stopper, just something we need to be vigilent\n\nDevelopers do love ALL their audience I hope.\n\nActually I joined KDE with a project as a developer and the first I mentioned was this very issue of screen resolution. So what I say is meant to be positive. I just think its fair to say that a project like KDE seems to allow developers to get more lazy about certain aspects. If your application is in the offcicial KDE CVS tree you should at least take simple resolution matters serious. For your code will shape the overall image of the official KDE package.\n"
    author: "IR"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "> Perhaps screen resolutions should get higher testing priority.\n\nit isn't the testing (we know pretty much every part of KDE that sucks at 800x600) as much as it is those who can and will fix the problems.\n\n> Just because your making open source software, does not mean you should\n> start to ignore lower resolutions.\n\nof course not. it's just hard when few devels run it at low resolutions. it's easy to forget. which is why we so need those people who keep an eye on those sorts of Q&A things.\n\n> Actually I joined KDE with a project as a developer and the first I\n> mentioned was this very issue of screen resolution.\n\nexcellent, so i suppose you are fixing the problems you come across? thanks!\n\n> just think its fair to say that a project like KDE seems to allow developers > to get more lazy about certain aspects.\n\nKDE devel doesn't work like that (\"hey! do this now!\"). it works more like what you are expressing personally: \"i don't like this, or i think it should be this way. i'm going to fix it.\" this allows for much greater personal satisfaction and higher quality work in each person's area of interest.\n\nwe just need more people like you who will not only notice a problem but then dive in a fix it.\n\n> For your code will shape the overall image of the official KDE package.\n\nindeed."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "There is the issue of 'not forcing rules' upon the developers. But for some things, these lacking rules allow packages which are included in the official package to have flaws which do create - to some extend - bad image, where a simple ruleset could have prevented this alltogether from the very beginning. \n \nSome enforced rules don't need to be considered 'bad' rules, as I cannot see the harm in forcing a development team to consider for example resolution issues. \n \nSuch a simple rule (and there could be more) would be relatively easy for a development team to follow. This guarantees that if you do not comply to the most general of usability issues, your application would potentially break the most basic of KDE usability issues and should therefor better not (yet) be allowed in the official KDE package. \n \nAnd community based as KDE is, it could even be the community deciding the most basic rules all official apps should at least comply to. This could prevent common devilish details to appear in our beloved common desktop creation. \n \nThe problem is some developers have not the greatest care about interface or for example screen resolution. But if they want their application to be shared with the official KDE CVS, the least the community and all your other fellow developers may expect is that you comply to official KDE rules. This would introduce some consistency, and remove these ever returning issues  at the same time. \n \nI know this is all theory but perhaps it is a theory that has some basic truth in it.\n"
    author: "IR"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "there are basic rules and guidelines that all KDE apps in CVS should follow, and apps are not allowed in until they meet them. i'm refering to the UI guidelines available on developer.kde.org, of course. making sure that the apps follow those guidelines is the job of every KDE developer. if an application doesn't meet those standards, then it should indeed be removed or even better: fixed. where this currently falls down on occasion is when developers either aren't aware of or don't really care about these issues.\n\nthis is one of the goals of the usability project: to educate and sensitize. and i think it is safe to say that we are making head-way. developers seem to be more conscious about those things.\n\nof course, we then have people like stephan binner and ryan cumming who often comb through the UIs of apps fixing style guide issues. KDE can always use more \"UI janitors\" who does similar things. it isn't a glorious job, but it is very useful.\n\ninterestingly, there is a KDE widget style in kdenonbeta called scheck that, when used (e.g. mykdeapp -style=scheck) highlights possible style guide violations. very cool little addition and i hope to see it go into kdesdk one of these days. it makes finding various style guide violations much easier. it doesn't catch 100% of all violations, of course, but it sure does help."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "I read almost all posts to the usability mailing list and keep an eye on the code so I do have a pretty good idea of what goes on \"under the hood.\" Most of the work done by the \"Usability Team\" is based on speculations about what users want and need. That's my problem with it and that's why I say that most changes made so far have been \"cosmetic\" in nature.\n\nHere's what needs to happen to really improve KDE's usability:\n\n1 - We need to determine and agree upon what KDE is going to offer its users. That means selecting basic services that need to be held to higher usability standards than whatever is determined to be non-essential. Look for comparison at what Apple is doing with its i* applications (iSync, iCal...). They've determined that all Mac users should have access to a small set of utilities which Apple will distribute itself. Third-party vendors can offer whatever they care to put out on the market but Apple can ensure that its customers have access to usable applications that do what most of them want to do. It looks to me like KDE's official set of packages contains so many applications that it has prevented the \"Usability Team\" from concentrating on the essential bits and from forming a clear picture of what KDE should be(come).\n\n2 - We need to understand how real people use these essential services (in KDE or other environments) and define goals for applications providing these services. These goals should correspond to what has been reported from observing actual users at work and not from \"most users do this, most users do that\" messages flying back and forth on the usability list.\n\n3 - We need to guide developers in their implementation of these goals. This means conducting user tests of those refocused applications and correcting small usability problems when they creep up as development is taking place.\n\nRight now, the \"Usability Team\" is stuck on a half-assed version of number 3. Without number 1 and 2, it's pretty much all BS. We can change things here and there but KDE as a whole will never become a usable environment.\n\nThink about how much care goes into designing KDE's software infrastructure and contrast this to the current \"usability engineering\" process. KDE's core developers have a vision of what KDE should be technically. KParts, DCOP... all this stuff didn't just design itself. Now I'm asking: where's KDE's vision of a _usable_ \"graphical desktop environment for Unix workstations\"?\n\n(still) Grumpy"
    author: "Grumpy"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "> I read almost all posts to the usability mailing list \n\nthis is a good first bit; note that a lot also happens on IRC and on other KDE lists as well.\n\n>  keep an eye on the code so I do have a pretty good idea of what goes on\n> \"under the hood.\"\n\ndo you contribute at all? sorry, i just don't know who you are...\n\n> Most of the work done by the \"Usability Team\" is based on speculations\n\nSOME of the work has been based on speculation. much of it is based either on well known principles (thanks to those who have been doing research for years in the field of HCI) or through/with actual user testing. yes, i actually test changes on users. imagine that.\n\n> 1 - We need to determine and agree upon what KDE is going to offer its users\n\nthat has already been done outside of the usability project itself. we have no need to reinvent that weel.\n\n>  It looks to me like KDE's official set of packages contains so many\n> applications that it has prevented the \"Usability Team\" from concentrating\n> on the essential bits and from forming a clear picture of what KDE should\n> be(come).\n\nit's a combination of two things: the usability project is young. we started on small things to get our feet and build some operation structures. we quickly moved up the ladder, however. take a look at the differences between kicker in 3.0 and 3.1 to see what i mean.\n\nthe other challenge is that all of KDE (and as you note, that's a big \"all\") is new territory from the perspective of usability. we can't simply tear into even all of the core pieces of KDE immediately since we lack the bodies to do so and because KDE is so huge. we're eating an elephant here. we need to do so one bite at a time. join the buffet and it'll get done that much faster.\n\n> We need to understand how real people use these essential services (in KDE\n> or other environments) \n\nindeed, we need more user testing. this will be an ongoing thing. we had one article written up during the 3.1 devel period, it would be nice to see that doubled in 3.2. additionally, some of us do test on a rather regular basis applications / interfaces that are being worked on with Real Life Users.\n\n> and define goals for applications providing these services.\n\nthe goals are largely defined for us by the feature set implemented / to be implemented by the maintainers/developers of the app. we also have a style guideline, which the usability project is tweaking and adding to over time in response to testing and discussion.\n\nbtw, i wouldn't use Apple's current software track as an example of HCI in action. MacOS X is the least usable MacOS ever.\n\n> We can change things here and there but KDE as a whole will never become a\n> usable environment.\n\nso we need to change everything everywhere instantly to a harmonious end or else not at all? c'mon. we have only so many people and a huge area to cover. it will take time. again, please look at kicker and compare between 3.0 and 3.1. now imagine that this gets done to all parts of KDE.\n\nthe more people we have actually doing things the quicker it will go. which is why helping existing developers be more aware of usability concerns is so important. people whining off to the side about how slow its going doesn't help make it go faster, though.\n\n> where's KDE's vision of a _usable_ \"graphical desktop environment for Unix\n> workstations\"?\n\nwe have a style guide, we have years of research and dozens of books/papers to draw upon, and we have the KDE technologies to work with. i think you may be confusing the usability project with a project who's goal is to perform R&D into neat and cool new interfaces that do things we don't have yet. this would be a lot of fun and maybe we'll get there one day. but we have a lot of work to do with the existing KDE code base first. there is no point in creating new things in the name of usability when the existing is crying out for help.\n\nthat said: what's the first thing YOU would address?"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: ">>> 1 - We need to determine and agree upon what KDE is going to offer its users\n>> that has already been done outside of the usability project itself. we have no need to reinvent that weel.\n\nThat's the problem: \"outside of the usability project.\" There is in fact a need to reinvent, or at least take a good look at, that wheel now that KDE has a \"Usability Team.\" The KDE chariot (since we're talking wheel 8-) won't go much farther if this rethinking doesn't happen.\n\n\n>> btw, i wouldn't use Apple's current software track as an example of HCI in action. MacOS X is the least usable MacOS ever.\n\nSnobbing Apple on usability issues just sounds silly. Though there are indeed problems with OS X, Apple remains one of the companies with the greatest commitment to usability. Following their example, in moderation, could only improve KDE.\n\n\n>> i think you may be confusing the usability project with a project who's goal is to perform R&D into neat and cool new interfaces that do things we don't have yet.\n\nNot at all. I'm not advocating for the creation of \"neat and cool new interfaces.\" I'm advocating for a better understanding of what usability really means for KDE and for a pervasive commitment to usability in KDE which I just can't see right now.\n\n\n>> that said: what's the first thing YOU would address?\n\nFirst, I would define those essential services I talked about earlier. Kdepim holds a large portion of them (calendar, address book, email) so that could be a good starting point. Then, I would gather as much material as I can about what people do with them: discover what's important in an address book, find out how people use their calendar and deal with some of the associated tasks (sync'ing to a PDA, coordinating events with friends and colleagues... who knows?) That kind of stuff. Once we get that, we can look at what we have (KMail, KAddressBook, KOrganizer) and see what we got wrong and what we got right and start addressing some of the usability issues in the existing software.\n\nCreating user profiles could help too. I would create, say, 5 fictitious KDE users to use as conceptual tools in usability discussions. Developers have tools (compilers, debuggers and what not); the \"Usability Team\" should have some too. :) I would write up something about each one of them, make them as real as possible (give each one a name and everything) and post them on usability.kde.org as resources for the whole KDE community to use: \"You want to write the latest and greatest <...>, think about Jose de Souza Magalhaes over in Sao Paulo and about how your application will affect him.\" Give usability faces.\n\nI've got more.\n\n(still a bit) Grumpy (but feeling better)"
    author: "Grumpy"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-18
    body: "fictitious users aren't very useful. they require developers to create a mental model of them, which will inevitably reflect their own biases. this is why testing with real life users is so important versus simply pondering what someone _might_ do: it enables one to discover patterns of usage that they would never even dream of otherwise.\n\nas for your description of dealing with the kdepim package, congratulations! you've JUST described EXACTLY what we've done in the recent past with a handful of smaller apps. what we could use, of course, is someone who can do up a report on korganizer, or kmail, or kaddressbook. they are all needed, and as i've said a few times now, it just takes time. if you are willing to write a comprehensive usability report on ONE of those apps (don't bother doing all of them, there's enough material in any ONE of them ;) please do so and post it to the usability list. follow the DTD on the usability web site and i promise you it will get serious consideration and issues will get addressed.\n\n> Snobbing Apple on usability issues just sounds silly\n\nholding them up as the usability mesiah is equally silly.\n\n> That's the problem: \"outside of the usability project.\" There is in fact a\n> need to reinvent, or at least take a good look at, that wheel now that KDE\n> has a \"Usability Team.\"\n\ni don't agree one whit. the usability project isn't here to reinvent KDE, it is to help refine and improve.  KDE has gotten thus far with all of its integration and features w/out the usability project and will continue to do so regardless of the usability project. that process isn't broken, so i don't see why we should attempt to fix it. we also have a style guide that simply needs to be followed more closely. there are indeed improvements to be made, but ripping every program into little pieces and putting it back together is not the way to do it. an iterative, cooperative process is required.\n\ni know that this is counter to traditional usability sense where those involved in HCI want to DOITALLATONCE, RIGHTNOW, BEFOREWECODE! this simply doesn't work with open source projects like KDE. just as open source brought to light new ways to write code, open source will require the bringing to light of new ways to approach usability.\n\nand to the outsiders it will appear doomed from the start, just like open source coding did to outsiders back in the day. but i feel confident that if we are diligent and take in the lessons we have learned from open source in general, in the end we will not only have results to show but they will be impressive.\n\njoin us today or wish you did later. ;-)\n\n*gets off of soapbox*"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-18
    body: ">> as for your description of dealing with the kdepim package, congratulations! you've JUST described EXACTLY what we've done in the recent past with a handful of smaller apps\n\nWhere? When?\n\n\n>> KDE has gotten thus far with all of its integration and features w/out the usability project and will continue to do so regardless of the usability project.\n\nI'm sure KDE can and will go on regardless of the usability project. But I feel that it would be a tremendous waste to carry on with a project of this importance, at a point when it can really make a difference in the current technological ecosystem (I'm starting to sound like Al Gore :), without committing more resources to the improvement of its usability. And to a certain extent this implies redefining KDE.\n\nI believe that we are at a critical juncture in the evolution of KDE (and free desktop environments in general). We can decide now that we're going to give KDE the means to become a true alternative to existing environments or decide to keep it around as a glorified toy for geeks. KDE is getting to a point where it is good-enough for a lot of things. It's stable, it provides an interesting development framework, it plays nice with other important Open Source components and applications. That's all very positive. At the same time, there are real opportunities \"out there\" for it both as a business and a home desktop. BUT ... all of this is irrelevant if we fail to fundamentally take into account KDE's ability to appeal and make sense to a wider audience. I obviously don't need to convince you of the benefits of good usability but sometimes I feel the \"Usability Team\" doesn't see the complete picture.\n\n\n>> i know that this is counter to traditional usability sense where those involved in HCI want to DOITALLATONCE, RIGHTNOW, BEFOREWECODE! this simply doesn't work with open source projects like KDE. just as open source brought to light new ways to write code, open source will require the bringing to light of new ways to approach usability.\n\nOS didn't \"bring to light new ways to write code.\" It brought to light new ways for people to collaborate and distribute their work. Coding technics and principles are still the same. Writing secure code or designing coherent interface and class hierarchies isn't obsolete in OS projects, is it? Same with usability. We may need to create new forms of usability partnerships with OS developers but the principles of the art remain the same. If you don't design for usability from the start, the resulting \"product\" will be of poor usability. You can't sprinkle usability on top of a crippled application and hope that people will be fooled.\n\n>> but i feel confident that if we are diligent and take in the lessons we have learned from open source in general, in the end we will not only have results to show but they will be impressive.\n\nThe lesson in usability, for os projects and others, is clear: focus your product's design around its intended users from the start or pay the consequences later.\n\n\n>> join us today or wish you did later. ;-)\n\nYou big tease!\n\n(not really) Grumpy (anymore, just plain tired)"
    author: "Grumpy"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-18
    body: "> > done in the recent past with a handful of smaller apps\n> Where? When?\n\na few months ago... unfortunately in the last web site change we lost the reports. hopefully they will reappear. i'm planning on starting a new push for usability reports come 3.2-devel\n\n> We can decide now that we're going to give KDE the means to become a true\n> alternative to existing environments or decide to keep it around as a\n> glorified toy for geeks\n\nthe fact that i support several non-techie users on KDE says your statement is false. it already is a true alternative. it needs more work, but it isn't the glorified toy for geeks you purport it to be with that statement. \n\nand by \"support\" i mean that i'm their \"primary care giver\" though i almost never have to do a thing: KDE works reliably, dependably, consistently and understandably enough for their day-to-day.\n\nit isn't perfect. but it works. now to make it better ;-)\n\n> OS didn't \"bring to light new ways to write code.\" It brought to light new\n> ways for people to collaborate and distribute their work\n\nyes indeed it did bring to light new ways to write code. the resulting lines of code look hauntingly familiar, but the process through which they were written was practically heresy against conventional development wisdom. \"what? a bunch of volunteers spread out around the world hacking on whatever interests them? no way that'll EVER work!\" they were, of course, wrong.\n\nthe same thing will likely occur with usability.\n\ni remember a few years back when Linux really started to get popular. certain newcomers started saying that the kernel needed to be redesigned, that the way the development process was going was unsustainable, etc, etc, etc... not unlike what you are saying. they were were wrong then, and those saying the same today are, IMO, wrong now.\n\nof course: how will we know? if by employing an interative during-devel/with-devels approach we elevate the usability of KDE beyond that of other systems, we will know it works.\n\nnow look what i've promised. heh. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-17
    body: "I've seen gnome on my mdk 9 and it has really cleaned up a bit.\n\nI loved the look of small icons. This is something I've allways felt could be improved in KDE, especially in the panel. Small icons in gnome look readable and crisp, whereas KDE small icons usually look too small and dull.\n\nI also like the nautilus folder notes and icon labels. This later offers much better functionallity than changing the folder icon to folder_red_with_smiley.png in konq. \n\n"
    author: "zelegans"
  - subject: "Re: KDE && 800x600 == bad"
    date: 2002-10-22
    body: "Unfortunately, I have a few machines at work which due to cheap graphics cards and/or old monitors, struggle even with 800x600.  If you think KDE is bad at this resolution, then try it at 640x480.\n\nYes, I know that there will only be a few people trying this, but sometimes it's the only option you have (other than text modes)."
    author: "Sean"
  - subject: "EVERYTHING gets out of screen"
    date: 2005-10-16
    body: "Then konqueror preferences, the kppp, the control center... is going this to be fixed soon?"
    author: "Paloseco"
  - subject: "Startup time ?"
    date: 2002-10-16
    body: "Hi all. A question for anyone testing/developing 3.1 . Has the startup time been reduced in the 3.1 branch ? My experience is that KDE runs *very* fast once it is loaded, but the start up time (meaning from when you login at KDM until you are able to open an application in the desktop) is a little too long. Typically many times what GNOME takes. I still prefer KDE as my desktop environment, I only login perhaps a couple times a day, and once it is working KDE is just beautifull. And I know that the price you pay for the start up time is fair enough if you consider \nthe formidable framework you are launching.\n\nBut I wonder if there is some work on reducing startup time. I am aware of the object prelink issue. However, my mandrake 9.0 binaries are apparently compiled with the objprelink1 optimization on, according to http://objprelink.sourceforge.net/ . And according to this page it is not cleat whether  objprelink2 would give any further benefits. I guess a way to speedup  startup time would probably be to delay the initialization of non-essential services. Just let the user login with the essential components loaded, and load the rest in the background after that.  Any thoughts, any further optimizations ?\n\nCheers \n:-)"
    author: "NewMandrakeUser"
  - subject: "Re: Startup time ?"
    date: 2002-10-16
    body: "objprelinking is a hack that tries to cure symptoms of a problem. prelinking is the solution to the problem itself. Prelinking is supported in the lates binutils and glibc 2.3. Thanks to prelinking, startuptimes should drop to fraction of what they were before. Does KDE work with prelinking? That I don't know for sure. To my knowledge the current stable releases (3.0.x series) does not. Any idea that will 3.1 work with prelinking?"
    author: "Janne"
  - subject: "Re: Startup time ?"
    date: 2002-10-17
    body: "A question:\n\nIn the latest kde apps for example koffice-1.2, when execute ./configure --help appears: \n\n--enable-objprelink     prelink apps using objprelink (obsolete)\n\nThe new apps not use objprelink ? \n\nNot need this ?"
    author: "fty"
  - subject: "Re: Startup time ?"
    date: 2002-10-17
    body: "Well, so far, 3.1 beta2 doesn't seem to work with prelinking. I'm not totally sure I've got it setup perfectly though. I installed glibc 2.3, ran prelink -a (my prelink.conf file included the KDE and QT directories) but the apps don't get prelinked. Worse, it causes qmake to fault with a \"unexpected reloc type 0x05\" which can only be fixed by prelink -u qmake.  "
    author: "Rayiner Hashem"
  - subject: "Re: Startup time ?"
    date: 2002-10-17
    body: "And you did report the bug to the prelink author yet? And I fail to see where qmake is involved in starting KDE/programs. Undo prelink for it and it's fine, I don't see why this is worse."
    author: "Anonymous"
  - subject: "Re: Startup time ?"
    date: 2002-10-17
    body: "First, I don't know if its a bug or just my configuration. Nothings failing, it's just that prelinking doesn't seem to have any effect. The number of relocations done at runtime remains the same even after prelinking. Also, qmake isn't involved, but without it, you can't build a lot of Qt apps. And prelinking seems to break it, which it isn't supposed to do."
    author: "Rayiner Hashem"
  - subject: "Re: Startup time ?"
    date: 2002-10-17
    body: "To speed up the KDE startup on Mandrake 9.0, try this:\n\n1) open up /usr/bin/startkde in your favorite editor\n2) search for \"/usr/bin/nspluginscan\"\n3) comment out that line (put a # in front of it)\n\nThis greatly improved my startup time. If I ever install a new Netscape plugin (why I would do that, I have no idea) I can always re-scan plugins from the KDE Control Center.\n\nNeedless to say, if you don't have the kdebase-nsplugins package installed, this tip isn't going to help you."
    author: "ShavenYak"
  - subject: "Re: Startup time ?"
    date: 2002-10-18
    body: "If you power cycle your machine often (as in if you turn it off when you don't use it); you can also probably cut away a few seconds by commenting out the removal of /tmp/kde-* in rc.sysinit (be *extra careful*, this is a critical script); as this will keep the KSyCoCa cache over reboots, thus savingthe startup process of KDE from having to rebuild it. "
    author: "Sad Eagle"
  - subject: "Re: Startup time ?"
    date: 2002-10-19
    body: "Great tip, thanks !. I am actually doing it in a per/user basis: from Konqueror, \"Settings -> Configure -> Netscape Plugins\" and uncheck \"Scan for new plugins at KDE startup\"\n\nAlso, I removed a bunch of irrelevant entries in my list of directories to scan, and it is much faster to scan now. \n\nThanks a lot for the pointer Shaven!"
    author: "NewMandrakeUser"
  - subject: "Re: Startup time ?"
    date: 2002-10-19
    body: "Hurm.  I wonder if turning it into something like:\n\n( sleep 15 ; /usr/bin/nspluginscan ) &\n\nMight be a good idea for everybody - although a nsplugin might be used at the very beginning of a KDE session, using a newly installed one is less likely.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Startup time ?"
    date: 2002-10-18
    body: "I've read about pre-linking, but don't know the details.  However, I do know that betas are not the place to be doing speed or size comparisons, since they often still include a lot of extra crash tracing code and symbols compiled in that full production versions may exclude."
    author: "Duncan"
  - subject: "Re: Startup time ?"
    date: 2002-10-18
    body: "Not that I'm a KDE developer or anything, but who cares?\n\nI get into KDE and stay in it for weeks or months.\nSo for me, KDE startup time is fairly irrelevant."
    author: "JoJoMan"
  - subject: "Re: Startup time ?"
    date: 2002-10-18
    body: "Who cares ?. I do. I share my computer at home with my wife and we keep loging in and out. It is a decent machine (PIII 550 Mhz), and loading KDE takes a minute. It is a little long :-)"
    author: "NewMandrakeUser"
  - subject: "Does anyone know how to use the konqi history?"
    date: 2002-10-18
    body: "I can't figure out how to get the history sorted by time, to figure out\nwhat was the most recently visited sites.\n\nDoes anyone know how?\n\nI tried phoenix the other day, it's got some nice mechanisms to sort/find\nin the history which would be nice."
    author: "JoJoMan"
  - subject: "Re: Does anyone know how to use the konqi history?"
    date: 2002-10-18
    body: "Right click on any entry in the history tree, Sort->By Date."
    author: "Sad Eagle"
  - subject: "Folder icon thumbnails?"
    date: 2002-10-22
    body: "Does anyone know if amidst the \"video thumbnail\" feature and the \"folder icons reflect folder contents\" feature, maybe they managed to add the feature that Win XP has (sorry to have to mention it) where the file browser can display a thumbnail image for a folder based on its contents?\n\nThis is a great feature for working with pictures (you can see a sampling of what's in a folder) and with music (you can make folder icons be the album art for the album they contain).  It seems that with all this eye candy and thumbnail generation going on, they could maybe use some of that code to add this feature, too.  At the very least a kludge showing a thumbnail over a folder icon for an image in that folder named \"folder.jpg\" like Windows does it - granted I'm not a programmer, but it sounds easy to me, what with all this other thumbnail and folder-icon stuff going on.\n\nLittle touches like this will help people switch over from other operating systems.  I know I'm going through withdrawal over this feature...\n"
    author: "Damek"
  - subject: "Kroupware"
    date: 2002-10-23
    body: "Kroupware - Kroup? What the heck? Argh! I really hope this kind of naming doesn't keep happening... it's seriously bad. What's up with everything having to have a K or a G in front of it? Sorry, just had to say it somewhere!\n"
    author: "Kroupster"
  - subject: "Re: Kroupware"
    date: 2002-10-23
    body: "I totally agree. Kroupware has to be one of the worst names ever."
    author: "Pingo"
  - subject: "Re: Kroupware"
    date: 2002-10-26
    body: "Don't forget Kandalf! (smirk)"
    author: "gandalf"
  - subject: "Re: Kroupware"
    date: 2002-10-27
    body: "It's a work code name. The final will be named nicer, perhaps \"Koala\" as previously suggested."
    author: "Anonymous"
  - subject: "Re: Kroupware"
    date: 2002-10-27
    body: "KThe Kpoint Kis, Kwhen Kwill Keverything Kstop Kbeing Kprefixed Kwith Ka K'K'?\n"
    author: "Anon"
  - subject: "patches?"
    date: 2002-10-24
    body: "Does KDE release patches? I don't see the need to get THE ENTIRE SOURCE TREE when I want to upgrade from 3.0 to 3.1 when it's ready. But I've never seen any diffs on any KDE ftp site or mirror."
    author: "Glen"
  - subject: "Re: patches?"
    date: 2002-10-24
    body: "I dont think they do, but admittatly, I haven't looked very hard, as I often download more than 1gb per day (go college connection :-) ).\n\nCouldn't you just use cvs and the release branches?"
    author: "shm"
  - subject: "Re: patches?"
    date: 2002-10-24
    body: "> But I've never seen any diffs on any KDE ftp site or mirror.\n\nThen look at this: ftp://ftp.kde.org/pub/kde/stable/3.0.3/contrib/diffs-3.0.2-3.0.3\nNo, it's not created for every release because I guess seldom requests for it."
    author: "Anonymous"
  - subject: "Re: patches?"
    date: 2002-10-24
    body: "The patches to upgrade between 3.0 and 3.1 would be huge. If you just want to download part of the source tree use the anonymous cvs server.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "I've been dying for tabbed browsing with Konqueror"
    date: 2002-11-03
    body: "I Love KDE.\n\ni've been dying for tabbed browsing in konqueror, and atlast it's here :)\nKDE is now 1337ly perfect "
    author: "ES @ GT }:)"
---
Most of you dot readers already know what KDE 3.1 will be about (<a href="http://dot.kde.org/1033624198/">3.1beta2 story</a>, <a href="http://dot.kde.org/1029974525/">3.1beta1 story</a>, <a href="http://dot.kde.org/1026405852/">3.1alpha1 story</a>).  For the rest of you, you can find a little <a href="http://promo.kde.org/newsforge/kde-3.1.html">teaser preview</a>, complete with some screenshots, at <a href="http://promo.kde.org/">promo.kde.org</a>.  Enjoy!  <strong>Update Saturday 19/Oct/2002, @16:17</strong>:  <a href="http://news.zdnet.co.uk/">ZDNet UK</a> has published the story <a href="http://news.zdnet.co.uk/story/0,,t269-s2124074,00.html"><em>New KDE to arrive with Halloween eye candy</em></a> based on the preview.  Thanks to them for helping get the word out!


<!--break-->
