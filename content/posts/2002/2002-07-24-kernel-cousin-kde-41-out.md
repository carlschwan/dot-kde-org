---
title: "Kernel Cousin KDE #41 Is Out"
date:    2002-07-24
authors:
  - "rkaper"
slug:    kernel-cousin-kde-41-out
comments:
  - subject: "OMG!"
    date: 2002-07-24
    body: "Look at this: http://svg.kde.org/Screenshots/index.html"
    author: "ac"
  - subject: "Re: OMG!"
    date: 2002-07-25
    body: "I suspect I need a 10GHz processor to actuall be able to\nwork with my computer with this...\n"
    author: "Q"
  - subject: "Re: OMG!"
    date: 2002-07-25
    body: "Something about those screenshots:  Chinese ideographs (check out ksvg-i18n.png) all appeared as question marks.  I'm assuming that that's been fixed since that shot was taken? :)"
    author: "dwyip"
  - subject: "Re: OMG!"
    date: 2002-08-17
    body: "Hi guys!\n\nThose screenshot are actually quite old.\nWe have full support of arabic, hebrew, yiddish, chinese,\npolish, czech, chinese, japanese (kanij)...\n\nAll works :)\nExcept top to bottom, this doesn't work.\n\nYou say OMG to those screenshot? You ought to see KSVG in action....\n\nBye\n Bye\n  Niko"
    author: "Nikolas Zimmermann"
  - subject: "Nevy OS"
    date: 2002-07-25
    body: " \"Nevy OS, an operation system based on Qt/E with its main component being Konq/E\".\n\nCan someone tell me what this is?  The website is really veuge.  Is it suppost to run on something like the Zaurus or a real desktop?  Does it have console tools or all graphical.  It talks about having an alpha, but I can't find it on its web page.  What version of QT?  What apps does it have?  Does it have the kde libs or only Qt?\n\nAnyone?"
    author: "Benjamin Meyer"
  - subject: "Re: Nevy OS"
    date: 2002-07-25
    body: "\"Can someone tell me what this is? The website is really veuge. Is it suppost to run on something like the Zaurus or a real desktop?\"\n\nSo far as I can tell, it's a stripped-down Linux system mostly designed for older, slower PCs and, I guess, 'information appliances'.  It uses Qt/e running on the Linux framebuffer for a GUI, avoiding the overhead of X.  From the screenshots and description it appears they are not using Qtopia but rather their own basic 'program launcher' environment which also has a simple file manager built-in, I suppose not that dissimilar to some of the more advanced WMs for X like Afterstep or Enlightenment.\n\nkdelibs would be inappropriate for this system as kdelibs requires X.  As it's a Linux system at heart, there's almost certainly some kind of text console available, but it's quite possible it's well hidden.\n\nAs for available software, if it's using Qt/e then any sotware that runs using Qt/e only should work.  Konq/e is listed as it's probably the single most important app for an environment like this - no-one is going to bother with it unless it can at least view webpages - but as you probably well know, there's a lot more Qt/e software out there.  If it's using Konq/e currently then that means it's using Qt/e 2.3.x.\n\nA compact system like this makes a lot of sense for people with limited computer hardware, and this is a particular problem for developing countries like India, where this comes from.  Think K6/200 with 16 or 32MB RAM or less as a target system.  Even in the developed world, this configuration is still widespread, and it doesn't run most modern software very well.\n\nLinux has had a lot of success breathing new life into old machines as servers, but has been less successful at turning these into GUI machines for Mr/Mrs/Miss/Ms Average.  The big X desktops are too complicated and resource-hungry.  Simple X WMs are not consistent enough with the (mostly Qt or GTK) apps that will be run.  Qtopia is nice, but is really aimed at handhelds and is probably still a little complicated for someone who's never used a computer before.\n\nI think NevyOS will fill quite a substantial niche that hasn't really been addressed properly (assuming they do it right, of course): a dead-simple interface with no way of breaking things, running powerful modern Qt apps well on ancient hardware.  Perfect for government-sponsored 'get-everyone-online' campaigns.  It could be just the thing to put Linux everywhere."
    author: "My Left Foot"
  - subject: "Re: Nevy OS"
    date: 2002-07-27
    body: "Same question: So where can I get it?  Can I test it on my computer?"
    author: "Michael Lehn"
  - subject: "SVG icons"
    date: 2002-07-25
    body: "I guess they are nice... But why is that the every example of SVG icons I have seen (screenshots etc.) has had HUGE icons? To show off how you can scale the size? IMO huge icons look silly. I like my desktop uncluttered, and that means that the icons are like they are supposed to be: not too big."
    author: "Janne"
  - subject: "Re: SVG icons"
    date: 2002-07-25
    body: "Yeah, it's pretty much to show off scaling. With a static screenshot, they can't show off the other cool thing about SVG icons: animated smooth scaling."
    author: "Carbon"
  - subject: "Re: SVG icons"
    date: 2002-07-25
    body: "It could also be that many of the people who want svg icons, want them because they can get big.  I only have 3 icons on my desktop, so I like the idea of being able to make them into nice big targets for my mouse."
    author: "theorz"
  - subject: "Re: SVG icons"
    date: 2002-07-25
    body: "Mmmm, I can't wait for SVG goodness to get into Linux. But .. one question - why are the KDE team doing their own implementation? What's wrong with using the GNOME peoples implementation (other than the parts to integrate with konqueror). I'm not trying to troll, but it strikes me that SVG is a huge spec and very complex to implement. We now have 4 implementations in the OSS community (that I know of):\n\n- GNOME2: libsvg?\n- KSVG\n- Mozilla SVG\n- Apache Batik\n\nSurely the actual drawing the image part could be encapsulated into a library and reused. It seems an awful shame to replicate so much work over and over."
    author: "Mike Hearn"
  - subject: "Re: SVG icons"
    date: 2002-07-25
    body: "Actually, KSVG does use GNOME code.\n\nlibart\n\nhttp://www.gnome.org/~mathieu/libart/libart.html\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: SVG icons"
    date: 2002-07-26
    body: "libart isn't *really* GNOME code though, even if it is developed in GNOME CVS.  \n\nIt has no dependencies on any GNOME or even GTK+ code, so it is an equal-opportunity library: everyone can use it, without bloating the code linking to someone else's toolkit.  Of course it's written in C, so C coders perhaps have a slightly easier time using it, but everyone uses libraries written in C all the time anyway, so it's no real advantage.\n\nAs for why there are 4 competing implementations of SVG on Linux: any full implementation of it is going to rely on one toolkit or another, as it has to draw on-screen and interact with the user.  But no-one is going to be willing to bloat their code with someone else's toolkit.  I guess you could build a library that knows about Xlib and can draw and receive events on its own but then you just made your life 10 times harder and need to write 5 times as much code\n\nIt's a lot more complicated than with the simple bitmap image formats, where a single standard library can do nearly everything.  Much of the implementation simply isn't sharable because it's toolkit specific: how to draw on the screen, how to detect input and events, oh, and the scripting:  whose Javascript interpreter do you use?  KJS?  Mozilla's?  I don't think Mozilla would be happy to have to include KJS, and vice versa.  They already have their own perfectly good implementations, adding someone else's would be some fairly serious bloat.  Of course, you could try and define a standard plug-in interface for interpreters and try and mangle both KJS and Mozilla to fit that, but then, that's a major refactor of some pretty fundamental code to both projects just to support SVG.  \n\nIt's simply easier in this case to build your own SVG implementation, and get the benefits of code reuse with your own toolkit and project.  What can be shared is already being shared, at least between the GNOME and KDE implementations, and that's libart."
    author: "My Left Foot"
  - subject: "Re: SVG icons"
    date: 2002-07-30
    body: "Mozilla SVG support also currently uses libart\nsee http://www.mozilla.org/projects/svg/\n"
    author: "David Fraser"
  - subject: "keep 'em coming!"
    date: 2002-07-25
    body: "Still loving the summaries.  Thanks for keeping them alive."
    author: "Navindra Umanee"
  - subject: "Re: keep 'em coming!"
    date: 2002-07-25
    body: ":]"
    author: "kc kde"
---
<a href="http://kt.zork.net/kde/kde20020724_41.html">Kernel Cousin KDE #41</a> has been published. This edition includes Konq/E updates, KOrganizer <a href="http://tux.ict.tbm.tudelft.nl/~janpascal/exchange/">compatiblity with Exchange 2000</a>, tabbed browsing updates for Konqueror, KDE 3.2 candidates such as fractions with <a href="http://apps.kde.com/na/2/info/id/1811">KBruch</a> and <a href="http://svg.kde.org/">KSVG</a>. Enjoy!
<!--break-->
