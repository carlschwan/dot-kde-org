---
title: "Python/C# Mania:  New Bindings Expand KDE Languages"
date:    2002-03-17
authors:
  - "Dre"
slug:    pythonc-mania-new-bindings-expand-kde-languages
comments:
  - subject: "Perl bindings"
    date: 2002-03-17
    body: "It's great that there are news on the language binding front. But, when will we ever see new Perl bindings for Qt/KDE? There are lots of skilled Perl programmers out there, and I'm sure they could benefit the KDE community to no end if there were bindings available.\n\nAnd, no the Qt bindings on CPAN doesn't seem to work :-( Could this be a conspiracy to force Perl people to switch to Python!!!! :-)"
    author: "KAP"
  - subject: "Re: Perl bindings"
    date: 2002-03-17
    body: "\"But, when will we ever see new Perl bindings for Qt/KDE?\"\n\nYes, Germain Gerand, TJ Mather and myself are working on Perl bindings for Qt 3/KDE 3, and they should be ready for KDE 3.1.\n\nThe bindings are generated from 'Perl Interface Generator' .pig files that used to be prepared by hand. We've added a .pig file generation option to the kalyptus bindings generator to autogenerate them. Germain has regenerated the bindings for Qt 2.2.x and KDE 2.2.2, and pretty well got that working in TJ's cvs.\n\nBut the big problem with Qt 3 is that the bindings assume the moc works the way it did for Qt 2. The was a type in Qt 1.x/2.x called 'QMember' which was a pointer to a function for the target slot to be called. In Qt 3 the moc has been changed so it uses an integer offset into the slot table as 'QMember' to invoke a slot. So the slot/signal handling needs to be redone (and the code is quite tricky to follow).\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Perl bindings"
    date: 2002-07-16
    body: "Is there a link where you post the information regarding the progress of your development?\n\nThanks.\n"
    author: "Ricardo Pacheco"
  - subject: "Re: Perl bindings"
    date: 2002-03-17
    body: "\"Could this be a conspiracy to force Perl people to switch to Python!!!! :-)\"\n\nTHAT would be a good conspiracy theory for a change *ducks away from flamethrowers and runs home* :-)"
    author: "Danny"
  - subject: "Re: Perl bindings"
    date: 2002-03-18
    body: "Don't make me bring out my Swiss Army Chainsaw! :-)"
    author: "Carbon"
  - subject: "Re: Perl bindings"
    date: 2002-03-20
    body: "Yes, there is still plenty of work, but things are moving...\n\nHere is a little screenshot with only PerlQt apps (running Qt-2.3.1).\nSome more skilled Perl/C/XS programmers would be quite welcome though..."
    author: "Germain"
  - subject: "Re: Perl bindings"
    date: 2002-03-20
    body: "\nuh oh... seems the attachment thingy didn't work...\n\nhttp://www.phoenix-library.org/germain/shots/PerlQt-2.3.1.png"
    author: "Germain"
  - subject: "Good news"
    date: 2002-03-17
    body: "Great news, good to see that mono will have support for both the major desktop environments."
    author: "dave"
  - subject: "genius"
    date: 2002-03-17
    body: "Now you get Mono for free under KDE?  Genius...  but will this require you to install GNOME?  Not so good..."
    author: "ac"
  - subject: "Re: genius"
    date: 2002-03-17
    body: "No, you are not required to install Gnome.  Mono is _not_ a part of Gnome... yet ;-)  Even if Mono is included in Gnome in the future, the Qt bindings will only require Mono's runtime (jit and corlib) and compiler (mcs)."
    author: "Adam Treat"
  - subject: "Re: genius"
    date: 2002-03-17
    body: "all hail the genius! :-)\n\ngood job, man."
    author: "ac"
  - subject: "Do we have ourselves a little TROLL here?"
    date: 2002-03-17
    body: "ac, you better get your act together man.\nI've seen you on gnome-news also.\nYou're the one always posting controversial stuff.\nNow what is \"Genius... but will this require you to install GNOME? Not so good...\" supposed to mean?\nHave you no respect for the gnome-developers?\nAnd no, if you would know what a compiler was, you wouldn't be asking this question...\nDidn't you figure out yet, that it's all about the community.\nWho cares what DE you like. All the developers are there to give a product to the community. Should you therefore continue to bash them?\nSilly man, crawl back in the hole where you came from...\n\n(To other folks, sorry you had to read this, but I've seen this fellow more than once, with no good word coming out of his mouth)\n\nYours truly,\nPissed of blashyrkh"
    author: "blashyrkh"
  - subject: "Re: Do we have ourselves a little TROLL here?"
    date: 2002-03-17
    body: "Does GNOME require the install of KDE?  See that would be bad just like it would be bad if the install of KDE required GNOME.  Nothing controversial here.  Self-important anti-trollers like you are just as bad as the real trollers.\n"
    author: "ac"
  - subject: "Re: Do we have ourselves a little TROLL here?"
    date: 2002-03-17
    body: "First of all, Mono doesn't require you to install GNOME as it isn't a part of GNOME. \nSecond of all, GNOMErs might start using aRts, which IS\nactually a part of KDE. There is nothing wrong with either."
    author: "anti-ac"
  - subject: "Re: Do we have ourselves a little TROLL here?"
    date: 2002-03-18
    body: ":: Second of all, GNOMErs might start using aRts, which IS actually a part of KDE.\n\nNo offense, but I've heard this for awhile without anybody who states it being able to back it up.  Where did this \"common knowledge\" come from - it's been repeated quite often, but I haven't seen any links to any developers actually discussing it.  I'd like to have documentation to potentially back up this.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Do we have ourselves a little TROLL here?"
    date: 2002-03-18
    body: "There was occasionally talk about using CSL by Stefan and Tim Janik, and that can output to OSS, Arts and Alsa, but no-one seems to know whats going to happen"
    author: "iain"
  - subject: "GNOME and aRts"
    date: 2002-03-20
    body: "Weird!  Iain replied to you earlier, but his response has disappeared.  At any rate, there has been discussion on the GNOME lists (from last August or September, I think) about using aRts.  No decision has been made yet, and there is no default sound server/sound system for GNOME 2.0.  The developers decided to wait and see how things play out in development between the different contenders.  The two front-runners are aRts and GStreamer: the former is a little more mature, while the latter is more integrated with GNOME (it uses GLib).  A decision will most likely be made for GNOME 2.2.  Cheers!"
    author: "aigiskos"
  - subject: "php-qt"
    date: 2002-03-17
    body: "Anyone working on php-qt or php-kde ?\nSince php-gtk is available, it should be possible to get a binding with your favorite toolkit :)"
    author: "Moby"
  - subject: "Re: php-qt"
    date: 2002-03-17
    body: "php-gtk is just a proof of concept, it's not usefull at all for real development"
    author: "Benko"
  - subject: "Re: php-qt"
    date: 2002-03-17
    body: "Well, I have to say it is.\nFor little apps, it's very good.\nYou see, PHP is the easiest language out there IMHO, and I already did some really good php-gtk programs with it.\nYes, the real problem here is that a php compiler is missing, so distribution of it is very hard on linux machines.\nBut indeed it's very good and very, very cool. I would love to have qt-php :)"
    author: "protoman"
  - subject: "Re: php-qt"
    date: 2002-03-17
    body: "This is such a non-sense. Php is a scripting language for dynamic web page. If you want an easy scripting language, yet powerful, take python."
    author: "Philippe Fremy"
  - subject: "Re: php-qt"
    date: 2002-03-17
    body: "What? Php-gtk is far from useless. I have written a proof of concept query tool in it, and it runs very fast, and because of PHP's excellent DB functionality, can connect to virtually any databas eunder the sun. PHP-Mole (http://www.akbkhome.com/Projects/Phpmole-IDE/) is a feature-rich IDE being written entirely in PHP-GTK. I'd suggest checking out stuff next time before making sweeping assumptions.</p>"
    author: "Jason Keirstead"
  - subject: "Re: php-qt"
    date: 2002-03-17
    body: "I have to disagree... as both a python and PHP programmer, both are useful and very complete languages.  PHP is extrodinarily good at generating text files of any sort (due to the way it handles output), but can be used to do virtually any task.  I have a PHP script that goes through my MP3 files, renames them (capitalizing words, spacing dashes, etc), and resets the ID3 tags (a binary operation) in them to what the filename suggests.\n\nLike Perl and Python, PHP supports modular, loadable libraries, and also supports classes.  It is very much capable of being interfaced to Qt and KDE and producing useful applications.  And even if your view is *still* that it is \"just a HTML scripting language\", let me kick this idea out - DCOP aware webpages on your intranet.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: php-qt"
    date: 2002-03-18
    body: "Yep, I'll second that. I have a PHP project that runs as a daemon, using SQL based mp3 playlists\nto play music, and talking about it (and automagically downloading and reading news) with\nFestival. I probably could've gotten better performance if I had written it in C or even Perl, but\nit didn't take me long to develop, and it was a good exercise to finish up learning PHP.\n\n*Shameless code promotion* That app isnt in devel anymore, but you can get it at\ngeekradio.sf.net, and I might just continue to work on it if there's any demand."
    author: "Carbon"
  - subject: "Re: php-qt"
    date: 2002-03-18
    body: "s/python/ruby/\n"
    author: "Guillaume Laurent"
  - subject: "Re: php-qt"
    date: 2003-11-11
    body: "I would really like to see a phpQt.\n\nMaybe now is the time since PHP5 is about to come out."
    author: "jaxn"
  - subject: "Parrot VM"
    date: 2004-03-11
    body: "Can PHP shares toolkit from other language if PHP is ported to Parrot?"
    author: "Vee Satayamas"
  - subject: "Re: php-qt"
    date: 2004-09-09
    body: "Does somebody knows if php-Qt is being developed? if not it could be a great project that i thought about a time ago.\n"
    author: "Ayoze"
  - subject: "php-dcop"
    date: 2002-03-18
    body: "Hi,\n\nWhat a surprise, I had the same idea yesterday.\nHowever, since php is written in C, this would require to\nadapt the C bindings into a php extension.\n\nThe next idea was to implement a dcop client wrapper for\nphp. Since dcopc has only about 30 functions, it should\nnot be too difficult.\n\nI started yesterday, and if there is any progress, you will\nread about it on the dot.\n"
    author: "Matthias Lange"
  - subject: "Re: php-qt"
    date: 2006-01-20
    body: "http://php-qt.berlios.de/"
    author: "nobody"
  - subject: "python bindings official or alpha ?"
    date: 2002-03-17
    body: "TheKompany's page says the 6th alpha version of the KDE2 bindings are released. Nothing about the final release or KDE3 bindings."
    author: "Philippe Fremy"
  - subject: "Re: python bindings official or alpha ?"
    date: 2002-03-18
    body: "Plus, I wasn't able to build the Python binding code, plus it is not included in most (any) distro.\n\nFor Python to really make it as a language under KDE, you would need to have support at the core level. That is, you install KDE, you have the Python bindings, period.\n\nhttp://www.ondelette.com/indexen.html - visit the wavelet forum!"
    author: "Daniel Lemire"
  - subject: "Re: python bindings official or alpha ?"
    date: 2002-03-20
    body: "<i>For Python to really make it as a language under KDE, you would need to have support at the core level. That is, you install KDE, you have the Python bindings, period.</i>\n\nWe should not repeat GNOME's mistake of requiring python and probably 200 other languages for a base install just because some applications are written using it. We don't want a 1 GB base install... (Remember that we could do just the same thing for C#, Java, C, Objective-C bindings... There's no real reason why Python is more important than any of those).\n\nIt's a sort of chicken and egg problem - distributors will start picking up PyKDE once there are applications for it. Developers will pick up PyKDE once they're aware of it, which is often by seeing it in a distribution.\n\nFor a start, I'll add PyKDE in one of the next Red Hat Linux releases; it's too late for the next one, unfortunately."
    author: "bero"
  - subject: "Ruby, oh Ruby"
    date: 2002-03-17
    body: "Geesh, I would love to see updated versions of Ruby/Qt and Ruby/KDE and sweep away all of those Python and Perl crowds ;D"
    author: "unnamed man"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-17
    body: "Ruby bindings have been updated to 0.15, look in the app box to the right.\n"
    author: "ac"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-17
    body: "Nope, they aren't updated. They appear in apps.kde.com as updated 'cause it is the first time they are added to the apps index. They are still for KDE 1.x and Qt 2.0.x"
    author: "unnamed man"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-17
    body: "\"look in the app box to the right\"\n\nWhat's this then? Sounds like an adventure game to me :)\n\nWhich version of Qt/KDE does the 0.15 release of the Ruby bindings relate to? As far as I know Nobuyuki Horie hasn't updated the bindings for the last six months.\n\nI've added a Ruby bindings code generation option to the kalyptus bindings generation utility - usage 'kalyptus -fruby <qt/kde headers>'. It needs a bit more work (maybe only a week or so to add method overloading code generation), but then it should be much easier to keep up to date than maintaining SWIG interface files.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-17
    body: "The Ruby bindings were done with SWIG ( http://www.swig.org/ ) , which doesn't exactly automate the process of writing wrappers around C++ code.\n\nI really doubt bindings will be able to keep up with KDE unless something more efficient comes along."
    author: "Neil Stevens"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-17
    body: "\nIt seems to be that something could be drawn from one of the indexing programs such as lxr. (http://lxr.kde.org/) which automatically parses the classes in KDE.  Failing that, the documentation is kept in a structured format, so bindings could be autogenerated from that information, complete with documentation for the bindings.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-17
    body: "What you've just described is Kalyptus.  It is a modification of KDoc by Richard Dale, and it is excellent.  Basically, all of these bindings (Java, C, Objective-C, C#, and perl) are drawn from Richard Dale's work.  Look a few posts above and you will find that Richard is working on some new Ruby bindings as we speak."
    author: "Adam Treat"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-19
    body: "Since it took the person who wrote Ruby bindings for GNUStep only about a day to do it (he didn't know anything aobut GNUStep and was a newbie to ruby) how long can it take to write Ruby Qt ... an hour?\n\nOh yeah I forgot KDE/Qt is C++ ... blech :-)"
    author: "NameSuggesterEngine"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-19
    body: "yeah, but kdec/qtc is C :P"
    author: "dc"
  - subject: "Re: Ruby, oh Ruby"
    date: 2002-03-19
    body: "KDE has language bindings for dynamic languages like Objective-C or Java, so it should be possible to write a dynamic bridge to those languages like the Ruby/GNUstep one (or even use the same code). I think JPython should work well with the Java bindings.\n\nBut it isn't that much effort to write static bindings, once you don't have to prepare heroic amounts of SWIG interfaces by hand.\n\n-- Richard"
    author: "Richard Dale"
  - subject: ".NET is a big waste of time"
    date: 2002-03-17
    body: "Now, these days, it seems that most programs are written in a programming language.  It also happens that most utilize an application programming interface (API).\n\nIn trying to understand what .NET was, I decided to split it into the C# language and the API (which is called CLI or something).  C++ was the object oriented derivate of C, and one example of API is Qt (of which KDE looks like an extension), another is GTK+ (of which GNOME looks like an extension) with its C++ bindings.  Then comes Objective-C and OpenStep, Java and Java (um, Java language and Java API)...\n\nI don't think the world needs another one!"
    author: ".NET is a big waste of time"
  - subject: "Re: .NET is a big waste of time"
    date: 2002-03-18
    body: "Bah.. yet more trolling. .NET is actually a good idea. First of all, C# looks like a nice language, although a pretty unoriginal java-copycat.\n\nBut the second idea, of a CLI (Common Language Interface), which makes it so you won't have to write new bindings for each and every language is a good idea.\n\nIf it works, MONO would be a very nice development platform for it seems BOTH Gnome and KDE. Way to go!\n\nPerhaps it will also mean that you can program once, and have it run under both Windows, KDE, and Gnome with native look and feel."
    author: "Gaute Lindkvist"
  - subject: "Re: .NET is a big waste of time"
    date: 2002-03-19
    body: "We can port program in Java since years and it work very well.\nIn Java, there is even a native toolkit integration. It is called AWT.\nAnd nobody uses it! It is not because it not slow or because the API is bad.\nIt goes even very fast.\n\nBut when you write your application for a OS (ex Linux), it looks awful\nin another one (ex Windows). To solve that, you finish by having a part \nof your code that handles the difference between the OS. Just because\nof that, nobody uses it.\n\nI do not want to see .NET. Just because it is Microsoft. They do not\ncompete by technology but by a huge distribution channel.\n\nIn the Java world, everything is not perfect too.\n\nI think there is no public implementation of AWT in QT. It is just Motif.\nTrolltech has written one for the Zaurus. But it is not available on Linux. \nIt should be very, very good to have it as a standard part of the JDK on Linux\n "
    author: "Marc"
  - subject: "Re: .NET is a big waste of time"
    date: 2005-02-22
    body: "AWT was terrible, you probably mean SWT?\nSwing is also terrible, first you need a good look & feel,\ncombining the new XP look+feel with JGoodies is a good way...\n\nBut with Java you can't make quickly some dialogs, with C#\nand .NET you can get good looking dialogs very quickly.\nJust like I could for years with Delphi.\n\nJava has its strong points, but GUI development is not one\nof them. And for Gnome and KDE we need GUI's not backend\nprograms.\n\nC# is a good programming languages, it has most of the things\nI liked from Delphi, and as a big improvement over C++ and C, it\nhas internal string class. It would be nice if Trolltech would help\nand make a version where the QString class is not needed any\nmore for the QT/C# bindings.\n\nBut I don't see many differences between using Mono with GTK#\nor QT#, and Java using SWT, for every plattform you need a\nnew GUI Design. Hopefully one day someone will come up with\na better solution for that..."
    author: "Boemer"
  - subject: "Re: .NET is a big waste of time"
    date: 2002-03-18
    body: "<i>Now, these days, it seems that most programs are written in a programming language.</i>\n\nNo, really ? Wow, that's really deep. Makes one think, doesn't it ?\n"
    author: "Guillaume Laurent"
  - subject: "What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: "Alright, I know this is totally offtopic. But in the Hello World screenshot (http://qtcsharp.sourceforge.net/snapshot.png) there is an xmms... thing in the toolbar of kate. Is this application specific? What is the source?\n\nI don't think it's xmms-kde, but I could be wrong.\n\nThe screenshot is at \n\nAnd to keep things vaguely relevant...\n\nThe speed of open source & especially KDE development never ceases to amaze. I guess a tower is much easier to vuild when the foundations aren't wrapped in bubble wrap.\n\nAlex W"
    author: "moreati"
  - subject: "Re: What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: "No, that's just regular old xmms minimized and placed in that location by me for easy access when working ;-)"
    author: "Adam Treat"
  - subject: "Re: What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: "Incredible, it looks so integrated. Although thinking about it, the reality of a toolbar embedded xmms would be fairly limiting. Forget I mentioned it.\n\nAlex"
    author: "moreati"
  - subject: "Re: What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: "A few of us once considered writing a KWin plugin that was also a Noatun plugin...\n\nNow that Noatun has a dcop interface it'd actually be easy to do.\n\nHow's that for integration? :-)\n\nHmm.. you could do toolbar integration too by writing a custom widget style..."
    author: "Neil Stevens"
  - subject: "Re: What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: ".. but then we'd have to use noatun.\n\nno offense, but an analogy of wmp::winamp on the windows side to noatun::xmms can be drawn; that is, noatun is just simply too heavy to deal with the everyday and mundane task of playing mp3s and just maintaining a userlist.\n\nalso, noatun needs to have better video support, imho.. something like mplayer (cross-platform) or aviplayer(x86-only)."
    author: "dc"
  - subject: "Re: What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: "This is what is known as a complaint. There are differences between a complaint and a suggestion. A complaint is ambiguous, unhelpful, based on obsolete and remote evaluations, and put in the wrong place. A suggestion is none of these things. Thank you (steps off soapbox)"
    author: "Carbon"
  - subject: "Re: What is that xmms widget in the screenshot?"
    date: 2002-03-18
    body: "Oh man, please do!  That would be awesome.  I go back and forth between Noatun and XMMS right now, but I think it will be Noatun when KDE3 comes out.  Can't wait to see your Hayes playlist ;-)"
    author: "Adam Treat"
  - subject: "Good work"
    date: 2002-03-18
    body: "Glad to see you guys at the KDE side of the fence\nget your share of languages. I'm sure it will bring\nmany new developers to your cause."
    author: "21mhz"
  - subject: "Newbie question"
    date: 2002-03-18
    body: "I've tried to understand what Mono offers, but I'm confused about what these bindings provide relative to .NET. (I looked into installing Mono and trying it out but was scared off by the requirement to upgrade glib. I've had enough experience updating GNOME libraries to be afraid of that.)\n\nThe point of .NET is what the early promise of Java was, right? It creates a portable executable that works on any platform with the right VM. So...\n\n1) Will Mono provide the ability to run .NET executables?  If so, using what widgets?\n\n2) Is the goal of the Gtk and Qt bindings to C# to make portable bytecodes (or whatever is used) that will run on other systems where those toolkits are available? Or are they just alternatives to using C or C++ to make compiled native executables, just using a different language?\n\n3) In the big picture, if .NET takes off and we'll be needing to run .NET executables on Linux, what will we presumably be using to do that?\n\n"
    author: "Otter"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-03-18
    body: "First off, see Ximian's Mono site for accurate answers. Short answers follow.\n\n1) Yes. GTK+/GNOME or Qt.\n\n2) The bindings allow a \".NET\" program to say \"make a button\" and get a (GTK+/Qt) button. Unlike (non-GNOME-ified) Java, where all platforms use the Java Swing widgets, Mono will use \"native\" widgets. (Incidentially, Java can display GTK+ widgets in lieu of the Swing widgets as well.)\n\n3) Mono. More precisely, a Mono-based bytecode interpreter and possibly a special \"gcc .NET\" with the capability to compile .NET bytecodes into native binary on a specific OS/CPU combination. For example, one might download a .NET application and compile it for Linux 2.6 on a PowerPC G5 to avoid the overhead of bytecode interpretation. (Since both Java and Perl manage to run pretty decently WITH bytecode interpretation, it's unlikely to be a major necessity.)"
    author: "Anonymous Coward"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-03-19
    body: "> (Since both Java and Perl manage to run pretty decently WITH bytecode interpretation, it's unlikely to be a major necessity.)\n\n*shrug* I guess that's why most end-user apps are made with C/C++/Object Pascal (Delphi)."
    author: "dc"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-03-19
    body: "I thought Visual Basic was quite popular.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-03-19
    body: "From looking at the code, it seems that the \"make a button\" and get a (GTK+/Qt) button scenario is still a dream.  If you have to say \"make a Qt button\" then it is pretty specific to Qt and KDE.\n\nFrom my understanding, the MONO folks are writing things so that you can write an application on Windows that will use Win32 widgets on that platform but use GTK+ widgets when run on Linux.\n\nIt would be great if you could just specify Qt or GTK+ as a default on the Linux side and truly have one codebase to execute on all three platforms (Win32, Gnome, and Qt).\n\nDoes anybody know if this is being worked on?"
    author: "Justin Malcolm"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-03-19
    body: "The relevant API is called System.Windows.Forms, and is the equivalent to Java's AWT. The current plan is to get the Gtk+ and Qt bindings into shape before writing an API wrapper to access them via System.Windows.Forms."
    author: "Richard Hestilow"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-03-20
    body: "What I liked from here, that I can compile C# into native code either it is Win32 or Linux - it is superb!\n\nJava has no such feature, if only it had!"
    author: ".coder"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-04-08
    body: "Great news! That's exactly what Java does. Unless you explicitly force java.compiler=NONE on startup, you're using the native code compiler (Hotspot). \n\nSee <a href=\"http://java.sun.com/docs/hotspot/PerformanceFAQ.html\">the Hotspot FAQ</a>.\n"
    author: "Babbacombe"
  - subject: "Re: Newbie question (Mono)"
    date: 2002-04-08
    body: "Great news! That's exactly what Java does. Unless you explicitly force java.compiler=NONE on startup, you're using the native code compiler (Hotspot). \n\nSee <a href=\"http://java.sun.com/docs/hotspot/PerformanceFAQ.html\">the Hotspot FAQ</a>.\n"
    author: "Babbacombe"
  - subject: "Re: Newbie question"
    date: 2002-03-19
    body: ">The point of .NET is what the early promise of Java was, right? It creates a portable executable that works on any platform with the right VM. So...\n\nThe idea of Mono is to achieve something like the (relative) platform-independence of Java.\nNow, I don't consider Microsoft evil, but it is not in their interest, and certainly not in their game plan for .NET to promote cross-platform portability despite whatever announcements are being made.\nYou'd be very na\u00efve to believe that MS would not block any move that threatened their profits of Windows, so go figure what the point of .Net is."
    author: "Non-believer"
  - subject: "I don't understand everything..."
    date: 2002-03-20
    body: "Hi\n\nI read that KDE will have a C# binding but I think KDE/C# applications won't feature as many functionalities as other KDE apps. I say that because I don't think C# is able all that KDE can do and vice-versa. For example, how will I include a khtml part into my app in KDE using C# ? However, if it's possible, it must not be portable, is it ?\n\nIs it just a QT/C# binding or will we be able to convert any KDE app to C# ?"
    author: "Julien Olivier"
  - subject: "Re: I don't understand everything..."
    date: 2002-03-20
    body: "\"Is it just a QT/C# binding or will we be able to convert any KDE app to C# ?\"\n\nAdam is hoping to have some KDE C# bindings ready for KDE 3.1. If you want portable code, then just stick to the Qt api, or if you want maximum functionality and desktop integration then use the Qt/KDE api. The choice is exactly the same as if you were writing code in C++.\n\nThe KDE C# code will look much the same as the Java bindings code. Here is an example of loading a KHTMLPart in Java:\n\n      khtmlpart = new KHTMLPart(this);\n      khtmlpart.begin();\n\n      khtmlpart.write(\"<HTML><BODY><H1>KSimpleBrowser</H1>\" +\n                        \"<P>To load a web page, type its URL in the line \" +\n                        \"edit box and press enter,</P>\" +\n                        \"</BODY></HTML>\");\n\n      khtmlpart.end();\n\nThis is from the from the KDE 2 Development book KSimpleBrowser example translated to Java. It's in the cvs under kdebindings/kdejava/koala/test/simplebrowser.\n\n-- Richard\n"
    author: "Richard Dale"
---
Today marks a special coincidence.  First,
<a href="mailto:manyoso at yahoo.com">Adam
Treat</a> released the initial version of
<a href="http://qtcsharp.sourceforge.net/">Qt bindings for C#</a>,
which consists of 476 Qt classes converted to C#.
The bindings work with the <a href="http://www.go-mono.com/">Mono</a>
compiler, runtime environment and class libraries, enabling a fully Open Source implementation of C# for Qt.  While not yet ready for
a real application, Adam has managed to write and execute a
Hello World! program
(<a href="http://qtcsharp.sourceforge.net/snapshot.png">screenshot</a>).
KDE bindings are on the drawing board.  Shortly thereafter,
<a href="mailto:phil at river-bank.demon.co.uk">Phil Thompson</a>, <A href="mailto:jbublitz@nwinternet.com">Jim Bublitz</a> and
<a href="http://www.thekompany.com/">theKompany.com</a>
released <a href="http://www.thekompany.com/projects/pykde/">KDE 2 and KDE
3 bindings for Python</a>.  Together with the Java, Objective C and C bindings in the
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/">kdebindings</a>
module, as well as the
<a href="http://sfns.u-shizuoka-ken.ac.jp/geneng/horie_hp/ruby/index.html">Ruby
bindings</a>, KDE is providing developers a broad gamut of application
development languages.  Great work, Adam, Phil and Jim!
<!--break-->
