---
title: "Dot Series: Creating Konqueror Service Menus"
date:    2002-07-24
authors:
  - "aseigo"
slug:    dot-series-creating-konqueror-service-menus
comments:
  - subject: "Almost off topic"
    date: 2002-07-23
    body: "\nThe fact that you dressed your two year old son up as a penguin for halloween almost scares me.  :)\n\nGreat article.  I used it to enhance that project I was asking questions about in irc.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Almost off topic"
    date: 2002-07-23
    body: "but he's so damn cute as a penguin! i'm hoping that suit will fit him again this year, though i'm doubtful. hrm.. time to start thinking of something equally strange for this October .......\n\nas for the article, i've added two tips to it in response to emails i've received: one for how to make a servicemenu appear for directories (inode/directory as the mimetype) and what to do when you have multiple commands to run (exec a shell! Exec=/bin/sh -c \"whatever; whatever && whatever\")\n\nthese additions should be reflected in the online article shortly..."
    author: "Aaron J. Seigo"
  - subject: "Re: Almost off topic"
    date: 2002-07-23
    body: "It might be worth adding a note about using the UIServer dcop interface to display dialogs too.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Almost off topic"
    date: 2002-07-24
    body: "well, then it starts becoming a tutorial on scripting KDE using dcop rather than a tutorial on making servicemenus. i dipped very briefly into kdcop only to demystify how i got the Exec line in the example.\n\na general \"how to script kde\" would make for a great tutorial, though. the topic is large enough to deffinitely warrant its own coverage. i polled people on irc the other evening for tutorial topics that they would like to see, and dcop scripitng was the first one they asked for nearly every time. =)\n\nmaybe next month....."
    author: "Aaron J. Seigo"
  - subject: "Re: Almost off topic"
    date: 2002-07-25
    body: "I guess you mean something like the one Olaf Zanger wrote see http://dot.kde.org/1001577974/ ;-)\n\nRich."
    author: "Richard Moore"
  - subject: "Re: Almost off topic"
    date: 2002-07-25
    body: "sort of. but a little less terse, a little more end-user oriented and updated to reflect the newer capabilities of the dcop command line app as well as the newer ones (e.g. dcopfind, dcopref) ... maybe some screenshots of kdcop in action, walking through writing a script step by step ... olaf's paper is great, its just not very accessable to end users and it's a little dated."
    author: "Aaron J. Seigo"
  - subject: "Re: Almost off topic"
    date: 2002-07-24
    body: "That sounds bloody brilliant.  Why don't more people know about this?  I tried this and it worked perfectly!\n\ndcop kio_uiserver UIServer messageBox 1 1 \"KDE Rules!\" \"Cool\" \"yes\" \"yes!\"\n\nI don't know why this one doesn't work:\n\ndcop kio_uiserver UIServer infoMessage 1 \"KDE Rules\"\n"
    author: "ac"
  - subject: "Re: Almost off topic"
    date: 2002-07-24
    body: "# dcop kio_uiserver UIServer infoMessage 1 \"KDE Rules\"\nNot enough arguments.\n\n'nough said..."
    author: "ineiti"
  - subject: "Re: Almost off topic"
    date: 2002-07-24
    body: "> dcop kio_uiserver UIServer infoMessage 1 \"KDE Rules\"\n\nyou need a running job to show the message in its progress dialog for this to work AFAIK."
    author: "Aaron J. Seigo"
  - subject: "Re: Almost off topic"
    date: 2005-03-29
    body: "danke"
    author: "tuncer"
  - subject: "Re: Almost off topic"
    date: 2002-08-08
    body: "My first thought was \"Execute with arguments...\" but I can't figure out how to handle the dialogs through DCOP. I would love to see a dcop tutorial."
    author: "Chuckie Schumer"
  - subject: "Re: Almost off topic"
    date: 2002-07-24
    body: "One thing I would like to know is if there is posible to group services created by the user un a submenu. this way you could avoid clutter if you have many services for an espcific mime type "
    author: "Mario"
  - subject: "Re: Almost off topic"
    date: 2002-07-25
    body: ">but he's so damn cute as a penguin! i'm hoping that suit will fit him again this year, though i'm doubtful. hrm.. time to start thinking of something equally strange for this October .......\nWill Konqui do? :-) or some BSD demon?"
    author: "Alex"
  - subject: "Re: Almost off topic"
    date: 2002-07-24
    body: "> The fact that you dressed your two year old son up as a penguin for halloween almost scares me. :)\n\nYou should've seen me in one, I gotta admit I don't look as cute, but hell, I turn some heads on the streets in it. \nAaron, don't know your wife, but  tell her another vegan says hi :) \nHow do you find the time to do everything you do? KC-KDE, tutorials and huge amounts of super high quality code. Mad props to you for what you do!"
    author: "Zack Rusin"
  - subject: "Exellent!!"
    date: 2002-07-24
    body: "Been wondering about how to do right click actions in KDE, and all along it's been so easy :-)\nWill these articles go into the help system? They sure are useful."
    author: "Fredrik C"
  - subject: "Wow"
    date: 2002-07-24
    body: "This was one of the most interesting bits of information I've read about KDE for a while.  I thought I was going to be thinking up stranger and stranger things to do with servicemenus but found myself absorbed by the dcop scripting possibilities instead.\n\nNow I've tried using this dcop thing, it seems to me like it's the KDE killer app.  People need to know about this stuff!\n\nThanks for all the efforts Aaron and the rest of the KDE crowd.\n\nRegards"
    author: "Wheely"
  - subject: "Re: Wow"
    date: 2002-07-25
    body: "Yeah, dcop is amazing. I've been using it in combination with lineak (http://lineak.sourceforge.net/) to enable me to use the \"internet keys\" on  my logitech wireless keyboard. For instance, I can make the \"Media\" button popup my noatun playlist, and then when I press it again it hides itself! Dcop  scripting sets KDE apart, IMHO. Do any other desktops have something like this?"
    author: "Matthew Kay"
  - subject: "Re: Wow"
    date: 2002-07-25
    body: "i remember something vaguely about applescript, which also kicked serious scripting ass. at least at the time; it's been several years since i was in the mac world."
    author: "Aaron J. Seigo"
  - subject: "service-menu repository?"
    date: 2002-07-24
    body: "BTW: is there any central repository where common service-menus are collected?\nWould be a great thing for \"wow, i always wanted to have this!\"-scripts with small efforts but big effects..."
    author: "Sebastian Eichner"
  - subject: "Re: service-menu repository?"
    date: 2002-07-24
    body: "not that i know of. you could always start one, though =) ...\n\nif people want to send me the servicemenus they come up with i'll make sure they get up on the web and categorized somewhere.\n\nif we do get a bunch of these together then we'll want a small manager interface for them (turning them on/off, installing new ones, etc) which shouldn't be difficult to do."
    author: "Aaron J. Seigo"
  - subject: "thanx "
    date: 2002-07-24
    body: "very!! usefull article.\ni will try it out @ home today\nthanx.\ngunnar"
    author: "gunnar"
  - subject: "Applying a menu option to all file mime types?"
    date: 2002-07-24
    body: "WOW ! Really nice article! I am currently working on a gpg frontend + was able to  integrate it to konqueror, adding options to encrypt/decrypt files, but i have one question: \n\nI would like to have my Encrypt file action applied to all file mimetypes.\nIs there an easy way to do it ?\n\nThanks a lot !"
    author: "y0k0"
  - subject: "Re: Applying a menu option to all file mime types?"
    date: 2002-07-24
    body: "ServiceTypes=allfiles\n\nit would be nice if one could also do things like image/*"
    author: "Aaron J. Seigo"
  - subject: "Re: Applying a menu option to all file mime types?"
    date: 2002-07-25
    body: "image/all might work.\n\n"
    author: "Waldo Bastian"
  - subject: "Re: Applying a menu option to all file mime types?"
    date: 2002-07-25
    body: "unfortunately no. i tried that. =("
    author: "Aaron J. Seigo"
  - subject: "Re: Applying a menu option to all file mime types?"
    date: 2002-07-24
    body: "ServiceTypes=allfiles\n\nDawit A."
    author: "Dawit A."
  - subject: "How would you?"
    date: 2002-07-25
    body: "How would you write an action which brings the image up as an attachment in a kmail composer?"
    author: "TCorbin"
  - subject: "Re: How would you?"
    date: 2002-07-25
    body: "/me opens up kdcop and explores for a few seconds\n\nthis one looks promising:\n\ndcop kmail KMailIface openComposer to cc bcc subject body hidden messageFile attachURL\n\ni imagine you'd call it something like:\n\ndcop kmail KMailIface openComposer '' '' '' '' '' 0 '' %u\n\nthis is completely untested but might get you on your way =)"
    author: "Aaron J. Seigo"
  - subject: "Re: How would you?"
    date: 2002-07-25
    body: "THANK YOU!\n\nNot only for this helpful reply, but for all the great work you and\nthe other kde folks are doing.   I absolutely LOVE kde.   I think kde\nlives up to the promise of the original mac.  I haven't been so excited\nabout computers in a long time.\n\nIt's funny, but one of my favorite things about KDE is the file open/save dialog.\nAbsolutely head and shoulders above anyone else's and it really makes a huge\ndifference is eas of use.\n\nSo anyway - thanks."
    author: "TCorbin"
  - subject: "Re: How would you?"
    date: 2002-07-25
    body: "I tried your example, but it didn't work, so I checked it out with some file in a shell and I got \"cannot handle datatype 'KURL'\".\n\nHow to pass a file using a url of that type?"
    author: "ronino"
  - subject: "Re: How would you?"
    date: 2002-07-25
    body: "the current version of dcop apparently doesn't support KURL via the command line. =(\n\ni talked w/waldo about it on IRC today and he's fixed it. now that it is in CVS i suppose we can expect a dcop command line tool that does KURLs in the upcoming beta =)"
    author: "Aaron J. Seigo"
  - subject: "further scripting : HTML changes with javascript"
    date: 2002-07-25
    body: "Hi,\n\tAfter seeing this article I started to play around with Konqueror a\nsmall bit and even though my javascript knowledge is next to non-existant\nI was able to do a few things ( obviously you need to fill in XXXX and YYYY ):\n#change the status bar\ndcop konqueror-XXXXX html-widgetYYYY  evalJS 'window.status=\"xxxx\"'\n\n#change the background color\ndcop konqueror-XXXXX html-widgetYYYY  evalJS 'document.bgColor = \"blue\"\n\n#popping up an alert box\ndcop konqueror-XXXXX html-widgetYYYY  evalJS \"alert(\\\"my message\\\")\"\n\n#disable javascript for this window \ndcop konqueror-XXXXX html-widgetYYYY setJScriptEnabled false\n\n#set the value of the email field in the form when you are\n#in the \"Post Reply\" e.g. http://dot.kde.org/1027447529/addPostingForm\ndcop konqueror-XXXXX  html-widgetYYYY evalJS \"document.forms[0].email.value=\\\"cph\\\"\"\nWith the above you could easily have a small piece of javascript to set\nthe username and password on those sites where they do not use cookies\n(or where you do not trust them with cookies).\n\nTo find out which konqueror html-widgetYYYY you should be using use:\ndcop konqueror-XXXXX  html-widgetYYYY evalJS \"document.title\""
    author: "CPH"
  - subject: "DCOP"
    date: 2002-07-25
    body: "Hrmmm... This is all grand. Seriously, this rocks.\n\nBut I am wondering if there is something like this for Gnome.\nIt would be a good thing to make DCOP an independent thing (unless it is already and I am missing out on stuff) and make it work anywhere, KDE, Gnome, or a non-GUI environment.\nAnd it would be a good thing, arguably, to make DCOP interface less binary-dependent, maybe?\n\nKDE ownz!"
    author: "MyIS"
  - subject: "Re: DCOP"
    date: 2002-07-25
    body: "> But I am wondering if there is something like this for Gnome.\n\nhttp://g-scripts.sourceforge.net/"
    author: "Anonymous"
  - subject: "Re: DCOP"
    date: 2002-07-25
    body: "he's talking about scripting w/dcop (generic, desktop-wide scripting), not application specific hacks.\n\nlooking over that page, however, i see some interesting things about Nautilus that i wasn't aware of, such as: \n\nyou need scripts to open files in a given editor? (Konqi just uses mimtypes and offers an Open With-> menu)\n\nshowing special file info is done with scripts? (Konqi provide kfilemetainfo, which are actual coded plugins, have their UI integrated w/Konqi and are available to all KDE apps ... hmmmm... perhaps that could be the another article: writing KFMI plugins?)\n\nhow do you ensure a script is only available for a given mimetype (e.g. is hidden when it doesn't apply)?\n\n\np.s. this isn't meant to bash nautilus at all, these are honest questions i have after reading that page.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: DCOP"
    date: 2002-07-26
    body: "My point is that advanced IPC like DCOP should be separate from a particular desktop suite. Then, competition and natural selection is much more focused - I'd like to be able to pick best parts of all OSS offerings instead of having my choice limited to the two camps."
    author: "MyIS"
  - subject: "Re: DCOP"
    date: 2002-07-27
    body: "the dcop library has bindings to several languages and has very few dependencies itself. AFAIK the only thing it requires is X11 and it's ICE auth mechanism. so pretty much anyone could use it if they were so inclined."
    author: "Aaron J. Seigo"
  - subject: "Re: DCOP"
    date: 2002-07-26
    body: "What you are looking for is Bonobo... it combines the speed of DCOP with the scriptability of KParts."
    author: "ac"
  - subject: "Re: DCOP"
    date: 2002-07-26
    body: "*LOL*"
    author: "ac"
  - subject: "Changing other menus"
    date: 2002-07-26
    body: "Is it possible to change for example the 'copy here/move here/link here' menu that pops up when you drag and drop a file to another directory?\n\nJohan Veenstra"
    author: "Johan Veenstra"
  - subject: "Re: Changing other menus"
    date: 2002-07-26
    body: "No, but that one is in kdebase/libkonq/konq_operations.cc, you can change it there ;)\n(what do you want to change about it?)"
    author: "David Faure"
  - subject: "Re: Changing other menus"
    date: 2002-07-27
    body: "> what do you want to change about it?\n\nDisable it because it is bothersome? You almost always want to =move= files. A modifier that allows to copy instead for the very rare cases where you want just that would suffice. As it is now, it looks like newbie stuff that bothers experienced users. And, yes, I know that I can control the behavior by modifiers now."
    author: "Melchior FRANZ"
  - subject: "Re: Changing other menus"
    date: 2004-10-12
    body: "It would be great to be able to change the \"Link Here\" behavior to make a hard link rather than a symlink (or maybe add another option)\n\nAssumably to make such a change, I'd have to change the source, and recompile. What kind of undertaking is this? I'll keep looking, but any links to konqueror compilation instructions would be great!\n\n~David.\n"
    author: "David Emerson"
  - subject: "Re: Changing other menus"
    date: 2004-10-12
    body: "(you're replying to a two-year-old thread, but nevermind :)\n\nhttp://developer.kde.org/source for how to get the latest sources and compile them.\nkdebase/libkonq/konq_operations.cc for where the symlink operation is called. You would have to special case local files and avoid calling KIO::symlink for them, and call link(2) instead. KIO has no support for hardlinks.\nAlso note that the operation will fail across filesystems..."
    author: "David Faure"
  - subject: "Service Menus and URLs"
    date: 2002-07-27
    body: "Creating service menu entries for file types is very cool...great article.  But here's a question for the KDE gurus.  How do you create a service menu that pops up when you right click a URL?\n\nIs this even possible?  \n\nThanks\n\nSD"
    author: "SD"
  - subject: "Splendid!"
    date: 2002-07-28
    body: "Finally I can add menus like \"Add to ZIP\" or \"Add to TGZ\"!"
    author: "Maciek"
  - subject: "Re: Splendid!"
    date: 2003-08-31
    body: "Hello\nI found a comment of yours which leads me to beleive that you know how to add functions to the right mouse click in konqueror. PLEASE tell me how. Specifically i want to add the funcions: add to zip, create zip and the same for tgz.\nthanks a million for your help.\nAyudame"
    author: "ayudame"
  - subject: "It didn't work."
    date: 2002-07-29
    body: "I followed the steps. I've got RH7.3 unofficial install of v3.0.2-0unl.  It has the ark desktop definition in /usr/share.  But the directory didn't even exist in ~/.kde/.  Is there some funky permission thing going on?\n\nMy file is:\n\n[Desktop Entry]\nServiceTypes=image/png,image/jpeg,image/gif\nActions=setAsWallpaper;setAsTiledpaper\n\n[Desktop Action setAsWallpaper]\nName=Set As Background Image\nIcon=background\nExec=dcop kdesktop KBackgroundIface setWallpaper %u 6\n\n[Desktop Action setAsTiledpaper]\nName=Set As Tiled Background Image\nIcon=background\nExec=dcop kdesktop KBackgroundIface setWallpaper %u 2\n\n\nAlso, the article said you can edit the file in place and it would automatically update the menu. But, I'm not seeing that happening.\n\nThanks."
    author: "jp"
  - subject: "RCS"
    date: 2002-07-30
    body: "is it possible to use this function together with RCS.\nFor example when i have worked with a file I only hace to \nright click on the file and press \"New version\" or something like that,\nwrite a description of the file and so on. \n\nci -u %u \n\nwould be the command that I want to run I guess but I also want\nto add a comment for the change in the file. Is there any solution for \nthis?\n\nFeel free to mail me a solution\n\n/Andreas\n"
    author: "Andreas Henningsson"
---
The ability to select mimetype-specific actions from Konqueror's context menu is an oft-requested feature. The pleasant surprise is that this is already possible. The even more pleasant surprise is that you don't need to be a software developer to do it. <a href="http://developer.kde.org/documentation/tutorials/dot/servicemenus.html">This article</a>, the fourth in the <a href="http://developer.kde.org/documentation/tutorials/dot/">dot tutorial series</a>, details step-by-step how to quickly and easily add new actions to Konqueror's context menu.
<!--break-->
