---
title: "Outlook Competition: Enter Kroupware and Kaplan"
date:    2002-09-23
authors:
  - "cmiramon"
slug:    outlook-competition-enter-kroupware-and-kaplan
comments:
  - subject: "Just one thing..."
    date: 2002-09-23
    body: "Please, please, please let us users choose how we want our windows. KNode lets you do this and it is great.\n\nI want to have my message window tall (as high as the screen) and the folder-list and the message-list on a side.\n\nSince most lines are wrapped at 80 characters, this is the only layout that makes sense to me. Having an Outlook-like wide message window that is always half-empty is not efficient. KNode features these \"sub-windows\" which can be arranged how you like it, if Kaplan will support this, this feature alone will put it far ahead of current KMail.\n\n"
    author: "Roland"
  - subject: "Re: Just one thing..."
    date: 2002-09-24
    body: "Agreed.\nI hope it will still be posible to have the \"normal\" kmail, as I dont't like that outlook-like interface."
    author: "renaud"
  - subject: "Re: Just one thing..."
    date: 2002-09-24
    body: "Of course, KMail and the other programs will still be startable as separate programs."
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Just one thing..."
    date: 2002-10-16
    body: "I agree with this and also use a similar window arrangement.\nJust letting you know that there is more than one person with the same \"wish list\".\n\nNard\n"
    author: "Nard"
  - subject: "Kaplan is most excellent"
    date: 2002-09-23
    body: "I was worried when I looked over Kroupware, as I'm working on a non-folder based alternate email/messaging/contact application for KDE.  It would plug into Kaplan quite nicely (or run as a seperate application), but Kroupware looks like you're locked into it's variant of KMail.  Moving to a large monolithic application that does a whole bunch of stuff (like Outlook) prevents you from picking and choosing components - your favorite addressbook with your favorite email application.\n\n(As an aside: Yeah - that's right, non-folder based.  But not flat.  It's a relational message database with a novel (I think) UI that is pretty nifty.  No clue if I can climb it up to version 1.0, right now I have pages of notes, a KWord document full of documentation, and a dozen PyQt files that semi-work as a bad text editor that saves to a mailbox format, and makes little icons of people pop up when I create files.  :)  If you're interested in discussing some interesting alternative concepts about how to build a communication interface, which I've dubbed KIM (KDE Integrated Messaging), feel free to email me.  I'm hoping it might debut around the time KDE 4.0 is released).\n\n--\nEvan (been awake over 24 hours now - please excuse any run on or fragment sentences - now, back to work)."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kaplan is most excellent"
    date: 2002-09-23
    body: "As I understand it, the first version of Kroupware is a stepping stone.  Sounds reasonable, given their schedule."
    author: "Anonymous"
  - subject: "open source and money"
    date: 2002-09-23
    body: "This is wonderful!  The german government is doing a great service to Open Source by funding the Kroupware/Kolab project like this.  Fingers crossed till Oct 15.  This is a greatly compressed schedule, and if they pull it off, it will be thanks to the pre-existing work of the KDE project.  Everybody wins."
    author: "Anonymous"
  - subject: "Re: open source and money"
    date: 2002-09-24
    body: "It is quite essential that it is not(!) a funding but rather\na regular contracted work. This proves even more the viability of\nFree Software!"
    author: "Jan-Oliver Wagner"
  - subject: "Athera"
    date: 2002-09-23
    body: "Wasn't TheKompany working on Athera a year ago? (and it was supposed to compete with Evolution?) Where is it now?"
    author: "Stof"
  - subject: "Re: Athera"
    date: 2002-09-23
    body: "It's a Qt project now.  Doesn't use KParts, etc."
    author: "ac"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "actually 1.0 is just about ready, we took a few months off to port it to the Sharp Zaurus and that project is in final beta.  It uses some really cool technology to deal with plug ins and components in a multi-platform fashion, which I understand is a goal of kroupware (croup by the way means horses ass or bad cough, both are valid definitions.  The word comes from the French actually which in turn is based on old German).  I don't know why no one wanted to talk to us, we've put forward this proposal many times, oh well."
    author: "Shawn Gordon"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "One reason is maybe that its development is not open and afair no source code was available for 0.9.9."
    author: "Anonymous"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "sure it's open development, just no one has been jumping in to help and the source is available."
    author: "Shawn Gordon"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "So what is the CVS repository with the current post-0.9.10 version?"
    author: "Anonymous"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "Since it looks like they want the final product to be native to KDE, I image they want to use KParts for components.  Maybe there would even be some interest in using your component model as well.  The ability to use a KPart as an Athera component or vice-versa could be a useful addition to both projects I would think."
    author: "anon"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "It's Aethera, dammit."
    author: "Anonymous"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "Aethera use to use KParts, but those weren't multi-platform, which is the same problem that Kroupware is going to run in to.  We switched to TINO (our OLE type tool) to replace it, and that is multi-platform and lightweight."
    author: "Shawn Gordon"
  - subject: "Re: Athera"
    date: 2002-09-24
    body: "Well, since KParts are all GPL'd (LGPL'd) I think they will eventually become cross-platform, when required."
    author: "KDE User"
  - subject: "Re: Athera"
    date: 2002-09-25
    body: "Exactly. It's just done when it's needed ;)\nAnd, btw, no one didn't replay to the \nquestion of the repository of that theKompany \nproject.\n\nCheers,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: Athera"
    date: 2002-09-25
    body: "I'm just heading out for a trade show, I've asked the engineer to respond with the system he has in place.  Aethera is a huge project so it's important to stay properly organized."
    author: "Shawn Gordon"
  - subject: "Re: Athera"
    date: 2002-09-28
    body: "Hi,\nFirst of all, I want to let you know that the sources are available \nhttps://sourceforge.net/project/showfiles.php?group_id=22463\nI even tried to offer a SRPM (i386 only) package to avoid some QT style problems and to simplify the building process.\n\nThe CVS is open only for people who really want to develop (help) the Aethera project. Since the project is pretty big I want to be carefull with the code.\nI am opened for anyone who wants to help, I will give him a CVS access and lot of info about the design.\nMy email is: eug@thekompany.com\n\nAnd I am working for the first stable release (1.0).\nRegards,\nEug\n"
    author: "Eug"
  - subject: "WOW.... I can't wait"
    date: 2002-09-23
    body: "This is the most exciting KDE event since KDE 2.0!\n\nKDE 3.1-beta series\nKDE 3.0-series\nKOffice 1.2\n\nI'm blown away!"
    author: "Jeff"
  - subject: "kroupware"
    date: 2002-09-23
    body: "I like the idea, I hate the name (kroupware?!?!?)"
    author: "fault"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "As you can read on that page, \"Kroupware\" is just a title for the current project:\n\nhttp://mail.kde.org/pipermail/kroupware/2002-September/000019.html\n\nNice evening\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "OK, that's good news.\n\nSince I was complaining about the name \"puic\" a couple of stories ago, I was reluctant to do the same thing again, but am glad others find \"kroupware\" unappealing.\n\nIn English, by the way, \"croup\" refers to a potentially fatal respiratory disease in babies.\n\nAgain, a great project, just like the Perl bindings -- just want to make sure they don't get saddled with a name that frightens users!"
    author: "Otter"
  - subject: "learn from GNOME"
    date: 2002-09-23
    body: "Hey.  KDE should learn from the GNOME project and assign a kick ass name to the project *first*.  And when I say *first*, I mean before any code has been written down at all.  Seriously.  Eliminate these half-assed temporary names!  ;-)"
    author: "ac"
  - subject: "Re: learn from GNOME"
    date: 2002-09-23
    body: "> KDE should learn from the GNOME project and assign a kick ass name to the project *first*.\n\nFirst assign a name and then begin creating the project? You mean like \"GNOME Office\"? :-)"
    author: "Anonymous"
  - subject: "Re: learn from GNOME"
    date: 2002-09-24
    body: "hehe, touch\u00e9\nI don't mean to offend gnomers, as I am both a gnome and kde user, but this one was touch\u00e9 :)"
    author: "dwt"
  - subject: "Re: learn from GNOME"
    date: 2002-09-24
    body: "The problem is that when the project fails you have one name less to chose.. and it is always emberassing to donate even more vapourware to the world. But the Gnome have some experience with this as well..."
    author: "AC"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "AFAIK, name suggestions are wellcome ;-) It is supposed to be renamed for the final release.\n"
    author: "Christoph"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "I hereby nominate Kinship, nickname \"Kin\".\n\n--Dan\nwww.doxpara.com\n"
    author: "Dan Kaminsky"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "How about Kapable?"
    author: "J\u00f8rgen"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "How about anything that doesn't begin with a K."
    author: "jmalory"
  - subject: "Re: kroupware"
    date: 2002-09-23
    body: "How about \"Longsight\"? A \"konqueror\" is more powerful than an \"explorer\" and you can get more information with a \"long sight\" than with just an \"outlook\".\n\nWhat do you think?\n\nOnly an idea (I posted it to the mailing-lists as well)\nPietz\n"
    author: "Andreas Pietzowski"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I second this suggestion. The \"Everything has to start with K\" thing is getting a bit old."
    author: "Vic"
  - subject: "Kroupware name"
    date: 2002-09-24
    body: "Kroupware is partly German funded. Why not shake a little bit the tyranny of English and choose a German name for once ?\n\nI vote for Bund, it is a nice simple German word and the name of an interesting Yiddish Socialist Party of the 19th century in Eastern Europe.\n\nCheers,\nCharles"
    author: "Charle de Miramon"
  - subject: "Re: Kroupware name"
    date: 2002-09-24
    body: "\"Bund\" is also the name of the german government. I don't think that the german government (they started the project!) want their software to be called \"Bund\" as well ;-)\n\nPietz"
    author: "Andreas Pietzowski"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "\"Foresight\"...I think that's what you were searching for..."
    author: "rjamestaylor"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I like that a lot :)\n"
    author: "Richard"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "We have a winner! \"Foresight\" is an excellent name!"
    author: "Janne"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I agree - Foresight is an excellent name.\n\nOf course, it's pretty much a given that the kind of people who use terms like Winblows, Linsucks and MSTurd (instead of MSWord), will immediately dub it Foreskin.  Ah, the 3rd grade potty humor that has become accepted practice on the net.  What a planet.  :)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "Nah, that's just you Evan :)\n"
    author: "Richard"
  - subject: "Re: kroupware"
    date: 2004-08-20
    body: "Nah, i dont agree, id say year 7 (1st grade)"
    author: "Will"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "Komms (as in 'Comms Center')\n\nor possibly Sextant, to keep on your Longsight theme\n\nYou get more info out of a sextant and a good map than a simple longsight.\n\nHave fun,\nChris"
    author: "Christian Lavoie"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "Great idea.  This name sounds a lot more professional than the ones with a \"K\" inside.  Maybe the KDE team will get sued by K-Mart if it continues to use this letter in the names of their applications..."
    author: "Ned Flanders"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I don't see why the letter K should be avoided.  Many programs for KDE start with a K and many programs for Gnome start with a G.  Users can easily identify the origin of a program along these lines.\n\nI'd like to see a word that is present in many languages, has the same or an equal meaning in those languages and expresses a concept related to the program.  Since most european languages share a lot of latinisms, I think the most obvious denominator should be a word derived from latin.\n\nWhich brings me to my proposal.  How about \"Koalition\"?  It seems that the meaning is about equal in german, english, french, spanish and a few other languages.  The meaning, according to my ethymological dictionary, coming from late-latin (coalitus), new-english (coalition) and french (coalition), is \"union, unification\" and goes back to latin (coalescere, coalitum) where it meant \"to unite, to grow together\".  While it used to be a chemical terminus (< 18th century), it was soon adopted in international relations (mostly a coalition _against_ somebody or something).  In modern german it means \"alliance within a government\" (hope I translated that one about right).\n\nSince the purpose of a groupware is to work together, the meaning (grow together) is not far from the purpose.  It is some sort of coalition against the established closed source groupware solutions, so in this respect it is equally descriptive.  And since it is contract work for the government the modern meaning is also there.\n\nJust my 2 as"
    author: "Joerg Gastner"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "> and many programs for Gnome start with a G.\n\nIt was true for GNOME 1, but if you look at GNOME 2 you'll see huge changes. \"gedit\", \"GNOME Character Map\", \"GWhatever\": all gone and replaced by \"Text Editor\", \"Character Map\", etc.\n\nInside the KDE community, KNames are acceptable, but if you look around, you'll see Mac and Win people complaining all the time about obscure and non-descriptive K/G-names."
    author: "Stof"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "> all gone and replaced by \"Text Editor\", \"Character Map\", etc.\n\nYou're talking about how they appear in the menu/panel. The name of the executable and so on of course didn't change. KDE3 can display the generic name too."
    author: "Anonymous"
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "True.... exactly! The user should get an informative and pleasing name, and the packages could be whatever.  The only problem with that is... should the user know the package name too?  (For reasons like: support calls, download updates, etc.) If so, then does it help much? I'm not sure, but I think so. \n\nBut, I still think it is worthwhile to please the user a little better than we're doing now. Konqueror or Kontour can come across as a miss-spelling and an annoyance to english-speaking purists. K-mail & K-word, though, may not be as bad because its obviously branding like MS Word, or whatever.\n\nSo... my thought was totally in agreement with yours.  If the K-menu had a hook to the language-preferences, such that what you see is the \"whatever program name\" for your language it would be a lot easier to get names that please people.  But the package name is what it is: K-mail, or Konqueror or whatever."
    author: "Jeff"
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "Stof, you're an idiot and a troll who never dies.  GNOME2 stole this tactic from KDE.  KDE has been displaying the generic name in the menu *forever*.  Matthias Ettrich was the first to suggest this from the very beginning.\n"
    author: "ac"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "Here's a minor rant that I just posted to Slashdot.  I don't really have incredibly strong opinions on this, as I've written an application named Kodan (but I chose the names Contact and Chatter for two new ones I'm working on).\n\n---\n(Talking about Foresight as a name)\n\nPersonally, I like it better than Hola, Komoused, and other proposed names. Especially the latter, which has a forced 'K'. KMail, kvim, kpaint, etc, I can see having the K-prefix, as these are KDE GUIzed versions of standard applications (kmail vs. the *nix standard mail, kvim vs. vim, and kpaint vs. xpaint). Konqueror is an extension of the Navigator and Explorer naming theme, and Konsole was cute when it came out with a K. Since then, it's gotten quite old, and although some new applications are coming out with a K name (Kopete), for awhile now many major and/or core application have dropped the K prefix (Noatun, Brahams, Cervisia, PixiePlus, Quanta). The prefixing on a normal word still makes sense to me to generate a recognizable namespace - KWord, KSpread, KChart, KFormula, KThesarus, KDevelop - but the cutsie 'use a K instead of a different letter' is, imo, dumb.\n---\n\n    Again, it's my opinion, but not a very strong one, and has shifted in the past.  I'm of the opinion that kpaint should show up in the KDE menu as just Paint, and kmail should should up as Mail.  I.E., the naming prefix is for the binary application file, and the name of the application presented to the user should be simply functional.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I think you are right about the \"cute\" part.  And you are certainly right about many new applications having dropped the K prefix.  On the other hand this project is contract work for the BSI which is part of the german government.  And in german there are lots of words starting with K.  Most of these (the latinisms) are written with a C in french, english and spanish.  You mentioned the name \"Konsole\" being cute when it came out, but that is the german spelling for it.  Do I find \"Console\" cute just because that's the proper spelling in english and french?  Nope.  English is actually the lingua franca of information technologies.  No argueing about that.  But a work for the german government doesn't necessarily have to be named in english, french, spanish or whatever-language-you-can-think-of.\n\nWhat about these words (all correctly spelled according to my german dictionary).  Do they look cute, just because they are not english?\n\nKabarett, Kadenz, Kakao, Kalender, Kamel, Kamera, Kampagne, Kanal, Kantor, Karbon, Karneval, Katalog, Kiosk, Klasse, Knoten, Kobalt, Kodex, Kognition, Koinzidenz, Kollegium, Kommunikation, Kommunion, Kompass, Komponist, Konferenz, Konjunktion, Konkordanz, Konspiration, Kontakt, Kontext, Kontrast, Konversation, Konzept, Kooperation, Korrespondenz, Kybernetik.\n\nYou can probably guess the meaning of quite a few of these words.  Furthermore, the over 100 million german native speakers take these words as perfectly natural.  To them, they are not cute, they are normal.\n\nEnglish is the de facto language of computing.  Agreed.  But there are other languages on this planet.  And I can not see why an application that is being payed for by german tax payers shouldn't start with the (perfectly normal) letter K.\n\nJust my opinion."
    author: "Joerg Gastner"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I was unaware that \"Kroupware\" is a german word.  I apologize.\n\n(I did know that Konsole is german, and I simply didn't get into it on Slashdot, where a favored troll technique is to bash countries.)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "No need to apologize.  Kroupware is not a german word.  Quite on the contrary, it is a \"cute\" use of the letter \"K\" in an english word.  I agree with you on the issue of \"Kroupware\" being \"cute\" (Actually I think it is worse).  \n\nMy comment was in reply to to your Slashdot rant which began with the words \"(Talking about Foresight as a name)\"\n\n\"Foresight\" is not a german word.  :)\n\nJoerg"
    author: "Joerg Gastner"
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "As I said, intentionally incorrect words are the only thing that sometimes irk me.  K- as a prefix is fine (It's functionally similar to MS, Open Office or Toyota as a prefix, a la MS Word, Open Office Writer, or Toyota Camry).  It simply indicates the class of application (it's a GUI app that uses KDE libraries).  K as an incidental letter (a la Konsole, Kicker) or part of an acronym (KUPS, KIM) are also fine.  And, as I say earlier, I think that applications that fit into the first category (using K to indicate their class), should be listed on the Kicker menu without the K, as just Mail, Paint and Thesaurus, just as the common way to refer to my other examples drops the prefix (Word, Writer, Camry).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "You put it into the right words.  I agree with you.  Especially with the Mail, Paint and Thesaurus stuff that should be showing up in Kicker/Kmenu in a way that users can relate to. \n\nBut keep in mind that something that seems intentionally incorrect at first glance might come from a a language where it makes perfect sense.  \n\n"
    author: "Joerg Gastner"
  - subject: "Include specific names"
    date: 2002-09-26
    body: "I'm trying to push the adoption of Linux desktops, and I find that there are some mistakes:\n- You can't put something like \"Browser\" to identify Konqueror, because it brokes the conection with the help manuals and with the sites. \nI had a personal experience with this: I accidentally shut down Kicker. But this doesn't have a visible name, and I tried to find \"Panel\", and I couldn't. After some searching, I could figure that this was the app called Kicker on other parts, and I executed it. \nSo, I sugest don't give up individual and unique names, you can use \"Konqueror browser\", \"Kicker app panel\", or putting the unique name somewhere visible.\nNow, the \"advance\" user, this who is not afraid of configuring something new or learn a new app, can install him/herself a distribution. But then there are some problems: half-baked simplified interfases made their path from absolute beginner to \"intermediate\" user hard.\nThat's only to encourage the very good work that you are doing. I really miss Konqui when I'm on Win, there nothing like this.\n"
    author: "Andrea"
  - subject: "Re: Include specific names"
    date: 2002-09-30
    body: "This is the default since KDE3.0 -- i.e. you see KMail (Mail Client), Kate(Text Editor) in the menues. "
    author: "Sad Eagle"
  - subject: "Re: kroupware"
    date: 2003-05-13
    body: "Hey, i am new to this site. I am actually looking for a name for a garment firm starting with K or C(can be converted to K. It would be kind if u can help me in any way. Names could mean anything, upto the respondents discretion.\n\n\nPls do reply asap.\nThanking you, \nShreya Guha."
    author: "shreya Guha"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "And then we also have a name for version 2:\n\nKoalescence"
    author: "Jos"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "Maybe Kooperate ?\nKreative ?\nKontact ?\nKommunicate ?"
    author: "Martien"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "My vote: kooperate\n\nAhh, I like voting...when are the next elections :-))\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "how about something withOUT shitty usage of the word 'k'?"
    author: "uh"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "What about :\n-\"Hola\" (which is spanish and means \"Hello\". Don't use \"Ola\" since it's already registered by France Telecom)\n- \"Komesta\" ( which is spanish and stands for \"Como esta ?\". It means \"How are you?\")\n- \"What's up\"\n- \"Zoomin\"\n\n\n"
    author: "Philippe Piriou"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "i like hola.. k+something is fine, but some app names have gotten cheesy trying to put the k in. kroupware is a perfect example."
    author: "fault"
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "How about withOutlook? :P"
    author: "cosmo"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "How about Collaborator?\n"
    author: "Meretrix"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "Or Konnect"
    author: "Andy"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "kommunication centre "
    author: "nick"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "I would be cool if the name can be pronounced also by non english people :)"
    author: "JC"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "Yeah, just like \"Kodak\", which was chosen because it can be pronounced easily in almost any language, and it doesn't mean anything rude ;-) And it starts with a \"K\"!"
    author: "G"
  - subject: "Re: kroupware"
    date: 2002-10-06
    body: "And Kodak is a registered trademark :-P"
    author: "Jens"
  - subject: "Re: kroupware"
    date: 2002-09-24
    body: "What about\n\n  Kongress\n\nThe name's a bit anarchistic and the project _is_ commissioned by the government.\n"
    author: "Jos"
  - subject: "Re: kroupware"
    date: 2002-09-25
    body: "It is commissioned by a part of the government.  The Federal Bureau of Security in Information Technologies.  Not by Parliament/Kongress/Assembly.  And I don't understand why it is supposed to be anarchistic."
    author: "Joerg Gastner"
  - subject: "Re: kroupware"
    date: 2002-09-26
    body: "Insight\n"
    author: "Anonymous"
  - subject: "Re: kroupware"
    date: 2002-09-27
    body: ">>The name's a bit anarchistic and the project _is_ commissioned by the government<<\nKommuni-station\nDoes this sound better?"
    author: "ac"
  - subject: "Re: kroupware -> Klanware or Kollaborate ?"
    date: 2002-09-23
    body: "At the risk of offending the PC crowd, how about Klanware or Kollaborate?\n"
    author: "Kalibrating"
  - subject: "Re: kroupware -> Klanware or Kollaborate ?"
    date: 2002-09-24
    body: "I'm thinking that Klanware would be an even more unfortunate name (ie. Klan)."
    author: "anonomous monkey"
  - subject: "Re: kroupware  -- kooltuo and other ideas...."
    date: 2002-09-26
    body: "A name that follows the \"K\" standard, sounds suitably stupid, and with the aide of a mirror, is appropriate.   of course \n\nThe boring ones:\n\noutlooK \nlooKout \nlookforward, look4ward, \nlook\nKooperate\nKoop\n\nlittle less boring perhaps:\n\nkoopt    ( from co-opt, which means to subvert for your own purposes, which is\n     either the opposite of cooperation, or the goal of it, depending on your point of view.)\n\nKontact!\n          (I know, it is more than a contact db, but \"Contact!\" is also used to said as a\n           signal to turn the prop to start up old bi-planes. Marketing imagery could be cute.)\n\nthat was fun, I feel better now..."
    author: "Big Brown Bat"
  - subject: "Kaplan in 3.2?"
    date: 2002-09-23
    body: "Kaplan looks great! Will it be part of KDE 3.2?"
    author: "Peter"
  - subject: "Re: Kaplan in 3.2?"
    date: 2002-09-23
    body: "It's in KDE PIM module but probably not compiled by default."
    author: "ac"
  - subject: "Re: Kaplan in 3.2?"
    date: 2002-09-25
    body: "When you do compile it, and run it, you will find out that the code to make kmail a KPART is not present in CVS, and Kaplan crashes when you click on mail. The organiser and addressbook parts do work though."
    author: "George"
  - subject: "Re: Kaplan in 3.2?"
    date: 2002-09-25
    body: "Checkout the make_it_cool branch of kdenetwork (kmail and mimelib) for getting the kmail KPart."
    author: "Bausi"
  - subject: "Re: Kaplan in 3.2?"
    date: 2002-09-26
    body: "Yep, the make_it_cool branch does change things,\n\nIn file included from certificatehandlingdialog.cpp:21:\ncertificatehandlingdialog.ui.h:11: no `void CertificateHandlingDialog::init()' member function declared in class `CertificateHandlingDialog'\nmake[1]: *** [certificatehandlingdialog.lo] Error 1"
    author: "George"
  - subject: "Re: Kaplan in 3.2?"
    date: 2002-09-27
    body: "The entire kdenetwork package is not tagged, you still have to do a toplevel\nmake in kdenetwork, try\n\ncvs co kdenetwork\ncd kdenetwork\ncvs update -r make_it_cool mimelib\ncvs update -r make_it_cool kmail\ncd ..\nmake -f Makefile.cvs\n./configure\nmake install\n\nDon."
    author: "Don"
  - subject: "single apps, too? "
    date: 2002-09-24
    body: "All this integration stuff looks very raw. I hope I can configure the degree of integration. I never want to use an integrated korganizer but integration of message based kparts like knote and kmail looks fine.\n\nI hope, that I can use both variants with kooperate -ehm- kroupware: integrated and/or single apps!?\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Great news, but..."
    date: 2002-09-24
    body: "As much as I hate to say it and get everyone's panties in a wad...\n\nPlease make it able to talk to MS Exchange servers.  This would go a long way towards eliminating the need for having two machines at work, or running some funky emulator/vmwaresque application.\n\nAlong those lines, thats the reason I don't really use KWord...its nice and powerful enough but I need to read/write MS Word docs.  I realize this is a nearly impossible task since the formats are not made available...I've come to accept using Crossover for this.\n\nThe Exchange thing is doable however since the Evolution folks seem to have done it. "
    author: "poremaster"
  - subject: "Re: Great news, but... [Outlook compatibility]"
    date: 2002-09-24
    body: "Exchange might be nice, but for widespread acceptance there would need to be some compatibility with Microsoft Outlook. Perhaps something like the Bynari Insight Connector that allows Outlook to use an IMAP server instead of Exchange.\n\nThis would provide an essential migration path for those who currently cannot give up Windows, or will not give up Outlook, and would allow SMEs to run Linux servers without giving up the capabilities of Exchange for their Windows users.\n\nVery few will completely ditch Windows, especially if they have mission critical Windows applications, and even fewer would accept an email / groupware system that can only work with some of their staff. \n\nEither an Outlook look and work alike for Windows is needed or Outlook compatibility.\n"
    author: "Steve Ball"
  - subject: "Re: Great news, but... [Outlook compatibility]"
    date: 2002-09-24
    body: "Please read the documentation to the Kroupware project first, before you start to comment and give great wishes, which are already included."
    author: "PM"
  - subject: "MsExchange Plugin for KOrganizer"
    date: 2002-09-24
    body: "Jan-Pascal van Best is working on a MsExchange plugin for KOrganizer similar to the Connector for Evolution. It is already functional, look http://tux.ict.tbm.tudelft.nl/~janpascal/exchange/ and it's free.\n\nThe Kroupware project will offer another solution. Ditch, your expensive MsExchange server and put an open source solution, the Kollab server, that you can access from either the KDE-PIM applications, Outlook clients and I imagine from other PIM clients because it is totally standard compliant.\n"
    author: "Charles de Miramon"
  - subject: "Re: Great news, but..."
    date: 2002-09-24
    body: "> Please make it able to talk to MS Exchange servers. This would go a long way towards eliminating the need for having two \n> machines at work, or running some funky emulator/vmwaresque application.\n\nLinux is much more popular on servers than on desktops, so an organization is much more likely to replace MS Exchange servers long before replacing Windows desktops with Linux desktops.\n\nKroupware makes this migration path possible (throw out MS Exchange, keep Outlook, gradually switch over to Linux on the desktop).\n\nOf course MS Exchange compatibility would be nice, but I don't think it should be a top priority - Having a replacement for MS Exchange itself is much more important, IMO."
    author: "Roland"
  - subject: "Re: Great news, but..."
    date: 2002-09-26
    body: "> Linux is much more popular on servers than on desktops, so an organization is\n> much more likely to replace MS Exchange servers long before replacing Windows\n> desktops with Linux desktops.\n\nThis is 180 degrees from the truth...at least where I work.  There is no way IT is going to switch its servers to linux.  They do, however, allow me and others in engineering to use linux workstations, primarily because the development work we are doing makes it necessary.\n\nHowever, they provide little/no support towards any integration efforts.  I still need another box running windows.  I can get/send mail via the Exchange server with my linux box fine but still need windows for the scheduling, etc.  It is a pain in the butt when you spend 100% of your development time on one machine to have to use another for one purpose.\n\nSince the tools we are talking about are workstation/client tools thats the angle I'm coming from with my original post.  Having the capability to use linux or windows workstations with the current IT infrastructure is a far more tangible goal than getting IT to migrate to linux servers.  In fact, the latter is an entirely different topic altogether."
    author: "poremaster"
  - subject: "Re: Great news, but..."
    date: 2002-10-06
    body: "In regard to the suggestion about changing from MS Exchange server to linux servers, not everyone has a choice about what server they connect to. My company uses Exchange 5.5 servers with IMAP disabled, and there's nothing I can do about it, except try and run Outlook under vmware."
    author: "Kevin O'Riordan"
  - subject: "Call it Foresight"
    date: 2002-09-24
    body: "I would just like to add a top-level \"me too\" to the thread above concerning the name of the app. I think that the name \"Foresight\" is just about the first good name I have ever heard mentioned in relation to a KDE-app, and the team really should consider using that name.\n\nPlease, please also consider dropping the K naming convention. It results in ugly, amateurish and clumsy names. As Jabberwokky said above it would be better to call simple functions by their general description. Now that the linux/kde is approaching the consumer market you will see that the k-names will be replaced with descriptions or simply replaced with other names in relevant distributions.\n\n"
    author: "will"
  - subject: "Re: Call it Foresight"
    date: 2002-09-25
    body: "No, the Sextant was cool. Imagine the nice startup logo,\nfoggy background image and all other stuff derived from it.\n\n\nYep, my vote goes for Sextant. Thanks for the one who ivented it!"
    author: "Anonymous"
  - subject: "Re: Call it Foresight"
    date: 2002-09-25
    body: "In \"sextant\" there is \"sex\". Do you think it's a good idea to have such a name ? I prefer Foresight which, more over, sounds a little like a german word :) Or why not \"Vorseit\" to make it more german ? No, I'm joking, forget it."
    author: "Julien Olivier"
  - subject: "Re: Call it Foresight -- ZeitGeist... yeah"
    date: 2002-09-26
    body: "\nZeitGeist -- literally: Time ghost\nIt is an English word!  Oxford says: \"Spirit of the times\"\n\nperfect...\n"
    author: "Big Brown Bat"
  - subject: "Re: Call it Foresight -- ZeitGeist... yeah"
    date: 2002-09-26
    body: "\"ZeitGeist -- literally: Time ghost\nIt is an English word! Oxford says: \"Spirit of the times\"\n\nNice name, unfortunately it's already used by Google:\n\nhttp://www.google.com/press/zeitgeist.html\n\nPeople in the know would automatically assume that KDE ripped the name from Google. What we need is an original name, and \"Foresight\" is that (altrough you could say it's a joke on \"Outlook\", but no more that \"Konqueror\" is in regards to \"Navigator\" and \"Explorer\")"
    author: "Janne"
  - subject: "Re: Call it Foresight -- ZeitGeist... yeah"
    date: 2002-09-26
    body: "And why not just 'PIM' or 'KPIM' ? I know it's already the name of a KDE package but, in fact, that app would be a front end to this package's apps."
    author: "Julien Olivier"
  - subject: "Re: Call it Foresight"
    date: 2002-09-30
    body: "> In \"sextant\" there is \"sex\". \nFor you...\n\n> Do you think it's a good idea to have such a name ?\nYes.\n\n> I prefer Foresight \nIn french, it says nothing, only one more english word. Sextant is better, it is - at least - a french word (the same sense that in english). Without any sexual connotation...\n"
    author: "Alain"
  - subject: "Re: Call it Foresight"
    date: 2002-09-30
    body: ">> In \"sextant\" there is \"sex\". \n> For you...\n\nWrong, not just for me, look:\n\nFirst letter: \"s\"\nSecond letter: \"e\"\nThird letter: \"x\"\n\nIt make s...e...x -> \"sex\"\n\nYou see ? It's not just for me, it's for everybody who can read.\n\nAnd, BTW, even if that doesn't seem to contain any sexual connotation for you, I still don't see the relation between email communication and a \"sextant\", may it be sexual or not."
    author: "Julien Olivier"
  - subject: "Re: Call it Foresight"
    date: 2002-09-30
    body: ":: And, BTW, even if that doesn't seem to contain any sexual connotation for you, I still don't see the relation between email communication and a \"sextant\", may it be sexual or not.\n\nSextant has no sexual connotations that I am aware of, in slang, common usage, or technical usage.  A sextant is often pictured in splash screens (didn't Netscape Navigator have one for quite awhile?), or to invoke a nautical/exploration feel.\n\nsextant\n     n 1: a unit of angular distance equal to 60 degrees\n     2: a measuring instrument for measuring the angular distance\n        between celestial objects; resembles an octant\n\nIf you are hung up on the first few letters or the sound of the word, I'd hate to see your reaction to such everyday words as dictionary, cunning, continue, shatterproof, fuchisa, pistol, pistachio, piston or booboo, or your horrified reaction to a woman who has sextuplets.  Especially if her hair is kinky.\n\nAs for the relation, I don't think it's to email per se, but rather to the concept of a PIM package letting you \"know where you are\", the same thing a sextant does.  Plus, as stated earlier, they look great on a splashscreen as they are decorative, old-fashioned, and \"warm\" in feel.  A compass would be another similar item.  \n\nIncidently, I hate to say this, but I just punched Foresight into kdict (is that another sexually suggestive application name?), and got this as a return:\n\nForesight\n\n   <graphics, tool> A software product from Nu Thena providing\n   graphical modelling tools for high level system design and\n   simulation.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Call it Foresight"
    date: 2002-09-30
    body: ">  If you are hung up on the first few letters or the sound of the word, I'd hate to see your reaction to such everyday words as dictionary, cunning, continue, shatterproof, fuchisa, pistol, pistachio, piston or booboo, or your horrified reaction to a woman who has sextuplets. Especially if her hair is kinky.\n\nIn fact, I'm not shocked by such names at all. The problem is just that I think people who don't like, or don't like it to be liked, will make bad jokes using the name of the product, ala \"Internet Exploder\". I just a name which can't be easily changed into something ridiculous or rude, etc... and if kdict was a more important project (such as a web browser or a mail client), it would probably be called kdick by those who hate it. So, yes, that would be a bad name IMO, even if the name in itself doesn't contain sexual connotation."
    author: "Julien Olivier"
  - subject: "Re: Call it Foresight"
    date: 2002-09-30
    body: "Come up with a name that can't be converted into a rude, crude or lewd variant.  I'll bet you you can't.  Explorer, as you say, becomes Exploder.  Does that mean that Explorer is a bad name?  No.  It just means that people are cunning linguists and will pervert any name to ridicule something. (ahem).\n\n15 plus years of Rocky Horror has taught me that *anything* can be perverted in jest.  For that matter, a half hour on a elementary school playground should show you that such 'creative renaming' is pretty much taught at a very early age.\n\nForesight -> Foreshaft, Foreskin, Foreplay, Soresight, Soreblight, Coresight (if it crashes), etc.  (Soreblight.  I like that one.  Right up there with Microflaccid).\n\nAny name will be perverted.  Don't worry about it.  \n\n:: even if the name in itself doesn't contain sexual connotation.\n\nIt doesn't.  Or at least no more than Foreskin does.  Oops.  I mean Foreshaft.  Darn it, I mean Foresight.  Whatever.  You get the idea.  It's *you* that sees the sexual connotation - I didn't even think of it when I saw the word - the image that lept into *my* mind was a nifty nautical splash screen with an antique sextant overlaid against an old map (I like old maps).  I never even began to connect it with sex.  So... who has the dirty mind, eh?\n\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Call it Foresight"
    date: 2002-09-30
    body: "OK you're right.\n\nMaybe I'm just a perverse :)"
    author: "Julien Olivier"
  - subject: "Re: Call it Foresight"
    date: 2002-10-07
    body: "sorry - that is much to easy. Although all names can bring wrong connotations, that does not mean that all do it equally well. In giving a name one should look out for the most obvious unwanted connotations. \"Foreskin\" is an example of a far-fetched connotation in my opinion.... \n\nToo bad \"Foresight\" is taken though   :(\n\n"
    author: "will"
  - subject: "Re: Call it Foresight"
    date: 2002-11-02
    body: "In swedish the word \"sextant\" consists of two words, \"sex\" and \"old woman\".\nI think that is a really funny name! Just wanted you all to know..."
    author: "Honken"
  - subject: "Re: Call it Foresight"
    date: 2002-10-06
    body: "Sex is positive... :-)"
    author: "Jens"
  - subject: "Call it Koolaide...."
    date: 2002-09-25
    body: "....nah seriously, don't call it anything starting with a K - enough of that already. Let's get abstract...\n\nMotion, Motionware, connect, connectus, Konnect(!), connectware, Konnectware(!), Share!, Communicate!, Kommunicate!(!), Inform, Expound, InMotion, @Share, Etecetera.\n\nJust some possibities. Sorry, I don't like Foresight, and I'm fairly sure that is the name of a software product already.\nT "
    author: "Thorsten Hitler"
  - subject: "Re: Call it Koolaide...."
    date: 2002-09-25
    body: "> Motionware\n\nCool"
    author: "KDE User"
  - subject: "Re: Call it Koolaide...."
    date: 2002-09-27
    body: "> Motionware\nHeh what a komedian!\n\nKoMedian sounds better than Noatun BTW"
    author: "ac"
  - subject: "Re: Call it Koolaide...."
    date: 2002-09-27
    body: "Another good name: KDE Personal Insight (courtesy of wbsoft)\n\nOr would this give copright conflicts with other Insight applications?\n\nOther idea, just call it PIM \nKDE Personal Information Management\n\nRinse"
    author: "rinse"
  - subject: "Where is KDE kernel cousin"
    date: 2002-09-25
    body: "I am having withdrawals without it."
    author: "Frank Rizzo"
  - subject: "Re: Where is KDE kernel cousin"
    date: 2002-09-26
    body: "http://lists.kde.org/?l=kc-kde&m=103158090724690&w=2"
    author: "Anonymous"
  - subject: "Re: Where is KDE kernel cousin"
    date: 2002-09-26
    body: "Well, that sucks donkey crap.  Back to crack then."
    author: "Frank Rizzo"
  - subject: "K naming convention"
    date: 2002-09-25
    body: "Since there is much talk about the typical KDE convention to start program names with a K, I figured I'd start a thread dedicated to just that.  To start off, here is my take on it:\n\nI think the 'K' naming scheme was at one time appropriate, as it easily showed that an application was developed for KDE.  I think it has gone on too far though, with some people stretching too far to come up with more K names.  I like some of the names, like Konqueror and Kontour, and things like KMail and KMix are fairly intuitive.  But I think that the best sign that the K naming scheme is inappropriate for the audience KDE is expanding into is the fact that many desktop oriented distributions are renaming the KDE menu items to show more generic terms like Internet Dialer and Browser.  I think that whether we like it or not the beef against the K naming scheme must be a valid one, because these distros are trying to act based on consumer preferences and customer demand.  It seems that there are enough people seeing this to be a problem to warrent some real discussion on the issue. "
    author: "anon"
  - subject: "Re: K naming convention"
    date: 2002-09-26
    body: "You are confusing descriptive names like \"Browser\", etc., and application names like \"Konqueror\". In the menus the descriptive names should be shown so that new users know which of the applications listed in the menu is the browser. This is already done by default. OTOH each application has to have a unique name. We can't simply call for example \"Konqueror\" \"browser\" because there are many browsers out there. One way to create a unique application name is to simply prepend a 'K'. So we could have called Konqueror \"KBrowser\". Some would probably have said that this isn't very creative. But we have KMail, KAddressbook, KAlarm, KOrganizer, KDevelop, KWrite, KEdit, etc. So either many developers aren't very creative or they didn't see the need for a fancy name. Whatever ...\n\nBasically we have two ways to choose a name for a new application.\na) We simply prepend a 'K' to a descriptive name. So in this particular case the application could be called KGroupware or KPIM (which isn't as appropriate as KGroupware since Kroupware isn't a _personal_ IM but a _group_ IM).\nb) We come up with a nice unique name. Of course this name doesn't have to have a 'k' in it. It's nice if a good name which starts with a 'K' is found. But IMO the 'k' must not be a requirement. Instead the name should be easily pronounceable in many different languages which means for example that ideally it shouldn't contain any consecutive consonants in one syllable because at least Spanish and Japanese native speakers have a hard time pronouncing consecutive consonants.\n\nJust my 2\u00a2\nIngo\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "The adressbook needs to get better"
    date: 2002-09-26
    body: "What I find missing with Kroupware is a writeable adressbook in LDAP. This should be implemented so it is possible to have ones own adressbook accross mail clients and also in webmail (f.x. horde has a very nice LDAP-adressbook app).\n\n"
    author: "tarjei"
  - subject: "SImular project"
    date: 2002-09-27
    body: "I have found a simular project for KDE 2:\nhttp://www.shadowcom.net/Software/infusion/index.html\n\n"
    author: "tofu"
  - subject: "Re: SImular project"
    date: 2002-09-27
    body: "neat"
    author: "fault"
  - subject: "Who really needs it "
    date: 2002-09-27
    body: "But how will it be with the speed... KDE is wonderfull, in no time it is up, and in now time I can read my e-mail, but have worked a little in KDE with Evolution (So I do not know how it is Gnome where it is made for) but I had the greatest difficulty to read some graphical mail... And no, my processor is not slow, my ram is not low and my Modem is not analog... So...\nBy the way, how many people does really need an implemented client?\" So still let us have the chance to use parts independent of each other...\n\nGreetz\n\nWinux \n- The virus who eats both ways -"
    author: "Winux"
  - subject: "Re: Who really needs it "
    date: 2002-09-29
    body: "I need it. A open groupware solution will give Linux more chances in the \"closed source world\".\n\nIf I ask my boss to switch the clients to Linux I get one answer: Outlook, Exchange (and other programs ;)).\n\nSo if I am able to use Exchange or a Server that can do things like Exchange without spending a lot of money in new software: I can switch to Linux or I have better chances for that.\n\n\nThat is more than only useful, it is great !\n\nhave fun\nFelix"
    author: "hal"
  - subject: "Who really needs it "
    date: 2002-09-27
    body: "But how will it be with the speed... KDE is wonderfull, in no time it is up, and in now time I can read my e-mail, but have worked a little in KDE with Evolution (So I do not know how it is Gnome where it is made for) but I had the greatest difficulty to read some graphical mail... And no, my processor is not slow, my ram is not low and my Modem is not analog... So...\nBy the way, how many people does really need an implemented client?\" So still let us have the chance to use parts independent of each other...\n\nGreetz\n\nWinux \n- The virus who eats both ways -"
    author: "Winux"
  - subject: "Is this really necessary?"
    date: 2002-09-27
    body: "I find the project very interesting, and clearly there is a lot \nof people esp. in the Windows world that use Outlook as both an organizer and \na mail/news client. But I can't help wondering if it is a step towards the \nright direction. The Unix philosophy is \"small and simple tools for each job that can be easily combined to take care of a more sophisticated job\". \n\nSo far KDE has worked under the terms of this philosophy.\nWe have a mail reader for reading mail, a news reader for reading news, \nan Organizer for organizing, a Ghostscript viewer for GS viewing etc. \n(granted, Konqueror can view just about anything but this doesn't happen \nby giving it zillions of features, but by giving it the ability to \ncooperate easily and transparently with other, external applications or \nmodules (e.g. IOSlaves)).\n\nEven if this can be sacrificed because of the merits of integration, I just \ndon't see what mail, news and schedules have in common, besides being \none's \"personal information\" (and news is not even that). Which one of \nus keeps their postal mail in their organizer?\n\nJust my $0.02 guys, I really don't mean to upset anyone. "
    author: "Dimitris Kamenopoulos"
  - subject: "Re: Is this really necessary?"
    date: 2002-09-28
    body: "This is the *nix philosophy - each component is a KPart which can theoretically be run by itself.  Now I'm personally of the opinion that KOffice Workspace and this are duplicates, and a much better solution would be to create an 'Outlook/KO Workspace' style application that can be configured to have icons on the right for various applications, all of which run inside the body of the program.  Then you just have a couple of Profiles (a la View Profiles in Konqueror), default ones like \"PIM Center\" (with KMail, KOrganizer, KNotes, KAddressbook), \"KOffice Workspace\" (with KWord, KSpread, KPresenter, etc), and \"Communications Center\" (with KMail, KAddressbook, KNode, Kopete, KSIRC and KHTML).\n\nIn other words, a framework for KParts, much like Konqueror but with an alternate UI oriented towards an 'Outlook/Workspace' interface.  Make it generic, ship with a couple common profiles, and let people add and edit their own.  (As a disclaimer I yet to try Kaplan out, and that's what I'm hoping it already is - it frustrated me that KOffice Workspace can't be arbatrarily edited and various profiles stored).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Is this really necessary?"
    date: 2002-09-29
    body: "> This is the *nix philosophy - each component is a KPart which can\n> theoretically be run by itself.\n\nFor the kaplan approach I completely agree, except there's nothing theoretical about being able to run the parts by themselves for KMail, KOrganizer and KAddressbook it's reality.\n\n> Now I'm personally of the opinion that KOffice Workspace and this are\n> duplicates, and a much better solution would be to create an 'Outlook/KO\n> Workspace' style application\n\nI can vaguely remember myself posting to kde-? stating something similar that really I only need one application, that application being a container for all the other applications I commonly use. I've seen others independently come up with the same idea.\n\nKaplan isn't a universal container but if you look at the Kaplan code which is not much code at all then you can see how easy it is to add new parts which is what I did for KMail.\n\nI guess the only modification you would need to Kaplan to make it a universal container is to find a way to automatically find the large size icon for an arbitrary part and make a part browser to select and add new parts.\n\nDon."
    author: "Don"
  - subject: "Re: Is this really necessary?"
    date: 2002-09-30
    body: "> I can vaguely remember myself posting to kde-? stating something similar that really I only need one application, that application being a container for all the other applications I commonly use. I've seen others independently come up with the same idea.\n\nDon,\n\nI disagree with such an approach. If you want just one \"application\", you already have it with the windowmanager.\n\nThe other side is that PIM and office are different in the way I work with it. PIMs are rather workflow or informationflow applications and office are document centric.\nWhere do you need a MDI in PIM? Never, but for office applications it makes (sometimes) sence, if you want to compare 2 documents, want to copy/paste or whatever comes to your mind. \nThere is one area where both have a common need: Revision control if there is a need for workflow of documents. But this doesn't justify to unify everything in one application (which itself is again the windowmanager).\n\nWhat I hate most is when documents are embedded in emails (or invitings), which some collegues often use in our company. What to do with such an email? Shall I save it to disc to keep it's documentation status?\n\nIf you want to make a frame, then make it at least a frame which fits for the purpose.\n\nPhilipp"
    author: "Philppp"
  - subject: "Re: Is this really necessary?"
    date: 2002-10-01
    body: "> I disagree with such an approach. If you want just one \"application\", \n> you already have it with the windowmanager.\n\nI'm not suggesting that KDE applications will stop working as standalone programs. By the way things are playing out it looks like KDE users will get the best of both worlds, that is applications that can be embedded in a container app or used separately, and those applications will interoperate either way.\n\nDon.\n"
    author: "Don"
  - subject: "Re: Is this really necessary?"
    date: 2002-09-28
    body: "Hi,\nThe beauty of the KParts architecture is that embedding KMail, KOrganizer, etc.. in one framework is just one option.\nAll the current applications will continue their life as standalone applications. Kaplan is just another option for users who like the all-in-one integrated option. If you only want a mail client, you will go on using KMail alone with less bloat.\n\n\nKaplan is also an open framework. One could think of a large set of different PIM applications that you could put together in Kaplan like lego bricks. It is a very powerful concept. I don't know if Evolution works in the same way : each component is a Bonobo part.\nCheers,\nCharles"
    author: "Charles de Miramon"
  - subject: "Re: Is this really necessary?"
    date: 2003-07-29
    body: "I see people talk about why there needs to be a tightly integrated client that combines email, calendaring, address book, etc.... In a larger office environment people need to use shared calendaring.  For shared calendaring to work there needs to be a conduit to move information between the clients.  The mail transport system is the best way to implement that since we already have the conduit available.  People also want to be able to easily chose the persons to schedule for meetings.  The email system has a directory available of all the users.  It is inefficient to create multiple directories and will cause directories with missing information or increase administration costs significantly.  Running separate applications will only work if there can be an underlying connector that all applications can easily attach to.  \n\nOne of the biggest problems with adoption of Linux on the desktop is that you have to be a guru to get much of the system to work with any sort of integration and ease.  Most users want something much easier than that.\n"
    author: "TheRustyCook"
  - subject: "mmmmhhh..."
    date: 2002-09-28
    body: "Make a KDE program like GIMP !!!"
    author: "Anonymous"
  - subject: "Re: mmmmhhh..."
    date: 2002-09-28
    body: "The GIMP Project is making GIMP for KDE !!!\n\n(...and we're still waiting for it.  :)  )\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: mmmmhhh..."
    date: 2002-09-28
    body: "Is there any work on separating the GIMP-code from the GUI-toolkit code?"
    author: "KDE User"
  - subject: "Re: mmmmhhh..."
    date: 2002-09-29
    body: "I've heard many times that v2 of the Gimp will be toolkit independent, but also that v2 is like Mozilla 1.0 was - really dern slow in getting here.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: mmmmhhh..."
    date: 2002-10-09
    body: "Why should you want this?\n\nThere is no problem at all running the GTK-based GIMP on KDE environments. I don't see the point to start developping such a beast as it already exist. Better improve GIMP on the field of pre-press work (CMYK separation support etc.).\n\nJust my two EUR 0.02."
    author: "JEL"
  - subject: "Why always do things like MS?"
    date: 2002-09-29
    body: "I understand that making apps that look and feel like a popular operating system's apps will help potential users of KDE migrate, but we have to consider one thing: as long as imitation is our game, we will NEVER be ahead. You can't copy ahead. It just doesn't work. Now I know that there are many innovations in the KDE world, like KParts, and that is great, but I would like to see more. I certainly do not think that MS creates perfect programs, therefore I don't want to copy them, or even want to have those mutant clones on my KDE box.\n\nI feel there is a lot of innovation left in the realm of user experience. I am currently working on a new way of navigating through the data on my computer. Notice I said \"data\". Any data. Anything that is stored on my machine should be accessible without having to remember which folder it's in, and what is the proper app to open it. No more \"/home/mp/projects/mushroom/06-12-01/other/pics/0001231-01.jpg\" Yechh! Unfortunately I am in the very early stages of this project, but email me if interested.\n\n"
    author: "mp"
  - subject: "Re: Why always do things like MS?"
    date: 2002-09-29
    body: "There is an equivalent for KParts in the MS Windows world. This is no innovation. But there are innovations, e.g. that the \"Overwrite\" dialog when copying/moving images shows you more information about the files like resolution and previews."
    author: "Anonymous"
  - subject: "apps.kde.com?"
    date: 2002-10-01
    body: "What has happened to apps.kde.com?"
    author: "KDe User"
  - subject: "Check with fluent literate English speakers please"
    date: 2002-10-02
    body: "and speakers of other languages as well.\n\n\ncroup\n     n 1: a disease of infants and young children; harsh coughing and\n          hoarseness and fever and difficult breathing [syn: spasmodic\n          laryngitis]\n     2: the part of a quadruped that corresponds to the human\n        buttocks [syn: hindquarters, croupe, rump]\n\nKroup sounds like croup and reminds me of near death of infants and smelly behinds. ....\n\nSheesh why not call it \"Poonomia\"?"
    author: "Dictionary_Enforcer "
  - subject: "Is it really a issue?"
    date: 2003-04-29
    body: "I have to admit that I actually do not care a bit about the name as long as it 100% compliant and does what's it supposed to do.\n\nJust my point of view."
    author: "ekstam"
---
It has been a long time dream of the 
<a href="http://pim.kde.org/">KDE PIM team</a> to be able to integrate the different PIM applications into one common interface shell that would permit the creation of an Outlook-like application and provide a fully integrated personal information management system. Development of such an application is in fact rapidly picking up steam.  In particular, the 
<a href="http://www.kroupware.org">Kroupware Project</a>, part of <A href="http://lists.kde.org/?l=kde-core-devel&m=103168388425502&w=2">a full-blown open-source groupware</a> solution for KDE and commissioned by the German government, was pre-announced and has already generated a flow of ideas and code. Karl-Heinz Zimmer showed a prototype of KOrganizer embedded in KMail:
<a href= "http://lernst.de/kroupware/KMail_showing_invitation.png">kroupware1</a>,
<a href="http://lernst.de/kroupware/KMail_showing_calendar_to_check_invitiation.png">kroupware2</a>.

But Don Sanders, <a href="http://kmail.kde.org/">KMail</a> hacker, went one step further. He managed to transform KMail into a KPart and demonstrated how the different PIM components can be embedded in the pre-existing framework by Matthias Hoelzer-Kluepfel and Daniel Molkentin known as <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdepim/kaplan/">Kaplan</a>:
<a href="http://trolls.troll.no/sanders/images/kaplan1-big.png">kaplan1</a>,
<a href="http://trolls.troll.no/sanders/images/kaplan2-big.png">kaplan2</a>,
<a href="http://trolls.troll.no/sanders/images/kaplan3-big.png">kaplan3</a>.
Great work that proves, once again, that the KPart architecture is sound and gives tangible results.  Expect the first official results of the Kroupware project on October 15th.
<!--break-->
