---
title: "Second Alpha of KDevelop 3.0 (aka Gideon) is Out"
date:    2002-11-23
authors:
  - "ctennis"
slug:    second-alpha-kdevelop-30-aka-gideon-out
comments:
  - subject: "Congratulations"
    date: 2002-11-23
    body: "Having used the alpha 1 release for the past month, I just want to say that Gideon is a big step forward for KDevelop. The project has come a long way. All of you who have worked to make this possible should be really proud."
    author: "Alexandros Karypidis"
  - subject: "God this is offtopic"
    date: 2002-11-23
    body: "but where did KDE 3.1 final go?  wasn't it due out on monday?"
    author: "standsolid"
  - subject: "Re: God this is offtopic"
    date: 2002-11-23
    body: "yeah.. it was. Last I heard 3.1 RC4 packages were made and they were waiting for the ok to call it final. Gentoo already has 3.1 final ebuilds available but unfortunately no source packages on the servers yet. My guess is that RC4 was ok'ed and everyone is in the process of tagging it in cvs, packaging, sending to packagers etc. and the announcement will come next week. "
    author: "frantik"
  - subject: "Re: God this is offtopic"
    date: 2002-11-23
    body: "Gentoo *sort of* has the ebuilds ready.\n\nI unmasked all the 3.1 entries in my profile, but it still fails on not being able to find the QT it needs even if I unmask *all* the QT entries in my profile.  So there's still a bit to be done there, but yes, you're still basically right, Gentoo's ready to go with this!  :-)\n\nI like Gentoo..."
    author: "Xanadu"
  - subject: "Re: God this is offtopic"
    date: 2002-11-23
    body: "Actually, if you go to /usr/portage/x11-libs/qt and manually do 'ebuild qt-3.1.ebuild merge' (or whatever it's called) it'll work."
    author: "Rayiner Hashem"
  - subject: "Re: God this is offtopic"
    date: 2002-11-23
    body: "I think RC4 has been OK'd, and they're just waiting for the mirrors to catch up so they don't get overloaded when they announce it."
    author: "Simon B"
  - subject: "Re: God this is offtopic"
    date: 2002-11-24
    body: "KDE 3.1 final will not be identical to RC4, read the release plan on http://developer.kde.org.\nEarliest possible release date now seems to be 2nd December 2002."
    author: "Anonymous"
  - subject: "Re: God this is offtopic"
    date: 2002-11-25
    body: "Yes, it was due some time ago. But I'm quite relieved they didn't stick to the schedule.\n\nThe KDE compile I tried (rc4, with additional patches, on Debian unstable) was *buggy*. I identified and reported four showstopper bugs within half an hour of browsing around in the default configuration (seperate user account, empty .kde directory). Two of those have been fixed by now.\n\nFor example, kfontconfig crashes on Apply when you use ISO8859-15 fonts. The Kicker panel cannot span over multiple screens in Xinerama setups. And a couple others.\nThose are probably the bugs that perhaps a KDE developer can never find because he/she doesn't have multiple monitors. But as soon as the release is out, the number of users increases a thousandfold, and there WILL be people who will be really crossed by this kind of bug.\n\nI'm just now beginning to learn C++ (and KDE development, where KDevelop was a *HUGE* help - thank you to all kdevelop developers!) and I'll be able to do more detailed reporting and perhaps even fixing bugs soon.\n\n\nI urge everybody to try the RC5 release (it's in CVS and will probably appear in FTP sites soon), so that bugs that weren't found by the developers can be fixed BEFORE the release is out. That's the whole idea of RC releases.\n\n\n-- Jens\n"
    author: "Jens"
  - subject: "Re: God this is offtopic"
    date: 2002-11-25
    body: "There is no RC5 and likely will never be."
    author: "Anonymous"
  - subject: "Problem with screenshot?"
    date: 2002-11-23
    body: "Am I the only one who has a problem seeing the PNG-file in Konqueror? it complains about image/PNG, but all other PNGs works great in my system..."
    author: "midnight_runner"
  - subject: "Re: Problem with screenshot?"
    date: 2002-11-23
    body: "At least I am without any sort of png-troubles.\nUsing Konqueror/KDE3.0.0"
    author: "Syllten"
  - subject: "Re: Problem with screenshot?"
    date: 2002-11-23
    body: "is Qt compiled with PNG support ?\n\n"
    author: "somekool"
  - subject: "Kdevelop2 project"
    date: 2002-11-23
    body: "I hope it is on their TODO to implement a filter to import Kdevelop2 project."
    author: "renaud"
  - subject: "Re: Kdevelop2 project"
    date: 2002-11-24
    body: "A script called \"kdevprj2kdevelop\" already was available with the alpha1.\nAnother method would be to simply import an existing directory.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Fonts & Type"
    date: 2002-11-23
    body: "Who actually codes with the text looking like that?  That looks like garbage.  How long will it take before people realize the importance of hinting?  I hate to rant but seeing that screenshot and reading the latest comments on FreeType.org (they downplay hinting), it pains me to think that people using Xfree are going to strain their eyes with the excessive use of AA.  The fact of the matter is that screen resolution has hit it's peek and is not going to change from 72-100 dpi for a /very/ long time so the importance of having bitmapped raster fonts for low resolution screen reading cannot be understated.\n\nFreeType reasons that a lot of available fonts aren't properly hinted (tuned) for screen display, which is true, but the majority of fonts available, especially free fonts, are for graphics and print use.  Not screen display.  It just has to be accepted that screen fonts are just hard to make and thus there aren't too many out there.  But that doesn't mean hinting /shouldn't/ be supported.  People normally only use about three fonts on their computers (Arial, Times New Roman, Courier New) and two if they're using default KDE setup (Helvetica, Courier).  Licensing issues are a bummer but that's business.  Besides the distros, if already paying for AcroRead, RealPlayer, StarOffice, etc. should be able to pony up the cash to legally turn the bytecode interpreter on.  I just leave AA off at the moment.\n\nThat's enought about that screenshot.\n\nAnyway... I look forward to using KVim in Gideon.\n\nDoes anyone know what the support for Python is like?"
    author: "the window typing on the keyboard"
  - subject: "Re: Fonts & Type"
    date: 2002-11-23
    body: "I so agree with this, fonts is one of the things that keeps me coding on windows.\nThe other being the help system."
    author: "fredrik c"
  - subject: "Re: Fonts & Type"
    date: 2002-11-23
    body: "Choosing fonts to code in kde is a pain without the AA mess.\n\nI'm tunning kde-rc3/mdk now and the new AA settings are just depressively blurred. I can tolerate jagged edges on characteres. I cannot tolerate bluriness on screen fonts.\n\nI changed everything to helvetica/courier and sony/fixed. There must be something wrong with the fonts since it is percievable a quality decrease from xft1->xft2 with the byte hinter enabled.  \n\nOs course, it suites me right for running beta/rc software... I should have known better ;)"
    author: "zelegans"
  - subject: "Re: Fonts & Type"
    date: 2002-11-23
    body: "That's strange...\n\nI use Mandrake 9.0 on an LCD screen with Lucida Sans, Palatino Linotype and Lucida Console mainly. They look great ! Really.\n\nI installed RedHat 8.0 recently on another machine and Nimbus fonts (Roman, Sans and fixed) look absolutely perfect.\n\nOn both PCs I use AA and it is very very nice."
    author: "Julien Olivier"
  - subject: "Re: Fonts & Type"
    date: 2002-11-23
    body: "Hm. I use a 133 DPI display (one of those high-res LCDs) and everything looks just peachy lightly hinted like that! I can stare at text for hours a day and it's like reading a piece of paper. Hinted, the fonts are illegible because they're so damn narrow."
    author: "Rayiner Hashem"
  - subject: "Re: Fonts & Type"
    date: 2002-11-23
    body: "The people at Freetype are most definitely NOT downplaying the importance of hinting.  They are quite concerned with hinting.  Because of the $*#@% apple patents on truetype bytecode, they can't enable the truetype bytecode interpreter.  Distros could and should, but for some reason many haven't.  In the absence of that, they have been working diligently on improving the auto-hinter.  With the new Freetype version, they have toned down the autohinter to generate glyphs that are more correct (have the same proportions as the font designer intended) instead of distorting them.  This produces more consistent-looking text.  Also, they have fixed the bugs that caused the slanted parts of letters like 's' to disappear like in the article screenshot.  They have also added a way for applications to specify what type of device they want the output optimized for (default, LCD, monochrome, etc).  Unfortunately this change is not backwards-compatible so fonts may not be optimized for LCDs until applications are changed (QT 3.1 is changed).  \n\nIf you look at the Freetype mailing list, you will see that several people are hard at work tweaking the autohinter in various ways.  The rendering quality of Freetype is only going to improve.  If you want the bytecode interpreter enabled, email your distro, or compile freetype yourself (it isn't that hard)."
    author: "not me"
  - subject: "Re: Fonts & Type"
    date: 2004-04-28
    body: "Just improve fonts i KDE. They are blurred! And it's so important. I have pain in my eyes after working with KDE. It's a pity such a nice work as kde is unfauvored by the fonts."
    author: "Jordi Ferrando"
  - subject: "Re: Fonts & Type"
    date: 2004-04-28
    body: "If you think GNOME looks better you may want to change freetype's font rendering setting as described by Rayiner Hashem at http://tinyurl.com/2eynt [osnews.com]"
    author: "Datschge"
  - subject: "Re: Fonts & Type"
    date: 2006-01-06
    body: "I have enabled the Bytecode Interpreter and compiled, and even installed the rpm available for my distro, and it looks sharper, but the fonts don't look any better. Bytecode interpreter rendering is that of Ubuntu, I don't really see the need to infringe."
    author: "Webterractive"
  - subject: "Dead CVS servers!"
    date: 2002-11-23
    body: "Off Topic!!!!\n\nDoes it have a reason why the majority of KDE Anon CVS servers are down ? Some of them are down for nearly 2 Weeks now and the last one that I used died 3 days ago."
    author: "Paradigma"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-23
    body: "From KDE-Devel-ML:\n\nRe: when is anoncvs.kde.org to work properly again?\nVon: Pavel Troller <patrol@sinus.cz>\nAn: kde-devel@kde.org\nDatum: Heute 18:18:36\n> Lo,\n> \n> See subject line. Is anoncvs.kde.org to work properly again any time\n> soon, or should I switch to a mirror?\n> \nHi!\nI'm afraid that mirrors are not updated as well because rsync.kde.org\nis now the same machine as anoncvs.kde.org and it is dead (not fully down,\nit pings, but services are dead - Dirk wrote me personally, that there\nis a problem with the network services and that the machine must be\nphysically rebooted, which probably still didn't happen :-( :-(.\nIt's sad that just in the time of last fixes for 3.1, non-developing\ntesters (i.e. which don't have a real CVS account at cvs.kde.org) are\ncut off and can't test.\n                                                With regards, Pavel Troller\n\n--\nWith greetings to Pavel :-)\n\nBye\n\n  Thorsten\n\n"
    author: "Thorsten Schnebeck"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-23
    body: "Ahh this clarifies something. Thank you for information."
    author: "Paradigma"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-23
    body: "anoncvs.kde.org works fine now (started working an hour ago). Just updated everything - nice."
    author: "jmk"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-24
    body: "Does this also include the cvsup server? AFAIK it has not been updated for a long time (>2 weeks) and I would love to report some bugs. Unfortunately I don't want to do that without a recent cvs(up).\nThanks in advance."
    author: "Michael Jahn"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-26
    body: "http://lists.kde.org/?l=kde-devel&m=103832751131395&w=2"
    author: "Anonymous"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-27
    body: "Thank You."
    author: "Michael Jahn"
  - subject: "cvsup in sync again"
    date: 2002-11-27
    body: "http://lists.kde.org/?l=kde-devel&m=103840012600616&w=2"
    author: "Anonymous"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-25
    body: "how does one specify additional C++FLAGS, Ive messed with export variables CXXFLAG etc, but nothing's happening\n"
    author: "ccrw"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-25
    body: "Use CFLAGS. It is picked up by configuration script for the C++ compiler too."
    author: "Brent"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-25
    body: "I did export CFLAGS=\"-O3\"  as a test, and when compiling QT 3.1.0...it still used  -O2. any reason why it ignores it?\n\n"
    author: "ccrw"
  - subject: "Re: Dead CVS servers!"
    date: 2002-11-25
    body: "grep QMAKE_CFLAGS_RELEASE $QTDIR/mkspecs/linux-g++/qmake.conf\n   QMAKE_CFLAGS_RELEASE    = -O2\n   QMAKE_CXXFLAGS_RELEASE  = $$QMAKE_CFLAGS_RELEASE"
    author: "Anonymous"
  - subject: "[OFFTOPIC] Wine script..."
    date: 2002-11-24
    body: "I want to be able to run Wine programs from konqueror but with a little yes/no alert showing up before it's run.\n\nCan anyone show me how to do this? "
    author: "isNaN"
  - subject: "Re: [OFFTOPIC] Wine script..."
    date: 2002-11-24
    body: "I'd write a script, sortof like this\nThen make it executable, and put it in your path,\nand make konq execute *.exe's using this script...\n\n#!/bin/sh\nEXECUTABLE=\"$1\"\nkdialog --yesno \"Are you sure you want to run $1 with wine?\"\nif [ $? = 0 ]; then wine \"$1\"; fi"
    author: "dwt"
  - subject: "Re: [OFFTOPIC] Wine script..."
    date: 2002-11-24
    body: "COOL!\n\nThat worked great. Thanks!"
    author: "isNaN"
  - subject: "Re: [OFFTOPIC] Wine script..."
    date: 2002-11-25
    body: "Is that kdialog util a KDE 3.1 specific thing? I can't find it in my KDE 3.0.5 install (SuSE 8.1).\n\nArend jr."
    author: "Arend van Beelen jr."
  - subject: "Re: [OFFTOPIC] Wine script..."
    date: 2002-11-25
    body: "it probably is, since I'm running kde-3.1_rc3.\nYou might use gdialog instead (it is normally included in gnome-1.4/gnome-2.x)\nYou could also check out the kde-base CVS version and try to get kdialog to work.\n(you will probably have to fix up some Makefiles to get it to work with kde-3.0.5,\nbut I'm pretty sure the actual code does not depend on kde-3.1 explicitely)\n"
    author: "dwt"
  - subject: "Re: [OFFTOPIC] Wine script..."
    date: 2002-11-25
    body: "It sounds like another port of the famous line of handy apps starting with dialog and Xdialog. They display menus, text files, progress bars, and other such handy things."
    author: "Carbon"
  - subject: "Re: [OFFTOPIC] Wine script..."
    date: 2002-11-25
    body: "No it's not KDE3.1-specific. You might have to download it as seperapte application (RPM). At least for SuSE there is a package kdialog.rpm, which is not in the default-kde. I am using this with 3.0 i think it was already present in 2.x.\nSebastian\n"
    author: "Sebastian Eichner"
  - subject: "QT-Copy doesnt compile :("
    date: 2002-11-25
    body: "make[4]: Entering directory `/home/moo/cvs/kde31/qt-copy/tools/designer/designer'\n/home/moo/cvs/kde31/qt-copy/bin/uic -L /home/moo/cvs/kde31/qt-copy/plugins listboxeditor.ui -o listboxeditor.h\n/home/moo/cvs/kde31/qt-copy/bin/uic: relocation error: /home/moo/cvs/kde31/qt-copy/bin/uic: undefined symbol: _ZTI11QTextStream\nmake[4]: *** [listboxeditor.h] Error 127\nmake[4]: Leaving directory `/home/moo/cvs/kde31/qt-copy/tools/designer/designer'\nmake[3]: *** [sub-designer] Error 2\nmake[3]: Leaving directory `/home/moo/cvs/kde31/qt-copy/tools/designer'\nmake[2]: *** [sub-designer] Error 2\nmake[2]: Leaving directory `/home/moo/cvs/kde31/qt-copy/tools'\nmake[1]: *** [sub-tools] Error 2\nmake[1]: Leaving directory `/home/moo/cvs/kde31/qt-copy'\nmake: *** [init] Error 2"
    author: "moocow"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2003-01-15
    body: "     Hello,\n\n     I have the same problem with QT 3.1. under solaris, using the g++ 3.2.1. Did you found a solution for this problem?\n\n     10x a lot,\n     Ionutz"
    author: "Ionutz"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2003-05-14
    body: "Google search took me to here.\n\n\n\ngmake[4]: Leaving directory `/opt/local/qt/tools/designer/uilib'\ncd designer && gmake -f Makefile\ngmake[4]: Entering directory `/opt/local/qt/tools/designer/designer'\n/opt/local/qt/bin/uic -L /opt/local/qt/plugins listboxeditor.ui -o listboxeditor.h\ngmake[4]: *** [listboxeditor.h] Segmentation Fault (core dumped)\ngmake[4]: Leaving directory `/opt/local/qt/tools/designer/designer'\ngmake[3]: *** [sub-designer] Error 2\ngmake[3]: Leaving directory `/opt/local/qt/tools/designer'\ngmake[2]: *** [sub-designer] Error 2\ngmake[2]: Leaving directory `/opt/local/qt/tools'\ngmake[1]: *** [sub-tools] Error 2\ngmake[1]: Leaving directory `/opt/local/qt'\ngmake: *** [init] Error 2\n"
    author: "Coredump anniversary"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2003-08-13
    body: "Make sure all the invoked executables are coming from\nyour $(QTDIR)/bin and all linked libraries are from\n$(QTDIR)/lib, where $(QTDIR) is the directory that\nholds your latest-greatest QT distribution. \nYou may have leftover bin and lib\nfiles from previous QT distributions that's confusing\nthe current installation process of QT.\n(do a 'which moc' and 'which uic' to see if they\nare indeed coming from $(QTDIR)/bin) "
    author: "Chen"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2004-11-03
    body: "I tried to compile qt 3.3.3 and got the same message. Some investigaion showed that the uic utility was compiled with -rpath pointing to the new installation directory. The latter is empty because I did not have a chance to install anything yet. So, it picked some library from my LD_LIBRARY_PATH. When I changed the LD_LIBRASRY_PATH to point to the build directory, everything started working OK."
    author: "Sergey"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2005-01-17
    body: "I just solved this problem while trying to compile qt on RH 9.  The problem was that an older version of the qt dll was being used by uic.  Not the one that was just built.\n\nDo ldd on uic.  It shows what version of qt uic is using.  Make sure it is the one you just built in QTDIR/lib."
    author: "beerinstein"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2007-04-07
    body: "LoL"
    author: "LoL"
  - subject: "Re: QT-Copy doesnt compile :("
    date: 2007-05-16
    body: "Hi, I have the same problem here...\nwhat should be the directory that LD_LIBRARY_PATH points to?\n\nthanks\nnick"
    author: "Nick"
  - subject: "Multiple Targets"
    date: 2002-11-25
    body: "Hi.\n\ndoes anyone know it the new Kdevelop supports more that one build target\nwith a single project definition?\n\nThanks"
    author: "Maurice"
  - subject: "Re: Multiple Targets"
    date: 2002-11-25
    body: "yes."
    author: "Caleb Tennis"
  - subject: "Re: Multiple Targets"
    date: 2002-11-25
    body: "Thanks"
    author: "Maurice"
  - subject: "Re: Multiple Targets"
    date: 2005-04-05
    body: "care to explain how ?"
    author: "help"
  - subject: "Can KDevelop eat it's KDE dogfood?"
    date: 2002-11-26
    body: "It would help clean the code, impose order, find bugs, etc ... if the entire base kde and libs \"project\" could be sucked out of CVS and fiddled with in KDevelop ... compiled, run, profiled, and debugged??\n\nSO CAN IT?!!\n\n.... I thought so.....\n\nCan the entire *BSD kernel be handled in the same way??? (I say BSD cuz it's well organized code ... there's no way KDevelop could ever be a developers \"front end\" for the linux kernel)."
    author: "BeSebuD ..."
  - subject: "Re: Can KDevelop eat ----> it's <---- KDE dogfood?"
    date: 2002-11-26
    body: "sorry, the grammar police will now shoot you ..."
    author: "anon"
  - subject: "Re: Can KDevelop eat ----> it's <---- KDE dogfood?"
    date: 2002-11-26
    body: "Bang!"
    author: "Grammar police"
  - subject: "Re: Can KDevelop eat it's KDE dogfood?"
    date: 2002-11-27
    body: "Just use Project-->GenerateProjectFile from the main menu of KDevelop and all existing projects (also BSD kernel, KDElibs) should be wrapped by a KDevelop project (of type \"Custom Project\"). An appropriate .kdevprj or .kdevelop file will be created. \nI already did that for some of the KDE projects, mainly kdelibs/kdeui.\n\nThis is already possible with KDevelop-2.1.x.\n\nCiao\nF@lk\n"
    author: "F@lk"
  - subject: "Multiple Projects"
    date: 2002-11-27
    body: "Does anyone know if KDev3.0 will support multiple projects loaded in a workspace similar to Visual Studio or KDE Studio Gold?  Not just multiple build targets in a project."
    author: "Mr. Sketch"
  - subject: "code completion??"
    date: 2002-11-27
    body: "it is working fine in 2.x.x releases, though, therefore i wonder how to enable it?\ni built an deb package from the sources using 'dpkg-buildpackage' and installed it, but despite displayed as an option in the \"project settings\" it doesn't work yet.\n\nAny suggestions, folks?\n\ngreetings,\n  kata >8^)"
    author: "katakombi"
---
The KDevelop team would like to announce the availability of <a href="http://download.kde.org/unstable/kdevelop-3.0-alpha2/">KDevelop 3.0 Alpha 2</a>, codenamed Gideon (<A href="http://www.kdevelop.org/graphics/pic_corner/gideon-3.0.png">screenshot</a>).  This release fixes many bugs since Alpha 1 was released over a month ago and adds a few minor features to the mix.  Users of KDevelop 2.x will notice substantial improvements and are encouraged to begin upgrading so that new bugs can be identified and squashed.  Both 2.x and 3.0 should work along side each other.  See the <a href="http://www.kdevelop.org/">KDevelop web site</a> or the previous <a href="http://dot.kde.org/1034445361">dot article</a> for more information.
<!--break-->
