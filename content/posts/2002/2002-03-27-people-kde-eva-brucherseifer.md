---
title: "People of KDE: Eva Brucherseifer"
date:    2002-03-27
authors:
  - "wbastian"
slug:    people-kde-eva-brucherseifer
comments:
  - subject: "And the Oscar for best new Word goes to..."
    date: 2002-03-26
    body: "Eva for \"Organizatorial\"!\n\n*claps from the KDE-Cafe*"
    author: "ac"
  - subject: "follow that link!"
    date: 2002-03-26
    body: "I died laughing \nIIRC this must be my seventh life now...."
    author: "tomte"
  - subject: "Re: follow that link!"
    date: 2002-03-26
    body: "This is about the tenth time I've seen it, and I just don't see the humor.  Ah, well - not everything is funny to everybody else (I keep telling myself that when a joke falls flat)  ;)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: follow that link!"
    date: 2002-03-26
    body: "Yes, if you don't have sound it's rather boring :D\n\nregards,\n\tMatze\n"
    author: "matze"
  - subject: "Re: follow that link!"
    date: 2002-03-27
    body: "...and what it's like if you have no sound && have no macromediaplayer? ;)\n"
    author: "Tobias"
  - subject: "Re: follow that link!"
    date: 2002-03-27
    body: ".\n\n\n\n\n\n\n\n\n\n\n\n   (or something similar)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: follow that link!"
    date: 2002-03-27
    body: "An infinite number of Konqueror windows pop up... really.\n"
    author: "ac"
  - subject: "Re: follow that link!"
    date: 2002-03-27
    body: "I have sound - in fact, the annoying vocal \"Broooom\"  sounds don't stop after I sigh and close the window.  I've noticed this several times... does KDE3 and the aRts updates fix this?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Why Mainz?"
    date: 2002-03-29
    body: "Why Mainz? I was just trapped for a whole week in Mainz, and it's quite hard to find something positive about it... \n"
    author: "Tim Jansen"
  - subject: "Re: Why Mainz?"
    date: 2002-03-30
    body: "Because of its roman roots with lots of stuff to see (Mainz is over 2000 years old), the 1000 year old cathedral, its charming old town with lots of pubs and cafes, the good wine from this region, the nice and open-minded people, the carneval and lots of other festivals, the university, lots of cultural events, \"father\" Rhine,.... although I might be a bit biased because it's my home town ;-) \n(Be sure to have a good guide next time you visit Mainz :-)\n\nHappy easter,\neva"
    author: "eva"
  - subject: "yes ofcourse..."
    date: 2002-04-05
    body: "yes ofcourse its like that, think of this, you go out on the streets, you see something moving and you look at it and see that there are humans walking on the street instead of aliens. What would you do, Where do you wanna go today, not where microsoft goes, thats for sure, cause see it like this, Sometimes, but sometimes not, it depends on the moment it happens and what does it do, i really dont know, just get it of and i'll be happy. If you get this storry come to *.*.undernet.org (IRC) and go to #linux my nick there is Shadur."
    author: "Vector Sigma"
---
In this hectic week before the KDE 3.0 release, 
<a href="mailto:tink@kde.org">Tink</a>
found time to interview
<a href="mailto:eva@kde.org">Eva Brucherseifer</a> for
this week's episode of
<a href="http://www.kde.org/people/people.html">The People Behind KDE</a>.
Eva is an electrical engineer from Germany and a driving force behind 
the <a href="http://women.kde.org/">KDE-Women</a> 
and <a href="http://edu.kde.org/">KDE-Edu</a> projects and the
<a href="http://mail.kde.org/mailman/listinfo/kde-solaris">KDE-Solaris mailing list</a>. Read more about Eva in her
<a href="http://www.kde.org/people/eva.html">interview</a>, and don't forget
to check out her favourite URL!



<!--break-->
