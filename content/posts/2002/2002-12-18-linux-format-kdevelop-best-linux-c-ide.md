---
title: "Linux Format: KDevelop Best Linux C++ IDE"
date:    2002-12-18
authors:
  - "Dre"
slug:    linux-format-kdevelop-best-linux-c-ide
comments:
  - subject: "KDevelop developed with KDevelop?"
    date: 2002-12-17
    body: "\"The project is extremely fast moving...\"\n\nDoes this mean KDevelop is developed with KDevelop? :)"
    author: "Loranga"
  - subject: "Re: KDevelop developed with KDevelop?"
    date: 2002-12-18
    body: "Kinda like the Chicken or the Egg, I guess...\n\n:-)"
    author: "Xanadu"
  - subject: "Re: KDevelop developed with KDevelop?"
    date: 2002-12-18
    body: "Indeed, it *is* developed with KDevelop. We started this way in late 98 when it was halfway usable and safe to write code (that's where the autosave feature came up after loosing some data here and there initially :-))\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: KDevelop developed with KDevelop?"
    date: 2002-12-22
    body: "Nah, that would infer that it's actually developed in MSDev (MS Visual Studio). No, I'm not trol-- err... actually, I guess I am, although I thought of it as weak, 5:53 AM humor moreso than anything else. In either case, it's true! Developing in, but not necessarily for, Windows is a significantly greater experience than in Linux for one major reason: Visual Studio. I'm tired. If you need actually question the reasoning behind my opinion, just ask."
    author: "kaoruAngel"
  - subject: "Re: KDevelop developed with KDevelop?"
    date: 2002-12-25
    body: "Well, if MS VS is the only IDE you have used it may be a reason for you to like windows more. But, MS VS is not as good as BCB IDE or CodeWarrior.\n"
    author: "Agron"
  - subject: "Well deserved credit"
    date: 2002-12-18
    body: "This is _again_ a great rating for kdevelop.\nI gotta tell ya. I never liked IDE's, but kdevelop is the best I've used so\nfar. It doesn't get in my way as some other IDE's do. It's very intuitive\nand straightforward. I have tried most of the IDE's in the review and I couldn't agree more with it.\nAnd, haha: \"It is great that an application which is open source is such an amazing contender and manages to put some of the proprietary commercial offerings to _shame_.\"\nThis (once again) proves that open source does (is able to) infact produce nicely polished applications. In your face open source enemies!\nUh, I'm getting of-topic here :)"
    author: "dwt"
  - subject: "one competitor is missing"
    date: 2002-12-18
    body: "Well, though VSlick is not counted as IDE, so far it does lots of things better than KDevelop does. But so far, Gideon seems to fix this in the nearest future."
    author: "SHiFT"
  - subject: "Congratulations from Anjuta developers"
    date: 2002-12-18
    body: "Congratulations to the KDevelop team from the Anjuta developers ! I'm currently going through the review to pick up our weaknesses. Hopefully, there can be better coordination between the IDE developers in the future. In fact, I've also downloaded KDevelop CVS to have a look at the code. Hopefully, we can find areas for code reuse in the form of common libraries.\n\n<shameless plug> Try out our latest release 1.0.1 - it has some really cool features like incremental search, a really powerful tools editor and error/warning indicators inside the editor window.</shameless plug>\n\nRgds,\nBiswa.\n"
    author: "Biswa"
  - subject: "Re: Congratulations from Anjuta developers"
    date: 2002-12-18
    body: "Thanks. What actually prevents you from KDevelop? Why not drop Anjuta but extend KDevelop? ;-)"
    author: "F@lk"
  - subject: "Re: Congratulations from Anjuta developers"
    date: 2002-12-18
    body: "Come one, don't be rude."
    author: "Shamyl Zakariya"
  - subject: "Re: Congratulations from Anjuta developers"
    date: 2002-12-18
    body: "Hey, didn't you see the \";-)\"! But indeed once about in January 2002 I tried Anjuta and that time I thought it was cooler than KDevelop. It had codecompletion before KDevelop."
    author: "F@lk"
  - subject: "Re: Congratulations from Anjuta developers"
    date: 2003-07-20
    body: "Anjuta lets me just create a simple text file and compile it quickly.  Kdevelop FORCES one to spend a lot of time entering the author and project title name, CVS options, and the like.  For someone who is learning a language, this is needless, and a total waste of time.  "
    author: "Teo"
  - subject: "Re: Congratulations from Anjuta developers"
    date: 2002-12-18
    body: "At least I had not the chance to look at Anjuta yet. But I had a look at the screenshots :-) and it looks very promising.\nI know that one of your team is working on SourceBase. There's an other constitution of an new IDE backend (for Gideon), too. Maybe we can do business with each other in this case. Roberto Raggi is the developer of it, though, what I saw and heard is very cool :-).\n"
    author: "Victor R\u00f6der"
  - subject: "Re: Congratulations from Anjuta developers"
    date: 2002-12-19
    body: "Yup - that is me. Unfortunately, sourcebase development has stagnated a bit recently since I'm concentrating more on getting some more important things right in Anjuta first, for example, a better project manager. One of the major aims of SourceBase is to be usable by all free IDEs. I'm hoping to restart SourceBase development once I implement the new project manager and some other stuff which I need at work.\n"
    author: "Biswa"
  - subject: "support for other languages?"
    date: 2002-12-18
    body: "Would that include python and java? You might yet drag me away from emacs if it did..."
    author: "caoilte"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "Yes - at least Java will be supported.\nI don't know at the moment if python will be supported too.\nBut with the new plugin-architecture it should be able to add support for any kind of programing language AFAIK."
    author: "SegFault"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "> You might yet drag me away from emacs if it did\nDon't think so. Gideon is already a great tool. But the editor can't compete with Emacs. As soon as I can use the Emacs in KDevelop I'll switch.\n"
    author: "Norbert"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "Gideon uses the kate parts as Editor. Kate is IMHO the best Editor i ever seen, including Emacs and Vim...\n\nGideon gonna be _very_ cool!"
    author: "Raffaele Sandrini"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "I bet you've never learned how to use the Emacs with all its macros, shortcuts and lisp integration... You can't be faster in writing code than with the emacs. I don't see a way to do all this in Kate... \nI have been using Emacs for nearly everything (the other things I do with vi or KWord) for years now. Nothing comes close..."
    author: "Peter"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "I bet you've never learned how to use the Emacs with all its macros, shortcuts and lisp integration... You can't be faster in writing code than with the emacs. I don't see a way to do all this in Kate... \nI have been using Emacs for nearly everything (the other things I do with vi or KWord) for years now. Nothing comes close..."
    author: "Peter"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "KDevelop uses the KTextEditor interface so at the moment, you can use kate, kvim, qEditor and nedit (experimental!) in KDevelop. Write an EMacs KTextEditor integration any you'll be able to use it in gideon as well."
    author: "HarryF"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "\"Write an EMacs KTextEditor integration any you'll be able to use it in gideon as well\". \nYes KEmacs... we really need it :(\n\nJad\n\n"
    author: "Jad"
  - subject: "Re: support for other languages?"
    date: 2002-12-19
    body: "This would be pretty cool, indeed. However, does not GNU Emacs even already support exporting its text editor as a widget? So, can someone estimate how difficult it would be to wrap it into a (K|X)part for someone who has never written a kpart before?\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "In this screenshot python is a option in 'New Project' dialog. Hope it will make it to the final version. \nhttp://www.kdevelop.org/graphics/screenshots/3.0/gideon-newproject.png\n\nBTW.\nCould Gideon be ported to other platforms or will QT license lock it to *nix only? I would love to use it at work."
    author: "Fredrik C"
  - subject: "Re: support for other languages?"
    date: 2002-12-18
    body: "I am using python with gideon (kdevelop 3) just fine for developing a python product for zope. All the features I have been looking for seem to work just fine. The class browser works, cvs intergration works, syntax highlighting works etc. I would say that it supports at least enough python for me to really like using it and I really like the new interface in kdevelop 3.\n\nI also did most of my python programming before kdevelop 3 in kdevelop 2 and that worked fine also."
    author: "kosh"
  - subject: "Re: support for other languages?"
    date: 2002-12-19
    body: "I'm working on a sample framework for Python/PyQT in Gideon. I'd be interested in any ideas/experiences from anyone currently using Gideon to develop in Python.\n"
    author: "Julian Rockey"
  - subject: "gideon"
    date: 2002-12-18
    body: "KDevelop2 is great, and KDevelop3(Gideon) promises to be even better! My only gripe about Gideon is that the generated projects doesn't work with my autoconf/automake setup. I have 2.54 and 1.6.3 and that works with all of KDE, Gideon, Quanta+ and all other apps I've tried for that matter.. only NOT with the stuff Gideon *generates*!! \n\nI hope this changes, it would be very cool to try out all the new features for real, and not just browse around in the menus..."
    author: "jd"
  - subject: "Re: gideon"
    date: 2002-12-18
    body: "That's not gideon's fault but a hassle with the build system in general. Try to replace your admin/ directory with the one from kde-common/admin (KDE CVS)."
    author: "HarryF"
  - subject: "Re: gideon"
    date: 2002-12-19
    body: "Hey! Thanks for the tip. That did it. :)\n\nThe generated code (KDE framework app) still didn't build though, it had missed to link libkdeprint, but that was fixable. However, I ended up editing the Makefile.am file, but shouldn't there be a place somewhere in Gideon to add libs to link? KDevelop2 has that.."
    author: "jd"
  - subject: "Re: gideon"
    date: 2002-12-19
    body: "I personally have had nothing but trouble with KDevelop's make files and configure scripts. At the moment I have a program that is giving me undefined symbol errors, for symbols that do exist in the libraries it's pointing to. I still haven't managed to figure it out after a month. Thanks,\n\nDavid"
    author: "David Findlay"
  - subject: "Re: gideon"
    date: 2005-02-17
    body: "Hi...\n\nI,m newby in Linux and development... And my english isn't good, so sorry...\n\nHere is: i've got a project (C++) build in Kdevelop2, and now i've install Fedora 3, with kdevelop3... i can't open that project (project name=CARD.kdevelop)... Gives me this errors:\n\"This is not a valid project file.\nXML error in line 5, column 35:\nerror occurred while parsing element\"\n\nCan you help me please? Thanks...\n"
    author: "stbcoder"
  - subject: "vim texteditor"
    date: 2002-12-18
    body: "is there a way to get kdevelop with vim on debian without compiling everthying from cvs?"
    author: "L1"
  - subject: "Re: vim texteditor"
    date: 2002-12-18
    body: "I don't know if Gideon is available as a .deb, but the Kvim plugin can be obtained by adding \"deb http://ers.linuxforum.hu/kde3deb/ ./\" to your /etc/apt/sources.list ."
    author: "CAPSLOCK2000"
  - subject: "Re: vim texteditor"
    date: 2002-12-18
    body: "replying to myself:\nGideon for Debian can be found at: http://mypage.bluewin.ch/kde3-debian/"
    author: "CAPSLOCK2000"
  - subject: "Thanks to the KDevelop team"
    date: 2002-12-18
    body: "As a beginner in programming, I was amazed by KDevelop 2 years and a half ago. KDevelop brought me to KDE and gave me the opportunity to get involved a bit in development. It is so intuitive and easy to use that I even thought C++ would also be easy... I have been using Gideon for a month and really, it rocks. Thanks to the KDevelop team for bringing such a tool for newbies."
    author: "annma"
  - subject: "Re: Thanks to the KDevelop team"
    date: 2002-12-18
    body: "Same here -- I could read and tweak C/C++ but the ease of the KDevelop & KDE/Qt combination is what encouraged me to take the plunge into writing my own applications a couple of years ago.\n\nThe developers should take pride in having provided such a useful tool.\n\n(Now, if they'd just return the \"KDE-Mini\" project option to Gideon....)"
    author: "Otter"
  - subject: "Re: Thanks to the KDevelop team"
    date: 2002-12-18
    body: "Yes! I miss KDE Mini as well -- most apps I write are based off of KDE Mini.\n\nHowever, the real reason I'm still using KDevelop 2 still is that after a couple weeks running gideon it just wasn't stable enough. of course, I posted bug reports. And of course, I understand it's alpha ;) I'm really, *really* looking forward to it, but for the time being, and with the amount of coding I'm doing, I'll stick to KDevelop 2 which is rock-solid.\n\nOh, and one thing -- my favourite part of KDevelop 2 is having a keyboard accellerator to show/hide the messages/callstack/terminal/etc view. I have a thinkpad with a relatively small screen and I need all the space for my code I can get. I poked around, but was unable to find an option to map a key combo to show/hide the bottom pane.\n\nAnd, also, I wasn't able to figure out how to tag files as being installable/where to install when make install is invoked. Under KDevelop 2 I just right click a file under the file view and set the install info, but while I'm sure gideon supports this, as it has great automake management, I couldn't figure out *how*.\n\nPerhaps I'll file a feature request. But nontheless, I'm impressed.\n"
    author: "Shamyl Zakariya"
  - subject: "Rapid Application Development & GNOME support"
    date: 2002-12-18
    body: "What about RAD support? KDevelop is a great code editor IDE, but does it support RAD like Delphi? I mean stuff like:\n- integrated form editor.\n- extensible, meaning that you can use your own widgets.\n- when you doubleclick on a widget, the editor will automatically create a callback function for one of the widget's signals (like MyForm::MyButton_OnClick), or will put the cursor on that function if it's already created.\n\nThose are the most important things that I miss in any IDE but Delphi/Kylix (I've never used MSVC++).\n\nAnd what about GNOME or GTK2 support?"
    author: "Stof"
  - subject: "Re: Rapid Application Development & GNOME support"
    date: 2002-12-18
    body: "I know your only a GNOME troll, but yes GNOME is supported since a while."
    author: "ac"
  - subject: "Re: Rapid Application Development & GNOME support"
    date: 2002-12-19
    body: "Ah, so asking wether true RAD is supported by KDevelop somehow makes me a GNOME troll? How logical.\n\nIs GTK2 also supported?"
    author: "Stof"
  - subject: "Re: Rapid Application Development & GNOME support"
    date: 2002-12-19
    body: "I mean that you should try out the KDevelop/QtDesigner integration. It does what you want to do."
    author: "L\u00facio Fl\u00e1vio"
  - subject: "Re: Rapid Application Development & GNOME support"
    date: 2002-12-21
    body: "i think qt designer is  currently the best form designer. \nvocationally I must develop with delphi jbuilder and c++ builder often.\nhowever thereby edit source code is terrible."
    author: "brat"
  - subject: "kvim plugin"
    date: 2002-12-18
    body: "can ya add kvim to the editors.  not to flame emacs.  sorry.  just love vim, that's all.  it would very cool.  imho."
    author: "rob mandel"
  - subject: "what do they mean by..."
    date: 2002-12-18
    body: "\"It is great that an application which is open source...\"\n\nI am sure that *because* it is free software, it is so good.\nway to go KDevelop team :)"
    author: "EvilSmile"
  - subject: "phoey"
    date: 2002-12-19
    body: "emacs works fine."
    author: "anonymous coward"
  - subject: "I only have one wish for KDevelop"
    date: 2002-12-19
    body: "\nThe biggest weakness of KDevelop in my oppinion is the lack of the abillity to use an external editor.\nI've used NEdit to write my code for ages, but I've been trying out KDevelop recently to see if I would find it usefull. It's an extremely good IDE, but I constantly find myself becomming slightly annoyed with the build-in editor. If I could just have KDevelop use NEdit as its editor I would be in development heaven :-)\n"
    author: "Jesper Juhl"
  - subject: "Re: I only have one wish for KDevelop"
    date: 2002-12-19
    body: "There is (experimental) support of nedit in gideon. However, I like kate now better, with it's code folding and all :)"
    author: "g to the izzo"
  - subject: "Re: I only have one wish for KDevelop"
    date: 2002-12-23
    body: "Vim has better code-folding capabilities.\nWhy not include Kvim also ?"
    author: "Asokan"
  - subject: "Re: I only have one wish for KDevelop"
    date: 2002-12-20
    body: "Gideon allows plugin editors.  Right now it uses the katepart, but it also works with kvim, qeditor, and nedit.  I personally like kate, so I haven't tried the others, but I do know they work."
    author: "Caleb Tennis"
  - subject: "Re: I only have one wish for KDevelop"
    date: 2002-12-21
    body: "Wonderful. I'll have to look into that... Thank you for the information!\n"
    author: "Jesper Juhl"
  - subject: "A couple of problems"
    date: 2003-11-06
    body: "First off le me say that KDevelop has changed my development ability totally.  I love it!  Now with that siad I am currently working with a project that is about 50 source/header files and over 10000 lines long.  Funny thing is once I got over about 7500 lines of code (that was a complete guess BTW) the automake.autoconf generators started messing up on occassion.  I was seeing all kind of wierd error messages about the linker and such.  Turns out the the automake.am file was becomig corrupted.  It actaully contained two of every .h and .cpp file.  Night mare, but once I figured it out and recognize the problem it only takes a quick makefile.am edit, rm -f Makefile and the cd../; aclocal, autoconf, automake. ./configure; make.\n\ner something like that. any how great IDE. "
    author: "gnuLNX"
  - subject: "Agreed... but.."
    date: 2004-01-31
    body: "I agree with that... KDevelope is the best in Linux...\nand I use that since I knew Linux Programming\n\nbut I hope that the new KDevelope will be similar like Visual C++ does..\nso it can give code hints etc.. (I mean.. if i write some functions.. it will automatically give a clue which parameter that i should put on :P )\n\n"
    author: "dufronte"
  - subject: "Re: Agreed... but.."
    date: 2004-01-31
    body: "It does this. \n\nhttp://www.kdevelop.org/index.html?filename=screenshots2.html\n"
    author: "teatime"
  - subject: "Re: Agreed... but.."
    date: 2004-01-31
    body: "hwoa... that's suprised me...\nhow could I activated this code hinting functionality ??\n\ncoz it doesn't work in mine..\n\n(i'm using KDevelope 2.1.5)\n\nthanks in advance..."
    author: "dufronte"
  - subject: "Re: Agreed... but.."
    date: 2004-01-31
    body: "Upgrade to KDevelop 3.0."
    author: "Henrique Pinto"
  - subject: "Re: Agreed... but.."
    date: 2004-02-01
    body: "oh.. thanks for the info..."
    author: "dufronte"
  - subject: "Congratulations from Magic C++ developers"
    date: 2004-05-28
    body: "Congratulations from Magic C++ developers \n \nCongratulations to the KDevelop team from the Magic C++ developers( http://www.magicunix.com )! \nDifferent from KDevelop which finishes all the tasks under linux, Magic C++ is a kind of visual remote unix and linux C/C++ IDE under windows.We are currently using and learning KDevelop to pick up our weaknesses. We are expecting all the IDE developers can communicate freely and share the knowledge. \n\nBest regards"
    author: "Magic C++ developers"
  - subject: "Conversion to GUI"
    date: 2005-11-03
    body: "Sir,\n\nApplication Environment :\nFront-end : ESQL-C\nO/s : Unix (AIX,SOLARIS) & LINUX\nRDBMS : INFORMIX 7.x for SE(STANDARD ENGINE)\n\nOur Requirement is to convert our CUI Application to GUI.\n\nCan your product MAGIC C++ meet our requirement?\n\nRegards,\nRamakrishnan R\n\n\n"
    author: "Ram"
  - subject: "What makes KDevelop so good?"
    date: 2007-03-16
    body: "You can call me a n00b when it comes to KDevelop, but let me tell you about my first experience with this \"fantastic\" program.\n\nTo install it I had to compile it for about an hour. Well, that fine with me. Microsoft's Visual Studio takes much longer to install. \n\nWhen I finally got through compiling and installing I wanted to create a project to compile a static library, which of course isn't possible since there are no templates for it!\n\nThe next thing I tried was to read some documentation about \"creating projects\" etc. Guess what!? There is no documentation! And if there is, it doesnt work!\n\nOk then... Google on \"documentation kdevelop\", reading \"you need the documentation plugin\", and finally checking in KDevelop if it actually was there, which It was. So... Why is there no documentation!? God knows.\n\nWell, well... What about creating a simple \"hello world\" program and just reconfigure it to compile as a static lib? I thought it was a good idea... What happens? KDevelop doesn't even know how to build its own \"hello world\" project!!!!!!!!!!!!!!!!!!!!!! That's totally amazing! I can't stop laughing!\n\nThe final thing I tried to do was to add files into the project... Can somebody please tell me how such a simple task can be accomplished? (Please don't answer, it was a retorical question). And how can you tell what files the project contains?\n\nI think this says it all. KDevelop no more.\n\n/ Pwn uras\n"
    author: "Egd"
  - subject: "Re: What makes KDevelop so good?"
    date: 2007-05-25
    body: "I am in EXACTLY the same situation as you are. If you already know answers, please , let me know.\nH.Kreitman   krei@netvision.net.il\n"
    author: "H.Kreitman"
  - subject: "Re: What makes KDevelop so good?"
    date: 2008-07-23
    body: "I agree. I have been trying to get this software to function properly. No luck after 2 hours.\n\nI will never go back to Microsoft. I will seek other solutions."
    author: "Dr. Thomas Rendleman"
  - subject: "Re: What makes KDevelop so good? - My solution "
    date: 2008-08-10
    body: "I highly recommend Ubuntu. I was using SUSE 10.2 - My computer crashed and I obtained a new and more powerful computer. I reinstalled SUSE 10.2 - It didn't work the same even though it was the same version. I switched to 10.3 - It didn't recognize my modem and network. I then tried SUSE 11. It recognized the network but not the modem. It also didn't recognize the sound card I have. It was less friendly than 10.2 and 10.3 - I found it difficult to work with and I was told to go to Ubuntu so many times, I decided to give it a shot.\n\nUbuntu solved all the issues. All hardware was recognized. I wanted to use it for a week or so before I said it was good. I have used it for many applications and would say that it much better than any OS out there.\n\nSorry SUSE, I am leaving you were I left Microsoft. Trash can "
    author: "Dr. Thomas Rendleman"
  - subject: "Re: What makes KDevelop so good? - My solution "
    date: 2009-01-04
    body: "Typicaly SuSE provides better KDevelop binaries than ubuntu.\nBut you are correct the KDevelop binary in 10.2 was broken. The one in 10.3 is better, but you had problems with 10.3\n\nI have 11.0 and I like it a lot, but I'm the KDevelop webmaster, so I know KDevelop and I'm a sysadmin so I guess I have a lot more experience than you, but nevertheless I think KDevelop and openSUSE are pretty easy to use even for newbies"
    author: "Amilcar Lucas"
  - subject: "Re: What makes KDevelop so good?"
    date: 2009-01-04
    body: "> I wanted to create a project to compile a static library, which of course isn't possible since there are no templates for it!\nWe cannot create templates for every single scenario. But there is a nice FAQ that explains about static libs and other stuff.\n\n> There is no documentation! And if there is, it doesnt work!\nHow do you mean there is no documentation ? Didn't you read the FAQ ?\n\n> KDevelop doesn't even know how to build its own \"hello world\" project!!\nMany binaries (provided by linux distros) do not have the correct dependencies, and that means that for instance, the compiler or another vital part of the program do not get installed, and then KDevelop can not run properly.\nIn you case you compiled KDevelop from sources, so you are the one to blame for not reading the \"required software list\" and installing it.\n\n> The final thing I tried to do was to add files into the project... Can somebody please tell me how such a simple task can be accomplished?\nAgain you did not read the FAQ. Why did you not read the FAQ ?\n"
    author: "Amilcar Lucas"
---
In a very thorough
<a href="http://www.linuxformat.co.uk/archives/LXF35.roundup.pdf">review</a>
(1.5MB PDF file) by
<a href="http://www.linuxformat.co.uk/">Linux Format</a>
(Issue 35, Christmas 2002, p. 36),
<a href="http://www.kdevelop.org/">KDevelop</a> 2.1
beat out six commercial and Open Source contenders to be crowned the Best
Linux C++ IDE.  In the concluding remarks in a review including competitors
from Kylix Open Edition (Delphi) and Studio Gold (KDE) to Anjuta (GNOME)
and Code Forge (Motif), Maurice Kelly notes that the current development
branch, Gideon, brings with it another major set of improvements which will
make KDevelop shine even brighter:
&quot;<em>[T]he project is extremely fast moving and a number of new features
are being developed in the upcoming 3.0 version which is currently
in alpha testing. These include support for languages other than C/C++,
<code>qmake</code> support and C++ auto-completion. It is great that an
application which is open source is such an amazing contender and
manages to put some of the proprietary commercial offerings to shame.</em>&quot;

<!--break-->
