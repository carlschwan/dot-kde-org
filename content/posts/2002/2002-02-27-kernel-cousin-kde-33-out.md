---
title: "Kernel Cousin KDE #33 Is Out"
date:    2002-02-27
authors:
  - "Someone"
slug:    kernel-cousin-kde-33-out
comments:
  - subject: "Megami"
    date: 2002-02-27
    body: "<a href=\"http://www.freekde.org/neil/megami/\">http://www.freekde.org/neil/megami/</a>\n<p>\nYou don't need to wait for KDE 3.1 to play it:  It's out for KDE 2.2.2 now, and I'll put out a KDE 3.0 tarball for it once KDE 3.0 is finalized.\n"
    author: "Neil Stevens"
  - subject: "sweet svg"
    date: 2002-02-27
    body: "Those svg icons look good.  And best of all they look good without ripping off another os.\n\nWith that talk a while back about unifying themes, I think a great looking svg theme like this would be a good thing to settle on.  Now we are not there yet, the svg libraries are new, and scalable gorilla needs more icons, but I can dream."
    author: "idspispopd"
  - subject: "Re: sweet svg"
    date: 2002-02-28
    body: "What is SVG?  KIllustrator apparently knows how to export \"Scaleable Vector Graphics,\" and if that is the same thing, that means the SVG icons are, um, scaleable vectors.  (And the big mystery is how to reimport the SVG, and if the export works at all.)\n\nVector: the thing KIllustrator does so that a line is a line, not a diagonal row of dots.\n\nScaleable: keeping the vectors as vectors so that the line doesn't become a diagonal row of large dots."
    author: "I was using KIllustrator when..."
  - subject: "Re: sweet svg"
    date: 2002-02-28
    body: "Sort of... Scalable Vector Graphics described in XML is a more acurrate description of it. However, you can do a lot more than just making pretty icons with it. Think flash (yuck, propiery stuff ;-).\n\nThe thing about it is that it is a free and open standard. See www.w3c.org if your interested. Adobe is supposed to have a \"refence\" implentation of it and I also believe that Mozilla have some support for it (I also believe that a native plugin/kpart/whatever is being worked on for konq).\n\nPlease note though that I am not by any means an expert on this area, so please correct me if I'm wrong...\n\n/J\u00f6rgen"
    author: "J\u00f6rgen Lundberg"
  - subject: "Re: sweet svg"
    date: 2002-02-28
    body: "From what I can tell SVG is basicly an easier to parse postscript...  It has some nice features of XML though, too so I wont be too critical.  It will be great to have scaleable icons again, like on my \"slab\", haveing gone about 5 years without them you miss the little things ;)\n\nWell see how it takes off, so far it looks promiseing.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: sweet svg"
    date: 2002-02-28
    body: "It is (as has been stated) a W3C recommendation and an alternative to Macromedia Flash.\n\nRegarding performance/speed - well, choice is always nice and they certainly do look cool :)\n\n"
    author: "Joergen Ramskov"
  - subject: "Re: sweet svg"
    date: 2004-02-15
    body: "Wow another Ramskov? You must live in Denmark! And be a distant relative!"
    author: "Ian Ramskov"
  - subject: "Re: sweet svg"
    date: 2005-05-12
    body: "Wee...accidently stumpled upon your post :)\n\nIt's actually my middle name, but who knows - we might be related in some way :)"
    author: "Joergen Ramskov"
  - subject: "Re: sweet svg"
    date: 2002-03-01
    body: "I probally talking something stupid, but sometime ago gnome and kde people who have neet each other on a meeting (I think it was a gnome meeting) agreed they needed some new font system on X, because true type isn't 100% free fonts, etc.\nSo, why not using svg in this case? true type are verctorial too, so I belive it whould be possible to use svg as to build fonts.\n(I hope someday we'll just have to drop some files in a folder to get new fonts without having to restart x/xfs).\n"
    author: "protoman"
  - subject: "Re: sweet svg"
    date: 2002-03-01
    body: "That would actually be a cool idea, but I think that would make for a very slow system.  Since (niko correct me if i am wrong) but each SVG object has its own DOM.  \n\nThe nice thing about TT fonts is they are everywhere, I mean, $15 and I have some very sexy ones for my desktop, or free if I use the ones that came with my MacOS.\n\nBut yes, it is an issue for those users who do not have MacOS or Windows.  I could have sworn thought that both Mandrake and SuSE had special TT fonts in their pro versions...  I never looked into it much but that is another route to go too.\n\njust my 2c \n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: sweet svg"
    date: 2002-03-02
    body: "Yep, the best and most used ttf fonts are free, but they can't be freely distributed, you have to download from the original maker (mostly microsoft).\nThis makes time for distros, so a 100% free solution would be better.\nAnyway, maybe using SVG as it is isn't the solution, but using only their \"soul\" a XML system that describes vectors."
    author: "protoman"
  - subject: "Re: sweet svg"
    date: 2002-02-28
    body: "Actually, they are ripp off of one of MacOsX icon themes. Even famous Ximian logo with the monkey is a ripp off, and again MacOsX :)\nBut very good ripp off ;-)\n\n"
    author: "antialias"
  - subject: "Re: sweet svg"
    date: 2002-03-01
    body: "I wonder, how GNOME ripped off Mac OS X since I find no icons that look like the SVG default theme *and* that most, if not all, Mac Os X icons are photo-realistic, while GNOME's SVG theme isn't. If it's a rip off, every thing is an rip off."
    author: "Rajan Rishyakaran"
  - subject: "Re: sweet svg"
    date: 2002-03-01
    body: ">I wonder, how GNOME ripped off Mac OS X since I find no icons that look like the SVG default theme...<\n\nYou din't search hard enough.\n\n>Mac Os X icons are photo-realistic, while GNOME's SVG theme isn't.<\n\nI never said that it was default MacOsX theme that had been ripped off.\n\n>If it's a rip off, every thing is an rip off.<\n\nGreat logical conclusion."
    author: "antialias"
  - subject: "Re: sweet svg"
    date: 2002-03-01
    body: "I do not doubt you are right, I am just interested in what exactly this mac theme looks like.  I have tried google and I can't find anything.  Could you post a link to some screenshots."
    author: "theorz"
  - subject: "Re: sweet svg MAC OSX"
    date: 2004-01-21
    body: "sweet svg,\nDude, MAC OSX is a \"rip off\" of FREE-BSD and various X Windows offerings. You do understand that the MAC OSX is just another unix-like OS running a window manager right? No, you probably don't since you are a simple minded MAC user.\n\nPut a sock in it."
    author: "Chris"
  - subject: "Re: sweet svg MAC OSX"
    date: 2004-03-17
    body: "Dude,\n\nRealising and actually understanding what the OS is accomplishing seems to have escaped your simple little Windows mind.\n\n"
    author: "Roger"
  - subject: "Re: sweet svg MAC OSX"
    date: 2004-03-28
    body: "The difference is that OSX has a focussed and managed project for the window manager, unlike linux, and BSD variants.  Any \"rip off\" is just all the good parts.  Everyone who uses Max OSX smiles and just knows what a smelly piece of shit windows is.\n\nEat your own winsocks"
    author: "mick"
  - subject: "Re: sweet svg MAC OSX"
    date: 2004-04-21
    body: "Chris and Mick are right. See it like this anything Unix is good and Windows is shit! CHris you know your stuff! ;-)\n"
    author: "Miss Trin"
  - subject: "improved malloc"
    date: 2002-02-27
    body: "The improved malloc means more speed! Everybody compile KDE with: \n\n--enable-fast-malloc=full\n\n!!!"
    author: "KDE User"
  - subject: "Re: improved malloc"
    date: 2002-02-28
    body: "I'll take a CVS snapshot of HEAD and update my KDE3 packages (including making it use the faster malloc), as soon as I get the 2.2 packages finalized ... I also intend to package some parts of kdenonbeta, including Kolf and Atlantik; maybe even Kue if it's ready. We'll have to see.\n\nOh yeah, and I have working qt3/kdelbis4/kdebase3 packages. Stay tuned for a URL."
    author: "DanielS"
  - subject: "Re: improved malloc"
    date: 2002-02-28
    body: "yay debian!\n"
    author: "KDE User"
  - subject: "Re: improved malloc"
    date: 2002-02-28
    body: "KAudioCreator is also pretty much 1.0 and other then converting to the kcddb library once it is finalilzed that app isn't going to change feature wise until after 3.1.\n<p>\n<a href=\"http://www.csh.rit.edu/~benjamin/desktop/programs/kaudiocreator/screenshots.html\">kaudiocreator/screenshots.html</a>\n<p> \n-Benjamin Meyer"
    author: "Benjamin Meyer"
  - subject: "Re: improved malloc"
    date: 2002-02-28
    body: "NOTE:\n\tThis is only for the IA-32 crowd."
    author: "Ian Reinhart Geiser"
  - subject: "Nikoz you seem to have an old version of xawdecode"
    date: 2002-02-28
    body: "see http://xawdecode.fr.st/ if you want to watch tv with desinterlacing ;-)"
    author: "huh"
  - subject: "desinterlacing?"
    date: 2002-02-28
    body: "What, are there hidden messages in television, encrypted with DES? :-)"
    author: "Neil Stevens"
  - subject: "Alpha blending in toolbar"
    date: 2002-02-28
    body: "When I look at the screenshots from KDE3 I see alpha blending is enabled\nfor the toolbar. \n\nLast week when I checked KDE3 out from CVS with the beta2 tag, I couldn't \nenable alphablending for toolbars, antialiasing wasn't working and truetype \nfonts were not showing up in the font list. \n\nI runned qtconfig and even compiled QT3 with freetype support enabled. \nI was only able to get transparant menus working by editting \"./qtrc/kderc\"\nor something.\n\nAnyone know what's causing this?\n"
    author: "Mike Machuidel"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "I am also having this problem.  I also compiled QT with freetype support.  If you start a KDE app from a Konsole do you get alphablending in the toolbars, but still no AA fonts?  Do you also have AA fonts in qtconfig but not in any KDE apps?  Are you using Debian?  Maybe we can get to the bottom of this."
    author: "not me"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "Yep, I'm using Debian, but I didn't create deb packages.\nIn qtconfig antialiasing is indeed working, so I could\nbe a dependency problem. What do you mean with starting\na KDE app from a Konsole to get toolbar alphablending\nworking?"
    author: "Mike Machuidel"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "For example, if I hit the Konqueror icon on Kicker, the resulting window's toolbar icons are not alphablended.  If I start one from the command line in Konsole, it gets alphablended toolbar icons! (but still no AA fonts!)  I wondered if you experienced the same phenomenon.  My guess is that it has something to do with the kdeinit hack for speeding up application start times.  Does any KDE developer have any hints about how I might find out for sure, and maybe try to fix this problem?  Is there a way to turn off kdeinit when configuring?"
    author: "not me"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "Whoops, I was misled by Konqueror's config dialog, which isn't part of the same process as Konqueror itself.  Actually, apps started from a Konsole _do_ get AA fonts (marked as [xft]) in addition to alphablended toolbar icons.  Now, my only question is why do apps started with kdeinit not get XRender-enhanced?  I have confirmed that this is what is going on:  from a Konsole \"kdeinit_wrapper kcmshell fonts\" results in no alphablending or AA fonts in the resulting window, while \"kcmshell fonts\" results in AA fonts being available and alphablending being used. (for me, YMMV).  I'm using a qt-copy snapshot that is QT 3.0.2-20020219."
    author: "not me"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "That's a bug in one of the X-libs (XRender I believe) that happens to get triggered with apps started via kdeinit. Upgrading XFree to 4.2 should solve\nthat.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-03-01
    body: "Ah, OK.  Thanks for the info.  I just found on the mailinglists that restarting kdeinit solved the problem, so I'll probably just do that until Xfree 4.2 gets into Debian."
    author: "not me"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "It sounds like a linking problem,\npossibly by use of kdeinit the \napps are being linked to the wrong\nlibraries, just a guess.\n\nI'd like to check this myself as\nwell, I only need to recompile \nKDE3 again, would take me about\n5 hours to have it running properly.\n\nDo you know if I check KDE3 out from CVS\nand compiled it once, I can do incremental\ncompilation from then on without compiling\nthe whole source tree again.I never tried \nbut know it should be possible, \nany experience with that?\n"
    author: "Mike Machuidel"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "Ys, there are some problems with alpha-blending and AA in kde3beta2, but I am sure that developers are aware of this problem. Some icons have screwed semitransparent shadows and some not (especially when you resize windows) and semitransparent shadows are darker then in kde2.2 although they have same values. Switching to 'Show hidden files' (and vice versa) doesn't show hidden files unless you click on the refresh button, desktop icons don't appear on desktop when you create new file on the desktop - you have to log out/in to acomplish that, changing fonts (from one Xft to another Xft) removes AA so you have to log out/in to get your AA back with new fonts, etc. etc., but this is not bugs.kde.org, and we are talking about beta software :)"
    author: "antialias"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-03-01
    body: "Actually, I'm doing one of your tutorials\nright now :) , had enough trying to get AA \nworking, these tutorials are really nice,\nI didn't know GIMP could be that easy :)\ntnx"
    author: "Mike Machuidel"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-03-01
    body: "Where is the tutorial?\nThanks,\nd"
    author: "Daniel"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-03-02
    body: "on www.kde-look.org the \nauthor is antialias :)"
    author: "Mike Machuidel"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "there is a bug in versions of XFree86 <4.2 that causes AA to sometimes fail due to an unitialized variable. if this is indeed your problem, the attatched patch may fix it. the patch, which applies to qapplication_x11.cpp, was written by puetzk and recently posted to kde[-core]-devel."
    author: "Aaron J. Seigo"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-02-28
    body: "or just upgrade to XFree86 4.2, of course =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-03-01
    body: "Thanks!  That patch does look pretty evil though.  I think I'll just upgrade when xfree 4.2 gets into Debian."
    author: "not me"
  - subject: "Re: Alpha blending in toolbar"
    date: 2002-03-01
    body: "well, the patch is actually pretty innocuous. it just makes an array of 50 longs on the stack and initializes them, thereby ensuring that the memory location XFree86 will then use is set to a known and sane value. it isn't pretty, but it isn't unsafe or doing anything overly bizarre either."
    author: "Aaron J. Seigo"
  - subject: "Re:"
    date: 2002-03-01
    body: "I don't want to be rude, but I don't those \"manga screenshots\" are giving anime and manga a bad reputation. :-)"
    author: "Sotf"
  - subject: "Re: Manga Shots"
    date: 2002-03-01
    body: "\nEh, I think they're just SVG that he found off the web or made himself.  Somebody with decent fan level talent did them, although realistically, they aren't in the 'anime/manga style', so it's a bit of a misnomer - where that term means a certain western definition of styles that originated primarily in Japan (of course, several Japanese works aren't in that \"style\", most notably Akira, and the style has quite a few very distinct subclasses from Tezuka to Miyazaki to Go Nagai and then to the modern studio synthesizers and innovators in Studio Gainax and Studio Bones).  Of course, by a strict \"proper\" (i.e., literal) meaning of the word, it's dead on... as are Peanuts and Sandman, as well as all animated works (many amotaku don't know that manga covers animated as well as still art).\n\nUm, yeah... let me back away from fandom in general and just say:  They aren't really very \"Mangaish\" anyway.  And those who wouldn't know that wouldn't know what \"Manga\" means.  But hey... they are SVGs, and right now the format is kinda rare.  I wonder what the archetypal SVG will be (a la the cad nozzle)?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Manga Shots"
    date: 2002-03-01
    body: "That tiger picture seems to be pretty archetypal of most vector formats.  I've seen it in lots of screenshots."
    author: "not me"
  - subject: "Re: Manga Shots"
    date: 2002-03-01
    body: "\nI've always associated the tiger with postscript, and it's used to demonstrate various postscript interpreters.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "feature"
    date: 2002-03-01
    body: "When i want to do things in kcontrol menu that needs to be root, is bored to log as root and run kcontrol.Would it be difficult to add kdesu in the parts of kcontrol that needs to be root to use this modules? On Windows 2000 they implement this feature."
    author: "daniel"
  - subject: "Re: feature"
    date: 2002-03-01
    body: "It has already been done.  Are you using the newest KDE?  For example the \"Login manager\" and \"Date & Time\" modules have a \"Modify\" button that uses kdesu to start the module as root."
    author: "not me"
  - subject: "Re: feature"
    date: 2002-03-01
    body: "The next step is to do the same to file browsing in Konq. You browse to /etc as a normal user and want to edit fstab. A simple RMB click and \"edit as super user\" would be fab."
    author: "Matt"
  - subject: "Re: feature"
    date: 2002-03-02
    body: "K menu -> System -> File Manager (super user mode)\n\nNo, that's not exactly what you asked for, but it's something that's already there, for you to use now.\n\nThe thing you've asked for needs to wait for 3.1.  And, of course, if you expect to get it at all you need to file a wishlist bug report on http://bugs.kde.org/ like you're supposed to."
    author: "Neil Stevens"
  - subject: "Re: feature"
    date: 2002-03-02
    body: "You can do this with one trick. \n\n1. RMB on a Textfile and Edit File Type..\n2. Add a new Application \"kdesu -c kwrite\"\n3. Now you can use this entry to open it with SU rights\n\nCU"
    author: "Hetfield"
  - subject: "Even better"
    date: 2002-03-04
    body: "If you have the RAM:\n\nkdesu -c kfmclient openURL\n\n(Check it, I am not on KDE).\nThat has the advantage of meaning \"open with the default app as root\". That way, you can, for example, listen to a wav as root ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Even better"
    date: 2003-02-06
    body: "When I run 'file manager - super user mode' or kdesu -c or anything like that the window opens up then closes really quickly. I tried to add & to run it in the background but it doesnt work. why does this happen? Especially the file manager, it is made to run not close!\n\nthanks"
    author: "cratos"
  - subject: "Re: Even better"
    date: 2003-02-06
    body: "ps my email is d_meehl@hotmail.com if you have an answer for me (please) =)\n\nthank you"
    author: "cratos"
---
<a href="mailto:aseigo@olympusproject.org">Aaron J. Seigo</a> delivers again with <a href="http://kt.zork.net/kde/kde20020222_33.html">Kernel Cousin KDE #33</a>. This week's summary includes talk of the <a href="http://usability.kde.org/">KDE Usability Project</A>, adding a mini-golf game to the <a href="http://games.kde.org/">KDE Games</a> package, a resolved problem with animated GIFs, a new KConfig backend based on XML, inclusion of <a href="http://lists.kde.org/?l=kde-core-devel&m=101351949010285&w=2">an improved malloc</a> into CVS, work to include SVG icon support (<a href="http://wildfox.physos.info/kde-svg-icons-1.png">konqi</a>, <a href="http://wildfox.physos.info/kde-svg-icons-2.png">128 icons</a>, <a href="http://wildfox.physos.info/">more SVG</a>) into KDE, and KMail configuration migration.

<!--break-->
