---
title: "KDE Print: Advanced KPrinter Usage"
date:    2002-05-26
authors:
  - "chowells"
slug:    kde-print-advanced-kprinter-usage
comments:
  - subject: "Nice stuff, and a suggestion"
    date: 2002-05-26
    body: "The concept of import/export, files, printers, database are all the same thing. These should not be seperated. An application should use 1 to 3 kio's (file stream, page, && table/sql). The conversion between fileformats should be external to the application. During the open/save dialogs, the applicable choices should be allowed. Likewise, the printer should simply be part of output (it should be a page kio). \nThis would allow :\n1) a clean seperation of import/export from application, \n2) new ideas to be easily added in the future (perhaps via a plugin on the open/save dialog)\n3) the constant re-use of import/export code. If somebody creates an output of png, and a 2'nd person creates a conversion of png->jpeg, then the application is now able to save in jpeg. Likewise, if app does a page kio of PS, and somebody follows up with PS->tiff, then a nice fax interface is easier. Or perhaps PS->mng, etc.\n Finally, the Location menu should be easily configurable with shortcuts to these. Allow a user to add what they want. I don't believe in paper, so I have no need for a printer. But I would love to send the data directly to kmail or kopete to send a file directly to someone.\n"
    author: "a.c."
  - subject: "Re: Nice stuff, and a suggestion"
    date: 2002-05-26
    body: "Did you by chance get confused on your way to kde-devel? :-)"
    author: "Carbon"
  - subject: "Re: Nice stuff, and a suggestion"
    date: 2002-05-27
    body: "No, only lost in time. This is simply one area that i will write some code for shortly, if I have time.\nBut If I don't have time, I am simply hoping that others will think about it or do it."
    author: "a.c."
  - subject: "Whoa!"
    date: 2002-05-27
    body: "That is indeed impressive stuff :)!"
    author: "Janne"
---
A rather unusual application of the 'Special Printer' feature in <a href="http://printing.kde.org/">KDEPrint</a>
has been employed by KDE user <a href="mailto:pevans@catholic.org">Paul Evans</a> for quite some time now. After being asked
to write an article about his inventions, he did so, and now finally it has been <a href="http://printing.kde.org/documentation/contrib/kprinter">published on printing.kde.org</a>. Allow yourself to be amazed at the power and flexibility of KPrinter, minus any involvement of dead flattened trees spoiled by
ink or black dust particles...
<!--break-->
