---
title: "South African Government Council Praises OSS/KDE"
date:    2002-02-06
authors:
  - "dbailey"
slug:    south-african-government-council-praises-osskde
comments:
  - subject: "The only other interesting part"
    date: 2002-02-07
    body: "<i>\"Nevertheless, there are already many open source office suites, such as KOffice, Gnome Office and OpenOffice/StarOffice, which already offer equivalent functionality and ease of use. Appendix A in [12] gives a detailed discussion of OSS alternatives to MS Office. Hence the significant obstacle to adoption of OSS desktops is neither technical nor is it any longer a matter of ease of use - it largely has to do with familiarity and to some extent compatibility.\"</i>"
    author: "someone"
  - subject: "Re: The only other interesting part"
    date: 2002-02-12
    body: "there is really no alternetives to MS office"
    author: "Andreas"
  - subject: "Re: The only other interesting part"
    date: 2002-02-13
    body: "LOL.LOL. Have you even tried anything else?????"
    author: "hakim"
  - subject: "using MS Word ?"
    date: 2002-02-08
    body: "Apparently, if you do \"View Source\" on http://www.naci.org.za/docs/opensource.html, it looks really like HTML document created using Microsoft Word."
    author: "ariya"
  - subject: "Re: using MS Word ?"
    date: 2002-02-08
    body: "lol\nthere goes his street-cred"
    author: "domi"
  - subject: "Re: using MS Word ?"
    date: 2002-02-08
    body: "The guy is getting KDE translated into 11 new languages and maybe he's persuaded the most important African government to back opensource with KDE at the heart.  That's enough street cred for me. "
    author: "Bill"
  - subject: "Re: using MS Word ?"
    date: 2002-02-10
    body: "It's great and all, but I'd like to see the results before I believe it. The SA goverment is not usually this nice to all its languages. Xhosa is the ruling partys language. If it isnt followed up they will let the program fade away. But still the gesture is apreciated, it's just that I've seen too much gestures and too little action."
    author: "Kobus"
  - subject: "Re: using MS Word ?"
    date: 2002-02-10
    body: "http://i18n.kde.org/stats/gui/i18n-status-table.html\n\nI'm not familiar with the mechanics of i18n but if those figures can be taken at face value it looks like they are making good progress with Xhosa and starting Zulu."
    author: "Bill"
  - subject: "Re: using MS Word ?"
    date: 2004-03-24
    body: "pleas i want to know important of ms-word in a desktop enviroment"
    author: "abdulmaliq shakirat"
---
The South African National Advisory Council on Innovation 
(<a href="http://www.naci.org.za/">NACI</a>) 
recently
<a href="http://www.naci.org.za/docs/opensource.html">published a document</a>
(<a href="http://www.naci.org.za/docs/opensource.pdf">pdf version</a>)
with far reaching recommendations about open source and open standards.  
The paper contains a narrative on the South African translation project 
<a href="http://www.translate.org.za">translate.org.za</a>
which is <A href="http://dot.kde.org/1004383235/">translating KDE</a> into all 11 official languages of South Africa.  <i>"KDE itself is sensitive to language issues and is currently translated into 42 languages, far in excess of any of the popular commercial packages.  It took Translate six weeks of work to translate enough of KDE into Xhosa to make it ready for release. Another six weeks were spent for other minor components and documentation.  It was so easy to include Xhosa in KDE because there is a spirit of co-operation and collaboration in open software projects. As a result it enjoys some of the richest translation tools and is multi-lingual from the ground up. Information is freely discussed and shared, which means that KDE, like most other open software, is rapidly being enhanced by thousands of volunteer programmers around the world."</i>
<!--break-->
