---
title: "Kernel Cousin KDE #40 Is Out"
date:    2002-07-11
authors:
  - "rkaper"
slug:    kernel-cousin-kde-40-out
comments:
  - subject: "wrt Trolltech buyout"
    date: 2002-07-11
    body: "From the KDE myths page:\n\n\"Myth: If Trolltech (the makers of Qt) were bought by a company that wasn't friendly to Free software, Qt could be closed up or development halted thereby killing KDE and hampering anyone who had developed software with Qt.\"\n\nFollowing this is mention of the KDE Free Qt Foundation.\n\nI just wanted to point out that Qt is GPL, and so it does not need any sort of foundation to remain free.  The KDE Free Qt Foundation only exists to protect future proprietary software development.\n\nJust an FYI :)"
    author: "Justin"
  - subject: "Re: wrt Trolltech buyout"
    date: 2002-07-11
    body: "The problem would still arise for people who rely on the commercial QT license.\nThus if e.g. M$ bought Trolltech, Opera (based on qt) might be stuck with their old QT versions forever.\n\nFortunately though, as soon as MS stops developing QT, it becomes BSD licensed. Meaning everyone and his son can use it for all purposes including free and proprietary software."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: wrt Trolltech buyout"
    date: 2002-07-11
    body: "Well, who decides whether they've stopped developing it?\n\nIf I was Microsoft and just bought Trolltech, I'd release a QT servicepack every year that would fix some bugs and create some new ones to keep it from becoming BSD licensed."
    author: "me"
  - subject: "Re: wrt Trolltech buyout"
    date: 2002-07-11
    body: "The KDE QT FREE FOUNDATION board consists of two KDE and two trolltech members. In case of a tie, the KDE members decision counts.\nIt's all there in the documents."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: wrt Trolltech buyout"
    date: 2002-07-12
    body: "In the case of Qt getting a BSD-license\nthen, I suppose, someone (like MS) could do whatever\nthey want with the software, including telling\nthe world it should use their special \n(incompatible) version of it.\nCorrect?"
    author: "Axel Ivarsson"
  - subject: "Re: wrt Trolltech buyout"
    date: 2002-07-12
    body: "What if Microsoft bought Trolltech and kept developing it (and not necesarily always in a direction that was always favourable to KDE) so as to prevent it from falling under a BSD license. But then it asked for such high licensing fees and other restricitons on commerical developers that it would effectively be able to kill KDE as a desktop platform as a mainstream environment.\n\n"
    author: "Robert Hayes"
  - subject: "Well, I said it bothered me..."
    date: 2002-07-11
    body: "\nI posted today about the whole binary release issue, and remarked about how I hated to keep saying the same thing - I just submitted an item to the myth page, so now I can just refer people to the URL.\n\nIncidently, I (very timidly) mentioned that, among the wide range of OSes that KDE runs on, there is a project to make it run on Windows.  Just to make sure - that is still true, correct?  I've read a few things about both it and work on the OSX port... the OSX port, of course, is now high profile, and releases binaries along with the KDE releases, whereas I haven't heard anything about the Windows effort in quite ahwile.\n\n(Also, when you submit a myth, there should be something about releasing the text for use... it's obvious, but always a good idea to have a legal release disclaimer)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "so that was you =)\n\nyour submission is now live on the site... thanks for contribution...\n\nand you are right about the legalese falderdall on the submission page... i'll look into that imediately ...\n\n/me senses Dre getting an email ... ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "Maybe you should keep track of who submits the myths too."
    author: "ac"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "why would i do that, mr ac? ;-)\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "Proper credit?"
    author: "ac"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "i plan to keep the site free of personal names and credit (including my own). i'd rather people judge the information on the basis of the information rather than based on who wrote it. i find the anonymous delivery of information to be more impactful and meaningful. hopefully it will eventually move to a more neutral domain (e.g. one that isn't mine) as well ..."
    author: "Aaron J. Seigo"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "I could host it as myths.unixcode.org if you'd allow the system to host myths about other projects as well. Would be good to see a debunking page that lists the truth about multiple projects, including competing ones."
    author: "Rob Kaper"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "hmmm... well... yes and no IMHO. I think that www.kde.org/myths should be the place (including a link on the main page - at least for a while). The reasons should be obvious :)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Well, I said it bothered me..."
    date: 2002-07-11
    body: "I think a non kde.org site would be more neutral and would not appear to be propaganda.\n"
    author: "Rob Kaper"
  - subject: "Klipart"
    date: 2002-07-11
    body: "Hello,\n\nInstead of creating KOffice Klipart, why not create a project with the same goals but for all the free office suites? You could then distribute it as cliparts-collection-VERSION.tar.bz2, which would be used by all free office suites (Koffice, GNOME Office, etc).\n\nOf course nothing keeps the others from using this images (if they're GPLed), but you would get more contributions if it wasn't KDE-specific.\n"
    author: "Carg"
  - subject: "Re: Klipart"
    date: 2002-07-11
    body: "Can't but agree. But for the developers to take notice of this you'll probably have to join koffice mailing list and the other mailing lists and post the email there.\n\nDavid"
    author: "David Siska"
  - subject: "Re: Klipart"
    date: 2002-07-11
    body: "Even if you don't post it to koffice-devel, it'll be likely noticed already. Afterall, the Dot is the KDE user and developer favorite site, isn't it ? :-)"
    author: "ariya"
  - subject: "kdemyth of openness"
    date: 2002-07-11
    body: "http://kdemyths.urbanlizard.com/viewMyth.php?mythID=48\n\nThis page says that being a developer is not a requirement to contribute to the community.  Just sign up to the public mailing list and give your opinions.  Hmm, I think the myth to openness is not such a myth.  They maybe public mailing lists, but I have to say my experience has not been one of total joy.  \n\nFor example, check out the kde-usability list.  An outsider replied to some comments about a post I made on kcontrol and he got slammed by one of the developers over an idea he had.  Here is a links to this incident: http://lists.kde.org/?l=kde-usability&m=102624589014771&w=2 and http://lists.kde.org/?l=kde-usability&m=102624858717781&w=2\n\n\"If you would have checked out the latest CVS...\"  He just assumed that the poster did not know what he was doing.  Why couldn't the developer responded, \"Did you check out the latest CVS?\"  Then he would have found out that the poster did infact have the latest CVS and the question at hand was still not resolved.  The poster then asked if \"anyone ever tried to make a real GUI for this, a la the Windows Device Manager\" and the developer kindly replied back code it yourself.  This was just a freaking question!!!  Instead of getting objective usability questions being thrown around, only personal attacks occured.  The best though has to be the developer's last remark: \"who liked the good times where people attached (fake-)screenshots and .ui files to their postings\"  Real open to non-developers wouldn't you say?\n\nTo prove this isn't an isolated incident, another incident happened over the very thing posted in the previous dot article on the koffice-devel list.  To get everyone up to speed, this was over the proposal to remove the inital dialog boxes in koffice.  The kde-developer replied back saying that \"Without going into this long monologue I want to point out that KWord is an application that has been build on the premise of how it should be done.\"  The way it should be done????? ... hmm real objective there don't you think?  This strikes me as a bad case of not inveted here syndrome.  Not only this, but another post was made on the same list regarding the dialog box issue and the only replies back were to the weak parts of the argument.  The strong points were just ignored.  Hell, even look at the posts in the pervious dot article.  All the users are for having the option to turn off the koffice dialog box, but core developers still refuse to admit that only a few want it.  I don't mind when people do not agree with me, but don't give me crap like \"it's the right way.\"  This kind of behaviour just makes me feel stronger in my opinion and want to push for it more out of spite.  Instead let's have a good heated argument and the best solution will arise in which everyones needs are addressed.\nLink to this thread: \nhttp://lists.kde.org/?t=102385752700002&r=1&w=2 \n\n\nThis has just been my experience, but in general from my reading of the mailing lists, often replies to an outsider's post are very condesending.  I do want to say that this true for all developers; some are extremely nice and post nice objective replies, but a problem does exist here and it should be addressed in my opinion.  These incidents will not keep me from following and enjoying KDE or from posting my opinions, but I think a real problem exists in which some of the core developers are not open to any suggestions that they did not come up with.  I guess I'm going to be banned from the lists now for posting this :-)."
    author: "Biz"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "I think there's a difference between criticizing something because you want to make it better and criticizing something because you don't like it.\n\nYou can tell the difference by the tone of the criticism, even if the criticism is factually identical.  If your tone is off, you'll be ignored.  This isn't just KDE, this is everywhere.  Open source, closed source, non-programming projects too.\n\nBut sometimes it's hard to resist whacking the wasp's nest to see if anything comes out...well, good luck!"
    author: "ac"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "Hello,\n\nI am criticizing to try to make things better!  I work in a computer lab where they use Linux/KDE and the suggestions I've tried to make were based on problems I've noticed working there.  The thing is here is that the posts/replies I was referencing weren't even mine.  I was just commenting on the tones of replies and saying that this could be problematic.  \n\nThanks,\nJesse"
    author: "Biz"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "Developers get a lot of \"good suggestions\". The problem is that a lot of \"good suggestions\" are crap and submitted by absolutely clueless people. In such an environment (some) developers tend to get a bit edgy when it comes to critique, even when it is constructive. That's a shame of course. A way to overcome that is to build up a reputation. If developers get to know you as someone who delivers usefull and constructive criticism they will be more inclined to take your input more seriously.\n\nOh and apart from that, you don't expect everyone in a 100+ group of people to be nice, do you? Face it, some of us are jerks ;-)\n\nCheers,\nWaldo\n--\nService depends on my mood and your attitude"
    author: "Waldo Bastian"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: ">A way to overcome that is to build up a reputation. If developers get to know\n>you as someone who delivers usefull and constructive criticism they will be\n>more inclined to take your input more seriously.\n\nOh no, I'm screwed then!!  No good reputation about me right now :-).  In all seriousness though, I do want to contribute my thoughts and more importantly my experiences with others using KDE and the problems they have had.  I just wanted to point out some negative points in the way ideas are treated on the lists in the hopes that they may get corrected.  This is the kinda talk here is what I was hoping for on the mailing lists; good talk where a mutial understanding is reached.  It does take more effort to do so, but it's well worth the extra engery.  \n\nAnyway, thanks for taking the time to respond back to me.  I honestly do appreciate it.\n\nLater,\nJesse"
    author: "Biz"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "first off, thanks for taking the time to read the site. even though it is still in pre-release form, i'm happy that some people are finding it useful and thought provoking...\n\n> This page says that being a developer is not a requirement to contribute to\n> the community\n\nand i've seen it with my own eyes enough times to know that this is true. \n\n> Just sign up to the public mailing list and give your opinions. \n\nif your opinions happen to be baseless and poorly thought out (even if you think otherwise), don't be surprised when they are dismissed. and don't expect people to immediately praise every concept you come up with as being brilliant either (even if it is brilliant). everyone explains and defends their ideas and concepts and nobody is Right all the time. it is part of the process of developing ideas as a group.\n\ni might also suggest keeping in mind that many of the people on the lists are not native english speakers and that paying attention to what they mean rather that the words they use is often a good idea.\n\n> This kind of behaviour just makes me feel stronger in my opinion and want to\n> push for it more out of spite.\n\never thought that this might be at least part the problem? if not, you should (because it is).\n\n\nby the way, there was a solution settled on regarding the koffice dialog. it was not the dismissal you keep making it out to be. i even pointed this out in a previous thread on this site."
    author: "Aaron J. Seigo"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "Hello,\n\nFirst off, the posts I was talking about were not even mine .. and I wasn't trying to push that dialog box further.  I only used that as an example.   \n\nAnyway, I just wanted to say this to everyone.  Please try to watch the tone of your responds back to people.  If you don't like a suggestion, just ignore it or try to kindly say why it's a bad idea.  If something can't be done or you don't know how to do something, just say so.  I honestly thought I was just going to get back some hate mail for what I said, but I have to say I there have been some pretty good posts back.  Thanks!\n\n> least part the problem? if not, you should (because it is).\n> ponses make me feel a lot better.  \n\nYes, this is a problem and It's something many people do; hence why I brought it up.  People do this all the time.  \n\n>  by the way, there was a solution settled on regarding the koffice dialog. it > was not the dismissal you keep making it out to be. i even pointed this out\n> in a previous thread on this site.\n\nAnd I posted back to you about how I thought there wasn't a strong agreement.  I was admitting I was wrong.  I promise, there will not be anymore posts about it from now on.  At least I do know it's been settled.\n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "When I was a SuSE 7.0 user, I remember to see an application a la Windows 9x's Device Manager.\nI'm not a SuSEr any more, could any SuSer say something about this?\n"
    author: "Ricardo Cruz"
  - subject: "Admin Tools (was: Re: kdemyth of openness)"
    date: 2002-07-11
    body: "Speaking of which.... the coming version of Lycoris appears to have some funky admin tools that are based on KDE.  Does anyone here know much about them? How complete are they, are they distro specific, which license are they under etc?\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: "When I was a SuSE 7.0 user, I remember to see an application a la Windows 9x's Device Manager.\nI'm not a SuSEr any more, could any SuSer say something about this?"
    author: "Ricardo Cruz"
  - subject: "Re: kdemyth of openness"
    date: 2002-07-11
    body: ">For example, check out the kde-usability list. An outsider replied to some \n>comments about a post I made on kcontrol and he got slammed by one of the \n>developers over an idea he had. Here is a links to this incident: \n>http://lists.kde.org/?l=kde-usability&m=102624589014771&w=2 and \n>http://lists.kde.org/?l=kde-usability&m=102624858717781&w=2\n\nThat was me. Please read my second mail again. If you still didn't get it, read it another time. My indent was not to jump at someones back and knock him down. \n\n>\"If you would have checked out the latest CVS...\"\n\nYes, that was a bit harsh and I should have answered better to the real question (I didn't find the location, Waldo later noted that it is in kdenonbeta), but wanted to make my point. \n\nFurthermore, my last remark was targeted at the development on the kde-usability list in general. When I joined the list, there were real improvements, but that changed a lot in the past :(.\n\nI appriciate the efforts of the list to put up usability guidelines and so on, but some ideas about how applications should be simply miss an important aspect: the current development. That's fine user who report whishlist items (using bugs.kde.org), they shouldn't need to care. But it's a different thing with people that drop in and make wild speculations on the lists.\n\nI am all for contributions, but if you have no concrete way to turn them into code, it's all pointless (see my mail).\n\nI was among the first to support those poeple that tried to improve the situation for non-coders by adding the kde-widgets to designer and promote designer as a tool for usability-interested people to create the suggestions for improvements. I was among the people who wrote documentation on designer and KDE for beginners. Do you call that \"not open to non-developers\"?\n\nI could as well also unsubscribe from kde-usability, but when developers unsubscribe, who remains? I want that project to succeed!\n\nPlease let me apologize to all that felt insulted by my mail. I hope you do understand now where the problem is.\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "New sport?"
    date: 2002-07-11
    body: "Is it becoming a new sport to pick single comments/statements from single developers which one dislike and to publish them publicly on news boards or editorials to spread the word that KDE is bad/closed/harsh?"
    author: "Anonymous"
  - subject: "Re: New sport?"
    date: 2002-07-11
    body: "no, i don't think this is a game.  i think this person was just frustrated with a common tone coming out of some of the developers.  this frustration is amplified because the negative tones are coming from key figures in the kde project.  the post is a little harsh, but i can see were it's coming from.  that's just my 2 cents."
    author: "Anonymous"
  - subject: "there are myths in the myths?"
    date: 2002-07-11
    body: "Look at myth number 27 \"Konqueror and web standards\"\n\nIt says \"the Konqueror developers have made good headway, primarily by implementing the various web standards including [...]support for:\n* HTML 4.1\"\n\nWhere did they get this HTML 4.1 from? Certainly not at the W3C\nAs for HTML, there exists HTML 4.0, 4.0.1, then XHTML 1.0 and XHTML 1.1\nI have seen in the past people claiming IExplorer was far better than Mozilla because Mozilla had no support for HTML 4.5\nI hope in this page, it's only a typo.\n\nBy the way, this very page does not mention PNG as standard supported by Konqueror. Does this mean that the situation has not improved (I'm still with KDE 2.2) and PNG support is not yet achieved? I hope is a forgotten entry.\n\noliv"
    author: "oliv"
  - subject: "Re: there are myths in the myths?"
    date: 2002-07-11
    body: "No, PNG support has been in Konqueror for quite a while (all the way back to the 2.2.x series).  I could view PNGs, and transparent PNGs, just fine with Konq 2.2.2. :)"
    author: "dwyip"
  - subject: "Re: there are myths in the myths?"
    date: 2002-07-11
    body: "Wrong !!!\n\nThe alpha channel is not supported in Konqui 2.2.2\n\nIt is supported in konqui 3.0 but badly cf http://www.libpng.org/pub/png/pngs-img.html"
    author: "shift"
  - subject: "Re: there are myths in the myths?"
    date: 2002-07-11
    body: "my typo, it is supposed to be 4.0.1. and i'll add PNG there too, since konqueror has supported that quite well for some time now. thanks for catching those items!"
    author: "Aaron J. Seigo"
  - subject: "Re: there are myths in the myths?"
    date: 2002-07-11
    body: "But is PNG _really_ working? Every time since 2.0, people told me it is, but when I opened an HTML page with some alpha-transparent PNG, it was just binary transparency. And the people outside out \"KDE advocate\" world have had the same experience I have. Official PNG page agrees with me. They updated recently and now say:\n\n\"full 32-bit alpha support as of version 3.0  (KDE3); binary transparency for palette images; no single-shade transparency support for 16-bit grayscale; strange banding artifacts in some alpha images; only binary transparency in versions prior to 3.0;\"\n\nBut the problem's root is Konqueror bases its PNG rendering on QT, which is a design mistake. Konqueror resorts on some library whose goal/function is not blending/manipulating images, so is always going to be late on this. PNG can have 64 bits images (i.e. 16 bits of transparency). As long as QT won't have this, Konqueror will not either. That's why Konqueror has the PNG working only from 3.0 (from my tests, other KDE users tests and PNG official homepage test). That's why Mozilla has had it working for two years.\n\nMy understanding of this peculiar PNG/QT2_vs_QT3 issue is due to the use of qpixmap that could not have smooth transparency before QT3 (while qimage could). This is a similar problem Attal (clone of heroes of might and magic) has with its images.\n\nWell the good news is the presence of qt3. The bad news is 16 bits alpha in konqueror is not for now.\n\noliv.\n\nBut if the PNG official page says it works now, I believe it :)"
    author: "oliv"
  - subject: "Re: there are myths in the myths?"
    date: 2003-05-20
    body: "I've found the best place to test your browser's ability to view transparent PNG's is:\n\nhttp://pobox.com/~jason1/testbed/pngtrans/"
    author: "Andre Hinds"
  - subject: "KDE Myth Site"
    date: 2002-07-11
    body: "Great work on the KDE Myth Site :-)\n\nJust two suggestions:\n\n- Please add a \"Show all Myths in one Page\" link \n- I can't find an article concerning the \"KDE doesn't have corporate support\" Myth. Does such an article already exist?\n\nKeep on your great work on usability and PR,\nTackat"
    author: "Tackat"
  - subject: "Re: KDE Myth Site"
    date: 2002-07-11
    body: "\"KDE doesn't have corporate support\"\n\nThere is the KDE League (http://www.kdeleague.org/).\nOn the main page:\n\"The KDE League is a group of industry leaders and KDE developers focused on facilitating the promotion, distribution, and development of KDE.\""
    author: "Ricardo Cruz"
  - subject: "Re: KDE Myth Site"
    date: 2002-07-11
    body: "> Please add a \"Show all Myths in one Page\" link \n\nthis has been requested a few times now.. i'll have this feature in by the weekend...\n\n> I can't find an article concerning the \"KDE doesn't have corporate support\"\n> Myth. Does such an article already exist?\n\nnothing like that yet. if no one submits such a myth, i'll write one up myself. it'll probably need to be accompanied with the corallary \"KDE is controlled by corporations\" myth ;-)\n"
    author: "Aaron J. Seigo"
  - subject: "Damn..."
    date: 2002-07-11
    body: "... those screensavers are nice! :)"
    author: "jd"
  - subject: "Re: Damn..."
    date: 2002-07-11
    body: "I totally agree. Helios is my favourite. I kept them on my M$ 2k box but because they were shareware I then removed them. Now that they have been open sourced, they keep a permanent home on my machine.\n\nOn a side note, I have been using Linux on and off for a few years now, but I was never happy with it being a replacement to M$, as my Internet Banking, and a few other apps, never worked to my satisfaction.\n\nI bought SuSE 8.0 Professional and I am now a convert. Everything worked out of the box, which not even M$ can do. Even my el-cheapo digital camera worked without me having to configure a thing.\n\nNow, if I want to do Internet Banking, I bounce my machine into Windows and once finished I head straight back.\n\nI have seen IE 5 running under Wine, so I simply need to set that up at some stage. With Konq I can log into the banking site, but they use some lame-brain non-standard floating DIV code for some of the key navigation elements, which simply do not appear under Konq.\n\nMy Desktop at the moment .. Quartz + Lightspeed ver3 + Redmond XP colours.\nOld habits die hard :) \n\nThanx for the great work that goes into KDE."
    author: "A convert from South Africa"
  - subject: "Clipboard crazyness"
    date: 2002-07-11
    body: "I have yet to upgrade to 3.0.2, so before I do it I wanted to inquire whether the clipboard has been fixed to work like it does in Windows & Mac.  Version 3.0.0 has it messed up a bit and there isn't an option (that I could see) to change its behaviour.\n\nIf it isn't in 3.0.2, is it planned for 3.1.x?"
    author: "Frank Rizzo"
---
Kernel Cousin KDE <a href="http://kt.zork.net/kde/kde20020710_40.html">Issue 40</a> is out, covering a myriad of news from the <a href="http://www.kde.org/mailinglists.html">KDE mailinglists</a>. This new issue covers <a href="http://users.skynet.be/kborrey/klipart/klipart.html">KOffice Clipart</a>, new artwork for <a href="http://unixcode.org/atlantik/">Atlantik</a>, printing issues, new OpenGL screensavers and the upcoming website on <a href="
http://kdemyths.urbanlizard.com/">debunking KDE Myths</a>. Enjoy!
<!--break-->
