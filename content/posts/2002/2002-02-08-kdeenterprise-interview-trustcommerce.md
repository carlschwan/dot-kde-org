---
title: "KDE::Enterprise: Interview with TrustCommerce"
date:    2002-02-08
authors:
  - "jbacon"
slug:    kdeenterprise-interview-trustcommerce
comments:
  - subject: "Well done."
    date: 2002-02-09
    body: "This is a very nice and balanced interview.  The guy rightfully loves KDE but also made some good points about some GNOME apps pushing the envelope where KDE apps weren't.  The guy also says he was a former GNOME fan, to his credit.\n\nI hope this inspires our wonderful KDE developers to even outdo themselves.  Of course, we don't have the money and development muscle of companies to profit on for our apps (like Ximian or Netscape or Sun without which the difficulty of GTK/bonobo development would have already slowed down progress -- this is not a flame, even Miguel has admitted this with Mono) but everything is going in the right direction to make this change.  \n\nKDE/Qt development is so easy now with KDevelop and even without that plain Open Source KDE apps with almost purely volunteer backing are doing very well.\n\n"
    author: "KDE User"
  - subject: "Klinux"
    date: 2002-02-09
    body: "Hi, I've also thought about the possibility of having a Klinux or K*BSD or whatever in the same way as in the article is explained: Only KDE.\n\nKDE should only need to target their development to this Klinux and ,after, major distributions would have to \"port\" KDE to their own Linux version.\n\nThis would also help KDE people to create a nice KHardwareConfigurator because they wouldn\u00b4t need to think in the different ways distributions do the configuration.\n\nI don\u00b4t know what KDE developers think about that. I also don\u00b4t know if this would require a lot of work for them or if this would simplify their work. Maybe I'll take look and help on that one ;-)"
    author: "lanbo"
  - subject: "Re: Klinux"
    date: 2002-02-09
    body: "Wy not make a website www.klinux.org and promote the opereting system maby we cane base it one Debian \n"
    author: "Dr88Dr88"
  - subject: "Re: Klinux"
    date: 2002-02-09
    body: "Indeed, Debian would be a nice choice for this :) I'm using testing right now\nand it rocks. I think the best thing to do is basing it on Potato\n(stable) and then upgrading all needed components from the ground up.\nThe installer could be graphical and could set up XFree and the kernel with\nhardware autodetection (like kudzu). Actually, I don't think we need\na new debian for this, debian can easly be extended and everybody is\nfree to do it. The only think you should try is getting it into the \nnext stable :)\n"
    author: "Machuidel"
  - subject: "Re: Klinux"
    date: 2002-02-11
    body: "on the other hand \"Klinux\" could be a new distro based on the LSB, now that would be a nice turn of events, a totally KDE centric Linux that is LSB standard, easy to install (ah la mandrake)and windows easy to install programs. "
    author: "L.D."
  - subject: "Re: Klinux"
    date: 2002-02-09
    body: "Why not Gentoo (www.gentoo.org) :)?"
    author: "janne"
  - subject: "Re: Klinux"
    date: 2002-02-09
    body: "Another useless discussion: It will not be done except *you* do it.\nAs KDE doesn't target to Linux, so will it not to \"Klinux\"."
    author: "KDE Developer"
  - subject: "Re: Klinux"
    date: 2002-02-09
    body: "I have recently been toying around with this concept - I am thinking of creating my own linux distribution (just for fun), based on mandrake linux, and based on a KDE desktop.\n\nI think the goal should be to create something EASIER to use than windows or a mac (most desktop distributions seem to try to reimplement the windows UI - that is certainly true of Xandros, Desktop/LX & Elx - and lets not forget \"Lindows\").  KDE in my opinion has already far surpassed windows in potential ease of use - the desktop can be customized down to something so simple a chimp could use it, or turned into your own super-personalised mega-desktop.\n\nAs far as actually doing this, I have done some of it.  I have a kcontrol module that can embed an arbitrary X app in the control center - this lets me embed Mandrake's drakconf tools into kcontrol - they are still GTK apps, so it looks funny, but otherwise it works well.  I have also tryed to create a set of config files that setup the desktop with a \"best of all worlds\" (in my opinion) UI - with mac features: Menu's at the top of the screen, icons appear on the desktop when removable media are inserted, windows features (the k-menu, a taskbar) and of course UNIX (network transparency, remote administration, scriptablity & an (optional) terminal icon.\n\nWhich brings me to my question.  If you were setting up a linux/KDE computer for your grandma (if your grandma's a tech-head, insert suitable non-computer literate relative here) to use (and self-administrate) how would you do it?"
    author: "Jonathon Sim"
  - subject: "Re: Klinux"
    date: 2002-02-09
    body: "Is someone from Mandrake reading this?  *PLEASE* adopt this module for embedding your config apps in KControl.  This should be the default when the user chooses the K Desktop Environment.  The Mandrake Settings are nothing but a mess otherwise.\n"
    author: "KDE User"
  - subject: "Jonathon Sim"
    date: 2002-02-10
    body: "I have noticed that integration of drakconf (& other x-admin tools ) into kcontrol is a fairly popular feature.  When I get the time I will pretty it up a little and try posting it to mandrake contrib.\n\nThe only major problem with it is with keyboard focus - when you run it as an ordinary user, kcontrol steals the keyboard focus after you enter the root password - and ordinary \"click to focus\" doesnt work, so you have to press tab before typing anything into the embedded config tool.\n\nThe way I get around this is through a hack - when you move the mouse over the embedded app, it grabs the keyboard focus, when you move it off, it releases it again.  While mostly this works well (ie you dont notice) sometimes you might be surprised where your key presses go."
    author: "Jonathon Sim"
  - subject: "Re: Jonathon Sim"
    date: 2002-02-10
    body: "woops.  Making the title my name is a little pretentious - must have typed in the wrong box"
    author: "Jonathon Sim"
  - subject: "Re: Jonathon Sim"
    date: 2002-02-10
    body: "Well, seeing that Tichard Dale seems to be working on making Kalyptus generate Perl bindings, \nperhaps even a Qt port of interactive.pm  would be possible, sometime in future, \nmaking the drak* tools use Qt+KDE?\n\n\n"
    author: "Sad Eagle"
  - subject: "Re: Jonathon Sim"
    date: 2002-02-10
    body: "Well, seeing that Richard Dale seems to be working on making Kalyptus generate Perl bindings, \nperhaps even a Qt port of interactive.pm  would be possible, sometime in future, \nmaking the drak* tools use Qt+KDE?\n\n(For those that don't know:\nMany of Mandrake tools don't use Gtk directly, bur rather an abstraction \nlibrary that lets them run transparently as text apps, Gtk ones,\nand even thorugh https. \n\nPerhaps this would be  a good reason for me to learn some Perl)\n\n\n\n"
    author: "Sad Eagle"
  - subject: "Re: Jonathon Sim"
    date: 2002-02-10
    body: "Yes I looked at that myself.  Unfortunatelly I really can't reconcile myself with perl - I know that some people like perl, but I only understand that in the same sense that I understand sado-masochism: I dont really care, so long as you leave me out of it.\n\nReally the user doesnt care what toolkit an application is written in, so long as there is a consistent (and pleasent) look and feel - something that could be achieved with a kde-default style widget theme for gtk, and judicious use of krdb.  Unfortunately noone seems to have written this widget theme, and krdb seems to reduce the interface of some applications (netscape) to giberish if you use the \"wrong\" font.  Perhaps I am missing an option that would allow me to enable it only for GTK apps?"
    author: "Jonathon Sim"
  - subject: "Re: Klinux"
    date: 2002-02-11
    body: "Forget Mandrake if they cared about KDE they wouldn't be using GTK and there CEO wouldn't be making false claims about Gnome and KDE usage. Why go through all the trouble to correct there big mistake by going with GTK? Take a serious look at Desktop/LX and you'll see a great beautiful KDE only distro. Here's an interview i did with there CTO and some screenshots.\nhttp://www.gui-lords.org/article.php?sid=195&mode=thread&order=0&thold=0"
    author: "Craig"
  - subject: "Re: Klinux"
    date: 2002-02-11
    body: "My understanding is that Desktop/LX does not release all their software under a GPL, or other free licence (correct me if Im wrong).  In this they are like many linux distributions (Suse, Caldera .....), but unlike others: Debian & Mandrake & no doubt a many more Ive never tried.  So I think a GPLed distribution that pays people to work on KDE is what I would call KDE friendly, even if they didn't even include kde in their distribution.  \n\nPersonally, I dont use linux just so I can run a KDE desktop"
    author: "Jonathon Sim"
  - subject: "Re: Klinux"
    date: 2002-02-12
    body: "OK your wrong:) Consider yourself corrected."
    author: "Craig"
  - subject: "Re: Klinux"
    date: 2002-02-12
    body: "OK Im partially wrong (as usual).  A quick google search brought me this:\n\nhttp://www.desktoplinux.com/articles/AT3599362330.html\n\n, where Joseph Cheek mentions that the icons & sounds & wallpapers are not freely redistributable.  I also know they include various non-free software - eg realplayer, adobe acrobat reader. That is probably what gave me the non-free software impressions I had (I guess icons and sounds arent software?).\n\nHowever if their software is free I should probably have a look at it. I haven't seen any of the many reviews of desktop/LX, or even the screenshots on their site, actually mention their administration software  - if you have tried it perhaps you could point out its cool features, so I can swipe them for myself.\n\n"
    author: "Jonathon Sim"
  - subject: "Re: Klinux"
    date: 2002-02-11
    body: "Hey,\nI was wondering how you go about having freshly inserted media be auto mounted and show up on the desktop. I know there's any number of automounting utilities out there but the ones I've tried seem to take the approach of calling system mount(...) functions continuously -- which is a less than optimal approach -- especially on my laptop, wherein my floppy and ultrabay zip drive continously grind and chunder as they perpetually attempt to mount uninserted media. It's noisy, and I imagine a drain on battery. It's also sufficiently crude that, while it does work, it's clearly a kludge and offends the part of me that likes elegant solutions vs. hacks.\n\nAttempting to solve the problem myself, I poked about for a while to see if there were ioctls which could tell you if media was inserted but frankly, I'm new to linux (programmer coming from BeOS) and don't really even know where to start. \n\nI'd love to work on this with you because I think, for one, that even with all the wonderful usability of KDE on top of whatever *nix you're using, you can *never* expect grandma to open a terminal and type \"mount /mnt/zip\" after inserting a zip disk. Or as is often the case for me, I get zip disks with unpredictable partitionaing/filesystems (don't ask why) and I have to su to root and type \"mount -t whatever /dev/hdc<whatever> /mnt/zip\" over and over with different permutations until it works. And then, of course, if it's a FAT filesystem only root gets to modify the files on the mounted disk, since root mounted it. What a pain.\n\nEven as a fella who's been using a terminal of some sort for 7 or 8 years, I *still* hate needing to use one just to mount a zip disk. I remember with fondness being able, under beos, to insert a zip disk with 2 different partitions with 2 different filesystems, and having two fresh icons appear on my desktop. It was very civilized.\n\nAnyway, I'd love to help. This is my biggest pet-peeve with linux (not kde/gnome/whatever).\n"
    author: "Shamyl Zakariya"
  - subject: "Re: Klinux"
    date: 2002-02-11
    body: "There are two programs I have tried that do this, and both work,more or less.\n\nThe most well established is autorun, used (and made) by redhat in particular to autorun cdroms (load audiocd player if audio cd is inserted, load konq otherwise)., at:\nhttp://sourceforge.net/projects/autorun/\n\nThe other is Kamount \nhttp://freshmeat.net/branches/4998/\n\nBoth programs focus on cdroms, although KAMount does mention zip disks etc.  Both have features the other doesnt: autorun lets you run arbitrary programs when a cd is inserted/removed, but KAMount can read the windows icon of the drive & make that the device icon (cooooool)- and maybe it supports zip disks (who knows?)\n\nI suggest that you try these two programs, and see if they help with your zip-disk.  I already have a few patches & scripts hanging around for both of them, and I would really like to know if one of them supports zip disks, so I can choose finally which I'll use\n\nYou can email me at: jonathonsim@iname.com <Unless your a spammer>\n"
    author: "Jonathon Sim"
  - subject: "Lycoris Desktop/LX"
    date: 2002-02-11
    body: "We actually have been very successful with our KDE only integrated distribution named Desktop/LX.  I guess since you are proposing this idea, it means the publicity hasn't been directed towards the dot yet. My appologies. Please visit our site ( http://www.lycoris.com ) and download our latest release, I'm sure you'll find the integrated control panel for networking, X configuration as well as the already set up file associations, realplayer, flash player and java virtual machine quite easy to use.\n\nAll my best,\n\nJason Spisak"
    author: "Jason Spisak"
  - subject: "I would buy Lycoris Desktop/LX if present in India"
    date: 2002-02-11
    body: "Any hopes of Lycoris D/LX available internationally?"
    author: "Asif Ali Rizwaan"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in India"
    date: 2002-02-12
    body: "We'd be glad to ship on to you right now.  We've sent hundreds to places as far apart as Australia and Ireland.  As far as the international language availability, yes we are going to be rolling out many national versions in the next few months. If you have any other questions, please feel free to email me.\n\nAll my best,\n\nJason Spisak"
    author: "Jason Spisak"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in India"
    date: 2002-02-12
    body: "Hi\n\nas David Johnson suggested just below you, why don't you release your configuration tools in a kde-linux package and make it downloadable for people not using Lycoris ?\n\nI know it doesn't seem like a good idea for Lycoris to make other distro's users to have the same tools but:\nyou use KDE to promote Lycoris so it would be fair to release your own work to improve KDE and promote it.\n\nMore over, if Red Hat, Mandrake, Debian... users can have your configuration tools, you'll get much more feedback from experienced linux users (because lots of Lycoris users are, I believe, beginners) and, thus, your own products will benefit from the community.\n\nI say all that because, in my opinion, the current weakness of Linux comes from its distributions. Most of them are commercial-based (even if free) and always try to re-invent the wheel. That's not good for Linux because ALL distros lack something another one has !\n\nOne example:\n\nI installed Mandrake-8.1 and got bad-looking GTK configuration tools, bad network (SMB shares, SMB printers) detection.\nI installed ELX and got a QWERTY keyboard whereas I'm using an AZERTY keyboard.\nI installed Redmond Linux (few months ago) and destroyed my other linux partition when trying to resize it.\nI installed Debian and... gave up, nothing worked well :(\n\nAll those problems are specific to each distro and don't exist on others.\n\nWhat I want is a perfect distribution featuring just what every linux user needs (Kernel, OpenOffice, KDE, XFree, gcc+libs, GNU tools and some servers) without forgetting security and stability, perfect configuration tools and good TTF fonts.\n\nI also noticed in Lycoris that, when you clicked on the web browser icon, mozilla opened. Why not Konqueror ? We don't need 2 good browsers on Linux. If you're making a GNOME distro, use Galeon+Mozilla but if you're doing a KDE distro, just use konqueror !\n\nI hope you'll understand that I'm not trolling but just asking for more cooperation between the different distributions and between distro developpers and the developpers community."
    author: "Julien Olivier"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in I"
    date: 2002-02-13
    body: "I really doubt the tools are portable.  That is the main problem with config tools.  Many other distros have config tools (SuSE, Mandrake) but they are not portable either.  To make their tools available to everyone, Lycoris would have to spend lots of time porting them, which just doesn't make sense.  If they are GPL'd, then anyone can port them.  Lycoris has no obligation to do it themselves.\n\nI was also puzzled by Mozilla being the default browser.  Perhaps KDE 3 will be able to change their minds about that.  IMHO their web browsing capabilities are about equal, but Konqueror has AA fonts and desktop integration while Moz does not.\n\nI've got some other questions too.  I am wondering why Lycoris hasn't partnered with CodeWeavers to provide the CrossOver plugin with Desktop/LX, and possibly Wine 1.0 in the future.  Has this been considered?  Also, aren't some of those icons from WinXP directly (with a palette shift maybe)?  It sure looks like it."
    author: "not me"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in I"
    date: 2003-04-19
    body: "Actually Lycoris partnered with TransGaming and has WineX3.0 available.\n\nNone of those icons are from XP - they are original"
    author: "Name"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in India"
    date: 2003-04-19
    body: "Actually the source code to Lycoris Desktop/LX IS Open Source.  Anyone can have the code ANY time they wish to use it.\n\nAs far as Mozilla - Mozilla has an e-mail client that is far superior.  The mozilla customized for Lycoris Desktop/LX is also enhanced above the typical Mozilla.  We need ONE *GOOD* browser in Linux, and Konq just isn't it!!\n\nDestroying your linux partition can't be blamed on a distro.  It's always wise to back up before using ANY system.\n\nMaybe you ought to give back to the community and program YOURSELF??"
    author: "Name"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in I"
    date: 2003-04-19
    body: "> Maybe you ought to give back to the community and program YOURSELF??\n\nI do.\n\nBTW: this thread is quite old. I changed my mind on LOTS of the things I said back then."
    author: "Julien Olivier"
  - subject: "Re: I would buy Lycoris Desktop/LX if present in India"
    date: 2005-08-06
    body: "u can buy linux distros on cd/dvd in india at www.linuxbazar.com"
    author: "spl"
  - subject: "Re: Lycoris Desktop/LX"
    date: 2002-02-12
    body: "Why did change the name from Redmond Linux for? That was a funky name, just imaging the slogan or headline:\n\n\"Redmond opens gates to Linux\"\n\noh, well..."
    author: "reihal"
  - subject: "Re: Lycoris Desktop/LX"
    date: 2006-03-29
    body: "This distro seems to have gone belly-up. Too bad, It's the best Linux distro I've used. It does have a few flaws, 4 ports are open and the firewall is enabled. It does not detect usb storage devices. It does have all plug-ins for the browser, which is one important factor between a operational Linux and another wasted disc you burned. Ubuntu is a joke, I don't see what functions that OS is providing. I cannot connect to the internet, and to change the background picture requires you to know where the picture file is, even though you're looking at it in the panel. I think the myth behind Ubuntu is that it has been around for a long time and people download it thinking because it is the first one of so called \"stable\" releases so it gets another notch on the pole. But it is really crap. Not enough people knew about Lycoris and to realize what kind of OS it is. It boots up faster than any of the other 30+ distros I've tried not to mention it operates considerably faster. Xandros just got the ok to sell it's OS from CompUSA stores. If you can look at a screenshot of Xandros and look at Lycoris, Xandros is ugly. Visually you will not even come close to a \"Windows XP\" enviroment with Xandros. "
    author: "Linux newbie"
  - subject: "Re: Klinux"
    date: 2002-02-12
    body: "Let me rain just a bit on your parade, okay? KDE is an OS neutral desktop. As such, stuff like a hardware configurator should not be a part of core KDE. Distros and systems that want stuff like that should create their own. The wonderful folk at KDE have more than enough to do without having to figure out how to configure hardware on Linux, *BSD, Solaris, IRIX, etc.\n\nAnother way to do this is to simply create a kde-linux package where all the Linux specific stuff goes, a kde-bsd package for any BSD specific stuff, another for Solaris, etc.\n\nWay back when Linux started getting ported to different architectures, Linus discovered an amazing thing. Portable software is more stable and robust than non-portable software. Make KDE as system neutral as you can make it, and get improved quality as a side effect."
    author: "David Johnson"
  - subject: "Re: Klinux"
    date: 2002-02-14
    body: "Well, that's just not going to do.  KDE could provide Kontrol modules where the front ends are well-defined (with extensions possible), implemented in a set of fuctions provided separately by the distro.  We could provide a standard implementation of those functions that strictly adhers to the LSB, and let distros derive from it if they like."
    author: "Chris Bordeman"
  - subject: "Re: Klinux"
    date: 2002-02-14
    body: "Well, that's just not going to do.  KDE could provide Kontrol modules where the front ends are well-defined (with extensions possible), implemented in a set of fuctions provided separately by the distro.  We could provide a standard implementation of those functions that strictly adhers to the LSB, and let distros derive from it if they like."
    author: "Chris Bordeman"
  - subject: "[...] I'd like to see a distribution that is KDE.."
    date: 2002-02-09
    body: ">[...] I'd like to see a distribution that is entirely based around KDE. All configuration is done through the KDE control panel, and the system is designed from the ground up to never, ever need to access it at the shell level.\"\n\nThis is also my dream. When will KDE be in control of the OS? KDE should not be an accessory (as a Desktop Environment) but an Integral part of the OS. I wonder what happened to the LinuxConf like KSysConf tool? I hope KDE 4.x will be the integral part of most distros...\n"
    author: "Asif Ali Rizwaan"
  - subject: "Re: [...] I'd like to see a distribution that is KDE.."
    date: 2002-02-09
    body: "Hmmm... I was six minutes earlier ;-)\nNice to see that it\u00b4s not only me thinking about the same thing."
    author: "lanbo"
  - subject: "Re: [...] I'd like to see a distribution that is KDE.."
    date: 2002-02-09
    body: "- Corel/Xandros\n\n- Desktop/LX\n\n"
    author: "Corba the Geek"
  - subject: "Nightmare not dream!"
    date: 2002-02-09
    body: "\"...is designed from the ground up to never, ever need to access it at the shell level\"\nI hate Windoze for just this reason. Until the arrival of KDE, I was quite content to have a few windows on my screen, one usually running xemacs, but most with a shell prompt. I still have lot of shell prompts arround but one on top of the other with the wonderful kde console and the terminal window at the bottom of konqi. The present KDE/LINUX combination allows the user to grow from naive to expert on a gentle learning path. Under Windoze everyone remains at the naive level.\nDon't just look at the price - feel the quality.\n"
    author: "Malcolm Agnew"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-09
    body: "I think what's really needed is a generalized configuration interface to everything, for which KDE acts like a frontend. Probalby, this could be accomplished using linuxconf or webmin modules. The big thing is, we don't want to take away the power of UNIX, but going to a shell shouldn't be neccessary unless you really need to tweak something low-level. Adding X resolutions, turning on/off daemons, etc, could all be done from KDE using such a frontend. \n\nI'm just not sure how to code it :-) Details, details..."
    author: "Carbon"
  - subject: "What would really be cool..."
    date: 2002-02-09
    body: "...would be if the Webmin guys generated their pages using XML. The browser interface could be generated with XSL using the Xalan/Xerces libraries (at least I think that's what they're called), and GUI equivalents could be created using KXmlGui and Glade (after being passed through appropriate XSLs). Implementing the changes would then be a matter of firing up the scripts identified in the XML document.\n\nThe great thing is there'd then be a great variety of configuration systems, but all using the one base, so you wouldn't have the repetition of bugs between, e.g., Linuxconf and Webmin. If you wanted you wouldn't even need to have Webmin running, which would ease security worries some people have.\n\nI'm afraid I've neither the time nor the expertise to do this at the moment, but it'd be cool if what's left of the KSysCtrl guys or maybe someone else could have a look at it. "
    author: "Bryan Feeney"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-09
    body: "Well, I use SuSE, and since version 7.0, I haven't had to use the shell to configure anything. This is due to YAST2, which is wonderful. I used to do most everything by hand, but this is just so easy, and it works. SuSE has also integrated all of the YAST2 modules into the Kcontrol center, so I _can_ configure my system entirely from KDE.\n\nActually, I think you have a good point, but you missed the previous point. There's a big difference between needing to have shell control and being able to have complete shell control. Having both options is key."
    author: "Rich Jones"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-09
    body: "Let me elaborate : I too like to click a mouse a few times to get something done which, if successful, I won't do again for months (or years). This is of course completely different from my/your frequent use of a command line entry:\n\n% locate something | grep -i somethingelse | grep -v butnotthis\n\nwhich I find myself doing daily.\n\nBut what if there is no success with the click click method:\n\nAn example: I recently bought an HP-Office psc 950. Installing under SuSE 7.3 with yast2 was completely unsuccessful - I could neither print nor scan. (Installation under my small Windoze partition was also unsuccessful). But with a some help from the open source community I went through a not inconsiderable number of one line changes entered on the command line and using vi to make other small changes. Now I can print and scan. (But not under Windoze).\nAcutally SuSE's (KDE) control center isn't that bad. When it works. But what happens if there are problems. Suppose I was trying to configure kmail and hit a problem. The first thing I would have done in 1976 (UNIX Edition 6) would have been:\n\n% man kmail\n\nand look for the files and/or environment parameters kmail uses.\nOoops! kail doesn't have a man page.\nPerhaps its somewhere else.\n\n% locate kmail | grep man \n\nNo, right first time, kail doesn't have a man page. So we really are getting to be like a poor man's Windoze.\nAnd don't tell me to use the kdehelpcenter (also no man page) since that dosn't work either (see:\n\tKDE bug report logs - #27240\n\tklauncher said: 'Error loading kio_help'.)\nI could go on ...\n\n\n"
    author: "Malcolm Agnew"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "Debian has a manpage for kmail, konqueror and others. Other distributions are free to use them if they wish."
    author: "ac"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "I didn't know that. Thank you. A very good reason to switch from SuSE to Debian!\n"
    author: "Malcolm Agnew"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "Too bad there are many more reasons for not switching. Trust me, you have reaaal easy right now.  :-)\n\n-N. (former long-time Debian user)\n"
    author: "Navindra Umanee"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-11
    body: "Such as? Debian and KDE often play nice, albiet the Debian testing and unstable distributions have rather fscked up KDEs at the moment."
    author: "Carbon"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-11
    body: "it's been a while, but off the top of my head:\n\n- hopelessly out-of-date stable releases\n- dselect\n- try building .deb's from source packages across releases. not pretty.\n- misses many \"advances\" that make Linux easy.  much manual fiddling required.  some basic features don't work out of the box.\n- flame-ridden debian-devel list and/or too much politics\n\nDebian of course has its nice points when you know what you are doing.  I used it for years, and had it installed on at least 10 boxes (for clients).  They all loved the results.  I just grew tired of it after a while.  I say, most of the *BSDs probably beat Debian at its game anyway, and are probably leaner, meaner and cleaner options.\n"
    author: "Navindra Umanee"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-12
    body: "<i>- hopelessly out-of-date stable releases</i>\n<br><br>\npoint given, mitigated (in my eyes) by always running unstable :-)\n<br><br>\n<i>- dselect</i>\n<br><br>\nI haven't touched dselect in over 2 years.  There is simply no need to. apt-* is your friend.\n<br><br>\n<i>- try building .deb's from source packages across releases. not pretty.</i>\n<br><br>\nDon't really know about this one.  What exactly is the problem?\n<br><br>\n<i>- misses many \"advances\" that make Linux easy. much manual fiddling required. some basic features don't work out of the box.</i>\n<br><br>\nLike? OOTB, my network, video, and sound work.  What else do you need?  Esp. when the programs themselves are so sweetly packaged, to give you \"workability\" out of the box...\n<br><br>\n<i>- flame-ridden debian-devel list and/or too much politics</i>\n<br><br>\nWell, the obvious solution to this is: don't read the mailing lists :-)  OTOH, I'm on the debian-kde list, and it is no worse (and mostly better) than the other lists I'm on (various kde-*, devel, solaris admin, and web prog, so a fairly wide variety).  And while I'll give *BSD all of their well-deserved mad-props, I've never run across something to compare to the simplicity of apt-get update &amp;&amp; apt-get upgrade.  And no, ports isn't even close (I always ended up having to manually tweak compiles and the order of things, not even counting the enormous difference in time (download/unpack vs. download/unpack/./configure/make/make install/with various pauses for workarounds inbetween(nested parens are cool))).\n<br><br>\nI use stable for my servers, and unstable for the workstations here at work (about 15).  I do the apt-get upgrade routine on my machine first, and if there is any breakage (fairly rare, abo. thrice a year), I just wait for a day and try again.  My user's couldn't be happier, especially when they want a new program, call up, ask for it, and before we hang up, it's installed and available in their K-Menu B-).  Ain't nothing even close....\n<br><br>\nAll this is IMHO, YMMV, IANAL, BRB, and STFU, addendum, etc, and so on...  IOW, no disrespect intended to any individual :-)\n<br><br>\n--Me\n\nP.S.  Apparently, I can't use <i> tags.  Oh bloody well.  I left them in so that you could differentiate...</i>"
    author: "David Bishop"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-12
    body: "> point given, mitigated (in my eyes) by always running un0stable\n\nAnd if I mention that unstable is sometimes buggy and *always* a moving target, then you will probably say it's mitigated by running stable.  :-)  \n\nActually, I'm sure it's fine for some users with big pipes and little to lose by apt-get upgrading all the time, or for people with the resources to test and clone a moving target before deploying.  I just don't fit in that category.\n\n> I haven't touched dselect in over 2 years. There is simply no need to. apt-* is your friend. \n\nYes, apt-* is magic, I will give you that.  That doesn't mean I still don't fallback on dselect from time-to-time, even if it is just to get a list of available packages.  Dselect is also still, afaik, the official and default front-end to managing debian packages.  Isn't that what the installation routine dumps you into at the end?  Ugh.\n\n> Don't really know about this one. What exactly is the problem? \n\nBuilding debian binary packages from source packages can be an incredible pain.  Especially when you are running a stable distribution and want to update from source packages in unstable.  What happens is that the source packages depend on many little utilities and these utilities turn out to be incompatible across Debian releases or sometimes outright missing in one version and present in the other.  I always had to hack random perl scripts to get packages to compile, upgrade/downgrade perl*, and sometimes the changes conflicted between packages...\n\nThen there's the little matter of undocumented source dependencies...  which has been ostensibly fixed in unstable for many years.  Too bad there hasn't been a stable release (or has there?).  :)\n\n> Like? OOTB, my network, video, and sound work. What else do you need? Esp. when the programs themselves are so sweetly packaged, to give you \"workability\" out of the box...\n\nThere were always a bunch of small things that I had to fix.  \"Poweroff\" would never poweroff my ATX box, it would just do a normal halt.  Then there's the poorly cobbled network configuration files, probably fixed in unstable.  Or apache always had to be recompiled to raise an artificially low compile-time limit on MaxServers. A LinuxConf that's included but that doesn't really work. Yes, I made customized .deb's for plenty of things -- unfortunately I can't access them right now.\n\nTell me, does the Debian install support LVM?\n\nOh, but I will say that the Debian bug-tracking system is very responsive.\n\n> flame-ridden debian-devel list and/or too much politics \n\nLet me clarify: I am strongly discouraged and have doubts about the future of Debian, when I witness purely political motivation and/or ego-trips causing artificial hardships for the user and would-be Debian developer.  \n\nWhat about the thread that non-free would be made more inaccessible to the new default Debian user? What about the impossibility of becoming a new debian developer after jumping through hoops for many many months?  It was that kind of thing that made me lose courage and faith in Debian.  \n\nOn the other hand, it sure sounds like you have a nice set-up and something that works for you.  \n\nI would finally like to point out that although I used to use and advocate Debian, even at the time, I would never advocate Debian to the newbie fresh-from-windows or the newbie i-ve-heard-the-linux-hype.  I would always grudgingly name one of the other user-friendly ones, in good faith.\n\nCheers,\n-N.\n\nPS I didn't really mean to whine today, but people did ask for it... :)\n"
    author: "Navindra Umanee"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "Use the kde help center.\n\nAnd don't tell me it doesn't work.  It is not KDE's fault that your distribution's packages are messed up so that you can't use KDE the way it was intended.  There is no reason for KDE to use man pages whn it has a better alternative available (the help center, which is available in all working KDE installations).  KDE has a very powerful command-line interface (thanks to Ian's efforts), it is not abandoning the command line.  KDE aims to combine the best of GUIs and Unix."
    author: "not me"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "But there I beg to differ. Man pages were/are one of the most innovative ideas belonging to UNIX philosphy - right from its roots in the early seventies (and what is LINUX when not just a very good version of UNIX).\nMan pages offer a unified, consistent, precise and concise format for documentation (somewhat like a corset).\nThe style of the \"help center\" offers a different, more verbose style of documentation - also valuable, but neither precise nor consistent. But sometimes in the server room I don't even have a screen cabable of running X/KDE. \n\n\"It is not KDE's fault that your distribution's packages are messed\" and not mine either. Nor would I have found out why the distribution was messed up if I didn't have a bash command window where I could enter:\n\n% ldd /opt/kde2/bin/meinproc\n        libxslt.so.1 => /usr/lib/libxslt.so.1 (0x40016000)\n        libxsltbreakpoint.so.1 => not found\n        libm.so.6 => /lib/libm.so.6 (0x4004d000)\n        ...\nto find out how my distribution \"was messed up\"."
    author: "Malcolm Agnew"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "man pages are far from being a universal documentation source anymore.  They rarely have all the information you need, they are slow to navigate and impossible to link between.  The GNU tools already use the (IMHO absolutely awful) texinfo format instead of man pages, due to these limitaions.  KDE is doing the same, only they are providing the help center, which integrates man pages, info pages, and KDE help files into a single interface where you can (in theory) get all the info you need.\n\nNote that I am not denying the usefulness of the command line.  I hear you when you say that you couldn't have found the problem without using bash.  The command line is a useful tool, and KDE is taking advantage of it.  But KDE need not have man pages, because you don't need to get your info about KDE from the command line.  If you need info about a KDE app, it is because you are running KDE, in which case you have access to the help center, unless your packages are messed up, which KDE can do nothing about.  If your \"man\" package was broken and man wouldn't run (happened to me before), would you blame program developers for not giving enough information when you type \"program --help\"?"
    author: "not me"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "I disagree that kde does not need man pages. Ever get one of those errors in windows that said see the online help but you could not get the online help or even the windows gui actually running? It becomes one of those catch 22 things then. It is a really good idea for kde to have man pages for at least the things that can stop it from starting so if something goes wrong you can still read the docs to figure out what needs to be fixed. Now I have to admit I have never had that problem with KDE and hope I never do but I like having it there just in case. \n\nOTOH I have had that problem way too often in windows."
    author: "kosh"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "The Debian maintainers are adding some man pages AFAIK, but it seems only to their packages and not to original KDE?"
    author: "someone"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "The KDE help center doesn't have any information on problems that would stop KDE from starting anyway, so there is no danger of there being any catch-22 (AFAIK, there is no documentation on problems like this - if there was, perhaps it would be best placed in man pages).  However, the help center can be run without starting KDE, so even if KDE couldn't start, you could still see the help.  Plus, if your GUI won't start, that's an XFree problem, which is not a KDE problem.  XFree has its own documentation."
    author: "not me"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "\"man pages are far from being a universal documentation source anymore\"\nAy, there's the rub (as Hamlet would say).\n\n\"They rarely have all the information you need\"\nThey should give you sufficient information as concisely as possible and also point to additional information - in particular the names of files that the program uses. I don't always want to go through a bloated 100 page tutorial.\n\n\"... impossible to link between\"\nThere are bags of tools which do this, i.e. xman.\n\n\"The GNU tools already use the (IMHO absolutely awful) texinfo format\"\nYes they are absolutely awful - Richard Stallman's \"not invented by me\" paronia.\n\n\"... unless your packages are messed up, which KDE can do nothing about\"\nFor those interested (searching on google shows that other folk are having the same problems - at least on the latest versions of REDHAT aand SuSE):\n\n\"/opt/kde2/bin/meinproc\" (part of KDE if I am not very much mistaken) is linked to both libxslt.so l. But libxsltbreakpoint.so does not exist anymore, at least not in SuSE 7.3. The kludge (which works for me) is to make a symbolic link:\n\n% ln -s /usr/lib/ibxslt.so /usr/lib/libxsltbreakpoint.so\n\nand KDE help center works quite sweetly.\n\n"
    author: "Malcolm Agnew"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "Oops! missed out a word here \"..linked to both libxslt.so l\" should read \".linked to both libxslt.so and libxsltbreakpoint.so\"\n"
    author: "Malcolm Agnew"
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: ":: They should give you sufficient information as concisely as possible and also point to additional information - in particular the names of files that the program uses. I don't always want to go through a bloated 100 page tutorial.\n\nWell, I'd agree with that, but I'll point out two semi solutions.  The first is the rumor I've heard that debian packagers make man pages for KDE apps.  They may just consist of \"This is what it does and see the help file for more\", but they exist.\n\nSecond, I just poked for a minute, and it seems reasonable to think that man files (albiet large, bulky ones) should be able to be autogenerated.  After all, that's the point of docbook - flexability).  You might look into the Jade Wrapper utility docbook2man as a starting point.  I'd agree that man pages would be nice.  If you do it, tell me.  :)\n\nOn the other hand, it's not a big enough deal for me to write the scripts to generate the man pages.\n\n--\nRvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Nightmare not dream!"
    date: 2002-02-10
    body: "xman hardly makes it easier to navigate man pages.  xman can't solve problems like the lack of hyperlinks and the fact that man pages are one continuous page of text.  The problem is that the man format is outdated for large, complex systems such as KDE.\n\nI think the problem we have here is that the KDE documentation stinks in general.  You mention not wanting to have to go through a 100 page tutorial - Nobody does.  There should not be any such thing in the help anyway.  Nobody uses that.  The help should be easy to navigate and include such things as command line arguments and environment variables in an easy to access way, along with help for less experienced users.  Right now, this documentation doesn't even exist for most programs.\n\nThere is a certain kind of documentation that, if it existed, would probably be best placed in man pages - documentation on problems starting KDE or the help center.  This documentation doesn't exist.  Until such documentation exists, KDE has no need for man pages."
    author: "not me"
  - subject: "It's not a dream!"
    date: 2002-02-11
    body: "Lycoris Desktop Linux (D/LX) is a KDE only distro based on Caldera and works wonderfully well. It's been on my main production box since late summer last year. Lycoris is at http://www.lycoris.com and the Lycoris Community at http://www.lycoris.org . I think it  will be worth your time to check this distro out."
    author: "TuxAir"
  - subject: "Re: Nightmare not dream!"
    date: 2006-12-14
    body: "The problem with kde is obtaining information!\nFor instance, HOW does one use the kde help center?!?\nAs near as I can tell -- after trying for an hour -- it is useless!\n\nOn the other hand \"man ... \" seems to actually work (I am not saying\nthat kde help center doesn't work, the point is HOW ?!?... at least\nwith man all you need to know is the three letters: \"man\")."
    author: "dpaddy"
  - subject: "Re: Nightmare not dream!"
    date: 2002-03-03
    body: "I thought unifying the linux experience was for the creators of distributions (Redhat, SuSE, Mandrake, ad infinitum).  KDE seems to have its hands full as it is with streamlining and keeping up with library changes, without going real deep into hardware configuration.  I love KSysVInit.  I love the formatted information in the control center.  I love process manager.  Those things aid in troubleshooting and doing what you otherwise would have to do commandline.  Now you have an easier time doing things commandline to change hardware configuration.  Heck, KDE even has its choice of text editors (Advanced, Kate, or no frills Text-Editor) so you don't have to dive into Vi(m), emacs, or the rest.  My point is, the tools are there already for those who want to link GUI and configuration.  For those who are not expert enough, there are configurators available from your favorite distro.  To the developers, please focus on streamlining and bug removal instead of spreading too thin with a \"Klinux\"."
    author: "Greg"
---
I have added <A href="http://enterprise.kde.org/interviews/trustcommerce/">another interview</a> to <a href="http://enterprise.kde.org/">KDE::Enterprise</a>, this time with Adam Wiggins from <a href="http://www.trustcommerce.com/">TrustCommerce</a>. Adam recently wrote a <a href="http://people.trustcommerce.com/~adam/office.html">report</a> on their use of Open Source and KDE, and <a href="http://enterprise.kde.org/interviews/trustcommerce/">this interview</a> was a chance to further explore the issue. <i>"It's simple: KDE makes the UNIX desktop usable for non-IT workers. If it wasn't for KDE, we'd have to pay a lot of money for proprietary hardware (Apple) or software (Microsoft). More importantly, the machines are more stable and easier for our sysadmin to maintain. [...] I'd like to see a distribution that is entirely based around KDE.  All configuration is done through the KDE control panel, and the system is designed from the ground up to never, ever need to access it at the shell level."</i> 
<!--break-->
