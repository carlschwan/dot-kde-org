---
title: "KDE 3.0.4:  Fourth Enhancement Release (And Two Security Advisories)"
date:    2002-10-10
authors:
  - "Dre"
slug:    kde-304-fourth-enhancement-release-and-two-security-advisories
comments:
  - subject: "ha!"
    date: 2002-10-09
    body: "Finally, some real KDE news for our friends at L&M!\n\nGood job guys, glad to see KDE 3.0.x so well maintained."
    author: "Navindra Umanee"
  - subject: "I'd just like to say..."
    date: 2002-10-09
    body: "Incredible. The quality and consistency of the KDE project is just amazing. Thanks to all our hard working project members (coders and otherwise!) that *continue* to produce the best damn desktop available.\n\nThree cheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "thanx"
    date: 2002-10-10
    body: "thanx for doing such a good job. kde is great!\nand for just don't hiding bugs and doing much blabla - just fixing that stuff (great to see this probs in beta stage).\n\ngreeting gunnar"
    author: "gunnar"
  - subject: "=)"
    date: 2002-10-10
    body: "i wondering about making a whole kde release for only a security patch !!!"
    author: "Mohasr"
  - subject: "Re: =)"
    date: 2002-10-10
    body: "Only a security patch ?\n\nWatch http://www.kde.org/announcements/changelogs/changelog3_0_3to3_0_4.html"
    author: "JC"
  - subject: "Re: =)"
    date: 2002-10-10
    body: "And what about people upgrading from KDE2, Gnome or Windows?\n\nIt's much better to give them 3.0.4 than 3.0.3 plus patches."
    author: "Roland"
  - subject: "Well Done"
    date: 2002-10-10
    body: "Well Done!!!\n\nKDE is the best.\n\nCheers\nCoomsie :3)"
    author: "Coomsie"
  - subject: "Crashed my box"
    date: 2002-10-10
    body: "Why is it that a security fix is a whole new release?  I guess they are pretty major security releases, but it seems that everytime I try to upgrade my KDE, it crashes.  This one is no different.  Upgrading Mdk 8.2 with KDE 3.0 installed, trying to upgrade to 3.04 (this one).  Seemed to install fine using the RPMs and urpmi.  Logged out and logged back in and things looked good, but then Kalarm crashed.  I started Korganizer.  It started fine, but then when I tried to add an event, it crashed.  Got it to output the debugging symbols and went to view them using Kwrite, but it crashed when I tried to save them.  Crash, crash, crash.  I finally completely uninstalled KDE and then reinstalled KDE3.0, after I installed KDE 2.2.2.  Don't ask, I was fighting with KDE3.0 all night just to get back to where I was this afternoon. This is a work machine and I need it for work.   But I love KDE so damn much that I eargerly await the next release so I can try to upgrade again. :-)"
    author: "Anonymous"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "Install from source, don't count on [first week] distributor packages."
    author: "Anonymous"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "Better; use Gentoo (www.gentoo.org). It's a source based distro so no RPM-dependancy-hell. You don't have to do anything manually just type \"emerge kde\" and you can enjoy the latest and greatest. Besides the ease of package management Gentoo has a lot of great packages e.g. Unreal Tournament 2003. With one command you can enjoy this great game."
    author: "John Herdy"
  - subject: "Re: Crashed my box"
    date: 2002-10-11
    body: "props to John, gentoo *ROCKS*\nI tried to do the same with Slackware and scripts, but gentoo does everything right, and is easy to admin too."
    author: "philip howells"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "Perhaps you should try a different distro? I can almost guarantee that when the Debian packages are out, they'll install perfectly first time."
    author: "Jon"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "\"I can almost guarantee that when the Debian packages are out, they'll install perfectly first time.\"\n\nYeah, and it seems that we will be getting Debian-packages for KDE3 sometime in 2004"
    author: "Janne"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "> Yeah, and it seems that we will be getting Debian-packages for KDE3 sometime in 2004\n\nDebian packages are already avaiable, for Woody (stable) and Sid (unstable):\n\nhttp://download.au.kde.org/pub/kde/stable/3.0.4/Debian/\n\n\n"
    author: "debs"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "Official packages? To my knowledge, there are un-official packages available, but no official-ones."
    author: "Janne"
  - subject: "Re: Crashed my box"
    date: 2002-10-11
    body: "Most of them are done by the packagers that do the official packages, so that's official enough for me :-)\nThey are just not uploaded to the debian servers.\n\nI read that the real official packages are postponed to the GCC3.2 switch.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "I used RPMs because it is the swiftest way to update.\nThen I notice that things don't work as they should and commence to exchange\npackages. Mostly after arts, kdelibs and kdebase the distribution-specific\n\"bugs\" are less annoying. \nThough... upgrading from sources has always produced the most satisfactory results."
    author: "Monster"
  - subject: "Re: Crashed my box"
    date: 2002-10-10
    body: "If you don't care about the security patch, why are you even upgrading KDE and complaining that it's a new release. \n\n\npffah"
    author: "fault"
  - subject: "Re: Crashed my box"
    date: 2002-10-25
    body: "Exactly the same problem on Mandrake 8.2 with all MDK updates applied.\n\nI've upgraded (with RPM) from KDE3.0.1 to KDE3.0.4 today. urpmi installed without problem.\n\nBut:\n- KControl can't be launched (immediate crash).\n- KMail crashes if you go to its configuration or try to do a new mail (!)\n- Konqueror crases (I searched \"crash\" in this page, and after the last occurence, it crashed, several times)\n\nI even created a blank new user, with no ~/.kde related directories, and without /tmp/kde related files or directories : it didn't got better.\n\nI give up. I'll try to go back to KDE3.0.1 if I can. And maybe put away Mandrake as a linux distribution, because I do not trust it anymore, and because I'm fed up with its unstable features.\n\nI only hope KDE will work smoothly on the new distrib I'll choose, because I've becomed a Konqueror and KMail addict!"
    author: "Mathieu Bois"
  - subject: "Re: Crashed my box"
    date: 2002-10-29
    body: "Personally advice that should not play on a work machine unless u are sure there is no harm to data. A way to prevent this is to partition some directory into separate partition.\n\nI done by this:\n\n/dev/hda1 - /boot\n/dev/hda2 - swap\n/dev/hda3 - /\n/dev/hdb1 - /var\n/dev/hdb2 - /usr/local\n/dev/hdb3 - /home\n/dev/hdb4 - /work\n\nwhat happened before i post this thread is my Mozilla 1.2b drive me crazy with tons of problems and bugs, so what I did was to uninstall Mozilla 1.2Xft (that including my earlier version, because it was done by upgrading from default shipped Mozilla 1.0.1 -> Mozilla 1.1 -> Mozilla 1.2b), this also caused Galeon and Evolution to gone due to dependencies.\n\nOne good plan was my settings, mails, profiles, bookmarks was retained and when I reinstall from Redhat package installer with Mozilla, Galeon and Evolution, everything is exactly the same...\n\nCool Redhat 8......very cool....\n"
    author: "Neo Gigs"
  - subject: "[OT] Hmmm. I wonder if Lindows uses KGhostview.."
    date: 2002-10-10
    body: "It would be annoying enough executing malicious code as an ordinary user... but as root."
    author: "Corba the Geek"
  - subject: "No RedHat binary release any more"
    date: 2002-10-10
    body: "For KDE 3.0.3, RedHat binaries have been shipped.\nIs this no longer done for 3.0.4 or will they shipped later?"
    author: "Scotty"
  - subject: "Re: No RedHat binary release any more"
    date: 2002-10-10
    body: "Ask RedHat. KDE is not providing any binary packages (how many times must this be repeated?)."
    author: "L.Lunak"
  - subject: "Re: No RedHat binary release any more"
    date: 2002-10-10
    body: "Sorry, but see http://www.kde.org/info/3.0.3.html - (unofficial!) Redhat packages had been provided under http://download.kde.org/stable/3.0.3/contrib/RedHat/7.3/ !?!\n\nI know that Redhat is packaging KDE different from KDE - the \"unofficial\" release was following the KDE-packaging policy and I have installed those on my RHL7.3 bases system.\nScotty"
    author: "Scotty"
  - subject: "Re: No RedHat binary release any more"
    date: 2002-10-11
    body: "The \"onofficial\" means that a volunteer has provided them. "
    author: "Sad Eagle"
  - subject: "Re: No RedHat binary release any more"
    date: 2002-10-11
    body: "RedHat does not provide KDE updates in general.\n\nThey support Gnome instead.\n\nTake a look at distributions that provide KDE packages on regular basis. These are mentioned at the release page.\n\nPersonally I'm using Debian, and it works fine. Upgrading is simple as 'apt-get upgrade kde'\n\nEleknader"
    author: "Eleknader"
  - subject: "Re: No RedHat binary release any more - look here"
    date: 2002-10-13
    body: "Here they are...\nftp://ftp.du.se/pub/mirrors/kde/stable/3.0.4/RedHat/7.3/i386/"
    author: "Jay S. Curtis"
  - subject: "Can I update my KDE 3.1 beta 2 to this ?"
    date: 2002-10-10
    body: "I am running KDE 3.1 beta 2 on Mandrake 8.2. I am wondering if I can/should update(?) to this from KDE 3.1 beta 2. Any suggestions/comments ?"
    author: "_deadfish"
  - subject: "Re: Can I update my KDE 3.1 beta 2 to this ?"
    date: 2002-10-10
    body: "3.0.x releases are bug and security fix releases.  All the changes versus 3.0.3 are either fixes to the existing code or fixes that are in 3.1.x, backported to the older code.\n\nSo I'd say you already have the updates over 3.0.3.  The security advisories don't say that the 3.1 tree is affected.\n\nWill"
    author: "Will Stephenson"
  - subject: "Re: Can I update my KDE 3.1 beta 2 to this ?"
    date: 2002-10-10
    body: "3.1 beta2 is also affected by both the security issues. Either patch your source or stop using the two affected programs."
    author: "fl0yd"
  - subject: "Re: Can I update my KDE 3.1 beta 2 to this ?"
    date: 2002-10-11
    body: "The KGhostview buffer overflow was fixed September 26 [1].  It looks like the fix was included in 3.1 Beta 2.\n\nThe file sharing security hole appears to have been fixed October 7 [2], so the exploit would still exist in 3.1 Beta 2.\n\n------\n\n[1]  http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdegraphics/kghostview/ps.c\n\n[2]  http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenetwork/kpf/src/"
    author: "Jiffy"
  - subject: "Donated for KDE"
    date: 2002-10-10
    body: "I switched from GNOME 2.1 (CVS) to KDE 3.1 (CVS) yesterday and went to the Bank for a 20\u0080 donation today. Thank you for such a cool Desktop Environment. It's the first time that I ever donated for something. Not even GNOME got a penny from me but after I played with KDE I said 'wow' this is worth it."
    author: "Charly"
  - subject: "Re: Donated for KDE"
    date: 2002-10-21
    body: "AWESOME... I think that everyone should follow your examble! Myself included! Ill go to the bank on the 1/11.\n\nAll Thumbs Up Charly!\n\n/kidcat"
    author: "kidcat"
  - subject: "If I install kde 3.0.4 will it erase my stuff"
    date: 2002-10-10
    body: "Sorry if this question is lame, but Iv been using Linux for about 1 week ( when I decided to reformat my windows only hd and install Linux instead ) and have already experienced problems with it. But if I download RPM or try to compile from source, will it be like I just upgraded my KDE or will everything ( desktop backgrounds, themes, configs, programs installation ) be erased ?"
    author: "Gilles Leblanc"
  - subject: "Re: If I install kde 3.0.4 will it erase my stuff"
    date: 2002-10-10
    body: "depends,\n\nuser related configuration stays as is but it may be possible that structures within the configuration itself has changed so you may need to adjust some of your settings. this is only a valid statement if you switch e.g. from kde 3.0.x to 3.1.x. but nothing of your configuration get erased.\n\nprgrams, desktop backgrounds, themes etc. this also depends some themes and backgrounds may be stored in your homedir and some are globally installed."
    author: "Klapper"
  - subject: "Re: If I install kde 3.0.4 will it erase my stuff"
    date: 2002-10-11
    body: "I have actually found that some apps change their config files so the application looks strange or misses features. \n\nWhen I upgraded from CDBakeoven from 1.8.9 to the cvs version in kdeextragear-1 (check it out of you haven't done it already) it refused to use mpg123/ogg123 before I deleted the config file .kde/share/config/<app>.rc. This also applies to Quanta3 and Kate. \n\nThis will not be a problem when upgradeing from 3.0.3 to 3.0.4, but is a good tip if you upgrade to a new major release (usually a bigger main number, y in x.x.y is a small fix and x.y simply y. is a big thing).\n\nAlso, I experienced (fx when recompileing Kde3Beta2 with a new compiler) that the KDE temp files in /tmp (or /var/tmp depending) needs to be deleted when upgradeing."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: If I install kde 3.0.4 will it erase my stuff"
    date: 2002-10-11
    body: "It shouldn't, since it's a bugfix-and-minor-improvements only update. All version bearing the same minor version number (the order is major.minor.bugfix, so we're talking about kde 3.0.x) should be exchangeable. A different story are minor version number changes, e.g when upgrading from 2.1 to 2.2, not all the settings seemed to have made it (though most did). The same is true for major number updates (e.g. 2.2.2 -> 3.0.0).\n\nTalking about programs, all kde programs should reside in /opt/kde or /usr, along with their default settings. Your personal settings are stored in /home/<username>/.kde/share/config . If things go wrong, you will be reverted to the app's default settings. But as I said, a bugfix update should pose no problems at all (of course, mistakes happen).\n\nHaving said that, I would recommend you to (1) install from RPM because it's easier, and (2) wait for two weeks before upgrading. A lot early vendor packages have their glitches, but they are usually sorted out after some time."
    author: "Anno v. Heimburg"
  - subject: "Re: If I install kde 3.0.4 will it erase my stuff"
    date: 2002-10-11
    body: "Thanks for the replies :)"
    author: "Gilles Leblanc"
  - subject: "Mandrake rpms keep original settings ?"
    date: 2002-10-10
    body: "I know this is a Mandrake question, but I hope some Mdk user can answer.\nWhen you upgrade KDE with the binaries provided in kde.org ... do you lose \nyour settings ? (such as Mandrake Menues, Login Manager (KDM) configuration,\netc. ?) I am planning to upgrade the Mandrake 9.0 binaries ...\n\nMany thanks !"
    author: "NewMandrakeUser"
  - subject: "Re: Mandrake rpms keep original settings ?"
    date: 2002-10-10
    body: "I just upgraded using the MDK binaries in Cooker. I used the Upgrade tool and everything installed fine.\n\nWill\n"
    author: "Will"
  - subject: "Re: Mandrake rpms keep original settings ?"
    date: 2002-10-10
    body: "Thank you Will,\n\nsame excellent luck with the 9.0 binaries. I had to use  --nodeps because of a TiMidity++ conflict, but Timidity is still running after the upgrade, it was just the way it was packaged (probably requiring kdemultimedia == 3.0.3)  :-)\n\nAnd Thank You MANDRAKE \n:-)"
    author: "NewMandrakeUser"
  - subject: "Very helpful"
    date: 2002-10-10
    body: "This was very helpful\n  \nI'm a linux sysadmin and my current project is porting KDE to win32 os. I haven't been able to find a HOWTO on compiling the code in Visual Basic yet but when I do I'll release it under the GPL "
    author: "Sir Bard"
  - subject: "Re: Very helpful"
    date: 2002-10-11
    body: "why using kde on win32?\ni dont understand... better running win in vmware under kde ;-)\n\ngunnar"
    author: "gunnar"
  - subject: "Re: Very helpful"
    date: 2002-10-11
    body: "Are you sure you didnt mean compiling the code in Visual C++. \nOr do :\nA : you actually think that you can compile C code in VB\nB : Im actually so dumb I think it can`t be done but it can\nC : you just posted this post to see who would bite"
    author: "Gilles Leblanc"
  - subject: "Re: Very helpful"
    date: 2002-10-21
    body: "check out his comment on \"ill license it GPL\".. this gotta be a bogus!\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Very helpful"
    date: 2002-10-11
    body: "You mean like the folks at kde-cygwin.sf.net ?\nWhy aren't you working with them?\n\nCheers,\nKevin\n"
    author: "Kevin Krammer"
  - subject: "Re: Very helpful"
    date: 2002-10-11
    body: "YHBT. YHL. HAND."
    author: "Frank Becker"
  - subject: "Re: Very helpful"
    date: 2002-10-10
    body: "Great news! Thank you..... I'm a Win32 user and this would be absolutely great!\nMaybe on msdn (http://msdn.microsoft.com/) you can find more help about the KDE/Visual Basic stuff.\n\nHope it helps."
    author: "Bill G."
  - subject: "kde 3.0.4 RPMS for RedHat 7.3"
    date: 2002-10-10
    body: "I've put together some kde 3.0.4 RPMS for RedHat 7.3 (they *may* work on RedHat 7.2, but that is untested).\n\nSee the posting on pclinuxonline.com for details: http://www.pclinuxonline.com/modules.php?name=News&file=article&sid=3541\n\nEven More details are available on my website:\nhttp://www.math.unl.edu/~rdieter/\n\nEnjoy.\n\n -- Rex "
    author: "Rex Dieter"
  - subject: "Thanks"
    date: 2002-10-12
    body: "I installed those on Red Hat 7.2, got it working without too much hassle, and (except a few minor glithces) it works well. Feels quite snappy too; programs start fast etc.\n\t"
    author: "Joni"
  - subject: "Re: Thanks"
    date: 2002-10-12
    body: "If you don't mind, could you elaborate on the \"few minor glitches\" you experienced?  Maybe they're fixable...\n\n-- Rex"
    author: "Rex Dieter "
  - subject: "Re: Thanks"
    date: 2002-10-18
    body: "Well, some problems with the font under the icons on my desktop; I can't set it to bold anymore. And when I change almost any desktop setting, the icons lose their order and revert to some odd default.\n\nAlso, I didn't get any graphic (splash screen, whatever) when KDE's starting, until I updated redhat-logos package to a version from 8.0 (and ignored some minor rpm conflicts to do that)...\n\nNothing that big, and I'm not even sure if the fault is in your packages...\nOverall I'm actually very pleased with this KDE setup.\n\n\nOh, btw: when I start kword as the user I normally use, I get the following:\n\"Mutex destroy failure: Device or resource busy\"\nWith other users it works well. Any ideas? Removing kword's config file under ~/.kde didn't help...\n"
    author: "Joni"
  - subject: "n/m"
    date: 2002-10-18
    body: "Never mind that kword question. I got it working by reading my old dot.kde.org postings. (Seems like I had the same problem before.)"
    author: "Joni"
  - subject: "Re: n/m"
    date: 2003-06-08
    body: "I can't find your old post regarding this error with kword, and now I'm having problems with kword on RH8.0 with the error message:\n\nMutex destroy failure: Device or resource busy\n\nI did a search for Joni and only three articles came up.  None of them had this one that I'm repling to.  I actually found this one with Google.  Just like you, it works fine with the root account. How did you fix this problem, or can you give me the hyperlink to your post/reply?\n\n/mario\n"
    author: "Mario Lombardo"
  - subject: "Re: Thanks"
    date: 2002-10-18
    body: "1.  regarding fonts... Hmm...I'll have to look at this one closer.  I've never seen this, but it may be a rh72+XFree4.1 vs rh73+XFree4.2 thing.\n\n2.  Re: missing splash screen.  Another odd one... my kdebase package has a Requires: for an updated redhat-logos (also in my repository).  It (apt) ought to have upgraded this for you automatically.  ??\n\n-- Rex\n\n"
    author: "Rex Dieter"
  - subject: "Re: Thanks"
    date: 2002-10-18
    body: "Well, I didn't use apt. (I've tried it sometime earlier but have run into problems, IIRC...) \n\nInstead I made a script to get all the rpms and then used 'rpm -Fvh'.\n(Would be easier to get them with wget if you'd have them on ftp server instead of http :)\n\nBut don't worry about it too much, I got it working anyways."
    author: "Joni"
  - subject: "Re: Thanks"
    date: 2002-10-18
    body: "wget works with for http.  I've used it to mirror my site on occasion:\nwget --mirror http://www.math.unl.edu/linux/redhat/apt/7.3/i386/RPMS.kde3\nought to do the trick. (-:\n\nIn the meantime, I'm working on trying to enable ftp access as well... and lining up a mirror or two....\n\nBack to apt... it is a wonderful tool, I highly recommend its use... and periodic\napt-get update && apt-get upgrade\ntasks will always keep your box current.  If you had problems in the past, please try again.  You won't regret it.  And if you *do* experience problems, please don't hesistate to send a gripe or two my way... I'd love to have a chance to fix anything that's broken.\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: Thanks"
    date: 2002-10-26
    body: "Btw (if this still reaches you), do you know how I could fix this?\nIf I try to view any Help whatsoever in KDE, I get the following:\n\n\"An error occured while loading help:/khelpcenter/index.html?anchor=welcome:\n\nCould not start process Unable to create io-slave:\nklauncher said: Error loading 'kio_help'.\"\n\nI've been wondering if there's some \"kdehelp\" RPM package or something missing in my system, but I'm not really sure."
    author: "Joni"
  - subject: "Re: Thanks"
    date: 2002-10-27
    body: "I haven't been able to reproduce this problem.  What version of kdebase do you have installed?  (rpm -q kdebase to find out).  What version of redhat are you using?\n\n-- Rex"
    author: "Rex Dieter "
  - subject: "Re: Thanks"
    date: 2002-10-28
    body: "\"kdebase-3.0.4-0\", i.e. the 3.0.4 package that you built. \nThis box is running (updated) RH 7.2."
    author: "Joni"
  - subject: "Re: Thanks"
    date: 2002-10-28
    body: "I'd recommend you upgrade to the latest version I have available (kdebase-3.0.4-0.73.1.1):\napt-get update\napt-get upgrade\nought to do it.\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: kde 3.0.4 RPMS for RedHat 7.3"
    date: 2002-10-12
    body: "Have you got any bad feedback from these rpms on RH 7.3? If you havn't got many severe complaints I'm going to install your packages... Thanks for providing them!"
    author: "Vilppa"
  - subject: "Re: kde 3.0.4 RPMS for RedHat 7.3"
    date: 2002-10-12
    body: "Nope, no bad feedback... (yet... (-:  ).\n \n-- Rex"
    author: "Rex Dieter "
  - subject: "Re: kde 3.0.4 RPMS for RedHat 7.3"
    date: 2002-10-17
    body: "Is there a standard de-install and install script?\n\nregrads,\nRuud"
    author: "ruud koendering"
  - subject: "Enhancement?"
    date: 2002-10-11
    body: "Is \"enhancement\" Newspeak for \"grave security fix?\""
    author: "Neil Stevens"
  - subject: "Re: Enhancement?"
    date: 2002-10-11
    body: "Last time I checked, there are other changes in there besides those two security-fixes."
    author: "Janne"
  - subject: "Re: Enhancement?"
    date: 2002-10-11
    body: "All bugfixes.\n\nKDE 3.0.4 comes from the frozen KDE_3_0_BRANCH."
    author: "Neil Stevens"
  - subject: "Re: Enhancement?"
    date: 2002-10-11
    body: "Yes, so? It says \"usability and stability enhancements\". Surely bugfixes can be both."
    author: "Janne"
  - subject: "Bug in the KAddressBook"
    date: 2002-10-11
    body: "Can anybody tell me why the bug in the KAddressBook which doesn't save the column width is still not solved in this release?\nAFAIK this problem was figured out in the 3.1.x CVS branch - so is it really so hard to backport this fix to the stable 3.0.x tree?"
    author: "zyzstar"
  - subject: "New bugs"
    date: 2002-10-15
    body: "Using SuSE 7.3 RPMs, I've had less luck w/this release.\n\nI'm getting memory leaks, Ksysguard applet dies, etc.\n\nAlso, the Desktop icon grid has been increased to an unreasonable width and height :(\n\nI'm going back to 3.03"
    author: "antiphon"
---
The KDE Project today
<a href="http://www.kde.org/announcements/announce-3.0.4.html">announced</a>
the release of KDE 3.0.4.
Besides a number of usability and stability enhancements,
it provides two important security
corrections.  The first corrects the file sharing program KPF, which
since KDE 3.0.1 has permitted a remote user to retrieve any file
readable by the user running KPF
(<a href="http://www.kde.org/info/security/advisory-20021008-2.txt">security
advisory</a>).
The second corrects the PostScript<sup>&reg;</sup> / PDF viewer KGhostView, which since KDE 1.1
permits carefully-crafted PostScript and PDF files to execute arbitrary
code
(<a href="http://www.kde.org/info/security/advisory-20021008-1.txt">security
advisory</a>).
If you cannot upgrade to KDE 3.0.4, which is strongly recommended,
you should immediately stop using both KPF and KGhostView.

<!--break-->
