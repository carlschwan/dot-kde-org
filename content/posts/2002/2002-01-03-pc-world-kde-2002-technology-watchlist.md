---
title: "PC World: KDE on the 2002 Technology Watchlist"
date:    2002-01-03
authors:
  - "Dre"
slug:    pc-world-kde-2002-technology-watchlist
comments:
  - subject: "RedHat & KDE"
    date: 2002-01-03
    body: "Does RedHat offer you to install KDE (without Gnome) during installation in the meantime?"
    author: "anonymous"
  - subject: "Re: RedHat & KDE"
    date: 2002-01-03
    body: "yes, but not by default"
    author: "a.c."
  - subject: "Re: RedHat & KDE"
    date: 2002-01-03
    body: "*hint* *hint* to Red Hat."
    author: "ac"
  - subject: "Re: RedHat & KDE"
    date: 2002-01-04
    body: "That's wrong. If you choose the Workstation type of install (the most automated) you must select between three groups of packages: Games, KDE and GNOME.\n\nIf you select both KDE and GNOME, you'll be asked for which is the default later on the install.\n\nSo, saying either GNOME or KDE is the default Red Hat desktop is BS."
    author: "Carg"
  - subject: "Re: RedHat & KDE"
    date: 2002-01-05
    body: "You needn't to install GNOME, but you have to install gtk+, anyway."
    author: "Rumcajs"
  - subject: "Re: RedHat & KDE"
    date: 2002-01-05
    body: "Yes, and your point is?\n\nGTK+ is a very small package (< 1Mb). Mandrake uses it, Conectiva uses, everybody does.\n\nThe only distro I know using Qt is SuSE, and I was not pleased with the results (YasT2 is way too slow). Maybe it was not Qt's fault, but writing a simple tool using a small library is not enough reason to put Red Hat down.\n\nBtw, Red Hat has split KDE in their devel branch, like Debian and Conectiva do already. That's nice."
    author: "Carg"
  - subject: "KDE major coming problems"
    date: 2002-01-03
    body: "Hmm, all very nice, shame RC1 has been delayed because of the *still* unfixed xrender problems and massive performance issues. Is this KDE's architecture starting to show cracks?"
    author: "Natra"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: "Shame? Delayed to massive performance issues? What drugs are you smoking?"
    author: "someone"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: "Crack ;-)"
    author: "BB"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: "Saying Natra is taking drugs will *not* make the problem go away. Just because you don't like his opinion *doesn't* mean he's wrong. Everybody should just wake up and smell the coffee: the emperor has no clothes.\n\nKDE has become too complicated for its developers. Take the new so-called \"printing architecture\". It *doesn't* have printer drivers, let alone an architecture. But it gets added along with the rest of the *non-working* bloat and non-working features. Think Java support. Think Netscape plugins. Think Kicker applets. Think Gtk style engine. Yeah, right.\n\nKDE has become a buzzword-compliant technology demo for Qt, users be damned. And once TrollTech pulls the plug on \"free\" Qt, the party will be *over*. Face it, KDE is falling apart.\n\n(Hopefully, I this post covers whatever rubbish Natra would have felt like adding and he won't post again. We have enough trolls here. How did I do, how did I do?)"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: "You seem to have problems with percepting reality too, are you using drugs ;-)?\n\nWhere is your proof for your \"too complicated\" claim? If you don't understand what kind of printing support was added, visit http://printing.kde.org. Java, Netscape plugins and so on work fine for most users, at least those able to read docs and install right java version, jsse and lesstif. GPL is not \"free\" for you? TrollTech cannot \"pull plug\" on this version. \n\n> We have enough trolls here. How did I do, how did I do?\n\nEnter yourself to the list of dot.kde.org trolls."
    author: "someone"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: "what are you talking about? kde is in no way too complex for its developers. of course there are some areas that get less attention but that's just because not much people get paid to work full-time on kde and the others are doing it for free!! I've been amazed at the great progress that KDE has made the last year and I have no doubt this trend will continue. as for the things you mention, I don't really care about gtk stuff, as many kde people probably do. what do we need gtk for? java works great for me, perhaps you don't know how to configure it properly? what do you mean by kicker applets? I have them. I have to admit I would like to have working netscape plugins though."
    author: "mark dufour"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: ">Saying Natra is taking drugs will *not* make the problem go away. Just because you don't like his opinion *doesn't* mean he's wrong. Everybody should just wake up and smell the coffee: the emperor has no clothes.\n\nNo, he is wrong.  KDE performs fabulously for what it does, and it does a LOT.  We never said that KDE is great for systems with less than 64mb of ram.\n\n\n> KDE has become too complicated for its developers. Take the new so-called \"printing architecture\". It *doesn't* have printer drivers, let alone an architecture. But it gets added along with the rest of the *non-working* bloat and non-working features. Think Java support. Think Netscape plugins. Think Kicker applets. Think Gtk style engine. Yeah, right.\n\na) The printing architecture works well.  It uses cups or lp for the printing backend, so it doesn't need drivers, it conforms to standards.\n\nb) Java works very well.  Hasn't actually changed for a while anyway\n\nc) Netscape plugins work fine for me.  I have no problems with flash, other than the fact that flash in itself sucks.  There was a while that they crashed frequently, but that appears fixed now.\n\nd) I didn't know ANYONE who said there was a problem with kicker applet\n\ne) Gtk Styles work fine in KDE2, they don't exist in kde3 yet though.  BTW, in KDE2, Gtk styles work faster in Qt than in Gtk.\n\n> KDE has become a buzzword-compliant technology demo for Qt, users be damned. And once TrollTech pulls the plug on \"free\" Qt, the party will be *over*. Face it, KDE is falling apart.\n\nTrollTech cannot pull the plug in Free Qt, it's just not permitted by the laws of time and space.  It is GPLed.  It will never go away.  If they stop developing it, one thing will happen: the KDE developers will maintain.  One thing might happen: Qt might be released under the BSD license (See the Free Qt Foundation).\n\n\n> (Hopefully, I this post covers whatever rubbish Natra would have felt like adding and he won't post again. We have enough trolls here. How did I do, how did I do?)\n\nAs a troll, you did fantastically.  RMS would be proud."
    author: "Charles Samuels"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-03
    body: "Charles, I am starting to think that you have worked too much on Noatun and have been neglecting your sense of humor. :-) This post was a parody of the troll posts we often see on the Dot. That is what I tried to explain in the end. I obviously should have been clearer.\n\nKDE works fine here. It was a joke aimed at the expense of trolls like Natra. I will refrain my impulses in the future."
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-04
    body: "I'm sorry to hear that. I thought that your's was actually funnier than many like the worm's (wiggle). Some others just need to chill a little. and yes, I did notice the last line and I'm sure that many others did as well."
    author: "a.c."
  - subject: "Re: KDE major coming problems"
    date: 2002-01-04
    body: "Well I thought it was funny :)"
    author: "cbcbcb"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-04
    body: "Samuels, if you have any reason to believe RMS has ever been a troll I beg you to share it with the rest of us. Please also tell me your definition of \"troll\", if you think that would help.\n\nTo me, he's just a guy who quit his MIT job to develop a FREE system.\n\nI also think Qt wouldn't be free today if it hadn't been for the FSF."
    author: "Carg"
  - subject: "Re: Definition of a Troll"
    date: 2002-01-09
    body: "Emotional hippy hiding under a bridge attempting to redefine the word \"Free\"..."
    author: "EMM"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-04
    body: "> (Hopefully, I this post covers whatever rubbish Natra would have felt like \n> adding and he won't post again. We have enough trolls here. How did I do, how \n> did I do?)\n\n9.7 for quality (of trolling)\n2.8 for presentation.\n\nIt's the judges recommendation that you mark your future trollings more clearly and specifically in advance. Furthermore the judges would have prefered it if you had demonstrated your positive kde-promoting abilities as well for contrast."
    author: "Matthijs Sypkens Smit"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-16
    body: "\" KDE has become a buzzword-compliant technology demo for Qt, users be damned. And once TrollTech pulls the plug on \"free\" Qt, the party will be *over*. Face it, KDE is falling apart.\"\n\nThere are 2 fundimental flaws in your arguement.\n\n1. Trolltech/Qt relies as much (if not more than) on KDE than KDE relies on Trolltech, if it wasn't for KDE then it is quite likely that Trolltech/Qt would have collapsed in the big dot.com bust or earlier, Trolltech therefore has quite an intereast in continuing to support QT and therefor KDE.\n\n2. QT is released under GPL and one of the basic advantages of GPL'd code is that even if the organisation that created it disappears or stops supporting it then progress can still be made, sure its great to have Trolltech spearheading QT's development but if Trolltech were to stop supporting the GPL version of QT then there would be plenty of developers willing to jump in and lend a hand."
    author: "Malcolm Davis"
  - subject: "Re: KDE major coming problems"
    date: 2002-01-04
    body: "> Is this KDE's architecture starting to show cracks?\n\nNo."
    author: "Joe"
  - subject: "Misinterpretation..."
    date: 2002-01-03
    body: "I think the above post is a parody...He even mentions\nthat it's the RUBBISH the other guy WOULD have posted.\nSo let's all take a deep breath and enjoy KDE."
    author: "Adam"
  - subject: "KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-04
    body: "I note that the article mentions StarOffice.  So much for KOffice being able to make StarOffice moot, let alone MS Office.\n\nThe one and only advantage StarOffice has over other *nix office suites is its ability to reliably read **and write** MS Office files.\n\nIt's such a huge advantage people are still willing to use StarOffice 5.x, even though it forces you to use its own massively outdated GUI desktop that is clunky and awful even by GNOME standards.  That people in droves are willing to abandon KDE (or GNOME) and enter the horrible world of StarOffice 5.x, and that the looming arrival of StarOffice 6 is producing near-maniacal levels of anticipation, should clue KDE developers in to the overwhelming importance of developing MS Office file filters for KOffice.\n\nNo other feature in KOffice, indeed, no other feature in KDE, is more important than getting extensive and highly compatible and realiable MS Office file filters for KOffice.   None!   KDE has **thousands** of developers.  They need to make this the top priority and get it done yesterday.  Nothing will do more to help KDE, and *nix in general.\n\nIf you are able to tell Windows users that free software is available that will let them use their current MS Office files, reliably, real-world; and communicate seamlessly with all others still stuck in Microsoft-land, you will break the SINGLE BIGGEST BARRIER people have to leaving Microsoft's clutches."
    author: "Leo"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-04
    body: "> The one and only advantage StarOffice has over other *nix office suites is its ability to reliably read **and write** MS Office files.\n\nAs one program is enough to disprove this argument: ApplixWare Office.\n\n> KDE has **thousands** of developers.\n\nWhere are they? Please count in kde-cvs archive how many has submitted something during last 6 months."
    author: "someone"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-04
    body: "but still! .. the filther is needed"
    author: "Andreas"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-07
    body: ">> KDE has **thousands** of developers.\n> Where are they? Please count in kde-cvs archive how many has submitted something during last 6 months.\n\nDon't forget to count in archives beside kde-cvs, like on SF or in individual projects.\nDon't forget people who cannot or don't have to commit every single day.\nDon't forget to look on appsy too, watching out some new projects which arrive regularly.\nIMO you'll end up with a number equal or higher than the above mentioned one."
    author: "josef"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-08
    body: "So, how many hundred are capable and willing to write MS Office filters?"
    author: "someone"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-04
    body: ">\nNo other feature in KOffice, indeed, no other feature in KDE, is more important than getting extensive and highly compatible and realiable MS Office file filters for KOffice. None! \n<\n\nWrong. I'd have to say that the window manager, Konqueror's HTTP KIO slave, and Kicker are all more important then Office filters :-)\n\nHmm, you probably meant to say \"most important feature to add\". Well, again, I'll disagree here. Yes, they're important, but frankly, it's not that exciting. The RTF filter is more important because any word processor can reliably export RTF, while only Microsoft Office can reliably export it's own format. Other word processors can come close, but it'll never be compat with the most recent version of Microsoft's little undocumented moving-target file format.\n\nThe second most important filter is StarOffice, because StarOffice has very good exporting and importing facilities, and because it's format is really open,\nrather then just sorta open like RTF or not open at all like MS Office.\n\n>\nThey need to make this the top priority and get it done yesterday.\n<\n\nUmm, don't you think this is a little arrogant of you, to assert that you are better and in more of a position to prioritize the KDE developers' time then they are?\n\nOn a final note, I like StarOffice. I'd rather use KOffice, cause of the integration with various KDE technologies I like (I just looooove KDE's file dialog, and previewing in Konqi would be handy), but StarOffice works better then KOffice at the moment, at least the latest stable KOffice. I even use the StarOffice 5 you so despise, cause it's GUI isn't _that_ bad, if you only do the tasks I do and ignore the parts that suck (that is, if your needs basically come to typing w/ basic formatting, tables, embedded images, columns, and the very occasional spreadsheet)."
    author: "Carbon"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-05
    body: "I agree with Carbon. I've played around with StarOffice 6 and it is pretty nifty, despite the slow boot time. The main reason I wouldn't use KOffice is because it does not underline misspelled words. That, text formatting, and WYSIWYG (which I understand is coming or in the newer versions of KOffice) is the only reason I use a word processor to begin with. \n\nAlso, I think instead of reinventing the wheel, perhaps stripping the conversion part of OpenOffice out and into a console program, so KOffice could use it. Then create a filter for KOffice for [Star|Open]Office's XML doc format. There should be a conversion from that format anyways."
    author: "Eean"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-05
    body: "SO 5.2 is outdated true but you should try Staroffice 6 Beta before you say anything against it! It is for me the best Office application at the moment that is avail. Nothing against KOffice but it is not really fished yet."
    author: "tom"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-05
    body: "SO 5.2 is outdated true but you should try Staroffice 6 Beta before you say anything against it! It is for me the best Office application at the moment."
    author: "tom"
  - subject: "Re: KOffice dissed!  MS Office filters badly needed!"
    date: 2002-01-10
    body: ">If you are able to tell Windows users that free software \n>is available that will let them use their current MS Office \n>files, reliably, real-world; and communicate seamlessly with \n>all others still stuck in Microsoft-land, you will break the \n>SINGLE BIGGEST BARRIER people have to leaving Microsoft's \n>clutches.\n\nIt's already broken, then, because OpenOffice is here now.  I use it every day for business and a number of companies I know well are conducting trials.  It's dual licensed much the same way Qt is (GPL and Some Other Random License I Don't Use), so it's certainly free enough for me.\n\nI'd certainly like to see KOffice get the filters going because KWord makes Openwriter look like a steaming hunk of crap performance wise -- just as Gnumeric makes Opencalc look the same -- but using Openoffice is a much lesser pain than, say, Netscape 4.x was for the last couple years before Konqueror matured.  (Come to think of it, Mozilla didn't kill Konqueror either despite the hype.)  \n\nTell you the truth, I'd just settle for Kivio being able to import Visio documents because they're the last old documents I'm unable to read from my Windows days (well, a couple Adobe Indesign files too, but I have EPS'es of those.)  I don't HAVE to use one and only one office suite; I actually use Gnumeric for all my spreadsheets except the ones I have to send to Windows users.  Get the functionality built and THEN worry about integration, at least in this one case where we're so close.\n\nOpenoffice is totally ready for daily use in my view, but rather than hurting KOffice, it will just provide a healthy sense of competition and filter code (not so sure about what it might do to others like Hancom.)  Openoffice is a Good Thing(tm)."
    author: "raindog"
  - subject: "Recognition and Future"
    date: 2002-01-04
    body: "It's nice that people are finally coming to understand how useful\nKDE has become. It's mature, stable and feature-rich.\n\nHowever, there are organizational and technical issues that should be addressed:\n * We need more resources for development, support and advertising. What\ncomes to my mind would be getting companies which make money off linux to\nemploy KDE developers first. Second, more people who will actually promote KDE,\ndeploy KDE at organizations, etc. Also encourage HCI/graphics/etc. researchers\nto use KDE.\n * KDE has some system and application level known performance problems. I\nbelieve there might be API and usability issues as well. Co-ordinated work\nis required to make KDE sharper in those respects.\n * To become the one true UNIX desktop, KDE needs the following:\n   - more language bindings --> more innovative applications, KDE3 is\ncoming around to that. (C bindings might be a good place to start in that direction)\n   - better system integration --> What mandrake is doing, but I'd imagine all\nviable platforms (debian gnu, freebsd, etc.) being supported equally well. Doing\notherwise would do injustice to those great platforms!\n\nFor instance, ppl were complaining that by default you can't install kde-only on\nredhat. Is that a problem? Probably no, but on Debian system I use I also feel\nthat there is less support for KDE than would be desirable. \n\nI should explain what I mean by innovative applications. KDE2 is already doing\nat least as well as certain competition is doing, such as GUIS of proprietary OS's.\nThe next step is to surpass them in all respects. Design-wise, functionality-wise\nand technology-wise.\n\nAnd no, I don't mean to troll. :)"
    author: "exa"
  - subject: "Re: Recognition and Future"
    date: 2002-01-04
    body: "> And no, I don't mean to troll. :)\n\nHmmm... Concise, to the point, yet clear and without ambiguity. I like it. I will have to remember that one. :-)"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "KDE in the News"
    date: 2002-01-04
    body: "I think that it is about time.  I have always been amazed at the work of the KDE development team.  Their past accomplishements, current projects, and future plans place it far and away ahead of all other GUI desktop environments, including those of MS whose new Luna interface actually steals several features the KDE desktop have had for over a year.  \n\nAs for the distro consolitation I think it is necessary, last time I looked there where over 190 different distros available, most of which are based on a couple of larger distro builds like Redhat, SuSE, and Mandrake.  However I do not like to see Redhat gobble up everything and get the associated press when SuSE is IMHO a far better, more complete, and freindlier distro for both new and experienced Linux users.  \n\nKDE rock on!!!"
    author: "D Bo"
  - subject: "Re: KDE in the News"
    date: 2002-01-04
    body: "I wouldn't take PC World too seriously. We already knew KDE was awesome, and some marketing magazine (and that's basically what it is, all the magazines that are the exclusive domain of hackers and proto-hackers are already completely online) says isn't that important, or that likely to become true.\n\nBesides, Debian rules more then SuSE ;-D </deliberate-holy-war-flamebait>"
    author: "Carbon"
  - subject: "Re: KDE in the News"
    date: 2002-01-04
    body: "Yah, We know that but lets think about the IT manager or CIO who uses these very reviews and opinions from these types periodicals(PC World, E-Whatever, and God knows what else) to base future purchase and implementation strategies on.\n\nSorry about missing Debian they definitely rule, next to everyone else except SuSE( ;b - had to say it), what other Distro has it's own packaging system?"
    author: "D Bo"
  - subject: "Re: KDE in the News"
    date: 2002-01-05
    body: "You just said:\n| what other Distro has it's own packaging system?\n\nTo which I have to reply:\n\nWhat other distro allows me to type \"make install\" and it'll suck down the source for me, apply any necessary patches, compile the app, settle any dependencies for me, and register the installation?\n\nOk, so maybe it's not a \"distro\", but it's FreeBSD.  And, it *does* have it's own packaging system.  It's called \"Packages\".  Hehehe."
    author: "a.c."
  - subject: "Re: KDE in the News"
    date: 2002-01-05
    body: "The Fink \"distribution\" (http://fink.sourceforge.net) for MacOS X does that, except it generates debian packages, instead of FreeBSD's anemic tarball packaging.  =)"
    author: "Ranger Rick"
  - subject: "Re: KDE in the News"
    date: 2002-01-05
    body: "You could have a look at Gentoo Linux (www.gentoo.org).\nIt's basically a ports-based Linux distro, but it's ports system\n(called portage) seems to have some new features compared to the\noriginal BSD ports system (not 100% sure about that, as I haven't\nused *BSD extensively yet).\nGentoo is still in the pre-1.0 phase and right now there are no\nbinary packages... which is why I couldn't play around with it\ntoo much yet, since my current PC is slow and doesn't have enough\nfree space on the HD to build XFree... but from what I've seen\nso far it looks really promising and might be the first distro to\npull me away from my beloved Slackware (and yes, I've tried Debian,\nand from all distros I've tried it got closest to Slack, but still\ncouldn't beat it)."
    author: "Marvin"
  - subject: "gentoo portage seems to be great"
    date: 2002-01-06
    body: "everything is in the topic ;)\n\nhttp://www.gentoo.org"
    author: "ced"
---
<a href="http://www.pcworld.com/">PC World.com</a> is running a
<a href="http://www.pcworld.com/news/article/0,aid,77604,00.asp">story</a>
about the technologies to watch for 2002.  Besides predicting consolidation in the Linux distros (particularly that <a href="http://www.redhat.com/">RedHat</a>
will swallow up another distro), the article opines that
&quot;<em>[t]he arrival of Sun Microsystem's StarOffice 6.0 and continued
work on the KDE desktop graphical interface could make Linux a more
viable alternative on the desktop</em>&quot;.  Nice to see KDE get the
recognition it deserves in the mainstream press.

<!--break-->
