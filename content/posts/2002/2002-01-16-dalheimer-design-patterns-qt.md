---
title: "Dalheimer: Design Patterns in Qt"
date:    2002-01-16
authors:
  - "Dre"
slug:    dalheimer-design-patterns-qt
comments:
  - subject: "Konqueror error :-("
    date: 2002-01-16
    body: "The \"Design Patterns in Qt\" link doesn't work in Konqi :-(\nUsing KDE 2.2.2 and now switching to Mozilla to see this link...\n\nNope, this isn't a bugreport. Those should go on bugs.kde.org ;-)"
    author: "Storm"
  - subject: "Re: Konqueror error :-("
    date: 2002-01-16
    body: "Works with KDE 2.2.2 here."
    author: "ac"
  - subject: "Re: Konqueror error :-("
    date: 2002-01-16
    body: "Yes, works perfectly here as well."
    author: "Jon"
  - subject: "Re: Konqueror error :-("
    date: 2002-01-16
    body: "Hmmm... strange\nmaybe because I use the ISP proxy?\nhopefully it works in kde 3 for me"
    author: "Storm"
  - subject: "Re: Konqueror error :-("
    date: 2002-01-17
    body: "Oh it does work... I had to enable java first. I've got it disabled by default."
    author: "Gerben"
  - subject: "Re: Konqueror error :-("
    date: 2002-01-23
    body: "OK, I found the problem. It was RedHat's fault...\nI updated my system to KDE 2.2.2 but for a strange reason RedHat didn't released all the KDE 2.2.2 rpm's at once.\nkdebase-2.2.1-1 (and others) were still on my system\n\nSo my system was running on KDE 2.2.1,5 :-)\n\nI'm now very happy with KDE, runs faster and constains less bugs..."
    author: "Storm"
  - subject: "[NOT RELATED]"
    date: 2002-01-16
    body: "When will the next kde3 beta come? and stable?"
    author: "Andreas"
  - subject: "Re: [NOT RELATED]"
    date: 2002-01-16
    body: "http://developer.kde.org/development-versions/kde-3.0-release-plan.html\n+ 1/2/3 weeks ;)\n\n\nhave fun\nHAL"
    author: "HAL"
  - subject: "What's most impressing about this..."
    date: 2002-01-16
    body: "...is that Qt became a great symbiosis of design, for example by employing several design patterns, and functionality. Functionality in the sense that it still works on a wide range of platforms with equivalent power.\n\nI've seen quite a number of projects which either became a pile of hacks (because it became apparent that certain compilers required certain workarounds. The business approach, so to say.) or a fairly well-designed but virtually unusuable collection of design patterns (because the authors refused to honour the importance of certain projects appropriately and instead insisted on clean code. You might call it the academic extreme).\n\nAs far as I've been able to figure out, there is of course a rather large number of hacks and workarounds in Qt as well, but those are very well hidden from the clients, so there's no need for guilty conscience or that \"dirty\" feeling. ;-)"
    author: "Frerich Raabe"
---
<a href="http://www.kde.org/people/kalle.html">Matthias Kalle Dalheimer</a>,
who besides being one of the initial KDE developers, author of a famous
c't article which helped galvanize KDE early in its life, author of an
important series of early KDE articles
(<a href="http://www.linux-magazin.de/ausgabe/1997/11/KDE/kde1.html">1</a>,
<a href="http://www.linux-magazin.de/ausgabe/1997/12/KDE/kde2.html">2</a>,
<a href="http://www.linux-magazin.de/ausgabe/1998/01/KDE/kde3.html">3</a>)
as well as the author of
<a href="http://www.oreillynet.com/cs/catalog/view/au/588?x-t=book.view">several
books on Qt development</a> published by
<a href="http://www.oreilly.com/">O'Reilly</a><sup>&reg;</sup>, has recently
authored an article entitled
<a href="http://www.onlamp.com/pub/a/onlamp/2002/01/10/designqt.html">Design
Patterns in Qt</a>.  In it he explores how the
concepts from the "Gang of Four" book
<a href="http://hillside.net/patterns/DPBook/DPBook.html">Design Patterns</a>
are used in Qt programming, focusing specifically on Qt's signal-slot
architecture.  Enjoy!


<!--break-->
