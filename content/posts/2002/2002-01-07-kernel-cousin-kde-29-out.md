---
title: "Kernel Cousin KDE #29 Is Out"
date:    2002-01-07
authors:
  - "rkaper"
slug:    kernel-cousin-kde-29-out
comments:
  - subject: "Icon Loading is slow..."
    date: 2002-01-07
    body: "Considerably when one right-click on a folder and properties then click on the 'folder' to change the icon. the Loading seems to annoy the user."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Icon Loading is slow..."
    date: 2002-01-07
    body: "bugs.kde.org"
    author: "Justin"
  - subject: "Re: Icon Loading is slow..."
    date: 2002-01-08
    body: "No wonder, then it loads all app icons you have. But how often do you change icons?"
    author: "someone"
  - subject: "KDE Look & feel thoughts..."
    date: 2002-01-07
    body: ">In particular, there has been a surge of eye candy for KDE2 in the last few months including the alternative icon set iKons 0.5, Mosfet's Pixie Plus image viewer and the fledgling KDE Passion project not to mention updates and forks of several native widget themes. There was quite a wait for these flourishes to appear after the release of KDE2, which was understandable given the large changes involved and the initially smaller user base. With KDE3 being more evolutionary than revolutionary (though the style API has changed considerably for native widget themes) there hopefully will be a much shorter wait for third party eye candy this time around.<\n\nWhere do we go guys? Why is so that great majority of KDE users are  obsessed by WinXP & MacOSX look and feel?\n\nI am really surprised when I see iKons on almost every KDE screenshot published on the web. iKons folders are OK but the rest is crap. Devices are bad rip of winXP devices (just uglier), mime icons are ugly, and action icons are just overpainted default action icons. While Gnome 2.0 goes KDE way (clean & beautiful icons) abandoning too dark icons, KDE seems to go old Gnome way (awful colors in iKons). What the hell is wrong with tackat's icons. We have to be proud to have such a set of clean (especially mimetypes) icons. Here is my recommendation to tackat: I've noticed some rips of winXP icons in default icon set (personalisation, power control & sound modules in Kcontrol have winXP look & feel). Please don't do that, we need to have look & feel different from Gnome, WinXP & MacOSX. This is KDE. \niKons doesn't deserve to be mentioned on Kernel Cousin KDE despite its popularity is enormous. It would be nice to have an alternative icon theme for KDE but alternative should mean as good as default ones. Unfortunatelly, there are no alternative icon themes on kde-look.org.\n\nWhat is 'fledgling KDE Passion project'? Nothing. A web site with a wishlist which sounds like this:\n' I want to interlace the mood of the G4 styles along with this simple interface I cannot yet explain, to create a solid foundation of quality and ease (in every sense) that does not yet exist.'\nWauuu.... I never heard about G4 style (the author probably means Aqua style from MacOSX). But the funny thing is that he wants to 'interlace' it 'with this simple interface he cannot yet explain'. Great project which deserves to be mentioned on Kernel Cousin KDE. The only problem is that the author cannot explain his own project. \nI am sorry, but I think that the person who posted it as 'news' can not explain its own news.\n\nIMHO there is some wallpapers published on kde-look.org which deserves place in KDE3 (in kdeart module):\n - daCode, Linux Addict, Xtux, Ayo73 by ayo73\n - KDE3green by qwertz\n - The K Desktop by fatal\n - Visualize your desktop by malloc\n - Cyndi by retox\n - Coffegirl by basse\n - Tuxperience by marccd\n\nbecause wallpaer-section in KDE 2xxx hasn't been changes for a long time."
    author: "falco"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "Mod this up! :-)\n\nI could not agree more. The standard KDE icons are beautiful. Even the small icons are easy to discriminate. I *hate* the \"photorealistic\" icons of gnome and OSX. Having photorealistic icons that are scaled down to 16*16 pixels is just plain dumb.\n\nAnd most standard KDE icons have a \"KDE\" look about them. This is very valuable, and we should not lose this. \n\nregards,\n\n     Androgynous Howard"
    author: "Androgynous Howard"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "I like the iKons icons. They are clear and nice. And if everybody has them on screenshots, the users like it.\nI think iKons can go with kde3.\nOk, not everything is good, but some of the default icons are also bad. (knotes and addressbook for example)\n\nI think iKons are a good base for the kde artists. The can put there work together.\nThere is also an other icon theme (http://www.kde-look.org/content/preview.php?file=658-1.png) named Anti-Icons.\nThis is also nice and clear and also has a good score on kde-look.org.\n\n\nI think we have some really good icon sets for kde now. They could be merged together and we have another icon set for the kde-default.\nMaybe you could include more than one icon theme in kde3. Why not 3 or 4.\n\nhave fun\nHAL"
    author: "hal"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "> I think iKons can go with kde3.\n\niKons as a whole still got a lot of copyright-issues (using parts of MS-artwork).\nTherefore it can't go with kde3. although I'd like to see some of the original iKons in kdelibs/kdeartwork. \n\n> Why not 3 or 4.\n\nThat's actually what I'd like to see. Having \"Slick\", \"Free Icons\" and \"Anti-Icons\" in kde 3 would be a nice thing."
    author: "Tackat"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "Ok the copyright thing is bad, thats a problem.\nIt would be bad if the hole kdeartwork can't be used because of copyright things.\nThere are enough problems with Krayon now.\nFor the people from Germany:\nhttp://www.heise.de/newsticker/data/odi-08.01.02-001/\n\n\nhave fun\nHAL"
    author: "hal"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-12
    body: "What would be great is the folder icons from iKons with the mime-type icons from Anti-Icons (taken off from KDE-Look for a while until the author polishes them up). Anti in particular has great legible icons"
    author: "Bryan Feeney"
  - subject: "Klassic icons"
    date: 2002-01-07
    body: "how about Klassic icon theme?\n\nhttp://www.kde-look.org/content/show.php?content=634"
    author: "Asif Ali Rizwaan"
  - subject: "Re: Klassic icons"
    date: 2002-01-08
    body: "it sucks because it hase these yellow icons.\nToo much windows!"
    author: "yves"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "> I've noticed some rips of winXP icons in default icon set \n> (personalisation, power control & sound modules in Kcontrol \n> have winXP look & feel). Please don't do that, we need to \n> have look & feel different from Gnome, WinXP & MacOSX. This is KDE. \n\nThere are some things in the WinXP-icons which would benefit KDE-icons a lot (I already wrote about that on kde-artist).On the recent icon-additions which are quite close to the XP-look & feel: blame Lenny on that ;-) *grin* *grin* *duck* \nActually I like Lenny's icons a lot. Still we got to preserve our own look and feel.\n\n> IMHO there is some wallpapers published on kde-look.org \n> which deserves place in KDE3 (in kdeart module)\n\nI already did put some of very good wallpapers from kde-look.org into kdeartwork some weeks ago. I'll add some further ones during the following days.\nThanks for your nice list. I'll have a look at these images.\n\nGreetings,\nTackat"
    author: "Tackat"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: ">iKons folders are OK but the rest is crap. Devices are bad rip of winXP devices (just uglier), mime icons are ugly, and action icons are just overpainted default action icons.<\n\nI remember, it was a few months ago, I warned the author of iKons on kde-artist mailing list that some of his icons had been copied from MS WinXP. I like his folder-icons very much, and it would be better that he packs his folder-icons with default kde icons (till he creates his own devices & mimetype icons).\nSome of the greatest painters started their careers imitating other peoples work, so ripping is also some kind of learning.\nProblem with iKons devices-icons is (if we forget that they are copies of winXP icons) that the author chooses dark-grey colour (not very much clean) sometimes in combination with not-very-well-chosen blue colour. One example is his Konsole icon which is not elegant and clean when you compare it with KDE's deafult Konsole icon.\nIIRC the author of iKons hasn't created many mimetypes icons, but those he has in his icon set have MS Word's, Excel's etc. wellknown signs. \n\n>While Gnome 2.0 goes KDE way (clean & beautiful icons) abandoning too dark icons...<\n\nYes, Gnome 2.0 has a nice icon set, very clean.\n\n>We have to be proud to have such a set of clean (especially mimetypes) icons.<\n\nAgree :), but wee also need cleanup, and that means:\n1) not too many semi-transparent shadows in mimeicons, \n2) application icons have very different sizes,\n3) we need many new application icons,\n4) we need some 32x32 icons in koffice,\n5) we still don't have some icons for Konqeror toolbars,\n6) more different types of folder-icons that can go together with our default mimetypes icons (and yes iKons folders, of course).\n7) 64x64 icons are not finished yet, so when you choose 64x64 icons for your desktop and file-manager apllication icons get scaled and that's ugly.\n\nA lot of work :)"
    author: "antialias"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-09
    body: "I don't quite understand your complaints - while I think the original icons are great, I think iKons is quite slick too. Granted, it looks a lot like XP, but to me, it also looks a lot like the other KDE icons. \n\n -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2007-04-30
    body: "Hi.I have a problem with my ikons on my desktop. Maybe you can help me.\nAll my ikons on desktop are selected and I can not move this selection. I wonted to copy something and then just this hapend. I tryed to restart but nothing hapend.\nIf somebody of you know how to fix this please give me the answer.\n\nBy MArko"
    author: "Marko"
  - subject: "KDE Look & feel thoughts..."
    date: 2002-01-07
    body: ">In particular, there has been a surge of eye candy for KDE2 in the last few months including the alternative icon set iKons 0.5, Mosfet's Pixie Plus image viewer and the fledgling KDE Passion project not to mention updates and forks of several native widget themes. There was quite a wait for these flourishes to appear after the release of KDE2, which was understandable given the large changes involved and the initially smaller user base. With KDE3 being more evolutionary than revolutionary (though the style API has changed considerably for native widget themes) there hopefully will be a much shorter wait for third party eye candy this time around.<\n\nWhere do we go guys? Why is so that great majority of KDE users are  obsessed by WinXP & MacOSX look and feel?\n\nI am really surprised when I see iKons on almost every KDE screenshot published on the web. iKons folders are OK but the rest is crap. Devices are bad rip of winXP devices (just uglier), mime icons are ugly, and action icons are just overpainted default action icons. While Gnome 2.0 goes KDE way (clean & beautiful icons) abandoning too dark icons, KDE seems to go old Gnome way (awful colors in iKons). What the hell is wrong with tackat's icons. We have to be proud to have such a set of clean (especially mimetypes) icons. Here is my recommendation to tackat: I've noticed some rips of winXP icons in default icon set (personalisation, power control & sound modules in Kcontrol have winXP look & feel). Please don't do that, we need to have look & feel different from Gnome, WinXP & MacOSX. This is KDE. \niKons doesn't deserve to be mentioned on Kernel Cousin KDE despite its popularity is enormous. It would be nice to have an alternative icon theme for KDE but alternative should mean as good as default ones. Unfortunatelly, there are no alternative icon themes on kde-look.org.\n\nWhat is 'fledgling KDE Passion project'? Nothing. A web site with a wishlist which sounds like this:\n' I want to interlace the mood of the G4 styles along with this simple interface I cannot yet explain, to create a solid foundation of quality and ease (in every sense) that does not yet exist.'\nWauuu.... I never heard about G4 style (the author probably means Aqua style from MacOSX). But the funny thing is that he wants to 'interlace' it 'with this simple interface he cannot yet explain'. Great project which deserves to be mentioned on Kernel Cousin KDE. The only problem is that the author cannot explain his own project. \nI am sorry, but I think that the person who posted it as 'news' can not explain its own news.\n\nIMHO there is some wallpapers published on kde-look.org which deserves place in KDE3 (in kdeart module):\n - daCode, Linux Addict, Xtux, Ayo73 by ayo73\n - KDE3green by qwertz\n - The K Desktop by fatal\n - Visualize your desktop by malloc\n - Cyndi by retox\n - Coffegirl by basse\n - Tuxperience by marccd\n\nbecause wallpaer-section in KDE 2xxx hasn't been changes for a long time."
    author: "falco"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "It's beginning to happen things in the themes department, at last. For all of us who aren't that impressed by all aqua-styles, at last some nice styles are beginning to pop up.\n\nKDE still lacks a real wow-theme, though. Something really pretty while still useful, a style that directly and strongly identifies a desktop as KDE, giving it it's own personality without looking like XP or Mac... I'm sure it'll come along. Something that makes Gnome and Mac and whatever have a \"KDE style\" setting in _their_ control centers. ;)\n\nI'd give it a whirl myself but I'm hopeless when it comes to design. ;)"
    author: "Johnny Andersson"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "hey \nWAHT ABOUT TABBED BROWSING IN KONQI ??\ni heard there was a discussion and a patch was up, but will it make it into the default 3.0/rc2/beta2 etc 3.0 release of kde\nwe need TABBED BROWSING\nn what about TABBED listing of files and folders ?"
    author: "crazycrusoe"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "'what about TABBED listing of files and folders ?'\n\nThat would be cool. Something like Galeon preferences tabs."
    author: "anonymous"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "JUST PERFECT TABBED KONQUEROR AND TABBER WERBROWSER JUST THE THING WE NEED IN KDE 3\nAND OFCOURSE DID ANY ONE HAD A LOOK AT THE CRASHHANDLING OF GALEON IT JUST ROCKS, IF ITS IN KDE IT WOULD ROCK, WE REALLY NEED THIS FOR KDE 3\nWE NEED THIS ONE WE NEED THIS ONE :-)\nROCK ONN"
    author: "crazycrusoe"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "I TABBED EV3RYTHING, NO MORE WIND0WS DIAL0GS OR ANYTHING ALL TABS.... OH AND I WANT  A HAX0R TRANSLATION FOR KDE 3 S0 1 KAN BE 1337 T00!!!!!  AND W3 NEED AN A0L CRACKER SO I CAN SEND EMAIL TO MY PRINCIPAL AND TEEEL HIM H0W KOOL KDE IS!!!!!\n\nwhew... i gotta lay off the counterstrike and get back to work.... ;)"
    author: "HaX0r B0y"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "first of all, we do not NEED tabbed browsing. and tabbed browsing will NOT be in KDE3.0 since no new major features are being allowed in at this point. look for it in 3.1, though.\n\ndisclaimer: i do not speak for KDE development whatsoever, i just have a handy crystal ball."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "umm too baaad :-( grrr\nso whence kde 3.1 going to be released with hopefully tabbed browsing ?\nany idea\n\nthanks"
    author: "crazycrusoe"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "When it is ready. And after KDE 3.0, which release date is yet unclear."
    author: "someone"
  - subject: "tabbed browsing"
    date: 2002-01-08
    body: "well, if you want tabbed browsing for khtml right now, I made a pretty small app for myself that does tabbed browsing/tab detaching/session management/history like galeon does. I have a few screenshots here:\n\nhttp://206.228.117.239/ktb/\n\nIf you want to mess with it, email me."
    author: "syn"
  - subject: "tabbed browsing"
    date: 2002-01-08
    body: "well, if you want tabbed browsing for khtml right now, I made a pretty small app for myself that does tabbed browsing/tab detaching/session management/history like galeon does. I have a few screenshots here:\n\nhttp://206.228.117.239/ktb/\n\nIf you want to mess with it, email me (click on my name)"
    author: "syn"
  - subject: "Re: tabbed browsing"
    date: 2002-01-08
    body: "btw, I've only tried it with kde2. it's probably easily portable kde3 with changes to my hacked up qtabwidget/qtabbar/qtab :)"
    author: "syn"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "THE THING WITH KDE ICONS\n\nTHEY DONT USE 2-3 COLORS FOR THE ICONS, THEY JUST USE MANY COLORS, BUT GNOME STICK TO 2-3 COLORS FOR THE ICONS, IT MAKE THEM REAL NEAT, KDE MUST TAKE A SIMILAR STEP IN ICONS 2-3 COLORS JUST NICE, JUST ROCK"
    author: "crazycrusoe"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-07
    body: "Please stop screaming!"
    author: "Per Wigren"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "It is exacty what i think. \n\nWe need some good artists..."
    author: "Johann"
  - subject: "Re: KDE Look & feel thoughts..."
    date: 2002-01-08
    body: "check out \nhttp://www.kde-look.org/content/show.php?content=691\n\nthe dragon theme pack..."
    author: "Juergen Appel"
---
<a href="http://kt.zork.net/kde/">Kernel Cousin KDE</a> is back with <a href="http://kt.zork.net/kde/kde20020104_29.html">Issue 29</a>. 
<a href="mailto:aseigo@olympusproject.org">Aaron J. Seigo</a>, <a href="mailto:cap@capsi.com">Rob Kaper</a> and <a href="tbutler@uninetsolutions.com">Timothy R. Butler</a> once again summarize discussions from the development mailing-lists. This issue covers a new Kate export, KBugBuster, KOffice filters, and Emacs-style keybindings amongst other new goodies!
<!--break-->
