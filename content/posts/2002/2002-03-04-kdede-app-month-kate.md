---
title: "KDE.de App of the Month:  Kate"
date:    2002-03-04
authors:
  - "Dre"
slug:    kdede-app-month-kate
comments:
  - subject: "Small mistake"
    date: 2002-03-04
    body: "The Klaus link shouldn't be http://dot.kde.org/1015234990/staerk_at_kde.org\nYou may want to use mailto: :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Small mistake"
    date: 2002-03-05
    body: "I could have sworn I had fixed that....\n\nThanks."
    author: "Navindra Umanee"
  - subject: "Well Done"
    date: 2002-03-05
    body: "I'm not surprised that Kate gets this award; it's a brilliant editor.  I use it for all my development these days, and if I try other editors, I always end up coming back to Kate."
    author: "Gareth Williams"
  - subject: "Re: Well Done"
    date: 2002-03-05
    body: "Yeah, but the naming of the editor was a close shave, though."
    author: "reihal"
  - subject: "Re: Well Done"
    date: 2002-03-05
    body: "Please tell me that isn't an incredibly off-colour pun.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Jey, I can't read it"
    date: 2002-03-05
    body: "Do I have to learn German too? :-)\n\nI think I forgot everything what German lessons in school (got it 2 years) taught me... :-("
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Jey, I can't read it"
    date: 2002-03-05
    body: "Yeah, I'm also beginning to forget german.\nMaybe I shouldn't write bug reports any more ;-)\n\nGruesse,\nJochen"
    author: "Jochen Puchalla"
  - subject: "Not a patch on SciTE"
    date: 2002-03-05
    body: "I used to use EditPlus2 under Win32 and a range of editors under Linux. Now I have found the best source code editor around which is SciTE (http://www.scintilla.org/SciTE.html), and as a plus it works under both platforms. It supports every programming language I use, has syntax highlighting, can do things such as comment out a block of code in one keystroke... but most importantly has *folding*. Once you start using folding you can never go back to a primitive editor. I absolutely recommend it.\n\nPhillip."
    author: "Phillip"
  - subject: "Re: Not a patch on SciTE"
    date: 2002-03-05
    body: "Thanks for the advertisement :-)\n\nKDevelop has syntax highlighting, it can comment out a block of code. But I don't know what the real use of folding is. To hide your bad code? :-) I don't need it anyway. Aren't there KDevelop plugins available for Java, etc?\n\nI wouldn't call Kate primitive, but it isn't bloated and it is easy to use. I wonder if SciTE understands KParts, I think not. Why need another program if everything I need, is already in KDE? (Except for Quanta+, I hope it becomes a part of the KDE release somewhere in the future...)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Folding when navigating source"
    date: 2002-03-05
    body: "I wasn't singling out Kate as primitive, I meant all editors without folding. Kate seems to strike a good balance for someone doing web pages. It's too bloated for me, who just wants to edit code and doesn't want things built in such as file selector, windows-within-windows, bash prompt etc. On the other hand it doesn't go whole-hog like many IDEs we see.\n\nSciTE is fast, and does the job I want it to well. Agreed it may not use KParts and weigh in at 682k, but there isn't any KDE app that fulfils the same job description. If you do coding in C, Java, PHP, etc (and not HTML) then try using a folding editor and you will suddenly start feeling the way I do :-)\n\nPhillip."
    author: "Phillip"
  - subject: "Re: Not a patch on SciTE"
    date: 2002-03-07
    body: "SciTE just one implementation of the Scintilla engine\nI can assure you that folding is very useful to navigate over large chunks of code without using a full blown IDE.\nIt seams like Scintilla is becoming more and more the text widget of choice for a number of projects, a would very much appreciate the choice of using Scintilla as a kpart.\nBTW it's written in C++."
    author: "Fredrik"
  - subject: "Re: Not a patch on SciTE"
    date: 2002-03-07
    body: "Have had a look at the scintilla sources some time ago (as I was on the trip around to find a better text buffer/widget for kate), but I don't think scintilla's sourcecode is that nice to port to kde (kparts) if you not only want to write a wrapper around it.\nScintilla is a nice text editing widget, for example (as mentioned above) its folding is really great, but on bigger files the current kate part is much faster and its hl is nicer too, as you can easily extend it via xml files ;) Overall, I can see no real advantage in scintilla over the current kate part in kdelibs other than the (nice) folding, which we will have soon (hopefully), too ;)r\nBut more kparts are always useful, than the user have more editorparts to work with ;)"
    author: "Christoph"
  - subject: "Re: Not a patch on SciTE"
    date: 2004-10-14
    body: "> Scintilla ... its folding is really great\n\nScintilla folding is still the best\n\n> which we will have soon (hopefully), too ;)\n\nhopefully yes.. I hope that finally Kate will add working folding \nsupport in next few years.  I was awaiting it latest 2 years (were using xemacs and Scite this time). Finally when Python support was announced I was really liked to try it, but found that folding is completely broken, and according to author comments in bugzilla there are no changes to fix it in near time... :( \n\n\n\n"
    author: "Alex V. Koval "
  - subject: "Re: Not a patch on SciTE"
    date: 2002-04-13
    body: "frankly, SciTE rocks. I have to admit that i have not taken too much time to look into the configurability of Kate, beyond what is possible through the gui.\n\nBUT:\nSciTE allows all kinds of config. through the gui and pulls up its config files for you through some menu options.  It not only has syntax highlighting, it has beutiful & intelligent syntax highlighting.  The colors that it brings by default are great.  You should try this editor. Some more great things about it:\n*folding (already mentioned)\n*shading (in a php file, it'll shade the php parts a light color: helpful)\n*dotted lines show tabbed sections. if you're on an if statement, it'll paint the dotted line blue all the way to the end of that section of code.\n*recognizes errors in html and other common languages (highlights them red)\n*a slew of other minor but important features that make it a joy to work with.\n*it is FAST (waaaay faster than kate)\n\ndont get me wrong, i dont want to knock KDE. i luv it. but i think kate, while flexible, doesnt do as good a job at being a source code editor as Scite. Maybe it would be a good idea to combine efforts w/Scintilla so as not to have to maintain millions of different text widgets.\n\n"
    author: "Pablo Liska"
  - subject: "Re: Not a patch on FTE"
    date: 2002-03-05
    body: "FTE works on windows,DOS,unix terminals,X and KDE.\nhas highlightning for about 50 languages including AREXX and motorola assembler\n\nAnd the F is for Folding.\n\nAs a bonus, I wrote a big chunk of the KDE port (and if you get KFTE, it\nhas a \"nicer than the original\" Qt port, as well."
    author: "Roberto alsina"
  - subject: "Re: Not a patch on SciTE"
    date: 2002-03-05
    body: "Good news: Kate of KDE 3.1 will apparently have folding abilities."
    author: "Someone"
  - subject: "kate  often cannot find string if too many files !"
    date: 2005-10-28
    body: "   when using kate's 'TOOL' menu, there is a item called \"find in files\", which will activate a very good searching dialog, but it often does not work, especially when in a directory whith many\nfiles, such as qt source dir, really upset!\n\n\n I'm using redhat9.0, with kate-2.1, kde-3.1-10"
    author: "gxw9999"
  - subject: "Re: kate  often cannot find string if too many files !"
    date: 2005-10-28
    body: "Your KDE version is pretty old. I suggest upgrading.  If the problem persists please report this at bugs.kde.org.  "
    author: "cm"
  - subject: "Kvim, where are you?"
    date: 2002-03-05
    body: "I am still waiting for kvim, the vim for KDE port. This would bring everything like folding, real syntax highlighting, powerful vi interface and so on.\n\nUnfortunately kvim in kdenonbeta does not even compile and the authors do not answer email. Sucks to have to use gvim."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Kvim, where are you?"
    date: 2002-03-05
    body: "Well you have two options:\na) Take that source code and try to make something out of it.\nb) Keep on complaining on message boards that you have to use gvim.\nBoth might work but I think I know the faster option"
    author: "Company"
  - subject: "Re: Kvim, where are you?"
    date: 2002-03-05
    body: "c) Pay someone to finish the port.  Easier than you might think."
    author: "Anon"
---
<a href="mailto:staerk_at_kde.org">Klaus St&auml;rk</a> informs us that
the <a href="http://www.kde.de/">German KDE website</a> has
<a href="http://www.kde.de/appmonth/2002/kate/index-script.php">announced</a>
the March 2002 <a href="http://www.kde.de/appmonth/">App of the Month</a>:
<a href="http://kate.kde.org/">Kate</a>
(<a href="http://apps.kde.com/">appsy</a>
<a href="http://apps.kde.com/nfinfo.php?id=978">entry</a>).
As usual, the useful (German) review includes a screenshot-laden
<a href="http://www.kde.de/appmonth/2002/kate/beschreibung.php">description</a>
of Kate, as well as a
<a href="http://www.kde.de/appmonth/2002/kate/autor.php">note by</a>,
and an
<a href="http://www.kde.de/appmonth/2002/kate/interview.php">interview
with</a>, its maintainer,
<a href="mailto:cullmann@kde.org">Christoph Cullmann</a>.
Find out more about your favorite editor and the talent behind it.

<!--break-->
