---
title: "Public Release of PerlQt 3.002 including RAD support"
date:    2002-09-17
authors:
  - "ggarand"
slug:    public-release-perlqt-3002-including-rad-support
comments:
  - subject: "shameless cheerleading."
    date: 2002-09-17
    body: "All I've got to say is *wow* this is absolutely terrific.  People have been waiting for a long time for PerlQt 3, and now it comes out with RAD support.  Congratulations to the new PerlQt team!\n\nThis is also the first I've heard of SMOKE. How does it work?  Is it like the libqtc library or something?  \n\nAny word on what Richard Dale is up to these days?"
    author: "Navindra Umanee"
  - subject: "Re: shameless cheerleading."
    date: 2002-09-17
    body: "I'd be interested in more info on Smoke as well. I'm going to releasing new (much improved) version of KJSEmbed soon and this could be used to make it even more powerful.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "What Smoke is (Re: shameless cheerleading.)"
    date: 2002-09-17
    body: "Smoke is a wrapper around Qt (later: KDE), which sort of indexes everything in Qt. All classes, all methods, with all arguments, etc. are put into cross-referencing arrays (for fast lookups). So smokeqt is a meta-definition of qt, where one can read (and call) the whole Qt API by simply reading a few arrays.\n\nThe main purpose of Smoke is to make it easy to write bindings from scripting languages to Qt and KDE - with an emphasis on performance.\n\nAt the moment it's only used by PerlQt, but feel free to look into it for developing your next generation language bindings ;)\n\nNote: most of the design of Smoke is from Ashley Winters, the PerlQt guy.\nI only helped generating the arrays, hacking kalyptus for that. So... in case of further questions on smoke, better ask on the kde-perl@kde.org mailing-list (which is where the PerlQt development happens)."
    author: "David Faure"
  - subject: "Re: What Smoke is (Re: shameless cheerleading.)"
    date: 2002-09-17
    body: "David is right on the money. In fact, Smoke contains two different types of information. \n\nFirst, it has a wrapper around every function in every class -- ala. SIP for PyQt. Static languages (Java, C#, etc) can use that part of PerlQt just like they would use SIP.\n\nIn addition to that, SMOKE contains meta-information... a sort of reflective introspection of the Qt library allowing queries of what functions are available and their arguments and return-types. The PerlQt binding takes full advantage of that information by providing do-what-I-mean function calling and automagic type coercion and multimethod dispatch. It also helps me make up nonsensical buzzwords to make me look smart.\n\nScripting languages can take advantage of that added information to provide neato stuff. Neato stuff is the best.\n\nIn conclusion (my writing teacher loves when I use that), SMOKE is the best thing since libc. Thank you. Please send your checks to me, since I'm homeless and can't work on PerlQt except to respond to e-mails from the local library. Feel free to Cc me on your Smoke questions to the PerlQt mailing-list if you're patient enough to wait a week for my response. Sadly, my Yahoo account would croak if I subscribed to the PerlQt mailing-list but only checked it once a week.\n\nSorry to dump it all on you Germain, but you've done a great job. Congratulations!"
    author: "Ashley Winters"
  - subject: "Re: What Smoke is (Re: shameless cheerleading.)"
    date: 2002-09-18
    body: "Wow! Looks like you guys have done some *amazing* work with SMOKE and PerlQt.  Congratulations!\n\nRegarding the usefullness of smoke for the C# bindings... I'm not sure this is the way to go.  We are in the midst of a major refactor of Qt# with the goal to replace libqtc altogether.  We've written a custom parser in C# that takes the Qt headers and outputs an xml representation of them.  We will then feed this xml into our generator.  We are going to replace libqtc with libqtsharp which will contain all the constructors for the QObjects and then we'll call libqt directly by mangling our function names.  The idea is to reduce our dependencies and tailor our glue code to meet the specific needs of the CLR.\n\nAnyways, Nice Work: Ashley, David, Everyone\n\nAdam Treat"
    author: "Adam Treat"
  - subject: "Re: shameless cheerleading."
    date: 2002-09-17
    body: "Hello Navindra,\nthanks for your kind words :)\n\nSMOKE basically provides a stack of arguments and a way to locate and call easily a given method by name and munged prototype.\nIt's mainly enormous arrays holding all characteristics of methods and pointers to the corresponding interface. Methods are looked up by binary search (hence the programmer must implement some caching...SMOKE does not do everything).\n\nThe main advantage of this strategy is you don't have to rely on manually tweaked interface files, which are the bane of others bindings. \n\nAs for Richard, I really have no idea... he was working with us some monthes ago, and then suddenly disappeared... I've sent some mails to him but never had an answer :-/\n\nG."
    author: "germain"
  - subject: "New Python Libraries"
    date: 2002-09-17
    body: "Hurm.  Python libraries for Qt and KDE would be much appreciated.  Right now, it takes me about 12 hours to compile them (via the sip library), but much more importantly, not only have I never gotten PyKDE working right.  In addition, from browsing mailing lists, many people have to disable core parts of the library to get it to compile.  I also have never gotten any binary installs to work, and from reading about it, it appears that the libraries may be somehow minor version specific (!!).\n\nDisclaimer:  I tilt at the libraries every couple months.  I just subscribed to the PyKDE mailing list to make a serious run at getting everything working.  My current problem is that calls don't seem to line up between what KDE provides and what sip generates.  Odd.\n\n(Incidently, I have an urge to poke at Smoke now and see if I can get it to create a Zend library just to write a couple dozen KDE apps in PHP to tweak the people on the dot who say PHP is just a web scripting language)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: New Python Libraries"
    date: 2002-09-17
    body: "<quote>My current problem is that calls don't seem to line up between what KDE provides and what sip generates. Odd.</quote>\n\nMaybe you missed a few spaces.  Also check for tabs.  Mixing them with spaces is lethal in Python!"
    author: "Anonymous"
  - subject: "Re: New Python Libraries"
    date: 2002-09-17
    body: "Heh.  I'm talking about when I compile PyKDE - the autogenerated .cpp files are making KDE calls that don't exist or are malformed.  Nor am I alone (although this particular problem seems to be unreported).\n\nI'm somewhat familiar with Python (not an expert, but I've written a few programs and I know the language passibly well).  I haven't gotten to the point of even compiling the PyKDE libraries, running into problems in kdecorecmodule.\n\nI'm not sure, but I'm fairly certain that's a key module I can't just skip (a la what most people seem to suggest on the mailing list when the compile hits a problem).  I'm probably going to have to edit the code, but there's something not right in editing autogenerated stubs - I need to see what's generating those incorrect entries.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: New Python Libraries"
    date: 2002-09-18
    body: "\"Nor am I alone (although this particular problem seems to be unreported).\"\n\n... so is the problem reported, or not?\n"
    author: "Richard"
  - subject: "Re: New Python Libraries"
    date: 2002-09-18
    body: "I am not alone in having this sort of error in compilation (the sip generated code not aligning to the Qt API), but this problems with the wrapper for this particular call hasn't appeared on the list (or anywhere else via Google).  The problem of one of the wrappers having this error has been reported, however, and the usual recommendation seems to be to just skip that module.  Since this is a core module, I cannot do that.\n\nAgain, I'm not slamming this project.  I've only tried on SuSE 7.3 and 8.0, and maybe SuSE has directories or library locations different than those that sip assumes.  Or maybe sip is borked.  Or maybe it's my fault somehow.  It's just a fairly common problem according to what I've read, so my only observation is that the compilation process (which, as a side note, does not use autoconf but rather a python script) could be a bit more robust. YMMV.\n\nThe end result looks great - it's just a little rough to get it installed in my experience.  My monitor died yesterday, so I haven't done anything with it today - maybe I'll have it running in the next twenty minutes :).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: New Python Libraries"
    date: 2002-09-18
    body: "Idiot.\n"
    author: "Richard"
  - subject: "Re: New Python Libraries"
    date: 2002-09-17
    body: "Hi,\n\nI'm packing sip/PyQt/PyKDE for SuSE Linux. I've also provided some .src.rpm\non sourceforge. ( http://sourceforge.net/project/showfiles.php?group_id=61057 ). Try to download the .src.rpms and compile them and bombard me with any questions that arise (English or German, what you prefer).\nAlso, feel free to post on the mailinglist at PyKDE@mats.gmd.de for any in-depth-questions.\n\n:)\n\n-Marc"
    author: "Marc Schmitt"
  - subject: "Re: New Python Libraries"
    date: 2002-09-18
    body: "What can I say, PyQt has worked for me for a very long time. I've only just recently (in the last couple of weeks) looked into PyKDE, and it worked out of the box too. Perhaps you're not using the latest Qt or KDE? There are always going to be some minor-version incompatibilities - some library developers out there seem to have no problem with changing APIs between minor versions...\n\nAnd it certainly didn't take _12_ hours to compile on my relatively low-powered Pentium-II 350 .. what are you using, a 386???\n"
    author: "Richard"
  - subject: "Re: New Python Libraries"
    date: 2002-09-18
    body: ":: Perhaps you're not using the latest Qt or KDE?\n\n   3.0.5 and 3.0.3.  SuSE's RPMs.\n\n:: And it certainly didn't take _12_ hours to compile on my relatively low-powered Pentium-II 350 .. what are you using, a 386???\n\n    A dual 700 PIII.  And it certainly took me 12 hours of elapsed clock time - but only several hours (maybe 4 to 5) actual compile time.  I kicked off the build.py, waited several minutes, realized that takes a long time, came back an hour later, kicked off make, came back an hour later, did make install as root, etc, etc.  Then I hit PyKDE, and it didn't work.  Okay, start from scratch... repeat.  Yeah - the actual compiles took a long time, but the compile process involved quite a bit of \"waiting on user\", and \"user sighing, deleting directories, untarring, trying again\" and \"user downloading source rpms and trying those\".  :)\n\n    Again, I'm not slamming the project (although by the antagonistic tone here, I feel like I'm having to defend the fact that it didn't work for me).  I'm sure it buttered your toast - it just didn't compile out of the tarball on a pretty vanilla SuSE install for me.  Cest la vie.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Awsome"
    date: 2002-09-17
    body: "That's awsome! Scroll to the end of the tutorial for the wonderful camel/QT logo ;)\n"
    author: "Anonymous"
  - subject: "SMOKE"
    date: 2002-09-17
    body: "Is SMOKE an answer to .NET's multi laguage capabilities?\n\nMarc"
    author: "Marc"
  - subject: "Re: SMOKE"
    date: 2002-09-18
    body: "In a word no. They are completely different beasts.  SMOKE allows the binding developer an easier method of creating bindings for Qt/KDE.  .Net is a development platform."
    author: "Adam Treat"
  - subject: "Re: SMOKE"
    date: 2002-09-18
    body: "I thought it was designed to be independant of the scripting language on one side and the language of the library on the other side, so that you could create bindings for all kinds of things. One example being Qt to python bindings. Aren't other combinations possible?\n\nMarc "
    author: "Marc"
  - subject: "Re: SMOKE"
    date: 2002-09-18
    body: "Yes, but then you're creating a language that makes calls to a toolkit library.  .NET is more like Java, in that it targets a virtual machine, which waaay back in the day we'd call it \"like P-Code\": a binary file that is executed by an interpreter.  .NET and Java can call Qt, since it's a toolkit library (assuming that the VM supports API extensions a la libraries, which they, like all modern VMs, both do).\n\nA .NET binary should run on any machine, but has the overhead hit of being abstracted from the system, with the expected performance and features issues.  Java forged the way, so those issues have been minimized, but still exist.  A Qt/whatever application can run on any supported platform if you have the right compiler or interpreter, a KDE/whatever application works the same, but you need the kdelibs.  It's lower level, and the binary isn't portable, since it's targeted to that platform.\n\n(To *really* confuse things, Java is both a VM and a language, and the language can be compiled and targeted to a platform, resulting in executable code not needing the VM.)\n\n--\nEvan (who just woke up, needs to run out the door, and may have said something stupid without rereading.  Correct at will, just be gentle. :)  )"
    author: "Evan \"JabberWokky\" E."
  - subject: "Qt-3.1b1 note"
    date: 2002-09-17
    body: "Just thought I'd drop a note for the most \"bleeding edge\" of you that already use Qt-3.1b1 from CVS:\nkalyptus can't parse it correctly yet, so you'll have to wait a bit or use a stock Qt.\n\nG."
    author: "germain"
  - subject: "puic?"
    date: 2002-09-18
    body: "I get it -- Perl + uic -- and maybe it's a pun that works in another language. But, I can tell you that in English that's a pretty distasteful sounding name!\n\nTerrific project, though. Thanks to everyone who was involved!!"
    author: "Otter"
  - subject: "Re: puic?"
    date: 2002-09-18
    body: "The difference is that uic is pronounced \"you-eye-see\", not \"uke\"."
    author: "anon"
  - subject: "RAD or just UI Support"
    date: 2002-09-18
    body: "Could please have a screenshot of your \"RAD\". Is RAD stands for Rapid Advanced Development Environment. I guess it is not RAD Environment, it is just UI support. Don't be carried to much.\n\n "
    author: "GUI"
  - subject: "Re: RAD or just UI Support"
    date: 2002-09-18
    body: "It seems you are confusing IDE (integrated development environment) and\nRAD (Rapid Application Development.)\n<p>\nAnyway, courtesy of Qt Designer, it's kind of both :\n<p>\n<a href=\"http://www.phoenix-library.org/germain/perlqt.png\">perlqt.png</a>\n<p>\nCheers,<Br>\nG."
    author: "germain"
  - subject: "Very Nice"
    date: 2002-09-18
    body: "Wonderful job on the packaging.  Downloaded it.  Followed the instructions (33 minute compile on a P3 700 with 256).  Copy/paste code from the tutorial - came right up.  I'm excited to have some free time to work on some perl applications.\n\nOnly question is... is PerlKDE on the way?"
    author: "Paul Seamons"
  - subject: "Couldn't be happier"
    date: 2002-09-20
    body: "Been waiting for that to happen for quite sometime now. I'm thrilled. I'll be on it in as soon as I am home !\n\n"
    author: "Olivier"
  - subject: "Ruby Bindings"
    date: 2002-09-20
    body: "What's the status of the ruby bindings. Can they use SMOKE too?\n\nThanks\n\nRitchie"
    author: "ritchie"
  - subject: "Will it work on Win32?"
    date: 2002-09-25
    body: "This would be fantastic for x-platform if it worked on Win32. I suppose the only issue is, will smoke build on win32 since the other components already exist. Any thoughts? Is it worth pursuing a port?"
    author: "Phil "
  - subject: "Re: Will it work on Win32?"
    date: 2002-09-25
    body: "Hi,\nto me, the main issue is : PerlQt and  Smoke are GPL, Qt-win free edition isn't...\nMaybe we can add a license exception for Qt-win free, if it does build...\nOtherwise, there is still the Cygwin solution. Should work.\n\nG.\n\n"
    author: "Germain Garand"
  - subject: "Problem building PerlQt"
    date: 2002-11-20
    body: "Following is a note that I sent to Germain. I'm not familiar with the protocol for this group so I'm also posting it here in the event that his is more appropriate than direct mail.\n\nHello Germain,\n\nFirst, let me thank you very much for picking up the work on PerlQt. I had \nused it several years ago for a number of applications and loved it. Now I am \nlooking at it again and pleased to see that it is still alive and going \nstrong.\n\nI downloaded:\n\n        PerlQt-3.002.tar.gz\n\nas well as:\n\n         qt-x11-free-3.1.0.tar.bz2\n\nand am running under:\n\n        RedHat 7.2\n        Linux officeroom 2.4.9-13 #1 Tue Oct 30 20:11:04 EST 2001 i686 unknown\n\nqt built successfully but I had problems with PerlQt. A comment from you at: \nhttp://dot.kde.org/1032279318 dated 17 Sept 2002 indicates that kalyptus \ncan't parse Qt-3.1b1 yet.  Although I am using 3.1.0, and not the beta \nrelease, your comment may still be applicable. That may explain the problem \nbut let me ask it anyway since two months have passed and perhaps that \nsituation has changed.\n\nThe configure command  for PerlQt dies with the following messages appearing \nat the end of the output to the terminal:\n\nGenerating bindings for smoke language...\nStarting writeDoc for qt...\nPreparsing...\nSkipping union QPDevCmdParam\nWriting smokedata.cpp...\nArgList isn't a known type (type=ArgList)\nCardinal isn't a known type (type=Cardinal)\nItem isn't a known type (type=Item)\nQEventLoop::ProcessEventsFlags isn't a known type \n(type=QEventLoop::ProcessEventsFlags)\nQPixmap) protected: QPixmap( int isn't a known type (type=QPixmap) protected: \nQPixmap( int)\nQPtrCollection::Item isn't a known type (type=QPtrCollection::Item)\nQtStaticMetaObjectFunction isn't a known type \n(type=QtStaticMetaObjectFunction)\nWidget isn't a known type (type=Widget)\nWidgetClass isn't a known type (type=WidgetClass)\nWordWrap isn't a known type (type=WordWrap)\nXtAppContext isn't a known type (type=XtAppContext)\nXtPointer isn't a known type (type=XtPointer)\nbool (* qt_static_property)(QObject* , int, int, QVariant* ) isn't a known \ntype (type=bool (* qt_static_property)(QObject* , int, int, QVariant* ))\nWriting x_*.cpp...\ngenerateVirtualMethod: QDnsSocket: No method found for \nQDnsSocket::className() const\nconfig.status: creating smoke/Makefile\nconfig.status: error: cannot find input file: smoke/Makefile.in\n\nIs this because I am using qt 3.1.0? If so, when do you expect to support \nthat version of qt? I am unable to find a previous version of qt at the \ntrolltech website and so I'm pretty much stuck with this.\n\nThe actual error is : \n\"generateVirtualMethod: QDnsSocket: No method found for \nQDnsSocket::className() const\"\n\nThe config.status error about not finding smoke/Makefile.in arises because \ngenerate.pl fails and a \"cd ../..\" following it in the configure script is \nnot executed thus causing the script to be executing in an unexpected \ndirectory. The return from generate.pl should probably be tested in a more \nrobust manner.\n\nThanks,\nBill Wetzel"
    author: "Bill Wetzel"
---
The <a href="http://perlqt.infonium.com/">PerlQt project</a> is pleased to announce today the first public release of PerlQt 3, a full-featured object-oriented <a href="http://www.perl.com/">Perl</a> interface to the Qt3 toolkit.

Key features include

support for nearly all Qt classes through <a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/smoke/README?rev=1.2&content-type=text/vnd.viewcvs-markup">SMOKE</a>, a language-neutral <a href="http://developer.kde.org/language-bindings/index.html">binding library</a> brought to you by Ashley Winters and David Faure (and Richard Dale's <A href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdebindings/kalyptus/README?rev=1.2&content-type=text/vnd.viewcvs-markup">kalyptus</a>), unlimited slots and signals, virtual function overloading, and Rapid Application Development (RAD) through <i>puic</i>, a Qt Designer compatible user interface compiler. Here is a <a href="http://perlqt.infonium.com/screenshot1.png">screenshot</a> of some PerlQt applications. There is also a <a href="http://perlqt.infonium.com/dist/current/doc/index.html">tutorial</a> available to help you get started. Enjoy!
<!--break-->
