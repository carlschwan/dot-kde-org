---
title: "MozillaQuest: Tabbed-Browsing Coming to Konqueror"
date:    2002-07-09
authors:
  - "numanee"
slug:    mozillaquest-tabbed-browsing-coming-konqueror
comments:
  - subject: "Konqueror and New features"
    date: 2002-07-08
    body: "It will be nice to have tabbed browsing but hows about fixing all the bugs in konqueror and getting java to work correctly etc before adding new features.  For example connect to any webmin enabled box using https, in V3.0 it gave you broken pipe in 3.0.1 it just sat there after entering the login and pass and in 3.0.2 it does the same, one of my old boxes still has konq 2 on it and it works fine.  Yes I have logged these as bugs but keep getting asked how to reproduce it can I send a site?  No just install webmin and the perl modules to enable https and hey presto every time.\n\nI love Konq easy to use light and in the later 2 versions was fairly quick but 3 is totally broke for me in most things I use it for, please lets have all the bugs fixed before adding new features."
    author: "Colin"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-08
    body: "I can't comment on your specific bugs (except that Java should really work if you use kwin in 3.0.2, and that in HEAD it supports Netscape's LiveConnect), but please, before complaining and suggestting \"KDE should do this\", try to get enough knowledge about what you're requesting so it at least make sense. Tabbed browsing is being developed primarily by a single developer, who is not really involved in the the HTTP/HTTPS transport and the HTML rendering; thus one being developed is no harm to the other... And, no, it's not a simple matter of having any developer switch to the area that needs some bug fixed; sometimes it is possible, but for things as complex as HTML rendering, it would take someone quite a lot of time to get acquainted with the code well; and that alone doesn't guarantee that the changes they'd make would not produce any regressions.\n\n\n"
    author: "Sad Eagle"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-09
    body: "There were some fixes for Webmin committed recently which will be perhaps backported to KDE 3.0.3."
    author: "Anonymous"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-09
    body: "How about fighting worldwide hunger and wars first before posting comments on newsboards?"
    author: "Anonymouse"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-09
    body: "Perhaps you are forgetting that posting comments on newsboards do not increase worldwide hunger and wars... adding new features does add new bugs and burries old ones deeper."
    author: "Carlos Rodrigues"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-10
    body: "You mean, you'd rather don't have tab browsing because it might have bugs? Or do you fear\nthat tabbrowsing mighty break https even worse?"
    author: "Anonymouse"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-10
    body: "yes, I think you will be surprised to know that there are *lot's* of users who would rather use a software with less features, but reasonable \"bugless\".\n\nI'm by no means being overly critic, what the guys have done with KDE for free is fantastic, I just happen to agree with the previous post.\n\nJose\n\n"
    author: "Jose C. alvarez"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-11
    body: "The first option is unavoidable, there is no bug free code and new features bring new bugs.\nBut although tabbrowsing doesn't have anything to do with https, is somewhat that. I like new features and KDE is getting better every day but maybe some more investment in the \"new features -> fix bugs -> new features\" cicle would be the best at this time instead of \"new features -> fix some bugs -> new features -> new features\""
    author: "Carlos Rodrigues"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-09
    body: "I seemed to have recently had this problem with webmin, but it's working fine for me now with KDE 3.0.1 on SUSE 8.0 so ...\n"
    author: "M Hunter"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-09
    body: "I agree that there is a need to shake out bugs before adding new features. But it seems that developers care more about adding some useless eye-candy than fixing bugs.\nI think it's better to have a rock-solid and bug-free KDE with the current set of features than adding new ones forgetting about things that need to be fixed."
    author: "Carlos Rodrigues"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-09
    body: "<sarcasm>Sounds great!\n\nWhy don't you pull out your favorite text editor and start hacking the code to get all those pesky bugs killed?</sarcasm>\n\nSeriously, remember that KDE is free software. If someone want to add a new feature to Konqueror, that is his/her right under the GPL. Tabbed browsing is a feature that a lot of users want, and you can be rest assured that any existing bugs are being investigated. It's not always easy to kill a bug in software because killing one bug might disable something else or create new bugs. Give the developers time to do it right and don't complain unless you are paying that developer's salary.\n\nThere is a big difference between complaining and submitting a bug report."
    author: "TinWeasil"
  - subject: "Re: Konqueror and New features"
    date: 2002-07-11
    body: "I do not have much time to spend fixing KDE's bugs but I have done it in some occasions.\n\nOf course anyone can add new features to KDE but there is someone in charge of the KDE release cycle isn't there?\n\nYes, bugs are being investigated and they aren't always easy to kill but some persist across KDE releases, some of those being just a matter of 2 - 3 lines of code (see first paragraph).\n\nPeople should complain, that is the only way to get it even better, and should submit bug reports (I surely do it every time I find one), in fact people should do both.\n\nAnd talking about bug reports... KDE's bug report system is awful and messy maybe they should consider bugzilla."
    author: "Carlos Rodrigues"
  - subject: "Is this news?"
    date: 2002-07-08
    body: "MozillaQuest is no good news site: This is no real news anymore, they just took the screenshot from http://konqueror.org and failed to mention Galeon as tabbed-browsing Linux browser."
    author: "Anonymous"
  - subject: "Re: Is this news?"
    date: 2002-07-08
    body: "Yes it's news to many people.  Don't read it if you don't want to."
    author: "KDE User"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "What I think he/she was referring to is that MozillaQuest is a \"troll site\".  For the past two years or so, it's been cut-and-pasting the same articles over and over again and changing the dates.  Basically, the guy behind it (Mike Angelo) is a Slashdot troll pretending to run a news site.\n\nIf Konqueror can get positive press, that's great.  But I'd rather it got positive press from a reputable source.  KDE shouldn't have to scrape so low into the journalistic sewage bucket to get a positive review."
    author: "ac"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "There's nothing wrong with this article that I can see, sorry but your argument is vague and doesn't hold."
    author: "ac"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "Actually, I didn't mention the article at all!  I said that for the past two years, he's been cutting-and-pasting articles and changing the dates.  There's the specifics.  You have the link to the site.  Read through the trash it's printed in the past.  It's adolescent chest-thumping, not journalism."
    author: "ac"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "If you happen to be fortunate enough to have access to plagiarism-detection software, run the text on these links through it:\n\nhttp://www.mozillaquest.com/Mozilla_News_01/Mozilla_RoadMap_21jun01_Story01.html\nhttp://www.mozillaquest.com/Mozilla_News_01/Mozilla_0-9-3_branched-01_Story01.html\nhttp://www.mozillaquest.com/Mozilla_News_01/Mozilla_0-9-4_delayed_Story01.html\nhttp://www.mozillaquest.com/Mozilla_News_01/Mozilla_0-9-5_released_Story01.html\n\nOf course, it's not illegal to plagiarize yourself.  Just lazy."
    author: "ac"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "I looked through these and they mostly seem different articles.  Mozilla releases a snapshot every other day, so who can blame him for reusing text though when even the KDE announcements seem to do that.  :-)"
    author: "ac"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "KDE announcements, although frequent, are actually rather informative and factual.  They may be a bit boring, admittedly, but they're hardly MozillaQuest quality.\n\nMozillaQuest generates articles by playing mad-libs with the template:\n\n\"Mozilla was scheduled to release their [milestone] milestone on [totally made up date], but it has been delayed due to bugs and bad management.  It will likely be the buggiest milestone ever.  Is Mozilla sweeping bugs under the carpet?  As of milestone [milestone], Mozilla had [totally made up number] open bugs.  We recommend you [something stupid] instead of downloading anything more current than [really old milestone].\"\n\nHave you read the articles all the way through?  After a while, it's all one article.  If there's nothing new to say, you usually don't print anything.  This guy prints the same freaking article time after time after time.  And he never prints a \"corrections\" section, which is rare in web media, but rarely as warranted as on MozillaQuest.\n\nI like KDE.  I think KDE is a good product with good people behind it.  They should not simply lap up any positive attention they get.  MQ is way below KDE."
    author: "ac"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "Ok ok.  Look I didn't know all this.  Why don't you relax a bit?  When we don't post articles people complain.  When we post articles people complain.  We just can't win. If you don't like the article quality how about you submit some good ones?"
    author: "Navindra Umanee"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "MozillaQuest is a well known Mozilla bashing site, so no wonder it praises Konqueror. Right."
    author: "Anonymous"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "Yep.  Check out http://www.mozillaquestquest.com/ for a great parody of the MozillaQuest site."
    author: "jmalory"
  - subject: "Re: Is this news?"
    date: 2002-07-15
    body: "So your unhappy that MozillaQuest gave Konqueror a good review? Your unhappy that KDE.NEWS published a none KDE sites positive feed back of Konqueror? Your unhappy that MozillaQuest cut-and-pastes articals from other websites? Is there a web site that doesn't cut-and-past other website articals? Please explain???"
    author: "Jim"
  - subject: "Re: Is this news?"
    date: 2002-07-09
    body: "I wish people would remember that Opera had tabbed browsing well before Mozilla, or Galeon. Opera, OK, OPERA!!"
    author: "Ezz"
  - subject: "Hear hear"
    date: 2002-07-08
    body: "I have exactly the same problem with Konqueror.  When I mailled kde about it I was told they \"thought\" it was a bug in a module of code and they \"think\" they have fixed it and I needed to install the latest CVS to see if they had.\n\nBah.  I have better things to do with my time than muck about with CVS trees.  I thought the idea of KDE was to entice new users and ex windows users?  Right so I report a bug and I am told to try the latest CVS release.  Scenario:  I know nothing about Linux (I actually do, been using it on and off since RH 5.1).  Given this, er, what is CVS?  How do I install from it? Sod that, don't have to do that in Windows, I just download an exe and presto.  What is wrong with releasing a patch rpm for the problem?  Or a tar.gz with a shell script?  And don't say it is because you don't get paid open source blah blah I know a lot of freeware people who write stuff for Windows who manage to release beta upgrades and patches etc without messing around with a CVS tree.\n\nAt the moment my browser of choice is Opera 6 and my, how nice and fast is that, STILL the best tabbed browser imho.  However, if KDE had fixed the bugs in Konq, IT would STILL be my browser of choice just like it was in my KDE 2 days.  Not anymore.  Sort it out KDE or I am off back to Gnome, ver 2 looks quite nice.\n\nA shame really, KDE was looking the most promising of the two."
    author: "Alex Boag-Munroe"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "Now theres a point I am a poor sod who is stuck with a single ISDN connection due to BT's inability to convert my exchange to ADSL but that another story.  Why not release just updates or service packs for we poor mortals on modems etc instead of a full download each time someting is updated to a new version?"
    author: "Colin"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "Well binary service packs do make some sense, but a simple single compile option change, could make those service packs VERY large.\nThe service packs are different for every distribution (so I guess that makes it the job of the distribiter).\n\nYou could compile every stable release from source. That way, you would only have to download the differences between the source codes.\n\nJohan Veenstra"
    author: "Johan Veenstra"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "Hi\nFixing the problem is all the KDE developers can do. They can't install the patch on your system. You need to fetch it from CVS / wait for the next release yourself.\n\n> What is wrong with releasing a patch rpm for the problem? \n\nNothing - except that KDE doesn't release any RPMs at all. All RPMs provided on the KDE site are from packagers FOR the project, but not BY it.\n\n> Or a tar.gz with a shell script?\n\nThis is called cvs. No develper can provide you with a binary-patch for ALL distributions around.\n\n> Sort it out KDE or I am off back to Gnome, ver 2 looks quite nice.\n\nYou wanna threat? Well - just switch! We won't miss you. Why didn't you provide the RPM yourself instead of whining? tststs... ;)\n\nCU\nAndi"
    author: "Andi"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "Excuse me, but I thought the aim of the likes of KDE and Gnome were to bring Linux closer to the desktop market.\n\nI don't expect them to install it.  They could reduce the headache though.  And as Colin said previous, why do you have to download a whole new distribution of KDE just to fix one bug.  It makes no sense.\n\nYou don't need a binary patch.  A shell script can be made to do a compile and install.  Give it some multiple choice options and away you go.\n\n\"We won't miss you.\"  Hmm that's very community minded innit.  I put a structured criticism together and u flame me LOL.  As far as me providing the RPM myself, assuming I know how to why should I?  Linux WON'T corner Windows the way most want it to if the ease-of-use feature responsibilities are hung on the end user.  Hence why Linux hasn't already cornered it today."
    author: "Alex Boag-Munroe"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "You are asking KDE to do the job of your distributor.  KDE provides the fixes.  The distributor is responsible for keeping you happy with updates and upgrade paths.  It's a very real distinction although it's hard for some people to understand."
    author: "Anonymous"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "KDE is developed towards the distributions\n\nIt's not developed towards the users that, afterall, it's the people that use it. Distributions can screw up KDE, but KDE as a project won't care.\n\nI suggested them to keep just one version of binaries for i386 following a standard file system organization. This wouldn't be much but they allways say that KDE will NEVER release binaries!\nHaving a binarie version they could just say: \"Hey I recompiled some soft/librarie that had a bug! Just overwrite xxx.so, etc....\", but BINARIES produce alergies to some kde developers although it would be so great for ALL users :-(\n\nI won't ask for that again... I will use KDE because it's good but I'll have my eyes open to new fresh ideas also.\n\n"
    author: "me"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "> I suggested them to keep just one version of binaries \n> for i386 following a standard file system organization.\n\nfor which OS? Linux, FreeBSD, NetBSD, OpenBSD, Solaris i386, SCO, what?\n\nfor which libc?\n\nfor which compiler?\n\nthis is (partly) why KDE produces source and leaves the binaries to the vendors."
    author: "Aaron J. Seigo"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "Hey! I'm not an idiot.\n\nYou know already.\nYou could compile it for what would suit what big majority of KDE users have. \n\ni386 and linux.\nYES! Let's solve the problems of the big majority!\n\n\nI don't know which libc. I'm not a developer, but I'm sure you know!\n\nYou might be a developer, but we, the users, are not stupid.\n\nAnd other platforms could still use the soft to compile. The same way they do now... But at least joe users could take great benefit from that move."
    author: "me"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "> i386 and linux.\n\nok, now let's pick a version of linux. realize that binaries don't run on every version of linux out of the box.\n\n> I don't know which libc. I'm not a developer, but I'm sure you know!\n\nno, i don't know, because different distros (and different versions of the same distro) use different versions. that is part of the problem.\n\nand why the LSB is so important for the future.\n\n> You might be a developer, but we, the users, are not stupid.\n\nhey now, i never called you stupid. i just pointed out the obvious problems. if you weren't aware of them before you are now.\n\nnow, that said, if you have a target platform in mind nothing stops you from building those binaries and uploading them somewhere for distribution. a fellow made a HUGE slew of KDE 3.0.2 binaries for RH7 himself, so it is quite doable."
    author: "Aaron J. Seigo"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "Suse binaries run on RedHat and on Mandrake. I don't know debian, but it's quite probable....(I guess that makes 90% of Linux boxes)\n\nAnd I was talking about delivering packages for LSB compliant distributions.\nOf course I'm not saying to keep binaries for all the universe of distributions.\n> fellow made a HUGE slew of KDE 3.0.2 binaries for RH7 himself, so it is quite doable\n\nIf it's so doable, why KDE developers cannot deliver the binaries and say, \"KDE has binaries that the majority of our users can install\". And also:\"Distro X and Y and Z are LSB compatible, hence the binaries will install stright away\""
    author: "me"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "> Suse binaries run on RedHat and on Mandrake. I don't know debian, but it's \n> quite probable....(I guess that makes 90% of Linux boxes)\n\nwhich versions of each? how are dependencies handled? how is it layed out in the FS?\n\nyes, you are right that once the LSB is adopted and the distros are all using the same gcc version (or one that is ABI compatiable) then things will be peachy. that isn't the world we live in today. and people were asking why this isn't done today.\n\n> If it's so doable, why KDE developers cannot deliver the binaries \n\nthe answer is in the question: they are developers. they are busy fixing the bugs and adding the features others in this thread are complaining they don't. some of them don't even use linux or i396 boxes!\n\nno, the real question is: if it is so doable and you want it so badly, why aren't you doing it?"
    author: "Aaron J. Seigo"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: ">no, the real question is: if it is so doable and you want it so badly, why aren't you doing it?\n\nYou said it first that is \"doable\". I don't find it so doable. :-)\n\nIf I knew how to do that I would have never asked to get those binaries ;-)\nBut thanks for your explanations.\n\nAnyway, do you really think we'll have the LSB in future?\nWill distros adapt it?\n\nI don't know much bout distro politics, and I also don't know if it would be difficult for them to change their internal structure... \u00bf?\u00bf?\u00bf?\nAny hopes?"
    author: "me"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "the next major version of most of the linux distros will be LSB complian if they aren't already. so, yes, LSB is a definite thing."
    author: "Aaron J. Seigo"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "No, SuSE binaries for KDE will not work reliably on Mandrake 8.x or RedHat 7.x since they use different g++ versions (2.95.3 and RH-2.96). \n\nAnd, BTW, LSB is not anywhere near complete enough to make SB-compliance sufficent for one KDE install to work. It doesn't specify the C++ ABI for starters. "
    author: "Sad Eagle"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "> If it's so doable, why KDE developers cannot deliver\n> the binaries and say, \"KDE has binaries that the\n> majority of our users can install\".\n\nYou make it sound like there is this mythical wonderful \"KDE developer land\" that is a separate place, where they all get together and laugh at users being unable to install KDE.\n\n*YOU* are the developer.  Anyone that uses KDE is the developer.  If you want it done, make binaries, and show the world that they can get binaries for KDE!  If you don't have the time to work on putting those binaries together, hire someone, or contact the discussion list with a *real plan* of how to make it happen.\n\nMost of the great ideas that make it out into the discussion forums on KDE don't die because they don't scratch someone's itch, they die because the person asking for such-and-such feature isn't even willing to take the time to put together something other than a pie-in-the-sky \"wouldn't it be nice\" one-sentence e-mail.\n\nIf you sat down one evening, and said, \"What do I need?\" and wrote it down, and then came to the KDE list, not begging, but asking as an equal, for people to help make it happen, I bet you would have \"official\" KDE binaries.\n\nThe problem is that there are too many people willing to come up with ideas, but they're not willing to see them through.\n\nI don't know a lick of C or C++, but I knew enough to get KDE ported to MacOSX by finding the right people to help me, saying, \"Look, I got 30% of this working, so I know it's possible.  Anyone want to help?\"  You don't have to be able to do everything to be a *part* of the KDE community, rather than just someone on the outside, wondering why his feature request never got implemented."
    author: "Ranger Rick"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "\"and why the LSB is so important for the future\"\n\nI agree that the LSB is important for Linux and also KDE, and it is POSSIBLE to make ONE RPM for ALL LSB-LINUXVERSIONS.\n\nIf we don't make the installation (on all points and all programs) better and eassier we will NEVER stand a chance against MS-Windows. LSB is the key to a solution for this. Lets use it when ready.\n\nGo for standards!"
    author: "Maarten Romerts"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "I am a developer, but i have no idea which version of libc the majority of linux distributions use.\n\nYou are asking for something that sounds simple, but it isnt. Hopefully with time  these issues will go away, but for now pretty much the only way to make a set of binaries that will run on almost all systems is to statically compile the programs, which is AFAIK not even possible to do with kde, and even if it was im sure it would make it even bigger and slower."
    author: "troels"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "The thing that spring to mind is getting Linux on the desktop and currently it does not stand a chance in the corporate enviroment because it does not have the ease of updates across all distros etc.\n\nIs it not time that all this superior knowledge was put together to make Linux the best in the man on the streets eyes?  To do this we need an easy to use update system for all distros, where joe bloggs can just press a button and it is done.\n\nUnited Linux may be a start but more is needed to get it mainstream.  The thing that killed Unix in the past was its diversity amongst dialects so lets not do it again.  Alex is right above although he could have choose better words to express himself.  Whether KDE or Gnome I dont care if one is more stable I use it but I prefer KDE.\n\nWe have to get away from this squabling amongst ourselves and make Linux the best there is and prove it the Joe Bloggs or we will stay as we are a minority forever."
    author: "Colin"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "http://www.linuxbase.org/"
    author: "Aaron J. Seigo"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "Im sorry, but this is just not true. I think linux is very much ready for many corporate uses. I think the biggest challenges is to make it ready for the average home user.\n\nIn almost all corporations they have an IT department that handles all hardware and software setups and upgrading, the normal employee dont have to worry about it. If they do then something is wrong and i wouldn't want to buy stocks in that company :)\n\nAre linux systems hard to upgrade? Hardly. If you use redhat you have the redhat network. If you are using debian you can just make the machine do an apt-get dist-upgrade once a day. Im sure that suse and mandrake have something similar. It really is as simple as pushing a button. Well, in debians case, typing in a single command. Please stop claiming that it is hard to upgrade, it just isnt true anymore, and havent been for quite a while now.\n\nThere is no big problem with diversity, a company can just pick one and just one distribution for the entire company. It might be a problem for commercial software vendors, but if you are afraid you cant run their software then use redhat, which seems to be the one distro that everyone supports.\n\nIm not saying linux is perfect, it isnt, (yet :) but upgrading is not its problem, i even find this easier to do than it is in windows."
    author: "troels"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "At the moment upgrading or installing programs on Linux sucks. I use mandrake 8.2 and many times, (when installing the right rpm for this distro) I get an error-message and the rpm doesn't install. In a worser case I get a broken system. All those different versions of Linux is not good, there are to much of it. \n\nHopefully things will chance when in about a few months most of the distro's are based on LSB (Linux Standards Base)."
    author: "Maarten Romerts"
  - subject: "Re: Hear hear"
    date: 2002-07-10
    body: "I disagree, i think installing software on some linux systems are very easy as long as you use packages made for your exact system.\n\nIn debian all you gotta do is type \"apt-get install programname\" and it will download it and install it for you. I know that there are graphical frontends for it but i havent tried any of those.\n\nI also remember mandrake having a graphical tool that would let you download and install packages as well as resolving the dependencies.\n\nI think most of the problem is that many users are trying to install software the windows way, which is indeed not all that easy. The dependancy hell as many refer to is caused by the fact that a linux system is much more modulized than a windows system. This is one of the strengths of unix, but it does make single package installation more complex. But i think the main problem is not how things are working, but that the users need to be reeducated to use the new tools correctly, just as you had to learn how to do things in windows years ago."
    author: "troels"
  - subject: "Re: Hear hear"
    date: 2002-07-11
    body: "I have never said that installing programs is difficult, but there are too many problems involved in that procces of installing a RPM, even when it is the right one for your system. \n\nIn fact I do believe that RMP has the potential to become the best and most easy to use system for installation and upgrading your system.\n\nBut at the moment things are goiing wrong because of missing standards. And if projects like the LSB succeeds, they will be solved."
    author: "Maarten Romerts"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "> We have to get [...]\n\nThis is my big gripe.  By \"we\" you mean \"not me at all, but possibly some other people\".  Please, be part of the \"we\".  If you want something like this to happen, be part of the process!  Start emailing distros and organizing statdards discussions.\n\nI think one of the ways of summing up a lot of the feeling of the KDE developers is this:\n\nMost of us got involved because we saw something that we thought would be good to fix or add (not because of super human coding skills or because we just couldn't find anything else to do with the time).  By people saying that they want the \"KDE developers\" to do this or that, they're basically saying that they expect something from others -- volunteers mostly -- that they aren't willing to do themselves.\n\nYou're saying \"not only do I want, I expect you to help me\", when you won't give us a hand.  I think that's what a lot of this \"we're not paid to do what you want\" stuff comes from.  It's not just that we're poor -- we just expect you, as an Open Source user to do your part.  Every one of us has a todo list that is growing faster than we can get the things on there done.  \n\nAnd Open Source not only has a different kinds of developers, it has a different kind of user, and really the line is blurry.  You can't come to Open Source and expect it to work the same way that you're used to.  You have to be a part of the solution here (or content).\n\nHow does this map to large companies?  Well, they need to have people working with them in Open Source if they want it to do things *exactly* their way.  It still will cost them a fraction of what it costs to support Windows."
    author: "Scott"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "This is completely irrelevant. Open Source is not motivated in capturing corporate interests, or in looking professional, or in getting good reviews. All these things are nice, but volunteer OSS itself is, pretty much by definition, about coding good software. Whether or not it's applicable to what you'd like to use it for is irrelevant; that's entirely up to the people developing it. If you want to make it better, learn to program and contribute, don't preach on issues you don't seem to have any real understanding of (like, say, implementing an update system, resolving binary incompatabilities, remerging forks, and how developers ought to think about the world)."
    author: "Carbon"
  - subject: "Re: Hear hear"
    date: 2002-08-02
    body: "I figure I will chime in hear, as I am a new user of FreeBSD, and a new user of KDE. (However, I've been writing code for 6 years on windows, so I'm no stranger to compiling...)\n\nFirst off, all this Linux talk is garbage. There are plenty of operating systems out there that KDE will run on. I happen to like FreeBSD. I wish it would get more respect from you pampered binary Linux guys.\n\nSecondly, I understand all the dependencies and different code base problems that occur during a build. I DON'T think that binaries are the solution on UNIX and Linux. If a distributor wants to make a binary, that's fine, but I will ALWAYS compile my code. It's more efficient and flexible that way. (Although slightly more time consuming in the short term)\n\nThird: All the above is fine and dandy for apps like apache and MySQL, because the code base isn't huge, like KDE. But for KDE, I REALLY think the CVS needs to be broken down into separate applications. Recompiling the entire KDE core and library set is REDICULOUS if all I want is the new version of Konqueror. Konqueror won't conquer ANYTHING if it continues to be built this way. \n\nFourth: For all of you people who don't like to compile, go use windows. The compiler is what makes Linux and UNIX the excellent platforms they are today.\n\n<rant off>"
    author: "Matthias Trevarthan"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "KDE is not developed as a way to entice users away from windows. It is developed to provide a solid, usable and useful GUI desktop on UNIX systems such as Linux, BSD, Solaris, etc... if that results in people leaving some other system and using KDE, great. but that isn't the main purpose.\n\nnow, when you submit your bug report and the developers fix it all they can do is point you to the latest development version to see if it is fixed since binaries aren't built every for every distribution. that is a non-trivial task indeed. if you can't or don't want to check the development in CVS, then you can wait until a packaged alpha, beta or final release becomes available. \n\nbut to say that they suck because, although they responded to your report and fixed your bug, they won't provide you personally with a binary of that fix is pretty sad."
    author: "Aaron J. Seigo"
  - subject: "Re: Hear hear"
    date: 2002-07-08
    body: "I didn't say they suck *scrolling up and down the forum* nope, not once did I say they suck.  If they sucked I wouldn't use the K Desktop Environment.\n\nAnd they didn't fix the bug either, which was the whole point."
    author: "Alex Boag-Munroe"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "I thought you didn't try the CVS? How can you tell it is not fixed?\n\nBut why not contributing to KDE? You could provide more information, more help or try a look in the code? I know this is much too hard for most of us, but sometimes new talents appear:-)\n\nAs a developers point of view: sometimes it's more fun to add features than to hunt a bug you cannot reproduce. Flaming doesn't help at all and threatening with switching to Gnome as well.\nThe Gnome people won't provide with binaries either and they would like here a \"Thank you\", too, as all the other people do spending their free time providing everybody with great software.\n"
    author: "Norbert"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "I didn't try the CVS.  It still isn't fixed in the release AFTER the CVS I was referring to (KDE 3.0.1)"
    author: "Alex Boag-Munroe"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "> And they didn't fix the bug either, which was the whole point.\n\nHow do you know ?  Did you try the current development version ? I think not. \nThis problem has long been fixed in the development version for the 3.1 release.  I presume this is your bug report\n\nhttp://bugs.kde.org/db/43/43105.html\n\nwhich was opened on May 26 and the bug was correctly fixed on Jun 09 over a month ago. I just tested it again with webmin-0.990 and it works without a hicup in the current version. Please verify your facts or atleast qualify your statements.\n\nRegards,\nDawit A."
    author: "Dawit A."
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "And I have only just been informed via email that the bug report has been closed.  Mail received at 05:33 on the 9th July.\n\nThis bug was reported by a friend of mine in the early 3.0 betas.  It has taken until today to fix it.  (You state it was fixed on June the 9th, are u sure u didnt mis read July the 9th?  I only got the automated mail closing it just now)\n\nAlso, my PC is mainly used for work, I like to have it stable, and I am not blessed with buckets of cash to have lots of PCs, namely one to test software on.  So running development releases is not a viable solution.  I have occasionally tried beta Red Hat distos and have had my fingers burned.\n\nSo I apologise if having a stable PC prevents be from having all the facts.  If it was fixed a month ago and not this morning, then I should have had the mail informing me of the fix.\n\n"
    author: "Alex Boag-Munroe"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "George Staikos wrote on Date: Thu, 30 May 2002:\n>[snip]\n\nif you can, try with the latest code in CVS. There was a problem with \npersistent SSL connections so we disabled them tonight.\n \nThanks\n--\n\nSo, if you are able to use cvs head, you can get the bugfix... or you can wait for 3.1... or you could ask for it to be backported to the 3.0.x series although it might be there now, I don't know."
    author: "ac"
  - subject: "Re: Hear hear"
    date: 2002-07-09
    body: "> and I needed to install the latest CVS to see if they had.\n\nYou're talking about #43105? The developer asked you \"if you can, try with the latest code in CVS\". If you can't, don't do it. If you don't want to help to enhance KDE, feel free to don't do it.\n\n> Sort it out KDE or I am off back to Gnome, ver 2 looks quite nice. A shame really, KDE was looking the most promising of the two.\n\n\"I will switch to Gnome if you don't change/implement this\", \"This is becoming/behaving like MS windows\", \"KDE is a shame\" - do people wonder if these individuals are ignored by developers?"
    author: "Anonymous"
  - subject: "Also...."
    date: 2002-07-08
    body: "Tabbed browsing isn't going to bridge the gap between Linux and Windows.  Apps that work will.  I know I know Windows crashes, blah blah but out of the box at least Microsoft manage to get IE to open simple https pages and post forms.\n\nEasy to use apps, easy to install and good documentation will bridge the gap.  How about working on that eh?  And fix your existing bugs before you go adding new ones, like the afore mentioned webmin problem and the quite frankly sh1t Java support.\n\nUpgrading your software the way Microsoft does with unfixed bugs and new features that may/may not work might seem Windozey, but people can have that already without switching to Linux.  We need to break that loop and give them a reason to switch, a better reason that \"Well, its free\"."
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Hi,\n\nmaybe you are right. There are some bugs I don't like.\n\nBut KDE is good in bug fixing. Maybe sometimes you have to ask twice or more but bugs are getting fixed.\n\nIn windows I can see a lot of bad bugs and they not get fixed.\n\nLook at the Bug Database, many bugs are fixed.\nI am currently running kde from cvs head and it is stable.\nIt is really good. Don't be so hard.\n\nIf you have problems with the documentation, well you can help to make it better. That is the reason why \"Well, it's free\" is a good reason.\nYou don't have to be a programer if you want to change the world ;)\n\n\nhave fun\nFelix\n\n\n"
    author: "hal"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Don't get me wrong this isn't a Linux v Windows debate.  I hate Windows with a passion not necessarily for the OS pros and cons but simply their business practice.\n\nI just feel that a simple bug like the https one with webmin which I reported when KDE 3 was in the alpha stages should not have got as far as version 3.0.1."
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "Funny you keep talking about that one, i am using webmin and i am using kde 3.0.1 and i have absolutely no problems.\n\nYou seem to be complaining a lot that the KDE team dont fix the bugs, but i have only seen you mention one. I use KDE every day, both at work and at home, and i haven't really noticed many bugs, ever really. (i have been using kde since version 1.0) Maybe i have just been lucky, maybe you have just been unlucky, who knows. But as i said, for me KDE has been a stable companion for almost 4 years now. Yes bugs should be fixed, and i think they are being fixed just fine.\n"
    author: "troels"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Using https and form posting all the time here, mate.  Out of the box Konqueror.  If you're not happy and know so much, why don't you join and help?"
    author: "Anonymous"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Go and log in to webmin via https.  Then tell me all is well."
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "I have no idea what webmin is and I don't have an account to that site anyway."
    author: "Anonymous"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Webmin is an excellent web administration tool for Linux available from http://www.webmin.com\n\nA god send to Linux admins imo.  It has the capability of using https so you can remotely admin a box over the Internet.  Works fine with all other browsers including konq 2.  Dont work with Konq 3."
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Sounds serious, it should be fixed.  Where is the bug report?"
    author: "Anonymous"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "I posted it and so did Alex but I am afraid now I no longer have the bug ID I am afraid and have NOT recieved a clsure report to date.  I posted it to both KDE and RedHat in case it was their distro, with the same response no one has come back and said it is fixed as yet.\n\nI also posted other bugs some hwere fixed but if I look  at the outstanding bug reports for KDE well there are plenty to say the least.  Some very old and still exist tom date.  All I am saying is lets fix the bugs before adding new features surely this would not be to difficult lets not get into the M$ syndrome \"Push it out the door before it is ready I would rather wait for a stable release not let the public beta test it as M$ do.\n\nSurley this is not to much to ask?"
    author: "Colin"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "As Chris Tucker puts it, \"Show me the money!\"."
    author: "Anonymous"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "There is acutally a bug in openssl where an app using openssl isn't correctly notified, once the ssl connection was shut down. The result of this was, that form data was not resent properly and thus lost.\nThis bug is still in openssl, but https/konqueror has a workaround since kde 3.0.1. Since then my homebanking works perfectly :-)\n\nGreetings,\neva"
    author: "eva"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "Not fixed in webmin tho, still broke.  And if it is openssl at fault how come konq 2, moz 1 and opera all work hunky dory?"
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "Works fine here. Mandrake 8.2, KDE 3.0.2"
    author: "Andrea"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "All is well... I JUST tested this now using konqi from KDE 3.0.2.\n\nNot a single problem.  Logged in and everything.  And am able to post/modify things without a problem... Sounds to me like you have a problem on YOUR machine.\n"
    author: "Jeff Stuart"
  - subject: "Re: Also....with webmin?"
    date: 2002-07-09
    body: "With webmin?  over https?  (NOTE THE HTTPS it works fine over normal port 80)"
    author: "Alex Boag-Munroe"
  - subject: "Re: Also....with webmin?"
    date: 2002-07-09
    body: "YES!! HTTPS... Yes, I CAN read... "
    author: "Jeff Stuart"
  - subject: "Re: Also....with webmin?"
    date: 2002-07-09
    body: "Oh and yes, it was webmin....\n\nJust to be perfectly clear here...\n\nIt was https://<ip address>:10000/.  And yes, it worked properly.... Hell, I noticed that I still had postgres database enabled. Fixed that right up.\n\nSounds to me that the problem is somewhere between chair and keyboard... "
    author: "Jeff Stuart"
  - subject: "Re: Also....with webmin?"
    date: 2002-07-09
    body: "doesn't work for me either but it also does't work with Mozilla, so the problem might be somewhere else."
    author: "Georg Kretzschmar"
  - subject: "irony"
    date: 2002-07-09
    body: "So 2 people have confirmed that it works, and this guy has ruined an entire discussion bashing KDE for supposedly not fixing a bug.  Lots of negativity, little love..."
    author: "ac"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "Let's face it: KDE is not VERY user focussed. This is not bad. It's just how it is.\n\nI mean, you still have to be a bit \"tricky\". You won't find X11 configuration tools or binaries from KDE, but it's ok as its their decission. Try to find a decent distro,.... THe problem is that a normal user will never realise how good is KDE because the user will try to use the Kontrol Center to change resolution or will try to download a patch from KDE.org, or install a KDE app double clicking it after having downloaded from KDE, etc....\n\nHonestly I find it pretty good and I would like more integration with any distro, but after talking to some KDE developers I know we will never get something as user friendly as windows or Mac.\n\nBut, we cannot tell them what to do as we are users!!! I mean we are not spending our hours working as they do.\n\nIt's just something that they deliver and we can take it or not, but NEVER criticize... It's not what everybody wants but after some training you can learn howto solve all the dependencies with rpm -Uvh, etc..."
    author: "JL Lanbo"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "I have, I am no Linux newby.  I am speaking on behalf of all those that I would love to see flock to Linux.\n\nIf I get some spare time I'll look into writing a python script or something that pulls stuff from CVS and installs it."
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "I made a perl script that did this in like 5 minutes. (well, 10, i overengineered it a bit so i can see the status on a web page)\n\nits not hard, it basically just loops over each directory, making sure to take arts first, then kdelibs, and does:\n\ncvs update\nmake -f Makefile.cvs\n./configure --some options here\nmake all install\n\nReally, its thats simple."
    author: "troels"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "Have you posted this on freshmeat or somewhere?  Sounds good."
    author: "Alex Boag-Munroe"
  - subject: "Re: Also...."
    date: 2002-07-10
    body: "No, it is too hacky for that :) It works for me, but cant be customized at all without editing the perl file. I tried a script written by someone else that was supposed to make it easy to do, but i couldnt figure out how to use it, which is why i made my own :-)\n\nBut i guess with a little work the options could be placed in a config file which would get rid of that problem."
    author: "troels"
  - subject: "Re: Also...."
    date: 2002-07-10
    body: "(troels, obviously this is not written to you, but rather to the gentleperson (and their company) who complains that KDE doesn't release packages or install scripts):\n\nIt's already done, anyway...\n\nJust go to the Control Center, choose \"Software\" and \"Online Update\".  You can set it to \"Automatic\", and it will just upgrade all your packages for you.  If you don't have that, you're not using SuSE... which is my point:\n\nThis stuff is very distro specific.  Most every distro has a way of handling software updates.  In SuSE's case, it's right in the KDE Control Center, making it easy for users.  Why is it different between distros?\n\nWell, that's simple - because that's the whole *point* of a \"disto\" - distributing software.  They are the glue to hold everything together.  That's their job, that's what they do, and that's what several of them are very very good at.\n\nSo when you say \"Why isn't anybody packaging this\" or \"Why isn't anybody making it so I just press a button and have the latest version\"... you are ignoring the fact that that is the *precise* reason distros exist, both community based ones and corporate based ones.\n\nIt's like you're asking \"when will Linux support keyboards?\".  Have you plugged one in?  \"No\".  Well... \"when will someone distibute these?\"  Have you tried going to a distributor? \"No\".  \n\nYearg.  This topic drives me nuts at times.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "The point I make is not that KDE is bad or Gnome or any Linux app but I see the trend to push it out the door before it is really ready.\n\nI am no newbie to Linux either I have used Linux for about 8 years now and over the last couple it has improved tremendously but the only thing that ever crashes my box is X  applications whether it is KDE, Gnome or whatever.\n\nLets face it though we all like a nice desktop even if mc is my file manager of choice the good old command prompt does not persaude Joe Bloggs CEO of Duff Computers INC to move to Linux on all his desktops.\n\nI am a user you are a user and so are we all but look we have got so far it is our OS not RedHats or Mandrakes or whoever surely we can persaude them all to get together and combine there knowledge to produce a standard format for updates?  It may be apt my favourite or rpm but whatever all use the same the same kernel etc so it makes it easy to do updates as above.\n\nThey can all add  their own nice little extras but lets get a standard together.  The other thing is I cannot post Like this to M$ as they would not listen it would go in file 13:-)  It may fall on deaf ears here but at least I get to say my piece."
    author: "Colin"
  - subject: "Re: Also...."
    date: 2002-07-08
    body: "*Distributions won't agree on having a common standard. (RedHat and Mandrake are not with UnitedLinux)\n*KDE won't agree on delivering binaries.\n\nLinux/BSB/free soft in general is \"tiring\"....It's not easy and nobody agrees on howto solve it...\n\nWhen something really needs to get fixed it's not, and this is tiring... We have been waiting for more than a year now for a REALLY fast konqueror, some people is waiting for bug fixes, and KDE developers like adding features. And this is tiring...\nIt's because they develop for fun and this is the way it has to be. But it's not compatible with what joe users want...\nThat's why I like looking at places like openbeos.org, reactos.org, some time ago freedows, etc.. Allways looking for a joe user THING.\nThis might be what a lot of us are waiting for. And this is not a TROLL, really.\n"
    author: "Anonymous"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "> Distributions won't agree on having a common standard\n\nhttp://www.linuxbase.org\n\n> KDE won't agree on delivering binaries.\n\nno, instead they work with the vendors to do so. this works much better for everyone. suggesting that KDE provide binaries would be a worse solution than what exists now.\n\n> When something really needs to get fixed it's not,\n\nplease take a look at the number of bugs that get closed on bugs.kde.org, please take a look at how things such as java script support, WYSIWYG printing in KWord, IMAP and SMTP AUTH in KMail, etc, etc, etc improved. these things take time, yes. but they do get addressed, and they do get fixed.\n\n> been waiting for more than a year now for a REALLY fast konqueror,\n\nand you got it. do you mean fast loading? well, that's up to the gcc people, and they've fixed quite a bit of that problem with gcc3.1 and are working on the remainder of the problem.\n\n> some people is waiting for bug fixes, and KDE developers like adding \n> features\n\nas Sad Eagle said, the fellow who wrote tabbed browsing was new to KDE devel. this was his first contribution ever, and it may well be his only to date. it didn't subtract anything from the KDE momentum, but rather it added to it. removing tabbed browsing wouldn't have helped fix any other bugs.\n\nthe same or similar can be said for nearly all the other new features.\n\nthat said, you do realize that KDE spends great stretches of time in \"no new features\" (aka \"freeze\") state, right? the only development that actually happens during those feature freezes (which can last from several weeks to several months!) are bug fixes. 3.0.1 and 3.0.2 were also bug-fix-and-better-translations-only releases. this alone discounts your entire \"they don't fix bugs\" complaint-fest.\n\n> That's why I like looking at places like openbeos.org, reactos.org, some\n> time ago freedows, etc..\n\nand yet KDE provides more than any of those do or probably ever will. perhaps this model of development actually works after all.\n\n> And this is tiring...\n\nno, what is tiring is your particular brand of ignorance, unrealistic expectations and FUD\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "Ok... I shut up my mouth\nI was talking in general about linux world, but I mixed it all with KDE.\nYes! KDE is a big and good project and the more you give the more I want :-(\n\nSorry for being so rude and thanks for your reply."
    author: "Anonymous"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "It does sound like a troll however, but im gonna try to make a civilized responce anyway :-)\n\n*It doesnt matter if the distributions agree, just pick one and ignore the rest. It might matter for commerical software vendors, but then again, it seems like lokis games and other software, like opera and netscape, works just fine on all distros.\n\n*Why on earth should KDE make binaries? The distros are making the needed binaries, if you need different binaries then switch to a distro that supplies them. Its that simple. Which version of KDE lacks binary packages? (except CVS, but if you cant compile it yourself then you probably shouldnt be running it in the first place)\n\nI find it funny that you think linux is not easy. I think i have used linux and windows about equally much by now, and i find that linux is in many ways far easier to use than windows.\n\nThings that really need fixing are fixed. If they arent then the chance is that it wasnt really all that serious.\n\nMy konqueror starts in less than 2 seconds and the interface is very responsive, i dont see the problem. On some older hardware maybe, but i find the performance exactable on this 233 MHz PII box. (though it does take about 6 seconds to start, but this is partly because it is compiled with standard (low) optimzations)\n\nI think it is extremely ironic that you complain that KDE is not doing what you want, and then point at BeOS as what the average user wants. I have a feeling that you are really talking about what YOU want.\n\nI want a flexible, integrated system that can be tailored to my needs and let me get my work done in an effective manor that doesn't stress my body too much. (carpal tunnel sundrome, etc). linux + kde is what i have found does this best for me. So i plainly started using kde because i liked it as a user. I have recently started to play around with the code because i also happen to like the platform as a programmer, but that was just recently."
    author: "troels"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "Mandrake : \"urpmi kdegames\"\nDebian : \"apt-get install kdegames\"\nGentoo : \"emerge kdegames\"\n....\n\nOh !!! That's very difficult :)"
    author: "Installation"
  - subject: "Re: Also...."
    date: 2002-07-09
    body: "> Tabbed browsing isn't going to bridge the gap between Linux and Windows. Apps \n> that work will. I know I know Windows crashes, blah blah but out of the box\n> at least Microsoft manage to get IE to open simple https pages and post forms.\n\nWhat does the first have to do with the latter one?\nGet yourself a new Box of SuSE (surely also RH, MDK, Deb etc....) and you will be able to do _everything_ you can do with a freshly installed W2k, esp, your mentioned opening of https-pages and forms.....And: who cares about Windows anyways?\n\n> Easy to use apps, easy to install and good documentation will bridge the gap. \n> How about working on that eh? And fix your existing bugs before you go adding \n> new ones, like the afore mentioned webmin problem and the quite frankly sh1t \n> Java support.\n\nHmm, if every OpenSource Project would have stopped evolution until EVERY bug was fixed, we would surely crawl around with sh, sed, awk etc. and not more.\n\n> Upgrading your software the way Microsoft does with unfixed bugs and new \n> features that may/may not work might seem Windozey, but people can have that \n> already without switching to Linux. We need to break that loop and give them \n> a reason to switch, a better reason that \"Well, its free\".\n\nOK, so propose a good way kde could build an update mechanism (better: create it). I'm using FreeBSD, so I upgrade everything with portinstall/portupgrade. So I want a button \"Update KDE\" which uses portinstall. Fine. Somebody else using debian needs that button which triggers apt-get, the SuSE-user would need the button to trigger Yast Online Update, and the user of this-brandnewish-linux-distro would need the button relying on the brandnewish-update-tool.\nSo why not leave that Stuff to the Distributors and blame _them_ for not beeing able to give the user a nice update mechanism (btw: I think most distros have a more or less good working update mechanism)?\nSee, why KDE won't ever provide binaries?\n\nCheers"
    author: "zapalotta"
  - subject: "Amazing"
    date: 2002-07-09
    body: "So people have been clamouring for Tabs in Konqueror for months.  Dot posts an article that focuses on the good news, and this is nothing but flames?\n\nWow people, show some love.\n"
    author: "ac"
  - subject: "Re: Amazing"
    date: 2002-07-10
    body: "Appearently, the dominant attitude is \"we deserve something for nothing\"."
    author: "dwyip"
  - subject: "It is surely a case of shoot the message..."
    date: 2002-07-12
    body: "Tabbed browsing is great. People love it. I doubt it is going to make anyone change their desktop though... :-)\n\nI think the problem is that features like this get over emphasised, while all the good work behind the scenes maintaining the sources is passed over. We need some balance here..."
    author: "AccUser"
  - subject: "Great news!"
    date: 2002-07-09
    body: "  I fell in love with tabbed browsing in Mozilla and was really hoping that Konq adopted that feature.  I've also got my fingers crossed that Group Bookmarks comes to Konq along with tabbed browsing.  \n\n  In Mozilla, I have a group bookmark labelled \"Comics\" (and several others, too) on my Personal Toolbar.  Clicking on it opens 20 tabs at once and loads all the web comics that I regularly read.  The \"News\" group bookmark does the same for my news sites.  \n\n  The combination of tabbed browsing and group bookmarks is a great enhancement to my net experience. \n\n  Now if only Konqueror could be forced to ignore site-specified font sizes so that I can read many sites, I would never load another browser.   As it stands now, I'm using Mozilla for most of my browsing because I can't read the tiny fonts that many sites use that Konqueror refuses to enlarge. And www.kde.org is one of them!  On first loading, the fonts are readable, displaying at my set font sizes.  However, it quickly switches to much tinier fonts, ones (pixel based?) that \"Increase Font Sizes\" is ineffectual in rendering.  All that does is increase spacing between paragraphs.  "
    author: "Rob Fargher"
  - subject: "Re: Great news!"
    date: 2002-07-09
    body: "OH WOW.. I gotta go try this!!! :D  Never did that before... \n\nAnd like you, at the moment, I use galeon (in this case) as my main browser and not konqi. :(  I think I'm gonna go grab the latest CVS and see how it behaves... "
    author: "Jeff Stuart"
  - subject: "Re: Great news!"
    date: 2002-07-09
    body: "I'm pretty sure you could do something like that in konqueror already.  Just open up all those comic sites in different tabs, then go to save view profile (in the window menu in 3.0, but it was moved to settings in the CVS).  Anyway save it as comics.  So now you can just do load view profile comics and it will bring them all up.  I'm not sure if that is exactly what you are looking for but, it seems to work and is real easy to do."
    author: "Doug Hanley"
  - subject: "To sum up"
    date: 2002-07-09
    body: "I am not alone in my opinions, :)\n\nhttp://www.pclinuxonline.com/modules.php?name=News&file=article&sid=2685"
    author: "Alex Boag-Munroe"
  - subject: "Re: To sum up"
    date: 2002-07-10
    body: "Of course, by saying things like this we ignore things like bugs.kde.org and kde-look.org.  BTW, dunno what happened in your case with bug reports -- every OTHER time I've heard of a KDE bug report it's been acknowledged and fixed (or at least worked on) quite speedily.  A friend of mine reported a raster nasty display bug with KDE on FreeBSD and received a response from Waldo Bastian in about two days; I dunno.  Maybe you picked a bad day; the KDE developers are human, after all ;)\n\nThough, exactly in what way has KDE pushed out the users and focused on the developers' ambitions?  As far as I can tell, it hasn't.  Just because your Webmin \"bug\" (which appearently hasn't actually been an issue for some time, judging by some responses in this thread) took a while to resolve doesn't necessarily mean that KDE is saying \"screw you guys, this is OUR project now\".\n\nBugs are reported, and they're fixed when they're fixed.  Features, -useful- features, have been added at a steady pace.  This isn't a problem with priorities -- it's just the way things work.  The KDE developers have always had a huge checklist of bug reports and/or wishlist items*, and many are involved in multiple sub-projects.  I think manpower, not selfishness, is the primary problem.\n\nPeople who make rants like this should try devising their own complex free-software projects, and then re-examine their opinions.  Nothing changes an opinion on something like experience.\n\n\n* Wishlist items coming from the developers and users, in case you choose to attack me on that; just check bugs.kde.org."
    author: "dwyip"
  - subject: "Re: To sum up"
    date: 2002-07-12
    body: "\nSpamming this forum is no substitute for forgetting your bug ID.  \n\nYou've made your point, now shut up!\n\n"
    author: "John"
  - subject: "Tabbed Browsing"
    date: 2002-07-09
    body: "Such a shame Opera isn't open source.  imo still the best MDI browser out there.\n\nIf Mozilla (Or even KDE, when they add the tab feature) add an option to open ALL new browser windows in a tab by default like Opera does then that browser gets my vote.  I like to stick solely to open source if I can and it would be nice to switch to an open source browser again."
    author: "Alex Boag-Munroe"
  - subject: "Re: Tabbed Browsing"
    date: 2002-07-09
    body: "I see that we haven't attempted to use Galeon at all ehh?  That's one of it's features... Settings | Preferences | User Interface | Tabs, checkbox Open popups in New Tabs.  \n\nAlex, next time, ya MIGHT want to do a little bit more research...\n\nP.S.\n   Sorry to the rest of ya for the flamin here... "
    author: "Jeff Stuart"
  - subject: "Re: Tabbed Browsing"
    date: 2002-07-09
    body: "Ah feck :P\n\nI sort of assumed that because Mozilla didn't have the option, Galeon wouldn't.  Keep forgetting Galeon just uses Gecko DOH\n\nLong Live Open Source!"
    author: "Alex Boag-Munroe"
  - subject: "Konq with Tabs"
    date: 2002-07-09
    body: "Great! I have been waiting for Tabbed Browsing for a looo...ng time. Nowadays I have started using Moz 1.0 /Galeon. Though, I should return to Konqueror once Tabbed Browsing is in.\n\nMany Thanks to the Coders!"
    author: "Anand Vaidya"
  - subject: "Re: Konq with Tabs"
    date: 2002-07-09
    body: "Yes, you can return to konqi :-)\nI'm using tabbeb browsing right now (kde-3.1-alpha1) and it's working great.\nNo bug so far."
    author: "JC"
  - subject: "All KDE apps should use tabs"
    date: 2002-07-09
    body: "Well, the title says it all.  It's a better solution than having a taskbar icon for each window, or a taskbar menu for each application.\n\nGeneralise the new Konqueror code, so that any KDE application that can have many windows or sub-windows, one per \"document\", can easily be written so that it has the option of displaying all these documents in a single tabbed window, with a single (non-menu) icon on the taskbar.\n\nDo it now.  When MS learns this trick, you can say they copied you!\n"
    author: "Desktop User"
  - subject: "Re: All KDE apps should use tabs"
    date: 2002-07-09
    body: "Hmmmmmmmm,\n\nAn ecxelent idea, tabs should be intergated in KDE is a universal standard, wich could be used be aplications. \n\nI realy would like this."
    author: "Maarten Romerts"
  - subject: "Re: All KDE apps should use tabs"
    date: 2002-07-10
    body: "I do not agree.  Take kate for example.  How many filesnames could you read in a tabbed view and how many filenames are you able to read in the \"frame\" view?  Suppose you have 20 files you edit on a regular basis.  Suppose you have kate opened to the half of your desktop.  With the tabbed view you would only be able to see the first 3 letters of each filename.  With the framed version you can see all of the names.\n\nSo what I am saying is this:  tabs make sense in some apps.  In others they would suck big time."
    author: "Soyburg"
  - subject: "Re: All KDE apps should use tabs"
    date: 2002-07-10
    body: "Well, what is possible is the creation of a Tab container KPart.  This has been done -- Microsoft has had it in their Common Controls OCX for quite some time -- but it'd still be nice to have.\n\nOddly enough, tabs were deemed by MS as a bad UI element, and thus should be avoided...or was that certain orientations of tabs?  I can never remember."
    author: "dwyip"
  - subject: "Re: All KDE apps should use tabs"
    date: 2002-08-13
    body: "Yes, it should be available as an option in KDE apps.  The alternative is that lots of different apps (Kate, Konqueror, Konsole, Kdevelop) will independently develop their own code to do the same thing.\n"
    author: "Desktop User 2"
  - subject: "reinvented the wheel, KDevelop also has this GUI"
    date: 2002-07-11
    body: "Sad to see that Konqueror tries an own implementation although KDevelop already uses several UI modes including TabPage mode since ages.\nI just want to remark:\nIf Konqueror had used QextMDI (like KDevelop did), it would automatically have also got other MDI modes additionally, for instance the MDI mode that people are used to from Opera or Internet Explorer.\n"
    author: "F@lk"
---
<a href="http://mozillaquest.com/">MozillaQuest</a> is running <a href="http://mozillaquest.com/News02/KDE_Konqueror_Tabs_Coming_Story01.html">a nice little story</a> on the upcoming Tabs feature in Konqueror. <i>"The K Desktop Environment (KDE) certainly has done lots to narrow the gap between the Linux desktop and the Microsoft Windows desktop. And the addition of tabbed-browsing to KDE's Konqueror browser is one more large step in closing that gap. In our opinion, the K Desktop Environment already is just as good as, if not better than, the MS Windows desktop."</i>  Stay tuned for the next alpha!
<!--break-->
