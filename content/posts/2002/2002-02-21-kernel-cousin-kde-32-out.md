---
title: "Kernel Cousin KDE #32 Is Out"
date:    2002-02-21
authors:
  - "Someone"
slug:    kernel-cousin-kde-32-out
comments:
  - subject: "TMS"
    date: 2002-02-21
    body: "The Thumbnail Managing Standard promotes ugly GNOME urls such as file:/// and makes the nice KDE urls like file:/ useless.  I think that's annoying and unpleasant in a standard.\n"
    author: "ac"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "Have you read the TMS?  I ask only because I just finished, and *nowhere* in there does it specify or even mention the file:/// syntax.  It does, however, once mention that you can use the file: http: or ftp: syntax to access the thumbnails (the second to last page, iirc).  So either I'm missing something, they changed their standard in between when you posted and I read it, or you misread.\n\nHAND."
    author: "David Bishop"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "Yes.  Do you know what MD5 is?  Did you see the examples? You didn't see file:///?  Are you blind?\n\nIt's not hard to put 2-and-2 together, but if you still don't believe me, I will spell it out."
    author: "ac"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "MD5 doesn`t hash \"file://\"."
    author: "Anonymous"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "It has to, otherwise http:// ftp:// and all other url:// will clash.\n"
    author: "ac"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "Ah, found it the second time through.  So, what was your complaint again?  IIRC (and I do) file:/// is the *proper* way to specify a local file using a uri: (see http://www.w3.org/Addressing/rfc1738.txt, section 3.10).  The fact is that kde provides file:/ as a convenience, that doesn't have to go away. Reread that section, and you'll see how they are talking about the canonical uri, *not* what you'll actually be typing in (no more than you'll have to reference files on the command line by typing vi file:///home/ac/.bashrc).  The konq developers are more than able to expand file:/home/foo to file:///home/foo while doing the md5 hash, seeing as they can already expand ~ to /home/foo.  To sum up, I still think your blowing it out of proportion, and don't actually understand what's going on."
    author: "David Bishop"
  - subject: "Re: TMS"
    date: 2002-02-22
    body: "I think you should look at section 3.1 yourself. It applies to \"URL schemes that involve the direct use of an IP-based protocol to a specified host on the Internet\", which does not apply to the \"file\" scheme.\n\n// is not just there to help out the : in delimiting the scheme name from the rest of the URL, but it actually has a meaning. The meaning is that an internet hostname follows, hence why it is not used with \"mailto\" and \"news\" URLs.\n\nThe document recommends its use with \"localhost\" or an empty string as the internet hostname, but doesn't really delve into the issue much further."
    author: "AC"
  - subject: "Re: TMS"
    date: 2002-02-22
    body: "My understanding is that the file:// is necessary *for a full uri*, and is then followed by either a hostname or nothing, then the path, which is what gives us that (admittedly ugly) file:///home/foo.  So kde apps can keep happily accepting file:/foo, and any requests for the full uri will (happily!) get the \"correct\" one in response.  The same as bash accepts ~ ...\n\nBtw, I'm sorry if at any point in the thread I seemed cross.  I certainly didn't mean to appear that way, and have probably not peppered my typing with enough smileys...  Here you go: :-)"
    author: "David Bishop"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "The file:/// is how you're supposed to reference a file on the local system using a URI.  GNOME developers didn't make it up.  It's following standards."
    author: "Jamin P. Gray"
  - subject: "Re: TMS"
    date: 2002-02-21
    body: "I agree. I frequently use RMB->Copy Image Location inside Konqueror. When I paste this into a Gimp->File Open I have to remove the \"file:/\" because it isn't recognized. It's annoying."
    author: "Sean Pecor"
  - subject: "Re: TMS"
    date: 2002-02-22
    body: "It is annoying that the Gimp tries to enforce a standard by the letter. I agree that it is always good to follow and implement standards. \n\nBut comfort is equally important. I like the fact that I can type\ne.g. www.gnotices.org in konqueror and it will add http://. Of course yahoo.com is not an URI by the book, but it is good enough for all USERS.\n\nSo the right thing would be to fix the limitation in gimp and other GTK/GNOME apps, so that both file:/// and file:/ are accepted like in KDE."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: TMS"
    date: 2002-02-23
    body: "<i>marked</i>\n<!--\nJust like KDE's standard for the Clipboard, or KDE's own drag'n'drop standard,..its done before...it wouldnt surprise me if KDE developers want to invent another incompatible \"standard\". The funny thing is that took \"major revisions\" to get it changed..geez KDE3 is only about to support proper X11 Clipboard procedures...geez.. even pre-gnome 1.0 had this.\n-->\n\n"
    author: "joe99"
  - subject: "Re: TMS"
    date: 2002-02-23
    body: "Since it's all internal, who cares whether it's file:/// or file:/? \n\n"
    author: "David Starner"
  - subject: "import filter"
    date: 2002-02-21
    body: "Can anyone please tell me what \"xslt\" files are? Thank You!"
    author: "me"
  - subject: "Re: import filter"
    date: 2002-02-21
    body: "XSLT's a language for transforming between XML document formats, although most XSLT implementations usually support folding into HTML (changing eg <br/> into <br> or <br /> and not using <![CDATA[ ... ] ] > sections, for instance)\n\nI'm not a great fan of it myself because it itself is an XML language, and quickly becomes very byzantine to code in for anything remotely complex, though it is a W3C standard, and has been around since 99 so there's plenty of implementations about."
    author: "Somebody"
  - subject: "Re: import filter"
    date: 2002-02-22
    body: "A xslt file allows you to convert a xml file in what you want. For example, kword in xsl:fo, rtf, html or your own xml format.\n\nIt work with templates. When a template is found ( == a markup), it execute the \"code\" of the template to generate the output fot this markup and for all the child markup.\n\nCurrently, just a xsl:fo xslt file exist in cvs only for tests."
    author: "Robert Jacolin"
  - subject: "Re: import filter"
    date: 2002-02-23
    body: "\"A xslt file allows you to convert a xml file in what you want. For example, kword in xsl:fo, rtf, html or your own xml format.\"\n\n... or, of course, an Abiword or OpenOffice document, scince AFAIK they all - as well as KOffice - use some XML format. Seems to be a Good Thing.\n\nUnfortunatly, at least KOffice files aren't \"plain\" XML, but a tarball containing several XML and Non-XML files (no idea about the others, but I wouldn't be surprised if they did something similar), so simply applying a stylesheet will not be enough. Not the best design choice, IMHO."
    author: "Henrik"
---
<a href="mailto:aseigo@olympusproject.org">Aaron J. Seigo</a> delivers again with <a href="http://kt.zork.net/kde/kde20020215_32.html">Kernel Cousin KDE #32</a>. This week's summary includes talk about DCOP for C programs, bleeding edge adventures, a XSLT <a href="http://koffice.org/filters/status.phtml">KOffice filter</a>, fixes for IRIX, the aRts  CVS module and changes to make KDE comply to the <A href="http://triq.net/~pearl/thumb-spec.php">Thumbnail Managing Standard</a>.
<!--break-->
