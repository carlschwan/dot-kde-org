---
title: "Kooka Scanner Suite Now With Website"
date:    2002-05-11
authors:
  - "mcolton"
slug:    kooka-scanner-suite-now-website
comments:
  - subject: "OCR"
    date: 2002-05-11
    body: "How good is OCR?  I remember trying it 4-5 years ago when I had my old scanner, and back then every OCR program I tried wouldn't work properly as it always missed words on the pages that I scanned in.. They were also terribly slow..\n\nWhat's the error-percentage today?\n\nPs. Keep up with the good work! Did you know that Kooka is one of the few programs that Mandrake have screenshots of for each distribution release? "
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: OCR"
    date: 2002-05-11
    body: "OCR is suprisingly good considering the pain it is to program ... I notice lots of people help with fun high-profile projects and less with low profile hard slogging type projects.  gOCR, sane, gimp, pango etc. prolly share some programmers but this thing needs corporate backing I think ...."
    author: "NameSuggesterEngine"
  - subject: "Re: OCR"
    date: 2002-05-11
    body: "In case no one notices .... SANE now has a backend that supports Gphoto2 devices as well (still under development).  Thus with one masterful API wee get Kooka, camera, scanner, an OCR support .... all exportable as a service to any KDE app that wants to use it.\n\nThis may not be \"Enterprise Ready\" just yet but the idea is quite frankly awesome and is bound to be good for development and catching bugs. The projects are essentially leveraging each others contributors.\n\nIt would be nice if KDE, Gnome and GNUstep could export services to one another."
    author: "Positive Spin Dr."
  - subject: "Re: OCR"
    date: 2002-05-12
    body: "How good is \"surprisingly good considering\"?  If you scan in a 8.5*11 sheet of paper with clear print on it, is it likely to get the whole thing with no errors?  1 error?  10 errors?  more?"
    author: "not me"
  - subject: "Re: OCR"
    date: 2002-05-12
    body: "Looking the screenshots on the website, the rate seems to be more on the order of 2-3 per paragraph. That was for some German text so perhaps english will be a little better with the lack of \"funny\" marks on the characters. I could probably tolerate that kind of error but i've never used OCR before so don't know what to expect.\n"
    author: "somewun"
  - subject: "Re: OCR"
    date: 2002-05-12
    body: "I'm french and I try gocr alone : it works not so bad with a single column text (the recognition will be perfect when gocr can recognize the CCEDILL and the ligature o-e), but the result is catastrophic with a multicolumn text."
    author: "Capit Igloo"
  - subject: "Does anyone know?"
    date: 2002-05-12
    body: "What OCR software does Google use for their Catalog site(http://catalogs.google.com/)?   It must be good if they can do 2,7000 catalogs.   It is probably completely automated I would assume."
    author: "Rosis"
  - subject: "Re: Does anyone know?"
    date: 2002-05-12
    body: "> It is probably completely automated I would assume.\n\nDon't count on it.\nThe company I work for is (among other things) a scanning bureau, we do document archiving and scanning and OCR of everything from courier consignment notes, to multipage forms, to books, and if you want any sort of OCR, there MUST be a manual repair process, where an operator compares the original with the OCR'd version, and fixes the mistakes.\n\nUsaualy the OCR engine has a configurable confidence threshold, so any document, or field that doesn't meet that confidence gets put in the repair queue.\n\nPart of the problem is that there are some letters and numbers, that if printed in the wrong font, even a 100% accurate OCR engine (which doesn't exist) could not possibly guarantee to get right.  Just look at the word Ill (That's capital 'i' and two lowercase 'L's) in a sans serif font to see what I mean.\n\nIn a big job, we could easily have a room full of operators keying continually to keep up with the repairing needed on the ouput of one (high speed) scanner, and that's with forms that have clearly marked boxes for text, and dropout colours."
    author: "Stuart Herring"
  - subject: "Re: Does anyone know?"
    date: 2002-05-12
    body: "The 'Ill' problem is automated to some degree by simply using a dictionary. If you have a word that looks like '|||', it's not hard to figure out that 'Ill' is the only word that actually exists. If there are alternatives, choose the one with higher frequency. The process can be further improved by analyzing the sentence grammar.\n\nDifferent fonts is a minor problem compared to handwriting recognition. I have an application on my Psion PDA, where I can write the word 'Reykjavik' very sloppily and still get it recognized. That's because there are no other alternatives. I can look at the list of alternative spellings, and see that 'Reukjavik', 'Heykjavik', 'Reukiavik' and other and other non-words have been ruled out.\n\nThere's no such thing as perfect OCR, but you can improve the process a lot by analyzing word by word instead of character by character."
    author: "Gaute Hvoslef Kvalnes"
  - subject: "How does it compare to Vuescan?"
    date: 2002-05-13
    body: "Anyone with Kooka and Vuescan experience have any opinions on how they stack up against each other. When I got my slide scanner, Vuescan was so much more sophisticated than anything Free that I gladly shelled out for it. Is Kooka anywhere near Vuescan?"
    author: "Anonymous"
  - subject: "Re: How does it compare to Vuescan?"
    date: 2002-05-14
    body: "Faster; not as accurate"
    author: "uncertainty_principal"
  - subject: "Don't like it so far :-}"
    date: 2002-05-14
    body: "Sorry, but I'm not a fan of kookas (current) UI. Like most Linux scanner/cd-writer GUIs kooka ask for a (scsi)-device. Why must I choose a device? I want _only_ choose the _name_ of my scanner/cdwriter? BTW is'nt  this something for KDE control center (hardware devices)? Same thing for default resolutions, gamma etc of the device? \nWhen the program starts most of the window has no function. This is the place where later the scanned images appear.\nKooka gallery has no standard KDE path dialog,\nThe sliders are hard to use - try to change resolution from 100dpi to 200dpi - I get only 196 or 201 dpi - I have to use the textedit widget. BTW the KDE-standard is first textedit widget then slider (pedant) ;-)\n\nI like UIs that help to solve tasks. I have a scanner, so I want to scan and\n- print (photo copy)\n- fax\n- mail\n- paste as (Image|text (ocr)\n\nDo I really need a gallery function when I could use my scanner with e.g. pixie by a kio_slave|libkscan|kpart?\nI would like to open kprinter and kprintfax and find there a special scanner extension for photo copy and fax. IMHO a special scan application is useful for special tasks like OCR. The lib is good, UI must be improved here as well, the app does not look like a tool integrated in my DE.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: Don't like it so far :-}"
    date: 2002-05-15
    body: "I find th preview window too small to make any accurate seclections.  You should be able to preview in the main window (or is it just not obvious how to do this?)."
    author: "John"
  - subject: "Re: Don't like it so far :-}"
    date: 2002-05-16
    body: "I like to comment some of the remarks just to explain why things are somehow\nnot so straightforward with scanning...\nWhy do you have to select a scan device? Well, you can easily have two scsi\nscanners of the same vendor and model attached and they only differ in their devicefile. If you are not so keen on scanning and have just one you should\nselect 'Always use that scanner and do not ask again' ;-)\nYou are right, after starting the program, the viewer appears to be empty.\nBut that changes immediately after you have scanned the first image. I dislike scan programs that do not show the scan results but save them silently. And that is what the gallery is for: You do not need to enter a filename for every single test scan. Just scan on and let kooka save your tries automatically, and if your scan result is that what you want (for me usually after ten tries ;-) you simply drag it to any konqueror around...\nIt is true that Kooka does not provide a fax or mail function (yet). But my proposal would be that the specific applications for that should accept drops of images and send them or use the scanservice provided by libkscan. The same for pixie and friends: They do not need Kooka - they need the scanservice if they want to receive images from the scanner ;)\n"
    author: "Klaas Freitag"
  - subject: "Re: Don't like it so far :-}"
    date: 2002-05-16
    body: "I like to comment some of the remarks just to explain why things are somehow not so straightforward with scanning ;-)\n\nWhy do you have to select a scan device? Well, you can easily have two scsi\nscanners of the same vendor and model attached and they only differ in their devicefile. If you are not so keen on scanning and have just one you should\nselect 'Always use that scanner and do not ask again' ;-)\n\nAfter starting the program, the viewer appears to be empty.\nBut that changes immediately after you have scanned the first image. I dislike scan programs that do not show the scan results but save them silently. And that is what the gallery is for: You do not need to enter a filename for every single test scan. Just scan on and let kooka save your tries automatically, and if your scan result is that what you want (for me usually after ten tries ;-) you simply drag it to any konqueror around...\n\nIt is true that Kooka does not provide a fax or mail function (yet). But my proposal would be that the specific applications for that should accept drops of images and send them or use the scanservice provided by libkscan. The same for pixie and friends: They do not need Kooka - they need the scanservice if they want to receive images from the scanner ;)\n"
    author: "Klaas Freitag"
  - subject: "Question"
    date: 2007-02-01
    body: "http://dwhs.info And what google catalog have relationship with kde?I mean that port of google suck.Like is not enough to see google in every day use."
    author: "Luka Horvatic"
---
The Kooka team is proud to announce the launch of the <a href="http://kooka.kde.org/">official Kooka website</a>. Kooka is a scanner management suite for KDE with support for Optical Character Recognition (OCR).  The Kooka web site offers extensive documentation on <a href="http://www.kde.org/kooka/doc/">Kooka</a> and the <a href="http://www.kde.org/kooka/libkscan/">KScan library</a>, future <a href="http://www.kde.org/kooka/plans/">project plans</a>, <a href="http://www.kde.org/kooka/screenshots/">screenshots</a>, and much more.
<!--break-->
<h3>Kooka supports</h3>
      <p><b>Scanning</b></p>
      <ul>
        <li>Scanner support through the <a href="http://www.mostang.com/sane/">SANE</a> library.</li>
        <li>Provides a user-friendly interface for important scanner options such as resolution, mode, threshold.</li>
        <li>Supports preview and full final scans.</li>
        <li>Supports interactive scan area selection.</li>
      </ul>
      <p><b>Image Storage</b></p>
      <ul>
        <li>The save assistant helps you to find the correct image format for your purpose and creates a filename automatically.</li>
        <li>Images are stored in the default gallery -- no need to find a place to save for every test scan.</li>
      </ul>
      <p><b>The Image Gallery</b></p>
      <ul>
        <li>A treeview-organised workplace where your images are stored.</li>
        <li>Create and remove folders to organise your image collections.</li>
        <li>Drag and drop with other KDE programs.</li>
      </ul>
      <p><b>OCR</b></p>
      <ul>
        <li>Kooka supports <a href="http://jocr.sourceforge.net/">GOCR</a>, an open source Optical Character Recognition program.</li>
      </ul>
      <p><b>KDE Scan Service</b></p>
      <ul>
        <li>The KScan Library provides a user-friendly scan interface for all other KDE applications.</p></li>
        <li>Currently supported by both <a href="http://www.koffice.org/">KOffice</a> and KView.</li>
      </ul>