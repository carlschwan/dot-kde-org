---
title: "KDE 3.1 RC3: Last Dance?"
date:    2002-11-12
authors:
  - "dmueller"
slug:    kde-31-rc3-last-dance
comments:
  - subject: "shit happens i guess..."
    date: 2002-11-12
    body: "Ok.. I sat there staring at the unstable/ dir on ftp.kde.org.. then *finaly*.. the 3.1rc3 dir apeard when i pressed F5 for only god knows which time.\n\nTime to let my darling rouge (dual PIII Tualatin 1266) stretch her legs for a while.\n\nAfter libs and base i checked back in to check if the two most visible points of annoyance was gone.. that was *not* the case :(\n\n1) the icons *still* blink on the desktop (#45472). And now a new bug has come up.. at least i think so.. i cant drag anything onto the desktop.. that is.. it shows that \u00d8 curser over anything and everything i try to drag there. Furthermore it has become persistant that i am not allowed to move my icons at all.. they just \"fly\" back. This prevents me from \"hacking\" the above syndrome, as i found out that i could make them stop blinking by pulling one of them a notch out of place and then right-click -> icons -> allign to grid.\n\n2) When using the high-color version of Keramik i still get a blue line at the bottom of every window.\n\nto be continued....\n\n/kidcat\n\nOS: LFS-4.0\nBuildmethod: BLFS\nNote: i dont use hardcore opts at all: -Os -march=pentium3 -fomit-frame-pointer"
    author: "kidcat"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "\"2) When using the high-color version of Keramik i still get a blue line at the bottom of every window.\"\n\nHave you tried deselecting \"Draw grab bars below windows\" in Configure [Keramik] under Window Decorations in Control Center?"
    author: "Morten Hustveit"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "yes.. shit happens.. i typed before i checked.. that blue line is a *feature*\ndo i need to flame myself to make sure others dont?\n\nok\n\nkidcat! you @#\u00a4$ stupid cat.. u never do anything @#\"& right!\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "> I sat there staring at the unstable/ dir on ftp.kde.org.. then *finaly*.. the 3.1rc3 dir apeard when i pressed F5 for only god knows which time.\n\n\nI think someone needs to get out more... ;-)\n"
    author: "Per"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "out? where *is* that place?\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "This is for you, \n\thttp://developer.kde.org/source/anoncvs.html\n\n\nP\n"
    author: "perraw"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "> 1) the icons *still* blink on the desktop (#45472). And now a new bug has come up..\n\nYou tried what the last comment suggested and added to the report if this is the error source? No."
    author: "Anonymous"
  - subject: "more info on the non-behaving desktop icons"
    date: 2002-11-12
    body: "ok.. i figured out that if i disabled icons on the desktop and re-enabled them i could once again move them and drag files to the desktop.\n\nthey still blink tho..\n\nAND i cant make the blinking go away by using the above method..\n\n/kidcat\n\nBTW: the reason (before someone starts bitchin about it) that i post like a madman is because it is *very* important to me that kde-3.1 becomes a Grand Slam Succes (TM). Hell i even vote for a RC4."
    author: "kidcat"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-12
    body: "I vote for RC10, RC20 whatever !!\n\nIn the grand scheme of things, if you look back at Netscape 4 for instance, not many people can tell you exactly when it was released, but they sure can tell you how broken it is/was. \n\nPeople remember IE4 has better CSS support than NS4. Which was released first? I don't know, and frankly it's irrelevant now :)\n\nDelay KDE 3.1 for as long as it takes to make it rock-solid enough for people to remember in the years to come. "
    author: "More RC's"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-12
    body: "I second this! Better another RC or two, but then more stable. Anyway: Keep up the good work guys."
    author: "Joersch"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-12
    body: "Could not agree more! IMO most major stable KDE releases are way to buggy to be named stable. Stable is *not* a synonim for usable. Why not just make a release, similar to the final one, with binaries and all, not a one week thing, but name it RC4 instead of 3.1 Final. \nIn other words, I'm sugesting that 3.1.0 should be renamed RC1 and 3.1.1 should be renamed 3.1.0.\n\nJad"
    author: "Jad"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-12
    body: "Right on! I second, uh, third this 100%. Pleeze, pleeze put stability above everything else. I know features are more sexy, but I'm using 3.0.4 right now and there are still a few glitches. And that would be the fourth bugfix release, right? Maybe it should have been 3.0 RC4. \nI love Linux, I love KDE, but for world domination we need a (well, almost ;) crashproof Desktop. I hope the many approving comments on this topic show its importance. \nThanx for KDE!"
    author: "sgipan"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-13
    body: "I am in support of this as well. I would love noatun not to crash on me. :) \n\nStabilty, speed, features. In that order."
    author: "Mark Hillary"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-13
    body: "I am in support of this as well. I would love noatun not to crash on me. :) \n\nStabilty, speed, features. In that order."
    author: "Mark Hillary"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-13
    body: "I too agree. I've not tried a RC, I tried a snapshot for a long time ago, and I was very happy to see the changes made on KDE, and since I use KDE every day, at work, at home and on the train, there are no doubt.\nI'd really like the version 3.1 come out as fast as possible, but not before it is stable secure and fast. (In that order as mentioned in the reply above)\n\nKDE 3.0.4 still supplies most really needed requieries as far as I'm concerned. So just delay KDE 3.1 until you are absolutelly shure that most bugs are gone"
    author: "Jarl Gjessing"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-15
    body: "\"People remember IE4 has better CSS support than NS4. Which was released first?\"\n\nNS4, of course.\n\nWhen Netscape announced Communicator they were trying to get a technology called \"Javascript Stylesheets\" (JSSS) into W3C, and Microsoft tried to copy them. Microsoft failed, and re-invented the whole thing, and renamed it Cascading Style Sheets (CSS).\n\nMicrosoft then announced a few months before Communicator's release that they were dropping JSSS and putting CSS into their new browser *AND*, more importantly, that it was \"on the way\" to become an approved W3C standard.\n\nNetscape did the wrong thing: they cried out, hurriedly dropped JSSS, tried to implement CSS within their ultra-tight time frame left until release, failed utterly, and then quickly wrote a Javascript-to-JSSS wrapper hack that could at least parse the most important CSS commands and internally convert them to Javascript \"document.write()\" command equivalents.\n\nJSSS originally was well thought out, quite fast (at least Netscape employees claimed later on) and technically better than CSS, because most of the rendering stuff could be done by Javascript (and Netscape's support for Javascript was *WAY* faster than anything IE had to offer at that time. Heck, IE didn't even understand HTTP/304 completely in version 4 and kept reloading stuff it already had!)\n\nThe CSS-to-Javascript-to-JSSS wrapper is what makes Netscape 4 so ultra-slow when it tries to work out CSS commands. It's not CSS, it's not JSSS, it's the wrapper; it wasn't supposed to be published, it was a dirty hack that was the only solution left. Had Netscape stayed with their JSSS technology, CSS might never have surfaced - but when MS told the world \"it's gonna be a standard soon\" and everybody jumped, it became a self fulfilling prophecy.\n\n\nSorry. Completely off topic but I just couldn't resist. ;)\n"
    author: "Jens"
  - subject: "RC9000"
    date: 2002-11-14
    body: "I am new to KDE and linux in general, but this idea of having more RCs (and thus getting a more stable product) is very apealing :) I am ALL for it!"
    author: "A_Newbie"
  - subject: "Re: more info on the non-behaving desktop icons"
    date: 2002-11-19
    body: "> Hell i even vote for a RC4.\n\nMe too, as always. I prefer waiting a few more weeks for KDE 3.1 if the result is more stable and reliable. KDE x.0.0 releases tend to appear sligthly \"unfinished\" and altough KDE 3.0 was pretty good in this respect, IMHO being the most polished x.0 release ever made by the KDE team, there are still some annoying glitches left even in KDE 3.0.4.\n\nGreetinx,\n\n  Gunter"
    author: "Gunter Ohrner"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-12
    body: "Heh, so I wasn't the only one then?"
    author: "x3ja"
  - subject: "Re: shit happens i guess..."
    date: 2002-11-17
    body: "Uncheck in the \"control-center->Desktop->behaviour->Display devices on desktop\"\nand the blinking stops. I haven't dug into the code to find out what exactly is turned off or on."
    author: "Turtle"
  - subject: "compiling qt31rc3 on mandrake 9"
    date: 2002-11-12
    body: "Hi there, I tried to build kde rc2 on mandrake 9  and, I stopped because I could not even compile qt 3.1rc3, the configuration went fine, althought I was told the -xft option for true types was to be disable due to \"internal functional testing\" ( the screenshot seems to have it working thought), but the compilation fails.  did anyone else encounter the same problem?\ntake care\nrenato"
    author: "renato dall'armi"
  - subject: "Re: compiling qt31rc3 on mandrake 9"
    date: 2002-11-12
    body: "What was the error you got when it stopped compiling?"
    author: "Johan Veenstra"
  - subject: "Multimedia"
    date: 2002-11-12
    body: "Ok.. two smallies from the Multimedia package...\n\n1) Noatun seems to have a tiny issue with internet-streams (is it called ShoutCast?). I have use live365.com as the testcase. It gives a loud click between the songs.. Xmms / Windows Media Scrambler (or what its called) does not do this. \n1b) Im not sure about whatever this has been fixed or not since its hard to reproduce: in rc2 Noatun in winamp-mode would screw the visualizasion area when theese louds click happened.. by this i mean that the area just became transparent untill visualizasion was disabled and re-enabled.\n\n2) Kmix in docked mode has a wierd quirk (with \"enable system tray volume control\"). It wont come up on the first click.. it simply *has* to have two clicks before the slider shows... this can be reproduced by: fist click it, then click around at another app or start something, then click kmix and it comes up.. its not like it requres a dubble-click. Dubble-clicking it also works (oubviously). Personally i dont mind this rather charming little bug.. but all other things in the dock responds on single-click.\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Multimedia"
    date: 2002-11-12
    body: "You really should use http://bugs.kde.org."
    author: "Anonymous"
  - subject: "Re: bugs.kde.org"
    date: 2002-11-12
    body: "a) I will.. as soon as i can set the version to rc3.\nb) It has been proved over and over again that posting bugs here too is can be a good thing because they get a lot of attention. In part because the developers also read here. And also because someone with novice/midranging programming skills might see it and thing \"now that doesnt sound so hard to fix.. i think ill take a whack at it\"\nc) what else do u expect to find here? reviews of all the new features? I mean.. its not \"news\".. its another rc that needs to be pounded like hell.\n\n(sorry for being harsh.. but i help the way i find best.. and im actually clicking my indexfinger numb and rouge is running hotter then ever from compiling everything at least twice before i claim it to be buggy - even intel's make errors from time to time ;-)"
    author: "kidcat"
  - subject: "Re: bugs.kde.org"
    date: 2002-11-12
    body: "a) If you can't choose rc3, then use rc2. Older numbers doesnt mean that the bugs aren't fixed.\n\nb) Just because it might work, doesn't mean it is the right way to do it. If noone fixes it right away then it is forgotten, if you file a bug report it will stay on the bug list until it is fixed. Alternatively you could write the correct mailinglist, but they will most likely tell you to write a bug report as well.\n\nAnd sorry for me being harsh, but to me these bug reports are just noise here. I could understand it if it was major bugs, but in that case the correct forum would still be the mailinglists."
    author: "Troels"
  - subject: "Re: Kmix bug"
    date: 2002-11-12
    body: "the bug described is #22186 - and it was marked as closed, reason: fixed....\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Kmix bug"
    date: 2002-11-13
    body: "I'm using 3.0.4 and those loud clicks happen on some (local) songs too...\nThey are always the same songs (no the clicks aren't part of the song...), but sometimes the click went away and returned. I hope it is really fixed. And some songs have wrong reported duration.\nOK, OK, i know this isn't the place to submit bugs."
    author: "yuu"
  - subject: "KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "Can anyone put a screenshot in any place of the web? Please, do not put it on that newsgroup, because the servers need autentication and the most of us cannot se the shots.\n\nAbout having a RC4, why do not releasing final now and error correcting packages after that?"
    author: "srs"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "That's like saying: Why not use the car now and fix the brakes later?"
    author: "Paul PARENA van Erk"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "It\u00b4s not the same thing! This RC may be the last and still there will be errors!"
    author: "srs"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "But they said \"several SEVERE bugs in RC2\"..."
    author: "Paul PARENA van Erk"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "It would make KDE no better than Windows. Not the kind of label that you want to get!"
    author: "ac"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "Why do you need to see a screenshot of RC3? These are release candidates - they're fixing bugs, not adding features or changing the UI!\n\nKDE-3.1rc3 looks the same as the KDE-3.1 alpha, beta and other RC releases. We don't need to be cluttering up the network with more and more screenshots depicting that which, for all intents and purposes, is the same desktop."
    author: "Vic"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "> KDE-3.1rc3 looks the same as the KDE-3.1 alpha, beta and other RC releases.\n\nNot exactly right: Crystal icon theme got default in RC2 and RC3 changes some icons of it (like Konqueror navigation buttons) again."
    author: "Anonymous"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "Different icons are not something that warrants updated screenshots. One can just download the new iconset to the release that they're running and see that result.\n\nThere have been no major GUI changes from alpha1 to RC3 and certainly none from RC2 to RC3."
    author: "Vic"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "Also the klipper icon changed, weird that this is still happening at RC level"
    author: "Bastiaan"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "yes, that's certainly worth a screenshot......................... ???"
    author: "Paul PARENA van Erk"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "And the Keramik Color Scheme changed at RC2.  mmmm... kde goodness"
    author: "standsolid"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-12
    body: "http://garaged.homeip.net/snapshots\n\nI have KDE from cvs compiled, last update and compile was last night, and i do it very often, if there is any feature that anyone want in a screenshot just tell me and i'll add it to the list\n\nthere is a script that takes a snapshot of the current desktop view every hour.\n\nsee u there\nG"
    author: "GaRaGeD"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-13
    body: "Thanks a lot Garaged, the shots are very good.\n\nAs it was proved in the above lines, kde rc3 not only MAY but HAS changed from rc2, and, as much of us defends, there is no problem in seeing its ever improved interface. More than that, not everyone has broadband for downloading and even enought time to compile all those programs. And more, Kde team does not work only for getting a better code, but a better usability according with ergonomic rules. This, in the most times, produces a new design (the way things are organized) and makes using kde ever better and better.\n\nAnd, there is no problem if I want to use a wm with a good design, usability, ergonomy as a frontend of a powerfull kernel and tools, that, together forms the GNU Linux.\n\nCongratulations KDE team, this promise to be the best kde ever.\nGracias garaged y asta la vista."
    author: "srs"
  - subject: "Re: KDE RC3 screenshot and RC??"
    date: 2002-11-14
    body: "thanks but... dude! I'm at work!\nA Somewhat Inappropriate Splash Pic Warning might have been issued \n:D"
    author: "spacefiddle"
  - subject: "testing and unstable branches?"
    date: 2002-11-12
    body: "I am not really on the inside track of kde development, but based on what I see above, I was wondering why kde doesnt do what debian does with stable testing and unstable branches?  I mean I dont know how it works with the developers if once a developer is done his/her part of the new version if he/she just starts on the next release changes or if they sit back until the actively developed branch is released before starting development on the next release...is 3.2 development already in full swing?  Would it be advantagous for kde to have development and stable branches like debian...and if not why not?"
    author: "bonito"
  - subject: "Re: testing and unstable branches?"
    date: 2002-11-12
    body: "KDE branches at each major release\n\ne.g. there is a KDE_3_0_BRANCH and a KDE_2_2_BRANCH\n\nThese are used for bugfixes and updated translations. That's how we are up to KDE 3.0.5.\n\nIf stability and security are major concerns, at least wait until 3.1.1 or 3.1.2 before upgrading and use 3.0.5 for now."
    author: "KDozer"
  - subject: "Re: testing and unstable branches?"
    date: 2002-11-12
    body: "Yes, but *after* the release of 3.1 Final will there be any development on KDE_3_0_BRANCH? The point is, a final should be *stable*.\nKDE 2.0.0 was everything but stable, KDE3.0 was much better, usable no doubt but IMO still to buggy to be called stable.\nI find it hard to believe that KDE3.1 will be stable before 3.1.1 or 3.1.2, so why not just name it stable then?\n\nJad"
    author: "Jad"
  - subject: "Re: testing and unstable branches?"
    date: 2002-11-12
    body: "believe it, einstein.  it's very stable. if it's not, it means you're not doing your job of testing the RC.\n\nps why not rename yourself jad beta?  you seem unstable... "
    author: "ac"
  - subject: "Re: testing and unstable branches?"
    date: 2002-11-12
    body: "A 3 phased development model is not very efficient. Also most people would probably wan't to work on the unstable branch as it is there the fun stuff happens. It adds additional administrative overhead. (bug checking, updating the testing tree, testing both the unstable and the testing tree)\n\nIf you are so afraid of stability problems, then don't use .0 releases. This is not KDE specific, but goes with all software. Don't use a kernel .0 release, or a gnome .0 release, or a gcc .0 release, etcetc.\n"
    author: "Troels"
  - subject: "Re: testing and unstable branches?"
    date: 2002-11-13
    body: "Developers are free to create whatever CVS branches they want to develop new features/ideas while still being able to maintain the 3.1 release. I for example made a Kolf branch for some new features I wanted to add.\n\nCheers,\nJason"
    author: "Jason Katz-Brown"
  - subject: "items?"
    date: 2002-11-12
    body: "Is there a list of bugs fixed in this RC?"
    author: "desau"
  - subject: "xfs errors when compiling qt-copy"
    date: 2002-11-12
    body: "When I compile qt-copy with -enable-xft I get the below errors. These are with xft2 libraries and development files installed (though the Debian packages at http://verbum.org/~walters/debian/local/i386/ ). I have been able to compile Mozilla and Phoenix with xft2, so I believe my system is in order.\n\ng++ -fno-exceptions -o ../../../bin/uic .obj/release-shared-mt/main.o\n.obj/release-shared-mt/uic.o .obj/release-shared-mt/form.o\n.obj/release-shared-mt/object.o .obj/release-shared-mt/subclassing.o\n.obj/release-shared-mt/embed.o .obj/release-shared-mt/widgetdatabase.o\n.obj/release-shared-mt/domtool.o .obj/release-shared-mt/parser.o\n-Wl,-rpath,/home/andre/devel/cvs/kde/qt-copy.build/lib\n-L/home/andre/devel/cvs/kde/qt-copy.build/lib -L/usr/X11R6/lib -lqt-mt\n-lpng -lz -lpthread -lSM -lICE -ldl -lXext -lX11 -lm -lXinerama\n-lXrender -lXft -lfreetype\n/home/andre/devel/cvs/kde/qt-copy.build/lib/libqt-mt.so: undefined\nreference to `FcPatternAddInteger'\n/home/andre/devel/cvs/kde/qt-copy.build/lib/libqt-mt.so: undefined\nreference to `FcPatternAddBool'\n/usr/X11R6/lib/libXft.so: undefined reference to `FcPatternGetBool'\n[Snip - Ed.]\n/usr/X11R6/lib/libXft.so: undefined reference to `FcFontMatch'\n/home/andre/devel/cvs/kde/qt-copy.build/lib/libqt-mt.so: undefined\nreference to `FcCharSetDestroy'\n/home/andre/devel/cvs/kde/qt-copy.build/lib/libqt-mt.so: undefined\nreference to `FcFontSetDestroy'\n/usr/X11R6/lib/libXft.so: undefined reference to `FcDefaultSubstitute'\ncollect2: ld returned 1 exit status\n\nAny clues?\n"
    author: "Andr\u00e9"
  - subject: "Re: xfs errors when compiling qt-copy"
    date: 2002-11-12
    body: "Yes, i had this problem too. Make sure that fontconfig and fontconfig-devel are installed.\n\nQt didnt link them in automatically for me, so i had to do a:\n\nfind . -name Makefile | xargs perl -pi -e 's/-lXft/-lXft -lfontconfig'\n\nand then remove libqt-mt.s* and rerun make.\n"
    author: "Troels"
  - subject: "Re: xfs errors when compiling qt-copy"
    date: 2002-11-17
    body: "YES! I am getting this right now using Mandrake Cooker! I dont know how to fix it, have you since your post?"
    author: "Phil Durand"
  - subject: "Re: xfs errors when compiling qt-copy"
    date: 2002-11-17
    body: "I had to add -lfontconfig to the libs section of the makefile of whatever directory that was in. I had those exact same errors."
    author: "chuzwuzza"
  - subject: "Shmooozin!"
    date: 2002-11-13
    body: "KDE 3.1 is shaping up to be the best desktop experience I've ever had on the x86 platform.  Great job guys.  I got the blinking icon issue that 'aligning to grid' didn't fix :("
    author: "ThanatosNL"
  - subject: "Re: Shmooozin!"
    date: 2002-11-13
    body: "the alling to grid only fixed in rc2\nif u want it fixed u can just disable devices on the desktop\n\n/kidcat"
    author: "kidcat"
  - subject: "video"
    date: 2002-11-13
    body: "Does anyone know what flags I have to set to get the newer video features working.  Things like video thumbnails, an embedded video player that actually embeds the video, and using xinelib to play the video.  Am I missing some flags when I compile kdemultimedia, or did this work get delayed to 3.2?"
    author: "theorz"
  - subject: "bugs.kde.org must have a bug of its own.."
    date: 2002-11-13
    body: "Konqueror times out...  \nTraceroute returns !H (host unreachable ICMP message), last hop:\n11  tbr2-p013701.sl9mo.ip.att.net (12.122.10.90)  46.737 ms !H  67.281 ms !H  46.452 ms !H\n\nI'm running Mdk 9.1 Cooker, now on KDE3.1RC3, from the cooker RPMs.  Kept upgraded via URPMI from an original Mdk 8.1 install.\n\nI was going to attempt to check on, and possibly report, the crashes I've been getting in Konqueror...  Often times, it will partially load a page... and freeze solid, no crash report or anything.  KWin won't close it... I have to hotkey XKill, and click the window.\n\nAlso, I haven't been able to load KNewsTicker graphical config in some time.  Clicking the config option does nothing.\n\nThese bugs have continued to persist thru the several RCs, at least.  The Konqueror one is the most frustrating..."
    author: "Duncan"
  - subject: "Re: bugs.kde.org must have a bug of its own.."
    date: 2002-11-13
    body: "bugs.kde.org hasnt really matured after the transision to bugzilla..\ni get wierd square-symbols when i have to pick the severity of the bug... i dont know it this is Konqi that is buggy or it is The Bug that is buggy. \n\nCould some one with kde3.0.x try and make a bugreport and just bail out just before submitting it? If you dont c a *totally* goofed up selection of \"bug type\" let me know so i can file yet another bugreport :)\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: bugs.kde.org must have a bug of its own.."
    date: 2002-11-13
    body: "The \"can't access configuration for this-and-that\" stuff is about 99.99% certain to be MDK packaging bugs -- until the MDK menu stuff is completely updated for the KDE Control Center changes in 3.1, expect those. I'd strongly suggest you switch to using vanilla KDE menus for a while (use menudrake to do that). That's one reason why some people would like testers to just use sources..\n\nAs to Konqueror freezes.. Never saw one myself, so no idea on what's going on.. I suspect none of the Konqui devels have either, or otherwise there would be a bug with a really scary severity rating attached. Nor do I recall anyone reporting a bug like that - and I am subscribed to kde-bugs-dist.. But here is one thing that might be useful: if the problems happens *really often*, go to konsole, run konqueror in gdb (just do gdb konqueror, and then type run<enter> at the prompt).  Then go do some browsing in the window. When it freezes, go back to the konsole, and press ctrl-c. Then type bt. It will tell you where in the code konqui was when you interrupted it.. Then press cont, wait a few seconds, and repeat the deal with ctrl-c. Then save the output, and report a bug. Of course, if you find a way to reproduce the bug consistently, it would help the most in diagnosing and fixing it. (You can also attach gdb to running processes - but I don't know a good way of figuring out which is the *right one*).."
    author: "Sad Eagle"
  - subject: "the new icons..."
    date: 2002-11-13
    body: "Hi.. just my two cent humble opinion.\n\nThe icon for ControlCenter looked much better in rc2.. however i use the Large size panel.. so i dont know if its because it didnt scale well.\n\nThe reason i like the old one better is that it was blue... this one just doesnt fit in with rest.. \n\nAnd while im at it.. the HelpCenter icon is now *screaming* out.. i dont know if this is the idea with it.. but it too spoils the nice crystal-blue drool-o-rama that is everywhere else.. besides Kmail.. the K should be blue too.. not that old kde-2ish orange.\n\nJust my two cent...\n\n/kidcat\n"
    author: "kidcat"
  - subject: "Re: the new icons..."
    date: 2002-11-13
    body: "Surgestion for a new HelpCenter icon:\nA crystalic blue book with [a] yellow (like the \"eye\" of the [K] button) question mark[s] on it.\n\nIt would be really awesome if all the standard icons oh the panel really showed the \"essence\" of the Crystal-SVG icon set.. which is why i allso vote for at \"bluishing\" of the [K] button.. as it is now it has this really neat yellow crystal core.. but the gear around it and the K would (imho) benefit from a \"crystalization\".\n\nOh.. and while im giving it a whack at being an Art Krittique (everyone thinks themself qualified, huh?)... something needs to be done t othe shell ontop the screen of Konsoles icon.. it just doesnt look as \"freindly\" as it should.. and its hard to see that it is endeede a shell.. at first glance it looks like some kind of middle-east head-wear :)\n\n/Le Krittique 'd Art Kidcat\n(lmao)"
    author: "kidcat"
  - subject: "kdebindings"
    date: 2002-11-13
    body: "HI all ..\n\nwhats the matter with the kdebindings at the moment? .. till rc1 i have to patch the makefiles like hell to get it running. In current rc3 dcopc looks really messed up. Somebody else with problems in this 'area' ?\n\nrgds\nmarc'O"
    author: "marco puszina"
  - subject: "Re: kdebindings"
    date: 2002-11-13
    body: "Not to add to ranting, but I've _never_ been able to make kdebindings compile completely.  I have absolutely no problem with any other package of KDE. \n\nThe biggest pain from kdebindings that I get seems to be from the java and smoke bindings... even though I've got java installed and working fine.\n\n"
    author: "desau"
  - subject: "library qt-mt not found?"
    date: 2002-11-14
    body: "what's with this (starting w/arts?  i certainly did compile Qt with thread support and i even tried passing --with-qt-dir=/usr/local/qt to ./configure\n\nchecking for Qt... configure: error: Qt (>= Qt 3.1 (20021021)) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\nMake sure that you have compiled Qt with thread support!\n"
    author: "Need Help Compiling"
  - subject: "Re: library qt-mt not found?"
    date: 2002-11-14
    body: "What tells QTDIR?"
    author: "thefrog"
  - subject: "Re: library qt-mt not found?"
    date: 2002-11-14
    body: "i have it in .bash_profile\n\n# for compiling qt\nQTDIR=/usr/local/qt\nPATH=$QTDIR/bin:$PATH\nMANPATH=$QTDIR/doc/man:$MANPATH\nLD_LIBRARY_PATH=$QTDIR/lib:$LD_LIBRARY_PATH"
    author: "Need Help Compiling"
  - subject: "Re: library qt-mt not found?"
    date: 2002-12-03
    body: "I am getting the same error, I've added this to my bash_profile and tried to ./configure and gmake QT 3.1 again (threaded) and i'm still getting this error, any ideas? I've spent a few days trying to firgure this out and there isn't really much help on this error. anymore ideas? "
    author: "Kevin"
  - subject: "Re: library qt-mt not found?"
    date: 2002-12-04
    body: "I  answered my own question, though i'd share... \n\nwhile also adding the above information to my .bash_profile I used this config option...\n\n./configure --with-qt-dir=/usr/local/qt/ --with-qt-includes=/usr/local/qt/include/ --with-qt-librries=/usr/local/lib/"
    author: "Kevin"
  - subject: "Re: library qt-mt not found?"
    date: 2003-01-19
    body: "Did you get it running?\nI have the same problem.\nDieter"
    author: "Dieter Kraft"
  - subject: "Re: library qt-mt not found?"
    date: 2003-03-10
    body: "If u have already compiled the QT, run make distclean or make confclean, and ./configure -thread\nEnjoy!      Best Regards     Reiser"
    author: "Reiser"
  - subject: "Re: library qt-mt not found?"
    date: 2003-03-10
    body: "If u have already compiled QT, run make distclean or make confclean, and ./configure -thread\nEnjoy!            Best Regards, Reiser\n"
    author: "Reiser"
  - subject: "Re: library qt-mt not found?"
    date: 2003-03-10
    body: "If u already heve compiled the QT, run  make distclean  or  make confclean,\nand   ./configure -thread\nEnjoy!      Best Regards         Reiser"
    author: "Reiser"
  - subject: "Re: library qt-mt not found?"
    date: 2003-05-28
    body: "Hi ,\nI am also getting this error while configuring for Juke music application. I have compiled Qt earlier(without -thread) option. But now i get the following message:\n\nchecking for Qt... configure: error: Qt (>= Qt 3.1 (20021021)) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\nMake sure that you have compiled Qt with thread support!\n\n\nany help on this will be preceous for me.\nregards,\nSrinivas Rao.M"
    author: "Srinivas Rao.M"
  - subject: "Re: library qt-mt not found?"
    date: 2003-03-10
    body: "If u have already compiled the qt, run make cleanconf, or make distclean\nand run again ./configure -thread\nEnjoy           Best Regards    Reiser"
    author: "Reiser"
  - subject: "Re: library qt-mt not found?"
    date: 2003-03-30
    body: "I have compiled QT with thread support, and I still have the same problem. I tried passing --with-qt- flags as well, nothing works. So NO, recompiling it again and again won't solve the problem. Oh, and I use RedHat 7.2. It originally comes with KDE 2.x and QT 2.x. I, however, chose not to install any desktop environments during the installation process. I then downloaded the latest version of XFree86, installed that. Then I built QT 3.1.1. Any ideas?\n\nThanks in advance,\n\nWill"
    author: "William Y."
  - subject: "Re: library qt-mt not found?"
    date: 2005-06-14
    body: "I had a similar problem in my Qt installation (3.3.3)\n\nI found a broken link in the Qt lib directory ('/usr/lib/qt3/lib/' in my distro). My linker found libqt-mt.so, a link which was pointing to an outdated shared library that wasn't there. I did have a shared lib 'libqt-mt.so.3.3.3' in the same dir, but the broken link was sending it to a nonexistent 'libqt-mt.so.3.2.3'. This  probably explains the \"can't find library qt-mt\" error message. So I deleted the stale link and made a new one that pointed to the correct file, and all worked fine! Apparently when they made a new version of Qt they forgot to update the links!\n\nhope this helps!\n\n--ps60k"
    author: "ps60k"
  - subject: "Re: library qt-mt not found?"
    date: 2006-04-04
    body: "I found close to the same\n\nI had /usr/lib/libqt-mt.so.3, but no corresponding link of /usr/lib/libqt-mt.so\n\nso while I had a versioned lib, I had no base, so nothing could find it unless it was looking strictly for so.3"
    author: "Munky"
  - subject: "Re: library qt-mt not found?"
    date: 2007-03-07
    body: "I have found the same problem on Mandriva. I soled it by using prefixes for the QT folder. I had to manually add the KDE folder into the configure file (around line 22733). Then it worked."
    author: "Omer Moussaffi"
  - subject: "Re: library qt-mt not found?"
    date: 2005-07-06
    body: "For slackware just link libqt-mt.so.3.3 to libqt-mt.so\n\nglibc-tls problem"
    author: "skontog"
  - subject: "Re: library qt-mt not found?"
    date: 2002-11-15
    body: "Are you sure that you compiled Qt 3.1 (yes 3.1) and had the right flags (like -thread or something) in it?\n\nTim"
    author: "Tim V"
  - subject: "Re: library qt-mt not found?"
    date: 2002-11-16
    body: "It's not due to missing qt-mt, the test failes for some other reasons, unfortunately I can't remember exactly.Check config.log, I think it was something like missing /lib/cpp\n"
    author: "cme"
  - subject: "Re: library qt-mt not found?"
    date: 2002-11-16
    body: "A clue?\n\n[root@prospero bin]# qtconfig\nqtconfig: relocation error: /usr/lib/libXft.so.2: undefined symbol: FcInit\n\ni do have xft, fontconfig, etc. installed, did use the -xft switch, and it does at least appear to have compiled in.  could this be related?\n"
    author: "Need Help Compiling"
  - subject: "Re: library qt-mt not found?"
    date: 2002-12-28
    body: "I got mine to work with this: ./configure --with-qt-dir=/usr/local/qt --with-qt-includes=/usr/local/qt/include/ --with-qt-libraries=/usr/local/qt/lib/"
    author: "dude"
  - subject: "Re: library qt-mt not found? fixed"
    date: 2003-01-15
    body: "Hi I just fixed that problem:\nThis is two way - longer and shorter :)\nFirst some explanations:\nProblem: qt compiled with different gcc than your project try to. thats all\n\nshorter change your symlink back to gcc-2.95 (or 6),\nand g++ too so\n---------------------\ndcgui-0.2# gcc -v\nReading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs\ngcc version 2.95.4 20011002 (Debian prerelease)\n\ndcgui-0.2# g++ -v\nReading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs\ngcc version 2.95.4 20011002 (Debian prerelease)\n---------------------\nand all be fine.\n\nLonger method \nuse gcc.3 and above to compile qt ( with thread support!!!) and any other libraries needed in project .\n"
    author: "Dofik"
  - subject: "Re: library qt-mt not found? fixed"
    date: 2003-01-22
    body: "That was it! Thanks a lot!\n\n--Khaled"
    author: "Khaled"
  - subject: "Re: library qt-mt not found? fixed"
    date: 2003-02-21
    body: "I know, that it is a stupid question, but how can I compile qt with thread support? Can anybody say me, what should I exactly do?\n\nThanks,\n  joco"
    author: "joco"
  - subject: "Re: library qt-mt not found? fixed"
    date: 2003-02-21
    body: "configure --thread --dont-forget-other-options"
    author: "Anonymous"
  - subject: "Re: library qt-mt not found?"
    date: 2003-05-12
    body: "Hi!\n\nI&#180;ve got the same problem here. I even tried --with-qt-lib ... etc - always te same error :( \n\nI used the qt-configure options used in the howto of kde-compile.\n\nsomeone any ideas?\n\ncu\n"
    author: "lonestar"
  - subject: "Re: library qt-mt not found?"
    date: 2003-11-21
    body: "I have the same problem.  I've compiled with gcc 3 and I still cannot get that damn qt-mt library error to go away. Im a gentoo'er and there isnt anyone over there that seems to know the solution.\n\nAny suggestions would be great.\n"
    author: "Joe"
  - subject: "Re: library qt-mt not found?"
    date: 2004-02-12
    body: "Howdy all\n\nSeems alot of us are getting his - anyone had this on Freebsd ? I am running 4.7-RELEASE. Semi-newbie."
    author: "psyche101"
  - subject: "Re: library qt-mt not found?"
    date: 2004-03-04
    body: "\ni am using redhat 8 and trying to install kopete on my system. i am getting the following error message. i installed qt with the installation of red hat.\nany one knowing the solution....\n\n\"checking for Qt... configure: error: Qt (>= Qt 3.1 (20021021)) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\nMake sure that you have compiled Qt with thread support!\"\n"
    author: "alqama"
  - subject: "Re: library qt-mt not found?"
    date: 2004-03-09
    body: "i've got exactly the same problem, except i'm using redhat 9 and installed qt-3.3.1, as it's required to run kde 3.2 (requires qt >= 3.2); by trying to install kdebase-3.2.0 i get the same error message as above: library qt-mt not found, check your installation and so on...\ni've set all the environemtn variables and compiled qt with thread suport, but it won't work; anyone got an idea?\n\nthanks for all help\n\nkind regards,\n\ncsae"
    author: "csae"
  - subject: "Re: library qt-mt not found?"
    date: 2006-12-17
    body: "Did someone found ?\n\nI have same problem. I did too:\nexport QTDIR=/usr/share/qt3\n\nand apt-get install make build-essential  gcc  gcc-3.4 build-essential g++ \nand so on\n"
    author: "xeratul"
  - subject: "Re: library qt-mt not found?"
    date: 2004-05-24
    body: "I'm using Mandrake 10 CE and had the same problem, (and little bit more grey hair :) !!!! So this meassage is mainly for mandrake users. \nIf you have ALL qt components installed:\n1. Go to : http://urpmi.org/easyurpmi/index.php\n2. Fill ALL boxes!!!! and \"type in a console as root\" ( for newbie write \"su\" and type password)\n\nThen\n\nTry to install ALL gcc components e.g. fortran77, java\nIf have any problems with installation in rpmdrake, try use in console as root  command:  urpmi name.of.package.mdk.rpm\n\nmight be work\n\nGood Luck :)"
    author: "Mr.NEUTRON"
  - subject: "Re: library qt-mt not found?"
    date: 2004-09-02
    body: "Hi,\n\ni had the same problem as you, and found it strange as all packages were installed...\n\nthe problem was in fact the one described above - qt library was compiled with a different gcc than the one used to compile the program.\n\nto solve:\n\nrm /usr/bin/gcc\nrm /usr/bin/g++\n\napt-get install gcc-3.3 g++-3.3\n\nln -s /usr/bin/gcc-3.3 /usr/bin/gcc\nln -s /usr/bin/g++-3.3 /usr/bin/g++\n\n... and it should configure with no errors now\n\nfor the record this was done in debian unstable.\n\nRegards\n\nFilipe"
    author: "Filipe"
  - subject: "Re: library qt-mt not found?"
    date: 2004-10-24
    body: "Sorry dude but that is not the solution for me.\n\nI want to install an application that uses QT, i have QT installed, did it with\napt-get (debian). apt-get compiles it itself. then run the .configure script of the application i want to compile and it says:\n\nchecking for Qt... configure: error: Qt (>= Qt 3.2) (library qt-mt) not found. Please check your installation!\n\nWell i did what you said. but:\ntw:/home/tjerk/downloaded/activeheart-1.2.1# rm /usr/bin/gcc\ntw:/home/tjerk/downloaded/activeheart-1.2.1# rm /usr/bin/g++\ntw:/home/tjerk/downloaded/activeheart-1.2.1# apt-get install gcc-3.3 g++-3.3\nReading Package Lists... Done\nBuilding Dependency Tree... Done\ngcc-3.3 is already the newest version.\ng++-3.3 is already the newest version.\n0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.\ntw:/home/tjerk/downloaded/activeheart-1.2.1# ./configure\n[cut]\nconfigure: error: no acceptable C compiler found in $PATH\n\n\nWell it makes the problem worse for me!!!\n\n\n"
    author: "Tjerk"
  - subject: "Re: library qt-mt not found?"
    date: 2006-05-18
    body: "Yep! for me it was because of the different version of gcc compiler.. after replacing it with version 3.3, symlink it and voila!\n\nthanks again for the workaround provided.. it cures my headache :))"
    author: "Blackwing"
  - subject: "Re: library qt-mt not found?"
    date: 2004-11-01
    body: "i'm with the same problem,i'm using slackware 9.0\nchec\nchecking for kde-config... /opt/kde/bin/kde-config\nchecking where to install... /opt/kde (as returned by kde-config)\nchecking for style of include used by make... GNU\nchecking for gcc... no\nchecking for cc... no\nchecking for cc... no\nchecking for cl... no\nconfigure: error: no acceptable C compiler found in $PATH\nSee `config.log' for more details.\n"
    author: "Fabio"
  - subject: "gcc missing (was: Re: library qt-mt not found?)"
    date: 2004-11-01
    body: "You are missing the compiler (gcc), that is not the same problem."
    author: "Nicolas Goutte"
  - subject: "Re: library qt-mt not found?"
    date: 2004-11-01
    body: "i tried to install the gcc but now is coming another error..\nConfiguring for a i686-pc-linux-gnu host.\nrm: cannot remove `libtool/Makefile': Not a directory\nrm: cannot remove `libtool/Makefile': Not a directory\nCreated \"Makefile\" in /root using \"mt-frag\"\n/root/gcc/gcc-3.2.3/configure: line 7: cc: command not found\n*** The command 'cc -o conftest -g   conftest.c' failed.\n*** You must set the environment variable CC to a working compiler.\n"
    author: "Fabio"
  - subject: "Re: library qt-mt not found?"
    date: 2004-12-21
    body: "I have the same problem and I am using Mandrake 10.1.  I am trying to install kdeveloper, which needs Qt 3.2 or greater to run.  To my knowledge Qt 3.2.3 had installed correctly, but the ./configure for kdeveloper can't find it.  The problem seems to have something to do with QTDIR.  When I set that variable to something different I get a different error.  On one error it tells me it can't find the libraries or headerfiles and the other error tells me it can't find library qt-mt.  Does anyone have any suggestions because I am completely lost."
    author: "Joe-Black"
  - subject: "Re: library qt-mt not found?"
    date: 2004-12-22
    body: "me too"
    author: "rgx112"
  - subject: "Re: library qt-mt not found?"
    date: 2005-01-02
    body: "I had the same problem and i am just posting this incase anyone is still having this problem make sure you use the -thread option when compiling qt and then when you are building a program that requires that version of qt use the --with-qt-dir=/path/to/qt's/root option when you are running ./configure"
    author: "joehacker2004"
  - subject: "Re: library qt-mt not found?"
    date: 2005-03-22
    body: "Someone mentioned upgrading to the lastest k3b.\n\nI confirm that compiling the latest k3b (0.11.22 in this case) worked for me on Ubuntu. Of course i had to install a bunch of additional QT and KDE stuff, and had to pass QT libs with \n\n./configure --with-qt-dir=/usr/share/qt/ --with-qt-libraries=/usr/share/qt3/lib/ --with-qt-includes=/usr/share/qt3/include/\n\nbut things are running now and k3b is recognizing my cd writer as writer.\n\nHope this helps."
    author: "Anonymous Coward"
  - subject: "Re: library qt-mt not found?"
    date: 2005-03-25
    body: "hi,\ni faced the same problem on my Gentoo-Linux while upgrading kdelibs.\ni have just recomplied QT & now i am able to upgrade my kdelibs too.\ni am not sure of the sequence, but i have defility upgraded gcc & glibc a few days back.\ncan someone though some more light to this mistry as to why this is required ?\ni was having impression of updating glibc/gcc is upwars compatible etc.. or is it some new features in these packages---have to find out :-)\n\nhope the above info helps...\n"
    author: "Arindam Haldar"
  - subject: "Re: library qt-mt not found?"
    date: 2005-08-29
    body: "# export QTLIB=\"/usr/share/qt3\"\n# ldconfig\n# ./configure\n\nthis should do it, i had the same problem and it worked for me"
    author: "Tim"
  - subject: "nspluginviewer is too slow!"
    date: 2002-11-14
    body: "And consume too much resource. I am wondering if there are some workaround. I can't bear the performance watching flash in konqueror using nspluginviewer."
    author: "idlecat"
  - subject: "I will approve a resolution for a new RC!"
    date: 2002-11-15
    body: "I just want to join my voice to those who want another release candidate.  I think the KDE team should not release a product with a lot of bugs (even those minor but annoying ones...) just because there was a deadline for it fixed months ago, like in some commercial development.\n\nI also think that some work should be done to increase the speed of KDE.  How can I prove that Linux is faster and more efficient than Windows if its best user interface is slower?"
    author: "Dominic Jacques"
  - subject: "Re: I will approve a resolution for a new RC!"
    date: 2002-11-15
    body: "Try compiling everything from source with gentoo and CFLAGS=\"-O3 mcpu=athlon-xp\"\nI'm not complaining right now :)\nRunning X-4.2.99.3, kde-3.1_rc3, gnome-2.1.2 :))) at amazing speed :)"
    author: "dwt"
  - subject: "Re: I will approve a resolution for a new RC!"
    date: 2002-11-16
    body: "Already done.\n\nI'm using Gentoo 1.4 on a PIII 450Mhz.  I know it's not a really fast machine for today's standards but I run Windows 2K on the same machine and it's faster.  However, I must say that GCC 3.2 and the patches for a preemptive kernel helped a lot to reduce the gap.  Maybe the next thing to do is to try to compile KDE with the fast malloc implementation."
    author: "Dominic Jacques"
  - subject: "Re: I will approve a resolution for a new RC!"
    date: 2002-11-16
    body: "I guess they don't tell you this when you use Gentoo, but any LFS'er around here (just ask kidcat) knows that -O3 is bad news--it makes the bins rather large, and the speed kill from the large bin sitting in memory is worse than the performance gains.  Stick with -O2, and be truly 'leet' :)\n<p>Still, I trust a Gentoo user compiling stuff more than a Mandrake user around here :)  No offense to Mandrake users of course."
    author: "ThanatosNL"
  - subject: "Re: I will approve a resolution for a new RC!"
    date: 2002-11-18
    body: "sorry ThanatosNL.. *but*... unless ppl are so fortunate to have some spiffy scsi raid the -Os is *still* the king of opts when it comes to kde.. i have now reached the point where my KDE speedwise saws down windows to the point where it becomes directly amusing :) I will publish a letter or an article about optimizing linux for workstation use in the near future (when i have bitch-slapped glibc to behave the way i want) that will focus on first responsivenes and seccond runtime.\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: I will approve a resolution for a new RC!"
    date: 2002-11-19
    body: "Shweet...next time I compile KDE I'll try -Os.  thx ;)"
    author: "ThanatosNL"
  - subject: "Re: I will approve a resolution for a new RC!"
    date: 2002-11-22
    body: "hey hey now\n\nI am a refugee LFS user.  I did the whole system I did it all. I'm running a 1.2 ghz athlon system\n\nI then switched to CRUX because the speed over SuSE --> LFE was very minimal.  I didn't like compiling a proggie whenever i downloaded it.  I wanted binaries.\n\nCRUX was _great_, except binaries were for the core install.  \n\nI set my g/f up with a computer and installed Mandrake 9 for her.   I was impressed with the rpm problems fixed (urpmi), and the compilation flags i used in LFS were probably apllied (Along with being compiled with GCC 3.1 and a newer glib than mdk8.2) for the speed increase.\n\nI am now using Mandrake (considering Gentoo)  and am happy.  I am a l337 linux user (i.e. I don't use draktools to edit my configuration.  vi for me thx).  I love having binary packages, and the speed isn't all that much better.  I know what compiling is, and I am a mandrake user.  my two cents  "
    author: "standsolid"
  - subject: "cant find libstdc++"
    date: 2002-11-15
    body: "checking if STL implementation is SGI like... no\nchecking if STL implementation is HP like... no\nconfigure: error: \"no known STL type found - did you forget to install libstdc++-devel ?\" \n..tho i have installed\nii  libstdc++5-dev  3.2.1-0pre6 The GNU stdc++ library version\n\nfrom KDELIBS-rc3 (running Debian Sid)\n\nis KDE3.1rc3 compatible with 3.2.1-pre6?"
    author: "help"
  - subject: "Re: cant find libstdc++"
    date: 2002-11-15
    body: "Checking for Qt... configure: error: Qt (>= Qt 3.1 (20021021)) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\nMake sure that you have compiled Qt with thread support\n\n./configure --prefix=/opt/kde/ --with-alsa --with-qt-dir=/opt/qt --with-qt-includes=/opt/qt/include/ --with-qt-libraries=/opt/qt/lib/\ndeclare -x QTDIR=\"/opt/qt\"\n\nQT 3.1 was installed into /opt/qt"
    author: "help"
  - subject: "Re: cant find libstdc++"
    date: 2002-11-15
    body: "configure:25214: checking for Qt\nconfigure: 25283: /opt/qt/include//qstyle.h\ntaking that configure:25391: rm -rf SunWS_cache; g++ -o conftest -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -fno-exceptions -fno-check-new -I/opt/qt/include/ -I/usr/X11R6/include  -DQT_THREAD_SUPPORT  -D_REENTRANT  -L/opt/qt/lib/ -L/usr/X11R6/lib   conftest.cc  -lqt-mt -lpng -lz -lm -ljpeg -ldl  -lXext -lX11 -lSM -lICE  -lpthread 1>&5/tmp/ccF8fRpF.o(.text+0xb): In function `main':\n: undefined reference to `QString::null'\n/tmp/ccF8fRpF.o(.text+0x10): In function `main':\n: undefined reference to `QStyleFactory::create(QString const &)'\n/tmp/ccF8fRpF.o(.text+0x1e): In function `main':\n: undefined reference to `QCursor::QCursor(int)'\n/tmp/ccF8fRpF.o(.text+0x29): In function `main':\n: undefined reference to `QCursor::~QCursor(void)'\n/tmp/ccF8fRpF.o(.QValueListPrivate<QString>::gnu.linkonce.t.(void)+0x21): In function `QValueListPrivate<QString>::QValueListPrivate(void)':\n/tmp/ccF8fRpF.o(.QValueListPrivate<QString>::gnu.linkonce.t.(void)+0x2a): In function `QValueListPrivate<QString>::QValueListPrivate(void)':\n: undefined reference to `QString::makeSharedNull(void)'\n/tmp/ccF8fRpF.o(.gnu.linkonce.t._._t17QValueListPrivate1Z7QString+0x27): In function `QValueListPrivate<QString>::~QValueListPrivate(void)':\n: undefined reference to `QString::shared_null'\n/tmp/ccF8fRpF.o(.gnu.linkonce.t._._t17QValueListPrivate1Z7QString+0x32): In function `QValueListPrivate<QString>::~QValueListPrivate(void)':\n: undefined reference to `QStringData::deleteSelf(void)'\n/tmp/ccF8fRpF.o(.gnu.linkonce.t._._t17QValueListPrivate1Z7QString+0x5f): In function `QValueListPrivate<QString>::~QValueListPrivate(void)':\n: undefined reference to `QString::shared_null'\n/tmp/ccF8fRpF.o(.gnu.linkonce.t._._t17QValueListPrivate1Z7QString+0x6a): In function `QValueListPrivate<QString>::~QValueListPrivate(void)':\n: undefined reference to `QStringData::deleteSelf(void)'\n/tmp/ccF8fRpF.o(.QValueListPrivate<QString>::gnu.linkonce.t.insert(QValueListIte\nrator<QString>, QString const &)+0x27): In function `QValueListPrivate<QString>:\n:insert(QValueListIterator<QString>, QString const &)':\n: undefined reference to `QString::QString(QString const &)'\n undefined reference to `QString::makeSharedNull(void)'\ncollect2: ld returned 1 exit status\nconfigure:25394: $? = 1\nconfigure: failed program was:\n#include \"confdefs.h\"\n#include <qglobal.h>\n#include <qapplication.h>\n#include <qcursor.h>\n#include <qstylefactory.h>\n#include <private/qucomextra_p.h>\n#if ! (QT_VERSION >= 0x030100)\n#error 1\n#endif\n\nint main() {\n    (void)QStyleFactory::create(QString::null);\n    QCursor c(Qt::WhatsThisCursor);\n    return 0;\n}\nconfigure:25434: error: Qt (>= Qt 3.1 (20021021)) (library qt-mt) not found. Please check your installation!\nFor more details about this problem, look at the end of config.log.\nMake sure that you have compiled Qt with thread support!\n\nthis is QT3.1 (installed with thread support)"
    author: "help"
  - subject: "Re: cant find libstdc++"
    date: 2002-12-30
    body: "Did you find what the problem was? Cause the same thing happens to me and I do not know what's wrong ?"
    author: "Francis Bouchard"
  - subject: "Re: cant find libstdc++"
    date: 2002-12-30
    body: "One thing to check: make sure the compile flags you may be passing to g++ through CXXFLAGS are OK (speaking from experience, it's pretty easy to make a typo in -march or -mcpu values). autoconf is pretty stupid, and to it a compile test failing because it failed or because of a typo in compile flags look the same. One tell-tale symptom of that is the test \"Checking whether the C++ compiler is GNU C++\" (or such) returning \"no\" even if you're using g++. \n"
    author: "Sad Eagle"
  - subject: "Re: cant find libstdc++"
    date: 2003-03-22
    body: "Hopefully, you got this fixed by now, but I'm putting this in because I just hit the problem myself and wanted to share the solution and have it archived on the web somewhere:\n\nFor me (upgrading to Slackware 9.0), I didn't have the kernel headers installed.  I assumed installing a new kernel from source would put new header files in /usr/include/linux, but it didn't, at least not as part of make bzlilo.  Once I installed the kernel-headers package, it worked perfectly.  Note that other compiles failed as well before the headers were installed, including Perl modules.\n\nSo, there's something to check.\n\n--RJ"
    author: "RJ Marquette"
  - subject: "Re: cant find libstdc++"
    date: 2004-01-20
    body: "For gentoo:\nemerge STLport"
    author: "David"
  - subject: "Re: cant find libstdc++"
    date: 2004-01-20
    body: "Sorry, but this doesn't fix the problem.\n"
    author: "Ralph"
  - subject: "Re: cant find libstdc++"
    date: 2004-01-21
    body: "Yay, emerge -u world works again on my Gentoo install!\nThanks, you are a thug."
    author: "Kevin"
  - subject: "Re: cant find libstdc++"
    date: 2004-01-21
    body: "sorry about the double post... and it doesn't fix it in gentoo. crap."
    author: "Kevin"
  - subject: "Re: cant find libstdc++"
    date: 2004-01-21
    body: "According to \n\nhttp://bugs.gentoo.org/show_bug.cgi?id=38634\n\ngentoo users can re-emerge gcc and that should fix things"
    author: "Kevin"
  - subject: "Re: cant find libstdc++"
    date: 2006-11-15
    body: "re emerge gcc\nthen run gcc-config -f "
    author: "Terrence"
  - subject: "Can't find [xft] fonts from kcontrol in RC2, RC3"
    date: 2002-11-16
    body: "All [xft] fonts such as Bitstream [xft] don't appear in font list so that I can't choose these fonts to costomize my desktop.  It did appear in previous version of KDE - 3.0.1, 3.0.3 and work fine.  I get all packages compiled successfully, but I don't know how to check whether there is problem in QT3.1.  And font anti-aliasing works well.\n\nCould anyone help me, please?\n\nOS: LFS4.0\nKDE3.1RC2\ngcc3.2\n"
    author: "yellowfish"
  - subject: "Re: Can't find [xft] fonts from kcontrol in RC2, RC3"
    date: 2002-11-16
    body: "I've noticed the same thing with RC2, exept my font of choice is Verdana.  I was just getting comfortable with trusting kde to xset everything too!  I'm waiting for 3.1 to stabilize, because my work depends on the stability of my box.  That's one of the reasons I use KDE on Linux.  I wonder if specifying FontPath  \"/usr/X11R6/lib/X11/fonts/truetype/\" would fix it? (I haven't tried it myself, because I'm marginally content with 3.0.4)"
    author: "Nick"
  - subject: "When Oh When will RC4 and Hence KDE 3.1 be ready"
    date: 2002-11-16
    body: "\nThey went thru the Alpah and Beta stages of LM 9 really quick But, Now they seem to be \"hung on RC3\".  And now especially that KDE 3.1 looks so spiffy. \n\nI don't have the stomach or the intestine to spare to keep trying to download and compile KDE stuff by hand. I'll leave it up to the PRO'z\n\nScott M. Yallen"
    author: "Scotty Yallen"
  - subject: "Re: Can't find [xft] fonts from kcontrol in RC2, RC3"
    date: 2002-11-16
    body: "I am also running LFS 4.0, GCC 3.2 but KDE 3.1 RC3 ... I don't think it is KDE issue (but I haven't checked) .. .in /etc/X11/XF86config-4 there should be a FontPath to the folder of your fonts... remember to have a fonts.path or create it with ttmkfont ... anyway, look at beyound.lfs just before X (Freetype something)..."
    author: "midnight_runner"
  - subject: "Re: Can't find [xft] fonts from kcontrol in RC2, RC3"
    date: 2002-11-17
    body: "I've specified /usr/X11R6/lib/X11/fonts/TTF in XF86config-4.  Where I added Bitstream Cyberbit Unicode and some other TrueType fonts.  The fonts.dir and fonts.scale were generated according to beyond.lfs (ttmkfont somethings).  It works well - Bitstream Cyberbit and Bitstream Cyberbit [xft] are available - in KDE3.0.3 and KDE3.0.4, and also KDE3.1beta2.  But for KDE3.1RC3, ONLY Bitstream Cyberbit (without [xft]) is available.  Unlike [xft] partner, Bitstream Cyberbit fonts are DOUBLE spaced which look unnatural.  \n\nFreetype version 2.1.2."
    author: "yellowfish"
  - subject: "Looking damn good!"
    date: 2002-11-17
    body: "I just compiled kdelibs and kdebase and installed them on my Slackware 8.1 system (building the rest right now). I was using KDE 3.1beta1 before this, and I must say that the changes from beta1 to RC3 are impressive. Everything looks better, feels faster and has an overall more 'polished' feel.\nThis is going to be an impressive release!"
    author: "Jesper Juhl"
  - subject: "Whats with KDE 3.1"
    date: 2002-11-21
    body: "Where is 3.1? There are no news whatsoever after the rc3 release. Is there going to be a rc4, cause there was a /pub/kde/unstable/kde-3.1-rc4/ on the ftp for a short time. Does anybody know anything."
    author: "Adrian"
  - subject: "Re: Whats with KDE 3.1"
    date: 2002-11-21
    body: "found something, look here\nhttp://lists.kde.org/?l=kde-devel&m=103767975826039&w=2"
    author: "Adrian"
  - subject: "Re: Whats with KDE 3.1"
    date: 2002-11-24
    body: "MMM...I think we'll know something new on Monday :|"
    author: "Giovanni"
  - subject: "Re: Whats with KDE 3.1"
    date: 2002-11-26
    body: "If theres nothing gonna be tomorrow I'll be sadly dissapointed :("
    author: "Adrian"
  - subject: "Re: Whats with KDE 3.1"
    date: 2002-11-26
    body: "http://lists.kde.org/?l=kde-devel&m=103824447218726&w=2"
    author: "Anonymous"
  - subject: "Where's KDE 3.1??? "
    date: 2002-12-05
    body: "Any news about KDE 3.1???? Does someone knows anything about this? why the silence???"
    author: "srs"
  - subject: "Re: Where's KDE 3.1??? "
    date: 2002-12-06
    body: "List:     kde-core-devel\nSubject:  KDE 3.1: delayed\nFrom:     Dirk Mueller <mueller@kde.org>\nDate:     2002-12-05 23:44:24\n[Download message RAW]\n\nHi, \n\nThe KDE 3.1 release has to be delayed further. Here is why. \n\nOn November 26th, we've been notified by FozZy from the \"Hackademy \nAudit Project\" about security problems in KDE. They can, after user \ninteraction, cause unwanted execution of commands with the\nprivileges of the user who runs KDE. We fixed those on the same day and \nupdated the \"hopefully final\" KDE 3.1 tarballs. Unfortunately, it was \nbecoming clear after a quick search in the KDE CVS that the \nproblematic code is repeated in many places and in many variations. \n\nYesterday, on the targetted announcement date of KDE 3.1, Waldo and I \nrealized that while we only had audited maybe 30% of the code yet, we have \nfound enough occasions for them to be a big showstopper.\n\nA short query on the packagers mailinglist showed that for the majority \nthere is no big pressure on having a KDE 3.1 to be released \naccording to the schedule. I'm considering a 3.1 with known security bugs a \nno-go anyway, even though we first thought that those are minor that the fix \ncan wait for 3.1.1, I no longer think that this is the case.\n\nWaldo, George, Lubos and I think that we can finish the audit by middle/end\nof next week. This however brings us in a bad position: its unlikely that we\nget many binary packages so short before christmas holidays, which means \nthat KDE 3.1 would go out, if released this year, probably with few or \nnone binary packages at the announcement date. \n\nSo, to sum up, we have two options:\n\na) Try to finish ASAP and try to get it out before christmas. December\n   12 could be a good tagging date.\n\nb) Take the time and schedule for a release next year. Something around\n   January 8, 2003 sounds like a good candidate (tagging date,\n   announcement rougly a week later)\n\nI neither like any of them, but I prefer to go with b), as it also allows\nfor other bugs which have been reported to be fixed. For an impression just \nhave a look at the lately steadily rising open bug count on \nhttp://bugs.kde.org/. \n\nIn any way I'll tar up and release the current 3_1_BRANCH as 3.1RC5 in a few \nhours. Many fixes for the above mentioned security problems are in there, \nbut there are still big chunks of code and patches pending for review. There \nwill be no binary packages as those which were made during the last week \nrefer to be \"KDE 3.1 final\" and are anyway not up to date. \n\nAs soon as the code review is finished we will have to release updates for \nKDE 3.0.x (and at least patches for KDE 2.x) anyway. \n\nComments, opinions, suggestions, flames welcome. \n\n\nDirk"
    author: "TimL"
  - subject: "Flashing Icon Thingy"
    date: 2002-11-25
    body: "This may be out-of-date information, however;\n\nI found out over the weekend that to stop the blinking icons was to turn off the 'hardware icons' on the desktop.  Sorry for being so vague, Im remembering from memory.\n\nIt must be something that keeps checking mounted and unmounted devices and causing a refresh of the desktop\n\nHope this helps.\n\nFor the record, KDE 3.1 RC3 is the most impressive desktop environment.  I was a Gnome 2.x person until Id seen KDE. Well Done! BTW, Im using KDE in a Linux From Scratch environment and was surprised how easy it was to install compared to Gnome.\n\nDan"
    author: "Dan Scannell"
  - subject: "Icon smoothing"
    date: 2002-11-25
    body: "What happened to Icon smoothing?\nI cant seem to enable iconsmoothing, any idea why?\nAll the icons have real ugly edges.."
    author: "Jarl E. Gjessing"
  - subject: "Re: talking about smoothing"
    date: 2002-11-26
    body: "Hi guys. I was thinking in two things and wanna know your oppinion about that. The first, is about the kde screensaver. It's a good one, with good modules, but how about putting in the Screensaver module of kde control panel the option for running XScreenSaver? At least running it, but the better would be constructing a kde tool for manipulating the modules and it's configs. This includes putting the XScreenSaver to lock the kde screen also, pressing the lock icon. \n\nThe second idea (the most important) is: who has already used XScreenSaver, has seen that it has a option for fadding the screen. The effect would be very nice in KDE 3.1. For example. When the user is going to logout, the screen could be faded to black staying only the kde confirm window visible, and the rest with black 80% or 90% (dark gray). If the user confirms, the screen may be fadding from black before kdm is show. In the same way, when the user logs in via kdm, the window may fade to black untill ksplash gets in action.\n\nThere is a lot of places where the fade effect would be very nice without confusing the user or spending user time.\n\nI believe that for the changes that kde team is proposing for 3.1, this fade option would be the crown. :)"
    author: "srs"
  - subject: "Boy is Keramik uggly!"
    date: 2002-11-29
    body: "I just gave the new RC a spin. I was able to stand it for about 10 minutes before everything in me cried for the old look and feel of Kde3.0. \n\nWhoever thought of making Keramik the default style? \n\nI haven't seen something as uggly in my life (not even the grotty first version of Windows was that bad). I have been using KDE beginning at pre 1.0 state and I am truly appaled by the lack of sophistication the keramik style puts forward. It simply looks as if some child was given a set of crayons and the result was integrated. \n\nI have been recommending and installing KDE based desktops for friends and relatives and I can't honestly recommend using the new style as this is unbearable for anyone older than 10-12 years of age. I sincerely hope the old icon style (which put forward a very good, sleek, professional and consistent look) will not be abandoned some day. \n\nThe same holds for the gradient overloaded controls which are a sore to my eyes. There is a very old saying in the graphics desing profession: \"If the designer can't find anything innovative he'll use a gradient.\" The latest styles I have seen were a mess in this regard and while there are times when a gradient may be pleasing to the eye the new style is way over the top.\n\nregards\nCharlyw (back on the old 3.0 icon set!)"
    author: "Charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-29
    body: "Waow\n\nI feel less alone now.\n\nI too HATE keramik and crystal. I really hope someone will decide to revert to good old KDE default style/icon theme.\n\nCurrently, I'm using the KDE default style (not highcolor, plain default) with RedHat bluecurve's icons. That's flat, clean and nice. I couldn't imagine me using the keramik/crystal combination for more than 5 minutes without reverting to something \"simpler\".\n\nNo offence to everaldo and quertz (I think he's keramik's artist). Your style/icons are nice. Yes, they are. But they don't fit for a desktop. I mean that I like to see keramik/crystal screenshots but I hate to have them on my desktop. I like GNOME artwork much better for a real desktop."
    author: "Julien Olivier"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-29
    body: "So I'm not the only one... \n\nI just realized that the icon set has got an own name: crystal. Hope this will NOT be the default for the release as the first impression really counts. And the SVG-icons really cost quite some time to show (even on a machine as fast as mine at 2Ghz I find that they slow things down). \n\nThe toolbar-icons (are those icons there members of the icon set or do they belong to keramik?) are a usability nightmare as well, as they are much too colourfull to be of any use. Didn't someone remember that 10% of all male computer users are colour blind (red green astigmatism)? These people (I'm lucky but I know some people who suffer from that) can't distinguish red from green and thus only recognize some things (such as a stop sign) by their form or alignment (red is always on top of the traffic lights), and a lot of icons only look right in their intended colour. IIRC (I switched very fast to the classical icon theme and style) there are even ones that you can only assign a funtion to by looking at the colour (really really bad!).\n\nIMHO it is really a bad sign for the general public that a subrelease of KDE is looking totally different (as it comes; of course you can revert). First impressions make of break the acceptance of any new software. That's why KDE's similarities and GNOME's dissimliarities from windows drove people to the use of the more familiar one. On Windows XP (have to use it at times) my first thing of customizing is disabling the \"design service\" which makes it revert to the w2k look and feel as XP's luna is a nightmare in itself again.\n\nregards\nCharlyw"
    author: "Charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-29
    body: ">> Hope this will NOT be the default for the release as the first impression really counts.\n\nSadly, it will be the default one.\n\nIt might be too late now to change it. Anyways, each time you complain about keramik/crystal being too colorful, too stylish or just ugly, there's always a KDE developer to tell you that you're the minority so you have to shut your mouth... very sad.\n\nThe problem with keramik/crystal, IMO, is that most users have only seen them on screenshots. When they'll have used them for a while, they'll want to go back to a cleaner style but that will be too late..."
    author: "Julien Olivier"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-29
    body: "They should have had a poll about it. I don't think that there would have been enough support for this uggly duckling to warrant making it default.\n\nregards\nCharlyw"
    author: "charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-29
    body: "Most people like me think it's awesome.  Most people are fed up hicolor default.  This has been confirmed at shows, etc."
    author: "ac"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-30
    body: "If you want to say what you like the vote on kde.look.org, ofcourse the downside is that you can't vote for the old stuff, only the new stuff."
    author: "Anne Observer"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-11-30
    body: "Really, I can't see the problem!\nHonestly, I like Keramik and Crystal, but, if you dont't you can always change, there's no need to worry I think!\nKeramik is the default style, but people aren't so stupid (I hope) to not be able to change it.\nSee you :)"
    author: "Giovanni"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-01
    body: "The Problem ist, that I showed this to some friends of mine that use computers on a irregular basis and they thought I was mocking them. They are supposed to get a computer set up by me and now I have the problem of convincing them all over again that KDE is something to be taken seriously (and useable!) because crystal looks like lego blocks (age 1-8 recommended) and the keramik bit clouds usability under a thick thick layer of gradients and misconceptions. \n\nregards\nCharly"
    author: "Charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-01
    body: "Show your friends kpersonalizer and their jaw will drop."
    author: "ac"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-01
    body: "They won't come that far. Heck, I won't because the default style could drive off a blind donkey. In my experience (>20 years working on computers of all sorts and sizes) the first impression counts and the average user will turn away in absolute disgust from the formerly brilliant KDE and never discover a way of turning the uggly duckling it now is into something useable again.\n\nregards\nCharlyw"
    author: "charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-01
    body: "> They won't come that far. \n\nFunny, because kpersonalizer is the first KDE program started for a new KDE user. \n\n> Heck, I won't because the default style could drive off a blind donkey.\n\nYou sound like a troll, no you're apparently one. We all now know your opinion, stop repeating yourself over and over again refering to your supposed super computer competence.\t"
    author: "Anonymous"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-03
    body: "You're insane.  I can't get enough of Keramic/Crystal.  It's about time KDE did something modern.  The default KDE 3.0 look (and Redhat's Bluecurve) is about Windows 95 quality.  Boring, boxy, drab.  Very ugly, seven years behind the times.  Everyone I've shown Keramic/Crystal to drools over it and can't wait to get it on their own desktops.  With Keramic/Crystal, it looks to me like KDE has finished copying Windows design and moved on to copying OSX, which is a very, very good thing.  I find myself staring at my desktop from the couch because it's the best looking desktop I've seen on an X86-based pc to date.\n\nMaybe it's an age thing.  You sound like you're pretty old.\n"
    author: "Maq"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-03
    body: "Well I have seen more elaborate designs fail and everything is more elaborate than gradients used all over the place as it is in keramik - gradients are a bad designers resort if they don't have a new idea; which they being bad never have. And fail for reasons. I was hoping that KDE wasn't going the way of the dodo but now it seems it will. Not that it matters but this is the third design change in short succession. How do you expect people will react to that? It would have been ok to have a new default design in KDE 4.0 with the new keramik in as a choice to see if it succeeds. If (let's say half a year after it has become available as an option) a vote was taken what to make the default but simply switching is sending the wrong impression: Let's tinker here, tinker there sometime we'll get it right sometimes we won't but does it matter? No, so what the heck let's have black text on a black background because it's cool.\n\nAbout the age thing: Just turned 38. And I do like clean and crisp but not overloaded designs. Luna, OS-X and now keramik are really some sort of an misconception. It's not a thing of usability if gradients and special effects make finding the controls some sort of hide and seek, hampered by sluggishness on anything slower than next years computer."
    author: "Charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: ">>>gradients are a bad designers resort if they don't have a new idea; which they being bad never have.\n \nPlease provide the readers of this topic a link showing some of your 'correct' interface designs, just to back your trolling. You sound like a 15 year boy to me."
    author: "IR"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: "Well I didn't yet have had the need to design an user interface as the designs I used weren't that bad. I on the other hand have a lot of experience with graphic design and people working in the trade. They all agree on one point and that's the cited poor man's design rule: \"If all else fails take a gradient to draw the attention of the customer.\" \n\nThe problem with keramik is that with all these gradients in and around any control it becomes hard to use as the difference between background and foreground get's obscured. The gradient that surrounds the important bit of that control draws the users attention to itself. Instead of presenting the information in the foreground keramik manages to present itself instead. The background should by all means NEVER be more eye catching than the foreground. It's the same bad design as having a word break larger than the line break in text formatting. Why's that: if the word break is larger than the line break the reading rythm of the reader is broken by the tendency to follow the text standing closer, which in this case is in the next line instead of the next word. \n\nIn UI design this is equivalent to have something like keramik which draws the attention of the user from the foreground (which is important) to the background (which should only guide the user from control to control). This need leads to backgrounds and control designs that might in the first moment look boring but being boring they serve a purpose. By trying to be visually interesting keramik defeats it's main purpose!\n\nregards\nCharlyw\n\nP.S.: I resent the fact that I am supposed to be trolling. I am trying to make a case for the old tried and tested design (which wasn't that old and boring to begin with) that was nilly willy replaced by the latest fad driven design. The one thing I resent the most is that the people who are using KDE on a daily bass (as opposed to the ones developing it) were never given the chance to voice their preferences. Now new KDE users will be presented with an IMHO unbearable design and they will turn away from it because the normal user hasn't got enough perseverance to change from any default. Normal users have an attention span that lasts from noon to midday and manuals are for loosers.\n\nIt happens easily that a developer is drawn to foregone conclusions if he is involved in a project. \n\nI have been developing software for more than 20 years now and have caught myself more than once being convinced that what I was doing was the only way to do it and that this new direction was the best there ever was, when in fact I was utterly and completely wrong. Admitting ones mistakes is a hard thing to do.\n"
    author: "Charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: "If I really like the design (and I am not the only one), do I need to care about your technical explanation why it is a bad design?"
    author: "IR"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: "You don't have to care but you should understand that there are people who have a different opinion. One thing you should do though is trying that if critics voice their opinion you can't simply dismiss them as trolls. Only the original designers could do that but if they do I really hope they can deliver a technical explanation why they think their design is good. And \"it's cool\" doesn't count here.\n\nregards\nCharlyw"
    author: "charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: ">>>Well I didn't yet have had the need to design an user interface as the designs I used weren't that bad. I on the other hand have a lot of experience with graphic design and people working in the trade.\n\nFollowing this logic, untill you show me something I like better than Kermik (and improving on the old to my taste) created by yourself, Im sticking with Keramik and consider you a mouth full! Sorry for the namecalling, but your writing style seems not as 'professional' as your invisible 'design skills'.\n\n>>>P.S.: I am trying to make a case for the old tried and tested design (which wasn't that old and boring to begin with)\n\nLook this is how you make your 'case':\n- I haven't seen something as uggly in my life (not even the grotty first version of Windows was that bad).\n\n- It simply looks as if some child was given a set of crayons and the result was integrated. \n\n- I can't honestly recommend using the new style as this is unbearable for anyone older than 10-12 years of age.\n\n- On Windows XP (have to use it at times) my first thing of customizing is disabling the \"design service\" which makes it revert to the w2k look and feel as XP's luna is a nightmare in itself again.\n\n- now I have the problem of convincing them all over again that KDE is something to be taken seriously (and useable!) because crystal looks like lego blocks (age 1-8 recommended)\n\n- They won't come that far. Heck, I won't because the default style could drive off a blind donkey.\n\n- the average user will turn away in absolute disgust from the formerly brilliant KDE\n\n\nThis makes people like me curious to your elite alternative design skills! I cant wait till you give some links so I can publicly call your designs blind donkey lego blocks.\n\n"
    author: "IR"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: "Hello,\n\nyour comments are truely up to the mark of someone who hasn't got a clue what I am talking about. In my first post I was voicing my disgust at something that is thrown at new and old users of KDE as if it were the salvation from M$. That was an emotional (as intended) account of what I and people I know think of the new design. Then you accused me of being a troll.  in reply gave you a technical explanation of what is wrong. I obviously lost you here completely. But to sum up (in simpler terms) what is wrong with keramik:\n\n- important information is hidden by background design.\n- slow even on modern computers.\n- second new design in short succession. This either means that the first new design (KDE-3.0 look) wasn't that good either or that there is no control over the developers. Neither is a good sign to the normal public.\n\nI never would have objected if keramik would have been an optional component for the lifetime of KDE 3.1 destined to become default in 3.2 because that would have encouraged comments on the design and would have given people like me the cance to comment and avoid the obvious mistakes.\n\nPlease refrain from namecalling as this really is something completely unnecessary. I didn't call the developer names and neither should you call me as a critic names just because my opinion doesn't match your's. And as to the design: please show me your superior design that proves that you are competent calling me names in the first place.\n\nregards\nCharlyw"
    author: "charlyw"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-05
    body: "What are *you* talking about? There was never a Keramik for KDE3.0; the \"KDE-3.0 look\" was just a slight re-adjustment of KDE2.x look.\n\nThe performance of Keramik has been studied carefully, and the code has been optimized a lot. Is it slower than HC? Yes. Is it slower enough to be noticeable on even an extraordinarily low-end machine? No. Tre are some initial setup costs on network links; but beyond that the drawing speed is good even on remote links. \n\nAnd your stuff about gradients is hillarious -- the KDE2 look after all is virtually entirely gradients. And of course, any continuous transition of colors is technically a gradient.\n"
    author: "Sad Eagle"
  - subject: "Re: Boy is Keramik uggly!"
    date: 2002-12-06
    body: ">>>your comments are truely up to the mark of someone who hasn't got a clue what I am talking about.\n\nsame here"
    author: "IR"
---
The <a href="http://www.kde.org/">KDE Project</a> today released KDE 3.1 RC3 with fixes for several severe bugs in RC2. This RC is <a href="http://developer.kde.org/development-versions/kde-3.1-release-plan.html">likely</a> the last before 3.1 although there remain a few items on the TODO list for KDE 3.1.  If you find a showstopper problem in this release candidate not yet listed on <a href="http://bugs.kde.org">bugs.kde.org</a>, please file a report as soon as possible. <a href="http://www.kde.org/info/3.1.html">The download links</a> are up as usual on the KDE 3.1 Info page. 

<!--break-->
