---
title: "KDE Dot News: Community FAQ"
date:    2002-06-24
authors:
  - "numanee"
slug:    kde-dot-news-community-faq
comments:
  - subject: "A lot of days without news"
    date: 2002-06-24
    body: "I can understand that not everything can be accepted in the dot, but sometimes there is a big lack of time without news. And I think this is bad for KDE as it shows very little support from the community.\n\nTrying to solve that I've posted some news KDE related in the past and they were not accepted. Probably it would have been a good idea to let some of these news appear although you don't like the content or style (remember that when somebody judges it's impossible being 100% objective).\n\nI will keep trying and improve my english skills writting and making it better so you like, but there is a limit.\n\nBy the way, in question \"Why was my article not approved?\" you answer: \"Perhaps you forgot to leave an email address?\" while in the post web page it says it's not required if you don't need a reply notification.\n\nI guess all of us have to improve in quality.\n\nThanks for your work, and please don't be so strict."
    author: "JL Lanbo"
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: "Which articles did you post that weren't accepted?  I don't seem to recall any from JL Lanbo...  The email address is only so that we can contact you to tell you what went wrong.  If you don't care, then of course there's no need for it.  :)\n\nThanks for the feedback."
    author: "Navindra Umanee"
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: "Yesteday, I sent one for example. I sent it with the same nickname (JL Lanbo) and it was about lincese issues with KDE/QT. But I use to change nicknames.\n\nRegarding to the e-mail, I don't like to make it public. May be a good idea to add an option to send the email just to you, so it won't be public for everybody?\n\nThanks again for your work.\n"
    author: "JL Lanbo"
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: "Oh right, that one.  One of the editors thought it was a good idea to wait for the next article in the series and post them together (I thought it was a good idea too).  Plus, you know how much we are sick of these license discussions... :)\n"
    author: "Navindra Umanee"
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: "I guess from time to time it's good to see what other people in the community think about this stuff. Do they care? Don't they? Any new KDE user that didn't know about that?\nI also think it's a good idea to wait until next article in the series.\n\n"
    author: "JL Lanbo"
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: ":)  Hey, I don't want to chime in with complaints (the Dot is my favorite site), but I kicked a few articles that would \"appear in the next quickies\", and never did.  I can't even recall what they were... other than one being Wasabi BSD releasing the first commercial BSD with KDE, and most of the rest being KDE stuff that appeared on NewsForge, but not here.  They were all minor news, certainly, just stuff I hit while wandering around the web, but they never did appear.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: "That's probably because nobody got to writing the Quickies...  There was a time when I was absolutely swamped, and generally I'm the only one to write these Quickies.  I apologise for that. \n\nI probably did try to salvage your submissions at some point though, I'd be surprised if I didn't.  And if I didn't they must still be in the queue unless the matter was dealt with at the time.  Ahh, yes.  I see one from you in the queue -- it's a nice article about the Zaurus Release.   I should have probably been more aggressive in pushing this one, but there may have been some internal confusion at the time.  I hope you will continue to submit articles of this caliber!\n\nThis community faq is a first step in a move to recruit new community support and new blood for the dot.\n"
    author: "Navindra Umanee"
  - subject: "Re: A lot of days without news"
    date: 2002-06-24
    body: "No problem - I had to put my own news site on hold (having to do with Rocky Horror, not KDE) due to just being unable to update due to moving and such.  I watched submissions slowly trickle off.  I just wonder if anybody else, like me, stopped sending in news because it wasn't getting posted or clearly rejected during that time.\n\n(And yeah - I know how upsetting it is to have to let a site slide for other priorities and lack of time.  You've done such a good job covering, I don't think anybody really realized that you were overloaded, Navindra.  In all reality, you've been doing a great job, as witnessed by the popularity of this site.)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Striving for Minimal Levels of Quality"
    date: 2002-06-25
    body: "I'm pretty sure that under the question \"Why do you not approve all submissions? Other sites do it,\" you did not mean to say:\n\n\"We are convinced we can provide a better service to our readers by maintaining a minimal level of quality and professionalism!\"\n\nWhile many sites on the Web are of lackluster quality, I'm quite certain that  most do not make it a goal to strive for a lack of quality! :)"
    author: "Anonymous"
  - subject: "Re: Striving for Minimal Levels of Quality"
    date: 2002-06-25
    body: "Ahh... but I said \"maintaining\", not \"striving\", doesn't that change the meaning significantly?  :)\n\nAnyway, I'll fix this misunderstanding for the final version."
    author: "Navindra Umanee"
  - subject: "Short news & interviews"
    date: 2002-06-27
    body: "Hi Navindra & other editors,\n\nthe 'dot' is still one of the few web-sites I visit every day although the most of the time I get disappointed because there is nothing to read here ;-). Sometimes it takes a week before something appears here. So, here are my suggestions how to improve the dot/kdenews:\n\n1. Accept short news with a link to a good story\n2. Accept news which are not directly related to kde but kde's future maybe depends on them\n3. Interviews made with KDE developers are funny, but I miss here some serious interviews with the same persons\n4. Yes, I know that apps.kde.com is official KDE application site where users can download, comment & review new KDE applications, but users usually do only the first thing. There are no many comments, reviews and discussions there. So, the dot/kdenews could be a place where we can announce an application and give it a place for some serious comments.\n5. More interactivity on this site, please. IMHO it is a good idea to ask users what they actually want, which problems they have when downloading, compiling, intalling and using KDE (I know we have bugs.kde.org, but posting such things here is more open way to get response for the user base).\n\nCheers,\n\nantialias"
    author: "Zeljko Vukman 'antialias'"
  - subject: "Re: Short news & interviews"
    date: 2002-06-27
    body: "I am quite disappointed with the dot lately, myself.  Now that I'm no longer depressed due to the lack of bandwidth and CPU (thanks Cyberbrain!), I'm slowing trying to push for adjustments.  This FAQ is meant in part to help the situation...  I definitely want more community involvement too.\n\nFWIW, I just wrote a Quickies article.  It features an Interview, a rant and some old Konqueror news.  I'm afraid I'll have to wait for an another editor to comment on it before pushing it through."
    author: "Navindra Umanee"
  - subject: "Relantive Usability Study Article?"
    date: 2003-08-06
    body: "Why is the Relantive KDE usability story not posted? This is IMHO bigb news, a very good argument for using KDE and of high interest to anyone using or promoting KDE.\n\nAny reasons for not posting my story?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Relantive Usability Study Article?"
    date: 2003-08-06
    body: "A lot of people submitted this story (Steve, one of our editors was the first to point it out).  We're definitely going to post it but we thought it would be cooler to wait for the English version to come out first (scheduled for soon) so that all of our readers can benefit.\n\nSorry, for not emailing back.  Personally, I've been quite inactive editorial wise, and I guess none of the other editors got to responding either."
    author: "Navindra Umanee"
  - subject: "Re: Relantive Usability Study Article?"
    date: 2003-08-06
    body: "As you wrote yourself in your story this is atm only useful for German speaking developers and dot.kde.org is an English newssite. Today relevantive published an <a href=\"http://www.linux-usability.de/download/summary_linux_usability.pdf\">English summary</a>, which is IMO rather short and doesn't contain more information as seen in most stories and comments on other English newssites, and scheduled the complete English findings and press release after completion of the project for 11th August."
    author: "binner"
  - subject: "Requesting Guidance"
    date: 2003-12-11
    body: "Sir,\n     I have experienced your product.And i am intended to do a project on \"threaded downloading\". similar to your project it should also download multiple segments of same file simultaneously.Please instruct me the following details.\n1.the software to implement it\n2.the technical details of the project.\n3.Difficulties to be solved"
    author: "raja"
  - subject: "question"
    date: 2007-08-24
    body: "hello......if i'm in the wrong place, please direct me...........\n\ni'm on my home page.....at the very top is a line that says....file, edit, view, etc..........\n\nbelow this line are two green arrows and a search bar\n\nbelow this line are the words customize links, free hotmail, realplayer, etc\n\nbelow this line are the words stumble!, i like it, send to, etc\n\non the very bottom on the left is the word start\n\ncan anyone tell me the names of these lines\n\nthe top line that starts with file, edit, i think is called the menu tool bar..........thanks for any help..........dg"
    author: "david"
---
By popular demand, we are answering some of the most frequently asked questions at the dot.  Read <a href="http://dot.kde.org/1024894942/#faq">the following FAQ</a> if you are interested in understanding how the dot operates, how best to contribute articles, and how to help improve the dot in general.  Nothing is really set in stone here. The FAQ will be updated as required and as per your comments, and may eventually be moved to a different final location. With your help, KDE Dot News can hopefully improve and serve your needs better in the future.






<!--break-->
<p>
<a name="faq"></a>
<h2>KDE Dot News Community FAQ</h2>

<h3>Why do I care?</h3>

<p> Understanding how the dot works will help improve article submission
quality and allow us to approve submissions faster.  Ideally, there
will be less work for us and less frustration for you, our
contributers.  </p>

<h3>How does the dot work?</h3>

<p>
Anyone can and is encouraged to submit articles to the dot.  Articles
are reviewed by the editors for content and presentation, edited if
necessary, and then possibly approved.
</p>

<h3>Why are more stories not posted?</h3>

<p>
You can help us in this department by submitting the news!  The whole
idea of the KDE news site is that it is community-supported.  We can't
do everything ourselves all the time...
</p>

<h3>Why was my article not approved?</h3>

<p> Read this FAQ carefully.  If this FAQ hasn't answered your
question, then you should definitely <a
href="mailto:dot@kdenews.org">write us</a> and ask us what went wrong.
Perhaps you forgot to leave an email address so that we couldn't get back to you?  Perhaps we had a
failing on our side, such as lack of time, and need further prompting.
</p>

<h3>Why do you not approve all submissions?  Other sites do it.</h3>

<p> Most successful sites do not in fact approve all submissions.  We
are convinced we can provide a better service to our readers by
maintaining a minimal level of quality and professionalism!  For example,
if we posted every application announcement, we would soon take over
the role of <a href="http://www.kde-apps.org/">KDE-apps.org</a> and ultimately drown out general news.  </p>

<p>
At the same time, we want the best compromise between serving as the
official news site for KDE and providing content that is of interest
to you.
</p>

<a name="format"></a>
<h2>How should I format my submission to get it accepted?</h2>

<p> All article submissions should begin with a 1-paragraph summary
that will be displayed on the front page of the dot.  In most cases,
this 1-paragraph summary will be the whole article.  The summary is <i>never</i> more than one paragraph for reasons of consistency. The summary should ideally be <i>complete</i> and contain the
important core information of the article: In other words, the
reader should in general not be forced to "click through" or "read on" to get to the essential information.
</p>

<p> Helpful hyperlinks should almost always be provided in the summary
itself so that the user can seek more information or follow
references.  If necessary, the rest of the body of the article will
consist of a full announcement, or an original content article.  </p>

<p>Article titles should be capitalised in book-title style.  Additionally, when linking to a KDE news item on an external site, we generally like to include the site name in the Title of the article, for example: "External Site Name: Title of External Article".  Furthermore, it would be nice to include a small interesting quote from the external article.  </p>

<p>
Submissions should ideally be typo-proof and grammatically-correct, although we realise this is not always possible.
</p>

<p> Whenever in doubt, <a href="mailto:dot@kdenews.org">ask</a>, or simply heed the pattern that we've
followed over the course of the <a
href="http://dot.kde.org/articles">500 articles</a> and more that
we've posted in the past!  </p>

<h3>What type of content is generally accepted?</h3>

<p>
Most legitimate KDE news is worthy, and almost every category of news
will be considered.
</p>

<p> Application announcements, however, present a special problem.  In
general we do not publish these, they are passed on to <a
href="http://kde-apps.org/"</a>kde-apps.org</a> which is currently the
official KDE application site.  In special circumstances, such as a
new stable release of an important app, or the achievement of an
important milestone, we will be open to publishing a dot article.
Thoughtful reviews of applications are also welcome.  </p>

<p> Commercial announcements are a bit iffy and have caused
controversy in the past.  We are serving the community, and want to
avoid getting entangled in commercial interests.  And if we do publish
commercial announcements, we would like to maintain some form of
fairness for all concerned.  That being said, news items of import
will certainly be considered particularly if they significantly increase KDE usability or desirability to KDE users or potential KDE users.  </p>

<p>
Finally, random calls for help in vaporware projects or random
requests for user support will under no circumstances be published
and, in all likelihood, will not be acknowledged.  Sorry, but we get
busy at the dot.
</p>

<h3>Who is allowed to post and comment?</h3>

<p> Anyone is, within reasonable confines.  Abuse is not acceptable.
Repeat offenders demonstrating malicious intent are not welcome and
may be banned indefinitely.  If you think you have been unfairly
treated, you are welcome to <a href="mailto:dot@kdenews.org">write
us</a>.</p>

<h3>I would like to contribute a regular column to the dot, am I welcome?</h3>

<p> <i>Absolutely</i>, we would love that!  Please <a
href="mailto:dot@kdenews.org">write us</a> and we will be happy to get
an arrangement going.  </p>

<h3>I have ideas for improving the dot or this FAQ...</h3>

<p> <a href="mailto:dot@kdenews.org">Write us</a>!  </p>

<h3>I'm still not satisfied, how can I join the KDE Dot News editorial
team?</h3>

<p> <a href="mailto:dot@kdenews.org">Write us</a> and tell us why you think you are qualified and what you
think you can contribute to the dot as an editor.</p>

<h3>I'm bored, this site is not enough!</h3>
<p>
There are tons of other sites you can visit.  Our favorites are <a
href="http://www.kde-look.org/">KDE-Look.org</a>, <a
href="http://www.kerneltraffic.org/kde/archives.html">KDE Traffic</a>, <a href="http://cvs-digest.org/">KDE-CVS-Digest</a> or any in the <a href="http://www.kde.org/">KDE
Family</a> of websites. There are also lively <a href="http://www.kde.org/mailinglists.html">usenet and email</a> discussion forums. The <a href="http://dot.kde.org/1016861082/">KDE Cafe</a> may be of particular interest.
</p>
<p>
<i>[Let <a href="mailto:navindra@kde.org">me know</a> if you are aware of any  other frequently updated sites with a KDE focus.]</i>
</p>
<h2>Known Bugs</h2>
<p>
HTML comments are not yet allowed.  The year and timezone is often not included in article dates.
</p>
<p>
<hr>
</p>
<p>
<i>Last Modified: 2005/03/31</i>
</p>





