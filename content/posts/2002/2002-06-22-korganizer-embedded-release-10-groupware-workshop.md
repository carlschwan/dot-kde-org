---
title: "KOrganizer: Embedded Release 1.0 + Groupware Workshop"
date:    2002-06-22
authors:
  - "numanee"
slug:    korganizer-embedded-release-10-groupware-workshop
comments:
  - subject: "Nice workshop"
    date: 2002-06-22
    body: "This workshop is really nice. Having all those screenshots does help a lot. \nthx"
    author: "Portisch"
  - subject: "Cool news..."
    date: 2002-06-23
    body: "Now if only I had friends to schedule stuff with :P\n\nAnyone up for a BBQ this weekend?\n\nKeep up the great work KOrganiser guys, I have been using the calender now for 3 and a half years.  Not one instance of data loss...  Pretty impressive.\n\n-ian reinhart geiser"
    author: "ian reinhart geiser"
  - subject: "NO!"
    date: 2002-06-23
    body: "Don't party, code! ;P"
    author: "Lenny666"
  - subject: "just few remarks"
    date: 2002-06-23
    body: "Hi,\n\nFirst, congratulations to the kdepim for this excellent job...\n\nNow - if you don't mind - I got some constructive remarks. These are just my thoughts, not flames or anything, so please take it easy ;)\n\n1. Most of the users are used to outlook scheduling. Sure, outlook sucks, but when 90% of users are used to it, you'll need to think about making it as much as comfortable for them to actually use it. The more similarity to outlook approach, the better for users who are coming from Windows world (hmm, I can already imagine the flames I'm going to get just from this remark)..\n\n2. If I read everything right, then I need to do some filtering in KMail to make it work with korganizer. Why taking this confusing approach? why not creating a special message that can be specifically detected by KMail, and when KMail identify it - it will show a special GUI message that a user is requested to reject, approve, or reject with another schedule timing? it will be much easier IMHO.\n\n3. This is just a suggestion, but I think interopebility could be good thing here - how about suggesting some common format for both korganizer and Evolution scheduling? that way a user can use Evolution to answer someone who's using Korganizer to setup meetings, and KMail user can answer someone who uses Evolution to setup meetings - so all of the users can really collaborate..\n\n4. Web Access to Korganizer and scheduling could be a really cool feature - but I guess this is for the long term plan ;)\n\nKeep up the good work, guys..\n\nHetz"
    author: "Hetz Ben Hamo"
  - subject: "Re: just few remarks"
    date: 2002-06-23
    body: "OMFG, lol, j00 sux0r, j00 0|_|t100k p3rs0n!\n\nNo, but seriously, all good points."
    author: "Nick Betcher"
  - subject: "Re: just few remarks"
    date: 2002-06-24
    body: "Rather than quoting, I'm just gonna address them by number.\n\n1 - I'm not sure how well thought out Outlook's groupware system is.  I've been sysadmin for large companies, and it's always one of the most difficult parts of the entire MS Office system.  I like Outlook, but there were only a bare handful of secretaries who understood the system and used it.\n\n2 - Basically, you want the filter enabled by default.  I'm leery of anything changing data on my system from an email by default.  Much better, IMO, would be a splash screen when you run KOrganizer that asks you if you want to enable it - and a checkbox in KOrganizer that makes a dcop call to enable or disable the feature.  IMO, leave it off my default, and make it easier to enable.  As for why it *should* be a seperate program...\n\n3 - ...it's so the feature you're asking for can be done.  For that matter, with a seperate filter, you can use *any* mail program with this system - even commandline only ones like pine, or any new one yet to be written or released.  At least, if it works the way it appears to, filtering text is the \"Unix Way of Doing Things\", and is built that way for the purposes of interoperability.\n\n4 - I'd say that's for an IMAP/Schedualing server.  Something along the lines of Horde.  KDE is going to start entering places where serverside components are going to become increasingly necessary.  Some of them have yet to be addressed by the open source community, at least to the point where there is a clear standard.  HTTP and IMAP are great, but when you get into the deeper realms of corporate data access, including contact data, schedualing and so on, you enter a murky \"I prefer...\" realm of incompatable protocols.   About the only semi-standard one is MS Exchange, and that fails for enterprise usage.  \n\n--\nEvan\n\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: just few remarks"
    date: 2002-06-24
    body: "ad 1.) I don't know what you mean by this. Do you mean the behaviour explained in your next point?\n\nad 2.) Well - this would mean some work on KMail. But the KMail guys are very busy with other things (Sphinx-project). Hopefully we can improve here...\n\nad 3.) In the howto there stands: \"Group scheduling adheres to open standards: iTIP (RFC 2446) and iMIP (RFC 2447). These are based on iCalendar, the native file format of KOrganizer. By using these standards, it is possible to exchange group scheduling messages with users of other applications implementing the standards, for example, Evolution and Outlook.\"\n\nad 4.) We'd like to have some server-plugins anyway (Jan-Pascal van Best already works on an\nexchange-plugin).\n\nSo all kdepim needs is developers, translators, developers, documenators, developers, artists, developers,...\n\nciao, happy scheduling ;-)"
    author: "Guenter Schwann"
  - subject: "Re: just few remarks"
    date: 2002-06-24
    body: "Beurk :(=\n\nDon't do as Outlook.\nThis software has the ergonomy of a Boeing 747 but it has powerful as a bicycle.\n\nLet the KDE team do their job. For the moment they make good things for the usability of their software and I think most of us use Linux with KDE because of the difference with the Windows world.\n\nThe aim of Linux is not to make everybody use it. We are not in the Star War movie :)\n\nLong life to Free Software !!\n\nPS : sorry for my poor english"
    author: "Let them "
  - subject: "Re: just few remarks"
    date: 2002-06-26
    body: "Slightly faster then a speeding snail\nAbout as powerful as a well-thrown piece of tissue paper\nAble to crash from high distances with a single click\nThe app from Microsoft...\n\nIt's a mediocre PIM! It's a horrible email client!\n\nNo, it's SuperOutlook!\n"
    author: "Carbon"
  - subject: "is this stuff possible to sync"
    date: 2002-06-24
    body: "is this stuff possible to get synced between my desktop korganizer and my pda korganizer?"
    author: "kde friend"
  - subject: "Re: is this stuff possible to sync"
    date: 2002-06-24
    body: "Not yet - as far as I know. But on a generic sync-tool is worked on (see kitchensync in kdepim)."
    author: "Guenter Schwann"
  - subject: "Doesn't work here"
    date: 2002-06-24
    body: "Hmm, doesn't work for me. I get so far that I have messages in the 'Scheduler Incoming Messages' dialog, but as soon as I press 'Accept' I see an 'IncomingDialog::acceptMessage(): Error!' error on the console. What wrong?\nKDE-3.0.1 on Redhat 7.1.\nBTW, why do I have to press 'Retrieve Messages' when its already in the \n.kde/share/apps/korganizer/income/ directory?"
    author: "koos"
  - subject: "Re: Doesn't work here"
    date: 2002-06-24
    body: "Are the Emailaddresses all ok? KOrganizer get confused if they aren't ok (at the moment) (for example if you receive a reply with another attendee-email-address than you send it).\n\nThis retrieve button will be obsolete with 3.1...\n\nciao\n\nP.S.: KOrganizer groupscheduling is far from complete, but I need some user's feedback..."
    author: "Guenter Schwann"
  - subject: "Email Reminder ?"
    date: 2002-06-25
    body: "Hi All, \n\nIs there a way to receive an email reminder before the scheduled event ?. \nBoth for group scheduling and personal use. It is nice (and very useful) to receive an \nemail, say, half an hour before the meeting/event (this lag time should be set by the user), \nautomatically sent by the calendar/organizer program\n\nThis is a great piece of software. The only thing it is holding me from using it is the lack\nof this feature (or my inability to find it ;-)\n\nMany thanks (both for the program and your response :-),\n-- Leo"
    author: "Leo"
  - subject: "Re: Email Reminder ?"
    date: 2002-06-26
    body: "Currently this is not possible (but often requested :-( ). Only workaround I could imagine would be to use the \"program-action\", and write a small script to send a mail.\n\nciao"
    author: "Guenter Schwann"
  - subject: "Re: Email Reminder ?"
    date: 2002-06-27
    body: "Thanks a lot Guenter !\n\nYes, that's what I thought I could do (a script), even though it gets a bit complicated  when you have a few users/machines. I'll see what I implement for myself ... \n\nMany thanks for the answer and for the code :-)\n-- Leo"
    author: "Leo"
  - subject: "Stuck with Evolution then"
    date: 2002-07-03
    body: "I can't stand Outlook but much as I desperately wanted to like KOrganizer it simply isn't good enough for my day to day use. I couldn't find how to display the current month instead of an arbitary 30 days around the current day (I know it must be configurable but damned if I could find it), navigation is clunky, visually it's not appealing which makes a difference in an organiser. Evolution is the best I've found so far, though I liked my old Acorn one <A href=\"http://www.organizer.ukgateway.net/shots.htm\">Organizer</A> (so simple). As far as I'm concerned the field is still wide open for someone to write a decent personal organiser, where someone thinks about the user instead of copying the rather poor Outlook.\n\nPhillip.\n"
    author: "Phillip"
  - subject: "Great app, several uses"
    date: 2002-07-05
    body: "I'm actually using Korganizer to manage my litle plants watering days and so on\n\nIt's a really useful app, the only thing it has to add is attachments to the events and so on so that I can put photos of my plants as I try different methods on them."
    author: "speed-R"
---
The <a href="http://korganizer.kde.org/">KOrganizer website</a> is featuring <a href="http://korganizer.kde.org/workshops.html">a new workshop</a> series.  The first <a href="http://korganizer.kde.org/workshops/KOrganizerIMIP/en/html/index.html">awesome document</a> is chockful of screenshots and useful tips on how to use group scheduling.  In other <a href="http://korganizer.kde.org/news.html">butt-kicking news</a>, the first stable release of <a href="http://korganizer.kde.org/korge.html">KOrganizer/Embedded</a> for <a href="http://www.trolltech.com/products/qtopia/index.html">Qtopia</a> has been released.  Get <a href="http://korganizer.kde.org/getting.html">organized</a>!
<!--break-->
