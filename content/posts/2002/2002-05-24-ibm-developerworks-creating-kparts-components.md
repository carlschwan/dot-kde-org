---
title: "IBM developerWorks: Creating KParts Components"
date:    2002-05-24
authors:
  - "Frank"
slug:    ibm-developerworks-creating-kparts-components
comments:
  - subject: "very"
    date: 2002-05-24
    body: "nice!"
    author: "fault"
  - subject: "KParts is an excellent idea"
    date: 2002-05-24
    body: "Yeah, I know I've bashed kde on more than a few occasions, but kparts is probably the best thing yet.  And, David, great job on all your tutorials!\n\nAnd, for a little more gratitude, here's some opinions as to how one might extend the kparts area..... (feel free to flame me if you think these are stupid ideas :P )\n\n1) Extend the xml so that instead of using the kparts framework, use the XML to define a language-independent model for using kparts.  IIRC, kparts uses a \"KPartsManager\" or the like, which may or may not depend on dcop (can't recall :P ).  So, if you were to use a form of dcop to coordinate/manage kparts, you can use sockets/sharedmem and a C structure in order to allow say, GNOME apps or other apps to use kparts.  That way, one can mix and match parts from different interfaces and still get a clean app.\n\n2) There isn't a #2 (or 3  or 4) :P\n\nJust an idea :)\n\nJustin Hibbits"
    author: "Justin Hibbits"
  - subject: "Re: KParts is an excellent idea"
    date: 2002-05-24
    body: "I believe this is what XParts is all about.\nAlt-F2, ggl:XPart ;)"
    author: "David Faure"
  - subject: "Re: KParts is an excellent idea"
    date: 2002-05-24
    body: "yeah, I know XParts is similar, but what I'm thinking of is the opposite (embedding kparts in OTHER apps, not just kde apps).  maybe I confused you :P\n\n(Yes, I know the title is kinda lame, but, couldn't think of a better one :P )\n\nJustin Hibbits"
    author: "Justin Hibbits"
  - subject: "Re: KParts is an excellent idea"
    date: 2002-05-24
    body: "Why not use the right tool for the right job?  Use XParts when you want to embed KDE components in OTHER apps or OTHER app-components into KDE. \n\nUse KParts when you want to embed KDE components within KDE components.\n\nSimple.\n"
    author: "ac"
  - subject: "Registration?"
    date: 2002-05-24
    body: "Why do they these big companies SUN/IBM insist on requiring registration.\nDo they want people to read it or just collect statistics?"
    author: "Fredrik C"
  - subject: "Re: Registration?"
    date: 2002-05-24
    body: "KDE tutorial should be found on KDE web sites.\n\n"
    author: "somekool"
  - subject: "Re: Registration?"
    date: 2002-05-25
    body: "I felt like skimming the document, but was put of by the registration. Sure, if I really _needed_ the doc I could register... but I feel I shouldn't have to."
    author: "ac"
  - subject: "Re: Registration?"
    date: 2002-05-25
    body: "You just need to give a username and password, it doesn't care to verify the email address."
    author: "dc"
  - subject: "Lame?"
    date: 2002-05-28
    body: "Lame is that this KDE documentation is being withheld from KDE developers."
    author: "Neil Stevens"
  - subject: "Non-Javascript version"
    date: 2002-05-25
    body: "So, can someone who trusts javascript grab a copy of that and stick it on developer.kde.org please?"
    author: "Neil Stevens"
  - subject: "Re: Non-Javascript version"
    date: 2002-05-27
    body: "Either you are paranoid or you are extremely lazy.\n\nEither way, this is lame.\n"
    author: "Mark"
  - subject: "I gotta admit..."
    date: 2002-05-25
    body: "...this whole don't-make-me-register thing is pretty ridiculous. I registered with IBM a year ago, and got nothing from them. In fact, I think they have an opt-IN policy with regard to spam.\n\nJeez lads, its well worth it, and it's not like they're asking for a pint of blood or anything."
    author: "Bryan Feeney"
---
Following <a href="http://www-106.ibm.com/developerworks/library/l-kparts/">the first introduction</a> to KParts on <a href="http://www-106.ibm.com/developerworks/">IBM developerWorks</a>, David Faure now goes on to show developers how to <a href=http://www-105.ibm.com/developerworks/education.nsf/linux-onlinecourse-bytitle/A96500917FA1DB0786256BC000808C0F?Open&t=gr,p=KParts>create KParts components</a> (free registration) in the first part of this tutorial series. You'll get an introduction to the core KParts concepts of read-only and read-write parts and network transparency. You'll learn how to create a read-only component, and then modify it to be a read-write component. You'll also learn how to deploy the component so that Konqueror can use it.



<!--break-->
<p>
<i><b>Note from David:</b> This tutorial also demonstrates how KDE3 simplifies component programming, so check it out even if you are familiar with KParts in KDE2. ;-) Registration is required, but that's fairly quick to get through, and you can then save the whole tutorial as a .zip and read it offline.</i>
</p>

