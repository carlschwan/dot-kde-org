---
title: "New KDE Mailing-List For German Speakers"
date:    2002-05-20
authors:
  - "binner"
slug:    new-kde-mailing-list-german-speakers
comments:
  - subject: "Number of German users?"
    date: 2002-05-20
    body: "Anybody got a clue approximately how many German KDE-users there are? Besides the ones that use it through work that is.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Number of German users?"
    date: 2002-05-20
    body: "Difficult question, and don't forget Austria and Switzerland who also have German speakers. The only thing which seems sure is that KDE is far more popular than Gnome in Germany.  There was a survey among 10.000 visitors of \"heise-online\" last year (http://www.heise.de/ct/01/17/186/) where 85% stated they use Linux on the desktop. 74% use SuSE and 10% Mandrake which both have KDE as default desktop. So if you assume that the majority use the default desktop and you would know the sales numbers of both distributions you could estimate how many are there at least."
    author: "Anonymous"
  - subject: "Blah"
    date: 2002-05-20
    body: "Schei\u00dfe, spreche ich nicht Deutsches :/\n(pardon my French ;)\n\nBut why does there have to be a German list?\nWhy not a Dutch one?\nOr French?\nSpanish?\nPortuguese?\n\n\n(not that I would like that.. I'd rather write/read English)"
    author: "CheeseHead"
  - subject: "Why?"
    date: 2002-05-20
    body: "One by one, I answer your questions:\n\n1) Because somebody set one up.\n\n2) Because nobody set one up, and all Dutch speak english don't they? :-)\n\n3) Because nobody set one up.\n\n4) Because nobody set one up.\n\n5) Because nobody set one up."
    author: "Neil Stevens"
  - subject: "Re: Why?"
    date: 2002-05-20
    body: "<quote>\nand all Dutch speak english don't they? :-)\n</quote>\n\nWell, most of them can perfectly read or write English.. but please, don't ask them to SPEAK English :)\nBeware of the horrible Dutch accent! (tm)\n"
    author: "CheeseHead"
  - subject: "Re: Blah"
    date: 2002-05-20
    body: "> But why does there have to be a German list?\n\nThere doesn't have to be one. But obviously there was need for it given the number of help requests the webmaster team of kde.de received in the last time. \n\n> Why not a Dutch one?\n\nAny signs for need of that? Or an active language portal to attract users?\n\n> Or French?\n\nI have the impression that \"kde-francophone\"'s topics are more about than just traduction. But I may be wrong there (no native french speaker)."
    author: "Anonymous"
  - subject: "Beware Dutch don't speak English"
    date: 2002-05-20
    body: " < 2) Because nobody set one up, and all Dutch speak english don't they? :-)\nThe last time a Dutch Politician said 'at your service' in English on Dutch televison, \nsomething really bad happened to him. \nBeware my Dutch friends, English is a lethal language.\n\nGroetjes uit Franjrijk\nEcureuil"
    author: "Ecureuil"
  - subject: "International support for KDE "
    date: 2002-05-20
    body: "Hi,\n\nthe reason why we started a german KDE mailing list for KDE users is that there were many mails directed to the KDE.de webteam asking for KDE support. So we decided to initiate a mailing list for german speaking KDE users. At the moment, we think that this is just the first step towards building a good support platform for german KDE users. The next step will probably be the initiation of a german KDE web forum - for people who don't really want to subscribe to a mailing list just for having an answer to a short question.\n\nObviously, there's a need to give KDE users support in their mother tongue. So the question is not \"Why is there a need for a german mailing list\". The question must be \"Why aren't there more KDE mailing lists in other languages than english ?\"\n\nThe better the support for KDE is on an international base, the better it is for the KDE project itself.\n\nSo if there's no mailing list in your mother tongue available yet, go for it and initiate it. The KDE users speaking other languages than english will be very thankful for that !\n"
    author: "Klaus Staerk"
---
<a href="http://www.kde.de/">kde.de</a> <a href="http://www.kde.de/news/news.php#197">reports</a> that a mailing-list for German-speaking KDE users called "kde-de" has been created, inviting both users and developers to <a href="http://mail.kde.org/mailman/listinfo/kde-de">subscribe</a>. As usual, there is an <a href="http://lists.kde.org/?l=kde-de&r=1&w=2">archive</a> and <a href="news://news.uslinuxtraining.com/kde.kde-de">NNTP access</a> available. This mailing-list completes the support with the <a href="news:de.comp.os.unix.apps.kde">de.comp.os.unix.apps.kde</a> newsgroup (<a href="http://groups.google.com/groups?hl=en&lr=&group=de.comp.os.unix.apps.kde">archive</a>) and #kde.de on irc.openprojects.net for German speakers.
<!--break-->
