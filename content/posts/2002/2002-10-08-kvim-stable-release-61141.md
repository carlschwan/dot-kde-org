---
title: "KVim Stable Release 6.1.141 "
date:    2002-10-08
authors:
  - "mmoeller-herrmann"
slug:    kvim-stable-release-61141
comments:
  - subject: "Why is there no horizontal slider ?"
    date: 2002-10-08
    body: "Hi,\nI've tried the new KVim. Nice job even if I'm rather a very basic user of vim and not in position to give a learned opinion.\n\nI'm wondering why in KVim and GVim there are no horizontal slider but only vertical sliders. If you change the font size to 14 of an opened file, you get an enormously large window much bigger than my screen.\nIf it's possible, keeping the window the same size and getting an horizontal slider would be much nicer.\n\nBut maybe it is a problem with vim more than with KVim.\n\nThanks for the work,\nCharles "
    author: "Charles"
  - subject: "Re: Why is there no horizontal slider ?"
    date: 2002-10-09
    body: "dunno for sure, but vi in general wraps lines for display purposes."
    author: "me"
  - subject: "Re: Why is there no horizontal slider ?"
    date: 2002-10-09
    body: "it is disabled by default.\nyou can type:\n\nset guioptions=mrbgtT\n\nto get it. there is also a menu option in kvim, but it does not stay across sessions (normal).\n\nemmanuel"
    author: "emmanuel"
  - subject: "Re: Why is there no horizontal slider ?"
    date: 2002-10-11
    body: "btw, put it in your ~/.vimrc so that it stays between sessions."
    author: "emmanuel"
  - subject: "Remote files"
    date: 2002-10-08
    body: "I can't tell from the site.. I could be blind, but I'm not seeing it.  My current version of kvim can't handle remote files via an ioslave (i.e. fish).   Does it handle them now?   That would rule my pants."
    author: "Black Napkins"
  - subject: "Best thing..."
    date: 2002-10-09
    body: "...since sliced bread!\n\nI mean that!\n\nI awaited kvim ever since the first announcement, use it for a couple of weeks now, and am as happy as ... as ... well go figure ;-)\n\nMany, many thanks to the developers!\n\nregards,\ntomte"
    author: "tomte"
  - subject: "kvim"
    date: 2002-10-09
    body: "thanks\n\n"
    author: "somekool"
  - subject: "thanks!"
    date: 2002-10-11
    body: "you know when you know a project is mature?\ni'm not upgrading from rc2. it works well enough already :O)\n\nthanks very much!\n\nemmanuel"
    author: "emmanuel"
---
After two release candidates and 5 months after <A href="http://dot.kde.org/1018455930/">KVim 6.0</a>, the KVim team is pleased to announce the release of the best version of <a href="http://freehackers.org/kvim/index.html">KVim</a> ever. It provides many <a href="http://freehackers.org/kvim/features.html">new features and improvements</a>: a new GUI for Qtopia systems, a new KDE toolbar, full DCOP support, much improved support for internationalisation and encodings, and improved portability. Read <a href="http://freehackers.org/kvim/kvim61141.html">the full announcement here</a> and check the <a href="http://freehackers.org/kvim/screenshots.html">screenshots</a>.
<!--break-->
