---
title: "LinuxWorld: Why KDE Apps Have A Bright Future"
date:    2002-06-29
authors:
  - "ee."
slug:    linuxworld-why-kde-apps-have-bright-future
comments:
  - subject: "I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "I agree with all what Nicholas Petreley said, but I think there is something I would also would like to be written: SPEED. Actually, I don't care that some apps take long to boot, it's only konqueror (the file manager).\n\nI never use konqueror to browse files because of its speed. I end up openning a konsole and doing it quicker.... :-(\n\nAnybody has results/benchmarks of konqui with gcc 3.1?\nShould be expect the amazing speed improvements that were announced back in time when developers said that speed problem was only due to the C++ linker?\n\nApart from that, KDE really rocks and I'm so happy with it ;-)"
    author: "JL Lanbo"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "...and stability ;)\nP.S. ...and MANY interface(usability) cleanups, and ..."
    author: "Chemerer"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "I have compiled KDE 3.0.1 with prelinking and gcc 3.1. And the result is\n30% faster not more. So as you, i don't use konqueror : when i want use konqueror, i launch it and go to have a coffe. My Konqueror starts in 5s\non my laptop (PIII 700). By Comparaison, nautilus 2 starts in 1/2s.\nBut i use many KDE and love Keramik..."
    author: "ellis"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "4 seconds in my case. I also got a laptop, but 1Ghz.\nLet me do some maths: 5 secs * (1 Ghz / 0.7 Ghz) = 5.7 secs. :-) your boot up should be slower. Just kidding :-) I guess this is not as easy as dummy maths ;-)\n\nActually it really surprised me when I counted the seconds. You tend to think that boots faster, but if you press the icon and start counting until you get the window it shocks you...\n\nAs I said, I don't care to wait for the web browser, or programs like kmail, but file browser should be fatser IMHO."
    author: "JL Lanbo"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "2 seconds until the window appears, on AMD K7 1GHz, without prelinking, with gcc 2.95\n\nafter 2 1/2 seconds the window is completely built up :-)\n\n"
    author: "cmj"
  - subject: "4 secs"
    date: 2002-06-29
    body: "4 seconds to start konqu as filemanager on my PII 266 with SuSE 8 (standard kde3.0.1 rpms) till the window appears. One second later files and folders\nappear.\n"
    author: "Thomas"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-01
    body: "i was very suprised when installing KDE 3.0.1 binaries for SuSe 7.2.\nOn my K7@1.2 GHz Konqueror starts almost instantaneously like much of\nthe other version 3 apps do too. Nice comparison between Version 2 and 3 apps\nrunning in parallel shows a (massive) speed improvement to me.\n(Konq. 2.2 took around 4-5 seconds and more to come up, no significant \ndifference if already cached)\nI guess it's there with prelinking but i don't know any details. \nTo me, to be honest, it now sets the pace...anyone using Win****XP?\nForget about luna...\n"
    author: "/mrc/"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "Nine @#$%&# seconds on a 400 MHz PIII laptop! It always seemed slow, of course, but until now I didn't realize just how slow things are! Actually, the speed issue is the only thing that ever made me think seriously of switching to a faster DE. But up to now I remained true to KDE, and I hope the developers will improve things someday."
    author: "kavau"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "Well, mine takes as much 3 secs (Athlon 1.1 Ghz, precompiled RPMs, SuSE 7.2), but i deeply agree with improving usability. Some options are difficult (or even impossible) to set up."
    author: "Alx5000"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "Konqueror 2.2.2 used to take like 10 s on my box.\nYesterday I installed the new KDE 3.0.2 debs ('apt-get install kde' in debian), and Konqueror starts in amazingly 4s first session, and then 2s for next!!!\n\nThis is on a PII 300 with 160 MB ram so I'm deeply impressed. The DEBs are compiled with GCC 2.95.x without optimizations or pre-linking.\n\nI cannot do else than recommend these DEBs to any debian user.\nhttp://calc.cx/kde.txt"
    author: "ealm"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "First it was a prelinking problem... and now is a distribution problem???\nWhat comes after? :-P\n\nI will try kde 3.0.2\n2 seconds on a PII 300 looks impressing. I hope the 3.0.2 release finally hits the speed improvement !!!"
    author: "JL Lanbo"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "2s on my Celeron 400Mhz, 192M.  My harddrive is relatively recent from last year.  KDE compiled from CVS, no special optimisation, all debug stuff removed."
    author: "Navindra Umanee"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "Odd; here, on my Celery 950 (using combreloc+glibc 2.2.5-post) with a hot disk cache, for release builds Konqueror starts in about 1.5-2 seconds (in fact, with MDK-GCC-2.96, it would be about 1-1.5 seconds, but I didn't try to optimize as aggressively with 3.1 yet, since I mostly doing debug builds).... Make sure you use --enable-fast-malloc=full on kdelibs flags (if you're on x86 Linux or FreeBSD), and --disable-debug for best performance of release builds; and of course using CPU-specific optimizations can be somewhat helpful. \n\nAnother data point: one of the Kopete developers has mentionned on IRC that for his P133, Konqueror takes 8-10 seconds to start... So something sounds weird about your 5 seconds.. "
    author: "Sad Eagle"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "hmm, on my PIII 450 konqueror starts in 3-4 seconds on a system with gcc 2.95.3 using objprelink in QT and KDE3.  on my other system (on the same box) with gcc 3.1 using combreloc prelinking it also takes about 3 to 4 seconds to start the first time, then once it's cached it takes 2 seconds tops."
    author: "DavyBrion"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "I have a P4 1700 (ok, It's  a faster machine, but is a normal one these days) and never experienced a startup time so long. My home dir has over 900 files and is opened in less than a second. Maybe you use it with the preview enabled for every file you have. Or need more RAM. I tried KDE3 when I saw the Gnome 2.0beta5 version (I used Gnome from pre1.0 to 2.0beta5, really disappointing) and was really surprised by the completeness and ease of use of KDE. It is far superior to Gnome in my opinion.  "
    author: "sxx"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-29
    body: "Whoah, P3/700 Mhz?  My system is a P2/300 and it takes me less than five seconds to launch Konqueror (albeit not much less :-)"
    author: "LukeyBoy"
  - subject: "why isn't konqui more threaded?"
    date: 2002-07-01
    body: "Jeez, thread the damn thing already. I have 541 files in my home directory (ls -1 | wc -l) and there is NO REASON that the entire UI of konqui should be frozen while it parses these and draws the stinkin' icons. Sometimes I just want to open up a directory, then type \"Alt-o subdir/subdir\", but I have to wait for my home directory to be parsed every freakin' time. I can stand a lot of CPU wasteage and a less than optimal internal implementation; what I want is PERCEIVED speed.\n"
    author: "Scott Baio"
  - subject: "Re: why isn't konqui more threaded?"
    date: 2002-07-03
    body: "Konqy shouldn't block and it doesn't block here, neither on the 1 GHz nor on the 200 MHz box. While loading the dir contents it ain't very responsive, but it doesn't block.\nI'm not sure if the file previews in icon mode might block.\n\nAlex\n\n"
    author: "aleXXX"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-15
    body: "\nOn my box, 1.4GHz Athlon Thunderbird with 512MB DDR RAM and 10.000RPM Ultra160 SCSI disk and KDE 3.1alpha1 compiled from source with gcc 2.95.3 (no obj. prelink etc.) Konqueror starts in ~ 1\u00bd - 2 sec.\n\n"
    author: "Jesper Juhl"
  - subject: "Fix to Konqueror startup problems"
    date: 2002-12-24
    body: "Edit your /etc/X11/XftConfig and comment out all directories that\ndon't exist, then restart X.  Konqueror (and perhaps other apps) should \nstart up much faster (e.g., from 5 sec to 2 or less on a PIII/1Ghz).\n\nNext question is how to get it from 2 sec to 1/2 sec...\n\nBryce"
    author: "Bryce Harrington"
  - subject: "If you want speed dont use KDE"
    date: 2002-06-29
    body: "KDE isnt for speed, is built for power and ease of use.\n\nYou cannot have speed and power along with ease of use.\n\nSpeed = hard to use commandline, or slim interface with mostly commandline.\n\nPower = commandline.\n\nSpeed, Power and Ease of use = KDE, OSX, etc, when you try to bring power of the commandline into the GUI, you cannot expect it to be fast and easy enough for normal people to use, \n\nKDE does its job. I like it, the stability does need work though, konq should never crash.\n"
    author: "Lucian"
  - subject: "Re: If you want speed dont use KDE"
    date: 2002-06-29
    body: "My take on KDE speed: There are two tasks that should start up really fast. They are both handled by Konqueror; web browsing and file management. They form the basis of today's computer use. That's why having Konqueror start up fast is so important. Don't kill me, but I can actually see the use of a \"quick start\" utility for Konq. I.e. a program loading some parts of Konq, making the start no slower than opening a new window in an already active Konqueror. It's not the best solution for everyone, but some of us have memory and CPU to spare for such a tool. That is if it's not possible in another way to make Konqueror start in a second or less. That would be even better."
    author: "Matt"
  - subject: "Re: If you want speed dont use KDE"
    date: 2002-06-30
    body: "That's bul...\n\nSpeed and power CAN go along with ease of use. Take ROX-Filer for example, it's an very comfortable file manager, and it's fast, first time takes less than a second, and afterwards it loads INSTANTLY.\n\nI use a Pentium III 450Mhz, with KDE 3.0.1, and Konqueror takes 4 seconds to open.\n\nI really liked Kde3, but the only problem was the speed, I don't mind waiting ~10 seconds for it to load, but after it loads, I wanna work fast, Gnome apps load in less then 2 seconds, even Nautilus 2 load in 2 seconds, so don't give me this crap of speed and power with ease of use, IT'S POSSIBLE!!!\n\nIf kde would only take care of the speed problems it would be the leading desktop enviroment, because besides the speed it's perfect."
    author: "D.E."
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-30
    body: "I use the rpm's for SuSE 7.2, on an Athlon 550 with 512MB mem.  KDE 3.0 was very slow, rather buggy and consequently a disappointing experience.\n\nKDE 3.0.1 feels like a whole new desktop. Much much faster and more stable too. I would even descibe it as snappy.\n\nI just wish the KDE team would finally tackle the issue of making the cut'n paste user experience across KDE, consistant and user friendly.  For instance, I can RMB cut/copy/paste while typing in this text - but there is no RMB copy option available for the text of the article I'm replying to.\n\nSame absence goes for Konsole .. does Lars Doelle have a personal hatred of Menu based cut/copy/paste?\n\nThese omissions continue to blight the KDE user experience! And frankly are becoming more noticable now as KDE features increase. \n"
    author: "John"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-30
    body: "Actually I don't agree with much of what Nicholas Ptrelely said, because Kde has already won the desktop war, in my opinion.  Congratulations.\n\nAs a former koffice developer I can't even use Kde because Kde is not a desktop for poor people. I'm sure that Kde 3 runs with acceptable performance on relatively new equipment, but I can't afford that right now.  Maybe never.  I could try to walk out of Walmart with one of those preloaded Lindows systems running at 1.2 Ghz.  Hmmm...  Would they let me develop software in jail as some kind of vocational rehabilitation program?  It seems that criminals have more rights here in the USA than poor people.\n\nYou want volunteers for koffice but cannot even see that they get the tools needed to do the job.  We are supposed to pay for the priviledge of contributing to your wonderful endeavour.  If that isn't elitism, what is?\n\nRecently I scraped up the cash to get my internet connection back, which was cancelled for non-payment over a year ago.  That wasn't so bad as I was broken of an addiction to computers and got some fresh air for a change.  For the past week or so I've been trolling the linux message boards some, and have finally mustered up the courage to drop by the old neighborhood.  It doesn't seem to be a bad place to live.  I just have to remember which side of the railroad tracks I live on, and to take off my hat in deference when nobility pass by. \n\nYou should be ashamed of all the filth and abuse you heaped on dep for expressing his opinions on some things. This is how you treat people who refuse to bow down to your sacred cows.  The Kde project is not God, and I will never bow down to it either. It's just a software project.  I think some of you owe dep an apology."
    author: "John Califf"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-30
    body: "> I'm sure that Kde 3 runs with acceptable performance on relatively new \n> equipment, but I can't afford that right now\n\ni run it and develop for it daily on a PII-400. this is a several-years-old machine. what do you have?\n\n> This is how you treat people who refuse to bow down to your sacred cows.\n\nno, this how people who purposefully misrepresent the truth to satisfy personal grudges get treated. i wish he would've made some decent points because then the project could've learned something from it and responded to it with improvements."
    author: "Aaron J. Seigo"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-30
    body: "KDE3, with the same feature set enabled, is about as fast or faster then KDE2. There are new, cycle-eating features added with each release, but the vast majority of them can be disabled easily, in case you're hungry for more speed."
    author: "Carbon"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-01
    body: "by Aaron J. Seigo on Sunday June 30, @09:12AM \n       i run it and develop for it daily on a PII-400. this is a several-years-old machine. what do you have?\n\nPI-200.  I think performance would be ok for routine use with plenty of RAM, but not for a development box, especially for working on koffice.  There is also the problem of disk space for intermediate files with a 2.5 G hard drive.  So by the time you add all that, may as well buy a new computer.  But what I now have works just fine so long as I do not use kde.\n\nMany people who do use Linux and many others who might want to can't use Kde 3 on perfectly good equipment because of hardware and resource requirements.  That doesn't have much to do with Kde 3, more with changes in Kde 2.0.  I did find Kde 3.0 to be considerably less responsive than 2.x, though, especially in starting apps.  I don't think that matters much for people with new hardware who may actually find Kde 3 faster.  Konqueror does use less memory.\n\nTo suggest that someone work on koffice with a setup like mine is ludicrous. I did that for some time, so it can be done. However, I don't want to further argue the point of why koffice developers need to have decent setups if kde is indeed serious about developing and maintaining koffice.  There might be quite a few developers with more to offer koffice than I had similarly limited by hardware.  Kde does not seem interested in attracting people to koffice work or doing anything to make it easier for them to stay involved. \n\n       > This is how you treat people who refuse to bow down to your sacred cows.\n\n       no, this how people who purposefully misrepresent the truth to satisfy personal grudges get treated. i wish he\n       would've made some decent points because then the project could've learned something from it and responded to it\n       with improvements.\n\nSome of the points were good ones.  Others seemed to stem from a misunderstanding of technical issues about klipper, etc., and some were as you say motivated by personal grudges.  People have a right to express their opinions, dissatisfactions and grudges. The response from the person who represents the Kde League just confirms the allegations dep made about immaturatity and vindictiveness among some kde people.  Anyway, I do not feel that a computer journalist should be a mouthpiece for Kde or Gnome or any project.  Evidently dep needed to break away from being a Kde mouthpiece. \n\nThis reminds me of vicious personal attacks on Richard Stallman, who may not be all here in some ways.  I don't think he's capable of responding to personal attacks in kind, which is to his credit.  Dep may be capable of doing that but won't stoop to the level of filthy namecalling kde sychophants indulge in.  Surely dep realizes that his article was not very well thought out, and is wise enough not to compound the damage.  So dep is going to use and report on Gnome now, instead of kde.  Big deal.\n\nFinally, nothing in dep's article is going to damage kde.  It may even get more people interested.  Anyone who reads the article will understand that dep has personal issues, as most of us do.  That is not the same as indulging in vicious personal attacks which seek to destroy the other party.  People who do that may have to live it down with years or even lifetimes of regret.\n"
    author: "John Califf"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-30
    body: "In case you didn't notice, Petreley has criticised KDE too, but nobody is jumping on him.  There's a reason for that."
    author: "Anonymous"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-06
    body: "I have got a used HP Kayak XV in perfect condition so far, w/ 2xPII300, 256mb ram, Seagate scsi 10k RPM 6 gb hard drive for $260, salestax included, bought in a brick-and-mortar store (ack, foam-and-chipboard here in U.S. ;-). I doubt you can buy it that cheap outside of U.S. I also imagine that a hard-enough-working pizza delivery driver ($17 before taxes in some places) can afford to buy that after a couple of months, if he/she doesn't have to pay previous credit card debt...\n\nI didn't have problems working on kdevelop on it (using kdevelop itself ;-). One doesn't have to wait for compiles to finish, that's what multitasking systems are for...\n\nAfter some tweaking, I even made qt applications work reasonably on a 486/100 notebook with 640x480/8bit mono display and 24 megs of ram.\n\nSingle-processor 300MHz systems with at least 64 megs of ram and a tweaked linux kernel should be enough for development work if you care to set-up things minimally in the first place (recompiling XFree without most of the extensions, and optimized for *size* (not speed) is the first step, I admit).\n\nThat's my experience, your mileage may vary, obviously.\n\nCheers, Kuba"
    author: "Kuba"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-06-30
    body: "Well, in my case, Konqi just needs about 1 second to get started. This seems quite fast but it may be because I'm using an AthlonMP(2 CPUs) 1.5 GHz with 1GB RAM, although, yes, I'm using a completely gcc3.1 compiled system (including KDE CVS).\nSo it may be either the hardware or gcc3.1 wich lets me feel to have a quite fast KDE.\n\nCheers,\nChristian Parpart."
    author: "Christian Parpart"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-02
    body: "That raises an interesting point.\n\nOn my P4-1.5GHz, Konq takes forever and a day to start, either as a browser or a file manager.\n\nOn my dual P3-750Mhz, Konq loads super-quickly.\n\nWhy is it that dual processors speed it up so much?  Does that indicate that perhaps there might be a way to parallelize/thread the startup tasks to achieve the same speed on a uniprocessor machine?  I am not a developer so that could be an entirely  clueless suggestion, but I thought I would mention anyway."
    author: "A person"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-02
    body: "There is something very wrong with your first setup.  Konq is fast on my years old computer."
    author: "ac"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-01
    body: "I am surprised how many people use console for file work, thought I was a GUI dummy:)\n\nYes the konqueror startup is a problem for me, but I do a lot of head/tail, find, and grep stuff, which is _really_ difficult in konqueror. I also make up temporary scripts, build long command lines, and switch user accounts, too, but I don't know how this 'console work' translates into a GUI.\n\nWindowing konq and kate helps, but can become clumsy - particularly with copy/paste sometimes working X11 style and sometimes Windows (I like to switch pasting between ctl-v and middle-click, and mouse-select doesn't always copy). Replacing a simple command line with several windows is not the way to go. I also get annoyed when the right-click menu doesn't have what I expected.\n\nKonqueror is my web browser and file transfer manager, _but_ the .part file is often rejected by the remote server when uploading (partitularly html) docs, so that also worries me.\n\nAnyway, kde 3.0.4 (release sources) gcc 2.95.3, glibc2.2.3, 5s on 128mb/256mbswap 800Mhz asus,trio3d (S3virge) from click to final display. Oh wait, that displays local web server page. 'k 4 secs then, maybe:)\n\nI had a power Windows user, who had _never_ seen Kde before, jump on my machine and show a newbie how to send email (I have single-click enabled). Also had a dummy windows user download internet .mp3 and play them on xmms. He remarked later,\"So if it runs Mediaplayer, can I install Reason on it, my machine is much slower than yours?\" I gave up explaining the issues and wine when he interrupted, \"Sure that's great, but I'd prefer a beer, thanks!\"\n\nSo what's with this Kde useability thing I've been reading about?\n"
    author: "Peter"
  - subject: "Re: I agree, but KDE also needs speed"
    date: 2002-07-02
    body: "Yes, yes, yes.  That is the reason why I am typing this from a GNOME 2 desktop in Galeon.  I love KDE, and I think that for my wife it is the way to go, but for bloated desktops GNOME 2 is much faster (Don't get me wrong, I like my bloated desktops - blackbox and the like are just too limited for me).\n<br>\nI have not compiled KDE 3 against gcc 3.1, but if there is a significant improvement I might switch over - and I develop most of my GUI apps in Gtk(wish Inti hadn't died).\n<br>\nThis is in no way to be a flame towards KDE developers, just an observation.  KDE does rock as a desktop, and - like I said - my wife finds it very useable."
    author: "Chris Parker"
  - subject: "SPEED"
    date: 2002-06-29
    body: "Oh, I would like to have speed, too. But this sort of drugs are still illegal in Germany.\nI don't think this speed discussions are necessary, I've read posts on other sites where people express their hate on the new GNOME (2.0), because not everybody seems to have the promised accelerations."
    author: "Somebody"
  - subject: "KDE Still needs to get Memory Efficient!!!"
    date: 2002-06-30
    body: "As Earlier posted by Asif Ali Rizwaan with the title \"Konqueror Failed a Test\" with KDE 2.2.x's Konqueror. I have tested again and found that Konqueror and KDE just get non-responsive if you open the 3.9 mb english to hindi (html)dictionary. If you open that with Kwrite there is no such trouble. Even Mozilla 1.0 hangs, so are Opera 6.03\n\nhttp://dot.kde.org/1005758245/1005822570/\n\nAnd the HTML file is at:\n\n http://sanskrit.gde.to/hindi/dict/eng-hin.html\n\nMy System Configuration:\n\nCeleron 622Mhz\n128 SDRAM\n20 GB HDD\nRedHat 7.2 with KDE 3.0\nNvidia TNT2 32mb \n4G / partition\n848 swap partition\n\nIt seems that KDE ignores stability alerts :(\n"
    author: "KDE User"
  - subject: "Re: KDE Still needs to get Memory Efficient!!!"
    date: 2002-06-30
    body: "on my pII-400 w/256Mb of ram running KDE from CVS with 5 konqi windows, compiling kdebase (in my 15 session konsole window), running kmail, ksirc, several kwrite windows, etc, etc.... it loaded just fine ... scrolling is beautiful as well ... before loading the memory pressure on this system was pretty high already due to the compile and other apps open so konqi didn't have a lot of room in that 256Mb to play with and it did just fine.\n\nthe only slow thing about it was how slow the file downalded as the server its coming from doesn't seem to be the best place to be getting 4Mb files from.\n\nso i suppose you can happily put this ersatz flaw aside now.\n\n> It seems that KDE ignores stability alerts :(\n\nyou mean like all the bugs that get closed on bugs.kde.org on a daily basis?\n\nwhatever."
    author: "Aaron J. Seigo"
  - subject: "speed"
    date: 2002-06-30
    body: "i agree to the speed issues above.\ni really love kde and i use it daily, but\n\n1. i am using the console (starting konqui takes too long!)\n2. i am using mozilla, because a. see 1. and the missing tabbed browsing (witch is in cvs and coming soon)\n\nhoping the speed problem will be adressed soon. for me, its one of the biggest problems of kde.\n\ngreeting from an very satisfied kdeuser\n-gunnar\n\nbtw: congratulation to brasil !!!!!"
    author: "gunnar"
  - subject: "Why don't we use the object-prelinking-embedding?"
    date: 2002-06-30
    body: "IF object-prelinking-embedding makes the KDE-sestem for at least 30 procent faster, why don't we enable this by default in KDE? If we already have the technology to make it a lot more faster, what is the reason for not using.\nThe devolpers should face the fact that KDE is to slow. At hate to say this but compared to Windows ME and 2000 the MS-software starts up a lot faster and works much quicker and nicer.\n\nThe reason I use Linux and KDE as desktop is because I like the philosophy of open-source, but in functionality and performance Windows 2000 is better, when using it as a productive desktop.\n\nHopefully the KDE-developpers are goiing to take the performance problem seriously, instead of hiding themselves behind all sort of tecnicall blabla!!\n\nTo be short: if the are technologies to improve the performance, the use them. Object-prelinking-embedding was already available before KDE 3.0, but not enabled in KDE. I like to know why.\n\nSome info about my system:\nMandrake 8.2\nAMD 450 mhz\n512 mb sd-ram\nGeforce2 graphicscard\n20 GB harddisk\n\n\n\n"
    author: "Maarten Romerts"
  - subject: "Re: Why don't we use the object-prelinking-embedding?"
    date: 2002-07-01
    body: "1) Object prelinking works, as far as I know, only for Intel x86 systems.  The KDE user- and developer-base isn't comprised purely of x86 machines. \n\n2) Object prelinking has been linked to some stability problems: just do a Google search and you'll see what I mean.\t\n\n3) Despite how much faster it can make things, objprelink is nothing more than a hack, and shouldn't be relied upon like so in the future.  The real work needs to be done in the dynamic loader itself.\n\nIf I have to choose between system speed and system stability, I choose the latter.  There's no point in having a system go fast if it's not stable -- Windows ME proved that.  (\"Boots 50% faster!\", Well, fine, but if it crashes 250% as much as Win98 did, there's no point...)\n\nAnd KDE HAS been taking the performance problem seriously, for quite a while now.  Do you know what kdeinit was designed to do?  Haven't you looked through the KDE mailing list archives?  Have you looked at Waldo Bastian's analysis of the bottlenecks in the linker for C++ systems, which is the first hit if you do a Google search for \"Waldo Bastian\" \"Link\"?\n\nI guess not."
    author: "dwyip"
  - subject: "Re: Why don't we use the object-prelinking-embedding?"
    date: 2002-07-01
    body: "Regarding 1, it's not just for x86, it's a gcc specific hack.  It's addressing a problem in GCC, specifically, how it handles objects in C++ code.\n\nOne of the really nice things about KDE is that it works across all Posix systems - AIX, Solaris, BSD, Apple's OSX, etc.  Most have compilers that handle C++ code efficently.  GCC does not (yet).  So, objprelink is pretty much a bandaid on a GCC problem.  And yes, the GCC team is working on making it handle objects in a better manner.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Why don't we use the object-prelinking-embedding?"
    date: 2002-07-01
    body: "> IF object-prelinking-embedding makes the KDE-sestem for at least 30 procent faster, why don't we enable this by default in KDE?\n\nBecause it's incompatible/obsolete with glibc 2.2.5 and recent binutils, read http://objprelink.sf.net/."
    author: "Anonymous"
  - subject: "KDE 3"
    date: 2002-07-01
    body: "Kde may be a little slower, but it is far more usable than gnome 2 hands down.\nOn my athlon 1800xp everthing is snappy and I have gnome2 installed as well, and I never use it.\n\nTony Caduto"
    author: "snorkel"
  - subject: "some truths thats you hide away"
    date: 2002-07-01
    body: "(previously banned abuser)\n<!--\nhttp://www.linuxandmain.com/modules.php?name=News&file=article&sid=112\n\nwhy not report this then? maybe because its not \"pro-KDE\"..keep patting yourself on the back, the facts are KDE is a slug now, Gnome2 isnt as bloated as you make out too be.. keep on dreaming that you have the \"ultimate desktop\".\n\nhttp://linuxtoday.com/news_story.php3?ltsn=2002-06-26-017-26-OP-GN-KE-0005\n\ncant stop free speech, learn about it dickhead.\n\n-->"
    author: "fuck_you_kde_webmaster"
  - subject: "Lies and accusations..."
    date: 2002-07-01
    body: "Both the links you give do not point to any factual statements. \nBoth fail to point out any shortcomings of KDE, but attack the developer community.\n\ndep is a person who thinks KDE is developed by Nazi Germans. What can you respond to such a person? How can one take crisizm of a Desktop Environment serious if the basis of the critisizm is such an absurd notion?\n\nI do not know sisiphus (boulder@mountain.com), but I think calling the justified pride of the KDE developers arrogance and telling them that Gnome is \"a work of art\" because of \"great user manuals\", \"comprehensive accessiblity\" and killer \"international support\" is no way to improve KDE or even Gnome2."
    author: "Moritz Moeller-Herrmann"
  - subject: "Does the physics geek inside you.."
    date: 2002-07-01
    body: "resist how he keeps calling inertia a force?  \n\nRepeat after me.\n\ninertia is not a force.\ninertia is not a force.\ninertia is not a force.\ninertia is not a force.\ninertia is not a force."
    author: "SilentStrike"
  - subject: "General KDE comment"
    date: 2002-07-01
    body: "I think that KDE apps and KDE itself will have a bright future as long as attention to overall UI issues is kept and even increased. Right now, KDE3.0x still has some \"geekish\" feel to it, albeit way, way, way less than abominations such as TWM and FVWM (sorry if I offended fans of these WMs). While KDE is no longer a primitive environment like it was in v1.0, right now what the end-user sees has become a bit too complex for its own good. \n\nA good hard look at the environment, from the \"K menu\" to various items in the Control Centre, including various standard dialog boxes should be had to see where there is an overload of options & features. Sometimes, really, less is more.\n\nI'm not saying make KDE 3.x less flexible and feature-rich, just make the UI less overwelming (sp?). It's just a question of how things are presented to the user (remember, not all users are developpers).\n"
    author: "Joseph B."
  - subject: "Re: General KDE comment"
    date: 2002-07-02
    body: "Perhaps KDE could use hidding-options in the menus (like the hidding-option in the menu of Kprint). With this concept we can make thing much more simpler (by shielding the advanced options), while preserving its flexebility at the same time."
    author: "Maarten Romerts"
  - subject: "Re: General KDE comment"
    date: 2002-07-03
    body: "Bingo. You got it.\n\nIn many places, there are surely some core options & features that the end-user *must* see, while there are others that only confuse quite a few users (the advanced ones). That would be a good approach to take.\n\n"
    author: "Joseph B."
  - subject: "two-faced"
    date: 2002-07-02
    body: "<a href=\"mailto:navindra@kde.org\">.</a>\n<!--\nhttp://www.linuxandmain.com/modules.php?name=News&file=article&sid=112\n\nwhy not report this then? maybe because its not \"pro-KDE\"..keep patting yourself on the back, the facts are KDE is a slug now, Gnome2 isnt as bloated as you make out too be.. keep on dreaming that you have the \"ultimate desktop\".\n\nhttp://linuxtoday.com/news_story.php3?ltsn=2002-06-26-017-26-OP-GN-KE-0005\n-->\n"
    author: "harhar"
  - subject: "Re: two-faced"
    date: 2002-07-02
    body: "I like KDE.  This is a KDE site.  The articles you quote have only one semi-point to them:  KDE is slow.  Check out the developer and user mailing lists, and you'll see that's a top priority right now, and has been since KDE 2.0.  Other than that, they are pointless as an emotional \"Windows Sucks!\" or \"OSX Rules!\" essay - they carry no information.  From reading both of these, I can glean totally nothing about Gnome2, or why it might be better.  \n\nNow, having said that, I'll ask you (and all other Gnome users and fans) to seriously search for a good essay on the strengths of Gnome compared and contrasted to KDE.  The final result can be \"Gnome is a better system\".  For that matter, I'd like to see articles on KDE compared to Windows XP, compared to OSX and compared to anything else out there.  All of those articles would be on topic on a KDE site.  I'm sure Navindra wouldn't mind publishing such an article, and I *know* that many people here would be interested in reading a well thought out essay that compares KDE against other desktops - even one that comes to conclusions unfavorable to KDE.\n\nAfter all, a well thought out essay on KDE's flaws can turn into a roadmap for future development.  And that's what open source is all about.  Don't forget - we're all on the same team.  Gnome's early html render engine was based on kfm's engine, and KDE and Gnome have worked together towards interoperability on quite a few levels.  There is no \"winner\", except for the user who has a desktop he likes, and a developer who can be proud of what he or she has created.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: two-faced"
    date: 2002-07-02
    body: "A quote from your second url: \"... but the KDE project looks increasingly like a house of cards sitting on a dodgy foundation.\"\nDid you ever write a single line of code for KDE ?\nThen you would know how ridiculous wrong this statement is.\n\nIMO KDE has currently only two problems: speed, and maybe too much options/configurability.\n\nIf Gnome 2.0 turns out to be better, we will do our best to catch up.\nWe both want to \"win\" :-)\n\n\"Ueberholen ohne einzuholen !\" ;-)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Stability needed"
    date: 2002-07-07
    body: "The base desktop is very stable (The kicker, icons, WM) But some of the applications are not. Konqueror tends to crash a lot and it even hangs on pages that it shouldn't. Konqueror isn't very stable when it comes to HTML, and it could do with faster response times when looking through large image diretorys. Im even losing confidence in Konqueror and I am using Mozilla 1.1a instead. which is the most stable Alpha program I've ever seen. I think if The KDE team added a crash reporting system like Mozilla and WinXP has it would help find those stability bugs."
    author: "norman"
  - subject: "KDE"
    date: 2003-03-30
    body: "I think KDE is alot better than it was 4 year's ago. Develouper's should come to relize the computer's are getting alot faster thing's are getting alot better, But just because the speed is there doesn't mean they need to add another line of code. They should refine some of what's already been written to optimise for more responsive load times. Also the Xine video player is awesome just they forgot that some dvd movie's have special features and button menu's there is no way from what I could tell to select those. That's a feature that's needed to enjoy dvd music video cd's. \n\nSystem:\nMandrake 9.0"
    author: "Xarva"
---
In what started as a series on KDE versus GNOME, columnist <a href="mailto:nicholas@petreley.com">Nicholas Petreley</a> has concluded the run with <a href="http://www.linuxworld.com/site-stories/2002/0628.kde.future.html">an article</a> that details why he feels the future for KDE/Qt is so bright.  He points out a few shortcomings in KDE (like the lack of tabbed browsing in Konqueror or underlining of typos in KWord -- both in CVS!) and says he feels confident that the KDE developers will fill those gaps. <i>"Put simply, the KDE class libraries and examples are a brilliant testimony to reusable objects done right. Features such as the sophisticated file dialog and toolbar functions are obviously a part of the standard KDE class library, which is why most KDE applications now include them. If you upgrade the file dialog, all applications that use it get upgraded automatically."</i> Basically, it's a essay for the layman explaining why a good foundation of coding tools is important for the final product.
<!--break-->
