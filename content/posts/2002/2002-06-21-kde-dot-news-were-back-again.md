---
title: "KDE Dot News: We're Back, Again"
date:    2002-06-21
authors:
  - "numanee"
slug:    kde-dot-news-were-back-again
comments:
  - subject: "Congratulations"
    date: 2002-06-21
    body: "It's too bad you guys can't afford your own server and bandwidth, to avoid some of the stress."
    author: "Neil Stevens"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "Oh, but this situation is very close to ideal.  Dedicated server, open bandwidth, and all managed by good KDE friends... :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "This is indeed the greatest of news. :-) I've been trying to limit my viewing of the Dot to once or twice a day so as not to consume too much of the Dot's bandwidth... now I can come check for articles and comments every few hours again. Yay!\n\nNow if only FreeKDE.org could come back online again, I'd be a very happy camper... "
    author: "Timothy R. Butler"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "Well, I believe Neil has something up his sleeve... but you didn't hear it from me!  ;-)"
    author: "Navindra Umanee"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "If I didn't know better I'd swear you were just giving me opportunities. :-)\n\nWe'll see if we have anything to show on Monday.  I say we just to give people a hint that it's *not* freekde or anything like freekde that's coming."
    author: "Neil Stevens"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "Wow... this new server is so much faster.\n\nAre you sure you really need to do the hardware upgrades?"
    author: "Neil Stevens"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "Must be the SCSI drive or something, then again we're running nothing else but the dot at the moment.  It's very cool to see that load is close to 0 on the new server *and* on bero.org.  Now there's first.\n\nThe hardware upgrade is already scheduled.  We should be going down in 3-4 hours from now...  Not sure how they plan to do the harddrive upgrade exactly.\n"
    author: "Navindra Umanee"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "The upgrade is planned for this afternoon... PeM is gone to the netcenter with the new material.\nYou can expect some changes in the afternoon, it should be done for the end of teh day.\n\nI'm really glad (and proud) that we help u and the community.\nKeep goin'\n\nRegards.\n\nThomas.\nAD of the printed magazine.\nAbsolutly not a technician (i'm lost at many times but...)\n;o)"
    author: "Thomas CORVOYSIER"
  - subject: "Re: Congratulations"
    date: 2002-06-21
    body: "It worked wonderfully well.  Robert/PeM work fast!  There was barely 10 minutes of downtime and they added 500M more of RAM plus a brand new 36G SCSI.  So beautiful.  This has given new life to the dot -- thank you!\n"
    author: "Navindra Umanee"
  - subject: "Lycoris and KDE3"
    date: 2002-06-21
    body: "Dear Navindra,\n\nNext time you have the ear of Jason Spisak, please could you ask him to include KDE 3 with D/LX?\n\nCheers,\n-g-"
    author: "garyK"
  - subject: "Re: Lycoris and KDE3"
    date: 2002-06-21
    body: "They are actually planning, and might do so soon. But it is hard moving an enitre distribution to an entirely new desktop. For one, Lycoris is very integrated; it is almost impossible to upgrade something without breaking. But then, I don't really like Lycoris anyway - it imitates Windows XP. Most users who don't depend on Windows-only applications (or apps with no altenatives on Linux) most of the time don't know how to use Windows. And those who do know how to use Windows most of the time depend on a Windows-only application with no altenative in sight on Linux.\n\nBut while we are asking, lets not only ask for KDE 3.0, but for Konqueror to be the default mail client."
    author: "Rajan R"
  - subject: "spassiba bolshoi to Cyberbrain"
    date: 2002-06-22
    body: "... or whatever it is in French. ;-)\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: spassiba bolshoi to Cyberbrain"
    date: 2002-06-24
    body: "A ton service ;-)\n\nPe"
    author: "PeM"
  - subject: "Re: spassiba bolshoi to Cyberbrain"
    date: 2002-06-24
    body: "\u00e7a explique aussi pourquoi le nouveau Dot est \u00e0 pr\u00e9sent aussi rapide en France...\n\nis it faster for everybody, or just for us French people ? transatlantic links are sometimes a bit slow...\n\nand, as he says, merci beaucoup :-))"
    author: "loopkin"
  - subject: "Re: spassiba bolshoi to Cyberbrain"
    date: 2002-06-24
    body: "It is extremely fast everywhere!   At least everywhere I've checked...  We are connected to a huge network pipe at Cyberbrain.\n\nHopefully we can keep this up as the server gets more and more busy.  Already more sites are being transferred..  :)\n"
    author: "Navindra Umanee"
  - subject: "Flatforty"
    date: 2002-06-27
    body: "> Flat Forty is generated statically every 30 minutes.\n\nPerhaps there is now enough CPU power to increase frequency?"
    author: "Anonymous"
  - subject: "Re: Flatforty"
    date: 2002-06-27
    body: "Perhaps, it's definitely *much* faster to generate now, although this server is running more than just the dot already.  Not sure it's necessary to update it more often though.  Is flatforty popular with anyone here? :)\n"
    author: "Navindra Umanee"
---
We run into the best of luck at KDE Dot News.  This time the air-conditioner in our server room died, resulting in a temperature of 52 degrees Celcius and a core meltdown.  Since we had also been suffering from limited bandwidth for a while now -- since the week of the KDE3 release when we managed to kill our host's pipe -- we decided to take this opportunity to move to a new hosting solution. Happily, we were awash with wonderful offers for help and support of all kinds from the community. It was an extremely tough call but we finally settled for a generous offer from CEO Guillaume Esnault, Robert Royer and <a href="http://pem.levillage.org/">Pierre-Emmanuel Muller</a> (also involved with the KDE French <a href="http://i18n.kde.org/">translation team</a>) of <a href="http://www.cyberbrain.net/">Cyberbrain</a> for professional ISP hosting in a securised datacenter, including a dedicated Compaq ML 330 Proliant (as <i>not</i> seen in Star Trek) directly connected to two 9Gb black fibers (open bandwidth use), full support, backups, and 24/24 monitoring.  A word of <i>warning</i> though: Expect some minor downtime as we further upgrade the hardware over the coming days. On the bright side, once we are settled, we will be able to support other KDE sites and services on this baby.  Read on to see why we should be proud of this community. <b>Update: 06/21 14:31:22 CEST</b> by <b><a href="mailto:navindra@kde.org">N</a></b>: The upgrade has been successfully completed.  Say 'bonjour' to all of 640 MB of RAM and a new 36 GB SCSI harddrive!  Thank you, <a href="http://www.cyberbrain.net/contacts.cbb">Cyberbrain team</a>.
<!--break-->
<p>
First of all, a <i>huge</i> thanks goes out <a href="http://www.bero.org/">Bernhard Rosenkraenzer</a> and <a href="http://www.redhat.de/">RedHat.de</a> who provided, and still provide, us with such great service and support for <a href="http://dot.kde.org/991372945/">over a year</a>, and thanks to <a href="http://www.mieterra.com/">Andreas Pour</a> of <a href="http://www.kde.com/">KDE.com</a> for providing, and still providing, resources for offsite backups.
</p>
<p>
Also, in no particular order, we would also like to thank:

Laurent Rathle of <a href="http://www.kde-france.org/">Kde France</a>, Frank Dekervel of <a href="http://www.kuleuven.ac.Be/">Katholieke Universiteit Leuven</a>, Chris Herrnberger, Jarl E. Gjessing of <a href="http://www.yaeda.org/">yaEDA</a> and Copenhagen Business School (<i>special thanks</i> for putting together a wonderful offer for a dedicated server with awesome bandwidth, and being a pleasure to work with), David McGlone of <a href="http://www.edificationweb.com">Edification Web Solutions</a>, Ulrich Wagner for offering free server parts, Jason Spisak of <a href="http://www.lycoris.com/">Lycoris Desktop/LX</a> (now with extra Vitamin C!) for a generous offer of two machines and Zope expertise, Nick Mailer of <a href="http://www.positive-internet.com/">The Positive Internet Company Ltd</a> (with a <i>truly</i> mouth-watering offer of a Debian GNU/Linux RAID system with a gigabyte of RAM, nightly backups, about 100 gigabytes of storage and up to 100 gigabytes of data transfer per month), Fredrik Danerklint, and of course our KDE friends Bernhard Rosenkraenzer, Klaus Staerk, Karl-Heinz Zimmer, Dirk Mueller, Martin Konold, Andreas Pour for various help and alternate efforts.
</p>
<p>
This is not an exhaustive list, but it certainly feels good to know we have so many friends ready to step up and help at the drop of a pin!
</p>