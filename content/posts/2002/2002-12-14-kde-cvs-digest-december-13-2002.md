---
title: "KDE-CVS-Digest for December 13, 2002"
date:    2002-12-14
authors:
  - "dkite"
slug:    kde-cvs-digest-december-13-2002
comments:
  - subject: "nice"
    date: 2002-12-13
    body: "Thank you  for the engaging manner in which you write your reports.  I almost feel like I'm right there.  Are you a professional writer?"
    author: "anonymous"
  - subject: "Re: nice"
    date: 2002-12-13
    body: "Actually, I'm a dirty fingernailed refrigeration mechanic who would have gotten into professional programming if I could have stayed in the small beautiful British Columbia, Canada town, and be able to support my family. Nelson BC is a very hard place to move away from for any reason.\n\nAnd no, I'm not a professional writer. Just someone looking for a way to give back a little to a community that has given me so much.\n\nGlad you enjoyed the digest.\n\nDerek\n\n"
    author: "Derek Kite"
  - subject: "Kernel cousin"
    date: 2002-12-13
    body: "Is this related to CVS digest the successor of KDE Kernel cousin?"
    author: "Hakenuk"
  - subject: "Re: Kernel cousin"
    date: 2002-12-13
    body: "There's no relation between the two.  One seems more focused on CVS updates and the other one on general mailing lists news.  Both are valuable although it might be nice to see some cooperation between the two.  There hasn't been a KC KDE in a while, I hope they aren't discouraged."
    author: "ac"
  - subject: "Re: Kernel cousin"
    date: 2002-12-16
    body: "Nope, we talked to Derek and thought about integrating the two. Currently I just don't have time to work on KC KDE. I have final exams to worry about right now and as you can read in this CVS-Digest in the few spare moments I have I'm working on some other stuff. I'm starting the winter break in four days (wish me luck ;) ) so KC KDE will return next week.\n\nZack"
    author: "Zack Rusin"
  - subject: "Why isn't here news about KDevelop?"
    date: 2002-12-16
    body: "Why isn't here news about KDevelop?"
    author: "F@lk"
  - subject: "Re: Why isn't here news about KDevelop?"
    date: 2002-12-16
    body: "Kdevelop is at a stage (correct me if I'm wrong) where a major release is out on alpha, most of the big features are finished, now incremental improvements are being done. Once a beta release is out, the focus moves to bug fixes, which will show up.\n\nThere are some things I notice for next week that will be highlighted.\n\nYou are not the only ones not getting attention. Keditbookmarks is in process of a rewrite, Karbon has had important updates and features added. Kexi is improving dramatically. Etc. \n\nI'm trying to automate the repetitive stuff to leave more time for research. The bug list is quick and easy now, except for the non-numbered bug fixes and features where I link to the commit in lists.kde.org. I have to do a manual search using the limited search at that site, which takes alot of time. Google doesn't have it, since the stuff very recent. \n\nAny suggestions? I haven't found a way to tie the email I get from the kde-cvs mailing list to the archives.\n\nI could link to the diff in webcvs.kde.org, but that would work only if a single file was changed.\n\nDerek"
    author: "Derek Kite"
---
The latest KDE-CVS-Digest is <A HREF="http://members.shaw.ca/dkite/dec13.html">out and ready</A>.
This week we cover everything from the security audit, a new <a href="http://lists.kde.org/?l=kde-core-devel&m=103952184519380&w=2">release dude</a> for KDE 3.2, bugfixes, and new features in <a href="http://kmail.kde.org/">KMail</a>, <a href="http://kopete.kde.org/">Kopete</a>, <A href="http://www.konqueror.org/">Konqueror</a>, <a href="http://www.koffice.org/krita/">Krita</a>, <a href="http://konsole.kde.org/konstruct/">Konstruct</a> and klaptopdaemon.
<!--break-->
