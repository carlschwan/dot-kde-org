---
title: "KDE Core Services: Trouble In Paradise"
date:    2002-02-08
authors:
  - "rmoore"
slug:    kde-core-services-trouble-paradise
comments:
  - subject: "CVS is readonly for now"
    date: 2002-02-08
    body: "The CVS is available again (at its new location, so remove any /etc/hosts\nentries!), but due to the lack of logging for cvs commits (mail setup issues),\nit is currently read-only.\nAny commit will fail, so don't lose time typing log messages just to see\nthe commits being rejected :}\nThis will hopefully get sorted by tomorrow."
    author: "David Faure"
  - subject: "Re: CVS is readonly for now"
    date: 2002-02-08
    body: "First, kudos to everyone who worked so hard to resolve the situation. I am eagerly looking forward to Beta2. Now, I have a lot of questions :)\n\nDoes this mean that the CVS has permanently moved to SuSE? What kind of hardware did IBM donate? How did they get involved? Did they volunteer or someone approached them? Where is \"master\" situated? Can we maintain shadow mail/list/CVS servers, which are kept in sync, and then brought into service just by switching the DNS records?\n\nHope someone answers these questions."
    author: "Nadeem Hasan"
  - subject: "skr1p4 k1ddi3z must die!"
    date: 2002-02-08
    body: "A DoS attack is trivial?  Does the CVS server suffer them often?"
    author: "not me"
  - subject: "Re: skr1p4 k1ddi3z must die!"
    date: 2002-02-08
    body: "Oh yeah, and thanks to those working to fix the problems, and thanks also for this informative Dot story.  It's always nice to know what's going on."
    author: "not me"
  - subject: "Delay no problem for me."
    date: 2002-02-08
    body: "I'm still enjoying kde2.2.2 - rock solid for me, so a delay is irrelevant to me at this point. It really sucks about the DoS, I'm glad IBM stepped up to the plate and supported the KDE developers.\n\nConsidering what you guys have gone through and the quality of past KDE releases, I'm sure KDE3 will be a awesome!"
    author: "jorge"
  - subject: "Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Who could have a motive to do such a thing? It boggles the mind, I can't think of anyone, can you?"
    author: "reihal"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "I can.  Steve.  It has to be Steve.                                                                                                                                             "
    author: "jmalory"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "yes, we have to find this \"Steve\" character since we ALL KNOW WHO \"STEVE\" IS :) Jobs? Steve Betcher? What Steve? Steve Edwards?"
    author: "Nick Betcher"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "I don't know what he meant, but I imagined the Dell commercail Steve....  But whatever...."
    author: "Ill Logik"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Steve Ballmer o fcourse :)"
    author: "fredi"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Yeah right... sheeesh!  Like where is Ballmer going to get a box that can DoS a Unix server?\n\nOh God, maybe their using the FreeBSD boxen over on HotMail!  Eeeek!"
    author: "Michael Collette"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-10
    body: ">Oh God, maybe their using the FreeBSD boxen over on HotMail! Eeeek!\n\nDo you think they have someone who can use a Unix box?\nOh my god, i'm getting scared :-)"
    author: "Data-Phil"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-14
    body: "Steve Erkel."
    author: "just some joker"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2004-03-22
    body: "DUDE my dad's name is steve betcher"
    author: "Tyler "
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "There are always guys who can only enjoy life by destroying stuff and killing people."
    author: "Lenny"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "So it was probably an American then? Well that's alright."
    author: "Malcolm Agnew"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "...or a german"
    author: "e.a"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Just for the record I'm from England not Germany. I thought that I was just issuing a fairly mild rebuke at the concept of relating a DoS with killing people. I didn't really want to start a flame war.\nActually what interests me most at the moment is whether kde3 will correct the broken kio_help in KDE2.2.2.\n\n"
    author: "Malcolm Agnew"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "kio_help is broken? News to me, it's working perfectly happily on my (and many other) 2.2.2 installations.  Have you reported a specific bug on bugs.kde.org that we can look into?\n"
    author: "Lauri Watts"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Search on google:\n\ngg:klauncher said Error loading kio_help\n\nand it takes you to the corressponding kde bug report quicker than you will find it from the kde site."
    author: "Malcolm Agnew"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-09
    body: "i had similar problems with kde2. they seem to have gone for good with kde3, as the helpcenter now works 100% reliably here. yay for progress!"
    author: "Aaron J. Seigo"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-09
    body: "bugno: <a-number> would get you there there even quicker. \n\nFor help to work, you need libxml2 (this is not the same thing as libxml) and libxslt.  These are dependencies for kdebase - which versions do you have installed? \n\nYou need to have installed the help - some early versions of the RedHat RPMs inadvertantly didn't install it. Do you have the most up to date ones available?  I'm assuming you have RedHat, as the bug report above is referring to RedHat. Do you have anything in $KDEDIR/share/doc/HTML/<yourlang>/<appname>/ ? You should have, depending on the application, an index.docbook, an index.cache.bz2, and perhaps some .png files if the document has screenshots.\n\nIf you go into one of these directories and run \"meinproc index.docbook\", do you get any more error messages?\n\nThe bug reports referenced above look rather like packaging errors, but there's not enough information provided to even investigate them."
    author: "Lauri Watts"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "...or a german"
    author: "e.a"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Stupidity has no boundaries..."
    author: "guillomovitch"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "I think script kiddies are universal. It's the 21st century equivalent of neighborhood vandalism. Unfortunately DoS attacks effect thousands of people and are a great drain on resources."
    author: "Sean Pecor"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "As an American I could choose to be offended or to chuckle. I'll chuckle ;)"
    author: "Shamyl Zakariya"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-08
    body: "Good for you that you can chuckle. Can we, nonetheless, stop this crap? I actually like a good brawl (a good one, I said ;-), I like hefty jokes. But jokes like this one are tasteless and bound to get people offended.\n\nLet's stick to puns the targets can laugh about!\n\nUwe"
    author: "Uwe Thiem"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-09
    body: "> Let's stick to puns the targets can laugh about!\n\nLike: Are Uwe Thiem or are Uwe Thus?... as the president might say."
    author: "Anonymous Coward"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-09
    body: "Looks like you've got a good brawl going on below -- once again, the Gnome/KDE war flares up ;)\n\nSilly, silly people."
    author: "Shamyl Zakariya"
  - subject: "Re: Who's behind the DoS attack, then?"
    date: 2002-02-12
    body: "As a German, I choose `leo:chuckle' which returns\n\tglucksen, kichern, leise lachen.\n\nThank you for ALT+F2 !!!"
    author: "AC"
  - subject: "Microsoft sales personel are descending on SAfrica"
    date: 2002-02-10
    body: "... this was timed to coincide."
    author: "NameSuggesterEngine"
  - subject: "Desktop look..."
    date: 2002-02-08
    body: "I have been so blinded by KDE for the last year that I've forgot to look on \"the other side\".\nNow it appeares even the GNOME guys do vector themes, AA, nice file management etc - and how nice it can look:\nhttp://jimmac.musichall.cz/themes.php3?skin=2\n\n...it's better than good"
    author: "ealm"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "on the off chance you read the actual notes about it you will see that he DOESNT reccomend this to anyone on less than an intel 400Mhz and says that this is very slow on a PPC.\n\nhrm, sounds like a real show stopper to me, seeing as I can get 90% of KDE's effects to work very fast on my 400Mhz PPC, and resonably on my 333 AMD K6.\n\nlets face it, KDE 3.0 is a marked improvment of speed and features, just becase GNOME has icons that can change size well dosent mean that they better.  I mean personally I think haveing a usable file dialog is 10000x more important that some silly eye candy.  For giggles ask a gnomer to change their wallpaper from the commandline... without restarting anything, when they cry that it cannot be done, fire up DCOP ;)\n\nI think you will find that KDE 2.x and now KDE 3.0 are far more usable than anything the GNOMEs have cranked out thus far.\n\njust my 2c\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "I'd agree with you, but I'd also like to have scalable icons in KDE. It's one of the things OSX has that I wish I had under kde. Hell, a scalable kicker would rock!"
    author: "Will Stokes"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "so please submit a patch.\ni am busy makeing it possible to script KDE applications with any scripting language you chose.  i think IBM/HP and compaq want that a little bit more than icons that grow and shrink.  i think what tacket, qwertz and other provide is adiquate.\n\ni would not trade dirk and davids work on KHTML for any eyecandy no matter what it does for me.  \n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "Amen!\n\nSo far as scaleable icons, when I have added new ones in manually (stuff only to show up on my desktop) I usually only make one big one (48x48).  From what I'm seeing here, they scale quite nicely already.  No, it's not the super spiffy kind of scaling you'd get with vectored graphics... so?\n\nAs a user, I want rock solid KHTML, Javascript, improvements to the Address book, better Palm integration, performance boosts.  I want, I want, I want! :) These are the make and break points of KDE.  Besides, I happen to strongly prefer Liquid to Aqua after using both.  KDE already looks very very sweet!\n\nAs much as I'm looking forward to seeing KDE3, what I've got now is working pretty nicely.  Take your time, miss some promised release dates, and do us all proud!  You KDE developer types have gotten us all used to them kinds of things, especially the proud part.\n"
    author: "Michael Collette"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "use evolution then."
    author: "joe99"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "> For giggles ask a gnomer to change their wallpaper from the commandline... \n> without restarting anything, when they cry that it cannot be done, fire up DCOP \n\nAnd by the time DCOP has fired up, esetroot is finished and a background is changed. Wow. Being able to change the background from the commandline. I feel more productive already.\n\nThen we think of the applications that wipe the floor with anything KDE has...Evolution...hmm, Gnumeric...Abiword...I'll keep quiet now shall I?"
    author: "Gnomer"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "nah, don't stop there, add the gimp, galeon, gnucash, mrproject and a little thing called Red-Carpet. (great apps, gotta love 'em)"
    author: "dave"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "KSpread, - Like GNUmeric only it dosent crash as much and has better integration\nKWord - Like Abiword only more features, granted thats not that hard\nKPresenter - Like... well... i dunno\nKivio - Like dia only with a more powerful python/xml based shape engine\nKate - what gedit always wanted to be\nQuanta - I think bluefish tries to be an html editor\nKDevelop - makes GIDE look like a booger, but then again no one actually writes\n\tgnome code, it just happens...\nKonqi - about 1/2 the size of mozzila, which is not even a true gnome app anyway,\n\tthey just wish it was\n\nFYI not only to all of these talk together but they are accessabe with dcop.  GNOME is a lose hack of any application they can get ahold of, while KDE seems to have some method behind its development.  Oh and that red carpets a real winner... I watched it hoze my mandrake box...  but i give them points for trying...\n\nKDE development may not have the alure to 1st year CS dropouts, but it seems to have a fairly stable following..."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "> KSpread, - Like GNUmeric only it dosent crash as much and has better integration\n\nHave you actually tried Gnumeric > 1.0 with Guppi. It's excellent.\n\n> KWord - Like Abiword only more features, granted thats not that hard\n\nAbiword at least started from a stable base, i.e. the ability to do WYSIWYG printing. I can't believe anyone would write a word processor and not consider the ability to print the most vital feature! Don't knock Abiword, it may be simple - but it does the job 99% of the time.\n\n> Kivio - Like dia only with a more powerful python/xml based shape engine\n\nSorry - but have you actually tried to use Kivio or Dia? I actually decided to use to produce some professional flow charts for ESA documentation and they both suck.\n\n> Konqi - about 1/2 the size of Mozilla\n\nBut Galeon is the gnome browser and to be honest - it's what I use all the time now. \n\nAlso Evolution beats the crap out of any KDE mail/pim program and I seriously think that KDE should dump kpilot and go with gpilotd which actually works (Evolution calendar syncs beautifully with my Palm).\n\nFor the record I use KDE2.2.2 as my desktop - but that doesn't precude me from using gnome apps if I think they're better for my productivity. I'm looking forward to the next release of KOffice alot, but at the moment - I'll stick with  whats best for the job.\n\n"
    author: "Dr_LHA"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "> Abiword at least started from a stable base, i.e. the ability to do WYSIWYG printing. I can't believe anyone would\n> write a word processor and not consider the ability to print the most vital feature! Don't knock Abiword, it may be \n> simple - but it does the job 99% of the time.\n\nI have to disagree.  Anything that Abiword is adequate for, I've found, I might as well use vi for.  Any non-trivial document I've ever written requires features not found in Abiword.\n\nThat said, KWord isn't much better.  My wife, an author, can't use KWord because, as of the latest stable release, printing is still screwed up.  However, at least it has styles, an absolute minimum for any document over a page or two.\n\nOne thing I *really* like about Abiword is the export to docbook.    I wish KWord would adopt this.  If I had more time, I'd write a couple of XSLT stylesheets to convert between the two.  Actually, Abiword's export and import facilities far outshine KWords, unless you're only interested in MSWord imports, in which case KWord's is marginally better, and both are vastly inferior to StarOffice.\n\n> For the record I use KDE2.2.2 as my desktop - but that doesn't precude me from using gnome apps if I think they're \n> better for my productivity. I'm looking forward to the next release of KOffice alot, but at the moment - I'll stick \n> with whats best for the job.\n\nIMHO, unless you need rigid control over formatting, KWord is much superior to Abiword.  Printing is sufficient, as long as (as I said) you aren't trying to do a resume, or something that requires true WYSIWYG.\n\nBut, in the end, StarOffice still beats the pants off of either, in both printing and features.  It's a memory hog, but it is the only real alternative to MSWord for people who's jobs depend on a good word processor."
    author: "Sean Russell"
  - subject: "Re: Desktop look..."
    date: 2002-02-14
    body: "   One thing I *really* like about Abiword is \n   the export to docbook. I wish KWord would adopt this. \n\n\nSee last section in \n   http://bulmalug.net/body.phtml?nIdNoticia=1061."
    author: "Ricardo Galli"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "I've tried koffice numerous times in the past, and it has never lasted long enough for me to be able to create a document and save it (kword, kspread, kpresenter...), so i use gnumeric and abiword, they're fast and they just dont crash for me - it's interesting that you've tried gnumeric and found it unstable, have you tried a recent version? i use the latest ximian version from red-carpet on redhat 7.2 and it's really stable.\nTrue abiword is lacking some features, but what it does it does well, and it has .doc export.\nI've never really used gide myself, so i can't comment on that but anjuta is ok (and more like kdevelop) - however i still prefer codewarrior.\nQuanta - yup that's good, (although if i just want to edit html bluefish is also very good)\nkonqueror is fine as a file manager, but in my opinion it's not the best web browser - that's galeon.\nKivio - never tried it, but i have used dia and it's a bit annoying.\nRed-Carpet - it works for me and it's great, but i do get the impression that it's not so great for mandrake.\n\nIt really sounds like we're not comparing equals here, kde's unstable for me (on redhat 7.2, with redhat updates) and it dsounds like gnome's unstable for you on mandrake.\nIt's a real shame that this should be the case because there are 2 great desktops out there and it looks like the distributors don't put enough effort into testing both of them. I guess i'll have to wait and see if kde3's any better and you can try gnome2 when that comes out."
    author: "dave"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "I'm a kde user, so I think I'm not too gnome oriented, but what you say is really shoking, as it's just wrong.\nKspread/Gnumeric: Not only it has less functions that Gnumeric, but I can crash it in less than 5 seconds (just adding some charts in it). Gnumeric is one of the rare Gnome/Gtk apps that really has a lead compared to KDE equivalent.\nAbiword/Kword: You cannot ignore that abiword is much better at preview and wysiwyg than Kword.\nKdevelop vs Gide. Shouldn't you compare it to glade?\nQuanta: If you are talking about Quanta +, I have to disagree. The functionalities of bluefish actually work. Some functionalities of quanta are just checkboxes without effect. Bluefish is really far better than Quanta+.\n\nKonqui may use half the size of Mozilla, it is not a comparble application! Why do you compare Konqui to Mozilla?! Can you write email, do icq and compose html pages in Konqueror? I'm not even sure it can display PNG transparency. TIP: Nautilus\n\nAnd I'm a KDE user. I happen to use some gnome apps because they are better for some situations."
    author: "oliv"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "I think Anjuta is a lot better that kdevelop ... although in it's early stage.\nBut probably it's a matter of preference - I prefer an IDE whith a great Editor\nsince this is the part I use at most when programming.\nThe kdevelop team did a great job but the editor is way behind what other\nIDEs provide.\nAnd I miss ksodipodi ;)"
    author: "mike.s"
  - subject: "Re: Desktop look..."
    date: 2002-02-13
    body: "nah, kdevelop is much more like THE ide that all other ide's live up to, MS VisualStudio. It's also much more mature and stable in development than something like ajunta, but then again, it has been development in 4 years. I agree with you in the editor part tho, although scintilla, which is used by ajunta, isn't THAT much better. Once kdevelop uses the kate part (soon), it'll blow everything else away again ;)"
    author: "purity"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "Before you complain that the theme is too slow try it. In this case slow on a 400Mhz machine should be read  \"as slow as kde2 on the same machine\", i just tried it - maybe you should too."
    author: "me"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "All of you, grow up.  This whole thread sounds like, \"My desktop can beat up your desktop.\"  Some like KDE, and some like GNOME.  It's nice to have the choice.  Even better, it's nice to have the choice of two really great FREE desktop environments.  Use the applications that you think work the best or that you are most comfortable with.  Knocking someone else's work is just rude (and this applies to everybody)."
    author: "andrew"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "I don't think any of us would argue that the GNOME developers know how to make a very attractive interface.  All of it *looks* great, but that's where my praise stops.  You can have the hottest, most attractive looking desktop in the world, but if it doesn't make you productive than what's the point?  Now I know a lot of users just want to type their essays, listen to mp3s and chat on IRC, but for those of us with work to do, GNOME is pretty but not pretty useful."
    author: "Etriaph"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "Sorry, screenshots I've seen of gnome don't impress me.  I find KDE's look (Highcolor default with mwm like window decorations) to be the best possible interface around."
    author: "KDE User"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "Would you care to give an example of what, in terms of productivity, KDE offers that you can't get on Gnome? The biggest slap against Gnome was that it was ugly, not nearly as pretty as KDE. KDE has had the reputation of looking really nice, but being a bear when it comes to resources. Now Gnome comes out with one theme that is system intensive and you start bashing it productivity. So please enlighten us with all of Gnome's productivity shortcomings."
    author: "Luther"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "With Gnome, I can't create a PHP script on my desktop, then, using DND, put it on ftp://login:pass@ftp.mywebsite.com and vice versa.\nI can't pop down ALL my windows in one time to, then, open just the one I need thanks to the dekstop icon.\nI even can't drag a desktop from Nautilus and drop it on the desktop to make a link to it. DnD MOVEs the folder ! (I tried that on Mdk8.1 out of the box)\nMost of the MIME-Types aren't well defined and it's very difficult to make associations (I mean more than 2 clicks for an association is way too much).\n\nThose 3 things definitely make GNOME unuseable for me at daily work (I'm a webmaster). Don't tell I'm a troll because I really like GNOME. I find it pretty, I find it fast but I just really can't use it.\n\nI think the problem with KDE (for me) is that the default look is nice but not really original: it looks like windows 2000 with a Microsoft Plus theme if you see what I mean. Of course, I exagerate but when I use GNOME (mainly at home, where I only listen to MP3s, surf using Mozilla and watch DVDs) or WindowMaker, I feel like I'm using something original. \n\nKDE just doesn't need to look like MacOS X or WinXP, it just needs to be visually original but keep a classic feel (CTRL-C / CTRL-V, konqueror ala MSIE etc...).\n\nI repeat, I don't think that KDE isn't pretty. The default theme is an enhanced version of Windows 2000 look and, with High Performance Liquid, you'll get Mac OS X. BUT there is no nice and original theme for KDE to replace the default one.\n\nSorry if I've said bullshit about GNOME but I just say what I noticed using GNOME on Mandrake 8.1 .I can't say if it's Mandrake's fault or GNOME's one."
    author: "Julien Olivier"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "I personally keep switching back and forth.  What I find amusing is the fact I typically end up using KDE more but 90% of the programs I use are built around GNOME because I cannot find ones for KDE that work for me as well.\n\nexamples:  PAN, GQview, gftp, xchat, glitter, plus a few more."
    author: "JM"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "\"With Gnome, I can't create a PHP script on my desktop, then, using DND, put it on ftp://login:pass@ftp.mywebsite.com and vice versa.\"\n\nAbsolutely untrue, i just did it.\n\n\"I can't pop down ALL my windows in one time to, then, open just the one I need thanks to the dekstop icon.\"\n\nI'm not aware of an icon to do this, but it is simple to bind a key in sawfish to minimize all windows.\n\n\" I even can't drag a desktop from Nautilus and drop it on the desktop to make a link to it. DnD MOVEs the folder ! (I tried that on Mdk8.1 out of the box)\"\n\nShift-drag makes a link, ctrl-drag makes a copy, alt-drag asks you which you want with a menu.\n"
    author: "joschi"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "...and if pressing even one key's too much for you dragging with the middle button also gives you the choice to link/move/copy."
    author: "dave"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "what middle button?\n\nwhat i love about KDE is i can use it with my single button mouse.\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "Does it make you feel good to bash the Gnome project at every opportunity?  And, two parent posts above yours explains quite nicely how to move/copy/link using Gnome with one button."
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "yes, the GNOME project is why I develop for KDE ;)\n\nbesides gnome never ran correctly on my mac anyway, one more reason to move to kde.  i dont have to remove random corefiles from the root directory nightly.\n\ncheers\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "[rantmode on]\nI'm glad that is why you develop for KDE.  And I'm sure happy to know that the reason you decided to work on it was because Gnome didn't work on your mac.  Want to know why I don't work on KDE?  Because of attitudes like the one you displayed on this message board.\n\nHow come one of the KParts tutorials goes out of it's way to criticize bonobo?  If I want to read about bonobo I'll go and do so.  I am perfectly capable of making up my own mind about which technology is better.\n\nWhy can't someone point out a feature in Gnome that they like without being attacked about it?  Are you that insecure that maybe the Gnome project can produce better code then the KDE project that you need to mindlessly bash them?  Hey, I know, why not go and send TrustCommerce nasty email now because they think that Gnome apps are more full featured then their KDE equivalents.\n[rantmode off]\n\nI come to the dot because I use KDE at work and am interested in seeing what is going on with the project.  But if this is the kinda crap I need to put up with whenever Gnome is brought up here, I don't think I'll bother with KDE anymore.\n\nOh, and I use Gnome on my iBook because KDE just crashes under Mac OS X."
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "I know you probably don't care, but one of the main reasons I chose KDE of GNOME was its developers.  I went into the GNOME irc channel, and asked some question which I dont remember anymore (97/98 dont remember which year) and no one answered me.  So I asked again in like 5min, and then the only person to answer me basically said RTFM.  I thought I'd try KDE cause I heard about it in /. I think, so I tried it and something didnt work. I went into the kde channel and someone there answered my question.\nI get the same crap in #mozilla.  They are all a bunch of jerks.  I asked a NS6 question, but I had the same problem with Mozilla.\nEverytime I've went into #kde or #kde-users with a question I got an answer.  Except if neil or krazykiwi are there.  One thing I can say for Neil, he at least seems to know anything about KDE like the back of his hand.  Krazikiwi never seems to know what I ask (which is fine), but then she (?) also has a really condencending attitute.  If I see her there as the only active person, I just leave.\n"
    author: "John Q Public"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "This is a flame and I make no apologies for it.\n\nGnome started for one reason and one reason only: RMS didn't agree with the KDE developers' interpretation of the GPL wrt the QT library. Gnome was set up with the intention of creating FUD to delay the uptake of the best thing to ever happen to desktop Linux and to bluff and bully the KDE crowd into getting the QT licencing changed. \n\nYes, you heard it right, Gnome was *deliberately* started to be \"bickering, competing and incompatible\" and to stop Linux having a single desktop standard if that standard was to be KDE.\n\nThe licence issue is *long* in the past. That out of the way, the Gnome crowd should have had to decency to either scrap Gnome completely (as did those working on the Harmony project, which was developing a GPL QT clone) so we could unite behind KDE or keep Gnome going as a low key longer-term hacker R&D project like Enlightenment. But no, we had to keep the ball rolling didn't we.\n\nWhy, given the adverse impact this has had on Linux and other target platforms? \n\nNIH syndrome partly; a lot of big egos (many in the US) were beaten to the punch by a bunch of (mainly) German students. \n\nAnd the fact that it relies on an existing library means that big egos who want to reinvent the universe can't develop their own object library; they have to do something useful.\n\nBut the main reason, irony of ironies, is that it is LGPL rather than KDE's GPL; yes folks, the desktop that began as *THE* GNU free desktop now boasts that it is more commercial-friendly. That's why Sun and HP are putting money into it. Guarantees success? Ah, look at CDE...\n\nGnome is an expensive, deliberately divisive vapourware project that should have been scrapped after the QT licence changes if the principals involved had any sense of decency or any *REAL* committment to free software. It continues because a bunch of pricks can't admit that they were wrong and continue to put their own giant egos ahead of the development of desktop Unix.\n\nMeanwhile KDE continues to release in its usual methodical fashion while Gnome 2 stays as FUD. (\"You may think KDE's kewl, but wait till you see Gnome 2!\") Pardon me while I puke...\n\nGnome and the bastards who've hyped this piece of vapourware and tried to sabotage KDE for the last five years can go to Hell! Who needs Microsoft trying to pull the rug from under the free Unix's when you've got this lot! (Yes, that includes RMS, who is responsible for initiating and encouraging this debacle).\n\nTo paraphrase the end of RMS's infamous letter of \"forgiveness\" to the KDE developers: Go KDE!!!"
    author: "About Gnome"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "The reason it's stayed around is because people use it, like it and develop for it. You find kde better, great, use kde, others find gnome better - for them it i s better to use gnome. It might have started because of licencing issues but now it was never meant to be a clone of kde - just a good desktop."
    author: "dave"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "Let us be realistic.  It is obvious that Gnome is the desktop war, in Linux.   I predict Gnome will live as a replacement for CDE, since Sun wants to force it on its users.    Linux and the BSDs will be dominated by KDE.   The Gnome team may as well not bother to support anything other than Solaris.   \n\nGnome has some great applications, but as a desktop environment it is poor, and it cannot keep up with the onslaught of KDE releases."
    author: "About Ximian"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "It sounds to me that you don't believe anyone actually uses gnome (unless they have a gun pointed at their head).  It would be really great to have some real numbers on the proportion of people using each desktop - i know it's quite hard to get statistics like this mainly because (almost) all distributions include both desktops.  However most of the people that i have spoken to who have tried ximian gnome after using kde for long periods of time seem very impressed and are enthusiastic about it \"better looking and faster than kde\". So i just wanted to assure you that some people do actually use and like gnome (and no there isn't a sun/ximian employee behind me forcing me to write this)\nI'm not quite sure why you seem to want gnome dead, surely it does you (or kde) no harm."
    author: "me"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "Have you ever coded bonobo?\nIt was coded by a bunch of poorly trainted chimps...\n\nThe fact of the matter is GNOME is not designed, it is a bunch of ripped off applications pasted together.  Their flagship apps are just ripoffs of other projects they where able to hijack.  OpenOffice, Mozzila, ESD, the list goes on...\n\nGNOME is just something that happend, KDE on the otherhand has some design.\n\nYou strike me as a user, so I will spare you any more details, but after 6 years of software design, you gotta know when something smells like an MFC / Motif ripoff...\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: ">  It was coded by a bunch of poorly trainted chimps...\n\nPlease note that Geiseri speaks on his' own behalf and the majority of KDE developers not necessarily share his view and verbalism."
    author: "KDE Developer"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "...because the gnome/FSF/debian/RMS-lovin trolls did it for years."
    author: "flo."
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "> I even can't drag a desktop from Nautilus and drop it on the desktop to make a link to it. DnD MOVEs the folder !\n\nShift Drag.\n"
    author: "someone"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "2 words\n\"File\" \"Dialog\"\n\nA) its ugly and B) its useless.\n\ni have no ability to save to the network in gimp... i may in nautlus but gedit just looks at me silly.  there is no consistancy, there is no usabilty, it is just a random act of some C coder playing arround.\n\nI like the ability to change views, bookmark, get previews, filter (by mime type not this silly extentions crap).\n\nKDE is just more profesional that is all, GNOME is nice for someone who wants to be 1337 but I unfortunately need a consistantly working print system to get by in life.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "*sigh*.  Your comments are not constructive, and you sound like a troll.  Does bashing GNOME somehow make you feel better?  KDE is a great desktop (kudos to the developers), but your attitude sucks."
    author: "andrew"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "/me was not the one who DOSed the cvs server.\n\nbesides i am bringing up the same UI braindamages over and over again...  the GNOMES just lacked the skill to ever fix them.  secondly i dont see your name in any cvs commits ;)\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "And the KDE project lacked the skill to make a component system on top of CORBA that performed fast enough.  Btw, the file dialog, along with most other nitpicks that people had about Gtk+ have been fixed in Gtk 2.\n\n(And if your comments show how the KDE project is \"professional\", I'm gonna make sure that I remove the professionalism from my work machine and just go back to being 1337)"
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "Sour grapes, eh?"
    author: "reihal"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "Eh?\nKParts, DCOP?\n\nEven the GNOMEs are considering to migrate to dcop and artsd because there ripoffs are so halfassed.\n\nOn the off chance you where a developer you would have known that almost everyone is ditching the CORBA approach.  CORBA on the desktop is silly and quite useless.\n\nDCOP is what you need when you need it, and KParts are far more flexable than anything that monkey business has come up with.\n\nAnd as for the file dialog, well... As the GNOMEs try to get their 2.0 release out the door, KDE will be releaseing yet a more advanced toolkit.  Meanwhile the GNOMES are looking at just porting to .NET....  silly monkeys :)\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: ">Even the GNOMEs are considering to migrate to dcop and artsd because there ripoffs are so halfassed.\n\nErm, they are...proof please (about dcop, not the artsd thing)\n\n> DCOP is what you need when you need it, and KParts are far more flexable than \n> anything that monkey business has come up with.\n\nDCOP is what you need when you need it. Wow. Thats deep.\n\nAnd actually, bonobo is the more powerful of the two. KParts is a way to embed things inside other things. Bonobo is a full component system including compound documents, printing, control embedding, remote activation, guiless components."
    author: "Troll Nick"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "No less than Miguel has praised and advocated adoption of DCOP by GNOME.\nCheck your own GNOME mail archives.\n\nAs for Bonobo pretty much every email I've read about it on the GNOME lists is critical.  Miguel doesn't like it, Red Hat doesn't like it.  The only people trying to support it are the morons like that Uranus guy who don't have the faintest idea what it's all about, and the well-respected Ximian hackers who have had to work on it.\n\nBonobo HAS been ruled OUT as a solution for local components on GNOME.  Inform yourself.\n"
    author: "KDE User"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Umm, yes there is a lot of people in the Gnome project that criticize Bonobo.  But I haven't heard anyone suggest replacing it with something else.  The criticism helps make Bonobo better.  If everyone dislikes it (as you so claim) why is it that every gnome project is starting to use Bonobo now?\n\nUnless you have actual links, use KDE and stop spreading mistruths about Gnome."
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "The GNOME project has been \"starting to use Bonobo\" for years now.  Just because they made big press releases about Bonobo in the past and are quiet about it now, doesn't that tell you something?  Bonobo is one of the main reasons for the whole Mono (Ximian) and Hub (Red Hat) debacles.\n\nFormally they are quite sneaky about it:\nhttp://mail.gnome.org/archives/gnome-hackers/2001-September/msg00107.html\n\nInformally, you get a whole different story on Bonobo.  PSST try to use Bonobo and then come back to me and tell me it's the best thing since sliced bread.\n"
    author: "KDE User"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Conclusions about bonobo:\nUse bonobo for what its meant to be used\nDon't use bonobo for stupid things.\n\nWow.\n\nI find bonobo simple to use. I can knock together a full bonobo component in an hour or so, less if it's a simple one that.\n\nHub does not exist, it's just an idea.\nMono does not (as yet) replace bonobo, and it's reason for existing is not to replace bonobo, and never was. It's main reason is that Miguel liked C# and wanted to program in it."
    author: "Bob"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Right.  An hour.  Wow.  Next you will start babbling about \"monikers\" and nobody will know what you are talking about...\n\nLike Miguel said, Mono allows you get rid of all the previous ugly COM/ActiveX/CORBA/Bonobo technology in a nice way.\n\nThat link is sure a sneaky way of saying Bonobo sucks.  Yeah, use it for coarse interfaces and out-of-process components.  That's a sure smaller field than when it was supposed to be the end-all be-all of component technology on Unix.\n\n"
    author: "ac"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "> Right. An hour. Wow. Next you will start babbling about \"monikers\" and nobody \n> will know what you are talking about...\n\nNope, no idea what monikers are.\nI've written trivial bonobo components in under 100 lines of code, most of that being boilerplate code. Any more complex components need more code, but the bonobo bit stays the same length. Just because you're not smart enough to work it out, doesn't mean it's hard.\n\nUse it for out-of-process components. Use it for in-process components as well. Just don't use it for stupid things that it doesn't need to be used for."
    author: "bob"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "hrm... dcop is about 3 lines...\n\nand to export a dcop interface is about as large as a simple class file, maby 5 lines plus 1 line per interface.  no silly monkies here...\n\nto add insult to injury most dcop abilities are added to base classes so the features flow all of the way up.  silly monkeys dont know about inheritance so they have to add the same feature to every application, where kde adds it to 1 and it becomes available everywhere.\n\noh and btw, you get all of this for about 150 extra K per application these are added to.  i dont even want to know how much corba bloats it by.\n\nif i had never coded with bonobo i would give you a fighting chance, but you have to give up. you are out of your league here.\n\nhugs and kisses\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "To use a bonobo component, it's a line for the simplest.\nTo export a bonobo interface it's \"about as large as a simple class file\".\n\nWow, DCOP is amazing, its the same. Bonobo is simply more powerful, simply because DCOP is not a component system, whereas Bonobo is. Oh...\n\nYou really are an asshole, and you are exactly the reasons I don't use KDE. Asshole developers, who flame gnome at every oppurtunity. Notice that this entire flamewar was started by yourself."
    author: "Bob"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "WOW!!! he's an asshole cause he's right!!!\nglad you don't use kde. we can do without morons like you."
    author: "me"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "He's an asshole because he's an asshole; either he does not know Bonobo or he's deliberatly lying about it to make it sound bad.  And calling people morons for having a different opinion then yours a) makes you look bad and b) certainly does not encourage people who otherwise might be interested in helping out the KDE project.\n"
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-12-21
    body: "Ive heard news about KDE developers being morons and now Ive now seen proof from the first list Ive seen - haha. Im glad I dont support KDE *I almost started supporting KDE but you guys are morons*!\n\nBTW KDE docs suck - I found nothing about DCOPC and hardly anything about DCOP+KPanelApplets - I even ended up coming here via a google search. :(\n\nRedhat is doing a good job with your desktop IMO. ;)"
    author: "Chexsum"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "you yourself said you could do it in 100 lines... now you say 1 line...  something tells me bonobo dont scale well...  prolly because it dosent.  \n\nas for DCOP not being a component system, well that is because KParts will do that.  you see there is no reason to have to use a CORBA just to read a simple string from a running application.  hence dcop.  But the nice thing about dcop is you can pass complex objects through it. \n\nonce you get to widgets, then you move on to KParts, again unlike bonobo, these integrate completely into the application, in process.  This is used for embedding complex objects into applications, again, the most complex instance of this is about 5 lines both ways, namely because you need a factory to create the instance. \n\nnow if you are really crafty and want to control the application from the application itself, aka scripting extentions, you can use plugins or KScript. \n\nGnome made the mistake of wrapping the whole mess together.  The only plus side of this is you only have to learn one API, but then you get locked in to one API, and it limits what you want to do.\n\nas for this \"flamewar\", i have facts to back up what i am saying.  i was not the troll who decided to share his lack of understanding of not only KParts/DCOP but bonobo too.  i am just blowing time during compiles...\n\n:)\n-ian reinhart geiser\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "That was an informative reply to a low-brow flame.  Thanks.\n"
    author: "KDE User"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "That was not an informative reply.  It was riddled with errors.  That reply was written by someone who seemingly does not know the capabilities of Bonobo."
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "If instead of blowing time during compiles criticizing something that you really don't seem very informed about, you read a little to see what was going on, you might not be pissing people off the way you are.\n\nCreating a bonobo control requires one line of code (and does not require modifying the widget at all):\ncontrol = bonobo_control_new (widget);\n\nThat is the case when you're dealing with a widget.  If you're creating a bonobo control which is not widget based, you instantiate a class descending from BonoboObject and return that.  In the case of a control, it takes one line.  In other cases, it requires some boilerplate code to instantiate everything and the code for the class.\n\nI have no idea what you're talking about when you refer to bonobo scaling.  You can create as large or as small a component as you want.  Most large Gnome applications are nothing but Bonobo controls and containers (Evolution and Nautilus are that way right now, Galeon is moving in that direction).  Gnome Panel 2 applets are just little bonobo controls.\n\nBonobo is a full featured component model.  There is nothing that you can't do in it.  The API is not limiting because the Bonobo API is really just used for component creation.  The component's API is designed by the component developer.\n\nAs to the rest of your post ... Bonobo works just fine in process, out of process or distributed.  Obviously it's easier to debug out of process components, but that doesn't mean that it's the only way to write them.  Bonobo also uses factories for component creation and requires just a handful of lines of code to create one.\n\nSo, what is it that you would like Bonobo to do that it doesn't?  And why do you need to continually bash these things when you don't even seem to understand them?"
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-02-13
    body: "You are missing the point, although this could be because you have no clue about OO design.  You are mixing up thing very well, so I will review:\n\tKParts - Component System\n\tKParts-plugins - Feature addons to applications\n\tDCOP - remote process messageing system\n\nWhere bonobo fails is it tries to be both, at least from where the how-tos lead you to belive.  My point is for remote messageing bonobois bloated, and for doing stuff like widget embedding bonobo is adiquate, but horribly object disorented.  \n\nThe largest problem with bonobo is evident from your example.  I mean how do you message back to the plugin?  How do you control it?  Well the answer is to use bonobo, but then you have the overhead of building more interfaces...  With KParts you can just base your component off of a base class and you have access there, low coupleing (and OOD term).  Ideally you could hack this with bonobo i think, but I am not a C wizzard.  I understand that you have limits because of design, but really at that point shouldnt you be fixing your design?  Now I admit for component embedding bonobo is okay, if you are familiar with C, but for messageing between applicaitons I think there are much better ways.\n\nNow I have to admit I have not touched bonobo in about a year so I cannot vouch for Gnome2, but that is still a beta at this point, I am talking todays reality.   I also do not doubt the abilites of bonobo, but there is something to be said for ease of use, and flexability of development.  I mean I can do anything I want with Xlib, so why use QT or GTK?  \n\n-ian reinhart geiser\np.s. the thread started with DCOP vs Bonobo for messageing.  KParts vs Bonobo is a much more fare fight."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-13
    body: "I'm not missing any point, nor do I have the slightest problem with OO design or methodologies.\n\nNow, if you actually used Bonobo, the way you talk to a component is self evident; they expose their methods.  There are several different helper API's already built into Bonobo (including PropertyBags for exposing set/get methods for the properties of an object as well as an EventSource/EventListener API).  Any other API's are defined by the object (I'm currently writing a Bonobo component to embed CD Burning support into any Gnome application and it exposes methods for scanning the SCSI bus for devices, etc).  Now, every Bonobo object is based off BonoboXObject or one of it's descendants (this uses either GtkObject in Gnome 1 and GObject in Gnome 2 ... tho I am not going to debate the merits of OO in C).\n\nAs to ease of use and flexibility of development, I have had no problems learning or using Bonobo to develop components.  Many large applications use Bonobo today, and many more (mostly any application of substance from the way it looks) will use it in Gnome2.\n\nWhen it comes to DCOP vs Bonobo, I don't think there is much to talk about.  As far as I can tell they are meant for totally different things.  Bonobo is a general purpose component system.  There are components today for handling printers, communicating with databases, etc.  I wouldn't have even piped up if a) you weren't mindlessly bashing gnome and b) spreading hugely inaccurate information about Bonobo."
    author: "Chris Conrad"
  - subject: "Re: Desktop look..."
    date: 2002-06-28
    body: "I don't know why your so ugly maybe it just runs in the family.succer"
    author: "Mike Roch"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "> No less than Miguel has praised and advocated adoption of DCOP by GNOME.\n> Check your own GNOME mail archives.\n\nYup, I did. All I could find was this reply.\n\n>> In fact, comparing DCOP to Bonobo at all is laughable, DCOP is not\n>> even a component model.\n>\n> For Automation it hardly matters.  DCOP is a pretty good tool, sure we\n> can have one, but not with the broken component naming scheme we have.\n\nNow, yes, he said it's a \"pretty good tool\", but I don't see anywhere where he \"advocated adoption of DCOP by GNOME\".\n\nIf you have any better links, please show us. \n"
    author: "bob"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "The first search result for DCOP was this:\n\nhttp://mail.gnome.org/archives/gnome-hackers-readonly/2001-July/msg00115.html\n\nI want to add two things:\n\n        * This should at least happen on the 2.x platform, not 1.x\n          anyways.  (I know its obvious, but wanted to make sure we\n          are on the same page).\n\n        * Maybe it is time for us to integrate DCOP as a messaging\n          bus, and use a DCOP event and keep gnome-session intact (for\n          the reasons explained by Havoc)\n"
    author: "ac"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "this is absurd. dcop is very much QT specific, so it would be almost impossible to switch to dcop for gnome."
    author: "ik"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "No, the protocol is not Qt specific.  Even the marshalling of Qt data types has been documented, so it would be easy to remove any Qt dependencies.\n\nUsing DCOP in GNOME is a real possibility.  However the people willing to do anything about it, are now pre-occupied with cloning .NET for GNOME.\n"
    author: "KDE User"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "silly monky lover, dcopc is written in gtk, even the Windowmaker guys are looking seriously at it.\n\ncheers\n\t-ian reinhart geiser\np.s. yes, this was a troll, you are finally getting entertaining."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Can't you give it a rest? This is not slashdot, although it seems like it now :-("
    author: "ac"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "silly monky lover, dcopc is written in gtk, even the Windowmaker guys are looking seriously at it.\n\ncheers\n\t-ian reinhart geiser\np.s. yes, this was a troll, you are finally getting entertaining."
    author: "Ian Reinhart Geiser"
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "It may be better then good... until you want a different color than the theme's default. Please, by all means, point me to the color selection tool in Gnome (HINT: there isn't one). Also try web browsing in Nautilus and then compare that to Konqueror. \n\nSure you can make Gnome look pretty - that's never been a problem. However, if you want customization, or a *really* good file manager, or a truly well integrated system - then you should use KDE.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "\"...try web browsing in Nautilus and then compare that to Konqueror...\"\n\nNautilus' browsing capabilities are supposed to be rudimentary.  It's more for inline local HTML file browsing than for \"real\" web usage.\n\nFor a fair comparison, try the latest Galeon and try to tell me it's not a seriously cool browser.  Not that Konq isn't great too, but Galeon is QUITE formidable.\n\n\"...if you want customization, or a *really* good file manager, or a truly well integrated system - then you should use KDE....\"\n\nNo, one should use what works best for them.  Nautilus is fine for my needs, KDE is no more customizable than Gnome in any way that I give a sh*t about, and as for \"integration?\"  Not to troll, but what the hell does \"integration\" mean anyhow?  My gnome apps all use the taskbar, status dock apps all use the same status dock, I can get clickable-URLs from just about every app that displays a URL...  is this what you're refering to?\n\nAs I said, both desktops are quite advanced, and it really comes down to a matter of taste.  This is doubly true because anyone can run KDE *APPS* in Gnome and vice-versa.  Where's the problem here?  Where's the LOVE? :)\n\nChoice is GOOD, and any advancement made for *either* KDE or Gnome is a WIN for Free software!"
    author: "Jens Knutson"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "\"For a fair comparison, try the latest Galeon and try to tell me it's not a seriously cool browser.\"\n\n  It's not a seriously cool browser. ;-) Okay, it's fine, but honestly I don't like the way Mozilla-rendered pages look. Konqi in most cases renders pages the way Internet Explorer does (using it's buggy site \"fallback\" system), which with IE being as popular as it is makes this a *good* thing in my mind. Also I find that Konqueror's interface (being QT rather than Mozilla skinned widgets) is much nicer to work with from an aestics viewpoint.\n\n\"KDE is no more customizable than Gnome in any way that I give a sh*t about\"\n\n  For examples, on the topic of styles. I like Mosfet's Liquid, but I'm not crazy about Mosfet's default color choice. In Gnome that would mean \"too bad, so sad - go make your own theme.\" However, within seconds I can customize the colorings of this theme. \n  Another good example is that of the KIO-Slave system. I use a lot of scp (Secure cp) and ssh, and so someone was able to create the increadibly nifty KIO-FISH that allows me to seamlessly access my remote host as if it was part of my computer. \n  There are many other ways you can custom taylor KDE, infact I use many add-ons each day. Also, if you look at apps.kde.com, you will be amazed at just how many ways you can extend KDE.\n\n  Finally, I might point out that KDE now has a year on GNOME in respect to official anti-aliasing support. Not that gdkxft isn't great, but how much better is it to be able to configure AA support by just going into KDE Control Center?\n\n\"Not to troll, but what the hell does \"integration\" mean anyhow?\"\n\n  IMO, integration means that a KWord document gets opened in KWord, a PDF file in KGhostView, and so forth - only they're embedded into Konqi. In other words, I'm referring to KDE's amazingly powerful KParts and DCOP. \n  I mean, in Gnome, if I want to browse the web, I open Galeon. I have the GTK-like overall look, but all of the forms and scroll bars on the web pages have a completely different look. \n  It's also worth pointing out that while the KOffice suite is tightly integrated to the point that all the apps can embed into KOShell, Gnome Office is simply a loose grouping of any office-like apps for Gnome.\n\n  Just a few thoughts. It's getting late, so I hope I'm still making sense...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: ">It's not a seriously cool browser. ;-) Okay, it's fine, but honestly I don't >like the way Mozilla-rendered pages look\n\nWHAAT?? Have you tried Galeon/Mozilla? I'm running RedHat 7.2 with Ximian gnome,\nand Galeon/Mozilla sure render pages a lot better than Konq... (I mean, on my computer the difference is huge, in favour to mozilla..)\n\nE."
    author: "Eivind"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "I've tried it, but I got very weird results with HTTP POST requests, such as duplicate requests. ;)"
    author: "Rob Kaper"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Yeah, that guy must have posted like 10 times.  Sheesh.\n"
    author: "ac"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: ":: WHAAT?? Have you tried Galeon/Mozilla? \n\n   Yup.  And I also like Konqueror's rendering much much better.  I also like IE over Netscape, and vanilla over chocolate, a good steak and mixed green salad over any pasta meal, and I prefer to wear comfortable button down shirts rather than cheap t-shirts.\n\n    Tastes differ.  Some of us just like KHTML better.  But, did you know that Konqueror is like Galeon, in that it can use the Mozilla engine for browsing the web?  Just go to View | View Mode when looking at an html document or web page, and you can set your preference.\n\n    So I can have my blue cheese and mixed greens while you get your lasagna, all in the same restaurant.  After all, Konqueror is just a frame for plugins.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Ick, I'd rather not know that is possible...  KHTML (using Konq from KDE-2.2.2) does a much better job on rendering pages than the last version of Moz I tried (i believe 0.9.2)...  Moz is supposed to be so standards-compliant, but I find a lot of the CSS2/XHTML1.0 stuff I do in my webpages displays the same in IE, Konq and Opera (the MOST standards-compliant browser), but looks terrible in Moz (on Windows or FreeBSD) and with all the needed fonts installed...  and no, I'm not using any MSHTML crap, I'm using straight-up 100% W3C-compliant CSS2 and XHTML1.0...  but I do like the tabbed-browsing available in Galeon and Opera (and from what I hear the latest builds of Moz) along w/ a few other features...  but like you mentioned, i'd rather wear t-shirts instead of button-downs, and prefer chocolate over vanilla! ; ^ )"
    author: "gLaNDix"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "\nMaybe I should have clarified that I prefer KHTML over Mozilla.  :)  But my point was, choice is good, as we all have different tastes, and Konqueror gives us choice.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "> Maybe I should have clarified that I prefer KHTML over Mozilla\n\nnope, i completely got that out of your message already...  i was just saying that 1.) i agree and 2.) i'd rather not know that Konq can use Moz' rendering engine (gecko, isn't it?) because of point #1... : ^ )\n\n> But my point was, choice is good, as we all have different tastes\n\nnice to see for once on an OpenSource-related website... seems so many people involved in OS software are so bullheaded and close-minded anymore...  kinda tired of all the GNOME vs. KDE junk...  haven't heard any for a while until i read the comments about the new GNOME screenshot...  i've gotta be honest... i like some of the GNOME icons, but i don't like their (at times) lack of consistancy...  i like KDE's consistancy...  unlike a lot of people in the Computer industry, I like a plain, dull, consistant look rather than a spooky dark look w/ whacked out fonts and kewl icons...  but like you said, all different tastes! :^ )"
    author: "gLaNDix"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: ":: i like some of the GNOME icons, but i don't like their (at times) lack of consistancy... i like KDE's consistancy\n\n   As do I.  But in the interests of choice, I converted the icons from Gnome to KDE, including the SVG Gorilla theme (KDE version = Kong).  But since KDE-Look won't send me my account's password, and the people won't answer emails, I can't upload it.  I finished them a couple days ago.\n\n   Want the icon theme released?  Yell at kde-look.  My account is \"JabberWokky\".\n\n   (Also, if you're an icon theme guru, tell me why the \"actions\" icons don't show up in the toolbar of Konqueror, etc, even though they exist.)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "That seriously looks nice.  I am glad someone is more rational.  While everyone else started bashing what I think is a beautiful set of icons (and related bickering) you made (or are making) them available to the rest of us.  Thank you.  Just one request, If you have not already render them to all the different sizes since I prefer my icons a little bigger.\n\nOn a related note, why are the standard kde icons not antialiased (eg the edges of  some of the icons are so jagged and garbled looking)?"
    author: "idspispopd"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: ":: Just one request, If you have not already render them to all the different sizes since I prefer my icons a little bigger.\n\n    I have an automated script that rebuilds from the SVGs, so there are 64x64, 48x48, 32x32, 22x22 and 16x16 icons for every single item.  This is a semi-temporary solution - I have a mapping file from Gnome -> KDE and Nautilus -> KDE that will allow me to build an import filter into the Icon Contol Center... it will have to wait until after the KDE 3.0 and Gnome 2.0 release (for the source to open up to changes on the KDE side, and for the icon format to finalize on the Gnome side).\n\n    Unfortunatly, some things don't map well - In Kong, I had to create five new icons based on the originals to cover some common icons.  That throws some doubt at the being able to completely automate the process.  Regardless, I should be able to perform conversions fairly quickly with the set of tools I used for Gorilla and Simple.\n\n:: On a related note, why are the standard kde icons not antialiased (eg the edges of some of the icons are so jagged and garbled looking)?\n\n    I have all anti-aliasing turned off for a variety of reasons too complex to go into detail right now.  Some asthetic, some technical, some temporary, some historical.  Heh.\n\n    And again, email to KDE-Look is being ignored... feel free to email them telling them to fix the \"Send password by email\" function.  I'm too stubborn to start a new account.  ;)  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: ":: And again, email to KDE-Look is being ignored... feel free to email them telling them to fix the \"Send password by email\" function. I'm too stubborn to start a new account. ;) \n\n    And of course, 42 minutes ago, it was taken care of.  :)  Thanks, guys, and look for the icon set to be uploaded in the next minute or two.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Desktop look..."
    date: 2002-02-08
    body: "A lot of replies talk about Gnome apps (gimp, abiword) while those apps are in fact GTK-apps, which is a small but significant difference...\n\nKind regards, Rinse"
    author: "Rinse"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "Hmm..., it almost seems that KDE and GNOME have switched places on the topic of file dialogs. GTK2's new file dialog is supposed to be better, wheras the file dialog in KDE 3 has that nasty \"windows quickbar\" thing that. Hopefully it's possible to turn that off (I haven't had a chance to compile beta1 yet, but I've seen the screenies...)"
    author: "Carbon"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "KDE3 new windows quickbar is something that some of us are looking forward too. Beacuse it means quick accsess to some of the most frequently directorys I\\we use.\nJust beacuse Microsoft came up with it donsen`t mean that it`s useless. Sometimes it might actually be wery conveniant.\n\n\nps:\nForgive my English."
    author: "tjodleiv"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "Well, I find it annoying. I have instant access to my home directory from the toolbar in the file dialog, and root from the pop down. I don't need quick access to any others, and if I did, I'd just create links in my home dir. The windows style quick bar takes up too much space for me. I'm not saying that it's bad or should be removed because M$ came up with it, but I'd like to ask the developers if there's a way to turn it off."
    author: "Carbon"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "Just close your left eye to not see it. problem solved.\nnow back to bashing gnome...."
    author: "me"
  - subject: "Re: Desktop look..."
    date: 2002-02-11
    body: "Well, it is rather nice to put a SFTP link in the quickbar. On the other hand, I had/have those in my bookmarks pull-down in the file dialog as well, so I could live without.\n "
    author: "Rob Kaper"
  - subject: "Re: Desktop look... (how about this one)"
    date: 2002-02-09
    body: "Want a nice desktop. Look at this:\nhttp://www.mosfet.org/liquid2.png"
    author: "John Herdy"
  - subject: "Re: Desktop look... (how about this one)"
    date: 2002-02-09
    body: "My word, thats fucking hideous!"
    author: "Erk"
  - subject: "Re: Desktop look... (how about this one)"
    date: 2002-02-10
    body: "man you have bad taste in themes, the only thing that looks remotely nice is the translucent kde menu"
    author: "dave"
  - subject: "Re: Desktop look... (how about this one)"
    date: 2002-02-10
    body: "Nah, I think it looks kewl. There's no such thing as bad taste in themes, because it's all about what the person using the theme likes. You don't have bad taste, you can just have unusual taste."
    author: "Carbon"
  - subject: "Re: Desktop look... (how about this one)"
    date: 2002-02-11
    body: "Something like http://developer.gnome.org/dotplan/images/owen-pixmap-alpha.png is not bad taste, it is ugly. But it was the intention, anyway :)\n\nOh, and it has alpha transparency and is not KDE ;)"
    author: "Company"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "Gnome has always had some excellent artists working for it, and it's looked great as a result. However there's some great stuff in the pipeline for KDE with the work of Everaldo and AntiAlias (and of course the huge amount of effort made by Tackat - cheers!). At the moment KDE is functionally better than Gnome. That goes without saying - we're at 2.2.2, they're at 1.4.x. When Gnome 2 comes out it'll be more or less even again.\n\nBut here's the thing. I love KDE. I use it all the time (er, except for right now where I'm using it with Enlightenment 'cause I'm a sucker for translucency). But I use Evolution for my mail - it looks good, is easy to use and has a funky address book (anyone ever think of doing a KOShell clone for Kmail, Korganiser, and the mail book?). I use Dia for all my technical drawing - it's simple, but it's also brilliantly easy for what I want to do. The same with the Gimp. Kivio and Krayon just aren't what I want. KSpread is nowhere near Gnumeric when it comes to export/import or serious scientific spreadsheet work (e.g. mindwarping stats assignments). But I use them both depending on what I want to do.\n\nI don't want to use either KDE or Gnome (or Qt and GTK for that matter). I want to use them both. Both have excellent apps. For my money KDE provides the better overall environment. People bitching about colouring Gnome themes need to calm down, you'll be able to do that when Gnome 2 comes out (around March, same as KDE 3). In fact, March should be a great month from the end-user point of view, as it'll mark a point of substantially closer integration between the two DEs. Hopefully by the following year people will have come up with a common themes format so the differences will become invisible.\n\nSeriously guys, even Slashdot's gotten over the KDE/Gnome thing. This kinda conversation's about as productive as a turnip. Really, what people need to do is to use both apps and environments, see their advantages and disadvantages without going postal, and then ripoff anything that seems like a good idea for their own team ;-)\n"
    author: "Bryan Feeney"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "I wish more people posting here were as level headed as you. \n\nWhen I first came to linux, migrating from BeOS, I was tinkering around with both GNOME & KDE and decided on the latter simply because the folks using it seemed more adult, less fanatical -- sadly, these days I begin to wonder :(\n\nWhy do people act so silly and fanatical about, of all things, the desktop? "
    author: "Shamyl Zakariya"
  - subject: "Re: Desktop look..."
    date: 2002-02-09
    body: "> Why do people act so silly and fanatical about, of all things, the desktop?\n\nBecause you can only run one desktop at one time in contrary to panels, browsers and filemanagers."
    author: "someone"
  - subject: "Re: Desktop look..."
    date: 2002-02-12
    body: "with the configurabilty of gnome and kde today need this really be the case? I mean, wouldn't it be possible to run kde but use nautilus rather than kde as the file manager and make it fit in, or use the gnome panel in kde(still with kde doing all the session management), or use konqueror and the kde panel in gnome eith gnome doing session management, or even have have a nautilus viewers for koffice and konqueror viewers for abiword and gnumeric etc. It would be really great to see more cooperation between the two desktops so that the boundaries could be blurred a little.  I guess one major problem is that they use different component architectures... "
    author: "me"
  - subject: "Re: Desktop look..."
    date: 2002-02-10
    body: "i like turnips :-)\n"
    author: "turnip"
  - subject: "Eyuuuch!"
    date: 2002-02-10
    body: "Ugly as hell! Seems like a MAC! :(("
    author: "Vajsravana"
  - subject: "Re: Eyuuuch!"
    date: 2002-02-10
    body: "i can't believe you kept a straight face while you said that.  well beauty in the eye of the beholder and all that"
    author: "dave"
  - subject: "KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-09
    body: "This is obviously a lie. KDE3B2 got delayed *AGAIN* due to lack of good management inside KDE team. When a project like this with 300 developers around the world gets delayed and dishonours the original schedule, it is proven that Open Source congregations sometimes are not as efficient as real Corporations. KDE this time had a BAD SCHEDULE management and other idiotic steps. "
    author: "DB"
  - subject: "Look everybody it's a troll!"
    date: 2002-02-09
    body: "idiot"
    author: "hey"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-10
    body: "Indeed.   If they were a corporation like the Ximian tools, they could have rushed a release out the door to meet the deadline.   Go corporatism!"
    author: "About Ximian"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-10
    body: "there are many morons floating around at the dot lately. Seems those nice gnome screenshot gave some people balls to start talking. I have only one advice to those: Wait for kde3.\n"
    author: "me"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-10
    body: "Many morons eh?  Been a blissful long time since we've had to delete comments on the dot.  Guess the trolls are back.  Sigh.\n\n-N.\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-10
    body: "Ooh, it's the developers fault that the servers went down? For some reason the VA Software guys didn't manage to get the servers running again. And how is it our fault?\n\nThere's nothing wrong with the shedule management. Some projects get delayed, but it's a good thing. You don't want to release a bad product?\n\nIf you are a GNOME user: Why is it that GNOME 2 is delayed till end march? Also bad shedule management??? It also seems that a bunch of TODO items for GNOME 2 aren't complete yet. Or isn't the list updated? Again bad management... And why does Ximian control GNOME? If Ximian goes bancrupt, GNOME is dead too. Maybe M$ wants to buy Ximian, very nice indeed... Oooh wait, Mono development already belongs to M$. They decide what Mono is going to be."
    author: "KDE User"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-12
    body: "hmmm... i'll agree the original poster was way off the mark, but you're no better. What on earth does kde release schedule have to do with gnome2?\nAs for ximian, they don't control gnome they do there own releases normally after the main release which have been customized  - if they controlled gnome they wouldn't have to, they could do any changes they want in the main release.\nIf Ximian goes bankrupt then gnome will be in no worse a position than kde is now.\nMS  buy ximian... right... (maybe you should change your nick to gnome basher)\nbtw i would guess the original poster didn't have the intelligence to use linux - in gnome or kde, hell he probably has trouble working out how to turn the computer on."
    author: "me"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-10
    body: "Real corporations have NEVER missed a target date?  It doesn't matter if it's Open Source or software for a fee, there's nothing wrong with not meeting a target date.. unless you're a (corporate) manger but (that) would be your own fault!  Instead of blaming KDE project management, praise them for having sense not to release if it's not ready, especially with 300? developers scattered around the world vs. a local cube-farm.  Besides, it was just a BETA, not a GA... so shorten the Beta if absolutely necessary.  If you work for a corporation that's never delayed a release, consider yourself unbelievably LUCKY!!!\n"
    author: "PsychoDog"
  - subject: "Re: KDE is ruled by BAD MANAGEMENT"
    date: 2002-02-11
    body: "Uh, what an ignorant human being.\nLet's look at recent history.\n\nGnome 2 is coming out about a year and a half later than it was originally supposed to. \nWinxp came out 4 months later than it was supposed to.\nMacOSX was supposed to come out in the Summer of two years ago, but it came out in April of last year.\n\nWhop-de-farking-do. "
    author: "flo."
  - subject: "Beta 2"
    date: 2002-02-12
    body: "Can any mailing-list surfers say how things look for the beta 2 release? Is there a scheduled date?"
    author: "cbcbcb"
  - subject: "Re: Beta 2"
    date: 2002-02-13
    body: "Get it from ftp://ftp.kde.org/pub/kde/unstable/kde-3.0-beta2 :-)..."
    author: "Anonymous"
  - subject: "Re: Beta 2"
    date: 2002-02-13
    body: "You rock. Mr. Anonymous :)"
    author: "elfy"
  - subject: "Re: Beta 2"
    date: 2002-02-13
    body: "1) Why are there SDL libs in KDE3 Beta ?\n2) Can I install Mandrake RPMs without removing QT2/KDE2 ?"
    author: "Julien Olivier"
---
As many people will have noticed, things haven't been too rosy in the KDE world 
for the last few days. Virtually all of our critical services have been broken, 
including cvs, mailing lists, kde.org mail addresses and a number of web sites 
(such as developer.kde.org). Unsurprisingly, this has meant that KDE 3.0 Beta 
2 (originally scheduled for Monday) has been delayed.
<!--break-->
<Br><br>
The problems we've been suffering are the result of a number of relatively 
  trivial problems all occurring simultaneously: a DoS attack meant that some 
  planned maintenance on <i>cvs.kde.org</i> needed to be performed immediately, 
  and this took longer than expected because it caused a schedule clash. At the 
  same time the machine room containing <i>master</i> was being cleared of Polychlorinated 
  Biphenyls, a toxic group of chemicals leaving no alternative but to take down 
  the machine (for more info on PCBs see <a href="http://www.epa.gov/opptintr/pcb/">http://www.epa.gov/opptintr/pcb/</a>). 
</p>
<p>Happily, I can now tell you that thanks to a great deal of generosity and hardwork 
  by David Faure (<a href="http://www.linux-mandrake.com/">Mandrake</a>) and the admin team, Dirk Mueller, Adrian Schroeter 
  (<a href="http://www.suse.com/">SuSE</a>), Stephan Kulow and Chris Schlaeger (SuSE), we have been able to set up 
  a replacement cvs server (kindly paid for by <a href="http://www.ibm.com/">IBM</a>) in the SuSE offices. Martin 
  Konold now has access to <i>master</i>, so mail and other services will be coming 
  back on line too. In addition, George Staikos can take credit for <s>annoying</s>
  encouraging dfaure, and 'making stupid jokes'. ;-)
<p>All of our data is intact, so the new release, while a few days late, will continue 
  as planned. Things will take a few days to sort themselves out, so please don't 
  all rush to update at once. Many thanks are due to everyone involved in sorting 
  this out.