---
title: "KDE 3.0.5a:  Security Release"
date:    2002-12-22
authors:
  - "Dre"
slug:    kde-305a-security-release
comments:
  - subject: "Customized builds"
    date: 2002-12-22
    body: "While downloading the source for 3.0.5a and thinking of the long compile ahead on my Athlon 800 (yes, I need to compile, I make modifications to a number of the programs in KDE), I got to realizing that there aren't really that many programs in the base KDE distribution that I use.  For example, all I use from kdegames is Shisen-Sho, and all I use from kdenetwork are kmail and kdict.\n\nI was just wondering how hard it would be to be able to do \"customized\" build, as in: ./configure --enable-apps=kmail,kdict --etc and just compile/install the requested programs.  Currently, for kdegames, I just do a make install in libkdegames and kshisen, but that's kind of ugly.  I would be eternally grateful if I could pick and choose my base applications, so compile times and disk usage would be greatly diminished."
    author: "KDE User"
  - subject: "Re: Customized builds"
    date: 2002-12-22
    body: "Hi!\n\nI just want to make sure: do you know 'setenv DO_NOT_COMPILE 'foo bar ....''?\n\nAndy"
    author: "Andy"
  - subject: "Re: Customized builds"
    date: 2002-12-22
    body: "You are my hero!  Thank you so much."
    author: "KDE User"
  - subject: "KDE 3.0.5a on Debian"
    date: 2002-12-22
    body: "As of this writing, Debian packages of KDE 3.0.5a have not yet been uploaded to download.us.kde.org. Debian users who are using something like:\n\ndeb http://download.us.kde.org/pub/kde/stable/latest/...\n\nin /etc/apt/sources.list will get an HTTP 404 error when trying to update. Either wait until the Debian packages are updated, replace '/latest/' with '/3.0.5/', or wait until Debian includes KDE 3.x in the distribution (whenever that is...?).\n"
    author: "Ken Arnold"
  - subject: "Version number"
    date: 2002-12-22
    body: "Why do you use 3.0.5a as version number? Why not 3.0.6?"
    author: "Stefan Nikolaus"
  - subject: "Re: Version number"
    date: 2002-12-22
    body: "If I remember well, KDE_3_0_6 branch was already used in early development of KDE 3.1 ..."
    author: "Nicolas Hadacek"
  - subject: "Re: Version number"
    date: 2002-12-22
    body: "It should be exactly the same as kde-3.0.5 plus the security fixes. No others bug fixes.\nMay be a kde-3.0.5.1 would be better :-)"
    author: "JC"
  - subject: "Error compiling kdepim-3.0.5a"
    date: 2002-12-22
    body: "I got the following error when compiling kdepim:\n\nmake[3]: Entering directory `/usr/src/kde-3.0.5a/kdepim-3.0.5a/kalarm'\ncp ../kalarmd/alarmguiiface.h .\n......\ng++ -DHAVE_CONFIG_H -I. -I. -I.. -I.. -I../libical/src/libical -I../libical/src/libical -I/opt/kde-3.0.5a/include -I/opt/qt/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -DNDEBUG -DNO_DEBUG -O2 -fno-exceptions -fno-check-new  -c -o kalarm.all_cpp.o `test -f kalarm.all_cpp.cpp || echo './'`kalarm.all_cpp.cpp\nIn file included from ../libical/src/libical/ical.h:2583,\n                 from alarmcalendar.cpp:40,\n                 from kalarm.all_cpp.cpp:9:\n../config.h:201: warning: `VERSION' redefined\nkalarm.h:30: warning: this is the location of the previous definition\nIn file included from editdlg.cpp:35,\n                 from kalarm.all_cpp.cpp:4:\n/opt/qt/include/qdir.h:80: parse error before `0'\n/opt/qt/include/qdir.h:86: missing ';' before right brace\n/opt/qt/include/qdir.h:88: parse error before `('\n/opt/qt/include/qdir.h:89: parse error before `const'\n/opt/qt/include/qdir.h:91: parse error before `const'\n......\n/opt/qt/include/qdir.h:128: non-member function `encodedEntryList(int, int)' cannot have `const' method qualifier\n/opt/qt/include/qdir.h:130: `DefaultFilter' was not declared in this scope\n/opt/qt/include/qdir.h:131: `DefaultSort' was not declared in this scope\n/opt/qt/include/qdir.h:131: virtual outside class declaration\n/opt/qt/include/qdir.h:131: non-member function `encodedEntryList(const QString &, int, int)' cannot have `const' method qualifier\n/opt/qt/include/qdir.h:132: `DefaultFilter' was not declared in this scope\n/opt/qt/include/qdir.h:133: `DefaultSort' was not declared in this scope\n/opt/qt/include/qdir.h:133: virtual outside class declaration\n/opt/qt/include/qdir.h:133: non-member function `entryList(int, int)' cannot have `const' method qualifier\n/opt/qt/include/qdir.h:135: `DefaultFilter' was not declared in this scope\n/opt/qt/include/qdir.h:136: `DefaultSort' was not declared in this scope\n/opt/qt/include/qdir.h:136: virtual outside class declaration\n......\n/opt/qt/include/qdir.h:230: no `bool QDir::operator !=(const QDir &) const' member function declared in class `QDir'\nIn file included from /opt/kde-3.0.5a/include/kfiledialog.h:32,\n                 from editdlg.cpp:40,\n                 from kalarm.all_cpp.cpp:4:\n/opt/kde-3.0.5a/include/kfile.h: In function `static bool KFile::isSortByName(const QDir::SortSpec &)':\n/opt/kde-3.0.5a/include/kfile.h:75: confused by earlier errors, bailing out\nmake[3]: *** [kalarm.all_cpp.o] Error 1\nmake[3]: Leaving directory `/usr/src/kde-3.0.5a/kdepim-3.0.5a/kalarm'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/usr/src/kde-3.0.5a/kdepim-3.0.5a/kalarm'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/usr/src/kde-3.0.5a/kdepim-3.0.5a'\nmake: *** [all] Error 2\n\nAnyone could help?\n\nLinux From Scratch 3.3\nGCC 2.95.3"
    author: "yellowfish"
  - subject: "Re: Error compiling kdepim-3.0.5a"
    date: 2002-12-23
    body: "Yup, I can confirm.\n\nEither don't configure with --enable-final or move kalarmapp.cpp in the Makefile till after spinbox2.cpp\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Error compiling kdepim-3.0.5a"
    date: 2002-12-23
    body: "Great!  I compile sucessfully when configure without --enable-final.\nA lot of thanks, Waldo."
    author: "yellowfish"
  - subject: "And 'nothing' will stop me..."
    date: 2002-12-23
    body: "Krootwarning:\n------------------------\n'You are running a graphical interface as root.\nThis is a bad idea because as root, you can damage your system, and nothing will stop you.'\n---------------------------------------------------------------------------------------\n\nI am just curious.\nWho is the 'nothing' ? Why and when is he going to stop me? After I had damaged my system or maybe before? Is the 'nothing' FBI?\n\nMaybe I am too paranoic. Is it possible to send Kroot's warning to bugs.kde.org for a semantic & syntactic cleanup?"
    author: "antialias"
  - subject: "Re: And 'nothing' will stop me..."
    date: 2002-12-23
    body: "Or maybe...  you're using the wrong language?  Try .dk for a change."
    author: "anon"
  - subject: "Re: And 'nothing' will stop me..."
    date: 2002-12-23
    body: "'Or maybe... you're using the wrong language?'\n\nYes, you're right, I always knew english was the wrong language. And I am sorry if I offended you but I can't stop laughing when I read this one: ' This is a bad idea because as root, you can damage your system, and nothing will stop you.'\n\n'Try .dk for a change.'\n\nThanks for the advice 'anon. coward'. Ooops, sorry again, you are only 'anon'.\n\nCheers,\n\nantialias"
    author: "antialias"
  - subject: "Re: And 'nothing' will stop me..."
    date: 2002-12-23
    body: "Dude... Stick to your day job. You will die of hunger as a comedian."
    author: "KamiKaze"
  - subject: "Re: And 'nothing' will stop me..."
    date: 2002-12-23
    body: "No, it';s not possible to send it to bugs.kde.org because it's not part of KDE. Try https://qa.mandrakesoft.com instead.\n"
    author: "Sad Eagle"
  - subject: "Re: And 'nothing' will stop me..."
    date: 2002-12-24
    body: "I don't see any errors in this message."
    author: "Beefy"
  - subject: "Kudos to the developers"
    date: 2002-12-23
    body: "Dunno if someone has already said this but hats off to the KDE developers for doing the security audit.  I'm sure it's not much fun going over all that code looking for these bugs.  Their efforts are appreciated by many people I'm sure.\n\nRegards,\n\nDeephack"
    author: "Deephack"
  - subject: "What happened to Mandrake Binaries for 3.0.5?"
    date: 2002-12-25
    body: "There have been no Mandrake Binaries for 3.0.5 or 3.0.5a.\nThey are usually very prompt in releasing KDE binaries.\nAnyone know what happened?\n\nMagnus.\n"
    author: "Magnus Pym"
  - subject: "Re: What happened to Mandrake Binaries for 3.0.5?"
    date: 2002-12-25
    body: "They probably enjoy their Christmas break. Wait for a few days :-)"
    author: "JC"
---
The KDE Project today released a
<a href="http://www.kde.org/info/security/advisory-20021220-1.txt">security advisory</a>
affecting all versions of KDE 2 and KDE 3.  The advisory is the culmination of
the security audit which <a href="http://dot.kde.org/1039281841/">delayed
the release of KDE 3.1</a> until January.  The KDE Project strongly encourages all KDE users to upgrade to
KDE 3.0.5a, which was also
<a href="http://www.kde.org/announcements/announce-3.0.5a.html">announced</a>
today, or to apply the
<a href="ftp://ftp.kde.org/pub/kde/security_patches/">patches</a> provided
for KDE 2.2.2.  Due to the year-end Holidays, few binary packages are
available at this time.  Please check
the <a href="http://www.kde.org/info/3.0.5a.html">KDE 3.0.5a information
page</a> and your vendor's website periodically for available packages.
 Note that some vendors are expected to incorporate
the security improvements into new builds of KDE 3.0.5. 

<!--break-->
