---
title: "KDE Print: Developer Tutorial Now Available"
date:    2002-04-10
authors:
  - "mgoffioul"
slug:    kde-print-developer-tutorial-now-available
comments:
  - subject: "Precise dimentions of an image?"
    date: 2002-04-10
    body: "Slightly OT - Is there a user way to print an image with precise measurments?  I have some VCR labels, and I want to print them out with a specific height and width.  I finally imported into KWord, but it seems to me there should be a quick and easy way to do it in KView or something.\n\nIf not, I'll probably make a quick and dirty cover / label printing app by hacking the example code.  I print four or five labels a week.  Stop me if such a beast exists.\n\n(I've also found that the global imperial versus metric measurements settings are not recognized by several apps.  I need to add that, plus a bundle of (very minor, mostly cosmetic) xinerama bugs.  Plus the only annoying bug in 3.0 - an odd \"you can only rename one file\" bug).\n\n--\nEvan \"Very pleased with 3.0\" E."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Precise dimentions of an image?"
    date: 2002-04-10
    body: "Actually, the KPrinter object resolution defines the real size of the printed image. A 300 pixels image will be 4 inches wide when printed with 75 dpi resolution. So, programmatically, you can either set the KPrinter resolution manually so that your image have the correct size, or rescale your image according the KPrinter resolution.\n\nAbout the availability of such a feature in existing tools, the demo application I wrote does it (I actually also missed that feature to print CD covers, that's why I implemented it). Normally, kuickshow should also implement something like that in future versions.\n\nMichael."
    author: "Michael Goffioul"
  - subject: "Re: Precise dimentions of an image?"
    date: 2002-04-10
    body: "Here's an idea: since CDs have a standard dimension how about having a print format for it just like for A4, Legal and bunch of envelopes. Just a thought\nCheers,\n\nKarim\n"
    author: "Karim"
  - subject: "Thanks!"
    date: 2002-04-10
    body: "Many thanks to the author(s) of the tutorial!\nNow I have to leave, there's some source waiting for me..."
    author: "none"
  - subject: "Re: Thanks!"
    date: 2002-04-10
    body: "Michael Goffioul is the author of the tutorial and I'd like to say another thanks to him for creating (singlehandedly as I can understand) the amazing KDEprint module! Excellent job!"
    author: "Matt"
  - subject: "Re: Thanks!"
    date: 2002-04-10
    body: "> Michael Goffioul is the author of the tutorial and I'd like to say\n> another thanks to him for creating (singlehandedly as I can understand)\n\nI recently met him -- fortunately he has still both hands in good health... ;-)\n\nBut he did it single-\"headedly\" by and large!\n\n> the amazing KDEprint module! Excellent job!\n\nDefinitely true. I myself was in for it from the beginning just because\nits excellent CUPS support. This fascinated me so much (and still does)\nthat I tend to be blind to KDEPrint's overall architecture, which is highly\nmodular, very flexible and extremly versatile. Just look at the support \nfor different print subsystems (print engines) between which you can even\nswitch \"on the fly\"! Or the \"special printers\", the \"external filter plugin\ninterface\" and many new features... And gosh!, the guy is still delivering\nnew stuff by the week into CVS-HEAD..."
    author: "Kurt Pfeifle"
  - subject: "OFF TOPIC : writing with different encodage"
    date: 2002-04-13
    body: "What's happened ? With kde 2.2.2, I can choose the encodage with kedit (I can't choose utf-8 unfortunately, but ISO-8859-3 is one of the possibility), but now, with kde 3.0, I can't choose the encode with kedit, kwrite, kate. Hey, you simply not use different encodage. Personnaly, I need to be able to edit ISO-8859-15, ISO-8859-3 and UTF-8 text file, why is it impossible ? It's not very serious :(("
    author: "off topic man"
  - subject: "Re: OFF TOPIC : writing with different encodage"
    date: 2002-04-15
    body: "Mi opinias same : tio estas tre nemalhavebla :( Kiel skribus, ?o a? ?o, kun kedit-o kaj kwrite-o, sen UTF-8a kaj ISO-8859-3a ?"
    author: "D-ro E."
  - subject: "please help"
    date: 2002-10-20
    body: "i am a programing white kde developer \nplease help me to write a program \n"
    author: "amir sadughi"
  - subject: "Re: please help"
    date: 2002-10-21
    body: "I'm not against helping you, but maybe:\n1) you should describe more explicitely your problem\n2) you should use the KDE development mailing list\n\nCheers.\nMichael."
    author: "Michael Goffioul"
---
A tutorial on <a href="http://printing.kde.org/developer/tutorial/">programming with the KDEPrint module</a> has been made available on the <a href="http://printing.kde.org">KDE Print web site</a>. The tutorial is intended for application developers who wish to make use of KDEPrint within their applications. It covers the <a href="http://printing.kde.org/developer/tutorial/basic.phtml">basic usage</a> as well as more advanced features such as the <a href="http://printing.kde.org/developer/tutorial/custom.phtml">print dialog customization</a> or the <a href="http://printing.kde.org/developer/tutorial/preview.phtml">automatic preview mechanism</a>. The tutorial is illustrated with code examples and screenshots. Most of the code examples are taken from a <a href="http://printing.kde.org/developer/tutorial/imgviewer.tar.gz">demo application</a> (a small image viewer), available for download. Enjoy!
<!--break-->
