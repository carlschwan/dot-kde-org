---
title: "Quanta Plus 3.0 Final Rolls Out"
date:    2002-10-10
authors:
  - "elaffoon"
slug:    quanta-plus-30-final-rolls-out
comments:
  - subject: "xml editing features of quanta?"
    date: 2002-10-10
    body: "\nI can't seem to find a straight forward list of features on\nthe site anywhere. How well does Quanta do as an XML editor?\n\nI do more XML than HTML or PHP or anything like that, and have\nbeen looking for a decent KDE based XML editor.\n"
    author: "corey"
  - subject: "Re: xml editing features of quanta?"
    date: 2002-10-10
    body: "> I can't seem to find a straight forward list of features on the site anywhere.\n\nThat's because I can't keep up with the feature set in any detail and still have any semblance of a life. In fact I often open Quanta from CVS with exclamations of \"Wow, I didn't know we had that\".\n\n> How well does Quanta do as an XML editor?\n\nWell first off there is also kxmleditor, a tree based XML editor you might look at. Quanta has it's roots in HTML but has been uprooted so to say. We use DTDs. I would have to verify with Andras our current status on our DTD parsing code. As I say, it's difficult to keep up with everything in quanta. Our end objective is to provide full auto-completion from a DTD. Enhanced functionality comes in our definition .rc files and tag files which exist in in their own DTD directory. These files are XML.\n\nCurrently we are good for XML but to be very good one needs to do a little bit of XML set up for their dialect and a little point and click. I will list out what you can get for this here. First, by defining your tag files and definition.rc XML file you will get full autocompletion as well as the full function of the structure tree for analyzing and navigating your document. By adding tagxml dialog descriptions you can get full dialog capability for any tag. Using Actions from the configure menu you can assign toolbar actions including a quick tag definition or a script defintion which can use any language that executes in the shell. This is very poweful. You can use this to validate documents or to run visually created dialogs with Kommaander which we will be releasing next week. You can create custom toolbars and even have toolbars associated with your project. In addition the upcoming 3.1 allows you to save groups of files in a project as a veiw to open and close as a group. Project views can include toolbars too.\n\nBeyond this we currently have the XSLT DTD installed and are working to integrate a graphical version of xsldbg into either 3.1 or shortly after. We will be adding tools for XML entities too. We are also working on a pre configured DocBook DTD setup. Quanta has a plugin interface and we intend to have various XML schema validators as plugins shortly, although you can configure plugins as a user as well as program actions too. We think it's better for most users if it's already on the menu.\n\nI don't know if you could say that Quanta is a really good XML editor yet as is. I do know that if you tend to work with a limited number of DTDs right now, with a little work you can make it virtually perfect. Our objective was not to deliver our hard coded idea of what you want so much as to deliver a tool that you could make do exactly what you want first. We hope with community imput from users to flesh out these features into a large number of preconfigured setups.\n\nI hope this helps answer your questions. We have done our best to make sure Quanta can be configured by a user to do any task related to tagging or scripting. Feel free to contact me if I can be of any further help.\n\nEric Laffoon\nQuanta plus project manager"
    author: "Eric Laffoon"
  - subject: "Re: xml editing features of quanta?"
    date: 2002-10-11
    body: "\nThis might be totally outside the scope for Quanta, but it would be cool to have an XML editor that supported XML Schemas.  An XML Schema is like a DTD, but it it geared towards data entry, so you can say that an element should contain a value between X and Y, and you can give an element help text etc. An XML Schema can be transformed into a data entry hierarchy (like a spreadsheet, but it's a hierarchy). If it would be possible to use Kommander to not only create dialogs, but inline the dialogs into the page (as a kpart?) then it should be possible to \"compile\" an XML schema using Kommander or something similar into a data entry form!  That would be cool!"
    author: "astor"
  - subject: "Re: xml editing features of quanta?"
    date: 2002-10-11
    body: "I am familiar with the concept of schemas, though I haven't used them yet. It is currently possible to use various schema validation programs in Quanta using our custom actions. You can also of course write a schema in Quanta. As far as total integration goes we wanted to get DTDs mastered first and then move on to schemas. Also we wanted to look into it more WRT whether we could incorporate exsiting tools as plugins for support or whether we would need to write something ourselves to best integrate this.\n\nAs far as integrating Kommander for data entry this is a rather natural thing for it. The mechanics of it should be fairly easy to resolve as a user, but if not let us know. Kommander uses XML ui files which are essentially the QT Designer ui file with custom widgets allowing you to associate text and actions. I don't want to get into detail here. We will be introducing it next week. However it is able to send and receive via stdout and DCOP. Using a language like Perl you can access sockets too. We have on our do list making it produce embeddable parts. This would enable for instance an intranet to embed a tabbed dialog or tree into a frame in konqueror. As far as further interaction and programming it goes the rule with a program like Kommander is that it will end up doing things the designers didn't have the remotest thought of. My use of Kaptain for PHP class dialogs was essentially using a function primarily for test in an unexpected place. So we're quite happy when someone finds a use that we didn't imagine yet."
    author: "Eric Laffoon"
  - subject: "Re: xml editing features of quanta?"
    date: 2002-10-11
    body: "Sorry, preview lost the screenshot."
    author: "Eric Laffoon"
  - subject: "thanx a lot"
    date: 2002-10-10
    body: "thank u for quanta.\ni am doing all my php coding wirh it. very nice. is the installation/configuration of the debugger explaint anywhere?\n-gunnar"
    author: "gunnar"
  - subject: "Re: thanx a lot"
    date: 2002-10-10
    body: "The debugger has no longer been maintained since before PHP 4.1 which had substantial changes. Installing it requires building PHP which is probably the most complex set of switches I've seen. So you can pretty much only use it on a local build of an outdated PHP. We hope shortly to look into this and address these issues. We'd like to offer solutions that are more up to date as well as possibly some that are less invasive.\n\nOne thing that is helpful right now is the ability to use the RMB on the strucutre tree and set up to parse as PHP and see all the variables and functions on a page. At least we are reducing the cause of difficulties in PHP by a better grasp of your page. ;-)"
    author: "Eric Laffoon"
  - subject: "Re: thanx a lot"
    date: 2002-10-11
    body: "> One thing that is helpful right now is the ability\n> to use the RMB on the strucutre tree and set up to\n> parse as PHP and see all the variables and functions\n> on a page. \n\nSorry Eric,\n\nthis doesn't work for me neither in the 3.0 nor in the current CVS version (just checked out to be sure). I compiled it myself under kde3.1b2 using qt3.05.\n\nAlso interesting is that PHP is not offered as a DTD-Choice by changing the DTD via project-options or the tools-menu, it is offered only as an option for new files in the quanta- and project options but has no effect on the structure view. \n\nclicking with the right mouse button anywhere in the structure view doe not do anything. No emty popupmenu or such.\n\nWhat to do?"
    author: "Jean-P. Febreze"
  - subject: "Re: thanx a lot"
    date: 2002-10-11
    body: "Well you can see it as the first screenshot on the site. I've included a snapshot here. I can offer two possible reasons. For one I had trouble building KDE beta 2 with QT 3.0.5 and I guess I'll just build CVS. We have to look into it because our current CVS has been reported to have issues with it. As a last resort, quantarc has been known to cause oddities in the past. It has been far less lately but you might try renaming it. It's in $KDEHOME/share/config.\n\nAs far as selecting PHP as a primary DTD that's a bad idea so we eliminated it. PHP is in fact a psuedo-DTD in Quanta because it is not really a DTD. It is a preprocessor. Making it a primary DTD doesn't make sense and it makes the use of HTML in the document a problem. Only if you had no HTML at all in a file would it make sense, however the editor functionality as it is remains identical for PHP this way so nothing is lost. I hope this makes sense. The only thing affected here would be the structure tree."
    author: "Eric Laffoon"
  - subject: "Question about donations and such.."
    date: 2002-10-10
    body: "Is there any way that things like the KDE e.V. or League can sponsor development? \n\nUnfortunatly, most people are greedy and won't pay for software unless they have to. Perhaps make Quanta closed source and make people pay a total of $100,000 to open it up again (blender :)). I'm just joking, of course. \n\nLastly, how about finding a company that can give donations or sponsor part of the project without taking it over. That seems quite do-able. \n\nI'd donate to Quanta myself, but I only make use of html editors like once a year. People who use quanta everyday should contribute! "
    author: "loft"
  - subject: "Re: Question about donations and such.."
    date: 2002-10-10
    body: "> Is there any way that things like the KDE e.V. or League can sponsor development? \n\nI could ask. Nobody in free software is rolling in money. However we are capable of making our appeal I think. \n\n> Unfortunatly, most people are greedy and won't pay for software unless they have to. \n\nI disagree. I think it's mostly a matter of economics. I don't rate low donation ratios at all to greed. You can only part with so much for so many causes. I think the addition of merchandise is good. I've done fundrasing for charitable causes and merchandising is the way to go. People get a cool product and the cause gets money. Traditionally causes get 10% from professional organizations which they are thrilled to have. Here we realize substantially more as all net profits are returned and no labor costs are factored in.\n\n> Lastly, how about finding a company that can give donations or sponsor part of the project without taking it over. That seems quite do-able. \n\nWe have that. The company is Kittyhooch LLP. Unfortunately it's a relative start up and it's my company so if it doesn't have a good month it just pays me less and I do without satellite TV another month and limit my spending on cigars and supplements so I can pay my guys. It's worth it. When I begin pursuing web work again shortly I will be phenominally more productive. \n\nI am open to corporate sponsorship. If you think about it any company using free software like Quanta would be incredibly well served to take a small fraction of their income and divert it to tool development. I'd be the most incredible tech support you could imagine for 1% of the revenue generated by any company using quanta to generate income with. It is foolish to limit or risk limiting your efficiency by determining that your tools are really free. It is free to use, not to produce.\n\nI consider myself the fortunate one. I really do believe it's better to give than receive. The emotional rewards for this project are impossible to describe. I did not start out to make free software. I started out wanting an editor. I fell in love with what I was doing and the lives I was touching. You can't imagine what it feels like to know you are touching the lives of potentially millions of people who appreciate you. Do you get that at your anywhere else?\n\n> I'd donate to Quanta myself, but I only make use of html editors like once a year.\n\nIt's okay, we don't exclude you. You can buy a mouse pad (or $5 woth of Quanta satisfaction). ;-) \n\n> People who use quanta everyday should contribute! \n\nAs their heart leads. I appreciate your encouragement. I finally decided to begin asking when I realized how much I was really giving in time and money. I don't want anyone to feel bad if they use it and never give anything. There are a lot of people I want to give Quanta to and I never want anything from them. That gift is my reward. You just can't assume the next guy will give your gift.\n\nOur choice is to become an outstanding first class tool. Many people may have been perfectly happy editing only (depricated) HTML. People may think we should just stick with the basics or try to reach our goals in 5 or 10 years instead of 1-2. Not contributing is a good way to vote for that. ;-) If you like where we're going our mouse pads and T-Shirts are reasonably priced. "
    author: "Eric Laffoon"
  - subject: "Kudos!"
    date: 2002-10-10
    body: "Congratulations on the 3.0 mark release. I think Eric's loving devotion to Quanta Plus is an inspiration to many, and certainly goes to show Free Software's advantage over proprietary software in being a labour of love.\n\nAlso, Wil Wheaton of Star Trek fame uses Quanta Plus. Shouldn't you? ;-)\n"
    author: "Haakon Nilsen"
  - subject: "Re: Kudos!"
    date: 2002-10-10
    body: "Aw come on... you're making me blush. ;-)"
    author: "Eric Laffoon"
  - subject: "XML word wrap mode"
    date: 2002-10-11
    body: "I have a question about XML editors. Is there any XML editor, preferably on KDE, that handles word-wrap correctly when used with XML indentation? Editing DocBook in Kate or KWrite is really painful. You have to manually insert newlines, which breaks formatting if you want to add more text in the middle of the paragraph. If you use word wrap, then indentation gets screwed up, the next line starts entirely left-justified, instead of following the previous line's indnetation.\n"
    author: "Rayiner Hashem"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-11
    body: "Kate-cvs has word-wrap.\nQuanta uses kate for editing..\nSo wait for KDE 3.1\n\nBuba"
    author: "buba"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-11
    body: "It's had word wrap. I think they are referring to a more intelligent soft wrap. Last I heard Christoph was working on that. I heard they did not think it would make it into 3.1 and I have not noted it in either beta. Regardless as soon as your version of KDE/Kate has it then it appears in Quanta."
    author: "Eric Laffoon"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-11
    body: "It's in CVS, but a bit hard to find. There's a switch \"Dynamic Word Wrap\" in Menu \"View\". It isn't in the configure dialog. "
    author: "eva"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-11
    body: "Wow, and I was still using kedit for soft wrap. Thank you!"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-11
    body: "Thanks! It seems it snuck in on beta 2 but not beta 1. We will address this setting promptly."
    author: "Eric Laffoon"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-11
    body: "Yep. It went in just after beta2 was tagged (we've been working on it in a branch for a while). Please test it and report any bugs you find so it's solid for 3.1 - I don't know of any bugs at the moment.  It also needs some usability testing (what should the behaviour of the home / end keys be?).\n\nYou can turn it on by default in editor -> view defaults."
    author: "Hamish Rodda"
  - subject: "Re: XML word wrap mode"
    date: 2002-10-12
    body: "Hey, cool. Didn't notice yet, that the dynamic wrap already made it into the configuration :-)\n\nTo me the dynamic word wrap already seems to be very stable. Didn't have any problems yet. Great work!\n\nBTW, some configuration of dynamic word wrap dependent on the current file type would be great. E.g. I'd like to have dynamic word wrap in tex files, but not in C++ source code. (Sorry, couldn't resist to post this wish ;-)"
    author: "eva"
  - subject: "Per-mimetype configs"
    date: 2002-10-13
    body: "There's some work going on towards a Kate plugin to fulfill this need... :)"
    author: "Hamish Rodda"
  - subject: "thanks !!"
    date: 2002-10-11
    body: "just wanna say thank you for this great piece of software.\nsomething like quanta is really important for linux I think.\nvery well done !!"
    author: "markus"
  - subject: "Quanta 3.1"
    date: 2002-10-11
    body: "> Quanta 3.1 will be released with KDE 3.1 too so it's coming very soon.\n\nAre you in a hurry to rush version numbers like this? :-)"
    author: "Anonymous"
  - subject: "Re: Quanta 3.1"
    date: 2002-10-12
    body: "> Are you in a hurry to rush version numbers like this? :-)\n\nYes, you got me. Shucks! We were hoping to slip out 5.1 by the end of the year. ;-) Okay, actually it's just our hyper speed programming...\n\nOkay, I'll stop clowning. We have been adding features and haven't got the full feature set we want. We didn't want to have a 3.0 PR7 just to get the features in... so as soon as we had a feature set we were happy with we froze it and shipped 3.0 because we know lots of people don't want to use a pre release and there's no reason to hold it up for features we can do in a point release. So when I contacted Dirk he said he'd like to ship it with KDE 3.1 and when we looked at our code base we could only do a few new features and enhance some of our in process features... so when I asked if we should do a point release one morning as I was waking up I was shouted down for being an idiot as we do have feature improvements... (I really do love this project but I need to be awake when I type as Andras is 10 hours ahead of me.) Of course I knew someone would ask your question.\n\nYou get the gold star. Look for version 3.5 later next month. ;-)"
    author: "Eric Laffoon"
  - subject: "WYSIWYG ?"
    date: 2002-10-11
    body: "Hi,\nwill there be a version with WYSIWYG-Editing ? It\u00b4s nice to do a quick job with an editor like Dreamweaver (this program sucks, but i\u00b4m just using it :) ) because you dont need to type any HTML and Dreamweaver doesnt modify your additional Code in a hand-coded webpage.\n\nMarkus"
    author: "Markus"
  - subject: "Re: WYSIWYG ?"
    date: 2002-10-11
    body: "Hi, see this link http://dot.kde.org/1031680234/ and look under kafka and wysiwyg topics. I've addressed this at some length. If I missed something let me know. ;-)"
    author: "Eric Laffoon"
  - subject: "Thanks!"
    date: 2002-10-11
    body: "Well, first of all this dot article caught me a bit by surprise since I thought \nI had been using Quanta 3 final for the past 15 days or so \n(with KDE 3.1 CVS, as a matter of fact). \nAny bugfixes I should know about?\n\nAnyway, I want to say a big thanks to the Quanta developers. I am currently \nusing Quanta for my (very first) professional PHP project, and I am \nvery happy with it. Esp. since I am not very familiar with PHP, Quanta's \nability to complete my code is a real life(and Coca Cola)-saver.\n\nI am also very impressed by Quanta's ability to \"detect\" html tags \n*within* PHP strings and code-complete them too! And project management, \nand web download/upload of the project (to name only a few)... \nhow did you manage to put so much *useful* functionality on a single product? I really am impressed :-)\n\nAmazing piece of work guys! I thought Eric was exaggerating when he \nwas writing \"we will get better than DreamWeaver\", but it turns out he knew what he was talking about :-)\n\nBTW I would really like to donate money (because I'm *earning* money \nthanks to Quanta), but unfortunately I don't have a credit card \n(and don't think I *will* have in the forseable future). \nAren't there any other ways?\n"
    author: "Dimitris Kamenopoulos"
  - subject: "Re: Thanks!"
    date: 2002-10-11
    body: "Actually I'm terribly emberassed because I have been so busy I have not put up new PHP docs. You can download the docs (which I will put up today) and install them in Quanta. This will give you context help. You will also have off line help with your PHP.\n\nSince you're new to PHP let me introduce you to some other cool features you may not know about. I created a sites directory in my server root on my local system with my user as owner. I then linked it to my home directory. Now my PHP files are actually on a live server. Now in the project settings on the Network tab you can click the box \"Use project prefix\" and enter your location like so... http://localhost/sites/kittyhooch/public_html/. Now when you preview a dynamic page in Quanta you will see your PHP rendered correctly. Now to sort it out select the structure tree tab on the left, right click and select Parse as->PHP. Now you will see your functions and variables listed on the left. We still have work to do like parse off page, respect scope and add classes but we're working on it as fast as we can. ;-) BTW you can add dialogs to PHP tags using tagxml described in our docs in the PHP tag files. However with the introduction of Kommander next week you will be able to draw dialogs for very custom use and include them on project toolbars. Look for the demo soon.\n\nAs for your compliments... they are very satisfying. Thank you. Knowing that we are helping you to deveolop and to earn money with Quanta is the most rewarding currency for me... though I will still accept donations. ;-) If you are in a country where PayPal is available they do a bank transfer so you do not actually need a credit card. Also many banks offer debit cards that function as credit cards too. If you do not have these options and still feel compelled to donate please contact me directly to look for a means.\n\nBTW check out our links for good resources on PHP. I wish you much success and satisfaction in your web development efforts. I hope we play some small part in your success.\n"
    author: "Eric Laffoon"
  - subject: "www.knome.org ?"
    date: 2002-10-11
    body: "What's up with this?\n\nwww.knome.org ???"
    author: "KDE User"
  - subject: "Re: www.knome.org ?"
    date: 2002-10-11
    body: "A joke by Bero pointing to http://dot.kde.org's IP."
    author: "Anonymous"
  - subject: "Kudos to Eric (and not only for the code)"
    date: 2002-10-12
    body: "I'm sure you recieve a lot of positive replies on your releases. But this time I don't want to give you kudos for the code. We all know it's great. I want to give you kudos for the way you manage this project. You are a great inspirator. I don't do anything with webdevelopment at all but the way you promote Quanta makes me follow it's development and even try it. Also kudos to the design and long terms goals of Quanta. The fact that you don't let WYSIWIG pushed in Quanta before it's ready for it, although a lot of people want it, makes you an excellent maintainer/developer. Last but not least: kudos to all the people that help Eric with this great product."
    author: "John Herdy"
  - subject: "Re: Kudos to Eric (and not only for the code)"
    date: 2002-10-12
    body: "What can I say? This is candy.\n\nI can only credit my passion and obsession with the project. Much of this has been fueled by the decisions I've been faced with along the way and the numerous emails which have shown me that a simple piece of software can in fact touch people's lives. For all the people who nitpick at free software but never actually play a part I wish I could convey how incredible it feels to know your efforts are apprciated. Perhaps I should post my favorite Teddy Roosevelt quote. It's long. To paraphrase, its better to go down in flames with purporse than to sit in the cold without one.\n\nAt any rate, I just happen to be out front. There's a long list of people to thank but I will just do the short list and say that some of the most influential people on our team are Andras Mantia, who I cannot praise enough in this lifetime, Marc Britton, whose enthusiasm and efforts on Kommander should be legend and Robert Nickel who is always there with some little piece of the puzzle to make things better. There are more too. Without these people and many others I would have little to talk about."
    author: "Eric Laffoon"
  - subject: "Vim as the editor?"
    date: 2002-10-14
    body: "Now that Vim is in KDevelop, any chance of using it as the editor for Quanta? I'd really like to use the Document Structure piece of Quanta, but non-modal editing is for the birds."
    author: "Smampy"
  - subject: "Re: Vim as the editor?"
    date: 2002-10-14
    body: "KDevelop and quanta are two different projects with different teams. Of particular note is how many people you find credited on KDevelop as opposed to Quanta. Gideon has a great plugin architecture that makes using Vim possible. For us it was like looking at a Ferarri. It's very impressive but we just can't make the payments. \n\nWe have a long list of things we want to do and we focus on efficient web development. Hopefully there is much more than our structure tree to like. I would say that short of several very talented people joining out team with the sole focus of a pluggable editor you will not likely see Vim editing in Quanta before you see it doing WYSIWYG. However Quanta can do some plugins and it can also do programmable actions that can zip files back and forth. Have a look at Settings>Configure actions."
    author: "Eric Laffoon"
  - subject: "Re: Vim as the editor?"
    date: 2002-10-16
    body: "Thanks for the response. \n\nI realize that they're different projects, but I thought the whole point of being part of a desktop environment... hell the whole point of shared libraries was so that applications wouldn't have to re-invent the wheel and would be able to take advantage of new features. I know it's not that simple, but I don't think it should be all that complex either.\n\nI haven't used Quanta for years because I really can't handle non-modal editing."
    author: "Smampy"
---
The Quanta team is pleased to announce the availability of Quanta 3. <a href="http://quanta.sourceforge.net/">Quanta</a>
has been transforming from a basic HTML editor to an extremely competent
and flexible tagging and scripting editor. Quanta 3 supports XHTML, XML dialects,
XSLT and more. Since adding these DTDs takes only XML skills, the upcoming 
version 3.1 will feature even more languages. We will be making a language/dialect
installer too. Inside 3.0 you'll find drag and drop templates, 
auto-completion for any installed language, now including PHP variables, and
a structure tree that introduces modes. There's too much to list here so 
have a look at our 
<a href="http://quanta.sourceforge.net/main2.php?newsfile=q3release">full announcement</a>.
Quanta 3.1 will be released with KDE 3.1 too so it's coming very soon. Also 
there are more updates to our site including in depth explanations of how your
<a href="http://quanta.sourceforge.net/main1.php?actfile=donate">donations impact the project</a>. 
Also we have new links up to where our sponsor is selling 
<a href="http://quanta.sourceforge.net/main1.php?contfile=promo">Quanta Promotional items</a> 
and donating all profits to the project. These are a lot of fun! We will also be
introducing tutorials soon on using actions and building and using script dialogs
with the upcoming Kommander. We want to extend
our best to our users and we hope you find your requests fulfilled in your next download.

<!--break-->
