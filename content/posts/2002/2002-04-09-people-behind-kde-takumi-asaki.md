---
title: "People Behind KDE: Takumi Asaki"
date:    2002-04-09
authors:
  - "Dre"
slug:    people-behind-kde-takumi-asaki
comments:
  - subject: "PSO on DC"
    date: 2002-04-09
    body: "Argh, another PSO addict. I tell you, that game is taking over the world. That's supposed to be Linux's job :-)"
    author: "Carbon"
  - subject: "He's got taste..."
    date: 2002-04-09
    body: "He's drinking our \"national beer\" in the picture. I wonder where the pizza is!\n\nTo me, this proves that all men are equal, but all hackers are more equal ;-)"
    author: "Steven"
  - subject: "Re: He's got taste..."
    date: 2002-04-10
    body: "Sorry, if this offends you, but in Belgium we have an expression: \"Heineken tastes like urine\". No kidding! :-)\n\nOkay, Heineken is known all over the world. It isn't easy to make a beer that 'konquered' the world and I have the greatest respect to that.\n\nWhen you ever get a chance to drink a Belgian beer, just do it (TM). Most people will agree a Duvel, Jupiler, Stella, Hoegaarden,...  tastes better.\n\nI only named a few popular beers, but the last ten years we had more than 4400 (!) different beers. (Not everything is still available of course.) Want proof? http://www.dma.be/p/bier/1_14_uk.htm :-)\n\nhttp://www.beerparadise.be"
    author: "Andy Goossens"
  - subject: "Re: He's got taste..."
    date: 2002-04-10
    body: "One word.\n\nGuinness.\n\nIt's thick life in a glass.  I'm fairly certain that you can live entirely off of it.  A friend calls it \"Irish soup\", and claims it's the world's finest fermented peat moss.  If you can't make lewd diagrammes in the head of your drink to amuse your female companions, it's not beer.  Best taken orally with copious amounts of cheerful company, although it also compliments a good friend and a quiet booth in a somber moment.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: He's got taste..."
    date: 2002-04-10
    body: "> Sorry, if this offends you, but in Belgium we have an expression: \"Heineken tastes like \n> urine\". No kidding! :-)\n\nAnd here we just think that.  ;-)\n\nActually my personal favorite is Negra Modelo -- a Mexican beer.  Latin American dark beers (in my opinion) are smoother than the European varieties.\n\nhttp://www.corona.com/negramodelo.html\n\n\n"
    author: "Scott"
  - subject: "Re: He's got taste..."
    date: 2002-04-10
    body: "I can only agree with that. Even dutch know that there is only one\nbeer:  Grolsch !\n"
    author: "Rocco"
  - subject: "Re: He's got taste..."
    date: 2002-04-10
    body: "Only one beer? Poor guys... :-)\n\nIn Belgium we have more than 400 beers to choose from. I guess there would be at least 1 beer you like. :-)"
    author: "Andy Goossens"
  - subject: "Re: He's got taste..."
    date: 2002-04-10
    body: "Although I have to agree with Rocco that Grolsch is the best lager beer in existence today (especially since I live in Twente where it is brewed), there are a variety of other good lagers and beers from Holland, like Gulpener or Alfa (lagers) or La Trappe (the only dutch Trappiste).\n\nThere are more (and better!) Belgian beers, but luckily we can get almost all of those here in Holland :)"
    author: "Theo van Klaveren"
  - subject: "Re: He's got taste..."
    date: 2002-04-11
    body: "I agree with you. Us, belgians may not be famous for many things, but definately for the beer!\n\"Beer, helping people code KDE since 199X (was it 1995?)\"\nAha, I just came up with an idea for a new KDE background :)\n(the normal slogan is: \"Beer, helping ugly people have sex since xxxx\")\nOfcourse imo is \"onze jup(iler)\" the best beer around :)\nAnd Duvel ofcourse.\n\nCheers mate"
    author: "blashyrkh"
  - subject: "Re: He's got taste..."
    date: 2002-04-12
    body: "mmmm, stella :)"
    author: "Drasil"
  - subject: "Re: He's got taste..."
    date: 2002-04-12
    body: "Yeah! Stella Artoise! :)\n"
    author: "Michel Stol"
  - subject: "Thanks to this interview I ported KRenju to KDE 3"
    date: 2002-04-10
    body: "Thanks to this interview I ported KRenju to KDE 3.  Once my hosting provider bothers to bring Apache back up (grumble grumble whine*), you can get it at http://www.freekde.org/neil/krenju-0.91.tar.bz2\n\nYou should probably also watch http://www.kde.gr.jp/~asaki/krenju/ for information.\n\n*Now you know why I won't re-launch freekde the news site until I have my own box. :-)"
    author: "Neil Stevens"
  - subject: "Re: Thanks to this interview I ported KRenju to KDE 3"
    date: 2002-04-10
    body: "I invite freekde.org to hobble along with the dot!\n\nTwo websites at 100 bits/s is surely better than one at 200 bits/s!\n\n-N.\n--\nThese views do not represent those of my non-existent employer."
    author: "Navindra Umanee"
  - subject: "Re: Thanks to this interview I ported KRenju to KDE 3"
    date: 2002-04-11
    body: "I put krenju-0.91 Neil ported\nhttp://www.kde.gr.jp/~asaki/krenju/krenju-0.91.tar.bz2\n\nThanks, Neil.\n"
    author: "Takumi ASAKI"
  - subject: "TABS...."
    date: 2002-04-10
    body: "In all the interviews I've read all prefer tabs.. Time to change the kde guidelines (or are they already, haven't read it for a long long time)?? ;)"
    author: "me =)"
---
In this week's episode of <a href="http://www.kde.org/people/people.html">The
People Behind KDE</a>, we travel to
<a href="http://www.city.osaka.jp/english/">Osaka</a>,
<a href="http://files.e-gov.go.jp/">Japan</a> for a beer with
Takumi Asaki.  Takumi, who loves <a href="http://www.renju.nu/">Renju</a>
and prefers
<a href="http://www.mikalina.com/Texts/tatami_big.htm">Tatami</a> to a bed,
is very active in making KDE accessible to the Japanese.
Read more about Takumi and his many talents
<a href="http://www.kde.org/people/asaki.html">here</a>.
<!--break-->
