---
title: "Quickies: Kinkatta-Lite, Linux Expo Birmingham, Ivan Retires"
date:    2002-01-09
authors:
  - "numanee"
slug:    quickies-kinkatta-lite-linux-expo-birmingham-ivan-retires
comments:
  - subject: "About Ivan..."
    date: 2002-01-09
    body: "I just wanted to say thanks to Ivan for the years of hard work that he put into the debian packages.  I worked with him several times, over the last year or so, and each time he was great.  Ivan, you'll be missed!\n\nD.A.Bishop"
    author: "David Bishop"
  - subject: "I'm here at CES, too !! `"
    date: 2002-01-09
    body: "Hi guys and gals,\n\ngreetings from the Sharp booth at CES from me, too !! :)) I'm sharing the booth with Ben to demo KDE, synchronizing the iPaq and the Zaurus with KDE/Qtopia Desktop and developing Qtopia apps using KDevelop. So far, people liked the show :) - there's lots of more people here now than there were at Comdex in November, so it's really worth showing off a real operating system with a real user interface :))\n\nHave fun,\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: I'm here at CES, too !! `"
    date: 2002-01-09
    body: "What's CES?  Comdex?  I wanted to link the event but wasn't sure what it was...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: I'm here at CES, too !! `"
    date: 2002-01-09
    body: "Try this:  http://www.cesweb.org/"
    author: "reihal"
  - subject: "Re: I'm here at CES, too !! `"
    date: 2002-01-09
    body: "CES= Consumer Electronics Show...  It is not COMDEX..  despite what my computer teacher says...   ;-p"
    author: "Ill Logik"
  - subject: "A few quick links from the article (Score: 5, Informative)"
    date: 2002-01-09
    body: "A nasty thread which recently erupted on debian-devel. A new libpng version broke many KDE packages and Ivan was flames. Link:\n<p>\n<a href=\"http://lists.debian.org/debian-devel/2002/debian-devel-200201/threads.html#00005\">debian-devel-200201/threads.html#00005</a>\n<p>\nBut that was apparently only the last straw. Ivan explained himself later, after having orphaned the packages. Link:\n<p>\n<a href=\"http://lists.debian.org/debian-kde/2002/debian-kde-200201/msg00079.html\">debian-kde-200201/msg00079.html</a>\n<p>\nNow watch as my karma goes through the roof! Oh, wait...\n"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "Debian KDE Live!"
    date: 2002-01-09
    body: "If anyone feels like talking to the new maintainers, they hang out regularly on OPN #debian-kde as calc and daniels (iirc)."
    author: "Carbon"
  - subject: "DAMN"
    date: 2002-01-10
    body: "Damn, sorry to see Ivan go. I've used his (high quality) packages in Debian for so long now. Oh well, everyone must move on at some point."
    author: "syn"
  - subject: "Zaurus network connection"
    date: 2002-01-10
    body: "How is the Sharp Zaurus connected to the 'net? Does it have some kind of connectivity built in or are they using an expansion card? Thanks."
    author: "al"
  - subject: "Re: Zaurus network connection"
    date: 2002-08-03
    body: "the cradle for the zaurus uses what is called tcp/ip over usb.  This is a way to form a network between both systems.  The host computer will use the cradle as a network device.  The host computer would then need to be set up as a gateway to forward http, ftp, etc. between the two networks.  There are also network interface devices (wirless and non) that can be used to connect directly to a LAN or cable modem."
    author: "Jon"
---
<A href="mailto:icefox@mediaone.net">Benjamin C. Meyer</a> wrote in with the news that <a href="http://kinkatta.sourceforge.net/">Kinkatta</a> has been ported to <a href="http://www.trolltech.com/products/embedded/index.html">Qt/Embedded</a>.  Kinkatta-Lite has been tested on <a href="http://www.infosync.no/show.php?id=1292&page=1">Sharp's new Zaurus</a> as well as the <a href="http://www.compaq.com/showroom/handhelds.html">iPAQ</a>.  Ben will be demo'ing Kinkatta-Lite on the Zaurus at <a href="http://www.cesweb.org/">CES Las Vegas</a>, courtesy of <a href="http://www.sharpelectronics.com/">Sharp</a>, and will release source and binaries when he gets back.  Until then we will have to content ourselves with <a href="http://www.csh.rit.edu/~benjamin/kinkatta-lite-pics/">the screenshots</a>.  <a href="">Jono Bacon</a> is <a href="http://jono.kdedevelopers.net/events/linexpbrum.html">currently organising</a> the KDE presence for <a href="http://www.linuxexpobirmingham.com/">Linux Expo Birmingham</a>, UK on May 29th + 30th.  Jono is scheduled to talk on KDE in business, while Richard Moore will be giving a talk on <a href="http://developer.kde.org/documentation/kde2arch/xmlgui.html">XMLGUI</a>.  Get <A href="http://jono.kdedevelopers.net/events/linexpbrum.html">involved</a>.
Finally, Someone wrote in to point us to <a href="http://www.debianplanet.org/article.php?sid=552&mode=thread&order=0&thold=0">this article</a> at <a href="http://www.debianplanet.org/">DebianPlanet</a>.  Sadly it appears that the long time maintainer of KDE Debian packages,   Ivan E. Moore, has stepped down.  Ivan has been responsible for some of the best KDE packages around, and his work will be missed.  Other Debian developers have stepped up to fill in his shoes.
<!--break-->
