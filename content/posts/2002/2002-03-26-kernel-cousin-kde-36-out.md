---
title: "Kernel Cousin KDE #36 Is Out"
date:    2002-03-26
authors:
  - "rkaper"
slug:    kernel-cousin-kde-36-out
comments:
  - subject: "good issue"
    date: 2002-03-25
    body: "I love these development updates, but this one doesn't seem to have any links to the archived threads.  Oops, I guess they were forgotten.  Very important feature.\n\n"
    author: "ac"
  - subject: "Re: good issue"
    date: 2002-03-25
    body: "hrm. they were included in the xml, but got lost somewhere in the xml -> html parsing ... will look into this. thanks for noticing."
    author: "Aaron J. Seigo"
  - subject: "KOffice! Woo hoo!"
    date: 2002-03-25
    body: "Alright, good to see development is going on again. But I'd like to ask, now that GIMP is definetly going to have modular interfaces, why continue developing Krayon? GIMP has thousands of devel hours behind it, and it's really up to about the level of power as Photoshop (no easy task). The only real reason it hasn't been a KDE app is the Gtk interface, right? Or am I missing something?"
    author: "Carbon"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-25
    body: "There was KDE interface but it was controversial because of license.  Otherwise do it yourself.\n"
    author: "ac"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-25
    body: "To clarify, one of the early KDE developers ported Gimp to KDE (before 2.x), at least to a functional point.  One or more of the Gimp developers got really upset, and the ethical thing in the KDE developers mind was to discontinue the port.  Personally, I agree with his decision, but I would have checked with the other Gimp developers to see how they felt.  The \"legally allowed\" thing is not always the right thing to do, and the moral judgement not to \"steal\" someone elses code, even GPLed code, was understandable.\n\nTo the Gimp developers credit, I believe this was before Qt was GPL, so they had a right to their opinion that led to their objections.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-26
    body: "So what is stopping a Kimp now?"
    author: "reihal"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-25
    body: "I think it would be a VERY important step adding this HIGH quality soft to KDE.\nDefinitely we should add open source efforts. IMHO, GIMP and DIA are some of the great products that every desktop should have.\n\nAnyway I think it's easier adding the KDE widgets to the modular GIMP than continuing developing Krayon. But, of course, I might be wrong :-) I don't want anybody to be annoyed."
    author: "lanbo"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-25
    body: "Krayon probably will support Gimp plugins in te future (if it doesn't doe it already)\nProblem with Gimp (for me) is dat it does not follow any standards in KDE nor Gnome qua look and feel. So it would be great to have a KDE-interface with the power of Gimp underneath.\n\nRinse"
    author: "rinse"
  - subject: "Re: Gimp"
    date: 2002-03-26
    body: "After using Photoshop, Corel Draw/photopaint and psp. Gimp seems to suport everything and more as features go, but look at the GUI the desine of the main program and you tend to see holes. if say I wanted to load up two pictures from the filemaneger Gimp would be loaded twice, instead of putting some code in to pass the information along to the already open gimp and kill it's self off. as for the GUI well three of four windows each of them it's own proccess it's slightly confusing, even if they just hid the other ones from the KDE Bar it would make me happy.\n\npersonaly I prefare GIMP and would love it even more if some of these 'features' were cleaned up. but we can't be perfect I supose.\n\n-- DoctorMO --"
    author: "Martin Owens"
  - subject: "Re: Gimp"
    date: 2002-03-30
    body: "You can open files with \"gimp-remote -n\" instead \"gimp\". That way, if an instance of Gimp is running when you open a file, the file will open in the existing Gimp process. Otherwise, Gimp is started as usual. "
    author: "Peter Backlund"
  - subject: "Re: Gimp"
    date: 2002-03-30
    body: "right thanks, I'll have to change all my file dependecies now...\n\nWill there be future relieses?\n\n-- DoctorMO --"
    author: "DoctorMO"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-26
    body: "Yes, the gimp is a cool program with a bad interface.  A kde based gui that looks and behaves like photoshop's gui qould be wonderfull.  Support for the vector stuff from corel draw would be nice to.  Right now I have to have two programs open to do work with mixed vector and bitmap graphics.  Why can't the gimp and a vector graphics app merge.\n\nWaring: I really have no idea what I am talking about since I spend most of my time programming.  I only touch graphics apps to do small stuff that I do not feel like bothering our graphics department for."
    author: "theorz"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-26
    body: "Well, I generally don't like MDI apps... but... photoshop 5/6's interface is about 2000x better than GIMP's. \n\nGIMP with krayon's interfaced (abiet.. fixed up a bit), or krayon with GIMP's features would be great.\n\nAlso, the GIMP doesn't begin to compare with Photoshop yet for any serious production work (uhm, cmyk, proper color matching, vector tools, easy to use scripting system)."
    author: "fault"
  - subject: "Re: KOffice! Woo hoo!"
    date: 2002-03-26
    body: "Eh? Not for any serious production work? No, not for print work (like you said, no cmyk or color matching) but for web work, photo touchups, render postproduction, etc, it's fine. It doesn't need vector tools, because it's not a vector app, it's a bitmapping app. And it does support Perl, which is a really pretty easy and powerful scripting system, if you know the language."
    author: "Carbon"
  - subject: "The world of the web"
    date: 2002-03-29
    body: "is one where people pay 1500$ for an OS and \"office suite\" and then 500-600$ for Photoshop, 200$ for DreamWeaver and 2000K for there computer.\n\nI.E> they pay 5K so they can make web pages ... you have to hope they are ***really*** productive and/or their pages look ***really*** good.\n\nsheesh."
    author: "NameSuggesterEngine"
---
<a href="http://kt.zork.net/kde/kde20020315_36.html">Kernel Cousin KDE #36</a> is out and as the KDE developers are working on polishing up the project for a new major release, there is lots of news from the mailing lists. This issue covers media streams, wallpapers, <a href="http://www.koffice.org/">KOffice</a> and of course the nearing <a href="http://developer.kde.org/development-versions/kde-3.0-features.html">KDE 3.0</a> release.
<!--break-->
