---
title: "OfB Open Choice Awards 2002: KDE 3.0 Best Desktop"
date:    2002-07-28
authors:
  - "wbastian"
slug:    ofb-open-choice-awards-2002-kde-30-best-desktop
comments:
  - subject: "Not to forget the \"best of the best\" poll"
    date: 2002-07-28
    body: "... where KDE3.0 is leading by a comfortable margin and ~1/3 of votes ;-)"
    author: "Marc Mutz"
  - subject: "And where..."
    date: 2002-07-28
    body: "the only runner up was Enlightenment which is not a desktop environment. And the only argument for that was that 'Enlightenment has a beautiful interface that is a pleasure to work with'. And to nominate Enlightment as a runner up for a desktop environment must be a bad joke. Enlightenment is obviously a dead project, and as you probably know Rasterman says that the future of computing is not desktop environment but those little appliances that run kastrated applications called 'embedded'."
    author: "antialias"
  - subject: "Re: And where..."
    date: 2002-07-28
    body: "Enlightenment is not a dead project.\nThe upcoming E17 is already great. I agree that there is no date for a stable release, but this project does not make very much noise as others projects."
    author: "JC"
  - subject: "Re: And where..."
    date: 2002-07-28
    body: "What do you mean not a desktop environment. It is certainly not just a window manager. It has a lot of functionality and extras, maybe not as COMPLETE as KDE, but still it certainly has some interesting stuff there."
    author: "A Ivarsson"
  - subject: "Re: And where..."
    date: 2002-07-28
    body: "It has probably something of a desktop, but it is certainly not a desktop environment. The development environments are actually the biggest part of a DE like KDE or GNOME, not the panel and background managment."
    author: "Spark"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "A desktop environment has to have some functionality which Enlightenment doesn't have: panel, icons on the desktop and at least a file manager. Not to talk about a lot of applications (browser, e-mail client, irc client, office suite etc. etc.). It has been announced that Evolution 0.17 would have some of DE functionality, but it is not here yet. And taking in consideration that Rasterman has a very pessimistic opinion about the future of Linux desktops I doubt it that you will see Enlightenment 0.17 in near future. When a main coder of a project doesn't believe in what he is doing the future of that project doesn't look bright."
    author: "antialias"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "Okay, maybe I confused desktop environment with user interface here. Still, i don't understand why every DE would need a panel like in KDE. There could be different approaches."
    author: "A Ivarsson"
  - subject: "Re: And where..."
    date: 2002-07-30
    body: "\nYes, of course, there can be different approaches...\nIn fact, Netscape promised to build Desktop based on Browser. Hey, when it was? I guess ... 7 years ago!\n\nSo far, all major OSes/environments (Windows, MacOS, KDE, GNOME) use Panel.\nAnd (almost) all users are get used to those Panels.\nSo, I think there is nothing wrong to use Panel in mainstream Desktop Environment.\n\nCheers,\n\nVadim Plessky\n"
    author: "Vadim Plessky"
  - subject: "Re: And where..."
    date: 2002-07-30
    body: "Not so fast. MacOS, IIRC, didn't use to have a panel before version 10, yet nobody would say that it wasn't a desktop environment. OSX inherited the panel from NeXT's dock which, however far you stretch it, was not really a panel. And NeXT didn't have icons on the desktop either. Yet, many will agree that NeXT was a desktop environment, much like GNUstep, which doesn't have a panel or icons on the desktop, but still provides the basic infrastructure libraries.\n \nIt doesn't have to look and smell like Windows to be a desktop. The power of KDE is that you don't really need the kicker or the desktop icons to be productive. It's true that the panel idea is very common (you forgot CDE and, by extension, XFCE), but it doesn't mean that one cannot do without it.\n\nOn the other hand, Raster & co keep stressing that E17 will not be a DE, but a desktop shell.\n"
    author: "cosmo"
  - subject: "Ot: Browser based desktop"
    date: 2002-07-30
    body: "> Yes, of course, there can be different approaches...\n>In fact, Netscape promised to build Desktop based on Browser. Hey, when it was? I guess ... 7 years ago!\n\nThey did! Have you looked at EOne(?), I think they do exactly that using mozilla.\n\n\n\n"
    author: "Morty"
  - subject: "Re: Ot: Browser based desktop"
    date: 2002-07-31
    body: "\nI was speaking about Nestcape, now AOL Time Warner.\nEOne is different product/company.\n\n"
    author: "Vadim Plessky"
  - subject: "Re: Ot: Browser based desktop"
    date: 2007-05-05
    body: "A web based desktop can be found at: www.oos.cc"
    author: "jimmy"
  - subject: "Re: And where..."
    date: 2002-07-31
    body: "About the panel. Is not the panel supposed to represent things that we usually do not keep on top of our desk? Like the trashcan. Usually, I put the trashcan on the floor and my \"home\"(my files) on the shelves. The desk(desktop) is only for stuff I am currently working with. I have fanally seen somebody take the trashcan of the desk, Mac OS X. Iguess it is possible in KDE too."
    author: "A Ivarsson"
  - subject: "Re: And where..."
    date: 2002-08-01
    body: "Yeah I had exactly the same thought. The desktop is stupid place to put my trashcan and important stuff on. And it's funny how this analogy fits for computer systems. :) The desktop is great to temporarily place work there but it's awefull for important stuff (because it constantly gets covered by something). "
    author: "Spark"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "Ok, Rasterman can be very pessimistic about the future of Linux desktops, but I am pretty optimistic. :-)\nWell, not only Enlightenment - also IceWM development stagnated (as it was predicted by some people). Hopefully, this doesn't affect KDE at all. Neither IceWM not E are important for KDE success.\nRe: \"Rasterman has a very pessimistic opinion about the future of Linux desktops\" - you are right. I doubt E will have success with such low morale.\n\nRegards,\n\nVadim Plessky"
    author: "Vadim Plessky"
  - subject: "Re: And where..."
    date: 2002-07-31
    body: "> ... at least a file manager.\n\nThe E17 filemanager is, or will be, http://evidence.sourceforge.net/.\n\nKristian\n"
    author: "Kristian K\u00f6hntopp"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "One other note: I should mention while E was the only runner up, we also considered GNOME 2.0 for the award. Unfortunately, GNOME kept crashing on our test system (using GARNOME-based GNOME 2.0), and some things just didn't work at all.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "\nWhat is GARNOME?  I am curious!\n\n"
    author: "Vadim Plessky"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "Hello Vadim,\n\nGARNOME, available at: http://www.gnome.org/~jdub/garnome/ is a method of delivering the GNOME desktop (and other projects) to testers.\n\nAdvantages:\n1) You can build an entire *self contained* GNOME system in a user account.\n2) You don't have to struggle with Makefiles, downloading packages, dependencies etc.  With GARNOME, the appropriate packages are downloaded and built for you\n3) GARNOME includes a host of other GNOME 2 apps a well as the the GNOME Desktop\n4) Did I mention self contained :)  This means it won't mess up your system libs etc etc.  To delete it, all you do is delete the garnome directory and you're done!\n\nIf you'd like more information, I'm sure I can help.  Try it out! 0.12.2 is extremely stable and well worth looking at :)"
    author: "Anon Man"
  - subject: "Re: And where..."
    date: 2002-07-30
    body: "\nHi Anon Man!\n\nThanks for explanation, GARNOME's idea sounds very solid and interesting.\nBut I am somewhat limited on time at a moment (KHTML still has bugs, need to close some :-), so I would prefer to download Mandrake RPMs, as I did before.\n\nCheers,\n\nVadim\n"
    author: "Vadim Plessky"
  - subject: "Re: And where..."
    date: 2002-07-29
    body: "Hi Vadim,\n\ngarnome is a port(age)-like installation system for DE. This means: building your own packages from source. Its supports Gnome2 and KDE3.0.2\nIf you like this kind of software distribution leave your current distro and switch over to Gentoo-Linux (gg:gentoo).\nInstallation of KDE in Gentoo: \"emerge kde\" (... wait half of a day ...) ready!\nIts so nice, and one of the best choices for stable KDE-packages!\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: And where..."
    date: 2002-07-31
    body: "Right, so you used a packaged version of KDE but compiled up your own version of GNOME 2.0. Did you read the release notes? I'm betting not, since only those people who haven't have had serious problems with GNOME 2.0.\n\nThanks for confirming what I already had a pretty good idea of. You are a KDE rag, and wouldn't know the quality of GTK/GNOME from a hole in the wall. Your awards are a sham... and in South Park language, I call shennanigans.\n"
    author: "Compton"
  - subject: "Re: And where..."
    date: 2002-07-31
    body: "Yes, I compiled my own version of GNOME 2, since my distro didn't have packages for it yet. I carefully followed the directions provided by the GARNOME team, installed all the needed dependencies, and then let it happily go to work. The result: An unusable copy of GNOME 2. \n\nAs I said, I could have just ignored GNOME 2, since there wasn't a Ximian package or a normal package available... but that would have only disqualified GNOME. Wasn't it better I spent the time and effort to *try* to consider it?\n\nNow, really, if you want to be fair about all of this, the GNOME users visiting the dot really should quit assuming guilty until proven innocent. I know you'd love to find a way to prove I tried to make GNOME 2 fail, but there simply isn't anyway to do that. I was very excited about GNOME 2. I wanted GNOME 2 to go well. I was even considering switching to GNOME 2 if everything went alright. That, if anything, was biased towards GNOME and not against it. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: And where..."
    date: 2002-08-01
    body: ">As I said, I could have just ignored GNOME 2, \n>since there wasn't a Ximian package or a normal \n>package available... but that would have only \n>disqualified GNOME. Wasn't it better I spent the \n>time and effort to *try* to consider it?\n\nNo, you didn't have to ignore it, but I didn't see you making it clear that you weren't using a packaged version, but rather one you'd thrown together yourself - and as I said, not read the notes.\n\nAs for this \"it wasn't ready\" - fine, if you want to do a proper comparison you wait until it is or ignore it making clear why. What you did was a hatchet job dressed up as fair comment.\n\nAs for slamming Evolution for not integrating with KDE... boo hoo. How about slamming KMail for not integrating with GNOME. Oh yes, that's \"because KDE is the most popular\" - which shows why your awards are a worthless sham and you have little or no journalistic integrity.\n"
    author: "Compton"
  - subject: "Re: And where..."
    date: 2002-08-01
    body: "Okay, first, I didn't comment on GNOME at all except saying that it might have been \"GNOME's year to shine\" had it not been for KDE 3.0. So there was clearly no place to mention the fact that I built my own packages (which usually provides good results with KDE). If you notice the Awards were done as brief mini-reviews, and as such, such details - especially about those that did not win was a bit excessive. I did read the instructions on the GARNOME site (as I mentioned) more info on the GNOME site, and who knows what else, so why I would say I didn't read the notes is beyond me.\n\nRe: waiting, you surely don't expect me to delay awards for a product that isn't ready, do you? C'mon it might not be ready until the end of the year, that is just silly.\n\nFinally, I didn't slam Evolution. I gave it a runners up, which was a pretty good thing considering there are also a number of other very good clients out there. No, Evolution isn't integrated with KDE, and yes I found KDE more intuitive, thus I found Evolution less intuitive then the one that integrates with a system I found more intuitive - and that is unfair how? Simply put KMail benefits from the features that caused us to choose KDE as the best desktop, therefore, KMail did have the upperhand. However, if you had read all of my comments, you would know there were other reasons I didn't pick Evolution (such as the fact that it was unstable and had GPG problems).\n\nI know it is terribly disappointing that GNOME didn't do an awards sweep this year, but as I noted Gaim and OEone - which both depend on GTK - both took top honors in different categories. \n\nSo far, you've slammed my reputation a number times with unsubstantiated evidence. Isn't that what you have accused me of doing with GNOME?"
    author: "Timothy R. Butler"
  - subject: "Re: And where..."
    date: 2002-08-01
    body: ">So there was clearly no place to mention the fact that I built my own packages \n\nOh really... no place to state a basic fact? Well, I suppose that tells us even more about your style.\n\n>(which usually provides good results with KDE)\n\nIt does with GNOME. The idiots who scream ./configure && make && make install is easy, know nothing. You need to know what to do... you already admit that you are familiar with KDE and not too familar with GNOME. Presumably, this is the first time you've bothered to compile it.\n\n>Re: waiting, you surely don't expect me to delay awards for a \n>product that isn't ready, do you? C'mon it might not be ready \n>until the end of the year, that is just silly.\n\nDid I say that? I believe I gave a couple of options. Wait, or don't includ GNOME 2 at all - stating why. You chose to do a hatchet job on a something you didn't understand and didn't even state that you had compiled it yourself. Just out of interest, when things didn't work in your self-compiled version, did you bother to check with some GNOME mailing lists to find out what your problem might be. Or did you forget all about such a basic thing.\n\n>more intuitive - and that is unfair how?\n\nCriticising a product, one written for GNOME, because it doesn't integrate with your own favourite desktop is fair how? You are simply twisting words. All of the things you criticising Evolution for were simply wrong... especially when it is used in a GNOME environment. As for \"unstable\" - this is simply rubbish. For all I know you have compiled the damned things yourself again. If you read my other comment, I've been using it for a very long time and had no trouble. Now obviously that doesn't rule out possible problems, but you do already have a track record for not revealing important details and a definite bias mixed with cluelessness. \n\n>I know it is terribly disappointing that GNOME didn't do \n>an awards sweep this year\n\nAnd this is pathetic... it just reveals your real biases.\n\nYou just don't get it, do you. GNOME 2 came out a month ago. it has had no time to mature. I don't *care* if it sweeps your stupid awards. I am tired of reading biased rubbish from KDE sites that try to portray themselves as neutral. Had you simply stated that you considered GNOME 2 too immature for the moment and left it out I doubt it would have caused any fuss.\n\n>So far, you've slammed my reputation a number \n>times with unsubstantiated evidence. Isn't that \n>what you have accused me of doing with GNOME?\n\nThe only reputation you are getting is for being biased; not revealing important details in your \"awards\"; and being a click-through-whore.\n\nNone of that is unsubstantiated for anyone who has followed your site's antics over the last day or so.\n\n"
    author: "Comption"
  - subject: "Re: And where..."
    date: 2002-08-01
    body: "First, yes, there was no room - BECAUSE I DIDN'T MENTION HOW GNOME FAIRED IN THE AWARDS. You can't mention why every package didn't get the award if you want to focus on those that did. Honestly, I don't remember if it was the first time I compiled the software or not. I suspect it probably was, I usually use the RPMs (when available), but GARNOME was so easy it was a snap... and fast too. I should note I've used/tested GNOME since version 0.12. Either way the product was *obviously* not ready for primetime yet - something most everyone admits - so why are you surprised it didn't win? KDE won, why not leave it at that?\n\nNow, on Evolution I used precompiled packages included with Mandrake 8.1 IIRC. Associate Editor Steven Hatfield I think may have used Red Carpet or used precompiled packages for SuSE 7.2... I'm pretty sure he used Red Carpet. We both had trouble.\n\nAnyway, I think I've shown I'm even willing to discuss things with trolls, so how about being a bit kinder, eh? Each message you accuse me of new and horrible wrongs... I'll be up to using Windows to write the review soon. ;-) And, no, I didn't do that - I used KWord (I vary between KWord and OpenOffice depending on my mood) and WordPerfect 8 for Linux.\n\nAnyway, so far you have yet to explain how I'm biased when I have attacked KDE in numerous places. You've also failed to show how most awards come with disclaimers about why each product considered didn't win. Listen, why get nasty? I'm trying to explain what happened here, and you *want* to think I did something... don't you?\n\nAbout the RedHat story: I didn't expect it to be big... it was, but I didn't expect it to be. It was, most people agreed, and I left a great big link at the bottom of it so everyone could see for themselves what happened. Furthermore, within an hour of RedHat's change of heart I had updated information online and contacted LT to let them know RedHat had resolved the problem. Would someone truly who is truly biased zealot do that?\n\nThe simple fact is, I've taken the middle road most of the time. I prefer KDE for myself, but at the same time I anxiously await every GNOME release. I also prefer Gaim to Kopete, think Mozilla is really nice, and prefer Mandrake's GTK-based config tools to SuSE's Qt-based ones. I think my record - if you really examine it - speaks for itself. I use what works best in my opinion.\n\nEmphasis on opinion - the exact reason why I made sure the RedHat story was a commentary story. It was my opinion RedHat was in the wrong. It is your opinion they are not. We are both biased different ways. Can't we leave it at that and drop the vicious attacks?\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: And where..."
    date: 2004-05-19
    body: "I just read through the board and i am stunned by your ignorance.  First of all, did you ever think about process time when rating.  Enlightenment using gnome's desktop enviroment even outperforms kde.  Using enlightenment, fluxbox, or icewm with the xfce desktop beats kde speedwise hands down.  I wouldn't be surprised if explorer could be faster.  You could probably check considering most of your boxes have windows on them.  Kde is for know-nothing linux users whothink they know what they are doing because they are efficient in windows.  Kde is just an explorer replacement for windows.  If you want user friendliness, why would you use linux in the first place.  As for your credibility, i know nothing about you, but knowing that you primarily use rpms and will try to use them at all cost is a good indicator of what you know.  Why would you use binaries that were compiled to some other computer, when you can compile them on yours, so they are built to your computer.  Using precompiled binaries is about as low as using the precompiled kernel that came with your distro.  Such things like Elinks and Naim most likely are rather to you.  I dont fully support Gnome as it is based on the same theory that kde is, user friendliness, but if i was so inept as to have to have someone combine a window manager, a desktop, a \"panel\", and a bunch of do-it-for-you configuration programs, i would choose gnome any day of the week and i definately would find myself qualified to rate anything in the linux community.  At least with most standalone wms and desktop enviroments you have the ability to tailor it exactly the way you want it.  Using kde i feel contricted, the same feeling as being in windows.  The beauty of linux is that you are free, why give up that freedom?  Although going without your fancy desktop enviroment and file manager is a little tough(though i dont see how), why take the fun away from yourself figuring out how to do it.  It is morons like you that are ruining linux.  If i was a pessimist and thought linux as we know it is dying, i would blame distros like red hat and mandrake, and most importantly, i would blame kde."
    author: "Dude"
  - subject: "Re: And where..."
    date: 2004-05-21
    body: "One word: Paragraphs.\n\nOne short sentence: Paragraphs are good.\n\nOne longer sentence: I would have replied more carefully, but the thought of reading that huge solid block of letters makes my head hurt.\n\nOne inadequate answer: I am a goddamn knowledgeable Linux user. The first kernel I compiled was 1.1.53pl15 in a Yggdrasil fall plug and play linux distro. Now that the geeky bragging is done: I install RPMs every time I can (unless the version I want is not available as RPM).\n\nIf it is not available, I *make* a RPM of it (checkinstall is nice and easy).\n\nWhy? Because the alleged optimization of self-compiling is simply unmeasured so far. Take Gentoo. Take Slackware. Take Fedora. Do something. Measure it.\n\nIf you have a number, good, then come and say how self-compiling is great. If you don't, you are just making noise, telling people they should do things the harder, slower (compiling takes longer) way, based on no data.\n\nYour comments about \"if i was so inept as to have to have someone combine a window manager, a desktop, a \"panel\", and a bunch of do-it-for-you configuration programs\" is really a sign of muddled thinking or ignorance.\n\nHow is kicker combined with kdesktop or kwin? They are separate programs.\n\nSure, they share libraries. They also share libraries with enlightenment, if you care.\n\nNow, they share way *more* libs with each other, that is true.\n\nBut how is that bad? That actually brings resource usage *down*.\n\nUnless all you want is to start a WM and look at it, you need apps. If those apps share libs with each other, there's less code duplication, and for equal functionality, resource usage should be lower.\n\nFor example, open kwin (without kicker, kdesktop, etc), and blackbox.\n\nResource usage for blackbox will be lower.\n\nNow, suppose you want to browser a web page. You can use a mozilla variant, or, konqueror, which are the only adequate browsers (ok, there's opera).\n\nGuess what? kwin+konqueror will use less resources than blackbox+konqueror or blackbox+mozilla.\n\nIf you really want to use your computer instead of fiddle with it or look at it, an integrated desktop and RPMs are the way to go, Dude. Trust me, I used Yggdrasil."
    author: "Roberto Alsina"
  - subject: "Re: And where..."
    date: 2004-05-21
    body: "He, I just replied to a guy flaming someone for a two-year-old thread. I must get out more. I blame flatforty though!"
    author: "Roberto Alsina"
  - subject: "Nice awards..."
    date: 2002-07-28
    body: "Their reasoning for choosing KMail instead of Evolution is... \"interesting\" at best:\n\"KMail received the award because it is very stable an mature. While Evolution is very nice, we've either experience or heard a number of problems with it. Further more, with estimates of KDE usage at over 50% of the market, having an application with a common look, dialogs, and address book is a big plus to lower training costs.\"\n\nThe whole article looks like this too me. \"We think this is cool and we heard that others have problems with something, also most people use this, etc\". \nI hope you don't misunderstand this as the bitching of a sore Gnome supporter, I just don't like when someone makes a big fuss about their ratings when infact they didn't more research than the usual slashdot user.\n\nChoosing Enlightenment because it looks neat and polished is also very interesting for a business related website. Considering that E is probably one of the least userfriendly of all windowmanagers and it's not even a DE...\n\nWhatever. Congratulations though..."
    author: "Spark"
  - subject: "Re: Nice awards..."
    date: 2002-07-28
    body: "I agree with you.  Choosing Enlightenment as the runner-up desktop environment should raise a red flag as to the judgment of the judges.  I also agree that the reasoning for not choosing Evolution was a bit weak.\n\nNontheless, KDE 3.0 is a beautiful desktop environment, and KMail is a great email client.  Both could deservedly be seen to win their awards (reasonable minds can differ :-).  I just think that the awards/recognition would be a bit more satisfying if the judges were more thorough in their thinking.\n\nCheers!"
    author: "aigiskos"
  - subject: "Re: Nice awards..."
    date: 2002-07-28
    body: "Hi, \n  Let me clarify a bit. On Evolution, we did experience problems, as did others we are in communication with. It should have said \"we've experienced <i>and</i> heard...\" Re the other information, having integration with the most popular desktop is indeed a big plus, it's something that Windows users <i>expect</i>.\n\n  As far as Enlightenment, if properly configured (as Mandrake has it), we were able to quickly change basic settings and launch all of our favorite applications. In addition to this, it looks and feels polished. Face it, people coming from a Windows background <i>expect polish</i>. Quite frankly, E worked much better in our tests then GNOME 2. GNOME 2 was extremely unrelable to work with.\n\n  Anyway, I hope this helps a bit."
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-07-28
    body: "No, this didn't help. \nEnlightenment is still a windowmanager (it can even be the windowmanager for GNOME) and beeing more integrated into the most used system is still no reason to award an application. Of course everyone who uses KDE should use KMail for this reason but that doesn't make Evolution (with GNOME integration) worse. You can't just expect that everyone of your readers is running KDE now that you awarded it. Evolution is also known to be very stable and reliable when \"properly configured\" (in recent versions at least).\n\nI whish their will be a time when KDE and GNOME supporters can be really fair and honest and not constantly backstab each other."
    author: "Spark"
  - subject: "Re: Nice awards..."
    date: 2002-07-28
    body: "Enlightenment is much more then a basic Window manager (even if it isn't a full blown desktop environment). But if it was, WM's and DE's both deserve a shot at the award. E is quite good, and suitable for business usage. \n\nNow back to Evolution. The key point is, KMail integrates with the most popular D.E. - the D.E. that is preferred with every distro that is admits it is a desktop distro. It is also the D.E. we found most usable in our awards. KMail has a very similar feature set (if only considering e-mail and not PIM functions) to Evolution. Now, if we add on top of that in our experience and that of our collegues, that Evolution crashes more, has more trouble with GPG, etc., doesn't that make it a good reason to pick KMail?\n\nBack to integration for a moment, consider this: We say \"KDE is really great, you should use KDE.\" Then, we are suppose to say \"you shouldn't use this nice, full featured, stable e-mail client that works with KDE, you should use this less stable one that looks completely different and will raise TCO (due to training costs), 'cuz it has PIM fuctionality.\" Does this make sense? I know that is taking what you are saying to the extreme, but I think it does demonstrate the fact that it is perfectly reasonable to base part of a decision on the fact that one works better with the best D.E. (according to our awards).\n\nIf you are still not convinced I'm not biased, please read my recent critique of KDE at http://www.linuxandmain.com/modules.php?name=News&file=article&sid=131 . I think this demonstrates I can sit on either side of the wall quite well.\n"
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "<puzzled>Shouldn't the training costs be lower since Evolution is *meant* to replace Outlook?\n\nI mean, in my office Outlook is the standard.  When I got started in Linux, for me to move from Outlook to Evolution was a breeze...\n\nIsn't that a valid point too?"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "It would be, but the point is, if you give people different tools that work differently, you raise the training costs. For instance, if you standardize on KDE, you have common save/insert box, standard dialog box, integrated address book, etc. Everything works together - like in Windows.\n\nOn the other hand GTK and thus Evolution has very different, confusing (for a newbie) dialog boxes, a seperate address book, etc. Also without changing settings, the right e-mail client will not launch when you click an e-mail link somewhere in KDE.\n\nThis is all solvable, but the former stuff is kind of bad in large deployments and the latter (changing of e-mail settings) is bad of SMB clients. If you've ever provided technical support, you will know how much the average person is comfortable doing on their own. The more everything is alike, the lower the TCO.\n\nAnd this completely ignores two other concerns. Having both GTK and QT/KDE running at the same time eats more resources, and thus requires more processing power (admittedly, just running GNOME requires less resources). Also, this ignores the aestetic preference for applications to look and act alike.\n\nAnyway, not everyone will agree, but I think you can see that this at least isn't a completely unreasonable point of view.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Oh I see.\n\nWould you be inclined to retry this when GNOME 2.2 (w/ the GTK2 port of Evolution comes out)?  Hopefully you'll be pleasently surprised :)\n\nThx for responding!\n\n[P.S.  See below for comment on GARNOME]"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Most definately. I've tried every major release of GNOME in the hopes that I'd fine it as usable as KDE. GNOME 2.0 feels like it is really headed in the right direction, the default layout iinally feels like it was designed with usability in mind. So, I'll be really looking forward to GNOME 2.2 and the GNOME ports of Evolution, as well as my personal favorite GTK apps - Gaim and GIMP.\n\nIf GNU/Linux had two desktops that were as usable (or more so then) Windows and Mac OS, that couldn't hurt at all!"
    author: "Timothy R. Butler"
  - subject: "GNOME 2.2? [Re: Nice awards...]"
    date: 2002-07-30
    body: "\nAnd when GNOME 2.2 will come out?\nGNOME2 release was delayed by 2 years...\nShould we wait another 2 years for GNOME2.2?\n\nAnd BTW: why we speak about GNOME 2.2, not about GNOME 2.1?\nDo they plan to mimick kernel releases (2.4 after 2.2)?\n"
    author: "Vadim Plessky"
  - subject: "Re: GNOME 2.2? [Re: Nice awards...]"
    date: 2002-07-30
    body: "> And when GNOME 2.2 will come out?\n\n6 months from now\n\n> GNOME2 release was delayed by 2 years...\n\nfalse, and - by the way - not for GNOME's fault\n\n> Should we wait another 2 years for GNOME2.2?\n\nno\n\n> Do they plan to mimick kernel releases (2.4 after 2.2)?\n\nyes"
    author: "A.C."
  - subject: "Re: GNOME 2.2? [Re: Nice awards...]"
    date: 2002-07-30
    body: ">> And when GNOME 2.2 will come out?\n>\n> 6 months from now\n\ngood, thanks for notice!\n\n>> GNOME2 release was delayed by 2 years...\n>\n> false, and - by the way - not for GNOME's fault\n\nHopefully, it's also not my fault. :-)\n\n>> Do they plan to mimick kernel releases (2.4 after 2.2)?\n>\n> yes\n\nThanks for explanation!\n"
    author: "Vadim Plessky"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "<quote>\nOn the other hand GTK and thus Evolution has very different, confusing (for a newbie) dialog boxes, a seperate address book, etc. Also without changing settings, the right e-mail client will not launch when you click an e-mail link somewhere in KDE.\n</quote>\n\nDon't forget about File Open/Save dialogs. Dialogs in GNOME1 and GNOME2 are TERRIBLE! I have many folders (200+ important ones, I guess), with many files in each one (some of them have 1500+ HTML files), and it's really headache to open/browse those folders using GNOME dialogs, or Mozilla, or some similar ugly, ill-fated tool.\nBut I found following elegant solution :-)\n\nI open URL in Konqueror (incl. URLs for local file system), than copy/paste URL to Mozilla or Galeon, that's it!\nDo I need to explain that Mozilla/Galeon /GNOME is some kind of *overhead* in such scenario?\n\nRegards,\n\nVadim Plessky\n"
    author: "Vadim Plessky"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "The file dialogs will be changed in GTK 2.4\n\nTake a look at the following URLs for an idea of what a very basic file selector will look like:\n\nhttp://digilander.libero.it/plfiorini/gnome-file-selector/alpha4_shot.png\nhttp://207.170.48.40/gfs-custom.png\n\nAlso, Mozilla is being ported to GTK2 and there is some talk about changing the file selectors in that.  If you *reall* don't like the look of Mozilla, wait for Galeon 2 to come out.  That will be completely integrated with GNOME 2\n\nHappy to be of assistance.\n"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "I always thought that mozilla uses it's own toolkit."
    author: "a"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "XUL based on GTK"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Is it Gtk+ or just Gdk?"
    author: "ac"
  - subject: "XUL, GTK, Mozilla [Re: Nice awards...]"
    date: 2002-07-30
    body: "\nYep. And all together looks very ugly, and works incredibly slow.\nMy favourite browser based on Mozilla code is KMeleon.\nSee http://kmeleon.sourceforge.net for details.\n\nIt works on Windows, though.. :-)\n\nRegards,\n\nVadim, \nsticked to KDE 3.0.2 and Linux-Mandrake\n"
    author: "Vadim Plessky"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "So, they're making it look more like the KDE dialog? \nGood - eventually they'll copy *all* of KDE's usability improvements, and I'll actually be able to try GNOME for more than half an hour without giving up in frustration at the bad design mistakes."
    author: "Jon"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "\nOk, thanks for screenshots.\nThey are indeed better than GNOME1 File Open/Save dialogs, but too much rudimentary, IMO.\nKDE3's dialogs are much more advanced.\n\nAs about Mozilla and Galeon - I don't like *both* of them.\nI use Konqueror, as it's superior to Mozilla and MS IE.\nWhen you have *THE BEST* technology at hand, available for free, there is no reason to use *second best*.\nI would appreciate if Mozilla will be re-written to GTK2. It takes Mozilla 1.0 around 20-30sec. to start here. Galeon much faster, but still uses too much memory (up to 40MB), and slow-downs my computer a lot...\nSo, Mozilla itself should be fixed first. AFAIK Chris Blizzard is working on it (at least, he promised me *to make Mozilla faster* some time ago)\nBut so far, I don't see results...\n\nCheers,\n\nVadim Plessky\n"
    author: "Vadim Plessky"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Those screenshots are _not_ how the actual fileselector will look like, they are just early concepts of volunteers. There is a lot of talk about it and many ideas are shared. You can be sure though, that the result won't be packed with features and stuff. While this looks cool, sometimes the \"rudimentary\" stuff is much faster to use. I don't think that the fileselector should be more complicated than the actual filemanager. \n\nAs for browsers and such, it's fine if Konqueror suits you best.\nPersonally I prefer Galeon because of it's browser-centric interface and the excellent Gecko engine. RAM usage is indeed quite high so this might be a deciding point for people low on RAM or just picky about it. I don't think there is a point in arguing about which one is the best but both can certainly do the job."
    author: "Spark"
  - subject: "Konqueror and Mozilla [Re: Nice awards...]"
    date: 2002-07-31
    body: "\nGaleon indeed looks like very interesting. I lik eit simple interface and do not miss Mozilla's XUL...\nSome major concerns for me about Mozilla:\n* printing (which is unfortunately sucks)\nNote that I need to print *in Cyrillic*, and Mozilla/OpenOffice/Gnome are far behind KDE in this respect\n* missing AA, font installation mechanism is obviously missing as well\n* slow startup/exit speed (but I already mentioned this)\n\nConformance to W3C standards is quite good in recent Mozilla builds, so there is no point to complain here.\n\nAs about Gecko vs. Konqueror - I believe we should compare Gecko with KHTML.\nKHTML itself is very lightweight, and I would love if someone with make XUL browser based on KHTML, or GTK2-based browser based on KHTML (like Galeon is based on Gecko)\n\n"
    author: "Vadim Plessky"
  - subject: "Re: Konqueror and Mozilla [Re: Nice awards...]"
    date: 2002-08-01
    body: "Hmm printing in cyrillic should get better with Pango and such but I don't know, I don't own a printer.\nAA will be there soon when the Gtk2 port and Galeon are ready.\nSlow startup speed isn't really a concern anymore, Galeon starts pretty fast and when it's loaded once, subsequent loads are _extremely_ fast. \nExit time is definetly fast. =) \n\nAs for Gtk2 based KHTML browser, this would be a major hassle, most probably much more difficult than a Gecko based Gtk browser and it would feel less integrated. So there is no real point not just a Qt based KHTML browser (like Konqueror or Konq-embedded).\n\nAlso buying new 64MB of RAM would be much cheaper than doing all the work just to get a browser that is a little bit lower on RAM usage. =)"
    author: "Spark"
  - subject: "Re: Konqueror and Mozilla [Re: Nice awards...]"
    date: 2002-08-01
    body: "Hi Vadim - about AA.  New GTK2 versions of Galeon (yes they exist :) are very speedy.\n\nOn top of that AA is *well* supported.\n\nJust an FYI"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "More trouble with GPG?\nHow many people did you ask? - i followed the instructions that came with evolution and set up GPG (having never used it before) in less than five minutes - and it signs/encrypts emails fine.  I tried to do the same with Kmail and it failed miserably - I gave up after a few hours."
    author: "me"
  - subject: "Re: Nice awards..."
    date: 2002-08-02
    body: "how did you manage to do this ??? GPG support in KMail can be set up by a trained monkey in a few minutes (even less)..."
    author: "loopkin"
  - subject: "Re: Nice awards..."
    date: 2002-08-03
    body: "Give the guy a break, he took time out to write his opinion. I personaly may disagree with some of the things he wrote, but there comes a time when people sit on the side line and flame a little too much when someone expresses their opinion or creates a peice of software for GNU (what happens soon as they intoduce the product they have taken \"THEIR\" \"PERSONAL TIME\" to produce they get flames instead of thanks. ie to much negative & not enough positive."
    author: "Jim"
  - subject: "Re: Nice awards..."
    date: 2002-07-28
    body: "Any chance of expanding a bit on the problems you found with evolution? I haven't found any problems myself since 1.0ish and haven't really heard of problems with it from others - so it would be interesting to hear what the problems were (installation maybe?).  Despite that, i know of many  people who don't use evolution, but either because they don't like the idea of an Outlook clone, or because they think the extra functionality must make it slower. So well done to kmail, but a thumbs down to the dodgy reasoning.\nHow good GNOME 2 worked for you is likely to vary depending on which distro you install it ontop of at the moment (because none of them have itegrated it into a release yet - normally I'd recommend waiting for your distro of choice to include it, or for ximian to package a 2.0 release for your distro) - This seems also to be the case with KDE, i've never had a good experience of KDE on Redhat (Despite the rest of redhat being excellent), i have however KDE running well on other distro's like Suse but found the distro lacking in certain areas (and the GNOME and enlightenment packages to be far less reliable).  So when shortlisting for awards or writing reviews i'd suggest that you make a quick note of which distribution (or preferably distributions) that you tested on."
    author: "Dave"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "I've been using Evolution since 1.0.1 (I'm now using 1.0.8). I started using it because I had used KMail at the time and didn't think it was as mature as Evolution. I've just recently looked at KMail included in KDE3.0.1 and have completely taken that thought back, I'll give you my specific reasons for this.\n\n1. Fonts- the fonts and sizes used in Evolution are controlled by gnome/gtk. Since I can't for the life of me get gtk1.x to display AA and TTF fonts as well as they look in KDE this is a major problem. It also bothers me that i can't easily select what fonts are used in what parts of Evolution(folder list, headers, message body, etc). This is something I am able to do in KMail. I can tell it what font and size for what area, and like my other KDE applications they look good. I also noticed after updating one of the gtk html libraries I can no longer CTRL-+ and CTRL-- to change font sizes within an email. This is annoying....\n\n2. Crashing/Xinerama- I lumped these two together, but basically it is this. I've had Evolution crash on me a lot. For sometimes what appears to be random reasons. I'll try to open up an email and it crashes, reload Evolution open the same mail with no problems. I don't know if this happens in KMail, but I have two friends that use it that I have talked to and neither said they have had this problem. Xinerama support is also very poor. This seems to be something common with gtk1.x apps. Evolution at first, when launched with Xinerama in use, would barely load, would sometimes not load the folder list, would always crash when showing email with html formatting and/or attachments, and similar with my contacts list. Then I updated some gtk libraries to see if they fixed it, and I now have a 50/50 shot of it working when doing any of these things. This is a real annoyance, something with Evolution specificly (since Gaim and Mozilla seem fine) and with X applications in general(xinerama support is something that linux needs to improve on to reach the level of usability of ms-Windows). With KMail, I haven't noticed these problems(though I've had limited use of it in this environment so far).\n\n3. Contacts list- I love the contacts list, when I used to use windows I used Outlook for contacts, and I love that Evolution uses the same model. I like the method of display and the \"cards\" method of editing the contacts. But here Evolution falls far short of Outlook. The display of the contacts requires the width to be set so that you can fully read some of the entires you desire. I have yet to find a way to get this width setting to stick to what i change it to. So everytime I go to my contacts list, I have to change it. VERY ANNOYING, and something you'd think would have been implemented before 1.x. Another annoying thing, is that I like to have subfolders under contacts to seperate business, friends, family, etc. But if I do this, I cannot use autocomplete for email address' that are not in the main contacts folder when writing an email. Another simple thing that should have been implemented a long time ago. Now, here KMail differs. I really don't like the address book layout used by KDE, but this is a minor inconvenience(or change) that I'll have to deal with to solve my other problems).\n\n4. mail queue/mail send- I like to go through my email, write responses and queue my email then send it all at once, this helps since as I write something I can think of something I forgot in an other email, go back and edit it, etc. Its something I've done since using Eudora as my email client back in windows(I'd kill for a Eudora version for qt/KDE). Evolution makes this difficult as I have to go to \"offline\" mode write my emails then go back to online mode to send them. Eudora and KMail both support queue message as the default for composing email, and the option to not send email when checking email. This lets me take 2 hours to write my email messages and still be able to check my email without sending any of my old email. Its something simple, and for finicky people like me, but its an option Eudora and KMail have(and I beleive some other clients as well) that I find usefull.\n\n5. CTRL-Enter sends email messages. I HATE THIS. absolutely hate it. I have a tendency to sometimes press CTRL-Enter accidentaly when typing(prbly because I'm used to using this feature in GAIM as the send button sequence, so I sometimes press it at the end of a sentence instead of just pressing enter alone). KMail defaults to this, but with the handy ability to configure the shortcut keys I can turn it off. Anyway, naturally this causes Evolution to send the message, which combined with the above problem makes for double emails to a lot of people. This leads me to my next complaing...\n\n6. Poor configuration/options- Evolution has a poor set of configuration options for features. I've touched on this a couple times before, but I think it needs its own list number. There seems to be in the gtk thought of programing the thought that users want less options. I hate this. Its one of the main reasons I like KDE more than Gnome. Gnome/Gnome Apps have 10% of the configuration options that KDE/KDE apps have. I think the only GTK app I've used that does not follow this thought is GAIM, as it has quite a good set of options, I always seem to find an option for what I'm looking for in GAIM, definitly not true in Evolution, and not true in the other GTK apps I can think of. In going through KMail I noticed this like other KDE apps, seems to have more options than i sometimes know what to do with. But at the same time, they are laid out such that what I'm looking for is easy to find and not complicated in wording.\n\nThis is pretty much my list of complaints as I can remember them right now. One thing I do think KMail needs to change(or I just don't see the option for it) is in-document spell checking. I like this feature, it lets me quickly spell check a document as I often mistype words(which is probably evident in this post, so please don't flame me for it =). I like that Evolution does this, though I wish Evolution would display a list of the closest matches when i right click instead of forcing me to load the full spell check window instead...\n\nI'll try and expand on any of these if you guys like, and if you know how to fix any of this in Evolution, PLEASE PLEASE tell me. I hate to have to switch with such poor thoughts of Evolution. Anyway, I'm going to start converting my email from Evolution to KMail in the next couple days, and unfortunately have yet to find a program that will do it for me so I have to do it by hand =("
    author: "IndieRockSteve"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Well...you *could* you the gdkxft applet - but its dodgy at best.  GTK 1.x apps do not have support for AA.\n\nThis changes with GTK 2 though - take a look at a GNOME 2 desktop - beautiful :)  Also, Evolution will be ported to GTK 2 sometime later this year.  \n\nI believe multihead support is going to be added (completely - as in during the design stage) to G2D.\n\nHave you submitted the keybings issues, the contact list problems etc. to the evolution bugzilla? (you should try file a usability issue) They *will* look through it and being as its flagged usability - they'll consider it very seriouly.\n\nRe: Options.  Actually, I think you'll find that its only power users who take advanatge of those myriads of options ;)  But - if you feel an option is *really* warranted and it does have a place - file a bug, make it known..."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "I have looked at GTK2/Gnome2 and have some of the libraries installed. Though right now I only have 1 program that is ported(Pan) that i use(i do use the gtk2 version of it). I'm waiting for the Gaim rewrite that I've been *hearing* about(they are rewriting it as shellable interface with a gtk2 shell as default but it will be open for a qt shell too, which should be amazingly cool). I will look at Evolution again when it is ported to GTK2.\n\nYes, I have submitted bug/usability reports to them. I did it around 1.0.3, still haven't seen anything =(\n\nIts not that I will never look at Evolution again in the future, its just that right now KMail does everything I need an email client to do.\n\nI just wish they could share mail directories so I could switch back and forth easily =)"
    author: "IndieRockSteve"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "\nRe: \"I just wish they could share mail directories so I could switch back and forth easily =)\"\n\nMan, this is easy!\nSetup local IMAP server (I use IMAP-2001, which comes with my Linux Mandrake 8.2), and store all mail on local IMAP server.\nThan you can use KMail and Evolution simultaneously!\n\n"
    author: "Vadim Plessky"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Actually, the poster makes a very valid point.  \n\nAs these are both Free Software projects, the developers should come together and decide to use a common mail directory/format.  This would make it simple for endusers to switch without having to setup servers.\n\n<shrugs>One of those little things which makes life easier"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Yea, it would be nice if all email programs could use the same Mail folder layout. While they all use mbox in some form, they handle directories and indexing differently. It would be nice if they could come up with a common format for doing this."
    author: "IndieRockSteve"
  - subject: "Spellchecking in KMail"
    date: 2002-07-29
    body: "Hi IndieRockSteve!\n\nYou wrote that KMail needs in-document spell checking.\n\nAre you talking about automatic spellchecking with underlining misspelled words? Then you are correct. KMail currently doesn't support this.\n\nOr are you talking about manual spellchecking? If this is the case, then you might want to try Edit->Spellchecking...\nI added a button for this to the toolbar of my composer for quick access. The next version of KMail (in KDE 3.1) will even ignore quoted text so that you don't have to manually ignore all the spelling errors of your correspondents. ;-)\n\nOr do you probably want a \"check spelling of this word/selected text\" action? I can't remember a corresponding wish in our bug report system. So you might want to submit one.\n\nIngo\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Spellchecking in KMail"
    date: 2002-08-01
    body: "I mean automatic spellchecking with underlined words.\n\nI also wish i could print my address book info.... but thats KAddressbook. which needs LOTS of work and is a whole 'nother rant. Anyone know of a addressbook program for KDE3 that supports vcards, has importing and exporting and can display the information in a method similar to evolution and outlook that doesn't require some odd MySQL or other database system to run along side of it?\n"
    author: "IndieRockSteve"
  - subject: "Re: Spellchecking in KMail"
    date: 2002-08-01
    body: "KAddressbook is what you are searching for. Unfortunately it's the CVS-version. So you have to wait for KDE 3.1 (or at least the beta)"
    author: "Guenter Schwann"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "As far as fonts are concerned, on kmail with KDE3.0 I could not find any way to change the fonts of HTML mails. Normal mails look great, with large readable fonts I can adjust, but HTML mails have fonts so small I can barely read them. \nIn konqueror for kde3.0, I found that I could not get it to display AA fonts on some websites (notably www.kde.org) while on others it displayed superb fonts, (such as the dot). I did not notice this in previous versions of konqueror.\nThese are my only complaints about KDE3.0, apart from the memory and speed which is slow on mandrake 8.1."
    author: "rithvik"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "All software was tested at least with Mandrake 8.2, save OEone which comes with its own light version of RedHat 7.1 (and of course SuSE, which was tested on itself ;-)). \n\nRE: problems with Evolution. This awards article was a several month endevor of testing software, so I can't quite remember, but I believe Evolution was crashing while viewing the inbox. I also believe I had some trouble with the setup wizard. Finally, while I didn't test that specific function, my esteemed OfB colleague Steven Hatfield was unable to get GPG working on it (and he knows GPG very well). Whenever he would send a GPG message using Evolution, it would only include a partial signiture. He tested on SuSE 7.2, IIRC.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "     True...GNOME 2 at this point is rough around the edges.  I would expect it to be.  The developers themselves have stated that it is a 'development version'.  People wanted to see that GNOME 2 was indeed real and didn't want to wait forever.    Gnome 2.2 will have all the major apps ported over, the rest of the user visible and usability changes pushed in and much better integration :)\n\n     What is interesting though is that you stated that GNOME 2 was unstable.  I find that...interesting.  For me, it is most certainly *very* solid.  Were you relying on distro supplied packages?  Those are a bit iffy at the moment.\n\nI would refer you to GARNOME:\n\nhttp://www.gnome.org/~jdub/garnome/\n\nIt allows you to build a GNOME 2 system in a completely self contained location.  By this I mean that you can create an account (testuser for example) and build your G2D in there.  When you're done, delete your GARNOME directory and your system is the same as it was...\n\nI've been using GARNOME for months now and to be quite honest have never experienced instability like you've stated."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "I wanted to try GNOME 2.0 as soon as it came out, and MDK didn't have packages for 8.2 at the time (I don't know if they do now), so I used GARNOME. The dl/install system that is GARNOME is absolutely beautiful. I love it. However the system I ended up with crashed on almost every control panel option, and I believe I even had nautilus crash as well. IIRC, even the bug reporter crashed on me! :-\\\n\n  Disappointing to say the least, but I'm looking forward to grabbing the next Mandrake 9.0 beta, which ought to have a good version of GNOME 2 in it.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Oh.  I'm sorry it happened :(\n\nGARNOME 0.12.2 is what I'm running - its *very* stable.\n\nThe Mandrake 9.0 Beta has a good version of GNOME 2.  But, I (personally - opinion only) believe that Red Hat has the better default setup of GNOME 2.  The limbo isos were removed from the servers (they're probably going to release another one very soon).  The GNOME 2 integration in RedHat is...wow!\n\n<chuckles>It has been nice talking to you.  Although I may not fully agree with your choices, you have responded very reasonably - I appreciate that.  I look forward to seeing your review of GNOME 2.2.  I agree, having to powerful desktops on Linux would be very nice :)\n\nThx for posting"
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "AVOID Red Hat at all costs.  They are trying to destroy KDE.  Don't support them."
    author: "ac"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "<sighs>Please."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "If you support the KDE Project, stay away from Red Hat until they wise up."
    author: "ac"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Keep on dreaming..."
    author: "Stof"
  - subject: "And if so.. who cares?"
    date: 2002-07-29
    body: "For RH it is extremely difficult to change it's mind after that (long ago) decision not to use KDE but Gnome. Money may be a reason (call it return on investment or whatever). Leave it to RH... as this is a totally valid point not to support KDE or even to try to use your own market strength (distro) to demolish KDE with knowingly buggy KDE releases distributed with RH.."
    author: "Thomas"
  - subject: "Re: And if so.. who cares?"
    date: 2002-07-29
    body: "Then it is totally valid for KDE to destroy RH by boycotting it."
    author: "ac"
  - subject: "Re: And if so.. who cares?"
    date: 2002-07-29
    body: "RedHat DOES ship KDE (and even allows you to choose KDE as default desktop during install!). That argument alone outweights everything you have said because you're just an anonymous coward."
    author: "Stof"
  - subject: "Re: And if so.. who cares?"
    date: 2002-07-30
    body: "Redhat does not support KDE at all and has contributed nothing directly to KDE. Still, if they want to turn their backs on the most popular desktop and a thriving community, who am I to disagree?"
    author: "ac"
  - subject: "Re: And if so.. who cares?"
    date: 2002-07-30
    body: "Who are you?  You're their bread and butter.  You have the power, bro."
    author: "ac"
  - subject: "Re: And if so.. who cares?"
    date: 2002-07-31
    body: "Why *should* RedHat contribute? RedHat *distributes* KDE and that's all that matters. They provide KDE's source code on the source CD. They are well within their rights, because they comply to the GPL and LGPL."
    author: "Stof"
  - subject: "Re: And if so.. who cares?"
    date: 2002-07-31
    body: ">RedHat *distributes* KDE and that's all that matters.\n\nNo it isn't, because Redhat KDE packages are a pile of crap and never updated, even for security issues. This harms Redhat KDE users and reflects badly on the project as a whole. Obviously *you* don't care about that, but a lot of other people do.\n\nThe least Redhat could do is *contribute* decent packages and keep them up to date. Or not supply packages at all. Either would be better than the current situation."
    author: "ac"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "\"Destroy KDE?\"\n\nCalm down everyone.  You might as well state that Suse and Mandrake are trying to \"destroy GNOME\" by shipping KDE as their default.\n\nThere is NO, NONE, NO TRUTH in *either* those claims."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "while i'm not so sure that RH's intent is to destroy KDE, i can say that RH's treatment of KDE is very different than SuSe's or Mandrake's treatment of GNOME. both SuSe and MDK manage to put out quality releases of GNOME and keep up with the GNOME releases. RH manages to do neither of those things. and this isn't because they are inept at providing decent and updated packages for things they care about, they simply don't care about KDE.\n\nat least SuSe and MDK show that they care about GNOME in that they give it fair and decent treatment. why? probably because they realize that many of their users appreciate decent GNOME packages and they try and give their customers what they want.\n\nRH on the other hand, doesn't seem to count desktop users as people who matter and/or are more content to stay the corporate party line than support that which their customers use and want.\n\nfeel free to interpret how that reflects on the GNOME and KDE projects as a whole. (and it does, believe me. i know from listening to users of both desktops talk about it.)"
    author: "Aaron J. Seigo"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Oh?\n\n* GNOME was founded to defeat KDE from the earliest Qt licensing complaints.\n\n* RH climbed aboard with GNOME early, and funds a good deal of GNOME development.\n\n* RH pays not one developer to even package KDE.\n\nCare to dismiss this?"
    author: "Neil Stevens"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "> GNOME was founded to defeat KDE from the earliest Qt licensing complaints.\n\ntrue, but no longer an issue since Qt is available through the GPL.\n\n> RH climbed aboard with GNOME early, and funds a good deal of GNOME development.\n\nYup, RedHat has always historically preferred Free software to non, so they put their weight behind GNOME.  Nowadays, who knows what they would have picked?  Since they went with GNOME, RH's defaults have been GNOME from then on for historical reasons; they wanted the interface to be as familiar as possible to the existing userbase.  This is (I believe) why RH's default gnome 1.2 and 1.4 desktops don't look like Ximian's, they were mimicing the look and feel that they had in earlier releases.\n\n> RH pays not one developer to even package KDE.\n\nNot true, Bernhard Rosenkraenzer (bero@redhat.com) maintains the RedHat KDE packages, and is under RedHat's employ."
    author: "Ranger Rick"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Except that Bero has repeatedly told us on the mailing lists that he does those packages on his own time, most recently telling us that he can't even use Red Hat boxes to compile upgrade packages."
    author: "Neil Stevens"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Yo\n\nCan we have some info on how many developers Suse has to work/package GNOME?"
    author: "Bah me"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "I guess one (recently read one was hired/switched to) and it's perhaps no full-time job."
    author: "Anonymous"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "They must have someone to package. Anyway, didn't they employ GNOME developers before, like Martin Baulig?"
    author: "ac"
  - subject: "Re: Nice awards..."
    date: 2002-07-31
    body: "last i checked SuSe had a couple (one works on German translation, another is a hacker, Martin Baulig i believe) and Mandrake has one or two as well. GNOME may not be their primary focus, but they ensure they have the man power and commitment not to make it a joke. this is good for the entire community since having a healthy GNOME around means competition, inspiration, choice and if needed a fallback position."
    author: "Aaron J. Seigo"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "there are lessons to be learned here, Mr. Anon Man (if that really is your name ;) :\n\n1) just because it works for you doesn't mean it isn't broken. when it works for everyone under sane conditions, *then* it isn't broken. \"it works for me\" doesn't hold water and isn't worth the web page it's written on. it's about three shades short of denial, really.\n\n2) people want things that work, not things that will work. in fact, they get tired and annoyed of things that will work someday but never quite seem to get there. cf the enlightenment project.\n\n3) nothing stands still. everything improves. and not just the system(s) you like to use: all systems with active development improve. they just do so at different paces. so choose your pony carefully based on pace of achievement, not based on promises.\n\n4) bitching about who gets an award is about as useful as trying to hold back the tide: you can't, and it doesn't really care."
    author: "Aaron J. Seigo"
  - subject: "Re: Nice awards..."
    date: 2002-07-29
    body: "Mr. Aaron:\n\nPlease take a look at my comments above:\n\n1)You base your comment that it is broken on one persons comment.  That too, is a very short sighted view.  There are many users who do use GARNOME with success.  Unless one has tried it themself they cannot make a valid comment.  I have tried it and it works flawlessly.\n\n2)This is the reason why GNOME released 2.0 now before all apps were ported.  There was a sense that comments would be made in the community that GNOME 2 would take too long.  Now, the developers have an idea of what to work with and a system to integrate against.\n\n3)Of course everything improves.  I pointed out that GNOME 2 has improved tremendously as well.  These are not 'promises'.  If you take a look at the GNOME 2 versions of applications you will be pleasently surprised.  They are very featureful, integrate well and are well designed in terms of usability.  As more apps get ported and the developer support for GTK2 solidifies, this trend will increase.\n\n4)If you see any evidence of 'bitching' I would be very confused.  If you notice, I conceded the point to Tim, was extremely polite and asked him if he'd test the new version of Evolution.  This is not bitching - it was a simple question."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "... continuing the tradition of numbered lists:\n\n1) of course if only one person can't get it to work, then the problem probably lies in not satisfying the \"under sane conditions\" requiremeent. but you simply countered with \"it works for me\", as do many others. in fact, it can work for a whole bunch of people and still have bugs that become show stoppers. take a look at the IMAP issues in KMail in the 3.0 release for an example of this.  this is something of a basic fact of software development, and simply accusing those who can't get it to work for them of incompetance, not trying hard enough, or whatever else is lame and unhelpful to your project. your reply to point #1 merely demonstrates your general lack of understanding of this issue. \n\n2) i'm quite happy that the GNOME team got a release out. i was begining to worry about it quite deeply as i believe that the friendly competition between GNOME and KDE is extremeley healthy for both. however, my point remains: if garnome or whatever other distribution mechanism of GNOME does not work reliably for the overwhelming majority of people who try it, it will be dismissed. on a related note, it seems that the longer people wait the more picky they become (i call it just being pissy ;-). this is probably why, at least in part, even though garnome works for you and your friends, it didn't make a showing in these awards.\n\n3) \"these are not promises\" ... \"as more apps get ported\" ... \"this trend will increase\" ... hrm, perhaps you and i have different meanings of the word \"promise\". the point is that people can only use what is here now. and while project X is working to get there, project Y is already moving on. KDE faces this same issue w/respect to projects like MS Windows and Mac OSX.\n\n4) there has been a lot of \"bitching\" here and elsewhere, as in \"why wasn't my pet piece of software XYZ version 3.2 picked instead?!\". i don't know if you are responsible for any of it, but the lesson is still there to be learned. complaining about who gets an award is rather futile."
    author: "Aaron J. Seigo"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Aaron,\n\nYou seem to be very defensive no?  Since when did I \"accuse\" anyone of incompetance?  Perhaps a quick word with Mr. Butler (to whom my points were raised) would enlighten you?  Did he feel pressured by my questions?  Did he feel that I was antagonistic?  I suspect you will find the answer is in the negative.  I think you will also find that in this conversation, it is you who are increasingly starting to use an antagonistic tone.\n\nWhere do you get this \"overwhelming majority\" 'fact'?  I would like to see your source of data for this.  My stating \"it works for me\" for not to dismiss his point of view - but to point out that others 'do' have it working.  Otherwise 'facts' such as the 'garnome does not work reliably... for an overwhelming majority of users' becomes accepted without question by others reading this conversation.  At least I am *only* speaking for myself.  I am also not in the habit of hanging out with my 'friends' and trying out GARNOME until it works.\n\nActually, kudos to Mr. Butler, who maintained an open mind and showed his willingness to experiment again.  Again, although I may disagree with him, I respect his opinion and his nature.  I will look forward to reading his column in the future.\n\nActually, perhaps you should take a look at the GNOME cvs from time to time.  I think you will find it illuminating.  I do not say 'this app *will* be ported'  I state 'this app is in *the process* of being ported'.  Again, you speak of promises...I speak of events already in place.  Again, you and I may disagree on this - but, I fear that this will be something that cannot be resolved.\n\nFinally, I think you and I have a disconnect.  You believe I should simply accept the opinion of others.  I...do not.  I asked Mr. Butler two questions as to why he considered KMail the superior client.  He answered both.  I conceded.  I then asked him if he'd care to try again.  He replied in the affirmative and we parted on amicable terms.  If this is your definition of bitching...<shrugs>I honestly cannot do anything about it.  It would be futlie to try and make you understand my mindset.\n\nI am not in the habit of randomly complaining about software choices.  Suffice it to say, I do have better things to do with my time.  This time was an exception, because I was not sure as to the validity of the choice."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "> You seem to be very defensive no?\n\nif by defensive you mean i don't agree with what you are saying, then yes.\nbut i do think you are applying a certain tone to my postings that isn't meant to be there, and you're taking a lot of it very personally when, as i stated in my original post in this thread, these are general lessons/points to learn from. all of us, not just you personally. my second posting was in reply to you, and it was more pointed because you utterly failed to grasp certain points by simply repeating yourself.\n\n> Since when did I \"accuse\" anyone of incompetance?\n\nit is a common saw, and one that was used elsewhere in response to these awards. if that one doesn't particularly apply to you, try the rest of the sentence: \", not trying hard enough, or whatever else\". \n\n> Where do you get this \"overwhelming majority\" 'fact'?\n\n*sigh* that wasn't what i said. try reading the sentence again.\n\ni said that if it doesn't work for the vast majority of people, it will be dismissed. i didn't say \"it doesn't work for the majority of people\", rather i was saying that just because it works for some or even most, it doesn't mean that people won't perceive a certain piece of software to be broken. in fact, the perception can be dead-on accurate even if it truly does work for most people. responding to \"it's fragile/broken/doesn't work for me\" with \"but it works for some/most people\" is fundamentally flawed. you couldn't understand why Timothy felt it broken enough not to rate 2nd place, i'm helping you understand the reasons why such a thing can occur. you don't have to accept it, but then again the future success of your software may depend upon it. you choose.\n\n> You believe I should simply accept the opinion of others.\n\nnot at all. you should challenge what you disagree with. likewise, i don't have to simply accept your opinions either, and i think i've shown good reasons not to.\n\n> Again, you speak of promises...I speak of events already in place.\n\nin various replies you've talked about apps that will be ported, trends that will increase, file dialogs that will be made better. these are future efforts. but let's pretend all of this was finished in CVS right now. reality for software means that an end user can reach out and touch it. anything else doesn't matter. being on schedule for release in X months doesn't make something real. when it is released, it is real. real for whom? the users. because that's who matters. fact is, these things you talk about aren't done yet, so yes, right now they are promises: as in a commitment to accomplish something in the future tense.\n\n>  If this is your definition of bitching...<shrugs>I honestly cannot do\n> anything about it.\n\nhm. did you read my reply, or just every few words of it? please, re-read my point #4 above and try and understand what i was actually saying, to wit: a lot of people have moaned over the fact that their pet software wasn't chosen for an award. that complaining is pretty much useless. had it been one or two people who made the same rather hollow complaints (you yourself said you asked and were answered satisfactorily to the point of conceding) that would've been one thing, but everywhere this awards thing has been posted there have been a gaggle of GNOME users spouting the same lines. how about giving some props to your GAIM buddies instead? think about how much more positively that energy would've been spent?\n\nif you want to continue this discussion, send me an email. you've got my address."
    author: "Aaron J. Seigo"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Aaron, your comments are reasonable, but I think you mistook some of my statements.\n\nEarlier I stated \"It worked for me\" and you said that I simply 'countered' with that argument.  Actually, if you take my sentence in context I stated that one cannot make an assumption as to the stability of the software unless one tried it out one's self.\n\nPlease keep in mind that moaning goes both ways.  If a KDE program was knocked, especially if KDE users thought that it was the better program - we'd see trouble too.  I think people (including me) were a bit confused.  I took the step of asking why the decision was made.\n\nI guess if we want to see all the user visible changes, we'll have to wait for GNOME 2.2.  That's a \"promise\", nothing's in CVS yet :)  However, keep in mind that GNOME 2.0 was widely stated as a developer's release.\n\nThis just general, but if you're not referring to me, please don't use \"you\", try \"one\" or some other general qualifier instead.  I misunderstood your statements to refer solely to me and this led to my reply.\n\nI will look you up Aaron."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "> but I think you mistook some of my statements.\n\nfair enough. so we've established that neither of us are mind readers ;-)\n\n> if you take my sentence in context I stated that one cannot make an\n> assumption as to the stability of the software unless one tried it \n> out one's self.\n\none can see how others have faired and play by the odds, though. ;-) you are correct in saying that you won't know how well it will perform for you on your machine with your set up unless you try it. the trick is to get people to try it. having people report less than optimal experiences isn't how to do that.\n\nthe GNOME project has some serious PR issues to take care of in the wake of GNOME2: it was later and less than what many of the users wanted and that has resulted in less hoopla than it probably could've used. i hope that the GNOME devels listen well to Havoc when he writes about releasing often and on time. this is probably the only way to get the codebase in order and keep users happy. this strategy has worked quite well for KDE and its users and developers, and it isn't like every KDE release has been exactly perfect.\n\nit simply means that there are less changes (and therefore less things to go wrong) in each release, the users' attention is kept, progress is easy to see for both users and developers (moral is huge for open projects), and problems can (and do) get fixed quickly and pushed out to the users.\n\n> If a KDE program was knocked, especially if KDE users thought \n> that it was the better program\n\n/me looks around for people defending kopete.\n\n> I will look you up Aaron.\n\ni look forward to it. take care..."
    author: "Aaron J. Seigo"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Listen to Havoc and turn your favorite project into a political nonsense game.  Hold meaningless weekly \"meetings\" and vote on your favorite foundation \"issues\" while just about nothing gets done. Make yourself the envy of the bestest commercial company. \n\nHurray."
    author: "ac"
  - subject: "Woah!"
    date: 2002-07-31
    body: "Have you read this:\n\nhttp://www106.pair.com/rhp/policy.html\n\nTalk about bureaucracy."
    author: "ac"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Anon Man:\n\n1) Face up to the facts. GNOME 2 was unstable. So what that you installed it and got it running well, if lots of other people found it to be broken. Your machine is not the only one. The hard data is the people you see complaining... and there are quite a few. Read mailing lists, read articles like this one. Or you can stick your head in the sand and not hear any of it :-)\n\n2) The GNOME 2 release was rushed by a long way... hardly any apps ported. Imagine if KDE 3 was released with just konqueror. LOL! Naturally the reason for the hurry has to be 'corporate pressures'.\n\n3) GNOME has a history of hyping up its future technologies and failing to deliver. Witness Bonobo... even miguel says it is crap now. Just because something is 'in the works' doesn't mean it will be any good. And lets face it, with the problems in porting to GNOME 2, don't get your hopes up.\n\n4) Why the hell are you even here hawking your GNOME shit? Sorry, but I suspect it is because of *sour grapes*. You can't believe than GNOME was passed up for enlightenment :-) Well, believe it!\n\nI think you should try KDE cvs... you'll find it very illuminating ;-)"
    author: "ac"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "<shrugs>\n\nVery well both of you.  We could stay up here all night, and accomplish not a single thing.\n\nAaron:\nTake me up on my offer.  Ask Mr. Butler on my tone in the discussion that we had.\n\nI look forward to meeting you both again..."
    author: "Anon Man"
  - subject: "Konqueror [Re: Nice awards...]"
    date: 2002-07-30
    body: "\nRe: \" 2) The GNOME 2 release was rushed by a long way... hardly any apps ported. Imagine if KDE 3 was released with just konqueror. LOL! Naturally the reason for the hurry has to be 'corporate pressures'.\"\n\nI think that *just* new version of Konqueror worth new *release*.\n\nProblem with GNOME (GNOME2) that they *do not have own browser technology*.\nMozilla is too heavy, and I doubt they can make it resonable light.\nI have looked over Mozilla sources - it's well over 150MB!\nYou need to trim sources at least by half, to make them reasonable in size and *hackable*.\n\nSo, if GNOME2 *had* own browser - I think it worth separate reelase..\nWithout browser - it's doesn't worth new release!\n\nRegards,\n\nVadim\n"
    author: "Vadim Plessky"
  - subject: "Re: Konqueror [Re: Nice awards...]"
    date: 2002-07-30
    body: "Hmm...I think the point is reusability\n\nWhy reinvent the wheel when there's a perfect one there?"
    author: "Bah me"
  - subject: "Re: Konqueror [Re: Nice awards...]"
    date: 2002-07-30
    body: ">Why reinvent the wheel when there's a perfect one there?\n\nThe point is, Mozilla is no where near perfect."
    author: "ac"
  - subject: "Re: Konqueror [Re: Nice awards...]"
    date: 2002-07-31
    body: "Besides of RAM usage (which is not good but not a big issue either) Mozilla is probably as close to perfect as free software can currently deliver. I'm talking about featureset. \nIt would take the GNOME guys years to create something that is on par with it and than it would still be in a disadvantage because Mozilla will continue to grow and unlike Mozilla, a Gnome-native browser wouldn't be cross plattform which would make compatibility issues even more of an headache than they already are. \nThere is really no point in doing that instead of improving Gecko.\nFor low RAM usage (for example to edit and show HTML mails in Evolution) there is still GtkHTML."
    author: "Spark"
  - subject: "Re: Konqueror [Re: Nice awards...]"
    date: 2002-07-31
    body: "by now you are probably right that it is probably too late (for a few reasons),  but a small handful of hackers working over the past few years accomplished exactly what you said would be pointless because they didn't agree with this line of thought. \n\nthey've spent probably 1/40th the man power (based on # of people and # of years) and the resulting product is free software, it's stabile, the feature set is great and is keeping pace with other HTML renderers, it's quite fast, it is native to the platform it runs in, has decent RAM usage and allows a single HTML widget to be used acrossed their platform. you may have heard of it: KHTML.\n\nthe javascript engine it uses is also native (KJS), which means it can be used outside of the HTML widget itself and in other platform pieces."
    author: "Aaron J. Seigo"
  - subject: "Re: Konqueror [Re: Nice awards...]"
    date: 2002-08-03
    body: "They have not accomplished it - konqueror fails to support many of the standards that mozilla supports, and it renders more pages badly. Don't believe me? take a look at some of the demos on http://www.mozilla.org/start/1.0/demos.html - all standards compliant pages.  Compare how well they work in mozilla and konqueror - then come back and tell me if they are equal (There are some cool demos there by the way, but even the complex spiral demo which is css1 only fails to display properly in IE or Opera, and didn't work properly in Konq last time i tried it.\nCreating a web browser is no easy task, and kde developers have done pretty well with konqueror, but it doesn't come close to mozilla (except in terms of integration with kde)"
    author: "me"
  - subject: "Re: Nice awards..."
    date: 2002-08-02
    body: "I try to avoid flames, but jesus christ\n\n1) Face up to the facts. GNOME 2 was unstable. So what that you installed it and got it running well, if lots of other people found it to be broken. Your machine is not the only one. The hard data is the people you see complaining... and there are quite a few. Read mailing lists, read articles like this one. Or you can stick your head in the sand and not hear any of it :-)\n\nUnstable? - you have got to be joking\nArticles like this one? - the comparison would be a gnome user installing from KDE cvs against garnome - not stable packages\n\nI have seen the occasional person complaining about lack of configuration options and that is about it\n\n2) The GNOME 2 release was rushed by a long way... hardly any apps ported. Imagine if KDE 3 was released with just konqueror. LOL! Naturally the reason for the hurry has to be 'corporate pressures'.\n\nHow rushed? - you just dont get it\nGnome is and always has been a framework which apps are built on top of\nThere is already a long list of apps ported - inc gnumeric,gaim,gftp,gnomedb\nand several more in the process\n\n3) GNOME has a history of hyping up its future technologies and failing to deliver. Witness Bonobo... even miguel says it is crap now. Just because something is 'in the works' doesn't mean it will be any good. And lets face it, with the problems in porting to GNOME 2, don't get your hopes up.\n\nHate to burst your bubble but Miquel!= to Gnome\n\n4) Why the hell are you even here hawking your GNOME shit? Sorry, but I suspect it is because of *sour grapes*. You can't believe than GNOME was passed up for enlightenment :-) Well, believe it!\n\nLike the KDE trolls pounce on any gnome story on Linux Today or Slashdot\n\nOn the Gnome User forum your message if reversed would be classed as flamebait\n"
    author: "redtux"
  - subject: "Re: Nice awards..."
    date: 2002-08-02
    body: "This guy is a known anti-KDE troll from Linux Today.  His name is Mike.  If you find his website it's rather sad..."
    author: "ac"
  - subject: "KMail vs. Evolution [Re: Nice awards...]"
    date: 2002-07-29
    body: "\nLet me give you my reasoning - why Evolution is not ready for everyday use.\nI am native Russian speaker, and Evolution ... doesn't support Russian.\nI even filed bug report about this - but there is no help!\nOn the other hand - Michael Haeckel was fixing bugs in KMail's NLS support at light-speed, that's why KMail, since KDE 2.1 release, has so good Cyrillic (Russian) support.\nI also recently tested it [KMail 1.4.2] with mails in Chineese - it also works very nice!\n\nDon't get me wrong.\nI guess Evolution for GNOME2 would be very good program. But GNOME2 should stabilize first. As Tim mentioned, it's still not ready at amoment...\n\nRegards,\n\nVadim Plessky\n\n"
    author: "Vadim Plessky"
  - subject: "Re: KMail vs. Evolution [Re: Nice awards...]"
    date: 2002-07-29
    body: "If it doesn't support your language then i guess that's a pretty good reason for using it!  Like you said GNOME 2 provides really good language support, so i would suggest you take another look at evolution when it is ported (they're going to do another 1.x release first, so the GNOME 2 version will probably be next year)\n(i'm not an evolution developer, but it looks like evolution _does_ support russian already - are you having problems with it?)"
    author: "Dave"
  - subject: "Re: KMail vs. Evolution [Re: Nice awards...]"
    date: 2002-07-30
    body: "\nwell, if you mean that I should bugreport to Evolution developers about support for Russian language - I did it. Not to much help from developers, though.\nLet me make things clear: I use KDE3, I use KMail exclusively, and I don't use GNOME and Evolution. But I am not going to bash GNOME or Evolution here. \nIt's fine with me if both develop further. And I hope NLS in GNOME/Evo will be fixed somewhere in the future.\n\nMy time is somewhat limited at a moment, so I can't spend much time on *testing* GNOME2 (like I did for KDE2 and do for KDE3). So, GNOME2 hackers should fix NLS in Evo and GNOME on their own...\nSorry if it sounds harsh - but that's it. I am not paid to fix GNOME bugs. :-)\n\n\n"
    author: "Vadim Plessky"
  - subject: "Re: KMail vs. Evolution [Re: Nice awards...]"
    date: 2002-07-30
    body: "No, what i meant is that from what i can tell (i don't speak russian..) Evolution _does_ support Russian now (in the stable 1.0x version).  So i was wondering whether you had actually installed it and tried using it - if so what was the problem.  "
    author: "Dave"
  - subject: "Re: KMail vs. Evolution [Re: Nice awards...]"
    date: 2002-08-01
    body: "Evolution works fine with Russian, so I don't know what you are talking about. As long as your iconv() supports the charset, Evolution supports the language. The only languages which this is not necessarily the case for are asian and BiDi languages (because character layout isn't the same as other langauges).\n\nOf course, the last 2 will magically be fixed when they port to GNOME2 and use Pango.\n\nIf text is displaying as dotted squares or something, then the problem is not evolution's fault - you just chose a font that doesn't contain the Russian glyphs. Use gnomecc to choose your font, or if you use the development version you can change the fonts used within Evolution itself. (all known issues with Russian messages has been traced to the user using the wrong font, usually an iso-8859-1 font or the like)."
    author: "anon"
  - subject: "Re: KMail vs. Evolution [Re: Nice awards...]"
    date: 2002-08-02
    body: "That's what i thought - sounds like the guy hasn't even tried evolution."
    author: "Dave"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "Why can't all the anti-kde looser gnomers like yourself just stop\nposting boring comments at dot.kde?? This is a forum for\npeople who love KDE."
    author: "A Ivarsson"
  - subject: "Re: Nice awards..."
    date: 2002-07-30
    body: "I complained about the article (which I found here), not about KDE.\nSometimes I read the dot because I'm interested in what KDE is doing, not because I want to argue."
    author: "Spark"
  - subject: "Re: Nice awards..."
    date: 2002-07-31
    body: "I've used Evolution for everything from personal organiser to POP3, to IMAP and LDAP - and I've been using it since it's early alpha days. It crashed a lot at first (hardly suprising, it was an early alpha)... and then suddenly (approx 18 months ago) it stopped crashing. Since then I've had ONE, repeat ONE, unexplained crash 6 months ago. This is an app I hammer on regularly - it is extraordinarily reliable. OfB are simply talking the usual KDE zealot rubbish, as a quick read of the other stuff on their site confirms.\n"
    author: "Compton"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "I see, a quick read of... ? Certainly not the positive mention of Debian, a distro that prefers GNOME. Certainly not the short article \"GNOME 2 Released, Presents Challenge to KDE.\" \n\nI know you don't like this award, but lets be fair and not disqualify a news site because you simply don't agree with who got an award. Afterall, can you prove that OfB spreads KDE zealot rubbish? Please do.\n\nAlternatively, we can discuss this rationally, as I did with Anon Man. Anon Man was certainly a good representitive of the GNOME community, rather then calling the executioner before the judge he was reasonable. You'll get a lot more information if you are reasonable too."
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "If there is one thing you can guarantee about KDE, it's that plenty of desperate dite admins will pop up and start slamming GNOME in an attempt to start a flame war and gain attention for their site. \n\nSo far, you've posted an awards piece slagging off Evolution for not integrating with KDE (duh!) - completely ignoring the fact that KMail is seriously nonfunctional for anyone not using a simple POP3 system; criticised GNOME for not running when you compiled it yourself (I suppose it never occured to you that you may have screwed something up yourself?) and starting a fuss over nothing by criticising RedHat (thereby inflaming the KDE zealort hordes into action) over something and NOTHING. And now you want to turn it around on to me... a representative of the GNOME community (which I most certainly am not). You've got a bloody cheek, I'll give you that much.\n\nYour troll site, ad-revenue generating career is off to a stellar start. Unfortunately you are also making sure that no-one except KDE advocates believes a single word you say. Perhaps that doesn't bother you, but I must warn you that they are a fickle bunch.\n"
    author: "Compton"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "Yup, that's true, and I'm not one of them. You'll notice the only things I said in the article about Evolution were positive. Certainly bashing GNOME does little to benefit me, and so I won't do it. Even if it did benefit me, I wouldn't bash it.\n\nNow, if you would just listen to me - something I think is hopeless - you would have noticed that I said that the KDE integration thing was because we found KDE to be easier to use. Thus programs that benefit from this easy to use style gain a bit. As well, I have listed problems with GPG and crashing as two other things that lowered Evolution's chances. It's a GREAT client. It just didn't win. Get over it - KMail is an excellent client too, and many people prefer it, yes, prefer it. Awards aren't exact, they are based on opinion and analysis. Thus you might like Evolution, but that doesn't make it the only possiblity.\n\nNow, about RedHat, if you would read all the comments you'd see most people agree with the assertion. RedHat's actions are definately note the way to treat a project the size of KDE or GNOME. Especially a project with major sponsorship from SuSE and Mandrake. And this was marked commentary, incase you didn't notice.\n\nFinally, read this message: http://dot.kde.org/1027806378/1028061892/1028089117/ . Now, I've noted there several things I've said that aren't exactly pro-KDE by any sense of the word. I've even suggested the future may be for GNOME. Now, tell me, does a KDE zealot write this kind of thing? "
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "<sighs>\nYou are giving the GNOME community a bad name.  Yes we can choose to disagree with his opinions.\n\n<b>We do not do so by flaming him!</b>\n\nPlease, let us act reasonably and keep in mind that GNOME 2.0 is acknowledged *by the developers* not to be completely ready for mass consumption.  By GNOME 2.2 this should be rectified.  Let us wait till until.\n\nPlease do calm down..."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "Hello Tim...the above post was *not* addressed at you - it was addressed at the post above.  Sorry if there's a misunderstanding."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "Hi Anon Man,\n  I had a feeling I knew who you were addressing it to. :-) Thanks for being the sensible voice from the GNOME community. I suspect there is no use trying to reason with Compton. *sigh*  \n\n  BTW, if you ever feel the urge to write something, you are welcome to the soapbox at OfB.\n\n  BTW 2, I believe it was you who posted a screenshot of GTK's new file selector dialog. Thanks! That's always been one of my pet peeves with GTK... I can't wait to have a decent open/save dialog for the Gimp! :-)\n\n  Have a good evening Anon! \n\n  -Tim "
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "Hi Tim,\n\nI suspect people on both sides are getting a bit...'agitated' over this.  I did not expect this thread to mushroom like it did.  Congrats on maintaining your cool!\n\nWe are like spoilt children fighting for control of a tiny apple, not thinking that if we 'work together' we will attract more that we can individually :)  I think a lot of people have lost the cooperation viewpoint...  It has become more of a 'my side should win' debate now, unfortunately.\n\nIt is for this reason that I am hoping for a good showing by GNOME 2.2 next year.   I honestly believe that we *need* two full featured DE for Linux.  At any rate, the initial GNOME 2.0.0 is (in my opinion only of course) a substantial improvement over GNOME 1.4\n\nThank you for the offer of the soapbox on OfB.  You can rest assured that if I do end up writing - it will be a measured, reasonable article :)\n\nOne minor quibble though (on a different note) - I am waiting for your take on this:\n\nYou stated in your Redhat & KDE article that KDE had \"over 50%\" of Linux users while GNOME hovered around 20%.  I'm not sure where your source is for this.  Could you please point it out?  Also, does this take into account that there are users who use KDE as a DE but a lot of GNOME apps?\n\nThanks!  I await your response."
    author: "Anon Man"
  - subject: "Re: Nice awards..."
    date: 2002-08-02
    body: "Howdy Anon,\n  The over 50% statistic (and accompanying stat for GNOME) was from OSNews. However, it can also be fairly reasonably figured (unscientifically this way):\n\n  1.) All the UnitedLinux *cought* distros prefer KDE. SuSE and Conectiva being the most notable desk top distros in the bunch - especially SuSE. SuSE is probably either second or third in desktop installations. I would imagine 90% of UL-distros users use KDE, based on my analysis.\n\n  2.) Mandrake, treats GNOME and KDE fairly equally, but as its heritage is as a KDE-friendly distro, it defaults to KDE. Mandrake is arguably the most popular desktop distribution. I'd think that at least 75-80% of Mandrake users probably use KDE based on my experiences.\n\n  3.) RedHat, I suspect, is probably second in desktops. Probably most RedHat users use GNOME, but many also use KDE. RedHat's announced disinterest in desktops, makes my suggestion that they are only second or less in desktop market share increasingly reasonable. I would imagine at least 30-50% of RedHat users use KDE.\n\n  4.) If we then consider various smaller distros like Lycoris and ELX, we end up with most of them using KDE as well. On these distros you probably have virtually 100% KDE users.\n\n  In the end, while GNOME may end up on more systems (even some sloppy server installs), I think most like KDE has a much larger portion of the actual desktop pie. This is most likely especially true now that Mandrake (presumably with a KDE default desktop) and Lindows are being sold preloaded at Wal-Mart.com.\n\n  Anyway, while I'm being fairly simplistic here, this coupled with polls on people's preferences, the fact that KDE was usable sooner then GNOME (fairly, I would say KDE 2.0 and GNOME 1.4 were when they both became somewhat usable), etc. make this case fairly convincing.\n\n  It might be interesting to do an actual analysis someday, although I'm not sure how you could ever make it actually accurate. Since distros don't know what their users use, and surveys are woefully inaccurate. :-( \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-08-02
    body: "Oh, one more bit - http://www.desktoplinux.com/news/NS3153607016.html . DL's poll - yes polls are woefully inaccurate - is probably the most accurate desktop survey so far. Based on over 2,500 responses, Mandrake and SuSE are on top.\n\n  HTH,\n    Tim"
    author: "Timothy R. Butler"
  - subject: "file selector"
    date: 2002-08-02
    body: "btw have you tried ximian gnome (1.x not 2.x) - they replaced the standard gnome file selector with one which i think you'd prefer.  I guess they'll do the same with 2.x when they do their release(although they hadn't last time i looked at the snapshots).  While i'm talking about gnome 2.0 i'd really recommend that with something as large as a Desktop Environment (gnome or kde) you wait for it to be integrated into a decent distro (My preference is redhat - you might not like them, but i think they do gnome best ;-) or for ximian to add their polish and package a release.  Why? - because this is what the vast majority of users do, so it will be more representative of the experience that they will get.\nThanks for reading this far... :-)"
    author: "Dave"
  - subject: "Re: file selector"
    date: 2002-08-02
    body: "Hi Dave,\n  Thanks for the advice. I haven't run Ximian GNOME, but Mandrake 8.1 (not 8.2) did come with Ximian's extentions to the GTK file selector (MDK removed them because apparently they caused problems with some apps). Anyway, I thought they were nice. Still not as nice as KDE 2.x/3.x's or apparently GTK 2.4's boxes either... but they were nice.\n\n   Thanks,\n        Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "But why do they release a 2.0 final version if it's still not ready for the masses. You know, this is meant honest and not as the usual flame against Gnome, but for my taste there are too much promises for future versions in the Gnome world. Every complaint will be fiexed, every feature added, in some far-off new Gnome version. But what about the current reality?"
    author: "Ralex"
  - subject: "Re: Nice awards..."
    date: 2002-08-01
    body: "Hi Ralex:\n\nThe decision was made for a number of reasons.  I have mentioned them earlier, but will do so again (because you have not flamed)\n\n1) Developers needed something to work against.  GNOME 2.0.0 was stated (by the core gnome developers themselves) to mean a general freezing of the API etc.  It is not meant to be extremely full featured.\n\n2) Some features had been stated (very far back) to be included only in later versions.  It was felt that if too much was added in one lump, debugging, management would become hell.  \n\nHere are some promises GNOME has kept:\n\n1) Nautlius is fast.  You do not have to take my word on this - please try it for yourself\n\n2) AA is there...\n\n3) The API is stable - a lot of changes have been made under the hood.  I think that is the most important point of this release.  The under the hood changes have been made, now the apps have to move to it.\n\n\n4) Usability has improved.  Granted, this is difficult to quantify, but the mess of dialogs and lists of preferences has been pared down.  You can still change stuff using gconf.  Also, the gnome developers appear to be pushing for more conformance of the HIG.\n\n\nNone of this souns very sexy - but it is extremely important if you want to develop further.  We would all love for the changes to come immediately, but that is unfortunately not the case <shrugs>Oh well...based on what I've seen, I'm *more* than willing to wait :)\n\nCurrent reality is actually quite good :)  If there's one thing to take away from this its \"Try it for yourself\".  Do not assume that just because someone said something bad about GNOME it is so.  Try it out!  You can use GARNOME, your dist. packages, Mandrake beta, etc to see what the new GNOME will be like."
    author: "Anon Man"
  - subject: "KMail is great!"
    date: 2002-07-28
    body: "Well deserved I think that KMail is at times very underestimated and really deserves to win this award. I'm a former windows/Outlook user but I really don't see the greatness with Outlook. I want a stable functional Email client that handles my mail stable and secure and I have found that in KMail. \n\nOf course if the criteria for winning is most Outlook a' like then Evolution should be the winner, but it wasn't was it ...\n\nCongrats to the developers and keep up the good work!"
    author: "DiCkE"
  - subject: "Congratulations !!"
    date: 2002-07-29
    body: "Again, congratulations !!!"
    author: "Murphy"
  - subject: "Update the \"Awards\"-page?"
    date: 2002-07-30
    body: "The KDE awars-page seems to need an update - the last award here ist from 2001!"
    author: "Ralex"
  - subject: "Tim R. Butler Open For Business"
    date: 2002-07-30
    body: "Tim Butler and the Open For Business website and these awards are a total sham!  Tim is a devout KDE user and pretty much the only person who runs Open For Business.  In fact he is the person who really started the embarrassing \"jihad\" against Red Hat today.  So anyway I hope all of KDE enjoys the shameful awards bestowed upon you.\n\nChristopher D. Felton"
    author: "chris felton"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: ">So anyway I hope all of KDE enjoys the shameful awards bestowed upon you.\n\nI'm certainly enjoying all the rabid GNOME supporters coming out of the Gnotices Ghost Town, desperately trying to find reasons other than that GNOME 2 was crap as to why GNOME did so poorly."
    author: "ac"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "You know Tim, he's on your mailing lists, he's on the dot, he's pro-KDE.  He certainly not impartial.  So he shouldn't try to pass off these awards as impartial.  It is a tactic of the worst political campaigns.  If you like winning fixed fights--good for you, but I respect integrity.  It is not very Christian.\n\nSincerely,\nChristopher D. Felton"
    author: "chris felton"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: ":: It is not very Christian.\n\nHunh?   \n\n*Plonk*!!!\n\n--\nEvan\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "Am I pro KDE? Yes. Am I pro GNOME? Yes. :-) See the paradox here? Right now I don't think GNOME is quite ready for primetime, however Anon Man (who was here yesterday) as well as others have very encouraging reports about GNOME 2.2. I await anxiously, and will give GNOME 2.2 the fair review it deserves - whether that be postive (I hope) or negative.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "It it just generally not a good idea to give official \"rewards\" if you are a user of one of those systems, especially if you are basically the only jury. It is completely impossible for a user of one system to be completely unbiased. \nIt was just a bit unlucky...\n\nBTW, when you testdrive GNOME again next it would be nice from you if you would choose the Ximian version. Those are meant to be good even without decent knowledge of installation issues and those are especially meant for businesses so they should fit to your site. :) When you test a version of Mandrake of Redhat or even compile for yourself, always keep in mind that some problems might be installation specific (like the missing-helpfiles desaster that lead to a lot of anger in Eugenia's GNOME 2 review).\nBeeing able to be easily installed by endusers is definetly one of the advantages of KDE but to be fair, GNOME shouldn't be tested under those conditions because this is prone to errors.\n\nAlso I suggest those to avoid trouble:\nDon't \"review\" something that you just took a short look at. It always takes awhile before people get really used to something new, even if it's good. :) As an (maybe stupid looking but I think it fits) example when I eat something new, it often happens that I don't like it at first but when I eat more of it, suddenly I get used to it and even start to love it! Same with new homes. When moving, you might think your new home would be terrible but after a while you get used to it and learn about all the good things of your new home and start to like it. Of course if it's really a bad new home, this won't happen. :)\nSo if you can only do a short review (like an installation, followed by a maybe one day triage) then clearly mark it as that and avoid generalizing comments about the quality or the competence of the developers.\n\nAlso always avoid commentaries about market share and such. :) That has absolutely no place in a review.\n\nMy point basically just is, that I'm sick of \"xyz sucks\" reviews. We should have more \"xyz rocks\" reviews. If something is loved by millions of people, it just can't generally suck, no way. :) Yes, this also applies to MS Windows although many Windows users probably don't exactly love it.\nI'm not saying that you would wrote such a \"xyz sucks\" review... Just want to make sure you don't. :) Constructive criticism and discussion is always welcome of course. "
    author: "Spark"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "Well, I'm not the only jury member I should note. :-) The other thing I should note is while I use KDE, I use KDE for the reasons we gave the award, rather then we gave the award because I use KDE. If GNOME 2 ever become more productive in my opinion then KDE, I would switch. I do have some loyalty to the community, but for the most part I'll use whatever product works best within my criterion (Free Software, mostly).\n\nA/f/a Ximian. I wanted to get a test run of GNOME 2 last month, and since Ximian IIRC still hasn't released a GNOME2 package, that would have caused GNOME not to be in the desktop at all. :-( I will try Ximian GNOME2 once it comes out, however I do consider it a mark against either D.E. if one can not use it without lots/some/a few problems directly from a distro CD. I'm sure you can understand this. It's really ashame that Ximian GNOME is seperate, and thus the average user doesn't get to enjoy the benefits.\n\nAnyway, you are right in the last part of your comments, as well. So far I haven't given any bad reviews of anything. Generally there is always something postive to highlight, which I try to do. On the other hand, I note the problems too, as my goal is to allow the reader to have the tools needed to make a fair judgement, and not just receive the rose colored glasses information. :-) Overall, I think that was indeed what you were suggesting - so we agree I suspect.\n\n  Best,\n     Tim\n\n"
    author: "Timothy R. Butler"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "I would have believed your claim to being impartial, but then i read your Radhat article.  The biggest load of Bullshit i have ever read - it deserves to be a comment on slashdot (modded down to -1 as a troll), not as a \"News\" article.\nYou just surrendered any credibility you had by publishing that."
    author: "me"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "It was not a \"News\" article, it was marked commentary. Surely, one has a right to an opinion, doesn't he? Apparently RedHat saw the problem with its earlier position to, as they have now said that they are going to demo KDE during their regular RH demonstrations, and they will provide a machine to demo RedHat on at the KDE booth.\n\nFurther more, I await anyone able to prove that what was said in the article was not true. Is it not true that RedHat should not contact a project they don't help and ask (as if it was fair) that the said project demo their software, while in return the project only receives their name printed in a list of names? Is that really a fair offer by RedHat? Certainly that isn't want Mandrake and SuSE are doing to GNOME! \n\nI'm not impartial about RedHat, I have never claimed to be. RedHat != GNOME.\n\nAnyway the key thing is - anyone who says they are completely impartial shouldn't be believed. However, one can do a very good job of disconnecting their opinions from their writing. And, when I am unsure that I have, I contact others and ask for them to see if I let any bias in to the article. Take for instance my SuSE review. I have a number of qualms with SuSE, so when I did the review, I had one ex-SuSE user and one current (pro-)SuSE user take a look and advise me on whether they thought it was fair. Both confirmed it was indeed fair. Infact, many people read the review and were left with the impression that my favorite distro was SuSE, since I presented a number of flaws, but presented them in a fair way. Mission accomplished.\n \nOn the other hand, I do publish commentary. Commentary is ALWAYS biased. You can't make your opinion known and not be biased. Non-biased commentary, in reality, is news. \n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Redhat not equal evil"
    date: 2002-07-31
    body: "\"Is it not true that RedHat should not contact a project they don't help and ask (as if it was fair) that the said project demo their software, while in return the project only receives their name printed in a list of names? Is that really a fair offer by RedHat? Certainly that isn't want Mandrake and SuSE are doing to GNOME!\"\n\nYou put this in a way that sounds bad for Redhat. \nIf you state it simply like it is, it sounds completely different:\n\"A Redhat PR guy made KDE (and every other exhibitor) the offer to include their name in lists at their booths, their website and a newsletter, when in turn they run their presentation on a Redhat distribution (which they provide) and put a \"Powered by Redhat\" sticker on the system.\"\n\nThis is the complete truth, not \"adjusted\" in any way. Does it still sound like Redhat did something highly immoral? Not really, does it? \nFlaws in your statement:\n- They didn't ask KDE to demo their software, they just asked them to run at least one system on Redhat and place a sticker there. Big deal.\n- It was just a simple PR exchange. Mentioning of KDE all over Redhat in lists against mentioning of Redhat on a sticker.\n\nThe offer of KDE in turn was that they wanted them to carry a system for them and demo KDE at their booth (and indeed KDE, not just have it running to demo something else). For a sticker! Also I could imagine that this was confused with KDE people asking to present KDE personally at Redhat booth, that's why the PR guy probably said that they are already full.\n\nThe only thing that Redhat did really wrong was offering KDE the same as every other smal exhibitor, as KDE is a damn big project and was probably pissed by this lack of respect. But wouldn't simply declining the offer have been _much_ simpler? Is this a reason to call Redhat a \"Linux for idiots\" (see the page title here: http://www.derkarl.org/rednot.phtml)? \nThe question is, who is acting immature here.\n\nBTW, neither do I use any kind of Redhat system, nor do I plan to in the future and I'm not related to any Redhat developer.  I would never think of defending them when they would really do something immoral.\nRedhat's PR treating of KDE was definetly arrogant, but KDE answered with arrogance too, so what. :) And maybe it was just a clueless PR guy who didn't had deep insight of the problems between Redhat and KDE. Maybe none at all. The fact that Redhat now even agreed to carry them a computer and monitor KDE in their booth (as you say) is pretty cool IMO."
    author: "Spark"
  - subject: "Re: Redhat not equal evil"
    date: 2002-07-31
    body: "The point of my phrasing is that RedHat was asking an awful lot from a project they publically attacked previously (admittedly before Qt with QPL'ed and then GPL'ed), have very little support for (security updates even fail to become available), and whom they pay no one TTBOMK to work on (Bero does KDE work on his own time). The really arrogant thing is, then, to ask this project - which is largly sponsored by SuSE and Mandrake - to use RedHat. \n\nThat would be, in effect, like going to the GNOME people with a similar offer from Lindows.com or Lycoris. These two distros have actually treated GNOME better then RedHat has treated KDE in the past (they just ignore GNOME), but it would be insane of them to ask GNOME developers to demo their system since they don't support GNOME.\n\nAnyway, that, I see is the heart of the problem. RedHat was asking for at least one system to run RedHat w/ a Shadowman sticker on it - something KDE's sponsors probably don't appreciate much - and in exchange they got... mentioned just like every much smaller project that did the same. Certainly, as you suggest, a project that has more lines of code then any other Open Source/Free Software project (IIRC) deserves a bit more respect before it gets a Rodney Dangerfield complex.\n\nStill, I don't think RedHat is evil. Infact many times I have considered them better then SuSE. UnitedLinux is much much more of a danger to the community then RedHat, IMO. RedHat also has some wonderful people such as Michael Tiemann who do a good job of presenting the GNU/Linux case. And, RedHat - like Mandrake - doesn't publish any of their software under non-free licenses, something I applaud them for. I just don't happen to like some of the actions of their Marketriods.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Redhat not equal evil"
    date: 2002-08-01
    body: "\"RedHat was asking an awful lot\"\n\nIsn't this quite a far-stretched interpretation of running one system on RedHat and putting a small RedHat sticker on a screen?\nIt was a simple offer obviously sent to every ehibitor, I don't see how you can call this bad intentions. The marketing guy most probably had no clue about the difficult past and from the wording of his mail, he didn't intent to offend the project in anyway. I can completely understand that KDE didn't want to take this offer but it was KDE that offended RedHat first (\"Linux for idiots\"), not the other way around. Taking pride into a project is one thing, overreacting is another thing. I just read the mailinglist archive and was happy about Kurt Granoth's response, I think he put it very well. \n\n"
    author: "Spark"
  - subject: "Re: Redhat not equal evil"
    date: 2002-08-01
    body: "I don't think it was a far-stretched interpretation. If your competitors are sponsoring a project, it is asking a lot of that project to use your distro on their *own* computer and display *your* sticker, at least if you have exactly ZERO paid developers working on that project at all (and that project is of the a stature of KDE, GNOME, X11, etc.). Its asking evening more, since you can't even use official RedHat packages to demo the machine...\n\nPerhaps Mr. Mann didn't know about KDE's large size, however, surely he did some research before denying KDE's requests. If not he was stupid, and if he did, and still said they wouldn't demo KDE (even though they actually are), he wasn't so bright either. Either way, I think what speaks volumes is that Todd Burr of RedHat decided to contact KDE and resolve this - he wouldn't have given in if RedHat really felt the way it seems Tommy Mann did.\n\nSo now, if RedHat provides a system that has a well configured KDE setup they stand to gain a LOT - people may stop at the KDE booth, see the great working RedHat setup, and start using RedHat. That's a pretty good deal, needless to say. As someone else pointed out, RedHat is the only one that is going to make any money out of this deal. Certainly KDE won't, nor will Mandrake or SuSE (unless the RedHat system is terrible compared to the other two's systems ;-)).\n\nAnyway Re Linux for Idiots, that did not appear until after RedHat's foible, so KDE hardly fired the first shot in this case. Was that called for? No, but Charles was quite reasonable until after the whole thing was over."
    author: "Timothy R. Butler"
  - subject: "Re: Redhat not equal evil"
    date: 2002-07-31
    body: "> Redhat's PR treating of KDE was definetly arrogant,\n> but KDE answered with arrogance too,\n\ni take exception to this. the response from Charles was \"sure, if you'll reciprocate by showing some KDE in your booth and provide us with a system to show Redhat on with the latest stable KDE as we don't have such a system available\". the wording was a little different, but we're not all poets. it wasn't arrogant, however.\n\nwhen redhat replied \"no, we won't show kde and now we won't give you a demo system. but hey, you could STILL use redhat, right? right? media matrix!\" that! was arrogance. well, actually, it was ignorance. because apparently the guy who wrote it was something of a marketing cluefuck who had no idea what KDE was.\n\ntim wrote an article on the reaction (which was basically, \"typical redhat. ask something of kde but give nothing back. still no support shown for kde. fine! i can just as easily steer clear of redhat as they can of kde.\") when that reaction was picked up by people at redhat via tim, they realized the error that had been made and worked with some kde folks to get it resolved. and resolved it was.\n\nif people had just sat there and shut up, redhat wouldn't be in the kde booth and kde wouldn't have gotten a public thumbs-up from redhat. in other words, the fact that some people found the guts and the brains to speak their mind affected actual, positive change. sometimes the status quo sucks. sometimes it needs to be messed with. as long as it is done with a measure of prudence and civility, it can result in good things.\n\nbut it is not arrogance to ask for reciprocity. it is not arrogance to promote your project, at least when you do so w/out request to abandon other \"competing\" projects in the process. it is not arrogance when one decides to use systems that support your project. it is not arrogance to expect that when a project does include your project, that they treat it well and don't botch it for the users with a half-hearted attempt.\n\narrogance is feeling you have the right to call someone else arrogant for having pride in and looking for fair treatment of their contributions."
    author: "Aaron J. Seigo"
  - subject: "Re: Redhat not equal evil"
    date: 2002-08-01
    body: "\"i take exception to this. the response from Charles was \"sure, if you'll reciprocate by showing some KDE in your booth and provide us with a system to show Redhat on with the latest stable KDE as we don't have such a system available\". the wording was a little different, but we're not all poets. it wasn't arrogant, however.\"\n\nI didn't meant this answer but the answer(s) after RedHat's rejection. \n\n\n\"when redhat replied \"no, we won't show kde and now we won't give you a demo system. but hey, you could STILL use redhat, right? right? media matrix!\" that! was arrogance. well, actually, it was ignorance. because apparently the guy who wrote it was something of a marketing cluefuck who had no idea what KDE was.\"\n\nNot just that, it was probably also a misunderstanding. Note the wording was quite unclear when KDE asked for \"KDE to be present\". Maybe this was understood as KDE people showing KDE at the RedHat booth. Either this or the marketing guy really had no clue as RedHat obviously planned to show KDE anyway.\nBut you can't blame the marketing guy for repeating his offer, after all this is his job! And it was not really a _lot_ he asked for and it was also just a friendly offer that KDE wanted to decline anyway (this seemed to be the general idea on the mailinglist). "
    author: "Spark"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-08-02
    body: "OK fair enough - you've admitted you don't like Redhat.  However i read it thinking it was supposed to be a new article - it was in the big box on the front page, and unfortunately on this win2k laptop that i'm currently using the font and size of the word \"commentry\" make it near impossible to read (i have anti-aliasing turned off, because they seem to make it look even worse in win2k on an lcd screen, i never thought i'd say this, but the fonts in linux - especially gnome 2 - aren't that bad).  If you want to make it clear it's opinion i'd suggest making it a bit bigger.\nPersonally i like Redhat - they are the only distro i have tried which has always been stable.  Last time i tried Suse (7.something) i got random crashes for no apparent reason - i would like to try it again but they don't have downloadable iso's, and their ftp install is painful (it makes a debian install feel like fun), in fact when i tried it i was left with a non-booting half installed suse system.  So suse may or may not have caught up with redhat in terms of reliability - i'll never know unless i spend money on it (which i won't because at the moment i don't trust it).  I have also tried mandrake, and it just seems to fall apart after a few weeks, configuration utilties stop working, updates fail etc - no crashes though, so better than suse.  So understandably i prefer redhat, and they're the only distro i could currently recommend to people."
    author: "me"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "\"I will try Ximian GNOME2 once it comes out, however I do consider it a mark against either D.E. if one can not use it without lots/some/a few problems directly from a distro CD.\"\n\nGnome is a desktop environment, not just some application so it somehow depends on careful integration by the distributor. GNOME provides the \"base\" for distributors to create their desktop systems just like Mozilla provides a base for browser developers. Many distributions have had _excellent_ Gnome integration in the past like Redhat, Progeny (RIP) or even Mandrake (although it was extremely colorful and fancy ;)). So the reason I suggest Ximian is simply that in this case the review won't be influenced too much by distributor specific implementations. If you review Redhat GNOME 2 (which would be possible in an early beta with Redhat Limbo) or Mandrake GNOME 2 (which should be there in Mandrake 9.0 Beta but I don't think the integration is quite finished yet), you should specifically make this clear in your review because problems could be problems of the distributor or (taking Limbo as an example) the default panel layout could be different so if this sucks, the distribution is guility, not GNOME. =) Ximian is just known to provide a GNOME distribution of awesome quality because they focus on this task.\nThere is not really anything they do that other distributors can't do, they even have a patch page now that conveniently sorts all the special patches they use for their distribution so other distributors can take them.\nThis just shows how different the philosophies of those two projects often are and a reviewer should be aware of those, otherwise it might happen that he compares apples to oranges (not that this would be a bad comparision, but there is no point in complaining that apples are less juicy than oranges ;)).\nThe GNOME way has just as much advantages (one of it is that GNOME can concentrate on develpment and usability while distributors can concentrate on packaging and integration with their system) and disadvantages as the KDE way. You should respect both.\n\nSorry for nitpicking so much on this issue, but I wanted to make my point clear. ;) I will shut up now."
    author: "Spark"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "Good information Spark, thanks. I will be - time permitting - testing both Limbo and Mandrake 9.0 Beta 2 soon, and I will give GNOME2 a try while using them. I will also give Ximian GNOME a try once a GNOME 2 version comes out, and keep in mind your points.\n\n  Best,\n     Tim"
    author: "Timothy R. Butler"
  - subject: "Christian??"
    date: 2002-08-02
    body: "What if I told you that Miguel and Nat were gay lovers.  Would that make GNOME non-Christian enough for you?  Sheesh."
    author: "ac"
  - subject: "Gnotices Ghost Town"
    date: 2002-07-31
    body: "> Gnotices Ghost Town\n\nHilarious burn!!  What the hell is up with GNOME anyway?  It's all but dead and now they are coming here. Did the rest commit mass suicide or something?"
    author: "dc"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-07-31
    body: "no, i think they moved to those web forums they created (http://help.gnomedesktop.org/forums/). it seems they spread out their readership over so too many sites so that none are able to keep \"critical mass\" going.. i've been watching the # of post on the web board and its steadily decreasing. it was ~100/day for the first 10 days or so and now it has slowed to a crawl. gnotices has also fell sharply in the same time frame it seems. feedback based websites need enough users feeding back, otherwise they get feeling rather empty and collapse. *shrug*"
    author: "Aaron J. Seigo"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-07-31
    body: "Smartboy.\nComments on Gnotices are stagnating because they have turned on some kind of moderation feature, it can take up to a day until your message appears. Of course this doesn't lead to very active discussions and flamewars. =)\nThat doesn't mean that GNOME readers are spread out. I wouldn't know about any webboards other than the official Gnome.org and Gnomedesktop.org (including it's new forum)."
    author: "Spark"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-07-31
    body: "yerf! why did they do that?"
    author: "Aaron J. Seigo"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "Good question. =) It really gives the impression of a ghost town but it's not true. Maybe there were too much flames so they decided to moderate _before_ things get too heated. "
    author: "Spark"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "GNOME has always been a closed community. It could be Sun asked them to stop the comments."
    author: "dc"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "Oh my god...\nYou aren't serious, are you?\n \nAaaw fuck it, I should better leave this \"friendly\" place or I won't be able to stay calm any longer.\n\nRead this for a clue: http://mail.gnome.org/archives/gnome-web-list/2002-July/msg00025.html"
    author: "Spark"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "I see.  So Ximian is in charge, not Sun."
    author: "ac"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "That is wrong in so many ways I'm tempted to lose my cool.\n\nYour comment shows *incredible* lack of understanding and a willingness to troll that I find...'breathtaking'\n\nIf you are so confident of your comments please present substantiated proof.  I suspect you will be remarkably unable to do so."
    author: "Anon Man"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "Let me guess... your aliases are \"ac\" and \"Aged Person\"? You sure like to troll about GNOME."
    author: "Stof"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "that would be double strange IMO, because i realy haven't seen much in the way of flames over at gnotices in a long time. yeah, you get the occasional annoyance posting, but it was pretty clean. as clean as theDot has been in any case (though this article has more than its fair share of trolling on both sides). was there a really bad flare up or something that i missed? i usually only go there one a week or so to keep up with that hap's in GNOME, so perhaps i missed it?"
    author: "Aaron J. Seigo"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "The reason you haven't seen flames for a long time now is because they began to moderate it."
    author: "Stof"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "Well about a year ago (or two; it's been a long time), alot of people began to abuse Gnotices. All kinds of trolls spread FUD (even after so many people had proven them wrong), call people names (\"Ximian is GAY!\") and posted gross pictures (like those from Goatse.cx). Finally, the Gnotices admins had enough of the abuses, so they started to moderate all posts.\nRemember, this is all because of the abuses. I'm quite happy about that they did, since the number of trolls exceeded the number of \"good\" posts."
    author: "Stof"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "Actually I would attribute this more to users being excited in the first week that they had a place to go.  Many people still rely on their distributor to provide GNOME pacakges and most have not supplied GNOME 2 stuff.  I think we'll see it increase as soon as GNOME 2 starts to get out there.\n\nSpark points out another good reason why there aren't 100 posts immediately.  The tone is kept civilized.  Honestly, when I'm reading parts of these messages (making no reference to a specific person) its like a free for all!  Off topic comments generate more offtopic comments etc etc etc..."
    author: "Anon Man"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-02
    body: "Re: gnomesupport.org\n\nThe other reason is that the site has not been widely announced as yet (only announced on fairly low volume sites such as the parent and gnotices)\n\nAnd please no-one submit to slashdot"
    author: "redtux"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-07-31
    body: ">What the hell is up with GNOME anyway? It's all but dead and now they are >coming here. Did the rest commit mass suicide or something?\n\nWe are all wondering the same thing.\nMaybe GNOME is not that cool anymore."
    author: "AI"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-01
    body: "WTF, do you think every GNOME user hates KDE with a passion? Not everybody is so narrowminded."
    author: "Spark"
  - subject: "Re: Gnotices Ghost Town"
    date: 2002-08-02
    body: "good reply dude :)"
    author: "lemon"
  - subject: "Re: Tim R. Butler Open For Business"
    date: 2002-07-31
    body: "Chris,\n  I see you've been busy investigating me. I'm an interesting choice to pick for a \"devout\" KDE user. Let me provide evidence <i>against</i> that idea. Certainly, if you asked someone who actually follows KDE Cafe (btw, I'm on the GNOME mailing list too), you would notice I wrote this - http://lists.kde.org/?l=kde-cafe&m=102512520406384&w=2 . The article referenced in there was also linked to on OfB at http://www.ofb.biz/modules.php?name=News&file=article&sid=132 .\n\n  You would also have noticed my recent editorial on LinuxandMain.com, in which I was accused of extreme anti-KDE bias - http://www.linuxandmain.com/modules.php?name=News&file=article&sid=131 . Trust me, these are not the writings of a devout KDE user.\n\n  What does this lead one to conclude? (1) I'm not fond of RedHat, so I reported on a major PR blunder they caused over the weekend. (2) I use KDE right now and prefer it, but I see some flaws in it, that keep me highly interested in GNOME 2. (3) I clearly mark items such as the RedHat report under commentary so that you know what is opinion and what is reporting. I refuse to mix the two any more then I can help.\n\n  Finally, you are incorrect about who is helping with OfB. While I am the primary contributor, I have two other collegues and many who participate in advising editorial opinions on the site.\n\n  Just because I didn't give RedHat a great report today doesn't mean I'm biased. Its an easy excuse to disqualify the awards, but it would be wrong, especially if you value integrity. Also, notice the awards include one to Gaim (a GTK/GNOME app) and OEone (a GTK/Mozilla based distro). \n\n  Hopefully this should go a long way to convincing you I have opinions that are both pro-KDE and pro-GNOME. On some issues I'm more pro-KDE, on others I'm more pro-GNOME. In the end, it all evens out quite nicely.\n\n  Best, \n     Tim"
    author: "Timothy R. Butler"
  - subject: "Kopete?"
    date: 2002-07-31
    body: "Kopete?  \"Best Communications Software\"??  Ohhhhh, I get it...it's a joke!  Of course Kopete, which crashes constantly and doesn't work properly with blocked MSN users (which is a documented bug that the developers \"don't have time to fix\") beat Gaim, which is a fully featured and *stable* IM client.\n\nWho the hell ran these awards?"
    author: "someguy"
  - subject: "Re: Kopete?"
    date: 2002-07-31
    body: "You are an idiot."
    author: "ac"
  - subject: "Re: Kopete?"
    date: 2002-07-31
    body: "Try out Kopete and judge it yourself."
    author: "Stof"
  - subject: "Re: Kopete?"
    date: 2002-08-01
    body: "i think the reason that ac called someguy (ah, anonymity) an idiot is that kopete didn't beat out gaim. gaim took the category and kopete came in second (or honorable mention, whatever). kind of makes someguy's little rant a little silly."
    author: "Aaron J. Seigo"
  - subject: "Re: Kopete?"
    date: 2002-08-01
    body: "\nI think Kopete pretty much got a nod as a really promising application.  That pretty much fits - I used it for a couple weeks, but it's not quite ready for prime time - no Yahoo, and AIM is not perfect.  I use Psi, FWIW.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kopete?"
    date: 2002-08-01
    body: "Too bad there isn't a way to mod down trolls in Squishdot. As Aaron already noted (and anyone who read the article would know) Gaim won, Kopete got an honorable mention (\"runner up\") for the progress it made."
    author: "Timothy R. Butler"
  - subject: "I love KDE"
    date: 2002-07-31
    body: "I love KDE, we all love KDE, it is the best that has\nhappened to Linux. Here at dot.kde we talk about\nnice things that happen to KDE such as awards.\nIf you want to talk bullshit about KDE or stuff\nthat we KDE-lovers won't like, go somewhere else.\nI just hate to see all anti-KDE loosers on this forum,\ndo you seriously think anybody gives a # about you?"
    author: "AI"
  - subject: "Re: I love KDE"
    date: 2002-08-01
    body: "Not everyone who disagrees with something and actually doesn't hate GNOME is a anti-KDE looser. Can you imagine?\nThis place seems very hostile towards \"outsiders\". Sad."
    author: "Spark"
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "First, on behalf of all those who 'have' trolled - I apologize.\n\nBut your comments are equally hurtful and close minded.  Let me break this down:\n\n1) Not everyone likes or prefers KDE.  Some people like/prefer GNOME.  This does not make us losers.  Notice that there are quite a few people making *valid* comments.  I noticed only two obvious trollers (of course, they posted multiple times...)  Your opinion (or those of others here) is not necessarily the right one.  You should be open to other viewpoints.\n\n2) Yes you should give a # about us because we use Linux - just like you do.\n\n3) \"KDE is the best thing that happened to Linux\"  At the risk of fanning the flames here I will ask what your justification for that statement is.  If KDE didn't exist - something else would take its place <shrugs>That's life."
    author: "Anon Man"
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "there is a middle ground between what you said and what Al said. i can't get myself to agree with either of you, but if i squash what you both said together, i think i can =)\n\nliking GNOME does not make one a loser. disliking KDE does not make one a loser. (and vice versa, for that matter). posting rantingly pro-GNOME or anti-KDE posts on a KDE discussion board (or vice versa) does get one close to loserdom though. judging by some of the posts in this discussion, there have certainly been some losers in the woodpile. there have also has been, as you note, some good discussion.\n\ni do wonder though: if gnotices were to post a congrats to GAIM for taking the award for messenging software and there were a bunch of \"but kopete shoulda won!\" posts, long threads about the implicit GNOME bias of timothy, and repeated encouragements to try out KDE from GARNOME for themself, would you consider them to be on topic? how would you feel reading that on gnotices?  one might be inclined to say, \"arg! kde fans! always on about KDE-this, KDE-that. why can't they just let GAIM have its moment?\"? \n\ni understand that it's a really hard line to tread; you don't want to shut up and say nothing about something you feel strongly about, but at the same time you proably don't want to encourage the thought that people who like GNOME troll around looking for opportunities to shamelessly pump GNOME in forums where the topic is KDE.\n\nas the two projects move towards interoperability and maturity, hopefully the same can happen between the user groups as well. perhaps when someone wins an award, the best thing to do is give them some props, shout out some support to your favourites that didn't win to keep improving so they can take an award next time and move on. there probably isn't much need to turn every award or comparison or review into a \"but we swear ours is better!\". it just rubs people the wrong way.\n\nin the immortal words of rodney king: \"why can't we all just .... get along?\""
    author: "Aaron J. Seigo"
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "I can only speak for myself.\n\nBut I'd like to know how many people here who have said that GNOME 2's usability is bad etc. have *actually* tried out GNOME 2. I also notice you mention 'pro GNOME' and 'anti KDE' posts.  Have you noticed no 'anti GNOME' posts?\n\nPS.  If you do want to see plenty of those, feel free to hop over to Slashdot when ever a GNOME/KDE story comes out.\n\nI point people to GARNOME so that they don't get their GNOME opinions from second sources but from their own experiences.  *That* is called exposing people to other opinions.  I guess that *is* called \"shamelessly pumping\" GNOME.\n\nAlso, one interesting thing.  You mention the GNOME trolling, but you do not mention one pertinent fact: Not a single comment here is directed at KDE 3's win of 'best desktop environment'.  Actually, wherever I've gone, I've seen 'no' comments about this.  People do not say anything, because at this point and time it is true.  What did concern people though was that Evolution was not chosen over KMail.  There were genuine questions asked.\n\nI am going to ignore the entire Redhat thread because it has no basis in the original topic of the post"
    author: "Anon Me"
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "\nOh, bloody hell.  Is there any possibility we can have a no-gnome-dot.kde.org that simply filters out every message with the word \"Gnome\" in it?\n\n:: But I'd like to know how many people here who have said that GNOME 2's usability is bad etc. have *actually* tried out GNOME 2. \n\nWhy *should* we use it?  In case you were not tipped off when you came in, this is a KDE site.\n\nI have not said anything about Gnome 2, nor have I used it.  Nor do I intend to.  I haven't used Windows ME, nor have I really done more than poke at Mac OSX or Windows XP.  Nor do I intend to.\n\nNo, my mind is not closed, it's just a simple reason:  I'm satisfied with KDE - it lets me do what I want.  Yes, the same reason millions of windows users stay with Windows.  But I have to work for a living, and I don't have the luxury of switching my entire toolset around.\n\nSo I don't really *CARE* about how whizbang Gnome 2 is, how many people use it, how great the applications are, how it'll make you coffee and wake you up with morning fellatio.  This is a frikken KDE news site, not \"KDE versus the world\".  I equally do not care about KDE vs. Windows, KDE vs. MacOS, KDE vs. Amiga, or KDE vs. Lemmy from Motorhead other than in the KDE side of it.  This does not imply anything other than two simple facts:\n\nI use KDE.  This is a KDE site.\n\nAnd if you keep this up, I swear to ghod, I will start comparing Gnome versus the Daystrom Mark VIII trinary computing core - a system that you stand about as much chance to use as I do of using Gnome in the near term.  (And yes, that is the computer system aboard the Enterprise NCC-1701 D in Star Trek: The Next Generation).\n\nAt least that would be an amusing conversation that I have some interest in - and about as much relavancy to this site as the current discussion has.\n\n--\nEvan\n\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "Well...I'm not sure how to respond to this.  You took it in such a...personal way.    I made no reference to you at all.\n\nThese are my points though:\n1) Yes this is a KDE site.  But this does not give *some* (not all) users the right to bash GNOME without expecting someone to refute it.\n2) You may not intend to try out GNOME, but the people who choose to comment on it should.\n3) I'm glad you like KDE.  I never asked you to switch.  I never even compared GNOME and KDE is any *one* of my posts.\n4) You may not care about how KDE interacts with the rest of the community.  There are people who abuse this though (from GNOME and KDE).  I notice KDE users pointing out where GNOME trolls have erred.  I will point the same to KDE trolls.  If you are not trolling - the comments don't apply to you.\n\nYou may not use GNOME (or even want to) but others might.\n\nI do not watch Star Trek, so any conversation of that nature would leave me in the lurch.  Ah well.\n\nAt any rate, since you do not want me here,  I bid you good day."
    author: "Anon Man"
  - subject: "Re: Comment."
    date: 2002-08-01
    body: ":: I do not watch Star Trek, so any conversation of that nature would leave me in the lurch. Ah well.\n\nWhich was my point precisely.  Might I suggest you discuss Gnome on FoodTV.com's forums?  It would be just as appropriate, and less likely to incite flame wars there.\n\n--\nEvan\n"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "> Oh, bloody hell. Is there any possibility we can have a no-gnome-dot.kde.org that simply filters out every message with the word \"Gnome\" in it?\n\nThat would be great.\n\n> Why *should* we use it? In case you were not tipped off when you came in, this is a KDE site.\n\nExactly. We have all already used gnome, enlightenment whatever.\nWe still like KDE more, that's why we use dot.kde.org"
    author: "AI"
  - subject: "Re: Comment."
    date: 2002-08-01
    body: "> I also notice you mention 'pro GNOME' and 'anti KDE' posts. Have you noticed\n> no 'anti GNOME' posts?\n\ni said \"vice versa\" several times in my post, which you have either conveniently ignored or simply conveniently ignored. ;-P\n\nbut the real thrust is that this is a kde discussion board. most of the threads in this discussion have been pretty off-topic for this board. opinions on the latest player transfers in the Premiere League are also off topic here, but there are discussion boards for football where such discussion is on topic. \n\n> What did concern people though was that Evolution was not chosen over KMail.\n> There were genuine questions asked.\n\nfine. ask about it over at Tim's web site (which people did), or on a general interest site like Linux Today (which people did). or, god forbid, email Tim directly (which i'm sure people did). but dragging that conversation in here is off topic. is that so hard to understand?\n\n> I am going to ignore the entire Redhat thread because it has no basis in the > original topic of the post\n\nno, don't ignore it, because that thread is typical and indicative of the entire issue. if you ignore all the stupidity, there is no stupidity. huzzah!"
    author: "Aaron J. Seigo"
  - subject: "Re: Comment."
    date: 2002-08-02
    body: "It was indeed quite stupid from me to post my criticism of the quoted article at this place. I just found it here and because I feel familiar with dot.kde, I posted my commentary here. A mistake.\nWhat I don't understand though is why so many people can offend non-KDE people at this place and rarely anybody (of the KDE regulars) steps in. This place really makes a \"if you don't use KDE, you most probably suck\" impression which is (hopefully) not intended.\n\nI invite some of you people to look by at the new GNOME forum from time to time, I think that would be quite interesting. :) I think (hope) you would find out that there are no bad feelings towards KDE and that _any_ KDE bashing will _not_ be tolerated by the majority of users there (same for GNOME bashing of course). \nWell please don't take this as a troll again, I know that there is no _need_ for any KDE user to spend time at a GNOME forum. :) It's just an invitation. \n\nThe URL BTW is http://help.gnomedesktop.org/forums/"
    author: "Spark"
  - subject: "Re: Comment."
    date: 2002-08-02
    body: "STFU about GNOME already.  GNOME is dead accept it and move on.  If you don't use KDE you're a loser, but that's not my problem."
    author: "ac"
  - subject: "Re: Comment."
    date: 2002-08-02
    body: "Ok, now it's too obvious. You are actually an anti-KDE zealot trying to hurt their image. Gotcha."
    author: "Spark"
  - subject: "Why all this trolling?"
    date: 2002-08-01
    body: "The poor guy who made this review giving a positive impression of KDE gets all the trolling and still keeps explaining his choice ;).\n\nI really don't understand those Gnome trolls: It has been happend a hundred times before: KDE always gets the awards, not Gnome. So this is probably a big conspiracy against Gnome? Or, maybe, the unthinkable has happened and there are more people liking KDE???"
    author: "Ralex"
  - subject: "Re: Why all this trolling?"
    date: 2002-08-01
    body: "<sighs>\nKDE does not *always* get the awards.  If you notice, GAIM and OeOne were awarded.  Those are GTK based...\n\nBeing as I was conversing with Mr. Butler, I should point out that I was honestly confused as to why Evolution was not chosen.  There are maybe two GNOME trolls.  The rest are making non-flaming comments.  There are also a number of KDE users choosing to flame GNOME as well - you do not mention this.\n\nI do not know if more people like KDE.  I cannot state that with any veracity - and neither can you.  But that's neither here nor there."
    author: "Anon Man"
  - subject: "Re: Why all this trolling?"
    date: 2002-08-01
    body: "Gaim is not Gnome. It's just, you said it yourself, \"GTK-based\". BTW, that's, honestly, one of my major problems with Gnome: Every application seems to be \"on its own\", only loosely connected to a not-so-consistant desktop.\n\nConcerning the desktop awards, it's AFAIK usually KDE getting those. I didn't say that in order to troll against Gnome, I just don't understand why Timothy Butler has to defend himself against accusations because of the decision for KDE, if he definitely isn't the first one doing so.\n\nConcerning evolution/kmail, I understand that here you can discuss the decision. But I also have the feeling that evolution is a bit overly hyped at the moment, as it is considered the killer app for Gnome. I, personally, don't like evolution, and I don't see any advantages in comparision to KMail/Korganizer. For my needs, at least (and don't say MS Exchange connectivity...)\n\n"
    author: "Ralex"
  - subject: "Re: Why all this trolling?"
    date: 2002-08-01
    body: "Gaim is Gnome. It can be compiled with Gnome panel support. And it uses the Gnome stock icons and dialogs."
    author: "Stof"
  - subject: "Re: Why all this trolling?"
    date: 2002-08-01
    body: "\"I just don't understand why Timothy Butler has to defend himself against accusations because of the decision for KDE\"\n\nI don't think that anybody has complained against this. Only the choice of Kmail over Evolution and Enlightenment over GNOME (as they aren't even comparable) seemed somewhat questionable."
    author: "Spark"
  - subject: "Stop bashing Enlightenment."
    date: 2002-08-01
    body: "All the GNOMiEs should stop spreading their little anti-Enlightenment trolls, k?  You just can't face up to the fact that Rasterman is doing just fine without you and your kind (ie RedHat)."
    author: "ac"
  - subject: "Yoga and Meditation"
    date: 2003-03-01
    body: "I recommend to everyone who wishes this beautiful website of Meditation, Religion, yoga, philosophy\u0085 I received a lot of inspiration from it hope you will too www.har-tzion.com"
    author: "sheleg33"
  - subject: "Yoga and Meditation"
    date: 2003-03-01
    body: "I recommend to everyone who wishes this beautiful website of Meditation, Religion, yoga, philosophy\u0085 I received a lot of inspiration from it hope you will too www.har-tzion.com"
    author: "sheleg33"
  - subject: "Re: Why all this trolling?"
    date: 2002-08-01
    body: "Honestly, gnome seems pretty boring to me, they are just doing the same as KDE. The only difference is that they are many steps behind. Enlightenment on the other hand, has something different."
    author: "AI"
  - subject: "There are no GNOME trolls"
    date: 2002-08-01
    body: "There are only anti-KDE and anti-GNOME trolls.\nIt's a mistake to count those people as part of the GNOME or KDE community!"
    author: "Stof"
  - subject: "Ximian not GNOME community?"
    date: 2002-08-01
    body: "Then Miguel de Icaza and Ximian are not part of the GNOME community.  They are anti-KDE."
    author: "ac"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-01
    body: "Then why do they support Freedesktop.org? Why does Miguel de Icaza contribute to interoperability standards between GNOME and KDE?"
    author: "Stof"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-01
    body: "Where do they support Freedesktop.org?  What has Miguel de Icaza done?  Their motives could be suspect in this.  It's just an excuse to not support KDE properly."
    author: "ac"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-01
    body: "What about Miguel and Ximian anti-KDE trolls?  That does not count?"
    author: "ac"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-01
    body: "\"It's just an excuse not to support KDE properly\"\n\nActually no.  I suspect its for those hundereds of users who actually do use both DE and don't want to be involved in this flamage.  If they can do something to help them out - I'm all for it.\n\nPlease point these \"Ximian anti-KDE trolls\" out.  I am very interested to see documented proof."
    author: "Anon Me"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-02
    body: "they are not KDE therefore they are ANTI-KDE (thats there reasoning, as stupid as it sounds)\n\n"
    author: "lemon"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-03
    body: "ac/dc is trolling (using FUD from 2 years ago!). We should ignore him."
    author: "Stof"
  - subject: "Re: Ximian not GNOME community?"
    date: 2002-08-03
    body: "Hah, and using your nick from 2 years ago too. "
    author: "Navindra Umanee"
  - subject: "I like both KDE and Gnome!"
    date: 2002-08-05
    body: "Hello:\n   I am happy that both KDE and Gnome exist.\nTo me, they are complement rather than competitors.\nFor example if I really like, many major KDE applications like Knode, Kmail, koffice, I also am very happy of excellent Gnome applications like GTKwave, and maybe also the excellent \"BlueFish\" a very good competitor to Quanta Plus.\nSo congratulation to both teams. I am sure that BIG RED(mont) will love both.\n\"Copy is the most sincere form of admiration.\"\nAndre G-\n\n\n"
    author: "Andre G-"
  - subject: "KDE Rocks!"
    date: 2002-08-07
    body: "KDE is the BEST window manager.\nWhat else it there to say. :)\nKeep up the good work!"
    author: "Jay Williams"
---
<a href="http://www.ofb.biz">Open for Business</a> has held its first annual
<a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=146">
OfB Open Choice Awards
</a> and KDE 3.0 has won in the category 
<strong>Best Desktop Environment</strong>:
<i>"If the KDE Project had been content to stick with KDE 2.x, this may very
well have been GNOME's year to shine. Unfortunately for the younger project, KDE
moved forward at such a rapid pace this year, some people thought the project's
development process might crumble under its own weight. It did not, and KDE 3.0
emerged as the most polished, professional desktop available for Unix and
Unix-like systems."</i>

The KDE mail client <a href="http://kmail.kde.org">KMail</a> won in the category
<strong>Best E-mail Client</strong> and KDE's integrated development environment
<a href="http://www.kdevelop.org">KDevelop</a> won in the category
<strong>Best Development Tool</strong>. 
<a href="http://kopete.kde.org/">Kopete</a>, KDE's multi-protocol messaging client, won an honourable mention in the category
<strong>Best Communications Software</strong>.


<!--break-->
