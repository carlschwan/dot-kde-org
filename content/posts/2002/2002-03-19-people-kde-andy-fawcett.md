---
title: "People of KDE: Andy Fawcett"
date:    2002-03-19
authors:
  - "Inorog"
slug:    people-kde-andy-fawcett
comments:
  - subject: "I know that name..."
    date: 2002-03-19
    body: "You're welcome. Ooh... Andy *Fawcett* :-)\n\nAnother KMail link, that's even easier to remember: http://kmail.kde.org (same contents btw)"
    author: "Andy Goossens"
---
One of the more recent endeavours in the KDE world is the <a href="http://edu.kde.org">KD<em>Edu</em>ware</a> project.  A strong and valiant team is playing hard with this new enterprise. <a href="mailto:tink@kde.org">Tink</a> speaks this week to
a member of this team, <a href="http://www.kde.org/people/andy.html">Andy Fawcett</a>.

<!--break-->
<br><br>
The truth about Andy, unveiled in the interview, is a bit frightening:  he has a bad addiction to computers.  And a harsh medicine to cure it:  <a href="http://devel-home.kde.org/~kmail/index.html">KMail</a> ... but, the more we think about this, the more it feels like <em>d&eacute;j&agrave; vu</em>. We have the chance to start a new week with another great interview from the <a href="http://www.kde.org/people/people.html">People series</a> that leaves us with a happy smile on our faces and with confidence in the future of our work. Thanks Andy, thanks Tink.
