---
title: "Ralf Nolden:  Using KDevelop"
date:    2002-03-07
authors:
  - "Dre"
slug:    ralf-nolden-using-kdevelop
comments:
  - subject: "cute, but rather useless.."
    date: 2002-03-07
    body: "After having tried a number of times to get going with KDevelop I can only say that this article is an other rather useless \"one more introduction\" story.  Useless in a sense that there are enough beginners introductions,  which show screendumps and tell what the features are. All these things can be found on the KDevelop website itself. \n\nWhat WOULD be usefull is if someone who knows the system would sit down and update the REAL tutorials to bring them up to level with the current version of KDevelop. Trying a number of tutorials I havened found one which could guide me step by step to an actualy complete and compilable program. \n\nUsually somewhere halfway they point you to classes that don't exists, options that are no longer there or files that cannot be found. As it seems a lot has been changed. \n\nAnyhow, if someone could prove me wrong with a link to an up-to-date tutorial (KDevelop 2.0.2) I would be more than happy.. "
    author: "Gerhard Hoogterp"
  - subject: "Re: cute, but rather useless.."
    date: 2002-03-08
    body: "Ralf Nolden is a main author of KDevelop, so he knows the system. This is also up-to-date, for he just wrote it. What's wrong, again?"
    author: "Jason Katz-Brown"
  - subject: "And now my 2 Aussie cents..."
    date: 2002-03-08
    body: "I too would just love to get my hands dirty with KDevelop and start pumping out apps for KDE. However, I have been unable to find a KDE programming guide that adequately explains the steps needed to get started. I remember a couple of months ago someone said they couldn\u0092t understand why more people didn\u0092t develop apps for KDE. Well this is the reason girls and boys. It\u0092s near impossible for novices like me to get started.\n\nI\u0092m sure there is hundreds of other people who also need an extra helping hand to just get started. I have no programming experience in GUIs, and my C++ programming experience is still very limited, but the best way to get better is to do it. I would love to spend time developing KDE apps, but if I can\u0092t get started, I\u0092m going to have to look elsewhere.\n\nThis isn\u0092t a rant, and is not meant to be flame bait. It is a simple request. Could someone please write a very detailed tutorial/guide to help clueless people like me get started. I would do it myself, but obviously I can\u0092t :) . My brand new LFS/KDE system will be finished tonight and I want to do some coding with it :(\n\n\nBy the way, I haven\u0092t read this tutorial yet. Hopefully it satisfies everything I\u0092ve requested and you can all call me a n00b :) . I just felt this needed to be said.\n\nThanks all for your time.\n\nJoel\n"
    author: "Joel Carr"
  - subject: "Re: And now my 2 Aussie cents..."
    date: 2002-03-08
    body: "I think Anne-Marie Mahfouf's tutorial is pretty eas to understand.\n<a href=\"http://women.kde.org/projects/coding/tut_kdevelop/\">http://women.kde.org/projects/coding/tut_kdevelop/</a>\n<p>\nCheers,<br>\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: And now my 2 Aussie cents..."
    date: 2002-03-08
    body: "... And my Canadian (well American really but that's irrellevant :) two cents....\nThere is a book available to get started with KDE / Qt programming written by Arthur Griffith titled \"KDE 2 / Qt Programming Bible\".  Although I wouldn't say it is or ever has been a bible for KDE app development it provided a very good start.  I think it would provide a great head start for someone new to c++ as well.  I got it at Chapters but I think it's also available at Amazon.com.\n\nGood luck,\nAllen"
    author: "Allen Lair"
  - subject: "Re: And now my 2 Aussie cents..."
    date: 2002-03-08
    body: "\nThis relates a bit more towards Qt, but since Qt is required to build KDE apps...\n\nI didn't purchace a book, and I only checked out two or three tutorials, but the biggest problem I ran into was the lack of explaination and sample code on how best to run moc, parse .ui files, etc... because coders with experience often have their own conventions about building projects.  I sort of had to poke and prod and semi-reverse engineer how moc handles files, and where it best fits in the build process.\n\nFor me, it wasn't that long, but it kinda annoyed me that the tutorials I read had so little information on the fundimental tools and how they actually work and relate to each other.  It's really quite simple when you do figure out all the ways they work, but it's underdocumented in what I read when I first tried my hand at making a few KDE apps.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: And now my 2 Aussie cents..."
    date: 2002-03-10
    body: "Hi.\nRalf's Tutorial is about KDevelop, not about too many aspects of kdelibs. If you need documentation on the architecture and even some useful tutorials, have a look at http://developer.kde.org and the API-doc generated by kdoc. Of course, this wont save you from lerning C/C++ from some book. But I can asure you, it works this way, but only with work, because it's a complex matter.\n\nCarsten."
    author: "Carsten"
  - subject: "Other language support"
    date: 2002-03-08
    body: "When is someone going to produce a java wrapper for QT and the KDElibraries and when will we see a java plug-in for KDevelop? Not being a C++ coder, this is all a bit beyond my very humble capabilities."
    author: "Ez"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "I heard rumors that someone did make a Java plug-in. Don't know where to find it :-("
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "KDevelop 3 (gideon) will hava java support - see 'kdevelop/parts/javasupport' in the HEAD branch. There is also a java plugin project template, which allows you to write plugins (ie KParts) in Java. The complete KDevelop 'core' plugin api is available to java programmers.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "Will KDevelop ever support other languages besides C++ and Java? "
    author: "sierrathedog04"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "That's funny.  Why don't you check out KDevelop instead of trolling here?  Maybe you'll be surprised to find out you can develop GNOME applications with it too."
    author: "ac"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "As far as I know Gideon currently supports C++ (C?), Fortran, Java, Perl, PHP and Python."
    author: "Schwann"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "And Objective-C too..\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "dcopjava, qtjava and kdejava are in the kdebindings module.\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Other language support"
    date: 2002-03-08
    body: "Maybe take a look at KDE Java Support:<br>\n<a href=\"http://developer.kde.org/language-bindings/java/\">http://developer.kde.org/language-bindings/java/</a>"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Other language support"
    date: 2002-03-09
    body: "OK, cool! It's great that these bindings do exist even of documetation is a little sparse. I'll have to look into how to use them properly. If I understand what I've read correctly, Java support in KDE/KDeveloper isn't as simple as installing KDevelop and adding java classpaths etc. I take it there's no syntax highlighting etc for Java within KDevelop?\n\nStill, the bindings are a very good (and necessary) start. I shall attempt to get set up to explore this a little and continue to wish for java support \"out of the box\" so to speak!"
    author: "Ez"
  - subject: "Re: Other language support"
    date: 2002-03-09
    body: "\"It's great that these bindings do exist even of documetation is a little sparse.\"\n\nThanks, but sorry about the lack of documentation.. One thing thats been added recently are a large number of sample programs. See kdebindings/qtjava/javalib/examples for a good proportion of the Qt example apps translated into Java. Kenneth Pouncey has translated Ralf's KScribble example app - see kdebindings/kdejava/koala/examples/kscribble. I think, even without much documentation, it's much easier to get going if you have some sample code to hack around.\n\nKDevelop 3 (gideon) has both class browsing and syntax highlighting. The plan is to base the development environment on gcj/gdb/automake 1.5, but I've had problems getting gcj 3.0.1 or 3.0.3 to work on my PowerPC machine. Recently Adam Dingle reported on the kde-java list that gcc 3.0.4/gcj work well with the bindings.\n\nKDevelop 1.4/2.x has Java syntax highlighting only.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: Other language support"
    date: 2002-03-09
    body: "pff.. if you know java, c++ shouldn't be too hard to learn\ndomi"
    author: "domi"
  - subject: "license issue"
    date: 2002-03-08
    body: "Hi,\nI read the article. Kdevelop looks like a eally nice ide.\nI'm currently working in a University. The soft I'm doing is private, but it wouldn't be a problem releasing binaries. I was planning to add a gui. My question is: can I add the QT gui to my project without being forced to open source it? (having separate libraries for example and linking them).\nIf this is not possible, can I do it with GTK? and does KDE also support GTK? (like windows support QT, borland, etc.) and, finally, it it doesn't support it: Would it be difficult to add GTK support?\n"
    author: "pep"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "Well, Qt is GPL.\n\nIf your soft is for internal use only, then there is no issue. With the GPL, you must publish your source code only if you distribute the software. If this only for your own work, a soft to be used only by yourself or your departement, then you have nothing to worry about. You should check with a lawyer though if you want to be absolutely sure.\n\nIf you plan on distributing the binaries without the sources, then you must pay a license fee to TrollTech (I think it is 1000$).\n\nAs far as I know, GTK is GPL only, meaning that if you redistribute your binaries, you must also redistribute your source code.\n\nHope this help!"
    author: "Daniel Lemire"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "I heard about people releasing closed sourced GTK products, maybe I'm wrong. What it would be definitely nice is writing in GTK and being able to run it in the KDE enviroment with the same copy/paste, open file dialogs, print infraestructure, look and feel... etc.\nIt would be nice to let the people choose their GUI programming"
    author: "pep"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "Was going to try to verify but http://www.gtk.org appears to be down.\nHowever I'm 99.999999% sure that GTK is LGPL and thus can be linked\nto by closed source programs. \n\nStandards for interoperability would be nice, not sure that it\nhas to go to the lengths you describe. Copy/paste is important,\nopen file dialogs, maybe not so much, as long as each makes sense.\n"
    author: "AdHoc"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "Yeah, Gnome is LGPL. That's why sun chose it.\n\ncheers,\nFrans"
    author: "Frans"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "Poor Sun, what have they gotten themselves into.  They jumped on GNOME bandwagon when it was hot and popular, and now seem to be having nothing but troubles in getting out a release and still haven't replaced CDE.  So much for the LGPL advantage. They have many developers and users who like and asked for KDE/Qt, maybe they should have listened.\n\nWhat will they do when Mono becomes defacto standard on GNOME?  Forget their Java?  Well at least Sun still seem to control some GNOME desktop development for now.\n"
    author: "ac"
  - subject: "Re: license issue"
    date: 2002-03-09
    body: "Power outage at UC Berkeley. I guess somebody noticed."
    author: "Devesh"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "the copy/paste problems are apparently a qt issue, which should be fixed in qt3.  However i've not tried it myself yet."
    author: "dave"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "gtk is lgpl, you can use it for the gui without any problems, without having to release the source. (or you could buy a licence for qt if you need to distribute your app and don't want to gpl it, but using gtk's just less fuss)"
    author: "dave"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "This is pretty bad for KDE I guess.\nIn my case for example, I just wanted to add a GUI as I was told by the University that I can distribute the binaries but not the source.\nOf course (I'm a student) I won't spend $1000 :-(\n\nI guess GNOME wins in that situations. Again, it would be nice being able to program in GTK and that it could integrate into KDE nicely.\n\nAnyway, you can allways use GNOME programs from KDE with X but it's not the same ..."
    author: "pep"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "try another university :)\n\nhonestly, yours is behind the times."
    author: "ac"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "It sounds to me like his university is only too well with the times - they could be protecting their IP rights. I know that anything I do at my university is automatically the property of the college, and they can patent and commercialise it to any degree they wish. There's no way they'll agree to source distribution, especially via the GPL. I suspect this is whats going on in his case."
    author: "Bryan Feeney"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "Right! At least they accepted to give the binaries and I wanted to add the GUI.\nThis is all the story. So KDE or GNOME in my case?"
    author: "pep"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "I really hope I am misunderstanding this, but it seems that you would have more freedom if you developed your code under windoze. Then you have no $1000 to pay, and are not required to distribute your source code. Or am I missing something?"
    author: "Rob"
  - subject: "Re: license issue"
    date: 2002-03-09
    body: "Why don't you separate the main engine from the GUI ? Make two apps: one that works in a terminal and a frontend written in QT/KDE. Then you ask your uni to be able to redistribute your frontend in GPL and the terminal version in binary-only. I think it's legal."
    author: "Julien Olivier"
  - subject: "Re: Windows ends up cheaper and easier"
    date: 2002-03-09
    body: "All the freedoms preached re free open source software would seem to be a misnomer in such cases. It would seem to cost developers more to produce their code under KDE than under Windows. Then they can market their binaries to a much smaller user base. Maybe someone familiar with the GPL could clarify these questions."
    author: "Jimmy"
  - subject: "Re: license issue"
    date: 2002-03-14
    body: "Did you think about the costs of Microsofts Visual Studio, Microsoft Windows license and maybe extra tools? I think you come better off when purchasing the QT licence and sell your binaries..."
    author: "MagMic"
  - subject: "Re: license issue"
    date: 2002-03-14
    body: "I don't think the cost of VC++ plus a copy of winDoze comes close to $1000 USD. And the winDoze market is much larger, so more potential payback too. Let's be honest - for a small one-man start-up company, QT licensing is pretty difficult to swallow."
    author: "Jimmy"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "I'd say to be safe you'd have to use something other than Qt. GTK is okay, but not object-orientated, which might be awkward. GTK-- is the official OOP version, but I've heard it's quite hard to learn. There's another OOP GTK toolkit called INTI being developed at Redhat Advanced Labs, but development seems to have stalled in the last year.\n\nThere is one other alternative called FLTK. It's been _ages_ since I last saw this, but it's a fairly decent easy to learn toolkit, and it's become object orientated in the last few years. I'd try it out first, then go to GTK.\n\nIf you really want to impress, a final option would be to create a GUI shell in Java using Swing and hook it up to your application via JNI, but that all depends on your Java skills. \n\nI'd got with FLTK."
    author: "Bryan Feeney"
  - subject: "Re: license issue"
    date: 2002-03-09
    body: "Just to clear a common misunderstanding (which I had for quite a long time myself): Although GTK is written in C, it is designed in a very object oriented fashion, even featuring oo-types with type checking. \nOf course not everyone likes to use C and macros for OOP (at least I don't :-), but GNOME/GTK really shines in the support of other languages. There are language bindings for a lot of languages, some of them being very good object-oriented languages, like Ruby, Eiffel or Python (and no, I do not count C++ among good OO languages any more, after having experience with C++ and the likes of Smalltalk, Ruby, Eiffel, Python, Cecil and others). \n"
    author: "Thorsten"
  - subject: "Re: license issue"
    date: 2002-03-09
    body: "except that the GTK+ bindings to Smalltalk, Ruby, Eiffel, Python, Cecil and others  are barely used (well, maybe not python as much.. but still). I'd have to say 90% or more of GTK+ development happens in C. The support of other languages is just a advertising point :P\n"
    author: "fault"
  - subject: "fltk + correction about QT license"
    date: 2002-03-14
    body: "fltk is very good and works cross-platform. The mailing list is well populated and very helpful. I think all the other posters are incorrect about having to pay $1000 or distribute sources. According to the Trolltech web site, you may distribute binary-only for free if the application is non-commercial:\nhttp://www.trolltech.com/developer/licensing/noncomm.html\n\nPhillip."
    author: "Phillip"
  - subject: "Re: license issue"
    date: 2002-03-09
    body: "you might want to try wx/windows.."
    author: "fault"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "That's easy, you can release the source to the GUI and not the backend as long as they don't link.\n"
    author: "ac"
  - subject: "Re: license issue"
    date: 2002-03-08
    body: "So, how can I make both programs intract?\nsockets? Can you give me an example? Or some documentation, please?\nThat would be nice, but more difficult to program...\nI guess it's normal that Trolltech doesn't let you do it easily. They developed QT, so they want money back.\nGTK it's maybe more suitable in my case.\nI'll study both options.\n"
    author: "pep"
  - subject: "Re: license issue"
    date: 2002-03-10
    body: "http://kdevelop.org/doc/programming/index-16.html has all the info on licenses.\nKDE is released under the LGPL license so you can develop comercial applications with it without paying for a license."
    author: "bjwbell"
  - subject: "Re: license issue"
    date: 2002-03-12
    body: "As long as you buy a commercial license for QT then you can release a commercial product linked to QT.  Plain and simple."
    author: "Anon E. Mous"
  - subject: "Designer & KDE style"
    date: 2002-03-08
    body: "I looked at the screenshots and noticed that Designer is used with the KDE \"style\". Just wondering, how is this done?\n\n-Kristian"
    author: "Kristian"
  - subject: "Re: Designer & KDE style"
    date: 2002-03-08
    body: "With Qt 2, it is possible to compile in KDE support (./configure -kde).  With Qt 3, the current KDE style is inherited without any recompiling."
    author: "Justin"
  - subject: "Re: Designer & KDE style"
    date: 2002-03-08
    body: "and whereas in Qt2 this was something of a hack, in Qt3 this is now part of the Qt/KDE infrastructure and applies to all Qt apps not just designer. all you need to do in KDE3 is turn on the \"Apply settings to non-KDE apps\" and Qt apps will use the KDE style and colors."
    author: "Aaron J. Seigo"
  - subject: "fine"
    date: 2002-03-14
    body: "I think it would be very useful to read a \"real\" step-by-step tutorial. Like the books bout 'Learning Visual C++' If anybody out there wrote a tutorial 'Learning C++ with Kdevelop'...\n\n... a book with cd: kdevelop on knoppix ready to run...\n\nWhen will KBasic be ready?"
    author: "Hakenuk"
  - subject: "Re: fine"
    date: 2002-11-27
    body: "I think this would be a very good step.\n\nTheres a new kind of programmer out, which want to programm fancy GUI apps only.\nMost of them are working with win32 systems and have not very rich linux/unix skills. I think the would be interessted but the hurdles for them are often to high!\n\nSo please start a knoppix mod with kdevelop ready to run,\nwould be perfect!"
    author: "gebrudergrimm"
---
<a href="http://www.kdevelop.org/">KDevelop</a> guru
<a href="http://www.kde.org/people/ralf.html">Ralf Nolden</a> has
<a href="http://www.linuxjournal.com/article.php?sid=4747">published</a>
a brief tutorial entitled "Developing C/C++ Applications with the KDevelop IDE"
at <a href="http://www.linuxjournal.com/">LinuxJournal</a>.
The tutorial explains how to set up a development environment and
how to create a small sample project, using a simple program to edit environment variables as an example
(<a href="http://www.linuxjournal.com/modules.php?op=modload&name=NS-articles/misc&file=4747l1">code
listing</a>).  The step-by-step guide starts with the cool
Application Wizard
(<a href="http://www.linuxjournal.com/modules/NS-articles/misc/4747f2.png">screenshot</a>),
then describes how to use Qt Designer
(<a href="http://www.linuxjournal.com/modules/NS-articles/misc/4747f3.png">screenshot)</a>,
edit code
(<a href="http://www.linuxjournal.com/modules/NS-articles/misc/4747f4.png">screenshot</a>)
and debug the program
(<a href="http://www.linuxjournal.com/modules/NS-articles/misc/4747f5.png">screenshot</a>).
Really nice introduction to KDevelop, Ralf!

<!--break-->
