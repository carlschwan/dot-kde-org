---
title: "KDE Report At Linux@work 2002"
date:    2002-07-07
authors:
  - "numanee"
slug:    kde-report-linuxwork-2002
comments:
  - subject: "t-shirts"
    date: 2002-07-07
    body: "I want that Crystal KDE t-shirt!!"
    author: "Anonymous"
  - subject: "Re: t-shirts"
    date: 2002-07-07
    body: "I have it :)\n\nRinse"
    author: "rinse"
  - subject: "Re: t-shirts"
    date: 2002-07-07
    body: "where can we get KDE stuff ?\n\nstickers, flyers, shirt, etc. ? \n\nplease do a KDE boutique ;)\n\n"
    author: "somekool"
  - subject: "Re: t-shirts"
    date: 2002-07-07
    body: "Depends on were you are living. \nthe Dutch team is planning on making a online store with kde goodies. \nThink of caps, mousepads, t-shirts mugs etc..\nYou can already buy the t-shirts, caps and mousepads through contacting me. But this only works in The Netherlands. When the store is ready, it is possible that international orders are excepted too, but that depends on the backoffice that is doin the shipping and handling of the orders.\n\nFor kde goodies in other countries, please check the kde site for third party vendors in America and Germany\n\nRinse"
    author: "rinse"
  - subject: "Re: t-shirts"
    date: 2002-07-07
    body: "You will here announce it when it's done, right? :)"
    author: "yatsu"
  - subject: "Re: t-shirts"
    date: 2002-07-08
    body: "yes, please, i am definitely interested too..."
    author: "emmanuel"
  - subject: "Re: t-shirts"
    date: 2002-07-08
    body: "Might be a subject for promo.kde.org ..."
    author: "saildrive"
  - subject: "Re: t-shirts"
    date: 2004-01-28
    body: "please let me know if you can offer me any tshirts for $.60/each.  I would like to purchase 200 quantities.\n\nthanks\nHanh\n"
    author: "Hanh Nguyen"
  - subject: "Re: t-shirts"
    date: 2004-01-28
    body: "You mean $0.60 / t shirt?"
    author: "rinse"
  - subject: "Okay"
    date: 2002-07-07
    body: "Okay :)\n\nRinse"
    author: "rinse"
  - subject: "Dutch and Belgian KDE-users ...."
    date: 2002-07-07
    body: "I hope some Belgian en Dutch KDE-users would like to help to promote the (Dutch) KDE desktop. Everybody can help here, you don't need special skills like programming. Just some enthousiasme and the love for the KDE-desktop. There is a special website with information about organising attendance for these events .. http://events.kde.org \n\nAlso if there are events you think KDE-nl should attend .. then please contact us at <b>promo@kde.nl</b>\n\ntake care'\n\nFab"
    author: "Fab"
  - subject: "Re: Dutch and Belgian KDE-users ...."
    date: 2002-07-07
    body: "Oooh, a checklist :-)\n\n* Belgian... OK\n* promote the (Dutch) KDE desktop... do it all the time, but not at events (yet)\n* programming skill... more or less :-)\n* i18n(\"enthousiasme\") + love... have it\n* http://events.kde.org... I'm even in their ACL ;-)\n\nHmmm... to bad there isn't a KDE-be group."
    author: "Andy Goossens"
  - subject: "Photos"
    date: 2002-07-07
    body: "Could please someone teach the photographer a lesson?\nI really don't think that this kind of amateur stuff is good publicity for KDE.\nPeople should get a license to photograph before they can buy a digital cam.\n\nDon't get me wrong, it sure is a good article, but the photos are too bad to be put on the net."
    author: "grovel"
  - subject: "Re: Photos"
    date: 2002-07-07
    body: "grovel,\n\nyou are right that the pics are of low-quality, but I can assure you it has nothing to do with my skills as a photographer.We didn't use a digital camera and all pics were scanned afterwards by a helpful person. \n\nSo it was either nothong (no pics) or these pics. \n\nThough I think the article in general is surely good publicity for KDE. \n\n\ntake care'\n\nFab"
    author: "Fab"
  - subject: "Re: Photos"
    date: 2002-07-07
    body: "It's a community effort which we support, being the community, and I think the photos are quite alright.  Who are they going to harm exactly?  The photos are for us, let's enjoy them.\n\nPlus, let's be realistic.  If these are the standards for posting KDE photos, then nothing will ever be posted on the dot...\n"
    author: "Navindra Umanee"
  - subject: "Re: Photos"
    date: 2002-07-08
    body: "I feel like this has been a common them lately, but here goes anyway:\n\n> Could please someone teach the photographer a lesson?\n\nSure.  When will you be meeting him to \"teach him a lesson\".  Better yet, why weren't you there to take the pictures yourself?  Will you be hiring a professional photographer for the next KDE event?  Or maybe providing a nicer camera?\n\n> I really don't think that this kind of amateur stuff is good publicity for \n> KDE.\n\nBecause?  I'm sure we lost half of our users today, just because of those photos.  You know I don't think all of these \"amateur\" programmers are good for KDE.  When is KDE going to start hiring professionals?!?  Oh, wait.\n\n> People should get a license to photograph before they can buy a digital cam.\n\nPeople should not complain that aren't willing to help.  \n\n> Don't get me wrong, it sure is a good article, but the photos are too bad to \n> be put on the net.\n\nDude, *nothing* is too bad to be put on the net.  I can post lots of examples.    \n:-)\n\nAnd sorry, I know this is a bit inflametory, but you just insulted people that spent time and money to do something great for KDE.  Constructive criticism is appreciated; help is appreciated; insults are not."
    author: "Scott"
---
<a href="http://www.kde.nl/">KDE-nl</a> was present at <a href="http://www.ltt.de/linux-at-work.2002/">Linux@work 2002</a> last June and they've written up an <a href="http://www.kde.org/international/netherlands/events/linux-at-work-2002/linux@work2002_e.html">nice bilingual report</a> on the event, complete with annotated photos.  <i>"In the morning at 8.00 AM we arrived at the Radisson Hotel. Fabrice and Rob already arranged the booth(table): at the wall you could find a poster of Konqi in A0-format, on the table there was one laptop, some Linux and KDE mascottes, KDE-flyers, a SuSE 8.0 box and a giant Tux. We also had a bust for displaying T-shirts. The laptop showed an installed SuSE 8.0 with the standard out-of-the-box KDE 3.0-desktop along with KOffice 1.2-beta1."</i>  Nice job, guys.
<!--break-->
