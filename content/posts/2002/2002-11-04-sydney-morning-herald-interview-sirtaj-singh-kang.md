---
title: "Sydney Morning Herald:  Interview with Sirtaj Singh Kang"
date:    2002-11-04
authors:
  - "Dre"
slug:    sydney-morning-herald-interview-sirtaj-singh-kang
comments:
  - subject: "Don't look at me, I voted for Kodos!"
    date: 2002-11-04
    body: "You know, just the mandatory Simpsons joke.\n\nAnyway, Taj rules, even if he liked perl way back then ;-)"
    author: "Roberto Alsina"
  - subject: "Re: Don't look at me, I voted for Kodos!"
    date: 2002-11-04
    body: "Bob old pal, if I had 20% of the clever lines that Commander Kang has I'd be growing extra limbs and eating more spinach in a shot.\n\nUnfortunately I have to write all my own lines....\n\n(btw tronical will vouch that this mystery python project does atleast exist)"
    author: "taj"
  - subject: "Re: Don't look at me, I voted for Kodos!"
    date: 2002-11-05
    body: "<aol>m2</aol> :-)"
    author: "Inorog"
---
Sirtaj Singh Kang, better known to many of us as "Taj", joined the KDE
project as a developer early in its history (read his People interview
<a href="http://www.kde.org/people/taj.html">here</a>).  He was recently
<a href="http://smh.com.au/articles/2002/11/01/1036027033828.html">interviewed</a>
by <a href="http://smh.com.au/">The Sydney Morning Herald Online</a>.
Interview topics range from why get involved in Open Source (<em>I work on
free software in my free time. Like the car mechanics who work on their own
hotrods in their spare time, it is in my professional interest to
understand and improve on my available toolset. It makes me a more
marketable employee.</em>) to KDE history and development to
Linux distributions.
<!--break-->
