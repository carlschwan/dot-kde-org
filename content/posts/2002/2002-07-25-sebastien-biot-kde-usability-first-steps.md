---
title: "Sebastien Biot: KDE Usability - First Steps"
date:    2002-07-25
authors:
  - "numanee"
slug:    sebastien-biot-kde-usability-first-steps
comments:
  - subject: "double-clicking and other quirks"
    date: 2002-07-25
    body: "I have to second the findings of the usability test.\nWhen I first used KDE 3.x, the single-clicking thing also \nthrew me.  It should be double-clicking by default.\n\nSecond thing that threw me was that when the mouse passed over a desktop icon, \nthe icon changed to an \"active\" state.  I generally have about 5-10 icons on the \ndesktop.  The effect is that everytime I move the mouse the entire screen is jumping with various changes.  This behaviour should also be turned off by default not because it wastes cycles, which it does, but because it makes the desktop non-estetic. \n\nAnd my last pet pieve (it didn't show up in the study, because users were not asked to perform it) is clipboard.  Ctrl-V/Ctrl-C works - that's great.  But it is worthless if everytime you select something, the clipboard gets overwritten.  I still haven't found a way to turn off this ridiculous behaviour (am I missing something?).  Sometimes I want to highlight a url somewhere and paste it into Konq, so I highlight the URL, then I try to highlight the URL in the address bar, so that I can overwrite it with the url I selected, but the clipboard just got whacked.  Anyway, there should be an option to turn off the X clipboard behaviour, because it is simply non-intuitive."
    author: "Frank Rizzo"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "<blockquote>And my last pet pieve (it didn't show up in the study, because users were not asked to perform it) is clipboard. Ctrl-V/Ctrl-C works - that's great. But it is worthless if everytime you select something, the clipboard gets overwritten. I still haven't found a way to turn off this ridiculous behaviour (am I missing something?).</blockquote>\n\nYes, you are. Did you notice the clipboard (Klipper) in the system tray? When you click on it, it presents you with a clipboard history."
    author: "Jakob Kosowski"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Not to mention that this has been fixed in KDE 3.  Selecting something and then pressing Ctrl-V does produce the desired effect.  The only way you could have problems now is if you tried to mix the two clipboards:  if you press Ctrl-C and then middle-click, or if you select something and try to paste it with Ctrl-V."
    author: "not me"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Oops, I stand corrected.  Clipboard actually does work.  Sorry, I was thinking about a different workstation which has an earlier rev of kde installed."
    author: "Frank Rizzo"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "> I have to second the findings of the usability test.\n When I first used KDE 3.x, the single-clicking thing also \nthrew me. It should be double-clicking by default.\n\nWRONG!!! I can't stress enough how much this is a terrible idea!!! Just because thousands of lemmings run off a cliff doesn't mean it is a good idea. Where did double clicking come from? Apple advanced the GUI they got from Xerox with a single button mouse. This pretty much indicated double clicking since a second button was not optional. I too found single clicking strange when I first started using KDE. However within a few weeks I found double clicking way more obtuse. Consider that it is not at all uncommon for people to suffer from repetative stress disorders, in particular carpal tunnel syndrome induced by double clicking on a mouse. This is a very real thing. Your chances of having real and painful medical problems after decades of double clicking are very good. They put warnings on cigarette packs about heart and lung disease. There ought to be one on your mouse about double clicking. A quick search on carpal tunnel syndrome and double clicking reveals plenty of activity including expensive ergonomic mouses that have a double click button so that people can use windoze without having to double click. I for one would feel ill if I thought KDE users were buying them because they did not know that a free software solution for the insanity of double clicking was there already.\n\nKDE should be saluted for doing the right thing... even though some people find it inconvenient to be open minded and realize the it is more productive, logical and less injurious to click once when the only need to click twice is the one button mouse and the morons in Redmond who never had a fresh idea or any sense. It seems to me if you are coming to KDE from windoze you will find yourself rebooting less and clicking less... both of which will make you more productive.\n\nGod help us if we allow \"usability\" to pander to the worst inclinations and stupidist conventions. Sometimes it's worth taking the heat for being right!"
    author: "Eric Laffoon"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "I agree 100%.. Single clicking is the way to go. When you browse internet, do you double click on links? When I first started using KDE, even I didn't like single clicking.. but then it felt so natural after a while. The only time I don't like single clicking is when I am in file browsing.\n\nAnother observation I would like to put forth is that of my father. He used Windoze till now but moved to Linux/KDE (KDX!) recently. He is not a techie person.. a complete newbie. After a while, he loved single clicking. When I asked him if I should change snigle click to double click like in windoze, he said no.. he likes it this way.. he said he got irritated at times with the windoze doubleclick.. mainly because he had to click within that short time else it wouldn't click!\n\nSarang"
    author: "sarang"
  - subject: "And even windows does it now..."
    date: 2002-07-26
    body: "I find single cliking much better, but you have to get used to it. About a week and you don't want to go back. Only problen is at work, me sitting and waiting but nothing happens, darn NT.\n\nOverheard 2 coworkers talking, both had installed win2k and they agreed on one thing, the best ting sice sliced bread was the abillity to set it to single clik. \n\n"
    author: "Morty"
  - subject: "Double-Clicking?"
    date: 2002-07-26
    body: "I agree completely. If KDE adopts this notion, maybe we can also get them to drop the metric system:)  The Windows 95 interface was so badly done that it spawned several well known usabilty \"Hall of Shame\" sites. Everytime I see Windows-type mistakes repeated in KDE it makes me boil. Add this to your usability test: Special=Inconsistent=Bad. I single click a registered mime type on the KDE desktop to open it with my preferred application, but have to middle click the same file in Konqueror. One or the other should be the default behavior, but not both.\n\nA mouse is a pointing device, by default that's the way it should behave. I work in industrial automation. It's typical now to use industrial touchscreens to provide graphical operator interfaces. Many times these are simply an X86 PC in a special form factor, with the touchscreen connected and configured as a serial mouse. I've never seen an instance where anyone \"intuitively\" attempted to double click a graphical control panel touch object. The same applies to kiosks or automatic teller machines - Mac and Windows users seem to grasp how to operate those machines at a glance.\n\nThe faster we make PC's work like everything else in the real world, the better off we'll be. Most of us don't double click to start a car, turn on the living room lights, and etc. ad infinitum. I don't want to click a desktop icon two or three times (to give it focus and to launch the associated application). \n\nShould KDE make that the default behavior based on some vague claim of \"improved usability\" without first asking for a rational explanation of how it improves the functional utility of everyone's desktop?\n"
    author: "Harlan Wilkerson"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "I'm all for single clicking, except that it is kinda frustrating to figure out how to simply select something for a change.\n\nThe automatic select cannot read my mind, if it is not fast enough, then it is not fast enough.  Then other times it is too fast, or it can be a bit \"sticky\" in which everywhere I move my mouse, it follows, and messes up a bunch of selects.\n\nWhat I'd like is...if there was a way to simply select something.  And I would like it if it could be done on a click.  So far, the mapping is: left opens, middle open in new window, and right opens the menu.  Can I win something here?  How about this: in single click, if the menu is open, and I click on the same icon I opened the menu on, it should leave it selected, period!  I not care if the mouse moved ever so little off the menu, select the thing, not open it!  I know that is 2 clicks, but I can live with that.  It would be nicer if a double click would select for a change.  If you are in single click mode, isn't it intuitive to assume that there was a role reversal in which select is now double click?  I don't know exactly how the GUI thing handles events, but if it is possible to reverse the role, then that would be nice.  Have a checkbox under the configuration for single clicking saying \"Double click selects\"."
    author: "Tim Vail"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Ctrl-click selects a file."
    author: "Darren Winsper"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-27
    body: "\nSo to select file(s) with single click I have to use 2 hands.  That sucks."
    author: "John"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-27
    body: "No, you just point to it. Right mouse button gives you the option. If you want to move it, just press left mouse button and don't release it until you draged whatever you are dragging above the target. Drop.\n\nThat's it."
    author: "Marko Samastur"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "There is a guy who even wrote a mousedriver which works without any clicks. Could this be done in KDE too?\n\nhttp://www.intern.de/97/25/24.shtml\n\n\nStefan"
    author: "Stefan Heimers"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Already done: http://accessibility.kde.org/kmousetool.html"
    author: "Anonymous"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Full ACK!"
    author: "Andreas"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Yes,\n\"better usability\" doesn't mean \"no evolution\".\n\nThe simple click is a good evolution because:\n->easier to learn\n  (beginners move the mouse when \"double clicking\" \n  =>so windows move the icon while they want to open an application, ...)\n->better consistancy \n  (why a simple clic open a menu \n  and I need double clic to open a folder in windows)\n->less effort \n\nThanks for the simple clic\n\nregards\n  "
    author: "thil"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-30
    body: "I agree, too.\n\nActually, the whole thing is a non-issue anyway because a new KDE-user should see the wizard where you can choose a look-and-feel right in the beginning, and all users who think that think Windows is the best can choose Windows right there, with double-clicking and all.\n\nKDE is a great desktop, while I certainly acknowledge that there is room for improvement (there always is), please don't do something just because \"it's in Windows\"."
    author: "RdC"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-10-30
    body: "wow, and i thought that the mac vs pc war was gross....\n\ni can't believe you can actually start an overheat discussion on double-clicking vs. single-clicking."
    author: "wza"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Note that he didn't ask the User how they felt about it.  I'm pretty sure if he explained what was happening and why the user would soon feel at home.  My dad for one lives quite well with the single-clicking.\n"
    author: "Navindra Umanee"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "And we come to the central matter: making users _know_. I.e., given the absolute truth that users don't read documentation unless their lives depend on it (it always does, but we, users, are crazy risk freaks :-/   ), we should find another way to make people know.\n\nOne thing would be a mandatory tutorial at the first use of the computer. This won't trap users that start to use an already configured/existing account.\n\nAnother thing would be to have \"intelligent assistants\" (hello, Microsoft) that would detect \"erroneous\" double-click usage etc.\n\nBad huh? But don't despair. There is a probable solution, and it's even as old as unix/X, probably. Fire up the \"xfig\" program (you don't have it installed? Shame on you!). And look in the top right corner. Move your mouse cursor over different elements of the display. Choose tools from the palette. You'll see there, in the top right corner, a small mouse buttons depiction, with explanations.\n\nNow, imagine we would add a general mechanism that would allow, at user's demand, the display of a panel on the desktop (or in the kicker) that would list the actions associated with the mouse (and perhaps with the keyboard buttons too), depending on the element of the desktop over which the mouse cursor is, and/or the element which has the focus.\n\nThis technique would also solve another usability problem mentioned: opening documents _for viewing only_ in the konq-embedded viewers. Well, the above technique would show that middle-clicking on a file opens it in a full-blown editor.\n\nNice huh?"
    author: "Inorog"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "> When I first used KDE 3.x, the single-clicking\n> thing also threw me. It should be\n> double-clicking by default.\n\nI think single clicking makes more sense.  Why should your bias have more weight than mine?\n\n> everytime you select something, the clipboard\n> gets overwritten. I still haven't found a way\n> to turn off this ridiculous behaviour\n\nSome of us like the normal X behavior.  Anyhow, as far as I know, your painful windows/mac way works fine in KDE 3."
    author: "KDE User"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "I Agree! I like single clicking I'm used to it!\nI like X clipboard / clipboard history combination, it's good and I'm used to it. Btw. in konqueror there is the cross button to clear the URL text box. And in all text boxes right click and \"Clear\" deletes the text. That's as efficient as selecting it and pressing \"Del\" IMHO. "
    author: "David Siska"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "IMHO the preference wizard should make clear to users who are used to Windows that they use the Window profile. Maybe it would be right to preselect the profile,  many people dont read any text. But it is also important to make the KDE settings default for people without Windows (or MacOS) exposure, because double clicks are very complicated for newbies."
    author: "Tim Jansen"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: ">I have to second the findings of the usability test.\n When I first used KDE 3.x, the single-clicking thing also \nthrew me. It should be double-clicking by default.\n\n\nNo way ... Double Click is for other world not for ours. Yes is good to configure this double click , triple click etc but not by default.\n"
    author: "sacx"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Just because windows does it this way doesn't necessaryly mean that it's better.\nBut in this case I think you are right. The users have already made their choise.\n\nEven Microsoft tried to introduce single click as part of their active desktop, but \nfailed miserably. Almost all users turned it off. My guess is that users feel that\nthey want the double click to make sure that they don't do anything by mistake,\nas you use clicks to select and double click to perfom an action.\n\nAnother good reason is that there are so many windows and mac boxes out there\nall configured with double click and chances are that KDE users will use one\nof those as well as KDE. In this case a different click behaviour would be\nconfusing.\n\n\nThere is however one place in KDE that not should have double click,\nand this is the tree in the \"Control Center\" where you select controls.\n\nYou should not need to double click here. This is a selection where you\nonly can select one control at the time. It should work like the tree\nview of the filemanager."
    author: "Uno Engborg"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-27
    body: "So much about consistency."
    author: "Marko Samastur"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-27
    body: "I personally like single clicks, because double clicking requires too much effort when you actually use mouse to do things. The only reasonable reason for double clicks I can think of is that double clicking was invented before multiple buttons on the mouse. We should not hang on to this medieval habit."
    author: "Maris Darbonis"
  - subject: "Advantages of double-click"
    date: 2002-07-29
    body: "The annoying thing about having single-click by default is that it makes the following actions harder to do:\n\n* Selecting multiple items. Generally, I want to select one icon as an \"anchor\" and then use Shift-Click or Ctrl-Click to extend my selection.\n\n* Right-clicking. Generally, I want to select an icon before right-clicking it, just to be sure I'm working on the correct icon.\n"
    author: "J. J. Ramsey"
  - subject: "Re: Advantages of double-click"
    date: 2002-07-29
    body: "\"Selecting multiple items. Generally, I want to select one icon as an \"anchor\" and then use Shift-Click or Ctrl-Click to extend my selection.\"\n\nstart with ctrl than start clicking...\n\n\"Right-clicking. Generally, I want to select an icon before right-clicking it, just to be sure I'm working on the correct icon.\"\n\nAn icon is highlighted before you right click it, how sure do you wan't to be?"
    author: "Gert-Jan van der Heiden"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-29
    body: "I don't know why people like the C-c, C-v stuff so much.  I think the X mechanism\nis so much better and simpler."
    author: "TCorbin"
  - subject: "mozilla double clicking"
    date: 2002-07-26
    body: "The author mentions that applications like Mozilla continue the double-clicking trend. This is a very bad example in my opinion.\n\nThere's nothing to double click in the browser, and in the mailer, the only thing I can see is to open up a mail message in its own window instead of using the preview pane. \n\nI do agree with his point though, I see people double clicking on hyperlinks all the time ... ugh ...\n"
    author: "jorge o. castro"
  - subject: "Can't KDE just IGNORE the second click?"
    date: 2002-07-29
    body: "The problem with the KDE default single-click mode is that the novice user that's accustomed to double-clicking everything ends up with 2 launches - which is exacerbated when launching is slow and they double-click again.\n\nCan't KDE (or QT, or whatever is handling mouse clicks) just be set to ignore a second click if it comes within the double-click time interval?  That way, single-clicking would still work, and double-clicking wouldn't hurt anything."
    author: "Rob"
  - subject: "*My* usability wish list "
    date: 2002-07-26
    body: "(or I have actually no big complaint appart from the control center)\n\n* Redesign the kde control center. I have often to use the search ability to find all the places that, for example, deal with fonts.\n\n* Improve the defaults for new users. \n\n* A window maker clip utility, bringing up a nice app launcher menu specific for wach desktop.\n\n* Make some of the window decorations have good size (or configurable size) grab bars. Glow, for example  is very poor in this issue.\n\n* Better color support for reverse color schemes. Some apps assume that the background is always some sort of white and the fonts some sort of black. konqueror should have independent link color settings.\n\n* The possibility to embed the 'desktop preview' pager into the panel. I'd really like to see the icons of the apps running in each desktop. I had kpager in KDE1 working very well with auto-hiding panel and tasklist.\n\n* Seed up the hide/unhide of the external task list.\n\nI have been using KDE since one of the betas of the 1.x series, so i'm not really representative for any usability study. :) \n\n"
    author: "TH"
  - subject: "Re: *My* usability wish list "
    date: 2002-07-26
    body: "I believe Charles pushed some changes to the KC in CVS.  I haven't checked it out yet, but it sounded much better than the previous layout."
    author: "Navindra Umanee"
  - subject: "Re: *My* usability wish list "
    date: 2002-07-26
    body: "Why not a \"user mode\" menu entry en KC?\n\nSomething in the line of Expert, (Intermediate?) and beginner?\n\nIn beginner mode, KC would boil down to a slide bar for eye candy settings, together with basic look and feel (and mouse) config options. Probably also a dropdown menu for styles and icons.\n\nTH\n\nPS: Why not a spell checker for these konq edit boxes :o)"
    author: "TH"
  - subject: "Re: *My* usability wish list "
    date: 2002-07-27
    body: "i understand how appealing the concept of user levels are when you don't really bother to think much about it, but trust me: the idea is braindamage of the first degree, which is why it is condemned by most usability experts and not implemented very often at all. why, you ask?\n\nfirst off, users are not uniformly \"expert\" or \"novice\" or whatever.\n\nthey may know how to use the spell checker  very, very well but know next to nothing about using tab stops. so are they an expert or a novice? or somewhere in between? reality is that they are an expert spell checker and a novice tab stop user. which means you really need these settings for each different major aspect of a program, which is obviously unworkable (you can end up with more user level settings than settings themselves!) ...user-levels only \"work\" when they are fine grained, which sort of defeats the purpose.\n\nsecond, users are horrible at determining their own user level. few get it right, especially when you ask them how well they know something when they may not understand what you are asking them about.\n\nthird, interfaces that change are the devil. they render past learned lessons less reliable and useful. so if someone uses the user-level system, this only extends the learning process and makes the system seem perpetually uncomfortable or else they never progress to being anything but a novice. in which case, why have any features that aren't novice features?\n\nfortunately user levels aren't really needed anyways. good defaults, well designed UIs and a measure of restraint on the feature count and feature duplication works quite well."
    author: "Aaron J. Seigo"
  - subject: "Re: *My* usability wish list "
    date: 2002-07-28
    body: "The GNOME people already tried this \"user mode\" thing before, both in Nautilus and in Sawfish control center. But they both failed, everybody (users & developers) thought it was a bad idea, and eventually they removed it."
    author: "Stof"
  - subject: "Re: *My* usability wish list "
    date: 2002-08-01
    body: "> * The possibility to embed the 'desktop preview' pager into the panel.\n> I'd really like to see the icons of the apps running in each desktop.\n> I had kpager in KDE1 working very well with auto-hiding panel and tasklist.\n\nHave you tried Kazbar? It's a panel applet (or extension or whatever they're called this week) that does pretty much what you describe here.\n"
    author: "Ross Smith"
  - subject: "Re: *My* usability wish list "
    date: 2002-08-02
    body: "It's not the sanme thing. \n\nKasbar is like a iconbox, it lists the icons of the apps currently running. What I am refering to is a kpager-like app that could be embeded in the kicker window, with icon and pixmap preview and window drag and drop between dekstops."
    author: "TH"
  - subject: "Opening tar archives in konqueror"
    date: 2002-07-26
    body: "First off, I'd like to point out that I respect the developers who developed some nice archive readers like ark.  However, if I was to statistically observe my own web browsing behavior, I'd say that pretty much 99+% of the time that I click on a tar.bz2, tar.gz, or even tgz files, I'm looking for a download, NOT for opening up the file to read it!\n\nThat is precisely why ark has became an pet peeve for me.  And it is a pretty annoying one because almost every time I click on a tar.bz2 file, I'd say to myself \"oh no, now I need to press backspace and then use right click to save as!\"\n\nThe quick solution to this problem, in my mind, is to make saving those types of archive files the default, and perhaps have ark (Archiver) as one of the top program in the \"open with\" for those users who love to look at their tar files (honestly, it beats me).  I think kdownload is going in the right direction here.\n\nThe ideal solution in my view is pretty much what Microsoft, like it or not, does.  I'm not a big Microsoft fan, but they seem to allow you to make different actions the default, like view, open, etc.  What we could do is, we could have \"save\" as default, and then open with ark as another good option.  If someone wants to change that, all they have to do is go through the file association properties and change it there.\n\nOf course, those opinions are not representative of a new user, but I think a new user would prefer if those tar.bz2 files were saved, not viewed -- or even better, just give them a dialog box that gives them the option of doing any of those actions."
    author: "Tim Vail"
  - subject: "Re: Opening tar archives in konqueror"
    date: 2002-07-26
    body: "I agree completely! I *always* download compressed files, or rpms, or what-have-you, then go to a command line and extract/install/whatever. For a complete GUI solution I suppose you could use Ark, though I find \"tar zxvf file.tar.gz\" or \"tar jxvf file.tar.bz2\" to be infinitely faster and simpler. And for rpm's, I've never encountered an intuitive rpm GUI (especially now that I'm using Mandrake, and can just type \"urpmi file.rpm\"..... heh heh heh.\n\nBut, I would say that a default of download makes *much* more sense, as its (like it or not) the default behaviour of Windoze. Besides, what good is viewing the contents of an archive on a remote computer? (well, in a temp file, but that's how it appears to the user). Also, in file-browsing mode, the whole archive-as-a-pseudo-directory is useless: 9 times out of 10 (or even 9 :) whatever is in the archive will not work in a pseudo-directory type thing. The best thing, probably, would be to have the default left-click option in filebrowsing mode to open Ark (or even be the \"Extract Here\" option from the context menu)\n\nThat should be all\n\nMatt"
    author: "Matthew Kay"
  - subject: "Re: Opening tar archives in konqueror"
    date: 2002-07-26
    body: "I think consistency means that left-clicking a link should always open it in the preferred application, or ask what to do if there is none.\n\nBy the way: try holding down the Shift key while clicking on a link."
    author: "Morten Hustveit"
  - subject: "Re: Opening tar archives in konqueror"
    date: 2002-07-27
    body: "Sure.  Have you ever tried shift-clicking on a link that redirects to another link that downloads the archive file?  That is where the REAL annoyance is.\n\nBecause that way it is very difficult for me to catch the link in order to right click to save it as something.\n\nJust wondering, does shift-click solve that problem?\n\nTim Vail"
    author: "Tim Vail"
  - subject: "Re: Opening tar archives in konqueror"
    date: 2002-07-26
    body: "That's why you should use the side bar in Konqueror. You drag the tarbol/rpm\nto a directory. Konqueror then pops up a thingy asking if you want to\ncopy, link etc the file. This is what I use all the time. If I need to\ntake a look at the contents of the tarbol, I can click on it. Konqueror makes\nmy life so easy... And no double clicking!"
    author: "ne..."
  - subject: "Re: Opening tar archives in konqueror"
    date: 2002-08-31
    body: "OK, to take a concrete example, if this is such an easy thing to deal with, would someone please tell me how, using konqueror, to get dvr-2.7.9.tar.gz off of sourceforge?  When I tried any of the methods that have been suggested in this discussion, I got a 10k file that was not a valid archive.  The actual size of the archive is more like 2.7 meg.  And when I finally removed x-tgz from the file associations, that is in fact what downloaded."
    author: "John Clarke"
  - subject: "Re: Opening tar archives in konqueror"
    date: 2002-08-31
    body: "You click on the link, a page is displayed with a number of mirror sites - you've been downloading that page.  The key here is *not* to right click and \"Save Target As...\", as the link to the file on Sourceforge is no longer a link to the file, but instead a link to the page of mirrors.  \n\nWait a few seconds, and you will automatically, without clicking anything, be presented with a dialogue box asking you to save or view.  If you don't want that, hit stop as soon as the mirror page appears, and there will be a link at the top of the page, saying \"If your download doesn not start automatically in a couple of seconds, you can manually download the file from...\", and a URL.  You can right click on *this* URL, and choose Save Link As....  If you are not on a page with a bunch of corporate banners and country flags all over the place, don't right click.\n\nI agree - the mirror system is a PITA, and I've downloaded that 10k file (which is the html page of mirrors, btw) several times myself, as you expect that link with the file name to be the link to the file - but it isn't.  If it's any consolation, you're not the only one, and Konqueror users aren't the only ones having issues with the new SF file system.\n\n--\nEvan "
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Opening tar archives in konqueror"
    date: 2003-08-11
    body: "That doesn't seem to be a correct way to me.\nIn fact, I like the bahaviour of konqueror when dealing with file I have on my local disk.\nRegarding archives that I want from the net, the behaviour should be always to download. \nAnyone knows how to set this kind of behaviour ?\n\n"
    author: "Marcel Janssen"
  - subject: "Press enter to go to the start of next line"
    date: 2002-07-26
    body: "After one hundred years of using typewriters, people were used to pressing enter at the end of the line, when they first encountered word processors. In general they learn(ed) pretty quickly not to do that and I doubt that anyone in the right mind would ask that we all change to the previous behaviour.\n\nI have a mother who had a fairly limited exposure to Windows before I put Linux  with KDE on her machine. From the start she didn't have any problems using single click and is in general quite happy with how the things are working. Or in her own words:\"This is much easier to use than Windows!\"\n\nSure, there are things that need to be fixed and article exposes a lot of them, but I'm not in favor of forcing next generations to learn old behavior just because it would save a bit of learning to the current generation of computer users. Obviously I BELIEVE that single clicking like word processor example is clearly superior and worth the trouble of learning it for those who got bad habits in other environments.\n\nIf anything, I'd make the change to double-clicking easy for users, but default should be sensible, not entrenched."
    author: "Marko Samastur"
  - subject: "Re: Press enter to go to the start of next line"
    date: 2002-07-27
    body: "the only thing I know : new KDE and Linux users always have difficulties with the single-click at the beginning. But, then, they change their Windows preferences to single-click... I see that beheviour numerous time. So, I think it's not a real favour to make double-click by default."
    author: "Capit Igloo"
  - subject: "Re: double-clicking and other quirks"
    date: 2002-07-26
    body: "Yes.  Double clicking is a bad idea used in Windows. single click in Windows selects, something less used than double click. so single click by default is the way it should go.\n\nKDE people add brilliance to the common functionalities. The drag and drop pop-up menu, for example. And the split window facilty that makes drag and drop cool to do, the multisession tabs in Konsole.. thanks to the developers"
    author: "Asokan K"
  - subject: "This is not really a usability study"
    date: 2002-07-26
    body: "Just to make sure everybody understands: this can hardly be called a usability study.\n\nRather, it's a test of how well KDE conforms to what the user is used to.\n\nKDE could have the worst UI in existence, but if these users had been used to that UI, they'd have done better than they did.\n\nNo, user familiarity with a given interface shouldn't be discounted. But, yes, just because they're used to something doesn't mean it's better.\n\nTab-completion, for instance, has got to be the closest any user interface has come to reading a user's mind. If there was some way to work that into a UI ... :)"
    author: "David B. Harris"
  - subject: "Re: This is not really a usability study"
    date: 2002-07-28
    body: "Mozilla just got a new feature: link-text autocomplete. Start typing the text of the link you want to go to, and it progressively looks through the document to match a link text to the stuff you're typing. That's neat :)\n"
    author: "richard"
  - subject: "Re: This is not really a usability study"
    date: 2002-07-28
    body: "Maybe soon we will see totally new concepts of the desktop\nor user interface for KDE, based upon everything else that is\nalso KDE."
    author: "A Ivarsson"
  - subject: "Windows vs Unix behavior"
    date: 2002-07-26
    body: "Wouldn't it be possible to ask the user during the installation whether (s)he wants a Windows like or an Unix like environment.  \n\nWindows like would mean:\n- double-clicking behavior\n- login as root by default\n- Ctrl-V/Ctrl-C cut-n-paste\n- no need to mount or unmount anything\n...\n\nUnix like would mean:\n- single-click behavior\n- login in as user\n- X-Windows cut-n-paste\n- mount and unmount\n...\n\nI think that would be the easiest way to serve both groups.\n\nCheers\nLukas\n"
    author: "Lukas"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-26
    body: "KDE has already something like this and it is well done.\nTo extend this to the whole system is just stupid. I like to use double click with KDE as I have not yet figured out our to make efficient selection with the one click option. But I will never used my PC logged as root for normal work. And the way KDE handle clipboard since QT3 is perfect."
    author: "Murphy"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-26
    body: "HOWTO select icons without clicking:\n\nGo to Peripherals/Mouse in the Control Center\nCheck 'Automatically select icons' under the header icons.\n\nIn windows this is the default, if you enable Single-click in windows. By the way, single-click has been in windows since Windows 95. It was an extention to Internet Explorer.\n\nGood luck, Jos"
    author: "Jos"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-26
    body: "I think the average Win98 user expects to be root maybe even without entering a password and I think there are good reasons for that. Logging in as user makes certain tasks more difficult and the only reason for this is securety. However, the average Win98 user doesn't gain much securety by logging in as user. I mean reinstalling my system costs me about an hour of work but loosing all the data in my home directory would be a real problem. But if for instance a virus would attack my system it wouldn't make any difference if I'm just a user or root on my system. In any case the virus could delete my personal data. "
    author: "Lukas"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-26
    body: "Not if you have a special account just for surfing..."
    author: "Murphy"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-26
    body: "Yes, but that doesn't mean that your default working account can't be root."
    author: "Lukas"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-08-02
    body: "Default login of \"root\" should NEVER be considered in any Unix-like operating environment.  Root simply leaves the user, and the user's system, entirely too open to external action that could be injurious or user mistakes that could be at the very least, frustrating.  Root should be available, a desktop should never default to root."
    author: "John Glendening"
  - subject: "Don't call it Unix vs Windows"
    date: 2002-07-26
    body: "Because Unix interface sounds 'difficult & foreign interface'. It should be something that a power user can switch.\n\nCall it:\n  Simple interface\n  vs\n  Improved Unix interface\n  vs\n  Legacy Windows interface\n \nThis would put clear that the first should be better than the other\n\n  Simple interface\n     Should have a simplified KControl, single-click to launch apps, drag to\n     select, no terminal on Kicker.\n  Improved Unix interface\n     Complete KControl, single-click, terminal easy to reach, etc\n  Legacy Windows interface\n     Exactly the same as Win98 default. This can be useful for people which have to switch frecuently, for people who are old win users and don't like to change, etc\n\n   Put an easy way change on this on KC, and to save a configuration to \"return\" in case of exploring alternatives. \n      \n  "
    author: "Andrea"
  - subject: "Re: Don't call it Unix vs Windows"
    date: 2002-08-31
    body: "No no no. Unix is a unique O/S and should never be made like Windows!"
    author: "Massimo"
  - subject: "Re: Don't call it Unix vs Windows"
    date: 2002-08-31
    body: "He's saying that there should be an option to make all the configurations \"Windows like\" - which makes sense to me.  I've seen plenty of KDE desktops arranged like Windows, Mac or NeXT desktops.  It's the same thing as chaging a style and window decoration, only deeper - keystrokes, kicker layout and configuration, menubar setting, etc... all would change.\n\nHaving these options would also be nice for another reason - you could have \"KDE Simplified\", \"KDE Poweruser\", \"Windows Style\", \"Mac Style\", \"NeXT Style\", \"Amiga Style\" and \"Integrate with OSX\" for people running KDE under OSX, so that Kicker avoids the Dock, keystrokes mirror OSX conventions, etc.\n\n--\nEvan (Who tossed in Amiga style for a friend who is a Workbench diehard)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-28
    body: "> - login as root by default\n\nAre you nuts?! o_0\nWe should *not* encourage insecure usage, no matter how \"userfriendly\"."
    author: "Stof"
  - subject: "Re: Windows vs Unix behavior"
    date: 2002-07-28
    body: "> Unix like would mean:\n> - mount and unmount\n\n/me looks down and shakes head\n\nHaving to tell the computer when you've inserted removable media in a drive is *bug*, not a feature.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Windows vs Unix behavior"
    date: 2003-02-04
    body: "Here-here, Simon!"
    author: "Cankan"
  - subject: "Act on single click / Warn on double click"
    date: 2002-07-26
    body: "If the default is supposed to be single click, would it not make sense to catch double clicks anyway and pop an error dialog to explain the situation and indicate ways in which it can be customized?"
    author: "Denys Duchier"
  - subject: "Re: Act on single click / Warn on double click"
    date: 2002-07-26
    body: "Yes, this would actually help people. It's the same behavior of ICQ: if it's configured to use the left mouse button, it displays a help dialog box when you try to use the right button."
    author: "Luiz"
  - subject: "Re: Act on single click / Warn on double click"
    date: 2002-07-27
    body: "Sounds good to me, just be sure there is a way to disable that dialog box after seeing it the first time :-)\n\nTim"
    author: "Tim Vail"
  - subject: "Please God No!"
    date: 2002-07-27
    body: "That is one of the most irritating behaviors I've seen in a program. Now I have to dismiss some condescending dialog box for my apparent wrongdoing. Maybe it should just do nothing. And maybe it should be designed such that a left click is the obvious choice. Or maybe it should default to right click as that is what everyone is doing anyways.\n\nMaybe a hint from what the major web browsers do (don't know if Konqueror does this while browsing) is needed. If you double-click on a link, it ignores the second click. I see people double-clicking on links all the time. Sure they don't completely understand when to single and when to double click, but it doesn't hurt for the most part because of this behavior. In Konqueror (file browsing at least) you end up with two copies of the file open. Almost certainly not what was intended."
    author: "DCMonkey"
  - subject: "Usability for everyone"
    date: 2002-07-26
    body: "Usability is not only a subject for beginners.\nTypcal studies create tasks for a beginner a try to assess how they survived.\nBut they does not care whether the participants understood what they were doing.\nThis is the typical MS-Windows mistake, which leads to the annoying \"we care for you\" GUI. One of these mistakes is the attempt to hide the file system.\n\nThis lead to my main topic:\nThe normal (non beginner) user wants to know what goes on in the file system.\nAs long as you work with your documents everything is fine, because of the clear structure of the \"home\" directory.\nBut when you install SW the horror beginns.\nAn application is spread over whole filesystem, if you try to compile from the sources with no chance of uninstalling.\nSo the user is afraid of installing because he does not know whats going on.\n\nThe alternativ are application directorys, where everthing for an application is stored in one directory.\nThe ROX-Desktop realised it (http://rox.sourceforge.net/), see the explanation of application directorys on (http://rox.sourceforge.net/appdirs.php3).\n\nThats what I call usability.\n\nI know that this is a big task to do, but this would also boost the development of new applications, because it will be easier to find beta-tester."
    author: "Cajus"
  - subject: "Re: Usability for everyone"
    date: 2002-07-26
    body: "Apps folders was definitely one of my favorite things about the Mac, I have been meaning to try ROX for a while..."
    author: "Spencer"
  - subject: "Re: Usability for everyone"
    date: 2002-07-26
    body: "Yes very good point.  I think that installing/removing software is THE usability issue left to address on the linux desktop.  Whenever this is brought up it is always brushed off as a distro issue.  Well the distros have not fixed it.  Something with kde's size would stand a much better chance at doing it.  Go together with gnome and others and set it up like the other standards on freedesktop.org.\n\nThe problem as I see it is caused by the lack of distinction between apps and os.  Sure we have great tools for managing packages that come with the os (apt, emerge, urpmi) but these tools do not help well for packages a user downloads.  Having a common install system for desktop apps that works in parallel to the existing solutions would be nice.  This way a user can download a distro independent package and install it without having to worry about the rest of the system.  This way my brother could download something like bonsai-buddy, install it in only his account so it does not effect the rest of my system, and then easily uninstall it when he comes to his senses.  No root passwords, no dealing with a giant list of packages that he does not understand, and no trying to figure out which package is the right one.\n\nThis is also something that I think is holding back commercial application support.  Right now for most users the app has to come with the os, or they can't install it.  Most distros prefer open source software to ship with, so the commercial apps do not ship with it.  So a commercial software company has the choice of a) open source the app to try to get it included with distros, or b) let the user download install the app (limiting to a small group of users from and already small linux user base) or c) don't support linux.\n"
    author: "theorz"
  - subject: "Re: Usability for everyone"
    date: 2002-07-27
    body: "1) KDE is not linux software, KDE is Unix, Linux, BSD, MacOS X and Windows software\n \n2) The independent packaging you are talking about is the goal of the Linux Standard Base. The reason why it's still not working is because it's not that easy"
    author: "anonymous"
  - subject: "Re: Usability for everyone"
    date: 2002-07-27
    body: "Well, the problem with easy installation of applications not included in ypur distro is that it's the number 1 source for viruses !\nI know you'll say that, as long it's not done as root, it is not risked but that's totally wrong !\nWhat's the worse for your brother ? Breaking the whole system or just losing all his work ?\nI think that doesn't make any difference for him. Remember that the only thing taht counts for users is their work and their configuration/customization because it's not that hard to re-install the system.\n\nSO I think you just should never trust anyone except your favourite distro's packagers. That's the way to security.\n\nNow, distros have to provide every good apps..."
    author: "Julien Olivier"
  - subject: "Re: Usability for everyone"
    date: 2002-07-28
    body: "> if you try to compile from the sources with no chance of uninstalling.\n\n<TT>make uninstall</TT>..."
    author: "Stof"
  - subject: "Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-26
    body: "As a result of reading this (and since I remember seeing problems in this area for a long time), I have changed the default settings for embedded viewers in Konqueror.\n\nOnly PS, PDF, HTML and images now trigger an embedded viewer, by default.\nAnything I missed?\n(The article mentions mp3s, but I think separate apps do the job better\nfor those, don't they? Monopolizing a whole konq window for an mp3 player's slider looks like a waste of screen space)\n\nOf course it's still possible for any user to change those settings in the File Association dialog, to preview them in Konqueror (even for koffice files).\n\nNote that the \"right click paste\" bug is already fixed in CVS too."
    author: "David Faure"
  - subject: "Title is truncated?"
    date: 2002-07-26
    body: "\"ged in CVS\" is missing from the title.\n\nHmm, I see MAXLENGTH=50 SIZE=50 in the HTML code, it looks like KHTML isn't ensuring that one can't type more than MAXLENGTH chars. It should, right?\n(Damn, I see bugs everywhere ;)"
    author: "David Faure"
  - subject: "Re: Title is truncated?"
    date: 2002-07-26
    body: "Oh, khtml _does_ truncate. But one doesn't see it when inserting chars at the beginning of the title.\n\nOk, dot bug then: why limit to 50? ;-)"
    author: "David Faure"
  - subject: "Re: Title is truncated?"
    date: 2002-07-27
    body: "We have to limit it to *something*.  :-)\n"
    author: "Navindra Umanee"
  - subject: "*something*"
    date: 2002-07-27
    body: "\n   That's kinda odd, but... :: shrug ::  Okay.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: *something*"
    date: 2002-07-27
    body: "What's kinda odd exactly?"
    author: "Navindra Umanee"
  - subject: "Re: *something*"
    date: 2002-07-27
    body: "\n\n   The fact that you limit the Title to just \"*something*\".  Is the Re: okay?  I didn't add it.\n\n--\nEvan (Pssst.  It's a joke.)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: *something*"
    date: 2002-07-27
    body: "har har :-)"
    author: "Navindra Umanee"
  - subject: "Re: Title is truncated?"
    date: 2002-07-27
    body: "Ok....if khtml does stop at 50 characters, it should not let us insert ANYWHERE in the text (that means beginning too).  So it's not just a dot bug, is it?\n\nSorry...  \n\nWell, 50 characters is a bit long for a title, but maybe the limit should not be looking at what is a reasonable length, but rather what is starting to get unreasonable.\n\nIf you feel that 50 characters is starting to become unreasonable, then by all means keep that limit.  Anyways, it saves us some reading by forcing some people to shorten long titles that are not well written, and takes a while to read.\n\nTim"
    author: "Tim Vail"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "i haven't yet rebuilt to check out your changes (so apologies in advance if this is already taken care of), but:\n\nperhaps anything text related (office docs, text files, etc) or image related (pngs, jpegs, etc) via the http protocol (or any other protocol that is read only)?\n\ni would also wonder about images stored locally .. are they more often viewed or editted? if they are more often viewed, then perhaps they should be shown in an embedded view ... "
    author: "Aaron J. Seigo"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "maybe i should read the cvs commits? ;-)\n\nkdebase/libkonq konq_settings.cc,1.47,1.48\nAuthor: faure\n\n\nModified Files:\n         konq_settings.cc\nLog Message:\nWe want to use the embedded viewer for images, too, by default.\n       return (serviceTypeGroup==\"image\"); // embedding is false by default except for image/*\n\n"
    author: "Aaron J. Seigo"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "Sure. I haven't changed what happened over HTTP.\n\nSorry, I should have mentionned it more clearly: this is about \"what happens when clicking on a file in a FILE MANAGEMENT view (iconview, listview, textview, treeview)\".\n\nSo it's only about protocols which support listing (file, ftp, fish, webdav etc.), and unrelated to HTTP.\n"
    author: "David Faure"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "ah, that makes tons of sense. of course, this is David we're talking about so i'm not surprised in the least. ;-)"
    author: "Aaron J. Seigo"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "\nJust a modest proposal:\n\nLeft click, view (embedded, when possible), middle click, edit, right click properties of the file.  That is, realistically, how I mentally see things (with the exception of web browsing, where I use middle click for opening a link in a new window), since middle click usually opens an editor for me.\n\nStill, the view/edit/properties system seems to be a bit more logical than the current random/new window/properties.  YMMV, I'm just one guy with an opinion, etc.  :)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "Sounds like a reasonable proposal, but it seems to me that it is not half that simple.  Different filetypes do have different properties, and are used for different purposes, so it is difficult to just have a \"one size fits all\" for all filetypes.\n\nI have nothing against embedded viewer for .doc, .txt, and those types of files that would normally be edited, just as long as #1 - I can save the document somewhere else from within the viewer, and #2 - when I press \"backspace\" it goes to the previous page, and does not trap my konqueror commands.\n\nIn another words, the problem I have with embedded viewer is not that they look bad or anything, it is that they are trapping too many konqueror shortcut keys (most notably, the backspace should be available no matter what).\n\nTim Vail"
    author: "Tim Vail"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-30
    body: "> Left click, view (embedded, when possible), middle click, edit, right click properties of the file.\n\nFunny, that's *exactly* konqueror's behaviour.\n"
    author: "David Faure"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-30
    body: "Not quite (and yes, I know who I'm talking to, David :)  ).\n\nLeft click does quite a few different things.  Middle click seldom edits, more often opens the target of the click in a new window.  Right click does, almost always (okay, I can't think of a time when it doesn't) bring up properties.\n\nMy proposal is to formalize left click as \"view\", never \"edit\", and use middle click as a \"edit\" default, rather than \"alternate program, usually in a different window\".  Right now, left click sometimes edits, sometimes views, and often to do the common task of editing, you have to right click, and Open With....  Since the two most common activities you do with files are view and edit, and generally the two applications are seperate (a image viewer vs. an image editor, a sound editor vs. a sound editor, a text viewer vs. a text editor, etc), it would make sense to move these two activities to the \"top\" of the selection tree, and assign them very consistant behaviour, no matter what the file is.\n\nLeft click: View\nMiddle click: Edit\nRight click: Properties\n\n   Right now, it sometimes works that way, sometimes doesn't.  All I'm saying is \"formalize and make it consistant\".  And if the developers all agree that it's a good idea, I'll go through and do the grunt work of making sure that it is configured consistantly along those lines.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-30
    body: "Left click does - what the user expects, view if this kind of file is generally viewed, liked images, PDFs etc., and launches the app (to edit OR view) otherwise.\nMiddle click launches the app, always. I don't see what you mean by \"seldom edits\". For files that can\nbe edited, middle click launches the app to edit them. Always. \n\"New window\" - did you mean a Konqueror window? Show me one file (not a dir, and not HTML) which opens a konq window when middle-click it, I'll be very surprised. (dirs and HTML are handled by\nkonq, so it's logical they open a new window ;). In all cases, MMB = launch app.\n\nYour idea isn't as consistent as you think, and surely not practical.\n\n1) VERY FEW users know about middle-clicking. On mouses with 2 buttons it means pressing both buttons at the same time. How ergonomic is that? My wife doesn't know MMB, my parents don't know MMB, and they surely don't want to have to press both buttons just to edit a kword doc.\n2) Not all files can be edited. What happens if you MMB a sound file, in your suggestion? Given that I have no sound editor installed?\n3) Sometimes you don't really know if you want to view or edit. You want something that shows you the stuff, and then you'll see if you need to change it or if it's fine.\n4) This change comes after a complain that \"clicking on a text file views it, but users don't\nknow how to edit it\". Your suggestion goes _back_, re-introduces this behaviour, and even more of it (clicking on a kword document would only show it, readonly. \"How do I edit it??\").\n\nPeople don't want to have to learn how to use a file manager all over again.\nThey don't care if \"it's formalized and consistent\" under the hood. It has to behave\nin a way that they expect, that's the most important thing. We already have/had too\nmany usability bugs that were due to the developer's own notion of consistency."
    author: "David Faure"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-30
    body: ":: Left click does - what the user expects, view if this kind of file is generally viewed, liked images, PDFs etc., and launches the app (to edit OR view) otherwise.\n\nIn other words, it does something different for each icon clicked.  That's confusing.  When most users click on an item, they want to view it.  That's what 85% or so of the KDE left click actions are assigned to do, view the file.  Why not just formalize it?\n\n:: Middle click launches the app, always. I don't see what you mean by \"seldom edits\". For files that can  be edited, middle click launches the app to edit them. Always. \n\nNot always - for instance image files are loaded in KView and html files are loaded in a KHTML view in Konqueror.  Again, in 65% or so of the cases, it does open in an editor - all I'm saying is formalize the already accepted behaviour of KDE and make it consistant.  \n\nThe notion of a difference between a \"embedded app\" and \"fully realized app\" is  nonexistant to a user.  Users work with files, and they don't care if it's inside Konqueror or running as it's own app.  There is a difference, however, between viewing and editing.  \n\n:: 1) VERY FEW users know about middle-clicking.\n\n    Very few users knew about double clicking.  And yet several million of them figured it out over the past couple of decades.  \n\n:: 2) Not all files can be edited. What happens if you MMB a sound file, in your suggestion? Given that I have no sound editor installed?\n\n    Then you've identified a lack in KDE's base of applications.  If the file format is standard, there should be an editor.  Until then, use a viewer, or search for a 3rd party editor like Audacity installed on the system.\n\n:: 3) Sometimes you don't really know if you want to view or edit. You want something that shows you the stuff, and then you'll see if you need to change it or if it's fine.\n\n    True.  It would be nice if viewers had an option \"Edit\" under the File menu that looked to the File Type db and launched the preferred editor for that type of file.  That would also allow quick viewing of files to find the one you want to edit.  Again, I would be willing to do the grunt work in making sure all viewers in CVS match this behaviour, if this proposal becomes accepted as a good idea.\n\n:: 4) This change comes after a complain that \"clicking on a text file views it, but users don't know how to edit it\". Your suggestion goes _back_, re-introduces this behaviour, and even more of it (clicking on a kword document would only show it, readonly. \"How do I edit it??\").\n\n    Yes it does go back to that behaviour, which was primarily inexplicable because it was one of the only files that did that.  But then, sometimes you want to just view a textfile - configuration files, most notably.  And yes, you *are* introducing a seperation between viewing and editing.  Is that difficult?  Well, the seperation already exists in the applications... KView versus Gimp, Noatun versus Audacity, Konqueror KHTML versus Quanta, Konqueror tar: versus Ark.  IMO, it clarifies that already existing seperation and allows users to clearly choose if they want to view or edit a file.\n\nAs for confusion: Left Click, View - Middle Click, Edit.   It's really quite simple, and easy to explain both to totally new computer users and new KDE users.  Easier than the current \"sometimes you get a viewing program, sometimes you get an editor, sometimes it's embedded, sometimes it's seperate\".\n\n--\nEvan  \n\n(Opinions?  Please don't think I'm flogging this for myself or out of a belief that this is the One True Way.  I think it's a good idea, yes, but I'd like to hear some other reasons why it's bad.  Reason number 4 up there is my personal tweak that gave me serious pause.  I think if it were clearly \"Left/View, Middle/Edit\", that issue would be negated.)"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2006-06-17
    body: "Evan, maybe you can help with something.  Recently, my\nkonqueror stopped viewing the contents of tar.gz and\ntar.bz2 files as though they were folders.  Now it always\nopens ark instead.  I have no idea what happened, but what\nare the correct settings to put it back the way it was?\n\nThanks.\n"
    author: "thomas"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-27
    body: "I find the embedded Noatun butt ugly.  Especially when you get to larger resolutions like 1280x1024 and a maximized Konq.  You have a whole screen of grey with 4 (I think) \"little\" buttons and a slider.  I understand I can change the file associations, but that is kinda annoying.\n"
    author: "Old Pink"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-30
    body: "What about using a split view for embedded viewers, such that the viewer would not replace the icon view but appear in a separate (sub) view? People could then select more documents while the (pre)view area is reused."
    author: "bela"
  - subject: "Re: Behaviour of konqueror (wrt embedded viewers) chan"
    date: 2002-07-30
    body: "Try the \"file preview\" konqueror profile, it does exactly that."
    author: "David Faure"
  - subject: "KDE usability"
    date: 2002-07-30
    body: "Hi all\n\nWhile there are a few things in KDE that could use improvement, the over all operation IMHO is much better than windows.\n\nPlease don't change KDE because of a small study carried out using computer-illiterates. The \"single click thing threw me\" isn't a valid argument. The single click is easier and more consistant than Windows. BTW click on apps in the panel bar at the bottom of Windows 2000. A single click will start the app.\n\nPeople have learned certian behaviors as a result of exposure to Windows. If a usability study were done on windows by users who had only been exposed to KDE then perhaps we would find that the results would be equally opposed to those things that windows does differently.\n\nIn short we get used to something, decide that it is the \"right way\" and then set out to impose this on others.\n\nKDE is fine. It took me an hour or so to get used to the differences and now I find it much preferable to Windows. In additon the whole Linux environment IMHO is far superior to Windows. \n\nJohn\n\nJohn"
    author: "John Gluck"
  - subject: "Hey, I wanna give thanks..."
    date: 2002-08-01
    body: "Thanks for your work, is really important for me and the people in every social organization, that use a free software. We (a LUG) are working to expand linux, and this project is a hot support to do.\n\nThanks boys and girls.\n\ncheers.\n\nruben"
    author: "ruben"
  - subject: "Configuration"
    date: 2003-05-12
    body: "I think the number of configuration options gives a false sense of flexibility.\n\nI would like to do this in KDE...\n1) Have folders open without the tree view (it is set to always open a new one).\n2) Have the folder window open in the same place and with the same size as the last time I opened that and only that folder (still in the current workspace).\n3) the first time I click on a new file type I should be offered ways of associating that file. I almost always want to edit the damn thing, not view it in a next to useless read only Konquerer window. Having to chase down the file associations every time on first use is a real pain.\n\nBoth the Mac and OS/2 solved these problems years ago and both allow people to walk up to them and become instantly productive.\n\nAnother gripe - if you are going to make the edges of windows so thin at least make them configurable or allow the middle drag of the title bar a'la Gnome.\n\nAnd could you kidnap the Gnome icon designer...\n\nyours, Marcus."
    author: "Marcus Baker"
---
Sebastian Biot looks at <a href="http://usability.kde.org/">KDE Usability</a> in <a href="http://www.viralata.net/kde_usability/001_01.html">the first of a series</a> of studies. <i>"While some participants noted that KDE looked different from Windows, none seemed bothered by the differences and the look-and-feel of KDE. Users identified all the elements of the interface without any trouble including KDE's Konqueror and KMail icons. Most users seemed to understand the K menu's presence and function intuitively and they used it much more than I had anticipated.  This test conducted in early July 2002 with four participants outlines of some of KDE 3.0's shortcomings including inconsistencies in KFileDialog and the difficulties of working with Konqueror's embedded viewers."</i> It's good to see people stepping up to do this kind of work -- the good news is that discussion of the study has already been started (<a href="http://lists.kde.org/?t=102695739700002&r=3&w=2">kde-usability</a>, <a href="http://lists.kde.org/?t=102695832300001&r=1&w=2">kde-cafe</a>).
<!--break-->
