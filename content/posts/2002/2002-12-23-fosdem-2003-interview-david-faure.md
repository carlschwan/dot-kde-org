---
title: "FOSDEM 2003: Interview with David Faure"
date:    2002-12-23
authors:
  - "AlainB"
slug:    fosdem-2003-interview-david-faure
comments:
  - subject: "only 2 full-time developers on KDE ?"
    date: 2002-12-23
    body: "Incredible. Gnome PR says they have over 100:\nhttp://www.gnome.org/pr-gnome20.html\n\nHow's this possible ? As i see it, KDE is *at least* one major version ahead of anything that exists under gnome brand. I'd say that using C++/Qt is one thing giving momentum to KDE, but.."
    author: "jmk"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-23
    body: "Maybe because programming with the Gnome APIs is not very efficient?"
    author: "Tim Jansen"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-24
    body: "Hmm, I'd like to see the names of those _100_ fulltime gnome developers....\n\nFor sure in KDE there are more than 2 persons paid to work on it - but most of them have to do some other things too. However 20 persons working 50-80% of their paid time on KDE + working 20-50% of their free time on KDE :), are 20 persons working fulltime on it, just not \"paid fulltime\". I think there's a matter of definition here.... but yes, in any case we're very far from 100 persons fulltime, whatever the definition of that.\n"
    author: "David Faure"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-24
    body: "> Hmm, I'd like to see the names of those _100_ fulltime gnome developers....\n \nWell, Sun Microsystems, RH and Ximian seem to fund GNOME heavily, so i wouldn't be suprised if it actually is true. However, two things leap to mind. I'd guess that you classify being a gnome developer only by being a corporate techie (in any of the above companies), and gnome somehow relates to your work. Second thing being that classical truth of software design states that adding more manpower to an unorganized project actually makes matters worse, not better. Therefore i'd suggest a *huge* thank you to all organizers (release dudes et al) and good spirit in KDE team. \n\nAs for the technical side, i'd guess that counting Qt as an essential part of KDE development makes matters a bit more even. It's hard to say how many guys TT has working on Qt/X11, but as it seems, there has to be quite a few true professionals. Great thanks to Qt development team as well.\n\nOther than that, merry christmas everyone!  (<- point taken)"
    author: "jmk"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-24
    body: "Perhaps KDE as many other projects are not very experienced in fund raising issues.\n\nAnd there is no international penpal alternative. (I don't have a credit card!)\n\nIs KDE e.V. \"gemeinn\u00fctzig\"?\n\nWhom to donate?"
    author: "Hakenuk"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-24
    body: "http://www.kde.org/helping.html has a link to http://www.kde.org/kde-ev/about/donations.php"
    author: "ac"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-24
    body: "You can read the corresponding FOSDEM interview of one of the Gnome developers here:\nhttp://www.fosdem.org/index/interviews/interviews_meeks/\n\nIts too bad they didn't ask some of the same questions to David Faure. I would have liked to hear his response.\n"
    author: "dapawn"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-24
    body: "Michael Meeks: I'm not convinced Gnome is (or ever will be) ready for the Home / Personal desktop market in terms of the functionality that people expect today. \n \nSomeone willing to try interpret this?"
    author: "jmk"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-25
    body: "No... It could also mean \"Nothing can ever catch up with Windows\" or \"Mono is the future\"."
    author: "Tim Jansen"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-25
    body: "> It could also mean \"Nothing can ever catch up with Windows\" or \"Mono is the future\".\n\nSomehow I seriously doubt mono with its wine ports of windows.forms etc. If Microsoft is able to design a document format that takes years to be reverse-engineered i wonder what they'll be able to do to a platform.\n\nFor the 'nothing will ever catch up with windows' point, i'd say that linux/kde already has. Probably not yet on every aspect of it, but most of it. Distributions need to evolve, a lot, but KDE is already very catchy usability-wise."
    author: "jmk"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-25
    body: "Michael Meeks: I'm [..] 1m tall, hairy all over, and like bananas.\n\nSomeone willing to try interpreting this? :-)"
    author: "Anonymous"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-28
    body: "Gnome developers mutate to Ximians?"
    author: "joe"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-25
    body: "Feel free to ask :)\nWhich questions?"
    author: "David Faure"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2002-12-27
    body: "Here is a question David. Are you aware of Andras Mantia? I sponsor him to work full time on Quanta. I'd like to get more involved in helping to develop KDE sponsorships because I find that developers are more proficient and consistent  when they can dedicate their time. Andras is orders of magnitude more efficient than our other developers because his head is always in the code. It doesn't hurt that he has no real distractions or financial worries either.\n\nBTW I'm a big fan of your work and I believe the API we have to work with is largely responsible for how productive we can be. I'm also looking at sponsoring a college student I was unable to in 2002. In my opinion more full time developers are the key to cornerstone leadership in key application projects. I believe the problem is that a great many individuals and corporate entities do not yet understand how substantial their contribution can be to their own uses as well as the community. \n\nI think half a dozen or a dozen full time people heading development on key desktop apps could radically transform the perceived viablilty of KDE as an alternative desktop in 2003. I'm looking to take a proactive stand on this."
    author: "Eric Laffoon"
  - subject: "Re: only 2 full-time developers on KDE ?"
    date: 2003-01-05
    body: "Sounds good - but I'm not sure what the question is :-)"
    author: "David Faure"
  - subject: "Nice interview"
    date: 2002-12-23
    body: "Nice interview, but not much I didn't know before (have read many a Faure interview, I must be a big fan ;-)\n\nNow, what is this new trend of always putting a space before punctuation ? Like I just did there. :-) Everybody's doing it, even skilled writers, but only for \"!\" and \"?\". I think it's ugly, inconsistent, and generally a weird thing to do. Can I have some insight into why people are doing this nowadays? :-) Should I be doing it, too?\n"
    author: "Fan"
  - subject: "Re: Nice interview"
    date: 2002-12-23
    body: "The (small) space before some punctuation marks (mainly \"?\", \"!\", \";\", \":\") is the rule in French writing. It isn't the rule in English at all. Maybe the French usage is having some influence on the English usage? That would be suprising, as it generally is the other way around."
    author: "J.-F. Lemaire"
  - subject: "Re: Nice interview"
    date: 2002-12-23
    body: "Yes, having a non breakable space before !, ? and such is a french rule. I guess that maybe Alain Burnet who asked the questions is also French. But having some elements of French in an English conversation is looked at as smart. \nFOSDEM is a very important event in the Open Source community and it's really nice to have David speaking here about KDE. I hope that lots of KDE developers will be able to go there."
    author: "annma"
  - subject: "Re: Nice interview"
    date: 2002-12-23
    body: "Hehe, when reading this I thought I made the mistake, but it's not me who did, it's the interviewer ;)\nIt's the normal way in French, but indeed not in English - don't start doing it :)"
    author: "David Faure"
  - subject: "Re: Nice interview"
    date: 2002-12-25
    body: "In fact (like has been said elswhere in the thread) it is the rule in french.\n\nIf you typeset with LaTeX, you get to notice such things... It should be noted that the space is only before  ;, :, ?, and ! and that it is not a space, but half a space. When properly typeset, it looks better than (IMHO) the english \"attached\" version. But of course it looks wrong (even in french) when using a fixed width font.\n\nAs an aside : when will kword be able to typeset as well as TeX ? (yes I know it can export to TeX, but then it uses an external rendering engine) Which would make it suddenly a wonderful program (I already like the interface a lot) instead of a <<very promising>> one :)"
    author: "hmmm"
  - subject: "Big thanks to TrollTech!"
    date: 2002-12-25
    body: "David Faure:\n\n\"Since January 2003 I'm no longer working for MandrakeSoft, but for Klar\u00e4lvdalens Datakonsult AB (don't try to pronounce that, just say \"KDAB\" like everyone else). This is Kalle Dalheimer's company. I'm still working 50% of my time on KDE and KOffice, sponsored by TrollTech. The other 50% I'm working on Qt/KDE-related commercial projects for KDAB. \"\n\nAfter Mandrake having so much trouble lately, I was afraid that David has to leave KDE development completely. Now I am very happy to read that David found another job and has at least 50% of his time to work on KDE which is great! Thanks to TrollTech for sponsoring this great developer and of course thanks to David F. for your great work :-)\n\n "
    author: "Marco"
  - subject: "Re: Big thanks to TrollTech!"
    date: 2002-12-25
    body: "You'll be even happier to hear that his work at KDAB seems to be on Kroupware so far.  Hope he doesn't get fed up with KDE.  :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: Big thanks to TrollTech!"
    date: 2002-12-25
    body: "Giving a hand with the KParts stuff in Kroupware, (quite natural, given that I co-designed KParts), but hopefully I'll also be working on many other things :)"
    author: "David Faure"
---
<a href="http://www.fosdem.org/">FOSDEM 2003</a> will take place February 8 and 9, 2003 in Brussels, Belgium.  The two-day summit will bring together leading developers in the Open Source community. As in previous years, FOSDEM attendees will have the opportunity to attend to the talks of the leading developers of major Open Source and Free Software projects. This year  <a href="http://www.fosdem.org/index/speakers/speakers_faure">David Faure</a> is confirmed for KDE.  We have <a href="http://www.fosdem.org/index/interviews/interviews_faure">interviewed him</a> as part of a new series for FOSDEM 2003.
<!--break-->
