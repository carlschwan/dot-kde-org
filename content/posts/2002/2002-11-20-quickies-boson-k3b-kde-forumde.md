---
title: "Quickies: Boson, K3b, KDE-Forum.de"
date:    2002-11-20
authors:
  - "numanee"
slug:    quickies-boson-k3b-kde-forumde
comments:
  - subject: "English web forum"
    date: 2002-11-19
    body: "http://www.kde-forum.org"
    author: "Anonymous"
  - subject: "Re: English web forum"
    date: 2002-11-19
    body: "Great forum.\nWhy haven't this been anounced???\nBtq, the page gets too wide on Ms Explorer 6....if anyone cares. :-)"
    author: "KDE User"
  - subject: "Re: English web forum"
    date: 2002-11-19
    body: "maybe becoz its not ready???  read KDE-LOOK.org."
    author: "ac"
  - subject: "Re: English web forum"
    date: 2002-11-19
    body: "Ok ok.\nBut it looks great in Opera 7."
    author: "KDE User"
  - subject: "not good"
    date: 2002-11-19
    body: "I don't want to sound bad, but you see, I know boson sinse 0.4, it's a cool program and all, but...\n\nI don't have a 3D board, so I can't play it anymore\nMaking 3D BEFORE AI dosen't sound good at all\n\nI think Boson shold have kept 3D for later, but well, at least I still have freecraft :)"
    author: "protoman"
  - subject: "Re: not good"
    date: 2002-11-19
    body: "Use software GL.  Mesa does this maybe."
    author: "ac"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "I think people don't know how to use Mesa right.  Every 3D game I've ever tried with Mesa has had framerates that are simply awful, even the ones with horrible graphics.  Even the 2D OpenGL game Chromium is unplayable.  I think people just aren't paying attention and are using features of Mesa that are godawfully slow in software but are accelerated in hardware.  I mean, I can run Half-Life in software mode on my computer and it is playable, but GLTron practically brings my system to a halt.  I can hardly even navigate the menu.  More attention needs to be paid to making 3D games work without accelerator cards, especially since Linux's 3D card support isn't very broad.  I do have a 3D accelerator but it is not supported."
    author: "not me"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "It's not a matter of using Mesa properly. Most professionally written games, such as half-life, use various occlusion algorithms that eliminate parts of the scene that are hidden. Every polygon constructed in a GL scene is rendered, pixel by pixel, even if it will never be seen on screen. Modern 3D accelleration hardware removes most of the unseen polys and only fills the pixels it needs too. Software implementations like Mesa don't, as doing so really isn't correct. In professionally written games, the \"world\" is usually broken into objects which are stored in a tree structure, often refered to as a visibility tree. When one node on the tree is determined to be invisible, all of its children are as well. This is quite complex to implement properly, so most OSS projects don't do that till much later in the development cycle."
    author: "Paladin128"
  - subject: "3D"
    date: 2002-11-20
    body: "One way to get \"free\" culling is to use some kind of scene graph. I use OpenSceneGraph (http://openscenegraph.org/), and it has view frustum culling based on hierarchical bounding spheres. So you can pretty easily get much of your data culled away. \n\nFor anyone not too experienced in OpenGL I'd suggest going with a scene graph instead of doing all the hard work by hand. There are other libs too, such as PLib, which is supposed to be really good too.\n"
    author: "Chakie"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "You're confusing software emulation of OpenGL with software mode in Half-Life.\n\nSoftware mode in Half-Life doesn't look anything like real OpenGL mode. If you would play Half-Life with software emulated OpenGL, you would get unplayable framerates."
    author: "."
  - subject: "Re: not good"
    date: 2002-11-21
    body: "I'm not confusing them, I'm just comparing them.  Half-Life's engine features an advanced software renderer that does everything Half-Life uses OpenGL for, except for the texture smoothing (which of course is a very nice feature, but slow in software).  The game looks exactly the same in software and OpenGL, except for the smoothing.  And it runs playably, while GLTron crawls, with much worse graphics.  I think the above comments about culling are very interesting.  Perhaps Boson should look into using a scene graph or something to speed up its rendering.  Or maybe they could use Crystal Space, it seems to be getting quite advanced now, and it has culling algorithms.  I guess they're probably too far along to switch to Crystal Space now though..."
    author: "not me"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "3D boards are pretty common nowadays, and are only going to become more so. \n\nGranted, Open Source can cater to the minority (ex. those without 3D cards) quite well, if that minority cares enough to help solve their problem. Thats why KDE has such great internationalization, better then most commercial software."
    author: "Eean"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "Nah, I don't think we should even BE catering to the non 3D cards any more.  HELL, you can get a CHEAP NVidia clone for UNDER $50!  And a DECENT one for under $100!\n\nAnd if you don't have one, then chances are VERY good you shouldn't be playing games on it anyway. :)  IE servers/business machines. :)"
    author: "jstuart"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "Well, then, please, tell me, how to install this decent NVidia clone to my lovely ThinkPad i1400 ? I'll gladly pay $50 for card and $50 for work, if you'll do it...\n"
    author: "Yarick"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "Hum, do you know that 100 dollars are aprox. 350 reais (brazilian currency)?\nIt's almost all my salary, and I still have to pay rent, water, eletricity, and we should not forget the important thing: food :)\n\nHeheh, seriously guys, I'm not agains 3D support, but look at Quake2! \nEven without a 3D board I can play it, because it have a software mode (and for what people said Half Life have too).\nThe question is that I think that programmer should not simply forget people who dosen't have 3D boards and simply say to them: go buy one for you :)"
    author: "protoman"
  - subject: "Re: not good"
    date: 2002-11-20
    body: "On the other hand, those people without a 3D card could also spend their time on writing fast software rendering. After all, *they* want it. Why should the developer, if he has no interest in unaccelerated 3D graphics, do this?\n\n\n"
    author: "AC"
  - subject: "Re: not good"
    date: 2006-03-12
    body: "ye i know it sucks don't it."
    author: "ma top"
  - subject: "Noe everyone knows how goot k3b is...."
    date: 2002-11-19
    body: "so why don't we incorporate k3b into KDE's code.  It's the prefect burning app.  Much better than KonCD IMHO.  a CVS merge is what ne need :) \n\noh, and has this been grought up before?  If so what release is planned on intergrating k3b into itself?  \n\nGood article glorifying the wonderful-ness that is k3b."
    author: "standsolid"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-19
    body: "Maybe merge the apps KonCD, K3B and cdbakeoven -> become the best cd-burning - tool ever. I know, it\u00b4s not possible :-("
    author: "star-flight"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-19
    body: "Agreed!  K3B is a very nice application and it makes burning cd's (audio, video, data) a breeze.  It may just be a case where the best app is going to be an add-on.  .i.e Think of windows xp (it comes w/ cd burner).  However, most people go elsewhere for something more powerful (.i.e nero,etc)  Maybe k3b will overtake koncd just as kopete is doing to kit.. \n\n"
    author: "SmallThinker"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-19
    body: "I really like K3B. I've been playing with it for a few weeks. I mostly got it for my girlfriend, so she could burn CDs without learning a bunch of unix commands.\n\nOne thing that bothers me is that it needs to be installed in your normal KDE directory. I installed it in /opt/k3b/ instead, and it can't find its icons or pictures, and has trouble with the k3bsetup thing.\n\nI read a bunch of messages about problems with K3B, and most of them are just answered with \"did you set your --prefix correctly? It needs to be the same as your KDE directory\".\n\nSo why is it imperative that it be installed into your KDE directory? Do most KDE apps do this? I seem to remember a similar problem when I tried Atlantik a while ago and threw it into /opt/atlantik. Is this the standard for KDE apps, or is K3B poorly designed in this respect?\n\nAm I alone in wanting bundled-with-kde programs to live in /opt/kde/bin and not-bundled-with-kde programs to live in /opt/program/bin? I think it's a pretty silly thing. Don't most programs compile the --prefix string into themselves, so that they know where to read their own files from? I thought that was the whole point of --prefix! It seems like a waste also, since they can always use the KDEDIR environment variable to find KDE files.\n\nI guess this turned into sort of an offtopic rant, but it's a pet peeve of mine. :)"
    author: "AC"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "it seems pretty logical to me that you put all your kde stuff in the same prefix.\nWhat's the point of putting everything in /opt/<appname>.\nTo me it makes sens if you put gnome apps in /opt/gnome and kde in /opt/kde, and large apps in /opt/<appname>.\nI always keep the source codes of apps that don't come with kde/gnome and then you can just do a make uninstall to remove it.\nJust my opinion."
    author: "dwt"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "Well, I like to keep things clean. If I want to uninstall something completely I can delete the /opt/<appname> directory, instead of searching hunting through the KDE dirs looking for everything to delete. I also tend to play with multiple versions of a program at the same time. So could have /opt/abc-0.9.7 and /opt/abc-0.9.8 both installed, and just by changing the /opt/abc symlink switch between them.\n\nI kind of think it should be beside the point whether I'm compiling a \"KDE app\" or a \"Gnome app\", as far as where I can install it... Why can't it just check for its files under the --prefix dir like everything else?\n\nI suppose I could keep the source code around and make uninstall, but that kind of thing tends to be the first thing I delete when I need some disk space..."
    author: "AC"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: ">  If I want to uninstall something completely I can delete the /opt/<appname> directory, instead of searching hunting through the KDE dirs looking for everything to delete. \n\nYou want to use \"checkinstall\"."
    author: "Anonymous"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "Couldn't agree more.\n\nJ.A."
    author: "Jad"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "In the infamous words of a certain kde developer whose name shall remain anonymous \"KDEDIR is dead, long live KDEDIRS\".  Make sure that you include the directory where you installed the application to the KDEDIRS variable.  You can also use KDEHOME to change your local .kde where you config files are stored etc.\n\nAnd for those of you who might not be aware of this, KDE since the original 3.0 release (or might be a bit prior than that) comes with a kde-config command line tool much like many other open source apps. You can use this tool to see where the different things in KDE are stored and where KDE looks up when it searches for icons, config files etc...\n\nGood day,\nYour friendly KDE helper"
    author: "KDE Helper"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "Thanks! That is a big help.\n\nI still think each app should check under its own --prefix directory for files though. What's the harm?\n\nBut KDEDIRS will certainly help me work around my problem. Thanks again. :)"
    author: "AC"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-21
    body: "Hi!\n\nMy experience is that KDE apps work pretty fine installed in any directory you like if you set $KDEDIR correctly before starting them. The only minor problem left is that they cannot integrate themselves with the KDE menu and mime type configuration as they don't know where the KDE file are but that's easy to fix manually. :-)\n\nJust try\n\nexport KDEDIR=/opt/k3b/\n/opt/k3b/bin/whatever-the-k3b-binary-is-called &\n\non the konsole, there's a good chance that this could work. If it does write a tiny wrapper script to start k3b.\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-21
    body: "Mh, okay, first read, then write, Gunter...\nIs there any way to wipe already written artcles from this board? ;)\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "This is a general *nix problem..."
    date: 2002-11-21
    body: "<rant>\nTitle says it all. Why are _all_ command line binaries installed into /usr/bin, while their auxiliary files are somewhere completely different, e.g. /usr/share/docs/whatever? The filesystem hierarchy is a complete mess.\n\nIt makes much more sense to put all the static files that belong to an application or package into a single directory. Apparently, Mac OS and BeOS-alikes are able to do that. Heck, even some Windows apps get it right (not messing up the System32 directory etc...). Unix fails completely.\n\nOf course there are technical problems, but they can all be solved. For example, in order to find binaries from the CLI, you need to be able to quickly list all binaries on the system. But that's not really a problem - write some scripts that automatically place symlinks to binaries into a directory.\n\nIn fact, a similar system already exists, it's called Stow: http://www.gnu.org/software/stow/stow.html\nYes, it has some problems. Yes, a really clean solution should be based on something like file attributes (meta-data like mime-types). But it's a step in the right direction anyway.\n</rant>"
    author: "Nicolai Haehnle"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "No, no, no ...\n\nK3B is nice, but hardwired with SCSI generic implemenation of Linux. It make him very unflexible!!! See the source.\n\nFrom \"flexibility\" point of view cdbakeoven is very better.\n\nBest regards\n\nAlex"
    author: "alex"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-20
    body: "I think k3b use cdrecord as backend, and cdrecord runs on many diferent platforms, not only on broken ones (linux :-) )."
    author: "fura"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-21
    body: "What's broken about Linux?  I just made a couple audio cds and burnt an iso yesterday on my computer running Debian and omg, what to do you know, it works.  "
    author: "rrp"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-21
    body: "You can read some realy bad words about Linux SCSI implementation in cdrecord's author's homepage (and *BSD userland too).\n\nBTW I failed to build k3b on NetBSD box - unfortunately it's Linux only app :-("
    author: "fura"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-21
    body: "In the end, it doesnt matter. I've never had a bad cd burn using my 50$ cd burner.  I'm sure they're fixing the scsi implementation in the 2.5 kernel."
    author: "rrp"
  - subject: "Re: Noe everyone knows how goot k3b is...."
    date: 2002-11-22
    body: "With the 2.5 kernel, you can do CD burning without IDE-SCSI. There are patched versions of cdrecord which support the 2.5 development kernels already available. Of course, the command line parameters are different, so none of the front-ends support it yet. But it'll be coming soon."
    author: "ShavenYak"
  - subject: "False information"
    date: 2002-11-19
    body: "The article about k3b leaves you with the feeling that there was no Unix GUI CD burning up to now. That either you used windoze/Mac or you were doomed to the command line. This is unfair to the developers (mainly one) of X-CD-Roast. This program is mentioned but underestimated. The author states that he used to use X-CD-Roast for audio burning and command line for data burning. That was probably his decision, but ISO burning on the fly has been available for a long while in X-CD-Roast. \n\nMoreover, most of the basic functionality he praises in K3b has been available for a long time in X-CD-Roast. Don't get me wrong. I like competition, I like to see new programs emerge, and I would love to see a great CD burning as a default app in KDE. Not only that, but also the K3b screenshots look fabulous. But this doesn't mean we have to be unfair with software that has been doing the job for tons of people for a long time."
    author: "NewMandrakeUser"
  - subject: "Re: False information"
    date: 2002-11-19
    body: "xcdroast is ugly and ridiculously hard to use.  enough said."
    author: "ac"
  - subject: "Re: False information"
    date: 2002-11-20
    body: "I prefer X-CD-Roast to the various KDE front ends (haven't tried K3b yet, though) but the author certainly mentions that it exists, but that it's just not adequate for his expectations."
    author: "Otter"
  - subject: "Re: False information"
    date: 2002-11-20
    body: "I switched from xcdroast to k3b. Drag and drop of folders with konq and renaming/creating directories within a project made this my favourite front end. \n\n\n"
    author: "zelegans"
  - subject: "Why I don't use X-CD-Roast"
    date: 2002-11-20
    body: "X-CD-Roast was the first cd burning gui I tried for linux. For some reason I was never able to burn an audio cd properly. The tracks all became (sort of) 1 big long one. I think it had to do with an incorrect table of contents on the cd or something, as it would play correctly on a computer, just not with a cd player.\n\nI also found the way some things worked to be a little unintuitive. I then updated my system, and the program stopped working. It said I didn't have a burner or something. The application's web site stated it couldn't work with the cd tools I had installed. It was then I bumped into k3b on the net and mate, it was brilliant. It is very easy to use, it makes sense, and it integrates nicely into my KDE environment. It gives you the option of configuring everything for you, and well, it just works.\n\nAnother thing is, you can tell just by looking at it that a great deal of time and effort has gone into making it as good as possible. I can honestly say k3b is the best, most professional application I have ever used with kde, outside of the kde CVS tree. It has a couple of little bugs though, and as soon as I have some time I will send in bug reports. Other than that, it is perfect.\n\nSo that's why I chose k3b :)"
    author: "Joel"
  - subject: "Re: False information"
    date: 2002-11-20
    body: "No mention of cdbakeoven either. Nor of KreateCD (now defunct?)."
    author: "Kirill"
  - subject: "Re: False information"
    date: 2002-11-20
    body: "Never did figure out X-CD-Roast. I would rather burn from the command line than use it. Now k3b is another matter. Very nice."
    author: "Echo6"
  - subject: "Re: False information"
    date: 2002-12-11
    body: "Can k3b make cd's bootable?"
    author: "CJ"
  - subject: "KDE 3.1"
    date: 2002-11-20
    body: "Off topic? Perhaps.\n\nAnyone know if we're going to see KDE 3.1 this week, or RC4?\n\nI noticed they deleted the rc2 tree off the unstable dir leaving only rc3... and they deleted rc1 when rc3 showed up... but still no appearance of 3.1 final or rc4."
    author: "Vic"
  - subject: "Re: KDE 3.1"
    date: 2002-11-20
    body: "RC4 already exists but only as tag KDE_3_1_0_RELEASE in CVS.\nhttp://lists.kde.org/?l=kde-devel&m=103767975826039&w=2"
    author: "Anonymous"
  - subject: "Re: KDE 3.1"
    date: 2002-11-20
    body: "\"The future plans include (of course) a patch release named KDE 3.1.1  \nroughly 4 weeks after the KDE 3.1.0 announcement date, and a KDE 3.2 release \naround early summer 2003.\"\n\nYES!  KDE for workgroups!  Finally!"
    author: "ac"
  - subject: "Short K3b question"
    date: 2002-11-21
    body: "Any progress on the \"multisession\" site?\n\nI had X-CD-Roast, KisoCD I/II, KonCD, cdbakoven, KreateCD etc. running, but K3b is best.\n\nOnly problem so far K3b 0.7:\n\n1. It couldn't empty CD-RWs in full mode, have to use KonCD\n2. The multisession \"editor\", bottom half of the main window, do not show\n    the former CD content. Is it possible to change the source, so that we can\n    Drag'n Drop, \"delete\" and \"overwrite\" older files?\n\nGo on with your GREAT work!\n\nThanks,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Short K3b question"
    date: 2002-11-25
    body: "Does mkisofs & friends really support changing the existing session at all? I know that you can overwrite old files with the new ones, but is there really any way, from the command line, to say re-attach directory trees from the old session in a new session, to change names of existing files, to delete existing files, etc?\n\nI.e. rudimentary stuff that's supported since ages by formerly-Adaptec CD-burning thingo, and that doesn't seem to be supported by anything on linux?"
    author: "Kuba"
  - subject: "Re: Short K3b question"
    date: 2002-11-26
    body: "You are RIGHT.\nGot the same answer from the K3b author.\ncdrecord can't handle all that \"nice\" stuff which Winbloze can do.\nBut better multisession support is in K3b 0.7.4.\n\nRegards,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Short K3b question"
    date: 2002-11-26
    body: "Argh, my brain.\n\nHere is the snipped from Sebastian Trueg:\n\n[-]\nSession import is implemented now but you won't be able to overwrite and delete old files since mkisofs does not support this. :(\n[-]\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "My favourite burning apps."
    date: 2002-11-27
    body: "K3b + Arson is the best! Best of both worlds."
    author: "killertux"
  - subject: "Re: My favourite burning apps."
    date: 2002-11-27
    body: "Hi\n\nWhich is the best ? Arson or K3B ? Could you please tell me which ups and downs they both have ?\n\nThanks."
    author: "Julien Olivier"
---
<A href="http://boson.sourceforge.net/">Boson</a>, the real-time 3D strategy game for KDE has been <a href="http://boson.sourceforge.net/announces/boson-0.7.php">freshly ported</a> to <a href="http://www.opengl.org/">OpenGL</a> in its 0.7 incarnation (<A href="http://boson.sourceforge.net/screenshots.php">screenshots</a>).  There is no AI yet, so a minimum of 2 players is required.  From <a href="http://www.unixreview.com/">UnixReview.com</a>: <i>"Do you miss the nice, slick GUI CD burning programs under Windows and Mac OS X? Or just want to stop using command-line tools to burn your CDs? K3b might just be what the doctor ordered."</i>  Check out <a href="http://www.unixreview.com/documents/s=7459/uni1037044484117/">the full review</a> of <a href="http://k3b.sourceforge.net/">K3b</a> including <a href="http://k3b.sourceforge.net/index.php?page=screenshots.html">screenshots</a>.  Finally, a couple of people wrote in to point out the launch of the <a href="http://www.kde-forum.de/">KDE-Forum.de</a> web forums for German speakers.  English web forums should be available soon.
<!--break-->
