---
title: "KCachegrind: Valgrind Unleashed"
date:    2002-09-04
authors:
  - "Otter"
slug:    kcachegrind-valgrind-unleashed
comments:
  - subject: "Compile errors"
    date: 2002-09-03
    body: "Seems very promising, but it gives compile errors:\n\ng++ -DHAVE_CONFIG_H -I. -I. -I.. -I/opt/kde3/include -I/usr/lib/qt3/include -I/usr/X11R6/include   -DQT_THREAD_SUPPORT  -D_REENTRANT  -O2 -fno-exceptions -fno-check-new  -c -o toplevel.o `test -f toplevel.cpp || echo './'`toplevel.cpp\ntoplevel.cpp: In method `void TopLevel::createActions()':\ntoplevel.cpp:229: parse error before `('\n\nAlso, the patch to valgrind 1.0.1 fails:\n\npatch: **** malformed patch at line 1802: @@ -729,9 +1696,7 @@ UCodeBlock* VG_(cachesim_instrument)(UCo\n\nI haven't looked into the problems yet, but I will. This is really an application I was looking for a long time."
    author: "Erik Hensema"
  - subject: "Re: Compile errors"
    date: 2002-09-04
    body: "Valgrind has already been packaged in Debian and Gentoo. Debian has\neven 1.0.1 in unstable. I don't know about other distros."
    author: "EK"
  - subject: "Packages"
    date: 2002-09-05
    body: "Mandrake has it too."
    author: "GF"
  - subject: "Re: Compile errors"
    date: 2002-09-05
    body: "Hmm, what does this have to do with anything? That's great that those distros have Valgrind packaged, but I see an alterior motive in your post OTHER than just trying to be a friendly user :) The reason this isn't relavent is because Valgrind 1.0.1 in ANY distro needs some patches for some enhanced profiling. Oh, don't let me forget:\n\n/me hands you a cookie for using Gentoo and Debian.\n\n\t\tNick Betcher\n\n-----------------\nQuote of the day:\n\"I'd like to get a refill for my meds.\" - patient\n\"I'm sorry sir, you're out of refills, we'll have to call your doctor.\" - pharmacy technician\n\"What are refills?\" - SAME patient"
    author: "Nick Betcher"
  - subject: "Re: Compile errors"
    date: 2002-09-04
    body: "The class KWidgetAction(...) is needed. But this class is new in KDE 3.1CVS\n...so: without KDE 3.1 (if you have 3.0) you can only compile it if you just comment the line out in which KWidgetAction appears. (does not matter that much .. \"just\" the filter will disappear than)\n\nCompiling with gcc 3.x gives a few errors, too. You have to remove the \"=0\" in the *.cpp files which the error-message tells you\n\nthere should be probably a fix soon (as much as I was told)"
    author: "Jens Henrik"
  - subject: "cool"
    date: 2002-09-04
    body: "cool cool cool.  Damn, this is cool.  Can't wait to run this app.\n\nThanks."
    author: "Chris Parker"
  - subject: "Re: cool"
    date: 2002-09-04
    body: "no doubt.. i've got the tarball sitting on my HD just waiting for me to have the time to play with it, but every time i look at the screenshots and read what it can do i get goose bumps.\n\na Free software profiler that takes into consideration things like cache misses, doesn't include the overhead of profiling in the profiling, has a wonderfully rich UI ..... damn!\n\n*grind is one of the most exciting sets of open source projects going on this year IMO."
    author: "Aaron J. Seigo"
  - subject: "KCachegrind: Valgrind Unleashed"
    date: 2002-09-04
    body: "Great tool!\nThis needs to be added to apps.kde.org so that more people will find and use it."
    author: "Nick"
  - subject: "In KDevelop?"
    date: 2002-09-04
    body: "This would be a pretty nice thing to integrate into KDevelop at some point in time. Would make it an even more fully featured application development center.\n\nNow, if I only could get Emacs as the editor in KDevelop. :)\n"
    author: "Chakie"
  - subject: "Re: In KDevelop?"
    date: 2002-09-04
    body: ">Now, if I only could get Emacs as the editor in KDevelop. :)\n\nYes, that would be nice!\nBut a KEmacs would be even better i think.\n"
    author: "[Bad-Knees]"
  - subject: "Re: In KDevelop?"
    date: 2002-09-04
    body: "Yes, it would be great :)\nMaybe some day, it's kind of fun (and maybe a little sad...) to read this now:\n\n\"Ga\u00ebl: Will there be a KEmacs? \nMatthias: XEmacs is the editor of choice for many of the KDE developers. In converse, some XEmacs-developers also mentioned interest in a tighter KDE integration. So the future regarding KEmacs looks pretty bright.\"\n\nin http://www.linux-center.org/articles/9809/interview.html\nSeptember 1998 :)\n\nJ.A."
    author: "jadrian"
  - subject: "Re: In KDevelop?"
    date: 2002-09-04
    body: "I remember someone mentioned an XEmacs KPart for KDevelop on the kdevelop ML\na while back, dunno how well it works.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Still Alpha Release..."
    date: 2002-09-04
    body: "Hi people,\n\nI think I'm forced to upload a more stable/compilable\nversion now :-) I never thought someone would mention it\nhere...\n\nI keep you up to date,\nJosef"
    author: "Josef Weidendorfer"
  - subject: "nice work !"
    date: 2002-09-04
    body: "great tool , gratulations !\n\ni hope more coders will use it and make kde\nreally fast.\n\nanother thing i am looking forward to , is\nthe caching of whole apps on invisible desktops,\n as mentioned on kde-core-devel"
    author: "chris"
  - subject: "fix for the patch......."
    date: 2002-09-04
    body: "to get the patch to apply, search for the \",53\" above line 1802, and then replace it with ,52.\n\n"
    author: "George Staikos"
  - subject: "New version of KCachegrind available..."
    date: 2002-09-05
    body: "Hi,\n\nI just uploaded new versions KCachegrind 0.1b and\nvalgrind-1.0.1 patch 0.1b.\n\nLook at the News section of the webpage for changes.\nAnd please give it a try.\n\nI should have all known bugs fixed :-)\n\nHappy profiling,\nJosef"
    author: "Josef Weidendorfer"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-06
    body: "Problem is that only Valgrind 1.0.2 is available now which your patch won't apply to :)"
    author: "Perra"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-06
    body: "Yeah, the patch for 1.0.1 fails on 1.0.2...\nBut one would hope for a synch between valgrinds cachegrind\nand kcachegrind so that the patch will not be necessary."
    author: "dnm"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-06
    body: "Wrong. Only the 1.0.2 =link= is available now. But the 1.0.1 archive is still\nthere: http://developer.kde.org/~sewardj/valgrind-1.0.1.tar.bz2."
    author: "Melchior FRANZ"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-06
    body: "Uhm, I downloaded the 1.0.2, it IS (or was) there, I tried to apply the patch \nfor 1.0.1 in the 1.0.2 directory and it fails. The patch does apply to\nthe 1.0.1 directory without complaining.\n\ncut from rpm-build -----------------------------------------------------\n-rw-r--r-- 501/501         105 2002-06-05 22:28:33 valgrind-1.0.2/tests/nanoleak.c\n-rw-r--r-- 501/501         491 2002-06-19 12:17:40 valgrind-1.0.2/tests/pth_pause.c\n-rw-r--r-- 501/501         996 2002-06-20 10:17:07 valgrind-1.0.2/tests/pth_sigpending.c\n-rw-r--r-- 501/501        2311 2002-08-25 22:02:25 valgrind-1.0.2/tests/pth_atfork1.c\n+ STATUS=0\n+ '[' 0 -ne 0 ']'\n+ cd valgrind-1.0.2\n++ /usr/bin/id -u\n+ '[' 505 = 0 ']'\n++ /usr/bin/id -u\n+ '[' 505 = 0 ']'\n+ /bin/chmod -Rf a+rX,g-w,o-w .\n+ echo 'Patch #0 (patch-0.1c-valgrind-1.0.1):'\nPatch #0 (patch-0.1c-valgrind-1.0.1):\n+ patch -p1 -s\n1 out of 1 hunk FAILED -- saving rejects to file configure.in.rej\n14 out of 35 hunks FAILED -- saving rejects to file vg_cachesim.c.rej\n3 out of 6 hunks FAILED -- saving rejects to file vg_include.h.rej\n1 out of 5 hunks FAILED -- saving rejects to file vg_main.c.rej\n2 out of 14 hunks FAILED -- saving rejects to file vg_symtab2.c.rej\n2 out of 2 hunks FAILED -- saving rejects to file vg_syscall_mem.c.rej\nerror: Bad exit status from /var/tmp/rpm-tmp.71239 (%prep)"
    author: "dnm"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-08
    body: "What are you trying to say?! That the 1.0.1-patch doesn't apply to the 1.0.2 binary? Surprise! As I mentioned above, you can still download the 1.0.1 binary, too which the 1.0.1 applies quite well (surprise again!)."
    author: "Melchior FRANZ"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-10
    body: "Well, the first thing I wanted to say is that 1.0.2 actually was there, \nwhich your post seemed to imply that it was not.\n\nI am not surprised... I am just stating facts, it's just that\n*cachegrind* 1.0.1 is not working too well (which is why\nJulian Seward released 1.0.2).\nAlso just so you know; patches can apply to newer versions,\nit just depends on how the new version was changed...\nmuch like a cvs merge can work without manual intervention,\na patch can work on a later version depending on what was modified\nin the new version. Yes, it is not as \"safe\" as a tested patch\nagainst a certain version but if you are not depending on it in a\nproduction environment, and what you got does not work, why not try it?\nAnd after I tried it I might tell others who are like of mind\nso that they would know it would not work right of the bat...\nThen you can decide if you want to do the patch manually in\nthe new code or just wait for someone to release a new patch...\nFor me cachegrind 1.0.1 gets an assert almost all of the time\nbut not with 1.0.2 (yes, unpatched)."
    author: "dnm"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-10
    body: "> Well, the first thing I wanted to say is that 1.0.2 actually\n> was there, which your post seemed to imply that it was not.\n\nOh well. My posting was the answer to Perra, who said \"Problem is that\nonly Valgrind 1.0.2 is available now which your patch won't apply to.\"\nI said that the link to 1.0.1 may have been removed, but the binary was\nstill there and posted the link. So there would be no problem to get\na working, patched cachegrind with a matching kcachegrind, even though\nit wasn't the very last version. Then you =repeated= that the 1.0.1 patch\nwouldn't apply to 1.0.2, which was already stated before and not really\nsurprising. Yes, my answer was a bit harsh, sorry. But it was easily\ntopped by two replies, presumably by the same child using different names\nand with really bad manners. He should wash his mouth with soap. ;-)"
    author: "Melchior FRANZ"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-10
    body: "... but meanwhile there's a 1.0.2 patch anyway. Thank you, Josef!  :-)"
    author: "Melchior FRANZ"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-11
    body: "Ok, trying to clarify (never been that good doing that lol)\nWhen I read your statement \"Only the 1.0.2 =link= is available now\"\nI thought that you had tried to access the 1.0.2 version during a transition,\nie the link to the 1.0.1 file was removed but the tar file was/is still there\nand a new link had been added to the 1.0.2 tar file but the tar file it pointed\nto was not uploaded yet (or temporarily removed).\nSo when I posted again the information I thought I provided was that \nyou can download it now + which files the patch failed for.\nAlso 1.0.1 cachegrind has never really worked for me.\n\nI will go try the new patch now though!"
    author: "dnm"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-24
    body: "Aaaaand.... as an example of patching sometimes working for closely related versions:\nthe patches V 0.2a and 0.1e both for valgrind 1.0.2,\nshould be used for 1.0.3.\nquote from kcachegrind site:\n\"Update: Both Valgrind patches work for 1.0.3, too (There is one reject because of the wrong version number, which can be safely ignored).\"\n\n(Yes, I have tested this and it works)"
    author: "dnm"
  - subject: "Re: New version of KCachegrind available..."
    date: 2002-09-08
    body: "you're not too bright aren't you?"
    author: "bloemist"
  - subject: "Offtopic ..."
    date: 2002-09-08
    body: "Whats going on with mosfet.org ?"
    author: "lescrh"
  - subject: "Re: Offtopic ..."
    date: 2002-09-09
    body: "He got flamed down by Slashdot."
    author: "Stof"
  - subject: "Re: Offtopic ..."
    date: 2002-09-09
    body: "and flamed b  mos of he kde communi\n\n\n9sorr m shif, , and keboard kes are no working."
    author: "dc"
---
<a href="http://developer.kde.org/~sewardj/">Valgrind</a>, a GPL'd tool to debug applications' memory allocation, is one of the most exciting profiling tools in the free software world. Think <a href="http://rational.com/products/purify_unix/index.jsp">Purify</a>, but free, easier and available for Linux. Now Josef Weidendorfer makes Valgrind even more attractive with the <A href="http://lists.kde.org/?l=kde-devel&m=103058238931947&w=2">first unofficial</a> release of <a href="http://www.weidendorfers.de/kcachegrind/index.html">KCachegrind</a>, a KDE front-end for Valgrind's profiling tool. (Click for screenshots of <a href="http://www.weidendorfers.de/kcachegrind/screenshot1.html">basic use</a> and a <a href="http://www.weidendorfers.de/kcachegrind/screenshot2.html">call tree graph</a>.) Now all x86 Linux developers can benefit from a professional grade profiling tool!

<!--break-->
