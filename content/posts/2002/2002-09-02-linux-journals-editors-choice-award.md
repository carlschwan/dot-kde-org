---
title: "Linux Journal's Editor's Choice Award"
date:    2002-09-02
authors:
  - "pfremy"
slug:    linux-journals-editors-choice-award
comments:
  - subject: "one award...too bad"
    date: 2002-09-02
    body: "<i>Congratulations, KDE developers! :-)</i>\n<br><Br>\n[ed: seems to be the joe99/dude13/Vitamin C/Mineral K/... troll again. pre-banned user. see html comment if you really want to gratify him and know what he wrote.]\n\n<!--\nso other than KDE's \"best Consumer Software\", KDE didnt win anything else... sure you guys may have a good desktop, but it lacks the *decent* apps,..no konq isnt better than galeon, kmail is definietly not better than evolution, and theres no comparison with gimp.\n\n# Communication Tool: Ximian for Evolution\n# Graphics Application: The GIMP\n# Web Client (Tie): Mozilla and Galeon\n\n\ngo the foot :)\n\n-->"
    author: "grapefruit"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "grapefruit:  If you are such a cheerleader for Gnome (\"go the foot\"), why are you posting on a KDE news and discussion site?"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "relax, Evan, he's kinda right, KDE is missing the really good apps as of now. That'll change hopefully soon, but right now, it's the sad truth."
    author: "me"
  - subject: "Re: one award...too bad"
    date: 2002-09-03
    body: "I love KDE and am very happy that they got an award for the desktop.  There are a number of apps that are really nice in KDE also.  I do use Galeon for my web browsing though.  It is a clean interface that works well It stays out of my way when I am browsing.  It also handles many pages that browsers like Opera and Konqueror just start choking on.  The others seem to not be able to do things as well.  \n\nI think that it is a mistake to link to closely the desktop with the apps.  That is just a M$ way of thinking.  I use Open Office for my office suite.  I still use Gnome toaster for cd burning (it's the best of the not so good graphical front-ends that I have found).  I really like Kwrite when I am editing text in a graphical environment but use VI when I am in text mode.  \n\nI have always felt that using Linux is about using what I think is best of breed.  That is best of breed for each item, not to be locked down to a whole environment for all the software packages.  Some people like emacs, Konqueror, etc. and that is good for them.  It hurts the development when people just get in a pissing match about \"mine is better than yours\".  Take some time to look at what others like in other packages and what they hate and everybody will win. "
    author: "Rusty"
  - subject: "Re: one award...too bad"
    date: 2002-09-03
    body: "I also thought there were only \"not so good\" cd burning front-ends util I tried K3B (http://k3b.sourceforge.net). Just try it, it's easy to use (DND and co.), very nice looking and... it WORKS !"
    author: "Julien Olivier"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: ">no konq isnt better than galeon\n\nYes it is!\n\n>kmail is definietly not better than evolution\n\nYes it is!\n\n>and theres no comparison with gimp\n\nNo there is'nt!\n\nAnything else you wanted to say?"
    author: "[Bad-Knees]"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "Are gnome message boards still down?"
    author: "Anonymous"
  - subject: "Stupid fscking comments"
    date: 2002-09-02
    body: "Please.. the dot used to be a quiet, friendly place with little flamebait like this. Guess as soon as a site gets past the \"known only by the people who enjoy it\" stage and becomes more widely known, this is bound to happen. A shame, though.\n\nReading comments here used to be fun and uplifting. No more."
    author: "Johnny Andersson"
  - subject: "Re: Stupid fscking comments"
    date: 2002-09-02
    body: "It's the GNOMEs organized trolling effort."
    author: "ac"
  - subject: "Re: Stupid fscking comments"
    date: 2002-09-02
    body: "Yet another insightful conspiracy theory from ac."
    author: "Stof"
  - subject: "Re: Stupid fscking comments"
    date: 2002-09-26
    body: "sad\n\ndivide the sheep,\ncontrol the herd.\n\n\n"
    author: "m$heaperd"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "KMail beats that bloated Evolution/Outlook crap hands down!\n\nKonqueror rules the world.  Galeon is not only butt-ugly, its rendering leaves much to be desired.\n\nGraphics Application: Look at Mosfet Paint. :/\n\nAnything else?"
    author: "ac"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "Sure, if mosfet.org comes back online agaian some day! (that was the right address, right?)"
    author: "Johannes Wilm"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "<i>pre-banned abuser</i>\n<!--\n> KMail beats that bloated Evolution/Outlook crap hands down!\n\nevolution still kicks ass, and it shows (it got an award)\n\n> Konqueror rules the world. Galeon is not only butt-ugly, its rendering leaves much to be desired.\n\nKonq is just a wannabe IE. Galeon renders as good as moz (funny that)...keep on waiting for your konq-tabs. Its old news for galeon and opera users.\n\n> Graphics Application: Look at Mosfet Paint. :/\n\nthis cant even compare to 5% of gimps functions, its a joke. cant you live with a gnome/gtk app that does the job, accept it, theres no KDE/QT comparison to gimp.\n\n> Anything else?\n\nSure! some Gtk/Gnome apps..\ngaim, xmms, pan, abi, gnumeric, dia, xchat\n\nthe k* versions of these apps suck, they're feature-less, less mature, crash more often, less developers, less organised...if you want a real joke, look at koffice in comparision. har har \n-->"
    author: "kiwifruit"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "\"Sure! some Gtk/Gnome apps..\ngaim, xmms, pan, abi, gnumeric, dia, xchat\"\n<br>\nXMMS is not a GNOME-app. GNOME can't claim credit for XMMS or anything related to it. Besides, I happily use it in my KDE-desktop. <flamesuit = on> Just typical... GNOME tries to take credit for Gtk-apps that have nothing to do with GNOME."
    author: "Janne"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "<i>pre-banned abuser</i>\n<!--\n> \"Sure! some Gtk/Gnome apps..\n\nGTK is the common toolkit, xmms/gaim isnt a gnome app, but has the same toolkit as used by gnome. QT-only apps are very-far and few between. GTK is still a very popular toolkit, more so than QT. examples like edonkey, xsane, freeciv, mplayer  are more native-looking in a GTK/GNOME environment, with GTK+ themes kicks in, where buttons/dialogs etc have a similar feel, which is obvious since gtk+ handles that. You jumped on this guy because you assumed he meant they're gnome apps, what he was probably meaning \"(Gtk or Gnome)\" not strictly Gnome.\n\nIts kinda of strange how you like to bring posters down, who make holes in the kde desktop, KDE isnt perfect, neither is Gnome. But when some say KDE isnt perfect, you flame them heavily. The orginal poster was right, you have a good desktop, but you lack decent applications, Gnome has the applications but lacks the complete desktop. Sure people may think K* is the best thing since sliced bread, in truth, its not. Learn to accept that not every KDE application is as good as the GTK+ (or) Gnome versions. \n\nThis isnt a Flame. Dont take it the wrong way. \n\n--\ndude_13\n-->"
    author: "dude_13"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "\"Its kinda of strange how you like to bring posters down, who make holes in the kde desktop, KDE isnt perfect, neither is Gnome. But when some say KDE isnt perfect, you flame them heavily. The orginal poster was right, you have a good desktop, but you lack decent applications, Gnome has the applications but lacks the complete desktop. Sure people may think K* is the best thing since sliced bread, in truth, its not. Learn to accept that not every KDE application is as good as the GTK+ (or) Gnome versions. \"\n===========================\n<rant>\nYou keep on talking about \"you don't have the apps\". So I assume that you don't run KDE? Then why are you here? What purpose does it serve to come here and try to ruin others discussions? KDE won an award. You can't just leave it at that, you (and I mean all of you) have to come here and start shouting \"Nah, KDE still blows. You may have decent desktop, but your apps suck!\". What does it matter to you if KDE won an award? It just seems like you people are annoyed when KDE wins something. Why don't you go play with your own desktops then and leave KDE-users with their desktops?\n</rant>"
    author: "Janne"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "> So I assume that you don't run KDE?\n\nyou assume too much.\n\n\n--\ndude_13"
    author: "dude_13"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "Why when faced with criticism about their product, do some people turn into hotheads? Why can't they take the valid criticism and ignore the jibes. I run BOTH desktops and I get sick and tired of little children fighting all the time - my dad is bigger than your dad. It puts a lot of normal people off.\n\nTake the criticism, which shows maturity and if you disagree with this person you can show him with better applications."
    author: "John"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "i am really getting sick and tired of this stupidity.\n\nhere's some insight for all of you that evidently lack the ability to procure it on your own:\n\n o if you are going to highlight the weak points of product, highlight ones that are worth highlighting. the issues you bring up are by no means the most interesting nor the most factual nor the deepest issues KDE has. you are right that KDE isn't perfect, but if you could demonstrate some ability to see WHAT wasn't perfect instead of parroting the same tired nonsense then perhaps people would take what you say a bit more seriously.\n\n o remember Thumper in Bambi? \"if you don't have nuffin' good to say...\" seriously, why do people need to say negative things at every opportunity? how about pass up every other urge to say something negative? we'd be half as negative and the message would still get through, i assure you!\n\n o if you want to chat up GNOME, Windows, MacOS,<insert something that isn't KDE related> go somewhere where you are actually on topic. it's like discussing growing watermellons on a board about meteorology. W T F?\n\nthose who are posting negative, angst-driven, controversial posts for no reason other than to make themselves feel good, put down others, start arguments, do something with their empty hours, etc are not doing anything to help anyone. \n\nthis is about a community, people. and those of you flaming back and forth are tearing it apart. there are people who are providing input into the KDE project that i personally find annoying or disagree with, but i value their input and therefore treat them and their ideas with respect, as do the vast majority of others. so put some positive effort into things or fuck off, for all i care."
    author: "Aaron J. Seigo"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "Konqueror is not just a Web Browser so it is a mistake comparing\nthem."
    author: "KDE User"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "> evolution still kicks ass, and it shows (it got an award)\n\nI agree.. no need to lambast a good program just because it uses another toolkit. Evolution is a fine program, and so is kmail :)\n\n> Konq is just a wannabe IE. Galeon renders as good as moz (funny that)...keep on waiting for your konq-tabs. Its old news for galeon and opera users.\n\nKonq isn't exactly a wannabe IE, but  I agree that Mozilla/Galeon still render a bit better than Konq. \"Tabs\" themselves are nothing new in galeon. They were in opera for a long time before galeon came around.\n\n> this cant even compare to 5% of gimps functions, its a joke. cant you live with a gnome/gtk app that does the job, accept it, theres no KDE/QT comparison to gimp.\n\nI agree,  hopefully the GIMP developers will FINALLY make GIMP toolkit independent in gimp 1.4/1.5/2.0, like they have been promising to for the last 128812178921289 years. ;)\n\nOn the other hand, looking from another perspective, you could have said the same thing about GIMP vs. Photoshop 7. While GIMP is close to Photoshop 2.1, 2.5, or (a little bit further from) 3.0, it's still waaaay far away from PS 5.0-7.0.\n\n>  Sure! some Gtk/Gnome apps..\n> gaim, xmms, pan, abi, gnumeric, dia, xchat\n\n> the k* versions of these apps suck, they're feature-less, less mature, crash more \n> often, less developers, less organised...if you want a real joke, look at koffice in \n> comparision. har har \n\nI think this has seriously changed in the last six months. Kopete is getting nearly as good as gaim, noatun as good as xmms (and I couldn't stand noatun until recently, but now it's pretty usable), knode has always been as good as pan, and koffice 1.2/cvs is a vast improvement. In kde 3.1, ksirc has fixed a number of long standing bugs, and kvirc has always been (arguably) much more featureful than xchat. Of course, I still use irssi on the console :)\n"
    author: "fault"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "\"I think this has seriously changed in the last six months. Kopete is getting nearly as good as gaim, noatun as gRe: one award...too badood as xmms (and I couldn't stand noatun until recently, but now it's pretty usable), knode has always been as good as pan, and koffice 1.2/cvs is a vast improvement.\"\n\nI can't comment on the messengers (aim + kopete) since I don't use those, but I definitely agree with you on the subject of noatun getting as good as xmms in the recent past.  \n\nWhere I disagree with you is the news clients.  Pan is capable of decoding multi-part binaries, has a task-manager, bandwidth management and handles yEnc (I could continue...).  Last time I checked, knode wasn't capable of these things.  While knode is a good newsreader, pan is an excellent one.\n\nAbout koffice:  Haven't tried it since 1.1.1.  As soon as it handles doc-imports as well as open office it will become my default office.  As has been pointed out a lot:  there is no such thing as a gnome office, just an assortment of applications.\n"
    author: "joerg gastner"
  - subject: "Re: one award...too bad"
    date: 2002-09-03
    body: ":: Where I disagree with you is the news clients. Pan is capable of decoding multi-part binaries, has a task-manager, bandwidth management and handles yEnc (I could continue...). Last time I checked, knode wasn't capable of these things. While knode is a good newsreader, pan is an excellent one.\n\nWhereas I don't do binaries over usenet, so none of those apply to me whatsoever.  I care about reading and posting short text.  KNode works for me fine.\n\nWhich brings out an important point - some of these apps that are supposedly filling the same role are totally different.  Evolution versus KMail, for instance.  I don't *want* a kitchen sink - I want an email application that is small and simple.  I prefer KMail over Evolution or Aethera.  What you want in an \"email application\" might be different.  So you choose a different application.  I'm not going to say the simplicity and stability of KMail makes it \"better\" than Evolution any more than I feel like wading into vi versus emacs - which is a *very* similar arguement.\n\nAs for Konqueror versus Galeon (I don't use Mozilla - again, I like simple fast apps), I vastly prefer Konqueror's rendering.  I am developing a site and checking in Galeon every once in awhile for Moz/NS compatability, and I just don't like the way Mozilla renders pages.  That's not a \"better\" or \"worse\" or even \"simple\" versus \"featureful\", that's just a asthetic choice.  Luckily, you can configure Konqueror to use gecko to browse sites if you prefer the way it renders pages.  (I don't do that for this project so I can keep cookies and accounts seperate).\n\n:: About koffice: Haven't tried it since 1.1.1. As soon as it handles doc-imports as well as open office it will become my default office.\n\nI installed this last beta, and have been using it heavily.  I don't deal with lots of MS office files, so that's not an issue - I do send stuff out, and I just PDF them.  I am (was) a MS Word junkie - a serious poweruser with all the key commands memorized and the ability to make really nice looking forms and documents very quickly with it.  I have paid for every version of CorelOffice for Linux, HancomOffice, etc.  With this latest release (the beta), KWord has become the first modern word processor (i.e. light DTP application) on X that I like.  It's fast, offers the fundimental stuff I want to do, and works.  It's certainly missing features, and the latest features are a big roughly integrated, but I really like where it's going.  And yes, I am the kind of person who can get excited over Word Processors.  I've been using them since the 70's, and I think Word 97 is the best to date.\n\nBut then, as I say - opinions differ.  :)   I don't think there is a \"best\" in something as vague as an email application or newsreader.  You obviously care very much about being able to download files off of usenet.  I use groups where you get *plonked* if you accidently post a binary.  Is it likely that our opinons on what the \"best\" newsreader is will differ?  Probably.\n\n--\nEvan (who uses XMMS and XChat, and dosen't have gnome installed on his system (actually, I think I had to install it for galeon))"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: one award...too bad"
    date: 2002-09-03
    body: ">  I installed this last beta, and have been using it heavily. I don't deal with lots of MS office files, so that's not an issue - I do send stuff out, and I just PDF them.\n\nYay ! How do you PDF kword documents ? I don't see any export filters, and the only way i can think of is printing a file to postcript and then converting it manually, but is there a nicer way ?"
    author: "jmk"
  - subject: "Re: one award...too bad"
    date: 2002-09-03
    body: "File -> Print, Select \"Print to File (PDF)\" in the printer name list. Works in every application using kprinter (i.e. virtually all KDE programs). \n\n\n"
    author: "Sad Eagle"
  - subject: "Re: one award...too bad"
    date: 2002-09-02
    body: "Now I hope you guys don't count him as part of the GNOME community.\n\nGaleon is better than Konqueror as a browser (though Konqueror is becoming better), but Konqueror is definitely better than Nautilus as a file manager.\nEvolution vs KMail - there's no comparison, they are 2 different things with their own advantages and disadvantages."
    author: "Stof"
  - subject: "umm"
    date: 2002-09-02
    body: "You people are totally off. Galeon is total crap. It shouldn't even qualify, because it's dependand on Mozilla. Now how cheesy is that ? If you don't believe me, try to Upgrade to Mozilla 1.0 after a clean install of Linux, and you'll get the dependancies message. And as far as KDE not having any good apps - well let's run down the list.\n\nQuanta Plus - Html Editor\nKdevelop - C++ IDE\nQt Designer\nKPPP - Dial up Tool\nKoffice\n\n\nNow lets review. Qt Designer is better than Glade. And Gnome doesn't even HAVE anything to compete with KDevelop, Quanta, or KPPP. Now what if you're a programmer or a hmtl coder - you would be left to search for programs for Linux. Hell how would you even connect to the internet ? And We all know Kwrite is better than Gnome's Gedit. And incase you didn't know - The Gimp is NOT a part of Gnome - if you noticed when you run KDE it's not located under the Gnome menu - that's because it's a program that just comes with Linux, like AbiWord or Emacs. If you are going to critisize KDE on it's own forum, at least do a decent evaluation...\n\n-chillin"
    author: "Chris Spencer"
  - subject: "Re: umm"
    date: 2002-09-02
    body: "> You people are totally off. Galeon is total crap.\n\nAnd yet it's my favorite browser. Heck, I even use it as my default browser in KDE!\n\n> It shouldn't even qualify, because it's dependand on Mozilla.\n\nCode Sharing(tm) is a Good Thing(tm). Why duplicate efford when we can use Mozilla's excellent rendering engine? Say whatever you want about Mozilla or extra dependencies, but Mozilla's rendering engine is *good*.\n\n\n> And Gnome doesn't even HAVE anything to compete with KDevelop\n\nAnjuta\n\n\n\nNow can we all stop the flamewar? I'd rather see Gimp killing Photoshop than that KDE duplicates efford by developing it's own image editor just because it can be part of KDE. (And no, I don't care that GTK+ apps look different than QT apps)"
    author: "Stof"
  - subject: "Re: umm"
    date: 2002-09-02
    body: "> Heck, I even use it as my default browser in KDE!\n\nWhy does this sound kind of stupid to me?"
    author: "KDE User"
  - subject: "Re: umm"
    date: 2002-09-03
    body: "Because I am not you."
    author: "Stof"
  - subject: "Re: umm"
    date: 2002-09-03
    body: "It's not stupid. A lot of people use Mozilla and Galeon inside of KDE. You don't HAVE to run Konqueror if you don't want to. Just because you use KDE doesn't mean it's sacriledge to run programs using non KDE/Qt toolkits. I currently use Mozilla because it gives me the same interface between WindowsXP and Linux. The significant majority of KDE/GNOME users use applications from each other's desktops. This is how it works in the real world (tm)."
    author: "fault"
  - subject: "Re: umm"
    date: 2002-09-06
    body: "Looking at it from a usability point of view, the KDE desktop\nwas created with this functionality integrated. Using some other program\nthat does the same thing....then it\u00b4s not KDE anymore,\nat least not for me. Also, Konqueror is not a web-browser,\nit is a plug-in thing that fits inte the KDE experience."
    author: "KDE User"
  - subject: "Re: umm"
    date: 2002-09-06
    body: "The fact is that (AFAIK), the majority of KDE users don't use exclusively KDE apps. I'm sure many developers do, and some users do, but most users don't.\n\nThis stems from the fact that X11 historically has had a wide variety toolkits that applications use. Hell, before KDE/GNOME/CDE, most applications had their own GUI toolkits. People are just use to having several different toolkits in usage at the same time. I don't think most current users used X11 from that era, but the tradition is still there I think.\n\nYou're right about Konqueror not being a web browser though (although ironically it's a very nice web browser) :)"
    author: "fault"
  - subject: "Re: umm"
    date: 2002-09-07
    body: "> The fact is that...\nReally? You have any statistical data on that?\n\n>(AFAIK)\nAh\n\nAnyway, I was only talking about Konqueror/Mozilla. Of course, if you find some\nweird app that you want to use, there\u00b4s no problem with that. There\u00b4s\nno point in getting absurd: only KDE-apps! nothing else! etc..."
    author: "KDE User"
  - subject: "Re: umm"
    date: 2002-09-08
    body: "> Really? You have any statistical data on that?\n\nOf course not. It's hard to provide much solid statistical data on much relating to _desktop_ users on Unices.\n\nI'm just relying on the fact that I haven't met a KDE user (yet), who doesn't have something like gtk+ installed. Of course, it might just be me :)\n\n> Anyway, I was only talking about Konqueror/Mozilla.\n\nAh, ok. I'll have to agree with you that (AFAIK) the majority of KDE users use Konqy. Of course, Mozilla (and Netscape before it), were/are always desktop/OS independent. \n\n>  Of course, if you find some weird app... that you want to use, there\u00b4s no problem with that.\n\nOr another app that you prefer.\n\n> There\u00b4s no point in getting absurd: only KDE-apps! nothing else! etc...\n\nYup, I totally agree."
    author: "fault"
  - subject: "Re: umm"
    date: 2002-09-07
    body: "<I>\"Using some other program that does the same thing....then it\u00b4s not KDE anymore,\"</I>\n\nWhat is your point and why should I care that it's not \"pure\" KDE anymore? Likewise, why should I care that GNOME isn't \"pure\" anymore if I use KDE apps inside GNOME?\nUse the right tool for the right job. And right now, Galeon *is* the right tool for me. I couldn't care less wether it's written for GNOME or KDE."
    author: "Stof"
  - subject: "Re: umm"
    date: 2002-09-07
    body: "> Use the right tool for the right job\nOf course, you are free to use any application you like, as I said in earlier posts: I'm not telling people what they should use or not.\n\n> What is your point\nMy point is that the KDE-developers put a lot of effort into their desktop environment, that environment includes Konqueror. Clearly, if you switch Konqueror for something else that does not have the same functionality, you do change KDE as a whole."
    author: "KDE User"
  - subject: "Re: umm"
    date: 2002-09-08
    body: "> Clearly, if you switch Konqueror for something else that does not have the same functionality, you do change KDE as a whole.\n\nHow is this changing KDE? If you don't use Konqueror to move around files but instead use \"mv\" in a terminal window, are you changing KDE? If so, I'd say that *every* single KDE user/developer is changing KDE in some way. Perhaps it's KDE developers themselves who are doing it. Perhaps it's a developer using (x)emacs instead of kate/kdevelop (like many developers do). Perhaps it's a developer using cvs instead of cervisia, etc..\n\nThis can be related to Konqueror and Mozilla.  Kate has features that xemacs doesn't (like KDE integration), and xemacs has features that kate doesn't. This is the same thing with Konqueror and Mozilla. Both have features that the other one does not. It's not right to say \"switch Konqueror with something that does not have the same functionality\", because Mozilla will never have the same functionality that Konqueror has, and Konqueror will never have the same functionality that Mozilla has.\n\n\nOnce again, as other posters have emphasized, use the right tool for *you* that does the job."
    author: "fault"
  - subject: "Re: umm"
    date: 2002-09-08
    body: "> ..but instead use \"mv\" in a terminal window..\nI guess I have to emphasize the \"switch\" here. Clearly, simply using some app lying around on your system does not change KDE.\n\n> use the right tool for *you* that does the job\nOf course.\n\n> It's not right to say \"switch Konqueror with something..\nOf course it is. Because Konqueror is a part of KDE. Mozilla is not.\n"
    author: "KDE User"
  - subject: "Re: umm"
    date: 2002-09-16
    body: "> Clearly, if you switch Konqueror for something else that does not have the same\n> functionality,\n\nIt is not \"switched\". It's just a different default. *Konqueror is still available*!\n\n\n> you do change KDE as a whole.\n\nThat's the idea. RedHat is trying to make GNOME and KDE look and act consistent."
    author: "Stof"
  - subject: "Re: umm"
    date: 2002-09-06
    body: "> Now can we all stop the flamewar? I'd rather see Gimp killing Photoshop than > that KDE duplicates efford by developing it's own image editor just because > it can be part of KDE. (And no, I don't care that GTK+ apps look different  > than QT apps)\n\nWell, GIMP is a great app, I agree. But it suffers some usability problems that could (should) be fixed by making a KDE frontend to it:\n\n-Its UI is totally unusable. You can't minimize the whole app, you have to minimize each dialog and each image at a time. There are different menus for the app and the images, etc...\n\n-You can't drag a file from your desktop to GIMP. It seems to work when GIMP has no picture loaded but it doesn't work if it has at least on picture loaded.\n\n-If you open a picture using GIMP by clicking on it, it opens GIMP anew even if GIMP has already been started. Then you have 2 GIMP main windows, two brushes dialogs etc... and no way to know which GIMP controls which picture.\n\nIn conclusion, GIMP is a great tool if you except USABILITY. That's why, when GIMP UI will finally be separated from its core, KDE guys should really make a frontend to it. I even think GNOME guys should do it too. So that GIMP becomes really usable."
    author: "Julien Olivier"
  - subject: "Re: umm"
    date: 2002-09-06
    body: "Indeed.. it'd be good to have a KDE frontend to GIMP, not a KDE equivalent of GIMP. But of course, projects like KDE are developer driven, so if a developer has interest to make a GIMP-like app, especially if the GIMP developers don't split the core from the interface by the next major version (gimp 1.6/2.0), like they've said for the last three years, then by all means make an equivalent for the reasons detailed by parent poster :)\n\n"
    author: "fault"
  - subject: "Re: umm"
    date: 2002-10-05
    body: "Kimp ?\n"
    author: "GF"
  - subject: "Re: umm"
    date: 2002-09-13
    body: " <i>-If you open a picture using GIMP by clicking on it, it opens GIMP anew even if GIMP has already been started. Then you have 2 GIMP main windows, two brushes dialogs etc... and no way to know which GIMP controls which picture.</i><p>\n\nUmm.. That happens if your doubleclicking is bound to \"gimp $file\", if however instead you \nuse \"gimp-remote -n $file\" then the existing gimp-process will be told to open the file. the \"-n\" switch tells\ngimp-remote to start a new gimp if no existing one is found.<p>\n\nThis is a simple bug in kde, can be fixed by \"s/gimp/gimp-remote -n/\" in the apropriate place."
    author: "Eivind"
  - subject: "Re: umm"
    date: 2002-09-13
    body: "Hmmm\n\nI didn't know that. Thanks !\n\nAnyway, why not assigning this action as default for 'gimp' command ?"
    author: "Julien Olivier"
  - subject: "Re: umm"
    date: 2002-09-16
    body: "\"-Its UI is totally unusable. You can't minimize the whole app, you have to minimize each dialog and each image at a time. There are different menus for the app and the images, etc...\"\n\nUse virtual desktops. That's what I always do.\nAnd just because you can't minimize the whole app in one click doesn't make the UI \"totally unusable\". How do you think people draw their artwork if they can't use the UI? Clearly, the UI *is* usable, you just need time to get used to it. I got used to it, the UI is very much usable.\n\n\n\" -You can't drag a file from your desktop to GIMP. It seems to work when GIMP has no picture loaded but it doesn't work if it has at least on picture loaded.\"\n\nThen how come Gimp opens the picture just fine when I drop a file on it?\n\n\n\" -If you open a picture using GIMP by clicking on it, it opens GIMP anew even if GIMP has already been started. Then you have 2 GIMP main windows, two brushes dialogs etc... and no way to know which GIMP controls which picture.\"\n\nBut this problem isn't big enough to make Gimp \"totally unusable\". Learn to use Gimp's file dialog! Hint: it has tab completion, just like every GTK+ file dialog.\n\n\n\"In conclusion, GIMP is a great tool if you except USABILITY.\"\n\nI can USE Gimp just fine, so it is USABLE."
    author: "Stof"
  - subject: "Re: umm"
    date: 2002-09-02
    body: "http://www.gnome.org/softwaremap/projects/gimp/\n\ndid you forget to take your pills again?\n\n"
    author: "Cabbage"
  - subject: "Re: umm"
    date: 2002-09-02
    body: "No it's not a gnome app. It just uses Gtk+. It it were a gnome application, it would have to use the whole framework to integrate."
    author: "Stephan"
  - subject: "Re: umm"
    date: 2002-09-02
    body: "I often find myself that when people are critiquing KDE, they mean the projects designed exclusely for KDE by a design team. When people are judging Gnome on the other hand, they include ANY program written in GTK+, such as The Gimp or Gaim to name a few. Both desktop enviroments are worth using, but this kind of judgement is a bit unfair and slanted in the favor of Gnome. And I agree with the other poster, can you really count Galeon as a top broswer when it's just a dependant of Mozilla ? People on here may say that Konqueror is cheesy or a rip-off of Internet Explorer, but at least they coded it themselves. I also agree with the other poster - Konqueror is a better file manager than Nautilis. I'm just curious, if people dislike KDE and think their apps are SOO inferor, then why bother comming here with a bad attitude in the first place. I know you sure as hell wouldn't catch me dead on a Windows XP forum, so why are you here ?\n\n-chillin"
    author: "Chris Spencer"
  - subject: "Not AGAIN..."
    date: 2002-09-02
    body: "It seems that once again this site has become an outlet for people to abuse one another. I'm quite convinced that there is a small group of trolls who have no interest in either KDE or GNOME (or Linux in general), but who need their ego massaged by deliberately inciting and offending other people. Don't you guys have something better to do with your time?\n\nSlightly more on-topic, I don't see the need for relying on exclusively KDE or GNOME applications. Sure, their visual appearance is different (so maybe Red Hat had the right idea about unifying themes?), but personally I like to use the best tool for the job. I use KDE as my day-to-day desktop, but I when it comes to the actual apps I use Mozilla, KMail, KDevelop, GVim, KPPP & Open Office... because I just find they suit my needs. Why should I HAVE to choose one set of applications? And just because Konq/Galeon/Opera/Emacs/whatever fulfills your needs, why assume that said application will be perfect for everyone else?\n\nHopefully the trolls will tire of posting abuse here and leave... I've actually really enjoyed visiting this site in the past, and it would be a pity if a small group of morons gave this site a bad name.\n"
    author: "Delete"
  - subject: "Re: Not AGAIN..."
    date: 2002-09-02
    body: "> Im quite convinced that there is a small group of trolls who have no interest \n> in either KDE or GNOME (or Linux in general), but who need their ego massaged \n> by deliberately inciting and offending other people.\n\nIt's a miracle! Somebody who gets it!"
    author: "Stof"
  - subject: "Re: Not AGAIN..."
    date: 2002-09-02
    body: "I think the name-calling editors will do a better job of giving the site a bad name than anonymous posters."
    author: "Neil Stevens"
  - subject: "Re: Not AGAIN..."
    date: 2002-09-02
    body: "Which ones would those be Neil?"
    author: "Delete"
  - subject: "Re: Not AGAIN..."
    date: 2002-09-03
    body: "Neil, I'm sorry for calling you a !@#$ time and time again.  I didn't know you'd take it *so* personally... ;-)"
    author: "Navindra Umanee"
  - subject: "I agree"
    date: 2002-09-02
    body: "You are right - I just get a little irritated when idiots like the one grapefruit senlessly bash a decent program. I tend to forget sometimes that people like him want exactly that - to cause irritation and insite arguments. Next time I'll just ignore idiots who look to cause conflict. And I agree, there isn't any reason to soely use one desktop environment over the other - I mean some people take sides viciously like they actually work on the damn project or something. But as far as my personal opinion (stated in a calm mannor): after a clean installation of Linux RedHat (just for example), KDE has software advantages over Gnome, and RedHat has been known for being biased towards Gnome. Personally, I'm not biased towards either, nor do I have any attachments - I just want to use the best software for Linux, and KDE has been that for me thus far...\n\n-chillin"
    author: "Chris Spencer"
  - subject: "Trolls need to die"
    date: 2002-09-02
    body: "\"Blablabla GNOME apps are better than KDE ones \nblablabla KDE is a better Desktop than GNOME blablabla ...\"\n\nIf I was not a linux user for 4 years, I believed our are takling about Batman vs. Spiderman or Pokemon vs. Digimon :)\nCan you (Yes you behind your screen) stop trolling unusefully ?\n\nKDE and GNOME are differents and have different goals :\n- GNOME integrate different applications to integrate in a desktop but their design/ergonomy/communication protocol are different so know GNOME team as to work with the developpers to integrate bonobo, gconf, ergonomy norms. GNOME/Gtk apps are good but inhomegenous (If somebody can translate this in good english 'cause i am french ?) between them.\n- KDE has work a lot on unification of applications : ergonomy, protocol,.... that's why DCOP, kparts,... are the squeleton of most of the KDE apps. For a long, GTK apps beat KDE one (gimp, xmms, abiword,...) but know it is not true (except for gimp). \nnoatun is very interesting as an alternative for xmms. It is completely (heaven gui) pluggeable and integrated in the KDE desktop. xmms has more plugins but is not integrated in GNOME.\nkoffice is an integrated office envirronement. Abiword + gnumeric + ... are much in advance but are not integrated :(\nkmail vs. evolution  depends of the user. They are very differents and can't really be compared (To my mine)\n...\n\nSo : KDE and GNOME are differents but none of them is better than the other and unfortunetally nobody can say if I have a more long than yours :)\n\n\nTrolling is an difficult art but none of these page's trolls are aristics ;)\nStop trolling !  \n"
    author: "Shift"
  - subject: "Integration"
    date: 2002-09-03
    body: "Just out of curiosity, why is XMMS less integrated than noatun? I mean differences for the user. The same question would apply to Gimp, why isn't it integrated into GNOME? Yes it doesn't use the GNOME libraries but those aren't always needed. It uses the same widget style, dialogs like the open/save file dialog are the same, drag and drop works... For XMMS there is even an applet to control it from the panel. What else would be needed?"
    author: "Spark"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "XMMS is less integrated than Noatun because its open file dialog is GTK one.\nMore over, XMMS isn't scriptable via DCOP. Last thing (but I'm not sure): does KDE remember XMMS opened windows when saving a session ?"
    author: "Julien Olivier"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "<i>does KDE remember XMMS opened windows when saving a session ?</i>\n\nYes it does. KDE remember all applications launched leaving the session. I use this with gkrellm (gtk lib), wmcoincoin (windowmaker lib), konsole (Kde lib),...\nIt is not an integration problem"
    author: "Shift"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "Xmms doesn't use Gnome file dialog (Which is much powerfull than gtk one)\n\nIt is not possible to integrate a part of it in other application (no bonobo) as noatun in konqueror. Of course it is not noatun who is integrated but a smaller player using its libs.\n\nXmms can't be manipulated by other apps and system(\"xmms-shell ...\") is not a good way to do it ;)\n\nXmms manage its windows itself and not the way Gnome apps do (stay-on-top, position, display,...). Have you seeen than when you select \"Show only borders when moving windows\" in your favorite WM, it is not respect by xmms.\n\nXmms support drag&drop but all Gnome application don't. Xmms don't use d&d lib of Gnome :(\n\nGimp doesn't respect Gnome design/ergonomy norms (If this one exist ;)\nGimp use multiple windows and tools are in an other window. \nAbiword use one window eaven for multiple document as gnumeric does.\n...\n\nIntegration is useful and Gnome need to work on it.\nI am waiting for abiword to integrate .xcf file in the document by using bonobo.\nKoffice do something like this but apps need work to support more used mimes-types :(\n\n"
    author: "Shift"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "> Xmms doesn't use Gnome file dialog (Which is much powerfull than gtk one)\n\nWhat GNOME file dialog? There is one? Does any GNOME app use that dialog?\nYes there's a Bonobo-based file selector for GNOME 1 (if you meant that one) but it was never finished. I fail to understand what you mean by \"more powerful\". The standard GTK+ file dialog supports tab completition, THE future I use most.  I don't know if this scares you, but *I can select files faster with the GTK+ file dialog than the Windows or KDE file dialog*! Ximian's GTK+ packages (with patches) fixes some of the most annoying bugs (like that the filename disappears when selecting a directory) and adds nice \"Home\", \"Documents\" and \"Desktop\" shortcut buttons. What more do you need?\n\nAnd a new file dialog is planned for GNOME 2.2."
    author: "Stof"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "> I fail to understand what you mean by \"more powerful\". \n\nI think he was pointing to the fact that the gtk+ file dialog is probably the most hated thing by end users of gtk apps. \n\nEven the http://developer.gnome.org/gnome-ui/hitsquad/filedialog.html says that it is ;)\n\n\n> The standard GTK+ file dialog supports tab completition, THE future I use most. \n\nWow, I didn't realize that it did. I'll have to check it out once I go home.\n\n> And a new file dialog is planned for GNOME 2.2.\n\nYeah, the new file dialog will probably be great."
    author: "fault"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "<i>I think he was pointing to the fact that the gtk+ file dialog is probably the most hated thing by end users of gtk apps. \n</i>\n\nNo it's not that. I love shell-style completion in gtk file dialog and I want this in KDE too because KDE one is less easy."
    author: "Installation"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "kde's file manager has several completion modes (drop down, auto, semi-auto, none) and has for a long, long time. what do you find lacking that isn't covered by one of these modes?"
    author: "Aaron J. Seigo"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "The problem is that TAB completion with most modes doesn't work in the file dialog because TAB jumps to the next element instead of completing the element. TAB only works with dropdown list mode, but then you still have no possibility to enter the next directory easily with the keyboard (or I didn't figure out how to do it). Tested with KDE 3.1 Beta. Also it seems to work completely different again in the input field at the top (moving to the next directory works by typing a slash, but dropdown autocompletion doesn't work here for me).\n\nOther problems I found so far with the KDE dialog (this is meant and hopefully understood as some constructive criticism :)) are:\n- Toolbar is a bit too complicated. At least \"reload\" doesn't need to be there for sure. ;) Especially as it autoupdates the view anyway if something changes... I believe that \"back\" and \"forward\" buttons are equally useless. Sometimes they might be handy but in most of the cases you only need the UP button so making this one a bit larger instead should be more efficient.\n- The special folder input field at the top isn't convenient, why isn't there only one locationbar? There are three input fields in the dialog when zero would be sufficient.\n- The encoding selection is completely ambiguous. What should a beginner do with it? Browsers can show descriptive names for it so I bet that it would be possible there, too.\n- Drag and drop to shortcut list doesn't work. I guess that's a planned feature.\n- The file view scrolls vertical. Ugh. At least the default view (small icons). I see the reasoning behind this, but... hm...\n- I just tried the thumbnail preview (nice to have) but it overlapps files. Maybe this is a work in progress of the Beta. Also the view isn't saved, so I wonder if it would be usefull if you use it a lot with a graphics application as it's not really convenient to change the view.\n\nNot that bad, but not perfect. ;) "
    author: "Spark"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "\"What more do you need?\"\n\nDon't get me started... ;) Yes the current Gtk 1.2 file dialog with \"Home\" and \"Desktop\" buttons patch works quite well, but there are so many things wrong with it:\n\n- It looks ugly. ;) Not even small icons besides the files to separate them from each other and emblems are completely missing...\n- The \"Files\" view is very small. Yes you can resize the window but it doesn't safe the size.\n- No bookmarks or custom shortcuts.\n- Tab completion isn't user visible (Nautilus and the KDE file dialog... no wait, it doesn't (I really thought it did) but Nautilus and Konqueror do and make it visible to the user by showing what would be completed when hitting tab (Konqueror by providing a list and Nautilus by showing the rest of the filename selected).\n- No thumbnails view like in Nautilus. Would be extremely convenient when selecting images (previews are a poor alternative).\n- No simple file save dialog (like in Mac OS X). Using the same layout for file open and file save seems overcomplicated to me, of course Windows and KDE suffer from the same problem.\n- Save dialog does not support \"drag and drop saving\". Granted, neither does anyone else but RISC and ROX...\n- No possibility to show hidden files (you have to type \".\" and hit tab to see them).\n- \"../\" isn't exactly what a new computer user should use to go up a directory and \"./\" is... well... nothing.\n\nSo I really can't wait for the new dialog that will hopefully use a Nautilus view. :)\nI just hope that will remain as simple as it is right now as I believe that a file dialog should be as straight forward as possible, not a full blown file manager. I also hope that Nautilus will get a column view that can be used for it as I really like it. "
    author: "Spark"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "Please stop trolling :)\n\nI agry with you that completion is missing in KDE file dialog (One exist but less easy than in gtk file dialog) :(\n\nWhat I need more is unification : I want the same file dialog (with Desktop, Home, patatipatata...) for all application belonging to Gnome. "
    author: "Installation"
  - subject: "Re: Integration"
    date: 2002-09-05
    body: "> Please stop trolling :)\n\nI assume that this is your idea of a joke? -_-"
    author: "Stof"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "\"Xmms doesn't use Gnome file dialog (Which is much powerfull than gtk one)\"\n\nAs Stof already said, which GNOME dialog? ;) Gtk and GNOME file dialog are equal(ly bad) and I hope that the new one will be equal too. It doesn't make much sense to have two different file dialogs.\n\n\n\"It is not possible to integrate a part of it in other application (no bonobo)\"\n\nTrue, that's the advantage of gstreamer... But then again, there are other music player libs so there is no real difference for the user (Nautilus can play music just fine).\n\n\n\"Xmms manage its windows itself and not the way Gnome apps do (stay-on-top, position, display,...).\"\n\nOk, but that's a minor issue. Winamp for Windows does the same and nobody cares... But it really should rather follow the wmspecs than Winamp...\n\n\n\"Xmms support drag&drop but all Gnome application don't. Xmms don't use d&d lib of Gnome :(\"\n\nWhat do you mean? I can drag songs to XMMS, what else do I need in XMMS? \n\n\n\"Gimp doesn't respect Gnome design/ergonomy norms (If this one exist ;)\"\n\nGimp does follow the HIG (mostly)! Well at least the Gtk2 version does. :)\nOf course it has a very unique design but every application is free to apply the design that is most efficient for the particular task. There is no right or wrong here and no common way that is best for every work.\n\n\n\"I am waiting for abiword to integrate .xcf file in the document by using bonobo.\"\n\nThis would be nice, but I guess that Gimp could be bonobonized without using the GNOME libraries (besides libbonobo of course). Just like OpenOffice.org. And this doesn't apply to XMMS. ;)\n\n\nWhat I mean is, there is no real need for Gimp and XMMS to use the GNOME-libs as it wouldn't make any real difference for the user, so why should they do it? "
    author: "Spark"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "> What I mean is, there is no real need for Gimp and XMMS to use the GNOME-libs as it wouldn't make any real difference for the user, so why should they do it? \n\nHow about things from gnome-libs such as stock icons, dialogs, and other infastructure things such as gconf?\n\nBut you're right, the reasons for using GNOME and KDE libraries versus GTK+ or Qt has gone down. In fact, a lot of people (who don't use GNOME or KDE) prefer pure gtk+ or qt apps."
    author: "fault"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "Well, stock icons aren't specific to GNOME. Those are part of Gtk AFAIK. And gconf is not GNOME specific either, in fact it could be used just as good by KDE applications. *hinthint* ;)"
    author: "Spark"
  - subject: "Re: Integration"
    date: 2002-09-06
    body: ">And gconf is not GNOME specific either, in fact it could be used just as good by KDE applications. *hinthint* ;)\n\nGconf is horrible. Let us hope that KDE never uses it. Hint hint :)"
    author: "ac"
  - subject: "Re: Integration"
    date: 2002-09-16
    body: "There you go again. What makes GConf more horrible than KdeConfig? They are both universal frontends which are able to use different backends. GConf doesn't have to use the default XML backend, you can use the LDAP backend or whatever backend you want. Same story for KdeConfig. What makes you think GConf is so horrible other than the stupid propaganda from anti-GNOME trolls?"
    author: "Stof"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "> Integration is useful and Gnome need to work on it.\n> I am waiting for abiword to integrate .xcf file in the document by using \n> bonobo.\n> Koffice do something like this but apps need work to support more used mimes-> types :(\n\n<rant>\nWell, Integration is important, but in this day and age, users focus on the application more than the document. This has been tried and tried again to be fixed by various software developers, but no approach has so far worked. Take for example OpenDoc/Document-Subscriber Linking by Apple and Microsoft OLE/DirectX Objects/Universal Data Access/DDE. Kparts/DCOP and Bonobo sound great from a developers perspective, but I don't think \n\nThe only exception I make to this is probably things like Windows Explorer and Konqueror being both file managers and web browsers (among other things). I think Bonobo's growth in usage will perhaps only come through increased protrayal of Nautilus as a web browser.\n\nOf course, this will never happen because of galeon.</rant>"
    author: "fault"
  - subject: "Re: Integration"
    date: 2002-09-03
    body: "integration in KDE is alive and well. it works primarily because most users are completely unaware that it is happening. i often hear comments about how kde \"just seems to work\". the seams that hold the applications together are pretty well invisible thanks to UI merging, ktrader autodetection of parts, aggressive use of interfaces and libraries, etc, etc..\n\nthe user can concentrate on what they perceive to be the \"application\" while the desktop provides document-centric integration transparently. \n\ncall it ubiquitous integration, if you will =)"
    author: "Aaron J. Seigo"
  - subject: "Believe the Linux Journal Editor"
    date: 2002-09-02
    body: "It looks like the Linux Journal Editors have taken their time to choose carefully the software that pleased them. On the KDE side, KDE was awarded. On the Gnome/Gtk side, Mozilla + Galeon, Gimp, and Evolution were awarded. This is what these people think as users. There is nothing more to say about it.\n\nNow if all people that just have to say \"Kmail is better than Evolution\" and \"I like Galeon more than Konqueror\", could refrain to post, that would be good. Currently, the dot looks like a child court, all childs saying \"my father has a better job than yours\".\n\nWe are all aware of the state of KDE and Gnome and it does not bring much anything to state your personal preference here. If you want to bash/defend gnome or KDE without any sensible arguments, there is an area dedicated to this: Slashdot\n\nIf you post here, please try to raise the level of the conversation instead of making it fall down. Like having backed-up facts and technical arguments. And there is no use to do it for every news at dot.kde.org\n\n"
    author: "Philippe Fremy"
  - subject: "Galeon and Konqueror"
    date: 2002-09-02
    body: "Overall, I find Galeon an excellent browser. However, it bothers me that I have to have Mozilla installed. I may as well use Mozilla. Also, I've had a problem with Galeon for the longest time. It won't keep my toolbar where I want it, i.e. after the location bar. Different people have reported the same or similar problems, but no one seems to have fixed it. At least that was the case about a month or so ago. Konqueror had many problems with a lot of web sites, but with KDE 3.0, it seems to be really improved and the difficulties are really much less frequent.\n\nEveryone has a reason for picking certain apps. All apps have their pros and cons. Stating that App XYZ sucks is not very contructive.\n\nAnyway...I like KDE because of the unified look and feel and how everything is so well integrated. But I don't knock people who like Gnome either.\n\nKeep up the good work KDE devlopers!"
    author: "Paul Vandenberg"
  - subject: "Re: Galeon and Konqueror"
    date: 2002-09-08
    body: "> Konqueror had many problems with a lot of web sites\nIs it not possible to use the Gecko-engine (Mozilla) with Konqueror?"
    author: "KDE User"
  - subject: "props"
    date: 2002-09-02
    body: "congrats to everyone involved in creating and supporting KDE! you've done a great job and continue to do so.... thanks for all of you efforts and for gifting us with a terrific environment and set of apps...\n\nspeaking of which... as for seeing more KDE *apps* get the nod in the future, this is obviously where our challenge lies. popular opinion is that KDE apps aren't up to snuff. unfortunately, popular opinion usually lingers long after the facts have changed. let's just keep making better apps and more of them. our users will thank us every step along the way and eventually, with enough dilligence and effort, public opinion will turn around as well...\n\ni can hear the tide rolling in ... "
    author: "Aaron J. Seigo"
  - subject: "KDE and GNOME: cooperate, instead of fighting...."
    date: 2002-09-02
    body: "To all guys scewing against echother that GNOME or KDE is better, think about this: both systems have their strong and weak points. For example:\nKDE: good intergration, but lacks quality at single applications.\nGNOME: lacks intergration, but has good quality at single applications.\n\nSo insted of fighting, why not combine the powers of both sides. For example:\nGNOME-aplications with the look and feel of KDE, nicely intergrated in the KDE-desktop. Ofcouse the same story for KDE-apps in gnome.\n\nNow this days I still a lot of diffence in the interfaces of all sort of Linux-programs. Apple and Windows are much further in creating one similair look and feel and this makes those systems more userfiendly, esspecially for non-technical users. \n\nIf KDE and GNOME could develop (together) a sytem that when you use one of these desktops, all KDE- and GNOME- (and perhaps others) apps have the same look and feel (even the same icons, open / save dialogs, etc. etc.), them I'sure that Linux becomes much more userfrienly and nicer to use.\n\nBecause in the end, both efforts of KDE and GNOME are the creation of the best userinterface and applications for (mostly) Linux......and Linux exist because collaboration, not figting, flametrowing, trolling, screwing or whatever.\n\nBy the way, this project already seems to try to the things I just said above:\nhttp://www.freedesktop.org\n\n\n"
    author: "Maarten Rommerts"
  - subject: "Re: KDE and GNOME: cooperate, instead of fighting...."
    date: 2002-09-02
    body: "of course people can use GNOME and KDE apps on the same system. as well as GTK, Qt, Xlib, Athena, Fox, etc, etc, etc... apps..\n\nGNOME and KDE will probably always remain somewhat unique, however.\n\nas for freedesktop.org, it is a cooperation between the two projects, not a seperate project. so you can look to it for further cooperation between the two, but not some meta-project."
    author: "Aaron J. Seigo"
  - subject: "Re: KDE and GNOME: cooperate, instead of fighting...."
    date: 2002-09-03
    body: "> KDE: good intergration, but lacks quality at single applications.\n\nI think the applications have gotten better since 3.0.\n\n> GNOME: lacks intergration, but has good quality at single applications.\n\nI think integration has gotten better since 2.0.\n\nBasically, I don't think old standards (that may have been true less than a year ago) apply to rapidly changing desktops such as KDE and GNOME."
    author: "fault"
  - subject: "slashdot quality :("
    date: 2002-09-02
    body: "seems like dot.kde.org\nreceive many \"slashdot quality\" post\nthese days.\n\nxxx is better than yyy !\nNo, yyy is total crap !\n...\n\n\nregards."
    author: "thierry"
  - subject: "Re: slashdot quality :("
    date: 2002-09-02
    body: "Yeah, I get it.  Please stop complaining, it only makes the situation worse.  This is a banned user who keeps jumping through hoops to get back on this site.  Let us work it out."
    author: "Navindra Umanee"
  - subject: "Re: slashdot quality :("
    date: 2002-09-03
    body: "sorry Navindra,\nnow you can banned me for this poor post :)\n\nyes, you can to it!\n\nanyway, thanks for the contribution."
    author: "thierry"
  - subject: "Re: slashdot quality :("
    date: 2002-09-03
    body: "Heh, we don't ban non-abusers for no reason.  :-)\n\nThat message wasn't directed at only you either."
    author: "Navindra Umanee"
  - subject: "Re: slashdot quality :("
    date: 2002-09-03
    body: "You sure? An admin (from another website) once said that the whole point of having power is to abuse it. :)"
    author: "Stof"
  - subject: "Re: slashdot quality :("
    date: 2002-09-18
    body: "Yes you do. You ban them for having opinions that you don't like - and what's more, your own posts are sub-slashdot flamebait. In fact, you are the worst webmaster I've ever seen.\n"
    author: "Nomo"
  - subject: "KDE vs. Gnome"
    date: 2002-09-02
    body: "Guys,\nIf you don't know how to stop the flames KDE vs. Gnome here\nis my ideea. Why Evolution and KMail and Mozilla and OpenOffice\nuse different mailboxes? \n\nAt least KDE & Gnome (if not together with Mozila and OO) should\ngo hand in hand in creating a shared library for:\n- Managing mailboxes, mail accounts/settings etc.\n- Address-books, Organizer (meetings, calendar, notes etc...)\n- Better cut/paste cooperation (including images)\n- User settings (Desktop Icons, Menu entries, Documents dirs)\n- Browser bookmarks, history and why not cookie.\n\nBoth KDE and Gnome got the bad Windows Ideea to be \"application centric\".\nHeck, is not the applications the users care about, but the data.\nThey care about their e-mails, their accounts, their documents,\nand settings etc... The one who want to try evolution must be able just to\nstart-it and have all the e-mails and settings in place.\nIf after a couple of weeks, he decide to go back on Kmail, everything\nreveived with Evolution should be there.\n\nWhy duplicate effort in implementing 2 sound systems?\nYou have one, OK. Ask the other guys to provide bindings for\nthe other.\n\nAnd so on. Duplicating efforts and trolling at each other it is\na good activity if you want users to stay in the Bill's courtyard.\n\nJust my only 2 cents....\n"
    author: "Anonymous peace maker"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-03
    body: "That is one of my small gripes at the moment. These are open source apps, yet I can't import and export Mail, address books, and other documents easily. I'd like to be able to easily and quickly view and edit KWord docs in Abiword, OO.org or Emacs, or Abiword or OO.org docs in KWord. If we're going to have millions of incompatable standards it's not good. I'm not suggesting having just one document format for all these programs, because they are all different. But they should be able to interchange documents."
    author: "David Findlay"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-03
    body: "One thing that I do to address interoperability with mail is to run an IMAP daemon on my machine locally. Then I just set kmail or whatever to use IMAP on localhost. Sure, kmail wants to create its own mail folders, but I can live with that; the mail is still accessible from Pine, kmail and anything else that supports IMAP. Even file locking works, so I don't have to worry about corruption or otherwise if two programs decide to access data simultaneously.\n\nIf you're not into getting an IMAP server going, a lot of times, you can just make links between pine's mbox, kmail's inbox, netscape's whatever, since they all use the same flat-file format for storing mail (just different file names). Be sure to back up though!\n\nI believe that the Sun people are working on getting some standards for document types: http://www.theregister.co.uk/content/4/26900.html\n\nAnd work is going into this too thanks to the german govt.: http://www.1dok.org/de/\n\nDesigning good document formats is a big deal, and I'm glad that nobody's just accepted a half-ass standard yet and instead taken time to see what is needed and what can work."
    author: "Brent Cook"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-03
    body: "> - Better cut/paste cooperation (including images)\n\nWhy do people keep asking this? *Clipboard has been fixed on KDE 3.0*!!\n\n\n> Why duplicate effort in implementing 2 sound systems?\n\nGNOME is planning on moving away from esd and switching to arts. I don't know what the progress is, though..."
    author: "Stof"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-03
    body: "\"Why do people keep asking this? *Clipboard has been fixed on KDE 3.0*!!\"\n\nI think he was asking about other data than simple text. Clipboard doesn't exactly work with images and other data.\n\n\n\"GNOME is planning on moving away from esd and switching to arts. I don't know what the progress is, though...\"\n\nGstreamer will support arts. The question remains, which soundserver will be default. I hope that arts becomes the de facto standard as we need one (it's especially important for gaming, I want to play Quake while listening to music and talking to people via voicecom! Well, at least two of this). \nI whish though that KDE would adopt Gstreamer in turn =) but it doesn't look like it (I would be happy to be proven wrong though)."
    author: "Spark"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-06
    body: "While KDE will use GNOME code, GNOME will never use KDE code. The chance of GNOME using arts is zero because arts is too much of a 'KDE project'. There is a possibility of Gstreamer in KDE, but the only way KDE code will end up in GNOME is if some developer a) Incorporates KDE code by accident or b) Removes all trace of KDE developer copyrights in the code. Cooperation between KDE and GNOME has always been one way, this will never change."
    author: "ac"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-06
    body: "Nah.  I can think of at least the early version of khtml became the early version of gtkhtml (or somesuch gnome lib).  Abiword and the KOffice group get along.  If I've seen those two, I'm sure there are plenty of other examples.  \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-06
    body: "If I recall correctly, gtkhtml is a port of khtmlw (kde 1.x), and gtkhtml2 is a port of khtml (pre2.0-3.x)\n"
    author: "d00d"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-06
    body: "> Cooperation between KDE and GNOME has always been one way, this will never change.\n\nYou obviously neglected xdnd, _NETWM, desktop entries, and of course, clipboard behavior changes. \n\nOf course, these came from freedesktop.org, a site started and maintained by Havoc Pennington, who has always been one of the most important GNOME/gtk+/and *shrudder* RedHat developers. Through freedesktop, there has been much cooperation and standards sharing between KDE and GNOME. This is actually more practical than code sharing in most of the time."
    author: "dc"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-03
    body: "RedHat to the rescue.\nThey are building a new enviroment that mixes KDE with GNOME.\nPeople in both projects are pissed off at them but I'm so happy they are trying to solve the problem instead of trolling :-)\n\nWay to go redhat!!!\nThanks"
    author: "Anonymous"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-03
    body: "This isn't the right way to solve the problem and Redhat knows it."
    author: "KDE User"
  - subject: "Re: KDE vs. Gnome"
    date: 2002-09-04
    body: "If KDE and GNOME had worked out some more standards, and a more unyfied look-and-feel thid wouldn't be nesseary..........Red Hat is sending GNOME and KDE a message with this.................We should open our eyes now."
    author: "Maarten Rommerts"
  - subject: "Re: KDE vs. Gnome"
    date: 2003-07-06
    body: "GREAT IDEA :-)!"
    author: "mojo"
  - subject: "Re: KDE vs. Gnome"
    date: 2004-10-16
    body: "the word you are looking for is synergy\nyou would like to see synergy between linux developers.. \n"
    author: "matt"
  - subject: "Re: KDE vs. Gnome"
    date: 2004-10-16
    body: "yes! yes! and another word you should be looking for is proactive.\nwe should be proactive about it!\n\n:-) "
    author: "KDE User"
  - subject: "Gnome?"
    date: 2002-09-03
    body: "Why do I have to read comments about Gnome everytime I visit\nthis site? This is dot.kde.org, not dot.gnome.org\n\nOne comment about Gnome once in a while would be ok but\nit\u00b4s really becoming too much."
    author: "KDE User"
  - subject: "Re: Gnome?"
    date: 2002-09-03
    body: "Amen.\n"
    author: "Johnny Andersson"
  - subject: "GOOD JOB"
    date: 2002-09-04
    body: "I also gota suggestion, CAN Kvim 6.1 be integrated into KDevelop by default in KDE 3.1. That would rock!!!! Also Evolution is much more powerful than Kmail, I cans ee the weather, news, I ahve a calendar, great filters, tasks etc. Hwoever Kmail, is the best if all you want is mail and only mail."
    author: "Alex"
  - subject: "Re: GOOD JOB"
    date: 2002-09-04
    body: "Please try Korganizer! It's a very fine app, and it integrates with\nkmail (you just have to add a special filter program to the kmail\ninbox to sort out messages for korganizer)...\n\nFor the weather thingy. I've tried evolution half a year ago and on the weather-page it had no european countries only the us... \nOr was it me who just did not know what to click?"
    author: "Thomas"
  - subject: "Re: GOOD JOB"
    date: 2002-09-04
    body: "Don't know, but I definetly have the weither for Duesseldorf/Germany there. =)\nNot that I would consider this to be an extremely important feature..."
    author: "Spark"
  - subject: "Re: GOOD JOB"
    date: 2002-09-04
    body: "Don't people understan. KDE is a complete desktop. All small parts/apps\nmake up KDE. The idea is NOT to have one huge app that solves\nall your problems."
    author: "OI"
  - subject: "Re: GOOD JOB"
    date: 2002-09-04
    body: "> I also gota suggestion, CAN Kvim 6.1 be integrated into KDevelop by default in KDE 3.1\n\nYou'll have to wait for KDE 3.2, but yes it can:\n\nhttp://www.freehackers.org/kvim/screenshots.html"
    author: "Philippe Fremy"
  - subject: "Re: GOOD JOB"
    date: 2002-09-05
    body: "Wow, very nice. I've been waiting for this for a god-knows-how-long-time (especially vim & kmail).\n\nIt'll be in 3.2? Sometimes I hate the feature freeze though :)\nOh well, I'll at least encourage me to continue wasting precious processor cycles compiling CVS in the next few months."
    author: "fault"
  - subject: "Re: GOOD JOB"
    date: 2002-09-05
    body: "<i>Hwoever Kmail, is the best if all you want is mail and only mail.</i>\n\nWasn't there somewhere the idea of integrating KMail and KNode (at least partially) ? Has this been abandoned ?"
    author: "loopkin"
  - subject: "Re: GOOD JOB"
    date: 2002-09-06
    body: ">Wasn't there somewhere the idea of integrating KMail and KNode (at least partially) ? Has this been abandoned ?\n\nNo, it's just that the main knode developers have no time. But there is code sharing happening, it is just going to take a while to complete the process."
    author: "ac"
  - subject: "Re: GOOD JOB"
    date: 2002-09-06
    body: "> integrating KMail and KNode \n\nPlease don't do this. It is just confusing and does not\nbring any advantages."
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-06
    body: ":: > integrating KMail and KNode \n\n:: Please don't do this. It is just confusing and does not bring any advantages.\n\nAre you kidding?  Usenet is a message medium, as is KMail - you use filters based on pretty much the same headers, you archive certain message, track sent correspondance.  Usenet is about shared discussion, as is email.  Usenet and mailing lists can benefit from the exact same tools.  KNode's biggest problem for me is the fact that it's a PITA to have seperate archive on topic for things that I am discussing on usenet and things I'm discussing in direct email - often with the smae people from usenet.  Everytime a discussion is taken to private email, it's a shift in apps.  Very annoying.\n\nFWIW, I used Pine for many years, and had a slew of awk tools to manage my usenet and email.  There are still some advantages to that configuration.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GOOD JOB"
    date: 2002-09-06
    body: "I completely agree. USENET and email have always been tied together. It makes sense to merge kmail and knode. Perhaps it'll be easier when libkdenetwork is stabilized for kde3.2.\n\nI personally like knode's interface better than kmail's, but I think it would easier to merge knode into kmail instead. It would NOT confuse users because Newsgroups+Email in the same application has been common in popular mail clients for a while. Look at both Outlook and Netscape (both 2.1/3.0/4.x/6.x/7.x mail clients)"
    author: "hear hear"
  - subject: "Re: GOOD JOB"
    date: 2002-09-07
    body: "> Usenet is about shared discussion, as is email\nI disagree, email is about posting mails. Discussions through email is just a side effect of how email works (the speed).\n\nEmail is normally not used for discussions by common people. Bringing applications together that does different things is just good for confusing the user, just look at Outlook, difficult to use and confusing. When people want to write a mail, they use the mail-writer. When people want to post a message to a discussion group, they use the discussion-message-poster :-)."
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-07
    body: ":: Email is normally not used for discussions by common people. \n\nI'm not sure how to respond to that.  Then what *is* email used for if not for discussions?  The whole *point* of email is to discuss things in a back and forth written dialogue.  I email my mom telling her I'll be missing Thanksgiving, but I'll be there for Christmas, she emails me back, telling me that I should show up early as my sister will be staying for a week before Christmas.  Discussion.  Mailing lists and usenet groups exist for people to do a one to many communication, and are functionally nearly identical.  Why not have them be in the same application.\n\n:: Bringing applications together that does different things is just good for confusing the user\n\nYes, but mailing lists and usenet are not only nearly identical, using the same terms and headers, etc, they also cross over quite a bit - many usenet threads are taken to private email and private email discussions move onto usenet groups.  Not only are they functionally nearly identical, but they interrelate in a practical way extraordinarily closely.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GOOD JOB"
    date: 2002-09-08
    body: "> Then what *is* email used for if not for discussions\nI think my earlier post answer this. There is a difference between email and newsgroups. Of course, you know this difference also. People typically use email for what it was created for, and USENET for what that was created for.\n\n> I email my mom...\nI can assure you, that my mom gets quite confused about all the options in Oulook Express when she tries to set up her email."
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-08
    body: ":: There is a difference between email and newsgroups.\n\nOkay.  What is this difference?  I send messages and recieve messages.  Let's take kde@lists.kde.com and comp.windows.x.kde for example.  How are the two functionally different?\n\n:: Of course, you know this difference also.\n\n    At the user level, I honestly don't see any difference whatsoever.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GOOD JOB"
    date: 2002-09-08
    body: "Maybe I\u00b4m far out here or something but I always thought that email was for sending mail to _one_ person, this person might eventually respond. Newsgroups are for posting comments about some certain topic, comments that hundreds of people read and might also comment on.\n\n> At the user level, I honestly don't see any difference whatsoever\nSeriously?"
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-09
    body: "::> At the user level, I honestly don't see any difference whatsoever\n:: Seriously?\n\nSeriously.  Mailing lists and usenet use the same terminology, concept - even the same headers.  If you reply private on usenet, the message is sent via standard email as opposed to to the group.  If you reply private on a mailing list, the message is sent via standard email as opposed to the list.  Threading is handled by the client, messages are stored on the client (although they expire on the server if you wait too long on usenet, versus filling your mailbox to your quota in email).\n\nFrom, To, Subject, CC - are you sending to one person or many?  Your choice.  Is it a list, a group, or a handful of recipients picked for that message from your addressbook.  It doesn't matter.  IMAP, POP3, NNTP, SMTP, UUCP, NFS spool - all are just ways of getting and sending messages comprised of a header and content to one or more recipients.\n\nEver use Pine?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GOOD JOB"
    date: 2002-09-09
    body: "Reading your response, it sounds like you don't see any particular advantage of having these two internet services. Maybe you would also argue that we could skip newsgroups alltogether? Since they are practically the same, according to you."
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-09
    body: "> Mailing lists.. use the same terminology, concept..\nYes, but not email as a whole. Only mailing lists, as I have written earlier."
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-09
    body: "Does email not send messages TO recipents, CC other people, have a SUBJECT and responses are generally quoted with the subject tagged with an Re:?\n\nBoth email and usenet.\n\nDo not good email clients use filtering, blacklists, folders, sent-messages folder, drafts folder, and an outbox folder?\n\nBoth email and usenet.\n\nUsenet and email are quite interrelated and woven together.  Applications use the same interfaces, the same editors, etc. \n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GOOD JOB"
    date: 2002-09-10
    body: "So if they are all the same, then why do we need the newsgroups services at all? Maybe you would also argue that we trash the newsgroups alltogether?"
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-10
    body: "Quite a few newsgroups have, over the past ten years, moved to mailing lists.  Most new groups, rather than face the public (read: spamable) forum of newsgroups have been created as mailing lists rather than newsgroups.\n\nMe, I like newsgroups - the browsability, the fact that you can ignore 'em for a month and then come back (whereas mailing lists always get delivered *somewhere* filling up diskspace).  You're not the first to suggest abandoning newsgroups because \"mailing lists do everything they do and are more controllable\".  It's a pretty common observation that mailing lists and usenet groups are redundant.  Still, there are communities on usenet that have been going strong for 15 years, and are pretty entrenched.\n\nThat fact is, usenet and mailing lists might be redundant (especially in the post-DNS era Internet when bangpaths aren't necessary - manual routing of mail made usenet much more attractive :) ), but people actively use both.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: GOOD JOB"
    date: 2002-09-11
    body: "> You're not the first to suggest abandoning newsgroups\nActually I don't really care, people who like them...please use them.\n\nAnyway, you seem to think just like me that there is an isolated group using newsgroups, not \"common people\". I argue not to combine this with email software, it will give my grandmother (and 1000s others) an extra headache when she tries to configure her mail because of the extra options. And what do you gain? That some techies get some extra functionality. What is more KDE?"
    author: "KDE User"
  - subject: "Re: GOOD JOB"
    date: 2002-09-11
    body: "\nWell, seen from one point of view, there is an isolated group using email.  Some people just use the web and never get their email working.  Usenet is a powerful tool, just like email is, and is functionally similar to email.  Many people use it through Google Groups simply because good GUI newsgroup programs are rare (easier to find on *nix, but the majority of users don't like installing software and don't use *nix).\n\nI started filling up a notebook with notes on a new, simplified communication tool for KDE using plugins for greater extendibility.  I'm kicking around the idea of using python, and have been playing with PyQt and PyKDE.  No idea if I'll get anywhere with it - I have little free time, and already have a big project I'm committed to that's sucking up my spare time.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
---
<i>"<a href="http://www.linuxjournal.com/article.php?sid=6260">Linux Journal's Editor's Choice Awards</a> are well-known as the premiere forum recognizing outstanding product developments and achievements in the Linux market. A panel of more than 50 distinguished Linux experts was assembled to nominate products for the awards, which were then sent to the Linux Journal editors who chose the final winners."</i> KDE 3.0 wins the honour of best Consumer Software. <a href="http://www.konqueror.org">Konqueror</a> obtained an honorable mention as Web Client but was beaten up by Mozilla and Galeon. <a href="http://www.kdevelop.org">KDevelop</a> also received an honorable mention as Development Tool alongst with Borland Kylix, both being beaten by Emacs -- but had they tried <a href="http://www.freehackers.org/kvim/">KVim</a>?
The <a href="http://www.trolltech.com/products/qtopia/designwins.html">Sharp Zaurus</a> was also selected as best Mobile Device and Product of the Year -- good news for <a href="http://www.trolltech.com/products/embedded/">QtE</a>.
<!--break-->
