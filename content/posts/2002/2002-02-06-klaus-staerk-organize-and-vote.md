---
title: "Klaus Staerk:  Organize and Vote"
date:    2002-02-06
authors:
  - "Dre"
slug:    klaus-staerk-organize-and-vote
comments:
  - subject: "German government is going to sponsor KDE?"
    date: 2002-02-06
    body: "Does it means that German government is going to became greatest sponsor of KDE?\n\nAnyway this is a big success for open source Desktop/OS!\n\nIt seams that governments are very friendly to the open source.\n(You know about the story about Korea and HancomLinux)\n\nWhat about whole EU to switch to KDE :)\nSame to US which is still judging MS."
    author: "Anton Velev"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2002-02-06
    body: "> Does it means that German government is going to became greatest sponsor of KDE?\n\nThey only sponsor afaik Project \u00c4gypten (http://www.gnupg.org/aegypten/) at the moment. I think the industry like SuSE, Mandrake, TrollTech, Caldera and RedHat are greater sponsors of KDE as they pay (full-time) developers to work on KDE for more than a single project."
    author: "someone"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2002-02-07
    body: "The German government sponsors KDE development indirectly with its univerities, both in studenst and in network/server infrastructure.\n\n\n\n"
    author: "anonymous"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2002-02-07
    body: "I never heard that german universities pay their studenst, quite the contrary. And if we are talking about network/server infrastructure: VA Linux/SourceForge, RedHat and TrollTech provides an equal or greater amount than german universities I think.\n"
    author: "someone"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2002-02-07
    body: "KDE was born in the German Universities, wasn't it?"
    author: "reihal"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2002-02-08
    body: "It maybe was started by studenst, but not by universities.\n"
    author: "someone"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2003-07-15
    body: "Hello. I'm Karine from Armenia. I'm not surprised do much as anothers, because I know that german government pay attention to its studenst. So, if it is possible for German Government , please give an opprtunity also to the studenst to abroad to increase thier knowledge.\nThank you\n\n"
    author: "Karine"
  - subject: "Re: German government is going to sponsor KDE?"
    date: 2003-07-26
    body: "The studenst from Pakistan specially from Lahore who want to study in germany for free can get the information and guidence from PGSO (Pak German Studenst Organization) \nwww.pgso.org\ninfo@pgso.org\n\n"
    author: "Babar Saeed"
  - subject: "Answer"
    date: 2002-02-06
    body: "Not yet.\nThey are still before the decision to take Linux or Windows.\nThe petition is to ensure that the Bundestag realy considers Linux as an alternative."
    author: "Philppp"
  - subject: "Successful petition? MS are annoyed??"
    date: 2002-02-06
    body: "Wow! Over 19,000 people have signed the petition even as I type this...\n\nAs the register reports at \n\n   http://www.theregister.co.uk/content/4/23964.html\n   \"MS Chief Lashes out at German Free Software Petition\"\n\nMicrosoft seem to be a little bit annoyed."
    author: "Corba the Geek"
  - subject: "I signed the petition"
    date: 2002-02-06
    body: "I signed the petition. so should you."
    author: "Matus Telgarsky"
  - subject: "Re: I signed the petition"
    date: 2002-02-07
    body: "Please understand, that this issue is more or less related to the german residents only.\n\nAs a german one, I don't think that it is in any case sensful to what other nationalities have to think/say.\nJust imagine what would happen to your oqn country if the whole China would vote for something. Would you even bother their mind?\n\nFrom my point of view, this petition is for Germans only.\n\n"
    author: "Philppp"
  - subject: "Re: I signed the petition"
    date: 2002-02-07
    body: "Heh, I wish someone would've explained this to me _before_ I signed :-)"
    author: "Carbon"
  - subject: "Re: I signed the petition"
    date: 2002-02-07
    body: "He, this is the Internet, would you like to see my passport?"
    author: "reihal"
  - subject: "Re: I signed the petition"
    date: 2002-02-08
    body: "Is Microsoft a german company?\n\n:-)\n"
    author: "Tim"
  - subject: "Re: I signed the petition"
    date: 2002-02-08
    body: "Is Microsoft a german company?\n\n:-)\n"
    author: "Tim"
  - subject: "Re: I signed the petition"
    date: 2002-02-08
    body: "oh so you believe in the right of nations to decide for themselfs? well wake up boy, if the German Bundestag will start using KDe and therefor has to ensure that EVERYTHING works (meaning squishing all the bugs) it will mean quite a lot to everyone else as well.\nAnd if the German Bundestag decides to send soldiers all around the world it means quite a lot for the rest of the world as well.\nMy point is that there just is no such thing as \"national sovereignty\".\nWhat if there was a petition asking for the US-government to force MS to release the source-code of Windows under the GPL don't you think it would have quite an impact on all of us, and wouldn't you sign if you could?"
    author: "Johannes Wilm"
  - subject: "Re: I signed the petition"
    date: 2002-02-10
    body: "Yes! Nations have to decide for themeself, especially in such cases.\nThe intentions maybe good, but this doesn't help, it's simply responsibility. Other countries are simply not responsible for how the German Bundestag is working.\n\nThe only 2 cases, where suvereingty is not the principle is first if it touches human rights and second if their is a direct effect on other nationality.\nWhy? Because all other nationalities don't have to live with the result of decisions made on the content of petitions.\n\nOr in a different view: Petitions are \"direct democrathy\" and therefore absolut comparable to ellections and in ellections only the residents have the right to vote. Do you still think that they others should be able to vote in your own country?\n\nYour example with MS is not comparable, as a release of MS code would even have direct effect on my daily live. \nEven your example with Bug Squashing is not important. Do you really think I vote for OS in Bundestag to have less bugs on my local system? \n\nGeneral:\nIt's nice to have the world interested in OS in German Bundestag but on the single vote it's useless.\nTrust me, they will through out all non german votes."
    author: "Philipp"
  - subject: "Re: I signed the petition"
    date: 2002-02-10
    body: "> Do you really think I vote for OS in Bundestag to have less bugs on my local system? \nwe are ot talking about YOUR local system, but all computers for all of mankind!\n> Your example with MS is not comparable, as a release of MS code would even have direct effect on my daily live.\nWell the Bundestag using Linux would also have a direct effect on my life - they would have to develop all the tools that are needed for parliaments. And that would mean that other countries could switch quite easily a well.\n> Do you still think that they others should be able to vote in your own country?\nWell I think that all smaller central european countries should be able to vote in Germany - since they are no more independent from Germany than German states. And all of the world should be able to vote for the US-president, because when that guy screws up this planet we all have to suffer.\n> Trust me, they will through out all non german votes.\nWell trust me they are going to through away ALL the votes. I have been collecting signatures for other causes in Germany before and the form that you need in order for the sinature to count looks like a form where you sign your own death sentence or something. It is actually a A4-form whcich you aren't even allowed to copy - you can only get copies at the local city council. So officially, noen of this will count."
    author: "Johannes Wilm"
  - subject: "Re: I signed the petition"
    date: 2002-02-14
    body: ">Trust me, they will through out all non german votes.\n\nAs one of the initiators of bundestux.de I can guarantee that not \na single vote will be thrown away. As the petition does not follow\nany legal rules we welcome ALL votes from EVERY country!\n\nIn our point of view the idea of Free Software/Open Source is not\nlimited to boundaries. And the impact of switching to Linux in\nthe Bundestag is obvious for all countries, especially in Europe.\n\nFeel free to contact me if you have any furhter questions...\n"
    author: "Christian Adelsberger"
  - subject: "UK not planning mandation of OSS"
    date: 2002-02-13
    body: "I would like to know where the information about Great Britain planning to mandate OS in the public sector comes from. I am aware of the draft policy on Open Source in the UK government but this does not say any such thing. The draft policy talks about a \"level playing field\" and \"best value\" approach.\n\nCan someone tell me if there is other news on the UK possition?\n\nRichard"
    author: "Richard Taylor"
  - subject: "Re: UK not planning mandation of OSS"
    date: 2002-02-15
    body: "Have a look at this:\n\nhttp://www.theregister.co.uk/content/archive/23778.html\n\nRegards,\n\nKlaus"
    author: "Klaus"
  - subject: "Re: UK not planning mandation of OSS"
    date: 2002-02-15
    body: "The Register link you mention is the UK Police force looking in to open source. It does not constitute the UK considering mandating open source that the petition claims. The UK government stories that I am aware of are:\n\nUK Police (as you mentioned)\n\nOffice of the E-Envoy draft OSS policy\n http://www.govtalk.gov.uk/rfc/rfc_document.asp?docnum=429\n\nA Deparment of Trade and Industry sponsored report from the National Computing Centre\n http://www.theregister.co.uk/content/4/23653.html\n\nNone of these justify the claim made in the German petition.\n\nRichard\n"
    author: "Richard Taylor"
---
<a href="staerk_at_kde.org">Klaus St&auml;rk</a> has written in with
two stories of interest from Germany.
In the first, the <a href="http://www.kde.de/">German KDE website</a> has
<a href="http://www.kde.de/appmonth/2002/korganizer/index-script.php">announced</a>
the February 2002 <a href="http://www.kde.de/appmonth/">App of the Month</a>:
<a href="http://korganizer.kde.org/">KOrganizer</a>. As usual, the
useful (German) review includes a
<a href="http://www.kde.de/appmonth/2002/korganizer/beschreibung.php">description</a>
of KOrganizer, as well as a
<a href="http://www.kde.de/appmonth/2002/korganizer/autor.php">note by</a>,
and an
<a href="http://www.kde.de/appmonth/2002/korganizer/interview.php">interview
with</a>, its maintainer, <a href="mailto:schumacher_at_kde.org">Cornelius
Schumacher</a> (<a href="http://www.kde.org/people/cornelius.html">People of KDE</a>, <a href="http://dot.kde.org/1012803929/">dot story</a>).
In the second story, many of you know that the German <a href="http://www.bundestag.de/">BundesTag</a> (parliament)
is considering making KDE/GNU/Linux the pricipal OS/Desktop in the
parliament.  Klaus points to an online petition called
<a href="http://www.bundestux.de/">BundesTux</a>
(<a href="http://www.bundestux.de/english.html">english</a>),
&quot;<em>where people can subscribe
in order to say </em>&quot;Yes, it would be good to have Linux on the
servers and desktops in the german Bundestag&quot;<em>. So when Linux becomes
the desktop OS there, what could be the desktop environment,
then?</em>&quot;  Even if you do not plan to endorse the petition,
I suggest that you head over there anyway, the petition is
quite the great read.





<!--break-->
