---
title: "APPS.KDE.com:  New Features for a New Year"
date:    2002-01-16
authors:
  - "Dre"
slug:    appskdecom-new-features-new-year
comments:
  - subject: "Wrong Link"
    date: 2002-01-17
    body: "Great! Keep up the good work.\n\nThe \"first page\" link doent't work, though.\nIt gives a: \n\n   Sorry, the requested URL 'https://apps.kde.com/nf/2/story/15' \n   was not found. Please check the URL to make sure that the path \n   is correct.\n\nI think you meant: http://apps.kde.com/nf/2/story/15\nwhich works fine.\n\nAnd now on for some OFF-TOPIC stuff from the Some-things-I\n-would-like-to-see-incorporated-in-KDE dept.\n\nIn this era of huge 60GB hard disks, some stuff for making\nkeeping track of large quantities of files and data easier \nwould be handy. \n\n- File Emblems (a la Nautilus). Stamping a file as \n\"Hot\", \"Urgent\" etc would be very handy! (and not very\ndifficult too, since Konqueror already does say blending \nof mimetype with file icon).\n\n- Virtual Folders in Kmail (a la Evolution). When you\nuse this option it creates automatically a virtual folder\ncontaining the mails resulted from applying various searches, \nfiltering etc. \n\nAnd a little annoyance: can the menu entry for creating a new\ndirectory be treated specialy in Konqueror popup menu? I mean\nit is getting placed according to alphabetical sorting, which\nputs it somewhere in the middle-to-bottom of the entries. 90% \nof the time that's what I want to do, and every time I have to\ntrack it down in the menu. As a reference, in Windows it is \ntreated as a special case (along with \"create shortcut\" and is \non the top of the right mouse button popup menu). (Well, maybe \nthis is sorted out in the CVS KDE)\n\nJust my E 0.02 (euro cents)."
    author: "BugPowder"
  - subject: "wishes"
    date: 2002-01-17
    body: "Good suggestions. Why don't you head over to http://bugs.kde.org and submit them as wishlist items?\n\nGreets\nAnno."
    author: "Anno v. Heimburg"
  - subject: "Re: Wrong Link"
    date: 2002-01-17
    body: "Well, this https:// stuff doesn't seem to originate in the dot article.  I'm guessing this is a hack so that apps.kde.com can get a handle on your real IP instead of a proxy IP, and this hack did indeed manage to break Netscape when I first tested.  Otherwise it's some complex but unreliable rewriting of URLs that apps.kde.com seems to perform quite often, in god-knows-when situations."
    author: "Navindra Umanee"
  - subject: "Re: Wrong Link"
    date: 2002-01-17
    body: "> Well, this https:// stuff doesn't seem to originate in the dot article.\n\nThis should only happen if you have elected to use cookie-based authentication over SSL only and/or have elected to view the site only using SSL.  In either case you get re-directed to the SSL version of the page.  Unfortunately I forgot to update the https portion of the Apache configuration, and so the stories were broken for https until I read the post.\n\n> I'm guessing this is a hack so that apps.kde.com can get a handle on\n> your real IP instead of a proxy IP, and this hack did indeed manage to\n> break Netscape when I first tested. \n\nIt has nothing to do with that, the new \"story\" feature was just plain broken using either https or non-frames mode.\n\n> Otherwise it's some complex but\n> unreliable rewriting of URLs that apps.kde.com seems to perform quite\n> often, in god-knows-when situations.\n\nYeah, did I ever mention that stateless protocols suck?  The problem is you can view the site in a number of different ways; most of the URL rewriting that occurs is meant to prevent following someone else's link from rewriting your site preferences.  The easier way to get around this was to use cookies, but a large number of site visitors disable those, so I came up with this (hack) solution."
    author: "Dre"
  - subject: "Re: Wrong Link"
    date: 2002-01-17
    body: "Ah strange.  I didn't fiddle with any SSL stuff, and suspect neither did that guy.  I can't reproduce it anymore anyway, but http:// was clearly getting rewritten to https:// for a reason or other. :-)\n\nThanks for all the work you've done on this valuable site.\n\nCheers,\n-N."
    author: "Navindra Umanee"
  - subject: "Clutter"
    date: 2002-01-17
    body: "APPS.KDE.com is a great site, very well maintained (thanks Dre!), but the interface is a bit cluttered.  It seems to me like the site has way more features than I would ever use.  Maybe it is time to remove some of the lesser-used links from the pages and simplify the sidebar and top bar?"
    author: "not me"
  - subject: "Re: Clutter"
    date: 2002-01-17
    body: "Yes!  Remove the frames please!  There is not enough viewing area on this site for the real important stuff.  Too much clutter also makes this site slow."
    author: "KDE User"
  - subject: "Re: Clutter"
    date: 2002-01-17
    body: "> Yes! Remove the frames please! There is not enough viewing\n> area on this site for the real important stuff. Too much\n> clutter also makes this site slow.\n\nThe frames actually don't take up any space.  And both the things you are unhappy with are changed in just a few mouse clicks.  The site is very configurable, and I don't expect that the defaults will satisfy everyone.\n\nIf you don't like frames very much, though, I still would suggest considering\nkeeping the top frame separate.  There really is no point in re-downloading all that HTML for each page, the top part doesn't change, and the frame is barely visible.\n\nSo, to change your settings click on the \"configure\" button in the yellow navigation bar at the top.  Halfway down the page you will see a setting for \"number of columns\".  Set it to 1.  Then \"Save\" your changes.  If you want the change to be in effect next time you visit, accept the cookie; otherwise you revert to the site defaults next time you arrive.\n\nOn the next page, you will see a link to click, click on that to have the changes take effect.\n\nBTW, the difference between \"1 column\" and \"no frames\" is the top frame.  So if you want to get rid of all frames, don't worry about the number of columns but pick the \"no frames\" option instead.\n\nI wish it would be easier to make a nice GUI config tool for this in HTML.\n\nOn a side note, somebody else commented today they did not like the current colors.  The entire site is rendered with a theme class.  If you don't like the colors, if you can make the about-30 buttons the site uses (you can take the existing ones and just retouch them) and give some values for the text and background colors, the site can be easily themed, the same as www.kde.com.  If you are interested in making a theme, just let me know and I will help any way I can.\n\nThe only other big configuration option missing is languages.  Anyone interested in helping to translate the site, I can give you some files to start on :-)."
    author: "Dre"
  - subject: "Speaking of apps..."
    date: 2002-01-17
    body: "...is work being done on the KDE installer at all?  Will it be in KDE 3.0?"
    author: "Frank Rizzo"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "I doubt it. For one thing, whoever is working on it, it isn't KDE itself. KDE developers will tell you time and time again (just go on OPN #kde and ask) that KDE provides only source!\n\nPlus, why the heck does KDE need an installer? It's better to have the distros package KDE. Debian, for instance, already has a wonderful automated packing system in apt-get/dpkg. Why should KDE overdo their work for the sake of making stuff look nifty?"
    author: "Carbon"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "KDE needs an installer for dummies like me. Ximian is very tempting...\n\nR"
    author: "Robo"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "No, it's very simple. I dunno about other distros, but Debian makes it incredibly simple. Just run Debian Woody, and type (as root):\n\napt-get install kde\n\nThere's really nothing more to it. Besides, KDE comes with most distros anyways."
    author: "Carbon"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Many Linux people just don't seem to understand. Of all the mainstream distro's out there, Debian and Slackware were the two I was unable to install. Caldera was easiest followed by three others...RedHat, Suse and Mandrake.\n\nWe M$ 'haters' need help with installation. Why have Ximian done what they have with RedCarpet? Because it's easy for dummies like me.\n\nRob"
    author: "Rob"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "so use one of the more newbie/user friendly debian-based distributions such as: libranet"
    author: "coba"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Never heard of it...do you have a link to post?"
    author: "Rob"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Which is valid for all rpm based distro's as well!\nConectiva ported apt from deb to rpm\n\nhttp://apt4rpm.sf.net is a good starting point to\ngather more info about apt for rpm based distro's\n\nThe apt4rpm project takes cares of the server side and\nthe apt-rpm (from connectiva) project provides the client\nside.\n\nSo happy updating :)"
    author: "Richard"
  - subject: "apt-get chaos"
    date: 2002-05-18
    body: "You should have a look at the other dists! IMHO debian isn't usable when trying to _work_ under linux \nat the moment. WHY: At work we'd got a Debian dist preinstalled (and \"maintained\" by \"good-alike\" admins) - if this had been my first contact with linux (thanks god I had experiences with SuSE/Redhad/Mandrake) I'd got a very,very bad attitude to linux. While working with our local sysadmin (and of course during my \"own\" work) I experienced so much trouble with debian:\n * apt-get discards a running XFree-4 installation without prompting - the reason had been the \n   fetch of a _console-based_ program which in turn had been a minor dependency to the desired fetch!\n   (later we discovered that our desired program runs well without this extension!)\n * Qt and KDE libs disappears regularly when fetching other programs\n * common programs like pine or pico aren't available\n * kmail and kghostview keep crashing (KDE2.2 runs well under SuSE!)\n * KDE3 isn't available - I'm running KDE3 using SuSE since months! (even the prerelease runs well,\n   and KDE2 is still available!)\n * the libs are partially a deadly mix of gcc-3 and gcc-2 compiled stuff; some compilations offered \n   as stable releases (Qt2-check qconfig!) are wreckage\n * the offered multi-threaded Qt's are still non-threaded\nIMHO apt-get causes to much trouble when trying to keep working. We waste to much time repairing our system! I experienced RPM-bases system as more comfortable & more controllable, In addition, rpm is\nbetter when having self-compiled stuff. \nOk, the last point (you see, I need a bit whining-out) is the hardware support. On debian a lot of hardware failed to work (accerlated X, isdn cards etc.) A lot of my firends share my experiences that SuSE8.0 installation is much easier than Window$ setup - difficult hardware (isdn cards, ADSL router, accerlated & 3D X) runs \"out of the box!\nBut the most important point IMHO: the debian guys should pay more attention for not shaking up the local installation!"
    author: "Ruediger Knoerig"
  - subject: "Re: apt-get chaos"
    date: 2003-03-01
    body: "This is just not true. Most of your points are false.\n\nSorry if you see debian to be  hardcore distro, but most experienced admins and developers \ni know are loving debian.\n\nAnd for newbies like you, there is http://knoppix.net, which will run from cd and can be installed on hdd in 15 minutes, including self-configuration.\n"
    author: "Alekibango"
  - subject: "Re: apt-get chaos"
    date: 2004-04-07
    body: "I must agree that you're way off base on this one.  Debian is head and shoulders above Red-Hat and Mandrake. (I cut my teeth in linux on those two) I can't speak to the others distros.  \n\nLong and short is if you think Debian is anything other than a top-notch distro, you just don't know how to use it."
    author: "steve"
  - subject: "Re: apt-get chaos"
    date: 2004-04-08
    body: "> Long and short is if you think Debian is anything other than a top-notch distro, you just don't know how to use it.\n\nDebian is indeed top knotch, but I've found that Gentoo packages break far less often. Yeah, probably my fault, but I used to use unstable, and Gentoo's ~x86 branch is a lot more reliable. \n\nI will miss by Debian times though, spent three and a half years with it.. gentoo is my lover now :("
    author: "AC"
  - subject: "Re: apt-get chaos"
    date: 2004-04-07
    body: "I must agree that you're way off base on this one.  Debian is head and shoulders above Red-Hat and Mandrake. (I cut my teeth in linux on those two) I can't speak to the others distros.  \n\nLong and short is if you think Debian is anything other than a top-notch distro, you just don't know how to use it."
    author: "steve"
  - subject: "Re: apt-get chaos"
    date: 2005-12-27
    body: "I changed to Debian from several years of successful Slackware employment. I've had only minor experience running RedHat on a desktop machine, which ran fine.\n\nIMHO, one of the biggest difficulties when migrating to Debian from some other \"bare-bones\" distro as Slackware is that you'll have to relearn how to do some things \"The Debian Way\", in order for it to work smoothly. Also, Debian is highly dependant on it's packaging system, Apt.\n\nThe trouble with Apt is that if you run a non-stable release of Debian (commonly refered to as \"testing\" or \"unstable\", with the latter being the most bleeding-edge), you might expect some weird behaviour from Apt from time to time.\nTesting if fairly ok, but Unstable's repositories might as well bomb out on you all of a sudden, leaving you with broken dependencies, non-installable packages or, in some cases, semi-permanent damage to apt itself...\n\nMost of the time, you can solve those issues, but the solutions often varies from case to case, and aren't too well documented. Often it includes removing some select files and performing an upgrade, but I've seen cases where people had reinstall Debian from scratch...\n\nAnother issue as of 2005 was the rapid integration of GCC 4.0 into Etch (testing). Several software packages has compatibility issues with this new compiler, and yet it was chosen as the default compiler for all packages.\n\nWeird.\nApt can also be rather cumbersome if you want to replace an package with a custom version for the sake of improved functionality, but other packages depends on the original package. This is where I like the simplicity of Slackware. It doesn't have much of a package system, so nothing gets in your way. Most of the time, this is only an issue on desktop systems wanting the latest, bleeding-edge stuff. Debian is less than perfectly suited for that.\n\nIf you want bleeding-edge stuff on your desktop, you might try Ubuntu instead (everything is so fluffy you'll almost forget its Linux! I use it on my ThinkPad X41.), or perhaps... The unmentionable distro... \"Gen-thoo\"...\n\nHowever, Debian testing (and to some degree, unstable) works well as a day-to-day desktop work distro, unless you want something outrageusly fancy.\n\nFor servers, I'd say Debian stable is close to unbeatable. Other possible choices would include some BSD derivative."
    author: "Martin Persson"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Speaking as a BeOS escapee, and relatively new to linux, the idea of installing KDE scared me at first. Those days I was running SuSE and every time KDE came out with a new version I'd download the RPMs, install them, and something would always break. Always something. It was a pain.\n\nEventually, I moved over to slackware, and once I read the tutorial for installing KDE from source I realized it couldn't possibly get any easier. Plus, you can get a optimized, objprelinked system with great performance. I think that the folks who complain about KDE's performance have never run an optimized locally compiled version.\n\nI'm not wanting to sound like a rtfm zealot, but, seriously, read the tutorial (http://www.kde.org/install-source.html). I've done this several times, now, with great results. And I've only been really using linux for a year, tops."
    author: "Shamyl Zakariya"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Take a look at http://www.mslinux.com/software/kinstaller/index.html he is working one a installer fore KDE"
    author: "dr88dr88"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Does this look kinstaller a bit like apt-get ... but the webpage makes notice it doesn't do dependecies? This app would really make life easier for a lot of people -- Fab"
    author: "fab"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "The problem with the distros and the way KDE needs to be installed for those that aren't using Debian is that the bug fixes don't come in a timely fashion. KDE is huge. When I want to install one basic program to fix a bug I can't. I have to get the entire package and sometimes more than one and pray that they compile when all I want to do is get one program and have it work. Very often they do not compile and sometimes, when I ask for help, I get ignored. Other time the distros do not enable the entire feature set that is available as a default. Such as support for ptal in the kdegraphics packages or alsa in the kdemultimedia libraries.\n\nThe beauty that comes with red-carpet is that individual packages are installable, dependancies are taken care and nobody has to explain anything to me. When there is a bug fix it is published, I can download with no muss, no fuss. I prefer KDE to gnome for various reasons, but the differences between the two environments are quickly evaporating. I hope that somebody will put some serious thought into this. The installers like red-carpet up2date and Ximians red-carpet are great. And the installers are very compelling reasons to switch."
    author: "Eli Wapniarski"
  - subject: "Re: Speaking of apps..."
    date: 2002-01-17
    body: "Yeah - this is an ongoing problem with KDE. It's nice that so much stuff IS bundled in kde-utils etc. but it's areal drag if only ONE of the apps in the bundle is broken and need re-compiling. SuSE, for instance, seem to have a habit of supplying a broken kpackage (won't recognise rpm files) with their upgrades.\n- Alan"
    author: "Alan Prescott"
  - subject: "Why apps.kde.com."
    date: 2002-01-17
    body: "I allways enter apps.kde.org ;)\n\nI think it is the better name for this site."
    author: "hal"
  - subject: "Re: Why apps.kde.com."
    date: 2002-01-17
    body: "Sounds like a good idea to have at least a pointer.  Of course, all parties involved would have to agree to it...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Why apps.kde.com."
    date: 2002-01-18
    body: "I think a forward to apps.kde.com is also ok.\nI think if someone looks for apps @ the kde domain, he will try http://www.kde.org/apps and apps.kde.org.\nIt's a place for oss applications. And a .com domain is the wrong (or not perfect) place for this.\n\n\nhave fun\nHAL"
    author: "hal"
  - subject: "RDF changed?"
    date: 2002-01-17
    body: "Application index on http://www.kde.org seems little broken after your changes."
    author: "someone"
  - subject: "Re: RDF changed?"
    date: 2002-01-18
    body: "Hmmm, thanks.  Seems like the old RDF that we were pulling disappeared.  Mailing Dre...\n\n-N."
    author: "Navindra Umanee"
  - subject: "Appsy default colors are too dark"
    date: 2002-01-18
    body: "and the fonts are too small. A bit of bright colors be good,."
    author: "Asif Ali Rizwaan"
---
Over the last few weeks, I have been busily adding a number of new features and enhancements to <a href="http://apps.kde.com/">appsy</a>. Most of them enable
greater customization of site content, such as receiving email notification when a particular application or any application within a particular category is updated.  Others provide more information and statistics about the site and the software listed on it.  And, not coincidentally, there is a new "news" system
where you can post comments on a story as well.  You can find out about the
details in the site's <a href="http://apps.kde.com/nfinfo.php?sid=15">first
story</a>.  My apologies to those who visited the site during the brief
moments when the reconstruction caused you problems.
<!--break-->
