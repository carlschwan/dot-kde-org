---
title: "People of KDE: Lubos Lunak"
date:    2002-01-14
authors:
  - "Inorog"
slug:    people-kde-lubos-lunak
comments:
  - subject: "Vaporware"
    date: 2002-01-14
    body: ">> When did you start to use KDE on a daily basis?\n> If I remember correctly, it was at the times KDE1.2 was released.\n\nThat`s consistent: He not only programs vaporware but used one. :-)"
    author: "someone"
  - subject: "Re: Vaporware"
    date: 2002-01-14
    body: "Ah ... It was a long time ago, you know ... *counting on fingers* many years ago. I guess it was KDE1.1.1 then. (1.2=1.1+1  ;)  )."
    author: "L.Lunak"
  - subject: "Re: Vaporware"
    date: 2002-01-14
    body: "Short mnemonic: Highest KDE1 was 1.1.x, highest KDE2 was 2.2.x and highest KDE3 will be 3.3.x?"
    author: "someone"
  - subject: "This is a good one :)"
    date: 2002-01-14
    body: ">I usually have both the latest stable release and a recent CVS build installed. So if one version doesn't work, I can run the other one.<\n\nLOL :)"
    author: "antialias"
  - subject: "Re: This is a good one :)"
    date: 2002-01-15
    body: "Heh :) I do the same here to minimize show effects :)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Best movie yet"
    date: 2002-01-14
    body: "I laughed my ass off hard at that proposed KDE commercial.  :)  That's the best one yet."
    author: "ac"
  - subject: "Re: Best movie yet"
    date: 2002-01-15
    body: "A bit off topic but did any USians see the new IBM commercials\nduring the playoffs yesterday?"
    author: "ne..."
  - subject: "Re: Best movie yet"
    date: 2002-01-15
    body: "What happened in it?  Different to \"The Heist\"?"
    author: "Daniel"
  - subject: "Re: Best movie yet"
    date: 2003-06-24
    body: "Can you tell me about the IBM COMMERICIALS ON TV? I don't have a tv but am very interested in learning more about the content and style of the IBM tv commercials. Thanks much"
    author: "madison"
  - subject: "A penchant for helpful hints"
    date: 2002-01-16
    body: "> a penchant for posting helpful hints on the development mailing lists.\n\nLubos had really helped smooth my entry into KDE development, too.  He gave plenty of tips and reviewed my code for the first couple weeks, so here's my 'Cheers' to him! ;)\n\nEllis"
    author: "Ellis Whitehead"
  - subject: "Re: A penchant for helpful hints"
    date: 2005-04-29
    body: "...just googling you and all i come up with is lunix (sp?) programming mumble jumble!!!\nhope you're well.  and dora too!!\n\nxoxo,\ngne"
    author: "gne"
---
Many KDE developers know that <a href="http://www.kde.org/people/lubos.html">Lubos Lunak</a> is a serious developer with a strong grasp of C++ and a penchant for posting helpful hints on the development mailing lists. KDE users will be happy to find out that Lubos is the developer behind the popular <a href="http://dforce.sh.cvut.cz/~seli/en/khotkeys/">KHotKeys</a>, a tool that has received high marks since its first appearance in KDE 1 and that is now a gem in Kicker's menu
editor (in KDE 2/3). Lubos is <a href="mailto:tink@kde.org">Tink</a>'s interlocutor for this week's <a href="http://www.kde.org/people/people.html">The People Behind KDE</a> interview.
<!--break-->
