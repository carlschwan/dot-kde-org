---
title: "People Behind KDE: Jason Katz-Brown and Masaji Takeyama"
date:    2002-05-27
authors:
  - "wbastian"
slug:    people-behind-kde-jason-katz-brown-and-masaji-takeyama
comments:
  - subject: "RPN"
    date: 2002-05-27
    body: "I challenge anyone here to produce the nicest, more origina picture of a reverse polish noatun, NOW!\n\nOn other news, there are guys working on KDE now that were born while I was in college. I must go kill myself in the fashion of Logan's Run now."
    author: "Roberto Alsina"
  - subject: "Re: RPN"
    date: 2002-05-27
    body: "... Just make a run for it with Jenny Agutter.\n"
    author: "Corba the Geek"
  - subject: "dot post"
    date: 2002-05-27
    body: "Haha, cool, Jason included a link to his first dot/developer post! I even vaguely remember mentally shaking my head at that one. ;)  \n\nGreat screenshot, btw."
    author: "Navindra Umanee"
  - subject: "Re: dot post"
    date: 2002-05-27
    body: "shaking your head as in \"wow, what an idiot!\"?\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: dot post"
    date: 2002-05-30
    body: "Nothing as rude as that! :)\n"
    author: "Navindra Umanee"
  - subject: "satisfaction"
    date: 2002-05-27
    body: "aaaaah! the satisfaction of helping someone to fail miserably. ;-)\n\nbut seriously: great work Jason! you've done a lot of great code and i can only assume we'll see more of the same in the future!\n"
    author: "Aaron J. Seigo"
  - subject: "killing grass?"
    date: 2002-05-28
    body: "> I had two rabbits, but they died after they ate grass which I bought. ;-(\n\nHmmmm. Now what are the odds in that?"
    author: "Carsten"
  - subject: "Re: killing grass?"
    date: 2002-05-31
    body: "> I had two rabbits, but they died after they ate grass which I bought. ;-(\nI had two rabbits, but they died after they ate grass which I brought. ;-(\n                                                               ^\n> Hmmmm. Now what are the odds in that?\n\nI think a herbicide in that.\n"
    author: "M. Takeyama"
  - subject: "Always Discovery Channel!!"
    date: 2002-05-28
    body: "I can't believe it!\nNo KDE developer answered Baywatch, right? Or my favourite Discovery Channel stuff but with the Baywatch cast :)))\n"
    author: "J.A."
  - subject: "Re: Always Discovery Channel!!"
    date: 2002-05-28
    body: "Masaji-san answered Baywatch... :)"
    author: "Jason Katz-Brown"
  - subject: "Re: Always Discovery Channel!!"
    date: 2002-05-31
    body: "> I can't believe it!\n> No KDE developer answered Baywatch, right? Or my favourite Discovery Channel\n> stuff but with the Baywatch cast :)))\n\nI am sorry, I just thoughd baywatch means to watch bay.\nWhat Baywatch is ?\n"
    author: "M. Takeyama"
  - subject: "Re: Always Discovery Channel!!"
    date: 2002-05-31
    body: "It's a show about several overly endowed lifeguards running around in slow motion.\n\nAlright, it's actually a show about lifeguards, but everyone watches it for the above part. :-)"
    author: "Carbon"
  - subject: "Two folks I rely on in one piece!"
    date: 2002-05-30
    body: "Gotta say it.  Thanks to both Jason and Takeyama-san!!\n\nI use Jason's Kiten quite frequently and it is a great little tool!  And I \ngotta say thank you to Takeyama-san as well, as I solely use the Japanized version\nof KDE....one of these days I'll find the time to help out with the translations(I got as\nfar as downloading the .po files)\n\nReally, a big thank you to both!!!"
    author: "rob"
  - subject: "Re: Two folks I rely on in one piece!"
    date: 2002-05-31
    body: "> Gotta say it. Thanks to both Jason and Takeyama-san!!\n>\n> I use Jason's Kiten quite frequently and it is a great little tool! And I \n> gotta say thank you to Takeyama-san as well, as I solely use the Japanized\n> version of KDE....one of these days I'll find the time to help out with \n> the translations(I got as far as downloading the .po files)\nThank ypu. other members of us are pleased with your reply.\n\n"
    author: "M. Takeyama"
  - subject: "Re: Two folks I rely on in one piece!"
    date: 2002-05-31
    body: "I too love the Japanese KDE translations, even if they are sometimes a little bit hard to read :-)\n\nThanks so much!\n\nJason"
    author: "Jason Katz-Brown"
---
Despite being up to her ears into the moving boxes, Tink managed to publish the last two interviews of
<a href="http://www.kde.org/people/people.html">this season</a>
today. You can learn more about 
<a href="http://www.kde.org/people/jason.html">Jason Katz-Brown</a>
who recently enriched KDE CVS with Kolf. And about 
<a href="http://www.kde.org/people/masaji.html">Masaji Takeyama</a>
who brings KDE to the masses in Japan. Enjoy!
<!--break-->
