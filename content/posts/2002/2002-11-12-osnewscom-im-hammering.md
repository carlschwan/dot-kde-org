---
title: "OSNews.com: I'm Hammering"
date:    2002-11-12
authors:
  - "numanee"
slug:    osnewscom-im-hammering
comments:
  - subject: "I'm hammering..."
    date: 2002-11-12
    body: "See, I'm hammering too :)<p>\n<a href=\"http://bugs.kde.org/buglist.cgi?emailreporter1=1&emailtype1=exact&email1=Luke7Jr%40yahoo.com\">bug list</a>"
    author: "Luke-Jr"
  - subject: "Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "The author of the article complained about the \naliased fonts and jagged edges around his icons.\nHe was unable to build the Qt library with support\nfor Xft, which I believe to be the root of the\naliased/jagged appearence of fonts/icons that he\nwas seeing.\n<p>\nDon't you see the problem here? All that fancy\nalpha transparency just does not work without\nthe Render extension on the X server. This means\nthat KDE 3.* + icons with alpha will look like\ncrap on Linux boxes still running the old \nXFree86 3.x.y X server, as well as the commercial\nX servers on Solaris and Irix which will likely \nnot support the Render extension in the near future.\n<p>\nI think KDE would do itself a better service by\nshipping with less eye-candy by default (the old\nhicolor icons were fine without the Render extension).\n<p>\nPaul.\n<p>\nPS. I am running KDE 3.0.1 on Solaris 2.6, and\nicons utilising the alpha channel do look like crap."
    author: "kesha"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "On first startup, you can choose from different themes/styles (one of which is the Keramik theme) - at least last time I checked, this was the case.\n\nSo I think it's still perfectly possible to select the 'default highcolor' scheme if you find the 'eye-candy galore' themes to look like total crap on your display.\n\nNow about what Red Hat 8.0 installs for a default...well yes...let's not go there shall we?\n\nIf you rm -rf ~/.kde I'm pretty sure you'll get a 'setup wizard' the first time you start KDE again, which will ask you which style you want to use, and how many CPU-cycle-consuming-but-nice-looking features you want or don't want."
    author: "W.K. Havinga"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "I am sorry about the \"crap\" comment.\n\nI know I am perfectly capable of installing the hicolor icons\nmyself. That really is not solving the underlying problem in\nthe long term. Eventually the hicolor icons will become obsolete \nas more features are added to KDE. There will be a discrepancy \nbetween the number of icons used in the default icon theme, and \nthe number of icons in the hicolor theme, which means I will see\n\"question-mark\" or \"kde-gear\" default icons for things which do \nnot have a corresponding icon in the hicolor theme.\n\nA better solution is to implement a software fallback rendering\nengine that could perform the same functions that Render performs,\nbut on X servers that do not support the Render extension. However,\nthis is probably too-much work for too-little benefit to the\nwider audience (the Linux + XFree86 4.x.y). After all, how many \nKDE Solaris users are there?\n\nMaybe I should stop being so selfish and let you enjoy the Crystal/Keramik\neye-candy by default (provided you have the Render extension ;)\n\nPaul.\n"
    author: "kesha"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "Well, there was a software fallback on KDE 2.2.2 (kalphapainter.cpp). But as far as I know it was removed after Qt started to support XRender. There is a thread about this on kde-devel or kde-core-devel. I tried to port kalphapainter to KDE 3 but I'm no graphics expert. It just didn't work. And some words to the hicolor theme: Of course I can (must) switch to it, but it doesn't look good either, without alpha blending. Hicolor on KDE 3 looks worse than on KDE 2.2.2.\n\nI wrote some mails about this to kde-devel and kde-solaris, but got no response. So it seems that there are indeed not very many non-Xfree users of KDE."
    author: "meikee"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "I believe that Keith Packard's plan for newer versions of the render library includes just what you wanted... a fallback for servers without the extension. So everyone will be able to benefit, not just KDE users."
    author: "AC"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "As far as I know, Keith Packards newer Render library is still for\nXFree86 4.x.y only. All it will do is provide software Render fallback\nfor cards which do not have hardware acceleration for Render. Render will\nnot be a stand-alone software-rendering library (to my knowledge), because\nin order to provide accelerated Render functionality it will have to be \na part of the XFree86 X server (an X server extension).\n\nWhich means that this will benefit users with older/weaker graphics cards, \nwho are running XFree86 4.x.y, but it will do nothing for people running\nXFree86 3.x.y, nor for commercial X servers on SUN and SGI.\n\nPaul.\n"
    author: "kesha"
  - subject: "Why on earth would KDE wait for Sun and SGI?"
    date: 2002-11-12
    body: "People like you can always choose the high color icons if you insit on using commercial closed source software with less abilities than XFree86.\n\nAlso, I don't think Sun can be called a friend of KDE."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Why on earth would KDE wait for Sun and SGI?"
    date: 2002-11-12
    body: "\n\tYeah !!! they are a  friend....but of GNOME....not KDE....."
    author: "Rodolfo Conde Mtz."
  - subject: "Re: Why on earth would KDE wait for Sun and SGI?"
    date: 2002-11-12
    body: ".. And the fact that XFree86 is the dominant implementation of X11 now, especially in the open source world (read: the world that encomposses KDE)"
    author: "asf"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "Hmm, does something changed to Solaris 2.8?\nI'm running it with the actual kde31-rc2 and have now problems with alphablending.\nIt was ugly with the kde 3.0.x series, but it seems to be gone now.\n\nI switched back to the classic icons because crystal looks to me a bit, hmm let's say \"psychedelic\" -:))\n\nOne things I'm still working on is that qt-copy-3.1.0-rc3 does not compile completely. The libs are fine, but qtdesigner, especially libeditor.so, is breaking -:(("
    author: "thefrog"
  - subject: "Re: Crystal icons look bad without Render extension"
    date: 2002-11-12
    body: "Did you try a 'make clean'?"
    author: "Peter"
  - subject: "Re: Crystal icons look bad without Render extensio"
    date: 2002-11-13
    body: "but he also said that he is running RedHat 8.0, which comes with Xfree86 4.2."
    author: "mefesto78"
  - subject: "Yep"
    date: 2002-11-12
    body: "RC2 is great! Its just a sin that Liquid isn't the default theme.\n\nThat screenshot you link to doesnt show the stipples, though, which really fill out the plain windows.\n\nOther than that, I looooove my KDE :)"
    author: "Moo"
  - subject: "Re: Yep"
    date: 2002-11-12
    body: "kde is great.\nThat's right, it the best of the best kde release \nI have ever compiled. KDE is really a stable, rich, \nnice environment for the linux desktop.\n\nkde team did a great job!\n\nsocialEcologist\n PS 1: yes, I think stripples are better than flat \nframe backgrounds\n PS 2: don't forget your --disable-debug --enable-final \nflag to compile a fast kde (with gcc3.2)\n"
    author: "socialEcologist"
  - subject: "I'm definitly HAMMERING !!! :)"
    date: 2002-11-12
    body: "\n\tKDE 3.1 really rocks.....tons of new features and many improvements.....its not just eyecandy...its really a GOOD software worth it......and BTW congratulations to the KOffice team....i couldnt even install OOo 1.0.1 in linux, and with KOffice aside from some crashes and some weird things in KWord and the interaction with KFormula, with some playing im been able to write really professional documents, matematical reports and all without me knowing a single LaTeX command, with the AA fonts, freetype and all my fonts....really COOL :)....and the UI of KFormula is more user friendly that the formula component of OOo (tested on Windowz) i hope soon the KOffice team has working filters for all the OOo formats...\n\n\n\tGreat job to everyone !!! Keep it up !!!!!\n\n\tAnd remember everyone...Linus uses KDE...uses KWord...uses KPresenter :)......\n\n\tCheers....\n"
    author: "Rodolfo Conde Mtz."
  - subject: "Noatun on RedHat 8.0"
    date: 2002-11-12
    body: "where is my mp3???noatun says that it supports mp3 but redhat says that they removed this support... :(.i have xmms now palying mp3 after installing a library but i want noatun.i really like noatun and its full integration with KDE.is there any way to play mp3 some how on noatun???if yes,mail me"
    author: "Ahmed"
  - subject: "I have the same prolem"
    date: 2002-11-13
    body: "Of course, with the number of complaints I've read about noatun maybe this is a good thing."
    author: "Rimmer"
  - subject: "Re: I have the same prolem"
    date: 2002-11-13
    body: "Red Hat is to blame..."
    author: "ac"
  - subject: "Re: I have the same prolem"
    date: 2003-03-31
    body: "i cant send aan IM"
    author: "ROBBY"
  - subject: "Re: I have the same prolem"
    date: 2003-03-31
    body: "i cant send aan IM"
    author: "ROBBY"
  - subject: "Re: I have the same prolem"
    date: 2004-12-11
    body: "i have a problem with attachment and i have formated my pc 4 time but the problem is same pls solve this problem  and one thing net is ok every sites is open but no file attache so i dont send any document  ok \nthx"
    author: "bharat"
  - subject: "Re: Noatun on RedHat 8.0"
    date: 2002-11-13
    body: "Well, Red Hat, having applied for its own software patents, is honoring other peoples' software patents.\n\nIf you want mp3, then you need to stop using Red Hat, and tell others to do the same."
    author: "Neil Stevens"
  - subject: "Re: Noatun on RedHat 8.0"
    date: 2003-04-28
    body: "Same problem. But there must me a solution for it, isn't it? I tried to install a newer Noatun from KDE.org but it doesn't work neither."
    author: "Ward Schmit"
  - subject: "Re: Noatun on RedHat 8.0"
    date: 2003-10-14
    body: "i believe in open-source and i'm not agree with the redhat software patents question. software isn't of redhat, so they coudn't invite us to use our linux box as they think we should. everyone likes play his favourites mp3s so why shoudn't?\nwell, redhat encourage not more mp3 but \"unfortunatelly\" xmms isn't of redhat, and that's the solution: there's an xmms plug-in for mp3 and it name is xmms-mpg123-1.2.7.21.i386.rpm. you could find it on google very fast, i use redhat9 and works fine. why should we use redhat and xmms and not redhat and xmms with plug-in?"
    author: "padreTony"
  - subject: "Why so many RC's ?"
    date: 2002-11-12
    body: "Dear friends,\n\nLet me respectfully suggest something.  Why not kill all the obvious bugs during the betas, make just one release candidate, give time to the interested distros to package it (as they do with the betas), check for showstoppers, fix them if any, and then either rename RC as final or make the final release. It occurs to me that this way:\n\n1) Less effort would go in release related stuff (as opposed to the effort of releasing / coordinating several RC's)\n\n2) More people would test the RC\n\nAfter all, a release candidate is exactly this: a version that looks finished, and that will in principle become the final release. If the idea is not to release binaries of the RC to avoid mixing up packaging errors with code bugs, the RC can be released src only.  \n\nEither way KDE is fantastic, and the developers/translators/etc., the whole team is making a damn great job. I just though I should post just one more opinion at the hectic bazaar, in case it helps :-) "
    author: "NewMandrakeUser"
  - subject: "Re: Why so many RC's ?"
    date: 2002-11-12
    body: "For this release I have really the impression that the turnaround times are really short. I finished compilation for rc2 on sunday, monday rc3 comes out ...\nSo what now? Testing rc2 or compiling rc3?\n\nCan't we slow down kde speed a little bit?\n"
    author: "thefrog"
  - subject: "Re: Why so many RC's ?"
    date: 2002-11-12
    body: "> Can't we slow down kde speed a little bit?\n \nNo, these are RCs and not several weeks living Betas with announcements and binary packages distribution."
    author: "Anonymous"
  - subject: "Re: Why so many RC's ?"
    date: 2002-11-12
    body: "> After all, a release candidate is exactly this: a version that looks finished, and that will in \n> principle become the final release. If the idea is not to release binaries of the RC to avoid \n> mixing up packaging errors with code bugs, the RC can be released src only. \n\nWell, that's what KDE 3.1 RC's all were. It's just that there is a major showstopper left: No Qt 3.1-final ;-)\n\nMarc"
    author: "Marc Mutz"
  - subject: "Re: Why so many RC's ?"
    date: 2002-11-13
    body: "> > principle become the final release. If the idea is not to release binaries of the RC to avoid \n> > mixing up packaging errors with code bugs, the RC can be released src only. \n>\n> Well, that's what KDE 3.1 RC's all were. It's just that there is a major showstopper left: No Qt > 3.1-final ;-)\n\n\nHa !, true :-), but really, while the last RC was not planned, several quick RCs were planned ahead in the release schedule for 3.1 - I guess my point is, if you plan them ahead, they are not really RC's. They are betas. And I don't see the use of short lived betas. By definition, the RC should be one (except if sh*t happens, but that you can't plan :-). So, my suggestion in the end is: why not plan just 1 RC, give people time to use it and see if it turns into final with no changes. As I said before, just an idea ... Cheers :-)"
    author: "NewMandrakeUser"
  - subject: "splash screen"
    date: 2002-11-12
    body: "The new KDE 3.1 splash is hella sharp!"
    author: "ac"
  - subject: "Toolbars"
    date: 2002-11-12
    body: "Hi\n\nI noticed something in the screenshots:\n\nOn those screenshots, you can see that konqueror has very few icons (back, forward, up, home, reload and stop). IMO that should be the default configuration for two reasons.\n\n1) It make konqueror easier to use because only the most useful items are on the toolbar.\n\n2) It makes it nicer because you can have only one toolbar instead of 2 or 3. And as keramik isn't very nice with lots of toolbars, it's better.\n\nI think this could be applied to lots of apps (kword for example). The less toolbar icons you have the easiest it is for the user to find what he looks for and the nicest it looks. Look at GNOME apps. I haven't seen any GNOME apps having more than one toolbar (in fact, I haven't seen any non-KDE app having more than 1 toolbar).\n\nAnd the most important part is that with 2 or 3 toolbars, you reduce vertical area for the app, which can result in usuability problem at low resolutions.\n\nCheers."
    author: "Julien Olivier"
  - subject: "Real Hammering !!! But Red Hat 8"
    date: 2002-11-12
    body: "KDE 3.1 is real cool .... but having some font problems with redhat 8.0\n\nRed Hat 8.0 is confusing .......with their own standards\n:(\n\nGood Work KDE Team"
    author: "Muthu.V"
  - subject: "Re: Real Hammering !!! But Red Hat 8"
    date: 2002-11-12
    body: "Learn it, because the way Red Hat 8 handles fonts is the way every distro will handle fonts in the future."
    author: "Joe"
  - subject: "Re: Real Hammering !!! But Red Hat 8"
    date: 2002-11-12
    body: "Yes, but not simply becuase it is Red Hat,but becuase as usual they are\nimplementing something that isn't quite finished yet ( close, tho)\nKeith Packard's excellent xft2, which does look like it will be the definitive\nsolution to font handling in Linux.\nNow if only they had used this \"best of breed\" approach to package\nmanagement we would all be using debs and apt.\n\nGod Speed , KDE 3.1"
    author: "jimjamjo"
  - subject: "Expanding keramik title bar?"
    date: 2002-11-12
    body: "Hey, what happened to the expanding frame around the title in the title bar?  In older versions, the frame around the title would grow for the active window, to be a little higher than the top of the title bar.  I thought that was a really nice effect, but it seems to be gone from the keramik screen shot in the article.\n"
    author: "Andy"
  - subject: "Re: Expanding keramik title bar?"
    date: 2002-11-12
    body: "In fact, it's an option. I don't know what the default is in this release though."
    author: "Julien Olivier"
  - subject: "Re: Expanding keramik title bar?"
    date: 2002-11-12
    body: "The default is still on."
    author: "Rayiner Hashem"
  - subject: "Re: Expanding keramik title bar?"
    date: 2002-11-12
    body: "I hope it's back with a more contrast default colour scheme."
    author: "ac"
  - subject: "easy customization is the answer to clutter."
    date: 2002-11-12
    body: "Next to being slow for those with older hardware, KDE being a little too busy seems to be the most common criticism of it.\nIt is a difficult job to square  frequency of use, comprehensiveness,and proper\ncategorisation.\nThe answer may lie in easy customisation , so that those who like a cleaner\nsimpler interface can cull away. Of course, KDE has this now but perhaps\nit needs to be emphasized more.\nThat or perhaps some kind of \"level\" toggle that could reaveal an extra layer\nwith more detail. At most 3 layers in total, but probably just 2 would be best.\n\nThis could do wonders for the Menu List.\n\n"
    author: "jimjamjo"
---
<a href="mailto:webmaster@gemineye.biz">Corey Taylor</a> has written an article for <A href="http://www.osnews.com/">OSNews.com</a> called <a href="http://www.osnews.com/story.php?news_id=2115">"KDE 3.1 RC2: I'm Hammering"</a>.  He has some font and icon issues with Red Hat 8.0, but overall seems happy. It's good to see people actually appreciating the new <a href="http://promo.kde.org/newsforge/kde-3.1.html">eyecandy</a> and all the hard work by the <a href="http://artist.kde.org/">artist team</a> and developers.  A wider range of opinions can be seen from their <a href="http://www.osnews.com/comment.php?news_id=2115">readers</a> -- one even provides some nice screenshots of <a href="http://home.mindspring.com/~heliosc/kde31.png">Liquid</a> and <a href="http://home.mindspring.com/~heliosc/fonts.png">Keramik</a>.
Sit tight for KDE 3.1, which has been <a href="http://developer.kde.org/development-versions/kde-3.1-release-plan.html">delayed</a> in favour of more RCs.







<!--break-->
<p>
<i>Are you hammering on KDE 3.1 RC2 as well? Use the reply button below and let the KDE developers know what you think of it!</i>
