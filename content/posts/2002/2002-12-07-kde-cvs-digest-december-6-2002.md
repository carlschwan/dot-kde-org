---
title: "KDE-CVS-Digest for December 6, 2002"
date:    2002-12-07
authors:
  - "dkite"
slug:    kde-cvs-digest-december-6-2002
comments:
  - subject: "ZModem?"
    date: 2002-12-07
    body: "Zmodem as in the ZModem data modem transfer protocol from the bbs days?  I though everyone on the kde team were basically students who read about zmodem in history books.\n\nCool."
    author: "Frank Rizzo"
  - subject: "Re: ZModem?"
    date: 2002-12-07
    body: "I'm not a KDE developer, only a simple developer. Also, I'm a student.\nIn Gymnasium (similiar to am. College) the technologist's education had the possibility to have a course in \"Net-technology\". There we didn't read aqbout those stuff, we practised them. It's was all very low-level and all the way through 1960 to 1995.\n\n"
    author: "Syllten"
  - subject: "They should have released it..."
    date: 2002-12-07
    body: "Anyway, we are currently all using previous version of KDE that have this security issue. Releasing it or not change nothing towards this situation. But if they haved released it at the start of december, KDE 3.1.1 will have been ready for mid-january, just like KDE 3.1 will be ready by this time.\nA lot of people will have had the time to look at it in their Christmas holidays. But today, what we have, it is frustrated people that are waiting already for one month and that will have to wait for another month for a security problem that affect also the KDE version they are currently using..."
    author: "murphy"
  - subject: "Re: They should have released it..."
    date: 2002-12-07
    body: "no, you're not being left waiting for security updates, just for 3.1 a release.\nAIUI, there will be security updates available for 2.2.x and 3.0.x as soon as possible (these are less work for packagers than 3.1 would have been), and you'll get another 3.1RC to tide you over for shiny features. \n\nSo it's not quite as bleak as all that..."
    author: "Kevin Puetz"
  - subject: "Re: They should have released it..."
    date: 2002-12-07
    body: "I respectfully disagree. Packagers, ie the distributions, wouldn't release something with a large number of known potential security issues.\n\n3.1-rc5 is at http://www.kde.org/info/3.1.html.\n\nDerek"
    author: "Derek Kite"
  - subject: "Koffice 1.2.1?"
    date: 2002-12-07
    body: "Hey, the author claims Koffice 1.2.1 to be released, but I can't find it anywhere. Neither on www.koffice.org or ftp.kde.org."
    author: "Syllten"
  - subject: "Re: Koffice 1.2.1?"
    date: 2002-12-07
    body: "http://developer.kde.org/development-versions/koffice-1.2-release-plan.html"
    author: "Anonymous"
  - subject: "hella cool"
    date: 2002-12-08
    body: "That download plugin for Konqueror is hella cool!"
    author: "ac"
---
The <A href="http://members.shaw.ca/dkite/latest.html">latest issue</A> of KDE-CVS-Digest is done and ready.  Read about everything from the latest CVS updates of <a href="http://kmail.kde.org/">KMail</a> to new <a href="http://www.konqueror.org/">Konqueror</a>, <a href="http://konsole.kde.org/">Konsole</a> and <a href="http://kate.kde.org/">Kate</a> features, and much more.
<!--break-->
