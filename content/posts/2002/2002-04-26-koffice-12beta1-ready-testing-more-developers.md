---
title: "KOffice 1.2beta1 Ready for Testing, More Developers"
date:    2002-04-26
authors:
  - "Dre"
slug:    koffice-12beta1-ready-testing-more-developers
comments:
  - subject: "Mac OS-X "
    date: 2002-04-26
    body: "So KDE runs on virtually every platform except OS_X. QT runs on OS_X, why doest the KDE project work on porting to OS_X with X11.\n\n"
    author: "james brown"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "I always thought that people switched from *nix to OSX *because* they preferred the OSX gui... if you're going to use a unix front and a unix back end, why not just use unix?  Is there more demand for an OSX port?\n\n... back to work for me ..."
    author: "Matt Casey"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "macosx is just as unix as e.g. freebsd"
    author: "Q"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "so blackbox, gnome, enlightenment all run on OS_X (darwin with Xfree86 4.2) You can even run enlightenment in rootless mode on top of the OS_X gui (quartz layer). \n\n"
    author: "james brown"
  - subject: "Re: Mac OS-X "
    date: 2002-10-28
    body: "OS X IS Unix."
    author: "User"
  - subject: "Mac OS X "
    date: 2004-02-09
    body: "Mac OS X is not UNIX, it's BSD (BSD is like a GNU)\nThe BSD at one-stage became so popular that they called it BSD-UNIX/UNIX-BSD\nUNIX is a trademark of the \"the Open group\", I Think :)\nThey decide what can be called UNIX or not.\nFor e.g Solaris is UNIX Based. that's why it's not free!\n\nFrom what I Know...\n\nBut Apple/Darwin still Kool in my boox"
    author: "RedVenim"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "I don't claim to be an expert on this, but I think that you don't understand the question which you asked.\n\nXFree86 will run on Mac OS X but it replaces the Mac GUI.  \n\nThat is, it is running on the UNIX foundation -- Darwin.\n\nThen KDE should run on X11 on Darwin.\n\nBut, KDE isn't ever going to run on Mac OS X because it is going to replace the Mac GUI.\n\nCheck out the XFree and Darwin sites for more info.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "you can run X11 apps in a rootless WM side-by-side with native Apple apps in OS X. so while one might not use kwin or kdesktop, they may well want to use konqueror, koffice apps, kmail, etc..."
    author: "Aaron J. Seigo"
  - subject: "Re: Mac OS-X "
    date: 2003-01-21
    body: "Precisely,\nI for one would like to get away from MS Word (which is completely unreliable)\non my Mac.\nGiven XDarwin and OroborOSX, having KDE apps on OS X would be grand. \n(lsbcgk 6).\nHowever, I will experiment with the linux version first."
    author: "Luc Beaudoin"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "Feel free to do it :)\n"
    author: "Tim Jansen"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "QT may be crossplatform, but KDE libs isn't. It doesn't run on any platform except UNIX/X.\nBut I rather have a pure port to OS X. If KOffice was a pure QT application, it won't be enough. KOffice won't be able to follow dirrectly the Macintosh Human Interface Guide (e.g. toolbars should be outside the main window as oppose to inside the main window).\n\nLet's just keep this wonder called KOffice to ourselves, k?"
    author: "Rajanr"
  - subject: "Re: Mac OS-X "
    date: 2002-04-26
    body: "Actually, if you look at the Qt for OS X screenshots, you'll see that they made the toolbar go in the proper position. However I think most users could get over that - the idea of porting KDE to Darwin/X11 is pretty cool though. There's no GPL Qt for OS X which is a sticking point as far as a pure conversion goes."
    author: "Bryan Feeney"
  - subject: "Re: Mac OS-X "
    date: 2002-04-27
    body: "Uhm, all the screenshots of QT on OS X shows the toolbar positioned inside the window.\nBesides, just like what happen here, by porting KOffice to Qt of OS X, and ask discreetly for RMS to start a flamewar and get some of his sidekicks to start a replacement project for KOffice for OS X, which could cause into a GPL version of QT...."
    author: "Rajan Rishyakaran"
  - subject: "Re: Mac OS-X "
    date: 2002-04-27
    body: "Now I know, I was confusing it with the menubar. That's a really insane way of arranging things in my opinion. Though the Mac interface kinda gets to me at times, probably I'm just too used to the Windows/KDE way of doing things at this stage.\n\nThe point I meant to make though was that there's no need to convert KDE to Qt/Mac to get it working on OS X, just convert it to Darwin/X11. Qt/X11 _is_ GPL. There's a load of screenshots scattered across the net showing how well Darwin/X11 works, there's a big chunk of Gnome/GTK apps working on it already. Maybe Darwin can be made another target for KDE 4."
    author: "Bryan Feeney"
  - subject: "Re: Mac OS-X "
    date: 2002-04-27
    body: "There's a couple of us working on it over at the fink project (http://fink.sourceforge.net/).  There are some interesting porting issues that keep it from being a simple recompile, mostly from KDE making some assumptions about dynamic libraries that aren't true on OSX.\n\nThere is a libdl compatibility library that works around some of those issues, but there are still some other things needing fixing.  I've gotten everything up to kdebase to build (and I even got a konsole!) but there are some os-specific bits that still need to fixed (kdeinit being the one that's most annoying).  It'll happen, but it will still be another couple of months if not more."
    author: "Ranger Rick"
  - subject: "Re: Mac OS-X "
    date: 2002-04-27
    body: "Because OSuX has broken dynamic libs :P"
    author: "unanimity"
  - subject: "Re: Mac OS-X "
    date: 2002-05-02
    body: "Qt runs on Windows. Does KDE runs on Windows ?\n\nThe question is almost the same. Qt for MacOS X is a new platform and has nothing\nto do with Qt for UNIX (but the same API).\n\nQt UNIX will run on MacOS X with X11 installed. So you can probanly easily port\nKDE to MacOS X, running on top of that Qt that runs on top of X11. Much like a\nusual UNIX system. But that would not be worth much. Why not running directly\nLinux or *BSD on that hardware ? You'll have a snapier system."
    author: "Hubert Figuiere"
  - subject: "Re: Mac OS-X "
    date: 2004-02-09
    body: "Here's a Question: if Mac OS X is integrating so much with Linux then Won't they take a piece of Linux's user base?\n\nIf Novell or Sco win then won't Linux users have to move to BSD alternative such as Freebsd\n\nJust Maybe in my view Apple will Be a Good Platform to design both a KDE to run on Aqua and a KDE that can run from BSDcmd-line (text)\n\nopendarwin.org has Apple's Base with XFree86 (Not OSX) if You Build on that won't it be great starting point? for PPC and x86 as well"
    author: "RedVenim"
  - subject: "Re: Mac OS-X "
    date: 2004-02-10
    body: "> Here's a Question: if Mac OS X is integrating so much with Linux then Won't they take a piece of Linux's user base?\n\nI don't think so as long as they want over 2200 Euro for a PowerPC G5 when a comparable PC cost just a little over 1100 Euro."
    author: "Christian Loose"
  - subject: "*Great stuff ! * Well done KDE and KOffice"
    date: 2002-04-26
    body: "Excellent!  Can't wait to give the new KOffice a try.... :-) \n\nA big \"thanks!\" from down here in little ol' New Zealand ..... \n  "
    author: "Sagittarius"
  - subject: "Kontour and Karbon"
    date: 2002-04-26
    body: "For those of us who aren't on kde mailing lists, what it the difference between Kontour and Karbon?\n\nIs Karbon a komplete rewrite?\n\njust wondering."
    author: "ac"
  - subject: "Re: Kontour and Karbon"
    date: 2002-04-26
    body: "they are two seperate apps that have essentially only the koffice and kde libs in common. one is more Adobe-like, while one is more Corel-like. they may or may not merge in the future. for now they are being developed by seperate people and have different UI styles and capabilities."
    author: "Aaron J. Seigo"
  - subject: "Re: Kontour and Karbon"
    date: 2002-04-26
    body: "Hi,\n\nwe plan to setup a webpage to explain that better.\nShort answer: like in illustrator all shapes are just paths with all the\nresulting advantages:\nhttp://www.xs4all.nl/~rwlbuis/karbon/pics/whirl.png\nhttp://www.xs4all.nl/~rwlbuis/karbon/pics/insert_knots.png\nhttp://www.xs4all.nl/~rwlbuis/karbon/pics/intersect1.png\nhttp://www.xs4all.nl/~rwlbuis/karbon/pics/intersect2.png\nhttp://www.xs4all.nl/~rwlbuis/karbon/pics/polygonize2.png\n\nBye\nLenny"
    author: "Lenny"
  - subject: "Re: Kontour and Karbon"
    date: 2002-04-26
    body: "Now, this looks VERY promising! Especially the antialiasing and transparency support. \n\nHave you ever had a look at Xara X (www.xara.com), Lenny? It has unsurpassed usability and great transparency features, as well as fractal fills, \"fuzzy\" borders, and so on... It's the reason I keep booting back to windows sometimes ;-)\n\nI would love to see something comparable on Linux!"
    author: "Benno"
  - subject: "Re: Kontour and Karbon"
    date: 2002-04-26
    body: "No, unfortunately not. But i heard good things about it.\n\nWe hope to get some user input anyway, after we made karbon14 enduser usable. ;)\n\nBye\nLenny"
    author: "Lenny"
  - subject: "Re: Kontour and Karbon"
    date: 2002-04-27
    body: "Here my user input: place import svg support soon. SVG will be soon the most used vectorial format on unix, since gnome and kde will support it for icons/etc.\nBeisdes it's open and cool :)"
    author: "Iuri Fiedoruk"
  - subject: "SVG troubles..."
    date: 2002-04-28
    body: "\nYes, it is cool, but the problem is that we still don't have any decent graphic tool for creating, importing and exporting svg. "
    author: "antialias"
  - subject: "Re: SVG troubles..."
    date: 2002-04-29
    body: "Well, then it's time for someone to get famous by developing such a tool... ;->"
    author: "Benno"
  - subject: "Office filters"
    date: 2002-04-26
    body: "Hi\n\nI imagine it might be a recurrent question but I don't see any change in MS Office import/export filters (in the changelog).\nIs the development of such filters stopped ? Is it possible to re-use the 95% perfect filters already available in OpenOffice or are you obliged to write them from scratch ?\n\nNowadays, when using KDE in an office where other people are still on Windows, you have 2 options:\n\n - Become the anti-social geek of the company by refusing Word,Excel and PowerPoint documents... and maybe get fired.\n\nOr\n\n - Wait 30 seconds for OpenOffice to open then either edit it directly from OO or export it to a real file format, modify it in kword, import it again to OO and save it back to Word/Excel/PowerPoint...\n\nSo, that would be GREAT if you could simply import/export from/to MS Office formats.\n\nOne on the reasons I hear most often to explain the fact that koffice doesn't use OO's filters is that OO uses raw X11 to display widgets while Koffice uses QT widgets. Isn't it possible to create a command-line script to convert MS Office files to Koffice format and vice-versa ?\n\nNot a troll, just an incomprehension of the problem developpers have to face for us, poor users, to get great MS Office import/export filters.\n\nAnyway, I really admire you for doing such a great job and, as somebody already said, I would vote for David Faure as french president if he had dared to present himself at french presidentials :)"
    author: "Julien Olivier"
  - subject: "Re: Office filters"
    date: 2002-04-26
    body: "As posted on /., Bart Decrem (the Eazel guy) had the idea to create an \"MS office middleware\", which converts office documents into XML [ http://www.linuxandmain.com/features/decrem.html ]. IMHO, this would be a great idea, because not every office development crew had to spend zillion of hours on reenginering and reinventing the wheel...\n\nBTW: A BIG thankyou to the koffice-developers. I cannot believe what you have done..! Great !"
    author: "Skaldrom"
  - subject: "Re: Office filters"
    date: 2002-04-26
    body: "Oh that's a great news provided that it's GPL (or LGPL) and that it uses what has been already done for OpenOffice."
    author: "Julien Olivier"
  - subject: "Re: Office filters"
    date: 2002-04-26
    body: "Take a look at this lib http://www.wvWare.com/\nAbiword seems to use it."
    author: "aThom"
  - subject: "Re: Office filters"
    date: 2002-05-04
    body: "Great if there is one universal standaard-format being developed for all office programs. Everebody will benefit from that (except Microsoft, but who cares....)."
    author: "Maarten Rommerts"
  - subject: "Re: Office filters"
    date: 2002-04-28
    body: "Are there any plans for Wordperfect 5.1 and above format conversion?  I have several clients who are dying to move to Linux but neither KOffice nor OpenOffice can open a WordPerfect file!  What's up with that?"
    author: "tzanger"
  - subject: "Re: Office filters"
    date: 2002-05-06
    body: "use Word Perfect to export the files to word files before backing them up(i would also back up the org. files too). Then switch to Linux and open them up with Kword or Koffice. You might need to keep one Word Perfect/ windows machine to handle bad exports. You use this machine by copy and paste the org. document to an email and emailing it to the linux machine. it is a pain, but it fixes that problem.\n\n "
    author: "Stuart Williams"
  - subject: "Re: Office filters"
    date: 2002-07-04
    body: "Yup, and what if you by chance found some old WP<unknown version>\ndocument and would like to open it/convert it now?\nSee, thats the problem I have right now :-/\n\nAny good suggestions on this, anyone??\n\nBest regards,\n  Kim"
    author: "Kim Poulsen"
  - subject: "Re: Office filters"
    date: 2002-07-05
    body: "KWord CVS, and also the one shipped with 1.2, can import WordPerfect faily well, be it 5.x or 6/7/8. However, only formatted text is supported at the moment (no graphics etc). I plan to improve it for 1.3, as well as adding WordPerfect export filter, so someday you'll be able to save KWord doc as WordPerfect doc."
    author: "ariya"
  - subject: "will scripting be less obscure ?"
    date: 2002-04-26
    body: "so far, I have yets to find any examples of how to use\nkoscript. \n\nAlthough I have used DCOP from the command line I find\nscripting in KOffice is pretty obscure.\n\nI'm interested in adding functions to kspread without\nwriting C++\n\n"
    author: "Phil"
  - subject: "Re: will scripting be less obscure ?"
    date: 2002-04-26
    body: "What about adding the functions you want to write to KSpread (in C++) so the developer can focus on writing a script editor and things like that (which is on the todo list), so that you will be able to write functions directly.\n\nJust compare with Gnumeric, Excel or StarCalc und you'll see what is missing.\n\nThis would be a great help! write to the koffice@kde.org mailing list or to the KSpread developers if you need help or if you have results to add to the KSpread source.\n\nRegards\nNorbert"
    author: "Norbert"
  - subject: "mistakes in announcement"
    date: 2002-04-26
    body: "I saw small mistakes in the announcement. (KPresenter section)\n\n\"Page views. A new thumbnail view of each page in the presentation is now displayed in the sidebar, and a larger view of the current page is displayed _____.\"\n\n_____?\n\n\"Note bar. A new notebar allows you to add comments to each page of a presentation.\"\n\nIs it \"note bar\" or \"notebar\"?"
    author: "Andy Goossens"
  - subject: "Footnotes?"
    date: 2002-04-26
    body: "Hi,\nCan anybody tell me if footnotes/endnotes have been implemented \nin KOffice 1.2beta? I can really use it without this. \nDont have KDE3.0 yet, so I cant check it out right now.\n\nregards,\nMarc"
    author: "Marc"
  - subject: "Re: Footnotes?"
    date: 2002-04-26
    body: "No, they are not yet implemented. Also missing is hyphenation.\n\nBoth omissions make kword unusable for longer scientific texts. I hope something will happen in this area, even though meanwhile OO is a good replacement."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Footnotes?"
    date: 2002-04-26
    body: "Hmm, seems like Abiword doesnt have it either. Is it so hard to code?\n\nregards,\nMarc"
    author: "Marc"
  - subject: "Re: Footnotes?"
    date: 2002-04-29
    body: "Footnotes are not very hard to do - although they do create some layout problems, in some worse cases, like: there would be room for the footnote text, but then the actual footnote number (what's the proper name?) would\nbe at the next page -> bug. Or if you move the text containing the footnote\nback to the first page, there's no room for the footnote on that page.\n(The only solution in such a case is to move both to the next page, I guess,\nbut I wonder how we'll be able to detect all that stuff ;)\n\n\nHyphenation, on the other hand, is a much more difficult matter. No clue how\nto address that one - and no \"just take the stuff from LaTeX\" isn't of any\nhelp, but actually extracting the relevant part, making it independent from the rest, and sending to me, would be very helpful ;)\nI was also pointed to http://www.dcs.shef.ac.uk/research/ilash/Moby/,\ndidn't have any time to look into this. Did I mention that KOffice severely\nlacked developers still? ;)"
    author: "David Faure"
  - subject: "Re: Footnotes?"
    date: 2002-04-26
    body: ">Both omissions make kword unusable for longer scientific texts. I hope something will happen in this area, even though meanwhile OO is a good replacement.\n\nPersonally I find Word unusable for any scientific texts also. If you want really good professional looking text/hyphenation/equations the only way to go is TeX/LaTeX. I can't imagine having tried to write my PhD thesis with Word!\n"
    author: "Dr_LHA"
  - subject: "Re: Footnotes?"
    date: 2002-04-26
    body: "Also, LyX (http://www.lyx.org) is a good choice -- it is\nan easy-to-use scientific word-processor that is a TeX frontend.\n\n"
    author: "A Sad Person"
  - subject: "Re: Footnotes?"
    date: 2002-04-27
    body: "Does it really matter? Phd thesis is a very limited market."
    author: "reihal"
  - subject: "Re: Footnotes?"
    date: 2002-04-27
    body: "But it might make a lot of colleges switch to Linux. I know mine has dual boot machines, and all the lecturers pretty much ordered us to use Linux from first year on. Imagine the effect of a a year's Comp. Sci. graduates hitting the workplaces of the world having been brough up on KOffice."
    author: "Bryan Feeney"
  - subject: "Re: Footnotes?"
    date: 2002-05-03
    body: "It's not only a Phd thesis where this stuff is needed. It's simply impossible to \"sell\" a word processor for professional use in eg Germany, Russia, Italy, and many other countries if this wp is not capable of proper hyphenation. This is not a problem for languages like English, of course. Depends very much on the average word length, use of compounds etc.\n\nFootnotes, endnotes, or automatic indices may not be as important in many environments. But there are still enough of them where this stuff is critical as well. Just think of any sort of scientific papers, book publishing, lawyers and so on.\n\nThomas\n\n"
    author: "thd"
  - subject: "Re: Footnotes?"
    date: 2002-04-26
    body: "Maybe I've missed something but... you can create a frame at the bottom of a page and configure it so that it copies itself on each page. Isn't it what you call footnotes ?"
    author: "Julien Olivier"
  - subject: "Re: Footnotes?"
    date: 2002-04-26
    body: "No, imagine you have 60 numerated Endnotes/References and you want to add \none at position 34. If you dont have an endnote function, you have to rename all \nthe ones which have shifted.\n"
    author: "Marc"
  - subject: "Re: Footnotes?"
    date: 2002-04-27
    body: "Shouldn't the page number function (found under insert->variable->page) be enough for this?"
    author: "harv"
  - subject: "Re: Footnotes?"
    date: 2002-04-27
    body: "Well, you really have never ever in your life seen a footnote? Have you ever read a book?\n\nA footnote, for instance, can put a superscript number in a word, and that number is a reference to a note, at the foot of the page.\n\nNo, page numbers are not good, because you can have many footnotes in a single page, or no footnote at all."
    author: "AC"
  - subject: "Re: Footnotes?"
    date: 2002-04-30
    body: "This is what I call footnotes, and (for me) this is OK, however:\nif I insert some text into the beginning of the document, the footnote must change page, with the point that it was entered.\n\nAlso this does not address the issue of end-notes, and references (which, if made configurable enough amount to the same thing)\n\nIs someone working on making something like framemakers cross-reference? (will solve the problem of end-notes)"
    author: "Will"
  - subject: "Word is dead and Adobe, watch Out!!!"
    date: 2002-04-26
    body: "I believe everyone will agree with me when I say Word will go the way of the dinosaur.  We really need to get KOffice up and running with QT for Windows.  People are afraid of switching because they don't know if they will be abl to do their work.  I'll admit, it was a concern of mine as a student.  I like the idea of making one universal translation for M$ files.  We should definitely work together.\n\nWith the interest in graphics, I think Adobe will have to take us seriously too.  We may really start to give them a run for their money.\n\nWhy aren't the rpms offered by application?  I think providing the base compnents with kword or kchart, and then letting people download the other applications they want would be nice for people who don't have fast connections."
    author: "droobiedoobiedoo"
  - subject: "Re: Word is dead and Adobe, watch Out!!!"
    date: 2002-04-26
    body: ">Why aren't the rpms offered by application?\n\nKDE provides source code. KDE does not provide packages or binaries. Packagers do that. Thus, how RPMs and DEBs are created is not something that is a responsibility of KDE, so go talk to the packagers for the various distributions. Also, the Debian packages are offered app by app in this fashion."
    author: "Carbon"
  - subject: "Re: Word is dead and Adobe, watch Out!!!"
    date: 2002-04-27
    body: "I was told Conectiva offers applications packaged into single RPMs.\n\nAs a Linux user you are free to choose whatever distribution you like.\n\nSo if you think you want small packages and your distributor doesn't want to offer them, switch.\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: Word is dead and Adobe, watch Out!!!"
    date: 2002-04-26
    body: "<em>Why aren't the rpms offered by application?</em>\n\nBecause not every one uses RPM, let alone Linux. RPMs for a Linux binary of a KDE application would be as useless to me as tits on a boar. They won't work for me on my Solaris box and they won't work for me on my FreeBSD box. I've got RPM ability on my Slackware partition, but unless that RPM was made for Slackware, it will have dependencies on packages not named in the Slackware style. Packages are meant for specific distributions, and KOffice is not limited to a specific distribution. If you want RPMs, talk to your distributor. Or use the source code, the ultimate in portable packaging.\n"
    author: "David Johnson"
  - subject: "Re: Word is dead and Adobe, watch Out!!!"
    date: 2002-04-27
    body: "KOffice for Windows (or Mac, as an earlier poster mentioned) would be neat, but there are two problems with such an effort, one technical and one legal.\n\nFirst, kdelibs was written to work on unix.  I think some people have managed to compile kdelibs on Windows by using cygwin and Qt/X11, but this is probably only because cygwin supports all of the unixness needed by kdelibs.  Compiling kdelibs with Visual C++ using Qt/Windows may not be possible at all.\n\nSecond, KOffice is GPL and there is no open source distribution of Qt/Windows.  This means that the licenses are incompatible and it is not really legal for someone to distribute a resulting build between these two.  The KDE team ought to change their license to allow linking to Qt, commercial or not, so that the possibility of a KOffice on Windows or OSX is at least legal.\n\n-Justin"
    author: "Justin"
  - subject: "GPL version of Qt is available for windows"
    date: 2002-05-14
    body: "A GPL version of QT for windows has been available for a while\n\nhttp://www.trolltech.com/products/qt/windows.html\nhttp://www.trolltech.com/developer/download/qt-win-noncomm.html"
    author: "Troll"
  - subject: "Zip vs tar and gzip"
    date: 2002-04-27
    body: "Will someone please enlighten me and the rest of us as to why a change in\nfile format from tar and gzip to Zip is necessary?\nWhat exactly breaks without this change?\nHow easy is it for a user of KOffice 1.1 to read 1.2 files?"
    author: "Paul Leopardi"
  - subject: "Re: Zip vs tar and gzip"
    date: 2002-04-27
    body: "I believe it is a part of the move of the open office suites (OO, Abi, Koffice\nand some others) towards a common file format. A lot of discussion seems to have gone into deciding stuff like that recently."
    author: "cosmo"
  - subject: "Re: Zip vs tar and gzip"
    date: 2002-04-27
    body: "tar-gzip only allows sequential access. If you want to extract a file from a tgz-file that contains 10 files, you have to process the whole file. Zip on the other hand allows more or less random access, if you want to extract a single file you can look it up in the index and just pull out that one file.\n\nThat gives more flexibility to process such files.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Zip vs tar and gzip"
    date: 2002-04-27
    body: "With tar you have to know the size of a file before you can write it into the tar archive.\nSo you have to write the document into a buffer to see how large it will get and then you have to write it into the archive as a whole.\n\nWith zip you can just start writing and finish whenever you're at the end of your dokument,\n\nKevin"
    author: "Kevin Krammer"
  - subject: "Koffice-viewers for windows."
    date: 2002-05-04
    body: "Isn\u00b4t it a good idea to make Windows-viewers for popular Koffice-programs like Kword, Kspread and Kpresenter? the problem is that when I want to use Koffice for proffesional work other people have to be able to open the documents made with this stuff. \n\nUnfortunatly most people are still working with MS-Windows and can\u00b4t open Koffice-files. Remeber alo that MS-Office may have come so populair because of the fee viewers availeble from Microsoft.\n\nIf there is already some development is going on on this point I really like to know. I think it could boost up the succes of Koffice and the entire open-soure.\n\nThanks anyway for the nice office-suite so far!"
    author: "Maarten Rommerts"
  - subject: "Re: Koffice-viewers for windows."
    date: 2003-06-01
    body: "I think that's a superb idea. It will make viewing my kword files in windows much easier (I keep forgetting to export to abiword format) and would mean not losing any formatting.\n\nIMO Kword is THE Killer Linux app (OoO is ok but KWOrd is much better and more fun to use)\n\nAnything that would enable it to be more widely used would fantastic"
    author: "netean"
  - subject: "Re: Koffice-viewers for windows."
    date: 2003-06-02
    body: "To certain extent, you can always export your document to RTF (Rich-text) which is of course readable by MS Word.\n"
    author: "Ariya"
---
The <a href="http://www.kde.org/">KDE Project</a> today
<a href="http://www.koffice.org/announcements/announce-1.2-beta1.phtml">announced</a> the release of <a href="http://www.koffice.org/">KOffice</a> 1.2beta1.
While the "final" 1.2 release is not scheduled for another 5 months, this
is a great chance to see what the active KOffice developers are up to,
and also a great time for new developers / companies to join the KOffice
project to accelerate the ascendence of KOffice into the
market-leading office suite position &lt;grin&gt;.
The highlights of this release are WYSIWYG in KWord, KPresenter and formula objects,
much enhanced scriptability via DCOP, and a number of new and enhanced
filters, including an XSLT framework for mapping between different
XML office formats.  Read the announcement for the many details,
and feel free to <a href="http://www.kdeleague.org/contribute.php?to=KOffice">give
thanks</a> to those awesome
<a href="http://www.koffice.org/developers.phtml">KOffice developers</a>!






<!--break-->
