---
title: "Bart Decrem re: Desktop Elegance"
date:    2002-05-07
authors:
  - "bdecrem"
slug:    bart-decrem-re-desktop-elegance
comments:
  - subject: "Thanks, Bart"
    date: 2002-05-07
    body: "That's downright decent of you!\n\nHave you found a better icon theme to suit your taste?  On the positive side, I believe the KDE Artists will be working on making the default application icons more photo-realistic for KDE 3.1.  At least your comments will give them more incentive to follow through!\n\nCheers.\n"
    author: "Navindra Umanee"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-07
    body: "Oh, nevermind, I just saw <A href=\"http://www.decrem.com/my_kde_desktop.jpg\">your screenshot</a>.  Cool!"
    author: "Navindra Umanee"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-07
    body: "He must be killing !\nHis desktop is so ugly.\nI'am fed up with this OsX style and panel,please be more creative ! \nThink of E themes or even slick icons which are more original.\nKDE is not and will never be OsX or XP, assume its difference. \nThanks..."
    author: "aThom"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-07
    body: "Oh, for the love of God.  EVERYONE assumes that the little panel centered in the middle of the screen at the bottom is a play on MacOS Xs panel.  But those people would be and are ignorant about the fact that the CDE and FVWM (a likeness of CDE) has their panel in the center of the bottom of the screen, nad has for a hell of a lot longer than OS X has been around.  Get a clue."
    author: "Etriaph"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-07
    body: "And thou it looks nice it's seems extremely impractical, 2 apps in the task bar and it's full. CDE came out before the taskbar (back in the Alt-Tab days). How does OSX handle it?"
    author: "Fredrik C"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-07
    body: "Now THAT'S ugly\nThe text beneath the sunflower icon on the desktop is unreadable\nThe W icon on the panel...no comment\nGnome terminal icon...\nAnti Aliasing turned off\n\nblah :/\n\n"
    author: "fler"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-09
    body: "Hope the icons don't get too photo realistic. Too much detail in icons make them\nharder too read. E.g. a trash bin shouldn't to look exactly like a real\nlife trach bin but rather like our mental concept of a general trash bin.\nIf they have too great detail they may clash with that mental concept and we\nhave to spend time analyzing the icon image, before we realize that is a\ntrash bin."
    author: "Uno Engborg"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-14
    body: "I fully agree with you.. however i think that all the bright (impress-the-geek-chicks) collors in the more photorealistic are kewl too. So i guess its up to the art-gods to find the right balance in this area :) Btw. the Home icon is kewl IMHO :D\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-14
    body: "I fully agree with you.. however i think that all the bright (impress-the-geek-chicks) collors in the more photorealistic are kewl too. So i guess its up to the art-gods to find the right balance in this area :) Btw. the Home icon is kewl IMHO :D\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: Thanks, Bart"
    date: 2002-05-14
    body: "Sorry for the 2xBummer(tm).\n\n/kidcat\n\n(Is it hard to intercept a dup page and > /dev/null it? Not that I would ask anyone to spend the time on my behalf when I'm the faulty-clicking one... but others make them as well, when net is laggy/jumpy/slow)"
    author: "kidcat"
  - subject: "Validation ?"
    date: 2002-05-10
    body: "\nhttp://dot.kde.org \n\nValidation results:\n\n'Warning: No Character Encoding detected! To assure correct validation, processing, and display, it is important that the character encoding is properly labeled.'\n\n'Sorry, this document does not validate as HTML 4.01 Transitional.'\n\nIs it possible to fix this?\n\nCheers,\n\nantialias"
    author: "antialias"
  - subject: "Nice reply"
    date: 2002-05-07
    body: "I think it was a nice reply, and probably something that wasn't too easy to do. Usually when you get flamed a lot you tend to get defensive and hold on to your opinion. Apparently Bart can admit that little slip and get on with it. :)"
    author: "Chakie"
  - subject: "Now..."
    date: 2002-05-07
    body: "His desktop now looks really butt ugly. The \"W\" icon is so pixelized. Slap on an QNX theme, and a iKons theme, it would be so much nicer, :-)"
    author: "Rajan R."
  - subject: "Re: Now..."
    date: 2002-05-07
    body: "dude, I AGREE!  I've asked our design team to come up with new app launcher icons.  Also, I'm trying to download RH7.3 so then I can do a brand new install of KDE3 and play with more themes.\n\nbd"
    author: "Bart Decrem"
  - subject: "Re: Now..."
    date: 2002-05-07
    body: "you can also install themes from kde-look.org. "
    author: "Pablo Liska"
  - subject: "Re: Now..."
    date: 2002-05-08
    body: "Yeah right.\n\nCall me a cynic, but it looks like you said it deliberately to get some\nfree advertising and it fell flat.\n\nAs flat as your attempts to regain face are now.\n\nLooks \"Pretty transparent\" to me (which ironically is a good instruction for your \"team\" of designers)"
    author: "Michael"
  - subject: "Re: Now..."
    date: 2002-05-08
    body: "Give me a break dude - I'm just saying \"I'm sorry\"."
    author: "Bart Decrem"
  - subject: "Re: Now..."
    date: 2002-05-08
    body: "Michael,\n\nEver gave any thoughts about things like, \"growing up\", and \"getting a life\" ?\n"
    author: "Guillaume Laurent"
  - subject: "Re: Now..."
    date: 2002-05-09
    body: "More than you did making that reply.\n\nIf you going to have a pop, put some decent effort into it.\n\nThe age-ist remarks are odd, perhaps you were bullied as a child by other children?"
    author: "Michael"
  - subject: "Re: Now..."
    date: 2002-05-08
    body: "geez, someone seems to be too pissed off, to accept a simple apology ..."
    author: "Carsten"
  - subject: "Re: Now..."
    date: 2002-05-09
    body: "Nope, I don't have any axe to grind.\n\nI didn't write kde or its icons and I'm not particulary bothered whether it's ugly or not.\n\nI can see through PR stunts though, if the rest of you can't."
    author: "Michael"
  - subject: "Re: Now..."
    date: 2002-05-10
    body: "And betray your company's Hancom Linux?\nWait, you don't have to use RedHat to use KDE 3.0. You could use QNX For KDE, get iKons, change that \"W\" icons, and practice consitency between icons. You can compile GNX For KDE 3 yourself... (don't tell me, you can't compile!)"
    author: "Rajan R."
  - subject: "hilarious writeup"
    date: 2002-05-07
    body: "This is a downright hilarious way to 'apolgise', especially the reflection on his own appearance. I love it! It's the first thing that made me laugh since yesterday sixish."
    author: "Matthijs Sypkens Smit"
  - subject: "Ugly desktop ;-)"
    date: 2002-05-07
    body: "If you want to avoid an ugly desktop you have to take care that GTK theme (see your Gimp) matches the rest of KDE desktop. Icons on your Kicker are too muxh mixed: everaldos Crystal theme, Mozilla's redstar which is scaled to 48x48 (missing size), Gnome terminal icon doesn't go well with the rest of icons and that 'W' icon (probably Hancom Word) which is not created in 48x48 size is destroyed by scaling. Advice: no matter which icon theme you use, keep your icons consistent.\n\nAnd I am curious: you are talking about a new icon theme for KDE and you say it will be beautiful. How come that we've never seen a preview of it on kde-artists mailing list? Or maybe you know something that I don't know.\n\nCheers,\n\nantialias"
    author: "antialias"
  - subject: "Re: Ugly desktop ;-)"
    date: 2002-05-08
    body: "Thanks for all those comments. I've cleaned up some of those things already.  \n\nRe: \"icon theme\" I was referring to Keramik, which I've been told will be the new default look.  I haven't been able to compile Keramik but the screenshots I've seen look terrific (http://www.kde-look.org/content/preview.php?file=1172-1.png). I guess Keramik doesn't come with an icon theme though, but the screenshot at shttp://www.kde-look.org/content/preview.php?file=1439-3.png of a matching background, kicker background, and I guess that's the Conectiva icons, combined with Keramik will be really beautiful, in my opinion."
    author: "Bart Decrem"
  - subject: "Re: Ugly desktop ;-)"
    date: 2002-05-08
    body: "Too me the crystall icons really suck, the only ones worse are Mandrake's! How they can produce such nice looking install, boot process and apps and then stick all those ugly icons all over the place is beyond me. I've found that going with an aqua theme in linux is about the only way to have a consistent user interface, because everyone has copied it. Much to apple's chagrin. Eric Yang produced some the most beautiful, flattering, knock offs of aqua I have ever seen. He created an enlightenment theme with a dockbar in the center that looks amazingly like a screenshot of the new imac on apple's site. Add all the various xmms, gkrellm etc. skins, kde aqua theme, and GTk aqua, and you can create one magnificient illusion. Until you fire up mozilla :("
    author: "kbeaumont"
  - subject: "Re: Ugly desktop ;-)"
    date: 2002-09-21
    body: "Google for \"Pinstripe linux\"."
    author: "Lorn"
  - subject: "Re: Ugly desktop ;-)"
    date: 2003-11-11
    body: "Though even mozilla can now look aqua-like if you use the firebird browser project."
    author: "Adrian"
  - subject: "Re: Ugly desktop ;-)"
    date: 2002-05-09
    body: "FWIW, it appears those icons are actually the default Hi-Color icons at really small sizes with alpha-blending enabled.\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Ugly desktop ;-)"
    date: 2002-05-09
    body: "but that looks fucking hideous.\n\nBut its all personal preference, so why is it being made a big deal about?"
    author: "sorry"
  - subject: "Re: Ugly desktop ;-)"
    date: 2003-05-09
    body: "Hi there. I am a newbie to linux. \nI have just installed it and downloaded the aqua theme from kde's site.\nBut it doesnt look like imac much cause the title bar has been changed to look different from osX. Can someone mail me the theme please.\nMy email address is -\ngladiator34@rediffmail.com\n"
    author: "gladiator"
  - subject: "Re: Ugly desktop ;-)"
    date: 2004-05-01
    body: "i am really looking for a ugly desktop,i mean ugly with a head cutt off or a big sign that says \"fuck my parents\" ,if anyone has or knows a place where i can get them ,thanks in advance ,oh by the way i am really depressed now.."
    author: "julia"
  - subject: "Re: Ugly desktop ;-)"
    date: 2006-05-06
    body: "hi, i'm a newbie to linux as well. if anyone has a real nice aqua theme.. maybe by eric yang?? :-) please do mail t to me at jimi_schmendrick@yahoo.com"
    author: "shailesh lal"
  - subject: "Brilliant marketing strategy"
    date: 2002-05-07
    body: "Ho can this guy have such a senior position in a leading linux company? \nLook at this gem he posted on slashdot (in the thread on his interview):\n\n>there are fashions in product naming, and if you violate those fashions, names >sound kind of odd. There's a reason people make good money worrying about >these issues.\n>For instance, during the dot-com phase, everyone put \".com\" in their company >name, but by now, everyone's removed that from their company name. Also, a few >years ago, it was very popular to make compound words with capitals in the >middle (HancomLinux) - but now that's not so popular anymore.\n>Similarly, single letters go through periods where they are hot and not. So a >few years ago, everyone loved using the letter Q in company names (Quantum >etc.). But that's really old now. When Eazel picked Z that was a decent >marketing decision (in addition to the fact that the Easel.com domain was not >available). In my personal opinion, the overuse of the letter K in all things >related to the KDE project gets old very quick and is not a huge asset. But >I'm sure KDE users feel the same about Galeon, Gnumeric and all the other G->words that are connected with GNOME. I just think the letter G is overall more >elegant - it sounds smooth and looks round, whereas the letter K is so, well, >square. Also, once I heard that KDE originally stood for Kool Desktop >Environment I could never quite get that thought of my head - and that's kind >of a traumatic thought:) (I fully appreciate that GNU Network MOdel >Environment is quite a mouthful). There - for what that's worth :)\n\nRecapitulating: this guy is paid a hefty amount of money to market a product, and it turns out that his marketing strategy (judging from the above post) is that of follwing the latest stupid fashion, with the result of completely hiding his product in the middle of other, similar-sounding names. \nGood. Now, to me all this sounds pretty stupid. It is quite obvious (to me at least) that the reason why Linux was the media darling until recently is precisely because of the funny name, the penguin, the story \"finnish student decides to write an OS\" etc. Can you imagine all those articles about Linux (and Linus) being written about the usual suit, a la Bart Decrem? \"We sell productivity solutions\". Really? How exciting! If i would ever need some marketing for any of my projects i would spend my money on ESR's advice instead - the way he imposed the term \"opensource\" (and himself), in the linux world -- well, THAT is a true marketing masterpiece! \n\nLinux companies need better leadership and better managers.\n"
    author: "Federico Damonte"
  - subject: "Re: Brilliant marketing strategy"
    date: 2002-05-07
    body: "Don't think 'following the latest fashion'.  It's more about -not ignoring- the latest fashion.  \n\nYou need to look at current fashions to see what will seem like a good product name to most people (that this matters is sad, I know, but unfortunately, for the majority of people, i.e. those unaware of the issues about naming, it will).  I believe the point he tries to make is that you must pay attention to what names are common and are perceived by average 'Joe User' to be that of an up-to-date product (i.e. calling a product Wibble/386 1.0 will make it sound rather archaic, calling it Wibble 98, Wibble 2000 or even Wibble 3001 will seem to be the name of a product of yesteryear to your average M$ user).  \n\nIf you put your knowledge about the issues and how much/little a name/brand really counts, you should be able to look at the impressions that names generate.  \n\nFor a simple exercise, take a list of current and not-so-current software package names and versions, and replace the indentifying bits of the names with gibberish (e.g. change Windows 2000 to MyWibble 2000, Linux kernel 2.1.27ac34 to FreshMash 2.1.27ac34 etc.) and then look down the list at different names, giving yourself about 1/2 a second to decide how up-to-date the product sounds."
    author: "John Allsup"
  - subject: "LOL"
    date: 2002-05-07
    body: "Pretty funny - thanks for making me laugh :)"
    author: "Joergen Ramskov"
  - subject: "What's the big hoopla about anyhow?"
    date: 2002-05-07
    body: "Well first off, I'd like to say I'm really pleasantly surprised by what a good sport Bart is being about all this. I mean, the fella never said anything bad about KDE except that it didn't fit into his aesthetic taste, and everybody goes batsh*t. \n\nSeriously -- do we use KDE because it's good looking or because it's a solid and well designed environment? I, personally, came over from BeOS a couple years ago and frankly neither GNOME or KDE look or feel anywhere near as good as BeOS did. I chose KDE quickly, after attempting to port my code to gtk; frankly I found the api a bit crude, whereas QT/KDE was much more in the simple OO style of BeOS (for reference, I was able to port my BeOS programs to Qt quite quickly). I feel uncomfortable using a desktop which I can't enjoy writing code for, so the problem resolved itself.\n\nLuckily for me, KDE looks great these days -- in fact, I don't even have the desire to port my GONX style from KDE 2 to 3 since I think light3 is fantastic (though if anybody wants GONX for kde3 I might give it a go. As far as I can tell everybody these days goes banannas over liquid and krystal and keramic, which I find distracting, childlike, and ugly, but hell: that's my opinion, and not a statement of fact). Nonetheless, it's irrelevant. Even if KDE still looked like it did when I started using it and writing code for it back in the 1.x days, I'd still love it, because it's solidly designed, well documented, and in my experience completely stable.\n\nAnyway, I'm glad to see that this preposterous tumult has died down, but I have to say I'm saddened that it took this level of self-deprecation from Bart to make it happen. At least his \"press release\" is in good humor, and it seems like there are no hard feelings. Kudos to Bart! \n\nBut shame, on us."
    author: "Shamyl Zakariya"
  - subject: "Creativity in diversity "
    date: 2002-05-07
    body: "\n\n----<< As far as I can tell everybody these days goes banannas over liquid and krystal and keramic, which I find distracting, childlike, and ugly, but hell: that's my opinion >>----\n\nI think its great that people have such diverse tastes.  Beauty is in the eye of the beholder, and KDE can cater for most tastes.  I personally think Keramic Accessories ( http://apps.kde.com/uk/0/info/vid/6309 ) is stunning.  Visually, KDE is going from strength to strength.\n\nWhere KDE still needs work IMO is with consistency across the desktop.  Even with KDE 3.0 I find that things are not always where I logically expect to find them. I repeatedly trip over the absence of RMB cut/copy/paste for Konsole - because I habitually use it in so many other places in KDE, and GNOME, and MS Windows (which I unfortunately have to use too).\n\nOr there are menu options I might use once in a lifetime that get in the way of other options I use frequently,  The RMB menu in Konqueror for example is starting to get way too crowded.  Applications (like cervisia and pixie)  should park themselves under \"Open with...\" not directly on the first menu where they are in the way of the Properties option.  \n\n"
    author: "John"
  - subject: "Re: Creativity in diversity "
    date: 2002-05-07
    body: "Which makes me wonder -- how do I remove Cervisa from the konq toolbar (not RMB)? I have no doubt it's nice & useful for those who do lots of cvs work, but I don't. In fact, the few times I *have* I did it from the command line because I was too uncertain of the mechanisms to feel comfortable letting it be automated.\n\nSo, what do I do to kill/hide it? "
    author: "Shamyl Zakariya"
  - subject: "Re: What's the big hoopla about anyhow?"
    date: 2003-04-02
    body: "You know, I loved your GONX theme, being a former BeOS user. Love to see it for 3.x."
    author: "Gabe"
  - subject: "Oh God, smileys from Kopete"
    date: 2002-05-07
    body: "This sucks. <img src=\"http://static.kdenews.org/dot.kde.org/EmoteImages/mad.gif\"></img> First I cannot disable those smileys I hate from Kopete, now I have to see them on here as well. Damnit. Can't we just happily use <img src=\"http://static.kdenews.org/dot.kde.org/EmoteImages/smile.gif\"></img> and <img src=\"http://static.kdenews.org/dot.kde.org/EmoteImages/happy.gif\"></img> and keep some respect for the history of the Internet?\n<p>\nIt hurts my eyes. Please let me disable it in my user preferences. And kopete-hackers, damnit, grant me my wish and include an option to disable these headache inducers.\n<p>\n<img src=\"http://static.kdenews.org/dot.kde.org/EmoteImages/laugh.gif\"></img>\n<p>\nSee, much nicer. <img src=\"http://static.kdenews.org/dot.kde.org/EmoteImages/smokin.gif\"></img>"
    author: "Rob Kaper"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-07
    body: "Heh.. good thing I was too lazy to implement them optionally in Kit."
    author: "Neil Stevens"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-07
    body: "Maybe they're just trying to encourage serious talk by threatening you with these damned thing.\n\nI know I'll never use an emoticon in here again!"
    author: "Charles Samuels"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-07
    body: "LOL!"
    author: "ac"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-07
    body: "Could thing LOL and ROTFL aren't replaced with illustrations automatically, isn't it? \n\n"
    author: "Sad Eagle"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-07
    body: "s/could/good. Having the preview button jump over the post button before posting would seem like a good feature, on other hand. "
    author: "Sad Eagle"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-12
    body: "what would RTFM be then?"
    author: "Sam Halliday"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-08
    body: "If these are being auto-generated from the post, consider useing the original chars as the alt."
    author: "a.c."
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-11
    body: "No auto-generation -- I was just messing with Rob.  :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2002-05-15
    body: "I feel so special now!"
    author: "Rob Kaper"
  - subject: "Re: Oh God, smileys from Kopete"
    date: 2003-03-15
    body: "LOSer"
    author: "qwe"
  - subject: "lol"
    date: 2002-05-08
    body: "well, nice reply Bart, I think most people who did not like the comments are happy now. As for me, I don't see see why his comments created a contraversy so much. KDE (at that time), was just not in his taste of style. Of course, instead of saying that KDE was basically hideous, he could have said it wasn't in his tastes.\n\nEveryone has their taste of style. While I really love KDE - the desktop and project, I tend to use fluxbox more because KDE is just too cluttered imho to use. Realise that it's imho before you flame :) "
    author: "fault"
  - subject: "m.shelby@verizon.net"
    date: 2002-05-08
    body: "I am so glad that Bart made those 'slips' in his article/interview! It illustrates almost \nperfectly why  I choose to use Linux/KDE.... \n\n-He had the timerity to give HIS opinion. Choice is what it's all about, people! \n-He had the platform/outlet to deliver his opinion in a much read and followed publication. \n(this shows interest in the community!)\n-He got a whole lot  (grin) of useful feedback FROM the community. This shows enthusiasm for the KDE project.\n-He had the maturity and composure to re-evaluate his remarks and attempt to explain them. \n\n\nSo he's a *geek.* So he made a hurried off-the cuff statement. \nSo what...\n\nSo we all are a little protective of KDE. Let's not be like the folks in Redmond who try to\nshove *whateverthehellwewantto* down our throats all the time. Lighten up! Just because\nwe have a disagreement doesn't me we \"take our ball and go home.....!\"\n\nI for one like the newer icon's on KDE. When XP first came out I hated the look of it. It seemed\nto \"cartoonish.\" The desktop didn't feel \"clickable.\" Now I have gotten used to it and in fact really\nlike the colors and graphics used. C'mon, lets admit when something looks good....! For me the \ndifference between XP and Win'98 is like the difference between KDE 2.2x and KDE 1.2x.\n\nHere's to you, KDE and the KDE  faithful.... Keep growing and keep bashing! "
    author: "Re: Bart's Appology"
  - subject: "Re: m.shelby@verizon.net"
    date: 2002-05-10
    body: "Yes, but ...\n\n -He had the timerity to give HIS opinion. Choice is what it's all about, people! \n\nI think he got attacked because the reasoning behind his statements seemed to be ill-stated/ill-researched/incomplete.  People then harrassed him about this with the goal of keeping quality of pieces put out about linux high.\n\n-He had the platform/outlet to deliver his opinion in a much read and followed publication. \n(this shows interest in the community!)\n\nI think a lot of people are rightly protective of the quality and applicability of the information that goes into linux publications ... otherwise, they will be less read and followed when the S-to-N ratio goes too low.\n\n -He got a whole lot (grin) of useful feedback FROM the community. This shows enthusiasm for the KDE project.\n\nThat's a good thing ... but not carefully rereading your statements and making sure that they really mean what you want to convey is not how you request feedback.  It takes a bit, emotionally (for the above reasons) out of everybody who makes the effort to put in feedback.\n\n -He had the maturity and composure to re-evaluate his remarks and attempt to explain them. \n\nSeriously, bravo.  Wish he'd re-evalutated the remarks (I do this by rereading anything lengthy I send out from top-to-bottom before sending it) and been more precise about what he saw and didn't see, and felt and didn't feel, and why, though, *before* making the effort to \n\n state his opinion (see above)\n in a publication that many people read (see above)\n for the purpose of collecting feedback (see above)"
    author: "Anonymous Coward"
  - subject: "Don't you just love it..."
    date: 2002-05-08
    body: "...when people are showing off their beautiful KDE desktop with a beautiful widget-theme, and the only app running on the desktop is a GTK-app? :D"
    author: "Per Wigren"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-09
    body: "I knew I had that one coming.  Two comments:\n1- I think all of us (in KDE, GNOME, elsewhere) should be working to make sure that end-users can use whatever their favorite app happens to be in any desktop environment, and have it seemlessly integrate with the \"native apps\" in that environment.  So the end user should be able to use GIMP or Evolution in KDE, or Konqueror in GNOME.  This is important because (1) it means more choice and flexibility for the end user, (2) will reduce duplication of efforts as people start to embrace quality product that happens to have been built using a different widget set and (3) ISVs need to know that they can build their app with whatever happens to be the best development tools for them, knowing that ALL Linux users will be able to use their software.  Many ISVs will NOT put major effort into developing desktop apps for Linux as long as they feel like they have to \"choose\" between KDE and GNOME and one of those is going to \"lose\" and if they bet on the wrong one, they'll be out of luck;\n2- I'm new to KDE so don't know how to make screenshots other than with GIMP, hence its appearance on my desktop.  Does KDE3.0 come with a screenshot utility?\n\nCheers,\n\nBart"
    author: "Bart Decrem"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-09
    body: ">Does KDE3.0 come with a screenshot utility?\n\nYes, It's called ksnapshot. You can find it somewhere like:\n\nkmenu -> Graphics -> More Programs -> KSnapshot"
    author: "ac"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-09
    body: "\"I think all of us (in KDE, GNOME, elsewhere) should be working to make sure that end-users can use whatever their favorite app happens to be in any desktop environment, and have it seemlessly integrate with the \"native apps\" in that environment.\"\n\nBart,\n\nGo look at the metatheme project. I know it is from gnome/ximian, but it lets you select themes for many different things (WM's, gtk, xmms, icons, etc). It uses a simple plugin system, just write a qt plugin. That way you can create metathemes that do exactly what you want. "
    author: "GnCuster"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-10
    body: "I've used Metathemes in Gnome, but wasn't aware that it also let me control my KDE  themage.  In any event, I'm talking about more than just themes - also things like Copy/Paste working properly, similar menu layouts, and eventually component embedding I guess.  \n\nI just tried copy/pasting between Abiword and K Word and wasn't able to do so.  The menus looked totally different.  Exit in one was Quit in the other, and the shortcuts were different.  That sort of stuff...\n\nBart"
    author: "Bart Decrem"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-10
    body: "\"but wasn't aware that it also let me control my KDE themage.\"\n\nAt the moment it cannot. But it's design is such that it could ;) I know some people who are working to make it so. I generaly agree with you that more work needs to be done to ensure that the linux desktop is coherent. I was simply pointing you to a tool many with in the gnome comunity (and by deduction most in the kde comunity) do not know exists"
    author: "GnCuster"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-09
    body: "I knew I had that one coming.  Two comments:\n1- I think we should all (KDE, GNOME, other developers) be working to make sure that users can use whatever their favorite app happens to be in any desktop environment,  If a user wants to use Evolution in KDE, or Konqueror in GNOME, that's a good thing and we should work to make sure that those apps integrate seamlessly with native apps.  3 reasons: (1) It gives more choice to users, and choice is a good thing, (2) It will help reduce duplication of effort as we can start embracing apps that have been built with \"the other\" toolkit, (3) Many ISVs will not put serious effort into developing Linux desktop apps as long as they feel like there are two platforms, one is going to \"lose\" and if they bet on the wrong platform, they'll be out of luck;\n2- I'm new to KDE.  How do I make screenshots, other than using GIMP?\n\nCheers,\n\nBart"
    author: "Bart Decrem"
  - subject: "Re: Don't you just love it..."
    date: 2002-05-09
    body: ">I'm new to KDE. How do I make screenshots, other than using GIMP?\n\nksnapshot does the trick"
    author: "Josh"
  - subject: " Bart Decrem re: Desktop Elegance"
    date: 2002-05-10
    body: "Well I guess you were right it is the kde that sucks. And it's true that icons and keramik are awesome. Maybe you should try gnome2 last build with these icons... but I guess you are now too much kde+addons, I think after 3 weeks you will change your mind,\ncheers, and by the way Ximian KDE is coming and evolution is being ported to Qt, so start whining."
    author: "Dough"
  - subject: "Re:  Bart Decrem re: Desktop Elegance"
    date: 2002-05-10
    body: "I'm actually using both KDE and GNOME now.  About 50/50.  Actually, the one big remaining issue I have with KDE is that I haven't found anything like Ximian Red Carpet.  I'm running Red Hat Linux and Red Carpet is just a really terrific updater for my Gnome packages, so I for one would be delighted if there was Red Carpet for KDE or an app that's similarly well done.  Is there?\n\nBart"
    author: "Bart Decrem"
  - subject: "Re:  Bart Decrem re: Desktop Elegance"
    date: 2002-05-10
    body: "That's up to your distribution.  If you use Red Hat they have an Updater, so do Mandrake, SuSE and Debian.\n"
    author: "ac"
  - subject: "Yes, KDE has an updater  ;-)"
    date: 2002-05-10
    body: "make -f Makefile.cvs\n./configure --prefix=/usr  (that's because you run RedHat)\nmake\nmake install\n\n"
    author: "antialias"
  - subject: "Re:  Bart Decrem re: Desktop Elegance"
    date: 2002-05-11
    body: "apt-get install kde\n\nHopefully every distribution will be able to do that eventually, not just debian. It would make redundant the need for the Ximian updater I think. Anyway, can't you get apt-rpm for distros like Mandrake (does it come with it?), Suse, Redhat?"
    author: "ac"
  - subject: "Re:  Bart Decrem re: Desktop Elegance"
    date: 2002-05-10
    body: "> by the way Ximian KDE is coming and evolution is being ported to Qt, so start whining.\n\nbullshit.\n"
    author: "ac"
  - subject: "Re:  Bart Decrem re: Desktop Elegance"
    date: 2002-05-10
    body: "> by the way Ximian KDE is coming and evolution is being ported to Qt, so start whining.\n\nReference for that please?"
    author: "Anonymous"
  - subject: "YHBT. YHL. HAND"
    date: 2002-05-12
    body: "[n/t]"
    author: "funny guy"
  - subject: "Re: YHBT. YHL. HAND"
    date: 2004-12-31
    body: "You Have Been Trolled.  You Have Lost.  Have A Nice Day."
    author: "Not so Funny Guy"
---
Here's <a href="http://www.decrem.com/i_am_butt_ugly.html">my official statement</a> in response to <a href="http://dot.kde.org/1019951547/">the recent controversy</a> over certain comments that slipped out of my mouth during a <i>LOOOONG</i> interview that also happened to include many <i>positive</i> and <i>not-so-controversial</i> comments about KDE.   I have now switched to using KDE on a daily basis and I am really enjoying it.  Thanks for the feedback and keep on hacking! :-)
<!--break-->
