---
title: "KDE-CVS-Digest for December 27, 2002"
date:    2002-12-27
authors:
  - "dkite"
slug:    kde-cvs-digest-december-27-2002
comments:
  - subject: "thasnk"
    date: 2002-12-27
    body: "Thank you for keeping us updated Derek.  Very interesting and useful report."
    author: "anon"
  - subject: "Re: thasnk"
    date: 2002-12-27
    body: "yep, I agree and I wanna say thasnk too ;)"
    author: "IR"
  - subject: "Re: thanks"
    date: 2002-12-27
    body: "\n\nyes, me 2 I agree.\n\nThis is a very good read if u wanna keep up with kde developement.\nI used to be subscribed to the kde-cvs mailinglist, but this is nicer to read\nand costs less time.\n\nbtw I compiled kde3.1 branch and it ROCKS!\n- the new font handling is GREAT (ok, it's a qt/xft2 thing... ;-)\n- Tabbed browsing ROCKS!\n- the crystal iconset is very nice, it's the best kde ever had\n- it feels moderner and even a little faster than 3.0 (I cant wait for a prelinked build)"
    author: "ac"
  - subject: "The Kmail Wars"
    date: 2002-12-27
    body: "The KMail battle.. well.. heated discussions.. are interesting to see.  Its good to see alot of work going on, but this illustrates an interesting problem that open source projects have.  Anybody can come along, branch the code and work on it.. and you may or may not want to re-merge it later.  Fortunately in this case, Kmail is going to benefit since new features and functions are getting merged back in, but some projects (Wine for example) get much more fragmented.  I guess thats a good sign that the KDE team holds together, even if some discussions are somewhat heated. :)\n\nOh, and thanks for the notes Derek!  Its great to see the work that goes on within KDE.."
    author: "elemur"
  - subject: "Re: The Kmail Wars"
    date: 2002-12-28
    body: "The reason that KMail doesn't get fragmented but Wine does is that KMail is released under the GPL while Wine used to be X11-license (now changed to LGPL)...\nWhen a program is under the GPL there is no reason for companies to fork and fragment, because all their changes must be available under the GPL also.. If it was under a BSD/X11-style license, companies can take the sourcebase, add their incompatible \"value\"-stuff and re-release it without giving their \"value\" back... That is the case with Wine (ReWind)...\n"
    author: "Per Wigren"
  - subject: "Re: The Kmail Wars"
    date: 2002-12-31
    body: "Not trying to start a license or \"software freedom\" war here, but...\nA BSD/X11 style license gives complete freedom whereas the GPL forces it to stay in the public domain."
    author: "Super PET Troll"
  - subject: "Yup"
    date: 2002-12-31
    body: "And that was exactly the poster's point*: Since the BSD/X11 license leaves more leeway on what to do with the code, it's more prone to forking/fracturing. I don't know whether I agree though.\n\n(*) Standard As-I-Understood-It disclaimer applies"
    author: "Annonymous"
  - subject: "Re: Yup"
    date: 2003-01-01
    body: "Yup! That was exactly my point. ;)\n"
    author: "Per Wigren"
  - subject: "What do you think?"
    date: 2002-12-27
    body: "What I struggled with this week, and need some feedback, is the amount of stuff. This week was the largest so far, possibly due to quoting the kmail threads. No problem there. But the stuff below that, the bugfixes and features. Were there too many? Too few? Was it easy to get around? \n\nI ask because I am building the page differently. Last week, I would parse the cvs log for the week for bug numbers, build links and layout. The bug numbers are sometimes features, sometimes the commits are part fixes, sometimes they don't fix the bug in retrospect, so I wanted to put the bug number in context with the commit comments. And, I would hand build the feature list, which took way too much time. I kept it very short for that reason.\n\nSo what I do now is watch the mailing list, put posts aside in a couple of subfolders, then run a script on those folders. The html tags, comments, links to the diffs etc are build, and if there is a bug number in the comment, I refer to it. Then I edit the output for clarity etc. The script isn't quite finished yet. I want to do better sorting, and divide things up into sections. And automate the table of contents, which I built by hand. And get the unicode stuff right. And somehow make it clear which patches were backported. Next week(s).\n\nYou are experiencing a work in progress. Good ideas are appreciated.\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: What do you think?"
    date: 2002-12-27
    body: "I must say I like the new format...very easy to follow, and full of just enough details to stay current. Two thumbs up!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: What do you think?"
    date: 2002-12-27
    body: "I thought there was too much whitespace that made the page longer than it should be.  Every diff link on its own line etc, seemed a bit too much."
    author: "anon"
  - subject: "Re: What do you think?"
    date: 2002-12-28
    body: "yeah, I agree with this.  If you can make the page shorter in length with the same content I think you'd be doing good."
    author: "Caleb Tennis"
  - subject: "Re: What do you think?"
    date: 2002-12-28
    body: "First of all, thank you very much for your effort. \n\nI think the new format is pretty straight-forward and very intuitive. Whitespace is just perfect like this."
    author: "AC"
  - subject: "Re: What do you think?"
    date: 2002-12-28
    body: "First of all, thanks for the time and effort!  Reading this stuff condensed into one page is really nice :-)\n\nI like the format this week.  Definitely plenty of information.  If it is too much work, you should back off some.  In the past time has been the major thing that has kept KC KDE from being a regular publication.  Having a small CVS-Digest is better than none at all because it is too much work to compile it.  So definitely let the scripts do the work for you, and don't worry too much about getting more information about every tiny little bugfix.\n\nThe only thing I think you need now is a nice stylesheet or something to format the entries to be more easily distinguished from each other.  Other than that, great job!"
    author: "not me"
  - subject: "Re: What do you think?"
    date: 2002-12-28
    body: "Ithink this new layout is just great. Others complain about the page being too long, with too many blank lines, but it's ok for me since I only read about new features of certain apps."
    author: "linuxgonz"
  - subject: "Still basic features are missing ..."
    date: 2002-12-27
    body: "http://www.glowingplate.com/dissent/  <---- see this site. Esp: the spellchecking!!\n\nPlus, overall, *all* Linux \"desktop\" applications are horribly horribly *slow*. So many apps are unusable on machines with < 128MB RAM (256 is better) and with CPU's slower than 500mhz ...  One application that has made *massive* advances in the speed department is Mozilla.  An application that used to take up to 45 seconds to laucnh on my p233 now launches in 9-10 seconds. And on my 500mhz machine it launches from zero in ~ 7 seconds.  Opening a new window is nearly instantaneous. Konqueror is unusably slow on the same machine (even with a konqueror process already running the pause when opening a new window is irritatingly obvious - up to 20 seconds at times).  Until the interfaces are overall much much much more snappy there's no way people will move from windows to linux or BSD with KDE ...\n\nWhen will the KDE team STOP fiddling with new features and settle down and do MASSIVE OPTIMIZATION?  This can be little UI fixes that increase the users perception of speed as well (think of NeXT on a 16mhz cpu ... most of the speed was in UI \"tricks\"), or rewriting \"stub\" type launchers for a series off platforms in optimized assembly code .. or something.  Is this kind of work just not sexy/fun enough??  Couldn't there be \"races\" to see who could get KDE to work the fastest and most usably on say a 233mhz PII iwth 128mbRAM ??  (I'd like to say 64MB but it is impossible to use KDE in less than 128 IMHO).  Wouldn't it bring out the geek pride?? \n\nAnd yes I know the argument: sure all those old boxes can be made into diskless \"XTerminals\" (with crappy video) or servers, routers ... But for low volume you can get DOS and Netware to work as a mailserver on a 486 too an how man of them do you need?"
    author: "Leaker"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-27
    body: "I am currently using Mandrake 9.0 (using KDE 3.0.x) on a laptop Toshiba with a celeron 400 (not really a fast processor, it is maybe like a PII 250 ?) and 192Mbytes of Ram and I must say that I am amazed by the speed of Konqueror browsing the web. It starts in just 4 seconds and browsing is really fast (very far from horribly slow).\nAre you sure that your problems are not coming from the binary you are using?? (remember, KDE provide source code, no binary).\n"
    author: "murphy"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-27
    body: "Optimization is really boring work, and very hard too. And if you have a semi decent machine KDE is pretty fast and it can be very hard to even know what is slow on a slower machine.\n\nMaybe you need to do some timing and traces to see which parts are taking most of the time in order to give the developers much needed information, or isn't that sexy/fun enough? :)"
    author: "Chakie"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-27
    body: "Neat little list.\n\nWhile I don't agree with everything you say, I really like the idea of a key combination to renice all busy processes, and popup a window.\n\n"
    author: "theorz"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-28
    body: "I seriously doubt Mozilla can be up and ready before Konqy opens only a new window (and I have here one K6/188 to test).\n\n> When will the KDE team STOP fiddling with new features and settle down and do MASSIVE OPTIMIZATION?\n\n Most probably never. If nothing else, optiziming code is not an easy thing to do. In fact it's difficult, often boring and time consuming thing to do. Which means not many people will be willing to do it (because of being boring), and many won't be even able to do it (because of being time consuming, and difficult). Oh, BTW, are you sure Windows XP really perform well on PII/233 with 128MiB RAM? Or are we going to compare today's applications to Jet Pac?\n\n If you're just complaining and nothing else, you're just wasting time. I'm quite sure it was said many times that KDE isn't the fastest thing in the world. Help with it, pay somebody for doing it, or ... That said, there's a mailing list dedicated to working on optimizing KDE being set up, so people (you?) willing to do more than just to complain should get their chance soon."
    author: "L.Lunak"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-28
    body: "so first you point us to a website that goes on about all the features that are lacking in KDE, then you say we should stop putting in new features to concentrate on optimization. hrm.\n\nthankfully, though, KDE is accomplishing both. new features and apps are added with each release. not every imaginable feature is available in KDE, but the same is true about Windows, MacOS X and every other desktop environment out there. but KDE provides a lot of features (and more with each release) for a nice price, with Freedom and with an extremeley transparent and open community of creators and users around it.\n\nas for efficiency, there are constant optimization efforts. there were three or four notable optimization improvements to file listings in konqueror and the file dialog committed to CVS in just the last week or two, including a way to recycle old konqueror processes/windows. compare the speed of konqueror in rendering web pages and listing files in 2.0 and 3.1 to see the sort of improvements that are being made.\n\ni'd also recommend using a decently built set of binaries on a decently configured system. i've run KDE3 on systems ranging from 64Mb to 1Gb of RAM and 166Mhz Petium 1's to 1.6Ghz Pentium 4's (and various systems in between) and so am fairly familiar with its performance from a user's perspective across a wide range of horsepower. given a decently built KDE (such as in SuSe 8.1 or MDK9) it works quite well even on moderate hardware. my main KDE devel station is a PII 400 w/256MB of RAM (only had 128 in it right through the 2.x series)..\n\ni do agree that there is room for improvement, though, as KDE isn't perfect. it would also  be cool if improvements to both the feature set and efficiency were made even faster than they are now (which is pretty darn fast IMHO). and the best way to see that happen is to get involved."
    author: "Aaron J. Seigo"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-28
    body: "premature optimization and all that.\n\nWe finally have available a compiler that is optimized for c++. Prelinking is coming along and almost there. The filesystems are optimized for server use, not desktop use, which is a plus and a minus (metadata such as icons in the files).\n\nTo say stop developing features is ridiculous. I want a decent wp with decent filters and the whole thing to work well, not crash and be fast. All are important, and the speed will come when the others are done.\n\nWasn't kde rewritten from 1x to 2x to speed up interapp communications?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-28
    body: "Check you facts, should you follow the latest news on KDE developments, you soon realize that in fact the developers pay attention to speed and performance optimization. That said, every new release of KDE consumes less memory and works faster than before.\n\nIf that's enough for you, then probably you first need to find out how to optimize your KDE (i.e compile from source, not using \"stock\" version). Also, there's KPersonalizer which helps you to find out optimal settings (features vs performance) for your machine.\n\nMy machine is P-II 400 MHz. Still, I'm a happy KOffice developer."
    author: "ariya"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-28
    body: "KDE screams (faster than WindowsXP nodoubt) on my p4 @ 2.4ghz with 1 gb of ram. \n\nI suggest get a better computer.. things, especially RAM are very cheap, and I'm just a poor student. \n\nThe above may sound like a troll, but it's not. Large amounts of optimization is pretty much unnecessary in modern computers. It's great to make KDE zippy (which it already is), but I implore for the KDE developers to NOT sacrifice features for speed. "
    author: "g to the izzo"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-28
    body: "Anti-constructive spurting. Sorry, nothing interesting wrapped up in teribilist vocabulary."
    author: "Inorog"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-29
    body: "I would like to comment on that one. We have here a p2-266 with about 320 MB of memory. It's serving about 10 X-terms and some other shell-users without any pain. So to say KDE is bloated and slow, I can't agree and so can't my users. And for the records, we're talking about kde2."
    author: "Hans"
  - subject: "Re: Still basic features are missing ..."
    date: 2002-12-29
    body: "\"http://www.glowingplate.com/dissent/ <---- see this site. Esp: the spellchecking!!\"\n\nHmm, silly kind of FUD, this site is.\nSomething like: \"lets look for ways to proof that Linux is not ready for the desktop\". So start installing a desktop using a monochrome monitor, checkout Xine, but not other similar apps, bashing kmail's spell checking, but refusing to look for more 'complete' mailapplications, or start using them in stead, Expecting Linux-vendors to create a product that is similar to MS Windows, leaving out the fact that it took MS at least 9 years to get it's GUI in a somehow decent way, complaining about scrollwheels (which were already supported in RH 7.1), since Windows supports them as well (forgetting that the support in Windows came from the vendor of the mouse, not from Microsoft..) etc. etc.\nLooking for basic things that are not available in KDE (as in the desktop), the site mentions none..\n\nRinse"
    author: "rinse"
  - subject: "kde performance -- give me a break"
    date: 2003-01-02
    body: "KDE is written in C++. While this is not necessarily a problem, it can be when Visual Basic reject programmers (which the KDE project is overrun with) do not know enough to avoid important pitfalls that plague C++ software projects; KDE suffers badly from stupid use of autoincrementing operators and iteration with C++ objects and masses of unnecessary allocations and deallocations of memory -- two of the most comon problems in C++ software.\n\nPerhaps the most cretinous of all problems is blaming the extremely slow startup times of KDE apps on GCC. The GNOME 1.x releases were hardly svelt (2.x fixes many of these issues), but GNOME is a fashion cat-walk superwaif when compared to KDE's 500lb fat-momma cheese-burger scoffing trailer trash.\n\nOne need only look at the recent fuss over ugly KDE hacks (such as prelinking) used to bandage up the design and coding flaws in the decrepit KDE architecture to see the truth."
    author: "GNOME user"
  - subject: "Re: kde performance -- give me a break"
    date: 2003-01-02
    body: "Go trolling elsewhere. Just for your information:\n1) most KDE ugly hacks are there used to bandage up the poor C++ support in GNU build tools (unless one is happy to limit their C++ coding to 'Hello World' style apps)\n2) prelinking is not a KDE thing, it's part of the GNU build tools (and I don't think it's ugly hack actually)\n3) Visual what?\n"
    author: "Zelva"
  - subject: "Re: kde performance -- give me a break"
    date: 2003-01-03
    body: "Hmm, KDE is faster than Gnome on my system :):):)\n\nRinse\n\n"
    author: "rinse"
  - subject: "Re: kde performance -- give me a break"
    date: 2003-01-05
    body: "> Hmm, KDE is faster than Gnome on my system\n\nOdd you should say that, it is here too (Debian Sid).  Gnome2 could do with some serious optimization, although it does login quicker than KDE3 does.  Otherwise Gnome2's the slowest GUI I've had the misfortune to use:  it's less responsive than MacOS X on an approximately equivalent machine, which is a pretty piss-poor show given how much more work MacOS X is doing with all that alpha-blending and real-time smoothscaling.\n\nActually here KDE3 is roughly the same speed as Windows XP/2000, and feels noticably faster than Windows 98 (this may be perception rather than reality:  for instance, I can continue to move/minimize windows and get on with the rest of my work when an app freezes momentarily, and Konq 3.0.x is significantly faster than IE, especially on sites with big tables like Slashdot).\n\nJust for the record, my system is PIII/866, 384MB RAM, GeForce2, and I'm not yet using prelink.  Once upon a time I had a PII/233 and KDE felt significantly slower than Windows 98, but on my current system this is no longer the case - KDE seems to benefit far more from a faster system than Windows does.  For what it's worth, I am also using a hacked-up Linux kernel with the O(1) scheduler, kernel pre-emption and low-latency patches, these seem to make an enormous difference.\n\nA PIII/866 is not exactly new technology.  I think those people who diss the speed of X/KDE either are astroturfers/trolls or have hopelessly slow machines (probably 500MHz or less, or with dreadfully substandard graphics cards).\n\nWe don't expect Windows XP to perform great on PII/233's with 64MB RAM, why do we expect KDE (which is arguably more capable than XP) to perform great on these outdated platforms?  It would be nice if it did, but unfortunately that's not how software engineering works.  For those people there's always fluxbox..."
    author: "My Left Foot"
  - subject: "Re: kde performance -- give me a break"
    date: 2003-08-16
    body: "Hi there I am running a PIII 450 with a TNT2 and while i wouldnt say that KDE is faster than windows 98 or 2k i will say that on the same machine it is faster than GNOME 2.2...longer to load yes, but more responsive. Also I have noticed that now I am running the 2.6test3 kernel the whole UI experience is much more similar to windows...very much snappier.....i think this is the pre-empting.\n\nTo be honest...I like the UI of GNOME better, please this is my opinion and not a troll, however even on low end hardware i find it less frustrating to use KDE as it has wireframe moving and re-sizing"
    author: "Andrew Mason"
  - subject: "Re: kde performance -- give me a break"
    date: 2003-09-12
    body: ">I think those people who diss the speed of X/KDE either are astroturfers/trolls\n>or have hopelessly slow machines (probably 500MHz or less, or with dreadfully >substandard graphics cards).\nThis is true.\n>We don't expect Windows XP to perform great on PII/233's with 64MB RAM\nThis is, strangely enough, not true. On a PII/64mb RAM XP runs allright, which is not the case with KDE. I\u00b4ll check out Gnome now.\n>Konq 3.0.x is significantly faster than IE, especially on sites with big\n        tables like Slashdot).\nI observed the same.\nBasically you\u00b4re right complaining about conferring old Win 9.x with new KDE versions. But one thing pisses me (as PI 166Mhz user) off. When I used old SuSe Linux 6.1 I had no performance problems at all but couldn\u00b4t install newer SW (glibc too old; not even jdk 1.2 - under Win98 I hadn\u00b4t had trouble with 1.4) Now I upgrated to debian woody, can install everything - and have severe performance problems\n"
    author: "gebhard"
  - subject: "KDE and GNOME are slower than Windows"
    date: 2003-01-21
    body: "Your observations are correct.  KDE and GNOME are extremely slow compared to MS Windows.  I have installed all of the above on the following CUP types: 486-33, P90, P166, AMDK6-300, AMDK6-500, and AMD 1 GHZ.  The results were always the same:  Windows was much much faster. \nKDE on a 1GHz processor was a happy experience (approximately as fast as Win98 on my old 486-33).  That calculates to Windows being some 30 times faster than KDE.  However, I can usually endure KDE on a 500MHZ CPU.\nWe are faced with the following possibilities:\nMicrosoft has some brilliant programmers and mathematicians who discovered how to make Windows run fast, and/or perhaps they are using a proprietary compiler that knows how to make code run fast, or perhaps they know some assembler tricks that have eluded the Linux gurus. \nOr, perhaps something is coded improperly in X, or the C compiler.\nI ran across a vendor who claimed their desktop GUI for Linux was fast, but I couldn\u0092t get the demo to work.  It cost about as much as Windows.  \nI also discovered the bugs you mentioned, and some others.  KDE and GNOME seem to have more problems than Windows 98.  I run Win2K at work and it has been mostly stable.\nI think all those claims of Linux stability and speed only apply to the shell environments.  As soon as you start X, you are in a slow Linux world.\n\nThe only solution I see is to get a really fast CPU, and wait till someone has the time and genius to make X run fast.\n"
    author: "Mark"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2003-05-12
    body: "I tend to agree with Marks comments. I have recently just switched to using linux full time as my desktop as the tools available for the platform made it much easier to do my job, however nothing I could do could speed up the Gnome or KDE performance so that it was at least close to the windows 98/2000 GUI. There is a slight lag when bringing up windows in Gnome/ KDE and the actual desktop environment loads significantly slower, even in Gnome. This doesn't change even if I am running on an Athlon 1800 with GF3ti or my PIII 450 with a TNT2. I have used hdparm, recompiled my kernel and upgraded to the latest version of X but I still find it no where near comparable. If anyone knows how to make X with KDE/Gnome quicker than windows 2k then please tell me how and I will be eternally grateful.\n\nAndrew"
    author: "Andrew Mason"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2003-07-10
    body: "I afgree with Mark. I am a freelancing software developer and got a offer to develop a linux driver for a special type of memory cards. So I had to install on my laptop (Acer Travelmate 514TE, 466MHz, 192RAM) a SUSE8.1 linux. At the very fist moment when I used the KDE I thought it is programmed in Java Swing. It is awfully slow compared to windows 2000 and windows 98.\nI assume when you want to start a KDE application the KDE is searching for files. Maybe one has to configure something like a search path of KDE?\nI cannot image that a C++ GUI is that slow.  If someone knows how to speed up KDE without recompiling the kernel and skipping most of the KDE overhead,please tell me!!"
    author: "Alex Balka"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2003-07-11
    body: "Fix your system settings: IDE DMA setup, localhost DNS name lookup, ..."
    author: "Anonymous"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2003-10-31
    body: "I've been having these same problems.\nYou are saying I need to fix my system (I have a PIII/550 w/ 128mb ram),\ncan you elaborate on that, or perhaps point me to a url where it is explained?\n\n"
    author: "David"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2003-11-20
    body: "Is Linux really better than windows?? \nor is it a case of \"Emperors new clothes\" to all the cheezel munchers.\nAll the suggested fixes are minimal...."
    author: "Thx1138"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2004-03-27
    body: "Linux is better than Windows is relative.\n\nLinux is better for me, because I do alot of work that I need to be secure, and I need a reliable system that will not go down due to system exploits, or virus attacks. \nLinux is also better for me, because I can a reliable appication for almost any task I need to do, that application is almost alway freeware...keyword is free.\nMost of the industry standards, for database access/creation, webscripting, webhosting, DNS hosting, all come standard in Linux, and are Free!\nYou can get these FREE open standards on Windows, but only as part of 3rd party install, and it usually cost money...in some cases alot of money.\nLinux is smart while installing, so you don't have to be. Linux by defaut partions your hard drive so that you have all your system files on a root partition (eg c:/) and all your installed applications, documents, and other personal data, on a home partions (eg d:/) Sooo if Somthing ever does go completly wrong with the OS, I can format only the system or Root partion, reinstall linux, and I have no lost files.\nFurther, you can designate a portable drive, like a USB keychane storage divice as your Home partion, basically take you computer with your where ever you go, in you pocket.\nAgain most of these things can be done in Windows, but you have to know how to do it, as by default windows installed everything in one partion, and alost keeps your personal files on that same partion.\n \nHowever\n\nWindows is better for my brother, becuase he plays games.\nWindows is more userfriendly than Linux.\nInstalling software on Windows is usually fast, and straight forward.\nWindows is far better in the printing area...every printer will work on windows. ( I had a lexmark X83 and it never worked in linux, and it never will)\nMore support for Multimedia hardware.\n\n\nMy opinion (relative)\n\nWindows PC's are FisherPrice...thats not a bad thing! If you want an enterainment system, that supports high end digital sound cards, the latest video card, and can play all the latest video games, then Windows is definatly for you.\n\nIf you need a reliable computer, that just works, and gets the job done, or you run a small office and don't feel like paying for 15 copies of Windows, and 15 copies of MS Office the choose Linux and OpenOffice 1.1 (nearly 100% compatible wit MS Office)  all the way.\n\n\nFor the record, I use Mandrake Linux 9.2, Mac OS 10.3, and Windows XP and if I had to choose just one for the rest of my life...Mac OS 10.3 because it just sooo sexy..."
    author: "Trapper"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2008-09-30
    body: "faggit"
    author: "lol"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2006-08-30
    body: "I've been trying a few different varients of linux over the past few years.  Don't get me wrong.  I don't yet know or understand much about linux and how it works on the inside. But I intend to because I've had a gut full of the good ol bill gates error messages, which tell you enough to know something serious just occured, but not enough for you to know what so you can figure out how to fix it. This inturn eventually leads to other problems, such as the notorious \"blue screen of death\" which invariably happens in the middle of something super important you've spent ages on, and are just about to finish. (It actually makes me wonder whether Bill Gates and good ol \"MURPHY\" and his law, are actually cousins!)\nWindows WILL slow down over time, the more antivirus, anti adware etc, or even just adding, trying and removing different software etc, will ALWAYS slow it down, even when you clean up registry's and do the typical housework and chores needed to keep Mr Gates \"little treasure\" healthy. Often it gets so bad that when you invoke a program to start, you wonder if it has actually frozen or locked up while trying to start, because it's taking so long to load!\n\nTaking all these things into account, such as downtime, slowtime, stress time etc, and bearing in mind you have to P.A.Y through the nose for the privilidge, linux in general seems more robust.  even if something does lock up, it doesn't seem to affect the whole system, so often you can work around the problem.  (Often when windows stuffs up, all you can do is move the mouse around the screen and press the reset button on the computer!)\n\nBack to the point of the KDE / Gnome speed issue though, have any of you guys tried out puppy linux?  If you haven't, then suss out these sites:  \n\nwww.puppylinux.org    and    www.puppylinux.com\n\nI reckon speed is better than or at least equivelant to XP. especially when loading and booting up the system. And as for it's size,   ...UN-BELEIVABLE!!! especially when you see what software it comes pre-loaded with!\n\nLet me know what you think   I'd love someone elses feedback on this distro.\n\n"
    author: "Rob"
  - subject: "When Linux will become a Final Product, not Beta?"
    date: 2007-06-12
    body: "I am running SLED 10 with Athlon XP-M 1.4 GHz and have the impression that KDE is getting slower and slower with every new distro I install.  Actually there are two principal stages when booting Linux: the first one is very slow and I guess are the processes of the linux kernel. It is very difficult to set which should be running and which should not because the information is written in hieroglyphs (at least for me I feel). The wait seems to me a little eternity. Then another long wait to start KDE (or Gnome). En revanche, Windows XP is incredibly fast at boot. Gentlemen, it is about time to find a solution: KDE and Linux developers should now spend their time fixing these issues, instead of fooling around with \"eye candy\" and \"wow effect\" crap. Every time I install a new distro, I have the same feeling: it is BETA software: always with bugs, always with issues. Bloated and heavier each time. Lets compare with Windows XP: it is a FINAL PRODUCT! That is what I want to find in Linux: A FINAL product, not a BETA product (a beta software is always under development, and never seems to be able to achieve this simple goal: to work flawlessly. For once. Yeah, developers want to keep themselves busy, fixing a coma or a dot, forgeting the simple principle of work that Microsoft so elegantly masters."
    author: "Maxei"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2007-06-27
    body: "to the bloke that said he wants a finished product id recommend trying out the latest debian stable distro which at the moment is etch.  should be what your looking for although the software will be somewhat older than what you might be used to(still more modern than windows:p) i run unstable and it seems like more than a finished product to me. and its free as always:)\n\nmy 2 cents:)"
    author: "kyle"
  - subject: "Re: KDE and GNOME are slower than Windows"
    date: 2008-04-19
    body: "Hi\n\nI agree with most of upper replies. I have old PII 400\n(100Mhz FSB !!! it matters :D ), 160 MB RAM, and I just dont get the performance I want. I\u00b4ve tried more than 10 distros so far and I just cant get machine running fast (I\u00b4ve NOT tried DEs such as KDE and GNOME but XFCE). I know that w2k would run flawlessly and fast but I just want to learn something new (which is hard BTW). I know that bash linux run fast, but getting fun with bash commands just isnt way of \"entertainment\" nowdays...\n\n\nFor the fun I was speaking of I\u00b4ve been trying to get my sound card working under linux but I just couldnt get it running at all (it was CS46xx), but that is another story..."
    author: "MetallMann"
  - subject: "Re: Still basic features are missing ..."
    date: 2003-01-02
    body: "OK ... I agree that adding memory can help ... I upped my p233 to 512MB (max for this mainboard) and it works much better. I'm glad to hear there are improvements in gcc and glibc that will help C++ projects.\n\nPlus I hope that the the excellent Qt/KDE documentation can help C++ coder refugees from Windows actually learn how to code better.  You don't learn good coding on Windows!! While C might be painful for GUIs but C coders are generally more efficient (maybe because they are older ;-) ...  If KDE and Qt contribute to improving the \"skills\" of C++ coders it will be a major plus for the industry!\n\nStill though - back to my problem - even with all my RAM a MAJOR THING (that I think requires a minor fix) is lack of konqueror repsonsiveness on the desktop (on a variety of kernels and OSes - BSD's, preempt linux etc.). Here's what happens:\n\n- I open Konqueror for the first time and it takes a long time (~10 seconds).\n- Afterward if I use the menubar *of the open Konqueror window* to start a new window it opens fairly quickly.  \n- However if I click on the Konqueror or Home icons to lauch a new window it takes ***forever*** (even longer than the first lauch) while the system tries to launch and new konqueror process then stops, swaps (galore on 128 meg system! less w/ 512 but still ton's of disk activity)\n\n\nCan't there be a user process called konqd (sort of like artsd) that could hook into fam and watch the FS *and* track windows etc etc. get started on login ... I guess this can't be tied into the KWM since KDE is WM agnostic so ... konqd anyone?? Am I nutz?\n\nWhy does this occur? Is there a simple fix? This is what I meant by refering to NeXT ... NeXT was actually pretty slow (bench marks reveal that!!) it drew to the screen using DPS (!!) and ran a 16mhz cpu but the engineers looked at what people di the most often and used UI tricks, stub code, separate \"background processes\" (this was on Mach so well not really the same), threading etc. to make those processes (opening new windows etc) faster.  They also designed things like the UI of their \"finder\" (equiv. of konqueror) to not encourage opening new windows!  Konqueror and the WM/Desktop are the things that need to most hacking to make fast IMHO.\n\nOT but:\n\nAnd there is not a single OSS project out there that can claim to have made the speed ups that Mozilla has made. Granted they started off slow as molasses but the facts are: they use C++, they use gcc, they made ***massive*** performance gains from ~0.96 to 1.0 and 1.1/1.2/1.3 ....  especially in launch times.  \n\n30-40 seconds (to *lauch*) down to 7-9 seconds....\n"
    author: "Leaker"
  - subject: "Re: Still basic features are missing ..."
    date: 2003-01-03
    body: "You can make your desktop more responsive with renice -10 [pid of X]\n\nRinse\n"
    author: "rinse"
  - subject: "Re: Still basic features are missing ..."
    date: 2003-01-03
    body: "I suspect we will see the same improvements with kde. Many of the applications are either in the middle of rewrites(kmail), or just finishing (konq). Koffice isn't finished yet. Once they are done, most of the desired features implemented, then optimization can happen. For example, konq became finished for me with 3.1, able to use it for online banking. This hardly can be called bloat.\n\nNow a bunch of optimizations are happening. The next release of konq will be faster. As the various applications become feature complete, then optimizations will be done.\n\nIt's easy to have a fast application that doesn't do everything you need. I noticed that with abiword. A year more ago it was very quick. Now it is a little slower, but much more feature complete. Once it is done, I'm sure the developers will speed things up.\n\nFrankly, I'm amazed at the speed of desktop development for linux.\n\nDerek"
    author: "Derek Kite"
  - subject: "Good work - keep it up!"
    date: 2002-12-31
    body: "\nYou are creating a great resource. Keep up the good work!\n"
    author: "Jesper Juhl"
---
This week's KDE-CVS-Digest is <A HREF="http://members.shaw.ca/dkite/dec27.html">now available</A>. Subjects discussed include the conclusion of the security audit, <A HREF="http://kmail.kde.org">KMail</A> merge problems, bugfixes and lots of new features in <A HREF="http://kate.kde.org">Kate</A>, <A HREF="http://edu.kde.org/kig">Kig</A>, <A HREF="http://extragear.kde.org/apps/gwenview.php">Gwenview</A>, Krdc, <A HREF="http://devel-home.kde.org/~kgpg/">kgpg</A>, <a href="http://konsole.kde.org/konstruct/">Konstruct</a>, <A HREF="http://kopete.kde.org">Kopete</A>, <A HREF="http://cervisia.sourceforge.net/">Cervisia</A>, <A HREF="http://www.kdevelop.org">KDevelop</A>, <A HREF="http://www.koffice.org">KOffice</A> and <A HREF="http://edu.kde.org/kalzium">Kalzium</A>.  And much more.

 
<!--break-->
