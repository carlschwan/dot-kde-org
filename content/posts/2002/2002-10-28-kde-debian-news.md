---
title: "KDE in Debian News"
date:    2002-10-28
authors:
  - "numanee"
slug:    kde-debian-news
comments:
  - subject: "Totally Off Topic but...."
    date: 2002-10-28
    body: "Can anyone tell me how to remove the Cervisia button from my Konqueror toolbar?  It sticks out like a sore thumb and seems to be there to stay...\n\nOther then that...\nBluecurve + KDE = bliss"
    author: "Rimmer"
  - subject: "Re: Totally Off Topic but...."
    date: 2002-10-28
    body: "Settings->Configure Toolbar\n\nOther than that...\nSuSE + KDE = total bliss!"
    author: "ac"
  - subject: "Re: Totally Off Topic but...."
    date: 2002-10-28
    body: "I doubt Configure Toolbars allows to remove the view-mode buttons, those are kind of highly dynamic, and part of an actionlist IIRC (so from \"Configure Toolbars\" you can remove them all, but not one in particular :}).\n\nUnfortunately at the moment, the only way to get rid of a viewmode button is to edit the .desktop file for the part, and remove MimeType=inode/directory from it.\n"
    author: "David Faure"
  - subject: "Great but"
    date: 2002-10-28
    body: "I finally got rid of the whoel viewmode button.  Konqueror looks a bit cleaner and lighter (a little more Nautilus like even).  Unfortunately, the KDE screensavers dont work (the screen just goes black).  The screensavers look fine in the preview... any suggestions?    "
    author: "Rimmer"
  - subject: "Re: Great but"
    date: 2002-10-28
    body: "Haha I would say you are a TROLL just because you use 'lighter', 'nautilus' and 'cleaner' in your two sentences you wrote."
    author: "TrollBuster"
  - subject: "I'm sorry but"
    date: 2002-10-28
    body: "Nautilus is beautiful.  The Konqueror tool bar is just too busy (too many buttons ect).  It looks a lot better after I removed some buttons (including the Cervisia thingy) though.  I know that Nautilus used to be SLOW, but now it seems just as fast as Konqeror.  Konqueror gets high marks for being very configurable (I love that you can edit the toolbars so easily).  Too bad some of the items can't be removed/added nicely."
    author: "Rimmer"
  - subject: "Re: I'm sorry but"
    date: 2002-10-28
    body: ".... now the magical question is... is nautilus as usable as konqueror... let me answer this for you...\n\n\n\nNO"
    author: "TrollBuster"
  - subject: "What is Nautilus missing exactly?"
    date: 2002-10-28
    body: "It seems very usable."
    author: "Rimmer"
  - subject: "Re: I'm sorry but"
    date: 2002-10-28
    body: "Maybe you should try it (and maybe Gnome too). Its very usable, very light and clean (as the previous poster mentioned) and just as fast as Konqueror.\nIt lacks lots of functionallity found in Konqueror - like splitting into two or more views, kioslaves etc - but its a very  slick and pleasant to use filemanager. \n\nCheers\nBTW im a KDE and not a Gnome user"
    author: "J. Andersen"
  - subject: "Re: I'm sorry but"
    date: 2002-10-28
    body: "I know GNOME very best. Belive me!"
    author: "TrollBuster"
  - subject: "Re: I'm sorry but"
    date: 2002-10-28
    body: "Please stop trolling."
    author: "Navindra Umanee"
  - subject: "Re: I'm sorry but"
    date: 2002-10-28
    body: "Where's the Troll ?"
    author: "TrollBuster"
  - subject: "Re: I'm sorry but"
    date: 2002-10-29
    body: "http://dot.kde.org/search?author=TrollBuster"
    author: "Navindra Umanee"
  - subject: "Re: I'm sorry but"
    date: 2002-10-29
    body: "So you reflect that the funny choosen nick that I use is to be set equal with Trolling ? Look, as I said earlier I know GNOME very good. Was using it for some years now and I know that Nautilus simply suck as Filemanager. The dramatic speedincrease of Nautilus wasn't due heavy Nautilus codechange or something it happened because they changed all kind of Bonobo components. Nautilus from GNOME 1 to GNOME 2 was usually a simple port with some enchancements but it stays the same sucking program for GNOME 2 as it was for GNOME 1. Now if you go inside the GNOME CVS you see basically no significant changes for Nautilus. Even for GNOME 2.2 Nautilus is not gonna change seriously. I use KDE now and can tell you that I use it in a regular basis. For more Nautilus based readings please go here http://www.gnomedesktop.org/article.php?sid=718"
    author: "TrollBuster"
  - subject: "Konqueror - Cervisia button"
    date: 2002-10-30
    body: "> I removed some buttons (including the Cervisia thingy) \n\nHow did you remove this Cervisia button ? I don't understand that it is not easy to remove a button so completly unesuful for end-users... (even, why is it in the default configuration ?)"
    author: "Alain"
  - subject: "Re: Konqueror - Cervisia button"
    date: 2002-10-30
    body: "It is only present if you install the kdesdk package - as this package is for developers, I don't really see a problem. I agree though that we need to make the view configuration stuff in konqueror a little more flexible.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Konqueror - Cervisia button"
    date: 2002-10-30
    body: "I have installed some development packages because it seems necessary to install (compil) some new app. Of course it is a very tiny problem, it's only a little irritating to see an icon that I never use and cannot remove (even by uninstalling Cervisia).\nMore troubling is the difficulty to keep a good size for the URL toolbar when it is beside an other (horizontal) tool bar. I posted a bug report about it, it seems a little better with KDE 3.0, but regularly, sometimes (with long URL), the URL bar does not keep its initial position. I must delete its content and launch again Konqui."
    author: "Alain"
  - subject: "Re: Konqueror - Cervisia button"
    date: 2002-10-30
    body: "There is a difference between the XXX-devel packages and kdesdk packages. The former are the header files etc. and, as you say, these are required if you want to comile stuff. kdesdk on the other hand is a package of development tools - Cervisia, kbabel, kbugbuster etc. and various scripts.\n\nYou say that removing Cervisia didn't remove the icon - this implies your packages are broken as it should remove both. I suggest you inform your distro (after checking to see if they've provided updated packages).\n\nCheers\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: Konqueror - Cervisia button"
    date: 2002-10-30
    body: "Thank you. I use Mandrake 9.0. I have removed the kdedsk package (and it implies only the removal of kdevelop). So, now, the Cervisia icon is replaced by a \"?\" icon. Perhaps it will desappear when I will install KDE 3.1...\n\nSo the better seems that, when uninstalling Cervisia, the distro removes the Cervisia icon and that when seing the Cervisia icon is not here, Konqueror stops to display a \"?\" icon."
    author: "Alain"
  - subject: "Re: Konqueror - Cervisia button"
    date: 2002-10-31
    body: "The correct way to remove it is, go to Control Center/File Associations and delete Cervisia for the type inode/directory."
    author: "Anonymous"
  - subject: "Re: Konqueror - Cervisia button"
    date: 2002-10-31
    body: "Yes, thank you ! Removing this option is not obvious, neither for the user, neither for the distro, neither for KDE, it seems...\n\nI would willingly take advantage of your aknowledge... I have a strange icon behaviour with Kate. When I modify a document, I have 2 icons in the taskbar, the Kate icon, and a \"?\" icon. When I save the document, the second icon disappears..."
    author: "Alain"
  - subject: "Re: Great but"
    date: 2002-10-28
    body: ">, the KDE screensavers dont work (the screen just goes black). The screensavers  > look fine in the preview... any suggestions? \n\nUsing RedHat, right? \n"
    author: "Sad Eagle"
  - subject: "Yep"
    date: 2002-10-28
    body: "You wouldn't not help me because of that would ya? :)"
    author: "Rimmer"
  - subject: "Re: Yep"
    date: 2002-10-28
    body: "The quality of KDE in RedHat is somewhat questionable.  They introduced a lot of bugs specific to their distribution."
    author: "ac"
  - subject: "How do I report this?"
    date: 2002-10-28
    body: "How can I notify RedHat about this problem?"
    author: "Rimmer"
  - subject: "Re: Yep"
    date: 2002-10-28
    body: "Well, then I can say \"it's a packaging bug\"; they apparently(*) know it, and fixed in RawHide.... Except that apparently(*) they also broke KMenu in those packages, so the only immediate solution I know of is either to build from sources, or to try to figure out the breakage yourself based on changes in the package; or you could simply try to wait for some sort of updated packages that don't break something bigger.. \n\n* = Based on KDE and RedHat bug system information"
    author: "Sad Eagle"
  - subject: "Re: Great but"
    date: 2002-10-29
    body: "To get the screensavers to work, I had to add /usr/X11/lib/xscreensaver to my path in Slackware. Everything appears to work fine (I use CVS though, YMMV)."
    author: "Brent Cook"
  - subject: "KDE3.1 in Debian"
    date: 2002-10-28
    body: "Hopefully a version of KDE 3.1 will be in SID in the near future. Not sure how long, but hopefully it will be there before (or shortly after) 3.1 is released "
    author: "dpash"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-28
    body: "In related to that, Gnome2 is already in Unstable, altrough it was released after KDE3. Bummer. Oh well, I'm voting with my feet and migrating to Gentoo."
    author: "Janne"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-28
    body: "Gnome 2 is in general unstable so who cares where it is :)"
    author: "TrollBuster"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-29
    body: "Don't be a morron. KDE 3 isn't in sid yet ... because debian is migrating to gcc 3.2 (as you know breaks c++ backwards compability and KDE is mostly c++). And yes. The Debian packages are ready at ... http://www.kde.org. And yes, made by Debian people. \n\nSo stop whining and apt-get install kde 3.0.4 (I'm using it now).\n\n\nRegards,\n\nC."
    author: "C."
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-29
    body: "\"Don't be a morron(sic)\"\n<br>\nI'm not. I just like to have software available in timely manner. KDE3 is MONTHS late! Xfree4.2 was about about 8 MONTHS late!\n<br>\n\"KDE 3 isn't in sid yet ... because debian is migrating to gcc 3.2\"\n<br>\nAnd before that KDE3 was delayed because migration to Xfree4.2. I wonder what excuse they think of after they have migrated to GCC3.2.\n<br>\n\"And yes. The Debian packages are ready at ... http://www.kde.org. And yes, made by Debian people.\"\n<br>\nUnofficial packages.\n<br>\n\"So stop whining and apt-get install kde 3.0.4 (I'm using it now).\"\n<br>\nNo thanks. I rather install Gentoo and do \"emerge kde\". Some distros have packages available soon after the release of the software, not over half a year later!"
    author: "Janne"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-11-05
    body: "Have you ever thought about HOW MANY ARCHATECTURES Debian supports?\n11 at last count. Getting XF 4.2 to work on all of them took months. There were test packages for most archatectures (i386, sparc, arm, powerpc) on branden's debian page (he's the X maintainer)"
    author: "LapTop006"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-11-19
    body: "> I just like to have software available in timely manner. \n> KDE3 is MONTHS late! \nNo one is stopping this from being available, but yourself!  -- that is how open source software works.\n\n"
    author: "fool"
  - subject: "Re: KDE3.1 in Debian"
    date: 2003-02-17
    body: "you don't pay for the people who work for you, so be more comprehensive or do it yourself!!"
    author: "please be more comprehensive"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-28
    body: "Well, maybe when KDE 4.0 is out :)"
    author: "AC"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-28
    body: "As far as I know, KDE 3.x integration is waiting on gcc3.2 integration, which is a much bigger deal than just KDE.  I have no idea when it's going to happen. "
    author: "mullr"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-28
    body: "And before that, KDE3 was delayed because of Xfree 4.2 integration..."
    author: "Janne"
  - subject: "Re: KDE3.1 in Debian"
    date: 2002-10-29
    body: "And so on...\n\nI'm thinking about making a KDE reference system on CD based on Knoppix, containing only KDE programs (thus optimized for them) and incorporating a new filesystem hierarchy (5 main folders: System [stuff necessary for booting], Libraries [everything shared], Programs [all programs], Settings [all settings by all programs and accounts], Documents [personal stuff by all users]) which allows easier backups. The main focus of this disk would be Usability and Accessibility surveys though.\n\nWe'll see when I'll have more time or someone else starts a similar project.\n\nDatschge (happy Knoppix/KDE user with AMD K6-2 300mHz and 228MB RAM =o)"
    author: "Datschge"
---
<a href="mailto:switch2002esN@SPAM_PLEASEyahoo.es">Switch</a> wrote us a while ago to inform us of <A href="http://www.linuxsilo.net/">unofficial</a> KDE 3.0.4 packages for Debian PowerPC.  Instructions <a href="http://bulmalug.net/body.phtml?nIdNoticia=1555">here in Spanish</a> -- English users just have to grab the sources.list line.  In related news, <a href="mailto:zack at kde.org">Zack Rusin</a> wrote in to inform us of the <a href="http://www.debian.org/devel/debian-desktop/">Debian Desktop</a> project. <i>"The Debian Desktop subproject is a group of volunteers who want to create the best possible operating system for home and corporate workstation use. Our motto is 'Software which Just Works'"</i>. Proper integration of <A href="http://davidpashley.com/debian-kde/faq.html">KDE into Debian</a> seems to be considered an important part of this project.   In fact, one of <A href="http://www.sable.mcgill.ca/">the school labs</a> here uses Debian with KDE 2.2.2 provided on the workstations.  It's done fairly well with few unpleasant surprises. Even better, the <a href="http://www.cs.mcgill.ca/">major teaching labs</a> here use <a href="http://www.freebsd.org/">FreeBSD</a> thin clients with KDE 3.x installed as the desktop.  Very cool.
<!--break-->
