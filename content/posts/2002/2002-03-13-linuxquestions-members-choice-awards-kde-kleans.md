---
title: "LinuxQuestions Members Choice Awards: KDE Kleans Up"
date:    2002-03-13
authors:
  - "Dre"
slug:    linuxquestions-members-choice-awards-kde-kleans
comments:
  - subject: "Told you so : )"
    date: 2002-03-13
    body: "Way to go KDE developers and users. I've believed that KDE was approaching 70% of the Linux Desktop market for almost a year now. Except now more people are starting to finally realize it. To say so know is not seen as flame bait anymore.\n\nCraig"
    author: "Craig"
  - subject: "Re: Told you so : )"
    date: 2002-03-13
    body: "If this is true then there must be now be a huge number of people using linux on the desktop, because a guy from ximian seemed to think that they had at least 800000 users (not including those installing from cd etc), and i would guess that substantially more people use standard gnome than ximian gnome (because it isn't included with any distro that i know of) if kde then had more than twice as many users as those 2 combine it would be very impressive. It is a shame that it cannot be tested properly though - most distros include gnome and kde, and surveys are notoriousely unreliable at finding out what (the majority of) people actually use.\n\nEither way, well done to KDE.  I use ximian gnome myself but am glad to see progress being made by all the desktops."
    author: "dave"
  - subject: "Re: Told you so : )"
    date: 2002-03-13
    body: "Am I the only person in penguinland that switches between kde, gnome, and assorted other desktops the way some people change shoes?  "
    author: "keith"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "Yes.\n\nDon't you have better ways to spend your time?\n"
    author: "Rayiner Hashem"
  - subject: "Re: Told you so : )"
    date: 2002-03-15
    body: "\nYou are not the only one who likes use different desktops, even that I \nlike kde the most at the moment.\n\nJaassu\n"
    author: "Jaassu"
  - subject: "Re: Told you so : )"
    date: 2002-03-15
    body: "I also switch all the time! it depends on what I should do! if I should do something quick .. then I use gnome becouse it have a almost zero loading time compared to kde. But If I know that I will sit infront of the computer for a while then I use kde most often. \n\nI hope that kde3 fixes the loading times!"
    author: "Andreas"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "Not everyone that downloads something actually uses it.\n\nKind regards, Rinse\n"
    author: "rinse"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "no, but they have this nice thing called red-carpet - i guess they get their numbers from people who use that.  (don't know for sure though)"
    author: "dave"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "There fudgeing there numbers by claiming downloads are equal to users. Gnome is lo buggy i've had to download it many times. \n\nCraig"
    author: "Craig"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "Then report this instead of whining about it. Do you think this is a common problem that occurs very often? And that the GNOME developers just sit there and do nothing about it?\nNo, it's a rare problem, that's why it hasn't been fixed yet. So don't complain and send a bugreport. Do you think the developers can read your mind?\n\nAnd for your information: GNOME has almost never crashed once on my system. KDE crash a little more often (though still significantly less than Windows crashes)."
    author: "Sotf"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "If you're using RedHat...\nIt's old news that KDE crashes easily on RedHat, caused by bad packages. RH is GNOME-centric and doesn't care about KDE."
    author: "Andy Goossens"
  - subject: "And that is..."
    date: 2002-03-14
    body: "TRUE! So true I changed almost all my kde rpms from rh's ones to mdk ones (kept some kdemultimedia and arts because mdk uses alsa).\nThe worse problem is with -devel packages that misses some files or have differences from normal kde packages. I could not compile xmms-kde and other kde apps for a looooooooong time."
    author: "protoman"
  - subject: "Re: And that is..."
    date: 2002-03-14
    body: "I have also not been able to compile KDE apps on mdk for a long time, and I DO\nhave all the devel rpms properly..some kind of thing with QT:speech API\nfunction missing when running the configure scripts...\n\nJust never realized why, and installing new versions of the rpm packages just\ndoesn't help...anyone have a solution?\n\nI guess this is totally off subject...sorry guys,\nbut it was an important question to me..\n\n/Erik"
    author: "Erik Unemyr"
  - subject: "Re: And that is..."
    date: 2002-03-15
    body: "yeah, all true\nsome packages are even wrong at all: their kdevelop doesn't work, it has been badly patched.\n\nbut i have to say that the quality improved lately, though i think they're still not packaging KDE with all the seriousness they should. Bero is doing hard work on this, but he should listen to comments people send him.\n\nif you look for KDE/QT packages for RH, have a look either at http://www.freshrpms.net/ or http://www.nikosoft.net/rpms/ that have a few of them."
    author: "anonymous"
  - subject: "Re: Told you so : )"
    date: 2002-03-14
    body: "Well, let's see:\n\nI saw somewhere that something like half a billion people now have internet access.\nIf we accept some of the pessimisitic estimates of Linux use, which come in around 2% of the total, that makes for about 10 million users.\n\nMay not be a big percentage, but that's a buncha people."
    author: "Dean Pannell (a.k.a. dinotrac)"
  - subject: "That, or..."
    date: 2002-03-13
    body: "Or maybe KDE users have more Linux questions than GNOME users, which would be a bad sign. Can't blame our documentators though, they've been doing a great job.\n\nBut seriously, those are nice numbers. They don't mean a lot, but nice."
    author: "Rob Kaper"
  - subject: "Re: That, or..."
    date: 2002-03-13
    body: "Actually i think its from Linux Format Magazine."
    author: "Craig"
  - subject: "Re: That, or..."
    date: 2002-03-13
    body: "You are right. See the same news here:\nhttp://www.linuxformat.co.uk/modules.php?op=modload&name=News&file=article&sid=372"
    author: "Andy Goossens"
  - subject: "Nope, not linuxformat survey."
    date: 2002-03-13
    body: "Nope, that is just a copy/paste job into linuxformats news section. linuxformat did there own survey a while back, not sure if they released the results yet but you can see here http://linuxformat.co.uk/awards/ that the wuestions are different and it is not the same survey.  Personally i'd never heard of linuxquestions.org before these results."
    author: "dave"
  - subject: "Re: That, or..."
    date: 2002-03-13
    body: "I guess this is a European site. If an American site does this poll, we'll get different results...\n\nBut hey, it's good to know KDE is appreciated :-)"
    author: "Andy Goossens"
  - subject: "Re: That, or..."
    date: 2002-03-13
    body: ">> If an American site does this poll, we'll get different results...\n\nback it up with some proof.\n\n"
    author: "me"
  - subject: "Re: That, or..."
    date: 2002-03-13
    body: "It can't be European.\nRH won, not SuSE.\n\n(No proof, sorry :)"
    author: "J.A."
  - subject: "Re: That, or..."
    date: 2002-03-13
    body: "It's a US based site, just look up the domain for god's sake...  That's why the author was surprised KDE won that strongly.\n"
    author: "KDE User"
  - subject: "newbie see newbie do"
    date: 2002-03-13
    body: "well, from my experience of the site, it's just that most people that go there don't know gnome even exists, let alone blackbox et al. everyone knows kde is icky in the real world ;-)"
    author: "trevor"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-13
    body: "You call Blackbox a Desktop Environment? Hah! Good one.\n"
    author: "tritone"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-13
    body: "I'm nowhere near a newbie; I've tried Gnome several times over the years and always it comes back to the same thing: Gnome is pretty, but KDE seems to have the integration I desire down far better and it looks \"more professional.\"\n\nI have trouble explaining that last part.  It's very personal and very circumstantial, but to me KDE feels like it belongs in an office, where Gnome belongs on a home desktop.  Maybe it's something to do with the icons, I don't know.  I know that you can make the icons smaller, but Gnome's iconsets seem to thrive on being large.  KDE has gon the opposite way: their icons default small and look good small.  Gnome definately has speed advantages since it doesn't have to deal with the C++ relocations that KDE does, but with glibc2.2.5 and gcc3.0.4 things are really improving on that front.\n\nAs far as KDE being icky in the real world I have to disagree.  While I don't like kwin at all (I use WindowMaker), the rest of KDE is very very well thought out and seems to work very well with each other.  I use OpenOffice over KOffice because it's more stable and works flawlessly with MS Office file formats (reading and writing), but KOffice has some very nice features (including vastly superior printer support and font handling onscreen) -- I wish I could get the features that KOffice and OpenOffice has into one office set; that would really be nice."
    author: "Andrew"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-13
    body: "I think it would be a better effort to try to put a KDE interface on top of the Open Office code. How feasible would this be and would it take less work than improving KOffice?\n\nAnyone?"
    author: "Office user"
  - subject: "Please!"
    date: 2002-03-13
    body: "KOffice is pretty good. It has got as many features as OpenOffice, but it is quite usable.\n\nWhen will people stop trying to unify OpenSource projects!!!\n\n(Plus I remind you that OpenOffice is not GPL!)\n\n--\nDaniel \nhttp://www.ondelette.com/indexen.html - wavelet forum"
    author: "Daniel Lemire"
  - subject: "Re: Please!"
    date: 2002-03-13
    body: "Well, not really. Think footnotes, think about the number of supported formulae in KSpread, think filters. On the other hand, KOffice has a great foundation and one can only dream of what it could become given enough developers. Seriously, there are maybe 10 developers working on KOffice. They need help."
    author: "KOffice fan"
  - subject: "more releases.."
    date: 2002-03-13
    body: "If KOffice would release more often, it would receive more attention. Sure, it may only be one or two new minor features in each component but still it would be appreciated and increase KOffice's exposure.\n\n<kinda-rantmode>With a release schedule of 1 release every (off the top of my head) 5 months KOffice barely has any exposure at all. Had the developers chosen to have a minor release planned for KDE3.0 they could have gained a hell of alot of attention! But no... /me shakes head in dismay. </kinda-rantmode>\n\nAnyway, best of luck to the KOffice developers. Your work is crucial to KDE sucess.\n\n(don't reply and mention OpenOffice. Yes, it's good. But it doesn't blend in well with KDE. Not to mention that it is SLOW)"
    author: "yatsu"
  - subject: "Re: more releases.."
    date: 2002-03-13
    body: "releasing often is just not possible with that few developers i fear.\n"
    author: "ik"
  - subject: "Re: Please!"
    date: 2002-03-14
    body: "> Plus I remind you that OpenOffice is not GPL!\n\nNo, but it's LGPL.  So what?  \n\n(see http://www.openoffice.org/FAQs/main_faq_new_p4.html#17)"
    author: "cm"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-13
    body: ":: How feasible would this be \n\n    It would take a near complete rewrite and serious kludging of the existing code - the resulting source would be neigh impossible to read.\n\n:: and would it take less work than improving KOffice?\n\n    No.  KOffice is not that far from OpenOffice.  Its biggest problem has been rewriting a core component (trying to find a fast, stable, flexible core for KWord) and printing.  It looks like the third time is the charm, and the new KWord core code is solid, and will continue to be supported, as it inherits quite a bit from new Qt stuff, and a solution for printing across KDE has been found.\n\n    Repeat it too often, and it starts to sound like a cop-out, but if you've been around KDE long enough you'll hear the phrase \"the stuff in CVS is what you're looking for\".  In KOffice's case, that's pretty accurate.  This next release is going to be the equivelent of what MS Works was a few years ago (before they replaced it with Word): a capable suite of applications suitable for nearly all home or small office needs[1].  Unless you're doing mailmerges or serious external data-driven spreadsheets, you'll have what you need.  And those will probably arrive in the next version.\n\n[1] Minus one application that probably in hindsight should have been put off - a database.  The lucky thing is that now that Qt is data aware with version 3.0, so all of KDE just got much more structured and database capable.  A good database app written now will be much cleaner than one written for 2.0, and then rewritten or trashed for 3.0 concepts.\n\n    Incidently, one of the biggest red herrings is \"MS Office compatability\".  I've been using KWord with associates using Word.  KWord is very capable at importing business style documents (i.e., no clip art sprinkled through the document).  I save as RTF, which Word can read easily.  In business correspondance, I had to flip to abiword to open only one Word document ever (and that was a document that somebody had thrown a whole bunch of form widgets into like text boxes and dropdowns).\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: newbie see newbie do"
    date: 2002-03-14
    body: "Having said that, OpenOffice doesn't have a database either."
    author: "KOffice fan"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-14
    body: "Yes, and neither does MS Office (it's one of the things added to make MS Office Professional).  Having said that, the \"classic\" office package back when you had a wide varity of choice was a Word Processor, Spreadsheet and Database (and sometimes an Image program).  To those are now added a PIM (and light groupware) and Presentation program.  Those tools cover the entire needs of 90% of businesses and people, other than a few niche apps - like Point of Sale terminals and Insurance quote programs - both of which could be built upon a good Database app.\n\nAnd that last reason is why I think a good database app would be nice.  But I also hold that the best time to start such an endevour is post-3.0 (which is basically now).  And no, I'm not going to do it - for one thing, I don't need it, or know anybody who really does to give feedback, and that's a bad problem when writing a program.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "What do you mean, \"data-aware\"? Qt 3 has a sql module, but I don't understand what data-awareness would mean. (A db lib for KDE 2 was written, but later trashed as you said would happen.)\n\nJason"
    author: "Jason Katz-Brown"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "I mean just that - the database classes in Qt.  There is QDataBrowser, QDataView and so on.  Now, I have yet to test it myself, but I have been led to understand that you can attach any widget (such as textboxes, scrollbars, etc) to datasources (through Signals and Slots, I would imagine), rather than collecting all the information, translating and storing it with fixed, explicit code.  (Anybody who has used Qt3's database features out there, please comment!)\n\nRegardless, abstration of datasources, XML and SQL in KDE will allow coders to develop at the higher end and add new connectors at the lower end.  This will made a KDE db application, like Konqueror, just a framework that loads views and interconnects datasources through some simple scripting.  Python anyone?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "I've not actually written any code using Qt3's DB stuff, be we looked at it as a driver layer for Rekall.\n\nWe concluded that while its probably fine for use in a dedicated application (eg., the code explicitely knows about the tables), it just does not provide enought functionality to use for a general-purpose database front end (like Rekall or Abcess). In practice, you'd end up using it simply to pass SQL queries to the RDBMS and to get back the results. The classes like QSQLCursor are just too limited. Indeed, as it stands at present, you cannot write a gernal purpose front end without coding server specific information into the front end (ie., the front end needs to know whether the RDBMS is MySQL, Postgres, etc., and act accordingly)\n\nOf course, you might consider this claim a call to arms :))\nRegards\nMike\nhttp://www.thekompany.com/projects/rekall\n"
    author: "Mike Richardson"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "Personally, I would like to see KWord save natively in XHTML (w/compression option) so that all of its documents could be displayed by _any_ Web browser. This is a superior file format and would actually put us a leg up on MS Word which requires the extra step to save to HTML. \n\nUsing XHTML and CSS would be better than using XML since not nearly as many people are familiar with the syntax. Open Office (whose spell checking I have never been able to get working) should be using XHTML instead of XML. Both are extensible but XHTML is more common."
    author: "KHTML"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "XHTML is a subset of XML. There are strucutures and data that you would want in a document that XHTML cannot represent. However, using a good transform you could automatically turn an XML Kword file into XHTML. That would be a much cleaner way of doing things.\n\nKeeping the conversion apart from the file format gives you good abstraction and it's a lot easier to maintain. If the XHTML specs change you just change the transform and not the Kword XML file format.\n\nThe whole point of XML is that you can easily defined rules to convert it. As for the syntax of XML: it's self descriptive. You read the DTD and you can understand exactly what it represents and how to validate an XML file. \n\nXHTML is just XML with a particular DTD. The world wide web consortium has pages decribing this www.w3c.org and there are plenty of good books on the subject. \n\nCSS is a good idea, keep the display details apart from the content. However CSS is *not* defined using XML and it's syntax is horrible. When it gets replaced with a good XML based definition then things will improve. \n"
    author: "Nick"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "That is not the same!\n\nThere is a big difference about a file that you could display in any browser and a file that you *could* transform to HTML.\n\nAs for KWord's DTD, it is not as self-descriptive as you are assuming it, for example <FORMAT>.\n\nAs you are telling, XHTML is well described! That is the biggest advantage! New developpers will not need to learn KWord's DTD but could have better documentation. They would only need to learn the private extension of KWord, the rest would be XHTML (modular.)\n\nI had tried to tell that back in 2001, but I did not managed to get my position understood.\n\nAs for CSS, it will never be defined in XML, as it purpose is exactly to remove such information from XML or (X)HTML."
    author: "Nicolas Goutte"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-16
    body: "Well, for one thing, when XHTML was designed, much of HTML's features were moved to CSS to help bring HTML back to it's original purpose, as a language to describe content in a way that seperates it from page layout. However, KWord, being a DTP, is more suited towards having these in the same unit, and CSS isn't XML anyways. Also, word processing requires control and expandability of many aspects that no version of HTML/CSS has. Some of these things are:\n\n* Fine-grain image placement : Thats in CSS only as of XHTML 1.0\n* Margin and coloration control : Also in CSS\n* Typeface and properties control : You guessed it, CSS\n* Embedding : KWord has the abilty to embed any properly coded KPart into it's document. XHTML has no such generic insertion mechanism, aiui.\n* Frames : KWord's DTP capabilites allows for framing, that is, dividing a collection of paragraphs arbitrarily and automatically into multiple connected areas. This is good for multi-colum pages, etc. XHTML has no such capability, last I checked.\n\nI'm fairly sure that there are other things that should be on this list that I'm forgetting. What all this means is, XHTML is not a suitable general purpose DTP or WP language. Exporting to XHTML/CSS is a good idea for web development (certainly, exporting valid XHTML would be a pleasant change from what Word does) but with a document of any complexity, you're probably going to lose, at the very least, some of your editing capabilities.\n\nUgh, and using propreitary XHTML extensions is a horrible idea. That would require reimplementing much of what CSS is already designed to do with XHTML, and that would be tantamount to a complete fork of XHTML. At that point, the standardization (which was the point of using XHTML in the first place) goes 'Whoosh!' down the drain.\n\nWhat would (and IMO should) happen to make something generic and standardized possible is if the various OSS office suite developers were to agree on a generic XML based format. This isn't too likely to happen any time soon, though, as most of the office suites are still working on feature impelementations (Abiword, KOffice) and/or code cleanups (OpenOffice), and changing the core file format would core changes that at this stage in development, would be a complete PITA to implement."
    author: "Carbon"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-16
    body: "As you are writting, what you cannot do in XHTML, you can do it in CSS.\n\nThe enhanced (former \"style\") mode of KWord's HTML export filter shows the way.\n\nFrames can be done using <DIV> elements and by placing them with CSS. It is the way that I plan to use for KWord's HTML export.\n\nThe only thing so far that I have found that do not exist in XHTML are variables. But that would only mean one or two private attributes (which type of variable and which format) in a <SPAN> element.\n\nXHTML modular explicitely allows private definitions and if you do not use them, you have only XHTML. And even if you use them, it does not mean that the result becomes not \"displayable\" in a browser.\n\nTherefore the problem is not the file format itself."
    author: "Nicolas Goutte"
  - subject: "Re: newbie see newbie do"
    date: 2002-07-30
    body: "testing"
    author: "Jaf"
  - subject: "Re: newbie see newbie do"
    date: 2002-07-30
    body: "Thank you for posting suspicious code here.  I will keep my eye on you."
    author: "Navindra Umanee"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-15
    body: "I already had this idea in 2001. It was rejected by David Faure."
    author: "Nicolas Goutte"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-16
    body: "What was the reason?\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: newbie see newbie do"
    date: 2002-03-16
    body: "The reason given by David Faure was that it did not want to use CSS.\n\nIn the meantime, I have also found out that the way how KWord saves and loads its files is not in favour of a change to XHTML + CSS.\n\nPart of the work has to be done any way for KWord's HTML export (enhanced mode). The enhanced mode (\"style\" in KWord 1.1) writes the files using XHTMl + CSS.\n\nHave a nice day/evening/night!"
    author: "Nicolas Goutte"
  - subject: "Re: newbie see newbie do"
    date: 2002-03-29
    body: "That's a problem. KDE rocks, but monopolies are never good. Not for a long time."
    author: "jakobk"
---
<a href="http://www.linuxquestions.org/">LinuxQuestions.org</a> has concluded
a vote on the Linux desktop, and KDE has once again had a great showing.
It came in first in the <em>Desktop Environment of the Year</em> category
with almost 69% of the votes.  In addition, KDE standouts
<a href="http://www.konqueror.org/">Konqueror</a> tied with
<a href="http://www.mozilla.org/">Mozilla</a> (22%) as <em>Browser of the
year</em>; <a href="http://devel-home.kde.org/~kmail/index.html">KMail</a>
took top honors as <em>Mail Client of the Year</em> (beating
<a href="http://www.ximian.com/products/ximian_evolution/">Evolution</a>
by a large margin); and <a href="http://www.koffice.org/">KOffice</a>
earned second place (substantially behind
<a href="http://www.sun.com/staroffice/">StarOffice</a>) as
<em>Office Suite of the Year</em>.  Check out the
<a href="http://www.linuxquestions.org/questions/showthread.php?s=&threadid=15903">complete
results</a>.

<!--break-->
