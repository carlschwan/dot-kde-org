---
title: "APPS.KDE.com, Other Sites Back Online"
date:    2002-10-14
authors:
  - "Dre"
slug:    appskdecom-other-sites-back-online
comments:
  - subject: "Woohoo!"
    date: 2002-10-13
    body: "I've missed Appsy. Thanks for your hard work Dre! :-)"
    author: "Timothy R. Butler"
  - subject: "hoowoo!"
    date: 2002-10-13
    body: "Thanks for all your hard work and dedication Dre!"
    author: "Navindra Umanee"
  - subject: "Re: hoowoo!"
    date: 2002-10-13
    body: "Quit tooting your own horn, Navindreas."
    author: "Charles Samuels"
  - subject: "Re: hoowoo!"
    date: 2002-10-14
    body: "Whatever you say Charles Stevens!"
    author: "Navindra Umanee"
  - subject: "Memory Tester"
    date: 2002-10-13
    body: "I highly recommend <a href=\"http://www.memtest86.com/\">Memtest86</a> for memory testing.  Is use it as part of my toolkit when I diagnose problems with computers as well as testing the memory on new computers I've either bought or assembled."
    author: "jmalory"
  - subject: "Re: Memory Tester"
    date: 2002-10-14
    body: "I know this is offtopic, but I have to wholeheartedly agree. Memtest86 finds errors that other testers don't. It has saved me a lot of trouble in the past. And you can read about its testing methodology on the site."
    author: "AC"
  - subject: "Kate"
    date: 2002-10-13
    body: "Where is the home Kate?\nhttp://kate.kde.org doesn't work."
    author: "Somebody"
  - subject: "Re: Kate"
    date: 2002-10-13
    body: "eh ... of Kate"
    author: "Somebody"
  - subject: "Re: Kate"
    date: 2002-10-13
    body: "Use http://www.kde.org/apps/kate/ until it's fixed."
    author: "Anonymous"
  - subject: "Re: Kate"
    date: 2002-10-14
    body: "You can thank Christoph for that.  :-p"
    author: "Navindra Umanee"
  - subject: "Re: Kate"
    date: 2002-10-14
    body: "Beautiful site - I like the new proposed theme.  Kudos to the author.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kate"
    date: 2002-10-14
    body: "Too bad the URL is still broken!   Not a good thing."
    author: "Navindra Umanee"
  - subject: "appsy contrib page broken ?"
    date: 2002-10-14
    body: "It displays horribly (both subframes have scrollbars, horribly inconvenient) in Moz 1.1 and Konq 3.1. Any chance to simplify it a little ?"
    author: "Guillaume Laurent"
  - subject: "Re: appsy contrib page broken ?"
    date: 2002-10-15
    body: "Select \"no frames\" from the yellow menu at the top and everything's alright (that one should be appsy's default view mode anyway)."
    author: "Datschge"
  - subject: "kde-look.org?"
    date: 2002-10-16
    body: "Is kde-look down also?  I not getting thru.\n\nThanks."
    author: "Richard"
  - subject: "http://kde-forum.org"
    date: 2002-10-18
    body: "A little off-topic but visit the new KDE web forum to post messages not related to news stories."
    author: "Anonymous"
---
After struggling for three weeks with a series of hardware anomalies (traced
after much effort to a bad bit in a memory module) and
a taxing OS / software upgrade (as well as with some time-consuming but
unrelated issues), <a href="http://apps.kde.com/">APPS.KDE.com</a>,
<a href="http://www.kde.com/">www.KDE.com</a>,
<a href="http://lists.kde.com/">LISTS.KDE.com</a> (which among other
things provides the <a href="http://dot.kde.org/">dot's</a> mailing lists),
<a href="http://promo.kde.org/">PROMO.KDE.org</a> and
<a href="http://www.kdeleague.org/">www.KDELeague.org</a> are finally
back online.
Service on the KDE.com sites may be spotty in the coming week as some kinks
in the OS and various software upgrades are worked out, but the most important
things appear to be working smoothly already.
Please <a href="http://apps.kde.com/uk/0/faq#faq8">contribute</a> any
recent releases of KDE or Qt software to the Appsy database
to ensure it remains comprehensive, and please be patient while
application updates proceed in first-in, first-out
order.  If you find areas of any of the sites that do not yet work as expected, please
let me know.


<!--break-->
