---
title: "KDE 3.0 Release Plan Update"
date:    2002-01-24
authors:
  - "nstevens"
slug:    kde-30-release-plan-update
comments:
  - subject: "Tab support in Konqueror"
    date: 2002-01-24
    body: "Let me be the first to express my excitement for the upcoming tab support in Konqueror 3.1. I am glad to see this handy feature finally making its way into Konq; perhaps I can finally switch over from Galeon and move to a purer KDE desktop.\n:Peter"
    author: "Peter Clark"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-01-24
    body: "Personally I can't see why you'd want things like \na tabbed browser or a tabbed console. It would be \nbetter to use tabbed windows system wide.\n\nA simple solution would be to maximize windows in \none workspace and select the windows in the shelf\n(correct term?). To make this process faster, I \nthink it would be a good compromise to display \nall windows of the active application directly \nin the shelf (or taskbar or whatever) and keep the \nwindows of all other apps in the hierarchical \nstructure.\n\nIf the shelf is positioned at the left or right edge \nof the screen, the window entries can be indented so \nthey're easily distinguishable fram the application \nentries."
    author: "Jens Tinz"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-01-25
    body: "Is this an idea you came up with, or an idea originated elsewhere?\n\nIf it's your idea, can you make up some diagrams and more exact explainations?\n\nIf it's a borrowed idea, could you point to some diagrams and more comprehensive explainations?\n\nThis sounds very intruguing, and as a fan of tabbed windows, I am very intersted in this."
    author: "Sam Kennedy"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-01-26
    body: "There's a window manager called pwm (http://www.students.tut.fi/~tuomov/pwm/) that's built around having tabbed windows.  You could get some implementation and usage ideas there."
    author: "Buuba"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-01-25
    body: "I agree with Jens.\n\nI use NetCaptor (windows tabbed browser) at work.  I find it very usefull that you can simply flick between different tabs, and open a new tab by simply shift clicking on a link.  The advantage is that I have available all the tabs necessary to perform some function, i.e. surfing during breaks.  However there is no reason why this concept could not be extending in actual work.\n\nSay for instance I get an email saying I have to perform some peice of work, e.g. I have to make a presentation about something.\n\nI could have some master app where all my work derives from, i.e. email/groupware application, which is able to spawn workspaces.\n\nFrom this app, I spawn a new workspace that begins with an email saying \"Hey Konrad can you give a presentation on something...\"\n\nI could then open a bunch of various documents associated with that email.  So all the work associated with that presentation is contained within the workspace, with tabs along the side of the screen, and with headers of each document (i.e. work process) involved in achieving the presentation.\n\nIf somebody interrupts me and asks me to do something else.  I spawn a new workspace.  Once I've finished I simply close it.  And reopen the previous workspace.\n\nThere shouldn't be a fixed number of workspaces to choose from either, you should be able to create as many as wish - dynamically.\n\nSo for instance you could have a horizontal bar, outlining all the workspaces you have open, and a bar above that with the windows you have open in the workspace you selected.\n\nYou could move throught the work you have for the day, by clicking on the workspace.\n\nCurrently tabbed interfaces and MDI's are designed around a particular application.  But it is very rare for me to be in the same application all day performing a variety of tasks.  Usually the only people who do, have taken the time to customize that application to perform a variety of things.  But this should really be the responsibility of the gui."
    author: "Konrad Dear"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-01-25
    body: "Just switch between workspaces for each task and use each taskbar to switch between windows for each task. And don't forget to uncheck \"show all windows\" in the taskbar configuration tool."
    author: "Julien Olivier"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-12-13
    body: "I use tabs in Konsole and Mozilla ALL THE TIME.\n\nI love tabbed multi-session interfaces. They allow me to open many many sessions, and easily hop between them.\n\nIn fact, I like it so much, I get frustrated when I have to do work on a Windows machine, because PuTTy doesn't have tabs. And the windows task bar is a POOR substitute.\n\nI like the idea of an system wide tab architecture, but I don't think it would be appropriate for some applications. Terminal apps, browsers, text editors, and such would all benefit. But what about admin interfaces? Server windows? It would have to be an OPTION for the application programmer, not the rule.\n\nThe greatest strength in a system wide tab architecture would be the uniform use of certain key strokes to switch between tabs. Mozilla currently implements CTRL+PGUP-PGDWN. And Konsole currently uses SHIFT+LEFT-RIGHT.\n"
    author: "Matthas Trevarthan"
  - subject: "Re: Tab support in Konqueror"
    date: 2006-04-13
    body: "I got frustrated working with putty and the taskbar too. There is a program (tabmanager)called WinTabber (http://www.wintabber.com) that can tab many programs like putty, notepad, cmd. This should help you out on windows!"
    author: "tabtastic"
  - subject: "Re: Tab support in Konqueror"
    date: 2002-01-25
    body: "I would just be happy if half the shockwave sites would work. I know that this is a web site problem not a Konqueror, but the authoring siftware from macromedia puts the check in that makes it impossible to view the pages with Konqueror. \n Just my two cents."
    author: "Bryan Ballard"
  - subject: "noatun and kcontrol"
    date: 2002-01-24
    body: "impressive stuff! I have two questions. when will noatun be able to play divx files? (I doubt this is a very intelligent question.. it's just one of those rare things I personally miss in kde! well, actually I don't miss it but it would be nice.) and about kcontrol, I'm normally a debian user (the debian kde packages are superb btw!) and a few days ago I decided to install SuSE, just for fun. I found out it does a terrible integration with the kde control panel, ugly and slow. so I wondered if people have thought about making a standard kde control hardware/software configuration, a sort of high-level layer that can distributions can communicate with. so that, for instance, if I want to install Flightgear, I can go to the software section and type in flightgear, or select it from a list and it will be installed, regardless of my distribution. it could be that apt-get downloads it for me, or yast-online-update or whatever. same thing for hardware detection etc. imho this would be a great addition to kde!"
    author: "mark dufour"
  - subject: "Re: noatun and kcontrol"
    date: 2002-01-24
    body: "Playing divx files and the like is easily done through programs such as aviplay (a part of avifile, search on freshmeat), mplayer and xine. Although there is nothing with a pure KDE interface for the moment, I don't notice that while watching DVDs..."
    author: "Hamish Rodda"
  - subject: "Re: noatun and kcontrol"
    date: 2002-01-24
    body: "playing divx is possible with noatun. You just need to download the DIVX play object for arts from <a href=\"http://mpeglib.sf.net\">http://mpeglib.sf.net</a> install it and restart artsd/noatun again"
    author: "zecke"
  - subject: "Re: noatun and kcontrol"
    date: 2002-01-24
    body: "> I wondered if people have thought about making a standard kde control hardware/software configuration, a sort of high-level layer that can distributions can communicate with. \n\nOf course people have *thought* about it.  Everyone agrees this would be a great thing to have, it has been discussed before.  The problem is, the amount of work involved is prohibitive, plus the distributions might not cooperate because much of their value comes from their third-party configuration tools, which would be rendered obsolete by a KDE native solution.\n\nThe Ximian setup tools come closest to accomplishing what you want.  I haven't heard anything about them in a while though, and the plan to make a KDE frontend for them fell flat on its face (twice).  So I don't know what's going on in that department.\n\nAbout KDE playing divx files:  the recent meeting about aRts was on the topic of video, does anyone know what happened at that meeting?"
    author: "not me"
  - subject: "Re: noatun and kcontrol"
    date: 2002-01-24
    body: "> impressive stuff! I have two questions. when will noatun be able to play divx files?\n\nOn saturday is an irc meeting about video support in arts (see older news on dot)."
    author: "Tim Jansen"
  - subject: "Re: noatun and kcontrol"
    date: 2002-01-24
    body: "I would suggest you install Xine along with it's various plugins especially including D4D to play back DVD's in Linux.\nMake a search for xine and checkout the various sites including the xine site. The xine site will not have the D4D plugin which is necesary to unscramble the CSS encoding on the dvd. Look else where for it using one of the search engines. You can find it somewhere. Another good plug in for Xine is the nav plugin to give you a \"special features\" menu.\nXine does an excellent job of playing your DVD movies."
    author: "Ernest Spencer"
  - subject: "Release Candidate"
    date: 2002-01-24
    body: "I'm happy that now the \"Release Candidate\" will be what it's name suggests. I don`t\nknow which project started this hopefully temporary fashion to call their latest \"Beta\" always \"Release Candidate\"."
    author: "someone"
  - subject: "Cool"
    date: 2002-01-24
    body: "KDE 3.0 and GNOME 2.0 in the same week.\n\nNow, that is going to be fun."
    author: "Carg"
  - subject: "Re: Cool"
    date: 2002-01-24
    body: "I also realised that...\n\nIf KDE and GNOME don't get delayed, they will appear in Mandrake 8.2 when it's released in the end of march.\nI don't use Mandrake (yet), but it's a desktop distro and they might profit from the new KDE/GNOME versions."
    author: "Storm"
  - subject: "Re: Cool"
    date: 2002-01-24
    body: "There is enough time for both to fail their schedule, particular Gnome as it's in an earlier phase."
    author: "someone"
  - subject: "Re: Cool"
    date: 2002-01-28
    body: "Gnome has adjusted their plan, Gnome 2.0 Desktop Final release is now scheduled for 29th March."
    author: "someone"
  - subject: "doh"
    date: 2002-01-24
    body: "And of course, <a href=\"http://www.kde.org/screenshots/kde3shots.html\">check here</a> for some of Rob's screenshots of KDE3."
    author: "n"
  - subject: "Re: doh"
    date: 2002-01-24
    body: "These are rather old ones, from November 2001 so pre KDE 3 Beta 1. And the only new feature shown in the four screenshots is the rewritten KDE printing manager. :-("
    author: "someone"
  - subject: "I don't want to be labeled as a mac weenie but...."
    date: 2002-01-24
    body: "I forget.  Can the desktop menu and the KDE menubar at the top of the screen be enabled at the same time?  That would be great for Mac interface emulation.  I don't think Apple would protest.  (just the Aqua part, though)"
    author: "Dylan Lainhart"
  - subject: "hmm"
    date: 2002-01-24
    body: "I tested the current beta last week and I'm impressed with the bunch of improvements which are small but make you wonder how you could live without them =)\n\nThough, in their strive for easy handling the KDE developers removed some options I greatly miss (perhaps I'm just too stupid to find them - I've got this problem quite often =). You can no longer deactivate the icon bar right of the sidebar in Konqueror and the tree view of the options in the icon bar (bookmarks, audiocd, ...) is not available. A other example is that you can't deactivate the annoying \"Windows\u00a9\"-Bar in the file dialog (you know the links to Desktop, Home, ...) and some others. It seems with every major release KDE needlessly loses some of its functionality wich is then restored in the the following releases (as the configurable window buttons)\n\nFortunately this are minor flaws I'm ready to endure for the best DE out there"
    author: "?"
  - subject: "Could it be true...WebDAV support through KIO???"
    date: 2002-01-24
    body: "- WebDAV support, Hamish Rodda <meddie@yoyo.cc.monash.edu.au>\n\nThis is one of THE biggest things I've been hoping for. If it is true, I will jump for joy (and maybe do a cartwheel or two). A WebDAV i/o slave would allow for all kinds of wonderful Web-based collaberative workflows. I could make Zope (which you're viewing right now) a seamless document repository for KOffice, etc. My ears have definately perked up.\n\nJoy!"
    author: "Eron Lloyd"
  - subject: "Re: Could it be true...WebDAV support through KIO?"
    date: 2002-01-24
    body: "But will it support the WebDAV extension Zope requires to feed the SOURCE to an object and not the rendered object?"
    author: "Anon"
  - subject: "Re: Could it be true...WebDAV support through KIO?"
    date: 2002-01-24
    body: "Zope serves up non-rendered source through the WebDAV port automatically.  Be sure that you're starting Zope with the right parameters, and there's nothing to it.\n\n--- Chris"
    author: "C W Carlson"
  - subject: "Re: Could it be true...WebDAV support through KIO?"
    date: 2002-01-27
    body: "Hi!\n\nHamish developed the WebDAV support with Zope in mind. He tested against my Zope server at iuveno AG, and also against Apache mod_dav.\n\nI am not exactly sure about the full features list, but you will definitely be able to use Konqueror + an editor of your choice as a kind of out-of-the-box IDE for Zope. SSL is also fully supported already.\n\nBTW: The next step is now to make sure that Zope can \"understand\" KOffice XML docs. Think of dynamically generated charts that you can customize in KOffice! We are trying to do the same in OpenOffice, too, which will be more platform-independent. But it should be easy to support KOffice file formats, too.\n\nJoachim"
    author: "Joachim Werner"
  - subject: "OT question"
    date: 2002-01-25
    body: "...I am simply wondering what this line means:\n\"from the should-have-turned-left-at-Tuebingen dept.\"\n\n\nCurious greetings from Tuebingen\n\n\n\nDavid"
    author: "David"
  - subject: "Re: OT question"
    date: 2002-01-25
    body: "Random Bugs Bunny reference, integrating the home of a university that has supported KDE with network resources."
    author: "Neil Stevens"
  - subject: "Request"
    date: 2002-01-25
    body: "Could not they release the 3.0 a day before ? Ill get then a \ngreat birthday present ! ;-)"
    author: "mister_fab"
  - subject: "What about the KDE print bugs?"
    date: 2002-01-27
    body: "I'm really impressed with the improvements in KDE 3, but I think two really serious problems have been ignored. One is the KHTML print bug which repeats the last lines from one page on the top of the next page, the other is the general KDE print problem which bases printer font size on screen font size, causing really huge fonts to appear on all printouts. Both of these problems are documented bugs. Any idea on when they'll be fixed? As much as I like KDE, how good is a desktop environment that can't print properly?"
    author: "hdw"
  - subject: "KDE/GNOME"
    date: 2002-01-28
    body: "Is there is chance that the KDE and GNOME teams are going to be getting together in the future on to sort out things like menu and pixmap settings?  \n\nIt seems really silly to me that I have KDE menus in one place and GNOME menus in another place - and this is the same with pixmaps other little things.  When I, as an enduser, want to put a wallpaper on my desktop it seems like a waste to go searching through multiple locations to find the one that I want.  Make it easy for me and others and we will switch on over."
    author: "csp"
  - subject: "Re: KDE/GNOME"
    date: 2002-02-06
    body: "Don't forget you have a $HOME directory all for yourself. Put all your wallpapers/pixmaps that's specific to yourself in a location of your choice."
    author: "elfy"
  - subject: "6 Feb - no Beta 2"
    date: 2002-02-06
    body: "kghlkjhkljhnmvnbvcytr"
    author: "sdgh"
  - subject: "Re: 6 Feb - no Beta 2"
    date: 2002-02-06
    body: "You should request your money back!"
    author: "someone"
  - subject: "Kasbar"
    date: 2002-02-07
    body: "I kind of liked the idea of the Kasbar.  It bears kind of a resemblance to Enlightenment.  Unfortunately it never quite worked right for me under KDE 2.2.2.  I hope the bugs get ironed out in 3."
    author: "Rob Benton"
---
The <a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">KDE 3.0 Release Plan</a> has been <a href="http://lists.kde.org/?l=kde-core-devel&m=101180254404085">updated</a>,  with the KDE 3.0 Release Candidate 1 (likely to be renamed Beta2 shortly) now scheduled for February 4th, followed by a "deep freeze", and the announcement of KDE 3.0 on March 18th.  A list of <a href="http://developer.kde.org/development-versions/kde-3.0-features.html">KDE 3.0 planned features</a> is available as well as a list of features <a href="http://developer.kde.org/development-versions/kde-3.1-features.html">postponed for KDE 3.1</a>.
<!--break-->
