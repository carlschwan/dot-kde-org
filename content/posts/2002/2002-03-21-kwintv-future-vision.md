---
title: "KWinTV:  Future Vision"
date:    2002-03-21
authors:
  - "Dre"
slug:    kwintv-future-vision
comments:
  - subject: "Rich get back to work!"
    date: 2002-03-20
    body: "I dont want to see anything out of you until you have Xmelegance done!\nI have been lusting over that thing now for months, dont make me come over there and force you to code ;)\n\nAlthough the kickerapplet with my webcam on is tempting too...  Time to check out the source and start hacking.\n\n-ian reinhart geiser\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Rich get back to work!"
    date: 2002-03-20
    body: "It's always good to see roadmaps for applications.\n\nI hope it will remain a true KDE application though, instead of a KDE-enhanced Qt application which QtVision sounds like,"
    author: "Rob Kaper"
  - subject: "Re: Rich get back to work!"
    date: 2002-03-20
    body: "What is more impressive is the source backend though, unlike more RFCs we see on the list this one has a formitable amount of code allready there.  Even some of the hardest parts are done.  It looks like once the backend is complete implementing video frontends should be relitively painless.\n\njust my 2c from the code here...\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Rich get back to work!"
    date: 2002-03-20
    body: "Wow! This is really impressive.  This is the kind on thing I really didn't think we would see on Linux for some time - but this looks great!\n\nKeep up the good work."
    author: "AnCow"
  - subject: "Re: Rich get back to work!"
    date: 2002-03-20
    body: "Don't worry, Rob.  It's going to be a true KDE app.  I've already made that very clear, and I don't think anyone disagrees.  The reason it was called QtVision was that I originally made it with qt-e.  The design was \"close\" to what we wanted for the kwintv rewrite so we decided to use it for an experiment, to see how to evolved as we brought it up to the desired kwintv structure.  So far it's going quite well, and within a month, we hope to use these pieces to assemble the new kwintv.\n\nIn addition, there are 3 other developers.  Mortiz Wenk, original author of kwintv, has joined us and I expect we'll start seeing commits from him soon.  Michael Reiher and Stefan Hellwig also expressed interest in working on this rewrite.\n\nAt least Michael and I have very little time until the beginning of May, so I expect that development will pick up to an even quicker pace then.  Hopefully we can start releasing functional \"betas\" starting this summer."
    author: "George Staikos"
  - subject: "Re: Rich get back to work!"
    date: 2002-03-21
    body: "Where can I find QtVision?\nI like fbtv (framebuffer TV from xawtv package) and \nwould like to try out QtVision.\n"
    author: "Raphael Wegmann"
  - subject: "Re: Rich get back to work!"
    date: 2002-03-22
    body: "It's not a useful application, sorry.  You could check out the code from kde cvs and revert back to the initial checkin, then build it inside qt-embedded.  But basically it barely works.  It only does grab+blt (no overlay) and it doesn't work well for scanning channels etc.  I don't recommend wasting time trying to use it unless you want to finish developing it. :)\n"
    author: "George Staikos"
  - subject: "Good Job"
    date: 2002-03-20
    body: "Looks nice, simple, and easy to use"
    author: "Anonymous"
  - subject: "tv out?"
    date: 2002-03-20
    body: "very cool.\nhow can i get this to work with tv out?\n"
    author: "anony"
  - subject: "Re: tv out?"
    date: 2002-03-21
    body: "This is a TV-in, camera-in application.  TV-out is a whole different game.  Sorry..."
    author: "George Staikos"
  - subject: "Re: tv out?"
    date: 2002-03-21
    body: "A bit off-topic, it would be soooooo nice to be able to enable TV-out without restarting X. Can it be done somehow using a Nvidia card?\n\n\nCiryon"
    author: "Ciryon"
  - subject: "Re: tv out?"
    date: 2004-01-04
    body: "como configurare trabalhar com tv out ?\n\nagradecia que me mandasse um tutorial"
    author: "rui"
  - subject: "Small mistake in the report"
    date: 2002-03-21
    body: "It seems there is a small error in the links to screenshots. The 2 Channel Wizard links go to http://www.kde.com/dot/kwintv/new-channel-wiz2.png and the first one had to point to http://www.kde.com/dot/kwintv/new-channel-wiz.png"
    author: "Andy Goossens"
  - subject: "Re: Small mistake in the report"
    date: 2002-03-21
    body: "fixed.  thanks.\n"
    author: "Navindra Umanee"
  - subject: "Teletext/videotext features"
    date: 2002-03-21
    body: "Hi,  \n\ndoes anyone of you know AleVT?  \nNot a KDE app, but it does everything I need \nfor VT support.  Maybe you could take a look at the \nfeatures to get some ideas (or code?).  It would be nice to \nsee its functionality in KWinTV...\n\nFeatures I've come to appreciate: \n\n- caching of pages, also for multi-pages\n- clicking on the sub page number (e.g. 2/3) \n  allows you to request a certain subpage \n  either from the page cache or live\n  (it cycles through e.g. 1/3, 2/3, 3/3); \n  and it *stays* at that sub page\n- if you click on any number that looks like a \n  page number you're taken to that page (think hyperlink)\n- the mouse wheel is used for paging up and down\n- page history (right mouse button goes back)\n- top text support\n\nCheers, \ncm.\n"
    author: "cm"
  - subject: "Re: Teletext/videotext features"
    date: 2002-03-21
    body: "I suspect we will use something like this for our teletext/videotext/closed captioning support.  I haven't tried it so I don't know exactly what it can do, but we'll be sure to look soon.\n"
    author: "George Staikos"
  - subject: "Reg. Teletext"
    date: 2003-03-20
    body: "Dear All,\n\nCan anyone help me to know the difference between teletext, videotext and closed captioning.  And I also want to know the format and transmission of Super Teletext.  Please help me.\n\nThanks And Regards,\nAnand G."
    author: "Anand"
  - subject: "Why KWinTV?"
    date: 2002-03-21
    body: "Why is it called KWinTV and not just something like KTV?  Isn't this kind of close to WinTV, a copyrighted program?  Also the Win part just makes it sound like we are just copying some Windows based program.\n\nSorry to nitpick.  Just my two cents."
    author: "adam"
  - subject: "Re: Why KWinTV?"
    date: 2002-03-22
    body: "You would have to ask Moritz Wenk that.  He wrote the original one.  I think it was called \"ktv\" at one time too.  I don't know....\n\nI just took over maintenance for a while. :)\n\nRegardless, I don't think Hauppauge would complain.  For one, this is positive advertising for them since it makes their brand name more popular (and they do nothing on the linux front).\n\nFor two, KWinTV is a TV application that runs under KWin so I think we're ok too. :)  But I'm not about to test something like this in court.\n"
    author: "George Staikos"
  - subject: "Re: Why KWinTV?"
    date: 2002-03-27
    body: "Good point, using \"win\" isn't a good idea. But why post an article over somenting like a name?"
    author: "Adam"
  - subject: "mabey i've been smoking to much crack"
    date: 2002-03-21
    body: "How about a program that with the help of a TV program listing would determine what shows you watch on a regular basis and then automaticly searches through up comming shows and notifies you when something you might be interested in watching is on.\n\n"
    author: "adam"
  - subject: "Re: mabey i've been smoking to much crack"
    date: 2002-03-21
    body: "Well I don't think this functionality belongs inside kwintv, but I do want to have at least 2-3 different plugin points.  I'm considering plugins for input (like IR remote), and for channels (listings, etc, and for other channel file formats).  This would make an interesting plugin.\n"
    author: "George Staikos"
  - subject: "Re: mabey i've been smoking to much crack"
    date: 2002-03-21
    body: "KWinTV is one of my favorite apps on KDE, implementing the mentioned features will make it a Killer app. It will be a  murderous app when the device plug-in architecture allows for video streams to come from file (like Aktion,MPlayer) and also from WebCams and even better DV-out devices on firewire ports (like Kino). Is the framework flexible enough to handle this? (Rename the app to get rid of the Win as well as the TV bit and call it KScreamer!) \n\nDoes KDE have a generic codec repository, etc? "
    author: "Rob Olsthoorn"
  - subject: "Re: mabey i've been smoking to much crack"
    date: 2002-03-24
    body: "You mean like digiguide (www.digiguide.com)?\n\nI wouldn't want this bound to a TVcard app - it's part of my life scheduling\nstuff!  Being able to have the pop-ups change channel for you (if you wanted)\nwould be nice, I guess (and good for digital video recording, of course).\n\n-- Peter"
    author: "Peter Jones"
  - subject: "nice work :-)"
    date: 2002-03-21
    body: "nice work, best of luck in to future. you might consider taking a look at gstreamer at some point in time (sooner rather than later would probably be more useful :-) because some features would be really nice to have -- for instance, a togglebutton that would turn on 'capture' or recording for what's on the screen, etc. such changes would be relatively painless to implement with gstreamer. ronald bultje has done some interesting work with gst-record in gstreamer cvs -- http://gstreamer.net/cvs/ -- while it's written in gtk, the backend stuff is toolkit-independent.\n\njust a thought. best of luck with this project.\n\nwingo"
    author: "andy wingo"
  - subject: "ummmm.... no"
    date: 2002-03-21
    body: "the best thing to do would be to see if arts can do it allready.\n\ni know it can record audio allready, i also know that there are developers working on better video support.  we dont need to keep re-inventing the wheel when we allready have a good start. afaik captureing the video is very easy with the v4l api, it is compressing the bugger into an mpeg or avi that is the painful part.  i am sure one could use a pipe to an exsiting encoder to do this but i am not sure about the details.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: ummmm.... no"
    date: 2002-03-21
    body: "We will definitely use some sort of backend for video capture.  The old code made this somewhat difficult to do well, but we have a better design for the new code.  I suspect we will again use plugins for this kind of thing, so that we could implement various backends such as gstreamer, ffmpeg (another much requested backend), arts, etc.\n\nIt doesn't make sense to write compression/encoding backends anymore.  I think this much is pretty obvious.\n\nAs for storing stills, Qt does a nice job so I think we'll stick with that.\n"
    author: "George Staikos"
  - subject: "Re: ummmm.... no"
    date: 2002-03-21
    body: "Hi George,\n\nJust to offer a bit of perspective, GStreamer actually wouldn't be all that useful as an output plugin. It's more of a way to make an application; the v4l stuff would then be internal to the gstreamer pipeline. Your Qt widget would actually embed an XWindow provided by a gstreamer element for the video, and you could even output sound to artsd if you like. But there's no real way to insert video that your app already has internally to a gstreamer pipeline. It's better to keep it all in gstreamer in the first place.\n\nAlso, GStreamer doesn't offer any new codecs (other than i guess tarkin, which other projects haven't picked up yet -- it's not exactly usable at this point though :) -- it's a framework. we have elements for ffmpeg, mpeg1 and 2, open quicktime, dv, mjpeg, rtjpeg, everything supported by avifile, and that's just on the video side of things.\n\nYou could still offer the ability to choose output and encoding methods with GStreamer, rather easily i might add, but it should probably remain internal to a GStreamer pipeline.\n\nbest regards,\nwingo."
    author: "andy wingo"
  - subject: "Re: ummmm.... no"
    date: 2002-03-22
    body: "We will not be making an application that requires other applications in order to work.  I dont' have gstreamer... and I don't want it.  Why should I have to have it in order to use kwintv?  V4L is all of a few thousand lines of code for full support anyways.\n\n"
    author: "George Staikos"
  - subject: "Re: ummmm.... no"
    date: 2002-03-22
    body: "i'd have left the thread as is a while ago, but misconceptions keep popping up -- gstreamer is not an application.\n\nit's a library that can be used to make applications. your situation is another example of the 'why should my app have to depend on anything at all' problem -- one just has to assess the advantages and disadvantages of lib dependencies versus wheel reinvention. i'm sure that your v4l stuff works great, that's not in doubt. gettting back to my initial post, i just said that if you wanted an extensible solution, for instance for easy pvr behavior, you might want to look at gstreamer. that's all.\n\nwingo."
    author: "andy wingo"
  - subject: "Re: ummmm.... no"
    date: 2002-03-21
    body: "you've expressed a preference for arts, which is of course fine. the technical, let's-not-reinvent-the-wheel part of your argument is, however invalid i think; it is gstreamer that has the wheel made, and arts that is remaking it. the storage and compression of the video stream is a solved problem, and you'll find that raw video requires much more metadata than pipes easily allow for.\n\nchoosing some other solution might be perfectly valid, but not for the reasons you have given, other than any existing integration of some other system with the app.\n\ncheers, wingo."
    author: "andy wingo"
  - subject: "no?"
    date: 2002-03-21
    body: "Hey, my name here?\n\n[quote]\nafaik captureing the video is very easy with the v4l api, it is compressing the bugger into an mpeg or avi that is the painful part\n[/quote]\n\nv4l capturing is easy :-). See the v4l API docs.\n\nThe point in video capturing is indeed the encoding to as many formats as wanted and all that. That's the hard part. That's why I chose to use gstreamer as my framework, because they offer a complete solution to this. You don't need to use gstreamer for capturing per s\u00e9, but it'd probably be a good idea to at least don't reinvent the wheel for encoding. I hope artsd (this will be your media framework?) or kWinTV will not try to reinvent a complete framework for capturing to as many formats as possible etc, that's exactly what gstreamer's core is trying to do (and they're doing a pretty good job). It's independent of Gtk, Gnome and everything else, so... I hope there can be cooperation, it'd definately be a good thing. Try looking at gstreamer, it's worth at least a read :-).\n\nB.t.w., do you guys have a kwintv chat/mailinglist somewhere for discussions?\n\n[quote]\ni am sure one could use a pipe to an exsiting encoder to do this but i am not sure about the details\n[/quote]\n\nThat's hard. The more formats you're gonna support, the more annoyed you'll get because of the differences between the encoders and all the work required for this, which is exactly why you should give gstreamer a chance, it solves this problem.\n\nAbout kWinTV as such: nice job, especially the device selection and channel detection part (from the shots I saw), exactly what I wanted gst-recorder to do as well (it's still under development, some parts are there, some aren't). That's how good applications are supposed to work!"
    author: "Ronald Bultje"
  - subject: "Re: no?"
    date: 2002-03-21
    body: "I've tried different programs to record video, but everything i found was not useable at all (too slow, no a/v-sync, lost frames, own fileformat, ...)\nI've written an new prorgram for recording, encoding and converting Video in different Formats. It consists of a frontend for the captureing and a library for handling audio and video files. Io to fileformats and codecs are implemented as modules - new formats can be added very easy.\n\nSo if this is helpfull for this porject, you can get it at\n   http://merkur.2y.net/av_convert"
    author: "Christian Tusche"
  - subject: "Re: no?"
    date: 2002-03-21
    body: "that's what gstreamer does too ;-)"
    author: "Ronald Bultje"
  - subject: "More information can be found here:"
    date: 2002-03-21
    body: "If you want more information, you can look on these sites...\n<br><br>\n\n<a href=\"http://www.kwintv.org/\">http://www.kwintv.org/</a>\n<br><br>\n<a href=\"http://mail.kde.org/mailman/listinfo/kwintv/\">http://mail.kde.org/mailman/listinfo/kwintv/</a>\n<br><br>\nIf you have questions about the rewrite, please forward them to the kwintv mailing list so that the other developers can see them too.\n<br><br>\nThanks!"
    author: "George Staikos"
  - subject: "What about OSD - menue for remote control via ir"
    date: 2002-03-21
    body: "I'd like to see a nice OSD menue overlayed the tv-frame to select a channel or some options from KWinTV with a remote control. Every good tv-set has such a menue so why not KWinTV.\n\nSee the images on this site to understand what i mean:\nhttp://www.cadsoft.de/people/kls/vdr/software.htm\n\n\nRegards buhzi"
    author: "buhzi"
  - subject: "Re: What about OSD - menue for remote control via ir"
    date: 2002-03-22
    body: "That's on our TODO list already.\n"
    author: "George Staikos"
  - subject: "only TV window"
    date: 2002-03-21
    body: "Hi,\npersonally only what I need to view television is window with TV.\nNo icons, no menus and i think there is no need for bar with minimize,maximize and close buttons :-)\nIt would be nice if there will be config where i can put coordinates where\ntv have to be when starting.\n"
    author: "Milan Svoboda"
  - subject: "Re: only TV window"
    date: 2002-03-21
    body: "That's why we included the option to turn all the decorations on and off (there's a single 'hide all the fluff' option). Allowing you to specify the window position is possible already using the commandline switches, but I agree having this in the config file too makes sense.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: only TV window"
    date: 2002-03-22
    body: "Does that option also turn off window manager's stuff? (bar with minimize, maximize, help, close buttons).\n\nMilan\n"
    author: "Milan Svoboda"
  - subject: "Re: only TV window"
    date: 2002-03-22
    body: "Not at the moment, but there will be a way to do that eventually.\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: only TV window"
    date: 2002-03-30
    body: "I've been messing around with running programs in the desktop, as part of learning kde programming, and I found that this:\n\nkstart --type Desktop kedit\n\nwill start kedit full screen in the desktop with no title bar. Replace kedit with kwintv I guess :)"
    author: "Roger Henderson"
  - subject: "Nice indeed but beware of this..."
    date: 2002-03-22
    body: "This looks incredible. It looks like this could become the Linux killer application for TV. At least it looks as a central place.\n\nAnd here are my concerns/whishlist (sorry, email defunct atm, I'd mail otherwise):\n\nPlease make sure you add support for MHP/DVB. This is a big thing in Europe !\nAt the moment digital cable and sattelite broadcasts are still on DVB but the standard (MHP, which includes DVB) is already final and first devices will appear.\n\nThere is excellent DVB card drivers for Linux available at http://www.linuxtv.org as well as the latest versions of the DVB recorder VDR (which plays DVD and MP3&Co. as well). This company also gets sponsored by NOKIA (MediaTerminal, Linux STB) for doing the MHP Linux implementation.\n\nAlso related to these two companies is http://www.ost-dev.net a NOKIA sponsored OpenSource development-framework for Linux on TVs.\nI mention the latter, since it still is nice to use the TV cards TV out, but this time with an OSD done by Linux, yammi :-)\nThis will be the future in EU.\n\nAlso I see many projects coming up which allow for http-hosted TV programs. Not only the nations have different formats, the publishing TV Guides have their own formats also.\n\nAlso, data-retreival (VT and other) is different for analog and digital streams.\n\nThus I would recommend to make this application as modular as possible.\n\nDCOP is very important ! It allows for many many good things.\n\nA clever plugin architecture would allow for different backends to be installed to get the TV program.\n\nIt would allow for interfaces to 'at' and 'cron' as well as the different cideo disk recorders out there.\n\nI for one have stored my TV program in a MySQL database which gets updated daily. I heard someone is working on an LDAP frontend to the TV program.\n\nSo it would be nice when the Channel and Program listers could iport data realtime from plugins/scripts/DCOP calls.\n\nAlso I would be happy to see not only the channel list being a kpart but also the 'screen' itself. Or did I overread it ?"
    author: "Jester"
  - subject: "zapping"
    date: 2002-03-22
    body: "Hi!\n\nWhile thinking about conversion routines... It would be very nice to be able to share channel list with zapping TV viewer...\n                                                                       Pavel\n"
    author: "Pavel Machek"
  - subject: "FreeBSD"
    date: 2002-03-23
    body: "Will it work on FreeBSD?"
    author: "fura"
  - subject: "Videorecorder functionality"
    date: 2002-03-23
    body: "Hello Richard & George,\n\nNice document, there are a lot of good ideas in it.\nSame counts for the replies, a lot of good ideas.\nEspecially importing TV guide info from web sites\nand setting alarms for your favorite TV programs\nwould be great. One thing I did not see mentioned\noften is videorecorder functionality. I would like\nit a lot if I could use my Linux box with TV card as\na full-featured videorecorder, which wakes up automatically\njust before the to-be-recorded program starts.\n\nBest regards,\n\nEric"
    author: "Eric Veltman"
  - subject: "Nagra support?"
    date: 2002-03-24
    body: "what about support for nagra in kwintv?\nor plugins to do that?"
    author: "fred"
  - subject: "TV Sync"
    date: 2002-03-25
    body: "I don't even have a TV card, but I do have a TV-out on my GeForce. Even if it is way too complicated to switch resolution (desktop size) in X I at least managed to get it working in no time. But - Xine and other players do not seem to sync to the TV correctly. Maybe it's the fault of the nvidia driver, all I know is that it is very disturbing. \n\nMy question to you is: does this KwinTV sync its drawing intelligently when using it in a multi monitor setup? Maybe it's X's job to do this but then it might still be possible to force good sync from the app.\n\nI'd also like too see temporal filtering when \"resizing\" 24/25/30 fps video to 50/60/75/whatever hz output devices :) Even if syncing is perfect there is a very noticeable jitter when just flipping frames on a \"nearest neighbor\" basis. Linear interpolation should be good enough and not eat too much CPU. On fast computers you could probably update the picture at the speed of the monitor sync producing an extremely smooth output. On slower machines you could go down to every second refresh when there is a mismatch like 30->75 or 24->80 and still get an improvement.\n\nOh, another wish to all video player gurus: Please include an option to force software rendering (in RGB space)! Overlays on my card seem to have worse colors, and they also do not use the same calibration mechanism as normal graphics. I also experienced this same fenomena on my old computer - in Linux hardware overlays did not work making the video look much better than in windows!"
    author: "Johan"
---
<a href="http://www.kde.org/people/rich.html">Richard Moore</a> has been
working diligently on a rewrite of
<a href="http://www.kwintv.org/">KWinTV</a>, the KDE
TV-card application.  The "new" KWinTV (screenshots:
<a href="http://www.kde.com/dot/kwintv/new-channel-wiz.png">Channel
Wizard</a>;
<a href="http://www.kde.com/dot/kwintv/new-channel-wiz2.png">Channel
Wizard - Scanning</a>;
<a href="http://www.kde.com/dot/kwintv/_current-channel-wiz.png">Channel
Scanner</a>;
<a href="http://www.kde.com/dot/kwintv/device-selection.png">Device
Selection</a>; and <a href="http://www.kde.com/dot/kwintv/latest-mainwin.png">Main window</a>) will be based on QtVision, a
Qt/Embedded TV application written by
<a href="http://www.kde.org/people/george.html">George Staikos</a>.
He has some really great ideas for future development, such as a panel applet to display a small TV, a TV screensaver, a TV desktop background and teletext/videotex support.  Even better, he
has asked the community for feedback on where to focus his efforts.



<!--break-->
<p />
&nbsp;
<p />
<h2 align="center">The KWinTV Rewrite</h2>
<div align="center">
<a href="mailto:rich@kde.org">Richard Moore</a><br />
March 19, 2002</br>
<small>(edited by <a href="mailto:pour at kde.org">Dre</a>)</small>
</div>

<h3>Introduction</h3>
<blockquote>
<p>
This document is an overview of the changes I have been making to the
rewrite of the <a href="http://www.staikos.on.ca/~staikos/kwintv/">KWinTV</a>
code which <a href="mailto:staikos at kde.org">George Staikos</a>
(<a href="http://www.kde.org/people/george.html">People page</a>) added
to CVS last week.  The rewrite is based on George's QtVision, a Qt/Embedded
TV application which is a basic but functional framework for using Video4Linux
with Qt.
As you will see, I have been pretty busy, so I tried to break things down
into manageable chunks. If you just want the raw data, then skip to the
bottom for links to the latest class documentation and the
ChangeLog. If you have any comments or suggestions please
email <a href="mailto:rich@kde.org">me</a>
(<a href="http://www.kde.org/people/rich.html">People page</a>).
</p>
</blockquote>

<h3>Overview</h3>
<blockquote>
<p>
I have been hacking at the newly imported KWinTV3 (Qtvision) code,
and the changes are becoming significant enough to warrant
some discussion. The main things I have done so far are as follows:
</p>
<ul>
<li>Ported the code to Qt/KDE 3.</li>
<li>Employed the XML GUI framework.</li>
<li> Split the old main window class into two, with the back end being a
  standalone QObject that can be reused.</li>

<li>Written a generic I/O framework for channel files.</li>

<li>Tried to make the code into a collection of reusable objects so that
  it can be split into a library for accessing TV devices and a
  collection of user interfaces that use the library.</li>

<li>Cleaned things up a bit.</li>

<li>Improved the scan wizard.</li>

<li>Added a bunch of kdoc comments to explain how things work to other
  developers (<em>hint!</em>).</li>
</ul>
</blockquote>

<h3>Current Plans</h3>
<blockquote>

<h4>General</h4>
<ul>
<li>At the moment I cannot actually use this app as PAL support seems to
  be broken, so obviously I plan to fix this.</li>

<li>There are still more code cleanups needed.</li>

<li>Write a channel editor.</li>
</ul>

<h4>Channel Wizard</h4>
<p>
There are a number of obvious improvements that can be made to the
channel wizard. At the moment the ones I am planning are:
</p>
<ul>
<li>Get the region data from V2 working using the XML region data
  format.</li>

<li>Extending the GUI to work with the region stuff.</li>

<li>Adding the ability to import old channel files.</li>

<li>Making the wizard look cooler.</li>
</ul>

<h4>Improved Code-Reuse</h4>
<p>
I would like to provide a range of ways this code can be used, the things
that I am tempted to work on at the moment are:
<ul>
<li>A KPart that can be embedded in Konqueror as a viewer for channel files.</li>

<li>A kicker applet and extension that contains a small TV.</li>

<li>A QWidget for displaying a TV.</li>

<li>A screensaver that shows a TV.</li>

<li>A hack that makes the desktop background a TV.</li>

<li>A DCOP interface to the Qt-Vision back-end.</li>
</ul>


<h4>Wouldn't It Be Nice If...</h4>
<p>
There are a number of things my ideal TV application would have:
</p>
<ul>
<li>An integrated TV listings viewer.</li>

<li>The ability to set alarms that trigger when a program is starting.</li>
<li>Teletext (videotex) support.</li>

<li>A good way to channel hop and find what is on.</li>

<li>Knowledge of the stations available in a given area, so that when
  you have found your channels you can just say 'this one is BBC 1' and
  the app will automatically assign the name, icons, a link to the
  website, listings etc.</li>
</ul>
</blockquote>

<h3>Detailed Changes</h3>
<blockquote>

<h4>General</h4>
<p>

I have made various cleanups to the code while I have been working on it,
partly to make it work nicely with my other changes, and partly just
to make it easier to work with. I have tried to add kdoc comments to the
part of the code I have worked on to make it easier to understand how it
all works. I have also added a build target that creates a ChangeLog
file from the log recorded by CVS using cvs2cl (see links) so people
can keep track of what is being worked on. To make this useful, please
try to give a decent explanation when you commit changes.
</p>

<h4>Main Window</h4>
<p>
I converted the QMainWindow of the embedded code into a KMainWindow
and ported it to use the XML GUI framework. In addition I have split the
code into two classes making the backend code and the application
shell independent. Finally, I have started re-adding some of the
features of the old KWinTV main window, with the aim of doing it all
cleanly this time.
</p>
<p>
Most of the basics are now in place and working, so it might even be
possible to use the app already. I have re-implemented the fullscreen,
hide all, and LCD channel number features of V2 as custom KActions,
this is much cleaner than the old code and it might be worth using
them in the next release of the V2 code too.
</p>

<h4>Channel File Handling</h4>
<p>
I have written a generic API for loading and saving channel files. The
framework is designed along the lines of KImageIO, and allows new
filters to be added as required. The code is independent of the
Video4Linux code, as shown by the command-line test_channelio tool
which converts between different channel formats.
</p>
<p>
The filters currently working are:
</p>
<ul>
<li>KWinTV 0.8.x channel format version 4 (Read-Only).</li>

<li>CSV format of the original Qt-Vision code (Read-Write).</li>

<li>A new XML channel format (Read-Write).</li>
</ul>
<p>
The only other formats I see a need for are the XawTV format and the
format used by the win32 TV app that came with my Haupage card.
</p>

<h4>Channel Wizard</h4>
<p>
The channel wizard in the embedded version of Qt-Vision could afford to
block the UI, but that is not acceptable here so I have rewritten it to
be non-blocking. I also made the actual scanning code stand-alone so
it should be possible to write a command line version etc. later.
</p>
<p>
I have started adding the ability to use the region the user is in to
speed up the scan. The data for a region is loaded from another XML
file (instead of being hard-coded C++ as in the old KWinTV) so it
should be easy to add new regions. You can test this using the
test_channelscannerregion tool. At the moment this code does not work
as there are problems converting the frequency data I have into
something the V4LDev class can understand.
</p>
</blockquote>

<h3>Links</h3>
<blockquote>

<h4>Developer Documentation</h4>
<p>
I have put a copy of the
<a href="http://xmelegance.org/devel/kwintv3-ChangeLog">ChangeLog</a>,
and the <a href="http://xmelegance.org/devel/kwintv3-docs/">latest
kdoc output</a> on my website so you can take a look even if you do not want
to download the code.
</p>

<h4>WebCVS</h4>
<p>
The code is in the KDE cvs in the directory kdenonbeta/kwintv3/ which
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdenonbeta/kwintv3/">can
 be seen online</a> via <a href="http://webcvs.kde.org/">WebCVS</a>.
</p>

<h4>cvs2cl</h4>
<p>
This is a great tool that builds a ChangeLog file by analyzing the
<a href="http://www.red-bean.com/cvs2cl/">logs recorded by cvs</a>.
</p>
</blockquote>
<h3>The End</h3>



