---
title: "KDE 3.1 Beta 1: Hot off the Servers"
date:    2002-08-22
authors:
  - "tbutler"
slug:    kde-31-beta-1-hot-servers
comments:
  - subject: "Looks great..."
    date: 2002-08-22
    body: "Can't wait to try it out tomorrow.  KDE now looks as good or better that OSX or WinXP right out of the box.  Great work."
    author: "Frank Rizzo"
  - subject: "not in OSX's league yet but better than XP"
    date: 2002-08-29
    body: "Its getting there"
    author: "Luciansk"
  - subject: "Are you serious??"
    date: 2002-09-04
    body: "I know this is a kde site and people will support their desktop, but you cannot be serious when you say that kde looks as good as OSX or Windows XP.  Kde is still far behind both in the look and consistancy of the user interface.  Neither Gnome or KDE will look as good untill X11 is replaced with a better windowing system or a new rendering model totally implemented.  Many of the things that give the edge to WinXP and OSX are things that are just cannot be done with the current Xfree, such as real transparency.  Untill things are changed, no Linux desktop can say they are better than WinXP or OSX.  One thing that bothers me is how blind some of the Linux community is to what the real world wants.  If you are trying to make a desktop that attracts everyday windows users, you'll need to focus on what they want.  When Redhat goes out and tries to add consistency between Gnome and Kde, the Kde people go out and scream about how they are destroying their desktop when all they are trying to do is make a desktop enviroment with some consistency so that users can use the best apps from both desktops.  The plain ignorance of some in the Linux community is what is holding it back from being a major player in the desktop market.\n\n"
    author: "Juan Briones"
  - subject: "Re: Are you serious??"
    date: 2002-09-04
    body: "Well, just to say that I believe KDE 3.1 with keramik style IS nicer than MacOSX and WinXP even if it has less special effects. I also think that Everaldo's icons are the nicest there is on earth. More over, KDE is the most themeable desktop. And no, I'm not saying that because I use KDE. I have no problem to say that the former KDE default style wasn't as good as MacOSX one but was not far from WinXP one."
    author: "Julien Olivier"
  - subject: "Re: Are you serious??"
    date: 2002-09-06
    body: "Xfree *can* handle real alpha transparency, and it is able to hardware accelerate this, before MacosX Jaguar came out.\n\nOn the other hand, MacosX and WinXP still don't have anything near the networking capabilities of Xfree (no, i'm not talking about vnc crap).\n\nBtw, the end-user desktop is won by first winning the corporate desktop. Most non-techie people I know, don't switch an entire OS just because it is more stable, faster, cheaper, prettier. But they will switch, if their company switches to that OS. \n\nIt are things like stability, good networking and administration capabilities, speed, decent apps, sane licensing issues, vendor support, migration from previous software that are important for the corporate desktop. It does *not* matter if it has real transparency.. "
    author: "Anonymous"
  - subject: "Re: Are you serious??"
    date: 2002-09-06
    body: "> On the other hand, MacosX and WinXP still don't have anything near the networking capabilities of Xfree (no, i'm not talking about vnc crap).\n\nThird party software has allowed Windows (sorry, don't know much about MacOS) to have window-level networking capabilties since Windows 3.1. WindowsXP remote desktop sharing (built into WinXP-Pro, and sharable through WinXP-HE), can also be configured to do window (x11 style) as well as desktop sharing (vnc style). \n\n> Xfree *can* handle real alpha transparency, and it is able to hardware accelerate this, before MacosX Jaguar came out.\n\nAnd this has been built into the Windows GDI since Win2k came out, and has been hardware accelerated by Nvidia since the 10.x Detno driver series (which I beleive was the first to have Win2k support). Other vendors also have support (I'm almost 100% sure ATI and Intel on-board chips do), but I don't have any ATI cards anymore to check :(\n\nDon't get me wrong, I love X11, but your facts are basically reverse (anti-Microsoft) FUD."
    author: "fault"
  - subject: "Are you serious??"
    date: 2002-09-04
    body: "I know this is a kde site and people will support their desktop, but you cannot be serious when you say that kde looks as good as OSX or Windows XP.  Kde is still far behind both in the look and consistancy of the user interface.  Neither Gnome or KDE will look as good untill X11 is replaced with a better windowing system or a new rendering model totally implemented.  Many of the things that give the edge to WinXP and OSX are things that are just cannot be done with the current Xfree, such as real transparency.  Untill things are changed, no Linux desktop can say they are better than WinXP or OSX.  One thing that bothers me is how blind some of the Linux community is to what the real world wants.  If you are trying to make a desktop that attracts everyday windows users, you'll need to focus on what they want.  When Redhat goes out and tries to add consistency between Gnome and Kde, the Kde people go out and scream about how they are destroying their desktop when all they are trying to do is make a desktop enviroment with some consistency so that users can use the best apps from both desktops.  The plain ignorance of some in the Linux community is what is holding it back from being a major player in the desktop market.\n\n"
    author: "Juan Briones"
  - subject: "Re: Are you serious??"
    date: 2002-09-05
    body: "Umn, I definatly think that KDE (and GNOME) can look better than WinXP/MacOSX. \n\nRemember that many people hate aqua, and in XP, they revert to the Windows Classic theme (win2000). \n\nNow, KDE and GNOME are both *much* more customizable than XP or OSX out of the box. \n\nX11 doesn't detract from the desktops any. It actually probably helps bunches. Now, it would be nice if X11 would support all of the special-fx capabilities of Quartz and the Windows GUI, but these sometimes actually make the interface *less* pleasing. The new version of MacOSX, Jaguar (10.1), actually scales back some of the effects by default. \n\nHopefully the X Transparency Server gets done soon ;)"
    author: "fault"
  - subject: "Re: Looks great..."
    date: 2002-10-05
    body: "I for one am sick of Keramik already. But there's no better alternative right now. I've been doing some Googleing, but haven't found anything pleasing. I'd love for my Linux desktop to look like this: http://www2.truman.edu/~d2550/desktoppic.jpg\n\nI'm almost starting to believe that XP has nicer themes than KDE/Gnome/etc...\n\nBut no, that's not the reason for switching back to Winblows. I'll continue using Linux. Just with a bit of a jealous look at some people with awesome XP themes..."
    author: "PARENA"
  - subject: "SuSE RPMS"
    date: 2002-08-22
    body: "SuSE RPMs exist in unstable dir all right. Except that some of the dependencies seem broken. libart_lgpl.so.2 sits under gnome2 dir which rpm doesn't know anything about (so --force, but it's ugly), and kdeaddons package seems to be missing.\n\nrpm -Fvh *.rpm\nerror: failed dependencies:\n        libart_lgpl_2.so.2 is needed by kdebase3-3.0.7-0\n        libart_lgpl_2.so.2 is needed by kdebase3-nsplugin-3.0.7-0\n\t.. \n\tlibkateinterfaces.so.0 is needed by kdeaddons3-3.0.3-6"
    author: "jmk"
  - subject: "Re: SuSE RPMS"
    date: 2002-08-22
    body: "you must also install the libart rpm first (rpm -Fvh does not install new packages).\n\nthe internet forgot the kdeaddons package somewhere, it is uploaded now (again?)."
    author: "Adrian"
  - subject: "Re: SuSE RPMS"
    date: 2002-08-22
    body: "> you must also install the libart rpm first (rpm -Fvh does not install new packages).\n\nYep. Wasn't there at the time.\n\n> the internet forgot the kdeaddons package somewhere, it is uploaded now (again?).\n\nThanks."
    author: "jmk"
  - subject: "Re: SuSE RPMS"
    date: 2002-08-22
    body: "I still can't find libart kdeaddins rpms on the site.  \n\nI also get the error:\n\nlibkviewsupport.so.0 is needed by kdegraphics3-3.0.7-0\n"
    author: "ld6772"
  - subject: "Re: SuSE RPMS"
    date: 2002-08-22
    body: "Well, installing kdeaddons on my SuSE 8.0 system means\nI cannot start konqueror for ftp or file-management\nanymore -- it crashes.\n\nWithout kdeaddons, everything works very well, very \nsmoothly. And I _love_ the font preview in konqueror."
    author: "Boudewijn Rempt"
  - subject: "Re: SuSE RPMS"
    date: 2002-09-01
    body: "This seems to be an error in one of the konqueror plugins.\nEither konqlistview (and/)or konqiconview causes this crash.\n\nYou can make a copy of your \"old\" directorys\n[prefix]/share/apps/konqlistview    and\n[prefix]/share/apps/konqiconview,\nthen install the kdeaddon package of 3.1-beta1 and afterwards restore above directories again...\n\nThis seems to work quite well...\n\nivo\n"
    author: "Ivo R\u00f6ssling"
  - subject: "Keramik rox"
    date: 2002-08-22
    body: "It is *really* nice to have a style that is not as boring as the previous ones and is not hard-to-use-each-day-\u00fcbercool-and-wierd as enlightenment styles. In fact it is a really lovely one and one that makes us differ in look from other desktops.\n\nCongrats to the Keramik people.\n\n\n/S"
    author: "Sam"
  - subject: "Re: Keramik rox"
    date: 2002-08-23
    body: "I actually _really_ like the .NET look, and I hope it is still available. I don't like my widgets gettin' all phat on me and taking up space!\n\nAhem. Sorry about the \"gettin' all phat\"... was momentarily overcome by the Ghost Of Aqua Widgets Past.\n"
    author: "Richard"
  - subject: "Than try High Performance Liquid"
    date: 2002-09-04
    body: "Niot only is it more mature than Keramik, (over 1 year in development) but it is also faster, more customizable, completely themes KDE and it looks just as good if not cooler. I really think liquid should ahve been selected as the default. If Keramik didn't leave the gray parts and was a complete theme than, I would consider them equal, but the way it is now and with the *great new scrollbars* I really liek Liquid a lot more."
    author: "Alex"
  - subject: "Re: Than try High Performance Liquid"
    date: 2002-09-04
    body: "Mostly I have a problem with the chunky-looking widgets. I want buttons to be buttons, not screen decorations :)"
    author: "Richard"
  - subject: "SVG?"
    date: 2002-08-22
    body: "Hi, I'm curious about SVG support in KDE. Will KDE 3.1 be able to handle icons in the SVG format?\n"
    author: "Joe"
  - subject: "Re: SVG?"
    date: 2002-08-22
    body: "> Will KDE 3.1 be able to handle icons in the SVG format?\n\nYes."
    author: "Chris Howells"
  - subject: "Re: SVG?"
    date: 2002-08-23
    body: "\nYou sure?  I could have sworn I read in one of the earlier kernel cousins that SVG support was being postponed to 3.2 because not all the features were supported yet.  Something about not including it until it worked properly so that it didn't set peoples expectations wrong.\n\nHowever, KC #43 also reitterates the SVG icon rumour.  Unless one of the developers pipes up to say yes or no, we'll have to wait and see.\n\n\n\n"
    author: "John"
  - subject: "Re: SVG?"
    date: 2002-08-23
    body: "> Unless one of the developers pipes up to say yes or no, \n> we'll have to wait and see.\n\nYes.\n\nTackat"
    author: "tackat"
  - subject: "Re: SVG?"
    date: 2002-08-23
    body: "You're confusing SVG icon support and full SVG support. The code for supporting SVG icons is already in CVS; what was postponed to 3.2 is code for full SVG file support - KSVG; the difference is that icons use only a small set of functionality of full SVG -- they don't use JavaScript scripting and animations, for instance. "
    author: "Sad Eagle"
  - subject: "Re: SVG?"
    date: 2002-08-23
    body: "What about generic SVG viewing? I can't find anything at all about SVG on the 3.1 or 3.2 feature plan pages."
    author: "Richard"
  - subject: "Re: SVG?"
    date: 2002-08-23
    body: "Hi,\n\nthere is a program in kdenonbeta called KSVG,\nwhich is a generic svg viewer (works within Konqueror)\n\nSee http://svg.kde.org for details...\n\nBye\n Bye\n  Niko"
    author: "Nikolas Zimmermann"
  - subject: "SuSE 8 rpm's"
    date: 2002-08-22
    body: "Hi.\nIs anoynone else experiencing problems with the new beta1 SuSE 8 rpm's?\nFor example:\n- I can not configure or display 99% of the usual toolbars, there's no URL field, no back button ...\n- Kmail still has no Aegypten Plugins pre-installed?\n\nThe Alpha1 was working quite good, hope it's just me causing this trouble. :o)\n\nBesides this problems KDE rocks!\nKeep on the good work."
    author: "Marcus Reuss"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-22
    body: "same here so you're not alone. Konqueror is absolutely unusable for web browsing because there's only 5 buttons on toolbar, menus are in wrong order etc. Seems like one or more rc-files are missing from packages.\n\n"
    author: "duuude"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-22
    body: "Also have this problem about no location bar, or missing menu 's in konqueror.\n\nAm i still missing something"
    author: "VenimK"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-22
    body: "Do you also not see HTML based buttons and forms? Like the threshhold in slashdot.org e.g.?\n\nI can't believe how bad these SuSE packages are -- even for a beta. \n\n* Incomplete dependencies\n* broken Konqueror\n* khtml unusable due to missing widgets\n* lacking OpenPGP plugins for kmail\n* KDVI does not work at all (\"No kvierwerpart found\")\n\nI do not know it the beta KDE software is this broken at the moment, or if SuSE didn't do their homework."
    author: "Moritz Moeller-HErrmann"
  - subject: "Weird."
    date: 2002-08-22
    body: "On another machine in the office, the same RPMS result in a usable khtml. Very strange. Maybe it is style dependant?\n\nDoes anyone experience the same problems?"
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Weird."
    date: 2002-08-22
    body: "Hmm, been trying different styles also, but that does no make showing op location bar en the missing menu 's in konqueror"
    author: "VenimK"
  - subject: "FOund the error!"
    date: 2002-08-23
    body: "OK, the reason for invisible HTML buttons and forms seems to have been an obsolete kdebindings3 package (KDE-3.03). After uninstalling it khtml works better than ever (again!! and fast!!!)\n\nUnfortunately I found a new bug: rlan and lan URLS do not work."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: FOund the error!"
    date: 2002-08-26
    body: "Sorry but I cannot confirm this. I have no kdebindings3 package installed and I have the same symptoms as above.\nI uninstalled all of kde before installing kde 3.1 beta1.\n\nBut I noticed that kdegraphics complains about a missing libkviewsupport.so.0.\nThis file is missing since the Alpha version.\n\nCheers,\nLaci\n\nP.S. remove NOSPAM from my email address if replying"
    author: "Laci"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-22
    body: "all *rc files which get's installed by make install are packaged.\n"
    author: "Adrian"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-22
    body: "Is there a fix for this yet?  I really like the beta release but konqueror is still unusable because of the missing icons/menus.\n\nThanks!"
    author: "Anonymous Coward"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-26
    body: "Me too, kde 3.1 beta1 is allmost unusable. Complains about libkviewsupport.so.0 when installing kdegraphics3-3.0.7-0.i386.rpm without --nodeps"
    author: "ComradeTux"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-08-31
    body: "Well, I've tried it and...\nuse kdegraphics3-3.0.6-0.rpm and then...."
    author: "Michael"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-09-03
    body: "I am having the exact same problems.  Because I had the original Suse 8.0 version, I had to update QT3 to 3.0.5 as well, but it still complains about libkviewsupport.so.0\n\nAnyone who knows how to fix this, let me know."
    author: "Howard"
  - subject: "Re: SuSE 8 rpm's"
    date: 2002-09-29
    body: "Normally I hate 'me too', but, well, me too!\n\nI had even more problems as kpackages kept on crashing after I reached half of the installations. So I retarted KDE and that gave me a nice blue screen...\n\nNext I headed to Gnome (can you believe it :-) and did stuff manually using a terminal.\n\nAnd now it keeps on complaining about the same lib you complained about: libkviewsupport.so.0\n\nI will try now to force the install. i just hope the libkviewsupport is only used in some very rare cases...\n\n-- \nRegards,\nMarko"
    author: "Marko Faas"
  - subject: "Features implemented?"
    date: 2002-08-22
    body: "Are all the features from http://developer.kde.org/development-versions/kde-3.1-features.html implemented? Will the red/yellow items be moved over to green at some point?"
    author: "Anon"
  - subject: "Re: Features implemented?"
    date: 2002-08-22
    body: "No. Yes, yellow in KDE 3.1 and red in KDE 3.1 or KDE 3.2."
    author: "Anonymous"
  - subject: "Re: Features implemented?"
    date: 2002-08-22
    body: "Thanks!"
    author: "Anon"
  - subject: "Download manager?"
    date: 2002-08-22
    body: "Oh man, and I was just about to get started on my very own download manager project, because that's one of the very, very few (and constantly decreasing amount of) things KDE has been missing and I thought it would be a good first project to try. Oh well, I suppose I'll have to try to do one myself before 3.1 sharp releases, and not install any of these prerelases before... or I could just see if there perhaps are any features missing that I could try to add to the released version.... :) Oh well.... how often does it happen that a program from main KDE is not fully featured out of the box??? Great work, all you involved. I just hope I can come up with good new ideas, so that I can actually code something myself."
    author: "Mattias"
  - subject: "Re: Download manager?"
    date: 2002-08-22
    body: "One feature that most people wants and the kget don\u00b4t have yet: download from more than one connection. Other features are find and select the best mirror, download a entire directory (pause and resume possible, of course) and, maybe, scan with a anti-virus. Too work to do. The first (more than one connection) is the critical one.\nThanks for the help. The KDE needs more developers."
    author: "Daniel Dantas"
  - subject: "Re: Download manager?"
    date: 2002-08-23
    body: "I'd say the directory grabbing was more important... probably easier to implement though."
    author: "Matt"
  - subject: "Re: Download manager?"
    date: 2002-08-23
    body: "Check out d4x.  Not kde, but I'm sure you'll like it.\n"
    author: "Neal Becker"
  - subject: "My girlfriend likes it,.... "
    date: 2002-08-22
    body: "\n\nShe even asks me to replace her Windows XP with SuSE and KDE3, because KDE3 is \"sexy\" and Windows XP is \"dumb\". -- one detail  -- it is only a year ago when she first started working with computers at all!\n\nThe only things that still confuse her are: manual mounting and unmounting and absence of her Adobe and Macromedia software she works with. "
    author: "SHiFT"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "muaha :-) i really laughed about that"
    author: "ch"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "your girlfriend roxx :-P"
    author: "ch"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "Your girlfriend is damn right ;-)\nYou should install an automounter and make her write Macromedia and Adobe about supporting Linux...\n"
    author: "labrum"
  - subject: "Bad, bad..."
    date: 2002-08-22
    body: "when she start to think that computer or software is sexy ;-)\n\nManual mounting: give her supermounting.\n\nAbsence of Adobe and Macromedia software: vmware.\n\n"
    author: "antialias"
  - subject: "Re: Bad, bad..."
    date: 2002-08-22
    body: "VMware sucks.  Get win4lin (www.netraverse.com).  Faster, cheaper."
    author: "gor"
  - subject: "VMware/win4lin"
    date: 2002-08-23
    body: "AMEN... I tried vmware and win4lin.... Went out and bought win4lin.  IT's a GREAT program... even plays wmp streams WITHOUT any problmes!!!!"
    author: "Jeff Stuart"
  - subject: "Re: VMware/win4lin"
    date: 2002-08-25
    body: "I agree. I too tried vmware and win4lin, and win4lin is better (for my use), it is a great program and the better way to use windows programs inside Linux."
    author: "Alain"
  - subject: "win4lin: ever seen Windows (re)boot in 6 seconds?"
    date: 2002-08-28
    body: "Full ACK.\n\nI've been using Win4lin for the last two years and now they support the preempt-patches in 2.4.18 and up, win4lin is _really_ slick and fast.\n\nInstall (scripted) Windows 98 in about 12 minutes. If you want to mess with your Windows installation, make a backup of ~/win and ~/.merge and you're set. Restoring a broken Windows installation has never been faster (about 30 seconds here).\n\nEvery user account has \"his own\" windows, where (optionally) only the differences to a generic install is saved. So if you want to experiment, create a \"test\" account, copy your ~/win and ~/.merge directories there, then mess with *that* installation. Good for trying out unknown software.\n\nReboot Windows in about 6-10 seconds, depending on your hardware. But even if you only have a K6-350 (which is what I started with): Win4LIN runs at just about original speed! Not like VMware (which admittedly does much more than just Windows 98), which isn't useable below a GHz.\n\nSo: go get it if you still need Windows apps and don't want/have time to experiment with Wine. \n\nIt's worth it (and cheaper than VMware).\n\nJens"
    author: "Jens"
  - subject: "Re: win4lin: ever seen Windows (re)boot in 6 seconds?"
    date: 2002-10-05
    body: "bad thing ive heard is that win4lin doesnt support directx! and the main purpose is to run some windows apps but not games."
    author: "dave"
  - subject: "Re: win4lin: ever seen Windows (re)boot in 6 seconds?"
    date: 2002-10-07
    body: "Yes. So? It's not supposed to.\nIt's a business application, not a games emulator. If you want to run games, you can experiment with Wine. If you want to reliably (as far as Windows goes) run MS Office, Lotus Notes, or whatever, go get Win4lin.\n\nThere are three things that don't work with Win4lin currently: sound recording (playback works perfectly), MIDI, and 3d hardware acceleration. No business user needs these features. If many people ask them to implement it, they'll probably do it.\n\nbtw: last Time I checked VMware didn't do 3d hardware accel either.\n\n\n-- Jens"
    author: "Jens"
  - subject: "Re: VMware/win4lin"
    date: 2003-01-31
    body: "I've been using (and promoting to my customers) VMware since Mandrake 7.2,\nabout 2 years now, I guess.\nThere are two great failings of VMware as I see it. The first is the fact that idling normally...it chews up about 25%-30% of my 1GHz Athlon. That's pretty hefty!\nSecondly is the minimal sound support. I mean MINIMAL!\nMy great love is sound recording, so you can imagine what pleasure it gave me to hear Win4Lin start up without the broken ruccus of VMWare doing it's inimitable rendering of the Windows98 theme song.\nAnd I am looking at my wmcpuload readout and it's hovering between 2%-5%... and that's mostly because I'm running DCGui at the same time.\nThe really noticable thing though is how lightening-fast win98 is. Oh, I may already have axed a few superfluous dll's, but it occurs to me that Win98 isn't such a bad OS after all, running like this anyhow.\n\nI look forward to sound recording and midi support. I won't be doing my music post production in Windows-proper any longer, as CoolEdit and SoundForge seem to totally rock in Win4Lin! "
    author: "OSbusters"
  - subject: "Re: Bad, bad..."
    date: 2002-08-22
    body: "Well -- lately, i played a lot with wine (especially WineX) -- it already runs a number  of apps even without WINDOWS installed -- i hope i will be able to run that stuff in it too -- the biggest problem is to install those things in it -- it seems like some windows-special things that installer requier are still missing. \n\nAs for supermount -- i used it when there was Mandrake 7.1, and i even liked it. Can someone tell me a place where i can find it now? Does it run in SuSE 8.0 well? "
    author: "SHiFT"
  - subject: "Re: Bad, bad..."
    date: 2002-08-25
    body: "<i>Manual mounting: give her supermounting.</i><p>\nnone of that filthy talk here please."
    author: "dave"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "I ALSO TRY TO CONVINCE MY GIRLFRIEND TO INSTALL LINUX AND KDE3 !!! ;)\nmaybe she will buy a linux distribution soon, because, you might know about the Valid/Invalid XP-version on the net, that makes it impossible to reinstall XP after 05/2002   hahahahaha :)\nhope, many people will switch to linux and the community will grow..\n^-^\n"
    author: "Mononoke"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "My girlfriend loves kde/linux too, esp the 3.1 releases.\nShe is the geeks dream :)"
    author: "montz"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-24
    body: "I managed to convert my mate and his girlfriend to Linux/KDE (well, her partially. She's still too into that damn Windows box.)"
    author: "Tau"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "What you forget to mention is, that your girlfriend is 400 pounds; has a moustache and was once a man.\n\nCome on, don't give me your load of crap stories about how your mannish significant other runs GNU/Linux or some weak window manager; please!"
    author: "English Troll"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-22
    body: "MUHUHAHAHAHA... Someone needs a geekgirl pretty bad huh?"
    author: "Danish Troll"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2002-08-23
    body: "you wish :)"
    author: "montz"
  - subject: "Re: My girlfriend likes it,.... "
    date: 2007-01-30
    body: "This is gud dear, u must encourage her to continue use keep up the sprit."
    author: "Prabhanjan"
  - subject: "Handy tooltips..."
    date: 2002-08-22
    body: "= great usability improvement.\n\nI am still missing Sort by type action in Konqueror to be remembered. Is it possible to fix it before KDE 3.1 is released? In some of my folders I have a lot of different file types and  it is very handy to have them sorted by type everytime I open that folder. Now I have to sort them every time I enter that folder/directory 'couse Konqi can not remember it when using Save view profile."
    author: "antialias"
  - subject: "browsing session crash recovery"
    date: 2002-08-22
    body: "I noticed that there should be a browsing session crash recovery included in the release. Well, konqi just crashed, but that session recovery didn't seem to work (at all) - only thing i got was KCrash pop-up.  Whassup with this ?"
    author: "jmk"
  - subject: "a question"
    date: 2002-08-22
    body: "hi this is a simple question :\n\nis konqueror capable of caching thumbnails in a view ?\n\nbecause if i enter camera:/my digicam/ , i get my digital camera view but it\nalways make the thumbnails from new , that take time !!!\n\nthx for answers\n\ngreets  chris"
    author: "chris"
  - subject: "Re: a question"
    date: 2002-08-22
    body: "I know it caches thumbs in regular directories. If it can't write (Or doesn't want to for safety reasons) to your camera then it probably won't cache. Right now thumbnails are cached in the same dir as the pictures, so you wouldn't want that in this case.\n\n"
    author: "Spencer"
  - subject: "\n\nfolder icons which reflect a folder's contents"
    date: 2002-08-22
    body: "I have to say this feature looks pretty sweet.  These types of things are the little touches that make me love kde."
    author: "lupinar"
  - subject: "Re: folder icons which reflect a folder's contents"
    date: 2002-10-22
    body: "it sounds good, if by that they mean something similar to the thumbnail folder icons in Windows XP.  I'm going through withdrawal without that feature; being able to pick our Ogg albums out by the cover art is a major benefit to us \"regular home users\"!"
    author: "Damek"
  - subject: "Some problems here"
    date: 2002-08-22
    body: "Hi, \n\nI installed Kde 3.1 on my Mandrake 8.2 box. So far, it looks very good, but I have noticed the followig problems so far.\n\n1) Kcontrol does not appears in K-menu.\n2) in Kcontrol -> LookNFeel only the following options appear: colors, Fonts, Icons, Launch feedback, Screen saver, Style, System notifications, and Theme Manager. NO window decoration nor Windows behavior.\n\nup for the good work!.\n\nmuyfeo"
    author: "Muyfeo"
  - subject: "Re: Some problems here"
    date: 2002-08-22
    body: "That's not a problem, Window Management is a separate entry, the settings are not in LookAndFeel,)"
    author: "gogo"
  - subject: "Re: Some problems here"
    date: 2002-08-22
    body: ">That's not a problem, Window Management is a separate entry, the settings are not in LookAndFeel,)\n\nWas this done under the auspices of \"Usability\"? If so it's pretty dumb. \"Window Management\" is meaningless to most people - and certainly something that changes the \"Look and Feel\" of windows (i.e. the theme) should appear under \"look and Feel\"! Moving options such as focus policy out of \"look and feel\" makes some sense.\n"
    author: "dr_lha"
  - subject: "Re: Some problems here"
    date: 2002-08-22
    body: "I usually just use the search tab to find what to configure... It is really fast and convinient... You should try it instead!"
    author: "Perra"
  - subject: "Re: Some problems here"
    date: 2002-08-22
    body: "Yes - I agree it's a nice feature. But it doesn't change the fact that this move doesn't help. For a power user like myself (10 years Unix/Linux - so I'm used to byzantine GUIs) it just confuses me because they moved an option, and for a beginner who doesn't have any clue what a \"Window Manager\" is because they've come from the Windows/Mac guide - it doesn't help to hide useful configuration options behind what is basically Unix/X11 jargon. \"Look and Feel->Window Decoration\" seemed somewhat obvious so is surely better for the newer user."
    author: "dr_lha"
  - subject: "Re: Some problems here"
    date: 2002-08-23
    body: "I like this move. Don't always talk about this legancy stuff.\nOr do you want all menus and icons and ... of KDE1 in KDEcurrent?\nIf everything stays as it is, development will stop!\n\nNew users will like it or do you like to have all L&F and WM items\nin one L&F-tree? The tabs are removed, great usability enhancement :-)"
    author: " "
  - subject: "RedHat Limbo2"
    date: 2002-08-22
    body: "Its a pity that RedHat Limbo (next RedHat 8.0) is fucking up KDE. In the task bar, they have deleted Kmail and Konqueror icons in stead of Mozilla and Evolution icons!. And the standar look&Feel is horrible (they want that KDE has the same L&F as Gnome). Ugly. And lot of things ... "
    author: "daniel"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-22
    body: "I liked Redhat distribution, but because of their ignorant behaviour towards KDE, I switched to Mandrake. And I never regretted it."
    author: "Bojan"
  - subject: "Should of tried SUSE 8 instead"
    date: 2002-09-04
    body: "I came from mandrake and SUSE is much better. BEtter configuration tools and ebtter isntaller, it evem includes more programs. THough I really miss supermount and Iw ish they woud integrate Kvim 6.1 into Kdevelop, that would rock,a s wella s samba isntalled automatically like in Lycoris."
    author: "Alex"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-22
    body: " RedHat has some old GNOME hackers making the big desitions.. and it apears that they dont like the no-mersy-kick-ass competition that the old C written hag is meating from the C++ written lace lady ;-P\n\n One could even argue that it is a \"political\" suicide they have made.. Just because they are one of the old and renoved distroes does not make them imortal. And the way KDE is regarded even by ppl how *doesnt* use Linux/BSD votes pretty strongly for a 100% KDE workstation situation.\n\n Perhaps they just cant grasp the concept of C++? I know old C hackers who still after 5 years just writes C wrapped in C++... so maybe its because they cant figure out how to port their tools to Qt/KDE *giggle-giggle-giggle*\n\n/kidcat"
    author: "kidcat"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "RH is going to be interesting to watch as it is commercial evolution in action; any company that refuses to adapt to changing conditions in the market will be left behind and hence die out. \n\nIt is fairly obvious that Linux on the desktop is a long way from dead and certainly gaining market share; it would be wise for RH to try to tap this emerging market. However, due to what can only be described as religious beliefs or an unwillingness to lose face, they still refuse to give reasonable support to the leading desktop environment.\n\nTheir support of Gnome is understandable as nobody likes to spend a large amount of time & money yet still manage to end up as second best, however, supporting gnome is one thing but ignoring KDE's existence & popularity is both childish and very bad business. \n\nThe stupidity in this is almost beyond words and reminds me of what James Randi calls the Henriette Syndrome \u0097 the overpowering need by some people to accept and believe something preposterous, and the ability to ignore and dismiss the contrary evidence, no matter what its quantity, nor how strong it is.\n\nFortunately, RH's arrogance will be their undoing as they will learn the hard way that if you don't give your customers what they want, they will go else where.\n\n\n\n"
    author: "Davy"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-22
    body: "Gah, this sucks.  This is much worse than RH only letting one guy work on the packages, and in his spare time to boot.  Speaking of which, what does Bero have to say about all this?\n\nGiven that RH is going to distribute a horribly trashed version of KDE with RH8, we now *really* need some KDE-friendly person(s) to make proper KDE-3.1 RPMs for Redhat. \n\nMaybe I'll do it, if no one else steps forward...I don't have much experience making RPMs, though.  Is there some software out that helps you make the build file correctly?"
    author: "Jason"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "You are wrong ... in RedHat Labs they modify the sources ... for example, KDE team, when they release a new version of KDE, they give rpms for Debian, Mandrake and RH with their own features. Then, theorically, the new KDE 3.1 binaries for RedHat 8.0 should be the one we see in the screenshoots, isnt it?"
    author: "Daniel"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "Uh, are you sure I'm wrong?  Maybe I didn't understand what you were trying to say, but it's the stated position of KDE that they don't supply binary packages for *any* distribution; it's up to the distros to package KDE themselves.  Any binary packages listed by KDE have been provided externally.  RH has traditionally shunted this job off onto a single interested employee (Bero), who has done a heroic job of it up to this point, but he's only one guy doing it in his spare time.\n\nThe concern is that the version of KDE 3.1 that ships with Redhat 8.0 is going to be crippled (by RH) in the following ways:\n\n1. re-skinned to look like GNOME.\n2. KMail icon replaced with Evolution (presumably configured as default mailer)\n3. Konqueror icon replaced with Mozilla (presumably configured as default browser)\n\nCan you tell me which part of this I have wrong?  I'd really love to hear that it's all wrong! :)\n"
    author: "Jason"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "What is so bloody wrong about 1?\n\nIt means GNOME-users will be able to use KDE-apps without them looking totally alien.\nThere are literally HUNDREDS of KDE-centric distributions out there, and ONE big GNOME-centric one. What is wrong with that? Shouldn't GNOME-users have their distribution?\n\nBesides.. the new GNOME2-desktop in Red Hat Limbo is incredibly slick."
    author: "Gaute Lindkvist"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "Does having an \"About KDE\" entry in an app make it look alien?\n"
    author: "Sad Eagle"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "I don't know, man.  IMO, they should leave the default Look-and-Feel settings. Look, I have no problem at all with Redhat choosing to make GNOME their default desktop, that's fine as wine.  \n\nBut why do they have to try to make KDE look like GNOME?  If a redhat user would prefer to use KDE, why try to undermine that choice?  They could simply provide (non-default) GNOME-ish look-and-feel settings for users who might want that.  \n\nPlus, new users will see the hacked KDE and conclude that it's some cheap GNOME knock-off (oh the irony...) that doesn't even have its own email tool or web browser, and was apparently not created by anyone (since the about box is gone).\n\nI admit, I'm jumping to conclusions a bit.  We still don't really have solid information about what's happening (however, see the screenshots I posted in the KC-KDE thread). "
    author: "Jason"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "I'm guessing that the answer is that it's easier for them to support a GUI that looks more or less the same whether it's GNOME or KDE.\n\nDon't forget that Redhat is changing the default GNOME look quite a bit as well to make this change.\n\nCheers."
    author: "aigiskos"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "Yes, and I have also learned since my last post that when the user runs KDE for the first time, the Redhat theme is simply one of the look-and-feel options.  So hopefully, they have left the default settings alone.\n\nSo, at this point, my only problem is the removal of the KDE about box.  I really can't see what purpose it serves, other than snubbing KDE developers...is the user meant to conclude that redhat themselves created the wonderful GUI and apps that they're using?  Developer credit is the currency of the Free software culture, so removing the credits is tantamount to stealing, IMO."
    author: "Jason"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-24
    body: "Stealing is taking away property from the owner. Our code is still all in CVS, so stop using MPAA/RIAA phrases when they do not make any sense. Furthermore, our license allows these changes as long as they are redsitributed under the same terms, so no harm done there either.\n\nStop bitching about Red Hat. If you don't like it, just don't use it and recommend something else when a friend or CTO asks you.\n\n"
    author: "Rob Kaper"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "And just by coincidence GNOME is being improved and KDE is being destroyed.  How convenient."
    author: "ac"
  - subject: "Re: RedHat Limbo2"
    date: 2002-08-23
    body: "This is exactly the idea.  You hit the nail on the head.  The only thing we can do is file bugs and complain LOUD AND CLEAR to RedHat.  It is not enough to complain here."
    author: "ac"
  - subject: "It won't cripple KDE, these ar epretty minor thing"
    date: 2002-09-04
    body: "The 3 things you lsited are not that bad. Especially since Evolution 1.0.8 and Mozilla 1.1 are better than their KDE counterparts adn the re-skinning is just to keep it consistent. Users cana lways change back to Keramik. Or acess Kmail from the KDE menu. \n\nIs till suggest people wait for SuSE 8.1 isntead of jumping Redhat's boat. It should rock."
    author: "Alex"
  - subject: "cvs tag?"
    date: 2002-08-22
    body: "How come cvs is not tagged? I checked from webcvs. Am I missing something?"
    author: "abemud"
  - subject: "Re: cvs tag?"
    date: 2002-08-23
    body: "Current release management sucks: No CVS tagging of releases, snapshots are not regularly updated, failed release dates without updates what or when something happens, releases without announcement (KOffice), no provided, incomplete and untested packages from packagers (not enough time given to them?)."
    author: "Anonymous"
  - subject: "Re: cvs tag?"
    date: 2002-08-23
    body: "Stop crying an help them!"
    author: "  "
  - subject: "problems on mandrake"
    date: 2002-08-22
    body: "I've had 2 big problems with mdk plus some minor ones.\nThe two are:\n\nKonq dosen't handle cookies and if you go to configuration it says can't find cookie server. \n\nBesides kmail dosen't have the gpg plugin it should (or I don't know where it is) so I can't use encrypted mail. \n\nBut I was already expecting some problems, once it's a beta. :) \nBut, does anyone have a solution for them?"
    author: "protoman"
  - subject: "Re: problems on mandrake"
    date: 2002-09-18
    body: "Since yesterday, I receive the same error message about the cookie server. Only as a regular user though, when I run Konqueror as root, everything works fine.\n\nI'm running Debian Unstable with XFree86 4.2.1 and KDE 3.1 beta 1...\n\nI hope anyone has a way to fix this :)"
    author: "rb338"
  - subject: "Re: problems on mandrake"
    date: 2002-09-18
    body: "Since yesterday, I receive the same error message about the cookie server. Only as a regular user though, when I run Konqueror as root, everything works fine.\n\nI'm running Debian Unstable with XFree86 4.2.1 and KDE 3.1 beta 1...\n\nI hope anyone has a way to fix this :)"
    author: "rb338"
  - subject: "Re: problems on mandrake"
    date: 2003-04-29
    body: "I also am getting this error message for my user ID on one of the systems I upgraded to 3.1-4.  My wife's userid on the same system is fine, and my other system (also 3.1-4) is fine as well."
    author: "Scott Bogert"
  - subject: "Need help with MD8.2 please"
    date: 2002-08-22
    body: "I've downloaded all the mandrake packages, then hunted down all the dependencies.  I am down to one and I can't figure it out.  I run: \n\nrpm -Uvh *.* \n\nand get the following:\n\nerror: failed dependencies:\n        libkateinterfaces.so.0   is needed by kdeaddons3-3.0.1-1mdk\n\nThe interesting thing is that kdeaddons3-3.0.1-1mdk does not exist in that folder or anywhere on the system.  This file is from the KDE 3.0.1 installation.  Furthermore, libkateinterfaces is provided by kdebase package.  \n\nWhat am I missing?"
    author: "Frank Rizzo"
  - subject: "Re: Need help with MD8.2 please"
    date: 2002-08-23
    body: "Use rpm -Uhv --force --nodeps"
    author: "fenris"
  - subject: "Re: Need help with MD8.2 please"
    date: 2002-08-23
    body: "It seems that you are installing KDE 3.0.1, not KDE 3.1 Beta. I am already running KDE 3.0.3 with Keramik theme! I think you should get KDE 3.0.3 rpms. "
    author: "NS"
  - subject: "Re: Need help with MD8.2 please"
    date: 2002-09-08
    body: "no, frank's previous version is kde 3.0.1!\nit is failing dependencies, because kdeaddons3_3.0.1 need to keep that file. there's no kdeaddons3_3.1_beta1 package for MDK. \n--nodeps may help (i did it so)"
    author: "luci"
  - subject: "Re: Need help with MD8.2 please"
    date: 2002-08-23
    body: "Hi,\nI had faced the same problem i went to rpmfind and downloaded the files. "
    author: "Progmaan"
  - subject: "Re: Need help with MD8.2 please"
    date: 2002-08-23
    body: "Hi,\nI had faced the same problem i went to rpmfind and downloaded these files\n\nlibart_lgpl2-2.3.10-2mdk.i586.rpm\nlibart_lgpl2-2.3.10-2mdk.i586.rpm"
    author: "Progmaan"
  - subject: "differences with the alpha release?"
    date: 2002-08-23
    body: "are theey many?\nThe alpha is already stable here..."
    author: "jaysaysay"
  - subject: "Re: differences with the alpha release?"
    date: 2002-08-23
    body: "Sure, this represents six weeks of work of all which where not on vacation."
    author: "Anonymous"
  - subject: "Eye Candy"
    date: 2002-08-23
    body: "Using Keramic as the default theme was a very wise choice. It gives KDE a very professional look that IMHO out does both OS X and XP.\n\n"
    author: "Davy"
  - subject: "That's a fanboy opinion"
    date: 2002-09-04
    body: "No new user will honestly choose Keramik vs Jaguar or WindowsXP. It's not as consistent or complete. It doesen't completely tehme KDE either. It also doesen't theme the Kicker. I think Keramik should come with this Kicker backkgroudn though: http://www.kde-look.org/content/show.php?content=1439 taht would make it better. I wish KDE devs would ahve seleted Mosfet HPL though."
    author: "Alex"
  - subject: "Kolf Courses Wanted"
    date: 2002-08-23
    body: "Hi!\n\nKolf is a new miniature golf game (http://katzbrown.com/kolf/) in kdegames in KDE 3.1, and you can easily design your own courses. To learn how, read the documentation that comes with kolf. Basically, click the pencil icon in the toolbar, making sure you don't start the game in strict mode, and drag stuff around. Choose the \"Create New\" course in the new game dialog to make a new course, of course. To edit it again, add it to your course list with the Add button in the new game dialog.\n\nYou can upload kolf courses you make here:\n\nhttp://katzbrown.com/kolf/Courses/Upload/\n\nand view uploaded courses here:\n\nhttp://katzbrown.com/kolf/Courses/User Uploaded/\n\nThanks!\n\nJason (Kolf Author)"
    author: "Jason Katz-Brown"
  - subject: "Re: Kolf Courses Wanted"
    date: 2002-08-24
    body: "First kolf is so much fun, thank you.\n\nSecond, is there anyway to add new artwork to give my course a different look."
    author: "theorz"
  - subject: "Re: Kolf Courses Wanted"
    date: 2002-08-24
    body: "Sorry, not yet. I hope to add a way to insert pictures into courses by 3.2.\n\n\nCheers,\nJason"
    author: "Jason Katz-Brown"
  - subject: "Cool!"
    date: 2002-08-23
    body: "Finally tabbed browsing is up in Konq. I have been using Moz 1.1b until now just for this feature. \n\nWith the Keramik theme, my Mandrake8.2+KDE3.1 will drive all those Windows and Mac users green with Envy.\n\nKDE is great!"
    author: "Anand"
  - subject: "Re: Cool!"
    date: 2002-08-24
    body: "\"my Mandrake8.2+KDE3.1 will drive all those Windows and Mac users green with Envy.\"\n\nNot untill the Kicker looks similar fancy, elegantly and completed and is customizeable like WinXP's Start-panel.\nIf you first boot the Desktop, after installing the OS, it makes sense, to give the Kicker/Panel/Start-bar(whatever;) as much 'Eye-Candy' as possible, and even a bit more, to give the user view, that is completed! ;)\nXP did that! and even Gnome did, i saw screenshots. but i love KDE ;_; so, have to wait for better look.\n\nMy opinion, the default \"high-color\" style of KDE should get some polish! it is already looking really good! (a bit similar to XP). With a nice kicker-background (if Kicker finally will drop it's BORDER!!!), and a nice coded Window-decoration like keramik, it is really great.\nin My opinion, the Keramik-styled Buttons just look too HEAVY! just look at Html-pages! the Buttons don't fit at all (till now, maybe kde3.1 will be diffrent), and the tabs look anything but not beautiful, it seems that someone forgot them there;) The whole Tab-pane/window should get it's OWN color, at least not the same like the \"window-background\" color!\n\nNeed more eye-candy ? just ask..\nargh, i should make screenshots/pictures, so that you all can see, how some >simple< things/changes make the KDE >really< lovely.\nanyway keep on the great work. and just remember, sometimes, simple changes to colors/borders could win many new friends ;)\n"
    author: "Mononoke"
  - subject: "KDE 3.11 for Workgroups"
    date: 2002-08-23
    body: "Can I have it?\n\nKristian\n"
    author: "Kristian K\u00f6hntopp"
  - subject: "Re: KDE 3.11 for Workgroups"
    date: 2002-08-26
    body: "Bwa ha haa\n\nThe ol' windows 3.11 for workgroup ... the crappy one ;-)"
    author: "Eng Genie"
  - subject: "KDE Bob"
    date: 2002-09-02
    body: "Hey, why not? After all, it was truly user friendly!"
    author: "Steve Hunt"
  - subject: "SuSE8 RPM's"
    date: 2002-08-23
    body: "The SuSE8 rpms are the worst ones i every saw from the dudes - missing rc files - missing pim - broken pam ect ect ect ... anyway .. compiled from the ground kde31b1 looks really cute and a lot of bugs from a1 seems to be fixed.\n\nfyi: i repacked the faulty packages .. ready for download here:\n\nhttp://www.marcooo.de/modules.php?name=News&file=article&sid=825\n\nrgds\nmarc'O\n"
    author: "marc'O puszina"
  - subject: "Re: SuSE8 RPM's"
    date: 2002-08-23
    body: "A couple of Problems:\n\nbase package says:\nerror: kdebase-3.0.7-1.i686.rpm cannot be installed\n\nand kdelibs, network, pim and utils are not available for download.\n"
    author: "ld6772"
  - subject: "Re: SuSE8 RPM's"
    date: 2002-08-23
    body: "wait a little bit dude ... still uploading .. rpm packe u'r tried to install is currently in upload progress .. as allready said in the article:\n\nNOTE2! - Die Packages werden zur Zeit u/loaded - eta 2h da nur 14k upstream... \n\n... jeow i know .. that german ;-) ... eta 2h till all packages are ready for download ...\n\nrgds\nmarc'O"
    author: "marc'O puszina"
  - subject: "Re: SuSE8 RPM's"
    date: 2002-08-23
    body: "That would be so great to make it compleet.\n\nHave to do some waiting, but hey it will be good.\nkdeaddons-3.0.7-1.i686.rpm\nkdeadmin-3.0.7-1.i686.rpm\nkdebase-3.0.7-1.i686.rpm\nkdelibs-3.0.7-1.i686.rpm\nkdenetwork-3.0.7-1.i686.rpm\nkdepim-3.0.7-1.i686.rpm\nkdeutils-3.0.7-1.i686.rpm\n\nThx thx"
    author: "VenimK"
  - subject: "Re: SuSE8 RPM's"
    date: 2002-08-23
    body: "well .. upload done .. had to resume 3 times because telco cut me off ... if anyone got a problem just give me a sign ...\n\nrgds\nmarc'O"
    author: "marc'O puszina"
  - subject: "Re: SuSE8 RPM's"
    date: 2002-08-23
    body: "Waauuw a big thx to marc'O, YOu rulez.\n\ndid the -nodeps -fore you suggested, reboot and open 'd konq. And everything was looking good. GREAT"
    author: "VenimK"
  - subject: "Redhat any one?"
    date: 2002-08-23
    body: "Has any of you seen a kde 3.1 Beta1-release for Redhat? "
    author: "fenris"
  - subject: "Re: Redhat any one?"
    date: 2002-08-23
    body: "Yes! I would love to see RedHat RPMs too. I'm dying to install and try out KDE 3.1 beta1. Is there some reason the RPMs have to be distribution specific? I am wondering if I could just install the Mandrake one for example. Yes I am a newbie. After suffering for many years with windows I am thrilled to be using Linux with KDE. It is sooooo much better! And KDE is so much better than Gnome too. But then you all knew that I am sure.\n\nAny further info about how to get KDE 3.1 beta on my RedHat 7.3 system would be most appreciated.\n\nOne last thing, I have been reading about troubles with Konqueror not working right, missing buttons, etc. and I am wondering if there is some way to fix that if I try the beta."
    author: "Michael Lyons"
  - subject: "kmail in 3.1x"
    date: 2002-08-23
    body: "does anyone got some ideas or some urls for me about this plugin stuff in kmail from kde3.1x?\n\nrgds\nmarc'O"
    author: "marc'O puszina"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-23
    body: "You'll find all necessary information here:\nhttp://www.gnupg.org/aegypten/\n\nPlease note that building the plugins isn't trivial. Here you'll find a detailed howto:\nhttp://www.gnupg.org/aegypten/development.en.html\n\nRegards,\nIngo\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-25
    body: "hi ingo .. \n\nthx alot for that info ... is agypt currently the only plugin stuff running with kmail? .... as far as i can see it's possible to code very nice stuff with this plugin interface ...\n\nrgds\nmarc'O"
    author: "marc'O puszina"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-27
    body: "Yes, currently there are only two plugins. One for PGP/MIME and one for S/MIME (both are part of the Aegypten project).\n\nAs I didn't have a look at the plugin interface I don't know how generic it is.\n\nRegards,\nIngo\n\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-26
    body: "I want to find out how to compile/use the Open PGP plugin.\nDoes anyone knows? I'm currently without GPG working on kmail!"
    author: "protoman"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-27
    body: "You don't need the plugins for using the simple build-in OpenPGP support (does only inline signing/encryption).\n\nRegards,\nIngo\n\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-27
    body: "Here is the thing, when I try to see a encrypted mail that worked on kde 3.0.2, kmail says I have no specified Open PGP plugin and I need to set it.\nEven signatures (trusted) aren't working."
    author: "protoman"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-27
    body: "Hmm, if it worked before (with KDE 3.0.2) then it should still work without plugin. If it doesn't work then please report this via bugs.kde.org.\n\nRegards,\nIngo\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: kmail in 3.1x"
    date: 2002-08-28
    body: "I've found what was happening, it's strange but the old pgped mail aren't opening, but new ones I receive are! Very weirdo. Luck I had nothing important on the old mails :)\nI hope this don't happens again in the future."
    author: "Iuri Fiedoruk"
  - subject: "The tooltip stuff"
    date: 2002-08-23
    body: "The preview of a document/url in the tooltip box is great widget :-)\nI love it."
    author: "JC"
  - subject: "compile problems with 3.1 beta"
    date: 2002-08-24
    body: "I'm trying to compile KDE 3.1 beta with gcc 3.2\n\nI successfully compiled&installed qt and kdelibs.\n\nwhen I compile kdelibs, /kdm/backend gives the following err\n\nauth.c: In function `ConvertAuthAddr':\nauth.c:678: dereferencing pointer to incomplete type\nauth.c:678: warning: implicit declaration of function `htonl'\nauth.c: In function `writeLocalAuth':\nauth.c:994: warning: unused variable `fd'\n/usr/include/bits/socket.h: At top level:\nauth.c:854: warning: `DefineSelf' defined but not used\nmake: *** [auth.o] Error 1\n\ni'm not a developer and don't understand this. Any suggesstions????\nWhere do I ask these kinda questions..\n\nthanks,\n-Mathi"
    author: "mathi"
  - subject: "Re: compile problems with 3.1 beta"
    date: 2002-08-24
    body: "Subscribe to the mailing list \"kde-devel\". I have found the list very helpful for problems similar to this.\n\nmailto:kde-devel-request@kde.org\n\namd the subject is \n\nsubscribe youremailaddress"
    author: "George"
  - subject: "KDE 3.1 in next RedHat"
    date: 2002-08-24
    body: "Do anyone know if KDE 3.1 (the final version) will make it into the next RedHat release, which is in beta stage now?"
    author: "Klara klok"
  - subject: "Re: KDE 3.1 in next RedHat"
    date: 2002-08-24
    body: "It won't."
    author: "Blai"
  - subject: "Validating downloaded source"
    date: 2002-08-25
    body: "OK, I've downloaded the source and the related PGP files. How do I now verify that the files I got are the KDE signed ones? I found the KDE signature page:\n\n  http://www.kde.org/signature.html\n\nbut it doesn't have any info regarding how to vefiry the signature! What command do I use?\n"
    author: "Richard"
  - subject: "Re: Validating downloaded source"
    date: 2002-08-25
    body: "man gpg"
    author: "Anonymous"
  - subject: "Re: Validating downloaded source"
    date: 2002-08-25
    body: "Thanks, but I could figure the \"man gpg\" part out for myself. The more obscure part, figuring out what data I'd bee supplied and the appropriate chain of commands, took me another half an hour. That time would have been eliminated with some simple help on the sig page.\n"
    author: "Richard"
  - subject: "CVS build scripts"
    date: 2002-08-29
    body: "Am I missing something?\n\nI can't get the build scripts from CVS to work.\n\nI checked out: KDE_3_1_0_BETA_1\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Issue with Icons in Mandrake 8.2"
    date: 2002-08-30
    body: "I noticed that if I choose the Connective Crystal Icon Theme. then. KDE Menu shown blank icons for most of the applications. This used to be OK in KDE 3.0.x. Whats up with this ? Any ideas how to fix.\n\n- _deadfish"
    author: "_deadfish"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2002-08-30
    body: "Another thing - The Mandrake binaries dont seem to be installing libkcm_kwinoptions.la and libkcm_konq.la. These are the modules for \"Window Behaviour\" and \"Desktop\" in the Control Center. Is there something I can do to fix this or is this a problem with the binaries ?"
    author: "_deadfish"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2002-09-15
    body: "the same problem here :( \nany solution?"
    author: "luci"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2003-01-30
    body: "i got this error with kde 3.1 (final release)"
    author: "os2"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2003-02-02
    body: "Ditto, but I'm on a somewhat borken SuSE 8.0..."
    author: "Tuba"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2003-03-03
    body: "I am having the same problem with Suse 8.1....when I upgraded to KDE 3.1 ...I seem to be missing libkcm_konq.la and others.\n\nAnyone have any luck with this"
    author: "Mark"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2003-03-07
    body: "I have fixed this on SuSE 8.1 by... uninstalling KDE (all of it) and reinstalling it again.\nApparently KDE 3 does not like to be upgraded to KDE 3.1 ;-)\n\nI have also taken the opportunity to upgrade the X server and QT, and this seems to work (using rpm -U).\n"
    author: "Francois Genolini"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2002-09-02
    body: "Yes, there are 2 Crystal themes with shipped with kde-3.1-beta1.\nOne with blanks icons :-( in kdelibs and the real theme in kdeartwork.\nIt should be ok for beta2.\n\nJC"
    author: "JC"
  - subject: "Re: Issue with Icons in Mandrake 8.2"
    date: 2003-03-07
    body: "I had the same probleme as i updatet my KDE from 3.0 to 3.1 on SUSE 8.0pro.\nI had solved the problem by deinstallation of qt3 and qt3-non-mt in yast2. Further\ni hed reinstalled the qt2 package in the same step. Then i has reinstalled the new\nqt3 and qt3-non-mt (both in version 3.1) from the console. You have to install it from\nthere because the packagemanager will not work whitout the qt-packages. After this\nmy KDE3.1 works fine.\n\nRegards from Switzerland\nM.von Arx"
    author: "Marcel von Arx"
  - subject: "How do I start KDE 3.1"
    date: 2002-09-03
    body: "I installed KDE 3.1 Beta 1 in Mandrake 9.0 RC1 using the rpm command. I was logged in as Administrator using Gnome. All of the 10 packages seems to be installed and the success indicators go to 100 %. But how do I start it? What else do I have to configure?"
    author: "Athlon"
  - subject: "Re: How do I start KDE 3.1"
    date: 2002-09-03
    body: "I don't use Mandrake but I guess you can switch gnome -> kde in the drakconf tool.\nIf not, if you run kdm or gdm, you should be able to change it.\nIf not, edit your ~/.xinitrc file and type \"startkde\". Save and re run \"startx\""
    author: "JC"
  - subject: "Konqi crash on local browse"
    date: 2002-09-03
    body: "Hello, all.\n\nI just compiled KDE 3.1 from the source tarballs on SuSE 8.0.  The new Keramik theme looks great, and I love the tabbed browsing, but...\n\nWhen I try to browse to /home/mydir, or /usr, or any local directory at all, Konqueror crashes.  I've already posted a bug report, but I thought I'd ask here as well to see if anyone knows an approach I haven't tried yet.  Is it worth it to compile the CVS version, or do I risk breaking my box so badly that my only hope is a format and re-install?  (Thought I was done with that unfortunate necessity when I got rid of Windows ;)\n\nThanks in advance...\n"
    author: "Eager tester"
  - subject: "Re: Konqi crash on local browse"
    date: 2002-09-04
    body: "Well, if you only compiled KDE from cvs, you don't have to format and re-install all your box.\nYou probably installed the new KDE in /opt/kde (check this) and just remove that directory (rm -rf /opt/kde). Then download the SuSE 8.0 packages and install them :-)\n\nIf you broke another package, I don't know SuSE, but you can probably just re-install the system without to format. I guess it would clean up your box without loosing data."
    author: "JC"
  - subject: "Re: Konqi crash on local browse"
    date: 2003-03-28
    body: "I had the same problem.\nWhen I desactivated the show previews option\nkonqueror stopped crashing.\n\n\nI hope this will be helpfull.\nIf you have a better fix please tell me about.\n\nElias"
    author: "Elias"
---
The KDE Project is pleased to announce the availability of KDE
3.1 Beta 1 for your testing enjoyment. This release, which marks
the second testing release of the KDE 3.1 branch, offers many
improvements and bug fixes over KDE 3.0.x. New features include
improved OpenPGP handling in KMail, handy tooltips that provide
details of files in Konqueror quickly, and even new ways to be
<I>less</I> productive thanks to four new games.




<!--break-->
<BR>
KDE 3.1 also officially introduces the much acclaimed Keramik
style as the new default widget style. On the PIM front, this
beta adds LDAP support to KAddressBook and also an AvantGo
conduit to KPilot. In all, this release sports hundreds of new
features, bug fixes, and improvements, many of which you can read
about <a
href="http://developer.kde.org/development-versions/kde-3.1-features.html">here</a>.
<BR><BR>
To get KDE 3.1 Beta 1, you can download source code tarballs at your favorite
mirror site, or right <a
href="ftp://ftp.kde.org/pub/kde/unstable/kde-3.1-beta1/src">here</a>. Binaries for several distributions are also available, see the full <a href="http://www.kde.org/announcements/announce-3.1beta1.html">announcement</a> for details and download locations. Hats off to <a href="http://www.kde.org/people/dirk.html">Dirk
Mueller</a> for coordinating this release and preparing the release announcement. In addition, congratulations to
all of the hard working developers for putting together such a
large set of improvements!



