---
title: "China Chooses KDE, KOffice for Desktop"
date:    2002-02-22
authors:
  - "Dre"
slug:    china-chooses-kde-koffice-desktop
comments:
  - subject: "If it would be KOffice 1.2..."
    date: 2002-02-22
    body: "I doubt that KOffice is really far enough as \"desktop office solution\". Obviously it was not enough advanced for Korean government. At least they will not be able to send spam letters because of missing serial letter functionality."
    author: "Anonymous"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-22
    body: "\nIt's ready for those who have not foolishly locked themselves into arcane, proprietary and freedom-depriving file formats.  KOffice does everything the vast majority of people need, is stable, fast, free, doesn't invade your privacy, does a fair job at importing other file formats and doesn't require you to sign a contract with a greedy, arrogant and law-breaking company.\n\nAs for Korea, you have no idea why they selected HancomOffice over KOffice or OpenOffice.  Maybe it has something to do with the fact that Hancom Linux is a Korean company and they wanted to support it; maybe it has to do with the fact that they used Hancom products before and just decided to stay with it; or maybe you are right that KOffice and OpenOffice doesn't meet their particular needs.  But since you don't know, your point is pure speculation, and there is nothing \"obvious\" about it."
    author: "Sage"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-23
    body: "I think the main reason Korea decided to go with Hancom is politics. Mayby you've missed China and Korea isn't best friends.."
    author: "Me"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-23
    body: "\"aren't\" not \"isn't\". \nmaybe some grammar help would be useful in KOffice..."
    author: "M"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-23
    body: "Please don't forget there is a lot of us talking another language than english.  What we need is a lot more than grammar help from an office solution.\n\nMen du kan jo pr\u00f8ve \u00e5 korrigere dette. ( In norwegian )"
    author: "Carlsen"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-24
    body: "> Men du kan jo pr\u00f8ve \u00e5 korrigere dette. ( In norwegian )\n----------------------------------------------------------^\n'norwegian' skrives med stor 'N' p\u00e5 engelsk -> Norwegian\n.. korreksjon utf\u00f8rt:-)\n\n--\nAndreas Joseph Krogh <andreak@officenet.no>"
    author: "Andreas Joseph Krogh"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-25
    body: "> (In norwegian)\n\nYeah, and since the material inside the parentheses doesn't end with a period (full stop), \"In\" probably shouldn't be capitalized, either. This looks like one of those \"with many eyes, all bugs are shallow\" thangs. If enough of us pitch in and submit patches, we just might be able to develop a completely error-free comment. Oh joy!\n\nMi tre sxatas tiel signifo-plenan, interkulturan, plurlingvan dialogon! (Cxu ankau en cxi tiu frazo oni trovos erarojn? Versxajne jes.)"
    author: "Leston Buell"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-26
    body: "> (Cxu ankau en cxi tiu frazo oni trovos erarojn? Versxajne jes.)\n\nHmm, cxu ni bezonas gramatikokontrolilon por Esperanto en KWord? Tiu estus ankaux utila en la posxtilo ktp... Kaj certe en la novajxgrupilo!"
    author: "Brion VIBBER"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-26
    body: "Non capisco perch\u00e9 vi ostiniate a non usare l'italiano... :-)"
    author: "OhGod"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-26
    body: "pareil"
    author: "julo"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-26
    body: "Also, ich finde es ja dreist, dass die Norweger jetzt \u00dcberhand gewinnen ;)\nWenigstens nicht in Olympia *g*\n\nI think KOffice is a nice product and it should be *enough* for most people..."
    author: "Freddy"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-27
    body: "huy (in Russian).\n"
    author: "i"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-26
    body: "> Non capisco perch\u00e9 vi ostiniate a non usare l'italiano... :-)\n\nPerch\u00e9 no siamo italiani, e no parliamolo? ;)"
    author: "Brion VIBBER"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-03-01
    body: "> Perch\u00e9 no siamo italiani, e no parliamolo? ;)\n\nMia scipovo de la itala lingvo ne estas perfekta, sed mi kredas, ke oni devus diri:\n\n   Perch\u00e9 non siamo italiani e non lo parliamo.\n\nCxu ne?"
    author: "Leston Buell"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-03-01
    body: "> Mia scipovo de la itala lingvo ne estas perfekta, sed mi kredas, ke oni devus \n> diri: Perch\u00e9 non siamo italiani e non lo parliamo.\n\nNu, pri tio mi ne certas cxar mi ne parolas gxin! Jen la dilemo: kiel oni diru \"mi ne parolas vian lingvon\" por ke la fremdlingvulo komprenu vin, se vi ne parolas la lingvon...\n"
    author: "Brion VIBBER"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-03-04
    body: "\u00e3\u00ee\u00f0\u00ff\u00f7\u00e8\u00e5 \u00ed\u00ee\u00f0\u00e2\u00e5\u00e6\u00f1\u00ea\u00e8\u00e5 \u00ef\u00e0\u00f0\u00ed\u00e8 :)"
    author: "belochitski"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-23
    body: "A :-)) would have been nice.\n\nWolfgang"
    author: "Wolfgang "
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-24
    body: "I think we *do* know why the Korean government chose Hancom Office:\n\n1. It is a Korean product, and it is in the government's \n   interest to support a local company.\n\n2. As a Korean product, its Korean language support \n   must be excellent.\n\n3. It is my understanding that Hancom Office is \n   the de facto standard in Korea. So no problems \n   reading other people's documents and older documents. \n   Most existing government documents are probably in \n   Hancom Office format. (Even if KOffice had Hancom \n   import and export filters, they would not be perfect, \n   and they're always a hassle.)\n\n4. The product is cross-platform. It runs natively on \n   Windows and Linux. There will be no problems stemming \n   from the fact that some employees will be using Linux \n   while others are using Windows. No need for \n   import/export filters.\n\n5. From what i have read, it is a very good product \n  (at least the word processor component).\n\nThis makes Hancom Office sound like a no-brainer to me, considering the environment it which it will be used. I think that the only other product to come close to satisfying these criteria would be StarSuite (the Asian version of StarOffice), but with StarSuite users would need to use import/export filters to read older Hancom documents and to exchange files with Hancom users."
    author: "Leston Buell"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-23
    body: "Must say I agree.\nI can't imagine anyone using KOffice seriously *yet*.\nI even find it to unstable. I've had kspread crash on me way to many times.\n"
    author: "Watermind"
  - subject: "Re: If it would be KOffice 1.2..."
    date: 2002-02-28
    body: "Whenever it crashes, I just open up another one.  It even saves\ndocuments for me.  Just hope the Konqueror with the icon in it\ndoesn't crash.\n\nWhereas, before I came to GNU/Linux, I would have entire OS crash\nand the disks would have to be repaired...  Finally I'd have to\ngo find the document's icon all over again."
    author: "No One"
  - subject: "Hooray"
    date: 2002-02-22
    body: "\"Hats off to Red Flag Linux for choosing the right product for the job.\"\n\nWell said.\n\nThe details of this interest me.  They mention \"decades of games,\" so I wonder how many beyond kdegames they ship.\n\nThey mention a VCD player, so I Wonder if that's xine or what.  If they can play VCDs with kaboodle I wish they'd tell me how they did it!\n\n\"Equipped with Netscape browser, pre-configured with 263/163/169/2911 dialup supporting environment.\"  That's disappointing, though vague.  They may center on Konq yet.   Must get screenshots.\n\n\"Support up to 6.4 billion users and groups\"  heh.\n\n"
    author: "Neil Stevens"
  - subject: "Re: Hooray"
    date: 2002-02-22
    body: "I don't know how they do it, but I read on apps.kde.com about kio_vcd:\nhttp://apps.kde.com/nf/2/info/id/1488\n"
    author: "roger"
  - subject: "Re: Hooray"
    date: 2002-02-24
    body: "Yeah, there\u00b4s a Xine-Icon default on their KDE-Desktop.\n\nI like Mplayer too.\n\nGreetz from Germany\n"
    author: "Christian Schuglitsch"
  - subject: "This information is untrue"
    date: 2002-02-22
    body: "The chinese government might have switched to Linux/KDE/KOffice. But Little did they know that after a couple of months, I bet that they are back with previous systems. This information gives the understanding that the whole entire China gov'ment sectors have adopted Linux, what is untrue. And among a population of 10 billion people, it is unlikely that the switch will it even 2% of it. \n<br><br>\n<i>ip == Reality Check</i><br>\n"
    author: "DN"
  - subject: "This information is quite true"
    date: 2002-02-22
    body: "why will they be back with their previous solutions after a couple of months?\n\nand no, this article doesn't give the impression that the all Chinese governmental concerns run Linux; although it should be alarming to those hoping Linux won't make inroads in the desktop operating system market. \n\np.s. china had a population of 1.273 billion in 2001, not 10 billion."
    author: "Aaron J. Seigo"
  - subject: "Re: This information is quite true"
    date: 2002-02-23
    body: "Having billions of users in Open Source systems are more significant that in other commercial projects, don't you think?\nImagine the speeds of future releases. Or just stepping by versions!\nWould somebody imagine such a number of developers in any OS project?\nRadical changes could appear in a very short time."
    author: "chanio"
  - subject: "Re: This information is quite true"
    date: 2002-02-24
    body: "i think having that many users would actually present new challenges to a Free software project. since they tend to encompass and encourage involvement by users and since the development process is very open and accessable to the public, one would have to wonder what sort of pressures would occur if the population of the \"public\" swelled to encompass hundreds of millions of people. it already gets fairly noisy as it is."
    author: "Aaron J. Seigo"
  - subject: "Re: This information is untrue"
    date: 2002-02-23
    body: "Hmm, I always thought that china has 1 biljon citizens :)\nFurthermore, I don't really care if they all use Linux or what so ever. that the goverment is looking at Linux as a serious prospect is a big win for Linux..\n\nRinse"
    author: "rinse"
  - subject: "Re: This information is untrue"
    date: 2002-02-23
    body: "Guess how many computers are round there? The government has power. Education is very hierachical. So if the standard computer students books will mention KDE every Chinese it-student will learn this. That's it."
    author: "Hakenuk"
  - subject: "Some corrections"
    date: 2002-02-22
    body: "It's the Chinese government that is adopting Red Flag Linux, not the entire population. For the short term at least, people that run Windows will continue to do so. \n\nIt should also be noted that much less than 1.3 billion people have computers in China.\n\nWhat's interesting about this is that the Chinese government will improve all the software shipped with Red Flag Linux (and that includes KDE) to have better l10n/i18n support for Chinese, which will solve many problems the Chinese may have had in adopting GNU/Linux.\n"
    author: "Carg"
  - subject: "Re: Some corrections"
    date: 2002-02-23
    body: "> What's interesting about this is that the Chinese government will improve all the software shipped with Red Flag Linux (and that includes KDE) to have better l10n/i18n support for Chinese, which will solve many problems the Chinese may have had in adopting GNU/Linux\n\nThat would be very good news. Does anyone know if there are any contacts between the Red Flag Linux developers and the KDE developers?"
    author: "jj"
  - subject: "Re: Some corrections"
    date: 2002-02-23
    body: "Note that the american government puts much pressure on china in order to regard software licences. \u00b4Most computers run illegal copys of win, so China ist just served as a future market by microsoft. Chinas support would be great. They will become more independent from us software and lack of software based espionage. Remember the boing airplane?"
    author: "Hakenuk"
  - subject: "Giving back."
    date: 2002-02-23
    body: "Correct me if I'm wrong, but the source code for Red Flag Linux is not available.\n<!--\nChinaMan maozedong@microsoft.com\n\nHaven't read anything coming from RedHat to China lately.  The guys in China are going to suck it in and never give back.  Just like how they copy everything on the face of the earth.  Copyrights, Patents nor OpenSource mean JACK SQUAT in Asia. \n\nIt's a freaking blackhole.  OK?  Lemme paraphrase:  \"China ain't gonna do JACK.\"\n-->\n\n\n\n\n"
    author: "n/a"
  - subject: "Re: China won't give back."
    date: 2002-02-23
    body: "You're wrong. The source code for Red Flag Linux 2.4 is available here[1].\n\n1. ftp://ftp.redflag-linux.com/pub/rf2.4/iso/src/24_src.iso\n"
    author: "Carg"
  - subject: "Re: China won't give back."
    date: 2002-06-17
    body: "That directory doesn't exist. Not now it doesn't, but there are directories where you can download source RPMs.\nI downloaded their Desktop 3  iso and when I booted it up for the first time there was this screen with just a box which looked like it wanted a serial number typing in. Unfortunately, the Chinese at the top of the box was broken, as is sometimes is in Linux, so I couldn't be certain what it was about.  A serial number for Linux? I  looked through their forums for users on their homepage and there was this one Chinese guy asking for the serial number for desktop 3, but the message was not there a couple of hours later. I mean, what's all that about, and why do they let you download a disc image if you need a serial number of all things to install it?"
    author: "Cral"
  - subject: "Re: China Chooses KDE, KOffice for Desktop"
    date: 2002-02-23
    body: "Yes. As far as I know, ALL the desktop linux distributions in China choose KDE as their default desktop. And, they all highlight KOffice as their \"desktop office suit sollution\". And so far, the KDE they shipped is better translated than gnome.<br><br>\n Check these sites(if you can read Chinese :):<br>\n XTeam: <A href=\"http://www.xteamlinux.com.cn\">http://www.xteamlinux.com.cn</a><br>\n Red Flag: <A href=\"http://www.redflag-linux.com\">http://www.redflag-linux.com</a><br>\n Happy: <A href=\"http://www.happylinux.com.cn\">http://www.happylinux.com.cn</a><br>\n Yangchunbaixue: <a href=\"http://dyp.v-eden.com\">http://dyp.v-eden.com</a> (a Chinese KDE, not a linux distro, with small screen shots :))\n<br><Br>\n Hope Chinese young guys can Konquer their language barrier as soon as possible and contribute to KDE and/or Gnome (they already do so, but limited, yet)\n<!--\njoe99\n\nits funny how you fucknuts think that \"China Chooses KDE\" ..which is obviously false, Red Flag Linux which is based upon RH.. the greatest Gnome supporter..but you tend to forget that this distro probably uses both..not *just* KDE. get over your egos guys, not every distro is just kde. fucken single minded prats.\n\nGo Redflag with Red Carpet haha.. Pango in Gnome2 is gonna own ya.\n-->\n\n"
    author: "xhq"
  - subject: "Hoorah for Linux and KDE"
    date: 2002-02-24
    body: "Team KDE deserves the Gold.  Even if only 10% of the Chinese \npopulation use and contribute to Linux/KDE, that is a \npotential of 100 million users and developers.\n\nI better sell my M$ stock...\n\nEd  "
    author: "Ed Rataj"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-24
    body: "> Even if only 10% of the Chinese population use and contribute to Linux/KDE\n\n50 indian people paid by Sun will contribute to Gnome according to a Slashdot."
    author: "Anonymous"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-24
    body: "and they will make a difference?\n\nlook how much they have pulled ahead with the current help.  basicly what it translates to is 50 more developers who will be guideing GNOME in SUN's direction...  \n\nsilly mokeys will be so pissed when they discover SUN will base GNOME 3 (or 4 or what ever it is today)on Java instead of .NET...  if they last that long...\n\nreally the fact that IBM is using KHTML and dcop means more than SUN throwing 50 developers at something that is still very broken...\n\n-ian reinhart geiser\n\n"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-25
    body: "I do (and you should too) believe they\u00b4ll make a difference. In free software, any help is appreciated. Be it from 50 people working for Sun, or you and me.\n\nGNOME has gotten pretty far actually. I have installed GNOME 2.0 post beta (from CVS). It\u00b4s very nice, and has what Sun wanted in the first place: accessibility (keynav, etc) and a _lot_ of usability improvements. With regarding GNOME, what\u00b4s good for Sun is good for all the GNOME users. (I also have KDE 3 from CVS installed).\n\nAs far as adopting Java, get real. It ain\u00b4t gonna happen. Ever. GNOME is written in C with the goal of supporting as many languages as possible. This means the core of GNOME will always be in C. And that hopefully GNOME supports/will support Java, Mono, C++, Python, Perl, Ada, etc. Sun has no power or desire to make GNOME become a Java project (rewriting GNOME would be too expensive for Sun: it would take a long time and would break source code compatibility big time. That costs more than people realize for a company like Sun). See the comments by de Icaza about moving GNOME to Mono: the same arguments apply.\n\nAnd your comment about IBM is, IMHO, naive. They use DCOP, KHTML, and they use Windows for some of their products. Go to news.gnome.org/gnome-news and search for \u00a8IBM\u00a8. It\u00b4s a very big company."
    author: "Carg"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-25
    body: "btw, you saying GNOME is very broken is plain wrong. There are many people using GNOME and GNOME-based apps out there, they wouldn\u00b4t if you were right.\n\nBut my point is: GNOME doesn\u00b4t hurt you. KDE will not die because of GNOME. GNOME will never affect you in a negative way. And if having a lot of people running KDE is important to you, know this: it\u00b4s much easier to switch to KDE from GNOME than, say, Windows. So having a lot of GNOME users is good for you.\n\nWhat I\u00b4d really like people to understand is that the people working on GNOME are \u00a8cool\u00a8. They\u00b4re not losers. They believe in a lot of the same things that you do, and they\u00b4re not doing anything against anyone. They diserve respect, and having you saying something like \u00a8[GNOME is] something that is still very broken...\u00a8, which is plain FUD, is not good for them, for KDE, for free software, for me, and, believe it or not, for you too.\n\nYou\u00b4re going to be a happier person if you love more than you hate. Give it a try.\n\n\n"
    author: "Carg"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-25
    body: "Great Perspective when China moves towards Linux.\n\nAsian (Japanese and Chinese) support has been a major headache for me as private Linux user. It is somewhat working nowadays, but support in applications is still something to be thankful for on individual basis rather than to be taken for granted. KDE supports it. Great improvement. Finding a font setting which looks good with both Asian and Roman characters is a challenge. Mozilla's Japanese version has 50% English menues. Why? Activation in applications differs between kde, gnome, and other x applications. Try copy and past between kde apps and Emacs. You won't enjoy it. The only way is: SAVE in emacs, reload with Kedit, then copy to KDE -- or the other way round. Koffice (I checked 6 months ago, maybe it is better now) messed totally up when I typed Chinese. Japanese was slightly better. I have high respect for the great work being done. But please do not underestimate the challenge of making a well working Japanese or Chinese environment, and -- printing. Asian truetypes must go all the way. Typing -- Preview -- Postscript -- Ghostscript -- Printer!\n\nStaroffice 6.0 is still beta, so that is no option for the Koreans. I expect the official one to get some annoying bugs fixed. Koffice is for the future, but not yet as default. I downloaded the Japanese demo-version of Hancom. My first impression was *very* positive. I would seriously consider buying it, if I needed a good and reliable office SW on my home PC. Actually, it does not hurt the spreading of Linux, if there are a few good quality reasonably proced commercial packages available in addition to open SW.\n\nChina is the opportunity for Linux, including the desktop. But it is too early to boast about already achieved victories. It will be a loooong march!\n"
    author: "Thomas Piekenbrock"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-25
    body: "Thank you! Finally a wise person. I really hate to see the KDE crowd an the gnome crowd trying to rip each others guts out.\nThe developers of both camps (if that is the word even) respect each other.\nAnd they deserve respect of both crowds too.\nWe're not animals are we? (uhm VietNam, W.W.II, Iraque, ...)\nJust code, people, instead of fighting over this :)\n\n"
    author: "blashyrkh"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-02-26
    body: "> The developers of both camps (if that is the word even) respect each other.\n\nExcept for one, Geiseri. :-("
    author: "Anonymous"
  - subject: "Re: Hoorah for Linux and KDE"
    date: 2002-03-01
    body: "I'd even go further and suggest that the continuation of gnome is in kde's interest, especially if it is used on other platforms (like solaris).  Why, because it draws attention to linux on the desktop (if gnome's good enough for sun...) , so rather than dismissing linux on the desktop people might have a look and find themselves using gnome _or_ kde (or both)."
    author: "dave"
  - subject: "Might be good, might be bad..."
    date: 2002-02-25
    body: "It's a good thing that a lot of users (and a few developers as well, not many developers per capita in most countries after all) will be using KDE. It's (according to me) the only desktop that is any good at all that is available to the different Linux based OSes out there. So as far as that goes, it's definitly good.\n\nI am on the other hand no beliver in Red Hat, and Red Flat apparently is based on it. Not so terrific as such maybe, and it's one more distro, which isn't that terribly good either. There is a limited amount of development happening in the world, duplication can both be good and counter productive. This falls in the latter category, again according to me.\n\nChina has a reputation of not caring about copyright (and as much as we hate the digital copyright developments, copyright is very good for us as citizens if you start to look at the whole picture. It's just that it's getting worse right now) and for wanting to make money. How will this hold up to the licenses and what will happen with code produced in China? It's a big subject, and I have no whatsoever answer up my sleave, I will let others debate it. So this could be horrible, or really great.\n\nThe Chinese state censors, it censors a lot, even more than the companies in the west does(which is yet another chapter to be dealt with). Is this a good thing? Well, no it's not of course. But what kind of implications will this have on the software? Again, touchy subject and no answers and a potential very big \"not good\".\n\nAbout KOffice and grammatical checks (yes yes, there is another thread, but I am lazy, we all are;)): It's not easy to do, Chinese, Swedish, and Finnish (for example) are radically different and I would suggest that the KOffice/KDE people finds some computer interested linguists to help them and produce a spelling/gram check Komponent to be used within KDE."
    author: "John Gustafsson"
---
<a href="http://www.newsforge.com/">NewsForge</a> has
<a href="http://www.newsforge.com/article.pl?sid=02/02/21/2211255">published</a> what it bills as the &quot;first-ever comprehensive English-language review
of <a href="http://www.redflag-linux.com/eindex.html">Red Flag Linux</a>&quot;. Most of you probably know that Red Flag Linux is the &quot;official&quot;
Chinese Linux distribution, and receives support - as well as contracts -
from the Chinese government.  What you may not have known is that,
despite being based on <a href="http://www.redhat.com/">Red Hat Linux</a>, Red Flag Linux
has opted for KDE as its default desktop.  Even more interesting, the
<a href="http://www.redflag-linux.com/chanpin/ecp.php?id=10000009">description</a>
of their &quot;Redflag Linux Desktop&quot; product lists none other than
<a href="http://www.koffice.org/">KOffice</a> as the &quot;desktop office
solution&quot;.  Hats off to Red Flag Linux for
choosing the right product for the job.  I'm not sure if the KDE
mailing lists are prepared for a billion more users, but it sure will
be nice to see how much KDE development is borne from
China's burgeoning info-tech industry!

<!--break-->
