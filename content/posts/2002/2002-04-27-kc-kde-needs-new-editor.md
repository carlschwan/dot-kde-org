---
title: "KC KDE Needs A New Editor"
date:    2002-04-27
authors:
  - "aseigo"
slug:    kc-kde-needs-new-editor
comments:
  - subject: "thanks aaron"
    date: 2002-04-27
    body: "I'd like to thank Aaron for the great job he has done with KC KDE over the years!  It has been a truly valuable service.\n\nIt would be really sad to see KC KDE disappear altogether, I hope someone volunteers to take up the task especially as Aaron is willing to help ease the transition.\n"
    author: "Navindra Umanee"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "I would like to thank Aaron as well. It was a joy reading those kc's. Mostly on monday with a nice cup of coffee, reading KC KDE. What better way to start the week. :-P \n\nTake care \n\nFab"
    author: "fab"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Yep, me too. You did a great job in summarizing all these mailing lists to the not-so-involved. This made it easy to catch a glance of what's going on with KDE.\n\nThanks!\n Sebastian"
    author: "Sebastian"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Agreed - I spent a half hour trying to find out if there was a (tentative) release schedual for 3.1 (only the 3.0 release is on the various *.kde.org sites, miserably out of date).  If KDE KC was up and running, I'd have at least an idea of what was going into it, and how soon it was looking to be released.\n\n(No, I'm not expecting it tomorrow - we're thinking of switching the company over to KDE + Crossover Office this fall)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "<a href=\"http://www.kde-look.org/news/news.php?id=28\">http://www.kde-look.org/news/news.php?id=28</a>\n<p>\nSVG Icon Support, Keramik style, Tabbed Browsing, Folder icons reflect contents and the fish ioslave are now in CVS. It seems that KDE 3.1 will rock!\n</p>"
    author: "ac"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Or in a more visual way...<br>\n<a href=\"http://www.babysimon.co.uk/kde/kde31features.png\">http://www.babysimon.co.uk/kde/kde31features.png</a>\n<p>\nIMHO those tabs could really do with an easy to access close button. \n</p>"
    author: "Bryan Feeney"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Am I the only one who thinks those window widgets are really jaggy? Maybe the author could try some anti-aliasing on the circles?"
    author: "Matt"
  - subject: "Re: thanks aaron"
    date: 2002-04-29
    body: "That screenshot was taken on a machine without alpha-blending (using VNC...) so there's more jag in there than normal..."
    author: "Simon MacMullen"
  - subject: "Re: thanks aaron"
    date: 2002-04-29
    body: "Hey!\n\nHow did you get those tabs in konqueror? I like them from Mozilla and would like to have them in kde, too.\n\nBye, Christian"
    author: "Christian Tr\u00f6ster"
  - subject: "Re: thanks aaron"
    date: 2002-04-29
    body: "Just use current cvs."
    author: "Lenny"
  - subject: "tabs!"
    date: 2002-05-02
    body: "Tabbed browsing is what keeps me using Galeon more than Konqueror... I've been hoping to see it in Konq for some time now, excellent!\nOpera introduced me to the tabbed browsing, I noticed Mozilla got it sometime, but Galeon 1.2's handling of these is probably the best... (option to have close buttons on them, etc). Opera opens a new window faster, though."
    author: "Juln"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Well, I can't thank you enough for the valiant effort of keeping me informed on the in' and out' on KDE development. You deserve a break, nowrun on back to your family and have fun."
    author: "David Nielsen"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Yes thanks Aaron,  I also have liked KC KDE from the start.\nIn fact I printed several of them so I can look back to\nhow kde has grown in a few years :)\n\nCheers,\n\nRob."
    author: "Rob Buis"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Hey, i thought you dont have a printer to test karbon printing. ;)\n\nAnd yes, i enjoyed kc kde very much, too."
    author: "Lenny"
  - subject: "Re: thanks aaron"
    date: 2002-04-28
    body: "<whisper mode on>\n\nHeh, I actually abused the printer at work for this.\nI can safely say this since I dont work there anymore ;)\n\n<whisper mode off>\n\nRob."
    author: "Rob Buis"
  - subject: "Re: thanks aaron"
    date: 2002-04-27
    body: "Big thanks for the service you provided to the KDE community. I read all issues of KC KDE from the beginning. And I really liked the view into the developers work on this great project. I hope it is possible to continue with KC KDE.\n\nbig thanks\n\nCAF"
    author: "CAF"
  - subject: "why not switch..."
    date: 2002-04-27
    body: "... to something like wikiwiki so the job is not up to a single person?\n\njust my 0.02 euro\nklaus"
    author: "klaus"
  - subject: "Re: why not switch..."
    date: 2002-04-27
    body: "well, it isn't a single person job, it just happens that up until now a single person has been primarily responsible for most of the work. the individual summarries can be written by whomever (there were several contributors to kc kde over time who did a great job writting summaries) and there is even a mailing list for coordinating the whole affair, but someone still needs to ensure that it all gets proof read, put into a single issue, that the links are good, write an intro and send it off to Zack @KT for inclusion into the kernel cousins site...\n\nthis is a task that can be fairly easily split up amongst several people. i'd probably even keep writing summarries if there was someone (or someones) to take care of the editing and do some of the summaries. i just can't do the whole thing myself right now."
    author: "Aaron J. Seigo"
  - subject: "Re: why not switch..."
    date: 2002-04-27
    body: "maybe it would be wise to spread teh work out among people who are members of the various lists, especially those who are on the lists mostly for informational purposes (and who dont spend loads fo time contributing actual content)\n\nI can do the kde-artists summary whenever there is anything to summarize."
    author: "Pablo Liska"
  - subject: "Re: why not switch..."
    date: 2002-04-27
    body: "you c aaron, it might work."
    author: "Juergen Appel"
  - subject: "Re: why not switch..."
    date: 2002-04-28
    body: "this would be great... please join the kc-kde@kde.org mailing list and announce yourself when you join. with enough people involved the load could indeed be distributed enough to ensure that kc kde continues."
    author: "Aaron J. Seigo"
  - subject: "Thanks"
    date: 2002-04-27
    body: "Well Done,\n\nIt takes a lot of time out of your personal time....\n\nThnaks again...\n\n\nHope someone takes it up or maybe a couple people...\n\nCoomsie\t;3)"
    author: "Coomsie"
  - subject: "Thanks"
    date: 2002-04-28
    body: "I'd like to thank you for providing us all the newsletter. Hope your work will be continued ... :)"
    author: "Me"
  - subject: "Thanks a lot!"
    date: 2002-04-28
    body: "As others, I would just like to say thanks for providing KC KDE - I have really enjoyed reading them."
    author: "Joergen Ramskov"
  - subject: "Collaboration - Ideas and Issues"
    date: 2002-04-30
    body: "\nI've done about 30 issues of KC Wine.  So I have a pretty good understanding of the processes involved.  This will probably just be a brain dump for anyone considering writing KC KDE (or any Kernel Cousin) issues:\n\nZack Brown has done a great job of putting together some tools to make writing these easy.  Check out <a href=\"http://kt.zork.net/author.html\">http://kt.zork.net/author.html</a> for\nmore details.  In particular, there's a stat tool for collecting list stats.  (For multiple lists you can just cat different mbox's together.)  I generally have a blank template that I fill in with the individual summaries.  I write all the XML with vi (though lately I've used some nicer editors, but only for syntax highlighting, which vim can do.)  When I'm done I throw the pages through xmllint to check everything.\n\nAs far as multiple people writing issues, I'd think it'd be easy.  Especially because KDE has so many mailing lists.  One person should be able to handle 2 or 3 lists pretty easily.  Or, you can even share one list by claiming threads.  \n\nExpect about 4 -6 hours per issue.  "
    author: "Brian Vincent"
  - subject: "I would love to"
    date: 2002-05-03
    body: "But have no time to go through the mailing lists... they are *big*."
    author: "Rajan R."
---
After 36 issues and over 300 mailing list thread summaries, I have decided to take a semi-permanent break as editor of the  <a href="http://kt.zork.net/kde/back-issues.html">Kernel Cousin KDE</a>  project. KC KDE was taking up quite a bit of my available time outside of work and family and, as enjoyable as it was to put KC KDE together, I have had to reprioritize things a bit. It would be a shame to see KC KDE simply go away forever, though. If anyone is interested in taking up the role of editor, I would be more than happy to get them started and co-edit the first few issues until they are confident and comfortable with the process.
<!--break-->
