---
title: "KDE Presence at CeBIT 2002"
date:    2002-03-07
authors:
  - "Inorog"
slug:    kde-presence-cebit-2002
comments:
  - subject: "I wish I was there"
    date: 2002-03-07
    body: "It sure would be nice to be ablt to see CeBIT some day. It's nice to know KDE will be there spreading the word."
    author: "Chakie"
  - subject: "Link to Danka should be \"http://www.danka.de/\"..."
    date: 2002-03-07
    body: "....as it is Danka Deutschland GmbH that is hosting the KDE people at CeBIT.\n\nBut, OTOH, this is a German language site, so pointing to our American bosses' site may be a good compromise...  ;-)\n\nHey, looking forward to meet some of you there!\n\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Re: Link to Danka should be \"http://www.danka.de/\"..."
    date: 2002-03-07
    body: "Unfortunately, I won't be able to attend CeBIT, otherwise I would love to come at visit the Danka stand.\n\nI can see on http://www.danka.de that the blurb about your presence at CeBIT includes a reference to the \"linux printing system\".\n\nForgive my ignorance (and I've not researched this at all), but what are Danka.de doing with respect to Linux. Why should Danka care about printing under Linux? I can see lots of pictures of big printers... but I don't see where Linux Desktop Printing fits in (I could imaging that you might stick linux + cups on a heavy duty print server - but that doesn't involve KDE..)\n"
    author: "Corba the Geek"
  - subject: "Re: Link to Danka should be \"http://www.danka.de/\"..."
    date: 2002-03-09
    body: "KDEPrint will be demonstrated at Danka booth (I'll be there during the weekend)."
    author: "Michael Goffioul"
  - subject: "Re: Link to Danka should be \"http://www.danka.de/\"..."
    date: 2002-03-10
    body: "> Forgive my ignorance (and I've not researched this at all), \n> but what are Danka.de doing with respect to Linux.\n\nDanka is a company selling printers and services. It is a maunfacturer-independent vendor, selling different brands. Of course our customers mainly use MS Windows (still), but we are also doing trainings, consulting and such. Our customers are recommended to use CUPS for their Unix and Linux systems. At CeBIT Danka is hosting a KDE demo point to show its new upcoming 3.0 release (with KDEPrint as one of the system utilities in the focus).\n\nKurt"
    author: "Kurt Pfeifle"
  - subject: "Maybe also in Italy"
    date: 2002-03-07
    body: "I've seen on italian i18n ml that some of us will be present in the next linuxevent (http://www.linuxevent.(it,org,net)) that will be held the 22nd/24th of May.\nAny suggestion about how to organize a serious booth with kde3?"
    author: "Gioele"
  - subject: "Re: Maybe also in Italy"
    date: 2002-03-07
    body: "When you want information about organizing an KDE booth at an event, the mailling list kde-events is the place to go. Please search in the Archive for the KDE-Promo FAQ, too.\n\nWhen you need informations about setting up a KDE booth, feel free to ask there.\n\nMfG\n Rainer"
    author: "physos"
  - subject: "Tickets for the Cebit"
    date: 2002-03-08
    body: "Hi, I live in Oslo, Norway, and I wouldlike to ask especially German readers whether they know of anybody handing out free tickets for the Cebit this year. I know German Telekom used to do that, but since I don't live in Germany I haven't been able to figure that out.\nI am trying to get a hold of 2 tickets.\n\nSincerely Johannes Wilm"
    author: "Johannes Wilm"
  - subject: "Re: Tickets for the Cebit"
    date: 2002-03-13
    body: "I think we should pay. It is very inportannt to go to this fair an beg for linux drivers, versions, support. It's the largest fair in Europe.\n\nI think 34 Euro ... that isn't to much..."
    author: "Hakenuk"
  - subject: "Re: Tickets for the Cebit"
    date: 2002-03-13
    body: "Who are you?"
    author: "Johannes Wilm"
  - subject: "SUSE "
    date: 2002-03-15
    body: "Also look at SuSE !\nThey present \"their\" KDE 3.0 with SuSE 8.0 \n\n:-)\nhappy Hacking !"
    author: "RitterJens"
---
The KDE Project is proud to announce its
presence at <a href="http://www.cebit.de/">CeBIT</a>, the world's largest computer trade show, taking place in Hannover from March 13 to March 20, 2002. (Note the correct booth location of the <b>KMail/Aegypten</b> presentation is <b>Pavillion P11/D 10-12</b>.)
<!--break-->
<Br><br>
<i>"For the first time, KDE will be present at no less than 2 to 4 partner-booths at CeBIT, each with a different emphasis on the great new features of KDE in its latest 3.0 incarnation,"</i> according to Torsten Rahn, member of the KDE development team.
<p>
At the <a href="http://www.sharp-usa.com/">Sharp</a> booth (Hall 1, Booth 7a2), KDE will present the new KDE 3.0
release as well as new developments in <a href="http://www.konqueror.org/embedded.html">Konqueror/Embedded</a> using the <A href="http://www.trolltech.com/products/embedded/index.html">Qt/Embedded</a>
toolkit and integrated in the Qtopia environment of the Sharp Zaurus PDA.
<p>
The second partner-booth is located at <a href="http://www.danka.de/">Danka</a> (Hall 1, Booth 6d2,
 partner-booth 6), where you can see a preview of the upcoming <A href="http://www.koffice.org/">KOffice</a> 1.2
 release. Learn also how the improved <a href="http://printing.kde.org/">KDE printing</a> architecture takes printing on the desktop to a whole new level.
<p>
Thirdly, <a href="http://www.gnupg.org/aegypten/">Project Ägypten</a> will demonstrate KMail/Aegypten for secure email exchange at the BSI booth (Pavillion P11/D 10-12).
<p>
And finally, rounding things up, KDE developers Torsten Rahn, Martin Konold and Ralf
Nolden will be giving daily speeches on KDE3 at the <a href="http://www.suse.com/">SuSE</a> booth (Hall 3, Booth
E45).
<p>
As a true open source  project, KDE also gives you the opportunity to talk to
the creators of the award-winning desktop environment.
<br><br><br>
<b>About KDE</b>: <a href="http://www.kde.org">KDE</a> is an independent, collaborative project by hundreds of
developers worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture. KDE provides a stable, mature desktop, an
office suite (KOffice), a large set of networking and administration tools,
and an efficient and intuitive development environment, including an
excellent IDE (KDevelop). KDE is working proof of the power of the Open
Source "Bazaar-style" software development model to create first-rate
technologies on par with and superior to even the most complex commercial
software.