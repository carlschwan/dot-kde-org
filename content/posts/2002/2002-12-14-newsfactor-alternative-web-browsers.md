---
title: "NewsFactor: Alternative Web Browsers"
date:    2002-12-14
authors:
  - "Dre"
slug:    newsfactor-alternative-web-browsers
comments:
  - subject: "Konqueror as a browsers is good"
    date: 2002-12-15
    body: "Hi there,\n\njust wanted to tell you how I love Konqueror. It's really a great browser, even was without tabs. I especially love view profiles, how bookmarks load so really quick and the font enlargement code.\n\nI tend to not have flash flugins enabled, because they only consume CPU and download and are advertising anyway.\n\nWhat I'd like to see are quick access buttons that would e.g. enable Javascript for the current page (reload maybe), current session. Same for proxy.\n\nBecause that's the occassions, where I go to the configuration. To enable JavaScript for a one-time view site on a per-site basis, to enable it globally, just for a single reload and of course, to disable the ad filtering proxy where it fails.\n\nHm... that would be it. Probably not hard to do, probably confusing to the user, probably somebody will do it? \n\nYours, Kay\n"
    author: "Debian User"
  - subject: "Re: Konqueror as a browsers is good"
    date: 2002-12-15
    body: "Install kdeaddons, then go to Tools -> HTML settings.  I don't think it remembers per-site though."
    author: "Hamish Rodda"
  - subject: "Re: Konqueror as a browsers is good"
    date: 2002-12-15
    body: "Wow, that already makes my day. :-)\n\nThanks, Kay"
    author: "Debian User"
  - subject: "Re: Konqueror as a browsers is good"
    date: 2002-12-15
    body: "I totally agree. I tried the suse 8.1-evaluation and it's really astonishing how far this KDE/linux thing has come. But why are the fonts so crappy in Mandrake compared to when i run Konqueror on SuSE? They really make konqueror shine even more."
    author: "KDE User"
  - subject: "Re: Konqueror as a browsers is good"
    date: 2002-12-16
    body: "Have you tried Mandrake 9.0? Personally, I am very impressed with the fonts."
    author: "Anon"
  - subject: "Re: Konqueror as a browsers is good"
    date: 2002-12-16
    body: ">  But why are the fonts so crappy in Mandrake compared to when \n>  i run Konqueror on SuSE?\n\nHave you enabled font antialiasing in KDE's Control Center in Mandrake ?. Maybe SuSE does it automatically for each user. Antialiased fonts in my ML 9.0 look very nice :-)"
    author: "NewMandrakeUser"
  - subject: "Re: Konqueror as a browsers is good"
    date: 2002-12-17
    body: "If you're talking 8.2 or some version lke that, there was a majoe break i the way one of teh RPM's was compiled (I honestly forget which I think maybe freetype, but I honestly forget).  There's was an upgrade out not long after 8.2's release, but there was a small hack to fix it (for the most part) first.\n\nEdit /etc/X11/XftConfig\n\nLook for some lines towards the bottom that mention removing AA from certain sized fonts.  I'm sorry I can't be more descriptive than this as it was a while back and I only did it once or twice.  You'll see something along these lines:\n\nmatch\n        any family == \"mono\"\n        all slant == roman\n        all weight < bold\nedit\n\n\nSo just look for those \"match\" sections.  Just shove a # infornt of the lines, and restart your X server.  I THINK  konsole was a bit messed up after that, but a quick re-config of that and all was well in Pretty Font Land, IIRC.  That's the hack to get pretty fonts in MDK 8.2 (or perhaps any distro that has messed up fonts, that I found anyay, YMMV)."
    author: "Xanadu"
  - subject: "Ironic"
    date: 2002-12-17
    body: "I find it ironic that the site crashes my Konq/2.2.2 on Mandrake 8.2 quite nicely.\n\nOnly other problem I've had with Konq is that it still doesn't do some Javascript very well.  Konq 3.0 definately has improved javascript and CSS support greatly, but I still find the odd javascript that Konq just doesn't do.\n\nIt is still my primary browser though, since it is very fast and works for nearly every site I go to."
    author: "anon"
  - subject: "Konqueror & PDF"
    date: 2002-12-18
    body: "is kghostview a PDF viewing program? because when i attempt to open a PDF file that's either embedded in konqueror or in the standalone kghostview program, i receive the message:\n\nCould not open file myfile.pdf.\n\ni've tried this with serveral different PDFs created at different sources.\n\nanyway, i read here that konqueror can render PDF's, but i don't know the secret.\n\nthis aside, konqueror is the best browser i've used....."
    author: "jhollett"
  - subject: "Re: Konqueror & PDF"
    date: 2002-12-18
    body: "Yes, kghostview can view PS and PDF files.\nIf neither work, maybe you're missing gs??\nCould be the famous renaming of the gs binary or something like that, IIRC some Mandrake KDE packages were fixed for that."
    author: "David Faure"
  - subject: "Re: Konqueror & PDF"
    date: 2003-01-03
    body: "i use KDE-3 konqueror on RH8.0 and feel, it responds\nmuch faster than in any other RH distro. the look\nis great too. as far as PDFs are concerned, my\nembedded PS/PDF viewer is working just fine.\ni moreover configured konqueror to use acroread\nplugin (of netscape) to display PDFs and it is\nworking fine too. i initially had problems with\nacroread, but, changing /etc/sysconfig/i18n fixed\nthe problem :\n\n#LANG=\"en_US.UTF-8\"\nLANG=\"en_US\"\nSUPPORTED=\"en_US.UTF-8:en_US:en\"\nSYSFONT=\"latarcyrheb-sun16\"\n\nacrobat's plugin can be downloaded from their\nsite. it's a bit slow initially but, has full PDF display\nfeatures, like bookmarks."
    author: "Dumbu"
  - subject: "truth?"
    date: 2002-12-18
    body: "I didn't find this article very convincing. It seemed to leave a lot of information out. Nice to spread the word about the existence of these other browsers, but I'd prefer it if the article provided a little more reliable and useful information..\n\nAnother thing:\n\n\"though Opera also pioneered tabbed browsing and mouse gestures, which have found their way into Mozilla and Konqueror, another IE alternative\"\n\nIs this true? Does Konqueror have mouse gestures? If so how do I use them? Even if so, this statement is still misleading since Mozilla only supports gestures through an extension and Konqueror only has tabbed browsing in a version that won't be officially released until next month.And then there's the whole question of what really counts as 'pioneered'.\n\nPersonally, I consider Phoenix to be better at actual web browsing than Konqueror (and Opera too) even though it is not officially released yet either. I love Konqueror for file management and I wish it were also the best browser so I didn't have to install anything else, but at the moment it is not. \n"
    author: "shar"
  - subject: "mouse gestures"
    date: 2002-12-19
    body: "Mouse gestures are definately something I would like to see in Konqueror, and I don't think they're already there. Actually, this is the only thing I'm missing. Konqueror has become my favourite browser and I usually don't use any other.\n\nDaniel"
    author: "Daniel Gruen"
  - subject: "Re: mouse gestures"
    date: 2002-12-20
    body: "You can try this (hopefully to be in 3.2) : http://dforce.sh.cvut.cz/~seli/en/khotkeys/"
    author: "L.Lunak"
  - subject: "OR: Help out Konqueror at browser wars :)"
    date: 2002-12-19
    body: "http://www.stephenbrooks.org/browserwars/\nMe"
    author: "Me"
---
The <a href="http://www.newsfactor.com/">NewsFactor Network</a> has
been running a series on "alternative" web browsers.  The
<a href="http://www.newsfactor.com/perl/story/20228.html">first installment</a> covered <a href="http://www.mozilla.org/">Mozilla</a> and the derived
Netscape; the
<a href="http://www.newsfactor.com/perl/story/20238.html">second installment</a> covers "A Night at the <a href="http://www.opera.com/">Opera</a>" and
"the Mighty <a href="http://www.konqueror.org/">Konqueror</a>".
A short, but well-written, story.
<!--break-->
