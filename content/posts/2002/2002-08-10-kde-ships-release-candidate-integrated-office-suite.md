---
title: "KDE Ships Release Candidate of Integrated Office Suite"
date:    2002-08-10
authors:
  - "Dre"
slug:    kde-ships-release-candidate-integrated-office-suite
comments:
  - subject: "First day SuSE packages broken"
    date: 2002-08-10
    body: "Please note that SuSE packages created before 9th August are missing some files for KWord and should be considered broken. Fixed packages are already available on some ftp mirrors but other still carry at the moment of this posting the broken versions."
    author: "Anonymous"
  - subject: "Re: First day SuSE packages broken"
    date: 2002-08-12
    body: "Okay - dumb question - how do I tell which ones I installed?\n\nrpm -qa |grep -i koffice\nkoffice-devel-1.2rc1-18\nkoffice-1.2rc1-18\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: First day SuSE packages broken"
    date: 2002-08-12
    body: "Revisions 18+ for i386 and 7+ for ppc are fixed. The broken packages had revisions from 0 to 2 AFAIR."
    author: "Anonymous"
  - subject: "Missing library requirement"
    date: 2002-08-10
    body: "Seems like libart is missing in the requirements list. If you install a binary package don't forget to download and install the provided libart package. If you compile from source, you can download libart at \n<A href=\"http://ftp.gnome.org/pub/GNOME/pre-gnome2/sources/libart_lgpl/libart_lgpl-2.3.8.tar.gz\">libart_lgpl-2.3.8.tar.gz</a>."
    author: "Anonymous"
  - subject: "gnome.org?"
    date: 2002-08-10
    body: "gnome.org? - is this cooperation between KDE and gnome for once? "
    author: "me"
  - subject: "Re: gnome.org?"
    date: 2002-08-10
    body: "libart is hosted in Gnome CVS, dunno you can call it a Gnome project. KDE and Mozilla use it just like any other library."
    author: "Anonymous"
  - subject: "Re: gnome.org?"
    date: 2002-08-11
    body: "Look for wvWare which is also kde-gnome. But mostly from kde people."
    author: "JC"
  - subject: "Re: gnome.org?"
    date: 2002-08-12
    body: "wvware mostly KDE people?\ndont let the maintainer Dom Lachowicz hear you say that.  "
    author: "Anon"
  - subject: "Re: gnome.org?"
    date: 2002-08-12
    body: "Hehe :)\n\nwvWare 1.0 is and was written by (and is maintained by) people closely associated with the AbiWord project, which I also happen to maintain. Not to mention its libole2 dependency that we helped write as well as the scaled-back Glib that I rewrote.\n\nwvWare 2.0 was a stalled collaboration between the KOffice folks and myself, of which the KDE folks did most of the work thus far. Please note that 'thus far' in this case means \"no where near functional.\" Werner, David Faure and a few Gnomies (myself included) chatted on IRC this weekend. Shaheed Hacque couldn't attend, unfortunately. Anyway, we talked about reviving the project, future plans, licensing, APIs, dependencies/requirements (such as libole2's replacement, libgsf - maintainer and lead author/architect Jody Goldberg[gnumeric], coauthored by me), division of labor, etc... and things seemed to go well. I'm only hoping that this turns out better than things did last summer.\n\nI am very much wvWare 1.0's current author and maintainer (mad props go to Caolan McNamara, though, for all of his hard work). wvWare 2.0 is very much incomplete and in a state flux, though it will be a teriffic product when it is finished. We hope to make it as good as, if not better than, wvWare 1.0 in a short amount of time.\n\nCheers,\nDom"
    author: "Dom Lachowicz"
  - subject: "Re: gnome.org?"
    date: 2002-08-13
    body: "Oops, sorry :-)"
    author: "JC"
  - subject: "Re: Missing library requirement"
    date: 2005-10-06
    body: "How do I obtain a copy of libART as required by KDE 3.4.2?\n\nver\n"
    author: "Vern Hoxie"
  - subject: "Re: Missing library requirement"
    date: 2005-12-03
    body: "http://ftp.gnome.org/pub/GNOME/sources/libart_lgpl/2.3/libart_lgpl-2.3.8.tar.gz"
    author: "Mark Shelby"
  - subject: "Wait a second...WHICH distributions?!?"
    date: 2002-08-10
    body: "I just want to say a HUGE \"thank you!\" to whoever worked to put together the RedHat RPMS.  I never thought I'd ever actually see links in the KDE announcement.  Thankyouthankyouthankyou."
    author: "ac"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-10
    body: "Jarl E. Gjessing, the same guy who recently contributed KDE 3.0.2 packages for Red Hat 7.3 (even in i386 and i686 versions)."
    author: "Anonymous"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-10
    body: "And you know what's even cooler?  You can apt-get them.  Get it setup at freshrpms.net and then point to the appropriate www.math.unl.edu directories.\n\nLater."
    author: "Bill"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-10
    body: "I seem to be missing a dependency:\n\nkdegraphics >= 3.0.2 is needed by koffice-1.2-1\n\nAny ideas where I can get this RedHat RPM?  The unl directories don't have this.  I can't even find this on RawHide!  Should I just --nodeps it?"
    author: "ac"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-10
    body: "I made these packages with my kde-3.0.2 packages installed.\nI could give any guaranties that they would work with older KDE versions, so I made the koffice package dependant to my own kde version.\nThis version is downlodable from KDE's ftp sites/mirrors and they are placed in the RedHat-Unofficial\ndirectory.\n\n"
    author: "Jarl E. Gjessing"
  - subject: "koffice problems"
    date: 2002-08-11
    body: "i am running redhat 7.3 with kde 3.1 stuff, after i intalled the koffice rpm provided from redhat, any koffice application gives me\n================================\nMutex destroy failure: Device or resource busy\n================================\n\nany ideas on what to do ??\n\nthanks"
    author: "crazycrusoe"
  - subject: "Re: koffice problems"
    date: 2002-08-11
    body: "My suggestion would be to get rid of the kde 3.1 bits, and downgrade to kde 3.0.2."
    author: "Rex Dieter"
  - subject: "Re: koffice problems"
    date: 2002-08-14
    body: "I have the same problems w/ teh mutex destroy message.  I\"ve installed kde and koffice by using rex's apt source on an rh7.3 box.\n\napt-get install kde3\napt-get install koffice\n\nopening any koffice application gives me the error.\n\nany other ideas?\nthanks!\n"
    author: "Anonymous"
  - subject: "Re: koffice problems"
    date: 2002-08-17
    body: "I think I've finally squashed the \"mutex detroy\" error problem.  Try \napt-get update && apt-get upgrade \nto get the latest version."
    author: "Rex Dieter"
  - subject: "Re: koffice problems"
    date: 2002-12-11
    body: "What is apt-get update?  Is that an application?\n\nThanks, I've run into the same problem...\n\n--\nEvan"
    author: "Evan Moseman"
  - subject: "Re: koffice problems"
    date: 2002-12-12
    body: "Yes, apt is a rpm dependancy resolution tool and greatly eases the task of keeping your machine up-to-date.  For more details about the use of apt to keep your KDE current, check out http://kde-redhat.sourceforge.net/"
    author: "Rex Dieter "
  - subject: "Re: koffice problems"
    date: 2003-01-07
    body: "running (or trying to) the Koffice 1.2-0.rc1.4 on RedHat 8.0. I got the mutex error.  I can't find any .desktop files in  $HOME.  I installed apt and updated things a bit (but nothing for kde/koffice came up).  Koffice still yields the mutex error.\n\nany more ideas?\n\nwhat did apt upgrade for you all?"
    author: "JP"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-11
    body: "FYI, the KDE 3.0.2 (including koffice-1.2rc1) RPMS referenced on my website, http://www.math.unl.edu/~rdieter/ are *not* the same as the \"unofficial\" redhat kde 3.0.2 rpms available from http://www.kde.org/\n\n-- Rex"
    author: "Rex Dieter"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-11
    body: "As far as I can see, you're packages are RawHide packages with you're own patches!!??\n\nThe meaning with my packages was, to make then run on a std. RedHat-7.2 without having to apply a plethora of new packages.\n\nI have tried to install the RawHide packages on my machine, and the dependency problem was a nightmare, but then again, the RawHide is not meant for production or stable workstation, but for developers and tasters (correct me if I'm wrong).\n\nIn fact the user dont have to download any packages other than the KDE packages, and just use packages delivered woth RedHat, this makes it an ease to install.\n"
    author: "Jarl E. Gjessing"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-11
    body: "Sorry, I meant testers not tasters :-)"
    author: "Jarl E. Gjessing"
  - subject: "Re: Wait a second...WHICH distributions?!?"
    date: 2002-08-11
    body: "I ported the rawhide packages to RedHat 7.2/7.3, yes.  I chose this so the kde I (and my department used) would mirror redhat's development (and redhat 8.0) as close as possible (and I get the advantage of seeing/watching redhat's bug-tracking via bugzilla.redhat.com).\n\nAs for dependancies, well, yes, there are some, and that's one reason why I my kde rpms are apt'able (apt-get), so that dependancies are handled for you."
    author: "Rex Dieter"
  - subject: "debian packages"
    date: 2002-08-10
    body: ".... are there any debian (3.0*) packages available too .. ?\n\nthx,\n  deucalion"
    author: "deucalion[tpoc]"
  - subject: "Re: debian packages"
    date: 2002-08-10
    body: "Not yet, watch http://people.debian.org/~bab/kde3/"
    author: "Anonymous"
  - subject: "Re: debian packages"
    date: 2002-08-11
    body: "thx!"
    author: "deucalion[tpoc]"
  - subject: "Re: debian packages"
    date: 2002-08-12
    body: "its no problem compiling them though. just add an extra entry to the /debian/changelog and go \"dpkg-builtpackage\" in the packages root-dir. (I tried it today). the only thng you need to make sure is that you have libxslt1-dev installed before compiling. For all other dependencies you should get error-messages rather in the beginning, but for this one, you don*t get before some time at the end of the built-process."
    author: "Johannes Wilm"
  - subject: "it's a great release"
    date: 2002-08-10
    body: "I can load my koffice 1.1 work without any problem and all visual glitches have been settled. I just love the frames. Great work. a huge thx."
    author: "happy user"
  - subject: "word filters"
    date: 2002-08-10
    body: "Can koffice export to MS Word .doc yet? "
    author: "me"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "shouldn't reply to my own post, but i found the filters status page (http://www.koffice.org/filters/status.phtml).\n.doc import but no export (this wouldn't be so bad if rtf support was good, but apparently both import/export are alpha)\n.xls import, no export (kspread)\npowerpoint import (alpha), no export (kpresenter)\n\nAre people working on these export filters? (as much as we might hate it MS's file formats are the standard these days - i apply for a job, they ask me to email my cv in .doc format)  Also is anyone working on OpenOffice import/export filters? (probably the second most important file format to support)"
    author: "me"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "KOffice is still lack of developers. Visit the filters status page, then go on to each filter, then count how many (part-time) developers are working on it. Filters will be substantially improved, but if only KOffice gets more help (code/test/money/etc)"
    author: "ariya"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "Oh how I hate all this jabbing about filters, like when you see a reviw of a office program you realy get a rant of how good/bad the filters are and not the more important: the use and fuctionality of said program. If people could start to think they vould see filters are NOT the be all for a package, but more a nice to have feature. Back in the old days World Perfect had closer to 90% of the wordprocessor market and Lotus 1-2-3 nearly the same for spreadsheets, but now Word and Exel have taken their places. This was not becouse of supperior filters I can tell you, you usaually got the text and mostly the pictures/clipart but the layout and formating got more or less mangled every time and had to be redone.\n\nMost of what you send/recive are ment as read only anyway, like your CV, and there Koffice realy shines thanks to the nice kprint framwork. Use PDFs, it's Adobes and realy not free but it's pretty close and cross platform. Besides I consider sending word/exel macroviruses very bad form:) \n\nAnd if you recive lots of heavy formated doc/xls files, the crossover-plugin don't cost that much and ms-word and xl viewers are gratis dowloaded from M$.\n\nIf you have lots of docs you regulary edit/use or some big not finished docs, and you want to change app, you have the only big reason to need filters."
    author: "Morty"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "I don't like recieving .doc's from people, but when i'm applying for jobs i'll do whatever they want - and most specify .doc's unfortunately"
    author: "me"
  - subject: "Re: word filters"
    date: 2002-08-11
    body: "Yeah, see the problem, have always hated the jobbhunting bit and this make it suck even more:) BTW have you tried to send a PDF anyway, with a polite note sayig you don't own word, but you could get back to them with a .doc file if they can't use the PDF (perhaps pointier to Acrobat reader). It's a gamble, but they would notice your application and thats usually good:) If one of the jobb requrements are ms-office skills, probably not:).\n\nMy last replay was mostly becouse today I read the second \"test/reviw\" of office progs. this week and both realy bolied down comparison of the filters, not what I was interested in (and consider more important) the usability, functionality, speed etc. Even payed for the crappy magazine:(\n "
    author: "Morty"
  - subject: "Re: word filters"
    date: 2002-08-12
    body: "On the other hand. Some do not accept doc-files because of the virus risc.\nPdf-files are always ok."
    author: "A"
  - subject: "Re: word filters"
    date: 2002-08-11
    body: "Oh how I hate when people tell me what features are important to me.  For many people if they can't seemlessly send and receive word documents from co-workers/clients the office suite is useless.  Count yourself lucky that these features are not needed by you.  Sure there are ways to get around it (PDFs, txt files etc), but they are unwieldy and difficult to understand for non-technical people (What do you mean you don't have Microsoft Excel?).  Until KOffice has a solid set of Import/Export filters it is simply unusable in most business environments. "
    author: "beergeek"
  - subject: "Re: word filters"
    date: 2002-08-12
    body: "> Oh how I hate when people tell me what features are important to me.\n\nReally? Maybe there's a reason they tell you? In any case the point wasn't aimed at you. It was made in general. And it was a good point. Having good import and export without a solid feature set is a sure way to alienate everyone.\n\n> For many people if they can't seemlessly send and receive word documents from co-workers/clients the office suite is useless.\n\nMaybe so, which is why I specify koffice docs in my office. There's always openoffice.org if for some reason I have to sent to the brain dead.\n\n> Count yourself lucky that these features are not needed by you.\n\nLuck has nothing to do with it. It was freedom of choice... goes well with entrepneurial spirit. If you're counting on luck you're always going to be living off whatever people will hand you because ships don't come in that you don't send out.\n\n> Sure there are ways to get around it (PDFs, txt files etc), but they are unwieldy and difficult to understand for non-technical people\n\nWhat are you a troll?! Do you know how to use the KDE print system to print to PDF. For that matter Postscript and PDF are options with ghostscript printing for years. Although I don't run windoze I find it really difficult to believe they won't have a PDF reader (they're still free and very popular on the web) and that there won't be a pdf association that makes it a one click affair. Besides... Wurd docs may look different from system to system for settings or fonts where as pdf is... well Portable Document Format. Duh?\n\n> (What do you mean you don't have Microsoft Excel?)\n\nIf you're ashamed of your computing choices switch... or did you already? I told a college student in the gym I used Linux and he looked at me with awe and asked \"What make's it so great?\". I tell other business people I don't use M$ software and they look at me like I had money falling out of my pockets and ask how I do it. I can assure you anyone who cuts the checks or hires the huge support staff is not going to scoff at you. However the ditzy secretary might... I'm sure you want to impress her.\n\n> Until KOffice has a solid set of Import/Export filters it is simply unusable in most business environments. \n\nSays you? I believe the previous poster was right. When Koffice produces a solid and usable suite that fits well within an organization it will be more widely used. It is more friendly and less bloated than OO and it has a lot of promise.\n\nAs for the filter issue, in case you're forgetting that Koffice is widely developed by volunteers, what is needed is for more people to step forward. I have my own project (Quanta) and I don't frankly care about the filters exporting because I'm sending PDF. If it means so much to you then I suggest you get off your ass and do something about it. Free software is about community. Everyone is welcome to use it but free software critics... may as well be freeloaders to complain about what is give to them for free.\n\nWhat is really needed for document exchange is open formats, preferably based on XML. Your reasoning does not take into account that M$ changes formats regularly to force upgrades and to mess with competitors by keeping them on a treadmill. In fact M$ Office does not even do a perfect job with the overly complex and difficut format... which dumps out all kinds of personal information about you in plain text...\n\nThankfully the world is not entirely populated by people with attitudes like this that utterly assure the perpetual domination of M$. Your reasoning would nix Koffice if M$ Office had only a 25% market share. Fortunately other will take initiative on other critical issues where MS office is weak... like standards, unwillingness to fix long standing bugs, bloat, privacy issues and hideous costs.\n\nBTW what I hate is people saying something in public forums that is poorly thought out and works to perpetuate the use of M$ software. The Koffice team has their priorities right."
    author: "Eric Laffoon"
  - subject: "Re: word filters"
    date: 2002-08-13
    body: "Eric Laffoon you are completely missing the point. \n\nLots of people (myself included) CANNOT ditch MSOffice because the most of the world assumes you can read .ppt and .doc. Sure it's easy to print to pdf in KDE but what good does that do me when the people I work with use Windows, Word, and Powerpoint? It isn't easy for them!\n\nI think you're absolutely wrong to think that internal features are more important than filters - most people use a fraction of the capabilities of Word, and would never notice their absence. Not being able to read (for instance) the de-facto standard for presenting (.ppt) EVERYONE will notice.\n\nI've tried KOffice and quite liked it, but until it has functional filters I'm simply not using it. \n\nThank heavens for openoffice!"
    author: "Edward Moyse"
  - subject: "Re: word filters"
    date: 2002-08-12
    body: "Umh... \nI'm running a small office with about 10 desktop linux pc for\nhalf a year now (previously windows pc). There's no MS-Office\nsoftware, just OpenOffice...\nWe are constantly\nrecieving MS-Word .docs by mail, but in half a year there were only\nabout 5 .doc-files which could not be opened with OO... \nmost users like the system (because of easy e-mail handling and sweet\nkprinter, everyone is sending .pdf-files now: it's fun. They even\nuse KOrganizer) There's only one user which is bitching about linux\nall the time. If he get's an .doc-file which is not displayed 100%\ncorrectly by OO he's always getting mad...\nI'd really like to have 100% correct filters for MS-Office but this is\nimpossible. We'll have to live with it."
    author: "Thomas"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "You can consider the RTF import filter as being much more than \"alpha\".\n\nHowever, the RTF export filter is not working correctly. (That is the problem of having lost a maintainer.)\n\nAs for whatever someone work for these filters, it is always problem: we are lacking manpower.\n\nHowever DOC import, RTF export, OpenOffice Writer export and XLS import have good chances for KOffice 1.3.\n\nVolunteers welcomed!\n\nHave a nice day/evening/night!"
    author: "Nicolas GOUTTE"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "OpenOffice writer export would be a good start :-) - it has good .doc export, so could be used to do the conversion. "
    author: "me"
  - subject: "Re: word filters"
    date: 2002-08-10
    body: "No.  See:\n\nhttp://www.koffice.org/filters/status.phtml"
    author: "RobW"
  - subject: "Off topic"
    date: 2002-08-10
    body: "Anyone got any idea what is happening to apps.kde.org? No new applications for what, 3 weeks??"
    author: "Bojan"
  - subject: "Re: Off topic"
    date: 2002-08-10
    body: "Sorry, I meant apps.kde.com"
    author: "Bojan"
  - subject: "Re: Off topic"
    date: 2002-08-10
    body: "Read the lastest \"News\" entry there to get an idea."
    author: "Anonymous"
  - subject: "Oh dear -- bad filters problem"
    date: 2002-08-10
    body: "Don't worry, I'll file a bugreport on this, but...\n\nWhen I try to open any Word document, I get \"Could not import file of type application/x-msword\".\n\nThe problem is that KWord sniffs MIME-types, and if the MIME-type is not on the \"I have a filter for this\" list, KWord doesn't even give you the option to choose the filter yourself (it doesn't even display it in the file-open dialog!).  Thus, even though it has an \"application/msword\" filter, KWord isn't smart enough to know that \"application/x-msword\" and \"application/vnd.msword\" (I think) mean EXACTLY the same document format.  There may be even more variations.  Multiple MIME-types should be able to point to one filter.\n\nAt the very least, KWord should provide SOME sort of method by which a user can open their documents when KWord does have an appropriate filter, but the MIME-type sniffer doesn't know this.  Along those lines, the file-open dialog should give the user the option to show all files regardless of MIME-type, so that they can choose the filter manually.\n\nAs the administrator of my home machine, I know I can work around this myself.  However, I think KWord is far too confident in its MIME-type sniffer, and the need for manual per-MIME-type workarounds should be removed."
    author: "ac"
  - subject: "Re: Oh dear -- bad filters problem"
    date: 2002-08-10
    body: "What's your work-around anyway ?"
    author: "ac"
  - subject: "Re: Oh dear -- bad filters problem"
    date: 2002-08-11
    body: "David answered this further down the thread.  Go to KControl File Associations, create a new file association called application/msword and give it the same extension, applications, and embedding options as application/x-msword, then delete application/x-msword.  Then log out and log back in again and you have it!\n\nUnfortunately, this may need to be done for every MIME type you need to change.  Fortunately, application/x-msword may be the only one you need to change."
    author: "ac"
  - subject: "Re: Oh dear -- bad filters problem"
    date: 2002-08-10
    body: "In short (for long, see: http://bugs.kde.org/db/46/46328.html ):\n\n- The official KDE3 mime type for MS Word is application/msword\n- Therefore it looks like a distribution problem.\n- KOffice as KDE is based on mime types.\n- The MS Word import is done by the OLE import filter. It needs to know what kind of file to expect.\n\nHave a nice day/evening/night!\n\n"
    author: "Nicolas GOUTTE"
  - subject: "Re: Oh dear -- bad filters problem"
    date: 2002-08-11
    body: "SuSE bug, IIRC. They renamed the mimetype.\nRename it back (file associations kcontrol module).\nIIRC this is already fixed in SuSE."
    author: "David Faure"
  - subject: "Re: Oh dear -- bad filters problem"
    date: 2002-08-12
    body: "The problem is NOT suse.\n\n>At the very least, KWord should provide SOME sort of method by which a user\n>can open their documents when KWord does have an appropriate filter, but the\n>MIME-type sniffer doesn't know this.\n\nThat is the bug.\nIf the user wants kwort to open file_xy with the word-filter then kword should do that. (In most cases the user is more intelligent then kword)"
    author: "matthias"
  - subject: "Re: Oh dear -- bad filters problem"
    date: 2002-08-12
    body: "I hate to disagree with myself on this, but I think it's not necessarily a \"bug\" in the strictest sense of the word.  If you want to push this idea, go ahead, but I think you should refer to it as a \"feature\" or an \"enhancement\".\n\nBasically there are tons of MIME types, many of them totally erroneous, or at least outdated.  As it was explained to me by one of the smart folks at KDE (who I actually believe!) there are only IANA-approved MIME types and then there is crap.  KDE really only recognizes the IANA-approved MIME types properly.\n\nThus, with a properly-configured OS, this is not a bug at all.  What we're talking about is a compatibility feature to deal with misconfigurations.  Which is fine, too, if that's what you want.  But it's certainly lower-priority than an actual bug.  I'm quite satisfied with my bugreport being \"closed\".  Feel free to differ."
    author: "ac"
  - subject: "Kontour seems to be totaly broken."
    date: 2002-08-11
    body: "Have you someone tried Kontour in this KOffice version? It seems that this package is totaly broken. I can't draw any lines, curves, bezier... - there are also tons of screen update bugs. Is the develompent of this application frozen or I'm missing something?\nI'm using MDK-8.2 rpm package.\n"
    author: "Zyzstar"
  - subject: "Re: Kontour seems to be totaly broken."
    date: 2002-08-11
    body: "The RedHat packages's kontour works just fine.\nI'm sorry to say this since this either means that it is the MDK packages or dependency problems on you're mahine"
    author: "Jarl E. Gjessing"
  - subject: "Re: Kontour seems to be totaly broken."
    date: 2002-08-12
    body: "Ups I have to modify my comment.\nYes the lines dont work, I just read the header as if kontour did not work at all, so I tried it and I used everything BUT lines sorry.\nI can see, that the status bar still shows the last selected, e.g. if you draw a reactangle and them select line, then the status bar still says rectangle mode."
    author: "Jarl E. Gjessing"
  - subject: "Re: Kontour seems to be totaly broken."
    date: 2002-08-12
    body: "Same with the SuSE packaged version.  No lines work, and if a polygon crosses the bottom border as you drag it a large black rectangle rises up from that border.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Kontour seems to be totaly broken."
    date: 2002-08-12
    body: "Well, on Mandrake 8.2, I can't draw lines in Kivio 1.0 neither. So maybe it's a problem with QT (3.0.4) ? I didn't have this problem on Kivio 1.0 on Suse 7.3 (QT 3.0.3)."
    author: "Julien Olivier"
  - subject: "What is libwv2?"
    date: 2002-08-12
    body: "I downloaded KOffice from CVS KOFFICE_1_2_RC1 and I get this when I configure:\n\nconfigure: WARNING: Could not find libwv2 anywhere, check http://www.sourceforge.net/projects/wvware/\n\n{all one line}\n\nWell, I have checked SF and this is only available on CVS.\n\nIs this correct that it requires this??\n\nOr, did somebody goof?\n\nOr, was it my mistake to use CVS to download it??\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: What is libwv2?"
    date: 2002-08-12
    body: "Ignore that warning. This is a very alpha development (of a new msword import filter) that's not part of this release. Ok, warning disabled for 1.2-final."
    author: "David Faure"
  - subject: "CVS import bug"
    date: 2002-08-12
    body: "Can KSpread now import CSV files of 100,000+ lines without locking up?"
    author: "ac"
  - subject: "Re: CVS import bug"
    date: 2002-08-12
    body: "I haven't seen a bug report on this, so it doesn't seem to be a problem to anyone.\n\nBTW: KSpread does only support 32,000 Rows, so how would you put in 100,000 lines?"
    author: "Peter"
  - subject: "Re: CVS import bug"
    date: 2002-08-13
    body: "Thats the problem.  It never imports them.  When I tried to import a CSV file with about 100,000 lines, KSpread stopped responding (wouldn't redraw the window) and started using 100% CPU.  I let this go overnight thinking it was just going to take a while to import, but in the morning, eight hours later, it was still using CPU and the window wasn't responding."
    author: "ac"
  - subject: "Re: CVS import bug"
    date: 2002-08-14
    body: "I've often noticed the KDE's error handling leaves a lot to be desired. For instance when my IMAP server was broken, any attempt to use it in KMail wyould crash Kmail. It would be nicer if it would provide an informative error message. Thanks,\n\nDavid"
    author: "David Findlay"
  - subject: "Re: CVS import bug"
    date: 2002-08-15
    body: "Thanks for filing a bug report (I'm sure you did that!)"
    author: "Peter"
  - subject: "Re: CVS import bug"
    date: 2002-08-15
    body: "Hi,\n\nI agree about the error handling, but really, to handle more then 20k lines of data, you don't need a spreadsheet, you need a database.  A lot of people I'm sure would agree with that statement.\nBut then again, you'd need a tool to get this data into a database table.\nOn top of that, you probably want to manipulate that data.\nTo make a long story short, you need tools in between the data-file (csv, text, xml, ...) and the database table.  Not just residing on mySQL, Oracle, or whatever, but on any database.\nThese type of conversions are done routinely in data warehouse projects.  The converions are called ETTL: Extraction Transformation Transportation & Loading.\nI'm proposing to write (in fact a large part of it is done) a tool for KDE called Kettle.  Send me a mail by the end of september and we'll start hacking.\nUntil then I'll be off on my honeymoon.\n\nCheers,\n\nMatt\nhttp://www.ibridge.be/kettle/\n"
    author: "Matt"
  - subject: "Wide Page?"
    date: 2002-08-12
    body: "Does anyone know why this page is so wide?  Can't spot anything that would cause it."
    author: "Navindra Umanee"
  - subject: "Re: Wide Page?"
    date: 2002-08-12
    body: "At the top of the press release (line 56 of the page source), it says \"<table [...] width=\"780\"  [...]>\". Removing the width attribute seems to fix the problem.\n\nNils"
    author: "Nils"
  - subject: "Re: Wide Page?"
    date: 2002-08-12
    body: "Line 58:\n\n<table border=\"0\" cellapacing=\"0\" cellpadding=\"0\" width=\"780\" align=\"center\"><tr><td>\n\nJust replace 780 by 100%."
    author: "Julien Olivier"
  - subject: "Re: Wide Page?"
    date: 2002-08-12
    body: "Thanks to both of you!"
    author: "Navindra Umanee"
  - subject: "Command line error."
    date: 2002-08-15
    body: "Hmmm, has anyone seen this error:\n\"Mutex destroy failure: Device or resource busy\"\nWhat does it mean?\n\n"
    author: "Tommy"
  - subject: "mmm."
    date: 2002-08-16
    body: "And krita?? \nIs included in the last release? "
    author: "lescrh"
  - subject: "Re: mmm."
    date: 2002-08-16
    body: "Not in the SuSE package, likely not in any. :(\n\nIncidently, KThesaurus crashes on launch.  I'm assuming that something that big is known.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
---
The <a href="http://www.koffice.org/">KOffice</a> team yesterday announced the release of KOffice 1.2 rc1.  It marks the last test release before
KOffice 1.2, scheduled for early next month, hits the Net.
Highlights since last month's beta2 release include a substantial number
of fixes and stability and performance enhancements, including important
corrections to the KWord and KPresenter WYSIWYG display, It also features new
read-only <a href="http://www.konqueror.org/">Konqueror</a> plugins for
each KOffice component, a new WordPerfect export filter for KWord, and
improvements to the KWord RTF import filter.  You can view a more detailed
<a href="http://www.koffice.org/announcements/changelog-1.2rc1.phtml">list
of changes</a> at the KOffice website, head straight to the
<a href="http://download.kde.org/unstable/koffice-1.2-rc1/">download</a>
directory, or read the release announcement below.


<!--break-->
<p />&nbsp;
<p />
<table border="0" cellapacing="0" cellpadding="0" width="100%" align="center"><tr><td>
<p>
  DATELINE AUGUST 9, 2002<br />
  FOR IMMEDIATE RELEASE
</p>
<h2>
  KDE Ships Release Candidate of Free Integrated Office Suite
</h2>
<p align="justify">
  <strong>
  Konqueror Read-Only Embedding, WordPerfect Export Filter and Improved
  Stability Highlight KOffice Release Candidate for Linux and Other
  UNIXes</strong>
</p>
<p align="justify">
  August 9, 2002 (The INTERNET).
  The <a href="http://www.kde.org/">KDE Project</a> today announced the
  immediate release of
  <a href="http://www.koffice.org/">KOffice</a> 1.2rc1, the third
  preview release for KOffice 1.2,
  <a href="http://developer.kde.org/development-versions/koffice-1.2-release-plan.html">scheduled</a>
  for final release early next month.  KOffice is an integrated office suite for
  KDE, the leading desktop for Linux.  KOffice utilizes free and open
  standards for its document formats, component communication and component
  embedding.
</p>
<p align="justify">
  This release is a public testing release to resolve any
  remaining stability issues prior to the final release next month.
  Besides a substantial number of fixes and stability and performance
  enhancements since the KOffice beta2 release last month, including
  important corrections to the KWord and KPresenter WYSIWYG display,
  KOffice 1.2rc1 features new read-only
  <a href="http://www.konqueror.org/">Konqueror</a> plugins for
  each KOffice component, a new WordPerfect export filter for KWord, and
  improvements to the KWord RTF import filter.
  A more detailed
  <a href="http://www.koffice.org/announcements/changelog-1.2rc1.phtml">list
  of changes</a> and <a href="http://www.koffice.org/releases/">notes about
  the release</a> are available at the KOffice
  <a href="http://www.koffice.org/">web site</a>.
</p>
<p align="justify">
  This release, available in
  <a href="http://i18n.kde.org/teams/distributed.html"><strong>36</strong></a>
  languages, includes
  a frame-based, full-featured word processor
    (<a href="http://www.koffice.org/kword/">KWord</a>);
  a presentation application
    (<a href="http://www.koffice.org/kpresenter/">KPresenter</a>);
  a spreadsheet application
    (<a href="http://www.koffice.org/kspread/">KSpread</a>);
  a flowchart application
    (<a href="http://www.thekompany.com/projects/kivio/">Kivio</a>);
  business quality reporting software
    (<a href="http://www.thekompany.com/projects/kugar/">Kugar</a>);
  and two <em>alpha-quality</em> vector-drawing applications
    (<a href="http://www.koffice.org/kontour/">Kontour</a> and
     <a href="http://www.koffice.org/karbon/">Karbon14</a>).
  Additionally, KOffice includes robust embeddable charts
    (<a href="http://www.koffice.org/kchart/">KChart</a>)
  and formulas
    (<a href="http://www.koffice.org/kformula/">KFormula</a>)
  as well as a built-in thesaurus (KThesaurus)
  and numerous import and export
    <a href="http://www.koffice.org/filters/">filters</a>.
</p>
<p align="justify">
  KOffice 1.2rc1 complements the release of
  <a href="http://www.kde.org/announcements/announce-3.0.2.html">KDE 3.0.2,</a>
  last month.  KDE is an international, powerful and easy-to-use
  Internet-enabled desktop for Linux and other UNIXes, and together with
  KOffice constitutes the only Open Source project to provide a complete
  desktop and productivity environment for Linux/UNIX.
</p>
<p align="justify">
  KOffice and all its components (including KDE) are available
  <em><strong>for free</strong></em> under Open Source licenses from the KDE
  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/src/">http</a>
  or <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
</p>
<h4>
  Installing KOffice 1.2rc1 Binary Packages
</h4>
<p align="justify">
  <u>Binary Packages</u>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.0 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/">http</a> or
  <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>
<p align="justify">
  Please note that the KDE Project makes these packages available from the
  KDE web site as a convenience to KDE users.  The KDE Project is not
  responsible for these packages as they are provided by third
  parties - typically, but not always, the distributor of the relevant
  distribution - using tools, compilers, library versions and quality
  assurance procedures over which KDE exercises no control. If you
  cannot find a binary package for your OS, or you are displeased with
  the quality of binary packages available for your system, please read
  the <a href="http://www.kde.org/packagepolicy.html">KDE Binary Package
  Policy</a> and/or contact your OS vendor.
</p>
<p align="justify">
  <u>Library Requirements / Options</u>.
  The library requirements for a particular binary package vary with the
  system on which the package was compiled.  Please bear in mind that
  some binary packages may require a newer version of Qt and other libraries
  than was shipped with the system (e.g., LinuxDistro X.Y
  may have shipped with Qt-3.0.0 but the packages below may require
  Qt-3.0.3).  For general library requirements for KDE, please see the text at
  <a href="#source_code-library_requirements">Source Code - Library
  Requirements</a> below.
</p>
<p align="justify">
  <a name="package_locations"><u>Package Locations</u></a>.
  At the time of this release, pre-compiled packages are available for:
</p>
<ul>
  <li>
    <a href="http://www.conectiva.com/">Conectiva Linux</a><!-- (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/Conectiva/README">README</a>)-->:
    <ul>
      <li>
        8.0:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/Conectiva/conectiva/RPMS.koffice/">Intel i386</a>
      </li>
    </ul>
  </li>
  <li>
    <a href="http://www.linux-mandrake.com/en/">Mandrake Linux</a>:
    <ul>
      <li>
        8.2:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/Mandrake/8.2/RPMS/">Intel i586</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/Mandrake/noarch/">language packages</a>)
      </li>
      <li>
        8.1:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/Mandrake/8.1/RPMS/">Intel i586</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/Mandrake/noarch/">language packages</a>)
      </li>
    </ul>
  </li>
  <li><a href="http://www.redhat.com/">RedHat Linux</a>
      (<em>unofficial</em>)
  <ul>
    <li>7.3:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/RedHat-Inofficial/7.3/RPMS/">Intel i386</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/RedHat-Inofficial/noarch/">language packages</a>)
  </ul></li>
  <li>
    <a href="http://www.suse.com/">SuSE Linux</a>:
    <ul>
      <li>
        8.0:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/i386/8.0/">Intel i386</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/noarch/">language packages</a>)
      </li>
      <li>
        7.3:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/i386/7.3/">Intel i386</a> and <a href="http://download.kde.org/stable/koffice-1.2rc1/SuSE/ppc/7.3/">PowerPC</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/noarch/">language packages</a>)
      </li>
      <li>
        7.2:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/i386/7.2/">Intel i386</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/noarch/">language packages</a>)
      </li>
      <li>
        7.1:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/i386/7.1/">Intel i386</a> and <a href="http://download.kde.org/stable/koffice-1.2rc1/SuSE/ppc/7.1/">PowerPC</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/noarch/">language packages</a>)
      </li>
      <li>
        7.0:  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/i386/7.0/">Intel i386</a> (<a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/noarch/">language packages</a>)
      </li>
    </ul>
  </li>
</ul>
<p align="justify">
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages may become available over the
coming days and weeks.
</p>
<h4>
  Compiling KOffice 1.2rc1
</h4>
<p align="justify">
  <a name="source_code-library_requirements"></a><u>Library
  Requirements</u>.
  KOffice 1.2rc1 requires the following libraries:
</p>
<ul>
  <li>
    kdelibs 3.0, though kdelibs 3.0.2 is recommend.  Both release can be
    downloaded following the instructions in the respective press releases
    (<a href="http://www.kde.org/announcements/announce-3.0.html#package_locations">3.0</a>,
    <a href="http://www.kde.org/announcements/announce-3.0.2.html">3.0.2</a>)
    at the KDE website.  For more information on these kdelibs releases, please
    see the <a href="http://www.kde.org/announcements/announce-3.0.html">KDE
    3.0 press release</a>.
  </li>
  <li>
    Qt 3.0.3, which is available in source code from Trolltech as
    <a href="ftp://ftp.trolltech.com/qt/source/qt-x11-3.0.3.tar.gz">qt-x11-3.0.3.tar.gz</a>, or later.
    and
  </li>
  <li>
    For reading help pages and other KOffice documentation,
    <a href="http://xmlsoft.org/">libxml2</a> &gt;= 2.4.9 and
    <a href="http://xmlsoft.org/XSLT/">libxslt</a> &gt;= 1.0.7.
  </li>
</ul>
<p align="justify">
  <u>Compiler Requirements</u>.
  Please note that some components of 
  KOffice 1.2rc1 (such as the Quattro Pro import filter
  and <a href="http://www.koffice.org/kchart/">KChart</a>) will not
  compile with older versions of <a href="http://gcc.gnu.org/">gcc/egcs</a>,
  such as egcs-1.1.2 or gcc-2.7.2.  At a minimum gcc-2.95-* is required.
</p>
<p align="justify">
  <a name="source_code"></a><u>Source Code/SRPMs</u>.
  The complete source code for KOffice 1.2rc1 is available for free download
  via one of the KDE
  <a href="http://download.kde.org/unstable/koffice-1.2-rc1/src/">http</a>
  or <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
  Additionally, source rpms are available for the following distributions:
</p>
<ul>
  <li>
    Conectiva <a href="http://download.kde.org/unstable/koffice-1.2-rc1/Conectiva/SRPMS.koffice/">8.0</a>
  </li>
  <li>
    Mandrake Linux <a href="http://download.kde.org/unstable/koffice-1.2-rc1/Mandrake/SRPMS/">8.x</a>
  </li>
  <li>
    RedHat Linux <a href="http://download.kde.org/unstable/koffice-1.2-rc1/RedHat-Inofficial/SRPMS/">7.3</a>
      (<em>unofficial</em>)
  </li>
  <li>
    SuSE Linux <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/SRPMS/">8.0</a>, <a href="http://download.kde.org/unstable/koffice-1.2-rc1/SuSE/SRPMS/">7.x</a>
  </li>
</ul>
<p align="justify">
  <u>Further Information</u>.
  For further instructions on compiling and installing KOffice, please consult
  the KOffice <a href="http://www.koffice.org/install-source.phtml">installation
  instructions</a>.  For
  problems with SRPMs, please contact the person listed in the applicable
  .spec file.
</p>
<h4>
  Corporate KOffice Sponsors
</h4>
<p align="justify">
  Besides the valuable and excellent efforts by the
  <a href="http://www.koffice.org/developers.phtml">KOffice developers</a>
  themselves, significant support for KOffice development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a> (which sponsors
  KOffice developers <a href="http://people.mandrakesoft.com/~david/">David
  Faure</a> and Laurent Montel),
  <a href="http://www.thekompany.com/">theKompany.com</a> (which has
  contributed Kivio and Kugar to KOffice), and
  <a href="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens
  Datakonsult AB</a> (which has contributed KChart to KOffice).  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE League</a>,
  as well as <a href="http://www.kde.org/donations.html">individual
  sponsors</a> (<a href="http://www.kde.org/support.html">donate</a>),
  provide significant support for KDE and KOffice.  Thanks!
</p>
<h4>
  About KOffice/KDE
</h4>
<p align="justify">
  KOffice is part of the KDE Project.
  KDE is an independent project of hundreds of developers, translators,
  artists and professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>
<p align="justify">
  Please visit the KDE family of web sites for the
  <a href="http://www.kde.org/faq.html">KDE FAQ</a>,
  <a href="http://www.kde.org/screenshots/kde3shots.html">screenshots</a>,
  <a href="http://www.koffice.org/">KOffice information</a> and
  <a href="http://developer.kde.org/documentation/kde3arch/">developer
  information</a>.
  Much more <a href="http://www.kde.org/whatiskde/">information</a>
  about KDE is available from KDE's
  <a href="http://www.kde.org/family.html">family of web sites</a>.
</p>
<hr>
<p align="justify">
  <font size="2">
  <em>Release Coordinator</em>:  Thanks to
  <a href="http://people.mandrakesoft.com/~david/">David Faure</a> for
  his excellent services as release coordinator.
  </font>
</p>
<p align="justify">
  <font size="2">
  <em>Press Release</em>:
  Written by <a href="mailto:pour AT kdeleague DOT org">Andreas
  Pour</a> from the <a href="http://www.kdeleague.org/">KDE League</a>, with the
  invaluable assistance of numerous gifted volunteers from the KDE Project.
  <!--[Translated into _____________ by ______________ .]-->
  </font>
</p>
<p align="justify">
  <font size="2">
  <em>Trademarks Notices.</em>

  KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
  Incorporated.

  Adobe Illustrator and PostScript are registered trademarks of Adobe Systems

  dBASE is a registered trademark of dBASE Inc.

  Corel, Quattro Pro and WordPerfect are registered trademarks of Corel
  Corporation or Corel Corporation Limited.

  Linux is a registered trademark of Linus Torvalds.

  Trolltech and Qt are trademarks of Trolltech AS.

  UNIX is a registered trademark of The Open Group.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>
<hr>
<table border="0" cellpadding="8" cellspacing="0">
  <tr>
    <th colspan="2" align="left">
      Press Contacts:
    </th>
  </tr>
  <tr valign="top">
    <td align="right" nowrap>
      United&nbsp;States:
    </td>
    <td nowrap>
      Andreas Pour<br />
      KDE League, Inc.<br />
      pour AT kdeleague DOT org<br />
      (1) 917 312 3122
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap>
      Europe (French and English):
    </td>
    <td nowrap>
      David Faure<br />
      faure@kde.org<br />
      (33) 4 3250 1445
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap>
      Europe (German and English):
    </td>
    <td nowrap>
      Ralf Nolden<br />
      nolden@kde.org<br />
      (49) 2421 502758
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap>
      Europe (English):
    </td>
    <td nowrap>
      Jono Bacon<br />
      jono@kde.org
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap>
      Southeast Asia (English and Indonesian):
    </td>
    <td nowrap>
      Ariya Hidayat<br />
      ariya@kde.org<br />
      (62) 815 8703177
    </td>
  </tr>
</table>
</td></tr></table>

