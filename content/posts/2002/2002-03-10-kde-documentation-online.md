---
title: "KDE Documentation Online"
date:    2002-03-10
authors:
  - "Dre"
slug:    kde-documentation-online
comments:
  - subject: "Sweet!"
    date: 2002-03-10
    body: "Now, when meinproc fails to work when compiling and installing, I can still get my docs! :)\n\nThanks Daniel...\n\nTroy Unrau\ntroy(at)kde.org"
    author: "Troy Unrau"
  - subject: "Re: Sweet!"
    date: 2002-03-10
    body: "Actually this service does not use meinproc either, but xsltproc. meinproc is only a wrapper around libxslt, meinproc's main task is to care about the chunking, i.e. the splitting up of the HTML into several files. The documentation related software may seem complicated at a first glance, but in fact KDE is using standard tools that are easy to work with."
    author: "Daniel Naber"
  - subject: "koffice docs ?"
    date: 2002-03-10
    body: "How about KOffice docs ? "
    author: "ac"
  - subject: "Re: koffice docs ?"
    date: 2002-03-10
    body: "See http://docs.kde.org/index_head.html"
    author: "Daniel Naber"
  - subject: "Images broken"
    date: 2002-03-10
    body: "Image conversion doesn't work, see http://docs.kde.org/2.2.2/kdebase/kicker/using-kicker.html"
    author: "Anonymous"
  - subject: "Re: Images broken"
    date: 2002-03-10
    body: "Thanks for telling me, it will be fixed with the next update (in some hours)."
    author: "Daniel Naber"
  - subject: "Forum @ docs"
    date: 2002-03-10
    body: "It would be nice to have a forum like this one below all pages in the documentation, so suggestions can be appended quickly by passers-by.\n\nThe respective doc maintainers should of course be able to remove redundant information from such a forum.\n\n"
    author: "Jos"
  - subject: "Re: Forum @ docs"
    date: 2002-03-10
    body: "Like PHP's \"annotated user manual\"? \nIt would be great indeed! And I think it would really help docs writers focusing their work. \n\nAnyway, many thanks to Daniel, it's a great job already!!"
    author: "Andrea Cascio"
  - subject: "Re: Forum @ docs"
    date: 2002-03-11
    body: ":)\n\nAlready on it.  I've been looking for something to contribute to KDE - I emailed Daniel as soon as I saw docs.kde.org, and I've already started working on it - it will be inspired by, but nicer than PHP's annotation system.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Forum @ docs"
    date: 2002-03-11
    body: "\nDocs Annotation TODO:\n\n o Prevent duplicate posts.\n\n ;)\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "kcontrol, wallpaper"
    date: 2002-03-11
    body: "setting wall paper in kcontrol does not work. Open dialog does not show preview for image file \nThis kde-2.2.2 compile from source\n\nSorry this is a bit off topic"
    author: "Anonymous"
  - subject: "Re: kcontrol, wallpaper"
    date: 2002-03-11
    body: "Yes, it is off topic, and you also didn't provide nearly enough information.\nTry OpenProjects #kde-users"
    author: "Carbon"
  - subject: "Downloadable?"
    date: 2002-03-12
    body: "Thanks for this GREAT documentation site. I do have one question though: how can I download the documents instead of reading them online?\n\nRegards,\nJohn Herdy."
    author: "John Herdy"
  - subject: "Re: Downloadable?"
    date: 2002-03-15
    body: "Hi,\nthere are two ways:\nwget -r docs.kde.org\nor point to\nhttp://www.linusetviviane.ch/docs.kde.org.tgz\nWhere the second is the first gzipped...\n\nGreets\n\nIneiti"
    author: "Ineiti"
  - subject: "Re: Downloadable?"
    date: 2004-07-26
    body: ">Hi,\n>there are two ways:\n>wget -r docs.kde.org\n>or point to\n>http://www.linusetviviane.ch/docs.kde.org.tgz\n>Where the second is the first gzipped...\n\nwget ?? What's that? Anyhow, the website didn't work. Any other ideas?\nNo I don't use Linux. My rinky dink system can't handle the conversion from Windoze 98 (let alone the conversion from 98 to 2K, XP, ME, etc) but I do want to read the documentation ahead of time to get a feel for how to use K. TIA."
    author: "Sean"
  - subject: "This is great"
    date: 2002-03-12
    body: "By browsing the documentation site I found out about a couple of apps that I dint knew...\nso you could say that docs.kde.org can be used as a advertising...\n\nRegards"
    author: "Mario"
  - subject: "KDevelop"
    date: 2002-03-12
    body: "The KDevelop manual is missing both in 2.2.2 and HEAD."
    author: "Anonymous"
  - subject: "Re: KDevelop"
    date: 2002-03-12
    body: "The KDevelop documentation is not yet written in DocBook, so it's not part\nof the automatic build process on docs.kde.org (and it won't be until it's Docbook)."
    author: "Daniel Naber"
  - subject: "documentation in pdf or postscript"
    date: 2002-03-16
    body: "Hi,\nIt would be very nice, when you provide also a download in pdf- or postscript-format, if this is possible.\nthank you,\nbye \n Andreas\n\n\n"
    author: "Andreas"
---
<a href="mailto:daniel.naber@t-online.de">Daniel Naber</a> has been busy adding to the KDE.org family.  <a href="http://docs.kde.org/">Docs.kde.org</a> is a new KDE documentation site which features KDE user documentation for both the 2.2.2 and HEAD (updated daily) branches.  The documentation is both browseable and searchable, and developers of KDE CVS apps can link directly to the appropriate documentation from their application's homepage, saving some work (url format: <font size="1"><code>http://docs.kde.org/&lt;VERSION&gt;/&lt;PACKAGE&gt;/&lt;APPNAME&gt;/</code></font>, <em>e.g.</em>, <font size="1"><code>
http://docs.kde.org/HEAD/kdenetwork/kmail/</code></font>).  While only available in English at this juncture, the goal is to eventually add all the translated documentation as well.  A very useful new addition to the KDE.org family, thanks, Daniel!
<!--break-->
