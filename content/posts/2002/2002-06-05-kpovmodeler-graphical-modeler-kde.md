---
title: "KPovModeler: A Graphical Modeler for KDE"
date:    2002-06-05
authors:
  - "azehender"
slug:    kpovmodeler-graphical-modeler-kde
comments:
  - subject: "Konqui SDK"
    date: 2002-06-05
    body: "It'd be nice if someone with the required ability could \"port\" (that is, redraw) the Konqui SDK to this package.  Then we all could use that guy in more graphics than Garfield or Snoopy. :-)\n<p>\n<a href=\"http://artist.kde.org/\">http://artist.kde.org/</a>"
    author: "Neil Stevens"
  - subject: "Re: Konqui SDK"
    date: 2002-06-05
    body: "Somehow I'm sure that port would involve quite a bit more than just drawing! :)\n"
    author: "Anonymous"
  - subject: "The big shops"
    date: 2002-06-05
    body: "The big shops still use OpenInventor and in conjunction with scripting languages (on SGIs and Suns, I might add). GUIs are great and all, but perhaps they should be geared towards extending the lead of complex packages instead of trying to make less complex apps even simpler. If graphics guys want Linux to go in this direction, it will help everyone in the graphics business. But if Linux keeps going the route it is, it might remain just an alternative to the ever-popular Windows - just a hack-tool for the masses, although only a small percentage of those masses. Isn't it time to change this?\n\n"
    author: "Anonymous SF"
  - subject: "The commons"
    date: 2002-06-05
    body: "I agree with you in the sense that it would be a pity if, after all the efforts done by the open-source movements, GNU/Linux systems remains just as a free alternative to Microsoft's Trust Softwares.\n\nBut, never forget that this little difference is, for at least a half \nof them, the reason that makes people use Linux.\n\nThe sole and unique question to think of is : \"Will the open-source community wait for proprietary high quality multimedia softwares to be ported to Linux before hacking better ones ?\"\n\nBy this I mean that it is not good for GNU/Linux not to develop some kinds of professional multimedia studios. Just think of an open-source software which allows you to product digital videos from scratch.\n\nAn \"open-source-minded\" guy."
    author: "Chucky"
  - subject: "good apps"
    date: 2002-06-09
    body: "KDE keep bringing in the best apps.\n\n:-)"
    author: "Ax"
  - subject: "Landscapes?"
    date: 2002-06-05
    body: "I'd like to see a way to create and manipulate landscapes, that is, rocks, skies, water, etc. easily. If you know Bryce 3d, you know what I mean."
    author: "Carsten Pfeiffer"
  - subject: "Re: Landscapes?"
    date: 2002-06-06
    body: "Hiya\n\nI'm working on a program to create and render landscapes (called 'landscape' ;-), although it's nowhere near as fancy as Bryce/Terragen etc.\n\nThere's currently no way to export created terrains, but I've got vague plans to add such functionality later.\n\n--Jon\n\nhttp://www.witchspace.com"
    author: "Jonathan Belson"
  - subject: "to bad"
    date: 2002-06-05
    body: "Povray suck's!"
    author: "Andreas"
  - subject: "Re: to bad"
    date: 2002-06-05
    body: "Maybe (I haven't used non Windows 3-D renderers, so I can't tell).\n\nBut the KPovRay program looks great already as far as modelling \ngoes, and it only its first public appereance!\n\nIt could also potentially use another renderer, if POV is all that\nbad (which remains to be proven, you just made an unsupported\nclaim).\n\nSo, generally, this stuff is GOOD NEWS.\n\n\n\n"
    author: "Bug Powder"
  - subject: "Re: to bad"
    date: 2002-06-05
    body: "povray is avaible for windows"
    author: "AAA"
  - subject: "Re: to bad"
    date: 2002-06-14
    body: "windows sux"
    author: "asdf"
  - subject: "Re: to bad"
    date: 2002-06-05
    body: "Not sure why you feel that Povray sucks. It seems to be a very good ray tracer and its radiosity engine has improved greatly in recent months (with the the version 3.5 release candidates). You might argue that the scripting language native to Povray is not as intuitive as, say, a 3D modeller, but if you look at Renderman, the standard used by Pixar for their movies and shorts, it is also based on a scripting language.\n\nIf you would like to see the quality that Povray can achieve, I suggest that you look at the following images posted to the Povray news groups (news.povray.org):\n\nhttp://news.povray.org/povray.binaries.images/24690/173491/distorted.jpg\nhttp://news.povray.org/povray.binaries.images/24424/171544/2cv_09_sm.jpg\nhttp://news.povray.org/povray.binaries.images/21845/148519/bolts.jpg"
    author: "falrick"
  - subject: "Re: to bad"
    date: 2002-06-11
    body: "I was especially amazed by those pictures:\n<p>\n<a href=\"http://www.irtc.org/ftp/pub/stills/2001-06-30/warm_up.jpg\">warm_up.jpg</a><br>\n<a href =\"http://www.irtc.org/ftp/pub/stills/2000-04-30/gt_city.jpg\">gt_city.jpg</a><br>\n<p>\nThe problem about all that is that it's hard to animate stuff. Most renderers are based on polygonal models I guess. While in theory, a procedural model is superior because of better scalability, it's hardly possible to animate this. You can use polygons for povray, but you'll lose a lot of \"cool\" stuff. However, for still images, povray is a very nice renderer."
    author: "Rolf Magnus"
  - subject: "Re: to bad"
    date: 2002-06-11
    body: "Yes, animating something in povray can be quite difficult, but an animation is just a sequence of still images. The idea behind this modeller is precisely to hide the complexities of a procedural renderer from the end user.\n\nIf you create the scene using object declarations and links, you can create an animation of sorts.\n1. Create the scenery. This is a fixed collection of objects that won't move during the animation. \n2. Then create the animatable objects. You can then place them (using links) on the scene.\n3. Render the scene.\n4. Change the name of the generated png to include the frame number.\n5. Change the translation/rotation of the objects a bit.\n6. While you don't have all the pictures, go to 3\n\nYou just have to use an application to join all pictures in a movie and it's done. No application name occurs to me but there are several out there.\n\nAnyway, animation is one of the features we intend to add to kpovmodeler later on. We have no clear view on how this will be implemented and we're always open to suggestions on how to do it."
    author: "Luis Carvalho"
  - subject: "Innovation 3D"
    date: 2002-06-05
    body: "Anyone interested in KDE 3D modelling could do worse than\nlooking at Innovation 3D. It's the most impressive open source\n3D modelling package I have seen so far:\n\nhttp://innovation3d.sourceforge.net\n"
    author: "Will"
  - subject: "Re: Innovation 3D"
    date: 2002-06-10
    body: "Sorry to burst your bubble but innovation3D sucks.  You can't have it open for too long or it will crash, and you can't open anything that you save.  So, hooray for i3D."
    author: "Anonymous"
  - subject: "POV-Ray speed"
    date: 2002-06-05
    body: "I remember POV Ray from when I was back on Windows. I quite liked it, but compared to commercial raytracers it had a BIG problem with speed. Is this still the case? I last tried it many years ago, but it could take forever to render a moderately big scene, whereas for instance LightWave would do it in only a few minutes. By the way, KPovModeler looks really nice, when I've got kde3 i'll definately try it again, and I might be able to do some doc writing for you. No promises however.\n\nthanks -mike"
    author: "Mike Hearn"
  - subject: "Re: POV-Ray speed"
    date: 2002-06-05
    body: "I never used other raytracers than povray, but I agree that povray could be faster. A great raytracer though.\n\nWould be great if you join our team and help with the user documentation."
    author: "Andreas Zehender"
  - subject: " KDE Graphics package?"
    date: 2002-06-05
    body: "So this is going to be part of the KDE Graphics package.  So when I install kde3.1 it will be included?"
    author: "theorz"
  - subject: "Re:  KDE Graphics package?"
    date: 2002-06-05
    body: "Yes. KPovModeler will be included in KDE 3.1."
    author: "Andreas Zehender"
  - subject: "Interesting but..."
    date: 2002-06-05
    body: "There are already a lot of 3d modellers available for KDE and GNOME and they are all the same.  None of the ones I've seen have support for animation.  What I'd like to see is something capable of modelling motion, such as water droplets, a bouncing ball, creatures that walk, etc.  I'd also like to see a modelling program that reads stereoscopic images/video and develops a model in 3D space.  Maybe these ideas might spur some innovation. :)"
    author: "A coward"
  - subject: "Where are my toolbars?"
    date: 2002-06-10
    body: "This program looks excellent, and was looking forward to trying it out. Un fortunatly when I started it up the only tool bar I had was the one with open, save, and print on it. The screenshots show loads of others so where are they? Couldn't edit or render the example scenes as a result. Maybe it's just me being stupid or lack of documentation but any ideas?"
    author: "David Williams"
  - subject: "Where are my toolbars?"
    date: 2002-06-10
    body: "This program looks excellent, and was looking forward to trying it out. Un fortunatly when I started it up the only tool bar I had was the one with open, save, and print on it. The screenshots show loads of others so where are they? Couldn't edit or render the example scenes as a result. Maybe it's just me being stupid or lack of documentation but any ideas?"
    author: "David Williams"
  - subject: "Re: Where are my toolbars?"
    date: 2002-06-12
    body: "You installed KPovModeler into the wrong directory.\n\nEither set your KDEDIR environment variable correctly or specify the kde directory with ./configure --prefix=yourkdedir\n\nSee http://www.kpovmodeler.org/install.html"
    author: "Andreas Zehender"
  - subject: "Compile?"
    date: 2002-06-10
    body: "Hey!\n\nJust a quick one here.  I can't get this to compile.  I'm running SuSE 8.0 with KDE3.0.1.  Configure just says:\n\nYou are missing at least one of opengl (mesa), glu, glut and glx.\nkpovmodeler will not be compiled.\nYou can download them from\nhttp://www.mesa.org\n\n\nBut:\n\net:/usr/src/kpovmodeler-0.2 # rpm -qa | grep mesa\nmesa-4.0.1-74\nmesaglu-4.0.1-74\nmesaglut-4.0.1-74\nmesasoft-4.0.1-74\nmesa-devel-4.0.1-74\nmesaglut-devel-4.0.1-74\n\nI have the -devel packages installed.  Anyone have a suggestion of what I can look for?\n\nThanx!"
    author: "Xanadu"
  - subject: "Re: Compile?"
    date: 2002-06-12
    body: "You can see in your configure output which header file it can't find.\n\nMaybe the glx header is missing?"
    author: "Andreas Zehender"
  - subject: "A standardformat for all 3d-moddeling programs"
    date: 2002-06-12
    body: "How about a standardformat for creating 3d-models and other graphics?\nIt seems that there are already many programs for this, and both have their strong and weak points. If all these programs used the same open-standardformat it would be very easy to work with. We could also perfectly bennefit from all the stong points of the different programs.\n\nConclusion: its not a matter about wich program we use, but wich format the programs use. "
    author: "Maarten Romerts"
  - subject: "Re: A standardformat for all 3d-moddeling programs"
    date: 2002-06-13
    body: "Good point!\n\nIt would be more than \"cool\" to have KPovModeler \"aware\" of most (all) other (OSS) renders out there.\n\nBMRT, Entropy both by Exluna (Larry Gritz)\nRenderMan (RIB) by Pixar\nMental Ray by mental images GmbH & Co. KG.\netc. pp.\n\nThank you for the great work!\n\n-Dieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: A standardformat for all 3d-moddeling programs"
    date: 2002-06-13
    body: "I'd have to second BMRT and of course that should automatically include Renderman.  I would like to see some comparisons between the speed and qualities of the various renderers especially the free ones."
    author: "Strog"
  - subject: "Re: A standardformat for all 3d-moddeling programs"
    date: 2002-08-13
    body: "mental ray is OSS? I got the impression it cost a whole lotta. "
    author: "Alex Gittens"
  - subject: "Re: A standardformat for all 3d-moddeling programs"
    date: 2002-08-13
    body: "Do you could read?\n\n...\"aware\" of most (all) other (OSS) renders...\"\n\nOr would you trolling, only?\n\n-Dieter\n\n"
    author: "Dieter N\u00fctzel"
  - subject: "Re: A standardformat for all 3d-moddeling programs"
    date: 2002-08-14
    body: "Yes, apparently too well. You see when someone writes (says) something like \"most (all) other (OSS) renderers...\", I tend to think they are qualifying the 'other renders' to mean 'other OSS renderers'. Sorry. \n\nBut that was a genuine question. Not trolling."
    author: "Alex Gittens"
---
After a good year of public existence, <a href="http://www.kpovmodeler.org/">KPovModeler</a> 0.2 is released.
KPovModeler is a full-featured graphical modeler and composer for creating <a href="http://www.povray.org/">POV-Ray</a>(TM) scenes under KDE 3. It now supports almost the full gamut of POV-Ray 3.1 functionality -- see these nice <a href="http://www.kpovmodeler.org/screenshots.html">screenshots</a>. KPovModeler is a new member of the KDE Graphics package, but we are still looking for a documentation writer to join the project.
<!--break-->
<p>Current features are:</p>
<ul>
  <li>Hierarchical object tree</li>
  <li>Non-blocking scene rendering with OpenGL</li>
  <li>Object modification with control points in a graphical view
    or direct manipulation of object attributes in a dialog</li>
  <li>Free configurable view layout with dock widgets</li>
  <li>Prototypes (declarations) and references</li>
  <li>Copy/paste and drag/drop of (a subset of) povray(!) code into
    and out of the object tree</li>
  <li>Undo and redo</li>
  <li>Scene rendering and texture preview with povray inside the program</li>
  <li>Support for almost all povray objects</li>
  <li>Support for all textures</li>
  <li>All projection modes of the camera</li>
</ul>

<p>What is still missing is user documentation. We lack the
 time to write it ourselves, so we would like someone to take
 over this task. If you are interested send
 <a href="mailto:zehender@kde.org">me</a> a mail. Basic POV-Ray knowledge would be an asset.</p>

<p>To see KPovModeler in action see <a href="http://www.kpovmodeler.org/screenshots.html">our nice screenshots</a>. 
The new version can be <a href="http://www.kpovmodeler.org/download.html">downloaded here</a>.</p>
