---
title: "Florida School Deploys KDE/GNU/Linux On Thin Clients"
date:    2002-03-04
authors:
  - "Dre"
slug:    florida-school-deploys-kdegnulinux-thin-clients
comments:
  - subject: "Wow"
    date: 2002-03-04
    body: "Well, this is awesome. It demonstrates how useful X can be in an environment like this, where\nyou can't afford individual top-of-the-line workstations, but still want good performance. And KDE\nin use is great too.\n\nI'd just like to know, what office software are they using, exactly? OO or SO, or maybe MS Office via\nthat windows emulation software? It didn't seem too clear on that."
    author: "Carbon"
  - subject: "Re: Wow"
    date: 2002-03-05
    body: "They are probably using KOffice from KDE and/or MS Office through rdesktop. I did not see any mention of Win32 \"emulation\" software being used; there is just rdesktop, a terminal client (kinda like an X server or VNC client) which can display remote applications from WinNT."
    author: "Triskelios"
  - subject: "Like a free ride when you've already paid"
    date: 2002-03-04
    body: "Why is using RedHat ironic?\n"
    author: "not me"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-04
    body: "Using Mandrake for everydays use seems better to me. (and it makes David Faure happy :-) They use Mandrake but only on stand-alone workstations. I hope they know what they're doing :-)\n\nLet servers run RedHat or other distros that are server-centric..."
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-04
    body: "Because RedHat was not very happy about KDE in the beginning (to put it mildly). If I remember correctly the RedHat CEO said something like \"there will never be a KDE in RedHat...\". Then Mandrake was released which was (in the beginning) RedHat + KDE and a couple of months later RedHat was \"forced\" to include KDE too ;-)\n\nNowadays things have cooled down and RedHat even sponsors a developer (Bernhard \"bero\" Rosenkraenzer)."
    author: "Marco Krohn"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-05
    body: "Bero isn't primarily sponsored for his KDE work, though, AFAIK. But there is a major difference between the visions of Red Hat US and Red Hat Europe. KDE has always been more popular in homebase Europe and this is also true for RH."
    author: "Rob Kaper"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-05
    body: "Redhat's \"problem\" with KDE was the QT license, the same license that kept KDE out of Debian until recently (how long has it been anyway?). Debian, being more vehement about that kind of thing, took longer.\n\nAnd with Mandrake's current releases being a buggy nightmare (I'm hoping 8.2 will be up to the quality of their earlier releases) - I've been recommending Redhat to newbies. \n\nEither way, I've been enjoying the Debian KDE packages immensely, they rock.\n\n\n\n"
    author: "jorge"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-05
    body: "Yep, I too love the support of KDE in Debian. This was primarily due to one underappreciated man, Ivan Moore. Last time I checked, he wasn't maintaining the packages anymore (other people are), but he deserves a lot of respect for making very good packages and turning the once hostile Debian project very KDE friendly now."
    author: "fault"
  - subject: "Urban Myth"
    date: 2002-03-05
    body: "Well Redhat said, they thought distributing KDE was illegal. They then skipped KDE in a few distributions and sponsored GNOME as a replacement for their commercial sucky \"Looking Glass\" Desktop. They basically fell for Miguel de Icazas propaganda and also wanted to have total control over the desktop environment development, which they would never have with KDE.\nThere might also have been anti-European prejudice and NIH-syndrome involved. I digress.\n\nBasically, they proved that the license issue was not the reason for their GNOME support was not the KDE license by including KDE back into Redhat (because of Mandrake) before the license isssue had been resolved.\n\nUntil this day the Redhat KDE is always a neglected, badly compiled and buggy part of their distribution and it is also the reason why in Europe nobody is seriously considering Redhat for desktop use. SuSE and Mandrake rule here."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: Urban Myth"
    date: 2002-03-05
    body: "1 troll point for you"
    author: "lol"
  - subject: "Re: Urban Myth"
    date: 2002-03-05
    body: "Badly compiled and buggy?  Then why are all these companies using KDE with Red Hat?"
    author: "ac"
  - subject: "Re: Urban Myth"
    date: 2002-03-05
    body: "Because they are based in the US and Redhat has a big presence in the US?"
    author: "ac"
  - subject: "yes, BADLY compiled and BUGGY. Here's why."
    date: 2002-03-11
    body: "Yes, badly compiled and buggy indeed. Horribly buggy. Want to see proof? Go here:\n\nhttps://bugzilla.redhat.com/bugzilla/show_bug.cgi?id=54220\n\nThis is a bug renders most european keyboards UNUSABLE. If you use RedHat with KDE, you'd better not try to use deadkeys or writing in Spanish, Italian, French, German, Hungarian, Portuguese, and the like. This bug have been present in pre-7.2 RawHide, then got into 7.2, and got through an errata that updated KDE to 2.2.2, to no avail. IT IS STILL OPEN. Red Hat is denying us something as simple and essential as the ability to write our own language.\n\nThat is why Moritz' criticism of RedHat releases of KDE as buggy and badly compiled are right on target. It is just too obvious that KDE support in Red Hat is just laughable.\n\nBTW, I am part of the KDE 3.0 Spanish translation effort. Needless to say, this bug has made my KBabel experience far more \"pleasant\".\n\nPLEASE RED HAT, GET A CLUE!"
    author: "Eduardo"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-05
    body: "KDE will be officially supported in the next version of RedHat. Bero has said that in a post on Slashdot."
    author: "Joergen Ramskov"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-05
    body: "It's my fault this is not clear in the article, since Dre actually gave the reason why he said that, but last minute I felt it was a bit too verbose.\n\nThe reason Dre used the word ironic there is because Red Hat more or less focuses on the Server market and insists on ignoring the desktop.\n\nApologies to Dre and everyone confused by this.\n"
    author: "Navindra Umanee"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-05
    body: "They don\u00b4t ignore the desktop. They spend a lot of resources on it (there\u00b4re the packagers, the manuals, and all the traning for their support staff).\n\nThe desktop is not RH\u00b4s focus, but they certainly don\u00b4t ignore it.\n"
    author: "Carg"
  - subject: "Re: Like a free ride when you've already paid"
    date: 2002-03-11
    body: "You're forgiven... just don't let it happen again.  ;-)"
    author: "concord"
  - subject: "Ah, this is good."
    date: 2002-03-05
    body: "I love to see it when people take advantage of \"KGL\" thin clients. :-) What a wonderful, elegant, efficient, solution!  Now if only I could set up a few thin clients and one of those SMP 1 GHz servers in my home... Gee, I want to check my e-mail in the basement... no the kitchen... no...\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Debian Jr."
    date: 2002-03-05
    body: ">The Debian Jr. Project is a Debian project to make Debian an OS that \n>children of all ages will want and be able to use.\n\nWell, I wish them luck. Never having successfully installed Debian myself (0 for 3 tries), I'm hoping Debian Jr. (aimed at children 8 and under, according to their site) might be within my capabilities."
    author: "Otter"
  - subject: "Re: Debian Jr."
    date: 2002-03-05
    body: "Debian Jr. is not \"a more simply debian\", installable and configurable by 10-Year-olds, but rather a bunch of packages that makes Debian and Linux interesting for kids of that age!\nBTW: When did you last try to install? When I first tried, i was scared first, then disappointed that there's no KDE;-) Now there is, and installing Debian is not harder, but just different!\nI love Debian, and KDE on Debian...\nMfG\nFranz Keferb\u00f6ck"
    author: "Keferb\u00f6ck Franz"
  - subject: "KDE making some big steps"
    date: 2002-03-05
    body: "I say, KDE/Linux is having more and more notable hits like this.  Glad it works out, hope it keeps working out. \n\nIn my opinion though, this is *huge*.  Anyone bet we have some people worried out there?  Microsoft has been known to go all out crazy in such situations and has been known to give *free* Windows licenses left and right, a touch of political pressure, to get Linux deployments reversed and to get people hooked on their software again.  So much for my statement about \"not caring\" about Microsoft.  We've been pretty safe in our niche so far.  ;-)\n\nI imagine we must be catching the eye of Sun as well?  Seems Sun has been making itself irrelevant, but then again I don't think Sun is aggressive about this type of market anymore.\n\nLater,\n-N.\n\n\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "The more Microsoft starts going out of its way to suppress this kind of thing, the more it validates KDE as a true competitor to Windows.  If we can show that Microsoft is actively going after us, maybe other people and businesses will take KDE more seriously."
    author: "not me"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "Once you define yourself by who your enemies are, your enemy has beaten you."
    author: "Neil Stevens"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "Know your enemy and know yourself; in a hundred battles, you will never be defeated. When you are ignorant of the enemy but know yourself, your chances of winning or losing are equal. If ignorant both of your enemy and of yourself, you are sure to be defeated in every battle. \n\n  - Art of War, Chapter 3: Offensive Strategy"
    author: "Sun Tzu"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "You stole my car! Bad bunny!\nNow you've driven it over that cliff! Bad, _bad_ bunny!!\n\n- Torg (Sluggy Freelance)\n\nSorry, just couldn't resist doing that when people started quoting :-)"
    author: "Carbon"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "OK, that's nice, but is that all KDE is?  An organization fighting against another?  Or is it a community sharing and producing software?\n\nIf KDE is to be thought of in military terms, then that's a real shame."
    author: "Neil Stevens"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "Generally, software is not made to be disused.  If KDE is gonna be used more it will be at the cost of other desktops.  It would be great if KDE could unseat Microsoft and be widely used."
    author: "Hey"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: ":: If KDE is to be thought of in military terms, then that's a real shame.\n\n    The Art of War is actually quite applicable to many many facets of life.  But I'd like to submit an alternate view:  MS is not the enemy, nor is Gnome.  The tasks we do are.  \n\n    Ignore the progress of the alternatives like Windows, Mac and Gnome, except for the simple acknowledging them as a source of ideas.  Just code KDE to accomplish the tasks you and those you know need to accomplish.  Make it easy as possible, and when it's stable, make it snappy (fast) and look nice.\n\n   ...of course, that's pretty much what's been going on to date.  I think the results of that speak for themselves.  We just saw the look nice phase hit at the end of the 2.x series, and for those using the 3.x prerelease, I think we're seeing the speed improvements - all on top of the stability we've come to expect.\n\n    I, for one, don't care if Windows (or Mac) has 99.9% of the userbase - I have KDE, which suits my needs.  I'd like KWord to be a bit more advanced, but that's about it (and for those who are wondering, I've used KWord in a realworld, twice weekly publishing with a sharp deadline role.  Granted, it was for two RPG groups, but that means lots of text, graphics and layout work for every game.  It performed well enough so that I'm satisfied it use it for business purposes now.   When the quirks with printing are resolved (in other words, as soon as the current CVS code goes stable), I'll recommend it to others.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "In general I would agree with your statements.\nUnfortunately, MS is trying everything they can to make my job harder if I am not using their software (like switching to different file formats that I can not read; web sites designed with outlook that I can not view, etc.)."
    author: "jj"
  - subject: "Re: KDE making some big steps"
    date: 2002-03-05
    body: "Unless you're directly doing business with Microsoft, it's not Microsoft wh's making your job harder.  It's your employers or customers who make your job harder by choosing to use Microsoft products.\n\nLay the blame where it belongs.  Don't treat all the Microsoft users like children who can't think for themselves."
    author: "Neil Stevens"
  - subject: "Great news from KDE site"
    date: 2002-03-05
    body: "I like the idea that the school use Linux and KDE. Because Linux is a great and economical alternative for all the world.\n\n\nGo ahead with KDE 3.0!"
    author: "Josue Irizarry"
  - subject: "KDE/GNU/Linux ????"
    date: 2002-03-05
    body: "What's this ??? KDE/GNU/Linux ??? KGL, well that sounds great !!!\n\nHas been a long time, exept Debian, someone wrote GNU/Linux.\nMaybe you gave me a reason to come back to KDE ...\n\nLong time i didn't use Konqueror ....\nI should apt-get install kde*"
    author: "Huch ?"
---
<a href="http://www.stmarys-school.org/">St. Mary's Catholic School</a>
in Rockledge, Florida has demonstrated the obvious:
that KDE/GNU/Linux (KGL) is great for kids and schools.  Working with
volunteers from
the <a href="http://www.mlinux.org/">Melbourne Linux Users Group</a>
(that's Melbourne, <em>Florida</em>),
<a href="http://www.idealcorp.com/">I.D.E.A.L. Technology</a>
(<a href="http://www.idealcorp.com/About/PressCenter/000067">press release</a>)
and <a href="http://www.ixctelecom.net/">IXC Telecom</a>,
the school deployed the
<a href="http://www.k12ltsp.org/">K12 Linux Terminal Server Project</a>
(<a href="http://www.k12ltsp.org/press.html">press release</a>),
a thin client enterprise solution.  The school's KGL network
consists of one beefy server (dual 1-GHz Intel<sup>&reg;</sup>), which
services 36 workstations (16 of which are dual-boot).  Apparently the
school has opted for KDE as the desktop, and can use
<a href="http://www.rdesktop.org/">rdesktop</a>, a
<a href="http://www.microsoft.com/WINDOWS2000/server/evaluation/news/bulletins/rdpspec.asp">Remote
Desktop Protocol</a> (RDP) client, to access MS Windows-based applications
from an MS Windows terminal server.  The school's
<a href="http://www.stmarys-school.org/smsk12linux_howandwhy.htm">report</a>
notes that <em>&quot;the cost per workstation was $50.00 using donated
Pentium class computers&quot;</em>.  Time to talk to the local school
board about saving some tax bucks!




<!--break-->
<p />
<h4>K12 Linux Terminal Server Project</h4>
<p>
The K12 Linux Terminal Server Project (K12LTSP), perhaps ironically based on
<a href="http://www.redhat.com/">Red Hat</a> Linux, is <em>&quot;free software with the
potential to save schools, public agencies and businesses millions of
dollars&quot;</em>.  K12TLSP exploits an extremely efficient,
easily-administered system architecture which will ring familiar to those
who have studied the <a href="http://largo.com/">City of Largo</a>'s
<a href="http://dot.kde.org/995949998/">deployment of KDE</a>:
</p>
<blockquote>
<p>
K12TLSP installs terminal software on a server that powers diskless
workstations (thin-clients). Applications run on the server with only
the display, keyboard and mouse running on the workstations. This allows the
continued use of older computers while avoiding costly upgrades.  New
K12LTSP terminals cost less than $200 each.  Even greater savings are
realized as diskless workstations have no hard drives or software to
maintain.  While the cost of operation is lower, users enjoy a faster
and more reliable software environment.
</p>
<p>
A typical installation of a Windows(tm) OS based computer lab of 20
workstations may cost more than $20,000 while the same lab running
K12LTSP would cost less than than $6,000.  Reusing legacy hardware can
reduce that cost to less than $2,000.
</p>
<p>
K12LTSP provides a wide selection of applications most often needed by users.
Web browsers, word processing, spreadsheet, presentation and graphics
applications are included and are free.  Updates in the 2.0 release include
automatic sound configuration on many computers.  This enables the use of
multimedia applications like <a href="http://www.real.com/">Real Audio</a>(tm).
</p>
<p>
Linux has experienced rapid growth as a file and Internet server operating
system. It's expansion to end user desktops is not surprising says
co-developer Paul Nelson. "No one operating system is the right solution
for all tasks but Linux thin-clients are now a superior choice for
providing basic desktop applications reliably at very low cost."
</p>
</blockquote>
<h4>Resources for Open Source in Schools</h4>
<p>
Here are some links and resources for those interested in deploying KDE
in schools:
</p>
<table border="0" cellspacing="0" cellpadding="8">
 <tr valign="top">
   <td align="center"><a href="http://www.k12ltsp.org/"><img src="http://www.kde.com/dot/edu/k12ltsp.png" width="148" height="43" border="0" alt="K12LTSP.org"></a><br clear="all" /></td>
   <td><p />The
   <a href="http://www.k12ltsp.org/">K12 Linux Terminal Server Project</a>,
   part of the K12Linux project, provides information and
   links to download and install Linux Terminal software for classroom
   workstations.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://k12linux.org/"><img src="http://www.kde.com/dot/edu/k12linux.png" width="150" height="50" border="0" alt="K12LINUX.org"></a><br clear="all" /></td>
   <td>The <a href="http://k12linux.org/">K-12 Linux Project</a>, part of the
   K12Linux project, site provides tutorials and guides for using
   Linux as a server, such as network administration tutorials and
   information for new Linux users.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://k12os.org/"><img src="http://www.kde.com/dot/edu/k12os.png" width="150" height="50" border="0" alt="K12OS.org"></a><br clear="all" /></td>
   <td><p /><a href="http://k12os.org/">K12OS.org</a>, part of the K12Linux
   project, provides help with installing and configuring Linux for
   your school.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://www.schoolforge.net/"><img src="http://www.kde.com/dot/edu/schoolforge.png" width="139" height="23" border="0" alt="SCHOOL FORGE"></a><br clear="all" /></td>
   <td><p /><a href="http://www.schoolforge.net/">Schoolforge</a> is a repository
   for educational software, lesson plans and projects.  Its mission is to
   unify independent organizations that advocate, use and develop open
   resources for primary and secondary education.  It seeks to empower member
   organizations to make open educational resources more effective, efficient
   and ubiquitous by enhancing communication, sharing resources, and increasing
   the transparency of development. Schoolforge members advocate the use of
   open source and free software, open texts and lessons and open curricula
   for the advancement of education and the betterment of humankind.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://edu.kde.org/"><img src="http://www.kde.com/dot/edu/kdeedutainment.png" width="159" height="35" border="0" alt="KDE Edutainment"></a><br clear="all" /></td>
   <td><p /><a href=""http://edu.kde.org/">KDE Edutainment</a> is a project
   designed to create educational software based around KDE.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://opensourceschools.org/"><img src="http://www.kde.com/dot/edu/opensourceschools.png" width="165" height="28" border="0" alt="Open Source Schools"></a><br clear="all" /></td>
   <td><p /><a href="http://opensourceschools.org/">Open Source Schools</a>
   is an online journal of Open Source software in schools.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://www.seul.org/edu/"><img src="http://www.kde.com/dot/edu/seuledu.png" width="119" height="55" border="0" alt="SEUL/edu"></a><br clear="all" /></td>
   <td><p /><a href="http://www.seul.org/edu/">SEUL/edu</a>, part of the
   <a href="http://www.seul.org/">SEUL</a> (Simple End User Linux) project,
   is a discussion group which covers all aspects of educational uses of Linux
   by teachers, parents and students.  The site also collects resources
   that should enable the development (with the help of interested volunteers)
   of various open source software that can make Linux more desirable to
   educators and parents for their children's education.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://www.debian.org/devel/debian-jr/"><img src="http://www.kde.com/dot/edu/debianjr.png" width="187" height="48" border="0" alt="Debian Jr. Project"></a><br clear="all" /></td>
   <td><p />The
   <a href="http://www.debian.org/devel/debian-jr/">Debian Jr. Project</a>
   is a <a href="http://www.debian.org/">Debian</a> project to make Debian
   an OS that children of all ages will want and be able to use.  It also
   packages educational software for use with Debian.</td>
 </tr>
 <tr valign="top">
   <td align="center"><a href="http://www.linuxforkids.org/"><img src="http://www.kde.com/dot/edu/linuxforkids.png" width="175" height="38" border="0" alt="Linux for Kids"></a><br clear="all" /></td>
   <td><p /><a href="http://www.linuxforkids.org/">Linux for Kids</a> promotes
   the use of Linux as an educational and entertainment platform for
   children.</td>
 </tr>
</table>



