---
title: "Kernel Cousin KDE #30 Is Out"
date:    2002-01-18
authors:
  - "rkaper"
slug:    kernel-cousin-kde-30-out
comments:
  - subject: "Faster startup with GCC3?"
    date: 2002-01-18
    body: "Hi all,\n\nIs it true that GCC3 has a kind of objprelink capability, so the startup time of the applications are going to be more faster than the actual ones?\n\nThank you for any info.\nJuan Ignacio."
    author: "Juan Ignacio"
  - subject: "Re: Faster startup with GCC3?"
    date: 2002-01-19
    body: "It's false. But recent versions of ld have a -z combreloc option (which is enabled by default) which helps to improve startup performance."
    author: "anonymous"
  - subject: "Focus on speed now"
    date: 2002-01-18
    body: "Hi,\n\nWouldn't it really be nice if <b>all</b> the KDE hackers, before rushing out\nKDE3, would spend and additional two weeks (or even a month) just addressing performance issues.\n\nWe know that khtml got some tuneups recently, but khtml is one thing\n(although it is a very important one!), but there are many others.\n\nThe biggest thing might still be the poor startup performance.\n\nFollowing the kde-cvs and kde-devel Mailing lists, recently there were some\nthreads where some hackers meant that they could enhance KDE's speed by at\nleast 30%-50% by not using certain C++ features that have huge overhead.\n(Virtual functions IIRC)\n\nSee this poll where \"Speed\" is the biggest wish of currently 50% of all\nvotes:\n\nhttp://kde-look.org/poll/poll.php?poll=5&PHPSESSID=43300db09b4ea01983705312b0a47fa1\n\nI love KDE, it is what made me quit using windows\n\nAnonymous Coward"
    author: "Anonymous Coward"
  - subject: "Re: Focus on speed now"
    date: 2002-01-18
    body: "'ve no experience of KDE code, but I know C++, and virtual functions are a pretty core part of what makes C++ such a powerful language. It is possible you could conceive an alternative using templates, but that would bloat the size of libraries and apps trememdously. Then you'd have performance problems 'cause it would take up too much memory! It's basically not something you can simply scrap for speed.\n\nIn response to the first comment on GCC3 speed ups, it's got better support for C++ generally, and does a better job with the linker, but from what I've heard we won't see any real improvement till GCC 3.1."
    author: "Bryan Feeney"
  - subject: "Re: Focus on speed now"
    date: 2002-01-18
    body: "Here are the details (read the whole thread) :\n\nhttp://lists.kde.org/?l=kde-cvs&m=101083409300315&w=2\n\n(I've only recently started C++, sorry for being not precise)"
    author: "Anonymous Coward"
  - subject: "Re: Focus on speed now"
    date: 2002-01-18
    body: "Ok, hi. I'm a windows user. Yeah, bring on the flames. I also use KDE (unsurprising, really, I didn't end up here by random IE clicks). Let me address the point above. I'm a C++ coder, nothing under linux it's all 'doze based, but the principles are the same.\n\nVirtual functions are gonna give you code overhead. They do reduce the speed of your code, because you're trying to maintain a vtable of functions (strictly \"class methods\") that have been derived from a different source. It is incredibly easy not to use virtual functions. Want to know how easy? Its the difference between this:\n\nvirtual void woot(int a);\n\nand\n\nvoid woot(int a);\n\nNot much difference eh. I have no freakin idea why this guy is recommending you use template classes instead of virtual functions - hello? they are 2 different things. Go hit yourself with a copy of C++ for Dummies. For those of you who find that book too confusing, it's as simple as this - put a virtual function in a base class, and you are forced to include it in all the classes you derive it from.\n\nAnyways, aside from that. KDE is nice, but it's way too slow. Put comparitively, installing RedHat or Debian with KDE2, and Win2k with MS Office for eg on the same box (p2-400), and Windows blows KDE away for speed. KDE is so slow it becomes unusable. I dont want to have to wait 30 seconds just for my freakin file manager to appear. I'd rather open a shell and do it manually than wait - but then whats the point of KDE.\n\nSure, I thought linux was meant to be faster/more stable than windows. Not in my world."
    author: "dB"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "Okay, now you're just flipping. 30 seconds to start Konqueror on a P2-400?  Please. My konqueror appears almost instantaneous on my computer which is much less beefy than a PII.  Its only a Celeron, and KDE is just heaven on it.\n\nDon't blame KDE for something *you* are doing wrong please.  \n\nQuite obviously you don't understand the importance of virtual functions either, do you.  Can you say super-casting? Maybe reading books meant for dummies isn't such a good thing, huh?"
    author: "ac"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "MFC... is that really C++?  \nno...\n\nAt any rate virtual tables add some overhead,  just like funcitons add overhead to C.  Really it is the compilers job to make the code run faster.  I would study some more, maby even take a class in compilers, it is interesting stuff, and would help you look less foolish.\n\nSo in short yes inheritance makes overhead, but then again, if we all wanted to run blindingly fast we would all write raw opcodes, Assemblers add too much overhead ;)\n\nOh btw, you will, on takeing a class on compilers find out why C++ is better than writeing all in raw assmembly... it scales better.  All of the code duplication would invite situations for bugs, and bloat the size even more.  \n\nCheers\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "The whole \"virtual functions are slow\" thing is misleading. Yes, virtual functions are slower\nthen static binding, and should not be used randomly -- but they're pretty much the\nfastest way of implementing dynamic binding.\n\nThe only common alternative is the use of switch statements.. But those are less\n flexible, nearly a bad with respect to pipelining, and quite a bit larger, so they waste memory\nand potentially put wastefull stuff into instruction cache..\n\nThe overhead is also not at all that signficiant -- a few\nconstant-time instructions, even involving a piepline flush, hardly make a difference\n with respect to interactive performance.."
    author: "A Sad Person"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "Is Linux faster then the Windows kernel? I would think so.\nIs the Windows graphic shell faster then KDE? probably.\n\nYou say that it takes 30 seconds to bring up the file manager and you're still using KDE! With that kind of performance I would wipe it off my harddrive right away.\nI also wish the performance would be better but it's getting better and it's perfectly usable now for me anyway (PIII 450)."
    author: "zank"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: ">Virtual functions are gonna give you code overhead. They do reduce the speed \n>of your code, because you're trying to maintain a vtable of functions \n>(strictly \"class methods\") that have been derived from a different source.\n\nYou're violating the good old 90/10 principle (10% of the code spents 90% of the time). Rewriting methods that are called once won't get you anywhere. Like all other low-level optimizations you are only wasting time (and obfuscate the code) unless you concentrate on the hotspots. And even there it is rather unlikely that virtual methods are the cause, unless you have tight-innerloops that frequently invoke them. Especially the startup delay won't change by doing any optimizations in KDE, it's a problem of runtime-linking.\nAnd BTW most programmers are too lazy to add the virtual keyword for methods that do not need it anyway."
    author: "Tim Jansen"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "if you are really a C++ coder you should look into a career change. or at least start using a language you understand better.\n\nin the email thread that was referenced they were talking about classes that use copy-on-write ref counted memory to make passing objects by value cheap. it is not as cheap as passing pointers since you will still have the overhead of the ctor and refcounting, but try assigning a QString to another QString several thousand times and see how quick it is. however, as fast as it is, it isn't quite as fast as using plain pointers.\n\nthat is what they were talking about. nothing about virtual methods.\n\nand what is this shit about \"put a virtual method in a base class  and you are forced to include it in all the classes you derive it from\"? i suppose you meant \"derive from it\", and i also think you were confused and thinking about pure virtual methods. virtual methods don't give you any more overhead than regular methods (which also get inherited. shock, shock!) when the linking can be done at compile time. it is only when you take advantage of features such as polymorphism that force run time linkage that you incur a performance penalty (beyond what is normal for a function/method call), and even that isn't a huge issue. vtables aren't exactly memory intensive either.\n\nno, it isn't the use of virtual methods that cause horrible slow downs, though they do have a small cost.\n\non that note, it is often quite practical to use a virtual method as that often saves development time and, used correctly, can even streamline your runtime operations.\n\nas for konq taking 30s to start, please try a properly built set of KDE binaries from a recent release. i would suggest something like 2.2.2 by SuSe, Redhat or Debian. they all do quite good with their builds (as does fbsd, apparently). also note that virtual methods or no, the start up time issues have to do with the linker and shared libraries.\n\ni'm still wondering if you are a really clever troll or just a moron who thinks he's a coder."
    author: "Aaron J. Seigo"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "Templates and inheritance combined can give you the same flexibility as virtual functions and no extra overhead. What you loose is object polymorphism. \n\nIt's a tradeoff: \n1) Either you go with object polymorphism, thus using vtables or\n2) You go with templates using \"recurring\" inheritance and inlining\n\n1) can give you a tremendous flexibility and extensibility at runtime. The price is speed. But who, cares, buy a bigger machine. If you have a smaller machine, then run a simple window manager or checkout KDE 1.0/Gnome 1.0 or something from CVS and run that.\n\n2) can give you blistering execution speeds which is in some cases faster than C. The price you'll have to pay is memory usage. But then again, buy some more ram sticks. You always have the KDE/Gnome 1.0 option.\n\nSoftware is in symbiosis with hardware. When hardware gets better, then software tend to utilize the extra boost to do something useful. It's a fact of life. Deal with it.\n\nI suggest that you should probably read up on C++ before starting to bash out on people. Obviously, the guy you bashed, probably read a couple of more books than you have..."
    author: "J\u00f6rgen S"
  - subject: "Amazing"
    date: 2002-01-19
    body: "Microsoft has taught a whole generation of very bad concepts.\nIf you really want speed, then don't worry about the language.\nworry about the algo, which is exactly what was happening.\n \nMSFT has built lousy software for such a long time, that ppl are buying into it.\nNext, you will be saying that Linux/BSD/Unix need to start worrying about\nsecurity by using Microsoft products."
    author: "a.c."
  - subject: "Virtual Functions"
    date: 2002-01-20
    body: "Virtual functions are useful for many reasons.  I guess your C++ for Dummies book doesn't really cover OOP. To tell you the truth, I have a hard time with some of the more advanced parts of it. But at least I am aware of where I'm ignorant.\n\nI can see, however, where virtual functions make coding easier. This leads to less bug-prone and easier to maintain code. It is a constant trade off. KDE would probably be easier to maintain if it was say, written in Python. But that wouldn't be a good idea (though in 5 years from now when computers are unimaginably fast - who knows?). KDE could have been written in assembly, but that would also be a bad idea."
    author: "Eean"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "1) the developers have not been rushing kde3 out the door\n\n2) many of the developers have been concentrating on performance issues\n\n3) start up performance is due to the gnu linker. gnu ld is not part of the kde project. the kde team can not do much about it, though they have helped out the gcc team wherever possible. stop blaming the kde hackers for a weakness in gcc.\n\n4) the speed up bero talked about was not based on any sort of actual benchmarking, it was just a number he pulled out of the air based on an educated guess.\n\n5) bero wasn't talking about virtual method usage, he was talking about things like copy-on-write shared memory based classes being passed about\n\n6) polls of users are nice, but they rarely tell the developers things they didn't know, nor do they often result in one of those users going, \"hey, you know what, i'm going to help make something happen.\"\n\nperhaps i'm just bitter tonight. or perhaps seeing the same wrong-headed things said over and over again (though i understand how people arive at those concepts) just gets exasperating after a while?"
    author: "Aaron J. Seigo"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "I totally agree. This discussion is *old*, and a certain percentage in a certain poll doesn't make something happen."
    author: "Johnny Andersson"
  - subject: "Re: Focus on speed now"
    date: 2002-01-22
    body: "I am not a KDE developer. I wouldn't like to critizise the job of the KDE developers but I found sad that you blamed gcc. Why? It's simple: You developed a product (call it A) using some other (call it B). When developing A you should know the weekness of B. If A is bad due to B it's your fault for having used it. Remember KDE 1. This was fast! Maybe you should have use C instead? Ok, let's say that C++ is nicer than C. Then just accept that KDE is slow because you decided it to be but not because of the gnu linker. Don't critisize gcc the same way you don't like KDE being critisized."
    author: "lanbo"
  - subject: "Re: Focus on speed now"
    date: 2002-01-22
    body: "I am a developer (albiet not a KDE developer).  There are two problems with what you are saying.\n\nWhen you design a project, you use tools.  When you then profile the project, you find where the slow spots are.   It is perfectly reasonable to honestly say: \"A big slowdown in launch speed is due to this tool\".\n\nThe second problem is that the KDE project does not deliver anything but source.  Any C++ compiler on any POSIX/X based system should be able to compile the entire environment.  Effort has been made to make it work with the Solaris compilers from Sun, and on many OSes like AIX, BSD, Linux and Solaris.\n\nSo it's more like: \"Hey, we built this blueprint for a widget.  You can use wood or steel.  If you use wood, we've noticed it can catch on fire.\"  GCC is not the only compiler used to compile KDE, but pointing out (and trying to fix) it's weaknesses is perfectly reasonable.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "> certain C++ features that have huge overhead. (Virtual functions IIRC)\n\nThe overhead of virtual functions is barely measurable (check \"The C++ Object Model\", by S. Lippman)."
    author: "Guillaume Laurent"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: ">The overhead of virtual functions is barely measurable (check \"The C++ Object Model\", by S. Lippman)\n\noh now you did it, you actually sited an actual source containing valid facts... just for that you should be kicked off of the dot ;)\n\nI think the largest problem with C++ is the fact that old school ignorant C and VBasic developers try to code with it.  You cannot write efficent C++  with a C style.  I have seen this in numerous clients code bases.  The only thing C++ about them is the compiler.  OOD and C++ are like any tool, they can be made to suck if you do not understand them.\n\nEither way, I have seen how well KDE/QT scales, and how well motif scaled.  This is a reason why I reccomend QT over MFC and Motif now.\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Focus on speed now"
    date: 2002-01-19
    body: "> Wouldn't it really be nice if <b>all</b> the KDE hackers, before rushing out\n> KDE3, would spend and additional two weeks (or even a month) just addressing > performance issues.\n\nWorking on performance issues is an extremely tedious work. It requires a lot of knowledge about the overall architecture, it requires huge amounts of time, preferably a really fast machine to make gprof enabled builds in reasonable time and a certain amount of knowledge about code generation and how C++ code gets translated.\n\nMost KDE hackers work on KDE in their free time. They do it because it is fun,\nbecause it possibly even allows them to relax from some other work. But often the free time is limited. That is why most often the main thing that counts when hacking is to get it working.\n\nIt is certainly not fun to sit down, make a gprof build, run applications, make profiling test cases to find out where performance issues are, study other peoples code, etc. It is not something you can enforce (that's how I read your statement) on people. You have to motivate themm to do it. If it's not combined with fun few people are going to do it in their free time, no matter how many polls are being made and how many users complain on lists. They better get down and dig into the code to help out!"
    author: "anonymous"
  - subject: "Tabbed browsing is the wrong approach"
    date: 2002-01-18
    body: "I think it's a mistake to include tabbed browsing support in Konqueror. Don't get me wrong -- tabbed browsing is great, but it is not the kind of task that individual applications should handle. Currently Konqueror windows are summarized in the taskbar -- the taskbar could just as well create a repositionable list of Konqueror windows that becomes visible as soon as you click the Konqueror main task in the taskbar (which would be the last window selected, or the first window in the order). This way, any application with many windows (e.g. multi-windowed e-mail, news, FTP, IRC ..) would be able to utilize this feature if the user wants it to, whereas the application developers would save a lot of time. Certainly it would also be possible to do this in a \"tab-like\" fashion, although this may be trickier than using an additional taskbar the content of which changes when you select an application that has multiple windows.\n\nI've submitted this as a wishlist item:\nhttp://bugs.kde.org/db/34/34981.html"
    author: "Eloquence"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-18
    body: "Personally, I would disagree.  I sed galeon for awhile (though currently I am using Konqi), and the one feature I desperately miss from Galeon is the tabbed interface.  Since Galeon and Mozilla both have tab support, I think that Konqi should have this too -- and that, if you don't want it, or want it in a different way, this is something that should be \"yet another option\"."
    author: "Matthew Kay"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-18
    body: "I like tabbed browser!\nI'm using Opera 'til Knoqui doesn't support it!\n\nplease hack it for us!:)\n\ntanx\nMauro"
    author: "Mauro"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-19
    body: "So you want something like PWM?\n\thttp://www.students.tut.fi/~tuomov/pwm/\n\nPWM is a tabbed window manager - each window can have lots of clients running in it. See\n\thttp://www.students.tut.fi/~tuomov/pwm/screenshots/pwm-2.jpg\nfor an example."
    author: "Jon"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-19
    body: "Wow, someone actually understood what I wrote :-). Thanks for the link, PWM is interesting."
    author: "Eloquence"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-19
    body: "why would it be a mistake if it is configurable abd you can turn it off? in fact, it will probably be off by default. if you don't like it, it won't get in your way. some people really DO like it though (i'm not one of them) and so will be happy to have the option to take advantage of tabbed browsing/file management.\n\nso don't worry, be happy."
    author: "Aaron J. Seigo"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-19
    body: "He's not saying he doesn't want it, he's specifically saying that he just wants it implemented in a way that allows for expandability and use in multiple applications."
    author: "Carbon"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-19
    body: "aaah... i just reread it again and i think i see what he's asking for: extending the taskbar so it can be configured to just show one \"tab\" for each window of the active application ... this is already pretty much how i use the taskbar with task grouping turned on (though of course it also shows all the other windows too). i think one of the strengths of tabbed browsing is that it puts this interface directly inside the application's UI so it is within easy reach of the menus, toolbars, etc ... it also means you don't need to switch the whole window nor move your mouse very far from the application itself, which can be a big win if you don't run the app maximized or have a taskbar at the top of your screen. *shrug*"
    author: "Aaron J. Seigo"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-21
    body: "I love that idea, and have been wanting something like that for quite some time. I recently read an old article on OS/2's Workplace Shell, which had a similar feature, and it got me thinking about it again."
    author: "Rakko"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-01-21
    body: "I think the main points in tabbed browsing are:\n* to save resources by sharing the same window\n* to minimize mouse movement required to switch between sites in the same window\n* to minimize the number of entries in the taskbar, and the number of windows - no need for any of that window switching, it's all in one.\n\nYour suggestion (which looks very much like the already available task grouping!) doesn't address either of those requests."
    author: "David Faure"
  - subject: "For a common tabbing approach"
    date: 2002-01-22
    body: "I think that tabbed sub-windows are very interesting for Konqueror, but it is also interesting for each multi-sheet program. For Kate (already here), KWord, KSpread (already here) and so... even KMail, KNode... And it would be possible to create in the same way these tabbed sub-windows... It would be very innovating and powerful...\n\nWhat would be also very powerful is the ability to create taskbars in every app in the same way, creating buttons from ANY command of the menus. It was made by a fantastic program in Windows 3.1, a shareware called Superbar (buttons had one icon or one text or icon + +ext).\n\nPerhaps it would have been a good idea for KDE 2, now I fear it is too late..."
    author: "Alain"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-08-26
    body: "I believe another benefit of tabbed-browsing really becomes evident when on a slow or error-prone connection.  For example, when doing queries in a search engine, I can quickly scan through and load all the pages I might want to investigate more deeply into.  While I'm reading the first, the rest are loading.  The best part is it can be configured not to focus/raise the windows.\n\nBut I see the point in doing this at the window manager level.  Sawfish is the closest thing I can find, so far, which does just what I explained.  If I want a window to have focus, I want to tell it to do so.  Even certain dialog boxes annoy the hell out of me when I'm typing and they pop up.\n\nTwo features I'd like using:\n\n1. Window doesn't get focus unless I tell it to.\n2. Window is created at one level lower of the focused window.\n\nGnome consolidates windows into one icon when it sees fit, I'm happy with its judgement.  I'd hope that multiple windows of the same application share resources.  Unless they're all running java... I don't forsee a problem with having more than 100 or so open.  Perhaps you could run many more in a tabbed environment, but who cares at that point?  "
    author: "hank"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-07-03
    body: "Please, don't do this to Konquerer. UI developers have been arguing over MDI for years now, and the conclusion has _always_ been (Read Alan Cooper's work. Read Nielsen's surveys. Read any UI expert's comment on MDI) that MDI is a broken concept, and that users can't handle it properly. Beyond that, it adds navigational and operational complexity. There is no need for us to main Konq's interface by adding a feature that _should be provided by the WM_.\n\nThe _window manager_ is responsible for handling window placement. If you think accessing windows in a tabbed fashion is convenient (and it is, of course), look for a window manager that supports this feature. Ion and PWM are two great examples of WMs that do. Oh, sure they lack features. Probably a lot _more_ good would be done spending time working on getting them up to speed than hurting Konq.\n\nMozilla suffers from tabs (though not horribly, yet, because we don't have an end-user market share), and we are only beginning to see problems caused by the confusion it creates. It's just a sticky pit of UI hacks now that it's in.\n\nWant an example? \n\nhttp://bugzilla.mozilla.org/show_bug.cgi?id=108973#c45\n\nHorrible. Don't let this happen to you."
    author: "Christian Reis"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2003-02-07
    body: "Huh?\n\nYour example is broken. The bug is about being able to lose all tabs in a window by a simply keystroke or click. The comment is about a strange suggestion as solution. Just because ridicilous solutions for a simple bug are discussed does not mean the behaviour is too complex. The simple solution is to pop up a window and ask for confirmation (which is the main suggestion in this bug).\n\nImage KDE had a method to quit by a click (it has). Imagine that it would quit without asking beforehand (it does not, it asks you how to proceed). You could lose all the state of all your windows on your desktop. Image somebody suggests a ridicilous solution for that. Does that make the concept of Windows in KDE overly complicated? No. So doesn't the example you posted for Tabs.\n\nEven if tabs would be complicated, there is no reason for users to suffer as you like to say. All people I know which don't like tabs, just don't use them. Easy as that.\n\nThat said, yes, tabs should be handled by the window manager, presumed I can still have all the comfort that direct support by the applications gives me (easy keyboard switching, drag and drop, creating new tabs, and so on).\n\n"
    author: "Benjamin"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2002-08-02
    body: "Hmmm... I'm not sure how old this thread is, as it doesn't give yearly timestamps, and the one gentleman mentioned KDE2, which is now being replaced.\n\nHowever, I have an opinion on the matter.\n\nI LIKE tabbed browsing. I LIKE tabbed Konsole. The ability to add another window, within the current window, and switch between said windows with a keystroke is very useful and efficient.\n\nI hate how the current setup works on window architectures. Windows never seem to open up the same size, and there is little hierarchy between windows of the same application. (ALT-TAB just doesn't do it for me...) (although I admit that KDE attempts to change this with the current taskbar)\n\nIsn't UNIX all about efficiency anyway? I mean, honestly, if I wanted to point and click all the time, I'd run a Microsoft box. (And what's up with everyone assuming KDE only runs on Linux?)\n\nAs for issues regarding implementation:  (window manager vs. application)\nI believe it would be slick to implement this in the window manager itself. That way the feature is always available to all applications. However, I don't think multiple WINDOWS need to be opened by the window manager. I think the tabbed architecture needs to be standardized in the base class of a window itself. I would think this would improve resource consumption.\n\nAgain, this may be a really really old thread, but I just thought I would chime in with my two cents.\n\nMatthias\nSystems Administrator,\nC/C++, Java, Graphics, PHP/Web Programmer,\nand Unix Enthusiast."
    author: "Matthias Trevarthan"
  - subject: "Re: Tabbed browsing is the wrong approach"
    date: 2003-01-04
    body: "Don't you feel stupid now."
    author: "Gnome User"
  - subject: "Outdated information about Debian KDE packages..."
    date: 2002-01-19
    body: "Last time I talked to Chris Cheney, the future KDE3 maintainer for Debian, he informed me that due to lack of time, Daniel Stone will not be able to maintain the KDE2 packages. It appears that for now Chris will be maintaining both."
    author: "Kamil Kisiel"
  - subject: "Document/File Menu entry: send document"
    date: 2002-02-20
    body: "Here's a good feature I saw when looking over by boss's shoulder (he uses windows):\nIn Excel you can just send the document you are working on to somebody else. \n\nWhy don't we implement this feature in all KDE applications which process singular files? I imagine all of the KOffice applications could increase usability with such a feature. \n\nLet's say, KWord links to KMail, registering the KWord-document as an attachment to an Email. \n\nThis functionality would be applicable to KPim-related tools and many more... \n\nRegards,\nMarkus\n"
    author: "Markus Heller"
---
<a href="mailto:aseigo@mountlinux.com">Aaron J. Seigo</a> delivers again with <a href="
http://kt.zork.net/kde/kde20020111_30.html">Kernel Cousin KDE #30</a>. This week the summaries of the development mailing-lists include talk about <a href="http://developer.kde.org/development-versions/kde-3.1-features.html	">KDE 3.1 features</a>, Debian package maintainance, KDE artwork, GCC3 and the new <A href="http://enterprise.kde.org/forum/">KDE::Enterprise Forum</a>. Enjoy!
<!--break-->
