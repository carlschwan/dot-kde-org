---
title: "ZDNet Covers Upcoming KDE 3.1"
date:    2002-10-19
authors:
  - "numanee"
slug:    zdnet-covers-upcoming-kde-31
comments:
  - subject: "Let me guess"
    date: 2002-10-19
    body: "* KDE league is not the publisher of KDE\n* similar to aqua and winXP\n* made its debut in the open source Mozilla browser\n"
    author: "Hakenuk"
  - subject: "Excellent..."
    date: 2002-10-20
    body: "Dre's article has been a huge success in creating a whole lot of PR. There's even been KDE 3.1 coverage in Norwegian IT news sites, and everywhere there's people talking about the fantastic new KDE about to be released ;) Excellent move."
    author: "Haakon Nilsen"
  - subject: "Re: Excellent..."
    date: 2002-10-21
    body: "And it's funny: such a great news from RL and nothing's happening here in this normally quite crowded board then. ;)"
    author: "Datschge"
  - subject: "i do hope they cluttered up the UI *more*!"
    date: 2002-10-23
    body: "we need *more* buttons and *more* options all over the place!!!"
    author: "noone"
  - subject: "Re: i do hope they cluttered up the UI *more*!"
    date: 2002-10-23
    body: "What means more? I hope they will someday devide the hole kde into modules. For example if you want to use konqueror only you have to install the whole kde (kdelibs, kdebase and lots of other depending software). This isn't the way it should be. To me, they are starting to imitate Macroshit :). Why can't they design an user interface on it's own? Why has everything to look like MS?\n\nBesides that it is a very good project! I like it."
    author: "Sjaak"
  - subject: "Re: i do hope they cluttered up the UI *more*!"
    date: 2002-10-24
    body: "> I hope they will someday devide the hole kde into modules.\n\nIt is divided into modules, such as kdelibs, kdebase, etc..\n\nReally, it's personal preference. In KDE, people complain that individual packages are not released. With GNOME, people complain that they don't like installing thirty different packages to get the whole environment. \n\nAnyways, it's really the distributer's job to splice KDE up, as for example, Debian does.\n\n"
    author: "fault"
  - subject: "Re: i do hope they cluttered up the UI *more*!"
    date: 2002-11-01
    body: "What I actualy wish for is for them to provide a single download file and then to install I select what I want to install and what not to. To have to download 23 different files is to engrossing and is inefficient. I think we should have single packages, .tar.gz maybe that contains everything else, and then users install by just running that one and having a menu asking what they would like to install, and whether they would like t remove any previous versions of KDE and so on. The old method could still be provided. I don't think space should be too much of a concern anyway."
    author: "Maynard"
  - subject: "Notebook usability"
    date: 2002-10-24
    body: "Congratulations to the KDE team. Great things have been archieved,\nand they're still getting better. \n\nSomething that just caught my eye on my brother's XP notebook:\nWhenever he puts his mobile phone next to the infrared diode, \na little sign comes up in the status bar telling him about other IR\nstations around. \n\nWhy don't we also develop something like that and automatically offer \na sync with the PIM module to the user?\n\nRegards,\n\nMarkus"
    author: "Markus Heller"
  - subject: "True ?????   "
    date: 2003-03-15
    body: "\nthe only thing i know is that THIS is a lot better AND A LOT MUCH MORE STABABLE THAT ANYTHING CREATED BY macroshit(i e ; i think ill by this dommain name :) )\nexcep version 3.2 \n\n\nsincerely yours...........\n\nVeikra \n\n"
    author: "Veikra"
---
<a href="mailto:voidcartman@yahoo.com">Ismail Donmez</a> wrote in to point out that <a href="http://www.zdnet.co.uk/">ZDNet UK</a> is featuring <a href="http://news.zdnet.co.uk/story/0,,t269-s2124074,00.html">a nice article</a> on the upcoming KDE 3.1.  The article is based on Dre's excellent <a href="http://promo.kde.org/newsforge/kde-3.1.html">teaser preview</a>, and despite one or two slight inaccuracies, provides some very nice publicity for KDE aimed at the general computing public.
<!--break-->
