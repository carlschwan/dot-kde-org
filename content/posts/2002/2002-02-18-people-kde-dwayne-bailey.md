---
title: "People of KDE: Dwayne Bailey"
date:    2002-02-18
authors:
  - "Inorog"
slug:    people-kde-dwayne-bailey
comments:
  - subject: "answers ... questions ... "
    date: 2002-02-18
    body: "congrats on the kid, thanks for the translations (not that i speak any african languages), and daniel is a guy.  but ... you hate tea?!\n\ngreat interview as usual... =)"
    author: "Aaron J. Seigo"
  - subject: "Re: answers ... questions ... "
    date: 2002-02-18
    body: "Doesn't like tea?  Why, that's heresy!  \n\nCraig and Wiggle will be knocking on Dwayne's door shortly...\n"
    author: "KDE User"
  - subject: "Re: answers ... questions ... "
    date: 2002-02-19
    body: "craig and wiggle.. sitting in a tree. k-i-s-s-i-n-g"
    author: "fault"
  - subject: "Finally"
    date: 2002-02-18
    body: "     ~ Who does the dish washing at your home?\n\n     Me.\n\nFinally someone doesn't answer \"dishwasher!\""
    author: "Jackson Dunstan"
  - subject: "fetus'"
    date: 2002-02-18
    body: "\"resemblance to a fetus' ultrasound image\"\n\nWay not Baby Ultrasound image? Or are you scared of the thought Police?"
    author: "Craig"
  - subject: "Re: fetus'"
    date: 2002-02-19
    body: "i think they just arrived."
    author: "Aaron J. Seigo"
  - subject: "Re: fetus'"
    date: 2002-02-19
    body: "And they're named Craig."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: fetus'"
    date: 2002-02-20
    body: "Unlike what the US administration leader you wants you to believe, a fetus is not a baby.\n\nfetus n: an unborn or unhatched vertebrate in the later stages of development showing the main recognizable features of the mature animal [syn: fetus] \n\nba-by : 1.An adult or young person who behaves in an infantile way. 2. A very young child; an infant. \n\nJust my 2 euro-cents.\n\n--\nTink"
    author: "Tink"
  - subject: "Re: fetus'"
    date: 2002-02-20
    body: "Oh man Tink. I guess i don't know what to say. I guess we have different value standards. "
    author: "Craig"
  - subject: "Re: fetus'"
    date: 2002-02-20
    body: "Please, don't turn this into a discussion on abortion. It was a correction of terminology:\n\nYou said \"maybe you meant baby ultrasound image, not fetal ultrasound image\". Tink said \"it isn't really a 'baby' until it's been born\". And that's correct: it is not a baby until it is born, that's the medical terminology, irregardless of any other properties of fetuses or babies. And let's end it with that, the Dot is a bad medium for political stuff."
    author: "Carbon"
  - subject: "Re: fetus'"
    date: 2002-02-21
    body: " \"Unlike what the US administration leader you wants you to believe, a fetus is not a baby.\"\n\nThats what makes me think Tink is promoting the murder of babies. Its always a good time and place to point out the murder of the innocent.\n\nCraig"
    author: "Craig"
  - subject: "Re: fetus'"
    date: 2002-02-21
    body: ">Its always a good time and place to point out the murder of the innocent.\n\nIf it's always a good time and place, then it would probably be a good time right now to go someplace else and talk about politics (or the murder of the innocent, or under-buttered popcorn, or whatever, as long as it's not KDE related). I don't care what you think about abortion, I doubt you really care what anyone else thinks about abortion except to the point that they agree with you (notice that I'm very specifically not saying what my opinion is on abortion, and I don't think Tink was either), and frankly, I don't think anyone else here is going to give a hoot either, at least not via this medium. Please, go elsewhere if you want to talk about abortion (kuro5hin or /., having broader topic ranges, would be good), as despite your rather broad allusion to the contrary, not every time and place is filled with people who would love to hear all about your opinions.\n\nI know I don't."
    author: "Carbon"
  - subject: "Re: fetus'"
    date: 2002-02-21
    body: "And I suppose a discussion about tea is KDE-related? :\u00ac)\n"
    author: "Malcolm"
  - subject: "Re: fetus'"
    date: 2002-02-21
    body: "Unless you're a vegetarian, you better STFU.  That cow or pig you are murdering is just as intelligent and innocent as that 2-year old fetus.\n"
    author: "ac"
  - subject: "Re: fetus'"
    date: 2002-02-22
    body: "2-year old fetus? :-)"
    author: "Carbon"
  - subject: "Re: fetus'"
    date: 2006-11-30
    body: "He or she is still a human.  I'm not going to do that whole common pro-life \"oh, look at her fingers\" thing because unless you look at the eight-week-old aborted fetus or watch an abortion, it never really makes sense to anybody.  Instead, a little science...\n\nHumans possess all the genetic material of the species Homo sapiens, as does a fetus.  Now, a tumor also contains all the genetic material of the species--however, a tumor cannot regulate its own internal processes, nor can it possess a seperate blood type from its host.  Also, it is not protected.  \n\nThe fetus is protected by two uteran layers and the placenta.  Why would the mother's body contain so many protective devices for something that technically is just a part of her body?  If the fetus were just a part of her body, there for the removal, it would not need protection from her body.  The mother's immune system constantly attacks the fetus/developing embryo.  And yet her own body protects the baby as well, disproving a virus/parasite comparison.\n\nI have heard pro-choice activists call the fetus a baby when they're not arguing with pro-lifers, which makes me laugh, since the main argument is that she isn't.  It seems like in the subconscious or something people do know the science.\n\nFood for thought: Does being unhatched make you unhuman?  Is age THAT important?  Is a teenager more human than a two-year-old?\n\nThank-you for your two euro-cents, tho, and I love your sign-in name."
    author: "PetrePan"
  - subject: "Best line of the interview..."
    date: 2002-02-19
    body: "\"I have to ask this :) tell me is Daniel a boy or a girl?\"\n\nNo disrespect to Mosfet at all but if you don't know culturally that Daniel is not Danielle then the picture(s) alone would leave one to wonder :)  \n\nWith as much code as he kicks out, tho', it's getting harder and harder to decide on Kuickshow or Pixie Plus."
    author: "tasty biznitcheos"
  - subject: "Mosfet, Tea and Namibia"
    date: 2002-02-19
    body: "Rest assured Dwayne, Mosfet is a guy but he likes to crossdress.\n\nYou hate tea because there is only crap around in Southern Africa. Even in Cape Town where they actually have tea shops, they sell you the stuff they broom up from the floor in India. Try some Darjeeling First Flush and you will change your mind. \n\nOf course, there is a KDE guy in Namibia. Me. I tried to make a KDE meeting possible in Namibia. The infrastructure is here but the travelling costs kill it."
    author: "Uwe Thiem"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-19
    body: "I don't think it's crossdressing, it's this american Goth crap, which is extremely annoying for somebody born and raised in Gothenburg.\n\nAaah, just sipping Twining's Earl Gray (the king of teas) from my largest cup.\n\n(and we're gonna beat the crap out of the americans in the icehockey tournament, that'll teach them)"
    author: "reihal"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-19
    body: "Yeah I have never been a goth fan, save for the music, but hey there are worse cultures to belong too, like the furries :\\\n\nPersonaly I think black tea is the best...  I buy the lose leaves and freeze them, keep them in the freezer and never take them out until you make your tea.\n\nAlso make sure you look at the leaves before you get them, make sure they are not beaten to a pulp, get them as dry and whole as possible... \n\nit takes quite a bit to get a good cup of tea arround here too, so i make my own...\n\noh and also, i sometimes drop in a whole clove...  it adds a hint of kick to the cup.\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-20
    body: ":: Yeah I have never been a goth fan, save for the music, but hey there are worse cultures to belong too, like the furries\n\n   As a participant in both cultures (among about a zillion other subcultures) they are all about the same thing - a sense of family and togetherness.  Having written and spoken about and for groups like these, they are much like the KDE team and family - a group of people working together for their own various reasons, all progressing their common goal which is constantly being redefined.  Certain aspects of any group with non-direct renumeration are fundimental, no matter the \"flavor\" of the group.  \n\n   Keep it in mind - to someone else, you're a freak for caring or even knowing what your computer is running.  The concept of \"Software with Freedom\" or even \"You can make your own computer programs\" is alien and unfathomable to the vast majority of people.  I've interviewed Furries and Linux advocates on the radio (seperate shows, of course :), and had the same people call in with the same question: \"What is wrong with you?\" (I've never interviewed a \"Goth\" as such, but I've interviewed artists in the Goth scene, Vampire: The Masquerade groups, etc.  KDE will get its show eventually).\n\n    The most important thing to consider is that these various volunteer groups have dealt with their own \"trolls\" that make the ones on the Dot look tame, their own \"over zealous promoters\" that make Craig look like a mellow guy, and their own \"erratic talents\" like Mosfet.  In general, they all follow the same \"5% does 95% of the work\" that KDE has.  And they all have solutions and problems without solutions that any community can benefit from.  As KDE continues, it will face issues from public relations (\"who speaks for me?\", see the flamewar in the article about the KDE League letter) to burnout of key positions that cannot be filled adequately, to that most deadly of all problems - simple growth.\n\n    KDE is a very special project - a community project.  But other special focus communities exist, and they are our brothers in spirit, if not in goal.  Consider that very carefully when you dismiss other groups as being \"bad\" and \"worse\".\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-20
    body: "+5, Nifty :-)\n\nI couldn't agree more with you. I see all sorts of similarites between the KDE project (and the OSS community in general) with lots of other communities. Heh, you almost wonder if anyone isn't part of some sort of group like this (any sociologists in the house?)"
    author: "Carbon"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-19
    body: "In the icekockey routnament in the Winter Olympics, sweden is whithout a chance. OK. Belarus is not a problem, but Finnland realy kick swedish ass. And USA kick finnish ass...\n\nCzech were better than the swedes, even though there were two Scanians in the swedish team, but unfortunately lost. I think USA or mayby Czech will win. Russia is the best team, but they aren't motivated, and Canada have the best players. But no team...\nAnd finland only have Sel\u00e4nne or what he's called.."
    author: "a Scanian"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-20
    body: "Confused and offtopic, aren't you?"
    author: "reihal"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-19
    body: "I'm from Gothenburg too - should I be annoyed?  Because I'm not. ;)"
    author: "Johnny Andersson"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-20
    body: "Well, I'm from Gotham City, but there is no way that I would dress up in weird clothes."
    author: "Batman"
  - subject: "Re: Mosfet, Tea and Namibia"
    date: 2002-02-21
    body: "Yeah, well, Mr. I've-got-free-money-and-have-pants-in-the-early-comics, you're not so tough!"
    author: "Robin"
  - subject: "Strange question about smoking"
    date: 2002-02-19
    body: "Being a smoker, I can't resist a reply:\n\n[quote]\n~ If you are a smoker, does it ever happen to you that your cigarette sets your ashtray on fire? How often?\n\nWhat a strange question. I become more and more convinced that smoker are really not that bright.\n[/quote]\n\nAn alternative explanation could be that Tinks' perception of smokers does not correspond well to reality :-P\n\nFunny guy though, and I think it's great to have support for many languages in [insert any desktop environment here]. What a task, to cover 12 different languages (11 South African & 1 British English). Amazing job, keep it up! I hope you are right about the South African children using KDE with your translations in the forseeable future...\n\nPS. I wonder, are the differences between the languages great, or are they dialects? (pardon my ignorance)"
    author: "AC"
  - subject: "Re: Strange question about smoking"
    date: 2002-02-27
    body: ">PS. I wonder, are the differences between the languages great, or are they >dialects? (pardon my ignorance)\n\nSome are dialects but that actually hasn't helped us much.\n\nThere are two major languages groups: Nguni and Sotho.  We are working on the\nNguni ones now.  Then there is Afrikaans which is a Dutch (with German and French infouences) decendent - this is not our major foucs because work has already been done on this language.  We are keeping a watchfull eye and will step in if it needs help to keep it in KDE."
    author: "Dwayne himself"
---
After a short reprieve, <a href="mailto:tink@kde.org">Tink</a> is back with a new interview. <a href="http://www.kde.org/people/dwayne.html">Dwayne Bailey</a>, a KDE friend who bears an amazing resemblance to a fetus' ultrasound image, is involved in translating KDE into the many languages spoken in South Africa. Dwayne's work recently <a href="http://dot.kde.org/1012991276/">drew high praise</a> from a South African official organization. Great work, Mr. Bailey.

<!--break-->
