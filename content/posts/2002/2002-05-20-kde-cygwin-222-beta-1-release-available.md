---
title: "KDE on Cygwin: 2.2.2 Beta 1 Release Available"
date:    2002-05-20
authors:
  - "binner"
slug:    kde-cygwin-222-beta-1-release-available
comments:
  - subject: "KDE in windows is really Kool"
    date: 2002-05-20
    body: "Kongratulations to the developers, KDE on windows is a very kool.\nKonsider all the kool things you kan do when windows and kde apps koexist.\nWhen the port is komplete, you kould run koffice right next to mikrosoft office and kopy and paste between them.\nI kould finally run my favorite kalendaring software on windows. \nKreative developers will be able to kombine windows koding tools with kdevelop to quikkly kreate kool kode.\nBut I am koncerned this will make people use mikrosoft more.\nWhy konvert to linux when all the kde stuff is available on your mikrosoft os?"
    author: "Koncerned User"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-05-20
    body: "Nice akcent :)\n"
    author: "CheeseHead"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-05-20
    body: ".\n<!--\nfuck  off!!!!!!!!!!\nsucks off!!!!!!!!!!\n\nI TOLD YOU BACKS OFF MAN!  DON'T HARRASS TO LINUX USERS WITH THAT SILLY COMMENTS !\n-->\n\n"
    author: "."
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-05-20
    body: "I am not a english native speaker - but either the previous poster is a troll - or he did not understood the comment. Or also possible - i misinterpreted his comments.\n\nThe comment: \"nice akcent\" refered to all the k-letters in the words (kode - code, kool - cool).\n\nJust my 2 cent komment ;o)\n\nIf i understood the above wrong please korrect me.\n\nThanks for the great work to all KDE participants.\n\n"
    author: "big_question"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-05-20
    body: "Well KDE for windows is sure alot slower than KDE for linux. Thats a reason why I would prefer to run it under Linux."
    author: "me"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-05-20
    body: "Its because of the cygwin emulation layer. Hopefully they'll be able to port the GPL Qt to Windows, and ditch cygwin and a X server at some point. Qt is obviously made to be ported relatively easily, though I'm sure its still a huge job."
    author: "Ian"
  - subject: "NO! Please don't!"
    date: 2002-05-20
    body: "A GPL Qt would do a lot of damage to Qt - a lot of their licenses are sold for in-house and/or bespoke products. If a GPLed alternative became available, people wouldn't have to by Qt anymore, the license revenue would crumble, and a very good and decent company would fall by the wayside. After all Qt has done for KDE and the free software community (there was never any great financial benefit to releasing the GPL version in the first place) it would be a shame to see the community then take them down.\n\nRemember, without Trolltech, we'd have to update Qt ourselves. That's not an easy job, look how long Gnome 2's been in development - every aspect of it had to be done by the community, from glib up. KDE benefits from havning a professional company of what, 50 people, working 40 hours a week on it's core library.\n\nI know I've said you don't need a reason to start coding, great stuff has come from people just looking to scratch an itch (look at the fish:/ and audiocd:/ io-slaves), but when there's a good reason to leave it alone, you should at least consider it."
    author: "Bryan Feeney"
  - subject: "You're totally wrong !"
    date: 2002-05-20
    body: "GPL mean you can use it to make GPL apps. So, if anyone does a GPL version of QT for windows, nobody will be able to make proprieatry software with it, just GPL software. It's the same NOW on linux.\n\nIn fact, I think it's a shame Trolltech hasn't released a GPL version of QT for windows because I don't imagine anybody to pay in order to make GPL software.. it's a total abheration !\n\nConclusion: if Trolls are too stupid to understand that people won't pay to make GPL apps, make a windows version of QT for windows ! After all, nobody forced them to make a GPL version for Linux !"
    author: "Julien Olivier"
  - subject: "Re: You're totally wrong !"
    date: 2002-05-20
    body: "The GPL says you must provide the source IF you distribute binaries. \n\nA lot of Trolltech's clients use QT for in-house development (software used only whithin the company) so they could use the hypothetical GPL'd QT for Windows and not release the source.\n"
    author: "Manfrodo"
  - subject: "Yes, please do"
    date: 2002-05-20
    body: "Just a few points:\n1) You basically argue that a) the free (GPLed) version for Linux is being subsidized by the Windows users. b) A port of Linux Qt to Win (GPLed) would seriously hurt Trolltechs business. So, if one day (which comes soon hopefully) Linux achieves the goad of \"world domination\", Trolltech will loos it's source from thos inhouse users of Win Qt anyway. What will they do? If you are right with your assumptions, then they will stop subsidizing Linux and make version 4.0 (or 5.0 or ...) non GPLed. That's the time, when KDE will have to work on Qt anyway.\n2) There are some open source applications, that should be available on Windows as well as on Linux that use Qt. These applications cannot be ported in a way that gives Win users their \"look and feel\", because it's currently XFrees look and feel instead. This is an obstacle many win users will not even attempt to master."
    author: "jmayer"
  - subject: "Re: Yes, please do"
    date: 2002-05-20
    body: "A lot of people don't seem to understand what I was on about. Most of Qt's business goes to programs developed in house, or programs developed by a company for a particular customer. It's not about making boxed products. In most cases the source is available anyway, if it's in-house it benefits everybody to have it, and for bespoke solutions (i.e. programs made to request) it's probably a part of the contract. That's why Qt non-commercial specifically states that you can't program with it in a company. If Qt/Win became GPL, it would make no difference to it's main customers, as they're usually making the source available for free anyway, so they could simply get the free GPL version (Qt might be able to charge, but everyone else could just copy it on, and it's been shown to be extremely difficult to make money from support).\n\nAs regards the loss of the Windows market, as Linux becomes more mainstream, more boxed software will be made available under proprietary licenses, and Qt should do well (though if the Free Software Foundations ideal comes to pass, Qt could find itself in trouble). I expect that the majority of Qt's money comes from Windows licenses.\n\nAs for making open-souce apps work on Windows, Windows users can get the look and feel by installing a theme. In a rootless X server running the WinXP theme for KDE most KDE apps would blend right in. Remember, a lot of the cheaper Windows applications don't look so homogenous. From the user's perspective a Cygwin system should look fine. Most Windows users who toy with the idea of installing Linux will have no problems with Cygwin."
    author: "Bryan Feeney"
  - subject: "Re: Yes, please do"
    date: 2002-05-21
    body: "I used to think that too but I changed my mind: no company will buy a product without a license or without a support. Trolltech's CEO Eirik Eng once told me that something like 1/5 of their sales was for Unix inhouse development where the client obviously could have used GPL Qt.\n\nAnd you'll get more with Trolltech's true Qt on windows, like the promising ActiveQt, than with a port that will always be late, doesn't provide support, is not commercially backed, ...\n"
    author: "Philippe Fremy"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-05-20
    body: "Do you have any special keyboard layout installed? I mean that when you press C, you get K :))? Such a layout should be default on the future verions of KDE, I think...\n\nJerzu"
    author: "Jerzu"
  - subject: "Re: KDE in windows is really Kool"
    date: 2003-06-24
    body: "Here I was thinking I had to use a B! What a silly bunt I am.\n\n(Badly paraphrased montypython joke.. yeahyeah, cant remember quite how it went!)\n\nNow, back to that b++ code"
    author: "shayne"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-06-03
    body: "Please help me how to start the KDE ?\nI have downloaded and installed the KDE cygwin directory but after that what i should do to start the KDE what files I need to have to start the KDE ?\nPlease help me \nThanks in advance\n\nBye\n{Praveen}"
    author: "PraveenML"
  - subject: "Re: KDE in windows is really Kool"
    date: 2002-06-28
    body: "I don't get it. Why have KDE on Windows?  KDE was originally designed for Linux, which is far better anyway! I think you can do kooler things when windows doesn't exist on your machine."
    author: "El Loco"
  - subject: "Re: KDE in windows is really Kool"
    date: 2003-01-20
    body: "I would still convert to linux anyways,  I have had much better experience with linux then any windoze box.\n"
    author: "AntiX"
  - subject: "Re: KDE in windows is really Kool"
    date: 2004-02-11
    body: "I like the idea cause there are apps in linux that I wish to use at work but it is frowned upon to change your OS at work with out permission from god.  I just wish I could get Ksimus to work on this in win2k :)"
    author: "DestroyerOfMidgets"
  - subject: "Screenshots"
    date: 2002-05-20
    body: "<a href=\"http://kde-cygwin.sourceforge.net/kde2.php#Screenshots\">http://kde-cygwin.sourceforge.net/kde2.php#Screenshots</a> - impressive!"
    author: "Anonymous"
  - subject: "XFree ?"
    date: 2002-05-20
    body: "Hi\n\nI don't understand something:\n\nWhy does it need an X server for it to work on Windows ?\nI mean why not just use QT for Windows to compile KDE ? Is it a legal problem or a technical one ?"
    author: "Julien Olivier"
  - subject: "Re: XFree ?"
    date: 2002-05-20
    body: "> Why does it need an X server for it to work on Windows ?\n<p>\n<a href=\"http://kde-cygwin.sourceforge.net/faq.php#E7\">http://kde-cygwin.sourceforge.net/faq.php#E7</a>"
    author: "Anonymous"
  - subject: "Uh uh kool uh"
    date: 2002-05-20
    body: "Yeah. It's cool... but who cares ? That not I don't want kde on windows, I just don't want windows - until it's open sourced of course. These people should work on koffice to push it at the required level instead of wasting their time (IMHO, they don't what they want...).\nSome tell that \"it would ease the migration later... blah blah\". It's bullshit: it won't show really what linux can do and just make people keep windows. Again, what gnu/linux should do is improve until it's really superior to other os/software. Then, the choice _will_ be obvious. And meanwhile, don't bother marketing or such: again, when it will be really superior (and I found the KDE framework full of potential for evolution, next step IMHO = full DBMS integration + at least partial switch to functional programming with strong typing in key components), it'll be the truth.\nBelieve me, boys and girls.\n"
    author: "HB"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-20
    body: "There's two reasons why this is beneficial. First it provides an easy non-destructive way for a windows user to experience the Linux desktop. Most people balk at the idea of partitioning their disk and installing a completely new operating system on it. This gives them a reason to give it a go.\n\nSecondly, there is work on a rootless windows version of X - this would allow KDE applications to operate transparently alongside Windows applications, which would allow more people to try them out, and, again give them more incentive to go for the whole thing.\n\nFinally, why on earth should anyone have to do anything for a good reason? If you've got an itch, scratch it and be proud ;-)"
    author: "Bryan Feeney"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-20
    body: "I agree. Although personally it's difficult to get beyond the feeling of 'wasting time' on the win32 port, I also remember trying out litestep (an afterstep clone for windows) five years ago on my w95 box. It was like a gateway drug, I've been linux-only for the last four years :-))\n\ncheers, wingo."
    author: "Andy Wingo"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-20
    body: "I think KDE's slowness on Windows can only hurt KDE's image. We all know most Windows users are not that smart (and that's an understatement). When they notice KDE's slowness, they will only think \"KDE is slow, KDE is Linux software, so Linux must suck.\""
    author: "Stof"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-21
    body: "What about GNOME's general suckiness?  Could that hurt KDE's image?  I think it's unfair too.\n"
    author: "ac"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-21
    body: "Yeah, rootless will make this pretty usable -- it's great on OSX (http://www.befunk.com/~ranger/osx-kde/)."
    author: "Ranger Rick"
  - subject: "KDE for OSX [Re: Uh uh kool uh]"
    date: 2002-05-22
    body: "Great to hear you make big progress. And nice shots!"
    author: "Anonymous"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-22
    body: "Right.  Additionally, the reason I would have is a little more contained within developer circles.  Porting any software to another disparate operating system, such as the win32 platform, uncovers a lot of weaknesses that, when found and fixed, can make it run even faster and smoother on all platforms.  For example, Habacker found that Unix Domain Sockets were heavily used and relied on for inter-process communication.  These discoveries and more usually don't come without a some-what reworked port of a software package by a different set of people.  Whether something is learned and applied to the HEAD branch ... well, good luck to Habacker and team!"
    author: "J"
  - subject: "Open source software"
    date: 2002-05-21
    body: "If KDE was a company, and you could move around people at will then you could argue that (though I still think you'd be wrong.) As it is, people are scratching their itch, which happens not be a office suite for KDE (or at least not their biggest of more interesting itch, if we take the analogy too far)."
    author: "Ian"
  - subject: "Re: Uh uh kool uh"
    date: 2002-05-22
    body: "What about companies IT rules ? Changing os isn't possible for bigger companies (expect you are the person who defines this rules), but introducing a new application by an administrator is quite easier. \n\nSo the strategy is to introduce some kde apps for specific needs and to extend the base of installed kde apps. \n\nOne day, it may be possible to delete explorer and set konqueror/kdesktop/kicker as default browser/desktop/... and than swithing the \"backend\" to another os like linux is not such a big task like it is now.\n\nRegards \nRalf \n \n\n\n"
    author: "Ralf Habacker"
  - subject: "Performance"
    date: 2002-05-20
    body: "How's Windows KDE's performance compared to Linux KDE?\nIs it faster or much slower?"
    author: "Stof"
  - subject: "Re: Performance"
    date: 2002-05-21
    body: "While I havent tried it out myself, I fully expect it to be slower.  The reason is because there are some \"go-betweens\" between KDE and Windows namely X and cygwin.  Essentially your computer would have to work harder since theres all those translation steps between KDE and Windows."
    author: "Jeff"
  - subject: "Re: Performance"
    date: 2002-05-21
    body: "It is significantly slower at starting apps. Once the app has started it is not too bad. It takes about 6 mins for startkde to complete on 500Mhz NT box with 1G of RAM.\n\nThe KDE1 version is a lot faster but still slower than a native environment."
    author: "hbc"
  - subject: "Re: Performance"
    date: 2002-05-21
    body: "Uh, you really mean 6 minutes? Perharps a spellerror (meant to be 6 seconds)?\nAnd if it is 6 minutes, is everything installed right and works everything correctly? \n\nIf this really is the case this sort of staruptimes are normal with very powerfull and fast computers THEN KDE ON WINDOWS REALLY SUCKS!!!!!\n\nDUMP IT AND CONTINUE TO WORK ON LINUXVERSIONS!!!!....NO SUPPORTING OF THAT GATESSHIT-PLATFORM!!!"
    author: "Maarten Rommerts"
  - subject: "Re: Performance"
    date: 2002-05-21
    body: "Thats 6 minutes (no spelling error). Everthing is installed and works correctly though (even the sound)."
    author: "hbc"
  - subject: "Re: Performance"
    date: 2002-05-21
    body: "This was only true for a pre alpha release. The beta release starts much faster (I have measured about 2 minutes (30 second for the second start) for the whole desktop on a PIII 733 MHZ Toshiba laptop). \n\nRegards \nRalf \n\n\n"
    author: "Ralf Habacker"
  - subject: "Re: Performance"
    date: 2002-05-22
    body: "1) its his fun project, so he will continue even if you continue to give such dumb comments.\n\n2) if any developer will stop coding, because his code does not perform the way he wishes at the beginning, we wouldnt have any software at all. (anybody say mozilla :) )\n\n3) i like it. particular in combination with remote x-sessions where no full blown x/window manager is installed.\n\n4) if it runs, it runs.. i usually start my pc once and use it all day, so the 6 extra minutes for startup are marginal."
    author: "cylab"
  - subject: "Re: Performance"
    date: 2002-05-22
    body: "> 4) if it runs, it runs.. i usually start my pc once and use it all day, so the 6 extra minutes for startup are marginal.\n\n... and this works also with laptop standby modes (expect combined with screen resolution changes on xfree currently), so you have to start only once a week or less.\n\nBTW: Why does windows need so much time to start if many applications are installed ? \nAnswer: In the boot process they preload all the needed dll's, so that application starting time is usable. If this is similar done for kde, than the loading time is like the value for second starting. \n\n\nRegards \nRalf \nEnjoy kde-cygwin \n"
    author: "Ralf Habacker"
  - subject: "DCOPServer could not be started"
    date: 2002-05-30
    body: "Is anyone else having a problem starting the DCOP Server?\n\nWhile the KDE Splash Screen is flashing the, \"Setting up interprocess communication\" icon, I look at the Cygwin bash window that I type \"startx\" from, and see:\n\nkdeinit: Launched DCOPServer, pid = 562 result = 0\n_KDE_IceTransmkdir: Owner of /tmp/.ICE-unix should be set to root\niceauth:  creating new authority file /cygdrive/c/.ICEauthority\nDCOPClient::attachInternal. Attach failed Could not open network socket\nDCOPClient::attachInternal. Attach failed Could not open network socket\nDCOPClient::attachInternal. Attach failed Could not open network socket\nDCOPClient::attachInternal. Attach failed Could not open network socket\nDCOPServer self-test failed.\nkdeinit: DCOPServer could not be started, aborting.\nkdeinit: launch -> dcopserver --nosid\n\nThis repeats 2 more times, then the X window shows a dialog that says, \"There was an error setting up inter-process communications for KDE.  The message returned by the system was:  Could not read network connection list.  /cygwin/c/.DCOPserver_<machinename>_-0.  Please check that the \"dcopserver\" program is running!\".  Click on OK, and the X window shuts down.\n\nI can start X with another window manager, but I can't run most KDE applications (Konsole fights with this problem, but then starts up, anyway).\n\nThanks for any help,\n- Andy\n"
    author: "Andy M"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-06-05
    body: "See http://cygwin.kde.org for howtos and mailing lists relating to the kde-cygwin project. I think this isn't the right place to ask for installation support.\n"
    author: "Ralf Habacker"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-07-31
    body: "Ralf, this issue is widespread (appearently anyway) and is *not* addressed on the website that you list, nor on http://www.cygwin.com - Why NOT ask here? "
    author: "Hunter Oaks"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-08-01
    body: "There are several thread in the kde-cygwin mailing list relating to this topic  for example see http://lists.kde.org/?l=kde-cygwin&w=2&r=1&s=dcopserver&q=b\n\nRalf \n"
    author: "Ralf Habacker"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-07-06
    body: "Did you ever find a solution to this? I noticed the QA deal, but that didn't seem to help me..."
    author: "mike"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-07-09
    body: "Mike/Andy - I fixed this by remounting root as binary (using the -b option). mount -m will list the mounts...\n\nCheers..."
    author: "Doug E"
  - subject: "ARE YOU CRAZY!"
    date: 2002-07-12
    body: "All your doing is giving windows users no reson to switch to Linux! FOols"
    author: "Alex"
  - subject: "Re: ARE YOU CRAZY!"
    date: 2002-08-01
    body: "Why, \ndo you not know that some company have IT rules, which defines the type os operation system ? \n\nI have a real example of a german company with 150.000 employees, in which desktop system have to be windows. If I would tell them to use linux, the will ask me \"What about the costs for re-educating all the employees\" and \"What about all our running applications, could we use them under linux ?\"  And today the linux desktop isn't ready to migrate immediatly. What about all the administrators, who have knowledge only about windows ? They need time to learn linux, so my strategy is to adopt linux as server platform, so that the administrators have time to learn, how linux works and second to provide linux applications (through cygwin and kde) onto the windows desktop, so that the user s can learn how it feels and how it works. If the admin are ready to admin linux and the users feel comfortable with linux application, is seems easy to pull away the underlaying os, isn't it ? \n\nOf course, there may be brute force way to bring linux to the users, but I feel more comfortable with this described way, additional because, as I know, companies does not change it's desktop structure very quickly. \n\nRegards \nRalf \n\n"
    author: "Ralf Habacker"
  - subject: "Re: ARE YOU CRAZY!"
    date: 2004-11-30
    body: "I completely agree with Ralf. I will describe the environment where I work:\nHuge company (50.000 employees). I work in a R&D unit where we develop telecomms software. Although our sw runs in a 'sort of' UNIX environment, we are forced to use windows in our desktops. The only solution if you want to have your trusted command-line tools? Cygwin.\nFurthermore, I have discovered that running X on top of Cygwin beats much more expensive X-windows clients. My company licenses one called 'ViewNow', which is terrible.\nWould I be happier with a modern Linux distro? No doubt.\nIs it realistic to expect it too happen? Not until Microsoft Project and many other tools run there.\n\nI am impressed by the great job done with Cygwin. Thanks a lot!\nRegards,\n/atomico\n\n"
    author: "atomico"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-09-28
    body: "It's OK for me!!! (Win95)"
    author: "Sandro"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-07-31
    body: "I was getting the similer problem and was not able to login in KDE\n now please tell me what is dcopserver is all about and why such a\nproblem occurs.\n\nthanks\nsaurabh"
    author: "saurabh naik"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-08-01
    body: "dcopserver is a server for the Desktop communication protocol, the common interprocess communication of kde and uses unix domain sockets. unix domain sockets stores established sockets in files, which are read by clients. \nThe problem is that cygwin supports a cr to cr/lf conversation mode for files (called \"textmode\"), which let dcopserver fails to read this socket file. \n\nHope that helps. \n\nRalf \n\n"
    author: "Ralf Habacker"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-08-04
    body: "Did anyone find a fix for this error, I'm getting it too."
    author: "Sam"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-08-05
    body: "See http://kde-cygwin.sourceforge.net/faq/kde2.php#D3.1\n\nRalf \n"
    author: "ralf habacker"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-09-11
    body: "I was having this same problem, but only when I logged in as a non-root user.  I was not able to find anything that helped at http://kde-cygwin.sourceforge.net.  After messing with it for quite a while on my Solaris 8 box I got it to work by chmod 01777 on /tmp/.ICE-unix, /tmp/.X11-pipe, and /tmp/.X11-unix.  As best as I can tell, this gives the non-root user enough rights to create the files unix domain sockets stores established sockets in for Solaris.  The problem is that these permissions get reset on boot so I had to create a small script to fix this.  Hope this helps. "
    author: "Mike"
  - subject: "Re: DCOPServer could not be started"
    date: 2005-04-02
    body: "I am having exactly same problem as you for my Solaris 10. The kde version is 3.3.1a that was install from my Solaris 10 companion CD. Could you please tell me your script-:)\n\ncheers\n \n\n"
    author: "vincent"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-11-22
    body: "hi,\n\nI recently installed 2 Debian testing machines and encountered the same problem. Seems like the permissions of the directory \"/usr/X11R6/bin/\" are screwed up.\n\nThis helped me:\n\nchmod go+rx /usr/X11R6/bin/\n\n\nhope this helps."
    author: "Nils Boysen"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-04-21
    body: "I am using slackware 10.2 and this seemed to work.  I tried some other methods posted here saying to remove various files but that led to more trouble.  Thanks for the help."
    author: "Andy Ritchey"
  - subject: "Re: DCOPServer could not be started"
    date: 2005-01-10
    body: "i am also facing same prob. in linux 7.2 and linux 0.9 if you have any solution please give me reply "
    author: "tejas upadhyay"
  - subject: "Re: DCOPServer could not be started"
    date: 2005-03-18
    body: "(18 mar 2005) Sorry, an old thread but a new problem.\n\nI had this problem too.  (Red Hat Linux 9.0, fully updated to final releases)\n\nIt began about two weeks ago.  The only effort I made to correct it was to start dcopserver manually at last login.  Dcopserver ran fine so I guessed at startup script/config probs. Since then I haven't had the problem, so I can't comment further."
    author: "Graham Matthews"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-08-30
    body: "I use kde that is installed with RedHat 7.3 and I had the same problem.  IT was because my path was screwed up.  I needed to add /usr/X11R6/bin."
    author: "Jared"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-09-02
    body: "I had the same problem but I came across a very simple solution.  Just run the command \"enter (yourhostname)\" where yourhostname is your computer's name, the one you put in the /etc/hostname."
    author: "lyhong"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-06-04
    body: "There's no the command \"enter\" at all!\nThat doesn't work to me!"
    author: "mavis"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-09-12
    body: "One of my users had the same problem after he tried to append to his PATH in .cshrc (his default shell). Apparently, he overwrote the default path rather then appending to it. When the user login, DCOPserver is run which then runs `iceauth' using the user's PATH. The path to `iceauth' is /usr/X11R6/bin, which was not in the user's PATH statement, hence the DCOPserver error. After he corrected his PATH statement, it fixed the DCOPserver issue."
    author: "Ken"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-04-21
    body: "Thx you are true ;)"
    author: "Shark"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-03-20
    body: "Another fix to this problem, at least in my situation, was my home directory did not have group and global write access. Changing it over to 777 fixed the DCOPserver problem along with a slew of other problems"
    author: "Misery's Comfort"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-01-30
    body: "Hy i discovered this thread in an effort to get my kanotix_2005_04 working. i've just installed it on my machine and DCOPserver keeps throwing kde inter-process communication failure on root and on my user. I am able to login but under a second WM provided IceWM(i guess) but this error pops up every time i press smth related to kde (e.g: running kde applications). Where is the problem and how can I solve it -i've hured it might be related to ICEauthority but i am new to linux and don't know anything about cli or commands or getting to that dir from cli etc. Please can u give me an answer and eventually write down some steps to accomplish this.Thank you,\nFord."
    author: "Ford"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-11-23
    body: "My solution to the problem was found on another post... it wasnt permissions or the chsrc file... I had to reinstall kdelibs (I just installed all available from my repositories) this cleared it right up... hope this helps someone!"
    author: "tek"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-10-10
    body: "I've got the same problem with my SuSE Linux 7.3:\nStarting Linux and X-Server will show me the graphical  login-screen. I switch to Textmode [Ctrl-Alt-F3], login as  root and start the text-based setup-tool YAST.\nI searched for installed packages with \"dcopserver\", deinstall  & reinstall it and shutdown the computer.\nAfter starting again, everything works fine (until the next  time error ??). I can't explain it, because I'm Linux-newbie.  May be, specialists will do it for me."
    author: "Dremus"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-10-28
    body: "One of my colleagues had this problem.\nIt appeared that she was out of disk space (due to quota limitations).\nWhen logging in, the system could not make a .DCOPserver_hostname:0 file under her account that contains data about the sockets KDE uses.\nStarting KDE, the system starts looking for this .DCOPserver_hostname:0 file, which is not there (of course).\nThis generates the error.\n\nRaising the quota, or deleting some files is a solution here."
    author: "hansa"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-11-07
    body: "That worked for me.  Thanks.  I am using Bastille and that turned on quotas."
    author: "Rusty"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-06-04
    body: "But that don't worked for me!\nHelp me!"
    author: "mavis"
  - subject: "Re: DCOPServer could not be started"
    date: 2002-12-04
    body: "I don't know if you found anything out as of yet, but I had the same issue and resolved it by deleting both the /tmp/.ICE_unix directory and the ~/.DCOPserver_<machinename>_-0 link.  Logged out and it worked fine.  Trying to find out if KDE's bugzilla has a report on this as of yet\n\n"
    author: "Musa Williams"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-01-20
    body: "On my computer the problem occured because of a crash of the whole system.\n\nremoving /tmp/.ICE_unix directory and the ~/.DCOPserver_<machinename>_-0 worked for me.\n\nthanks!"
    author: "Niko"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-04-08
    body: "removing /tmp/.ICE_unix directory and the ~/.DCOPserver* also worked for me.\n\n"
    author: "Tan Hong Cheong"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-08-15
    body: "This worked for me, cheerz!"
    author: "Kevin C."
  - subject: "Re: DCOPServer could not be started"
    date: 2003-10-08
    body: "on my machine (mandrake 9.1) the problem was that it was configured to get a hostname (which looked like this: 1,6,<mac-address of my nic>) from the dhcp server of my isp, which dcopserver seems quite to dislike.\n\nthe solution was to enter a hostname in /etc/hostname and to prevent dhcp from resetting it. to achieve this you just have to open /etc/sysconfig/network-scripts/ifcfg-eth1 (in my case eth1 is the nic connected to my cable modem) and change the line \"NEEDHOSTNAME=yes\" to \"NEEDHOSTNAME=no\"\n\nsilence"
    author: "silence"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-01-13
    body: "Did you manage to get this sorted out? I am having the exact same problem...any help?\n\nThanks,\nKartik"
    author: "Kartik"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-01-30
    body: "It's most probably a permission problem. I solved it by deleting ALL the invisible .gnome* and .kde*  direcories. Reconfiguring the Desktops was faster than fiddling around with dozens of files...\n\n"
    author: "Nmuller"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-02-04
    body: "One of my users recently switched to a tcsh default shell and the default .cshrc that comes with RH 7.3 does not include the /usr/X11R6/bin path in the $PATH env variable. I had to add to their path. \n\nNote: the deafult .cshrc that comes with RH 7.3 overwrites the $PATH env variable, It was very frustrating to figure this one out. especially since I checked the default rc's and .logins to make sure the paths were there.\n\n"
    author: "Jav"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-03-04
    body: "Non-root users could not log in to the RedHat 7.3 console with KDE.\n\nSOLUTION: Modify /usr/bin/startkde.   Basically, /usr/bin/startkde\ncould not find certain applications which exist in /usr/bin/X11.\nSo, add it to the path in the script.\n\nPut these lines in to /usr/bin/startkde:\n\n# Modification\n#\nPATH=/usr/bin/X11:$PATH\nexport PATH\n#\n"
    author: "Jeff"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-01-08
    body: "This worked for me too.\n\nI have a RedHat 7 system that has been running for almost 2 years, and suddenly on reboot, it throws this error.\n\nadding the PATH statement solved the problem:\n\nPut these lines in to /usr/bin/startkde:\n\n# Modification\n#\nPATH=/usr/bin/X11:$PATH\nexport PATH\n#\n\nThank you for the assistance!\n\n\n"
    author: "Kevin Ready"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-03-05
    body: "Non-root users could not log in to the RedHat 7.3 console with KDE.\n\nSOLUTION: Modify /usr/bin/startkde.   Basically, /usr/bin/startkde\ncould not find certain applications which exist in /usr/bin/X11.\nSo, add it to the path in the script.\n\nPut these lines in to /usr/bin/startkde:\n\n# Modification\n#\nPATH=/usr/bin/X11:$PATH\nexport PATH\n#\n"
    author: "Jeff"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-06-05
    body: "I am facing the same problem! \nBut all above mentions do not work for me at all!\nNot can I do anything without KDE!\nPlease - any suggestions or pointers will be greatly appreciated. \n\nRegards\n  Mavis "
    author: "mavis"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-06-25
    body: "Adding the path for Redhat 7.3 fixed my problem, too."
    author: "Brian"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-04-02
    body: "You must set the trust permision to the .ICE-unix  directory under the /tmp.\n\n\n"
    author: "Muammer"
  - subject: "Re: DCOPServer could not be started/ LOGIN FAILURE"
    date: 2003-05-16
    body: "Hello all,\n\nI am facing (it seems) the mother of all the problems described in this thread. I am running KDE using RedHat 7.3 on Linux. I logged in just fine to KDE, but tried to use KUser to add a new user - this gave me an error message saying that \"error in /my/path/dcopserver_<machinename>_0 make sure dcopserver is running\". \n\nNot sure what to do, I went ahead and added the user alright and tried to log out of X to see if I could reboot and figure something out. Big mistake - I have not been able to log in _AT ALL_ ever since. I have tried both Runlevel 5 and 3 and _no_user_ can log in, including root (me). I am currently reduced to Runlevel 1 and have tried bizarre things to no avail. First, deleteing .ICE-UNIX or .Xauthority did not do anything for me. I also deleted some files to free up space. Finally, I managed to get dcopserver running on Runlevel 1 but of course as soon as I say \"telinit 3\", it seems to wipe it out. If I reboot, it automatically shuts down dcopserver. Is there anything I can do to avoid this problem? I can possibly write a script in /etc/rc.d/rc3d that tries to start up dcopserver (and switch at boot to runlevel 3) but I wonder if that would do any good. \n\nPlease - any suggestions or pointers will be greatly appreciated. \n\nRegards\nSAUMIL MEHTA"
    author: "Saumil Mehta"
  - subject: "Re: DCOPServer could not be started/ LOGIN FAILURE"
    date: 2004-11-12
    body: "I recently ran into a similar problem: a message box informed me that I should make sure that the \"dcopserver\" is running and then the desktop login failed.\n\nThe cause of this in my situation was that the owner of the file .ICEauthority was set to root:root. How did it get set to that? Couldn't figure it out (maybe I ran something as root in my home dir).\n\nThe fix is simple. Login as root and issue\nchown myuser:myuser .ICEauthority\n(where myuser is, of course, your user name :) )\n\nCheers,\nOvidiu"
    author: "Ovidiu"
  - subject: "Re: DCOPServer could not be started/ LOGIN FAILURE"
    date: 2005-11-19
    body: "Hi I am facing similiar problems. I have three logins two users a1 and a2 and a root. With  login a1 I am able login, but with other login a2 I am facing similiar problem. I tryied to change the owner ship on .ICEauthority file. now the  problem is if I try to login as root it overwriting .ICEauthority filr for login a2, because of that root login is also  failing. Any suggestion to avoid this issue.\n\n-- john."
    author: "john"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-03-02
    body: "Yes! We had the same problem with DCOP on a\nbunch of Sun Rays, which have a bunch of users\non one server. The first user to fire up KDE\nends up creating /tmp/.ICE-unix/ and of course,\nthey own it. The next user is unable to write\nto the directory! When this is fixed the next\nuser can run KDE. I guess that the directory\nshould be owned by root with world write?\n\nGeoff Gibbs"
    author: "Geoff Gibbs"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-03-02
    body: "Please see http://wiki.kdenews.org/tiki-index.php?page=Performance+Tips and search for ICE-unix."
    author: "anonymous"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-09-01
    body: "I had the same trouble last week and tried the solutions listed here. None appeared to work.\n\nI could run kmail and other kde apps as root but not as the (only) existing end user. In the end I used \"chown\" and chowned all the files in the users home directory. \n\nE.G. \"cd /home/jack ;chown -R jack.jack .\"\n\nthat worked.....why i dont know but I didnt have the time to delve in to the detail I just needed it functioning.\n\nregards\nPhhil"
    author: "Phil Vossler"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-09-12
    body: "Same problem, different solution...\n\nA nasty install script clobbered my /etc/ld.so.conf file and it was missing critical ld library paths (such as /usr/X11/lib). Here's what mine contains:\n\nsh-2.05a$ cat /etc/ld.so.conf\n/usr/local/lib\n/usr/X11R6/lib\n/usr/i386-slackware-linux/lib\n/opt/kde/lib\n/usr/lib/qt/lib\n\nHope that helps anyone that remains stuck.\n\ndt\n"
    author: "dt"
  - subject: "Re: DCOPServer could not be started"
    date: 2005-09-29
    body: "Thanks to Phhil's.\nWorked out fine.\n\nchown -R uname:uname . \n\nBye.\nwathavy"
    author: "wathavy"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-11-27
    body: "Problem here is slightly different in that root and users (with accounts) other than me have no problem.  Trying to log in as myself I run into the same DCOPserver problem though it seems.  Problem started for no apparent reason; just simply was there one morning at startup.\n\nI have tried removing myself as a user (500), then creating a new myself (502) did not help.  Also removing the /tmp/.icw* directory mentioned above did not help.  There is no .DCOP* file or link to be found in the affected home directory.\n\nI have even tried removing the entire KDE, then reinstalling KDE but whatever the problem seems to be tied to files not affected by removing/reinstalling.\n\nSystem(s) here use SuSE 8.2 on, the one in question is SMP with plent of gigs to spare on HD."
    author: "Jim Forney"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-12-09
    body: "Hi,\n\nI also use a SuSE 8.2, so your situation should be faily similar to mine. With me the same problem occured while I was experimenting with network settings, so I guessed it was due to that, but I'm not sure.\n\nAnyway, I logged in as root and just did a SuSEconfig after I read that some people mentioned their settings were screwed up. The script told me that I had modified my kderc (somewhere in /etc/opt...; it tolfd me exactly where it was) and that it had generated a fresh one. So I copied that onto the old one and the problem was gone.\n\nHope this also solves it for you,\nDiederick."
    author: "Diederick de Vries"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-07-31
    body: "Hi all,\n   I am running SUSE 10.1 official version and i had the same problem when i tried to use konqueror to access a network folder. I was running konqueror as the only user that exist other than root.\n\nFIX :\n   The fix for me to as root chown the users home folder.\n        chown -R XXX /home/XXX   where XXX is the username\n\n   After that all worked fine.\n   I have found that few earlier weired problem like this also was due to some file in the users home folder with only root permission. I would be happy if someone can tell, whether there are any log files that we can look for the error messages like these in linux.\n  I mean like a log file which would have suggested that a file having only root permission failed to load the DCOPServer loading,\n\nNiroshan Hewakoparage"
    author: "niroshan"
  - subject: "Re: DCOPServer could not be started"
    date: 2003-12-19
    body: "I had a similar problem doing a Debian Sarge install (using the Debian Next-Gen installer). The problem was permissions on /usr/X11R6/bin, which contains the program iceauth, which (I think) dcopserver needs to start. As installed, the directory was owned by root:root, and permissions were set to drwx------. \n\nI used CTRL+ALT+F1 to get a text terminal. Logged as root, and then `cd /usr/X11R6` and `chmod go+rx bin`. Permissions are then drwxr-xr-x.\n\nI switched back to the X Server with ALT+F7, and login worked fine.\n\nHope this helps."
    author: "UltraOne"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-02-15
    body: "I had the same problem with dcopserver but didn't find the solution here.\nWhen fixing something under gnome I fixed dcopserver.  I am running gentoo\nand when I installed it I didn't set up /etc/hosts correctly.  That made\nan anoying message pop up in gnome and caused kde to crash.  By adding\n127.0.0.1 <mymachinename> localhost\nall was well.  Hope this helps someone out there."
    author: "alexander"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-03-25
    body: "i am having the same problem.  i got around it by creating a new user account and logging in as that user."
    author: "bob wachunas"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-05-02
    body: "I am having the same problem with KDE 3.2 after some apt-get upgrade (debian/testing). I removed the ICE/auth files as someone here suggested - but kde-init --exit keeps giving me these errors:\n\n/usr/X11R6/bin/iceauth:  timeout in locking authority file /home/knoppix/.ICEauthority\nDCOPClient::attachInternal. Attach failed Authentication Rejected, reason : None of the authentication protocols specified are supported and host-based authentication failed\nICE Connection rejected!\n\nDCOPClient::attachInternal. Attach failed Authentication Rejected, reason : None of the authentication protocols specified are supported and host-based authentication failed\nDCOPServer self-test failed.\nICE Connection rejected!\n\niceauth:  timeout in locking authority file /home/knoppix/.ICEauthority\nkdeinit: DCOPServer could not be started, aborting.\n\n___\nabove actions where done while being logged in to kde as root. trying to login as user 'knoppix' created these 3 lines in auth.log:\n\nMay  2 17:11:46 AMD1800 kdm: :0[5141]: (pam_unix) session closed for user knoppix\nMay  2 17:16:01 AMD1800 CRON[5873]: (pam_unix) session opened for user knoppix by (uid=0)\nMay  2 17:19:41 AMD1800 kdm: :0[6925]: (pam_securetty) access denied: tty ':0' is not secure !\n\n\nMaybe this gives a hint...\nThanks for any help\nRaga"
    author: "Raga"
  - subject: "Solved!"
    date: 2004-05-02
    body: "Well, don't ask me why, but this is how I solved the bug:\n\n1) create new user (root console: adduser <username>)\n2) successfully login as new user with kde3.2\n3) open root console\n4) rm /home/knoppix/.ICE* and .DCOP*\n5) cp /home/<new user>/.ICE* /home/knoppix/ \n   same with .DCOP (only the file, not the link)\n6) chown knoppix.knoppix /home/knoppix/.DCOP* \n   (same with .ICE*)\n\nInstead of '*' I typed the <TAB> key until match.\n\nI hope this helps somebody after me - and that the causing error will be detected soon. Before this I had tried to change the access rights to these files (no success) and to delete them (no successful recreation)."
    author: "Raga"
  - subject: "Re: Solved!"
    date: 2008-08-06
    body: "This one worked for me in KUbuntu 8.04.1\nProblem started upon reinstallation of Xsane followed by reboot.\nDon't forget to delete the dumb user just created afterwards!"
    author: "Alfonso fr"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-05-14
    body: "Maybe its not a direct reply but helpful in someways. After launching a kde session from kdm I get an error that the dcopserver could not be started. I am running the current debian/sarge. It seems that dcopserver is run as the same user login in. For locking purposes I believe it quite often occurs that application want to write to /tmp. So a user rights check gives me\n\ndrwxr-xr-x    6 root     root          192 May 14 20:45 tmp\n\nOkay so I add the sticky bit to /tmp via\n\n# chmod u+t /tmp\n\nWhich gives me no solution. So I decide to change the rights to\n\n# chmod 1777 /tmp\n\nWhich leaves me with\n\ndrwxrwxrwt    6 root     root          192 May 14 20:45 tmp\n\nAnd *whoa*: my login user can start dcopserver manually. Okay: Some people might say that I am digging a security whole and so on, but I do not want every user to write to their own ~/tmp directory but rather that my system gets back to work. Supposedly that package causing the trouble will be fixed one day *g. Hope that was of help\n\nSebastian"
    author: "Sebastian"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-05-17
    body: "I am getting the same error message in addition I am also getting \"kdeinit communication error.\n"
    author: "Angel Sosa"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-06-02
    body: "Got the same problem.  I am running Fedora Core I with multiple computers accessing home directory at the same time.  I removed ~/.ICEauthority-c and\n~/.ICEauthority-l then it works fine."
    author: "Yi-Kun Yang"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-08-10
    body: "I run KDE on fedoracore1 as well I am the only one logging on to my computer. If you remove ICEauth*c and ICEauth*l, it seems to fix the problem. This is a bug of some sort. But removing those two files always seems to fix my problem. I will see if a bug report has already been filed.\n\nBlackula\nFedora Core 1"
    author: "blackula"
  - subject: "THIS FIXES YOUR PROBLEM ON FEDORA CORE 1"
    date: 2004-08-10
    body: "This seems to fix my problem on fedora core 1, hope it fixes yours.\n\nBlackula\nFedora Core 1"
    author: "blackula"
  - subject: "Re: THIS FIXES YOUR PROBLEM ON Mandrake 10"
    date: 2005-07-09
    body: "I got the same problem and just renamed the ~/.ICEauthority-c and\n~/.ICEauthority-l to ICEauthority-c and ICEauthority-l everything worked out from there.\n\nthanks!"
    author: "Joaquin Jose Escay"
  - subject: "Re: DCOPServer could not be started"
    date: 2004-06-24
    body: "I got the same message when starting konsole.\nI use Gnome as the desktop manager, but I do use konsole and konqueror because I like them.\n\nI got the message about the DCOPServer not running when I started konsole.\nSo I chmod'ed ~/.ICEauthority to a+rw and this way it works like a charm.\nICEauthority was for some reason owned by root.root.\n\n\nhth,\nOli"
    author: "Oli"
  - subject: "Re: DCOPServer could not be started"
    date: 2005-03-26
    body: "I installed cygwin with Kde3 on my note. \nFirst time it worked, but After several setting, startx repeted followings..\n\n_KDE_IceTransmkdir: Owner of /tmp/.ICE-unix should be set to root\niceauth:  creating new authority file /home/?????/.ICEauthority\nCould not load library! Trying exec....\nDCOPServer self-test failed.\nkdeinit: Launched DCOPServer, pid = 3736 result = 0\nkdeinit: DCOPServer could not be started, aborting.\n+ test -n ''\n+ kwrapper ksmserver\nWarning: connect() failed: : Connection refused\nkdeinit: entering main\nkdeinit: Warning, socket_file already exists!\nKDEINIT_KIO_EXEC\n\nI found that dcopserver couldn't find my home directory, \ncause of my userid is korean using env LANG=ko_KR.EUC.\nso i changed LANG to C.\n\nNow it work fine..\n\n"
    author: "zoline"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-04-11
    body: "hi, I have encountered the similar problem when try to login KDE.\nBut have to clarify the point before explain the phenomenon,\nthat is: I have set the default shell as \"tcsh\" for my users and root.\ndefault shell in REDHAT-7.3 is \"bash\". \nin order to start kde, the only way for me is setting default shell back.\nthen problem is gone.\nactually, I am used to work with tcsh,not familiar with bash.\nFor the more depth reason, should be solved by linux-developer.\nhope help."
    author: "temp-join"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-06-25
    body: "I think this happens when you are logged into linux as an ordinary, su to root and then use /sbin/shutdown instead of logging out in the conventional way (at least that's what has happened to me.\n\nIn the past, as root, I have solved this problem by creating a symbolic link thus:\n\nln ~/.DCOPserver_localhost.localdomain__0 /home/alex/.DCOPserver_localhost.localdomain__0\n\nThis link will be readable by ordinary users by default.\n\nI'm not sure this is strictly speaking the proper solution but in the past it has worked. I came here to see if I could find anything better but I don't like the idea of messing around with those files, as mentioned above, so I will now try this again.\n"
    author: "alex cox"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-06-25
    body: "Yes, it worked for me fine."
    author: "alex cox"
  - subject: "Re: DCOPServer could not be started"
    date: 2006-11-14
    body: "may be a bit late but I fixed it this way after trying many other suggestions here.\n\nIf you delete ~/.ICEauthority file(make a back up just in case) and restart your session.  This did it for me.  Not sure what the problem was though."
    author: "Darren"
  - subject: "Re: DCOPServer could not be started"
    date: 2007-01-06
    body: "please, REMOVE the f#$%&/ spaces from your HOSTNAME=\"DONOTUSESPACESHERE\"\n\nit works.\n\ncheers,\n~your friendly nearby guru\n\np.s. paranoid is a noob =)"
    author: "YOUR FRIEND GURU"
  - subject: "Re: DCOPServer could not be started"
    date: 2007-01-15
    body: "My situation was that I could start KDE as root, but not as any other user. Having read this thread there seemed to be a common theme; this being the permissions of certain files and the ability to read/write from/to the user's home directory.\n\nIn order to facilitate sharing between my windows and linux partitions I had formatted my home partition as FAT32 so I could read in both. However, it does not  seem possible (certainly not easy) to set permissions on a FAT32 drive. So even though I mounted my user as both the owner and group for the drive (in fstab) and had given everyone full rwx permissions on the entire drive, KDE still had problems accessing it in the way it needed. I reformatted the drive as EXT3, created a new user and KDE launched without a problem as a normal user (this makes sense because root's home was on my root partition which is EXT3). In summary:\n\n1) PROBLEM: As a normal user with a home directory on a FAT32 drive (this follows for any file system where you cannot set permissions I suppose) I was getting a DCOPServer error when booting KDE and KDE and X would crash to the command line (however X itself would start).\n\n2) SOLUTION: I reformatted the partion where I had mounted my users' home directory as EXT3, deleted the old users and recreated them (with their home's on the EXT3 drive) and KDE started without a problem."
    author: "Brian"
  - subject: "Re: DCOPServer could not be started"
    date: 2007-01-20
    body: "This problem has been plaguing me for a while now, but I finally squelched it.\n\nFIRST let me clarify how I was seeing these same errors:\nI would connect to a server using VNC as a normal user.   From there I would su  as root and start a Konsole.  After this, I would begin to see these errors everytime I attempted to start a Konqeror, or just about any other command that I typed into the Konsole.  I basically could get anywhere from there even though I was root.\n\nSOLUTION:  As root, I could, however, remove ~/.DCOP*, then I closed out my  Konsole session and simply performed another 'su' to start another Konsole.  Everything has worked fine ever since.  Somewhere along the way, these ~/.DCOP* files had gotten corrupted after a server crash, I think, and couldn't be corrected, until I completely removed them.  I hope this helps!\n\nKen Banks"
    author: "Ken Banks"
  - subject: "Re: DCOPServer could not be started"
    date: 2007-01-29
    body: "The solution is to change the hostname in the next FILES:\n/etc/hostname\n/etc/hosts\n/etc/mailname\n/etc/init.d/knoppix-autoconfig\n\nThe hostname can't contain special symbols, like space,...."
    author: "pingu"
  - subject: "Re: DCOPServer could not be started"
    date: 2007-06-04
    body: "try adding this line on top of $HOME/.vnc/xstartup\n\"\"\"\nICEAUTHORITY=\"$HOME/.ICEauthority\"\n\"\"\""
    author: "Ali Ayoub"
  - subject: "Re: DCOPServer could not be started"
    date: 2008-01-10
    body: "i just ran into this using kde on aix.\n\ni finally tracked MY problem down to laziness on my part.\ni am using dhcp on this install, and after i successfully\ngot an ip address, i added it to /etc/hosts with my machine's\nname (this machine will pretty much be up all the time and\ni don't expect the ip address to change).\n\nso i added:\nx.x.x.x myname\n\nbut i really needed to add a fully qualified name as well,\nso i changed the line to:\nx.x.x.x myname myname.fully.qualified.com\n\nthat fixed it."
    author: "jim"
  - subject: "DCOPServer could not be started"
    date: 2009-01-19
    body: "my computer system open its windows but no applications because of the DCOPServer is not running. Thank you\n"
    author: "roilan panaligan"
  - subject: "Re: complainers"
    date: 2002-08-03
    body: "This is addressed to those who are complaining that KDE/Cygwin is preventing 'Windows users' from 'converting' to *NIX:  Do you really think that the people trying Cygwin are 'Windows users', or folks who use *NIX whenever possible, but are constrained to a Windows box at work, etc.?  The latter, I think, for the most part.  Also, what do you think the rate is at which people are 'converting to *NIX?'  Probably not very high.  In fact, mr./mrs. *NIX user, I would bet you have a Windows box available most of the time, don't you now?  It's just not cut-and-dried.  I was introduced to PCs about 2 years ago, and placed in front of a WinNT box.  Not everyone's dream, right?  A couple of months later, we upgraded to Win2k, and then I decided to check out *NIX.  I got a copy of FreeBSDs latest release, and dove in.  I now use *NIX and Windows at work and at home.  Anyway, long story short, while I prefer FreeBSD to the Microsoft OSs I've used, I would bet my story is alot more 'common' than you might think.  People use what gets the job done.  No one strictly buys Ford or Chevy [and nothing else] anymore.  \n\nKDE Cygwin is a 'good idea.'\n\n\nThanks,\n\nJoshua Lokken"
    author: "Joshua Lokken"
  - subject: "Re: complainers"
    date: 2004-09-08
    body: "You're right -- it's not just that black and white, sometimes you have to use windows, there is no way around it -- but for productiveness I choose (and like) *nix.\n\nThanks you all folks creating a nicer Windows :)"
    author: "Simon Heinzle"
  - subject: "Re: complainers"
    date: 2005-10-28
    body: "Excellent points, it's about time someone said it. I've been getting *really* annoyed by all the people who think ports of popular Free software are somehow hurting the use of Free OSes.  The logic I've heard goes like this:\n\n* User can get all the Free tools they want on Windows\n* User figures \"I have no compelling reason to switch to $FREE_OS\"\n\nIt's crap. I think it goes more like this:\n\n* User thinks $FREE_OS isn't worth it\n* User finds lots of Free tools that work very well on Windows, but are ports of tools created on $FREE_OS\n* User starts to think $FREE_OS might be very good, since they had all these tools a lot sooner.\n\nI've seen that first-hand.  My father-in-law is a Windows user.  Frustrated with IE, he tried Firefox and loved it.  As he's an astronomy buff, I pointed him at Celestia, which he enjoys immensely.  Audacity helped him with his LP -> CD project.  OpenOffice saved him $400 when he needed to generate documents at home for consumtion at work.\n\nSuddenly, he realized that *every computing need he has* is being filled by OpenSource software.  Why, then, was he paying for Windows?  He's now in the process of converting entirely to Linux.  I doubt he would have made the effort to change everything about his work habits at once: and doing things like porting KDE to Windows gives people the chance to \"fall back\" easily when the learning curve slows their work.\n\nKudos on this project, and never let anyone tell you this is harming the Free OS community: those people are clueless about how people make decisions."
    author: "Radiantmatrix"
  - subject: "Re: complainers"
    date: 2006-04-24
    body: "I initially got into Linux after having headaches working with a Windows 2k server. As a server Linux works like a charm, however I still need Windows on my work PC because of some custom Windows apps which my company use. Cygwin lets me have a little bit of Linux on my machine where otherwise it would had only been Windows\n"
    author: "Kumar Kanagarajah"
  - subject: "Re: complainers"
    date: 2008-12-21
    body: "Most of you guys here are saying that the only reason you need windows is that a few select apps only work in MS Windows.  Have you guys heard of wine? It's practically like Cygwin in reverse, by that I mean it allows you to install windows apps on *Nix.  I was really impressed by it and thought I should share my experiences.  I think it would be funny if someone managed to get the windows desktop interface and ported it to *nix. <a href=\"http://www.winehq.org\">Wine Homepage</a>\n\n\n134xt3r."
    author: "134xt3r"
  - subject: "KDE/QT on Microsoft Windows"
    date: 2002-08-09
    body: "I am very new to the Open Source arena, however, I know a good thing when I see it. My honest feeling is that all the bashers on porting KDE to Windows are not thinking clearly. They just have some unresolved issues and hatred toward Microsoft. The truth be told, the more Unix/Linux like OS/Applications ported to Microsoft Windows proves its power and flexibility, though somewhat still in its infancy. \n \nKDE on Cygwin will be very rewarding in the years to come. Free Software, I believe, is slowly gaining wide acceptance in areas where Microsoft compatible applications are the standard. How wide spread is this? I can't say. However, KDE on Cygwin has/will open many opportunities for KDE application software developed under the Free Software umbrella. As more Free Software applications become more stable, you can be sure Microsoft applications will still exist and used by many, but will slowly be overlooked more and more when the price of productivity applications is the driving force. \n \nKDE on Cygwin will/is the portal by which Free Software will become common place. Microsoft's (and its employees) only advantage is it had a head start on the Free Software community by many years, however, the Free Software advantage will be that of free software supporters from around the globe.\n\n=>TTFN<="
    author: "Marty Chapman"
  - subject: " Good Idea, but a bit premature"
    date: 2002-12-23
    body: "It's a good idea to have GNU/Linux (since most of the tools are gnu) tools available for linux. They offer a few more options in the face of a closed, high-priced market to those of us on tight budget. To that end, I can definitely understand and appreciate the intentions. My one concern about this is that this maneuver may be bit premature. I can't help but wonder how the good but sometimes buggy apps that I love to use in linux will react with Windows when they crash. Think about it. An application that's 70% running on a developing environment that's still very much under construction, all on top of an operating system that at its most stable still acts as though it's ony 85% complete and not application fault tolerant; to me, that's a scarey thought.\n\nIMHO, KDE developers should focus on making existing applications as useful and as stable as possible on LINUX before attempting to port them onto the unstable M$ OS. Consider the implications for a moment. If anything about the use of this project becomes problematic, that gives Microsoft supporters ammo with which to attack Linux, allowing them the opportunity to claim that if it's not stable under Windows, then it's not stable under Linux. This doesn't need to be true for them to claim it and for those considering a transition to Linux to believe it."
    author: "Arkain"
  - subject: "I know this sounds stupid but how do you work it?"
    date: 2003-12-12
    body: "i know kde but i don't know how to install linux programs on cygwin. i have redhat 9 and am getting fedora soon but i don't have anywhere to put them so i'm left with cygwin."
    author: "Brian"
---
The <a href="http://kde-cygwin.sourceforge.net">KDE on Cygwin</a> project, the project to port Qt and KDE to Windows, has <a href="http://sourceforge.net/forum/forum.php?forum_id=178738">announced</a> the <a href="http://kde-cygwin.sourceforge.net/kde2.php">first beta release of KDE 2.2.2</a> for <a href="http://www.cygwin.com/">Cygwin</a> and <a href="http://xfree.cygwin.com/">Cygwin/XFree86</a>. At the moment they offer kdelibs and kdebase for <a href="http://sourceforge.net/project/showfiles.php?group_id=27249">download</a> but it seems we can expect other packages like kdegames, kdepim and kdevelop in near future too. 
<!--break-->
