---
title: "Supporting KDE: So Many Options"
date:    2002-04-08
authors:
  - "wbastian"
slug:    supporting-kde-so-many-options
comments:
  - subject: "Don't give to the League if you want to help development"
    date: 2002-04-08
    body: "KDE eV is an organization by and for the KDE development effort.  KDE League messes around in politics.\n\nThe two organizations are different.  Keep the difference between the two clear if you give money."
    author: "Neil Stevens"
  - subject: "Re: Don't give to the League if you want to help development"
    date: 2002-04-08
    body: "I think both of these are important if we want to get more people to use KDE, although donations to the KDE League will have less impact on the overall development effort and final product.  Still, promotion of the product can be a good thing as well.  All of us here know how good KDE is, and probably already have it installed.  The people the KDE League is targeting are those who haven't experienced KDE before.\n\nIf you're interested only in bettering KDE, and really couldn't care if it is promoted to anyone else, then KDE eV is the place to put all of your donations.  But if you check out the KDE League's site (http://www.kdeleague.org) and you like what they're doing, maybe toss a little donation to them on top of your KDE eV one.\n\nThe good thing about having the two seperate accounts is that you decide where your money goes.  This is certainly a better situation than having one group which divides the funds between interests as it feels fit.  I think that is where the KDE League has done well; they keep themselves away from the development and leadership aspects of KDE so that you don't have to interact with them if you don't feel like it."
    author: "Anonymous"
  - subject: "Re: Don't give to the League if you want to help development"
    date: 2002-04-10
    body: "Well put."
    author: "Neil Stevens"
  - subject: "Re: Don't give to the League if you want to help d"
    date: 2002-04-09
    body: "Good that there are people out there who tell us what to do. I for myself would be too stupid to decide for myself.\n\nThank's a million -\n\nGuenther"
    author: "Guenther Palfinger"
  - subject: "Link to jobs board broken"
    date: 2002-04-08
    body: "Just thought that someone should fix the link to the jobs board, should be http://www.kde.org/jobs.html"
    author: "Kamil Kisiel"
  - subject: "Thanks Neil"
    date: 2002-04-08
    body: "since freekde.org is down, you have to speak out here, eh?\n\n/me chuckles then goes back to contributing to KDE by offering users tech support in #kde-users on irc.kde.org\n\nTroy"
    author: "Troy Unrau"
  - subject: "Re: Thanks Neil"
    date: 2002-04-08
    body: "I'm glad someone does speak out at times."
    author: "Rob Kaper"
  - subject: "Re: Thanks Neil"
    date: 2002-04-08
    body: "> I'm glad someone does speak out at times.\n\nSpeaking out is good, but should be done responsibly.  Regrettably, as it stands, both of Neil's statements are inaccurate.\n\nFirst, KDE eV, in the past, has not been \"by and for the KDE development effort.\"  KDE eV was formed to enter into a contract with a certain Norwegian company at the time that Qt was under the original license.  Since that time it has done very little for KDE development, most notably because it did not have, or seek to have, any funds.  Now that KDE eV is raising money, that will hopefully change (it will definitely change if enough proceeds are raised).\n\nHow the money is used is up to the KDE eV membership, subject to the charter, under which KDE eV is a nonprofit that helps KDE.  Nevertheless, historically, KDE eV has not been involved with development, and the new donation avenues mark a new and exciting chapter in its life.\n\nSecond, the KDE League has not \"messed around\" in politics.  Presumably Neil is referring to the comment in the Microsoft settlement.  But that did not cost the League any money.\n\nThe more important issue, which Neil ignores, is that KDE eV controls the League.  It has a majority position on the Board and a controlling vote at the membership level.  Therefor, whether you give to KDE eV or the League, in the end the same entity (KDE eV) controls how that money is spent, though of course in the KDE League it is incumbent on KDE eV to at least consider the wishes of the other members.  But both accounts benefit KDE.\n\nThe essential difference is that KDE eV could hire and pay a developer to work on KDE (though of course it currently lacks the funds to do anything like that), whereas the KDE League cannot.  The KDE League focuses on promotional efforts.  However, the League can, under its charter, pay for developers to attend developer meetings or to write papers for publication in computer journals.  It can also do useful things like set up PayPal accounts.  More importantly, the League can help promote KDE development and use, and possibly convince more developers to develop for KDE.\n\nIn fact, it is quite possible that the League can be very successful in encouraging a large number of developers to contribute free code to KDE.  In the final analysis, that is the most important promotional objective.  Of course, all of this becomes moot should KDE become illegal, hence the MS settlement letter.\n\nOf course you should decide carefully who you want to contribute to.  But when you do, you should reach your decision with accurate information.\n"
    author: "Dre"
  - subject: "Re: Thanks Neil"
    date: 2002-04-08
    body: ">Of course, all of this becomes moot should KDE become illegal, hence the MS settlement letter.\n\nWhat does this mean? Illegal?"
    author: "reihal"
  - subject: "Re: Thanks Neil"
    date: 2002-04-08
    body: "See, it's good Neil spoke up. His statement, even though it may have been inaccurate, at least resulted in a long explanation. So in the end it's all for the better. ;-)"
    author: "Rob Kaper"
  - subject: "Re: Thanks Neil"
    date: 2002-04-09
    body: "Yes, everyone.  The head of the KDE League is your source for unbiased, accurate information about the League.  Ignore all criticism and empty your wallets for Dre."
    author: "Neil Stevens"
  - subject: "Re: Thanks Neil"
    date: 2002-04-09
    body: "Neil, I've read your posts here and on KDE Cafe.  From what I've seen, with friends like you, KDE doesn't need enemies.  In particular, this last posting of yours is disturbing.  My inference is that you are accusing Dre of intellectual dishonesty and other malfeasance.  You pretty much debase his/her honour. Was that your intent? \n\nI think that you'd really like to make the world a better place.  I honestly do.  But, man oh man, are you ever going about it in the wrong way.  You don't have to be a Pollyana but I'd like to suggest that you find more positive ways to promote KDE."
    author: "Rob Fargher"
  - subject: "Re: Thanks Neil"
    date: 2002-04-08
    body: "Yes, what is that MS issue? Anyone give us a idea?"
    author: "Xuedong Zhang"
  - subject: "Re: Thanks Neil"
    date: 2002-04-10
    body: "The MS issue is when the KDE League entered a statement with the US government against a proposed settlement between the US government and Microsoft, thus entering into and taking a stand in US political and legal processes."
    author: "Neil Stevens"
  - subject: "Re: Thanks Neil"
    date: 2002-04-15
    body: ">The MS issue is when the KDE League entered a statement with the US government against a proposed settlement between the US government and >Microsoft, thus entering into and taking a stand in US political and legal processes.\n\nUm... yes.... and?\n\nThe settlement between the DoJ and M$ seems to already be acted upon by the latter party, or rather, be taken advantage of the naive wording of, in order to further advance their monopoly, penalise OEMs who seek to bundle Linux or dual boot their systems, and to constrain developers as we have seen with the new licences that Microsoft are currently implementing, with anti-GPL wording, mirroring that of the settlement.\n\nIt is right that Redhat, IBM and others with an investment in Linux should make themselves heard, since this is now the principle competition to the Redmond monopoly. It is transparent to all that Linux is now a target and that the judicial system and legislature need to be made highly aware of this.\n\nMoreover, with Desktop Environments now familiar to users and the preferred way of interacting with modern computers, handhelds, STBs and soon mobile phones, this is where the new battles for the future of computer interaction will be fought. The KDE as principle environment of Linux these days (no disrespect to GNOME users, yours is nice too :) ) will be facing the brunt of Microsoft's future tactics, most especially licence conditions, software patents against visual metaphors, data representations, work flows, etc. that will control this key battleground.\n\nShould the KDE League have issued a statement? They had more right and more responsibility to do so than Redhat or any other, to be frank.\n\nAt the moment, KDE ev. doesn't support any developers. Ideally money goes to both them and the KDE League. If money can only go to one, make it go to the League; because the truth is, if Microsoft manage to legitimise their conduct and manipulate their final judgement to positive effect on their market position, or take their political influence further to the point where Linux and the KDE become crippled or illegal, that is the end of the game. A single law passed without sufficient awareness of potential consequences could destroy the OSS movement entirely. We should be very aware that the fact is, these days, if something is passed into US law, it tends to be enforced upon the rest of the world sooner or later, especially in terms of copyright and patents. This is a significant possibility and a significant risk.\n\nI would love to see more developers paid to work full-time on the KDE. That would be wonderful! But in the end, OSS is always strong because there are always more willing developers, regardless of money. Where we are weak is in advertising and lobbying, and those things are a real problem.\n\nSo overall, support the KDE any way you can or feel comfortable. If you aren't quite sure of my arguments, don't give money. Fair enough. Code instead, or as well :) Or help a friend get to know Linux and the KDE for the first time, or a local organisation."
    author: "Dawnrider"
  - subject: "#kde-users"
    date: 2002-04-09
    body: "Yes, I'm glad to have help in there.\n\nI'll not say much to the rest of your comment, because I find it difficult to understand what you're saying.  It *seems* that you're implying that dot.kde.org should only welcome those people who just say \"woohoo\" to whatever gets posted, but that *can't* be what you mean."
    author: "Neil Stevens"
  - subject: "KDE e.V. Website"
    date: 2002-04-08
    body: "If you want to know more about KDE e.V. like board and statutes, visit http://devel-home.kde.org/~vorstand/ ."
    author: "Anonymous"
  - subject: "bugzilla.kde.org?"
    date: 2002-04-08
    body: "I personally find the kde bug database a bit confusing to use, are there any plans to switch to using bugzilla? I'm sure this has been discussed before somewhere, can someone who knows something about something please give me some details? What are the pros and cons? Would the transition be possible?\n\nThere are other open source projects that use bugzilla, eg:\n\nMozilla: http://bugzilla.mozilla.org/\nGnome: http://bugzilla.gnome.org/\nOpenOffice.org: http://www.openoffice.org/project_issues.html\n\nBugzilla does have its quirks, but, IMHO, it's a great system for managing your bugs. Maybe it's just a matter of preference, but I think it would be way better than the current bug database."
    author: "ac"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "> I personally find the kde bug database a bit confusing to use\n\nI personally find Bugzilla much more confusing to use.\n\n> are there any plans to switch to using bugzilla?\n\nNo, http://bugs.kde.org/db/36/36733.html.\n\n> There are other open source projects that use bugzilla, eg:\n\nThere are other open source projects that uses the same bug database as KDE, eg:\n\nDebian: http://bugs.debian.org\n\n> Bugzilla does have its quirks, but, IMHO, it's a great system for managing your bugs.\n\nIt adds a greater administration overhead while a bug squad team would be still missing."
    author: "Anonymous"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "> No, http://bugs.kde.org/db/36/36733.html\n\nOk, thanks for the informative url. Should have found that myself.\n\n> It adds a greater administration overhead while a bug squad team would be \n> still missing.\n\nYep, that's probably true.\n\nSo, I should probably learn how to use bugs.kde.org then :)... I'm a bit afraid to file bugs, because the system looks so -- I don't know -- unpolished (or unfamiliar). I'm probably just biased because I'm used to the \"bugzilla way\"."
    author: "ac"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "post bug from kde apps... \nits so sweet...\n\n"
    author: "somekool"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "i fully agree to this, bugzilla is pure confusing crap. i was using it for long reporting bugs to mozilla and gnome stuff. its a pain, i never felt so much confortable than now with bugs.kde.org its clean simple easy."
    author: "spamo"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "I strongly disagree. When I reported a bug for the first time using Bugzilla, I immediately understood everything. No confusion, no \"help! what should I do now?\". Everything was clear since the beginning."
    author: "Stof"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "Either way, the Debian/KDE bug system is a lot simpler and gets the job done for a majority of users.\n\nKISS."
    author: "fault"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "Stof ??? arent you the same pro gnome stof that permanently attacked that Largo dude on the kde 3 announcement ? your argumentations are worthless and only enlightens fire here on this place."
    author: "spamo"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "\"permanently attacked\"?\n\"your argumentations\"?\n\nEh? Are you Largo, spamo? You sure talk/have a writing style like him. :-) "
    author: "fault"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-09
    body: "Yes, this is \"Aged Person\"/Largo under another of his aliases. A favorite tactic of his is to switch names and support his other alias. It's funny though, because he is always so obvious!"
    author: "Talua"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-09
    body: "You seem to be the person that changes his nick all the time. Talua/Stof/AC."
    author: "spamo"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-09
    body: "> Talua/Stof/AC.\n\nYou're saying I'm AC? Why would I talk to myself?\n\nAnd no, I'm not Talua. If take a close look, you'll notice that I write differently than he does."
    author: "Stof"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-09
    body: "Pro-GNOME? Hey wait I'm posting this using Konqueror so I guess I'm pro-KDE too! :-)"
    author: "Stof"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: ">> I personally find the kde bug database a bit confusing to use\n>I personally find Bugzilla much more confusing to use.\n\nSame here.\n\n>There are other open source projects that uses the same bug database \n>as KDE, eg:\n>Debian: http://bugs.debian.org\n\nIIRC, it was developed for Debian's bug reporting and was picked up by KDE."
    author: "Otter"
  - subject: "Re: bugzilla.kde.org?"
    date: 2002-04-08
    body: "We don't use it because it's way too complicated :-)"
    author: "ac"
  - subject: "events.kde.org"
    date: 2002-04-08
    body: "Please notice, that http://events.kde.org is under heavy construction. It is not ready at all and many statements are drafts only. But feel free to contribute informations, resources, links, dates, etc. :-)\n\nPlease don't use the registration system, since it doesn't work yet!\n\nGreetings,\neva"
    author: "eva"
  - subject: "Re: events.kde.org"
    date: 2002-04-08
    body: "> Please don't use the registration system, since it doesn't work yet!\n\nI will not until it's clearly stated what happens with the personal data, who gets it and what happens with it."
    author: "Anonymous"
  - subject: "For the love of God pull the KDE 3 story!!!"
    date: 2002-04-08
    body: "I dunno about everyone else but I'm downloading pages at 700B/s. Is there any way you could move it down the story queue, and maybe start a new \"Comments on KDE 3\" thread to catch all the stray comments people still want to make. You guys _really_ seem to be low on bandwidth lately.\n\nAnother thing, when people are posting replies, could you just display the introduction to the story instead of the full thing so it doesn't take so long to download (I've only noticed this recently ;-)? People don't usually respond to the story directly anyway.\n\nSorry to moan so much, you guys do a fantastic job, and you've really helped build up a wider KDE community, but the site is definitely under a lot of strain at the moment."
    author: "Bryan Feeney"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-08
    body: "Did you even read this story?\n\n\"You can choose to donate either to:  KDE e.v., which uses funds primarily for KDE infrastructure (such as servers and possibly bandwidth ...\"\n\nYes, we're low on bandwidth. Now go do something about it. ;-)\n"
    author: "Rob Kaper"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-08
    body: "hhhm, why would i want to donate for bandwidth\nif it just get's thrown out of the window for \n10 zillions times of a superb(!) but 52k big\nannouncement + 100 comments?\n\nyes, one should still donate - but i think the\nrequest is very much valid...\n\nregards\nThilo"
    author: "Thilo Bangert"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-09
    body: "But this is how a news and discussion site works.. "
    author: "Rob Kaper"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-09
    body: "personnally, i dont see any problem here ;.)\n\n"
    author: "Somekool"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-09
    body: ">Yes, we're low on bandwidth. Now go do something about it. ;-)\n\n\nOk, is it possible to mirror the dot?"
    author: "reihal"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-09
    body: "Yes actually, I did read the story but I've got just over \u0080100 to get me to the end of June. I'm borrowing from all around me and I'm trying to figure out the Irish tax system to see if I can wrangle any tax back. This is what it's like to be a student. Maybe when I've got a career and money to spare I'll make donations, but until then there's not much I can do. What about you?\n\nAnyway, apparently it's some sort of bandwidth control and not the story that's causing the trouble. Sorry for the fuss."
    author: "Bryan Feeney"
  - subject: "Re: For the love of God pull the KDE 3 story!!!"
    date: 2002-04-09
    body: "> Anyway, apparently it's some sort of bandwidth control and not the \n> story that's causing the trouble.\n\nStill creating a discussion forum for a release announcement that does not require you to load a 100K+ story again and again only to read and respond to posts (though most people probably only read discussions) and does not require users who just want to read the announcement and are not interested in the discussion to load the first 40 comments could dramatically decrease bandwidth requirements for the dot after an announcement, and might help not to be put under bandwidth control next time. No use spending lots of money on bandwidth fees if you could live with less.\n\nNote: I really like the connection between stories and discussion during normal times,  but release times are somewhat different. So why not add a seperate Comments link to the dot homepage during these times to seperate comments from content?\n\n(Also the full announcement is linked from the dot homepage. Is it really necessary to re-post it on the dot? Why not make a brief version without the full list of changes and just link to a more detailed changelog for the dot version? You could still have a full announcement including all changes on www.kde.org. Again this could help reduce bandwidth requirements without reducing the quality of the dot.)\n"
    author: "Anonymous"
  - subject: "exciting features"
    date: 2002-04-08
    body: "************\n\nHow about some features on telling us of all the exciting features which **are already in** kde 3? I feel that I'm missing out on a lot of things because I simply don't know of them\n\nKudos to all involved.\nK\n\n******************"
    author: "kev kde user"
  - subject: "Re: exciting features"
    date: 2002-04-08
    body: "How about taking a look at the changelog or the release schedules on developer.kde.org."
    author: "Chris Howells"
---
KDE reached a milestone last week with the
<a href="http://www.kde.org/announcements/announce-3.0.html">release of
KDE 3.0</a>, but that doesn't mean that KDE development is finished.
Indeed, many things still need improvement and many exciting features
have yet to be added to KDE. Fortunately, there are
a number of different ways in which <em>you</em> can help make KDE
better!


<!--break-->
<h3>Help with Money</h3>
<p>
KDE is available for free and will always be available for free,
including each and every line of its source code, for everyone to modify
and distribute. If you enjoy KDE, please consider supporting the KDE
Project financially. KDE is in constant need of funds in order to
finance its operations and to get the word out about KDE.
</p>
<p>
Making donations to KDE is now easier than ever, since recently the
<a href="http://www.kdeleague.org/">KDE League</a> opened several KDE
<a href="https://www.paypal.com/">PayPal</a> accounts. You can choose
to donate either to:
</p>
<ul>
  <li>
    <a href="http://www.kde.org/support.html">KDE e.v.</a>, which
    uses funds primarily for KDE infrastructure (such as servers
    and possibly bandwidth in the future) and to assist developers
    who attend developer meetings with expenses; or
  </li>
  <li>
    <a href="http://www.kdeleague.org/contribute.php">KDE League</a>,
    which aims to help promote KDE and can also assist developers
    who attend developer meetings or prepare and present development
    papers with expenses.
  </li>
</ul>
<p>
Donations are recognized in the
<a href="http://www.kde.org/donations.html">Donations Hall of Fame</a>.
</p>

<h3>Help with Code</h3>
<p>
In the end KDE is a software project and thus what it mostly needs is code.
If you are interesting in starting to program for KDE, think small:
experience shows that looking at all of KDE can be a touch overwhelming for
people just getting started.
</p>
<p>
We recommend doing any of the following to get started:
</p>
<ul>
  <li>
    Visit the <a href="http://bugs.kde.org/">bugs database</a>, find a
    random bug, fix it, and send a patch to the maintainer.</li>
  <li>
    Take a favorite KDE application and implement a missing feature
    that you really want.
  </li>
  <li>
    Find a KDE 2.x application and port it to KDE 3.x
    (<a href="http://www.kde.org/announcements/announce-3.0.html#porting">instructions</a>).
  </li>
  <li>
    Take your favorite console or plain X application and write a KDE
    shell for it.
  </li>
  <li>
    Check out the <a href="http://www.kde.org/jobs.html">Jobs Board</a> for
    an open job that you might like to do.
  </li>
  <li>
    Use your imagination!  The possibilities are almost endless!
  </li>
</ul>

<h3>Helping with Documentation or Translations</h3>
<p>
If history has taught us one thing, it is that developers rarely write
documentation for their own programs.  Yet KDE ships with lots of
documentation for individual programs as well as guides and "howtos".
Also, you may have noticed that KDE is translated into many languages.
All of this is done by our dedicated
<a href="http://i18n.kde.org">Documentation/Internationalization Team</a>.
</p>
<p>
The team is made up of volunteers that all have various interests in
this area.  Some have an editorial bent and help "clean up" incoming
documentation.  Some are well versed in several languages and help
translate.  Some are excellent writers and deliver high quality
"books".
</p>
<p>
If you are also interested in making KDE the best documented desktop
out there, please consider joining!
</p>

<h3>Helping with Feedback</h3>
<p>
We like it when you tell us how much you like KDE, but we also like
to hear it when things do not work as they should. We rely on <em>your</em>
feedback to make KDE better. You can use the
<a href="http://bugs.kde.org/">bugs database</a> to report problems and
suggestions for improvements.
</p>
<p>
To make your reports useful, please make sure to include an accurate
description of the problem and a valid e-mail address, so that we may
contact you about the problem. Please do <em>not</em> use our
bugreporting system for support questions, you will get a much faster
and better answer to your questions on the
<a href="http://mail.kde.org/mailman/listinfo/kde">KDE mailing list</a>.
</p>

<h3>Resources</h3>
<ul>
  <li>
    <a href="http://www.kde.org/helping.html">Helping Out</a>
    &nbsp;&nbsp;[ www.kde.org ]
  </li>
  <li>
    <a href="http://www.kde.org/support.html">Support KDE</a>
    &nbsp;&nbsp;[ www.kde.org ]
  </li>
  <li>
    <a href="http://www.kdeleague.org/contribute.php">Contributing to KDE</a>
    &nbsp;&nbsp;[ www.kdeleague.org ]
  </li>
  <li>
    <a href="http://promo.kde.org/">KDE Promotion Project</a>
    &nbsp;&nbsp;[ promo.kde.org ]
  </li>
  <li>
    <a href="http://bugs.kde.org/">KDE Bug Tracking System</a>
    &nbsp;&nbsp;[ bugs.kde.org ]
  </li>
  <li>
    <a href="http://www.kde.org/jobs.html">How to help KDE</a>
    &nbsp;&nbsp;[ www.kde.org ]
  </li>
  <li>
    <a href="http://i18n.kde.org/">The KDE Translators' and Documenters'
    Web Site</a> &nbsp;&nbsp;[ i18n.kde.org ]
  </li>
  <li>
    <a href="http://events.kde.org/">KDE Events</a>
    &nbsp;&nbsp;[ events.kde.org ]
  </li>
</ul>

