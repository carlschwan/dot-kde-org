---
title: "developerWorks: Coding with KParts"
date:    2002-02-22
authors:
  - "numanee"
slug:    developerworks-coding-kparts
comments:
  - subject: "The book"
    date: 2002-02-22
    body: "They forgot to link to:\n<br><br>\n<a href=\"http://www.andamooka.org/kde20devel/index.shtml\">http://www.andamooka.org/kde20devel/index.shtml</a>"
    author: "reihal"
  - subject: "Re: The book"
    date: 2002-02-22
    body: "Well, in the Resources section, the link to the chapter 12 is a link to the book, at least its copy on developer.kde.org.\n\nDavid."
    author: "David Faure"
  - subject: "KVim is still alive?"
    date: 2002-02-22
    body: "On Apps.kde there is still version 0.1, which was submitted September 12th 2000. And the homepage-link doesn't work anymore.\nKVim would be great. I hope someone is still working on it, but just too shy to talk about it..."
    author: "Michael"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "Yes they are working on it but afaik it will be quite useless to most people because it will not sport the KTextEdit interface.  I hope I am wrong about this but I would not hold my breath.  Personally I think a VI style editor makes little to no sence in a GUI, but I also like editors like emacs and Kate.  ESC-:-wq vs Ctrl+S - Ctrl+Q  really it makes no sense or is there something that Vim can do that Kate cannot?\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "Yes. AFAIK Kate does not have folding and integrated Diff/Patch support. Also remapping a key or keysequence with a complete sequence of keys. E.g. :imap <F5> /**<RETURN><RETURN><RETURN>*/<ESC>hhxkkA - to create a shortcut on the key F5 for \n\n/**\n * \n * \n **/\n\nor \n\n:imap dre der\n\nto implement something like autocorretion. IMHO there are very much features (at least in VIM 6) which are currently not supported in Kate. (Is there support for block selection in Kate?)\n\ncu,\n  Emmy"
    author: "Emmeran \"Emmy\" Seehuber"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "kate will have code folding fairly soon. i've got an early version of it on my system right now from the code_folding cvs branch. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "There is some work going in a branch in KDE CVS on code folding, AFAIK. \n\n\n\nI don't think the remapping functionality is there per se, but adding a plugin to paste certain text segments seems easy enough for any qualified programmer to do quickly -- in fact, it looks like the Hellow World plugin example is nearly it. I might even consider trying to implement one, and to smuggle it into 3.1 ;-) And IMHO, using plugins for those sorts of things is the right way of doing it -- with a C++ interface, Kate is more programmable than just about anything there is (OK, one can do lots in EMACS lisp -- but if you're a C++ programmer, wouldn't you rather use your preferred language?)\n\nBTW, why would you need the feature for your example that if there is a nice easy-to-use comment-out feature?\n\nIf by block selection you mean selecting rectangular areas of the text then yes, it's supported -- Settings->Vertical Selection"
    author: "Sad Eagle"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "Wow, I did not follow the kvim development, because it had stopped for a long time.I just found the HP:\n<br><br>\n<a href=\"http://www.freehackers.org/kvim/screenshots.html\">http://www.freehackers.org/kvim/screenshots.html</a>\n<br><br>\nWell does vim do things kate doesn't? You must be joking. kate is a small child compared to vim-6.0. vim has configurable syntax highlighting for everything. vim has decent completeion, a million of features which you won't see until you look for them becasue they HELP you in your editing.\nSeriously, kate is nice, but vim is so advanced, you wouldn't believe it."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "I see... well i dont see really, i can see what Kate can do... I have no clue what Vim is doing half the time, but that is a personal problem.   ESC-:-hkkhhsh... whatever...  That is not how things operate in my world.\n\nI know the scripting support in Kate is still in its infancy, but the syntax highlighting is very nice.  Via dcop and the ability for Kate to use KScript you can manipulate your document via DCOP.  The plugin system also allows for a greater level of expanabilty.  I am a firm beliver in usability over wizzbang features, hence why I use KDE instead of something like Gnome or FVWM hacked to heck...  I think in the next year we will see Kate pull ahead.\n\njust my 2c...\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-24
    body: "Well I actually don't know much about the power of kate vs. vim.  I'm just used to vim and I don't see any reason to change to anything else.  So far none of my friends could show me any feature in any other editor, which would cause me to change the editor.  I mean: never change a running system as long as you don't need any new features...\nHowever I agree that many people are not used to something like \"ESC :wq\" and I don't have any problem with them.  I think everybody should use his/her favourite editor, that's why I would wish many KParts (hopefully including vim)."
    author: "Michael"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "Yeah, you are right, vim is quit advanced, but I don't really want to reprogram vim, because that would be really senseless, users which like vim wouldn't switch to a vim clone and new users would say: \"why use a clone if you can have the original  ?\"\nKate is not a vim/emacs clone and hopefully won't get one ;)\nSome of the vim/emacs/nedit/ultraedit/scintilla features (like folding, dyn. word wrap, ...) are very nice and we should try to get such \"more advanced\" stuff working, but I think for a project started around 1-2 years ago around the old KWrite codebase, the Kate KPart/App isn't such bad ;) And I even think our syntax highlighting is really one of the strong points (look at the new xml hl in the kde CVS, really nice, only a few typos inside) and the textbuffer we use isn't that bad (exspecially for bigger files), too.\nAnd from view of the KParts side: Kate is the only real implementation of the KTextEditor interfaces of kdelibs at the moment and one of the only real editor kparts for kde 2/3 which actually work ;)\nMy opinion at all is:\nVim/Emacs/Nedit and some other are really much ahead in some areas, but I hope we catch up at some important points as code folding, word wrap, scripting, .... but (K)Vim and the others need to catch up too in point of the kparts support."
    author: "Christoph Cullmann"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-23
    body: "> Why use kvim when you can have the original?\n\nSimple. I would want vim to replace everything that uses text editing/input\nin KDE. I am used to the vim keyboard layout, it is not intuitive, but it\nisn't supposed to be - it's supposed to be effective. I have problems with\nmy hand joints (Sehnenscheidenentz\u00fcndung in German) and I'm not able to use\nShift or CTRL (ie. the 5th finger) more than absolutely necessary. I have\nalready remapped my keyboard to use caps-lock as a second Enter, and the\nwindows keys as second backspace / delete keys, but that only distributes\nthe \"fifth finger load\" between hands, it doesn't eliminate it.\n\nEscape-Meta-Alt-Control-Shift (and xemacs) are completely out of reach for \nme. Even cutting and pasting with CTRL combinations are awkward, I don't \nlike taking my fingers off the default positions just to move the cursor \n(let alone to find the mouse).\n\nPlus, I always tend to hit ESC when finished writing, which - e.g. in licq -\nmakes me yell \"SHIT!!!\" 2 seconds after having written a 1000 word message to\nsomeone.\n\nSo, if this is in any way possible, I want a kvim plugin to (OPTIONALLY!) \nreplace Konqueror's <TEXTAREA>s, to replace multiline edit fields, and to replace\nthe default editor widget that plugs into everything.\n\n\nI think this is possible, and I will try to help anybody who dares.\n\nOf course, this is only MHO, and YMMV. HAND.\n\n\n(btw you have no idea how long typing this messages took, with all those\ncapital letters =;)"
    author: "Jens"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-23
    body: "Sorry, but I think you should quote me right (or try to read what I write better), I never wrote\n\n\"Why use kvim when you can have the original?\"\n\nI just pointed out that I don'T want Kate to be a (k)vim clone. I never said I don'T want the development of kvim ;)\n\n"
    author: "Christoph Cullmann"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-23
    body: "Sorry, I actually wasn't quoting (this was more intended to be an answer to\nwhat I thought the 'gist' of your message was.\n\nIf I misunderstood, sorry. I tend to read too quickly these days.\n"
    author: "Jens"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-23
    body: "No prob ;) I make the same thing too often, too ;)"
    author: "Christoph Cullmann"
  - subject: "Re: KVim is still alive?"
    date: 2002-02-22
    body: "The most exciting thing about a vim Kpart is not the email writing, but embedding it into KDevelop. Many coders like using vi(m) for programming and once you get used to it, you don't want to change. A Kpart gives you the ability to use the power of KDevelop with an editor that makes you effective."
    author: "cosmo"
  - subject: "(Was: KVim is still alive?) OT: KEmacs, KDE Octave"
    date: 2002-02-23
    body: "KVim would be _nice_.\nKEmacs would be *GREAT* :-)))\nBut that would be a really complicated (but also really damn great) project.\n\nAnd since I'm probably already off topic here let me ask for a KDE GUI frontend for Octave!! Ocatve is GPL and great for numerical computation and could be an alternative for MatLab for many people if it had a decent GUI."
    author: "J.A."
  - subject: "Re: (Was: KVim is still alive?) OT: KEmacs, KDE Octave"
    date: 2002-02-24
    body: "I agree a KDE GUI for octave would be really great. Math/computation programs are missing under linux..."
    author: "Thomas Capricelli"
  - subject: "OT: KEmacs, KDE Octave"
    date: 2002-02-24
    body: "Well, I wouldn't say Math/computation programs are *missing*.\nBoth Mathematica (algebraic computation) and MatLab (numerical computation) have Linux versions. I'd just like to see more and better open source or at least freeware tools.\n\nThere is MuPad (algebraic computation), which is kind of free and pretty good. For numerical computation there is Octave but the GUI is missing.\n\nJ.A.\n"
    author: "J.A."
  - subject: "Re: KVim is still alive?"
    date: 2002-02-24
    body: "Yes.\nKVim is alive\nKVim as a standalone app is working really nice, and you can find it in kdenonbeta\n\nKVim as a embedable component was/is very hard to get. But we'll release kvim-0.2 RSN. It will probably be announced on the dot, and doubtlessly on www.freehackers.org\n\nsee ya.\n\nThomas"
    author: "Thomas Capricelli"
  - subject: "XEmacs interface"
    date: 2002-02-22
    body: "Something I had thought about way back when was using QT XT widget support to embed the xemacs widget in an application. But at that time the status of the widget seemed to be experimental and I couldn't find much information. This was quite a while ago.\n\nNow the last time a got the latest release of xemacs, of the top of my head, it seemed like more work had done on it. I think it would be really nice to be able wrap a KParts interface around the xemacs widget and use it in KDevelop. Hopefully this would allow you to use all the nice lisp packages, taking advantage of all the great resources available for xemacs. One that I really like is word completion based on the buffers you have open.\n\n-Willy"
    author: "Willy"
  - subject: "\"KParts are KDE-specific and C++ only.\""
    date: 2002-02-23
    body: "'C++ only' isn't strictly true. In KDevelop 3 you can write KParts in Java (see kdevelop/parts/javasupport or the java plugin project template). They need a common C++ stub to start, and the rest of the code is just java. The KDE java  bindings have enough event callbacks and slot/signal handling to allow you to do pretty much everything you would in a C++ part.\n\nThe advantage of writing KParts in java is that you can compile them to platform independent JVM byte codes, and then download them from the internet (ie partlets?) into a running app..\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: \"KParts are KDE-specific and C++ only.\""
    date: 2002-02-23
    body: "I am confused by the docs on that subject.\n\nHow can the kparts loader know what to run?  Or is the final Java code something other than a JAR.  I have been trying to find docs on the subject, mainly because I want to be able to load KParts created with PyKDE, or maby even some other future language.\n\nThe partlets bring up an VERY cool idea though.  I am a little curious on the secureity of the plan but really it looks like what .Net promises... only now ;)\n\n\ncheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: \"KParts are KDE-specific and C++ only.\""
    date: 2002-02-24
    body: "\"How can the kparts loader know what to run?\" \n\nThe factory class from the C++ stub, 'MyTestFactory' here, uses KStdDirs/KInstance to find the jar file, and adds it to the class path. It then loads the corresponding java factory class. The MyTestFactory::createPartObject() method constructs an instance the java class 'MyTest' to start the part running.\n\nMyTestFactory::MyTestFactory(QObject *parent, const char *name)\n  : KDevFactory(parent, name)\n{\n...\n classPath = \"file:\";\n classPath += instance()->dirs()->findResourceDir(\"data\", \"kdevmytest/kdevmytest.jar\");\n classPath += \"kdevmytest/kdevmytest.jar\";\n\n env->CallStaticVoidMethod( urlLoaderClass,\n               addURLmethod,\n               env->NewStringUTF((const char *) classPath) );\n\n cls = env->FindClass(\"MyTestFactory\");\n...\n}\n\nBernd Gerhmann thought it might be possible to use the same C++ stub for different parts. He wrote:\n\n\"Perhaps it would be possible to write a generic C++ part that\ncan load arbitrary Java parts? (well, it least generic for a\ngiven service type). In the .desktop file you can define\na keyword X-KDevelop-Args whose contents are given to the\ncreatePartObject() method. So in principle one could think about\nreplacing $APPNAMELC$ in the template by the 'args' string that\ncreatePartObject() gets. This way, you don't have to compile C++\ncode for each java part.\"\n\nSo it needs more experimentation. Python or java code should just be another resource inside a kpart wrapper directory, and the part would just look normal to the outside world.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: \"KParts are KDE-specific and C++ only.\""
    date: 2002-02-24
    body: "I think this is the most effecive solution.  This is what korelibs and ActiveX Controls seem to do.  They Although from what I can tell for writeing COM objects in Python you have to wrap the individual python module in some code, in my mind defeating the purpose of writeing the module in python anyway :\\\n\nThe only issue that seems to come up with this is how to deal with multiple instances of the plugins.  This I have not researched very much.  \n\nI think this will be a big part of KDE 3.1.  I think by that point we will be seeing KDE components written in Python Java and hopefully even Ruby as this matures.  One other thing we need to keep our eyes on is dcop and dcop services.  These too in my mind will help integrate 3rd party modules into KDE.  The one GREAT advantage of DCOP based extentions is no BIC issues :)\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: \"KParts are KDE-specific and C++ only.\""
    date: 2002-02-25
    body: "All the example code that I quoted from MyTestFactory::MyTestFactory() could be moved to the MyTestFactory::createPartObject() method, where the part name string would be available from X-<app name>-Args, as per Bernd's scheme. So it does seem possible to write a generic C++ part wrapper for particular languages like Java or python.\n\nKDE/KParts has a good resource location and packaging mechanisms, which fit very well with script code (eg Ruby or Perl) or java bytecodes in .jar files. No need to have a common runtime, it is more important to have a common Qt/KDE object model, plus KParts packaging.\n\n-- Richard"
    author: "Richard Dale"
---
<a href="http://people.mandrakesoft.com/~david/">David Faure</a>, proud owner of an excellent <a href="http://people.mandrakesoft.com/~david/david_at_fosdem.jpg">t-shirt</a>, has written <a href="http://www-106.ibm.com/developerworks/linux/library/l-kparts/">a very nice article</a> for <A href="http://www-106.ibm.com/developerworks/">IBM developerWorks</a>.  David gives a beautiful overview of <a href="http://developer.kde.org/documentation/books/kde-2.0-development/ch12.html">KParts</a> and touches on everything from <a href="http://developer.kde.org/documentation/kde2arch/dcop.html">DCOP</a> to <a href="http://developer.kde.org/documentation/kde2arch/xmlgui.html">XML-GUI</a>, <a href="http://trolls.troll.no/~lars/xparts/">XParts</a>, and <a href="http://www.kde.gr.jp/help/doc/corba/doc/komop/HTML/">CORBA</a>. <i>"KParts is also used in more high-level interfaces, such as the TextEditor interface. The former is a complete interface that models the API of a text editor so that applications can interchangeably use any text editor available that implements this interface. vi users will love being able to type mail in KMail using a vi text-editor component (such a component is under development). A general ImageViewer interface is under development as well."</i>  Great to see KDE development getting the exposure it deserves.
<!--break-->
