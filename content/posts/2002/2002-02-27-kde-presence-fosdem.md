---
title: "KDE Presence at FOSDEM"
date:    2002-02-27
authors:
  - "numanee"
slug:    kde-presence-fosdem
comments:
  - subject: "Good interviews"
    date: 2002-02-27
    body: "Now, that was a long read :-) Good work, but sad some interviews got lost :-(\n\nIs David Faure still coding for KDE? Yes he is, I know. But I asked this because he seems to be writing articles and doing a lot of interviews lately :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Good interviews"
    date: 2002-02-27
    body: "\n One of the missing interviews is with Mickael Meeks (gnome). We might have it. Stay tuned :)\nWe're still missing the KDE-related questions on this one."
    author: "Thomas Capricelli"
  - subject: "Lack of KOffice developers"
    date: 2002-02-27
    body: "Let's hope some new developers join inspired by this interview and that we don't have to wait until all other KDE packages are \"perfect\" and whose developers will move on to KOffice."
    author: "Someone"
  - subject: "Geeks..."
    date: 2002-02-27
    body: "Why are developers always so ugly? :)\n"
    author: "Cute Tranny"
  - subject: "Re: Geeks..."
    date: 2002-02-27
    body: "This is because there are not enough females...\n\nI would have liked some names to associate with these <geek-stereotype>beards and greasy hair</geek-stereotype>!\n\n\n\n"
    author: "annma"
  - subject: "Re: Geeks..."
    date: 2002-02-28
    body: "\"This is because there are not enough females...\"\n\nOoh no, a planet full of males :-)\n\nannma: keep searching for females who want to contribute! We need them to brighten up our days... :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Geeks..."
    date: 2002-02-28
    body: "Developers in average are not ugly. It is just that I skew the average."
    author: "Roberto Alsina"
  - subject: "Re: Geeks..."
    date: 2002-02-28
    body: "Developers in average are not ugly, but very_ugly_developers are always moving outta. Everyone saw these (/or me)! ;)) "
    author: "Daniele Medri"
  - subject: "Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "This is a bit off topic, but I would like to know if only chosen people can post news on dot.kde.org? I posted something yesterday and it didn't appear on dot. It would be very nice if maintainers could send an email to people whose effort to contribute something has been refused. It is not the first time that it happened, and I feel I am not going to post anything in the future. \n\nBest regards,\n\nantialias"
    author: "antialias"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Hi,\n\nPaying customers are given priority, I hope you understand.  Everyone has to make a profit, I hope you agree.  :-)\n"
    author: "ac"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Its like Slashdot.\nThe editors choose whats interesting and what is not.\n\nSomeone"
    author: "Someone"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "I understand that, I just thought that linuxformat.co.uk awards where KDE is nominated in 4 chategories is news. I was wrong, photosession showing kde developers eating pizza & dancing is obviously more important ;-)\nBut who cares, freekde.org is fortunatelly back soon."
    author: "antialias"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Look, I'm sorry we haven't written you back yet.  You don't have to be a jerk about it.  \n\nThe truth of the matter is that I (personally) haven't really investigated or decided what to do with your article yet, and I had others to process.  Rob's submission came way before yours.  I don't have an infinite amount of time on my hands, and nor do the rest of the *volunteer* moderators.\n\nAnd yes, I'm glad freekde.org is coming back too.  And no, I'm not jealous that you have an alternative.\n"
    author: "Navindra Umanee"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: ">Look, I'm sorry we haven't written you back yet.<\n\nNo problem :)\n\n>You don't have to be a jerk about it.<\n\nHuh, it sounded like jerking? ;-) Sorry, english is not my native language, and it was not my intention to be either rude or unpolite.\n\n>I don't have an infinite amount of time on my hands, and nor do the rest of the *volunteer* moderators.<\n\nUnderstood.\n\n>And no, I'm not jealous that you have an alternative.<\n\nNothing to be envious about. Dot.kde.org links to freekde.org. Remark about freekde.org was not meant serious. Just a joke. When I start Konqueror the first thing I see is dot.kde.org. The same will be in the future.\n\nRegards,\n\nantialias"
    author: "antialias"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Thanks, sorry for my reaction.  \n\nI'm at school, working on a hardish SSA problem in the library and just, as a break, popped by the terminal to see how the dot denizens were doing.  Then I see this thread about \"chosen ones\" and wasn't exactly thrilled.\n\nThanks for the support.\n"
    author: "Navindra Umanee"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "You're running on Zope, aren't you? It's a pity that there isn't a Zope equivalent of 'scoop' (as run by www.kuro5hin.org), as that it baslly self running - the readers choose which of the submitted articles get posted."
    author: "Jon"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Believe Navindra, guys.  Navindra has been one of freekde's earliest supporters.\n\nAnd everyone's support is appreciated. :-)"
    author: "Neil Stevens"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Let's wait until KDE wins something. :-)\nDon't announce votings otherwise there will be reproaches afterward that it was influenced by KDE."
    author: "Anonymous"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "freekde.org comes back, that's great\n\nbtw: what's going on with http://www.kdedevelopers.net ? Is someone still maintaining it? I doubt it...\n\nI think a real KDE community site would be nice. Ok, we have www.kde.org for general info, the dot for news, ... but nothing for the KDE users/developers where they can see various stuff. I'm thinking of a poll, a common place to put pictures taken at meetings, interviews, ... What we have now, is pictures at host X, interviews at host Y, and so on... Not everyone has a host to share some things with the community."
    author: "Andy \"Storm\" Goossens"
  - subject: "Hosting"
    date: 2002-02-27
    body: "Yes, it's being maintained, and it's there to supply hosting for KDE contributors who need it.\n\nThese issues you address are real ones faced by the community, but right now people who need hosting have many alternatives.\n\nGo ahead.  Take some pictures, or do an interview, then ask around for hosting for it.  I bet you'll find many possible places to put it."
    author: "Neil Stevens"
  - subject: "Re: Hosting"
    date: 2002-02-27
    body: "But when you visit kdedevelopers.net now, you get a lot of MySQL errors. I'm not a frequent visitor, but I saw the problem yesterday, and today it isn't fixed yet.\n\nThe last time I saw it (without the errors), it wasn't updated in months (news, poll, etc)."
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-27
    body: "Hello,\n I'm the maintainer of kdedevelopers.net. There was recently a very small MySQL error on the server after a complete migration from a 3.2gig hard drive and Redhat 6.2 to 40gigs and SuSE 7.3. Now my users have much more room, and I soon hope to provide exclusive CVS hosting to a few if needed.\n\n Andy, although I don't post much on the front page, I do maintain the stuff \"behind the scenes\" (e.g. user webpages, email, ftp, etc). If you would like an account to host various stuff off of, please email me at nbetcher@usinternet.com.\n\n  Thanks,\n   Nick Betcher"
    author: "Nick Betcher"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-02-28
    body: "OK, I was wrong. Happens all the time :-)\n\nI don't need hosting at the moment, but I know some others that may want it.\n\nI recently looked into your KDE Installer Project source code. I'm a beginning C++ developer (but I know other languages), so I may learn something from the code. Why was this project stopped? I assume installing packages was to difficult to do.\n\nA small note for the KDE messenger: Dirk Mueller is working on Licq and he may give you some advice about it. (If he has some spare time that is...)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Posting news on dot.kde.org only for chosen?"
    date: 2002-03-05
    body: "\nHi,\n\nThere is indeed a problem with feedback to rejected submissions, but the problem stems primarily from the fact that we are all extremely busy and there are a lot of submissions we feel are inappropriate.  At other times, it may not be the case that the reason for a rejection can be explained properly, mainly b/c things involve value judgments and it takes a lot of time to draft a proper \"official\" response.\n\nThere are of course some people who advocate that a computer program do the submission review - that somewhere we should post a \"formula\" and apply it consistently to all submissions, so that somehow there is no editorial judgment involved.  This view is completely unrealistic, and though you may find the occasional site that attempts to post a submission policy, if you compare the policy with the accepted and rejected items (the latter being much harder to locate), you will find that there really isn't much point to it.\n\nAt least from my perspective, and I am just one editor of many, the broad outlines, which should be so obvious as not to need posting, are that:\n\n  (1) they are related to KDE\n  (2) they are news\n  (3) they are likely to be of interest to a broad base of our readers; bonus points if they are likely to generate discussion\n  (4) they are within the confines of the purpose of the site (so generally we do not accept third party press releases, such as application releases, since appsy exists for that purpose; however, application reviews are much more interesting, and you can see from the KDE.de App of the Month series and stories on some other published reviews, particularly in non-Linux media, that we do accept such submissions)\n  (5) they are responsible journalism\n\nIn your case, we decided not to post the story b/c it would be viewed as \"biasing\" the poll.  Our general policy is not to announce polls that involve KDE unless necessary to compensate from an announcement elsewhere.  Unlike some, I believe that polls should be unbiased and the dot should not be used as a vehicle to draw people to a poll that would otherwise not run across it.  This would be item (5) above.  I realize that others will disagree with this policy, but the one and pretty much only benefit of being the one that actually does the work, is that your opinion counts for more :-).\n\nIt's not clear to me, anyway, that the existence of a poll is even news (item (2) above).  There are many sites that have a daily poll.  I personally don't think polls are news.  Of course, the results could be, so please let us know when they are ready :-).\n\nAnyway, one thing people forget is that the Dot itself is an Open Source project.  We have maintainers, who work for free in their spare time, to create a site that KDE users will find useful and interesting.  In fact, the site was started by Navindra and myself (actually we were working simultaneously on two different sites, as we both felt a news site was needed, and when I found out about Navindra's efforts I asked to join him and he cordially agreed), but there are a large number of other editors involved, or with the ability to get involved, in the project.\n\nWhen you think of the site as an Open Source project, managed by volunteers who are very busy with other things as well, a lot of issues are hopefully clearer.  Just like I cannot demand that company A or developer B include feature Y in their Open Source product - or that they \"publish\" their paradigm for including or not including features, under the assumption that if somehow my feature request fits inside this published paradigm the company or developer has an *obligation* to include it - nobody can demand that the dot does anything.  The dot is not a monopoly, there are a million websites in the world you can publish news, many of which get far more traffic than this site.  FreeKDE.org is a fine example of a site which was launched because Neil was dissatisfied with our editorial policies.  People interested in commercial products know where the website is.  This is KDE's website, so the news is principally about KDE.\n\nWhat's important to consider is that if you look at the biggest opponents of our editorial policies -- which, frankly, are Shawn and Neil -- they are on exact opposite sides of the spectrum, or at least it seems to me.  There is no way to please everybody.  I try to do my best job for the site.  At this I and the other editors may fail at times, but overall, I think we do a great job here, and the success of the site speaks for itself.\n\nThat said, I do not mean to say the site can't use a facelift.  For example, I would like to see a \"subsection\" of the part where stories which are in fact arguably stories (i.e., ignoring trolls and the many tech support questions we get) which are not approved by the editors (i.e., unmoderated, except to remove trolls) are listed, and having a list of current threads on the right side of the main page.  I would also like to see  \"KDE PR\" section where companies can post press releases, and these can also be seen in a sidebar.  And of course there are other things I would like to see, like user moderation so that we don't have to yank obvious trolls.  My problem is, I don't know, or care to learn, Python, so I have not seriously suggested these ideas, perhaps other editors agree with them or not.\n\nAs to the future, the site may be migrating to PHP (which my \"original\" site was based on, before I joined with Navindra), in which case I can contribute quite a bit more code for improvements, since, well, I have a lot of PHP code.\n\nHope this answers some questions.  I am very busy the next few days so please accept my apologies if I ignore this thread from here on out.\n\nDre"
    author: "Dre"
  - subject: "Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "There are a few things that should be clarified, especially since we have software in Koffice.  First regarding Aethera, a comment was made that an update hadn't been release since April 2001, this is totally un-true, there have been a number of updates and as recently as 2 weeks ago with another coming next week.  We spend a tremendous amount of resources on Aethera.  And Aethera is GPL as well.  These are all simple things to know, we put out press about all of it when it happens.\n\nNext a comment about Kivio and Kugar.  Kivio is a very stable and feature complete application, and doesn't really require constant work, sure it could always use more features, but it is a very popular application based on the enormous amount of sales we've had of our stencils.  The biggest problem we've had with Kivio being in KOffice is other people changing and moving things around without asking us.  We had someone do a whole sale slice and dice of Kivio last year that really broke it, and everyone kept denying that anything happened until I took extreme steps and we got it figured out.  At that time we decided to focus on Kivio mp for a while and see what we could learn that could be leveraged back to Kivio in KOffice and give KOffice some time to settle down.  There is a difference between a stable and mature application and one that has been abandonded.\n\nKugar has always been what it is, and that is a report viewer, if you format your data for it, it does very nicely, we wrote a kio_slave for Konqueror to be able to browse and view reports using Kugar and we were asked to donate it all to KOffice, which we did.  Again it is stable and done and needs no work, it could use a report designer if someone wants to write one, we just haven't had time to do it, but it doesn't make Kugar unusable.\n\nThe big argument given to us for putting things in KOffice is that all those little library changes and other infrastructure changes that cause applications to break would automatically be applied to the KOffice applications since they were part of the tree.  I don't know if this continues to happen or not, but from our perspective it is a little frustrating to donate a major piece of work like Kivio and then get dissed about it.\n\nShawn Gordon\nPresident\ntheKompany.com\n\n"
    author: "Shawn Gordon"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "I think theKompany.com is doing good work. I don't know how much people work at your Kompany, but you are always so quiet. The KDE community doesn't know what you're doing at the moment and we don't hear any (press) releases lately. Speak with others about what you're doing at the moment, communicate with KDE users/developers.\n\ntheKompany is difficult to compare with Ximian, because you are both doing something different. But Ximian communicates much more with the community then you guys do. A Kompany that speaks, will be heard :-) If you don't say anything at all, nobody knows you exist. Only a suggestion..."
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "Quiet?  We run press releases every 7 to 10 days usually, and recently we've had 3 and 4 a week at times.  We have news on Linuxtoday, newsforge, desktoplinux, linuxdevices, gui-lords, pclinuxonline, linuxorbit, various german, italian, french and UK web sites as well.  We've recently been covered in Linux Magazine in the states, Linux Format and Linux Magazine in the UK, Linux Magazine in Germany and some magazines in Italy and Norway.  About the only place that doesn't have any information on us is the Dot, and from what I can understand from the non-published publishing policies, the kind of press and news we have is not something that the Dot chooses to cover, which is certainly fine as they have to stick with their objectives as well.\n\nI just don't know how we can make more noise than we already are about the many things we have and are doing.  "
    author: "Shawn Gordon"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-02
    body: "Time for me to change browsing habits :-)\n\nWhen I look at the site I can see I was wrong about making releases. They sites you refered had some press releases indeed.\n\nThe software you make is very useful but isn't widely known to the Linux community. It seems to me that the software isn't installed on many computers. We have for example KDevelop which looks like KDE Studio (Gold) but it's free and has a consistent look in KDE. Auto completion is a good feature but I'm not going to pay for KSG because I really need that.\n\nPutting some free apps (thinking of Aethera, ...) in KDE CVS and let developers working on it, would create better software that fits in KDE nicely. Being a part of the standard KDE installation would promote theKompany and users would also benefit. Aethare may look nice (it is), but I don't find it intuitive. If it isn't easy to use, people will use other programs. But that's only my opinion...\n\nNote: The frontpage of your website has a dead link\n-> http://www.thekompany.com/ksg\nForgot the /projects part :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "> Kivio is a very stable and feature complete application, and doesn't really require constant work\n\nHave a look at http://bugs.kde.org/db/pa/lkivio.html, eg #27670 (247 days old) makes it pretty unusable."
    author: "Someone"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "That hardly makes it unusable, and based on the thousands of people using it, they would tend to agree.  I see a number of bugs here that are fixed, and some other things like \"not all arrow heads have been implemented\" - again, hardly makes it unusable.  Now perhaps there is a feature or function missing that makes it unusable for *you*."
    author: "Shawn Gordon"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "> I see a number of bugs here that are fixed, and some other things\n\nWhy aren't they fixed in KOffice \"branch\"? I appreciate that theKompany donated Kivio to KOffice but I dislike the attitude \"we don't want to have to do anything with it anymore\". You insist on not calling it abandonded, but this \"branch\" was obviously abandonded by you.\n\n> like \"not all arrow heads have been implemented\"\n\nSo if it is not implemented, it's definitely a bug that other arrow types can be selected."
    author: "Someone"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-01
    body: "You are misleading once again, I never said \"we don't want to have to do anything with it anymore\", what I said is that we decided to step back from it for a time and perfect some technology applicable to Kivio and let KDE3 come out and KOffice to stabalize a bit more.  We've just recently solve some very interesting problems in Kivio mp with regard to rotation, distortion, text, connectors and such.  My plan is to pick up KOffice Kivio development in the next 2 to 3 months and see what we can leverage back into it."
    author: "Shawn Gordon"
  - subject: "And now for the *other* face of Shawn Gordon"
    date: 2002-03-03
    body: "http://dot.kde.org/999199700/999274963/\n\nSee #13 for his view on KOffice."
    author: "Neil Stevens"
  - subject: "Re: And now for the *other* face of Shawn Gordon"
    date: 2002-03-03
    body: "Neil your single minded dislike of us and Trolltech is really just a head scratcher.  What is factually wrong about that statement that you point to?  You may not like what I said, and certainly my comment about \"never catching up\" is my opinion, but seriously, how can an office suite that has a handful of developers or less working on it part time go head to head?\n\nI even tried using a current KWord to write the manuals for HancomOffice (how's that for support) but every time I tried to embed a graphic in the document, it crashed, so I had to give up on that attempt.\n\nNow certainly the work and output of David Faure is nothing short of stunning, and based on the huge number of Kivio stencils we've sold, I'm assuming that our Kivio application in KOffice is popular.  Certainly StarOffice and OpenOffice don't have anything like Kivio, and the psuedo office suite GnomeOffice doesn't, the Dia people explicitly didn't want their application in there.  \n\nFor Gods sake Neil, we contribute to KOffice - so take a breath, step back and gather some perspective.  Now I'm off to take the kids to visit grandma."
    author: "Shawn Gordon"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-05
    body: "This is an interview, so people answered what they thought is true, which might be different from what is true.\n\nI was not aware of any new release of Aethera since april 2001, and this was also the case for all the people in the interview (3 koffice developers, 2 kde developers, 1 user). Despite all your press releases, it looks like everyone knows about Aethera except KDE people. If someone had asked me an outlook-like suit I would have pointed him to Evolution instead of Aethera because I didn't know Aethera was out and maintained. So there is a communication problem. I only read the dot and linuxfr, and yet I know about every release of Evolution.\n\nFor the other apps, Kugar and Kivio, there hasn't been any commit from someone of the kompany for 11 month (except one 9 month ago to change the icon). So it looks very much _abandoned_ to me. I find it hard to believe that you are selling an application that haven't had any bug fixed or any tiny feature added in 11 month. But looking at your website, it looks like it is the case. Well, congratulation. If you wrote an app that was perfect at its first release, you should really give an interview as you would be the first one ever in the software industry. :-))\n\nThat said, if the application are already doing their job, it is ok for me. After all, I am using plenty of non maintained applications. But I maintain my statement : Kugar and Kivio are not maintained.\n\nThe \"little library changes and other infrastructure changes that cause applications to break\" have however been applied by the KDE crew and those two apps compile along with the KDE releases.\n\n\n"
    author: "Philippe Fremy"
  - subject: "Re: Some misleading information in the KOffice intervi"
    date: 2002-03-05
    body: "If you look at theKompany's webpage more exactly you will see that they sell Kivio *MP* which is maintained and not identical with Kivio/KOffice.is maintained."
    author: "Someone"
---
For those of you waiting for KDE news from <A href="http://dot.kde.org/1007746713/">FOSDEM</a>, <a href="mailto:kaper@kde.org">Rob Kaper</a> wrote in with a <a href="http://capsi.com/~cap/digicam/2002-02-18-fosdem/">photo impression</a> of the event. In addition, Philippe Fremy and Thomas Capricelli have provided us with <a href="http://www.freehackers.org/fosdem2002/index.html">their own report</a> including links to their interviews with FOSDEM organizer <a href="http://www.freehackers.org/fosdem2002/raphael.html">Raphael Bauduin</a>, Python author <a href="http://www.freehackers.org/fosdem2002/guido.html">Guido van Rossum</a>, the <a href="http://www.freehackers.org/fosdem2002/koffice.html">KOffice team</a>, and <a href="http://www.freehackers.org/fosdem2002/photos/">more photos</a>.  Enjoy.
<!--break-->
