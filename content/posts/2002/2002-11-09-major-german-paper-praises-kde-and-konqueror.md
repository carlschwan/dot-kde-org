---
title: "Major German Paper praises KDE and Konqueror"
date:    2002-11-09
authors:
  - "mmoeller-herrmann"
slug:    major-german-paper-praises-kde-and-konqueror
comments:
  - subject: "full of praise?"
    date: 2002-11-09
    body: "Hmm, full of praise? :-))\n\nThis 2. report \"Journey into the center of the hell\" is all over full of ironic but veeery well written. Its about a Linux installation, experiences with the community, problems with Samba... and some (positve) lines are related to KDE. Basically, what he writes is like this: \"Live can be easy, but now I want to lean Linux\" :-)\n\nIts worth reading but don't try to auto-translate: Babelfish may like vogon poetry but no ironic German (example:) \"Yes, we old Linux Fexe. Us nevertheless none makes which forwards. Gewieft as we are. The Wizzards OF OS even!\" :-))\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: full of praise?"
    date: 2002-11-09
    body: "Oh, here is part 1.\n\nhttp://www.sueddeutsche.de/index.php?url=/computer/neuetechnik/software/54600&datei=index.php"
    author: "Thorsten Schnebeck"
  - subject: "Re: full of praise?"
    date: 2002-11-09
    body: "That part is even better. He has described the installation in 9 steps. The first 8 steps are his ideas what could went wrong. In the last step he started the installation and discovers that it works and that KDE even make sense :)"
    author: "Adrian"
  - subject: "Re: full of praise?"
    date: 2002-11-09
    body: "Which document should we link?  computer/neuetechnik/software/56593 or /computer/neuetechnik/software/54600&datei=index.php or computer/neuetechnik/software/56570&datei=index.php?  I'm not sure what the difference is."
    author: "Navindra Umanee"
  - subject: "Re: full of praise?"
    date: 2002-11-10
    body: "http://www.sueddeutsche.de/index.php?url=/computer/neuetechnik/software/54600&datei=index.php\n is the link starting with Part 1 of the whole story."
    author: "Johann Lermer"
  - subject: "Re: full of praise?"
    date: 2002-11-09
    body: "The critical remarks do not concern KDE. In fact KDE gets most of the praise. The unix parts (installing and configuring samba and netscape-7.0) are daunting for Graff."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: full of praise?"
    date: 2002-11-10
    body: "Man o man I am new at this but linuxs had my attention for some time now. As a Wincat with many flee days I am glad i decided to buy the 8.1 version. With the KDE, oooh man, move over 6.0 because you are history to me. Now to start having fun playing and learning more about this Now and Future OS system. Mr. Schnebeck you are definitly in the groove. Because this new OS is for Crossovers, Newbees, Advance and Profis alike."
    author: "Ralph Robinson"
  - subject: "Only in the online edition?"
    date: 2002-11-10
    body: "I read their report a couple of weeks ago, only online. \nThere was just a small teaser in the printed edition, on page 1. "
    author: "anon"
  - subject: "Doesn't suprise me"
    date: 2002-11-27
    body: "Don't we all know that using KDE is easy (-er!) than Windows, the internal logic of KDE is nowadays very-very strong. Someone with some technical inclination (journalist writing for \"Neue Technik/Software\" section) should see that. And, what's more, installing SuSE8.1 is an absolute nobrainer. But, don't you forget: how many (mum/dad) Windows users have ever _installed_ Windows?? I have even heard of Win users who bought a new computer when, after two years of using Windows X-whatever, the performance was completely down the drains."
    author: "VON"
---
The big German newspaper <a href="http://www.sueddeutsche.de/">Süddeutsche</a> (400000+ copies sold every day) has published <a href="http://www.sueddeutsche.de/index.php?url=/computer/neuetechnik/software/54600&datei=index.php">a 10-part installation report</a> (German) of <a href="http://www.suse.com/us/private/products/suse_linux/i386/index.html">SuSE Linux 8.1</a>. The author, who has no experience with Linux and KDE whatsoever, is full of praise for <a href="http://www.kde.org/">KDE</a> and <a href="http://www.konqueror.org/">Konqueror</a>. Other projects such as the <a href="http://www.gimp.org/">Gimp</a> and <a href="http://www.openoffice.org/">OpenOffice.org</a> are also mentioned positively. 
<!--break-->
<p>
Roughly translated excerpts:
<p>
"... Anyone who is accustomed to GUIs by working with Mac or Windows, will get along with KDE quite well."
<p>
"... Konqueror, the universal talent, also as web browser, kmail and knode, the email program and newsreader for KDE, are top notch, did I mention that? ..."
<p>
"... I would like to mention that konqueror browser, an astounding web browser, file manager and file viewer, all in one, that can access the web, windows partitions and of course linux files without any problems..."
<p>
"...Konqueror is great for surfing, K-Mail, the Email program for KDE is a good mailer. Knode collects News."