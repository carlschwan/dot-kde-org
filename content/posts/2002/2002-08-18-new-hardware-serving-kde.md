---
title: "New Hardware Serving KDE"
date:    2002-08-18
authors:
  - "dmolkentin"
slug:    new-hardware-serving-kde
comments:
  - subject: "Nice machine"
    date: 2002-08-18
    body: "Wow, nice machine guys.  Thanks, IBM, for helping out the community!"
    author: "jmalory"
  - subject: "and thanx suse ! "
    date: 2002-08-18
    body: "and thx suse ! people often forget suse, think that they are anti-gpl ... but that isn't true ! they did great work with kde and alsa !! :-)"
    author: "daniel"
  - subject: "I miss K-Town"
    date: 2002-08-18
    body: "...as the US service families would call it.  My brother and I swapping stacks of C=64 games in Kaiserlautern were probably the highlight years of my life.  Late nights of Epyx games and Jumpman come swarming back from my memories. \n\n::sniff::"
    author: "b-boy"
  - subject: "Re: I miss K-Town"
    date: 2002-08-21
    body: "Oh yeah....and playing Bordello on the Speed of Light BBS out of Ramstein at 2 am on night shift.  And the potent cappucinos and ameretto sundaes at the Cafe Rialto.  Not to mention the Laser Discotech in Landstuhl.  God that place was great.  Movie theatres that serve beer!  What I'd give for a bottle of Diebels Alt right about now..."
    author: "Rick"
  - subject: "Amazing.."
    date: 2002-08-19
    body: "I have just one thing to say: IBM, you rock!"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "IBM and Linux in general :)"
    date: 2002-08-19
    body: " Thanks a BUNCH IBM :) and also thank you for showing the \"IBM server with Linux\" add on TV! Its nice that someone makes sure that even the most un-enlightened sysadms out there cant say they havent heard that Linux is ready for primetime. Now we just need to make them understand that KDE brings it to the desktop too :)\n\n I hope you have plans for releasing a KDE/GNU/Linux based IBM Workstation some time soon :) Id love to own a workstation that is intended to run Linux from the day it leaves the plant to the day it is taken offline to go to my \"museum\".\n\nWay To Go (TM)!!!\n/kidcat\n\nPS.: *kudos* to SuSE as well.. no doubt about that!"
    author: "kidcat"
  - subject: "Re: IBM and Linux in general :)"
    date: 2002-08-21
    body: "Yes sir!  That ad rocks!  And thank you IBM and SUSE\nand Dirk Mueller,  too!!"
    author: "Rick"
  - subject: "And an PIII 1.2Ghz?"
    date: 2002-08-21
    body: "Now IBM is really in trouble after shoping \naround the world. You can hardly find any\none selling PIII in asian market!"
    author: "ac"
  - subject: "Re: And an PIII 1.2Ghz?"
    date: 2002-08-21
    body: "Servers are different than desktop machines, Mr. Coward.  The CPU is seldom the bottleneck on a file, mail or web server.  This cpu can probably outpace the bandwidth and file system with little problem.  The only thing adding a P4 would do is make for a hotter machine drawing more power.  More memory and faster disk i/o is much more important.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
---
In the past, the KDE infrastructure has several times suffered from extended downtime of our websites and mailinglists. That was due to the fact that master.kde.org ailed from old and failing hardware. Forget the past, and welcome our new master: <a href="http://ktown.kde.org/~danimo/ktown-front.jpg">ktown.kde.org</a>!
<!--break-->
<p>ktown, a xSeries x232 kindly donated by <a href="http://www.ibm.com/">IBM</a> is located at the <a href="http://www.uni-kl.de/">University of Kaiserlautern</a> in Germany. As everyone loves specs, here they are:
<ul>
<li>PIII 1133MHz CPU</li>
<li>768 MB RAM</li>
<li>RAID5 68GB disk array (1 hot spare disk)
<li>directly connected with 100MBit/s</li>
</ul>
</p>

<p>ktown is now the new server behind the well-known address master.kde.org and hosts several KDE websites as well as the mailinglists. The old master will from now on serve for backup purposes. The basic setup was done at the SuSE offices (<a href="http://ktown.kde.org/~coolo/ktown/">Photos</a>) before the server was shipped to Kaiserslautern where Dirk Mueller did the final hardware works.</p>

<p>Thanks go to:
<ul>
<li><a href="http://www.ibm.com/">IBM</a> for donating the hardware</li>
<li><a href="http://www.suse.com/">SuSE</a> for doing <a href="http://ktown.kde.org/~coolo/ktown/">large amounts of the setup</a></li>
<li><a href="http://www.uni-kl.de/">The University of Kaiserslautern</a> for providing ktown's connection</a></li>
<li><a href="http://www.uni-tuebingen.de/">The University of Tuebingen</a> for hosting KDE's heart for so long</a></li>
<li>and of course all the people involved in the administrative efforts around the migration</li>
</ul>
</p>