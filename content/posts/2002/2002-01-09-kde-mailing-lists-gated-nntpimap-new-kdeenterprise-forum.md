---
title: "KDE Mailing Lists Gated To NNTP/IMAP, New KDE::Enterprise Forum"
date:    2002-01-09
authors:
  - "numanee"
slug:    kde-mailing-lists-gated-nntpimap-new-kdeenterprise-forum
comments:
  - subject: "kde-cvs splitted"
    date: 2002-01-09
    body: "The best thing about this is that they've splitted up the kde cvs mailing list so you can watch the commits for your favourite project only"
    author: "harryF"
  - subject: "Great!"
    date: 2002-01-10
    body: "I really hate reading mailing lists.. the news format is much better IMHO. (to bad it's read only)"
    author: "JRM"
  - subject: "Re: Great!"
    date: 2002-01-16
    body: "> I really hate reading mailing lists.. \n> the news format is much better IMHO. \n>(to bad it's read only)\n\nI am working on solving that. Please let me know which specific groups you would like to have post access on and I will try to get those first.\n\n-Chris"
    author: "Chris Molnar"
  - subject: "Anonymous IMAP problem here..."
    date: 2002-01-10
    body: "KDE Page:\n\n> Anonymous IMAP is available at the same server. Set login to 'anonymous, password 'you@email.address', folder to '#news' and server to 'news.uslinuxtraining.com'.\n\nWhen I add #news to prefix directory in KMail it says:  \"Could not enter directory\"\n\nimap://anonymous@news.uslinuxtraining.com:143/#news/;TYPE=LIST\n\nWhat am I doing wrong?  (Insert new brain is not an option ;)"
    author: "Geoff Rivell"
  - subject: "Anonymous IMAP problem here..."
    date: 2002-01-10
    body: "You are most likely using a too old version of KMail.\nSorry, there was just lask week a problem fixed in CVS to support news groups on this special server that didn't become visible before."
    author: "Michael H\u00e4ckel"
  - subject: "Re: Anonymous IMAP problem here..."
    date: 2002-01-10
    body: "> You are most likely using a too old version of KMail.\n\nKMail that came with KDE 2.2.2.  So yep.\n\nOH well, I'll wait for Debian to include the fix (lazy me)."
    author: "Geoff Rivell"
  - subject: "Re: Anonymous IMAP problem here..."
    date: 2002-01-13
    body: "damn, i've got the same problem"
    author: "domi"
  - subject: "kde-core-devel"
    date: 2002-01-10
    body: "is it just me or is there no kde-core-devel on that server ?"
    author: "ik"
  - subject: "Re: kde-core-devel"
    date: 2002-01-10
    body: "Try this: nntp://news.uslinuxtraining.com/kde.kde-core-devel\n\nYou can type that directly into the location of konqueror if your newsreader is buggy."
    author: "ac"
  - subject: "Re: kde-core-devel"
    date: 2002-01-11
    body: "i was using knode. seems kde-core-devel came later, now i did 'refresh group list'\nand kde-core-devel shows up.\nthanks anyway"
    author: "ik"
  - subject: "nntp://news.kde.org ?"
    date: 2002-01-12
    body: "Great !\nGorgeous !!\n\nWhen will we get a nntp://news.kde.org ??\nIt would be so nice to be able to discuss KDE right here !\n(html forums are nice but so heavy...I'd love to use Knode !!)\n\nG."
    author: "Germain"
  - subject: "Re: nntp://news.kde.org ?"
    date: 2002-01-12
    body: "Why? If you want to post why not use the KDE newsgroups in Usenet?\n\n comp.windows.x.kde        The K Desktop Environment. (english)\n de.comp.os.unix.apps.kde  Das K Desktop Environment. (german)\n\nThey should be out there on every news server near you."
    author: "someone"
  - subject: "Re: nntp://news.kde.org ?"
    date: 2002-01-12
    body: "well... \nIt seems that newsgroups are slowly but surely gliding toward a large number of small dedicated newsservers rather than this bloated awful beast of shared namespace, where a cow wouldn't find it's calf...\n\nThere are a couple of good reasons for that :\n1) an url such as news.kde.org is actually far simpler to recall than comp.tr.deadly.burried.projects.kde.\n\n2) there are no standard way of classifying a topic inside the shared tree (e.g: why is it comp.WINDOW$.x.kde, and not comp.unix.kde? comp.unix.cde exist, and it is a quite similar subject... and why linux.debian.gtk.gnome ? :-)\nso you often end with 3 or 4 newsgroups for the same topic, and you cross-post to all because, indeed, all claim to be THE most relevant newsgroup.\nIn contrast, you KNOW that news.kde.org is the most relevant place.\n\n3) It takes ages, and a lot of will, and a lot of efforts to have ONE newsgroup opened and widely spreaded, so you end with just one big KDE group. In contrast, a dedicated server may have 10th of groups - with fine grained topics."
    author: "Germain"
  - subject: "Re: nntp://news.kde.org ?"
    date: 2002-01-13
    body: "Imminent Death of Usenet Predicted!  PNGs at 11."
    author: "Neil Stevens"
  - subject: "Then can I access them in groups.google.com too ?"
    date: 2002-01-19
    body: "This will be really neat, as this is already possible for Mozilla mailing lists ! the Linux kernels and many other projects too ?"
    author: "khalid"
  - subject: "Re: Then can I access them in groups.google.com too ?"
    date: 2002-01-20
    body: "No."
    author: "someone"
---
Thanks to the efforts of <a href="mailto:molnar@kde.org">Christopher Molnar</a>, KDE mailing lists are now accessible read-only through NNTP at news.uslinuxtraining.com (<a href="nntp://news.uslinuxtraining.com/">news</a>, <a href="http://news.uslinuxtraining.com/main.php?template=kde">web</a>).  In other words, KDE Mailing Lists are now neatly accessible through your favourite newsreader (<a href="">KNode</a>, <A href="http://www.tin.org/">tin</a>).  The lists are also available through anonymous IMAP at the same server.  See the official <A href="http://www.kde.org/mailinglists.html">mailing list page</a> for more details. In related news, Jono Bacon wrote in to point us to the new <A href="http://enterprise.kde.org/forum/">KDE::Enterprise Forum</a> over at <a href="http://enterprise.kde.org/">KDE::Enterprise</a>.  The forum provides a place for discussions regarding the use of KDE within businesses, education, charities, and so on.  Enjoy.