---
title: "People of KDE: Neil Stevens"
date:    2002-03-11
authors:
  - "wbastian"
slug:    people-kde-neil-stevens
comments:
  - subject: "Go Neil Go!"
    date: 2002-03-11
    body: "I really enjoyed this interview, and a lot of thanks goes to the nicely revamped question set by Tink.  Great job!\n\nAs to Neil, I think you're crazy for using a Monitor resolution nobody else sane has ever heard of. That Noatun plugin in your screenshot sure looks intriguing.  And why is there no mention of freekde in this Interview?  You *again* lose a chance to market this site.  :-P  Be sure to drop us an email so that we can announce its return, btw.\n\nTink: Could you exert some of your widespread KDE influence and get Dre back on the list of interviewees??  I'd really like to know who this Dre guy is anyway.\n\nCheers,\n-N.\n"
    author: "Navindra Umanee"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-11
    body: "I have found 1152x864 to be the best resolution for 17\" monitors... try it out!"
    author: "will"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-11
    body: "I have to agree, that's what I use on my 17\" as well (me too!)"
    author: "KDE User"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-11
    body: "Here I'm happier with 1600x1200 on my Dell D1025TM and 1280x1024 on my Sony Multiscan17sf II (lower on that one because it doesn't support 1600x1200) with a Radeon 8500.  That's just me though.  I suppose my eyes will be fried before I know it."
    author: "Bob Raymond"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-11
    body: "Same for me, I love to have 1152x864 on my Samsung 17\"\n\n1024x768 is too small and 1280x1024 makes everything tiny ;)"
    author: "mETz"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-11
    body: "G400 with two 17\" monitors doing 1152x864 here! Highly recommended."
    author: "reihal"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-12
    body: "If FreeKDE had been up at the time, I'd have found *some* way to market it.  As it is, I got the domain mentioned at least twice - loooona, and my desktop screenshot. :-)\n\nThe one in the screenshot is Hayes, which has been enabled and now disabed again for shipping in KDE 3.0 itself.  so, once things have absolutely frozen I'll go and make final tarballs of everything on my site, and that will include Hayes."
    author: "Neil Stevens"
  - subject: "Sun!"
    date: 2002-03-12
    body: "AFAIK it's default resolution off lesser (17\"?) SUN monitors."
    author: "fura"
  - subject: "Re: Go Neil Go!"
    date: 2002-03-12
    body: "\"Tink: Could you exert some of your widespread KDE influence and get Dre back on the list of interviewees?? I'd really like to know who this Dre guy is anyway.\"\n\nYes I agree. We need a good interview of that Dre character. ;-)\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Questions.."
    date: 2002-03-11
    body: "The new questions are great Tink! Good work! :)"
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Nice"
    date: 2002-03-11
    body: "I really like the interviews,\nit is interesting to know some more about the people behind the job.\n(And it's a job well done, believe me)\nHehe, something good about belgium.\nThe beer! Luckily most people don't know\nmuch more then the beer ;-)\nMaybe we should donate belgian beer to\nthe KDE developers?\n"
    author: "blashyrkh"
  - subject: "Re: Nice"
    date: 2002-03-11
    body: "I can't get enough Belgian beer. Luckily I live in Belgium :-)\n\nI want to see more Belgian developers... Michael Goffioul (KDE Printing) is doing a very good job!"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Nice"
    date: 2002-03-12
    body: "And don't forget the waffles!  Beer and waffles, not a bad start, but certainly beer, waffles and a handful of KDE developers would be a better combination!  :-)\n\nAnyway, I guess I'm more of a fan of Mexican beer, so for me I'll stick to the waffles.  :-)\n\n-Scott"
    author: "Scott Wheeler"
  - subject: "\"without any particular reason whatsoever\""
    date: 2002-03-11
    body: "I hope it's just a bad coincidence that this guy get's honour and more attention nowadays."
    author: "Anonymous"
  - subject: "Re: \"without any particular reason whatsoever\""
    date: 2002-03-11
    body: "We al get the attention we deserve, even you.\n\n\n--\nTink"
    author: "Tink"
  - subject: "Re: \"without any particular reason whatsoever\""
    date: 2002-03-11
    body: "What about me?"
    author: "Starved of attention?"
  - subject: "Re: \"without any particular reason whatsoever\""
    date: 2002-03-13
    body: "here's some"
    author: "alex"
  - subject: "Re: \"without any particular reason whatsoever\""
    date: 2002-03-12
    body: "The Neil Stevens controversy continues over at Slashdot and on the lists:\n\nNeil's tirade against Dirk Mueller:\nhttp://www.newsforge.com/article.pl?sid=02/03/09/224213&mode=nocomment\nDirk Mueller's response:\nhttp://www.newsforge.com/article.pl?sid=02/03/10/149245\nWaldo Bastian's take on it all\nhttp://slashdot.org/comments.pl?sid=29227&cid=3136608\n\nSomeone (not me) with a back & forth with Rob Kaper defending Neil:\nhttp://slashdot.org/comments.pl?sid=29227&cid=3136623"
    author: "dingledongle"
  - subject: "Re: \"without any particular reason whatsoever\""
    date: 2002-03-13
    body: "Bah! (Score:5, Interesting) \nby Anonymous Coward on Saturday March 09, @10:10PM (#3136623)\n-----------------------------------------------------------------\nThis guy has been moaning about KDE since ages. He may contribute to KDE a bit, but by God he pisses off nearly every developer.\n\nlists.kde.org and dot.kde.org are where he trolls most.\n\nHe has:\n\nCriticized *many* KDE developers good work, even though they are working for free in their spare time.\n\nWould rather see Microsoft go off scott free and end up killing KDE than have Microsoft be punished for being a monopoly.\n\nHas sabotaged KDE CVS because people didn't agree with him.\n\nHe now wants to lead KDE.\n\nThe guy has an agenda to cripple KDE anyway he can, by sowing discord and criticizing everyone. He shows no respect for peoples work and never apologizes even when he is completely wrong. Its a miracle KDE has put up with him so far."
    author: "PrettyAccurate"
  - subject: "Re: \"without any particular reason whatsoever\""
    date: 2002-03-18
    body: "You mean like in that Asterix episode \"La Zizanie\" ?"
    author: "reihal"
  - subject: "But the question we really want the answer to is.."
    date: 2002-03-11
    body: "Does it ever happen that his cigarette sets light to the ashtray?\n\n:)"
    author: "cbcbcb"
  - subject: "Re: But the question we really want the answer to is.."
    date: 2002-03-12
    body: "Don't smoke, never have, never will. :-)"
    author: "Neil Stevens"
  - subject: "we really want the answer to is.."
    date: 2004-03-04
    body: "rich man needs......poor man her........if you eat........you will die"
    author: "NAVEEN"
  - subject: "Re: we really want the answer to is.."
    date: 2004-03-19
    body: "The answer is NOTHING\n\nThe rich man needs NOTHING\nThe poor man has NOTHING and \nif u eat NOTHING u will die"
    author: "angel"
  - subject: "heh"
    date: 2002-03-11
    body: "nice interview with one of the more contraversial (re: slashdot) developers (not saying that's a bad thing--- it's really quite cool).\n\ncode on neil :)"
    author: "fault"
  - subject: "I agree. ;-)"
    date: 2002-03-12
    body: "Monopoly, Pepsi,pizza, dark, belgian beer, tabs.\n\nI agree! ;-)"
    author: "Rob Kaper"
  - subject: "Re: I agree. ;-)"
    date: 2002-03-12
    body: "Well, considering I was referring to you when it comes to the beer, I hope you do.\n\nI also hope you agree with Atlantik :-)"
    author: "Neil Stevens"
  - subject: "Neil is the best!"
    date: 2002-03-12
    body: "I really enjoyed Necronomicon!  Keep up the good work!\n\nIf at least one person chuckled, it was worth it.  And that's including myself."
    author: "Anonymous Troll"
  - subject: "Re: Neil is the best!"
    date: 2002-03-12
    body: "That is funny.\n"
    author: "ac"
  - subject: "Aaron interview?"
    date: 2002-03-12
    body: "Has the KC KDE guy been interviewed yet?\n"
    author: "ac"
  - subject: "KDE Developer Location Map"
    date: 2002-03-14
    body: "Really cool feature! But it is nowhere near complete at the moment, I guess..."
    author: "Loranga"
  - subject: "Re: KDE Developer Location Map"
    date: 2002-03-14
    body: "Nifty little conincidence:  Chris Howells has now started another push to flesh out the map:\n\nhttp://worldwide.kde.org/map/"
    author: "Neil Stevens"
  - subject: "Re: KDE Developer Location Map"
    date: 2002-03-14
    body: "Cool, just wonder which KDE developer who lives at 0' long, 0' lat... :) (Check out the large map with names (which currently doesn't show any names...))"
    author: "Loranga"
  - subject: "pfft"
    date: 2002-06-12
    body: "Maybe someone should show this guy XMMS and mplayer... both using gtk-frontends, and offer a far better multimedia experience than No-cantdo-tun."
    author: "Vitamin C"
---
Did you ever, without any particular reason whatsoever, wonder "Who is Neil?"
Despair no longer, for in this week's installment of
<a href="http://www.kde.org/people/people.html">The People Behind KDE</a>, <a href="mailto:tink@tink.cc">Tink</a> came up with a whole new
set of questions to find out.
To learn what <a href="mailto:neil@qualityassistant.com">Neil</a> wears inside
and outside his bed or what his plans are for <a href="http://noatun.kde.org/">Noatun</a>, jump straight to the 
<a href="http://www.kde.org/people/neil.html">interview</a>.
<!--break-->
<br><br>
<b>Newsflash:</b><br><br>
Starting this week, the series has a set of new questions, as well as some new features.
The 
<a href="http://www.kde.org/people/urls.html">url page</a>
 is back, all the favorite urls of the People will be posted there so it's easy to access if you have a dull moment and want to surf all the favorite
urls.
Hope you enjoy the new questions and feel free to suggest new interview victims. ;-)