---
title: "First Alpha of KDevelop 3.0 (aka Gideon) is Out"
date:    2002-10-13
authors:
  - "ctennis"
slug:    first-alpha-kdevelop-30-aka-gideon-out
comments:
  - subject: "Bizzarro world day"
    date: 2002-10-12
    body: "I must have gone to some weird inverted dimension where everyone has long hair in the neck and the Keramik style is the ideal all UI-designers strive for :)\nI really don't mean to offense the author, or start a flamewar, but this has been bothering me since I first saw a screenshot of this style. Is Keramik still planned to become the default style for KDE 3.1(style and decoration)?\nI haven't really said anything about Keramik before because I didn't really want to offend anyone, I just switched to GNOME when I first heard that Keramik would become default, and figured the end is nigh for the KDE project. (I'm now a happy KDE user again) But if everyone just sits and pretends that everything is fine, the project wont evolve, so even if my opinion is useless, It still had to be said, and the screenshots of KDevelop 3.0 just happened to remind me. However, Keramik really seems to be popular among the majority of KDE users, and I guess that's all that really matters.\nBtw, the nicest window decoration I've seen ever (IMHO), is this one: http://kde-look.org/content/show.php?content=2865&PHPSESSID=44c18ce5f7c406e5f81a1d7f7c10537e\nI really hope this one get's included in future KDE releases."
    author: "Lof"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-12
    body: "> I just switched to GNOME when I first heard that Keramik would become default, and figured the end is nigh for the KDE project.\n\nA fool you are."
    author: "Yoda"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "I didn't really think that the end was nigh :) I just got a bit carried away in the post, and as for GNOME, I just needed to try something different for a while.\nPerhaps I went a bit over the top, Keramik isn't really THAT bad, it just clashes real bad with my taste.\nSince the Crystal icon theme has begun to look really good, I guess there's still time for some improvement's on the Keramik style. "
    author: "Lof"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: ">  it just clashes real bad with my taste.\n\nYeah, I don't like keramik-like styles much either. I use the light series most of the time.\n\nHowever, just because it's (a) default (through kpersonalizer) doesn't mean you have to use it. This is why KDE is customizable. \n\nPeople who say that the default style of themeable program XXX sucks, so they refuse to use XXX are pretty silly :)  "
    author: "fault"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "> However, just because it's (a) default (through kpersonalizer)\n\nIt's not default in kpersonalizer. It has been for a short period, but no more."
    author: "anonymous"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-12
    body: "Ah fuck off:)\n\nThe author."
    author: "The author"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-12
    body: "I must add, despite looking really XP-ish, the Connectiva Crystal Icon theme have really begun to shape up, I used to hate it, but now It's becoming very very nice :), just take a look at some of the \"new\" device icons, and the kscd and noatun icons for some examples of some beautiful icons (Crystal 0.7)."
    author: "Lof"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "hi,\nlast week i started teaching linux in an class. how should i teach these windowsonly users linux?\nof course we began with history, licence, harddiscpartitions etc after installing suse linux and playing araound a little bit i wanted to go to the console. no, i didnt start with vi. first motivate these people!\nok. teaching console handling plus tar, unzip,, rpm, cp, rm etc but how: with boring archives?\nok going to www.kde-look.de downloading styles (yes even the kde-xp style) and doing this stuff with the; Whoo. thats like we are used to do!\n\nand YES: a cool looking desktop is IMPORTANT for newbies (and for me too).\nevery linuxguro can easely switch to a clean style, but the normal user wants this.\n\nAND: i like it very much. yes one important reason to compiling kde 3.1beta was  the keramik style.\n\nin short: keramik and crystal REALLY ROCKS!!!\n\n-gunnar "
    author: "gunnar"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "Yeah, the new push towards highly prettified user interfaces is a real problem IMHO. Exactly what do transparent menus achieve, apart from making them harder to read? What do the great bloblike buttons achieve except take up more real-estate and introduce extra graphics that distract the eye?\n\nI'm really glad they're still shipping the .NET style. Keep it simple, and I'm happy :)\n"
    author: "Richard"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "Uh.... \nI believe KDE will continue to\ndeliver simple styles like the 'light style' or the .NET-style\nThere is a whole bunch of simple styles available. All of them\ndo not contain 'overload' like transparency and this stuff...\nThis means: By making the gui style a plugin, KDE has choosen a\nvery flexible way in handling user preferences.\nIf you choose a light style, this 'overload' (in your opinion) gets\nnot only deactivated. Better: The style-plugin you choose will be\nfaster and less memory-consuming. So it's up to the user..."
    author: "Thomas"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "Actually, speed isn't the best reason to choose a style. .NET is pretty fast, but from what I've seen (assuming you have enough RAM to run KDE comfortably) Keramik is just as fast. It's slightly faster than even Qinx."
    author: "Rayiner Hashem"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-13
    body: "This is really silly. You seem to be intelligent enough to switch between GNOME and KDE but you're not able to switch to another style plugin in KDE??? "
    author: "Bausi"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-14
    body: "I really like keramic. The only \"not so good\" stuff of the theme are the buttons, which are a bit too proeminent, and the window decoration. I wish the window dec was somewhat less complex. I've replaced it with MKUltra (similar to web window dec) which is ok, but not perfect.\n\nThe combination keramic + mkultra + crystal icons (this latter misses some smaller 16x16 icons) has been my favourite for quite some time now.\n"
    author: "zelegans"
  - subject: "Re: Bizzarro world day"
    date: 2002-10-14
    body: "Keramik is not pretty, you're right.  But you're not forced to use it.  The highcolor KDE default style, which IMO is fantastic, is still there.  So are a bunch of others, if you fancy them."
    author: "KDE User"
  - subject: "Re: Bizzarro world day"
    date: 2003-01-04
    body: "Holy crow, dude.  It's not like you can't CHANGE the default back to something less fancy.  Calm down!\n\nGNOME has a lot of good points that KDE can learn from... for one, I much prefer the way GNOME puts disk icons on the desktop only when they're mounted, instead of having those silly mounted/unmounted icons on the KDE desktop.  Nautilus also starts up and runs a lot faster than Konqueror.  But you know what else?  GNOME2 is sooo much less configurable than 1.4 was, and common dialogs like the fileselector aren't nearly as easy to use as the KDE dialogs.  So, KDE is still the best in my opinion.\n\nBTW, is the windec you're referring to MKUltra?  Because, it's my fav. as well."
    author: "stunji"
  - subject: "Re: Bizzarro world day"
    date: 2003-01-04
    body: "FYI, in 3.1 you can make the desktop show only the mounted icons - see Desktop -> Behavior in KControl. \n"
    author: "Sad Eagle"
  - subject: "Re: Bizzarro world day"
    date: 2003-01-06
    body: "Sweet!  I am waiting and waiting and waiting for a stable 3.1... (!)"
    author: "stunji"
  - subject: "Eclipse"
    date: 2002-10-13
    body: "I must admit that I haven't looked at Gideon yet, but if you want to see some really impressive OpenSource IDE (for Java), look at Eclipe. You can find it at www.eclipse.org. It suppports fun stuff like real-time parsing of the code for very intelligent code completion and error detection, or refactoring support that can do things like re-naming functions (in the class that implements it AND all code that uses it). "
    author: "Tim Jansen"
  - subject: "Re: Eclipse"
    date: 2002-10-13
    body: "Yeah, but when will it be usable with C++?"
    author: "tevo"
  - subject: "Re: Eclipse"
    date: 2002-10-13
    body: "They are working on supporting C/C++, but I doubt that they will be able to implement all the features that the Java mode has. The advantage of a small, limited and very restrictive language is that parsing is fast and relatively easy. "
    author: "Tim Jansen"
  - subject: "Re: Eclipse"
    date: 2002-10-21
    body: "you'd forgotten the most important point: _slow_ language (make it easy to debug in real-time, nor?)"
    author: "Ruediger Knoerig"
  - subject: "Re: Eclipse"
    date: 2002-10-13
    body: "If you want a *really* impressive IDE (or whatever you call it), check Together\nI can promise you that you will change your mind about what is a great IDE\n\nhttp://togethersoft.com\n\nTogether is not free software, but you can download a free version just to play with it"
    author: "Tanguy Krotoff"
  - subject: "Re: Eclipse"
    date: 2002-10-13
    body: "I must admit that I haven't looked at Together for over a year, but I dont like UML or other graphical representations.\nThey fail when the system gets more complex. Who needs a graphical representation when you would need a 200 inch screen to see everything? I much prefer to use text. You can read it top down, you can easily search without getting lost, you can display the differences between two versions and so on..."
    author: "Tim Jansen"
  - subject: "Re: Eclipse"
    date: 2002-10-13
    body: "The graphical representation doesn't replace your text editor, it's added to it. You can always copy-paste and scroll some code !\n\nDuring this summer I was in front of more than 600 classes in C++, it was not a problem for Together.\nAnd really, with 600 classes you like very much Together, especially if most of them are not yours and you have to understand them. It's faster to understand an UML diagram than simple text.\n\nYou don't need a big screen to see everything because you simply don't want (and don't need to)\nYou are a good designer and you split your \"big\" project in small ones that you can work on it (module).\nThe argument is also bad because with you text editor you can't see everything either.\n\nIf your are lost in an UML representation, i can do nothing for you. UML is here to represent in a more human way an program and we haven't find a better way to do it.\n"
    author: "Tanguy Krotoff"
  - subject: "Re: Eclipse"
    date: 2002-10-16
    body: "I would strongly advise to change your mind about UML. For example, nobody would really want to create a complex electric circuit without a wiring diagram. But for sure you have to organize your diagrams and split them into useful amounts of information.\n\nWe use Rational Rose in our company to model all software designs that are more complex than a simple one week hack. And I never want to go back to text only."
    author: "cylab"
  - subject: "Great..."
    date: 2002-10-13
    body: "Now all we need is someone to come up with Haskell support - at least for console based projects since there are not Qt/KDE bindings *yet* (hint) :)\n\nTSL"
    author: "TSL"
  - subject: "Code Folding is a Blessed thing!"
    date: 2002-10-13
    body: "Looks good to me."
    author: "Anonymous"
  - subject: "What about valgrind integration?"
    date: 2002-10-13
    body: "Thanks, looks nice. Apart from Keramik."
    author: "Dieter N\u00fctzel"
  - subject: "How alpha is it?"
    date: 2002-10-13
    body: "First of all, since the older (non-Gideon) version of KDevelop is the one shipping with kde 3.1, why is Gideon in the HEAD branch?  It's pretty confusing to have to come up with some weird branch name to get the currently-shipping version of the program...\n\nI am really interested in trying out Gideon, and was wondering if users who are familiar with it could post their impressions about its usability and stability.  I know it's been in development for quite a while now; can useful development be done with Gideon, or is it really only alpha-quality?\n\nI know there's an import function for translating *.kdevprj files to the new *.kdevelop format; is it also possible to go from *.kdevelop back to *.kdevprj?\n\nthanks in advance,\nJason\n"
    author: "Jason"
  - subject: "Re: How alpha is it?"
    date: 2002-10-13
    body: "Gideon performs pretty well for me; I've written a few smallish projects with it and modified some larger ones.  The code completion is starting to show real promise.\n\nCrashes are quite infrequent, and I really like the new IDEAI interface.  There are certainly parts which are more alpha-quality, but they tend to just not work rather than crash.\n\nThe thing I most like about Gideon is that it uses katepart (or <insert_your_favourite_ktexteditor_here> eg. kvim); the old editor with 2.x didn't quite work the way I wanted it too."
    author: "Hamish Rodda"
  - subject: "Re: How alpha is it?"
    date: 2002-10-13
    body: "It is pretty normal to have the development version in HEAD, because that's where the development takes place. Almost every sane person does this... after all, the CVS is intended for developers.\n(And BTW the rest of KDE does this as well, development is in HEAD and the releases are currently in the KDE_3_0_BRANCH)"
    author: "Tim Jansen"
  - subject: "Re: How alpha is it?"
    date: 2002-10-14
    body: "Yes, of course I know that development takes place in HEAD.  However, for the rest of KDE, the stuff in HEAD is the stuff that will be released with KDE 3.1.  This is not the case with Gideon/KDevelop.  That's what I meant, sorry it wasn't clear."
    author: "Jason"
  - subject: "RAD"
    date: 2002-10-13
    body: "Can Gideon be a Rapid Application Development tool, like Delphi? Complete integration between the UI designer and the editor would be really nice."
    author: "Stof"
  - subject: "glade integration"
    date: 2002-10-13
    body: "Are there any plans to integrate glade as the ui designer for GTK projects?\n\nMarc"
    author: "Marc"
  - subject: "Re: glade integration"
    date: 2002-10-13
    body: "That'd be a real kickass feature!"
    author: "Stof"
  - subject: "How to start it?"
    date: 2002-10-13
    body: "I've downloaded the sources and maked and installed.\n\nNow when I run gideon it only popups a very small window, with three menus:\nFile, Help and Window, The only one with reason to click on is File-Open or File->Quit.\nAlso in the toolbar there is a single open-icon.\n\nVery fun. :-)\n\nBut how does I start coding?\n"
    author: "Syllten"
  - subject: "Re: How to start it?"
    date: 2002-10-14
    body: "KDE somehow failed to register the KDevelop plugins. What you see is just the naked main program. So please, check why your KDE didn't be able to register KParts-based plugins; probably run a kbuildsyccoca manually.\n"
    author: "F@lk"
  - subject: "Re: How to start it?"
    date: 2002-10-14
    body: "I figured out gideon installed into /user/local/kde.\nIt's in my PATH, so I could run it, but the main program coudln't find the plugins, cause they were in /usr/local/kde/...something...\n\nI simply reconfigured with --prefix=$KDEDIR [1], and now everyting is so overwhelming I wanna pass out. :-)\n\n\n[1]: Actually, root doesn't use X, so therefor KDEDIR didn't exist while I reconfigured.\nInstead I did it with --prefix=/opt/kde3. Since i failed to run efter su -c \"make install\", I logged in as root, but under normal circumstances $KDEDIR would have been enough, I guess."
    author: "Syllten"
  - subject: "Re: How to start it?"
    date: 2002-10-14
    body: "Adding /usr/local/kde to KDEDIRS should have solved your problem too."
    author: "Anonymous"
  - subject: "Re: How to start it?"
    date: 2002-10-14
    body: "What you see is the naked main program; somehow KDE failed to register the KDevelop plugins which plug their user interface in. Try if a manual run of KDE's kbuildsyccoca helps, otherwise there's something wrong with your KDE installation in general."
    author: "F@lk"
  - subject: "d.k.o CENSORS dissent"
    date: 2002-10-13
    body: "I have posted many opinions here that run contrary to the majority opinion that have be REMOVED from this site.  BOYCOT CENSORSHIP, this is not nazi germany."
    author: "noone"
  - subject: "Which CVS?"
    date: 2002-10-14
    body: "Is this the HEAD branch or something else?\n\nI can never keep all these KDevelop strains straight...."
    author: "Otter"
  - subject: "Re: Which CVS?"
    date: 2002-10-14
    body: "KDevelop 3.0==Gideon==HEAD, everything else is obsolete."
    author: "Anonymous"
  - subject: "Do Kdevelop 2.1 and Gideon co-exist? "
    date: 2002-10-14
    body: "Kinda, i have some projects to be developed, and i use kDevelop intensively, but i'd also like to try gideon to give it a test.\ndo Kdevelop 2 and 3 co-exist on the same machine? "
    author: "SHiFT"
  - subject: "Re: Do Kdevelop 2.1 and Gideon co-exist? "
    date: 2002-10-14
    body: "yes"
    author: "F@lk"
  - subject: "Re: Do Kdevelop 2.1 and Gideon co-exist? "
    date: 2002-10-15
    body: "Fantastic! I use KDevelop about 4 to 5 hours a day, seriously, and I'm not willing to install gideon if it overwrites/breaks kdevelop.\n\nHowever, since it seems they can coexist, well, I know what I'm doing when I get home ;)\n\nGood work, people.\n"
    author: "Shamyl Zakariya"
  - subject: "yum yum"
    date: 2002-10-22
    body: "One word...\nOrgasmic\n\nit looks great, it handles great, and it is intuitive :)\n\nI LOVE it!! and this is just the Alpha??\nJoy :)\n\n--Andrew"
    author: "Andrew"
---
The KDevelop team would like to announce the availability of KDevelop 3.0 Alpha 1 (also known as Gideon).  This represents a complete redesign of KDevelop and includes tons of new features, and a new interface. Available through <a href="http://www.kde.org/ftpmirrors.html">KDE ftp mirrors</a>, the <a href="http://www.kdevelop.org/">KDevelop web site</a> and CVS, Gideon brings out the best in what an Integrated Development Environment should be.  Users of KDevelop 2.x will notice substantial improvements, while new users will be amazed at the flexibility and extensibility put into this program.  Improvements include: pluggable parts, rewritten Automake/Makefile.am support, support for more programming languages (Java, C, PHP, Perl, etc.), better code completion support, source code control integration, <a href="http://www.kdevelop.org/index.html?filename=changes.html">and more</a>!  Users wishing to experiment with these new features are encouraged to download this version and <a href="http://bugs.kde.org">report bugs</a>.  Of course, the team is always <a href="http://www.kdevelop.org/index.html?filename=join-the-team.html">looking for help</a>. Screenshots: (<a href="http://www.kdevelop.org/graphics/screenshots/3.0/gideon-idealmode-automake.png">1</a>,<a href="http://www.kdevelop.org/graphics/screenshots/3.0/gideon-tabbedmode-cvs.png">2</a>,<a href="http://www.kdevelop.org/graphics/screenshots/3.0/gideon-newproject.png">3</a>).
<!--break-->
