---
title: "KDE Presence at LinuxTag 2002 (Karlsruhe, Germany) "
date:    2002-05-26
authors:
  - "matze"
slug:    kde-presence-linuxtag-2002-karlsruhe-germany
comments:
  - subject: "Sun, eh?"
    date: 2002-05-26
    body: "Sun, eh?\n\nThat's kind of interesting.  Did they approach KDE or did\nKDE approach them?\n\nI just thought that they were GNOMErs.\n\n"
    author: "Anonymous Coward"
  - subject: "Re: Sun, eh?"
    date: 2002-05-27
    body: "I'd also like to know.\nWaiting for answers here...\n:-)"
    author: "lanbo"
  - subject: "Re: Sun, eh?"
    date: 2002-05-27
    body: "Come to the KDE booth to see why :)\n\nRalf"
    author: "Ralf Nolden"
  - subject: "Re: Sun, eh?"
    date: 2002-05-27
    body: "Unfortunately it's a bit far from Barcelona...\nBut let us know ;-)\n\nJL"
    author: "lanbo"
  - subject: "Re: Sun, eh?"
    date: 2002-05-27
    body: "Gnome will be there too, and it looks like sun's to thank for their presence too ( see http://news.gnome.org/gnome-news/1022431486/ ) don't forget to drop by :-)"
    author: "me"
  - subject: "Broken links"
    date: 2002-05-27
    body: "Half of the links on linuxtag web-site is broken. There are no information about community presence, workshops etc."
    author: "antialias"
  - subject: "Re: Broken links"
    date: 2002-05-27
    body: "<a href=\"http://events.kde.org/info/linuxtag2002/\">http://events.kde.org/info/linuxtag2002/</a>\n<p>\nis your friend. Lists all the KDE people coming and more.\n"
    author: "Rob Kaper"
---
The <a href="http://www.kde.org/">KDE</a> (the K Desktop Environment)
Project, one of the largest and most successful Open Source projects,
will be represented in three separate locations at
<a href="http://www.linuxtag.org/2002/deutsch/showitem.php3?item=30&lang=en">LinuxTag
2002</a>, being held in Karlsruhe, Germany from June 6-9.  In addition,
KDE Project members will host several KDE workshops during the event.
The KDE Project's primary focus this year will be the latest stable
KDE release, KDE 3.0.1, though KDE volunteers will also demonstrate
some of the exciting ongoing developments for the next major KDE
release, KDE 3.1. 
<!--break-->
<p>
At the main booth, KDE developers and other volunteers will demonstrate
various applications, such as KDE's file manager and web browser
(<a href="http://www.konqueror.org/">Konqueror</a>), KDE's integrated
development environment (<a href="http://www.kdevelop.org/">KDevelop</a>),
KDE's calendar and group scheduling application
(<a href="http://korganizer.kde.org/">KOrganizer</a>), and the astronomy
application (<a href="http://edu.kde.org/kstars/">KStars</a>).
Also, the latest beta release of 
<a href="http://koffice.kde.org">KOffice</a>, KDE's integrated
office suite will be shown. And of course, you will be able to meet lots of
KDE developers as well as Konqi, the KDE project's mascot. 
KDE will also be demonstrated running on Solaris 8, and due to the presence 
of developers from the <a href="http://opie.handhelds.org">Opie project</a>, 
latest tools for synchronising KDE with a PDA will be shown. 
</p>
<p>
At the German Ministry for Security in Information Technology booth,
the latest developments concerning KDE's mail client
(<a href="http://kmail.kde.org/">KMail</a>) will be presented.
As part of its sponsorship of
"<a href="http://www.bsi.de/aufgaben/projekte/sphinx/index.htm">Sphinx</a>",
an initiative to develop an Open Source framework for secure email
communications, the German government has selected KMail as one of the
reference implementations.
</p>
<p>
At the Linux printing booth, volunteers will demonstrate KDE's powerful
and advanced printing framework (<a href="http://printing.kde.org/">KDEPrint</a>).
The latest advancements in KDEPrint make it an excellent framework for
use in professional, enterprise and home office environments.
</p>
<p>
Apart from the presence at these booth, various KDE Project members will
host several workshops about KDE as a development platform.  Topics will
range from beginner "how-to-get-started" questions up to the use of
Linux on embedded systems.
</p>
<p>
The KDE Team would like to thank KDE e.V., Danka Germany, Sun Microsystems, 
SuSE Linux AG, kernel concepts, Linux New Media AG and Klarälvdalens Datakonsult AB for supporting our presence at Linuxtag 2002.
</p>