---
title: "KDE Stats: KDE Is Brought To You Today By..."
date:    2002-04-26
authors:
  - "sbinner"
slug:    kde-stats-kde-brought-you-today
comments:
  - subject: "myself"
    date: 2002-04-25
    body: "How is work progressing on KDE 3.1?\n"
    author: "John Kintree"
  - subject: "Re: myself"
    date: 2002-04-25
    body: "There's a little note on kde-look.org about implemented features in CVS snapshot."
    author: "jonemi"
  - subject: "Re: myself"
    date: 2002-04-26
    body: "The screenshot shows Keramik style, Tabbed Browsing, Folder icons reflect contents and the crystal icon theme: http://www.babysimon.co.uk/kde/kde31features.png"
    author: "simon"
  - subject: "KDE 3.1 progress..."
    date: 2002-04-25
    body: "Look here:\nhttp://developer.kde.org/development-versions/kde-3.1-features.html\n"
    author: "krzyko"
  - subject: "Uh..."
    date: 2002-04-26
    body: "why doesn't the total at the bottom come to 100%? Is that because of the occasional patch submitted by invisible pink unicorns?"
    author: "Carbon"
  - subject: "Re: Uh..."
    date: 2002-04-26
    body: "The colleague responsible for forging the rest up to 100% was on vacation when the statistic was created."
    author: "Anonymous"
  - subject: "Faure for president!"
    date: 2002-04-26
    body: "Votez!"
    author: "Sergio"
  - subject: "Re: Faure for president!"
    date: 2002-04-26
    body: ";)\n\nTo be fair, like coolo/kulow, statistics are a bit lying for me: I have a cronscript that commits the RDF files for www.kde.org regularly (hence\nthis being the \"most often committed file\"), and I manually run a script\nthat updates the API docs on developer.kde.org almost every day.\nHmm, I wonder why the websites are included in the stats, anyone knows\nhow to get that changed by the berlios guys?\n"
    author: "David Faure"
  - subject: "most frequent commits"
    date: 2002-04-26
    body: "About 220.000 By Kulow ...\n\nOver a period of 5 years that's about 40.000 a year, which amounts to about 110 commits a day; almost 5 an hour. Well, that's what I call 'commitment'\n\n(Sorry about that lame joke ...)"
    author: "Matthijs Sypkens Smit"
  - subject: "Re: most frequent commits"
    date: 2002-04-27
    body: "Ok, so I can't read very well ... I'm sorry."
    author: "Matthijs Sypkens Smit"
---
Have you ever wondered <i>who</i> contributes <i>what</i> to KDE? The <a href="http://developer.berlios.de/">berliOS</a> project attempts to answer this  question with <a href="http://kde-stats.berlios.de/">KDE CVS statistics</a>, a site tallying every developer's contributions (translators are currently not included). With regular updates planned, you can find the latest summary <a href="http://kde-stats.berlios.de/kde/total-20020416.html">here</a> and also statistics for every <a href="http://kde-stats.berlios.de/kde/">CVS module</a>. And before you start to wonder: The account <i>kulow</i> is used to transfer the work of the translators from kde-i18n into the other modules; Stephan Kulow's exclusive account is <i>coolo</i>.
<!--break-->
<p>
<i>[Also of interest is <a href="http://kde.mcamen.de/statistics.html">this graph</a> posted to the KDE Cafe by Marcus Camen in his message <a href="http://lists.kde.org/?l=kde-cafe&m=101768436519880&w=2">"KDE team breaks all records"</a>.]</i>
