---
title: "KDE::Enterprise: KDE At Conectiva"
date:    2002-01-19
authors:
  - "jbacon"
slug:    kdeenterprise-kde-conectiva
comments:
  - subject: "YES"
    date: 2002-01-20
    body: "Teixeira obviously knows what he's talking about. I find myself in agreement with almost everything he said, from the good things (very mature and well thought out desktop) to the bad things (performance problems on low end machines, KOffice not yet being ready for the real world .. while it has some nice features going for it, printing vs. screen is still a major PITA). I also agree with his opinion on eye candy -- fortunately KDE has a very nice setup for this, although IIRC the stupid program launch information was active in KDE 2.2.2 when I selected minimum amount of animations during setup.\n\nWhat I don't understand is why he says he misses gnucash under KDE. Why not run gnucash under KDE? A GTK app should not really be that problematic, aside from some look&feel issues. Besides, there's theKompany's Kapital (any experiences with that?), they might cut a deal with them to distribute it exclusively in Latin America. Similarly, they might try some apps from GnomeOffice: MrProject as project manager and abiword as word processor come to mind."
    author: "Eloquence"
  - subject: "GnuCash isn't an option"
    date: 2002-01-20
    body: "GnuCash has many many dependencies on GNOME libraries and not just GTK+.  This makes it essentially worthless for a KDE desktop.  The Kapital is more like it, but not a free$$$ product."
    author: "ac"
  - subject: "Re: GnuCash isn't an option"
    date: 2002-01-20
    body: "What's to keep you from just installing the GNOME libraries and using them to run GnuCash under KDE?"
    author: "Carbon"
  - subject: "Re: GnuCash isn't an option"
    date: 2002-01-20
    body: "Maybe you should try installing GnuCash under KDE before asking.  Go ahead..."
    author: "ac"
  - subject: "Re: GnuCash isn't an option"
    date: 2002-01-20
    body: "apt-get install gnucash?"
    author: "ddx"
  - subject: "Re: GnuCash isn't an option"
    date: 2002-01-21
    body: "Yes, and this even works on Conectiva too."
    author: "Evandro"
  - subject: "Re: GnuCash isn't an option"
    date: 2002-01-20
    body: "GnuCash has really evil dependencies, but since I solved them, it runs nicely under KDE. I think it's worth the maybe 2 hours downloading from rpmfind.net ;-)"
    author: "Titus Stahl"
  - subject: "Re: GnuCash isn't an option"
    date: 2002-01-23
    body: "KMyMoney is fre"
    author: "Duncan Mac-Vicar P."
  - subject: "Re: GnuCash isn't an option"
    date: 2004-02-13
    body: "Is it possible to export from GnuCash some format which can be imported into KMyMoney?\n\nI'm getting sick of the GnuCash GNOME bloat... you even have to run gconf-1 before GnuCash otherwise it will crash when you try to do anything non-trivial.\n"
    author: "TX"
  - subject: "Re: GnuCash isn't an option"
    date: 2004-02-15
    body: "KMyMoney now (0.6rc1+) now uses an XML based format to store it's data. You can certainly try to write a converter from GnuCash to KMyMoney. Let us (kmymoney2-developer@lists.sourceforge.net) know how it went. We might be interested to make it available for other KMyMoney users."
    author: "thb"
  - subject: "Re: GnuCash isn't an option"
    date: 2004-02-16
    body: "Aha, I see.  I never noticed that GnuCash was XML as well.  So KMyMoney2 is XML also, that makes things interesting.\n\nPerhaps it would be possible to write an XSL transformation to convert from one to the other.\n\nI'm certainly interested in trying an XSL transformation at the moment because KMyMoney2 has import *and* export for QIF format, whereas GnuCash is harsh and only has import.  This sort of file format \"compatibility\" almost feels like a Microsoft product, convert everything *to* your own format, but never, ever *from* it. It loses customers, you see.  And I can't blame them, if GnuCash had QIF export, I would have stopped using it long ago. :-)\n\nI will add this to my TODO list as soon as I manage to fix my TODO editor script to work with Kontact.  [Kontact is fantastic, maybe some day it will integrate something like KMyMoney2 also and we will have finance built into the same program with all the other personal information. :-)]\n"
    author: "TX"
  - subject: "Re: GnuCash isn't an option"
    date: 2004-02-16
    body: "KMyMoney also has the ability to define an external filter program which reads from stdin and writes to stdout (what else?). So you can import/export from/to whatever format you want (except that the filter interface for export is not yet implemented).\n\nKMyMoney currently supports it's own binary format and the XML format. Filenames ending in .kmy are binary, those ending in .xml are XML. In the future, there will be no binary format anymore. We currently keep it for historic reasons. Both formats are GZIP-ed on the fly to save disk space.\n\n"
    author: "thb"
  - subject: "Re: GnuCash isn't an option"
    date: 2005-03-24
    body: "I realize this is a year-old post, but I found this thread today while looking for how to export QIF from GnuCash. Did you ever get anywhere with that XSL transformation? I'd love to have a copy, if it is functional. I would take on the task myself, but I am only marginally familiar with XSL, having only recently dealt with it in learning to write DocBook format. I'd be willing to help, if such an effort is still in development.\n\nI've dealt with GnuCash's mile-long dependency list on every distribution I've used it on. First Mandrake, where it was nearly impossible to untangle the web of various RPM versions and sources. Then Gentoo, which actually made it fairly easy to install, but difficult to upgrade due to version dependencies.\n\nGentoo has good forward-dependency handling, but bad reverse-depdency handling (or at any rate, portage is a bad interface for it). I've used Gentoo for two years, so all my bank records are in GnuCash. I'm making an effort to switch to Kubuntu now, and installing GnuCash yesterday (very easy, thanks to apt-get) in this almost-strictly Gnome-free environment again reminded me of just how much baggage GnuCash is carrying. I want to use KMyMoney instead, since its usability factor blows its Gnome counterpart out of the water.\n\nI'll rewrite the XML by hand if I have to, but an automated conversion would save me a lot of time, and would be a great feature for KMyMoney to have.\n"
    author: "Eric Pierce"
  - subject: "Re: GnuCash isn't an option"
    date: 2005-03-26
    body: "Reverse dependency handling?  You mean, \"tell me every app which indirectly uses GTK?\"\n\nAnd no, I never did end up writing the XSL.  I found it easier to start using spreadsheets instead. :-(\n"
    author: "Trejkaz"
  - subject: "Re: GnuCash isn't an option"
    date: 2005-03-26
    body: "It shouldn't be too hard for a package manager to keep track of who is using GTK (and who is using the apps that use GTK, etc.), so if I decide to remove GTK someday, I'll be warned that a whole bunch of other things won't work anymore. apt handles this beautifully (rpm too, but not as well); portage can't tell you when you're about to remove something important, making it really difficult to remove software. My 10GB /usr partition got full recently, for precisely this reason. Lots of software I install just to try out, but then don't want/use it anymore.\n\nAnyhow, GnuCash still works fine, and after using KMyMoney a while I'm not sure it's going to be adequate for my finance needs. I'll probably stick with GnuCash until I can't stand it anymore :-)\n\nThanks for replying!"
    author: "Eric Pierce"
  - subject: "Re: GnuCash isn't an option"
    date: 2005-03-27
    body: "It's not so hard to remove software under Portage, you just need to think of it the other way around.  If I remove, say, GnuCash, and then do a depclean, it will remove everything it depended upon which is no longer used.  The other way is just to browse the world file and remove things from there which you don't use anymore, and then depclean after that.  I did that recently and freed up about 3 gigs. :-)\n"
    author: "Trejkaz"
  - subject: "Re: GnuCash isn't an option"
    date: 2005-11-21
    body: "I too was looking for a way to export QIF from GnuCash(ver. 1.8.11).  I tried a gnucash2gnumeric XSL file(v0.6) from XML Factory but had problems with it. While searching for a solution I came across KMyMoney, got curious, and installed it.  KMyMoney directly imported GnuCash's database.  I was then able to export KMyMoney Ledgers to QIF.\n\nKMyMoney(0.8.1)had everything I needed to enter transactions in Linux and to export them for later use in Quicken, and that was the solution I really wanted in the first place.\n\n-Louis\n\n\n\n\n"
    author: "Louis"
  - subject: "Some thoughts"
    date: 2002-01-20
    body: "hi,\n\nno offence at all, but the guy just kept pushing on the fact that KDE was so wonderfull. It is actually, but he also kept pushing Gnome in the corner.\nWas he somehow bought :)? No j/k. I just think he shouldn't be all this black and white about KDE versus Gnome. I don't think that any dedicated Gnome developer would be happy to read this. Those guys put a lot of their free time in the development. Gnome is a wonderfull desktop environment,\nand to say it honestly, I have found Gnome more stable then KDE in the first KDE 2.x months. Now they are about the same stability imo.\nBut Gnome is missing the organisation that KDE has, and that is pretty important, I think.\nAnyway, enough with the nagging :) \nGo KDE, go Gnome, go Linux!"
    author: "the red in the sky is KDE's"
  - subject: "Re: Some thoughts"
    date: 2002-01-20
    body: "Well I am a KDE user and my experiences with GNOME have been pretty much as negative as the next guy, and I definitely DON\"T AGREE that GNOME is PRETTY compared to KDE.   I think it is downright ugly and unprofessional compared to KDE despite what the guy said in the interview.  Also there is a lot of KDE candy on kde-look.org if you are into this sort of stuff.\n\nI suppose you are suggesting that we should censor this site so that you do not hear other people's bad experiences with GNOME.  Maybe it would be easier if you stick your head in the sand!"
    author: "ac"
  - subject: "Re: Some thoughts"
    date: 2002-01-20
    body: ">\"I suppose you are suggesting that we should censor this site so that you do not hear other people's bad experiences with GNOME. Maybe it would be easier if you stick your head in the sand!\"\n\nHe's saying nothing like that! To paraphrase, he was simply saying that he felt that Connectiva too easily discarded GNOME as a possibility. KDE isn't hurt by people taking a fair look st both DEs. In fact, it's probalby helpful, since if you choose KDE, it makes your arguments more viable if you're objective."
    author: "Carbon"
  - subject: "Re: Some thoughts"
    date: 2002-01-20
    body: "About Gnome not being pretty, that is arguable.\nEveryone has a different opinion about that.\n\"Do not discuss about tastes.\" It's pointless.\nNo personal offence, but develop an app first (just an app not even a DE).\nAnd then comment Gnome. Spend every free minute you have on it, for several years. You'll be unpleasantly surprised how much\neffort it takes to create a decent app.\nHow would you feel if someone said your app sucks,\nexcept it has some eye-candy?\nIf you comment someones work, then either do it with some some respect for the efforts the person has done for it or don't.\nIt's very easy to say that something is bad.\nAnyway, my idea was just, that the guy had a rather drastic opinion.\nIf I send bug reports to a developer, I always encourage them no matter\nif there app is still in early stage of development, well\nmaybe I'm a fool then (?).\nIn case you didn't notice I put a j/k after one of my sentences.\nI also said \"no offence\", to avoid useless flameing.\nSo your assumptions were not those that I wanted to evoque,\nand I did not even remotely insinuate that this site should be censored (which would be crap).\nBye. I hope you understand what I meant with my previous posting."
    author: "the red in the sky is KDE's"
  - subject: "Re: Some thoughts"
    date: 2002-01-21
    body: "Sure, but I also have a long memory, and I think it's ironic that GNOME is preaching to KDE about injustice instead of the other way around. :)  KDE has taken and still takes a lot of flack from GNOME camp about being ugly.  So I think they should be quiet when someone else expresses a different opinion."
    author: "ac"
  - subject: "Re: Some thoughts"
    date: 2002-01-21
    body: "I will not elaborate much further on this, but it seems that there is not\na hate-rivalry between KDE and Gnome, and that's good.\n(I'm not talking about the people who support each of the DE's, I'm talking about the developers)\nSo why should we even being discussing this?\nI am not part of the Gnome camp nor am I part of the KDE camp.\nI just support the efforts of developers and I am grateful that they\nhelp the desktop-user to migrate to other platforms than wintel.\nAbout the injustice you're talking about.\nI am not familiar with it. It may be true, it may not be true, but\nI've never heard of it. I respect your opinion about gnome.\nBut as I said before, one could argue into the infinite about beauty.\nPersonally I think both DE's have their advantages. (look'n'feel, usability)\nSo it is also useless to argue about which one is best or worst.\nTherefore I will not answer your next reply, because it will lead us to far :)\nBut anyway. Having a discussion is good because you hear someone else's opinion and if it's not the same as yours then you think about it.\nAnyway, In general I am in favour of an average opinion (not black, not white).\nDrastic opinions are almost never right.\n\ngreetz"
    author: "the red in the sky is KDE's"
  - subject: "Re: Some thoughts"
    date: 2002-01-21
    body: "while it may be annoying to hear someone say your project is not all that great, most Free software developers have quite thick skins and take such criticisms as a motivation to improve their software in the areas they are lacking. the truth may be a bitter pill, and half-truths/misinformation even more so, but it is undeniable that knowing what the honest impressions of the users are goes a long way to making software better.\n\ni suppose what i'm saying is: i doubt many GNOMErs lost sleep over this and if they did it was only because they were coding all the more tenatiously. =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Some thoughts"
    date: 2002-01-23
    body: "Just to add another perspective to the debate i try to use both KDE and GNOME, and see which one i end up using most of the time. For me it was also a no brainer, KDE's ok but seems less stable, and doesn't look as good - so i end up in gnome mos of the time (but occasionally trying KDE again to see if i'll be won over).  Before you say it's been totally stable for you - i can believe it, but for me konqueror has never lasted for more than an hour or do without crashing(roughly on a par with windows explorer on my 98 system), KOffice has never lasted more than five minutes and noatun rarely plays files (it crashes first) so even when in kde i end up using abiword, gnumeric, gnucash, galeon, evolution etc to get work done.  To be fair nautilus does feel a bit slower than konqueror but for me it tends to crash less often.\nSo I don't think the argument is quite as clear cut as KDE/GNOME is best period... i think it's worth trying the other one (whichever that might be for you) every now and again, It's also important to remember that other peoples experiences of stability etc may not be the same as yours."
    author: "dave"
  - subject: "Nice interviews"
    date: 2002-01-21
    body: "I really enjoy this interview series over at KDE::Enterprise.  It shows real companies doing real work with KDE in everyday business.  This is the sort of job I would expect the KDE League to be doing on a wider scale.  Maybe they will take a hint instead of staying undercover."
    author: "KDE User"
---
I have just added <a href="http://enterprise.kde.org/interviews/conectiva/">another interview</a> to <a href="http://enterprise.kde.org/">KDE::Enterprise</a>, this time with popular Latin America Linux distributor <a href="http://www.conectiva.com/">Conectiva</a>. The interview has some nice insights, covering their use of KDE, what their customers want, and their wide-range success in deploying KDE. <i>"Our employees range from Linux gurus to people who have very little computer experience like lawyers, accountants and personal secretaries. They all use Linux here and almost all use KDE by default, since we believe it is the best desktop for people to learn how to use. I really believe that our employees are the evidence that Linux is really not hard to use."</i>
<!--break-->
