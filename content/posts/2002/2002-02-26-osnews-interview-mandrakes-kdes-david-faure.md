---
title: "OSNews: Interview with Mandrake's & KDE's David Faure"
date:    2002-02-26
authors:
  - "JigSaw"
slug:    osnews-interview-mandrakes-kdes-david-faure
comments:
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-25
    body: "David, you rock!\n\nOu dit autrement:\nT'es une b\u00eate!"
    author: "me"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "\"David, you rock!\n\nOu dit autrement:\nT'es une b\u00eate!\"\n\nWhoow, d\u00e9j\u00e0-vu :-)\n(If you don't know what I'm talking about, check the comments at the interview page)\n\nGood job, David. It's a very nice interview! Now, more people know what's happening at KDE development level and it may bring more users/developers to KDE.\n\nKeep up the good work!"
    author: "Andy \"Storm\" Goossens"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "Let's hope nobody translates that French sentence into English word for word;)\n(No, it doesn't mean that in French ;)\n"
    author: "David Faure"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "Hey David,\n\nwhat happend to \"poor Jeremy\" (see comments)? Or did you forward his mail to a ranting Waldo ;-)\nNice interview! Keep up your great work!\n\nBye\n\n Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "Must have been his evil twin brother :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "Honestly I have no idea who that person is and what interaction we had.\nI blame my legendary bad memory for not remembering every mail I wrote\nin the last 3 years ;)\nProbably a misunderstanding, or an irritated reaction at a 100th duplicate bug report\nor a \"it doesn't work\" bug report with no details."
    author: "David Faure"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "I'm part of the lucky ones that understand it. It's quite funny...\n\nWho wants to know what it means? :-)\n\nOh, and I know where you can find a recent picture of him that confirms this French text... :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "Well, at first I thought it was the French equivalent of what people say in Ireland, i.e. that he's a right bull of a man. Except it's saying he's a girly beast. Now that's a photo I want to see ;-)"
    author: "Bryan Feeney"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "David Faure Ro><or !!! \n\n<troll mode=\"on\">\nEt en plus il est fran\u00e7ais alors il roxor encore plus.\nSi il \u00e9tait breton, il roxerait 1000x plus !!! ;)\n</troll>\n\n\n\n\n\n\n"
    author: "Shift"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "David Faure (for) president !\n\n<french>\nCa y est, les moules envahissent The DOT ! Il \u00e9tait temps !\n</french>"
    author: "julo"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "A quand une tribune sur dot.kde.org ?\n\n-----------------------------------------\n\nWhat do you think about the inclusion of a \"Tribune libre\" (Free Board ?) in the dot.kde.org site as in the most famous Linux French site http://linuxfr.org ?\nHere is the \"Tribune Libre\" : http://linuxfr.org/board/ \n\nYou can go there and post \"plop !\" or \"kikoooo !\" the French linux users will be happy to see new persons on the board ;)\n\n[ileave]\n\n/!\\ Danger, this post contains lots of \"moule\" private jokes !!!"
    author: "Shift"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "I vote for (Faure ?) that idea !\n\nI just hope my keyboard won't blo"
    author: "julo"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-02-26
    body: "Mais vraiment, vous les francais, vous etes trop bizarre!"
    author: "Navindra Umanee"
  - subject: "RE: OSNews: Interview with Mandrake's & KDE's Davi"
    date: 2002-03-01
    body: "\\\\\\(@v@)///  ,( e-boo !!)\n\nYes, we are crazy but we love that ;)"
    author: "Shift"
  - subject: "mono for kde"
    date: 2002-02-25
    body: "wouldn't it be ironic if microsoft's dotnet did the imposible, making gnome and kde applications compatible. \n\nbut wouldn't that allow proprietary dotnet applications built for windows to be used on Kde without the need for a professional qt license? after all the windows developer doesn't have to know that the application will be used with qt.\n "
    author: "jasper"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "No professional qt license would be required.\nThe running .net application will interact with a 'layer' that translates the commands to KDE/Qt calls."
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "And only developer of .Net layer would have to pay for a proffesional licence, I doubt that you force Microsoft to pay for a Qt licence because their .Net application could be run on Qt 'layer'.\nThere is still something which I dont understand. If I sell Python wrapper I have to buy a special licence for each copy sold, but when I have only a simple Python scripting possibility in my word processor I needn't to buy a developer licence for each copy, I hope. So when need I and when needn't I  a developer licence."
    author: "Rumcajs"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "Yes, you don't understand.  No, you do not need to buy a special license for each copy sold.  What does that even mean?\n"
    author: "buster"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "Python is open source. You don't need to pay money to do anything with it, since you can download the main Python distribution right off their site for free, not to mention that it comes with most distros anyways. Welcome to the wonderful world of free software :-)"
    author: "Carbon"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "the simulairity to pyqt is what were I was thinking about. \nthey weren't allowed to redistribute the windows binary version of pyqt because it allowed delevopers to sell pyqt application without a qt license. \n\nbut that wonn't work in the case of mono for kde.\nsince they can legally redistribute the layer under a mit license based on the qpl version of qt for x11. \nbut the most important differnce is that the developer doesn't have to write it with qt in mind. only an eula will stop users from using it with the qt-layer.\nand a eula is incompatible with the gpl. "
    author: "jasper"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "Well, the Qt bindings for Mono are coming along nicely.\n(See http://lists.ximian.com/archives/public/mono-list/2002-February/002808.html)\nI think I'll have a working DLL before kde3 is released.  Maybe the c# bindings will make it in time for kde3.1!\n\nCheers,\n\nAdam"
    author: "Adam Treat"
  - subject: "Re: mono for kde"
    date: 2002-02-26
    body: "Good job!  I'll make a mention in the next quickies.\n"
    author: "Navindra Umanee"
  - subject: "Not easy..."
    date: 2002-02-26
    body: ">rumours have it that the gcc/ld developers are working on prelinking\n\nRumours !!? I thought that open source teams worked in good cooperation each one helping the other in transparent communication...\n\n> most people don't seem to realize how few developers are behind KOffice :). \n\nYes, it is a big problem, new improvements are now coming too slowly... It is very well initiated, developpers have to realize that working on KOffice is a good way to enter in the KDE team and add improvements for many users...\n\nThank you David for this clear speach. Here it is a good communication !..."
    author: "Alain"
  - subject: "Re: Not easy..."
    date: 2002-02-26
    body: "Well, there are a _lot_ of Open Source developers, and they can't all be in telepathic (aka IRC :-D ) contact simeltanously, plus the Gcc/Ld guys may still be considering whether or not to implement prelinking."
    author: "Carbon"
  - subject: "Re: Not easy..."
    date: 2002-02-26
    body: "No need to have telepathic powers for understanding that there are problems in C++ prelinking (or around) and that we are very numerous to wait improvements.\nBut perhaps it is not easy to improve, I prefer such an explanation..."
    author: "Alain"
  - subject: "Re: Not easy..."
    date: 2002-02-27
    body: "Well, improvements are being worked on :-)\n\nLike Faure says in the interview, from what I understand, true prelinking is still in the process of being designed/written in GCC. Thats all there is to it, it will be ready when it's done :-)"
    author: "Carbon"
  - subject: "Improved Javascript"
    date: 2002-03-02
    body: "(This message is posted so late, no one will probably see it. :))\n\nAnyway, would it be possible to take out the KDE 3 javascript code and put it in an KDE 2 environment? I have some systems that aren't ready for a big(ish) upgrade and the only thing I'd like is better javascript support in Konq. So, would it be possible to compile/distribute it separately? My thought is: why wouldn't it?"
    author: "Matt"
  - subject: "Re: Improved Javascript"
    date: 2002-03-03
    body: "It _is_ possible, everything is ;)\nYou'd need to port the KDE3 khtml and kjs back to KDE2/Qt2, in particular all the\nQPtrList->QList QMemArray->QArray etc. Plus any changes in the rest of kdelibs that\naffect khtml. But it's only about porting, nothing difficult.\n\nYou'd get the current bad handling of events (and <tab> navigation in forms) problems though..."
    author: "David Faure"
---
<A HREF="http://www.osnews.com/">OSNews</A> is featuring an <A HREF="http://www.osnews.com/story.php?news_id=704">interesting interview</a> with <a href="http://www.kde.org/people/david.html">David Faure</a>, the French KDE developer who works for <a href="http://www.mandrake.com/">Mandrake</a>, and whose code and contributions can be found in many core elements of KDE. In this article, David talks about KDE 3's enhancements and speed improvements, the future of <A href="http://www.koffice.org/kword/">KWord</a>, <a href="http://www.konqueror.org/">Konqueror</a>, the debugging tools under Linux, and even brushes on topics such as GNOME 2, .NET, MacOSX and Mozilla. <i>"I do think that Linux will make it on the desktop. I think it has already made it to some desktops, and will continue to improve... 

My own family is obviously the testing bed for this, I have my own usability labs in the persons of my wife, her sister and my parents. 

I think that what is currently missing for non-technical users is better error reporting (e.g. sending errors to log files is not enough - any <a href="http://www.cups.org/">CUPS</a> developer listening? ;), and more focus on stability everywhere - users should never ever have to face a crash."</i>
<!--break-->
