---
title: "Impressions on the Paris Linux Expo"
date:    2002-02-02
authors:
  - "pfremy"
slug:    impressions-paris-linux-expo
comments:
  - subject: "Glad you did better than the US"
    date: 2002-02-02
    body: "We where lucky that we could borrow enough equiptment to do a demo.\n\nDCOP was a big hit along with webdav support.  The X guys even gave us two thumbs up for the use of XRender.  The only saveing grace about the show was we did get a significant amount of traffic, and most systems there seemed to be running KDE.\n\nGranted I was only there on Wednesday, so things may have gotten better but everyone please give Mandrake Software and the Etherboot guys a big hand for bailing us out and helping us get on the network and provideing a power supply for my laptop so I could do the dcop demos.\n\nMaby SuSE and TT will be there next year, but I wont hold my breath....\n\nCheers\n\t-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-02
    body: "Whoa, the big 2 SuSE and TT weren't there?  Not good.\n\nOn the other hand, I am pleased to hear that KDE dominated on the desktops.  This is a step forward for the US!"
    author: "KDE User"
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-04
    body: "The US? ??\n\nThere are other countrys in existance apart from the US with KDE developers.\nTypical fuckin ignorant american."
    author: "Bob the Bulder"
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-04
    body: "\nGerunds are commonly formed with an \"ing\", thus \"fucking\" rather than \"fuckin\".  The proper pluralization of \"countrys\" is \"countries\".  Nationalities are capitalized in english, thus \"american\" is incorrect and should be \"American\".\n\nNow that we have established /your/ ignorance, I believe the individual was referring to the subject of this thread and merely stating that he was glad that the recent convention in the USA had some positive factors to its credit.\n\n--\nEvan \"A little less vitrol would suit this site well\" E."
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-05
    body: "To be precise we established his ignorance concerning american (English?) language. To be fair we should need to know how his english compares to your second or third language. This is anyway off topic, yes his manners were all but appropriate, yes most americans confuse USA border lines with the world ones.\nRenato"
    author: "renato"
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-05
    body: ":: To be fair we should need to know how his english compares to your second or third language.\n\n  English is my second (third, if you count Latin) language.  And my fifth is moving along quite nicely.  \n\n:: his manners were all but appropriate\n\n  Agreed - he slammed someone for a quite possibly misinterpreted statement.  My take on the statement was quite different - that of an individual giving praise to all concerned (look at the subject of the thread to see why he would have made an encouraging, positive statement about the US based LinuxWorld).\n\n:: most americans confuse USA border lines with the world ones.\n\n   Having lived in the USA for quite awhile now, I have not seen any more ignorance than any other country has... merely the common contradictory \"we're the best/people are dumb\" sentiment held throughout the world.  The uneducated of every country are (due no doubt to a variant of Murphy's celebrated law) often the loudest and thus very embarrasing.  Sure American media is dominated by American issues.  I doubt that any news agency anywhere runs updates on landslides on Mons Olympus.  Luckily, there are NHK and BBC radio streams if you want news with your choice of regional focus.\n\n  Regardless, this *IS* off-topic.  My email is available if you wish to discuss this.  I only responded because of the vicious nature of the reply to what appeared to me to be a positive statement.  I'll always come to the defense of a good-intentioned person who is being harassed.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-02
    body: "On Thursday Jonathan ran KDE on his G4 laptop. That and Christopher Molnar's machine generated a significant amount of interest.\n\nImmediately after the first wave of conferences got out on Thursday morning (around 10:30am), the booth got very busy. George, Jonathan and I were kept busy answering questions much of the time. Traffic was very high and didn't relent until close to 6pm. I think it went pretty well!\n\nI'm definitely interested in doing NYC Linuxworld next year. However, I'll help make damn certain we have a proper booth setup. Next year if the following print collateral isn't designed and available I'll make it:\n\n- Brochures. People were often looking for these, and they're great to bring back to the boss.\n- CDs. This was a popular request on Thursday.\n- Booth posters and such. Something to dress up the booth, make an impression for those not yet familiar with KDE.\n- Local directory of KDE-friendly consultants. If a business wants to consider KDE then we should be able to have a directory they can take with them. \n\nI'd be interested to know how Friday went."
    author: "Sean Pecor"
  - subject: "Re: Glad you did better than the US"
    date: 2002-02-03
    body: "Yes we definitely have to have those things.  It was almost embarassing.  Especially since Ximian/Gnome bought out as much of the good realestate at the show as they could.  I got a real \"talking-to\" by the HP guys because of our lack of promotion at the show, and in general in North America.  Most of the people at the show preferred KDE to Gnome or other desktops, but we just don't seem to have any working promotional infrastructure in North America.\n\nWe also need to have:\n\n- Decent hardware\n- A more formal demo (Actually, couldn't we now script this with dcop too?  That would be nice)\n\n\nSean: Friday was busy and it was amazing when it just \"suddenly\" ended at 4:00 and I realized that I had missed lunch!  The KDE booth was particularily busy with some guys keeping me talking for as much as an hour straight.\n\n\nI'll try to send out an email to the list(s) about all the development and non-development issues that were mentioned to me on Thursday and Friday.\n\n"
    author: "George Staikos"
  - subject: "Next european linux meeting?"
    date: 2002-02-02
    body: "Which is the next planned linux meeting where KDE will have a booth?"
    author: "Gioele"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-02
    body: "Many (well some) KDE developers will be at FOSDEM, a free and open software developer meeting in Brussels, Belgium, the weekend of 16/17 February."
    author: "Rob Kaper"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "Always remember older booths and try to learn something on what could be improved. Follow this simple rule and the next booth will even be better :-)\n\nSean Pecor said (see above) already a few good things that could be done at FOSDEM. If time is too short to arrange them for FOSDEM, just use them for the booth after that.\n\nSomething different:\nI was thinking maybe we should make a KDE CD. Put the sources on it and the binaries (yeah... I know, KDE doesn't provide binaries...) and other useful stuff. A CD can be distro specific: e.g. a Mandrake KDE CD and that could create a way to let Mandrake sponsor those CD's. :-)\n\nEverybody happy... the user gets his KDE, KDE gets more users, and the distro that helped us gets attention too"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "FOSDEM is somewhat different: it's not a regular exhibition, it's a meeting by and for developers only. So the booth (if any) will have a larger social function and less of a promotional purpose."
    author: "Rob Kaper"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "Yes, we should probably keep a page somewhere relating how to make a successful booth, with things that will draw attention from the visitors and give them some incentive to get concerned about KDE.\n\nGerard had many copies of a sheet of paper presenting briefly the project and asking help for translation. All people would first take the sheet and read, and then come to us to speak. Or they would stop to read the paper and then I would come to talk to them. Else, they probably wouldn't have stopped. So having a small brochure is very helpful.\n\nThe big K poster and the T-shirts were visible and attracted people too. We should not hide. :-)\n\nWe had no demo of advanced feature, but we should definitely prepare something for the FOSDEM. We will have to show Gnome and GnuStep developers what you can do with an \"advanced\" desktop!\n"
    author: "Philippe Fremy"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "We will have to show Gnome and GnuStep developers what you can do with an \"advanced\" desktop!\n\n<troll>So you are going start coding for Gnome? Don\u0092t you like KDE \"Kiddy Desktop Environment\"?\n\nKDE looks slick acts sick. Once KDE 3.0 is released you can wait 6 months later and  KDE 4.0 will show up. KDE is only good as a KDE development tool. Maybe someday you will realize that Bob Businessperson \"ME\" sees this as a put-off. We see once every three years with MS as a pain. KDE will Kill KDE. Overwork your maintainers and they will quit. Stay a moving target and business people wont even look at you.</troll>\n\n"
    author: "Bob"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-04
    body: "Sorry, but your arguments don't make much sense. It doesn't matter if KDE versions are released fairly often, since binary compatibility between running desktops is maintained (given that you have compatibility libraries installed). \n\nAlso, the time span between releases is several years, not six months. About the same time span exists between major releases of GNOME and GnuStep as well, and only a slightly larger one (about 50% larger) between major sub-releases of the Linux kernel, and major releases of XFree86. \n\nKDE really isn't a moving target, and it certainly is useful as more then a KDE development tool; I use it for several hours every day, typing code for various projects in various languages (Konsole is a godsend), browsing the net with Konqi, listening to music, etc.\n\nConstructive critisicm is good, but please back up your arguments some more."
    author: "Carbon"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-06
    body: "Sorry Sir, but you're very wrong here..\n\nAt my previous job, all the developers came to work were from Windows world, and not a single developer knew about Linux..\n\nI installed to them 2 demo machines - both machines with Red Hat 7.2 - one of them is running Gnome & one is running KDE (and KDevelop). Guess which desktop enviroments they wanted on their 60 workstations? (KDE)\n\nI told the management that KDE is a very fast developing desktop enviroment - but it will be fully backward compatible - they took it with open hands.\n\nSo I really don't get your point, sir...\n\nHetz"
    author: "Hetz"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "Argh, I'm under exams from 16 to 18 :(\nNext one? ;)"
    author: "Gioele"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "LinuxTag in Stuttgart(Germany) June 6th-9th.\nhttp://www.linuxtag.org"
    author: "Tim Jansen"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-03
    body: "Err, LinuxTag moved to Karlsruhe this year.\nSee www.linuxtag.org\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-04
    body: "4. Chemnitzer Linux-Tag, 9. und 10. M\u00e4rz 2002\nhttp://www.tu-chemnitz.de/linux/tag/lt4/"
    author: "Anonymous"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-05
    body: "What about CeBIT Hannover, Germany, 13.-20.03.2002 (the world's largest computer show, http://www.cebit.de)?\n\nMaybe the KDE team could prepare an arrangement with SuSE?\nAll the others should be there, too...\n\nI am there. Maybe for two days as last year...;-)\n\nRegards,\n\tDieter"
    author: "Dieter N\u00fctzel"
  - subject: "Re: Next european linux meeting?"
    date: 2002-02-05
    body: "The KDE team will be present at the CeBIT 2002 in Hall 1.\nWe'll have partner stands at the booth of Danka Printing\nand Sharp.\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "Love the Logo with Konqi"
    date: 2002-02-02
    body: "KDE 3 logo looks kool in those T-Shirts. Knew Konqi is the mascot of KDE but not seen it in KDE much more than Kandalf the wizard."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Love the Logo with Konqi"
    date: 2002-02-03
    body: "I love those t-shirts as well. Just a question. Couldn't it be possible to buy t-shirts to support the KDE-project. I saw on the pics that a shirt was 13 euros. So I say ... I give 30 euros to the kde-project ... can I get one of those T-shirts?  (Yes I know I could do some shirt-printing myself .. it is just an idea ...)\n\nFab\n\nps I live in The Netherlands ... so if it could be done ... let me know :-0 \n\nAny ideas or suggestions or even plans ... shout it out ... \n\nps ps I like this jpeg as well >> see attachment or  http://www.kde-look.org/content/preview.php?file=72-1.jpg\n\nCould look nice as a logo on a shirt  .. it symbolizes nicely the status of KDE .. IT'S HOT !!\n\n\n"
    author: "fab"
  - subject: "Re: Love the Logo with Konqi"
    date: 2002-02-03
    body: "If they are for sale on FOSDEM, I can bring one back with me to the Netherlands. Or you could come to Brussels yourself, you wouldn't be the only dutchman."
    author: "Rob Kaper"
  - subject: "Re: Love the Logo with Konqi"
    date: 2002-02-03
    body: "Thanks to Gerard Delafond for the pain of printing the t-shirts and other stuff!  It's quite humourous to have the CVS splash on a T-shirt! :)\n"
    author: "ac"
  - subject: "KDE's fortunate to have David Faure on the team"
    date: 2002-02-03
    body: "This paragraph really jumps out from Philippe's discussion:\n\nDiscussing with David Faure, I told him about some complaints I had about Konqueror : the profile setting should be in the settings menu and the URL bar should accept URL drop by replacing the current URL. Then I saw this magic thing: A live coding session by a true talented KDE core devloper : David took his laptop and started coding under my eyes. In five minutes, it was done: the \"URL\" label would accept url and just open them. Really great! He did that again on the next day, when a visitor asked for the domtree plugin to support copy/paste of every branch. Really great. "
    author: "dingledongle"
  - subject: "Re: KDE's fortunate to have David Faure on the team"
    date: 2002-02-03
    body: "Yes, Philippe Fremy apparently thought so too (it's in the article descrip :-)"
    author: "Carbon"
  - subject: "Re: KDE's fortunate to have David Faure on the team"
    date: 2002-02-03
    body: "Actually, I added that quote myself, but I didn't re-author the article because that's mostly all I did.\n\n- Your Friendly Neighbourhood Dot Editor\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE's fortunate to have David Faure on the team"
    date: 2002-02-04
    body: "What is reassuring is that he hasn't done anything I wasn't able to do. He just did that a lot quicker.\n-> he types quicker\n-> He knew exactly which file to open\n-> he had the function after three seconds\n-> he had to open the qt help for whatever reason\n-> he already knew how to use KURL\n-> he patched\n-> it compiled just fine.\n-> it ran just fine.\n\nIt helps when you know your application and most of the KDE API.\n\n"
    author: "Philippe Fremy"
  - subject: "The T-shirts"
    date: 2002-02-03
    body: "Glad everybody loves my t-shirts. :-)\n<p>\nI made about 40 of them, all sold or given to the KDE team.\nI used the KDE3 splash screen, a bit modified. I put it in this place:\n<p>\n<a href=\"http://www.delafond.org/kde/konqui_tshirt.jpg\">http://www.delafond.org/kde/konqui_tshirt.jpg</a>\n<p>\nIf you want a similar T-shirt, you just need to download it, print it on a transfer paper and iron it on a blank t-shirt.\n<p>\nThere is nothing organized internationally, this is just a savage and individual work of the french team.\n<p>\nGerard"
    author: "Gerard Delafond"
  - subject: "Re: The T-shirts"
    date: 2002-02-03
    body: "That's why this a maybe an idea ... Maybe we can find an T-shirt printer webshop who decides to make KDE-shirts and give a portion back to the kde project. \n\nOr .... maybe there should be a some talking about logistics and funding for this kind of promo material. I personnaly think this could be done by people who are not programmers but end users of the KDE project who would like to contribute besides translating or mouth-to-mouth advertising.\n\nThere is enough artistic material that could be used to make some nice shirts or so .... Maybe some people of KDE promo could seriously look at this idea ... \n\nFabrice Mous aka Fab\n(kde-nl)"
    author: "fab"
  - subject: "Re: The T-shirts"
    date: 2002-02-03
    body: "Actually...on www.kde.org I found there's a guy selling KDE T-shirts but what was more interesting is that everyone can do it.\n\nHe's using http://www.cafepress.com service that alows anyone to upload their images for T-shirts or whatever merchandise they want and they do the rest (print, process, ship and send the cheques). The service itself is free. Every item has a base price that covers their expenses, but of course you are free to raise it. Difference is a profit that they send you.\n\nSo, with help of kde-artists and kde-league, you could set up a risk-free promotional shop for KDE that could make a buck or two for the project. \n"
    author: "Marko Samastur"
  - subject: "Re: The T-shirts"
    date: 2002-02-04
    body: "The problem with cafepress.com is that they are US based, so shipping to Europe is:\n- long (4 or more weeks)\n- expensive\n\nIf only they had a subsidary in eu... :("
    author: "Gioele"
  - subject: "Re: The T-shirts"
    date: 2002-02-04
    body: "Not only that, importing T-Shirts (or, as the customs people said, 'cotton products') can cause you some problems: there are certain limitations on importing 'cotton products' into the EU, the taxes are high and you may have go personally to the customs office and sign some declaration of the cotton's origin. Not using a business address when ordering them seems to help though."
    author: "Anynomous One"
  - subject: "Re: The T-shirts"
    date: 2002-02-04
    body: "OK. But it would be nice if there was a collection of images that was suited for printing on T-shirts. Then it would be up to an individual to take care of how to get that on a T-shirt.\n\nRight now, if you are like me without an artistic talent, you have only few choices. Either you buy an old T-shirt with KDE1 design or you can take that posted picture and have it printed on T-shirt. Although the picture itself is very nice, I feel a bit odd to announce that KDE is coming to a desktop near me, if it's already on every computer around me. So a collection like that would be great.\n\nIt would be even better if KDE project could get something back..."
    author: "Marko Samastur"
  - subject: "Re: The T-shirts"
    date: 2002-02-03
    body: "I just changed my wallpaper :-) Can't wait to switch to KDE 3...\n\nI know this picture is used in the CVS KSplash but now I can see it on my KDE 2. I wanted to try Beta 2 but this pc is low on speed and disk space :-( The binaries would be helpful, but I'm using RedHat (until the new Mandrake comes out) and they don't provide the beta rpms. :-("
    author: "Andy \"Storm\" Goossens"
  - subject: "What domtree plugin?"
    date: 2002-02-04
    body: "I did not know we had such a handy thing...a pointer would be helpful as a quick browse of the menus did not reveal the Big Secret!"
    author: "shaheed"
  - subject: "Re: What domtree plugin?"
    date: 2002-02-04
    body: "It's in kdeaddons.\nThen tools->Show DOM Tree in Konqueror.\n"
    author: "Sad Eagle"
  - subject: "Re: What domtree plugin?"
    date: 2002-02-04
    body: "Sheesh! My Konqueror is suddenly more powerful.\n\nWhy doesn't Mandrake 8.1 install kdeaddons?  There's too much good stuff in here.\n"
    author: "ac"
  - subject: "Funding of PR-stuff"
    date: 2002-02-04
    body: "Hey guys,\n<br><br>\ni read this site only randomly, so maybe what I have to say is already outdated:\n<br><br>\nIs there a possibility for normal KDE-Users to help you guys at the booths by funding, say, some posters or PR-stuff like Promo-CDs and stuff like that?\nI'm not really into C++ development ( I'M A JAVA MAN !!! ), so I can't do anything productive for KDE ... but as KDE needs better PR (IIUC) this semms to be a good idea.\n<br><br>\nComments?\n"
    author: "JAVA-MAN"
  - subject: "Re: Funding of PR-stuff"
    date: 2002-02-04
    body: "Ah but you can!  There is an extensive Java API for both KDE and Qt.  Not only\ndoes it have a binding, it also has support in KDevelop.  But its just a JAR\nso you can use your favourite IDE as well."
    author: "David Goodenough"
  - subject: "Re: Funding of PR-stuff"
    date: 2002-02-04
    body: "well then I'll have to look into this and see what I can do ..."
    author: "JAVA-MAN"
  - subject: "Re: Funding of PR-stuff"
    date: 2002-02-04
    body: "You do not need to be a developer to manage a KDE booth. Lof of visitors do not know exactly what KDE is, or have a specific interest and only want to see how KDE matches it. So being a concerned user is far enough to answer many questions. After, for the specific development question, you can redirect the visitor to a developer.\n\nSo yes, you can help the KDE booth by simply being there, giving ideas, bringing some demonstration material, bringing some equipment, sharing your successful experience with KDE, encourage people to switch, promote free software, explain what benefit helping KDE could bring, ..."
    author: "Philippe Fremy"
  - subject: "David"
    date: 2002-02-06
    body: "Well, we all knew David is a genius, that's old news. :)"
    author: "Joe"
---
I have written a <a href="http://phil.freehackers.org/linux-expo-2002.html">report of my experiences</a> at the KDE booth of the <a href="http://www.linuxexpoparis.com/">Paris Linux Expo 2002</a>. <i>"Then I saw this magic thing: A live coding session by a truly talented KDE core developer: David took his laptop and started coding under my eyes. In five minutes, it was done: the "URL" label on Konqueror would accept a pasted URL and simply open it. Really great! He did that again on the next day, when a visitor asked for the domtree plugin to support copy/paste of every branch."</i> You can also view <a href="http://www.kde-france.org/galerie/expo2002/jour1/gallery.htm">photos of the event</a> thanks to Laurent Rathle, webmaster of the excellent <a href="http://www.kde-france.org">kde-france.org</a> site.
<!--break-->
