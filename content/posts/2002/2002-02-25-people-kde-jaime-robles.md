---
title: "People of KDE: Jaime Robles"
date:    2002-02-25
authors:
  - "wbastian"
slug:    people-kde-jaime-robles
comments:
  - subject: "language course"
    date: 2002-02-25
    body: "Just a silly idea: the i18n data could be used to set up for use in a simple language program. Of course the subject would be computers only, which is a bit limited.\n\nAre there any good language learning programs for linux?\n"
    author: "Jos"
  - subject: "Re: language course"
    date: 2002-02-25
    body: "KVocTrain of KDE 3.0? :-)"
    author: "Anonymous"
  - subject: "Re: language course"
    date: 2002-02-25
    body: "There is a new module in KDE3 which is called kdeedu. The KDE-Edu project aims to develop educational software and the kdeedu module will be integrated in KDE3. For languages, we already have KVocTrain, we also have a spanish verbs program named KVerbos, Kitten (currently in kdenonbeta) will help you with Japanese. KLettres is targeted to very young children to make them learn sounds in French and Dutch is currently integrated. We also have a program to learn Norvegian verbs. As the KDE-Edu project is quite new, all this is in development. But there is definitively some great stuff in development for languages.<br>\nCheck out the kdeedu module from CVS using POST_KDE_3_0_BRANCH to try all these programs.\n<p>\nDon't hesitate to join the enthutiastic <A HREF=\"http://edu.kde.org\">KDE-Edu</A> team!\n<p>\nThanks Tink for this nice interview. I am always amazed by the translation teams, they do such a great job!"
    author: "annma"
  - subject: "Maybe..."
    date: 2002-02-25
    body: "Someone on the translation team could translate Tink's questions into better English, so that it doesn't read like she ran the thing though Babelfish. Here, I'll help.\n\n\"What is missing badly in KDE?\" -> \"What is badly missing in KDE?\" or \"What is KDE missing badly?\"\n\n\"What do you think, when will \"The Tea Cooker\" actually be able to make tea?\" -> \"When do you think that \"The Tea Cooker\" will actually be able to make tea?\"\n\n\"How would it look like?\" -> \"What would it look like\" or \"How would it look\"\n\nNot a flame, just helping the interviews read a little better.\n\n\n"
    author: "John"
  - subject: "Re: Maybe..."
    date: 2002-02-25
    body: "Many developers/contributors aren't native English speakers. And I'm one of them :-) They do their best to speak/write correct English, but mistakes do happen. It's almost impossible to let someone check each sentence you write.\n\nTink lives now in the USA, but she was born in The Netherlands. I think her English is much improved the last year, because she is getting used to it. OK, British English is 'cleaner' :-)\n\nMaybe we should make a webpage on kde.org to learn from our mistakes? :-) Or does a dedicated website already exist?"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Maybe..."
    date: 2002-02-27
    body: "Are you offering to proofread and correct the questions John?\nJust wondering....\n\n--\nTink"
    author: "Tink"
  - subject: "Re: Maybe..."
    date: 2002-03-06
    body: "Hmm, it seems more like he _is_ correcting the questions, rather then just offering to do so."
    author: "Carbon"
  - subject: "Thanks"
    date: 2002-02-27
    body: "As part of the KDE-ES translation team, I want to thank both Tink and Jaime for such a wolderful interview.\n\nAll the best"
    author: "Enjolras"
---
Sit back and relax! This week's episode of 
<a href="http://www.kde.org/people/people.html">The People behind KDE</a>
takes you to
<a href="http://www.usm.maine.edu/~maps/exhibit5/SPE17.JPG">Spain</a>. 
It is of course no coincidence that it is here that 
<a href="mailto:tink@kde.org">Tink</a> spoke with 
<a href="mailto:jaime@kde.org">Jaime Robles</a>, member of the <A href="http://es.i18n.kde.org/">Spanish KDE translation team</a>. Learn a little Spanish yourself through
<a href="http://www.kde.org/people/jaime.html">this excellent interview</a>.

<!--break-->
