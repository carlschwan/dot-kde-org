---
title: "Komba:  Security Release"
date:    2002-03-26
authors:
  - "Dre"
slug:    komba-security-release
comments:
  - subject: "Testing 123123123"
    date: 2002-03-29
    body: "Just saw the kde-cafe post, just checking to make sure :-)"
    author: "David Bishop"
  - subject: "Re: Testing 123123123"
    date: 2002-03-29
    body: "Heheh, sober dot article is pending. :)"
    author: "Navindra Umanee"
  - subject: "Very little activity here..."
    date: 2002-03-31
    body: "Wow, does nobody here use Komba, or is it just that nobody cares to say anything since the story was just a security update announcement?  I don't think I've seen a story with no relevant comments for quite a while on the dot."
    author: "Gilligan"
---
<a href="mailto:schwanz at fh-brandenburg.de">Frank Schwanz</a>, the developer of <a
href="http://zeus.fh-brandenburg.de/~schwanz/php/komba.php3">Komba</a>,
a popular <a href="http://samba.org/samba/">Samba</a> share browser,
today announced a security release.  All versions of Komba2 prior to today's release are
affected by a flaw which permits other users easily to learn a share
password whenever the share is mounted.  All Komba users are encouraged
to upgrade to version 0.73 and to discontinue use of Komba on multi-user
machines in the meantime.


<!--break-->
