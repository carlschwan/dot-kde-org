---
title: "LinuxPlanet: A Winding Path to KDE3"
date:    2002-02-07
authors:
  - "numanee"
slug:    linuxplanet-winding-path-kde3
comments:
  - subject: "well KDE 3.0 beta 1 has been released"
    date: 2002-02-07
    body: "the 19th of december..."
    author: "azhyd"
  - subject: "Re: well KDE 3.0 beta 1 has been released"
    date: 2002-02-07
    body: "Oops, let me rephrase that.\n\n-N.\n"
    author: "Navindra Umanee"
  - subject: "Waiting..."
    date: 2002-02-07
    body: "Does anyone know when CVS, mail, ... will be running again? It's taking a lot of time ( >2 days already ) to do only security related patches :-) But I suppose they are switching to a more powerful machine also.\n\nTiming is good, everybody is waiting for Beta 2 to see what improvements have to be done before the final release. We don't want the servers go down when the moment is critical (just before release)."
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "No, the timing is terrible - this is just when you want everyone working together to get CVS stable, and it's down. What has surprised me is how unconcerned everyone in #kde is about this.\n\nThis has shown that KDE development has a single point of failure, that needs to be rectified as soon as possible."
    author: "Jon"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "We're no unconcerned, but there is little we can do about it at the moment. We can bitch all day, but that is not constructive.\n\nThat mail is down is unfortunate and we should indeed make arrangements for master to be more accessible to a larger group of people.\n\nThat CVS is down is scheduled, although it does seem to take VA Linux very long to install/upgrade a machine. Last time I upgraded servers at home or work, I don't recall there was a 48 hour (and growing) downtime for me or our customers. I'm grateful for them hosting cvs.kde.org, but when it comes to Linux, VA appears to be incompetent. :("
    author: "Rob Kaper"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "> when it comes to Linux, VA appears to be incompetent.\n\nDon't worry, they're probably migrating the KDE servers from Linux to Windows.\n"
    author: "ac"
  - subject: "Re: Waiting..."
    date: 2002-02-09
    body: "For this kind of down time, I'm more inclined to believe that they were on MS and ran by MS."
    author: "a.c."
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "Ok, timing is not so good, but it could have been much worse. I think the best time to take the servers down is immediately after a release (or when the packages are done). Everybody is switching to the new KDE and start using it.\n\nI'm sure many developers are now at home/work/... working to eliminate bugs they know of. They have to wait to submit their patches, but not much time will be lost while the servers are down.\n\nI found a moment ago a bug in KDevelop, but I can't submit it because bugs.kde.org is also down :-("
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "Well, bug fixing requires some coordination and discussion often. The problem isn't that CVS is down. The problem isn't that mail is down. But both being down at the same time is just very unfortunate.\n"
    author: "Rob Kaper"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "> We don't want the servers go down when the moment is critical (just before release).\n\nSurely not, but didn't you notice (ftp) server goes down before every KDE release? At least this happend for some of the latest (beta) releases, perhaps DoS attacks? I would not be surprised when reading some idiot threatens to produce so much traffic for kde-look.org that they will have to go offline."
    author: "someone"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "Why did that idiot threaten to take down kde-look.org?  This is a beautiful site."
    author: "ac"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "Read the comments of the \"Save KDE-Look.org petition\" (http://www.kde-look.org/content/show.php?content=823)."
    author: "someone"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "Thanks I see the admins are taking a stand (http://www.kde-look.org/news/news.php?id=25).  This is a very nice site that is doing a lot for KDE themes.  It has so much good stuff on it. It is way better than the defacto kde.themes.org!! Who would have known there was so much KDE art?\n\nI wonder who runs this site.\n"
    author: "ac"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "The mysterious \"KDE-look.org Team\" runs it. But I could not tell you more than one name.\n\nLooks like an interesting task for Tink."
    author: "someone"
  - subject: "Re: Waiting..."
    date: 2002-02-07
    body: "the ftp server \"goes down\" to allow for mirroring to occur. that's why it happens _every_ time there is a release."
    author: "anon"
  - subject: "While we're waiting..."
    date: 2002-02-07
    body: "could someone post some nice screenshots to make us \"cowardly\" users not ready to compile from source drool some more? Thanks!\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "Now that CVS is not changing.... i can tell you for sure that the version provided is stable enough. Give it a try before anything changes! ;)"
    author: "u"
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "You can give a try to the CVS version now that it's not changing :)  No, seriously, it's very stable. The only serious problem I find is that somebody tweaked sth in the kcm_style thingy, and now it only supports QT styles,... but everything elssseeee.... wow!"
    author: "u."
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "The style problem is a Qt bug, and will be fixed (we're told) in 3.0.2. In the mean time you can get things working properly by running qtconfig and adding your kde lib directory to the library search path. You'll then get all the KDE styles (including the fantastic new 'light' style).\n\nRich.\n"
    author: "Richard Moore"
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "Thanks, I'll try to repair it. I'm the happiest man in the world now. :))"
    author: "u."
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "whoa, what it this new \"light\" style ??? :-D"
    author: "Eron Lloyd"
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "The current version DOES support KDE-Styles. This is a bug (?) of Qt.\nBut you can fix this like this:\nstart qtconfig and add this plugin-dir:\n$KDEDIR/lib/kde3/plugins\n\nThis will create a ~/.qt/qtrc file\nThen all styles reappear after a restart :-)\n\nLocutus"
    author: "Locutus"
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "Thanks very much to everyone, I could not live longer without my styles!"
    author: "u."
  - subject: "Re: While we're waiting..."
    date: 2002-02-07
    body: "I actually did make some new screenshots of current CVS (say, KDE 3.0 Beta 2) but I cannot update it because the website is managed through CVS also and as said, CVS is down for a while. As soon as it is up again I will post the new screenshots and announce the update on The Dot."
    author: "Rob Kaper"
  - subject: "Re: While we're waiting..."
    date: 2002-02-08
    body: "You could post one or two here in the meantime, to whet our appetites ;-)"
    author: "not me"
  - subject: "Re: While we're waiting..."
    date: 2002-02-10
    body: "Well, CVS is up again. Where are the screenshots?"
    author: "KDE User"
  - subject: "Klipper broken ?"
    date: 2002-02-07
    body: "Not by far. anyone using gnome applications for any length of time (me for example, using evolution for email. not the best, but much more stable and useful then the KDE competitor) will encounter this anoying behaviour difference between the way GNOME handles the clipboard and the way KDE does. sometimes I have to go around copying and pasting something in to different text editors just to get a line out of konsole and into an email. \nThe problem is that GNOME is doing the \"Right thing(tm)\". since in X, the things that you select with the mouse and the clipboard is not the same thing, and shouldn't be mixed. in KDE, for example, it's impossible to copy something, then mark somewhere else and pasting over it (that is w/o going to klipper and playing with \"previous content\" which is an anoyance).\nThe way it should work (and as far as I know does work in QT3 - which is what anoyed the article's author) is like this :\nwhen you select something with your mouse, you can paste it down with the middle mouse button. if you want to \"paste\" using and editor \"paste\" command or keyboard shortcut, you first have to \"copy\" it using the editor's command or keyboard shortcut (usually CTRL-C). this is (a) the correct thing according to X specs, (b) compatible with GNOME and (c) is the way windows users are used to work, which makes it easier for them to port over.\n\nNow, to get something from Konsole to the \"real\" clipboard (not the mouse selection), we either have to add a \"copy\" command to Konsole's menu, or add something similar to Klipper - which should be fairly easy: a command to harvest the mouse selection into the clipboard.\n"
    author: "Oded Arbel"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "Clipboards in Linux suck. There's really no easy and quick way of doing it (especially in time for KDE 3.0) that doesn't result in at least some incompatabilities with other forms of clipboard management. Probably the best way to do it is to start a new, external independent clipboard system, then code all the apps concered to use it, but this would take a lot of coderpower, so it's not likely to happen anytime soon."
    author: "Carbon"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "I do not agree - the standard way of doing this in X (having a clipboard, which is different then the mouse selection) is good - it's powerful, it's friendly, it's easy to understand (if you're not confused by using KDE2 too much ;-), especially for users who are used to other GUIs, and it works great across applications and toolkits as long as nobody breaks it.\nFortunatly, AFAIK KDE2 is the only really popular broken implementation out there - getting that out of the way is all what needs to be done."
    author: "Oded Arbel"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "Yes, it is all those things, and it's also limited to only text :-). The QT clipboard, iirc, has support for mimetypes."
    author: "Carbon"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "That's not true you can put whatever you want in the clipboard, just make sure the application your pasting it into support that format."
    author: "Oded Arbel"
  - subject: "Klipper broken ? Still ??"
    date: 2002-02-07
    body: "1) Oded Arbel says :  \nin X, the things that you select with the mouse and the clipboard is not the same thing, and shouldn't be mixed. in KDE, for example, it's impossible to copy something, then mark somewhere else and pasting over it (that is w/o going to klipper and playing with \"previous content\" which is an anoyance).\n\nYes, it is a big trouble of KDE2. Some months ago, it was said here that it will be solved with KDE3. In all the programs...\n\nSo I am very surprised to see that...\n\n2) Carbons answers :\nThere's really no easy and quick way of doing it (especially in time for KDE 3.0) that doesn't result in at least some incompatabilities with other forms of clipboard management.\n\nI understand that the clipboard troubles of KDE2 will again exist in KDE3. I think it is serious, it is a reason for delaying KDE3 of some weeks...\n\nIt is THE first improvement that I waited with KDE3..."
    author: "Alain"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "Konsole of KDE3 sets clipboard and selection when you select something. And Klipper of KDE3 has an option \"Synchronize contents of the clipboard and the selection\" which should work the same way as in KDE1 and KDE2."
    author: "Stephan Binner"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "I hope this \"syncing\" option is not enabled by default - its just good to break everything up again. OTOH - getting Konsole to copy everything selected to the clipboard as well as to PRIMARY is a good feature - its consistant with gpm and is thus the path of least surprise."
    author: "Oded Arbel"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-07
    body: "Yes, the old copy & paste behaviour of kde 2.0 was not very x like, and I hope kde 3 will be better in this direction, at least the texteditors using the kate part (Kwrite/Kate) will now work this way. Selection via mouse goes into the appinternal selection buffer, copy & paste actions via shortcuts or buttons/menu will go into x clipboard, as wanted. This is allready done for Beta 2 ;)"
    author: "Christoph"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-08
    body: "What if Klipper is made to work just the way it does in kde2, but if you try to paste a text that is identical to the one you have marked, it pastes the previous one instead?\n/J\u00f8rgen"
    author: "J\u00f8rgen"
  - subject: "Re: Klipper broken ?"
    date: 2002-02-08
    body: "Thinking about it a bit more, it's not surch a great id\u00e9 after all. Somtimes if you need to add a line in a text, and the line is the same as the one abow, you'll just mark the line, and paste it twice. :(\n/J\u00f8rgen "
    author: "J\u00f8rgen"
  - subject: "Klipper"
    date: 2002-02-07
    body: "That actually brings up an interesting question..is klipper really MEANT to work so damn strangely in KDE 2? Why, for the love of god, does it copy HIGHLIGHTED TEXT?!??! It makes it really annoying to copy something and then select some text that you want to paste over.. because you can't. The act of selecting the text causes it to be copied and then you have to manually set it back to what you wanted to copy in the first place. I can't find any way of turning this off either.. I don't want my text to be copied when I select it, I want it to be copied when I press the appropriate buttons to make it happen. Yes its how windows does it, but its the better way really.\n\n"
    author: "Enjolras"
  - subject: "Re: Klipper"
    date: 2002-02-07
    body: "Yes, that is the way it is meant to work.  Yes, it is dumb.  Yes, it is changed in QT/KDE 3."
    author: "not me"
  - subject: "Re: Klipper"
    date: 2002-02-08
    body: "Because highlighting for copy and middle clicking for paste is the standard X11 way of copy/paste. You're presumably used to the other standard, having to specifically click on buttons/use key combinations to cut and paste.\n\nI find it really annoying when I have to use Windows, precisely because Windows *doesn't* copy highlighted text. I usually click in the middle button at least a couple of times before I realise the problem."
    author: "Jon"
  - subject: "Re: Klipper"
    date: 2002-02-09
    body: "> Because highlighting for copy and middle clicking \n> for paste is the standard X11 way of copy/paste. \n\nYes and no.\n\nThis will still work in KDE3 / QT3. \nBut it's not the *only* standard X11 way of data transfer between\napps.  There is more than one type of selection.\n\nDid you read the specs on http://www.freedesktop.org/standards/clipboards.txt \nlinked in the article?\nI found it a very interesting read.  And the change we're \ntalking about here is a move of QT and KDE to a more full\ncompliance to the X way of handling selections!\n\nI always found the way selections work under X inconsistent and confusing. \nMost of the time it worked as expected, but sometimes not...\nBut it seems things will change for the better.\n\n[snip]\n\n> I find it really annoying when I have to use Windows, \n> precisely because Windows *doesn't* copy highlighted text.\n\nI miss that, too.\n\n"
    author: "Christian M\u00fcller"
  - subject: "Re: Klipper"
    date: 2002-10-18
    body: "Klipper pisses me off when I'm browsing the 'Net, also.  Here's how to fix that problem:\n\n1. At the console, type: xprop | grep WM_CLASS\n2. Then click on the open browser window.\n3. This will tell you the class name of that program.\n4. Now go into the Klipper config menu and select \"Advanced\".\n5. Add the class name of the aflicted application.\n\nViola!  No more annoying pop-ups when browsing the net.\n\nThis should also work on other applications as well."
    author: "macm"
  - subject: "Re: Klipper"
    date: 2003-06-27
    body: "Ugh.\n\nWhere is the Klipper config menu?"
    author: "Ed"
  - subject: "Re: Klipper"
    date: 2003-06-27
    body: "Right-click Klipper's icon in the system tray."
    author: "Anonymous"
  - subject: "Re: Klipper"
    date: 2003-06-27
    body: "I'm using KDE 2.2 installed from Debian's standard package.  I have no Klipper icon to right click and I couldn't find Klipper anywhere in the \"Control Center.\"\n\nI found out how to turn it (URL grabbing) completely so I've done that for now.\n "
    author: "Ed"
  - subject: "Re: Klipper"
    date: 2004-02-20
    body: "Finally. \n\nCtrl-Alt-V for me brought up klipper preferences. \n\nCan disable it from there. However I made extra sure by removing all \n'helper application' definitions...\n"
    author: "David"
  - subject: "jpeg support for background"
    date: 2002-02-07
    body: "after 5 times of (fully) recompiling CVS I'm beginning to think this is not really a bug and it's my fault. For some strange reason I can't put any jpeg format background images (it supports whatever else: png, tiff, ....). I can display them in konqueror, but they are not shown as background.\n\nI'm using mandrake's libjpeg62 rpm packages, and I configure QT with \"-system-libjpeg\", so it links using \"-ljpeg\".\n\nIs there anything wrong with what I do?"
    author: "a tired compiler..."
  - subject: "Re: jpeg support for background"
    date: 2002-02-08
    body: "Nope, I think that it is a bug. I have the same problem. Wait a while and see what next week's CVS has in store."
    author: "Brent Cook"
  - subject: "Re: jpeg support for background"
    date: 2002-02-08
    body: "The strange thing is that I didn't find any bug about it posted so far in the mail-lists.... nobody noticed it !?!?!?"
    author: "a tired compiler..."
  - subject: "Re: jpeg support for background"
    date: 2002-06-15
    body: "The simplest way to go around :\nconvert your jpeg files to png, using gimp - save as png"
    author: "nada"
  - subject: "Korganiser"
    date: 2002-02-07
    body: "So how *can* you get rid of that annoying Korganiser daemon?\n\nNot that it matters at the moment as I've stopped using KDE 2.2.x as it's too buggy and slow ;) (yes I've reported the particular bugs which annoy me)"
    author: "cbcbcb"
  - subject: "Re: Korganiser"
    date: 2002-02-08
    body: "I'm not very sure if it does not work, since I haven't tried it. But if you press right button on the icon, choose Configure Configure Alarm Daemon... and disable the option of starting it every time you login... doesn't it work? Sorry if this is a stupid answer, but I haven't had much time to go through it...."
    author: "a tired compiler..."
  - subject: "Re: Korganiser"
    date: 2002-02-08
    body: "Ooops! Now I notice you were referring to KDE 2.2.x... :("
    author: "a tired compiler..."
  - subject: "Re: Korganiser"
    date: 2002-02-08
    body: "Since you sound like a buggy and slow troll yourself, I won't answer your question!\n"
    author: "ac"
  - subject: "Re: Korganiser"
    date: 2006-12-05
    body: "Terriby annoying indeed. You can 'disable' it when right-clicking on it, but after a restart of Kontact, it will just reappear in the tray when I open Korgnaiser. It seems inposible to get rid of it for good.. If anyone knows?"
    author: "Niwde"
  - subject: "Hurrah! Klipper less annoying!"
    date: 2002-02-08
    body: "Personally, I hope that this change fixes one of my chief annoyances with KDE: the way klipper pops up the actions menu whenever I highlight a URL.  Now, I like the actions menu; that's why I enabled it.  But I'd much rather it only popped up when I explicitly clicked 'Edit->Copy', instead of every single freakin' time I highlight the the URL in Konqueror.\n\n"
    author: "Avdi nGrimm"
  - subject: "Re: Hurrah! Klipper less annoying!"
    date: 2002-02-11
    body: "> But I'd much rather it only popped up when I explicitly clicked\n> 'Edit->Copy', instead of every single freakin' time I highlight the the URL > in Konqueror.\n\nIt shouldn't ever do that, it completely ignores selections in konqueror, opera, netscape and mozilla. Check the \"Advanced...\" dialog."
    author: "Jake"
  - subject: "Please Please Please"
    date: 2002-02-08
    body: "PLEASE tell me that the current way of select/copy/paste will be available in KDE 3.  I will go insane if I have to do anything more than just hilighting (in ANY application) to copy.  I want klipper to remember everything I hilighted, just like it does now.  The world is perfect this way.  I cannot stand the windows/mac way of doing things, it slows you down big time.  I need my X/KDE2 style clipboard!!"
    author: "KDE User"
  - subject: "Re: Please Please Please"
    date: 2002-02-09
    body: "Yes it will be available!  In addition you will have a Mac/Win like clipboard which you can turn off with an option to Klipper."
    author: "KDE User"
  - subject: "Re: Please Please Please"
    date: 2002-02-09
    body: "Woo hoo!  Why am I not surprised?  The KDE guys rule.. =)"
    author: "KDE User"
  - subject: "Klipper"
    date: 2003-12-21
    body: "I have been looking at klipper, In the actions setup I have had expressions using the %s twice, the first is substituted but the second is not. is there something that I can do to make the substitution in the second case, or do I look for shell variables to store the value?\n\nOriginally I was using mozilla with a action as follows...\n\nps x |grep -q '[m]ozilla' && mozilla -remote \"openURL(%s, new-window)\" || mozilla \"%s\"\n\n\nI tried the %N$s format but it didn't work, any other ideas?\n"
    author: "William coleman"
---
While we wait for KDE CVS and KDE mail to be resurrected, <a href="mailto:hetz@witch.dyndns.org">Hetz</a> wrote in with a pointer to <a href="http://www.linuxplanet.com/linuxplanet/reviews/4046/2/">.dep's impressions</a> of a pre-beta2 version of KDE3. It's actually quite positive (he finds KDE3 stable already), with screenshots, but also not without some criticism of <A href="http://kmail.kde.org/">KMail</a> changes and a good point about the difficult-to-kill <a href="http://korganizer.kde.org/">KOrganizer</a> alarm daemon.
<!--break-->
<br><br>
The only other thing that made me react was the drastic new semantics of Klipper.  As a KDE2 user grown attached to Klipper and its quirks, I'm a bit apprehensive about the change in behaviour that's been forced by Qt3.  On the other hand, many people from <a href="http://www.freedesktop.org/standards/clipboards.txt">X Clipboard Interoperability</a> land have long been clamouring for this change, and I'm sure there's a way to get Klipper to revert back to its old behaviour.
<br><br>
However, Klipper may now be less than useful in its <i>default</i> incarnation considering that most of us don't want to know a PRIMARY selection from a CLIPBOARD selection. Consider Konsole users too, who now have to go out of their way to get a selection into the default Klipper. 
<br><br>
Oh, and before you get too excited, remember that KDE3 beta2 has not yet been released, let alone the final KDE3.