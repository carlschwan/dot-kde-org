---
title: "KDE Merchandise Sales to Support Developers"
date:    2002-11-08
authors:
  - "wbastian"
slug:    kde-merchandise-sales-support-developers
comments:
  - subject: "Selection"
    date: 2002-11-08
    body: "I must be a geek t-shirt junkie, as I look over my /., wil wheaton, geek tek, Sluggy Freelance, \"Fear the penguin\", \"Tux Rocking\", and \"Tux-with-a-whip\" shirt collection.  I plan on soon adding ones from Megatokyo, Penny Arcade, and now, KDE.  In fact, I have only two complaints:\n\n1. I wish there was more selection, than just the two different logos. Oh, well.\n\n2. I wish I wasn't poor :-(  \n\nHey!  Maybe the KDE developer that gets the grant should also get a free t-shirt (of his/her choice, of course).  Then, all I need to do is finish learning c++, kde, and qt, graduate from college, raise my children into adulthood, start contributing heavily to KDE, sign up for the grant thingy, and wait for the free t-shirt to come rolling in!  \n\nThat would rock...\n\nThanks Shawn!  I always knew you weren't the spawn of the devil! *grin*\n\nD.A.Bishop  "
    author: "David Bishop"
  - subject: "Internet Cafes"
    date: 2002-11-09
    body: "\n\nWhy not let internet cafes donate money to KDE developmennt. Let them add like 50 cents an hour per person to the tab and give this money to KDE developers.\n\n"
    author: "HAnZo"
  - subject: "New? Innovative?"
    date: 2002-11-08
    body: "Hi,\nSupporting KDE via merchandising might in general be a good idea - but where did you get the idea that this is \"new\"? I'm not exactly sure, but I think the idea of merchandising is already at least 30 years old.\nWasn't this tactic exactly the point of many accusations against MS?\n\n\tGuido"
    author: "Guido Winkelmann"
  - subject: "Re: New? Innovative?"
    date: 2002-11-08
    body: "The new part is that each month a KDE developer can win the profits. That's quite unique as far as I know.\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: New? Innovative?"
    date: 2002-11-08
    body: "Okay - there's something more or less new.\nStill, I think, you should use those buzzwords more carefully.\n(After all those years of M$ propaganda, I - and propably many others, too - have grown a little allergic to those...)"
    author: "Guido Winkelmann"
  - subject: "Re: New? Innovative?"
    date: 2002-11-08
    body: "Selling merchandise seems like a good way to support developers, and I wish TheKompany luck in this move.  However, I find it a bit strange that Gordon is doing this after criticizing Ximian so much for selling \"plush monkeys\" to do something similar."
    author: "aigiskos"
  - subject: "Re: New? Innovative?"
    date: 2002-11-08
    body: "actually they were selling plush monkeys as a way to make money for themselves, although I think they were just breaking even on them overall."
    author: "Shawn Gordon"
  - subject: "Idea, logo, slogan"
    date: 2002-11-08
    body: "I don't like the idea of a developer lottery, that asks for trouble and enviousness. Why not give the money to KDE e.V. so e.g. booths, flyers and t-shirts for exhibitions can be supported? I don't like the red/blue KDE logo, looks ugly and is not a known brand of KDE. I don't like the slogan \"Come get some\" for the same reason, Konqui is not Duke Nukem."
    author: "Anonymous"
  - subject: "Re: Idea, logo, slogan"
    date: 2002-11-08
    body: "well the artwork ideas and the logo's came from core KDE developers.  I thought it would be best to use people that were actively involved for ideas they thought were appealing.  The red/blue logo is artwork from Rik Hemsley that he put into KDE some time back and he wanted to see it on the merchandise."
    author: "Shawn Gordon"
  - subject: "Account records for last two months?"
    date: 2002-11-08
    body: "TheKompany's page states: \"We'll also be posting the accounting records for the merchandise store so you can see exactly how much is getting generated.\" According to http://linuxandmain.com/modules.php?name=News&file=article&sid=219 this merchandise exists since September. Where can one see the account records for September and October?"
    author: "Anonymous"
  - subject: "this month's winner?"
    date: 2002-11-08
    body: "according to the page that came up after registering, this month's winner is one Thomas Birke. congrats to Thomas.\n\nso ... .... who is Thomas Birke?"
    author: "Aaron J. Seigo"
  - subject: "Re: this month's winner?"
    date: 2002-11-08
    body: "How much did he win?"
    author: "Anton"
  - subject: "Re: this month's winner?"
    date: 2002-11-08
    body: "I was wondering about that too. The page said something about Thomas being a kdepim contributor. Unfortunately, Thomas isn't listed in kde-common/accounts and I couldn't find any email written by him in my two month old kdepim list archive, so I'm not exactly sure who is he."
    author: "Zack Rusin"
  - subject: "Re: this month's winner?"
    date: 2002-11-08
    body: "gg:Thomas Birke KDE\n\nhttp://lists.kde.org/?w=2&r=1&s=birke&q=a\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: this month's winner?"
    date: 2002-11-08
    body: "there shouldn't be any winner yet, we just got the pages up a couple days ago, but as has been stated the merchandise has been up for some time.  It's test data.  We're going to start the disbursements in December so that there is a decent amount of money now that it has officially been made available.\n\n"
    author: "Shawn Gordon"
  - subject: "Re: this month's winner?"
    date: 2002-11-08
    body: "you think i didn't try that before I posted? ;-)\n\nlists.kde.org brings up one posting by Thomas Birke to the bind-users list. that isn't even a KDE list. the google search (exactly the same one i did actually =) brings up the homepage of a fellow who has a link to kde.org on his website and that's it. so i'm still not really sure who Thomas Birke is in relation to KDE.\n\ncongrats to him all the same!"
    author: "Aaron J. Seigo"
  - subject: "yes .."
    date: 2002-11-08
    body: "I like the idea ...\n\nSay .. are these logo's free to use and if so where can I find them? Maybe there should be some poloshirts as well as developers these days are very fond of these. Ask them ... :-)\n\nThere has been some discussion on the kde-promo mailinglist about new kde-logos and also a way to sell merchandise to support the KDE-project. \n\nTrouble is that shipping merchandise overseas cost a lot of money. Could get some stuff from kernelconcepts (ask Eva Brucherseifer), but in general it is very hard to get reasonably priced T-shirts/ poloshirts. \n\nFab"
    author: "Fab"
  - subject: "What About Contributors That Aren't \"Developers\""
    date: 2002-11-08
    body: "Will translators, artists, etc., which are considered non-developers, also be part of this support program? I think they should not be left out, since their contributions are just as important as the coders'."
    author: "Menil"
  - subject: "Re: What About Contributors That Aren't \"Developers\""
    date: 2002-11-08
    body: "Hadn't really thought about it before, but that is a good idea, as long as they are doing active contributions to the project.  Go ahead and sign up."
    author: "Shawn Gordon"
  - subject: "Re: What About Contributors That Aren't \"Developers\""
    date: 2002-11-08
    body: "Well, in my opinion they should. My personal stance is that they must either have:\n\n   a.) Have contributed extensively to the project (not just occasional patches) or;\n\n   b.) Actively maintain a program, documentation, or translation (for a program or document) as maintaining is generally a rare thing people want to do.\n\nBoth of those would be a clear way to stand out, but I don't think you need to code. As Shawn said, Shawn will be looking at the CVS logs to find out how much you contribute. The CVS logs don't indicate whether or not it's code."
    author: "Nick Betcher"
  - subject: "Re: What About Contributors That Aren't \"Developers\""
    date: 2002-11-08
    body: "I'm translator and our projectmanager does CVS. Does this get my in de CVS-logs?\n\nFab"
    author: "Fab"
  - subject: "Re: What About Contributors That Aren't \"Developers\""
    date: 2002-11-13
    body: "Yeah, I was going to point that out too...  I don't have a CVS account, 'cause only project managers do for documentation..."
    author: "VrtlCybr2000"
  - subject: "Who manages donations?"
    date: 2002-11-08
    body: "Hello,\n\nI have a small company and germany and make some profits selling products and installations using KDE. I like the project very much and the thoughts of open source and free software. So what I do, I give them the software for free, but I don't work for free, so my time need's to be paid.\nIn the next step I thought about, how to make sure that development of KDE continues or even get's faster:)\nWell, it will get faster and better if developers have the possibility to work full time on the project! Because of that, I think about something like, all companies which are making money, selling KDE or free-software, should pay about 2-3% of the deal volumina, to support the KDE-Project!\nI think, with this money, more the one developer can get a job from the KDE-donations.\nSo, has anybody an idea, how to implement something like this?\nI will support programs like this!\nbr philipp"
    author: "Philipp Frenzel"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: ">> I have a small company and germany....\n\nwow, you are the owner of germany? :)))"
    author: "ac"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "No, I am...\n\n;-))\n"
    author: "Tim Gollnik"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "Philipp, you sell KDE-desktops? If so .. can I contact you for some info on that? \n\nFab"
    author: "Fab"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "First,\nsorry for the typo, but my english is not the best;)\nI meant \"in germany\"\n\nAnd \"yes\", u can contact me if u are interested in Linux with KDE,\nbut we follow our business in the south (Raum Stuttgart und Ostalb, Alb)\n\nGru\u00df Philipp"
    author: "Philipp Frenzel"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "Philip,\n\ndon't get me wrong ... I'm a already a Linux/kde-user. I just asking this because I want to have some case studies for use during events. SO if that's ok than I contact you. \n\n\nFab "
    author: "Fab"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "I would bring this up with KDE E.v.  and see what they say.  The recipient of the donations should probably be decided by them or an election of some sort ...  Interesting idea though ;-)"
    author: "Adam Treat"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "I'm not sure I see the reason for having KDE E.v. involved, if they wanted to do something like this, they could have done it.  If they decide that they want to do it instead of us, then we'll let them, but we thought to take some initiative on this since no one else was doing it, and we will also have full accounting out in public view so that there are no secrets as has gone on with some other KDE related projects."
    author: "Shawn Gordon"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "I think it is a good step from thekompany to make this offer.\nAs I said, it's very good to code free software, but people need money to pay theire flats, food...\nSo we need to establish a way, that companies making money with free software, and I speak of my own company too, should donnate money to the project. \nI don' t want to set the focus to KDE only, because other projects should also be supported.\nPerhaps somebody like theKompany, or like u shawn, could make a draft or recommendation for other companies, that the money will be given to the right developers.\nWell, it' s late, perhaps I repeated some sentences, but I hope the issue is visible:)"
    author: "Philipp Frenzel"
  - subject: "Re: Who manages donations?"
    date: 2002-11-08
    body: "We struggled with the whole idea of how to allocate the money and then just decided that if we or anyone made some decision on it, then we would be seen as judging the relative merits and being subject to some bias, so we decided on the random thing.  Our other way is to contribute projects or code where possible.  We haven't done so much of this recently because of financial pressures, but we will do more again in the future.\n\nI suppose the easiest way for you to make a decision is if you are particularly interested in a piece of software, then approach the author and offer to pay him to do specific work for you.\n\nShawn\n"
    author: "Shawn Gordon"
  - subject: "Re: Who manages donations?"
    date: 2002-11-09
    body: "Donations to KDE developers in general are best handled by KDE e.V., see http://www.kde.org/support.html. KDE e.V. has in the past given money to developers to repair broken hardware,  to get new machines for them, to run KDE's servers and also for travel/hotel costs, etc. for expos, hacker meetings or when KDE developers present KDE on conferences.\nThe members of KDE e.V. are KDE's core developers and they decide about the money. KDE e.V. is a german non-profit organisation, which will soon be \"gemeinn\u00fctzig\". We are also planning to have some kind of supportive membership in order to support the e.V. regularly.\n\nGreetings,\neva"
    author: "eva"
  - subject: "\"Wet  Konqui-T-Shirt\" Kontest!!"
    date: 2002-11-08
    body: "Great :)\nNow what about some \"wet konqui-t-shirt\" kontests? \nWe could put the pictures at http://babes.kde.org - it would beat the crap out of kde-look when it comes to eye candy ;) \nSo what do you think?\n\nJad"
    author: "Jad"
  - subject: "Re: \"Wet  Konqui-T-Shirt\" Kontest!!"
    date: 2002-11-08
    body: "Some male developers could act as modell too (wearing KDE requisites) and the pictures could put up at http://women.kde.org. Remember, there are more females than males on earth to become KDE users!"
    author: "Anonymous"
  - subject: "Re: \"Wet  Konqui-T-Shirt\" Kontest!!"
    date: 2002-11-08
    body: "One the other hand there are more more male KDE developers and we should keep them happy. But why not :) I think both could be done :)\n\nJad"
    author: "Jad"
  - subject: "Re: \"Wet  Konqui-T-Shirt\" Kontest!!"
    date: 2002-11-08
    body: "And who's gonna don those wet shirts? I don't think I'm the only one who would shudder at the thought of seeing geeks in wet t-shirts on stage... hiring some cute female models, on the other hand, sounds attractive, but would eat up all the profits ;-)"
    author: "Snarf"
  - subject: "Re: \"Wet  Konqui-T-Shirt\" Kontest!!"
    date: 2002-11-08
    body: "It could work the other way around. You'd need to have a konqui t-shirt to enter the Kontest and the award could be more KDE merchandise. So buy KDE Merchandise and go look for hot chicks (in case you're not one) ;)\nAny even if we did it some other way and in the end there was no profit, which KDeveloper is going to care about a couple of bucks when he can be awarded a poster with the \"wet konqui-t-shirt\" kontest winner :)\n\nJad\n\n"
    author: "Jad"
  - subject: "Re: \"Wet  Konqui-T-Shirt\" Kontest!!"
    date: 2002-11-09
    body: "I recall that some KDE guys are pretty buff ( there are even some pictures ;-)\n"
    author: "Roberto Alsina"
  - subject: "Merchandise"
    date: 2002-11-08
    body: "I'm not really a shirt-and-logo sort of guy (except I did like my \"Remember, Sex and Drugs and Rock'n'Roll are only substitutes for caving\" one), but I've got a stuffed Tux (one of the 8-inch .... err, 20cm .... ones) and (puts on fire retardant clothing) a similary sized Ximian monkey (well, they were giving them away and I felt obliged to help relieve them of some income) (fire retardant clothing off), but: can someone tell me where I can get a Konqui the same size. I've seen adverts for much bigger ones, but I want one to sit on the shelf. Hey, and if someone could to Konqui *and* Kate that would be really cool. Even if my wife says I am becoming a sad b*****d.\n\nMike\n"
    author: "mike ta"
  - subject: "Re: Merchandise"
    date: 2002-11-08
    body: "See http://www.kde.de/misc/kde-stuff.php for a 25cm cute one (produced by www.steiner-pluesch.de/). Available at http://www.emedia.de, http://www.freibergnet.de/ or http://www.kernelconcepts.de. The last one also sells the KDE pins sold on LinuxTag 2002."
    author: "Anonymous"
  - subject: "Re: Merchandise"
    date: 2002-11-08
    body: "English version of above KDE page is http://www.kde.org/kde-stuff.html."
    author: "Anonymous"
  - subject: "That site runs IIS on Win2k"
    date: 2002-11-08
    body: "Has anyone noticed that the site runs IIS on Win2k?"
    author: "Aidan Delaney"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-08
    body: "what site?  Our site runs Apache on Debian.  I don't know or care what Cafepress is running on :)."
    author: "Shawn Gordon"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-08
    body: "But I do.  Is there a way to move the KDE merchandise to an Apache and Linux webstore?  This will reflect poorly on our ability to promote Open Source.  Only when there's no Open Source webstore that will host kde merchandise should we use a closed source webstore.  Until then, I'm not going to make a purchase.  I hope you understand.\n\n"
    author: "Anonymous"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-08
    body: "I'm sorry, but that is just lame.  We don't control how the purchasing takes place, and you aren't hurting anyone but the KDE developers by making a decision like that.  Do you understand how Cafepress works?"
    author: "Shawn Gordon"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-09
    body: "It's not lame ... it's marketing.  If you write a letter to cafepress and express your concern about their website running on IIS and Win2k, and ask them to create a migration strategy with some actionable efforts then I will consider purchasing the merchandise.  "
    author: "Anonymous"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-09
    body: "OMG! Why don't *you* write a letter? All profits go to the developers, remember?? If you want to help promote KDE and free software buy a shirt, you'll be marketing in motion every time you put it on. "
    author: "150"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-09
    body: "Well, some people don't believe that the ends justify the means.\n\nEven though your newest announcement is a thing that will earn your company some deserved positive recognition, some will see the use of Microsoft servers to pay KDE developers as \"robbing Peter to pay Paul,\" as it will be indirectly funding a company that actively fights free software like KDE.\n\nSo rather than jump on people who feel that way, why not just have some understanding?"
    author: "Neil Stevens"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-08
    body: "so what? what relation does cafepress have to do with open software anyways? they are basically the largest custom merchendising company on the web."
    author: "asf"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-08
    body: "For one, if we truly believe in our product, we should by all means use it and encourage others to do so.  Having the merchandise hosted on an IIS and Win2k show lack of judgement on our part because we haven't done enough research to find a merchandising company that runs on Apache and Linux.  We can say Open Source is cool, this and that, but what ultimately be a deciding factors are the people who are actually using it.  Much like smoking, people tell you to not smoke but they smoke.  Do you think you will trust what they say?  "
    author: "Anonymous"
  - subject: "Re: That site runs IIS on Win2k"
    date: 2002-11-08
    body: "Where's all this \"we\" stuff.  It sounds like Shawn took the initiative.  I didn't see \"Anonymous\" anywhere in the article as a contributor to this effort.\n\nIt's a big world out there ... get used to it.\n\nJim"
    author: "Jim"
  - subject: "wow"
    date: 2002-11-08
    body: "very good idea!"
    author: "asf"
  - subject: "Next paycheck..."
    date: 2002-11-08
    body: "Next payday, I'm buying the messenger bag. I ride my bike to work, rain or shine, and need a good bag to take my lunch, books, etc. \n\nI like the yellow, and the messenger bag seems to have the best logo colors (black and red, as far as I can tell).\n\nGood show! I'm happy to contribute."
    author: "Shamyl Zakariya"
  - subject: "another KDE shop at Cafepress"
    date: 2002-11-08
    body: "I've had a KDE-themed shop up at Cafepress for over a year now. Sales weren't too bad at first, but have really tapered off what with the poor economy, job concerns in the tech sector and such. Anyway, I've been forwarding the profits to Mirko Boehm over at KDE e.V. whenever they come in. \n<p>\nFeel free to check it out:\n<p>\n    <a href=\"http://www.cafeshops.com/kde\">http://www.cafeshops.com/kde</a>\n<p>\nPerhaps I should work with Shawn to merge these two stores under their control ?\n"
    author: "David Huff"
  - subject: "um?"
    date: 2002-11-08
    body: "KDE thongs, huh?  *LOL*"
    author: "Navindra Umanee"
  - subject: "Re: um?"
    date: 2002-11-09
    body: "You know I just went in to add the polo shirt and saw the thong and the x-mas ornaments and decided to add them.  I thought the thong was funny as hell, especially with the slogan, but then again...."
    author: "Shawn Gordon"
  - subject: "Re: um?"
    date: 2002-11-09
    body: "x-mas? where?\n\nJad"
    author: "Jad"
  - subject: "Re: um?"
    date: 2002-11-09
    body: "Ok got it!\nhere: http://www.cafeshops.com/cp/store.aspx?s=kdegear and I looked\nhere: http://www.cafeshops.com/kde\n\nJ.A.\n"
    author: "Jad"
  - subject: "Re: um?"
    date: 2002-11-09
    body: "Yeah, I just added them yesterday. A pair of those + the baby-tee would make any woman a geek sex goddess... ;->"
    author: "David Huff"
  - subject: "Re: um?"
    date: 2002-11-09
    body: "Lol!! It's great!! :)\nI just whish there were Hooded Sweatshirt with konquis, and something on the back to, like the regular Sweatshirt.\n\nVery cool shop! Keep it up!\nJad"
    author: "Jad"
  - subject: "Re: another KDE shop at Cafepress"
    date: 2002-11-09
    body: "> Perhaps I should work with Shawn to merge these two stores under their\n> control ? \n\nIf you do, please keep all items!\nJad"
    author: "Jad"
  - subject: "Thong?"
    date: 2002-11-08
    body: "Is any one else incredibly impressed to see the kde thong?  That's a bold move when first trying out merchandising.  "
    author: "Jon"
  - subject: "Re: Thong?"
    date: 2002-11-09
    body: "I miss the matching bra ;-)\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: Thong?"
    date: 2002-11-09
    body: "Hacker's phantasies.... \n<p>\nA women's T-shirt (as can be seen <a href=\"http://www.cafeshops.com/cp/prod.aspx?p=kde.340642\">here</a> or <a href=\"http://www.cafeshops.com/cp/prod.aspx?p=kde.3808939\">here</a>) would me much nicer and more practical.\n<p>\neva"
    author: "eva"
  - subject: "Re: Thong?"
    date: 2002-11-09
    body: "Dunno, at least here in the US, many (younger) women wear thongs these days. Not sure about Europe."
    author: "hmm"
  - subject: "Re: Thong?"
    date: 2002-11-09
    body: "Impressed? Bold? Nah.\nBut I think it's funny and a good idea :)\nSwimsuits  or bikinis would be cool too.\n\nJad"
    author: "Jad"
  - subject: "What's with the new logo"
    date: 2002-11-08
    body: "I was just about to purchase the messenger bag when I had a look at the logo. Where the H-E double hockey sticks did that logo come from? That is not the standard logo for KDE? Hmm, you just lost my money!\n\njr"
    author: "Jules"
  - subject: "Re: What's with the new logo"
    date: 2002-11-09
    body: "no, *I* didn't loose your money, some KDE developer just lost it.  See my previous comments for where the logo's came from."
    author: "Shawn Gordon"
  - subject: "Another Suggestion"
    date: 2002-11-09
    body: "KDE the FLAME THROWER!  (the kids love this)\n\n:-)"
    author: "Xanadu"
  - subject: "thong...."
    date: 2002-11-11
    body: "Gotta wonder how many women will be wearing the official KDE thong with the come get some logo....\n\nKarl"
    author: "Karl"
  - subject: "Re: thong...."
    date: 2002-11-11
    body: "official, nothing.  this is no official."
    author: "ac"
  - subject: "Colours on shirts (other than white)"
    date: 2002-11-12
    body: "Nice logos!\n\nBut - why are all the shirts white? (ash grey counts as white for me) \nIt would have been very nice with something dark - brown, black, blue or green, just something not bright and white!\n\nI think I will by a white one anyway\n\nand: Why the thong? - why not mens underwear too? \n\n(Well, I suppose I could try wearing the thong :-P)\n\nDag"
    author: "Dag"
---
<a href="mailto:shawn@thekompany.com">Shawn Gordon</a> of 
<a href="http://www.thekompany.com">theKompany</a> has found 
<a href="http://www.thekompany.com/merchandise/">a new and innovative way to support KDE development</a>: 
a new line of 
<a href="http://www.cafeshops.com/cp/store.aspx?s=kdegear">KDE-themed merchandise</a> has been made available and each month a random KDE developer will be awarded with the profits from the sale.
<!--break-->
<p>
"I came up with this after discussion with some of the core KDE developers," Gordon says. "There is no decent KDE merchandise and a lot of people would like to have some. We've made some original designs for the various products and have made them available at slightly above cost to the general public.  We don't want to, nor will we make money off of this; this is what will happen with the profits:
<p>
"We have made 
<a href="http://www.thekompany.com/merchandise/subscribe.php3">a registration system</a> that KDE developers can sign up for to apply for it as a kind of 'grant'.  All the money made from the month will go to a randomly selected person from the list, you can only win once until everyone else who is in the system has won.  We will verify that in fact you are contributing to KDE and not just trying to get free money by checking the CVS logs, and the money will get paid out."
<p>
"The idea is to try and help support KDE developers that aren't hired full time to work on KDE.  The amount of money they can earn is directly related to how much merchandise gets sold, so it is in everyone's best interest to try and promote this as much as possible.  theKompany gets nothing out of this, but is simply the facilitator. We're hoping this can do some good and provide some incentive for all those hard working KDE developers that don't get any compensation for their work," Gordon said.
