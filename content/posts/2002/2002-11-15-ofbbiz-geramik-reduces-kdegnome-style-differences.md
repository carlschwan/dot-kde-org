---
title: "OfB.biz: Geramik Reduces KDE/GNOME Style Differences"
date:    2002-11-15
authors:
  - "tbutler"
slug:    ofbbiz-geramik-reduces-kdegnome-style-differences
comments:
  - subject: "why this could be good"
    date: 2002-11-15
    body: "Red Hat now has the option of defaulting to Keramik/Geramik under KDE and BlueCurve under GNOME.  It would be the best of both worlds for everyone.\n<p>\nDistributors like Mandrake and SuSE can now integrate all applications in the default KDE desktop.  It will be great.  Look at a great screenshot of Mandrake with Keramik/Geramik: <a href=\"http://www.kde-look.org/content/pics/3957-1.png\">3957-1.png</a>"
    author: "anonymous"
  - subject: "consistency is nice, but"
    date: 2002-11-15
    body: "this just looks awful!\nFonts and icons are way to big!"
    author: "katakombi"
  - subject: "Re: consistency is nice, but"
    date: 2002-11-15
    body: "no, no. that's just a problem with gtk, not geramik."
    author: "static"
  - subject: "Re: consistency is nice, but"
    date: 2002-11-15
    body: "It does look awful.\n\nSorry, but geramik doesn't make GTK apps look like kde apps.  They still look like GTK apps with big, and inconsistent (w/ the app) buttons."
    author: "Jeff"
  - subject: "Re: why this could be good"
    date: 2002-11-15
    body: "redhat probably won't do it due to their queer obsessive fascination with gnome."
    author: "static"
  - subject: "Re: why this could be good"
    date: 2002-11-15
    body: "what's exactly is queer with having a facination with gnome?\n\nI won't even go into suse's facination with kde."
    author: "asf"
  - subject: "Re: why this could be good"
    date: 2002-11-18
    body: "KDE is the standard desktop. Things that Red Hat have done recently have not been anything to do with standards. Look at them removing MP3 and MPEG support from their latest desktop and crippling the KDE environment.\n\nThe new KDE 3.1 and associated themes look really cool!\n\nMr Fizz"
    author: "Mr Fizz"
  - subject: "Re: why this could be good"
    date: 2002-11-21
    body: "Sir,\nHow exactly was KDE \"crippled\" by redhat?\nredhat's distribution of KDE was completely functional.  "
    author: "anonymous howard"
  - subject: "Re: why this could be good"
    date: 2002-11-22
    body: "This includes the menu editor?"
    author: "Anonymous"
  - subject: "Re: why this could be good"
    date: 2002-11-22
    body: "> redhat's distribution of KDE was completely functional.\n\nAnd how is kappfinder working? I hear it was removed."
    author: "Anonymous"
  - subject: "Re: why this could be good"
    date: 2002-11-22
    body: "so your example of how redhat's KDE was \"crippled\" is that kappfinder is no longer included?\n\n\n\n"
    author: "anonymous howard"
  - subject: "Re: why this could be good"
    date: 2002-11-23
    body: "You spoke about completeness, that's was what I quoted."
    author: "Anonymous"
  - subject: "Re: why this could be good"
    date: 2002-11-23
    body: "No, what i spoke about was the fact that redhat's version of KDE was not \"crippled.\"\n\none missing application does not make KDE crippled.  besides, it shouldn't be the duty of the user to run an application that searches for legacy applications and to have them added into the menu.  the desktop environment should do that automagically."
    author: "Anonymous Howard"
  - subject: "On the right track... but!"
    date: 2002-11-15
    body: "If only everyone would work on a common way to use themes. it seems like such a waste having to recode a theme multiple times depending on the desktop enviorment you wish to support. Just think load up a GTK app and it automaticly looks like your kde enviorment, or vice versa. to bad it will most likely never happen :("
    author: "David Ricciardi"
  - subject: "Re: On the right track... but!"
    date: 2002-11-15
    body: "And why excactly should the 2 desktops be integrated again ? People tend to forget they are 2 seperate companies with 2 distinctly different styles and are trying to take their software in 2 different directions - so why is everyone trying to force them to integrate in the first place ? Personally I like having the option to switch between the 2 - it's the main avantage Linux has over Windows in the first place: the freedom to choose..."
    author: "Chris Spencer"
  - subject: "Re: On the right track... but!"
    date: 2002-11-15
    body: "The problem is not whether KDE and GNOME must merge. They shouldn't and, anyway, they won't. The problem is: while it's perfect to have two major desktop engines, with great underground libraries it's stupid to have different looks and different feels. I mean, would you find normal not to be able to use some wallpapers on Linux because they're 'for windows' ? That would be non-sense. Well, for the average user, choosing a desktop is not the same as choosing a wallpaper, an icon theme, a style etc... it should be a matter of underground libs, not external look and feel.\n\nLook: I prefer KDE but if I need GIMP, I'd like it to look like all my desktop. That doesn't mean that I want KDE and GNOME to merge !\n\nMore over, when I add some fonts to my system, I want them to work in koffice as well as abiword. If I set up 'Lucida Sans' as my default font for KDE, I want it to be the default font for GTK apps too because that's logic in fact.\n\nEtc etc etc..."
    author: "Julien Olivier"
  - subject: "Re: On the right track... but!"
    date: 2002-11-15
    body: "If you divide the world into equal Qt and GTK+ halves, your attitude might make sense. But that's not how the world is.\n\nAt work I use KDE on FreeBSD. Native to the system are not only Qt and GTK+ applications, but XUL, Motif, Xt, FOX and FLTK applications. And what about Gkrellm and Xmms skins? I also have Acrobat reader and Realplayer. What about them. Or those remote Solaris apps I run on the desktop like Clearcase and Framemaker?\n\nIf only things could be like Windows! There you have clashing styles between Quicktime, Realplayer, MediaPlayer and WinAmp. And you have non-M$ looks in Netscape. I've run across installation and driver configuration programs that look like funky web pages. And of course, every new release of MSOffice introduces a new and inconsistant UI.\n\nHmmm, may be the Macintosh is better. But wait! Apple's own Quicktime has a brushed metal look that clashes with Aqua.\n\nI don't hear Windows and Macintosh users demanding that some dictator arise and mandate uniform looks and feels. Why does Open Source need one?\n"
    author: "David Johnson"
  - subject: "Re: On the right track... but!"
    date: 2002-11-15
    body: "You say Apple's quicktime clashes with aqua, this is true, but look what they did with Aqua.  They made it so programs written for Aqua will all have similar usabilities.  Is it the Cocoa interface?  i forget.  But it makes things a lot better looking and a more seamless interface."
    author: "standsolid"
  - subject: "Re: On the right track... but!"
    date: 2002-11-15
    body: "If windows or Mac do things in a bad way, we shouldn't do like them. The good way is 'consistency' between apps. If we can't have Acroread or Real Player look integrated, too bad but that would be better than nothing. More over, maybe when we'll have a stable, consistent look and feel on both KDE and GNOME, other toolkits will try to imitate it. I mean, which look do you expect OpenOffice to mimic now ? GNOME one ? KDE one ? Windows one ? There is no dominant look on linux and that's the problem."
    author: "Julien Olivier"
  - subject: "Re: On the right track... but!"
    date: 2002-11-16
    body: "\"There is no dominant look on linux and that's the problem.\"\n\nI'm still not convinced it is a problem. I used Windows and Macintosh as examples of popular platforms that do not have consistant interfaces, and whose users are NOT complaining about it.\n\nOne of the recurring mantras of the Linux community is \"if we don't do xxx then we will never be accepted.\" But other platforms are accepted that have exactly the same problems. OSX has a more integrated look than Windows, yet I fail to see the hordes of users migrating from Windows to OSX because of it. Maybe the problem really isn't as big as people claim.\n\nThere are some things that need to be standardized, such as drag-n-drop and menu layouts. But a mere difference in the look of widgets is not that big a deal. In my opinion. Of course, I use FreeBSD instead of Linux, so I've never seen the need for a look-and-feel czar."
    author: "David Johnson"
  - subject: "Re: On the right track... but!"
    date: 2002-11-16
    body: "I'm not saying that we must have a consistent look and feel in order to be accepted and get users. I don't care about how many users Linux has. Really. There are, of course, bigger problems that Linux has to solve but that's not the point here.\n\nWhat I say is that I prefer an unified desktop than a patchwork. If Windows users and Mac users prefer a patchwork, that's great for them. But in the free world, we have the possibility to make things better and we shouldn't say 'Windows and Mac don't have this feature so it's not important'. Else, we would be glad with virii, lack of freedom, unstable software etc...\n\nMore over, Windows has a better integrated printer dialog: you set up printers once and every app use it. That's not the case on Linux. That's an example of details that make Linux look weird for beginners."
    author: "Julien Olivier"
  - subject: "Re: On the right track... but!"
    date: 2002-11-16
    body: "Uhm, the real matter here is usability for the new users. First of all both worlds, Windows and Macintosh, are extending and tweaking their already existing UI with new features while many core parts stay true over all system revisions and are often still set up globally. On the other hand Linux, as well as BSD, has several toolkits which are fundamentally different and just now things are moving in the direction to avoid irritations due to them for new users.\n\n-----\nI'm still not convinced it is a problem. I used Windows and Macintosh as examples of popular platforms that do not have consistant interfaces, and whose users are NOT complaining about it.\n\nOne of the recurring mantras of the Linux community is \"if we don't do xxx then we will never be accepted.\" But other platforms are accepted that have exactly the same problems. OSX has a more integrated look than Windows, yet I fail to see the hordes of users migrating from Windows to OSX because of it. Maybe the problem really isn't as big as people claim."
    author: "Datschge"
  - subject: "Re: On the right track... but!"
    date: 2002-11-17
    body: "\n> Hmmm, may be the Macintosh is better. But wait! Apple's own\n> Quicktime has a brushed metal look that clashes with Aqua.\n\nThat's not a good example.  They do this with iTunes too, but it's only the 'skin' that's different. It might as well just be a change of colour.  All the buttons, sliders and other widgets remain the same."
    author: "John"
  - subject: "Re: On the right track... but!"
    date: 2002-11-15
    body: "That's a kickass idea, and you wouldn't be the first to have thought about it. There's absolutely zero reason why a widget toolkit theme system couldn't be specced out on freedesktop.org, the only reason why it hasn't happened is that it's a big task and nobody has yet had time to do it. \n\nI dunno how much you've done/are doing for free software, but if you want to jump in that'd be a great place to start."
    author: "Mike Hearn"
  - subject: "The worst"
    date: 2002-11-15
    body: "What about a patch to swap Gnome2's dialog button orders back to Gnome1/KDE order?"
    author: "Anonymous"
  - subject: "Re: The worst"
    date: 2002-11-15
    body: "Why? The current GNOME2/MacOS button order is more usability friendly. It's more applicable to how people in either RTL or LTR languages think. GNOME1/KDE/WINDOWS show it opposite than what people think internally in their heads."
    author: "asf"
  - subject: "Re: The worst"
    date: 2002-11-15
    body: "If it is more usability friendly then why do I have problems with it and am confused that different X clients use different button orders? If I use MacOS I know that everything will behave different (but equal!) so that's not a problem. For Unix/Linux it introduces needless inconsistency. Why should I have to adjust my internal thinking (how?) just because someone suddenly thinks that I have to in contrary to previous thinking?"
    author: "Anonymous"
  - subject: "so patch KDE, not gnome"
    date: 2002-11-15
    body: "gnome is doing the right thing"
    author: "noone"
  - subject: "Re: The worst"
    date: 2002-11-15
    body: ">  it introduces needless inconsistency\n\nfix KDE then. "
    author: "asf"
  - subject: "Re: The worst"
    date: 2002-11-15
    body: "No, and you wrongly assume that only Gnome and KDE X clients exist."
    author: "Anonymous"
  - subject: "Re: The worst"
    date: 2002-11-16
    body: "of course other clients exist. I wasn't implying as such. anyways, inconsistency is commonplace in the world of XWindows, so your point is moot. much more important to improve usability, imho."
    author: "asf"
  - subject: "There are more such themes"
    date: 2002-11-15
    body: "Have a look in the GTK section of KDE-Look.org. There you will also find matching GTK themes for Liquid and QNX, although these do not follow the KDE colors."
    author: "Yaba"
  - subject: "Good but bad"
    date: 2002-11-15
    body: "Hi\n\nI think this is a great idea to make matching styles. It would even better if GNOME and KDE acted the same (file/printer dialogs, ioslaves for example) and had common icons.\n\nBut I still think Keramik is not so great. Neither is liquid by the way. I mean, those are nice-looking styles and they'll probably please a lot of people but I think they're \"too much\".\n\nLook at GNOME2's default look: it's simple and elegant. Isn't it possible to reproduce GNOME2's default theme in KDE ?\n\nI think Keramik/Geramik and Liquid should be options for those who like \"speacial\" eye-candy but not for everyone.\n\nI must admit that I really like what RedHat did with BlueCurve. My only problem is that I don't like the WAY they did it. Isn't it possible for KDE people to take BlueCurve's code, make it better and include it in KDE CVS (with a different name) and for GNOME people to do the same ? SO we would have a simple default style for both KDE and GNOME (modified BlueCurve) and an \"artistic\" default style for both of them (Keramik/Geramik).\n\nThe same for icons: extend GNOME icons with KDE \"gnomized\" icons to make a standard icon theme for both KDE and GNOME and extend crystal with some GNOME \"crystalized\" icons to make an \"artistic\" icon theme for both KDE and GNOME.\n\nThis post is not a flame against KDE artwork. I just say that some people prefer keramik/crystal and some prefer GNOME artwork. And in my case, I love GNOME artwork but I prefer KDE apps. So I have to chose between look and feel. I chose 'feel' (KDE) on my work's computer where I need code colorization, a working ftp ioslave etc..., and 'look' (GNOME) on my home's computer where I just browse the web using Mozilla and read emails with Evolution.\n\nWhat do you think ? Do you like keramik ? Do you like GNOME style ? What about BlueCurve in CVS ?"
    author: "Julien Olivier"
  - subject: "Re: Good but bad"
    date: 2002-11-15
    body: "I too really prefer clean/simple to cool/artistic. Of course, taste differs, which is the very reason for having theming in the first place. As a Gnome user, I think it can only be good for everybody if the most used themes are ported across; with the exception of a few ranting zealots, most of us do run apps from both desktops and it is nice to have an integrated look for all your stuff.\n\nThat said, having a full common theme format will never happen. This is not due to any animosity between desktop developers, but due to the simple fact that many themes (at least for gtk) are not just an xml file and some icons, but theme engines, coded as a plugin for the toolkit (thinice being maybe the most used right now). I assume you can do similar stuff for Qt. These won't be portable, ever. What _could_ be done is to write an engine for gtk and for Qt that accepts the same theme format. The downside is that it would be sort of a least common denominator of both toolkits' capabilities (theme creators would not have the same fine-grained control over their creations) and would likely not be as fast or efficient than an engine used only for one of them.\n\nAs an aside, the \"where is the close button\" thing is really a non-issue. I have observed users using both Gnome2 and KDE apps at the same time and there is never any confusion. It seems this is due to the 'safe' option being highlighted by default, and users will take that - rather than position - as a cue for what button to press.\n\n/Janne"
    author: "Janne"
  - subject: "Re: Good but bad"
    date: 2002-11-15
    body: "Texstar made the blue curve theme available on his ftp site.  This is advantageous for those who want the integrated look, but doesn't mess with the features of kde or gnome, ie single-click versus double-click or any other such controversies.\n"
    author: "Mike"
  - subject: "Re: Good but bad"
    date: 2002-11-15
    body: "Texstar's Bluecurve (renamed to Freecurve due to legal reasons) is sooo cool!\n\nGet it from http://www.pclinuxonline.com/"
    author: "meee!!!"
  - subject: "Re: Good but bad"
    date: 2002-11-16
    body: "Where exactly?  Looked everywhere with no luck :-(."
    author: "Clueless"
  - subject: "Re: Good but bad"
    date: 2002-11-16
    body: "I am a very simple person I guess, I really like keramik, same with Liquid...I find the\nthemes beautiful and enhance a person's kde experience.\n\nRedhat is doing it their way with bluecurve and I respect that, But I guess that will be one of my reasons of not using redhat, I appreciate KDE and is very satisfied with it, (kde user since the very first version of kde). \n\n"
    author: "Kenneth Oncinian"
  - subject: "Re: Good but bad"
    date: 2002-11-17
    body: "Well, I like the keramik style. I just like the \"flat\" windows, i.e. the flat window borders. And I think the icons are more abstract and that's more modern.\nAnd the buttons look more like the buttons on other modern devices. The old themes  use a button style that looks like a device from the 60s or 70s.\nBut I think that the UI of my desktop has to adapt to the modern styles. So I use keramik (and would us Aqua on Mac if I had one, and even prefer the new MS Windows XP desktop over the \"old\" Windows).\n"
    author: "Detlef Grittner"
  - subject: "common looks"
    date: 2002-11-15
    body: "ok, this is one step forward. what is still needed is:\n1. packages which include both gtk and kde version of some style -> and which can easily be installed using the control-center or gnome's equivalent.\n2. ditto for icons.\n3. a way to have gtk-apps use the kde-filedialog and the other way round.\n4. ditto for print-dialog.\n4. a way to include gnome-configlets (control-center modules) in the control-center.\n5. and a way for gnome/gtk to read other things from /.kde/config and take over those options.\n6. a common way to use kio-slaves. (this should probably be for all*nix-apps)"
    author: "Johannes Wilm"
  - subject: "Re: common looks"
    date: 2002-11-15
    body: "As for icons, the newest gnome beta (2.1.2) has support for Icon themes!....unfortunately, it's totally packaged differently than the kde icon themes, once you get under the size directories. This causes some pain and hassle in porting kde themes to gnome and vice versa. Perhaps there could be a conversion script, and perhaps this would be just too complex."
    author: "karl11"
  - subject: "Symbolic Links?"
    date: 2002-11-16
    body: "Could this problem be fixed with 1001 symbolic links, or is it more complicated than that?"
    author: "Joel"
  - subject: "Noatun"
    date: 2002-11-15
    body: "Hey, I just installed Mandrake 9.0. Now I can play like \n8 songs simultaneously with this noatun. Its really cool.\nGreat stuff!"
    author: "Mandrake"
  - subject: "Re: Noatun"
    date: 2002-11-15
    body: "for those who have slower computers, Rik Hemsley (kde developer dude) has written a very very fast fast fast ogg (not mp3. oh poo.) player 4 kde. is very difficult for me to make it skip. it plays without cutting out even when i have my system (750mhz) so overloaded my applications stop drawing themselves. is very nice.  well, when using arts it skips a bit under these circumstances. when using oss it doesnt.\n<p>\n<a href=\"http://rikkus.info/squelch.html\">squelch.html</a>\n"
    author: "static"
  - subject: "Re: Noatun"
    date: 2002-11-16
    body: "Big whoop. In BeOS, I used to be able to play 4 AVI's at the same time on an old PII-300. The system would max out at around 30 MP3 streams. Don't get me wrong, I love KDE (using it as my only desktop for a long time) but Noatun and aRts aren't exactly the most amazing parts of the project..."
    author: "Rayiner Hashem"
  - subject: "a common theme place"
    date: 2002-11-15
    body: "would be also good from a kde user standpoint since you wouldnt lose that kind of datas when you have to wipe out a .kde directory after upgrades or problems.\nThe themes datas are fairly consistent over releases and would only require some small scripts to actualise them after a kde version upgrade.\nRight now, it's a PITA. Either you lose all your datas and rebuild your prefs from the grounds up, or you copy .kde prior to the wiping out, and then manually copy directories and files afterwards by errors and tries.\nthere could be a common /usr/share/themes and ~/themes with inside directories for kde, gtk, gnome, qt, whatever other wms you use.\nIt would make also such cross wm theme like the one this news is about more consistent for people who want to use them even if they dont have qt or kde installed."
    author: "jaysaysay"
  - subject: "This is not about KDE & GNOME"
    date: 2002-11-15
    body: "This is about the desktop providing a common look and feel across all applications, not the porting of a theme to GNOME.\n\nCurrently, when a user runs a GNOME/GTK-based application under KDE, it looks out of place. The theme means that GNOME/GTK-based applications will look similar to KDE applications, and thus provide a more integrated experience for the user. Ok, provided that the KDE and GTK themes match. :-)\n\nBut this only solves one problem, and that is of look. It doesn't solve the feel problem, since KDE and GNOME do things differently. Personally, my heart tells me that the look is more important than the feel, but my head tells me otherwise. That is probably the same for most people, hence the reason so much effort goes into developing themes, and little is done on developing a shared user interface experience."
    author: "AccUser"
  - subject: "Re: This is not about KDE & GNOME"
    date: 2002-11-15
    body: "I think that effort has be done on look first. Because when the look of both desktops will be consistant, people will really see \"Linux\" as a unified desktop and will feel differences between KDE and GNOME as bugs. So they'll ask more and more consistancy between them and developers will start taking it very seriously (if they're not already). Nowadays, with so different looks, people expect apps to behave differently so everybody is accustomed to make a choice between KDE and GNOME and avoid the counterpart desktop."
    author: "Julien Olivier"
  - subject: "Re: This is not about KDE & GNOME"
    date: 2002-11-19
    body: "I absolutely agree with your point of view.\nMy heart says: Eye-candy!\nMy head says: Function!\n\nSo I settled on KDE with some Apple Platinum-like\ntheme and colourscheme, since I like that GUI a lot.\nI never owned one of these paradise fruit myself,\nsadly enough ;-)\n\nMaybe we\u00b4ll really see a shift towards \"same functionality\"\nwhen people (programmers?) are satisfied with the\nnumber of themes with the same look.\n\nIf I was into coding, I\u00b4d started doing it myself a while\nago..."
    author: "Ingo"
  - subject: "About usability..."
    date: 2002-11-15
    body: ">>> If KDE and GTK+ apps don\u00b4t act the same should they really look the same?\n\nIMHO, yes -- though I understand and find your observation important enough to imply that differences in appearance would be useful so the user is not misled.\n\nBut I think we\u00b4re in a process here. Everyone is starting to understand looks and feel do not necessarily imply the same internals. If we can have Gnome and KDE to be externally identical, then one can choose between them based on real technical advantages they have.\n\nThis, of course, doesn\u00b4t imply everyone should use Keramik/Geramik; we should have tons of themes so that one chooses a GTK theme he likes and the respective KDE alternative will be available. If I\u00b4m not wrong, the XFCE people already work on this for their upcoming version 4.\n\nSo, IMHO, we must accept some usability problems for the moment, hoping this \"sacrifice\" will bring one day more control for the user of his/her own desktop.\n"
    author: "Anonymous"
  - subject: "Re: About usability..."
    date: 2002-11-15
    body: "I agree wholeheartedly.  \"The perfect should not be the enemy of the good\" and so on.  This is a first step.  I think I'm safe in assuming the process won't stop here.\n\nAnd, on top of all that, the reason people use Gnome apps in KDE is because *they like them!*  If I wanted total UI consistency, I'd use Konqueror instead of Mozilla (not really a Gnome app but it's getting a GTK native theme)  I'd use [...umm, I don't actually know...] instead of the Gimp, and I'd use Konqui instead of gFTP for huge ISO downloads where resumable downloads are a necessity.\n\nBut, I like Mozilla so much better than Konqui that I can deal with the interface seams.  Gimp and gFTP have never let me down.  And now, *some* of the seams between the applications have been removed.  This is a good thing.\n\nNow if only I can get GTK apps and Wine apps to use kprinter instead of their own native print dialogs!  (I currently get two printer dialogs for all non-KDE apps so that I can use all of the kprinter goodness, but this bugs the hell out of my wife)"
    author: "ac"
  - subject: "Re: About usability..."
    date: 2002-11-16
    body: "Kinda offtopic, but...\n\nAre you saying you don't use Konqui for FTP because you need resumable downloads? Konqi has supported resuming downloads for a long time... Just ignore me, if that's not what you meant. :-)"
    author: "AC"
  - subject: "Re: About usability..."
    date: 2002-11-16
    body: "No, I'm saying I started using gFTP back when Konqui DIDN'T support resumable downloads.  But frankly, having grown used to it, I like gFTP and see no reason to switch.  (after all, I really don't have a toolkit bias)"
    author: "ac"
  - subject: "Great!"
    date: 2002-11-15
    body: "That's a great idea!\n\nI was just working on a GTK style which looks like KDE because I have to install a very user-friendly linux with many programs and KDE as desktop at school. (That's the same school, where I'm learning english...sorry about my english mistakes. Maybe my english looks a little bit like german...i'm still learning! :P)\n\nThe default look of GTK is very irritating for users who never worked with linux before.\n\nI think this is a little but important step to make linux more user-friendly!"
    author: "Marcel Schreier"
  - subject: "Re: Great!"
    date: 2002-11-15
    body: "the default look of gtk is very irritating."
    author: "static"
  - subject: "Re: Great!"
    date: 2002-11-16
    body: "gnome is drab like soviet era apartment block.\n\nI have tried to like it, and I probably use more\ngnome apps than kde but I  am sorry but I just can't.\n\nLiquid and Keramik are both very pleasing to the\neye albeit sometimes a little busy."
    author: "knome"
  - subject: "GTK2 version"
    date: 2002-11-15
    body: "Is it included? Is it planned?"
    author: "Junior"
  - subject: "Good! But ..."
    date: 2002-11-15
    body: "Any volunteers to port Light v3 theme in same fashion as well? :)"
    author: "Cliff"
  - subject: "What if..."
    date: 2002-11-15
    body: "<trolling>What if people stop WASTING time on 2 different desktops ?</trolling>\nIf we're talking about a merged user experience (both look and feel) about linux desktop - and we are, aren't we ? - what is the need for 2 different systems ?\nI mean, it's a good thing to have common themes, but is this really what will lead to a wider use of linux on the desktop ? Well I don't think so. I respect the work done on both, and I actually can't decide the one I prefer, but as soon as they offer the same set of features, they should (in a perfect world) MERGE. More talented programmers could then interact with a common goal, and better performance. Besides, mainstream users wouldn't be so confused."
    author: "@l3X"
  - subject: "Re: What if..."
    date: 2002-11-15
    body: "This is suggested the whole time, but is never, ever going to happens. For a start GTK and Qt are completely incompatible so what would have to happen is one desktop would die. Can you honestly see either camp allowing that to happen?"
    author: "Ed Moyse"
  - subject: "Re: What if..."
    date: 2002-11-15
    body: "With that kind of \"uniform\" thinking, I'm suprised you haven't joined a cult yet. Picking a desktop environment couldn't be that damn hard, so why bitch about them not being unified. And even if it was that hard to choose, you should be thankful that you actually HAVE 2 excellent desktops (unlike Windows) instead of pissing and moaning about the burden of having 2 great environments to choose from - yeah man I really pity you. They are 2 different styles and cater to 2 different groups of people that's why. That same kind of small minded thinking you're doing is what keeps people from using Linux in the first place. By your logic, one could say \"Why bother with more than one OS ?\" or \"Why make more than one distrobution of Linux\" or \"Why even bother with Linux in the first place and just stick with one version of Unix ?\" The same reason I just stated: The are different compaines with different objectives who meet the likes and needs of different groups of people, that's why..."
    author: "Chris Spencer"
  - subject: "Re: What if..."
    date: 2002-11-20
    body: "It's not about uniform thinking... I personally enjoy having the choice, if you bother to care. It was deliberately extreme, and no I'm not in any kind of cult, neither windowish nor OSSish. This comment was only about the USER side experience. And you really could have avoided the end of your comment, because it is SO far from reality... So step back and watch the big picture, instead of reacting before thinking.\nI already know that a merge won't happen anytime soon. I know that code should have to be thrown, and I would rather not be the one to tell one of the team to trash their wonderful work.\n\nOK, now that things are clear about my opinion, let's look at facts from the user side, because this side will decide the future of linux on the desktop. What is the need of 2 diffrent implementation if the user experience is the same ?\nWhy would GM build 2 cars with opposite inside designs but that people could not differenciate at all ? Nobody wins. The user doesn't have a choice, and GM has spent a lot of money in 2 designs and 2 production lines.\nIf it's about 2 styles and 2 groups of people, so be it. But it has a huge cost for the linux community.\nI would rather have ONE desktop with all the features of BOTH, than TWO different desktops to choose from.\n"
    author: "@l3X"
  - subject: "Re: What if..."
    date: 2002-11-15
    body: ">  If we're talking about a merged user experience (both look and feel) about linux desktop - and we are, aren't we ? - what is the need for 2 different systems ?\n\nIt is not your need or your itch being scratched. I say this without malice or intent to offend, but this is a very ignorant proposition. It should be forever dropped. Quick and simple...\n1) Underlying differences - architecture, language, programming philosophies...\n2) Volunteers - who is going to be the ass to order people who don't take orders?\n3) Resolutions - how do you resolve the many, many, many underlying differences and in whose favor? Which toolkit, which apps, which architcture?\n4) Freedom - You can't tell me what to do!!! Quanta will *NOT* undergo the years of arguing, compromises and programmer defections this would cause. We would ally ourselves with whatever defectors would continue with QT/KDE libraries and apps under whatever banner.\n\nThink this through! A \"single desktop\" would mean massive portions of existing code thrown away, one or both toolkits abandoned and replaced, a majority of programmers defecting and the biggest debacle to hit open source in it's entire history. Why? So those people with narrow minds could more closely emulate the worst blight to ever hit software... Microsoft! We would also end up with at least three desktops where there were two and the new one would be the worst and fartherest behind.\n\nWe enjoy selecting from different brands for everything we consume. Do you truly believe that people are too vapid to be able to comprehend two different brands of desktops or want to have a choice? If so you must believe that M$ being a monopoly is a result of natural market conditions and not illegal and amoral practices. I would dearly love to have a KDE version of gimp or a replacement that uses a rational interface... but I can't see destroying both desktops and creating an OSS gestapo to take away my freedom. How can people knowingly suggest we emulate all that we despise? If you must become like your rival then what banner do you fight under? We're fascists like them, only we don't charge you to tell you what to do?\n\nOSS is about freedom. Don't like it? There is a clear alternative to freedom headquartered in Redmond Washington for those who dislike choice. It ought to be a lesson for all of us as to how choice is destroyed and why it is important!\n\nTattoo this on your forehead so you don't forget...\nOSS == CHOICE!!!"
    author: "Eric Laffoon"
  - subject: "No need to merge..."
    date: 2002-11-16
    body: "Just ignore one of them.  Run only KDE apps,\nand guess what?  Everything just works!  "
    author: "steve"
  - subject: "Re: What if..."
    date: 2002-11-16
    body: "IMHO, this was a bigger problem back when neither KDE nor GNOME were full featured enough for one to use totally independently. These days, I run a 100% KDE desktop, and I don't really miss apps. KDE has enough apps that it's perfectly usable, and it's just fine that GNOME and KDE are seperate. Besides, KDE is going one way, GNOME another. I respect the GNOME project, but I really hate the GNOME look (any theme!) and I hate the direction GNOME 2.0 is going. So it's good that there is two different projects to fit two different niches."
    author: "Rayiner Hashem"
  - subject: "No need for unification"
    date: 2002-11-15
    body: "Seriosly guys, both camps are developing new applications at such a high rate that merging the look and feel of the two desktops is becoming ludicrous.  Distributions can pick'n'choose one desktop, develop and hack at that untial it looks and feels \"polished,\" and not waste time pleasing other camps.  The other desktop can be manually added on by a knowledgeable user.  As for the newbie, it's not going to matter.\n\nConsider RedHat's problem.  They prefer Gnome, and have done a great job making Gnome look really nice.  A lot of people have problems with what they've done to KDE.  Why not only support Gnome?\n\nKDE is getting ready to hold it's own (moreso than Gnome; which would require a few non-gnome apps, such as Mozilla and OpenOffice), with koffice and konqueror maturing rapidly.  Another distro could do a better job of shipping a great KDE if they ditched Gnome.  Consider the Windows clones (Lindows, Lycoris).  Though they're still at KDE2, I believe this is the right direction to go.  Develop on one platform, tweak and tailor that desktop to look and feel great, and watch as the criticisms of applications misbehaving, gui inconsistency, and bad interpolation wither away."
    author: "ThanatosNL"
  - subject: "Re: No need for unification"
    date: 2002-11-16
    body: "<i>Seriosly guys, both camps are developing new applications at such a high rate that merging the look and feel of the two desktops is becoming ludicrous. Distributions can pick'n'choose one desktop, develop and hack at that untial it looks and feels \"polished,\" and not waste time pleasing other camps. The other desktop can be manually added on by a knowledgeable user. As for the newbie, it's not going to matter.</i>\n\nIt matters to me. I run KDE, but I also use gtk+ apps. With Geramik, I've got a single look (but not feel) in most of the apps I use."
    author: "Erik Hensema"
  - subject: "Re: No need for unification"
    date: 2002-11-17
    body: "And don't forget Gimp!"
    author: "Marcel Schreier"
  - subject: "Re: No need for unification"
    date: 2002-11-19
    body: "Actually, Lindows uses KDE 3.01 w/Keramik"
    author: "antiphon"
  - subject: "Freecurve"
    date: 2002-11-15
    body: "Hi\n\nI've just installed freecurve from pclinuxonline.com and I must admit that it's great. KDE apps and GNOME look the same and, what's more important to me, neither KDE apps nor GNOME apps look like toys.\n\nI mean that Keramik / Geramik are nice but look too much like fisher price toys (using any color scheme by the way) to be the default style. I really think that freecurve should be the default for both KDE and GNOME. Then, Liquid and Keramik should be styles for those who like 'art'.\n\nNow, I know what you'll say: freecurve==redhat==evil. That's not the good way to see the problem: Any style that look nice in both KDE and GNOME and don't have any patent (the only restriction for Bluecurve is to change its name -> freecurve)should be included in (at least) kdeartwork, even if they are done by Microsoft or RedHat. What will you do if RedHat creates a great MSWorld import/export filter for kword ? You won't include it because THEY did it ? That would be stupid.\n\nSo please include freecurve in CVS and, if people like it, make it default for KDE. Same thing for GNOME developers.\n\nI hope you'll be smarter than RedHat."
    author: "Julien Olivier"
  - subject: "Re: Freecurve"
    date: 2002-11-15
    body: "By the way, freecurve also has a great KDE/GNOME window decoration. You should include it too in CVS."
    author: "Julien Olivier"
  - subject: "Re: Freecurve"
    date: 2002-11-15
    body: "Yes, we should include free(blue)curve in future releases.  Should we call it \"redcurve\" and have the default color scheme match RedHat's web site.\n\nBUT PLEASE, restore the \"Stickey\" button.  Only the @#@#!!s at RedHat would consider its removal a 'feature'.\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Freecurve"
    date: 2002-11-15
    body: "Personally, I didn't think FreeCurve looked that nice. It also is missing something big that Geramik has - both desktops have matching colors. If I use KDE and have GTK apps using Geramik, all my apps will always match. Not true with FreeCurve.\n\nPersonally, I like GTK2's default style much better than BlueCurve/FreeCurve..."
    author: "Timothy R. Butler"
  - subject: "Re: Freecurve"
    date: 2002-11-15
    body: "About the problem of colors not matching, I believe that could be done as it was done for keramik/geramik... but I don't know how.\n\nWhen you say that KDE2's default look is nicer than freecurve, it's a matter of taste but, anyway, there is no matching style for gtk. Else, I would agree to have it the default for both KDE and GNOME. Highcolor is (was) a great style but too different from GNOME flat styles IMO."
    author: "Julien Olivier"
  - subject: "Re: Freecurve"
    date: 2002-11-16
    body: "Install Geramik's pieces for GTK, but use the bluecurve style pixmaps.\n\nI'll work on this and come up with detailed instrctions.\n\nNote that my instructions for installing BlueCurve on KDE installed from source were posted on the KDE list.\n\nI would like to see the KDE2 style for GNOME.  I haven't installed Geramik yet, it is in my in box.  When I get it installed, I will look into this further.\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Freecurve"
    date: 2002-11-16
    body: "OOOPPPPPSSSSS:\n\nIt appears that the two GNOME apps that regularly use are GTK2.\n\nSo, I guess that I will have to wait till Geramik is GT2. :-\\\n\n--\nJRT"
    author: "James Richard Tyrer"
  - subject: "Re: Freecurve"
    date: 2002-11-16
    body: "Actually, I meant GTK2 not KDE2. KDE2's theme was fine though, and there is a GTK style that I saw awhile ago..."
    author: "Timothy R. Butler"
  - subject: "Re: Freecurve"
    date: 2002-11-16
    body: "> Highcolor is (was) a great style but too different\n> from GNOME flat styles IMO.\n\nMh, Eazel's old GTK+ engine (Crux) comes pretty close to KDE's Highcolor style. That's the engine I'm personally using for the few GTK apps I need (xchat, GIMP, gkrellm, etc.) and it looks quite ok.\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: Freecurve"
    date: 2002-11-16
    body: "I think you might be in the minority. Judging from kdelook.org, people prefer Liquid/Keramik, and that seems to be the reason it's the default."
    author: "Rayiner Hashem"
  - subject: "Re: Freecurve"
    date: 2002-11-16
    body: "Maybe that's because people who like the (still) default hicolor style do not go to kdelook.org to download Liquid/Keramik? Not everyone is a theming freak."
    author: "Robert"
  - subject: "Re: Freecurve"
    date: 2002-11-18
    body: "Well said!"
    author: "Vajsravana"
  - subject: "Re: Freecurve"
    date: 2002-11-24
    body: "I'm looking for Freecurve, i've searched on pclinuxonline.com but i haven't found it. Can you help me?\nThanks!"
    author: "Roby"
  - subject: "Re: Freecurve"
    date: 2002-11-25
    body: "it's here:\n\nftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/linux/distributions/mandrake/9.0/rpms/\n\nYou can access it whether in http:// or ftp://."
    author: "Julien Olivier"
  - subject: "Re: Freecurve"
    date: 2002-11-25
    body: "What's the differece, if any, between package in\n\nftp://ftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/linux/distributions/mandrake/9.0/rpms/\n\nand package in\n\nftp://ftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/apt/Mandrake/RPMS.texstar/\n\nThankS!"
    author: "Roby"
  - subject: "Re: Freecurve"
    date: 2002-11-25
    body: "Judging from the size of the files, I'm pretty certain they are exactly the same."
    author: "Julien Olivier"
  - subject: "But its still a long way to go!"
    date: 2002-11-17
    body: "I would pesonally love to strip out all this cheezy gtk rubbish off my system, KDE is simply better in every way. GTK is very basic, very little customisabillity and still looks like motif (yuck) Its like running two operating systems on a desktop. Geramik will help, but i really want the 'kimp' and for mandrake to stop using that stupid GTK for their drak and drake tools. Even their curses version is better.\n"
    author: "Norman"
  - subject: "geramik on gnome!"
    date: 2002-11-17
    body: "i have no kde on this machine (my harddisk is full). but this theme really looks great on my gnome desktop. see the screenshot!\n\nmy only question: can i change the colors without having kde running? and i'd love to have this keramik window manager decorations. :-)\n\nmany thanks\n\nnnn\n"
    author: "nnn"
  - subject: "Re: geramik on gnome!"
    date: 2002-11-17
    body: "See http://art.gnome.org/theme_list.php?category=metacity for not less than three 3 Keramik window decoration clones (but I doubt any of these has the bubble overlapping the window border for the active window)."
    author: "Anonymous"
  - subject: "Re: geramik on gnome!"
    date: 2002-11-18
    body: "thanks very much!\n\nmetacity won't work with my old gnome... \nbut i have found some other very nice themes cause you gave me the link\n\nnnn\n"
    author: "nnn"
  - subject: "Re: switch"
    date: 2002-11-18
    body: "the answer is just switch to kde, in other words move gnome to recycle bin (trash, /dev/null), i mean that if you liked the kde look - that means that kde is for you not gnome, why to lie yourself with kdeish gnome when you can get a real native kde.\n\np.s. i guess that you can oppose me and say that you have too mutch lovely gnome apps, but ... research kde ones and you will be impressed from their speed, stability (probably gnome has too), but most of it - compatibility, integration and unified accessible userfriendly look - a features that gnome lacks... examples are common toolbars (incl toolbar setup), common options panes, common file and print dialogs, integrated reusable components (textedit, browser, other kparts)... and everything is in the same way made and as well very intuitive, many time and money are spent by volunteers and sponsors to make this big step.. so enjoy."
    author: "Anton Velev"
  - subject: "Re: switch"
    date: 2002-11-18
    body: "i have tried kde a lot of times. i'm sure it is more advanced from a technical point of view. what was allways annoying is the slow startup of programs (linking)... but who cares. my most important apps run on both environments (jedit, phoenix, eclipse, openoffice ...) - so why should i switch... \nit would be great to have a common component system (uno?) for both environments.\nthe main reason why i'm more interested in gtk/gnome is the license thing. it is LGPL and not GPL like QT. for instance eclipse-swt (cross platform java toolkit) is linked to gtk because it is LGPL (and there are many other toolkits and language-bindings linked to gtk and not to qt for the reason of license compatibility ...)\n\nnnn\n"
    author: "nnn"
  - subject: "Re: switch"
    date: 2002-11-18
    body: "well, well, well, you are rigth, GPL sux, yes!\n\nhowever KDE-LIBS are LGPL (and some of them with other licenses, like BSD) and everyone is free link with them even commercial software, anyway i am annoyed of the fact that many of KDE apps are GPLd, even konqueror and kate, it's terrific - what i am not fully sure are the underlaying reusable components fully free of shits (opps sorry i wanted to say free of GPL ;)).\nHowever there is light and hope because kool apps like noatun are BSD licensed, some other kool apps are with Artistic license, also there are terrific examples like kword that are fully gpl which means that it's impossible non-gpl app to embed kde's richtext reusable component.\n\nThe good news is that step-by-step slowly opensource world starts resistance against gpl and soon or later every author (i am sure that they would have this right) will choose another license for his oos app.\n\nThank you for your statement!\n\nBut be sure kde is still the better DE and BSD/LGPL licensed the core libs, only some of base apps are GPL infested."
    author: "Anton Velev"
  - subject: "Re: switch"
    date: 2002-11-18
    body: "I don't see why GPL is so bad. I mean that it's totally fair for developers to want their code to be used only by free applications. LGPL looks more like slavery to me: you create a free lib and anybody can use it to make proprietary software. I know some people don't care about their apps are free or proprietary but I really do and I'm glad to know that nobody will use KDE code in proprietary software.\n\nIf you want to make proprietary software, whether use GTK/GNOME or pay developers to build your own toolkit/library set."
    author: "Julien Olivier"
  - subject: "Re: GPL issue"
    date: 2002-11-18
    body: "I am not talking about direct use of the code of kde for properitary software i am talking about integration of commercial software with kde (and i don't exclude the posibility that the creators of the commercial app decide to make it available to oss world).\n\nAssume that software making company (which in order to exist and pay it's developers have to sell it of course), wants to make a cool linux app. What it can do?\n1) pay for a commercial toolkit - like commercial version of qt\n2) use LGPL or other non-GPL toolkit in order to integrate with properitary code\nand there is no option to make it GPL because it will be no-longer possible to sell it.\n\nChoosing a first option will make the software not well integrated with KDE (well of course KDE is QT based and they have common support of themes, components, widgets but on the QT's OO level), but it's not the same with state-of-art kde technologies like kparts, kioslaves, dcop, etc. as well as some common reusable kde components like toolbars, dialogs, panels, and other kde-specific features that are natural and intuitive for the end user already. And this is more true when we are talking about some other toolkits like motif.\n\nChoosing the second option will give the opportunity of sharing the experience with opensource world and in the same time keep some best-selling features properitary. But in this case there is no GPL code that can be used at all. Which would mean that all the used toolkits must not be GPL, at this point GTK is toolkit that provides fully this opportunity, but it lacks many of the features that KDE and QT have.\n\nThe conclusion we make here is that making it possible to create a commercial software integrated with kde will be possible only when kde is BSD/LGPL or other non GPL license. As far as i know current situation at the kde codebase is not fully clear what is exactly the license. Dont take me wrong i am not claiming the whole KDE as GPL but just there is for me undefined the policy of kde for licensing. There is a statement that says that most of the code kde libraries are LGPL/BSD and the policy is not to GPL, i am not exactly sure how legal is it because kde is based on GPLd QT version which should make it automatically gpl, but if it's not a problem making opensource program for kde will make in all cases to use at least one QT object (which is supposed to be GPL), like QPainter for examle. In other words it's possible to have on later stage a serious legap problems.\nOn other hand many kde apps are GPLd, acutally the official statement is that the libs of KDE are LGPL and apps are GPL. But this makes it absolutely impossible to re-use or integrate your software with any real kde app???\n(Note that we are still assuming that you want to make non-gpl app in order to keep some best-seller key features as properitary)\nTake for example kate or kword, they are gpl so you can't base or integrate the kdes' richtext component with your app. (May be i am wrong about kate because may be KTextComponent as a part of kdelib is LGPL and kate only is GPL, but for kword part?).\n\nGuys, I like kde, it's my favourite, i also tried to code it's code base is brilliant and it's very easy to write kde/qt apps, but don't GPL it, one operating environment to succeed needs apps and commercial investments, if there are no money around it, it will not have commercial apps, it will not have commercial opensource apps. Some one may oppose me and claim me as M$ troll but beleive me there is no software industry without money made from software. Look why StarOffice, Eclipse, Netscape are GTK based?\n\nFinally let me just give one example:\nAssume that company X that makes a program Y - a very cool site grabber for windows, program is for example available as 90 day trial for free download, and decided to expand to linux maket. Well such a cool app needs good auditory and as many as possible potential payable clients, it seems that integration with kde is best option at this stage.. yes it is! But well it would need to be well integrated with kde - the cool kde toolbars, file and print dialogs, kde help system help, embeddable browser component (khtml), support for grabbing via other protocolls - not just http (kioslaves), it needs to dock as a systray icon in kicker, etc, etc.. Well as you would see kde has good infrastructure for integration with it, strong reusability and mutch more - not just a big amount of users. ... but ah, shit, some of this cool features may be are GPLed? UGH! How they will sell if they GPL the program! And for example they are not bad guys they are ready to opensource parts of it in order to share the experience with opensource world and as well for example to expect the community to extend and make some other cool plugins - never mind if they are gpl - the important is that their app is popular, community friendly and it's best seller because of their special feature :(. Well if for example only the kicker is GPL they will lack one very important and intuitive feature to dock in the systray on kde. Then no program, no business, no improvement, no development.\n\nSo from this aspect i think that GPL is bad stuff. Think about it."
    author: "Anton Velev"
  - subject: "Re: GPL issue"
    date: 2002-11-18
    body: "> Look why StarOffice, ... , Netscape are GTK based?\n \nIn your dreams only. Nice troll, now go away please."
    author: "Anonymous"
  - subject: "Re: GPL issue"
    date: 2002-11-19
    body: "Boot Netscape 6 or later or Mozilla and enjoy the ugly GTK dumps.\nabout soffice/ooffice sorry that i can't find the proof but be sure it's not kde/qt app what i am sure is that sun is working on integration/refactoring of soffice/ooffice to fully integrate/comply with bonobo component model.\n\nI am not enemy of kde, neither evil, i like it, use it, and even write for own experiments kde apps (as i said i like kde/qt api is very easy and intuitive). What i criticize is GPL, read carefully."
    author: "Anton Velev"
  - subject: "Re: GPL issue"
    date: 2002-11-19
    body: "I have two solutions to that:\n\n1) Do like QT: sell KDE licenses to those who want to make proprietary software. Is it possible ? Money could go to kde-league and, then, be redistributed to all kde developers.\n\n2) Integrate KDE best functionalities into QT so that you could create a QT app that would look integrated in KDE. In fact I think the best solution would be to \"merge\" KDE and QT so that KDE becomes cross-platform and QT apps get integrated into KDE. KDE CVS should only have applications (koffice, konqueror, kmail etc...)   and use this modified QT instead of kdelibs.\n\nBut the two points above are useful only in the case where you can't buy a license for QT and use kdelibsfreely with it. I assume that must be possible as kdelibs are LGPL.\n\nCould anyone light my lamp please ?"
    author: "Julien Olivier"
  - subject: "Re: GPL issue"
    date: 2002-12-21
    body: "where in the GPL does it say \"YOU MUST DISTRIBUTE YOUR SOFTWARE FOR FREE\".  gpl is free-as-in-speech, not free-as-in-beer IIRC"
    author: "standsolid"
  - subject: "Re: switch"
    date: 2005-09-11
    body: "Seems like we have a slave here with us, with no sense of the meaning of freedom. If you think GPL is shit, don't use any GPLed software, instead buy and waste your hardearned money on something else and don't ever say anything without using your underdeveloped mind! GPL means freedom and you are a slave!"
    author: "xScoDe"
  - subject: "Re: geramik on gnome!"
    date: 2002-11-18
    body: ">my only question: can i change the colors without having kde running?\n\nsure.. edit config files by hand in ~/.qt and ~/.kde \n\nwhich ones, I don't know, i'm at work in win2k :)"
    author: "asf"
  - subject: "GNOME look"
    date: 2002-11-19
    body: "I know this isn't the perfect place to make such a confession but, well, anyway.\n\nI love RedHat artwork ! I love GNOME2 artwork !\n\nOK, now: why am I saying that ?\n\nFirst, because that's right: I just installed RedHat 8.0 and I find it great... looking. I'm writing it from Mozilla on RH8 and that's nice-looking. Icons are great, bluecurve (style and window dec.) is great, Nautilus toolbar icons are great (original, simple and nice), wallpapers are great etc...\n\nBUT\n\nGNOME is so horrible to use ! It is fast (yes, faster than KDE) but it lacks lots of functionalities (ftp:// doesn't work in Nautilus, there is no way to know if an app is starting, there is no 'show desktop' button, the desktop is in '~/.gnome-desktop', there is no good office suite etc...).\n\nSo my problem is that I'd like to have a desktop that looks like GNOME and act like KDE. So, I installed Mandrake and downloaded freecurve. That's almost perfect but 2 things are still lacking:\n\n - automatic color scheme matching between GNOME and KDE.\n - a GNOME icon set for KDE\n\nThe first one is coming according to someone on this forum.\n\nNow, is it so difficult to make GNOME and KDE icon sets match ? I'm talking about stock icons (left, right, up, reload, stop, print, save, copy, paste etc...).\n\nMy dream would be to be able, one day, to choose 'GNOME look' in kpersonalizer.\n\nYou know, I'm pretty sure it could even attract GNOME users to KDE.\n\nI don't know why but I've read lots of comments on french sites (linuxfr.org for example) saying that keramik is awful, much too colorful, and the same for crystal. I know ot's a matter of tase but I think it's stupid to lose users because they prefer the other desktop's look."
    author: "Julien Olivier"
  - subject: "Re: GNOME look"
    date: 2002-11-19
    body: "http://www.kdelook.org/content/show.php?content=3511\nhttp://www.kdelook.org/content/show.php?content=2207"
    author: "Anonymous"
  - subject: "Re: GNOME look"
    date: 2002-11-19
    body: "GNOME2.2 (due out in Jan/Feb 2003) will address some of your complaints, I think.\n\n*I'm not sure about broken ftp functionality in gnome-vfs: please file a bug in gnome.bugzilla.org if there isn't one already.  gftp works well otherwise.\n\n*Havoc has put in a show desktop button.\n\n*Havoc was working on a common launch-feedback scheme with KDE (I don't know whether it will be implemented for GNOME 2.2 or not, though) under freedesktop.org\n\n*Abiword is hoping to have version 2 out by January (it's looking very nice!), and Gnumeric is working under a similar time-schedule (but it might be delayed).  Mergeant is also hoping to come out with its 1.0 version around that time.  The big, gaping hole, of course, is a presentation program (Agnubis is still embryonic).  There's always KPresent or Impress for that.\n\n*The GNOME folks are working on a common color-scheme notification system using xsettings, so that color changes can be registered across the board.  I don't know whether that will be implemented for GNOME 2.2 or not, though.\n\nAt any rate, I hope this summary has been informative.  Both KDE and GNOME are proceeding excellently.  Both are becoming (and are) great desktops with active development communities.  It's heartening to see how well both desktops are doing, and I think that the combination of competition and cooperation (see freedesktop.org) is making both KDE and GNOME better faster.\n\nCheers!\n\nAndrew\n\nP.S. I like GNOME's look, but I think KDE looks pretty sweet as well."
    author: "aigiskos"
  - subject: "Re: GNOME look"
    date: 2002-11-20
    body: "hi\n\nDon't get me wrong. I'm not trying to bash GNOME or saying \"KDE is far better then GNOME. GNOME sucks etc...\". I know GNOME developers have already done a great job and are currently working on filling the gasp which currently make KDE better for me. Of course I'd like to try GNOME 2.2 as well as 2.4 and every future releases as I'll try each version of KDE.\n\nBut I don't have a fast internet connection so I have to use what comes with distros I can buy in stores (Mandrake, Red Hat, Debian, Slackware and Suse). And the GNOME I got in those distros just makes it harder for me to achieve my work. For the moment...\n\nI'm sure future is bright for both desktops and I hate when peope bash one of them or say they want one of them to die.\n\nI'd just like them to share more artwork and underlying technology but i'm also aware that something is cooking in this direction."
    author: "Julien Olivier"
  - subject: "Re: GNOME look"
    date: 2002-11-20
    body: "What *would* be good, in my opinion anyway, would be if desktop elements were more modular.  Im meaning that in the sense of creating some (vague) standards as to what goes where, how elements communicate etc.\n\nWouldnt it be nice...\n\n* to use a single icon set to be used by any program, gnome or kde (as you said)\n* to be able to choose things like, which html renderer to use (maybe you like interface in konqueror, but what mozilla html renderer), media players, whatnot.\n* for all apps, (as this thread is about) to be able appear as if kde or gnome, depending on what you want!!\n\n..this strikes me as the only way to get full choice - as in, you can use what you want for what.  Well, it wont be that that simple, but if some standards where banged out, it'd make for a more consistent experience.  Its a big deal for user-friendliness - who wants to have to learn 5 diff ways of doing something?\n\nIts not a case of KDE/Gnome merging... who wants that - the more the merrier.  But not duplicating effort, except where there is real benefit.\n\nBear in mind, i dont really know what Im talking about."
    author: "martinf"
  - subject: "KDe User"
    date: 2002-11-19
    body: "I think the iKons iconset is the best there is for Linux."
    author: "Mandrake"
  - subject: "KDE and GNOME apps interoperability ..."
    date: 2002-11-24
    body: "hey all,\n\ni've been waiting for this step since a long time (and i have really welcomed redhat's attempt with rh 8.0)\n\nthe next thing i'm waiting for is :\nkde apps and gnome apps dock in either panels (without any change in behaviour).\n\nanyway\ni love KDE and will be using for a long long time :)\n\nES @ GT }=)"
    author: "EvilSmile"
  - subject: "Re: KDE and GNOME apps interoperability ..."
    date: 2002-11-25
    body: ">the next thing i'm waiting for is :\n>kde apps and gnome apps dock in either panels (without any change in behaviour).\n \nConsider it done: it is already in KDE-3.1 and gnome-2.1.x. And it's great :)\nfreedesktop.org's standards pay off!"
    author: "dwt"
  - subject: "Screenshot of Mozilla 1.2 with Xft and Geramik"
    date: 2002-11-27
    body: "http://ftp.ibiblio.org/pub/Linux/distributions/contrib/texstar/screenshots/mozilla-xft/snapshot3.png"
    author: "Anonymous"
  - subject: "K .. K ... K"
    date: 2003-01-29
    body: "When is KDE going to K in it's program names. Isn't anyone else gettting\nsick of it?"
    author: "anon"
  - subject: "Re: K .. K ... K"
    date: 2003-01-29
    body: "No, actually it's brilliant.   Nothing wrong with namespacing..."
    author: "ac"
---
<a href="mailto:Craig.Drummond@lycos.co.uk">Craig Drummond</a> has released a new theme and "engine" for GTK programs that provides something many people have been looking for: a common look and feel for KDE and GNOME applications. While Red Hat's Blue Curve attempts to do something similar, Mr. Drummond's <a href="http://www.kde-look.org/content/show.php?content=3952">Geramik</a> is the first theme implementation to provide smooth integration between environments. Read <a href="http://www.ofb.biz/modules.php?name=News&file=article&sid=181">the full story</a> at <a href="http://www.ofb.biz/">OfB.biz</a>.
<!--break-->
<p>
<i>[Ed: What about usability issues?  If KDE and GTK+ apps don't act the same or provide the same set of functionality in practice, should they really look the same?  Wouldn't that mislead the user?]</i>