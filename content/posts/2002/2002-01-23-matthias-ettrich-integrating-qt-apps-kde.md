---
title: "Matthias Ettrich: Integrating Qt Apps with KDE"
date:    2002-01-23
authors:
  - "numanee"
slug:    matthias-ettrich-integrating-qt-apps-kde
comments:
  - subject: "What do I think?"
    date: 2002-01-23
    body: "Beautiful!\n\nVery Beautiful!"
    author: "Paul Seamons"
  - subject: "Re: What do I think?"
    date: 2002-01-23
    body: "I can only agree. \n\n Only thing is: would a former windows programmer need to do anything extra to port a QtWin app to QtX11 and make use of the KDE standard dialogs? \n\n/Thomas"
    author: "Thomas Olsen"
  - subject: "Re: What do I think?"
    date: 2002-01-23
    body: "I guess that would depend on the amount of KDE support they wanted. For basic things like themes, dialogs, dcop and ioslave support I think it could be transparent as far as the app developer is concerned.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: What do I think?"
    date: 2002-01-23
    body: "Excellent idea, but why can't we use plugins for everything ? If there are style plugins, we can have common dialogs, printing, ..etc plugins, so we could mantain a single code ( even without any #ifdefs ) among all platforms. There should be also possible, for example, to embed KSpread in Hancom Office in the future.\nPS. \nWhere can I find info about \"the new way of handling signals and slots in Qt 3.0\" ?. Can I call some slot using only its name ? how can I pass the arguments ?"
    author: "Rumcajs"
  - subject: "A win for a commercial KDE"
    date: 2002-01-23
    body: "This may help wooo more large scale applications developers to QT and inevitably KDE.  I mean right now I dont see CA, Adobe or SAP beating KDE's door down.  Maby with a more integration with the Unix desktop these companies will be ready to abandon Motif for QT and see what the Unix desktop has to offer.  I mean what is the worse that happens?  We get more software that looks good under KDE?\n\n-ian reinhart geiser"
    author: "Ian Reinhart Geiser"
  - subject: "Funny, I was just researching..."
    date: 2002-01-23
    body: "How to tie in qt3 designer database app with kde3. Any pointers to information on this? I am starting a project which will track job orders for a service trade. www.netidea.com/~dkite\n\nThis would be easy and automatic for developers. However there are a few neat things in kde that wouldn't be available, such as embedding kchart, kspread etc.\n\nWould the kio stuff make for example ssh available to an app that wants to connect to a Mysql database?\n\nDerek"
    author: "Derek Kite"
  - subject: "Re: Funny, I was just researching..."
    date: 2002-01-23
    body: "I'm not entirely sure about this, but for embedding stuff like KSpread, KChart, etc, would require access to KParts, and to the specific compenents. I don't think QT really provides a version of this sort of thing by itself, so it wouldn't be a matter of mapping a QT call to a KDE call so much as allowing optional support for certain KDE features."
    author: "Carbon"
  - subject: "Re: Funny, I was just researching..."
    date: 2002-01-23
    body: "> How to tie in qt3 designer database app with kde3\n\nErr, using KDevelop ?\nI did this last month: a kde3 app that is a database app, with all dialogs coming from qt3 designer.\nUse KDevelop to generate the kde3 compiling environment, and clicking a .ui from kdevelop simply fires up qt designer.\nWith the .ui.h feature, it's really great (no need to inherit every dialog anymore).\n\nIf you meant: so that the app can also be a qt-only app if wanted, then\nwe're back to Matthias's proposal ;)"
    author: "David Faure"
  - subject: "This is a great idea"
    date: 2002-01-23
    body: "But is has to be implemented in such a way that a single library can be included with the binary, in the same way that the Qt library can be included.  The challenge we always have in creating software for KDE is having to compile for every combintation of distribution and version of KDE, it is almost impossible and you have to leave people behind.\n\nOne of our goals for the last year was to create a KDE compatibility layer.  As Matthias said, we rely on certain services on Windows and Mac, so if we are on Linux then why not default to KDE as the environment?  We've had to build some of this from scratch already.\n\nQt isn't a desktop, it is a windowing toolkit.  KDE provides the desktop and if there is a clean way to do what Matthias is saying, it would be awesome.  We would be please to help with this project if possible."
    author: "Shawn Gordon"
  - subject: "Re: This is a great idea"
    date: 2002-01-23
    body: ":: Qt isn't a desktop, it is a windowing toolkit.\n\n    Which is kinda starting to make me wonder if there should be a Qt/KDE added to the line.  Like Qt/X11, Qt/Windows, Qt/OSX... KDE offers so much more in terms of standardized environment over X, that maybe it would be appropriate for a Qt that maintains portable code (a la the rest of the line), and uses \"Native tools\" for printing, opening files, etc.\n\n    It's similar to this concept, yes, but IMO a bit cleaner in terms of concept and probably maintainability.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: This is a great idea"
    date: 2002-01-23
    body: "Yes, but what would KDE use? Qt/KDE? That's impossible. So you'd have to have both Qt/X11 and Qt/KDE installed.\n\nIt meight be possible to LD_PRELOAD Qt/KDE for Qt-only apps, though."
    author: "Erik Hensema"
  - subject: "Re: This is a great idea"
    date: 2002-01-23
    body: "So Qt/KDE would be a (binary compatable) wrapper to Qt/X11 that replaces some of the Qt/X11 calls with native KDE calls? (which is almost what Matthias was suggesting, but not quite)."
    author: "drawoc suomynona"
  - subject: "Re: This is a great idea"
    date: 2002-01-23
    body: "Bingo - as I said, it's mostly different in conceptual position than actual code.  It just makes it easier to decide which Qt to use - use Qt/Win for a Windows app, Qt/X11 for an X app, Qt/OSX for a Mac app, Qt/KDE for a KDE app. all using the same source, and portable across all environments and making use of the native features of each.  Since KDE has features and conventions above and beyond either Qt or X, it makes sense.\n\nIt's really fundimentally the same idea, just placed differently in a more sensible and easier to comprehend position, especially for companies that just want to use Qt for its \"real\" reason - a cross platform windowing library.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: This is a great idea"
    date: 2002-01-23
    body: "Isn't it really Qt/X11, Qt/Kdeembedded, Qt/KDE/X11, Qt/OSX/X11. \n\nAccording to some kind of mathemtical logic folloes: \n\na) Using a wrapper:\nMyXQtKDEApplication  = X11 (Qt (KDE(myCode)))\nand \nMyXQtWrapperApplication  = X11 (Qt (Wrapper (myCode)))\n\nIf we now assume:\nMyXQtKDEApplication = MyXQtWrapperApplication\n\nThis results in :\nX11 (Qt (KDE(myCode))) = X11 (Qt(Wrapper(myCode)))\nand \nQt (KDE(myCode)) = Qt(Wrapper(myCode))\nand thus if we would assume komutativity of X11 and Qt(*):\nKDE (myCode)  = Wrapper(myCode)\n\nThus in words, if we assume any KDE application would behave same as a Qt application with a wrapper, then the KDE must be the wrapper itself. \n\nAnd if the KDE application behaves differently than the Application using the wrapper, then follows the KDE behaviour is not the wrapper behaviour, even if it has KDE the same interfaces to mycode as the Wrapper.\n\n(*) if inherriage would be komutative, which it probably is not\n\n\nb) Not using a wrapper, but Qt directly:\n\nMyXQtKDEApplication  = X11 (Qt (KDE(myCode)))\nand \nMyXQtApplication  = X11 (Qt (myCode))\n\nIf we now assume:\nMyXQtKDEApplication = MyXQtApplication\n\nThis results in :\nX11 (Qt (KDE(myCode))) = X11 (Qt(myCode))\n\nand thus if we would assume komutativity of X11 and Qt(*):\nQt (KDE(myCode)) = Qt(myCode)\nand \nKDE(myCode) = myCode \n\nThus in words, if we assume any KDE application would behave same as a Qt application, then the KDE must do nothing. \n\nAnd if the KDE application behaves differently than the Application using qt directly, then follows the KDE, even if it has the same interfaces to mycode than Qt. \n\n(*) if inherriage would be komutative, which it probably is not\n\nComments ?"
    author: "the mathematican ?!"
  - subject: "Interesting, but..."
    date: 2002-01-23
    body: "Interesting, but...\n\n...isn't this merely slapping the KDE look-and-feel on top of Qt apps? A guess for some people this would be a good thing, but from my perspective it's just superficial fluff.\n\nThis sounds more like a way to get KDE applications available as Qt-only apps, then the other way around. So give me an example. Show me the benefits to a Qt-only application, like LinCVS, by using this new library."
    author: "David Johnson"
  - subject: "Re: Interesting, but..."
    date: 2002-01-23
    body: "You could open ftp://ftp.microsoft.com/top_secret.doc in the file dialog, or print four pages ( at least ) on a single sheet or maybe even display manuals using konqueror."
    author: "Rumcajs"
  - subject: "Re: Interesting, but..."
    date: 2002-01-23
    body: "KIO is not superficial fluff.  Did you read the entire post? :)"
    author: "Justin"
  - subject: "here's an examples"
    date: 2002-01-23
    body: "In our Rekall product, if you were to use something like this, you wouet simple things like themes being consistant, printer services,cut & paste ability.  Now from your perspective this is probably no big deal unless you run into it as a problem.  For us it make it easier for us to make a Qt app interoperate with KDE.  This is a win..win situation really.  It makes direct KDE support very simple for people creating Qt apps, which brings more apps to the desktop, which brings more people, which brings more critical mass, etc., etc.\n\nthink big picture here."
    author: "Shawn Gordon"
  - subject: "Let me just chime in here..."
    date: 2002-01-23
    body: "With my AOL-Like Me Too on the side of this being a really good idea. I for one would love Shawn's applications to integrate better with my desktop.\n\nDavid"
    author: "David Joham"
  - subject: "Don't like this idea"
    date: 2002-01-23
    body: "Qt strength is that as a programmer you only have to learn one API for different plattforms. In terms of Windows/MFC I would say that is A Good Thing. But for KDE it is not. KDE is an open source project. One of its goals is to make the best desktop environment for Unix/Linux. Its focused on the integration of the whole stuff - not on publishing single apps. You need people wanting to learn the KDE-API.\nIf you have a KDE compatibility layer in Qt why (as an app-programmer) do you want to learn the additional KDE-API?\n\nSo, this does not sound like a win-win situation. KDE will lost future core programmers.\nIMHO the right way is to have KDE core libs for other plattforms, too - maybe this can only be done if Qt is GPL/QPL for all free software on all platforms. Then we can say to a e.g. Windows user: Hey, look at this nice application: If you want to run it in a complete enviroment: Leave your plattform and come on over here.\n\nBye\n\n  Thorsten"
    author: "Thorsten Schnebeck"
  - subject: "Sounds good."
    date: 2002-01-23
    body: "I'm using Delphi/Kylix, so this sounds like a great idea to me. But I assume Borland needs to migrate CLX to QT3 first though."
    author: "Mattias Dahlberg"
  - subject: "UNIX?"
    date: 2002-01-23
    body: "KDE is a powerful Open Source graphical desktop environment for _Unix_ workstations. It combines ease of use, contemporary functionality, and outstanding graphical design with the technological superiority of the _Unix_ operating system. \n\n(C) www.kde.org"
    author: "Andi"
  - subject: "Re: UNIX?"
    date: 2002-01-23
    body: "And why not make it a great multi-platform environment now ?"
    author: "Julien Olivier"
  - subject: "Maybe stupid idea"
    date: 2002-01-23
    body: "I don't really know KDE API but I'm thinking of one thing:\n\nWhy wouldn't KDE use QT-only applications, using libQtKDE to make the integration ? Only KDE-specific applications (dcop, kioslaves...) sould use the KDE API.\n\nSo, most of the work would be made into libQtKDE and app developpers wouldn't have to worry about KDE Api. If each app is written in QT and uses libQtKDE, there will be no difference between \"native\" KDE apps and simple QT apps since libQtKDE would act the same way on them.\n\nBut, for it to be really efficient, libQtKDE needs to be VERY powerful. It needs to emulate 100% of KDE functionalities through QT.\nAnd QT needs to be modified to integrate more KDE-like functionalities so-that QT-only apps using libQtKDE don't lose some capabilities they had with KDE API.\n\nJust my 2\u00a2 (or 0.02\u00a4 if you prefer)."
    author: "Julien Olivier"
  - subject: "OT: Euro"
    date: 2002-01-23
    body: "AFAIK\n1. The simbol (\u00a2) is dolar cents, and not to be used with Euro.\n2. The decimal separator is comma (,). The dot (.) is the thousands separator\n3. And I'm pretty sure the Euro symbol (\u00a4), should come before the quantity money.\n\nExample: \u00a42.340,50\nAm I right?\nI hope new KDE versions do get the Kontrol Centre->Personalization settings correct for countries using the Euro :-)"
    author: "Watermind"
  - subject: "Re: OT: Euro"
    date: 2002-01-23
    body: "1.What is the symbol for Euro cent ?\n2.I'm a programmer :) I always use \".\" instead of \",\". But I think you're right.\n3.In France, I've always seen prices written like this: \"34,99 \u00a4\". In Germany, I saw  both \"\u00a434,99\" and \"34,99 \u00a4\". I don't which is right... maybe there's no rule..."
    author: "Julien Olivier"
  - subject: "Re: OT: Euro"
    date: 2002-01-23
    body: "I don't think there is a a symbol for Euro cent.\nI'm a programmer too ;-)\nI've seen it both ways here in Portugal too, and I do hope there is a rule. It's the same (currency) unit, everybody should write it the same way.\n\nNow read this fast!\n1.325\u00a4\n1,32\u00a4\n\u00a41,32\n\u00a41,320\n1,325.32\u00a4\n1.325.320\u00a4\n1.32\u00a4\n\u00a41.32\n\u00a41.325,32"
    author: "Watermind"
  - subject: "Re: OT: Euro"
    date: 2002-01-23
    body: "> I hope new KDE versions do get the Kontrol Centre->Personalization settings correct for countries using the Euro :-)\n\nhttp://lists.kde.org/?l=kde-cvs&w=2&r=1&s=euroland&q=b"
    author: "someone"
  - subject: "Re: OT: Euro"
    date: 2002-01-23
    body: "Arghh! No, don't do that to me! In the English speaking world it's the totally opposite way around - a comma for thousands and a point for the decimal separator. I just couldn't survive like that. Frankly though I don't think it matters how it's written, so long as it's the same value. We're different countries with different languages and cultures, and we should write it our own way. So long as you use the symbol people can work out the rest for themselves.\n\nThat said I'm gutted about the cent thing. It's the only part of the currency I thought I'd got working! If I hit the AltGr-4 in KDE I just get a ? - the same key combination gets me a \u00a4 in most GTK/Gnome apps. I'm using the enGB locale (I can't find an enIE for KDE, and the standard IE is in Irish and seems to be incomplete)."
    author: "Bryan Feeney"
  - subject: "Re: OT: Euro"
    date: 2002-01-23
    body: "The *english speaking world* is the opposite way around! :-)\nAnd of course you could live like that, changing is easy ;-)\nAbout the \u00a4 in KDE, you need to be using iso8859-15, and I had to change the var LANG to pt_PT@euro - in suse all I had to do was change it in YaST2 RC-editor - I don't know whether enGB@euro works or not... but I don't think so... I know SuSE mentioned something about it in the euro how-to.\n\nI type the \u00a4 using AltGr-E"
    author: "Watermind"
  - subject: "Don't think so."
    date: 2002-01-23
    body: "> But, for it to be really efficient, libQtKDE needs to be VERY powerful. It needs to emulate 100% of KDE functionalities through QT.\n\nIt doesn't really have to be powerful at all. Remember that libqtkde would just call the appropriate functions within kdelibs and it's component libs.\n\n>And QT needs to be modified to integrate more KDE-like functionalities so-that QT-only apps using libQtKDE don't lose some capabilities they had with KDE API.\n\nWell, not really. This lib would be targetted to apps that do not use the KDE API anyways, so without the lib, they would not have any of the capablities that the KDE API provides (without getting it from elsewhere or doing it themselves, that is)."
    author: "syn"
  - subject: "A hack, nothing more"
    date: 2002-01-23
    body: "<rant>\nOk first off before even commenting on the idea I would like to loudly complain about the kde about box.  It has several issues that need to be addressed.  (I am referring to the standard one that everyone uses, not the far and few between custom ones)  The about is made up of 3 tabs, 2 of which I have major qualms about.  In Authors tab lists information about the authors, but does it is such a way that it becomes too long.  I can't recall a single about about (using the kde about box) that could hold all of the developers info.  One possible change would be to have the e-mail placed next to the name of the person.  There are many different ideas of how to re-arrange it, but you get the idea.  Next, the License agreement tab, why can't wrap around be used?  Either turn it on or for default gpl and lgpl format it to fit the default kdeabout size. Maybe these issues are why not even all of the kde apps use the kde about *cough* utilities *cough*.\n</rant>\n\nOk, lets talk about this QT/KDE lib.  First off I am glad that this discussion is being had.  It is something that has been brewing for a long time and needs to be talked about.\n\nIs a qt/kde lib a good thing?  With this qt/kde library developers would then think that it is ok to stick entirly to qt and never move to kde.  Right from that alone it would cause more hurt then help.  Also it is presuming that TrollTech will be releasing a non-commercial windows 3.0 license.  Who says they have to?  Who says they have to release 3.1.  (just go and try to get the mac version...) If anything the non-commercial package is probably hurting them financially in the market that they are making money mostly in (windows duh) and if I was in the marketing department I would certently (sp?) dangle \"free\" old versions of qt-windows for people to try out.  Ok, but for the argument we are going to assume that they will continue to release non-commercial packages for if they didn't then this whole lib would be for not.\n\nLet us now look at the alternative.  This alternative it to clean up kdelibs so that it will port on windows (and mac?). We are not talking about the kdebase, but kdelibs.  This will require the code to be gone over and cleaned up to be more compliant etc which will also practicly be a code review.  Doing this will be worth its weight in gold.  You want to know why?  Because then all of the little qt-only developers will then see that they can get all of the kde functionality (not just the stuff in the qt/kdelib) and still have cross platform compatibility.  They will then care more about kde, help out with it etc.  <off topic> Heck with kdelibs ported someone might port kdebase!  How cool would it be to have the kde desktop on windows?  Porting kdebase would be more work, but something that can be done.  </off topic> Trolltech did most of the work with abstracting qt and it only makes sense to try to get kdelibs for windows first.  I am surprised that someone hasn't tried already.  And I hear that person in the back who says kde-cygwin is here.  Here I am laughing back at you.  Tell any average person that you have to install an \"emulator\" to run the app and they will almost laugh at you too.  cygwin is not the answer.  Qt runs natively on windows so why can't kdelibs?  Also we would also be relying on 2 other sources (qt and cygwin) for portability.  Having it be as simply as a recompile is a much better solution.\n\nWhat do we see in every other dot.kde.org story?  Some dude asking the developers to speed up the apps.  Adding another library is not the way to do that!  Just think if we had this lib so that the windows users could run kde apps.  I can just see the 10 million posts asking why is \"kde\" slow for them. Doesn't matter that it is qt only they saw some kde thing somewhere...\n\nBack in the day developers wrote qt only apps because more people would install qt apps the kde apps (yah people are morons).  Now the license issues is just about forgotten and more and more people are using kde.  So it is time to port to kde right?  Wrong, along the way qt dangled this little cross compatibility thing in our faces.  Our little linux app suddenly was usable on our friends, parents, girlfriends parents, etc machines.  Granted we didn't get much of any developer support from windows users (can you please add this huge ass feature for free, yesterday? Oh and thanks for the app! -anon user), but having your mom run your little xyz app on her work machine and her pride when she can show everyone (even if she never uses it) was worth it.  So why should I port my app to kde?  I could try ifdefing all over, but doing that will only make things worse.\n\nIf we make this library then we will be like Microsoft in just applying a layer over hacks rather then fixing the real problems (think 16 bit days).  What is worse is the fact that all of this new code means new bugs.  Tightening, cleaning house and removing old code is better then adding new items if possible.  In QT 3.0 there is a settings class called QSettings.  It does something very similar to KConfig...  Is the kde group going to drop KConfig and use QSettings?  No (correct me if I am wrong), should it?  Yes.  Why?  Because it is just another splinter, another layer, another level of confusion.  Developers do not need the anoyance of deciding which one to choose. (re-read this paragraph with your own personal k* vs q* class annoyance)  If I could honestly choose I would say that a lot of the duplication that KDElibs does should be ripped out and given to Trolltech to incorporate into QT as a gift of thanks for all of the free work they have done in qt.  Heck this would remove a layer of virtual functions and make all kde apps just that little bit more faster, haha!  (and even if we couldn't do that for legal reason fazing out the kde class should be the next option)  If this qt/kde lib were to be made then we should ditch the kdelib.  Why you ask?  Because the point of this is to give applications a very similar look at feel, but if there are two different libs kdelibs and qt/kdelibs then there will always be differences no matter how hard we try.\n\nLets talk about new developer.  How about commercial developers?  They all only use qt and don't even think about using kde.  <insert ovious (sp?) reasons>  Now if the kdelibs were cross compilable then they would link to them and simply distribute both libs. (back to the 2 libs issue, but nonless)  There are some developers who have qt and kde apps with ifdefs, but it is time consuming, anoying and creates messy code.  Even then they choose to use the qt class over the kde one every time just to make their job easier.  This means that their app doesn't look exactly like other kde apps.  Small, but in the end it shows.  Remember Corel?  Remember how they had some dude go around and make everyone clean up their apps to act the same?  Remember how just yesterday you read about that _again_ in some magazine as a reason why kde comes off so clean?  If this library solves that completely why would we need kdelib, why not break apart kdelibs into a cross platform section and another part that is for \"kde apps\"?  Oh wait, but then developers wouldn't use the \"kde apps\" library which is were we are right now. The library is only a temporary solution to the problem.\n\nAdding yet another layer will do kde little good.  Heck when I read this story my first though was hey I can now port my kde app *from* kde to qt and use this little lib for the fun gui stuff.  Course I would rip out a lot of extra kde functionality, but I get to compile on windows!  I am sure that I am not the only developer who thought this before even finishing the title of the article.  This library wont convice any qt developer to port their app over to kde, if anything it will stall them even longer if not forever.  This only hurts kde users.  I am willing to bet anyone that the qt/kdelib will cause more apps to be written qt only then kde only.  Granted it may make the kde desktop larger, but it will only cause in the end someone porting kdelibs.  The kde group has to either merge their kde code in qt (give it to them?) or make sure that the kdelibs works everywhere that the qtlibs work to stop this compitition of which library to compile for.\n\nWhat we are really talking about is a replacement for kdelibs, but then we are back to where we started aren't we?\n\nI vote to either a) make kdelibs cross compilable or b) Donate kdelib code to trolltech/etc and begin to faze out kdelibs in favor of qtlibs or something like that.  Maybe Trolltech will include the kdelibs into the QTlib if we ask them nicely so then anyone who downloads the qtlib will get both. :)  Choice B is probably the harder to do of the two (rewriting it all? contacting all of the authors? setting up so that patches in the future are signed over to trolltech?, etc), but in the long run will benefit *everyone* much better then any hack.  KDE was ment to be a desktop envoirment, not a core widget lib (although we did start making one at one point)  And for those that don't want to give up their code to trolltech you can remind them that as long as trolltech is around it will go into the GPL version of the code along with their own commercial version. <rant> I hate people that complain about the gpl qt code and how they can't sell their own little apps without giving away the source.  They want to use something for free and make money off it.  They sound like my friends who sell crappy windows shareware using a warzed version of vc++ and still complain.</rant>  Maybe this is what should be the target for KDE4.0  The removal of the classes that are extensions of what QT already has and only have classes that QT doesn't have.  Maybe have someone at trolltech who will add the core widget stuff that kde needs to QT.  KDE is about the desktop and not the widget set.\n\n-Benjamin Meyer\n\nP.S.  I am sure that someone out there will pick apart some little sentence in the middle of this in an attempt to flame me back.  Before you do *please* think first if what I really meant and get across sloppily at 2am and then make constructive critizim on that not my horrible english spelling skills.  Oh and don't bother repyling to my rants for I will ignore you, they are offtopic. \n\nP.P.S Can someone that is allowed post my response to the kde-core list?  Thanks."
    author: "Benjamin C Meyer"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "Well...\n\nI couldn't agree more with you !\n\nMaybe because I'm an app developper and not a kde-core developper.\n\nThere is only one thing that I'd like to comment: \n\n>> Maybe this is what should be the target for KDE4.0 The removal of the classes that are extensions of what QT already has and only have classes that QT doesn't have.\n\nIf what you say (Plan B) happens, every single developper in the world will write QT-only applications. That's obvious.\nSo now the problem is: why developping classes that fit only in KDE and not in QT ? Nobody will use them anyway !\n\nI think that if things go that way (which I expect), KDE specific classes should be used only in KDE Desktop specific apps: kdesktop, kicker...\nBut, in fact, what is in KDE that couldn't be proted to Windows or Mac (and thus, integrated into QT) ?\n\nAnother question (not a flame, I swear): If QT includes all the KDE stuff, won't it be slower that before at a point that all QT-only apps will lose performance ?\n\nHowever, I know that including KDE classes into QT would be a VERY huge work but that would really be a great step for linux (not only KDE). That could also give a good idea to gtk-gnome developpers to merge everything in one portable lib too."
    author: "Julien Olivier"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "Ok, Maybe I should have put B first and then A.  I am not saying that we have to intigrate KDElibs into QT simply that was one crazy possiblity.  Making the kdelibs cross-platform compilable is the closest things to a good solution I can think of at this early in the morning.  The only problem with it is that it will take some time and some time is not now and people want things now rather then right.\n\n-Benjamin"
    author: "Benjamin C Meyer"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "I couldn't agree more with Benjamin C. Meyer's posts."
    author: "Bojan"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "Ok, had some coffee\n\n\nThe cause of the problem (developers/companies sticking to qt and not using kde's core libs) stems from the fact that they wish to port to windows.  \n\nThe reason why kde wont benefit from a \"qtkdelib\" is that for most of the application developers, having the file dialog the same as kde is all that matters, having the default select clicking in a treelist be double click isn't.  It is the little thing, icons, focus etc that will through off the user as they move from kde application to qt application whom will look similar, but behave differently.\n\nWhat I fear is that open source developers will stick to Qt simply so that they can compile their app on windows sacrificing any features kde could add to them.  Thus makeing the kde desktop \"broken\" (like how netscape has a different copy paste sceme then every other app in unix. It is \"broken\" to most first year cs students here at school until they figure it out).\n\nThe kdelibs encompass many things.  A set number of them, KApplication, KTextView, etc are little more then small additions to Qt widget so that they can better incorperate into the desktop.  These core widget specific items are what make up the look and feel of the application (default double clicking in a treelist for example)  Putting these classes in a seperate library that can compile on windows will allow commercial/noncommercial developers to use them.  On the windows side KFileDialog etc would simply pass right to the QFileDialog (and windows file dialog) while on the unix side it would have our kde file dialog.  Then when these apps are combined with kde they will behave similarly with the rest of the desktop.  Then when these application wish to add kde specific library functionality (kspell for example) there are only a few ifdefs that are easy to manage rather then the nightmare of hundreds of little ifdefs surrounding kapp and qapp.  These new ifdef additions do not effect the ui, but only effect the feature fill of the app.\n\nHaving a simply small ui library concisting of all of the KAction, etc classes that was cross-compilable with windows would allow people not to worry about which library to base the application on and simply the kde one.  They would then feel more at home when wanted to add more specific kde features on the unix side.  And even if they didn't add any specific kde features it would still work and feel right along with kde.\n\nThe problem is developers and companies not wishing to utilize kdelibs simply because they wish to cross-port.  Lets remove that problem for them.\n\n-Benjamin Meyer"
    author: "Benjamin C Meyer"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "If there are volunteers to do such port, please reply to this mail. I'm sure that we have thousands of them soon and I'm sure that you, Benjamin, already started the port on MacOS/X. Hey, why we have to work on some small hack for a month or two, when we could have redesigned/rewrited KDE API just in a few years and surely all commercial companies wil be using it instead of Qt, no doubt. I'm sure that our KDE developers didn't clean the API and it had been you to tell them to do it."
    author: "Rumcajs"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "I am confused, was this a flame?  The above post was simply saying that to get qt apps to look similar to kde apps they need the kde ui stuff so we should remove that from the kdelibs into its own lib so that it can be used on windows, linux and mac.  That would be cleaner then a libqtkde\n\n-Ben"
    author: "Benjamin C Meyer"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "I was joking, of course. \nIt is easy to say but hard to do. I think that it isn't as simple and would require much efforts and time,  ( how many have we developers, packagers and testers, which have both MacOS and Linux. ) I see also no advantages over libqtkde. Instead of Qt available on a few platforms ( kde, x11, win, mac ), you proposed KDE libs available for a few platforms ( qt, win, mac )."
    author: "Rumcajs"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-24
    body: "The advantage is that the applications will behave the same.\n\n-Benjamin"
    author: "Benjamin C Meyer"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "Port KDE to Qt? I'll help. I fully agree with Ben's proposal. Take all the KDE widgets and dialogs and put them in Qt. Trolltech would have to do most of the work, but I would be more than happy to take an app or two and rip out the ifdef spaghetti."
    author: "David Johnson"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-24
    body: "The widget stuff barely require much of a port for they are all generally very simply with a few items to clean up.  Giving them to Tolltech wouldn't solve the problems, but makeing the widgets into their own lib would help.  FOr that lib would be cross-compatible.\n\n-Ben"
    author: "Benjamin C Meyer"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "99.99% agreement. The 0.01% disagreement comes because I think there is a significant number of developers who choose Qt-only because it imposes fewer dependencies on the enduser.\n\nBut I couldn't agree more on your overall assessment. Too much stuff in kdelibs is merely window dressing for Qt. Way back in history it made sense. KDE was an application framework while Qt was just a gui toolkit. But now Qt *is* an application framework. Let the KDE libraries provide only what Qt does not provide. Keep KIO, kparts, etc., but throw out the KDE dialogs, widgets, etc.\n\nI don't use GNOME, and I think they made some bad mistakes. But they do one thing right. They have only one widget toolkit, GTK+. For the most part, GNOME is not a layer on top of GTK+, but non-GUI stuff to complete an application framework. This is why GTK-only apps integrate so well with GNOME, because GTK-only apps have nothing to fight.\n\nThere is no reason in the world for KDE to have its own file dialog, color dialog, toolbar, etc."
    author: "David Johnson"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-24
    body: "KDE's File, color, toolbar etc are good and shouldn't be dropped the problem stems from little things.  We shouldn't through away all of the extensions that we have made to Qt, but make it so that people want to use those extensions.  Having them inside of a non-cross platform compatable class is not that way.\n\n-Ben"
    author: "Benjamin C Meyer"
  - subject: "Small library?"
    date: 2002-01-23
    body: "It's *all* needed, the kdelibs, to make a KDE app.\n\nAs for making kdelibs portable, it'll happen.  kde-cygwin is a start, but not the end.\n\nRik keeps saying he likes it, and gradually sells more and more people on the idea (even me), and eventually someone's going to code it. :-)"
    author: "Neil Stevens"
  - subject: "Re: A hack, nothing more"
    date: 2002-01-23
    body: "As for the about box, send a patch.\n\nAs for KConfig, KDE isn't going to drop it in favor of a class that does far less.\n\nAs for donating kdelibs to Trolltech, Trolltech already can use kdelibs as much as they want. It's free software.  They just can't make proprietary versions of the LGPL parts.  Don't blame KDE because Trolltech will use only software they can make non-free on Windows and MacOS.\n\nAs for making your app available to friends: If you want to code for Windows, code for Windows and accept the consequences of certain facilities not being available.  Or, rather, your friends have chosen their OS, and they have to live with it."
    author: "Neil Stevens"
  - subject: "What about gnome/gtk"
    date: 2002-01-23
    body: "Could it be possible so gnome/gtk apps could use it to? They could benefit greatly from the kioslaves and the kdeprint mechanism."
    author: "Jeff Brubaker"
  - subject: "Re: What about gnome/gtk"
    date: 2002-01-23
    body: "they can already use the kdeprint mechanism, all they have to do is use kprinter as printcommand instead of lp"
    author: "coba"
  - subject: "Re: What about gnome/gtk"
    date: 2002-01-23
    body: "I think this would be great.\nWe need REALLY BATTER integration between apps made with gtk and qt.\nIf they could support drag-and-drop, bahave the same way and look the\nsame way, it would be like if gtk apps where qt and vice-verse depending on what enviroement you where running (kde or gnome or other).\nI know this would be difficult, but in a XML age we are now, I don't see this as impossible, and woud make much more easy to use *nix, nas all apps would bahave as the desktop asks them to do."
    author: "Iuri Fiedoruk"
  - subject: "creating new platform"
    date: 2002-01-23
    body: "While I really like the idea to have Qt apps using advanced KDE technolgy like KIO, I am afraid that the construction of a second X11 like platform will create bad echo from the Linux/Unix user community.\n\nRight now you can use a Qt app on GNOME or with just a windowmanager without loosing functionality.\n\nSo, while we know that actually the KDE users get an enhanced verion of the app, GNOME users might feel they get a light version.\n\nMoreover, a user using both environments, might feel he/she is \"forced\" to use KDE if he/she wants the real stuff.\n\nPerhaps this could be avoided by defaulting to Qt only and use KDE enhancements only when asked to do so.\n(by commandline switch or configuration item).\nAn example for this behaviour is licq's Qt GUI. By default it wil work the same on all Desktops, but it can be, if the users decides so, be enhanced for KDE use (at compile time ony AFAIK)\n\nCheers,\nKevin"
    author: "Kevin Krammer"
  - subject: "Re: creating new platform"
    date: 2002-01-23
    body: "KDE has always been a platform on top of X11. KDE is sometimes refered to as not only a DE but as an development platform.\n\nSo I don't se a problem whit this if you are running the Qt apllication on\nan inferior platform:) Gnome/Windows/Mac/X11-only you get the same functioality.\nYou don't lose any, but on the superior platform KDE, you get the added functinality KDE provides."
    author: "Morty"
  - subject: "Drop qt-layer!"
    date: 2002-01-23
    body: "Why does kde uses the qt layer in back?\n\nQt have many classes that a kde-program newer will use, it only waist memory!\n\nwhy dont strip out all unnessesary classes from qt and put them into libkde.so ?, It will save you memory, and improve startuptime!\n\nThen you can make a kde-libqt.so that is a qt-compatible library.\n\n\nSorry for my realy bad english."
    author: "Jimmy W"
  - subject: "Re: Drop qt-layer!"
    date: 2002-01-23
    body: "Well, KDE uses a lot more of Qt than you may think, but it is a valid option for KDE to fork Qt.\n\nThere's one big problem though: licensing.  Right now, KDE isn't committed to either of the Qt licenses, so KDE isn't tied to the GPL license Qt offers.  If KDE were to fork Qt, then KDE would have to use the GPL.  This would severely limit the licensing options of KDE apps, and damage KDE development momentum.\n\nOn the other hand, it'd put the community firmly in control of its core library.\n\nUltimately what will keep KDE from forking Qt is the amount of work involved.  Until a fork of Qt is actually made by somebody, and that fork gives a compelling reason to switch, it's just not going to be worth the effort in the eyes of most KDE developmers."
    author: "Neil Stevens"
  - subject: "Re: Drop qt-layer!"
    date: 2002-01-23
    body: "Since GPL QT only allows you to link against GPL programs, you could not write programs for it using other open source licenses. I consider that a big drawback."
    author: "ac"
  - subject: "LD_PRELOAD"
    date: 2002-01-23
    body: "Hi! I am not sure if all developers or companies will use a libQtKDE.\nMight it be possible to use LD_PRELOAD to catch libqt calls? This would be very cool"
    author: "Henning"
  - subject: "Discussion"
    date: 2002-01-23
    body: "Wow, the discussion has sort of went all over the place.  And Ben, your posts are way too long :P\n\nAnyhow, it seems there are two ideas floating around..  let me re-iterate what people are saying, and then I'll give my take.\n\nA) KDE should become another platform for Qt (which is circular, as Matthias put it), just like Windows and Mac.\n\nB) kdelibs should be more portable, so that people may use them on other Qt-supported platforms.\n\nBoth of these are good ideas, but they are both very different as well.\n\nI really like (A).  Just look at the FileDialog.  On Windows and Mac you get the native dialog.  On KDE you get a stripped-down Qt-based one.  This is ironic, because it means KDE is the least supported environment by Qt.  As KDE grows and takes over the Unix desktop (IMO, of course), potential customers of Trolltech are going to find that the lack of KDE support in Qt makes Qt/X11 less viable.  X11 is barely a desktop platform.  Why should there not be Qt/Windows, Qt/Mac, and Qt/KDE ?  And what Matthias talks about is doing it via a plugin, so that all you hardcores out there that want Qt/X11 would still be able to have it.  Qt/KDE would be a bonus.\n\n(A) is helpful to Trolltech, because it makes Qt more viable.  It is also helpful to application developers, like myself, who use Qt for both Windows and X11.  Why should my app work best elsewhere?  I want it to work best on Linux/KDE, my primary platform!\n\nI have less to say about (B), since it is a more clear argument.  Basically, it says we should take KDE everywhere.  This makes it a library layer on top of Qt (which it is) that application developers could use wherever they go.  This is not a bad idea, although it makes KDE seem like less of an integrated desktop if it is so movable like that.  KDE layered over X11 isn't quite so bad, since there really isn't anything to X.  But layering KDE over Windows could be strange.  Users might not understand why their FileDialog looks so different.\n\nIf you haven't figured it out from the above text, I choose (A) as the route of choice.  The downside people have mentioned are that developers wouldn't use specific KDE-API's since they would be trying to maintain a cross-platform app.  This is true, and is no different than avoiding direct use of the Win32 API either.  However, I don't think there is anything wrong with an #ifdef here or there.  I already do it for enough things on Win32 and X11 (like user-idle, system tray, etc), do you think it would stop me from adding KDE specifics as well?  Absolutely not, I am just waiting for KDE3 :)\n\nBut a proxy library \"libQtKDE\" would make commercial developers more interested in using KDE-specifics, since otherwise they need multiple binaries."
    author: "Justin"
  - subject: "Re: Discussion"
    date: 2002-01-24
    body: "Ok, so the first comment was way too long and a bit wacked.  But as stated in my second long post the problems comes from developers who have (or want) to cross-developer, but dont' care to have hundrededs of ifdefs around places that use kmainwindow vs qmainwindow and other simple stuff.  Why can't those ui classes be taken out of kdelibs and made cross-compilable in their own lib?  In the windows version it will use windows file dialog and in unux it would use kde's.  They they are not hindered about which the widget set to use.\n\n-Benjamin Meyer"
    author: "Benjamin C Meyer"
  - subject: "Re: Discussion"
    date: 2002-01-24
    body: "\"In the windows version it will use windows file dialog and in unux it would use kde's. They they are not hindered about which the widget set to use.\"\n\nNote that if you are developing primarily for windows (with the linux/mac port a bonus), I see very little incentive to use the kdelibs extenstions as Qt already has native dialogs under Win32."
    author: "drawoc suomynona"
  - subject: "Re: Discussion"
    date: 2002-01-24
    body: "Point taken, but those are not the primary target of this.  The primary target is all of the little open source projects that were created to run under linux, but are kept qt only so that they will work under windows.\n\n-Benjamin"
    author: "Benjamin C Meyer"
  - subject: "Re: Discussion"
    date: 2002-01-26
    body: "> Users might not understand why their FileDialog looks so different.\n\nJust a suggestion: What if one would port the kwin window manager to Win32 and use that as an alternative shell for windows (instead of explorer). LiteStep and others take this approach, why couldn't KDE? I think the portability of it all can do more good than harm.\n\nJust my two bits,"
    author: "Coolvibe"
  - subject: "KDE needs its \"own\" toolkit in the long run"
    date: 2002-01-23
    body: "The implications of basing KDE on Qt have been described before. Using Qt for KDE has been a good solution and probably will be a good solution for some time, but earlier or later KDE should get its own toolkit. This is of course a big task, but has to be done, its inevitable. This doesn\u00b4t mean all work on KDE3.0 should be halted, but one should aim at a replacement in a few years. The KDE developers have already shown that the can create a good framework. Why not carrying on extending it until Qt is obsolete. lib by lib, class by class.\n\nMarc"
    author: "Marc"
  - subject: "Re: KDE needs its \"own\" toolkit in the long run"
    date: 2002-01-23
    body: ">Why not carrying on extending it until Qt is obsolete. lib by lib, class by class.\n\nThat is not the right question.\n\nThe right question is why, not why not."
    author: "Roberto Alsina"
  - subject: "Re: KDE needs its \"own\" toolkit in the long run"
    date: 2002-01-25
    body: "Nobody is motivated to do it, since there is no real reason to do it (no \"pain pressure\").\nReplacing Qt would be a huge task, and Qt is GPL'ed, you could simply fork it, but then you would loose all future developments of Qt.\n\nIf you want to, do it, nobody keeps you away from doing it.\n\nBye\nAlex"
    author: "aleXXX"
  - subject: "This sounds wonderful!  I'd love to use it."
    date: 2002-01-24
    body: "I absolutely adore the KDE file dialog.  Funnily enough, it's one of my favorite\nparts of KDE.   I HATE using a Qt file dialog, after using the KDE one it's\nsooo frustrating."
    author: "TomL"
  - subject: "Why not provide a \"Gimp/Gnome\" integration to KDE?"
    date: 2002-01-24
    body: "For the Gnome desktop, GTK applications integrate naturally (such as \"the Gimp\"). Why not provide the same functionality to KDE ? \n\nApart from the technical discussion, I'm considering \"the growing number of cross-platform but Qt-only applications becoming available\" that would benefit from it.\n\nThere are already \"The Kompany\" products (as highlighted by Shawn Gordon) and the most obvious to me are:\n- Avifile\n- Qcad.\n\nI hope others could complement this list with Qt applications they love, use often & would greatly appreciate to integrate better in the KDE desktop."
    author: "Phil Ozen"
  - subject: "Welcome to OS/2 Workplace shell :-)"
    date: 2002-01-26
    body: "In the workplace shell you could insert a new class/object into an already existing (even running) inheritance hierarchy!\n\nWhen Windows 95 entered the scene with the famous X to close an application - a Xit application that inserted itself (read KApplication) above Object class (QApplication) showed up within a few days. This worked for ALL programs AND folders!\n\nCouldn't this be done with Qt only applications? You are able to replace system calls via weak(?) linking...\n\n[checkout Stardock - started with games and Object Desktop for OS/2 (zip folders, etc...)\n now they try to provide the same thing to Windows\n  http://www.stardock.com/products/odnt/\n]\n\n/RogerL"
    author: "RogerL"
---
<a href="http://www.kde.org/people/matthias.html">Matthias Ettrich</a> is <a href="http://lists.kde.org/?l=kde-core-devel&m=101170367405607&w=2">proposing a new strategy</a> for more strongly integrating Qt-only applications into KDE.  It's an intriguing proposal and relevant to KDE, considering the growing number of cross-platform but Qt-only applications becoming available.   The initial <i>"not-perfect but simple"</i> basic idea is that a small libQtKDE proxy library would invoke KDE functionality when available, or otherwise fall back to Qt functionality.  This would not involve changing Qt or KDE, but would require the programmer to link against the libQtKDE wrapper.  The <a href="http://lists.kde.org/?l=kde-core-devel&m=101171177428868&w=2">benefits</a> for the former Qt-only developer is that KDE functionality would be made available cheaply without giving up the Qt-only cross-platform approach.  The benefits for the KDE user is that what would have otherwise been a pure Qt-only application could now potentially integrate much more strongly with KDE.  <A href="http://lists.kde.org/?l=kde-core-devel&m=101173049524869&w=2">Good thing</a>  or <a href="http://lists.kde.org/?l=kde-core-devel&m=101171501707178&w=2">bad thing</a>?  Read on for the full details of the proposal.
<!--break-->
<br><br>
<hr>
<b>From:</b> Matthias Ettrich &lt;<a href=
"mailto:ettrich@trolltech.com">ettrich@trolltech.com</a>&gt;<br>
<b>To:</b> kde-core-devel@kde.org<br>
<b>Subject:</b> Qt-only KDE applications<br>
<b>Date:</b> Tue, 22 Jan 2002 13:45:39 +0100<br>
<b>X-Mailer:</b> <a href="http://kmail.kde.org/">KMail</a> [version 1.3.8]<br>
<br><br>
Some time ago there was a rant somewhere about the emerging number of
cross-platform Qt-only application that do not use KDE's API and thus
do not benefit from KDE's additional features on Unix. To make things
worse, those apps - although using the same core toolkit - do not integrate
much better into KDE than any other X11 application.
<br><br>
Qt 3.0's style plugins will improve the situation somewhat, but not
sufficiently. Here's a rough, non-complete list of features a Qt
application should be able to utilize when running on a KDE desktop:
<br><br>
<ul>
<li>Standard KDE dialogs:
<ul><li>File dialog
       <li>The amazing print system / printer dialog
       <li>Color dialog
       <li>Others (like the lovely about box)
</ul>
<li>Network transparency via KIO
<li>Exporting interfaces via DCOP<br>
<i>
(thanks to the new way of handling signals and slots in Qt 3.0 this
        can be done with Q_OBJECT only, no need for dcopidl and
        friends)</i>
<li>Access to standard icons and icon effects
<li>Obey other common settings for application main windows that go
   beyond the standard QStyle specification
<li>Use KDE's style hints even if qtconfig specifies something else
</ul>
<br><br>
One suggestion was to go cross-platform with parts of KDE and to
promote KDE as cross-platform API as well.
<br><br>
There's another solution, which I'd like to promote here.
<br><br>
KDE from the perspective of a cross-platform toolkit like Qt, is
really just another platform. Just as we use the standard MS-Windows
file dialog on MS-Windows, Apple's on MacOSX, Qt should be able to use
KDE's dialog when running on top of KDE.
<br><br>
The main reason why this isn't so easy, is the circular dependency: Qt
uses KDE uses Qt.
<br><br>
However, we don't need to start with a perfect solution. If would be a
major improvement already if we could make it <i>easy</i> to use the above
mentioned KDE features from otherwise Qt-only applications, without
having to write/maintaining two different versions of your application
code.
<br><br>
Technically, the simplest approach does not even require changing Qt or
KDE:
<br><br>
<blockquote>
We write another small library libQtKDE, that wraps KDE
functionality. Either there exists two binary compatible versions of
this library, one using Qt-only, one using KDE, or we make one version
of the library that dynamically loads a KDE plugin when installed.
<br><br>
The library contains a dummy application-class (so that it's possible
to reimplement virtual QApplication functions) which instantiates
either a QApplication or a KApplication.
<br><br>
In addition there are a bunch of static functions like
getOpenFileName, NetAccess::download, etc.
<br><br>
Later, the static functions in Qt itself (like
QFileDialog::getOpenFilename, QPrinter::setup, ...) could utilize the
extended versions, but that's a second step. We could also provide
cross-platform functions that instantiate an MSIE/ActiveX control on
MS-Windows and a KHTML/Konqueror on KDE.
<br><Br>
An application using libQtKDE is somewhere in the middle between a standard
Qt-only application and a true KDE application using the KDE API. But this is
seeing it from the programmer's perspective. For the user, it will much
closer to a true KDE application. Just that the same binary will run without
having KDE installed - with a slightly different look and feel and less
functionality.
</blockquote>
<br><br>
Opinions? Do you think that would be useful at all?