---
title: "KDE 3.1 Alpha1: Brings New Eye Candy, New Features"
date:    2002-07-11
authors:
  - "numanee"
slug:    kde-31-alpha1-brings-new-eye-candy-new-features
comments:
  - subject: "Congratulations (and broken link)"
    date: 2002-07-11
    body: "The title says it all: Could someone please fix the broken link to the compilation instructions in the announcement...\n\nWay to go! Thanks for a marvelous desktop."
    author: "Anonymous"
  - subject: "Re: Congratulations (and broken link)"
    date: 2002-07-11
    body: "The one that points you to the \"outdated\" page or is there another one?"
    author: "Navindra Umanee"
  - subject: "Re: Congratulations (and broken link)"
    date: 2002-07-12
    body: "The one with the outdated page. I did not see any other one."
    author: "Anonymous"
  - subject: "cool drop shadow"
    date: 2002-07-11
    body: "man, that drop shadow for menus are damn cool ! wonder what users of other competing desktop will comment :-)"
    author: "ariya"
  - subject: "Re: cool drop shadow"
    date: 2002-07-11
    body: "Yeah, this menu-drop-shadow really rules! It would also be nice if windows could have drop shadows. What do you think?"
    author: "Andreas Pietzowski"
  - subject: "Re: cool drop shadow"
    date: 2002-07-11
    body: "Hmm, I believe this is more of a job for X. Let's wait for Keith.\n\nAnyway, drop shadow menu combined with mouse cursor shadow looks really kick-ass. Pick-up good icons/window decoration/splashscreens/ and you're set to make your Windows friends totally jealous."
    author: "ariya"
  - subject: "Re: cool drop shadow"
    date: 2002-07-11
    body: "A BLACK X-mouse-pointer with a BLACK-drop-shadow...? *g*\n\nBTW: Which windows-friends? :)\n"
    author: "Andreas Pietzowski"
  - subject: "Re: cool drop shadow"
    date: 2002-07-12
    body: "Forget the pointer. Those goodies will keep people busy anyway :-)"
    author: "ariya"
  - subject: "Re: cool drop shadow"
    date: 2002-07-12
    body: "> A BLACK X-mouse-pointer with a BLACK-drop-shadow...? *g*\nOh, that reminds me of another new KDE feature: builtin white mouse cursor support :). I'm not sure if it made it in to 3.1a1, though.\n\n-Ryan "
    author: "Ryan Cumming"
  - subject: "Re: cool drop shadow"
    date: 2002-07-12
    body: "Someone please redesign those horrible X pointers. The whole font is butt ugly."
    author: "."
  - subject: "Re: cool drop shadow"
    date: 2002-07-11
    body: "Yes they look sweet.\n\nJust curious, does anyone want to explain how are these being done?  I thought shadows like these are impossible in X."
    author: "theorz"
  - subject: "Re: cool drop shadow"
    date: 2002-07-13
    body: "Better than windows; \"Show shadows under menus\" (Display Properties->Apperance->Effects) is a boring name. I don't know what exactly they are called in KDE, but I don't think it could be worse."
    author: "goo.."
  - subject: "announcement: point unclear"
    date: 2002-07-11
    body: "in the announcement, i believe it should be stated clearly that translucent menus are for all applications, not only for kate! (and they were already in kde3.0)"
    author: "emmanuel"
  - subject: "KDE-PIM"
    date: 2002-07-11
    body: "No screenshots and much \"work in progress\", but so far KDE-PIM package also has made great improvements compared to 3.0.x. Kudos!\n\nThorsten"
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE-PIM"
    date: 2002-07-11
    body: "Would it be possible for you to mention some of the kde-pim improvements/enhancements\nwe can look forward to?  I am eagerly looking forward to the new kde-pim stuff.\nI love the all the stuff now, and am excited about how good it will get in the\nfuture."
    author: "Tom Corbin"
  - subject: "Re: KDE-PIM"
    date: 2002-07-11
    body: "Ok, you have a kcontrol module for selecting an addressbook storage backend.\nSo far you can select old multi-vcard file, single-vcard dir and an (untested) sql backend. There is some talk about a possible server approach for most of the pim datas.\n\nkaddressbook has a configurable view style front end (tables, icons, cards...) with an instant category filter (show friends, show family). The card input looks like Outlooks contact dialog and is full featured. The addressbook has a printing backend.\n\nWhat else.\nThere is hard work in stabilizing an common KDE sync API based on ksync and kitchen sync. If you compile the ksync backend program you get a tool to sync your bookmarks, calendars and addressbook.\n\nKOrganizers support for publishing appointment to other PIM programs is now more compatible. A special highlight is a (free ;-) plugin for accessing an Exchange2000 server. But this is really work in progress and needs tester (*hint*).\n\nI hope that file locking is ready for 3.1 so that you can put one central adressbook, calendar, bookmark, etc.  file on a server and use this from multiple computers. I have one calender at home and another one at work. Often on both desktops runs an instance of korganizer. Caused by the missing file locking I can not use one file for both instances.\n\nOther stuff like Outlook-like integration of the pim apps and kpilot is out of my personal interests so I don't know much about recent improvements.\n\nIf you are interested in kde-pim enter the mailing list or listen via a read-only ML2NG mapping at \nnews://news.uslinuxtraining.com\n\nBye\n\n  Thorsten\n\n "
    author: "Thorsten Schnebeck"
  - subject: "Re: KDE-PIM"
    date: 2002-07-12
    body: ">So far you can select old multi-vcard file, single-vcard dir and an (untested) sql backend. There is some talk about a possible server approach for most of the pim datas\n\nThis would be very great. The mail problem my clients have with KMail is the inabillity to have a shared adressbook. If you could tap into an SQL-server (MySQL comes to mind!) they could all share one adressbook.\nI would be more than happy to test this feature!\n\nFor the rest of the KDE team: you are unbelievable! KDE's great..."
    author: "Paul Wiegers"
  - subject: "Re: KDE-PIM"
    date: 2004-12-12
    body: "I confirm this. \nThe only thing i am waiting very hard for is to connect my mysql addresses to kaddressbook!\nAll other stuff is great for me."
    author: "Stefan Stolz"
  - subject: "Konqi rocks"
    date: 2002-07-11
    body: "I took snapshot from cvs HEAD about week ago and I'll have to say that konqi is in very good shape. It's much more stable than 3.0.2 and startup speed is very good. Overall functionality feels more stable than alpha quality. 3.1 Will Rock Your gSocks Off ;)"
    author: "Jaana"
  - subject: "Screenshots"
    date: 2002-07-11
    body: "Screenshots......*drool*\n\nCongrats again KDE - especially everaldo on his wonderful icon\ntheme.\n\nKDE is really starting to distance itself from most other desktop\nenvironments without creating a totally alien environment.\n\nCan't wait to get home and install.\n"
    author: "Greg"
  - subject: "gcc 3.1"
    date: 2002-07-11
    body: "Hi!\nLooks great!!!\nI would really like to know if what we've been hearing about gcc c++ linker being not so good has been solved as we were told with gcc 3.1.\nDoes it finally make KDE fast loading apps? :-)\nI am really looking forward to have a good distro with KDE 3 compiled with gcc 3.1. This must fly!!!\nHuuuha!!!\n"
    author: "Anonymous"
  - subject: "Re: gcc 3.1"
    date: 2002-07-11
    body: "unfortunately, it seems g++ 3.1 isn't quite that nice, at least for kde:\nhttp://lists.kde.org/?l=kde-core-devel&m=102166265006814&w=2\n\n:O/\n\nemmanuel"
    author: "emmanuel"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "I read it\nThat's a shame :-(\nWhat has happened with all what was said?\nI am really surprised of that results... What is happening?\nAnd why should we use gcc 3.1 if it's WORSE BOTH in time compiling and starting apps?\n\nI guess KDE will be faster starting apps only in a couple of years due to faster PCs. Or should we hope on something I missed from the post? ->http://lists.kde.org/?l=kde-core-devel&m=102166265006814&w=2"
    author: "Anonymous"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "The results are a bit dubious, as the -g3 parameter does very different things under GCC 2.95 and GCC 3.1. I suspect that relocating the DWARF symbols that 3.1 produces with -g3 accounts for the extra relocation time. Oh, and GCC 3.1 produces faster code, more friendly warnings and errors, and is more standards compliant. Some people care about more than speed."
    author: "Ryan Cumming"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "Why?  Because GCC 2.95 sucked as far as C++ support was concerned.  Any C++ compiler suite that \n\n(1) didn't implement the STL correctly (look at the header files)\n(2) won't let you do templated friend classes (least never worked for me)\n(3) doesn't understand namespaces (2.95 faked understanding)\n(4) allows file descriptors to work with iostreams (bad behavior; the C++ Standard outlaws this)\n(5) a lot more that I'm missing\n\njust, well, is bleh.  GCC 3.1 fixes these. \n\nAs far as GCC 3.1 goes for speed, eh, oh well.  If you turn on the optimizing features of the thing, I've found that the performance is great -- both in synthetic benchmarks and in everyday use.  I've used GCC 3.1 to compile everything on this system, and although C++ compile time is slower (who really cares about that?), performance is more than acceptable."
    author: "dwyip"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "I guess you are right, but it really shocked me the benchmark results showed above.\nI didn't know about the 5 nice fixes you said. I only knew that speed was going to be better because this was the only thing explained in user forums like this one.\nGood to see that at least some problems have been solved.\nThanks for your work developers!!! :-)\n\nCould anyone give a reasonable schedule to see when (if ever) we'll see loading apps speed increase with better compilers? After all, is it really a problem, or just waiting for faster PCs is the way to go? Should developers concentrate their efforts on writting a nice desktop waiting for those faster machines in future?\nDevelopers replies with their feelings on the topic will be greatly appreciated."
    author: "Anonymous"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "> and although C++ compile time is slower (who really cares about that?)\n\nwell developpers on not so new (but no so old) cpus :("
    author: "azhyd"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "> well developpers on not so new (but no so old) cpus :(\n\nEh, I don't really mind.  Gives me an excuse to say \"I'm working\" when all I'm doing is waiting for a compile to finish :D"
    author: "dwyip"
  - subject: "Re: gcc 3.1"
    date: 2002-07-14
    body: "Not everybody in this world has the money to buy a new PC. Look at most of thecountries in the third-world. Linux (and ofcourse KDE and other open-source)has very much chances, because thos people don't have much money to spend and Linux is free. But if people have to spend a lot of money to run programs with acceteble performance they will proberly stick to a system like Windows 95, 98 or ME. Simply because these sytems run very well on low-resource machines."
    author: "Perfomance must improve."
  - subject: "Re: gcc 3.1"
    date: 2002-07-15
    body: "This is certainly true.\nBut it ain't simple to make it run faster.\nAnother option would be to have one quite fast server and a bunch of slow (e.g. old pentiums) boxes acting as X terminals.\n\nBye\nAlex\n"
    author: "aleXXX"
  - subject: "Re: gcc 3.1"
    date: 2002-07-16
    body: "> But it ain't simple to make it run faster.\n\nwell.. i hope gcc 3.1 will be further improved (a lot!) during the next months.\n\nanother idea: what about using the intel compiler suite instead of gcc-3.1? anybody tried this one together with kde 3.0/3.1? i've read the intel compiler made a lot of progress regarding \"gcc compatibility\" recently.. and it's supposed to be faster and create about 10 to 30 percent faster binaries than gcc 2.95.3."
    author: "dan"
  - subject: "Re: gcc 3.1"
    date: 2002-07-12
    body: "afaik the startup time is adressed in the dynamic linker which is a part of glibc and not of gcc. so we should see an improuvment with glibc-2.3. \nany experiences with glibc CVS?"
    author: "tschortsch"
  - subject: "Re: gcc 3.1"
    date: 2002-07-13
    body: "There is also a patch against glib-2.2.5 contained in the prelink sources:  ftp://people.redhat.com/jakub/prelink/prelink-20011010.tar.bz2"
    author: "Anonymous"
  - subject: "SuSE RPMs ?"
    date: 2002-07-11
    body: "Anyone made SuSE 8.0 RPMs of these yet ?"
    author: "jmk"
  - subject: "Re: SuSE RPMs ?"
    date: 2002-07-11
    body: "Yes!\n<p>\n<A href=\"http://download.au.kde.org/pub/kde/unstable/kde-3.1-alpha1/SuSE/\">http://download.au.kde.org/pub/kde/unstable/kde-3.1-alpha1/SuSE/</a>\n\n\n"
    author: "ac"
  - subject: "Re: SuSE RPMs ?"
    date: 2002-07-11
    body: "Redirects me to download.au.kde.org, which seems to be empty (no kde-3.1 dir in unstable)."
    author: "jmk"
  - subject: "Re: SuSE RPMs ?"
    date: 2002-07-11
    body: "There are some on download.kde.org mirrors (in unstable/kde-3.1-alpha1/SuSE/i386/8.0/); I'd assume Adrian/SuSE has made them, but I do not know for sure. \n"
    author: "Sad Eagle"
  - subject: "thanks cyberbrain.net!"
    date: 2002-07-11
    body: "Increased Apache MaxClients to 800:\n\n 10:51pm  up 3 days, 16:33,  5 users,  load average: 0.61, 2.00, 3.57\n749 processes: 738 sleeping, 11 running, 0 zombie, 0 stopped\nCPU states:  2.0% user,  7.7% system,  0.0% nice, 90.1% idle\nMem:   643696K av,  627828K used,   15868K free,       0K shrd,   18788K buff\nSwap:  819304K av,    4536K used,  814768K free                  211340K cached\n\nFinally, we're more comfortable now.  :-)"
    author: "Navindra Umanee"
  - subject: "Alpha ???"
    date: 2002-07-12
    body: "Great job guys!!!\nThis release seems to be more stable than 3.0.2. How could it happen??\nThe only issue is that this release don't have the drop shadow menus."
    author: "Daniel Dantas"
  - subject: "Re: Alpha ???"
    date: 2002-07-12
    body: "I don't have the alpha here but you can enable it under look --> style I think.\n\nhave fun\nFelix"
    author: "halux"
  - subject: "Closing tabs"
    date: 2002-07-12
    body: "Are there plans to allow for closing a tab in konqueror without having to right-click the tab, or use a menu option?  I've used tabbed environments in mozilla and visual studio .net, which handle this issue differently.  Mozilla provides an X on each tab, while vs.net provides an X to the far right of the tabs which closes the active tab.  IMO, both of these implementations are more efficient to use.\n\nI can't wait to get my hands on this one :)"
    author: "Matt Casey"
  - subject: "Re: Closing tabs"
    date: 2002-07-12
    body: "YEAH we need the X buttons to close each of the tabs like galeon tabs for example\nplease add this feature\n\n"
    author: "crazycrusoe"
  - subject: "Re: Closing tabs"
    date: 2002-07-12
    body: "btw, do u know how to enable the tabs by default, so that every time u click on a link it will open in a tab instead of a new window? because right now i have to right click on a link, then select Open In New Tab before it'll work and i want it to happen automatically."
    author: "DavyBrion"
  - subject: "Re: Closing tabs"
    date: 2004-07-21
    body: "Try Ctrl-click to open links in new tabs"
    author: "Yaacov"
  - subject: "shadow menu & transparent Panel"
    date: 2002-07-12
    body: "I just compiled kdelibs & kdebase with gcc3.1 and is running fine. I need to enable the shadow menu and transparent panel. how do I do that? I don't see any option for that in the control center....\n\n-Mathi"
    author: "Mathi"
  - subject: "Re: shadow menu & transparent Panel"
    date: 2002-07-12
    body: "No transparent Panel in alpha\nAlso no shadows(its post alpha)\n"
    author: "Anon"
  - subject: "Re: shadow menu & transparent Panel"
    date: 2002-07-12
    body: ":("
    author: "Mathi"
  - subject: "Re: shadow menu & transparent Panel"
    date: 2002-07-12
    body: "Pull the patches for these revisions from WebCVS, apply them and recompile:\n  http://lists.kde.org/?l=kde-cvs&m=102587761819332&w=2\n  http://lists.kde.org/?l=kde-cvs&m=102587727619017&w=2"
    author: "Anonymous"
  - subject: "Re: shadow menu & transparent Panel"
    date: 2002-07-12
    body: "can you explain me how to pull the patches from WebCVS or point me to some doc.....\n\nthanks a lot,\n-Mathi"
    author: "Mathi"
  - subject: "Re: shadow menu & transparent Panel"
    date: 2002-07-13
    body: "Are you sure, that you have the right knowledge to do all this? Read the links, search the file in WebCVS e.g. http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdefx/kstyle.cpp and click on \"Diff to previous 1.17\"."
    author: "Anonymous"
  - subject: "Great!"
    date: 2002-07-12
    body: "hi,\ni love kde. its looking great and i am doing all my work with it!!!\n\ni would love an integrated pim (like evolution/outlook). maybe sometimes it becomes reality.\n\ngreetings\ngunnar"
    author: "gunnar"
  - subject: "System Req.s"
    date: 2002-07-12
    body: "What i would like to see are the minimum system req's for KDE 3.1alpha1. I have alot of friends willing to do this \"linux/kde\" thingie with decent hardware although not the latest and greatest there is.. (bit hard when you a student..).\n\nFurther more i would like to congratulate the developers on another great release. Keep up the pace and good work guys..\n\nOn a sidenote, i hear / read alot of article about how UI design is bad on KDE or how developers are only interested in personal quirks and dont care for users in general. I myself take these comments with a large grain of salt but it does raise some problems. Perhaps some PR could turn the tide around for the good. \n\nKudo's on another good job...\n\nWalther"
    author: "Walther"
  - subject: "KXine as Video Player in KDE?"
    date: 2002-07-12
    body: "It would be good for KDE to have this KXine player which is quite good. visit:\nhttp://kxine.sourceforge.net."
    author: "Rizwaan"
  - subject: "Re: KXine as Video Player in KDE?"
    date: 2002-07-12
    body: "KDE 3.1 will ship with a xine arts wrapper that will use xine to play movies through noatun and kaboodle (and anything else that supports arts).\n\n"
    author: "Simon MacMullen"
  - subject: "Re: KXine as Video Player in KDE?"
    date: 2002-07-12
    body: "That doesn't look much like a KDE app to me, though it does look a lot more usable than anything from Xine's UI people. :-)\n\nI'll stick with Kaboodle and aRts, though.  Though I watch Kiki using Ogle and my DVD drive, I have been watching Lain with Kaboodle from CVS HEAD in recent weeks."
    author: "Neil Stevens"
  - subject: "slow rtc connection"
    date: 2002-07-12
    body: "it would be nice to have patches against stable releases tarball:\nlike a kdebase3.0.2-3.1.a.tar.bz2 so that modem users can test them too.\nplease?"
    author: "jaysaysay"
  - subject: "Re: slow rtc connection"
    date: 2002-07-12
    body: "If you're on a modem, use cvsup so that you can pull down patches that way.  http://developer.kde.org/source/cvsup.html\n\notherwise, feel free to make your own patch downloads and make them available to people."
    author: "Neil Stevens"
  - subject: "Re: slow rtc connection"
    date: 2002-07-12
    body: "anyway i did get arts libs and base, wow it's impressive.\nKonqueror is much faster on this pIII 600.\ngreat work!"
    author: "jaysaysay"
  - subject: "Faster login?"
    date: 2002-07-12
    body: "Is the login process faster in this release?\n\nOn my default SuSE 8.0 install it takes up to 30 seconds on a 433MHz Celeron box."
    author: "Cihl"
  - subject: "Re: Faster login?"
    date: 2002-07-12
    body: "depending on ram size, subsequent logins happen very quickly. most of the stuff stays in cache, and is readily available.  my win2k can also take up to 30 seconds from login to the time i have an available desktop. they just show you the desktop a little sooner.\n\nif you need fast logins, might i suggest ICEwm, fvwm2, for for blazing logins, twm."
    author: "Mark"
  - subject: "Newbie question"
    date: 2002-07-12
    body: "I know Suse 8.0 rpm's exist for KDE 3.1 alpha but I was wondering\nif there were any specific instructions available to guide someone\nin the proper install order?\n\nThanks,\n\nTodd"
    author: "Sven"
  - subject: "Problems with KDE 3.x"
    date: 2002-07-12
    body: "Here are the problems that I (as a relative linux newbie <1yr) ran into with KDE 3.x\n\n1.  Installation, Installation, Installation - you download a million RPMs, get someone in the newsgroup to give you the exact -Uvh or whatever command and install.  Then X goes nuts - the system still tries to start kde2 - the system is useless.  Back to Windows and to newsgroups, thankfully someone points out how to change the start-up sequence.  Now kde 3.x starts fine.  Still pain in the ass.  On the other hand, I went to Ximian.com, downloaded their little utility and it updated gnome without my intervention - beautiful.\n\n2.  Fonts - yeah, I know, every distro has its own \"import your true type fonts\" utility, but they all suck compared to KDE utilities.  It would be wonderful, if the Fonts node in the KControl center would include an option to install your fonts either from a folder or a windows partition or a just a single font.  \n\n3.  Resolution - This is probably not a KDE function, but again, it would be great if it were an option in the Control Center.  Dropping to a console to do this is silly, given that it changes the resolution of the desktop, not the console.  Is there a technical reason that this can't be done, because I can't believe I am the first person to be annoyed by this.\n\n4.  Themes - I heard that someone has already committed the code, but, yes, an applet that combines icons, windows decoration, etc... is sorely needed.\n\n5.  Default KDE look is ugly.  All the screenshots are great.  Why not make the default look that way. \n\nOther than that, very nice."
    author: "Frank Rizzo"
  - subject: "Re: Problems with KDE 3.x"
    date: 2002-07-12
    body: "Look under KControl/System/Font Installer..."
    author: "Craig"
  - subject: "Re: Problems with KDE 3.x"
    date: 2002-07-12
    body: "1. I really think 'rpm -Uvh *' is not that difficult to remember (not much more than the name of 'you know this kde installer thingy'). But anyway some people are working on this already, I'm not sure if that's going to be part of 3.1 though.\n\n2. KDE is not limited to Linux/XFree86, that's why there is no font installer in the 'official' KDE packages. However several XFree86 font installation/management programs exist for KDE so it's really up to the distributions to include whichever they see fit.\n\n3. Same as above, that's really an XFree86 issue. You can change your resolution whith Ctr-Alt +/-, but if you want to change your screen size you have to restart X (that's a real pain).\n\n4. You're right there, a recurrent (and rightly so) complaint.\n\n5. The style in the screenshots (Keramik) will be the default in 3.1 (but probably with a blue color theme).\n"
    author: "Chris"
  - subject: "Re: Problems with KDE 3.x"
    date: 2002-07-13
    body: "i cant agree with point 1) here.\n\non the one hand you said compiling kde was difficult and on the other hand you used ximian red carpet to install a precompiled gnome which was easier.\n\nwhy did you compile kde at all then ? i mean why didn't you simply grab the precompiled kde binary packages and installed them ?\n\ni want to see you installing gnome 2 from sources which slightly require more shit like python+expat, (open-)jade, jadetex, docbook sgml 3.0/3.1/4.0/4.1, docbook xml (with correct XML catalog), scrollkeeper... setting up all these necessarities is more painfull.. even kde CVS compiles are far easier since you deal with the checked out sources only."
    author: "Martin"
  - subject: "Re: Problems with KDE 3.x"
    date: 2002-07-13
    body: ">>why did you compile kde at all then<<\n\nI didn't compile, I downloaded premade binaries for Mandrake 8.2\nWhere did I say that I was compiling?"
    author: "Frank Rizzo"
  - subject: "Re: Problems with KDE 3.x"
    date: 2002-07-13
    body: "sorry i have misread something."
    author: "Martin"
  - subject: "Re: Problems with KDE 3.x"
    date: 2002-07-17
    body: "I'm using FreeBSD. Installation of KDE is as simple as typing \"pkg_add -r kde\".\nThat's all - you get the latest KDE. No need for Ximian addons."
    author: "fura"
  - subject: "Re: Problems with KDE 3.x"
    date: 2003-06-28
    body: "On fonts:\n\n1) I had KDE 2.2 using my true type fonts, installed KDE 3.1.2 and it just didn't use my fonts, I also have Opera which continued running with my fonts. I had to run the Font Installer in admin mode so I can get my fonts back into KDE.\n\n2) Why the hell the fonts choices for Konsole are limited, I am forced to use a font with wide gaps aroung every letter which looks very ugly.\n\nKonsole:\n\nUsing the XTerm (XFree 4.x) keybord scheme I can't use my home and end keys, I can't copy with Ctrl-Ins as I used to in KDE 2.2.\n\nIt's a bigger problem if things that used to work stopped working than if things don't work, I wonder why does anybody want to change something that works for people now. "
    author: "Martin Tsachev"
  - subject: "Slick Stuff"
    date: 2002-07-12
    body: "KDE3 looks to be very slick.  Thank you developers."
    author: "VonDrake"
  - subject: "Hmmm"
    date: 2002-07-12
    body: "Can someone explain how this Desktop Sharing thing that I see in the screenies works, exactly? Easiest way would be to use X's remote viewing capabilities, but that would introduce permissions issues, and it wouldn't really allow one person to watch another using the app... Perhaps VNC is used instead?"
    author: "Carbon"
  - subject: "Re: Hmmm"
    date: 2002-07-12
    body: "Yep, it's VNC, but with real desktop sharing instead of opening an additional X server for every VNC connection. \nSee: http://www.tjansen.de/krfb/\n"
    author: "Anonymous"
  - subject: "Re: Hmmm"
    date: 2002-07-14
    body: "When you use the old krfb versions, note that the user interface of the 3.1 version in completely different. The old krfb was a permanently running kicker applet, the new one is only running during a connection. This confusd many users of older versions."
    author: "Tim Jansen"
  - subject: "They forgot the moast important feature."
    date: 2002-07-14
    body: "They forgot the moast important feature.  Dark-Transparant kicker boarder!\nI saw a patch at kde-look. Please apply!"
    author: "Jimmy Wennlund"
  - subject: "Re: They forgot the moast important feature."
    date: 2002-07-14
    body: "where where ? on kde-look.org ? is there a screenshot ??\ndo you mean, that finally the border of Kicker has got the size \"0\" ?\nso that a (pixmap)-background will be shown without that 'unprofessional' border-view finally ? must look smooth :))\ngreetz\n"
    author: "me"
  - subject: "khtml and vnunet.com?"
    date: 2002-07-14
    body: "\nI'm running KDE 3.0.1 (haven't had time to upgrade to 3.0.2 yet). \n\nCan someone running 3.1alpha try out www.vnunet.com and tell me if it displays  without some text areas overwriting themselves and making an unreadable mess.  The Javascript menus driving the \"News areas\" down the left hand side bar are a good example of this.  In fact, if you compare how this site looks between khtml and mozilla, you can see a lot of differences.  khtml renders it wrong in a number of places.\n\n"
    author: "John"
  - subject: "Re: khtml and vnunet.com?"
    date: 2002-07-15
    body: "Not working :-("
    author: "Jaana"
  - subject: "Howto install SuSE8 - KDE3.1alpha1 binaries ???"
    date: 2002-07-14
    body: "Hi folks,\ni lately updated to KDE 3.0.2, and, i am really happy with that great stuff!\nKDE 3.0.2 rocks da house !!!\nbut, of course, i also wanna try the new \"KDE 3.1 alpha1\" ;)\nwhen i tried to install the rpm's into a new directory with the option \"--prefix /opt/kde3.1alpha1\"..\ni got the ERROR: \"package is not relocateable\"..\ni downloaded the packages from \"ftp.kde.org\".\nDoes anybody know how to manage that easily ?\n(i already tried this: \"http://developer.kde.org/build/build2ver.html\" )\n\nthanks very much.. :)\n\n..a kde lover"
    author: "KDE3.0.2 User!"
  - subject: "Re: Howto install SuSE8 - KDE3.1alpha1 binaries ???"
    date: 2002-07-14
    body: "The option --bad-reloc of RPM should help, try it."
    author: "Alex"
  - subject: "Re: Howto install SuSE8 - KDE3.1alpha1 binaries ???"
    date: 2002-07-14
    body: "thanks, but, that didn't help,\nnow i tried this:\nrpm -ihv --relocate /opt/kde3=/opt/kde3.1_alpha1 --badreloc --nodeps *.rpm\nbut then the console-window was scrolling much conflict-messages like this:\n\n\"/usr/share/doc/KDE3-API/libkmid/notearray_h.html from install of kdelibs3-devel-doc-3.0.6-0 conflicts with file from package kdelibs3-devel-doc-3.0.2-4\"\n\ni wish i can keep the stable \"3.0.2\" installation but also can test the new\n\"alpha1\". is it possible that both \"co-exists\", how ??\n\ngreetz\n:)"
    author: "KDE3.0.2 User!"
  - subject: "Re: Howto install SuSE8 - KDE3.1alpha1 binaries ???"
    date: 2002-07-15
    body: "hi,\ni managed now to install, but, i really cannot start the alpha1-version..\ni already created a new user 'alpha1' and changed many variables like\n$WINDOWMANAGER or $KDEDIR,... and also i set them inside the files \".bashrc\",\n\".xinitrc\" or \".profile\".\ni also linked the file \"/usr/X11R6/bin/kde\" from \"/opt/kde3/bin/startkde\" to\n\"/opt/kde3.1_alpha1/bin/startkde\" and at last i moved kde3 to kde3_ and linked\nkde3 to the folder of kde3.1_alpha1. (then kde or X crashed) i tried and tried.. \nnow, that both version co-exists, HOW DO I START ? ;)\nanybody may help ??\n\nthanks"
    author: "KDE3.0.2 User!"
  - subject: "Re: Howto install SuSE8 - KDE3.1alpha1 binaries ???"
    date: 2002-07-16
    body: "check out the startkde file .. i guess there are some things hard coded to /opt/kde3 ..\n\nrgds\nmarc'O"
    author: "marc'O"
  - subject: "Quick Browser and window shades"
    date: 2002-07-15
    body: "Could someone try the QuickBrowser on the 3.1alpha and tell me if you can open a Konqueror window by clicking on a QuickBrowser folder? I will frequently work in a  common folder and the QuickBrowser can be a good way to get there but I'd have to traverse that tree ever time for each file I'm working with. Operating like OS/2's  SmartCenters Drive Folders would be a great feature. I could create bookmakes in Konqueror (and do) but this feature in QuickBrowser would be better. IMHO.\n\nThe other thing I found \"interesting\" is the window Shade operation. I just don't want it to unfold and foldup when I move my mouse off/on the window. How about the old way of just double-clicking the title bar or unfolding when selected from the taskbar?\n\nKDE is looking awesome these days and with word of Konquerors speed increases, it might be my new \"home\" for a while. Great work people."
    author: "bc"
  - subject: "A feature I would really, really like to see"
    date: 2002-07-18
    body: "in Konqueror is the following:\n\n* drag an URL from the Location Toolbar to the Bookmark Toolbar: it should be added as a new bookmark\n\n* right-click on an item in the Bookmark Toolbar: instead of a Toolbar Menu, I would like to see options to rename/remove the bookmark, or to change its icon.\n\n* I would like to be able to add a menu to Kicker which contains all items in the Bookmark toolbar. The reasoning is simple: My Bookmark Toolbar contains all my most visited websites. I also want to be able to access them quickly (meaning no more than two mouse clicks) if no Konqueror window is open!\n\nJust some suggestions... I think KDE is a great piece of work and I couldn't live without it!\n\nKavau"
    author: "kavau"
  - subject: "Re: A feature I would really, really like to see"
    date: 2002-07-18
    body: "You can already add a \"bookmarks\" menu to Kicker. I think you need to use Add... Special in KDE 3.0\n\ndave"
    author: "dave"
  - subject: "KGet"
    date: 2002-07-21
    body: "KGet is a killer app. that is what I needed. Thanks!"
    author: "me"
  - subject: "Re: KGet"
    date: 2005-04-29
    body: "uhuh"
    author: "re"
  - subject: "Compiling kde on redhat 7.3"
    date: 2002-07-26
    body: "Has anyone got a sucess story with compiling kde on Redhat 7.3\nI have tried compiling it with redhats gcc 2.96 and with gcc 3.1 (built from source). Compiling it with gcc2.96 does not work as it fails to build, building it \nwith 3.1 works but kde crashes at startup with dcop related errors ( stuff about it thinking that another dcop is running ). Anyhows my question is has anyone been in this situation and found their way outa it yet ? I've been using gnome for the last week but I want the new kde wm so bad !\n\nThanks in advance\nBryan"
    author: "bryan hunt"
  - subject: "Re: Compiling kde on redhat 7.3"
    date: 2002-09-09
    body: "dcop errors give me a hard time here..\nstill can't find the way to fix it."
    author: "wooopy"
---
A brand new alpha of the breath-taking KDE 3.1 development branch <a href="http://www.kde.org/announcements/announce-3.1alpha1.html">has been announced</a>.  This release sports everything from wonderful <a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/">new eye candy</a> to tons of popular <a href="http://developer.kde.org/development-versions/kde-3.1-features.html">new features</a> including new and exciting "easter eggs" (aka <A href="http://bugs.kde.org/">bugs</a>) just waiting to be discovered.  Remember, this is not a stable release -- those of you concerned with stability should use <a href="http://www.kde.org/info/3.0.2.html">KDE 3.0.2</a>, whereas those of you who want to help KDE 3.1 be the best KDE ever should use this alpha.  Kudos to <a href="http://www.kdeleague.org/">Dre</a> for writing <a href="http://www.kde.org/announcements/announce-3.1alpha1.html">the announcement</a> and to the tireless <a href="http://www.kde.org/people/dirk.html">Dirk Mueller</a> for coordinating this release.  Party!



<!--break-->
