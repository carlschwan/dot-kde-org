---
title: "Mosfet.org on Desktop Elegance"
date:    2002-04-28
authors:
  - "numanee"
slug:    mosfetorg-desktop-elegance
comments:
  - subject: "Irony!"
    date: 2002-04-28
    body: "This guy lives in Korea.\nHe work for a Korean company.\nThat company has just sold 120,000 units of its software to the Korean govt.\nHe wants to learn to speak the Korean language.\nEven his wife is Korean!\n\nSo how does he explain comments like \"And the letter 'K' is kind of offensive\"!?"
    author: "Just Me"
  - subject: "Re: Irony!"
    date: 2002-04-28
    body: "Believe it or not, some people don't use the latin alphabet :-)\n\nIn the latin alphabet the K is very angular, which is what I suppose he dislikes in some way.\nI kind of agree, although as Mosfet says, the icons are easily changed."
    author: "abdulhaq"
  - subject: "Re: Irony!"
    date: 2002-04-28
    body: "The letter F is even more angular, but on the other hand the letters U and C are rounded and curved. The angular letters represent the musculine principle and the curved letters represent the feminine principle.\nThats why the word FUCK is so beautifully balanced. Naturally so."
    author: "reihal"
  - subject: "Re: Irony!"
    date: 2002-09-08
    body: "Take the time to meet some Koreans...\n\nAsk them about the traditional spelling (before the Japanese invasion).\n\nThen you will understand why all the Coreans held up signs saying \"Corea\" at the World Cup this year..."
    author: "D Herring"
  - subject: "KDE beats all"
    date: 2002-04-28
    body: "I have the opposite opinion.  KDE's beauty is *unmatched* by any desktop out there free or commercial.  I am visually crippled when I try to use another desktop.  Don't get rid of the K ever.\n"
    author: "ac"
  - subject: "Re: KDE beats all"
    date: 2002-04-28
    body: "YES, \n\nand thanks for the icons \n(symbolic, ease to understand and beautiful)\n\n\n"
    author: "thil"
  - subject: "Re: KDE beats all"
    date: 2002-04-29
    body: "I especially like the KDE 3.0 startup screen, it's the nicest I've ever seen. All that needs work is the icons, but beyond that, let's admit KDE is up there with XP and OS X.\n\nPeace out, JohnFive\nPS Keep up the good work K Developers!\n"
    author: "JohnFive"
  - subject: "Re: KDE beats all"
    date: 2002-05-09
    body: "Darn right. Keep the K. It stands, after all, for Karl's Desktop Environment!!!"
    author: "Karl"
  - subject: "If you don't like K's"
    date: 2002-04-28
    body: "Mosfet:\nAlso, being a free software project, if he wanted a KDE with alternate names to applications he could of done so easily.\n>>>>>>>>>>>>>>>>>>>\n\nFor an example, look at Lindows <http://www.lindows.com/lindows_products.php> and its apps WordPublisher and Spreadsheet Pro. They may look familiar.  ;-)\n\nNot that there's been a lot of people paying $339 for KWord..."
    author: "Otter"
  - subject: "Re: If you don't like K's"
    date: 2002-04-28
    body: "They know why they renamed the programs! Not, because the letter K in particular was offensive, but people who don`t know KDE usually find it strange when programnames start with a letter that does not really belong to the words used in the names.\n\nI keep getting asked \"what`s this K for?\". It`s probably a benefit for the whole KDE which becomes well known this way, but it`s not making the program names attractive. In fact it makes them hard to pronounce and look unfamiliar to novices.\n\nIt would already be nicer if the K was somehow separated, like k-word or k-presenter, so people see it is just word or presenter, but a special version called k-*.\n\nStefan"
    author: "Stefan Heimers"
  - subject: "Re: If you don't like K's"
    date: 2002-04-29
    body: "Probably the slickest way to do that would be with capitalization, like kWord, kPresenter, ...er .. kCookiejar? \n\nI'm way leery of this 'Lindows' thing.\n\nI feel that if any 'clutter' was to be reworked in KDE, maybe reorganizing the parts of kControl, and combining and refining some of them.\n"
    author: "Juln"
  - subject: "Re: If you don't like K's"
    date: 2002-04-29
    body: "How about KDE Word, KDE Presenter\nA well-known big company solved it easy.\nMS Word\n\nThe program is word, but it comes from the KDE project instead of MS...\nJust my ?0.02\n\n"
    author: "Syllten"
  - subject: "Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "<b>DISCLAIMER: I am not responsible for hurted feelings, offended people, flamewars, infected computers with Windows installed on it, damaged computers, stolen girlfriends and drunken guinny pigs.\n\nAlso, this is not intended as a flame or troll.\n\nThe only thing I'm trying to reach with posting this message is to get a visually more attractive KDE (standard looks) so that the new users will like it too and we can forget about the horrible Windows</b>\n\nKDE is a great piece of software, it's very nicely written, it's code is easy to understand, and the internals are great (Microsoft can learn a lot from you KDE guys ;)\n\nHowever, KDE is all about the _desktop_ and making it easier for users to use Linux (clicking and dragging instead of typing long texts in the console).\nUntil some time ago, Linux and KDE was mostly used by one type of people: programmers.\n\nNow with all of the new users coming to Linux and KDE every year, there will be a lot less programmers and more 'normal' users.\nThese 'normal' users are only interested in a few things: speed, stability, nice looks and games :o)\nWell, Linux itself is pretty fast and very stable.\nNormal users don't use the command line, they want a visual environment to use, so they try KDE (or GNOME or whatever).\n\n<i>- This is the most important part, read this very carefully -</i>\n\nThe new user starts KDE, and 20 seconds later the user starts to scream and running around the room.\nWhy? Simple, because he'll see the most ugliest color combinations, window theme and widgetstyle he has ever seen.\n\nThis, is also known as the 'first impression'.\n\nIf the first impression is not good, the user won't use the software and run back to Micro$oft.\n\nNow most people will say: hey but it's only the default look! you can change that easely!\nYes, I know that, but new users (those coming from Windows) have no idea about how to change a theme/style or other thingies, even when there's a large button yelling at them where the control center is.\n(believe me, I've seen my father and mother working with Windows and he wouldn't be able to find out how to change a style/theme in KDE\nHeck, they couldn't even found the mousecursor back the first time! :)\n\nIf the default theme/style would be beautiful, and those other people would like to have a 'ugly' desktop again, they can easely change it back because they DO KNOW how to change the settings.\n\n<i>- You may read less carefully now -</i>\n\nNow, my point is that programmers and some other people don't really care about how their desktop looks, they only care about the internal workings (usually) and most of them can't combine colors and have a very bad visual taste (nofi).\n\nNormal users expect to see something nice when starting a new program or in this case; Linux with KDE.\n\nSo please, let the programmers do what they're good at (and they are)\nAnd let the REAL (non-wannabee) artists do what they're good at (Everaldo, you're the best!)\n\nI've read at the KDE-artists mailing list that Everaldo will probably be making  new standard icons for KDE 3.1 ..\nWell, I really hope that will happen (along with a new default theme and style) because KDE really <b>deserves</b> it to look nice!\n\nInternally it's a wonderful DE, so let's make it visually attractive too.\nThat way, Linux+KDE beats Windows (including XP) in 2 ways: internal and visually.\n\nGod/Allah bless Tux, Linux, KDE, all of it's programmers and other people, Mosfet (but not for the standard theme/style ;) and Everaldo ;)\n\nThanks for reading this message.\nAmen.\n\n\nps. Replace the HTML tags yourself while reading this message, dot.kde doesn't allow me to use HTML encoding for my message :/\n"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "Are you sure you've used KDE?  The first thing a new user sees is KPersonalizer which lets you choose whatever look you want for your desktop.\n\nI strongly recommend the default which beats all desktops hands down.\n"
    author: "ac"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "I've used KDE 1.x, 2.x and I've just switched completely to Linux with KDE 3.0, 3 weeks ago so I'm now using KDE everyday.\n\nKPersonalizer is a nice idea, but it doesn't let the user choose a *nice* look, and by pressing Next -> Next -> Next without choosing anything (like the average user does) will give you an uber-ugly-desktop :/\n\nUsers should see the desktop and say: \"Wow! That's cool! I wanna use this forever and I'll ditch Windoze!\"\n\nThey'll definitly not say that with the default theme/style(s).\n\nAlso, this default theme/style is used in magazines, ads and other things, so that's another reason why it should look *good* (to attract new people)"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "But it does look good.  Which looks good to you?  Aqua?  I think Aqua is a stupid waste of resource."
    author: "ac"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "Sorry, but IMNSHO it doesn't..\n\nAqua does look much better, but it's not how KDE should look (KDE should have it's own 'identity' so that people can recognize it).\n\nAltough the dropshadows would be a good idea because they make the windows stand out instead of making everything flat so you can easier pick a window to move, close or whatever.\n\nAnd what you are talking about (waste of recources) is something very different than I was talking about (redesign of theme/style/iconset to make it better looking, not extra features)\n\nShadows wouldn't waste that much resources btw\n(if it were possible to use alpha channels with X - long live DirectFB/Berlin :)\n"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "XRENDER is really cool."
    author: "odinhuntr"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "Well, the problem with the XRENDER Extension is that you won't get an alpha channel.. (afaik)\n\nFor example, you cannot have 2 translucent windows, where one window is hovering over the other window, while the contents in the lower window (ie. a progressbar, or a movie, etc) are being updated (so you can see it through the upper window)\nWell, you can.. but with the XRENDER extension the lower window wouldn't be updated.\n\nThat's why it's currently not possible to use dropshadows because the contents behind the shadow wouldn't be updated when nessecary (when moving a window, etc)\n"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "I think this is precisely what XRENDER _does_.  It gives you true transparency, whereas in the past all we've had is that fake, pseudo-transparency."
    author: "Tack"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "Sorry but I wasn't talking about the pseudo 'fake' transparency (stippled) and 'real' transparancy (aka alpha blending) cases.\n\nBelieve me, XRENDER does NOT allow you to have nice alpha blended ('real transparency') (drop)shadows behind your windows.\n(maybe static, but that would be plain ugly when moving anything)"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-29
    body: "What you want is http://www.xfree86.org/~keithp/talks/KeithPackardAls2000/index.html"
    author: "ac"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-29
    body: "Yeah, the Translucent Window Extension is what I meant.. but AFAIK that's not usable yet.. :/"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-29
    body: "Have you ever stopped to consider the fact that anything that has to do with\nlooks is a matter of personal taste ?  I have not seen any form of survey, let\nalone a study that you or anyone else did that shows people choose a particular desktop system only and simply based on looks!!! For crying out loud people this is getting old.  This same thing gets hashed over everytime a new version of KDE is released.\n\nI personally like the default look and feel.  I use the Slick icons because they well work better for me.  Period.  I can guarantee you 100% that if the default kde-look is changed to whatever you think is \"uber cool\", there will be as many people crying it is \"butt ugly\"!!!  Then what ?  What is the condition for passing the \"uber-cool\" level ? And this is exactly why the thing is configurable.  You do not like it. Change it.  If the use is so close minded as to not at least ask someone if this is changable, well then perhaps (s)he should stick with what they know.  The default look of KDE has steadly improved and is looking more refined every release, but the day it goes for uber-colness is the day of its downfall.\n\nRegards,\nDawit A."
    author: "Dawit A."
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-30
    body: "<quote>\nYou do not like it. Change it.\n</quote>\n\nThis quote only tells me that you haven't read my original post very well..\n\n\nAnyway, like I've already said in another post, I think there should be some compromises between the 'ugly' and 'nice' looks (unfortunately, there is no nice look, yet) to create a new default look that appeals everyone.\n\nThat way, everyone can be happy, and we all have what we want: a perfect Desktop Environment with a default nice/ugly look :)\n\nAnd it doesn't have to be 'uber-cool' ..\n\nYou guys probably think I would like to see something like the XP or Keramik style as the default look.. but I wouldn't.\nAltough they look spiffy, they're not really usable for daily work.\n\nWhat I personally would like to see is a look that's cleaner than the current one, designed by a (non-wannabee) artist instead of a programmer, better color combinations and better use of gradients (and not overuse).\n\nAnd XP or Keramik doesn't come even close to the previous description (and windoze neither)"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-30
    body: "Why don't you design a style and contribute it to the KDE project?\n"
    author: "KDE User"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-30
    body: "Heh I knew someone was going to say that :)\n\nThat's indeed a very good idea!\n\nI'll see what I can come up with in the next few months, and post it at kde-look.org\n\nIf people like it (at least a score of > 90%), I'll contribute it to the KDE project.\nIf not, I'll shut my mouth and I won't complain anymore.\n\nDeal?\n"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-30
    body: "Deal! Hooray!\n"
    author: "KDE User"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "<quote>\nNormal users don't use the command line, they \nwant a visual environment \n</quote>\n\nthis is very true...for linux to push it's way onto the desktop\nusers should NEVER come into a situation where they HAVE to use\nthe command line...(that dosn't stop the rest of us from using it though!)\n\n<quote>\nhe'll see the most ugliest \ncolor combinations, window theme and widgetstyle \nhe has ever seen.\n</quote>\n\nI don't think it's quite that bad...still better than gnome in my opinion\n\n<quote>\nnew users (those coming from Windows) have no \nidea about how to change a theme/style or other thingies\n</quote>\n\nwhat's your point? i know a LOT of windows users who don't know\nhow to change windows themes and can just barely check their email?\ndoes that mean we need to make kmail easier for windows users too?\n\n<quote>\nLinux+KDE beats Windows (including XP) in 2 ways: internal and visually.\n</quote>\n\nAlready does!!!\ni have been using Linux/KDE for 4 years and i all i can say is that it has\nreally come a long way in such a short period of time, and is only getting\nbetter at exponental rates.  \n\nKDE rocks!"
    author: "Brad"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "<quote>\nthis is very true...for linux to push it's way onto the desktop users should NEVER come into a situation where they HAVE to use the command line...(that dosn't stop the rest of us from using it though!)\n</quote>\n\nIndeed, the commandline is very handy and should always be there of course :)\n\n<quote>\nI don't think it's quite that bad...still better than gnome in my opinion\n</quote>\n\nYes, but this isn't about GNOME ;)\nGNOME 1.x is even more ugly, dark and gloomy (IMNSHO)..\n\n<quote>\nwhat's your point? i know a LOT of windows users who don't know how to change windows themes and can just barely check their email? does that mean we need to make kmail easier for windows users too?\n</quote>\n\nOften when I say the default KDE theme/style/iconset is ugly, people tell me I can change everything myself.\nBut those Windows users can't and even don't want to configure everything theirselfes to make it look better.\nThey want to install and use it right away (remember the talk about the 'first impression' in my first post?).\n\nKMail is something different, it's not a configuration thingie but a program for daily use and not comparible to a few settings.\n\n<quote>\nAlready does!!!\ni have been using Linux/KDE for 4 years and i all i can say is that it has really come a long way in such a short period of time, and is only getting better at exponental rates.\n</quote>\n\nI agree, it has indeed come a long way in a very short period of time and it's getting much better every year.\n\n\nAnyway, I'm looking forward to install KDE 3.1 with (probably, not sure yet) default icons made by Everaldo.. it's a large step in the right direction :)\n"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "I agree with you.  I find it strange that Gart calls KDE ugly but not GNOME which is 10 times worse. This is biggest sign something is not right."
    author: "ac"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "gnome 2 will be the worst shit ever released on this planet. a pile of shit that totally looks inconsistent. i recall that havoc pennigton dude shittalk on /. with which he slapped his own face with.\n\nyou dont belive me ?\n\ninstall gnome 2 beta 4 and start some default applications that comes with it. look at the menues, different heights, look at the menu entries, look at the pixelspaces between arranged buttons etc. they simply nailed their shit on a window and let the use be happy with what they offer them. kde has a clean overviewable ui. i dont know how long it takes to nail out all these issues. the people on gnome say things like 'report a bug' 'supply patches' ... ok but as soon as something gets changed again you can supply patches and report bugs again its a neverending loop. gnome badly failed. they may have a nice new codebase but thats almost all."
    author: "garnome"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "Ugly?\nFrankly it is a matter of taste: once I spent many looking for replacement of the default look, but now I let the defaults as they are.\n\nBut I installed Windows XP recently on my computer (no flame please, it is for games), the default look was UGLY, it had some kind of fisher price look-and-feel, the colors were flashy, UGLY! I was quite relieved to find that you can make it look normal..\n\nOTOH I saw recently a laptop with MacOSX: quite beautifull really.\n\nSo I find KDE's default look quite nice.\n\nFor me the real main problem for KDE/Gnome are:\n- consistency: many different toolkits.. \nCould everyone stop using Tk ?\nThanks. :-) Porting NEdit to Qt would be nice too: I hate Motif.\n\n- fonts: the fonts on Unix are ugly compared to Windows fonts, and I think that even when you use Windows fonts the rendering is not so nice (or maybe I didn't select a good set of fonts).\nFidling with fonts is not very pleasant too: you find a nice looking font at 12pt and after you think it's too small: you increase its size at 14pt--> ugly you have to select another font and 14pt a font is not the same that for another font..\n\n- performances: the desktops under Linux feel heavier than Windows GUI..\n"
    author: "renoX"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "<quote>\nBut I installed Windows XP recently on my computer (no flame please, it is for games), the default look was UGLY, it had some kind of fisher price look-and-feel, the colors were flashy, UGLY! I was quite relieved to find that you can make it look normal..\n</quote>\n\nI agree, but it does look more professional in some way.\n(but I don't like it either)\n\n<quote>\nFor me the real main problem for KDE/Gnome are:\n - consistency: many different toolkits.. \nCould everyone stop using Tk ?\n Thanks. :-) Porting NEdit to Qt would be nice too: I hate Motif.\n</quote>\n\nYeah, that's an even bigger problem, Motif should be moved to /dev/null :)\n\n<quote>\n- performances: the desktops under Linux feel heavier than Windows GUI..\n</quote>\n\nThe reason why the Linux desktops feel so heavy is because X is very, very slow..\n\nLet's hope (X)DirectFB and/or Berlin (both way faster than X) are ready for use next year (and let's hope NVIDIA will finally release those specs for their gpu's).\n\nI'm actually really looking forward to (X)DirectFB ( http://www.directfb.org ), it even allows you to have 3D objects on your desktop (in the background, whatever you want) while staying fast, very fast."
    author: "rve"
  - subject: "Why do you claim that X is slow?"
    date: 2002-04-29
    body: "Are you simply bullshitting, or just repeating some untrue FUD you heard somewhere?"
    author: "Bob"
  - subject: "Re: Why do you claim that X is slow?"
    date: 2002-04-29
    body: "No, I'm not bullshitting.. try moving a window and see for yourself: it's far too slow.\n\nIt's impossible to smoothly move a window in X on any computer (which IS possible in Windows, BeOS, DirectFB and OSX)\n\nAnd it's definitly not bullshit.\n"
    author: "rve"
  - subject: "Re: Why do you claim that X is slow?"
    date: 2002-04-29
    body: "Are you sure you are using an accelerated xserver? Because I have never seen windows move so smoothly in Windows as I can see in X. On the other hand, KDE apps are really slow to redraw for which Qt is mainly to be blamed I guess. \n\nBTW, KDE apps take way too much time to load. This HAS TO BE TAKEN CARE OF! My favourite app is gqview - just look how fast it loads!"
    author: "mac"
  - subject: "Re: Why do you claim that X is slow?"
    date: 2002-04-29
    body: "<quote>\nAre you sure you are using an accelerated xserver? Because I have never seen windows move so smoothly in Windows as I can see in X. On the other hand, KDE apps are really slow to redraw for which Qt is mainly to be blamed I guess.\n</quote>\n\nYes, I'm using a hardware accelerated server (with a GeForce3 Ti 200)\nHowever, I've tried it on many other different systems and it's the same on all of them, no matter how fast the system is.\n\nAnd what you mean with the 'slow KDE apps' is probably what I meant with the slow X server ;)\n(note: I'm not trying to bash XFree, I like it very much, it's just the slowlyness that's irritating me)\n\n<quote>\nBTW, KDE apps take way too much time to load. This HAS TO BE TAKEN CARE OF! My favourite app is gqview - just look how fast it loads!\n</quote>\n\nThat's because the standard GCC linker isn't very good for C++ programs.\nIt should be faster whith GCC 3.1..\n(or just use the free Intel compiler + linker)\n"
    author: "rve"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: "> If the first impression is not good, the user won't use the software and run back to Micro$oft.\n\n\nTo be honest: I prefer the KDE2.2.2 default style to all commercial desktops. Win 9.x/NT looks boring, while WinXP looks too childish (i hate the XP colors).\n\nMacOSX looks beautiful, but the window decoration buttons don't show what they are good for, so novices have to try them for some time until they know how to handle them.\n\nIn contrast, KDE 2.2.x looks clean, nice, professional and is intuitive to use.\n\nWhen I installed a KDE 3.0 release candidate I was really disapointed about the XP style logout and screen lock icons, which just wouldn't fit in the rest of the KDE desktop.\n\nStefan"
    author: "Stefan Heimers"
  - subject: "Re: Ugly default windowtheme/widgetstyle/iconset"
    date: 2002-04-28
    body: ">> If the first impression is not good, the user won't use the software and run back to Micro$oft.\n> To be honest: I prefer the KDE2.2.2 default style to all commercial desktops.\n\nThat's my opinion as well, maybe MacOS being an exception. However KDE's default widget and window manager styles are excellent IMHO. They are not too childish, it's an excellent compromise of good look and usability which a bit \"I'm used to this look\" for Windows users thrown in.\n\nI personally do not know any alternative icon set which is even close to being able to rival KDE's standard icons - and I have checked quite a few.\n\n> When I installed a KDE 3.0 release candidate I was really disapointed about the XP style logout and\n> screen lock icons, which just wouldn't fit in the rest of the KDE desktop.\n\nFull ACK. Both of these icons do not only look somehow strange, they do not even sligtly match the style of the other icons... I wonder what the reasons where to include these...\nThe new sound mixer icons in another candidate - although the old icon admittedly looked a bit ugly the new one is not really better... It looks more blurred like the GNOME icons which - while many of them look really nice by itself - are IMHO not too useful as icons which I prefer to have some contrast and a clear border to the surroundings. KDE's default icons do that quite well...\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Mosfet's response"
    date: 2002-04-28
    body: "Hi!  Let me start off by saying that KDE is great software!  I have a lot of respect for it and admire both its internals and a lot of its visuals.  I'll also make a disclaimer and say that I usually use GNOME.\n\nDecrem basically said that he didn't like the aesthetics of KDE.  He is entitled to that opinion, and no one need share it.  Many people think KDE looks great as is.\n\nMosfet's response isn't very constructive, however.  First, Mosfet made at least one (repeated) cheap shot: he kept pointing out Eazel's demise and the money it spent in the process.  Frankly, Eazel's money-spending habits don't have anything to do with KDE's look.  Eazel is not a valid point when discussing KDE default appearance.  The fact that Nautilus was originally developed by a company (with the help of volunteers) and that Konqueror was developed mostly by volunteers also has nothing to do with KDE's look.\n\nSecondly, Mosfet mentions the beauty of C++ over C in ease of development.  This may be true, but again, what does this have to do with KDE's look?  Your average end-user doesn't give a rat's butt about the programming language in which the desktop environment was written.  Sad to say, but true.\n\nThird, there are many ways to change KDE's default look, as Mosfet pointed out.  Will the average end-user (a) know how to change this look or (b) ever bother to do so?  Many would say that the end-user would not understand how to do this (no matter how easy it is to do so) and that even more never bother to even try.  The problem is that people will take a first look at the desktop to decide whether they want to use it.  They don't stop to think how easily the default appearance can be changed.  Is that criterion stupid for choosing a desktop?  Yes, I think so, but I also think that it is true.\n\nSo, before jumping all over Decrem, it might be a good idea to take a long careful look at KDE's default look and try to decide if it can be improved to be more aesthetically pleasing for the masses.  Maybe it's great as is.  Maybe it isn't.  Think about Decrem's criticism before jumping all over for him.\n\nThat said, KDE is a great desktop.  Keep up the good work."
    author: "aigiskos"
  - subject: "Re: Mosfet's response"
    date: 2002-04-28
    body: "Gart said that KDE was butt-ugly.  That was constructive?  Everything was a carefully calculated slam of KDE to sully a good name.  Mosfet did the right thing by questioning Gart's motives and purposes for attacking KDE.  Look and feel goes way deep in KDE and is core to what KDE 2 and above is all about.  The average user just has to use KPersonalizer the first time, but there is no GNOME look yet.  For 3.1 maybe there will but this will not shut up Gart.\n"
    author: "ac"
  - subject: "Re: Mosfet's response"
    date: 2002-04-28
    body: "You're right: \"butt-ugly\" was certainly not constructive.  He apologized on Slashdot for saying that, by the way.  My question: why would he want to slam (in a carefully calculated way, no less) KDE?  Seriously, what does he have to gain by that?  Hancom Office would integrate much better into KDE than into GNOME, since Hancom Office uses Qt and not GTK+.  He has nothing to gain currently by slamming KDE (or by preferring GNOME's look).  It sounded to me like he was just voicing his opinion, which he should have put more diplomatically (or perhaps just kept to himself entirely).\n\nI'm not sure what you mean that there is no GNOME look yet.  GNOME 1.x already has a look (some people think it's too dark and dreary; some think it's pretty).  GNOME2 will look a lot like GNOME 1.x except that there will be new stock icons, greater use of the menu bar, and lots of UI clean-ups.  Its UI will be by no means perfect, but it will be a \"GNOME look,\" whatever that means. :-)\n\nAt any rate, you're right that Decrem was not being constructive.  On the other hand, that fact does not mean that a constructive discussion cannot result from his statement, right?\n\nCheers!"
    author: "aigiskos"
  - subject: "Re: Mosfet's response"
    date: 2002-05-01
    body: "> I'm not sure what you mean that there is no GNOME look yet.\n\nthere is no GNOME look [option into kpersonalizer] yet.\nFor [KDE]3.1 maybe there will but this will not shut up Gart."
    author: "GermainGarand"
  - subject: "Re: Mosfet's response"
    date: 2002-04-28
    body: "You may want to read the paragraph starting with \"Sound good, but how does this affect end users?\"... And the cost and manhours it takes to develop applications definitely effects users, too.\n "
    author: "ac"
  - subject: "Re: Mosfet's response"
    date: 2002-04-28
    body: "> Decrem basically said that he didn't like the aesthetics of KDE.\n\nHe said it was butt ugly and he didn't like the use of the K among other things. He seemed to offer it a grudging admission that it was pretty good nonetheless. Given his high visibility his comments are fair game for further commentary and he ought to know it as should you. Also Mosfet is right that he should try to make more diplomatic statements. I mean, the letter K? Come on. That's just lame.\n\n> Mosfet's response isn't very constructive, however. First, Mosfet made at least one (repeated) cheap shot: he kept pointing out Eazel's demise and the money it spent in the process. Frankly, Eazel's money-spending habits don't have anything to do with KDE's look.\n\nBut what about the other way around? Mosfet asks a relevent and entertaining question if if fact we are to believe what Decrem said. That is, was Eazel's money spent just because the visual aesthetics of KDE were disliked? If you actually have a historical view the Eazel team went out of their way to find unfavorable comments about KDE that positioned their product well. Look up their Linux Magazine interview. In the end they are forced to admit it's pretty good but not before the requisite digs. I always wander if people are telling the real reason when they behave that way and when they make PR announcements that sound like they are producing the first user friendly interface tools for Linux. I personally suspect part of the decision was that KDE was pretty far along with konqueror and they could come in and be heros with GNOME replacing GMC. I mean let's face it, the opportunities given the current state of software were not the same. \n\n> Eazel is not a valid point when discussing KDE default appearance.\n\nNo but it is when discussing Bart Decrem's opinions because it looks like he's still walking around with some residual unresolved attitudes... still in the \"what can we say bad about KDE\" Eazel mode.\n\n> Secondly, Mosfet mentions the beauty of C++ over C in ease of development. This may be true, but again, what does this have to do with KDE's look?\n\nOnce again perhaps you should go read up on what you are commenting on. Mosfet explained how it worked regarding impacting graphics but you must have skimmed over that part. Much of KDE's smooth and fast gradient processing and more is directly related to C++ and visuals. Further more if you have seen his Liquid scheme and read about it you would know that he replaces what is typically bitmaps in most theming engines and replaces it on the fly with efficient code he wrote in C++ that results in more even appearance and far better scalability for different reslutions.\n\n> Your average end-user doesn't give a rat's butt about the programming language in which the desktop environment was written. Sad to say, but true.\n\nWell are you average? By and large you may be right about computer users but at least proportionally Linux users seem to be more technically oreinted. Remember they keep telling us that it's not a mainstream desktop. Besides I know what Mosfet has done for open source (as compared to say... you) so even if I knew nothing about it I'd at least give it a read with some interest. I'd also have not missed the last question as you did. ;)\n\n> Third, there are many ways to change KDE's default look, as Mosfet pointed out. Will the average end-user (a) know how to change this look or (b) ever bother to do so?\n\nWell gee, I wonder? If you actually bothered to fire up KDE for the first time (and your distro left it in) you would see kpersonalizer which would allow you to set your eye candy level out of the gate. Also anyone who opens kcontrol and looks at look and feel would. So the answer is those who are curious and those who use it the first time. Besides I find it offensive someone would suggest we make a bunch of decisions for people because they lack initiative. If you were running an older system you would not be happy if KDE automatically set up all the eye candy it could and your system crawled. KDE is done visually to make it productive for new users. I think they did a good job. An example of a bad job is XMMS which opens by default for me in black with a bitmap scheme that is so small I can't read it on my 19 inch monitor with 1280x1024. I figure out how to double size it and it's hideously pixelated and still near impossible to figure out what's where. No thanks!\n\n>  So, before jumping all over Decrem, it might be a good idea to take a long careful look at KDE's default look and try to decide if it can be improved to be more aesthetically pleasing for the masses. Maybe it's great as is. Maybe it isn't.\n\nAt the risk of sounding rude that is a bit ludicrous to say as there have been a number of people working on that look (which I think is beautiful and functional) for years now. That has to beat your considerations by at least a couple man hours. There's even a web site, mailing list and group of people focused just on KDE's looks. Given the critisism of KDE for supposedly looking like other software I think it further illustrates that KDE is willing to utilize good ideas from other designers. In fact is is when you begin to use KDE and reazlize that it is highly efficient and pliable that you begin to see the real beauty\n\n> Think about Decrem's criticism before jumping all over for him.\n\nI think people have. It was pretty crass. It sounded to me like a dumb excuse to say he doesn't use it because he doesn't like the look since he shoudl know how to use it. I could understand his bias based on his history but it sounds like the same old thing to me... looking for some lame excuse to say it's not quite there. In the first place beauty is subjective so it's an opinion to try to avoid anyway because there is no persuading people or arguable better or worse. At least with KDE they have focused on what can be measured and what should be focused on. That is usability, even though I think it's very attractive besides."
    author: "Eric Laffoon"
  - subject: "Re: Mosfet's response"
    date: 2002-04-28
    body: "This opinion is brought to you by the letter K and the number 3.0.0\n\nI read both opinions.  Then, I read everyone's opinions here.  Bart was being a jerk, and Mosfet replied with a VERY mature and reasoned response.  3 cheers for Mosfet.  Next time you're in Atlanta, I'll buy you a beer.\n\nThe technical merits have been covered quite well by people more technically adept than myself.  However, I want to look at Bart's conclusion that he didn't like the letter K.  I think the art on kde-look.org proves you can do some amazing things with the 11th letter of the alphabet.  <troll> Besides, this seems more eye-catching than the muddy footprint of a gnome. </troll>\n\nThe letter K is no sillier than the infamous red-hat or my personal hero, Geeko (SuSE).  Mandrake, Debian, and every other project in the OS that got off the ground and into code has a mascot.  Its half the fun and joy of this OS.  Heck, is K any sillier than E?  \n\nIn conclusion, I think it is out of place for someone who writes a QT app, who runs an OS that looks like a Penguin to be that concerned with our use of the 11th letter of the alphabet.\n\nLong live the K.\n\n--dru\n\nPS, the default looks great.  If you don't like it, switch to liquid with the conectiva icons....it looks much prettier than most everything."
    author: "Droobiedoobiedoo"
  - subject: "Re: Mosfet's response"
    date: 2002-04-29
    body: "Unfortunately I think that aigiskos's views seem to be in the minority, but I for one tend to agree with him. Normally I don't post to these types of discussions because it tends to encourage hotheads. However, I would like to say a few things:\n\nIt doesn't matter who says what about KDE, the mature people will always listen to the criticisms made and ask if they have a point or not. This separates the bigots from the people who seriously want a good desktop.\n\nI personally like kde to develop with, but prefer the look of gnome for reasons I have never been able to work out; my wife who is totally computer illiterate agrees.\n\nThe way to go is not to emulate windows features but to try and find the best way to work and use the applications. Too many desktops (gnome included) try to copy windows features, ignoring the fact that it may not be the best way.\n\nI am a professional software developer, who has also desktop, UNIX and networking support experience, so I am not what you would class a \"user\"; but I never forget that one of the aims of desktops like KDE is to spread the use of Linux to ordinary users (like my wife) who think windows 95 is hard and would panic at seeing the Kpersonaliser (misspelt on purpose).\n\nIt would be nice to get away from the politics of gnome; I for one call Linux Linux and not GNU/Linux. If we were to give credit for all the diverse parts of Linux we would say GNU/Xfree86/KDE/Gnome/QT/.../Linux.\n\nBasically to summarise:\n\nRemove the politics, listen to people (however abrupt they may be) for possible criticisms and even though you may be a super brain, don't forget the users."
    author: "John"
  - subject: "KDE proves itself for newbies"
    date: 2002-04-28
    body: " I tried this experiment in my college, where I have installed a linux server in the lab (fresh install of Mandrake 8.1 standard with samba ). The people who use the computers in the lab, mostly use it as a sort of cybercafe, to casually browse the net in their spare time, apart from practicals (which are done using turboC on windows). These people mostly have never heard of linux. I left the machine on running various desktops (Gnome, KDE and Enlightenment) and observed the guys and gals who would visit to browse. \n\n The people who accessed the machine with KDE on it immediately started exploring around. If by chance they managed to start a konqueror window, or it was running, they would immediately start using the browser, or they would take quite some time to realize that the machine was not running a version of Windows. On other desktops, the immediate reaction usually was \"What the heck is this?\" or \"how do I get out of this?\". I would have to explain that they could start galeon or netscape to browse, but still they would remain confused.\n\n I am using KDE3.0 at home, while the one in college is 2.2.1. 3.0 is great to use, but even 2.2.1 proved intuitive enough for newbies (esp. those who thought computers would run only with windows on it). I think this speaks a lot of the userfriendliness of kde.\n "
    author: "Rithvik"
  - subject: "Re: KDE proves itself for newbies"
    date: 2002-04-28
    body: "Hi!\n\nAlthough I really like KDE and it's the only desktop environment I currently use I can't totally agree...\n\n> I think this speaks a lot of the userfriendliness of kde.\n\nIt merely shows that KDE is the most Windows-like environment you tested. Whether this really is a good thing remains to be discussed. <g>\n\nI think if you repeat this experiment with people having another computer-related background you may get different results...\n\nGreetinx,\n\n  Gunter Ohrner"
    author: "Gunter Ohrner"
  - subject: "Re: KDE proves itself for newbies"
    date: 2002-04-28
    body: "True, but as far as I know, most newbies who have used a computer have used Windows. That the 'default' look of Kde is similar to Windows, IMHO, is a plus point.\n\nFor a true newbie, like some of my relatives, even OS X may prove confusing. Most of them try of memorize ways to do the simplest things and get stuck (Example \"Go to the toolbar and click the mail icon to send the mail.\" \"What do I do, there is no mail icon on the toolbar??\"). For a new person to learn, he must find it intuitive enough to go about doing whatever he feels and get some result, so he learns through experience and logic. I have seen people delve into kde, change wallpapers, check out the games, and so on. I have not seen them do the same on others. "
    author: "Rithvik"
  - subject: "What we need to do..."
    date: 2002-04-28
    body: "...Is to identify the biggest things people complain when it comes to KDE (some of the things I mention affect all desktops, not just KDE). I think this list covers the worst (not in any particular order):\n\n1. Performance\n2. Icons\n3. Fonts\n4. Generic eye-candy\n\nThen we need to figure out how to handle those problems:\n\n1. Performance: KDE 3 is a step in to right direction (so I have heard, I still use 2.2.2). And I think the new linker in GCC 3.1 will significantly improve responsiveness of KDE. So we are in a right track here. Of course we should still explore ways to improve performance.\n\n2. Icons: There are alo of nice icons available for KDE (and other desktops), so the question really is to have nice DEFAULT icons. Everaldos Crystal-icons would do the trick just fine :)\n\n3. Fonts: this is the big one. Unfortunately there's very little KDE-folks can do about this, this is up to the xfree-folks. AA helps alot, but the rendering could be improved. I hope xfree 4.3 fixes the hinting, that should improve quality nicely. But we also need nice fonts. You can use the MS-fonts (I use them. One of the few good things MS has ever done), but we could use some fonts of our own, instead of having to live on the mercy of MS when it comes to fonts.\n\n4. Generic eye-candy. This covers such things as window-decorations, transparency, color-schemes and the like. Again, we are on a right track. Keramik window-decoration for example looks really good, and I'm looking forward of seeing it in KDE :). The default color-scheme is nice and easy on the eyes. And transparency is nice, but it could be improved (like having REAL transparency where stuff behind the window get's updated. But that's up to the xfree-folks)\n\nAll in all, I think things are moving along quite nicely."
    author: "Janne"
  - subject: "Re: What we need to do..."
    date: 2002-04-28
    body: "> There are alo of nice icons available for KDE (and other desktops), so the question really is to\n> have nice DEFAULT icons. Everaldos Crystal-icons would do the trick just fine :)\n\nWhich I don't like as much as the default icons which IMHO have a much cleaner look. I think you cannot make everybody happy by law... ;)\n\n> But we also need nice fonts.\n\nI personally think the GhostScript Type1 fonts which I'm using have quite a good quality. However maybe a font-section on kde-look.org with nice PS Type1-Fonts would be cool.\n\nGreetinx,\n\n  Gunter Ohrner\n"
    author: "Gunter Ohrner"
  - subject: "Re: What we need to do..."
    date: 2002-04-28
    body: "I have always been very fond of KDE's default icons -- they really\nare clear, crisp, and please my eyes. Not only mine: I remember\ninstalling KDE on a friends laptop. He took it home with him. The \nnext day he told me his girlfriend had asked him to install those\nbeautiful icons on her windows computer :-)."
    author: "Boudewijn Rempt"
  - subject: "Re: What we need to do..."
    date: 2002-04-28
    body: "\"Which I don't like as much as the default icons which IMHO have a much cleaner look. I think you cannot make everybody happy by law... ;)\"\n\nTrue. I myself have no problems with the default KDE-icons. But the icons seem to be one of the thing people most often complain when it comes to KDE. I use Crystal-icons myself. Mayne Kpersonaliser should present the user with different icons he could choose from? I don't remember that does it do that already, it's been a while since I ran Kpersonaliser myself :)."
    author: "Janne"
  - subject: "Re: What we need to do..."
    date: 2002-04-30
    body: "> But the icons seem to be one of the thing people most often complain when it comes to KDE. \n\nIt's always the same problem when you're happy with something, usually you're quite quiet so only unhappy people will be seen..\n\nMaybe it could be solved by a poll or something like this."
    author: "renoX"
  - subject: "Don't worry be happy..."
    date: 2002-04-28
    body: "Lycoris? A MS WindowsXP look & feel clone. This is good, they are going to sell their product.\nCrystal icon theme? A MacOSX & MS WindowsXP mixed look & feel clone. (90 % popularity on kde-look). This is also good. We can get new users from windows world.\niKons theme? MS WindowsXP look & feel clone. (90 % popularity on kde-look.org). Very, very good. We can attract new users from windows world.\nLiquid? An MacOSX Aqua clone. (90 % popularity on kde-look.org). Good, we can maybe attract users from MacOSX world.\nFuture KDE look & feel? A mix of crystal, ikons & liquid. A mix of WinXP and MacOSX. We take the best from both of these environments. We can be the most popular desktop in the world.\n\nI feel I am going to switch to Gnome 2."
    author: "antialias"
  - subject: "Hehe..."
    date: 2002-04-28
    body: "I have to clarify above statements :) Of course I am not going to switch to Gnome, I just want to point that Mr. Bart knows that KDE is highly customizable. If he doesn't like the K (actually Go\nbutton) he can change it. He can either hire an artist to do that for him or change it and replace it with Gnome foot. So, saying that KDE is ugly is contradictio in adjecto. If you are a tasteless person you will have an ugly desktop.\n\nDefault icon set in KDE is not made to please some adolescents. It is made with respect to all aspects of good (G)UI: clear, non-disturbing, consistent & effective. And the only icon-set we have for KDE that takes care about these elements is our default icon-set. That is the only icon set where I can easily recognize what is what, and which icon represents which application, and where mimetype icons don't look as if they are all the same and where you don't need to guess what is what."
    author: "antialias"
  - subject: "Re: Don't worry be happy..."
    date: 2002-04-28
    body: "> I feel I am going to switch to Gnome 2.\n\nand what do you think you get with this ? gnome 2 offers nothing, no browser like konqueror, no filemanager like konqueror, no mailer like kmail nothing. you dont get consistency in the menues etc. basically all you get with gnome 2 is anger, frustration, pissed off feelings, half working applications."
    author: "desperado"
  - subject: "Re: Don't worry be happy..."
    date: 2004-03-14
    body: "WHAT!!!!!\n\nI feel inclined to argue. GNOME I believe is much better than KDE. It is much less clunky and looks better due to GTK.\n\nInconsitency? No file browser?\n\nThe first point is definately wrong, and the file browser is nautilus. It integrates quite well with Konqeror if you need it to.\n\nThe themes are much better in GNOME besides.\n\nGNOME 2 is much better than KDE.\n\nSurse there are things in KDE which GNOME doesn't have that I wish it did, but overall, I think GNOME is best."
    author: "NerdNetwork"
  - subject: "Re: Don't worry be happy..."
    date: 2004-03-14
    body: "Good job, you replied to a post two years ago. And Gnome still sucks ass.\n\nPlastik is a better theme than any gtk theme.  "
    author: "anon"
  - subject: "Re: Don't worry be happy..."
    date: 2004-03-14
    body: "I don't think so, and I hadn't noted the dated, I do apologise for the inconvenience.\n\nHowever, GNOME's GTK I believe is a better theming engine than whatever KDE uses. I believe GNOME integrates with the network much better, and generally, acts better. Although, I do respect your opinion.\n\nWhat version do you have (of GNOME I mean), and which distribution?\n\nI have Fedora Core 1, with GNOME 2.4.\n\nOf course, we are all subject to our opinions ;)"
    author: "NerdNetwork"
  - subject: "Keramik"
    date: 2002-04-28
    body: "Now, I don't want to offend anyone here, but I think making the kermaik+crystal look default in KDE 3.1 is a horrible idea.\nDistracting themes and icons is not IMHO, a good thing, they should be kept simple and efficient.\n\nHaving too many styles, icon themes and window decorations available in kcontrol is not a good idea either, as it's only confusing for the user.\nIn kcontrol, Look & Feel section, provide a link to kde-look.org instead.\n\nA choice of 3 widget styles, 3 window decorations and perhaps 2 or 3 icon themes as well as 3 'metathemes' in the kcontrol theme manager consisting of the elements of the above mentioned styles, decorations and icons is IMHO, the way to go.\n\nAs a default style, something like a combination of Light Style 2nd and 3rd edition, and .NET style would be perfect.\nMore specifically, the tabs, sliders and scrollbars from Light Style 2nd edition, the menus from .NET style, and the rest from Light Style 3rd edition.\n\nThe default icon theme, should be something elegant, yet simple and illustrate an obvious function, something like the icons in QNX RTOS.\n\nKermaik and crystal should, IMHO, be one of the 2 remaining alternatives for the user, as well as the iKons and/or Slick icons themes ( as it is now).\nDon't get me wrong, Keramik and Crystal are very nice and original, but I think a simple (but elegant) look allows one to be more productive.\n\nI also think more resources should be donated to making good interface guidlines, like the ones Apple have published on their site, detailed guidelines for icons, widget placement in applications and so on, for better consistency between KDE applications.\n\nI don't want to sound ungrateful or arrogant, in that case I'm sorry.\nI'm also no expert in GUI design, infact, I don't know shit about GUI design, it's just that an interface like in QNX or BeOS feels more productive, and I think that is what we should aim for.\n\nI have very little spare time on my hands, but I'm willing to donate most of that time to work on an icon theme, unless there's a complete lack of interest for yet another icon theme.\n\nI just want to give a big THANKS! to all the KDE developers and everyone else involved in the KDE project, I've been jumping back and forth between GNOME, KDE and WindowMaker/GNUstep, and I've finally decided to stay with KDE, since it IMHO feels far more professional."
    author: "Magnus"
  - subject: "Re: Keramik"
    date: 2002-04-28
    body: "This is embarrasing, what I wrote was imcomplete.\nTo clarify; What I mean about interface guidlines, I ment MORE detailed guidelines, like the ones from Apple, I'm aware of the present guidlines, wich are great!, but Apple has published VERY detailed interface guidlines for OS X, like measurements between widgets in pixels, wich gives you a whole lot of consistency.\n\nSorry about that."
    author: "Magnus"
  - subject: "Re: Keramik"
    date: 2002-04-29
    body: "> Distracting themes and icons is not IMHO, a good thing, they should be kept simple and efficient.\n\nI completely agree. That's why I hate keramik and liquid.I think there should really be no default, after all, that's what kpersonalizer is for. "
    author: "unanimity"
  - subject: "Re: Keramik"
    date: 2004-02-14
    body: "I don't mean t say that you are not entitled to your own opinion but in this case I believe you missed the concept. Having multiple options for look and feel are the plus side to Linux. Windows users strive to get a truely unique look out of their not so unique Operating System. What you ask for is to limit Linux users to a few styles alone and that would mean leaning in the \"Windows\" direction. With that thought in mind I have to ask you this....who should be the one to decide which styles should be added to the default KDE collection and which should be left out? Maybe you? Maybe Bill Gates? No, variety and choice is good, it is conformity and limitations that are the bad idea here. Linux is great and getting better daily thanks to the \"open\" attitude whic dictates that the end user CAN have it his or her own way. Oh yeah, one last thing before I end this rebuttle.... the \"kermaik+crystal look\" (note keramik is mispelled) you dislike so much is very well liked by the majority of the KDE users. Don't believe me? Just go on to any Linux theme site read the posts under the keramik based themes."
    author: "tekjunky"
  - subject: "A quick question"
    date: 2002-04-28
    body: "Forgive me if this has already been asked, but is there any way to have konqueror the web browser and konqueror the file manager with completely different toolbar layouts?\n\nSpecifically, I want to get rid of those zoom icons(ugh! IMHO), also I don't see the need for an 'up' button whilst web browsing\n\nI also have to agree with the general consensus that the default theme/icons aren't very nice ... ugly is a bit strong but they are not particularly eyecatching\n\n"
    author: "Gogs"
  - subject: "Re: A quick question"
    date: 2002-04-28
    body: "Yes!\nConfigure the toolbars, and save the settings with; Window -> Save View Profile\n"
    author: "Magnus"
  - subject: "Re: A quick question"
    date: 2002-04-28
    body: "Yes I've tried that, but that doesn't seem to save the actual toolbar icons...in other words once you change the toolbar it applies to all profiles"
    author: "Gogs"
  - subject: "Re: A quick question"
    date: 2002-04-28
    body: "Hmm yes..\nI just noticed, sorry :("
    author: "Magnus"
  - subject: "I enjoy KDES flexibilty of eye candy."
    date: 2002-04-28
    body: "I think that KDEs edge is the fact that you can have any look you like (icons, colors, window decorations and whole lot more). Windoze hasn't changed much since Win95 and Gnome has an ugly grey look >_<. I really like the eyecandy and I love kde-look. I visit it almost every day! Im always changing my desktop and it really rocks! Well done to all the hard workers who make the cool themes."
    author: "Norman"
  - subject: "ease of install"
    date: 2002-04-28
    body: "Having read most of the above comments, I feel something is missing from the discussion:\nAlthough Linux currently is being tried mostly be technically oriented users, these  are not necessarily IT-people. Installing a new theme by compiling like Mosfet's Liquid, is not what a user expects or wants.\n\nThe easiest way I think is how Mozilla does it, go to the View-menu, choose \"Apply theme\"/\"Get new theme\" and off you go to a theme web-site where you can install a new theme in the most easiest way imaginable.\n\nIf KDE really wants to make it easy for its users to change the default theme or icons, one should have to go to the Control Center, click on \"Look and Feel\" / \"Change\" and a browser would pop up with a link http://www.kde-look.org/(...blablabla...). Each theme then having an option \"Install theme\". Now that's ease of use.\n\nUsers really should't be bothered with compiling themes. Do you really expect your mother to *compile* a theme?\n\n(I know this doesn't have to be done with icon-themes, but most KDE 3-themes require me to do so. All current KDE 3-themes *by default* fail to install with SuSE 8.0.)\n\ncheers,\n\nPatrick"
    author: "Patrick Smits"
  - subject: "Re: ease of install"
    date: 2002-04-30
    body: "What about the installation of the apps and for that matter kde?.it's defly not easy to downlaod and install and upgrade kde.whew!, to install kde 3.0 you have to remove kde 2.2?.sorry, but thats tough for papa:), who likes the next,next,finished interface.\nsaw a thread on kde-devel discussing this topic.anything came outta that??.\ncaio,\n-c"
    author: "corleone"
  - subject: "Re: ease of install"
    date: 2002-05-09
    body: "Let's take this argument one step further!\n\nIf people want Linux to be adopted widely, then compiling has to be abolished completely! Not just with KDE Themes.\n\nI realize this is a KDE site and it's not their responsibility, but I just had to reply."
    author: "Rocky1138"
  - subject: "Re: ease of install"
    date: 2002-06-01
    body: "I agree with this guy 100%. Im new to this Linux thing and and the only way we are going to get more people to switch from SOFTMICRO is for Linux to become as easy to use as them.\nTheyve got the easy install down to the most part now work on abolishing any compiling for the newbies."
    author: "Greg Parsons"
  - subject: "Re: ease of install"
    date: 2002-05-31
    body: "Good points. Even though I'm a computer programmer for the past 12 years (admittedly, mainly on windows), I have a tough time \"installing\" anything from applications to themes.\n\nIf Linux is to pose a serious threat to Windows, and I hope it does, it has to come up with an installshield like application to make these tasks as easy as they are on Windows or MacOS. Certainly, the fragmentation between distros is not helping but the UnitedLinux consortium (which DOES NOT include RedHat) gives one hope."
    author: "Anjan"
  - subject: "Re: ease of install"
    date: 2002-10-16
    body: "Check out gentoo linux... haven't had any problems with it.  Real easy to use.\n\nhttp://www.gentoo.org"
    author: "bob"
  - subject: "Re: ease of install"
    date: 2002-10-16
    body: "Hi all. I just have to reply to this...\n\nFirst off... LINUX is NOT WINDOZ!!!\nLinux was NEVER intended to be a wintendo replacement. Linux is a UTILITY OS not a freakin playstation.\nSecondly, if you start to break up all the tools of Linux, such as the compilers, you will then have to find a way for things to work/talk to the kernel. In doing this, you will end up with a microsloth system.\nThirdly, cuz yes it is a word today, the reason LINUX is so hard for most people is because it is a complete open core OS. It's meant to be tweaked by experienced or semi-experienced ppl.\nYour everyday dumb dumb wintendo player \"all i know how to do is check e-mail and play mp3 music cuz im to freakin lazy to try to read or learn something new\" should not use linux.\n\nLINUX IS NOT WINBLOZ!!!! get that thru your head. Stop comparing apples to oranges.\n\nL8rz.\n\n<--LINUZ: More configurable than a Mr. Potato Head!!!-->"
    author: "LiquidFlex"
  - subject: "Re: ease of install"
    date: 2002-10-20
    body: "Right, Linux /isn't/ meant for everyone.  Neither is Windows.  Or MacOS for that  matter.  People who don't have time, patience, or interest enough to delve into the intricate matters of linux (or darwin, or bsd, or anything like this) use windows because it's something they're comfortable with and doesn't make them have to go out and buy a $50 o'reily book (btw, i love o'reily ;).  A lazy user is a user who wants to use a computer to its fullest (usually involving *nix), but doesn't want to do any of the work towards it (i.e. RingTFM, typing 'make', or things spelled out for you by the developer).  So if a user doesn't want to learn linux, that doesn't neccessarily make them lazy, probably just uninterested.\n\nAlso, if you do want to push linux so much, you have to make it more than a \"utility os\".  I think id and epic are on the right track by making their games so versatile.  If you /don't/ want other people than you to use linux, then that's being selfish.\n\nP.S. It's hard to take an opinion full of \"winbloz\" and \"microsloth\" seriously, it makes you sound like a script kiddie."
    author: "kaworu-sama"
  - subject: "Re: ease of install"
    date: 2002-11-22
    body: "The problem though is with this statement..\n\n \"Your everyday dumb dumb wintendo player \"all i know how to do is check e-mail and play mp3 music cuz im to freakin lazy to try to read or learn something new\" should not use linux.\"\n\nThe reason why I say this is a problem is because I'm not to lazy to read and to learn.. the problem is the lack of documentation. It's unbelievable. I can understand little documentation for cutting edge open source stuff coming out but where is the documentation on how to install the mosfet-liquid theme? I went though and read on how to do the whole tar, ./configure and then make install but then when I'm still stuck. The last little info after make install says to add some XX to your path which I did in my etc/profile, but then what?  How do I get this to work the SuSE 8.1 distro? The theme manager says to add a theme you can browser for the .theme of what was created. Well I did a find and I only found .themes for the current ones I can select from nothing related to mosfet or liquid. So now what? Point being why isn't there more on all the main theme pages about how to get themes working for your linux distro? The directions at kde-look.org are incredibly weak. Maybe I'm an idiot but I don't think it's too much to ask to have some more \"how to's\" around and yes I've searched all over Google. \n\nFor another topic I can go off on how incredibly painful it is to get any of my other browsers to run java applets (besides the default Konqueror).\n\nI do Java programming for a living and I'm not a lazy idiot, and I'm really working hard to learn this. The problem is even the books available are not that great. The only way I'm really getting any answers is from those helping me on mailing lists. What I'd love to do is be directed to some RTFM pages, but those seem few and far between as I scour the net. I don't mind working with config files and doing stuff from the command line, but it would be nice to have more instructions around for specifics once you get past the whole untarring, /confige, make install process.\n \n"
    author: "Rick"
  - subject: "Re: ease of install"
    date: 2002-11-22
    body: "The problem though is with this statement..\n\n \"Your everyday dumb dumb wintendo player \"all i know how to do is check e-mail and play mp3 music cuz im to freakin lazy to try to read or learn something new\" should not use linux.\"\n\nThe reason why I say this is a problem is because I'm not to lazy to read and to learn.. the problem is the lack of documentation. It's unbelievable. I can understand little documentation for cutting edge open source stuff coming out but where is the documentation on how to install the mosfet-liquid theme? I went though and read on how to do the whole tar, ./configure and then make install but then when I'm still stuck. The last little info after make install says to add some XX to your path which I did in my etc/profile, but then what?  How do I get this to work the SuSE 8.1 distro? The theme manager says to add a theme you can browser for the .theme of what was created. Well I did a find and I only found .themes for the current ones I can select from nothing related to mosfet or liquid. So now what? Point being why isn't there more on all the main theme pages about how to get themes working for your linux distro? The directions at kde-look.org are incredibly weak. Maybe I'm an idiot but I don't think it's too much to ask to have some more \"how to's\" around and yes I've searched all over Google. \n\nFor another topic I can go off on how incredibly painful it is to get any of my other browsers to run java applets (besides the default Konqueror).\n\nI do Java programming for a living and I'm not a lazy idiot, and I'm really working hard to learn this. The problem is even the books available are not that great. The only way I'm really getting any answers is from those helping me on mailing lists. What I'd love to do is be directed to some RTFM pages, but those seem few and far between as I scour the net. I don't mind working with config files and doing stuff from the command line, but it would be nice to have more instructions around for specifics once you get past the whole untarring, /confige, make install process.\n \n"
    author: "Rick"
  - subject: "Re: ease of install"
    date: 2002-11-22
    body: "sorry for the double post:) I wanted to make sure the X notify by email button was checked and I think I missed it the first time. Maybe I am just an idiot after all:)"
    author: "Rick"
  - subject: "KDE1"
    date: 2002-04-29
    body: "Not only is KDE interface opinion functional AND estethically pleasant, it has been so from the very start,\nimho.\n\nI very well remember when i switched to SuSE (from slackware) and the reason: I wanted to chek this new thing called KDE. And i remember very well the first impression I got: it was quite simply the best looking desktop I had ever seen. I was a simple computer user back then (yes, the mythical average user, not too advanced, neither completely clueless) but I could tell a desktop environment from a simple GUI (like fvwm) and THAT, however primitive, was a desktop, no doubt about it. It would take GNOME a lot of time to have the functionality KDE 1 already  had way  back then. But more important than my own good impression was that of a friend of mine to whom i immediately showed the new shiny toy. He was then a very cynical hard nose sysadmin, the one that would never appreciate a piece of software if it were not really functional and useful for some specific purpose. His comment when he saw KDE was: \"Finally something  professional on Linux!\". THis guy now is a webmaster at the language lab, and he designs the system and the web pages  that allow the teachers to teach lanaguage courses on line --  believe me, the guy really can tell a good interface from a mediocre one. We spent a whole afternoon dissecting KDE, loooking into the libraries, studying the overall structure, we even looked  at the source code of the page of kde.org, and his impresion was thoroughly positive: \"these people know they trade\".\nIf I look at screenshots of KDE 1, i still think that my first impression was correct: that desktop was really magnifecnt: the kwin look was sober and original at the same time (i particularly liked the \"teeth\" on the top of the windows) and the file manager and panel were so intuitive anybody could  use them  -- i started to explore my linux file system beacuse of kfm.\nA few days later, I erased my window partition and haven't used windows  (at home) since then.\n I would like to know who designed the kwin look of KDE 1 and the original kde.org website and say them \"thanks\" -- my friend was right: those people really knew their trade.\n\nFederico\n"
    author: "Federico Damonte"
  - subject: "Tabbed Browsing ?"
    date: 2002-04-29
    body: "\nAm I the only one who noticed that the KDE 3.1 screenshot for Konqueror looked like it had tabbed browsing?\n\nI've been using Mozilla for a week now (my first time ever) and the tabbed browsing feature has me hooked. With the MMB opening up a new Tab, and with background loading, it's totally superb.  It'll be great when Konqueror gets this.\n\n"
    author: "John"
  - subject: "Re: Tabbed Browsing ?"
    date: 2002-04-29
    body: "Yes, KDE 3.1 will have tabbed browsing. The patch came too late to get into KDE 3.0."
    author: "Jon"
  - subject: "Re: Tabbed Browsing ?"
    date: 2002-04-29
    body: "If you get one of the daily snapshots of download from CVS you'll find tabbed browsing is a really nice feature.  I particularly like the fact that tabs are saved in the profile.  This way when stating konq you have a tab for file managing and a tab already set to start web browsing.  It suits my style and saves lots of screen real estate.\n\n\nregards"
    author: "Wheely"
  - subject: "Konqueror icon"
    date: 2002-04-29
    body: "I think the konqueror icon looks like an eggyoke. Can't someone change this image into more appealing? Also I think the K gears look a bit outdated. Perhaps there should be a contest."
    author: "Pieter Philipse"
  - subject: "Novice users don't use a default KDE look."
    date: 2002-04-29
    body: "Is KDE ugly? Wel, that is totaly personal.\n\nShould KDE change the default look & feel to make it more appealing for novice users?\nNope, it is a waste of time.\n\nWhy?\nBecause novice users usually don't see the default look of KDE, they see the default look of their distro. KDE out of the box on SuSE has a different look&feel compared to Mandrake, etc.. \nUsers who compile KDE theirself will see the default KDE-look, but users with that much experience usually change their desktop in whatever they like, dispite how good it looks when they installed it.\n\n\nRinse\n"
    author: "rinse"
  - subject: "Re: Novice users don't use a default KDE look."
    date: 2002-04-29
    body: "<quote>\nWhy?\n Because novice users usually don't see the default look of KDE, they see the default look of their distro. KDE out of the box on SuSE has a different look&feel compared to Mandrake, etc..\n</quote>\n\nThose distributions have indeed a somewhat modified look, but IMHO it's all based on the same default look.."
    author: "rve"
  - subject: "Re: Novice users don't use a default KDE look."
    date: 2002-04-29
    body: "Well, by default, Mandrake ships with its own set of icons, which IMHO look quite a\nbit worse then the KDE ones. This is probably mostly a matter of colorscheme, since a mix\nof light-blue, purple, and yellow isn't going to produce too many pieces of beauty....\n\n\n"
    author: "Sad Eagle"
  - subject: "Re: Novice users don't use a default KDE look."
    date: 2002-04-29
    body: "I totally agree that the Mandrake iconset isn't nice either, but it's not all blue/purple/yellow.. there are many default icons too (folder and mime-type icons for example)\n"
    author: "rve"
  - subject: "Re: Novice users don't use a default KDE look."
    date: 2002-04-30
    body: "Yep, but this means that KDE has no influence on what the desktop on distro's looks like. If KDE uses Liquid and Crystall as the default, and Mandrake/SuSe don't like this, they would still change it into something that appeals to them. \n\n\nI agree that the Mandrake icons are quit ugly ;)\n\nRinse"
    author: "rinse"
  - subject: "Theme or Style?"
    date: 2002-04-29
    body: "Can anyone tell me, what's the difference between a Theme and a Style in KDE?\nI've noticed that the Themes which were available in KDE1.1.1 are still avaulable in KDE2/3. What's the preferred method now, styles?\nWhy do styles not exists as a complete theme?"
    author: "Ez"
  - subject: "Re: Theme or Style?"
    date: 2002-04-29
    body: "Well, Style = a coded plugin that defines how widgets look. You need one of these to see anything interesting on your screen ;-). \n\nAs for \"Theme\", there are really 2 things -- one is \"pixmap themes\", which \nare a bunch of pixmaps describing how widgets look, and a .themerc listing them. \nThese are actually used through a single style plugin -- KThemeStyle -- but appear in the widget style menu as distinct options.  From the user's point of view, they are quite similar to styles, exacept you don't need to compile them, and they're not as flexible -- they don't adjust with color changes, have limited abilities for shaped widgets, etc. On the other hand, no coding is  required to create one.. \n\nThe second use of \"theme\" is for the theme manager themes. These date back to KDE1, and are basically a collection of icons (possibly) wallpaper, color schemes, etc. There was some discussion on KDE development lists to update the theme manager for 3.1...\n\n"
    author: "Sad Eagle"
  - subject: "Light theme third revision"
    date: 2002-04-29
    body: "\nHi SadEagle, this is a bit off topic, but do you plan to add menu translucency to you Light theme second & third revision?"
    author: "antialias"
  - subject: "Re: Light theme third revision"
    date: 2002-05-01
    body: "First of all, I LOVE Light Style 2nd and 3rd revision. However, I've noticed some performance issues with em both, it's more severe in 2nd (try maximising a window, and see the artifacts).\nAre there any plans for a 4th revision?\nAnd yeah, menu effects would be wonderful!"
    author: "Magnus"
  - subject: "Re: Light theme third revision"
    date: 2002-05-03
    body: "Not me nearly certainly  -- I don't maintain Light*; I merely commited\na few minor bugfixes to it... No idea on what other people may be planning.."
    author: "Sad Eagle"
  - subject: "The Default Look"
    date: 2002-04-29
    body: "The default look of various desktops:\n\nWin95/98/Me/NT/2K: butt ugly utilitarian.\nWinXP: crayola eye-candy\nMac OS 9: halfway decent utilitarian\nMac OSX: gorgeous eye-candy\nGnome: halfway decent utilitarian (but very customizable)\nKDE: very decent utilitarian (and very customizable)\n\nDefault desktops should be utilitarian, because they offend no one. I know several people who have chosen not to upgrade to WinXP or OSX precisely because of the eyecandy. They don't take it seriously. But at the same time, the desktop shouldn't be ugly.\n\nKDE wins the prize for the best default desktop in the aesthetics dpartment. If it still isn't good enough for your tastes, you're not stuck with it, because it's extremely customizable.\n\nOn the other hand, if you don't like the look of default WinXP or OSX, there's not much you can do about it.\n\np.s. I'm not ragging on Gnome. It's *default* desktop is very plain however. "
    author: "David Johnson"
  - subject: "Default KDE is beautiful!"
    date: 2002-04-29
    body: "I don't get it.  The KDE default theme is very attractive, in my opinion.  And I've certainly never met anyone that felt differently.  People seem to fall into two categories about KDE's look, and they are:\n\n1. \"Wow, this is really beautiful\"\n\n2. \"This isn't Windows.  I'm scared!\"\n\nNote that most of the people in category #2 also find WinXP unattractive... because it doesn't look like 9x.\n\nThe most notable quote on KDE's look that I have heard was when the cable guy came to my house to install my cable modem.  He sat down and put his hand on the mouse.  A moment later a look of awe came over his face, and he said, \"Woah....this is REALLY beautiful...what is this, a Macintosh?\"  Exact words.  And yes, I was using the default KDE (2.x) theme, icons, etc.  The only difference was that I had wallpaper from Propoganda.\n"
    author: "Adam Wiggins"
  - subject: "Re: Default KDE is beautiful!"
    date: 2002-04-29
    body: "I couldn't agree more!  It is puzzling when you hear people with completely different but extremely harsh opinions of the same beautiful desktop, enought to make you doubt their sincerity.\n"
    author: "ace"
  - subject: "Re: Default KDE isn't beautiful!"
    date: 2002-04-30
    body: "I'm certainly not one of the \"I'm scared guys\" ..\n\nIt's a case of love and hate, some people love it, some people hate it.\n\nI'm one who \"hates\" it.\n\nIt's impossible to make everyone happy with the same look, I know that.\n\nBut currently, there is NO <default> theme or style or iconset at all which looks *nice*, *clean* and *professional*\nSo only the 'love it' guys are happy now, and the 'hate it' guys not.\n\nAnd THAT'S my (and many others') problem.\n\n\nMaybe it's possible to create a default look with some compromises for both sides?\n"
    author: "rve"
  - subject: "Re: Default KDE isn't beautiful!"
    date: 2002-05-01
    body: "> It's impossible to make everyone happy with the same look, I know that.\n\nSo, the default theme is just here to limit the number \nof people who \"hates\" it.\n\n> And THAT'S my (and many others') problem.\n\nNot really a problem if you can easily change the 'look and feel'.\nwww.kde-look.org\n\nregards "
    author: "thierry"
  - subject: "Re: Default KDE isn't beautiful!"
    date: 2002-05-01
    body: "<quote>\nNot really a problem if you can easily change the 'look and feel'.\nwww.kde-look.org\n</quote>\n\nPlease read my first post a little better..\n"
    author: "rve"
  - subject: "marmite"
    date: 2002-05-02
    body: "I'd have to agree here... kde's default theme is like marmite, you either love it or hate it. (and i'm one of the one's who hates marmite)"
    author: "dave"
  - subject: "Re: Default KDE is beautiful!"
    date: 2002-05-08
    body: "When I first installed KDE3, my GF was jumping up and down saying \"suai suai suai suai suai suai,\" which is Thai for beautiful like a hindred times in a row.  She was right."
    author: "Daniel Bodanske"
  - subject: "Offended by K and certain other letters"
    date: 2002-04-29
    body: "I was playing with my desktop the other day, and suddenly realized that K is offensive along with the letters 'C', 'U' and 'F'. See screenshot for details."
    author: "Judge Dread"
  - subject: "Re: Offended by K and certain other letters"
    date: 2002-04-29
    body: "lol :)\n\n\nbtw. K kan also be seen as the start for a Dutch word named \"kut\" wich is a dirty/offensive word for \"vagina\" ( <-- same in English and Dutch )\n"
    author: "rve"
  - subject: "Re: Offended by K and certain other letters"
    date: 2002-05-01
    body: "We have already been through all of this."
    author: "reihal"
  - subject: "Re: Offended by K and certain other letters"
    date: 2002-05-01
    body: "Yes, and I actually don't mind the big K at all.\n"
    author: "rve"
  - subject: "Kod and Kolygons"
    date: 2002-04-30
    body: "I don't mind KDE default look... in fact i like it for the most part a lot. Though there is no denying for me that some of the current icons are looking dated. I know there are new sets around and in development. It's not an issue for me. Also I agree with Mosfet's tone that one really shouldn't bitch too much about a free package created by virtually all volunteer effort.\n\nI really do loath however the K at the beginning of every app. It's even more irksome, the K being so meaningless. About the only thing i can thank Adobe for last year is in harrassing \"Killustrator\"... at least the new K name shows some imagination and actually (mis)spells a word! About the most disgusting sylable to stick in my throat in all of the software kingdom is \"kword\"... of all the pathetic rip off names. I only wish microsoft would rumble about suing over that one and light a fire under someone's butt.\n\nAlas for *cough* *kough* ... kkk... koughus... uh, koffice.\n\nBut then what can you do? The masses of developers aren't savvy in this area, and some even think it as sheik as pocket protectors to stick that K on there. It's no new thing with KDE. This has been going on with software forever.\n\nThis would lead me into a rant about the utter imbicility of the 'mascot' thing certain operating systems and software packages feel identify them or have come to identify them... personally i like to keep the mongoloid fowl and leprous midget images as far from me as possible. But that's just me, obviously.\n\nBTW, that reminds me, where can i get myself a plush gear toy?\n\n"
    author: "Raggy"
  - subject: "I dislike some visual changes in KDE3"
    date: 2002-05-01
    body: "I just saw KDE3 and some stuff, like the black close and the red(!) lock buttons, really hurts my eyes. I couldn't use it enough to see if I can to get used to that, but I feel that the KDE2 had definitely a more pleasant look.\nEven when a uniform grayish desktop is not maybe the most appealing, always are a lot more useable.\n"
    author: "Julio Cesar Gazquez"
  - subject: "Re: I dislike some visual changes in KDE3"
    date: 2002-05-01
    body: "Then copy and use the KDE2 close and lock button pngs."
    author: "Someone"
  - subject: "Hancom  Office is no better"
    date: 2002-05-02
    body: "I have to agree with the guy that the default kde theme has never been particularly atteactive, in fact kde1 (in particular  KFM) was the ugliest software i had ever seen... then i installed the HancomOffice demo - now HancomWord is ugly. (HancomSheet isn't so bad, but wait a minute they're supposed to be part of the  same office suite, shouldn't they share the same look and feel?)"
    author: "dave"
  - subject: "Who rules?"
    date: 2002-05-05
    body: "Kde and Mosfet.\nnuff said."
    author: "nojoe"
---
Recently, there have been some <a href="http://slashdot.org/article.pl?sid=02/04/25/0122208&mode=thread">comments in the press</a> regarding KDE's look and feel which were, to say the least, rather unflattering.  The comments centered mainly around <a href="http://dot.kde.org/1012076875/">KDE's icons</a> and the overall elegancy of the desktop.  Like many of us, Mosfet felt these comments were unwarranted and somewhat misinformed, but he took the extra step of writing up a <a href="http://www.mosfet.org/elegance.html">public response</a>.  I'm glad someone did.  A lot of people have always enjoyed KDE's default look and feel, but with sites like <a href="http://www.kde-look.org/index.php?xcontentmode=all">KDE-Look.org</a> (includes <a href="http://www.kde-look.org/index.php?xcontentmode=iconsall">icon themes</a>), <a href="http://apps.kde.com/">appsy</a>'s <a href="http://apps.kde.com/uk/0/browse/Art_Music/Themes">theme section</a>, as well as the new dedicated <a href="http://themes.freshmeat.net/browse/61/?topic_id=61">themes section</a> on <a href="http://freshmeat.net/">freshmeat</a>, there are now more possibilities than ever to adjust KDE to your personal liking. KDE 3.1 will offer <a href="http://www.babysimon.co.uk/kde/kde31features.png">even more</a>.
<!--break-->
