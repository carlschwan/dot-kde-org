---
title: "TuxReports: KWord 1.2beta2 snapshots"
date:    2002-07-09
authors:
  - "numanee"
slug:    tuxreports-kword-12beta2-snapshots
comments:
  - subject: "Brief describes it best"
    date: 2002-07-08
    body: "Looking forward for a real review of at least 3 components of KOffice 1.2."
    author: "Anonymous"
  - subject: "Re: Brief describes it best"
    date: 2002-07-08
    body: "Write one!"
    author: "KDE User"
  - subject: "Surprised link"
    date: 2002-07-08
    body: "This link was a surprise.  The snapshots were not meant to be a review.  I was having fun with the product and wrote about the KWord experiences.\n\nIMHO, a review can't be written until I've worked with the product for quite some time. It's a policy of mine set years ago on the site.  Since I just got the apps installed it will take some time fiddling with things.\n\nThe KWord product is quite spectacular.  My hats are off to the developers.  Just take a look at the snapshots and decide for yourself whether you'd like to install it.  IMHO, go for it.\n\n   -- LPH\n* * * * * * * * * * \nPenguin Flight Instructor\nhttp://www.tuxreports.com\nWhere Penguins Fly"
    author: "LPH"
  - subject: "Re: Surprised link"
    date: 2002-07-09
    body: "Yeah - I read your review off of NewsForge, and got the idea that this was more of a preliminary note.  I considered submitting it to the Dot, but it was so sparse, I figured I'd wait for a meatier review - I have no doubt that several will be cropping up in the near future.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Koffice"
    date: 2002-07-09
    body: "Hello,\n\nIt's good to see Koffice getting some press, as it is really maturing into a great office suite; however, I have a small gripe about it and want to know where others stand on this issure.  \n\nSome time ago I posted a wishlist item to koffice-devel list about having the option to shut the inital modal template dialog off in koffice and just default to an empty document.  This caused quite a heated argument that was continued on the usability mailing list.  Most people on the usability list were in favour of having the option, but it was quite the opposite on the koffice-devel list.  I was just wondering what other non-developers thought of having the option to default to a blank document instead of asking you everytime.  \n\nIt seems to me that ignoring the fact that many people do not like this modal dialog box is a very bad approach to gaining new users and giving koffice a chance to effectively compete with the other office suites out there.  Koffice is a wonderful set of programs and it's a great addition to KDE, but many do not like modal dialog boxes or wizard like dialogs one bit and find them very obtrusive.  (I will say though, that this dialog box isn't as bad for Kpresenter)  Every other person I know that uses KDE, including a professor at Pennsylvania State University's Information Science department who's field of study includes human information processing found this dialog to greatly hinder usability.  On a side note, I was thinking of seeing if this professor would consider doing a class project where his students test the usabilty of Koffice, then post the results to the koffice-devel mailing list.  I am not sure if this will happen, but I think it would be great for the project if it does.\n\nAnyway, I am extremely grateful for all the KDE developers and wish them the best of luck.  They have put out a wonderful product, but this is just one of those areas that I feel needs serious attention.\n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Um.\n\nIs this a troll?  It smells legit, but I'm starting to have serious bells go off whenever I hear \"the developers said...\".  Interesting.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "\"the developers said...\" \n\nUhh ... where did I say this?  Listen, I am just curious about this topic and don't see why you'd think I'm trolling. \n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "It's nothing personal.  There are a lot of trolls on this forum of late, and well, people get paranoid.  I, for one, think your original post was quite valid."
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "\nNo, no... it's not you - I was commenting on the atmosphere of late.  There's a fellow who used to say KDE was a Nazi inspired desktop (literally), who now has started off on a jihad about how the core developers are all stuckup and don't listen to users.  While your message reads fine, I was just commenting that you can't take anything for granted of late, and lamenting the fact that I've taken to reading messages for hidden agendas.  The fellow (and this is my opinion, having read his articles but not the original source he is quoting from) has a habit of framing harmless quotes to appear sinister in his essays.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "linuxandmain"
    date: 2002-07-10
    body: "I'm glad I'm not the only one who has noticed this.  It's really pathetic how this guy has been attempting to poison attitudes towards KDE and creating general bad feelings in the community.  Others have been gleefully taking up his cue to harm an innocent project."
    author: "ac"
  - subject: "Re: linuxandmain"
    date: 2002-07-11
    body: "Hey!\n\nYou're ac too!  We're twins! ;-)"
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Heh, as many things in KDE you have to do some tricks to get the right results. Mayby your 'professor at Pennsylvania State University's Information Science department who's field of study includes human information processing' is not so brilliant as ordinary KDE users. I don't understand that he has not been able to think about a work-around for that 'problem'. \n\nHere is how-to: just create an empty (for example) A4 document and save it as emptyA4.kwd .\nWhen work done, save it 'as', and don't save shanges to your empty document so you always have template for a new document without need to face modal template dialog again and again.\nNext time you need A4 doc just open this empty one and you will not be 'tortured' by the 'initial modal template dialog'. Then, if you wish, you can save empty documents for 'memo', us-letter' etc, etc. \n\nCheers,\n\nantialias\n"
    author: "antialias"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "First off, I'd like to clear something up.  I shouldn't have said that my prof friend uses koffice.  I just presented it to him and asked him what he thought since that is the sorta stuff he studies.  Sorry about the confusion.\n\nAnyway, thank you for this suggestion, but I do not feel this is not a proper solution to the problem.  Plus, does this work with kspead and kivio where this dialog serves even less purpose in my opinion (I would check myself, but I removed koffice and don't feel like recompiling it.) Regardless, I just don't see why there was so much fighting on the lists over the option to turn it off.  I mean, it was like I was asking them to rewrite Koffice.  Anyway, I do not want to start a flame war here.  I just wanted to see what others thought.  \n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Not to add fuel to the fire, but I agree.  There's certainly a lot of well-deserved backlash against showing \"wizards\" (which, essentially, the opening dialog box is) for even the most basic tasks.  This wizard inserts an extra step in starting a blank document (from 0 steps to 1 step), and doesn't really remove any steps from opening an existing document (2 steps either way).  The only thing that is easier is opening a recent document (from 2 steps to 1 step).  I guess the real question is: do we need a wizard at all for a process that only takes 2 steps?  How many steps should a wizard save before it becomes useful?  And if you usually open your documents by clicking on the documents in the filemanager, the wizard is a total waste of time.\n\nThat said, it's a minor annoyance in the big picture.  It would certainly be nice to have an option for \"always start with blank A4 document\" or something, though.\n\nBut from a strict usability point of view...unless you've open recent documents (through the wizard) more often than creating new documents, the dialog box is wasting more time than it's saving you.  No question about it."
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Wrong. Create an empty (for example A4) document, call it emptyA4.kwd, and then there where you have your icon for Kword right click on the icon, choose Properties, and under Execute tab > Command write this: kword empty.kwd .\nYou can do that for all kind of templates.\n\nCheers,\n\nantialias"
    author: "antialias"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Er, that makes me wrong how?  Working around a problem does not solve the problem.  Your \"solution\" results in a different usability problem, where the user needs to remember to avoid using the \"Save\" button when they want to save.  (Now THAT'S counter-intuitive!)\n\nThe fact that you're willing to go through so much effort to bypass the dialog indicates that you agree with me--the dialog gets in the way.  But I'm sure you won't see it that way ;-)"
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "'Er, that makes me wrong how?'\n\nIt is possible to start Kword without template dialog. You said: 'This wizard inserts an extra step in starting a blank document...'\n\n'Working around a problem does not solve the problem.'\n\nI solved the 'problem', I can start Kword with an empty document, and without template dialog.\nI don't need it because I always use A4 format. But maybe other people like to have that dialog every time they open Kword. I don't see 'two clicks' as a big problem. In any word processor, and in any complex application you have to click twice or three times to get some things done. What if we had an empty A4 format as default and you would like (for a change) to use us-letter? \n\n'Your \"solution\" results in a different usability problem, where the user needs to remember to avoid using the \"Save\" button when they want to save. (Now THAT'S counter-intuitive!)'\n\nYour 'solution' also results in a usability problem, where the user have to click twice (minimum) to get templates dialog. Koffice developers probably wanted to make template dialog more visible for users. For example, in Microsoft word the template dialog is hidden and many users are not aware that it actually exists.\n\n'The fact that you're willing to go through so much effort to bypass the dialog indicates that you agree with me--the dialog gets in the way.'\n\n1. An effort that takes 10 seconds ;-)\n2. I agree that you and me don't like that dialog (I don't need it), but again there are probably some users who like it :)\n3. I think that opening an empty document without dialog is also possible by executiong one of the dcop? commands, so you can save your document afterwards, without using 'save as',\nbut I am not sure about this.\n\n"
    author: "antialias"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Heehee!\n\nSorry.  When someone replies to a multiple-point post with a simple \"WRONG!\", I can't help but think they're saying all of my points were wrong (\"Wrong!  There is no backlash!  Wrong!  The wizard does not insert an extra step!  Wrong!  It is not a minor annoyance!  Wrong!  It would NOT be nice to have an option!\").  You know, like on Crossfire.  (If you've never seen Crossfire, nevermind).  I was being obstinate because you were being a bit strident.  Great way to communicate, innit?\n\nAnyway, I find the dialog to be a \"minor annoyance\".  I find having to avoid the save button to be worse than a minor annoyance.  It's a matter of taste, I suppose.  I'd certainly rather put up with the dialog box than have to use a program with a \"don't press me\" button.\n\nAnd it WOULD TOO be nice to have an option!"
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "Considering that it is not a huge change, and it would obviously be something users want, I think that it is a valid and good suggestion.\n\nOr even better, \"start with template XXX\". If I have gone through the trouble of making a spiffy template, I want that to be the first thing that is up when I start the app. Other might always want the wizard, while yet others surely want the last document they worked on.\n\nIf the developers are bogged down with requests, I can see why this isn't done right away. My suggestion would be that those who want it and use KWord simply makes the changes and submits the change to the developers. Or if nobody is a developer and has the time, make a little pool where everyone who wants it put in let's say \u00801, and whomever codes the feature first gets the money."
    author: "John Gustafsson"
  - subject: "Re: Koffice"
    date: 2002-07-14
    body: "> Or even better, \"start with template XXX\". If I have gone through the \n> trouble of making a spiffy template, I want that to be the first thing that > is up when I start the app. \n\nImportant note: KOffice 1.2-beta2 remembers the last template that you\nselected, and pre-selects it the next time you start the app.\nIf you create your own spiffy template, and you always use it,\nthen simply hitting Enter at the opening dialog will select it right away."
    author: "David Faure"
  - subject: "Re: Koffice"
    date: 2002-07-10
    body: "I'm going to write a patch to Konqueror to emulate this useful KOffice behavior.\n\nEvery time you start up Konqueror, it will open a modal dialog to ask the question \"what web site do you want to visit?\". There'll also be a listing of recently viewed websites and icons for your bookmarks that you can click on.\n\nPresumably everyone who likes this box so much in KOffice would like it equally well in Konq. If not, I'd be genuinely curious to know what they feel the difference is.\n\n(and yes, I was joking about writing the patch)\n"
    author: "Stuart"
  - subject: "Re: Koffice"
    date: 2002-07-11
    body: "Heh. Email this to the koffice mailing list: koffice@mail.kde.org"
    author: "Bob Smith"
  - subject: "Re: Koffice"
    date: 2002-07-14
    body: "Konq is a viewer, KWord is an editor. The comparison doesn't hold.\n\nThe danger here is that if KWord doesn't ask, then you'll start typing your text, and _then_ you'll realize you didn't use the right template around it.\nIf it's only about the page size, no problem you can change it.\nBut if it's about a DTP document vs a WordProcessing document, or if you wanted the standard company header, or the FAX fields, etc. etc. then you'll have to end up copying your text from the current doc into the right template, not very nice. That's why KOffice asks the question on start."
    author: "David Faure"
  - subject: "Re: Koffice"
    date: 2002-07-24
    body: "Yeah, but if a user is that stupid to use the wrong template; that's their fault.\n\nIf they're doing work at Office or Home/School, then they best be checking they're using the right equipment.\n\nAlso, computers have a certain minimum level of complexity, and some of us can handle it while others can not.  An option to hide the wizard on start-up is fine.  It's not like you can't \"uncheck\" that option.\n\nGiving users choice is not stupid.  So everyone arguing about it, please STFU.  If an average user can't figure out what's going on, then why did they start using it on a regular basis in the first place?\n\nJust let the change occur using the Options for allowing/disallowing wizard showing at startup.  Then everyone is happy.\n\nIf anyone thinks that's stupid, then tell MS to change their Office product to include a start-up wizard b/c I'm sure they'll think it'll attract more users."
    author: "wgrim"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "I agree with most of what you said, namely that popping up a dialog every time you create a new document in KOffice is somewhat off-putting.  I'd rather see a normal empty document too by default and some other way to handle the other current choices."
    author: "Navindra Umanee"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "In my opinion you're right and defaulting to a standard template is the preferred way.  Most of the time I and other people I know only work with one standard template that we have defaulted to.\n\nNow I need to click on it in a file browser to open KWord, which means I first have to open the file browser or use the wizard-dialog in KWord, which is an extra step. I think most of the people use a standard template (p.e. for a particular paper format like A4 or US-Letter) and a few different templates (p.e. for letters to companies). I think MS Word and Open Office.org in this respect do the right thing. There can't be that many people that are wrong!"
    author: "Patrick Smits"
  - subject: "Re: Koffice"
    date: 2002-07-09
    body: "To all who agree with this change: \n\nMake your thoughts known to the KOffice developers!\n\nEmail your suggestions to koffice@mail.kde.org, the KOffice mailing list. Maybe if enough complain the developers will finally agree to get rid of that lame dialog box."
    author: "Bob Smith"
  - subject: "Re: Koffice"
    date: 2002-07-10
    body: "it has been discussed. to death. read the threads on koffice-devel and kde-usability. you may even discover that, contrary to what Biz implies, a workable conclusion was arrived at that doesn't require the complete removal of the introductory window but does remove the annoyance of an intervening modal dialog.\n\nthe concept is to merge what is in the introductory window with the actual application window, perhaps as a sidebar, to be shown at start up. best of both worlds!\n\nthere are some very real issues with simply \"removing it\", including setting (and subsequently resetting) the default template. or quick and obvious access to recent documents (yes, there is a menu item for it and yes, it is quicker and more obvious in the current window) ... or how to conveniently walk a user through the set up of a complex template w/out bringing them through a completely foreign interface just for that purpose. it isn't like the developers got together and went \"pff... leave it as it is! who, us care?\" there was actual discussion with actual points made on both sides and a new concept was developed.\n\nperhaps you (the collective you) could sponser a developer to create the merged-window-widget-thingy. (just realize that it won't be in the upcoming release of of koffice: it is already in feature freeze.)\n\n\nbtw, my personal opinion (as if that matters any ;) as to why people dislike the window soooo much is twofold: they aren't used to it (other office suites do it differently, with their own braindamage as a result; but of course, we wouldn't want something unique in our KDE apps ;-) and it is a seperate, modal window (which is penultimately frustrating for a user: you open an app and a flurry of windows open, some of which are locked by others which are in front of it). in other words it is simultaneously frusrating and disorienting in its current form. no wonder people don't like it. interestingly, but perhaps unsurprisingly, few complain about the actual capabilities it offers. i have a feeling that the koffice devels are right: by merging it into the main window people would probably be quite happy with it."
    author: "Aaron J. Seigo"
  - subject: "Re: Koffice"
    date: 2002-07-10
    body: "Hello,\n\nDo you think properly redesigning the dialog box is any easier a solution than finding a way to reset default templates if the dialog box were made to be shut off?  I know you said, in the list, that you thought of changing the modal dialog box, but it did not seem that any strong agreements were made and this is why I brough it up again.  Also, why does every discussion about this always centre around Kword.  Kivio is not template based and yet this dialog still appears.  The templates in Kspread make no sense to me and I don't see this dialog being of any use for this program either.  I did mention that the dialog does have advantages for Kpresenter, but it could use work, as you mentioned.  \n\nAs to you're personal opition about people not being used to it, I say this.  I think that most of the people on this list have had plenty of time to get used to it, but they still do not seem to like it.  The one poster has gone through great pains to avoid it.  Please do not get me wrong.  I have no problems with inovation and am all for it, but just do it with great caution as people in general resist change.  Anyway, thanks for responding to this post and I'm sorry for not letting this topic die.\n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: Koffice"
    date: 2002-07-11
    body: "The difference between him and us is he codes and we don't.  So yeah, his opinion DOES count more.  I'm not saying this to be snide; I'm saying this because I think these coders don't get the credit they deserve for being pulled in twelve directions by people who don't help (that's me, guilty as charged).\n\nThey see the usability problem.  They have a solution that they like, and they're working on it.  It doesn't match our solutions, but there is always the possibility that their solution is better.  Until I see their solution (or have a deep enough understanding that it's equivalent to having seen it), I'll shut my mouth on this topic.\n\nBut once I see it, it better be good ;-)"
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-11
    body: "Hello,\n\nI am not at all saying that the developers do not deserve more credit.  Hell, I still have a hard time believing the project has grown so mature since KDE 1.x.  They are great coders, but sometimes it takes an outsider like yourself to point out problems.  I am an mech engineer and run across this in my work as well where an outsider sees things differently and provides a better solution.  That outsider may not know how to implement the solution, but their idea is often better.  Sometimes knowledge of a particular topic does get in the way.  This can not be avoided.  Having people do usability studies that don't know a thing about coding is extremely valuable in my opinion and their opinions should considered just as valuable.  Again, please do not get me wrong.  I think the developers are all extremely talented people and I tip my hat at them for what they are doing and wish them all my thanks.\n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: Koffice"
    date: 2002-07-14
    body: "You remind me of that idea of \"merging into the (already much crowded) UI\",\nI did see it flow around some posts, but didn't remember it was considered\nto be the best solution by the kde-usability people.\nIf that's indeed the case, can you post a wishlist item to bugs.kde.org,\nso that we are reminded to do it after KOffice-1.2 ?\n\nPersonnally I think the UI (toolbars in particular) is for stuff that you need often, not for stuff that you only need once before you start to create\nyour document, so this merged wiget would take useless space during\nthe editing of the document, IMHO. But I'm no usability guru ;)"
    author: "David Faure"
  - subject: "Re: Koffice"
    date: 2002-07-15
    body: "<< Personnally I think the UI (toolbars in particular) is for stuff that you need often, not for stuff that you only need once before you start to create\nyour document, so this merged wiget would take useless space during\nthe editing of the document, IMHO. But I'm no usability guru ;) >>\n\nIt sounds like he's thinking of a MS Office XP style sidebar. IMHO, the sidebar, as it works in Office, is a pretty good idea. It lists your recently used documents, templates, and has options for various new documents. It even has a checkbox at the bottom so you can choose not to see it again, if you wish.\n\nNot that an idea originating with MS would go over too well here, but we all probably know who they got the idea for the shadows under the menus from... ;)"
    author: "Josh"
  - subject: "Simple Solution"
    date: 2002-07-10
    body: "I can see a simple solution to this problem: add a tickbox at the bottom of the dialoge with the words \"Do not show this next time\" or something simalar. When it's ticked the program can be set to not show it the next time the program is started.\n\nThis is a solution that should please both the people who don't like this and the people who finds this useful.\n\n- James"
    author: "James Pole"
  - subject: "Re: Simple Solution"
    date: 2002-07-10
    body: "I think this is acceptable for me. Everybody then gets to see the dialog, but can skip it in the future.\n"
    author: "Patrick Smits"
  - subject: "Re: Simple Solution"
    date: 2002-07-14
    body: "So you're stuck with the same template for ever?\nOr you then need another checkbox somewhere to bring this dialog back...\nPlus, when you do want to use another template, it's kinda hard.\nReally, is it so hard to just press Enter?"
    author: "David Faure"
  - subject: "Re: Simple Solution"
    date: 2002-07-14
    body: "David, \n\nIt's a matter of planned featured versus available features versus used features.  KWord has a wonderfully extensible architecture that will allow it to someday become a very solid DTP app.  I can tell that you're thinking long-term about the day that someone can simply use KWord as a drop-in replacement for FrameMaker.  For the DTP crowd, it's bad to assume a template.  For the DTP crowd, it's always nice to be asked.\n\nBut KWord is not replacing FrameMaker yet.  Not only that, but when it DOES get that good (and I have faith that it will), it will still be primarily used as a word processing app.  There are simply more word processing users than DTP users.  DTP is a small and exclusive market.  It's very ambitious and very smart for KWord to target that.  But don't ignore the users.  Even if the users seem to be using your tool not for its intended purpose, this is merely a fact that you should consider when developing your app.\n\nIf someone discovered an easy way that KWord could be used to edit MP3 files, and, in fact, that's what most people used it for, the developers can (and should) sit up and take notice.  If someone says \"KWord could be the best MP3 editor in the world if it only had (easy to implement) feature X\", the correct response should not be \"KWord is not an MP3 editor\", but \"What's the best way to make the users happy?\"  KWord may very well not be an MP3 editor, but if that's what people want to use it for, let them.\n\nPeople use KWord as a word processor, for now.  A typewriter replacement, not a professional typesetting replacement.  Take notice.  I understand (I really do!) your attachment to keeping a DTP framework in place because you don't want people to see KWord as a dumb Winword ripoff (because it isn't).  Nevertheless, people are using KWord as they would use Winword, and they aren't going to stop.\n\nSo in answer to your questions:\n\n- So you're stuck with the same template for ever?\nNo, you are stuck with one template until you choose another template.  Although (brace yourself) most users really would not care if they WERE stuck with the same template forever.  Scary, huh?  Nevertheless true.\n\n- Or you then need another checkbox somewhere to bring this dialog back...\nSounds like you're already solving the problem! ;-)  Yes, perhaps a checkbox to disable/enable the dialog would be a very good solution.  A simple disabled-by-default \"always use this template\" pref (not a checkbox, actually) would solve everyone's problems and DTP users and word processing users would live in harmony.\n\n- Plus, when you do want to use another template, it's kinda hard.\nMost users would never use more than one template.  Sure it's creepy and wrong and underutilizing a wonderful tool, but there it is.  Many users don't ever change their document fonts either.  I shudder to think.\n\n- Really, is it so hard to just press Enter?\nNo, but it is very noticeable (small gripe: it's not a matter of hitting enter, it's selecting the template and then hitting enter).  When you start KWord, and you select a template (always the same template, never anything else), you begin to wonder \"Why do I have to do this every time?\"  Can't I skip this step since I know I will never ever choose a different option as long as I live?  Yes, I understand that some users like the option.  Those users should have an option not to skip it.  Then everyone will be happy.  And that, after all, is the goal.\n\nac"
    author: "ac"
  - subject: "Re: Simple Solution"
    date: 2002-07-15
    body: "> (small gripe: it's not a matter of hitting enter, it's selecting the template and then hitting enter)\nOnce again: I have changed this in CVS (and in 1.2-beta2), and the last-selected template\nis _pre-selected_ now, so it's really only about hitting Enter.\n\nThanks for the other arguments, but I think you're missing the point. This isn't only about\nDTP users. This is about Fax templates, Memo templates, Letter templates, .... you'd be surprised how many templates people can come up with. Most of those are not part of KOffice yet (due\nto the translation issue), but those templates are available now at http://www.koffice.org/download\n\nHonestly, I think this isn't about WP vs DTP users, it's about home users vs corporate users\n(I would guess that corporate users are the ones that use templates much more; those above, plus their own).\n\nFor the record, Powerpoint comes up with a similar template dialog ; Lotus WordPro too (IIRC) ; ....\nWinword doesn't. So? ;-)\n"
    author: "David Faure"
  - subject: "Re: Simple Solution"
    date: 2002-07-15
    body: "You're correct.  And I dislike that about Powerpoint and Wordpro.  So at least I'm consistent about it ;)"
    author: "ac"
  - subject: "Re: Koffice"
    date: 2002-07-10
    body: "I agree, I never use that dialog even if I intend on using one of its options --I close it and then do it \"manually.\" In any case I'm surprised that the issue sparked such discussion. Would it be that complicated to include the option of disabling it? What disadvantages could the option possibly have?\n"
    author: "hmm"
  - subject: "Re: Koffice"
    date: 2002-07-14
    body: "You completely miss the fact that your post led to a change in the code:\nthe template dialog now remembers the last template you selected,\nand pre-selects it on the next run.\n\nSee also my other post here about why opening on a blank document can be asking for trouble down the road."
    author: "David Faure"
---
<a href="http://www.tuxreports.com/index.php">Tux Reports</a> is featuring <a href="http://www.tuxreports.com/modules.php?op=modload&name=News&file=article&sid=855">a brief review</a> of <a href="http://dot.kde.org/1025176121/">KWord 1.2beta2</a> particularly highlighting the popular WYSIWYG feature. <i>"KWord launched into a beautiful layout. Since I hadn't seen any screenshots of the new layout, it caught my attention. Nice job!"</i>  The review includes some nice screenshots.
<!--break-->
