---
title: "Quickies: Community Study, KSteak, Kde France"
date:    2002-06-09
authors:
  - "numanee"
slug:    quickies-community-study-ksteak-kde-france
comments:
  - subject: "737 people working on KDE?"
    date: 2002-06-09
    body: "That article in \"S\u00fcddeutsche Zeitung\" says there are 737 members in the KDE \"team\". Where do they get this number from? Would be interesting."
    author: "Stephan Oehlert"
  - subject: "Re: 737 people working on KDE?"
    date: 2002-06-09
    body: "~> wc -l programming/kde/kde-common/accounts\n    745 programming/kde/kde-common/accounts\n\nUnfortunately they didn't realize that some accounts are shared across many contributors, as in the case of translation teams.  So the number should be a bit bigger.  But, they're in the ballpark at least."
    author: "Neil Stevens"
  - subject: "Re: 737 people working on KDE?"
    date: 2002-06-09
    body: "That programming/kde is surely your own path, Neil :-)\n\nAnother way to see it is to browse http://webcvs.kde.org/cgi-bin/cvsweb.cgi/*checkout*/kde-common/accounts?rev=1.802"
    author: "ariya"
  - subject: "Re: 737 people working on KDE?"
    date: 2002-06-10
    body: "Of course it's my own path.  I pasted it hastily, having been awake longer than I should have been. :-)"
    author: "Neil Stevens"
  - subject: "decisions about the project's general direction"
    date: 2002-06-09
    body: "\u00dcber die generelle Richtung des Projekts bestimmt eine Kerngruppe, der derzeit rund 20 Entwickler angeh\u00f6ren. Sie trifft die wichtigsten Entscheidungen \u0096 zum Beispiel welche Anwendungen in neu ver\u00f6ffentlichte Versionen aufgenommen werden \u0096 auf Grundlage demokratischer Wahlen. Mitglied in diesem \u0084KDE Core Team\u0093 kann jeder werden, der sich verdient macht.\n\nRough translation:\nThe core team, about 20 people, decide about the projects' general direction. They make the most important decisions - like what applications are included in new releases - based on democratic elections. Member of this group of core developers can become, whoever has contibuted to the project accordingly.\n\nIs that true, did I miss something? "
    author: "me"
  - subject: "Re: decisions about the project's general direction"
    date: 2002-06-09
    body: "I'd rather call it a \"loose democratic system\". People take their position on certain issues and that ends in a result. \n\nOf course, the ideas of people acually working on that issue which is subject to discuss and long time project members ideas are mostly accepted because they often know best. However, that's not really a \"dictatorship\" of the core-team as it might appear in the article. KDE is always open for reasonable arguments, that's one of the thing KDE makes so successfull and popular.\n\nThat's why I think calling the way decistions in KDE are made a \"group dynamic process\" fits it best.\n\nCheers,\n\n</daniel>"
    author: "Daniel Molkentin"
  - subject: "What are they talking about?"
    date: 2002-06-10
    body: "Making decisions democratically has been consistently rejected!\n\nThe concept of some developers getting to make special decision has been consistently denied!"
    author: "Neil Stevens"
  - subject: "Quit Breaking Records!!!"
    date: 2002-06-09
    body: "Would you guys quit breaking so many records?!? How am I suppose to keep up to date on all of this if there are new records broken so often!!! Sheesh, could some of you go work on the GNOME project or something to slow down the record breaking? <g>\n\n  -Tim"
    author: "Timothy R. Butler"
  - subject: "Re: Quit Breaking Records!!!"
    date: 2002-06-09
    body: "While I don't work on any GNOME projects, I do keep an eye on Abiword and Gnumeric on daily basis :-8"
    author: "ariya"
---
<A href="mailto:lrathle@kde-france.org">Laurent Rathle</a> wrote in to plug the <A href="http://www.kde-france.org/">Kde France</a> site where French speakers can enjoy forums, news, translations of <a href="http://dot.kde.org/">KDE Dot News</a> items as well as <a href="http://kt.zork.net/kde/">KC KDE</a> issues, amongst other resources.  Alongst with <a href="http://www.kde.org/fr/">KDE.org/fr/</a>, KDE French users should be well catered for.

<A href="mailto:staerk@kde.org">Klaus Staerk</a> wrote in to point out that <a href="http://www.der-fritz.de/ksteak">KSteak</a>, the English-German translation tool,
is the KDE.de <a href="http://www.kde.de/appmonth/2002/ksteak/">App of the Month</a> (de) for June 2002.  An <a href="http://www.kde.de/appmonth/2002/ksteak/interview.php">interview</a> (de) with the author as well as a <a href="http://www.kde.de/appmonth/2002/ksteak/beschreibung.php">screenshot laden description</a> (de) of the app is available. <a href="mailto:mmh@gmx.net">Moritz Moeller-Herrmann</a> also wrote in to point that a major German newspaper recently carried an <a href="http://www.sueddeutsche.de/aktuell/sz/artikel187.php">introduction to KDE</a> (de).

Marcus Camen notes that KDE is <a href="http://kde.mcamen.de/statistics.html">breaking records</a> -- again.

Finally, <a href="mailto:a.brand@em.uni-frankfurt.de">Andreas Brand</a> is a sociologist studying KDE for a German University under "Project Electronic Labor Markets".  Check out <a href="http://lists.kde.org/?l=kde-cafe&m=102328776511973&w=2">all the details here</a>, and feel free to help out Andreas.
<!--break-->
