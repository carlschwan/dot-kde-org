---
title: "KDE Events Opens Its Doors: Register to Join KDE at LinuxTag"
date:    2002-05-07
authors:
  - "eva"
slug:    kde-events-opens-its-doors-register-join-kde-linuxtag
comments:
  - subject: "kudos"
    date: 2002-05-07
    body: "Thanks for another nice site, eva!\n"
    author: "Navindra Umanee"
  - subject: "Is there an event in Malaysia"
    date: 2002-05-07
    body: "Or at least in Singapore? There is a lot of hype about OSS here..."
    author: "Rajan R."
  - subject: "Re: Is there an event in Malaysia"
    date: 2002-05-08
    body: "If you know about one and you (or anyone else) is going to present KDE there - please tell me. I'd be glad to add another event where KDE is present :-)\nAnd of course you can get help from the kde-events mailing list.\n\nYou are probably right, that the site is mostly about European events and ressources - that's not because I think it's more important or anything, but rather because that is the information I have. Now it's time for people from other places on the world to add their bit.\n\neva\n"
    author: "eva"
  - subject: "Re: Is there an event in Malaysia"
    date: 2002-05-08
    body: "Well, the only event they have here was LinuxWorld 2000 Kuala Lumpur, but never had ever since because of the economy :-(\n\nHopefully, there would be another event, since hype has been growing about OSS and Linux here"
    author: "Rajan R."
  - subject: "Spreading Ourselves Too Thin?"
    date: 2002-05-08
    body: "This seems to be yet another website in a very long list that may be duplicating efforts, promo.kde.org, enterprise.kde.org, www.kdeleague.com - heck there's even a women.kde.org. This isn't to say kde development isn't lightning fast, but  why all the websites? News that gets posted one usually finds it way back to the dot.  Just a starting point for opinions."
    author: "Jon"
  - subject: "Re: Spreading Ourselves Too Thin?"
    date: 2002-05-08
    body: "I wouldn't say there are too many KDE websites, only too many without active maintainer. Add www.konqueror.org, multimedia.kde.org and several language portals like www.kde.org/fr/). These look like ideal projects for people who want to contribute to KDE but don't want to program or translate it."
    author: "Anonymous"
  - subject: "Re: Spreading Ourselves Too Thin?"
    date: 2002-05-08
    body: "Another thing that comes to my mind where everyone can help: Get an account on freshmeat.net and add/update popular KDE applications to promote your favorite desktop. "
    author: "Anonymous"
---
I'm proud to announce that <a href="http://events.kde.org">KDE Events</a>, a new site for information  about upcoming KDE events, is now online. The site is meant for community members who want to help represent KDE at trade shows and expositions, want to give talks or present their pet project at the KDE booth, and so on... KDE Events also includes a <a href="http://events.kde.org/registration/">registration form</a> for people who want to support KDE at upcoming events. In particular, you are all invited to <a href="http://events.kde.org/registration/">sign up online</a> for the upcoming <a href="http://www.linuxtag.de">LinuxTag 2002</a> taking place June 6-9 in Karlsruhe, Germany.
<!--break-->
