---
title: "KDE/aRts Video Roadmap Meeting Results"
date:    2002-01-28
authors:
  - "nstevens"
slug:    kdearts-video-roadmap-meeting-results
comments:
  - subject: "mpeglib"
    date: 2002-01-28
    body: "I've found mpeglib to be pretty good.  It certainly plays the few MPEG videos I have on my computer, and even manages to do full-screen video just fine on my Celeron box.\n\nHowever, the integration with KDE/Noatun doesn't seem to be particularly good (Noatun usability is something else that could be worked on :-). It's just an independent X window that pops up to play the video.  I see the approach has promise though, and the Noatun/aRts sound filters actually work. Mpeglib capabilities isn't really well-known in the KDE world, I'd say.\n\nDon't know much about the other two efforts.  Xine seems to be pretty advanced from the webpage.\n"
    author: "Navindra Umanee"
  - subject: "Re: mpeglib"
    date: 2002-01-28
    body: "AFAIK Noatun is not intended for video playback, it is playlist-based. For watching videos Kaboodle is the right app."
    author: "Tim Jansen"
  - subject: "Re: mpeglib"
    date: 2002-01-29
    body: "I think Charles may disagree, but I consider Noatun a music player and only a music player.\n\nKaboodle is supposed to be the generic player."
    author: "Neil Stevens"
  - subject: "Re: mpeglib"
    date: 2002-01-29
    body: "Yeah, I use to think the same way.  Video players with playlists just seems stupid.\n\nUntil, that is, I popped a cd I burned with two divx files on it (first and second halfs of a movie) and Windows XP said the cd had video files and asked if I wanted to watch it.  So I just said yes, and it opened up windows media player (and no, we shouldn't make a windows media player style player, it's too big and bloated) but it put both files in the playlist so it just played the whole movie.  No need for me to watch one then go open the other file when the first finished.\n\nIt just gave for a very nice experience, so my opinion about playlists in video players has changed.\n\nAnd a nice all-in-one media player would be excellent.  At least a all in one video player and an all in one sound player, so there is no need to have 5 programs that all do pretty much the same thing only with different file formats (divx, mpeg, dvd)."
    author: "Travis Emslander"
  - subject: "Re: mpeglib"
    date: 2002-01-28
    body: "> Don't know much about the other two efforts. Xine seems to be pretty\n> advanced from the webpage.\n\nXine is, like mplayer and all the others, intended for playback and sometimes recording of video. They do not allow you create pipelines like DirectShow and GStreamer do (and Arts for audio). Pipelines are very useful for many applications though, basically everything that goes beyond simple playback and recording (video editing, video conferences and all kinds of 'smart' features like cameras that detect movement and follow the user). "
    author: "Tim Jansen"
  - subject: "Re: mpeglib"
    date: 2002-01-28
    body: "So witch approach of the three does allow you to crate pipelines and create software with advanced features?\n\nI dont think that the people at this meeting, who are all very experienced multimedia coders are as shortsighted as you describe them (sorry if I missunderstood you).\n\n\n\n\n"
    author: "tester"
  - subject: "Re: mpeglib"
    date: 2002-01-28
    body: "Team blue does, and red possibly as well."
    author: "Tim Jansen"
  - subject: "Xine rox"
    date: 2002-02-03
    body: "all pron vids full screen on my P233"
    author: "NameSuggesterEngine"
  - subject: "gstreamer"
    date: 2002-01-28
    body: "I haven't checked up on it in a while, but last I knew the gstreamer project was aspiring to be an entire multimedia framework capable of audio and video, and wasn't toolkit/desktop dependent on Gnome.  In fact the last time I read their site they invited KDE developers to build upon it for a KDE framework.  I guess I would like to see this, because it seems to be doing things with an extensible plugin architecture, seems to support many different formats already, and most of  I hate to see the wheel get invented for the hundredth time (how many different audio/video libraries can you think of out there? Too many I bet) let's use something already good and established and not add to the pool of unfinished multimedia libraries.\n\nThis is all IMHO of course.\n\nJoe"
    author: "Joe Theriault"
  - subject: "Re: gstreamer"
    date: 2002-01-28
    body: "The main problems are:\n- KDE coders usually prefer C++, and GStreamer is written in C\n\n- they use GLib's object framework which is a quite clumsy way of having OO features in C, especially if you are used to C++\n\n- GStreamer and Arts share much of the same functionality for audio. GStreamer's main advantage is video support, Arts main advantage is MCOP (which allows its use as a sound server, among other things). It wouldn't make much sense to use them both, which would require a painful decision...\n\n\n"
    author: "Tim Jansen"
  - subject: "Re: gstreamer"
    date: 2002-01-30
    body: "true enough about your first point, although GOB (http://www.5z.com/jirka/gob.html) is neat.\n\nand you're right on your second point. gstreamer does, however, have some arts elements. i've been maintaining the build system for them (unfortunately they're borked in the latest release, but we've got another one coming soon), so the sound -server aspect can be accomodated. i think the competition is quite nifty, especially considering that my main interest is audio.\n\ni guess my main point is do take a look at gstreamer. even if c is distasteful, you can see how we've done things and perhaps learn from them.\n\nbest regards,\nwingo."
    author: "andy wingo"
  - subject: "Re: gstreamer"
    date: 2002-01-30
    body: "when i referred to your second point i of course meant your third ;) my bad."
    author: "andy wingo"
  - subject: "Re: gstreamer"
    date: 2002-01-28
    body: "As someone who is not a Linux multimedia developer, I could care less about C vs. C++ or the issues with Glib's object framework. I am simply a user and those problems are only pertinent to developers.\n\nI have other problems as a user. Currently my aRts using apps stomp on my OSS or esd using apps. Having aRts doesn't really fix the problem of multiple apps accessing the sound card. I still can't play more than one video stream to my X desktop. Multiple movie players and XawTV windows stop each other from outputting any video at all.\n\nIf developer problems continue to force single-desktop centric solutions, I don't think users will ever get something they can live with. I'd like to see something like CSL be successful and maybe have ALSA really take hold, but I just don't see that happening any time soon.\n\nUnderstand I don't mean to detract from the KDE, aRts, Gnome, and Gstreamer developers. I really value the work that they do and appreciate their commitment to open source. Ultimately though, if they can't work together and their software causes just as many problems as they solve for my desktop apps, I have to ask why they even bothered in the first place"
    author: "Reid Hekman"
  - subject: "correction"
    date: 2002-01-29
    body: "The correct phrase is \"I *couldn't* care less\"."
    author: "A. Pedant"
  - subject: "Re: gstreamer"
    date: 2002-01-29
    body: "Obviously you are unaware of the magic of artsdsp.  Simply run \"artsdsp yourprogram\" and yourprogram will be able to play music through aRts.  I believe you can even trick esd this way (try \"artsdsp esd\").  There is a beta version of a kernel module that emulates an OSS sound card but sends the output to aRts.  If this ever matures, it would even remove the need for artsdsp.\n\nA single-desktop specific solution would actually be a great improvement over the current situation, where every project fends for itself, wasting much time and effort.  If there were a couple of dominant media frameworks (aRts, gstreamer, etc) it would greatly simplify Linux multimedia development.  You might even see companies (think Sorensen) releasing Linux versions of their codecs, if they could be assured that such codecs could be easily used and integrated into Linux systems.\n\nIn the end, if KDE uses aRts and GNOME uses gstreamer (as is almost guaranteed to happen), it just means that there are two high-quality open-source multimedia frameworks out there.  It will still be much better than how it is now.\n"
    author: "not me"
  - subject: "Re: gstreamer"
    date: 2002-01-30
    body: ">There is a beta version of a kernel module that emulates an OSS sound card but sends the output to aRts. If this ever matures, it would even remove the need for artsdsp.\n\nLinuxism! Yuck. artsdsp is indeed a nice workaround, but a kernel-module is not going to remove the need, since all the world isn't Linux you know. Us FreeBSD people that use KDE still need artsdsp.\n\nMaybe the people could build in a esd compatibility layer? Some sort of hacked esd daemon which sends data to artsd? That would be lots better, IMHO, and also crossplatform"
    author: "Coolvibe"
  - subject: "Re: gstreamer"
    date: 2002-01-30
    body: "Well, since \"artsdsp esd\" works, there is no need for any further compatibility layer.  The kernel module is just a convenience.  (although I did hear somewhere that artsdsp doesn't work on everything quite yet)"
    author: "not me"
  - subject: "Re: gstreamer"
    date: 2002-01-30
    body: "FreeBSD?\n\nOk, here are so many different tastes to please.\n\nLeave KDE and Gnome people to code thinking in Linux, then, you hack it around to make it work on your flavor of BSD... We are complaining about how hard it is to keep developers' efforts focused on two or three multimedia workarounds.\n\nMaybe it would be easier for you and for us if you switch to Linux. Leave BSD for the servers and if you want multimedia, switch to Linux, or Mac OS X. Not Windows."
    author: "linuxgonz"
  - subject: "Re: gstreamer"
    date: 2002-02-03
    body: "> Leave KDE and Gnome people to code thinking in Linux, then, you hack it around to make it work on your flavor of BSD... We are complaining about how hard it is to keep developers' efforts focused on two or three multimedia workarounds.\n\nWhat?!? What about writing PORTABLE CODE instead of the 'kludge around until it works on that other platform'? \n\nYou also forget that FreeBSD is just as suited for multimedia as a flimsy linux box. I shouldn't be forced to use Linux or Windows to do multimedia \n\nKeep userspace things in userspace please, no kernelspace kludges.\n\nCheers,\nCoolvibe"
    author: "Coolvibe"
  - subject: "Re: gstreamer"
    date: 2002-01-31
    body: ">Having aRts doesn't really fix the problem of multiple apps accessing the \n>sound card. I still can't play more than one video stream to my X desktop. \n>Multiple movie players and XawTV windows stop each other from outputting any \n>video at all.\n\nWell, I'm not sure anyone can really do anything about that.  The problem you're experiencing is because your hardware can only support one overlay and the app is using Xv poorly, ignoring the fact it's already in use by something else.  I'm not sure if it's a problem with the Xv API, or the way Xv apps are coded, but when two apps try to use a single Xv overlay at the same time, bad things happen."
    author: "Chad Kitching"
  - subject: "GStreamer revisited"
    date: 2002-01-28
    body: "This meeting brought out more interested parties than I ever imagined it would.  It turns out one of those interested parties works on GStreamer.  In addition to other useful information, he asked me to let GStreamer developers give a \"pitch\" for KDE to use GStreamer for video.\n\nAll the issues Tim brings up are my concerns as well, and I'm bringing them up with the GStreamer developers.  But I'm not ruling it out, especially if it turns out transcode is too architecturally limited.\n\nhm... I wonder if the blue team is going to give any updates here. :-)"
    author: "Neil Stevens"
  - subject: "I'm glad to hear the decision"
    date: 2002-01-28
    body: "I like the direction that KDE is going with aRts and Noatun, it certainly has potential to be more powerful than the alternatives at some point.  But I'm also glad to see that Xine integration with KDE is proceeding, since right now Xine is very good as a player, and I'd hate to see KDE ignore it.  I think Noatun has the potential to be a great all purpose player similar to Windows Media Player, whereas XMMS and Xine are parallel to Winamp in the Windows world.  Media Player intergrates into Windows better, but some people still prefer their Winamp as a dedicated mp3 player.  Choice is a good thing, having both alternatives actively worked on by the KDE team is great news for KDE."
    author: "Anonymous"
  - subject: "arts and alsa"
    date: 2002-01-28
    body: "I wonder, wether arts will support native alsa (no oss emulation), or was it just me, that did not get this combination to work ?"
    author: "Milifying"
  - subject: "only three teams?"
    date: 2002-01-28
    body: "whats about mplayer - license problems (non-gpl)?"
    author: "katakombi"
  - subject: "Re: only three teams?"
    date: 2002-01-28
    body: "Unreadable code ;)"
    author: "Charles Samuels"
  - subject: "Re: only three teams?"
    date: 2002-12-02
    body: "Iwould like some information about Malta Katakombi of Rabat and photos."
    author: "Althea  Buttigieg"
  - subject: "Re: only three teams?"
    date: 2002-12-02
    body: "Iwould like some information about Malta Katakombi of Rabat and photos."
    author: "Althea  Buttigieg"
  - subject: "Don't forget the users!"
    date: 2002-01-28
    body: "To read all this proposals and choices is a good thing but let's don't forget\nthe final end-user. Since we want to make KDE succesfull to as many users as possible\nwe have to not forget that a simple end-user, non-techie, must know that he/she can\nplay all multmedia using the same application. This is why Windows Media Player\nhas success over other tools, most of them better for a particular type of media.\n\n<b>A normal user doesn't have to know that the multimedia file format x is\nplayed better with program y than program z</b>\n\nForm this point of view, the goal should be a \"wrapper\" application, let call it\nKDE Multimedia Player, that should use a plug-in model to be able to play and\nmanage all multimedia content a user migt use (including multimedia streaming).\n\nLet's not forget this!\n\nSocrate"
    author: "Socrate"
  - subject: "Re: Don't forget the users!"
    date: 2002-01-29
    body: "Indeed, the \"KDE Media Player\" already exists, since kde 2.1, and does exactly what you describe (with Vorbis, MPEG, and mp3)"
    author: "Charles Samuels"
  - subject: "Re: Don't forget the users!"
    date: 2002-01-29
    body: "Vorbis, MPEG, MP3 *by default,* aRts has plugins of course for more codecs like Tracker files, SPC, and DivX ;-)"
    author: "Charles Samuels"
  - subject: "Aktion?"
    date: 2002-01-28
    body: "Is aKtion dead? I like the interface of aKtion though it can't play much video files. \n\nI request that Noatun be like aKtion when playing video files that is 'single windowed' instead of 'multiple windowed' like xine or zzplayer."
    author: "Asif Ali Rizwaan"
  - subject: "Re: Aktion?"
    date: 2002-01-28
    body: "Kaboodle aims to be what you want.\n\nAnd yes, aKtion is essentially dead.  I don't think it ever even got ported to use aRts.  xanim frontends aren't very popular these days."
    author: "Neil Stevens"
  - subject: "the teams.."
    date: 2002-01-28
    body: "Allow me to share some thoughts...\n\nTeam Blue - could someone clarify a bit more about advantages (and disadvantages if possible) about libav? it's the first time I read about it, and from reading the content of the mail linked on the article - it doesn't says how it will support any codec (other then mpeg) - so the more details - the better...\n\nTeam red - I quickly read the \"about\" part of transcode and it looks impressing - but I have 4 questions about it:\n\nA. how do you actual play the file? stand alone player? kio slave? \nB. Does it let the end user to convert from format A to format B?\nC. How does it handle \"3rd party\" codecs - Windows Media V7, V8, etc..\nD. does it allow to \"broadcast\" in a sense that I can put a DVD (lets say for this case - uncrypted DVD) and to broadcast it to another machine accross the net?\n\nTeam Xine - well, I use Xine for a lots of playback and I think it's a pretty good program, but it got something that I know many people did object for it - it got ffmpeg inside which remove the needs for some codecs that cannot officially be distributed (like wmv for example) - how will the KDE team react to an integration of it to KDE at all?\n\nAnother issue with Xine that it's written entire in C (if I'm not mistaken), and it could be a problem with some developers who wants a C++ framework...\n\nRegarding MPlayer - I know it's not GPL'd - but someone could talk to Arpi about it - he might be convinced to \"license\" a seperated copy to be GPL'd - they already plan to move to GPL later...\n\nComments, people?\n\nThanks,\nHetz"
    author: "Hetz Ben hamo"
  - subject: "Re: the teams.."
    date: 2002-01-29
    body: "Transcode:\n\nA) We plan to write a good C++ API for it, standing between the app and the external video system, just the same as with aRts.\n\nB) It could.\n\nC) Plugins.  Note that transcode itself is GPL, though.  KDE apps would be shielded from that by the API, but any codec plugins would have to abide by the transcode license.\n\nD) It could, given a broadcast export filter.\n\nXine:\n\nThere is no xine team.\n\nMPlayer:\n\nYou're free to be the mplayer team if you like."
    author: "Neil Stevens"
  - subject: "Re: the teams.."
    date: 2002-01-31
    body: ">Regarding MPlayer - I know it's not GPL'd - but someone could talk to Arpi \n>about it - he might be convinced to \"license\" a seperated copy to be GPL'd - \n>they already plan to move to GPL later...\n\nAccording to mplayer's developers themselves: \"It [mplayer's source distribution] contains several files with incompatible licenses especially on the redistribution clauses.\"  Even if he wanted to, he couldn't grant an exception.  They're working towards rewriting the non-GPL (or incompatible with GPL) parts, however, until that's done, the only way to make it GPL would be to strip out those parts"
    author: "Chad Kitching"
  - subject: "I don't understand \"ONE WILL BE CHOSEN\""
    date: 2002-01-29
    body: "I don't understand \"one will be chosen to be part of the KDE Multimedia standard.\". Do you mean that one solution will prevail and the other one will be scrapped ? If so, it will be a waste.\n\nTeam red assumption is that aRts could NOT BECOME a video framework and should be used only for audio. They found an alternative into \"Transcode\". Please, reconsider.\n\nA fruitful way of splitting the job would be that both teams (Red & Blue)work on a complementary approach (not a concurrent one).\n\nTeam Blue could provide:\n- a framework able to stream videos -> improvement of aRts\n- a player able to playback (& record ?) audio + video ->improvement of Noatun\n- a consistent video output for aRts -> the new \"libav\" library\n\nTeam Red could provide:\n- a collection of modules (import, filters, export)in the same way as Gstreamer but written in C++ -> the \"Transcode\" modules.\n\nTo be complementary, we need:\n- Transcode modules must be aRts aware\n- Trancode \"X ouptut\" module to be merged with the \"libav\" lbrary.\n\nThe problem is to make Transcode interfacable to aRts and the other way round. I don't know if this compromise is technically possible BUT it's what KDE need.\n\nIf we don't do it that way, we will have 2 frameworks (aRts & Transcode)not compatible. At the end of the day, a \"Standard\" will be chosen & the other party will be frustrated.\n\nI hope other users can give their view on this approach.\n\n"
    author: "Phil Ozen"
  - subject: "Re: I don't understand \"why all caps is bad\""
    date: 2002-01-29
    body: "You don't understand open source development.  If the developers want to work on seperate things, that is what they will do.  No one can stop them, and no one has the authority to tell them not to do it.  Open source might not be the most \"efficient\" way to do things, and some code might get \"wasted\" along the way, but you can't go around assigning people to do specific jobs when they are working on their own time for their own reasons.  Eventually, KDE will get video, and it will be good."
    author: "not me"
  - subject: "Re: I don't understand \"why all caps is bad\""
    date: 2002-01-30
    body: "Thanks \"not me\" for your explanation. Sorry, me not understanding Open source.\n\nNow, me understand that proposing is bad. Me not doing again. Me promise.\n\nMe also not using CAPS, never again. It's bad."
    author: "Phil Ozen"
  - subject: "Re: I don't understand \"why all caps is bad\""
    date: 2002-01-30
    body: "Don't be obnoxious. One of the most annoying things you can do is make fun of someone's nick and act like a troll when you find your opinions aren't universally accepted by people more involved in the project then you. When someone explains to you (in a netequittish way, I might add) that your ideas on what's going on are inaccurate, the proper response is to _find out more_, rather then complain that your proposal wasn't accepted.\n\nWhat you were doing was just proposing, but you were proposing based on an invalid idea: that KDE developers can just be assigned to task. They can't. Simple as that."
    author: "Carbon"
  - subject: "Re: I don't understand \"why all caps is bad\""
    date: 2002-01-30
    body: "Ok, my mistake. I got the point.\n\n"
    author: "Phil Ozen"
  - subject: "Re: I don't understand \"ONE WILL BE CHOSEN\""
    date: 2002-01-29
    body: "Transcode (we don't call ourselves red :-) developers don't think that artsd can't do video.  No, at least some of us just think it'd be inefficient to add to aRts, and rewrite parts of aRts, to add such functionality.  aRts is already a great audio system.  It's not broken, so why mess it up?\n\nInstead, we can take an existing video framework, and adapt it to interlock with aRts.   We can leverage the strengths of aRts, and still reuse the knowledge and code of existing video code and development.\n\nAs for libav, we already talked about using libav in a transcode video export plugin.  As for aRts awareness, we'd definitely use aRts for transcode audio output.\n\nEvery one of us in the transcode team likes aRts, I think.  No problem."
    author: "Neil Stevens"
  - subject: "whoops"
    date: 2002-01-29
    body: "your friendly editor got the \"team red\" thing from rob's email...  whoops. :)\n\n"
    author: "Navindra Umanee"
  - subject: "Re: whoops"
    date: 2002-01-30
    body: "I don't have the background to understand why \"team red\" looks bad... Could you explain ? Thanks."
    author: "Phil Ozen"
  - subject: "Re: I don't understand \"ONE WILL BE CHOSEN\""
    date: 2002-01-30
    body: "Thanks again for your explanations.\n\nNow, let's wait for the promising results. To what I've seen, Transcode seems already very advanced. To integrate it into KDE will be a giant step forward for video streaming.\n\nBy the way, the author of \"Transcode\" Thomas \u00d6streich is not part of the \"Transcode team\". Does that mean that there will be 2 versions of \"Transcode\", the same way as aRts does. The original version will be standalone & desktop independant. The other one will be KDE specific and managed by the \"Transcode team\". Thanks for your confirmation.\n"
    author: "Phil Ozen"
  - subject: "Re: I don't understand \"ONE WILL BE CHOSEN\""
    date: 2002-01-31
    body: "Well, we decided to try transcode before contacting him.\n\nWe don't plan on forking transcode, though."
    author: "Neil Stevens"
  - subject: "IO to video files"
    date: 2002-01-29
    body: "Hi!\nSome time ago, i've started writing a simple programm for converting video fileformats ( mainly to create Divx-Avi-files ). This has not been relasesd until now. However it includes a library for reading and writing to different A/V-Formats. It has a modular structure, so any IO to the files and de-/encoding of audio and video data is done by dynamicaly loaded modules.\nThis is written in C++, so i think the code could be usefull here. ( There are not very much codecs supported at this time, but the interface should be quite usefull now. )\nIf you are interested, you can get it here:\n  http://merkur.2y.net/av_convert"
    author: "Christian"
  - subject: "Why inventing the wheel a second time?"
    date: 2002-02-01
    body: "I don't know why people spent so much time on discussing how to integrate videoplayback in KDE. There are quite a few players around who do this fairly well. Some of them are quite mature software projects and evolving fast. Trying to catch up with these would be rather a waste of time.\n\nI'm thinking aspecially of the xine-project. And the best of all, it's availible as a library and it suports arts. All what's missing is a nice UI that integrates better in the KDE look and feel."
    author: "frieder"
  - subject: "User Opinion = Success / Devel Opinion = Flop "
    date: 2002-02-05
    body: "Many developers just want to do what *THEY* want. Things only that will satisfact themselves or a political thought. Normally End-users are very discriminated in KDE development and their opinions don't weight too much. "
    author: "DB"
---
The  <a href="http://dot.kde.org/1011042476/">aRts/KDE Video Roadmap Meeting</a> ending up having a fairly large turnout, peaking at 26 participants on the IRC channel.  There was so much turnout, in fact, that participants could not agree on a single line of strategy for video development in aRts and KDE.  Some wished for a successor to <a href="http://mpeglib.sourceforge.net/">mpeglib</a>, something that would work within <a href="http://www.arts-project.org/">aRts</a> but with the optimizations necessary for high framerate video.  Others saw no need for aRts itself to be extended for video, and proposed using one of two external video systems. Fortunately, there were volunteers willing to code for each approach, so development will commence on multiple paths concurrently.  In time, when the results are mature and ready for use by KDE, one will be chosen to be part of the <a href="http://multimedia.kde.org/">KDE Multimedia</a> standard.  Read on to find out more about the teams and the code already produced.
<!--break-->
<br><br>
The teams are:
<br><br>
<a href="http://lists.kde.org/?l=kde-multimedia&m=101208382517010&w=2">Team Blue</a>, which will work on <a href="http://lists.kde.org/?l=kde-multimedia&m=101209282332523&w=2">libav</a>, the successor to mpeglib, and on integrating video into aRts.  libav is in alpha status with version 0.1.0 available for <a href="http://mpeglib.sourceforge.net/download/">download</a>.  Members of Team Blue count Ryan Cumming, Matthias Kretz, Charles Samuels, Martin Vogt, Stefan Westerfeld.
<br><br>
<a href="http://lists.kde.org/?l=kde-multimedia&m=101208239114785&w=2">Team Red</a>, which will work with the <a href="http://www.theorie.physik.uni-goettingen.de/~ostreich/transcode/">Transcode system</a>, writing the code necessary for aRts audio integration and KDE application integration.  A kde-transcode mailing list is is available for <a href="mailto:kde-transcode-subscribe@lists.capsi.com">subscription</a>.
Team Red members include Rob Kaper, Neil Stevens, and Fabian Wolf.
<br><br>
<a href="http://lists.kde.org/?l=kde-multimedia&m=101208778724170&w=2">Team Xine</a>, which is considering integrating <a href="http://xine.sourceforge.net/">Xine</a> with KDE.  Proponents include Daniel Molkentin and Ewald Snel.  Ewald Snel has already written <a href="http://rambo.its.tudelft.nl/~ewald/xine/">an aRts output plugin</a> for Xine.
<br><br>
May the best Team win! 