---
title: "Kernel Cousin KDE #42 Is Out"
date:    2002-08-01
authors:
  - "aheimburg"
slug:    kernel-cousin-kde-42-out
comments:
  - subject: "transparent kicker"
    date: 2002-08-01
    body: "the guy who did this kicker-hack said that there won't be real transparency until X supports it (makes sense to me :).\n\nI once heard that the mighty keith packard is working on a transparency server for X (He's also written a paper about it). Is that right? If yes, does anyone know anything about its current status?\n\nAnother question, a little weird maybe... It seems other operating systems are working towards using 3d-features for window-management, so that moving, resizing and making transparent and other things need less cpu and work smoother (windows will probably do so in their next version, I think it was called longhorn, not sure). Has anyone thought about doing that for linux/x/qt/kde? How hard would it be to do that?\n\nthanks!"
    author: "me"
  - subject: "Re: transparent kicker"
    date: 2002-08-01
    body: "It would be really nice if there is a \"kernel cousin XFree86\" or something. I can't really tell what the status of XFree86's development is just by reading the mailing lists."
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "This is a really great idea!\n\nI really wish that someone would do this (im not qualified, ok?)"
    author: "isNaN"
  - subject: "Re: transparent kicker"
    date: 2002-08-01
    body: "I believe Enlightenment was destined to use 3D hardware. Not sure if thats still the case though, especially since Raster seems to think desktops on Linux are a waste of time."
    author: "Psiren"
  - subject: "Re: transparent kicker"
    date: 2002-08-01
    body: "Windows has been using the graphic card's hardware for accelerating the drawing speed for ages, at least since Windows 3.1 - at least 2D stuff like a hardware-accelerated mousecursor or moving rectangles (read: windows) around the desktop. Guess why Matrox' Mystique and Millenium cards were/are so popular."
    author: "joe"
  - subject: "Re: transparent kicker"
    date: 2002-08-01
    body: "hum, that story about transparency server for X is very old .See that discussion(12/29/2001): http://www.kde-look.org/news/news.php?id=22 for example, but I can find very much old. I'm afraid that's just vaporware :(("
    author: "Capit Igloo"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "That's right. Transparency server IS vaporware :( Keith Packard even wrote a paper about that 2 years ago (http://www.xfree86.org/~keithp/talks/KeithPackardAls2000/index.html), but where are the results ? Actually, XFree86 is totally obsolete. That's why Windows will win at the end, unless the major distributions switch to a less obsolete system like DirectFB."
    author: "disillusioned linux user"
  - subject: "Re: transparent kicker"
    date: 2002-08-03
    body: "those are some strong assertions, unfortunately you were light on the supporting information. i've been to the directFB site, but they too are light on details.\n\nfor those of us who are interested, can you provide a link to a point-by-point comparison between XFree86 and DirectFB when it comes to the following issues: speed, memory, hardware support (video cards, mice, etc), operating system support, network support, legacy application support, difficulty of porting from X11 to DirectFB (assuming there is any), etc...?\n\ncan you also describe why XFree86 is \"totally obsolete\"? (again in technical terms as i'm not really interested in sycophantic rhetoric)\n\ncan you also back your assertion as to how the current usage of XFree86 will cause MS Windows to \"win\" in the end?\n\nthanks... "
    author: "Aaron J. Seigo"
  - subject: "Re: transparent kicker"
    date: 2002-08-03
    body: "I'd also suggest the following: security; does DirectFB require all apps to run as root? Or does it require all apps to have direct access to H/W (which provides a potential plenty of local DoS possibilities?)"
    author: "Sad Eagle"
  - subject: "Re: transparent kicker"
    date: 2002-08-03
    body: "> can you also back your assertion as to how the current usage of XFree86 will \n> cause MS Windows to \"win\" in the end?\n\nIt won't. Windows didn't lose in the first place. ;-)"
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2002-08-04
    body: "I'm not the original poster but I'm probably the person who should answer in the first place so if you don't mind I'll reply answering some of your and others questions about DirectFB - \n1) speed - the short answer DirectFB wins since all operations are performed directly on the framebuffer device (you need a kernel compiled with a framebuffer device support)\n2) hardware support - now here's the problem: 1 should give DirectFB an edge as far as all drawing operations go, but the hardware support is very poor and drivers are available for a very limited set of cards, all others use a vesa driver and it just doesn't work all that well (read as - it's slow)\n3) operating system support - DirectFB is developed mainly by developers working for a company specializing in video related technologies working on linux, so I could imagine os support is somewhat limited,\n4) network support - it renders through framebuffer, so no not really\n5) legacy application support - zero (well unless you want to count one image viewer)\n6) difficulty of porting from X11 to DirectFB - trivial and impossible at the same time. This is a great question and also the reason why DirectFB isn't used anywhere right now. All applications working under X11 can work on DirectFB thanks to XDirectFB. So the transition isn't the problem (assuming you have a supported video card and the chances for that are very slim). We can switch at any point and get transparency. But what then? You login, everything is painfully slow, you make two or three windows transparent, take a screenshot to show your friends and logout. Transparency in itself is useless, having multiple transparent windows opened doesn't make you more productive. What would be required to make DirectFB work is a native toolkit (think Aqua) that is capable of using the power of the underlying system. Oh, and no I don't mean Gtk+-FB which is a straight Gtk+ port to a DirectFB. I'm talking about a fully native toolkit - one that isn't limited by capabilities of other graphics api's. A toolkit that makes vector manipulations on windows trivial. And here's our problem - we just can't take Qt and port it to DirectFB - it wouldn't give us anything. All those comments about how DirectFB is the savior are simply not true, they're just naive. One of my biggest problems with DirectFB (which I think was also one of the questions) is that all applications have to be run as root. To use the power of DirectFB a completely redesigned toolkit is required. Then and only then one could see effects similar to the ones Quarts/Aqua give on Mac OS X. So, switching to DirectFB would be a waste of time for us (unless we'll all agree that we do want to sacrifice speed and stability of our systems for transparent windows). We would have to design a completely new toolkit and base a new desktop environment on it. Everything that at this point is available through DirectFB would be possible through extensions in XFree86 (and other X11R6 conforming Xservers in fact). People thought AA fonts would be impossible in X11 because of the way XFontStruct was handled within, Keith proved them wrong with XRender extension. We can prove them wrong again (it's just that the whole 'getting money to buy food' and 'not enough companies willing to pay to work fulltime on stuff that won't bring them profits' presents a little time problem :) )"
    author: "Zack Rusin"
  - subject: "Re: transparent kicker"
    date: 2002-08-04
    body: "> 1) speed - the short answer DirectFB wins since all operations are performed \n> directly on the framebuffer device (you need a kernel compiled with a \n> framebuffer device support)\n\nIs this an issue at all? I mean, how much time does it take for a 1 Ghz CPU to transfer a few bytes to the X server through a socket? (pixmaps are transferred using shared memory) Nobody runs KDE on a 486."
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2002-08-04
    body: "1) Open a window.\n2) Click on a frame.\n3) Do a quick drag accross your viewport.\nHow fluent was it?"
    author: "Zack Rusin"
  - subject: "Re: transparent kicker"
    date: 2002-08-04
    body: "about as fluent as Rain Man in danger of missing Wopner. =P\n\nand not all of us run (or will be running in the near future) KDE on 1GHz processors with half a gig of RAM. many still have and use 266MHz - 400Mhz systems."
    author: "Aaron J. Seigo"
  - subject: "Re: transparent kicker"
    date: 2002-08-09
    body: "This is not because X is slow, this is because of the communication between the client and the window manager (or something like that; I read it a while ago).\nStart Opera or QT Designer or any window-in-window app. Drag the title bar of the windows inside the window, and behold how smooth that goes! And my CPU monitor tells me it didn't take a lot of CPU power.\n\nX is not slow."
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2002-08-09
    body: "Oh BTW, this was tested on my old Pentium 233 with 48 MB RAM, a half year ago, on XFree86 3.3.6."
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2002-08-10
    body: "Wow, very impressive, now who would've thought... Now if DirectFB or Quartz people knew what you know it would've saved them a lot of work. Go on their lists and tell them that they can use X11 since you checked it and MDI applications don't flicker in X11 while draging their child windows. And I'm sure you know what happens underneath because you wouldn't be posting this, right? First of all no one said that 'X is slow' it's something you made up to strenghten your argument. Now, I'm sure you know that the fact of MDI application not flickering during movement of their child windows is due to the fact that X interaction with those windows is limited to redrawing the contents of the parent window. In the same sense that Emacs doesn't flicker while you're writting letters, MDI application windows don't flicker when you drag windows inside of them. Menaging windows is a completely different animals, X11 queues request from WM and sends them few at a time. These queues slow down processing of data when all request are coming from the local machine. In that sense DirectFB model of rendering to FB directly beats X11 hands down. And yes X11 is slower then Quartz, DirectFB or Windows GDI (or whatever it's called), and yes it will become an issue once we'd like to do any of the effects Aqua does. It doesn't mean that we couldn't solve them (Rasterman did without even touching X11) but it sure as hell will be an issue sooner or later."
    author: "Zack Rusin"
  - subject: "Re: transparent kicker"
    date: 2002-08-03
    body: "Lack of transparency doesn't make a windowing system fail! Transparency/translucent windows/shadows are only *eyecandy*. If you just want something productive (which is what 90% of the users want), then X is sufficient."
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2004-05-21
    body: "And 640k memory is more than we ever need...\n"
    author: "Bosson"
  - subject: "Re: transparent kicker"
    date: 2004-05-21
    body: "Seriously, X11 needs a lot more to survive as holistic window-manager:\n\n* Serious event compressions and priorities (at least for redraw)\n* Backstore and optional save-under that works (perhaps as a cache?)\n* Transparency!!! (as more than save-under)\n\nThis can be done using direct-fb and then implementing X11 on top, plus som extra extensions. "
    author: "Bosson"
  - subject: "Re: transparent kicker"
    date: 2002-08-01
    body: "I like the idea of opengl (or something using hardware 3d) for desktop rendering, it seems much more future orientated than the project using the frame buffer.  Projects like berlin had this right but its lack of backward compatibility and developer support is a problem.  Would it be possible for a new version of X to exist that does this?  Maybe call it X12, dump older the stuff from x that is not commonly used, and add the new stuff to it.  Try for being compatible with something like 80% of the present apps.  Personally having features like real transparency, xrender, color management, a new font setup, and better hardware acceleration would outweigh the temporary loss of compatibility.  Since this is open source the apps people use the most will be updated fairly quickly.  The way I see it, as long as it is clear what the new standard will be people will follow it.  The transition from kde1 to kde2 was not easy, many programs got left behind, but in the end we are better off for it."
    author: "lupinar"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "I cannot understand the backward compatibility complains whenever a new windowing system i discussed.\nLook at MacOSX: you can run XFree86 or any other X-Server there and the windows behave like native/local apps.\n"
    author: "Lenny"
  - subject: "Re: transparent kicker"
    date: 2002-08-03
    body: "Then what about games? Using OpenGL for rendering your desktop eats away precious video memory."
    author: "Stof"
  - subject: "Re: transparent kicker"
    date: 2002-08-05
    body: "I can only think of one graphics server that does use OpenGL: Quartz Extreme. Unless while playing games you constantly move windows, iconify them, maximize them, close them etc., you shouldn't have a problem."
    author: "Rajan Rishyakaran"
  - subject: "Fresco"
    date: 2002-08-05
    body: "The reason why Fresco doesn't make it easy for X11 apps to run on Fresco is that Fresco wants to get rid of one of the most major problems with X Window System - lack of consistency between application. \"What do you mean by lack of consistency?\" Open GIMP, open Kontour, compare the look. And the UI itself.\n\nBesides, we are fighting for the desktop, if we were really interested in X11 apps, why not just clone Windows in the first place. You get more apps that way.\n\nMost Fresco guys agree with should just go down the Mac OS X route, using XDarwin to load X11 apps. Maybe we would make something that works like Classic to run X11 apps.\n\nThere probably be someone to write an implementation of Qt on Fresco as soon as some developers work on Moscow and get sufficient amount of classes and widgets. KDE Libs would be harder, since Fresco pushes CORBA, plus it relies on many X11 stuff."
    author: "Rajan Rishyakaran"
  - subject: "Re: transparent kicker"
    date: 2002-08-01
    body: "Probably I'm rather looking at DirectFB because they seem to make more progress unfortunatly. See this screenshot for drooling:\nhttp://directfb.org/screenshots/XDirectFB-MultiApp.png\n\nThey already have a XDirectFB server which allows for such things as transparency. Their multi application core will allow you to run native DirectFB apps (which could already be basically every Gtk app as Gdk has DirectFB as an optional target) besides a XDirectFB server to load \"legacy\" X applications.\nMeanwhile you could just use XDirectFB as a X replacement, this is still a bit buggy though and you won't gain a lot from this besides inactive windows beeing translucent and a cool cursor. This would look like this:\nhttp://directfb.org/screenshots/XDirectFB-Rootless.png\n\nThe biggest problem with DirectFB though is of course hardware support. Matrox works pretty well but I couldn't RIVA driver to work with my Geforce 4 yet so I have to use VESA and that's pretty slow and I have to use a low resolution."
    author: "Spark"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "To me directfb + xdirectfb is what we should have been using 5 years ago.  Today we should be switching from the old directfb to one that uses the 3d hardware for acceleration.  With macosx using opengl for the gui now, and windows longhorn to do it as well 2d acceleration's days are numbered.  DirectFBGL looks like a step in the right direction, but once it is working well they should start moving the basic drawing functions (Rectangle filling/drawing, Triangle filling/drawing, Line drawing, ...) to use it as well."
    author: "theorz"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "One step after another. :) Longhorn won't come out before 2005 (and as we all know software release dates, probably later). It's not that we are close to seeing the end of 2D acceleration. Currently there wouldn't be much gain, even with fast cards. Sure there would be a gain with a fast Geforce 4 but it wouldn't allow for completely new fast interfaces and for current interfaces 2D acceleration is already pretty fast (like the Windows 2000 GUI, IMO it's very fast). \nSo having DirectFB be alive and kicking in the next few years (biggest problem beeing compatibility to FreeBSD, etc) would already be awesome. "
    author: "Spark"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "well... as I understand it, 2D acceleration is not very advanced and for making cool transparency effects the 3D hardware would be more suitable. Check Evas for openGL accelerated 2D graphics. btw. can you say antialias!!!! :)\n\nOf course if someone will not blow the idea, by adding too much vector objects and useless stuff like lights, bumpmapping.  But maybe by the time it will be done, we all will be running atleast GeForce5 and simple stuff like that would not ruin our fps :)\n\n\"Hey i just overclocked my GeForce, my windowmanager is running more smoothly now - i can kill my windows faster than ever\""
    author: "montz"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "QT embedded isn't running with directFB?"
    author: "tofu"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "AFAIK no, it's running directly on the Linux FB but not using DirectFB. I think it could be ported though. I remember a message to the DirectFB list from someone with a @trolltech.com email but who knows. :)"
    author: "Spark"
  - subject: "Re: transparent kicker"
    date: 2002-08-02
    body: "This is an interesting paper to read:\n\nhttp://www.opengl.org/developers/code/features/siggraph2002_bof/sg2002bof_apple.pdf"
    author: "Janne"
  - subject: "Re: transparent kicker"
    date: 2002-08-04
    body: "Thanks, very interesting."
    author: "Zack Rusin"
  - subject: "filter for latex"
    date: 2002-08-01
    body: "A filter for latex would be great for me. Lyx is great for writing docs, but kword would give me better control for finetuning the final. looks like it is planned. Looking forward to it."
    author: "Rithvik"
  - subject: "Kivio improvements"
    date: 2002-08-01
    body: "Hello,\n\nI don't know if this is being planned, but what would be really nice in kivio is the ability to rotate and scale stencils.  Well, you can kinda scale now, but I was thinking of a more formal approach than just grabbing a stencil handle and dragging.  One where the user can specify exact sizes.\n\nAnother feature that would be nice in the long run would be the ability to embed flow charts.  For example, save an entire flow chart as an individual stencil so it could be used in other flow charts.  Then have the ability to click on that stencil and have the underlying flow chart pop up.  This would make logic and data flow diagrams really easy to do.  \n\nLastly, I thought the undo-redo stuff for kivio was done in the HEAD CVS.  \n\nCheers,\nJesse"
    author: "Biz"
  - subject: "Re: Kivio improvements"
    date: 2002-08-02
    body: ">Another feature that would be nice in the long run would be the ability to embed flow charts. For example, save an entire flow chart as an individual\n>stencil so it could be used in other flow charts. Then have the ability to\n> click on that stencil and have the underlying flow chart pop up. This \n> would make logic and data flow diagrams really easy to do. \n\nI have alvays thougt this to be a great idea, but why stop there. Make it possible to embed anything, like cliking on a stencile you get some design description in a KWord dokument, another pops upp a class definition .h file in a kate part etc. Make it so you can embed both external files be it kivio files or whatever or truly embed them in the kvio dokument zip file. This I think will  make Kivio a extremly good design/dukumentation tool, and outshine the other charting programs. \n\nAnd besides most of the embeding stuff already exist in KOffice:)\n\n"
    author: "Morty"
  - subject: "KOffice XML formats"
    date: 2002-08-02
    body: "I think more people would use KOffice if it used the same XML formats that OpenOffice.org uses internally (not just an import/export filter). I'd MUCH rather use KOffice (as I think it is more enjoyable to use than OOo) to store my files, but having compatibility with other platforms is a must in most environments. I know there were some big debates on this, but I doubt it is something that will go away any time soon. I'd rather see energy focused on 100% OpenOffice.org/StarOffice compatibility than 70% Word/40% WordPerfect, etc. I cannot offer C++ skills to do this, but with enough interest from others, I would be willing to donate some $$$. I think this a worthy cause. Any thoughts?\n\nCheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: KOffice XML formats"
    date: 2002-08-03
    body: "the german gov is funding research into a common XML-based word processor file format. if they are successful (the process is still ongoing) we can most likely expect to see it implemented in the major open source word processors, possibly with some of that work also being sponsered. this is some ways off yet, but if things work out it should be an even better solution than picking one format as a defacto \"standard\" based on popularity of the software alone."
    author: "Aaron J. Seigo"
  - subject: "Re: KOffice XML formats"
    date: 2002-08-04
    body: "Hmm...I'm a bit skeptical that the German govt. could come up with a better solution than hundreds of hackers, most with extensive background in office application development, in developing an XML format for documents. Besides, if what you said was true, it would only just cover word processing files, whereas OpenOffice.org has a impressively comprehensive XML solution for word processing, spreadsheets, presentations, and illustrations. All things that could map to the KOffice equivalent. And if such a solution from a third party came about, great. But as of now, the spec is there, it's proven in the field, so could be used right away (we all know how long govt. research can take).\n\nThanks for the info, tho'...\n\nP.S.: is the KOffice XML formats documented anywhere? I'd like to compare the two.\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: KOffice XML formats"
    date: 2002-08-07
    body: "The German governement isn't actually designing a file format.\nRather laying out the grounds so that one can be designed, if possible by the KOffice/Abi/OO developers. But most open-source developers don't do things that others want for free ;)\n\nYes the KOffice XML format is documented. See e.g. http://www.koffice.org/DTD/kword-1.2.dtd\n\nI do agree though that the openoffice XML looks very nice, and I'm among the ones who support the idea of moving to that file format in the future, as the native one. But that's a big change, and lots of work... Volunteers?"
    author: "David Faure"
  - subject: "Re: KOffice XML formats"
    date: 2003-01-23
    body: ";-) Do not worry, Eron, it is not Gerhard Schroeder and Joschka Fischer who are working on this standard format. They hired professionals to do the job, because they are rather busy doing other stuff. I believe a professional computer engineer is at least as able as a hacker for defining standards. In fact, defining standards is a job that is more suitable to an engineer than to a hacker, I think. But some people are both, why not?..."
    author: "Guido"
  - subject: "Re: KOffice XML formats"
    date: 2003-01-23
    body: "Wow, this is quite an old thread! :-)\n\nI'm not concerned about *who* is working on it, just the fact that a high-quality specification already exists, in the form of the OpenOffice XML formats, so Germany doesn't have to re-invent the wheel. They could just move the entire German govt. to this format and help to fund its further development. Also, OASIS several months ago initiated a coalition to do just this, with Sun Microsystems and Corel already on board. Check it out here: http://www.oasis-open.org/news/oasis_news_11_20_02.shtml\n\nCheers,\n\nEron"
    author: "Eron Lloyd"
  - subject: "Re: KOffice XML formats"
    date: 2003-01-24
    body: "It is an old thread, indeed. I found it while I was looking for information about compatibility between OOo and KOffice.\nAnd That's also when I learned about this project of Germany. Before, I thought they were simply moving to OOo. Well, let's hope they will co-work with the coallition you're talking about. After all, having one (open) standard is what we all want, isn't it?\n\nBest regards,\n\nGuido.\n\n"
    author: "Guido"
  - subject: "Re: KOffice XML formats"
    date: 2004-07-29
    body: "I didn't even find it and I found it old. Thank you google."
    author: "Lance"
  - subject: "Re: KOffice XML formats"
    date: 2004-07-30
    body: "Instead of answering to the old thread, perhaps I should have answered only to your post. :-)\n\nWere you searching something specific? If the answer is not somewhere on http://www.koffice.org , perhaps I could still help you nevertheless.\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice XML formats"
    date: 2004-07-29
    body: "If I remember well, one of the wishes of the German Goverment was a storability of 60 years. That is not something that you can get out of a hat like a rabbit. (You do not only need to store but also to think how you are going to read in 60 years. 60 years ago digital computers were in infancy.)\n\nHave a nice day!"
    author: "Nicolas Goutte"
  - subject: "Re: KOffice XML formats"
    date: 2004-07-29
    body: "There is the OASIS open office specification and that is what we, in KOffice, are heading too.\n\nHave a nice day!"
    author: "Nicolas Goutte"
---
<a href="http://kt.zork.net/kde/kde20020731_42.html">Kernel Cousin KDE #42</a> has hit the virtual shelves. Juergen Appel talks about <a href="http://www.koffice.org/filters/status.phtml">KOffice filter</a> improvements, a Lyrics plugin for <a href="http://noatun.kde.org/">Noatun</a>, a new Kivio developer, a transparent Kicker (<a href="http://vortex.bd.psu.edu/~mus11/img/kicker1.png">1</a>,<a href="http://vortex.bd.psu.edu/~mus11/img/kicker2.png">2</a>,<a href="http://vortex.bd.psu.edu/~mus11/img/kicker3.png">3</a>), <a href="http://crl.research.compaq.com/projects/mercury/">voice synthesization</a> and much more.
<!--break-->
