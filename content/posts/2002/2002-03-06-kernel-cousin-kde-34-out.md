---
title: "Kernel Cousin KDE #34 is Out"
date:    2002-03-06
authors:
  - "numanee"
slug:    kernel-cousin-kde-34-out
comments:
  - subject: "KDE 3.0: no Crystal icon theme :-("
    date: 2002-03-06
    body: "Unfortunately the Crystal icon theme won't make it into KDE 3.0.\nEveraldo told me that he doesn't consider Crystal to be ready and that he wouldn't like to see incomplete stuff in kde 3. He'd rather prefer to finish the icontheme and include it with KDE 3.1.\n<br><br>\nJudging from the tarball he sent to me I can guarantee all fan's of everaldo's crystal-theme that it will be very well worth the wait. :-)\n<br><br>\nThe good news is that several excellent icon-themes will be shipped with KDE 3.0: Kristof Borrey's 'IKons'-theme and AmiBug's 'Slick'-theme are the most impressive ones.\n<br><br>\nAlso many great wallpapers made it into KDE 3.0\n<br><br>\nLooking at the latest submissions on <a href=\"http://www.kde-look.org\">http://www.kde-look.org</a> (search for: \"Kubical -Perspektiva\" e.g.) I'm very sure that KDE 3.1 will even be a larger and more striking step in terms of artwork.\nI'd like to thank all artists who have worked on KDE so far (most notably the qerson that always refuses to be credited ;-) . In addition I'd like to thank Frank Karlitschek who did a fantastic job by founding and maintaining KDE-Look (<a href=\"http://www.kde-look.org\">http://www.kde-look.org</a>)! Keep up the great work!\n<br><br>\nGreetings,<br>\nTackat\n\n\n \n"
    author: "Tackat"
  - subject: "Re: KDE 3.0: no Crystal icon theme :-("
    date: 2002-03-06
    body: "> In addition I'd like to thank Frank Karlitschek\n\nThanks Frank!  Finally, a name behind the famous site.  What an excellent job, KDE-Look.org was so sorely needed and has successfully filled in an important niche in the KDE community.\n\n-N.\n\nPS Yes, and thanks to the qerson who also happened to design our logo...  :-)\n"
    author: "Navindra Umanee"
  - subject: "Re: KDE 3.0: no Crystal icon theme :-("
    date: 2002-03-06
    body: "Speaking of art in KDE 3.0, what about the wallpapers that shipped with KDE1?  Sometime before KDE2 they got shipped to the Attic (they're still there in CVS).  Why shouldn't those be included?\n\nIf you don't know what I'm talking about, I'll post a list.\n\nHint: indigo_paint.jpg is one of them."
    author: "David Walser"
  - subject: "Re: KDE 3.0: no Crystal icon theme :-("
    date: 2002-03-06
    body: "Browsing through the icons for crystal, I started thinking about the icon for core-dumps. Why is it a bomb? I, as a developer myself, can understand this, but I think an ordinary user may be scared of it. Imagine that suddenly a new icon appears apparently without any interaction from you, and it is a BOMB!!! Virus alert!! Virus alert!!! Maybe it should be something less dramatic.\n"
    author: "Meretrix"
  - subject: "Re: KDE 3.0: no Crystal icon theme :-("
    date: 2002-03-06
    body: "On the other hand, a non-dev user has no use for cores, so they would just want to delete them all (which would be a likely action if they thought the core might be \"dangerous\"...)"
    author: "Jason"
  - subject: "User systems don't produce core files"
    date: 2002-03-07
    body: "SuSE does not create core files by default."
    author: "Moritz Moeller-Herrmann"
  - subject: "Re: KDE 3.0: no Crystal icon theme :-("
    date: 2002-03-10
    body: "how about a pile of rubbish, or broken glass?<br>\nor maybe just binary data, \n ___________ \n|010101110| \\\n|010100101|__\\\n|010101011000|\n|100101010100|\n|101001010101|\n|010101110100| \n|100101010101|\n|101010100010|\n ''''''''''''\nthat would be more appropriate ;o)\nmaybe kandalf could give you an introduction about those\nnasty files...<br>\nor maybe include it in the \"what's this?\" help feature."
    author: "smoke"
  - subject: "Almost there"
    date: 2002-03-07
    body: "Everaldo's icons are really, really promising. They are so good that IMHO they can be used to bring KDE over the last remaining 15% which is lacking compared to the professionality of Windows and Mac. \n\nThis could done if Everaldo designed kicker (including a new application launcher button), a theme, and the illustrating graphics into a single coherent and simplified design for KDE - based on this style.\n\nNow if just people would turn off underlining before they make those screenshots. Something dies in me each time I see it turned on....\n\n"
    author: "will"
  - subject: "FreeKDE.org and trolls?"
    date: 2002-03-06
    body: "I don't remember any trolls on freekde.org, certainly not the same amounts as have been here.. so I wonder why it was concluded that forced registration was not an effective measure."
    author: "Rob Kaper"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-06
    body: "Consider that freekde.org had almost no comments at all, and that amongst those comments there were still a number of trolls, at least if I remember correctly.  \n\nSo while it's true that registration stopped comments, in my opinion it stopped as many good comments as it did bad (which is OK for freekde.org).  Furthermore, look at Slashdot, where trolls come mostly from registered users, regardless of the fact that Slashdot allows anonymous posting.\n\nNote, none of this is meant as a criticism of freekde.org.  Freekde.org has a different philosophy and different goals to the dot, and I respect that.  freekde.org is a site that I quite enjoyed and had easy access to on my bookmark toolbar.  I'll be glad when it's back.\n\nBesides, registration sucks. :) If you really want to be registered, please send us your photo, credit card number, SIN number, home address, email address, expected IP addresses,...  ah nevermind.  :-p\n\nAnd come on, it's been quite void of trolls here on the dot, all considered.  I honestly think things are going well, but feel free to disagree -- anonymously if you want. :-)"
    author: "Navindra Umanee"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-06
    body: "Can't I just troll instead, as usual? ;-)"
    author: "Rob Kaper"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-06
    body: "Eeek! What, you made me write that up for nothing? ;-)\n\n"
    author: "Navindra Umanee"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-06
    body: "Rob:  The reason they're not doing it is that they have a different tactic:  Vigorous deleting of posts.  Whether the posts get deleted enough is aigood followup question to ask. :-)\n\nNavindra:  Merely requiring an email address is enough to regulate this stuff; there is no need for proof of ID.  You can set up limits by domain, and this should be effective because it requires the troublemaker to spend money to get more accounts."
    author: "Neil Stevens"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-06
    body: "I know what you meant, but just as a factual clarification, it is quite a rare event for a post to be deleted. Of course, once you are branded as a troll, anything goes.  \n\nWe obviously need a FAQ for this site.  I've had one in mind for a while just never got to it.\n\nAlso, certainly feel quite free to send us your email addresses and domains... I'll make a web form if there's a demand.  :p\n"
    author: "Navindra Umanee"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-07
    body: "There *is* a demand - just make it a requirement. :-)"
    author: "Neil Stevens"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-07
    body: "r-r-right... because hotmail and yahoo freemail accounts are both expensive and difficult to set up ....\n\nbesides, i'm sure that setting up dozens of fake email addresses is probably one of the first things a career troll does."
    author: "Aaron J. Seigo"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-07
    body: "Way to show your lack of reading comprension."
    author: "Neil Stevens"
  - subject: "Re: FreeKDE.org and trolls?"
    date: 2002-03-07
    body: "The word \"domain\", as you used it, was ambiguous. I assumed you meant domain as found in the email address used to register. I didn't consider that you might have meant something like \"the domain/IP address of system the visitor is using\", because that's equally stupid. Think of firewall/proxying issues, changing IPs (dial-up / dhcp), having more than one system available, and that it doesn't require registration to track that information in the first place, etc... So I can't imagine that is what you meant. What did you mean by \"limits by domain\"?"
    author: "Aaron J. Seigo"
  - subject: "Color me nifty"
    date: 2002-03-06
    body: "Karbon... well, I'm honored :-)\n\nHmm, what does it do that KIllustrator can't, though? I'm saying this out of curiousity, since I really don't know much about vector graphics (bitmapping is more my style)"
    author: "Carbon"
  - subject: "Re: Color me nifty"
    date: 2002-03-06
    body: "you mean kontour? ;-) read the kc ... someone else asked that exact question on the mailing list and got an answer ..."
    author: "Sun Tzu"
  - subject: "Pictures of Crystal ?"
    date: 2002-03-06
    body: "Could someone please point me in the right direction for some pictures of the Crystal theme?\n\nI can only seem to get to the icons, nothing else.."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: Pictures of Crystal ?"
    date: 2002-03-06
    body: "One day before the article was sent online, all of everaldo's artwork was removed, i was told. However, there is a small box down left on KDE-look,\ncalled advanced, search for everaldo. You wont find the noatun skin and the wallpaper, because they are *still* removed, but you'll find everaldo's latest screenshot.."
    author: "Juergen Appel"
  - subject: "pictres of crystal"
    date: 2005-09-19
    body: "crystal ismy mom i want to see pictres of her she has red hair and my name is madison moore i hwr little girl."
    author: "madison moore"
  - subject: "Re: pictres of crystal"
    date: 2007-12-11
    body: "I know a crystal moore with red hair who used to live in columbus and now lives near amanda, if you think this could be her write me back and I will give you lots of information. Thanks and good luck on your search."
    author: "dustin"
  - subject: "thoughts"
    date: 2002-03-06
    body: "I would like to know when is going to be implemented the KMetaStyle engine for KDE 3.x .I think, icons themes, as Ikons and Crystal, or kde themes, as Liquid,Aqua and so on, must be into a whole KMetaStyle engine, whith theirs icons, sounds, colors, etc.. to be profesional.\n\nAnother thing: Are there roadmaps to implement into Arts (to use with Noatun) things like Divx (OpenDivx libraries),mpeg and so on?\n\nAnd the last :-) : when will be KDE compiled with gcc 3.x, the new ld (better start ups) and the ObjPrelink hack? :-)\n\nThey are my wishes :-)\n\nGood work KDE team!! \n\n"
    author: "Daniel"
  - subject: "Re: thoughts"
    date: 2002-03-06
    body: "I have heard romours that KDE compiles with GCC devel, and it will be ok by GCC3.1 Hopefully it will arrive soon, although I have no idea..."
    author: "Me"
  - subject: "Hmmm..."
    date: 2002-03-07
    body: "> Robert Jacolin [*] asked, \"what is the difference with kontour ?\" Lenny answered, \"The most visible difference is that the former killustrator/kontour was based on shape primitives, while karbon is path-based like illustrator and freehand. But most important: kontour and karbon have a somewhat different culture/background. It's not easy to describe but it matters a lot for a hobby-project. Think of kde/gnome but less dramatic. I think it's like evolution: either kontour and karbon are so similar that they can merge, or they are so different that they have the right to co-exist. In the end it's about having fun and learning new stuff.\"\n\n> Q) Is it ready?\n> A) No. But even in its pre-alpha stage it is already fun to use. \n\nNot easy to describe, less dramatic... Is Koffice having no more priority than fun and learning new stuff ?\n\nKSpread, Krayon, Kivio seem always waiting developpers. Is Koffice becoming Fun Office ?"
    author: "Alain"
  - subject: "Re: Hmmm..."
    date: 2002-03-07
    body: "Wy don't you join the Koffice denvelop theam and denvelop one KSpread, Krayon, Kivo Koffice need more denvelopers"
    author: "Dr88Dr88"
  - subject: "Re: Hmmm..."
    date: 2002-03-07
    body: "> Not easy to describe, less dramatic... Is Koffice having no more\n> priority than fun and learning new stuff ?\n\n> KSpread, Krayon, Kivio seem always waiting developpers. Is\n> Koffice becoming Fun Office ?\n\nOK, so you're saying Lenny, who in his spare time is contributing to a project which very well could help us all, should, instead of doing the project he wants, finish someone else's project, which he does not want to do, b/c . . . you want him to?\n\nHello, get a clue, be thankful he is working on his project, and work on Kontour, Krayon, KSpread and Kivio yourself instead of criticizing others' contributions."
    author: "Sage"
  - subject: "Re: Hmmm..."
    date: 2002-03-07
    body: ">  Hello, get a clue, be thankful he is working on his project\n\nI didn't say that working on this project was a bad thing, I only want to say that I feel it is not a good thing for the credibility of KOffice to acquire yet another vector graphics program, and in pre-alpha stage. It is yet another no usable application in KOffice...\n\nI would have prefer that Kontour stay the only one \"vector graphic program\" of KDE, until Karbon become better or complementary, at first usable, not in alpha or beta stage.\n\n> and work on Kontour, Krayon, KSpread and Kivio yourself instead of criticizing others' contributions.\n\nI am ready to find bugs and lacks in KSpread, Krayon and Kivio, but the old bugs and lacks are always here.\n\nPerhaps you, developpers, don't like critics, but it may be a good thing to listen users saying that KOffice is not going in the direction of a all usable office suite."
    author: "Alain"
  - subject: "Re: Hmmm..."
    date: 2002-03-08
    body: ">Perhaps you, developpers, don't like critics, but it may be a good thing to listen users saying that KOffice is not going in the direction of a all usable office suite.\n\nI'm not a KDE developer myself, but I am an OSS developer on a few multi-developer projects, and I'm going to guess here (correct me if I'm wrong) that you aren't a KOffice developer yourself.\n\nReporting bugs does not make you a developer, it makes you a helpful user, unless you go about patching those bugs. Nor does criticism make you a developer, or in most cases even a helpful user, as I've learned from both perspectives (\"Not going in the direction of a usable office suite\"? \"Until it becomes better or complimentary\"? What do you mean, exactly?) Since I doubt you know any of the KOffice developers personally, or are involved in actual development discussion in any way, it seems rather brash of you go to about telling the developers what to do (in broad, ambigous statements) without being particularly involved in the project."
    author: "Carbon"
  - subject: "Re: Hmmm..."
    date: 2002-03-08
    body: "Is the entry of this site restricted to the developers ? \nIs it necessary to \"know personally\" any developer ?\nI am just a KDE user, (even a little more, but being a simple user is enough) I think that this site is a way to dialog, including saying what users are waiting. I do it in a positive way.\nDo you want that users say \"OK, bravo, I want many KOffice pre-alpha apps, I want duplicate apps, I want that bugs will not be fixed, I want fun and more fun ?\" ? Will it be positive ?\nI feel sad that your answers are only \"Shut up !\"... Is it positive ?\nAnd plesse, don't give me bad hidden intentions, my statements are not ambiguous.\nI only say that, as a KDE user, I disagree with the addition of a duplicate pre-alpha application in KOffice. And I have explained my reasons.\n\n(and when I say \"better or complementary\", I mean that I feel a vector app may enter in KOffice if it is a better app that the existing vector app (and it replaces it) or if it is complementary (and it coexists). And not in alpha or beta stage, I wish that Koffice rocks...)"
    author: "Alain"
  - subject: "Re: Hmmm..."
    date: 2002-03-08
    body: "first of all, if he wasn't working on karbon, he probably wouldn't be working on any other koffice app. i say this not because i know the fellow, but because that's the way open source development usually works. so koffice isn't losing anything by gaining karbon.\n\nsecondly, if you traipse on over to lists.kde.org, click on the kde-cvs link and search for krita, kspread and kivio (the apps you were concerned about) i think you will be pleased to see active development occuring. kivio seems to be seeing the least amount of development, but that isn't surprising given its history.\n\nyes, koffice is taking time to mature. but if the next six months are anything like the last six months, i think we'll be singing a different tune in the fall."
    author: "Aaron J. Seigo"
  - subject: "Re: Hmmm..."
    date: 2002-03-08
    body: "I initially felt a bit like you... but... the (bad) state of the current Kontour (and the low activity on it, if any) speaks for itself. Try it out (and check kde-cvs), you'll see what I mean.\n\nMy line of thought is: better have two vector drawing apps in KOffice-1.2 than none at all\n(as might have happened)."
    author: "David Faure"
  - subject: "Re: Hmmm..."
    date: 2002-03-08
    body: "Thank you, I more understand. I hope it will be a good choice and that Karbon will be enough advanced for KOffice 1.2."
    author: "Alain"
  - subject: "Re: Hmmm..."
    date: 2002-03-11
    body: "It will still be a long time until KOffice becomes a widely used office package. So I think it's OK to experiment a little at this point. But there definitely should be a point in the near future where KOffice throws out one of the drawing programs, depending on activity. I hope that the developers are sufficiently well organized to allow such a decision."
    author: "Xirzon"
  - subject: "My view"
    date: 2002-03-09
    body: "Hi Alain and others,\n\ni know what you feel and think about \"yet another duplicate effort\".\n\nOpen source is driven by personal satisfaction. In my eyes killustrator/kontour failed, because the\nauthors never used it on a daily/serious basis. I have some years of experience in graphic design and\nknow quite exactly what i want to do and how. I couldnt achieve this with killu/kontour and from the source (yes, i spent quite some time in looking at it) found, that the design makes it impossible to ever achieve those ting. So i started to discuss with developers 2 years ago. But those developers didnt know Photshop or Illustrator either and the discussions were very point- and useless (\"no! better improve it, than writing something from scratch\" - ironically this was done 2 years later).\nSo, please dont blame me for being non-cooperative.\n\nAlain, David, what would you do if someone would say to you \"One desktop is enough. Improve gnome!\"? What is the sense in open source programming, when they take your only reward: personal satisfaction? I finally came up with the conclusion: better work on an application myself which probably will never be usefull because im the only developer but which i like 110%. Well and some guys liked it too, they helped and made karbon14 a better, nearly usefull app. With moving karbon14 to koffice we try to gain more developers/artist and thus increase development speed. We can currently do things which were never possible in killu/kontour before. Of course we lack some feature, though. BUT we lack them because we lack time and contributors, not because the design forbids them.\n\nIf you feel offended by our work in our spare time, we could also make karbon a commercial closed source app. If you dont want it in koffice, just tell us. Our users and contributors drive karbon.\n\nBye\nLenny"
    author: "Lenny"
  - subject: "Re: My view"
    date: 2002-03-14
    body: "> If you feel offended by our work in our spare time, we could also make karbon a commercial closed source app.\n\nNo, no, no, I absolutely not felt offended by your work. My lack of understanding was not about you, but about the KOffice team who decided a duplicate app.\nIf anyone who creates a pre-alpha app may add a duplicate app in KOffice, were is it going ?\n\nNow, I have more information, I know that Kontour seems without future, and I see that you have a good punch.\n\nGood work, good luke !"
    author: "Alain"
  - subject: "Re: My view"
    date: 2002-03-14
    body: "Thank you. :)"
    author: "Lenny"
---
This <a href="http://kt.zork.net/kde/kde20020301_34.html">week's edition</a> of KC KDE is out.  Read about the <i>huge</i> <A href="http://lists.kde.org/?l=kde-cvs&m=101516456515743&w=2">scary</a> number of KDE3 bugfixes that have emerged from the low-profile KDE III gathering, everaldo's Crystal project (with plenty of screenshots), Kicker's menu <a href="http://qwertz.r--e.net/composite.png">sidebar</a>, an <a href="http://www.arts-project.org/">aRts</a> update, the new <A href="http://www.appel-systems.de/jay/kdepromofaq.txt">kde-promo FAQ</a>, <a href="http://www.xs4all.nl/~rwlbuis/karbon/pics/">Karbon</a> moving into KOffice, and more.

<!--break-->
