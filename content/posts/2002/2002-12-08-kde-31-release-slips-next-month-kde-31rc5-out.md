---
title: "KDE 3.1 Release Slips to Next Month, KDE 3.1RC5 Out"
date:    2002-12-08
authors:
  - "Dre"
slug:    kde-31-release-slips-next-month-kde-31rc5-out
comments:
  - subject: "That's great..."
    date: 2002-12-07
    body: "Really that is the most appreciable gesture for the KDE Team... Giving Priority to security :) Thanks I can wait a month or two for a stable, secure KDE..."
    author: "Rizwaan"
  - subject: "Online survey shows that the decision is correct"
    date: 2002-12-07
    body: "\"Should KDE 3.1 wait for security fixes before release?\":\n<a href=\"http://pclinuxonline.com/modules.php?name=Surveys&op=results&pollID=136&mode=&order=&thold=\">pclinuxonline poll</a>"
    author: "Anonymous"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-07
    body: "Okay, I too think that KDE should wait indeed till all severe bugs are fixed, before releasing a final version. But I think bugs would be found much faster if RPMs were made for the release candidates.\n\nIn the thread about KDE 3.1rc2 on this site, on the question if RPM's were made available, someone said:\n\"no, because kde 3.1 final is right around the corner.. too much work. this is meant for people willing to test anyways, and that means ppl who compile from src\"\n\nTwo times wrong: this was posted on november the 5th, so 'around the corner' was a little to optimistic, and I am willing to use a release candidate and report bugs, but I really don't have the time to compile the source myself."
    author: "Sylvester"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-07
    body: "The interesting period is the one to the next release, here RC3, not the final one.\n  KDE 3.1 RC2 was released on November 4th, 2002. \n  KDE 3.1 RC3 was released on November 11th, 2002. "
    author: "Anonymous"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-08
    body: "that was planned to be that way due to SVG"
    author: "a.c."
  - subject: "Re: RPMs"
    date: 2002-12-08
    body: "\"Okay, I too think that KDE should wait indeed till all severe bugs are fixed, before releasing a final version. But I think bugs would be found much faster if RPMs were made for the release candidates.\"\n\nI have to agree with this.  I tried compiling KDE from source once, and I'll *never* try that again.  If RPMs were put out, I'd download them, install them, and it'd take up about four hours of my time--mostly in the downloading, which I can automate and spend elsewhere.  But if I try to compile on my 450MHz Celeron/96M RAM (which can handle Red Hat 8.0 with KDE 3.0.5 just fine--I'd never even *think* about Windows eXPloit on this box), I'd be waiting for it to finish compiling when the next release came out.  Testing RPMs--whether they came from KDE or from Red Hat--would definitely be a major convenience (hint, hint)."
    author: "R2-D2"
  - subject: "Re: RPMs"
    date: 2002-12-09
    body: "Actually, it only takes about 2 days to build on my 400 MHz K6 III.\n\nYou can 'nice' it and it won't interfere with doing other things if you install it in: \"/usr/kde3/\" and then change your KDEDIR and PATH after the whole thing is installed.\n\nIt is a real pain the first time you build it, and from time to time I do screw up something on my system and it won't build again.  But, if you don't install other things from source, you won't have that problem."
    author: "James Richard Tyrer"
  - subject: "Re: RPMs"
    date: 2002-12-12
    body: ">Actually, it only takes about 2 days to build on my 400 MHz K6 III.\n\nI think you're missing the point.  \n\n\"about 2 DAYS\"\n\n*DAYS*\n\nThe idea is about building RPM's for those that use that package manager (I use Gentoo, so...).  An RPM person (and I guess a DEB too) could install it in about 5 minutes (minus DL time, of course).\n\nThat is what the dissucion is about.  Again, I'm on Gentoo.  I EXPECT things to take a while.  Sometimes even a couple days to finish.  But an RPM user that *WANTS* to help the KDE project by helping debug it, will want RPMs (or DEBs, of course)."
    author: "Xanadu"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-08
    body: "Yes, it would be a Good Thing to, at least, always have a final RC release with binaries and all - in other words to only call stable release what we usually call aq x.x.1 release. \n\nJ.A.\n\n"
    author: "Jad"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-09
    body: "\"Okay, I too think that KDE should wait indeed till all severe bugs are fixed, before releasing a final version. But I think bugs would be found much faster if RPMs were made for the release candidates\"\n\nBut surely these sort of bugs, security bugs, are (as has just been shown by them being found by an audit, but not by actual use) /not/ likely to be found faster if binaries are available?  Sure, bugs *in general* will be, but these?\nThat's not to say, of course, that security bugs aren't found if the source isn't available - IIS is proof of that!\n\n-- jc"
    author: "jaycee"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-10
    body: "If KDE-3.1 had to be delayed because of _security_ bugs, the RCs should be _immediately_ deleted by the users that decided to compile them when they went out. So, what makes RC RPMs necessary in all this?"
    author: "Inorog"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-10
    body: "If KDE-3.1 had to be delayed because of _security_ bugs, the RCs should be _immediately_ deleted by the users that decided to compile them when they went out. So, what makes RC RPMs necessary in all this?"
    author: "Inorog"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-13
    body: "In initial message: \"The security fixes will be backported to KDE 2.2.2.\"\n\nI conclude from this sentence that these security issues are not new at all, but that people just became aware of it now. So why delaying the release? Almost everyone has these bugs already in his system...\nWhy don't they get the security fixes ready for KDE 3.1.1 or something like that and release KDE 3.1 now?"
    author: "Sylvester"
  - subject: "Re: Online survey shows that the decision is correct"
    date: 2002-12-08
    body: "Wrong question :-)\n\n\"Should KDE release 3.1 this year without binary-packages?\"\n\nTo release something with known security issues is out of question!\n\nBye\n\n  Thorsten "
    author: "Thorsten Schnebeck"
  - subject: "compile failure in kdelibs/kdeui/kedittoolbar.cpp"
    date: 2002-12-07
    body: "Patch to fix this mistake: http://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdelibs/kdeui/kxmlguiclient.h.diff?r1=1.40&r2=1.40.2.1"
    author: "Anonymous"
  - subject: "Re: compile failure in kdelibs/kdeui/kedittoolbar."
    date: 2002-12-07
    body: "Thank you! I spent hours last night trying to figure out what was the problem. Hope this works."
    author: "Damn G..."
  - subject: "Re: compile failure in kdelibs/kdeui/kedittoolbar."
    date: 2002-12-07
    body: "You can also download today's updated kdelibs source tarball."
    author: "Anonymous"
  - subject: "Building right now"
    date: 2002-12-07
    body: "\nkdelibs and kdebase are already done. And Konqueror still leaks memory :("
    author: "cbcbcb"
  - subject: "Re: Building right now"
    date: 2002-12-07
    body: "konqueror working fine here!  i have only 128M RAM but it is good quality.  it does not leak!"
    author: "anon"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "There's a script here which still shows gradual increase in memory usage in 3.1rc5: http://bugs.kde.org/show_bug.cgi?id=51061\n\nPerhaps you might like to try this script and see if you can reproduce the leak. You might need to run the script a few times to really see the increase in memory usage. "
    author: "cbcbcb"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "I have a gig of ram and here is my current memory usage:\nMem:   1033592k total,   989508k used,    44084k free,    42556k buffers\nSwap:  2097136k total,     2360k used,  2094776k free,   671500k cached\n\nafter this uptime\n19:37:25 up 3 days, 1 min,  1 user,  load average: 0.00, 0.10, 0.17\n\nI am running currently gaim, xchat and this konq window. Memory leak? I think so."
    author: "Kirk"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "This is how Linux works.  Nothing to do with KDE."
    author: "ac"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "I don't think so. Take a look at the states. You've got 670MB in the page cache, 45MB free, and 40MB in buffers. All that is available memory which is serving as a file cache. So you've got 750+ MB free. That's only about 250MB used, which seems about right. I've been running for few hours now, and I've got 175MB used."
    author: "Rayiner Hashem"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "I think you are reading the numbers wrong.\nI'll bet you are only looking at this bit: \"Mem: 1033592k total, 989508k used\" and then you think all your memory is used up - NOT TRUE!\nMost of that memory is being used for buffers, cache etc. If a program on your box was to request a 100MB block of memory then there would be no problem satisfying that demand.\nThe reason you see such a high number as \"used\" is simply becourse Linux will always try to use as much memory as possible for cache and buffers to improve system performance, but as soon as some program needs the RAM, it's imidiately released and handed over to the program.\n\nThis does not signify a memory leak, it only signifies that you do not know how Linux works.\n\nIf you want to go hunting for memory leaks, I sugest you take a look at the actual memory usage of a specific application, not of the system as a whole.\n"
    author: "JJ"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "Here Konqueror - or some other part of KDE that \"can't be removed\" (kind of like IE on windows) - leaks badly and swaps like mad. As for RAM, 128 megs is definitely *not* enough.\n\nIn genera; KDE is as a slow as molasses on my 600MhZ P3 with 128 megs RAM (and nowhere near as snappy as XP on same system).\n\n"
    author: "Leaker"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "> In genera; KDE is as a slow as molasses on my 600MhZ P3 with 128 megs RAM (and nowhere near as snappy as XP on same system).\n \nThere's lots of stuff you can do to improve performance. For example : \n\n1) When compiling KDE, optimize for your current CPU - for example, don't build for i386 if you have a Pentium.\n\n2) Don't build KDE with debug info (if you want to report bugs, enabeling debug is good though).\n\n3) Turn the eye candy down a bit. Stuff like transparency, antialiasing, animated menus, a huge background image etc all take a chunk out of your performance.\n\n4) Make sure your X configuration is making the best possible use of your graphics card. There are plenty of ways to improve general X performance.\n\n5) Go over your system configuration and make sure you are not running a lot of programs you don't need/use anyway. Many distributions run lots of daemons and other stuff by default that you may not need - disable some of that to free up some CPU cycles and RAM.\n\nI'm sure you (or someone else) can come up with other good ideas, but now you have a place to start if you want to improve performance :-)"
    author: "Jesper Juhl"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "6) Use gcc 3.2.x.\n\n7) Use a binutils version which uses combreloc.\n\n8) Use glibc 2.3.\n\n9) Use current XFree version, otherwise delete unnecessary fonts.\n\n10) Compile Qt with --disable-opengl if you don't need it (screensavers, kpovmodeler). This will reduce the number of relocations needing linking at start-up noticeable.\n\n11) Or use \"prelink\" (not equal to objprelink) to medicate binaries and libraries.\n\n12) Configure kdelibs with --enable-fast-malloc=full"
    author: "Anonymous"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "13. User a kernel with preempt, lowlatency, O(1).\n\n14. Compile with -O3 -fomit-frame-pointer -pipe -march=yourcpu (*)\n\n15. Add fam to start on boot (allows KDE to track files quicker)\n\n16. Add your hostname to /etc/hosts (if it's not already there)\n\n(*): One could argue about -O3. Some think that -Os is a better choice. I don't really notice a difference between -O2, -O3 and -Os, but it makes a difference on slower cpus. \n\nSome of these hints are taken from a discussion on the gentoo mailing list."
    author: "Michael Jahn"
  - subject: "excellent !"
    date: 2002-12-08
    body: "nothing to add !"
    author: "thierry"
  - subject: "I can think of one more!"
    date: 2002-12-11
    body: "16) Run kdebugdialog and turn off all debug output."
    author: "Anonymous"
  - subject: "Re: Building right now"
    date: 2002-12-09
    body: "> 14. Compile with -O3 -fomit-frame-pointer -pipe -march=yourcpu (*)\n\nAnd how do you expect that compiling with \"-pipe\" will improve the performance of KDE? As far as I can tell, it only controls the communication between the various build tools during the build process and has no effect on the contents of the output files. From \"info gcc\":\n\"\n`-pipe'\n     Use pipes rather than temporary files for communication between the\n     various stages of compilation.  This fails to work on some systems\n     where the assembler is unable to read from a pipe; but the GNU\n     assembler has no trouble.\"\n\n> 16. Add your hostname to /etc/hosts (if it's not already there)\n\nWhat about all the people who do not get a static PI-addres/hostmane from their ISP but only a dynamic IP-adresses? Should they have anything more than \"127.0.0.1       localhost\" in that file? How and when does the contents of that file affect the performance of KDE?"
    author: "Erik"
  - subject: "Re: Building right now"
    date: 2002-12-09
    body: "> And how do you expect that compiling with \"-pipe\" will improve the performance of KDE? As far as I can tell, it only controls the communication between the various build tools during the build process and has no effect on the contents of the output files. From \"info gcc\":\n\nI did not specifically say, that -pipe improves perfomance ;-). But again, this is not by me but was on found on a gentoo mailing list/forum:\nhttp://forums.gentoo.org/viewtopic.php?t=3209\n\nYou can also have a look there for a discussion of the hostname item.\n"
    author: "Michael Jahn"
  - subject: "Re: Building right now"
    date: 2002-12-11
    body: "> 11) Or use \"prelink\" (not equal to objprelink) to medicate binaries and \n> libraries.\n\nPrelink is not buildable. It ends with: \nprelink.c: In function `prelink_set_checksum': \nprelink.c:819: warning: passing arg 1 of \n`elf32_xlatetof' from incompatible pointer type \nprelink.c:819: warning: passing arg 3 of \n`elf32_xlatetof' makes integer from pointer \nwithout a cast \nprelink.c:819: too many arguments to function \n`elf32_xlatetof' \nprelink.c:822: warning: passing arg 1 of \n`elf32_xlatetom' from incompatible pointer type \nprelink.c:822: warning: passing arg 3 of \n`elf32_xlatetom' makes integer from pointer \nwithout a cast \nprelink.c:822: too many arguments to function \n`elf32_xlatetom' \nmake[2]: *** [prelink.o] Error 1 \n\nI have prelink-20021002, libelf-0.8.2, gcc-3.2.1 and glibc-2.3.1."
    author: "Erik"
  - subject: "Re: Building right now"
    date: 2003-01-18
    body: "Try with libelf-0.8.0 .\n\nIt may be caused with libelf-0.8.2 .\n"
    author: "shivaken"
  - subject: "Re: Building right now"
    date: 2002-12-10
    body: "Can you tell how to achieve these optimisations please? Or point me to the apropriate documentation.\nI\u00b4m running a Dual P3 (Katmai core) with 256Mb memeory and I\u00b4ve never checked if the things I compile are optimized for my CPU.\n\nTIA\n"
    author: "Sangohn Christian"
  - subject: "Re: Building right now"
    date: 2002-12-11
    body: "> 2) Don't build KDE with debug info (if you want to report bugs, enabeling\n> debug is good though).\n\nMy recommendation is to always build kde with --enable-debug=full and then strip the installed binaries and libraries. When you need to report a bug you don't have to rebuild, just reinstall the binary and the libraries it uses.\n\nI assume that strip --strip-all will remove all the overhead produced by --enable-debug=full except for debug output (that is seen when running applications from Konsole).\n\n> 4) Make sure your X configuration is making the best possible use of your\n> graphics card. There are plenty of ways to improve general X performance.\n\nAnd what would that be except for decreasing resolution/colors, which I do not want to sacrifice?"
    author: "Erik"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "Bzzzt, wrong.  I only have a Celeron 350 and it is _fine_ here."
    author: "ac"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "You clearly don't understand what Konqueror is or how it works.  It can easily be removed, as can all of its plugins  and  components.  This has been demonstrated time and time again.  If  you can honestly  find a reproducible way to trigger a leak then  file a  detailed bug  report  and we will fix it.  Valgrind is a great tool for that."
    author: "kde developer"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "\nI can 100% reproduce a leak in Konqueror. I just use it to browse the web, and slowly but surely its memory usage increases. However, if I run Konqueror in valgrind it is unusably slow, so that's out. Konqueror does leak memory, and KDE developers denying that isn't going to help. :)\n\nI have reported two scriptable memory leaks in 3.1rc3 and these seem to have been fixed. However, I think the Konqueror/KDE developers should be putting some serious effort into tracing memory leaks (or switch KDE to use a garbage collection strategy)"
    author: "cbcbcb"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "Can you explain your method of verifying that memory is leaking? I assume (/hope) that you're not just looking at the current total memory usage...\n\nTom"
    author: "Tom"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "\nThe VIRT column provided in top 3.1.1 for the Konqueror process\n"
    author: "cbcbcb"
  - subject: "Re: Building right now"
    date: 2002-12-08
    body: "It's normal for the process size to grow somewhat with time due to fragmentation of the free memory. However, over time the memory use should stabilize, e.g. under normal workloads, if you have used it for 2 days, it should hardly increase the 3rd day.\n\nIf that's not the case then there is something wrong indeed, the key is then to identify the site or construct that causes it :-}\n\nCheers,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: Building right now"
    date: 2002-12-10
    body: "Is there some valid reason that you don't simply buy another 256 Megs of memory?\n\nYou are correct that LINUX can NOT run KDE efficently with only 128 Megs of memory?\n\nHowever, it isn't KDE that is slow, it is the system that is slow and the reason is that swaping to disk is slow."
    author: "James Richard Tyrer"
  - subject: "Re: Building right now"
    date: 2002-12-10
    body: "I COMPLETELY AGREE :)\n\nHmmm, wait ... What would I rather spend money on, buying a license of XP or buying more RAM?\n\nGee, let me think now.\n\nMore RAM makes the whole Linux experience so much better. Just do it, damnit :)"
    author: "More RC's"
  - subject: "Re: Building right now"
    date: 2002-12-10
    body: "please do not troll on linux with such stupid argumentation.\nLinux without KDE is fast on low memory machines.\nKDE requires a huge amount of RAM, and this is definetely not the OS fault. Rox is much much faster/lighter than KDE or Gnome and does not require that much. So is XFCE. Linux/FreeBSD/whateverOS is not responsible for the amounts of RAM that both Gnome and KDE require."
    author: "oliv"
  - subject: "Re: Building right now"
    date: 2002-12-10
    body: "I did not make a \"stupid agrumentation\" you misunderstood me.\n\nKDE does require 256 Megs of ram to run well.  That is NOT the OS's fault.\n\nHowever, it *is* the OS swaping virtual memory to disk that results in KDE being slow.  This actually does not effect the speed when running an application.  However, when starting an application or switching to a different application, the first thing that happens is a VM disk swap.  This is what slows it down.\n\nIn todays market, 0.75 Geg of memory is not expensive.  So, I don't think that the 256 Megs (0.25 Geg) of memory that you need run KDE is a huge amount of RAM."
    author: "James Richard Tyrer"
  - subject: "Re: Building right now"
    date: 2002-12-11
    body: "i suppose the key (and variable) term is \"runs well\". i have it on a few 64MB systems around here and it runs just fine with recent releases on recent distros (e.g. MDK9 and SuSe8.1). no fancy wallpapers or other memory-consuming items and only 3-4 apps running at a time, but it's snappy enough for daily use ...\n\nof course, my primary desktop has 256MB of RAM... =)"
    author: "Aaron J. Seigo"
  - subject: "Re: Building right now"
    date: 2002-12-18
    body: "Hey guys,\ni've a stupid question. Which version of qt are you using to compile 3.1rc5?\nI get always errors like this, when compiling kdelibs :\n\nuic -nounload -o keygenwizard.h ./keygenwizard.ui\nuic: relocation error: /root/qt-x11-free-3.1.1/bin/uic: undefined symbol: _ZN7QString9fromAsciiEPKci\nmake[4]: *** [keygenwizard.h] Error 127\nmake[4]: Leaving directory .../kdelibs-3.1rc5/kio/kssl'\nmake[3]: *** [all-recursive] Error 1\nmake[3]: Leaving directory .../kde-3.1-rc5/src/kdelibs-3.1rc5/kio/kssl'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory .../kde-3.1-rc5/src/kdelibs-3.1rc5/kio'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory .../kde-3.1-rc5/src/kdelibs-3.1rc5'\nmake: *** [all] Error 2\n\nI've dl and compiled qt-x11-free-3.1.1.\n\nAnd by the way, is there anywhere a doku/faq how to compile kde3.1?\nI searched and searched...but the guys from KDE have obviously forgotten? to put some instructions on the website. And the FAQ's there are only for old versions...\n\nThx,\n\nTheDude."
    author: "Dude"
  - subject: "Re: Building right now"
    date: 2002-12-18
    body: "> uic: relocation error: /root/qt-x11-free-3.1.1/bin/uic: undefined symbol: _ZN7QString9fromAsciiEPKci\n\nuic is picking up another Qt library as the one it was linked against. Play with LD_LIBRARY_PATH/ld.so.conf.\n\n> And by the way, is there anywhere a doku/faq how to compile kde3.1?\n\nhttp://developer.kde.org/build/compile_cvs.html"
    author: "Anonymous"
  - subject: "Re: Building right now"
    date: 2002-12-19
    body: "Thx...hope it'll work."
    author: "Dude"
  - subject: "An uanassailable decision."
    date: 2002-12-07
    body: "Who can fault this decision except for those who haven't\nprogressed beyond the \"Are we there yet?\" stage."
    author: "guinstar"
  - subject: "Why?"
    date: 2002-12-07
    body: "On the release page it says:\nBugs...none\nSecurity issues...none\n\nSo why not release it?"
    author: "KDE User"
  - subject: "Re: Why?"
    date: 2002-12-07
    body: "You didn't read the news article? There is a security audit still running which may lead to discovery of exploitable security leaks not fixed in RC5 and fixing potential security flaws in general."
    author: "Anonymous"
  - subject: "getting the 3.1 branch with cvs(up)"
    date: 2002-12-07
    body: "Hi,\nI've been using cvsup for a few months now. Until 3.1 was branched, HEAD was always usable, but now that the feature freeze is over, HEAD becomes too unstable.\n\nI want to download the (almost) stable 3.1 branch with cvsup. So I set tag=KDE_3_1_BRANCH. But if I cvsup then, I only get a corrupted system: missing Makefile.cvs, almost no qt-copy, no configure, lots of files missing. Does anyone know, what I am doing wrong?\nMicha\n\n"
    author: "Michael Jahn"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-08
    body: "you are relying on KDE ... which is wrong.\n\nBuy commercial software and you will have less to worry about and have a better life."
    author: "anon"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-08
    body: "> you are relying on KDE ... which is wrong.\n\nabsolutely not.\nthere are commercial software where you\nCAN rely on KDE."
    author: "KDE User"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-08
    body: "dear god here come the flames..."
    author: "standsolid"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-08
    body: "Looking at:\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/qt-copy/\n\nI see qt-copy does not have a KDE_3_1_BRANCH tag.\n\nYou're not supposed to get a configure file.  You're supposed to type\n\n    make -f Makefile.cvs\n\nto create the configure file.  I don't know why you aren't getting a Makefile.cvs (kdelibs seems to have it tagged for KDE_3_1_BRANCH).  You might want to try \"tag=.\".\n\nI haven't successfully compiled from CVS since KDE 3.1 Beta 2.  I have had nothing but trouble compiling KDELIBS with the recent Release Candidates.  There is an annoying bug (51112) I want to further investigate and possibly fix, but I can't even compile.  My most recent problem is this error:\n\n.libs/dcopclient.o: In function `DCOPClient::staticMetaObject()':\n.libs/dcopclient.o(.text+0x939a): undefined reference to `QMetaObject::new_metaobject(char const*, QMetaObject*, QMetaData const*, int, QMetaData const*, int, QMetaProperty const*, int, QMetaEnum const*, int, bool (*)(QObject*, int, int, QVariant*), QClassInfo const*, int)'\n.libs/dcopclient.o: In function `DCOPClient::applicationRegistered(QCString const&)':\n.\n.\n.\n\nI'm sure I'm just doing something wrong.  I did switch from QT 3.1.0 beta 2 to QT 3.1.0 final.  Oh well, I think I'll go try the tarballs."
    author: "Jiffy"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-08
    body: "maybe you should delete your *.moc files.  you may need to delete files generated from *.ui at some point."
    author: "ac"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-08
    body: "> You're not supposed to get a configure file. You're supposed to type\n> make -f Makefile.cvs\n> to create the configure file. I don't know why you aren't getting a Makefile.cvs (kdelibs seems to have it tagged for KDE_3_1_BRANCH). You might want to try \"tag=.\".\n\nYes, I know. But there _were_ configure files in cvs. I don't know what went wrong the first couple of times I tried. Either it was fixed in CVS or I made some stupid mistake. Nonetheless it works now and that's all that counts. \n\nTo answer to your problem: I always do a \"make clean distclean\" in all cvs modules before recompiling everything. This gets rid of old files which could be in the way. Maybe this could help you too.\nMichael Jahn\n"
    author: "Michael Jahn"
  - subject: "Re: getting the 3.1 branch with cvs(up)"
    date: 2002-12-10
    body: "About HEAD, well that is now the branch for KDE 3.2, so you can expect that it won't be as stable as it was befor the branching :o) about KDE_3_1_BRANCH, try using a different cvs mirror\n\nRinse\n\n"
    author: "rinse"
  - subject: "Thank you for putting security first!"
    date: 2002-12-08
    body: "\nShipping a KDE 3.1 release with known security problems would have been bad. Every project should put a huge emphasis on software security, and I'm glad to see the KDE developers postpone the release date in order to fix security issues. That's the right desition to make.\n\nA big thank you from me to the KDE developers! \n"
    author: "Jesper Juhl"
  - subject: "Hmmm not what with 3.1rc5"
    date: 2002-12-08
    body: "I almost had 3.0.99 built when I saw 3.1rc5 so I started compiling it instead.\nI hit the following building kdelibs.  Wonder if I need to go back and update libxml etc.  Course I am using the latest (a bit buggy) version of QT which has caused some pains.  Anyone run into this yet?  See below from make >make.log while\nbuilding kdelibs-3.1rc5\n<p />\n<code>Ed:  compile output placed in comment</code>\n<!--\n/bin/sh ../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I.. -I../kdefx -I../interfaces -I../dcop -I../libltdl -I../kdecore -I../kdeui -I../kio -I../kio/kio -I../kio/kfile -I.. -I/usr/local/qt/include -I/usr/X11R6/include -I/usr/local/kde-3.1rc5/include   -DQT_THREAD_SUPPORT -pipe -O3 -mcpu=pentium3 -march=pentium3 -msse -mfpmath=sse -fforce-mem -fforce-addr  -D_REENTRANT -I/usr/include/pcre  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -pipe -O3 -mcpu=pentium3 -march=pentium3 -msse -mfpmath=sse -fforce-mem -fforce-addr -fno-exceptions -fno-check-new  -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT  -c -o kxmlguibuilder.lo `test -f 'kxmlguibuilder.cpp' || echo './'`kxmlguibuilder.cpp\n/usr/local/qt/bin/moc ./kedittoolbar.h -o kedittoolbar.moc\nsource='kedittoolbar.cpp' object='kedittoolbar.lo' libtool=yes \\\ndepfile='.deps/kedittoolbar.Plo' tmpdepfile='.deps/kedittoolbar.TPlo' \\\ndepmode=gcc3 /bin/sh ../admin/depcomp \\\n/bin/sh ../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I.. -I../kdefx -I../interfaces -I../dcop -I../libltdl -I../kdecore -I../kdeui -I../kio -I../kio/kio -I../kio/kfile -I.. -I/usr/local/qt/include -I/usr/X11R6/include -I/usr/local/kde-3.1rc5/include   -DQT_THREAD_SUPPORT -pipe -O3 -mcpu=pentium3 -march=pentium3 -msse -mfpmath=sse -fforce-mem -fforce-addr  -D_REENTRANT -I/usr/include/pcre  -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -pipe -O3 -mcpu=pentium3 -march=pentium3 -msse -mfpmath=sse -fforce-mem -fforce-addr -fno-exceptions -fno-check-new  -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT  -c -o kedittoolbar.lo `test -f 'kedittoolbar.cpp' || echo './'`kedittoolbar.cpp\nkxmlguiclient.h: In member function `bool KEditToolbarWidget::save()':\nkxmlguiclient.h:284: `virtual void KXMLGUIClient::setXMLFile(const QString&, \n   bool = false, bool = true)' is protected\nkedittoolbar.cpp:399: within this context\nkxmlguiclient.h:284: `virtual void KXMLGUIClient::setXMLFile(const QString&, \n   bool = false, bool = true)' is protected\nkedittoolbar.cpp:402: within this context\nmake[3]: *** [kedittoolbar.lo] Error 1\nmake[3]: Leaving directory `/builddir/kde-3.1rc5/kdelibs-3.1rc5/kdeui'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/builddir/kde-3.1rc5/kdelibs-3.1rc5/kdeui'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/builddir/kde-3.1rc5/kdelibs-3.1rc5'\nmake: *** [all] Error 2\n-->"
    author: "J Battles"
  - subject: "Re: Hmmm not what with 3.1rc5"
    date: 2002-12-08
    body: "Read this: http://dot.kde.org/1039281841/1039286480/"
    author: "ac"
  - subject: "Re: Hmmm not what with 3.1rc5"
    date: 2002-12-08
    body: "Thanks,\n\nI should have looked at the CVS updates before posting.  Found the updates.  LOL\nI made the assumption if it was released, it would probably compile."
    author: "J Battles"
  - subject: "What a good idea"
    date: 2002-12-08
    body: "You are absolutely right to put security first.\nLet's go and build the most secure and most stable unix desktop kde team.\n\n"
    author: "socialEcologist"
  - subject: "devices:/ file-system"
    date: 2002-12-08
    body: "Can anyone tell me why it is that whenever I try to mount a CD or floppy as a simple user I get the message \"only root can do that\"?\n\nMounting these devices works ok from command-line (my fstab is correct)..."
    author: "Alexandros Karypidis"
  - subject: "Re: devices:/ file-system"
    date: 2002-12-09
    body: "I had the same problem.\n\nTo fix it, I unchecked the 'Show Floppy/CDROM' boxes in Desktop Behavior settings. Then I right-clicked the desktop and created new floppy and cd-rom device icons manually with the Create New... menu.\n\nThis sounds like a bug; I also noticed that the context menu with the manually created device icons is much bigger (now has eject, etc.) They also don't move when the devices are mounted.\n\nCreate the icons manually for now. Wierd!"
    author: "Brent Cook"
  - subject: "Re: devices:/ file-system"
    date: 2002-12-09
    body: "You sure there is the user option in /etc/fstab for you cdrom/burner/floppy devices?\ne.g. like this:\n/dev/cdroms/cdrom1\t/mnt/burner\tiso9660\t\tnoauto,ro,user\t0 0\n/dev/cdroms/cdrom0\t/mnt/cdrom\tiso9660\t\tnoauto,ro,user\t0 0\n"
    author: "diederik"
  - subject: "Re: devices:/ file-system"
    date: 2002-12-12
    body: "Yes, my floppy/dvd/cdrw are defined as:\n\n/dev/cdrecorder      /media/cdrecorder    auto       ro,noauto,user,exec   0 0\n/dev/cdrom           /media/cdrom         auto       ro,noauto,user,exec   0 0\n/dev/fd0             /media/floppy        auto       noauto,user,sync      0 0\n\nwhere /dev/cdrom --> /dev/scd1\nand /dev/cdrecorder --> /dev/scd0\n\n(ide-scsi emulation)\n"
    author: "Alexandros Karypidis"
  - subject: "Trouble with Lilo..."
    date: 2002-12-08
    body: "I just compiled, and am enjoying 3.1 RC 5. Congradulations to the KDE team! I really like the integration of xscreensaver - Now my list of screensavers is huge!\n\nHowever, I have one major problem: in KDM, the lilo entries are not displayed. instead, I get a blank selection box under the restart option. My lilo paths are correct, and I'm using Mandrake 9.0. Any ideas?"
    author: "Braden MacDonald"
  - subject: "xscreensaver?!?!?  Yippee"
    date: 2002-12-09
    body: "I love the xscreensaver stuff in Redhat 8.  Can't wait to be able to use it with KDE."
    author: "Rimmer"
  - subject: "Re: xscreensaver?!?!?  Yippee"
    date: 2002-12-09
    body: "Seriously, what's your point?  It has always been in KDE."
    author: "ac"
  - subject: "Seriously..."
    date: 2002-12-09
    body: "if can you use xscreensaver modules in KDE it's not f*&#!% clear."
    author: "Rimmer"
  - subject: "Re: Seriously..."
    date: 2002-12-09
    body: "Then don't use RedHat.  Seriously, what's your point?  \n\nKDE has had support for xscreensaver for a long time.  Since 2000/01/26.\n\nhttp://webcvs.kde.org/cgi-bin/cvsweb.cgi/kdeartwork/kscreensaver/\n"
    author: "ac"
  - subject: "Re: Seriously..."
    date: 2002-12-09
    body: "You're wrong. KDE has had support for it's own screensavers only - always a stupid \"Not Invented Here\" thing about KDE I thought. Support for xscreensavers modules is a new and welcome thing."
    author: "dr_lha"
  - subject: "Re: Seriously..."
    date: 2002-12-09
    body: "_you're_ wrong.  look at the damn link and the CVS logs - always the stupid trolls i thought. *sigh*\n \nRevision 1.1 / (download) - annotate - [select for diffs], Wed Jan 26 04:00:48 2000 UTC (2 years, 10 months ago) by jones \nBranch: MAIN \n\nConfiguration for xscreensaver hacks\n"
    author: "ac"
  - subject: "Re: Seriously..."
    date: 2002-12-10
    body: "My bad. It's still not been in there in an obvious way though."
    author: "dr_lha"
  - subject: "Re: Seriously..."
    date: 2002-12-11
    body: "The xscreensaver support has always been spotty.  It works on some distros and not on others.  I know it hasn't worked on Debian for quite some time.  I would have thought KDE didn't support xscreensaver myself if I hadn't already seen the support in some other distros.  Hopefully that has been fixed."
    author: "not me"
  - subject: "I think there is a way but..."
    date: 2002-12-09
    body: "I can't figure it out.  I found a xscreensaver.kss file in /usr/bin.  This brings up the xscreensaver configuration utility.  Unfortunately, there isn't a xscreensaver.desktop file to be found (I'm running Redhat 8).  The xscreensaver.desktop file is needed to make xscreensaver an option in the KDE screensaver dialog (I think).  Now searching on Google revealed a number of hits for rpm's with xscreensaver.desktop files.  So it appears (am I wrong here) that there is wrapper for xscreensaver that causes KDE to treat it like any other screensaver.  It possible that this works in distributions other then Redhat.\n\nThe question is... where can you get xscreensaver.desktop file.  I wonder if this wrapper is no longer being maintained?  ac could probably answer these questions if he wasn't so busy being mean."
    author: "Rimmer"
  - subject: "Problem executing xscreensaver.kss"
    date: 2002-12-09
    body: "Well I copied one of my .desktop files and tried to make it point to xscreensaver.kss.  The xscreensaver entry is now visible in the KDE screensaver dialog.  Unfortunately, I've discovered that the program doesn't seem to do anything when called (with xscreensaver.kss).  It's strange, because running xscreensaver.kss -test works fine.  xscreensaver.kss -setup also works (and the setup buttom in the KDE control panel runs the xscreensaver configuration tool.  Any suggestions?"
    author: "Rimmer"
  - subject: "Re: Problem executing xscreensaver.kss"
    date: 2003-01-22
    body: "read the /usr/bin/xscreensaver.kss script... You can modify it to run xscreensaver in a way that works (I hope,I'm just looking into it now)...\nOf course you still need to create that .desktop file"
    author: "redcane"
  - subject: "Re: Problem executing xscreensaver.kss"
    date: 2003-01-24
    body: "Hi,\n\nJust install kdeartwork from kde 3.1 (not earlier) and you should be all set. That is what I did last night on a RH8 machine. He had kde 3.0.x before and upgrading to kde 3.1 solved all problems for him...\n\nP"
    author: "perraw"
  - subject: "Re: Trouble with Lilo..."
    date: 2002-12-09
    body: "Have a look at $KDEDIR/share/config/kdm/kdmrc:\n# Offer LiLo boot options in shutdown dialog. Default is false\nUseLilo=false\n\nSet it to UseLilo=true if you want Lilo in KDM.\n"
    author: "Ingo Kl\u00f6cker"
  - subject: "Re: Trouble with Lilo..."
    date: 2002-12-09
    body: "Can we make it true by default on Linux?"
    author: "ac"
  - subject: "Re: Trouble with Lilo..."
    date: 2002-12-09
    body: "I run Linux, and have not had any use for LILO for over a year now.\nGRUB is the way to go, so all the KDE LILO bindings are useless\nto me. Perhaps it should not be hard wired to true/false, but rather\ndetermined at run time which boot system is used on the box and\nenable/disable the LILO boot options based on that?\n\nPaul.\n"
    author: "Paul Koshevoy"
  - subject: "Re: Trouble with Lilo..."
    date: 2002-12-09
    body: "I have it set to UseLilo=true, it still shows only an empty selection box."
    author: "Braden MacDonald"
  - subject: "Re: Trouble with Lilo..."
    date: 2002-12-11
    body: "I had this problem too, its not a matter of selecting LILO or not, its a matter of your map files not cooperating in /boot/.  Try setting them to global readable.  Maybe try global writeable.\n\nGreg"
    author: "greg"
  - subject: "Compile Issues"
    date: 2002-12-08
    body: "Everytime I try and do a make on kdelibs I get this error:\n\n.libs/dcopclient.o: In function `QPtrList<DCOPClientTransaction>::replace(unsigned int, DCOPClientTransaction const *)':\n.libs/dcopclient.o(.QPtrList<DCOPClientTransaction>::gnu.linkonce.t.replace(unsigned int, DCOPClientTransaction const *)+0x1c): undefined reference to `QGList::replaceAt(unsigned int, void *)'\n.libs/dcopclient.o: In function `QPtrList<_IceConn>::replace(unsigned int, _IceConn const *)':\n.libs/dcopclient.o(.QPtrList<_IceConn>::gnu.linkonce.t.replace(unsigned int, _IceConn const *)+0x1c): undefined reference to `QGList::replaceAt(unsigned int, void *)'\ncollect2: ld returned 1 exit status\nmake[3]: *** [libDCOP.la.closure] Error 1\nmake[3]: Leaving directory `/source/kde/kdelibs-3.1rc5/dcop'\nmake[2]: *** [all-recursive] Error 1\nmake[2]: Leaving directory `/source/kde/kdelibs-3.1rc5/dcop'\nmake[1]: *** [all-recursive] Error 1\nmake[1]: Leaving directory `/source/kde/kdelibs-3.1rc5'\nmake: *** [all] Error 2\n\nAnyone have ideas as to how I can fix it?"
    author: "Chris Anderson"
  - subject: "Re: Compile Issues"
    date: 2002-12-10
    body: "Are you using gcc 3.2?\n\nI'm not sure if it solves the problem, but I tried with 2.95 and got the same linker error (cvs from 3.12.2002 or so).\nTried gcc 3.2, but didn't get qt to compile witb it, so I gave up and apt-getted unofficial kde3.1 and got it working.\n\nTommi"
    author: "Tommi Uimonen"
  - subject: "Re: Compile Issues"
    date: 2002-12-10
    body: "\"make clean && make\" should do it.\n\nLooks like you have old objects and the linker is trying to link them with new objects compiled with newer headers."
    author: "John"
  - subject: "Re: Compile Issues"
    date: 2003-01-03
    body: "Have no solution, but having the same problem. Was wondering if you founf a solution in the mean time."
    author: "Jord Swart"
  - subject: "Re: Compile Issues"
    date: 2003-01-03
    body: "Thanks for the replies, I planned to switch distros so it didn't end up mattering in the end (had free time, installed mandrake to play with it).\n\nAs for a workaround, I'd try what they mentioned, I had a lot of problems with Redhat's GCC 2.96 (read up on it, understand why now). GCC 3.2 works flawlessly so far"
    author: "Chris Anderson"
  - subject: "Re: Compile Issues"
    date: 2003-01-03
    body: "Got the solution: specify all the qt paths (qt dir, qt lib etc) when doing the configure. If in doubt do a configure --help, you will see the needed options listed. \n\nPersonally I was using the instructions at women.kde.org, but somehow setting the environment variables wasn't enough. Don't know why (might have to do with my debian installation?).\n\nOh, just in case, make sure you are using a recent version of qt (3.1 and above or qt-copy from cvs). \n\nCheers,\n\nJord"
    author: "Jord Swart"
  - subject: "stop making us suffer!"
    date: 2002-12-09
    body: "no!!!! we want a final!  I will boycott this release forever!\n\n.:starts compiliing:.\n\nsorry for rant. i'm impatient.  "
    author: "standsolid"
  - subject: "Suse didn't want it"
    date: 2002-12-09
    body: "Hello,\n\nthe truth is likely that SuSE just didn't want a KDE 3.1 release now, since it doesn't meet their own distribution schedule.\n\nI hope this isn't a long-term trend for KDE. Anyway, I am using Karolinga KDE packages for KDE3.1 on Debian, and who cares, if Suse allows to call them final or not. They are secure enough for me now.\n\nThe long term direction should certainly include security audits for the external interfaces. I welcome that. It's only that keeping back KDE 3.1 doesn't make 3.0 and 2.x very much more secure....\n\nRegards,\nKay\n"
    author: "A Debian user..."
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "Take your stupid theories elsewhere, KDE has a very open development process, and the decision to delay the release was based on general consensus on the KDE mailing lists."
    author: "Another Debian user"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "Well, stupid, maybe they are.\n\nBut it's no secret how Suse manpower is the one making decisions about KDE releases, is it?\n\nI find it strange that at the END of a feature freeze, bug fix cycle, a Suse employee proposes a further delay for a code audit to fix bugs that current stable releases already have...\n\nThe good is that if they took it too far, they would loose control, yes.\n\nRegards,\nKay"
    author: "Debian User"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "That's because SuSE cares about quality :-)\n\nCheers,\nWaldo\n-- \nbastian@kde.org -=|[ SuSE, The Linux Desktop Experts ]|=- bastian@suse.com"
    author: "Waldo Bastian"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "No.  If SuSE cared that much about quality the code audit would (should?) have happened before feature freeze - before any RC at any rate."
    author: "SniggleWitch"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "It was not a SuSE audit, but one by an independent, external group.\n"
    author: "Anonymous"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-18
    body: "Maybe SuSE should hire that independent testing group!?!?  And if the bug fixes could work on the existing releases of KDE-2.2.2 and KDE-3.0, why wouldn't they work on KDE-3.1 as well?  I mean it is like someone else said, if the exiating releases of KDE had the same pre-existing security issues, it would have made no difference to release this one.  Becuase it was not as though those prior releases were without any security problems.  If they had been, THEY WOULD NOT HAVE NEEDED TO BE FIXED!  So please explain all of this again.  How is releasing KDE-3.1 now, worse than having all of the benefits it offers, when the security of all the prior releases were NO BETTER than KDE-3.1?!?!  Again, IF THE SAME SECURITY ISSUES WERE NOT PRESENT IN THEM, THE FIXES WOULD NOT HAVE BEEN RETROFITTED!!"
    author: "Xavian-Anderson Macpherson"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "Ok, sorry for mixing KDE with Suse issues.\n\nBut one thing I cannot let go unnoticed. It's not like the KDE 3.0.0 shipped with 8.0 was tested much. I encountered it at work:\n\na) It didn't save settings.\nb) Suse control modules failed to load in kcontrol.\n\nI really hope, Suse gets KDE 3.1 right. But would it hurt much for that, if KDE 3.1 was ready some weeks before Suse 9.0 ?\n\nYours, \nKay"
    author: "Debian User"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "> But it's no secret how Suse manpower is the one making decisions about KDE releases, is it?\n\nIt's written SuSE. And if it's no secret how it comes that nobody knows that? Perhaps because it's untrue?\n\n> I find it strange that at the END of a feature freeze, bug fix cycle, a Suse employee proposes a further delay for a code audit to fix bugs that current stable releases already have.\n\nYou mean Dirk M\u00fcller? I don't think that he is employed by SuSE. If you would have picked the previous or the next release coordinator but with this one you fail."
    author: "Anonymous"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "So you want them to release a version with known security issues?! Is that a responsible thing to do? What they have decided is obviously the right thing to do no matter who proposed it. \n\nUntil you provide some solid evidence, your talk about Suse controlling the KDE project is pure FUD, so bring on the evidence or keep quiet.\n\nFurthermore, it's not like you can't get KDE 3.1 - just grab it - the RC5 version (which would have otherwise been the 3.1 final) is available, so what is your problem exactly? "
    author: "joergen ramskov"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "Typical for a debian user - paranoid & hungry for pre-final releases. Not very surprising \nsince debian distro's are known for being in a permanent pre-release state (something for people \nwhich prefer working on the system over working with the system).\nIMHO you did a very,very good job! Who wants the new kde should compile it - there have been enough\nrelease candidates (aka pre-releases) - but for KDE's PR its better to get a final what's worth calling it -\nand not a final released with a security bug list from network & software security experts. \nFor the Suse guy's & ladies: Thank you for spending so much manpower & knowledge for this project!  "
    author: "Ruediger Knoerig"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-10
    body: "That person is NOT a typical debian user and such a moron would be tore apart on #debian-kde or on the lists for spouting bull like that. All the debian-kde people I know are happy the release is being delayed until these security problems can be sorted out. "
    author: "kosh"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-10
    body: "\nHi there,\n\nThis is clearly part of why debian-kde is just talking. I just want to mention that KDE3.x is not allowed to enter Debian unstable before the gcc3.2.x transition is done. \n\nThere is no technical reason to bind these issues.\n\nAnd sadly enough, the transition won't happen this year, maybe next year, maybe....\n\nDebian really suffers from pseudo-explanations. I remember when KDE3 couldn't enter Debian because of many other things. Including and not limited SSL licence, Woody release, ....\n\nWe have a tendency to make up reasons for things we don't want yet. Yes, likely we don't have KDE packaging infrastructure in place yet. But we don't want to say so.\n\nThe thing I love about Debian is that once it IS in place, it will be great. It is not now. You will see, even after gcc3.2 transition, there will be other reasons...\n\nThanks, Kay"
    author: "Debian User"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-10
    body: "Do you know what a SONAME is?\nDo you really expect Debian to distribute things prohibted in the license?"
    author: "Debian user"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-10
    body: "Please go ahead and tell Suse how illegal distribution of KDE is."
    author: "Debian User"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "The KDE developers that work at SuSE in general were KDE developers first and SuSE employees later.  I believe that their priorities follow that pattern (specifically for KDE issues).\n\nFor instance for 3.0 I remember Waldo advocating setting things back a bit even thought that was *not* convenient for SuSE, which was trying to get 8.0 out the door.\n\nAgain, most of these folks are KDE developers that were hired by SuSE *because* of their commitment to KDE.  If SuSE hires some of the more dedicated KDE developers, of course SuSE employees have a say in important KDE issues, but please keep in mind \"SuSE employees\" != \"SuSE\".\n\nIf you don't like this, well, lobby for other companies to pay people to work on KDE.  ;-)"
    author: "Scott Wheeler"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "I even dare to say: If SuSE really employs 50+% of the KDE core developers then they deserve it \nto be slightly ahead of the competition.\n\nMarc\n(Not a SuSE user)"
    author: "Marc"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-11
    body: "bingo! :-D\n\n/kidcat\n(not a suse user either)"
    author: "kidcat"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "There will be updates for KDE 3.0 and KDE 2.2 shortly. In fact one of the reasons I wanted to delay 3.1 is that that allows us (KDE) to focus on those security updates first.\n\nCheers,\nWaldo\n-- \nbastian@kde.org -=|[ SuSE, The Linux Desktop Experts ]|=- bastian@suse.com"
    author: "Waldo Bastian"
  - subject: "GNOME fud"
    date: 2002-12-09
    body: "Just another GNOME user jealous at the fact that KDE is open and isn't controlled by lethargic companies like Ximian, SUN and RedHat."
    author: "ac"
  - subject: "Troll Detector"
    date: 2002-12-09
    body: "**BEEP**\n**BEEP**\n**BEEP**"
    author: "AC"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "Well, first I do agree that those problems should be found earlier...\nI don't like the fact RC releases have no binaries too, I predict (but hope I'm wrong) lots of bugs will be in 3.1 final because of this, let's hope not.\n\nBut between this and saying SuSe controls KDE releases there is a long way.\nI was called as troll because asumptions that if binaries where made those security problems would not be found anyway, I asked explanation (open-source coders don't test the program for security problem, but audit the source they told me, and I humbly asked escuse, but I still belive other bugs are there).\nWhat does it have to do?\n\nSimply, sometimes we think we know more than other people but we don't! And we must ask cordially for those people explain to us how things really are, so calm down, if people of KDE team says that Suse have anything to do with this.\n\nSo I thanks Waldo for enlightenning (did I wrote right?) us :)"
    author: "protoman"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-09
    body: "Even though this assertion has been rebutted it is still remarkable for the perspective and the clear ignorance behind it. SuSE is not the only company supporting KDE development anyway. Mandrake, TrollTech and others have. theKompany has sponsored KDE development and my company, Kitty Hooch (kittyhooch.com), continues to sponsor at least one full time developer on Quanta.\n\nIgnorance is not such a bad thing unless it resists knowledge. We all start out ignorant. Here's what I learned... One full time developer is easily worth 10 or more \"spare time programmers\". That is not an insult to those who volunteer. They simply don't have their head in the code as much and don't have the momentum so they are often less efficient in the time they do have. It's human nature. However beyond that, without someone out in front taking a lead in development the entire process tends to fall apart and grind to a halt! Starting a large project back in motion can take months to get it back to a good momentum.\n\nIn light of what I know now after several years managing a project the value of those full time people to KDE and it's individual projects is beyond estimation. In many cases it is the difference between a project exploding or fading away. For this reason, even if the absurd assertion that started this thread were true, it seems that we all owe companies like SuSE and Mandrake a great debt of grattitude. Especially since they allow these programmers to simply follow their passion as I do with Andras. If I were to schedule a release to better enable me to pay Andras (I don't) I would find it hard to believe that anyone would be upset given that it would take several times as long and be far worse quality without him. \n\nIt is hard to believe there are KDE users ready to bite the hand that feeds them. It has not been easy to pay Andras this year but I was glad to do it. If there were as many contributors who felt as strong as the critics KDE would have more polished full featured apps than we could imagine. If you want things sooner make it possible for more people to work full time instead of attacking those companies who already are doing a lot."
    author: "Eric Laffoon"
  - subject: "Re: Suse didn't want it"
    date: 2002-12-10
    body: "Kay,\nYou're full of shit. The stated reasons are entirely correct, as I watched the whole thing unfold on kde-packager. Go away.\n\nScant regards,\nDaniel, Debian KDE co-maintainer"
    author: "Daniel Stone"
  - subject: "problem compiling kdelibs on mdk 8.2"
    date: 2002-12-09
    body: "I got a 1.3Ghz K7 on my work, so I decided to compile it, but.......\nAnyone have a solution for this?\n\n/bin/sh ../libtool --silent --mode=compile --tag=CXX g++ -DHAVE_CONFIG_H -I. -I. -I.. -I../kdefx -I../interfaces -I../dcop -I../libltdl -I../kdecore -I../kdeui -I../kio -I../kio/kio -I../kio/kfile -I.. -I/usr/lib/qt31/include -I/usr/X11R6/include -I/opt/kde31/include   -DQT_THREAD_SUPPORT  -D_REENTRANT   -Wnon-virtual-dtor -Wno-long-long -Wundef -Wall -pedantic -W -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -ansi -D_XOPEN_SOURCE=500 -D_BSD_SOURCE -Wcast-align -Wconversion -O2 -fno-exceptions -fno-check-new  -DQT_NO_TRANSLATION -DQT_CLEAN_NAMESPACE -DQT_NO_ASCII_CAST -DQT_NO_COMPAT  -c -o kedittoolbar.lo `test -f 'kedittoolbar.cpp' || echo './'`kedittoolbar.cpp\nkedittoolbar.cpp: In method `bool KEditToolbarWidget::save ()':\nkxmlguiclient.h:284: `void KXMLGUIClient::setXMLFile (const QString &,\nbool = false, bool = true)' is protected\nkedittoolbar.cpp:399: within this context\nkxmlguiclient.h:284: `void KXMLGUIClient::setXMLFile (const QString &,\nbool = false, bool = true)' is protected\nkedittoolbar.cpp:402: within this context\n"
    author: "protoman"
  - subject: "Re: problem compiling kdelibs on mdk 8.2"
    date: 2002-12-09
    body: "Sorry, there is already a message about how to fix this, I didn't saw that.\n"
    author: "protoman"
  - subject: "kdebindings question"
    date: 2002-12-09
    body: "As I understand it there is a problem with kdebindings (there is certainly\nat least one open \"it does not compile\" bug) which have stopped those building\nthe Debian packages from including kdebindings.  For some reason kdebindings\nhas never been included in the Debian packages, and it would be really neat\nto get included this time.  Any chance of getting so it builds cleanly enough\nfor them to include it?\n"
    author: "David Goodenough"
  - subject: "Re: kdebindings question"
    date: 2002-12-10
    body: "I think most of kdebindings should build, apart from qtobjc and kdeobjc (those aren't built by default).\n\nNote that the license for qtjava and kdejava has been changed from GPL to LGPL. I hope that helps with any SWT licensing issues. I'm keen on looking into what's involved in doing a kde-swt binding in the new year - a KDE version of Eclipse would be really neat.\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: kdebindings question"
    date: 2002-12-11
    body: "Richard, I'm interested in learning to build language bindings for qt and kde. I would especially like to build bindings for common lisp, which I'm just now learning and enjoying thoroughly. I really don't know where to begin and I'm a new programmer who has never attempted to bind a language... can you point me to some literature that might help me understand the big picture of how language bindings are designed? So far googling has turned up nothing in terms of an introduction to the subject. It would be nice to have some background before I try to grok the details. Thanks!"
    author: "newguy"
  - subject: "Re: kdebindings question"
    date: 2002-12-11
    body: "The only general purpose bindings generation tool I know of is SWIG, but I don't think that is powerful enough to wrap the large Qt/KDE apis (c. 750 classes, 14000 methods).\n\nThe tool I use is called kalyptus (in kdebindings), which was derived from a documentation generator, kdoc. You can run some headers through that and see what sort of output it produces (eg use options -fjava or -fc to generate java or C).\n\nThe PerlQt bindings use a library called SMOKE, which is language independent. I think the best way to do bindings for Common Lisp would be to use that - the language should be dynamic enough. There is a kde-perl mailing list where you can discuss SMOKE with Ashley Winters who came up with the idea\n\n-- Richard"
    author: "Richard Dale"
  - subject: "Re: kdebindings question"
    date: 2002-12-13
    body: "Thanks. SMOKE looks interesting."
    author: "newguy"
  - subject: "Wallpaper (background) control panel module"
    date: 2002-12-10
    body: "Does anyone know why they went back to the old (imho lame) wallpaper control panel module?  I loved the one in beta2, and if they don't put it back in I'll probably end up hacking the one from beta2 into my 3.1 installation ;)\n\nThe thing I really liked about the wall-paper module from beta2 was that if I had multiple wall-papers selected I could instantly change between them by just clicking on the image name I wanted and then \"apply\".  The wall-paper module in rc5/3.0.5 does not allow this.  There is no way for me to force the display of any given wall-paper without either waiting ~100 hours for it to choose it on its own or going back to the \"single wall-paper\" mode.\n\nBrandon"
    author: "Brandon Zehm"
  - subject: "Re: Wallpaper (background) control panel module"
    date: 2002-12-10
    body: "Because the new one was functionally broken in several ways."
    author: "Anonymous"
  - subject: "fonts, cpu and icons"
    date: 2002-12-10
    body: "I compiled qt 3.1 and kdelibs/kdebase/arts 3.1.\nBut now I can't get antialiased fonts. I probally missed something in compilation.\nCan anyone please tell me how I compile qt with antialias support?\n\nMore, when compiling I noticed no cpu flags where passed to the compiler. Is that right? In this case how can I make it compile for athlon to get more speed?\n\nAnd for last... icons look terrible :( Only win2k icons look well in kde 3.1, any tipo about this?\n\nThanks in advance."
    author: "protoman"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-10
    body: "Probably you just need to recompile Qt with xft/xrender support. I can't say for certain, because gentoo automates all this away (for better? for worse?) but I've gotten the impression reading the kde lists that the configure script for qt 3.1 no longer turns it on by default. \n\nI could be wrong about the configure -- but this is clearly a lack of xrender support in qt."
    author: "Shamyl Zakariya"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-10
    body: "Hum, I enabled xft, but didn't remeber do see xrender.\nI'll try and inform about it laqter if works, or not."
    author: "protoman"
  - subject: "No, still no good"
    date: 2002-12-10
    body: "I included xrender, make sure it was enabled (I even have the translucent menus with xrender).\nI have xft support because I'm getting ttf fonts in kde. The only thing is that the fonts aren't antialiased.\nI have $QT_XFT set to 1, I have xft enabled in my ~/.qt/qtrc...\nOn kde3.0 (that I still have installed) I have antialiased fonts...\n\nSo I have no clue on what's wrong :)\nMaybe I should do a clean recompilation of QT... but that would make no sense at all...."
    author: "protoman"
  - subject: "Re: No, still no good"
    date: 2002-12-10
    body: "Hum, no.\nMy mistake, it was set to use software transparency.\nQT isn't compiling with xrender, so I'll try recompiling it all....\nMan this will take a lot of time! :("
    author: "protoman"
  - subject: "Re: No, still no good"
    date: 2002-12-11
    body: "Yeah, recompiled everthing from zero and worked.\nToo bad qt 3.1 ships with this bug in configure :("
    author: "protoman"
  - subject: "Re: No, still no good"
    date: 2002-12-11
    body: "Hi,\n\nI ran into the same problem.  Can you please tell me the exact options you used to compile qt?\n\nMany thanks!"
    author: "huangdi"
  - subject: "Re: No, still no good"
    date: 2002-12-11
    body: "Sure:\n./configure -thread -prefix /usr/lib/qt31 -cups -xft -xrender\n\nthe -thread, -prefix and -cups options would make no difference, the thing is -xft and -xrender.\nAnd note that you make to do a make clean and then recompile everthing (I know, I know, it took me 4 hours on a 1.3 Ghz athlon)."
    author: "protoman"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-12
    body: "To optimize for your CPU when building KDE you need to set a few environment variables before running \"configure\" and \"make\". The env vars you need to set are CFLAGS, CPPFLAGS and CXXFLAGS. The values you can put in there depend a bit on what your compiler supports. On my box (1.4GHz Athlon) using the gcc 2.95.3 compiler I use these values : CFLAGS=\"-O2 -march=k6 -mcpu=k6\" (same for the two others).  Newer compilers like gcc 3.2.x have better support for newer CPU's and support flags like -march=athlon, -march=athlon-xp etc... Check the gcc manual for your version of your compiler and see what options it can handle for -march= and -mcpu=.  Also, I'd recommend only using -O2 and not the -O3 or higher optimizations, since higher than -O2 sometimes break things while -O2 is safe (at least that's my experience)."
    author: "Jesper Juhl"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-12
    body: "Thanks, I got it.\nI don't see many speed diference, but I assume it's better to compile for k6 anyway :)"
    author: "protoman"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-12
    body: "If you have a newer compiler you'll see a greater difference by using one of the newer -march=athlon or -march=athlon-xp (or -march=i686 , -march=pentiumpro etc if you have an Intel CPU) options. \n"
    author: "Jesper Juhl"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-12
    body: "I have a gcc-3.2, but I don't know how to make qt use it.\n\nIt always use g++ (I have both, the 3.2 is g++-3.2) even that I set CXX, and there is no option in configure to change it.\nAs I have to compile qt with the same gcc as the rest of kde... "
    author: "protoman"
  - subject: "Re: fonts, cpu and icons"
    date: 2002-12-12
    body: "Stfw. Hint: mkspecs/linux-g++/qmake.conf\n\nHTH. HAND.\n\nJ"
    author: "Jaana"
  - subject: "Re: fonts, cpu and icons"
    date: 2003-08-04
    body: "Which option better: \"-march=athlon\" or \"-march=athlon-tbird\" for my AMD athlon processor 650 mhz?\n"
    author: "Pavel"
  - subject: "Cooker & Antialiased Fonts"
    date: 2002-12-10
    body: "I have been running KDE 3.0.4 for ML 9.0, with beautiful antialiased fonts. However, an upgrade to KDE 3.1rc5 from cooker (yeah, I know, it is beta) ruined the antialising, which still works for instance in Gnome's Nautilus. Any ideas ?. Thanks !"
    author: "NewMandrakeUser"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-11
    body: "gnome's antialias is not connected to kde antialias , i think you know that and you wrote it nevertheless.\n\nchris"
    author: "ch"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-11
    body: "Yes Chris, I know that but I didn't mean to bash anyone. My only point is that the upgrade didn't hurt X itself. I guess the 3.1 rc's are being compiled without antialiasing flags, but I wanted to hear someone else's experience. KDE 3.1 is JUST AMAZING, I love it, and it is loading much faster than 3.0.* in my athlon box :-)"
    author: "NewMandrakeUser"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-11
    body: "I had this problem (look at above's message).\nBasically xrender wasn't being compiled in QT 3.1 because of a bug in configure (it says it compile with it by default but it dosen't, you have to put the xrender option in configure).\nTry to use translucents menus with xrender, or look if your icons aren't a bad on borders. If menu looks empty or icons borders aren't nice, your qt is missing xrender.\n\nThis made me recompiling ALL qt again (sight) and then worked.\n"
    author: "protoman"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-11
    body: "Thanks a lot protoman !. I just read the thread :-) . I think I'll wait until the fine mandrake folks recompile it (I am too busy to do it myself right now) ;-)"
    author: "NewMandrakeUser"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-11
    body: "You're welcome.\n\nI don't know if by default kde builds binaries for your architerure (I didn't see any -march=athlon on compilation), so maybe it just builds for i386 (anyone knows this?).\n\nIf that's the case and you use mandrake 8.2 and have a good internet connection I can place the sources+binaries on a http (they're come with a go.sh that have all compilation options I used).\nMy mdk 8.2 have some upgrated packages (from security and few others as samba that I can tell you where I got)."
    author: "protoman"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-11
    body: "Oh yeah.\nIf there is a easy way (and someone teach it to me) to make the sources into rpms I can gladly make it."
    author: "protoman"
  - subject: "Re: Cooker & Antialiased Fonts"
    date: 2002-12-19
    body: "Thanks a lot, but it won't be necessary. I found a post in cooker's list with the recipe to fix it: I just ran fc-cache, and that configured fontconfig properly. Cheers :-)"
    author: "NewMandrakeUser"
  - subject: "Hats off to KDE"
    date: 2002-12-11
    body: "I wanted to start by congrats to KDE, it takes a lot of courage to pull a working and viable entity off the shelves and patch it later (read: i now fully beleive the linux world IS bettter than the \"other guys\")\n\nNow admittedly, I am a near linux/power hungry newbie, but I need to ask: does it really matter if your x comes up 0.1ms slower if it means that it is more secure? I am mainly writing in reponse to those who are raving that 3.1 loads faster. Personally, I would eat a few extra seconds if it meant that everything I need to be secure loaded everytime. If you happen to make something load faster, but without sacrificing security/functionality, then I would love to see your code."
    author: "noSunlightWorksForMe"
  - subject: "Re: Hats off to KDE"
    date: 2002-12-11
    body: "Well, never undeestimate the effect of 1ms on a p4@3Gz on a 486 SX system ;)"
    author: "zelegans"
  - subject: "Re: Hats off to KDE"
    date: 2002-12-12
    body: "true enough, but you see the point. Loading linux up everyday for a typical use where it is being shutdown every night, well, this is still going to be a slow process.\n\nyou still need to load a keyboard in order for it to work properly, there isn't much you can do to prevent that. now if you need to load on more module in order for the computer to be safe from some of the people in this thread, then this is where my point comes into play."
    author: "NoSunlightWorksForMe"
  - subject: "Looks good, but bloated."
    date: 2002-12-12
    body: "I'm running RC5, and I've noticed a lot of improvements in things that I use.\n\nHowever, I also noticed how much longer this release took to compile than other releases.  KDE is getting way too fat with a high number of apps few people ever use.  I hope that the KDE team does some research and a policy shift soon, and starts trimming the fat.\n\nI would also like to see a release branch where there's a feature freeze and a focus on stability, reducing the memory footprint, and increasing performance.\n\nOther than that, I have very little to complain about.  KDE is (imo) clearly the best desktop available."
    author: "Absinthe"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-12
    body: "I agree. And the bad thing is that it was very easy to prevent this problem, just by doing separeted packages instead of a few with lots of things in side.\nFor example: I never use noatun, but as I don't wanted to crash anything by removing it from makefile, there was I compiling something I don't use..."
    author: "protoman"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "> I never use noatun, but as I don't wanted to crash anything by removing it from makefile, there was I compiling something I don't use...\n#####\nYou certainly don't configure by hand every time, but use a custom configure script instead, no? Simply add a line like this before the configure command: \nexport DO_NOT_COMPILE=\"noatun\""
    author: "Melchior FRANZ"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "I think it's more appropriate to have the user set what they'd like installed than to make them turn off what they don't.  And there are, between all of the KDE bundles, so many applications that this is very tedious.\n\nIMO, the solution is to narrow the focus of these bundles down to the most important, most needed/wanted applications.  Move the rest of them out.  The developers of those applications might be offended, but it's the right thing to do.  KDE's getting porky.\n\nAnd you know what they say.  You can't teach a pig how to sing.   It wastes your time, and it annoys the pig.\n"
    author: "Dylan Carlson"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "Maybe someone should create a ncurses program for running KDE configure scripts that have all those kind of options?\nIt would help people a lot IMHO.\n\nThere was a discussion about what matters, binaries or sources, for the end-user in kde-core-devel.\nWell, take the number of hidden options that exists in configure (most users dosen't even know about export) and I tell you: what matters are binaries :("
    author: "protoman"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "\"KDE is getting way too fat with a high number of apps few people ever use.\".\n\nMy view is the more applications, the better. KDE can be packaged to only include the basic desktop and no/few applications, or packaged to include everything. \n\nSome distributions make seperate packages for all programs (i.e. noatun has it's own RPM), some only make one large package for every KDE package (i.e. kdemultimedia is just one big package). This is a packing issue.\n\nMy view is that KDE should include as much as possible, giveing users the option to choose what parts they want. Noone is forceing you to install everything. \n\nI'm no computer wizard, but even I have figured out how to split the KDE packages into seperate, smaller RPM's giveing my girlfriend, parents and friends the ability to choose what spesific parts of KDE they want.\n\nI have been useing KDE since 2.2, and spendt much time on the new searching for good KDE apps for everything. Alot of the stuff I've downloaded seperatelly over the years has now made it into KDE, and I think that's just great. No more browsing every apps homepage to check for updates, just upgrade KDE and get the latest stable versions of everything (and again, it -is- optionall if I want to install these parts or not). \n\nI think KDE should have ONE (not two, three, just the best one) application for everything as standard."
    author: "\u00d8yvind S\u00e6ther"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "I can't disagree more.  If you asked around, I think you'd find most people would disagree also.\n\nPeople should not have to make subpackages out of the various KDE suites to eliminate the applications that, when it comes down to it, most people realistically don't use.  This may be anecdotal, but just about everyone I've talked to has complained about KDE bloat, many moreso than I ever have.\n\nPeople don't really have a lot of choice when it comes to narrowing that stuff down.  It takes over 9 hours on a 1.5G machine to install most of the packages associated with a KDE release.  \n\nCall me crazy, but that's excessive for a desktop distribution, at least half of the applications of which are not widely used.  \n\n1.  It's a manual process for those who compile KDE to remove them (if it is in fact possible).\n\n2.  The makers of binary packages for linux distros and other unix platforms alike have to default to compiling everything, and therefore forcing this bloat upon their users.\n\n3.  Most people are not rolling custom RPM's of the KDE stuff for their friends and neighbors, and most people are not the beneficiaries of such packages.   Most people are getting everything + the kitchen sink.\n\nTake it for what it's worth.    KDE should focus on building a good, stable, small footprint core, and let people tack on additional applications with secondary packages as it suits them.\n\nThrowing every little kde application in the world into these bundles might be great for having a lot of bullets on a Gnome vs KDE comparison chart, or a presentation or something -- but ultimately it results in a lot of wasted bandwidth, CPU time, disk space and unnecessarily complicates an otherwise gorgeous desktop suite.\n\nI would appeal very strongly to the KDE committee to make some adjustments in this way."
    author: "Dylan Carlson"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "Well, a nice thing would be to have a small \"[kx ]dialog\" based script/makefile-thingy, which asks which parts you want to compile.\nWould be nice if somebody would try to implement something like this.\n\nLike ./configure --ask-for-packages\nand then a dialog pops up where you can select/deselect the various subdirs.\n\nAlex"
    author: "aleXXX"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "Yes. The word is focus.\nBut even limiting to a maximum of 3 per category would be an\nbe a big improvement. This also applies to the Distros.\nFor the intial install less is more.\nLater one can install any package you want."
    author: "penkuin"
  - subject: "Re: Looks good, but bloated."
    date: 2002-12-13
    body: "Cant - per package group - there be voted what packages go in it? So each new KDE release would be released via publicly the current most populair KDE based apps?\n\nThis would strip at least some bloat for each package group."
    author: "IR"
  - subject: "GAH!"
    date: 2002-12-14
    body: "I'm just going to wait for KDE 3.1. I'm running RC3 right now, and it works great. I've got a question though: Will there be patches? I'd hate downloading all the code on 56k again."
    author: "crichards"
  - subject: "Download is a love act !!!"
    date: 2002-12-26
    body: "Do it again, baby ...\n\nJust do it again ...\n\nDownload is a great love act ..."
    author: "Lover John"
  - subject: "From an end-user.. Thanks!"
    date: 2002-12-16
    body: "As an end-users and dedicated KDE user I think that waiting to release the next version a month or two is absolutely the right decision.  I have migrated from the land of Redmondians because I get sick and tired of over-priced and underperforming products.  If Bill, Steve and their many men of merry would have taking the time to develop and audit their software properly they wouldn't be getting such bad press and putting their clientele's data at risk.  I can wait for better coding.  KDE 3.0x is working just fine for right now.  Sure, I want all the improvements I've been reading about.  But, It just goes to show that you can have it feature rich and fast or you can take your time and have a better product with new features and better code/security by doing it right in the first place.  Keep up the good work!\n\nThanks again for giving me an alternative that works. :)"
    author: "Curtis Rey"
  - subject: "Kde3.1rc5-MandrakeRPM, \"missing\" file, solution! ;"
    date: 2002-12-31
    body: "\nif these RPMs complain that libart_lgpl2-2.so.2 is missing, get it from:\n\nftp://rpmfind.net/linux/KDE/stable/koffice-1.2/Mandrake/8.2/libart_lgpl2-2.3.10-2mdk.i586.rpm\n\njust installed mdk to try out kde3.1rc5, and didn't want to install all the \"bulk\" and scrapped some apps during the installation... anyways, i think there should be a note with the binaries saying that this rpm is needed in order to run this kde release (without it, when you type \"startx\" it just quits after briefly showing the X screen, without displaying any error message...).\nno reply necessary (i guess) - just actions (as suggested above) should be taken ;)\n"
    author: "MaxPayne"
  - subject: "security fixes"
    date: 2003-01-03
    body: "dunno about you but i have been a kde user since 2.2 and there is one thing im sure of : even after they complete their audit and delay release, the damn thing will still have bugs, will still crash, and will still eventually be found to have security holes, so just release the damn thing.\n:p\nif it actually is solid, i will sh*t a gold brick and send them a chunk of it.\n\n"
    author: "anonymous loser"
---
The much-anticipated release of KDE 3.1, originally
<a href="http://developer.kde.org/development-versions/kde-3.1-release-plan.html">scheduled</a>
for this week, has been delayed, most likely until early next month.
On the positive side, the delay could not have been for a better reason.
Dirk Mueller, the KDE 3.1 Release Coordinator,
<a href="http://lists.kde.org/?l=kde-core-devel&m=103913196531620&w=2">explained</a>
that the delay was caused by a security audit of the 3.1 CVS tree.  The audit was prompted by the identification of a class of vulnerabilities by
FozZy from the "Hackademy Audit Project" (thanks to FozZy and all others
who help identify security issues in KDE, and a <em>big</em> thanks to Dirk
Mueller, Waldo Bastian, George Staikos, Lubos Lunak and the others
who are leading or helping in the current security audit).
After discussing the issues with the packaging engineers and KDE
developers, and in
light of the upcoming year-end Holidays, the decision was virtually
unanimous to wait until early January for the official 3.1 release.

<!--break-->
<p>
While the decision was a difficult one, and is sure to disappoint quite
some people, hats off to the KDE Project for making the right decision
and treating security with the utmost importance that is warranted.
The security fixes will be backported to KDE 2.2.2.
<p>
In the meantime, what was to have been KDE 3.1 (with some, but obviously
not all, of the security audit completed) has been re-tagged as
<a href="http://www.kde.org/info/3.1.html">KDE 3.1 RC5</a> and is now <a href="http://download.kde.org/unstable/kde-3.1-rc5/src/">available for testing</a>.  The KDE Project
hopes that with this release more bugs will be found and reported by the
community so they can be fixed while the security audit continues.
Stay tuned.


