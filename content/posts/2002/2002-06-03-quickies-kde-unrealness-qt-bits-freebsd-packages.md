---
title: "Quickies: KDE Unrealness, Qt Bits, FreeBSD Packages"
date:    2002-06-03
authors:
  - "numanee"
slug:    quickies-kde-unrealness-qt-bits-freebsd-packages
comments:
  - subject: "KDE FreeBSD packages."
    date: 2002-06-03
    body: "Please don't take this is as destructive criticism, but....\n\nwhenever I've tried KDE FreeBSD packages, they have always required higher version numbers of everything than are in the ports or packages collection. Releasing KDE FreeBSD packages is cool and everything, but what's the point if we'll need to compile everything else manually to get it to work? Just a thought."
    author: "Urugern"
  - subject: "Re: KDE FreeBSD packages."
    date: 2002-06-03
    body: "Are you sure you are using the STABLE packages instead of the CURRENT packages?  I know, it can be confusing..."
    author: "ac"
  - subject: "Re: KDE FreeBSD packages."
    date: 2002-06-03
    body: "I'm not sure what you are talking about? With the kde 3 releases, packages for Every dependency were publicly available on ftp.kde.org.\nSo you could just remove your old stuff and install the new ones without any problems..."
    author: "blashyrkh"
  - subject: "Re: KDE FreeBSD packages."
    date: 2002-06-04
    body: "All the dependencies are also on mango.firepipe.net.  If you follow the instructions, i.e. setenv PACKAGESITE to the right place, and then \"pkg_add -r kde\", all your dependencies would be updated along with all of KDE.\n\nYou don't need to compile *anything* manually, we use a currently cvsup'ed copy of the ports tree, the only thing newer in this set than you would normally have, is KDE itself.\n\n"
    author: "Lauri Watts"
  - subject: "Congratulations on your awards"
    date: 2002-06-03
    body: "The awards page (<A href=\"http://www.kde.org/awards.html\">http://www.kde.org/awards.html</a>) hasn't been updated for some time."
    author: "Per"
  - subject: "Unicode fonts"
    date: 2002-06-04
    body: "What is the recommended Unicode font for KDE?  The BibleTime people have CODE2000 linked, but it's not free or Free.  Any hints?"
    author: "wilco"
  - subject: "Re: Unicode fonts"
    date: 2002-06-04
    body: "CODE2000 is free for download.\n"
    author: "ac"
  - subject: "Re: Unicode fonts"
    date: 2002-06-04
    body: "Indeed, Code 2000 is freeware.  The only other reasonably complete Unicode fonts are Arial Unicode MS (which is from MS and is only available as a Win32 self-installer, though you can copy it from an existing Win32 install over to Linux) and Bitstream Cyberbit (which is commercial, albeit free to download).\n\nFonts are a tough issue in free software.  Good fonts cost money.  Free fonts tend to be of very poor quality (often just bitmap fonts) or very incomplete from a Unicode perspective (though it's certainly not a requirement that Unicode fonts should contain the whole BMP like some of these attempt to do).\n\nI think in the future, you will find BibleTime supporting different fonts for different modules so that, e.g., a Syriac module only needs a font with good Syriac support and a Tamil module only needs a font with Tamil codepoints.  This should solve some of the problem since there are a number of good free fonts that cover smaller ranges at least.\n\nIHS,\nChris\n"
    author: "Chris Little"
  - subject: "Re: Unicode fonts"
    date: 2002-06-04
    body: "Try http://www.ccss.de/slovo/unifonts.htm for a list of more or less free Unicode fonts.\n\nRegards,\nMartin"
    author: "Martin Gruner"
  - subject: "Unreal 2 sshot"
    date: 2002-06-04
    body: "Are you sure it's kde? it looks more like gnome to me...and we can easily recognize wmaker and mc"
    author: "dob"
  - subject: "Re: Unreal 2 sshot"
    date: 2002-06-04
    body: "There are two screens shown on this screenshot\nthe left one uses windowmaker(see the clip?)\nthe right kde, look at the panel at the bottom, the leftmost icons are\nK, Home, KDE-Kontrol-Center, search"
    author: "Bert"
  - subject: "Re: Unreal 2 sshot"
    date: 2002-06-04
    body: "You have to remember it is a game. It definitly does have Kicker with the K button, but I think there may be elements of other GUIs as well.\n\nA similar example would be in the (hilarious) movie Office Space the computer shots are mostly Mac, but at one point he quits to a dos prompt and another point (while in the Mac GUI) he downloads a virsus from the floppy A:\\ drive."
    author: "Ian Monroe"
  - subject: "wow"
    date: 2002-06-04
    body: "soundgarden looks very nice now.. is is almost done now?"
    author: "fault"
  - subject: "Re: wow"
    date: 2002-06-04
    body: "I suppose you mean \"Rosegarden\", if so thanks for your appreciation, and no, although it's already useable for basic composition, it's far from done. You're very much welcome to try it, though.\n"
    author: "Guillaume Laurent"
  - subject: "Re: wow"
    date: 2002-06-04
    body: "> I suppose you mean \"Rosegarden\", \n\nOops.. this is what happens when you type things while you are half-awake :)"
    author: "fault"
  - subject: "Java equivalent to Signals and Slots?"
    date: 2002-06-04
    body: "Guillaume wrote on his page regarding Signals and Slots:\"Why do you think all newly created languages like Java or C# have this same kind of flexibility ?\"\n\nWhat I am missing until now in Java is something as nice as Qt Signals and Slot. Maybe I missed it, when I learned Java. I normally register Listeners (which always have to be of a certain type) and then I have to generate Events (or catch them).\nQts Signals and Slots are much easier. Anybody here to tell me what their Java equivalent is?"
    author: "ac"
  - subject: "Re: Java equivalent to Signals and Slots?"
    date: 2002-06-04
    body: "I assume when you write Java that you mean AWT/Swing. AFAIK there is no Slot/Signal  analog in AWT/Swing. However, there is a nice Java wrapper for QT/KDE written/maintained by Richard Dale, which of course mirrors the C++ QT/KDE APIs. It is part of the kdebindings module. "
    author: "AdHoc"
  - subject: "Re: Java equivalent to Signals and Slots?"
    date: 2002-06-04
    body: "Actually I was referring to the type-unsafety of the slot and signals (as opposed to libsig++ type-safety). Java doesn't have the equivalent of slot and signals, also implementing them would be quite doable.\n"
    author: "Guillaume Laurent"
  - subject: "Re: Java equivalent to Signals and Slots?"
    date: 2004-12-20
    body: "The closest java equivalent would be the JMS Messaging API provided in J2EE. "
    author: "Praveenkumar Begur"
---
<a href="mailto:joup@bigfoot.com">Joup</a> notes that the <a href="http://freebsd.kde.org/">KDE on FreeBSD</a> team is <a href="http://lists.csociety.org/pipermail/kde-freebsd/2002-June/001785.html">busy</a> 
readying <a href="http://mango.firepipe.net/">KDE 3.0.1 packages</a> and is in need
of serious testers.

<A href="mailto:evan@starnix.com">Evan Leibovitch</a> points us to his <a href="http://www.starnix.com/banks-n-browsers.html">Banks 'n' Browsers</a> webpage.  He surveys 300 banks in 30 countries but needs updates for KDE3.  We <a href="http://dot.kde.org/1008056893/">also reported</a> on <A href="http://home.in.tum.de/~strutyns/banking.php">this issue</a> a while back.



<a href="http://www.telegraph-road.org/">Guillaume Laurent</a> wrote in some time ago
to plug the new site design of the <a href="http://www.all-day-breakfast.com/rosegarden/">Rosegarden project</a> -- the MIDI, audio
sequencer and music notation editor for X.  The website looks great and <A href="http://www.all-day-breakfast.com/rosegarden/pictures.html">so does the project</a> -- developers welcome.
<!--break-->
<p>
On a Qt-related note, check out Guillaume's interesting <a href="http://www.telegraph-road.org/writings/gtkmm_vs_qt.html">article on Qt vs gtkmm</a>.  If you want to see Qt Unicode in action, Martin Gruner points us to <a href="http://www.bibletime.de/">BibleTime</a>'s awesome <a href=http://www.bibletime.de/gallery.shtml>gallery</a>.  Finally, <A href="mailto:markcox@iprimus.com.au">Mark Cox</a> brings the 
<a href="http://doc.trolltech.com/qq/qq01-seriously-weird-qregexp.html">Serious Weird QRegExp</a> tutorial to our attention.
<p>
<a href="mailto:fulgore@mindspring.com">Fulgore</a> wrote in to point out that the <a href="http://unitedlinux.com">UnitedLinux</a>
partnership involving SuSE, Caldera, TurboLinux and Conectiva,
has <a href="http://unitedlinux.com/en/release_plans/">specified KDE 3.0</a> as part of the distribution base, guaranteeing 
KDE3 core availability to developers targeting UnitedLinux-based distributions.
<p>
We failed to trumpet it at the time, but it seems that KDE has been awarded the prestigious title of "Open Source Project of the Year" and <a href="http://www.kdevelop.org/">KDevelop</a> was named "Best Development Tool" by <a href="http://www.linuxformat.co.uk/">Linux Format Magazine</a>. Check out the scans: <a href="http://static.kdenews.org/mirrors/www.chowells.uklinux.net/award1.jpg">award1.jpg</a> &amp; <a href="http://static.kdenews.org/mirrors/www.chowells.uklinux.net/award0.jpg">award0.jpg</a>. I dare say KDE3, too, has been extremely well received so far.
<p>
Finally, for something <i><a href="http://www.kde-look.org/">seriously cool</a></i>, Oblom points out this <a href="http://www.gamekult.com/tout/jeux/fiche_screenshots.html?cmedia=ME0000152671">screenshot</a> of Unreal II which brings a glimpse of KDE
to millions of Windows users.