---
title: "2002 KDE e.V. Meeting Report"
date:    2002-11-27
authors:
  - "chowells"
slug:    2002-kde-ev-meeting-report
comments:
  - subject: "Who's been putting LSD on my cornflakes?"
    date: 2002-11-27
    body: "\"Cornelius Schumacher did first steps towards turning KMail into a KPart\"\n\nGez, I thought I had something to do with that and had been happily using KMail, KAddressBook and KOrganizer integrated together in Kontact/Kaplan for over a month.\n\nDon."
    author: "Don"
  - subject: "Re: Who's been putting LSD on my cornflakes?"
    date: 2002-11-27
    body: "Erm, that was before you even started. Look at the date of the meeting. It was slightly before we discussed that. Cornelius tried to bring kmail towards a kpart, but discarded his changes as he found it needed some basic restructuring first.\n\nCheers,\n Daniel"
    author: "Daniel Molkentin"
  - subject: "Re: Who's been putting LSD on my cornflakes?"
    date: 2002-11-27
    body: "24-28 August, I see, that makes a lot more sense then.\n\nDon.\n"
    author: "Don"
  - subject: "KDE e.V.?"
    date: 2002-11-27
    body: "What does e.V. mean?"
    author: "cfcvs"
  - subject: "Re: KDE e.V.?"
    date: 2002-11-27
    body: "leo.org dictionary says:\n\neingetragener Verein [e.V.]\nregistered association\n"
    author: "Rainer Kiehne"
  - subject: "Congrats!"
    date: 2002-11-27
    body: "Congratulations to all the folks who attended, for getting so much done. A lot of the things that were achieved here can hardly be called \"hacker fun\", which makes this all the more impressive."
    author: "zero"
  - subject: "Group photo"
    date: 2002-11-30
    body: "Chris has style man, nice shirt (not that I and millions of other geeks don't have one just like it)....\n\nBero has the hip Stallman look going on... It's nice to see I'm not the only one :)\n\n"
    author: "David Nielsen"
  - subject: "bad news about kde"
    date: 2002-12-10
    body: "de sucks.\nit's really fucked up - use blackbox instead - its fast and secure!"
    author: "anonymous"
---
From the 24th to 28th of August 2002, members of KDE e.V. were invited to meet at the <a href="http://www.unibw-hamburg.de">University of the Federal Armed Forces</a> in Hamburg, Germany, to discuss various matters relating to KDE e.V. and KDE in general. A <a href="http://www.kde.org/kde-ev/about/2002meeting.php">full report</a> (with annotated <a href="http://developer.kde.org/~howells/kdeev-photos/1600x1200/group.jpg">group photo</a>) from this meeting is now available on the <a href="http://www.kde.org/kde-ev/">KDE e.V.</a> web site.
<!--break-->
