---
title: "Trademarks vs. Open Source:  SuSE Enjoined"
date:    2002-01-09
authors:
  - "Dre"
slug:    trademarks-vs-open-source-suse-enjoined
comments:
  - subject: "More info on /."
    date: 2002-01-09
    body: "Read this (Threshold: 5):\nhttp://slashdot.org/comments.pl?sid=25850&cid=0&pid=0&startat=&threshold=5&mode=thread&commentsort=0&op=Change\n\nThere are some comments about the laywer called Gravenreuth - apparently he is a \n\nHere is a quote:\n\"I don't know how many of you are aware of this, but this Gravenreuth guy, the attorney, is one of the most hated men in the German IT scene. He has been going to court over cases like these for at least ten years.\""
    author: "Joergen Ramskov"
  - subject: "Re: More info on /."
    date: 2002-01-09
    body: "I can confirm that he is hated, even outside of Germany."
    author: "bg"
  - subject: "Yes ... change the name to:"
    date: 2002-01-12
    body: "\"Gravenreuth\" the KDE drawing application.  The put the story of Gravenreuth in the about box. Don't call him an asshole or liable him just describe his activities and how it lead to the name change. Leave users to conclude how he has wasted taxpayers dollars ..."
    author: "NameSuggesterEngine"
  - subject: "KImageShop to Krayon to ??"
    date: 2002-01-09
    body: "So, once the program was renamed from KImageShop to Krayo. Now this must happen again ?"
    author: "art"
  - subject: "Do you remember Mr. 'ac' ?"
    date: 2002-01-09
    body: "On Tuesday, July 03, at 05:40 PM 'ac' wrote this on dot.kde.org:\n>Adobe is correct to defend its trademark. The KDE project has to come up with better names, like they already did with Krayon, etc.<\n\nOn Tuesday, July 03, at 08:18 PM 'antialias' wrote this on dot.kde.org:\n>Don't tell me we can't face the same problem if Krayon Computers, Krayon Technologies or Krayon Systems and Design Company decide to sue KDE because of using the name Krayon.<"
    author: "antialias"
  - subject: "Re: Do you remember Mr. 'ac' ?"
    date: 2002-01-09
    body: "Unfortunately, I was right :("
    author: "antialias"
  - subject: "Stop finding stupid names for KDE apps"
    date: 2002-01-09
    body: "Maybe a good solution for problems like this and the general confusion of users would be to stop inventing stupid names for software.\nIf you write a paint program, then just call it 'KDE Paint Program' or maybe KPaint, and a) you can be pretty sure that no one will sue you and b) everybody knows what your program does.\n\nCompanies invent names because they have to, to differentiate themselves from their competition, to get a well-known brand name, and they usually back it up with a large marketing budget. KDE doesn't and I am pretty sure that there are many apps out there that won't be used because the users don't know what they are good for. Think of Krayon, Noatun, Keystone, Kontour, Kate, Quanta, Kit, Kivio, Kugar... many users may have the app that they need on their harddisk and they won't even notice it. And so do i - 1 week ago I didn't know what Krayon is good for, and I still don't know what Kugar is.\n\n(you can, of course, still use these names internally, for CVS and so on; you have to in order to prevent nameclashes)"
    author: "AC"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-09
    body: "you are so right. we should just name it for it does. like if i have an illustration program i should call it killustrator! oh. wait. damn.\n\n;-P\n\nas for the \"i have no idea what that program does because the name doesn't enlighten me\" problem, KDE3's KMenu will be a bit more helpful in that area."
    author: "Aaron J. Seigo"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-09
    body: "> KDE3's KMenu\n\nWell this was supposed to be solved a long time ago.  Matthias Ettrich's idea was that what was shown in the KMenu should be something descriptive and *not* the binary name in the first place.\n\nNow it seems people have disregarded this advice and instead added more useless visual clutter to KDE?  Just an impression I get, I haven't actually checked.\n\n-N."
    author: "Navindra Umanee"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "> KMenu should be something descriptive and *not* the binary name in the first place.\n\nhmm..I always hate it when I have this package called X, which I cannot find in my menu because it is called Y. And then, if is happens to hang, I cannot kill -9 it because name X nor Y shows up when I run ksysguard (or any other process viewer). Finnaly (after killing half my desktop) I find out that the app was, logically ofcourse, called Z.\n\nI want to know what I am running, and the name should be the same _everywere_. If we want descriptional menus (wich is good) we should also want also discriptional names in packages and processes.\n\nDanny"
    author: "Danny"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "What's wrong with the Konqueror example?  \"Konqueror Web Browser\", for example?\nBesides if you know the binary name, you should do \"Alt-F2 binaryname\" :-)\n\nMaybe ksysguard can be hacked to do what you want, but a bit tough..."
    author: "Navindra Umanee"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-09
    body: "Yeah, I thought KIllustrator was a good name...   And I don't see any bone head lawyer raising issues again KPaint...  Hmm, looks a bit much like MS Paint to me!!!   I think the real issue is that this is allowed in Germany..."
    author: "Ill Logik"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-09
    body: "> 1 week ago I didn't know what Krayon is good for\n\nOf course you didn't, Krayon is NOT released !"
    author: "David Faure"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "I read the recent reports of the lawsuit and felt a slight pang of guilt. It's partly my fault, since I \"half-kiddingly\" suggested Krayon originally as a new name for KImageShop in the kimageshop mailing list.\n\nI hope Suse isn't mad at me ;)"
    author: "Sean Pecor"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-09
    body: "What speaks more to you ? KFM, or Konqueror ? Which name do you reasily remember among the two ?\nKFM was \"KDE File Manager\", just what you suggest above.\nKonqueror is quite known now, as being a good web browser.\nIf it was still named KFM, nobody would know about it (except for long-time KDE users)."
    author: "David Faure"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "> What speaks more to you ? KFM, or Konqueror ?\n\nKFM is bad as well. \"File Manager\" or \"Web Browser\" would more obvious to the newbie. Because Konqueror is used so often this is just the name that is the easiest to learn. But I can remember the time a year ago, when I was still using Gnome, that Konqueror was 'one of the programs starting with K' and I had no idea what it was. Even when somebody showed me KDE 2.0 later with Konqueror in the file manager mode I had no idea that it was also a web browser."
    author: "AC"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "My KDE menu lists \"Konqueror Web Browser\" and \"Konqueror File Browser\".  Seems perfectly acceptable to me..."
    author: "Navindra Umanee"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "Yes, more or less. My room mate would probably ask me whether a \"Konqueror\" web browser is different from a normal web browser, but at least you get an idea what it is."
    author: "AC"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "It's only  a simple convention that will take a few seconds to get used to, I'd say. You have to assume at least a certain amount of intelligence from the user."
    author: "Navindra Umanee"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-11
    body: "That's the wrong approach - most users don't want to think or even discover their desktop or find out why this thing is called \"Konqueror\". They only want to get the latest sport news on the ESPN web site."
    author: "AC"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-11
    body: "What's ESPN web site?\n\nSorry - you asked for it!"
    author: "Navindra Umanee"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "> If you write a paint program, then just call it 'KDE Paint Program' or maybe  KPaint, and a) you can be pretty sure that no one will sue you and b)everybody knows what your program does.\n\nWell, Linux and KDE were always a matter of choice. There will always be more than one paint program for KDE out there. How should the author call their programs? KPaint 1, KPaint 2, KPaint 3,.....?\nSo you just have to invent a name that might not be directly related to its functionality."
    author: "Christian Loose"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-01-10
    body: "Is is then time to then rename Linux, GNU, apache, mutt, mozilla,\nanaconda, amanda, and all the others?"
    author: "GL"
  - subject: "Re: Stop finding stupid names for KDE apps"
    date: 2002-12-10
    body: "Program names that don't mean anything aren't just limited to KDE though... these ones either don't mean anything at all or don't relate whatsoever to their purpose...\n\nMicrosoft Visio - Diagramming software\nMicrosoft Excel - Spreadsheet Software\nMicrosoft Access - Database software\nOpera - Web Browser\nMacromedia Dreamweaver - HTML editor\nMacromedia Fireworks - graphics program\nMacromedia Flash - animation software\nAdobe Acrobat - PDF viewer\nAdove GoLive - HTML editor\n\nI could go on for hours but I think I'll stop right there..."
    author: "Chris Hope"
  - subject: "Bad for distributors, or bad for Germany?"
    date: 2002-01-09
    body: "Maybe it will just make them think twice abour opening offices in Germany. Perhaps being content with exporting to the country is wise advice. After all, as long as you are in Europe, you won't have any problems with the borders.\n\nThis could be good news for Linux users in france. :-)"
    author: "J\u00e9r\u00f4me Loisel"
  - subject: "it's over..."
    date: 2002-01-09
    body: "look at www.crayon.de, seems like everything is settled and the preliminary injunction is over."
    author: "harryF"
  - subject: "Re: it's over..."
    date: 2002-01-10
    body: "Good news, really! So this firm is obviously not as crazy as Symicron (the most (in)famous client of Gravenreuth).\nI don't know what you are going to do now, but I'm going to write an email to Crayon, telling them (politely) why this wasn't a good idea in the first place.\n\n\tGuido"
    author: "Guido"
  - subject: "Chilling Effects"
    date: 2002-01-10
    body: "If KDE is truly concerned about chilling effects, then it should drop its own trademarking practices.\n\nI was a website owner have been approached about KDE trademarks in the past.\n\nLet's be consistent."
    author: "Neil Stevens"
  - subject: "Re: Chilling Effects"
    date: 2002-01-10
    body: "> If KDE is truly concerned about chilling effects, then it should drop its own trademarking practices.\n\nUnfortunately we can't do this because else we had no legal means to stop evil people from abusing \"KDE\". It's the same with \"Linux\". It's a registered trademark of Linus Torvalds. If you want to know why have a look at this:\n\nhttp://www.linux10.org/history/ (search for \"trademark\").\n\nThe same could happen to us if \"KDE\" wasn't already a registered trademark."
    author: "Ingo Kl\u00f6cker"
  - subject: "Differences in German law"
    date: 2002-01-10
    body: "From what I remember from the last time, German law is somewhat different wrt copyright infringement.  In Germany, _any_laywer_ and _independantly_ sue for copyright infringement without consulting the owner of the copyright.  Several law firms subsist on making frivioulous infringement lawsuits and then trying to extort settlement money from the accused or payment from the copyright holder.  This is likely going to be tossed into the gutter, just like the Kontour suit (which was _not_ authorized by Adobe IIRC) didn't seem to generate anything other than a name change (no settlement fees/fines)."
    author: "Raven667"
  - subject: "KILL-US-Tra(i)tor"
    date: 2002-01-10
    body: "KIllustrator is a fine name, and all Adobe products prefixes 'Adobe XXX' like \"Adobe Acrobat Reader\", So if KDE has \"KDE's Acrobat Reader\", it should not be a problem for Adobe.\n\nI Wonder why Microsoft is not Suing other office suites which are named 'Office' like StarOffice, KOffice, OpenOffice since these sounds more like Microsoft Office?\n\nThis is all BillShit or KBillShit ;)"
    author: "Asif Ali Rizwaan"
  - subject: "Active development?"
    date: 2002-01-10
    body: "Is Krayon not under development anymore? The Heise News says something like this."
    author: "Norbert"
---
According to a
<a href="http://www.zdnet.com/zdnn/stories/news/0,4586,5101397,00.html?chkpt=zdnnp1tp02">story</a>
at <a href="http://www.zdnet.com/">ZDNet</a> (earlier <a href="http://www.heise-online.de/newsticker/data/odi-07.01.02-000/">story</a> at
<a href="http://www.heise-online.de/">heise online</a>),
<a href="http://www.suse.com/">SuSE</a> has
been enjoined by a German court from distributing its distribution in
Germany, apparently because the court decided that the
<a href="http://www.koffice.org/">KOffice</a>
painting program <a href="http://www.koffice.org/krayon/">Krayon</a>
violates the <em>anonymous</em> plaintiff's trademark (the article says
copyright) in Crayon.  Fortunately, boxes already shipped to stores appear not to be
affected.  One cannot know for sure who is behind
this, but the rumour mill is placing bets on
<a href="http://www.crayon.de/">CRAYON</a>, a German website which sells
CDs with cartoons and other images.
This follows not long after another Germany-centered dispute
over the KOffice application
<a href="http://www.koffice.org/kontour/">Kontour</a> (f/k/a KIllustrator),
which
an <a href="http://www.adobe.com/">Adobe</a> lawyer claimed violated
Adobe's trademark in Illustrator.  Any German lawyer care to explain
why an anonymous plaintiff is permitted to get an
injunction, and how trademark law can be violated when it is quite
difficult to see any potential for confusion?  <strong>Update, Wednesday 
January 09, @10:23AM:</strong>  A more recent heise online <a href="http://www.heise-online.de/newsticker/data/odi-08.01.02-001/">story</a>
reveals that the trademark holder is <a href="http://www.seidel-online.de/">Seidel Softwareservice</a>, which
recently spun off the CRAYON website, and while Krayon in fact was not included
on the SuSE 7.3 CDs, apparently its menu entry was.  It also raises the
obvious point about the chilling effect actions like this might have on
the desire of Linux distributions to include a large number of software
packages on a CD. <strong>Update, Thursday 
January 10, @01:10AM:</strong>  SuSE has <a href="http://linuxtoday.com/news_story.php3?ltsn=2002-01-09-022-20-PR-SS-LL">announced</a> that the injunction has been removed (see also the heise online <a href="http://www.heise.de/newsticker/data/odi-09.01.02-003/">story</a>), <a href="http://www.crayon.de/">apparently</a> without having to pay any license fees to the plaintiff.



<!--break-->
