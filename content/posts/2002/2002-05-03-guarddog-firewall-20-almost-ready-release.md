---
title: "Guarddog Firewall 2.0 Almost Ready For Release"
date:    2002-05-03
authors:
  - "sedwards"
slug:    guarddog-firewall-20-almost-ready-release
comments:
  - subject: "a killer app"
    date: 2002-05-03
    body: "This app could be a killer app for Linux.  Think small Linux boxes acting as user-friendly firewalls. Think power of Linux firewalling with the user-friendliness of Windoze.  Hot stuff.\n\n"
    author: "ac"
  - subject: "excellent!"
    date: 2002-05-03
    body: "This is a great utility -- I'm using it for about a year already. No need to learn the lowlevel details of firewall configuration, that change with every major kernel version ;)\n\nThe only thing I'm missing is a better logging configuration. E.g. in the uni network, my logs get spammed quickly by some windows computers trying to assimilate My Computer ;)\n"
    author: "Carsten Pfeiffer"
  - subject: "I love guard dog"
    date: 2002-05-03
    body: "The most user-friendly firewall app for KDE.\nMainstream users do not need to learn all the cryptic codes at all.\nGreat work! :-)"
    author: "Dog Lover"
  - subject: "Protection against Trojans?"
    date: 2002-05-03
    body: "I'm just wondering if this is the type of firewall that just blocks incoming traffic or if it also can force network access rules onto specific programs on an app-per-app basis? The latter is just as important in high security environments like military institutions where the \"enemy\" is not just your average script-kiddie."
    author: "ZoneAlarm user"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "I like to know this too. Norton Personal Firewall 2002 also has this feature, and it is very comfortable to be able to control and watch which applications tries to access the Internet.\n"
    author: "Stig"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "yeap but you're not supposed to have spyware and trojans on your linux box :)\n\nFrom the screenshot it looks like by default you have to open in and outgoing ports which is a pain compared to good old statefull"
    author: "fler"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "btw you can block specified apps /pids / users from accessing the network with iptables's owner match support\n\nFor ex to prevent mozilla from going anywhere\niptables -A OUTPUT -m owner --cmd-owner mozilla -j DROP\n\nyou could of course do it the other way around and block all outgoing traffic by default and allow only specified apps to access the network"
    author: "fler"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "So a good idea would be to have a program which blocks all outgoing traffic by default, and then prompts you to let programs access the internet or open up a port, like ZoneAlarm. It would be complicated, because it really should prompt in an anogistic fashion, whether your in KDE, gnome, console whatever. Though having a program which prompts you say, only in KDE, and requires editing a text file otherwise would still be handy.\n\nThough is the only way it differenates programs is by their name? Couldn't someone write a trojan named Mozilla and then bypass the rules?"
    author: "Ian Monroe"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "Yes a zonealarm type system would be nice.  Guarddog is a great piece of software, but it is made to work with, ipchains and iptables.  This prevents them from getting the most out of iptables.  It would be nice if ipchains support is dropped in the future.  Though I do not have any spyware problems with linux now, so guarddog is more than adequate for now."
    author: "theorz"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "It blocks incoming *and* outgoing traffic. Unfortunately it doesn't work on an app by app basis. Linux doesn't support that kind of security out of the box (yet), and implementing that now would require quite a bit of kernel hacking, and the results wouldn't work easily on stock distros the way Guarddog does now. I believe Linux is getting a more pluggable security architecture in the next kernel series, which should make this kind thing much easier to do.\n\nAlso bear in mind that blocking on a per app basis only works for apps running on the same machine as the firewall. i.e. it won't work for packets that didn't originate on the firewall machine.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: ":: Linux doesn't support that kind of security out of the box\n\nIIRC, that was one of the advantages of iptables over ipchains, which is why iptables are the new preferred Linux network control method.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "iptables doesn't really let you do per app stuff. The big advantage of iptables over ipchains is the connection tracking features.\n\nBesides, if wanted to block a local application from sending data, the obvious place to do that is on the kernel call level, and not down at the packet level.\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-03
    body: "Fler wrote:\n\nbtw you can block specified apps /pids / users from accessing the network with iptables's owner match support\n\nFor ex to prevent mozilla from going anywhere\niptables -A OUTPUT -m owner --cmd-owner mozilla -j DROP\n\nyou could of course do it the other way around and block all outgoing traffic by default and allow only specified apps to access the network\n \n\nSo it looks like you can filter on an app by app basis."
    author: "Dude"
  - subject: "Re: Protection against Trojans?"
    date: 2002-05-05
    body: "Only if it's running on the same machine though - and you're better having a separate\nfirewall\n\nRemember ZA is fiction-ware, if you run malware code on windows 98 you are toast\nas there's no security model to prevent bypassing ZA. It's only that numerous malware hasn't yet appeared to demonstrate that, \nwhich means you even gain any benefit from ZA yet.\n\n\nA few things you need to think about if you really want to stop apps connecting to the internet\n\na) `mv malware mozilla-bin` - you need crypto to prove that mozilla is mozilla and\nthat it hasn't changed.\nb) export LD_LIBRARY_PATH=~/malware-libs / export LD_PRELOAD=~/malware/lib - lets me\nuse mozilla's access to run my program.\nc) ... thousands of others...\n\nIf you want to stop an app doing something - and there's a lot more an app might\ndo than access the network, a bigger problem that you can't really expect a front-end\nto packet filtering to solve, look at running it under a chrooted\nuser-mode-linux environment - give it root then if you like ;o)"
    author: "Michael"
  - subject: "Door Locks"
    date: 2002-05-06
    body: "Just because you can break in through the window, doesn't mean you shouldn't lock your door. When GRC was talking to Microsoft about the full implementation of TCP/IP in Windows XP Microsoft had a hard time grasping this concept. They argued that because drivers could be installed in current machines giving windows machines raw sockets (and thus ip-spoofing capablity), what could be so bad about giving alll windows machines this by default? \n\nHaving ZoneAlarm-like functionality would be nice in Linux because the crackers would have to go through the extra effort to get a program to connect to the internet without user permission. Though your right, checksums would be needed to verify programs or else getting around the firewall would be way to easy.\n\nIan\nhttp://ian.webhop.org"
    author: "Ian M"
  - subject: "Re: Door Locks"
    date: 2002-05-06
    body: "This is a good comment.\nZone-alarm is protecting us from \"legitimate\" software calling out without our knowledge ie spyware.\nFurther the spyware is only really hostile in the same sense that Mcdonalds is hostile, it's just something you want to keep under control before it does do you harm\n\nThis software can only crawl so far up the hostility ladder before the principals will fall foul of anti-hacking laws.\nCommercial spyware that renames itself as mozilla to dial out would probably be illegal. \n  \nLight protection could be quite effective against spyware."
    author: "Simon"
  - subject: "Re: Door Locks"
    date: 2002-05-07
    body: "If you don't trust your applications, you need sandboxing.\n\nSandboxing, as I hinted above, is more than a 'yes/no' question to\n\"can program X connect to x.x.x.x on port Y\".\n\nBy definition, that's a lot of questions to answer for your web browser -\nor else you allow your web browser all access on port 80? In which case, what are\nyou protecting by asking the question?\n\nPerhaps you really want your web browser not to send personal info?\n\n\"Protect the info\" then seems a better idea than pretending you've secured\nthe network against information leakage, no?\n\nYou have to learn from the mistakes windows software has made,\nnot copy what they do to try and reach the same unsatisfactory point.\n"
    author: "Michael"
  - subject: "Re: Door Locks"
    date: 2002-05-07
    body: "No, simply put, ZA doesn't make anything harder for code running on the\nsame machine as ZA.\n\nPeriod.\n\n(I would expand further on the performing moustaches stuff about raw sockets,\nbut there's plenty of that elsewhere - suffice to say linux tcp/ip has them and\nI doubt you'll get far trying to get them removed - certainly not with\ncliched statements about doors and windows)"
    author: "Michael"
  - subject: "Re: Protection against Trojans?"
    date: 2005-01-22
    body: "No, you're wrong. You don't need \"crypto\".\n\nYou need the OS to tell you the path to the program that is trying to access the internet, or it to tell you the path to the program that is originally calling the library.\n\nCrypto. Wtf!"
    author: "Bloke"
  - subject: "remote machines"
    date: 2002-05-03
    body: "Is it possible to use Guarddog to configure a firewall on a remote machine that doesn't have KDE installed?"
    author: "Richard Stellingwerff"
  - subject: "Re: remote machines"
    date: 2002-05-03
    body: "Guarddog creates a shell script /etc/rc.firewall, that you can easily transfer to another machine and execute there."
    author: "Carsten Pfeiffer"
  - subject: "Re: remote machines"
    date: 2002-05-08
    body: "It would be wonderful if that could be automated with ssh or something!\nTo make watchdog scp /etc/rc.firewall from the firewall when it starts and scp it back when it is done. And run \"ssh firewall /etc/init.d/firewall reload\". :)\n"
    author: "Per Wigren"
  - subject: "linux junky"
    date: 2002-05-03
    body: "Does it make sense to have a window manager on a firewall.  Won't this open up uneeded ports and have uneeded programs running."
    author: "Mark"
  - subject: "Re: linux junky"
    date: 2002-05-03
    body: "You're absolutely right. This is the approach which \n<a href=\"http://www.smoothwall.org>smoothwall</a> uses: keep what's running on the firewall to a minimum.\n\n--m--"
    author: "Matthew Trump"
  - subject: "Re: linux junky"
    date: 2002-05-04
    body: "Smoothwall is probably higher assurance and less vulnerable than Guarddog.  Smoothwall is fine if you can support it yourself.  If you can't, then you'll probably run into Richard Morrell or one of his minions.  Freshmeat has some choice comments about Smoothwall support...even the paying customers get crapped on, while GPL users are beneath dirt.\nhttp://freshmeat.net/projects/smoothwall/?topic_id=253 (scroll down to messageboard)\n\nIf you just need to block portscans and script kiddie attacks, then Guarddog is sufficient for what you need.  Simon on the other hand has been very helpful in the give and take with the KDE community.  Thanks Simon!"
    author: "Josh"
  - subject: "Re: linux junky"
    date: 2002-05-03
    body: "You're missing the point.. This is about having a firewall on a normal desktop workstation.  Also, the script that it generates can be used on other machines."
    author: "Christian A Str\u00f8mmen [Number1/NumeroUno]"
  - subject: "Re: linux junky"
    date: 2002-05-03
    body: "You could run a firewall along side the window manager to block the extra ports. .. ;-)\n\n--\nSimon"
    author: "Simon Edwards"
  - subject: "Re: linux junky"
    date: 2002-05-03
    body: "Or you just tell X and kdm/xdm not to listen on a TCP/IP port, and you're just as safe as not running X.  The \"-nolisten tcp\" command line will prevent XFree86 from opening TCP (all communication will be done via UNIX domain sockets instead)."
    author: "Chad Kitching"
  - subject: "Re: linux junky"
    date: 2002-05-07
    body: "> and you're just as safe as not running X.\n\nNo you aren't.\n\nPrecisely because by running applications on the firewall you\nrisk bugs in those applications compromising that machine.\n\n2 examples\n\na) Using them to connect to the internet and some untrusted data compromising\nthe application (consider a bug in, say, konqueror that was exploited\nby visiting a site, or a bug in mozilla that was compromised by reading\nan email)\n\nb) Having them used by a successful exploit to a normal user account to gain\nhigher privileges - plenty of old exploits have exercised bugs in XFree to\ndo this.\n\nBugs like these on a desktop / firewall using the same machine compounds the\ndamage - precisely why best practise would recommend running services / applications\noff the firewall and running the minimum on the firewall (certainly\nnot using it at a desktop with all your personal data / passwords etc on it)"
    author: "Michael"
  - subject: "Re: linux junky"
    date: 2002-05-06
    body: "Why should it matter? A firewall would block those open ports from usage anyways.If it doesn't, it's not a firewall.\n\nBesides, if you are uncomfortable with this, just copy the guarddog-generated script from your desktop computer to your firewall.\n\n"
    author: "fault"
  - subject: "Kcontrol Module?"
    date: 2002-05-03
    body: "This would make for a nice kcontrol module, if anyone is looking for something to work on."
    author: "Pablo Liska"
  - subject: "Re: Kcontrol Module?"
    date: 2002-05-03
    body: "AMEN I totally agree!\nI would also suggest being able have the ability to manually enter port numbers (with a description) for those wacky protocols that 0.5% of people use but since the firewall doesn't have it listed they can't configure.\n\nA KControl module and having everything exposed via DCOP would absolutely rule."
    author: "tzanger"
  - subject: "Re: Kcontrol Module?"
    date: 2002-05-04
    body: "> I would also suggest being able have the ability to manually enter port \n> numbers (with a description) for those wacky protocols that 0.5% of people \n> use but since the firewall doesn't have it listed they can't configure.\n\nWhat about trying it out? This is possible already.\n"
    author: "Carsten Pfeiffer"
  - subject: "Re: Kcontrol Module?"
    date: 2002-05-06
    body: "ugh, please don't complicate kcontrol even more. It's been far too cluttered for far too long."
    author: "fault"
  - subject: "Re: Kcontrol Module?"
    date: 2002-05-06
    body: "You mean too functional? \n\nAs far as I see it: better to consolidate/organize functions in one place than have hundreds of \"uncluttered\" programs/files/places that only do one thing. \n\nIt would be great if kcontrol did what control panel does in windows, but organized things a bit better, thanks to kparts.  Windows 2000 server has this extra section called \"computer management\" that is like kcontrol (but with less stuff in it and therefore somewhat harder to find and creating more \"clutter\" -- lots of icons in the admin section of control panel).  \"Computer Management\" includes interfaces for the logical volume manager, the firewal configurator (which btw has a bunch of user-friendly wizards and also allows fine-grained control), user management, etc.\n\nMaybe we should have two kcontrols? one for basic stuff and one for things like this? I think its better to have it in one \"place\" (not really one place since its just kparts pulling in other progs -- as far as i understand) and just organize things well.\n\n\n"
    author: "Pablo Liska"
  - subject: "Re: Kcontrol Module?"
    date: 2002-05-07
    body: "> I think its better to have it in one \"place\" (not really one place since its \n> just kparts pulling in other progs -- as far as i understand) and just organize > things well.\n\nYeah, but it's not better that you decide where we want things.\nIt's better if we can configure something to put arbitrary things in one place, or not, as we see fit.\n\n\n\n"
    author: "Michael"
  - subject: "firestarter ownz j00 all"
    date: 2002-05-04
    body: "this program is a wannabe \"firestarter\" ...keep on believing that kde rocks, and keep on being \"OwN3d\" by trolltech bitches.\n"
    author: "joe99"
  - subject: "Re: firestarter ownz j00 all"
    date: 2002-05-04
    body: "> ...keep on believing that kde rocks, \n\nHmm, keep on dreaming that there'll be anything else that's decent for linux in \nnext .. hmm, let's say for another 2 or 3 years. So far, i'm gonna stick with the \nbest there is - KDE3.\n\n> and keep on being \"OwN3d\" by trolltech bitches.\n\nInteresting point. If you own a VCR you're \"OwN3d\" by the company that made it ?\nNever thought of it that way ;)"
    author: "foo"
  - subject: "Re: firestarter ownz j00 all"
    date: 2002-05-06
    body: "joe99 is a wannabe \"troll\" ...keep on believing that you troll well, and keep on being \"OwN3d\" by real trolling bitches.\n"
    author: "uh"
  - subject: "IP forwarding?"
    date: 2002-05-06
    body: "I had really hard time getting IP Masquerading/Forwarding to work to share my cable modem connection through my Linux box also to my Windows computer. I have no intent in learning to understand what that what I did to accomplish that really means and how it works, but I'd really like to do the same using a simple GUI-based tool.\n\nSo is it possible to use Guarddog to create not only firewall, but also IP Forw/Masq-rules?"
    author: "R2D3"
  - subject: "Re: IP forwarding?"
    date: 2002-05-13
    body: "The same guy who does GuardDog has a little app called GuideDog that does this for you."
    author: "Anonymous"
  - subject: "Re: IP forwarding?"
    date: 2002-05-23
    body: "It is possible.  I have a Network of Window Computers running off my Linux RH 7.2 machine (2.4 kernel, so using iptables).  I also have an ADSL connection (via Verizon).  To do the masquerading I used Guidedog.  There are a couple other things to do which I would be happy to share with you.  Just email me if you are still interested and if my set-up sounds similar to yours."
    author: "Ted"
  - subject: "Re: IP forwarding?"
    date: 2002-05-23
    body: "It is possible.  I have a Network of Window Computers running off my Linux RH 7.2 machine (2.4 kernel, so using iptables).  I also have an ADSL connection (via Verizon).  To do the masquerading I used Guidedog.  There are a couple other things to do which I would be happy to share with you.  Just email me if you are still interested and if my set-up sounds similar to yours."
    author: "Ted"
  - subject: "Re: IP forwarding?"
    date: 2004-11-06
    body: "I am trying to get guidedog to work with teh guarddog firewall. The firewall works well on my Fedora core 1 box, but I am unable to get guidedog to work at port forwarding for my windows boxes to work. I have P2P software that use port 2234 and 1124 but when i try starting them, they are not working. Any help will be nice for setting up guidedog to work."
    author: "Rob"
---
<a href="http://www.simonzone.com/software/guarddog/">Guarddog</a> is an easy to use, yet powerful, firewall for Linux machines running KDE 2 or 3. Guarddog isn't just a pretty GUI face thrown over the standard command-line firewalling utilities, it allows you to quickly and easily specify your firewall policy at a <i>high-level</i>, and then takes care of the rest. The first release candidate, version 1.9.15, is out and now needs heavy user testing. Everyone is encouraged to test it on as many weird and wonderful network setups as possible and report their experiences before the final official 2.0 release. The full announcement follows.
<!--break-->
<p>
<blockquote>
<p>
After over a year of development, version 2 of <a href="http://www.simonzone.com/software/guarddog/">Guarddog firewall</a> is nearing
completion.  I now wish to invite all in the Linux community to help test
the current development version of Guarddog on as many different network
configurations and with as many varied network clients as possible to help
shake out any bugs. As well as bug reports, I'm also interested in hearing
about which features are successfully being used, so that I can try to
determine what features can be trusted and which not.
<p>
Guarddog is a user-friendly firewall configuration utility for Linux designed
for the KDE 2 and KDE 3 desktop environment. Unlike other firewall utilities,
Guarddog takes a <i>goal-oriented</i> approach allowing you to specify on a high-level what it must permitted without having to painfully spell out a list of
rules.
<p>
Features:
<p>
<ul>
<li>Direct support for over 60 common network protocols.</li>
<li>Allows you to divide machines into different 'Zones'. Where each zone can
  have different security policies.</li>
<li>Uses Linux's native iptables or ipchains packet filtering faciltities.</li>
<li>Paranoid and fail-safe by design.</li>
<li>Extensive documentation and tutorials.</li>
<li>Published under the GNU GPL.</li>
</ul><p>
Guarddog is available at:<p>

<a href="http://www.simonzone.com/software/guarddog/">http://www.simonzone.com/software/guarddog/</a><p>

RPMs are available for most popular distributions. Information about testing
and results are available at:<p>

<a href="http://www.simonzone.com/software/guarddog/testing.php">
http://www.simonzone.com/software/guarddog/testing.php</a><p>

Kind regards,<p>

Simon Edwards<br>
Guarddog Developer<br>
<a href="mailto:simon@simonzone.com">simon@simonzone.com</a><p>
</blockquote>