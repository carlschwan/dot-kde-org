---
title: "KDE 3.0.1 Ported to MacOS X and Darwin"
date:    2002-06-01
authors:
  - "Dre"
slug:    kde-301-ported-macos-x-and-darwin
comments:
  - subject: "great!"
    date: 2002-05-31
    body: "I have a lot of friends who run macosx, and they are constantly looking for new unix software to install through XDarwin and fink. This will probably be the first thing they download now :)\n\nGo fink."
    author: "fault"
  - subject: "Re: great!"
    date: 2002-06-01
    body: "I just download and installed KDE from fink. I am running a TiBook 550 with OS 10.1.3, and all I can say is that KDE runs like an absolute dog as of right now. On the fink web site it talks about the slow dynamic library loader for KDE. This is absolutely true.\n\nThe X on X program, which allows you to run X-Window System apps rootless ontop of Mac OS X, loads on my machine to a window-managerless terminal in about 5 seconds.\n\nThe KDE startup sequence takes approximately 80-90 seconds to load to the point where your basic desktop is running. Each app you launch after that takes another 30+ seconds to load. In many cases the KDE blinking cursor times out after it default setting of 30 seconds. Konq takes approximately 60 seconds to load the first time, and each time you wish to open a new browser window is takes at least 30 seconds.\n\nI of course understand that this is beta software. KDE (specifically Kdevelop) is one of the main reasons I used to have linux on my tibook, and I would love to be able to use kdevelop next to all of my os x apps. In addition, I use MS Office and Excel extensively, and it would be great to how much Kspread could support my spreadsheet needs.\n\nAs a side note, debian has still not released XFree 4.2 in packaged binary form yet, which means my radeon mobility requires I make World for XFree. Anyone who has or has tried to maintain two different XFree trees, one packaged and one compiled, knows it is easier to just use the distrib package.\n\nNow, fink lets me use my favorite linux apps (KDE + kdevelop) under Xfree 4.2 under Mac OS X. Who woulda thunk? It just needs a little fine tuning.\n\n"
    author: "Ian Zepp"
  - subject: "Re: great!"
    date: 2002-06-01
    body: "qt-3.0.4 slows down kde monstrously in ppc linux (at least that's what we've found at Gentoo), so this may be the problem here too.  Hopefully Trolltech will fix the problem, and kde will be viable on OSX too."
    author: "daybird"
  - subject: "it would be great if "
    date: 2002-06-01
    body: "it would be great if KDE ran outside of X11 on OS-X, QT does already. So things like konqueror and koffice could be just another OS_X applicaction. "
    author: "james brown"
  - subject: "Re: it would be great if "
    date: 2002-06-02
    body: "it would be great if someone were to port qt/x11 to cocoa or carbon\n\n(or simply port libX11 more directly than XDarwin does, which would be a lot easier than above)"
    author: "fault"
  - subject: "Re: great!"
    date: 2002-06-05
    body: "As an alternative to kspread, OpenOffice also runs under Mac OS X now, at least partially.  Give it a try www.openoffice.org\n\nErik\n"
    author: "Erik"
  - subject: "btw.../"
    date: 2002-05-31
    body: "the fink faq should be updated (Q3.6: Why are there no packages for KDE?)"
    author: "fault"
  - subject: "That's just wrong."
    date: 2002-05-31
    body: "IE & Konqi in the same window?\n\nThat's just _wrong_.\n\nThe apocolypse will soon be upon us."
    author: "ac"
  - subject: "One of those screenshots..."
    date: 2002-06-01
    body: "Now why on earth does one of the screenshots (<a href=\"http://fink.sourceforge.net/news/kde3-thesin-gimp.html\">kde3-thesin-gimp.html</a>) show a screenie I took on my machine? (<a href=\"http://www.srcf.ucam.org/~rjw57/xine-xdirectfb.png\">xine-xdirectfb.png</a>) [Not a MacOS X box, nor particularly KDE related]"
    author: "Damocles"
  - subject: "Re: One of those screenshots..."
    date: 2002-06-01
    body: "Heh...\n\nTheSin (Justin Hallett, the guy who made the screenshot) is the maintainer of xine on fink.  He probably happened to have it open."
    author: "Ranger Rick"
  - subject: "Cheers you Canook !!!! (Justin)"
    date: 2002-06-01
    body: "You did well. I am running linux apps on linux now and mac os x apps on mac os x.The bsd kernel had turned into an inferno for me when I ran linux apps on my cube. Don't ask me to choose I love them both Mac OS X (BSD) and linux. But I prefer KDE 3.0 on linux kernel...besides my screen can only hold so much at one time. KDE, Gnome, Mac OS X etc.. all on the same sceen ( I know you can boot into full screen mode but the video is poor at 16mb ) so for me it is to yaboot or not. See you in GPL heaven and cheers to you !!!\n\nRod Ross :)"
    author: "Rod Ross (myXtie)"
  - subject: "Liquid style on OS X?  Don't embarass yourself!!!"
    date: 2002-06-01
    body: "For a thousand times I have restrained myself from commenting on the \"Liquid\" style or others of its kind.  Apple's popular Aqua interface has inspired numurious computer artists to clone or imitate it on non-Mac platforms, even though some of them may find it hard to admit so.   If you have ever been introduced to the Windows customization community, you must have seen dozens of Aqua-like themes (named either \"aqua\" or \"liquid\").  I myself contributed to some of the most popular and highest quailty ones before I switched to Linux as my primary OS.  The Liquid style on KDE, well I hate to say this, does not even compare to half of the similar themes for Windows.   But it's the only thing for Linux.  Therefore no complaint about that.  Now someone is daring enough to put it on Mac OS X, so that the rest of the world could share our taste of cheap art.  Sorry for being so mean, but I couldn't help thinking the screenshots of \"Liquid\" on OS X as the ugliest thing ever happened to a Mac, which has always been appreciated for its pure innovative beauty.  Porting KDE to Mac OS X is a BRILLIANT thing.  But pleeeeeeeeeease leave the \"Liquid\" style behind.  Don't embarass yourself.  If there's something we can learn from Apple, the that is: simplicity is beauty; originality is beauty.  My favorite KDE style: Light 3rd with ModSystem.  I don't know whether they are \"original\" or not.  But they surely are less embarassing then the \"Liquid\" style when appearing on Mac OS X.\n\n\n"
    author: "Felix"
  - subject: "Calling your bluff"
    date: 2002-06-01
    body: "Okay, you said that you wrote some of the most popular themes for Windows. Where are they? Which ones? Where is the archive and what are the download stats?\n\nNone of the Windows themes come close to Liquid because they *can't*. They are simple Pixmap themes. They can't even handle color schemes, alphablending, or proper pixmap alignment! Much less all the other customization available in Liquid such as stipple opacity, toolbar button style, etc... Liquid is a lot more advanced than any of the Aqua styles for Windows, which are simple collections of pixmaps. Your talking out your rear.\n\nLiquid is a lot more customizable than MacOS as well, now that I think about it :P I think it looks nicer, too, with my colorscheme :) Something you can't do on a mac.\n"
    author: "KLover"
  - subject: "Re: Calling your bluff"
    date: 2002-06-01
    body: "Could you tell me what colorscheme that is? :o) I'm looking into changing mine.."
    author: "Verwilst"
  - subject: "Re: Calling your bluff"
    date: 2002-06-02
    body: "I selected my own colors from KDE Control Center. Mess around with until you get something you like! Liquid is cool like that."
    author: "KLover"
  - subject: "Re: Calling your bluff"
    date: 2002-06-02
    body: "I know, but i was wondering what kind of scheme you've put together, so maybe i like it too :) I have a white/blue one, but just in case i'll ever get tired of it... :o)"
    author: "Verwilst"
  - subject: "Re: Calling your bluff"
    date: 2002-06-01
    body: "Why do I inevitablly sense unforgiveable narrow-mindedness and unfounded arrogance whenever any positive comment about Windows appears in the Linux comminity????  I don't argue that KDE is superior in terms of flexibility of customization.  But that doesn't make the Liquid theme superior in the aesthetic way.  Use some Windows themes and try a real Mac; if you still believe the Liquid theme \"looks nicer\", well, I don't wanna argue with you about that.  Art and beauty don't have any rationale.  It's just your taste against mine.\n\nAs to your challenge, sorry but I won't tell about my Windows themes just to arouse a new round of the Windows vs Linux battle, which usually turns into hideous personal attacks.  I still remember the cheap performance of some Linux fans in a Linux-on-the-desktop discussion at ZDNet.  Don't get me wrong.  I'm a Linux fan myself.  But sometimes I do feel sorry for some part of the community that can't be objective about either others or themselves.  Not long ago someone said the upcoming GNOME 2 would be even better than Max OS X.  Let's wait and see.  I hope the statement won't turn out to be too pathetic.\n\nIn case you haven't heard it, Apple issued a warning a couple of months ago against those Auqa style themes on non-Mac platforms, and as a result, some of the best Windows Aqua themes had to be pulled off the net."
    author: "Felix"
  - subject: "Because your a liar and a troll?"
    date: 2002-06-02
    body: "Heh, I knew calling your bluff would show your a liar. You said you \"contributed to some of the most popular and highest quailty\" themes for Windows and when I call your bluff and ask you to specify which ones you won't do it, using the lame excuse that you don't want to start a flamewar, (nevermind your original post was a big flame). If you sit around bragging that you worked on some of the most popular themes for Windows but won't tell anyone which ones it makes you look like a big liar. Especially when in the context of putting down other people's work.\n\nYou also say \"Sorry for being so mean, but I couldn't help thinking the screenshots of \"Liquid\" on OS X as the ugliest thing ever happened to a Mac, which has always been appreciated for its pure innovative beauty.\"\n\nbut in a later post say:\n\n\"Before I say the following words, let me make it clear that I have never owned a Mac and I'm a Linux user.\"\n\nWhat a load of crap. Go away, troll. If you don't like Liquid just say you don't like it, but don't make things up and flame.\n"
    author: "KLover"
  - subject: "Re: Because your a liar and a troll?"
    date: 2002-06-02
    body: "What a typical response!  I have no interest at all in defending myself to YOU.  However, since this post is viewed by other fair and reasonable readers, I feel obliged to say just a few more words.\n\nNo, I don't own a Mac.  But I surely have used more than one Macs.  What's confusing you here????\n\nI don't have to tell you about my Windows themes because:\n1) This is not about me or Windows themes.\n2) This is about the Liquid theme and the original Aqua style from Apple.\n\nMy original post said just 3 things:\n1) There're better themes than the Liquid theme.\n2) I don't like the Liquid theme.\n3) I don't think it wise to put the Liquid theme on Mac OS X.\n\nApparently I did lid some flame in you.  But I didn't make anything up.  As of those Windows themes (except mine), I've already told you about the copyright story.  You may not find them easily now.  I haven't used Windows for quite a while.  I don't have the latest info for you.\n\n\n\n"
    author: "Felix"
  - subject: "Re: Because your a liar and a troll?"
    date: 2002-06-02
    body: "1. I agree, there is better things than liquid, but in other people's minds, liquid is the best.\n2. liquid wasn't meant to be an exact ripoff of aqua.. it's wholy c++ (no pixmaps)\n3. liquid can do things that aqua can't (be colorized)"
    author: "fault"
  - subject: "Re: Because your a liar and a troll?"
    date: 2002-06-02
    body: "All you said is true.\n\nWhy do people like the Liquid theme?  Why was the Liquid theme ever created?   The answer to both questions is: the Auqa interface from Apple.  Include the Liquid theme in KDE for Mac?  Maybe.  Use it as the default style?   Could really use some reconsideration.\n\nThe Liquid theme surely can do a lot more than the Aqua, such as colorization as you mentioned.  However, there're people like me who don't like the Liquid theme at all, but I have never met a single person who is not attracted by the Aqua.   Why?   Let's look at another example.  Dell had a laptop model which came with customizable color pads.  It was cute and kind of impressive.  Apple's PowerBook is all silver and its iBook is simply white.  They can't be colorized.  But people say their beauty is stunning.  It probably took more effort for Dell to design those colorizable but less charming laptop cases.  I guess that teaches us the truth about graphic appeal: simplicity often wins over complexity.\n\n\n\n\n\n\n\n\n\n\n\n"
    author: "Felix"
  - subject: "Re: Because your a liar and a troll?"
    date: 2002-06-02
    body: "> Why do people like the Liquid theme? Why was the Liquid theme ever created? The answer to both questions is: the Auqa interface from Apple.\n\nTrue, I think liquid (any many other things, such as Luna for winxp), were created because of aqua. If aqua had not existed, these themes would not have either.\n\n> Include the Liquid theme in KDE for Mac? Maybe. Use it as the default style? Could really use some reconsideration.\n\nUh, who said it was default style? As far as I can tell, hicolor would be. However, IF liquid were to be made default, it would be logical since it looks aqua-like. It's always nice to have a desktop in which everything looks similiar.\n\n> However, there're people like me who don't like the Liquid theme at all, but I have never met a single person who is not attracted by the Aqua.\n\nI personally don't like liquid nor aqua. However, unlike you, I won't criticize the screenshot author for using either liquid or aqua.  Neither do many, many, many, people. A lot of my friends who are die-hard mac users for over 15 years hated Aqua and still do. They say it completely detracts from the simplicity of human design elements that Apple worked to create all of this time. "
    author: "fault"
  - subject: "Re: Because your a liar and a troll?"
    date: 2002-06-02
    body: "I've got an idea; how about everyone just stops this thread now.\n\nWe spent months porting an entire freakin' desktop environment to MacOS X, and it's spawned 30 comments about a theme I spent at most *10 minutes* building a Fink package for.  It's no big deal.  Liquid isn't the default.  I just thought it would be funny to have it running in OSX.  I rather like it, but I don't pretend it's a replacement for MacOS X's native style.\n\nChill out, people, there are much more important things to worry about.\n"
    author: "Ranger Rick"
  - subject: "Re: Because your a liar and a troll?"
    date: 2003-01-20
    body: "Ranger Rick.\n\nThanks to you and the rest of the Fink team.  I am a user!  I appreciate the liquid theme, when I was running KDE rootless it helped blend the \"looks\" of the two desktops nicely but NOT to much.  I just found it less visually jarring.\n\nNow that Apple has an X11 I am running KDE less then before since I can set most of the KDE apps or for that matter the X11 unix based apps that I used, from the app menu of it.  \n\nI used KDE and still do because of all the Linux desktops it is the most pleasing to the eye.  And the best orginized.  Gnome looks garrish, and creepy.  But thats a matter of taste.  \n\nBut thanks again for all the work you and the team have done!  I greatly appreciate it."
    author: "Chris C"
  - subject: "Non issue"
    date: 2002-06-03
    body: "You said: \nHowever, there're people like me who don't like the Liquid theme at all, but I have never met a single person who is not attracted by the Aqua\n\nReply:\nUm, no - you're the only one as far as I can tell. To me and most normal people, Aqua and the Liquid Theme have the same level of asthetic quality. The fact that Aqua is running on top of a propreitry, cumbersome, graphics API tied to other propreitry formats such as PDF, and not on the flexible, powerful QT platform tips the scales firmly in favour of KDE and the Liquid Theme."
    author: "Liquid Theme Fan"
  - subject: "Re: Non issue"
    date: 2002-06-21
    body: "must....not...be...pedantic....\n\nPDF is a proprietary format? Then how come there's Ghostscript and xpdf and everything? :)"
    author: "Feanor"
  - subject: "Re: Non issue"
    date: 2002-10-26
    body: "The fact that it is documented does not mean that it isn't proprietary.  Adobe chose to publish specs for the PDF format in order to enhance its adoption.  They didn't relinquish any of their rights by doing so."
    author: "Gander"
  - subject: "Re: Because your a liar and a troll?"
    date: 2002-06-02
    body: "Your original post:\n\n1) Stated that you were a contributer to some of the most popular themes for Windows, (although you won't name any of them when people ask you to).\n\n2) Proceeds to flame that Liquid doesn't even compare to half the Windows themes, when in actuality there is no comparison.\n\n3) Says that it's an embarrasment to have running on MacOSX and lots of other nasty things.\n\nSince then you've been backtracking the entire time, trying to come up with excuses why you can't tell anyone which themes you worked on and trying to sound more reasonable. Come on, even if you took down the themes if they were, as you state, some of the best for Windows google surely would have them cached or there would be some trace of it. But nooo, it must be a Secret. It's pretty obvious here your lying, but of course you can always prove me wrong, (and of course you won't). You won't even name the themes.\n\nIt's pretty obvious you probably never even used Liquid, either, since it really isn't much like MacOSX at all.\n\nOh well, enough time spent on the troll. If I were you I'd change your nick and try to start over, you've pretty much embarrassed yourself enough with your old one."
    author: "KLover"
  - subject: "Re: Calling your bluff"
    date: 2002-06-01
    body: "Mac costs money, lots of it compared to linux. Liquid is cool and free. I would love to try a mac, but do not know anyone who has it or would sponser me one, a loptop would be nice.\n\n "
    author: "once"
  - subject: "Re: Calling your bluff"
    date: 2002-06-03
    body: "Go to an Apple Shop and play with it.  It's really true: you can't compare Aqua with anything you've seen on Linux or on Windows or on anything else before."
    author: "Michael Lehn"
  - subject: "Crap."
    date: 2002-06-03
    body: "I went to my local computer store and played with one of the new Apple iMacs.  Look, it's not bad, and I suppose that for specific Apple-centric applications like Photoshop or Quicktime it's the best solution, but as for the interface? I didn't like it.\n\nYou can't compare the new-age look and feel of KDE, running on the flexible, powerful QT platform, to the clunky, slow, propreitry Aqua interface running on the new Apples. In the industry there is definitely a buzz about KDE. People who haven't even considered Unix on the desktop are taking a look at KDE and saying: \"Hell, that's better than CDE, Windows 2K/XP and Apple Aqua combined!\""
    author: "Liquid Theme Fan"
  - subject: "Re: Calling your bluff"
    date: 2002-06-03
    body: "Personally I think the look of Windows 3.1 or 95 is more usable than Aqua. "
    author: "fault"
  - subject: "Re: Calling your bluff"
    date: 2002-06-08
    body: "Not a troll,  it's true I prefer gnome and aqua instead of kde , it's personnal, I _DON'T want to convince anyone I\"m right (or wrong :) ) !  but I would like to say some thoughts :\n\nI personnally own a macintosh with OSX\n\nyes Aqua/Cocoa/Quartz is proprietary,  yes I prefere free software and the goal of the GNU project,  BUT  Cocoa API is very coherent et well designed,  Objective C is a lot of more hmm \"object\" than C++ (I like object programming, so it's normal I like objective C )  , the feel of iTunes, Finder, Omniweb, and other typical aqua applications is so.. intuitive (for me) \nQuartz (the window server) are many features X11 lacks (real alpha channel, composing and many visual effects) ,  yes it's proprietary software, but they are good ones. \nho but not all is perfect, for example Quartz doesn't have remote display like X11 (it's a shame for me :( )\nIt would be great if Quartz was GPLed :)  but it's not,  it's not a reason to do bashing. \nkde or gnome people have to judge Aqua/Quartz on its own merits and default , not only its license.\n\nother thing :\n, I prefer gnome instead of kde (it's not important, it's only personnal taste ,technical and easthetics ones.. kde is great too, at works I show KDE to people , some people think KDE is better than gnome, it's perfectly fine for me, ok ? no flame ?  ok :) ) and I think :\n\nAqua, Gnome, KDE   all got many qualities and differences\n\nI'm fond of the dock of Aqua and the Finder collumn view of Aqua, I'm fond of Nautilus 2 and the Panel of Gnome 2,  I'm fond of KOffice, Kdevelopp of KDE 3\n(I'm also fond of xemacs.. well :) )\n\nso what  ?  One computer with OSX (I use it now to post on Dot.kde :) ) and I like it ,very convenient, very good interface,  and I have a linux box with mostly Gnome2 and I'm fond of too (for different reason)\n\nIt's not a point to try to be rationnate here :  different interface/project for differents peoples,   it's a good thing\n\nMy opinion : why I prefere Aqua or gnome 2 instead of KDE3 ?  because I prefere simple GUI  ,  I mean, not too much option or icones everywhere\n\nAqua try to be very clean , gnome 2 now try to do the same\n\nKDE 3 is more coherent than gnome 2 in its API  and kde 3 got better and more functionnal dialog box like open/save , yes (kde 3 is very impressive)  but it's not my taste in GUI.  (kde 3 seems  too \"windows-like\", I mean, too many many button,options settingss , and so on.. kcontrol is very very great to the ones who like to personnalize the Desktop ,   me I think it's too heavy , not pratical for beginner)\n\nit's only my believings,   I know people who love KDE , others only swear with Aqua  , and some prefers to stay on windows.\ndifferents peoples, differents tastes.\n\nso what ?  it's no use to be rationnate on asthetics or only visual GUI effects,  liquid or not liquid,  it's useless to be rationnate on this.\n\nKDE has its very own feel , the KDE touch  , and I think it's better than windows XP (mmm..  less cubersome,  less tricky .. if you see what I mean )  \ngreat icons, many beautiful themes, can be very impressive and beautiful\n, KDe people can be proud. \n\nGnome 2 is a different beast,  It's good.  Nautilus is nice and clean, Galeon, evolution, future gimp 1.3.x are impressive and nice.   gnome is beauty too\n(tigert and jimmac contributed many good and easy to read icons)  , gnome 2 begin to be very good.\n\nAqua/Quartz , has many many great visual effects,  excellent anti aliasing (patents prevents good anti aliasing on X11), mostly the cleaner GUI now , impressive color (some people hate them, other like  them) and for me a very easy feel.\n\nwell I say that,  but I know some other people who hate all of this and prefere FVWM2 and a emacs :)  (and it's  fine because it's useful for them)\n\n"
    author: "Michel Galle"
  - subject: "Re: Calling your bluff"
    date: 2003-12-19
    body: "#1. Aqua\n#2. KDE (default)\n#3. Xfce4 (KDE theme)\n#4. Gnome\n#5. Winblows\n\n:)."
    author: "Pacifist"
  - subject: "Re: Calling your bluff"
    date: 2006-02-10
    body: "OK, you get a screenshot of MacOSX in one window (or tab) and get the screenshot of the crap-I mean \"Liquid\". Put it next to each other and see how they compare."
    author: "anonymous"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-06-01
    body: "it's up to the user to decide what style to use. a lot of people actually DO like liquid. I'd imagine the fink packages default to highcolor (kde 3.0.x), or keramik (kde 3.1?)"
    author: "fault"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-06-01
    body: "Oh, I absolutely believe that a lot of people like Liquid on KDE, just like a lot of people like Mac theme on Windows, WinXP style on Win95.  There's even some software organization dedicated to mimick Mac OS on Windows (http://www.aqua-soft.org).  That proves the influence that Apple has on the rest of the world.\n\nEver seen a Lycoris desktop?  I'm astonished by their effort to imitate Windows XP, from the icons to the color theme and to the way things are arranged.  Some Lycoris testers installed the OS to a school lab and PROUDLY deceived the students to think it's Windows XP.\n\nFunny things happen in this community.  On one hand, some people are zealously believing neither Windows nor Max OS X compares to the superiority of Linux.  On the other hand, people are trying so hard to make their Linux look more like a Mac or a Windows."
    author: "Felix"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-06-01
    body: "> Funny things happen in this community. On one hand, some people are zealously \n> believing neither Windows nor Max OS X compares to the superiority of Linux. On \n> the other hand, people are trying so hard to make their Linux look more like a \n> Mac or a Windows.\n\nWow, theres a shock, people have different opinions. Just because their all part of the Linux community doesn't mean their all going to think the same way."
    author: "Bob"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-06-01
    body: "Ah come off it, lots of people like Liquid, lots of people don't. I tried it, hated it, went back to the default style. So what? And there are software projects to do pretty much everything in the world, look at this thread, porting KDE to the Mac? Does that prove the influence KDE has on the rest of the world? No, I don't think it does.\n\nYep, Lycoris emulates XP as much as possible. This is one of their selling points. This is ONE Linux distro amongst many others. Lycoris is for people who really really like XP, but want to use Linux. Perhaps one day there will be a Linux distro that emulates a mac. Maybe.\n\nPersonally, my Linux looks like, well, Linux I guess. Although as a platform it doesn't really have one look. And finally, every single Mac user I've ever met in my life has believed the Mac is superior to everything else, but surprising though it may seem to you, not every Linux user is that way. In fact, most aren't as they are systems administrators who are paid to have professional opinions. I guess it's because all the users who weren't fantastically loyal left the Mac platform years ago, and because the only people who use Macs are those who have spare cash floating around and see their computers more as a fashion statement than a tool. I don't understand that attitude, but then I don't buy Nike trainers either, so what do I know?"
    author: "Mike"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-06-02
    body: "Before I say the following words, let me make it clear that I have never owned a Mac and I'm a Linux user.\n\nThat fashion statement is exactly what puts the pride in every Mac user.  When OS X came out, I was stunned -- hey, it's UNIX, it's easy and it's a beauty -- can you believe that?  What the heck has the UNIX/Linux world been doing over the years?!\n\nI don't mind being a follower or even a copier, but I seriouly resent showing off in front of the leader and the creator.\n\n\n"
    author: "Felix"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2003-04-28
    body: "Hold on, I love Linux, but I also love my Mac.  \nIt has been the most usefull tool I have ever purchased\nfor school.  THe Mac runs statistical programs and data\norganizing programs that I have found on no other platform.  \nEssentially I was pigeon holed into buying a Mac so that\nI could function properly within my department at the University.\nSo, to say that they are only fashion statements is not universaly\ntrue, however it is mostly true.\n\nDaveO"
    author: "DaveO"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2005-02-13
    body: "Fashion statement?\n\nI was introducd to Mac in order to produce (work).\n\nIts not a fashion, its an old system."
    author: "John Plumridge"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2006-10-19
    body: "No one would want to emulate OS X on Linux because they could just get OS X on it's own. XP sucks, why would anyone want to emulate it?"
    author: "Tom"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-08-11
    body: "There is a COMPLETE difference between the LOOK of an OS and the UNDERWORKINGS of an OS.  I use Linux because I like how it works.  The fact that I think Mac OS X is the best LOOKING desktop says absolutely NOTHING about my loyalty to Linux - good or bad.  The fact that I like my KDE desktop to look very similar to OS X says nothing about me as a Linux user - good or bad.  The fact that some distributions fill this need by making it so that it ALREADY looks like another OS says nothing about the Linux community - good or bad.  \nMy point is this: The great thing about the majority of the Linux community, I find, is that we are able to realize our own shortcomings as well as our own superiorities.  Linux is stable.  Windows is easy to use.  OS X is pretty.  That's just how it is right now.  Looking at OS X, realizing it's an attractive desktop, and emulating that look in KDE is a perfectly fine action.  "
    author: "Brian"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-10-25
    body: "I definately agree with Brian. I consider Mac OS X to be the best \"looking\" OS out there. Defenetely superior to windows. but in my experience never as good as linux. In fact linux is that much better than Mac that OS X is based on Unix (so technically linux aswell). I love linux till death, but the OS X look (Aqua) is definately better than the default KDE or GNOME. At least that's my opinion. I think it's awsome that you can make your Linux box look so much like OS X. Because that means that now instead of just having the power and reliability of a Linux box, you now have the beauty to go along with it. :)"
    author: "TemPLeST"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-12-12
    body: "Yes, the beauty but not the functionality of OS X.  Linux has along way to go on the desktop before it will be a viable alternative in the business world.  There is still no software for linux to do what people use their computers for.  If you're an engineer, where's the 3D CAD program for linux? (Don't even tell me about LinuxCAD - it's a joke).  If you're in the advertising or publishing business, where are the multimedia development tools for linux?  Even if you're just a home user, where's RealOne Player, Windows Media Player, or Quicken for linux?  All these applications, and many more, are available for OS X.  The open source software projects are coming along, but take GNUCash, for instance.  It's maybe nice, but it can't export a QIF file, making it totally worthless for a personal finance manager.  Or Kapital, the KDE personal finance manager, which is full of bugs, and also can't export a QIF.  I run Debian linux servers, but when 10.2.2 Jaguar went mainstream I finally gave on the linux desktop.  It has nothing to do with \"fashion statements\".  It has to do with wanting a rock solid UNIX operating system with the most functional GUI yet developed in modern computing.  And needing an operating system for which a variety of *high quality* software is available so you can use the machine for work in a production environment.  It has to do with being able to plug in your digital camera, scanner, or printer, and it Just Works(tm) with no fiddling around with configuration files.  It has to do with function, not fashion.\n--\nChris Olson\nNetwork Administrator\nAST Communications,  Inc.       http://linux.astcomm.net\nBarron,  WI   USA"
    author: "Chris Olson"
  - subject: "CAD on linux"
    date: 2003-04-28
    body: "I am an engineering student as Utah State University and had the same thought that you did about the CAD software on linux.  The program that we use in class, called Unigraphics, is very powerfull and also quite expensive.  We run it on SGI O2 machines in class, but I did some research online and discovered that the company the makes the software plans to have a linux version for the next edition of the software.  It is taking time, but the longer linux is used for real tasks, the more acceptance it will gain from the business folks.  We all just need to be patient and work hard to contiue to make open source work."
    author: "Kyle Horne"
  - subject: "Re: CAD on linux"
    date: 2005-04-24
    body: "www.qcad.org\nhave autocad from autodesk, to Linux, here in Brazil $$ 1500 $$"
    author: "Rodrigo Maia"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2002-12-06
    body: "I have my own RHL 7.2 computer, and its really cool. About a month ago, a friend of mine whos a mac freak showed me Jaguar, and it kicked Gnome+KDE so hard. I like linux more, but i really would like getting a GUI that mimics 10.2, because it is with out a dought the best looking GUI around."
    author: "Dan"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2003-03-06
    body: "true, true :) I have a friend who has ibook with mac os x and i really LOVE this OS. I tried to make KDE similiar to it, but it's not the same. If someone have seen animated icons in the dock on os x, he knows what i am talking about :] (Does someone know how to make this effect on KDE?)"
    author: "oort"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2003-12-08
    body: "well... the first place u should look at is kde-look.org .use karamba , and superkaramba to aplly the variety of themes they have to make ur kde look like osx\n\nin the \"kde improvments\" they have a script called \"smoothdock\" try to find the older version \"mria\"\n\ni've been trying to get my kde look like osx for about a week ... scanning the net for themes scripts.....\nand i didnt get it to look and feel exactly like the osx. when the icons are larger they dont look as good .. they arent smooth like in the osx ...\n\nbut i think that if u'll scan kde-look.org from left to right u'll find what ur looking for ..\ngood luck.\n"
    author: "Andrew"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2004-01-15
    body: "Well, we have all gotton our selves flusterd havn't we?? \n\nLets look at this argument the way we're meant too. Each OS is aimed at a different market to for fill a different niche and do a different job. For example....\n\nMac OS X... Desinged for Audio/Video production (I.e. Avid stuff)\nWindows XP... Desinged for Audio/Visual/multimedia playback (i.e. media player 9)\nLinux....... Desinged for the people who just can't leave the frekin thing alone!!\nUnix..... Desinged (orginally) for network server's..\n\nNow... the above list is how the os's plain work.. they do the aobve better than there rivals.. THe way you want your system too look is purly personal.. so you want your mac to look like win xp... its still gunna do the above task better than XP ever will but if the user likes the look of xp they can have it.... its the same argument for all of these situations.. you buy the system that does the job best.. you find an interface that pleases you most.. full stop.\n\nI recently switched from Windows XP to Mac OS9 (yeah, full platform switch) becuase i wanted to run protools and the windows version seemed rubbish, slow and blotted in comparison to the mac version.\n\nBob."
    author: "Clever Dick"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2004-04-21
    body: "I agree with Bob. Any OS can look like another OS,but each OS is for a different usage. I feel like if you want to make your favorite look like another OS that's fine.\nNow,about KDE looking like OSX give me a break! It's meant to mimick Windows from the start. That's what it's function is.To be easy for Windows users moving to Linux. And it is that. I don't have aproblem seeig people do that. It's cool. But it will never be good at mimicking it from a default standpoint. I am a Linux users who uses both Gnome and KDE. I perfer Gnome 2.4. To me it is closets thing to looking halfway like Mac OSX in it's default look. It always gets the bad mojo from users,but I don't see how. It mimicks OSX features pretty well,even has themes and a graphical login screen that is themeable. And Gnome has FAST USER SWITCHING ala OSX and XP. Try that in KDE. Only version I know of a Linux using KDE with FAST USER SWITCHING is Xandros and it is slow as a snail. I recently made a Panther theme for Gnome 2.6,and let me say I fooled my own mother who uses a Mac at work! Rumour has it that Gnome was around before Mac decided to start calling it Aqua in OSX. Hmmmm...could it be the two are the same just with a little tweaking? Why maybe so.\n\n\n\nLong live the Simian,\nMiss Trin"
    author: "Miss Trin"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2005-03-29
    body: "I'm a linux user, I've used Mandrake with KDE and I now use Ubuntu with Gnome. I like the look of Gnome, it just looks so clean and slick. I liked KDE, although it reminds me a little of tin foil and can be buggy. I've also used OSX, and it is simply the best looking. It kills any version of Windows, Gnome, KDE, *box, and everything else I've ever used in the looks department. Each OS has its uses, Linux makes a great firewall, server, or desktop for someone that can handle it. Windows is good for the everyday person that can't really use a computer well, it \"just works\" without much buggering about. Mac OSX looks great, but I don't think we should just say that's all it's good at. OSX is a very competant OS, not just for looks but for all your everyday use. It is, however, particularly strong as a graphical editing OS. I am a linux user, but I will use anything, and judge it on it's strengths and weaknesses, not just on what does and does not look good.\n\nI think if people want to skin GTK to look like OSX or get an iTunes XMMS skin they can...why the hell not?! People made them because someone, somewhere in the world wanted it. We wouldn't be here talking about it if it was just one or two people that actually do this. People do it for a reason, people copy other people for a reason, they think it looks good, if you don't then don't do it, it's much easier than complaining about it, and much less annoying too."
    author: "FordFan753"
  - subject: "Re: Liquid style on OS X?  Don't embarass yourself"
    date: 2006-01-31
    body: "I don't really see the argument here, but here's my 2 cents.  I work with Linux at my job and at home.  I can do anything that anyother OS can do.  The thing is, Linux is a bit more underground than the leading brand.  It's really cheap as f***.  Point I'm trying to make is why not make it look like the leading brands?  I like to throw people off with it too both parties wheather they are MS or OsX users 90% of them do not have a clue what to do when they step on to my platform.  They come up and ask if they want to use my computer...I always say yes...no matter what.  They see the winampclassic skin on my Desktop and then they see the dock positioned in the bottom-center with the similar icons that resemble OsX.  They don't know what to do.  I really think this fight is worthless and it bores me.  Don't bother replying cos I won't answer.      "
    author: "cahn"
  - subject: "Is it fast there?"
    date: 2002-06-01
    body: "Mac OSX requires the best hardware available; hence it is fast. How does KDE performs in Mac OSX? "
    author: "Rayman"
  - subject: "Re: Is it fast there?"
    date: 2002-06-07
    body: "Slow as a bugger, but so was Gnome. Its largely XFree86's fault, as its not accellerated at all, but i heard the current CVS xFree86 was getting optimised for 10.2.\n\nMind you, that doesnt mean its not usable, but the interface is slow compared to running native apps and kde on a linux kernel. not all the kde stuff is sorted out yet as well, its in the process of being ported, theyre not finished yet.\n\nNonetheless I have found it quite usable allready. running gui unix apps next to mac os apps on the same desktop is like having 2 machines in 1... and eliminates my need of reboots or an extra machine. :)"
    author: "x4ce"
  - subject: "Re: Is it fast there?"
    date: 2002-06-07
    body: "Mac OS X doesn't require much hardware, just a G3, not G4.\nUnfortunately, I'm running it on the lowest end machine that OS X\nruns on, close to a 266mHz G3 66mHz bus.  I have to play games waiting for\nKDE to boot on X (now sure what's slowing it down, but someone mentioned \nthe equiv to dlls???, maybe how they were ported???, and the other comment on XWindows).  Also, this graphics hardware on the original G3 minitower is not accelerated, only the newer graphics cards were accelerated in Mac OS X.\n\nWith that said, I am very glad to be able to run open source software on the hardware I have available to me.  Also, to see linux based software until I can get the hw to run natively.\n\nGreat job FINK!!\n\nJerry "
    author: "Jerry"
  - subject: "Re: Is it fast there?"
    date: 2002-06-08
    body: "Actually, we've since figured out what part of the speed issue was.  There's a bug in qt 3.0.4 that affects linuxppc as well; a one-line patch shaves 30 seconds off of konqueror's startup, among other things.  A new release is due soon (hopefully this weekend if the test binaries are OK)."
    author: "Ranger Rick"
  - subject: "Re: Is it fast there?"
    date: 2002-06-10
    body: "The update is much better.  Now I have to find another excuse to play games!!\n\nIt keeps getting better, GREAT JOB FINK GROUP!!\nJerry"
    author: "Jerry"
  - subject: "And QT ?"
    date: 2002-06-01
    body: "I though that the Qt licence fordid the installation of Qt on a not free system for example win32 (ie. you have to pay to get that version). What about the X version of Qt, is it free also ?\n\nThanks"
    author: "g78"
  - subject: "Re: And QT ?"
    date: 2002-06-01
    body: "QT-X11 is perfectly fine on MacOSX, we're *running* it in X11.\n\nNot only that, I can only assume they're OK with it, they've accepted patches from the Fink team (3.0.4 includes some of our fixes for building on Darwin with X11)."
    author: "Ranger Rick"
  - subject: "Re: And QT ?"
    date: 2002-06-02
    body: "1. the license for qt/X11 license is  the GPL. \n\n2. the GPL does not forbid installation of Qt on a non-free system, like win32, last time I checked. if it did, companies like redhat (through cygwin, would have run into problems a long time ago).\n\n3. the gpl'd qt/X11 itself can be ported to win32 or macosx natively (trolltech would probably not even be unhappy with you as it would not compete much with their commercial offerings since the end result would be GPL). There is nothing in the GPL that says it couldn't be done, however, nobody has done it yet.\n\nThis would of course be the key to running KDE apps fast on macosx or win32. The other barrier is some of the X11-specific parts in kdelibs, but that's not a huge issue as the usage of a native qt."
    author: "fault"
  - subject: "gr8"
    date: 2002-06-01
    body: "why not set aside resources in porting it to windows too when you're at it..., idiots.\n\ni guess it's prestige, and nothing else that is the drive forece...\n"
    author: "me"
  - subject: "so what ?"
    date: 2002-06-01
    body: "eh?"
    author: "nobody"
  - subject: "Re: gr8"
    date: 2002-06-01
    body: "soon... http://kde-cygwin.sourceforge.net/ :)"
    author: "blashyrkh"
  - subject: "Re: gr8"
    date: 2002-06-02
    body: "Firstly, perhaps you should set aside some of *your* time to improving your spelling.\n\nSecondly, the developers in question are free to do what they wish with *their own time*.  Free software is very much about \"scratching an itch\" - often developers will work on a project just because they \"feel like it\". Without that mentality, a lot of excellent software would never have been created. \n\nSo quit criticising the work of others. Maybe you could even contribute something positive yourself? \n"
    author: "Sam"
  - subject: "Re: gr8"
    date: 2002-06-02
    body: "Or perhaps you can worry about what you do with your own time, and stop worrying about mine.  =)\n\nI did it because it's fun, and because I wanted my favorite terminal, konsole, on OSX.  The rest is gravy, but I've learned a lot in the process.  I had konsole running in just a few days of real work (albeit statically built).  At that point, I told others about it and we decided it would be fun to get everything working for real.\n\nI'm not a \"resource\", I'm a guy who likes to tinker.  I do a lot of other \"productive\" things for the Fink project, but this was a way for me to get my hands dirty in getting a lot of great X11 apps running on MacOS X all at once.\n\nNot only that, in the process of getting KDE working, we've found a lot of other issues in libtool and other stuff that were not 100% on MacOS X, so the things that went into getting KDE working bled over into helping a lot of other software work better as well."
    author: "Ranger Rick"
  - subject: "Re: gr8"
    date: 2002-06-02
    body: "> I did it because it's fun, and because I wanted my favorite terminal, konsole, on OSX.\n\nKonsole is my favorite too. The version at http://konsole.kde.org is even better than the KDE 3.0 one."
    author: "Al"
  - subject: "Re: gr8"
    date: 2002-06-07
    body: "LOL! Prestige! youre quite funny gr8... you should consider becoming a stand up comedian or sthing... *pinks a tear away...* *snirff*\n\nthe guys who ported it maintain a packet manager for OSX. They ported the debian tools to OSX so you can easily install and maintain unix applications on mac os x and darwin. If youre running the opensource darwin, they also provide you the means of installing x11 on it, so that you have a graphical frontend, which doesnt come with darwin. they had the gnome desktop enviroment for a while, and its a Good Thing they were able to add KDE as well. By consequence the gui stuff also runs on mac os x (since they share the same unix underpinnings) so that mac os x users can run things like gimp and other unix gui apps.\n\nporting to W32 would be a whole other beast since its not unix... and its way beyond the scope of their project. BTW why dont you just throw that M$ big bloated with difficult words wrapped crap out (M$ says : if you cant make it good, just slap some difficult words on it and it will appear as state of the art) and try a real OS?"
    author: "x4ce"
  - subject: "I'd like to just run this on Darwin, without Aqua"
    date: 2002-06-06
    body: "MacOS X users can just run Darwin, without any of the rest of MacOS X, if we want.  I'm interested in running KDE from there, as if I were running Darwin on a PC.  I wonder if that's possible..."
    author: "Leo"
  - subject: "Re: I'd like to just run this on Darwin, without A"
    date: 2002-07-07
    body: "yes, i've done it and it works great. :)"
    author: "michael"
  - subject: "Re: I'd like to just run this on Darwin, without A"
    date: 2005-10-23
    body: "Would you like to tell me how this is done?\nThanks.\nJames."
    author: "james sullivan"
  - subject: "Re: I'd like to just run this on Darwin, without A"
    date: 2005-10-23
    body: "You have to show username and password fields in the login window... then use this as the user name:\n\n>console\n\n\nwith no password. you'll be sent to the Darwin login screen. If that isn't good enough, let me know and I'll hunt down the changes to the startup system that take you straight to the Darwin login screen."
    author: "Michael Sitarzewski"
---
The <a href="http://fink.sourceforge.net/">Fink project</a>, which
&quot;<em>wants to bring the full world of Unix
<a href="http://www.opensource.org/">Open Source</a> software to
<a href="http://www.opensource.apple.com/">Darwin</a> and
<a href="http://www.apple.com/macosx/">Mac OS X</a></em>&quot;, has
<a href="http://fink.sourceforge.net/news/kde.php">announced</a> the
successful port of KDE 3.0.1 to the platform.  
The package list for the initial release includes a great part of the KDE
core distribution, including <a href="http://www.konqueror.org/">Konqueror</a>
(<a href="http://fink.sourceforge.net/news/kde3-rangerrick-ie_and_konq.png">screenshot</a>).  Importantly, it also includes both
<a href="http://www.koffice.org/">KOffice</a>
(<a href="http://fink.sourceforge.net/news/kde3-rangerrick-koffice.png">screenshot</a>)
and <a href="http://www.kdevelop.org/">KDevelop</a>, as well as
<a href="http://ich.bin.kein.hoschi.de/fish/">kio_fish</a> and, you guessed
it, the <a href="http://www.mosfet.org/liquid.html">Liquid</a> style.
Hopefully they can resolve the linker/libtool issues which are responsible
for the rather large size of the binary packages.
Nevertheless, a number of Fink project members have reported successfully
using KDE on their Mac OS for several weeks.  Congratulations to the
Fink team!

<!--break-->
