---
title: "People of KDE: Dimitris Kamenopoulos"
date:    2002-01-23
authors:
  - "wbastian"
slug:    people-kde-dimitris-kamenopoulos
comments:
  - subject: "Nice interview"
    date: 2002-01-23
    body: "Especially the last answer gave me a good laugh. :)"
    author: "Rob Kaper"
  - subject: "Re: Nice interview"
    date: 2002-01-23
    body: "Never heard it expressed like that before... with three names. :)"
    author: "Johnny Andersson"
  - subject: "Christmas?"
    date: 2002-01-23
    body: "> I'm working on a simple Annotea plugin for Konqueror. Hope I'll have finished with it till Christmas\n\nWhich christmas? I hope the interview was conducted in December 2001 :-)..."
    author: "someone"
  - subject: "Polytonic greek"
    date: 2002-01-25
    body: "Kal\u00eamera, it would be great if it were possible to type polytonic greek (with unicode font such as TT MS Palatino or TT Titus Cyberbit Basic) properly. As StarOffice 6.0 beta is able to handle Unicode, it would be easy then for Linux users too to write classical greek. At least I could not figure out a way to do so in KDE 2.2, but maybe with KDE 3.0 it will be possible!\nBTW, good quote of Lord of the Rings, I like it too.\nBye \nDirk"
    author: "Dirk Heimann"
  - subject: "Re: Polytonic greek"
    date: 2002-01-25
    body: "As of KDE 2.0, keyboard is always an X11 issue. So, what you \nneed is a polytonic-enabled keyboard layout for xkb (and a utf-8 locale\nin your system). There has been a beta version around for a couple of months\nin i18ngr@hellug.gr (it's a list), but it's author wants to patch a few things before sending it to XFree86. If I were you, I would wait for X to officially\nadopt this keyboard, since patching a new layout (esp. a non-latin one) into an installed version of X can be a nightmare."
    author: "Dimitris Kamenopoulos"
  - subject: "Re: Polytonic greek"
    date: 2002-01-25
    body: "Thanks, that saves some hours of figuring out things without success. Bye"
    author: "Dirk Heimann"
---
In this week's episode of <a href="http://www.kde.org/people/people.html">The People Behind KDE</a>, you can learn all about
<a href="http://www.kde.org/people/dimitri.html">Dimitris Kamenopoulos</a>.
Dimitris provides internationalisation (<a href="http://i18n.kde.org/">i18n</a>) and localisation (l10n) of KDE into Greek. Rush to the <a href="http://www.kde.org/people/dimitri.html">interview</a>, if you want to find out how to spend time on KDE without having your girlfriend complain.
<!--break-->
