---
title: "KWinTV Rewrite Alpha 1"
date:    2002-07-01
authors:
  - "numanee"
slug:    kwintv-rewrite-alpha-1
comments:
  - subject: "problems"
    date: 2002-07-01
    body: "Hi!\n\nThanks to the kwinTV authors for giving me an alternative to that crappy xawtv!\n\nI just compiled Alpha1 sucessfully. When it starts up, I have to select a video device and a video source. Both are drop-down-boxes. Problem is that I can't drop them down because they're empty. So, basically, I can't select a video device or source. It doesn't seem to accept cmd-line options either.\n\nIs that a bug or did I miss something?\n\nthanks!\n   ben"
    author: "ben"
  - subject: "Re: problems"
    date: 2002-07-01
    body: "I think you didn\u00b4t miss anything. It\u00b4s a bug. I experienced the same problem. On the kwintv mailing list (look at www.kwintv.org), There is also a thread about it.\n"
    author: "Sangohn Christian"
  - subject: "Re: problems"
    date: 2002-07-01
    body: "You probably forgot to add the v4l module to your XF86Config. If you don't do this\nit won't work.\n\nRich."
    author: "Richard Moore"
  - subject: "Re: problems"
    date: 2002-07-01
    body: "Did you add the load \"v4l\" to your XF86Config? After that, the boxes are not empty."
    author: "Me"
  - subject: "Re: problems"
    date: 2002-07-01
    body: "my advice:\n\nstick with the former kwintv. (see what they say at http://www.kwintv.org)\n\nthat rewrite is far from being usable.\n\nformer kwintv version that is in cvs works with KDE3, and has a bunch of all the features which are missing in that alpha 1 version.\n\npay attention that that CVS version is touchy to compile, and u'll have to modify by hand the Makefiles if u want things like fullscreen."
    author: "loopkin"
  - subject: "Re: problems"
    date: 2002-07-02
    body: "You might want to try zapping until the latest kwintv stabalizes..  zapping has support for XV video overlay and also has capture support."
    author: "Bill Peck"
  - subject: "Video Enabled KDE"
    date: 2002-07-01
    body: "When will our KDE be Video enabled? it is KDE 3.1 or later? BTW, Please suggest a best (not so costly) AGP card for KDE+Linux for better experience. Will Nvidia Geforce works great with detonator drivers? Since performance of DE is based on the speed of rendering  by the display card."
    author: "KDE User"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-01
    body: "Don't expect proprietary drivers to work well with free software.\n\nThat's the biggest advice I could give you if you're shopping for a video card."
    author: "Neil Stevens"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-01
    body: "I have had good luck with the NVidia drivers. im running a geforce 256. I will not talk about proprietary software though. might start a flame war. "
    author: "Echo6"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-02
    body: "The kernel part of the driver is open. Nvidia's gl implemention is not."
    author: "Bluewolf"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-02
    body: "\nNo it isn't, there's an open wrapper which links in a binary-only object file"
    author: "cbcbcb"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-01
    body: "While i would generally agree with you, i have to add that i have actually had much better experiences with nvidias closed source drivers than with matrox open source drivers. I had problems with crashes earlier, but in the last year or so i havent experienced a graphics lockup once.\n\nBut yes, i wish they would open their driver, but until they do i will have to do with the closed one if i want hardware 3d accel, which i do as the newly ported opengl screensavers look darn nice :)\n\n"
    author: "troels"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-03
    body: "You can also use the latest Utah GLX to get hardware acceleration on nvidia cards. I have no idea if it's any faster or not, though."
    author: "Carbon"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-01
    body: "That isn't some type of warning about free software not working with cards that \nhave propietary drivers is it?\n"
    author: "Old Pink"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-02
    body: "Everyone that i know who runs (i mean really runs) linux all have nVidia cards: TNT2 Ultra, GeForce 1, GeForce 3 Ti and a Geforce 4 Ti.  We've had no problems running anything that we've thrown at it including quake3 and the like ... The performance is also right on par with windows (usually exact actually) with nVidia's drivers.  I also do some graphics development with SDL and not a single glitch their either ... \n\nAlso dont forget all the nVidia driver specific stuff you can do with X like cursor shadows etc... (Appendix D)   http://download.nvidia.com/XFree86_40/1.0-2960/README.txt\n\nDownside... cannot use valgrind yet!! with qt and kde using the nVidia drivers\n\nOn the other hand if you want to go with another non-nVidia card expect lengthy delays between driver releases, no extra features, and slower performance than can be had on windows.  That of course is all second hand knowledge gained mostly by laughing at some other 'friends' who try to get their cards to work But I will say that ATI support has improved but then again ..."
    author: "Jesse"
  - subject: "Re: Video Enabled KDE"
    date: 2002-07-02
    body: "I would say that I *really* use Linux - I haven't used anything else for my desktop in a couple of years.  Plus I have several friends that run Linux as their primary desktop.  None of us have nVidia cards... but then, none of us play games other than a bit of solitaire.  \n\nI personally use Matrox and ATI cards - most of my systems have one or both of these.  My current main system is a triple head system with a Matrox G400 and an ATI All-in-Wonder Pro.  I have video in and, if I switch over a monitor, video out (if I were just running dual head, I could have video out all the time).  I can easily play full screen video with no problem and watch TV in a window (well, I just moved, so I don't have TV in hooked up right now).\n\nI've never played Quake3 - it's quite possible I've never even *seen* Quake3.  My point is, you have to choose what you want to do with your system and pick a video card that matches your needs and your budget.  To me, video is nice, but I don't even know if I'm running hardware accelerated 3d - it's not like it lets me ssh to my servers any faster.  As long as I can watch a few DVDs and the occasional DivX of a Buffy episode, I'm totally satisfied.\n\nOh, and as long as I can get 1280x1024 on three monitors.  Yes, I like my screen real estate.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "If you can't get the devices to show up..."
    date: 2002-07-02
    body: "First, please read the release notes in full.  You must have the line:\n\nLoad \"v4l\"\n\nin the modules section of your XF86Config.  We don't support V4L directly yet, only Xv.\n\nNext, restart your X server after adding that line, and make sure that your bttv or other kernel drivers are loaded before your X server starts.\n\nIf you still don't get any devices, then run \"xvinfo\".  Try to decipher the output and see if it makes sense to you.  If you don't see your tuner listed there, then that's why it's not working.  If you do see it there, then please send the output to the mailing list including a description of your hardware, software, setup etc.\n\nThanks\n\n"
    author: "George Staikos"
  - subject: "Bloat"
    date: 2002-07-03
    body: "What is all this bloat for! I need to watch tv, not interface. XawTV does not give me all this crap and is very usable!"
    author: "Wouter de Vries"
  - subject: "Re: Bloat"
    date: 2002-07-03
    body: "If you switch the view mode to 'TV mode' you get a bare tv widget like xawtv.\nPersonally though I don't regard xawtv as particularly usable though (neither does its author which is why he's working on motv too).\n\nRich.\n"
    author: "Richard Moore"
---
As <a href="http://dot.kde.org/1016654707/">scheduled</a>, the <a href="http://www.kwintv.org/">KWinTV</a> <a href="http://www.kwintv.org/rewrite.html">rewrite</a> has left the vapourware stage with the first alpha now available. It <a href="http://www.kwintv.org/images/qtvision-xv-4.jpg">looks great</a> already, and should become a killer application for those of you owning a TV card. <i>" This release is intended as a basic demonstration of the design of the application. It provides functionality in the form of support for Xv video streams, OSS mixer (/dev/video, mixer 0), and XML channel files. It most likely only works on Linux, and in fact may only work on ia32 hardware. Camera devices may or may not work."</i> See the <a href="http://www.kwintv.org/rewrite_relnotes.html">Alpha 1 announcement</a> for full details on what is and what isn't working at this point.
<!--break-->
