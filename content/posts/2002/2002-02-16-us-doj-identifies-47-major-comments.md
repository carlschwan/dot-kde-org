---
title: "US DoJ Identifies 47 \"Major\" Comments"
date:    2002-02-16
authors:
  - "Dre"
slug:    us-doj-identifies-47-major-comments
comments:
  - subject: "Nice."
    date: 2002-02-15
    body: "Nice comment, Dre. I think it was very well put."
    author: "Sashmit B. Bhaduri"
  - subject: "Re: Nice."
    date: 2002-02-16
    body: "One small step for KDE, one giant leap for the Open Source community...\n\n16/17 february: FOSDEM at Brussels... they can already start celebrating :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Nice."
    date: 2002-02-16
    body: "i stumbled on this piece this morning over coffee at cnn.com and i couldn't agree more: the piece by Andreas was very thorough and well constructed IMO. great work (and exposure for KDE)!"
    author: "Aaron J. Seigo"
  - subject: "\"Major\""
    date: 2002-02-15
    body: "Eh, why is \"Major\" in quotes.  Is that supposed to be ironic or something?\n"
    author: "ac"
  - subject: "Re: \"Major\""
    date: 2002-02-15
    body: "> Eh, why is \"Major\" in quotes. Is that supposed to be ironic or something?\n\nProbably because it is the wording of, and implies the opinion of, the US DoJ Antitrust Division; something that may not be identical with, and should in any case not be confused with the wordings and opinions of Dre."
    author: "will"
  - subject: "Re: \"Major\""
    date: 2002-02-15
    body: "\nIn other words, and put very simply:\n\nQuote marks were used because the word was quoted from the source.\n\n--\nEvan"
    author: "Evan \"JabberWokky\" E."
  - subject: "Re: \"Major\""
    date: 2002-02-15
    body: "A surprisingly clumsy construct in this instance, however your surmission is seemingly wrong.  The quotes are the DoJ's.  In this case, I suppose Dre should have used \"\"Major\"\" or \\\"\"Major\"\\\" or '\"Major\"'.  Something like that.\n"
    author: "ac"
  - subject: "Yes!"
    date: 2002-02-16
    body: "To quote /.: \n\n\"wow, I enjoyed reading the response from the KDE league Inc. It seemed more relevant to me than the other responses, was brief and has a nifty introduction to KDE.\"\n\nBeautiful.\n"
    author: "KDE User"
  - subject: "Zope unstable?"
    date: 2002-02-16
    body: "\"Zope Error\nThe Zope server is down temporarily for maintenance, please try again shortly.\n...\"\n\nI can't count how many times I already saw that on dot.kde.org :-("
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Zope unstable?"
    date: 2002-02-16
    body: "You've seen that message many times, but *not* recently and *not* a fraction as often as it used to be.  Am I right?\n\nI actually caused it myself earlier today during a manual server restart.\n"
    author: "Navindra Umanee"
  - subject: "Re: Zope unstable?"
    date: 2002-02-16
    body: "You're right... the dot has been really solid lately (which befits KDE). You're doing some great work here. Thanks!"
    author: "AC"
  - subject: "Re: Zope unstable?"
    date: 2002-02-16
    body: "I posted that @12:36AM Only a few minutes after I got that \"Zope Error\" message.\n\nBefore that, dot.kde.org was _really_ getting slow with a chance you couldn't see any page at all (timed out).\n\nOnce restarted, the normal speed was back :-)"
    author: "Andy \"Storm\" Goossens"
  - subject: "Re: Zope unstable?"
    date: 2002-02-16
    body: "Part of that problem (and by no means not the only one) is that we compete for resources with other fairly busy sites.  The Zope server doesn't seem able to respond in a timely manner when the load is 10 or above, although apache itself does fine.\n\nI should try and bypass Medusa and see if I can get better performance there, but the problem may fix itself since I heard webcvs may be migrating to another box."
    author: "Navindra Umanee"
  - subject: "Kudos"
    date: 2002-02-16
    body: "That's a ton of work, and much needed.  Thanks to whoever wrote the comment.\n\nCheers,\n\nEric\n\n"
    author: "Eric E"
  - subject: "Re: Kudos"
    date: 2002-02-16
    body: "That was Dre, the keeper of the apps.\nImpressive piece of legalese."
    author: "reihal"
  - subject: "Re: Kudos"
    date: 2002-02-16
    body: "Dre, (great) writer of all press-releases, too."
    author: "Jason Katz-Brown"
  - subject: "Re: Kudos"
    date: 2002-02-18
    body: "Not to knock down Andreas (he does a lot), I was under the impression that the League gets some outside help in polishing those things."
    author: "Neil Stevens"
  - subject: "Re: Kudos"
    date: 2002-02-18
    body: "Really?  *I* was under the impression the League did nothing and was dead in the water.\n"
    author: "KDE User"
  - subject: "Now we know"
    date: 2002-02-16
    body: "where all the money went for the KDE League."
    author: "Wiggle"
  - subject: "Re: Now we know"
    date: 2002-02-16
    body: "What money ?  Did you give any ? I did not think so! So why or why are you concerned what is done with the money you have no idea existed, Trolly troll ?"
    author: "Troll Sweeper"
  - subject: "Re: Now we know"
    date: 2002-02-16
    body: "What money?  The entrance fee every organization paid to get their logo on the kde league page, as detailed in the bylaws.\n\nTry *reading* about the League sometime.  You'll be a better defender of it if you do."
    author: "Neil Stevens"
  - subject: "Re: Now we know"
    date: 2002-02-16
    body: ">>Troll Sweeper: \"What money ? Did you give any ? I did not think so!\"\n\n>>Neil Stevens: \"What money? The entrance fee every organization paid to get their logo on the kde league page, as detailed in the bylaws...\"\n\nNow I do not think I am the only one who noticed the context of the sentence, by what money, he did not mean that there was no money, he meant what money has the person complaining put into the League.  Yet another straw man argument, from the master!   Million straw men march!  Ohhh!  YEEEAAAH!"
    author: "About Ximian"
  - subject: "Re: Now we know"
    date: 2002-02-17
    body: "wiggle just had too many sour grapes, you know."
    author: "reihal"
  - subject: "The League is not KDE"
    date: 2002-02-16
    body: "Now that the League has gotten into a political matter in one country, it had better start making very clear that it does not speak for KDE.  KDE is a community built around people sharing software with each other.  If the League begins to paint KDE with a political tint, it may do irreparable harm to the KDE community by driving people away, and making people stay away.  (Note that by politicizing KDE, the League would achieve the opposite of its stated goals.)\n\nNow that the League has stepped away from its stated purpose of helping KDE by attacking someone else, it had better start making very clear that money given to it may not be used for KDE advocacy.  Otherwise that would be dishonest.  And we don't want a dishonest body representing KDE to the world.\n\nNow that the League has made clear it does not believe KDE is good enough, that the government has to go steal from someone else for KDE to thrive (although the current state of KDE makes that position obviously false), it has no business claiming to speak for KDE.  Some of us feel KDE is doing fine, and can stand up on its own merits, and doesn't need an overbearing government punishing success in the marketplace.\n\nAs an immediate measure, I recommend that a link to the League be removed from www.kde.org, and hope that the League website change its look slightly, as to not mislead people into thinking it's a part of KDE."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "\" Some of us feel KDE is doing fine, and can stand up on its own merits, and doesn't need an overbearing government punishing success in the marketplace.\"\n\nYou can definitely tell he is a troll, or a monopoly apologist from that one sentence.    MS success in the marketplace!   Stop, your slaying us!    MS is killing the marketplace!  "
    author: "About Ximian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "The proposed settlement in the Microsoft case is a legal matter that may have consequences for KDE's legal position in the future and may influence our ability to further develop KDE.\n\nI do not think that the KDE League takes a political position, it merily makes clear that the proposed settlement, which according to the existing laws in the U.S. should protect the public interest, is ineffective in protecting the interests of KDE from abuse by the defendant. \n\nI understand that you do not approve of some of the existing laws in the U.S. and that you would like to see them not enforced but those are political issues that you should discuss with your political representative. \n\nSpeaking on behalve of myself,\nWaldo"
    author: "Waldo Bastian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "My issue here isn't with the law.  My issue is with the League taking part in a political process within the law of one country.\n\nKDE is international.  KDE is diverse.  If the League is to represent KDE it must limit itself to actions that represent KDE's diversity.\n\nTaking a position in a legal process that doesn't involve KDE is no way to do that."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-17
    body: "\"...in a legal process that doesn't involve KDE...\".\n\nI think you're wrong here. This DOES involve KDE, whether you're in the USA or not.\n\nOf course, who should defend KDE (ie. a group of individuals, KDE League, etc) is another matter.\n"
    author: "Evandro"
  - subject: "Who should defend KDE?  A group of individuals"
    date: 2002-02-18
    body: "Sounds fine by me.  That way there'd be no doubt about whether the words speak for KDE, or just for those individuals within the community."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "Oh, and I am doing my part to get the law changed.  I intend to vote Libertarian this fall.  I just don't need KDE League going behind my back and taking political positions in the name of \"the developers\" to make my job harder."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "Oh shut up.\n\nThe KDE league submitted a comment to the antitrust trial, BIG DEAL.   \n\nGo read Atlas Shrugged again and hush.   "
    author: "About Ximian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "That is funny, I am a real libertarian.   I do not want politicians going behind my back and taking politicial positions in the name of \"the people\" to make my life miserable.\n\nReal libertarians are against all politicial authority.   Real libertarians do not want a seat at the table of power, they want to knock that table over.   You guys should call yourself Consitutionalists or Jeffersionists, or Randists."
    author: "About Ximian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "I guess I should be flattered, that my words have enough impact with you that you feel compelled to reply to me repeatedly, even though you have little to say about the actual point I make - about the role the KDE League should play when communicating outside of the KDE Community."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "I personally do not know how many people seem to think that they can be part of a *major* project (in any area of society) without becoming at least somewhat involved in politics. Politics is a pervasive force in every angle of daily life, and I believe that if more people would recognize and mobilize to properly direct this massive force we would be much better off that taking a \"purist\" stance wishing not to taint our activities with the stain of political involvement. Free software is a *highly* political movement. I support the KDE League's response 100%. Thank you Dre.\n\n</rant>"
    author: "Eron Lloyd"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "Please read the piece by Adreas, where you will read a disclaimer that the views in the document are those of the KDE League and not necessarily those of its members. You may also realize while reading it that this is not a political piece, but a rebuttal of the proposed settlement of a law suit.\n\nThe paper had nothing to do with KDE \"not being good enough\", but rather was all about ensuring that judgements that may adversly effect KDE's ability to showcase its ability \"to stand up on its own merits\" are not  handed down without taking into consideration the perspective of those it will effect.\n\nAnd if you are truly concerned about the quality of representation KDE gets, how about paying some attention to your typically melodramatic and conspiratorial posts/rants. I think the phrase is, \"the rafter in your own eye\"."
    author: "Aaron J. Seigo"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "I did read the piece.  The piece makes claims that the \"developers\" need \"protection,\" in direct conflict with the stated directive that the League not be involved with development.\n\nAnd try reading what I wrote before replying to it.  If Andreas Pour had sent that in as a KDE developer I wouldn't have a thing to say about it.  It's only that the piece was sent in the name of the KDE League that provokes my reply.  Unlike myself and freekde.org, the KDE League positions itself as a public representative of the KDE user and developer community."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "the league is not a public representative, but rather a public promoter of kde. there is a huge difference, and this is covered fairly well in their FAQ. when the league speaks with their own voice w/regard to kde it is no different that what you do on freekde.org; which btw has a name that also certainly sounds representative of kde and is linked to from kde.org sites. in other words you are condemning someone for doing exactly what you do."
    author: "Aaron J. Seigo"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "<blockquote>*\nKDE League Mission Statement\n\n\"To establish KDE as a desktop standard for PCs, workstations, and mobile devices, to promote software development for KDE and to promote the use of KDE by enterprises and individuals.\"\n\nKDE League Objectives\n\nTo accomplish this mission, the objectives of the League are to: \n\nsustain, provide and facilitate formal and informal education about the features, capabilities and other qualities of KDE;\nencourage corporations, governments, enterprises and individuals to use KDE;\nencourage corporations, governments, enterprises and individuals to develop for KDE;\nsustain, provide and facilitate formal and informal education about development for KDE;\nprovide expertise, information, direction and position papers regarding KDE and its development and use;\nfoster communication and cooperation between and among KDE developers; and\nfoster communication and cooperation between KDE developers and the public through publications, articles, web sites, meetings, attendance at trade shows, press releases, interviews, promotional materials and committees.\n</blockquote>\n\nHow does participating in the public comment period of a US DOJ settlement further any of these objectives, or the mission the objectives support?\n\nIf I wanted to show that KDE is a thriving user and developer community, that KDE 2.2.2 and the soon to be here KDE 3 are great environments for many segements of the computer using population, that developing for KDE is a rewarding experience, and that basing ones business on KDE (either as a user or vendor) is a safe bet, the last thing I want to do is to imply that the future of KDE is in the control of Microsoft and the US government.\n\nIf the settlement is upheld despite public comment, should people not use KDE?  The KDE League says that KDE needs protection."
    author: "Neil Stevens"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "> sustain, provide and facilitate formal and informal education\n> about the features, capabilities and other qualities of KDE;\n\nWell, it provides some of that, in the cover letter, which presents the court with something to look at it might not otherwise have considered.  So if nothing else, it educates the court, which some may consider an exceptionally important audience, about KDE.  In my opinion, this would be no different than Andreas meeting with representatives of the German Bundestag to discuss using KDE with them, something, though perhaps on some level \"political\", might do KDE a lot of good.\n\n> encourage corporations, governments, enterprises and individuals\n> to use KDE;\n> encourage corporations, governments, enterprises and individuals\n> to develop for KDE;\n\nWhether it does these things, remains to be seen, but it is possible that if the court mentions KDE in its opinion, this will bring additional attention to KDE (such as media coverage).\n\n> sustain, provide and facilitate formal and informal education\n> about development for KDE;\n\nThe comments to talk about this, again mostly in the cover letter.\n\n> provide expertise, information,\n> direction and position papers regarding KDE and its development\n> and use;\n\nHmm, this one seems to apply quite well.  Provide expertise -- probably the court would not have learnd about KDE absent this comment.  Information, it's there.  Position papers - that's what the document is.  And it all regards - both as the ultimate topic but also in examples throughout that I seemed to run across - development and use of KDE.\n\n> foster communication and cooperation between and among KDE\n> developers; and\n\nNot applicable in this case, and apparently the opposite result has been achieved, as demonstrated by this thread, at least in part.\n\n> foster communication and cooperation between KDE developers\n> and the public through publications, articles, web sites,\n> meetings, attendance at trade shows, press releases, interviews,\n> promotional materials and committees\n\nWell, again we have a \"publication\" and possibly a \"promotional material\", which is fostering communication between KDE developers and the public (it tells the public about KDE, and requests cooperation).\n\nSo, it seems within the guidelines you qouted to me (I did not check if that is correct).  Not that this means it should have been sent in by the League, but I just don't see how it violates the mission statement.  It might actually end up being a very effective promotional, educational and invitational paper.  \n\nI for one, though, hope it does not divide the KDE community.  And I don't think it will, as it seems almost everyone, even if not agreeing with every word, feel that consideration by the district court of Open Source when reaching its decision is a good thing, and, looking at the other 46, it presents a unique, Open Source perspective which hopefully will be of value to the court."
    author: "Sage"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "\"Libertarians\"(the right ones at least) believe that government muscles in too much on corporations turf and hurts the profits of corporations.   I think he objects to this more from the principle of a \"free market.\"   He thinks that this is saying KDE needs the government to protect it.   In reality the reverse is true, KDE needs to make sure that Mircosoft does not buy enough lobbying, to use it unfairly against KDE.  Given Microsoft's history, this is not something that should fall on incredulous ears. \n\nThere has never been a free market.   The earliest European corporations, like the Dutch East India Company, would drag their countries into wars and colonizations for access to markets.   The Ottomans invaded Arabia for a coffee monopoly, which they held for quite a while, until some Arabs smuggled coffee seeds into Europe.   Recently the United States invaded Afghanistan on behalf of its oil companies and they are now well entrenched on the Caspian reserves.  This has always been like this.   Rich corporations have always been hard to distinguish from their governments.   Does Mircosoft, bully the government through lobbying?   Or does the government control Microsoft, through legislation?   You cannot tell, they are seemingly one entity at times acting together and at others enemies.\n\nI think he is hung up on an ideal that does not exist and is pissed off that KDE Leauge is messing up the vaunted \"free market.\"\n\n"
    author: "About Ximian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "Oh please, ximian, not another rant about invading Afghanistan for the oil companies. \nMy country's government invaded on MY behalf. And I am against most of our current oil driven stance, I prefer not drilling in our refuges, and long (and vote for) more investment into energy sources such as Hydrogen cells. But in general I agree with miuch of what you write above and lean anywhere from Republican, Democrat, to Libertarian depending on the issue involved. Having a foothold near the Caspian reserves is just a pleasant sidenote to the Afghan military action but wasn't the main cause.\n\nAnyway...\n\n\"provide expertise, information, direction and position papers regarding KDE and its development and use\"\n\nThis seems to me what the KDE League did in this instance. And, as a happy KDE user and lover of freedom, I am glad that they took the time and effort to fight for my side rather then silently sit by while a behemoth corporation tries to buy their way out of sanctions and into a more controlling position with the help of government."
    author: "John Marttila"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "===========================\nMISSION\n\nOBJECTIVES\n\nThe mission of the League is to establish the K Desktop Environment (\"KDE\"), a network transparent contemporary desktop environment, as a desktop standard for PCs, workstations, ... To accomplish this mission, the objectives of the League are to:\n\n[snip]\n...\nprovide expertise, information, direction and position papers regarding KDE and its development and use; \n\n===========================\nA position paper submitted to the US DoJ falls within the mission of the League \"to establish the K Desktop Environment ... as a desktop standard for [PCs and workstations]\".  Education of lawmakers about the \"development and use\" of KDE is a necessary part of doing business in the real world.  \n\nIn a black and white world, it would be very easy for the KDE League to ignore that education can provide influence.  Influence is about making or shifting opinions.  The KDE League has a mandate to educate and education is not limited to other developers; it includes the general public and other opinion makers in society.  If the KDE League cannot do these things as I interpret your comments, Neil, then it cannot fulfill its mandate to establish KDE as a desktop standard.  To me, a standard must have critical mass and hence many users.\n\nGood debate...I just disagree with the narrow interpretation of the KDE League's mandate."
    author: "Phil Wiggins"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "Do not make justifications for Neil Stevens.    The US DOJ asked for public comments, even from non-Americans and that is what they got, comments.   It is good that the KDE League made a comment, let him crow and bitch all he wants about that.   The KDE League did nothing wrong.   "
    author: "About Ximian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: ">>\"Now that the League has gotten into a political matter in one country..\"\n>>\"If the League begins to paint KDE with a political tint...\"\n>>\"Note that by politicizing KDE...\"\n\nRelax.  The KDE League shows no sign of becoming a political party or overly politicized.   I do not know how you reached that conclusion, is it because they submitted a comment?   That is absurd.  Why should KDE not comment?   So they do not punish success in the marketplace and violate some mythical \"free market?\"   \n\n>>\"Now that the League has stepped away from its stated purpose of helping KDE by attacking someone else\"\n\nNice straw man argument!  Attacking someone else!!!   They just submitted a comment.   \n\n>>\"Now that the League has made clear it does not believe KDE is good enough, that the government has to go steal from someone else for KDE to thrive...\"\n\nLet us make a straw men army.      \n\n>>\"...doesn't need an overbearing government punishing success in the marketplace.\"\n\nDamn KDE League punishing the fair and successful Microsoft!   I think you should make more straw men arguments.   When you make enough, invade France.   Look Microsoft to a large extent is the government.   The right wing Jewish lobby does not even have as many politicians on their payroll as Microsoft.   \n\nP.S.  Is the plural of straw man argument, straw men arguments or straw man arguments?  I think men should be plural as well.\n\n\n\n\n\n\n\n\n\n"
    author: "About Ximian"
  - subject: "Re: Plural of \"straw man argument\""
    date: 2002-02-16
    body: "The rule is that you pluralized the most important word only. I would rule that the most important word here is \"argument\", thus the plural is \"straw man arguments\".\n\n- Michael\n"
    author: "Michael Goetze"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "If you had read the letter before starting the rant, you would have come across this sentence:\n\n\"I would also like to point out that the views and opinions in this memorandum express the views of the KDE League, and may not necessarily express the views of its members.\"\n\n"
    author: "tester"
  - subject: "Re: The League is not KDE"
    date: 2002-02-16
    body: "I have been monitoring this thread started by Neil Stevens and from clues he has posted I think I solved the problem.\n\nHypothesis -  Neil Stevens, a Libertarian (1), objects to KDE Leagues on the principal that it would be interfering(2) with the virgin sancitity of the \"free market.\"(3)\n\nProof -\n\n1. >>\"I intend to vote Libertarian this fall.\"\n\n2. >>\"...doesn't need an overbearing government punishing success in the marketplace.\"\n\n>>\"Now that the League has stepped away from its stated purpose of helping KDE by attacking someone else\"\n\n>>\"Now that the League has gotten into a political matter in one country..\"\n\nDamn KDE screwing up the Capitalist free market with their one comment.   \n\n3. One of the central positions of libertarians is that the state should move out of many of its roles and that big government is evil(note; only government with the adjective, big, appended is evil, not government in general), and that institutions of capital should take some of their roles over.   They also oppose to intereference by government on the flow of capital.  As you can see by how far Neil is willing to stretch his arguments(to the point of absurdness and absolute falsity, that is how freaking far), he is pretty passionate about this, and he does feel KDE is interfering with his iconized free market.  \n\nHe has his panties in a bunch over an ideologicial issue most of us would not consider.   Would you consider arguing that KDE submitting this comment is admitting they are inadequate?  "
    author: "About Ximian"
  - subject: "It should read:"
    date: 2002-02-16
    body: "Hypothesis - Neil Stevens, a Libertarian (1), objects to KDE League's comments on the principal that it would be interfering(2) with the virgin sancitity of the \"free market.\"(3)"
    author: "About Ximian"
  - subject: "Re: The League is not KDE"
    date: 2002-02-17
    body: "According to the letter, the current measures proposed against Microsoft are not good enough and could in fact encourage Microsoft to extend its Monopoly, thus harming KDE.\n\nNow, KDE is doing great and *can* stand up on its own merits. What it can't do is stand up while being crippled by Monopolistic actions. Now Microsoft might 'turn good' and not extend its hold on the market at the expence of taking a hit on possible profits. That is however, unlikely.\n\nIn the future Microsoft may start to tie in hardware to their software. It certainly makes sense from a business perspective. Microsoft might not even need to push it. Companies feeling the pinch from open source solutions would be interested in forcing this competition out. Let only 'trusted solutions' run on the new hardware. Broad commercial support would deter any remedial action by Government, so open source could be left out in the cold.\n\nSo, I think the kdeleague is justified in speaking out on this issue, since without acting now, in the future it might have to speak out more desperately but no one will be listening."
    author: "ac"
  - subject: "Re: The League is not KDE"
    date: 2002-02-17
    body: "> Now that the League has gotten into a political matter\n> in one country\n\nNo, for two reasons. A court case has nothing to do with politics. The U.S. is a collection of semi-autonomous states, and they are legally entitled to challenge a ruling that affects them.\n\nSecond, Microsoft is an international company. The outcome of this case will affect the O.S. marketplace the world over (even if it currently appears its effect will be to re-inforce the status quo).\n\n\n> If the League begins to paint KDE with a political tint, \n> it may do irreparable harm to the KDE community by driving\n> people away, and making people stay away.\n\nThis is a danger, however most reasonably informed people in I.T. see Microsoft as an abusive monopoly that needs serious limitations. As mentioned above, I do'nt accept that this is a political manner (and if it's in just one country, why should it affect the majority of KDE users?), however as a promotional body the KDE League is always going to risk putting _someone's_ back up with what it says.\n\n> Now that the League has stepped away from its stated purpose\n> of helping KDE by attacking someone else\n\nIt hasn't \"attacked\" anyone. It's simply given a criticism of a ruling based on it's stated opinions of a company. I can't see Bill Gates going to bed in tears because of this.\n\n> that money given to it may not be used for KDE advocacy\n\nOne, there was a helluva lot (perhaps even a little too much) advocacy of KDE in the introduction to the response. Second, by endeavouring to bring a level of equality to the marketplace it's aiding it's advocacy efforts - making it easier for people to make the choice.\n\n> Now that the League has made clear it does not believe\n> KDE is good enough\n\nI wonder why I reply to these things at all... The whole point of this thing is that even if KDE is good enough, or even better (which is IMHO, excluding config tools), in the current marketplace, with Microsoft abusing it's monopoly, it will still fail to gain acceptance. The aim of the trial, and it's ultimate solution, is to enable products like Linux/KDE and Windows XP etc. to compete on their merits. Currently they can't do that, distro's are jumping through hoops to offer solutions that are ten's of thousands of pounds cheaper, yet more stable and usable, and they've _still_ only got about 10% of the market.\n\n> Some of us feel KDE is doing fine, \n\nNo argument there\n\n> and can stand up on its own merits, \n\nI've already dealt with this one\n\n> and doesn't need an overbearing government punishing\n> success in the marketplace.\n\nThe government is not punishing success, in fact, under U.S. law a monopoly is not an offence (it is in most of the E.U. AFAIK). What it's punishing is the abuse of it's success. Microsoft is an unscrupulous company. It's main executives have almost certainly perjured themselves. It's been involved in numerous accountancy investigations with the SSEC - all settled out of court, and a countless series of monopoly investigations. It has openly defied previous rulings, and abused it's monopoly to crush opposition. It's shown itself incapable of responsibly and legally handling it's monopoly privilege, despite numerous trials, and so the various states and lawyers are trying to find a way to force it to act decently in the interests of the consumer first and it's competitors second.\n\nI gotta find better things to do on a Saturday night...\n"
    author: "Bryan Feeney"
  - subject: "Re: The League is not KDE"
    date: 2002-02-17
    body: "He is objecting, from the point of a free market.  He assumes that one exists and that this is asking the government to interfere with it.  Nice post, but it will not change his mind."
    author: "About Ximian"
  - subject: "My \"Major\" Comment"
    date: 2002-02-17
    body: "I don't give two hoots about Microsoft, and many KDE people don't either. They make nice keyboards/mice and that's about it as far as I'm concerned. Many are not interested in this US DoJ stuff and all that.  Microsoft is doing just fine and will continue doing just fine.  The US DoJ is just fine and will continue doing just fine.\n\nHowever, we can view this letter as a means to an end, and this League letter is really quite interesting from a KDE Promotion point of view.\n \nOne of the \"unofficial\" (because I'm not going to read by-laws to back that up) aims of the League has always been to promote KDE in America.  Now many will flame saying that the world is bigger than America, which is true, but the point is that marketing KDE in America to the mainstream is where KDE promotion could use the most help.  Face it, we do quite well in Europe, but GNOME has traditionally beat the crap out of us in US mainstream press.  \n\nNo popularity for KDE means fewer new developers and maybe less old developers.  KDE needs developers.  Ever notice how all the US Unix developers and companies often choose GNOME over KDE?\n\nSo worse comes to worse, this letter won't have any effect on Microsoft, but it's certainly a cheap way (I hope) for the League to bring and promote KDE to the mainstream at large.  Many people who haven't heard of KDE will suddenly become aware of it, there interest will be piqued by the short KDE introduction in the League letter.  \n\nThe US government is starting to think about KDE.  We are getting KDE into the minds of people. Microsoft will look at KDE even closer.  If we're lucky, Microsoft will say something about KDE.  It doesn't matter what.  Red Hat will have a bigger reason to switch to KDE, if KDE is popular in the US.\n\nSo that's quite a cool and cheap (I hope) way of getting some promotion for KDE, it's the beginnings of a marketing effort.  Now if we could leverage that letter somehow to get KDE even more promotion, that would be icing on the cake.  Personally I hope that the KDE League will continue using smart tactics like this to bring KDE to the americans, with their limited budget.\n\nSome of you will say it's bad thing to make KDE popular.  To that I say \"nya nya nya\".\n\nCheers,\nNavin.\n\nI speak for myself.\n"
    author: "Navindra Umanee"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: "KDE's a fine collection of software, but the US Unix companies aren't going\nto adopt it because of financial issues (their customers who want to develop\nproprietary applications will have to pay for licenses, and the money goes\nto Troll Tech, not to Sun, HP, etc).  Because the Gnome libraries are LGPLed,\nthey don't have to give money to someone else.  Clearly they are calculating\nthat they can overcome KDE's present technical lead -- Gnome may be behind\nbut it beats that CDE crap the big guys are selling now.\n"
    author: "Joe Buck"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: "Qt is cheaper than both GTK+ and Motif. Motif costs more in direct $$$ and as does GTK+ in developer time and money wasted.\n"
    author: "waggle"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: "Promote the stuff all you want, I say.  Just don't mislead people into thinking that you speak for the KDE user and/or developer community.\n\nThe KDE League positions itself as the mouthpiece of KDE.  It sets up its website to look like the KDE website, and repeatedly claims to speak for the needs of developers in the DOJ submission."
    author: "Neil Stevens"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: "\nNeil,\n\nI would just like to point out that, from reading through the comments on this story as well as related coverage on various other sites, it appears you are the only one having a problem with the comment.  This is not to say you are the only one against it, but it would suggest that a large majority in not only the KDE but also in the Open Source community think it was a positive.\n\nSo even if, as you claim, the KDE League purported to speak on behalf of the KDE community, which claim seems to lack any basis in fact, it does appear to me that it in fact has broad community support."
    author: "Sage"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: "This shouldn't be an issue.  Political views on this shouldn't be an issue.  They're needlessly divisive to the community.  Are you saying that just because maybe a majority of the vocal developers agree that you'd like to drive off the minority?\n\nKDE can use as many developers and contributing users as it can get.  Taking a political stand will only keep some people away.  After reading that, if I weren't already involved with KDE, I'd probably not join.  I wouldn't want to help an organization that holds the views outlined in Pour's piece.  Anyone who holds principled views in opposition wouldn't join an organization that violates those principles.\n\nSince the League's goal is to *maximize* the number of developers and users that help, I just ask that it either distance itself from KDE, so that it doesn't taint KDE's image with its own, or that it refrain from getting involved in non-KDE matters.  Speak up about KDE, not the other guys.\n\n(About Ximian, before you reply again: *What issues I disagree with in the article aren't at issue.*  The point is that the KDE League should refrain from being a political body to begin with, and stay in marketing as it says it is supposed to.  Why don't you go write some code for KDE instead of writing two posts for everyone I write?)"
    author: "Neil Stevens"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: "I trust you when you say that you don't care about Microsoft but you might want to consider that Microsoft might care about you. Care in the same way as a drug-dealer cares about a friend who holds back a kilo of his cocaine. This is of course a bad example because there is much more money involved in the Microsoft case than in the average drug deal.\n\nMicrosoft and its laywers are very much aware of KDE. In fact in 1999 they mention KDE and KOffice in their court filing as an example of competition to prove that they are not a monopoly. (See http://www.microsoft.com/presspass/trial/fof/IV.asp point 128 & 129) Microsoft has no trouble finding KDE when it is in their interest.\n\nAnd to remind you how Microsoft thinks about this competition... Steve Balmer thinks Linux is just as bad as cancer.\n\nSo instead of considering this as a cheap way to get some publicity, you might want to consider this as an attempt to cover your ass by someone who cares about you in the traditional sense.\n\nCheers,\nWaldo\n\nPS. we need to educate the DOJ a bit better, in court documents from that time\nhe describes KDE as \"a German firm not affiliated with Caldera\".\n"
    author: "Waldo Bastian"
  - subject: "Re: My \"Major\" Comment"
    date: 2002-02-18
    body: ">Steve Balmer thinks Linux is just as bad as cancer.\n\nI believe this is how an Agent in The Matrix describes humans. What's very funny is how The Matrix came out before Balmer made that comment, iirc. </evil-grin>"
    author: "Carbon"
  - subject: "MS Patents?"
    date: 2002-02-28
    body: "Whoa there, have a look at Exhibit H at the end of the Red Hat submission.  I assume its a list of all MS software patents awarded since 1988.  More than half were awarded in the last 2 years, and the rate of patents in the last few months has exceeded ONE PATENT PER DAY and is apparantly continuing to accelerate.\n\nScary."
    author: "Ian"
---
Those of you following the US antitrust proceedings against
<a href="http://www.microsoft.com/">Microsoft</a>
might be interested to note that the
<a href="http://www.usdoj.gov/">US DoJ</a>
<a href="http://www.usdoj.gov/atr/index.html">Antitrust Division</a>
has <a href="http://www.usdoj.gov/atr/cases/ms-major.htm">selected
47 "major"</a> comments from the
<a href="http://www.usdoj.gov/atr/cases/f10000/10043.htm">30,000+ comments</a>
submitted under the
<a href="http://www4.law.cornell.edu/uscode/15/16.html">Tunney Act</a>.
Included amongst those are comments from the
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00028788.htm">KDE
League, Inc.</a> (<a href="http://www.kde.com/league/kde_league_objection.pdf">nice PDF version</a>, <a href="http://www.kdeleague.org/">website</a>) as well as
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00030616.htm">Red Hat,
Inc.</a> (<a href="http://www.redhat.com/">website</a>).
After a brief review, other comments making significant references to
Open Source include
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00008557.htm">John A. Carroll</a>,
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00025808.htm">Steven Waldman</a>,
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00028313.htm">Ralph
Nader and James Love</a>,
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00030600.htm">The American Antitrust Institute</a> and the
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00033734.htm">U.S. Senate</a> (mainly Red Hat's testimony),
and comments making some reference to Open Source include
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00033613.htm">Consumers for Computing Choice and Open Platform Working Group</a>,
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00012543.htm">Paul Johnson</a>,
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00028571.htm">Dan Kegel</a>,
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00030468.htm">Mason
Thomas</a> and
<a href="http://www.usdoj.gov/atr/cases/ms_tuncom/major/mtc-00029411.htm">SBC Communications Inc.</a>.
Hopefully this volume of comments
will ensure that the district court will pay adequate attention to the
issues confronting Open Source developers in particular when reviewing the
proposed settlement.



<!--break-->
